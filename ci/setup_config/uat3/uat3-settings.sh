export ENV_PREFFIX=uat3
export HYBRIS_SERVERS_INT="ip-172-31-61-31.ec2.internal ip-172-31-61-30.ec2.internal"
export APACHE_SERVERS_INT="ip-172-31-62-53.ec2.internal ip-172-31-54-32.ec2.internal ip-172-31-63-71.ec2.internal"
export HYBRIS_SERVER_A="ip-172-31-61-31.ec2.internal"
export HYBRIS_SERVER_B="ip-172-31-61-30.ec2.internal"
export HYBRIS_SERVER_HOME=/app/hybris
export HYBRIS_HOME=/app/hybris-${ENV_PREFFIX}/6.2/hybris
export HYBRIS_BIN=$HYBRIS_HOME/bin
export HYBRIS_TEMP=$HYBRIS_HOME/temp/hybris
export SETUP_CLUSTERED=true
declare -A CLUSTER_ID=( ["ip-172-31-61-31.ec2.internal"]=0 ["ip-172-31-61-30.ec2.internal"]=1 )

export SOLR_SERVERS_INT="ip-172-31-61-113.ec2.internal"
declare -A SOLR_CLUSTER=( ["ip-172-31-61-113.ec2.internal"]="master slave" )
export SOLR_MODE=master_slave

export APACHE_LOCAL_SERVERS_INT="ip-172-31-62-53.ec2.internal"
export AGS_ROUTING_SERVERS_INT="ip-172-31-63-71.ec2.internal"
export AGS_ENTRY_POINT="uat3.graphicstandards.com"
export WEL_ROUTING_SERVERS_INT="ip-172-31-54-32.ec2.internal"
export WEL_ENTRY_POINT="uat3.efficientlearning.com"
export LOCAL_ENTRY_POINT="eCommerce-UAT3-Web-ELB-1702442707.us-east-1.elb.amazonaws.com"
export BO_ENTRY_POINT="uat3.hybris.wiley.com"

export DB_HOST=ecommerce-nonprod-db.chof5nbig7hv.us-east-1.rds.amazonaws.com
export DB_NAME=ecommUAT3
export DB_USER=ecommUAT3
export DB_PASSWORD=passUAT3Usr_23
