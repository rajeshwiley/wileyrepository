export ENV_PREFFIX=uat2
export HYBRIS_SERVERS_INT="ip-172-31-62-82.ec2.internal ip-172-31-56-209.ec2.internal"
export APACHE_SERVERS_INT="ip-172-31-5-8.ec2.internal ip-172-31-54-176.ec2.internal ip-172-31-49-56.ec2.internal ip-172-31-48-126.ec2.internal"
export HYBRIS_SERVER_A="ip-172-31-62-82.ec2.internal"
export HYBRIS_SERVER_B="ip-172-31-56-209.ec2.internal"
export HYBRIS_SERVER_HOME=/app/hybris
export HYBRIS_HOME=/app/hybris-${ENV_PREFFIX}/6.2/hybris
export HYBRIS_BIN=$HYBRIS_HOME/bin
export HYBRIS_TEMP=$HYBRIS_HOME/temp/hybris
export SETUP_CLUSTERED=true
declare -A CLUSTER_ID=( ["ip-172-31-62-82.ec2.internal"]=0 ["ip-172-31-56-209.ec2.internal"]=1 )

export SOLR_SERVERS_INT="ip-172-31-56-251.ec2.internal"
declare -A SOLR_CLUSTER=( ["ip-172-31-56-251.ec2.internal"]="master slave" )
export SOLR_MODE=master_slave

export APACHE_LOCAL_SERVERS_INT="ip-172-31-5-8.ec2.internal"
export AGS_ROUTING_SERVERS_INT="ip-172-31-54-176.ec2.internal"
export AGS_ENTRY_POINT="uat2.graphicstandards.com"
export WEL_ROUTING_SERVERS_INT=""
export WEL_ENTRY_POINT="uat2.efficientlearning.com"
export B2B_B2C_ROUTING_SERVERS_INT="ip-172-31-48-126.ec2.internal"
export B2B_ENTRY_POINT="uat2.business.wiley.com"
export B2C_ENTRY_POINT="uat2.store.wiley.com"
export LOCAL_ENTRY_POINT="eCommerce-UAT2-Web-ELB-458001561.us-east-1.elb.amazonaws.com"
export BO_ENTRY_POINT="uat2.hybris.wiley.com"

export DB_HOST=ecommerce-nonprod-db.chof5nbig7hv.us-east-1.rds.amazonaws.com
export DB_NAME=ecommUAT2
export DB_USER=ecommUAT2
export DB_PASSWORD=passUAT2Usr_23
