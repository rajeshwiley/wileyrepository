export ENV_PREFFIX=perf
export HYBRIS_SERVERS_INT="ip-172-31-35-148.ec2.internal ip-172-31-37-48.ec2.internal"
export APACHE_SERVERS_INT="ip-172-31-46-27.ec2.internal ip-172-31-43-169.ec2.internal ip-172-31-62-198.ec2.internal ip-172-31-11-50.ec2.internal ip-172-31-50-74.ec2.internal"
export HYBRIS_SERVER_A="ip-172-31-35-148.ec2.internal"
export HYBRIS_SERVER_B="ip-172-31-37-48.ec2.internal"
export HYBRIS_SERVER_HOME=/app/hybris
export HYBRIS_HOME=/app/hybris-${ENV_PREFFIX}/6.2/hybris
export HYBRIS_BIN=$HYBRIS_HOME/bin
export HYBRIS_TEMP=$HYBRIS_HOME/temp/hybris
export SETUP_CLUSTERED=true
declare -A CLUSTER_ID=( ["ip-172-31-35-148.ec2.internal"]=0 ["ip-172-31-37-48.ec2.internal"]=1 )

export SOLR_SERVERS_INT="ip-172-31-45-35.ec2.internal ip-172-31-36-218.ec2.internal"
declare -A SOLR_CLUSTER=( ["ip-172-31-45-35.ec2.internal"]="master" ["ip-172-31-36-218.ec2.internal"]="slave" )
export SOLR_MODE=master_slave

export APACHE_LOCAL_SERVERS_INT="ip-172-31-46-27.ec2.internal ip-172-31-43-169.ec2.internal"
export AGS_ROUTING_SERVERS_INT="ip-172-31-62-198.ec2.internal"
export AGS_ENTRY_POINT="perf.graphicstandards.com"
export WEL_ROUTING_SERVERS_INT="ip-172-31-11-50.ec2.internal"
export WEL_ENTRY_POINT="perf.efficientlearning.com"
export B2B_B2C_ROUTING_SERVERS_INT="ip-172-31-50-74.ec2.internal"
export B2B_ENTRY_POINT="perf.business.wiley.com"
export B2C_ENTRY_POINT="perf.store.wiley.com"
export LOCAL_ENTRY_POINT="eCommerce-Perf-Web-ELB-1719545505.us-east-1.elb.amazonaws.com"
export BO_ENTRY_POINT="perf.hybris.wiley.com"
