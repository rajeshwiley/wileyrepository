export ENV_PREFFIX=uat
export HYBRIS_SERVERS_INT="ip-172-31-63-199.ec2.internal ip-172-31-58-156.ec2.internal"
export APACHE_SERVERS_INT="ip-172-31-48-179.ec2.internal ip-172-31-48-9.ec2.internal ip-172-31-11-176.ec2.internal ip-172-31-58-17.ec2.internal"
export HYBRIS_SERVER_A="ip-172-31-63-199.ec2.internal"
export HYBRIS_SERVER_B="ip-172-31-58-156.ec2.internal"
export HYBRIS_SERVER_HOME=/app/hybris
export HYBRIS_HOME=/app/hybris-${ENV_PREFFIX}/6.2/hybris
export HYBRIS_BIN=$HYBRIS_HOME/bin
export HYBRIS_TEMP=$HYBRIS_HOME/temp/hybris
export SETUP_CLUSTERED=true
declare -A CLUSTER_ID=( ["ip-172-31-63-199.ec2.internal"]=0 ["ip-172-31-58-156.ec2.internal"]=1 )

export SOLR_SERVERS_INT="ip-172-31-55-62.ec2.internal"
declare -A SOLR_CLUSTER=( ["ip-172-31-55-62.ec2.internal"]="master slave" )
export SOLR_MODE=master_slave

export APACHE_LOCAL_SERVERS_INT="ip-172-31-48-179.ec2.internal"
export AGS_ROUTING_SERVERS_INT="ip-172-31-48-9.ec2.internal"
export AGS_ENTRY_POINT="uat.graphicstandards.com"
export WEL_ROUTING_SERVERS_INT="ip-172-31-11-176.ec2.internal"
export WEL_ENTRY_POINT="uat.efficientlearning.com"
export B2B_B2C_ROUTING_SERVERS_INT="ip-172-31-58-17.ec2.internal"
export B2B_ENTRY_POINT="uat.business.wiley.com"
export B2C_ENTRY_POINT="uat.store.wiley.com"
export LOCAL_ENTRY_POINT="eCommerce-UAT-Web-ELB-997773317.us-east-1.elb.amazonaws.com"
export BO_ENTRY_POINT="uat.hybris.wiley.com"
