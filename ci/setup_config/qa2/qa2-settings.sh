export ENV_PREFFIX=qa2
export HYBRIS_SERVERS_INT="ip-172-31-44-238.ec2.internal ip-172-31-43-30.ec2.internal"
export APACHE_SERVERS_INT="ip-172-31-45-62.ec2.internal ip-172-31-48-61.ec2.internal"
export HYBRIS_SERVER_A="ip-172-31-44-238.ec2.internal"
export HYBRIS_SERVER_B="ip-172-31-43-30.ec2.internal"
export HYBRIS_SERVER_HOME=/app/hybris
export HYBRIS_HOME=/app/hybris-${ENV_PREFFIX}/6.2/hybris
export HYBRIS_BIN=$HYBRIS_HOME/bin
export HYBRIS_TEMP=$HYBRIS_HOME/temp/hybris
export SETUP_CLUSTERED=true
declare -A CLUSTER_ID=( ["ip-172-31-44-238.ec2.internal"]=0 ["ip-172-31-43-30.ec2.internal"]=1 )

export SOLR_SERVERS_INT="ip-172-31-40-78.ec2.internal"
declare -A SOLR_CLUSTER=( ["ip-172-31-40-78.ec2.internal"]="master slave" )
export SOLR_MODE=master_slave

export APACHE_LOCAL_SERVERS_INT="ip-172-31-45-62.ec2.internal"
export AGS_ROUTING_SERVERS_INT=""
export AGS_ENTRY_POINT="qa2.graphicstandards.com"
export B2B_B2C_ROUTING_SERVERS_INT="ip-172-31-48-61.ec2.internal"
export B2B_ENTRY_POINT="qa2.business.wiley.com"
export B2C_ENTRY_POINT="qa2.store.wiley.com"
export LOCAL_ENTRY_POINT="eCommerce-QA2-Web-ELB-1602085096.us-east-1.elb.amazonaws.com"
export BO_ENTRY_POINT="qa2.hybris.wiley.com"

export DB_HOST=ecommerce-nonprod-db.chof5nbig7hv.us-east-1.rds.amazonaws.com
export DB_NAME=ecommQA2
export DB_USER=ecommQA2
export DB_PASSWORD=passQA2Usr_23