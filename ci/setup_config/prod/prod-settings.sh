export ENV_PREFFIX=prod
export HYBRIS_SERVERS_INT="ip-172-31-44-18.ec2.internal ip-172-31-40-20.ec2.internal"
export APACHE_SERVERS_INT="ip-172-31-36-230.ec2.internal ip-172-31-41-246.ec2.internal ip-172-31-51-65.ec2.internal ip-172-31-17-241.ec2.internal"
export HYBRIS_SERVER_A="ip-172-31-44-18.ec2.internal"
export HYBRIS_SERVER_B="ip-172-31-40-20.ec2.internal"
export HYBRIS_SERVER_HOME=/app/hybris
export HYBRIS_HOME=/app/hybris-${ENV_PREFFIX}/6.2/hybris
export HYBRIS_BIN=$HYBRIS_HOME/bin
export HYBRIS_TEMP=$HYBRIS_HOME/temp/hybris
export SETUP_CLUSTERED=true
declare -A CLUSTER_ID=( ["ip-172-31-44-18.ec2.internal"]=0 ["ip-172-31-40-20.ec2.internal"]=1 )

export SOLR_SERVERS_INT="ip-172-31-44-53.ec2.internal ip-172-31-39-140.ec2.internal"
declare -A SOLR_CLUSTER=( ["ip-172-31-44-53.ec2.internal"]="master" ["ip-172-31-39-140.ec2.internal"]="slave" )
export SOLR_MODE=master_slave

export APACHE_LOCAL_SERVERS_INT="ip-172-31-36-230.ec2.internal ip-172-31-41-246.ec2.internal"
export AGS_ROUTING_SERVERS_INT="ip-172-31-51-65.ec2.internal"
export AGS_ENTRY_POINT="www.graphicstandards.com"
export WEL_ROUTING_SERVERS_INT="ip-172-31-17-241.ec2.internal"
export WEL_ENTRY_POINT="www.efficientlearning.com"
export LOCAL_ENTRY_POINT="eCommerce-Prod-Web-ELB-983834451.us-east-1.elb.amazonaws.com"
export BO_ENTRY_POINT="www.hybris.wiley.com"
export WEL_ROUTING_SERVERS_INT="ip-172-31-17-241.ec2.internal"
export WEL_ENTRY_POINT="www.efficientlearning.com"
