export ENV_PREFFIX=dev
export HYBRIS_SERVERS_INT="ip-172-31-58-97.ec2.internal"
export DB_SERVER_INT="ip-172-31-61-186.ec2.internal"
export APACHE_SERVERS_EXT="ec2-52-71-165-105.compute-1.amazonaws.com"
export APACHE_SERVERS_INT="ip-172-31-51-10.ec2.internal"
export HYBRIS_SERVER_A="ip-172-31-58-97.ec2.internal"
export HYBRIS_SERVER_HOME=/app/hybris
export HYBRIS_HOME=/app/hybris-${ENV_PREFFIX}/6.2/hybris
export HYBRIS_BIN=$HYBRIS_HOME/bin
export HYBRIS_TEMP=$HYBRIS_HOME/temp/hybris
export SETUP_CLUSTERED=false
declare -A CLUSTER_ID=( ["ip-172-31-58-97.ec2.internal"]=0 )

export APACHE_LOCAL_SERVERS_INT=""
export AGS_ROUTING_SERVERS_INT=""
export AGS_ENTRY_POINT=""
export LOCAL_ENTRY_POINT=""
export BO_ENTRY_POINT=""

export DB_HOST=ecommerce-nonprod-db.chof5nbig7hv.us-east-1.rds.amazonaws.com
export DB_NAME=ecommDEV
export DB_USER=ecommDEV
export DB_PASSWORD=passDEVUsr_23
