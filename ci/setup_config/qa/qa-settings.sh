export ENV_PREFFIX=qa
export HYBRIS_SERVERS_INT="ip-172-31-60-199.ec2.internal ip-172-31-58-2.ec2.internal"
export APACHE_SERVERS_INT="ip-172-31-54-98.ec2.internal  ip-172-31-49-94.ec2.internal ip-172-31-60-221.ec2.internal"
export HYBRIS_SERVER_A="ip-172-31-60-199.ec2.internal"
export HYBRIS_SERVER_B="ip-172-31-58-2.ec2.internal"
export HYBRIS_SERVER_HOME=/app/hybris
export HYBRIS_HOME=/app/hybris-${ENV_PREFFIX}/6.2/hybris
export HYBRIS_BIN=$HYBRIS_HOME/bin
export HYBRIS_TEMP=$HYBRIS_HOME/temp/hybris
export SETUP_CLUSTERED=true
declare -A CLUSTER_ID=( ["ip-172-31-60-199.ec2.internal"]=0 ["ip-172-31-58-2.ec2.internal"]=1 )

export SOLR_SERVERS_INT="ip-172-31-49-75.ec2.internal"
declare -A SOLR_CLUSTER=( ["ip-172-31-49-75.ec2.internal"]="master slave" )
export SOLR_MODE=master_slave

export APACHE_LOCAL_SERVERS_INT="ip-172-31-54-98.ec2.internal"
export AGS_ROUTING_SERVERS_INT=""
export AGS_ENTRY_POINT="qa.graphicstandards.com"
export WEL_ROUTING_SERVERS_INT="ip-172-31-49-94.ec2.internal"
export WEL_ENTRY_POINT="qa.efficientlearning.com"
export B2B_B2C_ROUTING_SERVERS_INT="ip-172-31-60-221.ec2.internal"
export B2B_ENTRY_POINT="qa.business.wiley.com"
export B2C_ENTRY_POINT="qa.store.wiley.com"
export LOCAL_ENTRY_POINT="eCommerce-QA-Web-ELB-1507064656.us-east-1.elb.amazonaws.com"
export BO_ENTRY_POINT="qa.hybris.wiley.com"

export DB_HOST=ecommerce-nonprod-db.chof5nbig7hv.us-east-1.rds.amazonaws.com
export DB_NAME=ecommQA
export DB_USER=ecommQA
export DB_PASSWORD=passQAUsr_23
