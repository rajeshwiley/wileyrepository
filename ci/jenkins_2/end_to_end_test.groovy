pipeline {
    environment {
        NAME_ENV = 'prtest'
        WORKDIR = '/srv/hybris_build'
        PLATFORM_HOME = "$WORKDIR/hybris/bin/platform/"
        SCRIPTS = "$WORKDIR/scripts"
        LOCAL_PROPERTIES_DIR = "$WORKDIR/hybris/config/properties/$NAME_ENV"
        // File for rewriting of properties on-the-fly
        LOCAL_PROPERTIES_REWRITE = "$WORKDIR/99-local.properties"
        MYSQL_DOCKER_NAME = "mysql-prtest-end-$BUILD_NUMBER"
        MYSQL_PASSWORD = "FmW04BD4WY978_T74sR5gg1Q"
        HYBRIS_DOCKER_NAME = "hybris-prtest-end-$BUILD_NUMBER"
        TMP_HYBRIS_SERVER = "$WORKDIR/hybris/temp/hybrisDockerfiles"
        DOCKER_FOLDER_PATH = "$WORKDIR/hybris/docker"
        PROD_DATA_ORIGIN_FOLDER = "/opt/prod_dump"
        MYSQL_DATA_ORIGIN_FOLDER = "/opt/mysql_data"
        MYSQL_WORK_FOLDER = "/srv/pr_mysql_data"
        HYBRIS_MEDIA_ORIGIN = "sys-master.zip"
        HYBRIS_MEDIA_WORK_FOLDER = "/srv/pr_media/sys-master"
        DOCKER_HYBRIS_LOCAL_PROPERTIES_FILE = "/srv/hybris/config/properties/$NAME_ENV/99-local.properties"
        DOCKER_HYBRIS_MEDIA_WORK_FOLDER = "/srv/hybris/data/media/sys_master/sys-master" // temporary workaround
        DOCKER_REGISTRY_HYBRIS_REPO = "registry.ecomm-internal.wiley.com/hybris"
    }
    agent {
        node {
            label "${jenkins_node}"
        }
    }
    stages {
        stage('GIT checkout') {
            steps {
                dir("$WORKDIR/") {
                    checkout([$class                           : 'GitSCM',
                              branches                         : [[name: "${GIT_VERSION}"]],
                              userRemoteConfigs                : [[credentialsId: 'e18299fb-8d19-414a-b9e4-1c6d75178974',
                                                                   url          : 'git@github.com:John-Wiley-Sons/hybwiley.git']]])
                }
            }
        }
        stage('GIT tag create') {
            steps {
                    sh """cd $WORKDIR
                      git config user.email 'jenkins@wiley.com'
                      git config user.name 'jenkins'
                      git tag -a  ${GIT_TAG_NAME}  -m 'Jenkins'
                    """
            }
        }
        stage('ant clean all') {
            steps {
                ansiColor('xterm') {
                    echo "ant clean"
                    sh '''#!/bin/bash
                    set -x
                    rm -rf $WORKDIR/hybris/log/*
                    cd $PLATFORM_HOME
                    . ./setantenv.sh
                    ant customize
                    ant clean build
                    echo "should be removed after maven proxy enable"
                    aws s3 cp s3://wiley-ecomm-dev/Docker_Artefacts/mysql-connector-java-5.1.39-bin.jar $PLATFORM_HOME/lib/dbdriver/'''
                }
            }
        }
        stage('ant production') {
            steps {
                echo "ant production"
                sh '''#!/bin/bash
                set -x
                cd $PLATFORM_HOME
                . ./setantenv.sh
                rm -rf $TMP_HYBRIS_SERVER/*
                ant production
                ant dockerfiles'''
            }
        }
        stage('Create Docker') {
            steps {
                echo "Create Docker"
                sh returnStdout: true, script: '''#!/bin/bash
                set -e
                set -x
                cd $TMP_HYBRIS_SERVER
                sudo docker build -t hybris-$BUILD_NUMBER .'''
            }
        }
        stage('Restore data from origin for PRTEST') {
            steps {
                echo "Create MySQL"
                sh '''#!/bin/bash
                set -x
                echo "Restoring MySQL data for PRTEST..."
                sudo rsync -a --delete $MYSQL_DATA_ORIGIN_FOLDER/ $MYSQL_WORK_FOLDER
                echo "Restoring Media files for PRTEST..."
                sudo rm -rf $HYBRIS_MEDIA_WORK_FOLDER
                sudo mkdir -p $HYBRIS_MEDIA_WORK_FOLDER
                sudo unzip -q $PROD_DATA_ORIGIN_FOLDER/$HYBRIS_MEDIA_ORIGIN -d $HYBRIS_MEDIA_WORK_FOLDER/
                sudo chown -R 830 $HYBRIS_MEDIA_WORK_FOLDER'''
            }
        }
        stage('Prepare local properties for hybris in docker'){
            steps {
                sh '''
                set -x
                sudo su hybris
                echo "Rewriting local properties..."
                echo "db.dns=$MYSQL_DOCKER_NAME" > $LOCAL_PROPERTIES_REWRITE
                echo "db_schema=prtest" >> $LOCAL_PROPERTIES_REWRITE
                echo "db.username=root" >> $LOCAL_PROPERTIES_REWRITE
                echo "db.password=$MYSQL_PASSWORD" >> $LOCAL_PROPERTIES_REWRITE
                echo "Disable S3 media storage"
                echo "media.default.storage.strategy=localFileMediaStorageStrategy" >> $LOCAL_PROPERTIES_REWRITE
                echo "Add hybris user permission"
                '''
            }

        }
        stage('Run ant incrementalupdate in docker') {
            steps {
                sh '''#!/bin/bash
                set -x
                echo "Starting MySQL container..."
                sudo docker run -d --rm --name $MYSQL_DOCKER_NAME -v $MYSQL_WORK_FOLDER:/var/lib/mysql pr_mysql --innodb-flush-log-at-trx-commit=0
                '''

                // Check if MySQL already started
                script {
                    DB_IP = sh(script: "sudo docker inspect -f '{{ .NetworkSettings.IPAddress}}' $MYSQL_DOCKER_NAME", returnStdout: true).trim()
                    def i = 100
                    while(true) {
                        sleep 3
                        db_status = sh(script: "mysqlshow -h$DB_IP -uroot -p$MYSQL_PASSWORD", returnStatus: true)
                        if(db_status == 0) {
                            break
                        }
                        i--
                    }
                }
                withCredentials([string(credentialsId: '1a3420c2-b2f0-4eee-b4f1-61026536fd9d', variable: 'HYBRIS_SECRET')]) {
                    sh '''#!/bin/bash
                    set -x
                    echo "Starting Hybris containers ant incrementalupdate"
                    sudo docker run --rm --link $MYSQL_DOCKER_NAME --name $HYBRIS_DOCKER_NAME \
                    -v $HYBRIS_MEDIA_WORK_FOLDER:$DOCKER_HYBRIS_MEDIA_WORK_FOLDER \
                    -v $LOCAL_PROPERTIES_REWRITE:$DOCKER_HYBRIS_LOCAL_PROPERTIES_FILE \
                    -e NAME_ENV=$NAME_ENV \
                    -e HYBRIS_SECRET=$HYBRIS_SECRET \
                     hybris-$BUILD_NUMBER ant incrementalupdate'''
                }

                // Check logs to find out errors
                LogParser()
                // If build failed, skip further stages
                script {
                    if (currentBuild.result == "FAILURE") {
                        error("Ant incrementalupdate in docker failed")
                    }
                }
            }
        }
        stage('Start Hybris') {
            steps {
                withCredentials([string(credentialsId: '1a3420c2-b2f0-4eee-b4f1-61026536fd9d', variable: 'HYBRIS_SECRET')]) {
                    sh '''#!/bin/bash
                    set -x
                    echo "Starting Hybris containers..."
                    sudo docker run -d --rm --link $MYSQL_DOCKER_NAME --name $HYBRIS_DOCKER_NAME \
                    -v $HYBRIS_MEDIA_WORK_FOLDER:$DOCKER_HYBRIS_MEDIA_WORK_FOLDER \
                    -v $LOCAL_PROPERTIES_REWRITE:$DOCKER_HYBRIS_LOCAL_PROPERTIES_FILE \
                    -e NAME_ENV=$NAME_ENV \
                    -e HYBRIS_SECRET=$HYBRIS_SECRET \
                     hybris-$BUILD_NUMBER'''
                }

                // Check if Hybris already started
                script {
                    def i = 140
                    while(true) {
                        sleep 15
                        status = sh(script: "sudo docker inspect -f '{{ .State.Health.Status }}' $HYBRIS_DOCKER_NAME", returnStdout: true).trim()
                        println(status)
                        if(status == 'healthy') {
                            break
                        }
                        i--
                    }
                }

                sh '''#!/bin/bash
                set -x
                sudo docker logs $HYBRIS_DOCKER_NAME
                '''
                // Check logs to find out errors
                LogParser()
                // If build failed, skip further stages
                script {
                    if (currentBuild.result == "FAILURE") {
                        error("Hybris start in docker failed")
                    }
                }
            }
        }
//        TODO How we will run those scope of test
        stage('ant manualtests') {
            steps {
                echo "ant manualtests"
//                sh '''#!/bin/bash
//                set -x
//                cd $PLATFORM_HOME
//                . ./setantenv.sh
//                ant allwebtests
//                '''

            }
        }
        stage('postman') {
            steps {
                echo "postman"
                sh '''#!/bin/bash
                set -x
                sudo rm -rf newman
                echo "{\\"values\\":[{\\"key\\":\\"domain\\",\\"value\\":\\"$HYBRIS_DOCKER_NAME:9002\\"}]}" > $WORKSPACE/environment.json
                cp -r $SCRIPTS/postman $WORKSPACE
                sudo docker run --rm --name newman --link $HYBRIS_DOCKER_NAME -v $WORKSPACE/:/etc/newman \\
                    --entrypoint /bin/bash postman/newman:ubuntu -c "npm i -g newman-reporter-html; newman run postman/wiley.postman_collection.json \\
                    -g postman/testing.postman_environment.json --folder tests --insecure \\
                    -e environment.json -r html,cli --suppress-exit-code"
                sudo chown -R jenkins:jenkins newman
                '''
                publishHTML(target: [
                        reportName           : 'newman',
                        reportDir            : 'newman',
                        reportFiles          : 'newman*.html',
                        keepAll              : true,
                        alwaysLinkToLastBuild: true,
                        allowMissing         : false])
                // Check logs to find out errors
                LogParser(true)
            }
        }
//        TODO add UI test in parallel
        stage('UI test') {
            steps {
                echo "UI test"
//                sh '''#!/bin/bash
//                set -x
//                cd $PLATFORM_HOME
//                . ./setantenv.sh
//                ant allwebtests
//                '''

            }
        }
        stage('Push Hybris image to registry') {
            steps {
                script {
                    // Store Hybris container for DEV/RELEASE job only
                    if ("$HYBRIS_IMAGE_TAG" != "") {
                        echo "DEBUG: TAG name is $HYBRIS_IMAGE_TAG"
                        echo "DEBUG: Tagging hybris image..."
                        sh(script: "sudo docker tag hybris-$BUILD_NUMBER $DOCKER_REGISTRY_HYBRIS_REPO:$HYBRIS_IMAGE_TAG")
                        echo "DEBUG: Pushing hybris image to Docker registry..."
                        sh(script: "sudo docker push $DOCKER_REGISTRY_HYBRIS_REPO:$HYBRIS_IMAGE_TAG")
                    } else {
                        echo "DEBUG: Skipping push Hybris image to registry"
                    }
                }
            }
        }
    }
    post {
        always {
//            input "wait"
            sh 'sudo docker stop $(sudo docker ps -q)|| true'
        }
        failure {
            echo "failure"
            // Email to github pr author will be sent in parent job
        }
        // Cleanup after post failure, otherwise groovy scripts will be deleted before
        cleanup {
            deleteDir()
        }
    }
}

def LogParser(unstableOnWarning = false) {
    step([$class           : 'LogParserPublisher',
          failBuildOnError : true,
          unstableOnWarning: unstableOnWarning,
          projectRulePath  : "ci/jenkins_2/lib/parser_error",
          useProjectRule   : true])
}