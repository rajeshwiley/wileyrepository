def normalizeBranchName(branch) {
    // localize branch name
    branch = branch.replace('origin/', '')
    // Truncate branch name
    if (branch.length() > 20) {
        branch = branch.substring(0, 20)
    }
    // Replace unsupported symbols
    allowed = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
               'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
               '0','1','2','3','4','5','6','7','8','9', '-', '.']

    // New branch name, will be filled during normalization
    normalizedBranchName = ""
    // Iterate over every symbol in branch name
    branch.each {
        String s ->
            // Take every symbol, search it in allowed list
            // If found, then concatenate symbol to new branch name
            // If not found, then replace symbol by dash
            normalizedBranchName += (allowed.find { it == s}) ? s : '-'
    }
    return normalizedBranchName
}

def getImageTag(branch, commit) {
    // Truncate git commit SHA hash
    commit = commit.substring(0, 7)

    branchName = normalizeBranchName(branch)

    // Prepare tag for Docker image
    // Separator symbol is underscore
    return branchName + '_' + commit + '_' + System.currentTimeMillis()
}

def getLatestTag(mainlines, branch) {
    // Convert stringified list to Array of String
    mainlineList = mainlines.split(',')
    branchName = normalizeBranchName(branch)

    for (mainline in mainlineList) {
        // Normalize mainline name
        normalizedMainlineName = normalizeBranchName(mainline)

        if (branchName.contains(normalizedMainlineName)) {
            return 'latest_' + normalizedMainlineName
        }
    }
    return false
}

return this