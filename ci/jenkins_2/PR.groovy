pipeline {
    environment {
        TAG_DATE = sh(returnStdout: true, script: "date +%Y%m%d%H%M").trim()
        GIT_TAG_NAME = "${env.JOB_NAME}_${TAG_DATE}" //  timestamp for unique tag name
    }
    options {
        timeout(time: 8, unit: 'HOURS')
        parallelsAlwaysFailFast()
    }
    agent {
        node {
            label "master"
        }
    }
    stages {
        stage('WorkWithGitHubLabels') {
            steps {
                script {
                    githubPRRemoveLabels labelProperty: labels('test_me')
                    githubPRRemoveLabels labelProperty: labels('need CI tests support')
                    githubPRRemoveLabels labelProperty: labels('Junit test Passed')
                    githubPRRemoveLabels labelProperty: labels('Junit test FAILED')
                    githubPRRemoveLabels labelProperty: labels('end to end test Passed')
                    githubPRRemoveLabels labelProperty: labels('end to end test FAILED')
                }
                script {
                    githubPRAddLabels labelProperty: labels('CI tests in progress')
                }
            }
        }
        stage('PR') {
            parallel {
                stage('junit_tests') {
                    steps {
                        script {
                            junit_tests = build(job: 'junit_tests',
                                    propagate: false, parameters: [[$class: 'StringParameterValue',
                                                                    name  : 'jenkins_node', value: "${jenkins_node}"],
                                                                   [$class: 'StringParameterValue',
                                                                    name  : 'GIT_VERSION', value: "${GITHUB_PR_HEAD_SHA}"]])
                            if (junit_tests.result == 'FAILURE') {
                                error("junit_tests")
                            }
                        }
                    }
                    post {
                        success {
                            // Add GitHub Label from PR
                            script {
                                githubPRAddLabels labelProperty: labels('Junit test Passed')
                            }
                        }
                        failure {
                            // Add GitHub Label from PR
                            script {
                                githubPRAddLabels labelProperty: labels('Junit test FAILED')
                                githubPRComment comment: githubPRMessage("Build ${junit_tests.number} ${junit_tests.result} ${junit_tests.absoluteUrl}")
                            }
                        }
                    }
                }
                stage('end_to_end_test') {
                    steps {
                        script {
                            end_to_end_test = build(job: 'end_to_end_test',
                                    propagate: false, parameters: [[$class: 'StringParameterValue',
                                                                    name  : 'GIT_TAG_NAME', value: "$GIT_TAG_NAME"],
                                                                   [$class: 'StringParameterValue',
                                                                    name  : 'jenkins_node', value: "${jenkins_node}"],
                                                                   [$class: 'StringParameterValue',
                                                                    name  : 'GIT_VERSION', value: "${GITHUB_PR_HEAD_SHA}"]])
                            if (end_to_end_test.result == 'FAILURE') {
                                error("end_to_end_test")
                            }
                        }
                    }
                    post {
                        success {
                            script {
                                githubPRAddLabels labelProperty: labels('end to end test Passed')
                            }
                        }
                        failure {
                            // Add GitHub Label from PR
                            script {
                                githubPRAddLabels labelProperty: labels('end to end test FAILED')
                                githubPRComment comment: githubPRMessage("Build ${end_to_end_test.number} ${end_to_end_test.result} ${end_to_end_test.absoluteUrl}")
                            }
                        }
                    }
                }
            }
        }
    }
    post {
        always {
            //            input "wait"
            script {
                // Remove GitHub Label from PR
                githubPRRemoveLabels labelProperty: labels('CI tests in progress')
            }
        }
        failure {
            echo "failure"
            // Send mail to github pr author
            script {
                emailext(recipientProviders: [[$class: 'RequesterRecipientProvider']],
                        to: "$GITHUB_PR_AUTHOR_EMAIL",
                        subject: "$JOB_NAME $currentBuild.result",
                        body: "Test $currentBuild.result\n $BUILD_URL");
            }
        }
        // Cleanup after post failure, otherwise groovy scripts will be deleted before
        cleanup {
            deleteDir()
        }
    }
}