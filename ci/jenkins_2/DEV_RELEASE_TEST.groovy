pipeline {
    environment {
        GIT_MAINLINES_FOR_HYBRIS_LATEST_TAG = "master,develop,release,feature/p4.3-v0.0" // stringified groovy list, separated by comma
        DOCKER_REGISTRY_HYBRIS_REPO = "registry.ecomm-internal.wiley.com/hybris"
        HYBRIS_IMAGE_TAG = "" // placeholder for Hybris docker image tag, will be filled at the first stage
        TAG_DATE = sh(returnStdout: true, script: "date +%Y%m%d%H%M").trim()
        GIT_TAG_NAME = "${env.JOB_NAME}_${TAG_DATE}" //  timestamp for unique tag name
    }
    options {
        timeout(time: 8, unit: 'HOURS')
        parallelsAlwaysFailFast()
    }
    agent {
        node {
            label "master"
        }
    }
    stages {
        stage ('Prepare Hybris image tag name') {
            steps {
                script {
                    // Prepare Hybris docker image first. It will be used further for all docker image actions (push, delete, latest tag)
                    // Load groovy script from devops submodule of hybwiley repo located in Jenkins job workspace
                    docker_tag_funcs = load "ci/jenkins_2/lib/docker_tags.groovy"

                    echo "DEBUG: Getting current Hybris image tag..."
                    HYBRIS_IMAGE_TAG = docker_tag_funcs.getImageTag("${env.GIT_BRANCH}", "${env.GIT_COMMIT}")
                    echo "DEBUG: TAG name is $HYBRIS_IMAGE_TAG"
                }
            }
        }
        stage('PR') {
            parallel {
                stage('junit_tests') {
                    steps {
                        script {
                            junit_tests = build(job: 'junit_tests',
                                    propagate: false, parameters: [[$class: 'StringParameterValue',
                                                                    name  : 'jenkins_node', value: "${jenkins_node}"],
                                                                   [$class: 'StringParameterValue',
                                                                    name  : 'GIT_VERSION', value: "${env.GIT_COMMIT}"]])
                            if (junit_tests.result == 'FAILURE') {
                                error("junit_tests")
                            }
                        }
                    }
                }
                stage('end_to_end_test') {
                    steps {
                        script {
                            end_to_end_test = build(job: 'end_to_end_test',
                                    propagate: false, parameters: [[$class: 'StringParameterValue',
                                                                    name  : 'GIT_TAG_NAME', value: "$GIT_TAG_NAME"],
                                                                   [$class: 'StringParameterValue',
                                                                    name  : 'jenkins_node', value: "${jenkins_node}"],
                                                                   [$class: 'StringParameterValue',
                                                                    name  : 'GIT_VERSION', value: "${env.GIT_COMMIT}"],
                                                                   [$class: 'StringParameterValue',
                                                                    name  : 'HYBRIS_IMAGE_TAG', value: "$HYBRIS_IMAGE_TAG"]])
                            if (end_to_end_test.result == 'FAILURE') {
                                error("end_to_end_test")
                            }
                        }
                    }
                }
            }
            post {
                failure {
                    withCredentials([usernamePassword(credentialsId: 'ddf5ab04-63b9-4c76-8635-2079bb7dd841', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
                        // delete image with regular tag via API
                        script {
                            // HEAD http request, get digest
                            DIGEST = sh(script: "curl -k --silent -H \"Accept: application/vnd.docker.distribution.manifest.v2+json\" -I https://registry.ecomm-internal.wiley.com/v2/hybris/manifests/$HYBRIS_IMAGE_TAG | grep -Fi Docker-Content-Digest", returnStdout: true)
                            // Make sure that such tag exists
                            if (DIGEST.trim()) {
                                // Get digest value
                                DIGEST = DIGEST.replace('Docker-Content-Digest: ', '')
                                // DELETE http request, using digest
                                echo "DEBUG: Deleting hybris image with digest $DIGEST"
                                sh(script: "curl -k -X DELETE --user $USER:$PASS https://registry.ecomm-internal.wiley.com/v2/hybris/manifests/$DIGEST")
                            } else {
                                echo "DEBUG: There is no any hybris image with tag $HYBRIS_IMAGE_TAG. Nothing to delete."
                            }
                        }
                    }
                }
            }
        }
        stage ('Push Hybris latest tag to registry') {
            agent {
                label "${jenkins_node}"
            }
            steps {
                checkout([$class                           : 'GitSCM',
                          branches                         : [[name: "${env.GIT_COMMIT}"]],
                          userRemoteConfigs                : [[credentialsId: 'e18299fb-8d19-414a-b9e4-1c6d75178974',
                                                               url          : 'git@github.com:John-Wiley-Sons/hybwiley.git']]])

                script {
                    // Find out do we need to push latest tag for git mainlines
                    // For custom git branches we don't push latest tag to registry
                    LATEST_TAG = docker_tag_funcs.getLatestTag("$GIT_MAINLINES_FOR_HYBRIS_LATEST_TAG", "${env.GIT_BRANCH}")

                    if (LATEST_TAG) {
                        echo "DEBUG: TAG name is $HYBRIS_IMAGE_TAG"
                        echo "DEBUG: Pulling hybris image tag $HYBRIS_IMAGE_TAG"
                        sh(script: "sudo docker pull $DOCKER_REGISTRY_HYBRIS_REPO:$HYBRIS_IMAGE_TAG")

                        echo "DEBUG: Latest TAG name is $LATEST_TAG"
                        echo "DEBUG: Tagging hybris image as latest..."
                        sh(script: "sudo docker tag $DOCKER_REGISTRY_HYBRIS_REPO:$HYBRIS_IMAGE_TAG $DOCKER_REGISTRY_HYBRIS_REPO:$LATEST_TAG")
                        echo "DEBUG: Pushing latest hybris image to Docker registry..."
                        sh(script: "sudo docker push $DOCKER_REGISTRY_HYBRIS_REPO:$LATEST_TAG")
                    } else {
                        echo "DEBUG: Skipping latest tag"
                    }
                }
            }
        }
        stage('Add Git TAG') {
            steps {
                sshagent(['e18299fb-8d19-414a-b9e4-1c6d75178974'])
                        {
                            sh "git config user.email 'jenkins@wiley.com'"
                            sh "git config user.name 'jenkins'"
                            sh "git tag -a  ${GIT_TAG_NAME}  -m 'Jenkins'"
                            sh "git push origin ${GIT_TAG_NAME} "
                        }
            }
        }
    }
    post {
        success {
            script {
                // Send mail to Dev && LEAD team
                emailext(recipientProviders: [[$class: 'RequesterRecipientProvider']],
                        to: "SpecialJWS-HCOMTeamLeads@epam.com specialjws-hcomdevelopers@epam.com",
                        subject: "$JOB_NAME ${currentBuild.currentResult}",
                        body: "Test ${currentBuild.currentResult}\n $BUILD_URL");
            }
        }
        unsuccessful {
            echo "failure"
            // Send mail to github pr author
            script {
                emailext(recipientProviders: [[$class: 'RequesterRecipientProvider']],
                        to: "SpecialJWS-HCOMTeamLeads@epam.com specialjws-hcomdevelopers@epam.com",
                        subject: "$JOB_NAME ${currentBuild.currentResult}",
                        body: "Test ${currentBuild.currentResult}\n $BUILD_URL");
                error("Test failure")
            }
        }
        cleanup {
            deleteDir()
        }
    }
}