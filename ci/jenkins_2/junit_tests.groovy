pipeline {
    environment {
        NAME_ENV = 'prtest'
        WORKDIR = '/srv/hybris_build'
        PLATFORM_HOME = "$WORKDIR/hybris/bin/platform/"
        SCRIPTS = "$WORKDIR/scripts"
        LOCAL_PROPERTIES = "$WORKDIR/hybris/config/local.properties"
        WILEY_OPT_CONFIG_DIR = "$WORKDIR/hybris/config/properties/$NAME_ENV"
        LOCAL_PROPERTIES_10 = "$WILEY_OPT_CONFIG_DIR/10-local.properties"
        LOCAL_PROPERTIES_20 = "$WILEY_OPT_CONFIG_DIR/20-local.properties"
        LOCAL_PROPERTIES_TMP = "/tmp/file"
        MYSQL_DOCKER_NAME = "mysql-prtest-junit-$BUILD_NUMBER"
        TMP_HYBRIS_SERVER = "$WORKDIR/hybris/temp/hybris/hybrisServer/"
        HYBRIS_LOG = "$WORKDIR/hybris/log"
    }
    agent {
        node {
            label "${jenkins_node}"
        }
    }
    stages {
        stage('GIT checkout') {
            steps {
                dir("$WORKDIR/") {
                    checkout([$class                           : 'GitSCM',
                              branches                         : [[name: "${GIT_VERSION}"]],
                              userRemoteConfigs                : [[credentialsId: 'e18299fb-8d19-414a-b9e4-1c6d75178974',
                                                                   url          : 'git@github.com:John-Wiley-Sons/hybwiley.git']]])
                }
            }

        }
        stage('ant clean all') {
            steps {
                ansiColor('xterm') {
                    echo "ant clean"
                    sh '''#!/bin/bash
                    set -x
                    rm -rf $WORKDIR/hybris/log/*
                    rm -rf $LOCAL_PROPERTIES
                    cd $PLATFORM_HOME
                    . ./setantenv.sh
                    ant customize
                    ant clean build
                    echo "should be removed after maven proxy enable"
                    aws s3 cp s3://wiley-ecomm-dev/Docker_Artefacts/mysql-connector-java-5.1.39-bin.jar $PLATFORM_HOME/lib/dbdriver/'''
                }
            }
        }
        stage('codequality') {
            steps {
                echo "codequality"
                sh '''#!/bin/bash
                set -x
                rm -rf $HYBRIS_LOG/*
                cd $PLATFORM_HOME
                . ./setantenv.sh
                cd $WORKDIR/ci/
                ant codequality'''

                dir("$HYBRIS_LOG/") {
                    step([$class          : 'CheckStylePublisher',
                          canComputeNew   : false,
                          defaultEncoding : '',
                          failedTotalAll  : '0',
                          healthy         : '',
                          pattern         : 'checkstyle/checkstyle_*.xml',
                          unHealthy       : '',
                          unstableTotalAll: '0'])
                }
            }
        }
        stage('Create DB') {
            steps {
                echo "Create DB"
                sh returnStdout: true, script: '''#!/bin/bash
                set -e
                set -x
                sudo docker run --rm -d --name $MYSQL_DOCKER_NAME -e MYSQL_USER=pr_test \
                 -e MYSQL_PASSWORD=pr_test -e MYSQL_DATABASE=pr_test -e MYSQL_ALLOW_EMPTY_PASSWORD=tue \
                 mysql:5.6 --innodb-flush-log-at-trx-commit=0'''
            }
        }
        stage('Hybris properties') {
            steps {
                withCredentials([string(credentialsId: '1a3420c2-b2f0-4eee-b4f1-61026536fd9d', variable: 'HYBRIS_SECRET')]) {
                    sh '''MYSQL_PR_TEST_IP="$(sudo docker inspect \
                    -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $MYSQL_DOCKER_NAME)"
                    echo "Decrypt property"
                    sudo docker run --rm \
                    -v $LOCAL_PROPERTIES_10:/tmp/file \
                    registry.ecomm-internal.wiley.com/base-java \
                    openssl enc -d -aes-256-cfb -iter 300000 -nosalt \
                    -k $HYBRIS_SECRET -in /tmp/file >$LOCAL_PROPERTIES_TMP
                    cat $LOCAL_PROPERTIES_TMP > $LOCAL_PROPERTIES_10
                    sudo docker run --rm \
                    -v $LOCAL_PROPERTIES_20:/tmp/file \
                    registry.ecomm-internal.wiley.com/base-java \
                    openssl enc -d -aes-256-cfb -iter 300000 -nosalt \
                    -k $HYBRIS_SECRET -in /tmp/file >$LOCAL_PROPERTIES_TMP
                    cat $LOCAL_PROPERTIES_TMP > $LOCAL_PROPERTIES_20
                    echo "db.password=pr_test" >> $LOCAL_PROPERTIES_20
                    echo "db_schema=pr_test" >> $LOCAL_PROPERTIES_20
                    echo "db.username=pr_test" >> $LOCAL_PROPERTIES_20
                    echo "db.dns=$MYSQL_PR_TEST_IP" >> $LOCAL_PROPERTIES_20
                    echo "Disable S3 media storage"
                    echo "media.default.storage.strategy=localFileMediaStorageStrategy" >> $LOCAL_PROPERTIES_20'''
                }

            }
        }
        stage('ant initialize -Dtenant=junit') {
            steps {
                sh '''#!/bin/bash
                set -x
                cd $PLATFORM_HOME
                . ./setantenv.sh
                ant server && ant server
                cd $WORKDIR/ci/
                export WILEY_OPT_CONFIG_DIR=$WILEY_OPT_CONFIG_DIR
                ant initialize -Dtenant=junit'''
                LogParser()

            }
        }
        stage('alltests') {
            steps {
                sh '''#!/bin/bash
                set -x
                cd $PLATFORM_HOME
                . ./setantenv.sh
                export WILEY_OPT_CONFIG_DIR=$WILEY_OPT_CONFIG_DIR
                ant server && ant server
                ant alltests -Dtestclasses.packages.excluded=com.wiley.ws.test.test.groovy.webservicetests.*
                mv $HYBRIS_LOG/junit $WORKSPACE/'''
                junit '**/junit/**/*.xml'

                script {
                    if (currentBuild.result == "UNSTABLE") {
                        error("Ant alltests")
                    }
                }
            }
        }
        stage('allwebtests') {
            steps {
                sh '''#!/bin/bash
                set -x
                cd $PLATFORM_HOME
                . ./setantenv.sh
                export WILEY_OPT_CONFIG_DIR=$WILEY_OPT_CONFIG_DIR
                ant server && ant server
                ant allwebtests
                echo "Cleanup previous junit log folder"
                rm -rf $WORKSPACE/junit
                mv $HYBRIS_LOG/junit $WORKSPACE/'''

                junit '**/junit/**/*.xml'

                script {
                    if (currentBuild.result == "UNSTABLE") {
                        error("Ant allwebtests")
                    }
                }
            }
        }
    }
    post {

        always {
//            input "wait"
            sh 'sudo docker stop $(sudo docker ps -q)|| true'
        }
        failure {
            echo "failure"
            // Email to github pr author will be sent in parent job
        }
        // Cleanup after post failure, otherwise groovy scripts will be deleted before
        cleanup {
            deleteDir()
        }
    }

}

def LogParser() {
    step([$class          : 'LogParserPublisher',
          failBuildOnError: true,
          projectRulePath : "ci/jenkins_2/lib/parser_error",
          useProjectRule  : true])
}