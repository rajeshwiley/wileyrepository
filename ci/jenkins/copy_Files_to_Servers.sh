cd $HYBRIS_TEMP/hybrisServer/hybris/bin
for folder in *; do
	for server in ${HYBRIS_SERVERS_INT}; do
		ssh hybris@$server "
			rm -rf ${HYBRIS_SERVER_HOME}/hybris/bin/$folder
		"
	done
done
if [ -d $HYBRIS_TEMP/hybrisServer/hybris/config ]; then
	cd $HYBRIS_TEMP/hybrisServer/hybris/config
	for folder in *; do
		for server in ${HYBRIS_SERVERS_INT}; do
			ssh hybris@$server "
				rm -rf ${HYBRIS_SERVER_HOME}/hybris/config/$folder
				if [ ! -d ${HYBRIS_SERVER_HOME}/ci ]; then
					mkdir -p ${HYBRIS_SERVER_HOME}/ci
				fi
				if [ ! -d ${HYBRIS_SERVER_HOME}/scripts ]; then
					mkdir -p ${HYBRIS_SERVER_HOME}/scripts
				fi
				if [ ! -d ${HYBRIS_SERVER_HOME}/hybris/bin ]; then
					mkdir -p ${HYBRIS_SERVER_HOME}/hybris/bin
				fi
			"
		done
	done
fi

for server in ${HYBRIS_SERVERS_INT}; do
	rsync -a --delete $HYBRIS_TEMP/hybrisServer/hybris/bin/ hybris@$server:${HYBRIS_SERVER_HOME}/hybris/bin
	if [ -d $HYBRIS_TEMP/hybrisServer/hybris/config ]; then
		rsync -a --delete $HYBRIS_TEMP/hybrisServer/hybris/config/ hybris@$server:${HYBRIS_SERVER_HOME}/hybris/config
	fi
	rsync -a --delete $HYBRIS_HOME/../scripts/ hybris@$server:${HYBRIS_SERVER_HOME}/scripts
	rsync -a --delete $HYBRIS_HOME/../ci/ hybris@$server:${HYBRIS_SERVER_HOME}/ci
	rsync -a --delete $HYBRIS_HOME/data/media/sys_master/ hybris@$server:${HYBRIS_SERVER_HOME}/hybris/data/media/sys_master/

	ssh hybris@$server "
		if [ $SETUP_CLUSTERED == true ]; then
			sed -i -e 's/cluster.id=2/cluster.id='${CLUSTER_ID[${server}]}'/' ${HYBRIS_SERVER_HOME}/hybris/config/local.properties
      	  sed -i -e 's/cluster.broadcast.method.jgroups.tcp.bind_addr=127.0.0.1/cluster.broadcast.method.jgroups.tcp.bind_addr='${server}'/' ${HYBRIS_SERVER_HOME}/hybris/config/local.properties
		fi
		if [ ! -d ${HYBRIS_SERVER_HOME}/hybris/data/wiley/master/ebp-customer ]; then
			mkdir -p ${HYBRIS_SERVER_HOME}/hybris/data/wiley/master/ebp-customer
		fi
		cd ${HYBRIS_SERVER_HOME}/hybris/bin/platform
		. ./setantenv.sh
		ant deploy
	"
done
rm -rf $HYBRIS_TEMP/hybrisServer/*