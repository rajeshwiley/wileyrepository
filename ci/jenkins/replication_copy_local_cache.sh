DUMP_DIR=/app/dumps/${environment}
source ${DUMP_DIR}/ci/setup_config/${source_environment}/${source_environment}-settings.sh

set -- $HYBRIS_SERVERS_INT
SOURCE_HYBRIS_A=$1


source ${DUMP_DIR}/ci/setup_config/${environment}/${environment}-settings.sh
rsync -a --delete hybris@${SOURCE_HYBRIS_A}:${HYBRIS_SERVER_HOME}/hybris/data/media/sys_master/cache/ ${HYBRIS_HOME}/data/media/sys_master/cache
