cd ${HYBRIS_BIN}/platform
. ./setantenv.sh
sed -i -e 's/clustermode=true/clustermode=false/' ${HYBRIS_HOME}/config/local.properties
ant migrate -Drelease=3.0 -Dscope=SAMPLEDATA
sed -i -e 's/clustermode=false/clustermode=true/' ${HYBRIS_HOME}/config/local.properties
