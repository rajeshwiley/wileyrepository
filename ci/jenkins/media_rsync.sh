for server in $APACHE_SERVERS_INT; do
        if [ $server = 'ip-172-31-54-32.ec2.internal' ]; then
                for fldr in $APP_LIST; do
                ssh -o StrictHostKeyChecking=no media_sync@$server -p 11556 "
                if [ ! -d /app/static/$fldr/_ui ];then
                        mkdir -p /app/static/$fldr/_ui
                fi
                "
                rsync -av  -e "ssh -p 11556" --delete $HYBRIS_BIN/custom/wiley/$fldr/web/webroot/_ui/ media_sync@$server:/app/static/$fldr/_ui
          done
        else
                for fldr in $APP_LIST; do
                ssh -o StrictHostKeyChecking=no media_sync@$server "
                if [ ! -d /app/static/$fldr/_ui ];then
                        mkdir -p /app/static/$fldr/_ui
                fi
                "
                rsync -av --delete $HYBRIS_BIN/custom/wiley/$fldr/web/webroot/_ui/ media_sync@$server:/app/static/$fldr/_ui
          done
        fi
done
