cd ${HYBRIS_BIN}/platform
. ./setantenv.sh
if [ ${INIT_PARAMETER} = initialize ]; then
	if [ -f /app/jenkins/scripts/typesystem/${environment} ]; then
		source /app/jenkins/scripts/typesystem/${environment}
		if [ ! -z "$NEW_TSN" ];then
			ant droptypesystem -DtypeSystemName="$NEW_TSN"
		fi
		if [ ! -z "$OLD_TSN" ];then
			ant droptypesystem -DtypeSystemName="$OLD_TSN"
		fi
		rm -rf /app/jenkins/scripts/typesystem/${environment}
	fi
	ant ${INIT_PARAMETER}
fi

if [ ${INIT_PARAMETER} = updatesystem ]; then
	sed -i -e 's/clustermode=true/clustermode=false/' ${HYBRIS_HOME}/config/local.properties
	if [ -f ${HYBRIS_BIN}/custom/migration/resources/migration/release_${RELEASE_VERSION}/${environment}_update_system.json ]; then
		ant ${INIT_PARAMETER} -DconfigFile=${HYBRIS_BIN}/custom/migration/resources/migration/release_${RELEASE_VERSION}/${environment}_update_system.json
	else
		ant ${INIT_PARAMETER} -DconfigFile=${HYBRIS_BIN}/custom/migration/resources/migration/release_${RELEASE_VERSION}/update_system.json
	fi
	sed -i -e 's/clustermode=false/clustermode=true/' ${HYBRIS_HOME}/config/local.properties
fi
