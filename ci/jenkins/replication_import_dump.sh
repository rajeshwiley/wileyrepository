DUMP_DIR=/app/dumps/${environment}
source ${DUMP_DIR}/ci/setup_config/${environment}/${environment}-settings.sh

#drop database
mysql --host=${DB_HOST} --user=${DB_USER} --password=${DB_PASSWORD} -e "DROP DATABASE ${DB_NAME}"
#create database
mysql --host=${DB_HOST} --user=${DB_USER} --password=${DB_PASSWORD} -e "CREATE DATABASE ${DB_NAME}"
#import data from dump
mysql --host=${DB_HOST} --user=${DB_USER} --password=${DB_PASSWORD} ${DB_NAME} < /app/dumps/${environment}/database_dump.sql

#recover internal user passwords
mysql --host=${DB_HOST} --user=${DB_USER} --password=${DB_PASSWORD} ${DB_NAME} -e "UPDATE users SET passwd='1:loIOXJnMUaEKyuBOnrhuWA==hAaaIQseC2dAA+ro7c9//fTQMWzAeJyVqt2K8oUUQ4nGK+KFYSXjNf5G0fbAlb9u' WHERE p_uid='admin'"
mysql --host=${DB_HOST} --user=${DB_USER} --password=${DB_PASSWORD} ${DB_NAME} -e "UPDATE users SET passwd='1:ucjGFDv9ytwcf9OzS9FQ5g==epY++yLpgy57lmCnovoEAPYTPUH/uBj3KMCL1CTI6LnGK+KFYSXjNf5G0fbAlb9u' WHERE p_uid='cmsmanager'"
mysql --host=${DB_HOST} --user=${DB_USER} --password=${DB_PASSWORD} ${DB_NAME} -e "UPDATE users SET passwd='1:A2jLsdUOhr3Q6u25j71YVw==/loKwfffaNX70ljEtQYRlece+rgeCtcRDD3OMDDVB6rGK+KFYSXjNf5G0fbAlb9u' WHERE p_uid='productmanager'"
mysql --host=${DB_HOST} --user=${DB_USER} --password=${DB_PASSWORD} ${DB_NAME} -e "UPDATE users SET passwd='1:w8AaWiqX1ke9INSgpCylMw==Wj4IVBggqn5aekkwwhxlmOognprackcxiI267wdIob3GK+KFYSXjNf5G0fbAlb9u' WHERE p_uid='csagent'"

#remove nodes in the cluster
mysql --host=${DB_HOST} --user=${DB_USER} --password=${DB_PASSWORD} ${DB_NAME}  -e "DELETE FROM JGROUPSPING"

#remove locking init
mysql --host=${DB_HOST} --user=${DB_USER} --password=${DB_PASSWORD} ${DB_NAME} -e "UPDATE props SET VALUESTRING1=0 WHERE NAME='system.locked'"

#correct solr server names in DB
set -- $SOLR_SERVERS_INT
mysql --host=${DB_HOST} --user=${DB_USER} --password=${DB_PASSWORD} ${DB_NAME} -e "UPDATE solrendpointurl SET p_url='http://${1}:8983/solr' where p_url='http://ip-172-31-44-53.ec2.internal:8983/solr'"
if [ -z "$2" ]; then
	mysql --host=${DB_HOST} --user=${DB_USER} --password=${DB_PASSWORD} ${DB_NAME}  -e "UPDATE solrendpointurl SET p_url='http://${1}:3983/solr' where p_url='http://ip-172-31-39-140.ec2.internal:3983/solr'"
else
	mysql --host=${DB_HOST} --user=${DB_USER} --password=${DB_PASSWORD} ${DB_NAME}  -e "UPDATE solrendpointurl SET p_url='http://${2}:3983/solr' where p_url='http://ip-172-31-39-140.ec2.internal:3983/solr'"
fi
	
#remove file with type system names
rm -rf /app/jenkins/scripts/typesystem/${environment}

#add type system file if exists
if [ -f /app/jenkins/scripts/typesystem/${source_environment} ]; then
	cp /app/jenkins/scripts/typesystem/${source_environment} /app/jenkins/scripts/typesystem/${environment}
fi

if [ ! -z ${REMOVE_EXT_USERS} ] && [ ${REMOVE_EXT_USERS} == "true" ]; then
	mysql --host=${DB_HOST} --user=${DB_USER} --password=${DB_PASSWORD} ${DB_NAME} -e "DELETE FROM usr USING users as usr JOIN pgrels pgr ON pgr.sourcePK=usr.pk JOIN usergroups ugs ON pgr.targetPK = ugs.pk WHERE ugs.p_uid NOT IN ('admingroup','cockpitgroup','employeegroup','csagentmanagergroup','cmsmanagergroup','customermanagergroup','productmanagergroup','customerservicegroup','csagentgroup','asagentgroup','asagentsalesgroup','asagentsalesmanagergroup','promomanagergroup','vjdbcReportsGroup','webservicegroup','analyticsperspectivegroup') AND usr.p_uid <> 'anonymous';"
fi

if [ ! -z ${REMOVE_ORDERS} ] && [ ${REMOVE_ORDERS} == "true" ]; then
	mysql --host=${DB_HOST} --user=${DB_USER} --password=${DB_PASSWORD} ${DB_NAME} -e "DELETE FROM orders"
	mysql --host=${DB_HOST} --user=${DB_USER} --password=${DB_PASSWORD} ${DB_NAME} -e "DELETE FROM orderentries"
	mysql --host=${DB_HOST} --user=${DB_USER} --password=${DB_PASSWORD} ${DB_NAME} -e "DELETE FROM orderdiscrels"
	mysql --host=${DB_HOST} --user=${DB_USER} --password=${DB_PASSWORD} ${DB_NAME} -e "DELETE FROM ordercancelconfigs"
fi

##remove pins tables
if [ ! -z ${REMOVE_PINS} ] && [ ${REMOVE_PINS} == "true" ]; then
	mysql --host=${DB_HOST} --user=${DB_USER} --password=${DB_PASSWORD} ${DB_NAME} -e "DELETE FROM pins"
fi

##remove voucherinvalidations tables
if [ ! -z ${REMOVE_VOUCHERS} ] && [ ${REMOVE_VOUCHERS} == "true" ]; then
	mysql --host=${DB_HOST} --user=${DB_USER} --password=${DB_PASSWORD} ${DB_NAME} -e "DELETE FROM voucherinvalidations"
	mysql --host=${DB_HOST} --user=${DB_USER} --password=${DB_PASSWORD} ${DB_NAME} -e "DELETE FROM discountslp"
	mysql --host=${DB_HOST} --user=${DB_USER} --password=${DB_PASSWORD} ${DB_NAME} -e "DELETE FROM dr USING discountrows as dr JOIN discounts ds ON ds.PK=dr.p_discount WHERE ds.p_code NOT IN ('20_percent', '10_percent_partner', '5_percent_partner')"
	mysql --host=${DB_HOST} --user=${DB_USER} --password=${DB_PASSWORD} ${DB_NAME} -e "DELETE FROM discounts WHERE p_code not in ('20_percent', '10_percent_partner', '5_percent_partner')"
fi
