DUMP_DIR=/app/dumps/${environment}

rm -rf ${DUMP_DIR}/*

if [ ! -d ${DUMP_DIR} ]; then
	mkdir -p ${DUMP_DIR}
fi

cp -r ${WORKSPACE}/* ${DUMP_DIR}/