DUMP_DIR=/app/dumps/${environment}

cat "${WORKSPACE}"/ci/setup_config/${source_environment}/${source_environment}-settings_secured.sh >> ${DUMP_DIR}/ci/setup_config/${source_environment}/${source_environment}-settings.sh

if [ -f "${WORKSPACE}"/ci/setup_config/${environment}/${environment}-settings_secured.sh ]; then
	cat "${WORKSPACE}"/ci/setup_config/${environment}/${environment}-settings_secured.sh >> ${DUMP_DIR}/ci/setup_config/${environment}/${environment}-settings.sh
fi