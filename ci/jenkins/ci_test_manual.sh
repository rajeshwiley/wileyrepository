rm -rf ${WORKSPACE}/hybris/log
mkdir -p ${WORKSPACE}/hybris/log
cd ${HYBRIS_BIN}/platform
. ./setantenv.sh
cd ${HYBRIS_HOME}/../ci
ant manualtests -Dtestclasses.packages=com.wiley.ws.test.test.groovy.webservicetests.v3.spock.* -Dtarget.hybris.host=${targetHost} -Dtarget.hybris.port=${targetPort} -Dtarget.hybris.secure.port=${targetSecurePort}
mv ${HYBRIS_HOME}/temp/hybris/unittests ${WORKSPACE}/hybris/log