if [ $TO_RECONFIG_LOCAL_APACHE = true ]; then
	conf_folder=/etc/httpd/conf
	file_name=wiley.conf
	cert_file=/app/jenkins/scripts/certs/dlevine.pem
	for server in ${APACHE_LOCAL_SERVERS_INT}; do
		scp -i ${cert_file} ${WORKSPACE}/ci/apache_config/${file_name} ec2-user@${server}:/tmp
		scp -r -i ${cert_file} ${WORKSPACE}/ci/apache_pages ec2-user@${server}:/tmp
		ssh -t -i ${cert_file} ec2-user@${server} "
			sudo mkdir -p /app/apache_pages
			sudo rsync -a /tmp/apache_pages/ /app/apache_pages
			sudo rm -rf /tmp/apache_pages
			sudo sed -i -e 's/HYBRIS_SERVER_1/'${HYBRIS_SERVER_A}'/' /tmp/${file_name}
			sudo sed -i -e 's/HYBRIS_SERVER_2/'${HYBRIS_SERVER_B}'/' /tmp/${file_name}
			sudo sed -i -e 's/LOCAL_ENTRY_POINT/'${LOCAL_ENTRY_POINT}'/' /tmp/${file_name}
			sudo sed -i -e 's/BO_ENTRY_POINT/'${BO_ENTRY_POINT}'/' /tmp/${file_name}
			sudo mv ${conf_folder}/${file_name} ${conf_folder}/${file_name}_backup
			sudo mv /tmp/${file_name} ${conf_folder}
			sudo /etc/init.d/httpd reload
		"
	done
fi	

if [ $TO_RECONFIG_ROUTING = true ]; then
	conf_folder=/app
	file_name=ags_hybris_router.conf
	cert_file=/app/jenkins/scripts/certs/graphic-standards.pem
	for server in ${AGS_ROUTING_SERVERS_INT}; do
		scp -i ${cert_file} -o StrictHostKeyChecking=no ${WORKSPACE}/ci/routing_config/${file_name} ec2-user@${server}:/tmp
		ssh -t -i ${cert_file} ec2-user@${server} "
			sed -i -e 's/HYBRIS_SERVER_1/'${HYBRIS_SERVER_A}'/' /tmp/${file_name}
			sed -i -e 's/HYBRIS_SERVER_2/'${HYBRIS_SERVER_B}'/' /tmp/${file_name}
			sed -i -e 's/AGS_ENTRY_POINT/'${AGS_ENTRY_POINT}'/' /tmp/${file_name}
			mv ${conf_folder}/${file_name} ${conf_folder}/${file_name}_backup
			mv /tmp/${file_name} ${conf_folder}
			sudo sed -i -e 's/\/app\/hybris_router.conf/\/app\/ags_hybris_router.conf/' /etc/httpd/conf/httpd.conf
			sudo /etc/init.d/httpd reload
		"
	done
fi	

if [ $TO_RECONFIG_ROUTING_WEL = true ]; then
	conf_folder=/app
	file_name=wel_hybris_router.conf
	cert_file=/app/jenkins/scripts/certs/wel-admin.pem
	for server in ${WEL_ROUTING_SERVERS_INT}; do
		scp -i ${cert_file} -o StrictHostKeyChecking=no ${WORKSPACE}/ci/routing_config/${file_name} ec2-user@${server}:/tmp
		ssh -t -i ${cert_file} ec2-user@${server} "
			sed -i -e 's/HYBRIS_SERVER_1/'${HYBRIS_SERVER_A}'/' /tmp/${file_name}
			sed -i -e 's/HYBRIS_SERVER_2/'${HYBRIS_SERVER_B}'/' /tmp/${file_name}
			sed -i -e 's/WEL_ENTRY_POINT/'${WEL_ENTRY_POINT}'/' /tmp/${file_name}
			mv ${conf_folder}/${file_name} ${conf_folder}/${file_name}_backup
			mv /tmp/${file_name} ${conf_folder}
			sudo /etc/init.d/httpd reload
		"
	done
fi	

if [ $TO_RECONFIG_ROUTING_B2B_B2C = true ]; then
	conf_folder=/app
	file_name=b2b_b2c_hybris_router.conf
	cert_file=/app/jenkins/scripts/certs/dlevine.pem
	for server in ${B2B_B2C_ROUTING_SERVERS_INT}; do
		scp -i ${cert_file} -o StrictHostKeyChecking=no ${WORKSPACE}/ci/routing_config/${file_name} ec2-user@${server}:/tmp
		scp -r -i ${cert_file} ${WORKSPACE}/ci/apache_pages ec2-user@${server}:/tmp
		ssh -t -i ${cert_file} -o StrictHostKeyChecking=no ec2-user@${server} "
			sudo mkdir -p /app/apache_pages
			sudo rsync -a /tmp/apache_pages/ /app/apache_pages
			sudo rm -rf /tmp/apache_pages
			sudo sed -i -e 's/HYBRIS_SERVER_1/'${HYBRIS_SERVER_A}'/g' /tmp/${file_name}
			sudo sed -i -e 's/HYBRIS_SERVER_2/'${HYBRIS_SERVER_B}'/g' /tmp/${file_name}
			sudo sed -i -e 's/B2C_ENTRY_POINT/'${B2C_ENTRY_POINT}'/g' /tmp/${file_name}
			sudo sed -i -e 's/B2B_ENTRY_POINT/'${B2B_ENTRY_POINT}'/g' /tmp/${file_name}
			sudo mv ${conf_folder}/${file_name} ${conf_folder}/${file_name}_backup
			sudo mv /tmp/${file_name} ${conf_folder}
			sudo /etc/init.d/httpd reload
		"
	done
fi	