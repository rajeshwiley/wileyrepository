cd ${HYBRIS_BIN}/platform
. ./setantenv.sh

#sed -i -e "/db.type.system.name/d" ${HYBRIS_HOME}/config/local.properties

export NEW_TSN=""
export OLD_TSN=""
export TO_DELETE_TSN=""

function checkTSN {
	if [ -z "$OLD_TSN" ] && [ -z "$NEW_TSN" ]; then
		if [ -z "$DAILY_REBUILDING" ] || [ "$DAILY_REBUILDING" == "false" ]; then
			NEW_TSN=${RELEASE_VERSION}
		else
			NEW_TSN=${environment}_1
		fi
		createTSN "$NEW_TSN"
 		return 0
	fi

	if [ -z "$OLD_TSN" ] && [ ! -z "$NEW_TSN" ];then
		OLD_TSN="$NEW_TSN"
		if [ -z "$DAILY_REBUILDING" ] || [ "$DAILY_REBUILDING" == "false" ]; then
			NEW_TSN=${RELEASE_VERSION}
		else
			generateTSN
		fi
		createTSN "$NEW_TSN"
        return 0
	fi

	if [ ! -z "$OLD_TSN" ] && [ ! -z "$NEW_TSN" ];then
		TO_DELETE_TSN="$OLD_TSN"
		OLD_TSN="$NEW_TSN"
		if [ -z "$DAILY_REBUILDING" ] || [ "$DAILY_REBUILDING" == "false" ]; then
			NEW_TSN=${RELEASE_VERSION}
		else
			generateTSN
		fi
		createTSN "$NEW_TSN"
        return 0
	fi
}


function createTSN {
	echo "Creating Type System $1"
	ant clean all createtypesystem -DtypeSystemName="$1"
}

function dropTSN {
	echo "Type System $1 will be dropped"
	ant droptypesystem -DtypeSystemName="$1"
}

function generateTSN {
	if [[ ! $NEW_TSN = *${environment}* ]]; then
		NEW_TSN=${environment}_1
	else
		OIFS=$IFS
		IFS="_"
		arrTemp=($NEW_TSN)
		
		NEW_TSN=${environment}_$(( ${arrTemp[-1]}+1 ))
	fi
}

function exportToFile {
	if [ -z "$NEW_TSN" ]; then
		echo "" > /app/jenkins/scripts/typesystem/${environment}
	fi
	if [ ! -z "$NEW_TSN" ]; then
		echo "export NEW_TSN=$NEW_TSN" > /app/jenkins/scripts/typesystem/${environment}
	fi
	if [ ! -z "$OLD_TSN" ]; then
		echo "export OLD_TSN=$OLD_TSN" >> /app/jenkins/scripts/typesystem/${environment}
	fi
	if [ ! -z "$TO_DELETE_TSN" ]; then
		echo "export TO_DELETE_TSN=$TO_DELETE_TSN" >> /app/jenkins/scripts/typesystem/${environment}
	fi
}

function rollback {
	if [ -z "$OLD_TSN" ] && [ ! -z "$NEW_TSN" ] && [ -z "$TO_DELETE_TSN" ]; then
		dropTSN "$NEW_TSN"
		unset NEW_TSN
	fi

	if [ ! -z "$OLD_TSN" ] && [ ! -z "$NEW_TSN" ] && [ -z "$TO_DELETE_TSN" ];then
		dropTSN "$NEW_TSN"
		NEW_TSN="$OLD_TSN"
		unset OLD_TSN
		unset TO_DELETE_TSN
	fi

	if [ ! -z "$OLD_TSN" ] && [ ! -z "$NEW_TSN" ] && [ ! -z "$TO_DELETE_TSN" ];then
		dropTSN "$NEW_TSN"
		NEW_TSN="$OLD_TSN"
		OLD_TSN="$TO_DELETE_TSN"
		unset TO_DELETE_TSN
	fi
	
	exportToFile
}

function remove {
	if [ ! -z "$TO_DELETE_TSN" ];then
		dropTSN "$TO_DELETE_TSN"
		unset TO_DELETE_TSN
	fi
	exportToFile
}

if [ ! -f /app/jenkins/scripts/typesystem/${environment} ]; then
	touch /app/jenkins/scripts/typesystem/${environment}
fi
source /app/jenkins/scripts/typesystem/${environment}


if [ -z $TSN_MODE ]; then
	checkTSN
	exportToFile
	else
	case "$TSN_MODE" in
		rollback)
			rollback
			;;
		remove)
			remove
			;;
	esac
fi
