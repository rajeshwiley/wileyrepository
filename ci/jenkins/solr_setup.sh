for server in $SOLR_SERVERS_INT; do
	for slrtype in ${SOLR_CLUSTER[${server}]}; do
		ssh solr@$server "
			if [ $SOLR_MODE = master_slave ]; then
				if [ ! -d /app/solr/${slrtype} ]; then
					mkdir -p /app/solr/${slrtype}
				fi
			fi
		"
		if [ $SOLR_MODE = master_slave ]; then
			rsync -av --delete ${HYBRIS_BIN}/ext-commerce/solrserver/resources/solr/ solr@$server:/app/solr/${slrtype}
			rsync -av ${HYBRIS_HOME}/../ci/setup_config/${environment}/solr/${SOLR_MODE}/${slrtype}/ solr@$server:/app/solr/${slrtype}

			ssh solr@$server "
				cd /app/solr/${slrtype}
				chmod -R +x *
				./stop-with-zookeeper-bg.sh
				./start-with-zookeeper-bg.sh
			"
		fi
	done
done