cd ${HYBRIS_BIN}/platform
. ./setantenv.sh
rm -rf $HYBRIS_TEMP/hybrisServer/*
ant production -Dproduction.legacy.mode=false
cd $HYBRIS_TEMP/hybrisServer
unzip -q hybrisServer-AllExtensions.zip
unzip -q hybrisServer-Platform.zip
if [ $COPY_CONFIG = true ];then
	unzip -q hybrisServer-Config.zip
fi
if [ $COPY_LICENSE = true ];then
	unzip -q hybrisServer-Licence.zip
fi
cp ${HYBRIS_HOME}/../ci/jenkins/hybris $HYBRIS_TEMP/hybrisServer/hybris/bin/platform
chmod +x $HYBRIS_TEMP/hybrisServer/hybris/bin/platform/hybris