cd ${HYBRIS_BIN}/platform
. ./setantenv.sh
ant ${PREPARE_PARAMETER} generate_git_info -Dconfig=${environment}
if [ -e $HYBRIS_HOME/../configs/${environment}/keys/${environment}.pem ]; then
	cp -rf $HYBRIS_HOME/../configs/${environment}/keys $HYBRIS_HOME/config
fi
