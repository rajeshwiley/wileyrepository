for server in ${HYBRIS_SERVERS_INT}; do
	rsync -a ${HYBRIS_HOME}/../ci/jenkins/hybris hybris@$server:${HYBRIS_SERVER_HOME}/hybris/bin/
	ssh hybris@${server} "
		cd ${HYBRIS_SERVER_HOME}/hybris/bin/platform
		. ./setantenv.sh
		if [ -f ./hybris ]; then
			chmod +x ./hybris
			./hybris $1
		fi
	"
done
