for server in ${HYBRIS_SERVERS_INT}; do
	rsync -a ${HYBRIS_HOME}/../ci/jenkins/hybris hybris@$server:${HYBRIS_SERVER_HOME}/hybris/bin/
	#Stop Application
	ssh hybris@${server} "
        cd ${HYBRIS_SERVER_HOME}/hybris/bin/platform
        . ./setantenv.sh
        if [ -f ./hybris ]; then
			chmod +x ./hybris
			./hybris stop
        fi
    "


    # Copy new files to server and start hybris
    rsync -a --delete $HYBRIS_TEMP/hybrisServer/hybris/bin/ hybris@$server:${HYBRIS_SERVER_HOME}/hybris/bin
    if [ -d $HYBRIS_TEMP/hybrisServer/hybris/config ]; then
    	rsync -a --delete $HYBRIS_TEMP/hybrisServer/hybris/config/ hybris@$server:${HYBRIS_SERVER_HOME}/hybris/config
    fi
    rsync -a --delete $HYBRIS_HOME/../scripts/ hybris@$server:${HYBRIS_SERVER_HOME}/scripts
    rsync -a --delete $HYBRIS_HOME/../ci/ hybris@$server:${HYBRIS_SERVER_HOME}/ci
	rsync -a --delete $HYBRIS_HOME/data/media/sys_master/ hybris@$server:${HYBRIS_SERVER_HOME}/hybris/data/media/sys_master/
    
    ssh hybris@$server "
        if [ $SETUP_CLUSTERED == true ]; then
            sed -i -e 's/cluster.id=2/cluster.id='${CLUSTER_ID[${server}]}'/' ${HYBRIS_SERVER_HOME}/hybris/config/local.properties
            sed -i -e 's/cluster.broadcast.method.jgroups.tcp.bind_addr=127.0.0.1/cluster.broadcast.method.jgroups.tcp.bind_addr='${server}'/' ${HYBRIS_SERVER_HOME}/hybris/config/local.properties
        fi
        cd ${HYBRIS_SERVER_HOME}/hybris/bin/platform
        . ./setantenv.sh
        ant deploy
		./hybris start
    "
    
    
    #Wait until application will be balanced
    sleep 120


done
rm -rf $HYBRIS_TEMP/hybrisServer/*