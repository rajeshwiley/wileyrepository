scp -r "${WORKSPACE}"/ci/jenkins/hybris hybris@${INSTANCE_NAME}:${HYBRIS_SERVER_HOME}/hybris/bin/platform

if [ $DEBUG_STATUS == "true" ];then
	echo "$INSTANCE_NAME is restarting in debug mode"
	ssh hybris@${INSTANCE_NAME} "
		cd ${HYBRIS_SERVER_HOME}/hybris/bin/platform
		. ./setantenv.sh
		if [ -f ./hybris ]; then
			./hybris restart debug
		fi
	"
fi

if [ $DEBUG_STATUS == "false" ];then
	echo "$INSTANCE_NAME is restarting in normal mode"
	ssh hybris@${INSTANCE_NAME} "
		cd ${HYBRIS_SERVER_HOME}/hybris/bin/platform
		. ./setantenv.sh
		if [ -f ./hybris ]; then
			./hybris restart
		fi
	"
fi