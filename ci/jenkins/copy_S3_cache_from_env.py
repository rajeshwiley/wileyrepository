'''
Created on Mar 25, 2016

@author: Viachaslau_Kabak
'''

from boto3.session import Session
import os
from threading import Thread

sourceEnv=os.getenv("source_environment")
destEnv=os.getenv("environment")

sourceBucket="wiley-ecomm-{0}-media-storage".format(sourceEnv)
destBucket="wiley-ecomm-{0}-media-storage".format(destEnv)

s3_access_key="AKIAIUWULZNX6OV5CH6Q"
s3_secret_access_key="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"
folder="sys-master"

def initSessionResource(AWStype="s3"):
    session = Session(aws_access_key_id=s3_access_key, aws_secret_access_key=s3_secret_access_key, region_name='us-east-1')
    return session.resource(AWStype)


def s3RemoveFolder():
    successCount=0
    failedCount=0
    r=initSessionResource()
    threadList=[]
    for i in r.Bucket(destBucket).objects.filter(Prefix="{0}/".format(folder)):
        try:
            def remS3Item():
                r.Object(destBucket, i.key).delete()
            worker = Thread(target=remS3Item, args=())
            worker.start()
            threadList.append(worker)
            successCount+=1
        except:
            print("Failed to remove key {0}".format(i.key))
            failedCount+=1
    for thr in threadList:
        thr.join()
    print("Succeeded: {0}; Failed: {1}".format(successCount, failedCount))

def s3CopyFolder():
    successCount=0
    failedCount=0
    threadList=[]
    r=initSessionResource()
    for i in r.Bucket(sourceBucket).objects.filter(Prefix="{0}/".format(folder)):
        try:
            def cpS3Item():
                r.Object(destBucket, i.key).copy_from(CopySource="{0}/{1}".format(sourceBucket, i.key))
            worker = Thread(target=cpS3Item, args=())
            worker.start()
            threadList.append(worker)
            successCount+=1
        except:
            print("Failed to copy key {0}".format(i.key))
            failedCount+=1
    for thr in threadList:
        thr.join()
    print("Succeeded: {0}; Failed: {1}".format(successCount, failedCount))
    
    
    
print("Removing {0} folder from {1} bucket".format(folder, destBucket))
s3RemoveFolder()
print("Copying {0} folder from {1} bucket to {2}".format(folder, sourceBucket, destBucket))
s3CopyFolder()