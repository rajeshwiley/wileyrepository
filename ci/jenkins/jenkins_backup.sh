cd /app
backup_file=jenkins_backup_`date +%Y%m%d_%H%M`.tar.gz
#tar czf /app/backup/${backup_file} --exclude='*.log' --exclude='jenkins/jobs/*/builds/*' --exclude='jenkins/jobs/*/workspace/*' jenkins
tar czf /app/backup/${backup_file} --exclude='*.log' --exclude='jenkins/jobs/*/workspace/*' jenkins
cd /app/backup
for ff in *.tar.gz
do
	find $ff -mtime +7 -exec rm {} \;
done
python2.6 /usr/bin/s3cmd sync --skip-existing --delete-removed /app/backup/ s3://wiley-ecomm-dev/Jenkins_backup/