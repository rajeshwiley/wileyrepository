if [ ! -d $HYBRIS_HOME/../configs ]; then
	mkdir -p $HYBRIS_HOME/../configs
fi
if [ ! -d $HYBRIS_HOME/../scripts ]; then
	mkdir -p $HYBRIS_HOME/../scripts
fi
if [ ! -d $HYBRIS_HOME/../ci ]; then
	mkdir -p $HYBRIS_HOME/../ci
fi
if [ ! -d $HYBRIS_HOME/../.git ]; then
	mkdir -p $HYBRIS_HOME/../.git
fi
if [ ! -d $HYBRIS_BIN/custom ]; then
	mkdir -p $HYBRIS_BIN/custom
fi
rsync -av --delete "${WORKSPACE}"/configs/ $HYBRIS_HOME/../configs/
rsync -av --delete "${WORKSPACE}"/hybris/bin/custom/ $HYBRIS_BIN/custom
rsync -av --delete "${WORKSPACE}"/scripts/ $HYBRIS_HOME/../scripts/
rsync -av --delete "${WORKSPACE}"/ci/ $HYBRIS_HOME/../ci/
rsync -av --delete "${WORKSPACE}"/.git/ $HYBRIS_HOME/../.git/

cp -r "${WORKSPACE}"/ci/setup_config/${environment} /app/setup_configs