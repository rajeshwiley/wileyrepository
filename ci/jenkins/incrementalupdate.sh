#!/bin/bash

###############################################################################################
### Turn off wiremock if it is on
if grep -q "wiremock.start.in.embedded.mode=true" ${HYBRIS_HOME}/config/local.properties; then
	sed -i -e 's/wiremock.start.in.embedded.mode=true/wiremock.start.in.embedded.mode=false/' ${HYBRIS_HOME}/config/local.properties
	export wasChanged=true
fi
###############################################################################################
rm -rf $HYBRIS_HOME/data/media/sys_master/cache/*
cd ${HYBRIS_BIN}/platform
. ./setantenv.sh

if [ ${INIT_PARAMETER} = updatesystem ]; then
	sed -i -e 's/clustermode=true/clustermode=false/' ${HYBRIS_HOME}/config/local.properties

    ##############################################################################################
	#### set Hybris RUNTIME_PROPERTIES
	###Disable task.engine.loadonstartup
	##https://jira.wiley.ru/browse/ECSC-15671
	export HYBRIS_RUNTIME_PROPERTIES="${HYBRIS_HOME}../configs/runtime/env.properties"
	###############################################################################################

    ant incrementalupdate
    sed -i -e 's/clustermode=false/clustermode=true/' ${HYBRIS_HOME}/config/local.properties

elif [ ${INIT_PARAMETER} = initialize ]; then
	if [ -f /app/jenkins/scripts/typesystem/${environment} ]; then
		source /app/jenkins/scripts/typesystem/${environment}
		if [ ! -z "$NEW_TSN" ];then
			ant droptypesystem -DtypeSystemName="$NEW_TSN"
		fi
		if [ ! -z "$OLD_TSN" ];then
			ant droptypesystem -DtypeSystemName="$OLD_TSN"
		fi
		rm -rf /app/jenkins/scripts/typesystem/${environment}
	fi
	ant ${INIT_PARAMETER}
fi


###############################################################################################
### Turn on wiremock if it was turned off by script
if [ ! -z "$wasChanged" ]; then
	sed -i -e 's/wiremock.start.in.embedded.mode=false/wiremock.start.in.embedded.mode=true/' ${HYBRIS_HOME}/config/local.properties
fi
###############################################################################################