#!/bin/sh
DUMP_DIR=/app/dumps/${environment}
source ${DUMP_DIR}/ci/setup_config/${source_environment}/${source_environment}-settings.sh

if [ ! -z "$DB_HOST_REPLICA" ]; then
	mysqldump --skip-comments --skip-add-drop-table --complete-insert --compress --verbose --extended-insert --host=${DB_HOST_REPLICA} --result-file=${DUMP_DIR}/database_dump.sql --user=${DB_USER} --password=${DB_PASSWORD} ${DB_NAME}
else
	mysqldump --skip-comments --skip-add-drop-table --complete-insert --compress --verbose --extended-insert --host=${DB_HOST} --result-file=${DUMP_DIR}/database_dump.sql --user=${DB_USER} --password=${DB_PASSWORD} ${DB_NAME}
fi

#substitute links in DB
sed -i -e "s/www.graphicstandards.com/${environment}.graphicstandards.com/g" ${DUMP_DIR}/database_dump.sql
sed -i -e "s/www.efficientlearning.com/${environment}.efficientlearning.com/g" ${DUMP_DIR}/database_dump.sql
sed -i -e "s/www.hybris.wiley.com/${environment}.hybris.wiley.com/g" ${DUMP_DIR}/database_dump.sql

sed -i -e "s/${source_environment}.graphicstandards.com/${environment}.graphicstandards.com/g" ${DUMP_DIR}/database_dump.sql
sed -i -e "s/${source_environment}.efficientlearning.com/${environment}.efficientlearning.com/g" ${DUMP_DIR}/database_dump.sql
sed -i -e "s/${source_environment}.hybris.wiley.com/${environment}.hybris.wiley.com/g" ${DUMP_DIR}/database_dump.sql

C_TAG=`curl ${BO_ENTRY_POINT}/agsstorefront/health`
SOURCE_TAG=`echo ${C_TAG} | awk '{print $17}'|sed 's/^"\(.*\)-dirty",$/\1/'`

echo "SOURCE_TAG=${SOURCE_TAG}" > ${DUMP_DIR}/${source_environment}_tag.sh