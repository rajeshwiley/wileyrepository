#!/bin/bash
set -e
WILEY_OPT_CONFIG_DIR="/srv/hybris/config/properties/${NAME_ENV}"
WILEY_OPT_PLATFORM_DIR="/srv/hybris/bin/platform/"
if [[ -z "${NAME_ENV}" ]]; then
  echo "You must add environment variable NAME_ENV"
  exit 1
fi
if [[ -z "${HYBRIS_SECRET}" ]]; then
  echo "You must add environment variable HYBRIS_SECRET"
  exit 1
fi
if [[ -z "${HYBRIS_SECRET_20}" ]]; then
  HYBRIS_SECRET_20=${HYBRIS_SECRET}
fi

#Add custom properties
if [[ -v CLUSTER_NODE_GROUPS ]]; then
  echo "cluster.node.groups=${CLUSTER_NODE_GROUPS}" >> ${WILEY_OPT_CONFIG_DIR}/99-local.properties
fi
if [[ -v CLUSTER_NAME ]]; then
    echo "cluster.broadcast.method.jgroups.channel.name=${CLUSTER_NAME}" >> ${WILEY_OPT_CONFIG_DIR}/99-local.properties
fi
if [[ -v CLUSTER_NODE_IP ]]; then
  echo "aws.host.ip=${CLUSTER_NODE_IP}" >> ${WILEY_OPT_CONFIG_DIR}/99-local.properties
fi
if [[ -v DB_NAME ]] && [[ "${DB_NAME}" != '' ]]; then
  echo "db_schema=${DB_NAME}" >> ${WILEY_OPT_CONFIG_DIR}/99-local.properties
#  We have the same media bucket pattern name like database
  echo "media.globalSettings.s3MediaStorageStrategy.bucketId=wiley-ecomm-temp-media-storage-${DB_NAME}" >> ${WILEY_OPT_CONFIG_DIR}/99-local.properties
fi
if [[ -v JAVA_MEM ]]; then
  echo "java.mem=${JAVA_MEM}" >> ${WILEY_OPT_CONFIG_DIR}/99-local.properties
fi
# Decrypt properties
openssl enc -d -aes-256-cfb -iter 300000 -nosalt -k ${HYBRIS_SECRET} \
-in ${WILEY_OPT_CONFIG_DIR}/10-local.properties \
-out ${WILEY_OPT_CONFIG_DIR}/10-local.properties

openssl enc -d -aes-256-cfb -iter 300000 -nosalt -k ${HYBRIS_SECRET_20} \
-in ${WILEY_OPT_CONFIG_DIR}/20-local.properties \
-out ${WILEY_OPT_CONFIG_DIR}/20-local.properties

# Decrypt Hybris licence key for PROD env only
if [[ ${NAME_ENV} = "prod" ]]; then
    openssl enc -d -aes-256-cfb -iter 300000 -nosalt -k ${HYBRIS_SECRET_20} \
    -in /srv/hybris/config/properties/licence/installedSaplicenses.properties \
    -out /srv/hybris/config/licence/installedSaplicenses.properties
fi

export WILEY_OPT_CONFIG_DIR=${WILEY_OPT_CONFIG_DIR}

#Run ant server for apply config dir
cd ${WILEY_OPT_PLATFORM_DIR}
#TODO hotfix temporary twice run ant server
${WILEY_OPT_PLATFORM_DIR}apache-ant/bin/ant server && \
${WILEY_OPT_PLATFORM_DIR}apache-ant/bin/ant server

exec "$@"
