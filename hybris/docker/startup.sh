#!/bin/bash

pidfile=/var/run/hybris.pid

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

function send_stop_signal_to_hybris_wrapper {
               hybris_wrapper_pid=$(ps aux|grep 'wrapper-linux-x86-64'|grep -v grep|awk '{print $2}')
               kill -15 $hybris_wrapper_pid
               while kill -0 $hybris_wrapper_pid 2> /dev/null; do
                  sleep 15
              done
           }
#catch SIGTERM and stop Hybris
trap send_stop_signal_to_hybris_wrapper SIGINT SIGTERM

#Start Hybris
./hybrisserver.sh &
PID=$!
wait $PID  

exit 0