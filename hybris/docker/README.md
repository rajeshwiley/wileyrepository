#### **How to build Hybris docker container:**

```bash
cd hybris/bin/platform/
. ./setantenv.sh
ant customize
ant clean build
ant production
ant dockerfiles
cd hybris/temp/hybrisDockerfiles
docker build -t hybris .
```

### **How to run Hybris docker container:**

```bash
docker run --rm --link $MYSQL_DOCKER_NAME --name $HYBRIS_DOCKER_NAME \
                    -e NAME_ENV=$NAME_ENV \
                    -e HYBRIS_SECRET=$HYBRIS_SECRET \
                     hybris-$BUILD_NUMBER
```

#### **Required properties:**

*$NAME_ENV* -- Environment name. (local,dev....uat)

 *$HYBRIS_SECRET* -- Password for encrypt properties files

------

#### **Optional properties:**

CLUSTER_NODE_GROUPS -- Hybris property *cluster.node.groups*. 

​			Options: *backoffice, frontend, standalone* 

CLUSTER_NAME -- Hybris property *cluster.broadcast.method.jgroups.channel.name*. For every environment deploy we 			  

​			use unique value.

CLUSTER_NODE_IP -- Hybris property *cluster.broadcast.method.jgroups.tcp.bind_addr*.

JAVA_MEM -- Hybris property *java.mem*. Should be reduced for non-PROD environments.

#### **You can set throw mounting configuration file:**

```bash
docker run --rm -v 99-local.properties:/srv/hybris/config/properties/prtest/99-local.properties -e NAME_ENV=prtest -e HYBRIS_SECRET=**** hybris
```

## Warning 

**In this case you can't use Optional properties.**