package com.wiley.recaptcha.aspects;

import java.util.List;

import javax.servlet.http.HttpServletRequest;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public interface WileyAspectRequestStrategy
{
	HttpServletRequest getRequest(List<Object> args);
}
