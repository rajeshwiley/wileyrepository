package com.wiley.recaptcha.aspects;

import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.PredicateUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.validation.BindingResult;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;
import net.minidev.json.parser.ParseException;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public class WileyRecaptchaAspect
{
	private static final Logger LOG = Logger.getLogger(WileyRecaptchaAspect.class);

	private static final String RECAPTCHA_SITE_KEY_PROPERTY = "recaptcha.publickey";
	private static final String RECAPTCHA_SECRET_KEY_PROPERTY = "recaptcha.privatekey";
	private static final String RECAPTCHA_RESPONSE_PARAM = "g-recaptcha-response";
	private static final String RECAPTCHA_VERIFY_URL = "https://www.google.com/recaptcha/api/siteverify";
	private static final String CAPTCHA_ENABLED_FOR_CURRENT_STORE = "captchaEnabledForCurrentStore";
	private static final String RECAPTCHA_PUBLIC_KEY = "recaptchaPublicKey";

	private SiteConfigService siteConfigService;
	private BaseStoreService baseStoreService;
	private WileyAspectRequestStrategy requestStrategy;

	public Object prepare(final ProceedingJoinPoint joinPoint) throws Throwable
	{
		if (!isCaptchaEnabledForCurrentStore() || StringUtils.isEmpty(getPublicKey()))
		{
			return joinPoint.proceed();
		}
		final List<Object> args = Arrays.asList(joinPoint.getArgs());
		final HttpServletRequest request = getRequestStrategy().getRequest(args);

		if (request != null)
		{
			request.setAttribute(CAPTCHA_ENABLED_FOR_CURRENT_STORE, Boolean.TRUE);
			request.setAttribute(RECAPTCHA_PUBLIC_KEY, getPublicKey());
		}
		return joinPoint.proceed();
	}

	public Object advise(final ProceedingJoinPoint joinPoint) throws Throwable
	{
		final boolean captchaEnabledForCurrentStore = isCaptchaEnabledForCurrentStore();
		if (!captchaEnabledForCurrentStore || StringUtils.isEmpty(getPublicKey()))
		{
			return joinPoint.proceed();
		}

		final List<Object> args = Arrays.asList(joinPoint.getArgs());
		HttpServletRequest request = (HttpServletRequest) CollectionUtils.find(args,
				PredicateUtils.instanceofPredicate(HttpServletRequest.class));

		if (request == null && RequestContextHolder.getRequestAttributes() instanceof ServletRequestAttributes)
		{
			final ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder
					.getRequestAttributes();
			request = requestAttributes.getRequest();
		}

		if (request != null)
		{
			request.setAttribute(CAPTCHA_ENABLED_FOR_CURRENT_STORE, Boolean.TRUE);
			request.setAttribute(RECAPTCHA_PUBLIC_KEY, getPublicKey());
			final String recaptchaResponse = request.getParameter(RECAPTCHA_RESPONSE_PARAM);
			if (StringUtils.isBlank(recaptchaResponse) || !checkAnswer(recaptchaResponse))
			{
				// if there is an error add a message to binding result.
				final BindingResult bindingResult = (BindingResult) CollectionUtils.find(args,
						PredicateUtils.instanceofPredicate(BindingResult.class));
				if (bindingResult != null)
				{
					bindingResult.reject("recaptcha.challenge.field.invalid", "Please verify that you are not a robot");
				}
				request.setAttribute("recaptchaChallangeAnswered", Boolean.FALSE);
			}
		}
		return joinPoint.proceed();
	}

	private boolean checkAnswer(final String recaptchaResponse) throws MalformedURLException
	{
		HttpRequestRetryHandler httpRequestRetryHandler = new DefaultHttpRequestRetryHandler(3, true);
		CloseableHttpClient client = HttpClients.custom().setRetryHandler(httpRequestRetryHandler).build();
		URL url = new URL(RECAPTCHA_VERIFY_URL);
		HttpHost httpHost = new HttpHost(url.getHost(), url.getPort(), url.getProtocol());
		HttpPost httpPost = new HttpPost(RECAPTCHA_VERIFY_URL);

		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("secret", getSecretKey()));
		params.add(new BasicNameValuePair("response", recaptchaResponse));
		httpPost.setEntity(new UrlEncodedFormEntity(params, HTTP.DEF_PROTOCOL_CHARSET));

		try
		{
			HttpResponse httpResponse = client.execute(httpHost, httpPost);
			int statusCode = httpResponse.getStatusLine().getStatusCode();

			if (statusCode != HttpStatus.SC_OK)
			{
				return false;
			}

			JSONParser jsonParser = new JSONParser(JSONParser.DEFAULT_PERMISSIVE_MODE);
			JSONObject rootObject = (JSONObject) jsonParser.parse(httpResponse.getEntity().getContent());
			String successValue = rootObject.getAsString("success");

			return Boolean.valueOf(successValue);
		}
		catch (IOException | ParseException e)
		{
			LOG.error("Exception occurred while checking captcha answer", e);
			return false;
		}
		finally
		{
			httpPost.releaseConnection();
		}
	}

	private boolean isCaptchaEnabledForCurrentStore()
	{
		final BaseStoreModel currentBaseStore = getBaseStoreService().getCurrentBaseStore();
		return currentBaseStore != null && BooleanUtils.isTrue(currentBaseStore.getCaptchaCheckEnabled());
	}

	private String getPublicKey()
	{
		return getSiteConfigService().getProperty(RECAPTCHA_SITE_KEY_PROPERTY);
	}

	private String getSecretKey()
	{
		return getSiteConfigService().getProperty(RECAPTCHA_SECRET_KEY_PROPERTY);
	}

	public SiteConfigService getSiteConfigService()
	{
		return siteConfigService;
	}

	@Required
	public void setSiteConfigService(final SiteConfigService siteConfigService)
	{
		this.siteConfigService = siteConfigService;
	}

	public BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

	@Required
	public void setBaseStoreService(final BaseStoreService baseStoreService)
	{
		this.baseStoreService = baseStoreService;
	}

	public WileyAspectRequestStrategy getRequestStrategy()
	{
		return requestStrategy;
	}

	@Required
	public void setRequestStrategy(final WileyAspectRequestStrategy requestStrategy)
	{
		this.requestStrategy = requestStrategy;
	}
}
