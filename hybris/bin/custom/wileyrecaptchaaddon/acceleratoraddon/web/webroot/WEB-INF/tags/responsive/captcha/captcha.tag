<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="publicKey" required="true"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<c:if test="${requestScope.captchaEnabledForCurrentStore}">
    <div id="g-recaptcha_widget" class="g-recaptcha" data-sitekey="${publicKey}"></div>
    <div style="color: red" id="g-recaptcha_widget-help"><form:errors/></div>
</c:if>

