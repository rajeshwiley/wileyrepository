package com.wiley.recaptcha.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
@Controller
@RequestMapping(value = "/login/captcha")
public class WileyCaptchaPageController
{
	@RequestMapping(value = "/widget/{widgetName:.*}", method = RequestMethod.GET)
	public String getWidget(@PathVariable("widgetName") final String widgetName)
	{
		return "addon:/wileyrecaptchaaddon/pages/widget/" + widgetName;
	}
}
