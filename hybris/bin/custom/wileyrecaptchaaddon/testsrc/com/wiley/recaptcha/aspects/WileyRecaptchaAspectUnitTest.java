package com.wiley.recaptcha.aspects;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.ProceedingJoinPoint;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.validation.BindingResult;

import static reactor.util.Assert.notNull;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyRecaptchaAspectUnitTest
{
	private static final String RECAPTCHA_SITE_KEY_PROPERTY = "recaptcha.publickey";
	private static final String RECAPTCHA_SITE_KEY_PROPERTY_VALUE = "publickey";
	private static final String CAPTCHA_ENABLED_FOR_CURRENT_STORE = "captchaEnabledForCurrentStore";
	private static final String RECAPTCHA_PUBLIC_KEY = "recaptchaPublicKey";

	@Mock
	private SiteConfigService siteConfigServiceMock;

	@Mock
	private BaseStoreService baseStoreServiceMock;

	@Mock
	private WileyAspectRequestStrategy requestStrategyMock;

	@Mock
	private ProceedingJoinPoint joinPointMock;

	@Mock
	private BaseStoreModel baseStoreModelMock;

	@Mock
	private HttpServletRequest requestMock;

	@Mock
	private BindingResult bindingResultMock;
	
	private Object[] args = new Object[2];

	@InjectMocks
	private WileyRecaptchaAspect aspect;

	@Before
	public void setUp() throws Throwable
	{
		when(baseStoreServiceMock.getCurrentBaseStore()).thenReturn(baseStoreModelMock);
		when(joinPointMock.proceed()).thenReturn(new Object());
		when(joinPointMock.getArgs()).thenReturn(args);
	}

	@Test
	public void prepareCaptchaDisabledTest() throws Throwable
	{
		when(baseStoreModelMock.getCaptchaCheckEnabled()).thenReturn(Boolean.FALSE);

		final Object prepare = aspect.prepare(joinPointMock);

		notNull(prepare);
		verify(siteConfigServiceMock, never()).getProperty(RECAPTCHA_SITE_KEY_PROPERTY);
		verify(baseStoreModelMock, only()).getCaptchaCheckEnabled();
		verify(joinPointMock, times(1)).proceed();
		verify(joinPointMock, never()).getArgs();
		verify(requestStrategyMock, never()).getRequest(anyList());
		verify(requestMock, never()).setAttribute(any(), any());
	}

	@Test
	public void preparePublicKeyIsNullTest() throws Throwable
	{
		when(baseStoreModelMock.getCaptchaCheckEnabled()).thenReturn(Boolean.TRUE);
		when(siteConfigServiceMock.getProperty(RECAPTCHA_SITE_KEY_PROPERTY)).thenReturn(null);

		final Object prepare = aspect.prepare(joinPointMock);

		notNull(prepare);
		verify(siteConfigServiceMock, only()).getProperty(RECAPTCHA_SITE_KEY_PROPERTY);
		verify(baseStoreModelMock, only()).getCaptchaCheckEnabled();
		verify(joinPointMock, times(1)).proceed();
		verify(joinPointMock, never()).getArgs();
		verify(requestStrategyMock, never()).getRequest(anyList());
		verify(requestMock, never()).setAttribute(any(), any());
	}

	@Test
	public void prepareRequestIsNullTest() throws Throwable
	{
		when(baseStoreModelMock.getCaptchaCheckEnabled()).thenReturn(Boolean.TRUE);
		when(siteConfigServiceMock.getProperty(RECAPTCHA_SITE_KEY_PROPERTY)).thenReturn(RECAPTCHA_SITE_KEY_PROPERTY_VALUE);
		when(requestStrategyMock.getRequest(anyList())).thenReturn(null);

		final Object prepare = aspect.prepare(joinPointMock);

		notNull(prepare);
		verify(siteConfigServiceMock, only()).getProperty(RECAPTCHA_SITE_KEY_PROPERTY);
		verify(baseStoreModelMock, only()).getCaptchaCheckEnabled();
		verify(joinPointMock, times(1)).getArgs();
		verify(requestStrategyMock, only()).getRequest(anyList());
		verify(requestMock, never()).setAttribute(any(), any());
		verify(joinPointMock, times(1)).proceed();
	}

	@Test
	public void prepareFullTest() throws Throwable
	{
		when(baseStoreModelMock.getCaptchaCheckEnabled()).thenReturn(Boolean.TRUE);
		when(siteConfigServiceMock.getProperty(RECAPTCHA_SITE_KEY_PROPERTY)).thenReturn(RECAPTCHA_SITE_KEY_PROPERTY_VALUE);
		when(requestStrategyMock.getRequest(anyList())).thenReturn(requestMock);

		final Object prepare = aspect.prepare(joinPointMock);

		notNull(prepare);
		verify(siteConfigServiceMock, times(2)).getProperty(RECAPTCHA_SITE_KEY_PROPERTY);
		verify(baseStoreModelMock, only()).getCaptchaCheckEnabled();
		verify(joinPointMock, times(1)).getArgs();
		verify(requestStrategyMock, only()).getRequest(anyList());
		verify(requestMock, times(1)).setAttribute(CAPTCHA_ENABLED_FOR_CURRENT_STORE, Boolean.TRUE);
		verify(requestMock, times(1)).setAttribute(RECAPTCHA_PUBLIC_KEY, RECAPTCHA_SITE_KEY_PROPERTY_VALUE);
		verify(joinPointMock, times(1)).proceed();
	}

	@Test
	public void adviseCaptchaDisabledTest() throws Throwable
	{
		when(baseStoreModelMock.getCaptchaCheckEnabled()).thenReturn(Boolean.FALSE);

		final Object prepare = aspect.advise(joinPointMock);

		notNull(prepare);
		verify(siteConfigServiceMock, never()).getProperty(RECAPTCHA_SITE_KEY_PROPERTY);
		verify(baseStoreModelMock, only()).getCaptchaCheckEnabled();
		verify(joinPointMock, times(1)).proceed();
		verify(joinPointMock, never()).getArgs();
		verify(requestMock, never()).setAttribute(any(), any());
	}

	@Test
	public void advisePublicKeyIsNullTest() throws Throwable
	{
		when(baseStoreModelMock.getCaptchaCheckEnabled()).thenReturn(Boolean.TRUE);
		when(siteConfigServiceMock.getProperty(RECAPTCHA_SITE_KEY_PROPERTY)).thenReturn(null);

		final Object prepare = aspect.advise(joinPointMock);

		notNull(prepare);
		verify(siteConfigServiceMock, times(1)).getProperty(RECAPTCHA_SITE_KEY_PROPERTY);
		verify(baseStoreModelMock, only()).getCaptchaCheckEnabled();
		verify(joinPointMock, times(1)).proceed();
		verify(joinPointMock, never()).getArgs();
		verify(requestMock, never()).setAttribute(any(), any());
	}

	@Test
	public void adviseRequestNotNullResponseNullBindingNullTest() throws Throwable
	{
		when(baseStoreModelMock.getCaptchaCheckEnabled()).thenReturn(Boolean.TRUE);
		when(siteConfigServiceMock.getProperty(RECAPTCHA_SITE_KEY_PROPERTY)).thenReturn(RECAPTCHA_SITE_KEY_PROPERTY_VALUE);
		args[0] = requestMock;

		final Object prepare = aspect.advise(joinPointMock);

		notNull(prepare);
		verify(siteConfigServiceMock, times(2)).getProperty(RECAPTCHA_SITE_KEY_PROPERTY);
		verify(baseStoreModelMock, only()).getCaptchaCheckEnabled();
		verify(joinPointMock, times(1)).getArgs();
		verify(requestMock, times(1)).setAttribute(CAPTCHA_ENABLED_FOR_CURRENT_STORE, Boolean.TRUE);
		verify(requestMock, times(1)).setAttribute(RECAPTCHA_PUBLIC_KEY, RECAPTCHA_SITE_KEY_PROPERTY_VALUE);
		verify(requestMock, times(1)).setAttribute("recaptchaChallangeAnswered", Boolean.FALSE);
		verify(bindingResultMock, never()).reject("recaptcha.challenge.field.invalid", "Please verify that you are not a robot");
		verify(joinPointMock, times(1)).proceed();
	}

	@Test
	public void adviseRequestNotNullResponseNullBindingNotNullTest() throws Throwable
	{
		when(baseStoreModelMock.getCaptchaCheckEnabled()).thenReturn(Boolean.TRUE);
		when(siteConfigServiceMock.getProperty(RECAPTCHA_SITE_KEY_PROPERTY)).thenReturn(RECAPTCHA_SITE_KEY_PROPERTY_VALUE);
		args[0] = requestMock;
		args[1] = bindingResultMock;

		final Object prepare = aspect.advise(joinPointMock);

		notNull(prepare);
		verify(siteConfigServiceMock, times(2)).getProperty(RECAPTCHA_SITE_KEY_PROPERTY);
		verify(baseStoreModelMock, only()).getCaptchaCheckEnabled();
		verify(joinPointMock, times(1)).getArgs();
		verify(requestMock, times(1)).setAttribute(CAPTCHA_ENABLED_FOR_CURRENT_STORE, Boolean.TRUE);
		verify(requestMock, times(1)).setAttribute(RECAPTCHA_PUBLIC_KEY, RECAPTCHA_SITE_KEY_PROPERTY_VALUE);
		verify(bindingResultMock, times(1)).reject("recaptcha.challenge.field.invalid",
				"Please verify that you are not a robot");
		verify(requestMock, times(1)).setAttribute("recaptchaChallangeAnswered", Boolean.FALSE);
		verify(joinPointMock, times(1)).proceed();
	}
}