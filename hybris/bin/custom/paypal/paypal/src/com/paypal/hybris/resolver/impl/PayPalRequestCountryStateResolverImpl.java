package com.paypal.hybris.resolver.impl;

import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.servicelayer.exceptions.SystemException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.ebay.api.CountryCodeType;
import com.paypal.hybris.resolver.PayPalCountryStateResolver;

import static org.apache.commons.lang.StringUtils.isNotBlank;


/**
 * Country and state resolver for sending request to PayPal.
 */
public class PayPalRequestCountryStateResolverImpl implements PayPalCountryStateResolver
{
	private static final Logger LOG = Logger.getLogger(PayPalRequestCountryStateResolverImpl.class);

	private static final Map<String, String> COUNTRY_CODE_MAP = Collections.singletonMap(
			CountryCodeType.CN.value(),
			CountryCodeType.C_2.value());

	@Resource
	private CommonI18NService commonI18NService;

	@Override
	public Optional<String> resolveCountry(final String countryCode)
	{
		Optional<String> resolvedCountryCode = Optional.empty();
		if (isNotBlank(countryCode))
		{
			resolvedCountryCode = Optional.of(COUNTRY_CODE_MAP.getOrDefault(countryCode, countryCode));
		}

		if (LOG.isDebugEnabled())
		{
			LOG.debug(String.format("Country code [%s] resolved to [%s]", countryCode, resolvedCountryCode.toString()));
		}

		return resolvedCountryCode;
	}

	@Override
	public Optional<String> resolveState(final String countryCode, final String stateCode)
	{
		Optional<String> resolvedStateCode = Optional.empty();
		if (isNotBlank(countryCode))
		{
			if (isNotBlank(stateCode))
			{
				resolvedStateCode = Optional.of(mapInternalStateToPayPalState(countryCode, stateCode));
			}
			else
			{
				resolvedStateCode = resolveCountry(countryCode);
			}
		}

		if (LOG.isDebugEnabled())
		{
			LOG.debug(String.format("State code [%s-%s] resolved to [%s]", countryCode, stateCode, resolvedStateCode.toString()));
		}

		return resolvedStateCode;
	}

	private String mapInternalStateToPayPalState(final String countryCode, final String stateCode)
	{
		String payPalStateCode = stateCode;
		try
		{
			CountryModel countryModel = commonI18NService.getCountry(countryCode);
			RegionModel regionModel = commonI18NService.getRegion(countryModel, countryCode + "-" + stateCode);
			if (isNotBlank(regionModel.getPayPalCode()))
			{
				payPalStateCode = regionModel.getPayPalCode();
			}
		}
		catch (SystemException e)
		{
			LOG.warn(String.format("Could not resolve region for country [%s] and state [%s]", countryCode, stateCode), e);
		}
		return payPalStateCode;
	}
}
