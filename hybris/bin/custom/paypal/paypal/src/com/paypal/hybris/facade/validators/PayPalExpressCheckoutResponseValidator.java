package com.paypal.hybris.facade.validators;

import com.paypal.hybris.data.GetExpressCheckoutDetailsResultData;


public interface PayPalExpressCheckoutResponseValidator
{
	boolean validateResponse(final GetExpressCheckoutDetailsResultData responseData);
}
