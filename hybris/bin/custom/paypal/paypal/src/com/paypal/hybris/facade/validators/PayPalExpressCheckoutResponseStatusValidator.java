package com.paypal.hybris.facade.validators;

import com.paypal.hybris.constants.PaypalConstants;
import com.paypal.hybris.data.GetExpressCheckoutDetailsResultData;


public class PayPalExpressCheckoutResponseStatusValidator implements PayPalExpressCheckoutResponseValidator
{
	@Override
	public boolean validateResponse(final GetExpressCheckoutDetailsResultData responseData)
	{
		return PaypalConstants.STATUS_SUCCESS.equalsIgnoreCase(responseData.getAck());
	}
}
