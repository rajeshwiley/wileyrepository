/**
 *
 */
package com.paypal.hybris.facade.impl;

import de.hybris.platform.acceleratorfacades.payment.impl.DefaultPaymentFacade;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.PromotionOrderEntryConsumedData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PromotionResultData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.commerceservices.order.CommerceCheckoutService;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.stereotype.Component;

import com.ebay.api.AbstractRequestType;
import com.ebay.api.AbstractResponseType;
import com.ebay.api.DoAuthorizationRequestType;
import com.ebay.api.DoAuthorizationResponseType;
import com.ebay.api.DoCaptureRequestType;
import com.ebay.api.DoCaptureResponseType;
import com.ebay.api.DoExpressCheckoutPaymentRequestType;
import com.ebay.api.DoExpressCheckoutPaymentResponseType;
import com.ebay.api.DoReferenceTransactionRequestType;
import com.ebay.api.DoReferenceTransactionResponseType;
import com.ebay.api.GetExpressCheckoutDetailsRequestType;
import com.ebay.api.GetExpressCheckoutDetailsResponseType;
import com.ebay.api.PaymentDetailsType;
import com.ebay.api.SetExpressCheckoutRequestType;
import com.ebay.api.SetExpressCheckoutResponseType;
import com.paypal.hybris.constants.PaypalConstants;
import com.paypal.hybris.data.AbstractRequestData;
import com.paypal.hybris.data.AbstractResultData;
import com.paypal.hybris.data.DoAuthorizationRequestData;
import com.paypal.hybris.data.DoAuthorizationResultData;
import com.paypal.hybris.data.DoCaptureRequestData;
import com.paypal.hybris.data.DoCaptureResultData;
import com.paypal.hybris.data.DoExpressCheckoutPaymentRequestData;
import com.paypal.hybris.data.DoExpressCheckoutPaymentResultData;
import com.paypal.hybris.data.DoReferenceTransactionRequestData;
import com.paypal.hybris.data.DoReferenceTransactionResultData;
import com.paypal.hybris.data.GetExpressCheckoutDetailsRequestData;
import com.paypal.hybris.data.GetExpressCheckoutDetailsResultData;
import com.paypal.hybris.data.SetExpressCheckoutRequestData;
import com.paypal.hybris.data.SetExpressCheckoutResultData;
import com.paypal.hybris.enums.PaymentActionType;
import com.paypal.hybris.facade.validators.PayPalExpressCheckoutResponseValidator;
import com.paypal.hybris.model.PaypalPaymentInfoModel;
import com.paypal.hybris.service.PaypalPaymentService;


/**
 * @author Aliaksei_Sery (EPAM Systems)
 *
 */
@Component
public class PayPalPaymentFacade extends DefaultPaymentFacade
{
	private final static Logger LOG = Logger.getLogger(PayPalPaymentFacade.class);

	@Resource(name = "paypalPaymentService")
	private PaypalPaymentService paypalPaymentService;

	@Resource(name = "cartService")
	private CartService cartService;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "commerceCheckoutService")
	private CommerceCheckoutService commerceCheckoutService;

	private List<PayPalExpressCheckoutResponseValidator> responseValidators;

	private ConfigurationService configurationService;

	private Converter<AbstractRequestData, AbstractRequestType> setExprCheckoutReqDataConverter;
	private Converter<AbstractResponseType, AbstractResultData> setExprCheckoutResConverter;
	private Converter<AbstractRequestData, AbstractRequestType> getExprCheckoutDetReqDataConverter;
	private Converter<AbstractResponseType, AbstractResultData> getExprCheckoutDetResConverter;
	private Converter<AbstractRequestData, AbstractRequestType> doExprCheckoutPaymentReqDataConverter;
	private Converter<AbstractResponseType, AbstractResultData> doExprCheckoutPaymentResConverter;
	private Converter<AbstractRequestData, AbstractRequestType> doAuthorizationReqDataConverter;
	private Converter<AbstractResponseType, AbstractResultData> doAuthorizationResConverter;
	private Converter<AbstractRequestData, AbstractRequestType> doCaptureReqDataConverter;
	private Converter<AbstractResponseType, AbstractResultData> doCaptureResConverter;
	private Converter<AbstractRequestData, AbstractRequestType> doRefTransactionReqDataConverter;
	private Converter<AbstractResponseType, AbstractResultData> doRefTransactionResConverter;

	@Resource private Converter<AddressData, AddressModel> addressReverseConverter;

	public SetExpressCheckoutResultData preparePaypalPayment(final SetExpressCheckoutRequestData requestData)
	{
		LOG.debug("Prepare payment for PayPal");

		prepareCartEntriesForPayment(requestData.getSessionCart());

		//convert to req object
		final SetExpressCheckoutRequestType request = (SetExpressCheckoutRequestType) setExprCheckoutReqDataConverter
				.convert(requestData);

		//call service
		final SetExpressCheckoutResponseType response = paypalPaymentService.setExpressCheckout(request);

		//convert to result data object
		final SetExpressCheckoutResultData resultData = (SetExpressCheckoutResultData) setExprCheckoutResConverter.convert(response);

		if (PaypalConstants.STATUS_SUCCESS.equalsIgnoreCase(resultData.getAck()))
		{
			List<PaymentDetailsType> paymentDetails = request.getSetExpressCheckoutRequestDetails().getPaymentDetails();

			final String paymentAction = CollectionUtils.isNotEmpty(paymentDetails) ?
					paymentDetails.get(0).getPaymentAction().value() :
					configurationService.getConfiguration().getString(PaypalConstants.DEFAULT_PAYMENT_ACTION_NAME);

			final PaypalPaymentInfoModel paymentInfo = new PaypalPaymentInfoModel();
			final UserModel user = userService.getCurrentUser();
			paymentInfo.setToken(resultData.getToken());
			paymentInfo.setUser(user);
			paymentInfo.setPaymentAction(PaymentActionType.valueOf(paymentAction.toUpperCase()));
			if(Boolean.TRUE.toString().equalsIgnoreCase(
				configurationService.getConfiguration().getString(PaypalConstants.USE_REFERENCE_TRANSCATION))){
				paymentInfo.setUseReferenceTransaction(Boolean.TRUE);
			} else {
				paymentInfo.setUseReferenceTransaction(Boolean.FALSE);
			}
			Boolean isCredit = sessionService.getAttribute(PaypalConstants.IS_PAYPAL_CREDIT);
			if(isCredit != null && isCredit){
				paymentInfo.setCode(PaypalConstants.PAYPAL_CREDIT_PAYMENT_INFO_CODE);
			} else {
				paymentInfo.setCode(PaypalConstants.PAYPAL_PAYMENT_INFO_CODE);
			}
			modelService.save(paymentInfo);

			final CartModel cart = cartService.getSessionCart();
			cart.setPaymentInfo(paymentInfo);
			modelService.save(cart);
		}

		return resultData;
	}

	public GetExpressCheckoutDetailsResultData getExpressCheckoutDetails(final GetExpressCheckoutDetailsRequestData requestData)
	{
		LOG.debug("Get express checkout details from PayPal");
		GetExpressCheckoutDetailsRequestType request = (GetExpressCheckoutDetailsRequestType) getExprCheckoutDetReqDataConverter
				.convert(requestData);

		final GetExpressCheckoutDetailsResponseType response = paypalPaymentService.getExpressCheckoutDetails(request);

		GetExpressCheckoutDetailsResultData resultData = (GetExpressCheckoutDetailsResultData) getExprCheckoutDetResConverter
				.convert(response);

		if (validateResponse(resultData))
		{
			final CartModel cart = cartService.getSessionCart();
			if (cart.getPaymentInfo() instanceof PaypalPaymentInfoModel)
			{
				final PaypalPaymentInfoModel paymentInfo = (PaypalPaymentInfoModel) cart.getPaymentInfo();
				paymentInfo.setPayerId(resultData.getPayerId());
				paymentInfo.setPayer(resultData.getPayer());
				paymentInfo.setFinancingFeeAmount(resultData.getFinancingFeeAmount());
				paymentInfo.setFinancingChangeTolerance(resultData.getFinancingChangeTolerance());
				paymentInfo.setFinancingTotalCost(resultData.getFinancingTotalCost());
				paymentInfo.setFinancingTerm(resultData.getFinancingTerm());
				paymentInfo.setFinancingMonthlyPayment(resultData.getFinancingMonthlyPayment());
				paymentInfo.setFinancingCurrencyCode(resultData.getFinancingCurrencyCode());
				paymentInfo.setIsFinancing(resultData.isIsFinancing());

				if (resultData.getBillingAddress() != null)
				{
					AddressModel billingAddress = modelService.create(AddressModel.class);
					billingAddress = addressReverseConverter.convert(resultData.getBillingAddress(), billingAddress);
					final UserModel user = cart.getUser();
					if (user instanceof CustomerModel && ((CustomerModel) user).getType() == CustomerType.GUEST)
					{
						billingAddress.setEmail(user.getUid().substring(user.getUid().indexOf("|") + 1));
					}
					else
					{
						billingAddress.setEmail(resultData.getPayer());
					}
					billingAddress.setOwner(cart);
					modelService.save(billingAddress);

					paymentInfo.setBillingAddress(billingAddress);
				}
				else if (cart.getPaymentAddress() != null)
				{
					paymentInfo.setBillingAddress(cart.getPaymentAddress());
				}

				modelService.save(paymentInfo);
				// do we need to update cart?
				modelService.save(cart);

				recalculateCart(cart);
			}
		}

		return resultData;
	}

	private void recalculateCart(final CartModel cartModel)
	{
		final CommerceCheckoutParameter commerceCheckoutParameter = new CommerceCheckoutParameter();
		commerceCheckoutParameter.setCart(cartModel);
		commerceCheckoutService.calculateCart(commerceCheckoutParameter);
	}

	/**
	 * Make call to service's doExpressCheckoutPayment method. Additionally converts from dto object to request and from
	 * response to result dto object.
	 *
	 * @param requestData dto object with service call params
	 * @return dto object with service call results
	 */
	public DoExpressCheckoutPaymentResultData doExpressCheckoutPayment(final DoExpressCheckoutPaymentRequestData requestData)
	{
		prepareCartEntriesForPayment(requestData.getSessionCart());

		final DoExpressCheckoutPaymentRequestType request = (DoExpressCheckoutPaymentRequestType) doExprCheckoutPaymentReqDataConverter
				.convert(requestData);
		final DoExpressCheckoutPaymentResponseType response = paypalPaymentService.doExpressCheckoutPayment(request);

		DoExpressCheckoutPaymentResultData resultData = (DoExpressCheckoutPaymentResultData) doExprCheckoutPaymentResConverter
				.convert(response);

		return resultData;
	}

	/**
	 * Make call to service's authorization method. Additionally converts from dto object to request and from response to
	 * result dto object.
	 *
	 * @param requestData dto object with service call params
	 * @return dto object with service call results
	 */
	public DoAuthorizationResultData doAuthorization(final DoAuthorizationRequestData requestData)
	{
		LOG.debug("Do authorization of certain amount with PayPal");

		final DoAuthorizationRequestType request = (DoAuthorizationRequestType) doAuthorizationReqDataConverter.convert(requestData);
		final DoAuthorizationResponseType response = paypalPaymentService.doAuthorization(request);

		final DoAuthorizationResultData resultData = (DoAuthorizationResultData) doAuthorizationResConverter.convert(response);

		return resultData;
	}

	/**
	 * Make call to service's capture method. Additionally converts from dto object to request and from response to
	 * result dto object.
	 *
	 * @param requestData dto object with service call params
	 * @return dto object with service call results
	 */
	public DoCaptureResultData doCapture(final DoCaptureRequestData requestData)
	{
		LOG.debug("Do capture of certain amount with PayPal");

		final DoCaptureRequestType request = (DoCaptureRequestType) doCaptureReqDataConverter.convert(requestData);
		final DoCaptureResponseType response = paypalPaymentService.doCapture(request);

		final DoCaptureResultData resultData = (DoCaptureResultData) doCaptureResConverter.convert(response);

		return resultData;
	}

	public DoReferenceTransactionResultData doReferenceTransaction(final DoReferenceTransactionRequestData requestData)
	{
		LOG.debug("DoReferenceTransaction call with PayPal");
		final DoReferenceTransactionRequestType request = (DoReferenceTransactionRequestType)doRefTransactionReqDataConverter.convert(requestData);
		final DoReferenceTransactionResponseType response = paypalPaymentService.doReferenceTransaction(request);

		final DoReferenceTransactionResultData resultData = (DoReferenceTransactionResultData) doRefTransactionResConverter.convert(response);

		return resultData;
	}

	public void setPaymentInfo(final CartData cartData)
	{
		CCPaymentInfoData ccPaymentInfo = cartData.getPaymentInfo();
		if (ccPaymentInfo == null)
		{
			final CartModel cartModel = cartService.getSessionCart();

			// Check if it's PayPal payment
			if (cartModel.getPaymentInfo() instanceof PaypalPaymentInfoModel)
			{
				// Create CCPaymentInfo data and set accountHolderName as a flag
				ccPaymentInfo = new CCPaymentInfoData();
				ccPaymentInfo.setAccountHolderName("PayPal");
			}
		}
		cartData.setPaymentInfo(ccPaymentInfo);
	}

	/**
	 *  Has been decided transfer order with each entry quantity equal to 1 (see: ECSC-24087)
	 * @param cartData - session cartData
	 */
	private void prepareCartEntriesForPayment(final CartData cartData)
	{
		final int entryAmount = cartData.getEntries().size();

		cartData.setTotalUnitCount(entryAmount);
		cartData.setAppliedProductPromotions(new ArrayList<>());
		cartData.getEntries().stream().forEach(entry ->
		{
			entry.setBasePrice(entry.getSubtotalPrice());
			entry.setQuantity(1L);
			if (entry.getTotalPrice().getValue().subtract(entry.getSubtotalPrice().getValue())
					.compareTo(BigDecimal.ZERO) != 0)
			{
				adjustPromotionsIfTheyAre(cartData.getAppliedProductPromotions(),
						entry.getEntryNumber(), entry.getTotalPrice());
			}
		});
	}

	/**
	 * Included next cases:
	 * 1 - two promotions for one entry
	 * 2 - one promotion for two entries
	 * 3 - usual promotions for entries
	 * @param entryNumber - entry number from Order
	 * @param price - promotion price for the entry
	 */
	private void adjustPromotionsIfTheyAre(final List<PromotionResultData> promotions,
										   final Integer entryNumber,
										   final PriceData price)
	{
		final PromotionResultData promotion = new PromotionResultData();
		final PromotionOrderEntryConsumedData promoEntry = new PromotionOrderEntryConsumedData();
		promoEntry.setQuantity(1L);
		promoEntry.setOrderEntryNumber(entryNumber);
		promoEntry.setAdjustedUnitPrice(price.getValue().doubleValue());
		promotion.setConsumedEntries(Arrays.asList(promoEntry));
		promotions.add(promotion);
	}

	/**
	 * Resolves a given URL to a full URL including server and port, etc.
	 *
	 * @param responseUrl
	 *           - the URL to resolve
	 * @param isSecure
	 *           - flag to indicate whether the final URL should use a secure connection or not.
	 * @return a full URL including HTTP protocol, server, port, path etc.
	 */
	@Override
	public String getFullResponseUrl(final String responseUrl, final boolean isSecure)
	{
		final BaseSiteModel currentBaseSite = getBaseSiteService().getCurrentBaseSite();

		final String fullResponseUrl = getSiteBaseUrlResolutionService().getWebsiteUrlForSite(currentBaseSite, isSecure,
				responseUrl);

		return fullResponseUrl == null ? "" : fullResponseUrl;
	}


	public void setPaypalPaymentService(final PaypalPaymentService paypalPaymentService)
	{
		this.paypalPaymentService = paypalPaymentService;
	}

	@Required
	public void setSetExprCheckoutReqDataConverter(
			final Converter<AbstractRequestData, AbstractRequestType> setExprCheckoutReqDataConverter)
	{
		this.setExprCheckoutReqDataConverter = setExprCheckoutReqDataConverter;
	}

	@Required
	public void setSetExprCheckoutResConverter(
			final Converter<AbstractResponseType, AbstractResultData> setExprCheckoutResConverter)
	{
		this.setExprCheckoutResConverter = setExprCheckoutResConverter;
	}

	@Required
	public void setGetExprCheckoutDetReqDataConverter(
			final Converter<AbstractRequestData, AbstractRequestType> getExprCheckoutDetReqDataConverter)
	{
		this.getExprCheckoutDetReqDataConverter = getExprCheckoutDetReqDataConverter;
	}

	@Required
	public void setGetExprCheckoutDetResConverter(
			final Converter<AbstractResponseType, AbstractResultData> getExprCheckoutDetResConverter)
	{
		this.getExprCheckoutDetResConverter = getExprCheckoutDetResConverter;
	}

	@Required
	public void setDoExprCheckoutPaymentReqDataConverter(
			final Converter<AbstractRequestData, AbstractRequestType> doExprCheckoutPaymentReqDataConverter)
	{
		this.doExprCheckoutPaymentReqDataConverter = doExprCheckoutPaymentReqDataConverter;
	}

	@Required
	public void setDoExprCheckoutPaymentResConverter(
			final Converter<AbstractResponseType, AbstractResultData> doExprCheckoutPaymentResConverter)
	{
		this.doExprCheckoutPaymentResConverter = doExprCheckoutPaymentResConverter;
	}

	@Required
	public void setDoAuthorizationReqDataConverter(
			final Converter<AbstractRequestData, AbstractRequestType> doAuthorizationReqDataConverter)
	{
		this.doAuthorizationReqDataConverter = doAuthorizationReqDataConverter;
	}

	@Required
	public void setDoAuthorizationResConverter(
			final Converter<AbstractResponseType, AbstractResultData> doAuthorizationResConverter)
	{
		this.doAuthorizationResConverter = doAuthorizationResConverter;
	}

	@Required
	public void setDoCaptureReqDataConverter(final Converter<AbstractRequestData, AbstractRequestType> doCaptureReqDataConverter)
	{
		this.doCaptureReqDataConverter = doCaptureReqDataConverter;
	}

	@Required
	public void setDoCaptureResConverter(final Converter<AbstractResponseType, AbstractResultData> doCaptureResConverter)
	{
		this.doCaptureResConverter = doCaptureResConverter;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	/**
	 * @param doRefTransactionReqDataConverter
	 *           converter from request data object to stab request
	 */
	@Required
	public void setDoRefTransactionReqDataConverter(final Converter<AbstractRequestData, AbstractRequestType> doRefTransactionReqDataConverter)
	{
		this.doRefTransactionReqDataConverter = doRefTransactionReqDataConverter;
	}


	/**
	 * @param doRefTransactionResConverter
	 *           converter from stab response to result data object
	 */
	@Required
	public void setDoRefTransactionResConverter(final Converter<AbstractResponseType, AbstractResultData> doRefTransactionResConverter)
	{
		this.doRefTransactionResConverter = doRefTransactionResConverter;
	}

	/**
	 * @param sessionService
	 *           service to manage session state
	 */
	@Required
	public void setSessionService(SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	public Converter<AbstractRequestData, AbstractRequestType> getSetExprCheckoutReqDataConverter() {
		return setExprCheckoutReqDataConverter;
	}

	public void setResponseValidators(
			final List<PayPalExpressCheckoutResponseValidator> responseValidators)
	{
		this.responseValidators = responseValidators;
	}

	private boolean validateResponse(final GetExpressCheckoutDetailsResultData resultData) {
		if (CollectionUtils.isEmpty(responseValidators)) {
			return true;
		}

		return responseValidators.stream().allMatch(validator -> validator.validateResponse(resultData));
	}
}
