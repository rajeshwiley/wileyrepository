package com.paypal.hybris.converters.populators.impl;

import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.RegionData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.annotation.Resource;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

import com.paypal.hybris.data.GetExpressCheckoutDetailsResultData;
import com.paypal.hybris.resolver.PayPalCountryStateResolver;


/**
 * Populates delivery {@link AddressData} based on the given {@link GetExpressCheckoutDetailsResultData}.
 */
public class GetExprCheckoutDetailsResultDataDeliveryAddressPopulator
		implements Populator<GetExpressCheckoutDetailsResultData, AddressData>
{
	@Resource
	private PayPalCountryStateResolver payPalResponseCountryStateResolver;

	@Override
	public void populate(final GetExpressCheckoutDetailsResultData resultData, final AddressData addressData)
			throws ConversionException
	{
		final String[] parsedName = StringUtils.split(resultData.getAddressName(), null, 2);
		if (ArrayUtils.isNotEmpty(parsedName))
		{
			addressData.setFirstName(parsedName[0]);
			if (parsedName.length > 1)
			{
				addressData.setLastName(parsedName[1]);
			}
		}
		else
		{
			addressData.setFirstName(resultData.getPayerFirstName());
			addressData.setLastName(resultData.getPayerLastName());
		}
		addressData.setShippingAddress(true);
		addressData.setLine1(resultData.getLine1());
		addressData.setLine2(resultData.getLine2());
		addressData.setPostalCode(resultData.getPostalCode());
		addressData.setTown(resultData.getTown());
		addressData.setVisibleInAddressBook(false);
		addressData.setDefaultAddress(false);

		String country = resultData.getCountryIsoCode();
		String stateOrProvince = resultData.getStateOrProvince();
		payPalResponseCountryStateResolver.resolveCountry(country).ifPresent(countryCode -> {
			final CountryData countryData = new CountryData();
			countryData.setIsocode(countryCode);
			addressData.setCountry(countryData);
		});
		payPalResponseCountryStateResolver.resolveState(country, stateOrProvince).ifPresent(stateCode -> {
			final RegionData regionData = new RegionData();
			regionData.setIsocode(stateCode);
			addressData.setRegion(regionData);
		});
	}
}
