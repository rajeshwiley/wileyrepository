package com.paypal.hybris.converters.populators.impl;

import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.List;

import com.ebay.api.DoExpressCheckoutPaymentRequestDetailsType;
import com.ebay.api.DoExpressCheckoutPaymentRequestType;
import com.ebay.api.PaymentDetailsType;
import com.paypal.hybris.converters.populators.AbstractRequestPaymentDetailsPopulator;
import com.paypal.hybris.data.DoExpressCheckoutPaymentRequestData;


/**
 * @author Andrei_Krauchanka (EPAM Systems)
 */
public class DoExprCheckoutPaymentReqPaymentDetailsPopulator extends
		AbstractRequestPaymentDetailsPopulator<DoExpressCheckoutPaymentRequestData, DoExpressCheckoutPaymentRequestType>
{
	/**
	 * Populate the target instance with values from the source instance.
	 *
	 * @param requestData
	 * 		the source object
	 * @param request
	 * 		the target to fill
	 * @throws de.hybris.platform.servicelayer.dto.converter.ConversionException
	 * 		if an error occurs
	 */
	@Override
	public void populate(final DoExpressCheckoutPaymentRequestData requestData, final DoExpressCheckoutPaymentRequestType request)
			throws ConversionException
	{
		List<PaymentDetailsType> paymentDetails = createPaymentDetailsList(requestData.getSessionCart());
		DoExpressCheckoutPaymentRequestDetailsType requestDetails = request.getDoExpressCheckoutPaymentRequestDetails();
		if (requestDetails == null)
		{
			requestDetails = new DoExpressCheckoutPaymentRequestDetailsType();
			request.setDoExpressCheckoutPaymentRequestDetails(requestDetails);
		}
		requestDetails.getPaymentDetails().addAll(paymentDetails);
		setPaymentActionForAllPaymentDetails(requestData.getPaymentAction(), requestDetails.getPaymentDetails());
	}
}
