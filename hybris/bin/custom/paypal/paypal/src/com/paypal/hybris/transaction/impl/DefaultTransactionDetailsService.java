package com.paypal.hybris.transaction.impl;

import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.util.List;
import java.util.stream.Collectors;

import com.ebay.api.AckCodeType;
import com.ebay.api.ErrorType;
import com.ebay.api.GetTransactionDetailsRequestType;
import com.ebay.api.GetTransactionDetailsResponseType;
import com.paypal.hybris.converters.impl.PayPalRequestDataConverter;
import com.paypal.hybris.converters.impl.PayPalResponseConverter;
import com.paypal.hybris.data.GetTransactionDetailsRequestData;
import com.paypal.hybris.data.GetTransactionDetailsResultData;
import com.paypal.hybris.data.ResultErrorData;
import com.paypal.hybris.service.PaypalPaymentService;
import com.paypal.hybris.transaction.TransactionDetailsService;

import static com.paypal.hybris.constants.PaypalConstants.ERROR_CODE_10004;
import static com.paypal.hybris.constants.PaypalConstants.ERROR_CODE_10004_LONG_DESCRIPTION;
import static java.util.Collections.singletonList;


/**
 * @author Andrei_Krauchanka (EPAM Systems)
 */
public class DefaultTransactionDetailsService implements TransactionDetailsService
{
	private PaypalPaymentService paypalPaymentService;
	private PayPalRequestDataConverter getTransactionDetailsReqDataConverter;
	private PayPalResponseConverter getTransactionDetailsResConverter;
	private ConfigurationService configurationService;

	@Override
	public GetTransactionDetailsResultData getTransactionDetails(final String transactionId)
	{
		final GetTransactionDetailsRequestData requestData = new GetTransactionDetailsRequestData();
		requestData.setTransactionId(transactionId);

		final GetTransactionDetailsRequestType request = (GetTransactionDetailsRequestType) getTransactionDetailsReqDataConverter.convert(requestData);
		final GetTransactionDetailsResponseType responseType = paypalPaymentService.getTransactionDetails(request);

		if (transactionNotExists(responseType)) {
			return transactionNotExistsResultData();
		}

		return (GetTransactionDetailsResultData) getTransactionDetailsResConverter.convert(responseType);
	}

	private boolean transactionNotExists(final GetTransactionDetailsResponseType responseType)
	{
		final List<ErrorType> responseErrorList = responseType.getErrors().stream()
				.filter(error -> error.getErrorCode().equals(getConfigurationService().getConfiguration().getString(ERROR_CODE_10004))
						&& error.getLongMessage().equals(getConfigurationService().getConfiguration().getString(ERROR_CODE_10004_LONG_DESCRIPTION)))
				.collect(Collectors.toList());

		if (AckCodeType.FAILURE.name().equalsIgnoreCase(responseType.getAck().toString()) && !responseErrorList.isEmpty()) {
			return true;
		}
		return false;
	}

	private GetTransactionDetailsResultData transactionNotExistsResultData()
	{
		final GetTransactionDetailsResultData resultData = new GetTransactionDetailsResultData();
		resultData.setAck(AckCodeType.FAILURE.name());
		final ResultErrorData resultErrorData = new ResultErrorData();
		resultErrorData.setErrorCode(getConfigurationService().getConfiguration().getString(ERROR_CODE_10004));
		resultData.setErrors(singletonList(resultErrorData));
		return resultData;
	}

	public PaypalPaymentService getPaypalPaymentService()
	{
		return paypalPaymentService;
	}

	public void setPaypalPaymentService(final PaypalPaymentService paypalPaymentService)
	{
		this.paypalPaymentService = paypalPaymentService;
	}

	public PayPalRequestDataConverter getGetTransactionDetailsReqDataConverter()
	{
		return getTransactionDetailsReqDataConverter;
	}

	public void setGetTransactionDetailsReqDataConverter(
			final PayPalRequestDataConverter getTransactionDetailsReqDataConverter)
	{
		this.getTransactionDetailsReqDataConverter = getTransactionDetailsReqDataConverter;
	}

	public PayPalResponseConverter getGetTransactionDetailsResConverter()
	{
		return getTransactionDetailsResConverter;
	}

	public void setGetTransactionDetailsResConverter(final PayPalResponseConverter getTransactionDetailsResConverter)
	{
		this.getTransactionDetailsResConverter = getTransactionDetailsResConverter;
	}

	public ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}
}
