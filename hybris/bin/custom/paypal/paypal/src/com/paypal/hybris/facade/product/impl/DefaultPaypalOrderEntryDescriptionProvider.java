package com.paypal.hybris.facade.product.impl;

import com.paypal.hybris.facade.product.PaypalOrderEntryDescriptionProvider;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;

public class DefaultPaypalOrderEntryDescriptionProvider implements PaypalOrderEntryDescriptionProvider
{
	@Override
	public String getEntryDescription(OrderEntryData entry)
	{
		return entry.getProduct().getDescription();
	}
}
