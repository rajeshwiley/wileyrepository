package com.paypal.hybris.facade.order.converters.populator;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.springframework.beans.factory.annotation.Autowired;

import com.paypal.hybris.model.PaypalPaymentInfoModel;


/**
 * Populator for PayPal payment information.
 */
public class PayPalPaymentInfoOrderPopulator implements Populator<AbstractOrderModel, AbstractOrderData>
{
	@Autowired
	private Converter<PaypalPaymentInfoModel, CCPaymentInfoData> paypalPaymentInfoConverter;

	@Override
	public void populate(final AbstractOrderModel abstractOrderModel, final AbstractOrderData abstractOrderData)
			throws ConversionException
	{
		PaymentInfoModel paymentInfo = abstractOrderModel.getPaymentInfo();
		if (paymentInfo instanceof PaypalPaymentInfoModel)
		{
			CCPaymentInfoData paymentInfoData = paypalPaymentInfoConverter.convert((PaypalPaymentInfoModel) paymentInfo);
			abstractOrderData.setPaymentInfo(paymentInfoData);
		}
	}
}
