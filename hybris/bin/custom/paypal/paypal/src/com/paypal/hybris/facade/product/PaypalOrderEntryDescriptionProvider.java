package com.paypal.hybris.facade.product;

import de.hybris.platform.commercefacades.order.data.OrderEntryData;

public interface PaypalOrderEntryDescriptionProvider
{
	String getEntryDescription(OrderEntryData entry);
}
