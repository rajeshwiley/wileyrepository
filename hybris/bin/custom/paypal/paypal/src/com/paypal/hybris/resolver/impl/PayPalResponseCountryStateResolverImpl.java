package com.paypal.hybris.resolver.impl;

import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.exceptions.SystemException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.ebay.api.CountryCodeType;
import com.paypal.hybris.resolver.PayPalCountryStateResolver;

import static com.paypal.hybris.constants.PaypalConstants.SUPPORTED_COUNTRIES_WITH_STATE;
import static org.apache.commons.lang.StringUtils.isNotBlank;
import static org.apache.commons.lang.StringUtils.EMPTY;
import static java.util.stream.Collectors.toSet;
import static java.util.Collections.emptySet;


/**
 * Country and state resolver for PayPal responses.
 */
public class PayPalResponseCountryStateResolverImpl implements PayPalCountryStateResolver
{
	private static final Logger LOG = Logger.getLogger(PayPalResponseCountryStateResolverImpl.class);

	private static final Map<String, String> COUNTRY_CODE_MAP = Collections.singletonMap(
			CountryCodeType.C_2.value(),
			CountryCodeType.CN.value());

	private static final Pattern COMMA_PATTERN = Pattern.compile(",");

	@Resource
	private ConfigurationService configurationService;

	@Resource
	private CommonI18NService commonI18NService;

	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Override
	public Optional<String> resolveCountry(final String countryCode)
	{
		Optional<String> resolvedCountryCode = Optional.empty();
		if (isNotBlank(countryCode))
		{
			resolvedCountryCode = Optional.of(COUNTRY_CODE_MAP.getOrDefault(countryCode, countryCode));
		}

		if (LOG.isDebugEnabled())
		{
			LOG.debug(String.format("Country code [%s] resolved to [%s]", countryCode, resolvedCountryCode.toString()));
		}

		return resolvedCountryCode;
	}

	@Override
	public Optional<String> resolveState(final String countryCode, final String stateCode)
	{
		Optional<String> resolvedStateCode = Optional.empty();
		String resolvedCountryCode = resolveCountry(countryCode).orElse(EMPTY);
		if (isNotBlank(stateCode) && isNotBlank(resolvedCountryCode) && getSupportedCountriesWithState().contains(
				resolvedCountryCode))
		{
			String internalStateCode = mapPayPalStateToInternalState(resolvedCountryCode, stateCode);
			if (isStateValid(resolvedCountryCode, internalStateCode))
			{
				resolvedStateCode = createStateCode(resolvedCountryCode, internalStateCode);
			}
		}
		if (LOG.isDebugEnabled())
		{
			LOG.debug(String.format("State code [%s-%s] resolved to [%s]", countryCode, stateCode, resolvedStateCode.toString()));
		}

		return resolvedStateCode;
	}

	private Optional<String> createStateCode(final String countryCode, final String stateCode)
	{
		return Optional.of(countryCode + "-" + stateCode);
	}

	private Set<String> getSupportedCountriesWithState()
	{
		String supportedCountries = configurationService.getConfiguration().getString(SUPPORTED_COUNTRIES_WITH_STATE, EMPTY);
		return isNotBlank(supportedCountries) ? COMMA_PATTERN.splitAsStream(supportedCountries).collect(toSet()) : emptySet();
	}

	private String mapPayPalStateToInternalState(final String countryCode, final String stateCode)
	{
		String internalStateCode = stateCode;
		try
		{
			CountryModel countryModel = commonI18NService.getCountry(countryCode);
			RegionModel regionModelExample = new RegionModel();
			regionModelExample.setCountry(countryModel);
			regionModelExample.setPayPalCode(stateCode);
			RegionModel regionModel = flexibleSearchService.getModelByExample(regionModelExample);
			internalStateCode = regionModel.getIsocodeShort();
		}
		catch (SystemException e)
		{
			if (LOG.isDebugEnabled())
			{
				LOG.debug(String.format("Couldn't resolve PayPal code for country[%s] and state[%s]", countryCode, stateCode), e);
			}
		}
		return internalStateCode;
	}

	private boolean isStateValid(final String countryCode, final String stateCode)
	{
		boolean stateValid = false;
		if (stateCode != null)
		{
			try
			{
				CountryModel countryModel = commonI18NService.getCountry(countryCode);
				RegionModel regionModel = commonI18NService.getRegion(countryModel, countryCode + "-" + stateCode);
				stateValid = regionModel.getIsocodeShort() != null;
			}
			catch (SystemException e)
			{
				LOG.warn(String.format("Could not resolve region for country [%s] and state [%s]", countryCode, stateCode), e);
			}
		}
		return stateValid;
	}
}
