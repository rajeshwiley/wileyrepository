package com.paypal.hybris.converters.populators;

import com.ebay.api.AddressType;
import com.ebay.api.BasicAmountType;
import com.ebay.api.CountryCodeType;
import com.ebay.api.CurrencyCodeType;
import com.ebay.api.PaymentActionCodeType;
import com.ebay.api.PaymentDetailsItemType;
import com.ebay.api.PaymentDetailsType;
import com.ebay.api.SellerDetailsType;
import com.paypal.hybris.constants.PaypalConstants;
import com.paypal.hybris.facade.product.PaypalOrderEntryDescriptionProvider;
import com.paypal.hybris.facade.product.impl.PayPalPriceFormatter;
import com.paypal.hybris.resolver.PayPalCountryStateResolver;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.DeliveryOrderEntryGroupData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.order.data.OrderEntryGroupData;
import de.hybris.platform.commercefacades.order.data.PickupOrderEntryGroupData;
import de.hybris.platform.commercefacades.order.data.PromotionOrderEntryConsumedData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.PromotionResultData;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import static com.ebay.utils.PaypalStringUtils.trimDescription;


/**
 * Abstract class to provide basic logic of populating web service call request
 * with payment details data, e.g products details, shipping methods and so on.
 *
 * @author Andrei_Krauchanka (EPAM Systems)
 */
public abstract class AbstractRequestPaymentDetailsPopulator<SOURCE, TARGET> implements Populator<SOURCE, TARGET>
{
	private ConfigurationService configurationService;
	private PriceDataFactory priceDataFactory;
	private PayPalPriceFormatter payPalPriceFormatter;
	private PaypalOrderEntryDescriptionProvider paypalOrderEntryDescriptionProvider;

	@Resource
	private PayPalCountryStateResolver payPalRequestCountryStateResolver;

	protected List<PaymentDetailsType> createPaymentDetailsList(final AbstractOrderData cart)
	{
		// support only one currency type for all cart prices
		List<PaymentDetailsType> detailsList = new ArrayList<>();

		for (DeliveryOrderEntryGroupData deliveryGroup : cart.getDeliveryOrderGroups())
		{
			deliveryGroup.setDeliveryAddress(cart.getDeliveryAddress());
			detailsList.add(createPaymentDetails(cart, deliveryGroup, detailsList.size()));
		}

		for (PickupOrderEntryGroupData pickupGroup : cart.getPickupOrderGroups())
		{
			detailsList.add(createPaymentDetails(cart, pickupGroup, detailsList.size()));
		}

		assignOrderDiscountToPaymentDetails(detailsList, cart.getOrderDiscounts(), true);

		return detailsList;
	}

	private void assignOrderDiscountToPaymentDetails(final List<PaymentDetailsType> detailsList, final PriceData discount,
			boolean subtractDiscountFromTotal)
	{
		if (BigDecimal.ZERO.compareTo(discount.getValue()) < 0)
		{
			for (PaymentDetailsType paymentDetails : detailsList)
			{
				BigDecimal orderTotal = BigDecimal.valueOf(Double.valueOf(
						paymentDetails.getOrderTotal().getValue().replaceAll(PaypalConstants.THOUSAND_SEPARATOR, "")));
				BigDecimal itemTotal = BigDecimal.valueOf(
						Double.valueOf(
								paymentDetails.getItemTotal().getValue().replaceAll(PaypalConstants.THOUSAND_SEPARATOR, "")));
				if (orderTotal.compareTo(discount.getValue()) >= 0)
				{
					if (subtractDiscountFromTotal)
					{
						orderTotal = orderTotal.subtract(discount.getValue());
						itemTotal = itemTotal.subtract(discount.getValue());
					}
					paymentDetails.getOrderTotal().setValue(orderTotal.toString());
					paymentDetails.getItemTotal().setValue(itemTotal.toString());
					paymentDetails.getPaymentDetailsItem().add(createOrderDiscountPaymentItem(discount));
					break;
				}
			}
		}
	}

	private void applyDiscountOnPaymentDetails(final AbstractOrderData cart, final PaymentDetailsType paymentDetails,
			final OrderEntryData entry,
			CurrencyCodeType currencyCode)
	{
		BigDecimal appliedProductDiscounts = BigDecimal.ZERO;
		if (CollectionUtils.isNotEmpty(cart.getAppliedProductPromotions()))
		{
			for (PromotionResultData promotionResultData : cart.getAppliedProductPromotions())
			{
				for (PromotionOrderEntryConsumedData promotionOrderEntry : promotionResultData.getConsumedEntries())
				{
					if (promotionOrderEntry.getOrderEntryNumber().equals(entry.getEntryNumber()))
					{
						final PaymentDetailsItemType detailsItem = new PaymentDetailsItemType();
						BigDecimal discountValue = BigDecimal.valueOf(promotionOrderEntry.getAdjustedUnitPrice()).subtract(
								entry.getBasePrice().getValue());
						detailsItem.setName(entry.getProduct().getName() + " " + PaypalConstants.DISCOUNT);
						detailsItem.setQuantity(BigInteger.valueOf(promotionOrderEntry.getQuantity()));
						detailsItem.setDescription(entry.getProduct().getName() + " " + PaypalConstants.DISCOUNT);

						final BasicAmountType basicAmount = new BasicAmountType();
						basicAmount.setValue(discountValue.toString());
						basicAmount.setCurrencyID(currencyCode);

						detailsItem.setAmount(basicAmount);
						if (StringUtils.isNotBlank(detailsItem.getAmount().getValue()))
						{
							paymentDetails.getPaymentDetailsItem().add(detailsItem);
							appliedProductDiscounts = appliedProductDiscounts.add(discountValue);
						}
					}
				}
			}

		}
		final BigDecimal basePriceValue =  entry.getBasePrice().getValue().multiply(BigDecimal.valueOf(entry.getQuantity()));
		final BigDecimal totalPriceValue = entry.getTotalPrice().getValue();
		final BigDecimal additionalProductDiscounts = basePriceValue.subtract(totalPriceValue.subtract(appliedProductDiscounts));
		if (additionalProductDiscounts.compareTo(BigDecimal.ZERO) > 0)
		{
			final PaymentDetailsItemType detailsItem = new PaymentDetailsItemType();
			detailsItem.setName(entry.getProduct().getName() + " " + PaypalConstants.DISCOUNT);
			detailsItem.setDescription(entry.getProduct().getName() + " " + PaypalConstants.DISCOUNT);

			final BasicAmountType basicAmount = new BasicAmountType();
			basicAmount.setValue(additionalProductDiscounts.negate().toString());
			basicAmount.setCurrencyID(currencyCode);
			detailsItem.setAmount(basicAmount);
			
			paymentDetails.getPaymentDetailsItem().add(detailsItem);
		}
	}

	private PaymentDetailsItemType createOrderDiscountPaymentItem(final PriceData discount)
	{
		final PaymentDetailsItemType detailsItem = new PaymentDetailsItemType();
		detailsItem.setName(PaypalConstants.ORDER_DISCOUNT);
		detailsItem.setQuantity(BigInteger.valueOf(1));
		detailsItem.setDescription(PaypalConstants.ORDER_DISCOUNT);

		final BasicAmountType basicAmount = new BasicAmountType();
		basicAmount.setValue(discount.getValue().negate().toString());
		basicAmount.setCurrencyID(CurrencyCodeType.valueOf(discount.getCurrencyIso()));

		detailsItem.setAmount(basicAmount);
		return detailsItem;
	}

	private PaymentDetailsType createPaymentDetails(final AbstractOrderData cart, final OrderEntryGroupData entryGroup,
			int groupNumber)
	{
		PaymentDetailsType paymentDetails = new PaymentDetailsType();
		paymentDetails.setSellerDetails(createSellerDetails());

		final String currencyIsoCode = entryGroup.getTotalPriceWithTax().getCurrencyIso();
		final CurrencyCodeType currencyCode = CurrencyCodeType.valueOf(currencyIsoCode);

		paymentDetails.setPaymentRequestID(cart.getCode() + PaypalConstants.PAYMENT_REQUEST_ID_SEPARATOR + groupNumber);

		for (final OrderEntryData entry : entryGroup.getEntries())
		{
			final PaymentDetailsItemType detailsItem = createPaymentDetailsItem(entry, currencyCode);
			paymentDetails.getPaymentDetailsItem().add(detailsItem);
			applyDiscountOnPaymentDetails(cart, paymentDetails, entry, currencyCode);
		}

		// in case of gross amount items total price already includes taxes
		PriceData detailsItemsTotalPriceData = getPriceDataFactory().create(PriceDataType.BUY,
				entryGroup.getTotalPrice().getValue(), currencyIsoCode);

		PriceData detailsDeliveryTotalData;
		if (entryGroup instanceof DeliveryOrderEntryGroupData && cart.getDeliveryCost() != null)
		{
			detailsDeliveryTotalData = cart.getDeliveryCost();
		}
		else
		{
			detailsDeliveryTotalData = getPriceDataFactory().create(PriceDataType.BUY, BigDecimal.valueOf(0), currencyIsoCode);
		}

		AddressType address = createAddressForGroup(entryGroup);
		if (address != null)
		{
			paymentDetails.setShipToAddress(address);
		}

		//setting known params
		PriceData detailsTaxTotalData = getPriceDataFactory().create(PriceDataType.BUY, BigDecimal.valueOf(0), currencyIsoCode);
		// in case of net pricing separately calculate taxes otherwise taxes were already included in items total
		if (cart.isNet())
		{
			detailsTaxTotalData = cart.getTotalTax();
		}

		final BasicAmountType itemTotal = createBasicAmountType(detailsItemsTotalPriceData, currencyCode);
		final BasicAmountType shippingTotal = createBasicAmountType(detailsDeliveryTotalData, currencyCode);
		final BasicAmountType taxTotal = createBasicAmountType(detailsTaxTotalData, currencyCode);

		double totalPrice =
				detailsItemsTotalPriceData.getValue().doubleValue() + detailsDeliveryTotalData.getValue().doubleValue()
						+ detailsTaxTotalData.getValue().doubleValue();
		PriceData detailsOrderTotalData = getPriceDataFactory().create(PriceDataType.BUY, BigDecimal.valueOf(totalPrice),
				currencyIsoCode);
		final BasicAmountType orderTotal = createBasicAmountType(detailsOrderTotalData, currencyCode);

		paymentDetails.setItemTotal(itemTotal);
		paymentDetails.setShippingTotal(shippingTotal);
		paymentDetails.setTaxTotal(taxTotal);
		paymentDetails.setOrderTotal(orderTotal);

		return paymentDetails;
	}

	private PaymentDetailsItemType createPaymentDetailsItem(final OrderEntryData entry, final CurrencyCodeType currencyCode)
	{
		//getting item info from entry
		final String name = entry.getProduct().getName();
		final Long quantity = entry.getQuantity();

		final PriceData unitPrice = entry.getBasePrice();

		final BasicAmountType amount = createBasicAmountType(unitPrice, currencyCode);

		final PaymentDetailsItemType detailsItem = new PaymentDetailsItemType();
		detailsItem.setName(name);
		detailsItem.setQuantity(BigInteger.valueOf(quantity));
		detailsItem.setDescription(trimDescription(paypalOrderEntryDescriptionProvider.getEntryDescription(entry))
				.orElse(trimDescription(entry.getProduct().getName()).orElse(null)));
		detailsItem.setAmount(amount);

		return detailsItem;
	}

	protected BasicAmountType createBasicAmountType(final PriceData priceData, final CurrencyCodeType currencyCode)
	{
		final BasicAmountType basicAmount = new BasicAmountType();
		String formattedPriceWithoutCurrencySymbol = payPalPriceFormatter.formatPrice(priceData.getValue(),
				priceData.getCurrencyIso(), false);
		basicAmount.setValue(formattedPriceWithoutCurrencySymbol);
		basicAmount.setCurrencyID(currencyCode);
		return basicAmount;
	}

	private AddressType createAddressForGroup(final OrderEntryGroupData entryGroup)
	{
		AddressData addressData = null;
		String addressName = StringUtils.EMPTY;
		if (entryGroup instanceof DeliveryOrderEntryGroupData)
		{
			DeliveryOrderEntryGroupData deliveryEntryGroup = (DeliveryOrderEntryGroupData) entryGroup;
			addressData = deliveryEntryGroup.getDeliveryAddress();

			if (addressData != null)
			{
				String addressTitle = addressData.getTitle();
				StringBuilder addressNameBuilder = new StringBuilder();
				if (addressTitle != null)
				{
					addressNameBuilder.append(addressTitle).append(PaypalConstants.ADDRESS_NAME_SEPARATOR);
				}
				addressNameBuilder.append(addressData.getFirstName()).append(PaypalConstants.ADDRESS_NAME_SEPARATOR);
				addressNameBuilder.append(addressData.getLastName());

				addressName = addressNameBuilder.toString();
			}
		}
		else if (entryGroup instanceof PickupOrderEntryGroupData)
		{
			PickupOrderEntryGroupData pickupEntryGroup = (PickupOrderEntryGroupData) entryGroup;
			PointOfServiceData pointOfService = pickupEntryGroup.getDeliveryPointOfService();
			addressData = pointOfService.getAddress();

			StringBuilder addressNameBuilder = new StringBuilder();
			addressNameBuilder.append(PaypalConstants.S2S_ADDRESS_NAME_PREFIX).append(PaypalConstants.ADDRESS_NAME_SEPARATOR);
			addressNameBuilder.append(pointOfService.getName());
			addressName = addressNameBuilder.toString();
		}

		AddressType createdAddress = null;
		if (addressData != null)
		{
			final AddressType address = new AddressType();
			address.setName(addressName);
			address.setStreet1(addressData.getLine1());
			address.setStreet2(addressData.getLine2());
			address.setCityName(addressData.getTown());
			address.setPostalCode(addressData.getPostalCode());

			if (addressData.getCountry() != null)
			{
				payPalRequestCountryStateResolver.resolveCountry(addressData.getCountry().getIsocode())
						.ifPresent(countryCode -> address.setCountry(CountryCodeType.fromValue(countryCode)));

				payPalRequestCountryStateResolver.resolveState(addressData.getCountry().getIsocode(),
						addressData.getRegion() != null ? addressData.getRegion().getIsocodeShort() : null)
						.ifPresent(stateCode -> address.setStateOrProvince(stateCode));
			}
			createdAddress = address;
		}
		return createdAddress;
	}

	private SellerDetailsType createSellerDetails()
	{
		SellerDetailsType sellerDetails = new SellerDetailsType();
		sellerDetails.setPayPalAccountID(
				getConfigurationService().getConfiguration().getString(PaypalConstants.PAYPAL_SELLER_EMAIL));
		return sellerDetails;
	}

	protected void setPaymentActionForAllPaymentDetails(final String configuredPaymentAction,
			final List<PaymentDetailsType> paymentDetailsList)
	{
		String paymentAction = configuredPaymentAction;

		// in case of multiple shipping ignore config and set Order payment type
		if (paymentDetailsList.size() > 1)
		{
			paymentAction = PaypalConstants.ORDER_PAYMENT_ACTION_NAME;
		}
		else if (StringUtils.isBlank(configuredPaymentAction))
		{
			paymentAction = getConfigurationService().getConfiguration().getString(PaypalConstants.DEFAULT_PAYMENT_ACTION_NAME);
		}

		// set calculated payment action for all payment details
		for (final PaymentDetailsType paymentDetails : paymentDetailsList)
		{
			paymentDetails.setPaymentAction(PaymentActionCodeType.fromValue(paymentAction));
		}
	}

	protected ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	protected PriceDataFactory getPriceDataFactory()
	{
		return priceDataFactory;
	}

	public void setPriceDataFactory(final PriceDataFactory priceDataFactory)
	{
		this.priceDataFactory = priceDataFactory;
	}

	protected PayPalPriceFormatter getPayPalPriceFormatter()
	{
		return payPalPriceFormatter;
	}

	public void setPayPalPriceFormatter(final PayPalPriceFormatter payPalPriceFormatter)
	{
		this.payPalPriceFormatter = payPalPriceFormatter;
	}

	@Required
	public void setPaypalOrderEntryDescriptionProvider(final PaypalOrderEntryDescriptionProvider
																   paypalOrderEntryDescriptionProvider)
	{
		this.paypalOrderEntryDescriptionProvider = paypalOrderEntryDescriptionProvider;
	}
}
