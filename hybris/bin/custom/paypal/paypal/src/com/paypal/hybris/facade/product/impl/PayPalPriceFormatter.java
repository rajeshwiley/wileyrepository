package com.paypal.hybris.facade.product.impl;

import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.i18n.I18NService;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.apache.commons.lang.StringUtils;


public class PayPalPriceFormatter
{
	private static final String CURRENCY_SYMBOL = "\u00A4";

	private final ConcurrentMap<String, NumberFormat> currencyFormats = new ConcurrentHashMap<>();

	private CommerceCommonI18NService commerceCommonI18NService;

	private CommonI18NService commonI18NService;

	private I18NService i18NService;

	public PayPalPriceFormatter(final CommerceCommonI18NService commerceCommonI18NService,
			final CommonI18NService commonI18NService, final I18NService i18NService)
	{
		this.commerceCommonI18NService = commerceCommonI18NService;
		this.commonI18NService = commonI18NService;
		this.i18NService = i18NService;
	}

	public String formatPrice(final BigDecimal value, final String currencyIso, final boolean addCurrencySymbol)
	{
		final CurrencyModel currency = commonI18NService.getCurrency(currencyIso);
		final LanguageModel currentLanguage = commonI18NService.getCurrentLanguage();
		Locale locale = commerceCommonI18NService.getLocaleForLanguage(currentLanguage);
		if (locale == null)
		{
			// Fallback to session locale
			locale = i18NService.getCurrentLocale();
		}
		final NumberFormat currencyFormat = createCurrencyFormat(locale, currency, addCurrencySymbol);
		return currencyFormat.format(value);
	}

	private NumberFormat createCurrencyFormat(final Locale locale, final CurrencyModel currency,
			final boolean addCurrencySymbol)
	{
		final String keySuffix = addCurrencySymbol ? "_cs" : StringUtils.EMPTY;
		final String key = locale.getISO3Country() + "_" + currency.getIsocode() + keySuffix;

		NumberFormat numberFormat = currencyFormats.get(key);
		if (numberFormat == null)
		{
			final NumberFormat currencyFormat = createNumberFormat(locale, currency, addCurrencySymbol);
			numberFormat = currencyFormats.putIfAbsent(key, currencyFormat);
			if (numberFormat == null)
			{
				numberFormat = currencyFormat;
			}
		}
		// don't allow multiple references
		return (NumberFormat) numberFormat.clone();
	}

	private NumberFormat createNumberFormat(final Locale locale, final CurrencyModel currency, final boolean addCurrencySymbol)
	{
		DecimalFormat currencyFormat = (DecimalFormat) NumberFormat.getCurrencyInstance(locale);

		if (!addCurrencySymbol)
		{
			currencyFormat = recreateFormatWithoutCurrencySymbol(currencyFormat);
		}

		adjustDigits(currencyFormat, currency);
		adjustSymbol(currencyFormat, currency);
		return currencyFormat;
	}

	private DecimalFormat recreateFormatWithoutCurrencySymbol(final DecimalFormat currencyFormat)
	{
		String pattern = currencyFormat.toPattern();
		String newPattern = pattern.replace(CURRENCY_SYMBOL, StringUtils.EMPTY).trim();
		DecimalFormat newFormat = new DecimalFormat(newPattern);
		return newFormat;
	}

	private DecimalFormat adjustDigits(final DecimalFormat format, final CurrencyModel currencyModel)
	{
		final int tempDigits = currencyModel.getDigits() == null ? 0 : currencyModel.getDigits().intValue();
		final int digits = Math.max(0, tempDigits);

		format.setMaximumFractionDigits(digits);
		format.setMinimumFractionDigits(digits);
		if (digits == 0)
		{
			format.setDecimalSeparatorAlwaysShown(false);
		}

		return format;
	}

	private DecimalFormat adjustSymbol(final DecimalFormat format, final CurrencyModel currencyModel)
	{
		final String symbol = currencyModel.getSymbol();
		if (symbol != null)
		{
			final DecimalFormatSymbols symbols = format.getDecimalFormatSymbols(); // does cloning
			final String iso = currencyModel.getIsocode();
			boolean changed = false;
			if (!iso.equalsIgnoreCase(symbols.getInternationalCurrencySymbol()))
			{
				symbols.setInternationalCurrencySymbol(iso);
				changed = true;
			}
			if (!symbol.equals(symbols.getCurrencySymbol()))
			{
				symbols.setCurrencySymbol(symbol);
				changed = true;
			}
			if (changed)
			{
				format.setDecimalFormatSymbols(symbols);
			}
		}
		return format;
	}
}
