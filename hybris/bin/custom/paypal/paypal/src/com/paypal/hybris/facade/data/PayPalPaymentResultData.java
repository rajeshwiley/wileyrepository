package com.paypal.hybris.facade.data;

import java.util.ArrayList;
import java.util.List;

import com.paypal.hybris.data.ResultErrorData;


public class PayPalPaymentResultData
{
	public enum PayPalPaymentStatus
	{
		SUCCESS, FAILURE
	}

	private PayPalPaymentStatus status;
	private List<ResultErrorData> errors;

	PayPalPaymentResultData(final PayPalPaymentStatus status)
	{
		this.status = status;
	}

	PayPalPaymentResultData(final PayPalPaymentStatus status, final List<ResultErrorData> errors)
	{
		this.status = status;
		this.errors = errors;
	}

	public static PayPalPaymentResultData success()
	{
		return new PayPalPaymentResultData(PayPalPaymentStatus.SUCCESS);
	}

	public static PayPalPaymentResultData failure()
	{
		return new PayPalPaymentResultData(PayPalPaymentStatus.FAILURE, new ArrayList<>());
	}

	public static PayPalPaymentResultData failure(List<ResultErrorData> errors)
	{
		return new PayPalPaymentResultData(PayPalPaymentStatus.FAILURE, errors);
	}

	public boolean isSuccess()
	{
		return status == PayPalPaymentStatus.SUCCESS;
	}

	public PayPalPaymentStatus getStatus()
	{
		return status;
	}

	public List<ResultErrorData> getErrors()
	{
		return errors;
	}
}
