package com.paypal.hybris.resolver;

import java.util.Optional;

import javax.annotation.Nullable;


/**
 * Interface for PayPal country and state resolver classes.
 */
public interface PayPalCountryStateResolver
{
	/**
	 * Resolve country code for the given country code.
	 * @param countryCode the country code
	 * @return the resolved country code for the given country code
	 */
	Optional<String> resolveCountry(@Nullable String countryCode);

	/**
	 * Resolve state code for the given country and state code.
	 * @param countryCode the country code
	 * @param stateCode the state code
	 * @return the resolved state code for the given country and state code
	 */
	Optional<String> resolveState(@Nullable String countryCode, @Nullable String stateCode);
}
