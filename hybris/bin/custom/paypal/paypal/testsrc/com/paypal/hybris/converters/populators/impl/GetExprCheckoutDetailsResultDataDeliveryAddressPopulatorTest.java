package com.paypal.hybris.converters.populators.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.data.AddressData;

import com.paypal.hybris.data.GetExpressCheckoutDetailsResultData;
import com.paypal.hybris.resolver.PayPalCountryStateResolver;

import static org.junit.Assert.assertThat;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;

import static org.mockito.Mockito.anyString;

import java.util.Optional;


/**
 * Unit test for {@link GetExprCheckoutDetailsResultDataDeliveryAddressPopulator}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class GetExprCheckoutDetailsResultDataDeliveryAddressPopulatorTest
{
	private final GetExpressCheckoutDetailsResultData resultData = new GetExpressCheckoutDetailsResultData();
	private final AddressData addressData = new AddressData();

	@InjectMocks
	private GetExprCheckoutDetailsResultDataDeliveryAddressPopulator populator;

	@Mock
	private PayPalCountryStateResolver payPalResponseCountryStateResolver;

	@Before
	public void setup()
	{
		resultData.setPayerFirstName("Nelson");
		resultData.setPayerLastName("Muntz");
		Mockito.when(payPalResponseCountryStateResolver.resolveCountry(anyString())).thenReturn(Optional.empty());
		Mockito.when(payPalResponseCountryStateResolver.resolveState(anyString(), anyString())).thenReturn(Optional.empty());
	}

	@Test
	public void shouldPopulateDeliveryAddressNameFromNamePropertyWithNoWithespaces()
	{
		// given
		resultData.setAddressName("Homer");

		// when
		populator.populate(resultData, addressData);

		// then
		assertThat(addressData, allOf(
				hasProperty("firstName", is(equalTo("Homer"))),
				hasProperty("lastName", is(nullValue()))
		));
	}

	@Test
	public void shouldPopulateDeliveryAddressNameFromNamePropertyWithAWithespace()
	{
		// given
		resultData.setAddressName("Homer Simpson");

		// when
		populator.populate(resultData, addressData);

		// then
		assertThat(addressData, allOf(
				hasProperty("firstName", is(equalTo("Homer"))),
				hasProperty("lastName", is(equalTo("Simpson")))
		));
	}

	@Test
	public void shouldPopulateDeliveryAddressNameFromNamePropertyWithMultipleWithespaces()
	{
		// given
		resultData.setAddressName("Homer J. Simpson");

		// when
		populator.populate(resultData, addressData);

		// then
		assertThat(addressData, allOf(
				hasProperty("firstName", is(equalTo("Homer"))),
				hasProperty("lastName", is(equalTo("J. Simpson")))
		));
	}

	@Test
	public void shouldPopulateDeliveryAddressNameFromPayerNameWhenNamePropertyIsNull()
	{
		// given
		resultData.setAddressName(null);

		// when
		populator.populate(resultData, addressData);

		// then
		assertThat(addressData, allOf(
				hasProperty("firstName", is(equalTo(resultData.getPayerFirstName()))),
				hasProperty("lastName", is(equalTo(resultData.getPayerLastName())))
		));
	}

	@Test
	public void shouldPopulateDeliveryAddressNameFromPayerNameWhenNamePropertyIsEmpty()
	{
		// given
		resultData.setAddressName(StringUtils.EMPTY);

		// when
		populator.populate(resultData, addressData);

		// then
		assertThat(addressData, allOf(
				hasProperty("firstName", is(equalTo(resultData.getPayerFirstName()))),
				hasProperty("lastName", is(equalTo(resultData.getPayerLastName())))
		));
	}

}
