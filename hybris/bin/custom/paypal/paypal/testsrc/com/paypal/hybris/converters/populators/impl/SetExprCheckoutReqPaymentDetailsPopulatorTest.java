package com.paypal.hybris.converters.populators.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.DeliveryOrderEntryGroupData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.ebay.api.SetExpressCheckoutRequestType;
import com.paypal.hybris.data.SetExpressCheckoutRequestData;
import com.paypal.hybris.facade.product.impl.PayPalPriceFormatter;

import static org.junit.Assert.assertThat;

import java.math.BigDecimal;
import java.util.Collections;

import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

import static org.mockito.Mockito.when;

import static com.paypal.hybris.constants.PaypalConstants.DEFAULT_PAYMENT_ACTION_NAME;
import static com.paypal.hybris.constants.PaypalConstants.NO_SHIPPING_DOES_NOT_DISPLAY;


/**
 * Unit test for {@link SetExprCheckoutReqPaymentDetailsPopulator}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SetExprCheckoutReqPaymentDetailsPopulatorTest
{
	private static final String CURRENCY = "USD";

	private final CartData cartData = new CartData();
	private final SetExpressCheckoutRequestData requestData = new SetExpressCheckoutRequestData();
	private final SetExpressCheckoutRequestType request = new SetExpressCheckoutRequestType();

	@Mock(answer = Answers.RETURNS_DEEP_STUBS)
	private ConfigurationService configurationService;

	@Mock
	private PriceDataFactory priceDataFactory;

	@Mock
	private PayPalPriceFormatter payPalPriceFormatter;

	@InjectMocks
	private SetExprCheckoutReqPaymentDetailsPopulator populator;

	@Before
	public void setup()
	{
		cartData.setDeliveryOrderGroups(Collections.singletonList(createDeliveryOrderEntryGroupData(createPriceData())));
		cartData.setPickupOrderGroups(Collections.emptyList());
		cartData.setOrderDiscounts(createPriceData());
		cartData.setTotalDiscounts(createPriceData());

		requestData.setSessionCart(cartData);

		when(configurationService.getConfiguration().getString(DEFAULT_PAYMENT_ACTION_NAME)).thenReturn("Authorization");
		when(priceDataFactory.create(PriceDataType.BUY, BigDecimal.ZERO, CURRENCY)).thenReturn(createPriceData());
		when(priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(0.0), CURRENCY)).thenReturn(createPriceData());
		when(payPalPriceFormatter.formatPrice(BigDecimal.ZERO, CURRENCY, false)).thenReturn("0.0");
	}

	@Test
	public void shouldNotDisplayShippingAddressForDigitalCart()
	{
		// given
//		cartData.setPhysicalItemsQuantity(0);

		// when
		populator.populate(requestData, request);

		// then
		assertThat("Shipping address should not be displayed for digital cart", request,
				hasProperty("setExpressCheckoutRequestDetails",
						hasProperty("noShipping", is(equalTo(NO_SHIPPING_DOES_NOT_DISPLAY)))));
	}

	@Test
	public void shouldDisplayShippingAddressForPhysicalCart()
	{
		// given
//		cartData.setPhysicalItemsQuantity(1);

		// when
		populator.populate(requestData, request);

		// then
		assertThat("Shipping address should be displayed for physical cart", request,
				hasProperty("setExpressCheckoutRequestDetails",
						hasProperty("noShipping", is(nullValue()))));
	}

	private PriceData createPriceData()
	{
		PriceData priceData = new PriceData();
		priceData.setValue(BigDecimal.ZERO);
		priceData.setCurrencyIso(CURRENCY);
		return priceData;
	}

	private DeliveryOrderEntryGroupData createDeliveryOrderEntryGroupData(final PriceData priceData)
	{
		DeliveryOrderEntryGroupData deliveryOrderEntryGroupData = new DeliveryOrderEntryGroupData();
		deliveryOrderEntryGroupData.setTotalPrice(priceData);
		deliveryOrderEntryGroupData.setTotalPriceWithTax(priceData);
		deliveryOrderEntryGroupData.setEntries(Collections.emptyList());
		return deliveryOrderEntryGroupData;
	}
}
