package com.paypal.hybris.converters.populators.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.ebay.api.DoCaptureRequestType;
import com.paypal.hybris.data.DoCaptureRequestData;


/**
 * Unit test for {@link DoCaptureReqPopulator}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DoCaptureReqPopulatorTest
{
	public static final String TEST_INVOICE_ID = "testInvoiceId";
	public static final String TEST_CURRENCY_ISOCODE = "USD";
	public static final double TEST_AMOUNT = 100.0;

	private final DoCaptureRequestData doCaptureRequestData = new DoCaptureRequestData();
	private final DoCaptureRequestType doCaptureRequestType = new DoCaptureRequestType();

	@InjectMocks
	private DoCaptureReqPopulator populator;


	@Test
	public void shouldPopulateInvoiceId()
	{
		// given
		doCaptureRequestData.setInvoiceId(TEST_INVOICE_ID);
		doCaptureRequestData.setCurrencyIsoCode(TEST_CURRENCY_ISOCODE);
		doCaptureRequestData.setAmount(TEST_AMOUNT);

		// when
		populator.populate(doCaptureRequestData, doCaptureRequestType);

		// then
		assertEquals(TEST_INVOICE_ID, doCaptureRequestType.getInvoiceID());
	}
}
