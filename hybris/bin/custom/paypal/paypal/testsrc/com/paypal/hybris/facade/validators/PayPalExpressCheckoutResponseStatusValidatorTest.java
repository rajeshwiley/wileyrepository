package com.paypal.hybris.facade.validators;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

import com.paypal.hybris.constants.PaypalConstants;
import com.paypal.hybris.data.GetExpressCheckoutDetailsResultData;


@UnitTest
public class PayPalExpressCheckoutResponseStatusValidatorTest
{

	private PayPalExpressCheckoutResponseStatusValidator testInstance = new PayPalExpressCheckoutResponseStatusValidator();

	@Test
	public void shouldReturnTrueIfStatusIsSuccess() {
		//Given
		GetExpressCheckoutDetailsResultData responseData = new GetExpressCheckoutDetailsResultData();
		responseData.setAck(PaypalConstants.STATUS_SUCCESS);
		//When
		boolean result = testInstance.validateResponse(responseData);
		//Then
		Assert.assertTrue("Validation result should be True", result);
	}

	@Test
	public void shouldReturnFalseIfStatusIsOtherThanSuccess() {
		Arrays.asList(PaypalConstants.STATUS_FAILURE,
				PaypalConstants.STATUS_ACCEPTED,
				PaypalConstants.STATUS_ERROR,
				PaypalConstants.STATUS_SUCCESS_WITH_WARNINGS, "UNKNOWN_RANDOM_STRING").forEach(status -> {

			//Given
			GetExpressCheckoutDetailsResultData responseData = new GetExpressCheckoutDetailsResultData();
			responseData.setAck(PaypalConstants.STATUS_FAILURE);
			//When
			boolean result = testInstance.validateResponse(responseData);
			//Then
			Assert.assertFalse(String.format("Validation result should be False for status string %s", status), result);
		});
	}
}