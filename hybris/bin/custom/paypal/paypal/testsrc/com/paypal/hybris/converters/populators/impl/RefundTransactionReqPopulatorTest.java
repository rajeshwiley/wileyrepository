package com.paypal.hybris.converters.populators.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.ebay.api.RefundTransactionRequestType;
import com.paypal.hybris.data.RefundTransactionRequestData;


/**
 * Unit test for {@link RefundTransactionReqPopulator}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class RefundTransactionReqPopulatorTest
{
	public static final String TEST_INVOICE_ID = "testInvoiceId";
	public static final String TEST_CURRENCY_ISOCODE = "USD";
	public static final double TEST_AMOUNT = 100.0;
	public static final String TEST_TRASACTION_ID = "testTrasactionId";

	private final RefundTransactionRequestData refundTransactionRequestData = new RefundTransactionRequestData();
	private final RefundTransactionRequestType refundTransactionRequestType = new RefundTransactionRequestType();

	@InjectMocks
	private RefundTransactionReqPopulator populator;

	@Test
	public void shouldPopulateInvoiceId()
	{
		// given
		refundTransactionRequestData.setInvoiceId(TEST_INVOICE_ID);
		refundTransactionRequestData.setCurrencyIsoCode(TEST_CURRENCY_ISOCODE);
		refundTransactionRequestData.setAmount(BigDecimal.valueOf(TEST_AMOUNT));
		refundTransactionRequestData.setTransactionId(TEST_TRASACTION_ID);

		// when
		populator.populate(refundTransactionRequestData, refundTransactionRequestType);

		// then
		assertEquals(TEST_INVOICE_ID, refundTransactionRequestType.getInvoiceID());
	}
}
