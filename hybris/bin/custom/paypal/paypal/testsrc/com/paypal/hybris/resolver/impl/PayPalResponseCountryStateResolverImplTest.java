package com.paypal.hybris.resolver.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import static org.junit.Assert.assertEquals;

import java.util.Optional;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.when;

import static com.paypal.hybris.constants.PaypalConstants.SUPPORTED_COUNTRIES_WITH_STATE;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PayPalResponseCountryStateResolverImplTest
{
	private static final String C_2 = "C2";
	private static final String CN = "CN";
	private static final String COUNTRY_CODE = "example country code";
	private static final String NULL_CODE = null;
	private static final String EMPTY_CODE = "";
	private static final String SINGLE_WHITE_SPACE_CODE = " ";
	private static final String MULTIPLE_WHITE_SPACE_CODE = "                ";
	private static final String STATE_CODE = "example state code";
	private static final String INTERNAL_STATE_CODE = "example internal state code";

	@InjectMocks
	private PayPalResponseCountryStateResolverImpl payPalResponseCountryStateResolver;

	@Mock
	private PayPalResponseCountryStateResolverImpl payPalResponseCountryStateResolverMock;

	@Mock
	private ConfigurationService configurationServiceMock;

	@Mock
	private CommonI18NService commonI18NServiceMock;

	@Mock
	private FlexibleSearchService flexibleSearchServiceMock;

	@Mock
	private RegionModel regionModelMock;

	@Mock
	private CountryModel countryModelMock;

	@Mock
	Configuration configurationMock;

	@Before
	public void setUp()
	{
		when(commonI18NServiceMock.getCountry(COUNTRY_CODE)).thenReturn(countryModelMock);
		when(commonI18NServiceMock.getRegion(countryModelMock, COUNTRY_CODE + "-" + INTERNAL_STATE_CODE))
				.thenReturn(regionModelMock);
		when(flexibleSearchServiceMock.getModelByExample(Mockito.isA(RegionModel.class))).thenReturn(regionModelMock);
		when(configurationServiceMock.getConfiguration()).thenReturn(configurationMock);
	}

	@Test
	public void shouldMakeTheMapOfTheString()
	{
		Optional<String> testString = payPalResponseCountryStateResolver.resolveCountry(COUNTRY_CODE);
		assertEquals(Optional.of(COUNTRY_CODE), testString);
	}

	@Test
	public void shouldReturnEmptyOptionalBecauseOfNullCode()
	{
		Optional<String> testString = payPalResponseCountryStateResolver.resolveCountry(NULL_CODE);
		assertEquals(Optional.empty(), testString);
	}

	@Test
	public void shouldReturnEmptyOptionalBecauseOfEmptyCode()
	{
		Optional<String> testString = payPalResponseCountryStateResolver.resolveCountry(EMPTY_CODE);
		assertEquals(Optional.empty(), testString);
	}

	@Test
	public void shouldReturnEmptyOptionalBecauseOfOneWhiteSpaceCode()
	{
		Optional<String> testString = payPalResponseCountryStateResolver.resolveCountry(SINGLE_WHITE_SPACE_CODE);
		assertEquals(Optional.empty(), testString);
	}

	@Test
	public void shouldReturnEmptyOptionalBecauseOfMultipleWhiteSpaceCode()
	{
		Optional<String> testString = payPalResponseCountryStateResolver.resolveCountry(MULTIPLE_WHITE_SPACE_CODE);
		assertEquals(Optional.empty(), testString);
	}

	@Test
	public void shouldConvertCNtoC2()
	{
		Optional<String> testString = payPalResponseCountryStateResolver.resolveCountry(C_2);
		assertEquals(Optional.of(CN), testString);
	}

	@Test
	public void shouldResolveStateCode()
	{
		when(regionModelMock.getIsocodeShort()).thenReturn(INTERNAL_STATE_CODE);
		when(configurationMock.getString(SUPPORTED_COUNTRIES_WITH_STATE, EMPTY_CODE))
				.thenReturn(COUNTRY_CODE);
		assertEquals(Optional.of(COUNTRY_CODE + "-" + INTERNAL_STATE_CODE),
								payPalResponseCountryStateResolver.resolveState(COUNTRY_CODE, STATE_CODE));
	}

	@Test
	public void shouldReturnEmptyOptionalBecauseCountryCodeIsNotPresent()
	{
		when(regionModelMock.getIsocodeShort()).thenReturn(INTERNAL_STATE_CODE);
		when(configurationMock.getString(SUPPORTED_COUNTRIES_WITH_STATE, EMPTY_CODE))
				.thenReturn(EMPTY_CODE);
		assertEquals(Optional.empty(),
				payPalResponseCountryStateResolver.resolveState(COUNTRY_CODE, STATE_CODE));
	}

	@Test
	public void shouldReturnEmptyOptionalBecauseInputCountryCodeIsEmpty()
	{
		when(regionModelMock.getIsocodeShort()).thenReturn(INTERNAL_STATE_CODE);
		when(configurationMock.getString(SUPPORTED_COUNTRIES_WITH_STATE, EMPTY_CODE))
				.thenReturn(EMPTY_CODE);
		assertEquals(Optional.empty(),
				payPalResponseCountryStateResolver.resolveState(EMPTY_CODE, STATE_CODE));
	}

	@Test
	public void shouldReturnEmptyOptionalBecauseInputStateCodeIsEmpty()
	{
		when(regionModelMock.getIsocodeShort()).thenReturn(EMPTY_CODE);
		when(configurationMock.getString(SUPPORTED_COUNTRIES_WITH_STATE, EMPTY_CODE))
				.thenReturn(COUNTRY_CODE);
		assertEquals(Optional.empty(),
				payPalResponseCountryStateResolver.resolveState(COUNTRY_CODE, EMPTY_CODE));
	}

	@Test
	public void shouldReturnEmptyOptionalBecauseInputStateCodeAndCountryCodeAreEmpty()
	{
		when(regionModelMock.getIsocodeShort()).thenReturn(EMPTY_CODE);
		when(configurationMock.getString(SUPPORTED_COUNTRIES_WITH_STATE, EMPTY_CODE))
				.thenReturn(EMPTY_CODE);
		assertEquals(Optional.empty(),
				payPalResponseCountryStateResolver.resolveState(EMPTY_CODE, EMPTY_CODE));
	}

	@Test
	public void shouldReturnEmptyOptionalBecauseIsoCodeShortIsNull()
	{
		when(regionModelMock.getIsocodeShort()).thenReturn(NULL_CODE);
		when(configurationMock.getString(SUPPORTED_COUNTRIES_WITH_STATE, EMPTY_CODE))
				.thenReturn(COUNTRY_CODE);
		assertEquals(Optional.empty(),
				payPalResponseCountryStateResolver.resolveState(COUNTRY_CODE, STATE_CODE));
	}
}
