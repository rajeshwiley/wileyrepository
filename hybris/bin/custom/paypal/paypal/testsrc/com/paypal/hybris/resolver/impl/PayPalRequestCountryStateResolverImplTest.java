package com.paypal.hybris.resolver.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import static org.junit.Assert.assertEquals;

import static org.mockito.Mockito.when;

import java.util.Optional;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PayPalRequestCountryStateResolverImplTest
{
	private static final String C_2 = "C2";
	private static final String CN = "CN";
	private static final String COUNTRY_CODE = "example country code";
	private static final String NULL_CODE = null;
	private static final String EMPTY_CODE = "";
	private static final String SINGLE_WHITE_SPACE_CODE = " ";
	private static final String MULTIPLE_WHITE_SPACE_CODE = "                ";
	private static final String STATE_CODE = "example state code";
	private static final String EXAMPLE_PAYPAL_CODE = "example PayPal code";

	@InjectMocks
	private PayPalRequestCountryStateResolverImpl payPalRequestCountryStateResolver;

	@Mock
	private CommonI18NService commonI18NServiceMock;

	@Mock
	private RegionModel regionModelMock;

	@Mock
	private CountryModel countryModelMock;

	@Before
	public void setUp()
	{
		when(commonI18NServiceMock.getCountry(COUNTRY_CODE)).thenReturn(countryModelMock);
		when(commonI18NServiceMock.getRegion(countryModelMock, COUNTRY_CODE + "-" + STATE_CODE)).thenReturn(regionModelMock);
	}

	@Test
	public void shouldMakeTheMapOfTheString()
	{
		Optional<String> testString = payPalRequestCountryStateResolver.resolveCountry(COUNTRY_CODE);
		assertEquals(Optional.of(COUNTRY_CODE), testString);
	}

	@Test
	public void shouldReturnEmptyOptionalBecauseOfNullCode()
	{
		Optional<String> testString = payPalRequestCountryStateResolver.resolveCountry(NULL_CODE);
		assertEquals(Optional.empty(), testString);
	}

	@Test
	public void shouldReturnEmptyOptionalBecauseOfEmptyCode()
	{
		Optional<String> testString = payPalRequestCountryStateResolver.resolveCountry(EMPTY_CODE);
		assertEquals(Optional.empty(), testString);
	}

	@Test
	public void shouldReturnEmptyOptionalBecauseOfOneWhiteSpaceCode()
	{
		Optional<String> testString = payPalRequestCountryStateResolver.resolveCountry(SINGLE_WHITE_SPACE_CODE);
		assertEquals(Optional.empty(), testString);
	}

	@Test
	public void shouldReturnEmptyOptionalBecauseOfMultipleWhiteSpaceCode()
	{
		Optional<String> testString = payPalRequestCountryStateResolver.resolveCountry(MULTIPLE_WHITE_SPACE_CODE);
		assertEquals(Optional.empty(), testString);
	}

	@Test
	public void shouldConvertCNtoC2()
	{
		Optional<String> testString = payPalRequestCountryStateResolver.resolveCountry(CN);
		assertEquals(Optional.of(C_2), testString);
	}

	@Test
	public void shouldReturnStateCodeBecauseOfNullPayPalStateCode()
	{
		when(regionModelMock.getPayPalCode()).thenReturn(NULL_CODE);
		assertEquals(Optional.of(STATE_CODE), payPalRequestCountryStateResolver.resolveState(COUNTRY_CODE, STATE_CODE));
	}

	@Test
	public void shouldReturnExamplePayPalCode()
	{
		when(regionModelMock.getPayPalCode()).thenReturn(EXAMPLE_PAYPAL_CODE);
		assertEquals(Optional.of(EXAMPLE_PAYPAL_CODE), payPalRequestCountryStateResolver.resolveState(COUNTRY_CODE, STATE_CODE));
	}

	@Test
	public void shouldReturnStateCodeBecauseOfEmptyPayPalStateCode()
	{
		when(regionModelMock.getPayPalCode()).thenReturn(EMPTY_CODE);
		assertEquals(Optional.of(STATE_CODE), payPalRequestCountryStateResolver.resolveState(COUNTRY_CODE, STATE_CODE));
	}

	@Test
	public void shouldReturnStateCodeBecauseOfSingleWhiteSpacePayPalStateCode()
	{
		when(regionModelMock.getPayPalCode()).thenReturn(SINGLE_WHITE_SPACE_CODE);
		assertEquals(Optional.of(STATE_CODE), payPalRequestCountryStateResolver.resolveState(COUNTRY_CODE, STATE_CODE));
	}

	@Test
	public void shouldReturnStateCodeBecauseOfMulitpleWhiteSpacePayPalStateCode()
	{
		when(regionModelMock.getPayPalCode()).thenReturn(MULTIPLE_WHITE_SPACE_CODE);
		assertEquals(Optional.of(STATE_CODE), payPalRequestCountryStateResolver.resolveState(COUNTRY_CODE, STATE_CODE));
	}

	@Test
	public void shouldReturnEmptyOptionalBecauseOfEmptyCountryCode()
	{
		assertEquals(Optional.empty(), payPalRequestCountryStateResolver.resolveState(EMPTY_CODE, STATE_CODE));
	}

	@Test
	public void shouldReturnEmptyOptionalBecauseOfNullCountryCode()
	{
		assertEquals(Optional.empty(), payPalRequestCountryStateResolver.resolveState(NULL_CODE, STATE_CODE));
	}

	@Test
	public void shouldReturnEmptyOptionalBecauseOfSingleWhiteSpaceCountryCode()
	{
		assertEquals(Optional.empty(), payPalRequestCountryStateResolver.resolveState(SINGLE_WHITE_SPACE_CODE, STATE_CODE));
	}

	@Test
	public void shouldReturnEmptyOptionalBecauseOfMultipleWhiteSpaceCountryCode()
	{
		assertEquals(Optional.empty(), payPalRequestCountryStateResolver.resolveState(MULTIPLE_WHITE_SPACE_CODE, STATE_CODE));
	}
}

