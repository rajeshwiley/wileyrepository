package com.paypal.hybris.converters.populators.impl;

import com.paypal.hybris.facade.order.converters.populator.PayPalPaymentInfoOrderPopulator;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.paypal.hybris.model.PaypalPaymentInfoModel;

import static org.mockito.Mockito.isA;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.never;
import static org.mockito.BDDMockito.given;


/**
 * Unit test for {@link PayPalPaymentInfoOrderPopulator}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PayPalPaymentInfoOrderPopulatorTest
{
	private static final CCPaymentInfoData PAYMENT_INFO_DATA = new CCPaymentInfoData();

	private final AbstractOrderData abstractOrderData = new AbstractOrderData();

	@Mock
	private Converter<PaypalPaymentInfoModel, CCPaymentInfoData> paypalPaymentInfoConverter;

	@Mock
	private PaypalPaymentInfoModel paypalPaymentInfoModel;

	@Mock
	private CreditCardPaymentInfoModel creditCardPaymentInfoModel;

	@Mock
	private AbstractOrderModel abstractOrderModel;

	@InjectMocks
	private PayPalPaymentInfoOrderPopulator payPalPaymentInfoOrderPopulator;

	@Before
	public void setup()
	{
		given(paypalPaymentInfoConverter.convert(isA(PaypalPaymentInfoModel.class))).willReturn(PAYMENT_INFO_DATA);
	}

	@Test
	public void shouldPopulatePaymentInfoForPayPalOrder()
	{
		// given
		given(abstractOrderModel.getPaymentInfo()).willReturn(paypalPaymentInfoModel);

		// when
		payPalPaymentInfoOrderPopulator.populate(abstractOrderModel, abstractOrderData);

		// then
		verify(paypalPaymentInfoConverter).convert(paypalPaymentInfoModel);
		assertSame("Payment info should be populated", PAYMENT_INFO_DATA, abstractOrderData.getPaymentInfo());
	}

	@Test
	public void shouldNotPopulatePaymentInfoForNonPayPalOrder()
	{
		// given
		given(abstractOrderModel.getPaymentInfo()).willReturn(creditCardPaymentInfoModel);

		// when
		payPalPaymentInfoOrderPopulator.populate(abstractOrderModel, abstractOrderData);

		// then
		verify(paypalPaymentInfoConverter, never()).convert(isA(PaypalPaymentInfoModel.class));
		assertNull("Payment info should not be populated for credit card payment", abstractOrderData.getPaymentInfo());
	}

	@Test
	public void shouldNotPopulateIfPaymentInfoIsNull()
	{
		// given
		given(abstractOrderModel.getPaymentInfo()).willReturn(null);

		// when
		payPalPaymentInfoOrderPopulator.populate(abstractOrderModel, abstractOrderData);

		// then
		verify(paypalPaymentInfoConverter, never()).convert(isA(PaypalPaymentInfoModel.class));
		assertNull("Payment info should not be populated for null payment", abstractOrderData.getPaymentInfo());
	}
}
