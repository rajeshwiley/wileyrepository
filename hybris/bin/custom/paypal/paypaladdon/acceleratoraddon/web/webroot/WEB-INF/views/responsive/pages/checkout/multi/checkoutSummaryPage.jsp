<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<spring:url value="/checkout/multi/summary/placeOrder" var="placeOrderUrl"/>
<spring:url value="/checkout/multi/termsAndConditions" var="getTermsAndConditionsUrl"/>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<template:page pageTitle="${pageTitle}" hideHeaderLinks="true">
<div class="order-review-page">
	<div class="row">
		<div class="col-xs-12 col-sm-12">
			<cms:pageSlot position="SideContent" var="feature" element="div" class="support-message">
				<cms:component component="${feature}"/>
			</cms:pageSlot>
			<h1 class="page-title icon-lock"><spring:theme code="checkout.multi.secure.checkout" text="Secure Checkout" /></h1>
		</div>
	</div>
	
	<div class="row">
		<multi-checkout:checkoutSteps checkoutSteps="${checkoutSteps}" progressBarId="${progressBarId}">
			<ycommerce:testId code="checkoutStepFour">
				
				<div class="order-total-section checkout-desktop">
					<div class="checkout-accordion-content-wrap">
						<multi-checkout:orderTotalsSummary cartData="${cartData}" showTaxEstimate="true" showTax="true" />
					</div>
					
					<form:form action="${placeOrderUrl}" id="placeOrderForm1" commandName="placeOrderForm">
						<div class="checkout-full-width-wrap">
							<div class="checkout-accordion-content-wrap">
							 	
							   <div class="order-terms-and-conds checkbox-component">  
							 	<form:checkbox id="termsConds" path="termsCheck"/>
							 	<spring:message code="checkout.summary.placeOrder.readTermsAndConditions.popup.link" var="termsAndConditionsPopupLink"/>
							 	<label for="termsConds">
							 		<spring:theme code="checkout.summary.placeOrder.readTermsAndConditions" text="Terms and Conditions"/>
							 		&nbsp;<a class="termsAndConditionsLink" title="${termsAndConditionsPopupLink}" 
							 				href="${getTermsAndConditionsUrl}">${termsAndConditionsPopupLink}</a>
							 	 </label>
							   </div>
							</div>
						</div>
						<div class="checkout-accordion-content-wrap">
							<button id="placeOrder" type="submit" class="button button-main large purchase-main-button">
								<spring:theme code="checkout.summary.placeOrder" text="Place Order"/>
							</button>
						</div>
					</form:form>
				</div>
			</ycommerce:testId>
		</multi-checkout:checkoutSteps>
		
		<multi-checkout:checkoutOrderSummary cartData="${cartData}" showDeliveryAddress="true" showPaymentInfo="true" showTaxEstimate="true" showTax="true" />
		
		<!-- Terms and Conditions Popup placeholder -->
		<div id="termsAndConditionsPopup" class="modal fade modalWindow"></div>
	</div>
</div>	
</template:page>