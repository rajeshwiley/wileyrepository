<%@ page import="java.math.BigDecimal"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="paypalButtonLabel" value="text.checkout.checkoutWith" />

<c:set var="paypalCheckoutUrlValue" value="/paypal/checkout/hop/expressCheckoutShortcut"/>
<c:set var="paypalCheckoutCartUrlValue" value="/paypal/checkout/hop/expressCheckoutCartShortcut"/>
<c:set var="paypalCheckoutAuthenticatedUrlValue" value="/paypal/checkout/hop/authenticatedExpressCheckoutShortcut"/>

<c:if test="${cartData.totalPriceWithTax.value.compareTo(BigDecimal.ZERO) != 0}">

	<c:choose>
		<c:when test="${payWithPayPalFlag}">
 			<spring:url var="paypalCheckoutUrl" value="${paypalCheckoutAuthenticatedUrlValue}" />
 			<c:set var="paypalButtonLabel" value="text.checkout.payWith" />
		</c:when>
		<c:when test="${pageType eq 'CART'}">
			<spring:url var="paypalCheckoutUrl" value="${paypalCheckoutCartUrlValue}" />
		</c:when>
		<c:otherwise>
			<spring:url var="paypalCheckoutUrl" value="${paypalCheckoutUrlValue}" />
		</c:otherwise>
	</c:choose>

	<div class="order-button-group">
		<a href="${paypalCheckoutUrl}" class="button button-outlined icon-paypal large" role="button" aria-label="Checkout with PayPal">
			<spring:theme code="${paypalButtonLabel}" text="Checkout with" />
		</a>
	</div>
</c:if>

