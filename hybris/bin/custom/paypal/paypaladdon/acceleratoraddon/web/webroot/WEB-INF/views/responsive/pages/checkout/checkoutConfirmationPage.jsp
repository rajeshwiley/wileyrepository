<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="checkout" tagdir="/WEB-INF/tags/responsive/checkout" %>

<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<c:url value="${continueUrl}" var="continueShoppingUrl" scope="session"/>

<sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
	<checkout:guestCheckoutConfirmationOrderDetails orderData="${orderData}" showTax="true"/>
</sec:authorize>

<sec:authorize access="!hasRole('ROLE_ANONYMOUS')">
	<checkout:checkoutConfirmationOrderDetails orderData="${orderData}" showTax="true"/>
</sec:authorize>
