package com.paypal.hybris.addon.controllers.utils;

import com.paypal.hybris.data.AbstractResultData;
import com.paypal.hybris.data.ResultErrorData;

import java.util.List;
import java.util.stream.Collectors;


public final class ControllerUtils
{
	private static final String PAYPAL_CHECKOUT_ERROR = "PayPal Checkout Error";

	private ControllerUtils() { }

	public static List<String> getLongErrorMessages(final AbstractResultData resultData)
	{
		return resultData.getErrors().stream()
				.filter(error -> !isUsedForLocalizedMessage(error))
				.map(error -> PAYPAL_CHECKOUT_ERROR + ": " + error.getLongMessage())
				.collect(Collectors.toList());
	}

	public static List<String> getShortErrorMessages(final AbstractResultData resultData)
	{
		return resultData.getErrors().stream()
				.filter(error -> !isUsedForLocalizedMessage(error))
				.map(error -> PAYPAL_CHECKOUT_ERROR + ": " + error.getShortMessage())
				.collect(Collectors.toList());
	}

	public static List<String> getDetailedErrorMessages(final AbstractResultData resultData)
	{
		return resultData.getErrors().stream()
				.map(error -> PAYPAL_CHECKOUT_ERROR
						+ " (" + error.getErrorCode() + "): " + error.getShortMessage() + ". " + error.getLongMessage())
				.collect(Collectors.toList());
	}

	public static List<String> getLocalizedLongErrorMessages(final AbstractResultData resultData)
	{
		return resultData.getErrors().stream()
				.filter(ControllerUtils::isUsedForLocalizedMessage)
				.map(ResultErrorData::getLongMessage)
				.collect(Collectors.toList());
	}

	private static boolean isUsedForLocalizedMessage(final ResultErrorData data) {
		final Boolean useLocalizedLongMessage = data.getUseLocalizedLongMessage();
		return useLocalizedLongMessage != null && useLocalizedLongMessage;
	}
}