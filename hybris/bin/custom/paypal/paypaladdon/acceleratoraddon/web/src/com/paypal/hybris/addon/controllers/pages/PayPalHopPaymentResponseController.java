package com.paypal.hybris.addon.controllers.pages;

import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.security.GUIDCookieStrategy;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.order.CartService;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.base.Preconditions;
import com.paypal.hybris.addon.controllers.PaypaladdonControllerConstants;
import com.paypal.hybris.addon.controllers.utils.ControllerUtils;
import com.paypal.hybris.addon.controllers.utils.PayPalUserHelper;
import com.paypal.hybris.addon.strategies.TaxAvailabilityCheckStrategy;
import com.paypal.hybris.constants.PaypalConstants;
import com.paypal.hybris.converters.populators.impl.GetExprCheckoutDetailsResultDataDeliveryAddressPopulator;
import com.paypal.hybris.data.GetExpressCheckoutDetailsRequestData;
import com.paypal.hybris.data.GetExpressCheckoutDetailsResultData;
import com.paypal.hybris.data.ResultErrorData;
import com.paypal.hybris.facade.impl.PayPalPaymentFacade;
import com.wiley.facades.order.WileyCheckoutFacade;
import com.wiley.storefrontcommons.controllers.util.WileyGlobalMessages;


/**
 * @author Aliaksei_Sery (EPAM Systems)
 */
@Controller
@RequestMapping(value = "/paypal/checkout/hop")
public class PayPalHopPaymentResponseController extends AbstractPageController
{
	private static final Logger LOG = LoggerFactory.getLogger(PayPalHopPaymentResponseController.class);

	protected static final String REDIRECT_URL_CART = REDIRECT_PREFIX + "/cart";
	protected static final String REDIRECT_URL_LOGIN = REDIRECT_PREFIX + "/paypal/login";
	protected static final String REDIRECT_URL_STUDENT_VERIFICATION =
			REDIRECT_PREFIX + "/checkout/multi/student-verification/add";
	private static final String REDIRECT_URL_SUCCESS = "paypal.payment.success.redirect";
	private static final String REDIRECT_URL_ERROR = "paypal.payment.error.redirect";
	private static final String ACTION_FOR_BILLING_ADDRESS = "paypal.payment.billing.address.save.if.taxes.unavailable";

	@Resource(name = "payPalPaymentFacade")
	private PayPalPaymentFacade paypalFacade;

	@Resource(name = "checkoutFacade")
	private WileyCheckoutFacade checkoutFacade;

	@Resource(name = "userFacade")
	private UserFacade userFacade;

	@Resource(name = "guidCookieStrategy")
	private GUIDCookieStrategy guidCookieStrategy;

	@Resource
	private CartService cartService;

	@Resource
	private GetExprCheckoutDetailsResultDataDeliveryAddressPopulator deliveryAddressPopulator;

	@Resource(name = "payPalUserHelper")
	private PayPalUserHelper payPalUserHelper;

	@Resource
	private SiteConfigService siteConfigService;

	@Resource
	private TaxAvailabilityCheckStrategy taxAvailabilityCheckStrategy;


	@RequestMapping(value = "/response")
	public String doHandleHopResponse(final HttpServletRequest request, final HttpServletResponse response,
			final RedirectAttributes redirectAttributes)
	{
		final boolean loginAfterPaypal = getSiteConfigService().getBoolean(PaypalConstants.PAYPAL_LOGIN_AFTER_PAYMENT, false);

		if (loginAfterPaypal && !payPalUserHelper.isHardLogin(request, false))
		{
			return REDIRECT_URL_LOGIN;
		}

		if (isStudentVerificationRequired())
		{
			return REDIRECT_URL_STUDENT_VERIFICATION;
		}

		final String token = getSessionService().getAttribute("PAYPAL_TOKEN");

		// we are supposing that session is expired if there is no payPal token
		if (token == null)
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"checkout.error.session.timedout");
			return REDIRECT_URL_CART;
		}
		final GetExpressCheckoutDetailsRequestData requestData = new GetExpressCheckoutDetailsRequestData();
		requestData.setToken(token);

		final GetExpressCheckoutDetailsResultData resultData = paypalFacade.getExpressCheckoutDetails(requestData);

		final boolean hasErrorsOrFailedStatus = !PaypalConstants.STATUS_SUCCESS.equalsIgnoreCase(resultData.getAck())
				|| hasCheckoutDetailsErrors(resultData);

		if (hasErrorsOrFailedStatus)
		{
			ControllerUtils.getDetailedErrorMessages(resultData).stream().forEachOrdered(LOG::error);

			ControllerUtils.getLongErrorMessages(resultData).stream().forEachOrdered(
					errorMessage -> GlobalMessages.addFlashMessage(
							redirectAttributes, WileyGlobalMessages.STATIC_ERROR_MESSAGES_HOLDER, errorMessage));

			ControllerUtils.getLocalizedLongErrorMessages(resultData).stream().forEachOrdered(
					errorMessage -> GlobalMessages.addFlashMessage(
							redirectAttributes, WileyGlobalMessages.STATIC_ERROR_MESSAGES_HOLDER, errorMessage));

			return getRedirectUrlError();
		}

		CartData cartData = checkoutFacade.getCheckoutCart();

		if (!Boolean.parseBoolean(siteConfigService.getProperty(ACTION_FOR_BILLING_ADDRESS)) && !cartData.isTaxAvailable())
		{
			return getRedirectUrlSuccess();
		}

		populateAddress(resultData);

		if (cartData.getDeliveryMode() == null)
		{
			checkoutFacade.setCheapestDeliveryModeForCheckout();
		}

		LOG.info("PayPal payer ID: " + resultData.getPayerId());
		return getRedirectUrlSuccess();
	}

	private String getRedirectUrlSuccess()
	{
		String successRedirect = siteConfigService.getProperty(REDIRECT_URL_SUCCESS);
		Preconditions.checkArgument(StringUtils.isNoneBlank(successRedirect),
				"No successful payment redirect configured");
		return REDIRECT_PREFIX + successRedirect;
	}

	private String getRedirectUrlError()
	{
		final String errorRedirect = siteConfigService.getProperty(REDIRECT_URL_ERROR);
		Preconditions.checkArgument(StringUtils.isNoneBlank(errorRedirect),
				"No error payment redirect configured");
		return REDIRECT_PREFIX + errorRedirect;
	}

	private boolean hasCheckoutDetailsErrors(final GetExpressCheckoutDetailsResultData resultData)
	{
		final List<ResultErrorData> errors = resultData.getErrors();
		return errors != null && !errors.isEmpty();
	}

	private boolean isStudentVerificationRequired()
	{
		return checkoutFacade.isStudentFlow() && !checkoutFacade.isCartHasStudentVerification();
	}

	private void populateAddress(final GetExpressCheckoutDetailsResultData resultData)
	{
		/*
		 * We need to set payment address before delivery one because otherwise taxes for AS wouldn't be calculated. Please find description below
		 *  - in scope of paypalFacade#getExpressCheckoutDetails paymentInfo.billingAddress is saved with new address model
		 *  - billingAddress is partOf attribute, so if paymentInfo is saved with new billingAddress old one would be removed
		 *  - currently the same model is used for abstractOrder.paymentAddress and abstractOrder.paymentInfo.billingAddress
		 *  - as result abstractOrder.paymentAddress could be null or different from  billingAddress (if initially it was pointed to another model)
		 *  - if abstractOrder.paymentAddress is null taxes for AS wouldn't be calculated - WileyDetermineExternalTaxStrategy#isAddressForTaxPresent
		 */
		AddressModel previousPaymentAddress = cartService.getSessionCart().getPaymentAddress();
		checkoutFacade.setPaymentAddress(cartService.getSessionCart().getPaymentInfo().getBillingAddress());
		final AddressData deliveryAddressData = getDeliveryAddressData(resultData);
		userFacade.addAddress(deliveryAddressData);
		checkoutFacade.setDeliveryAddress(deliveryAddressData);
		validateTaxAvailability(previousPaymentAddress);
	}

	private void validateTaxAvailability(AddressModel previousPaymentAddress)
	{
		if (!taxAvailabilityCheckStrategy.isAvailable())
		{
			checkoutFacade.setPaymentAddress(previousPaymentAddress);
		}
	}

	private AddressData getDeliveryAddressData(final GetExpressCheckoutDetailsResultData resultData)
	{
		final AddressData addressData = new AddressData();
		deliveryAddressPopulator.populate(resultData, addressData);
		return addressData;
	}

	private boolean isPayPalExpressCheckoutShortcut(final HttpServletRequest request)
	{
		return BooleanUtils.toBoolean(
				request.getParameter(PaypaladdonControllerConstants.PAY_PAL_EXPRESS_CHECKOUT_SHORTCUT_PARAM));
	}
}

