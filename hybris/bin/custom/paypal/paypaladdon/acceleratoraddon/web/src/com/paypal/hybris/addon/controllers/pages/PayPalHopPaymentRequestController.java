package com.paypal.hybris.addon.controllers.pages;

import com.paypal.hybris.addon.controllers.PaypaladdonControllerConstants;
import com.paypal.hybris.addon.controllers.utils.ControllerUtils;
import com.paypal.hybris.addon.controllers.utils.PayPalUserHelper;
import com.paypal.hybris.addon.strategies.TaxAvailabilityCheckStrategy;
import com.paypal.hybris.constants.PaypalConstants;
import com.paypal.hybris.data.SetExpressCheckoutRequestData;
import com.paypal.hybris.data.SetExpressCheckoutResultData;
import com.wiley.facades.order.WileyCheckoutFacade;
import com.wiley.facades.payment.WileyPayPalPaymentFacade;
import com.wiley.storefrontcommons.controllers.util.WileyGlobalMessages;
import de.hybris.platform.acceleratorservices.uiexperience.UiExperienceService;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.checkout.steps.AbstractCheckoutStepController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commerceservices.enums.UiExperienceLevel;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.configuration.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import java.util.Locale;
import java.util.Map;


/**
 * @author Aliaksei_Sery (EPAM Systems)
 */
@Controller
@RequestMapping
public class PayPalHopPaymentRequestController extends AbstractCheckoutStepController
{
	private static final Logger LOG = LoggerFactory.getLogger(PayPalHopPaymentRequestController.class);

	protected static final String SECURE_GUID_SESSION_KEY = "acceleratorSecureGUID";
	protected static final String REDIRECT_URL_ADD_PAYMENT_METHOD = REDIRECT_PREFIX + "/checkout/multi/payment-method/list";
	protected static final String REDIRECT_TO_PAYMENT_ADDRESS = REDIRECT_PREFIX + "/checkout/multi/payment-address/add";
	private static final String LOGIN_PAGE_KEY = "paypal.login.page";
	private static final String PAYMENT_METHOD_PAGE_KEY = "paypal.payment.method.page";

	public static final String RETURN_URL = "/paypal/checkout/hop/response/?shortcut=true";
	public static final String CART_URL = "/cart";

	@Resource(name = "wileyPayPalPaymentFacade")
	private WileyPayPalPaymentFacade paypalFacade;

	@Resource
	private UiExperienceService uiExperienceService;

	@Resource(name = "userFacade")
	private UserFacade userFacade;

	@Resource
	private ConfigurationService configurationService;

	@Resource(name = "checkoutFacade")
	private WileyCheckoutFacade checkoutFacade;

	@Resource(name = "payPalUserHelper")
	private PayPalUserHelper payPalUserHelper;

	@Resource
	private TaxAvailabilityCheckStrategy taxAvailabilityCheckStrategy;

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.hybris.platform.storefront.controllers.pages.checkout.steps.CheckoutStepController#enterStep(org.springframework
	 * .ui.Model, de.hybris.platform.storefront.controllers.pages.checkout.steps.RedirectAttributes)
	 */
	@Override
	@RequestMapping(value = "/paypal/checkout/hop/expressCheckoutShortcut", method = {RequestMethod.GET, RequestMethod.POST})
	public String enterStep(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException,
			CommerceCartModificationException
	{
		return performEnterStep(model, redirectAttributes,
				PaypaladdonControllerConstants.PAY_PAL_HOP_EXPRESS_CHECKOUT_SHORTCUT_URL, RETURN_URL, getPaymentMethodPageUrl());
	}

	private String performEnterStep(final Model model, final RedirectAttributes redirectAttributes, final String hopPaymentUrl,
			final String returnUrl, final String cancelUrl) throws CMSItemNotFoundException
	{
        getSessionService().removeAttribute(PaypalConstants.PAY_PAL_REPEAT_INITIAL_EXPR_CHECKOUT_RESULT);
		getSessionService().setAttribute(PaypalConstants.IS_PAYPAL_CREDIT, Boolean.FALSE);

		if (redirectToLoginPage(model))
		{
			getSessionService().setAttribute(PaypaladdonControllerConstants.PAY_PAL_HOP_URL_ATTR, hopPaymentUrl);
			return REDIRECT_PREFIX + getSiteConfigService().getProperty(LOGIN_PAGE_KEY);
		}

		return prepareExpressCheckout(model, redirectAttributes, returnUrl, cancelUrl, PaypalConstants.CREDIT_CARD,
				PaypalConstants.DEFAULT_SOLUTION_TYPE_NAME, true);
	}

	private String getPaymentMethodPageUrl()
	{
		return getSiteConfigService().getProperty(PAYMENT_METHOD_PAGE_KEY);
	}

	@RequestMapping(value = "/paypal/checkout/hop/authenticatedExpressCheckoutShortcut", method = RequestMethod.GET)
	public String enterStepAsGuest(final Model model, final RedirectAttributes redirectAttributes)
			throws CMSItemNotFoundException, CommerceCartModificationException
	{
		if (payPalUserHelper.isCartAnyonymous())
		{
			return PaypaladdonControllerConstants.PAY_PAL_LOGIN_REDIRECT;
		}
		else
		{
			return enterStep(model, redirectAttributes);
		}
	}

	@RequestMapping(value = "/paypal/checkout/hop/expressCheckoutCartShortcut", method = {RequestMethod.GET, RequestMethod.POST})
	public String enterStepFromCart(final Model model, final RedirectAttributes redirectAttributes)
			throws CMSItemNotFoundException, CommerceCartModificationException
	{
		paypalFacade.removeDeliveryAddressFromCart();
		return performEnterStep(model, redirectAttributes,
				PaypaladdonControllerConstants.PAY_PAL_HOP_EXPRESS_CHECKOUT_CART_SHORTCUT_URL, RETURN_URL, CART_URL);
	}

	@RequestMapping(value = "/checkout/multi/payment-method/expressCheckoutMark")
	public String expressCheckoutMark(final Model model, final RedirectAttributes redirectAttributes)
			throws CMSItemNotFoundException
	{
		getSessionService().setAttribute(PaypalConstants.IS_PAYPAL_CREDIT, Boolean.FALSE);
		return prepareExpressCheckout(model, redirectAttributes,
				"/paypal/checkout/hop/response/", "/checkout/multi/payment-method/list",
				PaypalConstants.CREDIT_CARD, PaypalConstants.DEFAULT_SOLUTION_TYPE_NAME, false);
	}

	@RequestMapping(value = "/paypal/checkout/hop/creditCartShortcut")
	public String creditCartShortcut(final Model model, final RedirectAttributes redirectAttributes)
			throws CMSItemNotFoundException
	{
		getSessionService().setAttribute(PaypalConstants.IS_PAYPAL_CREDIT, Boolean.TRUE);
		if (redirectToLoginPage(model))
		{
			getSessionService().setAttribute(PaypaladdonControllerConstants.PAY_PAL_HOP_URL_ATTR,
					PaypaladdonControllerConstants.PAY_PAL_HOP_CREDIT_CART_SHORTCUT_URL);
			return REDIRECT_PREFIX + "/login/checkout";
		}

		paypalFacade.removeDeliveryAddressFromCart();
		return prepareExpressCheckout(model, redirectAttributes,
				"/paypal/checkout/hop/response/?shortcut=true", "/cart",
				PaypalConstants.BML, PaypalConstants.SOLE_SOLUTION_TYPE_NAME, false);
	}

	public String prepareExpressCheckout(final Model model, final RedirectAttributes redirectAttributes, final String returnUrl,
			final String cancelUrl, final String fundingSource, final String solutionType,
			final boolean isInContextCheckoutAvailable) throws CMSItemNotFoundException
	{
		if (checkoutFacade.getCheckoutCart() == null || CollectionUtils.isEmpty(checkoutFacade.getCheckoutCart().getEntries()))
		{
			return REDIRECT_URL_CART;
		}

		final Configuration configuration = configurationService.getConfiguration();
		Locale userLocale = JaloSession.getCurrentSession().getSessionContext().getLocale();
		final SetExpressCheckoutRequestData requestData = new SetExpressCheckoutRequestData();
		requestData.setPaymentAction(configuration.getString(PaypalConstants.PAYMENT_ACTION));

		final UiExperienceLevel uiExperienceLevel = uiExperienceService.getUiExperienceLevel();
		requestData.setLocale(userLocale);
		requestData.setUiExperienceLevel(uiExperienceLevel);

		if (PaypalConstants.BML.equals(fundingSource) && !easyPaymentsSelected())
		{
			requestData.setFundingSource(fundingSource);
		}

		if (easyPaymentsSelected())
		{
			requestData.setFundingSource(PaypalConstants.FINANCE);
		}

		requestData.setSolutionType(solutionType);
		requestData.setCancelUrl(paypalFacade.getFullResponseUrl(cancelUrl, true));
		requestData.setReturnUrl(paypalFacade.getFullResponseUrl(returnUrl, true));
		if (checkoutFacade.isDigitalSessionCart())
		{
			paypalFacade.removeDeliveryAddressFromCart();
		}
		if (!taxAvailabilityCheckStrategy.isAvailable())
		{
			return onTaxUnavailable(cancelUrl);
		}
		requestData.setSessionCart(checkoutFacade.getCheckoutCart());

		final SetExpressCheckoutResultData resultData = paypalFacade.preparePaypalPayment(requestData);

		final StringBuilder redirectUrl = new StringBuilder();
		if (PaypalConstants.STATUS_SUCCESS.equalsIgnoreCase(resultData.getAck()))
		{
			LOG.info("PayPal express checkout token: " + resultData.getToken());

			getSessionService().setAttribute("PAYPAL_TOKEN", resultData.getToken());
			redirectUrl.append(REDIRECT_PREFIX);
			if (uiExperienceLevel == UiExperienceLevel.DESKTOP)
			{
				if (isInContextCheckoutAvailable && configuration.getBoolean(PaypalConstants.IN_CONTEXT_CHECKOUT_ENABLED))
				{
					redirectUrl.append(configuration.getString(PaypalConstants.IN_CONTEXT_CHECKOUT_REDIRECT_URL));
				}
				else
				{
					if (easyPaymentsSelected())
					{
						redirectUrl.append(configuration.getString(PaypalConstants.SETT_REDIRECT_URL_DESKTOP_EP));
					}
					else
					{
						redirectUrl.append(configuration.getString(PaypalConstants.SETT_REDIRECT_URL_DESKTOP));
					}
				}
			}
			else if (uiExperienceLevel == UiExperienceLevel.MOBILE)
			{
				redirectUrl.append(configuration.getString(PaypalConstants.SETT_REDIRECT_URL_MOBILE));
			}
			redirectUrl.append(resultData.getToken());
		}
		else
		{
			ControllerUtils.getDetailedErrorMessages(resultData).stream().forEachOrdered(LOG::error);
			ControllerUtils.getLongErrorMessages(resultData).stream().forEachOrdered(
					errorMessage -> GlobalMessages.addFlashMessage(
							redirectAttributes, WileyGlobalMessages.STATIC_ERROR_MESSAGES_HOLDER, errorMessage));

			redirectUrl.setLength(0);
			redirectUrl.append(REDIRECT_PREFIX + cancelUrl);
		}

		return redirectUrl.toString();
	}

	private String onTaxUnavailable(final String cancelUrl)
	{
		checkoutFacade.resetCartPaymentInfo();
		return REDIRECT_PREFIX + cancelUrl;
	}

	private boolean easyPaymentsSelected()
	{
		return Boolean.TRUE.toString().equalsIgnoreCase(configurationService.getConfiguration().
				getString(PaypalConstants.USE_EASY_PAYMENT))
				&& Boolean.TRUE.equals(getSessionService().getAttribute(PaypalConstants.IS_PAYPAL_CREDIT))
				&& !PaypalConstants.AUTHORIZATION_PAYMENT_ACTION_NAME.equalsIgnoreCase(configurationService.getConfiguration().
				getString(PaypalConstants.PAYMENT_ACTION));
	}

	/**
	 * Calculates if user should be redirected to login page.
	 * Redirect can be disabled with PAYPAL_LOGIN_AFTER_PAYMENT parameter set to true.
	 * He will see login page in case paypal guest redirect option is set to false and he isn't
	 * hard login to site or is anonymous. Otherwise user will go to login page only if he has account
	 * and is not hard login.
	 *
	 * @param model
	 * 		model with parameter
	 * @return true if redirect is needed, false otherwise
	 */
	private boolean redirectToLoginPage(final Model model)
	{
		boolean redirectToLoginPage;
		boolean skipLogin = getSiteConfigService().getBoolean(PaypalConstants.PAYPAL_LOGIN_AFTER_PAYMENT, false);
		if (skipLogin)
		{
			redirectToLoginPage = false;
		}
		else
		{
			boolean isGuestUserRedirect = getSiteConfigService().getBoolean(PaypalConstants.PAYPAL_GUEST_REDIRECT, true);
			if (isGuestUserRedirect)
			{
				redirectToLoginPage = !userFacade.isAnonymousUser() && !payPalUserHelper.isHardLogin(model);
			}
			else
			{
				redirectToLoginPage = !payPalUserHelper.isHardLogin(model);
			}
		}			

		return redirectToLoginPage;
	}

	@RequestMapping(value = "/paypal/hop/error", method = RequestMethod.GET)
	public String doPayPalPageError(@RequestParam(required = true) final String decision,
			@RequestParam(required = true) final String[] reasonCodes, final Model model,
			final RedirectAttributes redirectAttributes)
			throws CMSItemNotFoundException
	{
		final Map<String, String> errorsDetails = getSessionService().getAttribute(
				PaypaladdonControllerConstants.PAY_PAL_ERRORS_DETAILS);

		final String redirectUrl = REDIRECT_URL_CART;
		model.addAttribute("decision", decision);
		model.addAttribute("reasonCodes", reasonCodes);
		model.addAttribute("errorsDetails", errorsDetails);
		model.addAttribute("redirectUrl", redirectUrl.replace(REDIRECT_PREFIX, ""));
		model.addAttribute(WebConstants.BREADCRUMBS_KEY,
				getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.hostedOrderPageError.breadcrumb"));
		storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));

		GlobalMessages.addErrorMessage(model, "paypal.general.error.header");

		return PaypaladdonControllerConstants.Views.Pages.MultiStepCheckout.PAY_PAL_HOSTED_ORDER_PAGE_ERROR_PAGE;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.hybris.platform.storefront.controllers.pages.checkout.steps.CheckoutStepController#back(de.hybris.platform.
	 * storefront.controllers.pages.checkout.steps.RedirectAttributes)
	 */
	@Override
	public String back(final RedirectAttributes redirectAttributes)
	{
		// YTODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.hybris.platform.storefront.controllers.pages.checkout.steps.CheckoutStepController#next(de.hybris.platform.
	 * storefront.controllers.pages.checkout.steps.RedirectAttributes)
	 */
	@Override
	public String next(final RedirectAttributes redirectAttributes)
	{
		// YTODO Auto-generated method stub
		return null;
	}

}
