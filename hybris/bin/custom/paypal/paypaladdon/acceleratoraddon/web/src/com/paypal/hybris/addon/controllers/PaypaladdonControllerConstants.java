/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.paypal.hybris.addon.controllers;

import com.paypal.hybris.addon.model.PayPalExpressCheckoutShortcutModel;
import com.paypal.hybris.addon.model.PayPalMiniCartComponentModel;


public interface PaypaladdonControllerConstants
{
	String ADDON_PREFIX = "addon:/paypaladdon/";
	String REDIRECT_PREFIX = "redirect:";

	String PAY_PAL_HOSTED_ORDER_POST_PAGE = ADDON_PREFIX + "pages/checkout/multi/hostedOrderPostPage";
	String CHECKOUT_PAGE_VIEW_NAME_FRAGMENT = "pages/checkout/";

	String PAY_PAL_EXPRESS_CHECKOUT_SHORTCUT_PARAM = "shortcut";
	String PAY_PAL_HOP_URL_ATTR = "payPalHopRequestUrl";
	String PAY_PAL_ERRORS_DETAILS = "errorsDetails";
	String PAY_PAL_GUEST_CHECKOUT_UID = "guest-checkout-uid";

	String PAY_PAL_HOP_EXPRESS_CHECKOUT_SHORTCUT_URL = "/paypal/checkout/hop/expressCheckoutShortcut";
	String PAY_PAL_HOP_EXPRESS_CHECKOUT_CART_SHORTCUT_URL = "/paypal/checkout/hop/expressCheckoutCartShortcut";
	String PAY_PAL_HOP_EXPRESS_AUTHENTICATED_CHECKOUT_SHORTCUT_URL = "/paypal/checkout/hop/authenticatedExpressCheckoutShortcut";
	String PAY_PAL_HOP_CREDIT_CART_SHORTCUT_URL = "/paypal/checkout/hop/creditCartShortcut";

	String PAY_PAL_LOGIN_REDIRECT = REDIRECT_PREFIX + "/paypal/login";

	interface Actions
	{
		interface Cms
		{
			String PREFIX = "/view/";
			String SUFFIX = "Controller";
			String PAY_PAL_MINI_CART_COMPONENT = PREFIX + PayPalMiniCartComponentModel._TYPECODE + SUFFIX;
			String PAY_PAL_EXPRESS_CHECKOUT_SHORTCUT = PREFIX + PayPalExpressCheckoutShortcutModel._TYPECODE + SUFFIX;
		}
	}

	interface Views
	{
		interface Pages
		{
			interface MultiStepCheckout
			{
				String SILENT_ORDER_POST_PAGE = ADDON_PREFIX + "pages/checkout/multi/silentOrderPostPage";
				String CHECKOUT_SUMMARY_PAGE = ADDON_PREFIX + "pages/checkout/multi/checkoutSummaryPage";
				String PAY_PAL_HOSTED_ORDER_PAGE_ERROR_PAGE = ADDON_PREFIX + "pages/checkout/multi/payPalErrorPage";
			}

			interface Checkout
			{
				String CHECKOUT_CONFIRMATION_PAGE = ADDON_PREFIX + "pages/checkout/checkoutConfirmationPage";
				String CHECKOUT_LOGIN_PAGE =  "pages/checkout/checkoutLoginPage";
			}
		}

		interface Fragments
		{
			interface Cart
			{
				String CART_POPUP = ADDON_PREFIX + "fragments/cart/cartPopup";
				String ADD_TO_CART_POPUP = ADDON_PREFIX + "fragments/cart/addToCartPopup";
			}
		}
	}
}
