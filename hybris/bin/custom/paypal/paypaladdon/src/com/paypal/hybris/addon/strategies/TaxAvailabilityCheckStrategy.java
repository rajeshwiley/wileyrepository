package com.paypal.hybris.addon.strategies;


public interface TaxAvailabilityCheckStrategy
{
	boolean isAvailable();

}
