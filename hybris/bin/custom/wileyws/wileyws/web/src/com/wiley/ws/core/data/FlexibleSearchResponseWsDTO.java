package com.wiley.ws.core.data;

import java.io.Serializable;


/**
 * Created by Uladzimir_Barouski on 9/27/2016.
 */
public class FlexibleSearchResponseWsDTO implements Serializable
{
	private Boolean result;

	public Boolean getResult()
	{
		return result;
	}

	public void setResult(final Boolean result)
	{
		this.result = result;
	}
}
