/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package com.wiley.ws.core.validator;

import de.hybris.platform.commercewebservicescommons.dto.user.PrincipalWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.user.UserGroupWsDTO;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


/**
 * The type Principal list dto validator.
 */
public class PrincipalListDTOValidator implements Validator
{
	UserService userService;
	String fieldPath;
	boolean canBeEmpty = true;

	/**
	 * Supports boolean.
	 *
	 * @param clazz
	 * 		the clazz
	 * @return the boolean
	 */
	@Override
	public boolean supports(final Class clazz)
	{
		return List.class.isAssignableFrom(clazz) || UserGroupWsDTO.class.isAssignableFrom(clazz);
	}

	/**
	 * Validate.
	 *
	 * @param target
	 * 		the target
	 * @param errors
	 * 		the errors
	 */
	@Override
	public void validate(final Object target, final Errors errors)
	{
		final List<PrincipalWsDTO> list = (List<PrincipalWsDTO>) (fieldPath == null ? target : errors.getFieldValue(fieldPath));
		final String uidFieldName = fieldPath == null ? "uid" : fieldPath + ".uid";

		if (list == null || list.isEmpty())
		{
			if (!canBeEmpty)
			{
				errors.reject("field.required");
			}
		}
		else
		{
			for (final PrincipalWsDTO principal : list)
			{
				if (StringUtils.isEmpty(principal.getUid()))
				{
					errors.reject("field.withName.required", new String[] { uidFieldName }, "Field {0} is required");
					break;
				}
				else
				{
					if (!userService.isUserExisting(principal.getUid()))
					{
						errors.reject("user.doesnt.exist", new String[] { principal.getUid() },
								"User {0} doesn''t exist or you have no privileges");
						break;
					}
				}
			}
		}
	}

	/**
	 * Gets field path.
	 *
	 * @return the field path
	 */
	public String getFieldPath()
	{
		return fieldPath;
	}

	/**
	 * Sets field path.
	 *
	 * @param fieldPath
	 * 		the field path
	 */
	public void setFieldPath(final String fieldPath)
	{
		this.fieldPath = fieldPath;
	}

	/**
	 * Gets can be empty.
	 *
	 * @return the can be empty
	 */
	public boolean getCanBeEmpty()
	{
		return canBeEmpty;
	}

	/**
	 * Sets can be empty.
	 *
	 * @param canBeEmpty
	 * 		the can be empty
	 */
	public void setCanBeEmpty(final boolean canBeEmpty)
	{
		this.canBeEmpty = canBeEmpty;
	}

	/**
	 * Gets user service.
	 *
	 * @return the user service
	 */
	public UserService getUserService()
	{
		return userService;
	}

	/**
	 * Sets user service.
	 *
	 * @param userService
	 * 		the user service
	 */
	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

}
