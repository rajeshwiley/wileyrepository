package com.wiley.ws.core.data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProcessDefinitionDto
{
	@Size(max = 255)
	@NotNull
	private String code;

	private String content;

	@NotNull
	private Long id;

	@NotNull
	private Boolean active;

	@NotNull
	private Long version;

	@Size(max = 255)
	@NotNull
	private String checksum;

	public String getCode()
	{
		return code;
	}

	public void setCode(final String code)
	{
		this.code = code;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(final String content)
	{
		this.content = content;
	}

	public Long getId()
	{
		return id;
	}

	public void setId(final Long id)
	{
		this.id = id;
	}

	public Boolean getActive()
	{
		return active;
	}

	public void setActive(final Boolean active)
	{
		this.active = active;
	}

	public Long getVersion()
	{
		return version;
	}

	public void setVersion(final Long version)
	{
		this.version = version;
	}

	public String getChecksum()
	{
		return checksum;
	}

	public void setChecksum(final String checksum)
	{
		this.checksum = checksum;
	}
}
