package com.wiley.ws.core.v3.controller;

import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.webservicescommons.errors.exceptions.WebserviceValidationException;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.Size;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.wiley.facades.wileyws.customer.update.dto.CustomerUpdateRequest;
import com.wiley.facades.wileyws.customer.update.facade.WileyCustomerUpdateFacade;

import static com.wiley.ws.core.constants.WileyWSConstants.APPLICATION_TYPE_JSON;
import static de.hybris.platform.webservicescommons.util.YSanitizer.sanitize;
import static java.lang.String.format;


/**
 * This controller is about inbound messages for updates in customer information.
 */
@Controller
@RequestMapping("/customers")
@Validated
public class WileyCustomerUpdateController
{
	private static final Logger LOG = Logger.getLogger(WileyCustomerUpdateController.class);

	@Resource
	private WileyCustomerUpdateFacade wileyCustomerUpdateFacade;

	@Secured("ROLE_TRUSTED_CLIENT")
	@RequestMapping(value = "{customerId:.+}", method = RequestMethod.POST,
			produces = { APPLICATION_TYPE_JSON }, consumes = { APPLICATION_TYPE_JSON })
	public ResponseEntity updateCustomer(@PathVariable("customerId") @Size(max = 255) final String customerId,
			@Valid @RequestBody final CustomerUpdateRequest customerUpdateRequest, final BindingResult bindingResult)
			throws DuplicateUidException
	{
		if (bindingResult.hasErrors())
		{
			throw new WebserviceValidationException(bindingResult);
		}
		if (LOG.isDebugEnabled())
		{
			LOG.debug(format("Customer update request [%s]: %s", sanitize(customerId),
					String.valueOf(customerUpdateRequest)));
		}
		wileyCustomerUpdateFacade.updateCustomer(customerId, customerUpdateRequest);
		return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
	}

}
