package com.wiley.ws.core.system.v1.controller;

import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.cronjob.model.JobModel;
import de.hybris.platform.processengine.enums.ProcessState;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.cronjob.CronJobService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.wiley.core.model.BusinessProcessUpdateMaintenanceCronJobModel;
import com.wiley.ws.core.data.BusinessProcessPatchRequestDTO;
import com.wiley.ws.core.data.JobDTO;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
@Controller
@RequestMapping(value = "/processes")
public class WileyProcessesController
{
	private static final String DEFAULT_TYPE_SYSTEM = "db.type.system.name";
	private static final String DEFAULT_PREVIOUS_TYPE_SYSTEM = "db.type.system.name.previous";
	private static final String JOBS_PATH = "/wileyws/system/v1/jobs/";
	private static final String JOB_NAME = "wileyBusinessProcessUpdateJobPerformable";
	private static final List<ProcessState> DEFAULT_STATE = Collections.singletonList(ProcessState.RUNNING);

	@Resource
	private ConfigurationService configurationService;

	@Resource
	private CronJobService cronJobService;

	@Resource
	private ModelService modelService;

	@Secured("ROLE_ADMINGROUP")
	@RequestMapping(method = RequestMethod.PATCH,
			consumes = "application/merge-patch+json", produces = "application/json")
	@ResponseBody
	public JobDTO updateProcessesWithLatestTypeSystem(@RequestParam(required = false) final List<ProcessState> state,
													  @RequestParam(required = false) final String typeSystem,
													  @Valid @RequestBody final BusinessProcessPatchRequestDTO requestDto,
													  final HttpServletResponse response)
	{
		String typeSystemName = StringUtils.defaultIfBlank(typeSystem, getStringProperty(DEFAULT_PREVIOUS_TYPE_SYSTEM));
		String newTypeSystem = StringUtils.defaultIfBlank(requestDto.getTypeSystem(), getStringProperty(DEFAULT_TYPE_SYSTEM));

		final JobModel job = cronJobService.getJob(JOB_NAME);
		final Collection<CronJobModel> cronJobs = job.getCronJobs();
		final Optional<CronJobModel> runningCronJobOptional = cronJobs.stream()
				.filter(r -> CronJobStatus.RUNNING.equals(r.getStatus()))
				.findAny();

		if (runningCronJobOptional.isPresent())
		{
			final CronJobModel runningCronJob = runningCronJobOptional.get();
			response.setStatus(HttpServletResponse.SC_CONFLICT);
			return createLocationDTO(runningCronJob);
		}

		final BusinessProcessUpdateMaintenanceCronJobModel cronJob
				= modelService.create(BusinessProcessUpdateMaintenanceCronJobModel.class);
		cronJob.setTypeSystem(typeSystemName);
		cronJob.setProcessStates(CollectionUtils.isNotEmpty(state) ? state : DEFAULT_STATE);
		cronJob.setNewTypeSystem(newTypeSystem);
		cronJob.setJob(job);

		modelService.save(cronJob);
		cronJobService.performCronJob(cronJob);

		response.setStatus(HttpServletResponse.SC_ACCEPTED);
		return createLocationDTO(cronJob);
	}

	private JobDTO createLocationDTO(final CronJobModel cronJob)
	{
		final JobDTO jobDTO = new JobDTO();
		jobDTO.setHref(JOBS_PATH + cronJob.getPk().toString());
		jobDTO.setId(cronJob.getPk().getLong());
		jobDTO.setCode(cronJob.getCode());
		jobDTO.setCreated(cronJob.getCreationtime());
		return jobDTO;
	}

	private String getStringProperty(final String key) {
		return configurationService.getConfiguration().getString(key);
	}
}
