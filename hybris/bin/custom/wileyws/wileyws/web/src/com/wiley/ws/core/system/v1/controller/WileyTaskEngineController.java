package com.wiley.ws.core.system.v1.controller;

import de.hybris.platform.task.TaskService;

import javax.annotation.Resource;

import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.wiley.ws.core.data.TaskEngineActiveStatusDto;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
@Controller
@RequestMapping(value = "/task-engine/active")
public class WileyTaskEngineController
{
	@Resource
	private TaskService taskService;

	@Secured("ROLE_ADMINGROUP")
	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public TaskEngineActiveStatusDto getStatus()
	{
		final TaskEngineActiveStatusDto status = new TaskEngineActiveStatusDto();
		status.setActive(taskService.getEngine().isRunning());
		return status;
	}

	@Secured("ROLE_ADMINGROUP")
	@RequestMapping(method = RequestMethod.PUT, consumes = "application/json")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void changeTaskEngineStatus(@RequestBody final TaskEngineActiveStatusDto status)
	{
		if (status.getActive())
		{
			taskService.getEngine().start();
		}
		else
		{
			taskService.getEngine().stop();
		}
	}
}
