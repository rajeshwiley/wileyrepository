package com.wiley.ws.core.v2.controller;

/**
 * This an empty bean to override those controller, that we can't comment out
 */
public class EmptyController
{
}
