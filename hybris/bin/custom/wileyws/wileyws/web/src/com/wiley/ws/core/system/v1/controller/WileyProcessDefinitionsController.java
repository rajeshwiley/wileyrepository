package com.wiley.ws.core.system.v1.controller;

import de.hybris.platform.converters.Converters;
import de.hybris.platform.processengine.model.DynamicProcessDefinitionModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.wiley.core.servicelayer.processdefinition.WileyProcessDefinitionService;
import com.wiley.ws.core.data.ProcessDefinitionDto;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
@Controller
@RequestMapping(value = "/process-definitions")
public class WileyProcessDefinitionsController
{
	@Resource
	private WileyProcessDefinitionService wileyProcessDefinitionService;

	@Resource
	private Converter<DynamicProcessDefinitionModel, ProcessDefinitionDto> wileyProcessDefinitionConverter;

	@Secured("ROLE_ADMINGROUP")
	@RequestMapping(method = RequestMethod.GET,
			consumes = "application/json", produces = "application/json")
	@ResponseBody
	public List<ProcessDefinitionDto> getProcessDefinitions(@RequestParam(required = false) final String code)
	{
		final List<DynamicProcessDefinitionModel> definitions;

		if (StringUtils.isEmpty(code))
		{
			definitions = wileyProcessDefinitionService.getAllProcessDefinitions();
		}
		else
		{
			definitions = wileyProcessDefinitionService.getProcessDefinitionsByCode(code);
		}

		return Converters.convertAll(definitions, wileyProcessDefinitionConverter);
	}
}
