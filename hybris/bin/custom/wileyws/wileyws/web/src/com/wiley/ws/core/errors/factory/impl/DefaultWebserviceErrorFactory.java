/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package com.wiley.ws.core.errors.factory.impl;

import de.hybris.platform.webservicescommons.dto.error.ErrorWsDTO;
import de.hybris.platform.webservicescommons.errors.converters.AbstractErrorConverter;
import de.hybris.platform.webservicescommons.errors.factory.WebserviceErrorFactory;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;


/**
 * Default factory used to create a list of errors by using configured converters.
 */
public class DefaultWebserviceErrorFactory implements WebserviceErrorFactory
{
	private List<AbstractErrorConverter> converters;

	/**
	 * Create error list list.
	 *
	 * @param obj
	 * 		the obj
	 * @return the list
	 */
	@Override
	public List<ErrorWsDTO> createErrorList(final Object obj)
	{
		final List<ErrorWsDTO> errors = createTarget();
		for (final AbstractErrorConverter converter : converters)
		{
			if (converter.supports(obj.getClass()))
			{
				errors.addAll(converter.convert(obj));
			}
		}
		return errors;
	}

	/**
	 * Create target list.
	 *
	 * @return the list
	 */
	protected List<ErrorWsDTO> createTarget()
	{
		return new LinkedList<>();
	}

	/**
	 * Sets converters.
	 *
	 * @param converters
	 * 		the converters
	 */
	@Required
	public void setConverters(final List<AbstractErrorConverter> converters)
	{
		this.converters = converters;
	}
}
