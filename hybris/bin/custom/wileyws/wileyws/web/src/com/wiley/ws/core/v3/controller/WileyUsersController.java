package com.wiley.ws.core.v3.controller;

import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.webservicescommons.errors.exceptions.WebserviceValidationException;

import java.net.URI;
import java.net.URISyntaxException;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.wiley.ws.core.customers.impl.WileyWsCustomerFacadeImpl;
import com.wiley.ws.core.product.crud.dto.CustomerCreateRequestWsDto;
import com.wiley.ws.core.v2.controller.BaseCommerceController;


@Controller
@RequestMapping(value = "/customers")
public class WileyUsersController extends BaseCommerceController
{
	private static final Logger LOG = Logger.getLogger(WileyUsersController.class);

	@Resource(name = "wileyWsCustomerFacade")
	private WileyWsCustomerFacadeImpl customerFacade;
	@Resource(name = "customerCreateRequestWsDtoValidator")
	private Validator customerCreateRequestWsDtoValidator;


	/**
	 * Registers a customer.
	 *
	 * @param externalApplicationId
	 * 		Optional id of external system
	 * @param externalCustomerId
	 * 		Optional customer's id in external system
	 * @param customer
	 * 		User's object
	 * @throws de.hybris.platform.commerceservices.customer.DuplicateUidException
	 * 		in case the requested login already exists
	 * @throws UnknownIdentifierException
	 * 		if the title code is invalid
	 * @throws WebserviceValidationException
	 * 		if any filed is invalid
	 * @bodyparams email, password, titleCode, firstName, lastName
	 * @security Permitted only for external ebp clients
	 */
	@Secured({ "ROLE_EXTERNAL_CLIENT" })
	@RequestMapping(method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity registerCustomer(
			@RequestHeader(value = "ExternalApplicationId", required = false) final String externalApplicationId,
			@RequestHeader(value = "ExternalCustomerId", required = false) final String externalCustomerId,
			@RequestBody final CustomerCreateRequestWsDto customer)
			throws DuplicateUidException, UnknownIdentifierException, IllegalArgumentException, WebserviceValidationException,
			URISyntaxException
	{
		validate(customer, "customer", customerCreateRequestWsDtoValidator);

		register(externalApplicationId, externalCustomerId, customer);

		return createResponse(customer);
	}

	private ResponseEntity createResponse(final CustomerCreateRequestWsDto customer) throws URISyntaxException
	{
		final String customerId = customerFacade.getCustomerId(customer.getEmail());
		final String path = String.format("/customers/%s", customerId);
		return ResponseEntity.created(new URI(path)).build();
	}

	private void register(final String externalApplicationId, final String externalCustomerId,
			final CustomerCreateRequestWsDto customer) throws DuplicateUidException
	{
		final RegisterData registrationData = getDataMapper().map(customer, RegisterData.class);

		registrationData.setExternalApplicationId(externalApplicationId);
		registrationData.setExternalCustomerId(externalCustomerId);

		customerFacade.register(registrationData);
	}
}
