package com.wiley.ws.core.v3.controller.test;

import javax.annotation.Resource;

import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.wiley.ws.core.transaction.dto.WileyWsTestTransactionDTO;
import com.wiley.ws.core.transaction.facade.WileyTransactionFacade;
import com.wiley.ws.core.v2.controller.BaseController;

import static com.wiley.ws.core.constants.WileyWSConstants.APPLICATION_TYPE_JSON;


@Controller
@Profile("wileyws_test")
@RequestMapping("/transactions")
public class WileyInternalTransactionController extends BaseController
{

	@Resource
	private WileyTransactionFacade wileyTransactionFacade;

	@RequestMapping(value = "/{transactionId}/rollback", method = RequestMethod.PUT, produces = { APPLICATION_TYPE_JSON })
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	public WileyWsTestTransactionDTO rollbackCurrentTransation(@PathVariable final String transactionId)
	{
		final WileyWsTestTransactionDTO wileyWsTestTransactionDTO = wileyTransactionFacade.rollbackTransaction(
				transactionId);
		return wileyWsTestTransactionDTO;
	}

	@RequestMapping(method = RequestMethod.POST, produces = { APPLICATION_TYPE_JSON })
	@ResponseBody
	@ResponseStatus(HttpStatus.CREATED)
	public WileyWsTestTransactionDTO beginNewTransaction()
	{
		return wileyTransactionFacade.beginNewTransaction();
	}
}
