/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package com.wiley.ws.core.v2.helper;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.ProductSearchFacade;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.search.facetdata.ProductCategorySearchPageData;
import de.hybris.platform.commerceservices.search.facetdata.ProductSearchPageData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.commercewebservicescommons.dto.search.facetdata.ProductCategorySearchPageWsDTO;
import de.hybris.platform.commercewebservicescommons.dto.search.facetdata.ProductSearchPageWsDTO;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import com.wiley.ws.core.util.ws.SearchQueryCodec;


/**
 * The type Products helper.
 */
@Component
public class ProductsHelper extends AbstractHelper
{
	@Resource(name = "productSearchFacade")
	private ProductSearchFacade<ProductData> productSearchFacade;
	@Resource(name = "cwsSearchQueryCodec")
	private SearchQueryCodec<SolrSearchQueryData> searchQueryCodec;
	@Resource(name = "solrSearchStateConverter")
	private Converter<SolrSearchQueryData, SearchStateData> solrSearchStateConverter;
	/**
	 * Search products product search page ws dto.
	 *
	 * @param query
	 * 		the query
	 * @param currentPage
	 * 		the current page
	 * @param pageSize
	 * 		the page size
	 * @param sort
	 * 		the sort
	 * @param fields
	 * 		the fields
	 * @return the product search page ws dto
	 */
	@Cacheable(
			value = "productSearchCache",
			key = "T(de.hybris.platform.commercewebservicescommons.cache.CommerceCacheKeyGenerator)"
					+ ".generateKey(false,true,'DTO',#query,#currentPage,#pageSize,#sort,#fields)")
	public ProductSearchPageWsDTO searchProducts(final String query, final int currentPage, final int pageSize, final String sort,
			final String fields)
	{
		final ProductSearchPageData<SearchStateData, ProductData> sourceResult = searchProducts(query, currentPage, pageSize,
				sort);
		removePriceRangeIfEmpty(sourceResult);
		if (sourceResult instanceof ProductCategorySearchPageData)
		{
			return dataMapper.map(sourceResult, ProductCategorySearchPageWsDTO.class, fields);
		}

		return dataMapper.map(sourceResult, ProductSearchPageWsDTO.class, fields);
	}

	/**
	 * Search products product search page data.
	 *
	 * @param query
	 * 		the query
	 * @param currentPage
	 * 		the current page
	 * @param pageSize
	 * 		the page size
	 * @param sort
	 * 		the sort
	 * @return the product search page data
	 */
	@Cacheable(
			value = "productSearchCache",
			key = "T(de.hybris.platform.commercewebservicescommons.cache.CommerceCacheKeyGenerator)"
					+ ".generateKey(false,true,'Data',#query,#currentPage,#pageSize,#sort)")
	public ProductSearchPageData<SearchStateData, ProductData> searchProducts(final String query, final int currentPage,
			final int pageSize, final String sort)
	{
		final SolrSearchQueryData searchQueryData = searchQueryCodec.decodeQuery(query);
		final PageableData pageable = createPageableData(currentPage, pageSize, sort);
		final ProductSearchPageData<SearchStateData, ProductData> sourceResult =
				productSearchFacade.textSearch(solrSearchStateConverter.convert(searchQueryData), pageable);
		return sourceResult;
	}

	protected void removePriceRangeIfEmpty(final ProductSearchPageData<?, ProductData> searchResult)
	{
		searchResult.getResults().stream().forEach(resultItem ->
		{
			if (resultItem.getPriceRange() == null
				|| (resultItem.getPriceRange().getMaxPrice() == null
					&& resultItem.getPriceRange().getMinPrice() == null))
			{
				resultItem.setPriceRange(null);
			}
		});
	}
}
