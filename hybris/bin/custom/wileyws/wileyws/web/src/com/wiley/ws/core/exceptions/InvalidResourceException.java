/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.ws.core.exceptions;

import javax.servlet.ServletException;


/**
 * The type Invalid resource exception.
 *
 * @author KKW
 */
public class InvalidResourceException extends ServletException
{
	/**
	 * Instantiates a new Invalid resource exception.
	 *
	 * @param message
	 */
	public InvalidResourceException(final String message)
	{
		super(message);
	}
}
