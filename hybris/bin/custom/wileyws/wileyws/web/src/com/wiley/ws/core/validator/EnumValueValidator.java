/**
 *
 */
package com.wiley.ws.core.validator;

import de.hybris.platform.core.HybrisEnumValue;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


/**
 * Validates if provided string is valid {@link HybrisEnumValue} value;
 */
public class EnumValueValidator implements Validator
{
	private static final String MESSAGE = "enum.invalidValue";

	/**
	 * The constant DEFAULT_MESSAGE.
	 */
	public static final String DEFAULT_MESSAGE = "[{0}] is not valid value for [{1}]";

	private EnumerationService enumerationService;

	private final String enumClass;

	/**
	 * Instantiates a new Enum value validator.
	 *
	 * @param enumClass
	 * 		the enum class
	 */
	public EnumValueValidator(final String enumClass)
	{
		this.enumClass = enumClass;
	}

	/**
	 * Supports boolean.
	 *
	 * @param paramClass
	 * 		the param class
	 * @return the boolean
	 */
	@Override
	public boolean supports(final Class<?> paramClass)
	{
		return String[].class == paramClass;
	}


	/**
	 * Validate.
	 *
	 * @param o
	 * 		the o
	 * @param errors
	 * 		the errors
	 */
	@Override
	public void validate(final Object o, final Errors errors)
	{
		Assert.notNull(errors, "Errors object must not be null");
		final String[] param = (String[]) o;

		for (final String v : param)
		{
			validate(v, errors);
		}
	}

	private void validate(final String value, final Errors errors)
	{
		Object result;
		try
		{
			result = enumerationService.getEnumerationValue(enumClass, value);
		}
		catch (final UnknownIdentifierException e)
		{
			result = null;
		}
		if (result == null)
		{
			errors.reject(MESSAGE, new String[] { value, enumClass }, DEFAULT_MESSAGE);
		}

	}

	/**
	 * Sets enumeration service.
	 *
	 * @param enumerationService
	 * 		the enumeration service
	 */
	@Required
	public void setEnumerationService(final EnumerationService enumerationService)
	{
		this.enumerationService = enumerationService;
	}

}
