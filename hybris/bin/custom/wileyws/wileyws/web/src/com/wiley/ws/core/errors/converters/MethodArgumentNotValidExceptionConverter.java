package com.wiley.ws.core.errors.converters;

import de.hybris.platform.webservicescommons.dto.error.ErrorWsDTO;

import java.util.List;

import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;


/**
 * Created by Georgii_Gavrysh on 9/2/2016.
 */
public class MethodArgumentNotValidExceptionConverter extends WileywsValidationErrorConverter
{
	@Override
	public boolean supports(final Class clazz)
	{
		return MethodArgumentNotValidException.class.isAssignableFrom(clazz);
	}

	@Override
	public void populate(final Object obj, final List<ErrorWsDTO> errorWsDTOs)
	{
		final BindingResult bindingResult = ((MethodArgumentNotValidException) obj).getBindingResult();
		if (bindingResult != null && super.supports(bindingResult.getClass()))
		{
			super.populate(bindingResult, errorWsDTOs);
		}
	}
}
