/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.ws.core.v2.controller;

import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceSaveCartException;
import de.hybris.platform.commerceservices.search.pagedata.PaginationData;
import de.hybris.platform.webservicescommons.cache.CacheControl;
import de.hybris.platform.webservicescommons.cache.CacheControlDirective;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonProcessingException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.wiley.core.enums.OrderType;
import com.wiley.integrations.order.dto.OrderEntryWsDTO;
import com.wiley.ws.core.abstractorderentry.WileyWsAbstractOrderEntryFacade;
import com.wiley.ws.core.cart.WileyasWsCartFacade;
import com.wiley.ws.core.cart.dto.CreateCartEntryRequestWsDTO;
import com.wiley.ws.core.cart.dto.CreateCartRequestWsDTO;
import com.wiley.ws.core.cart.dto.WileyCartWsDTO;


/**
 * The type Carts controller.
 *
 * @pathparam userId User identifier or one of the literals below : <ul> <li>'current' for currently authenticated user</li>
 * <li>'anonymous' for anonymous user</li> </ul>
 * @pathparam cartId Cart identifier <ul> <li>cart code for logged in user</li> <li>cart guid for anonymous user</li>
 * <li>'current' for the last modified cart</li> </ul>
 * @pathparam entryNumber Entry number. Zero-based numbering.
 * @pathparam promotionId Promotion identifier (code)
 * @pathparam voucherId Voucher identifier (code)
 */
@Controller
@RequestMapping(value = "/{baseSiteId}/users/{userId}/carts")
@CacheControl(directive = CacheControlDirective.NO_CACHE)
public class CartsController extends BaseCommerceController
{
	private static final Logger LOG = Logger.getLogger(BaseCommerceController.class);
	private static final long DEFAULT_PRODUCT_QUANTITY = 1;
	@Resource(name = "wileyasWsCartFacade")
	private WileyasWsCartFacade wileyasWsCartFacade;
	@Resource
	private Validator createCartRequestWsDTOValidator;
	@Resource
	private WileyWsAbstractOrderEntryFacade wileyWsAbstractOrderEntryFacade;

	/**
	 * Lists all customer carts. Allowed only for non-anonymous users.
	 *
	 * @param fieldsSetLevel
	 * 		the fields
	 * @param type
	 * 		the type
	 * @return All customer carts
	 * @formparam savedCartsOnly optional parameter. If the parameter is provided and its value is true only saved carts are
	 * returned.
	 * @formparam currentPage optional pagination parameter in case of savedCartsOnly == true. Default value 0.
	 * @formparam pageSize optional {@link PaginationData} parameter in case of savedCartsOnly == true. Default value 20.
	 * @formparam sort optional sort criterion in case of savedCartsOnly == true. No default value.
	 * @queryparam fields Response configuration (list of fields, which should be returned in response).
	 */
	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public List<WileyCartWsDTO> getCarts(
			@RequestParam(required = false, defaultValue = DEFAULT_FIELD_SET) final String fieldsSetLevel,
			@RequestParam(required = false) final String type) throws JsonProcessingException
	{
		OrderType cartType = StringUtils.isNotBlank(type) ? OrderType.valueOf(type) : null;
		Collection<WileyCartWsDTO> carts = wileyasWsCartFacade.getUserCartsForType(cartType);
		return carts.stream().map(cart -> dataMapper.map(cart, WileyCartWsDTO.class, fieldsSetLevel))
				.collect(Collectors.toList());
	}

	/**
	 * Returns the cart with a given identifier.
	 *
	 * @param fieldsSetLevel
	 * 		level of details in response
	 * @return Details of cart and it's entries
	 * @queryparam fields Response configuration (list of fields, which should be returned in response)
	 */
	@RequestMapping(value = "/{cartId}", method = RequestMethod.GET)
	@ResponseBody
	public WileyCartWsDTO getCart(
			@PathVariable final String cartId,
			@RequestParam(required = false, defaultValue = DEFAULT_FIELD_SET) final String fieldsSetLevel)
	{
		final WileyCartWsDTO cart = wileyasWsCartFacade.getUserCart(cartId);
		return dataMapper.map(cart, WileyCartWsDTO.class, fieldsSetLevel);
	}

	/**
	 * Creates a new cart of a specified type
	 *
	 * @param createCartRequestWsDTO
	 * 		Request body parameter (DTO in xml or json format) which contains details like cart type, country etc.
	 * @param fields
	 * 		the fields
	 * @param enforceSingleCartCreation
	 * 		if this flag is set to true, Hybris will guarantee that only one cart with the provided
	 * 		in the request body type will exist for this customer and this site.
	 * @param autoUserCreationStrategy
	 * 		If this header is set and Hybris did not found the user with the provided in the path identifier,
	 * 		Hybris will try to create a new customer with the specified identifier using the strategy provided
	 * 		in this header. Only one strategy is supported for Phase 4: ALM.
	 * @return Created cart data
	 * @throws CommerceSaveCartException
	 */
	@RequestMapping(method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	@ResponseBody
	public WileyCartWsDTO createCart(@Valid @RequestBody final CreateCartRequestWsDTO createCartRequestWsDTO,
			@RequestHeader(required = false, value = "Fields-Set-Level", defaultValue = DEFAULT_FIELD_SET) final String fields,
			@RequestHeader(required = false, value = "Enforce-Single-Cart-Per-Type",
					defaultValue = "false") final boolean enforceSingleCartCreation,
			@RequestHeader(required = false, value = "User-Auto-Creation-Strategy") final String autoUserCreationStrategy)
			throws CommerceSaveCartException
	{
		validate(createCartRequestWsDTO, "createCartRequestWsDTO", createCartRequestWsDTOValidator);
		if (enforceSingleCartCreation)
		{
			wileyasWsCartFacade.ensureSingleCart(createCartRequestWsDTO, false);
		}
		WileyCartWsDTO responseWsDTO = wileyasWsCartFacade.createCart(createCartRequestWsDTO);
		if (enforceSingleCartCreation)
		{
			//we need to execute this second time to avoid concurrent requests
			wileyasWsCartFacade.ensureSingleCart(createCartRequestWsDTO, true);
		}

		return dataMapper.map(responseWsDTO, WileyCartWsDTO.class, fields);
	}

	/**
	 * @param baseSiteId
	 * 		{@link String} parameter of the baseSite id.
	 * @param cartId
	 * 		{@link String} parameter of the cart id.
	 */
	@RequestMapping(value = "/{cartId}", method = RequestMethod.DELETE)
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void deleteCart(@PathVariable final String baseSiteId, @PathVariable final String userId,
			@PathVariable final String cartId)
	{
		wileyasWsCartFacade.deleteUserCart(baseSiteId, userId, cartId);
	}


	/**
	 * Adds an entry to the cart.
	 *
	 * @param cartId
	 * 		the cart id
	 * @param cartEntry
	 * 		the new entry of the cart
	 * @return Added oreder entry
	 * @throws CommerceCartModificationException
	 * @headerparam Enforce Unique Business Key
	 * @headerparam the fields
	 */
	@RequestMapping(value = "/{cartId}/entries", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity addCartEntry(@PathVariable final String cartId,
			@Valid @RequestBody(required = true) final CreateCartEntryRequestWsDTO cartEntry,
			@RequestHeader(required = false, value = "Fields-Set-Level",
					defaultValue = DEFAULT_FIELD_SET) final String fields) throws CommerceCartModificationException
	{
		if (cartEntry.getBusinessKey() != null)
		{
			final Optional<OrderEntryWsDTO> existingOrderEntryWsDTO = wileyWsAbstractOrderEntryFacade.getEntryForBusinessKey(
					cartEntry.getBusinessKey());

			if (existingOrderEntryWsDTO.isPresent())
			{
				return new ResponseEntity<>(dataMapper.map(existingOrderEntryWsDTO.get(), OrderEntryWsDTO.class, fields),
						HttpStatus.CONFLICT);
			}
		}

		OrderEntryWsDTO orderEntryWsDTO = wileyasWsCartFacade.addEntryToCart(cartId, cartEntry);
		return new ResponseEntity<>(dataMapper.map(orderEntryWsDTO, OrderEntryWsDTO.class, fields), HttpStatus.CREATED);
	}

}
