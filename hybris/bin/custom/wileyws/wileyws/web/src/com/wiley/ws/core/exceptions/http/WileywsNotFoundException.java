package com.wiley.ws.core.exceptions.http;

/**
 * Created by Mikhail_Asadchy on 10/13/2016.
 */
public class WileywsNotFoundException extends RuntimeException
{
	public WileywsNotFoundException()
	{
	}

	public WileywsNotFoundException(final String message)
	{
		super(message);
	}

	public WileywsNotFoundException(final String message, final Throwable cause)
	{
		super(message, cause);
	}

	public WileywsNotFoundException(final Throwable cause)
	{
		super(cause);
	}

	public WileywsNotFoundException(final String message, final Throwable cause,
			final boolean enableSuppression, final boolean writableStackTrace)
	{
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
