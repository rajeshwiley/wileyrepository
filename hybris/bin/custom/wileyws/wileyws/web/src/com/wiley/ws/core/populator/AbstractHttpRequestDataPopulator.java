/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package com.wiley.ws.core.populator;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;


/**
 * The type Abstract http request data populator.
 *
 * @author krzysztof.kwiatosz
 */
public abstract class AbstractHttpRequestDataPopulator
{

	/**
	 * Update string value from request string.
	 *
	 * @param request
	 * 		the request
	 * @param paramName
	 * 		the param name
	 * @param defaultValue
	 * 		the default value
	 * @return the string
	 */
	protected String updateStringValueFromRequest(final HttpServletRequest request, final String paramName,
			final String defaultValue)
	{
		final String requestParameterValue = getRequestParameterValue(request, paramName);
		if ("".equals(requestParameterValue))
		{
			return null;
		}
		return StringUtils.defaultIfBlank(requestParameterValue, defaultValue);
	}

	/**
	 * Update boolean value from request boolean.
	 *
	 * @param request
	 * 		the request
	 * @param paramName
	 * 		the param name
	 * @param defaultValue
	 * 		the default value
	 * @return the boolean
	 */
	protected boolean updateBooleanValueFromRequest(final HttpServletRequest request, final String paramName,
			final boolean defaultValue)
	{
		final String booleanString = updateStringValueFromRequest(request, paramName, null);
		if (booleanString == null)
		{
			return defaultValue;
		}
		return Boolean.parseBoolean(booleanString);
	}

	/**
	 * Update double value from request double.
	 *
	 * @param request
	 * 		the request
	 * @param paramName
	 * 		the param name
	 * @param defaultValue
	 * 		the default value
	 * @return the double
	 */
	protected Double updateDoubleValueFromRequest(final HttpServletRequest request, final String paramName,
			final Double defaultValue)
	{
		final String booleanString = updateStringValueFromRequest(request, paramName, null);
		if (booleanString == null)
		{
			return defaultValue;
		}
		return Double.valueOf(booleanString);
	}

	/**
	 * Gets request parameter value.
	 *
	 * @param request
	 * 		the request
	 * @param paramName
	 * 		the param name
	 * @return the request parameter value
	 */
	protected String getRequestParameterValue(final HttpServletRequest request, final String paramName)
	{
		return request.getParameter(paramName);
	}
}
