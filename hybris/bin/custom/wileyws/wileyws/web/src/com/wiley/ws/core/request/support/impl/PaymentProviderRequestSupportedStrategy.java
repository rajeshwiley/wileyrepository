/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package com.wiley.ws.core.request.support.impl;

import de.hybris.platform.commerceservices.order.CommercePaymentProviderStrategy;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Required;

import com.wiley.ws.core.exceptions.UnsupportedRequestException;
import com.wiley.ws.core.request.support.RequestSupportedStrategy;


/**
 * Implementation for {@link RequestSupportedStrategy} which checks if request is supported based on payment provider
 * name
 */
public class PaymentProviderRequestSupportedStrategy implements RequestSupportedStrategy
{
	private CommercePaymentProviderStrategy paymentProviderStrategy;

	private Map<String, List<String>> unsupportedRequestMap = new HashMap();

	/**
	 * Is request supported boolean.
	 *
	 * @param requestId
	 * 		the request id
	 * @return the boolean
	 */
	@Override
	public boolean isRequestSupported(final String requestId)
	{
		final String paymentProvider = paymentProviderStrategy.getPaymentProvider();
		if (paymentProvider != null)
		{
			final List<String> unsupportedRequests = unsupportedRequestMap.get(paymentProvider);
			if (unsupportedRequests != null)
			{
				return !unsupportedRequests.contains(requestId);
			}
		}
		return true;
	}

	/**
	 * Check if request supported.
	 *
	 * @param requestId
	 * 		the request id
	 * @throws UnsupportedRequestException
	 * 		the unsupported request exception
	 */
	@Override
	public void checkIfRequestSupported(final String requestId) throws UnsupportedRequestException
	{
		final String paymentProvider = paymentProviderStrategy.getPaymentProvider();
		if (paymentProvider != null)
		{
			final List<String> unsupportedRequests = unsupportedRequestMap.get(paymentProvider);
			if (unsupportedRequests != null)
			{
				if (unsupportedRequests.contains(requestId))
				{
					throw new UnsupportedRequestException(
							"This request is not supported for payment provider : " + paymentProvider);
				}
			}
		}
	}

	/**
	 * Gets payment provider strategy.
	 *
	 * @return the payment provider strategy
	 */
	public CommercePaymentProviderStrategy getPaymentProviderStrategy()
	{
		return paymentProviderStrategy;
	}

	/**
	 * Sets payment provider strategy.
	 *
	 * @param paymentProviderStrategy
	 * 		the payment provider strategy
	 */
	@Required
	public void setPaymentProviderStrategy(final CommercePaymentProviderStrategy paymentProviderStrategy)
	{
		this.paymentProviderStrategy = paymentProviderStrategy;
	}

	/**
	 * Gets unsupported request map.
	 *
	 * @return the unsupported request map
	 */
	public Map<String, List<String>> getUnsupportedRequestMap()
	{
		return unsupportedRequestMap;
	}

	/**
	 * Sets unsupported request map.
	 *
	 * @param unsupportedRequestMap
	 * 		the unsupported request map
	 */
	public void setUnsupportedRequestMap(final Map<String, List<String>> unsupportedRequestMap)
	{
		this.unsupportedRequestMap = unsupportedRequestMap;
	}
}
