/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package com.wiley.ws.core.util.ws.impl;

import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryTermData;

import java.util.ArrayList;
import java.util.List;

import com.wiley.ws.core.util.ws.SearchQueryCodec;


/**
 * The type Default search query codec.
 */
public class DefaultSearchQueryCodec implements SearchQueryCodec<SolrSearchQueryData>
{
	/**
	 * Decode query solr search query data.
	 *
	 * @param queryString
	 * 		the query string
	 * @return the solr search query data
	 */
	@Override
	public SolrSearchQueryData decodeQuery(final String queryString)
	{
		final SolrSearchQueryData searchQuery = new SolrSearchQueryData();
		final List<SolrSearchQueryTermData> filters = new ArrayList<SolrSearchQueryTermData>();

		if (queryString == null)
		{
			return searchQuery;
		}

		final String[] parts = queryString.split("\\|");
		for (int i = 0; i < parts.length; i++)
		{
			if (i == 0)
			{
				searchQuery.setFreeTextSearch(parts[i]);
			}
			else if (i == 1)
			{
				searchQuery.setSort(parts[i]);
			}
			else
			{
				final SolrSearchQueryTermData term = new SolrSearchQueryTermData();
				final String[] filter = parts[i].split(":");
				term.setKey(filter[0]);
				term.setValue(filter[1]);
				filters.add(term);
			}
		}

		searchQuery.setFilterTerms(filters);

		return searchQuery;
	}

	/**
	 * Encode query string.
	 *
	 * @param searchQueryData
	 * 		the search query data
	 * @return the string
	 */
	@Override
	public String encodeQuery(final SolrSearchQueryData searchQueryData)
	{
		if (searchQueryData == null)
		{
			return null;
		}

		final StringBuilder builder = new StringBuilder();
		builder.append((searchQueryData.getFreeTextSearch() == null) ? "" : searchQueryData.getFreeTextSearch());


		if (searchQueryData.getSort() != null || (searchQueryData.getFilterTerms() != null && !searchQueryData.getFilterTerms()
				.isEmpty()))
		{
			builder.append("|");
			builder.append((searchQueryData.getSort() == null) ? "" : searchQueryData.getSort());
		}

		final List<SolrSearchQueryTermData> terms = searchQueryData.getFilterTerms();
		if (terms != null && !terms.isEmpty())
		{
			for (final SolrSearchQueryTermData term : searchQueryData.getFilterTerms())
			{
				builder.append("|");
				builder.append(term.getKey());
				builder.append(":");
				builder.append(term.getValue());
			}
		}

		//URLEncode?
		return builder.toString();
	}
}
