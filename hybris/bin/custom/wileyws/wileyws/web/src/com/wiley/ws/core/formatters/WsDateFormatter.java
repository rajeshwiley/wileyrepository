/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package com.wiley.ws.core.formatters;

import java.util.Date;


/**
 * The interface Ws date formatter.
 *
 * @author michal.flasinski
 */
public interface WsDateFormatter
{
	/**
	 * To date date.
	 *
	 * @param timestamp
	 * 		the timestamp
	 * @return the date
	 */
	Date toDate(String timestamp);

	/**
	 * To string string.
	 *
	 * @param date
	 * 		the date
	 * @return the string
	 */
	String toString(Date date);
}
