package com.wiley.ws.core.errors.converters;

import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.webservicescommons.dto.error.ErrorWsDTO;
import de.hybris.platform.webservicescommons.errors.converters.ExceptionConverter;

import java.util.List;

import javax.annotation.Resource;

import com.wiley.core.jalo.user.WileyUserManager;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


public class WileywsDuplicateUidExceptionConverter extends ExceptionConverter
{

	public static final String EXCEPTION = "Exception";
	public static final String ERROR = "Error";

	@Resource
	private WileyUserManager userManager;

	@Override
	public boolean supports(final Class clazz)
	{
		return DuplicateUidException.class.isAssignableFrom(clazz);
	}

	@Override
	public void populate(final Object exception, final List<ErrorWsDTO> webserviceErrorList)
	{
		validateParameterNotNullStandardMessage("exception", exception);
		validateParameterNotNullStandardMessage("webserviceErrorList", webserviceErrorList);

		DuplicateUidException duplicateUidException = (DuplicateUidException) exception;

		ErrorWsDTO error = createError(duplicateUidException);

		webserviceErrorList.add(error);
	}

	private ErrorWsDTO createError(final DuplicateUidException duplicateUidException)
	{
		ErrorWsDTO error = new ErrorWsDTO();
		error.setType(duplicateUidException.getClass().getSimpleName().replace(EXCEPTION, ERROR));
		error.setMessage(this.filterExceptionMessage(duplicateUidException));
		final String customerID = userManager.getCustomerByLogin(duplicateUidException.getMessage()).getCustomerID();
		error.setCustomerId(customerID);
		return error;
	}
}
