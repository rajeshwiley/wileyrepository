/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package com.wiley.ws.core.conv;

import java.util.Date;
import java.util.Map;
import java.util.Set;

import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;
import org.springframework.util.Assert;

import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.ExtendedHierarchicalStreamWriter;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;


/**
 * The type Oauth 2 access token converter.
 */
public class Oauth2AccessTokenConverter extends AbstractRedirectableConverter
{
	/**
	 * Can convert boolean.
	 *
	 * @param type
	 * 		the type
	 * @return the boolean
	 */
	@Override
	public boolean canConvert(final Class type)
	{
		return type == getConvertedClass();
	}

	/**
	 * Formatted value string.
	 *
	 * @param value
	 * 		the value
	 * @return the string
	 */
	protected String formattedValue(final String value)
	{
		return value;
	}

	/**
	 * Marshal.
	 *
	 * @param source
	 * 		the source
	 * @param writerOrig
	 * 		the writer orig
	 * @param context
	 * 		the context
	 */
	@Override
	public void marshal(final Object source, final HierarchicalStreamWriter writerOrig, final MarshallingContext context)
	{
		final OAuth2AccessToken token = (OAuth2AccessToken) source;
		final ExtendedHierarchicalStreamWriter writer = (ExtendedHierarchicalStreamWriter) writerOrig.underlyingWriter();


		writer.startNode(OAuth2AccessToken.ACCESS_TOKEN, String.class);
		writer.setValue(formattedValue(token.getValue()));
		writer.endNode();

		writer.startNode(OAuth2AccessToken.TOKEN_TYPE, String.class);
		writer.setValue(formattedValue(token.getTokenType()));
		writer.endNode();

		final OAuth2RefreshToken refreshToken = token.getRefreshToken();
		if (refreshToken != null)
		{
			writer.startNode(OAuth2AccessToken.REFRESH_TOKEN, String.class);
			writer.setValue(formattedValue(refreshToken.getValue()));
			writer.endNode();

		}
		final Date expiration = token.getExpiration();
		if (expiration != null)
		{
			final long now = System.currentTimeMillis();
			writer.startNode(OAuth2AccessToken.EXPIRES_IN, Integer.class);
			writer.setValue(String.valueOf((expiration.getTime() - now) / 1000));
			writer.endNode();
		}
		final Set<String> scope = token.getScope();
		if (scope != null && !scope.isEmpty())
		{
			final StringBuffer scopes = new StringBuffer();
			for (final String s : scope)
			{
				Assert.hasLength(s, "Scopes cannot be null or empty. Got " + scope);
				scopes.append(s);
				scopes.append(' ');
			}

			writer.startNode(OAuth2AccessToken.SCOPE, String.class);
			writer.setValue(formattedValue(scopes.substring(0, scopes.length() - 1)));
			writer.endNode();
		}
		final Map<String, Object> additionalInformation = token.getAdditionalInformation();
		for (final Map.Entry<String, Object> entry : additionalInformation.entrySet())
		{
			writer.startNode(entry.getKey(), String.class);
			writer.setValue(formattedValue(String.valueOf(entry.getValue())));
			writer.endNode();
		}
	}

	/**
	 * Unmarshal object.
	 *
	 * @param reader
	 * 		the reader
	 * @param context
	 * 		the context
	 * @return the object
	 */
	@Override
	public Object unmarshal(final HierarchicalStreamReader reader, final UnmarshallingContext context)
	{
		return getTargetConverter().unmarshal(reader, context);
	}

	/**
	 * Gets converted class.
	 *
	 * @return the converted class
	 */
	@Override
	public Class getConvertedClass()
	{
		return DefaultOAuth2AccessToken.class;
	}
}
