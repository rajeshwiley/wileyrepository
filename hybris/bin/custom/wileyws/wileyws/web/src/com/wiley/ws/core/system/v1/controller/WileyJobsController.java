package com.wiley.ws.core.system.v1.controller;

import de.hybris.platform.cronjob.model.CronJobModel;

import javax.annotation.Resource;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.wiley.core.servicelayer.cronjob.WileyCronJobService;
import com.wiley.ws.core.data.JobDTO;
import com.wiley.ws.core.exceptions.http.WileywsNotFoundException;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
@Controller
@RequestMapping(value = "/jobs")
public class WileyJobsController
{
	private static final String JOBS_PATH = "/wileyws/system/v1/jobs/";

	@Resource
	private WileyCronJobService cronJobService;

	@Secured("ROLE_ADMINGROUP")
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public JobDTO getCronJobStatusByPk(@PathVariable final Long id)
	{
		final CronJobModel cronJobModel = cronJobService.getCronJobByPk(id);
		if (cronJobModel != null)
		{
			final JobDTO jobDto = new JobDTO();
			jobDto.setHref(JOBS_PATH + cronJobModel.getPk().toString());
			jobDto.setId(cronJobModel.getPk().getLongValue());
			jobDto.setCode(cronJobModel.getCode());
			jobDto.setStatus(cronJobModel.getStatus());
			jobDto.setCreated(cronJobModel.getCreationtime());

			return jobDto;
		}

		throw new WileywsNotFoundException("Job with id " + id + " doesn't exist");
	}
}
