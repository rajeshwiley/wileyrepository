/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package com.wiley.ws.core.auth;

import java.util.Collection;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;


/**
 * The type Guest authentication token.
 */
public final class GuestAuthenticationToken extends AbstractAuthenticationToken
{
	private final String email;

	/**
	 * Instantiates a new Guest authentication token.
	 *
	 * @param email
	 * 		the email
	 * @param authorities
	 * 		the authorities
	 */
	public GuestAuthenticationToken(final String email, final Collection<? extends GrantedAuthority> authorities)
	{
		super(authorities);
		this.email = email;
		setAuthenticated(true);
	}

	/**
	 * Gets credentials.
	 *
	 * @return the credentials
	 */
	@Override
	public Object getCredentials()
	{
		return null;
	}

	/**
	 * Gets principal.
	 *
	 * @return the principal
	 */
	@Override
	public Object getPrincipal()
	{
		return email;
	}
}
