package com.wiley.ws.core.v2.resolver;

import com.wiley.core.wiley.restriction.exception.ProductNotVisibleException;
import com.wiley.core.wileyas.order.exception.BusinessKeyMissingException;
import com.wiley.integrations.order.dto.OrderEntryWsDTO;
import com.wiley.integrations.order.exception.DuplicateEntryBusinessKeyException;
import com.wiley.ws.core.exceptions.carts.WileyasSaveCartException;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.webservicescommons.resolver.RestHandlerExceptionResolver;
import de.hybris.platform.webservicescommons.util.YSanitizer;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class WileyasRestHandlerExceptionResolver extends RestHandlerExceptionResolver
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyasRestHandlerExceptionResolver.class);
	private Converter<AbstractOrderEntryModel, OrderEntryWsDTO> wileyOrderEntryWsConverter;

	@Override
	protected ModelAndView doResolveException(final HttpServletRequest request, final HttpServletResponse response,
			final Object handler,
			final Exception ex)
	{
		final ModelAndView result;

		if (ex instanceof ModelSavingException && ex.getCause() != null && ex.getCause()
				.getCause() instanceof DuplicateKeyException)
		{
			result = super.doResolveException(request, response, handler, (Exception) ex.getCause().getCause());
		}
		else if ((ex instanceof CommerceCartModificationException
				&& ex.getCause() != null)
				|| ex instanceof WileyasSaveCartException)
		{
			final Throwable cause = ex.getCause();
			if (cause instanceof BusinessKeyMissingException || cause instanceof ProductNotVisibleException)
			{
				result = super.doResolveException(request, response, handler, (Exception) cause);
			}
			else if (ex instanceof WileyasSaveCartException)
			{
				result = doResolveCustomException(request, response, ex,
						((WileyasSaveCartException) ex).getExistingCart());
			}
			else
			{
				result = super.doResolveException(request, response, handler, ex);
			}
		}
		else if (ex instanceof DuplicateEntryBusinessKeyException)
		{
			result = doResolveCustomException(request, response, ex,
					((DuplicateEntryBusinessKeyException) ex).getOrderEntry());
		}
		else
		{
			result = super.doResolveException(request, response, handler, ex);
		}

		return result;
	}

	protected ModelAndView doResolveCustomException(final HttpServletRequest request,
			final HttpServletResponse response, final Exception ex, final Object cause)
	{
		response.setStatus(this.calculateStatusFromException(ex));
		LOG.warn(String.format("Detected exception [%s]: %s", ex.getClass().getName(), YSanitizer.sanitize(ex.getMessage())));
		if (this.shouldDisplayStack(ex))
		{
			LOG.error(ExceptionUtils.getStackTrace(ex));
		}

		final ServletServerHttpRequest inputMessage = new ServletServerHttpRequest(request);
		final ServletServerHttpResponse outputMessage = new ServletServerHttpResponse(response);

		try
		{
			return this.writeWithMessageConverters(cause, inputMessage, outputMessage);
		}
		catch (Exception e)
		{
			LOG.error("Handling of [" + ex.getClass().getName() + "] resulted in Exception", e);
		}
		finally
		{
			outputMessage.close();
		}

		return null;
	}

	public Converter<AbstractOrderEntryModel, OrderEntryWsDTO> getWileyOrderEntryWsConverter()
	{
		return wileyOrderEntryWsConverter;
	}

	@Required
	public void setWileyOrderEntryWsConverter(
			final Converter<AbstractOrderEntryModel, OrderEntryWsDTO> wileyOrderEntryWsConverter)
	{
		this.wileyOrderEntryWsConverter = wileyOrderEntryWsConverter;
	}
}
