/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package com.wiley.ws.core.conv;

import de.hybris.platform.commercefacades.product.data.ReviewData;

import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.wiley.ws.core.product.data.ReviewDataList;


/**
 * Specific converter for a {@link ReviewDataList} object.
 */
public class ReviewDataListConverter extends AbstractRedirectableConverter
{
	/**
	 * Can convert boolean.
	 *
	 * @param type
	 * 		the type
	 * @return the boolean
	 */
	@Override
	public boolean canConvert(final Class type)
	{
		return type == getConvertedClass();
	}

	/**
	 * Marshal.
	 *
	 * @param source
	 * 		the source
	 * @param writer
	 * 		the writer
	 * @param context
	 * 		the context
	 */
	@Override
	public void marshal(final Object source, final HierarchicalStreamWriter writer, final MarshallingContext context)
	{
		final ReviewDataList reviews = (ReviewDataList) source;

		for (final ReviewData rd : reviews.getReviews())
		{
			writer.startNode("review");
			context.convertAnother(rd);
			writer.endNode();
		}
	}

	/**
	 * Unmarshal object.
	 *
	 * @param reader
	 * 		the reader
	 * @param context
	 * 		the context
	 * @return the object
	 */
	@Override
	public Object unmarshal(final HierarchicalStreamReader reader, final UnmarshallingContext context)
	{
		return getTargetConverter().unmarshal(reader, context);
	}

	/**
	 * Gets converted class.
	 *
	 * @return the converted class
	 */
	@Override
	public Class getConvertedClass()
	{
		return ReviewDataList.class;
	}


}
