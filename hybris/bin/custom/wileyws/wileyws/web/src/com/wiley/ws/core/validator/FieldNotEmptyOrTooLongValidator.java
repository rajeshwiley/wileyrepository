/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package com.wiley.ws.core.validator;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


/**
 * Implementation of {@link org.springframework.validation.Validator} that validates one specific string property
 * specified by {@link #fieldPath} property in any object if it is not null, not blank and shorter than maxLength
 */
public class FieldNotEmptyOrTooLongValidator implements Validator
{
	private static final String FIELD_REQUIRED_AND_NOT_TOO_LONG_MESSAGE_ID = "field.requiredAndNotTooLong";

	private String fieldPath;
	private int maxLength;
	private String errorMessageId;

	/**
	 * Supports boolean.
	 *
	 * @param clazz
	 * 		the clazz
	 * @return the boolean
	 */
	@Override
	public boolean supports(final Class clazz)
	{
		return true;
	}

	/**
	 * Validate.
	 *
	 * @param object
	 * 		the object
	 * @param errors
	 * 		the errors
	 */
	@Override
	public void validate(final Object object, final Errors errors)
	{
		Assert.notNull(errors, "Errors object must not be null");
		final String fieldValue = (String) errors.getFieldValue(fieldPath);
		final String resultErrorMessageId = errorMessageId != null ? errorMessageId : FIELD_REQUIRED_AND_NOT_TOO_LONG_MESSAGE_ID;

		if (StringUtils.isBlank(fieldValue) || StringUtils.length(fieldValue) > maxLength)
		{
			errors.rejectValue(fieldPath, resultErrorMessageId, new String[] { String.valueOf(maxLength) }, null);
		}
	}

	/**
	 * Gets field path.
	 *
	 * @return the field path
	 */
	public String getFieldPath()
	{
		return fieldPath;
	}

	/**
	 * Sets field path.
	 *
	 * @param fieldPath
	 * 		the field path
	 */
	@Required
	public void setFieldPath(final String fieldPath)
	{
		this.fieldPath = fieldPath;
	}


	/**
	 * Gets max length.
	 *
	 * @return the max length
	 */
	public int getMaxLength()
	{
		return maxLength;
	}

	/**
	 * Sets max length.
	 *
	 * @param maxLength
	 * 		the max length
	 */
	@Required
	public void setMaxLength(final int maxLength)
	{
		this.maxLength = maxLength;
	}

	/**
	 * Gets error message id.
	 *
	 * @return the error message id
	 */
	public String getErrorMessageId()
	{
		return errorMessageId;
	}

	/**
	 * Sets error message id.
	 *
	 * @param errorMessageId
	 * 		the error message id
	 */
	public void setErrorMessageId(final String errorMessageId)
	{
		this.errorMessageId = errorMessageId;
	}
}
