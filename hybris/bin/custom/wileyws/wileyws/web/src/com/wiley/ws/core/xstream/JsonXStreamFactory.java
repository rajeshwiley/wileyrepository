/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package com.wiley.ws.core.xstream;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.mapper.MapperWrapper;


/**
 * JSon specific {@link XStream} instances factory.
 */
public class JsonXStreamFactory extends XmlXStreamFactory
{
	private XStream jsonInstance;

	/**
	 * After properties set.
	 *
	 * @throws Exception
	 * 		the exception
	 */
	@Override
	public void afterPropertiesSet() throws Exception
	{
		jsonInstance = getObjectInternal();
	}

	/**
	 * Gets object.
	 *
	 * @return the object
	 * @throws Exception
	 * 		the exception
	 */
	@Override
	public Object getObject() throws Exception
	{
		return jsonInstance;
	}

	/**
	 * Gets object internal.
	 *
	 * @return the object internal
	 * @throws Exception
	 * 		the exception
	 */
	@Override
	protected XStream getObjectInternal() throws Exception
	{
		final XStream stream = new XStream(new com.thoughtworks.xstream.io.json.JsonHierarchicalStreamDriver())
		{
			@Override
			protected MapperWrapper wrapMapper(final MapperWrapper next)
			{
				return createMapperWrapper(next);
			}
		};

		stream.setMode(com.thoughtworks.xstream.XStream.NO_REFERENCES);
		return stream;
	}
}
