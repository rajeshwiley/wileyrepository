package com.wiley.ws.core.errors.converters;

import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.webservicescommons.dto.error.ErrorWsDTO;
import de.hybris.platform.webservicescommons.errors.converters.ExceptionConverter;

import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;
import javax.json.stream.JsonParsingException;
import javax.xml.bind.UnmarshalException;

import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;

import com.wiley.core.wileyws.exceptions.WileyWsValidationException;
import com.wiley.ws.core.product.dataintegration.common.error.ErrorSwgDTO;


/**
 * Created by Georgii_Gavrysh on 9/2/2016.
 */
public class WileywsExceptionConverter extends ExceptionConverter
{
	private static final String EXCEPTION = "Exception";
	private static final String ERROR = "Error";

	@Resource(name = "wileywsCountryRegionErrorConverter")
	private WileywsCountryRegionErrorConverter wileywsCountryRegionErrorConverter;

	@Override
	public boolean supports(final Class clazz)
	{
		return super.supports(clazz) && !MethodArgumentNotValidException.class.isAssignableFrom(clazz)
				&& !DuplicateUidException.class.isAssignableFrom(clazz);
	}

	@Override
	public void populate(final Object o, final List<ErrorWsDTO> webserviceErrorList)
	{
		final Optional<Exception> jsonParsingException = getCauseJsonParsingException(o);
		final Optional<WileyWsValidationException> wileyCountryRegionException = getCauseWileyCountryRegionException(o);
		if (jsonParsingException.isPresent())
		{
			super.populate(jsonParsingException.get(), webserviceErrorList);
		}
		if (wileyCountryRegionException.isPresent())
		{
			wileywsCountryRegionErrorConverter.populate(wileyCountryRegionException.get(), webserviceErrorList);
		}
		else if (o instanceof HttpMessageNotReadableException)
		{
			populateParsingException(webserviceErrorList, (HttpMessageNotReadableException) o);
		}
		else
		{
			super.populate(o, webserviceErrorList);
		}
	}

	private void populateParsingException(final List<ErrorWsDTO> webserviceErrorList,
			final HttpMessageNotReadableException jsonParsingException)
	{
		ErrorWsDTO error = new ErrorWsDTO();
		error.setType(jsonParsingException.getClass().getSimpleName().replace(EXCEPTION, ERROR));
		error.setMessage(this.filterExceptionMessage(jsonParsingException));
		error.setReason(ErrorSwgDTO.ReasonEnum.JSON_PARSE_ERROR.name());
		webserviceErrorList.add(error);
	}

	private Optional<Exception> getCauseJsonParsingException(final Object o)
	{
		Optional<Exception> result = Optional.empty();
		if (o instanceof HttpMessageNotReadableException)
		{
			Throwable cause = ((HttpMessageNotReadableException) o).getCause();
			if (cause != null && cause instanceof UnmarshalException)
			{
				Throwable causeLinkedException = ((UnmarshalException) cause).getLinkedException();

				if (causeLinkedException != null)
				{
					Throwable linkedExceptionCause = causeLinkedException.getCause();
					if (linkedExceptionCause instanceof JsonParsingException)
					{
						result = Optional.of((JsonParsingException) linkedExceptionCause);
					}
				}
			}
		}
		return result;
	}

	private Optional<WileyWsValidationException> getCauseWileyCountryRegionException(final Object o)
	{
		Optional<WileyWsValidationException> result = Optional.empty();
		if (o instanceof WileyWsValidationException)
		{
			result = Optional.of((WileyWsValidationException) o);
		}
		return result;
	}
}
