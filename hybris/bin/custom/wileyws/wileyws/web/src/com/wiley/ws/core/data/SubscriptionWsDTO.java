package com.wiley.ws.core.data;

import de.hybris.platform.webservicescommons.jaxb.adapters.DateAdapter;

import java.util.Date;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;



public class SubscriptionWsDTO implements java.io.Serializable
{

	@XmlJavaTypeAdapter(DateAdapter.class)
	private Date expires;

	private String code;

	private String productCode;

	public SubscriptionWsDTO()
	{

	}


	public void setExpires(final Date expires)
	{
		this.expires = expires;
	}


	public Date getExpires()
	{
		return expires;
	}


	public void setCode(final String code)
	{
		this.code = code;
	}


	public String getCode()
	{
		return code;
	}


	public void setProductCode(final String productCode)
	{
		this.productCode = productCode;
	}


	public String getProductCode()
	{
		return productCode;
	}


}
