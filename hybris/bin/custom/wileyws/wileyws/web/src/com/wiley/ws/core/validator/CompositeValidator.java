/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package com.wiley.ws.core.validator;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


/**
 * Implementation of {@link org.springframework.validation.Validator} that validates objects using composition of other
 * validators
 */
public class CompositeValidator implements Validator
{
	private Validator[] validators;

	/**
	 * Supports boolean.
	 *
	 * @param clazz
	 * 		the clazz
	 * @return the boolean
	 */
	@Override
	public boolean supports(final Class clazz)
	{
		for (final Validator v : validators)
		{
			if (v.supports(clazz))
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * Validate.
	 *
	 * @param object
	 * 		the object
	 * @param errors
	 * 		the errors
	 */
	@Override
	public void validate(final Object object, final Errors errors)
	{
		for (final Validator v : validators)
		{
			if (v.supports(object.getClass()))
			{
				v.validate(object, errors);
			}
		}
	}

	/**
	 * Get validators validator [ ].
	 *
	 * @return the validator [ ]
	 */
	public Validator[] getValidators()
	{
		return validators;
	}

	/**
	 * Sets validators.
	 *
	 * @param validators
	 * 		the validators
	 */
	@Required
	public void setValidators(final Validator[] validators)
	{
		this.validators = validators;
	}
}
