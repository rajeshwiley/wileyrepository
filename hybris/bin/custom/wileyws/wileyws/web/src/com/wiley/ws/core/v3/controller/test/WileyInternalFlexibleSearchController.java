package com.wiley.ws.core.v3.controller.test;

import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.context.annotation.Profile;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.wiley.ws.core.data.FlexibleSearchResponseWsDTO;


/**
 * This controller is used to validate saved data by flexible search query
 */
@Controller
@Profile("wileyws_test")
public class WileyInternalFlexibleSearchController
{
	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Secured("ROLE_TRUSTED_CLIENT")
	@RequestMapping(value = "/flexible-search", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public FlexibleSearchResponseWsDTO executeQuery(@RequestParam final String query)
	{
		SearchResult searchResult = flexibleSearchService.search(query);
		FlexibleSearchResponseWsDTO response = new FlexibleSearchResponseWsDTO();
		response.setResult(CollectionUtils.isNotEmpty(searchResult.getResult()));
		return response;
	}
}
