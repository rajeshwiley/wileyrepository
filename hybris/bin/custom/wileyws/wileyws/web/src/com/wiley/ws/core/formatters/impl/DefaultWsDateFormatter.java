/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package com.wiley.ws.core.formatters.impl;

import java.util.Date;

import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import com.wiley.ws.core.formatters.WsDateFormatter;


/**
 * The type Default ws date formatter.
 *
 * @author michal.flasinski
 */
public class DefaultWsDateFormatter implements WsDateFormatter
{
	private final DateTimeFormatter parser = ISODateTimeFormat.dateTimeNoMillis();

	/**
	 * To date date.
	 *
	 * @param timestamp
	 * 		the timestamp
	 * @return the date
	 */
	@Override
	public Date toDate(final String timestamp)
	{
		return parser.parseDateTime(timestamp).toDate();
	}

	/**
	 * To string string.
	 *
	 * @param date
	 * 		the date
	 * @return the string
	 */
	@Override
	public String toString(final Date date)
	{
		return parser.print(date.getTime());
	}

}
