package com.wiley.ws.core.v2.controller;

import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;
import de.hybris.platform.cms2.model.contents.components.CMSParagraphComponentModel;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.wiley.ws.core.exceptions.http.WileywsNotFoundException;
import com.wiley.ws.core.paragraph.WileyWsParagraphFacade;


@Controller
@RequestMapping(value = "/wileyb2c/paragraphcomponents")
public class WileyParagraphComponentController
{
	@Resource(name = "wileyWsParagraphFacade")
	private WileyWsParagraphFacade wileyWsParagraphFacade;

	@RequestMapping(value = "/{uid}/content", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
	@ResponseBody
	public String getParagraphContent(final HttpServletRequest request, final HttpServletResponse response,
			@PathVariable final String uid)
			throws BindException
	{
		response.setHeader("X-Frame-Options", "ALLOWALL");

		List<AbstractCMSComponentModel> cmsComponents = wileyWsParagraphFacade.getAbstractCMSComponents(uid);
		if (cmsComponents.isEmpty())
		{
			throw new WileywsNotFoundException("Could not find Paragraph Component with uid = " + uid + " on wileyb2c.");
		}
		AbstractCMSComponentModel abstractCMSComponentModel = cmsComponents.get(0);
		if (!(abstractCMSComponentModel instanceof CMSParagraphComponentModel))
		{
			throw new ClassCastException("Component " + uid + " is not a Paragraph Component");
		}
		wileyWsParagraphFacade.setFallbackLanguage(Boolean.TRUE);
		CMSParagraphComponentModel paragraphComponent = (CMSParagraphComponentModel) abstractCMSComponentModel;
		return wileyWsParagraphFacade.getParagraphContent(paragraphComponent);
	}
}
