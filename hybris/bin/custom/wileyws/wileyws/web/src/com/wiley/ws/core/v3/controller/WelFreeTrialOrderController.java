package com.wiley.ws.core.v3.controller;

import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.order.InvalidCartException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.wiley.facades.product.exception.RepeatedFreeTrialOrderException;
import com.wiley.ws.core.order.WelFreeTrialOrderWsFacade;
import com.wiley.ws.core.order.dto.FreeTrialResponseWsDTO;
import com.wiley.ws.core.order.dto.FreeTrialWsDTO;
import com.wiley.ws.core.v2.controller.BaseController;
import com.wiley.ws.core.validator.FreeTrialWsDTOValidator;

import static com.wiley.ws.core.constants.WileyWSConstants.APPLICATION_TYPE_JSON;


/**
 * The Wel free trial order controller.
 */
@Controller
@RequestMapping(value = "/wel/order")
public class WelFreeTrialOrderController extends BaseController
{

	private static final Logger LOG = Logger.getLogger(WelFreeTrialOrderController.class);

	private static final String WEL_BASE_SITE = "wel";

	@Autowired
	private FreeTrialWsDTOValidator freeTrialWsDTOValidator;

	@Resource(name = "welFreeTrialOrderWsFacade")
 	private WelFreeTrialOrderWsFacade welFreeTrialOrderWsFacade;

	@Resource(name = "authenticationManager")
	private AuthenticationManager authenticationManager;

	@RequestMapping(value = "/freetrial", method = RequestMethod.POST, produces = { APPLICATION_TYPE_JSON }, consumes = {
			APPLICATION_TYPE_JSON })
	@Secured({"ROLE_TRUSTED_CLIENT", "ROLE_EXTERNAL_CLIENT"})
	@ResponseBody
	public FreeTrialResponseWsDTO createFreeTrialOrder(@Validated @RequestBody final FreeTrialWsDTO freeTrialWsDTO,
			final HttpServletRequest request, final HttpServletResponse response)
			throws DuplicateUidException, CommerceCartModificationException, RepeatedFreeTrialOrderException,
			InvalidCartException, IllegalAccessException
	{
		validate(freeTrialWsDTO, freeTrialWsDTO.getClass().getName(), freeTrialWsDTOValidator);
		Boolean isUserExisting = welFreeTrialOrderWsFacade.isUserExisting(freeTrialWsDTO.getEmail());
		welFreeTrialOrderWsFacade.setBaseSite(WEL_BASE_SITE);

		if (!isUserExisting)
		{
			welFreeTrialOrderWsFacade.registerNewUser(freeTrialWsDTO);
		}
		loginExistingUser(freeTrialWsDTO, request);

		// By default all search restrictions are disabled in DisableRestrictionsFilter for web services.
		welFreeTrialOrderWsFacade.enableSearchRestrictions();
		return welFreeTrialOrderWsFacade.placeFreeTrialOrder(freeTrialWsDTO);
	}

	public void loginExistingUser(final FreeTrialWsDTO freeTrialWsDTO, final HttpServletRequest request)
	{
		final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(freeTrialWsDTO.getEmail().
				toLowerCase(), freeTrialWsDTO.getPassword());
		token.setDetails(new WebAuthenticationDetails(request));
		final Authentication authentication = getAuthenticationManager().authenticate(token);
		SecurityContextHolder.getContext().setAuthentication(authentication);
		welFreeTrialOrderWsFacade.loginCurrentUser();
	}

	public AuthenticationManager getAuthenticationManager()
	{
		return authenticationManager;
	}
}
