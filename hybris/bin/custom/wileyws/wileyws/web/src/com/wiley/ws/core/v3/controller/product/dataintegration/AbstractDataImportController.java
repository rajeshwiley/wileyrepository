package com.wiley.ws.core.v3.controller.product.dataintegration;

import javax.validation.ValidationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.wiley.ws.core.product.dataintegration.common.error.ErrorListSwgDTO;
import com.wiley.ws.core.product.dataintegration.common.error.ErrorSwgDTO;
import com.wiley.ws.core.product.dataintegration.common.exception.DataImportNotFoundException;
import com.wiley.ws.core.product.dataintegration.common.exception.DataImportValidationConstraintsException;
import com.wiley.ws.core.product.dataintegration.common.exception.DataImportValidationErrorsException;
import com.wiley.ws.core.v2.controller.BaseController;

import static com.wiley.ws.core.product.dataintegration.common.error.ErrorSwgDTO.ReasonEnum.JSON_PARSE_ERROR;
import static com.wiley.ws.core.product.dataintegration.common.error.ErrorSwgDTO.TypeEnum.PARSING;


@RestController
public abstract class AbstractDataImportController extends BaseController
{
	private static final Logger LOG = LoggerFactory.getLogger(AbstractDataImportController.class);

	@ExceptionHandler({ MethodArgumentNotValidException.class, ValidationException.class, HttpMessageNotReadableException.class })
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	private ErrorListSwgDTO handleBadRequest(final Exception ex)
	{
		LOG.error("JSON parsing error occurs", ex);

		final String message = buildErrorMessage(ex);
		final ErrorSwgDTO error = ErrorSwgDTO.of(PARSING, JSON_PARSE_ERROR, message, null, null);
		return ErrorListSwgDTO.of(error);
	}

	@ExceptionHandler(DataImportValidationErrorsException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	private ErrorListSwgDTO handleValidationErrors(final DataImportValidationErrorsException e)
	{
		//TODO: Proper logging to be added
		return e.getErrors();
	}

	@ExceptionHandler(DataImportValidationConstraintsException.class)
	@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
	@ResponseBody
	private ErrorListSwgDTO handleConstraintErrors(final DataImportValidationConstraintsException e)
	{
		//TODO: Proper logging to be added
		return e.getErrors();
	}

	@ExceptionHandler(DataImportNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ResponseBody
	private ErrorListSwgDTO handleProductNotFoundException(final DataImportNotFoundException e)
	{
		return null;
	}

	private String buildErrorMessage(final Exception ex)
	{
		final String message = ex.getCause() == null ? ex.getMessage() : ex.getCause().getMessage();
		return message.replaceAll(System.lineSeparator(), "");
	}
}
