package com.wiley.ws.core.data;

import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BusinessProcessPatchRequestDTO
{
	@Size(max = 255)
	private String typeSystem;

	@JsonProperty("typeSystem")
	public String getTypeSystem()
	{
		return typeSystem;
	}

	public void setTypeSystem(final String typeSystem)
	{
		this.typeSystem = typeSystem;
	}
}
