package com.wiley.ws.core.data;

import java.util.List;


public class AuthSubscriptionsWsDTO implements java.io.Serializable
{

	private boolean passwordValid;

	private List<SubscriptionWsDTO> subscriptions;

	public AuthSubscriptionsWsDTO()
	{
		// default constructor
	}


	public void setPasswordValid(final boolean passwordValid)
	{
		this.passwordValid = passwordValid;
	}


	public boolean isPasswordValid()
	{
		return passwordValid;
	}


	public void setSubscriptions(final List<SubscriptionWsDTO> subscriptions)
	{
		this.subscriptions = subscriptions;
	}


	public List<SubscriptionWsDTO> getSubscriptions()
	{
		return subscriptions;
	}


}
