/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package com.wiley.ws.core.conv;

import java.util.Date;

import com.thoughtworks.xstream.converters.SingleValueConverter;
import com.wiley.ws.core.formatters.WsDateFormatter;


/**
 * Converter for a specific date format.
 */
public class CustomDateConverter implements SingleValueConverter
{
	private WsDateFormatter wsDateFormatter;

	/**
	 * Sets ws date formatter.
	 *
	 * @param wsDateFormatter
	 * 		the ws date formatter
	 */
	public void setWsDateFormatter(final WsDateFormatter wsDateFormatter)
	{
		this.wsDateFormatter = wsDateFormatter;
	}

	/**
	 * Can convert boolean.
	 *
	 * @param type
	 * 		the type
	 * @return the boolean
	 */
	@Override
	public boolean canConvert(final Class type)
	{
		return type == Date.class;
	}

	/**
	 * To string string.
	 *
	 * @param obj
	 * 		the obj
	 * @return the string
	 */
	@Override
	public String toString(final Object obj)
	{
		return wsDateFormatter.toString((Date) obj);

	}

	/**
	 * From string object.
	 *
	 * @param str
	 * 		the str
	 * @return the object
	 */
	@Override
	public Object fromString(final String str)
	{
		return wsDateFormatter.toDate(str);
	}
}
