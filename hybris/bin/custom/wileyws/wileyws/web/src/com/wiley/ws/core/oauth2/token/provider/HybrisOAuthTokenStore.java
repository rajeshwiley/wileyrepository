/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.ws.core.oauth2.token.provider;

import de.hybris.platform.webservicescommons.model.OAuthAccessTokenModel;
import de.hybris.platform.webservicescommons.model.OAuthRefreshTokenModel;
import de.hybris.platform.webservicescommons.oauth2.token.OAuthTokenService;
import de.hybris.platform.webservicescommons.util.YSanitizer;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;
import org.springframework.security.oauth2.common.util.SerializationUtils;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.AuthenticationKeyGenerator;
import org.springframework.security.oauth2.provider.token.DefaultAuthenticationKeyGenerator;
import org.springframework.security.oauth2.provider.token.TokenStore;


/**
 * The type Hybris o auth token store.
 */
public class HybrisOAuthTokenStore implements TokenStore
{
	private static final Logger LOG = Logger.getLogger(HybrisOAuthTokenStore.class);
	private final AuthenticationKeyGenerator authenticationKeyGenerator = new DefaultAuthenticationKeyGenerator();
	private OAuthTokenService oauthTokenService;

	/**
	 * Gets access token.
	 *
	 * @param authentication
	 * 		the authentication
	 * @return the access token
	 */
	@Override
	public OAuth2AccessToken getAccessToken(final OAuth2Authentication authentication)
	{
		OAuth2AccessToken accessToken = null;
		OAuthAccessTokenModel accessTokenModel = null;
		final String authenticationId = authenticationKeyGenerator.extractKey(authentication);
		try
		{
			accessTokenModel = oauthTokenService.getAccessTokenForAuthentication(authenticationId);
			accessToken = deserializeAccessToken((byte[]) accessTokenModel.getToken());
		}
		catch (final ClassCastException e)
		{
			LOG.warn("Could not extract access token for authentication " + authentication, e);
			oauthTokenService.removeAccessTokenForAuthentication(authenticationId);
		}
		catch (final UnknownIdentifierException e)
		{
			if (LOG.isInfoEnabled())
			{
				LOG.debug("Failed to find access token for authentication " + authentication);
			}
		}

		if (accessToken != null && accessTokenModel != null && !StringUtils.equals(authenticationId,
				authenticationKeyGenerator.extractKey(deserializeAuthentication((byte[]) accessTokenModel.getAuthentication()))))
		{
			removeAccessToken(accessToken.getValue());
			// Keep the store consistent (maybe the same user is represented by this authentication but the details have
			// changed)
			storeAccessToken(accessToken, authentication);
		}
		return accessToken;
	}

	/**
	 * Store access token.
	 *
	 * @param token
	 * 		the token
	 * @param authentication
	 * 		the authentication
	 */
	@Override
	public void storeAccessToken(final OAuth2AccessToken token, final OAuth2Authentication authentication)
	{
		OAuthRefreshTokenModel refreshTokenModel = null;
		if (token.getRefreshToken() != null)
		{
			final String refreshTokenKey = extractTokenKey(token.getRefreshToken().getValue());
			try
			{
				refreshTokenModel = oauthTokenService.getRefreshToken(refreshTokenKey);
			}
			catch (final UnknownIdentifierException e)
			{
				refreshTokenModel = oauthTokenService.saveRefreshToken(refreshTokenKey, serializeRefreshToken(
						token.getRefreshToken()), serializeAuthentication(authentication));
			}
		}

		oauthTokenService.saveAccessToken(extractTokenKey(token.getValue()), serializeAccessToken(token),
				authenticationKeyGenerator.extractKey(authentication), serializeAuthentication(authentication),
				authentication.isClientOnly() ? null : authentication.getName(), authentication.getOAuth2Request().getClientId(),
				refreshTokenModel);
	}

	/**
	 * Read access token o auth 2 access token.
	 *
	 * @param tokenValue
	 * 		the token value
	 * @return the o auth 2 access token
	 */
	@Override
	public OAuth2AccessToken readAccessToken(final String tokenValue)
	{
		OAuth2AccessToken accessToken = null;

		try
		{
			final OAuthAccessTokenModel accessTokenModel = oauthTokenService.getAccessToken(extractTokenKey(tokenValue));
			accessToken = deserializeAccessToken((byte[]) accessTokenModel.getToken());
		}
		catch (final ClassCastException e)
		{
			LOG.warn("Failed to deserialize access token for  " + YSanitizer.sanitize(tokenValue), e);
			removeAccessToken(tokenValue);
		}
		catch (final UnknownIdentifierException e)
		{
			if (LOG.isInfoEnabled())
			{
				LOG.info("Failed to find access token for token " + YSanitizer.sanitize(tokenValue));
			}
		}

		return accessToken;
	}

	/**
	 * Remove access token.
	 *
	 * @param token
	 * 		the token
	 */
	@Override
	public void removeAccessToken(final OAuth2AccessToken token)
	{
		removeAccessToken(token.getValue());
	}

	/**
	 * Remove access token.
	 *
	 * @param tokenValue
	 * 		the token value
	 */
	public void removeAccessToken(final String tokenValue)
	{
		oauthTokenService.removeAccessToken(extractTokenKey(tokenValue));
	}

	/**
	 * Read authentication o auth 2 authentication.
	 *
	 * @param token
	 * 		the token
	 * @return the o auth 2 authentication
	 */
	@Override
	public OAuth2Authentication readAuthentication(final OAuth2AccessToken token)
	{
		return readAuthentication(token.getValue());
	}

	/**
	 * Read authentication o auth 2 authentication.
	 *
	 * @param token
	 * 		the token
	 * @return the o auth 2 authentication
	 */
	@Override
	public OAuth2Authentication readAuthentication(final String token)
	{
		OAuth2Authentication authentication = null;

		try
		{
			final OAuthAccessTokenModel accessTokenModel = oauthTokenService.getAccessToken(extractTokenKey(token));
			authentication = deserializeAuthentication((byte[]) accessTokenModel.getAuthentication());
		}
		catch (final ClassCastException e)
		{
			LOG.warn("Failed to deserialize authentication for " + YSanitizer.sanitize(token), e);
			removeAccessToken(token);
		}
		catch (final UnknownIdentifierException e)
		{
			if (LOG.isInfoEnabled())
			{
				LOG.info("Failed to find authentication for token " + YSanitizer.sanitize(token));
			}
		}
		return authentication;
	}

	/**
	 * Store refresh token.
	 *
	 * @param refreshToken
	 * 		the refresh token
	 * @param authentication
	 * 		the authentication
	 */
	@Override
	public void storeRefreshToken(final OAuth2RefreshToken refreshToken, final OAuth2Authentication authentication)
	{
		oauthTokenService.saveRefreshToken(extractTokenKey(refreshToken.getValue()), serializeRefreshToken(refreshToken),
				serializeAuthentication(authentication));
	}

	/**
	 * Read refresh token o auth 2 refresh token.
	 *
	 * @param token
	 * 		the token
	 * @return the o auth 2 refresh token
	 */
	@Override
	public OAuth2RefreshToken readRefreshToken(final String token)
	{
		OAuth2RefreshToken refreshToken = null;

		try
		{
			final OAuthRefreshTokenModel refreshTokenModel = oauthTokenService.getRefreshToken(extractTokenKey(token));
			refreshToken = deserializeRefreshToken((byte[]) refreshTokenModel.getToken());
		}
		catch (final ClassCastException e)
		{
			LOG.warn("Failed to deserialize refresh token for token " + YSanitizer.sanitize(token), e);
			removeRefreshToken(token);
		}
		catch (final UnknownIdentifierException e)
		{
			if (LOG.isInfoEnabled())
			{
				LOG.info("Failed to find refresh token for token " + YSanitizer.sanitize(token));
			}
		}

		return refreshToken;
	}

	/**
	 * Remove refresh token.
	 *
	 * @param token
	 * 		the token
	 */
	@Override
	public void removeRefreshToken(final OAuth2RefreshToken token)
	{
		removeRefreshToken(token.getValue());
	}

	/**
	 * Remove refresh token.
	 *
	 * @param token
	 * 		the token
	 */
	public void removeRefreshToken(final String token)
	{
		oauthTokenService.removeRefreshToken(extractTokenKey(token));
	}

	/**
	 * Read authentication for refresh token o auth 2 authentication.
	 *
	 * @param token
	 * 		the token
	 * @return the o auth 2 authentication
	 */
	@Override
	public OAuth2Authentication readAuthenticationForRefreshToken(final OAuth2RefreshToken token)
	{
		return readAuthenticationForRefreshToken(token.getValue());
	}

	/**
	 * Read authentication for refresh token o auth 2 authentication.
	 *
	 * @param value
	 * 		the value
	 * @return the o auth 2 authentication
	 */
	public OAuth2Authentication readAuthenticationForRefreshToken(final String value)
	{
		OAuth2Authentication authentication = null;
		try
		{
			final OAuthRefreshTokenModel refreshTokenModel = oauthTokenService.getRefreshToken(extractTokenKey(value));
			authentication = deserializeAuthentication((byte[]) refreshTokenModel.getAuthentication());
		}
		catch (final ClassCastException e)
		{
			LOG.warn("Failed to deserialize authentication for refresh token " + YSanitizer.sanitize(value), e);
			removeRefreshToken(value);
		}
		catch (final UnknownIdentifierException e)
		{
			if (LOG.isInfoEnabled())
			{
				LOG.info("Failed to find refresh token for " + YSanitizer.sanitize(value));
			}
		}

		return authentication;
	}

	/**
	 * Remove access token using refresh token.
	 *
	 * @param refreshToken
	 * 		the refresh token
	 */
	@Override
	public void removeAccessTokenUsingRefreshToken(final OAuth2RefreshToken refreshToken)
	{
		removeAccessTokenUsingRefreshToken(refreshToken.getValue());
	}

	/**
	 * Remove access token using refresh token.
	 *
	 * @param refreshToken
	 * 		the refresh token
	 */
	public void removeAccessTokenUsingRefreshToken(final String refreshToken)
	{
		oauthTokenService.removeAccessTokenUsingRefreshToken(extractTokenKey(refreshToken));
	}

	/**
	 * Find tokens by client id collection.
	 *
	 * @param clientId
	 * 		the client id
	 * @return the collection
	 */
	@Override
	public Collection<OAuth2AccessToken> findTokensByClientId(final String clientId)
	{
		final List<OAuth2AccessToken> accessTokenList = new ArrayList<OAuth2AccessToken>();
		final List<OAuthAccessTokenModel> accessTokenModelList = oauthTokenService.getAccessTokensForClient(clientId);
		OAuth2AccessToken accessToken;
		for (final OAuthAccessTokenModel accessTokenModel : accessTokenModelList)
		{
			try
			{
				accessToken = deserializeAccessToken((byte[]) accessTokenModel.getToken());
				accessTokenList.add(accessToken);
			}
			catch (final ClassCastException e)
			{
				//invalid token so we remove it from database
				LOG.warn("Failed to deserialize access token for client : " + YSanitizer.sanitize(clientId), e);
				oauthTokenService.removeAccessToken(accessTokenModel.getTokenId());
			}
		}

		return accessTokenList;
	}

	/**
	 * Find tokens by client id and user name collection.
	 *
	 * @param clientId
	 * 		the client id
	 * @param userName
	 * 		the user name
	 * @return the collection
	 */
	@Override
	public Collection<OAuth2AccessToken> findTokensByClientIdAndUserName(final String clientId, final String userName)
	{
		final List<OAuth2AccessToken> accessTokenList = new ArrayList<OAuth2AccessToken>();
		final List<OAuthAccessTokenModel> accessTokenModelList = oauthTokenService.getAccessTokensForClientAndUser(clientId,
				userName);
		OAuth2AccessToken accessToken;
		for (final OAuthAccessTokenModel accessTokenModel : accessTokenModelList)
		{
			try
			{
				accessToken = deserializeAccessToken((byte[]) accessTokenModel.getToken());
				accessTokenList.add(accessToken);
			}
			catch (final ClassCastException e)
			{
				//invalid token so we remove it from database
				LOG.warn("Failed to deserialize access token for client : " + YSanitizer.sanitize(clientId), e);
				oauthTokenService.removeAccessToken(accessTokenModel.getTokenId());
			}
		}

		return accessTokenList;

	}


	/**
	 * Find tokens by user name collection.
	 *
	 * @param userName
	 * 		the user name
	 * @return the collection
	 */
	public Collection<OAuth2AccessToken> findTokensByUserName(final String userName)
	{
		final List<OAuth2AccessToken> accessTokenList = new ArrayList<OAuth2AccessToken>();

		final List<OAuthAccessTokenModel> accessTokenModelList = oauthTokenService.getAccessTokensForUser(userName);
		OAuth2AccessToken accessToken;
		for (final OAuthAccessTokenModel accessTokenModel : accessTokenModelList)
		{
			try
			{
				accessToken = deserializeAccessToken((byte[]) accessTokenModel.getToken());
				accessTokenList.add(accessToken);
			}
			catch (final ClassCastException e)
			{
				//invalid token so we remove it from database
				LOG.warn("Failed to deserialize access token for user : " + YSanitizer.sanitize(userName), e);
				oauthTokenService.removeAccessToken(accessTokenModel.getTokenId());
			}
		}

		return accessTokenList;
	}

	/**
	 * Extract token key string.
	 *
	 * @param value
	 * 		the value
	 * @return the string
	 */
	protected String extractTokenKey(final String value)
	{
		if (value == null)
		{
			return null;
		}
		MessageDigest digest;
		try
		{
			digest = MessageDigest.getInstance("SHA-256");
		}
		catch (final NoSuchAlgorithmException e)
		{
			throw new IllegalStateException("SHA-256 algorithm not available.  Fatal (should be in the JDK).", e);
		}

		try
		{
			final byte[] bytes = digest.digest(value.getBytes("UTF-8"));
			return String.format("%032x", new BigInteger(1, bytes));
		}
		catch (final UnsupportedEncodingException e)
		{
			throw new IllegalStateException("UTF-8 encoding not available.  Fatal (should be in the JDK).", e);
		}
	}

	/**
	 * Serialize access token byte [ ].
	 *
	 * @param token
	 * 		the token
	 * @return the byte [ ]
	 */
	protected byte[] serializeAccessToken(final OAuth2AccessToken token)
	{
		return SerializationUtils.serialize(token);
	}

	/**
	 * Serialize refresh token byte [ ].
	 *
	 * @param token
	 * 		the token
	 * @return the byte [ ]
	 */
	protected byte[] serializeRefreshToken(final OAuth2RefreshToken token)
	{
		return SerializationUtils.serialize(token);
	}

	/**
	 * Serialize authentication byte [ ].
	 *
	 * @param authentication
	 * 		the authentication
	 * @return the byte [ ]
	 */
	protected byte[] serializeAuthentication(final OAuth2Authentication authentication)
	{
		return SerializationUtils.serialize(authentication);
	}

	/**
	 * Deserialize access token o auth 2 access token.
	 *
	 * @param token
	 * 		the token
	 * @return the o auth 2 access token
	 */
	protected OAuth2AccessToken deserializeAccessToken(final byte[] token)
	{
		return SerializationUtils.deserialize(token);
	}

	/**
	 * Deserialize refresh token o auth 2 refresh token.
	 *
	 * @param token
	 * 		the token
	 * @return the o auth 2 refresh token
	 */
	protected OAuth2RefreshToken deserializeRefreshToken(final byte[] token)
	{
		return SerializationUtils.deserialize(token);
	}

	/**
	 * Deserialize authentication o auth 2 authentication.
	 *
	 * @param authentication
	 * 		the authentication
	 * @return the o auth 2 authentication
	 */
	protected OAuth2Authentication deserializeAuthentication(final byte[] authentication)
	{
		return SerializationUtils.deserialize(authentication);
	}

	/**
	 * Gets oauth token service.
	 *
	 * @return the oauth token service
	 */
	public OAuthTokenService getOauthTokenService()
	{
		return oauthTokenService;
	}

	/**
	 * Sets oauth token service.
	 *
	 * @param oauthTokenService
	 * 		the oauth token service
	 */
	@Required
	public void setOauthTokenService(final OAuthTokenService oauthTokenService)
	{
		this.oauthTokenService = oauthTokenService;
	}
}
