package com.wiley.ws.core.errors.converters;

import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.webservicescommons.dto.error.ErrorWsDTO;
import de.hybris.platform.webservicescommons.errors.converters.AbstractLocalizedErrorConverter;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.context.MessageSource;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;


/**
 * Created by Mikhail_Asadchy on 10/18/2016.
 *
 * Converts FieldErrors and ObjectErrors to ErrorWsDTO in a way that resolves localization messages properly.
 */
public class WileywsValidationErrorConverter extends AbstractLocalizedErrorConverter
{
	private static final String TYPE = "ValidationError";
	private static final String SUBJECT_TYPE = "parameter";
	private static final String REASON_INVALID = "invalid";
	private static final String REASON_MISSING = "missing";
	private I18NService i18NService;

	private MessageSource messageSource;

	@Override
	public boolean supports(final Class clazz)
	{
		return Errors.class.isAssignableFrom(clazz);
	}

	@Override
	public void populate(final Object o, final List<ErrorWsDTO> webserviceErrorList)
	{
		final Errors errors = (Errors) o;
		final Locale currentLocale = i18NService.getCurrentLocale();
		for (final ObjectError error : errors.getGlobalErrors())
		{
			final ErrorWsDTO errorDto = createTargetElement();
			errorDto.setType(TYPE);
			errorDto.setSubjectType(SUBJECT_TYPE);
			errorDto.setMessage(resolveMessage(currentLocale, error));
			errorDto.setSubject(error.getObjectName());
			errorDto.setReason(REASON_INVALID);
			webserviceErrorList.add(errorDto);
		}
		for (final FieldError error : errors.getFieldErrors())
		{
			final ErrorWsDTO errorDto = createTargetElement();
			errorDto.setType(TYPE);
			errorDto.setSubjectType(SUBJECT_TYPE);
			errorDto.setMessage(resolveMessage(currentLocale, error));
			errorDto.setSubject(error.getField());
			errorDto.setReason(error.getRejectedValue() == null ? REASON_MISSING : REASON_INVALID);
			webserviceErrorList.add(errorDto);
		}
	}

	private String resolveMessage(final Locale currentLocale, final ObjectError error)
	{
		String result;

		final String[] codes = error.getCodes();
		if (!ObjectUtils.isEmpty(codes))
		{
			for (final String code : codes)
			{
				result = getMessageOrNull(code, error.getArguments(), currentLocale);
				if (result != null)
				{
					return result;
				}
			}
		}

		final String defaultMessage = error.getDefaultMessage();
		if (defaultMessage != null)
		{
			result = isMessageCode(defaultMessage) ? getMessageOrNull(getDefaultMessageCode(defaultMessage), error.getArguments(),
					currentLocale) : defaultMessage;
			if (result != null)
			{
				return result;
			}
		}
		return null;
	}

	private boolean isMessageCode(final String defaultMessage)
	{
		return defaultMessage.contains("{") && defaultMessage.contains("}");
	}

	private String getDefaultMessageCode(final String messageExpression)
	{
		return messageExpression.substring(messageExpression.indexOf("{") + 1, messageExpression.indexOf("}"));
	}


	private String getMessageOrNull(final String code, final Object[] args, final Locale locale)
	{
		return messageSource.getMessage(code, args, null, locale);
	}

	@Required
	public void setI18NService(final I18NService i18NService)
	{
		this.i18NService = i18NService;
	}

	@Required
	public void setMessageSource(final MessageSource messageSource)
	{
		this.messageSource = messageSource;
	}

}
