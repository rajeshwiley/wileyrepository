package com.wiley.ws.core.v3.controller.product.dataintegration;

import javax.validation.Valid;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.wiley.ws.core.product.crud.dto.WileyProductWsDto;
import com.wiley.ws.core.product.crud.dto.WileyVariantProductWsDto;
import com.wiley.ws.core.product.crud.facade.WileyProductCrudFacade;

import static com.wiley.ws.core.constants.WileyWSConstants.APPLICATION_TYPE_JSON;


@RestController
@RequestMapping(value = WileyProductCrudController.PRODUCTS_CRUD_BASE,
		produces = { APPLICATION_TYPE_JSON }, consumes = { APPLICATION_TYPE_JSON })
@Secured("ROLE_TRUSTED_CLIENT")
@Validated
public class WileyProductCrudController extends AbstractDataImportController
{

	public static final String PRODUCTS_CRUD_BASE = "/products";
	private static final String BASE_PRODUCT_UPDATE = "/{productId}";
	private static final String VARIANT_PRODUCT_CREATE = "/{productId}/variants";
	private static final String VARIANT_PRODUCT_UPDATE = "/{productId}/variants/{variantId}";

	@Autowired
	private WileyProductCrudFacade wileyProductCrudFacade;

	@RequestMapping(method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Void> createBaseProduct(@Valid @RequestBody final WileyProductWsDto product,
			final UriComponentsBuilder uriBuilder)
	{
		final String productCode = wileyProductCrudFacade.createProduct(product);
		final UriComponents uriComponents = uriBuilder
				.path(PRODUCTS_CRUD_BASE + BASE_PRODUCT_UPDATE)
				.buildAndExpand(productCode);
		return ResponseEntity.created(uriComponents.toUri()).build();
	}

	@RequestMapping(value = BASE_PRODUCT_UPDATE, method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void updateBaseProduct(@PathVariable("productId") @Size(max = 255) final String productId,
			@Valid @RequestBody final WileyProductWsDto product
	)
	{
		wileyProductCrudFacade.updateProduct(productId, product);
	}

	@RequestMapping(value = VARIANT_PRODUCT_CREATE, method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Void> createVariantProduct(@PathVariable final String productId,
			@RequestBody @Valid final WileyVariantProductWsDto variantProductDto,
			final UriComponentsBuilder uriBuilder)
	{
		final String variantProductCode = wileyProductCrudFacade.createVariantProduct(productId, variantProductDto);

		final UriComponents uriComponents = uriBuilder
				.path(PRODUCTS_CRUD_BASE + VARIANT_PRODUCT_UPDATE)
				.buildAndExpand(productId, variantProductCode);
		return ResponseEntity.created(uriComponents.toUri()).build();
	}

	@RequestMapping(value = VARIANT_PRODUCT_UPDATE, method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void updateVariantProduct(@PathVariable final String productId, @PathVariable final String variantId,
			@RequestBody @Valid final WileyVariantProductWsDto variantProductDto)
	{
		wileyProductCrudFacade.updateVariantProduct(productId, variantId, variantProductDto);
	}

}
