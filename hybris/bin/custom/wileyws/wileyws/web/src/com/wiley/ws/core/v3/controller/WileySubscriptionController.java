package com.wiley.ws.core.v3.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.Size;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.wiley.facades.wileyws.subscriptions.dto.SubscriptionSearchResult;
import com.wiley.ws.core.exceptions.http.WileywsNotFoundException;
import com.wiley.ws.core.wileysubscription.Wileyb2cWsSubscriptionFacade;
import com.wiley.ws.core.wileysubscription.dto.PaymentTransactionWsDTO;
import com.wiley.ws.core.wileysubscription.dto.SubscriptionCreateRequestWsDTO;
import com.wiley.ws.core.wileysubscription.dto.SubscriptionCreateResponseWsDTO;
import com.wiley.ws.core.wileysubscription.dto.SubscriptionUpdateRequestWsDTO;
import com.wiley.ws.core.wileysubscription.exception.NoSuchSubscriptionException;


/**
 * Controller handles requests used to interact with Wiley Subscriptions.
 */
@Controller
@RequestMapping(value = "/subscriptions")
@Validated
public class WileySubscriptionController
{

	@Resource
	private Wileyb2cWsSubscriptionFacade wileyb2cWsSubscriptionFacade;

	@Secured("ROLE_TRUSTED_CLIENT")
	@RequestMapping(method = RequestMethod.POST,
			consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public SubscriptionCreateResponseWsDTO createWileySubscription(
			@RequestBody @Valid final SubscriptionCreateRequestWsDTO subscriptionRequestDto)
	{
		return wileyb2cWsSubscriptionFacade.createSubscription(subscriptionRequestDto);
	}

	@Secured("ROLE_TRUSTED_CLIENT")
	@RequestMapping(value = "/{id}", method = RequestMethod.POST,
			consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity updateWileySubscription(@PathVariable @Size(max = 255) final String id,
			@RequestBody @Valid final SubscriptionUpdateRequestWsDTO subscriptionRequestDto)
	{
		ResponseEntity response;

		if (wileyb2cWsSubscriptionFacade.doesSubscriptionExist(id))
		{
			wileyb2cWsSubscriptionFacade.updateSubscription(id, subscriptionRequestDto);
			response = ResponseEntity.status(204).build();
		}
		else
		{
			response = ResponseEntity.status(404).build();
		}

		return response;
	}

	@Secured("ROLE_TRUSTED_CLIENT")
	@RequestMapping(value = "/{id}/transactions", method = RequestMethod.POST,
			consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity createPaymentTransactions(@PathVariable @Size(max = 255) final String id,
			@Valid @RequestBody final PaymentTransactionWsDTO paymentTrandactioWsDTO)
	{
		try
		{
			wileyb2cWsSubscriptionFacade.createPaymentTransactions(id, paymentTrandactioWsDTO);
		}
		catch (NoSuchSubscriptionException e)
		{
			throw new WileywsNotFoundException(e.getMessage(), e);
		}
		return ResponseEntity.status(HttpStatus.CREATED).build();
	}

	@Secured("ROLE_TRUSTED_CLIENT")
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<SubscriptionSearchResult> getSubscriptions(
			@RequestParam("externalCode") @Size(max = 255) final String wsSubscriptionExternalCode)
			throws BindException
	{
		return wileyb2cWsSubscriptionFacade.getInternalCodesByExternalCode(wsSubscriptionExternalCode);
	}

	private void validate(final SubscriptionUpdateRequestWsDTO orderStatusWsDTO, final Validator validator,
			final BindingResult bindingResult) throws BindException
	{
		validator.validate(orderStatusWsDTO, bindingResult);

		if (bindingResult.hasErrors())
		{
			throw new BindException(bindingResult);
		}
	}


}
