/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.ws.core.v2.filter;

import de.hybris.platform.commercewebservicescommons.errors.exceptions.CartException;
import de.hybris.platform.commercewebservicescommons.strategies.CartLoaderStrategy;
import de.hybris.platform.servicelayer.i18n.I18NService;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.BadRequestException;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.context.MessageSource;

import com.wiley.ws.core.exceptions.UnknownResourceException;


/**
 * Filter that puts cart from the requested url into the session.
 */
public class CartMatchingFilter extends AbstractUrlMatchingFilter
{
	public static final String PATH_CART_ID_INVALID_KEY = "path.cartId.invalid";
	public static final String PATH_CART_ID_INVALID_USER_KEY = "path.cartId.invalid.user";
	private String regexp;
	private CartLoaderStrategy cartLoaderStrategy;
	private I18NService i18NService;
	private MessageSource messageSource;

	/**
	 * Do filter internal.
	 *
	 * @param request
	 * 		the request
	 * @param response
	 * 		the response
	 * @param filterChain
	 * 		the filter chain
	 * @throws ServletException
	 * 		the servlet exception
	 * @throws IOException
	 * 		the io exception
	 */
	@Override
	protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response,
			final FilterChain filterChain) throws ServletException, IOException
	{
		if (matchesUrl(request, regexp))
		{
			final String cartId = getValue(request, regexp);
			try
			{
				cartLoaderStrategy.loadCart(cartId);
			}
			catch (CartException e)
			{
				if (CartException.NOT_FOUND.equals(e.getReason()))
				{
					String localizedMessage = getMessageSource().getMessage(PATH_CART_ID_INVALID_KEY, new String[] { cartId },
							"Cannot find cart with guid ''{0}''", getI18NService().getCurrentLocale());
					throw new UnknownResourceException(localizedMessage);
				}
				else if (CartException.INVALID.equals(e.getReason()))
				{
					String localizedMessage = getMessageSource().getMessage(PATH_CART_ID_INVALID_USER_KEY,
							new String[] { cartId },
							"Cart with guid ''{0}'' was found, but does not belong to the specified customer",
							getI18NService().getCurrentLocale());
					throw new BadRequestException(localizedMessage);
				}
				else
				{
					throw e;
				}
			}
		}

		filterChain.doFilter(request, response);
	}

	/**
	 * Gets regexp.
	 *
	 * @return the regexp
	 */
	protected String getRegexp()
	{
		return regexp;
	}

	/**
	 * Sets regexp.
	 *
	 * @param regexp
	 * 		the regexp
	 */
	@Required
	public void setRegexp(final String regexp)
	{
		this.regexp = regexp;
	}

	/**
	 * Gets cart loader strategy.
	 *
	 * @return the cart loader strategy
	 */
	public CartLoaderStrategy getCartLoaderStrategy()
	{
		return cartLoaderStrategy;
	}

	/**
	 * Sets cart loader strategy.
	 *
	 * @param cartLoaderStrategy
	 * 		the cart loader strategy
	 */
	@Required
	public void setCartLoaderStrategy(final CartLoaderStrategy cartLoaderStrategy)
	{
		this.cartLoaderStrategy = cartLoaderStrategy;
	}

	public I18NService getI18NService()
	{
		return i18NService;
	}

	@Required
	public void setI18NService(final I18NService i18NService)
	{
		this.i18NService = i18NService;
	}

	public MessageSource getMessageSource()
	{
		return messageSource;
	}

	@Required
	public void setMessageSource(final MessageSource messageSource)
	{
		this.messageSource = messageSource;
	}
}
