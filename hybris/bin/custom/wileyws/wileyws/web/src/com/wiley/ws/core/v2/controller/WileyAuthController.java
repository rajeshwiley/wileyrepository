package com.wiley.ws.core.v2.controller;

import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.webservicescommons.cache.CacheControl;
import de.hybris.platform.webservicescommons.cache.CacheControlDirective;

import java.util.Collections;
import java.util.Objects;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.wiley.facades.product.data.WileySubscriptionData;
import com.wiley.facades.subscription.WileySubscriptionFacade;
import com.wiley.facades.welags.customer.WelAgsCustomerFacade;
import com.wiley.ws.core.data.AuthSubscriptionsWsDTO;
import com.wiley.ws.core.data.SubscriptionWsDTO;
import com.wiley.ws.core.user.data.AuthCheckResultWsDTO;
import com.wiley.ws.core.user.data.AuthWsCredentials;


/**
 * Wiley Controller for User Authentication
 *
 * @pathparam userId User identifier or one of the literals below : <ul> <li>'current' for currently authenticated user</li>
 * <li>'anonymous' for anonymous user</li> </ul>
 */
@Controller
@RequestMapping(value = "/{baseSiteId}/users")
@CacheControl(directive = CacheControlDirective.PRIVATE)
public class WileyAuthController extends BaseController
{
	private static final Logger LOG = Logger.getLogger(WileyAuthController.class);

	private static final String NUMBER_SIGN = "\\u0023";

	@Resource(name = "welAgsCustomerFacade")
	private WelAgsCustomerFacade customerFacade;

	@Autowired
	private WileySubscriptionFacade subscriptionFacade;

	@Resource(name = "authWsCredentialsDTOValidator")
	private Validator authWsCredentialsDTOValidator;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "modelService")
	private ModelService modelService;

	/**
	 * Web service for entitlement system to give username/password as input and to receive from hybris valid/not valid output.
	 * Validates a user's password matching the provided in request value with the value stored in the Hybris application.
	 *
	 * @return
	 */
	@Secured({ "ROLE_TRUSTED_CLIENT", "ROLE_EXTERNAL_CLIENT" })
	@RequestMapping(value = "/{userId}/auth", method = RequestMethod.POST)
	@ResponseBody
	public AuthCheckResultWsDTO checkUser(@RequestBody(required = true) final AuthWsCredentials userCredentials)
	{
		validate(userCredentials, "userCredentials", authWsCredentialsDTOValidator);
		CustomerData customerData = getCustomerData(userCredentials);

		final AuthCheckResultWsDTO dto = new AuthCheckResultWsDTO();
		if (customerData != null)
		{
			dto.setFirstName(customerData.getFirstName());
			dto.setLastName(customerData.getLastName());
			dto.setPasswordValid(true);
		}
		else
		{
			dto.setPasswordValid(false);
		}
		return dto;
	}

	private CustomerData getCustomerData(final AuthWsCredentials userCredentials)
	{
		CustomerData customerData = customerFacade.checkPasswordGetCutomerName(userCredentials.getPassword());

		if (customerData == null)
		{
			customerData = getCustomerWithIncorrectPassword(userCredentials);
		}
		return customerData;
	}

	private CustomerData getCustomerWithIncorrectPassword(final AuthWsCredentials userCredentials)
	{
		CustomerData customerData = null;
		String strippedPass = getStrippedPassword(userCredentials);

		if (!Objects.equals(strippedPass, userCredentials.getPassword()))
		{
			customerData = customerFacade.checkPasswordGetCutomerName(strippedPass);
			markUserWithIncorrectPassword(customerData);
		}

		return customerData;
	}

	private void markUserWithIncorrectPassword(final CustomerData customerData)
	{
		if (customerData != null)
		{
			final CustomerModel currentCustomer = (CustomerModel) userService.getCurrentUser();
			currentCustomer.setHasStrippedPassword(true);
			modelService.save(currentCustomer);
		}
	}

	private String getStrippedPassword(final AuthWsCredentials userCredentials)
	{
		return userCredentials.getPassword().replaceAll(NUMBER_SIGN, StringUtils.EMPTY);
	}



	/**
	 * Web service for external system to check existence and expiration time for a subscription.
	 * Validates a user's password matching the provided in request value with the value stored in the Hybris application.
	 * Searches for active subscriptions for the Hybris site and returns theirs expiration dates
	 * in the format "yyyy-MM-dd'T'HH:mm:ssZZ" .
	 *
	 * @return
	 */
	@Secured("ROLE_TRUSTED_CLIENT")
	@RequestMapping(value = "/{userId}/auth/subscription", method = RequestMethod.POST)
	@ResponseBody
	public AuthSubscriptionsWsDTO checkUserSubscriptions(@RequestBody(required = true) final AuthWsCredentials userCredentials)
	{
		validate(userCredentials, "userCredentials", authWsCredentialsDTOValidator);
		final boolean passwordEquals = customerFacade.checkPassword(userCredentials.getPassword());
		final AuthSubscriptionsWsDTO dto = new AuthSubscriptionsWsDTO();
		if (passwordEquals)
		{
			dto.setPasswordValid(true);
			WileySubscriptionData subscriptionData = subscriptionFacade.getActiveSubscription();
			final SubscriptionWsDTO subscriptionWsDTO = dataMapper.map(subscriptionData, SubscriptionWsDTO.class,
					"expires,code,productCode");
			if (subscriptionWsDTO != null)
			{
				dto.setSubscriptions(Collections.singletonList(subscriptionWsDTO));
			}
			else
			{
				dto.setSubscriptions(Collections.EMPTY_LIST);
			}

		}
		else
		{
			dto.setPasswordValid(false);
		}


		return dto;
	}
}
