/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package com.wiley.ws.core.validator;

import de.hybris.platform.commercefacades.product.data.ReviewData;

import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import static de.hybris.platform.customerreview.model.CustomerReviewModel.COMMENT;
import static de.hybris.platform.customerreview.model.CustomerReviewModel.HEADLINE;
import static de.hybris.platform.customerreview.model.CustomerReviewModel.RATING;


/**
 * The type Review data validator.
 */
@Component("reviewValidator")
public class ReviewDataValidator implements Validator
{
	private static final double RATING_MIN = 1.0d;
	private static final double RATING_MAX = 5.0d;

	/**
	 * Supports boolean.
	 *
	 * @param clazz
	 * 		the clazz
	 * @return the boolean
	 */
	@Override
	public boolean supports(final Class clazz)
	{
		return ReviewData.class.equals(clazz);
	}

	/**
	 * Validate.
	 *
	 * @param target
	 * 		the target
	 * @param errors
	 * 		the errors
	 */
	@Override
	public void validate(final Object target, final Errors errors)
	{
		ValidationUtils.rejectIfEmpty(errors, HEADLINE, "field.required");
		ValidationUtils.rejectIfEmpty(errors, COMMENT, "field.required");
		validateRating(errors);
	}

	/**
	 * Validate rating.
	 *
	 * @param errors
	 * 		the errors
	 */
	protected void validateRating(final Errors errors)
	{
		Assert.notNull(errors, "Errors object must not be null");
		final Double rating = (Double) errors.getFieldValue(RATING);

		if (rating == null)
		{
			errors.rejectValue(RATING, "field.required");
		}
		else
		{
			if (rating.doubleValue() < RATING_MIN || rating.doubleValue() > RATING_MAX)
			{
				errors.rejectValue(RATING, "review.rating.invalid");
			}
		}
	}

}
