/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package com.wiley.ws.core.util.ws;

/**
 * The interface Search query codec.
 *
 * @param <QUERY>
 * 		the type parameter
 */
public interface SearchQueryCodec<QUERY>
{
	/**
	 * Decode query query.
	 *
	 * @param query
	 * 		the query
	 * @return the query
	 */
	QUERY decodeQuery(String query);

	/**
	 * Encode query string.
	 *
	 * @param query
	 * 		the query
	 * @return the string
	 */
	String encodeQuery(QUERY query);
}
