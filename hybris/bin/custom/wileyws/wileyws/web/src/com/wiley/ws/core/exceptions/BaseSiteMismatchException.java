/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package com.wiley.ws.core.exceptions;

import javax.servlet.ServletException;


/**
 * The type Base site mismatch exception.
 */
public class BaseSiteMismatchException extends ServletException
{
	/**
	 * Instantiates a new Base site mismatch exception.
	 *
	 * @param baseSiteIdInRequest
	 * 		the base site id in request
	 * @param baseSiteIdInCart
	 * 		the base site id in cart
	 */
	public BaseSiteMismatchException(final String baseSiteIdInRequest, final String baseSiteIdInCart)
	{
		super("Base site '" + baseSiteIdInRequest + "' from the current request does not match with base site '"
				+ baseSiteIdInCart + "' from the cart!");
	}
}
