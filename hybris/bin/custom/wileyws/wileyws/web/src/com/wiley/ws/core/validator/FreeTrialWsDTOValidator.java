package com.wiley.ws.core.validator;

import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.wiley.ws.core.order.dto.FreeTrialWsDTO;

import static com.wiley.core.constants.WileyCoreConstants.MAX_FIELD_LENGTH;



public class FreeTrialWsDTOValidator implements Validator
{

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return FreeTrialWsDTO.class.equals(aClass);
	}

	@Override
	public void validate(final Object o, final Errors errors)
	{
		FreeTrialWsDTO freeTrialWsDTO = (FreeTrialWsDTO) o;
		if (StringUtils.length(freeTrialWsDTO.getFirstName()) + StringUtils.length(freeTrialWsDTO.getLastName())
				> MAX_FIELD_LENGTH  - 1)
		{
			errors.rejectValue("firstName", "",
					"{validation.freetrial.name.invalid}");
			errors.rejectValue("lastName", "",
					"{validation.freetrial.name.invalid}");
		}
	}
}
