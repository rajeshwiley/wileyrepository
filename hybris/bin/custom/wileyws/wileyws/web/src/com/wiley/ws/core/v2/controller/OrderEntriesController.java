package com.wiley.ws.core.v2.controller;

import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.webservicescommons.mapping.FieldSetLevelHelper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.wiley.integrations.order.dto.OrderEntryWsDTO;
import com.wiley.ws.core.abstractorderentry.WileyWsAbstractOrderEntryFacade;
import com.wiley.ws.core.exceptions.UnknownResourceException;
import com.wiley.ws.core.order.dto.GetOrderEntriesResponseWsDTO;
import com.wiley.ws.core.orderEntry.WileyWsOrderEntryFacade;
import com.wiley.ws.core.orderEntry.data.UpdateOrderEntryInfoRequest;


@Controller
@RequestMapping(value = "/{baseSiteId}/orderentries")
public class OrderEntriesController extends BaseCommerceController
{
	private static final String DEFAULT_ORDERENTRIES_SORT = "byDate";

	@Autowired
	private WileyWsOrderEntryFacade wileyWsOrderEntryFacade;
	@Autowired
	private FieldSetLevelHelper fieldSetLevelHelper;
	@Autowired
	private WileyWsAbstractOrderEntryFacade wileyWsAbstractOrderEntryFacade;

	/**
	 * Search and return all OrderEntries according requested parameters.
	 *
	 * @param userId
	 * 		the user id
	 * @param businessItemId
	 * 		the Business Item ID
	 * @param businessKey
	 * 		the Business Key
	 * @param statuses
	 * 		statuses
	 * @param pageSize
	 * 		the size of the page, default value is 10
	 * @param currentPage
	 * 		current page, default value is 0
	 * @param sort
	 * 		the sort, available only 'byDate' value
	 * @param fieldsSetLevel
	 * 		fields set level
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public GetOrderEntriesResponseWsDTO searchOrderEntries(
			@RequestParam(required = false) final String userId,
			@RequestParam(required = false) final String businessItemId,
			@RequestParam(required = false) final String businessKey,
			@RequestParam(required = false) final List<String> statuses,
			@RequestParam(required = false, defaultValue = "10") final Integer pageSize,
			@RequestParam(required = false, defaultValue = "0") final Integer currentPage,
			@RequestParam(required = false, defaultValue = DEFAULT_ORDERENTRIES_SORT) final String sort,
			@RequestParam(required = false, defaultValue = DEFAULT_FIELD_SET) final String fieldsSetLevel)
	{
		final GetOrderEntriesResponseWsDTO response = wileyWsOrderEntryFacade.getOrderEntries(userId,
				businessItemId, businessKey, statuses, pageSize, currentPage, sort);

		List<OrderEntryWsDTO> mappedOrderEntries = new ArrayList<>();
		response.getResults().stream().forEach(orderEntry ->
		{
			mappedOrderEntries.add(dataMapper.map(orderEntry, OrderEntryWsDTO.class, fieldsSetLevel));
		});

		response.setResults(mappedOrderEntries);

		return response;
	}

	@RequestMapping(value = "/{entryId}/info", method = RequestMethod.POST)
	@ResponseBody
	@ResponseStatus(value = HttpStatus.OK)
	public OrderEntryWsDTO updateOrderEntry(@PathVariable final String entryId,
			@RequestBody final UpdateOrderEntryInfoRequest updateOrderEntryInfoRequest,
			@RequestHeader(required = false, name = "Fields-Set-Level",
					defaultValue = DEFAULT_FIELD_SET) final String fieldsSetLevel)
			throws UnknownResourceException
	{
		if (!fieldSetLevelHelper.isLevelName(fieldsSetLevel, OrderEntryModel.class))
		{
			throw new ConversionException("Incorrect field:'" + fieldsSetLevel + "'");
		}

		final OrderEntryWsDTO orderEntryWsDTO;
		try
		{
			orderEntryWsDTO =
					wileyWsOrderEntryFacade.checkAndUpdateOrderEntryDetails(entryId, updateOrderEntryInfoRequest);

		}
		catch (ModelNotFoundException e)
		{
			throw new UnknownResourceException("Cannot find entry with guid '" + entryId + "'");
		}

		return dataMapper.map(orderEntryWsDTO, OrderEntryWsDTO.class, fieldsSetLevel);
	}

	@RequestMapping(value = "/{entryId}", method = RequestMethod.GET)
	@ResponseBody
	@ResponseStatus(value = HttpStatus.OK)
	public OrderEntryWsDTO getOrderEntry(@PathVariable final String entryId, @PathVariable final String baseSiteId,
			@RequestParam(required = false, name = "fieldsSetLevel",
					defaultValue = DEFAULT_FIELD_SET) final String fieldsSetLevel)
			throws UnknownResourceException
	{
		final OrderEntryWsDTO orderEntryWsDTO;
		try
		{
			orderEntryWsDTO = wileyWsAbstractOrderEntryFacade.getEntryDetails(baseSiteId, entryId);
		}
		catch (UnknownIdentifierException | AmbiguousIdentifierException e)
		{
			throw new UnknownResourceException(String.format("Cannot find entry with guid '%s'", entryId), e);
		}

		return dataMapper.map(orderEntryWsDTO, OrderEntryWsDTO.class, fieldsSetLevel);
	}
}
