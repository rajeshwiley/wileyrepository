package com.wiley.ws.core.errors.converters;

import de.hybris.platform.webservicescommons.dto.error.ErrorWsDTO;
import de.hybris.platform.webservicescommons.errors.converters.AbstractLocalizedErrorConverter;

import java.util.List;

import com.wiley.core.wileyws.exceptions.WileyWsValidationException;


public class WileywsCountryRegionErrorConverter extends AbstractLocalizedErrorConverter
{
	private static final String VALIDATION = "validation";
	private static final String PARAMETER = "parameter";

	@Override
	public boolean supports(final Class clazz)
	{
		return WileyWsValidationException.class.isAssignableFrom(clazz);
	}

	@Override
	public void populate(final Object o, final List<ErrorWsDTO> errorWsListDto)
	{
		final WileyWsValidationException exception = (WileyWsValidationException) o;
		final ErrorWsDTO errorDto = createTargetElement();
		errorDto.setType(exception.getType());
		errorDto.setMessage(exception.getMessage());
		errorDto.setReason(VALIDATION);
		errorDto.setSubject(exception.getPath());
		errorDto.setSubjectType(PARAMETER);
		errorWsListDto.add(errorDto);
	}
}
