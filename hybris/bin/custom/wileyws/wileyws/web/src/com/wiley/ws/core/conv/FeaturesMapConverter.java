/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package com.wiley.ws.core.conv;

import java.util.Map;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.io.json.JsonWriter;


/**
 * The type Features map converter.
 *
 * @author michal.flasinski
 */
public class FeaturesMapConverter implements Converter
{


	/**
	 * Marshal.
	 *
	 * @param source
	 * 		the source
	 * @param writer
	 * 		the writer
	 * @param context
	 * 		the context
	 */
	@Override
	public void marshal(final Object source, final HierarchicalStreamWriter writer, final MarshallingContext context)
	{
		for (final Map.Entry<String, String> entry : ((Map<String, String>) source).entrySet())
		{
			writer.startNode("feature");
			if (writer instanceof JsonWriter)
			{
				((JsonWriter) writer).startNode("key", String.class);
				((JsonWriter) writer).setValue(entry.getKey());
				((JsonWriter) writer).endNode();

				((JsonWriter) writer).startNode("value", String.class);
				((JsonWriter) writer).setValue(entry.getValue());
				((JsonWriter) writer).endNode();
			}
			else
			{
				writer.addAttribute("key", entry.getKey());
				writer.addAttribute("value", entry.getValue() == null ? "" : entry.getValue());
			}
			writer.endNode();
		}
	}

	/**
	 * Can convert boolean.
	 *
	 * @param arg0
	 * 		the arg 0
	 * @return the boolean
	 */
/*
	 * (non-Javadoc)
	 * 
	 * @see com.thoughtworks.xstream.converters.ConverterMatcher#canConvert(java.lang.Class)
	 */
	@Override
	public boolean canConvert(final Class arg0)
	{
		return Map.class.isAssignableFrom(arg0);
	}

	/**
	 * Unmarshal object.
	 *
	 * @param arg0
	 * 		the arg 0
	 * @param arg1
	 * 		the arg 1
	 * @return the object
	 */
/*
	 * (non-Javadoc)
	 * 
	 * @see com.thoughtworks.xstream.converters.Converter#unmarshal(com.thoughtworks.xstream.io.HierarchicalStreamReader,
	 * com.thoughtworks.xstream.converters.UnmarshallingContext)
	 */
	@Override
	public Object unmarshal(final HierarchicalStreamReader arg0, final UnmarshallingContext arg1)
	{
		return null;
	}
}
