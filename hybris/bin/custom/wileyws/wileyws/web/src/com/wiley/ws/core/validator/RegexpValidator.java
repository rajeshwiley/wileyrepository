/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 */
package com.wiley.ws.core.validator;

import org.apache.commons.validator.routines.RegexValidator;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


/**
 * Validates one specific string property specified by {@link #fieldPath} in any object
 * if it is valid against given regular expression
 */
public class RegexpValidator implements Validator
{

	private String fieldPath;
	private String regularExpression;
	private String errorMessageID;

	/**
	 * Supports boolean.
	 *
	 * @param aClass
	 * 		the a class
	 * @return the boolean
	 */
	@Override
	public boolean supports(final Class<?> aClass)
	{
		return true;
	}

	/**
	 * Validate.
	 *
	 * @param o
	 * 		the o
	 * @param errors
	 * 		the errors
	 */
	@Override
	public void validate(final Object o, final Errors errors)
	{
		Assert.notNull(errors, "Errors object must not be null");
		final String fieldValue = (String) errors.getFieldValue(getFieldPath());

		RegexValidator validator = new RegexValidator(getRegularExpression());

		if (!validator.isValid(fieldValue))
		{
			errors.rejectValue(getFieldPath(), getErrorMessageID(), new String[] { getFieldPath() }, null);
		}
	}


	/**
	 * Sets field path.
	 *
	 * @param fieldPath
	 * 		the field path
	 */
	@Required
	public void setFieldPath(final String fieldPath)
	{
		this.fieldPath = fieldPath;
	}

	/**
	 * Gets field path.
	 *
	 * @return the field path
	 */
	public String getFieldPath()
	{
		return this.fieldPath;
	}

	/**
	 * Sets regular expression.
	 *
	 * @param regularExpression
	 * 		the regular expression
	 */
	@Required
	public void setRegularExpression(final String regularExpression)
	{
		this.regularExpression = regularExpression;
	}

	/**
	 * Gets regular expression.
	 *
	 * @return the regular expression
	 */
	public String getRegularExpression()
	{
		return this.regularExpression;
	}

	/**
	 * Sets error message id.
	 *
	 * @param errorMessageID
	 * 		the error message id
	 */
	@Required
	public void setErrorMessageID(final String errorMessageID)
	{
		this.errorMessageID = errorMessageID;
	}

	/**
	 * Gets error message id.
	 *
	 * @return the error message id
	 */
	public String getErrorMessageID()
	{
		return this.errorMessageID;
	}



}
