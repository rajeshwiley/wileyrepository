/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package com.wiley.ws.core.conv;

import de.hybris.platform.commercefacades.search.data.SearchQueryData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.core.convert.converter.Converter;

import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;


/**
 * SearchQuery data converter renders a {@link SolrSearchQueryData} as
 *
 * <pre>
 * {@code
 * <currentQuery>a:relevance</currentQuery>
 * }***
 * </pre>
 */
public class SearchQueryDataConverter extends AbstractRedirectableConverter
{
	private Converter<SolrSearchQueryData, SearchQueryData> solrSearchQueryEncoder;

	/**
	 * Gets solr search query encoder.
	 *
	 * @return the solr search query encoder
	 */
	protected Converter<SolrSearchQueryData, SearchQueryData> getSolrSearchQueryEncoder()
	{
		return solrSearchQueryEncoder;
	}

	/**
	 * Sets solr search query encoder.
	 *
	 * @param solrSearchQueryEncoder
	 * 		the solr search query encoder
	 */
	@Required
	public void setSolrSearchQueryEncoder(final Converter<SolrSearchQueryData, SearchQueryData> solrSearchQueryEncoder)
	{
		this.solrSearchQueryEncoder = solrSearchQueryEncoder;
	}

	/**
	 * Can convert boolean.
	 *
	 * @param type
	 * 		the type
	 * @return the boolean
	 */
	@Override
	public boolean canConvert(final Class type)
	{
		return type == SolrSearchQueryData.class;
	}

	/**
	 * Marshal.
	 *
	 * @param source
	 * 		the source
	 * @param writer
	 * 		the writer
	 * @param context
	 * 		the context
	 */
	@Override
	public void marshal(final Object source, final HierarchicalStreamWriter writer, final MarshallingContext context)
	{
		final String query = getSolrSearchQueryEncoder().convert((SolrSearchQueryData) source).getValue();
		writer.setValue(query);
	}

	/**
	 * Unmarshal object.
	 *
	 * @param reader
	 * 		the reader
	 * @param context
	 * 		the context
	 * @return the object
	 */
	@Override
	public Object unmarshal(final HierarchicalStreamReader reader, final UnmarshallingContext context)
	{
		return getTargetConverter().unmarshal(reader, context);
	}

	/**
	 * Gets converted class.
	 *
	 * @return the converted class
	 */
	@Override
	public Class getConvertedClass()
	{
		return SolrSearchQueryData.class;
	}
}
