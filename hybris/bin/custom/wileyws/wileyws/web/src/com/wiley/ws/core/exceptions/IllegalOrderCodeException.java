package com.wiley.ws.core.exceptions;

/**
 * Created by Anton_Lukyanau on 8/15/2016.
 */
public class IllegalOrderCodeException extends IllegalArgumentException
{
	public IllegalOrderCodeException(final String message) {
		super(message);
	}
}
