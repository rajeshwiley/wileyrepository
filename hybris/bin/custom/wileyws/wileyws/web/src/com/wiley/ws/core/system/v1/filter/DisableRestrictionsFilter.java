package com.wiley.ws.core.system.v1.filter;

import de.hybris.platform.search.restriction.SearchRestrictionService;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.springframework.beans.factory.annotation.Required;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public class DisableRestrictionsFilter implements Filter
{

	private SearchRestrictionService searchRestrictionService;

	@Override
	public void init(final FilterConfig filterConfig) throws ServletException
	{

	}

	/**
	 * Disable flexible search restrictions for each web service call
	 *
	 * @param request
	 * @param response
	 * @param filterChain
	 * @throws IOException
	 * @throws ServletException
	 */
	@Override
	public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain filterChain)
			throws IOException, ServletException
	{
		searchRestrictionService.disableSearchRestrictions();
		filterChain.doFilter(request, response);
	}

	@Override
	public void destroy()
	{

	}

	@Required
	public void setSearchRestrictionService(final SearchRestrictionService searchRestrictionService)
	{
		this.searchRestrictionService = searchRestrictionService;
	}
}
