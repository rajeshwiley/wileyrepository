/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package com.wiley.ws.core.exceptions;

import de.hybris.platform.core.model.c2l.LanguageModel;

import javax.servlet.ServletException;


/**
 * The type Unsupported language exception.
 *
 * @author krzysztof.kwiatosz
 */
public class UnsupportedLanguageException extends ServletException
{

	private LanguageModel language;

	/**
	 * Instantiates a new Unsupported language exception.
	 *
	 * @param languageToSet
	 * 		the language to set
	 */
	public UnsupportedLanguageException(final LanguageModel languageToSet)
	{
		super("Language " + languageToSet + " is not supported by the current base store");
		this.language = languageToSet;
	}

	/**
	 * Instantiates a new Unsupported language exception.
	 *
	 * @param msg
	 * 		the msg
	 */
	public UnsupportedLanguageException(final String msg)
	{
		super(msg);
	}

	/**
	 * Gets language.
	 *
	 * @return the language
	 */
	public LanguageModel getLanguage()
	{
		return language;
	}
}
