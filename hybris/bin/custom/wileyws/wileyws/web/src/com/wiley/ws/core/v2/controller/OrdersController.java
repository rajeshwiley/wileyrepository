/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.ws.core.v2.controller;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.order.OrderFacade;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.OrderHistoriesData;
import de.hybris.platform.commercewebservicescommons.dto.order.OrderHistoryListWsDTO;
import de.hybris.platform.commercewebservicescommons.strategies.CartLoaderStrategy;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.webservicescommons.cache.CacheControl;
import de.hybris.platform.webservicescommons.cache.CacheControlDirective;
import de.hybris.platform.webservicescommons.errors.exceptions.WebserviceValidationException;

import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.wiley.integrations.alm.strategy.AlmUserAutoCreationStrategy;
import com.wiley.integrations.order.dto.CreateOrderEntryRequestWsDTO;
import com.wiley.integrations.order.dto.CreateOrderRequestWsDTO;
import com.wiley.integrations.order.dto.OrderEntryWsDTO;
import com.wiley.integrations.order.dto.OrderWsDTO;
import com.wiley.integrations.order.importing.WileyOrderImportFacade;
import com.wiley.ws.core.abstractorderentry.WileyWsAbstractOrderEntryFacade;
import com.wiley.ws.core.exceptions.NoCheckoutCartException;
import com.wiley.ws.core.exceptions.PaymentAuthorizationException;
import com.wiley.ws.core.exceptions.UnknownResourceException;
import com.wiley.ws.core.order.WileyWsOrderFacade;
import com.wiley.ws.core.v2.helper.OrdersHelper;

import static com.wiley.core.constants.WileyCoreConstants.ALM_AUTO_CREATION_STRATEGY;


/**
 * Web Service Controller for the ORDERS resource. Most methods check orders of the user. Methods require authentication
 * and are restricted to https channel.
 *
 * @pathparam code Order GUID (Globally Unique Identifier) or order CODE
 * @pathparam userId User identifier or one of the literals below : <ul> <li>'current' for currently authenticated user</li>
 * <li>'anonymous' for anonymous user</li> </ul>
 */
@Controller
@RequestMapping(value = "/{baseSiteId}")
public class OrdersController extends BaseCommerceController
{
	private static final Logger LOG = Logger.getLogger(OrdersController.class);

	@Resource(name = "orderFacade")
	private OrderFacade orderFacade;
	@Resource(name = "cartLoaderStrategy")
	private CartLoaderStrategy cartLoaderStrategy;
	@Resource
	private WileyWsOrderFacade wileyWsOrderFacade;
	@Resource
	private WileyWsAbstractOrderEntryFacade wileyWsAbstractOrderEntryFacade;
	@Resource
	private WileyOrderImportFacade wileyOrderImportFacade;
	@Resource(name = "wileyasUserService")
	private UserService userService;
	@Resource(name = "baseSiteService")
	private BaseSiteService baseSiteService;
	@Resource(name = "ordersHelper")
	private OrdersHelper ordersHelper;
	@Resource
	private AlmUserAutoCreationStrategy almUserAutoCreationStrategy;
	@Resource
	private BusinessProcessService businessProcessService;
	@Resource
	private Validator createCartRequestWsDTOValidator;

	/**
	 * Returns details of a specific order based on order GUID (Globally Unique Identifier). The response
	 * contains a detailed order information.
	 *
	 * @param guid
	 * 		the guid
	 * @param baseSiteId
	 * 		the baseSite id
	 * @param fieldsSetLevel
	 * 		the fields
	 * @return
	 * @queryparam fields Response configuration (list of fields, which should be returned in response)
	 */
	@RequestMapping(value = "/orders/{guid}", method = RequestMethod.GET)
	@ResponseBody
	public OrderWsDTO getOrder(@PathVariable final String guid, @PathVariable final String baseSiteId,
			@RequestParam(required = false, defaultValue = DEFAULT_FIELD_SET) final String fieldsSetLevel)
			throws UnknownResourceException
	{
		BaseSiteModel baseSite = baseSiteService.getBaseSiteForUID(baseSiteId);

		OrderWsDTO orderWsDTO;
		try
		{
			orderWsDTO = wileyWsOrderFacade.getOrderDetails(guid, baseSite);
		}
		catch (ModelNotFoundException e)
		{
			throw new UnknownResourceException("Cannot find order with guid '" + guid + "'");
		}

		return dataMapper.map(orderWsDTO, OrderWsDTO.class, fieldsSetLevel);
	}

	/**
	 * Returns specific order details based on a specific order code. The response contains detailed order information.
	 *
	 * @param code
	 * 		the code
	 * @param fields
	 * 		the fields
	 * @return Order data
	 * @queryparam fields Response configuration (list of fields, which should be returned in response)
	 */
	@Secured({ "ROLE_CUSTOMERGROUP", "ROLE_CLIENT", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/users/{userId}/orders/{code}", method = RequestMethod.GET)
	@CacheControl(directive = CacheControlDirective.PUBLIC, maxAge = 120)
	@Cacheable(value = "orderCache", key = "T(de.hybris.platform.commercewebservicescommons.cache.CommerceCacheKeyGenerator)"
			+ ".generateKey(true,true,'getOrderForUserByCode',#code,#fields)")
	@ResponseBody
	public OrderWsDTO getOrderForUserByCode(@PathVariable final String code,
			@RequestParam(defaultValue = DEFAULT_FIELD_SET) final String fields)
	{
		final OrderData orderData = orderFacade.getOrderDetailsForCode(code);
		final OrderWsDTO dto = dataMapper.map(orderData, OrderWsDTO.class, fields);
		return dto;
	}

	/**
	 * Returns order history data for all orders placed by the specific user for the specific base store. Response
	 * contains orders search result displayed in several pages if needed.
	 *
	 * @param statuses
	 * 		the statuses
	 * @param currentPage
	 * 		the current page
	 * @param pageSize
	 * 		the page size
	 * @param sort
	 * 		the sort
	 * @param fields
	 * 		the fields
	 * @param response
	 * 		the response
	 * @return Order history data.
	 * @queryparam statuses Filters only certain order statuses. It means: statuses=CANCELLED,CHECKED_VALID would only return
	 * orders with status CANCELLED or CHECKED_VALID.
	 * @queryparam currentPage The current result page requested.
	 * @queryparam pageSize The number of results returned per page.
	 * @queryparam sort Sorting method applied to the return results.
	 * @queryparam fields Response configuration (list of fields, which should be returned in response)
	 */
	@Secured({ "ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@CacheControl(directive = CacheControlDirective.PUBLIC, maxAge = 120)
	@RequestMapping(value = "/users/{userId}/orders", method = RequestMethod.GET)
	@ResponseBody
	public OrderHistoryListWsDTO getOrdersForUser(@RequestParam(required = false) final String statuses,
			@RequestParam(required = false, defaultValue = DEFAULT_CURRENT_PAGE) final int currentPage,
			@RequestParam(required = false, defaultValue = DEFAULT_PAGE_SIZE) final int pageSize,
			@RequestParam(required = false) final String sort,
			@RequestParam(defaultValue = DEFAULT_FIELD_SET) final String fields, final HttpServletResponse response)
	{
		validateStatusesEnumValue(statuses);

		final OrderHistoryListWsDTO orderHistoryList = ordersHelper.searchOrderHistory(statuses, currentPage, pageSize, sort,
				addPaginationField(fields));

		// X-Total-Count header
		setTotalCountHeader(response, orderHistoryList.getPagination());

		return orderHistoryList;
	}

	/**
	 * Returns {@value  com.wiley.ws.core.v2.controller.BaseController#HEADER_TOTAL_COUNT} header with a total number of results
	 * (orders history for all orders placed by the specific user for the specific base store).
	 *
	 * @param statuses
	 * 		the statuses
	 * @param response
	 * 		the response
	 * @queryparam statuses Filters only certain order statuses. It means: statuses=CANCELLED,CHECKED_VALID would only return
	 * orders with status CANCELLED or CHECKED_VALID.
	 */
	@Secured({ "ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/users/{userId}/orders", method = RequestMethod.HEAD)
	@ResponseBody
	public void getCountOrdersForUser(@RequestParam(required = false) final String statuses, final HttpServletResponse response)
	{
		final OrderHistoriesData orderHistoriesData = ordersHelper.searchOrderHistory(statuses, 0, 1, null);

		setTotalCountHeader(response, orderHistoriesData.getPagination());
	}

	/**
	 * Authorizes cart and places the order. Response contains the new order data.
	 *
	 * @param cartId
	 * 		the cart id
	 * @param securityCode
	 * 		the security code
	 * @param fields
	 * 		the fields
	 * @return Created order data
	 * @throws PaymentAuthorizationException
	 * 		When there are problems with the payment authorization. For example: there is no session cart or no payment
	 * 		information set for the cart.
	 * @throws InvalidCartException
	 * 		the invalid cart exception
	 * @throws WebserviceValidationException
	 * 		When the cart is not filled properly (e. g. delivery mode is not set, payment method is not set)
	 * @throws NoCheckoutCartException
	 * 		the no checkout cart exception
	 * @formparam cartId Cart code for logged in user, cart GUID for guest checkout
	 * @formparam securityCode CCV security code.
	 * @queryparam fields Response configuration (list of fields, which should be returned in response)
	 */
	@Secured({ "ROLE_CUSTOMERGROUP", "ROLE_CLIENT", "ROLE_CUSTOMERMANAGERGROUP", "ROLE_TRUSTED_CLIENT" })
	@RequestMapping(value = "/users/{userId}/orders", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	@ResponseBody
	public OrderWsDTO placeOrder(@RequestParam(required = true) final String cartId,
			@RequestParam(required = false) final String securityCode,
			@RequestParam(defaultValue = DEFAULT_FIELD_SET) final String fields)
			throws PaymentAuthorizationException, InvalidCartException, WebserviceValidationException,
			NoCheckoutCartException
	{
		if (LOG.isDebugEnabled())
		{
			LOG.info("placeOrder");
		}

		cartLoaderStrategy.loadCart(cartId);

		validateCartForPlaceOrder();

		//authorize
		if (!checkoutFacade.authorizePayment(securityCode))
		{
			throw new PaymentAuthorizationException();
		}

		//placeorder
		final OrderData orderData = checkoutFacade.placeOrder();
		final OrderWsDTO dto = dataMapper.map(orderData, OrderWsDTO.class, fields);
		return dto;
	}

	@RequestMapping(value = "/orders", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity createOrder(@PathVariable final String baseSiteId,
			@Valid @RequestBody final CreateOrderRequestWsDTO order,
			@RequestHeader(required = false, name = "Fields-Set-Level", defaultValue = "DEFAULT") final String fields,
			@RequestHeader(required = false, name = "User-Auto-Creation-Strategy") final String autoUserCreationStrategy)
			throws WebserviceValidationException
	{
		validate(order, "order", createCartRequestWsDTOValidator);
		for (CreateOrderEntryRequestWsDTO entry : order.getEntries())
		{
			if (entry.getBusinessKey() != null && !OrderStatus.CANCELLED.getCode().equals(entry.getStatus()))
			{
				final Optional<OrderEntryWsDTO> existingOrderEntryWsDTO = wileyWsAbstractOrderEntryFacade.getEntryForBusinessKey(
						entry.getBusinessKey());

				if (existingOrderEntryWsDTO.isPresent())
				{
					return new ResponseEntity<>(dataMapper.map(existingOrderEntryWsDTO.get(), OrderEntryWsDTO.class, fields),
							HttpStatus.CONFLICT);
				}
			}

		}
		String userId = order.getUserId();
		if (ALM_AUTO_CREATION_STRATEGY.equals(autoUserCreationStrategy)
				&& StringUtils.isNotBlank(userId) && !userService.isUserExisting(userId))
		{
			almUserAutoCreationStrategy.createNewUserByAlmId(userId);
		}
		final Pair<OrderWsDTO, OrderProcessModel> pair =
				wileyOrderImportFacade.importOrderAndCreateFulfilment(order, baseSiteId);
		businessProcessService.startProcess(pair.getValue());
		return new ResponseEntity<>(dataMapper.map(pair.getKey(), OrderWsDTO.class, fields), HttpStatus.CREATED);
	}
}
