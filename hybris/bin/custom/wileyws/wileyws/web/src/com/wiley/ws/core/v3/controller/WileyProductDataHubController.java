package com.wiley.ws.core.v3.controller;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.Size;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.wiley.facades.product.ProductUpdateResult;
import com.wiley.ws.core.product.data.hub.dto.ProductSwgDTO;
import com.wiley.ws.core.product.data.hub.service.WileyProductDataHubFacade;
import com.wiley.ws.core.v2.controller.BaseController;

@Deprecated
public class WileyProductDataHubController extends BaseController
{
	@Resource
	private WileyProductDataHubFacade wileyProductDataHubFacade;

	public ResponseEntity<Void> createOrUpdateProduct(@PathVariable("productCode") @Size(max = 255) final String productCode,
			@Valid @RequestBody final ProductSwgDTO product)
	{
		ProductUpdateResult result = wileyProductDataHubFacade.createOrUpdateProductForSite(productCode, product);

		if (result == ProductUpdateResult.CREATED)
		{
			return new ResponseEntity<>(HttpStatus.CREATED);
		}

		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

}
