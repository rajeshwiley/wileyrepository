package com.wiley.ws.core.v2.strategies;

import de.hybris.platform.commercewebservicescommons.errors.exceptions.CartException;
import de.hybris.platform.commercewebservicescommons.strategies.impl.DefaultCartLoaderStrategy;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.user.UserService;

import org.springframework.beans.factory.annotation.Required;


/**
 * Strategy that only checks if cart exists and NOT load it to session
 */
public class WileyasCartLoaderStrategy extends DefaultCartLoaderStrategy
{
	private static final String CART_NOT_FOUND_MESSAGE = "Cart not found.";
	private static final String CART_INVALID_USER_MESSAGE = "Invalid cart user.";

	private UserService wileyasUserService;

	/**
	 * Loads customer's cart without cart restoration
	 *
	 * @param cartID
	 */
	@Override
	protected void loadUserCart(final String cartID, final boolean refresh)
	{
		CartModel cart = getCommerceCartService().getCartForGuidAndSiteAndUser(cartID,
				getBaseSiteService().getCurrentBaseSite(), getWileyasUserService().getCurrentUser());
		if (cart == null)
		{
			cart = getCommerceCartService().getCartForGuidAndSite(cartID, getBaseSiteService().getCurrentBaseSite());
			if (cart == null)
			{
				throw new CartException(CART_NOT_FOUND_MESSAGE, CartException.NOT_FOUND, cartID);
			}
			else
			{
				throw new CartException(CART_INVALID_USER_MESSAGE, CartException.INVALID, cartID);
			}
		}
	}

	public UserService getWileyasUserService()
	{
		return wileyasUserService;
	}

	@Required
	public void setWileyasUserService(final UserService wileyasUserService)
	{
		this.wileyasUserService = wileyasUserService;
	}
}
