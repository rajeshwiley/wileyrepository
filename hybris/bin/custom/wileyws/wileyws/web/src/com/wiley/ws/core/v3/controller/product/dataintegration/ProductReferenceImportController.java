package com.wiley.ws.core.v3.controller.product.dataintegration;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.wiley.ws.core.product.dataintegration.productrelation.dto.ProductRelationSwgDTO;
import com.wiley.ws.core.product.dataintegration.productrelation.facade.ProductReferenceImportFacade;

import static com.wiley.ws.core.constants.WileyWSConstants.APPLICATION_TYPE_JSON;


@RestController
@RequestMapping(value = "/products", produces = { APPLICATION_TYPE_JSON }, consumes = { APPLICATION_TYPE_JSON })
@Secured("ROLE_TRUSTED_CLIENT")
@Validated
public class ProductReferenceImportController extends AbstractDataImportController
{
	private static final Logger LOG = LoggerFactory.getLogger(ProductReferenceImportController.class);

	private final ProductReferenceImportFacade productReferenceImportFacade;

	@Autowired
	public ProductReferenceImportController(final ProductReferenceImportFacade productReferenceImportFacade)
	{
		this.productReferenceImportFacade = productReferenceImportFacade;
	}

	@RequestMapping(value = "/{productId}/variants/{variantId}/references", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.CREATED)
	public void createProductRelation(
			@PathVariable("productId") final String productId,
			@PathVariable("variantId") final String variantId,
			@Valid @RequestBody final List<ProductRelationSwgDTO> productReferenceDTOs
	)
	{
		LOG.info("Creating product reference for ProductOption: [{}], of BaseProduct: [{}]", variantId, productId);
		productReferenceImportFacade.createProductReference(productReferenceDTOs, productId, variantId);
	}
}
