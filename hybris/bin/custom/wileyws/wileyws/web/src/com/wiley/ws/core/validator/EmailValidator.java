/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package com.wiley.ws.core.validator;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


/**
 * Validates one specific string property specified by {@link #fieldPath} in any object if it is valid email address
 */
public class EmailValidator implements Validator
{
	private static final String INVALID_EMAIL_MESSAGE_ID = "field.invalidEmail";
	private String fieldPath;

	/**
	 * Supports boolean.
	 *
	 * @param aClass
	 * 		the a class
	 * @return the boolean
	 */
	@Override
	public boolean supports(final Class<?> aClass)
	{
		return true;
	}

	/**
	 * Validate.
	 *
	 * @param o
	 * 		the o
	 * @param errors
	 * 		the errors
	 */
	@Override
	public void validate(final Object o, final Errors errors)
	{
		Assert.notNull(errors, "Errors object must not be null");
		final String fieldValue = (String) errors.getFieldValue(fieldPath);

		if (!org.apache.commons.validator.routines.EmailValidator.getInstance().isValid(fieldValue))
		{
			errors.rejectValue(fieldPath, INVALID_EMAIL_MESSAGE_ID, new String[] { fieldPath }, null);
		}

	}

	/**
	 * Gets field path.
	 *
	 * @return the field path
	 */
	public String getFieldPath()
	{
		return fieldPath;
	}

	/**
	 * Sets field path.
	 *
	 * @param fieldPath
	 * 		the field path
	 */
	@Required
	public void setFieldPath(final String fieldPath)
	{
		this.fieldPath = fieldPath;
	}
}
