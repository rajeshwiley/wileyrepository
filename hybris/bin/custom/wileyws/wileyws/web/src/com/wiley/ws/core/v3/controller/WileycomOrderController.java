package com.wiley.ws.core.v3.controller;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.Size;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.wiley.ws.core.order.WileycomWsOrderFacade;
import com.wiley.ws.core.order.dto.OrderStatusWsDTO;
import com.wiley.ws.core.order.dto.OrderUpdateRequestWsDTO;
import com.wiley.ws.core.order.dto.ShippingEventWsDTO;



/**
 * The Wileycom order controller.
 */
@Controller
@RequestMapping(value = "/orders")
@Validated
public class WileycomOrderController
{

	@Resource(name = "wileycomWsOrderFacade")
	private WileycomWsOrderFacade wsOrderFacade;


	/**
	 * Update order status or cancel.
	 *
	 * @param code
	 * 		the Order code
	 * @param orderStatusWsDTO
	 * 		the order status ws dto
	 * @return the response entity
	 */
	@Secured("ROLE_TRUSTED_CLIENT")
	@RequestMapping(value = "/{code}/status", method = RequestMethod.PUT, consumes = {
			MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity updateOrderStatus(@PathVariable @Size(max = 255) final String code,
			@Valid @RequestBody final OrderStatusWsDTO orderStatusWsDTO)
	{
		wsOrderFacade.updateStatusAndAddHistoryEntry(code, orderStatusWsDTO);
		return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
	}

	@Secured("ROLE_TRUSTED_CLIENT")
	@RequestMapping(value = "/{code}", method = RequestMethod.POST,
			consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity updateOrder(@PathVariable @Size(max = 255) final String code,
			@Valid @RequestBody final OrderUpdateRequestWsDTO orderUpdateRequestWsDTO)
	{
		wsOrderFacade.updateOrder(code, orderUpdateRequestWsDTO);
		return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
	}

	/**
	 * Update order shipping details.
	 *
	 * @param code
	 * 		the code
	 * @param shippingEventWsDTO
	 * 		the shipping event ws dto
	 * @return the response entity
	 */
	@Secured("ROLE_TRUSTED_CLIENT")
	@RequestMapping(value = "/{code}/shipping", method = RequestMethod.PUT, consumes = {
			MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<Void> updateOrderShippingDetails(@PathVariable @Size(max = 255) final String code,
			@Valid @RequestBody final ShippingEventWsDTO shippingEventWsDTO
	)
	{
		wsOrderFacade.updateShipping(code, shippingEventWsDTO);
		return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
	}

}
