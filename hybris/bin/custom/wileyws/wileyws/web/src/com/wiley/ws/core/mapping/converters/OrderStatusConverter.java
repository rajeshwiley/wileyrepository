/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package com.wiley.ws.core.mapping.converters;

import de.hybris.platform.webservicescommons.mapping.WsDTOMapping;
import de.hybris.platform.core.enums.OrderStatus;

import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;


/**
 * Bidirectional converter between {@link OrderStatus} and String
 */
@WsDTOMapping
public class OrderStatusConverter extends BidirectionalConverter<OrderStatus, String>
{
	@Override
	public String convertTo(final OrderStatus orderStatus, final Type<String> type, final MappingContext mappingContext)
	{
		return orderStatus.toString();
	}

	@Override
	public OrderStatus convertFrom(final String s, final Type<OrderStatus> type, final MappingContext mappingContext)
	{
		return OrderStatus.valueOf(s);
	}
}
