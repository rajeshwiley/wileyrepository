package com.wiley.ws.core.data;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TaskEngineActiveStatusDto
{
	@NotNull
	private Boolean active;

	@JsonProperty("active")
	public Boolean getActive()
	{
		return active;
	}

	public void setActive(final Boolean active)
	{
		this.active = active;
	}
}
