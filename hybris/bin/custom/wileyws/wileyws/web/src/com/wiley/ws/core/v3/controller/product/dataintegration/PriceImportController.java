package com.wiley.ws.core.v3.controller.product.dataintegration;

import static com.wiley.ws.core.constants.WileyWSConstants.APPLICATION_TYPE_JSON;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.Size;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wiley.ws.core.product.dataintegration.price.dto.PriceSwgDTO;
import com.wiley.ws.core.product.dataintegration.price.facade.PriceImportFacade;


@RestController
@RequestMapping(value = "/prices", produces = { APPLICATION_TYPE_JSON }, consumes = { APPLICATION_TYPE_JSON })
@Secured("ROLE_TRUSTED_CLIENT")
@Validated
public class PriceImportController extends AbstractDataImportController
{

	private static final Logger LOG = LoggerFactory.getLogger(PriceImportController.class);

	@Autowired
	PriceImportFacade priceImportFacade;

	@RequestMapping(value = "/{priceId}", method = RequestMethod.PUT)
	public ResponseEntity<?> updateOrCreatePrice(@PathVariable("priceId") @Size(max = 255) final String priceId,
			@Valid @RequestBody final PriceSwgDTO priceSwgDTO, final HttpServletRequest request)
	{
		LOG.debug("Updating or Creating price for : {}", priceSwgDTO);
		priceSwgDTO.setId(priceId);
		ResponseEntity<?> responseEntity;
		if (priceImportFacade.isPriceRowAlreadyExist(priceId, priceSwgDTO))
		{
			priceImportFacade.updatePrice(priceId, priceSwgDTO);
			responseEntity = new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		else
		{
			priceImportFacade.createPrice(priceId, priceSwgDTO);
			final HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.add("Location", request.getRequestURL().toString());
			responseEntity = new ResponseEntity<>(responseHeaders, HttpStatus.CREATED);
		}
		return responseEntity;
	}

}
