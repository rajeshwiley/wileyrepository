package com.wiley.ws.core.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.processengine.model.DynamicProcessDefinitionModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.springframework.util.Assert;

import com.wiley.ws.core.data.ProcessDefinitionDto;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public class WileyProcessDefinitionPopulator
		implements Populator<DynamicProcessDefinitionModel, ProcessDefinitionDto>
{
	@Override
	public void populate(final DynamicProcessDefinitionModel source,
			final ProcessDefinitionDto target)
			throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setCode(source.getCode());
		target.setContent(source.getContent());
		target.setId(source.getPk().getLong());
		target.setActive(source.getActive());
		target.setVersion(source.getVersion());
		target.setChecksum(source.getChecksum());
	}
}
