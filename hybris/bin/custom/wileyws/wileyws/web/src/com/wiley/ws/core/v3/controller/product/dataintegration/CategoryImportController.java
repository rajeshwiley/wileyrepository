package com.wiley.ws.core.v3.controller.product.dataintegration;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.wiley.ws.core.product.dataintegration.category.dto.CategorySwgDTO;
import com.wiley.ws.core.product.dataintegration.category.dto.UpdateCategorySwgDTO;
import com.wiley.ws.core.product.dataintegration.category.facade.CategoryImportFacade;

import static com.wiley.ws.core.constants.WileyWSConstants.APPLICATION_TYPE_JSON;


@RestController
@RequestMapping(value = "/categories", produces = { APPLICATION_TYPE_JSON }, consumes = { APPLICATION_TYPE_JSON })
@Secured("ROLE_TRUSTED_CLIENT")
@Validated
public class CategoryImportController extends AbstractDataImportController
{
	private static final String HTTP_HEADER_LOCATION = "Location";
	
	@Autowired
	private CategoryImportFacade categoryImportFacade;

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity createCategory(@Valid @RequestBody final CategorySwgDTO category, final HttpServletRequest request)
	{
		categoryImportFacade.createCategory(category);
		return buildResponseEntity(request, category.getId());
	}

	private ResponseEntity buildResponseEntity(final HttpServletRequest httpRequest, final String categoryId)
	{
		final HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add(HTTP_HEADER_LOCATION, httpRequest.getRequestURL() + categoryId);
		return new ResponseEntity(httpHeaders, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{categoryId}", method = RequestMethod.PUT)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void updateCategory(@PathVariable("categoryId") final String categoryId,
			@Valid @RequestBody final UpdateCategorySwgDTO category)
	{
		categoryImportFacade.updateCategory(categoryId, category);
	}

	@RequestMapping(value = "/{categoryId}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteCategory(@PathVariable("categoryId") final String categoryId)
	{
		categoryImportFacade.deleteCategory(categoryId);
	}
}
