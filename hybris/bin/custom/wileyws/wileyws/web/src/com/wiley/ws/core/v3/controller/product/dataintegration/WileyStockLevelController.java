package com.wiley.ws.core.v3.controller.product.dataintegration;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.Size;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wiley.ws.core.inventory.InventoryUpdateInfo;
import com.wiley.ws.core.inventory.WileycomWsStockLevelFacade;
import com.wiley.ws.core.inventory.dto.InventoryUpdateRequestWsDTO;

import static com.wiley.ws.core.constants.WileyWSConstants.APPLICATION_TYPE_JSON;


/**
 * An inbound interface to apply changes in inventory level
 */
@RestController
@RequestMapping(value = "/inventory", produces = { APPLICATION_TYPE_JSON }, consumes = { APPLICATION_TYPE_JSON })
@Secured("ROLE_TRUSTED_CLIENT")
@Validated
public class WileyStockLevelController extends AbstractDataImportController
{
	@Resource
	private WileycomWsStockLevelFacade wileycomWsStockLevelFacade;

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT,
			consumes = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity updateStockLevel(@PathVariable @Size(max = 255) final String id,
			@Valid @RequestBody final InventoryUpdateRequestWsDTO inventoryUpdateRequestWsDTO)
	{
		ResponseEntity responseEntity;

		InventoryUpdateInfo inventoryUpdateInfo = wileycomWsStockLevelFacade.updateStockLevel(id, inventoryUpdateRequestWsDTO);
		if (InventoryUpdateInfo.CREATED.equals(inventoryUpdateInfo))
		{
			responseEntity = ResponseEntity.status(HttpStatus.CREATED).build();
		}
		else
		{
			responseEntity = ResponseEntity.status(HttpStatus.NO_CONTENT).build();
		}

		return responseEntity;
	}


}
