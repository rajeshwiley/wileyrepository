/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.ws.core.v2.filter;

import com.wiley.integrations.alm.strategy.AlmUserAutoCreationStrategy;
import com.wiley.ws.core.exceptions.UnknownResourceException;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.user.UserService;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.context.MessageSource;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.wiley.core.constants.WileyCoreConstants.ALM_AUTO_CREATION_STRATEGY;


/**
 * Filter that puts user from the requested url into the session.
 */
public class UserMatchingFilter extends AbstractUrlMatchingFilter
{
	/**
	 * The constant ROLE_ANONYMOUS.
	 */
	public static final String ROLE_ANONYMOUS = "ROLE_ANONYMOUS";
	/**
	 * The constant ROLE_CUSTOMERGROUP.
	 */
	public static final String ROLE_CUSTOMERGROUP = "ROLE_CUSTOMERGROUP";
	/**
	 * The constant ROLE_CUSTOMERMANAGERGROUP.
	 */
	public static final String ROLE_CUSTOMERMANAGERGROUP = "ROLE_CUSTOMERMANAGERGROUP";
	/**
	 * The constant ROLE_TRUSTED_CLIENT.
	 */
	public static final String ROLE_TRUSTED_CLIENT = "ROLE_TRUSTED_CLIENT";
	public static final String ROLE_EXTERNAL_CLIENT = "ROLE_EXTERNAL_CLIENT";
	private static final String CURRENT_USER = "current";
	private static final String ANONYMOUS_USER = "anonymous";
	public static final String PATH_USER_ID_INVALID_KEY = "path.userId.invalid";
	private String regexp;
	private UserService userService;
	private I18NService i18NService;
	private MessageSource messageSource;
	private AlmUserAutoCreationStrategy almUserAutoCreationStrategy;

	/**
	 * Do filter internal.
	 *
	 * @param request
	 * 		the request
	 * @param response
	 * 		the response
	 * @param filterChain
	 * 		the filter chain
	 * @throws ServletException
	 * 		the servlet exception
	 * @throws IOException
	 * 		the io exception
	 */
	@Override
	protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response,
			final FilterChain filterChain) throws ServletException, IOException
	{
		final Authentication auth = getAuth();

		final String userID = getValue(request, regexp);

		if (userID == null)
		{
			if (hasRole(ROLE_CUSTOMERGROUP, auth) || hasRole(ROLE_CUSTOMERMANAGERGROUP, auth))
			{
				setCurrentUser((String) auth.getPrincipal());
			}
			else
			{
				// fallback to anonymous
				setCurrentUser(userService.getAnonymousUser());
			}
		}
		else if (ANONYMOUS_USER.equals(userID))
		{
			setCurrentUser(userService.getAnonymousUser());
		}
		else if (!userService.isUserExisting(userID))
		{
			final String autoCreationStrategy = request.getHeader("User-Auto-Creation-Strategy");
			boolean throwException = true;

			if (!StringUtils.isEmpty(autoCreationStrategy)
				&& autoCreationStrategy.equals(ALM_AUTO_CREATION_STRATEGY))
			{
				throwException = ObjectUtils.isEmpty(almUserAutoCreationStrategy.createNewUserByAlmId(userID));
			}

			if (throwException)
			{
				String localizedMessage = getMessageSource().getMessage(PATH_USER_ID_INVALID_KEY, new String[] { userID },
						"Cannot find user with uid ''{0}''", getI18NService().getCurrentLocale());
				throw new UnknownResourceException(localizedMessage);
			}
			else
			{
				setCurrentUser(userID);
			}
		}
		else if (hasRole(ROLE_TRUSTED_CLIENT, auth) || hasRole(ROLE_CUSTOMERMANAGERGROUP, auth)
				|| hasRole(ROLE_EXTERNAL_CLIENT, auth))
		{
			setCurrentUser(userID);
		}
		else if (hasRole(ROLE_CUSTOMERGROUP, auth))
		{
			if (CURRENT_USER.equals(userID) || userID.equals(auth.getPrincipal()))
			{
				setCurrentUser((String) auth.getPrincipal());
			}
			else
			{
				throw new AccessDeniedException("Access is denied");
			}
		}
		else
		{
			// could not match any authorized role
			throw new AccessDeniedException("Access is denied");
		}

		filterChain.doFilter(request, response);
	}

	/**
	 * Gets auth.
	 *
	 * @return the auth
	 */
	protected Authentication getAuth()
	{
		return SecurityContextHolder.getContext().getAuthentication();
	}

	/**
	 * Gets regexp.
	 *
	 * @return the regexp
	 */
	protected String getRegexp()
	{
		return regexp;
	}

	/**
	 * Sets regexp.
	 *
	 * @param regexp
	 * 		the regexp
	 */
	@Required
	public void setRegexp(final String regexp)
	{
		this.regexp = regexp;
	}

	/**
	 * Gets user service.
	 *
	 * @return the user service
	 */
	protected UserService getUserService()
	{
		return userService;
	}

	/**
	 * Sets user service.
	 *
	 * @param userService
	 * 		the user service
	 */
	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	public I18NService getI18NService()
	{
		return i18NService;
	}

	@Required
	public void setI18NService(final I18NService i18NService)
	{
		this.i18NService = i18NService;
	}

	public MessageSource getMessageSource()
	{
		return messageSource;
	}

	@Required
	public void setMessageSource(final MessageSource messageSource)
	{
		this.messageSource = messageSource;
	}

	public AlmUserAutoCreationStrategy getAlmUserAutoCreationStrategy()
	{
		return almUserAutoCreationStrategy;
	}

	@Required
	public void setAlmUserAutoCreationStrategy(final AlmUserAutoCreationStrategy almUserAutoCreationStrategy)
	{
		this.almUserAutoCreationStrategy = almUserAutoCreationStrategy;
	}

	/**
	 * Has role boolean.
	 *
	 * @param role
	 * 		the role
	 * @param auth
	 * 		the auth
	 * @return the boolean
	 */
	protected boolean hasRole(final String role, final Authentication auth)
	{
		for (final GrantedAuthority ga : auth.getAuthorities())
		{
			if (ga.getAuthority().equals(role))
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * Sets current user.
	 *
	 * @param uid
	 * 		the uid
	 */
	protected void setCurrentUser(final String uid)
	{
		final UserModel userModel = userService.getUserForUID(uid);
		userService.setCurrentUser(userModel);
	}

	/**
	 * Sets current user.
	 *
	 * @param user
	 * 		the user
	 */
	protected void setCurrentUser(final UserModel user)
	{
		userService.setCurrentUser(user);
	}
}
