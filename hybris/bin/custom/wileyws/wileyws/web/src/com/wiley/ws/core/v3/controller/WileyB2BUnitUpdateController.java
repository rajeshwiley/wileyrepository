package com.wiley.ws.core.v3.controller;

import de.hybris.platform.webservicescommons.errors.exceptions.WebserviceValidationException;

import javax.annotation.Nonnull;
import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.Size;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.wiley.ws.core.b2b.unit.dto.B2BUnitSwgDTO;
import com.wiley.ws.core.b2b.unit.facade.WileyB2BUnitUpdateFacade;

import static com.wiley.ws.core.constants.WileyWSConstants.APPLICATION_TYPE_JSON;



@Controller
@RequestMapping("/b2b/accounts")
@Validated
public class WileyB2BUnitUpdateController
{
	@Resource
	private WileyB2BUnitUpdateFacade wileyB2BUnitUpdateFacade;

	@RequestMapping(value = "{sapAccountNumber}", method = RequestMethod.POST,
			produces = { APPLICATION_TYPE_JSON }, consumes = { APPLICATION_TYPE_JSON })
	@Secured("ROLE_TRUSTED_CLIENT")
	public ResponseEntity<Void> updateB2bUnit(@Nonnull @PathVariable @Size(max = 255) final String sapAccountNumber,
			@Valid @RequestBody final B2BUnitSwgDTO b2BUnitSwgDTO, final BindingResult bindingResult)
	{
		
		if (bindingResult.hasErrors())
		{
			throw new WebserviceValidationException(bindingResult);
		}
		wileyB2BUnitUpdateFacade.updateB2BUnit(sapAccountNumber, b2BUnitSwgDTO);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

}
