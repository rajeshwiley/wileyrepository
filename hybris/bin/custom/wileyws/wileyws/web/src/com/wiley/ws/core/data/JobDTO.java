package com.wiley.ws.core.data;

import de.hybris.platform.cronjob.enums.CronJobStatus;

import java.util.Date;

import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JobDTO
{
	private Long id;

	@Size(max = 255)
	private String code;

	private CronJobStatus status;

	private Date created;

	private String href;

	@JsonProperty("id")
	public Long getId()
	{
		return id;
	}

	public void setId(final Long id)
	{
		this.id = id;
	}

	@JsonProperty("code")
	public String getCode()
	{
		return code;
	}

	public void setCode(final String code)
	{
		this.code = code;
	}

	@JsonProperty("status")
	public CronJobStatus getStatus()
	{
		return status;
	}

	public void setStatus(final CronJobStatus status)
	{
		this.status = status;
	}

	@JsonProperty("created")
	public Date getCreated()
	{
		return created;
	}

	public void setCreated(final Date created)
	{
		this.created = created;
	}

	@JsonProperty("href")
	public String getHref()
	{
		return href;
	}

	public void setHref(final String href)
	{
		this.href = href;
	}
}
