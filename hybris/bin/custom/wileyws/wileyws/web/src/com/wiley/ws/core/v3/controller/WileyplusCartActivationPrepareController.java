package com.wiley.ws.core.v3.controller;

import de.hybris.platform.webservicescommons.errors.exceptions.WebserviceValidationException;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.io.IOException;
import java.net.URISyntaxException;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Profile;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.facades.wileyb2c.wileyplus.url.strategy.WileyplusPurchaseOptionsPageBuildURLStrategy;
import com.wiley.facades.wileyplus.cart.activation.dto.CartActivationRequestDto;
import com.wiley.facades.wileyplus.cart.activation.dto.PurchaseOrderPageRedirectResponseDTO;

import static com.wiley.ws.core.constants.WileyWSConstants.APPLICATION_TYPE_JSON;


/**
 * Is used as input point for WileyPlus Edugen app
 * Returns page with incoming json and javaScript to be called on customer's side
 */
@Controller
@Profile(WileyCoreConstants.WILEY_PHASE2A_PROFILE_NAME)
@RequestMapping("/ucart/wileyPlus")
public class WileyplusCartActivationPrepareController implements InitializingBean
{
	private static final String CONFIG_MAX_URL_LENGTH = "wileyplus.maxGeneratedUrlLength";
	@Resource
	private WileyplusPurchaseOptionsPageBuildURLStrategy wileyplusPurchaseOptionsPageURLBuilder;
	@Resource
	private ConfigurationService configurationService;

	private Integer maxUrlLength;

	@RequestMapping(method = RequestMethod.POST, produces = { APPLICATION_TYPE_JSON }, consumes = { APPLICATION_TYPE_JSON })
	@Secured("ROLE_TRUSTED_CLIENT")
	@ResponseBody
	public PurchaseOrderPageRedirectResponseDTO activateCart(
			@Valid @RequestBody final CartActivationRequestDto cartActivationRequestDto, final BindingResult bindingResult)
			throws IOException, URISyntaxException
	{
		if (bindingResult.hasErrors())
		{
			throw new WebserviceValidationException(bindingResult);
		}
		final PurchaseOrderPageRedirectResponseDTO response = new PurchaseOrderPageRedirectResponseDTO();
		String url = wileyplusPurchaseOptionsPageURLBuilder.buildURL(cartActivationRequestDto);
		if (url.length() > maxUrlLength)
		{
			throw new IllegalArgumentException(String.format("Generated url is too long. "
					+ "Max length is '%d' generated length was '%d'", maxUrlLength, url.length()));
		}
		response.setUrl(url);
		return response;
	}

	@Override
	public void afterPropertiesSet() throws Exception
	{
		maxUrlLength = configurationService.getConfiguration().getInt(CONFIG_MAX_URL_LENGTH);
	}
}
