/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package com.wiley.ws.core.exceptions;

import de.hybris.platform.core.model.c2l.CurrencyModel;

import javax.servlet.ServletException;


/**
 * The type Unsupported currency exception.
 *
 * @author krzysztof.kwiatosz
 */
public class UnsupportedCurrencyException extends ServletException
{

	private CurrencyModel currency;

	/**
	 * Instantiates a new Unsupported currency exception.
	 *
	 * @param currencyToSet
	 * 		the currency to set
	 */
	public UnsupportedCurrencyException(final CurrencyModel currencyToSet)
	{
		super("Currency " + currencyToSet + " is not supported by the current base store");
		this.currency = currencyToSet;
	}

	/**
	 * Instantiates a new Unsupported currency exception.
	 *
	 * @param msg
	 * 		the msg
	 */
	public UnsupportedCurrencyException(final String msg)
	{
		super(msg);
	}

	/**
	 * Gets currency.
	 *
	 * @return the currency
	 */
	public CurrencyModel getCurrency()
	{
		return currency;
	}
}
