/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package com.wiley.ws.core.exceptions;



/**
 * The type Unknown resource exception.
 */
public class UnknownResourceException extends RuntimeException
{
	/**
	 * Instantiates a new Unknown resource exception.
	 *
	 * @param msg
	 * 		the msg
	 */
	public UnknownResourceException(final String msg)
	{
		super(msg);
	}

	/**
	 * Instantiates a new Unknown resource exception.
	 *
	 * @param msg
	 * 		the msg
	 * @param cause
	 * 		root cause
	 */
	public UnknownResourceException(final String msg, final Throwable cause) {
		super(msg, cause);
	}
}
