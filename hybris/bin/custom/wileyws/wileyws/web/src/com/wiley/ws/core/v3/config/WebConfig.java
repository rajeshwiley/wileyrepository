/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package com.wiley.ws.core.v3.config;


import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.mvc.annotation.ResponseStatusExceptionResolver;
import org.springframework.web.servlet.mvc.method.annotation.ExceptionHandlerExceptionResolver;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
import org.springframework.web.servlet.mvc.support.DefaultHandlerExceptionResolver;

import com.wiley.ws.core.request.mapping.handler.CommerceHandlerMapping;


/**
 * Spring configuration which replace <mvc:annotation-driven> tag. It allows override default
 * RequestMappingHandlerMapping with our own mapping handler
 */
@Configuration
@ImportResource({ "WEB-INF/config/v3/springmvc-v3-servlet.xml" })
public class WebConfig extends WebMvcConfigurationSupport
{
	@Resource(name = "messageConvertersV3")
	private List<HttpMessageConverter<?>> httpMessageConverters;

	@Resource
	private List<HandlerExceptionResolver> exceptionResolversV3;

	private ApplicationContext applicationContext;

	/**
	 * Request mapping handler mapping request mapping handler mapping.
	 *
	 * @return the request mapping handler mapping
	 */
	@Override
	@Bean
	public RequestMappingHandlerMapping requestMappingHandlerMapping()
	{
		final CommerceHandlerMapping handlerMapping = new CommerceHandlerMapping("v3");
		handlerMapping.setOrder(0);
		handlerMapping.setDetectHandlerMethodsInAncestorContexts(true);
		handlerMapping.setInterceptors(getInterceptors());
		handlerMapping.setContentNegotiationManager(mvcContentNegotiationManager());
		return handlerMapping;
	}

	/**
	 * Configure message converters.
	 *
	 * @param converters
	 * 		the converters
	 */
	@Override
	protected void configureMessageConverters(final List<HttpMessageConverter<?>> converters)
	{
		converters.addAll(httpMessageConverters);
	}

	/**
	 * Configure handler exception resolvers.
	 *
	 * @param exceptionResolvers
	 * 		the exception resolvers
	 */
	@Override
	protected void configureHandlerExceptionResolvers(final List<HandlerExceptionResolver> exceptionResolvers)
	{
		final ExceptionHandlerExceptionResolver exceptionHandlerExceptionResolver = new ExceptionHandlerExceptionResolver();
		exceptionHandlerExceptionResolver.setApplicationContext(applicationContext);
		exceptionHandlerExceptionResolver.setContentNegotiationManager(mvcContentNegotiationManager());
		exceptionHandlerExceptionResolver.setMessageConverters(getMessageConverters());
		exceptionHandlerExceptionResolver.afterPropertiesSet();

		exceptionResolvers.add(exceptionHandlerExceptionResolver);
		exceptionResolvers.addAll(exceptionResolversV3);
		exceptionResolvers.add(new ResponseStatusExceptionResolver());
		exceptionResolvers.add(new DefaultHandlerExceptionResolver());
	}

	/**
	 * Sets application context.
	 *
	 * @param applicationContext
	 * 		the application context
	 * @throws BeansException
	 * 		the beans exception
	 */
	@Override
	public void setApplicationContext(final ApplicationContext applicationContext) throws BeansException
	{
		super.setApplicationContext(applicationContext);
		this.applicationContext = applicationContext;
	}

	/**
	 * Configure content negotiation.
	 *
	 * @param configurer
	 * 		the configurer
	 */
	@Override
	public void configureContentNegotiation(final ContentNegotiationConfigurer configurer)
	{
		configurer.favorPathExtension(false).favorParameter(true);
	}
}
