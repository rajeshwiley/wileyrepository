package com.wiley.ws.core.filter;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.wiley.ws.core.constants.WileyWSConstants;
import com.wiley.ws.core.transaction.service.WileyTransactionService;


/**
 * Filter to set transaction if transactionId parameter is present
 *
 * Author Maksim_Kozich (EPAM)
 */
public class TransactionFilter implements Filter
{

	@Resource
	private WileyTransactionService wileyTransactionService;

	@Override
	public void init(final FilterConfig filterConfig) throws ServletException
	{

	}

	/**
	 * Disable flexible search restrictions for each web service call
	 */
	@Override
	public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain filterChain)
			throws IOException, ServletException
	{
		String transactionId = ((HttpServletRequest) request).getHeader(WileyWSConstants.TRANSACTION_ID_PARAMETER);

		if (StringUtils.isNotBlank(transactionId))
		{
			wileyTransactionService.startUsingTransaction(transactionId);
		}
		filterChain.doFilter(request, response);
	}

	@Override
	public void destroy()
	{
	}
}
