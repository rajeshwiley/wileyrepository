package com.wiley.ws.core.validator;

import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.Objects;

import javax.validation.constraints.NotNull;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.wiley.ws.core.cart.dto.CreateCartRequestWsDTO;
import com.wiley.integrations.order.dto.AddressWsDTO;


/**
 * Validates  payment address country is equal to main country if paymentAddress is provided.
 */
public class PaymentAddressCountryValidator implements Validator
{

	public static final String ERRORS_MESSAGE_CODE = "validation.createCart.paymentAddress.country.invalid";
	public static final String COUNTRY_PATH = "country";

	/**
	 * Supports boolean.
	 *
	 * @param aClass
	 * 		the a class
	 * @return true if param class is assignable from CreateCartRequestWsDTO.class. False otherwise.
	 */
	@Override
	public boolean supports(final Class<?> aClass)
	{
		return CreateCartRequestWsDTO.class.isAssignableFrom(aClass);
	}

	/**
	 * Validates if paymentAddress.country is equal to country field.
	 *
	 * @param target
	 * 		the target object to validate
	 * @param errors
	 * 		the errors to store validation results
	 */
	@Override
	public void validate(@NotNull final Object target, @NotNull final Errors errors)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("target", target);
		ServicesUtil.validateParameterNotNullStandardMessage("errors", errors);
		final CreateCartRequestWsDTO createCartRequestWsDTO = (CreateCartRequestWsDTO) target;

		AddressWsDTO paymentAddressDTO = createCartRequestWsDTO.getPaymentAddress();
		if (paymentAddressDTO != null)
		{
			if (!Objects.equals(createCartRequestWsDTO.getCountry(), paymentAddressDTO.getCountry()))
			{
				errors.rejectValue(COUNTRY_PATH, ERRORS_MESSAGE_CODE);
			}
		}
	}



}
