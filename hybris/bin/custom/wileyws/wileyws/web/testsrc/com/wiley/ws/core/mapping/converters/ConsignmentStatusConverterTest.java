/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.ws.core.mapping.converters;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import static org.junit.Assert.assertEquals;

import org.junit.Test;




/**
 * The type Consignment status converter test.
 */
@UnitTest
public class ConsignmentStatusConverterTest
{
	ConsignmentStatusConverter converter = new ConsignmentStatusConverter();
	String stringStatus = ConsignmentStatus.PICKUP_COMPLETE.toString();
	ConsignmentStatus status = ConsignmentStatus.PICKUP_COMPLETE;

	/**
	 * Test convert from.
	 */
	@Test
	public void testConvertFrom()
	{
		final ConsignmentStatus result = converter.convertFrom(stringStatus, null, null);
		assertEquals(status, result);
	}

	/**
	 * Test convert to.
	 */
	@Test
	public void testConvertTo()
	{
		final String result = converter.convertTo(status, null, null);
		assertEquals(stringStatus, result);
	}
}
