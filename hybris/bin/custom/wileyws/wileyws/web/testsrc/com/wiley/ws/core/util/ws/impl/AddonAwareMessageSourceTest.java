/**
 *
 */
package com.wiley.ws.core.util.ws.impl;

import de.hybris.bootstrap.annotations.UnitTest;

import java.io.File;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.google.common.collect.Lists;

import static java.text.MessageFormat.format;


/**
 * The type Addon aware message source test.
 */
@UnitTest
@RunWith(Parameterized.class)
public class AddonAwareMessageSourceTest
{
	String input;
	String output;

	AddonAwareMessageSource ms;

	//Make expected path platform dependent, not-hardcoded
	private static final String OUTPUT_RESULT_PATH =
			format("{0}WEB-INF{0}messages{0}addons{0}webserviceaddon{0}base", File.separator);

	/**
	 * Instantiates a new Addon aware message source test.
	 *
	 * @param in
	 * 		the in
	 * @param out
	 * 		the out
	 */
	public AddonAwareMessageSourceTest(final String in, final String out)
	{
		input = in;
		output = out;

		ms = new AddonAwareMessageSource();
	}

	/**
	 * Parameters collection.
	 *
	 * @return the collection
	 */
	@Parameters
	public static Collection<String[]> parameters()
	{
		return Lists.newArrayList(new String[] {
				"/Users/Develop/yenv/dev/sources/commercewebservices/wileyws/web/webroot/WEB-INF/messages/addons/webserviceaddon/"
						+ "base.properties",
				OUTPUT_RESULT_PATH }, new String[] {
				"/Users/Develop/yenv/dev/sources/commercewebservices/wileyws/web/webroot/WEB-INF/messages/addons/webserviceaddon/"
						+ "base_en.properties",
				OUTPUT_RESULT_PATH }, new String[] {
				"/Users/Develop/yenv/dev/sources/commercewebservices/wileyws/web/webroot/WEB-INF/messages/addons/webserviceaddon/"
						+ "base_en_US.properties",
				OUTPUT_RESULT_PATH }, new String[] {
				"/Users/Develop/yenv/dev/sources/commercewebservices/wileyws/web/webroot/WEB-INF/messages/addons/webserviceaddon/"
						+ "base_message_en_US.properties",
				OUTPUT_RESULT_PATH }, new String[] {
				"/Users/Develop/yenv/dev/sources/commercewebservices/WEB-INF/messages/addons/wileyws/web/webroot/WEB-INF/"
						+ "messages/addons/webserviceaddon/base.properties",
				OUTPUT_RESULT_PATH });
	}

	/**
	 * Format path test.
	 */
	@Test
	public void formatPathTest()
	{
		final String actual = ms.formatPath(input, "/WEB-INF/messages/addons/");
		Assert.assertEquals(output, actual);
	}


}
