package com.wiley.ws.core.errors.converters;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.jalo.user.Customer;
import de.hybris.platform.webservicescommons.dto.error.ErrorWsDTO;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.jalo.user.WileyUserManager;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileywsDuplicateUidExceptionConverterTest
{
	private static final String TEST_CUSTOMER_ID = "testCustomerId";
	private static final String TEST_TEST_COM = "test@test.com";
	public static final String DUPLICATE_UID_ERROR = "DuplicateUidError";

	@Mock
	private WileyUserManager userManager;
	@Mock
	private Customer customer;
	@InjectMocks
	private WileywsDuplicateUidExceptionConverter wileywsDuplicateUidExceptionConverter =
			new WileywsDuplicateUidExceptionConverter();

	private DuplicateUidException duplicateUidException = new DuplicateUidException(TEST_TEST_COM);

	@Test
	public void successfulSupportTest()
	{
		//given
		//when
		final boolean supports = wileywsDuplicateUidExceptionConverter.supports(DuplicateUidException.class);
		//then
		assertTrue(supports);
	}

	@Test
	public void failedSupportTest()
	{
		//given
		//when
		final boolean supports = wileywsDuplicateUidExceptionConverter.supports(Exception.class);
		//then
		assertFalse(supports);
	}

	@Test
	public void populateTest()
	{
		//given
		when(userManager.getCustomerByLogin(anyString())).thenReturn(customer);
		when(customer.getCustomerID()).thenReturn(TEST_CUSTOMER_ID);
		List<ErrorWsDTO> webserviceErrorList = new ArrayList<>();
		//when
		wileywsDuplicateUidExceptionConverter.populate(duplicateUidException, webserviceErrorList);

		//then
		assertFalse(webserviceErrorList.isEmpty());
		assertEquals(webserviceErrorList.get(0).getMessage(), TEST_TEST_COM);
		assertEquals(webserviceErrorList.get(0).getType(), DUPLICATE_UID_ERROR);
	}
}
