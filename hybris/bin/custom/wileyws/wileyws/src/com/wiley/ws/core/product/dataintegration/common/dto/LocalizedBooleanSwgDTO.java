package com.wiley.ws.core.product.dataintegration.common.dto;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.validator.constraints.NotBlank;

import com.wiley.core.validation.constraints.ValidLocale;

/**
 * Localized version of boolean.
 */
public class LocalizedBooleanSwgDTO
{
	@NotBlank
	@ValidLocale
	private String locale = null;
	@NotNull
	private Boolean value;

	public String getLocale()
	{
		return locale;
	}

	public void setLocale(final String locale)
	{
		this.locale = locale;
	}

	public Boolean getValue()
	{
		return value;
	}

	public void setValue(final Boolean value)
	{
		this.value = value;
	}

	@Override
	public String toString()
	{
		return new ToStringBuilder(this)
				.append("locale", locale)
				.append("value", value)
				.toString();
	}
}

