package com.wiley.ws.core.product.crud.facade;

import com.wiley.ws.core.product.crud.dto.WileyProductWsDto;
import com.wiley.ws.core.product.crud.dto.WileyVariantProductWsDto;


public interface WileyProductCrudFacade
{
	/**
	 * Creates product
	 *
	 * @param productDto
	 * 		- product dto
	 * @return created product code
	 */
	String createProduct(WileyProductWsDto productDto);

	/**
	 * Updates product and returns result of creation
	 *
	 * @param productId
	 * 		- base product id
	 * @param productDto
	 * 		- product dto
	 */
	void updateProduct(String productId, WileyProductWsDto productDto);

	/**
	 * Create variant product {@link com.wiley.core.model.WileyPurchaseOptionProductModel}
	 *  @param productId
	 * 		- base product Id
	 * @param variantProductDto
	 * @return created variant product code
	 */
	String createVariantProduct(String productId, WileyVariantProductWsDto variantProductDto);

	/**
	 * Update variant product {@link com.wiley.core.model.WileyPurchaseOptionProductModel}
	 *
	 * @param productId
	 * 		- base product id
	 * @param variantId
	 * 		- variant product id
	 * @param variantProductDto
	 * 		- variant product dto
	 */
	void updateVariantProduct(String productId, String variantId, WileyVariantProductWsDto variantProductDto);
}
