package com.wiley.ws.core.customers.impl;

import de.hybris.platform.commercefacades.customer.impl.DefaultCustomerFacade;
import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.Assert;

import com.wiley.core.servicelayer.i18n.WileycomI18NService;
import com.wiley.ws.core.customers.WileyWsCustomerFacade;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


public class WileyWsCustomerFacadeImpl extends DefaultCustomerFacade implements WileyWsCustomerFacade
{
	@Resource
	private WileycomI18NService wileycomI18NService;

	@Resource
	private UserService userService;

	@Override
	public void register(final RegisterData registerData) throws DuplicateUidException
	{
		validateParameterNotNullStandardMessage("registerData", registerData);
		Assert.hasText(registerData.getLogin(), "The field [Login] cannot be empty");
		Assert.hasText(registerData.getPassword(), "The field [Password] cannot be empty");

		final CustomerModel newCustomer = getModelService().create(CustomerModel.class);

		if (StringUtils.isNotBlank(registerData.getFirstName()) && StringUtils.isNotBlank(registerData.getLastName()))
		{
			newCustomer.setFirstName(registerData.getFirstName());
			newCustomer.setLastName(registerData.getLastName());
			newCustomer.setName(getCustomerNameStrategy().getName(registerData.getFirstName(), registerData.getLastName()));
		}

		if (StringUtils.isNotBlank(registerData.getTitleCode()))
		{
			final TitleModel title = getUserService().getTitleForCode(registerData.getTitleCode());
			newCustomer.setTitle(title);
		}

		setUidForRegister(registerData, newCustomer);

		newCustomer.setSessionLanguage(getCommonI18NService().getCurrentLanguage());
		newCustomer.setSessionCurrency(getCommonI18NService().getCurrentCurrency());
		Optional<CountryModel> currentCountry = wileycomI18NService.getCurrentCountry();
		if (currentCountry.isPresent())
		{
			newCustomer.setSessionCountry(currentCountry.get());
		}

		newCustomer.setExternalApplicationId(
				StringUtils.isEmpty(registerData.getExternalApplicationId()) ? null : registerData.getExternalApplicationId());
		newCustomer.setExternalCustomerId(
				StringUtils.isEmpty(registerData.getExternalCustomerId()) ? null : registerData.getExternalCustomerId());

		getCustomerAccountService().register(newCustomer, registerData.getPassword());
	}

	@Override
	public String getCustomerId(final String customerUid)
	{
		Assert.hasText(customerUid, "The [customerUid] cannot be empty");

		final CustomerModel customerModel = (CustomerModel) userService.getUserForUID(customerUid);

		return customerModel.getCustomerID();
	}
}
