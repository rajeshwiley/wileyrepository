package com.wiley.ws.core.wileysubscription.dto;

import java.util.Date;
import java.util.Objects;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import static com.wiley.core.constants.WileyCoreConstants.WILEY_EBP_DATE_TIME_FORMAT_NAME;


/**
 * The payment transaction information for a subscription.
 */
public class PaymentTransactionWsDTO
{
	@NotNull
	private Double amount;

	@NotNull
	private String currency;

	@NotNull
	private Date date;

	@NotNull
	@Pattern(regexp = "CAPTURE|REFUND", flags = Pattern.Flag.CASE_INSENSITIVE,
			message = "{validation.default.Pattern}")
	private String paymentStatus;

	@NotNull
	@Pattern(regexp = "CARD|INVOICE|PAYPAL", flags = Pattern.Flag.CASE_INSENSITIVE,
			message = "{validation.default.Pattern}")
	private String paymentType;

	private String paymentToken;

	public PaymentTransactionWsDTO amount(final Double amount)
	{
		this.amount = amount;
		return this;
	}

	public Double getAmount()
	{
		return amount;
	}

	public void setAmount(final Double amount)
	{
		this.amount = amount;
	}

	public PaymentTransactionWsDTO currency(final String currency)
	{
		this.currency = currency;
		return this;
	}

	public String getCurrency()
	{
		return currency;
	}

	public void setCurrency(final String currency)
	{
		this.currency = currency;
	}

	public PaymentTransactionWsDTO date(final Date date)
	{
		this.date = date;
		return this;
	}

	@JsonProperty("date")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = WILEY_EBP_DATE_TIME_FORMAT_NAME)
	public Date getDate()
	{
		return date;
	}

	public void setDate(final Date date)
	{
		this.date = date;
	}

	public PaymentTransactionWsDTO paymentStatus(final String paymentStatus)
	{
		this.paymentStatus = paymentStatus;
		return this;
	}

	public String getPaymentStatus()
	{
		return paymentStatus;
	}

	public void setPaymentStatus(final String paymentStatus)
	{
		this.paymentStatus = paymentStatus;
	}

	public PaymentTransactionWsDTO paymentType(final String paymentType)
	{
		this.paymentType = paymentType;
		return this;
	}

	public String getPaymentType()
	{
		return paymentType;
	}

	public void setPaymentType(final String paymentType)
	{
		this.paymentType = paymentType;
	}

	public PaymentTransactionWsDTO paymentToken(final String paymentToken)
	{
		this.paymentToken = paymentToken;
		return this;
	}

	public String getPaymentToken()
	{
		return paymentToken;
	}

	public void setPaymentToken(final String paymentToken)
	{
		this.paymentToken = paymentToken;
	}


	@Override
	public boolean equals(final java.lang.Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		PaymentTransactionWsDTO paymentTransaction = (PaymentTransactionWsDTO) o;
		return Objects.equals(this.amount, paymentTransaction.amount)
				&& Objects.equals(this.currency, paymentTransaction.currency)
				&& Objects.equals(this.date, paymentTransaction.date)
				&& Objects.equals(this.paymentStatus, paymentTransaction.paymentStatus)
				&& Objects.equals(this.paymentType, paymentTransaction.paymentType)
				&& Objects.equals(this.paymentToken, paymentTransaction.paymentToken);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(amount, currency, date, paymentStatus, paymentType, paymentToken);
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("class PaymentTransaction {\n");

		sb.append("    amount: ").append(toIndentedString(amount)).append("\n");
		sb.append("    currency: ").append(toIndentedString(currency)).append("\n");
		sb.append("    date: ").append(toIndentedString(date)).append("\n");
		sb.append("    paymentStatus: ").append(toIndentedString(paymentStatus)).append("\n");
		sb.append("    paymentType: ").append(toIndentedString(paymentType)).append("\n");
		sb.append("    paymentToken: ").append(toIndentedString(paymentToken)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(final  java.lang.Object o)
	{
		if (o == null)
		{
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
