package com.wiley.ws.core.wileysubscription.populator.impl;

import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.apache.commons.lang3.StringUtils;

import com.paypal.hybris.constants.PaypalConstants;
import com.paypal.hybris.enums.PaymentActionType;
import com.paypal.hybris.model.PaypalPaymentInfoModel;
import com.wiley.ws.core.wileysubscription.dto.PaymentInfoUpdateRequestWsDTO;

import static com.wiley.facades.populators.PopulateUtils.populateFieldIfNotNull;


/**
 * Implementation for {@link PaypalPaymentInfoModel}.
 */
public class PayPalPaymentInfoCreatingStrategy extends AbstractPaymentInfoCreatingStrategy<PaypalPaymentInfoModel>
{

	@Override
	public Class<PaypalPaymentInfoModel> getPaymentInfoClass()
	{
		return PaypalPaymentInfoModel.class;
	}

	@Override
	public void populate(final PaymentInfoUpdateRequestWsDTO paymentInfoUpdateRequestWsDTO,
			final PaypalPaymentInfoModel paypalPaymentInfoModel) throws ConversionException
	{
		// populate required fields
		if (StringUtils.isEmpty(paypalPaymentInfoModel.getCode()))
		{
			paypalPaymentInfoModel.setCode(PaypalConstants.PAYPAL_PAYMENT_INFO_CODE);
		}
		if (paypalPaymentInfoModel.getPaymentAction() == null)
		{
			paypalPaymentInfoModel.setPaymentAction(PaymentActionType.SALE);
		}

		// populate optional fields
		populateFieldIfNotNull(this::populateBillingAddress, paypalPaymentInfoModel,
				paymentInfoUpdateRequestWsDTO.getPaymentAddress());
	}
}
