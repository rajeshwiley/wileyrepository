package com.wiley.ws.core.paragraph.impl;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;
import de.hybris.platform.cms2.model.contents.components.CMSParagraphComponentModel;
import de.hybris.platform.cms2.servicelayer.daos.CMSComponentDao;
import de.hybris.platform.cms2.services.ContentCatalogService;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.jalo.c2l.LocalizableItem;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.AbstractItemModel;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.ws.core.paragraph.WileyWsParagraphFacade;


public class WileyWsParagraphFacadeImpl implements WileyWsParagraphFacade
{
	@Autowired
	private CMSComponentDao cmsComponentDao;

	@Autowired
	private CatalogVersionService catalogVersionService;

	@Autowired
	private CommonI18NService commonI18NService;

	@Autowired
	private CommerceCommonI18NService commerceCommonI18NService;

	@Autowired
	private SessionService sessionService;

	@Autowired
	private ContentCatalogService contentCatalogService;
	@Override
	public List<AbstractCMSComponentModel> getAbstractCMSComponents(final String uid)
	{
		List<CatalogVersionModel> allSessionContentCatalogs = new ArrayList<CatalogVersionModel>();
		for (CatalogVersionModel catalogVersion: catalogVersionService.getSessionCatalogVersions())
		{
			if (contentCatalogService.isContentCatalog(catalogVersion))
			{
				allSessionContentCatalogs.add(catalogVersion);
			}
		}
		final List<AbstractCMSComponentModel> cmsComponents = cmsComponentDao.findCMSComponentsByIdAndCatalogVersions(
				uid, allSessionContentCatalogs);
		return cmsComponents;
	}

	@Override
	public String getParagraphContent(final CMSParagraphComponentModel paragraphComponent)
	{
		final LanguageModel currentLanguage = commerceCommonI18NService.getCurrentLanguage();
		final Locale locale = commonI18NService.getLocaleForLanguage(currentLanguage);
		return paragraphComponent.getContent(locale);
	}

	@Override
	public void setFallbackLanguage(final Boolean enabled)
	{
		if (sessionService != null)
		{
			sessionService.setAttribute(LocalizableItem.LANGUAGE_FALLBACK_ENABLED, enabled);
			sessionService.setAttribute(AbstractItemModel.LANGUAGE_FALLBACK_ENABLED_SERVICE_LAYER, enabled);
		}
	}
}
