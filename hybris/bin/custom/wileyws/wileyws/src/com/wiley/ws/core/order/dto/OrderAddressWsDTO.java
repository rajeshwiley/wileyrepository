package com.wiley.ws.core.order.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wiley.ws.core.address.dto.AddressWsDTO;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.validation.constraints.NotNull;


/**
 * An address
 *
 * Author Herman_Chukhrai (EPAM)
 */
public class OrderAddressWsDTO extends AddressWsDTO
{
	/**
	 * Unique address identifier.
	 */
	@NotNull
	private String addressId;


	@JsonProperty("addressId")
	public String getAddressId()
	{
		return addressId;
	}

	public void setAddressId(final String addressId)
	{
		this.addressId = addressId;
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}

		if (o == null || getClass() != o.getClass())
		{
			return false;
		}

		final OrderAddressWsDTO that = (OrderAddressWsDTO) o;

		return new EqualsBuilder()
				.append(addressId, that.addressId)
				.isEquals();
	}

	@Override
	public int hashCode()
	{
		return new HashCodeBuilder(17, 37)
				.append(addressId)
				.toHashCode();
	}

	@Override
	public String toString()
	{
		return new ToStringBuilder(this)
				.appendSuper(super.toString())
				.append("addressId", addressId)
				.toString();
	}
}
