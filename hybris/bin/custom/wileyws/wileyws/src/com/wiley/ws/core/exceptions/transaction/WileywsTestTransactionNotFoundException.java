package com.wiley.ws.core.exceptions.transaction;

/**
 * Created by Maksim_Kozich on 11/09/2016.
 */
public class WileywsTestTransactionNotFoundException extends RuntimeException
{
	public WileywsTestTransactionNotFoundException()
	{
		super();
	}

	public WileywsTestTransactionNotFoundException(final String message)
	{
		super(message);
	}

	public WileywsTestTransactionNotFoundException(final String message, final Throwable cause)
	{
		super(message, cause);
	}

	public WileywsTestTransactionNotFoundException(final Throwable cause)
	{
		super(cause);
	}

	public WileywsTestTransactionNotFoundException(final String message, final Throwable cause,
			final boolean enableSuppression, final boolean writableStackTrace)
	{
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
