package com.wiley.ws.core.common.dto;

import java.util.Objects;

import javax.validation.constraints.Pattern;


/**
 * Standard Java locale code. See https://docs.oracle.com/javase/8/docs/api/java/util/Locale.html for details. The method
 * {@link org.apache.commons.lang.LocaleUtils#toLocale(String)} will be used to parse the locale's string representation.
 */
public class LocaleSwgDTO
{
	private String value = null;

	public LocaleSwgDTO(final String value)
	{
		this.value = value;
	}

	@Pattern(regexp = "^[a-z]{2}(_[A-Z]{2}(_.+)?)?$", message = "validation.locale.invalid")
	public String getValue()
	{
		return value;
	}

	public void setValue(final String value)
	{
		this.value = value;
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		LocaleSwgDTO localeSwgDTO = (LocaleSwgDTO) o;
		return Objects.equals(value, localeSwgDTO.value);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(value);
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("class LocaleSwgDTO {\n");
		sb.append("  value: ").append(value).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
