package com.wiley.ws.core.order;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;

import com.wiley.ws.core.order.exception.WileycomWsOrderInvalidShippedQuantiytException;


/**
 * Created by Georgii_Gavrysh on 8/11/2016.
 */
public class ShippedQuantityExcessValidator
{
	public void validate(final OrderModel orderModel)
	{
		for (final AbstractOrderEntryModel abstractOrderEntryModel : orderModel.getEntries())
		{
			final Long entryQuantity = abstractOrderEntryModel.getQuantity();
			final long shippedQuantity = orderModel.getConsignments().stream().flatMap(consignment ->
				 consignment.getConsignmentEntries().stream()).
					filter(consignmentEntry -> consignmentEntry.getOrderEntry().equals(abstractOrderEntryModel)).
					mapToLong(consignmentEntryModel -> consignmentEntryModel.getShippedQuantity()).sum();
			if (entryQuantity < shippedQuantity)
			{
				throw new WileycomWsOrderInvalidShippedQuantiytException(orderModel.getCode(),
						abstractOrderEntryModel.getProduct().getCode(), shippedQuantity,  entryQuantity);
			}
		}
	}
}
