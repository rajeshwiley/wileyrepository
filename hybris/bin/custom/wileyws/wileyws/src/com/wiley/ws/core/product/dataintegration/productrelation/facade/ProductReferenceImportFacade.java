package com.wiley.ws.core.product.dataintegration.productrelation.facade;

import java.util.Collection;

import com.wiley.ws.core.product.dataintegration.productrelation.dto.ProductRelationSwgDTO;


public interface ProductReferenceImportFacade
{
	void createProductReference(Collection<ProductRelationSwgDTO> dtos, String productId, String variantId);
}
