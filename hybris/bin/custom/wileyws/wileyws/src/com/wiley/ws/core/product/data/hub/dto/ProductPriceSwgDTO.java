package com.wiley.ws.core.product.data.hub.dto;

import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.codehaus.jackson.annotate.JsonUnwrapped;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wiley.ws.core.common.dto.CountrySwgDTO;
import com.wiley.ws.core.common.dto.CurrencySwgDTO;
import com.wiley.core.validation.constraints.DoubleNonnegative;


/**
 * Price value for a product. The country field defines a country where the price is applicable.
 * If the country is not present, the price will be considered as applicable for any country.
 **/
public class ProductPriceSwgDTO
{

	private Double price = null;
	private CurrencySwgDTO currency = null;
	private CountrySwgDTO country = null;
	private Long minQuantity = null;
	private String subscriptionTerm = null;

	/**
	 * Value of the price.
	 **/
	@NotNull
	@DoubleNonnegative
	@JsonProperty("price")
	public Double getPrice()
	{
		return price;
	}

	public void setPrice(final Double price)
	{
		this.price = price;
	}

	/**
	 **/
	@Valid
	@NotNull
	@JsonProperty("currency")
	public CurrencySwgDTO getCurrency()
	{
		return currency;
	}

	public void setCurrency(final CurrencySwgDTO currency)
	{
		this.currency = currency;
	}

	/**
	 **/
	@Valid
	@JsonUnwrapped
	@JsonProperty("country")
	public CountrySwgDTO getCountry()
	{
		return country;
	}

	public void setCountry(final CountrySwgDTO country)
	{
		this.country = country;
	}

	/**
	 * Scale price definition. A customer needs to order at least this number of units for this price to be effective.
	 **/
	@JsonProperty("minQuantity")
	public Long getMinQuantity()
	{
		return minQuantity;
	}

	public void setMinQuantity(final Long minQuantity)
	{
		this.minQuantity = minQuantity;
	}

	/**
	 * Only for subscription products.
	 * The code of the subscription term for the subscription product for which the price is actual.
	 * Each subscription term defines the billing plan and must be preconfigured on the Hybris side.
	 **/
	@Size(max = 255)
	@JsonProperty("subscriptionTerm")
	public String getSubscriptionTerm()
	{
		return subscriptionTerm;
	}

	public void setSubscriptionTerm(final String subscriptionTerm)
	{
		this.subscriptionTerm = subscriptionTerm;
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		ProductPriceSwgDTO productPrice = (ProductPriceSwgDTO) o;
		return Objects.equals(price, productPrice.price)
				&& Objects.equals(currency, productPrice.currency)
				&& Objects.equals(country, productPrice.country)
				&& Objects.equals(subscriptionTerm, productPrice.subscriptionTerm)
				&& Objects.equals(minQuantity, productPrice.minQuantity);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(price, currency, country, minQuantity, subscriptionTerm);
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("class ProductPriceSwgDTO {\n");
		sb.append("  price: ").append(price).append("\n");
		sb.append("  currency: ").append(currency).append("\n");
		sb.append("  country: ").append(country).append("\n");
		sb.append("  minQuantity: ").append(minQuantity).append("\n");
		sb.append("  subscriptionTerm: ").append(subscriptionTerm).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
