package com.wiley.ws.core.product.crud.converter.populator.baseproduct;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.model.AuthorInfoModel;
import com.wiley.core.model.WileyProductModel;
import com.wiley.ws.core.product.crud.dto.AuthorInfoWsDto;
import com.wiley.ws.core.product.crud.dto.WileyProductWsDto;
import com.wiley.ws.core.product.dataintegration.common.util.LocalizedPopulatorUtil;


public class AuthorInfoPopulator implements Populator<WileyProductWsDto, WileyProductModel>
{

	private final ModelService modelService;
	private final LocalizedPopulatorUtil localizedPopulatorUtil;

	@Autowired
	public AuthorInfoPopulator(final ModelService modelService, final LocalizedPopulatorUtil localizedPopulatorUtil)
	{
		this.modelService = modelService;
		this.localizedPopulatorUtil = localizedPopulatorUtil;
	}

	@Override
	public void populate(final WileyProductWsDto wileyProductWsDto, final WileyProductModel wileyProductModel)
			throws ConversionException
	{
		if (CollectionUtils.isNotEmpty(wileyProductWsDto.getAuthorInfos()))
		{
			wileyProductModel.setAuthorInfos(
					wileyProductWsDto.getAuthorInfos()
							.stream()
							.map(this::toAuthorInfoModel)
							.collect(Collectors.toList())
			);
		}
	}

	private AuthorInfoModel toAuthorInfoModel(final AuthorInfoWsDto authorInfoWsDto)
	{
		final AuthorInfoModel authorInfo = new AuthorInfoModel();
		modelService.attach(authorInfo);
		localizedPopulatorUtil.populateStrings(authorInfoWsDto.getName(), authorInfo::setName);
		localizedPopulatorUtil.populateStrings(authorInfoWsDto.getRole(), authorInfo::setRole);

		return authorInfo;
	}
}
