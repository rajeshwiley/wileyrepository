package com.wiley.ws.core.product.data.hub.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.ws.core.product.data.hub.converter.MediaSwgDTOPDHConverter;
import com.wiley.ws.core.product.data.hub.dto.ProductSwgDTO;


/**
 * Populate {@link ProductSwgDTO#getPicture()} to {@link ProductModel#setPicture(MediaModel)}
 */
public class MediaPDHPopulator implements Populator<ProductSwgDTO, ProductModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(MediaPDHPopulator.class);

	@Autowired
	private ModelService modelService;

	@Autowired
	private KeyGenerator pdhMediaCodeGenerator;

	@Autowired
	private MediaSwgDTOPDHConverter mediaSwgDTOPDHConverter;

	@Override
	public void populate(final ProductSwgDTO product, final ProductModel productModel) throws ConversionException
	{
		if (product.getPicture() != null)
		{

			if (productModel.getPicture() != null)
			{
				LOG.debug("Removing old product picture [{}]", productModel.getPicture());
				modelService.remove(productModel.getPicture());
			}

			final String mediaCode = String.valueOf(pdhMediaCodeGenerator.generate());
			final MediaModel mediaModel = mediaSwgDTOPDHConverter.convertToMediaModel(product.getPicture(), mediaCode,
					productModel.getCatalogVersion());
			productModel.setPicture(mediaModel);
		}
		else
		{
			LOG.debug("Skip populating of product picture due to null");
		}
	}
}
