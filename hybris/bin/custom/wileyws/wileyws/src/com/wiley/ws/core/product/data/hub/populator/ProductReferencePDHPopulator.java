package com.wiley.ws.core.product.data.hub.populator;

import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.model.WileyProductModel;
import com.wiley.ws.core.product.data.hub.converter.ProductReferenceSwgDTOPDHConverter;
import com.wiley.ws.core.product.data.hub.dto.ProductSwgDTO;


/**
 * Populate {@link ProductSwgDTO#getProductReferences()} to {@link ProductModel#setProductReferences(Collection)}
 */
public class ProductReferencePDHPopulator implements Populator<ProductSwgDTO, WileyProductModel>
{

	private static final Logger LOG = LoggerFactory.getLogger(ProductReferencePDHPopulator.class);

	@Autowired
	private CommonI18NService commonI18NService;

	@Autowired
	private ModelService modelService;

	@Autowired
	private ProductReferenceSwgDTOPDHConverter productReferenceSwgDTOPDHConverter;


	@Override
	public void populate(final ProductSwgDTO product, final WileyProductModel productModel) throws ConversionException
	{
		if (product.getProductReferences() != null)
		{
			if (CollectionUtils.isNotEmpty(productModel.getProductReferences()))
			{
				LOG.debug("Removing old product references [{}]", productModel.getProductReferences());
				modelService.removeAll(productModel.getProductReferences());
			}

			List<ProductReferenceModel> productReferences = product.getProductReferences().stream()
					.map(reference -> productReferenceSwgDTOPDHConverter.convertToProductRefereceModel(reference, productModel))
					.collect(Collectors.toList());

			productModel.setProductReferences(productReferences);
		}
		else
		{
			LOG.debug("Skip populating of product references due to null");
		}
	}



}
