package com.wiley.ws.core.exceptions.http;

/**
 * Created by Mikhail_Asadchy on 10/13/2016.
 */
public class WileywsBadRequestException extends RuntimeException
{
	public WileywsBadRequestException()
	{
	}

	public WileywsBadRequestException(final String message)
	{
		super(message);
	}

	public WileywsBadRequestException(final String message, final Throwable cause)
	{
		super(message, cause);
	}

	public WileywsBadRequestException(final Throwable cause)
	{
		super(cause);
	}

	public WileywsBadRequestException(final String message, final Throwable cause,
			final boolean enableSuppression, final boolean writableStackTrace)
	{
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
