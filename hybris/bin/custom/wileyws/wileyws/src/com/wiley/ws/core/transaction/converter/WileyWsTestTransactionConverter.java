package com.wiley.ws.core.transaction.converter;


import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import com.wiley.ws.core.product.data.hub.dto.ProductSwgDTO;
import com.wiley.ws.core.transaction.WileyWsTestTransaction;
import com.wiley.ws.core.transaction.dto.WileyWsTestTransactionDTO;


/**
 * Basic properties {@link ProductSwgDTO} to {@link ProductModel}
 */
public class WileyWsTestTransactionConverter implements Converter<WileyWsTestTransaction, WileyWsTestTransactionDTO>
{

	@Override
	public WileyWsTestTransactionDTO convert(final WileyWsTestTransaction wileyWsTestTransaction) throws ConversionException
	{
		return convert(wileyWsTestTransaction, new WileyWsTestTransactionDTO());
	}

	@Override
	public WileyWsTestTransactionDTO convert(final WileyWsTestTransaction wileyWsTestTransaction,
			final WileyWsTestTransactionDTO transactionWsDTO)
			throws ConversionException
	{
		transactionWsDTO.setTransactionId(String.valueOf(wileyWsTestTransaction.getObjectID()));
		return transactionWsDTO;
	}
}
