package com.wiley.ws.core.b2b.unit.exception;

public class B2BUnitUpdateBadRequestException extends RuntimeException
{
	public B2BUnitUpdateBadRequestException(final String message)
	{
		super(message);
	}

	public B2BUnitUpdateBadRequestException(final String message, final Throwable cause)
	{
		super(message, cause);
	}
}
