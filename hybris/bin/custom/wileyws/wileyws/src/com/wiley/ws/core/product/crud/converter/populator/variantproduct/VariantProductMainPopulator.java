package com.wiley.ws.core.product.crud.converter.populator.variantproduct;

import com.wiley.core.enums.ProductEditionFormat;
import com.wiley.core.enums.WileyAssortment;
import com.wiley.core.model.WileyPurchaseOptionProductModel;
import com.wiley.ws.core.product.crud.dto.WileyVariantProductWsDto;
import com.wiley.ws.core.product.dataintegration.common.util.LocalizedPopulatorUtil;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


public class VariantProductMainPopulator implements Populator<WileyVariantProductWsDto, WileyPurchaseOptionProductModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(VariantProductMainPopulator.class);

	private final LocalizedPopulatorUtil localizedPopulatorUtil;

	@Autowired
	public VariantProductMainPopulator(final LocalizedPopulatorUtil localizedPopulatorUtil)
	{
		this.localizedPopulatorUtil = localizedPopulatorUtil;
	}

	@Override
	public void populate(final WileyVariantProductWsDto variantProductWsDto,
			final WileyPurchaseOptionProductModel variantProductModel) throws ConversionException
	{
		validateParameterNotNull(variantProductWsDto, "VariantProductDto can't be null");
		validateParameterNotNull(variantProductModel, "VariantProductModel can't be null");

		variantProductModel.setCode(variantProductWsDto.getId());
		variantProductModel.setIsbn(variantProductWsDto.getIsbn10());
		variantProductModel.setEditionFormat(getEditionFormatFromDto(variantProductWsDto));
		variantProductModel.setCustom(variantProductWsDto.getCustom());

		variantProductModel.setAssortments(Collections.singletonList(WileyAssortment.B2CASSORTMENT));

		localizedPopulatorUtil.populateBooleans(variantProductWsDto.getPrintOnDemand(), variantProductModel::setPrintOnDemand);
	}

	private ProductEditionFormat getEditionFormatFromDto(final WileyVariantProductWsDto variantProductWsDto)
	{
		final String editionFormat = variantProductWsDto.getEditionFormat();
		if (editionFormat != null)
		{
			try
			{
				return ProductEditionFormat.valueOf(editionFormat);
			}
			catch (IllegalArgumentException e)
			{
				LOG.warn("EditionFormat value [{}] for product code [{}] isn't correct", editionFormat,
						variantProductWsDto.getId());
			}
		}
		return null;
	}

}
