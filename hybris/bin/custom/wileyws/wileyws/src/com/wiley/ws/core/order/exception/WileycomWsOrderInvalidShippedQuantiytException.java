package com.wiley.ws.core.order.exception;

/**
 * Created by Georgii_Gavrysh on 8/12/2016.
 */
public class WileycomWsOrderInvalidShippedQuantiytException extends WileywsValidationException
{
	private static final String ERROR_MESSAGE_FORMAT = "Shipped quantity [%d] is greater"
			+ " than order product quantity in order [%d]."
			+ " Order code: [%s], product code: [%s].";
	private String orderCode;
	private String productCode;
	private long shippedQuantity;
	private long orderProductQuantity;

	public WileycomWsOrderInvalidShippedQuantiytException(final String orderCode, final String productCode,
			final long shippedQuantity, final long productQuantity)
	{
		super();
		this.orderCode = orderCode;
		this.productCode = productCode;
		this.shippedQuantity = shippedQuantity;
		this.orderProductQuantity = productQuantity;
		super.setMessage(String.format(ERROR_MESSAGE_FORMAT, shippedQuantity, orderProductQuantity, orderCode, productCode));
	}

	public String getOrderCode()
	{
		return orderCode;
	}

	public String getProductCode()
	{
		return productCode;
	}

	public long getShippedQuantity()
	{
		return shippedQuantity;
	}

	public long getOrderProductQuantity()
	{
		return orderProductQuantity;
	}

	public void setOrderCode(final String orderCode)
	{
		this.orderCode = orderCode;
	}

	public void setProductCode(final String productCode)
	{
		this.productCode = productCode;
	}

	public void setShippedQuantity(final long shippedQuantity)
	{
		this.shippedQuantity = shippedQuantity;
	}

	public void setOrderProductQuantity(final long orderProductQuantity)
	{
		this.orderProductQuantity = orderProductQuantity;
	}

}
