package com.wiley.ws.core.transaction.service;

import javax.validation.constraints.NotNull;

import com.wiley.ws.core.transaction.WileyWsTestTransaction;


/**
 * Created by Maksim_Kozich on 22.09.2016.
 */
public interface WileyTransactionService
{
	WileyWsTestTransaction getTransaction(@NotNull String transactionId);
	WileyWsTestTransaction startUsingTransaction(@NotNull String transactionId);
	WileyWsTestTransaction beginNewTransaction();
	WileyWsTestTransaction rollbackTransaction(@NotNull String transactionId);
	WileyWsTestTransaction commitTransaction(@NotNull WileyWsTestTransaction transaction);
	void cleanUpCache();
}
