package com.wiley.ws.core.abstractorderentry.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Optional;

import javax.annotation.Resource;

import com.wiley.core.abstractorderentry.service.WileyAbstractOrderEntryService;
import com.wiley.integrations.order.dto.OrderEntryWsDTO;
import com.wiley.ws.core.abstractorderentry.WileyWsAbstractOrderEntryFacade;


public class WileyWsAbstractOrderEntryFacadeImpl implements WileyWsAbstractOrderEntryFacade
{
	@Resource
	private WileyAbstractOrderEntryService wileyAbstractOrderEntryService;
	@Resource
	private Converter<AbstractOrderEntryModel, OrderEntryWsDTO> wileyOrderEntryWsConverter;

	@Override
	public Optional<OrderEntryWsDTO> getEntryForBusinessKey(final String businessKey)
	{
		final Optional<AbstractOrderEntryModel> entryForBusinessKey =
				wileyAbstractOrderEntryService.getAbstractOrderEntryForBusinessKey(businessKey);

		if (entryForBusinessKey.isPresent())
		{
			return Optional.ofNullable(wileyOrderEntryWsConverter.convert(entryForBusinessKey.get()));
		}

		return Optional.empty();
	}

	@Override
	public OrderEntryWsDTO getEntryDetails(final String baseSiteId, final String entryId)
	{
		return wileyOrderEntryWsConverter.convert(
				wileyAbstractOrderEntryService.getAbstractOrderEntryForGuid(baseSiteId, entryId));
	}
}
