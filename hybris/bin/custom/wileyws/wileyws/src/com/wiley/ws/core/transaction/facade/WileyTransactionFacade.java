package com.wiley.ws.core.transaction.facade;

import javax.validation.constraints.NotNull;

import com.wiley.ws.core.transaction.dto.WileyWsTestTransactionDTO;


/**
 * Created by Maksim_Kozich on 22.09.2016.
 */
public interface WileyTransactionFacade
{
	WileyWsTestTransactionDTO beginNewTransaction();
	WileyWsTestTransactionDTO rollbackTransaction(@NotNull String transactionId);
}
