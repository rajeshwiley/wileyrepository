package com.wiley.ws.core.product.dataintegration.common.util;

import java.util.List;
import java.util.Locale;
import java.util.function.BiConsumer;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.wiley.ws.core.product.dataintegration.common.dto.LocalizedBooleanSwgDTO;
import com.wiley.ws.core.product.dataintegration.common.dto.LocalizedLongStringSwgDTO;
import com.wiley.ws.core.product.dataintegration.common.dto.LocalizedStringSwgDTO;


/**
 * Util that helps in localized DTO population
 */
public interface LocalizedPopulatorUtil
{
	/**
	 * Populates list of {@link LocalizedStringSwgDTO} into model setter consumer
	 */
	void populateStrings(@Nullable List<LocalizedStringSwgDTO> localizedStrings,
			@Nonnull BiConsumer<String, Locale> setterConsumer);

	/**
	 * Populates list of {@link LocalizedLongStringSwgDTO} into model setter consumer
	 */
	void populateLongStrings(@Nullable List<LocalizedLongStringSwgDTO> localizedStrings,
			@Nonnull BiConsumer<String, Locale> setterConsumer);

	/**
	 * Populates list of {@link LocalizedBooleanSwgDTO} into model setter consumer
	 */
	void populateBooleans(@Nullable List<LocalizedBooleanSwgDTO> localizedBooleans,
			@Nonnull BiConsumer<Boolean, Locale> setterConsumer);

}
