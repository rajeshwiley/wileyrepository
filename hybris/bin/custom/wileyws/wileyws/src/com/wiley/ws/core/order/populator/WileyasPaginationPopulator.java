package com.wiley.ws.core.order.populator;

import com.wiley.ws.core.order.dto.PaginationWsDTO;
import de.hybris.platform.commerceservices.search.pagedata.PaginationData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import static org.springframework.util.Assert.notNull;

public class WileyasPaginationPopulator implements Populator<PaginationData, PaginationWsDTO>
{

    @Override
    public void populate(final PaginationData source, final PaginationWsDTO target) throws ConversionException
    {
        notNull(source, "Source object should not be NULL");
        notNull(target, "Target object should not be NULL");

        target.setTotalResults((int) source.getTotalNumberOfResults());
        target.setTotalPages((int) source.getNumberOfPages());
        target.setSort(source.getSort());
        target.setPageSize(source.getPageSize());
        target.setCurrentPage(source.getCurrentPage());
    }
}
