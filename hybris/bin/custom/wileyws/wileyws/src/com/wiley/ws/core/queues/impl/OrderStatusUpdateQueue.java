/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package com.wiley.ws.core.queues.impl;

import com.wiley.ws.core.queues.data.OrderStatusUpdateElementData;


/**
 * Queue for {@link com.wiley.ws.core.queues.data.OrderStatusUpdateElementData}
 */
public class OrderStatusUpdateQueue extends AbstractUpdateQueue<OrderStatusUpdateElementData>
{
	// EMPTY - just to instantiate bean
}
