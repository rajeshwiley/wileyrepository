package com.wiley.ws.core.inventory.impl;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.WarehouseService;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Value;

import com.wiley.core.product.WileyProductService;
import com.wiley.core.wileycom.stock.service.WileycomStockLevelService;
import com.wiley.ws.core.inventory.InventoryUpdateInfo;
import com.wiley.ws.core.inventory.WileycomWsStockLevelFacade;
import com.wiley.ws.core.inventory.dto.InventoryUpdateRequestWsDTO;
import com.wiley.ws.core.product.dataintegration.common.exception.DataImportValidationErrorsException;


/**
 * Created by Raman_Hancharou on 4/3/2017.
 */
public class WileycomWsStockLevelFacadeImpl implements WileycomWsStockLevelFacade
{
	public static final String WAREHOUSE_ID = "warehouseId";

	@Value("wileyProductCatalog")
	private String productCatalogId;
	@Value("Staged")
	private String stagedCatalogVersion;
	@Value("Online")
	private String onlineCatalogVersion;

	@Resource
	private WileycomStockLevelService wileycomStockLevelService;
	@Resource
	private ModelService modelService;
	@Resource
	private ProductService productService;
	@Resource
	private WarehouseService warehouseService;
	@Resource
	private CatalogVersionService catalogVersionService;
	@Resource(name = "productService")
	private WileyProductService wileyProductService;


	@Override
	public InventoryUpdateInfo updateStockLevel(final String code, final InventoryUpdateRequestWsDTO inventoryUpdateRequestWsDTO)
	{
		InventoryUpdateInfo inventoryUpdateInfo;
		StockLevelModel stockLevelModel;
		WarehouseModel warehouse;

		// validate that warehouse with passed id exists
		try
		{
			warehouse = warehouseService.getWarehouseForCode(inventoryUpdateRequestWsDTO.getWarehouseId());
		}
		catch (UnknownIdentifierException | AmbiguousIdentifierException e)
		{
			throw DataImportValidationErrorsException.ofNoReferenceException(StockLevelModel._TYPECODE, WAREHOUSE_ID);
		}

		try
		{
			stockLevelModel = wileycomStockLevelService.findStockLevelByCode(code);
			if (!inventoryUpdateRequestWsDTO.getWarehouseId().equals(stockLevelModel.getWarehouse().getCode()))
			{
				throw DataImportValidationErrorsException.ofImmutableFieldException(StockLevelModel._TYPECODE, WAREHOUSE_ID);
			}
			Long existedSequenceId = stockLevelModel.getSequenceId();
			if (existedSequenceId != null && inventoryUpdateRequestWsDTO.getSequenceId() != null
					&& inventoryUpdateRequestWsDTO.getSequenceId() <= existedSequenceId)
			{
				inventoryUpdateInfo = InventoryUpdateInfo.LOWER_SEQUENCE_ID;
			}
			else
			{
				updateStockLevel(inventoryUpdateRequestWsDTO, stockLevelModel);
				inventoryUpdateInfo = InventoryUpdateInfo.UPDATED;
			}
		}
		catch (UnknownIdentifierException e)
		{
			stockLevelModel = modelService.create(StockLevelModel.class);
			stockLevelModel.setCode(code);
			stockLevelModel.setWarehouse(warehouse);
			updateStockLevel(inventoryUpdateRequestWsDTO, stockLevelModel);
			inventoryUpdateInfo = InventoryUpdateInfo.CREATED;
		}
		return inventoryUpdateInfo;
	}

	protected void updateStockLevel(final InventoryUpdateRequestWsDTO inventoryUpdateRequestWsDTO,
			final StockLevelModel stockLevelModel)
	{
		final int oldStockLevelAmount = stockLevelModel.getAvailable();
		final int newStockLevelAmount = inventoryUpdateRequestWsDTO.getQuantity();

		stockLevelModel.setSequenceId(inventoryUpdateRequestWsDTO.getSequenceId());
		stockLevelModel.setNextDeliveryTime(inventoryUpdateRequestWsDTO.getNextAvailabilityDate());
		stockLevelModel.setAvailable(inventoryUpdateRequestWsDTO.getQuantity());
		stockLevelModel.setProductCode(inventoryUpdateRequestWsDTO.getProductId());
		List<ProductModel> products = findProductsInProductCatalog(inventoryUpdateRequestWsDTO.getProductId());
		if (!products.isEmpty())
		{
			stockLevelModel.setProducts(products);
		}
		modelService.save(stockLevelModel);

		if (oldStockLevelAmount != 0 ^ newStockLevelAmount != 0)
		{
			wileyProductService.updateProductsModifiedTime(inventoryUpdateRequestWsDTO.getProductId());
		}
	}

	protected List<ProductModel> findProductsInProductCatalog(final String productCode)
	{
		List<ProductModel> products = new ArrayList<>();
		try
		{
			products.add(productService.getProductForCode(getProductCatalogVersion(stagedCatalogVersion),
					productCode));
		}
		catch (UnknownIdentifierException e)
		{
		}
		try
		{
			products.add(productService.getProductForCode(getProductCatalogVersion(onlineCatalogVersion),
					productCode));
		}
		catch (UnknownIdentifierException e)
		{
		}
		return products;
	}

	protected CatalogVersionModel getProductCatalogVersion(final String catalogVersion)
	{
		return catalogVersionService.getCatalogVersion(productCatalogId, catalogVersion);
	}
}
