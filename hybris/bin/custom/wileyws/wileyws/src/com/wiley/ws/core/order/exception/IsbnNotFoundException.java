package com.wiley.ws.core.order.exception;


import de.hybris.platform.servicelayer.util.ServicesUtil;

import javax.validation.constraints.NotNull;


/**
 * Created by Georgii_Gavrysh on 8/15/2016.
 */
public class IsbnNotFoundException extends WileywsValidationException
{
	private static final String ERROR_MESSAGE_FORMAT = "Order entry not found for the ISBN given. Order code = [%s], ISBN = [%s]";

	private String orderCode;
	private String isbn;

	public IsbnNotFoundException(@NotNull final String orderCode, @NotNull final String isbn)
	{
		super(String.format(ERROR_MESSAGE_FORMAT, orderCode, isbn));
		ServicesUtil.validateParameterNotNullStandardMessage(orderCode, "orderCode");
		ServicesUtil.validateParameterNotNullStandardMessage(isbn, "isbn");
		this.orderCode = orderCode;
		this.isbn = isbn;
	}

	public String getOrderCode()
	{
		return orderCode;
	}

	public String getIsbn()
	{
		return isbn;
	}

	public void setOrderCode(final String orderCode)
	{
		this.orderCode = orderCode;
	}

	public void setIsbn(final String isbn)
	{
		this.isbn = isbn;
	}
}
