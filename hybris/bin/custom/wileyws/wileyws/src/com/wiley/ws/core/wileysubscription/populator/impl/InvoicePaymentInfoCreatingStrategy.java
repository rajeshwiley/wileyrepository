package com.wiley.ws.core.wileysubscription.populator.impl;

import de.hybris.platform.core.model.order.payment.InvoicePaymentInfoModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

import com.wiley.ws.core.wileysubscription.dto.PaymentInfoUpdateRequestWsDTO;

import static com.wiley.facades.populators.PopulateUtils.populateFieldIfNotNull;


/**
 * Implementation for {@link InvoicePaymentInfoModel}.
 */
public class InvoicePaymentInfoCreatingStrategy extends AbstractPaymentInfoCreatingStrategy<InvoicePaymentInfoModel>
{

	@Resource
	private KeyGenerator guidKeyGenerator;

	@Override
	public void populate(final PaymentInfoUpdateRequestWsDTO paymentInfoUpdateRequestWsDTO,
			final InvoicePaymentInfoModel invoicePaymentInfoModel) throws ConversionException
	{
		// populate required fields
		if (StringUtils.isEmpty(invoicePaymentInfoModel.getCode()))
		{
			invoicePaymentInfoModel.setCode(guidKeyGenerator.generate().toString());
		}

		// populate optional fields
		populateFieldIfNotNull(this::populateBillingAddress, invoicePaymentInfoModel,
				paymentInfoUpdateRequestWsDTO.getPaymentAddress());
	}

	@Override
	public Class<InvoicePaymentInfoModel> getPaymentInfoClass()
	{
		return InvoicePaymentInfoModel.class;
	}
}
