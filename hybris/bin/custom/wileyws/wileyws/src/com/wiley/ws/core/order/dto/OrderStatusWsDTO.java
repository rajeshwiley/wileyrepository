package com.wiley.ws.core.order.dto;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import de.hybris.platform.core.enums.OrderStatus;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;



/**
 * The dto for order status update.
 */
public class OrderStatusWsDTO
{
	/**
	 * Available order status
	 */
	public enum StatusEnum {
		NOT_YET_PROCESSED(OrderStatus.NOT_YET_PROCESSED),
		PARTIALLY_PROCESSED(OrderStatus.PARTIALLY_PROCESSED),
		COMPLETELY_PROCESSED(OrderStatus.COMPLETED),
		CANCELLED(OrderStatus.CANCELLED),
		FAILED(OrderStatus.FAILED);

		private OrderStatus value;

		public OrderStatus getValue()
		{
			return value;
		}

		StatusEnum(final OrderStatus value) {
			this.value = value;
		}

		@Override
		@JsonValue
		public String toString() {
			return String.valueOf(this.name());
		}
	}
	@NotNull
	private StatusEnum status;
	@Valid
	@NotNull
	private StatusMessageWsDTO message;


	/**
	 * Available order status
	 **/
	public OrderStatusWsDTO status(final StatusEnum status) {
		this.status = status;
		return this;
	}

	@JsonProperty("status")
	public StatusEnum getStatus() {
		return status;
	}
	public void setStatus(final StatusEnum status) {
		this.status = status;
	}


	/**
	 **/
	public OrderStatusWsDTO message(final StatusMessageWsDTO message) {
		this.message = message;
		return this;
	}

	@JsonProperty("message")
	public StatusMessageWsDTO getMessage() {
		return message;
	}
	public void setMessage(final StatusMessageWsDTO message) {
		this.message = message;
	}





	@Override
	public String toString()
	{
		return new ToStringBuilder(this)
				.append("status", status)
				.append("message", message)
				.toString();
	}

	@Override
	public boolean equals(final Object obj)
	{
		if (obj == null)
		{
			return false;
		}
		if (obj == this)
		{
			return true;
		}
		if (obj.getClass() != getClass())
		{
			return false;
		}
		OrderStatusWsDTO rhs = (OrderStatusWsDTO) obj;
		return new EqualsBuilder()
				.append(this.status, rhs.status)
				.append(this.message, rhs.message)
				.isEquals();
	}

	@Override
	public int hashCode()
	{
		return new HashCodeBuilder()
				.append(status)
				.append(message)
				.toHashCode();
	}


}
