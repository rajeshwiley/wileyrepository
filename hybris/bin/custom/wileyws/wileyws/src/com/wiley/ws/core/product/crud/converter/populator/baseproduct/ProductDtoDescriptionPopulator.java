package com.wiley.ws.core.product.crud.converter.populator.baseproduct;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.List;
import java.util.Locale;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.LocaleUtils;

import com.wiley.core.model.WileyProductModel;
import com.wiley.ws.core.product.crud.dto.WileyProductWsDto;
import com.wiley.ws.core.product.dataintegration.common.dto.LocalizedLongStringSwgDTO;


public class ProductDtoDescriptionPopulator implements Populator<WileyProductWsDto, WileyProductModel>
{
	@Override
	public void populate(final WileyProductWsDto productDto, final WileyProductModel wileyProductModel) throws ConversionException
	{
		final List<LocalizedLongStringSwgDTO> descriptions = productDto.getDescription();
		if (CollectionUtils.isNotEmpty(descriptions))
		{
			for (final LocalizedLongStringSwgDTO description : descriptions)
			{
				final Locale locale = LocaleUtils.toLocale(description.getLocale());
				wileyProductModel.setDescription(description.getValue(), locale);
			}
		}
	}
}
