package com.wiley.ws.core.orderEntry.data;

import java.util.Objects;

import org.apache.commons.lang3.builder.ToStringBuilder;


public class UpdateOrderEntryInfoRequest
{
	private String status;
	private String additionalInfo;

	public String getStatus()
	{
		return status;
	}

	public void setStatus(final String status)
	{
		this.status = status;
	}

	public String getAdditionalInfo()
	{
		return additionalInfo;
	}

	public void setAdditionalInfo(final String additionalInfo)
	{
		this.additionalInfo = additionalInfo;
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		UpdateOrderEntryInfoRequest that = (UpdateOrderEntryInfoRequest) o;
		return Objects.equals(status, that.status) && Objects.equals(additionalInfo, that.additionalInfo);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(status, additionalInfo);
	}

	@Override
	public String toString()
	{
		return new ToStringBuilder(this)
				.append("status", status)
				.append("additionalInfo", additionalInfo)
				.toString();
	}
}
