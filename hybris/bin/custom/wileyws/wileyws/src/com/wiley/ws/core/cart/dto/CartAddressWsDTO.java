package com.wiley.ws.core.cart.dto;

import java.util.Objects;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Common Address DTO for cart related requests.
 */
public class CartAddressWsDTO
{
	/**
	 * Postal code.
	 */
	@Size(max = 255)
	private String postcode;
	/**
	 * Two-letter ISO 3166-1 alpha-2 code that specifies a country.
	 * See https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2 for details.
	 */
	@NotNull
	@Pattern(regexp = "^[A-Z]{2}$")
	private String country;
	/**
	 * ISO code that specifies a state or province. The codes were derived from ISO 3166-2 codes, for non-standard
	 * Armed Forces regions values were provided by Wiley. See https://jira.wiley.ru/browse/ECSC-5892 for details.
	 */
	@Pattern(regexp = "^[0-9A-Z]{1,3}$")
	private String state;
	/**
	 * City/town.
	 */
	@Size(max = 255)
	private String city;
	/**
	 * First line of the address.
	 */
	@Size(max = 255)
	private String line1;
	/**
	 * Second line of the address.
	 */
	@Size(max = 255)
	private String line2;
	/**
	 * User’s first name.
	 */
	@Size(max = 255)
	private String firstName;
	/**
	 * User’s last name.
	 */
	@Size(max = 255)
	private String lastName;
	/**
	 * Phone number.
	 */
	@Size(max = 255)
	private String phoneNumber;

	/**
	 * Email.
	 */
	@Size(max = 255)
	private String email;

	/**
	 * User’s organization/institution/university.
	 */
	@Size(max = 255)
	private String organization;

	/**
	 * User’s department.
	 */
	@Size(max = 255)
	private String department;

	@JsonProperty("postcode")
	public String getPostcode()
	{
		return postcode;
	}

	public void setPostcode(final String postcode)
	{
		this.postcode = postcode;
	}

	@JsonProperty("country")
	public String getCountry()
	{
		return country;
	}

	public void setCountry(final String country)
	{
		this.country = country;
	}

	@JsonProperty("state")
	public String getState()
	{
		return state;
	}

	public void setState(final String state)
	{
		this.state = state;
	}

	@JsonProperty("city")
	public String getCity()
	{
		return city;
	}

	public void setCity(final String city)
	{
		this.city = city;
	}

	@JsonProperty("line1")
	public String getLine1()
	{
		return line1;
	}

	public void setLine1(final String line1)
	{
		this.line1 = line1;
	}

	@JsonProperty("line2")
	public String getLine2()
	{
		return line2;
	}

	public void setLine2(final String line2)
	{
		this.line2 = line2;
	}

	@JsonProperty("firstName")
	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(final String firstName)
	{
		this.firstName = firstName;
	}

	@JsonProperty("lastName")
	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(final String lastName)
	{
		this.lastName = lastName;
	}

	@JsonProperty("phoneNumber")
	public String getPhoneNumber()
	{
		return phoneNumber;
	}

	public void setPhoneNumber(final String phoneNumber)
	{
		this.phoneNumber = phoneNumber;
	}

	@JsonProperty("email")
	public String getEmail()
	{
		return email;
	}

	public void setEmail(final String email)
	{
		this.email = email;
	}

	@JsonProperty("organization")
	public String getOrganization()
	{
		return organization;
	}

	public void setOrganization(final String organization)
	{
		this.organization = organization;
	}

	@JsonProperty("department")
	public String getDepartment()
	{
		return department;
	}

	public void setDepartment(final String department)
	{
		this.department = department;
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (!(o instanceof CartAddressWsDTO))
		{
			return false;
		}
		final CartAddressWsDTO that = (CartAddressWsDTO) o;
		return Objects.equals(postcode, that.postcode)
				&& Objects.equals(country, that.country)
				&& Objects.equals(state, that.state)
				&& Objects.equals(city, that.city)
				&& Objects.equals(line1, that.line1)
				&& Objects.equals(line2, that.line2)
				&& Objects.equals(firstName, that.firstName)
				&& Objects.equals(lastName, that.lastName)
				&& Objects.equals(phoneNumber, that.phoneNumber)
				&& Objects.equals(email, that.email)
				&& Objects.equals(organization, that.organization)
				&& Objects.equals(department, that.department);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(postcode, country, state, city, line1, line2, firstName, lastName, phoneNumber, email, organization,
				department);
	}

	@Override
	public String toString()
	{
		return new ToStringBuilder(this)
				.append("postcode", postcode)
				.append("country", country)
				.append("state", state)
				.append("city", city)
				.append("line1", line1)
				.append("line2", line2)
				.append("firstName", firstName)
				.append("lastName", lastName)
				.append("phoneNumber", phoneNumber)
				.append("email", email)
				.append("organization", organization)
				.append("department", department)
				.toString();
	}
}
