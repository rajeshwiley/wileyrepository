package com.wiley.ws.core.common.dto;

import java.util.Objects;

import javax.validation.constraints.Pattern;


/**
 * Wrapper for currency string representation.
 * hree-letter ISO 4217 code that specifies a currency. See https://en.wikipedia.org/wiki/ISO_4217 for details.
 */
public class CurrencySwgDTO
{
	private String value = null;

	public CurrencySwgDTO(final String value)
	{
		this.value = value;
	}

	@Pattern(regexp = "^[A-Z]{3}$", message = "validation.currency.invalid")
	public String getValue()
	{
		return value;
	}

	public void setValue(final String value)
	{
		this.value = value;
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		CurrencySwgDTO currencySwgDTO = (CurrencySwgDTO) o;
		return Objects.equals(value, currencySwgDTO.value);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(value);
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("class CurrencySwgDTO {\n");
		sb.append("  value: ").append(value).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
