package com.wiley.ws.core.order.service;

import de.hybris.platform.core.model.order.AbstractOrderModel;


/**
 * Author Herman_Chukhrai (EPAM)
 */
public interface WileycomSubtotalWithoutDiscountApplier
{

	void recalculateAndSetSubtotalWithoutDiscount(AbstractOrderModel orderModel);
}
