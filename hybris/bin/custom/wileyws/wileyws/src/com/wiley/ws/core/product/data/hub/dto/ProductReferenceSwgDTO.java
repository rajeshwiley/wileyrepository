package com.wiley.ws.core.product.data.hub.dto;

import java.util.Objects;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Reference to a product of the specified type.
 **/
public class ProductReferenceSwgDTO
{


	private String productCode = null;
	private ReferenceTypeEnum referenceType = null;

	public enum ReferenceTypeEnum
	{
		SIMILAR, PRODUCT_SET, PRODUCT_SET_COMPONENT, FREE_TRIAL, TEXTBOOK, WILEY_PLUS_COURSE, WILEY_PLUS_PRODUCT_SET
	}

	/**
	 **/
	@NotNull
	@Size(max = 255)
	@JsonProperty("productCode")
	public String getProductCode()
	{
		return productCode;
	}

	public void setProductCode(final String productCode)
	{
		this.productCode = productCode;
	}

	/**
	 * Type of the reference.
	 **/
	@NotNull
	@JsonProperty("referenceType")
	public ReferenceTypeEnum getReferenceType()
	{
		return referenceType;
	}

	public void setReferenceType(final ReferenceTypeEnum referenceType)
	{
		this.referenceType = referenceType;
	}


	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		ProductReferenceSwgDTO productReference = (ProductReferenceSwgDTO) o;
		return Objects.equals(productCode, productReference.productCode)
				&& Objects.equals(referenceType, productReference.referenceType);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(productCode, referenceType);
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("class ProductReferenceSwgDTO {\n");
		sb.append("  productCode: ").append(productCode).append("\n");
		sb.append("  referenceType: ").append(referenceType).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
