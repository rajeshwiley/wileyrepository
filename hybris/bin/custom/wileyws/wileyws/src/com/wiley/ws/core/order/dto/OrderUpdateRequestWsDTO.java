package com.wiley.ws.core.order.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;


/**
 * The result of the order changes which took part on SAP ERP side. In case of order change (count of entries, delivery method,
 * delivery cost, etc) we expect to have full reculculated order back (totalPrice, totalTax, totalDiscount, etc)
 *
 * Author Herman_Chukhrai (EPAM)
 */
public class OrderUpdateRequestWsDTO
{
	/**
	 * Unique identifier of the user's B2B unit (company). Applicable only for B2B customers.
	 */
	private String sapAccountNumber;
	/**
	 * Two-letter ISO 3166-1 alpha-2 code that specifies a country. See https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2
	 * for details.
	 */
	private String country;
	/**
	 * Three-letter ISO 4217 code that specifies a currency. See https://en.wikipedia.org/wiki/ISO_4217 for details.
	 */
	private String currency;
	/**
	 * ID of school a customer is currently enrolled to
	 */
	private String schoolId;
	/**
	 * Code of the delivery option (priority code) that was chosen by the customer for this order.
	 */
	private String deliveryOptionCode;
	@Valid
	private OrderAddressWsDTO deliveryAddress;
	@Valid
	private OrderAddressWsDTO paymentAddress;
	/**
	 * The full list of the order entries. The order entries from the request might be adjusted, new entries might be added.
	 * If an entry is absent in this list, it is considered as removed. Only one entry can exist for a given product code.
	 */
	@Valid
	private List<OrderUpdateEntryRequestWsDTO> entries = new ArrayList<>();
	/**
	 * Sum of the totals of all the order entries. An entry's total is calculated as the entry's original price without any
	 * discount multipled by the entry's quantity. The en_US locale is used for the value.
	 */
	private Double subtotal;
	/**
	 * List of applied global (order level) discounts
	 */
	@Valid
	private List<DiscountWsDTO> discounts = new ArrayList<>();
	/**
	 * Sum of all discounts (including any promotions and vouchers) applied to the order. The en_US locale is used for the value.
	 */
	private Double totalDiscount;
	/**
	 * Delivery cost for the chosen delivery option. The en_US locale is used for the value.
	 */
	private Double deliveryCost;
	/**
	 * Total tax for the order. The en_US locale is used for the value.
	 */
	private Double totalTax;
	/**
	 * Total price for the order. Total price is calculated as subtotal minus totalDiscount plus deliveryCost plus totalTax.
	 * The en_US locale is used for the value.
	 */
	private Double totalPrice;
	/**
	 * The list of messages for the user describing events occured on the order processing.
	 */
	@Valid
	private List<UpdateMessageWsDTO> messages = new ArrayList<>();
	/**
	 * List of provided access tokens
	 */
	@Valid
	private List<DigitalProductAccessTokenWsDTO> digitalProductAccessTokens = new ArrayList<>();

	@JsonProperty("sapAccountNumber")
	public String getSapAccountNumber()
	{
		return sapAccountNumber;
	}

	public void setSapAccountNumber(final String sapAccountNumber)
	{
		this.sapAccountNumber = sapAccountNumber;
	}

	@JsonProperty("country")
	public String getCountry()
	{
		return country;
	}

	public void setCountry(final String country)
	{
		this.country = country;
	}

	@JsonProperty("currency")
	public String getCurrency()
	{
		return currency;
	}

	public void setCurrency(final String currency)
	{
		this.currency = currency;
	}

	@JsonProperty("schoolId")
	public String getSchoolId()
	{
		return schoolId;
	}

	public void setSchoolId(final String schoolId)
	{
		this.schoolId = schoolId;
	}

	@JsonProperty("deliveryOptionCode")
	public String getDeliveryOptionCode()
	{
		return deliveryOptionCode;
	}

	public void setDeliveryOptionCode(final String deliveryOptionCode)
	{
		this.deliveryOptionCode = deliveryOptionCode;
	}

	@JsonProperty("deliveryAddress")
	public OrderAddressWsDTO getDeliveryAddress()
	{
		return deliveryAddress;
	}

	public void setDeliveryAddress(final OrderAddressWsDTO deliveryAddress)
	{
		this.deliveryAddress = deliveryAddress;
	}

	@JsonProperty("paymentAddress")
	public OrderAddressWsDTO getPaymentAddress()
	{
		return paymentAddress;
	}

	public void setPaymentAddress(final OrderAddressWsDTO paymentAddress)
	{
		this.paymentAddress = paymentAddress;
	}

	@JsonProperty("entries")
	public List<OrderUpdateEntryRequestWsDTO> getEntries()
	{
		return entries;
	}

	public void setEntries(final List<OrderUpdateEntryRequestWsDTO> entries)
	{
		this.entries = entries;
	}

	@JsonProperty("subtotal")
	public Double getSubtotal()
	{
		return subtotal;
	}

	public void setSubtotal(final Double subtotal)
	{
		this.subtotal = subtotal;
	}

	@JsonProperty("discounts")
	public List<DiscountWsDTO> getDiscounts()
	{
		return discounts;
	}

	public void setDiscounts(final List<DiscountWsDTO> discounts)
	{
		this.discounts = discounts;
	}

	@JsonProperty("totalDiscount")
	public Double getTotalDiscount()
	{
		return totalDiscount;
	}

	public void setTotalDiscount(final Double totalDiscount)
	{
		this.totalDiscount = totalDiscount;
	}

	@JsonProperty("deliveryCost")
	public Double getDeliveryCost()
	{
		return deliveryCost;
	}

	public void setDeliveryCost(final Double deliveryCost)
	{
		this.deliveryCost = deliveryCost;
	}

	@JsonProperty("totalTax")
	public Double getTotalTax()
	{
		return totalTax;
	}

	public void setTotalTax(final Double totalTax)
	{
		this.totalTax = totalTax;
	}

	@JsonProperty("totalPrice")
	public Double getTotalPrice()
	{
		return totalPrice;
	}

	public void setTotalPrice(final Double totalPrice)
	{
		this.totalPrice = totalPrice;
	}

	@JsonProperty("messages")
	public List<UpdateMessageWsDTO> getMessages()
	{
		return messages;
	}

	public void setMessages(final List<UpdateMessageWsDTO> messages)
	{
		this.messages = messages;
	}

	@JsonProperty("digitalProductAccessTokens")
	public List<DigitalProductAccessTokenWsDTO> getDigitalProductAccessTokens()
	{
		return digitalProductAccessTokens;
	}

	public void setDigitalProductAccessTokens(
			final List<DigitalProductAccessTokenWsDTO> digitalProductAccessTokens)
	{
		this.digitalProductAccessTokens = digitalProductAccessTokens;
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}

		if (o == null || getClass() != o.getClass())
		{
			return false;
		}

		final OrderUpdateRequestWsDTO that = (OrderUpdateRequestWsDTO) o;

		return new EqualsBuilder()
				.append(sapAccountNumber, that.sapAccountNumber)
				.append(country, that.country)
				.isEquals();
	}

	@Override
	public int hashCode()
	{
		return new HashCodeBuilder(17, 37)
				.append(sapAccountNumber)
				.append(country)
				.toHashCode();
	}


	@Override
	public String toString()
	{
		return new ToStringBuilder(this)
				.append("sapAccountNumber", sapAccountNumber)
				.append("country", country)
				.append("entries", entries)
				.append("totalPrice", totalPrice)
				.toString();
	}
}
