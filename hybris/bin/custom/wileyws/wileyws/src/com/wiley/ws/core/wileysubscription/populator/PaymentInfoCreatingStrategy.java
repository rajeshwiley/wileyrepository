package com.wiley.ws.core.wileysubscription.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.wiley.core.model.WileySubscriptionModel;
import com.wiley.ws.core.wileysubscription.dto.PaymentInfoUpdateRequestWsDTO;


/**
 * Strategy is used to create and populate Payment info for WileySubscription
 */
public interface PaymentInfoCreatingStrategy<T extends PaymentInfoModel> extends Populator<PaymentInfoUpdateRequestWsDTO, T>
{
	/**
	 * Checks if Payment info is supported by current strategy.
	 *
	 * @param obj
	 * 		PaymentInfo to check
	 * @return true if payment info is supported else false
	 */
	boolean isSupported(@Nullable T obj);

	/**
	 * Method checks payment info if it is supported by current strategy
	 * and creates new payment info if current payment info is not supported.
	 * The previous payment info is removed.
	 * Bind payment info with WileySubscription.
	 *
	 * @param paymentInfo
	 * @param wileySubscription
	 * @return supported payment info.
	 */
	@Nonnull
	T getOrCreateNewSupportedPaymentInfo(@Nullable T paymentInfo, @Nonnull WileySubscriptionModel wileySubscription);
}
