package com.wiley.ws.core.product.crud.converter.populator.baseproduct;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.variants.model.VariantTypeModel;

import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.model.WileyProductModel;
import com.wiley.core.model.WileyPurchaseOptionProductModel;
import com.wiley.ws.core.product.crud.dto.WileyProductWsDto;

/**
 * Populates Variant type during base product creation
 *
 * @author Nitin_Gakhar
 *
 */
public class WileyProductVariantTypePopulator implements Populator<WileyProductWsDto, WileyProductModel>
{
	private final TypeService typeService;

	@Autowired
	public WileyProductVariantTypePopulator(final TypeService typeService)
	{
		this.typeService = typeService;
	}

	@Override
	public void populate(final WileyProductWsDto wileyProductWsDto, final WileyProductModel wileyProductModel)
			throws ConversionException
	{
		wileyProductModel.setVariantType(
				(VariantTypeModel) typeService.getComposedTypeForCode(WileyPurchaseOptionProductModel._TYPECODE));
	}

}
