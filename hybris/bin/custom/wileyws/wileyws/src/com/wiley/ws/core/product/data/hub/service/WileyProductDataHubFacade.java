package com.wiley.ws.core.product.data.hub.service;

import com.wiley.facades.product.ProductUpdateResult;
import com.wiley.ws.core.product.data.hub.dto.ProductSwgDTO;


public interface WileyProductDataHubFacade
{
	/**
	 * Update an already existing product. If a product with the specified code doesn't exist, a new product is created.
	 *
	 * @param productCode
	 * 		Product code
	 * @param externalProduct
	 * 		product DTO that is serialized from external PDH request.
	 * 		This object contains updates which should be applied for the Hybris product.
	 * @return ProductUpdateResult product operation result
	 */
	ProductUpdateResult createOrUpdateProductForSite(String productCode, ProductSwgDTO externalProduct);
}
