package com.wiley.ws.core.order.dto;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Author Herman_Chukhrai (EPAM)
 */
public class DigitalProductAccessTokenWsDTO
{

	/**
	 * ISBN-13. See: https://en.wikipedia.org/wiki/International_Standard_Book_Number
	 */
	@NotNull
	private String isbn;

	/**
	 * An external (non hybris) fulfillment process will include handover of required transactional information to VitalSource
	 * and Lightning Source. In case If any of these entitlement systems return code that should be used by a customer
	 * to obtain an access to digital content, then it will be provided back to hybris
	 */
	@NotNull
	private String token;

	@JsonProperty("token")
	public String getToken()
	{
		return token;
	}

	public void setToken(final String token)
	{
		this.token = token;
	}

	@JsonProperty("isbn")
	public String getIsbn()
	{
		return isbn;
	}

	public void setIsbn(final String isbn)
	{
		this.isbn = isbn;
	}

	@Override
	public String toString()
	{
		return new ToStringBuilder(this)
				.append("isbn", isbn)
				.append("token", token)
				.toString();
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}

		if (o == null || getClass() != o.getClass())
		{
			return false;
		}

		final DigitalProductAccessTokenWsDTO that = (DigitalProductAccessTokenWsDTO) o;

		return new EqualsBuilder()
				.append(isbn, that.isbn)
				.append(token, that.token)
				.isEquals();
	}

	@Override
	public int hashCode()
	{
		return new HashCodeBuilder(17, 37)
				.append(isbn)
				.append(token)
				.toHashCode();
	}
}
