/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package com.wiley.ws.core.queues.impl;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import com.google.common.base.Predicate;
import com.google.common.collect.Lists;
import com.wiley.ws.core.queues.UpdateQueue;


/**
 * Abstract implementation of {@link com.wiley.ws.core.queues.UpdateQueue} using {@link TreeMap}
 * for storing elements WARNING: this queue has limited capacity due to its in-memory nature
 *
 * @param <T>
 * 		the type parameter
 */
public abstract class AbstractUpdateQueue<T> extends TreeMap<Long, T> implements UpdateQueue<T>
{
	private int maxCapacity = 1000;

	/**
	 * Gets items.
	 *
	 * @return the items
	 */
	@Override
	public List<T> getItems()
	{
		return Lists.newArrayList(values());
	}

	/**
	 * Gets items.
	 *
	 * @param newerThan
	 * 		the newer than
	 * @return the items
	 */
	@Override
	public List<T> getItems(final Date newerThan)
	{
		return Lists.newArrayList(tailMap(Long.valueOf(newerThan.getTime())).values());
	}

	/**
	 * Add item.
	 *
	 * @param item
	 * 		the item
	 */
	@Override
	public void addItem(final T item)
	{
		if (size() < maxCapacity)
		{
			Long timeKey = getTimeKey(item);
			while (containsKey(timeKey))
			{
				timeKey = Long.valueOf(timeKey.longValue() + 1);
			}
			put(timeKey, item);
		}
	}

	/**
	 * Add items.
	 *
	 * @param items
	 * 		the items
	 */
	@Override
	public void addItems(final List<T> items)
	{
		for (final T item : items)
		{
			addItem(item);
		}
	}

	/**
	 * Remove items.
	 *
	 * @param olderThan
	 * 		the older than
	 */
	@Override
	public void removeItems(final Date olderThan)
	{
		final SortedMap<Long, T> clone = (SortedMap<Long, T>) clone();
		final SortedMap<Long, T> newerThan = clone.tailMap(Long.valueOf(olderThan.getTime()));
		clear();
		putAll(newerThan);
	}

	/**
	 * Remove items.
	 */
	@Override
	public void removeItems()
	{
		clear();
	}

	/**
	 * Remove items.
	 *
	 * @param predicate
	 * 		the predicate
	 */
	@Override
	public void removeItems(final Predicate<T> predicate)
	{
		final Iterator<T> it = values().iterator();
		while (it.hasNext())
		{
			if (predicate.apply(it.next()))
			{
				it.remove();
			}
		}
	}

	/**
	 * Gets last item.
	 *
	 * @return the last item
	 */
	@Override
	public T getLastItem()
	{
		T ret = null;
		if (!isEmpty())
		{
			ret = lastEntry().getValue();
		}
		return ret;
	}

	/**
	 * Gets max capacity.
	 *
	 * @return the max capacity
	 */
	public int getMaxCapacity()
	{
		return maxCapacity;
	}

	/**
	 * Sets max capacity.
	 *
	 * @param maxCapacity
	 * 		the max capacity
	 */
	public void setMaxCapacity(final int maxCapacity)
	{
		this.maxCapacity = maxCapacity;
	}

	/**
	 * Gets time key.
	 *
	 * @param item
	 * 		the item
	 * @return the time key
	 */
	protected Long getTimeKey(final T item)
	{
		return Long.valueOf(System.currentTimeMillis());
	}

}
