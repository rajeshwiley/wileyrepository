package com.wiley.ws.core.product.data.hub.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.util.Collection;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.ws.core.product.data.hub.dto.ProductSwgDTO;


/**
 * Populate from {@link ProductSwgDTO#getCountries()} to {@link ProductModel#setCountryRestrictions(Collection)}
 */
public class CountryPDHPopulator implements Populator<ProductSwgDTO, ProductModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(CountryPDHPopulator.class);

	@Autowired
	private CommonI18NService commonI18NService;

	@Override
	public void populate(final ProductSwgDTO product, final ProductModel productModel) throws ConversionException
	{
		if (product.getCountries() != null)
		{
			final Collection<CountryModel> countries = product.getCountries().stream()
					.map(country -> commonI18NService.getCountry(country.getValue()))
					.collect(Collectors.toList());

			productModel.setCountryRestrictions(countries);
		}
		else
		{
			LOG.debug("Skip populating countries due to null");
		}

	}


}
