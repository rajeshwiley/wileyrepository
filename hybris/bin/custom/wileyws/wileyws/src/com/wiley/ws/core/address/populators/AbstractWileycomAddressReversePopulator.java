package com.wiley.ws.core.address.populators;

import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import javax.annotation.Resource;

import com.wiley.core.i18n.WileyCommonI18NService;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


/**
 * Created by Raman_Hancharou on 10/21/2016.
 */
public abstract class AbstractWileycomAddressReversePopulator
{
	@Resource
	private CommonI18NService commonI18NService;

	@Resource
	private WileyCommonI18NService wileyCommonI18NService;

	protected void populateCountry(final AddressModel target, final String countryCode)
	{
		target.setCountry(commonI18NService.getCountry(countryCode));
	}

	protected void populateRegion(final AddressModel target, final String regionCode)
	{
		final CountryModel country = target.getCountry();
		validateParameterNotNull(country, String.format("Could not find region [%s] without specified country.", regionCode));
		if (regionCode != null)
		{
			// Country value from target is used because this field in the request could be omitted at all.
			// All fields are optional
			target.setRegion(wileyCommonI18NService.getRegionForShortCode(country, regionCode));
		}
		else
		{
			final RegionModel region = target.getRegion();
			if (region != null && region.getIsocode() != null)
			{
				try
				{
					// check, if previous region is valid for new country
					wileyCommonI18NService.getRegionForShortCode(country, region.getIsocode());
				}
				catch (UnknownIdentifierException e)
				{
					// reset region
					target.setRegion(null);
				}
			}
		}
	}

	public CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	public WileyCommonI18NService getWileyCommonI18NService()
	{
		return wileyCommonI18NService;
	}
}
