package com.wiley.ws.core.product.data.hub.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.subscriptionservices.model.SubscriptionTermModel;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.fest.util.Collections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.model.WileyProductModel;
import com.wiley.core.product.WileySubscriptionTermService;
import com.wiley.ws.core.common.dto.ShortStringSwgDTO;
import com.wiley.ws.core.product.data.hub.dto.ProductSwgDTO;


/**
 * Populate {@link ProductSwgDTO#getSubscriptionTerms()}
 * to {@link WileyProductModel#setSubscriptionTerms(Set)}
 */
public class SubscriptionTermPDHPopulator implements Populator<ProductSwgDTO, WileyProductModel>
{

	private static final Logger LOG = LoggerFactory.getLogger(SubscriptionTermPDHPopulator.class);

	@Autowired
	private WileySubscriptionTermService wileySubscriptionTermService;

	@Override
	public void populate(final ProductSwgDTO product, final WileyProductModel productModel)
			throws ConversionException
	{
		final List<ShortStringSwgDTO> subscriptionTerms = product.getSubscriptionTerms();
		if (subscriptionTerms != null)
		{
			if (Collections.isEmpty(subscriptionTerms))
			{
				productModel.setSubscriptionTerms(null);
			}
			else
			{
				Set<SubscriptionTermModel> subsciptionTermModels = subscriptionTerms.stream().map(
						term -> wileySubscriptionTermService.getSubscriptionTerm(term.getValue())).collect(Collectors.toSet());
				productModel.setSubscriptionTerms(subsciptionTermModels);
			}

			LOG.debug("Product [{}]: subscription terms was set to [{}] from DTO values [{}]", productModel.getCode(),
					productModel.getSubscriptionTerms(), subscriptionTerms);
		}
		else
		{
			LOG.debug("Skip populating of subscriptionTerm due to null");
		}
	}
}
