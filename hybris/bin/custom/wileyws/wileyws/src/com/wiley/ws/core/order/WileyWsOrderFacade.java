package com.wiley.ws.core.order;

import com.wiley.integrations.order.dto.OrderWsDTO;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;


public interface WileyWsOrderFacade
{
	OrderWsDTO getOrderDetails(String guid, BaseSiteModel baseSite);
}
