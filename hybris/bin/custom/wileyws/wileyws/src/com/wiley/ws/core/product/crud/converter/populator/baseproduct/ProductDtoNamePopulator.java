package com.wiley.ws.core.product.crud.converter.populator.baseproduct;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.model.WileyProductModel;
import com.wiley.ws.core.product.crud.dto.WileyProductWsDto;
import com.wiley.ws.core.product.dataintegration.common.dto.LocalizedStringSwgDTO;
import com.wiley.ws.core.product.dataintegration.common.util.LocalizedPopulatorUtil;


public class ProductDtoNamePopulator implements Populator<WileyProductWsDto, WileyProductModel>
{

	private final LocalizedPopulatorUtil localizedPopulatorUtil;

	@Autowired
	public ProductDtoNamePopulator(final LocalizedPopulatorUtil localizedPopulatorUtil)
	{
		this.localizedPopulatorUtil = localizedPopulatorUtil;
	}

	@Override
	public void populate(final WileyProductWsDto productDto, final WileyProductModel wileyProductModel) throws ConversionException
	{
		final List<LocalizedStringSwgDTO> names = productDto.getName();
		if (CollectionUtils.isNotEmpty(names))
		{
			localizedPopulatorUtil.populateStrings(names, wileyProductModel::setName);
		}
	}
}
