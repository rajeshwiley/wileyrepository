package com.wiley.ws.core.product.dataintegration.common.service.impl;

import de.hybris.platform.validation.exceptions.HybrisConstraintViolation;
import de.hybris.platform.validation.model.constraints.ConstraintGroupModel;
import de.hybris.platform.validation.services.ValidationService;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.wiley.core.validation.service.ConstraintGroupService;
import com.wiley.ws.core.product.dataintegration.common.exception.DataImportValidationConstraintsException;
import com.wiley.ws.core.product.dataintegration.common.error.ErrorListSwgDTO;
import com.wiley.ws.core.product.dataintegration.common.error.ErrorSwgDTO;
import com.wiley.ws.core.product.dataintegration.common.exception.DataImportValidationErrorsException;
import com.wiley.ws.core.product.dataintegration.common.service.DataImportValidationService;

import static com.wiley.ws.core.product.dataintegration.common.error.ErrorSwgDTO.ReasonEnum.IMMUTABLE_FIELD_ERROR;
import static com.wiley.ws.core.product.dataintegration.common.error.ErrorSwgDTO.TypeEnum.VALIDATION;


public class DataImportValidationServiceImpl implements DataImportValidationService
{
	protected static final String MESSAGE_CODE_IMMUTABLE = "The field is immutable, it can't be changed after initialization";
	protected static final String MESSAGE_ITEM_NOT_UNIQUE = "The proposed ID is not unique";
	protected static final String SUBJECT_CODE = "code";
	protected static final String SUBJECT_ID = "id";


	private final ValidationService validationService;
	private final ConstraintGroupService constraintGroupService;

	@Value("${product.crud.validation.group.ids}")
	private String constraintGroupIds;

	private Collection<ConstraintGroupModel> constraintGroups;

	@Autowired
	public DataImportValidationServiceImpl(final ValidationService validationService,
			final ConstraintGroupService constraintGroupService)
	{
		this.validationService = validationService;
		this.constraintGroupService = constraintGroupService;
	}

	public <T> void validate(final T model)
	{
		final Collection<ConstraintGroupModel> constraintGroups = getConstraintGroups();
		final Set<HybrisConstraintViolation> violations = validationService.validate(model, constraintGroups);
		if (CollectionUtils.isNotEmpty(violations))
		{
			List<ErrorSwgDTO> errors = violations
					.stream()
					.map(violation -> ErrorSwgDTO.of(
							VALIDATION.toString(),
							violation.getConstraintModel().getId(),
							violation.getLocalizedMessage(),
							violation.getTypeName(),
							violation.getQualifier()
					))
					.collect(Collectors.toList());
			throw new DataImportValidationConstraintsException(ErrorListSwgDTO.of(errors));
		}
	}

	@Override
	public void validateItemDoesNotExist(final String code)
	{
		throw new UnsupportedOperationException("Validation is being performed in subclasses");
	}

	@Override
	public void validateCodeIsImmutable(final String currentCode, final String newCode, final String type)
	{
		if (!StringUtils.equals(currentCode, newCode))
		{
			final ErrorSwgDTO error = ErrorSwgDTO.of(VALIDATION, IMMUTABLE_FIELD_ERROR, MESSAGE_CODE_IMMUTABLE, type,
					SUBJECT_CODE);
			throw new DataImportValidationErrorsException(ErrorListSwgDTO.of(error));
		}
	}


	private Collection<ConstraintGroupModel> getConstraintGroups()
	{
		if (constraintGroups == null)
		{
			if (StringUtils.isBlank(constraintGroupIds))
			{
				constraintGroups = Collections.emptyList();
			}
			else
			{
				constraintGroups = Stream.of(StringUtils.split(constraintGroupIds, ','))
						.map(constraintGroupService::getConstraintGroupForId)
						.collect(Collectors.toList());
			}
		}
		return constraintGroups;
	}

}
