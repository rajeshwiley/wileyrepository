package com.wiley.ws.core.order.dto;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;


/**
 * Created by Georgii_Gavrysh on 8/10/2016.
 */
public class ShippingEventWsDTO
{
	@NotNull
	private String shippingDate;

	@NotNull
	private String trackingId;

	@NotNull
	private String carrier;

	@Valid
	private List<ShippingEventEntryWsDTO> entries;

	public String getShippingDate()
	{
		return shippingDate;
	}

	public String getTrackingId()
	{
		return trackingId;
	}

	public String getCarrier()
	{
		return carrier;
	}

	public List<ShippingEventEntryWsDTO> getEntries()
	{
		return entries;
	}

	public void setShippingDate(final String shippingDate)
	{
		this.shippingDate = shippingDate;
	}

	public void setTrackingId(final String trackingId)
	{
		this.trackingId = trackingId;
	}

	public void setCarrier(final String carrier)
	{
		this.carrier = carrier;
	}

	public void setEntries(final List<ShippingEventEntryWsDTO> entries)
	{
		this.entries = entries;
	}

	@Override
	public String toString()
	{
		return new ToStringBuilder(this)
				.append("shippingDate", shippingDate)
				.append("trackingId", trackingId)
				.append("carrier", carrier)
				.append("entries", entries)
				.toString();
	}

	@Override
	public boolean equals(final Object obj)
	{
		if (obj == null)
		{
			return false;
		}
		if (obj == this)
		{
			return true;
		}
		if (obj.getClass() != getClass())
		{
			return false;
		}
		ShippingEventWsDTO other = (ShippingEventWsDTO) obj;
		return new EqualsBuilder()
				.append(this.shippingDate, other.shippingDate)
				.append(this.trackingId, other.trackingId)
				.append(this.carrier, other.carrier)
				.append(this.entries, other.entries)
				.isEquals();
	}

	@Override
	public int hashCode()
	{
		return new HashCodeBuilder()
				.append(shippingDate)
				.append(trackingId)
				.append(carrier)
				.append(entries)
				.toHashCode();
	}
}
