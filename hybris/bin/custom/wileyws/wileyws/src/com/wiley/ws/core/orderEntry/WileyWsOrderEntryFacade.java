package com.wiley.ws.core.orderEntry;

import com.wiley.ws.core.order.dto.GetOrderEntriesResponseWsDTO;
import org.springframework.transaction.annotation.Transactional;

import com.wiley.integrations.order.dto.OrderEntryWsDTO;
import com.wiley.ws.core.orderEntry.data.UpdateOrderEntryInfoRequest;

import java.util.List;


public interface WileyWsOrderEntryFacade
{
	/**
	 * Validate and update orderEntry details.
	 *
	 * @param entryId
	 * 		the order entry Id
	 * @param updateOrderEntryInfoRequest
	 * 		the update order entry info request
	 * @return updated order entry DTO
	 */
	@Transactional
	OrderEntryWsDTO checkAndUpdateOrderEntryDetails(String entryId,
			UpdateOrderEntryInfoRequest updateOrderEntryInfoRequest);

    GetOrderEntriesResponseWsDTO getOrderEntries(String userId,
                                                 String businessItemId, String businessKey,
                                                 List<String> statuses, Integer pageSize,
                                                 Integer currentPage, String sort);
}
