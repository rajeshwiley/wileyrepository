package com.wiley.ws.core.inventory;

import com.wiley.ws.core.inventory.dto.InventoryUpdateRequestWsDTO;


/**
 * Created by Raman_Hancharou on 4/3/2017.
 */
public interface WileycomWsStockLevelFacade
{
	/**
	 * Update stockLevel if exists with passed code or create new StockLevel
	 *
	 * @param code
	 * 		the code of stock level
	 * @param inventoryUpdateRequestWsDTO
	 * 		the stock level information ws dto
	 * @return InventoryUpdateInfo
	 */
	InventoryUpdateInfo updateStockLevel(String code, InventoryUpdateRequestWsDTO inventoryUpdateRequestWsDTO);
}
