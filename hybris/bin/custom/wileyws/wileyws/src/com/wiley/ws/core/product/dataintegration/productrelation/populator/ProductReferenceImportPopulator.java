package com.wiley.ws.core.product.dataintegration.productrelation.populator;

import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.base.Preconditions;
import com.wiley.core.model.WileyPurchaseOptionProductModel;
import com.wiley.ws.core.product.dataintegration.common.exception.DataImportNotFoundException;
import com.wiley.ws.core.product.dataintegration.common.exception.DataImportValidationErrorsException;
import com.wiley.ws.core.product.dataintegration.common.service.ProductImportService;
import com.wiley.ws.core.product.dataintegration.productrelation.dto.ProductRelationSwgDTO;


public class ProductReferenceImportPopulator implements Populator<ProductRelationSwgDTO, ProductReferenceModel>
{
	private static final String NO_REFERENCE_TYPE_ERROR = "type";
	private final ProductImportService productImportService;
	private final EnumerationService enumerationService;

	@Autowired
	public ProductReferenceImportPopulator(
			final ProductImportService productImportService,
			final EnumerationService enumerationService)
	{
		this.productImportService = productImportService;
		this.enumerationService = enumerationService;
	}

	@Override
	public void populate(final ProductRelationSwgDTO source, final ProductReferenceModel target)
			throws ConversionException
	{
		Preconditions.checkNotNull(source, "Source can't be null");
		Preconditions.checkNotNull(target, "Target can't be null");
		Preconditions.checkArgument(StringUtils.isNotBlank(source.getTargetProductId()),
				"targetProductId can't be blank string");
		try
		{
			final WileyPurchaseOptionProductModel targetProduct =
					productImportService.getProductForCode(source.getTargetProductId());
			target.setTarget(targetProduct);
		}

		catch (final DataImportNotFoundException e)
		{
			throw DataImportValidationErrorsException
					.ofNoReferenceException(ProductReferenceModel._TYPECODE, "targetProductId");
		}

		final ProductReferenceTypeEnum referenceType = getReferenceType(source.getType());
		target.setReferenceType(referenceType);
		target.setActive(true);
		target.setPreselected(false);
	}

	private ProductReferenceTypeEnum getReferenceType(final String typeString)
	{
		try
		{
			return enumerationService.getEnumerationValue(ProductReferenceTypeEnum.class, typeString);
		}
		catch (UnknownIdentifierException e) {
			throw DataImportValidationErrorsException.ofNoReferenceException(ProductReferenceModel._TYPECODE,
					NO_REFERENCE_TYPE_ERROR);
		}
	}
}
