package com.wiley.ws.core.product.dataintegration.category.facade;

import com.wiley.ws.core.product.dataintegration.category.dto.CategorySwgDTO;
import com.wiley.ws.core.product.dataintegration.category.dto.UpdateCategorySwgDTO;
import com.wiley.ws.core.product.dataintegration.common.exception.DataImportNotFoundException;
import com.wiley.ws.core.product.dataintegration.common.exception.DataImportValidationConstraintsException;
import com.wiley.ws.core.product.dataintegration.common.exception.DataImportValidationErrorsException;


public interface CategoryImportFacade
{
	/**
	 * Creates new category from incoming DTO
	 *
	 * @param categorySwgDTO
	 * @throws DataImportValidationConstraintsException
	 * @throws DataImportValidationErrorsException
	 */
	void createCategory(CategorySwgDTO categorySwgDTO);

	/**
	 * Updates existed category from incoming DTO
	 *
	 * @param categoryId
	 * @param categorySwgDTO
	 * @throws DataImportValidationConstraintsException
	 * @throws DataImportValidationErrorsException
	 * @throws DataImportNotFoundException
	 */
	void updateCategory(String categoryId, UpdateCategorySwgDTO categorySwgDTO);

	/**
	 * Deletes existed category for specified categoryId
	 *
	 * @param categoryId
	 * @throws DataImportNotFoundException
	 */
	void deleteCategory(String categoryId);
}
