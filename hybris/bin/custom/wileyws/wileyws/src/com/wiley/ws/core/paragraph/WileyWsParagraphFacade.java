package com.wiley.ws.core.paragraph;

import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;
import de.hybris.platform.cms2.model.contents.components.CMSParagraphComponentModel;

import java.util.List;


public interface WileyWsParagraphFacade
{
	List<AbstractCMSComponentModel> getAbstractCMSComponents(String uid);

	String getParagraphContent(CMSParagraphComponentModel paragraphComponent);

	void setFallbackLanguage(Boolean enabled);
}
