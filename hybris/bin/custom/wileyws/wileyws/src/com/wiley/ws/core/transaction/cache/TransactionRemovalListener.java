package com.wiley.ws.core.transaction.cache;

import de.hybris.platform.core.Registry;

import org.apache.log4j.Logger;

import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalNotification;
import com.wiley.ws.core.transaction.WileyWsTestTransaction;


/**
 * Created by Maksim_Kozich on 17.11.2016.
 */
public class TransactionRemovalListener implements RemovalListener<String, WileyWsTestTransaction>
{
	private static final Logger LOG = Logger.getLogger(TransactionRemovalListener.class);

	@Override
	public void onRemoval(final RemovalNotification<String, WileyWsTestTransaction> removal)
	{
		Registry.setCurrentTenant(Registry.getMasterTenant());

		WileyWsTestTransaction transaction = removal.getValue();
		transaction.activateAsCurrentTransaction();
		try
		{
			transaction.rollback(); // tear down properly
			LOG.info("Transaction " + transaction.getObjectID() + " successfully rolled back");
		}
		catch (Exception e)
		{
			LOG.error("Failed to rollback transaction " + transaction.getObjectID() + " inside RemovalListener: " + e);
		}
	}
}