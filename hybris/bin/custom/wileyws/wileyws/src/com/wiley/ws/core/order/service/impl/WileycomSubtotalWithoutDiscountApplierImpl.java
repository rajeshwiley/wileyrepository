package com.wiley.ws.core.order.service.impl;

import de.hybris.platform.core.CoreAlgorithms;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.apache.commons.collections.CollectionUtils;

import com.wiley.ws.core.order.service.WileycomSubtotalWithoutDiscountApplier;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


/**
 * Author Herman_Chukhrai (EPAM)
 */
public class WileycomSubtotalWithoutDiscountApplierImpl implements WileycomSubtotalWithoutDiscountApplier
{

	private static final int ROUND_PRECISION = 2;

	@Override
	public void recalculateAndSetSubtotalWithoutDiscount(@NotNull final AbstractOrderModel orderModel)
	{
		validateParameterNotNullStandardMessage("orderModel", orderModel);

		final List<AbstractOrderEntryModel> entries = orderModel.getEntries();
		if (CollectionUtils.isNotEmpty(entries))
		{
			final double subTotalWithoutDiscount = entries.stream().mapToDouble(e -> e.getBasePrice() * e.getQuantity()).sum();
			orderModel.setSubTotalWithoutDiscount(CoreAlgorithms.round(subTotalWithoutDiscount, ROUND_PRECISION));
		}
	}
}
