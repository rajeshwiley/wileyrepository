package com.wiley.ws.core.product.data.hub.converter;

import de.hybris.platform.europe1.model.PriceRowModel;

import com.wiley.ws.core.product.data.hub.dto.ProductPriceSwgDTO;


/**
 * ProductPriceSwgDTO to PriceRowModel converter
 */
public interface ProductPriceSwgDTOPDHConverter
{
	PriceRowModel convertToPriceRowModel(ProductPriceSwgDTO price, String productCode);
}
