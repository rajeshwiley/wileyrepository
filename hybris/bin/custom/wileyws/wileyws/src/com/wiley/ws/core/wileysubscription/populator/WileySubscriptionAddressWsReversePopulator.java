package com.wiley.ws.core.wileysubscription.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;

import com.wiley.ws.core.wileysubscription.dto.WileySubscriptionAddressWsDTO;

import static com.wiley.facades.populators.PopulateUtils.populateFieldIfNotNull;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


/**
 * Populates {@link AddressModel}AddressModel from {@link WileySubscriptionAddressWsDTO}
 */
public class WileySubscriptionAddressWsReversePopulator implements Populator<WileySubscriptionAddressWsDTO, AddressModel>
{

	@Resource
	private Populator<WileySubscriptionAddressWsDTO, AddressModel> wileycomAddressWsReversePopulator;

	@Override
	public void populate(@NotNull final WileySubscriptionAddressWsDTO source, @NotNull final AddressModel target)
			throws ConversionException
	{
		validateParameterNotNullStandardMessage("source", source);
		validateParameterNotNullStandardMessage("target", target);

		wileycomAddressWsReversePopulator.populate(source, target);

		populateFieldIfNotNull(AddressModel::setFirstname, target, source.getFirstName());
		populateFieldIfNotNull(AddressModel::setLastname, target, source.getLastName());
	}

}
