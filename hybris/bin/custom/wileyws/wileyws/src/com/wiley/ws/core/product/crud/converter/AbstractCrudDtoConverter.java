package com.wiley.ws.core.product.crud.converter;

import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;


public abstract class AbstractCrudDtoConverter<SOURCE, TARGET> implements Converter<SOURCE, TARGET>
{
	@Override
	public TARGET convert(final SOURCE source) throws ConversionException
	{
		return convert(source, null);
	}
}
