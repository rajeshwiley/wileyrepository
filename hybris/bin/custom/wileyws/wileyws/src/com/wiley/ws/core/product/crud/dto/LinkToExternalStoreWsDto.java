package com.wiley.ws.core.product.crud.dto;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonValue;


/**
 * LinkToExternalStore
 */
public class LinkToExternalStoreWsDto
{
	public enum TypeEnum
	{
		GOOGLE_PLAY_STORE("GOOGLE_PLAY_STORE"),
		ITUNES_STORE("ITUNES_STORE"),
		WOL_STORE("WOL_STORE"),
		WILEY_PLUS_STORE("WILEY_PLUS_STORE"),
		REQUEST_QUOTE("REQUEST_QUOTE");

		private String value;

		TypeEnum(final String value)
		{
			this.value = value;
		}

		@JsonValue
		public String getValue()
		{
			return value;
		}

		@Override
		public String toString()
		{
			return new ToStringBuilder(this)
					.append("value", value)
					.toString();
		}
	}


	private TypeEnum type;
	private String url;

	public TypeEnum getType()
	{
		return type;
	}

	public void setType(final TypeEnum type)
	{
		this.type = type;
	}

	public String getUrl()
	{
		return url;
	}

	public void setUrl(final String url)
	{
		this.url = url;
	}

	@Override
	public String toString()
	{
		return new ToStringBuilder(this)
				.append("type", type)
				.append("url", url)
				.toString();
	}
}
