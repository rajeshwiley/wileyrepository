package com.wiley.ws.core.b2b.unit.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.annotation.Nonnull;

import com.wiley.ws.core.address.populators.AbstractWileycomAddressReversePopulator;
import com.wiley.ws.core.b2b.unit.dto.AddressSwgDTO;

import static com.wiley.facades.populators.PopulateUtils.populateFieldIfNotNull;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


public class WileyB2BUnitAddressReversePopulator extends AbstractWileycomAddressReversePopulator
		implements Populator<AddressSwgDTO, AddressModel>
{
	@Override
	public void populate(@Nonnull final AddressSwgDTO source, @Nonnull final AddressModel target) throws ConversionException
	{
		validateParameterNotNull(source.getAddressId(), "External addressId can't be null");

		populateFieldIfNotNull((a, val) -> a.setExternalId(val), target, source.getAddressId());
		populateFieldIfNotNull((a, val) -> a.setPostalcode(val), target, source.getPostcode());
		populateFieldIfNotNull((a, val) -> a.setTown(val), target, source.getCity());
		populateFieldIfNotNull((a, val) -> a.setStreetname(val), target, source.getLine1());
		populateFieldIfNotNull((a, val) -> a.setStreetnumber(val), target, source.getLine2());
		populateFieldIfNotNull((a, val) -> a.setFirstname(val), target, source.getFirstName());
		populateFieldIfNotNull((a, val) -> a.setLastname(val), target, source.getLastName());
		populateFieldIfNotNull((a, val) -> a.setPhone1(val), target, source.getPhoneNumber());

		populateFieldIfNotNull((a, val) -> populateCountry(a, val), target, source.getCountry());
		populateRegion(target, source.getState());
	}
}
