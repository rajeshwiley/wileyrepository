package com.wiley.ws.core.product.crud.converter;

import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.keygenerator.impl.PersistentKeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.wiley.core.enums.WileyAssortment;
import com.wiley.core.model.WileyProductModel;
import com.wiley.ws.core.product.crud.converter.baseproduct.DownloadableContentToMediaContainerConverter;
import com.wiley.ws.core.product.crud.dto.WebLinkDto;
import com.wiley.ws.core.product.crud.dto.WileyProductWsDto;
import com.wiley.ws.core.product.crud.facade.WileyProductCrudCatalogVersionProvider;
import com.wiley.ws.core.product.dataintegration.common.util.LocalizedPopulatorUtil;


public class WileyProductWsDtoConverter extends AbstractCrudDtoConverter<WileyProductWsDto, WileyProductModel>
{
	private final ModelService modelService;

	private final LocalizedPopulatorUtil localizedPopulatorUtil;

	private final Populator<WileyProductWsDto, WileyProductModel> productWsDtoPopulators;

	private final DownloadableContentToMediaContainerConverter downloadableContentToMediaContainerConverter;

	private final PersistentKeyGenerator productCrudTransactionIdGenerator;

	private final WileyProductCrudCatalogVersionProvider wileyProductCrudCatalogVersionProvider;

	private final WebLinkConverter webLinkConverter;

	@Autowired
	public WileyProductWsDtoConverter(final ModelService modelService,
			final LocalizedPopulatorUtil localizedPopulatorUtil,
			@Qualifier("productWsDtoPopulators") final Populator<WileyProductWsDto, WileyProductModel> productWsDtoPopulators,
			final DownloadableContentToMediaContainerConverter downloadableContentToMediaContainerConverter,
			final PersistentKeyGenerator productCrudTransactionIdGenerator,
			final WileyProductCrudCatalogVersionProvider wileyProductCrudCatalogVersionProvider,
			final WebLinkConverter webLinkConverter)
	{
		this.modelService = modelService;
		this.localizedPopulatorUtil = localizedPopulatorUtil;
		this.productWsDtoPopulators = productWsDtoPopulators;
		this.downloadableContentToMediaContainerConverter = downloadableContentToMediaContainerConverter;
		this.productCrudTransactionIdGenerator = productCrudTransactionIdGenerator;
		this.wileyProductCrudCatalogVersionProvider = wileyProductCrudCatalogVersionProvider;
		this.webLinkConverter = webLinkConverter;
	}

	@Override
	public WileyProductModel convert(final WileyProductWsDto productDto, final WileyProductModel wileyProductModel)
			throws ConversionException
	{
		final WileyProductModel convertedProduct = getOrCreateProductModel(wileyProductModel);
		productWsDtoPopulators.populate(productDto, convertedProduct);

		convertedProduct.setBestSeller(productDto.getBestSeller());

		populateExcerpts(productDto, convertedProduct);

		localizedPopulatorUtil.populateLongStrings(productDto.getDownloadsTab(), convertedProduct::setDownloadsTab);

		localizedPopulatorUtil.populateBooleans(productDto.getTextbook(), convertedProduct::setTextbook);

		localizedPopulatorUtil.populateLongStrings(productDto.getAboutAuthors(), convertedProduct::setAboutAuthors);

		localizedPopulatorUtil.populateLongStrings(productDto.getPressRelease(), convertedProduct::setPressRelease);

		localizedPopulatorUtil.populateLongStrings(productDto.getNewToEdition(), convertedProduct::setNewToEdition);

		localizedPopulatorUtil.populateLongStrings(productDto.getWhatsNew(), convertedProduct::setWhatsNew);

		localizedPopulatorUtil.populateLongStrings(productDto.getRelatedWebsites(), convertedProduct::setRelatedWebsites);

		localizedPopulatorUtil.populateLongStrings(productDto.getNotes(), convertedProduct::setNotes);

		localizedPopulatorUtil.populateLongStrings(productDto.getReviews(), convertedProduct::setReviews);

		localizedPopulatorUtil.populateLongStrings(productDto.getErrata(), convertedProduct::setErrata);

		localizedPopulatorUtil.populateLongStrings(productDto.getTableOfContents(), convertedProduct::setTableOfContents);

		populateWebLinks(productDto, convertedProduct);
		populateAssortments(convertedProduct);
		
		return convertedProduct;
	}

	private WileyProductModel getOrCreateProductModel(final WileyProductModel wileyProductModel)
	{

		final WileyProductModel convertedProduct;
		if (wileyProductModel == null)
		{
			final CatalogVersionModel catalogVersion = wileyProductCrudCatalogVersionProvider.getCatalogVersion();
			convertedProduct = modelService.create(WileyProductModel.class);
			convertedProduct.setCode(buildProductCode());
			convertedProduct.setCatalogVersion(catalogVersion);
			convertedProduct.setApprovalStatus(ArticleApprovalStatus.APPROVED);
		}
		else
		{
			convertedProduct = wileyProductModel;
		}
		return convertedProduct;
	}

	private void populateWebLinks(final WileyProductWsDto productDto, final WileyProductModel convertedProduct)
	{
		if (CollectionUtils.isNotEmpty(productDto.getWebLinks()))
		{
			final List<WebLinkDto> webLinkDtos = productDto.getWebLinks()
					.stream()
					.map(WebLinkDto::new)
					.collect(Collectors.toList());
			convertedProduct.setWebLinks(webLinkConverter.convertAll(webLinkDtos));
		}
	}

	private void populateExcerpts(final WileyProductWsDto productDto, final WileyProductModel convertedProduct)
	{
		if (CollectionUtils.isNotEmpty(productDto.getExcerpts()))
		{
			final List<MediaContainerModel> excerpts = productDto.getExcerpts()
					.stream()
					.map(downloadableContentToMediaContainerConverter::convert)
					.collect(Collectors.toList());
			convertedProduct.setExcerpts(excerpts);
		}
	}

	private void populateAssortments(final WileyProductModel product)
	{
		product.setAssortments(Collections.singletonList(WileyAssortment.B2CASSORTMENT));
	}

	private String buildProductCode()
	{
		return productCrudTransactionIdGenerator.generate().toString();
	}
}
