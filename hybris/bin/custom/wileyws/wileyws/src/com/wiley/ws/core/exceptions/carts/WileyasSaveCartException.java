package com.wiley.ws.core.exceptions.carts;

import de.hybris.platform.commerceservices.order.CommerceSaveCartException;

import com.wiley.ws.core.cart.dto.WileyCartWsDTO;


public class WileyasSaveCartException extends CommerceSaveCartException
{
	private WileyCartWsDTO existingCart;

	public WileyasSaveCartException(final String message)
	{
		super(message);
	}

	public WileyasSaveCartException(final String message, final Throwable cause)
	{
		super(message, cause);
	}

	public WileyasSaveCartException(final String message, final WileyCartWsDTO existingCart)
	{
		super(message);
		this.existingCart = existingCart;
	}

	public WileyCartWsDTO getExistingCart()
	{
		return existingCart;
	}

	public void setExistingCart(final WileyCartWsDTO existingCart)
	{
		this.existingCart = existingCart;
	}
}
