package com.wiley.ws.core.transaction.dto;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * A transaction
 *
 * Author Maksim_Kozich (EPAM)
 */
public class WileyWsTestTransactionDTO
{
	/**
	 * Unique address identifier.
	 */
	@NotNull
	private String transactionId;

	@JsonProperty("transactionId")
	public String getTransactionId()
	{
		return transactionId;
	}

	public void setTransactionId(final String transactionId)
	{
		this.transactionId = transactionId;
	}


	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}

		if (o == null || getClass() != o.getClass())
		{
			return false;
		}

		final WileyWsTestTransactionDTO that = (WileyWsTestTransactionDTO) o;

		return new EqualsBuilder()
				.append(transactionId, that.transactionId)
				.isEquals();
	}

	@Override
	public int hashCode()
	{
		return new HashCodeBuilder(17, 37)
				.append(transactionId)
				.toHashCode();
	}

	@Override
	public String toString()
	{
		return new ToStringBuilder(this)
				.append("transactionId", transactionId)
				.toString();
	}
}
