package com.wiley.ws.core.product.data.hub.dto;

import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.Size;

import org.codehaus.jackson.annotate.JsonUnwrapped;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wiley.ws.core.common.dto.CountrySwgDTO;
import com.wiley.ws.core.common.dto.ShortStringSwgDTO;


/**
 * ProductSwgDTO that can by displayed or/and purchased.
 **/
public class ProductSwgDTO
{


	public enum SubtypeEnum
	{
		PAPERBACK,
		HARDCOVER,
		LOOSE_LEAF,
		EBOOK,
		ETEXT,
		OBOOK,
		EMRW,
		JOURNAL_BACKFILE,
		COURSE,
		SUBSCRIPTION,
		OLR_SUBSCIPTION,
		REGISTRATION_CODE,
		PRODUCT_SET
	}

	private SubtypeEnum subtype = null;

	public enum EditionFormatEnum
	{
		DIGITAL, PHYSICAL, DIGITAL_AND_PHYSICAL
	}

	public enum DigitalContentTypeEnum
	{
		WILEY_PLUS, VITAL_SOURCE, LIGHTNING_SOURCE, WILEY_DOWNLOAD
	}

	private String code = null;
	private Boolean purchasable = null;
	private Boolean countable = null;
	private EditionFormatEnum editionFormat = null;
	private List<LocalizedStringSwgDTO> name;
	private List<LocalizedLongStringSwgDTO> description;
	private List<LocalizedLongStringSwgDTO> summary;
	private MediaSwgDTO picture = null;
	private Date onlineDate = null;
	private Date offlineDate = null;
	private List<ShortStringSwgDTO> categories;

	private List<ProductReferenceSwgDTO> productReferences;
	private String authors = null;
	private String isbn = null;
	private String printIssn = null;
	private String onlineIssn = null;
	private Date dateImprint = null;
	private Date releaseDate = null;
	private List<ShortStringSwgDTO> subscriptionTerms = null;
	private List<CountrySwgDTO> countries;
	private List<ShortStringSwgDTO> assortments;
	private List<ProductPriceSwgDTO> prices;
	private Boolean readyForPublication = null;
	private Boolean printOnDemand = null;
	private String defaultWarehouse = null;
	private Boolean textbook = null;
	private String taxCategory = null;
	private String taxTransactionType = null;
	private String externalCompany = null;
	private String pdmProductCode = null;
	private String sapProductCode = null;
	private DigitalContentTypeEnum digitalContentType = null;

	/**
	 * ProductSwgDTO code
	 **/
	@Size(max = 255)
	@JsonProperty("code")
	public String getCode()
	{
		return code;
	}

	public void setCode(final String code)
	{
		this.code = code;
	}

	/**
	 * The flag defines whether the product can be added to the cart.
	 * For example, a product set component can be restricted from purchase.
	 * The attribute is mandatory for products. If no value will be passed along, <i>true</i> will be set by default.
	 **/
	@JsonProperty("purchasable")
	public Boolean getPurchasable()
	{
		return purchasable;
	}

	public void setPurchasable(final Boolean purchasable)
	{
		this.purchasable = purchasable;
	}

	/**
	 * The flag defines whether the product can be added to cart more than once.
	 * Non-countable products do not have the change quantity option on the cart page.
	 * For example, some digital eBooks are not countable products.
	 * The attribute is mandatory for products. If no value will be passed along, <i>true</i> will be set by default.
	 **/
	@JsonProperty("countable")
	public Boolean getCountable()
	{
		return countable;
	}

	public void setCountable(final Boolean countable)
	{
		this.countable = countable;
	}

	/**
	 * Enumeration value that specifies a product subtype.
	 * It defines essential product qualities, product representation and checkout flow specifics.
	 * The attribute is mandatory for products. If no value will be passed along, <i>BOOK</i> will be set by default.
	 **/
	@JsonProperty("subtype")
	public SubtypeEnum getSubtype()
	{
		return subtype;
	}

	public void setSubtype(final SubtypeEnum subtype)
	{
		this.subtype = subtype;
	}

	/**
	 * Enumeration value that specifies a product format. Digital product cannot have any associated shipping costs.
	 * The attribute is mandatory for products. If no value will be passed along, <i>PHYSICAL</i> will be set by default.
	 **/
	@JsonProperty("editionFormat")
	public EditionFormatEnum getEditionFormat()
	{
		return editionFormat;
	}

	public void setEditionFormat(final EditionFormatEnum editionFormat)
	{
		this.editionFormat = editionFormat;
	}

	/**
	 * The title (display name) of the product.
	 **/
	@Valid
	@JsonProperty("name")
	public List<LocalizedStringSwgDTO> getName()
	{
		return name;
	}

	public void setName(final List<LocalizedStringSwgDTO> name)
	{
		this.name = name;
	}

	/**
	 * The detailed product description with HTML tags.
	 **/
	@Valid
	@JsonProperty("description")
	public List<LocalizedLongStringSwgDTO> getDescription()
	{
		return description;
	}

	public void setDescription(final List<LocalizedLongStringSwgDTO> description)
	{
		this.description = description;
	}

	/**
	 * The short description of the product.
	 **/
	@Valid
	@JsonProperty("summary")
	public List<LocalizedLongStringSwgDTO> getSummary()
	{
		return summary;
	}

	public void setSummary(final List<LocalizedLongStringSwgDTO> summary)
	{
		this.summary = summary;
	}

	/**
	 **/
	@Valid
	@JsonProperty("picture")
	public MediaSwgDTO getPicture()
	{
		return picture;
	}

	public void setPicture(final MediaSwgDTO picture)
	{
		this.picture = picture;
	}

	/**
	 * The value is used in visibility rules.
	 * If the online date of a product is after the current time, the product won't be available on the storefront.
	 **/
	@JsonProperty("onlineDate")
	public Date getOnlineDate()
	{
		return onlineDate;
	}

	public void setOnlineDate(final Date onlineDate)
	{
		this.onlineDate = onlineDate;
	}

	/**
	 * The value is used in visibility rules.
	 * If the offline date of a product is before the current time, the product won't be available on the storefront.
	 **/
	@JsonProperty("offlineDate")
	public Date getOfflineDate()
	{
		return offlineDate;
	}

	public void setOfflineDate(final Date offlineDate)
	{
		this.offlineDate = offlineDate;
	}

	/**
	 * The list of categories' codes that contain the product.
	 * Categories form the product taxonomy.
	 * Categories are used to group products and display products using the site navigation links.
	 * The categories with the specified codes must exist in Hybris, or the request fails.
	 **/
	@Valid
	@JsonUnwrapped
	@JsonProperty("categories")
	public List<ShortStringSwgDTO> getCategories()
	{
		return categories;
	}

	public void setCategories(final List<ShortStringSwgDTO> categories)
	{
		this.categories = categories;
	}

	/**
	 * Outbound references from the product.
	 * The references are used to specify product options, product sets and other relations between the products.
	 **/
	@Valid
	@JsonUnwrapped
	@JsonProperty("productReferences")
	public List<ProductReferenceSwgDTO> getProductReferences()
	{
		return productReferences;
	}

	public void setProductReferences(final List<ProductReferenceSwgDTO> productReferences)
	{
		this.productReferences = productReferences;
	}

	/**
	 * Authors of the product. Authors are displayed on the product details page, cart page, etc.
	 **/
	@Size(max = 255)
	@JsonProperty("authors")
	public String getAuthors()
	{
		return authors;
	}

	public void setAuthors(final String authors)
	{
		this.authors = authors;
	}

	/**
	 * The International Standard Book Number (ISBN).
	 * ISBN is displayed on the products details page and indexed for the product search purposes.
	 **/
	@Size(max = 255)
	@JsonProperty("isbn")
	public String getIsbn()
	{
		return isbn;
	}

	public void setIsbn(final String isbn)
	{
		this.isbn = isbn;
	}


	/**
	 * The International Standard Serial Number (ISSN) for the print version of the product.
	 * The print ISSN is displayed on the products details page and indexed for the product search purposes.
	 **/
	@Size(max = 255)
	@JsonProperty("printIssn")
	public String getPrintIssn()
	{
		return printIssn;
	}

	public void setPrintIssn(final String printIssn)
	{
		this.printIssn = printIssn;
	}

	/**
	 * The International Standard Serial Number  (ISSN) for the online version of the product.
	 * The online ISSN is displayed on the products details page and indexed for the product search purposes.
	 **/
	@Size(max = 255)
	@JsonProperty("onlineIssn")
	public String getOnlineIssn()
	{
		return onlineIssn;
	}

	public void setOnlineIssn(final String onlineIssn)
	{
		this.onlineIssn = onlineIssn;
	}

	/**
	 * The online release date of a product.
	 **/
	@JsonProperty("releaseDate")
	public Date getReleaseDate()
	{
		return releaseDate;
	}

	public void setReleaseDate(final Date releaseDate)
	{
		this.releaseDate = releaseDate;
	}


	/**
	 * The date when the product was published. The date is displayed on the product details page, cart page, etc.
	 **/
	@JsonProperty("dateImprint")
	public Date getDateImprint()
	{
		return dateImprint;
	}

	public void setDateImprint(final Date dateImprint)
	{
		this.dateImprint = dateImprint;
	}

	/**
	 * The codes of the available subscription terms for the subscription product.
	 * The terms should be considered as purchase options for the product,
	 * a customer has to choose one and only one of these terms.
	 * Each subscription term defines the billing plan and must be preconfigured on the Hybris side
	 **/
	@Valid
	@JsonUnwrapped
	@JsonProperty("subscriptionTerms")
	public List<ShortStringSwgDTO> getSubscriptionTerms()
	{
		return subscriptionTerms;
	}

	public void setSubscriptionTerms(final List<ShortStringSwgDTO> subscriptionTerms)
	{
		this.subscriptionTerms = subscriptionTerms;
	}

	/**
	 * The list of countries where the product is visible and available to customers.
	 **/
	@Valid
	@JsonUnwrapped
	@JsonProperty("countries")
	public List<CountrySwgDTO> getCountries()
	{
		return countries;
	}

	public void setCountries(final List<CountrySwgDTO> countries)
	{
		this.countries = countries;
	}

	/**
	 * The list of codes of assortments that contain the product.
	 * A base store has a single assortment, while the product can belong to multiple assortments.
	 * The assortments with the specified codes must exist in Hybris, or the request fails.
	 **/
	@Valid
	@JsonUnwrapped
	@JsonProperty("assortments")
	public List<ShortStringSwgDTO> getAssortments()
	{
		return assortments;
	}

	public void setAssortments(final List<ShortStringSwgDTO> assortments)
	{
		this.assortments = assortments;
	}

	/**
	 * Prices for the product in different currencies and countries.
	 * The full set of prices is expected to be passed along,
	 * thus the received prices will completely replace any prices for the product that are already stored in Hybris.
	 **/
	@Valid
	@JsonProperty("prices")
	public List<ProductPriceSwgDTO> getPrices()
	{
		return prices;
	}

	public void setPrices(final List<ProductPriceSwgDTO> prices)
	{
		this.prices = prices;
	}

	/**
	 * The flag defines whether the product is ready for publication and should by copied from Staged to Online catalog version.
	 * Once the product is copied to the Online catalog version all the product modifications
	 * that might be done in several web service calls will be visible and available to customers.
	 **/
	@JsonProperty("readyForPublication")
	public Boolean getReadyForPublication()
	{
		return readyForPublication;
	}

	public void setReadyForPublication(final Boolean readyForPublication)
	{
		this.readyForPublication = readyForPublication;
	}

	/**
	 * The flag defines whether the product is a print on demand product.
	 **/
	@JsonProperty("printOnDemand")
	public Boolean getPrintOnDemand()
	{
		return printOnDemand;
	}

	public void setPrintOnDemand(final Boolean printOnDemand)
	{
		this.printOnDemand = printOnDemand;
	}

	/**
	 * The flag defines whether the product is a print on demand product.
	 **/
	@Size(max = 255)
	@JsonProperty("defaultWarehouse")
	public String getDefaultWarehouse()
	{
		return defaultWarehouse;
	}

	public void setDefaultWarehouse(final String defaultWarehouse)
	{
		this.defaultWarehouse = defaultWarehouse;
	}

	/**
	 * The flag defines whether the product is a textbook and a the school should be requested on checkout.
	 * The attribute is mandatory for products. If no value will be passed along, <i>false</i> will be set by default.
	 **/
	@JsonProperty("textbook")
	public Boolean getTextbook()
	{
		return textbook;
	}

	public void setTextbook(final Boolean textbook)
	{
		this.textbook = textbook;
	}

	/**
	 * A code that is used by external ERP systems for this product line. Is used for the tax calculation.
	 **/
	@Size(max = 255)
	@JsonProperty("taxCategory")
	public String getTaxCategory()
	{
		return taxCategory;
	}

	public void setTaxCategory(final String taxCategory)
	{
		this.taxCategory = taxCategory;
	}

	/**
	 * A type of the product that characterizes the transaction for the tax calculation (for example, goods, services, etc.)
	 **/
	@Size(max = 255)
	@JsonProperty("taxTransactionType")
	public String getTaxTransactionType()
	{
		return taxTransactionType;
	}

	public void setTaxTransactionType(final String taxTransactionType)
	{
		this.taxTransactionType = taxTransactionType;
	}

	/**
	 * A unique code that is used to reference the company that owns the product
	 **/
	@Size(max = 255)
	@JsonProperty("externalCompany")
	public String getExternalCompany()
	{
		return externalCompany;
	}

	public void setExternalCompany(final String externalCompany)
	{
		this.externalCompany = externalCompany;
	}

	/**
	 * PDM product code that is used for integration with Sabrix.
	 **/
	@Size(max = 255)
	@JsonProperty("pdmProductCode")
	public String getPdmProductCode()
	{
		return pdmProductCode;
	}

	public void setPdmProductCode(final String pdmProductCode)
	{
		this.pdmProductCode = pdmProductCode;
	}

	/**
	 * Unique identifier of the product in the SAP ERP system
	 **/
	@Size(max = 255)
	@JsonProperty("sapProductCode")
	public String getSapProductCode()
	{
		return sapProductCode;
	}

	public void setSapProductCode(final String sapProductCode)
	{
		this.sapProductCode = sapProductCode;
	}

	/**
	 * Defines, which content system provides access to the digital product. Only for digital products.
	 */
	@JsonProperty("digitalContentType")
	public DigitalContentTypeEnum getDigitalContentType()
	{
		return digitalContentType;
	}

	public void setDigitalContentType(final DigitalContentTypeEnum digitalContentType)
	{
		this.digitalContentType = digitalContentType;
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		ProductSwgDTO product = (ProductSwgDTO) o;
		return Objects.equals(code, product.code)
				&& Objects.equals(purchasable, product.purchasable)
				&& Objects.equals(countable, product.countable)
				&& Objects.equals(subtype, product.subtype)
				&& Objects.equals(editionFormat, product.editionFormat)
				&& Objects.equals(name, product.name)
				&& Objects.equals(description, product.description)
				&& Objects.equals(summary, product.summary)
				&& Objects.equals(picture, product.picture)
				&& Objects.equals(onlineDate, product.onlineDate)
				&& Objects.equals(offlineDate, product.offlineDate)
				&& Objects.equals(categories, product.categories)
				&& Objects.equals(productReferences, product.productReferences)
				&& Objects.equals(authors, product.authors)
				&& Objects.equals(isbn, product.isbn)
				&& Objects.equals(printIssn, product.printIssn)
				&& Objects.equals(onlineIssn, product.onlineIssn)
				&& Objects.equals(dateImprint, product.dateImprint)
				&& Objects.equals(releaseDate, product.releaseDate)
				&& Objects.equals(subscriptionTerms, product.subscriptionTerms)
				&& Objects.equals(countries, product.countries)
				&& Objects.equals(assortments, product.assortments)
				&& Objects.equals(prices, product.prices)
				&& Objects.equals(printOnDemand, product.printOnDemand)
				&& Objects.equals(defaultWarehouse, product.defaultWarehouse)
				&& Objects.equals(textbook, product.textbook)
				&& Objects.equals(taxCategory, product.taxCategory)
				&& Objects.equals(taxTransactionType, product.taxTransactionType)
				&& Objects.equals(externalCompany, product.externalCompany)
				&& Objects.equals(pdmProductCode, product.pdmProductCode)
				&& Objects.equals(sapProductCode, product.sapProductCode)
				&& Objects.equals(digitalContentType, product.digitalContentType);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(code, purchasable, countable, subtype, editionFormat, name, description, summary, picture,
				onlineDate, offlineDate, categories, productReferences, authors, isbn, printIssn, onlineIssn, dateImprint,
				releaseDate, subscriptionTerms, countries, assortments, prices, readyForPublication, printOnDemand,
				defaultWarehouse, textbook, taxCategory, taxTransactionType, externalCompany, pdmProductCode, sapProductCode,
				digitalContentType);
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("class ProductSwgDTO {\n");

		sb.append("  code: ").append(code).append("\n");
		sb.append("  purchasable: ").append(purchasable).append("\n");
		sb.append("  countable: ").append(countable).append("\n");
		sb.append("  subtype: ").append(subtype).append("\n");
		sb.append("  editionFormat: ").append(editionFormat).append("\n");
		sb.append("  name: ").append(name).append("\n");
		sb.append("  description: ").append(description).append("\n");
		sb.append("  summary: ").append(summary).append("\n");
		sb.append("  picture: ").append(picture).append("\n");
		sb.append("  onlineDate: ").append(onlineDate).append("\n");
		sb.append("  offlineDate: ").append(offlineDate).append("\n");
		sb.append("  categories: ").append(categories).append("\n");
		sb.append("  productReferences: ").append(productReferences).append("\n");
		sb.append("  authors: ").append(authors).append("\n");
		sb.append("  isbn: ").append(isbn).append("\n");
		sb.append("  printIssn: ").append(printIssn).append("\n");
		sb.append("  onlineIssn: ").append(onlineIssn).append("\n");
		sb.append("  dateImprint: ").append(dateImprint).append("\n");
		sb.append("  releaseDate: ").append(releaseDate).append("\n");
		sb.append("  subscriptionTerms: ").append(subscriptionTerms).append("\n");
		sb.append("  countries: ").append(countries).append("\n");
		sb.append("  assortments: ").append(assortments).append("\n");
		sb.append("  prices: ").append(prices).append("\n");
		sb.append("  readyForPublication: ").append(readyForPublication).append("\n");
		sb.append("  printOnDemand: ").append(printOnDemand).append("\n");
		sb.append("  defaultWarehouse: ").append(defaultWarehouse).append("\n");
		sb.append("  textbook: ").append(textbook).append("\n");
		sb.append("  taxCategory: ").append(taxCategory).append("\n");
		sb.append("  taxTransactionType: ").append(taxTransactionType).append("\n");
		sb.append("  externalCompany: ").append(externalCompany).append("\n");
		sb.append("  pdmProductCode: ").append(pdmProductCode).append("\n");
		sb.append("  sapProductCode: ").append(sapProductCode).append("\n");
		sb.append("  digitalContentType: ").append(digitalContentType).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
