package com.wiley.ws.core.wileysubscription.impl;

import de.hybris.platform.converters.Converters;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

import com.wiley.core.enums.PaymentModeEnum;
import com.wiley.core.model.WileySubscriptionModel;
import com.wiley.core.model.WileySubscriptionPaymentTransactionModel;
import com.wiley.core.subscription.services.WileySubscriptionService;
import com.wiley.facades.wileyws.subscriptions.dto.SubscriptionSearchResult;
import com.wiley.ws.core.wileysubscription.Wileyb2cWsSubscriptionFacade;
import com.wiley.ws.core.wileysubscription.converters.populator.Wileyb2cWsPaymentTransactionPopulator;
import com.wiley.ws.core.wileysubscription.dto.PaymentInfoUpdateRequestWsDTO;
import com.wiley.ws.core.wileysubscription.dto.PaymentTransactionWsDTO;
import com.wiley.ws.core.wileysubscription.dto.SubscriptionCreateRequestWsDTO;
import com.wiley.ws.core.wileysubscription.dto.SubscriptionCreateResponseWsDTO;
import com.wiley.ws.core.wileysubscription.dto.SubscriptionUpdateRequestWsDTO;
import com.wiley.ws.core.wileysubscription.exception.NoSuchSubscriptionException;
import com.wiley.ws.core.wileysubscription.populator.PaymentInfoCreatingStrategy;

import static com.wiley.facades.populators.PopulateUtils.populateFieldIfNotNull;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


/**
 * Default implementation for {@link Wileyb2cWsSubscriptionFacade}.
 */
public class Wileyb2cWsSubscriptionFacadeImpl implements Wileyb2cWsSubscriptionFacade
{
	@Resource
	private ModelService modelService;

	@Resource
	private WileySubscriptionService wileySubscriptionService;

	@Resource
	private Populator<SubscriptionUpdateRequestWsDTO, WileySubscriptionModel> wileyUpdateSubscriptionWsReversePopulator;

	@Resource
	private Populator<SubscriptionCreateRequestWsDTO, WileySubscriptionModel> wileyCreateSubscriptionWsReversePopulator;

	@Resource
	private Converter<WileySubscriptionModel, SubscriptionSearchResult> subscriptionSearchResultConverter;

	@Resource
	private Converter<WileySubscriptionModel, SubscriptionCreateResponseWsDTO> wileySubscriptionCreateResponseWsConverter;

	private Map<PaymentModeEnum, PaymentInfoCreatingStrategy<PaymentInfoModel>> paymentMode2StrategyMap;

	private PaymentModeEnum defaultPaymentMode;

	@Resource
	private Wileyb2cWsPaymentTransactionPopulator wileyb2cWsPaymentTransactionPopulator;

	@Override
	public boolean doesSubscriptionExist(@Nonnull final String subscriptionCode)
	{
		validateParameterNotNullStandardMessage("subscriptionCode", subscriptionCode);

		boolean result = false;
		try
		{
			result = wileySubscriptionService.getSubscriptionByCode(subscriptionCode) != null;
		}
		catch (UnknownIdentifierException e)
		{
			// do nothing.
		}
		return result;
	}

	@Override
	@Nonnull
	public SubscriptionCreateResponseWsDTO createSubscription(@Nonnull final SubscriptionCreateRequestWsDTO subscriptionDto)
	{
		validateParameterNotNullStandardMessage("subscriptionDto", subscriptionDto);

		final WileySubscriptionModel newSubscription = modelService.create(WileySubscriptionModel.class);

		wileyCreateSubscriptionWsReversePopulator.populate(subscriptionDto, newSubscription);
		populateFieldIfNotNull(this::populatePaymentInfo, newSubscription, subscriptionDto.getPaymentInfo());

		modelService.saveAll(getModelsToSave(newSubscription));

		return wileySubscriptionCreateResponseWsConverter.convert(newSubscription);
	}

	/*Transaction is required because PaymentInfoCreatingStrategy.getOrCreateNewSupportedPaymentInfo() removes old
	 payment info.*/
	@Override
	@Transactional
	public void updateSubscription(@Nonnull final String subscriptionCode,
			@Nonnull final SubscriptionUpdateRequestWsDTO subscriptionDto)
	{
		validateParameterNotNullStandardMessage("subscriptionCode", subscriptionCode);
		validateParameterNotNullStandardMessage("subscriptionDto", subscriptionDto);

		final WileySubscriptionModel subscription = wileySubscriptionService.getSubscriptionByCode(subscriptionCode);

		wileyUpdateSubscriptionWsReversePopulator.populate(subscriptionDto, subscription);
		populateFieldIfNotNull(this::populatePaymentInfo, subscription, subscriptionDto.getPaymentInfo());

		modelService.saveAll(getModelsToSave(subscription));
	}

	@Override
	public void createPaymentTransactions(@Nonnull final String id,
			@Nonnull final PaymentTransactionWsDTO paymentTransactionWsDTO)
	{
		final WileySubscriptionPaymentTransactionModel paymentTransaction = modelService.create(
				WileySubscriptionPaymentTransactionModel.class);

		paymentTransaction.setCode(UUID.randomUUID().toString());
		populateWileySubscription(paymentTransaction, id);

		wileyb2cWsPaymentTransactionPopulator.populate(paymentTransactionWsDTO, paymentTransaction);

		modelService.save(paymentTransaction);
	}

	@Override
	public List<SubscriptionSearchResult> getInternalCodesByExternalCode(@Nonnull final String externalCode)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("externalCode", externalCode);
		final List<WileySubscriptionModel> subscriptions = wileySubscriptionService.getInternalCodesByExternalCode(externalCode);
		return Converters.convertAll(subscriptions, subscriptionSearchResultConverter);
	}

	private void populateWileySubscription(final WileySubscriptionPaymentTransactionModel paymentTransaction,
			final String subscriptionCode)
	{
		try
		{
			final WileySubscriptionModel wileySubscriptionModel =
					wileySubscriptionService.getSubscriptionByCode(subscriptionCode);
			paymentTransaction.setSubscription(wileySubscriptionModel);
		}
		catch (UnknownIdentifierException e)
		{
			throw new NoSuchSubscriptionException(e.getMessage(), e);
		}
	}

	private void populatePaymentInfo(final WileySubscriptionModel subscriptionModel,
			final PaymentInfoUpdateRequestWsDTO paymentInfoDto)
	{
		PaymentInfoModel paymentInfo = subscriptionModel.getPaymentInfo();

		PaymentInfoCreatingStrategy<PaymentInfoModel> paymentInfoCreatingStrategy =
				getPaymentInfoCreationStrategy(paymentInfo, paymentInfoDto.getPaymentType());

		paymentInfo = paymentInfoCreatingStrategy.getOrCreateNewSupportedPaymentInfo(paymentInfo, subscriptionModel);
		paymentInfoCreatingStrategy.populate(paymentInfoDto, paymentInfo);
	}

	@Nonnull
	private PaymentInfoCreatingStrategy<PaymentInfoModel> getPaymentInfoCreationStrategy(
			final PaymentInfoModel paymentInfo, final PaymentInfoUpdateRequestWsDTO.PaymentType requestedPaymentType)
	{
		PaymentInfoCreatingStrategy<PaymentInfoModel> paymentInfoCreatingStrategy;

		if (paymentInfo != null && requestedPaymentType == null)
		{
			paymentInfoCreatingStrategy = paymentMode2StrategyMap.values().stream()
					.filter(strategy -> strategy.isSupported(paymentInfo))
					.findFirst()
					.orElse(null);
		}
		else if (requestedPaymentType != null)
		{
			paymentInfoCreatingStrategy = paymentMode2StrategyMap.get(requestedPaymentType.getValue());
		}
		else
		{
			paymentInfoCreatingStrategy = paymentMode2StrategyMap.get(defaultPaymentMode);
		}

		if (paymentInfoCreatingStrategy == null) {
			throw new IllegalStateException(String
					.format("Could not find PaymentInfoCreatingStrategy for paymentInfo [%s] and requestedPaymentType [%s]",
							paymentInfo == null ? null : paymentInfo.getCode(), requestedPaymentType));
		}


		return paymentInfoCreatingStrategy;
	}

	private List<Object> getModelsToSave(final WileySubscriptionModel subscription)
	{
		List<Object> modelsToSave = new ArrayList<>(3);

		addToListIfRequireSaving(modelsToSave, subscription);

		PaymentInfoModel paymentInfo = subscription.getPaymentInfo();
		if (paymentInfo != null)
		{
			addToListIfRequireSaving(modelsToSave, paymentInfo);
			AddressModel billingAddress = paymentInfo.getBillingAddress();
			if (billingAddress != null)
			{
				addToListIfRequireSaving(modelsToSave, billingAddress);
			}
		}
		final AddressModel deliveryAddress = subscription.getDeliveryAddress();
		if (deliveryAddress != null)
		{
			addToListIfRequireSaving(modelsToSave, deliveryAddress);
		}
		return modelsToSave;
	}

	private <T> void addToListIfRequireSaving(final List<T> list, final T object)
	{
		if (modelService.isModified(object))
		{
			list.add(object);
		}
	}

	@Required
	public void setPaymentMode2StrategyMap(
			final Map<PaymentModeEnum, PaymentInfoCreatingStrategy<PaymentInfoModel>> paymentMode2StrategyMap)
	{
		this.paymentMode2StrategyMap = paymentMode2StrategyMap;
	}

	@Required
	public void setDefaultPaymentMode(final PaymentModeEnum defaultPaymentMode)
	{
		this.defaultPaymentMode = defaultPaymentMode;
	}

}
