package com.wiley.ws.core.product.crud.converter.populator.variantproduct;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import com.wiley.core.enums.WileyProductLifecycleEnum;
import com.wiley.core.model.WileyPurchaseOptionProductModel;
import com.wiley.ws.core.product.crud.dto.LocalizedLifecycleStatusWsDto;
import com.wiley.ws.core.product.crud.dto.WileyVariantProductWsDto;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;
import static org.apache.commons.lang.LocaleUtils.toLocale;


public class LifeCycleStatusPopulator
		implements Populator<WileyVariantProductWsDto, WileyPurchaseOptionProductModel>
{
	@Override
	public void populate(final WileyVariantProductWsDto variantProductWsDto,
			final WileyPurchaseOptionProductModel variantProductModel) throws ConversionException
	{
		validateParameterNotNull(variantProductWsDto, "VariantProductDto can't be null");
		validateParameterNotNull(variantProductModel, "VariantProductModel can't be null");

		if (variantProductWsDto.getLifecycleStatus() != null)
		{
			variantProductWsDto.getLifecycleStatus()
					.stream()
					.forEach(lifecycleStatus -> populateLifeCycleStatus(variantProductModel, lifecycleStatus));
		}
	}

	private void populateLifeCycleStatus(final WileyPurchaseOptionProductModel variantProductModel,
			final LocalizedLifecycleStatusWsDto lifecycleStatus)
	{
		variantProductModel.setLifecycleStatus(WileyProductLifecycleEnum.valueOf(lifecycleStatus.getStatus().toString()),
				toLocale(lifecycleStatus.getLocale()));
	}
}
