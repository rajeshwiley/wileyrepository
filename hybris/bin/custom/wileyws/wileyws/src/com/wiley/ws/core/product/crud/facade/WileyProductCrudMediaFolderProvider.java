package com.wiley.ws.core.product.crud.facade;

import de.hybris.platform.core.model.media.MediaFolderModel;
import de.hybris.platform.servicelayer.media.MediaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;


public class WileyProductCrudMediaFolderProvider
{
	private String mediaFolderName;
	private final MediaService mediaService;

	@Autowired
	public WileyProductCrudMediaFolderProvider(final MediaService mediaService)
	{
		this.mediaService = mediaService;
	}

	public MediaFolderModel getMediaFolder()
	{
		return mediaService.getFolder(mediaFolderName);
	}

	@Required
	public void setMediaFolderName(final String mediaFolderName)
	{
		this.mediaFolderName = mediaFolderName;
	}
}
