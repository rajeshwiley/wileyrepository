package com.wiley.ws.core.order.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.validation.constraints.NotNull;

import com.wiley.ws.core.order.dto.OrderAddressWsDTO;

import static com.wiley.facades.populators.PopulateUtils.populateFieldIfNotNull;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


/**
 * Populates external id for {OrderAddressWsDTO}
 */
public class WileycomOrderAddressExternalIdReversePopulator implements Populator<OrderAddressWsDTO, AddressModel>
{

	@Override
	public void populate(@NotNull final OrderAddressWsDTO source, @NotNull final AddressModel target) throws ConversionException
	{
		validateParameterNotNullStandardMessage("source", source);
		validateParameterNotNullStandardMessage("target", target);

		populateFieldIfNotNull(AddressModel::setExternalId, target, source.getAddressId());
	}

}
