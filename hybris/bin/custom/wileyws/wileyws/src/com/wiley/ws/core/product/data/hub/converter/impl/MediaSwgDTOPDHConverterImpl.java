package com.wiley.ws.core.product.data.hub.converter.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.model.ModelService;

import com.wiley.ws.core.product.data.hub.converter.MediaSwgDTOPDHConverter;
import com.wiley.ws.core.product.data.hub.dto.MediaSwgDTO;

import org.springframework.beans.factory.annotation.Autowired;


/**
 * Converter implementation
 */
public class MediaSwgDTOPDHConverterImpl implements MediaSwgDTOPDHConverter
{
	@Autowired
	private ModelService modelService;

	public MediaModel convertToMediaModel(final MediaSwgDTO media, final String code,
			final CatalogVersionModel catalogVersion)
	{
		MediaModel mediaModel = modelService.create(MediaModel.class);
		mediaModel.setCode(code);
		mediaModel.setCatalogVersion(catalogVersion);
		mediaModel.setInternalURL(media.getUrl());
		mediaModel.setMime(media.getMime());
		mediaModel.setAltText(media.getAltText());
		return mediaModel;
	}
}
