package com.wiley.ws.core.wileysubscription.dto;

import java.util.Date;
import java.util.Objects;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * The subscription to create.
 */
public class SubscriptionCreateRequestWsDTO extends AbstractSubscriptionRequestWsDTO
{

	private String isbn;

	private String orderId;

	@NotNull
	@Size(max = 255)
	@JsonProperty("isbn")
	public String getIsbn()
	{
		return isbn;
	}

	public void setIsbn(final String isbn)
	{
		this.isbn = isbn;
	}

	@NotNull
	@Size(max = 255)
	@JsonProperty("orderId")
	public String getOrderId()
	{
		return orderId;
	}

	public void setOrderId(final String orderId)
	{
		this.orderId = orderId;
	}

	@NotNull
	@Override
	public String getExternalCode()
	{
		return super.getExternalCode();
	}

	@NotNull
	@Override
	public SubscriptionStatus getSubscriptionStatus()
	{
		return super.getSubscriptionStatus();
	}

	@NotNull
	@Override
	public Date getSubscriptionStartDate()
	{
		return super.getSubscriptionStartDate();
	}

	@NotNull
	@Override
	public Date getSubscriptionEndDate()
	{
		return super.getSubscriptionEndDate();
	}

	@NotNull
	@Override
	public Integer getContractDurationValue()
	{
		return super.getContractDurationValue();
	}

	@NotNull
	@Override
	public ContractDurationUnit getContractDurationUnit()
	{
		return super.getContractDurationUnit();
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (!(o instanceof SubscriptionCreateRequestWsDTO))
		{
			return false;
		}
		if (!super.equals(o))
		{
			return false;
		}
		final SubscriptionCreateRequestWsDTO that = (SubscriptionCreateRequestWsDTO) o;
		return Objects.equals(isbn, that.isbn)
				&& Objects.equals(orderId, that.orderId);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(super.hashCode(), isbn, orderId);
	}
}
