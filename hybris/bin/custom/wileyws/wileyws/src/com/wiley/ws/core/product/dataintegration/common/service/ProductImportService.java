package com.wiley.ws.core.product.dataintegration.common.service;

import de.hybris.platform.core.model.product.ProductModel;

import com.wiley.core.model.WileyPurchaseOptionProductModel;


public interface ProductImportService
{
	<T extends ProductModel> T getProductForCode(String productCode);

	void checkVariantBelongsToBaseProduct(WileyPurchaseOptionProductModel productOption,
			ProductModel productModel);
}
