package com.wiley.ws.core.product.dataintegration.price.dto;

public class PriceSwgDTO
{

	private String id;

	private String productId;

	private Double price;

	private String currency;

	private String country;

	private Integer minQuantity = new Integer(1);

	private Long sequenceId;

	public String getId()
	{
		return id;
	}

	public void setId(final String id)
	{
		this.id = id;
	}

	public String getProductId()
	{
		return productId;
	}

	public void setProductId(final String productId)
	{
		this.productId = productId;
	}

	public Double getPrice()
	{
		return price;
	}

	public void setPrice(final Double price)
	{
		this.price = price;
	}

	public String getCurrency()
	{
		return currency;
	}

	public void setCurrency(final String currency)
	{
		this.currency = currency;
	}

	public String getCountry()
	{
		return country;
	}

	public void setCountry(final String country)
	{
		this.country = country;
	}

	public Integer getMinQuantity()
	{
		return minQuantity;
	}

	public void setMinQuantity(final Integer minQuantity)
	{
		this.minQuantity = minQuantity;
	}

	public Long getSequenceId()
	{
		return sequenceId;
	}

	public void setSequenceId(final Long sequenceId)
	{
		this.sequenceId = sequenceId;
	}

	@Override
	public String toString()
	{
		final StringBuilder builder = new StringBuilder();
		builder.append("PriceSwgDTO [id=");
		builder.append(id);
		builder.append(", productId=");
		builder.append(productId);
		builder.append(", price=");
		builder.append(price);
		builder.append(", currency=");
		builder.append(currency);
		builder.append(", country=");
		builder.append(country);
		builder.append(", minQuantity=");
		builder.append(minQuantity);
		builder.append(", sequenceId=");
		builder.append(sequenceId);
		builder.append("]");
		return builder.toString();
	}

}
