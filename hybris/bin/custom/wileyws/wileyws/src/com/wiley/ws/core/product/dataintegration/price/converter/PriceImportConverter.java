
package com.wiley.ws.core.product.dataintegration.price.converter;

import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.europe1.model.PDTRowModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.product.UnitService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.ws.core.constants.WileyWSConstants;
import com.wiley.ws.core.product.crud.converter.AbstractCrudDtoConverter;
import com.wiley.ws.core.product.dataintegration.common.exception.DataImportValidationErrorsException;
import com.wiley.ws.core.product.dataintegration.price.dto.PriceSwgDTO;


public class PriceImportConverter extends AbstractCrudDtoConverter<PriceSwgDTO, PriceRowModel>
{

	private final CommonI18NService commonI18NService;
	private final ModelService modelService;
	private final UnitService unitService;

	@Autowired
	public PriceImportConverter(final ModelService modelService, final CommonI18NService commonI18NService,
			final UnitService unitService)
	{
		this.modelService = modelService;
		this.commonI18NService = commonI18NService;
		this.unitService = unitService;
	}

	@Override
	public PriceRowModel convert(final PriceSwgDTO priceSwgDTO, final PriceRowModel priceRowModel)
			throws ConversionException
	{
		ServicesUtil.validateParameterNotNull(priceSwgDTO, "PriceSwgDTO object must not be null");
		boolean isNewProduct = false;
		PriceRowModel convertedPriceRowModel = priceRowModel;
		if (priceRowModel == null)
		{
			convertedPriceRowModel = modelService.create(PriceRowModel.class);
			isNewProduct = true;
		}
		final UnitModel unit = unitService.getUnitForCode(WileyWSConstants.DEFAULT_UNIT_PIECES);
		convertedPriceRowModel.setCode(priceSwgDTO.getId());
		if (isNewProduct)
		{
			convertedPriceRowModel.setProductId(priceSwgDTO.getProductId());
		}
		else
		{
			validateProductIdIsSame(convertedPriceRowModel, priceSwgDTO);
		}
		convertedPriceRowModel.setUnit(unit);
		convertedPriceRowModel.setNet(Boolean.TRUE);
		convertedPriceRowModel.setPrice(priceSwgDTO.getPrice());
		try
		{
			convertedPriceRowModel.setCurrency(commonI18NService.getCurrency(priceSwgDTO.getCurrency()));
		}
		catch (final UnknownIdentifierException e)
		{
			throw DataImportValidationErrorsException.ofNoReferenceException(PriceRowModel._TYPECODE, "Currency");
		}
		try
		{
			convertedPriceRowModel.setCountry(commonI18NService.getCountry(priceSwgDTO.getCountry()));
		}
		catch (final UnknownIdentifierException e)
		{
			throw DataImportValidationErrorsException.ofNoReferenceException(PriceRowModel._TYPECODE, "Country");
		}
		convertedPriceRowModel.setMinqtd(Long.valueOf(priceSwgDTO.getMinQuantity().longValue()));
		convertedPriceRowModel.setSequenceId(priceSwgDTO.getSequenceId());
		return convertedPriceRowModel;
	}

	private void validateProductIdIsSame(final PriceRowModel convertedPriceRowModel, final PriceSwgDTO priceSwgDTO)
	{
		String existingProductId = convertedPriceRowModel.getProductId();
		String newProductId = priceSwgDTO.getProductId();
		if (!StringUtils.equalsIgnoreCase(existingProductId, newProductId))
		{
			throw DataImportValidationErrorsException.ofImmutableFieldException(PriceRowModel._TYPECODE,
					PDTRowModel.PRODUCTID);
		}
	}

}
