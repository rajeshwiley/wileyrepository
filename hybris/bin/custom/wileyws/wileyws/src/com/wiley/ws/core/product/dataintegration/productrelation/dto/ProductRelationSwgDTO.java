package com.wiley.ws.core.product.dataintegration.productrelation.dto;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.validator.constraints.NotBlank;


public class ProductRelationSwgDTO
{
	@NotBlank
	private String targetProductId;
	@NotBlank
	private String type;

	public String getTargetProductId()
	{
		return targetProductId;
	}

	public String getType()
	{
		return type;
	}

	public void setTargetProductId(final String targetProductId)
	{
		this.targetProductId = targetProductId;
	}

	public void setType(final String type)
	{
		this.type = type;
	}

	@Override
	public String toString()
	{
		return new ToStringBuilder(this)
				.append("targetProductId", targetProductId)
				.append("type", type)
				.toString();
	}
}
