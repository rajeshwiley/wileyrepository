package com.wiley.ws.core.cart.populator;

import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.util.PriceValue;

import javax.annotation.Resource;

import com.wiley.integrations.order.dto.PriceValueWsDTO;


public class WileyasPriceValueReverseConverter implements Converter<PriceValueWsDTO, PriceValue>
{
	private static final boolean IS_NET = true;

	@Resource
	private CommonI18NService commonI18NService;

	@Override
	public PriceValue convert(final PriceValueWsDTO priceValueWsDTO)
			throws ConversionException
	{
		PriceValue priceValue = new PriceValue(commonI18NService.getCurrency(priceValueWsDTO.getCurrency()).getIsocode(),
				priceValueWsDTO.getValue(), IS_NET);
		return priceValue;
	}

	@Override
	public PriceValue convert(final PriceValueWsDTO priceValueWsDTO, final PriceValue priceValue) throws ConversionException
	{
		return convert(priceValueWsDTO);
	}
}
