package com.wiley.ws.core.product.dataintegration.category.converter.populator;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.ws.core.product.crud.converter.populator.LocalizedValuesNullationService;
import com.wiley.ws.core.product.crud.facade.WileyProductCrudCatalogVersionProvider;
import com.wiley.ws.core.product.dataintegration.category.dto.CategorySwgDTO;
import com.wiley.ws.core.product.dataintegration.category.service.CategoryImportService;
import com.wiley.ws.core.product.dataintegration.common.exception.DataImportValidationErrorsException;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;
import static org.apache.commons.lang.LocaleUtils.toLocale;


public class CategoryImportPopulator implements Populator<CategorySwgDTO, CategoryModel>
{

	private static final String SUBJECT_TYPE_LOCALIZED_STRING = "LocalizedString";
	private static final String SUBJECT_LOCALE = "locale";
	private static final String SUBJECT_CATEGORY = "supercategories";

	private final CategoryImportService categoryImportService;
	private final WileyProductCrudCatalogVersionProvider wileyProductCrudCatalogVersionProvider;
	private final LocalizedValuesNullationService<CategoryModel> localizedValuesNullationService;

	@Autowired
	public CategoryImportPopulator(final CategoryImportService categoryImportService,
			final WileyProductCrudCatalogVersionProvider wileyProductCrudCatalogVersionProvider,
			final LocalizedValuesNullationService<CategoryModel> localizedValuesNullationService)
	{
		this.categoryImportService = categoryImportService;
		this.wileyProductCrudCatalogVersionProvider = wileyProductCrudCatalogVersionProvider;
		this.localizedValuesNullationService = localizedValuesNullationService;
	}

	@Override
	public void populate(final CategorySwgDTO categorySwgDTO, final CategoryModel categoryModel) throws ConversionException
	{
		validateParameterNotNull(categorySwgDTO, "Category dto object must not be null");
		validateParameterNotNull(categoryModel, "Category model object must not be null");

		if (StringUtils.isEmpty(categoryModel.getCode())) {
			categoryModel.setCode(categorySwgDTO.getId());
		}

		populateName(categorySwgDTO, categoryModel);
		populateSuperCategories(categorySwgDTO, categoryModel);
		populateCatalogVersion(categoryModel);
	}

	private void populateCatalogVersion(final CategoryModel categoryModel)
	{
		if (categoryModel.getCatalogVersion() == null)
		{
			categoryModel.setCatalogVersion(wileyProductCrudCatalogVersionProvider.getCatalogVersion());
		}
	}

	private void populateSuperCategories(final CategorySwgDTO categorySwgDTO, final CategoryModel categoryModel)
	{
		try
		{
			final List<CategoryModel> superCategories = categorySwgDTO.getSupercategories()
					.stream()
					.map(categoryImportService::getCategoryForCode)
					.collect(Collectors.toList());
			categoryModel.setSupercategories(superCategories);
		}
		catch (UnknownIdentifierException e)
		{
			throw DataImportValidationErrorsException.ofNoReferenceException(CategoryModel._TYPECODE, SUBJECT_CATEGORY);
		}
	}

	private void populateName(final CategorySwgDTO categorySwgDTO, final CategoryModel categoryModel)
	{
		try
		{
			localizedValuesNullationService.resetValue(categoryModel, (c, locale) -> c.setName(null, locale));
			categorySwgDTO.getName()
					.stream()
					.forEach(name -> categoryModel.setName(name.getValue(), toLocale(name.getLocale())));
		}
		catch (IllegalArgumentException e)
		{
			throw DataImportValidationErrorsException.ofNoReferenceException(SUBJECT_TYPE_LOCALIZED_STRING, SUBJECT_LOCALE);
		}
	}
}
