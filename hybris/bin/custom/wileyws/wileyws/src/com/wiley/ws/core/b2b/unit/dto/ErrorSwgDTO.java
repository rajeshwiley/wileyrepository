package com.wiley.ws.core.b2b.unit.dto;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Description of a server error.
 */
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-07-26T11:08:47.854Z")
public class ErrorSwgDTO
{

	private String code = null;
	private String message = null;


	/**
	 * Error code.
	 **/
	public ErrorSwgDTO code(final String code)
	{
		this.code = code;
		return this;
	}

	@JsonProperty("code")
	public String getCode()
	{
		return code;
	}

	public void setCode(final String code)
	{
		this.code = code;
	}


	/**
	 * Error description.
	 **/
	public ErrorSwgDTO message(final String message)
	{
		this.message = message;
		return this;
	}

	@JsonProperty("message")
	public String getMessage()
	{
		return message;
	}

	public void setMessage(final String message)
	{
		this.message = message;
	}


	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		ErrorSwgDTO errorSwgDTO = (ErrorSwgDTO) o;
		return Objects.equals(this.code, errorSwgDTO.code)
				&& Objects.equals(this.message, errorSwgDTO.message);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(code, message);
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("class Error {\n");

		sb.append("    code: ").append(toIndentedString(code)).append("\n");
		sb.append("    message: ").append(toIndentedString(message)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(final Object o)
	{
		if (o == null)
		{
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}

