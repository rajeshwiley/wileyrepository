package com.wiley.ws.core.wileysubscription.populator.impl;

import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.UUID;

import org.apache.commons.lang3.StringUtils;

import com.wiley.core.util.Assert;
import com.wiley.ws.core.wileysubscription.dto.PaymentInfoUpdateRequestWsDTO;
import com.wiley.ws.core.wileysubscription.dto.WileySubscriptionAddressWsDTO;

import static com.wiley.facades.populators.PopulateUtils.populateFieldIfNotNull;


/**
 * Implementation for {@link CreditCardPaymentInfoModel}.
 */
public class CreditCardPaymentInfoCreatingStrategy
		extends AbstractPaymentInfoCreatingStrategy<CreditCardPaymentInfoModel>
{
	@Override
	public Class<CreditCardPaymentInfoModel> getPaymentInfoClass()
	{
		return CreditCardPaymentInfoModel.class;
	}

	@Override
	public void populate(final PaymentInfoUpdateRequestWsDTO paymentInfoDto,
			final CreditCardPaymentInfoModel paymentInfoModel) throws ConversionException
	{
		final UserModel user = paymentInfoModel.getUser();
		Assert.state(user != null, () -> "User for PaymentInfo should not be null.");

		// Required fields
		if (StringUtils.isEmpty(paymentInfoModel.getCode()))
		{
			paymentInfoModel.setCode(user.getUid() + "_" + UUID.randomUUID());
		}
		populateFieldIfNotNull(this::populateCcOwner, paymentInfoModel, paymentInfoDto.getPaymentAddress());
		populateFieldIfNotNull(CreditCardPaymentInfoModel::setNumber, paymentInfoModel, paymentInfoDto.getPaymentToken());
		populateFieldIfNotNull(this::populateCreditCardType, paymentInfoModel, paymentInfoDto.getCreditCardType());
		populateFieldIfNotNull(this::populateValidToMonth, paymentInfoModel, paymentInfoDto.getCreditCardValidToMonth());
		populateFieldIfNotNull(this::populateValidToYear, paymentInfoModel,
				paymentInfoDto.getCreditCardValidToYear());

		// Optional fields
		populateFieldIfNotNull(this::populateBillingAddress, paymentInfoModel, paymentInfoDto.getPaymentAddress());
	}

	private void populateValidToMonth(final CreditCardPaymentInfoModel paymentInfoModel, final Integer month)
	{
		paymentInfoModel.setValidToMonth(String.valueOf(month));
	}

	private void populateValidToYear(final CreditCardPaymentInfoModel paymentInfoModel, final Integer year)
	{
		paymentInfoModel.setValidToYear(String.valueOf(year));
	}

	private void populateCreditCardType(final CreditCardPaymentInfoModel paymentInfoModel,
			final PaymentInfoUpdateRequestWsDTO.CreditCardType creditCardType)
	{
		paymentInfoModel.setType(creditCardType.getValue());
	}

	private void populateCcOwner(final CreditCardPaymentInfoModel paymentInfoModel,
			final WileySubscriptionAddressWsDTO addressWsDTO)
	{
		final String firstName = addressWsDTO.getFirstName();
		final String lastName = addressWsDTO.getLastName();

		Assert.state(StringUtils.isNotEmpty(firstName) && StringUtils.isNotEmpty(lastName),
				() -> "First name and last name are required in payment address for credit card.");

		paymentInfoModel.setCcOwner(firstName + " " + lastName);
	}
}
