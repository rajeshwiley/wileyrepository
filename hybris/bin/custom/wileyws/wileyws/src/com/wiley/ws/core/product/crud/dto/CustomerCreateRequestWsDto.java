package com.wiley.ws.core.product.crud.dto;

/**
 * CustomerCreateRequestWsDto
 */
public class CustomerCreateRequestWsDto
{

	private String email;

	private String password;

	private String firstName;

	private String lastName;

	private TitleCode titleCode;

	public String getEmail()
	{
		return email;
	}

	public void setEmail(final String email)
	{
		this.email = email;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(final String password)
	{
		this.password = password;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(final String firstName)
	{
		this.firstName = firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(final String lastName)
	{
		this.lastName = lastName;
	}

	public TitleCode getTitleCode()
	{
		return titleCode;
	}

	public void setTitleCode(final TitleCode titleCode)
	{
		this.titleCode = titleCode;
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("class CustomerCreateRequestWsDto {\n");
		sb.append("email: ").append(email).append("\n");
		sb.append("password: ").append(password).append("\n");
		sb.append("firstName: ").append(firstName).append("\n");
		sb.append("lastName: ").append(lastName).append("\n");
		sb.append("titleCode: ").append(titleCode).append("\n");
		sb.append("}");
		return sb.toString();
	}

	public enum TitleCode
	{
		DR("dr"),
		MISS("miss"),
		MR("mr"),
		MRS("mrs"),
		MS("ms"),
		REV("rev"),
		NOTITLE("notitle");

		private String value;

		TitleCode(final String value)
		{
			this.value = value;
		}

		public String getValue()
		{
			return value;
		}

		@Override
		public String toString()
		{
			return String.valueOf(value);
		}
	}
}

