package com.wiley.ws.core.product.crud.converter.populator.variantproduct;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.model.WileyPurchaseOptionProductModel;
import com.wiley.core.model.WileyWebLinkModel;
import com.wiley.ws.core.product.crud.converter.WebLinkConverter;
import com.wiley.ws.core.product.crud.dto.LinkToExternalStoreWsDto;
import com.wiley.ws.core.product.crud.dto.WebLinkDto;
import com.wiley.ws.core.product.crud.dto.WileyVariantProductWsDto;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


public class ExternalStoresPopulator
		implements Populator<WileyVariantProductWsDto, WileyPurchaseOptionProductModel>
{
	private final WebLinkConverter webLinkConverter;

	@Autowired
	public ExternalStoresPopulator(final WebLinkConverter webLinkConverter)
	{
		this.webLinkConverter = webLinkConverter;
	}

	@Override
	public void populate(final WileyVariantProductWsDto variantProductWsDto,
			final WileyPurchaseOptionProductModel variantProductModel) throws ConversionException
	{
		validateParameterNotNull(variantProductWsDto, "VariantProductDto can't be null");
		validateParameterNotNull(variantProductModel, "VariantProductModel can't be null");

		final List<WileyWebLinkModel> externalStores = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(variantProductWsDto.getExternalStores()))
		{
			for (final LinkToExternalStoreWsDto linkToExternalStoreWsDto : variantProductWsDto.getExternalStores())
			{
				final WileyWebLinkModel webLinkModel = webLinkConverter.convert(new WebLinkDto(linkToExternalStoreWsDto));
				externalStores.add(webLinkModel);
			}
		}
		variantProductModel.setExternalStores(externalStores);
	}
}
