package com.wiley.ws.core.order.service;

import de.hybris.platform.core.model.order.AbstractOrderModel;

import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.wiley.ws.core.order.dto.DigitalProductAccessTokenWsDTO;


/**
 * It is in charge of applying external digital product access tokens order entries.
 */
public interface WileycomDigitalTokenApplier
{

	/**
	 * Applies external digital tokens to each to order entry
	 *
	 * @param orderModel
	 * @param tokens
	 */
	void applyDigitalTokens(@Nonnull AbstractOrderModel orderModel, @Nullable List<DigitalProductAccessTokenWsDTO> tokens);

}
