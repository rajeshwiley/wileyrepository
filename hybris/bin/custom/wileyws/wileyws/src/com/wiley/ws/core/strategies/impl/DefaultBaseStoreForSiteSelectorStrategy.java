/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package com.wiley.ws.core.strategies.impl;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import javax.annotation.Resource;

import com.wiley.core.store.WileyBaseStoreService;
import com.wiley.ws.core.strategies.BaseStoreForSiteSelectorStrategy;


/**
 * Default implementation of {@link BaseStoreForSiteSelectorStrategy} which returns first base store from collection.
 */
public class DefaultBaseStoreForSiteSelectorStrategy implements BaseStoreForSiteSelectorStrategy
{

	@Resource
	private WileyBaseStoreService wileyBaseStoreService;

	/**
	 * Gets base store.
	 *
	 * @param baseSiteModel
	 * 		the base site model
	 * @return the base store
	 */
	@Override
	public BaseStoreModel getBaseStore(final BaseSiteModel baseSiteModel)
	{
		final Optional<BaseStoreModel> optionalBaseStoreForBaseSite = wileyBaseStoreService.getBaseStoreForBaseSite(
				baseSiteModel);
		return optionalBaseStoreForBaseSite.orElse(null);
	}
}
