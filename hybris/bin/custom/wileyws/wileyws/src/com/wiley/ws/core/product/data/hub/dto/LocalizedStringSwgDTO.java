package com.wiley.ws.core.product.data.hub.dto;

import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wiley.ws.core.common.dto.LocaleSwgDTO;


/**
 * String value with the associated locale.
 **/
public class LocalizedStringSwgDTO
{


	private LocaleSwgDTO loc = null;
	private String val = null;

	/**
	 **/
	@Valid
	@NotNull
	@JsonProperty("loc")
	public LocaleSwgDTO getLoc()
	{
		return loc;
	}

	public void setLoc(final LocaleSwgDTO loc)
	{
		this.loc = loc;
	}

	/**
	 **/
	@NotNull
	@Size(max = 255)
	@JsonProperty("val")
	public String getVal()
	{
		return val;
	}

	public void setVal(final String val)
	{
		this.val = val;
	}


	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		LocalizedStringSwgDTO localizedStringSwgDTO = (LocalizedStringSwgDTO) o;
		return Objects.equals(loc, localizedStringSwgDTO.loc) && Objects.equals(val, localizedStringSwgDTO.val);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(loc, val);
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("class LocalizedStringSwgDTO {\n");
		sb.append("  loc: ").append(loc).append("\n");
		sb.append("  val: ").append(val).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
