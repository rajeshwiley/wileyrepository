package com.wiley.ws.core.product.dataintegration.common.dto;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.validator.constraints.NotBlank;

import com.wiley.core.validation.constraints.ValidLocale;

/**
 * String value with the associated locale.
 */
public class LocalizedStringSwgDTO
{
	@NotBlank
	@ValidLocale
	private String locale = null;

	private String value = null;

	public String getLocale()
	{
		return locale;
	}

	public void setLocale(final String locale)
	{
		this.locale = locale;
	}

	public String getValue()
	{
		return value;
	}

	public void setValue(final String value)
	{
		this.value = value;
	}

	@Override
	public String toString()
	{
		return new ToStringBuilder(this)
				.append("locale", locale)
				.append("value", value)
				.toString();
	}
}

