package com.wiley.ws.core.wileysubscription.dto;

import java.util.Objects;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wiley.ws.core.address.dto.AddressWsDTO;


/**
 * Address DTO for Wiley Subscriptions
 */
public class WileySubscriptionAddressWsDTO extends AddressWsDTO
{
	@NotNull
	@Size(max = 255)
	@JsonProperty("firstName")
	private String firstName;

	@NotNull
	@Size(max = 255)
	@JsonProperty("lastName")
	private String lastName;

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(final String firstName)
	{
		this.firstName = firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(final String lastName)
	{
		this.lastName = lastName;
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (!(o instanceof WileySubscriptionAddressWsDTO))
		{
			return false;
		}
		if (!super.equals(o))
		{
			return false;
		}
		final WileySubscriptionAddressWsDTO that = (WileySubscriptionAddressWsDTO) o;
		return Objects.equals(firstName, that.firstName)
				&& Objects.equals(lastName, that.lastName);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(super.hashCode(), firstName, lastName);
	}

	@Override
	public String toString()
	{
		return new ToStringBuilder(this)
				.appendSuper(super.toString())
				.append("firstName", firstName)
				.append("lastName", lastName)
				.toString();
	}
}
