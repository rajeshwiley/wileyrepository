package com.wiley.ws.core.product.data.hub.converter.impl;

import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.product.UnitService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.product.WileySubscriptionTermService;
import com.wiley.ws.core.product.data.hub.converter.ProductPriceSwgDTOPDHConverter;
import com.wiley.ws.core.product.data.hub.dto.ProductPriceSwgDTO;

import static com.wiley.ws.core.constants.WileyWSConstants.DEFAULT_NET;
import static com.wiley.ws.core.constants.WileyWSConstants.DEFAULT_UNIT_PIECES;


/**
 * Converter implementation
 */
public class ProductPriceSwgDTOPDHConverterImpl implements ProductPriceSwgDTOPDHConverter
{
	@Autowired
	private ModelService modelService;

	@Autowired
	private CommonI18NService commonI18NService;

	@Autowired
	private UnitService unitService;

	@Autowired
	private WileySubscriptionTermService wileySubscriptionTermService;

	public PriceRowModel convertToPriceRowModel(final ProductPriceSwgDTO price, final String productCode)
	{
		PriceRowModel priceRow = modelService.create(PriceRowModel.class);
		priceRow.setUnit(unitService.getUnitForCode(DEFAULT_UNIT_PIECES));
		priceRow.setNet(DEFAULT_NET);
		priceRow.setPrice(price.getPrice());
		priceRow.setMinqtd(price.getMinQuantity());
		priceRow.setCurrency(commonI18NService.getCurrency(price.getCurrency().getValue()));
		priceRow.setProductId(productCode);

		if (price.getCountry() != null && StringUtils.isNotBlank(price.getCountry().getValue()))
		{
			priceRow.setCountry(commonI18NService.getCountry(price.getCountry().getValue()));
		}

		if (StringUtils.isNotBlank(price.getSubscriptionTerm()))
		{
			priceRow.setSubscriptionTerm(wileySubscriptionTermService.getSubscriptionTerm(price.getSubscriptionTerm()));
		}
		return priceRow;
	}

}
