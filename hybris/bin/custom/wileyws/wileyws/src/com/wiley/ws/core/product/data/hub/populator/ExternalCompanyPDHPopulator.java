package com.wiley.ws.core.product.data.hub.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.model.ExternalCompanyModel;
import com.wiley.core.externalcompany.WileycomExternalCompanyService;
import com.wiley.ws.core.product.data.hub.dto.ProductSwgDTO;


/**
 * Populate from {@link ProductSwgDTO#getExternalCompany()} to {@link ProductModel#setExternalCompany(ExternalCompanyModel)}
 */
public class ExternalCompanyPDHPopulator implements Populator<ProductSwgDTO, ProductModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(ExternalCompanyPDHPopulator.class);

	@Autowired
	private WileycomExternalCompanyService wileycomExternalCompanyService;

	@Override
	public void populate(final ProductSwgDTO product, final ProductModel productModel) throws ConversionException
	{
		String externalCompanyId = product.getExternalCompany();
		if (StringUtils.isNotBlank(externalCompanyId))
		{
			ExternalCompanyModel externalCompany = wileycomExternalCompanyService.getExternalCompany(externalCompanyId);
			productModel.setExternalCompany(externalCompany);
			LOG.debug("Product [{}]: external company was set to [{}] from DTO value [{}]", productModel.getCode(),
					productModel.getExternalCompany(), externalCompanyId);
		}
		else
		{
			LOG.debug("Skip populating external company due to null");
		}

	}
}
