package com.wiley.ws.core.order.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.validation.constraints.NotNull;

import com.wiley.ws.core.address.dto.AddressWsDTO;
import com.wiley.ws.core.address.populators.AbstractWileycomAddressReversePopulator;

import static com.wiley.facades.populators.PopulateUtils.populateFieldIfNotNull;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


/**
 * Author Herman_Chukhrai (EPAM)
 */
public class WileycomAddressWsReversePopulator extends AbstractWileycomAddressReversePopulator
		implements Populator<AddressWsDTO, AddressModel>
{

	@Override
	public void populate(@NotNull final AddressWsDTO source, @NotNull final AddressModel target) throws ConversionException
	{
		validateParameterNotNullStandardMessage("source", source);
		validateParameterNotNullStandardMessage("target", target);

		populateFieldIfNotNull(AddressModel::setPostalcode, target, source.getPostcode());
		populateFieldIfNotNull(AddressModel::setTown, target, source.getCity());
		populateFieldIfNotNull(AddressModel::setStreetname, target, source.getLine1());
		populateFieldIfNotNull(AddressModel::setStreetnumber, target, source.getLine2());
		populateFieldIfNotNull(AddressModel::setPhone1, target, source.getPhoneNumber());

		populateFieldIfNotNull(this::populateCountry, target, source.getCountry());
		populateRegion(target, source.getState());
	}
}
