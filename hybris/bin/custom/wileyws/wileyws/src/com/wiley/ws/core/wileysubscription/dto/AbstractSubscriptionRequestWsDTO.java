package com.wiley.ws.core.wileysubscription.dto;

import de.hybris.platform.subscriptionservices.enums.TermOfServiceFrequency;

import java.util.Date;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Conteins common fields for SubscriptionRequest
 */
public abstract class AbstractSubscriptionRequestWsDTO
{

	public enum SubscriptionStatus
	{
		ACTIVE(de.hybris.platform.subscriptionservices.enums.SubscriptionStatus.ACTIVE),
		EXPIRED(de.hybris.platform.subscriptionservices.enums.SubscriptionStatus.EXPIRED),
		CANCELLED(de.hybris.platform.subscriptionservices.enums.SubscriptionStatus.CANCELLED);

		private de.hybris.platform.subscriptionservices.enums.SubscriptionStatus value;

		SubscriptionStatus(final de.hybris.platform.subscriptionservices.enums.SubscriptionStatus value)
		{
			this.value = value;
		}

		public de.hybris.platform.subscriptionservices.enums.SubscriptionStatus getValue()
		{
			return value;
		}
	}

	public enum BillingFrequency
	{
		FREE("FREE"), DAILY("DAILY"), WEEKLY("WEEKLY"), MONTHLY("MONTHLY"), QUARTERLY("QUARTERLY"), ANNUALLY("ANNUALLY");

		private String billingFrequencyCode;

		BillingFrequency(final String billingFrequencyCode)
		{
			this.billingFrequencyCode = billingFrequencyCode;
		}

		public String getBillingFrequencyCode()
		{
			return billingFrequencyCode;
		}
	}

	public enum ContractDurationUnit
	{
		DAY(TermOfServiceFrequency.DAILY), WEEK(TermOfServiceFrequency.WEEKLY), MONTH(TermOfServiceFrequency.MONTHLY), QUARTER(
			TermOfServiceFrequency.QUARTERLY), YEAR(TermOfServiceFrequency.ANNUALLY);

		private TermOfServiceFrequency value;

		ContractDurationUnit(final TermOfServiceFrequency value)
		{
			this.value = value;
		}

		public TermOfServiceFrequency getValue()
		{
			return value;
		}
	}

	private String externalCode;

	private SubscriptionStatus subscriptionStatus;

	private Date subscriptionStartDate;

	private Date subscriptionEndDate;

	private Date nextBillingDate;

	private Double billingPriceValue;

	private String billingPriceCurrency;

	private BillingFrequency billingFrequency;

	private Integer contractDurationValue;

	private ContractDurationUnit contractDurationUnit;

	private Boolean renewable;

	private PaymentInfoUpdateRequestWsDTO paymentInfo;

	private WileySubscriptionAddressWsDTO shippingAddress;

	@Size(max = 255)
	@JsonProperty("externalCode")
	public String getExternalCode()
	{
		return externalCode;
	}

	public void setExternalCode(final String externalCode)
	{
		this.externalCode = externalCode;
	}

	@JsonProperty("subscriptionStatus")
	public SubscriptionStatus getSubscriptionStatus()
	{
		return subscriptionStatus;
	}

	public void setSubscriptionStatus(
			final SubscriptionStatus subscriptionStatus)
	{
		this.subscriptionStatus = subscriptionStatus;
	}

	@JsonProperty("subscriptionStartDate")
	public Date getSubscriptionStartDate()
	{
		return subscriptionStartDate;
	}

	public void setSubscriptionStartDate(final Date subscriptionStartDate)
	{
		this.subscriptionStartDate = subscriptionStartDate;
	}

	@JsonProperty("subscriptionEndDate")
	public Date getSubscriptionEndDate()
	{
		return subscriptionEndDate;
	}

	public void setSubscriptionEndDate(final Date subscriptionEndDate)
	{
		this.subscriptionEndDate = subscriptionEndDate;
	}

	@JsonProperty("nextBillingDate")
	public Date getNextBillingDate()
	{
		return nextBillingDate;
	}

	public void setNextBillingDate(final Date nextBillingDate)
	{
		this.nextBillingDate = nextBillingDate;
	}

	@JsonProperty("billingPriceValue")
	public Double getBillingPriceValue()
	{
		return billingPriceValue;
	}

	public void setBillingPriceValue(final Double billingPriceValue)
	{
		this.billingPriceValue = billingPriceValue;
	}

	@JsonProperty("billingPriceCurrency")
	public String getBillingPriceCurrency()
	{
		return billingPriceCurrency;
	}

	public void setBillingPriceCurrency(final String billingPriceCurrency)
	{
		this.billingPriceCurrency = billingPriceCurrency;
	}

	@JsonProperty("billingFrequency")
	public BillingFrequency getBillingFrequency()
	{
		return billingFrequency;
	}

	public void setBillingFrequency(
			final BillingFrequency billingFrequency)
	{
		this.billingFrequency = billingFrequency;
	}

	@JsonProperty("contractDurationValue")
	public Integer getContractDurationValue()
	{
		return contractDurationValue;
	}

	public void setContractDurationValue(final Integer contractDurationValue)
	{
		this.contractDurationValue = contractDurationValue;
	}

	@JsonProperty("contractDurationUnit")
	public ContractDurationUnit getContractDurationUnit()
	{
		return contractDurationUnit;
	}

	public void setContractDurationUnit(
			final ContractDurationUnit contractDurationUnit)
	{
		this.contractDurationUnit = contractDurationUnit;
	}

	@JsonProperty("renewable")
	public Boolean getRenewable()
	{
		return renewable;
	}

	public void setRenewable(final Boolean renewable)
	{
		this.renewable = renewable;
	}

	@Valid
	@JsonProperty("paymentInfo")
	public PaymentInfoUpdateRequestWsDTO getPaymentInfo()
	{
		return paymentInfo;
	}

	public void setPaymentInfo(final PaymentInfoUpdateRequestWsDTO paymentInfo)
	{
		this.paymentInfo = paymentInfo;
	}

	@Valid
	@JsonProperty("shippingAddress")
	public WileySubscriptionAddressWsDTO getShippingAddress()
	{
		return shippingAddress;
	}

	public void setShippingAddress(final WileySubscriptionAddressWsDTO shippingAddress)
	{
		this.shippingAddress = shippingAddress;
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (!(o instanceof AbstractSubscriptionRequestWsDTO))
		{
			return false;
		}
		final AbstractSubscriptionRequestWsDTO that = (AbstractSubscriptionRequestWsDTO) o;
		return Objects.equals(externalCode, that.externalCode)
				&& subscriptionStatus == that.subscriptionStatus
				&& Objects.equals(subscriptionStartDate, that.subscriptionStartDate)
				&& Objects.equals(subscriptionEndDate, that.subscriptionEndDate)
				&& Objects.equals(nextBillingDate, that.nextBillingDate)
				&& Objects.equals(billingPriceValue, that.billingPriceValue)
				&& Objects.equals(billingPriceCurrency, that.billingPriceCurrency)
				&& billingFrequency == that.billingFrequency
				&& Objects.equals(contractDurationValue, that.contractDurationValue)
				&& contractDurationUnit == that.contractDurationUnit
				&& Objects.equals(renewable, that.renewable)
				&& Objects.equals(paymentInfo, that.paymentInfo)
				&& Objects.equals(shippingAddress, that.shippingAddress);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(externalCode, subscriptionStatus, subscriptionStartDate, subscriptionEndDate, nextBillingDate,
				billingPriceValue, billingPriceCurrency, billingFrequency, contractDurationValue, contractDurationUnit, renewable,
				paymentInfo, shippingAddress);
	}
}
