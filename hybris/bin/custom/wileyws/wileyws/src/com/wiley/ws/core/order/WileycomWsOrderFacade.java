package com.wiley.ws.core.order;

import javax.validation.constraints.NotNull;

import com.wiley.ws.core.order.dto.OrderStatusWsDTO;
import com.wiley.ws.core.order.dto.OrderUpdateRequestWsDTO;
import com.wiley.ws.core.order.dto.ShippingEventWsDTO;



/**
 * The interface Wileycom ws order facade.
 */
public interface WileycomWsOrderFacade
{
	/**
	 * Update status and add history entry.
	 * In case if Status of {@link OrderStatusWsDTO} will be CANCELLED,
	 * order will be cancelled.
	 * @param code
	 * 		the code
	 * @param orderStatusWsDTO
	 * 		the order status ws dto
	 */
	void updateStatusAndAddHistoryEntry(String code, OrderStatusWsDTO orderStatusWsDTO);

	/**
	 * Update order information + update information of order entries, order discounts, etc.
	 *
	 * @param orderCode order code
	 * @param orderUpdateRequestWsDTO DTO which contains all order information from SAP ERP
	 */
	void updateOrder(String orderCode, OrderUpdateRequestWsDTO orderUpdateRequestWsDTO);

	/**
	 * retrieves order by code and writes shipping information into the order
	 * @param orderCode
	 * @param shippingEventWsDTO
	 */
	void updateShipping(@NotNull String orderCode, @NotNull ShippingEventWsDTO shippingEventWsDTO);

}
