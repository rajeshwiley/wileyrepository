package com.wiley.ws.core.transaction.service.impl;

import de.hybris.platform.servicelayer.util.ServicesUtil;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;

import org.apache.log4j.Logger;
import org.springframework.scheduling.annotation.Scheduled;

import com.google.common.cache.Cache;
import com.wiley.ws.core.constants.WileyWSConstants;
import com.wiley.ws.core.exceptions.transaction.WileywsTestTransactionNotFoundException;
import com.wiley.ws.core.transaction.WileyWsTestTransaction;
import com.wiley.ws.core.transaction.service.WileyTransactionService;


/**
 * Created by Maksim_Kozich on 12.09.2016.
 */
public class WileyTransactionServiceImpl implements WileyTransactionService
{
	private static final Logger LOG = Logger.getLogger(WileyTransactionServiceImpl.class);

	public static final String TRANSACTION_NOT_FOUND_MESSAGE = "Transaction not found: ";

	@Resource
	private Cache<String, WileyWsTestTransaction> wileyWsTestTransactionCache;


	@Override
	public WileyWsTestTransaction getTransaction(@NotNull final String transactionId)
	{
		ServicesUtil.validateParameterNotNullStandardMessage(transactionId, "transactionId");

		final WileyWsTestTransaction transaction = wileyWsTestTransactionCache.getIfPresent(transactionId);
		if (transaction == null)
		{
			throw new WileywsTestTransactionNotFoundException(TRANSACTION_NOT_FOUND_MESSAGE + transactionId);
		}

		return transaction;
	}

	@Override
	public WileyWsTestTransaction startUsingTransaction(@NotNull final String transactionId)
	{
		ServicesUtil.validateParameterNotNull(transactionId, "Parameter transactionId can not be null");

		WileyWsTestTransaction transaction = getTransaction(transactionId);
		transaction.activateAsCurrentTransaction();
		LOG.info("Start using EXISTING transaction " + getKey(transaction));
		return transaction;
	}


	@Override
	public WileyWsTestTransaction beginNewTransaction()
	{
		WileyWsTestTransaction newTransaction = new WileyWsTestTransaction();
		LOG.info("Begin NEW transaction (add to cache)" + getKey(newTransaction));
		newTransaction.activateAsCurrentTransaction();
		newTransaction.begin();
		wileyWsTestTransactionCache.put(getKey(newTransaction), newTransaction);
		return newTransaction;
	}

	@Override
	public WileyWsTestTransaction rollbackTransaction(@NotNull final String transactionId)
	{
		ServicesUtil.validateParameterNotNull(transactionId, "Parameter transactionId can not be null");

		final WileyWsTestTransaction currentTransaction = getTransaction(transactionId);

		LOG.info("Rollback transaction " + getKey(currentTransaction) + " (invalidate in cache)");

		wileyWsTestTransactionCache.invalidate(getKey(currentTransaction));
		return currentTransaction;
	}

	@Override
	public WileyWsTestTransaction commitTransaction(@NotNull final WileyWsTestTransaction transaction)
	{
		ServicesUtil.validateParameterNotNull(transaction, "Parameter transaction can not be null");

		final WileyWsTestTransaction currentTransaction = (WileyWsTestTransaction) WileyWsTestTransaction.current();
		currentTransaction.isRollbackOnCommitError();
		if (currentTransaction.isRollbackOnly())
		{
			LOG.info("Commit haven't happened due to current transaction " + getKey(transaction) + " is rollbackOnly");
		}
		else
		{
			LOG.info("Commit transaction " + getKey(transaction));
			transaction.activateAsCurrentTransaction();
			transaction.commit();


			currentTransaction.activateAsCurrentTransaction();
		}
		return transaction;
	}

	@Override
	@Scheduled(fixedRate = WileyWSConstants.TRANSACTION_EXPIRATION_CLEANUP_INTERVAL_MILLISECONDS)
	public void cleanUpCache()
	{
		LOG.debug("CleanUp transactions cache");
		wileyWsTestTransactionCache.cleanUp();
	}

	private String getKey(final WileyWsTestTransaction currentTransaction)
	{
		return String.valueOf(currentTransaction.getObjectID());
	}
}
