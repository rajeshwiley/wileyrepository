package com.wiley.ws.core.wileysubscription.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;

import java.util.Optional;

import javax.annotation.Resource;

import com.wiley.core.model.WileySubscriptionModel;
import com.wiley.core.util.Assert;
import com.wiley.core.wileyws.order.WileycomWsOrderService;
import com.wiley.ws.core.wileysubscription.dto.AbstractSubscriptionRequestWsDTO;
import com.wiley.ws.core.wileysubscription.dto.SubscriptionCreateRequestWsDTO;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


/**
 * WileySubscription reverse populator. Populates {@link WileySubscriptionModel} from {@link SubscriptionCreateRequestWsDTO}
 */
public class WileyCreateSubscriptionWsReversePopulator
		implements Populator<SubscriptionCreateRequestWsDTO, WileySubscriptionModel>
{

	private static final boolean REQUIRE_RENEWAL_FLAG_DEFAULT_VALUE = true;

	@Resource
	private WileycomWsOrderService wileycomWsOrderService;

	@Resource
	private KeyGenerator wileyb2cSubscriptionCodeGenerator;

	@Resource
	private Populator<AbstractSubscriptionRequestWsDTO, WileySubscriptionModel> wileySubscriptionWsReversePopulator;

	@Override
	public void populate(final SubscriptionCreateRequestWsDTO subscriptionDto,
			final WileySubscriptionModel subscriptionModel) throws ConversionException
	{
		validateParameterNotNullStandardMessage("subscriptionDto", subscriptionDto);
		validateParameterNotNullStandardMessage("subscriptionModel", subscriptionModel);

		String orderId = subscriptionDto.getOrderId();
		String isbn = subscriptionDto.getIsbn();

		Assert.state(orderId != null, () -> "OrderId cannot be null.");
		Assert.state(isbn != null, () -> "Isbn cannot be null.");

		final OrderModel order = wileycomWsOrderService.findOrderByCode(orderId);
		final UserModel user = order.getUser();

		Assert.state(order.getUser() instanceof CustomerModel, () -> String
				.format("Order [%s] should have assigned user and the user should be instance of Customer.", order.getCode()));

		final Optional<AbstractOrderEntryModel> optionalOrderEntry = wileycomWsOrderService.getOrderEntryModelByISBN(order,
				isbn);
		if (!optionalOrderEntry.isPresent())
		{
			throw new UnknownIdentifierException(
					String.format("OrderEntry is not found for order [%s] and product isbn [%s].", order.getCode(), isbn));
		}

		subscriptionModel.setHybrisRenewal(false);

		wileySubscriptionWsReversePopulator.populate(subscriptionDto, subscriptionModel);
		subscriptionModel.setRequireRenewal(
				subscriptionDto.getRenewable() != null ? subscriptionDto.getRenewable() : REQUIRE_RENEWAL_FLAG_DEFAULT_VALUE);
		subscriptionModel.setRenewalEnabled(true);
		subscriptionModel.setCustomer((CustomerModel) user);
		populateOrderEntry(optionalOrderEntry.get(), subscriptionModel);
		subscriptionModel.setCode(wileyb2cSubscriptionCodeGenerator.generate().toString());
	}

	private void populateOrderEntry(final AbstractOrderEntryModel orderEntry, final WileySubscriptionModel subscriptionModel)
	{
		subscriptionModel.setOrderEntry(orderEntry);
		final ProductModel product = orderEntry.getProduct();
		subscriptionModel.setIsbn(product.getIsbn());
		subscriptionModel.setProduct(product);
	}
}
