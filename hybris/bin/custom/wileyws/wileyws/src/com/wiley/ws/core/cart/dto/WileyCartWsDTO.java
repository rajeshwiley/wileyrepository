package com.wiley.ws.core.cart.dto;

import com.wiley.integrations.order.dto.AbstractOrderWsDTO;

import java.util.Objects;

public class WileyCartWsDTO extends AbstractOrderWsDTO
{
	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		WileyCartWsDTO cart = (WileyCartWsDTO) o;
		return true;
	}

	@Override
	public int hashCode()
	{
		return Objects.hash();
	}

	@Override
	public String toString()
	{
		return super.toString();
	}
}

