package com.wiley.ws.core.order;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.WarehouseService;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import javax.annotation.Resource;
import javax.validation.ValidationException;
import javax.validation.constraints.NotNull;

import org.apache.log4j.Logger;

import com.wiley.core.wileyws.order.WileycomWsOrderService;
import com.wiley.ws.core.order.dto.ShippingEventEntryWsDTO;
import com.wiley.ws.core.order.dto.ShippingEventWsDTO;
import com.wiley.ws.core.order.exception.IsbnNotFoundException;


/**
 * Created by Georgii_Gavrysh on 8/10/2016.
 */
public class WileycomWsConsignmentTransformer
{
	private static final Logger LOG = Logger.getLogger(WileycomWsConsignmentTransformer.class);

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "warehouseService")
	private WarehouseService warehouseService;

	@Resource(name = "wileycomWsOrderService")
	private WileycomWsOrderService wileycomWsOrderService;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	public ConsignmentModel createConsignment(@NotNull final ShippingEventWsDTO shippingEventWsDTO,
			@NotNull final OrderModel orderModel)
			throws ConversionException
	{
		ServicesUtil.validateParameterNotNull(shippingEventWsDTO, "shippingEventWsDTO");
		ServicesUtil.validateParameterNotNull(orderModel, "orderModel");
		final ConsignmentModel consignmentModel = getModelService().create(ConsignmentModel.class);
		try
		{
			try
			{
				consignmentModel.setShippingDate(getRfc3339DateFormat().parse(shippingEventWsDTO.getShippingDate()));
			}
			catch (ParseException e)
			{
				throw new ValidationException(String.format("Failed to parse shippingEventWsDTO.getShippingDate(): %s",
						shippingEventWsDTO.getShippingDate()));
			}
			consignmentModel.setTrackingID(shippingEventWsDTO.getTrackingId());
			consignmentModel.setCarrier(shippingEventWsDTO.getCarrier());

			consignmentModel.setCode(getConsignmentCode(shippingEventWsDTO, orderModel));
			consignmentModel.setShippingAddress(orderModel.getDeliveryAddress());
			consignmentModel.setDeliveryMode(orderModel.getDeliveryMode());
			consignmentModel.setStatus(ConsignmentStatus.SHIPPED);
			consignmentModel.setOrder(orderModel);
			/**
			 * get any warehouse because warehouse is mandatory, but is unnecessary for consignment use in wileycom
			 */
			consignmentModel.setWarehouse(getWarehouseService().getDefWarehouse().get(0));
			consignmentModel.setConsignmentEntries(createEntries(orderModel, shippingEventWsDTO, consignmentModel));
		}
		catch (Exception e)
		{
			getModelService().detachAll();
			throw e;
		}
		return consignmentModel;
	}

	private Set<ConsignmentEntryModel> createEntries(final OrderModel orderModel, final ShippingEventWsDTO shippingEventWsDTO,
			final ConsignmentModel consignmentModel)
	{
		Set<ConsignmentEntryModel> result = new HashSet<>((int) (shippingEventWsDTO.getEntries().size() / 0.75) + 1);
		for (final ShippingEventEntryWsDTO shippingEventEntryWsDTO : shippingEventWsDTO.getEntries())
		{
			final ConsignmentEntryModel consignmentEntryModel = getModelService().create(ConsignmentEntryModel.class);
			consignmentEntryModel.setConsignment(consignmentModel);
			Long shippedQuantity;
			try
			{
				shippedQuantity = Long.parseLong(shippingEventEntryWsDTO.getShippedQuantity());
			}
			catch (NumberFormatException e)
			{
				throw new ValidationException(
						String.format("Shipped quantity can not be parsed: shippingEventEntry.shippedQuantity = [%s]",
								shippingEventEntryWsDTO.getShippedQuantity())
						);
			}
			if (shippedQuantity <= 0)
			{
				throw new ValidationException(
						String.format("Shipped quantity must be positive: shippingEventEntry.shippedQuantity = [%s]",
								shippingEventEntryWsDTO.getShippedQuantity())
				);
			}
			consignmentEntryModel.setShippedQuantity(shippedQuantity);
			final Optional<AbstractOrderEntryModel> optionalOrderEntryModelByISBN = getWileycomWsOrderService().
					getOrderEntryModelByISBN(orderModel, shippingEventEntryWsDTO.getIsbn());
			if (!optionalOrderEntryModelByISBN.isPresent())
			{
				throw new IsbnNotFoundException(orderModel.getCode(), shippingEventEntryWsDTO.getIsbn());
			}
			final AbstractOrderEntryModel abstractOrderEntryModel = optionalOrderEntryModelByISBN.get();
			consignmentEntryModel.setOrderEntry(abstractOrderEntryModel);
			consignmentEntryModel.setQuantity(abstractOrderEntryModel.getQuantity());
			result.add(consignmentEntryModel);
		}
		return result;
	}

	private String getConsignmentCode(final ShippingEventWsDTO shippingEventWsDTO, final OrderModel orderModel)
	{
		return orderModel.getCode() + shippingEventWsDTO.getTrackingId() + UUID.randomUUID();
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	public WarehouseService getWarehouseService()
	{
		return warehouseService;
	}

	public void setWarehouseService(final WarehouseService warehouseService)
	{
		this.warehouseService = warehouseService;
	}

	public WileycomWsOrderService getWileycomWsOrderService()
	{
		return wileycomWsOrderService;
	}

	public void setWileycomWsOrderService(final WileycomWsOrderService wileycomWsOrderService)
	{
		this.wileycomWsOrderService = wileycomWsOrderService;
	}

	public ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	public SimpleDateFormat getRfc3339DateFormat()
	{
		return new SimpleDateFormat(getConfigurationService().getConfiguration().
				getString("rfc3339.java.date.format", "yyyy-MM-dd'T'HH:mm:ssXXX"));
	}


}
