/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package com.wiley.ws.core.queues.channel;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.ws.core.queues.UpdateQueue;
import com.wiley.ws.core.queues.data.OrderStatusUpdateElementData;


/**
 * The type Order status update channel listener.
 */
public class OrderStatusUpdateChannelListener
{
	private static final Logger LOG = Logger.getLogger(OrderStatusUpdateChannelListener.class);
	private UpdateQueue<OrderStatusUpdateElementData> orderStatusUpdateQueue;
	private Converter<OrderModel, OrderStatusUpdateElementData> orderStatusUpdateElementConverter;

	/**
	 * On message.
	 *
	 * @param order
	 * 		the order
	 */
	public void onMessage(final OrderModel order)
	{
		LOG.debug("OrderStatusUpdateChannelListener got new status for order with code " + order.getCode());
		final OrderStatusUpdateElementData orderStatusUpdateElementData = getOrderStatusUpdateElementConverter().convert(order);
		getOrderStatusUpdateQueue().addItem(orderStatusUpdateElementData);
	}

	/**
	 * Gets order status update queue.
	 *
	 * @return the order status update queue
	 */
	public UpdateQueue<OrderStatusUpdateElementData> getOrderStatusUpdateQueue()
	{
		return orderStatusUpdateQueue;
	}

	/**
	 * Sets order status update queue.
	 *
	 * @param orderStatusUpdateQueue
	 * 		the order status update queue
	 */
	@Required
	public void setOrderStatusUpdateQueue(final UpdateQueue<OrderStatusUpdateElementData> orderStatusUpdateQueue)
	{
		this.orderStatusUpdateQueue = orderStatusUpdateQueue;
	}

	/**
	 * Gets order status update element converter.
	 *
	 * @return the order status update element converter
	 */
	public Converter<OrderModel, OrderStatusUpdateElementData> getOrderStatusUpdateElementConverter()
	{
		return orderStatusUpdateElementConverter;
	}

	/**
	 * Sets order status update element converter.
	 *
	 * @param orderStatusUpdateElementConverter
	 * 		the order status update element converter
	 */
	@Required
	public void setOrderStatusUpdateElementConverter(
			final Converter<OrderModel, OrderStatusUpdateElementData> orderStatusUpdateElementConverter)
	{
		this.orderStatusUpdateElementConverter = orderStatusUpdateElementConverter;
	}

}
