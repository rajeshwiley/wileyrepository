package com.wiley.ws.core.product.data.hub.dto;

import java.util.Objects;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * MediaSwgDTO asset, for example, an image.
 **/
public class MediaSwgDTO
{

	private String url = null;
	private String altText = null;
	private String mime = null;

	/**
	 * URL of the media asset.
	 **/
	@NotNull
	@Size(max = 5000)
	@JsonProperty("url")
	public String getUrl()
	{
		return url;
	}

	public void setUrl(final String url)
	{
		this.url = url;
	}

	/**
	 * Alternative text for an image.
	 **/
	@Size(max = 255)
	@JsonProperty("altText")
	public String getAltText()
	{
		return altText;
	}

	public void setAltText(final String altText)
	{
		this.altText = altText;
	}

	/**
	 * Mime type of the media asset.
	 **/
	@Size(max = 255)
	@JsonProperty("mime")
	public String getMime()
	{
		return mime;
	}

	public void setMime(final String mime)
	{
		this.mime = mime;
	}


	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		MediaSwgDTO media = (MediaSwgDTO) o;
		return Objects.equals(url, media.url)
				&& Objects.equals(altText, media.altText)
				&& Objects.equals(mime, media.mime);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(url, altText, mime);
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("class MediaSwgDTO {\n");

		sb.append("  url: ").append(url).append("\n");
		sb.append("  altText: ").append(altText).append("\n");
		sb.append("  mime: ").append(mime).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
