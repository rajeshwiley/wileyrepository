package com.wiley.ws.core.common.dto;

import java.util.Objects;

import javax.validation.constraints.Size;


/**
 * Wrapper for string value with size constraint
 */
public class ShortStringSwgDTO
{

	private String value = null;

	public ShortStringSwgDTO(final String value)
	{
		this.value = value;
	}

	@Size(max = 255)
	public String getValue()
	{
		return value;
	}

	public void setValue(final String value)
	{
		this.value = value;
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		ShortStringSwgDTO shortStringSwgDTO = (ShortStringSwgDTO) o;
		return Objects.equals(value, shortStringSwgDTO.value);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(value);
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("class ShortStringSwgDTO {\n");
		sb.append("  value: ").append(value).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
