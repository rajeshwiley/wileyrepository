package com.wiley.ws.core.order.dto;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Author Herman_Chukhrai (EPAM)
 */
public class DiscountWsDTO
{
	/**
	 * name or code of a provided discount. Name should be in language specified int the request if it is presented.
	 * Otherwise English is a default language in the system.
	 */
	private String name;
	/**
	 * a flag that 'value' field contains an absolute value
	 */
	private Boolean absolute;
	/**
	 * a value of provided discount
	 */
	private Double value;

	@JsonProperty("name")
	public String getName()
	{
		return name;
	}

	public void setName(final String name)
	{
		this.name = name;
	}

	@JsonProperty("absolute")
	public Boolean getAbsolute()
	{
		return absolute;
	}

	public void setAbsolute(final Boolean absolute)
	{
		this.absolute = absolute;
	}

	@JsonProperty("value")
	public Double getValue()
	{
		return value;
	}

	public void setValue(final Double value)
	{
		this.value = value;
	}


	@Override
	public String toString()
	{
		return new ToStringBuilder(this)
				.append("name", name)
				.append("value", value)
				.toString();
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}

		if (o == null || getClass() != o.getClass())
		{
			return false;
		}

		final DiscountWsDTO that = (DiscountWsDTO) o;

		return new EqualsBuilder()
				.append(name, that.name)
				.append(value, that.value)
				.isEquals();
	}

	@Override
	public int hashCode()
	{
		return new HashCodeBuilder(17, 37)
				.append(name)
				.append(value)
				.toHashCode();
	}
}
