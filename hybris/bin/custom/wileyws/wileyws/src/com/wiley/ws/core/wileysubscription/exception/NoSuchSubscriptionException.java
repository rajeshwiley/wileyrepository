package com.wiley.ws.core.wileysubscription.exception;

/**
 * Created by Mikhail_Asadchy on 10/14/2016.
 */
public class NoSuchSubscriptionException extends RuntimeException
{

	public NoSuchSubscriptionException()
	{
	}

	public NoSuchSubscriptionException(final String message)
	{
		super(message);
	}

	public NoSuchSubscriptionException(final String message, final Throwable cause)
	{
		super(message, cause);
	}

	public NoSuchSubscriptionException(final Throwable cause)
	{
		super(cause);
	}

	public NoSuchSubscriptionException(final String message, final Throwable cause, final boolean enableSuppression,
			final boolean writableStackTrace)
	{
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
