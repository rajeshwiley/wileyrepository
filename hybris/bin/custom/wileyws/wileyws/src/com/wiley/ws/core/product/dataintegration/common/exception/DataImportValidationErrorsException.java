package com.wiley.ws.core.product.dataintegration.common.exception;

import com.wiley.ws.core.product.dataintegration.common.error.ErrorListSwgDTO;
import com.wiley.ws.core.product.dataintegration.common.error.ErrorSwgDTO;

import static com.wiley.ws.core.product.dataintegration.common.error.ErrorSwgDTO.ReasonEnum.BAD_REFERENCE_ERROR;
import static com.wiley.ws.core.product.dataintegration.common.error.ErrorSwgDTO.ReasonEnum.IMMUTABLE_FIELD_ERROR;
import static com.wiley.ws.core.product.dataintegration.common.error.ErrorSwgDTO.TypeEnum.TRANSFORMATION;
import static com.wiley.ws.core.product.dataintegration.common.error.ErrorSwgDTO.TypeEnum.VALIDATION;


/**
 * Thrown when validation error occurs for ProductCrud operation in WS extension
 *
 * @author ygalav
 */
public class DataImportValidationErrorsException extends RuntimeException
{
	private ErrorListSwgDTO errors;

	public DataImportValidationErrorsException(final ErrorListSwgDTO errors)
	{
		this.errors = errors;
	}

	public ErrorListSwgDTO getErrors()
	{
		return errors;
	}

	public void setErrors(final ErrorListSwgDTO errors)
	{
		this.errors = errors;
	}

	public static DataImportValidationErrorsException ofNoReferenceException(final String subjectType, final String subject)
	{
		final ErrorSwgDTO error = ErrorSwgDTO.of(TRANSFORMATION, BAD_REFERENCE_ERROR, "The reference can't be resolved",
				subjectType, subject);
		return new DataImportValidationErrorsException(ErrorListSwgDTO.of(error));
	}

	public static DataImportValidationErrorsException ofImmutableFieldException(final String subjectType, final String subject)
	{
		final ErrorSwgDTO error = ErrorSwgDTO.of(VALIDATION, IMMUTABLE_FIELD_ERROR,
				"The field is immutable, it can't be changed after initialization", subjectType, subject);
		return new DataImportValidationErrorsException(ErrorListSwgDTO.of(error));
	}
}
