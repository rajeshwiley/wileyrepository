package com.wiley.ws.core.order.dto;


/**
 * Created by Artyom_Bazyk on 5/3/2017.
 */
public class FreeTrialResponseWsDTO
{
	private String orderId;

	private String magicLink;

	public String getMagicLink()
	{
		return magicLink;
	}

	public void setMagicLink(final String magicLink)
	{
		this.magicLink = magicLink;
	}

	public String getOrderId()
	{
		return orderId;
	}

	public void setOrderId(final String orderId)
	{
		this.orderId = orderId;
	}

}
