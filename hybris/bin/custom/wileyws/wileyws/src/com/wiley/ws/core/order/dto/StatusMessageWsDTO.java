package com.wiley.ws.core.order.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.validation.constraints.NotNull;


/**
 * Created by Anton_Lukyanau on 7/28/2016.
 */
public class StatusMessageWsDTO
{

	private String code;
	@NotNull
	private String message;


	/**
	 * Code of the order processing event.
	 **/
	public StatusMessageWsDTO code(final String code) {
		this.code = code;
		return this;
	}

	@JsonProperty("code")
	public String getCode() {
		return code;
	}
	public void setCode(final String code) {
		this.code = code;
	}


	/**
	 * Displayable description of the order processing event. This message is not localized. For internal usage only.
	 **/
	public StatusMessageWsDTO message(final String message) {
		this.message = message;
		return this;
	}

	@JsonProperty("message")
	public String getMessage() {
		return message;
	}
	public void setMessage(final String message) {
		this.message = message;
	}



	@Override
	public String toString()
	{
		return new ToStringBuilder(this)
				.append("code", code)
				.append("message", message)
				.toString();
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}

		if (o == null || getClass() != o.getClass())
		{
			return false;
		}

		final StatusMessageWsDTO that = (StatusMessageWsDTO) o;

		return new EqualsBuilder()
				.append(code, that.code)
				.append(message, that.message)
				.isEquals();
	}

	@Override
	public int hashCode()
	{
		return new HashCodeBuilder(17, 37)
				.append(code)
				.append(message)
				.toHashCode();
	}
}
