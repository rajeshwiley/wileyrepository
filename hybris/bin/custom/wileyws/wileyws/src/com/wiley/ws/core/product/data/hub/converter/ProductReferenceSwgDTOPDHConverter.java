package com.wiley.ws.core.product.data.hub.converter;

import de.hybris.platform.catalog.model.ProductReferenceModel;

import com.wiley.core.model.WileyProductModel;
import com.wiley.ws.core.product.data.hub.dto.ProductReferenceSwgDTO;


/**
 * ProductReferenceSwgDTO to ProductReferenceModel converter
 */
public interface ProductReferenceSwgDTOPDHConverter
{
	ProductReferenceModel convertToProductRefereceModel(ProductReferenceSwgDTO reference,
			WileyProductModel sourceProduct);
}
