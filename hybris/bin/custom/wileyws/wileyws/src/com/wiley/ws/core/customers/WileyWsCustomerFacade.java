package com.wiley.ws.core.customers;

public interface WileyWsCustomerFacade
{
	String getCustomerId(String customerUid);
}
