package com.wiley.ws.core.order.exception;

import de.hybris.platform.servicelayer.exceptions.SystemException;


/**
 * Created by Georgii_Gavrysh on 9/2/2016.
 */
public class WileywsValidationException extends SystemException
{
	private String message;

	public WileywsValidationException()
	{
		super("");
	}

	public WileywsValidationException(final String message)
	{
		super(message);
	}

	public WileywsValidationException(final Throwable cause)
	{
		super(cause);
	}

	@Override
	public String getMessage()
	{
		return message;
	}

	public void setMessage(final String message)
	{
		this.message = message;
	}
}
