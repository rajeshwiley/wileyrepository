package com.wiley.ws.core.product.crud.converter.populator.variantproduct;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.classification.features.FeatureValue;
import de.hybris.platform.classification.features.LocalizedFeature;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.ws.core.product.crud.converter.populator.LocalizedValuesNullationService;
import com.wiley.ws.core.product.crud.dto.WileyVariantProductWsDto;
import com.wiley.ws.core.product.crud.facade.WileyProductCrudCatalogVersionProvider;
import com.wiley.ws.core.product.dataintegration.common.dto.LocalizedDateSwgDTO;
import com.wiley.ws.core.product.dataintegration.common.dto.LocalizedStringSwgDTO;

import static com.wiley.core.wileyb2c.classification.Wileyb2cClassificationAttributes.IMPACT_FACTOR;
import static com.wiley.core.wileyb2c.classification.Wileyb2cClassificationAttributes.ISBN13;
import static com.wiley.core.wileyb2c.classification.Wileyb2cClassificationAttributes.NUMBER_OF_PAGES;
import static com.wiley.core.wileyb2c.classification.Wileyb2cClassificationAttributes.ONLINE_ISSN;
import static com.wiley.core.wileyb2c.classification.Wileyb2cClassificationAttributes.PRINT_ISSN;
import static com.wiley.core.wileyb2c.classification.Wileyb2cClassificationAttributes.PUBLICATION_DATE;
import static com.wiley.core.wileyb2c.classification.Wileyb2cClassificationAttributes.SOCIETY_LINK;
import static com.wiley.core.wileyb2c.classification.Wileyb2cClassificationAttributes.VOLUME_AND_ISSUE;
import static org.apache.commons.lang.LocaleUtils.toLocale;


public class ClassificationAttributesPopulator implements Populator<WileyVariantProductWsDto, FeatureList>
{
	private final LocalizedValuesNullationService<LocalizedFeature> localizedValuesNullationService;

	@Autowired
	private CategoryService categoryService;

	@Autowired
	private WileyProductCrudCatalogVersionProvider wileyProductCrudCatalogVersionProvider;

	@Autowired
	public ClassificationAttributesPopulator(
			final LocalizedValuesNullationService<LocalizedFeature> localizedValuesNullationService)
	{
		this.localizedValuesNullationService = localizedValuesNullationService;
	}

	@Override
	public void populate(final WileyVariantProductWsDto variantProductWsDto, final FeatureList featureList)
			throws ConversionException
	{
		final String classCode = variantProductWsDto.getPurchaseOption();
		resetAllFeatureValues(featureList);

		final Map<String, Object> wsFeaturesValueMap = getFeaturesValueMap(variantProductWsDto);
		final CatalogVersionModel classificationCatalogVersion =
				wileyProductCrudCatalogVersionProvider.getClassificationCatalogVersion();
		final ClassificationClassModel classificationClass = (ClassificationClassModel) categoryService
				.getCategoryForCode(classificationCatalogVersion, classCode);
		final Iterator<ClassAttributeAssignmentModel> iterator = classificationClass
				.getAllClassificationAttributeAssignments().iterator();

		while (iterator.hasNext())
		{
			final ClassAttributeAssignmentModel assignment = iterator.next();
			final Feature feature = featureList.getFeatureByAssignment(assignment);
			if (feature != null)
			{
				final Object featureValue = wsFeaturesValueMap
						.get(feature.getClassAttributeAssignment().getClassificationAttribute().getCode().toLowerCase());
				addFeature(feature, featureValue);
			}
		}
	}

	private void addFeature(final Feature feature, final Object featureValue)
	{
		if (featureValue != null)
		{
			if (feature instanceof LocalizedFeature)
			{
				addLocalizedFeatureValue(feature, featureValue);
			}
			else
			{
				feature.addValue(new FeatureValue(featureValue));
			}
		}
	}

	private void addLocalizedFeatureValue(final Feature feature, final Object localizedFeatureValue)
	{
		final LocalizedFeature localizedFeature = (LocalizedFeature) feature;
		if (localizedFeatureValue instanceof Collection)
		{
			for (final Object localizedFeatureDto : (Collection) localizedFeatureValue)
			{
				if (localizedFeatureDto instanceof LocalizedDateSwgDTO)
				{
					final LocalizedDateSwgDTO localizedDateSwgDTO = (LocalizedDateSwgDTO) localizedFeatureDto;
					localizedFeature.addValue(new FeatureValue(localizedDateSwgDTO.getDatetime()),
							toLocale(localizedDateSwgDTO.getLocale()));
				}
				else if (localizedFeatureDto instanceof LocalizedStringSwgDTO)
				{
					final LocalizedStringSwgDTO localizedStringSwgDTO = (LocalizedStringSwgDTO) localizedFeatureDto;
					localizedFeature.addValue(new FeatureValue(localizedStringSwgDTO.getValue()),
							toLocale(localizedStringSwgDTO.getLocale()));
				}
			}
		}
		else
		{
			localizedFeature.addValue(new FeatureValue(localizedFeatureValue));
		}
	}

	private void resetAllFeatureValues(final FeatureList featureList)
	{
		for (final Feature feature : featureList.getFeatures())
		{
			if (feature instanceof LocalizedFeature)
			{
				localizedValuesNullationService.resetValue((LocalizedFeature) feature,
						(localizedFeature, locale) -> localizedFeature.removeAllValues(locale));
			}
			else
			{
				feature.removeAllValues();
			}
		}
	}

	private Map<String, Object> getFeaturesValueMap(final WileyVariantProductWsDto variantProductWsDto)
	{
		final Map<String, Object> wsFeaturesValueMap = new HashMap<>();
		wsFeaturesValueMap.put(ISBN13.getCode().toLowerCase(), variantProductWsDto.getIsbn13());
		wsFeaturesValueMap.put(NUMBER_OF_PAGES.getCode().toLowerCase(), variantProductWsDto.getNumberOfPages());
		wsFeaturesValueMap.put(PRINT_ISSN.getCode().toLowerCase(), variantProductWsDto.getPrintIssn());
		wsFeaturesValueMap.put(ONLINE_ISSN.getCode().toLowerCase(), variantProductWsDto.getOnlineIssn());
		wsFeaturesValueMap.put(IMPACT_FACTOR.getCode().toLowerCase(), variantProductWsDto.getImpactFactor());
		wsFeaturesValueMap.put(VOLUME_AND_ISSUE.getCode().toLowerCase(), variantProductWsDto.getVolumeAndIssue());
		wsFeaturesValueMap.put(SOCIETY_LINK.getCode().toLowerCase(), variantProductWsDto.getSocietyLink());
		wsFeaturesValueMap.put(PUBLICATION_DATE.getCode().toLowerCase(), variantProductWsDto.getPublicationDate());
		return wsFeaturesValueMap;
	}
}
