package com.wiley.ws.core.product.dataintegration.common.dto;

import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.wiley.core.validation.constraints.ValidLocale;

/**
 * Localized version of date-time.
 */
public class LocalizedDateSwgDTO
{
	@NotBlank
	@ValidLocale
	private String locale = null;

	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
	private Date datetime = null;

	public String getLocale()
	{
		return locale;
	}

	public void setLocale(final String locale)
	{
		this.locale = locale;
	}

	public Date getDatetime()
	{
		return datetime;
	}

	public void setDatetime(final Date datetime)
	{
		this.datetime = datetime;
	}

	@Override
	public String toString()
	{
		return new ToStringBuilder(this)
				.append("locale", locale)
				.append("datetime", datetime)
				.toString();
	}
}

