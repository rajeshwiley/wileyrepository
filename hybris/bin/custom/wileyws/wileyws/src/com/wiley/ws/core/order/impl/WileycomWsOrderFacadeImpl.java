package com.wiley.ws.core.order.impl;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.wiley.core.wileyws.order.WileycomWsOrderHistoryService;
import com.wiley.core.wileyws.order.WileycomWsOrderService;
import com.wiley.ws.core.order.ShippedQuantityExcessValidator;
import com.wiley.ws.core.order.WileycomWsConsignmentTransformer;
import com.wiley.ws.core.order.WileycomWsOrderFacade;
import com.wiley.ws.core.order.dto.OrderStatusWsDTO;
import com.wiley.ws.core.order.dto.OrderUpdateRequestWsDTO;
import com.wiley.ws.core.order.dto.ShippingEventWsDTO;
import com.wiley.ws.core.order.dto.StatusMessageWsDTO;
import com.wiley.ws.core.order.exception.OrderWsNotFoundException;


/**
 * The type Wileycom ws order facade.
 */
public class WileycomWsOrderFacadeImpl implements WileycomWsOrderFacade
{
	private static final Logger LOG = LoggerFactory.getLogger(WileycomWsOrderFacadeImpl.class);

	@Resource(name = "wileycomWsOrderService")
	private WileycomWsOrderService wileycomWsOrderService;

	@Resource
	private Populator<OrderUpdateRequestWsDTO, AbstractOrderModel> wileycomOrderReversePopulator;

	@Resource(name = "wileycomWsConsignmentTransformer")
	private WileycomWsConsignmentTransformer wileycomWsConsignmentTransformer;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "shippedQuantityExcessValidator")
	private ShippedQuantityExcessValidator shippedQuantityExcessValidator;

	@Resource
	private WileycomWsOrderHistoryService wileycomWsOrderHistoryService;


	@Override
	public void updateStatusAndAddHistoryEntry(final String code, final OrderStatusWsDTO orderStatusWsDTO)
	{
		OrderModel orderModel = findOrderByCode(code);
		OrderStatus orderStatus = orderStatusWsDTO.getStatus().getValue();
		String message = createMessage(orderStatusWsDTO.getMessage());
		wileycomWsOrderService.updateOrderStatusAndAddHistoryEntry(orderModel, orderStatus, message);
	}


	@Override
	public void updateShipping(@NotNull final String orderCode, @NotNull final ShippingEventWsDTO shippingEventWsDTO)
	{
		final OrderModel orderModel = findOrderByCode(orderCode);
		ConsignmentModel consignment;
		consignment = getWileycomWsConsignmentTransformer().createConsignment(shippingEventWsDTO,
					orderModel);
		final Set<ConsignmentModel> consignments = new HashSet<>(orderModel.getConsignments());
		consignments.add(consignment);
		orderModel.setConsignments(consignments);
		getShippedQuantityExcessValidator().validate(orderModel);
		getModelService().saveAll(consignments);
		getModelService().save(orderModel);
		LOG.debug(String.format("Order with updated shipping has been updated. Order code = [%s]", orderModel.getCode()));
	}

	@Override
	@Transactional
	public void updateOrder(final String orderCode, final OrderUpdateRequestWsDTO orderUpdateRequestWsDTO)
	{
		final OrderModel orderModel = findOrderByCode(orderCode);

		OrderModel snapshot = wileycomWsOrderHistoryService.createHistorySnapshot(orderModel);
		wileycomWsOrderHistoryService.saveHistorySnapshot(snapshot);

		wileycomOrderReversePopulator.populate(orderUpdateRequestWsDTO, orderModel);
		final String historyMessage = createOrderMessage(orderUpdateRequestWsDTO);
		wileycomWsOrderService.updateOrderAndAddHistoryEntry(orderModel, snapshot, historyMessage);
	}

	private String createOrderMessage(final OrderUpdateRequestWsDTO orderDTO)
	{
		return CollectionUtils.isNotEmpty(orderDTO.getMessages()) ?
				orderDTO.getMessages().stream()
						.map(message -> String.format("[%s]: [%s] (Code: [%s], Product: [%s])",
								message.getMessageType().name(), message.getMessage(), message.getCode(), message.getIsbn()))
						.collect(Collectors.joining("\n")) : null;
	}

	protected OrderModel findOrderByCode(final String orderCode)
	{
		try
		{
			return wileycomWsOrderService.findOrderByCode(orderCode);
		}
		catch (UnknownIdentifierException e)
		{
			throw new OrderWsNotFoundException(e, orderCode);
		}
	}

	private String createMessage(final StatusMessageWsDTO messageWsDTO)
	{
		StringBuilder message = new StringBuilder(messageWsDTO.getMessage());
		return StringUtils.isEmpty(messageWsDTO.getCode()) ? "" : message.append(" (")
				.append(messageWsDTO.getCode()).append(")").toString();
	}

	public WileycomWsConsignmentTransformer getWileycomWsConsignmentTransformer()
	{
		return wileycomWsConsignmentTransformer;
	}

	public void setWileycomWsConsignmentTransformer(
			final WileycomWsConsignmentTransformer wileycomWsConsignmentTransformer)
	{
		this.wileycomWsConsignmentTransformer = wileycomWsConsignmentTransformer;
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	public ShippedQuantityExcessValidator getShippedQuantityExcessValidator()
	{
		return shippedQuantityExcessValidator;
	}

	public void setShippedQuantityExcessValidator(final ShippedQuantityExcessValidator shippedQuantityExcessValidator)
	{
		this.shippedQuantityExcessValidator = shippedQuantityExcessValidator;
	}
}
