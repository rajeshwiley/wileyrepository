package com.wiley.ws.core.cart;

import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceSaveCartException;

import java.util.Collection;

import com.wiley.core.enums.OrderType;
import com.wiley.integrations.order.dto.OrderEntryWsDTO;
import com.wiley.ws.core.cart.dto.CreateCartEntryRequestWsDTO;
import com.wiley.ws.core.cart.dto.CreateCartRequestWsDTO;
import com.wiley.ws.core.cart.dto.WileyCartWsDTO;
import com.wiley.ws.core.exceptions.carts.WileyasSaveCartException;


/**
 * The interface Wiley AS ws cart facade.
 */
public interface WileyasWsCartFacade
{

	/**
	 * @param request
	 * 		{@link CreateCartRequestWsDTO} parameter object that holds the cart type to be created along
	 * 		with some additional details such as country, contact address, payment address etc.
	 * @return {@link WileyCartWsDTO}
	 * @throws CommerceSaveCartException
	 * 		if cart cannot be saved
	 */
	WileyCartWsDTO createCart(CreateCartRequestWsDTO request)
			throws CommerceSaveCartException;

	/**
	 * Checks whether hybris has cart for {@link CreateCartRequestWsDTO#getType()}, current site and current user.
	 * Method guarantees existance of only one cart in database and deletes all duplicates.
	 * Method leaves the older cart by PK comparison.
	 *
	 * @param request
	 * 		{@link CreateCartRequestWsDTO} parameter object that holds the cart type to be created along
	 * 		with some additional details such as country, contact address, payment address etc.
	 * @param isNewCartCreated
	 * 		defines whether one cart was created in this thread or not
	 * @return {@link WileyCartWsDTO}
	 * @throws WileyasSaveCartException
	 * 		if cart already exists
	 */
	void ensureSingleCart(CreateCartRequestWsDTO request, boolean isNewCartCreated)
			throws WileyasSaveCartException;

	/**
	 * Returns all created carts for current user with given typ
	 *
	 * @param type
	 * @return
	 */
	Collection<WileyCartWsDTO> getUserCartsForType(OrderType type);

	/**
	 * Returns all created carts for current user with given typ
	 *
	 * @param cartId
	 * @return
	 */
	WileyCartWsDTO getUserCart(String cartId);

	/**
	 * Method adds new entry to the cart
	 *
	 * @param cartId
	 * 		{@link String} parameter of the cart id.
	 * @return {@link OrderEntryWsDTO}
	 * @throws CommerceCartModificationException
	 * 		if cart cannot be modified.
	 */
	OrderEntryWsDTO addEntryToCart(String cartId, CreateCartEntryRequestWsDTO cartEntryDTO)
			throws CommerceCartModificationException;

	/**
	 * @param baseSiteId
	 * 		{@link String} parameter of the baseSite id.
	 * @param userId
	 * 		{@link String} parameter of the user id.
	 * @param cartId
	 * 		{@link String} parameter of the cart id.
	 */
	void deleteUserCart(String baseSiteId, String userId, String cartId);
}
