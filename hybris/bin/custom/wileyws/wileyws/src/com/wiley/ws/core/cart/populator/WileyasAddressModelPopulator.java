package com.wiley.ws.core.cart.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.validation.constraints.NotNull;

import com.wiley.ws.core.cart.dto.CartAddressWsDTO;

import static com.wiley.facades.populators.PopulateUtils.populateFieldIfNotNull;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


/**
 * Populates {AddressModel} to {CartAddressWsDTO}
 */
public class WileyasAddressModelPopulator implements Populator<AddressModel, CartAddressWsDTO>
{
	@Override
	public void populate(@NotNull final AddressModel source, @NotNull final CartAddressWsDTO target) throws ConversionException
	{
		validateParameterNotNullStandardMessage("source", source);
		validateParameterNotNullStandardMessage("target", target);

		populateFieldIfNotNull(CartAddressWsDTO::setPostcode, target, source.getPostalcode());
		populateFieldIfNotNull(this::populateCountry, target, source.getCountry());
		populateFieldIfNotNull(this::populateRegion, target, source.getRegion());

		populateFieldIfNotNull(CartAddressWsDTO::setCity, target, source.getTown());
		populateFieldIfNotNull(CartAddressWsDTO::setLine1, target, source.getLine1());
		populateFieldIfNotNull(CartAddressWsDTO::setLine2, target, source.getLine2());
		populateFieldIfNotNull(CartAddressWsDTO::setFirstName, target, source.getFirstname());
		populateFieldIfNotNull(CartAddressWsDTO::setLastName, target, source.getLastname());
		populateFieldIfNotNull(CartAddressWsDTO::setPhoneNumber, target, source.getPhone1());
		populateFieldIfNotNull(CartAddressWsDTO::setEmail, target, source.getEmail());
		populateFieldIfNotNull(CartAddressWsDTO::setOrganization, target, source.getCompany());
		populateFieldIfNotNull(CartAddressWsDTO::setDepartment, target, source.getDepartment());
	}

	private void populateCountry(final CartAddressWsDTO target, final CountryModel countryModel)
	{
		target.setCountry(countryModel.getIsocode());
	}

	private void populateRegion(final CartAddressWsDTO target, final RegionModel regionModel)
	{
		target.setState(regionModel.getIsocodeShort());
	}
}
