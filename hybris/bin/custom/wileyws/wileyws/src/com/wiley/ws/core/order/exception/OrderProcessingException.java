package com.wiley.ws.core.order.exception;

public class OrderProcessingException extends RuntimeException
{
	public OrderProcessingException()
	{
	}

	public OrderProcessingException(final String message)
	{
		super(message);
	}

	public OrderProcessingException(final String message, final Throwable cause)
	{
		super(message, cause);
	}

	public OrderProcessingException(final Throwable cause)
	{
		super(cause);
	}
}
