package com.wiley.ws.core.abstractorderentry;

import java.util.Optional;

import com.wiley.integrations.order.dto.OrderEntryWsDTO;


public interface WileyWsAbstractOrderEntryFacade
{
	Optional<OrderEntryWsDTO> getEntryForBusinessKey(String businessKey);

	OrderEntryWsDTO getEntryDetails(String baseSiteId, String entryId);
}
