package com.wiley.ws.core.inventory.dto;

import java.util.Date;

import javax.annotation.Nonnegative;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Created by Raman_Hancharou on 4/3/2017.
 */
public class InventoryUpdateRequestWsDTO
{
	private String warehouseId;
	private String productId;
	private int quantity;
	private Date nextAvailabilityDate;
	private Long sequenceId;

	@NotNull
	@Size(max = 255)
	@JsonProperty("warehouseId")
	public String getWarehouseId()
	{
		return warehouseId;
	}

	public void setWarehouseId(final String warehouseId)
	{
		this.warehouseId = warehouseId;
	}

	@NotNull
	@Size(max = 255)
	@JsonProperty("productId")
	public String getProductId()
	{
		return productId;
	}

	public void setProductId(final String productId)
	{
		this.productId = productId;
	}

	@NotNull
	@Nonnegative
	@JsonProperty("quantity")
	public int getQuantity()
	{
		return quantity;
	}

	public void setQuantity(final int quantity)
	{
		this.quantity = quantity;
	}

	@JsonFormat(pattern = "yyyy-MM-dd")
	@JsonProperty("nextAvailabilityDate")
	public Date getNextAvailabilityDate()
	{
		return nextAvailabilityDate;
	}

	public void setNextAvailabilityDate(final Date nextAvailabilityDate)
	{
		this.nextAvailabilityDate = nextAvailabilityDate;
	}

	@Nonnegative
	@JsonProperty("sequenceId")
	public Long getSequenceId()
	{
		return sequenceId;
	}

	public void setSequenceId(final Long sequenceId)
	{
		this.sequenceId = sequenceId;
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}

		if (o == null || getClass() != o.getClass())
		{
			return false;
		}

		final InventoryUpdateRequestWsDTO that = (InventoryUpdateRequestWsDTO) o;

		return new EqualsBuilder()
				.append(quantity, that.quantity)
				.append(warehouseId, that.warehouseId)
				.append(productId, that.productId)
				.append(nextAvailabilityDate, that.nextAvailabilityDate)
				.append(sequenceId, that.sequenceId)
				.isEquals();
	}

	@Override
	public int hashCode()
	{
		return new HashCodeBuilder(17, 37)
				.append(warehouseId)
				.append(productId)
				.append(quantity)
				.append(nextAvailabilityDate)
				.append(sequenceId)
				.toHashCode();
	}

	@Override
	public String toString()
	{
		return new ToStringBuilder(this)
				.append("warehouseId", warehouseId)
				.append("productId", productId)
				.append("quantity", quantity)
				.append("nextAvailabilityDate", nextAvailabilityDate)
				.append("sequenceId", sequenceId)
				.toString();
	}
}
