package com.wiley.ws.core.transaction.cache;

import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalListeners;
import com.wiley.ws.core.transaction.WileyWsTestTransaction;


/**
 * Created by Maksim_Kozich on 17.11.2016.
 */
public class TransactionCacheFactory
{
	private int transactionExpirationTimeoutSeconds;

	private int transactionMaximumSize;

	@Resource
	private RemovalListener<String, WileyWsTestTransaction> removalListener;

	public Cache<String, WileyWsTestTransaction> getCache() throws Exception
	{
		return CacheBuilder.newBuilder()
				.maximumSize(transactionMaximumSize)
				.expireAfterAccess(transactionExpirationTimeoutSeconds, TimeUnit.SECONDS)
				.removalListener(
						RemovalListeners.asynchronous(removalListener, Executors.newFixedThreadPool(transactionMaximumSize)))
				.build();
	}

	public int getTransactionExpirationTimeoutSeconds()
	{
		return transactionExpirationTimeoutSeconds;
	}

	public void setTransactionExpirationTimeoutSeconds(final int transactionExpirationTimeoutSeconds)
	{
		this.transactionExpirationTimeoutSeconds = transactionExpirationTimeoutSeconds;
	}

	public int getTransactionMaximumSize()
	{
		return transactionMaximumSize;
	}

	public void setTransactionMaximumSize(final int transactionMaximumSize)
	{
		this.transactionMaximumSize = transactionMaximumSize;
	}
}
