package com.wiley.ws.core.product.dataintegration.category.dto;

import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.wiley.ws.core.product.dataintegration.common.dto.LocalizedStringSwgDTO;


public class UpdateCategorySwgDTO
{
	private List<LocalizedStringSwgDTO> name;
	private List<String> supercategories;

	public List<LocalizedStringSwgDTO> getName()
	{
		return name;
	}

	public void setName(final List<LocalizedStringSwgDTO> name)
	{
		this.name = name;
	}

	public List<String> getSupercategories()
	{
		return supercategories;
	}

	public void setSupercategories(final List<String> supercategories)
	{
		this.supercategories = supercategories;
	}

	@Override
	public String toString()
	{
		return new ToStringBuilder(this)
				.append("name", name)
				.append("supercategories", supercategories)
				.toString();
	}
}
