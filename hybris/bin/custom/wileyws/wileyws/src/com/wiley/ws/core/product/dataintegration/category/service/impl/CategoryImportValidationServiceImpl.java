package com.wiley.ws.core.product.dataintegration.category.service.impl;

import de.hybris.platform.validation.services.ValidationService;

import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.category.WileyCategoryService;
import com.wiley.core.validation.service.ConstraintGroupService;
import com.wiley.ws.core.product.crud.facade.WileyProductCrudCatalogVersionProvider;
import com.wiley.ws.core.product.dataintegration.common.error.ErrorListSwgDTO;
import com.wiley.ws.core.product.dataintegration.common.error.ErrorSwgDTO;
import com.wiley.ws.core.product.dataintegration.common.exception.DataImportValidationErrorsException;
import com.wiley.ws.core.product.dataintegration.common.service.impl.DataImportValidationServiceImpl;

import static com.wiley.ws.core.product.dataintegration.common.error.ErrorSwgDTO.ReasonEnum.NOT_UNIQUE_ID;
import static com.wiley.ws.core.product.dataintegration.common.error.ErrorSwgDTO.TypeEnum.VALIDATION;


public class CategoryImportValidationServiceImpl extends DataImportValidationServiceImpl
{
	private static final String SUBJECT_TYPE_CATEGORY = "Category";

	private final WileyCategoryService categoryService;
	private final WileyProductCrudCatalogVersionProvider wileyProductCrudCatalogVersionProvider;

	@Autowired
	public CategoryImportValidationServiceImpl(final ValidationService validationService,
			final ConstraintGroupService constraintGroupService, final WileyCategoryService categoryService,
			final WileyProductCrudCatalogVersionProvider wileyProductCrudCatalogVersionProvider)
	{
		super(validationService, constraintGroupService);
		this.categoryService = categoryService;
		this.wileyProductCrudCatalogVersionProvider = wileyProductCrudCatalogVersionProvider;
	}

	@Override
	public void validateItemDoesNotExist(final String code)
	{

		if (categoryService.isCategoryForCodeExists(wileyProductCrudCatalogVersionProvider.getCatalogVersion(), code))
		{
			final ErrorSwgDTO error = ErrorSwgDTO.of(VALIDATION, NOT_UNIQUE_ID, MESSAGE_ITEM_NOT_UNIQUE, SUBJECT_TYPE_CATEGORY,
					SUBJECT_ID);
			throw new DataImportValidationErrorsException(ErrorListSwgDTO.of(error));
		}

	}
}
