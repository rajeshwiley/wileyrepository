package com.wiley.ws.core.product.data.hub.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.ws.core.product.data.hub.converter.ProductPriceSwgDTOPDHConverter;
import com.wiley.ws.core.product.data.hub.dto.ProductSwgDTO;



/**
 * Populate {@link ProductSwgDTO#getPrices()} to {@link ProductModel#setEurope1Prices(Collection)}
 */
public class PricePDHPopulator implements Populator<ProductSwgDTO, ProductModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(PricePDHPopulator.class);

	@Autowired
	private ModelService modelService;

	@Autowired
	private ProductPriceSwgDTOPDHConverter productPriceSwgDTOPDHConverter;

	@Override
	public void populate(final ProductSwgDTO product, final ProductModel productModel) throws ConversionException
	{
		if (product.getPrices() != null)
		{
			List<PriceRowModel> priceRows = product.getPrices().stream().map(price -> productPriceSwgDTOPDHConverter
					.convertToPriceRowModel(price, productModel.getCode()))
					.collect(Collectors.toList());

			if (CollectionUtils.isNotEmpty(productModel.getEurope1Prices()))
			{
				LOG.debug("Removing old product prices [{}]", productModel.getEurope1Prices());
				modelService.removeAll(productModel.getEurope1Prices());
			}
			productModel.setEurope1Prices(priceRows);
		}
		else
		{
			LOG.debug("Skip populating of product prices due to null");
		}
	}

}