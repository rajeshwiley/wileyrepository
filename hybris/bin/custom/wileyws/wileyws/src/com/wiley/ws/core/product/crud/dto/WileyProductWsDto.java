package com.wiley.ws.core.product.crud.dto;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.wiley.ws.core.product.dataintegration.common.dto.LocalizedBooleanSwgDTO;
import com.wiley.ws.core.product.dataintegration.common.dto.LocalizedLongStringSwgDTO;
import com.wiley.ws.core.product.dataintegration.common.dto.LocalizedStringSwgDTO;


/**
 * BaseProduct
 */
public class WileyProductWsDto
{
	@Valid
	private List<LocalizedStringSwgDTO> name;

	@Valid
	private List<LocalizedLongStringSwgDTO> description = new ArrayList<>();

	@Valid
	private List<AuthorInfoWsDto> authorInfos = new ArrayList<>();

	private String textLanguage = null;

	@Valid
	private List<LocalizedLongStringSwgDTO> keywords = new ArrayList<>();

	@Valid
	private List<LocalizedBooleanSwgDTO> textbook = null;

	private Boolean bestSeller = null;

	private ImageContainerWsDto coverImage = null;

	@Valid
	private List<DownloadableContentWsDto> excerpts = new ArrayList<>();

	@Valid
	private List<LocalizedLongStringSwgDTO> downloadsTab = new ArrayList<>();

	@Valid
	private List<LocalizedLongStringSwgDTO> aboutAuthors = new ArrayList<>();

	@Valid
	private List<LocalizedLongStringSwgDTO> pressRelease = new ArrayList<>();

	@Valid
	private List<LocalizedLongStringSwgDTO> newToEdition = new ArrayList<>();

	@Valid
	private List<LocalizedLongStringSwgDTO> whatsNew = new ArrayList<>();

	@Valid
	private List<LocalizedLongStringSwgDTO> relatedWebsites = new ArrayList<>();

	@Valid
	private List<LocalizedLongStringSwgDTO> notes = new ArrayList<>();

	@Valid
	private List<LocalizedLongStringSwgDTO> reviews = new ArrayList<>();

	@Valid
	private List<LocalizedLongStringSwgDTO> errata = new ArrayList<>();

	@Valid
	private List<LocalizedLongStringSwgDTO> tableOfContents = new ArrayList<>();

	private List<String> subjectIds = new ArrayList<>();

	private List<String> courseIds = new ArrayList<>();

	private List<String> brandIds = new ArrayList<>();

	private List<String> seriesIds = new ArrayList<>();

	@Valid
	private List<WebLinkWsDto> webLinks = new ArrayList<>();

	/**
	 * The title (display name) of the product. Max length of the field is 255 characters.
	 *
	 * @return name
	 **/
	@Size(max = 255)
	public List<LocalizedStringSwgDTO> getName()
	{
		return name;
	}

	public void setName(final List<LocalizedStringSwgDTO> name)
	{
		this.name = name;
	}

	/**
	 * The detailed product description with HTML tags. Max length of the field is 21844 characters.
	 *
	 * @return description
	 **/

	public List<LocalizedLongStringSwgDTO> getDescription()
	{
		return description;
	}

	public void setDescription(final List<LocalizedLongStringSwgDTO> description)
	{
		this.description = description;
	}

	/**
	 * List of authors (and authors' roles)
	 *
	 * @return authorInfos
	 **/
	public List<AuthorInfoWsDto> getAuthorInfos()
	{
		return authorInfos;
	}

	public void setAuthorInfos(final List<AuthorInfoWsDto> authorInfos)
	{
		this.authorInfos = authorInfos;
	}

	public String getTextLanguage()
	{
		return textLanguage;
	}

	public void setTextLanguage(final String textLanguage)
	{
		this.textLanguage = textLanguage;
	}

	public List<LocalizedLongStringSwgDTO> getKeywords()
	{
		return keywords;
	}

	public void setKeywords(final List<LocalizedLongStringSwgDTO> keywords)
	{
		this.keywords = keywords;
	}

	public List<LocalizedBooleanSwgDTO> getTextbook()
	{
		return textbook;
	}

	public void setTextbook(final List<LocalizedBooleanSwgDTO> textbook)
	{
		this.textbook = textbook;
	}

	public Boolean getBestSeller()
	{
		return bestSeller;
	}

	public void setBestSeller(final Boolean bestSeller)
	{
		this.bestSeller = bestSeller;
	}

	public ImageContainerWsDto getCoverImage()
	{
		return coverImage;
	}

	public void setCoverImage(final ImageContainerWsDto coverImage)
	{
		this.coverImage = coverImage;
	}

	public List<DownloadableContentWsDto> getExcerpts()
	{
		return excerpts;
	}

	public void setExcerpts(final List<DownloadableContentWsDto> excerpts)
	{
		this.excerpts = excerpts;
	}

	public List<LocalizedLongStringSwgDTO> getDownloadsTab()
	{
		return downloadsTab;
	}

	public void setDownloadsTab(final List<LocalizedLongStringSwgDTO> downloadsTab)
	{
		this.downloadsTab = downloadsTab;
	}

	public List<LocalizedLongStringSwgDTO> getAboutAuthors()
	{
		return aboutAuthors;
	}

	public void setAboutAuthors(final List<LocalizedLongStringSwgDTO> aboutAuthors)
	{
		this.aboutAuthors = aboutAuthors;
	}

	public List<LocalizedLongStringSwgDTO> getPressRelease()
	{
		return pressRelease;
	}

	public void setPressRelease(final List<LocalizedLongStringSwgDTO> pressRelease)
	{
		this.pressRelease = pressRelease;
	}

	public List<LocalizedLongStringSwgDTO> getNewToEdition()
	{
		return newToEdition;
	}

	public void setNewToEdition(final List<LocalizedLongStringSwgDTO> newToEdition)
	{
		this.newToEdition = newToEdition;
	}

	public List<LocalizedLongStringSwgDTO> getWhatsNew()
	{
		return whatsNew;
	}

	public void setWhatsNew(final List<LocalizedLongStringSwgDTO> whatsNew)
	{
		this.whatsNew = whatsNew;
	}

	public List<LocalizedLongStringSwgDTO> getRelatedWebsites()
	{
		return relatedWebsites;
	}

	public void setRelatedWebsites(final List<LocalizedLongStringSwgDTO> relatedWebsites)
	{
		this.relatedWebsites = relatedWebsites;
	}

	public List<LocalizedLongStringSwgDTO> getNotes()
	{
		return notes;
	}

	public void setNotes(final List<LocalizedLongStringSwgDTO> notes)
	{
		this.notes = notes;
	}

	public List<LocalizedLongStringSwgDTO> getReviews()
	{
		return reviews;
	}

	public void setReviews(final List<LocalizedLongStringSwgDTO> reviews)
	{
		this.reviews = reviews;
	}

	public List<LocalizedLongStringSwgDTO> getErrata()
	{
		return errata;
	}

	public void setErrata(final List<LocalizedLongStringSwgDTO> errata)
	{
		this.errata = errata;
	}

	public List<LocalizedLongStringSwgDTO> getTableOfContents()
	{
		return tableOfContents;
	}

	public void setTableOfContents(final List<LocalizedLongStringSwgDTO> tableOfContents)
	{
		this.tableOfContents = tableOfContents;
	}

	public List<String> getSubjectIds()
	{
		return subjectIds;
	}

	public void setSubjectIds(final List<String> subjectIds)
	{
		this.subjectIds = subjectIds;
	}

	public List<String> getCourseIds()
	{
		return courseIds;
	}

	public void setCourseIds(final List<String> courseIds)
	{
		this.courseIds = courseIds;
	}

	public List<String> getBrandIds()
	{
		return brandIds;
	}

	public void setBrandIds(final List<String> brandIds)
	{
		this.brandIds = brandIds;
	}

	public List<String> getSeriesIds()
	{
		return seriesIds;
	}

	public void setSeriesIds(final List<String> seriesIds)
	{
		this.seriesIds = seriesIds;
	}

	public List<WebLinkWsDto> getWebLinks()
	{
		return webLinks;
	}

	public void setWebLinks(final List<WebLinkWsDto> webLinks)
	{
		this.webLinks = webLinks;
	}

	@Override
	public String toString()
	{
		return new ToStringBuilder(this)
				.append("name", name)
				.append("description", description)
				.append("authorInfos", authorInfos)
				.append("textLanguage", textLanguage)
				.append("keywords", keywords)
				.append("textbook", textbook)
				.append("bestSeller", bestSeller)
				.append("coverImage", coverImage)
				.append("excerpts", excerpts)
				.append("downloadsTab", downloadsTab)
				.append("aboutAuthors", aboutAuthors)
				.append("pressRelease", pressRelease)
				.append("newToEdition", newToEdition)
				.append("whatsNew", whatsNew)
				.append("relatedWebsites", relatedWebsites)
				.append("notes", notes)
				.append("reviews", reviews)
				.append("errata", errata)
				.append("tableOfContents", tableOfContents)
				.append("subjectIds", subjectIds)
				.append("courseIds", courseIds)
				.append("brandIds", brandIds)
				.append("seriesIds", seriesIds)
				.append("webLinks", webLinks)
				.toString();
	}
}

