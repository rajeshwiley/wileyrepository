package com.wiley.ws.core.order.populator;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.UnitService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Value;

import com.wiley.core.product.WileyProductService;
import com.wiley.ws.core.order.dto.OrderUpdateEntryRequestWsDTO;
import com.wiley.ws.core.order.service.WileycomExternalDiscountApplier;

import static com.wiley.core.constants.WileyCoreConstants.ONLINE_CATALOG_VERSION;
import static com.wiley.facades.populators.PopulateUtils.populateFieldIfNotNull;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


/**
 * Author Herman_Chukhrai (EPAM)
 */
public class WileycomOrderEntryReversePopulator implements Populator<OrderUpdateEntryRequestWsDTO, AbstractOrderEntryModel>
{
	@Value("${pdh.product.catalog.id}")
	private String productCatalogId;
	private static final String DEFAULT_UNIT_PIECES = "pieces";

	@Resource
	private CatalogVersionService catalogVersionService;

	@Resource
	private WileyProductService productService;

	@Resource
	private UnitService unitService;

	@Resource
	private WileycomExternalDiscountApplier wileycomExternalDiscountApplier;

	@Override
	public void populate(@NotNull final OrderUpdateEntryRequestWsDTO source, @NotNull final AbstractOrderEntryModel target)
			throws ConversionException
	{
		validateParameterNotNullStandardMessage("source", source);
		validateParameterNotNullStandardMessage("target", target);

		populateFieldIfNotNull(AbstractOrderEntryModel::setBasePrice, target, source.getBasePrice());
		populateFieldIfNotNull(AbstractOrderEntryModel::setTotalPrice, target, source.getTotalPrice());
		populateFieldIfNotNull(this::populateQuantity, target, source.getQuantity());
		populateFieldIfNotNull(this::populateProduct, target, source.getIsbn());
		populateUnit(target);
		wileycomExternalDiscountApplier.applyDiscounts(target, source.getDiscounts());
	}

	private void populateUnit(final AbstractOrderEntryModel target)
	{
		target.setUnit(unitService.getUnitForCode(DEFAULT_UNIT_PIECES));
	}

	private void populateProduct(final AbstractOrderEntryModel target, final String productIsbn)
	{
		final CatalogVersionModel onlineCatalogVersion = catalogVersionService.getCatalogVersion(productCatalogId,
				ONLINE_CATALOG_VERSION);
		final ProductModel product = productService.getProductForIsbn(productIsbn, onlineCatalogVersion);
		if (product != null)
		{
			target.setProduct(product);
		}
	}

	private void populateQuantity(final AbstractOrderEntryModel target, final Integer quantity)
	{
		target.setQuantity(Long.valueOf(quantity.longValue()));
	}
}
