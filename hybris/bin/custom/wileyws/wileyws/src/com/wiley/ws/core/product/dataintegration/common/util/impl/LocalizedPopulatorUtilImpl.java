package com.wiley.ws.core.product.dataintegration.common.util.impl;

import java.util.List;
import java.util.Locale;
import java.util.function.BiConsumer;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.google.common.base.Preconditions;
import com.wiley.ws.core.product.dataintegration.common.dto.LocalizedBooleanSwgDTO;
import com.wiley.ws.core.product.dataintegration.common.dto.LocalizedLongStringSwgDTO;
import com.wiley.ws.core.product.dataintegration.common.dto.LocalizedStringSwgDTO;
import com.wiley.ws.core.product.dataintegration.common.util.LocalizedPopulatorUtil;

import static org.apache.commons.lang3.LocaleUtils.toLocale;


public class LocalizedPopulatorUtilImpl implements LocalizedPopulatorUtil
{

	@Override
	public void populateStrings(@Nullable final List<LocalizedStringSwgDTO> localizedStrings,
			@Nonnull final BiConsumer<String, Locale> setterConsumer)
	{
		Preconditions.checkNotNull(setterConsumer, "setterConsumer should be not null");

		if (localizedStrings != null)
		{
			for (final LocalizedStringSwgDTO localizedString : localizedStrings)
			{
				setterConsumer.accept(localizedString.getValue(), toLocale(localizedString.getLocale()));
			}
		}
	}

	@Override
	public void populateLongStrings(@Nullable final List<LocalizedLongStringSwgDTO> localizedStrings,
			@Nonnull final BiConsumer<String, Locale> setterConsumer)
	{
		Preconditions.checkNotNull(setterConsumer, "setterConsumer should be not null");

		if (localizedStrings != null)
		{
			for (final LocalizedLongStringSwgDTO localizedString : localizedStrings)
			{
				setterConsumer.accept(localizedString.getValue(), toLocale(localizedString.getLocale()));
			}
		}
	}

	@Override
	public void populateBooleans(@Nullable final List<LocalizedBooleanSwgDTO> localizedBooleans,
			@Nonnull final BiConsumer<Boolean, Locale> setterConsumer)

	{
		Preconditions.checkNotNull(setterConsumer, "setterConsumer should be not null");

		if (localizedBooleans != null)
		{
			for (final LocalizedBooleanSwgDTO localizedBoolean : localizedBooleans)
			{
				setterConsumer.accept(localizedBoolean.getValue(), toLocale(localizedBoolean.getLocale()));
			}
		}
	}
}
