package com.wiley.ws.core.order.populator;

import com.wiley.integrations.order.dto.OrderEntryWsDTO;
import com.wiley.ws.core.order.dto.GetOrderEntriesResponseWsDTO;
import com.wiley.ws.core.order.dto.PaginationWsDTO;
import de.hybris.platform.commerceservices.search.pagedata.PaginationData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;

import static org.springframework.util.Assert.notNull;

public class WileyasGetOrderEntriesResponsePopulator
        implements Populator<SearchPageData<AbstractOrderEntryModel>, GetOrderEntriesResponseWsDTO>
{
    @Resource
    private Converter<PaginationData, PaginationWsDTO> wileyasPaginationConverter;

    @Resource
    private Converter<AbstractOrderEntryModel, OrderEntryWsDTO> wileyOrderEntryWsConverter;

    @Override
    public void populate(final SearchPageData<AbstractOrderEntryModel> source,
                         final GetOrderEntriesResponseWsDTO target) throws ConversionException
    {
        notNull(source, "Source object should not be NULL");
        notNull(target, "Target object should not be NULL");

        target.setPagination(wileyasPaginationConverter.convert(source.getPagination()));
        target.setResults(wileyOrderEntryWsConverter.convertAll(source.getResults()));
    }

    public Converter<PaginationData, PaginationWsDTO> getWileyasPaginationConverter() {
        return wileyasPaginationConverter;
    }

    public void setWileyasPaginationConverter(final Converter<PaginationData,
            PaginationWsDTO> wileyasPaginationConverter)
    {
        this.wileyasPaginationConverter = wileyasPaginationConverter;
    }

    public Converter<AbstractOrderEntryModel, OrderEntryWsDTO> getWileyOrderEntryWsConverter()
    {
        return wileyOrderEntryWsConverter;
    }

    public void setWileyOrderEntryWsConverter(
            final Converter<AbstractOrderEntryModel, OrderEntryWsDTO> wileyOrderEntryWsConverter)
    {
        this.wileyOrderEntryWsConverter = wileyOrderEntryWsConverter;
    }
}
