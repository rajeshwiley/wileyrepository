/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package com.wiley.ws.core.cronjob;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.order.dao.CommerceCartDao;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.time.TimeService;
import de.hybris.platform.servicelayer.user.UserService;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.ws.core.model.OldCartRemovalCronJobModel;


/**
 * A Cron Job to clean up old carts.
 */
public class OldCartRemovalJob extends AbstractJobPerformable<OldCartRemovalCronJobModel>
{
	private static final Logger LOG = Logger.getLogger(OldCartRemovalJob.class);

	private CommerceCartDao commerceCartDao;
	private TimeService timeService;
	private UserService userService;

	private static final int DEFAULT_CART_MAX_AGE = 2419200;
	private static final int DEFAULT_ANONYMOUS_CART_MAX_AGE = 1209600;

	/**
	 * Perform perform result.
	 *
	 * @param job
	 * 		the job
	 * @return the perform result
	 */
	@Override
	public PerformResult perform(final OldCartRemovalCronJobModel job)
	{
		try
		{
			if (job.getSites() == null || job.getSites().isEmpty())
			{
				LOG.warn("There is no sites defined for " + job.getCode());
				return new PerformResult(CronJobResult.FAILURE, CronJobStatus.FINISHED);
			}

			final int cartAge = job.getCartRemovalAge() != null ? job.getCartRemovalAge().intValue() : DEFAULT_CART_MAX_AGE;
			final int anonymousCartAge = job.getAnonymousCartRemovalAge() != null ?
					job.getAnonymousCartRemovalAge().intValue() :
					DEFAULT_ANONYMOUS_CART_MAX_AGE;

			for (final BaseSiteModel site : job.getSites())
			{
				for (final CartModel oldCart : getCommerceCartDao().getCartsForRemovalForSiteAndUser(new DateTime(
						getTimeService().getCurrentTime()).minusSeconds(cartAge).toDate(), site, null))
				{
					getModelService().remove(oldCart);
				}

				for (final CartModel oldCart : getCommerceCartDao().getCartsForRemovalForSiteAndUser(new DateTime(
								getTimeService().getCurrentTime()).minusSeconds(anonymousCartAge).toDate(), site,
						getUserService().getAnonymousUser()))
				{
					getModelService().remove(oldCart);
				}
			}

			return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
		}
		catch (final Exception e)
		{
			LOG.error("Exception occurred during cart cleanup", e);
			return new PerformResult(CronJobResult.ERROR, CronJobStatus.ABORTED);
		}
	}

	/**
	 * Gets commerce cart dao.
	 *
	 * @return the commerce cart dao
	 */
	protected CommerceCartDao getCommerceCartDao()
	{
		return commerceCartDao;
	}

	/**
	 * Sets commerce cart dao.
	 *
	 * @param commerceCartDao
	 * 		the commerce cart dao
	 */
	@Required
	public void setCommerceCartDao(final CommerceCartDao commerceCartDao)
	{
		this.commerceCartDao = commerceCartDao;
	}

	/**
	 * Gets time service.
	 *
	 * @return the time service
	 */
	protected TimeService getTimeService()
	{
		return timeService;
	}

	/**
	 * Sets time service.
	 *
	 * @param timeService
	 * 		the time service
	 */
	@Required
	public void setTimeService(final TimeService timeService)
	{
		this.timeService = timeService;
	}

	/**
	 * Gets model service.
	 *
	 * @return the model service
	 */
	protected ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * Gets user service.
	 *
	 * @return the user service
	 */
	protected UserService getUserService()
	{
		return userService;
	}

	/**
	 * Sets user service.
	 *
	 * @param userService
	 * 		the user service
	 */
	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}
}
