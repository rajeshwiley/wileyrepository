package com.wiley.ws.core.cart.populator;

import com.wiley.core.enums.OrderType;
import com.wiley.core.cart.WileyCartFactory;
import com.wiley.ws.core.cart.dto.CreateCartRequestWsDTO;
import de.hybris.platform.converters.impl.AbstractPopulatingConverter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.apache.commons.lang.StringUtils;

import javax.annotation.Resource;


/**
 * Author Maksim_Kozich
 */
public class WileyasCartModelReverseConverter extends AbstractPopulatingConverter<CreateCartRequestWsDTO, CartModel>
{

	@Resource(name = "wileyCartFactory")
	private WileyCartFactory wileyCartFactory;

	@Override
	public CartModel convert(final CreateCartRequestWsDTO createCartRequestWsDTO) throws ConversionException
	{
		OrderType cartType = StringUtils.isNotBlank(createCartRequestWsDTO.getType()) ?
				OrderType.valueOf(createCartRequestWsDTO.getType()) : OrderType.GENERAL;
		CartModel cartForOrderType = wileyCartFactory.createCartWithType(cartType);
		superPopulate(createCartRequestWsDTO, cartForOrderType);
		return cartForOrderType;
	}

	protected void superPopulate(final CreateCartRequestWsDTO createCartRequestWsDTO, final CartModel cartForOrderType)
	{
		super.populate(createCartRequestWsDTO, cartForOrderType);
	}

	public WileyCartFactory getWileyCartFactory()
	{
		return wileyCartFactory;
	}

	public void setWileyCartFactory(final WileyCartFactory wileyCartFactory)
	{
		this.wileyCartFactory = wileyCartFactory;
	}
}
