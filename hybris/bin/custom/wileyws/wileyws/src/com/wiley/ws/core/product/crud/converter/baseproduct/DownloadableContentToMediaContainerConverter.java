package com.wiley.ws.core.product.crud.converter.baseproduct;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Collections;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.ws.core.product.crud.converter.AbstractCrudDtoConverter;
import com.wiley.ws.core.product.crud.dto.DownloadableContentWsDto;
import com.wiley.ws.core.product.crud.facade.WileyProductCrudCatalogVersionProvider;
import com.wiley.ws.core.product.dataintegration.common.util.LocalizedPopulatorUtil;


public class DownloadableContentToMediaContainerConverter
		extends AbstractCrudDtoConverter<DownloadableContentWsDto, MediaContainerModel>
{

	private final ModelService modelService;
	private final WileyProductCrudCatalogVersionProvider wileyProductCrudCatalogVersionProvider;
	private final LocalizedPopulatorUtil localizedPopulatorUtil;


	@Autowired
	public DownloadableContentToMediaContainerConverter(final ModelService modelService,
			final WileyProductCrudCatalogVersionProvider wileyProductCrudCatalogVersionProvider,
			final LocalizedPopulatorUtil localizedPopulatorUtil)
	{
		this.modelService = modelService;
		this.wileyProductCrudCatalogVersionProvider = wileyProductCrudCatalogVersionProvider;
		this.localizedPopulatorUtil = localizedPopulatorUtil;
	}

	@Override
	public MediaContainerModel convert(final DownloadableContentWsDto downloadableContentWsDto,
			final MediaContainerModel targetContainer)
			throws ConversionException
	{
		final CatalogVersionModel catalogVersion = wileyProductCrudCatalogVersionProvider.getCatalogVersion();
		MediaContainerModel mediaContainer = targetContainer;
		if (targetContainer == null)
		{
			mediaContainer = modelService.create(MediaContainerModel.class);
		}
		mediaContainer.setCatalogVersion(catalogVersion);
		localizedPopulatorUtil.populateStrings(downloadableContentWsDto.getTitle(), mediaContainer::setName);

		final MediaModel media = modelService.create(MediaModel.class);
		media.setCatalogVersion(catalogVersion);
		media.setMime(downloadableContentWsDto.getMimeType());
		media.setURL(downloadableContentWsDto.getUrl());
		media.setCode(UUID.randomUUID().toString());

		mediaContainer.setMedias(Collections.singletonList(media));
		mediaContainer.setQualifier(UUID.randomUUID().toString());
		return mediaContainer;
	}

}
