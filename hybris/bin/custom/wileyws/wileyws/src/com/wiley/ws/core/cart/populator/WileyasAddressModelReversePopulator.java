package com.wiley.ws.core.cart.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;

import com.wiley.ws.core.cart.dto.CartAddressWsDTO;

import static com.wiley.facades.populators.PopulateUtils.populateFieldIfNotNull;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


/**
 * Populates {OrderAddressWsDTO} to {AddressModel}
 */
public class WileyasAddressModelReversePopulator implements Populator<CartAddressWsDTO, AddressModel>
{
	@Resource
	private CommonI18NService commonI18NService;

	@Override
	public void populate(@NotNull final CartAddressWsDTO source, @NotNull final AddressModel target) throws ConversionException
	{
		validateParameterNotNullStandardMessage("source", source);
		validateParameterNotNullStandardMessage("target", target);

		populateFieldIfNotNull(AddressModel::setPostalcode, target, source.getPostcode());

		populateCountryAndRegion(target, source);

		populateFieldIfNotNull(AddressModel::setTown, target, source.getCity());
		populateFieldIfNotNull(AddressModel::setLine1, target, source.getLine1());
		populateFieldIfNotNull(AddressModel::setLine2, target, source.getLine2());
		populateFieldIfNotNull(AddressModel::setFirstname, target, source.getFirstName());
		populateFieldIfNotNull(AddressModel::setLastname, target, source.getLastName());
		populateFieldIfNotNull(AddressModel::setPhone1, target, source.getPhoneNumber());
		populateFieldIfNotNull(AddressModel::setEmail, target, source.getEmail());
		populateFieldIfNotNull(AddressModel::setCompany, target, source.getOrganization());
		populateFieldIfNotNull(AddressModel::setDepartment, target, source.getDepartment());
	}

	private void populateCountryAndRegion(final AddressModel target, final CartAddressWsDTO source)
	{
		String countryIsoCode = source.getCountry();
		if (countryIsoCode != null)
		{
			CountryModel countryModel = getCommonI18NService().getCountry(countryIsoCode);
			target.setCountry(countryModel);
			String regionIsoCodeShort = source.getState();
			if (regionIsoCodeShort != null)
			{
				String regionIsoCode = countryModel.getIsocode() + "-" + source.getState();
				RegionModel regionModel = getCommonI18NService().getRegion(countryModel, regionIsoCode);
				target.setRegion(regionModel);
			}
		}
	}

	public CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}
}
