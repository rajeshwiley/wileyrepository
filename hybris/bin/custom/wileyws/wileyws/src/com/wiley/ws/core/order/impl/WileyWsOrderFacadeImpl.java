package com.wiley.ws.core.order.impl;

import com.wiley.core.order.WileyOrderFulfilmentProcessService;
import com.wiley.core.order.service.WileyOrderService;
import com.wiley.core.store.WileyBaseStoreService;
import com.wiley.integrations.order.dto.CreateOrderRequestWsDTO;
import com.wiley.integrations.order.dto.OrderWsDTO;
import com.wiley.ws.core.order.WileyWsOrderFacade;
import com.wiley.ws.core.order.dto.GetOrderEntriesResponseWsDTO;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.site.BaseSiteService;

import javax.annotation.Resource;


public class WileyWsOrderFacadeImpl implements WileyWsOrderFacade
{

	@Resource
	private WileyOrderService wileyOrderService;

	@Resource
	private Converter<SearchPageData<AbstractOrderEntryModel>, GetOrderEntriesResponseWsDTO>
			wileyasGetOrderEntriesResponseConverter;

	@Resource
	private Converter<OrderModel, OrderWsDTO> wileyOrderWsConverter;

	@Resource
	private Converter<CreateOrderRequestWsDTO, OrderModel> wileyasOrderReverseWsConverter;

	@Resource
	private ModelService modelService;

	@Resource
	private BaseSiteService baseSiteService;

	@Resource
    private WileyBaseStoreService wileyBaseStoreService;

	@Resource
	private WileyOrderFulfilmentProcessService wileyOrderFulfilmentProcessService;

	@Override
	public OrderWsDTO getOrderDetails(final String guid, final BaseSiteModel baseSite)
	{
		return wileyOrderWsConverter.convert(wileyOrderService.getOrderForGUID(guid, baseSite));
	}
}
