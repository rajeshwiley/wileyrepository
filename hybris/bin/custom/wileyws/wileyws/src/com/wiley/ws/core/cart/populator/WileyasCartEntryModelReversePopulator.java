package com.wiley.ws.core.cart.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.util.PriceValue;

import javax.annotation.Resource;

import com.wiley.core.model.WileyExternalDiscountModel;
import com.wiley.core.product.WileyProductService;
import com.wiley.ws.core.cart.dto.CreateCartEntryRequestWsDTO;
import com.wiley.integrations.order.dto.DiscountValueWsDTO;
import com.wiley.integrations.order.dto.PriceValueWsDTO;


public class WileyasCartEntryModelReversePopulator implements Populator<CreateCartEntryRequestWsDTO, CartEntryModel>
{
	private static final long DEFAULT_QUANTITY = 1L;

	@Resource
	private Converter<PriceValueWsDTO, PriceValue> wileyasPriceValueReverseConverter;

	@Resource
	private Converter<DiscountValueWsDTO, WileyExternalDiscountModel> wileyExternalDiscountReverseWsConverter;

	@Resource
	private WileyProductService productService;

	@Override
	public void populate(final CreateCartEntryRequestWsDTO cartEntryDTO,
			final CartEntryModel cartEntry) throws ConversionException
	{
		final ProductModel product = productService.getProductForCode(cartEntryDTO.getProductCode());
		cartEntry.setProduct(product);
		cartEntry.setBusinessItemId(cartEntryDTO.getBusinessItemId());

		cartEntry.setBusinessKey(cartEntryDTO.getBusinessKey());
		cartEntry.setOriginalBusinessKey(cartEntryDTO.getBusinessKey());
		if (cartEntryDTO.getQuantity() == null)
		{
			cartEntry.setQuantity(DEFAULT_QUANTITY);
		}
		else
		{
			cartEntry.setQuantity(cartEntryDTO.getQuantity().longValue());
		}

		cartEntry.setExternalPriceValues(wileyasPriceValueReverseConverter.convertAll(cartEntryDTO.getExternalPrices()));

		cartEntry.setExternalDiscounts(wileyExternalDiscountReverseWsConverter.convertAll(cartEntryDTO.getExternalDiscounts()));

		cartEntry.setNamedDeliveryDate(cartEntryDTO.getDeliveryDate());
		cartEntry.setAdditionalInfo(cartEntryDTO.getAdditionalInfo());
	}

	public Converter<PriceValueWsDTO, PriceValue> getWileyasPriceValueReverseConverter()
	{
		return wileyasPriceValueReverseConverter;
	}

	public void setWileyasPriceValueReverseConverter(
			final Converter<PriceValueWsDTO, PriceValue> wileyasPriceValueReverseConverter)
	{
		this.wileyasPriceValueReverseConverter = wileyasPriceValueReverseConverter;
	}

	public Converter<DiscountValueWsDTO, WileyExternalDiscountModel> getWileyExternalDiscountReverseWsConverter()
	{
		return wileyExternalDiscountReverseWsConverter;
	}

	public void setWileyExternalDiscountReverseWsConverter(
			final Converter<DiscountValueWsDTO, WileyExternalDiscountModel> wileyExternalDiscountReverseWsConverter)
	{
		this.wileyExternalDiscountReverseWsConverter = wileyExternalDiscountReverseWsConverter;
	}
}
