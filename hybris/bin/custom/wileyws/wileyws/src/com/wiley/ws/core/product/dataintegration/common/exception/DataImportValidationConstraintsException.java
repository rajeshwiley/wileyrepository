package com.wiley.ws.core.product.dataintegration.common.exception;

import com.wiley.ws.core.product.dataintegration.common.error.ErrorListSwgDTO;


/**
 * @author ygalav.
 */
public class DataImportValidationConstraintsException extends RuntimeException
{
	private ErrorListSwgDTO errors;

	public DataImportValidationConstraintsException(final ErrorListSwgDTO errors)
	{
		this.errors = errors;
	}

	public ErrorListSwgDTO getErrors()
	{
		return errors;
	}

	public void setErrors(final ErrorListSwgDTO errors)
	{
		this.errors = errors;
	}
}
