package com.wiley.ws.core.order.exception;

/**
 * The type Order ws not found exception.
 */
public class OrderWsNotFoundException extends WileywsValidationException
{
	private static final String ERROR_MESSAGE_FORMAT = "Order not found, order code = [%s]";

	private String orderCode;

	public OrderWsNotFoundException(final String orderCode)
	{
		this.orderCode = orderCode;
	}

	public OrderWsNotFoundException(final Throwable cause, final String orderCode)
	{
		super(cause);
		this.orderCode = orderCode;
	}

	@Override
	public String getMessage()
	{
		return String.format(ERROR_MESSAGE_FORMAT, orderCode);
	}

	public String getOrderCode()
	{
		return orderCode;
	}

	public void setOrderCode(final String orderCode)
	{
		this.orderCode = orderCode;
	}
}