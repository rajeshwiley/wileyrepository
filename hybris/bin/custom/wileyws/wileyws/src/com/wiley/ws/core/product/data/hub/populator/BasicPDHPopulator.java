package com.wiley.ws.core.product.data.hub.populator;


import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang.LocaleUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.enums.ProductEditionFormat;
import com.wiley.core.enums.WileyAssortment;
import com.wiley.core.enums.WileyDigitalContentTypeEnum;
import com.wiley.core.enums.WileyProductSubtypeEnum;
import com.wiley.ws.core.product.data.hub.dto.LocalizedLongStringSwgDTO;
import com.wiley.ws.core.product.data.hub.dto.LocalizedStringSwgDTO;
import com.wiley.ws.core.product.data.hub.dto.ProductSwgDTO;

import static com.wiley.facades.populators.PopulateUtils.populateFieldIfNotNull;


/**
 * Basic properties {@link ProductSwgDTO} to {@link ProductModel}
 */
public class BasicPDHPopulator implements Populator<ProductSwgDTO, ProductModel>
{
	private static final String EN_LANG_ISOCODE = "en";

	@Autowired
	private EnumerationService enumerationService;

	@Override
	public void populate(final ProductSwgDTO product, final ProductModel productModel) throws ConversionException
	{
		// TODO : [ECSC-20075] commented out code which uses obsolete attribute. Should be deleted
		populateFieldIfNotNull((p, val) -> p.setCode(val), productModel, product.getCode());
//		populateFieldIfNotNull((p, val) -> p.setPurchasable(val), productModel, product.getPurchasable());
		populateFieldIfNotNull((p, val) -> p.setCountable(val), productModel, product.getCountable());
		populateFieldIfNotNull((p, val) -> p.setOnlineDate(val), productModel, product.getOnlineDate());
		populateFieldIfNotNull((p, val) -> p.setOfflineDate(val), productModel, product.getOfflineDate());
		populateFieldIfNotNull((p, val) -> p.setIsbn(val), productModel, product.getIsbn());
// 		populateFieldIfNotNull((p, val) -> p.setPrintIssn(val), productModel, product.getPrintIssn());
//		populateFieldIfNotNull((p, val) -> p.setOnlineIssn(val), productModel, product.getOnlineIssn());
//		populateFieldIfNotNull((p, val) -> p.setReleaseDate(val), productModel, product.getReleaseDate());
		populateFieldIfNotNull((p, val) -> p.setPrintOnDemand(val), productModel, product.getPrintOnDemand());
		populateFieldIfNotNull((p, val) -> p.setTextbook(val), productModel, product.getTextbook());
		populateFieldIfNotNull((p, val) -> p.setTaxCategory(val), productModel, product.getTaxCategory());
		populateFieldIfNotNull((p, val) -> p.setTaxTransactionType(val), productModel, product.getTaxTransactionType());
		populateFieldIfNotNull((p, val) -> p.setSapProductCode(val), productModel, product.getSapProductCode());
		populateFieldIfNotNull((p, val) -> p.setPdmCode(val), productModel, product.getPdmProductCode());

		populateEnumerationFields(product, productModel);
		populateLocalizationFields(product, productModel);

	}

	private void populateEnumerationFields(final ProductSwgDTO product, final ProductModel productModel)
	{
		if (product.getAssortments() != null)
		{
			List<WileyAssortment> assortments = product.getAssortments().stream()
					.map(assortment -> enumerationService.getEnumerationValue(WileyAssortment.class, assortment.getValue()))
					.collect(Collectors.toList());
			productModel.setAssortments(assortments);
		}


		if (product.getSubtype() != null)
		{
			productModel.setSubtype(
					enumerationService.getEnumerationValue(WileyProductSubtypeEnum.class, product.getSubtype().name()));
		}

		if (product.getEditionFormat() != null)
		{
			productModel.setEditionFormat(
					enumerationService.getEnumerationValue(ProductEditionFormat.class, product.getEditionFormat().name()));
		}


		if (product.getDigitalContentType() != null)
		{
			productModel.setDigitalContentType(enumerationService
					.getEnumerationValue(WileyDigitalContentTypeEnum.class, product.getDigitalContentType().name()));
		}

	}

	private void populateLocalizationFields(final ProductSwgDTO product, final ProductModel productModel)
	{
		if (product.getName() != null)
		{
			for (LocalizedStringSwgDTO localizedName : product.getName())
			{
				productModel.setName(localizedName.getVal(), LocaleUtils.toLocale(localizedName.getLoc().getValue()));
			}
		}


		if (product.getDescription() != null)
		{
			for (LocalizedLongStringSwgDTO localizedDescription : product.getDescription())
			{
				productModel.setDescription(localizedDescription.getVal(),
						LocaleUtils.toLocale(localizedDescription.getLoc().getValue()));
			}
		}


		if (product.getSummary() != null)
		{
			for (LocalizedLongStringSwgDTO localizedSummary : product.getSummary())
			{
				productModel.setSummary(localizedSummary.getVal(), LocaleUtils.toLocale(localizedSummary.getLoc().getValue()));
			}
		}

		// TODO : [ECSC-20075] commented out code which uses obsolete attribute. Should be deleted
/*		if (product.getAuthors() != null)
		{
			productModel.setAuthors(product.getAuthors(), LocaleUtils.toLocale(EN_LANG_ISOCODE));
		}*/
	}

}
