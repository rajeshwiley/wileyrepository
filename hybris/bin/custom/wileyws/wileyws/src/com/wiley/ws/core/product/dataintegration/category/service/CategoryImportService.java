package com.wiley.ws.core.product.dataintegration.category.service;

import de.hybris.platform.category.model.CategoryModel;


public interface CategoryImportService
{
	/**
	 * Returns categoryModel for specified categoryCode.
	 * Catalog version is provided by {@link com.wiley.ws.core.product.crud.facade.WileyProductCrudCatalogVersionProvider}
	 *
	 * @param categoryCode
	 * @return
	 */
	CategoryModel getCategoryForCode(String categoryCode);
}
