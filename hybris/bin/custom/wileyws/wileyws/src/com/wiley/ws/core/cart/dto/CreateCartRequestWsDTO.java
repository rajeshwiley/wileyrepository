package com.wiley.ws.core.cart.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wiley.integrations.order.dto.AddressWsDTO;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Objects;


/**
 * CreateCartRequest DTO
 */
public class CreateCartRequestWsDTO
{
	/**
	 * The type of the cart. Defines the checkout flow and different controls/actions that are available to the customer.
	 */
	@Size(max = 255)
	@Pattern(regexp = "GENERAL|AS_QUOTED|AS_FUNDED", message = "{validation.default.Pattern}")
	private String type;

	/**
	 * Two-letter ISO 3166-1 alpha-2 code that specifies a country.
	 * See https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2 for details.
	 */
	@NotNull
	@Pattern(regexp = "^[A-Z]{2}$")
	private String country;

	/**
	 * Three-letter ISO 4217 code that specifies a currency.
	 * See https://en.wikipedia.org/wiki/ISO_4217 for details.
	 */
	@Pattern(regexp = "^[A-Z]{3}$")
	private String currency;

	@Valid
	private AddressWsDTO contactAddress;

	@Valid
	private AddressWsDTO paymentAddress;

	@Size(max = 21844)
	private String externalText;

	@JsonProperty(value = "type")
	public String getType()
	{
		return type;
	}

	public void setType(final String type)
	{
		this.type = type;
	}

	@JsonProperty("country")
	public String getCountry()
	{
		return country;
	}

	public void setCountry(final String country)
	{
		this.country = country;
	}

	@JsonProperty("currency")
	public String getCurrency()
	{
		return currency;
	}

	public void setCurrency(final String currency)
	{
		this.currency = currency;
	}

	@JsonProperty("contactAddress")
	public AddressWsDTO getContactAddress()
	{
		return contactAddress;
	}

	public void setContactAddress(final AddressWsDTO contactAddress)
	{
		this.contactAddress = contactAddress;
	}

	@JsonProperty("paymentAddress")
	public AddressWsDTO getPaymentAddress()
	{
		return paymentAddress;
	}

	public void setPaymentAddress(final AddressWsDTO paymentAddress)
	{
		this.paymentAddress = paymentAddress;
	}

	@JsonProperty("externalText")
	public String getExternalText()
	{
		return externalText;
	}

	public void setExternalText(final String externalText)
	{
		this.externalText = externalText;
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (!(o instanceof CreateCartRequestWsDTO))
		{
			return false;
		}
		final CreateCartRequestWsDTO that = (CreateCartRequestWsDTO) o;
		return Objects.equals(type, that.type)
				&& Objects.equals(country, that.country)
				&& Objects.equals(currency, that.currency)
				&& Objects.equals(contactAddress, that.contactAddress)
				&& Objects.equals(paymentAddress, that.paymentAddress)
				&& Objects.equals(externalText, that.externalText);
	}

	@Override
	public int hashCode()
	{

		return Objects.hash(type, country, currency, contactAddress, paymentAddress, externalText);
	}

	@Override
	public String toString()
	{
		return new ToStringBuilder(this)
				.append("type", type)
				.append("country", country)
				.append("currency", currency)
				.append("contactAddress", contactAddress)
				.append("paymentAddress", paymentAddress)
				.append("externalText", externalText)
				.toString();
	}
}
