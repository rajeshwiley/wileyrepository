package com.wiley.ws.core.product.dataintegration.category.dto;

import org.apache.commons.lang3.builder.ToStringBuilder;


public class CategorySwgDTO extends UpdateCategorySwgDTO
{

	public CategorySwgDTO() { }

	public CategorySwgDTO(final UpdateCategorySwgDTO updateCategorySwgDTO)
	{
		this.setName(updateCategorySwgDTO.getName());
		this.setSupercategories(updateCategorySwgDTO.getSupercategories());
	}

	private String id;

	public String getId()
	{
		return id;
	}

	public void setId(final String id)
	{
		this.id = id;
	}

	@Override
	public String toString()
	{
		return new ToStringBuilder(this)
				.append("id", id)
				.append("name", getName())
				.append("supercategories", getSupercategories())
				.toString();
	}
}
