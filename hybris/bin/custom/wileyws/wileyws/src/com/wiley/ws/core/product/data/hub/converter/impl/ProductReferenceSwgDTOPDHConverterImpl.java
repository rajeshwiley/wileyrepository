package com.wiley.ws.core.product.data.hub.converter.impl;

import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.model.ModelService;

import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.model.WileyProductModel;
import com.wiley.core.product.WileyProductService;
import com.wiley.ws.core.product.data.hub.converter.ProductReferenceSwgDTOPDHConverter;
import com.wiley.ws.core.product.data.hub.dto.ProductReferenceSwgDTO;


/**
 * Converter implementation
 */
public class ProductReferenceSwgDTOPDHConverterImpl implements ProductReferenceSwgDTOPDHConverter
{
	private static final boolean DEFAULT_ACTIVE_TRUE = true;
	private static final boolean DEFAULT_PRESELECT_TRUE = true;
	private static final String QUALIFIER_SEPARATOR = "_";

	@Autowired
	private ModelService modelService;

	@Autowired
	private EnumerationService enumerationService;

	@Autowired
	private WileyProductService wileyProductService;

	@Override
	public ProductReferenceModel convertToProductRefereceModel(final ProductReferenceSwgDTO reference,
			final WileyProductModel sourceProduct)
	{

		ProductReferenceModel productReference = modelService.create(ProductReferenceModel.class);
		productReference.setActive(DEFAULT_ACTIVE_TRUE);
		productReference.setPreselected(DEFAULT_PRESELECT_TRUE);
		productReference.setSource(sourceProduct);

		productReference.setQualifier(generateQualifier(sourceProduct.getCode(), reference.getProductCode()));
		ProductModel targetProduct = wileyProductService.getProductForCode(sourceProduct.getCatalogVersion(),
				reference.getProductCode());
		productReference.setTarget(targetProduct);
		ProductReferenceTypeEnum referenceType = enumerationService.getEnumerationValue(ProductReferenceTypeEnum.class,
				reference.getReferenceType().name());
		productReference.setReferenceType(referenceType);

		return productReference;
	}

	private String generateQualifier(final String sourceProductCode, final String targetProductCode)
	{
		return new StringBuilder(sourceProductCode).append(QUALIFIER_SEPARATOR).append(targetProductCode).toString();
	}
}
