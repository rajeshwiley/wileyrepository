package com.wiley.ws.core.order.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.util.DiscountValue;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

import org.apache.commons.collections.CollectionUtils;

import com.wiley.ws.core.order.dto.OrderUpdateRequestWsDTO;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


/**
 * Author Herman_Chukhrai (EPAM)
 */
public class WileycomGlobalDiscountReversePopulator implements Populator<OrderUpdateRequestWsDTO, OrderModel>

{

	private String currencyIsoCode;

	@Override
	public void populate(@NotNull final OrderUpdateRequestWsDTO source, @NotNull final OrderModel target)
			throws ConversionException
	{
		validateParameterNotNullStandardMessage("source", source);
		validateParameterNotNullStandardMessage("target", target);

		if (CollectionUtils.isNotEmpty(source.getDiscounts()))
		{
			validateParameterNotNull("currencyIsoCode", "Currency isoCode should be provided");

			final List<DiscountValue> discountValues =
					source.getDiscounts()
							.stream()
							.map(dto -> new DiscountValue(dto.getName(), dto.getValue(), dto.getAbsolute(), currencyIsoCode))
							.collect(Collectors.toList());
			target.setGlobalDiscountValues(discountValues);
		}
	}

	public void setCurrencyIsoCode(final String currencyIsoCode)
	{
		this.currencyIsoCode = currencyIsoCode;
	}
}
