package com.wiley.ws.core.product.dataintegration.category.service.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;

import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.ws.core.product.crud.facade.WileyProductCrudCatalogVersionProvider;
import com.wiley.ws.core.product.dataintegration.category.service.CategoryImportService;


public class CategoryImportServiceImpl implements CategoryImportService
{
	private final CategoryService categoryService;
	private final WileyProductCrudCatalogVersionProvider wileyProductCrudCatalogVersionProvider;

	@Autowired
	public CategoryImportServiceImpl(final CategoryService categoryService,
			final WileyProductCrudCatalogVersionProvider wileyProductCrudCatalogVersionProvider)
	{
		this.categoryService = categoryService;
		this.wileyProductCrudCatalogVersionProvider = wileyProductCrudCatalogVersionProvider;
	}

	public CategoryModel getCategoryForCode(final String categoryCode)
	{
		final CatalogVersionModel catalogVersion = wileyProductCrudCatalogVersionProvider.getCatalogVersion();
		return categoryService.getCategoryForCode(catalogVersion, categoryCode);
	}

}
