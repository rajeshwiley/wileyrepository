package com.wiley.ws.core.product.dataintegration.price.facade.impl;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Date;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import com.wiley.core.price.service.WileyPriceService;
import com.wiley.core.product.WileyProductService;
import com.wiley.ws.core.product.dataintegration.common.service.DataImportValidationService;
import com.wiley.ws.core.product.dataintegration.price.converter.PriceImportConverter;
import com.wiley.ws.core.product.dataintegration.price.dto.PriceSwgDTO;
import com.wiley.ws.core.product.dataintegration.price.facade.PriceImportFacade;


public class PriceImportFacadeImpl implements PriceImportFacade
{

	private static final Logger LOG = LoggerFactory.getLogger(PriceImportFacadeImpl.class);

	@Value("Online")
	private String onlineCatalogVersion;
	@Value("wileyProductCatalog")
	private String productCatalogId;

	@Resource
	private WileyPriceService wileyPriceService;

	@Resource
	private WileyProductService productService;

	@Resource
	private CatalogVersionService catalogVersionService;

	@Resource
	private PriceImportConverter priceImportConverter;

	@Resource
	private ModelService modelService;

	@Resource
	private DataImportValidationService dataImportValidationService;

	@Override
	public void updatePrice(final String priceId, final PriceSwgDTO priceSwgDTO)
	{
		LOG.debug("Updating base product from DTO: {}", priceSwgDTO);
		final PriceRowModel oldPriceRowModel = wileyPriceService.getPriceRowForPriceId(priceId);
		if (shouldUpdatePriceRow(priceSwgDTO, oldPriceRowModel))
		{
			final PriceRowModel updatedPriceRowModel = priceImportConverter.convert(priceSwgDTO, oldPriceRowModel);
			dataImportValidationService.validate(updatedPriceRowModel);
			modelService.save(updatedPriceRowModel);
			markOnlineProductModified(updatedPriceRowModel.getProductId());
			LOG.debug("Updating price row details {} ", updatedPriceRowModel.getCode());
		}
	}

	private boolean shouldUpdatePriceRow(final PriceSwgDTO priceSwgDTO, final PriceRowModel oldPriceRowModel)
	{
		return null == priceSwgDTO.getSequenceId() || null == oldPriceRowModel.getSequenceId()
				|| priceSwgDTO.getSequenceId() > oldPriceRowModel.getSequenceId();
	}

	private void markOnlineProductModified(final String productCode)
	{

		if (StringUtils.isNotEmpty(productCode))
		{
			try
			{
				ProductModel product = findProductInOnlineProductCatalog(productCode);
				product.setModifiedtime(new Date());
				modelService.save(product);
			}
			catch (UnknownIdentifierException e)
			{
				LOG.info("ProductCode [{}] for online catalog version was not found", productCode);
			}
		}
	}

	private ProductModel findProductInOnlineProductCatalog(final String productCode)
	{
		ProductModel product = null;
		product = productService.getProductForCode(
				catalogVersionService.getCatalogVersion(productCatalogId, onlineCatalogVersion), productCode);
		return product;
	}

	@Override
	public void createPrice(final String priceId, final PriceSwgDTO priceSwgDTO)
	{
		LOG.debug("Creating price from priceSwgDTO : {}", priceSwgDTO);
		final PriceRowModel newPriceRowModel = priceImportConverter.convert(priceSwgDTO);
		dataImportValidationService.validate(newPriceRowModel);
		LOG.debug("Saving PriceRowModel to Database {}", newPriceRowModel.getCode());
		modelService.save(newPriceRowModel);
		markOnlineProductModified(newPriceRowModel.getProductId());
		LOG.debug("New PriceRowModel [{}] created Successfully", newPriceRowModel.getCode());
	}

	@Override
	public boolean isPriceRowAlreadyExist(final String priceId, final PriceSwgDTO priceSwgDTO)
	{
		return wileyPriceService.isPriceRowAlreadyExist(priceId);
	}

}
