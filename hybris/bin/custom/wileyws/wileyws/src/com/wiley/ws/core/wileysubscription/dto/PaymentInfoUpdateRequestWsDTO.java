package com.wiley.ws.core.wileysubscription.dto;

import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wiley.core.enums.PaymentModeEnum;


/**
 * Used to update WileySubscription payment info.
 */
public class PaymentInfoUpdateRequestWsDTO
{

	public enum PaymentType
	{
		CARD(PaymentModeEnum.CARD), INVOICE(PaymentModeEnum.INVOICE), PAYPAL(PaymentModeEnum.PAYPAL);

		private PaymentModeEnum value;

		PaymentType(final PaymentModeEnum value)
		{
			this.value = value;
		}

		public PaymentModeEnum getValue()
		{
			return value;
		}
	}

	public enum CreditCardType
	{
		VISA(de.hybris.platform.core.enums.CreditCardType.VISA),
		MASTER(de.hybris.platform.core.enums.CreditCardType.MASTER),
		AMEX(de.hybris.platform.core.enums.CreditCardType.AMEX),
		DISCOVER(de.hybris.platform.core.enums.CreditCardType.DISCOVER),
		DINERS(de.hybris.platform.core.enums.CreditCardType.DINERS);

		private de.hybris.platform.core.enums.CreditCardType value;

		CreditCardType(final de.hybris.platform.core.enums.CreditCardType value)
		{
			this.value = value;
		}

		public de.hybris.platform.core.enums.CreditCardType getValue()
		{
			return value;
		}
	}

	@NotNull
	@JsonProperty("paymentType")
	private PaymentType paymentType;

	@JsonProperty("paymentToken")
	private String paymentToken;

	@JsonProperty("creditCardType")
	private CreditCardType creditCardType;

	@JsonProperty("creditCardValidToMonth")
	private Integer creditCardValidToMonth;

	@JsonProperty("creditCardValidToYear")
	private Integer creditCardValidToYear;

	@Valid
	@NotNull
	@JsonProperty("paymentAddress")
	private WileySubscriptionAddressWsDTO paymentAddress;

	public PaymentType getPaymentType()
	{
		return paymentType;
	}

	public void setPaymentType(final PaymentType paymentType)
	{
		this.paymentType = paymentType;
	}

	public String getPaymentToken()
	{
		return paymentToken;
	}

	public void setPaymentToken(final String paymentToken)
	{
		this.paymentToken = paymentToken;
	}

	public CreditCardType getCreditCardType()
	{
		return creditCardType;
	}

	public void setCreditCardType(final CreditCardType creditCardType)
	{
		this.creditCardType = creditCardType;
	}

	public Integer getCreditCardValidToMonth()
	{
		return creditCardValidToMonth;
	}

	public void setCreditCardValidToMonth(final Integer creditCardValidToMonth)
	{
		this.creditCardValidToMonth = creditCardValidToMonth;
	}

	public Integer getCreditCardValidToYear()
	{
		return creditCardValidToYear;
	}

	public void setCreditCardValidToYear(final Integer creditCardValidToYear)
	{
		this.creditCardValidToYear = creditCardValidToYear;
	}

	public WileySubscriptionAddressWsDTO getPaymentAddress()
	{
		return paymentAddress;
	}

	public void setPaymentAddress(final WileySubscriptionAddressWsDTO paymentAddress)
	{
		this.paymentAddress = paymentAddress;
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (!(o instanceof PaymentInfoUpdateRequestWsDTO))
		{
			return false;
		}
		final PaymentInfoUpdateRequestWsDTO that = (PaymentInfoUpdateRequestWsDTO) o;
		return paymentType == that.paymentType
				&& Objects.equals(paymentToken, that.paymentToken)
				&& creditCardType == that.creditCardType
				&& Objects.equals(creditCardValidToMonth, that.creditCardValidToMonth)
				&& Objects.equals(creditCardValidToYear, that.creditCardValidToYear)
				&& Objects.equals(paymentAddress, that.paymentAddress);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(paymentType, paymentToken, creditCardType, creditCardValidToMonth, creditCardValidToYear,
				paymentAddress);
	}

	@Override
	public String toString()
	{
		return new ToStringBuilder(this)
				.append("paymentType", paymentType)
				.append("paymentToken", paymentToken)
				.append("creditCardType", creditCardType)
				.append("creditCardValidToMonth", creditCardValidToMonth)
				.append("creditCardValidToYear", creditCardValidToYear)
				.append("paymentAddress", paymentAddress)
				.toString();
	}
}
