package com.wiley.ws.core.product.crud.converter.populator.variantproduct;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.model.WileyPurchaseOptionProductModel;
import com.wiley.ws.core.product.crud.dto.WileyVariantProductWsDto;
import com.wiley.ws.core.product.dataintegration.common.exception.DataImportValidationErrorsException;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


public class CountryRestrictionsPopulator
		implements Populator<WileyVariantProductWsDto, WileyPurchaseOptionProductModel>
{
	private final CommonI18NService commonI18NService;

	@Autowired
	public CountryRestrictionsPopulator(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

	@Override
	public void populate(final WileyVariantProductWsDto variantProductWsDto,
			final WileyPurchaseOptionProductModel variantProductModel) throws ConversionException
	{
		validateParameterNotNull(variantProductWsDto, "VariantProductDto can't be null");
		validateParameterNotNull(variantProductModel, "VariantProductModel can't be null");

		final List<String> countryRestrictions = variantProductWsDto.getCountryRestrictions();

		List<CountryModel> countryRestrictionModels = null;
		if (countryRestrictions != null)
		{
			countryRestrictionModels = countryRestrictions
					.stream()
					.map(this::getCountry)
					.collect(Collectors.toList());

		}
		variantProductModel.setCountryRestrictions(countryRestrictionModels);
	}

	private CountryModel getCountry(final String country)
	{
		try
		{
			return commonI18NService.getCountry(country);
		}
		catch (UnknownIdentifierException e)
		{
			throw DataImportValidationErrorsException.ofNoReferenceException("ProductVariant", "countryRestrictions");
		}
	}
}