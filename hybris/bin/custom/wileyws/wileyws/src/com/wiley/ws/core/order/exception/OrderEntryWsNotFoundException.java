package com.wiley.ws.core.order.exception;

import de.hybris.platform.servicelayer.exceptions.SystemException;



/**
 * The type OrderEntry not found exception.
 */
public class OrderEntryWsNotFoundException extends SystemException
{
	public OrderEntryWsNotFoundException(final String message)
	{
		super(message);
	}
}