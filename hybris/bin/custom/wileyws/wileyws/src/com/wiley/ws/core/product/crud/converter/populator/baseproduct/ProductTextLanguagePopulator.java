package com.wiley.ws.core.product.crud.converter.populator.baseproduct;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.model.WileyProductModel;
import com.wiley.ws.core.product.crud.dto.WileyProductWsDto;
import com.wiley.ws.core.product.dataintegration.common.exception.DataImportValidationErrorsException;


public class ProductTextLanguagePopulator implements Populator<WileyProductWsDto, ProductModel>
{

	private static final String ERROR_FIELD_TEXT_LANGUAGE = "textLanguage";
	private final CommonI18NService commonI18NService;

	@Autowired
	public ProductTextLanguagePopulator(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

	@Override
	public void populate(final WileyProductWsDto wileyProductWsDto, final ProductModel wileyProductModel)
			throws ConversionException
	{
		final String textLanguage = wileyProductWsDto.getTextLanguage();
		if (StringUtils.isBlank(textLanguage))
		{
			return;
		}

		try
		{
			final LanguageModel language = commonI18NService.getLanguage(textLanguage);
			wileyProductModel.setTextLanguage(language);
		}
		catch (final UnknownIdentifierException e)
		{
			throw DataImportValidationErrorsException.ofNoReferenceException(WileyProductModel._TYPECODE,
					ERROR_FIELD_TEXT_LANGUAGE);
		}

	}
}
