package com.wiley.ws.core.b2b.unit.facade;

import com.wiley.ws.core.b2b.unit.dto.B2BUnitSwgDTO;


public interface WileyB2BUnitUpdateFacade
{
	boolean updateB2BUnit(String sapAccountNumber, B2BUnitSwgDTO b2bUnit);
}
