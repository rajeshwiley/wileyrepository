package com.wiley.ws.core.b2b.unit.exception;

public class B2BUnitUpdateNotFoundException extends RuntimeException
{
	public B2BUnitUpdateNotFoundException(final String message)
	{
		super(message);
	}

	public B2BUnitUpdateNotFoundException(final String message, final Throwable cause)
	{
		super(message, cause);
	}
}
