package com.wiley.ws.core.b2b.unit.dto;

import java.util.Objects;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * String value with the associated locale.
 */
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-07-26T11:08:47.854Z")
public class LocalizedStringSwgDTO
{

	private String loc = null;
	private String val = null;


	/**
	 * Standard Java locale code. See https://docs.oracle.com/javase/8/docs/api/java/util/Locale.html for details.
	 * The method https://commons.apache.org/proper/commons-lang/javadocs/api-2.6/org/apache/commons/lang/LocaleUtils.html
	 *    #toLocale(java.lang.String)
	 * will be used to parse the locale's string representation.
	 * For example, en_US
	 **/
	public LocalizedStringSwgDTO loc(final String loc)
	{
		this.loc = loc;
		return this;
	}

	@NotNull
	@Pattern(regexp = "^[a-z]{2}(_[A-Z]{2}(_.+)?)?$")
	@JsonProperty("loc")
	public String getLoc()
	{
		return loc;
	}

	public void setLoc(final String loc)
	{
		this.loc = loc;
	}


	/**
	 **/
	public LocalizedStringSwgDTO val(final String val)
	{
		this.val = val;
		return this;
	}

	@NotNull
	@Size(max = 255)
	@JsonProperty("val")
	public String getVal()
	{
		return val;
	}

	public void setVal(final String val)
	{
		this.val = val;
	}


	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		final LocalizedStringSwgDTO localizedStringSwgDTO = (LocalizedStringSwgDTO) o;
		return Objects.equals(this.loc, localizedStringSwgDTO.loc)
				&& Objects.equals(this.val, localizedStringSwgDTO.val);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(loc, val);
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("class LocalizedString {\n");

		sb.append("    loc: ").append(toIndentedString(loc)).append("\n");
		sb.append("    val: ").append(toIndentedString(val)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(final Object o)
	{
		if (o == null)
		{
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}

