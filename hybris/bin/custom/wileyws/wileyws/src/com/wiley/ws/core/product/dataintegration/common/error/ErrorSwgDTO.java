package com.wiley.ws.core.product.dataintegration.common.error;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;


/**
 * ErrorWs
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public final class ErrorSwgDTO implements Serializable
{

	private static final long serialVersionUID = 1L;

	private ErrorSwgDTO()
	{
	}

	public enum TypeEnum
	{
		VALIDATION("validation"),

		TRANSFORMATION("transformation"),

		PARSING("parsing");

		private String value;

		TypeEnum(final String value)
		{
			this.value = value;
		}

		@Override
		public String toString()
		{
			return value;
		}
	}

	/**
	 * Provided in case of validation errors only. Presents the kind of issue with error subject
	 */
	public enum ReasonEnum
	{
		INVALID("invalid"),

		MISSING("missing"),

		JSON_PARSE_ERROR("json_parse_error"),

		BAD_REFERENCE_ERROR("bad_reference_error"),

		NOT_UNIQUE_ID("not_unique_id"),

		IMMUTABLE_FIELD_ERROR("immutable_field_error");

		private String value;

		ReasonEnum(final String value)
		{
			this.value = value;
		}

		@Override
		public String toString()
		{
			return value;
		}

	}

	private String type = null;

	private String reason = null;

	private String message = null;

	private String subjectType = null;

	private String subject = null;

	/**
	 * Type of an error occurred during processing of request
	 *
	 * @return type
	 **/
	public String getType()
	{
		return type;
	}

	public void setType(final String type)
	{
		this.type = type;
	}

	/**
	 * Provided in case of validation errors only. Presents the kind of issue with error subject
	 *
	 * @return reason
	 **/
	public String getReason()
	{
		return reason;
	}

	public void setReason(final String reason)
	{
		this.reason = reason;
	}

	/**
	 * Detailed error-related message
	 *
	 * @return message
	 **/
	public String getMessage()
	{
		return message;
	}

	public void setMessage(final String message)
	{
		this.message = message;
	}

	/**
	 * Provided in case of validation errors only. Values other from \"parameter\" have been reserved for internal needs.
	 *
	 * @return subjectType
	 **/
	public String getSubjectType()
	{
		return subjectType;
	}

	public void setSubjectType(final String subjectType)
	{
		this.subjectType = subjectType;
	}

	/**
	 * Provided in case of validation errors only. Depending on whether validation issue belongs to object
	 * or localized at some object field it could be object name or field name accordingly.
	 *
	 * @return subject
	 **/
	public String getSubject()
	{
		return subject;
	}

	public void setSubject(final String subject)
	{
		this.subject = subject;
	}

	public static ErrorSwgDTO of(
			final String type,
			final String reason,
			final String message,
			final String subjectType,
			final String subject)
	{
		final ErrorSwgDTO errorWsDto = new ErrorSwgDTO();
		errorWsDto.type = type;
		errorWsDto.reason = reason;
		errorWsDto.message = message;
		errorWsDto.subjectType = subjectType;
		errorWsDto.subject = subject;
		return errorWsDto;
	}

	public static ErrorSwgDTO of(
			final TypeEnum type,
			final ReasonEnum reason,
			final String message,
			final String subjectType,
			final String subject)
	{
		return of(type.toString(), reason.toString(), message, subjectType, subject);
	}
}

