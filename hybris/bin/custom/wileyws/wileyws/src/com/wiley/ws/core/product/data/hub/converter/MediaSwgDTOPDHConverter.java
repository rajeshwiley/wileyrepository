package com.wiley.ws.core.product.data.hub.converter;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaModel;

import com.wiley.ws.core.product.data.hub.dto.MediaSwgDTO;


/**
 * MediaSwgDTO to MediaModel converter
 */
public interface MediaSwgDTOPDHConverter
{
	MediaModel convertToMediaModel(MediaSwgDTO media, String code, CatalogVersionModel catalogVersion);
}
