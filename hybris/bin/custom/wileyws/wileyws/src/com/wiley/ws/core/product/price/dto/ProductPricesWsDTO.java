package com.wiley.ws.core.product.price.dto;

import java.io.Serializable;
import java.util.List;

import com.wiley.ws.core.product.dataintegration.price.dto.PriceSwgDTO;


public class ProductPricesWsDTO implements Serializable
{

	/**
	 * Defines whether prices are stackable.
	 */
	private boolean stackable;

	private List<PriceSwgDTO> prices;

	public boolean isStackable()
	{
		return stackable;
	}

	public void setStackable(final boolean stackable)
	{
		this.stackable = stackable;
	}

	public List<PriceSwgDTO> getPrices()
	{
		return prices;
	}

	public void setPrices(final List<PriceSwgDTO> prices)
	{
		this.prices = prices;
	}
}
