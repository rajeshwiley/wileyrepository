package com.wiley.ws.core.order.populator;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.order.DeliveryModeService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;

import org.apache.commons.collections.CollectionUtils;

import com.wiley.core.address.WileyAddressService;
import com.wiley.core.model.SchoolModel;
import com.wiley.core.wileyb2b.unit.WileyB2BUnitService;
import com.wiley.core.wileycom.customer.service.WileycomCustomerAccountService;
import com.wiley.core.wileycom.order.WileycomOrderEntryService;
import com.wiley.ws.core.order.dto.OrderAddressWsDTO;
import com.wiley.ws.core.order.dto.OrderUpdateEntryRequestWsDTO;
import com.wiley.ws.core.order.dto.OrderUpdateRequestWsDTO;
import com.wiley.ws.core.order.exception.SchoolWsNotFoundException;
import com.wiley.ws.core.order.service.WileycomDigitalTokenApplier;
import com.wiley.ws.core.order.service.WileycomExternalDiscountApplier;
import com.wiley.ws.core.order.service.WileycomSubtotalWithoutDiscountApplier;

import static com.wiley.facades.populators.PopulateUtils.populateFieldIfNotNull;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;
import static org.apache.commons.collections.CollectionUtils.subtract;


/**
 * Author Herman_Chukhrai (EPAM)
 */
public class WileycomOrderReversePopulator implements Populator<OrderUpdateRequestWsDTO, AbstractOrderModel>
{

	@Resource
	private ModelService modelService;
	@Resource
	private CommonI18NService commonI18NService;
	@Resource
	private WileyB2BUnitService wileyB2BUnitService;
	@Resource
	private WileycomCustomerAccountService wileycomCustomerAccountService;
	@Resource
	private DeliveryModeService deliveryModeService;
	@Resource
	private Populator<OrderUpdateEntryRequestWsDTO, AbstractOrderEntryModel> wileycomOrderEntryReversePopulator;
	@Resource
	private Populator<OrderAddressWsDTO, AddressModel> wileycomAddressWsReversePopulator;
	@Resource
	private Populator<OrderAddressWsDTO, AddressModel> wileycomOrderAddressExternalIdReversePopulator;
	@Resource
	private WileyAddressService wileyAddressService;
	@Resource
	private WileycomOrderEntryService wileycomOrderEntryService;
	@Resource
	private WileycomExternalDiscountApplier wileycomExternalDiscountApplier;
	@Resource
	private WileycomDigitalTokenApplier wileycomDigitalTokenApplier;
	@Resource
	private WileycomSubtotalWithoutDiscountApplier wileycomSubtotalWithoutDiscountApplier;

	@Override
	public void populate(@NotNull final OrderUpdateRequestWsDTO source, @NotNull final AbstractOrderModel target)
			throws ConversionException
	{
		validateParameterNotNullStandardMessage("source", source);
		validateParameterNotNullStandardMessage("target", target);

		populateFieldIfNotNull(this::populateUnit, target, source.getSapAccountNumber());
		populateFieldIfNotNull(this::populateCountry, target, source.getCountry());
		populateFieldIfNotNull(this::populateCurrency, target, source.getCurrency());
		populateFieldIfNotNull(this::populateSchool, target, source.getSchoolId());
		populateFieldIfNotNull(this::populateDeliveryMode, target, source.getDeliveryOptionCode());
		populateFieldIfNotNull(AbstractOrderModel::setSubtotal, target, source.getSubtotal());
		populateFieldIfNotNull(AbstractOrderModel::setTotalDiscounts, target, source.getTotalDiscount());
		populateFieldIfNotNull(AbstractOrderModel::setDeliveryCost, target, source.getDeliveryCost());
		populateFieldIfNotNull(AbstractOrderModel::setTotalTax, target, source.getTotalTax());
		populateFieldIfNotNull(AbstractOrderModel::setTotalPrice, target, source.getTotalPrice());
		if (CollectionUtils.isNotEmpty(source.getEntries()))
		{
			populateEntries(target, source.getEntries());
		}
		populateFieldIfNotNull(this::populateDeliveryAddress, target, source.getDeliveryAddress());
		populateFieldIfNotNull(this::populatePaymentAddress, target, source.getPaymentAddress());
		wileycomExternalDiscountApplier.applyDiscounts(target, source.getDiscounts());
		wileycomDigitalTokenApplier.applyDigitalTokens(target, source.getDigitalProductAccessTokens());
		wileycomSubtotalWithoutDiscountApplier.recalculateAndSetSubtotalWithoutDiscount(target);
	}

	private void populatePaymentAddress(final AbstractOrderModel orderModel, final OrderAddressWsDTO addressDTO)
	{
		AddressModel addressModel = getNotNullAddress(orderModel, orderModel.getPaymentAddress());
		orderModel.setPaymentAddress(transformAddress(addressModel, addressDTO));
	}

	private void populateDeliveryAddress(final AbstractOrderModel orderModel, final OrderAddressWsDTO addressDTO)
	{
		AddressModel addressModel = getNotNullAddress(orderModel, orderModel.getDeliveryAddress());
		orderModel.setDeliveryAddress(transformAddress(addressModel, addressDTO));
	}

	private AddressModel getNotNullAddress(final AbstractOrderModel orderModel, final AddressModel addressModel)
	{
		if (addressModel == null)
		{
			AddressModel newAddressModel = modelService.create(AddressModel.class);
			newAddressModel.setOwner(orderModel);
			return newAddressModel;
		}
		return addressModel;
	}

	private AddressModel transformAddress(final AddressModel addressModel, final OrderAddressWsDTO addressDTO)
	{
		wileycomOrderAddressExternalIdReversePopulator.populate(addressDTO, addressModel);
		wileycomAddressWsReversePopulator.populate(addressDTO, addressModel);
		return addressModel;
	}

	private void populateEntries(final AbstractOrderModel orderModel, final List<OrderUpdateEntryRequestWsDTO> entries)
	{
		final List<AbstractOrderEntryModel> updatedEntries = new ArrayList<>();
		entries.forEach(entry -> {

			final AbstractOrderEntryModel orderEntryModel = wileycomOrderEntryService
					.findOrderEntryByIsbn(orderModel, entry.getIsbn())
					.orElseGet(() -> {
						final OrderEntryModel entryModel = modelService.create(OrderEntryModel.class);
						entryModel.setOrder(orderModel);
						return entryModel;
					});
			wileycomOrderEntryReversePopulator.populate(entry, orderEntryModel);
			updatedEntries.add(orderEntryModel);
		});

		final Collection<AbstractOrderEntryModel> entriesToRemove = subtract(orderModel.getEntries(), updatedEntries);
		final Collection<AbstractOrderEntryModel> entriesToUpdate = subtract(orderModel.getEntries(), entriesToRemove);
		modelService.removeAll(entriesToRemove);

		final Collection<AbstractOrderEntryModel> allEntries = new HashSet<>();
		allEntries.addAll(entriesToUpdate);
		allEntries.addAll(updatedEntries);

		orderModel.setEntries(new ArrayList<>(allEntries));
	}

	private void populateUnit(final AbstractOrderModel target, final String sapAccountNumber)
	{
		final B2BUnitModel b2BUnit = wileyB2BUnitService.getB2BUnitForSapAccountNumber(sapAccountNumber);
		target.setUnit(b2BUnit);
	}

	private void populateCountry(final AbstractOrderModel target, final String countryCode)
	{
		target.setCountry(commonI18NService.getCountry(countryCode));
	}

	private void populateCurrency(final AbstractOrderModel target, final String currencyCode)
	{
		target.setCurrency(commonI18NService.getCurrency(currencyCode));
	}

	private void populateSchool(final AbstractOrderModel target, final String schoolId)
	{
		final Optional<SchoolModel> school = wileycomCustomerAccountService.getSchoolByCode(schoolId);
		if (school.isPresent())
		{
			target.setSchool(school.get());
		}
		else
		{
			throw new SchoolWsNotFoundException(MessageFormat.format("School with code [{0}] wasn't found", schoolId));
		}
	}

	private void populateDeliveryMode(final AbstractOrderModel target, final String deliveryOptionCode)
	{
		target.setDeliveryMode(deliveryModeService.getDeliveryModeForCode(deliveryOptionCode));
	}

}
