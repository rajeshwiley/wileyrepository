package com.wiley.ws.core.product.crud.converter.populator.baseproduct;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.category.WileyCategoryService;
import com.wiley.core.model.WileyProductModel;
import com.wiley.ws.core.product.crud.dto.WileyProductWsDto;
import com.wiley.ws.core.product.dataintegration.common.exception.DataImportValidationErrorsException;
import com.wiley.ws.core.product.crud.facade.WileyProductCrudCatalogVersionProvider;


public class CategoriesPopulator implements Populator<WileyProductWsDto, WileyProductModel>
{
	private final WileyCategoryService wileyCategoryService;
	private final WileyProductCrudCatalogVersionProvider wileyProductCrudCatalogVersionProvider;

	@Autowired
	public CategoriesPopulator(final WileyCategoryService wileyCategoryService,
			final WileyProductCrudCatalogVersionProvider wileyProductCrudCatalogVersionProvider)
	{
		this.wileyCategoryService = wileyCategoryService;
		this.wileyProductCrudCatalogVersionProvider = wileyProductCrudCatalogVersionProvider;
	}

	@Override
	public void populate(final WileyProductWsDto wileyProductWsDto, final WileyProductModel wileyProductModel)
			throws ConversionException
	{
		final CatalogVersionModel catalogVersion = wileyProductCrudCatalogVersionProvider.getCatalogVersion();

		populateCategories(wileyProductWsDto.getSubjectIds(), wileyProductModel, catalogVersion,
				ProductModel::setSubjects, "subjectIds");
		populateCategories(wileyProductWsDto.getBrandIds(), wileyProductModel, catalogVersion,
				ProductModel::setBrands, "brandIds");
		populateCategories(wileyProductWsDto.getSeriesIds(), wileyProductModel, catalogVersion,
				ProductModel::setSeries, "seriesIds");
		populateCategories(wileyProductWsDto.getCourseIds(), wileyProductModel, catalogVersion,
				ProductModel::setCourses, "courseIds");
	}

	private void populateCategories(final List<String> categoryIds,
			final WileyProductModel wileyProductModel,
			final CatalogVersionModel catalogVersion,
			final BiConsumer<WileyProductModel, List<CategoryModel>> consumer,
			final String subject
	)
	{
		final List<CategoryModel> categories = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(categoryIds))
		{
			for (final String categoryId : categoryIds)
			{
				try
				{
					final CategoryModel categoryModel = wileyCategoryService.getCategoryForCode(catalogVersion, categoryId);
					categories.add(categoryModel);
				}
				catch (final UnknownIdentifierException e)
				{
					throw DataImportValidationErrorsException.ofNoReferenceException(WileyProductModel._TYPECODE, subject);
				}
			}
		}
		consumer.accept(wileyProductModel, categories);
	}
}
