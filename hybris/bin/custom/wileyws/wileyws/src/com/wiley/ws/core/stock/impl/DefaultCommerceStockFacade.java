/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package com.wiley.ws.core.stock.impl;

import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.commerceservices.stock.CommerceStockService;
import de.hybris.platform.webservicescommons.util.YSanitizer;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import de.hybris.platform.storelocator.pos.PointOfServiceService;

import org.springframework.beans.factory.annotation.Required;

import com.wiley.ws.core.stock.CommerceStockFacade;
import com.wiley.ws.core.strategies.BaseStoreForSiteSelectorStrategy;


/**
 * Default implementation of {@link com.wiley.ws.core.stock.CommerceStockFacade}<br/>
 * TODO: when moving to commercelayer:<br/>
 * TODO: please add validation to the default implementation of BaseSiteService and throw exceptions from there<br/>
 * TODO: ideally get rid of createStockData()
 */
public class DefaultCommerceStockFacade implements CommerceStockFacade
{
	private BaseSiteService baseSiteService;
	private CommerceStockService commerceStockService;
	private ProductService productService;
	private PointOfServiceService pointOfServiceService;
	private BaseStoreForSiteSelectorStrategy baseStoreForSiteSelectorStrategy;

	/**
	 * Is stock system enabled boolean.
	 *
	 * @param baseSiteId
	 * 		the base site id
	 * @return the boolean
	 * @throws UnknownIdentifierException
	 * 		the unknown identifier exception
	 */
	@Override
	public boolean isStockSystemEnabled(final String baseSiteId) throws UnknownIdentifierException
	{
		// it's not checked in the service layer (!) : 
		ServicesUtil.validateParameterNotNull(baseSiteId, "Parameter baseSiteId must not be null");
		final BaseSiteModel baseSiteModel = getBaseSiteService().getBaseSiteForUID(baseSiteId);
		if (baseSiteModel == null)
		{
			throw new UnknownIdentifierException("Base site with uid '" + YSanitizer.sanitize(baseSiteId) + "' not found!");
		}

		return getCommerceStockService().isStockSystemEnabled(getBaseStoreForSiteSelectorStrategy().getBaseStore(baseSiteModel));
	}

	/**
	 * Gets stock data for product and base site.
	 *
	 * @param productCode
	 * 		the product code
	 * @param baseSiteId
	 * 		the base site id
	 * @return the stock data for product and base site
	 * @throws UnknownIdentifierException
	 * 		the unknown identifier exception
	 * @throws IllegalArgumentException
	 * 		the illegal argument exception
	 * @throws AmbiguousIdentifierException
	 * 		the ambiguous identifier exception
	 */
	@Override
	public StockData getStockDataForProductAndBaseSite(final String productCode, final String baseSiteId)
			throws UnknownIdentifierException, IllegalArgumentException, AmbiguousIdentifierException
	{
		// it's not checked in the service layer (!) :
		ServicesUtil.validateParameterNotNull(baseSiteId, "Parameter baseSiteId must not be null");
		final BaseSiteModel baseSiteModel = getBaseSiteService().getBaseSiteForUID(baseSiteId);
		if (baseSiteModel == null)
		{
			throw new UnknownIdentifierException("Base site with uid '" + YSanitizer.sanitize(baseSiteId) + "' not found!");
		}

		final ProductModel productModel = getProductService().getProductForCode(productCode);

		return createStockData(getCommerceStockService().getStockLevelStatusForProductAndBaseStore(productModel,
						getBaseStoreForSiteSelectorStrategy().getBaseStore(baseSiteModel)),
				getCommerceStockService().getStockLevelForProductAndBaseStore(productModel,
						getBaseStoreForSiteSelectorStrategy().getBaseStore(baseSiteModel)));
	}

	/**
	 * Gets stock data for product and point of service.
	 *
	 * @param productCode
	 * 		the product code
	 * @param storeName
	 * 		the store name
	 * @return the stock data for product and point of service
	 * @throws UnknownIdentifierException
	 * 		the unknown identifier exception
	 * @throws IllegalArgumentException
	 * 		the illegal argument exception
	 * @throws AmbiguousIdentifierException
	 * 		the ambiguous identifier exception
	 */
	@Override
	public StockData getStockDataForProductAndPointOfService(final String productCode, final String storeName)
			throws UnknownIdentifierException, IllegalArgumentException, AmbiguousIdentifierException
	{
		final ProductModel productModel = getProductService().getProductForCode(productCode);
		final PointOfServiceModel pointOfServiceModel = getPointOfServiceService().getPointOfServiceForName(storeName);

		return createStockData(getCommerceStockService().getStockLevelStatusForProductAndPointOfService(productModel,
				pointOfServiceModel), getCommerceStockService().getStockLevelForProductAndPointOfService(productModel,
				pointOfServiceModel));
	}

	/**
	 * This method is used here instead of regular populator beacause {@link CommerceStockService} returns all values
	 * separately.<br/>
	 * Ideally would be to improve it someday by returning StockLevelModel.
	 *
	 * @param stockLevelStatus
	 * 		stock level status
	 * @param stockLevel
	 * 		stock level
	 * @return stockData stock data
	 */
	protected StockData createStockData(final StockLevelStatus stockLevelStatus, final Long stockLevel)
	{
		final StockData stockData = new StockData();
		stockData.setStockLevelStatus(stockLevelStatus);
		stockData.setStockLevel(stockLevel);
		return stockData;
	}

	/**
	 * Gets commerce stock service.
	 *
	 * @return the commerce stock service
	 */
	public CommerceStockService getCommerceStockService()
	{
		return commerceStockService;
	}

	/**
	 * Sets commerce stock service.
	 *
	 * @param commerceStockService
	 * 		the commerce stock service
	 */
	@Required
	public void setCommerceStockService(final CommerceStockService commerceStockService)
	{
		this.commerceStockService = commerceStockService;
	}

	/**
	 * Gets base site service.
	 *
	 * @return the base site service
	 */
	public BaseSiteService getBaseSiteService()
	{
		return baseSiteService;
	}

	/**
	 * Sets base site service.
	 *
	 * @param baseSiteService
	 * 		the base site service
	 */
	@Required
	public void setBaseSiteService(final BaseSiteService baseSiteService)
	{
		this.baseSiteService = baseSiteService;
	}

	/**
	 * Gets base store for site selector strategy.
	 *
	 * @return the base store for site selector strategy
	 */
	public BaseStoreForSiteSelectorStrategy getBaseStoreForSiteSelectorStrategy()
	{
		return baseStoreForSiteSelectorStrategy;
	}

	/**
	 * Sets base store for site selector strategy.
	 *
	 * @param baseStoreForSiteSelectorStrategy
	 * 		the base store for site selector strategy
	 */
	@Required
	public void setBaseStoreForSiteSelectorStrategy(final BaseStoreForSiteSelectorStrategy baseStoreForSiteSelectorStrategy)
	{
		this.baseStoreForSiteSelectorStrategy = baseStoreForSiteSelectorStrategy;
	}

	/**
	 * Gets product service.
	 *
	 * @return the product service
	 */
	public ProductService getProductService()
	{
		return productService;
	}

	/**
	 * Sets product service.
	 *
	 * @param productService
	 * 		the product service
	 */
	@Required
	public void setProductService(final ProductService productService)
	{
		this.productService = productService;
	}

	/**
	 * Gets point of service service.
	 *
	 * @return the point of service service
	 */
	public PointOfServiceService getPointOfServiceService()
	{
		return pointOfServiceService;
	}

	/**
	 * Sets point of service service.
	 *
	 * @param pointOfServiceService
	 * 		the point of service service
	 */
	@Required
	public void setPointOfServiceService(final PointOfServiceService pointOfServiceService)
	{
		this.pointOfServiceService = pointOfServiceService;
	}
}
