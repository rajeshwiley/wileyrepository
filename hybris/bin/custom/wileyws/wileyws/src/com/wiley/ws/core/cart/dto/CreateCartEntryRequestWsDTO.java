package com.wiley.ws.core.cart.dto;

import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.DefaultValue;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;

import com.wiley.integrations.order.dto.DiscountValueWsDTO;
import com.wiley.integrations.order.dto.PriceValueWsDTO;

/**
 * CreateCartEntryRequest DTO
 */
public class CreateCartEntryRequestWsDTO
{
	@NotNull
	@Size(max = 255)
	private String productCode;

	@Size(max = 255)
	private String businessItemId;

	@Size(max = 255)
	private String businessKey;

	@Min(1)
	@DefaultValue("1")
	private Integer quantity;

	@Valid
	private List<PriceValueWsDTO> externalPrices;

	@Valid
	private List<DiscountValueWsDTO> externalDiscounts;

	private Date deliveryDate;

	@Size(max = 21844)
	private String additionalInfo;

	@JsonProperty("productCode")
	public String getProductCode()
	{
		return productCode;
	}

	public void setProductCode(final String productCode)
	{
		this.productCode = productCode;
	}

	@JsonProperty("businessItemId")
	public String getBusinessItemId()
	{
		return businessItemId;
	}

	public void setBusinessItemId(final String businessItemId)
	{
		this.businessItemId = businessItemId;
	}

	@JsonProperty("businessKey")
	public String getBusinessKey()
	{
		return businessKey;
	}

	public void setBusinessKey(final String businessKey)
	{
		this.businessKey = businessKey;
	}

	@JsonProperty("quantity")
	public Integer getQuantity()
	{
		return quantity;
	}

	public void setQuantity(final Integer quantity)
	{
		this.quantity = quantity;
	}

	@JsonProperty("externalPrices")
	public List<PriceValueWsDTO> getExternalPrices()
	{
		return externalPrices;
	}

	public void setExternalPrices(final List<PriceValueWsDTO> externalPrices)
	{
		this.externalPrices = externalPrices;
	}

	@JsonProperty("externalDiscounts")
	public List<DiscountValueWsDTO> getExternalDiscounts()
	{
		return externalDiscounts;
	}

	public void setExternalDiscounts(final List<DiscountValueWsDTO> externalDiscounts)
	{
		this.externalDiscounts = externalDiscounts;
	}

	@JsonProperty("deliveryDate")
	public Date getDeliveryDate()
	{
		return deliveryDate;
	}

	public void setDeliveryDate(final Date deliveryDate)
	{
		this.deliveryDate = deliveryDate;
	}

	@JsonProperty("additionalInfo")
	public String getAdditionalInfo()
	{
		return additionalInfo;
	}

	public void setAdditionalInfo(final String additionalInfo)
	{
		this.additionalInfo = additionalInfo;
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		final CreateCartEntryRequestWsDTO that = (CreateCartEntryRequestWsDTO) o;
		return quantity == that.quantity
				&& Objects.equals(productCode, that.productCode)
				&& Objects.equals(businessItemId, that.businessItemId)
				&& Objects.equals(businessKey, that.businessKey)
				&& Objects.equals(externalPrices, that.externalPrices)
				&& Objects.equals(externalDiscounts, that.externalDiscounts)
				&& Objects.equals(deliveryDate, that.deliveryDate)
				&& Objects.equals(additionalInfo, that.additionalInfo);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(productCode,
				businessItemId,
				businessKey,
				quantity,
				externalPrices,
				externalDiscounts,
				deliveryDate,
				additionalInfo);
	}

	@Override
	public String toString()
	{
		return new ToStringBuilder(this)
				.append("productCode", productCode)
				.append("businessItemId", businessItemId)
				.append("businessKey", businessKey)
				.append("quantity", quantity)
				.append("externalPrices", externalPrices)
				.append("externalDiscounts", externalDiscounts)
				.append("deliveryDate", deliveryDate)
				.append("additionalInfo", additionalInfo)
				.toString();
	}
}
