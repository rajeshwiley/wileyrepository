package com.wiley.ws.core.cart.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.validation.constraints.NotNull;

import com.wiley.ws.core.cart.dto.CartEntryWsDTO;

import static com.wiley.facades.populators.PopulateUtils.populateFieldIfNotNull;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


/**
 * Author Maksim_Kozich
 */
public class WileyasCartEntryModelPopulator implements Populator<AbstractOrderEntryModel, CartEntryWsDTO>
{
	@Override
	public void populate(@NotNull final AbstractOrderEntryModel source, @NotNull final CartEntryWsDTO target)
			throws ConversionException
	{
		validateParameterNotNullStandardMessage("source", source);
		validateParameterNotNullStandardMessage("target", target);

		populateFieldIfNotNull(this::populateCartId, target, source.getOrder());
		populateFieldIfNotNull(CartEntryWsDTO::setEntryId, target, source.getGuid());
		populateFieldIfNotNull(this::populateProductCode, target, source.getProduct());
		populateFieldIfNotNull(this::populateBusinessItemId, target, source);
		populateFieldIfNotNull(this::populateQuantity, target, source.getQuantity());
		populateFieldIfNotNull(CartEntryWsDTO::setAdditionalInfo, target, source.getAdditionalInfo());
	}

	private void populateCartId(final CartEntryWsDTO target, final AbstractOrderModel cartModel)
	{
		target.setCartId(cartModel.getGuid());
	}

	private void populateProductCode(final CartEntryWsDTO target, final ProductModel productModel)
	{
		target.setProductCode(productModel.getCode());
	}

	private void populateBusinessItemId(final CartEntryWsDTO target, final AbstractOrderEntryModel source)
	{
		target.setBusinessItemId(source.getBusinessItemId());
	}

	private void populateQuantity(final CartEntryWsDTO target, final Long quantity)
	{
		target.setQuantity(quantity.intValue());
	}

}
