package com.wiley.ws.core.address.dto;

import java.util.Objects;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Common Address DTO
 */
public class AddressWsDTO
{
	/**
	 * Postal code.
	 */
	@NotNull
	@Size(max = 255)
	private String postcode;
	/**
	 * Two-letter ISO 3166-1 alpha-2 code that specifies a country. See https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2
	 * for details.
	 */
	@NotNull
	private String country;
	/**
	 * Two-letter ISO code that specifies a state or province. The codes were derived from ISO 3166-2 codes, for non-standard
	 * Armed Forces regions values were provided by Wiley. See https://jira.wiley.ru/browse/ECSC-5892 for details.
	 */
	private String state;
	/**
	 * Destination city.
	 */
	@NotNull
	@Size(max = 255)
	private String city;
	/**
	 * First line of the address.
	 */
	@NotNull
	@Size(max = 255)
	private String line1;
	/**
	 * Second line of the address.
	 */
	@Size(max = 255)
	private String line2;
	/**
	 * Phone number.
	 */
	@Size(max = 255)
	private String phoneNumber;

	@JsonProperty("postcode")
	public String getPostcode()
	{
		return postcode;
	}

	public void setPostcode(final String postcode)
	{
		this.postcode = postcode;
	}

	@JsonProperty("country")
	public String getCountry()
	{
		return country;
	}

	public void setCountry(final String country)
	{
		this.country = country;
	}

	@JsonProperty("state")
	public String getState()
	{
		return state;
	}

	public void setState(final String state)
	{
		this.state = state;
	}

	@JsonProperty("city")
	public String getCity()
	{
		return city;
	}

	public void setCity(final String city)
	{
		this.city = city;
	}

	@JsonProperty("line1")
	public String getLine1()
	{
		return line1;
	}

	public void setLine1(final String line1)
	{
		this.line1 = line1;
	}

	@JsonProperty("line2")
	public String getLine2()
	{
		return line2;
	}

	public void setLine2(final String line2)
	{
		this.line2 = line2;
	}

	@JsonProperty("phoneNumber")
	public String getPhoneNumber()
	{
		return phoneNumber;
	}

	public void setPhoneNumber(final String phoneNumber)
	{
		this.phoneNumber = phoneNumber;
	}


	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (!(o instanceof AddressWsDTO))
		{
			return false;
		}
		final AddressWsDTO that = (AddressWsDTO) o;
		return Objects.equals(postcode, that.postcode)
				&& Objects.equals(country, that.country)
				&& Objects.equals(state, that.state)
				&& Objects.equals(city, that.city)
				&& Objects.equals(line1, that.line1)
				&& Objects.equals(line2, that.line2)
				&& Objects.equals(phoneNumber, that.phoneNumber);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(postcode, country, state, city, line1, line2, phoneNumber);
	}
}
