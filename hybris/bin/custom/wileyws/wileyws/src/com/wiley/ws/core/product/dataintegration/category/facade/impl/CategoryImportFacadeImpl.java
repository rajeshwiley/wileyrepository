package com.wiley.ws.core.product.dataintegration.category.facade.impl;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.converters.impl.AbstractConverter;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.wiley.ws.core.product.dataintegration.category.dto.CategorySwgDTO;
import com.wiley.ws.core.product.dataintegration.category.dto.UpdateCategorySwgDTO;
import com.wiley.ws.core.product.dataintegration.category.facade.CategoryImportFacade;
import com.wiley.ws.core.product.dataintegration.category.service.CategoryImportService;
import com.wiley.ws.core.product.dataintegration.common.exception.DataImportNotFoundException;
import com.wiley.ws.core.product.dataintegration.common.service.DataImportValidationService;


public class CategoryImportFacadeImpl implements CategoryImportFacade
{
	private static final Logger LOG = LoggerFactory.getLogger(CategoryImportFacadeImpl.class);

	private final ModelService modelService;
	private final DataImportValidationService dataImportValidationService;
	private final CategoryImportService categoryImportService;
	private final AbstractConverter<CategorySwgDTO, CategoryModel> categoryImportConverter;

	@Autowired
	public CategoryImportFacadeImpl(final ModelService modelService,
			@Qualifier("categoryImportValidationService") final DataImportValidationService dataImportValidationService,
			final CategoryImportService categoryImportService,
			@Qualifier("categoryImportConverter") final AbstractConverter<CategorySwgDTO, CategoryModel> categoryImportConverter)
	{
		this.modelService = modelService;
		this.dataImportValidationService = dataImportValidationService;
		this.categoryImportService = categoryImportService;
		this.categoryImportConverter = categoryImportConverter;
	}

	@Override
	public void createCategory(final CategorySwgDTO categorySwgDTO)
	{
		LOG.debug("Creating category with data: {}", categorySwgDTO);
		dataImportValidationService.validateItemDoesNotExist(categorySwgDTO.getId());
		final CategoryModel categoryModel = modelService.create(CategoryModel.class);
		categoryImportConverter.populate(categorySwgDTO, categoryModel);
		dataImportValidationService.validate(categoryModel);
		modelService.save(categoryModel);
		LOG.debug("Category with code [{}] has been created", categoryModel.getCode());
	}

	@Override
	public void updateCategory(final String categoryId, final UpdateCategorySwgDTO updateCategorySwgDTO)
	{
		LOG.debug("Updating category with data: {}", updateCategorySwgDTO);
		try
		{
			final CategoryModel categoryModel = categoryImportService.getCategoryForCode(categoryId);
			categoryImportConverter.populate(new CategorySwgDTO(updateCategorySwgDTO), categoryModel);
			dataImportValidationService.validate(categoryModel);
			modelService.save(categoryModel);
			LOG.debug("Category with code [{}] has been updated", categoryModel.getCode());
		}
		catch (UnknownIdentifierException e)
		{
			throw new DataImportNotFoundException();
		}
	}

	@Override
	public void deleteCategory(final String categoryId)
	{
		LOG.debug("Deleting category with code {}", categoryId);
		try
		{
			final CategoryModel categoryModel = categoryImportService.getCategoryForCode(categoryId);
			modelService.remove(categoryModel);
			LOG.debug("category with code [{}] has been deleted", categoryId);
		}
		catch (UnknownIdentifierException e)
		{
			throw new DataImportNotFoundException();
		}
	}

}
