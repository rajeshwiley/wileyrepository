package com.wiley.ws.core.order;

import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.order.InvalidCartException;

import com.wiley.facades.product.exception.RepeatedFreeTrialOrderException;
import com.wiley.ws.core.order.dto.FreeTrialResponseWsDTO;
import com.wiley.ws.core.order.dto.FreeTrialWsDTO;


/**
 * Facade for Free Trial Order Creation
 */
public interface WelFreeTrialOrderWsFacade
{
	/**
	 *  Enable search restrictions
	 */
	void enableSearchRestrictions();

	/**
	 *  Registration a new user
	 * @param freeTrialWsDTO
	 * @throws DuplicateUidException
	 */
	void registerNewUser(FreeTrialWsDTO freeTrialWsDTO) throws DuplicateUidException;

	/**
	 *  Login the current user
	 */
	void loginCurrentUser();

	/**
	 *  Find and set a base site by baseSiteId
	 * @param baseSiteId
	 */
	void setBaseSite(String baseSiteId);

	/**
	 * Creation a new free trial order
	 * @param freeTrialWsDTO
	 * @return
	 * @throws CommerceCartModificationException
	 * @throws RepeatedFreeTrialOrderException
	 * @throws InvalidCartException
	 * @throws IllegalAccessException
	 */
	FreeTrialResponseWsDTO placeFreeTrialOrder(FreeTrialWsDTO freeTrialWsDTO)
			throws CommerceCartModificationException, RepeatedFreeTrialOrderException, InvalidCartException,
			IllegalAccessException;

	/**
	 * Check if there is a user with provided email
	 * @param email
	 * @return
	 */
	Boolean isUserExisting(String email);
}
