package com.wiley.ws.core.order.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;


/**
 * An order entry - an order line for a single product.
 *
 * Author Herman_Chukhrai (EPAM)
 */
public class OrderUpdateEntryRequestWsDTO
{
	/**
	 * A unique alphanumeric value that identifies the product item across all the systems.
	 */
	private String sapProductCode;
	/**
	 * ISBN-13. See: https://en.wikipedia.org/wiki/International_Standard_Book_Number
	 */
	@NotNull
	private String isbn;
	/**
	 * Quantity of the product in the order entry. Product pieces are considered as default measurement units.
	 */
	@NotNull
	@Min(1)
	private Integer quantity;
	/**
	 * Price of the single product item. The en_US format is used for the value.
	 */
	@NotNull
	private Double basePrice;
	/**
	 * Collection of applied local (entry level) discounts
	 */
	@Valid
	private List<DiscountWsDTO> discounts = new ArrayList<>();
	/**
	 * Price of all the product items in the entity (quantity x basePrice). The en_US format is used for the value.
	 */
	@NotNull
	private Double totalPrice;

	@JsonProperty("sapProductCode")
	public String getSapProductCode()
	{
		return sapProductCode;
	}

	public void setSapProductCode(final String sapProductCode)
	{
		this.sapProductCode = sapProductCode;
	}

	@JsonProperty("isbn")
	public String getIsbn()
	{
		return isbn;
	}

	public void setIsbn(final String isbn)
	{
		this.isbn = isbn;
	}

	@JsonProperty("quantity")
	public Integer getQuantity()
	{
		return quantity;
	}

	public void setQuantity(final Integer quantity)
	{
		this.quantity = quantity;
	}

	@JsonProperty("basePrice")
	public Double getBasePrice()
	{
		return basePrice;
	}

	public void setBasePrice(final Double basePrice)
	{
		this.basePrice = basePrice;
	}

	@JsonProperty("discounts")
	public List<DiscountWsDTO> getDiscounts()
	{
		return discounts;
	}

	public void setDiscounts(final List<DiscountWsDTO> discounts)
	{
		this.discounts = discounts;
	}

	@JsonProperty("totalPrice")
	public Double getTotalPrice()
	{
		return totalPrice;
	}

	public void setTotalPrice(final Double totalPrice)
	{
		this.totalPrice = totalPrice;
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}

		if (o == null || getClass() != o.getClass())
		{
			return false;
		}

		final OrderUpdateEntryRequestWsDTO that = (OrderUpdateEntryRequestWsDTO) o;

		return new EqualsBuilder()
				.append(isbn, that.isbn)
				.append(quantity, that.quantity)
				.isEquals();
	}

	@Override
	public int hashCode()
	{
		return new HashCodeBuilder(17, 37)
				.append(isbn)
				.append(quantity)
				.toHashCode();
	}

	@Override
	public String toString()
	{
		return new ToStringBuilder(this)
				.append("isbn", isbn)
				.append("quantity", quantity)
				.append("totalPrice", totalPrice)
				.toString();
	}
}
