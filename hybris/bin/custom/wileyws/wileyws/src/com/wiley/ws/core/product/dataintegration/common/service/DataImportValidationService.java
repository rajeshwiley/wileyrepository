package com.wiley.ws.core.product.dataintegration.common.service;

public interface DataImportValidationService
{
	/**
	 * Performs model object validation for specified constraintGroupId
	 *
	 * @param model
	 * @param <T>
	 * @thorws WileyProductCrudValidationConstraintsException
	 */
	<T> void validate(T model);

	void validateItemDoesNotExist(String code);

	void validateCodeIsImmutable(String currentCode, String newCode, String type);

}
