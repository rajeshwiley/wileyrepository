package com.wiley.ws.core.common.dto;

import java.util.Objects;

import javax.validation.constraints.Pattern;


/**
 * Wrapper for country string representation.
 * Two-letter ISO 3166-1 alpha-2 code that specifies a country. See https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2 for details.
 */
public class CountrySwgDTO
{
	private String value = null;

	public CountrySwgDTO(final String value)
	{
		this.value = value;
	}

	@Pattern(regexp = "^[A-Z]{2}$", message = "validation.country.invalid")
	public String getValue()
	{
		return value;
	}

	public void setValue(final String value)
	{
		this.value = value;
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		CountrySwgDTO countrySwgDTO = (CountrySwgDTO) o;
		return Objects.equals(value, countrySwgDTO.value);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(value);
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("class CountrySwgDTO {\n");
		sb.append("  value: ").append(value).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
