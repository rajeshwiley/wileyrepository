package com.wiley.ws.core.product.dataintegration.common.service.impl;

import de.hybris.platform.validation.services.ValidationService;

import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.model.WileyPurchaseOptionProductModel;
import com.wiley.core.product.WileyProductService;
import com.wiley.core.validation.service.ConstraintGroupService;
import com.wiley.ws.core.product.crud.facade.WileyProductCrudCatalogVersionProvider;
import com.wiley.ws.core.product.dataintegration.common.error.ErrorListSwgDTO;
import com.wiley.ws.core.product.dataintegration.common.error.ErrorSwgDTO;
import com.wiley.ws.core.product.dataintegration.common.exception.DataImportValidationErrorsException;

import static com.wiley.ws.core.product.dataintegration.common.error.ErrorSwgDTO.ReasonEnum.NOT_UNIQUE_ID;
import static com.wiley.ws.core.product.dataintegration.common.error.ErrorSwgDTO.TypeEnum.VALIDATION;


public class ProductImportValidationServiceImpl extends DataImportValidationServiceImpl
{
	private final WileyProductService productService;
	private final WileyProductCrudCatalogVersionProvider wileyProductCrudCatalogVersionProvider;

	@Autowired
	public ProductImportValidationServiceImpl(
			final ValidationService validationService,
			final ConstraintGroupService constraintGroupService,
			final WileyProductService productService,
			final WileyProductCrudCatalogVersionProvider wileyProductCrudCatalogVersionProvider)
	{
		super(validationService, constraintGroupService);
		this.productService = productService;
		this.wileyProductCrudCatalogVersionProvider = wileyProductCrudCatalogVersionProvider;
	}

	@Override
	public void validateItemDoesNotExist(final String productCode)
	{
		if (productService.isProductForCodeExist(wileyProductCrudCatalogVersionProvider.getCatalogVersion(), productCode))
		{
			final ErrorSwgDTO error = ErrorSwgDTO.of(VALIDATION, NOT_UNIQUE_ID, MESSAGE_ITEM_NOT_UNIQUE,
					WileyPurchaseOptionProductModel._TYPECODE, SUBJECT_ID);
			throw new DataImportValidationErrorsException(ErrorListSwgDTO.of(error));
		}
	}

}
