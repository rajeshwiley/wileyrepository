package com.wiley.ws.core.order.exception;

import de.hybris.platform.servicelayer.exceptions.SystemException;


/**
 * Author Herman_Chukhrai (EPAM)
 */
public class SchoolWsNotFoundException extends SystemException
{
	public SchoolWsNotFoundException(final String message)
	{
		super(message);
	}
}
