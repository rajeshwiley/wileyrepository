package com.wiley.ws.core.cart.impl;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.order.CommerceSaveCartException;
import de.hybris.platform.commerceservices.order.CommerceSaveCartService;
import de.hybris.platform.commerceservices.order.CommerceSaveCartTextGenerationStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.commerceservices.service.data.CommerceSaveCartParameter;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.context.MessageSource;
import org.springframework.transaction.annotation.Transactional;

import com.wiley.core.enums.OrderType;
import com.wiley.core.event.WileyOrderEntryEvent;
import com.wiley.core.wiley.order.WileyCommerceCartService;
import com.wiley.integrations.order.dto.OrderEntryWsDTO;
import com.wiley.ws.core.cart.WileyasWsCartFacade;
import com.wiley.ws.core.cart.dto.CreateCartEntryRequestWsDTO;
import com.wiley.ws.core.cart.dto.CreateCartRequestWsDTO;
import com.wiley.ws.core.cart.dto.WileyCartWsDTO;
import com.wiley.ws.core.exceptions.carts.WileyasSaveCartException;


/**
 * The type Wileyas ws facade.
 */
public class WileyasWsOrderCartFacadeImpl implements WileyasWsCartFacade
{
	private static final String ENFORCE_CART_EXCEPTION_MSG =
			"A cart with the provided type already exists. Its details are provided in the response body.";

	private CommerceSaveCartService commerceSaveCartService;

	private WileyCommerceCartService wileyCommerceCartService;

	private BaseSiteService baseSiteService;

	private UserService userService;

	private CommerceSaveCartTextGenerationStrategy saveCartTextGenerationStrategy;

	private Converter<CreateCartRequestWsDTO, CartModel> cartModelReverseConverter;

	private Converter<CreateCartEntryRequestWsDTO, CartEntryModel> wileyasCartEntryModelReverseConverter;

	private Converter<AbstractOrderEntryModel, OrderEntryWsDTO> wileyOrderEntryWsConverter;

	private Converter<CartModel, WileyCartWsDTO> wileyasCartWsConverter;

	private CommerceCartService commerceCartService;

	private ProductService productService;

	private CommerceCommonI18NService commerceCommonI18NService;

	private MessageSource messageSource;

	private EventService eventService;

	private ModelService modelService;

	@Override
	@Transactional
	public WileyCartWsDTO createCart(final CreateCartRequestWsDTO request)
			throws CommerceSaveCartException
	{
		CartModel cartModel = getCartModelReverseConverter().convert(request);

		final CommerceSaveCartParameter parameter = new CommerceSaveCartParameter();
		parameter.setCart(cartModel);
		parameter.setName(saveCartTextGenerationStrategy.generateSaveCartName(cartModel));
		parameter.setDescription(saveCartTextGenerationStrategy.generateSaveCartDescription(cartModel));
		getCommerceSaveCartService().saveCart(parameter);

		return wileyasCartWsConverter.convert(cartModel);
	}

	@Override
	public void ensureSingleCart(final CreateCartRequestWsDTO request, boolean isNewCartCreated)
			throws WileyasSaveCartException
	{
		OrderType orderType = OrderType.valueOf(request.getType());
		final List<CartModel> existCarts = wileyCommerceCartService.getAllCartsForUserAndType(
				baseSiteService.getCurrentBaseSite(),
				userService.getCurrentUser(),
				orderType);

		if (existCarts.size() == 1 && !isNewCartCreated)
		{
			WileyCartWsDTO existingCartWsDTO = wileyasCartWsConverter.convert(existCarts.get(0));
			throw new WileyasSaveCartException(ENFORCE_CART_EXCEPTION_MSG, existingCartWsDTO);
		}

		if (existCarts.size() > 1)
		{
			CartModel oldestCart = removeNewestCarts(existCarts);
			WileyCartWsDTO existingCartWsDTO = wileyasCartWsConverter.convert(oldestCart);
			throw new WileyasSaveCartException(ENFORCE_CART_EXCEPTION_MSG, existingCartWsDTO);
		}
	}

	private CartModel removeNewestCarts(final List<CartModel> existCarts)
	{
		List<CartModel> cartsForRemoval = new LinkedList<>(existCarts);
		Optional<CartModel> oldestCreatedCart = existCarts.stream().min((cart1, cart2) -> cart1.getPk().compareTo(cart2.getPk()));
		if (oldestCreatedCart.isPresent()) {
			cartsForRemoval.remove(oldestCreatedCart.get());
			modelService.removeAll(cartsForRemoval);
			return oldestCreatedCart.get();
		}
		return null;
	}

	@Override
	public Collection<WileyCartWsDTO> getUserCartsForType(final OrderType type)
	{
		BaseSiteModel baseSite = baseSiteService.getCurrentBaseSite();
		UserModel user = userService.getCurrentUser();

		return Converters.convertAll(wileyCommerceCartService.getAllCartsForUserAndType(baseSite, user, type),
				wileyasCartWsConverter);
	}

	@Override
	public WileyCartWsDTO getUserCart(final String cardId)
	{
		BaseSiteModel baseSite = baseSiteService.getCurrentBaseSite();
		UserModel user = userService.getCurrentUser();

		return wileyasCartWsConverter.convert(wileyCommerceCartService.getCartForGuidAndSiteAndUser(cardId, baseSite, user));
	}

	@Override
	@Transactional
	public OrderEntryWsDTO addEntryToCart(final String cartId, final CreateCartEntryRequestWsDTO cartEntryDTO)
			throws CommerceCartModificationException
	{
		CartModel cartModel = getCommerceCartService().getCartForGuidAndSiteAndUser(cartId,
				getBaseSiteService().getCurrentBaseSite(), getUserService().getCurrentUser());

		CartEntryModel cartEntry = getWileyasCartEntryModelReverseConverter().convert(cartEntryDTO);
		ProductModel productModel = cartEntry.getProduct();

		getCommerceCommonI18NService().setCurrentCurrency(cartModel.getCurrency());

		final CommerceCartParameter parameter = new CommerceCartParameter();
		parameter.setEnableHooks(true);
		parameter.setCart(cartModel);
		parameter.setQuantity(cartEntry.getQuantity());
		parameter.setProduct(productModel);
		parameter.setUnit(productModel.getUnit());
		parameter.setCreateNewEntry(true);

		parameter.setBusinessKey(cartEntry.getBusinessKey());
		parameter.setBusinessItemId(cartEntry.getBusinessItemId());
		parameter.setExternalPriceValues(cartEntry.getExternalPriceValues());
		parameter.setExternalDiscounts(cartEntry.getExternalDiscounts());
		parameter.setNamedDeliveryDate(cartEntry.getNamedDeliveryDate());
		parameter.setAdditionalInfo(cartEntry.getAdditionalInfo());

		CommerceCartModification commerceCartModification = getCommerceCartService().addToCart(parameter);
		OrderEntryWsDTO result = getWileyOrderEntryWsConverter().convert(commerceCartModification.getEntry());
		return result;
	}

	@Override
	@Transactional
	public void deleteUserCart(final String baseSiteId, final String userId, final String cartId)
	{
		BaseSiteModel baseSite = baseSiteService.getBaseSiteForUID(baseSiteId);
		UserModel user = userService.getUserForUID(userId);

		final CartModel cart = wileyCommerceCartService.getCartForGuidAndSiteAndUser(cartId, baseSite, user);

		cart.getEntries().forEach(entry -> {
			WileyOrderEntryEvent wileyOrderEntryEvent = new WileyOrderEntryEvent("DELETED", entry);
			eventService.publishEvent(wileyOrderEntryEvent);
		});

		wileyCommerceCartService.removeCart(cart);
	}

	public CommerceSaveCartService getCommerceSaveCartService()
	{
		return commerceSaveCartService;
	}

	@Required
	public void setCommerceSaveCartService(final CommerceSaveCartService commerceSaveCartService)
	{
		this.commerceSaveCartService = commerceSaveCartService;
	}

	public CommerceSaveCartTextGenerationStrategy getSaveCartTextGenerationStrategy()
	{
		return saveCartTextGenerationStrategy;
	}

	@Required
	public void setSaveCartTextGenerationStrategy(
			final CommerceSaveCartTextGenerationStrategy saveCartTextGenerationStrategy)
	{
		this.saveCartTextGenerationStrategy = saveCartTextGenerationStrategy;
	}

	public Converter<CreateCartRequestWsDTO, CartModel> getCartModelReverseConverter()
	{
		return cartModelReverseConverter;
	}

	@Required
	public void setCartModelReverseConverter(
			final Converter<CreateCartRequestWsDTO, CartModel> cartModelReverseConverter)
	{
		this.cartModelReverseConverter = cartModelReverseConverter;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	public UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setBaseSiteService(final BaseSiteService baseSiteService)
	{
		this.baseSiteService = baseSiteService;
	}

	public CommerceCommonI18NService getCommerceCommonI18NService()
	{
		return commerceCommonI18NService;
	}

	@Required
	public void setWileyasCartWsConverter(final Converter<CartModel, WileyCartWsDTO> wileyasCartWsConverter)
	{
		this.wileyasCartWsConverter = wileyasCartWsConverter;
	}

	@Required
	public void setWileyCommerceCartService(final WileyCommerceCartService wileyCommerceCartService)
	{
		this.wileyCommerceCartService = wileyCommerceCartService;
	}


	public Converter<CreateCartEntryRequestWsDTO, CartEntryModel> getWileyasCartEntryModelReverseConverter()
	{
		return wileyasCartEntryModelReverseConverter;
	}

	@Required
	public void setWileyasCartEntryModelReverseConverter(
			final Converter<CreateCartEntryRequestWsDTO, CartEntryModel> wileyasCartEntryModelReverseConverter)
	{
		this.wileyasCartEntryModelReverseConverter = wileyasCartEntryModelReverseConverter;
	}

	public Converter<AbstractOrderEntryModel, OrderEntryWsDTO> getWileyOrderEntryWsConverter()
	{
		return wileyOrderEntryWsConverter;
	}

	@Required
	public void setWileyOrderEntryWsConverter(
			final Converter<AbstractOrderEntryModel, OrderEntryWsDTO> wileyOrderEntryWsConverter)
	{
		this.wileyOrderEntryWsConverter = wileyOrderEntryWsConverter;
	}

	public ProductService getProductService()
	{
		return productService;
	}

	@Required
	public void setProductService(final ProductService productService)
	{
		this.productService = productService;
	}

	public CommerceCartService getCommerceCartService()
	{
		return commerceCartService;
	}

	@Required
	public void setCommerceCartService(final CommerceCartService commerceCartService)
	{
		this.commerceCartService = commerceCartService;
	}

	protected BaseSiteService getBaseSiteService()
	{
		return baseSiteService;
	}

	@Required
	public void setCommerceCommonI18NService(final CommerceCommonI18NService commerceCommonI18NService)
	{
		this.commerceCommonI18NService = commerceCommonI18NService;
	}

	public MessageSource getMessageSource()
	{
		return messageSource;
	}

	@Required
	public void setMessageSource(final MessageSource messageSource)
	{
		this.messageSource = messageSource;
	}

	public EventService getEventService()
	{
		return eventService;
	}

	public void setEventService(final EventService eventService)
	{
		this.eventService = eventService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}
}
