package com.wiley.ws.core.order.impl;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.search.restriction.SearchRestrictionService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.services.BaseStoreService;

import javax.annotation.Resource;

import com.wiley.core.enums.OrderType;
import com.wiley.facades.order.WileyMagicLinkFacade;
import com.wiley.facades.product.exception.RepeatedFreeTrialOrderException;
import com.wiley.facades.welags.customer.WelAgsCustomerFacade;
import com.wiley.facades.welags.order.WelAgsCheckoutFacade;
import com.wiley.ws.core.order.WelFreeTrialOrderWsFacade;
import com.wiley.ws.core.order.dto.FreeTrialResponseWsDTO;
import com.wiley.ws.core.order.dto.FreeTrialWsDTO;


public class WelFreeTrialOrderWsFacadeImpl implements WelFreeTrialOrderWsFacade
{
	@Resource(name = "baseSiteService")
	private BaseSiteService baseSiteService;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "welCustomerFacade")
	private WelAgsCustomerFacade welAgsCustomerFacade;

	@Resource(name = "welAgsCheckoutFacade")
	private WelAgsCheckoutFacade wileyCheckoutFacade;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "welMagicLinkFacade")
	private WileyMagicLinkFacade magicLinkFacade;

	@Resource(name = "searchRestrictionService")
	private SearchRestrictionService searchRestrictionService;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;

	@Override
	public void registerNewUser(final FreeTrialWsDTO freeTrialWsDTO) throws DuplicateUidException
	{
		commonI18NService.setCurrentCurrency(baseStoreService.getCurrentBaseStore().getDefaultCurrency());
		final RegisterData data = new RegisterData();
		data.setFirstName(freeTrialWsDTO.getFirstName());
		data.setLastName(freeTrialWsDTO.getLastName());
		data.setLogin(freeTrialWsDTO.getEmail());
		data.setPassword(freeTrialWsDTO.getPassword());
		getWelAgsCustomerFacade().register(data);
	}

	@Override
	public void loginCurrentUser()
	{
		getWelAgsCustomerFacade().loginSuccess();
	}

	@Override
	public void setBaseSite(final String baseSiteId)
	{
		BaseSiteModel baseSiteModel = getBaseSiteService().getBaseSiteForUID(baseSiteId);
		if (baseSiteModel == null)
		{
			throw new UnknownIdentifierException(String.format("Could not find base site for uid [%s].", baseSiteId));
		}
		getBaseSiteService().setCurrentBaseSite(baseSiteModel, true);
	}


	@Override
	public FreeTrialResponseWsDTO placeFreeTrialOrder(final FreeTrialWsDTO freeTrialWsDTO)
			throws CommerceCartModificationException, RepeatedFreeTrialOrderException, InvalidCartException,
			IllegalAccessException
	{
		OrderModel orderModel = getWileyCheckoutFacade().placeFreeTrialOrder(freeTrialWsDTO.getProductCode(), false,
				OrderType.FREE_TRIAL_WEB_SERVICE);
		modelService.save(orderModel);

		FreeTrialResponseWsDTO freeTrialResponseWsDTO = new FreeTrialResponseWsDTO();
		freeTrialResponseWsDTO.setOrderId(orderModel.getCode());
		freeTrialResponseWsDTO.setMagicLink(magicLinkFacade.getMagicLink());
		return freeTrialResponseWsDTO;
	}

	@Override
	public void enableSearchRestrictions()
	{
		searchRestrictionService.enableSearchRestrictions();
	}

	@Override
	public Boolean isUserExisting(final String email)
	{
		return  getUserService().isUserExisting(email.toLowerCase());
	}

	public BaseSiteService getBaseSiteService()
	{
		return baseSiteService;
	}

	public UserService getUserService()
	{
		return userService;
	}

	public WelAgsCustomerFacade getWelAgsCustomerFacade()
	{
		return welAgsCustomerFacade;
	}

	public WelAgsCheckoutFacade getWileyCheckoutFacade()
	{
		return wileyCheckoutFacade;
	}


}
