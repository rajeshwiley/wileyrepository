package com.wiley.ws.core.order.dto;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 * Created by Georgii_Gavrysh on 8/10/2016.
 */
public class ShippingEventEntryWsDTO
{
	@NotNull
	@Size(max = 255, message = "The field length can not be greater than 255")
	private String isbn;

	@NotNull
	private String shippedQuantity;

	public String getIsbn()
	{
		return isbn;
	}

	public String getShippedQuantity()
	{
		return shippedQuantity;
	}

	public void setIsbn(final String isbn)
	{
		this.isbn = isbn;
	}

	public void setShippedQuantity(final String shippedQuantity)
	{
		this.shippedQuantity = shippedQuantity;
	}

	@Override
	public String toString()
	{
		return new ToStringBuilder(this)
				.append("isbn", isbn)
				.append("trackingId", shippedQuantity)
				.toString();
	}

	@Override
	public boolean equals(final Object obj)
	{
		if (obj == null)
		{
			return false;
		}
		if (obj == this)
		{
			return true;
		}
		if (obj.getClass() != getClass())
		{
			return false;
		}
		ShippingEventEntryWsDTO other = (ShippingEventEntryWsDTO) obj;
		return new EqualsBuilder()
				.append(this.isbn, other.isbn)
				.append(this.shippedQuantity, other.shippedQuantity)
				.isEquals();
	}

	@Override
	public int hashCode()
	{
		return new HashCodeBuilder()
				.append(isbn)
				.append(shippedQuantity)
				.toHashCode();
	}
}
