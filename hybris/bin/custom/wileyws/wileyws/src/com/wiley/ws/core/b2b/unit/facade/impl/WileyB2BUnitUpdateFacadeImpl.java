package com.wiley.ws.core.b2b.unit.facade.impl;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Objects;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import com.wiley.core.event.B2BUnitActiveUpdatedEvent;
import com.wiley.core.wileyb2b.unit.WileyB2BUnitService;
import com.wiley.ws.core.b2b.unit.dto.B2BUnitSwgDTO;
import com.wiley.ws.core.b2b.unit.exception.B2BUnitUpdateBadRequestException;
import com.wiley.ws.core.b2b.unit.exception.B2BUnitUpdateException;
import com.wiley.ws.core.b2b.unit.exception.B2BUnitUpdateNotFoundException;
import com.wiley.ws.core.b2b.unit.facade.WileyB2BUnitUpdateFacade;


public class WileyB2BUnitUpdateFacadeImpl implements WileyB2BUnitUpdateFacade
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyB2BUnitUpdateFacadeImpl.class);

	@Resource
	private WileyB2BUnitService wileyB2BUnitService;

	@Resource
	private Populator<B2BUnitSwgDTO, B2BUnitModel> wileyB2BUnitReversePopulator;

	@Resource
	private ModelService modelService;

	@Resource
	private EventService eventService;

	@Override
	@Transactional
	public boolean updateB2BUnit(final String sapAccountNumber, final B2BUnitSwgDTO b2bUnitSwgDTO)
	{
		final B2BUnitModel b2bUnitModel = getB2BUnitModelForSapAccountNumber(sapAccountNumber);
		try
		{
			final boolean isB2BUnitActiveUpdated = !Objects.equals(b2bUnitSwgDTO.getActive(), b2bUnitModel.getActive());
			wileyB2BUnitReversePopulator.populate(b2bUnitSwgDTO, b2bUnitModel);
			modelService.save(b2bUnitModel);
			modelService.saveAll(b2bUnitModel.getAddresses());
			if (isB2BUnitActiveUpdated)
			{
				eventService.publishEvent(new B2BUnitActiveUpdatedEvent(sapAccountNumber));
			}
		}
		catch (UnknownIdentifierException | IllegalArgumentException e)
		{
			throw new B2BUnitUpdateBadRequestException(e.getMessage(), e);
		}
		catch (RuntimeException e)
		{
			throw new B2BUnitUpdateException(e.getMessage(), e);
		}

		return true;
	}

	private B2BUnitModel getB2BUnitModelForSapAccountNumber(final String sapAccountNumber)
	{
		try
		{
			return wileyB2BUnitService.getB2BUnitForSapAccountNumber(sapAccountNumber);
		}
		catch (UnknownIdentifierException | AmbiguousIdentifierException e)
		{
			throw new B2BUnitUpdateNotFoundException(e.getMessage(), e);
		}
	}
}
