package com.wiley.ws.core.product.crud.converter;

import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.enums.WileyWebLinkTypeEnum;
import com.wiley.core.model.WileyWebLinkModel;
import com.wiley.ws.core.product.crud.dto.WebLinkDto;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


public class WebLinkConverter extends AbstractCrudDtoConverter<WebLinkDto, WileyWebLinkModel>
{

	private final ModelService modelService;

	@Autowired
	public WebLinkConverter(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	@Override
	public WileyWebLinkModel convert(final WebLinkDto webLinkDto, final WileyWebLinkModel webLinkModel)
			throws ConversionException
	{
		validateParameterNotNull(webLinkDto, "webLink dto can't be null");

		WileyWebLinkModel webLink = webLinkModel;
		if (webLinkModel == null) {
			webLink = modelService.create(WileyWebLinkModel.class);
		}


		webLink.setType(getWebLinkTypeFromDto(webLinkDto));
		webLink.setParameters(getWebLinkUrlFromDto(webLinkDto));
		return webLink;
	}

	private WileyWebLinkTypeEnum getWebLinkTypeFromDto(final WebLinkDto webLinkDto)
	{
		final String webLinkType = webLinkDto.getType();
		if (webLinkType != null)
		{
			return WileyWebLinkTypeEnum.valueOf(webLinkType);
		}
		else
		{
			return null;
		}
	}

	private List<String> getWebLinkUrlFromDto(final WebLinkDto webLinkDto)
	{
		final String webLinkUrl = webLinkDto.getUrl();
		if (webLinkUrl != null)
		{
			return Collections.singletonList(webLinkUrl);
		}
		else
		{
			return null;
		}
	}
}
