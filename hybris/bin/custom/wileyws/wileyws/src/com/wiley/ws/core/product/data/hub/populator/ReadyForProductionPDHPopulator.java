package com.wiley.ws.core.product.data.hub.populator;

import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import com.wiley.ws.core.product.data.hub.dto.ProductSwgDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Populate from {@link ProductSwgDTO#getReadyForPublication()} to {@link ProductModel#setApprovalStatus(ArticleApprovalStatus)}
 */
public class ReadyForProductionPDHPopulator implements Populator<ProductSwgDTO, ProductModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(ReadyForProductionPDHPopulator.class);

	@Override
	public void populate(final ProductSwgDTO product, final ProductModel productModel) throws ConversionException
	{
		if (Boolean.TRUE.equals(product.getReadyForPublication()))
		{
			productModel.setApprovalStatus(ArticleApprovalStatus.APPROVED);
		}
		else
		{
			productModel.setApprovalStatus(ArticleApprovalStatus.CHECK);
		}

		LOG.debug("Product [{}]: approval status was set to [{}], DTO value is [{}]", productModel.getCode(),
				productModel.getApprovalStatus(), product.getReadyForPublication());

	}
}
