package com.wiley.ws.core.order.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PaginationWsDTO
{
    @Min(1)
    private Integer pageSize;

    @Min(0)
    private Integer currentPage;

    @NotNull
    private String sort;

    @Min(0)
    private Integer totalPages;

    @Min(0)
    private Integer totalResults;

    @JsonProperty("pageSize")
    public Integer getPageSize()
    {
        return pageSize;
    }

    public void setPageSize(final Integer pageSize)
    {
        this.pageSize = pageSize;
    }

    @JsonProperty("currentPage")
    public Integer getCurrentPage()
    {
        return currentPage;
    }

    public void setCurrentPage(final Integer currentPage)
    {
        this.currentPage = currentPage;
    }

    @JsonProperty("sort")
    public String getSort()
    {
        return sort;
    }

    public void setSort(final String sort)
    {
        this.sort = sort;
    }

    @JsonProperty("totalPages")
    public Integer getTotalPages()
    {
        return totalPages;
    }

    public void setTotalPages(final Integer totalPages)
    {
        this.totalPages = totalPages;
    }

    @JsonProperty("totalResults")
    public Integer getTotalResults()
    {
        return totalResults;
    }

    public void setTotalResults(final Integer totalResults)
    {
        this.totalResults = totalResults;
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        PaginationWsDTO that = (PaginationWsDTO) o;
        return new EqualsBuilder()
                .append(this.pageSize, that.pageSize)
                .append(this.currentPage, that.currentPage)
                .append(this.sort, that.sort)
                .append(this.totalPages, that.totalPages)
                .append(this.totalResults, that.totalResults)
                .isEquals();
    }

    @Override
    public int hashCode()
    {
        return new HashCodeBuilder()
                .append(pageSize)
                .append(currentPage)
                .append(sort)
                .append(totalPages)
                .append(totalResults)
                .toHashCode();
    }

    @Override
    public String toString()
    {
        return new ToStringBuilder(this)
                .append("pageSize", pageSize)
                .append("currentPage", currentPage)
                .append("sort", sort)
                .append("totalPages", totalPages)
                .append("totalResults", totalResults)
                .toString();
    }
}
