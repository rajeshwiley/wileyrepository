package com.wiley.ws.core.order.service.impl;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.util.DiscountValue;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.Assert;

import com.wiley.ws.core.order.dto.DiscountWsDTO;
import com.wiley.ws.core.order.service.WileycomExternalDiscountApplier;


/**
 * Default implementation of {@link WileycomExternalDiscountApplier}
 */
public class WileycomExternalDiscountApplierImpl implements WileycomExternalDiscountApplier
{
	@Override
	public void applyDiscounts(@Nonnull final AbstractOrderModel orderModel,
			@Nullable final List<DiscountWsDTO> externalDiscounts)
	{
		Assert.notNull(orderModel);

		final CurrencyModel currency = orderModel.getCurrency();
		Assert.notNull(currency);

		if (CollectionUtils.isNotEmpty(externalDiscounts))
		{
			List<DiscountValue> discountValueList = externalDiscounts.stream()
					.map(discount -> createDiscountValue(orderModel, currency, discount))
					.collect(Collectors.toList());

			orderModel.setGlobalDiscountValues(discountValueList);
		}
	}

	@Override
	public void applyDiscounts(@Nonnull final AbstractOrderEntryModel orderEntryModel,
			@Nullable final List<DiscountWsDTO> externalDiscounts)
	{
		Assert.notNull(orderEntryModel);

		final AbstractOrderModel orderModel = orderEntryModel.getOrder();
		Assert.notNull(orderModel);
		final CurrencyModel currency = orderModel.getCurrency();
		Assert.notNull(currency);

		if (CollectionUtils.isNotEmpty(externalDiscounts))
		{
			List<DiscountValue> discountValueList = externalDiscounts.stream()
					.map(discount -> createDiscountValue(orderModel, currency, discount))
					.collect(Collectors.toList());

			orderEntryModel.setDiscountValues(discountValueList);
		}
	}

	private DiscountValue createDiscountValue(final AbstractOrderModel orderModel, final CurrencyModel currency,
			final DiscountWsDTO discount)
	{
		String code = StringUtils.isNotEmpty(discount.getName()) ? discount.getName() : generateCode("DISC", orderModel);
		boolean absolute = discount.getAbsolute() != null ? discount.getAbsolute() : true;
		double value = discount.getValue() != null ? discount.getValue() : 0.0;

		return new DiscountValue(code, value, absolute, currency.getIsocode());
	}

	private String generateCode(final String prefix, final AbstractOrderModel orderModel)
	{
		return prefix + orderModel.getCode();

	}
}
