package com.wiley.ws.core.order.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;


/**
 * Description of an event occurred on the order processing.
 * The isbn field is used to reference the order entry that was modified as a result of the order processing event.
 *
 * Author Herman_Chukhrai (EPAM)
 */
public class UpdateMessageWsDTO
{

	public enum MessageType
	{
		WARNING,
		ERROR,
		INFO;

		@Override
		@JsonValue
		public String toString()
		{
			return new ToStringBuilder(this)
					.toString();
		}
	}

	/**
	 * Code of the order processing event.
	 */
	private String code;
	@Valid
	@NotNull
	private MessageType messageType;
	/**
	 * Displayable description of the order processing event. The message should be localized using the provided in the
	 * request language and country.
	 */
	@NotNull
	private String message;
	/**
	 * A unique alphanumeric value that identifies the product item across all the systems.
	 */
	private String sapProductCode;
	/**
	 * ISBN-13. See: https://en.wikipedia.org/wiki/International_Standard_Book_Number
	 */
	private String isbn;

	@JsonProperty("code")
	public String getCode()
	{
		return code;
	}

	public void setCode(final String code)
	{
		this.code = code;
	}

	@JsonProperty("messageType")
	public MessageType getMessageType()
	{
		return messageType;
	}

	public void setMessageType(final MessageType messageType)
	{
		this.messageType = messageType;
	}

	@JsonProperty("message")
	public String getMessage()
	{
		return message;
	}

	public void setMessage(final String message)
	{
		this.message = message;
	}

	@JsonProperty("sapProductCode")
	public String getSapProductCode()
	{
		return sapProductCode;
	}

	public void setSapProductCode(final String sapProductCode)
	{
		this.sapProductCode = sapProductCode;
	}

	@JsonProperty("isbn")
	public String getIsbn()
	{
		return isbn;
	}

	public void setIsbn(final String isbn)
	{
		this.isbn = isbn;
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}

		if (o == null || getClass() != o.getClass())
		{
			return false;
		}

		final UpdateMessageWsDTO that = (UpdateMessageWsDTO) o;

		return new EqualsBuilder()
				.append(messageType, that.messageType)
				.append(message, that.message)
				.isEquals();
	}

	@Override
	public int hashCode()
	{
		return new HashCodeBuilder(17, 37)
				.append(messageType)
				.append(message)
				.toHashCode();
	}

	@Override
	public String toString()
	{
		return new ToStringBuilder(this)
				.append("messageType", messageType)
				.append("message", message)
				.append("code", code)
				.append("isbn", isbn)
				.toString();
	}
}
