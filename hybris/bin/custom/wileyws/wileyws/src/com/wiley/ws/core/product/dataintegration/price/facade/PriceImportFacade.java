package com.wiley.ws.core.product.dataintegration.price.facade;

import com.wiley.ws.core.product.dataintegration.price.dto.PriceSwgDTO;


public interface PriceImportFacade
{

	/**
	 * Updates price.
	 *
	 * @param priceId
	 * @param priceSwgDTO
	 */
	void updatePrice(String priceId, PriceSwgDTO priceSwgDTO);

	/**
	 * Creates price row
	 *
	 * @param priceSwgDTO
	 */
	void createPrice(String priceId, PriceSwgDTO priceSwgDTO);

	/**
	 * Checking if price row already exist
	 *
	 * @param priceId
	 * @param priceSwgDTO
	 * @return boolean
	 */
	boolean isPriceRowAlreadyExist(String priceId, PriceSwgDTO priceSwgDTO);

}
