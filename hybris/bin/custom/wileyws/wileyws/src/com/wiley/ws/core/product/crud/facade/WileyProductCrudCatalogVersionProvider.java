package com.wiley.ws.core.product.crud.facade;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;


public class WileyProductCrudCatalogVersionProvider
{
	private String productCatalogId;
	private String productCatalogVersion;
	private String classificationCatalogId;
	private String classificationCatalogVersion;

	private final CatalogVersionService catalogVersionService;

	@Autowired
	public WileyProductCrudCatalogVersionProvider(final CatalogVersionService catalogVersionService)
	{
		this.catalogVersionService = catalogVersionService;
	}

	public CatalogVersionModel getCatalogVersion()
	{
		return catalogVersionService.getCatalogVersion(productCatalogId, productCatalogVersion);
	}

	public CatalogVersionModel getClassificationCatalogVersion()
	{
		return catalogVersionService.getCatalogVersion(classificationCatalogId, classificationCatalogVersion);
	}

	@Required
	public void setProductCatalogId(final String productCatalogId)
	{
		this.productCatalogId = productCatalogId;
	}

	@Required
	public void setProductCatalogVersion(final String productCatalogVersion)
	{
		this.productCatalogVersion = productCatalogVersion;
	}

	@Required
	public void setClassificationCatalogId(final String classificationCatalogId)
	{
		this.classificationCatalogId = classificationCatalogId;
	}

	@Required
	public void setClassificationCatalogVersion(final String classificationCatalogVersion)
	{
		this.classificationCatalogVersion = classificationCatalogVersion;
	}
}
