/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package com.wiley.ws.core.queues.util;

import javax.annotation.Nullable;

import org.apache.commons.lang.StringUtils;

import com.google.common.base.Predicate;
import com.wiley.ws.core.queues.data.ProductExpressUpdateElementData;


/**
 * The type Product express update element predicate.
 */
public class ProductExpressUpdateElementPredicate implements Predicate<ProductExpressUpdateElementData>
{
	private ProductExpressUpdateElementData productExpressUpdateElementData;

	/**
	 * Instantiates a new Product express update element predicate.
	 */
	public ProductExpressUpdateElementPredicate()
	{
		super();
	}

	/**
	 * Instantiates a new Product express update element predicate.
	 *
	 * @param productExpressUpdateElementData
	 * 		the product express update element data
	 */
	public ProductExpressUpdateElementPredicate(final ProductExpressUpdateElementData productExpressUpdateElementData)
	{
		super();
		this.productExpressUpdateElementData = productExpressUpdateElementData;
	}

	/**
	 * Apply boolean.
	 *
	 * @param input
	 * 		the input
	 * @return the boolean
	 */
	@Override
	public boolean apply(@Nullable final ProductExpressUpdateElementData input)
	{

		return areElementsEqual(productExpressUpdateElementData, input);
	}

	/**
	 * Are elements equal boolean.
	 *
	 * @param element1
	 * 		the element 1
	 * @param element2
	 * 		the element 2
	 * @return the boolean
	 */
	protected boolean areElementsEqual(final ProductExpressUpdateElementData element1,
			final ProductExpressUpdateElementData element2)
	{
		if (element1 == element2)
		{
			return true;
		}

		if (element1 == null || element2 == null)
		{
			return false;
		}

		if (!StringUtils.equals(element1.getCode(), element2.getCode()))
		{
			return false;
		}

		if (!StringUtils.equals(element1.getCatalogVersion(), element2.getCatalogVersion()))
		{
			return false;
		}

		if (!StringUtils.equals(element1.getCatalogId(), element2.getCatalogId()))
		{
			return false;
		}

		return true;
	}

	/**
	 * Gets product express update element data.
	 *
	 * @return the product express update element data
	 */
	public ProductExpressUpdateElementData getProductExpressUpdateElementData()
	{
		return productExpressUpdateElementData;
	}

	/**
	 * Sets product express update element data.
	 *
	 * @param productExpressUpdateElementData
	 * 		the product express update element data
	 */
	public void setProductExpressUpdateElementData(final ProductExpressUpdateElementData productExpressUpdateElementData)
	{
		this.productExpressUpdateElementData = productExpressUpdateElementData;
	}

}
