package com.wiley.ws.core.product.crud.converter.baseproduct;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.base.Preconditions;
import com.wiley.ws.core.product.crud.dto.ImageContainerWsDto;
import com.wiley.ws.core.product.crud.dto.ImageWsDto;
import com.wiley.ws.core.product.crud.dto.WileyProductWsDto;
import com.wiley.ws.core.product.crud.facade.WileyProductCrudCatalogVersionProvider;


public class ExternalImagePopulator implements
		Populator<WileyProductWsDto, ProductModel>
{
	private final ModelService modelService;
	private final ImageWsDtoToMediaModelPopulator imageWsDtoToMediaModelPopulator;
	private final WileyProductCrudCatalogVersionProvider wileyProductCrudCatalogVersionProvider;

	@Autowired
	public ExternalImagePopulator(
			final ModelService modelService,
			final ImageWsDtoToMediaModelPopulator imageWsDtoToMediaModelPopulator,
			final WileyProductCrudCatalogVersionProvider wileyProductCrudCatalogVersionProvider)
	{
		this.modelService = modelService;
		this.imageWsDtoToMediaModelPopulator = imageWsDtoToMediaModelPopulator;
		this.wileyProductCrudCatalogVersionProvider = wileyProductCrudCatalogVersionProvider;
	}

	@Override
	public void populate(
			@Nonnull final WileyProductWsDto source,
			@Nullable final ProductModel target)
			throws ConversionException
	{
		Preconditions.checkNotNull(source, "SOURCE can't be null");
		Preconditions.checkNotNull(target, "TARGET can't be null");

		final ImageContainerWsDto coverImage = source.getCoverImage();
		if (coverImage == null || CollectionUtils.isEmpty(coverImage.getRenditions()))
		{
			return;
		}
		final MediaContainerModel mediaContainerModel = modelService.create(MediaContainerModel.class);
		mediaContainerModel.setQualifier(UUID.randomUUID().toString());
		mediaContainerModel.setCatalogVersion(wileyProductCrudCatalogVersionProvider.getCatalogVersion());

		populateImages(coverImage, mediaContainerModel);
		target.setExternalImage(mediaContainerModel);
	}

	private void populateImages(final ImageContainerWsDto coverImage, final MediaContainerModel mediaContainerModel)
	{
		final List<ImageWsDto> renditions = coverImage.getRenditions();
		final List<MediaModel> images = new ArrayList<>();
		for (final ImageWsDto imageWsDto : renditions)
		{
			final MediaModel image = modelService.create(MediaModel.class);
			imageWsDtoToMediaModelPopulator.populate(imageWsDto, image);
			images.add(image);
		}
		mediaContainerModel.setMedias(images);
	}
}
