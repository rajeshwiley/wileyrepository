package com.wiley.ws.core.transaction;

import de.hybris.platform.tx.DefaultTransaction;


/**
 * Created by Maksim_Kozich on 12.09.2016.
 */
public class WileyWsTestTransaction extends DefaultTransaction
{

	@Override
	public void activateAsCurrentTransaction()
	{
		this.setAsCurrent();
	}
}
