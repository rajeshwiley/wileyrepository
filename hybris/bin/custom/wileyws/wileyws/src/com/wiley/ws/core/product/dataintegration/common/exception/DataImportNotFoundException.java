package com.wiley.ws.core.product.dataintegration.common.exception;

public class DataImportNotFoundException extends RuntimeException
{
	public DataImportNotFoundException()
	{
		super();
	}

	public DataImportNotFoundException(final String message, final Throwable cause)
	{
		super(message, cause);
	}
}
