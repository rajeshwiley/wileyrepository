package com.wiley.ws.core.product.crud.facade;

import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import com.google.common.base.Preconditions;
import com.wiley.core.model.WileyProductModel;
import com.wiley.core.model.WileyPurchaseOptionProductModel;
import com.wiley.ws.core.product.crud.converter.WileyProductWsDtoConverter;
import com.wiley.ws.core.product.crud.converter.populator.LocalizedValuesNullationService;
import com.wiley.ws.core.product.crud.converter.populator.variantproduct.ClassificationAttributesPopulator;
import com.wiley.ws.core.product.crud.dto.WileyProductWsDto;
import com.wiley.ws.core.product.crud.dto.WileyVariantProductWsDto;
import com.wiley.ws.core.product.dataintegration.common.service.ProductImportService;
import com.wiley.ws.core.product.dataintegration.common.service.impl.ProductImportValidationServiceImpl;


public class WileyProductCrudFacadeImpl implements WileyProductCrudFacade
{

	private static final Logger LOG = LoggerFactory.getLogger(WileyProductCrudFacadeImpl.class);

	private final WileyProductWsDtoConverter wileyProductWsDtoConverter;
	private final Populator<WileyVariantProductWsDto, WileyPurchaseOptionProductModel> wileyVariantProductConverter;
	private final ClassificationAttributesPopulator classificationAttributesPopulator;
	private final ClassificationService classificationService;
	private final ModelService modelService;
	private final WileyProductCrudCatalogVersionProvider wileyProductCrudCatalogVersionProvider;
	private final LocalizedValuesNullationService<ProductModel> localizedValuesNullationService;
	private final ProductImportValidationServiceImpl productImportValidationService;
	private final TransactionTemplate transactionTemplate;
	private final ProductImportService productImportService;

	@Autowired
	public WileyProductCrudFacadeImpl(final WileyProductWsDtoConverter wileyProductWsDtoConverter,
			@Qualifier("wileyVariantProductConverter")
			final Populator<WileyVariantProductWsDto, WileyPurchaseOptionProductModel> wileyVariantProductConverter,
			final ClassificationAttributesPopulator classificationAttributesPopulator,
			final ClassificationService classificationService, final ModelService modelService,
			final WileyProductCrudCatalogVersionProvider wileyProductCrudCatalogVersionProvider,
			final LocalizedValuesNullationService<ProductModel> localizedValuesNullationService,
			final ProductImportValidationServiceImpl productImportValidationService,
			final TransactionTemplate transactionTemplate,
			final ProductImportService productImportService)
	{
		this.wileyProductWsDtoConverter = wileyProductWsDtoConverter;
		this.wileyVariantProductConverter = wileyVariantProductConverter;
		this.classificationAttributesPopulator = classificationAttributesPopulator;
		this.classificationService = classificationService;
		this.modelService = modelService;
		this.wileyProductCrudCatalogVersionProvider = wileyProductCrudCatalogVersionProvider;
		this.localizedValuesNullationService = localizedValuesNullationService;
		this.productImportValidationService = productImportValidationService;
		this.transactionTemplate = transactionTemplate;
		this.productImportService = productImportService;
	}

	@Override
	public String createProduct(final WileyProductWsDto productDto)
	{
		LOG.debug("Creating base product from DTO: {}", productDto);
		final WileyProductModel product = wileyProductWsDtoConverter.convert(productDto);
		modelService.attach(product);

		productImportValidationService.validate(product);
		if (product.getAuthorInfos() != null)
		{
			product.getAuthorInfos().forEach(authorInfo -> productImportValidationService.validate(authorInfo));
		}
		transactionTemplate.execute(new TransactionCallbackWithoutResult()
		{
			@Override
			protected void doInTransactionWithoutResult(final TransactionStatus transactionStatus)
			{
				LOG.debug("Saving base product [{}] to Data Base", product.getCode());
				modelService.save(product);
			}
		});
		LOG.debug("Base product [{}] created Successfully", product.getCode());
		return product.getCode();
	}

	@Override
	public void updateProduct(final String productId, final WileyProductWsDto productDto)
	{
		LOG.debug("Updating base product from DTO: {}", productDto);

		final WileyProductModel product = productImportService.getProductForCode(productId);
		Preconditions.checkNotNull(product);
		final Collection<ItemModel> toDelete = resetBaseProduct(product);
		final WileyProductModel productToReplace = wileyProductWsDtoConverter.convert(productDto, product);
		productImportValidationService.validate(productToReplace);
		transactionTemplate.execute(new TransactionCallbackWithoutResult()
		{
			@Override
			protected void doInTransactionWithoutResult(final TransactionStatus transactionStatus)
			{
				LOG.debug("Remove old product-related data for product [{}] ", product.getCode());
				toDelete.forEach(item -> LOG.debug("Removing item [{}] with PK [{}]",
						item.getClass().getName(), item.getPk()));
				modelService.removeAll(toDelete);
				LOG.debug("Saving base product [{}] to Data Base", product.getCode());
				final Collection<CategoryModel> supercategories = productToReplace.getSupercategories();
				productToReplace.setSupercategories(Collections.emptyList());
				modelService.save(productToReplace);
				productToReplace.setSupercategories(supercategories);
				modelService.save(productToReplace);
			}
		});
	}

	@Override
	public String createVariantProduct(final String productId, final WileyVariantProductWsDto variantProductDto)
	{
		productImportValidationService.validateItemDoesNotExist(variantProductDto.getId());
		final WileyPurchaseOptionProductModel variantProductModel = modelService.create(
				WileyPurchaseOptionProductModel.class);
		updateBaseProductForVariant(variantProductModel, productId);
		updateCatalogVersionForProduct(variantProductModel);
		variantProductModel.setApprovalStatus(ArticleApprovalStatus.APPROVED);
		wileyVariantProductConverter.populate(variantProductDto, variantProductModel);

		productImportValidationService.validate(variantProductModel);
		transactionTemplate.execute(new TransactionCallbackWithoutResult()
		{
			@Override
			protected void doInTransactionWithoutResult(final TransactionStatus transactionStatus)
			{
				modelService.save(variantProductModel);
				updateClassificationAttributes(variantProductDto, variantProductModel);
			}
		});
		LOG.debug("Created variant product with code: [{}]", variantProductModel.getCode());
		return variantProductModel.getCode();
	}

	@Override
	public void updateVariantProduct(final String productId, final String variantId,
			final WileyVariantProductWsDto variantProductDto)
	{
		final WileyPurchaseOptionProductModel variantProductModel = productImportService.getProductForCode(variantId);
		productImportValidationService.validateCodeIsImmutable(variantId, variantProductDto.getId(),
				WileyPurchaseOptionProductModel._TYPECODE);
		Collection<ItemModel> itemsToBeRemoved = resetVariantProduct(variantProductModel);

		updateBaseProductForVariant(variantProductModel, productId);
		wileyVariantProductConverter.populate(variantProductDto, variantProductModel);

		productImportValidationService.validate(variantProductModel);
		transactionTemplate.execute(new TransactionCallbackWithoutResult()
		{
			@Override
			protected void doInTransactionWithoutResult(final TransactionStatus transactionStatus)
			{
				modelService.removeAll(itemsToBeRemoved);
				modelService.save(variantProductModel);
				updateClassificationAttributes(variantProductDto, variantProductModel);
			}
		});
		LOG.debug("Updated variant product with code: [{}]", variantProductModel.getCode());
	}

	private void updateClassificationAttributes(final WileyVariantProductWsDto variantProductDto,
			final WileyPurchaseOptionProductModel variantProductModel)
	{
		final FeatureList featureList = classificationService.getFeatures(variantProductModel);
		classificationAttributesPopulator.populate(variantProductDto, featureList);
		classificationService.replaceFeatures(variantProductModel, featureList);
		productImportValidationService.validate(variantProductModel);
	}

	private Collection<ItemModel> resetBaseProduct(final ProductModel wileyProductModel)
	{
		LOG.debug("Resetting localized product values to all NULLs");
		localizedValuesNullationService.resetValue(wileyProductModel, (baseProduct, locale) -> {
			baseProduct.setName(null, locale);
			baseProduct.setDescription(null, locale);
			baseProduct.setSearchKeywords(null, locale);
			baseProduct.setAboutAuthors(null, locale);
			baseProduct.setPressRelease(null, locale);
			baseProduct.setNewToEdition(null, locale);
			baseProduct.setWhatsNew(null, locale);
			baseProduct.setRelatedWebsites(null, locale);
			baseProduct.setNotes(null, locale);
			baseProduct.setReviews(null, locale);
			baseProduct.setErrata(null, locale);
			baseProduct.setTableOfContents(null, locale);
			baseProduct.setTextbook(null, locale);
			baseProduct.setDownloadsTab(null, locale);
		});
		final Collection<ItemModel> toDelete = new HashSet<>();
		toDelete.addAll(wileyProductModel.getAuthorInfos());
		toDelete.addAll(wileyProductModel.getExcerpts());

		if (wileyProductModel.getExternalImage() != null)
		{
			final String externalImageQualifier = wileyProductModel.getExternalImage().getQualifier();
			LOG.debug("Preparing ExternalImage [{}] to be removed for product [{}]",
					externalImageQualifier, wileyProductModel.getCode());
			toDelete.add(wileyProductModel.getExternalImage());
			if (CollectionUtils.isNotEmpty(wileyProductModel.getExternalImage().getMedias()))
			{
				LOG.debug("Preparing Medias to be removed for ExternalImage [{}]",
						externalImageQualifier);
				toDelete.addAll(wileyProductModel.getExternalImage().getMedias());
			}
		}
		LOG.debug("Setting ExternalImage to NULL");
		wileyProductModel.setExternalImage(null);
		LOG.debug("Setting AuthorInfos to empty list");
		wileyProductModel.setAuthorInfos(Collections.emptyList());
		LOG.debug("Setting Excerpts to empty list");
		wileyProductModel.setExcerpts(Collections.emptyList());
		LOG.debug("Setting superCategories to empty list");
		wileyProductModel.setSupercategories(Collections.emptyList());

		return toDelete;
	}

	private Collection<ItemModel> resetVariantProduct(final WileyPurchaseOptionProductModel variantProductModel)
	{
		final Collection<ItemModel> toDelete = new HashSet<>();
		toDelete.addAll(variantProductModel.getExternalStores());
		localizedValuesNullationService.resetValue(variantProductModel, (variantProduct, locale) -> {
			variantProduct.setLifecycleStatus(null, locale);
			variantProduct.setPrintOnDemand(null, locale);
		});
		return toDelete;
	}

	private void updateBaseProductForVariant(final WileyPurchaseOptionProductModel variantProductModel,
			final String productId)
	{
		variantProductModel.setBaseProduct(productImportService.getProductForCode(productId));
	}

	private void updateCatalogVersionForProduct(final ProductModel productModel)
	{
		productModel.setCatalogVersion(wileyProductCrudCatalogVersionProvider.getCatalogVersion());
	}
}