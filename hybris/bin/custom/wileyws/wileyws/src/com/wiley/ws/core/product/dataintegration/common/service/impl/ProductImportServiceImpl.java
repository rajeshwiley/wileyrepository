package com.wiley.ws.core.product.dataintegration.common.service.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.base.Preconditions;
import com.wiley.core.model.WileyPurchaseOptionProductModel;
import com.wiley.core.product.WileyProductService;
import com.wiley.ws.core.product.crud.facade.WileyProductCrudCatalogVersionProvider;
import com.wiley.ws.core.product.dataintegration.common.exception.DataImportNotFoundException;
import com.wiley.ws.core.product.dataintegration.common.service.ProductImportService;


public class ProductImportServiceImpl implements ProductImportService
{
	private static final Logger LOG = LoggerFactory.getLogger(ProductImportServiceImpl.class);

	private final WileyProductCrudCatalogVersionProvider wileyProductCrudCatalogVersionProvider;
	private final WileyProductService productService;

	@Autowired
	public ProductImportServiceImpl(
			final WileyProductCrudCatalogVersionProvider wileyProductCrudCatalogVersionProvider,
			final WileyProductService productService)
	{
		this.wileyProductCrudCatalogVersionProvider = wileyProductCrudCatalogVersionProvider;
		this.productService = productService;
	}

	@Override
	public <T extends ProductModel> T getProductForCode(final String productCode)
	{
		final CatalogVersionModel catalogVersion = wileyProductCrudCatalogVersionProvider.getCatalogVersion();
		try
		{
			return (T) productService.getProductForCode(catalogVersion, productCode);
		}
		catch (ClassCastException | UnknownIdentifierException e)
		{
			throw new DataImportNotFoundException("Unable to find a product with code " + productCode, e);
		}
	}

	@Override
	public void checkVariantBelongsToBaseProduct(final WileyPurchaseOptionProductModel purchaseOption,
			final ProductModel productModel) {
		Preconditions.checkNotNull(purchaseOption, "Purchase Option cant be null");
		Preconditions.checkNotNull(productModel, "Base Product cant be null");
		LOG.debug("Verify Purchase Option with code [{}] belongs to Base Product with code [{}]",
				purchaseOption.getCode(), productModel.getCode());

		if (CollectionUtils.isNotEmpty(productModel.getVariants())) {
			final boolean contains = productModel.getVariants()
					.stream()
					.anyMatch(variant -> variant.getCode().equals(purchaseOption.getCode()));
			if (!contains) {
				LOG.error("Purchase Option product with code: [{}], does not belong to Base Product with code: [{}]",
						purchaseOption.getCode(), productModel.getCode());
				throw new DataImportNotFoundException();
			}
		}
	}
}
