package com.wiley.ws.core.wileysubscription.dto;

/**
 * The subscription to update or create.
 */
public class SubscriptionUpdateRequestWsDTO extends AbstractSubscriptionRequestWsDTO
{
	// empty
}
