package com.wiley.ws.core.product.dataintegration.productrelation.facade.impl;

import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import com.google.common.base.Preconditions;
import com.wiley.core.model.WileyPurchaseOptionProductModel;
import com.wiley.ws.core.product.dataintegration.common.service.ProductImportService;
import com.wiley.ws.core.product.dataintegration.productrelation.dto.ProductRelationSwgDTO;
import com.wiley.ws.core.product.dataintegration.productrelation.facade.ProductReferenceImportFacade;
import com.wiley.ws.core.product.dataintegration.productrelation.populator.ProductReferenceImportPopulator;


public class ProductReferenceImportFacadeImpl implements ProductReferenceImportFacade
{
	private static final Logger LOG = LoggerFactory.getLogger(ProductReferenceImportFacadeImpl.class);


	private final ModelService modelService;
	private final ProductReferenceImportPopulator productReferenceImportPopulator;
	private final ProductImportService productImportService;
	private final TransactionTemplate transactionTemplate;

	@Autowired
	public ProductReferenceImportFacadeImpl(
			final ModelService modelService,
			final ProductReferenceImportPopulator productReferenceImportPopulator,
			final ProductImportService productImportService,
			final TransactionTemplate transactionTemplate)
	{
		this.modelService = modelService;
		this.productReferenceImportPopulator = productReferenceImportPopulator;
		this.productImportService = productImportService;
		this.transactionTemplate = transactionTemplate;
	}

	@Override
	public void createProductReference(final Collection<ProductRelationSwgDTO> dtos,
			final String productId, final String variantId)
	{
		final ProductModel baseProduct = productImportService.getProductForCode(productId);
		final WileyPurchaseOptionProductModel purchaseOptionProduct = productImportService.getProductForCode(variantId);
		productImportService.checkVariantBelongsToBaseProduct(purchaseOptionProduct, baseProduct);

		final Collection<ProductReferenceModel> oldProductReferences = purchaseOptionProduct.getProductReferences();

		final List<ProductReferenceModel> productReferences =
				dtos
						.stream()
						.map(dto -> convertDtoToProductReferenceModel(dto, purchaseOptionProduct))
						.collect(Collectors.toList());

		transactionTemplate.execute(new TransactionCallbackWithoutResult()
		{
			@Override
			protected void doInTransactionWithoutResult(final TransactionStatus transactionStatus)
			{
				LOG.debug("Store product references in transaction, q-ty: [{}]", productReferences.size());
				modelService.removeAll(oldProductReferences);
				modelService.saveAll(productReferences);
			}
		});
	}

	private ProductReferenceModel convertDtoToProductReferenceModel(final ProductRelationSwgDTO dto,
			final WileyPurchaseOptionProductModel purchaseOption)
	{
		Preconditions.checkNotNull(dto.getTargetProductId(),
				"ProductRelationSwgDTO.targetProductId can't be null");
		Preconditions.checkNotNull(dto.getTargetProductId(),
				"ProductRelationSwgDTO.type can't be null");

		LOG.debug("Converting ProductRelationSwgDTOs to ProductReferenceModel. targetProductId: [{}], type: [{}]",
				dto.getTargetProductId(), dto.getType());
		ProductReferenceModel productReference = modelService.create(ProductReferenceModel.class);
		productReference.setSource(purchaseOption);
		productReferenceImportPopulator.populate(dto, productReference);
		return productReference;
	}

}
