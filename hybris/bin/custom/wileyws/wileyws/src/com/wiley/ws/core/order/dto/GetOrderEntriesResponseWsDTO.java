package com.wiley.ws.core.order.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wiley.integrations.order.dto.OrderEntryWsDTO;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.validation.constraints.NotNull;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetOrderEntriesResponseWsDTO
{
    private List<OrderEntryWsDTO> results;

    @NotNull
    private PaginationWsDTO pagination;

    @JsonProperty("results")
    public List<OrderEntryWsDTO> getResults()
    {
        return results;
    }

    public void setResults(final List<OrderEntryWsDTO> results)
    {
        this.results = results;
    }

    @JsonProperty("pagination")
    public PaginationWsDTO getPagination()
    {
        return pagination;
    }

    public void setPagination(final PaginationWsDTO pagination)
    {
        this.pagination = pagination;
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        GetOrderEntriesResponseWsDTO that = (GetOrderEntriesResponseWsDTO) o;
        return new EqualsBuilder()
            .append(this.results, that.results)
            .append(this.pagination, that.pagination)
            .isEquals();
    }

    @Override
    public int hashCode()
    {
        return new HashCodeBuilder()
                .append(results)
                .append(pagination)
                .toHashCode();
    }

    @Override
    public String toString()
    {
        return new ToStringBuilder(this)
            .append("results", results)
            .append("pagination", pagination)
            .toString();
    }
}
