package com.wiley.ws.core.wileysubscription.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import com.wiley.core.enums.PaymentModeEnum;
import com.wiley.core.model.WileySubscriptionPaymentTransactionModel;
import com.wiley.ws.core.wileysubscription.dto.PaymentTransactionWsDTO;

import static com.wiley.facades.populators.PopulateUtils.populateFieldIfNotNull;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


/**
 * WileySubscriptionPaymentTransaction populator.
 */
public class Wileyb2cWsPaymentTransactionPopulator
		implements Populator<PaymentTransactionWsDTO, WileySubscriptionPaymentTransactionModel>
{
	@Resource
	private CommonI18NService commonI18NService;

	@Override
	public void populate(@Nonnull final PaymentTransactionWsDTO paymentTransactionWsDTO,
			@Nonnull final WileySubscriptionPaymentTransactionModel paymentTransaction) throws ConversionException
	{
		validateParameterNotNullStandardMessage("paymentTransactionWsDTO", paymentTransactionWsDTO);
		validateParameterNotNullStandardMessage("paymentTransaction", paymentTransaction);

		populateFieldIfNotNull(WileySubscriptionPaymentTransactionModel::setAmount, paymentTransaction,
				paymentTransactionWsDTO.getAmount());
		populateFieldIfNotNull(this::populateCurrency, paymentTransaction, paymentTransactionWsDTO.getCurrency());
		populateFieldIfNotNull(WileySubscriptionPaymentTransactionModel::setTime, paymentTransaction,
				paymentTransactionWsDTO.getDate());
		populateFieldIfNotNull(WileySubscriptionPaymentTransactionModel::setType, paymentTransaction,
				paymentTransactionWsDTO.getPaymentStatus() != null ?
						PaymentTransactionType.valueOf(paymentTransactionWsDTO.getPaymentStatus()) :
						null);
		populateFieldIfNotNull(WileySubscriptionPaymentTransactionModel::setPaymentMode, paymentTransaction,
				paymentTransactionWsDTO.getPaymentType() != null ?
						PaymentModeEnum.valueOf(paymentTransactionWsDTO.getPaymentType()) :
						null);
		paymentTransaction.setPaymentToken(paymentTransactionWsDTO.getPaymentToken());
	}

	private void populateCurrency(final WileySubscriptionPaymentTransactionModel paymentTransaction, final String currencyIsocode)
	{
		final CurrencyModel currency = commonI18NService.getCurrency(currencyIsocode);
		paymentTransaction.setCurrency(currency);
	}
}
