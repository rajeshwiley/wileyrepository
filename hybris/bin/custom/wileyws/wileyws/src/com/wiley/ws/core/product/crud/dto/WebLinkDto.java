package com.wiley.ws.core.product.crud.dto;

public class WebLinkDto
{
	private String type;
	private String url;

	private WebLinkDto(final String type, final String url)
	{
		this.type = type;
		this.url = url;
	}

	public WebLinkDto(final WebLinkWsDto webLinkWsDto)
	{
		this(webLinkWsDto.getType().getValue(), webLinkWsDto.getUrl());
	}

	public WebLinkDto(final LinkToExternalStoreWsDto linkToExternalStoreWsDto)
	{
		this(linkToExternalStoreWsDto.getType().getValue(), linkToExternalStoreWsDto.getUrl());
	}

	public String getType()
	{
		return type;
	}

	public String getUrl()
	{
		return url;
	}
}
