package com.wiley.ws.core.product.crud.converter.baseproduct;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaFormatModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.media.MediaService;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.ws.core.product.crud.dto.ImageWsDto;
import com.wiley.ws.core.product.crud.facade.WileyProductCrudCatalogVersionProvider;
import com.wiley.ws.core.product.crud.facade.WileyProductCrudMediaFolderProvider;


public class ImageWsDtoToMediaModelPopulator implements Populator<ImageWsDto, MediaModel>
{
	private final MediaService mediaService;
	private final WileyProductCrudCatalogVersionProvider wileyProductCrudCatalogVersionProvider;
	private final WileyProductCrudMediaFolderProvider wileyProductCrudMediaFolderProvider;

	@Autowired
	public ImageWsDtoToMediaModelPopulator(final MediaService mediaService,
			final WileyProductCrudCatalogVersionProvider wileyProductCrudCatalogVersionProvider,
			final WileyProductCrudMediaFolderProvider wileyProductCrudMediaFolderProvider)
	{
		this.mediaService = mediaService;
		this.wileyProductCrudCatalogVersionProvider = wileyProductCrudCatalogVersionProvider;
		this.wileyProductCrudMediaFolderProvider = wileyProductCrudMediaFolderProvider;
	}

	@Override
	public void populate(final ImageWsDto imageWsDto, final MediaModel mediaModel) throws ConversionException
	{
		mediaModel.setCode(UUID.randomUUID().toString());
		mediaModel.setAltText(imageWsDto.getAltText());
		mediaModel.setURL(imageWsDto.getUrl());
		mediaModel.setMime(imageWsDto.getMimeType().getValue());

		final MediaFormatModel mediaFormat = mediaService.getFormat(imageWsDto.getMediaFormat().getValue());
		mediaModel.setMediaFormat(mediaFormat);

		mediaModel.setCatalogVersion(wileyProductCrudCatalogVersionProvider.getCatalogVersion());
		mediaModel.setFolder(wileyProductCrudMediaFolderProvider.getMediaFolder());
	}
}
