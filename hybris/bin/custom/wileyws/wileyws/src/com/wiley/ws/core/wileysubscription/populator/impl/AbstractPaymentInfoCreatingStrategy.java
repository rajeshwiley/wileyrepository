package com.wiley.ws.core.wileysubscription.populator.impl;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.Resource;

import com.wiley.core.model.WileySubscriptionModel;
import com.wiley.ws.core.wileysubscription.dto.WileySubscriptionAddressWsDTO;
import com.wiley.ws.core.wileysubscription.populator.PaymentInfoCreatingStrategy;


/**
 * Contains common implementatin of PaymentInfoCreatingStrategy.
 */
public abstract class AbstractPaymentInfoCreatingStrategy<T extends PaymentInfoModel>
		implements PaymentInfoCreatingStrategy<T>
{
	@Resource
	private ModelService modelService;

	@Resource
	private Populator<WileySubscriptionAddressWsDTO, AddressModel> wileySubscriptionAddressWsReversePopulator;

	@Override
	public T getOrCreateNewSupportedPaymentInfo(@Nullable final PaymentInfoModel paymentInfo,
			@Nonnull final WileySubscriptionModel wileySubscription)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("wileySubscription", wileySubscription);

		PaymentInfoModel supportedPaymentInfo = paymentInfo;

		if (!isSupported(paymentInfo))
		{
			if (paymentInfo != null)
			{
				modelService.remove(paymentInfo);
			}

			supportedPaymentInfo = createAndInitInstance(wileySubscription);
		}

		return (T) supportedPaymentInfo;
	}

	@Override
	public boolean isSupported(@Nullable final PaymentInfoModel obj)
	{
		return getPaymentInfoClass().isInstance(obj);
	}

	private T createAndInitInstance(final WileySubscriptionModel subscriptionModel)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("subscriptionModel", subscriptionModel);

		final T paymentInfo = modelService.create(getPaymentInfoClass());

		paymentInfo.setOwner(subscriptionModel);
		subscriptionModel.setPaymentInfo(paymentInfo);
		paymentInfo.setUser(subscriptionModel.getCustomer());

		return paymentInfo;
	}

	protected void populateBillingAddress(final PaymentInfoModel paymentInfoModel,
			final WileySubscriptionAddressWsDTO addressWsDTO)
	{
		AddressModel billingAddress = paymentInfoModel.getBillingAddress();
		if (billingAddress == null)
		{
			billingAddress = modelService.create(AddressModel.class);
			paymentInfoModel.setBillingAddress(billingAddress);
			billingAddress.setOwner(paymentInfoModel);
		}
		wileySubscriptionAddressWsReversePopulator.populate(addressWsDTO, billingAddress);
	}

	public abstract Class<T> getPaymentInfoClass();
}
