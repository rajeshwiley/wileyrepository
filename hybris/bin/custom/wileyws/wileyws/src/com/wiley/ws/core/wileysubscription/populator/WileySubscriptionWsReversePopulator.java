package com.wiley.ws.core.wileysubscription.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.subscriptionservices.model.BillingFrequencyModel;

import javax.annotation.Resource;

import com.wiley.core.billingfrequency.WileyBillingFrequencyService;
import com.wiley.core.model.WileySubscriptionModel;
import com.wiley.ws.core.wileysubscription.dto.AbstractSubscriptionRequestWsDTO;
import com.wiley.ws.core.wileysubscription.dto.WileySubscriptionAddressWsDTO;

import static com.wiley.facades.populators.PopulateUtils.populateFieldIfNotNull;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


/**
 * WileySubscription reverse populator. Populates {@link WileySubscriptionModel} from {@link AbstractSubscriptionRequestWsDTO}
 */
public class WileySubscriptionWsReversePopulator implements Populator<AbstractSubscriptionRequestWsDTO, WileySubscriptionModel>
{

	@Resource
	private CommonI18NService commonI18NService;

	@Resource
	private WileyBillingFrequencyService wileyBillingFrequencyService;

	@Resource
	private ModelService modelService;

	@Resource
	private Populator<WileySubscriptionAddressWsDTO, AddressModel> wileySubscriptionAddressWsReversePopulator;

	@Override
	public void populate(final AbstractSubscriptionRequestWsDTO subscriptionDto,
			final WileySubscriptionModel subscriptionModel) throws ConversionException
	{
		validateParameterNotNullStandardMessage("subscriptionDto", subscriptionDto);
		validateParameterNotNullStandardMessage("subscriptionModel", subscriptionModel);

		populateFieldIfNotNull(WileySubscriptionModel::setExternalCode, subscriptionModel, subscriptionDto.getExternalCode());
		populateFieldIfNotNull(this::populateSubscriptionStatus, subscriptionModel, subscriptionDto.getSubscriptionStatus());
		populateFieldIfNotNull(WileySubscriptionModel::setStartDate, subscriptionModel,
				subscriptionDto.getSubscriptionStartDate());
		populateFieldIfNotNull(WileySubscriptionModel::setExpirationDate, subscriptionModel,
				subscriptionDto.getSubscriptionEndDate());
		populateFieldIfNotNull(WileySubscriptionModel::setNextBillingDate, subscriptionModel,
				subscriptionDto.getNextBillingDate());
		populateFieldIfNotNull(WileySubscriptionModel::setBillingPriceValue, subscriptionModel,
				subscriptionDto.getBillingPriceValue());
		populateFieldIfNotNull(this::populateBillingPriceCurrency, subscriptionModel, subscriptionDto.getBillingPriceCurrency());
		populateFieldIfNotNull(this::populateBillingFrequency, subscriptionModel, subscriptionDto.getBillingFrequency());
		populateFieldIfNotNull(WileySubscriptionModel::setContractDurationValue, subscriptionModel,
				subscriptionDto.getContractDurationValue());
		populateFieldIfNotNull(this::populateContractDurationFrequency, subscriptionModel,
				subscriptionDto.getContractDurationUnit());
		populateFieldIfNotNull(this::populateShippingAddress, subscriptionModel, subscriptionDto.getShippingAddress());
	}

	private void populateContractDurationFrequency(final WileySubscriptionModel subscriptionModel,
			final AbstractSubscriptionRequestWsDTO.ContractDurationUnit contractDurationUnit)
	{
		subscriptionModel.setContractDurationFrequency(contractDurationUnit.getValue());
	}

	private void populateBillingFrequency(final WileySubscriptionModel subscriptionModel,
			final AbstractSubscriptionRequestWsDTO.BillingFrequency billingFrequency)
	{
		final BillingFrequencyModel billingFrequencyByCode = getBillingFrequencyByCode(
				billingFrequency.getBillingFrequencyCode());
		subscriptionModel.setBillingFrequency(billingFrequencyByCode);
	}

	private void populateBillingPriceCurrency(final WileySubscriptionModel subscriptionModel, final String currencyIsocode)
	{
		final CurrencyModel currency = commonI18NService.getCurrency(currencyIsocode);
		subscriptionModel.setBillingPriceCurrency(currency);
	}

	private void populateSubscriptionStatus(final WileySubscriptionModel subscriptionModel,
			final AbstractSubscriptionRequestWsDTO.SubscriptionStatus status)
	{
		subscriptionModel.setStatus(status.getValue());
	}

	private void populateShippingAddress(final WileySubscriptionModel subscriptionModel,
			final WileySubscriptionAddressWsDTO addressWsDTO)
	{
		AddressModel deliveryAddress = subscriptionModel.getDeliveryAddress();
		if (deliveryAddress == null)
		{
			deliveryAddress = modelService.create(AddressModel.class);
			subscriptionModel.setDeliveryAddress(deliveryAddress);
			deliveryAddress.setOwner(subscriptionModel);
		}
		wileySubscriptionAddressWsReversePopulator.populate(addressWsDTO, deliveryAddress);
	}

	private BillingFrequencyModel getBillingFrequencyByCode(final String code)
	{
		return wileyBillingFrequencyService.getBillingFrequencyByCode(code);
	}
}
