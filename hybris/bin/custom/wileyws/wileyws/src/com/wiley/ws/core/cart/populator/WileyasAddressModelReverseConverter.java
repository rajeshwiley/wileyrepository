package com.wiley.ws.core.cart.populator;

import de.hybris.platform.converters.impl.AbstractPopulatingConverter;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Resource;

import com.wiley.ws.core.cart.dto.CartAddressWsDTO;


/**
 * Author Maksim_Kozich
 */
public class WileyasAddressModelReverseConverter extends AbstractPopulatingConverter<CartAddressWsDTO, AddressModel>
{
	@Resource
	private ModelService modelService;

	@Override
	public AddressModel convert(final CartAddressWsDTO source) throws ConversionException
	{
		AddressModel result = modelService.create(AddressModel.class);
		superPopulate(source, result);
		return result;
	}

	protected void superPopulate(final CartAddressWsDTO cartAddressWsDTO, final AddressModel addressModel)
	{
		super.populate(cartAddressWsDTO, addressModel);
	}
}
