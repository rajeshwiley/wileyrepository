package com.wiley.ws.core.cart.populator;

import de.hybris.platform.converters.impl.AbstractPopulatingConverter;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Resource;

import com.wiley.ws.core.cart.dto.CreateCartEntryRequestWsDTO;


public class WileyasCartEntryModelReverseConverter extends
		AbstractPopulatingConverter<CreateCartEntryRequestWsDTO, CartEntryModel>
{
	@Resource
	private ModelService modelService;

	@Override
	public CartEntryModel convert(final CreateCartEntryRequestWsDTO createCartEntryRequestWsDTO)
			throws ConversionException
	{
		CartEntryModel cartEntry = modelService.create(CartEntryModel.class);
		superPopulate(createCartEntryRequestWsDTO, cartEntry);
		return cartEntry;
	}

	protected void superPopulate(final CreateCartEntryRequestWsDTO createCartEntryRequestWsDTO,
			final CartEntryModel cartEntry)
	{
		super.populate(createCartEntryRequestWsDTO, cartEntry);
	}
}
