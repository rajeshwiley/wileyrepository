package com.wiley.ws.core.wileysubscription;

import java.util.List;

import javax.annotation.Nonnull;

import com.wiley.facades.wileyws.subscriptions.dto.SubscriptionSearchResult;
import com.wiley.ws.core.wileysubscription.dto.PaymentTransactionWsDTO;
import com.wiley.ws.core.wileysubscription.dto.SubscriptionCreateRequestWsDTO;
import com.wiley.ws.core.wileysubscription.dto.SubscriptionCreateResponseWsDTO;
import com.wiley.ws.core.wileysubscription.dto.SubscriptionUpdateRequestWsDTO;


/**
 * WS Subscription facade
 */
public interface Wileyb2cWsSubscriptionFacade
{

	/**
	 * Checks if there is WileySubscription for provided code in system.
	 *
	 * @param subscriptionCode
	 * @return true if exists else false.
	 */
	boolean doesSubscriptionExist(@Nonnull String subscriptionCode);

	/**
	 * Creates new subscription.
	 *
	 * @param subscriptionDto
	 * @return {@link SubscriptionCreateResponseWsDTO}
	 */
	@Nonnull
	SubscriptionCreateResponseWsDTO createSubscription(@Nonnull SubscriptionCreateRequestWsDTO subscriptionDto);

	/**
	 * Updates existing subscription
	 *
	 * @param subscriptionCode
	 * @param subscriptionDto
	 * @throws de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException
	 * 		if subscription is not found.
	 * @throws de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException
	 * 		if more then one subscription is found for code.
	 */
	void updateSubscription(@Nonnull String subscriptionCode, @Nonnull SubscriptionUpdateRequestWsDTO subscriptionDto);

	/**
	 * Creates Payment transactions for Subscription
	 *
	 * @param id
	 * 		subscription code
	 * @param paymentTransactionWsDTO
	 */
	void createPaymentTransactions(@Nonnull String id, @Nonnull PaymentTransactionWsDTO paymentTransactionWsDTO);

	/**
	 * Search for subscriptions by its external code (identifier).
	 *
	 * @param externalCode
	 * 		The external universally unique identifier of the subscription. Expected to be provided by SAP ERP.
	 * @return internal codes of all found subscriptions.
	 */
	List<SubscriptionSearchResult> getInternalCodesByExternalCode(@Nonnull String externalCode);
}
