package com.wiley.ws.core.constants;

/**
 * The type Wiley ws constants.
 */
@SuppressWarnings("PMD")
public final class WileyWSConstants extends GeneratedWileyWSConstants
{
	/**
	 * The constant EXTENSIONNAME.
	 */
	public static final String EXTENSIONNAME = "wileyws";

	private WileyWSConstants()
	{
		// empty
	}

	/**
	 * The constant MODULE_NAME.
	 */
	public static final String MODULE_NAME = "wileyws";
	/**
	 * The constant MODULE_WEBROOT.
	 */
	public static final String MODULE_WEBROOT = ("y" + "commercewebservices").equals(MODULE_NAME) ? "rest" : MODULE_NAME;
	/**
	 * The constant CONTINUE_URL.
	 */
	public static final String CONTINUE_URL = "session_continue_url";
	/**
	 * The constant CONTINUE_URL_PAGE.
	 */
	public static final String CONTINUE_URL_PAGE = "session_continue_url_page";
	/**
	 * The constant OPTIONS_SEPARATOR.
	 */
	public static final String OPTIONS_SEPARATOR = ",";

	/**
	 * The constant HTTP_REQUEST_PARAM_LANGUAGE.
	 */
	public static final String HTTP_REQUEST_PARAM_LANGUAGE = "lang";
	/**
	 * The constant HTTP_REQUEST_PARAM_CURRENCY.
	 */
	public static final String HTTP_REQUEST_PARAM_CURRENCY = "curr";

	/**
	 * The constant ROOT_CONTEXT_PROPERTY.
	 */
	public static final String ROOT_CONTEXT_PROPERTY = "commercewebservices.rootcontext";
	/**
	 * The constant DEFAULT_ROOT_CONTEXT.
	 */
	public static final String DEFAULT_ROOT_CONTEXT = "/rest/v1/,/rest/v2/";
	/**
	 * The constant URL_SPECIAL_CHARACTERS_PROPERTY.
	 */
	public static final String URL_SPECIAL_CHARACTERS_PROPERTY = "commercewebservices.url.special.characters";
	/**
	 * The constant DEFAULT_URL_SPECIAL_CHARACTERS.
	 */
	public static final String DEFAULT_URL_SPECIAL_CHARACTERS = "?,/";

	public static final String APPLICATION_TYPE_JSON = "application/json";

	public static final String DEFAULT_UNIT_PIECES = "pieces";

	public static final boolean DEFAULT_NET = true;

	public static final String TEXT_TYPE_HTML = "text/html";

	public static final String TRANSACTION_ID_PARAMETER = "transactionId";

	public static final int TRANSACTION_EXPIRATION_TIMEOUT_SECONDS = 10;

	public static final int TRANSACTION_EXPIRATION_CLEANUP_INTERVAL_MILLISECONDS = 10000;

	public static final int TRANSACTION_MAXIMUM_SIZE = 5;
}
