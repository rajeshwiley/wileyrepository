package com.wiley.ws.core.b2b.unit.exception;

public class B2BUnitUpdateException extends RuntimeException
{
	public B2BUnitUpdateException(final String message)
	{
		super(message);
	}

	public B2BUnitUpdateException(final String message, final Throwable cause)
	{
		super(message, cause);
	}
}
