package com.wiley.ws.core.order.service;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;

import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.wiley.ws.core.order.dto.DiscountWsDTO;


/**
 * It is in charge of applying external discounts to order and order entries.
 */
public interface WileycomExternalDiscountApplier
{

	/**
	 * Applies external discount to order. Clean up applied discounts.
	 *
	 * @param orderModel
	 * @param externalDiscounts
	 */
	void applyDiscounts(@Nonnull AbstractOrderModel orderModel, @Nullable List<DiscountWsDTO> externalDiscounts);

	/**
	 * Applies external discount to order entry. Clean up applied discounts.
	 *
	 * @param orderEntryModel
	 * @param externalDiscounts
	 */
	void applyDiscounts(@Nonnull AbstractOrderEntryModel orderEntryModel, @Nullable List<DiscountWsDTO> externalDiscounts);

}
