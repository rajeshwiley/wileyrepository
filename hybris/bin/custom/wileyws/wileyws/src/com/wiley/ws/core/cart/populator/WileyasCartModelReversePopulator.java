package com.wiley.ws.core.cart.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;

import com.wiley.core.i18n.WileyCommonI18NService;
import com.wiley.ws.core.cart.dto.CreateCartRequestWsDTO;
import com.wiley.integrations.exceptions.carts.UnknownCountryException;
import com.wiley.integrations.order.dto.AddressWsDTO;

import static com.wiley.facades.populators.PopulateUtils.populateFieldIfNotNull;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


/**
 * Author Maksim_Kozich
 */
public class WileyasCartModelReversePopulator implements Populator<CreateCartRequestWsDTO, CartModel>
{
	@Resource
	private CommonI18NService commonI18NService;

	@Resource
	private WileyCommonI18NService wileyCommonI18NService;

	@Resource
	private Converter<AddressWsDTO, AddressModel> wileyasAddressReverseWsConverter;

	@Override
	public void populate(@NotNull final CreateCartRequestWsDTO source, @NotNull final CartModel target)
			throws ConversionException
	{
		validateParameterNotNullStandardMessage("source", source);
		validateParameterNotNullStandardMessage("target", target);

		populateCountryAndCurrency(target, source);
		populateFieldIfNotNull(this::populateContactAddress, target, source.getContactAddress());
		populateFieldIfNotNull(this::populatePaymentAddress, target, source.getPaymentAddress());
		populateFieldIfNotNull(CartModel::setExternalText, target, source.getExternalText());
	}


	private void populateCountryAndCurrency(final AbstractOrderModel target,
			final CreateCartRequestWsDTO source)
	{
		final String countryCode = source.getCountry();
		CountryModel country;
		try
		{
			country = getCommonI18NService().getCountry(countryCode);
		}
		catch (UnknownIdentifierException ex)
		{
			throw new UnknownCountryException(ex.getMessage());
		}
		target.setCountry(country);

		final String currencyCode = source.getCurrency();
		if (StringUtils.isBlank(currencyCode))
		{
			target.setCurrency(getWileyCommonI18NService().getDefaultCurrency(country));
		}
		else
		{
			target.setCurrency(getCommonI18NService().getCurrency(currencyCode));
		}
	}

	private void populateContactAddress(final CartModel cartModel, final AddressWsDTO addressDTO)
	{
		AddressModel addressModel = getAddressModelWithOwner(cartModel, addressDTO);
		addressModel.setContactAddress(true);
		cartModel.setContactAddress(addressModel);
	}

	private void populatePaymentAddress(final CartModel cartModel, final AddressWsDTO addressDTO)
	{
		AddressModel addressModel = getAddressModelWithOwner(cartModel, addressDTO);
		addressModel.setBillingAddress(true);
		cartModel.setPaymentAddress(addressModel);
	}

	private AddressModel getAddressModelWithOwner(final CartModel cartModel, final AddressWsDTO addressDTO)
	{
		AddressModel addressModel = getWileyasAddressReverseWsConverter().convert(addressDTO);
		addressModel.setOwner(cartModel);
		return addressModel;
	}

	public CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	public WileyCommonI18NService getWileyCommonI18NService()
	{
		return wileyCommonI18NService;
	}

	public Converter<AddressWsDTO, AddressModel> getWileyasAddressReverseWsConverter()
	{
		return wileyasAddressReverseWsConverter;
	}
}
