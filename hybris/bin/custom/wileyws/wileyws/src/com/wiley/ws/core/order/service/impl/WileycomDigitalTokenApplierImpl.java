package com.wiley.ws.core.order.service.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;

import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.commons.collections.CollectionUtils;

import com.wiley.ws.core.order.dto.DigitalProductAccessTokenWsDTO;
import com.wiley.ws.core.order.exception.OrderEntryWsNotFoundException;
import com.wiley.ws.core.order.service.WileycomDigitalTokenApplier;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


/**
 * Default implementation of {@link WileycomDigitalTokenApplier}
 *
 * Author Herman_Chukhrai (EPAM)
 */
public class WileycomDigitalTokenApplierImpl implements WileycomDigitalTokenApplier
{
	@Override
	public void applyDigitalTokens(@Nonnull final AbstractOrderModel orderModel,
			@Nullable final List<DigitalProductAccessTokenWsDTO> tokens)
	{
		validateParameterNotNullStandardMessage("orderModel", orderModel);
		if (CollectionUtils.isNotEmpty(tokens))
		{
			tokens.stream().forEach(tokenDTO -> {
				final String isbn = tokenDTO.getIsbn();
				final String token = tokenDTO.getToken();
				AbstractOrderEntryModel entryModel = findOrderEntryByIsbn(orderModel, isbn);
				entryModel.setDigitalProductAccessToken(token);
			});
		}
	}

	private static AbstractOrderEntryModel findOrderEntryByIsbn(final AbstractOrderModel orderModel, final String isbn)
	{
		AbstractOrderEntryModel result = null;
		final List<AbstractOrderEntryModel> entries = orderModel.getEntries();
		if (CollectionUtils.isNotEmpty(entries))
		{
			result = orderModel.getEntries().stream()
					.filter(e -> e.getProduct() != null && isbn.equals(e.getProduct().getIsbn()))
					.findAny()
					.orElse(null);
		}
		if (result == null)
		{
			throw new OrderEntryWsNotFoundException(String.format("Can't find order entry by product isbn [%s]", isbn));
		}
		return result;
	}
}
