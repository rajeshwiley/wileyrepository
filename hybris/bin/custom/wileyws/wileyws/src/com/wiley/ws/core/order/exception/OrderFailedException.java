package com.wiley.ws.core.order.exception;

import de.hybris.platform.servicelayer.exceptions.SystemException;


public class OrderFailedException extends SystemException
{
	public OrderFailedException(final String message)
	{
		super(message);
	}
}
