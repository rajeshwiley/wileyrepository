package com.wiley.ws.core.product.crud.converter.populator.variantproduct;

import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.catalog.model.classification.ClassificationSystemVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.classification.ClassificationSystemService;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.model.WileyPurchaseOptionProductModel;
import com.wiley.ws.core.product.crud.dto.WileyVariantProductWsDto;
import com.wiley.ws.core.product.dataintegration.common.exception.DataImportValidationErrorsException;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;
import static org.apache.commons.lang.StringUtils.isNotEmpty;


public class PurchaseOptionsPopulator
		implements Populator<WileyVariantProductWsDto, WileyPurchaseOptionProductModel>
{
	private String systemVersion;
	private String systemId;

	private final ClassificationSystemService classificationSystemService;

	@Autowired
	public PurchaseOptionsPopulator(final ClassificationSystemService classificationSystemService)
	{
		this.classificationSystemService = classificationSystemService;
	}

	@Override
	public void populate(final WileyVariantProductWsDto variantProductWsDto,
			final WileyPurchaseOptionProductModel variantProductModel) throws ConversionException
	{
		validateParameterNotNull(variantProductWsDto, "VariantProductDto can't be null");
		validateParameterNotNull(variantProductModel, "VariantProductModel can't be null");

		final Collection<CategoryModel> superCategories = new ArrayList<>();
		final String purchaseOption = variantProductWsDto.getPurchaseOption();
		if (isNotEmpty(purchaseOption))
		{
			superCategories.add(getClassificationClassModel(purchaseOption));
		}
		variantProductModel.setSupercategories(superCategories);
	}

	private ClassificationClassModel getClassificationClassModel(final String classificationClassCode)
	{
		try
		{
			final ClassificationSystemVersionModel classificationSystemVersion = classificationSystemService.getSystemVersion(
					systemId, systemVersion);
			return classificationSystemService.getClassForCode(classificationSystemVersion, classificationClassCode);
		}
		catch (UnknownIdentifierException e)
		{
			throw DataImportValidationErrorsException.ofNoReferenceException("ProductVariant", "purchaseOption");
		}
	}

	public String getSystemVersion()
	{
		return systemVersion;
	}

	@Required
	public void setSystemVersion(final String systemVersion)
	{
		this.systemVersion = systemVersion;
	}

	public String getSystemId()
	{
		return systemId;
	}

	@Required
	public void setSystemId(final String systemId)
	{
		this.systemId = systemId;
	}
}
