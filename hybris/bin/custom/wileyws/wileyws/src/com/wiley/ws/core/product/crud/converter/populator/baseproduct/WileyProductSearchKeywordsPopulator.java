package com.wiley.ws.core.product.crud.converter.populator.baseproduct;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.LocaleUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.model.WileyProductModel;
import com.wiley.ws.core.product.crud.converter.populator.LocalizedValuesNullationService;
import com.wiley.ws.core.product.dataintegration.common.dto.LocalizedLongStringSwgDTO;
import com.wiley.ws.core.product.crud.dto.WileyProductWsDto;


public class WileyProductSearchKeywordsPopulator implements Populator<WileyProductWsDto, WileyProductModel>
{

	private final LocalizedValuesNullationService<WileyProductModel> localizedValuesNullationService;

	@Autowired
	public WileyProductSearchKeywordsPopulator(
			final LocalizedValuesNullationService<WileyProductModel> localizedValuesNullationService)
	{
		this.localizedValuesNullationService = localizedValuesNullationService;
	}

	@Override
	public void populate(final WileyProductWsDto wileyProductWsDto, final WileyProductModel wileyProductModel)
			throws ConversionException
	{
		final List<LocalizedLongStringSwgDTO> keywordsDtoList = wileyProductWsDto.getKeywords();
		localizedValuesNullationService.resetValue(wileyProductModel,
				(product, locale) -> product.setSearchKeywords(null, locale));
		if (CollectionUtils.isEmpty(keywordsDtoList))
		{
			return;
		}
		keywordsDtoList.forEach(keyword -> populateKeyword(keyword, wileyProductModel));
	}

	private void populateKeyword(final LocalizedLongStringSwgDTO localizedLongValue, final ProductModel productModel)
	{
		productModel.setSearchKeywords(localizedLongValue.getValue(), LocaleUtils.toLocale(localizedLongValue.getLocale()));
	}


}
