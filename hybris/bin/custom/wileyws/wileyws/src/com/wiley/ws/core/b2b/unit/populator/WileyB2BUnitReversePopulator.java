package com.wiley.ws.core.b2b.unit.populator;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wiley.core.address.WileyAddressService;
import com.wiley.ws.core.b2b.unit.dto.AddressSwgDTO;
import com.wiley.ws.core.b2b.unit.dto.B2BUnitSwgDTO;
import com.wiley.ws.core.b2b.unit.dto.LocalizedStringSwgDTO;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import static com.wiley.facades.populators.PopulateUtils.populateFieldIfNotNull;
import static org.apache.commons.collections.CollectionUtils.subtract;
import static org.apache.commons.lang.LocaleUtils.toLocale;


public class WileyB2BUnitReversePopulator implements Populator<B2BUnitSwgDTO, B2BUnitModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyB2BUnitReversePopulator.class);

	@Resource
	private Populator<AddressSwgDTO, AddressModel> wileyB2BUnitAddressReversePopulator;

	@Resource
	private WileyAddressService wileyAddressService;

	@Resource
	private ModelService modelService;

	@Override
	public void populate(@Nonnull final B2BUnitSwgDTO source, @Nonnull final B2BUnitModel target)
			throws ConversionException
	{
		populateFieldIfNotNull((u, val) -> u.setSapAccountNumber(val), target, source.getSapAccountNumber());
		
		populateFieldIfNotNull((u, val) -> populateLocName(u, val), target, source.getName());
		populateFieldIfNotNull((u, val) -> u.setActive(val), target, source.getActive());
		populateFieldIfNotNull((u, val) -> u.setSapSalesRepContactName(val), target, source.getSalesRepContactName());
		populateFieldIfNotNull((u, val) -> u.setSapSalesRepContactEmail(val), target, source.getSalesRepContactEmail());
		populateFieldIfNotNull((u, val) -> u.setSapSalesRepContactPhone(val), target, source.getSalesRepContactPhone());

		final Collection<AddressModel> updatedAddresses = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(source.getShippingAddresses()))
		{
			for (AddressSwgDTO shippingAddress : source.getShippingAddresses())
			{
				final AddressModel shippingAddressModel = transformAddress(shippingAddress, target, true, false);
				updatedAddresses.add(shippingAddressModel);
			}

			if (target.getShippingAddress() == null || !updatedAddresses.contains(target.getShippingAddress()))
			{
				Optional<AddressModel> first = updatedAddresses.stream().findFirst();
				if (first.isPresent()) {
					target.setShippingAddress(first.get());
				}
			}
			modelService.removeAll(subtract(target.getShippingAddresses(), updatedAddresses));
		}

		if (source.getSoldToAddress() != null)
		{
			final AddressModel billingAddressModel = transformAddress(source.getSoldToAddress(), target, false, true);
			updatedAddresses.add(billingAddressModel);
			if (target.getBillingAddress() != null && !Objects.equals(target.getBillingAddress(), billingAddressModel))
			{
				modelService.remove(target.getBillingAddress());
			}
			target.setBillingAddress(billingAddressModel);
		}

		final Set<AddressModel> allAddresses = new HashSet<>(target.getAddresses());
		allAddresses.addAll(updatedAddresses);
		target.setAddresses(allAddresses);
	}

	private AddressModel transformAddress(final AddressSwgDTO address, final ItemModel owner, boolean isShipping,
			boolean isBilling)
	{
		final AddressModel addressModel = wileyAddressService
				.getAddressByExternalIdAndOwner(address.getAddressId(), owner.getPk().toString())
				.orElseGet(() -> createAddressModel(owner));
		wileyB2BUnitAddressReversePopulator.populate(address, addressModel);
		addressModel.setShippingAddress(isShipping);
		addressModel.setBillingAddress(isBilling);
		return addressModel;
	}

	private AddressModel createAddressModel(final ItemModel owner)
	{
		AddressModel addressModel = modelService.create(AddressModel.class);
		addressModel.setOwner(owner);
		return addressModel;
	}

	private void populateLocName(final B2BUnitModel target, final List<LocalizedStringSwgDTO> names)
	{
		for (LocalizedStringSwgDTO name : names)
		{
			target.setLocName(name.getVal(), toLocale(name.getLoc()));
		}
	}
}
