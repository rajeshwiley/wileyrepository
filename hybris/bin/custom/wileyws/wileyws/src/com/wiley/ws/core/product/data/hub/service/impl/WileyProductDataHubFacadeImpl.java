package com.wiley.ws.core.product.data.hub.service.impl;


import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

import com.wiley.core.model.WileyProductModel;
import com.wiley.core.product.WileyProductService;
import com.wiley.facades.product.ProductUpdateResult;
import com.wiley.ws.core.product.data.hub.dto.ProductSwgDTO;
import com.wiley.ws.core.product.data.hub.service.WileyProductDataHubFacade;


public class WileyProductDataHubFacadeImpl implements WileyProductDataHubFacade
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyProductDataHubFacadeImpl.class);

	private String productCatalogId;
	private String productCatalogVersion;

	@Resource
	private WileyProductService productService;

	@Resource
	private CatalogVersionService catalogVersionService;

	@Resource
	private ModelService modelService;

	@Resource
	private Populator<ProductSwgDTO, WileyProductModel> wileyProductPDHPopulatorList;

	/**
	 * {@inheritDoc}
	 *
	 * @throws IllegalArgumentException
	 * 		if exisitng product is not of expected WileyProductModel type (during update operation)
	 */

	@Override
	@Transactional
	public ProductUpdateResult createOrUpdateProductForSite(final String productCode, final ProductSwgDTO externalProduct)
	{
		final CatalogVersionModel catalogVersion = catalogVersionService.getCatalogVersion(productCatalogId,
				productCatalogVersion);

		ProductUpdateResult result = null;
		WileyProductModel product = productService.getWileyProductForCodeIfExists(productCode, catalogVersion);

		if (product != null)
		{
			if (product.getClass() != WileyProductModel.class)
			{
				throw new IllegalArgumentException(
						String.format(
								"Update operation failed because existing product has [%s] type but expected WileyProductModel",
								product.getClass().getSimpleName()));
			}

			result = ProductUpdateResult.UPDATED;
			LOG.debug("Found product for update: pk=[{}], code=[{}], catalog=[{}/{}], class=[{}]", product.getPk(),
					product.getCode(), product.getCatalogVersion().getCatalog().getId(),
					product.getCatalogVersion().getVersion(), WileyProductModel.class.getSimpleName());
		}
		else
		{
			product = modelService.create(WileyProductModel.class);
			product.setCode(productCode);
			product.setCatalogVersion(catalogVersion);

			result = ProductUpdateResult.CREATED;
			LOG.debug("Created product item: code=[{}], catalog=[{}/{}], class=[{}]", productCode, productCatalogId,
					productCatalogVersion, WileyProductModel.class.getSimpleName());

		}

		populateAndSaveProduct(externalProduct, product);
		return result;
	}

	private void populateAndSaveProduct(final ProductSwgDTO externalProduct, final WileyProductModel product)
	{
		wileyProductPDHPopulatorList.populate(externalProduct, product);
		LOG.debug("Product [{}] is populated with wileyProductPDHPopulatorList", product.getCode());

		modelService.saveAll();
		LOG.debug("Product [{}] and all related items were saved", product.getCode());
	}

	@Required
	public void setProductCatalogId(final String productCatalogId)
	{
		this.productCatalogId = productCatalogId;
	}

	@Required
	public void setProductCatalogVersion(final String productCatalogVersion)
	{
		this.productCatalogVersion = productCatalogVersion;
	}
}
