package com.wiley.ws.core.product.crud.dto;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.wiley.ws.core.product.dataintegration.common.dto.LocalizedStringSwgDTO;


/**
 * A downloadable content. It can be PDF file but not restricted.
 */
public class DownloadableContentWsDto
{
	@Valid
	private List<LocalizedStringSwgDTO> title = new ArrayList<>();

	private String url = null;

	private String mimeType = null;

	/**
	 * The title (display name) of the dowloadable content. Max length of the field is 255 characters.
	 *
	 * @return title
	 **/
	public List<LocalizedStringSwgDTO> getTitle()
	{
		return title;
	}

	public void setTitle(final List<LocalizedStringSwgDTO> title)
	{
		this.title = title;
	}

	/**
	 * URL of the media asset.
	 *
	 * @return url
	 **/
	public String getUrl()
	{
		return url;
	}

	public void setUrl(final String url)
	{
		this.url = url;
	}

	/**
	 * Mime type of the downloadable content. It can be but limited: 'application/pdf', 'application/epub+zip'
	 *
	 * @return mimeType
	 **/
	public String getMimeType()
	{
		return mimeType;
	}

	public void setMimeType(final String mimeType)
	{
		this.mimeType = mimeType;
	}

	@Override
	public String toString()
	{
		return new ToStringBuilder(this)
				.append("title", title)
				.append("url", url)
				.append("mimeType", mimeType)
				.toString();
	}
}

