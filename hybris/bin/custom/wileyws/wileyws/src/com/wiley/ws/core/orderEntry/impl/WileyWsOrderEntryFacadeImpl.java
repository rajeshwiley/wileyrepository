package com.wiley.ws.core.orderEntry.impl;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.processengine.enums.ProcessState;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.site.BaseSiteService;

import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.transaction.annotation.Transactional;

import com.wiley.core.model.WileyOrderProcessModel;
import com.wiley.core.order.WileyOrderFulfilmentProcessService;
import com.wiley.core.order.service.WileyOrderEntryService;
import com.wiley.core.order.service.WileyOrderStateSerializationService;
import com.wiley.core.util.savedvalues.ItemModificationHistoryService;
import com.wiley.core.util.savedvalues.ItemModificationInfo;
import com.wiley.integrations.order.dto.OrderEntryWsDTO;
import com.wiley.integrations.order.dto.OrderWsDTO;
import com.wiley.ws.core.order.dto.GetOrderEntriesResponseWsDTO;
import com.wiley.ws.core.order.exception.OrderFailedException;
import com.wiley.ws.core.order.exception.OrderProcessingException;
import com.wiley.ws.core.orderEntry.WileyWsOrderEntryFacade;
import com.wiley.ws.core.orderEntry.data.UpdateOrderEntryInfoRequest;


public class WileyWsOrderEntryFacadeImpl implements WileyWsOrderEntryFacade
{
	@Resource(name = "wileyasOrderEntryService")
	private WileyOrderEntryService wileyOrderEntryService;
	@Resource
	private EnumerationService enumerationService;
	@Resource
	private ModelService modelService;
	@Resource
	private Converter<SearchPageData<AbstractOrderEntryModel>, GetOrderEntriesResponseWsDTO>
			wileyasGetOrderEntriesResponseConverter;
	@Resource
	private Converter<OrderModel, OrderWsDTO> wileyOrderWsConverter;
	@Resource
	private Converter<OrderEntryModel, OrderEntryWsDTO> wileyOrderEntryWsConverter;
	@Resource
	private WileyOrderFulfilmentProcessService wileyOrderFulfilmentProcessService;
	@Resource
	private ItemModificationHistoryService wileyItemModificationHistoryService;
	@Resource
	private WileyOrderStateSerializationService wileyOrderStateSerializationService;
	@Resource(name = "baseSiteService")
	private BaseSiteService baseSiteService;

	@Transactional
	@Override
	public OrderEntryWsDTO checkAndUpdateOrderEntryDetails(final String entryId,
			final UpdateOrderEntryInfoRequest updateOrderEntryInfoRequest)
	{
		final OrderEntryModel orderEntry = wileyOrderEntryService.getOrderEntryForGUID(entryId);
		checkOrderEntryStatus(orderEntry, updateOrderEntryInfoRequest);
		final OrderModel order = orderEntry.getOrder();
		String serializedOrderState = wileyOrderStateSerializationService.serializeToOrderState(order);

		if (StringUtils.isNotBlank(updateOrderEntryInfoRequest.getStatus()))
		{
			OrderStatus updatedOrderStatus = enumerationService.getEnumerationValue(OrderStatus.class,
					updateOrderEntryInfoRequest.getStatus());
			checkSentOrderEntryStatus(updatedOrderStatus);
			orderEntry.setStatus(updatedOrderStatus);
		}
		checkOrderFulfillmentProcess(order);

		if (StringUtils.isNotBlank(updateOrderEntryInfoRequest.getAdditionalInfo()))
		{
			orderEntry.setAdditionalInfo(updateOrderEntryInfoRequest.getAdditionalInfo());
		}

		final ItemModificationInfo modificationInfo = wileyItemModificationHistoryService.createModificationInfo(orderEntry);
		modelService.save(orderEntry);
		wileyItemModificationHistoryService.logModifications(modificationInfo);

		try
		{
			wileyOrderFulfilmentProcessService.startFulfilmentProcessForOrder(order, true, serializedOrderState, false);
		}
		catch (ModelSavingException e)
		{
			throwOrderProcessingException(order, e);
		}

		return wileyOrderEntryWsConverter.convert(orderEntry);
	}

	private void checkSentOrderEntryStatus(final OrderStatus orderStatus)
	{
		if (orderStatus == OrderStatus.CANCELLED)
		{
			throw new IllegalArgumentException("Order status '" + orderStatus + "' sent.");
		}
	}

	private void checkOrderEntryStatus(final OrderEntryModel orderEntryModel,
			final UpdateOrderEntryInfoRequest updateOrderEntryInfoRequest)
	{
		OrderStatus orderEntryStatus = orderEntryModel.getStatus();
		String newOrderEntryStatus = updateOrderEntryInfoRequest.getStatus();
		if (orderEntryStatus == OrderStatus.CANCELLED && newOrderEntryStatus != null && !newOrderEntryStatus.equals(
				orderEntryStatus.getCode()))
		{
			throw new IllegalArgumentException("Status of the '" + OrderStatus.CANCELLED + "' entry can't be changed.");
		}
	}

	private void checkOrderFulfillmentProcess(final OrderModel order)
	{
		Optional<WileyOrderProcessModel> orderProcess = wileyOrderFulfilmentProcessService.getOrderProcessActiveFor(order);
		if (orderProcess.isPresent())
		{
			if (orderProcess.get().getProcessState() == ProcessState.FAILED)
			{
				throw new OrderFailedException(
						"Order entry update rejected. Order fulfillment process failed for [" + order.getGuid() + "]");
			}
			throwOrderProcessingException(order, null);
		}
	}

	private void throwOrderProcessingException(final OrderModel order, final Throwable cause)
	{
		throw new OrderProcessingException(
				"Modification rejected. Active fulfillment process is running for order [" + order.getGuid() + "]", cause);
	}

	@Override
	public GetOrderEntriesResponseWsDTO getOrderEntries(final String userId,
			final String businessItemId, final String businessKey,
			final List<String> statuses, final Integer pageSize,
			final Integer currentPage, final String sort)
	{
		final BaseSiteModel baseSite = baseSiteService.getCurrentBaseSite();
		final PageableData pagination = createPageableData(pageSize, currentPage, sort);

		SearchPageData<AbstractOrderEntryModel> searchResult = wileyOrderEntryService
				.getOrderEntriesForBusinessValues(baseSite, userId, businessItemId, businessKey, statuses, pagination);
		return wileyasGetOrderEntriesResponseConverter.convert(searchResult);
	}

	private PageableData createPageableData(final Integer pageSize, final Integer currentPage, final String sort)
	{
		final PageableData pageableData = new PageableData();

		pageableData.setPageSize(pageSize);
		pageableData.setCurrentPage(currentPage);
		pageableData.setSort(sort);

		return pageableData;
	}
}
