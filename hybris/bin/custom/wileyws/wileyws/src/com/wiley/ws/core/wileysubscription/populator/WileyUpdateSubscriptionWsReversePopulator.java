package com.wiley.ws.core.wileysubscription.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.annotation.Resource;

import com.wiley.core.model.WileySubscriptionModel;
import com.wiley.core.util.Assert;
import com.wiley.ws.core.wileysubscription.dto.AbstractSubscriptionRequestWsDTO;
import com.wiley.ws.core.wileysubscription.dto.SubscriptionCreateRequestWsDTO;
import com.wiley.ws.core.wileysubscription.dto.SubscriptionUpdateRequestWsDTO;

import static com.wiley.facades.populators.PopulateUtils.populateFieldIfNotNull;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


/**
 * WileySubscription reverse populator. Populates {@link WileySubscriptionModel} from {@link SubscriptionCreateRequestWsDTO}
 */
public class WileyUpdateSubscriptionWsReversePopulator
		implements Populator<SubscriptionUpdateRequestWsDTO, WileySubscriptionModel>
{
	@Resource
	private Populator<AbstractSubscriptionRequestWsDTO, WileySubscriptionModel> wileySubscriptionWsReversePopulator;

	@Override
	public void populate(final SubscriptionUpdateRequestWsDTO subscriptionDto,
			final WileySubscriptionModel subscriptionModel) throws ConversionException
	{
		validateParameterNotNullStandardMessage("subscriptionDto", subscriptionDto);
		validateParameterNotNullStandardMessage("subscriptionModel", subscriptionModel);

		Assert.state(Boolean.FALSE.equals(subscriptionModel.getHybrisRenewal()), () -> String
				.format("WileySubscription [%s] should have hybrisRenewal flag set to false.", subscriptionModel.getCode()));

		wileySubscriptionWsReversePopulator.populate(subscriptionDto, subscriptionModel);

		populateFieldIfNotNull(WileySubscriptionModel::setRequireRenewal, subscriptionModel, subscriptionDto.getRenewable());
	}
}
