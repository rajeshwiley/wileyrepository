package com.wiley.ws.core.product.data.hub.populator;

import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.ws.core.product.data.hub.dto.ProductSwgDTO;


/**
 * Populate from {@link ProductSwgDTO#getCategories()} to {@link ProductModel#setSupercategories(Collection)}
 */
public class CategoryPDHPopulator implements Populator<ProductSwgDTO, ProductModel>
{

	private static final Logger LOG = LoggerFactory.getLogger(CategoryPDHPopulator.class);

	@Autowired
	private CategoryService categoryService;

	@Override
	public void populate(final ProductSwgDTO product, final ProductModel productModel) throws ConversionException
	{
		if (product.getCategories() != null)
		{
			List<CategoryModel> categories = product.getCategories().stream()
					.map(category -> categoryService.getCategoryForCode(productModel.getCatalogVersion(), category.getValue()))
					.collect(Collectors.toList());

			productModel.setSupercategories(categories);
		}
		else
		{
			LOG.debug("Skip populating categories due to null");
		}

	}

}
