package com.wiley.ws.core.wileysubscription.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import com.wiley.core.model.WileySubscriptionModel;
import com.wiley.ws.core.wileysubscription.dto.AbstractSubscriptionRequestWsDTO;
import com.wiley.ws.core.wileysubscription.dto.SubscriptionCreateResponseWsDTO;


/**
 * WileySubscription reverse populator. Populates {@link SubscriptionCreateResponseWsDTO} from {@link WileySubscriptionModel}
 */
public class WileySubscriptionCreateResponseWsPopulator
		implements Populator<WileySubscriptionModel, SubscriptionCreateResponseWsDTO>
{
	@Resource
	private Populator<AbstractSubscriptionRequestWsDTO, WileySubscriptionModel> wileySubscriptionWsReversePopulator;


	@Override
	public void populate(@Nonnull final WileySubscriptionModel wileySubscriptionModel,
			@Nonnull final SubscriptionCreateResponseWsDTO subscriptionCreateResponse)
			throws ConversionException
	{
		ServicesUtil.validateParameterNotNullStandardMessage("wileySubscriptionModel", wileySubscriptionModel);
		ServicesUtil.validateParameterNotNullStandardMessage("subscriptionCreateResponse", subscriptionCreateResponse);

		subscriptionCreateResponse.setInternalSubscriptionId(wileySubscriptionModel.getCode());
	}
}
