package com.wiley.ws.core.product.crud.converter.populator;

import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.util.Locale;
import java.util.function.BiConsumer;

import org.apache.commons.lang3.LocaleUtils;
import org.springframework.beans.factory.annotation.Autowired;


public final class LocalizedValuesNullationService<T>
{

	private final CommonI18NService commonI18NService;

	@Autowired
	public LocalizedValuesNullationService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

	public void resetValue(final T item, final BiConsumer<T, Locale> productModelConsumer)
	{
		for (final LanguageModel lang : commonI18NService.getAllLanguages())
		{
			final Locale locale = LocaleUtils.toLocale(lang.getIsocode());
			productModelConsumer.accept(item, locale);
		}
	}
}
