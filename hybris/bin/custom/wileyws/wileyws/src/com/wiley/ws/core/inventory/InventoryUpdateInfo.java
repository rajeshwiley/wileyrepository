package com.wiley.ws.core.inventory;

/**
 * Created by Raman_Hancharou on 4/3/2017.
 */
public enum InventoryUpdateInfo
{
	CREATED,
	UPDATED,
	LOWER_SEQUENCE_ID
}
