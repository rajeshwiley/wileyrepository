package com.wiley.ws.core.transaction.facade.impl;

import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;

import com.wiley.ws.core.transaction.WileyWsTestTransaction;
import com.wiley.ws.core.transaction.dto.WileyWsTestTransactionDTO;
import com.wiley.ws.core.transaction.facade.WileyTransactionFacade;
import com.wiley.ws.core.transaction.service.WileyTransactionService;


/**
 * Created by Maksim_Kozich on 22.09.2016.
 */
public class WileyTransactionFacadeImpl implements WileyTransactionFacade
{
	@Resource
	private WileyTransactionService wileyTransactionService;

	@Resource
	private Converter<WileyWsTestTransaction, WileyWsTestTransactionDTO> wileyWsTestTransactionConverter;

	@Override
	public WileyWsTestTransactionDTO beginNewTransaction()
	{
		WileyWsTestTransaction transaction = wileyTransactionService.beginNewTransaction();

		WileyWsTestTransactionDTO result = wileyWsTestTransactionConverter.convert(transaction);

		return result;
	}

	@Override
	public WileyWsTestTransactionDTO rollbackTransaction(@NotNull final String transactionId)
	{
		final WileyWsTestTransaction transaction = wileyTransactionService.rollbackTransaction(transactionId);

		WileyWsTestTransactionDTO result = wileyWsTestTransactionConverter.convert(transaction);

		return result;
	}
}
