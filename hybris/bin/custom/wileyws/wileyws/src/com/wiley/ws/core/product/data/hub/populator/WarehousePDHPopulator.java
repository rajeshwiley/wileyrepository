package com.wiley.ws.core.product.data.hub.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.WarehouseService;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import com.wiley.ws.core.product.data.hub.dto.ProductSwgDTO;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * Populate from {@link ProductSwgDTO#getDefaultWarehouse()} to {@link ProductModel#setDefaultWarehouse(WarehouseModel)}
 */
public class WarehousePDHPopulator implements Populator<ProductSwgDTO, ProductModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(WarehousePDHPopulator.class);

	@Autowired
	private WarehouseService warehouseService;

	@Override
	public void populate(final ProductSwgDTO product, final ProductModel productModel) throws ConversionException
	{
		if (product.getDefaultWarehouse() != null)
		{
			if (StringUtils.isBlank(product.getDefaultWarehouse()))
			{
				productModel.setDefaultWarehouse(null);
			}
			else
			{
				WarehouseModel warehouseModel = warehouseService.getWarehouseForCode(product.getDefaultWarehouse());
				productModel.setDefaultWarehouse(warehouseModel);
			}

			LOG.debug("Product [{}]: warehouse set to [{}] due to ProductSwgDTO.getDefaultWarehouse() is [{}]",
					productModel.getCode(), productModel.getDefaultWarehouse(), product.getDefaultWarehouse());
		}
		else
		{
			LOG.debug("Skip populating default warehouse due to null");
		}
	}
}
