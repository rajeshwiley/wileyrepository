package com.wiley.ws.core.cart.populator;


import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.user.AddressModel;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.ws.core.cart.dto.CartAddressWsDTO;


/**
 * The type WileyasAddressModelPopulator unit test.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyasAddressModelPopulatorUnitTest
{
	private static final String POSTAL_CODE = "POSTAL_CODE";
	private static final String TOWN = "TOWN";
	private static final String LINE1 = "LINE1";
	private static final String LINE2 = "LINE2";
	public static final String COUNTRY_ISO = "COUNTRY_ISO";
	public static final String REGION_ISO_SHORT = "REGION_ISO_SHORT";
	public static final String FIRST_NAME = "FIRST_NAME";
	public static final String LAST_NAME = "LAST_NAME";
	public static final String PHONE = "PHONE";
	public static final String EMAIL = "EMAIL";
	public static final String COMPANY = "COMPANY";
	public static final String DEPARTMENT = "DEPARTMENT";

	@InjectMocks
	private WileyasAddressModelPopulator testInstance;

	@Mock
	private AddressModel sourceMock;

	@Mock
	private CartAddressWsDTO targetMock;

	@Mock
	private CountryModel countryModelMock;

	@Mock
	private RegionModel regionModelMock;

	@Test
	public void testPopulate()
	{
		// given
		when(sourceMock.getPostalcode()).thenReturn(POSTAL_CODE);

		when(countryModelMock.getIsocode()).thenReturn(COUNTRY_ISO);
		when(sourceMock.getCountry()).thenReturn(countryModelMock);
		when(regionModelMock.getIsocodeShort()).thenReturn(REGION_ISO_SHORT);
		when(sourceMock.getRegion()).thenReturn(regionModelMock);

		when(sourceMock.getTown()).thenReturn(TOWN);
		when(sourceMock.getLine1()).thenReturn(LINE1);
		when(sourceMock.getLine2()).thenReturn(LINE2);
		when(sourceMock.getFirstname()).thenReturn(FIRST_NAME);
		when(sourceMock.getLastname()).thenReturn(LAST_NAME);
		when(sourceMock.getPhone1()).thenReturn(PHONE);
		when(sourceMock.getEmail()).thenReturn(EMAIL);
		when(sourceMock.getCompany()).thenReturn(COMPANY);
		when(sourceMock.getDepartment()).thenReturn(DEPARTMENT);

		// when
		testInstance.populate(sourceMock, targetMock);

		// then
		verify(targetMock).setPostcode(POSTAL_CODE);
		verify(targetMock).setCountry(COUNTRY_ISO);
		verify(targetMock).setState(REGION_ISO_SHORT);
		verify(targetMock).setCity(TOWN);
		verify(targetMock).setLine1(LINE1);
		verify(targetMock).setLine2(LINE2);
		verify(targetMock).setFirstName(FIRST_NAME);
		verify(targetMock).setLastName(LAST_NAME);
		verify(targetMock).setPhoneNumber(PHONE);
		verify(targetMock).setEmail(EMAIL);
		verify(targetMock).setOrganization(COMPANY);
		verify(targetMock).setDepartment(DEPARTMENT);

	}

	@Test(expected = IllegalArgumentException.class)
	public void testPopulateWithNullSource()
	{
		testInstance.populate(null, targetMock);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testPopulateWithNullTarget()
	{
		testInstance.populate(sourceMock, null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testPopulateWithNullSourceAndTarget()
	{
		testInstance.populate(null, null);
	}
}
