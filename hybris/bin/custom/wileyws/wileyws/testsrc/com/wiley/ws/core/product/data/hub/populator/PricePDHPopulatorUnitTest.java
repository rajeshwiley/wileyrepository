package com.wiley.ws.core.product.data.hub.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.ws.core.product.data.hub.converter.ProductPriceSwgDTOPDHConverter;
import com.wiley.ws.core.product.data.hub.dto.ProductPriceSwgDTO;
import com.wiley.ws.core.product.data.hub.dto.ProductSwgDTO;


/**
 * Test for {@link PricePDHPopulator}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)

public class PricePDHPopulatorUnitTest
{
	private static final String TEST_PRODUCT_CODE = "productcode";

	@InjectMocks
	private PricePDHPopulator pricePDHPopulator;

	@Mock
	private ModelService modelServiceMock;

	@Mock
	private CatalogVersionService catalogVersionServiceMock;

	@Mock
	private ProductPriceSwgDTOPDHConverter productPriceSwgDTOPDHConverterMock;

	@Mock
	private ProductSwgDTO productMock;

	@Mock
	private ProductModel productModelMock;

	@Mock
	private CatalogVersionModel stagedCatalogVersionMock;

	@Test
	public void shouldSkipPopulatingDueToDTOPricesIsNull()
	{
		//Given
		when(productMock.getPrices()).thenReturn(null);

		//When
		pricePDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock, never()).setEurope1Prices(any());
		verify(modelServiceMock, never()).remove(any());
	}

	@Test
	public void shouldNotExecuteModelServiceDueToModelPricesIsNull()
	{
		//Given
		when(productMock.getPrices()).thenReturn(Collections.emptyList());
		when(productModelMock.getEurope1Prices()).thenReturn(null);

		//When
		pricePDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(modelServiceMock, never()).remove(any());
	}


	@Test
	public void shouldNotExecuteModelServiceDueToModelPricesIsEmptyList()
	{
		//Given
		when(productMock.getPrices()).thenReturn(Collections.emptyList());
		when(productModelMock.getEurope1Prices()).thenReturn(Collections.emptyList());

		//When
		pricePDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(modelServiceMock, never()).remove(any());
	}


	@Test
	public void shouldRemoveOldPrices()
	{
		//Given
		when(productMock.getPrices()).thenReturn(Collections.emptyList());
		List<PriceRowModel> priceModels = Collections.singletonList(mock(PriceRowModel.class));
		when(productModelMock.getEurope1Prices()).thenReturn(priceModels);

		//When
		pricePDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(modelServiceMock).removeAll(eq(priceModels));
	}

	@Test
	public void shouldPopulateProductModelPrices()
	{
		//Given
		when(productModelMock.getCode()).thenReturn(TEST_PRODUCT_CODE);
		List<ProductPriceSwgDTO> prices = new LinkedList<>();
		prices.add(mock(ProductPriceSwgDTO.class));
		prices.add(mock(ProductPriceSwgDTO.class));
		when(productMock.getPrices()).thenReturn(prices);

		List<PriceRowModel> expectedPriceModels = prepareExpectedPriceRowModels(prices);

		//When
		pricePDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock).setEurope1Prices(eq(expectedPriceModels));
	}

	public List<PriceRowModel> prepareExpectedPriceRowModels(final List<ProductPriceSwgDTO> prices)
	{

		List<PriceRowModel> priceModels = new LinkedList<>();
		for (ProductPriceSwgDTO price : prices)
		{
			PriceRowModel priceRowModel = mock(PriceRowModel.class);
			when(productPriceSwgDTOPDHConverterMock.convertToPriceRowModel(price, TEST_PRODUCT_CODE))
					.thenReturn(priceRowModel);
			priceModels.add(priceRowModel);
		}
		return priceModels;
	}
}
