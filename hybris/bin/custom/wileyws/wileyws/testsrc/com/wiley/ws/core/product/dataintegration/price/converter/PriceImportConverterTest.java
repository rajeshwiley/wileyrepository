package com.wiley.ws.core.product.dataintegration.price.converter;

import com.wiley.ws.core.product.dataintegration.common.exception.DataImportValidationErrorsException;
import com.wiley.ws.core.product.dataintegration.price.dto.PriceSwgDTO;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.product.UnitService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.any;


@RunWith(MockitoJUnitRunner.class)
public class PriceImportConverterTest
{
    private static final String PRICE_SWG_DTO_PRICE_ID = "BOOK1";
    private static final String PRICE_SWG_DTO_PRODUCT_ID = "WCOM_VARIANT_PRODUCT";
    private static final String PRICE_SWG_DTO_US_CURRENCY_ISO_CODE = "USD";
    private static final String PRICE_SWG_DTO_US_COUNTRY_ISO_CODE = "US";
    private static final double PRICE_SWG_DTO_PRICE = 100.0;
    private static final int PRICE_SWG_DTO_MIN_QUANTITY = 20;
    private static final long PRICE_SWG_DTO_SEQUENCE_ID = 999L;

    private static final String DUMMY_PRICE_ROW_PRODUCT_ID = "WCOM_VARIANT_TEST_BOOK";
    private static final String DUMMY_UK_CURRENCY_ISO_CODE = "GBP";
    private static final String DUMMY_UK_COUNTRY_ISO_CODE = "UK";
    private static final double DUMMY_PRICE = 50.0;
    private static final long DUMMY_MIN_QUANTITY = 45L;
    private static final long DUMMY_SEQUENCE_ID = 800L;

    @Mock
    private ModelService modelServiceMock;
    @Mock
    private CommonI18NService commonI18NServiceMock;
    @Mock
    private UnitService unitServiceMock;
    @Mock
    private UnitModel unitModelMock;
    @Mock
    private CurrencyModel currencyModelUSMock;
    @Mock
    private CountryModel countryModelUSMock;
    @Mock
    private CurrencyModel oldCurrencyModelUKMock;
    @Mock
    private CountryModel oldCountryModelUKMock;

    private PriceSwgDTO priceSwgDTO;
    private PriceRowModel oldPriceRowModel;
    private PriceImportConverter priceImportConverter;

    @Before
    public void setUp()
    {
        prepareCommonMocks();
        priceImportConverter = new PriceImportConverter(modelServiceMock, commonI18NServiceMock, unitServiceMock);
    }

    private void prepareCommonMocks()
    {
        given(commonI18NServiceMock.getCurrency(anyString())).willReturn(currencyModelUSMock);
        given(currencyModelUSMock.getIsocode()).willReturn(PRICE_SWG_DTO_US_CURRENCY_ISO_CODE);
        given(commonI18NServiceMock.getCountry(anyString())).willReturn(countryModelUSMock);
        given(countryModelUSMock.getIsocode()).willReturn(PRICE_SWG_DTO_US_COUNTRY_ISO_CODE);
        given(unitServiceMock.getUnitForCode(anyString())).willReturn(unitModelMock);
    }

    @Test
    public void shouldCreatePriceWithSameProductId()
    {
        //given
        preparePriceSwgDTO(PRICE_SWG_DTO_PRODUCT_ID);
        given(modelServiceMock.create(any(Class.class))).willReturn(new PriceRowModel());

        //when
        PriceRowModel convertedPriceRowModel = priceImportConverter.convert(priceSwgDTO, null);

        //then
        assertThat(convertedPriceRowModel).isNotNull();
        assertThat(convertedPriceRowModel.getProductId()).isEqualTo(PRICE_SWG_DTO_PRODUCT_ID);
        assertThat(convertedPriceRowModel.getPrice()).isEqualTo(PRICE_SWG_DTO_PRICE);
        assertThat(convertedPriceRowModel.getCurrency().getIsocode()).isEqualTo(PRICE_SWG_DTO_US_CURRENCY_ISO_CODE);
        assertThat(convertedPriceRowModel.getCountry().getIsocode()).isEqualTo(PRICE_SWG_DTO_US_COUNTRY_ISO_CODE);
        assertThat(convertedPriceRowModel.getMinqtd()).isEqualTo(PRICE_SWG_DTO_MIN_QUANTITY);
        assertThat(convertedPriceRowModel.getSequenceId()).isEqualTo(PRICE_SWG_DTO_SEQUENCE_ID);
    }

    @Test
    public void shouldUpdatePriceWithSameProductId()
    {
        //given
        prepareOldPriceRowModel(PRICE_SWG_DTO_PRODUCT_ID);
        preparePriceSwgDTO(PRICE_SWG_DTO_PRODUCT_ID);

        //when
        PriceRowModel convertedPriceRowModel = priceImportConverter.convert(priceSwgDTO, oldPriceRowModel);

        //then
        assertThat(convertedPriceRowModel).isNotNull();
        assertThat(convertedPriceRowModel.getProductId()).isEqualTo(PRICE_SWG_DTO_PRODUCT_ID);
        assertThat(convertedPriceRowModel.getPrice()).isEqualTo(PRICE_SWG_DTO_PRICE);
        assertThat(convertedPriceRowModel.getCurrency().getIsocode()).isEqualTo(PRICE_SWG_DTO_US_CURRENCY_ISO_CODE);
        assertThat(convertedPriceRowModel.getCountry().getIsocode()).isEqualTo(PRICE_SWG_DTO_US_COUNTRY_ISO_CODE);
        assertThat(convertedPriceRowModel.getMinqtd()).isEqualTo(PRICE_SWG_DTO_MIN_QUANTITY);
        assertThat(convertedPriceRowModel.getSequenceId()).isEqualTo(PRICE_SWG_DTO_SEQUENCE_ID);
    }

    @Test(expected = DataImportValidationErrorsException.class)
    public void shouldThrowExceptionForPriceWithDifferentProductId()
    {
        //given
        prepareOldPriceRowModel(DUMMY_PRICE_ROW_PRODUCT_ID);
        preparePriceSwgDTO(PRICE_SWG_DTO_PRODUCT_ID);

        //when
        priceImportConverter.convert(priceSwgDTO, oldPriceRowModel);
    }

    @Test(expected = DataImportValidationErrorsException.class)
    public void shouldThrowExceptionWhenRetrievingCurrency()
    {
        //given
        prepareOldPriceRowModel(PRICE_SWG_DTO_PRODUCT_ID);
        preparePriceSwgDTO(PRICE_SWG_DTO_PRODUCT_ID);
        given(commonI18NServiceMock.getCurrency(anyString())).willThrow(UnknownIdentifierException.class);

        //when
        priceImportConverter.convert(priceSwgDTO, oldPriceRowModel);
    }

    @Test(expected = DataImportValidationErrorsException.class)
    public void shouldThrowExceptionWhenRetrievingCountry()
    {
        //given
        prepareOldPriceRowModel(PRICE_SWG_DTO_PRODUCT_ID);
        preparePriceSwgDTO(PRICE_SWG_DTO_PRODUCT_ID);
        given(commonI18NServiceMock.getCountry(anyString())).willThrow(UnknownIdentifierException.class);

        //when
        priceImportConverter.convert(priceSwgDTO, oldPriceRowModel);
    }

    private void preparePriceSwgDTO(final String dummyProductId)
    {
        priceSwgDTO = new PriceSwgDTO();
        priceSwgDTO.setProductId(dummyProductId);
        priceSwgDTO.setId(PRICE_SWG_DTO_PRICE_ID);
        priceSwgDTO.setPrice(PRICE_SWG_DTO_PRICE);
        priceSwgDTO.setCurrency(PRICE_SWG_DTO_US_CURRENCY_ISO_CODE);
        priceSwgDTO.setCountry(PRICE_SWG_DTO_US_COUNTRY_ISO_CODE);
        priceSwgDTO.setMinQuantity(PRICE_SWG_DTO_MIN_QUANTITY);
        priceSwgDTO.setSequenceId(PRICE_SWG_DTO_SEQUENCE_ID);
    }

    private void prepareOldPriceRowModel(final String mockProductId)
    {
        prepareOldCurrencyModelMock();
        prepareOldCountryModelMock();

        oldPriceRowModel = new PriceRowModel();
        oldPriceRowModel.setProductId(mockProductId);
        oldPriceRowModel.setPrice(DUMMY_PRICE);
        oldPriceRowModel.setCurrency(oldCurrencyModelUKMock);
        oldPriceRowModel.setCountry(oldCountryModelUKMock);
        oldPriceRowModel.setMinqtd(DUMMY_MIN_QUANTITY);
        oldPriceRowModel.setSequenceId(DUMMY_SEQUENCE_ID);
    }

    private void prepareOldCurrencyModelMock()
    {
        given(oldCurrencyModelUKMock.getIsocode()).willReturn(DUMMY_UK_CURRENCY_ISO_CODE);
    }

    private void prepareOldCountryModelMock()
    {
        given(oldCountryModelUKMock.getIsocode()).willReturn(DUMMY_UK_COUNTRY_ISO_CODE);
    }
}