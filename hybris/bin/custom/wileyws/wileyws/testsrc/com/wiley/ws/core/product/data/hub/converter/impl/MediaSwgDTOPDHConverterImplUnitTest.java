package com.wiley.ws.core.product.data.hub.converter.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.wiley.ws.core.product.data.hub.dto.MediaSwgDTO;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Test for {@link MediaSwgDTOPDHConverterImpl}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class MediaSwgDTOPDHConverterImplUnitTest
{

	private static final String TEST_MEDIA_CODE = "media_code";
	private static final String TEST_MEDIA_URL = "http://testurl/";
	private static final String TEST_MEDIA_MIME = "testtextmime";
	private static final String TEST_MEDIA_ALTTEXT = "testalttext";

	@InjectMocks
	private MediaSwgDTOPDHConverterImpl mediaSwgDTOConverter;

	@Mock
	private ModelService modelServiceMock;

	@Mock
	private MediaSwgDTO mediaMock;

	@Mock
	private CatalogVersionModel catalogVersionMock;

	@Mock
	private MediaModel mediaModelMock;

	@Test
	public void shouldConvertToMedia()
	{
		//Given
		when(modelServiceMock.create(MediaModel.class)).thenReturn(mediaModelMock);
		when(mediaMock.getUrl()).thenReturn(TEST_MEDIA_URL);
		when(mediaMock.getMime()).thenReturn(TEST_MEDIA_MIME);
		when(mediaMock.getAltText()).thenReturn(TEST_MEDIA_ALTTEXT);

		//When
		MediaModel resultMediaModel = mediaSwgDTOConverter.convertToMediaModel(mediaMock, TEST_MEDIA_CODE, catalogVersionMock);

		//Then
		verify(resultMediaModel).setCode(TEST_MEDIA_CODE);
		verify(resultMediaModel).setCatalogVersion(catalogVersionMock);
		verify(resultMediaModel).setInternalURL(TEST_MEDIA_URL);
		verify(resultMediaModel).setMime(TEST_MEDIA_MIME);
		verify(resultMediaModel).setAltText(TEST_MEDIA_ALTTEXT);
	}

}
