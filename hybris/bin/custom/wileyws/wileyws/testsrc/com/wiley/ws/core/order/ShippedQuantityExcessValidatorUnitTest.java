package com.wiley.ws.core.order;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.HashSet;

import com.wiley.ws.core.order.exception.WileycomWsOrderInvalidShippedQuantiytException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Created by Georgii_Gavrysh on 8/11/2016.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ShippedQuantityExcessValidatorUnitTest
{
	@InjectMocks
	private ShippedQuantityExcessValidator testedValidator;

	@Mock
	private OrderModel orderMock;

	@Mock
	private OrderEntryModel orderEntryFirstMock;

	@Mock
	private OrderEntryModel orderEntrySecondMock;

	@Mock
	private ConsignmentModel consignmentFirstMock;

	@Mock
	private ConsignmentModel consignmentSecondMock;

	@Mock
	private ConsignmentEntryModel consignmentFirstEntryForFirstOrderEntryMock;

	@Mock
	private ConsignmentEntryModel consignmentFirstEntryForSecondOrderEntryMock;

	@Mock
	private ConsignmentEntryModel consignmentSecondEntryForFirstOrderEntryMock;

	@Mock
	private ConsignmentEntryModel consignmentSecondEntryForSecondOrderEntryMock;

	@Mock
	private ProductModel productModelFirstMock;

	@Mock
	private ProductModel productModelSecondMock;

	@Before
	public void setup()
	{
		when(orderMock.getCode()).thenReturn("TEST_ORDER");

		when(orderEntryFirstMock.getProduct()).thenReturn(productModelFirstMock);
		when(orderEntrySecondMock.getProduct()).thenReturn(productModelSecondMock);

		when(productModelFirstMock.getCode()).thenReturn("PRODUCT_CODE_1");
		when(productModelSecondMock.getCode()).thenReturn("PRODUCT_CODE_2");

		when(consignmentFirstEntryForFirstOrderEntryMock.getOrderEntry()).thenReturn(orderEntryFirstMock);
		when(consignmentFirstEntryForSecondOrderEntryMock.getOrderEntry()).thenReturn(orderEntrySecondMock);
		when(consignmentSecondEntryForFirstOrderEntryMock.getOrderEntry()).thenReturn(orderEntryFirstMock);
		when(consignmentSecondEntryForSecondOrderEntryMock.getOrderEntry()).thenReturn(orderEntrySecondMock);
	}

	private void setupOneOrderEntryOneConsignment()
	{
		when(orderMock.getEntries()).thenReturn(Arrays.asList(orderEntryFirstMock));

		when(orderMock.getConsignments()).
				thenReturn(new HashSet<>(Arrays.asList(consignmentFirstMock)));

		when(consignmentFirstMock.getConsignmentEntries()).
				thenReturn(new HashSet<>(Arrays.asList(consignmentFirstEntryForFirstOrderEntryMock)));
	}


	private void setupTwoOrderEntriesTwoConsignments()
	{
		when(orderMock.getEntries()).thenReturn(Arrays.asList(orderEntryFirstMock, orderEntrySecondMock));

		when(orderMock.getConsignments()).
				thenReturn(new HashSet<>(Arrays.asList(consignmentFirstMock, consignmentSecondMock)));

		when(consignmentFirstMock.getConsignmentEntries()).
				thenReturn(new HashSet<>(Arrays.asList(consignmentFirstEntryForFirstOrderEntryMock,
						consignmentFirstEntryForSecondOrderEntryMock)));
		when(consignmentSecondMock.getConsignmentEntries()).
				thenReturn(new HashSet<>(Arrays.asList(consignmentSecondEntryForFirstOrderEntryMock,
						consignmentSecondEntryForSecondOrderEntryMock)));
	}

	@Test
	public void testOneOrderEntryOneConsignmentValidShippedEqualToOrdered()
	{
		setupOneOrderEntryOneConsignment();

		when(orderEntryFirstMock.getQuantity()).thenReturn(2L);

		when(consignmentFirstEntryForFirstOrderEntryMock.getShippedQuantity()).thenReturn(2L);

		testedValidator.validate(orderMock);
	}

	@Test
	public void testOneOrderEntryOneConsignmentValidShippedLessOrdered()
	{
		setupOneOrderEntryOneConsignment();

		when(orderEntryFirstMock.getQuantity()).thenReturn(2L);

		when(consignmentFirstEntryForFirstOrderEntryMock.getShippedQuantity()).thenReturn(1L);

		testedValidator.validate(orderMock);
	}

	@Test(expected = WileycomWsOrderInvalidShippedQuantiytException.class)
	public void testOneOrderEntryOneConsignmentInvalid()
	{
		setupOneOrderEntryOneConsignment();

		when(orderEntryFirstMock.getQuantity()).thenReturn(2L);

		when(consignmentFirstEntryForFirstOrderEntryMock.getShippedQuantity()).thenReturn(3L);

		testedValidator.validate(orderMock);
	}

	@Test
	public void testTwoOrderEntriesTwoConsignmentsValidShippedEqualToOrdered()
	{
		setupTwoOrderEntriesTwoConsignments();

		when(orderEntryFirstMock.getQuantity()).thenReturn(3L);

		when(orderEntrySecondMock.getQuantity()).thenReturn(4L);

		when(consignmentFirstEntryForFirstOrderEntryMock.getShippedQuantity()).thenReturn(1L);
		when(consignmentSecondEntryForFirstOrderEntryMock.getShippedQuantity()).thenReturn(2L);

		when(consignmentFirstEntryForSecondOrderEntryMock.getShippedQuantity()).thenReturn(1L);
		when(consignmentSecondEntryForSecondOrderEntryMock.getShippedQuantity()).thenReturn(3L);

		testedValidator.validate(orderMock);
	}

	@Test
	public void testTwoOrderEntriesTwoConsignmentsValidShippedLessOrdered()
	{
		setupTwoOrderEntriesTwoConsignments();

		when(orderEntryFirstMock.getQuantity()).thenReturn(3L);

		when(orderEntrySecondMock.getQuantity()).thenReturn(4L);

		when(consignmentFirstEntryForFirstOrderEntryMock.getShippedQuantity()).thenReturn(1L);
		when(consignmentSecondEntryForFirstOrderEntryMock.getShippedQuantity()).thenReturn(1L);

		when(consignmentFirstEntryForSecondOrderEntryMock.getShippedQuantity()).thenReturn(1L);
		when(consignmentSecondEntryForSecondOrderEntryMock.getShippedQuantity()).thenReturn(2L);

		testedValidator.validate(orderMock);
	}

	@Test (expected = WileycomWsOrderInvalidShippedQuantiytException.class)
	public void testTwoOrderEntriesTwoConsignmentsInvalid()
	{
		setupTwoOrderEntriesTwoConsignments();

		when(orderEntryFirstMock.getQuantity()).thenReturn(3L);

		when(orderEntrySecondMock.getQuantity()).thenReturn(4L);

		when(consignmentFirstEntryForFirstOrderEntryMock.getShippedQuantity()).thenReturn(1L);
		when(consignmentSecondEntryForFirstOrderEntryMock.getShippedQuantity()).thenReturn(3L);

		when(consignmentFirstEntryForSecondOrderEntryMock.getShippedQuantity()).thenReturn(1L);
		when(consignmentSecondEntryForSecondOrderEntryMock.getShippedQuantity()).thenReturn(2L);

		testedValidator.validate(orderMock);
	}
}
