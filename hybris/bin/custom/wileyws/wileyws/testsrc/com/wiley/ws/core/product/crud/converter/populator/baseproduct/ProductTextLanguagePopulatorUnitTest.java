package com.wiley.ws.core.product.crud.converter.populator.baseproduct;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyProductModel;
import com.wiley.ws.core.product.crud.dto.WileyProductWsDto;
import com.wiley.ws.core.product.dataintegration.common.error.ErrorSwgDTO;
import com.wiley.ws.core.product.dataintegration.common.exception.DataImportValidationErrorsException;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ProductTextLanguagePopulatorUnitTest
{

	private static final String ERROR_FIELD_TEXT_LANGUAGE = "textLanguage";


	private static final String VALID_LANGUAGE_CODE = "en";
	private static final String INVALID_LANGUAGE_CODE = "qwerty";

	@Rule
	public ExpectedException exception = ExpectedException.none();

	@Mock
	private CommonI18NService commonI18NService;

	private ProductTextLanguagePopulator testInstance;

	private WileyProductModel wileyProductModel;

	private LanguageModel validLanguage;
	private LanguageModel invalidLanguage;

	@Mock
	private ProductModel targetModel;

	@Before
	public void setUp() throws Exception
	{
		testInstance = new ProductTextLanguagePopulator(commonI18NService);
		validLanguage = getValidLanguage(VALID_LANGUAGE_CODE);
		setupInvalidLanguageSystemResponse(INVALID_LANGUAGE_CODE);
	}

	@Test
	public void shouldPopulateLanguageIfSuchLanguageExists()
	{
		//Given
		WileyProductWsDto dto = givenDto(VALID_LANGUAGE_CODE);
		//When
		testInstance.populate(dto, targetModel);
		//Then
		verify(targetModel).setTextLanguage(validLanguage);
	}

	@Test
	public void shouldThrowAnExceptionIfNoSuchLanguage()
	{
		exception.expect(notValidLanguageExceptionMatcher);
		//When
		testInstance.populate(givenDto(INVALID_LANGUAGE_CODE), targetModel);
	}

	@Test
	public void shouldNotPopulateAnythingButWorkFineIfLanguageIsBlank()
	{
		//Given
		WileyProductWsDto dto = givenDto(null);
		//When
		testInstance.populate(dto, targetModel);
		//Then
		verify(targetModel, never()).setTextLanguage(any(LanguageModel.class));
	}

	private WileyProductWsDto givenDto(final String textLanguage)
	{
		WileyProductWsDto dto = new WileyProductWsDto();
		dto.setTextLanguage(textLanguage);
		return dto;
	}

	private LanguageModel getValidLanguage(final String languageCode)
	{
		LanguageModel language = mock(LanguageModel.class);
		when(language.getIsocode()).thenReturn(languageCode);

		when(commonI18NService.getLanguage(languageCode)).thenReturn(language);
		return language;
	}

	private void setupInvalidLanguageSystemResponse(final String languageCode)
	{
		when(commonI18NService.getLanguage(languageCode)).thenThrow(UnknownIdentifierException.class);
	}

	private BaseMatcher<DataImportValidationErrorsException> notValidLanguageExceptionMatcher =
			new BaseMatcher<DataImportValidationErrorsException>()
			{
				@Override
				public void describeTo(final Description description)
				{
				}

				@Override
				public boolean matches(final Object o)
				{
					if (o instanceof DataImportValidationErrorsException)
					{
						final ErrorSwgDTO error =
								((DataImportValidationErrorsException) o).getErrors().getErrors().get(0);
						assertEquals(WileyProductModel._TYPECODE, error.getSubjectType());
						assertEquals(ERROR_FIELD_TEXT_LANGUAGE, error.getSubject());
						return true;
					}
					return false;
				}
			};

}