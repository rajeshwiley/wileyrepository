package com.wiley.ws.core.wileysubscription.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.subscriptionservices.enums.SubscriptionStatus;
import de.hybris.platform.subscriptionservices.enums.TermOfServiceFrequency;
import de.hybris.platform.subscriptionservices.model.BillingFrequencyModel;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.billingfrequency.WileyBillingFrequencyService;
import com.wiley.core.model.WileySubscriptionModel;
import com.wiley.ws.core.wileysubscription.dto.AbstractSubscriptionRequestWsDTO;
import com.wiley.ws.core.wileysubscription.dto.WileySubscriptionAddressWsDTO;


/**
 * Default unit test for {@link WileySubscriptionWsReversePopulator}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileySubscriptionWsReversePopulatorUnitTest
{

	@Mock
	private CommonI18NService commonI18NServiceMock;

	@Mock
	private WileyBillingFrequencyService wileyBillingFrequencyServiceMock;

	@Mock
	private ModelService modelServiceMock;

	@Mock
	private Populator<WileySubscriptionAddressWsDTO, AddressModel> wileySubscriptionAddressWsReversePopulatorMock;

	@InjectMocks
	private WileySubscriptionWsReversePopulator wileySubscriptionWsReversePopulator;

	// Test data

	@Mock
	private AbstractSubscriptionRequestWsDTO abstractSubscriptionRequestWsDTOMock;

	@Mock
	private WileySubscriptionModel wileySubscriptionModelMock;

	@Mock
	private CurrencyModel currencyModelMock;
	private static final String TEST_CURRENCY_ISOCODE = "USD";

	@Mock
	private BillingFrequencyModel billingFrequencyModelMock;
	private static final String TEST_BILLING_FREQUENCY_CODE = "DAILY";

	@Mock
	private AddressModel addressModelMock;

	@Mock
	private WileySubscriptionAddressWsDTO shippingAddressWsDTOMock;

	@Before
	public void setUp() throws Exception
	{
		when(commonI18NServiceMock.getCurrency(eq(TEST_CURRENCY_ISOCODE))).thenReturn(currencyModelMock);
		when(currencyModelMock.getIsocode()).thenReturn(TEST_CURRENCY_ISOCODE);

		when(wileyBillingFrequencyServiceMock.getBillingFrequencyByCode(eq(TEST_BILLING_FREQUENCY_CODE))).thenReturn(
				billingFrequencyModelMock);
		when(billingFrequencyModelMock.getCode()).thenReturn(TEST_BILLING_FREQUENCY_CODE);

		when(modelServiceMock.create(eq(AddressModel.class))).thenReturn(addressModelMock);
	}

	@Test
	public void testPopulateWhenPropertiesAreNotNull()
	{
		// Given
		final String testExternalCode = "some_external_code";
		final AbstractSubscriptionRequestWsDTO.SubscriptionStatus subscriptionStatus =
				AbstractSubscriptionRequestWsDTO.SubscriptionStatus.ACTIVE;
		final Date subscriptionStartDateMock = mock(Date.class);
		final Date subscriptionEndDateMock = mock(Date.class);
		final Date subscriptionNextBillingDateMock = mock(Date.class);
		final double subscriptionBillingPriceValue = 10.5;
		final int contractDurationValue = 5;
		final boolean subscriptionRenewable = true;
		final AbstractSubscriptionRequestWsDTO.BillingFrequency billingFrequency =
				AbstractSubscriptionRequestWsDTO.BillingFrequency.DAILY;
		final AbstractSubscriptionRequestWsDTO.ContractDurationUnit contractDurationUnit =
				AbstractSubscriptionRequestWsDTO.ContractDurationUnit.MONTH;

		when(abstractSubscriptionRequestWsDTOMock.getExternalCode()).thenReturn(testExternalCode);
		when(abstractSubscriptionRequestWsDTOMock.getSubscriptionStatus()).thenReturn(subscriptionStatus);
		when(abstractSubscriptionRequestWsDTOMock.getSubscriptionStartDate()).thenReturn(subscriptionStartDateMock);
		when(abstractSubscriptionRequestWsDTOMock.getSubscriptionEndDate()).thenReturn(subscriptionEndDateMock);
		when(abstractSubscriptionRequestWsDTOMock.getNextBillingDate()).thenReturn(subscriptionNextBillingDateMock);
		when(abstractSubscriptionRequestWsDTOMock.getBillingPriceValue()).thenReturn(subscriptionBillingPriceValue);
		when(abstractSubscriptionRequestWsDTOMock.getBillingPriceCurrency()).thenReturn(TEST_CURRENCY_ISOCODE);
		when(abstractSubscriptionRequestWsDTOMock.getBillingFrequency()).thenReturn(billingFrequency);
		when(abstractSubscriptionRequestWsDTOMock.getContractDurationValue()).thenReturn(contractDurationValue);
		when(abstractSubscriptionRequestWsDTOMock.getContractDurationUnit()).thenReturn(contractDurationUnit);

		WileySubscriptionAddressWsDTO shippingAddressMock = mock(WileySubscriptionAddressWsDTO.class);
		when(abstractSubscriptionRequestWsDTOMock.getShippingAddress()).thenReturn(shippingAddressMock);

		// When
		wileySubscriptionWsReversePopulator.populate(abstractSubscriptionRequestWsDTOMock, wileySubscriptionModelMock);

		// Then
		verify(wileySubscriptionModelMock).setExternalCode(eq(testExternalCode));
		verify(wileySubscriptionModelMock).setStatus(eq(SubscriptionStatus.ACTIVE));
		verify(wileySubscriptionModelMock).setStartDate(same(subscriptionStartDateMock));
		verify(wileySubscriptionModelMock).setExpirationDate(same(subscriptionEndDateMock));
		verify(wileySubscriptionModelMock).setNextBillingDate(same(subscriptionNextBillingDateMock));
		verify(wileySubscriptionModelMock).setBillingPriceValue(eq(subscriptionBillingPriceValue));
		verify(wileySubscriptionModelMock).setBillingPriceCurrency(same(currencyModelMock));
		verify(wileySubscriptionModelMock).setBillingFrequency(same(billingFrequencyModelMock));
		verify(wileySubscriptionModelMock).setContractDurationValue(eq(contractDurationValue));
		verify(wileySubscriptionModelMock).setContractDurationFrequency(eq(TermOfServiceFrequency.MONTHLY));
		verify(wileySubscriptionModelMock).setDeliveryAddress(same(addressModelMock));
		verify(wileySubscriptionAddressWsReversePopulatorMock).populate(same(shippingAddressMock), same(addressModelMock));
	}

	@Test
	public void testPopulateWhenPropertiesAreNull()
	{
		// Given
		when(abstractSubscriptionRequestWsDTOMock.getExternalCode()).thenAnswer(invocation -> null);
		when(abstractSubscriptionRequestWsDTOMock.getSubscriptionStatus()).thenAnswer(invocation -> null);
		when(abstractSubscriptionRequestWsDTOMock.getSubscriptionStartDate()).thenAnswer(invocation -> null);
		when(abstractSubscriptionRequestWsDTOMock.getSubscriptionEndDate()).thenAnswer(invocation -> null);
		when(abstractSubscriptionRequestWsDTOMock.getNextBillingDate()).thenAnswer(invocation -> null);
		when(abstractSubscriptionRequestWsDTOMock.getBillingPriceValue()).thenAnswer(invocation -> null);
		when(abstractSubscriptionRequestWsDTOMock.getBillingPriceCurrency()).thenAnswer(invocation -> null);
		when(abstractSubscriptionRequestWsDTOMock.getBillingFrequency()).thenAnswer(invocation -> null);
		when(abstractSubscriptionRequestWsDTOMock.getContractDurationValue()).thenAnswer(invocation -> null);
		when(abstractSubscriptionRequestWsDTOMock.getContractDurationUnit()).thenAnswer(invocation -> null);

		// When
		wileySubscriptionWsReversePopulator.populate(abstractSubscriptionRequestWsDTOMock, wileySubscriptionModelMock);

		// Then
		verify(wileySubscriptionModelMock, never()).setHybrisRenewal(any());
		verifyNoMoreInteractions(wileySubscriptionModelMock);
		verifyZeroInteractions(wileySubscriptionAddressWsReversePopulatorMock);
	}

	@Test
	public void testPopulateHybrisRenewalIsTrue()
	{
		// Given
		when(wileySubscriptionModelMock.getHybrisRenewal()).thenReturn(true);

		// When
		wileySubscriptionWsReversePopulator.populate(abstractSubscriptionRequestWsDTOMock, wileySubscriptionModelMock);

		// Then
		verify(wileySubscriptionModelMock, never()).setHybrisRenewal(any());
	}

	@Test
	public void testPopulateHybrisRenewalIsFalse()
	{
		// Given
		when(wileySubscriptionModelMock.getHybrisRenewal()).thenReturn(false);

		// When
		wileySubscriptionWsReversePopulator.populate(abstractSubscriptionRequestWsDTOMock, wileySubscriptionModelMock);

		// Then
		verify(wileySubscriptionModelMock, never()).setHybrisRenewal(anyBoolean());
	}

	@Test
	public void testPopulateWhenShippingAddressAlreadyExists()
	{
		// Given
		when(wileySubscriptionModelMock.getDeliveryAddress()).thenReturn(addressModelMock);

		when(abstractSubscriptionRequestWsDTOMock.getShippingAddress()).thenReturn(shippingAddressWsDTOMock);

		// When
		wileySubscriptionWsReversePopulator.populate(abstractSubscriptionRequestWsDTOMock, wileySubscriptionModelMock);

		// Then
		verify(wileySubscriptionModelMock, never()).setDeliveryAddress(any());
		verify(wileySubscriptionAddressWsReversePopulatorMock).populate(same(shippingAddressWsDTOMock), same(addressModelMock));
	}

	@Test
	public void testPopulateWhenShippingAddressNotExists()
	{
		// Given
		when(wileySubscriptionModelMock.getDeliveryAddress()).thenReturn(null);

		when(abstractSubscriptionRequestWsDTOMock.getShippingAddress()).thenReturn(shippingAddressWsDTOMock);

		// When
		wileySubscriptionWsReversePopulator.populate(abstractSubscriptionRequestWsDTOMock, wileySubscriptionModelMock);

		// Then
		verify(wileySubscriptionModelMock).setDeliveryAddress(same(addressModelMock));
		verify(wileySubscriptionAddressWsReversePopulatorMock).populate(same(shippingAddressWsDTOMock), same(addressModelMock));
	}
}