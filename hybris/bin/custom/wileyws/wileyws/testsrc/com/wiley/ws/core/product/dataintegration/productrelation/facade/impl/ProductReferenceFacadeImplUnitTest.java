package com.wiley.ws.core.product.dataintegration.productrelation.facade.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.transaction.support.TransactionTemplate;

import com.wiley.core.model.WileyProductModel;
import com.wiley.core.model.WileyPurchaseOptionProductModel;
import com.wiley.ws.core.product.dataintegration.common.service.ProductImportService;
import com.wiley.ws.core.product.dataintegration.productrelation.populator.ProductReferenceImportPopulator;

import static java.util.Collections.emptyList;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ProductReferenceFacadeImplUnitTest
{
	private static final String PURCHASE_OPTION_PRODUCT_CODE = "PURCHASE_OPTION_PRODUCT_CODE";
	private static final String ANOTHER_PURCHASE_OPTION_PRODUCT_CODE = "ANOTHER_PURCHASE_OPTION_PRODUCT_CODE";
	private static final String BASE_PRODUCT_CODE = "BASE_PRODUCT_CODE";
	@Mock
	private ModelService modelService;
	@Mock
	private ProductReferenceImportPopulator importProductReferencePopulator;
	@Mock
	private ProductImportService productImportService;
	@Mock
	private TransactionTemplate transactionTemplate;

	private ProductReferenceImportFacadeImpl testInstance;

	@Mock
	private WileyPurchaseOptionProductModel purchaseOptionOne;
	@Mock
	private WileyPurchaseOptionProductModel purchaseOptionTwo;
	@Mock
	private WileyProductModel baseProduct;

	@Before
	public void setUp() throws Exception
	{
		when(productImportService.getProductForCode(PURCHASE_OPTION_PRODUCT_CODE)).thenReturn(purchaseOptionOne);
		when(productImportService.getProductForCode(ANOTHER_PURCHASE_OPTION_PRODUCT_CODE)).thenReturn(purchaseOptionTwo);

		when(productImportService.getProductForCode(BASE_PRODUCT_CODE)).thenReturn(baseProduct);

		testInstance = new ProductReferenceImportFacadeImpl(modelService, importProductReferencePopulator, productImportService,
				transactionTemplate);

	}

	@Test
	public void shouldCheckIfBaseProductExists()
	{
		//When
		testInstance.createProductReference(emptyList(), BASE_PRODUCT_CODE, PURCHASE_OPTION_PRODUCT_CODE);
		//Then
		verify(productImportService).getProductForCode(BASE_PRODUCT_CODE);
	}

	@Test
	public void shouldCheckIfPurchaseOptionProductExists()
	{
		//When
		testInstance.createProductReference(emptyList(), BASE_PRODUCT_CODE, PURCHASE_OPTION_PRODUCT_CODE);
		//Then
		verify(productImportService).getProductForCode(PURCHASE_OPTION_PRODUCT_CODE);
	}

	@Test
	public void shouldCheckIfPurchaseOptionBelongsToBaseProduct()
	{
		//When
		testInstance.createProductReference(emptyList(), BASE_PRODUCT_CODE, PURCHASE_OPTION_PRODUCT_CODE);
		//Then
		verify(productImportService).checkVariantBelongsToBaseProduct(purchaseOptionOne, baseProduct);
	}

}