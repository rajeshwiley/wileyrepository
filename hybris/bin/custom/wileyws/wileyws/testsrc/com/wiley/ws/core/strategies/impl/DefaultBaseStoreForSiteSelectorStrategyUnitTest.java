package com.wiley.ws.core.strategies.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.store.BaseStoreModel;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.store.WileyBaseStoreService;


/**
 * Default unit test for {@link DefaultBaseStoreForSiteSelectorStrategy}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultBaseStoreForSiteSelectorStrategyUnitTest
{

	@Mock
	private WileyBaseStoreService wileyBaseStoreServiceMock;

	@InjectMocks
	private DefaultBaseStoreForSiteSelectorStrategy defaultBaseStoreForSiteSelectorStrategy;

	// Test data

	@Mock
	private BaseSiteModel baseSiteModelMock;

	@Mock
	private BaseStoreModel baseStoreModelMock;

	@Test
	public void testGetBaseStoreWhenStoreIsFound()
	{
		// Given
		when(wileyBaseStoreServiceMock.getBaseStoreForBaseSite(eq(baseSiteModelMock))).thenReturn(
				Optional.of(baseStoreModelMock));

		// When
		final BaseStoreModel baseStore = defaultBaseStoreForSiteSelectorStrategy.getBaseStore(baseSiteModelMock);

		// Then
		assertNotNull(baseStore);
	}

	@Test
	public void testGetBaseStoreWhenStoreIsNotFound()
	{
		// Given
		when(wileyBaseStoreServiceMock.getBaseStoreForBaseSite(eq(baseSiteModelMock))).thenReturn(Optional.empty());

		// When
		final BaseStoreModel baseStore = defaultBaseStoreForSiteSelectorStrategy.getBaseStore(baseSiteModelMock);

		// Then
		assertNull(baseStore);
	}

}