package com.wiley.ws.core.product.data.hub.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import com.wiley.ws.core.common.dto.CountrySwgDTO;
import com.wiley.ws.core.product.data.hub.dto.ProductSwgDTO;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Test for {@link CountryPDHPopulator}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CountryPDHPopulatorUnitTest
{
	public static final String TEST_COUNTRY_ID_1 = "US";
	public static final String TEST_COUNTRY_ID_2 = "CA";

	@InjectMocks
	private CountryPDHPopulator countryPDHPopulator;

	@Mock
	private CommonI18NService commonI18NServiceMock;

	@Mock
	private ProductSwgDTO productMock;

	@Mock
	private ProductModel productModelMock;

	@Test
	public void shouldSkipPopulatingDueToCountriesAreNull()
	{
		//Given
		when(productMock.getCountries()).thenReturn(null);

		//When
		countryPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock, never()).setCountryRestrictions(any());
	}

	@Test
	public void shouldPopulateCountries()
	{
		//Given
		List<CountrySwgDTO> countries = Arrays.asList(new CountrySwgDTO(TEST_COUNTRY_ID_1), new CountrySwgDTO(TEST_COUNTRY_ID_2));
		when(productMock.getCountries()).thenReturn(countries);
		List<CountryModel> countryModels = prepareServiceAndModels(countries);

		//When
		countryPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock).setCountryRestrictions(eq(countryModels));
	}

	private List<CountryModel> prepareServiceAndModels(final List<CountrySwgDTO> countries)
	{
		List<CountryModel> countryModels = new LinkedList<>();
		for (CountrySwgDTO country : countries)
		{
			CountryModel countryModel = mock(CountryModel.class);
			when(commonI18NServiceMock.getCountry(country.getValue())).thenReturn(countryModel);
			countryModels.add(countryModel);
		}
		return countryModels;
	}
}
