package com.wiley.ws.core.product.data.hub.populator;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.HybrisEnumValue;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import static org.junit.Assert.assertNotNull;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wiley.core.enums.ProductEditionFormat;
import com.wiley.core.enums.WileyDigitalContentTypeEnum;
import com.wiley.core.enums.WileyProductSubtypeEnum;
import com.wiley.ws.core.product.data.hub.dto.ProductSwgDTO;

import static com.wiley.ws.core.product.data.hub.dto.ProductSwgDTO.DigitalContentTypeEnum;
import static com.wiley.ws.core.product.data.hub.dto.ProductSwgDTO.EditionFormatEnum;
import static com.wiley.ws.core.product.data.hub.dto.ProductSwgDTO.SubtypeEnum;


/**
 * Test to check mapping between PDH DTO and hybris database values used in {@link BasicPDHPopulator}
 */
@IntegrationTest
public class PDHPopulatorsEnumerationIntegrationTest extends ServicelayerTransactionalTest
{

	private static final Logger LOG = LoggerFactory.getLogger(PDHPopulatorsEnumerationIntegrationTest.class);

	@Resource
	private BasicPDHPopulator basicPDHPopulator;

	private ProductSwgDTO productSwgDTO;
	private ProductModel productModel;

	@Before
	public void setup()
	{
		productSwgDTO = new ProductSwgDTO();
		productModel = new ProductModel();
	}

	@Test
	public void shouldPopulateEditionFormat()
	{

		for (EditionFormatEnum editionFormat : EditionFormatEnum.values())
		{
			productSwgDTO.setEditionFormat(editionFormat);

			basicPDHPopulator.populate(productSwgDTO, productModel);

			assertHybrisValue(editionFormat, ProductEditionFormat.class, productModel.getEditionFormat());
		}
	}

	@Test
	public void shouldPopulateSubtype()
	{

		for (SubtypeEnum subtypeEnum : SubtypeEnum.values())
		{
			productSwgDTO.setSubtype(subtypeEnum);

			basicPDHPopulator.populate(productSwgDTO, productModel);

			assertHybrisValue(subtypeEnum, WileyProductSubtypeEnum.class, productModel.getSubtype());
		}
	}

	@Test
	public void shouldPopulateDigitalContentTypeEnum()
	{

		for (DigitalContentTypeEnum digitalContentTypeEnum : DigitalContentTypeEnum.values())
		{
			productSwgDTO.setDigitalContentType(digitalContentTypeEnum);

			basicPDHPopulator.populate(productSwgDTO, productModel);

			assertHybrisValue(digitalContentTypeEnum, WileyDigitalContentTypeEnum.class, productModel.getDigitalContentType());
		}
	}

	private <T extends HybrisEnumValue> void assertHybrisValue(final Enum dtoEnumValue, final Class<T> hybrisEnum,
			final T hybrisEnumValue)
	{

		assertNotNull(
				String.format("DTO enum [%s.%s] hasn't any match to values in [%s] enum", dtoEnumValue.getClass().getSimpleName(),
						dtoEnumValue.name(), hybrisEnum.getSimpleName()), hybrisEnumValue);
	}
}
