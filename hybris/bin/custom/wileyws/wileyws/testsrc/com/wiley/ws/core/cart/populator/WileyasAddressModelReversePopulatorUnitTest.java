package com.wiley.ws.core.cart.populator;


import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.ws.core.cart.dto.CartAddressWsDTO;


/**
 * The type WileyasAddressModelReversePopulator unit test.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyasAddressModelReversePopulatorUnitTest
{
	public static final String POST_CODE = "POST_CODE";
	public static final String COUNTRY_ISO = "COUNTRY_ISO";
	public static final String REGION_ISO_SHORT = "REGION_ISO_SHORT";
	public static final String REGION_ISO = COUNTRY_ISO + "-" + REGION_ISO_SHORT;
	public static final String CITY = "CITY";
	public static final String LINE_1 = "LINE1";
	public static final String LINE_2 = "LINE2";
	public static final String FIRST_NAME = "FIRST NAME";
	public static final String LAST_NAME = "LAST NAME";
	public static final String PHONE_NUMBER = "PHONE NUMBER";
	public static final String EMAIL = "EMAIL";
	public static final String ORGANIZATION = "ORGANIZATION";
	public static final String DEPARTMENT = "DEPARTMENT";

	@InjectMocks
	private WileyasAddressModelReversePopulator testInstance;

	@Mock
	private CommonI18NService commonI18NServiceMock;

	@Mock
	private CartAddressWsDTO sourceMock;

	@Mock
	private AddressModel targetMock;

	@Mock
	private CountryModel countryModelMock;

	@Mock
	private RegionModel regionModelMock;

	@Before
	public void setUp()
	{
		when(countryModelMock.getIsocode()).thenReturn(COUNTRY_ISO);
		when(commonI18NServiceMock.getCountry(COUNTRY_ISO)).thenReturn(countryModelMock);
		when(commonI18NServiceMock.getRegion(countryModelMock, REGION_ISO)).thenReturn(regionModelMock);

		when(sourceMock.getPostcode()).thenReturn(POST_CODE);
		when(sourceMock.getCountry()).thenReturn(COUNTRY_ISO);
		when(sourceMock.getState()).thenReturn(REGION_ISO_SHORT);
		when(sourceMock.getCity()).thenReturn(CITY);
		when(sourceMock.getLine1()).thenReturn(LINE_1);
		when(sourceMock.getLine2()).thenReturn(LINE_2);
		when(sourceMock.getFirstName()).thenReturn(FIRST_NAME);
		when(sourceMock.getLastName()).thenReturn(LAST_NAME);
		when(sourceMock.getPhoneNumber()).thenReturn(PHONE_NUMBER);
		when(sourceMock.getEmail()).thenReturn(EMAIL);
		when(sourceMock.getOrganization()).thenReturn(ORGANIZATION);
		when(sourceMock.getDepartment()).thenReturn(DEPARTMENT);
	}

	@Test
	public void testPopulateFields()
	{
		// given

		// when
		testInstance.populate(sourceMock, targetMock);

		// then
		verifyMandatoryFieldsPopulated();
		verify(targetMock).setCountry(countryModelMock);
		verify(targetMock).setRegion(regionModelMock);
	}

	@Test
	public void testCountryAndRegionNotPopulatedIfCountryIsNull()
	{
		// given
		when(sourceMock.getCountry()).thenReturn(null);

		// when
		testInstance.populate(sourceMock, targetMock);

		// then
		verifyMandatoryFieldsPopulated();
		verify(targetMock, never()).setCountry(any());
		verify(targetMock, never()).setRegion(any());
	}

	@Test
	public void testOnlyRegionNotPopulatedIfStateIsNull()
	{
		// given
		when(sourceMock.getState()).thenReturn(null);

		// when
		testInstance.populate(sourceMock, targetMock);

		// then
		verifyMandatoryFieldsPopulated();
		verify(targetMock).setCountry(countryModelMock);
		verify(targetMock, never()).setRegion(any());
	}

	protected void verifyMandatoryFieldsPopulated()
	{
		verify(targetMock).setPostalcode(POST_CODE);
		verify(targetMock).setTown(CITY);
		verify(targetMock).setLine1(LINE_1);
		verify(targetMock).setLine2(LINE_2);
		verify(targetMock).setFirstname(FIRST_NAME);
		verify(targetMock).setLastname(LAST_NAME);
		verify(targetMock).setPhone1(PHONE_NUMBER);
		verify(targetMock).setEmail(EMAIL);
		verify(targetMock).setCompany(ORGANIZATION);
		verify(targetMock).setDepartment(DEPARTMENT);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testPopulateWithNullSource()
	{
		testInstance.populate(null, targetMock);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testPopulateWithNullTarget()
	{
		testInstance.populate(sourceMock, null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testPopulateWithNullSourceAndTarget()
	{
		testInstance.populate(null, null);
	}
}
