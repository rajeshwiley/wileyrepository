package com.wiley.ws.core.product.dataintegration.category.facade.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.validation.services.ValidationService;

import java.util.Arrays;

import javax.annotation.Resource;

import org.apache.commons.lang.LocaleUtils;
import org.junit.Before;
import org.junit.Test;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.category.WileyCategoryService;
import com.wiley.ws.core.product.dataintegration.category.dto.CategorySwgDTO;
import com.wiley.ws.core.product.dataintegration.category.facade.CategoryImportFacade;
import com.wiley.ws.core.product.dataintegration.common.dto.LocalizedStringSwgDTO;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;


@IntegrationTest
public class CategoryImportFacadeImplIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{

	private static final String CATALOG = "wileyProductCatalog";
	private static final String CATALOG_VERSION = "Staged";

	private static final String NEW_CATEGORY_ID = "testNewCategoryId";
	private static final String EXISTED_CATEGORY_ID = "testExistedCategoryId";
	private static final String CATEGORY_NAME = "test category name";
	private static final String LOCALE_EN = "en";
	private static final String CATEGORY_SUBJECTS = "subjects";

	@Resource
	private CategoryImportFacade categoryImportFacade;
	@Resource
	private WileyCategoryService categoryService;
	@Resource
	private ValidationService validationService;
	@Resource
	private CatalogVersionService catalogVersionService;
	@Resource
	private I18NService i18NService;

	@Before
	public void setUp() throws ImpExException
	{
		importCsv("/wileyws/test/category-import.impex", DEFAULT_ENCODING);
		i18NService.setCurrentLocale(LocaleUtils.toLocale(LOCALE_EN));
	}

	@Test
	public void shouldCreateCategory()
	{
		categoryImportFacade.createCategory(givenCategory(NEW_CATEGORY_ID));
		final CategoryModel categoryModel = categoryService.getCategoryForCode(NEW_CATEGORY_ID);
		assertEquals(CATEGORY_NAME, categoryModel.getName(LocaleUtils.toLocale(LOCALE_EN)));
		assertEquals(CATEGORY_SUBJECTS, categoryModel.getSupercategories().get(0).getCode());
	}

	@Test
	public void shouldUpdateCategory()
	{
		categoryImportFacade.updateCategory(EXISTED_CATEGORY_ID, givenCategory(EXISTED_CATEGORY_ID));
		final CategoryModel categoryModel = categoryService.getCategoryForCode(EXISTED_CATEGORY_ID);
		assertEquals(CATEGORY_NAME, categoryModel.getName(LocaleUtils.toLocale(LOCALE_EN)));
		assertEquals(CATEGORY_SUBJECTS, categoryModel.getSupercategories().get(0).getCode());
	}

	@Test
	public void shouldDeleteCategory()
	{
		assertTrue(categoryService.isCategoryForCodeExists(getCatalogVersion(), EXISTED_CATEGORY_ID));
		categoryImportFacade.deleteCategory(EXISTED_CATEGORY_ID);
		assertFalse(categoryService.isCategoryForCodeExists(getCatalogVersion(), EXISTED_CATEGORY_ID));
	}

	private CatalogVersionModel getCatalogVersion()
	{
		return catalogVersionService.getCatalogVersion(CATALOG, CATALOG_VERSION);
	}


	private CategorySwgDTO givenCategory(final String categoryId)
	{
		final CategorySwgDTO categorySwgDTO = new CategorySwgDTO();
		categorySwgDTO.setId(categoryId);
		categorySwgDTO.setName(Arrays.asList(givenLocalizedString(CATEGORY_NAME, LOCALE_EN)));
		categorySwgDTO.setSupercategories(Arrays.asList(CATEGORY_SUBJECTS));
		return categorySwgDTO;
	}

	private LocalizedStringSwgDTO givenLocalizedString(final String value, final String locale)
	{
		final LocalizedStringSwgDTO localizedString = new LocalizedStringSwgDTO();
		localizedString.setValue(value);
		localizedString.setLocale(locale);
		return localizedString;
	}


}
