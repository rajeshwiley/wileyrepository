package com.wiley.ws.core.product.data.hub.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.ExternalCompanyModel;
import com.wiley.core.externalcompany.WileycomExternalCompanyService;
import com.wiley.ws.core.product.data.hub.dto.ProductSwgDTO;


/**
 * Test for {@link CountryPDHPopulator}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExternalCompanyPDHPopulatorInitTest
{
	public static final String TEST_EXTERNAL_COMPANY = "textEC";

	@InjectMocks
	private ExternalCompanyPDHPopulator categoryPDHPopulator;

	@Mock
	private WileycomExternalCompanyService wileycomExternalCompanyServiceMock;

	@Mock
	private ProductSwgDTO productMock;

	@Mock
	private ProductModel productModelMock;

	@Mock
	private ExternalCompanyModel externalCompanyModelMock;

	@Test
	public void shouldSkipPopulatingDueToExternalCompanyIsNull()
	{
		//Given
		when(productMock.getExternalCompany()).thenReturn(null);

		//When
		categoryPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock, never()).setExternalCompany(any());
	}

	@Test
	public void shouldSkipPopulatingDueToExternalCompanyIsEmpty()
	{
		//Given
		when(productMock.getExternalCompany()).thenReturn(StringUtils.EMPTY);

		//When
		categoryPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock, never()).setExternalCompany(any());
	}

	@Test
	public void shouldPopulateCountry()
	{
		//Given
		ExternalCompanyModel externalCompanyModelMock = mock(ExternalCompanyModel.class);
		when(productMock.getExternalCompany()).thenReturn(TEST_EXTERNAL_COMPANY);
		when(wileycomExternalCompanyServiceMock.getExternalCompany(TEST_EXTERNAL_COMPANY))
				.thenReturn(externalCompanyModelMock);

		//When
		categoryPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock).setExternalCompany(eq(externalCompanyModelMock));
	}

}
