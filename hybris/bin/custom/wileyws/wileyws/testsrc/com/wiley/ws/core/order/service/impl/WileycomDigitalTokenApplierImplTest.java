package com.wiley.ws.core.order.service.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.ws.core.order.dto.DigitalProductAccessTokenWsDTO;
import com.wiley.ws.core.order.exception.OrderEntryWsNotFoundException;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;


/**
 * Author Herman_Chukhrai (EPAM)
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileycomDigitalTokenApplierImplTest
{
	@InjectMocks
	private WileycomDigitalTokenApplierImpl testInstanceMock;

	@Mock
	private DigitalProductAccessTokenWsDTO token1;
	@Mock
	private DigitalProductAccessTokenWsDTO token2;
	@Mock
	private AbstractOrderModel order;
	@Mock
	private AbstractOrderEntryModel entry1;
	@Mock
	private AbstractOrderEntryModel entry2;
	@Mock
	private ProductModel product1;
	@Mock
	private ProductModel product2;

	private static final String TOKEN_1_TOKEN = "TOKEN_1_TOKEN";
	private static final String TOKEN_1_ISBN = "TOKEN_1_ISBN";
	private static final String TOKEN_2_TOKEN = "TOKEN_2_TOKEN";
	private static final String TOKEN_2_ISBN = "TOKEN_2_ISBN";


	@Before
	public void setUp() throws Exception
	{
		when(token1.getIsbn()).thenReturn(TOKEN_1_ISBN);
		when(token1.getToken()).thenReturn(TOKEN_1_TOKEN);
		when(token2.getIsbn()).thenReturn(TOKEN_2_ISBN);
		when(token2.getToken()).thenReturn(TOKEN_2_TOKEN);

		when(order.getEntries()).thenReturn(asList(entry1, entry2));
		when(entry1.getProduct()).thenReturn(product1);
		when(entry2.getProduct()).thenReturn(product2);

	}

	@Test
	public void testOnlyOneTokenShouldBePopulated() throws Exception
	{
		when(product1.getIsbn()).thenReturn(TOKEN_1_ISBN);

		testInstanceMock.applyDigitalTokens(order, singletonList(token1));

		verify(entry1).setDigitalProductAccessToken(TOKEN_1_TOKEN);
		verify(entry2, never()).setDigitalProductAccessToken(Matchers.anyString());
	}

	@Test
	public void testTokensShouldBePopulated() throws Exception
	{
		when(product1.getIsbn()).thenReturn(TOKEN_1_ISBN);
		when(product2.getIsbn()).thenReturn(TOKEN_2_ISBN);

		testInstanceMock.applyDigitalTokens(order, asList(token1, token2));

		verify(entry1).setDigitalProductAccessToken(TOKEN_1_TOKEN);
		verify(entry2).setDigitalProductAccessToken(TOKEN_2_TOKEN);
	}

	@Test
	public void testNoOneTokenShouldBePopulated() throws Exception
	{
		testInstanceMock.applyDigitalTokens(order, Collections.emptyList());

		verify(entry1, never()).setDigitalProductAccessToken(Matchers.anyString());
		verify(entry2, never()).setDigitalProductAccessToken(Matchers.anyString());
	}

	@Test(expected = OrderEntryWsNotFoundException.class)
	public void testOrderEntryNotFound() throws Exception
	{
		when(product1.getIsbn()).thenReturn(TOKEN_2_ISBN);

		testInstanceMock.applyDigitalTokens(order, singletonList(token1));
	}
}