package com.wiley.ws.core.cart.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.util.PriceValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.integrations.order.dto.PriceValueWsDTO;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyasPriceValueReverseConverterUnitTest
{
	private static final double VALUE = 12.34;
	private static final String ISO_CODE = "USD";
	private static final String NONEXISTENED_ISO_CODE = "TTT";

	@Spy
	@InjectMocks
	private WileyasPriceValueReverseConverter testInstance;

	@Mock
	private PriceValue targetMock;

	@Mock
	private PriceValueWsDTO sourceMock;

	@Mock
	private CommonI18NService commonI18NServiceMock;

	@Mock
	private CurrencyModel currencyMock;

	@Before
	public void prepare()
	{
		when(sourceMock.getCurrency()).thenReturn(ISO_CODE);
		when(sourceMock.getValue()).thenReturn(VALUE);
		when(currencyMock.getIsocode()).thenReturn(ISO_CODE);
		when(commonI18NServiceMock.getCurrency(ISO_CODE)).thenReturn(currencyMock);
	}

	@Test(expected = Exception.class)
	public void testGetNonexistenedCurrency()
	{
		// given
		when(sourceMock.getCurrency()).thenReturn(NONEXISTENED_ISO_CODE);
		when(commonI18NServiceMock.getCurrency(NONEXISTENED_ISO_CODE)).thenThrow(new Exception());

		// when
		targetMock = testInstance.convert(sourceMock);

		// then
	}

	@Test
	public void testConvertFields()
	{
		// given

		// when
		targetMock = testInstance.convert(sourceMock);

		// then
		assertEquals(ISO_CODE, targetMock.getCurrencyIso());
		assertEquals(VALUE, targetMock.getValue(), 2);
	}

	@Test
	public void testConvertWithTwoAttributes()
	{
		// given

		// when
		PriceValue result = testInstance.convert(sourceMock, targetMock);

		// then
		assertEquals(ISO_CODE, result.getCurrencyIso());
		assertEquals(VALUE, result.getValue(), 2);
	}

	@Test
	public void testConvertWithNetTrue()
	{
		// given

		// when
		PriceValue result = testInstance.convert(sourceMock, targetMock);

		// then
		assertTrue(result.isNet());
	}
}
