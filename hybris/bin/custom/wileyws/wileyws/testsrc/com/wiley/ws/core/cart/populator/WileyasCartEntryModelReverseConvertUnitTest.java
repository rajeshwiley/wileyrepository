package com.wiley.ws.core.cart.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.servicelayer.model.ModelService;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.ws.core.cart.dto.CreateCartEntryRequestWsDTO;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyasCartEntryModelReverseConvertUnitTest
{

	@Spy
	@InjectMocks
	private WileyasCartEntryModelReverseConverter testInstance;

	@Mock
	private ModelService modelService;

	@Mock
	private CreateCartEntryRequestWsDTO sourceMock;

	@Mock
	private CartEntryModel targetMock;

	@Test
	public void testPopulateFields()
	{
		// given
		when(modelService.create(CartEntryModel.class)).thenReturn(targetMock);

		// when
		CartEntryModel result = testInstance.convert(sourceMock);

		// then
		verify(testInstance).superPopulate(sourceMock, targetMock);
		assertEquals(targetMock, result);
	}
}
