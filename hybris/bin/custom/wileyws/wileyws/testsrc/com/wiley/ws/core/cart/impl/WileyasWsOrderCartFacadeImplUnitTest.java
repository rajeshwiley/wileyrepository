package com.wiley.ws.core.cart.impl;


import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.order.CommerceSaveCartService;
import de.hybris.platform.commerceservices.order.CommerceSaveCartTextGenerationStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceSaveCartParameter;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.enums.OrderType;
import com.wiley.core.wiley.order.WileyCommerceCartService;
import com.wiley.ws.core.cart.dto.CreateCartRequestWsDTO;
import com.wiley.ws.core.cart.dto.WileyCartWsDTO;
import com.wiley.ws.core.exceptions.carts.WileyasSaveCartException;


/**
 * The type WileyasWsOrderCartFacadeImpl unit test.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyasWsOrderCartFacadeImplUnitTest
{
	private static final String CART_NAME_TEST = "CART_NAME_TEST";
	private static final String CART_DESCRIPTION_TEST = "CART_DESCRIPTION_TEST";
	private static final boolean ENFORCE = false;
	private static final String USER_AUTO_CREATION = "";
	private static final String USER_ID = "test user id";
	private static final String TEST_ORDER_TYPE = "orderType";

	@InjectMocks
	private WileyasWsOrderCartFacadeImpl testInstance;

	@Mock
	private CommerceSaveCartService commerceSaveCartServiceMock;

	@Mock
	private BaseSiteService baseSiteServiceMock;

	@Mock
	private CommerceSaveCartTextGenerationStrategy saveCartTextGenerationStrategyMock;

	@Mock
	private Converter<CreateCartRequestWsDTO, CartModel> cartModelReverseConverterMock;

	@Mock
	private Converter<CartModel, WileyCartWsDTO> cartModelConverterMock;

	@Mock
	private CreateCartRequestWsDTO requestMock;

	@Mock
	private CartModel cartModelMock;

	@Mock
	private WileyCartWsDTO cartWsDTOMock;

	@Mock
	private UserService userServiceMock;

	@Mock
	private UserModel userMock;

	@Mock
	private BaseSiteModel siteMock;

	@Mock
	private OrderType orderTypeMock;

	@Mock
	private WileyCommerceCartService wileyCommerceCartServiceMock;

	@Mock
	private ModelService modelServiceMock;

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Before
	public void setUp()
	{
		testInstance.setWileyasCartWsConverter(cartModelConverterMock);
		testInstance.setCartModelReverseConverter(cartModelReverseConverterMock);
		when(cartModelConverterMock.convert(cartModelMock)).thenReturn(cartWsDTOMock);
		when(userServiceMock.getCurrentUser()).thenReturn(userMock);
		when(userMock.getUid()).thenReturn(USER_ID);
		when(requestMock.getType()).thenReturn(TEST_ORDER_TYPE);
		when(baseSiteServiceMock.getCurrentBaseSite()).thenReturn(siteMock);
	}

	@Test
	public void testCreateCart() throws Exception
	{
		// given
		when(cartModelReverseConverterMock.convert(requestMock)).thenReturn(cartModelMock);
		when(saveCartTextGenerationStrategyMock.generateSaveCartName(cartModelMock)).thenReturn(CART_NAME_TEST);
		when(saveCartTextGenerationStrategyMock.generateSaveCartDescription(cartModelMock)).thenReturn(CART_DESCRIPTION_TEST);
		when(cartModelConverterMock.convert(cartModelMock)).thenReturn(cartWsDTOMock);

		// when
		WileyCartWsDTO result = testInstance.createCart(requestMock);

		// then
		assertEquals(result, cartWsDTOMock);
		ArgumentMatcher<CommerceSaveCartParameter> parameterArgumentMatcher = new CommerceSaveCartParameterArgumentMatcher(
				cartModelMock, CART_NAME_TEST, CART_DESCRIPTION_TEST);
		verify(commerceSaveCartServiceMock).saveCart(argThat(parameterArgumentMatcher));
	}

	@Test
	public void testCheckCartNotExists() throws Exception
	{
		// given
		when(wileyCommerceCartServiceMock.getAllCartsForUserAndType(eq(siteMock), eq(userMock), any(OrderType.class)))
				.thenReturn(Collections.emptyList());

		// when
		testInstance.ensureSingleCart(requestMock, false);
	}

	@Test
	public void testCartExistsWithoutNewCartCreated() throws Exception
	{
		// given
		when(wileyCommerceCartServiceMock.getAllCartsForUserAndType(eq(siteMock), eq(userMock), any(OrderType.class)))
				.thenReturn(Collections.singletonList(cartModelMock));
		thrown.expect(new WileyasSaveCartExceptionMatcher(cartWsDTOMock));

		// when
		testInstance.ensureSingleCart(requestMock, false);
	}

	@Test
	public void testCartExistsNewCartCreated() throws Exception
	{
		// given
		when(wileyCommerceCartServiceMock.getAllCartsForUserAndType(eq(siteMock), eq(userMock), any(OrderType.class)))
				.thenReturn(Collections.singletonList(cartModelMock));

		// when
		testInstance.ensureSingleCart(requestMock, true);
	}


	@Test
	public void testMultipleCartExists() throws Exception
	{
		// given
		CartModel cart1 = mockCart(20202);
		CartModel cart2 = mockCart(10); //pk with lowest order
		CartModel cart3 = mockCart(40404);
		WileyCartWsDTO cart2WsDTOMock = mock(WileyCartWsDTO.class);
		when(cartModelConverterMock.convert(cart2)).thenReturn(cart2WsDTOMock);
		when(wileyCommerceCartServiceMock.getAllCartsForUserAndType(eq(siteMock), eq(userMock), any(OrderType.class)))
				.thenReturn(Arrays.asList(cart1, cart2, cart3));
		thrown.expect(new WileyasSaveCartExceptionMatcher(cart2WsDTOMock));


		try
		{
			// when
			testInstance.ensureSingleCart(requestMock, false);
		}
		finally
		{
			// then
			verify(modelServiceMock).removeAll(Arrays.asList(cart1, cart3));
		}

	}


	private CartModel mockCart(long pk)
	{
		CartModel cart = mock(CartModel.class);
		when(cart.getPk()).thenReturn(PK.fromLong(pk));
		return cart;
	}

	private class CommerceSaveCartParameterArgumentMatcher extends ArgumentMatcher<CommerceSaveCartParameter>
	{
		private CartModel expectedCartModel;
		private String expectedName;
		private String expectedDescription;

		CommerceSaveCartParameterArgumentMatcher(final CartModel cartModel, final String expectedName,
				final String expectedDescription)
		{
			this.expectedCartModel = cartModel;
			this.expectedName = expectedName;
			this.expectedDescription = expectedDescription;
		}

		@Override
		public boolean matches(final Object o)
		{
			CommerceSaveCartParameter parameter = (CommerceSaveCartParameter) o;
			return expectedCartModel.equals(parameter.getCart()) && expectedName.equals(parameter.getName())
					&& expectedDescription.equals(parameter.getDescription());
		}
	}


	class WileyasSaveCartExceptionMatcher extends BaseMatcher<WileyasSaveCartException>
	{
		private WileyCartWsDTO existingCart;

		WileyasSaveCartExceptionMatcher(final WileyCartWsDTO existingCart)
		{
			this.existingCart = existingCart;
		}

		@Override
		public boolean matches(final Object o)
		{
			if (o instanceof WileyasSaveCartException)
			{
				WileyasSaveCartException cartException = (WileyasSaveCartException) o;
				return cartException.getExistingCart() == existingCart;
			}
			return false;
		}

		@Override
		public void describeTo(final Description description)
		{
			description.appendValue(WileyasSaveCartException.class)
					.appendText(" with existingCart=").appendValue(existingCart);
		}

		@Override
		public void describeMismatch(final Object item, final Description description)
		{
			description.appendText("was ").appendValue(item.getClass());
			if (item instanceof WileyasSaveCartException)
			{
				description.appendText(" with existingCart=").appendValue(((WileyasSaveCartException) item).getExistingCart());
			}
		}
	}
}
