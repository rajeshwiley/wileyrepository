package com.wiley.ws.core.customer;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.customer.impl.DefaultCustomerAccountService;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.persistence.security.PasswordEncoder;
import de.hybris.platform.persistence.security.PasswordEncoderFactory;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.i18n.WileyCountryService;
import com.wiley.ws.core.customers.impl.WileyWsCustomerFacadeImpl;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;


@IntegrationTest
public class WileyWsCustomerFacadeImplIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{

	private static final String TEST_CUSTOMER_UID = "TEST@test.com";
	private static final String TESTPASS = "testpass";
	private static final String TEST_NAME_F = "TestNameF";
	private static final String TEST_NAME_L = "TestNameL";
	private static final String MR = "mr";
	private static final String PBKDF_2 = "pbkdf2";
	private static final String EN = "en";
	private static final String USD = "USD";
	private static final String SESSION_COUNTRY = "US";
	private static final String TEST_EXTERNAL_APPLICATION_ID = "TEST_EXTERNAL_APPLICATION_ID";
	private static final String TEST_EXTERNAL_CUSTOMER_ID = "TEST_EXTERNAL_CUSTOMER_ID";

	@Resource
	private WileyWsCustomerFacadeImpl wileyWsCustomerFacade;
	@Resource
	private UserService userService;
	@Resource(name = "core.passwordEncoderFactory")
	private PasswordEncoderFactory encoderFactory;
	@Resource
	private CommonI18NService commonI18NService;
	@Resource
	private SessionService sessionService;
	@Resource
	private WileyCountryService wileyCountryService;
	@Resource
	private DefaultCustomerAccountService customerAccountService;

	private RegisterData registerData;
	private LanguageModel currentLanguage;
	private CurrencyModel currentCurrency;

	@Before
	public void setUp() throws ImpExException
	{
		registerData = new RegisterData();
		registerData.setLogin(TEST_CUSTOMER_UID);
		registerData.setPassword(TESTPASS);
		registerData.setFirstName(TEST_NAME_F);
		registerData.setLastName(TEST_NAME_L);
		registerData.setTitleCode(MR);
		registerData.setExternalApplicationId(TEST_EXTERNAL_APPLICATION_ID);
		registerData.setExternalCustomerId(TEST_EXTERNAL_CUSTOMER_ID);

		currentLanguage = commonI18NService.getLanguage(EN);
		commonI18NService.setCurrentLanguage(currentLanguage);

		currentCurrency = commonI18NService.getCurrency(USD);
		commonI18NService.setCurrentCurrency(currentCurrency);

		sessionService.setAttribute("sessionCountry", wileyCountryService.findCountryByCode(SESSION_COUNTRY));

		customerAccountService.setPasswordEncoding(PBKDF_2);
	}

	@Test
	public void shouldCreateCustomer() throws DuplicateUidException
	{
		wileyWsCustomerFacade.register(registerData);

		final CustomerModel customerModel = (CustomerModel) userService.getUserForUID(TEST_CUSTOMER_UID.toLowerCase());

		assertEquals(TEST_CUSTOMER_UID.toLowerCase(), customerModel.getUid());
		assertEquals(TEST_CUSTOMER_UID, customerModel.getOriginalUid());
		assertEquals(TEST_NAME_F, customerModel.getFirstName());
		assertEquals(TEST_NAME_L, customerModel.getLastName());
		assertEquals(TEST_NAME_F + " " + TEST_NAME_L, customerModel.getName());
		assertEquals(MR, customerModel.getTitle().getCode());
		final PasswordEncoder pbkdf2Encoder = encoderFactory.getEncoder(PBKDF_2);
		assertTrue(pbkdf2Encoder.check(customerModel.getUid(), customerModel.getEncodedPassword(), TESTPASS));
		assertEquals(EN, customerModel.getSessionLanguage().getIsocode());
		assertEquals(USD, customerModel.getSessionCurrency().getIsocode());
		assertEquals(SESSION_COUNTRY, customerModel.getSessionCountry().getIsocode());
		assertEquals(TEST_EXTERNAL_APPLICATION_ID, customerModel.getExternalApplicationId());
		assertEquals(TEST_EXTERNAL_CUSTOMER_ID, customerModel.getExternalCustomerId());
	}

	@Test(expected = IllegalArgumentException.class)
	public void throwsExceptionIfLoginNull() throws DuplicateUidException
	{
		registerData.setLogin(null);

		wileyWsCustomerFacade.register(registerData);
	}

	@Test(expected = IllegalArgumentException.class)
	public void throwsExceptionIfLoginEmpty() throws DuplicateUidException
	{
		registerData.setLogin("");

		wileyWsCustomerFacade.register(registerData);
	}

	@Test(expected = IllegalArgumentException.class)
	public void throwsExceptionIfPasswordNull() throws DuplicateUidException
	{
		registerData.setPassword(null);

		wileyWsCustomerFacade.register(registerData);
	}

	@Test(expected = IllegalArgumentException.class)
	public void throwsExceptionIfPasswordEmpty() throws DuplicateUidException
	{
		registerData.setPassword("");

		wileyWsCustomerFacade.register(registerData);
	}

	@Test(expected = IllegalArgumentException.class)
	public void throwsExceptionIfTakesNull() throws DuplicateUidException
	{
		wileyWsCustomerFacade.register(null);
	}

	@Test
	public void thereIsNoNameIfAtLeastOneNameEmpty() throws DuplicateUidException
	{
		registerData.setFirstName(null);

		wileyWsCustomerFacade.register(registerData);

		final CustomerModel customerModel = (CustomerModel) userService.getUserForUID(TEST_CUSTOMER_UID.toLowerCase());

		assertEquals(TEST_CUSTOMER_UID.toLowerCase(), customerModel.getUid());
		assertEquals(TEST_CUSTOMER_UID, customerModel.getOriginalUid());
		assertNull(customerModel.getFirstName());
		assertNull(customerModel.getLastName());
		assertNull(customerModel.getName());
		assertEquals(MR, customerModel.getTitle().getCode());
		final PasswordEncoder pbkdf2Encoder = encoderFactory.getEncoder(PBKDF_2);
		assertTrue(pbkdf2Encoder.check(customerModel.getUid(), customerModel.getEncodedPassword(), TESTPASS));
		assertEquals(EN, customerModel.getSessionLanguage().getIsocode());
		assertEquals(USD, customerModel.getSessionCurrency().getIsocode());
		assertEquals(SESSION_COUNTRY, customerModel.getSessionCountry().getIsocode());
		assertEquals(TEST_EXTERNAL_APPLICATION_ID, customerModel.getExternalApplicationId());
		assertEquals(TEST_EXTERNAL_CUSTOMER_ID, customerModel.getExternalCustomerId());
	}

	@Test
	public void thereIsNoTitleIfTitleCodeIsEmpty() throws DuplicateUidException
	{
		registerData.setTitleCode(null);

		wileyWsCustomerFacade.register(registerData);

		final CustomerModel customerModel = (CustomerModel) userService.getUserForUID(TEST_CUSTOMER_UID.toLowerCase());

		assertEquals(TEST_CUSTOMER_UID.toLowerCase(), customerModel.getUid());
		assertEquals(TEST_CUSTOMER_UID, customerModel.getOriginalUid());
		assertEquals(TEST_NAME_F, customerModel.getFirstName());
		assertEquals(TEST_NAME_L, customerModel.getLastName());
		assertEquals(TEST_NAME_F + " " + TEST_NAME_L, customerModel.getName());
		assertNull(customerModel.getTitle());
		final PasswordEncoder pbkdf2Encoder = encoderFactory.getEncoder(PBKDF_2);
		assertTrue(pbkdf2Encoder.check(customerModel.getUid(), customerModel.getEncodedPassword(), TESTPASS));
		assertEquals(EN, customerModel.getSessionLanguage().getIsocode());
		assertEquals(USD, customerModel.getSessionCurrency().getIsocode());
		assertEquals(SESSION_COUNTRY, customerModel.getSessionCountry().getIsocode());
		assertEquals(TEST_EXTERNAL_APPLICATION_ID, customerModel.getExternalApplicationId());
		assertEquals(TEST_EXTERNAL_CUSTOMER_ID, customerModel.getExternalCustomerId());
	}

	@Test(expected = DuplicateUidException.class)
	public void throwsExceptionForDuplicateUid() throws DuplicateUidException
	{
		wileyWsCustomerFacade.register(registerData);
		wileyWsCustomerFacade.register(registerData);
	}

	@Test
	public void returnCustomerId() throws DuplicateUidException
	{
		wileyWsCustomerFacade.register(registerData);

		String customerId = wileyWsCustomerFacade.getCustomerId(registerData.getLogin().toLowerCase());

		assertNotNull(customerId);
	}

	@Test(expected = IllegalArgumentException.class)
	public void throwsExceptionIfUidIsNull()
	{
		wileyWsCustomerFacade.getCustomerId(null);
	}
}
