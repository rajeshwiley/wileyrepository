package com.wiley.ws.core.cart.populator;


import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.ws.core.cart.dto.CartAddressWsDTO;


/**
 * The type WileyasAddressModelReverseConverter unit test.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyasAddressModelReverseConverterUnitTest
{
	@Spy
	@InjectMocks
	private WileyasAddressModelReverseConverter testInstance;

	@Mock
	private ModelService modelServiceMock;

	@Mock
	private CartAddressWsDTO sourceMock;

	@Mock
	private AddressModel targetMock;

	@Test
	public void testConvert()
	{
		// given
		when(modelServiceMock.create(AddressModel.class)).thenReturn(targetMock);

		// when
		AddressModel result = testInstance.convert(sourceMock);

		// then
		assertEquals(result, targetMock);
		verify(testInstance).superPopulate(sourceMock, targetMock);
	}
}
