package com.wiley.ws.core.order;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.ordersplitting.WarehouseService;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Optional;
import java.util.TimeZone;

import javax.validation.ValidationException;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.wileyws.order.WileycomWsOrderService;
import com.wiley.ws.core.order.dto.ShippingEventEntryWsDTO;
import com.wiley.ws.core.order.dto.ShippingEventWsDTO;
import com.wiley.ws.core.order.exception.IsbnNotFoundException;


/**
 * Created by Georgii_Gavrysh on 8/12/2016.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileycomWsConsignmentTransformerUnitTest
{
	private static final String TRACKING_ID_TEST = "TRACKING_ID_TEST";
	private static final String CARRIER_TEST = "CARRIER_TEST";
	private static final String SHIPPING_DATE_TEST = "2001-01-07T16:39:57-08:00";
	private static final String ISBN_PRODUCT_FIRST_ORDER_ENTRY = "ISBN_PRODUCT_FIRST_ORDER_ENTRY";
	private static final String ISBN_NO_PRODUCT_IN_ORDER = "ISBN_NO_PRODUCT_IN_ORDER";

	private static Date correspondingDate;

	@BeforeClass
	public static void setupClass()
	{
		final Calendar calendar = Calendar.getInstance();
		calendar.set(2001, Calendar.JANUARY, 07, 14, 39, 57);
		calendar.set(Calendar.MILLISECOND, 0);
		calendar.setTimeZone(TimeZone.getTimeZone("GMT-10:00"));
		correspondingDate = calendar.getTime();
	}

	@Mock
	private ModelService modelServiceMock;

	@Mock
	private WarehouseService warehouseServiceMock;

	@Mock
	private ConfigurationService configurationServiceMock;

	@Mock
	private Configuration configurationMock;

	@Mock
	private WileycomWsOrderService wileycomWsOrderServiceMock;

	@Mock
	private OrderModel orderMock;

	@Mock
	private OrderEntryModel orderEntryFirstMock;

	private Optional<AbstractOrderEntryModel> optionalOrderEntryFirst;

	@Mock
	private ConsignmentModel consignmentMock;

	@Mock
	private ConsignmentEntryModel consignmentEntryFirstMock;

	private ShippingEventWsDTO shippingEventWsDTOMock;

	private ShippingEventEntryWsDTO shippingEventEntryWsDTOFirst;

	@Mock
	private AddressModel deliveryAddressMock;

	@Mock
	private DeliveryModeModel deliveryModeMock;

	@Mock
	private WarehouseModel warehouseMock;

	private WileycomWsConsignmentTransformer testedClass;

	@Before
	public void setup()
	{
		testedClass = new WileycomWsConsignmentTransformer();
		testedClass.setModelService(modelServiceMock);
		testedClass.setWarehouseService(warehouseServiceMock);
		testedClass.setWileycomWsOrderService(wileycomWsOrderServiceMock);
		testedClass.setConfigurationService(configurationServiceMock);

		when(configurationServiceMock.getConfiguration()).thenReturn(configurationMock);
		when(configurationMock.getString("rfc3339.java.date.format", "yyyy-MM-dd'T'HH:mm:ssXXX")).
				thenReturn("yyyy-MM-dd'T'HH:mm:ssXXX");

		when(modelServiceMock.create(ConsignmentModel.class)).thenReturn(consignmentMock);
		when(modelServiceMock.create(ConsignmentEntryModel.class)).thenReturn(consignmentEntryFirstMock);

		when(warehouseServiceMock.getDefWarehouse()).thenReturn(Collections.singletonList(warehouseMock));

		shippingEventEntryWsDTOFirst = new ShippingEventEntryWsDTO();
		shippingEventEntryWsDTOFirst.setIsbn(ISBN_PRODUCT_FIRST_ORDER_ENTRY);

		optionalOrderEntryFirst = Optional.of(orderEntryFirstMock);

		when(wileycomWsOrderServiceMock.getOrderEntryModelByISBN(orderMock, ISBN_NO_PRODUCT_IN_ORDER)).
				thenReturn(Optional.empty());
	}

	private void setupShippingDto()
	{
		shippingEventWsDTOMock = new ShippingEventWsDTO();
		shippingEventWsDTOMock.setTrackingId(TRACKING_ID_TEST);
		shippingEventWsDTOMock.setCarrier(CARRIER_TEST);
		shippingEventWsDTOMock.setShippingDate(SHIPPING_DATE_TEST);
		shippingEventWsDTOMock.setEntries(Collections.EMPTY_LIST);
	}

	private void setupShippingDtoWithOneEntry()
	{
		setupShippingDto();
		shippingEventWsDTOMock.setEntries(Arrays.asList(shippingEventEntryWsDTOFirst));
		shippingEventEntryWsDTOFirst.setIsbn(ISBN_PRODUCT_FIRST_ORDER_ENTRY);
	}

	private void setupOrder()
	{
		when(orderMock.getDeliveryAddress()).thenReturn(deliveryAddressMock);
		when(orderMock.getDeliveryMode()).thenReturn(deliveryModeMock);
	}

	private void setupOrderWithOneEntry()
	{
		setupOrder();
		when(wileycomWsOrderServiceMock.getOrderEntryModelByISBN(orderMock, ISBN_PRODUCT_FIRST_ORDER_ENTRY)).
				thenReturn(optionalOrderEntryFirst);
	}

	@Test
	public void testSuccessConsignment()
	{
		setupShippingDto();
		setupOrder();
		testedClass.createConsignment(shippingEventWsDTOMock, orderMock);
		verify(consignmentMock).setCarrier(CARRIER_TEST);
		verify(consignmentMock).setTrackingID(TRACKING_ID_TEST);
		verify(consignmentMock).setCode(Matchers.startsWith(orderMock.getCode() + TRACKING_ID_TEST));
		verify(consignmentMock).setShippingAddress(deliveryAddressMock);
		verify(consignmentMock).setDeliveryMode(deliveryModeMock);
		verify(consignmentMock).setStatus(ConsignmentStatus.SHIPPED);
		verify(consignmentMock).setOrder(orderMock);
		verify(consignmentMock).setWarehouse(warehouseMock);
		verify(consignmentMock).setShippingDate(correspondingDate);
	}

	@Test
	public void testSuccessOneConsignmentEntry()
	{
		setupShippingDtoWithOneEntry();
		setupOrderWithOneEntry();

		shippingEventEntryWsDTOFirst.setShippedQuantity("2");
		when(orderEntryFirstMock.getQuantity()).thenReturn(3L);

		testedClass.createConsignment(shippingEventWsDTOMock, orderMock);

		verify(consignmentMock).setConsignmentEntries(Collections.singleton(consignmentEntryFirstMock));

		verify(consignmentEntryFirstMock).setConsignment(consignmentMock);
		verify(consignmentEntryFirstMock).setShippedQuantity(2L);
		verify(consignmentEntryFirstMock).setQuantity(3L);
		verify(consignmentEntryFirstMock).setOrderEntry(orderEntryFirstMock);
	}

	@Test(expected = IsbnNotFoundException.class)
	public void testNoSuchISBNInOrder()
	{
		setupShippingDtoWithOneEntry();
		setupOrderWithOneEntry();
		shippingEventEntryWsDTOFirst.setIsbn(ISBN_NO_PRODUCT_IN_ORDER);

		shippingEventEntryWsDTOFirst.setShippedQuantity("2");
		when(orderEntryFirstMock.getQuantity()).thenReturn(3L);

		testedClass.createConsignment(shippingEventWsDTOMock, orderMock);
	}

	@Test(expected = ValidationException.class)
	public void testNonPositiveShippedQuantity()
	{
		setupShippingDtoWithOneEntry();
		setupOrderWithOneEntry();
		shippingEventEntryWsDTOFirst.setShippedQuantity("-1");
		testedClass.createConsignment(shippingEventWsDTOMock, orderMock);
	}
}
