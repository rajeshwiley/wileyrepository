package com.wiley.ws.core.order.service.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Author Herman_Chukhrai (EPAM)
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileycomSubtotalWithoutDiscountApplierImplTest
{
	@InjectMocks
	private WileycomSubtotalWithoutDiscountApplierImpl subtotalWithoutDiscountApplier;
	@Mock
	private AbstractOrderModel orderMock;
	@Mock
	private AbstractOrderEntryModel entry1Mock;
	@Mock
	private AbstractOrderEntryModel entry2Mock;

	private static final Double ENTRY_1_BASE_PRICE = 2.0;
	private static final Long ENTRY_1_QUANTITY = 11L;
	private static final Double ENTRY_2_BASE_PRICE = 2.0;
	private static final Long ENTRY_2_QUANTITY = 22L;
	private static final Double SUBTOTAL_WITHOUT_DISCOUNT = 66.0;

	@Test
	public void testSubtotalCalculation() throws Exception
	{
		//given
		when(orderMock.getEntries()).thenReturn(Arrays.asList(entry1Mock, entry2Mock));
		when(entry1Mock.getBasePrice()).thenReturn(ENTRY_1_BASE_PRICE);
		when(entry1Mock.getQuantity()).thenReturn(ENTRY_1_QUANTITY);
		when(entry2Mock.getBasePrice()).thenReturn(ENTRY_2_BASE_PRICE);
		when(entry2Mock.getQuantity()).thenReturn(ENTRY_2_QUANTITY);

		//when
		subtotalWithoutDiscountApplier.recalculateAndSetSubtotalWithoutDiscount(orderMock);

		//then
		verify(orderMock).setSubTotalWithoutDiscount(SUBTOTAL_WITHOUT_DISCOUNT);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testNullableOrderProvided() throws Exception
	{
		//given
		final AbstractOrderModel orderModel = null;

		//when
		subtotalWithoutDiscountApplier.recalculateAndSetSubtotalWithoutDiscount(orderModel);
	}
}