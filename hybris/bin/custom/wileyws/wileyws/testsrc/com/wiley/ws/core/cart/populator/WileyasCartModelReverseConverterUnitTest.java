package com.wiley.ws.core.cart.populator;


import com.wiley.core.enums.OrderType;
import com.wiley.core.cart.WileyCartFactory;
import com.wiley.ws.core.cart.dto.CreateCartRequestWsDTO;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.CartModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


/**
 * The type WileyasCartModelReverseConverter unit test.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyasCartModelReverseConverterUnitTest
{
	public static final OrderType ORDER_TYPE = OrderType.AS_FUNDED;
	public static final String ORDER_TYPE_CODE = ORDER_TYPE.getCode();

	@Spy
	@InjectMocks
	private WileyasCartModelReverseConverter testInstance;

	@Mock
	private WileyCartFactory wileyCartFactory;

	@Mock
	private CreateCartRequestWsDTO sourceMock;

	@Mock
	private CartModel targetMock;

	@Test
	public void testPopulateFields()
	{
		// given
		when(sourceMock.getType()).thenReturn(ORDER_TYPE_CODE);
		when(wileyCartFactory.createCartWithType(ORDER_TYPE)).thenReturn(targetMock);

		// when
		CartModel result = testInstance.convert(sourceMock);

		// then
		verify(testInstance).superPopulate(sourceMock, targetMock);
		assertSame(result, targetMock);
	}

	@Test
	public void testPopulateDefaultCartType()
	{
		// given
		when(sourceMock.getType()).thenReturn(null);
		when(wileyCartFactory.createCartWithType(OrderType.GENERAL)).thenReturn(targetMock);

		// when
		CartModel result = testInstance.convert(sourceMock);

		// then
		verify(testInstance).superPopulate(sourceMock, targetMock);
		assertSame(result, targetMock);
	}
}
