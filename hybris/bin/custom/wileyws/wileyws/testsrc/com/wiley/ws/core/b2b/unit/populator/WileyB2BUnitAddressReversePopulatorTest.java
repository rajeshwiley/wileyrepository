package com.wiley.ws.core.b2b.unit.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.i18n.WileyCommonI18NService;
import com.wiley.ws.core.b2b.unit.dto.AddressSwgDTO;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyB2BUnitAddressReversePopulatorTest
{
	private static final String ADDRESS_ID = "1234";
	private static final String COUNTRY = "PL";
	private static final String STATE = "KR";
	private static final String CITY = "Krakow";
	private static final String FIRST_NAME = "Bob";
	private static final String LAST_NAME = "Bill";
	private static final String LINE_1 = "line1";
	private static final String LINE_2 = "line1";
	private static final String PHONE_NUMBER = "911";
	private static final String POST_CODE = "098-221";

	@InjectMocks
	private WileyB2BUnitAddressReversePopulator testInstance;

	private AddressSwgDTO notEmptyAddressSwgDTO = new AddressSwgDTO();
	private AddressSwgDTO emptyAddressSwgDTOWithId = new AddressSwgDTO();
	private AddressSwgDTO emptyAddressSwgDTO = new AddressSwgDTO();

	@Mock
	private AddressModel mockAddressModel;
	@Mock
	private CommonI18NService mockCommonI18NService;
	@Mock
	private WileyCommonI18NService mockWileyCommonI18NService;
	@Mock
	private CountryModel mockCountryModel;
	@Mock
	private RegionModel mockRegionModel;


	@Before
	public void setUp()
	{
		notEmptyAddressSwgDTO.setAddressId(ADDRESS_ID);
		notEmptyAddressSwgDTO.setCity(CITY);
		notEmptyAddressSwgDTO.setCountry(COUNTRY);
		notEmptyAddressSwgDTO.setFirstName(FIRST_NAME);
		notEmptyAddressSwgDTO.setLastName(LAST_NAME);
		notEmptyAddressSwgDTO.setLine1(LINE_1);
		notEmptyAddressSwgDTO.setLine2(LINE_2);
		notEmptyAddressSwgDTO.setPhoneNumber(PHONE_NUMBER);
		notEmptyAddressSwgDTO.setPostcode(POST_CODE);
		notEmptyAddressSwgDTO.setState(STATE);

		emptyAddressSwgDTOWithId.setAddressId(ADDRESS_ID);

		when(mockCountryModel.getIsocode()).thenReturn(COUNTRY);

		when(mockCommonI18NService.getCountry(COUNTRY)).thenReturn(mockCountryModel);
		when(mockWileyCommonI18NService.getRegionForShortCode(mockCountryModel, STATE)).thenReturn(mockRegionModel);
		when(mockAddressModel.getCountry()).thenReturn(mockCountryModel);
	}

	@Test
	public void shouldSetExternalIdIfNotNull()
	{
		testInstance.populate(notEmptyAddressSwgDTO, mockAddressModel);
		verify(mockAddressModel).setExternalId(ADDRESS_ID);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowExceptionIfExternalIdIsNull()
	{
		testInstance.populate(emptyAddressSwgDTO, mockAddressModel);
	}

	@Test
	public void shouldSetPostCodeIfNotNull()
	{
		testInstance.populate(notEmptyAddressSwgDTO, mockAddressModel);
		verify(mockAddressModel).setPostalcode(POST_CODE);
	}

	@Test
	public void shouldNotSetPostCodeIfNull()
	{
		testInstance.populate(emptyAddressSwgDTOWithId, mockAddressModel);
		verify(mockAddressModel, never()).setPostalcode(POST_CODE);
	}

	@Test
	public void shouldSetCountryIfNotNull()
	{
		testInstance.populate(notEmptyAddressSwgDTO, mockAddressModel);
		verify(mockAddressModel).setCountry(mockCountryModel);
	}

	@Test
	public void shouldNotSetCountryIfNull()
	{
		testInstance.populate(emptyAddressSwgDTOWithId, mockAddressModel);
		verify(mockAddressModel, never()).setCountry(mockCountryModel);
	}

	@Test
	public void shouldSetRegionIfNotNull()
	{
		testInstance.populate(notEmptyAddressSwgDTO, mockAddressModel);
		verify(mockAddressModel).setRegion(mockRegionModel);
	}

	@Test(expected = UnknownIdentifierException.class)
	public void shouldThrowAnExceptionIfNoRegionFound()
	{
		when(mockWileyCommonI18NService.getRegionForShortCode(mockCountryModel, STATE)).thenThrow(
				UnknownIdentifierException.class);
		testInstance.populate(notEmptyAddressSwgDTO, mockAddressModel);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowAnExceptionForRegionUpdateIfCountryIsNull()
	{
		when(mockAddressModel.getCountry()).thenReturn(null);
		testInstance.populate(notEmptyAddressSwgDTO, mockAddressModel);
	}

	@Test
	public void shouldNotSetRegionIfNull()
	{
		testInstance.populate(emptyAddressSwgDTOWithId, mockAddressModel);
		verify(mockAddressModel, never()).setRegion(mockRegionModel);
	}

	@Test
	public void shouldSetCityIfNotNull()
	{
		testInstance.populate(notEmptyAddressSwgDTO, mockAddressModel);
		verify(mockAddressModel).setTown(CITY);
	}

	@Test
	public void shouldNotSetCityIfNull()
	{
		testInstance.populate(emptyAddressSwgDTOWithId, mockAddressModel);
		verify(mockAddressModel, never()).setTown(CITY);
	}

	@Test
	public void shouldSetStreetNameIfNotNull()
	{
		testInstance.populate(notEmptyAddressSwgDTO, mockAddressModel);
		verify(mockAddressModel).setStreetname(LINE_1);
	}

	@Test
	public void shouldNotSetStreetNameIfNull()
	{
		testInstance.populate(emptyAddressSwgDTOWithId, mockAddressModel);
		verify(mockAddressModel, never()).setStreetname(LINE_1);
	}

	@Test
	public void shouldSetStreetNumberIfNotNull()
	{
		testInstance.populate(notEmptyAddressSwgDTO, mockAddressModel);
		verify(mockAddressModel).setStreetnumber(LINE_2);
	}

	@Test
	public void shouldNotSetStreetNumberIfNull()
	{
		testInstance.populate(emptyAddressSwgDTOWithId, mockAddressModel);
		verify(mockAddressModel, never()).setStreetnumber(LINE_2);
	}

	@Test
	public void shouldSetFirstNameIfNotNull()
	{
		testInstance.populate(notEmptyAddressSwgDTO, mockAddressModel);
		verify(mockAddressModel).setFirstname(FIRST_NAME);
	}

	@Test
	public void shouldNotSetFirstNameIfNull()
	{
		testInstance.populate(emptyAddressSwgDTOWithId, mockAddressModel);
		verify(mockAddressModel, never()).setFirstname(FIRST_NAME);
	}

	@Test
	public void shouldSetLastNameIfNotNull()
	{
		testInstance.populate(notEmptyAddressSwgDTO, mockAddressModel);
		verify(mockAddressModel).setLastname(LAST_NAME);
	}

	@Test
	public void shouldNotSetLastNameIfNull()
	{
		testInstance.populate(emptyAddressSwgDTOWithId, mockAddressModel);
		verify(mockAddressModel, never()).setLastname(LAST_NAME);
	}

	@Test
	public void shouldSetPhoneNumberIfNotNull()
	{
		testInstance.populate(notEmptyAddressSwgDTO, mockAddressModel);
		verify(mockAddressModel).setPhone1(PHONE_NUMBER);
	}

	@Test
	public void shouldNotSetPhoneNumberIfNull()
	{
		testInstance.populate(emptyAddressSwgDTOWithId, mockAddressModel);
		verify(mockAddressModel, never()).setPhone1(PHONE_NUMBER);
	}
}
