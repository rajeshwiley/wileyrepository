package com.wiley.ws.core.product.data.hub.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.ws.core.product.data.hub.converter.MediaSwgDTOPDHConverter;
import com.wiley.ws.core.product.data.hub.dto.MediaSwgDTO;
import com.wiley.ws.core.product.data.hub.dto.ProductSwgDTO;


/**
 * Test for {@link MediaPDHPopulator}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class MediaPDHPopulatorUnitTest
{

	private static final String TEST_PRODUCT_MODEL_CODE = "productmodelcode";
	private static final String TEST_GENERATED_MEDIA_CODE = "pdh0001111";

	@InjectMocks
	private MediaPDHPopulator mediaPDHPopulator;

	@Mock
	private ModelService modelServiceMock;

	@Mock
	private KeyGenerator pdhMediaCodeGeneratorMock;

	@Mock
	private MediaSwgDTOPDHConverter mediaSwgDTOPDHConverterMock;

	@Mock
	private ProductSwgDTO productMock;

	@Mock
	private ProductModel productModelMock;

	@Mock
	private CatalogVersionModel catalogVersionMock;

	@Mock
	private MediaSwgDTO mediaMock;

	@Mock
	private MediaModel mediaModelMock;

	@Before
	public void setup()
	{
		when(pdhMediaCodeGeneratorMock.generate()).thenReturn(TEST_GENERATED_MEDIA_CODE);
	}

	@Test
	public void shouldSkipPopulatingDueToDTOPictureIsNull()
	{
		//Given
		when(productMock.getPicture()).thenReturn(null);

		//When
		mediaPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock, never()).setPicture(any());
		verify(modelServiceMock, never()).remove(any(MediaModel.class));
	}

	@Test
	public void shouldNotRemoveOldPictureDueToNullReference()
	{
		//Given
		when(productMock.getPicture()).thenReturn(mediaMock);
		when(productModelMock.getPicture()).thenReturn(null);

		//When
		mediaPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(modelServiceMock, never()).remove(any(MediaModel.class));
	}

	@Test
	public void shouldRemoveOldPicture()
	{
		//Given
		when(productMock.getPicture()).thenReturn(mediaMock);
		when(productModelMock.getPicture()).thenReturn(mediaModelMock);

		//When
		mediaPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(modelServiceMock).remove(eq(mediaModelMock));
	}

	@Test
	public void shouldPopulateProductWithMediaModel()
	{
		//Given
		when(productMock.getPicture()).thenReturn(mediaMock);
		when(productModelMock.getCode()).thenReturn(TEST_PRODUCT_MODEL_CODE);
		when(productModelMock.getCatalogVersion()).thenReturn(catalogVersionMock);
		when(mediaSwgDTOPDHConverterMock.convertToMediaModel(mediaMock, TEST_GENERATED_MEDIA_CODE, catalogVersionMock))
				.thenReturn(mediaModelMock);

		//When
		mediaPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock).setPicture(mediaModelMock);
	}

}
