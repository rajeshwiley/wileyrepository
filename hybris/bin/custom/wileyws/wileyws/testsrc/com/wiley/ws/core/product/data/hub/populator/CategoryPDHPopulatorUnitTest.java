package com.wiley.ws.core.product.data.hub.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.ws.core.common.dto.ShortStringSwgDTO;
import com.wiley.ws.core.product.data.hub.dto.ProductSwgDTO;


/**
 * Test for {@link CategoryPDHPopulator}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CategoryPDHPopulatorUnitTest
{

	private static final String TEST_CATEGORY_ID_1 = "category1";
	private static final String TEST_CATEGORY_ID_2 = "category2";

	@InjectMocks
	private CategoryPDHPopulator categoryPDHPopulator;

	@Mock
	private CategoryService categoryServiceMock;

	@Mock
	private ProductSwgDTO productMock;

	@Mock
	private ProductModel productModelMock;

	@Mock
	private CatalogVersionModel catalogVersionModelMock;


	@Test
	public void shouldSkipPopulatingDueToCategoriesAreNull()
	{
		//Given
		when(productMock.getCategories()).thenReturn(null);

		//When
		categoryPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock, never()).setSupercategories(any());
	}

	@Test
	public void shouldPopulateCategories()
	{
		//Given
		when(productModelMock.getCatalogVersion()).thenReturn(catalogVersionModelMock);
		List<ShortStringSwgDTO> categories = Arrays.asList(new ShortStringSwgDTO(TEST_CATEGORY_ID_1),
				new ShortStringSwgDTO(TEST_CATEGORY_ID_2));
		when(productMock.getCategories()).thenReturn(categories);
		List<CategoryModel> categoryModels = prepareServiceAndModels(categories);

		//When
		categoryPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock).setSupercategories(eq(categoryModels));
	}

	private List<CategoryModel> prepareServiceAndModels(final List<ShortStringSwgDTO> categories)
	{
		List<CategoryModel> categoryModels = new LinkedList<>();
		for (ShortStringSwgDTO category : categories)
		{
			CategoryModel categoryModel = mock(CategoryModel.class);
			when(categoryServiceMock.getCategoryForCode(catalogVersionModelMock, category.getValue())).thenReturn(categoryModel);
			categoryModels.add(categoryModel);
		}
		return categoryModels;
	}

}

