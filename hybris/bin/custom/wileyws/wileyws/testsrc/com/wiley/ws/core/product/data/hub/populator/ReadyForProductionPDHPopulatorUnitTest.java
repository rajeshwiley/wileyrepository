package com.wiley.ws.core.product.data.hub.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.core.model.product.ProductModel;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.wiley.ws.core.product.data.hub.dto.ProductSwgDTO;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Test for {@link ReadyForProductionPDHPopulator}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ReadyForProductionPDHPopulatorUnitTest
{
	@InjectMocks
	private ReadyForProductionPDHPopulator readyForProductionPDHPopulator;

	@Mock
	private ProductSwgDTO productMock;

	@Mock
	private ProductModel productModelMock;


	@Test
	public void shouldSetCheckStatusDueToDTOValueIsNull()
	{
		//Given
		when(productMock.getReadyForPublication()).thenReturn(null);

		//When
		readyForProductionPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock).setApprovalStatus(eq(ArticleApprovalStatus.CHECK));
	}

	@Test
	public void shouldSetCheckStatusDueToDTOValueIsFalse()
	{
		//Given
		when(productMock.getReadyForPublication()).thenReturn(Boolean.FALSE);

		//When
		readyForProductionPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock).setApprovalStatus(eq(ArticleApprovalStatus.CHECK));
	}

	@Test
	public void shouldSetApprovedStatus()
	{
		//Given
		when(productMock.getReadyForPublication()).thenReturn(Boolean.TRUE);

		//When
		readyForProductionPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock).setApprovalStatus(eq(ArticleApprovalStatus.APPROVED));
	}
}

