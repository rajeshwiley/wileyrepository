package com.wiley.ws.core.wileysubscription.populator.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.InvoicePaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.same;
import static org.mockito.Matchers.startsWith;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.wiley.core.model.WileySubscriptionModel;
import com.wiley.core.util.TestUtils;
import com.wiley.ws.core.wileysubscription.dto.PaymentInfoUpdateRequestWsDTO;
import com.wiley.ws.core.wileysubscription.dto.WileySubscriptionAddressWsDTO;


/**
 * Default unit test for {@link CreditCardPaymentInfoCreatingStrategy}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CreditCardPaymentInfoCreatingStrategyUnitTest
{

	@Mock
	private ModelService modelServiceMock;

	@Mock
	private Populator<WileySubscriptionAddressWsDTO, AddressModel> wileySubscriptionAddressWsReversePopulatorMock;

	@InjectMocks
	private CreditCardPaymentInfoCreatingStrategy creditCardPaymentInfoCreatingStrategy;

	// Test data

	@Mock
	private CreditCardPaymentInfoModel creditCardPaymentInfoModelMock;

	@Mock
	private WileySubscriptionModel wileySubscriptionModelMock;

	@Mock
	private CustomerModel customerModelMock;
	private static final String TEST_CUSTOMER_UID = "Test uid.123";

	@Mock
	private PaymentInfoUpdateRequestWsDTO paymentInfoUpdateRequestWsDTOMock;

	@Before
	public void setUp() throws Exception
	{
		when(wileySubscriptionModelMock.getCustomer()).thenReturn(customerModelMock);
		when(customerModelMock.getUid()).thenReturn(TEST_CUSTOMER_UID);
		when(creditCardPaymentInfoModelMock.getUser()).thenReturn(customerModelMock);
	}

	@Test
	public void testIsSupportedPositiveCase()
	{
		// Given
		// no changes in test data

		// When
		final boolean supported = creditCardPaymentInfoCreatingStrategy.isSupported(creditCardPaymentInfoModelMock);

		// Then
		assertTrue(supported);
	}

	@Test
	public void testIsSupportedNegativeCase()
	{
		// Given
		// no changes in test data

		// When
		final boolean supported = creditCardPaymentInfoCreatingStrategy.isSupported(mock(InvoicePaymentInfoModel.class));

		// Then
		assertFalse(supported);
	}

	@Test
	public void testIsSupportedWithNullParameter()
	{
		// Given
		// no changes in test data

		// When
		final boolean supported = creditCardPaymentInfoCreatingStrategy.isSupported(null);

		// Then
		assertFalse(supported);
	}

	@Test
	public void testGetOrCreateNewSupportedPaymentInfoPositiveCase()
	{
		// Given
		// no changes in test data

		// When
		final CreditCardPaymentInfoModel actualPaymentInfo =
				creditCardPaymentInfoCreatingStrategy.getOrCreateNewSupportedPaymentInfo(creditCardPaymentInfoModelMock,
						wileySubscriptionModelMock);

		// Then
		assertSame(creditCardPaymentInfoModelMock, actualPaymentInfo);
		verifyZeroInteractions(modelServiceMock, wileySubscriptionAddressWsReversePopulatorMock);
	}

	@Test
	public void testGetOrCreateNewSupportedPaymentInfoWhenPaymentInfoIsNotSupported()
	{
		// Given
		when(modelServiceMock.create(eq(CreditCardPaymentInfoModel.class))).thenReturn(creditCardPaymentInfoModelMock);
		final InvoicePaymentInfoModel invalidPaymentInfo = mock(InvoicePaymentInfoModel.class);

		// When
		final CreditCardPaymentInfoModel actualPaymentInfo =
				creditCardPaymentInfoCreatingStrategy.getOrCreateNewSupportedPaymentInfo(invalidPaymentInfo,
						wileySubscriptionModelMock);

		// Then
		assertSame(creditCardPaymentInfoModelMock, actualPaymentInfo);

		verify(modelServiceMock).create(eq(CreditCardPaymentInfoModel.class));
		verify(modelServiceMock).remove(eq(invalidPaymentInfo));
		verify(modelServiceMock, never()).save(eq(creditCardPaymentInfoModelMock));

		verify(creditCardPaymentInfoModelMock).setOwner(eq(wileySubscriptionModelMock));
		verify(creditCardPaymentInfoModelMock).setUser(eq(customerModelMock));
		verify(wileySubscriptionModelMock).setPaymentInfo(eq(creditCardPaymentInfoModelMock));

		verifyZeroInteractions(wileySubscriptionAddressWsReversePopulatorMock);
	}

	@Test
	public void testGetOrCreateNewSupportedPaymentInfoWhenParameterIsNull()
	{
		// Given
		when(modelServiceMock.create(eq(CreditCardPaymentInfoModel.class))).thenReturn(creditCardPaymentInfoModelMock);

		// When
		final CreditCardPaymentInfoModel actualPaymentInfo =
				creditCardPaymentInfoCreatingStrategy.getOrCreateNewSupportedPaymentInfo(null,
						wileySubscriptionModelMock);

		// Then
		assertSame(creditCardPaymentInfoModelMock, actualPaymentInfo);

		verify(modelServiceMock).create(eq(CreditCardPaymentInfoModel.class));
		verify(modelServiceMock, never()).remove(any(CreditCardPaymentInfoModel.class));
		verify(modelServiceMock, never()).save(eq(creditCardPaymentInfoModelMock));

		verify(creditCardPaymentInfoModelMock).setOwner(eq(wileySubscriptionModelMock));
		verify(creditCardPaymentInfoModelMock).setUser(eq(customerModelMock));
		verify(wileySubscriptionModelMock).setPaymentInfo(eq(creditCardPaymentInfoModelMock));

		verifyZeroInteractions(wileySubscriptionAddressWsReversePopulatorMock);
	}

	@Test
	public void testGetPaymentInfoClass()
	{
		// Given
		// no changes in test data

		// When
		final Class<CreditCardPaymentInfoModel> paymentInfoClass = creditCardPaymentInfoCreatingStrategy.getPaymentInfoClass();

		// Then
		assertNotNull(paymentInfoClass);
		assertTrue(CreditCardPaymentInfoModel.class.isAssignableFrom(paymentInfoClass));
	}

	@Test
	public void testPopulateWithNonNullFields()
	{
		// Given
		final AddressModel addressModelMock = mock(AddressModel.class);
		when(modelServiceMock.create(AddressModel.class)).thenReturn(addressModelMock);

		final WileySubscriptionAddressWsDTO addressWsDTOMock = mock(WileySubscriptionAddressWsDTO.class);

		final PaymentInfoUpdateRequestWsDTO.CreditCardType creditCardType = PaymentInfoUpdateRequestWsDTO.CreditCardType.VISA;
		final int validToMonth = 10;
		final int validToYear = 2020;
		final String paymentToken = "2342423424234";
		final PaymentInfoUpdateRequestWsDTO.PaymentType paymentType = PaymentInfoUpdateRequestWsDTO.PaymentType.PAYPAL;

		when(paymentInfoUpdateRequestWsDTOMock.getCreditCardType()).thenReturn(creditCardType);
		when(paymentInfoUpdateRequestWsDTOMock.getCreditCardValidToMonth()).thenReturn(validToMonth);
		when(paymentInfoUpdateRequestWsDTOMock.getCreditCardValidToYear()).thenReturn(validToYear);
		when(paymentInfoUpdateRequestWsDTOMock.getPaymentToken()).thenReturn(paymentToken);
		when(paymentInfoUpdateRequestWsDTOMock.getPaymentType()).thenReturn(paymentType);
		when(paymentInfoUpdateRequestWsDTOMock.getPaymentAddress()).thenReturn(addressWsDTOMock);

		final String firstName = "SomeFirstName";
		final String lastName = "SomeLastName";

		when(addressWsDTOMock.getFirstName()).thenReturn(firstName);
		when(addressWsDTOMock.getLastName()).thenReturn(lastName);

		// When
		creditCardPaymentInfoCreatingStrategy.populate(paymentInfoUpdateRequestWsDTOMock, creditCardPaymentInfoModelMock);

		// Then
		verify(modelServiceMock).create(eq(AddressModel.class));

		verify(creditCardPaymentInfoModelMock).setCode(startsWith(TEST_CUSTOMER_UID));
		verify(creditCardPaymentInfoModelMock).setCcOwner(eq(firstName + " " + lastName));
		verify(creditCardPaymentInfoModelMock).setNumber(eq(paymentToken));
		verify(creditCardPaymentInfoModelMock).setType(eq(CreditCardType.VISA));
		verify(creditCardPaymentInfoModelMock).setValidToYear(eq(String.valueOf(validToYear)));
		verify(creditCardPaymentInfoModelMock).setValidToMonth(eq(String.valueOf(validToMonth)));

		verify(wileySubscriptionAddressWsReversePopulatorMock).populate(same(addressWsDTOMock), same(addressModelMock));
		verify(creditCardPaymentInfoModelMock).setBillingAddress(same(addressModelMock));

		verify(addressModelMock).setOwner(creditCardPaymentInfoModelMock);
	}

	@Test
	public void testPopulateWithNullFields()
	{
		// Given
		final PaymentInfoUpdateRequestWsDTO paymentInfoUpdateRequestWsDTOMock = mock(PaymentInfoUpdateRequestWsDTO.class,
				(Answer) invocation -> null);

		// When
		creditCardPaymentInfoCreatingStrategy.populate(paymentInfoUpdateRequestWsDTOMock, creditCardPaymentInfoModelMock);

		// Then
		verifyZeroInteractions(modelServiceMock, wileySubscriptionAddressWsReversePopulatorMock);
		verify(creditCardPaymentInfoModelMock).setCode(startsWith(TEST_CUSTOMER_UID));
		verify(creditCardPaymentInfoModelMock, never()).setCcOwner(any());
		verify(creditCardPaymentInfoModelMock, never()).setNumber(any());
		verify(creditCardPaymentInfoModelMock, never()).setValidToMonth(any());
		verify(creditCardPaymentInfoModelMock, never()).setValidToYear(any());
		verify(creditCardPaymentInfoModelMock, never()).setType(any());
		verify(creditCardPaymentInfoModelMock, never()).setBillingAddress(any());
	}

	@Test
	public void testPopulateWhenRequiredFieldsAlreadyExists()
	{
		// Given
		when(creditCardPaymentInfoModelMock.getCode()).thenReturn("PAYPAL PAYMENT");

		// When
		creditCardPaymentInfoCreatingStrategy.populate(paymentInfoUpdateRequestWsDTOMock, creditCardPaymentInfoModelMock);

		// Then
		verifyZeroInteractions(modelServiceMock, wileySubscriptionAddressWsReversePopulatorMock);
		verify(creditCardPaymentInfoModelMock, never()).setCode(any());
	}

	@Test
	public void testPopulateWithIllegalStateForFirstAndLastName()
	{
		checkIllegalStateWithFirstAndLastName(null, null);
		checkIllegalStateWithFirstAndLastName("SomeFirstName", null);
		checkIllegalStateWithFirstAndLastName(null, "SomeLastName");
		checkIllegalStateWithFirstAndLastName("", "");
	}

	private void checkIllegalStateWithFirstAndLastName(final String firstName, final String lastName)
	{
		// Given
		final AddressModel addressModelMock = mock(AddressModel.class);
		when(modelServiceMock.create(AddressModel.class)).thenReturn(addressModelMock);

		WileySubscriptionAddressWsDTO addressWsDTOMock = mock(WileySubscriptionAddressWsDTO.class);
		when(addressWsDTOMock.getFirstName()).thenReturn(firstName);
		when(addressWsDTOMock.getLastName()).thenReturn(lastName);
		when(paymentInfoUpdateRequestWsDTOMock.getPaymentAddress()).thenReturn(addressWsDTOMock);

		// When
		TestUtils.expectException(IllegalStateException.class, () -> creditCardPaymentInfoCreatingStrategy
				.populate(paymentInfoUpdateRequestWsDTOMock, creditCardPaymentInfoModelMock));

		// Then
		verify(creditCardPaymentInfoModelMock, never()).setCcOwner(any());
	}

}