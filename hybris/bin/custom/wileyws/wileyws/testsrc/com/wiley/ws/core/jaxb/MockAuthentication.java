package com.wiley.ws.core.jaxb;

import org.springframework.security.authentication.AbstractAuthenticationToken;


/**
 * The type Mock authentication.
 */
public class MockAuthentication extends AbstractAuthenticationToken
{
	private final String principal;

	/**
	 * Instantiates a new Mock authentication.
	 *
	 * @param name
	 * 		the name
	 * @param authenticated
	 * 		the authenticated
	 */
	public MockAuthentication(final String name, final boolean authenticated)
	{
		super(null);
		setAuthenticated(authenticated);
		this.principal = name;
	}

	/**
	 * Gets credentials.
	 *
	 * @return the credentials
	 */
	@Override
	public Object getCredentials()
	{
		return null;
	}

	/**
	 * Gets principal.
	 *
	 * @return the principal
	 */
	@Override
	public Object getPrincipal()
	{
		return this.principal;
	}
}
