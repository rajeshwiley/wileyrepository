package com.wiley.ws.core.product.data.hub.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.enumeration.EnumerationService;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.enums.ProductEditionFormat;
import com.wiley.core.enums.WileyAssortment;
import com.wiley.core.enums.WileyDigitalContentTypeEnum;
import com.wiley.core.enums.WileyProductSubtypeEnum;
import com.wiley.ws.core.common.dto.ShortStringSwgDTO;
import com.wiley.ws.core.product.data.hub.dto.ProductSwgDTO;


/**
 * Test for {@link BasicPDHPopulator} of enumeration fields populating
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class BasicPDHPopulatorEnumerationUnitTest
{
	private static final String TEST_ASSORTMENT = "B2CASSORTMENT";
	private static final ProductSwgDTO.SubtypeEnum TEST_SUBTYPE = ProductSwgDTO.SubtypeEnum.COURSE;
	private static final ProductSwgDTO.EditionFormatEnum TEST_EDITION_FORMAT = ProductSwgDTO.EditionFormatEnum.DIGITAL;
	private static final ProductSwgDTO.DigitalContentTypeEnum TEST_CONTENT_TYPE =
			ProductSwgDTO.DigitalContentTypeEnum.VITAL_SOURCE;


	@InjectMocks
	private BasicPDHPopulator basicPDHPopulator;

	@Mock
	private ProductSwgDTO productMock;

	@Mock
	private ProductModel productModelMock;

	@Mock
	private EnumerationService enumerationServiceMock;


	@Test
	public void shouldSkipPopulatingAssortments()
	{
		//Given
		when(productMock.getAssortments()).thenReturn(null);

		//When
		basicPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock, never()).setAssortments(any());
	}

	@Test
	public void shouldPopulateAssortments()
	{
		//Given
		List<ShortStringSwgDTO> assortments = Collections.singletonList(new ShortStringSwgDTO(TEST_ASSORTMENT));
		when(productMock.getAssortments()).thenReturn(assortments);
		when(enumerationServiceMock.getEnumerationValue(WileyAssortment.class, TEST_ASSORTMENT)).thenReturn(
				WileyAssortment.B2CASSORTMENT);

		//When
		basicPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock).setAssortments(Collections.singletonList(WileyAssortment.B2CASSORTMENT));
	}

	@Test
	public void shouldSkipPopulatingSubtype()
	{
		//Given
		when(productMock.getSubtype()).thenReturn(null);

		//When
		basicPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock, never()).setSubtype(any());
	}

	@Test
	public void shouldPopulateSubtype()
	{
		//Given
		when(productMock.getSubtype()).thenReturn(TEST_SUBTYPE);
		when(enumerationServiceMock.getEnumerationValue(WileyProductSubtypeEnum.class, TEST_SUBTYPE.name())).thenReturn(
				WileyProductSubtypeEnum.COURSE);


		//When
		basicPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock).setSubtype(WileyProductSubtypeEnum.COURSE);
	}

	@Test
	public void shouldSkipPopulatingEditionFormat()
	{
		//Given
		when(productMock.getEditionFormat()).thenReturn(null);

		//When
		basicPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock, never()).setEditionFormat(any());
	}

	@Test
	public void shouldPopulateEditionFormat()
	{
		//Given
		when(productMock.getEditionFormat()).thenReturn(TEST_EDITION_FORMAT);
		when(enumerationServiceMock.getEnumerationValue(ProductEditionFormat.class, TEST_EDITION_FORMAT.name())).thenReturn(
				ProductEditionFormat.DIGITAL);


		//When
		basicPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock).setEditionFormat(ProductEditionFormat.DIGITAL);
	}


	@Test
	public void shouldSkipPopulatingDigitalContentType()
	{
		//Given
		when(productMock.getDigitalContentType()).thenReturn(null);

		//When
		basicPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock, never()).setDigitalContentType(any());
	}

	@Test
	public void shouldPopulateDigitalContentType()
	{
		//Given
		when(productMock.getDigitalContentType()).thenReturn(TEST_CONTENT_TYPE);
		when(enumerationServiceMock.getEnumerationValue(WileyDigitalContentTypeEnum.class, TEST_CONTENT_TYPE.name())).thenReturn(
				WileyDigitalContentTypeEnum.VITAL_SOURCE);

		//When
		basicPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock).setDigitalContentType(WileyDigitalContentTypeEnum.VITAL_SOURCE);
	}

	// TODO : [ECSC-20075] commented out code which uses obsolete attribute. Should be deleted
/*
	@Test
	public void shouldSkipPopulatingPrintIssn()
	{
		//Given
		when(productMock.getPrintIssn()).thenReturn(null);

		//When
		basicPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock, never()).setPrintIssn(any());
	}
*/

}

