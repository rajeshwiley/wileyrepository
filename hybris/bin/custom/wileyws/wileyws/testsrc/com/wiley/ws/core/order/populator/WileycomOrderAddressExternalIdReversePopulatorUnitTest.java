package com.wiley.ws.core.order.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.user.AddressModel;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.ws.core.order.dto.OrderAddressWsDTO;


/**
 * Default unit test for {@link WileycomOrderAddressExternalIdReversePopulator}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileycomOrderAddressExternalIdReversePopulatorUnitTest
{

	@InjectMocks
	private WileycomOrderAddressExternalIdReversePopulator wileycomOrderAddressExternalIdReversePopulator;

	// Test data

	@Mock
	private OrderAddressWsDTO orderAddressWsDTOMock;

	@Mock
	private AddressModel addressModelMock;

	@Before
	public void setUp() throws Exception
	{
		wileycomOrderAddressExternalIdReversePopulator = new WileycomOrderAddressExternalIdReversePopulator();
	}

	@Test
	public void testPopulatesWithNotNullFields()
	{
		// Given
		String addressId = "SomeAddressId";
		when(orderAddressWsDTOMock.getAddressId()).thenReturn(addressId);

		// When
		wileycomOrderAddressExternalIdReversePopulator.populate(orderAddressWsDTOMock, addressModelMock);

		// Then
		verify(addressModelMock).setExternalId(eq(addressId));
	}

	@Test
	public void testPopulatesWithNullFields()
	{
		// Given
		when(orderAddressWsDTOMock.getAddressId()).thenReturn(null);

		// When
		wileycomOrderAddressExternalIdReversePopulator.populate(orderAddressWsDTOMock, addressModelMock);

		// Then
		verify(addressModelMock, never()).setExternalId(anyString());
	}

	@Test
	public void testPopulatesWithIllegalParameters()
	{
		internalTestIllegalParameters(null, null);

		internalTestIllegalParameters(orderAddressWsDTOMock, null);
		verifyZeroInteractions(orderAddressWsDTOMock);

		internalTestIllegalParameters(null, addressModelMock);
		verifyZeroInteractions(addressModelMock);
	}

	private void internalTestIllegalParameters(final OrderAddressWsDTO orderAddressWsDTO, final AddressModel addressModel)
	{
		// When
		try
		{
			wileycomOrderAddressExternalIdReversePopulator.populate(orderAddressWsDTO, addressModel);
			fail("Expected " + IllegalArgumentException.class);
		}
		catch (IllegalArgumentException e)
		{
			// Success
		}
	}
}