package com.wiley.ws.core.product.dataintegration.productrelation.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyPurchaseOptionProductModel;
import com.wiley.ws.core.product.dataintegration.common.exception.DataImportValidationErrorsException;
import com.wiley.ws.core.product.dataintegration.common.service.ProductImportService;
import com.wiley.ws.core.product.dataintegration.productrelation.dto.ProductRelationSwgDTO;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ProductReferenceImportPopulatorTest
{

	private static final String VALID_REFERENCE_TYPE = "ACCESSORIES";
	private static final String INVALID_REFERENCE_TYPE = "incorrectTypeString";

	@Rule
	public ExpectedException exception = ExpectedException.none();

	@Mock
	private ProductImportService productImportService;
	@Mock
	private EnumerationService enumerationService;

	private ProductReferenceImportPopulator testInstance;

	@Before
	public void setUp()
	{
		when(enumerationService.getEnumerationValue(ProductReferenceTypeEnum.class, VALID_REFERENCE_TYPE))
				.thenReturn(ProductReferenceTypeEnum.valueOf(VALID_REFERENCE_TYPE));
		when(enumerationService.getEnumerationValue(ProductReferenceTypeEnum.class, INVALID_REFERENCE_TYPE))
				.thenThrow(UnknownIdentifierException.class);

		testInstance = new ProductReferenceImportPopulator(productImportService, enumerationService);
	}

	@Test
	public void shouldThrowExceptionWhenTargetProductIdIsBlank()
	{
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage("targetProductId can't be blank string");

		testInstance.populate(givenSource(), givenTarget());
	}

	@Test
	public void shouldThrowExceptionWhenSourceIsNull()
	{
		exception.expect(NullPointerException.class);
		exception.expectMessage("Source can't be null");

		testInstance.populate(null, givenTarget());
	}

	@Test
	public void shouldThrowExceptionWhenTargetIsNull()
	{
		exception.expect(NullPointerException.class);
		exception.expectMessage("Target can't be null");

		testInstance.populate(givenSource(), null);
	}

	@Test
	public void shouldThrowExceptionWhenIncorrectTypeString()
	{
		final ProductRelationSwgDTO productDto = givenSource(INVALID_REFERENCE_TYPE, "id");

		exception.expect(DataImportValidationErrorsException.class);

		testInstance.populate(productDto, givenTarget());
	}

	@Test
	public void shouldPopulateTargetWithCorrectData()
	{
		final ProductRelationSwgDTO source = givenSource(VALID_REFERENCE_TYPE, "id");
		final ProductReferenceModel target = givenTarget();
		final WileyPurchaseOptionProductModel purchaseOptionProduct = givenPurchaseOptionProduct();

		when(productImportService.getProductForCode(source.getTargetProductId()))
				.thenReturn(purchaseOptionProduct);

		testInstance.populate(source, target);
		assertEquals(purchaseOptionProduct, target.getTarget());
		assertEquals(ProductReferenceTypeEnum.ACCESSORIES, target.getReferenceType());
	}

	private ProductRelationSwgDTO givenSource()
	{
		return new ProductRelationSwgDTO();
	}

	private ProductRelationSwgDTO givenSource(final String type, final String targetProductId)
	{
		final ProductRelationSwgDTO source = givenSource();
		source.setType(type);
		source.setTargetProductId(targetProductId);
		return source;
	}

	private ProductReferenceModel givenTarget()
	{
		return new ProductReferenceModel();
	}

	private WileyPurchaseOptionProductModel givenPurchaseOptionProduct()
	{
		return new WileyPurchaseOptionProductModel();
	}
}
