package com.wiley.ws.core.product.data.hub.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyProductModel;
import com.wiley.ws.core.product.data.hub.converter.ProductReferenceSwgDTOPDHConverter;
import com.wiley.ws.core.product.data.hub.dto.ProductReferenceSwgDTO;
import com.wiley.ws.core.product.data.hub.dto.ProductSwgDTO;


/**
 * Test for {@link ProductReferencePDHPopulator}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ProductReferencePDHPopulatorUnitTest
{

	private static final String TEST_PRODUCT_MODEL_CODE = "productmodelcode";

	@InjectMocks
	private ProductReferencePDHPopulator productReferencePDHPopulator;

	@Mock
	private CommonI18NService commonI18NServiceMock;

	@Mock
	private ProductReferenceSwgDTOPDHConverter productReferenceSwgDTOPDHConverterMock;

	@Mock
	private ModelService modelServiceMock;

	@Mock
	private ProductSwgDTO productMock;

	@Mock
	private WileyProductModel wileyProductModelMock;

	@Mock
	private CatalogVersionModel catalogVersionMock;

	@Test
	public void shouldSkipPopulatingDueToListIsNull()
	{
		//Given
		when(productMock.getProductReferences()).thenReturn(null);

		//When
		productReferencePDHPopulator.populate(productMock, wileyProductModelMock);

		//Then
		verify(wileyProductModelMock, never()).setProductReferences(any());
		verify(modelServiceMock, never()).removeAll(any(Collection.class));
	}


	@Test
	public void shouldRemoveOldProductReferenceModels()
	{
		//Given
		when(productMock.getProductReferences()).thenReturn(Collections.emptyList());
		List<ProductReferenceModel> mockReferences = Collections.singletonList(new ProductReferenceModel());
		when(wileyProductModelMock.getProductReferences()).thenReturn(mockReferences);

		//When
		productReferencePDHPopulator.populate(productMock, wileyProductModelMock);

		//Then
		verify(modelServiceMock).removeAll(eq(mockReferences));
	}

	@Test
	public void shouldNotExecuteModelServiceToRemoveEmptyReferenceList()
	{
		//Given
		when(productMock.getProductReferences()).thenReturn(Collections.emptyList());
		when(wileyProductModelMock.getProductReferences()).thenReturn(Collections.emptyList());

		//When
		productReferencePDHPopulator.populate(productMock, wileyProductModelMock);

		//Then
		verify(modelServiceMock, never()).removeAll(any(Collection.class));
	}

	@Test
	public void shouldNotExecuteModelServiceToRemoveNullReferenceList()
	{
		//Given
		when(productMock.getProductReferences()).thenReturn(Collections.emptyList());
		when(wileyProductModelMock.getProductReferences()).thenReturn(null);

		//When
		productReferencePDHPopulator.populate(productMock, wileyProductModelMock);

		//Then
		verify(modelServiceMock, never()).removeAll(any(Collection.class));
	}


	@Test
	public void shouldSetConvertedReferencesToProduct()
	{
		ProductReferenceSwgDTO productReference = mock(ProductReferenceSwgDTO.class);
		ProductReferenceModel productReferenceModelMock = mock(ProductReferenceModel.class);
		when(productReferenceSwgDTOPDHConverterMock.convertToProductRefereceModel(productReference, wileyProductModelMock))
				.thenReturn(productReferenceModelMock);

		when(productMock.getProductReferences()).thenReturn(Collections.singletonList(productReference));

		//When
		productReferencePDHPopulator.populate(productMock, wileyProductModelMock);

		//Then
		verify(wileyProductModelMock).setProductReferences(Collections.singletonList(productReferenceModelMock));
	}

}
