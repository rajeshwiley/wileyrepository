package com.wiley.ws.core.product.data.hub.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.subscriptionservices.model.SubscriptionTermModel;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyProductModel;
import com.wiley.core.product.WileySubscriptionTermService;
import com.wiley.ws.core.common.dto.ShortStringSwgDTO;
import com.wiley.ws.core.product.data.hub.dto.ProductSwgDTO;


/**
 * Test for {@link SubscriptionTermPDHPopulator}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SubscriptionTermPDHPopulatorUnitTest
{
	public static final String TEST_SUBSCRIPTION_TERM_ID = "termid";

	@InjectMocks
	private SubscriptionTermPDHPopulator subscriptionTermPDHPopulator;

	@Mock
	private WileySubscriptionTermService wileySubscriptionTermServiceMock;

	@Mock
	private ProductSwgDTO productMock;

	@Mock
	private WileyProductModel productModelMock;

	@Mock
	private SubscriptionTermModel subscriptionTermMock;

	@Test
	public void shouldSkipPopulatingDueToSubscriptionTermsListIsNull()
	{
		//Given
		when(productMock.getSubscriptionTerms()).thenReturn(null);

		//When
		subscriptionTermPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock, never()).setSubscriptionTerms(any());
	}

	@Test
	public void shouldSetNullDueToSubscriptionTermsListIsEmptyList()
	{
		//Given
		when(productMock.getSubscriptionTerms()).thenReturn(Collections.EMPTY_LIST);

		//When
		subscriptionTermPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock).setSubscriptionTerms(isNull(Set.class));
	}


	@Test
	public void shouldSetSubscriptionTerm()
	{
		//Given
		List<ShortStringSwgDTO> subscriptionTerms = Collections.singletonList(new ShortStringSwgDTO(TEST_SUBSCRIPTION_TERM_ID));
		when(productMock.getSubscriptionTerms()).thenReturn(subscriptionTerms);
		when(wileySubscriptionTermServiceMock.getSubscriptionTerm(TEST_SUBSCRIPTION_TERM_ID)).thenReturn(
				subscriptionTermMock);
		Set<SubscriptionTermModel> set = new HashSet<>();
		set.add(subscriptionTermMock);

		//When
		subscriptionTermPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock).setSubscriptionTerms(set);
	}

}
