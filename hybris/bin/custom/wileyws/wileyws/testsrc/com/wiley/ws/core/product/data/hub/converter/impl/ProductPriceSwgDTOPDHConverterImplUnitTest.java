package com.wiley.ws.core.product.data.hub.converter.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.product.UnitService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.subscriptionservices.model.SubscriptionTermModel;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.product.WileySubscriptionTermService;
import com.wiley.ws.core.product.data.hub.dto.ProductPriceSwgDTO;


/**
 * Test for {@link ProductPriceSwgDTOPDHConverterImpl}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ProductPriceSwgDTOPDHConverterImplUnitTest
{

	private static final String TEST_PRODUCT_CODE = "productcode";

	private static final Double TEST_PRICE = 1.0D;
	private static final Long TEST_PRICE_MIN_QUANTITY = 1L;
	private static final String TEST_CURRENCY_USD = "USD";
	private static final String TEST_COUNTRY_US = "USD";
	private static final String TEST_COUNTRY_WHITESPACED = "  ";
	private static final String TEST_SUBSCRIPTION_TERM_ID = "termid";
	private static final String TEST_SUBSCRIPTION_TERM_ID_WHITESPACED = "  ";
	private static final String DEFAULT_UNIT = "pieces";
	private static final Boolean DEFAULT_NET = true;

	@InjectMocks
	private ProductPriceSwgDTOPDHConverterImpl productPriceSwgDTOPDHConverter;

	@Mock
	private ModelService modelServiceMock;

	@Mock
	private CommonI18NService commonI18NServiceMock;

	@Mock
	private WileySubscriptionTermService wileySubscriptionTermServiceMock;

	@Mock
	private UnitService unitServiceMock;

	@Mock(answer = Answers.RETURNS_DEEP_STUBS)
	private ProductPriceSwgDTO priceMock;

	@Mock
	private ProductModel productModelMock;

	@Mock
	private PriceRowModel priceRowModelMock;

	@Before
	public void setup()
	{
		when(modelServiceMock.create(any(Class.class))).thenAnswer(invocation ->
		{
			return mock((Class<PriceRowModel>) invocation.getArguments()[0]);
		});
	}

	@Test
	public void shouldConvertAndPopulatePrice()
	{
		//Given
		when(priceMock.getPrice()).thenReturn(TEST_PRICE);

		//When
		PriceRowModel priceRowModel = productPriceSwgDTOPDHConverter.convertToPriceRowModel(priceMock, TEST_PRODUCT_CODE);

		//Then
		verify(priceRowModel).setPrice(TEST_PRICE);
	}

	@Test
	public void shouldConvertAndPopulateMinqtd()
	{
		//Given
		when(priceMock.getMinQuantity()).thenReturn(TEST_PRICE_MIN_QUANTITY);

		//When
		PriceRowModel priceRowModel = productPriceSwgDTOPDHConverter.convertToPriceRowModel(priceMock, TEST_PRODUCT_CODE);

		//Then
		verify(priceRowModel).setMinqtd(TEST_PRICE_MIN_QUANTITY);
	}

	@Test
	public void shouldConvertAndPopulateCurrency()
	{
		//Given
		CurrencyModel currencyModelMock = mock(CurrencyModel.class);
		when(commonI18NServiceMock.getCurrency(TEST_CURRENCY_USD)).thenReturn(currencyModelMock);
		when(priceMock.getCurrency().getValue()).thenReturn(TEST_CURRENCY_USD);

		//When
		PriceRowModel priceRowModel = productPriceSwgDTOPDHConverter.convertToPriceRowModel(priceMock, TEST_PRODUCT_CODE);

		//Then
		verify(priceRowModel).setCurrency(currencyModelMock);
	}

	@Test
	public void shouldConvertAndPopulateUnit()
	{
		//Given
		UnitModel unitModelMock = mock(UnitModel.class);
		when(unitServiceMock.getUnitForCode(DEFAULT_UNIT)).thenReturn(unitModelMock);

		//When
		PriceRowModel priceRowModel = productPriceSwgDTOPDHConverter.convertToPriceRowModel(priceMock, TEST_PRODUCT_CODE);

		//Then
		verify(priceRowModel).setUnit(unitModelMock);
	}

	@Test
	public void shouldConvertAndPopulateProduct()
	{
		//Given
		//no addition condition

		//When
		PriceRowModel priceRowModel = productPriceSwgDTOPDHConverter.convertToPriceRowModel(priceMock, TEST_PRODUCT_CODE);

		//Then
		verify(priceRowModel).setProductId(TEST_PRODUCT_CODE);
	}

	@Test
	public void shouldConvertAndPopulateNet()
	{
		//Given
		//no addition condition

		//When
		PriceRowModel priceRowModel = productPriceSwgDTOPDHConverter.convertToPriceRowModel(priceMock, TEST_PRODUCT_CODE);

		//Then
		verify(priceRowModel).setNet(DEFAULT_NET);
	}

	@Test
	public void shouldConvertAndNotPopulateCountryDueToDTOCountryIsNull()
	{
		//Given
		when(priceMock.getCountry()).thenReturn(null);

		//When
		PriceRowModel priceRowModel = productPriceSwgDTOPDHConverter.convertToPriceRowModel(priceMock, TEST_PRODUCT_CODE);

		//Then
		verify(priceRowModel, never()).setCountry(any());
	}

	@Test
	public void shouldConvertAndNotPopulateCountryDueToDTOCountryIsEmpty()
	{
		//Given
		when(priceMock.getCountry().getValue()).thenReturn(StringUtils.EMPTY);

		//When
		PriceRowModel priceRowModel = productPriceSwgDTOPDHConverter.convertToPriceRowModel(priceMock, TEST_PRODUCT_CODE);

		//Then
		verify(priceRowModel, never()).setCountry(any());
	}

	@Test
	public void shouldConvertAndNotPopulateCountryDueToDTOCountryIsWhitespaced()
	{
		//Given
		when(priceMock.getCountry().getValue()).thenReturn(TEST_COUNTRY_WHITESPACED);

		//When
		PriceRowModel priceRowModel = productPriceSwgDTOPDHConverter.convertToPriceRowModel(priceMock, TEST_PRODUCT_CODE);

		//Then
		verify(priceRowModel, never()).setCountry(any());
	}

	@Test
	public void shouldConvertAndPopulateCountry()
	{
		//Given
		CountryModel countryModelMock = mock(CountryModel.class);
		when(commonI18NServiceMock.getCountry(TEST_COUNTRY_US)).thenReturn(countryModelMock);
		when(priceMock.getCountry().getValue()).thenReturn(TEST_COUNTRY_US);

		//When
		PriceRowModel priceRowModel = productPriceSwgDTOPDHConverter.convertToPriceRowModel(priceMock, TEST_PRODUCT_CODE);

		//Then
		verify(priceRowModel).setCountry(countryModelMock);
	}

	@Test
	public void shouldConvertToPriceRowModelClazz()
	{
		//Given
		//no addition condition

		//When
		PriceRowModel priceRowModel = productPriceSwgDTOPDHConverter.convertToPriceRowModel(priceMock, TEST_PRODUCT_CODE);

		//Then
		assertEquals(PriceRowModel.class, priceRowModel.getClass().getSuperclass()); //superclass due to mocked object
	}


	@Test
	public void shouldConvertAndNotPopulateSubscriptionTermDueToNull()
	{
		//Given
		when(priceMock.getSubscriptionTerm()).thenReturn(null);

		//When
		PriceRowModel priceRowModel = productPriceSwgDTOPDHConverter.convertToPriceRowModel(priceMock, TEST_PRODUCT_CODE);

		//Then
		verify(priceRowModel, never()).setSubscriptionTerm(any());
	}

	@Test
	public void shouldConvertAndNotPopulateSubscriptionTermDueToEmpty()
	{
		//Given
		when(priceMock.getSubscriptionTerm()).thenReturn(StringUtils.EMPTY);

		//When
		PriceRowModel priceRowModel = productPriceSwgDTOPDHConverter.convertToPriceRowModel(priceMock, TEST_PRODUCT_CODE);

		//Then
		verify(priceRowModel, never()).setSubscriptionTerm(any());
	}

	@Test
	public void shouldConvertAndNotPopulateSubscriptionTermDueToWhitespaced()
	{
		//Given
		when(priceMock.getSubscriptionTerm()).thenReturn(TEST_SUBSCRIPTION_TERM_ID_WHITESPACED);

		//When
		PriceRowModel priceRowModel = productPriceSwgDTOPDHConverter.convertToPriceRowModel(priceMock, TEST_PRODUCT_CODE);

		//Then
		verify(priceRowModel, never()).setSubscriptionTerm(any());
	}

	@Test
	public void shouldConvertAndPopulateSubscriptionTerm()
	{
		//Given
		SubscriptionTermModel subscriptionTermModelMock = mock(SubscriptionTermModel.class);
		when(wileySubscriptionTermServiceMock.getSubscriptionTerm(TEST_SUBSCRIPTION_TERM_ID)).thenReturn(
				subscriptionTermModelMock);
		when(priceMock.getSubscriptionTerm()).thenReturn(TEST_SUBSCRIPTION_TERM_ID);

		//When
		PriceRowModel priceRowModel = productPriceSwgDTOPDHConverter.convertToPriceRowModel(priceMock, TEST_PRODUCT_CODE);

		//Then
		verify(priceRowModel).setSubscriptionTerm(subscriptionTermModelMock);
	}

}