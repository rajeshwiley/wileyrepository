package com.wiley.ws.core.product.crud.converter.populator.baseproduct;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Locale;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyProductModel;
import com.wiley.ws.core.product.dataintegration.common.dto.LocalizedLongStringSwgDTO;
import com.wiley.ws.core.product.crud.dto.WileyProductWsDto;


@RunWith(MockitoJUnitRunner.class)
public class ProductDtoDescriptionPopulatorTest
{

	private ProductDtoDescriptionPopulator testInstance = new ProductDtoDescriptionPopulator();

	@Test
	public void shouldPopulateAllLocalesForProductDescription() throws Exception
	{
		//Given
		final WileyProductWsDto sourceDto = givenProductDto(
				givenLocalizedString("en", "en_value"),
				givenLocalizedString("fr", "fr_value")
		);
		final WileyProductModel targetProduct = mock(WileyProductModel.class);
		//When
		testInstance.populate(sourceDto, targetProduct);
		//Then
		verify(targetProduct).setDescription(eq("en_value"), any(Locale.class));
		verify(targetProduct).setDescription(eq("fr_value"), any(Locale.class));
	}

	private WileyProductWsDto givenProductDto(final LocalizedLongStringSwgDTO... descriptions)
	{
		final WileyProductWsDto wileyProductWsDto = mock(WileyProductWsDto.class);
		when(wileyProductWsDto.getDescription()).thenReturn(Arrays.asList(descriptions));
		return wileyProductWsDto;
	}

	private LocalizedLongStringSwgDTO givenLocalizedString(final String locale, final String value)
	{
		final LocalizedLongStringSwgDTO localizedString = mock(LocalizedLongStringSwgDTO.class);
		when(localizedString.getLocale()).thenReturn(locale);
		when(localizedString.getValue()).thenReturn(value);
		return localizedString;
	}

}