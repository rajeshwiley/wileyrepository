package com.wiley.ws.core.cart.populator;


import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.i18n.WileyCommonI18NService;
import com.wiley.ws.core.cart.dto.CreateCartRequestWsDTO;
import com.wiley.integrations.exceptions.carts.UnknownCountryException;
import com.wiley.integrations.order.dto.AddressWsDTO;


/**
 * The type WileyasCartModelReversePopulator unit test.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyasCartModelReversePopulatorUnitTest
{
	private static final String COUNTRY_ISO = "COUNTRY ISO";
	private static final String COUNTRY_ISO_2 = "COUNTRY ISO 2";
	private static final String EXTERNAL_TEXT = "EXTERNAL TEXT";

	@Spy
	@InjectMocks
	private WileyasCartModelReversePopulator testInstance;

	@Mock
	private CommonI18NService commonI18NServiceMock;

	@Mock
	private WileyCommonI18NService wileyCommonI18NServiceMock;

	@Mock
	private Converter<AddressWsDTO, AddressModel> wileyasAddressModelReverseConverterMock;

	@Mock
	private CreateCartRequestWsDTO sourceMock;

	@Mock
	private AddressWsDTO sourceContactAddressMock;

	@Mock
	private AddressWsDTO sourcePaymentAddressMock;

	@Mock
	private CartModel targetMock;

	@Mock
	private AddressModel targetContactAddressMock;

	@Mock
	private AddressModel targetPaymentAddressMock;

	@Mock
	private CountryModel targetCountryMock;

	@Mock
	private CurrencyModel targetCurrencyMock;

	@Mock
	private CurrencyModel targetCurrencyMock2;

	@Before
	public void setUp()
	{
		when(commonI18NServiceMock.getCountry(COUNTRY_ISO)).thenReturn(targetCountryMock);
		when(wileyCommonI18NServiceMock.getDefaultCurrency(targetCountryMock)).thenReturn(targetCurrencyMock);
		when(wileyasAddressModelReverseConverterMock.convert(sourceContactAddressMock)).thenReturn(targetContactAddressMock);
		when(wileyasAddressModelReverseConverterMock.convert(sourcePaymentAddressMock)).thenReturn(targetPaymentAddressMock);

		when(sourceMock.getCountry()).thenReturn(COUNTRY_ISO);
		when(sourceMock.getContactAddress()).thenReturn(sourceContactAddressMock);
		when(sourceMock.getPaymentAddress()).thenReturn(sourcePaymentAddressMock);
		when(sourceMock.getExternalText()).thenReturn(EXTERNAL_TEXT);
	}

	@Test
	public void testPopulateFieldsWithDefaultCurrency()
	{
		// when
		testInstance.populate(sourceMock, targetMock);

		// then
		verify(targetMock).setCountry(targetCountryMock);
		verify(targetMock).setCurrency(targetCurrencyMock);
		verify(targetMock, never()).setCurrency(targetCurrencyMock2);

		verify(targetMock).setContactAddress(targetContactAddressMock);
		verify(targetContactAddressMock).setContactAddress(true);
		verify(targetContactAddressMock, never()).setBillingAddress(any());
		verify(targetContactAddressMock).setOwner(targetMock);
		verify(targetMock).setPaymentAddress(targetPaymentAddressMock);
		verify(targetPaymentAddressMock).setBillingAddress(true);
		verify(targetPaymentAddressMock, never()).setContactAddress(any());
		verify(targetPaymentAddressMock).setOwner(targetMock);
		verify(targetMock).setExternalText(EXTERNAL_TEXT);
	}

	@Test
	public void testPopulateFieldsWithCurrency()
	{
		// given
		when(sourceMock.getCurrency()).thenReturn(COUNTRY_ISO_2);
		when(commonI18NServiceMock.getCurrency(COUNTRY_ISO_2)).thenReturn(targetCurrencyMock2);

		// when
		testInstance.populate(sourceMock, targetMock);

		// then
		verify(targetMock).setCountry(targetCountryMock);
		verify(targetMock).setCurrency(targetCurrencyMock2);
		verify(targetMock, never()).setCurrency(targetCurrencyMock);

		verify(targetMock).setContactAddress(targetContactAddressMock);
		verify(targetContactAddressMock).setContactAddress(true);
		verify(targetContactAddressMock, never()).setBillingAddress(any());
		verify(targetContactAddressMock).setOwner(targetMock);
		verify(targetMock).setPaymentAddress(targetPaymentAddressMock);
		verify(targetPaymentAddressMock).setBillingAddress(true);
		verify(targetPaymentAddressMock, never()).setContactAddress(any());
		verify(targetPaymentAddressMock).setOwner(targetMock);
		verify(targetMock).setExternalText(EXTERNAL_TEXT);
	}

	@Test
	public void testNotPopulateContactAddressIfNull()
	{
		// given
		when(sourceMock.getContactAddress()).thenReturn(null);

		// when
		testInstance.populate(sourceMock, targetMock);

		// then
		verify(targetMock).setCountry(targetCountryMock);
		verify(targetMock).setCurrency(targetCurrencyMock);

		verify(targetMock, never()).setContactAddress(any());
		verify(targetContactAddressMock, never()).setOwner(any());

		verify(targetMock).setPaymentAddress(targetPaymentAddressMock);
		verify(targetPaymentAddressMock).setOwner(targetMock);
		verify(targetMock).setExternalText(EXTERNAL_TEXT);
	}

	@Test
	public void testNotPopulatePaymentAddressIfNull()
	{
		// given
		when(sourceMock.getPaymentAddress()).thenReturn(null);

		// when
		testInstance.populate(sourceMock, targetMock);

		// then
		verify(targetMock).setCountry(targetCountryMock);
		verify(targetMock).setCurrency(targetCurrencyMock);
		verify(targetMock).setContactAddress(targetContactAddressMock);
		verify(targetContactAddressMock).setOwner(targetMock);

		verify(targetMock, never()).setPaymentAddress(any());
		verify(targetPaymentAddressMock, never()).setOwner(any());

		verify(targetMock).setExternalText(EXTERNAL_TEXT);
	}

	@Test
	public void testNotPopulateInstitutionIfNull()
	{
		// given
		when(sourceMock.getExternalText()).thenReturn(null);

		// when
		testInstance.populate(sourceMock, targetMock);

		// then
		verify(targetMock).setCurrency(targetCurrencyMock);
		verify(targetMock).setContactAddress(targetContactAddressMock);
		verify(targetContactAddressMock).setOwner(targetMock);
		verify(targetMock).setPaymentAddress(targetPaymentAddressMock);
		verify(targetPaymentAddressMock).setOwner(targetMock);
		verify(targetMock, never()).setExternalText(any());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testPopulateWithNullSource()
	{
		testInstance.populate(null, targetMock);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testPopulateWithNullTarget()
	{
		testInstance.populate(sourceMock, null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testPopulateWithNullSourceAndTarget()
	{
		testInstance.populate(null, null);
	}

	@Test(expected = UnknownCountryException.class)
	public void testPopulateWithUnknownCountryException()
	{
		when(commonI18NServiceMock.getCountry(COUNTRY_ISO)).thenThrow(UnknownIdentifierException.class);
		testInstance.populate(sourceMock, targetMock);
	}
}
