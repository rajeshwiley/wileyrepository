package com.wiley.ws.core.product.crud.converter;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.classification.features.LocalizedFeature;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.validation.services.ValidationService;

import java.util.Arrays;
import java.util.Date;

import javax.annotation.Resource;

import org.apache.commons.lang.LocaleUtils;
import org.junit.Before;
import org.junit.Test;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.model.WileyPurchaseOptionProductModel;
import com.wiley.core.model.WileyWebLinkModel;
import com.wiley.core.product.WileyProductService;
import com.wiley.ws.core.product.crud.dto.LinkToExternalStoreWsDto;
import com.wiley.ws.core.product.crud.dto.LinkToExternalStoreWsDto.TypeEnum;
import com.wiley.ws.core.product.crud.dto.LocalizedLifecycleStatusWsDto;
import com.wiley.ws.core.product.crud.dto.LocalizedLifecycleStatusWsDto.StatusEnum;
import com.wiley.ws.core.product.crud.dto.WileyVariantProductWsDto;
import com.wiley.ws.core.product.crud.facade.WileyProductCrudFacadeImpl;
import com.wiley.ws.core.product.dataintegration.common.dto.LocalizedBooleanSwgDTO;
import com.wiley.ws.core.product.dataintegration.common.dto.LocalizedDateSwgDTO;
import com.wiley.ws.core.product.dataintegration.common.dto.LocalizedStringSwgDTO;
import com.wiley.ws.core.product.dataintegration.common.exception.DataImportValidationConstraintsException;
import com.wiley.ws.core.product.dataintegration.common.exception.DataImportValidationErrorsException;

import static java.util.Arrays.asList;
import static junit.framework.Assert.assertEquals;
import static org.apache.commons.lang.LocaleUtils.toLocale;


@IntegrationTest
public class WileyProductCrudFacadeImplIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{

	private static final String SYSTEM_ID = "WileyMediumClassificationCatalog";
	private static final String SYSTEM_VERSION = "1.0";

	private static final String PURCHASE_OPTION_BOOK = "book";
	private static final String PURCHASE_OPTION_JOURNAL = "journal";

	private static final String ISBN13_CODE = buildFeatureCode(PURCHASE_OPTION_BOOK, "isbn13");
	private static final String PUBLICATION_DATE_CODE = buildFeatureCode(PURCHASE_OPTION_BOOK, "publicationdate");
	private static final String NUMBER_OF_PAGES_CODE = buildFeatureCode(PURCHASE_OPTION_BOOK, "numberofpages");
	private static final String PRINT_ISSN_CODE = buildFeatureCode(PURCHASE_OPTION_JOURNAL, "printissn");
	private static final String ONLINE_ISSN_CODE = buildFeatureCode(PURCHASE_OPTION_JOURNAL, "onlineissn");
	private static final String VOLUME_AND_ISSUE_CODE = buildFeatureCode(PURCHASE_OPTION_JOURNAL, "volumeandissue");
	private static final String IMPACT_FACTOR_CODE = buildFeatureCode(PURCHASE_OPTION_JOURNAL, "impactfactor");
	private static final String SOCIETY_LINK_CODE = buildFeatureCode(PURCHASE_OPTION_JOURNAL, "societylink");

	private static final String LOCALE_EN = "en";
	private static final String PRODUCT_ID = "PRODUCT_CRUD_TEST_BASE";
	private static final String NEW_VARIANT_PRODUCT_ID = "NEW_PRODUCT_CRUD_TEST_VARIANT";
	private static final String EXISTED_VARIANT_PRODUCT_ID = "PRODUCT_CRUD_TEST_VARIANT";
	private static final String ISBN_10 = "0123456789";
	private static final String LIFE_CYCLE_STATUS = "ACTIVE";
	private static final String EDITION_FORMAT = "DIGITAL";
	private static final String COUNTRY_CN = "CN";
	private static final String COUNTRY_US = "US";
	private static final Boolean CUSTOM = true;
	private static final Boolean PRINT_ON_DEMAND = true;
	private static final String EXTERNAL_STORE_TYPE = "GOOGLE_PLAY_STORE";
	private static final String EXTERNAL_STORE_URL = "https://google.com";

	private static final String ISBN_13 = "0123456789012";
	private static final Date PUBLICATION_DATE = new Date();
	private static final String NUMBER_OF_PAGES = "23";

	private static final String PRINT_ISSN = "12345678";
	private static final String ONLINE_ISSN = "87654321";
	private static final String VOLUME_AND_ISSUE = "volumeAndIssue";
	private static final String SOCIETY_LINK = "societyLink";
	private static final Double IMPACT_FACTOR = 0.01;

	private static final String INVALID_ISBN_10 = "0123456789aaa";
	private static final String INVALID_COUNTRY = "USSR";
	private static final String INVALID_LOCALE = "xx_XX";



	@Resource
	private WileyProductCrudFacadeImpl wileyProductCrudFacade;

	@Resource
	private WileyProductService productService;

	@Resource
	private ClassificationService classificationService;

	@Resource
	private CommonI18NService commonI18NService;
	@Resource
	private I18NService i18NService;

	@Resource
	private ValidationService validationService;

	@Before
	public void setUp() throws ImpExException
	{
		importCsv("/wileyws/test/product-import.impex", DEFAULT_ENCODING);
		validationService.reloadValidationEngine();
		i18NService.setCurrentLocale(LocaleUtils.toLocale(LOCALE_EN));
	}

	@Test
	public void shouldCreateVariantProductWithAllBaseAttributes()
	{
		final String returnedVariantCode = wileyProductCrudFacade.createVariantProduct(PRODUCT_ID,
				givenBaseVariantProduct(NEW_VARIANT_PRODUCT_ID));
		assertEquals(NEW_VARIANT_PRODUCT_ID, returnedVariantCode);

		final WileyPurchaseOptionProductModel variantProductModel = (WileyPurchaseOptionProductModel)
				getProductForCode(NEW_VARIANT_PRODUCT_ID);
		verifyAllBaseAttributesSaved(variantProductModel, NEW_VARIANT_PRODUCT_ID);
	}

	@Test
	public void shouldUpdateVariantProductWithAllBaseAttributes()
	{
		wileyProductCrudFacade.updateVariantProduct(PRODUCT_ID, EXISTED_VARIANT_PRODUCT_ID,
				givenBaseVariantProduct(EXISTED_VARIANT_PRODUCT_ID));

		final WileyPurchaseOptionProductModel variantProductModel = (WileyPurchaseOptionProductModel) getProductForCode(
				EXISTED_VARIANT_PRODUCT_ID);

		verifyAllBaseAttributesSaved(variantProductModel, EXISTED_VARIANT_PRODUCT_ID);
	}

	@Test
	public void shouldCreateBookVariantProductWithAllClassificationAttributes()
	{
		wileyProductCrudFacade.createVariantProduct(PRODUCT_ID, givenBookVariantProduct(NEW_VARIANT_PRODUCT_ID));

		final ProductModel variantProductModel = getProductForCode(NEW_VARIANT_PRODUCT_ID);

		verifyAllBookClassificationAttributesSaved(variantProductModel);
	}

	@Test
	public void shouldUpdateBookVariantProductWithAllClassificationAttributes()
	{
		wileyProductCrudFacade.updateVariantProduct(PRODUCT_ID, EXISTED_VARIANT_PRODUCT_ID,
				givenBookVariantProduct(EXISTED_VARIANT_PRODUCT_ID));

		final ProductModel variantProductModel = getProductForCode(EXISTED_VARIANT_PRODUCT_ID);

		verifyAllBookClassificationAttributesSaved(variantProductModel);
	}

	@Test
	public void shouldCreateJournalVariantProductWithAllClassificationAttributes()
	{
		wileyProductCrudFacade.createVariantProduct(PRODUCT_ID, givenJournalVariantProduct(NEW_VARIANT_PRODUCT_ID));

		final ProductModel variantProductModel = getProductForCode(NEW_VARIANT_PRODUCT_ID);

		verifyAllJournalClassificationAttributesSaved(variantProductModel);
	}

	@Test
	public void shouldUpdateJournalVariantProductWithAllClassificationAttributes()
	{
		wileyProductCrudFacade.updateVariantProduct(PRODUCT_ID, EXISTED_VARIANT_PRODUCT_ID,
				givenJournalVariantProduct(EXISTED_VARIANT_PRODUCT_ID));

		final ProductModel variantProductModel = getProductForCode(EXISTED_VARIANT_PRODUCT_ID);

		verifyAllJournalClassificationAttributesSaved(variantProductModel);
	}

	@Test(expected = DataImportValidationConstraintsException.class)
	public void shouldThrowAnExceptionIfVariantProductIsNotValid()
	{
		final WileyVariantProductWsDto variantProduct = givenBaseVariantProduct(NEW_VARIANT_PRODUCT_ID);
		variantProduct.setIsbn10(INVALID_ISBN_10);

		wileyProductCrudFacade.createVariantProduct(PRODUCT_ID, variantProduct);
	}

	@Test(expected = DataImportValidationErrorsException.class)
	public void shouldThrowAnExceptionIfVariantIdIsNotUnique()
	{
		final WileyVariantProductWsDto variantProduct = givenBaseVariantProduct(EXISTED_VARIANT_PRODUCT_ID);
		wileyProductCrudFacade.createVariantProduct(PRODUCT_ID, variantProduct);
	}

	@Test(expected = DataImportValidationErrorsException.class)
	public void shouldThrowAnExceptionIfBadCountryReferenceIsUsed()
	{
		final WileyVariantProductWsDto variantProduct = givenBaseVariantProduct(NEW_VARIANT_PRODUCT_ID);
		variantProduct.setCountryRestrictions(Arrays.asList(INVALID_COUNTRY));
		wileyProductCrudFacade.createVariantProduct(PRODUCT_ID, variantProduct);
	}

	@Test(expected = DataImportValidationErrorsException.class)
	public void shouldThrowAnExceptionIfAttemptToChangeVariantId()
	{
		final WileyVariantProductWsDto variantProduct = givenBaseVariantProduct(NEW_VARIANT_PRODUCT_ID);
		wileyProductCrudFacade.updateVariantProduct(PRODUCT_ID, EXISTED_VARIANT_PRODUCT_ID, variantProduct);
	}

	private void verifyAllBaseAttributesSaved(final WileyPurchaseOptionProductModel variantProductModel,
			final String variantProductId)
	{
		assertEquals(variantProductId, variantProductModel.getCode());
		assertEquals(getProductForCode(PRODUCT_ID), variantProductModel.getBaseProduct());
		assertEquals(ISBN_10, variantProductModel.getIsbn());
		assertEquals(LIFE_CYCLE_STATUS, getLifecycleFromModel(variantProductModel));
		assertEquals(1, variantProductModel.getClassificationClasses().size());
		assertEquals(PURCHASE_OPTION_BOOK, getClassificationClassFromModel(variantProductModel));
		assertEquals(EDITION_FORMAT, variantProductModel.getEditionFormat().getCode());
		assertEquals(asList(getCountryForCode(COUNTRY_CN), getCountryForCode(COUNTRY_US)),
				variantProductModel.getCountryRestrictions());
		assertEquals(CUSTOM, variantProductModel.getCustom());
		assertEquals(PRINT_ON_DEMAND, variantProductModel.getPrintOnDemand(toLocale(LOCALE_EN)));

		final WileyWebLinkModel wileyWebLink = getWileyWebLinkFromModel(variantProductModel);
		assertEquals(EXTERNAL_STORE_TYPE, wileyWebLink.getType().toString());
		assertEquals(EXTERNAL_STORE_URL, wileyWebLink.getParameters().get(0));
	}

	private void verifyAllBookClassificationAttributesSaved(final ProductModel variantProductModel)
	{
		final FeatureList featureList = classificationService.getFeatures(variantProductModel);
		assertEquals(3, featureList.getFeatures().size());
		verifyFeatureAdded(ISBN_13, featureList, ISBN13_CODE);
		verifyLocalizedFeatureAdded(PUBLICATION_DATE, featureList, PUBLICATION_DATE_CODE, LOCALE_EN);
		verifyFeatureAdded(NUMBER_OF_PAGES, featureList, NUMBER_OF_PAGES_CODE);
	}

	private void verifyAllJournalClassificationAttributesSaved(final ProductModel variantProductModel)
	{
		final FeatureList featureList = classificationService.getFeatures(variantProductModel);
		assertEquals(5, featureList.getFeatures().size());
		verifyFeatureAdded(PRINT_ISSN, featureList, PRINT_ISSN_CODE);
		verifyFeatureAdded(ONLINE_ISSN, featureList, ONLINE_ISSN_CODE);
		verifyFeatureAdded(VOLUME_AND_ISSUE, featureList, VOLUME_AND_ISSUE_CODE);
		verifyLocalizedFeatureAdded(SOCIETY_LINK, featureList, SOCIETY_LINK_CODE, LOCALE_EN);
		verifyFeatureAdded(IMPACT_FACTOR, featureList, IMPACT_FACTOR_CODE);
	}

	private void verifyFeatureAdded(final Object expectedFeatureValue, final FeatureList featureList, final String featureCode)
	{
		assertEquals(expectedFeatureValue, featureList.getFeatureByCode(featureCode).getValue().getValue());
	}

	private void verifyLocalizedFeatureAdded(final Object expectedFeatureValue, final FeatureList featureList,
			final String featureCode, final String locale)
	{
		final LocalizedFeature localizedFeature = (LocalizedFeature) featureList.getFeatureByCode(featureCode);
		assertEquals(expectedFeatureValue, localizedFeature.getValue(toLocale(locale)).getValue());
	}

	private ProductModel getProductForCode(final String productId)
	{
		return productService.getProductForCode(productId);
	}

	private CountryModel getCountryForCode(final String countryCode)
	{
		return commonI18NService.getCountry(countryCode);
	}

	private WileyWebLinkModel getWileyWebLinkFromModel(final WileyPurchaseOptionProductModel variantProductModel)
	{
		return variantProductModel.getExternalStores().iterator().next();
	}

	private String getClassificationClassFromModel(final WileyPurchaseOptionProductModel variantProductModel)
	{
		return variantProductModel.getClassificationClasses().get(0).getCode();
	}

	private String getLifecycleFromModel(final WileyPurchaseOptionProductModel variantProduct)
	{
		return variantProduct.getLifecycleStatus(toLocale(LOCALE_EN)).toString();
	}

	private WileyVariantProductWsDto givenBaseVariantProduct(final String variantProductId)
	{
		WileyVariantProductWsDto variantProductDto = new WileyVariantProductWsDto();
		variantProductDto.setId(variantProductId);
		variantProductDto.setIsbn10(ISBN_10);
		variantProductDto.setLifecycleStatus(asList(givenLocalizedLifecycleStatus(LIFE_CYCLE_STATUS, LOCALE_EN)));
		variantProductDto.setPurchaseOption(PURCHASE_OPTION_BOOK);
		variantProductDto.setEditionFormat(EDITION_FORMAT);
		variantProductDto.setCountryRestrictions(asList(COUNTRY_CN, COUNTRY_US));
		variantProductDto.setCustom(CUSTOM);
		variantProductDto.setPrintOnDemand(asList(givenLocalizedBoolean(PRINT_ON_DEMAND, LOCALE_EN)));
		variantProductDto.setExternalStores(
				asList(givenLinkToExternalStore(EXTERNAL_STORE_TYPE, EXTERNAL_STORE_URL)));
		return variantProductDto;
	}

	private WileyVariantProductWsDto givenBookVariantProduct(final String variantProductId)
	{
		final WileyVariantProductWsDto variantProductDto = givenBaseVariantProduct(variantProductId);

		variantProductDto.setPurchaseOption(PURCHASE_OPTION_BOOK);
		variantProductDto.setIsbn13(ISBN_13);
		variantProductDto.setPublicationDate(asList(givenLocalizedDate(PUBLICATION_DATE, LOCALE_EN)));
		variantProductDto.setNumberOfPages(NUMBER_OF_PAGES);
		return variantProductDto;
	}

	private WileyVariantProductWsDto givenJournalVariantProduct(final String variantProductId)
	{
		final WileyVariantProductWsDto variantProductDto = givenBaseVariantProduct(variantProductId);

		variantProductDto.setPurchaseOption(PURCHASE_OPTION_JOURNAL);
		variantProductDto.setPrintIssn(PRINT_ISSN);
		variantProductDto.setOnlineIssn(ONLINE_ISSN);
		variantProductDto.setVolumeAndIssue(VOLUME_AND_ISSUE);
		variantProductDto.setSocietyLink(asList(givenLocalizedString(SOCIETY_LINK, LOCALE_EN)));
		variantProductDto.setImpactFactor(IMPACT_FACTOR);
		return variantProductDto;
	}

	private LocalizedBooleanSwgDTO givenLocalizedBoolean(final Boolean value, final String locale)
	{
		final LocalizedBooleanSwgDTO localizedBooleanSwgDTO = new LocalizedBooleanSwgDTO();
		localizedBooleanSwgDTO.setValue(value);
		localizedBooleanSwgDTO.setLocale(locale);
		return localizedBooleanSwgDTO;
	}

	private LocalizedDateSwgDTO givenLocalizedDate(final Date date, final String locale)
	{
		final LocalizedDateSwgDTO localizedDateSwgDTO = new LocalizedDateSwgDTO();
		localizedDateSwgDTO.setDatetime(date);
		localizedDateSwgDTO.setLocale(locale);
		return localizedDateSwgDTO;
	}

	private LocalizedStringSwgDTO givenLocalizedString(final String value, final String locale)
	{
		final LocalizedStringSwgDTO localizedString = new LocalizedStringSwgDTO();
		localizedString.setValue(value);
		localizedString.setLocale(locale);
		return localizedString;
	}

	private LocalizedLifecycleStatusWsDto givenLocalizedLifecycleStatus(final String status, final String locale)
	{
		final LocalizedLifecycleStatusWsDto lifecycleStatus = new LocalizedLifecycleStatusWsDto();
		lifecycleStatus.setStatus(StatusEnum.valueOf(status));
		lifecycleStatus.setLocale(locale);
		return lifecycleStatus;
	}

	private LinkToExternalStoreWsDto givenLinkToExternalStore(final String type, final String url)
	{
		final LinkToExternalStoreWsDto linkToExternalStoreWsDto = new LinkToExternalStoreWsDto();
		linkToExternalStoreWsDto.setType(TypeEnum.valueOf(type));
		linkToExternalStoreWsDto.setUrl(url);
		return linkToExternalStoreWsDto;
	}

	private static String buildFeatureCode(final String classificationClassCode, final String attributeName)
	{
		return SYSTEM_ID + "/" + SYSTEM_VERSION + "/" + classificationClassCode + "." + attributeName;
	}
}
