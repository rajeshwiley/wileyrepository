package com.wiley.ws.core.wileysubscription.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.converters.Populator;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileySubscriptionModel;
import com.wiley.core.util.TestUtils;
import com.wiley.ws.core.wileysubscription.dto.AbstractSubscriptionRequestWsDTO;
import com.wiley.ws.core.wileysubscription.dto.SubscriptionCreateResponseWsDTO;


/**
 * Default unit test for {@link WileySubscriptionCreateResponseWsPopulator}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileySubscriptionCreateResponseWsPopulatorUnitTest
{

	@InjectMocks
	private WileySubscriptionCreateResponseWsPopulator wileySubscriptionCreateResponseWsPopulator;

	// ----------- Mock for injection

	@Mock
	private Populator<AbstractSubscriptionRequestWsDTO, WileySubscriptionModel> wileySubscriptionWsReversePopulator;

	// ----------- Test data

	@Mock
	private WileySubscriptionModel wileySubscriptionModelMock;

	@Mock
	private SubscriptionCreateResponseWsDTO subscriptionCreateResponseWsDTOMock;

	@Test
	public void testPopulateSuccessCase()
	{
		// Given
		final String someSubscriptionCode = "someSubscriptionCode";
		when(wileySubscriptionModelMock.getCode()).thenReturn(someSubscriptionCode);

		// When
		wileySubscriptionCreateResponseWsPopulator.populate(wileySubscriptionModelMock, subscriptionCreateResponseWsDTOMock);

		// Then
		verify(subscriptionCreateResponseWsDTOMock).setInternalSubscriptionId(eq(someSubscriptionCode));
	}

	@Test
	public void testPopulateWithIllegalParameters()
	{
		// When
		TestUtils.expectException(IllegalArgumentException.class,
				() -> wileySubscriptionCreateResponseWsPopulator.populate(null, null));
		// Then
		verifyZeroInteractions(wileySubscriptionWsReversePopulator);

		// ----------------------------------------------

		// When
		TestUtils.expectException(IllegalArgumentException.class,
				() -> wileySubscriptionCreateResponseWsPopulator.populate(wileySubscriptionModelMock, null));
		// Then
		verifyZeroInteractions(wileySubscriptionWsReversePopulator, wileySubscriptionModelMock);

		// ----------------------------------------------

		// When
		TestUtils.expectException(IllegalArgumentException.class,
				() -> wileySubscriptionCreateResponseWsPopulator.populate(null, subscriptionCreateResponseWsDTOMock));
		// Then
		verifyZeroInteractions(wileySubscriptionWsReversePopulator, subscriptionCreateResponseWsDTOMock);
	}

}