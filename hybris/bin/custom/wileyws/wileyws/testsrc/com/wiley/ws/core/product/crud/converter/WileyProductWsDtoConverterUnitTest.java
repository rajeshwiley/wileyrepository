package com.wiley.ws.core.product.crud.converter;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.keygenerator.impl.PersistentKeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.enums.WileyAssortment;
import com.wiley.core.model.WileyProductModel;
import com.wiley.ws.core.product.crud.converter.baseproduct.DownloadableContentToMediaContainerConverter;
import com.wiley.ws.core.product.crud.converter.populator.baseproduct.WileyProductSearchKeywordsPopulator;
import com.wiley.ws.core.product.crud.dto.DownloadableContentWsDto;
import com.wiley.ws.core.product.crud.dto.WileyProductWsDto;
import com.wiley.ws.core.product.crud.facade.WileyProductCrudCatalogVersionProvider;
import com.wiley.ws.core.product.dataintegration.common.util.LocalizedPopulatorUtil;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyProductWsDtoConverterUnitTest
{

	private static final String MOCK_PRODUCT_CODE = "MOCK_PRODUCT_CODE";

	@InjectMocks
	private WileyProductWsDtoConverter testInstance;

	@Mock
	private ModelService modelService;
	@Mock
	private LocalizedPopulatorUtil localizedPopulatorUtil;
	@Mock
	private Populator<WileyProductWsDto, WileyProductModel> productWsDtoPopulators;
	@Mock
	private WileyProductSearchKeywordsPopulator wileyProductSearchKeywordsPopulator;
	@Mock
	private DownloadableContentToMediaContainerConverter downloadableContentToMediaContainerConverter;
	@Mock
	private PersistentKeyGenerator productCrudTransactionIdGenerator;
	@Mock
	private WileyProductCrudCatalogVersionProvider wileyProductCrudCatalogVersionProvider;
	@Mock
	private WebLinkConverter webLinkConverter;
	@Mock
	private CatalogVersionModel mockCatalogVersion;
	@Mock
	private WileyProductModel mockWileyProductModel;

	@Before
	public void setUp() throws Exception
	{
		testInstance = new WileyProductWsDtoConverter(modelService, localizedPopulatorUtil, productWsDtoPopulators,
				downloadableContentToMediaContainerConverter, productCrudTransactionIdGenerator,
				wileyProductCrudCatalogVersionProvider, webLinkConverter);

		when(wileyProductCrudCatalogVersionProvider.getCatalogVersion()).thenReturn(mockCatalogVersion);
		when(productCrudTransactionIdGenerator.generate()).thenReturn(MOCK_PRODUCT_CODE);
		when(modelService.create(WileyProductModel.class)).thenReturn(mockWileyProductModel);
	}

	@Test
	public void shouldCallProductWsDtoPopulators()
	{
		//Given
		WileyProductWsDto dto = mock(WileyProductWsDto.class);
		when(dto.getExcerpts()).thenReturn(Collections.singletonList(mock(DownloadableContentWsDto.class)));

		WileyProductModel wileyProductModel = mock(WileyProductModel.class);
		//When
		testInstance.convert(dto, wileyProductModel);
		//Then
		verify(productWsDtoPopulators).populate(dto, wileyProductModel);
	}

	@Test
	public void shouldCreateProductSetCatalogVersionAndCodeWhenNoTargetProduct()
	{
		//Given
		WileyProductWsDto dto = mock(WileyProductWsDto.class);

		//When
		final WileyProductModel resultProduct = testInstance.convert(dto, null);

		//Then
		verify(modelService).create(WileyProductModel.class);
		verify(mockWileyProductModel).setCode(MOCK_PRODUCT_CODE);
		verify(mockWileyProductModel).setCatalogVersion(mockCatalogVersion);
		assertEquals(mockWileyProductModel, resultProduct);
	}

	@Test
	public void shouldPopulateB2cAssortment()
	{
		//Given
		WileyProductWsDto dto = mock(WileyProductWsDto.class);

		//When
		final WileyProductModel resultProduct = testInstance.convert(dto, null);

		//Then
		verify(resultProduct).setAssortments(Collections.singletonList(WileyAssortment.B2CASSORTMENT));
	}
}