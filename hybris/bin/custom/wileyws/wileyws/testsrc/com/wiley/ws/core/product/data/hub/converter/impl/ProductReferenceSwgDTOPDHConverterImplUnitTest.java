package com.wiley.ws.core.product.data.hub.converter.impl;


import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyProductModel;
import com.wiley.core.product.WileyProductService;
import com.wiley.ws.core.product.data.hub.dto.ProductReferenceSwgDTO;


/**
 * Test for {@link ProductReferenceSwgDTOPDHConverterImpl}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ProductReferenceSwgDTOPDHConverterImplUnitTest
{
	private static final String TEST_PRODUCT_MODEL_CODE = "productmodelcode";
	private static final String TEST_TARGET_PRODUCT_CODE = "targetproductcode";
	private static final String TEST_EXPECTED_REFERENCE_QUALIFIER = "productmodelcode_targetproductcode";
	private static final ProductReferenceSwgDTO.ReferenceTypeEnum
			TEST_TARGET_PRODUCT_DTO_SIMILAR_REFTYPE = ProductReferenceSwgDTO.ReferenceTypeEnum.SIMILAR;

	@InjectMocks
	private ProductReferenceSwgDTOPDHConverterImpl productReferenceSwgDTOPDHConverter;

	@Mock
	private ModelService modelServiceMock;

	@Mock
	private EnumerationService enumerationServiceMock;

	@Mock
	private WileyProductService wileyProductServiceMock;

	@Mock
	private ProductReferenceModel productReferenceModelMock;

	@Mock
	private ProductReferenceSwgDTO productReferenceMock;

	@Mock
	private WileyProductModel wileyProductModelMock;

	@Mock
	private CatalogVersionModel catalogVersionMock;

	@Before
	public void setup()
	{
		when(modelServiceMock.create(ProductReferenceModel.class)).thenReturn(productReferenceModelMock);

		when(wileyProductModelMock.getCode()).thenReturn(TEST_PRODUCT_MODEL_CODE);
		when(wileyProductModelMock.getCatalogVersion()).thenReturn(catalogVersionMock);

		//enum setup
		when(enumerationServiceMock.getEnumerationValue(eq(ProductReferenceTypeEnum.class), any())).thenAnswer(
				(invocationOnMock) ->
				{
					String productReferenceTypeName = (String) invocationOnMock.getArguments()[1];
					return ProductReferenceTypeEnum.valueOf(productReferenceTypeName);
				});
	}

	@Test
	public void shouldConvertToProductReferenceModel()
	{
		//Given
		when(productReferenceMock.getReferenceType()).thenReturn(TEST_TARGET_PRODUCT_DTO_SIMILAR_REFTYPE);
		when(productReferenceMock.getProductCode()).thenReturn(TEST_TARGET_PRODUCT_CODE);

		ProductModel expectedTargetProduct = mock(ProductModel.class);
		when(wileyProductServiceMock.getProductForCode(catalogVersionMock, TEST_TARGET_PRODUCT_CODE))
				.thenReturn(expectedTargetProduct);

		//When
		ProductReferenceModel productReferenceModel = productReferenceSwgDTOPDHConverter.convertToProductRefereceModel(
				productReferenceMock, wileyProductModelMock);

		//Then
		verify(productReferenceModel).setActive(Boolean.TRUE);
		verify(productReferenceModel).setPreselected(Boolean.TRUE);
		verify(productReferenceModel).setSource(wileyProductModelMock);
		verify(productReferenceModel).setQualifier(TEST_EXPECTED_REFERENCE_QUALIFIER);
		verify(productReferenceModel).setTarget(expectedTargetProduct);
		verify(productReferenceModel).setReferenceType(ProductReferenceTypeEnum.SIMILAR);
	}

}
