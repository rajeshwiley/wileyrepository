package com.wiley.ws.core.order.service.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.jalo.order.AbstractOrderEntryDiscountValuesAttributeHandler;
import de.hybris.platform.jalo.order.GlobalDiscountValuesAttributeHandler;
import de.hybris.platform.servicelayer.internal.model.attribute.impl.DefaultDynamicAttributesProvider;
import de.hybris.platform.servicelayer.internal.model.impl.ModelValueHistory;
import de.hybris.platform.servicelayer.model.ItemContextBuilder;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;
import de.hybris.platform.servicelayer.model.strategies.DefaultFetchStrategy;
import de.hybris.platform.util.DiscountValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.ws.core.order.dto.DiscountWsDTO;

import static com.wiley.ws.core.order.service.impl.WileycomExternalDiscountApplierImplTest.DiscountConstants.DISCOUNT_1_ABSOLUTE;
import static com.wiley.ws.core.order.service.impl.WileycomExternalDiscountApplierImplTest.DiscountConstants.DISCOUNT_1_NAME;
import static com.wiley.ws.core.order.service.impl.WileycomExternalDiscountApplierImplTest.DiscountConstants.DISCOUNT_1_VALUE;
import static com.wiley.ws.core.order.service.impl.WileycomExternalDiscountApplierImplTest.DiscountConstants.DISCOUNT_2_ABSOLUTE;
import static com.wiley.ws.core.order.service.impl.WileycomExternalDiscountApplierImplTest.DiscountConstants.DISCOUNT_2_NAME;
import static com.wiley.ws.core.order.service.impl.WileycomExternalDiscountApplierImplTest.DiscountConstants.DISCOUNT_2_VALUE;


/**
 * Author Herman_Chukhrai (EPAM)
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileycomExternalDiscountApplierImplTest
{

	@InjectMocks
	private WileycomExternalDiscountApplierImpl testInstanceMock;
	@Mock
	private DiscountWsDTO discount1;
	@Mock
	private DiscountWsDTO discount2;

	interface DiscountConstants
	{
		String DISCOUNT_1_NAME = "DISCOUNT_1_NAME";
		double DISCOUNT_1_VALUE = 2.0d;
		boolean DISCOUNT_1_ABSOLUTE = true;
		String DISCOUNT_2_NAME = "DISCOUNT_2_NAME";
		double DISCOUNT_2_VALUE = 1.0d;
		boolean DISCOUNT_2_ABSOLUTE = false;
	}

	@Before
	public void setUp() throws Exception
	{
		when(discount1.getName()).thenReturn(DISCOUNT_1_NAME);
		when(discount1.getValue()).thenReturn(DISCOUNT_1_VALUE);
		when(discount1.getAbsolute()).thenReturn(DISCOUNT_1_ABSOLUTE);

		when(discount2.getName()).thenReturn(DISCOUNT_2_NAME);
		when(discount2.getValue()).thenReturn(DISCOUNT_2_VALUE);
		when(discount2.getAbsolute()).thenReturn(DISCOUNT_2_ABSOLUTE);
	}

	@Test
	public void testOrderDiscounts() throws Exception
	{
		final OrderModel order = new OrderModel(createNewBuilder().build());
		order.setCurrency(new CurrencyModel());
		testInstanceMock.applyDiscounts(order, Arrays.asList(discount1, discount2));

		final List<DiscountValue> globalDiscountValues = order.getGlobalDiscountValues();
		assertTrue(CollectionUtils.isNotEmpty(globalDiscountValues));
		assertEquals(2, globalDiscountValues.size());
		checkCartDiscount(order,
				DISCOUNT_1_NAME,
				DISCOUNT_1_ABSOLUTE,
				DISCOUNT_1_VALUE
		);

		checkCartDiscount(order,
				DISCOUNT_2_NAME,
				DISCOUNT_2_ABSOLUTE,
				DISCOUNT_2_VALUE
		);
	}

	@Test
	public void testOrderEntriesDiscounts() throws Exception
	{
		final OrderEntryModel entry = new OrderEntryModel(createNewBuilder().build());
		final OrderModel order = new OrderModel();
		order.setCurrency(new CurrencyModel());
		entry.setOrder(order);
		testInstanceMock.applyDiscounts(entry, Arrays.asList(discount1, discount2));

		assertTrue(CollectionUtils.isNotEmpty(entry.getDiscountValues()));
		assertEquals(2, entry.getDiscountValues().size());
		checkCartEntryDiscount(entry,
				DISCOUNT_1_NAME,
				DISCOUNT_1_ABSOLUTE,
				DISCOUNT_1_VALUE
		);

		checkCartEntryDiscount(entry,
				DISCOUNT_2_NAME,
				DISCOUNT_2_ABSOLUTE,
				DISCOUNT_2_VALUE
		);
	}

	private static ItemContextBuilder createNewBuilder()
	{
		ItemContextBuilder builder = new ItemContextBuilder();
		builder.setValueHistory(new ModelValueHistory());
		builder.setFetchStrategy(new DefaultFetchStrategy());

		final Map<String, DynamicAttributeHandler> handlers = new HashMap<>();
		handlers.put("globalDiscountValues", new GlobalDiscountValuesAttributeHandler());
		handlers.put("discountValues", new AbstractOrderEntryDiscountValuesAttributeHandler());
		builder.setDynamicAttributesProvider(new DefaultDynamicAttributesProvider(handlers));

		return builder;
	}

	private void checkCartEntryDiscount(final AbstractOrderEntryModel cartEntry, final String discountCode,
			final boolean absolute, final Double value)
	{
		final List<DiscountValue> discountValues = cartEntry.getDiscountValues();
		final Optional<DiscountValue> optionalDiscountValue = findDiscountValueByCode(discountCode, discountValues);

		assertTrue(String.format("DiscountValue with code [%s] should be in cart entry.", discountCode),
				optionalDiscountValue.isPresent());

		final DiscountValue discountValue = optionalDiscountValue.get();
		checkDiscountValueState(absolute, value, discountValue);
	}

	private void checkCartDiscount(final AbstractOrderModel orderModel, final String discountCode,
			final boolean absolute, final Double value)
	{
		final List<DiscountValue> discountValues = orderModel.getGlobalDiscountValues();
		final Optional<DiscountValue> optionalDiscountValue = findDiscountValueByCode(discountCode,
				discountValues);

		assertTrue(String.format("DiscountValue with code [%s] should be in cart", discountCode),
				optionalDiscountValue.isPresent());

		final DiscountValue discountValue = optionalDiscountValue.get();
		checkDiscountValueState(absolute, value, discountValue);
	}

	private Optional<DiscountValue> findDiscountValueByCode(final String discountCode, final List<DiscountValue> discountValues)
	{
		return discountValues.stream()
				.filter(discountValue -> discountCode.equals(discountValue.getCode()))
				.findFirst();
	}


	private void checkDiscountValueState(final boolean absolute, final Double value, final DiscountValue discountValue)
	{
		assertEquals("DiscountValue.absolute", absolute, discountValue.isAbsolute());
		assertEquals("DiscountValue.value", value, Double.valueOf(discountValue.getValue()));
	}


}