package com.wiley.ws.core.order.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.order.DeliveryModeService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.SchoolModel;
import com.wiley.core.wileyb2b.unit.WileyB2BUnitService;
import com.wiley.core.wileycom.customer.service.WileycomCustomerAccountService;
import com.wiley.core.wileycom.order.WileycomOrderEntryService;
import com.wiley.ws.core.order.dto.OrderUpdateRequestWsDTO;
import com.wiley.ws.core.order.service.WileycomDigitalTokenApplier;
import com.wiley.ws.core.order.service.WileycomExternalDiscountApplier;
import com.wiley.ws.core.order.service.WileycomSubtotalWithoutDiscountApplier;

import static java.util.Optional.of;


/**
 * Author Herman_Chukhrai (EPAM)
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileycomOrderReversePopulatorTest
{
	@InjectMocks
	private WileycomOrderReversePopulator populator = new WileycomOrderReversePopulator();

	private OrderUpdateRequestWsDTO nonEmptyOrderDTO = new OrderUpdateRequestWsDTO();
	private OrderUpdateRequestWsDTO emptyOrderDTO = new OrderUpdateRequestWsDTO();

	@Mock
	private CommonI18NService commonI18NService;
	@Mock
	private WileyB2BUnitService wileyB2BUnitService;
	@Mock
	private WileycomCustomerAccountService wileycomCustomerAccountService;
	@Mock
	private DeliveryModeService deliveryModeService;

	@Mock
	private OrderModel orderModel;
	@Mock
	private CountryModel countryModel;
	@Mock
	private CurrencyModel currencyModel;
	@Mock
	private B2BUnitModel unitModel;
	@Mock
	private SchoolModel schoolModel;
	@Mock
	private DeliveryModeModel deliveryModeModel;
	@Mock
	private WileycomOrderEntryService wileycomOrderEntryService;
	@Mock
	private WileycomExternalDiscountApplier wileycomExternalDiscountApplier;
	@Mock
	private WileycomDigitalTokenApplier wileycomDigitalTokenApplier;
	@Mock
	private WileycomSubtotalWithoutDiscountApplier wileycomSubtotalWithoutDiscountApplier;

	private static final String COUNTRY_CODE = "UK";
	private static final String CURRENCY_CODE = "USD";
	private static final String SAP_ACCOUNT_NUMBER = "SAP_NUMBER";
	private static final String SCHOOL_ID = "SCHOOL_ID";
	private static final String DELIVERY_OPTION_CODE = "DELIVERY_OPTION_CODE";
	private static final Double SUBTOTAL = 55.5;
	private static final Double TOTAL_DISCOUNT = 2.5;
	private static final Double DELIVERY_COST = 3.5;
	private static final Double TOTAL_TAX = 1.5;
	private static final Double TOTAL_PRICE = 34.5;

	@Before
	public void setUp() throws Exception
	{
		//given
		initOrderUpdateDTO();
		when(commonI18NService.getCountry(COUNTRY_CODE)).thenReturn(countryModel);
		when(commonI18NService.getCurrency(CURRENCY_CODE)).thenReturn(currencyModel);
		when(wileyB2BUnitService.getB2BUnitForSapAccountNumber(SAP_ACCOUNT_NUMBER)).thenReturn(unitModel);
		when(wileycomCustomerAccountService.getSchoolByCode(SCHOOL_ID)).thenReturn(of(schoolModel));
		when(deliveryModeService.getDeliveryModeForCode(DELIVERY_OPTION_CODE)).thenReturn(deliveryModeModel);
		when(wileycomOrderEntryService.findOrderEntryByIsbn(eq(orderModel), Matchers.anyString())).thenReturn(Optional.empty());
	}

	private void initOrderUpdateDTO()
	{
		nonEmptyOrderDTO.setCountry(COUNTRY_CODE);
		nonEmptyOrderDTO.setCurrency(CURRENCY_CODE);
		nonEmptyOrderDTO.setSapAccountNumber(SAP_ACCOUNT_NUMBER);
		nonEmptyOrderDTO.setSchoolId(SCHOOL_ID);
		nonEmptyOrderDTO.setDeliveryOptionCode(DELIVERY_OPTION_CODE);
		nonEmptyOrderDTO.setSubtotal(SUBTOTAL);
		nonEmptyOrderDTO.setTotalDiscount(TOTAL_DISCOUNT);
		nonEmptyOrderDTO.setDeliveryCost(DELIVERY_COST);
		nonEmptyOrderDTO.setTotalTax(TOTAL_TAX);
		nonEmptyOrderDTO.setTotalPrice(TOTAL_PRICE);
	}

	@Test
	public void shouldPopulateNonNullFields() throws Exception
	{
		//when
		populator.populate(nonEmptyOrderDTO, orderModel);
		//then
		verify(orderModel).setCountry(countryModel);
		verify(orderModel).setCurrency(currencyModel);
		verify(orderModel).setUnit(unitModel);
		verify(orderModel).setSchool(schoolModel);
		verify(orderModel).setDeliveryMode(deliveryModeModel);
		verify(orderModel).setSubtotal(SUBTOTAL);
		verify(orderModel).setTotalDiscounts(TOTAL_DISCOUNT);
		verify(orderModel).setDeliveryCost(DELIVERY_COST);
		verify(orderModel).setTotalTax(TOTAL_TAX);
		verify(orderModel).setTotalPrice(TOTAL_PRICE);
	}

	@Test
	public void shouldNotPopulateNullFields() throws Exception
	{
		//when
		populator.populate(emptyOrderDTO, orderModel);
		//then
		verify(orderModel, never()).setCountry(any());
		verify(orderModel, never()).setCurrency(any());
		verify(orderModel, never()).setUnit(any());
		verify(orderModel, never()).setSchool(any());
		verify(orderModel, never()).setDeliveryMode(any());
		verify(orderModel, never()).setSubtotal(any());
		verify(orderModel, never()).setTotalDiscounts(any());
		verify(orderModel, never()).setDeliveryCost(any());
		verify(orderModel, never()).setTotalTax(any());
		verify(orderModel, never()).setTotalPrice(any());
	}

}