package com.wiley.ws.core.wileysubscription.converters.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.enums.PaymentModeEnum;
import com.wiley.core.model.WileySubscriptionPaymentTransactionModel;
import com.wiley.ws.core.wileysubscription.dto.PaymentTransactionWsDTO;

import static com.wiley.core.util.TestUtils.expectException;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;


/**
 * Default unit test for {@link Wileyb2cWsPaymentTransactionPopulator}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cWsPaymentTransactionPopulatorUnitTest
{

	@Mock
	private CommonI18NService commonI18NServiceMock;

	@InjectMocks
	private Wileyb2cWsPaymentTransactionPopulator wileyb2cWsPaymentTransactionPopulator;

	// Test data

	@Mock
	private PaymentTransactionWsDTO paymentTransactionWsDTOMock;

	@Mock
	private WileySubscriptionPaymentTransactionModel wileySubscriptionPaymentTransactionModelMock;

	@Mock
	private CurrencyModel currencyModelMock;

	@Test
	public void testPopulateWithNonNullFields()
	{
		// Given
		double amount = 20.5;
		String currency = "USD";
		Date date = new Date();
		PaymentModeEnum paymentType = PaymentModeEnum.CARD;
		PaymentTransactionType paymentStatus = PaymentTransactionType.CAPTURE;

		String paymentTypeString = "CARD";
		String paymentStatusString = "CAPTURE";
		String paymentToken = "123123123313";
		when(paymentTransactionWsDTOMock.getAmount()).thenReturn(amount);
		when(paymentTransactionWsDTOMock.getCurrency()).thenReturn(currency);
		when(paymentTransactionWsDTOMock.getDate()).thenReturn(date);
		when(paymentTransactionWsDTOMock.getPaymentStatus()).thenReturn(paymentStatusString);
		when(paymentTransactionWsDTOMock.getPaymentType()).thenReturn(paymentTypeString);
		when(paymentTransactionWsDTOMock.getPaymentToken()).thenReturn(paymentToken);

		when(commonI18NServiceMock.getCurrency(eq(currency))).thenReturn(currencyModelMock);

		// When
		wileyb2cWsPaymentTransactionPopulator.populate(paymentTransactionWsDTOMock, wileySubscriptionPaymentTransactionModelMock);

		// Then
		verify(wileySubscriptionPaymentTransactionModelMock).setAmount(eq(amount));
		verify(wileySubscriptionPaymentTransactionModelMock).setCurrency(eq(currencyModelMock));
		verify(wileySubscriptionPaymentTransactionModelMock).setTime(eq(date));
		verify(wileySubscriptionPaymentTransactionModelMock).setType(eq(paymentStatus));
		verify(wileySubscriptionPaymentTransactionModelMock).setPaymentMode(eq(paymentType));
		verify(wileySubscriptionPaymentTransactionModelMock).setPaymentToken(eq(paymentToken));
	}

	@Test
	public void testPopulateWithNullFields()
	{
		// Given
		when(paymentTransactionWsDTOMock.getAmount()).thenReturn(null);
		when(paymentTransactionWsDTOMock.getCurrency()).thenReturn(null);
		when(paymentTransactionWsDTOMock.getDate()).thenReturn(null);
		when(paymentTransactionWsDTOMock.getPaymentStatus()).thenReturn(null);
		when(paymentTransactionWsDTOMock.getPaymentType()).thenReturn(null);
		when(paymentTransactionWsDTOMock.getPaymentToken()).thenReturn(null);

		// When
		wileyb2cWsPaymentTransactionPopulator.populate(paymentTransactionWsDTOMock, wileySubscriptionPaymentTransactionModelMock);

		// Then
		verify(wileySubscriptionPaymentTransactionModelMock).setPaymentToken(eq(null));
		verifyNoMoreInteractions(wileySubscriptionPaymentTransactionModelMock);
	}

	@Test
	public void testPopulateWithIllegalParameters()
	{
		expectException(IllegalArgumentException.class,
				() -> wileyb2cWsPaymentTransactionPopulator.populate(null, null));

		expectException(IllegalArgumentException.class,
				() -> wileyb2cWsPaymentTransactionPopulator.populate(paymentTransactionWsDTOMock, null));
		verifyZeroInteractions(paymentTransactionWsDTOMock);

		expectException(IllegalArgumentException.class,
				() -> wileyb2cWsPaymentTransactionPopulator.populate(null, wileySubscriptionPaymentTransactionModelMock));
		verifyZeroInteractions(wileySubscriptionPaymentTransactionModelMock);
	}

}