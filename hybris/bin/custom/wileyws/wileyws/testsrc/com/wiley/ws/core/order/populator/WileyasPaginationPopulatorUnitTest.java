package com.wiley.ws.core.order.populator;

import com.wiley.ws.core.order.dto.PaginationWsDTO;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.search.pagedata.PaginationData;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyasPaginationPopulatorUnitTest
{
    private static final Integer PAGE_SIZE = 10;
    private static final Integer PAGE_CURRENT = 1;
    private static final String SORT = "byDate";
    private static final Integer RESULTS_NUMBER = 30;
    private static final Integer PAGES_NUMBER = RESULTS_NUMBER / PAGE_SIZE;

    @InjectMocks
    private WileyasPaginationPopulator targetMock;

    @Mock
    private PaginationData paginationMock;

    private PaginationWsDTO paginationWsDTO;

    @Before
    public void prepare()
    {
        when(paginationMock.getNumberOfPages()).thenReturn(PAGES_NUMBER);
        when(paginationMock.getTotalNumberOfResults()).thenReturn(RESULTS_NUMBER.longValue());
        when(paginationMock.getCurrentPage()).thenReturn(PAGE_CURRENT);
        when(paginationMock.getPageSize()).thenReturn(PAGE_SIZE);
        when(paginationMock.getSort()).thenReturn(SORT);

        paginationWsDTO = new PaginationWsDTO();
    }

    @Test(expected = IllegalArgumentException.class)
    public void populateWithNullDataObjectTest()
    {
        // given

        // when
        targetMock.populate(null, paginationWsDTO);

        // then
    }

    @Test(expected = IllegalArgumentException.class)
    public void populateWithNullDtoObjectTest()
    {
        // given

        // when
        targetMock.populate(paginationMock, null);

        // then
    }

    @Test
    public void populateSuccessfulTest()
    {
        // given

        // when
        targetMock.populate(paginationMock, paginationWsDTO);

        // then
        assertNotNull(paginationWsDTO);
        assertEquals(PAGE_SIZE, paginationWsDTO.getPageSize());
        assertEquals(PAGE_CURRENT, paginationWsDTO.getCurrentPage());
        assertEquals(SORT, paginationWsDTO.getSort());
        assertEquals(RESULTS_NUMBER, paginationWsDTO.getTotalResults());
        assertEquals(PAGES_NUMBER, paginationWsDTO.getTotalPages());
    }
}
