package com.wiley.ws.core.product.crud.converter.populator.baseproduct;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.variants.model.VariantTypeModel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyProductModel;
import com.wiley.core.model.WileyPurchaseOptionProductModel;
import com.wiley.ws.core.product.crud.dto.WileyProductWsDto;

@RunWith(MockitoJUnitRunner.class)
public class WileyProductVariantTypePopulatorTest
{
	@Mock
	private TypeService typeService;
	@Mock
	private WileyProductModel targetModel;
	private VariantTypeModel variantTypeModel;
	private WileyProductVariantTypePopulator testInstance;

	@Before
	public void setUp() throws Exception
	{
		testInstance = new WileyProductVariantTypePopulator(typeService);
	}

	@Test
	public void shouldPopulateVariantTypeForBaseProduct()
	{
		// Given
		final WileyProductWsDto dto = new WileyProductWsDto();
		// When
		when(typeService.getComposedTypeForCode(WileyPurchaseOptionProductModel._TYPECODE)).thenReturn(variantTypeModel);
		testInstance.populate(dto, targetModel);
		// Then
		verify(targetModel).setVariantType(variantTypeModel);
	}
}
