package com.wiley.ws.core.wileysubscription.populator.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.InvoicePaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.wiley.core.model.WileySubscriptionModel;
import com.wiley.ws.core.wileysubscription.dto.PaymentInfoUpdateRequestWsDTO;
import com.wiley.ws.core.wileysubscription.dto.WileySubscriptionAddressWsDTO;


/**
 * Default unit test for {@link InvoicePaymentInfoCreatingStrategy}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class InvoicePaymentInfoCreatingStrategyUnitTest
{

	@Mock
	private KeyGenerator guidKeyGeneratorMock;

	@Mock
	private ModelService modelServiceMock;

	@Mock
	private Populator<WileySubscriptionAddressWsDTO, AddressModel> wileySubscriptionAddressWsReversePopulatorMock;

	@InjectMocks
	private InvoicePaymentInfoCreatingStrategy invoicePaymentInfoCreatingStrategy;

	// Test data

	private static final String TEST_GUID = "111-2222-eeeee-44444";

	@Mock
	private PaymentInfoUpdateRequestWsDTO paymentInfoUpdateRequestWsDTOMock;

	@Mock
	private InvoicePaymentInfoModel invoicePaymentInfoModelMock;

	@Mock
	private WileySubscriptionModel wileySubscriptionModelMock;

	@Mock
	private CustomerModel customerModelMock;

	@Before
	public void setUp() throws Exception
	{
		when(guidKeyGeneratorMock.generate()).thenReturn(TEST_GUID);
		when(wileySubscriptionModelMock.getCustomer()).thenReturn(customerModelMock);
	}

	@Test
	public void testPopulateRequiredFieldsWhenFieldsIsNotNull()
	{
		// Given
		String existingCode = "Some existing code";
		when(invoicePaymentInfoModelMock.getCode()).thenReturn(existingCode);

		// When
		invoicePaymentInfoCreatingStrategy.populate(paymentInfoUpdateRequestWsDTOMock, invoicePaymentInfoModelMock);

		// Then
		assertEquals(existingCode, invoicePaymentInfoModelMock.getCode());
	}

	@Test
	public void testPopulateRequiredFieldsWhenFieldsIsNull()
	{
		// Given
		when(invoicePaymentInfoModelMock.getCode()).thenReturn(null);

		// When
		invoicePaymentInfoCreatingStrategy.populate(paymentInfoUpdateRequestWsDTOMock, invoicePaymentInfoModelMock);

		// Then
		verify(invoicePaymentInfoModelMock).setCode(eq(TEST_GUID));
	}

	@Test
	public void testIsSupportedPositiveCase()
	{
		// Given
		// no changes in test data

		// When
		final boolean supported = invoicePaymentInfoCreatingStrategy.isSupported(invoicePaymentInfoModelMock);

		// Then
		assertTrue(supported);
	}

	@Test
	public void testIsSupportedNegativeCase()
	{
		// Given
		// no changes in test data

		// When
		final boolean supported = invoicePaymentInfoCreatingStrategy.isSupported(mock(CreditCardPaymentInfoModel.class));

		// Then
		assertFalse(supported);
	}

	@Test
	public void testIsSupportedWithNullParameter()
	{
		// Given
		// no changes in test data

		// When
		final boolean supported = invoicePaymentInfoCreatingStrategy.isSupported(null);

		// Then
		assertFalse(supported);
	}

	@Test
	public void testGetOrCreateNewSupportedPaymentInfoPositiveCase()
	{
		// Given
		// no changes in test data

		// When
		final InvoicePaymentInfoModel actualPaymentInfo =
				invoicePaymentInfoCreatingStrategy.getOrCreateNewSupportedPaymentInfo(invoicePaymentInfoModelMock,
						wileySubscriptionModelMock);

		// Then
		assertSame(invoicePaymentInfoModelMock, actualPaymentInfo);
		verifyZeroInteractions(modelServiceMock, wileySubscriptionAddressWsReversePopulatorMock);
	}

	@Test
	public void testGetOrCreateNewSupportedPaymentInfoWhenPaymentInfoIsNotSupported()
	{
		// Given
		when(modelServiceMock.create(eq(InvoicePaymentInfoModel.class))).thenReturn(invoicePaymentInfoModelMock);
		final CreditCardPaymentInfoModel invalidPaymentInfo = mock(CreditCardPaymentInfoModel.class);

		// When
		final InvoicePaymentInfoModel actualPaymentInfo =
				invoicePaymentInfoCreatingStrategy.getOrCreateNewSupportedPaymentInfo(invalidPaymentInfo,
						wileySubscriptionModelMock);

		// Then
		assertSame(invoicePaymentInfoModelMock, actualPaymentInfo);

		verify(modelServiceMock).create(eq(InvoicePaymentInfoModel.class));
		verify(modelServiceMock).remove(eq(invalidPaymentInfo));
		verify(modelServiceMock, never()).save(eq(invoicePaymentInfoModelMock));

		verify(invoicePaymentInfoModelMock).setOwner(eq(wileySubscriptionModelMock));
		verify(invoicePaymentInfoModelMock).setUser(eq(customerModelMock));
		verify(wileySubscriptionModelMock).setPaymentInfo(eq(invoicePaymentInfoModelMock));

		verifyZeroInteractions(wileySubscriptionAddressWsReversePopulatorMock);
	}

	@Test
	public void testGetOrCreateNewSupportedPaymentInfoWhenParameterIsNull()
	{
		// Given
		when(modelServiceMock.create(eq(InvoicePaymentInfoModel.class))).thenReturn(invoicePaymentInfoModelMock);

		// When
		final InvoicePaymentInfoModel actualPaymentInfo =
				invoicePaymentInfoCreatingStrategy.getOrCreateNewSupportedPaymentInfo(null,
						wileySubscriptionModelMock);

		// Then
		assertSame(invoicePaymentInfoModelMock, actualPaymentInfo);

		verify(modelServiceMock).create(eq(InvoicePaymentInfoModel.class));
		verify(modelServiceMock, never()).remove(any(PaymentInfoModel.class));
		verify(modelServiceMock, never()).save(eq(invoicePaymentInfoModelMock));

		verify(invoicePaymentInfoModelMock).setOwner(eq(wileySubscriptionModelMock));
		verify(invoicePaymentInfoModelMock).setUser(eq(customerModelMock));
		verify(wileySubscriptionModelMock).setPaymentInfo(eq(invoicePaymentInfoModelMock));

		verifyZeroInteractions(wileySubscriptionAddressWsReversePopulatorMock);
	}

	@Test
	public void testGetPaymentInfoClass()
	{
		// Given
		// no changes in test data

		// When
		final Class<InvoicePaymentInfoModel> paymentInfoClass = invoicePaymentInfoCreatingStrategy.getPaymentInfoClass();

		// Then
		assertNotNull(paymentInfoClass);
		assertTrue(InvoicePaymentInfoModel.class.isAssignableFrom(paymentInfoClass));
	}

	@Test
	public void testPopulateWithNonNullFields()
	{
		// Given
		final AddressModel addressModelMock = mock(AddressModel.class);
		when(modelServiceMock.create(AddressModel.class)).thenReturn(addressModelMock);

		final PaymentInfoUpdateRequestWsDTO paymentInfoUpdateRequestWsDTOMock = mock(PaymentInfoUpdateRequestWsDTO.class);
		final WileySubscriptionAddressWsDTO addressWsDTOMock = mock(WileySubscriptionAddressWsDTO.class);

		when(paymentInfoUpdateRequestWsDTOMock.getPaymentAddress()).thenReturn(addressWsDTOMock);

		// When
		invoicePaymentInfoCreatingStrategy.populate(paymentInfoUpdateRequestWsDTOMock, invoicePaymentInfoModelMock);

		// Then
		verify(modelServiceMock).create(eq(AddressModel.class));

		verify(invoicePaymentInfoModelMock).setCode(eq(TEST_GUID));
		verify(wileySubscriptionAddressWsReversePopulatorMock).populate(same(addressWsDTOMock), same(addressModelMock));
		verify(invoicePaymentInfoModelMock).setBillingAddress(same(addressModelMock));
		verify(addressModelMock).setOwner(invoicePaymentInfoModelMock);
	}

	@Test
	public void testPopulateWithNullFields()
	{
		// Given
		final PaymentInfoUpdateRequestWsDTO paymentInfoUpdateRequestWsDTOMock = mock(PaymentInfoUpdateRequestWsDTO.class,
				(Answer) invocation -> null);

		// When
		invoicePaymentInfoCreatingStrategy.populate(paymentInfoUpdateRequestWsDTOMock, invoicePaymentInfoModelMock);

		// Then
		verifyZeroInteractions(modelServiceMock, wileySubscriptionAddressWsReversePopulatorMock);
		verify(invoicePaymentInfoModelMock).setCode(eq(TEST_GUID));
		verify(invoicePaymentInfoModelMock, never()).setBillingAddress(any());
	}
}