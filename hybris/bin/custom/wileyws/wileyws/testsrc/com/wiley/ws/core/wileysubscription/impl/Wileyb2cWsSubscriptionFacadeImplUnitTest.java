package com.wiley.ws.core.wileysubscription.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.wiley.core.enums.PaymentModeEnum;
import com.wiley.core.model.WileySubscriptionModel;
import com.wiley.core.subscription.services.WileySubscriptionService;
import com.wiley.core.util.TestUtils;
import com.wiley.core.wileyws.order.WileycomWsOrderService;
import com.wiley.ws.core.wileysubscription.converters.populator.Wileyb2cWsPaymentTransactionPopulator;
import com.wiley.ws.core.wileysubscription.dto.PaymentInfoUpdateRequestWsDTO;
import com.wiley.ws.core.wileysubscription.dto.SubscriptionCreateRequestWsDTO;
import com.wiley.ws.core.wileysubscription.dto.SubscriptionCreateResponseWsDTO;
import com.wiley.ws.core.wileysubscription.dto.SubscriptionUpdateRequestWsDTO;
import com.wiley.ws.core.wileysubscription.populator.PaymentInfoCreatingStrategy;


/**
 * Default unit test for {@link Wileyb2cWsSubscriptionFacadeImpl}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cWsSubscriptionFacadeImplUnitTest
{
	@Mock
	private ModelService modelServiceMock;

	@Mock
	private WileySubscriptionService wileySubscriptionServiceMock;

	@Mock
	private WileycomWsOrderService wileycomWsOrderServiceMock;

	@Mock
	private Populator<SubscriptionUpdateRequestWsDTO, WileySubscriptionModel> wileyUpdateSubscriptionWsReversePopulator;

	@Mock
	private Populator<SubscriptionCreateRequestWsDTO, WileySubscriptionModel> wileyCreateSubscriptionWsReversePopulator;

	@Mock
	private Wileyb2cWsPaymentTransactionPopulator wileyb2cWsPaymentTransactionPopulatorMock;

	@Mock
	private PaymentInfoCreatingStrategy<PaymentInfoModel> paymentInfoCreatingStrategyMock;

	@Mock
	private Converter<WileySubscriptionModel, SubscriptionCreateResponseWsDTO> wileySubscriptionCreateResponseWsConverterMock;

	@InjectMocks
	private Wileyb2cWsSubscriptionFacadeImpl wileyb2cWsSubscriptionFacade;

	// Test data

	private static final String TEST_SUBSCRIPTION_CODE = "SomeSubscriptionCode";

	private static final String TEST_ORDER_CODE = "Test_order_code";

	@Mock
	private WileySubscriptionModel wileySubscriptionModelMock;

	@Mock
	private SubscriptionCreateRequestWsDTO subscriptionCreateRequestWsDTOMock;

	@Mock
	private SubscriptionUpdateRequestWsDTO subscriptionUpdateRequestWsDTOMock;

	@Mock
	private OrderModel orderModelMock;

	@Mock
	private CustomerModel customerModelMock;

	@Mock
	private PaymentInfoModel paymentInfoModelMock;

	@Mock
	private PaymentInfoUpdateRequestWsDTO paymentInfoUpdateRequestWsDTOMock;

	@Mock
	private SubscriptionCreateResponseWsDTO subscriptionCreateResponseWsDTOMock;

	@Before
	public void setUp() throws Exception
	{
		ReflectionTestUtils.setField(wileyb2cWsSubscriptionFacade, "wileySubscriptionCreateResponseWsConverter",
				wileySubscriptionCreateResponseWsConverterMock);

		wileyb2cWsSubscriptionFacade.setDefaultPaymentMode(PaymentModeEnum.CARD);
		wileyb2cWsSubscriptionFacade.setPaymentMode2StrategyMap(
				Collections.singletonMap(PaymentModeEnum.CARD, paymentInfoCreatingStrategyMock));

		when(paymentInfoCreatingStrategyMock.isSupported(eq(paymentInfoModelMock))).thenReturn(true);

		when(orderModelMock.getUser()).thenReturn(customerModelMock);

		when(modelServiceMock.create(eq(WileySubscriptionModel.class))).thenReturn(wileySubscriptionModelMock);
		when(wileySubscriptionServiceMock.getSubscriptionByCode(eq(TEST_SUBSCRIPTION_CODE))).thenReturn(
				wileySubscriptionModelMock);

		when(orderModelMock.getCode()).thenReturn(TEST_ORDER_CODE);
		when(wileycomWsOrderServiceMock.findOrderByCode(eq(TEST_ORDER_CODE))).thenReturn(orderModelMock);
		when(subscriptionCreateRequestWsDTOMock.getOrderId()).thenReturn(TEST_ORDER_CODE);
	}

	@Test
	public void testDoesSubscriptionExistWhenItemExists()
	{
		// Given
		// no changes in test data

		// When
		boolean result = wileyb2cWsSubscriptionFacade.doesSubscriptionExist(TEST_SUBSCRIPTION_CODE);

		// Then
		assertTrue(result);
		verify(wileySubscriptionServiceMock).getSubscriptionByCode(eq(TEST_SUBSCRIPTION_CODE));
	}

	@Test
	public void testDoesSubscriptionExistWhenItemNotFoundNullCase()
	{
		// Given
		when(wileySubscriptionServiceMock.getSubscriptionByCode(eq(TEST_SUBSCRIPTION_CODE))).thenReturn(null);

		// When
		boolean result = wileyb2cWsSubscriptionFacade.doesSubscriptionExist(TEST_SUBSCRIPTION_CODE);

		// Then
		assertFalse(result);
		verify(wileySubscriptionServiceMock).getSubscriptionByCode(eq(TEST_SUBSCRIPTION_CODE));
	}

	@Test
	public void testDoesSubscriptionExistWhenItemNotFoundExceptionCase()
	{
		// Given
		final String subscriptionCode = "Unknown code";
		when(wileySubscriptionServiceMock.getSubscriptionByCode(eq(subscriptionCode)))
				.thenThrow(UnknownIdentifierException.class);

		// When
		boolean result = wileyb2cWsSubscriptionFacade.doesSubscriptionExist(subscriptionCode);

		// Then
		assertFalse(result);
		verify(wileySubscriptionServiceMock).getSubscriptionByCode(eq(subscriptionCode));
	}

	@Test
	public void testDoesSubscriptionExistWithIllegalParameter()
	{
		TestUtils.expectException(IllegalArgumentException.class, () -> wileyb2cWsSubscriptionFacade.doesSubscriptionExist(null));
	}

	@Test
	public void testCreateSubscriptionSuccessCase()
	{
		// Given
		final AddressModel addressModelMock = mock(AddressModel.class);

		String orderId = "SomeOrderId";
		when(subscriptionCreateRequestWsDTOMock.getOrderId()).thenReturn(orderId);
		when(wileySubscriptionModelMock.getPaymentInfo()).thenReturn(paymentInfoModelMock);

		when(wileycomWsOrderServiceMock.findOrderByCode(eq(orderId))).thenReturn(orderModelMock);

		when(paymentInfoCreatingStrategyMock
				.getOrCreateNewSupportedPaymentInfo(eq(paymentInfoModelMock), eq(wileySubscriptionModelMock))).thenReturn(
				paymentInfoModelMock);

		when(subscriptionCreateRequestWsDTOMock.getPaymentInfo()).thenReturn(paymentInfoUpdateRequestWsDTOMock);

		when(paymentInfoModelMock.getBillingAddress()).thenReturn(addressModelMock);

		// mark entities as changed
		when(modelServiceMock.isModified(eq(wileySubscriptionModelMock))).thenReturn(true);
		when(modelServiceMock.isModified(eq(paymentInfoModelMock))).thenReturn(true);
		when(modelServiceMock.isModified(eq(addressModelMock))).thenReturn(true);

		// When
		wileyb2cWsSubscriptionFacade.createSubscription(subscriptionCreateRequestWsDTOMock);

		// Then
		verify(wileyCreateSubscriptionWsReversePopulator).populate(same(subscriptionCreateRequestWsDTOMock),
				same(wileySubscriptionModelMock));
		verify(paymentInfoCreatingStrategyMock).populate(same(paymentInfoUpdateRequestWsDTOMock), same(paymentInfoModelMock));
		// check that all changed entities is changed
		verify(modelServiceMock).saveAll(eq(Arrays.asList(wileySubscriptionModelMock, paymentInfoModelMock, addressModelMock)));
	}

	@Test
	public void testCreateSubscriptionShouldReturnSubscriptionCode()
	{
		// Given
		when(wileySubscriptionCreateResponseWsConverterMock.convert(same(wileySubscriptionModelMock))).thenReturn(
				subscriptionCreateResponseWsDTOMock);

		// When
		final SubscriptionCreateResponseWsDTO subscription = wileyb2cWsSubscriptionFacade.createSubscription(
				subscriptionCreateRequestWsDTOMock);

		// Then
		assertSame(subscriptionCreateResponseWsDTOMock, subscription);
	}

	@Test
	public void testCreateSubscriptionWhenStrategyForPaymentInfoIsNotFound()
	{
		// Given
		when(subscriptionCreateRequestWsDTOMock.getPaymentInfo()).thenReturn(paymentInfoUpdateRequestWsDTOMock);
		when(paymentInfoUpdateRequestWsDTOMock.getPaymentType()).thenReturn(null);
		when(wileySubscriptionModelMock.getPaymentInfo()).thenReturn(null);

		when(paymentInfoCreatingStrategyMock.getOrCreateNewSupportedPaymentInfo(eq(null), eq(wileySubscriptionModelMock)))
				.thenReturn(paymentInfoModelMock);

		// When
		wileyb2cWsSubscriptionFacade.createSubscription(subscriptionCreateRequestWsDTOMock);

		// Then
		verify(paymentInfoCreatingStrategyMock).getOrCreateNewSupportedPaymentInfo(eq(null),
				eq(wileySubscriptionModelMock));
		verify(paymentInfoCreatingStrategyMock).populate(eq(paymentInfoUpdateRequestWsDTOMock), eq(paymentInfoModelMock));
	}

	@Test
	public void testCreateSubscriptionWhenPaymentTypeIsNotSpecified()
	{
		// Given
		when(subscriptionCreateRequestWsDTOMock.getPaymentInfo()).thenReturn(paymentInfoUpdateRequestWsDTOMock);
		when(paymentInfoUpdateRequestWsDTOMock.getPaymentType()).thenReturn(null);
		when(paymentInfoCreatingStrategyMock.getOrCreateNewSupportedPaymentInfo(eq(null), eq(wileySubscriptionModelMock)))
				.thenReturn(paymentInfoModelMock);

		// When
		wileyb2cWsSubscriptionFacade.createSubscription(subscriptionCreateRequestWsDTOMock);

		// Then
		verify(paymentInfoCreatingStrategyMock).getOrCreateNewSupportedPaymentInfo(eq(null), eq(wileySubscriptionModelMock));
		verify(paymentInfoCreatingStrategyMock).populate(eq(paymentInfoUpdateRequestWsDTOMock), eq(paymentInfoModelMock));
	}

	@Test
	public void testUpdateSubscriptionSuccessCase()
	{
		// Given
		final AddressModel addressModelMock = mock(AddressModel.class);

		String orderId = "SomeOrderId";
		when(wileySubscriptionModelMock.getPaymentInfo()).thenReturn(paymentInfoModelMock);

		when(wileycomWsOrderServiceMock.findOrderByCode(eq(orderId))).thenReturn(orderModelMock);

		when(paymentInfoCreatingStrategyMock
				.getOrCreateNewSupportedPaymentInfo(eq(paymentInfoModelMock), eq(wileySubscriptionModelMock))).thenReturn(
				paymentInfoModelMock);

		when(subscriptionUpdateRequestWsDTOMock.getPaymentInfo()).thenReturn(paymentInfoUpdateRequestWsDTOMock);

		when(paymentInfoModelMock.getBillingAddress()).thenReturn(addressModelMock);

		// mark entities as changed
		when(modelServiceMock.isModified(eq(wileySubscriptionModelMock))).thenReturn(true);
		when(modelServiceMock.isModified(eq(paymentInfoModelMock))).thenReturn(true);
		when(modelServiceMock.isModified(eq(addressModelMock))).thenReturn(true);

		// When
		wileyb2cWsSubscriptionFacade.updateSubscription(TEST_SUBSCRIPTION_CODE, subscriptionUpdateRequestWsDTOMock);

		// Then
		verify(wileyUpdateSubscriptionWsReversePopulator).populate(same(subscriptionUpdateRequestWsDTOMock),
				same(wileySubscriptionModelMock));
		verify(paymentInfoCreatingStrategyMock).populate(same(paymentInfoUpdateRequestWsDTOMock), same(paymentInfoModelMock));
		// check that all changed entities is changed
		verify(modelServiceMock).saveAll(eq(Arrays.asList(wileySubscriptionModelMock, paymentInfoModelMock, addressModelMock)));
	}

	@Test
	public void testUpdateSubscriptionWhenStrategyForPaymentInfoIsNotFound()
	{
		// Given
		when(subscriptionUpdateRequestWsDTOMock.getPaymentInfo()).thenReturn(paymentInfoUpdateRequestWsDTOMock);
		when(paymentInfoUpdateRequestWsDTOMock.getPaymentType()).thenReturn(null);
		when(wileySubscriptionModelMock.getPaymentInfo()).thenReturn(null);

		when(paymentInfoCreatingStrategyMock.getOrCreateNewSupportedPaymentInfo(eq(null), eq(wileySubscriptionModelMock)))
				.thenReturn(paymentInfoModelMock);

		// When
		wileyb2cWsSubscriptionFacade.updateSubscription(TEST_SUBSCRIPTION_CODE, subscriptionUpdateRequestWsDTOMock);

		// Then
		verify(paymentInfoCreatingStrategyMock).getOrCreateNewSupportedPaymentInfo(eq(null),
				eq(wileySubscriptionModelMock));
		verify(paymentInfoCreatingStrategyMock).populate(eq(paymentInfoUpdateRequestWsDTOMock), eq(paymentInfoModelMock));
	}

	@Test
	public void testUpdateSubscriptionWhenPaymentTypeIsNotSpecified()
	{
		// Given
		when(subscriptionUpdateRequestWsDTOMock.getPaymentInfo()).thenReturn(paymentInfoUpdateRequestWsDTOMock);
		when(paymentInfoUpdateRequestWsDTOMock.getPaymentType()).thenReturn(null);
		when(wileySubscriptionModelMock.getPaymentInfo()).thenReturn(paymentInfoModelMock);
		when(paymentInfoCreatingStrategyMock
				.getOrCreateNewSupportedPaymentInfo(eq(paymentInfoModelMock), eq(wileySubscriptionModelMock)))
				.thenReturn(paymentInfoModelMock);

		// When
		wileyb2cWsSubscriptionFacade.updateSubscription(TEST_SUBSCRIPTION_CODE, subscriptionUpdateRequestWsDTOMock);

		// Then
		verify(paymentInfoCreatingStrategyMock).isSupported(same(paymentInfoModelMock));
		verify(paymentInfoCreatingStrategyMock).getOrCreateNewSupportedPaymentInfo(eq(paymentInfoModelMock),
				eq(wileySubscriptionModelMock));
		verify(paymentInfoCreatingStrategyMock).populate(eq(paymentInfoUpdateRequestWsDTOMock), eq(paymentInfoModelMock));
	}

	@Test
	public void testCreateSubscriptionWithInvalidParams()
	{
		// When
		TestUtils.expectException(IllegalArgumentException.class,
				() -> wileyb2cWsSubscriptionFacade.createSubscription(null));
		// Then
		verifyZeroInteractions(modelServiceMock, wileyCreateSubscriptionWsReversePopulator, wileycomWsOrderServiceMock,
				wileySubscriptionServiceMock);
	}

	@Test
	public void testUpdateSubscriptionWithInvalidParams()
	{
		// When
		TestUtils.expectException(IllegalArgumentException.class,
				() -> wileyb2cWsSubscriptionFacade.updateSubscription(null, null));
		// Then
		verifyZeroInteractions(modelServiceMock, wileyUpdateSubscriptionWsReversePopulator, wileycomWsOrderServiceMock,
				wileySubscriptionServiceMock);

		// When
		TestUtils.expectException(IllegalArgumentException.class,
				() -> wileyb2cWsSubscriptionFacade.updateSubscription(TEST_SUBSCRIPTION_CODE, null));
		// Then
		verifyZeroInteractions(modelServiceMock, wileyUpdateSubscriptionWsReversePopulator, wileycomWsOrderServiceMock,
				wileySubscriptionServiceMock);

		// When
		TestUtils.expectException(IllegalArgumentException.class,
				() -> wileyb2cWsSubscriptionFacade.updateSubscription(null, subscriptionUpdateRequestWsDTOMock));
		// Then
		verifyZeroInteractions(modelServiceMock, wileyUpdateSubscriptionWsReversePopulator, wileycomWsOrderServiceMock,
				wileySubscriptionServiceMock);
	}
}