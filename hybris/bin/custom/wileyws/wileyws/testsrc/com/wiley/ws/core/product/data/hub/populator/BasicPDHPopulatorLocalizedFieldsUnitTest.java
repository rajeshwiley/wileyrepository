package com.wiley.ws.core.product.data.hub.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang.LocaleUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.ws.core.common.dto.LocaleSwgDTO;
import com.wiley.ws.core.product.data.hub.dto.LocalizedLongStringSwgDTO;
import com.wiley.ws.core.product.data.hub.dto.LocalizedStringSwgDTO;
import com.wiley.ws.core.product.data.hub.dto.ProductSwgDTO;


/**
 * Test for {@link BasicPDHPopulator} of localized fields populating
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class BasicPDHPopulatorLocalizedFieldsUnitTest
{

	private static final String LOCALIZED_LOCALE_1 = "en";
	private static final String LOCALIZED_VALUE_1 = "val";

	private static final String LOCALIZED_LOCALE_2 = "pl";
	private static final String LOCALIZED_VALUE_2 = "val";

	private static final String TEST_AUHTORS = "authors";
	private static final String EN_LANG_ISOCODE = "en";

	@InjectMocks
	private BasicPDHPopulator basicPDHPopulator;

	@Mock
	private ProductSwgDTO productMock;

	@Mock
	private ProductModel productModelMock;

	@Test
	public void shouldSkipPopulatingProductNameDueToDTONameIsNull()
	{
		//Given
		when(productMock.getName()).thenReturn(null);

		//When
		basicPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock, never()).setName(any());
		verify(productModelMock, never()).setName(any(), any());
	}

	@Test
	public void shouldPopulateProductName()
	{
		//Given
		List<LocalizedStringSwgDTO> localizedStringSwgDTOList = new LinkedList<>();
		localizedStringSwgDTOList.add(createLocalizedStringSwgDTO(LOCALIZED_LOCALE_1, LOCALIZED_VALUE_1));
		localizedStringSwgDTOList.add(createLocalizedStringSwgDTO(LOCALIZED_LOCALE_2, LOCALIZED_VALUE_2));

		when(productMock.getName()).thenReturn(localizedStringSwgDTOList);

		//When
		basicPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock, never()).setName(any());
		verify(productModelMock).setName(LOCALIZED_VALUE_1, LocaleUtils.toLocale(LOCALIZED_LOCALE_1));
		verify(productModelMock).setName(LOCALIZED_VALUE_2, LocaleUtils.toLocale(LOCALIZED_LOCALE_2));
	}

	private LocalizedStringSwgDTO createLocalizedStringSwgDTO(final String loc, final String val)
	{
		LocalizedStringSwgDTO localizedStringSwgDTO = new LocalizedStringSwgDTO();
		localizedStringSwgDTO.setLoc(new LocaleSwgDTO(loc));
		localizedStringSwgDTO.setVal(val);
		return localizedStringSwgDTO;

	}

	@Test
	public void shouldSkipPopulatingProductDescriptionDueToDTODescriptionIsNull1()
	{
		//Given
		when(productMock.getDescription()).thenReturn(null);

		//When
		basicPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock, never()).setDescription(any());
		verify(productModelMock, never()).setDescription(any(), any());
	}

	@Test
	public void shouldPopulateProductDescription()
	{
		//Given
		List<LocalizedLongStringSwgDTO> localizedLongStringSwgDTOList = createLocalizedLongStringSwgDTOList();

		when(productMock.getDescription()).thenReturn(localizedLongStringSwgDTOList);

		//When
		basicPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock, never()).setDescription(any());
		verify(productModelMock).setDescription(LOCALIZED_VALUE_1, LocaleUtils.toLocale(LOCALIZED_LOCALE_1));
		verify(productModelMock).setDescription(LOCALIZED_VALUE_2, LocaleUtils.toLocale(LOCALIZED_LOCALE_2));
	}

	@Test
	public void shouldSkipPopulatingProductSummaryDueToDTOSummaryIsNull1()
	{
		//Given
		when(productMock.getSummary()).thenReturn(null);

		//When
		basicPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock, never()).setSummary(any());
		verify(productModelMock, never()).setSummary(any(), any());
	}

	@Test
	public void shouldPopulateProductSummary()
	{
		//Given
		List<LocalizedLongStringSwgDTO> localizedLongStringSwgDTOList = createLocalizedLongStringSwgDTOList();

		when(productMock.getSummary()).thenReturn(localizedLongStringSwgDTOList);

		//When
		basicPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock, never()).setName(any());
		verify(productModelMock).setSummary(LOCALIZED_VALUE_1, LocaleUtils.toLocale(LOCALIZED_LOCALE_1));
		verify(productModelMock).setSummary(LOCALIZED_VALUE_2, LocaleUtils.toLocale(LOCALIZED_LOCALE_2));
	}

	private List<LocalizedLongStringSwgDTO> createLocalizedLongStringSwgDTOList()
	{
		List<LocalizedLongStringSwgDTO> localizedLongStringSwgDTOList = new LinkedList<>();
		localizedLongStringSwgDTOList.add(createLocalizedLongStringSwgDTO(LOCALIZED_LOCALE_1, LOCALIZED_VALUE_1));
		localizedLongStringSwgDTOList.add(createLocalizedLongStringSwgDTO(LOCALIZED_LOCALE_2, LOCALIZED_VALUE_2));
		return localizedLongStringSwgDTOList;
	}


	private LocalizedLongStringSwgDTO createLocalizedLongStringSwgDTO(final String loc, final String val)
	{
		LocalizedLongStringSwgDTO localizedStringSwgDTO = new LocalizedLongStringSwgDTO();
		localizedStringSwgDTO.setLoc(new LocaleSwgDTO(loc));
		localizedStringSwgDTO.setVal(val);
		return localizedStringSwgDTO;

	}

	// TODO : [ECSC-20075] commented out code which uses obsolete attribute. Should be deleted
/*	@Test
	public void shouldSkipPopulatingAuthors()
	{
		//Given
		when(productMock.getAuthors()).thenReturn(null);

		//When
		basicPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock, never()).setAuthors(any());
	}

	@Test
	public void shouldPopulateAuthors()
	{
		//Given
		when(productMock.getAuthors()).thenReturn(TEST_AUHTORS);

		//When
		basicPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock).setAuthors(TEST_AUHTORS, LocaleUtils.toLocale(EN_LANG_ISOCODE));
	}*/
}

