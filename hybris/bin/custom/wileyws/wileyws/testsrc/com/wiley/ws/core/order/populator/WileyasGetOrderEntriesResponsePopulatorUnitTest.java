package com.wiley.ws.core.order.populator;

import com.wiley.integrations.order.dto.OrderEntryWsDTO;
import com.wiley.ws.core.order.dto.GetOrderEntriesResponseWsDTO;
import com.wiley.ws.core.order.dto.PaginationWsDTO;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.search.pagedata.PaginationData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyasGetOrderEntriesResponsePopulatorUnitTest
{
    @InjectMocks
    private WileyasGetOrderEntriesResponsePopulator target;

    @Mock
    private Converter<PaginationData, PaginationWsDTO> wileyasPaginationConverterMock;

    @Mock
    private Converter<AbstractOrderEntryModel, OrderEntryWsDTO> wileyOrderEntryWsConverterMock;

    @Mock
    private SearchPageData<AbstractOrderEntryModel> searchPageDataMock;

    @Mock
    private List<AbstractOrderEntryModel> resultMock;

    @Mock
    private PaginationData paginationMock;

    @Mock
    private GetOrderEntriesResponseWsDTO orderEntriesResponseWsDTOMock;

    @Mock
    private PaginationWsDTO paginationDtoMock;

    @Mock
    private List<OrderEntryWsDTO> entryDtoListMock;

    @Before
    public void prepare()
    {
        target.setWileyasPaginationConverter(wileyasPaginationConverterMock);
        target.setWileyOrderEntryWsConverter(wileyOrderEntryWsConverterMock);
        when(searchPageDataMock.getPagination()).thenReturn(paginationMock);
        when(searchPageDataMock.getResults()).thenReturn(resultMock);
        doReturn(paginationDtoMock).when(wileyasPaginationConverterMock).convert(any(PaginationData.class));
        doReturn(entryDtoListMock).when(wileyOrderEntryWsConverterMock).convertAll(any());
    }

    @Test(expected = IllegalArgumentException.class)
    public void populateWithNullDataObjectTest()
    {
        // given

        // when
        target.populate(null, orderEntriesResponseWsDTOMock);

        // then
    }

    @Test(expected = IllegalArgumentException.class)
    public void populateWithNullDtoObjectTest()
    {
        // given

        // when
        target.populate(searchPageDataMock, null);

        // then
    }

    @Test
    public void populateSuccessfulTest()
    {
        // given

        // when
        target.populate(searchPageDataMock, orderEntriesResponseWsDTOMock);

        // then
        verify(wileyasPaginationConverterMock).convert(any());
        verify(wileyOrderEntryWsConverterMock).convertAll(any());
    }
}
