package com.wiley.ws.core.wileysubscription.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.subscriptionservices.enums.SubscriptionStatus;
import de.hybris.platform.subscriptionservices.enums.TermOfServiceFrequency;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.model.WileySubscriptionModel;
import com.wiley.ws.core.wileysubscription.dto.AbstractSubscriptionRequestWsDTO;
import com.wiley.ws.core.wileysubscription.dto.PaymentInfoUpdateRequestWsDTO;
import com.wiley.ws.core.wileysubscription.dto.SubscriptionCreateRequestWsDTO;
import com.wiley.ws.core.wileysubscription.dto.SubscriptionCreateResponseWsDTO;
import com.wiley.ws.core.wileysubscription.dto.SubscriptionUpdateRequestWsDTO;
import com.wiley.ws.core.wileysubscription.dto.WileySubscriptionAddressWsDTO;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;


/**
 * Default integration test for {@link Wileyb2cWsSubscriptionFacadeImpl}
 */
@IntegrationTest
public class Wileyb2cWsSubscriptionFacadeImplIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{

	// WileySubscription
	private static final String EXTERNAL_CODE = "12341234";
	private static final String ISBN = "9780471000053";
	private static final String ORDER_WITH_SUBSCRIPTION = "orderWithSubscription";
	private static final AbstractSubscriptionRequestWsDTO.SubscriptionStatus SUBSCRIPTION_STATUS =
			AbstractSubscriptionRequestWsDTO.SubscriptionStatus.ACTIVE;
	private static final Date SUBSCRIPTION_START_DATE = Date.from(LocalDateTime.now().toInstant(ZoneOffset.UTC));
	private static final Date SUBSCRIPTION_END_DATE = Date.from(LocalDateTime.now().toInstant(ZoneOffset.UTC));
	private static final Date NEXT_BILLING_DATE = Date.from(LocalDateTime.now().toInstant(ZoneOffset.UTC));
	private static final double BILLING_PRICE_VALUE = 25.55;
	private static final String BILLING_PRICE_CURRENCY = "USD";
	private static final AbstractSubscriptionRequestWsDTO.BillingFrequency BILLING_FREQUENCY =
			AbstractSubscriptionRequestWsDTO.BillingFrequency.FREE;
	private static final int CONTRACT_DURATION_VALUE = 5;
	private static final AbstractSubscriptionRequestWsDTO.ContractDurationUnit CONTRACT_DURATION_UNIT =
			AbstractSubscriptionRequestWsDTO.ContractDurationUnit.DAY;
	private static final boolean RENEWABLE = false;

	// CreditCardPaymentInfo
	private static final int CREDIT_CARD_VALID_TO_MONTH = 5;
	private static final int CREDIT_CARD_VALID_TO_YEAR = 2020;
	private static final String PAYMENT_TOKEN = "325435435435";

	// Billing address
	private static final String BILLING_ADDRESS_POSTCODE = "123123";
	private static final String BILLING_ADDRESS_COUNTRY = "US";
	private static final String BILLING_ADDRESS_STATE = "CA";
	private static final String BILLING_ADDRESS_CITY = "SomeCity";
	private static final String BILLING_ADDRESS_LINE_1 = "Line 1";
	private static final String BILLING_ADDRESS_LINE_2 = "Line 2";
	private static final String BILLING_ADDRESS_FIRST_NAME = "First name";
	private static final String BILLING_ADDRESS_LAST_NAME = "Last name";

	private static final String BILLING_ADDRESS_PHONE_NUMBER = "1231231235555";
	// Shipping address
	private static final String SHIPPING_ADDRESS_POSTCODE = "123123";
	private static final String SHIPPING_ADDRESS_COUNTRY = "US";
	private static final String SHIPPING_ADDRESS_STATE = "CA";
	private static final String SHIPPING_ADDRESS_CITY = "SomeCity";
	private static final String SHIPPING_ADDRESS_LINE_1 = "Line 1";
	private static final String SHIPPING_ADDRESS_LINE_2 = "Line 2";
	private static final String SHIPPING_ADDRESS_FIRST_NAME = "First name";
	private static final String SHIPPING_ADDRESS_LAST_NAME = "Last name";
	private static final String SHIPPING_ADDRESS_PHONE_NUMBER = "1231231235555";
	private static final String SHIPPING_ADDRESS_POSTCODE_UPDATE = "123123_UPDATE";
	private static final String SHIPPING_ADDRESS_LINE_1_UPDATE = "Line 1_UPDATE";
	private static final String SHIPPING_ADDRESS_LINE_2_UPDATE = "Line 2_UPDATE";
	@Resource
	private Wileyb2cWsSubscriptionFacadeImpl wileyb2cWsSubscriptionFacade;

	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Resource
	private ModelService modelService;

	@Before
	public void setUp() throws Exception
	{
		importCsv("/wileyws/test/wileysubscription/Wileyb2cWsSubscriptionFacadeImplIntegrationTest/test-subscriptions.impex",
				DEFAULT_ENCODING);
	}

	@Test
	public void testCreateSubscription()
	{
		// Given
		final SubscriptionCreateRequestWsDTO subscriptionCreateRequestWsDTO = createAndInitSubscriptionCreateRequestWsDTO();

		// When
		final SubscriptionCreateResponseWsDTO createResponseWsDTO = wileyb2cWsSubscriptionFacade.createSubscription(
				subscriptionCreateRequestWsDTO);
		clearModelContextFromUnsavedData();

		// Then
		assertNotNull(createResponseWsDTO);

		final String subscriptionCode = createResponseWsDTO.getInternalSubscriptionId();
		assertNotNull(subscriptionCode);

		final WileySubscriptionModel wileySubscription = getWileySubscriptionByCode(
				subscriptionCode);

		checkStateOfWileySubscription(subscriptionCode, wileySubscription);
		checkAddresses(wileySubscription, subscriptionCreateRequestWsDTO);
	}

	private void checkAddresses(final WileySubscriptionModel wileySubscription,
			final AbstractSubscriptionRequestWsDTO subscriptionCreateRequestWsDTO)
	{
		checkStateOfAddress(wileySubscription.getDeliveryAddress(), subscriptionCreateRequestWsDTO.getShippingAddress());
		checkStateOfAddress(wileySubscription.getPaymentInfo().getBillingAddress(),
				subscriptionCreateRequestWsDTO.getPaymentInfo().getPaymentAddress());
	}

	@Test
	public void testUpdateSubscription()
	{
		// Given
		final String existingSubscriptionCode = "wsTestSubscription";

		final SubscriptionUpdateRequestWsDTO subscriptionUpdateRequestWsDTO = createAndInitSubscriptionUpdateRequestWsDTO();
		final WileySubscriptionAddressWsDTO shippingAddress = subscriptionUpdateRequestWsDTO.getShippingAddress();

		// When
		wileyb2cWsSubscriptionFacade.updateSubscription(existingSubscriptionCode, subscriptionUpdateRequestWsDTO);
		clearModelContextFromUnsavedData();

		// Then
		WileySubscriptionModel wileySubscription = getWileySubscriptionByCode(existingSubscriptionCode);
		assertNotNull(wileySubscription);

		checkStateOfWileySubscription(existingSubscriptionCode, wileySubscription);
		checkAddresses(wileySubscription, subscriptionUpdateRequestWsDTO);

		//Given
		updateShippingAddress(shippingAddress);

		// When
		wileyb2cWsSubscriptionFacade.updateSubscription(existingSubscriptionCode, subscriptionUpdateRequestWsDTO);
		clearModelContextFromUnsavedData();

		// Then
		wileySubscription = getWileySubscriptionByCode(existingSubscriptionCode);
		assertNotNull(wileySubscription);
		checkStateOfWileySubscription(existingSubscriptionCode, wileySubscription);
		checkAddresses(wileySubscription, subscriptionUpdateRequestWsDTO);
	}

	private static void updateShippingAddress(final WileySubscriptionAddressWsDTO shippingAddress)
	{
		shippingAddress.setPostcode(SHIPPING_ADDRESS_POSTCODE_UPDATE);
		shippingAddress.setLine1(SHIPPING_ADDRESS_LINE_1_UPDATE);
		shippingAddress.setLine2(SHIPPING_ADDRESS_LINE_2_UPDATE);
	}

	private SubscriptionCreateRequestWsDTO createAndInitSubscriptionCreateRequestWsDTO()
	{
		final SubscriptionCreateRequestWsDTO subscriptionCreateRequestWsDTO = new SubscriptionCreateRequestWsDTO();
		subscriptionCreateRequestWsDTO.setExternalCode(EXTERNAL_CODE);
		subscriptionCreateRequestWsDTO.setIsbn(ISBN);
		subscriptionCreateRequestWsDTO.setOrderId(ORDER_WITH_SUBSCRIPTION);
		subscriptionCreateRequestWsDTO.setSubscriptionStatus(SUBSCRIPTION_STATUS);
		subscriptionCreateRequestWsDTO.setSubscriptionStartDate(SUBSCRIPTION_START_DATE);
		subscriptionCreateRequestWsDTO.setSubscriptionEndDate(SUBSCRIPTION_END_DATE);
		subscriptionCreateRequestWsDTO.setNextBillingDate(NEXT_BILLING_DATE);
		subscriptionCreateRequestWsDTO.setBillingPriceValue(BILLING_PRICE_VALUE);
		subscriptionCreateRequestWsDTO.setBillingPriceCurrency(BILLING_PRICE_CURRENCY);
		subscriptionCreateRequestWsDTO.setBillingFrequency(BILLING_FREQUENCY);
		subscriptionCreateRequestWsDTO.setContractDurationValue(CONTRACT_DURATION_VALUE);
		subscriptionCreateRequestWsDTO.setContractDurationUnit(CONTRACT_DURATION_UNIT);
		subscriptionCreateRequestWsDTO.setRenewable(RENEWABLE);

		final PaymentInfoUpdateRequestWsDTO paymentInfoWsDTO = createAndInitPaymentInfoUpdateRequestWsDTO();
		subscriptionCreateRequestWsDTO.setPaymentInfo(paymentInfoWsDTO);

		final WileySubscriptionAddressWsDTO paymentAddressWsDTO = createAndInitPaymentAddressWsDTO();
		paymentInfoWsDTO.setPaymentAddress(paymentAddressWsDTO);

		final WileySubscriptionAddressWsDTO shippingAddressWsDTO = createAndInitShippingAddressWsDTO();
		subscriptionCreateRequestWsDTO.setShippingAddress(shippingAddressWsDTO);
		return subscriptionCreateRequestWsDTO;
	}

	private SubscriptionUpdateRequestWsDTO createAndInitSubscriptionUpdateRequestWsDTO()
	{
		final SubscriptionUpdateRequestWsDTO subscriptionUpdateRequestWsDTO = new SubscriptionUpdateRequestWsDTO();
		subscriptionUpdateRequestWsDTO.setExternalCode(EXTERNAL_CODE);
		subscriptionUpdateRequestWsDTO.setSubscriptionStatus(SUBSCRIPTION_STATUS);
		subscriptionUpdateRequestWsDTO.setSubscriptionStartDate(SUBSCRIPTION_START_DATE);
		subscriptionUpdateRequestWsDTO.setSubscriptionEndDate(SUBSCRIPTION_END_DATE);
		subscriptionUpdateRequestWsDTO.setNextBillingDate(NEXT_BILLING_DATE);
		subscriptionUpdateRequestWsDTO.setBillingPriceValue(BILLING_PRICE_VALUE);
		subscriptionUpdateRequestWsDTO.setBillingPriceCurrency(BILLING_PRICE_CURRENCY);
		subscriptionUpdateRequestWsDTO.setBillingFrequency(BILLING_FREQUENCY);
		subscriptionUpdateRequestWsDTO.setContractDurationValue(CONTRACT_DURATION_VALUE);
		subscriptionUpdateRequestWsDTO.setContractDurationUnit(CONTRACT_DURATION_UNIT);
		subscriptionUpdateRequestWsDTO.setRenewable(RENEWABLE);

		final PaymentInfoUpdateRequestWsDTO paymentInfoWsDTO = createAndInitPaymentInfoUpdateRequestWsDTO();
		subscriptionUpdateRequestWsDTO.setPaymentInfo(paymentInfoWsDTO);

		final WileySubscriptionAddressWsDTO paymentAddressWsDTO = createAndInitPaymentAddressWsDTO();
		paymentInfoWsDTO.setPaymentAddress(paymentAddressWsDTO);

		final WileySubscriptionAddressWsDTO shippingAddressWsDTO = createAndInitShippingAddressWsDTO();
		subscriptionUpdateRequestWsDTO.setShippingAddress(shippingAddressWsDTO);
		return subscriptionUpdateRequestWsDTO;
	}

	private WileySubscriptionAddressWsDTO createAndInitShippingAddressWsDTO()
	{
		final WileySubscriptionAddressWsDTO shippingAddressWsDTO = new WileySubscriptionAddressWsDTO();
		shippingAddressWsDTO.setPostcode(SHIPPING_ADDRESS_POSTCODE);
		shippingAddressWsDTO.setCountry(SHIPPING_ADDRESS_COUNTRY);
		shippingAddressWsDTO.setState(SHIPPING_ADDRESS_STATE);
		shippingAddressWsDTO.setCity(SHIPPING_ADDRESS_CITY);
		shippingAddressWsDTO.setLine1(SHIPPING_ADDRESS_LINE_1);
		shippingAddressWsDTO.setLine2(SHIPPING_ADDRESS_LINE_2);
		shippingAddressWsDTO.setFirstName(SHIPPING_ADDRESS_FIRST_NAME);
		shippingAddressWsDTO.setLastName(SHIPPING_ADDRESS_LAST_NAME);
		shippingAddressWsDTO.setPhoneNumber(SHIPPING_ADDRESS_PHONE_NUMBER);
		return shippingAddressWsDTO;
	}

	private PaymentInfoUpdateRequestWsDTO createAndInitPaymentInfoUpdateRequestWsDTO()
	{
		final PaymentInfoUpdateRequestWsDTO paymentInfoWsDTO = new PaymentInfoUpdateRequestWsDTO();
		paymentInfoWsDTO.setPaymentType(PaymentInfoUpdateRequestWsDTO.PaymentType.CARD);
		paymentInfoWsDTO.setPaymentToken(PAYMENT_TOKEN);
		paymentInfoWsDTO.setCreditCardType(PaymentInfoUpdateRequestWsDTO.CreditCardType.VISA);
		paymentInfoWsDTO.setCreditCardValidToMonth(CREDIT_CARD_VALID_TO_MONTH);
		paymentInfoWsDTO.setCreditCardValidToYear(CREDIT_CARD_VALID_TO_YEAR);
		return paymentInfoWsDTO;
	}

	private WileySubscriptionAddressWsDTO createAndInitPaymentAddressWsDTO()
	{
		final WileySubscriptionAddressWsDTO paymentAddressWsDTO = new WileySubscriptionAddressWsDTO();
		paymentAddressWsDTO.setPostcode(BILLING_ADDRESS_POSTCODE);
		paymentAddressWsDTO.setCountry(BILLING_ADDRESS_COUNTRY);
		paymentAddressWsDTO.setState(BILLING_ADDRESS_STATE);
		paymentAddressWsDTO.setCity(BILLING_ADDRESS_CITY);
		paymentAddressWsDTO.setLine1(BILLING_ADDRESS_LINE_1);
		paymentAddressWsDTO.setLine2(BILLING_ADDRESS_LINE_2);
		paymentAddressWsDTO.setFirstName(BILLING_ADDRESS_FIRST_NAME);
		paymentAddressWsDTO.setLastName(BILLING_ADDRESS_LAST_NAME);
		paymentAddressWsDTO.setPhoneNumber(BILLING_ADDRESS_PHONE_NUMBER);
		return paymentAddressWsDTO;
	}

	private void checkStateOfWileySubscription(final String subscriptionCode, final WileySubscriptionModel wileySubscription)
	{
		assertNotNull(wileySubscription);
		assertEquals(subscriptionCode, wileySubscription.getCode());
		assertEquals(EXTERNAL_CODE, wileySubscription.getExternalCode());
		assertEquals(ISBN, wileySubscription.getIsbn());
		assertEquals(ISBN, wileySubscription.getProduct().getIsbn());
		assertEquals(wileySubscription.getProduct(), wileySubscription.getOrderEntry().getProduct());
		assertEquals(ORDER_WITH_SUBSCRIPTION, wileySubscription.getOrderEntry().getOrder().getCode());
		assertEquals(SubscriptionStatus.ACTIVE, wileySubscription.getStatus());
		assertEquals(SUBSCRIPTION_START_DATE, wileySubscription.getStartDate());
		assertEquals(SUBSCRIPTION_END_DATE, wileySubscription.getExpirationDate());
		assertEquals(NEXT_BILLING_DATE, wileySubscription.getNextBillingDate());
		assertEquals(BILLING_PRICE_VALUE, wileySubscription.getBillingPriceValue());
		assertEquals(BILLING_PRICE_CURRENCY, wileySubscription.getBillingPriceCurrency().getIsocode());
		assertEquals(BILLING_FREQUENCY.getBillingFrequencyCode(), wileySubscription.getBillingFrequency().getCode());
		assertEquals(CONTRACT_DURATION_VALUE, wileySubscription.getContractDurationValue().intValue());
		assertEquals(TermOfServiceFrequency.DAILY, wileySubscription.getContractDurationFrequency());
		assertEquals(true, wileySubscription.getRenewalEnabled().booleanValue());
		assertEquals(RENEWABLE, wileySubscription.getRequireRenewal().booleanValue());

		checkStateOfPaymentInfo(wileySubscription);
	}

	private void checkStateOfPaymentInfo(final WileySubscriptionModel wileySubscription)
	{
		final PaymentInfoModel paymentInfo = wileySubscription.getPaymentInfo();
		assertNotNull(paymentInfo);
		assertTrue(paymentInfo instanceof CreditCardPaymentInfoModel);
		CreditCardPaymentInfoModel creditCardPaymentInfo = (CreditCardPaymentInfoModel) paymentInfo;
		assertEquals(CreditCardType.VISA, creditCardPaymentInfo.getType());
		assertEquals(String.valueOf(CREDIT_CARD_VALID_TO_MONTH), creditCardPaymentInfo.getValidToMonth());
		assertEquals(String.valueOf(CREDIT_CARD_VALID_TO_YEAR), creditCardPaymentInfo.getValidToYear());
	}

	private void checkStateOfAddress(final AddressModel addressModel,
			final WileySubscriptionAddressWsDTO addressDTO)
	{
		assertNotNull(addressModel);
		assertEquals(addressDTO.getPostcode(), addressModel.getPostalcode());
		assertEquals(addressDTO.getCountry(), addressModel.getCountry().getIsocode());
		assertEquals(addressDTO.getCountry() + "-" + addressDTO.getState(), addressModel.getRegion().getIsocode());
		assertEquals(addressDTO.getCity(), addressModel.getTown());
		assertEquals(addressDTO.getLine1(), addressModel.getLine1());
		assertEquals(addressDTO.getLine2(), addressModel.getLine2());
		assertEquals(addressDTO.getFirstName(), addressModel.getFirstname());
		assertEquals(addressDTO.getLastName(), addressModel.getLastname());
		assertEquals(addressDTO.getPhoneNumber(), addressModel.getPhone1());
	}

	private WileySubscriptionModel getWileySubscriptionByCode(final String code)
	{
		final WileySubscriptionModel example = new WileySubscriptionModel();
		example.setCode(code);
		return flexibleSearchService.getModelByExample(example);
	}

	private void clearModelContextFromUnsavedData()
	{
		modelService.detachAll();
	}
}