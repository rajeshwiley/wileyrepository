package com.wiley.ws.core.product.dataintegration.common.service.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyProductModel;
import com.wiley.core.model.WileyPurchaseOptionProductModel;
import com.wiley.core.product.WileyProductService;
import com.wiley.ws.core.product.crud.facade.WileyProductCrudCatalogVersionProvider;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ProductImportServiceImplUnitTest
{
	private static final String PURCHASE_OPTION_PRODUCT_CODE = "PURCHASE_OPTION_PRODUCT_CODE";
	private static final String ANOTHER_PURCHASE_OPTION_PRODUCT_CODE = "ANOTHER_PURCHASE_OPTION_PRODUCT_CODE";
	private static final String BASE_PRODUCT_CODE = "BASE_PRODUCT_CODE";

	@Mock
	private WileyProductCrudCatalogVersionProvider wileyProductCrudCatalogVersionProvider;
	@Mock
	private WileyProductService productService;

	private ProductImportServiceImpl testInstance;

	@Before
	public void setUp() throws Exception
	{
		testInstance = new ProductImportServiceImpl(wileyProductCrudCatalogVersionProvider, productService);
	}

	@Test(expected = RuntimeException.class)
	public void shouldThrowAnExceptionIfProductOptionDoesNotBelongToBaseProduct()
	{
		//Given
		final WileyPurchaseOptionProductModel anotherPurchaseOptionProduct =
				givenVariantProduct(ANOTHER_PURCHASE_OPTION_PRODUCT_CODE);
		final ProductModel baseProduct = givenBaseProduct(BASE_PRODUCT_CODE, anotherPurchaseOptionProduct);
		final WileyPurchaseOptionProductModel purchaseOptionProduct = givenVariantProduct(PURCHASE_OPTION_PRODUCT_CODE);
		//When
		testInstance.checkVariantBelongsToBaseProduct(purchaseOptionProduct, baseProduct);
	}

	@Test
	public void shouldDoNotThrowAnExceptionIfProductHasNoVariants()
	{
		//Given
		final ProductModel baseProduct = givenBaseProduct(BASE_PRODUCT_CODE);
		final WileyPurchaseOptionProductModel purchaseOptionProduct = givenVariantProduct(PURCHASE_OPTION_PRODUCT_CODE);
		//When
		testInstance.checkVariantBelongsToBaseProduct(purchaseOptionProduct, baseProduct);
	}

	@Test
	public void shouldWorkFineIfVariantBelongsToProduct()
	{
		//Given
		final WileyPurchaseOptionProductModel purchaseOptionProduct =
				givenVariantProduct(PURCHASE_OPTION_PRODUCT_CODE);
		final WileyPurchaseOptionProductModel anotherPurchaseOptionProduct =
				givenVariantProduct(ANOTHER_PURCHASE_OPTION_PRODUCT_CODE);
		final ProductModel baseProduct = givenBaseProduct(BASE_PRODUCT_CODE, purchaseOptionProduct, anotherPurchaseOptionProduct);
		//When
		testInstance.checkVariantBelongsToBaseProduct(purchaseOptionProduct, baseProduct);
	}

	private WileyPurchaseOptionProductModel givenVariantProduct(final String code)
	{
		final WileyPurchaseOptionProductModel purchaseOptionProduct = mock(WileyPurchaseOptionProductModel.class);
		when(purchaseOptionProduct.getCode()).thenReturn(code);
		return purchaseOptionProduct;
	}

	private WileyProductModel givenBaseProduct(final String code, final WileyPurchaseOptionProductModel... variants)
	{
		final WileyProductModel baseProduct = mock(WileyProductModel.class);
		when(baseProduct.getCode())
				.thenReturn(BASE_PRODUCT_CODE);

		when(baseProduct.getVariants())
				.thenReturn(
						Arrays.asList(variants)
				);
		return baseProduct;
	}

}