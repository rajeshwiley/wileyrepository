package com.wiley.ws.core.product.crud.converter.populator.baseproduct;

import de.hybris.bootstrap.annotations.IntegrationTest;

import java.util.Arrays;
import java.util.Locale;

import javax.annotation.Resource;

import org.junit.Test;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.model.WileyProductModel;
import com.wiley.ws.core.product.crud.dto.WileyProductWsDto;
import com.wiley.ws.core.product.dataintegration.common.dto.LocalizedStringSwgDTO;
import com.wiley.ws.core.product.dataintegration.common.util.LocalizedPopulatorUtil;

import static org.fest.assertions.Assertions.assertThat;



@IntegrationTest
public class ProductDtoNamePopulatorIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	private static final String EN_VALUE = "en_value";
	private static final String FR_VALUE = "fr_value";

	@Resource
	private ProductDtoNamePopulator productDtoNamePopulator;

	@Resource
	private LocalizedPopulatorUtil localizedPopulatorUtil;


	@Test
	public void shouldPopulateAllLocalesForProductName() throws Exception
	{
		//Given
		final WileyProductWsDto sourceDto = givenProductDto(
				givenLocalizedString("en", EN_VALUE),
				givenLocalizedString("fr", FR_VALUE)
		);
		final WileyProductModel targetProduct = new WileyProductModel();

		//When
		productDtoNamePopulator.populate(sourceDto, targetProduct);

		//Then
		assertThat(targetProduct.getName(Locale.ENGLISH)).isEqualTo(EN_VALUE);
		assertThat(targetProduct.getName(Locale.FRENCH)).isEqualTo(FR_VALUE);
		assertThat(targetProduct.getName(Locale.GERMAN)).isNull();
	}

	private WileyProductWsDto givenProductDto(final LocalizedStringSwgDTO... names)
	{
		final WileyProductWsDto wileyProductWsDto = new WileyProductWsDto();
		wileyProductWsDto.setName(Arrays.asList(names));
		return wileyProductWsDto;
	}

	private LocalizedStringSwgDTO givenLocalizedString(final String locale, final String value)
	{
		final LocalizedStringSwgDTO localizedString = new LocalizedStringSwgDTO();
		localizedString.setLocale(locale);
		localizedString.setValue(value);
		return localizedString;
	}

}