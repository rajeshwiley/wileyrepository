package com.wiley.ws.core.product.data.hub.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.ws.core.product.data.hub.dto.ProductSwgDTO;


/**
 * Test for {@link BasicPDHPopulator}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class BasicPDHPopulatorSimpleFieldsUnitTest
{
	private static final String TEST_CODE = "productcode";
	private static final Boolean TEST_PURCHASABLE = true;
	private static final Boolean TEST_COUNTABLE = true;
	private static final Date TEST_ONLINE_DATE = new Date(100L);
	private static final Date TEST_OFFILE_DATE = new Date(200L);
	private static final String TEST_ISBN = "isbn";
	private static final String TEST_PRINT_ISSN = "printisbn";
	private static final String TEST_ONLINE_ISSN = "printisbn";
	private static final Date TEST_RELEASE_DATE = new Date(400L);
	private static final Boolean TEST_PRINT_ON_DEMAND = true;
	private static final Boolean TEST_TEXTBOOK = true;
	private static final String TEST_TAX_CATEGORY = "taxcategory";
	private static final String TEST_TAX_TRANSACTION_TYPE = "taxtransactiontype";
	private static final String TEST_SAP_PRODUCT_CODE = "sapproductcode";
	private static final String TEST_PMD_PRODUCT_CODE = "pdmProductCode";

	@InjectMocks
	private BasicPDHPopulator basicPDHPopulator;

	@Mock
	private ProductSwgDTO productMock;

	@Mock
	private ProductModel productModelMock;


	@Test
	public void shouldSkipPopulatingCode()
	{
		//Given
		when(productMock.getCode()).thenReturn(null);

		//When
		basicPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock, never()).setCode(any());
	}

	@Test
	public void shouldPopulateCode()
	{
		//Given
		when(productMock.getCode()).thenReturn(TEST_CODE);

		//When
		basicPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock).setCode(TEST_CODE);
	}

	// TODO : [ECSC-20075] commented out code which uses obsolete attribute. Should be deleted
/*
	@Test
	public void shouldSkipPopulatingPurchasable()
	{
		//Given
		when(productMock.getPurchasable()).thenReturn(null);

		//When
		basicPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock, never()).setPurchasable(any());
	}

	@Test
	public void shouldPopulatePurchasable()
	{
		//Given
		when(productMock.getPurchasable()).thenReturn(TEST_PURCHASABLE);

		//When
		basicPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock).setPurchasable(TEST_PURCHASABLE);
	}
*/

	@Test
	public void shouldSkipPopulatingCountable()
	{
		//Given
		when(productMock.getCountable()).thenReturn(null);

		//When
		basicPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock, never()).setCountable(any());
	}

	@Test
	public void shouldPopulateCountable()
	{
		//Given
		when(productMock.getCountable()).thenReturn(TEST_COUNTABLE);

		//When
		basicPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock).setCountable(TEST_COUNTABLE);
	}

	@Test
	public void shouldSkipPopulatingOnlineDate()
	{
		//Given
		when(productMock.getOnlineDate()).thenReturn(null);

		//When
		basicPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock, never()).setOnlineDate(any());
	}

	@Test
	public void shouldPopulateOnlineDate()
	{
		//Given
		when(productMock.getOnlineDate()).thenReturn(TEST_ONLINE_DATE);

		//When
		basicPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock).setOnlineDate(TEST_ONLINE_DATE);
	}

	@Test
	public void shouldSkipPopulatingOfflineDate()
	{
		//Given
		when(productMock.getOfflineDate()).thenReturn(null);

		//When
		basicPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock, never()).setOfflineDate(any());
	}

	@Test
	public void shouldPopulateOfflineDate()
	{
		//Given
		when(productMock.getOfflineDate()).thenReturn(TEST_OFFILE_DATE);

		//When
		basicPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock).setOfflineDate(TEST_OFFILE_DATE);
	}

	@Test
	public void shouldSkipPopulatingIsbn()
	{
		//Given
		when(productMock.getIsbn()).thenReturn(null);

		//When
		basicPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock, never()).setIsbn(any());
	}

	@Test
	public void shouldPopulateIsbn()
	{
		//Given
		when(productMock.getIsbn()).thenReturn(TEST_ISBN);

		//When
		basicPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock).setIsbn(TEST_ISBN);
	}

	// TODO : [ECSC-20075] commented out code which uses obsolete attribute. Should be deleted
/*
	@Test
	public void shouldSkipPopulatingPrintIssn()
	{
		//Given
		when(productMock.getPrintIssn()).thenReturn(null);

		//When
		basicPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock, never()).setPrintIssn(any());
	}

	@Test
	public void shouldPopulatePrintIssn()
	{
		//Given
		when(productMock.getPrintIssn()).thenReturn(TEST_PRINT_ISSN);

		//When
		basicPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock).setPrintIssn(TEST_PRINT_ISSN);
	}

	@Test
	public void shouldSkipPopulatingOnlineIssn()
	{
		//Given
		when(productMock.getOnlineIssn()).thenReturn(null);

		//When
		basicPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock, never()).setOnlineIssn(any());
	}

	@Test
	public void shouldPopulateOnlineIssn()
	{
		//Given
		when(productMock.getOnlineIssn()).thenReturn(TEST_ONLINE_ISSN);

		//When
		basicPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock).setOnlineIssn(TEST_ONLINE_ISSN);
	}

	@Test
	public void shouldSkipPopulatingReleaseDate()
	{
		//Given
		when(productMock.getReleaseDate()).thenReturn(null);

		//When
		basicPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock, never()).setReleaseDate(any());
	}

	@Test
	public void shouldPopulateReleaseDate()
	{
		//Given
		when(productMock.getReleaseDate()).thenReturn(TEST_RELEASE_DATE);

		//When
		basicPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock).setReleaseDate(TEST_RELEASE_DATE);
	}
*/

	@Test
	public void shouldSkipPopulatingPrintOnDemand()
	{
		//Given
		when(productMock.getPrintOnDemand()).thenReturn(null);

		//When
		basicPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock, never()).setPrintOnDemand(any());
	}

	@Test
	public void shouldPopulatePrintOnDemand()
	{
		//Given
		when(productMock.getPrintOnDemand()).thenReturn(TEST_PRINT_ON_DEMAND);

		//When
		basicPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock).setPrintOnDemand(TEST_PRINT_ON_DEMAND);
	}

	@Test
	public void shouldSkipPopulatingTextbook()
	{
		//Given
		when(productMock.getTextbook()).thenReturn(null);

		//When
		basicPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock, never()).setTextbook(any());
	}

	@Test
	public void shouldPopulateTextbook()
	{
		//Given
		when(productMock.getTextbook()).thenReturn(TEST_TEXTBOOK);

		//When
		basicPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock).setTextbook(TEST_TEXTBOOK);
	}

	@Test
	public void shouldSkipPopulatingTaxCategory()
	{
		//Given
		when(productMock.getTaxCategory()).thenReturn(null);

		//When
		basicPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock, never()).setTaxCategory(any());
	}

	@Test
	public void shouldPopulateTaxCategory()
	{
		//Given
		when(productMock.getTaxCategory()).thenReturn(TEST_TAX_CATEGORY);

		//When
		basicPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock).setTaxCategory(TEST_TAX_CATEGORY);
	}

	@Test
	public void shouldSkipPopulatingTaxTransactionType()
	{
		//Given
		when(productMock.getTaxTransactionType()).thenReturn(null);

		//When
		basicPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock, never()).setTaxTransactionType(any());
	}

	@Test
	public void shouldPopulateTaxTransactionType()
	{
		//Given
		when(productMock.getTaxTransactionType()).thenReturn(TEST_TAX_TRANSACTION_TYPE);

		//When
		basicPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock).setTaxTransactionType(TEST_TAX_TRANSACTION_TYPE);
	}

	@Test
	public void shouldSkipPopulatingSapProductCode()
	{
		//Given
		when(productMock.getSapProductCode()).thenReturn(null);

		//When
		basicPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock, never()).setSapProductCode(any());
	}

	@Test
	public void shouldPopulateSapProductCode()
	{
		//Given
		when(productMock.getSapProductCode()).thenReturn(TEST_SAP_PRODUCT_CODE);

		//When
		basicPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock).setSapProductCode(TEST_SAP_PRODUCT_CODE);
	}

	@Test
	public void shouldSkipPopulatingPdmProductCode()
	{
		//Given
		when(productMock.getPdmProductCode()).thenReturn(null);

		//When
		basicPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock, never()).setPdmCode(any());
	}

	@Test
	public void shouldPopulatePdmProductCode()
	{
		//Given
		when(productMock.getPdmProductCode()).thenReturn(TEST_PMD_PRODUCT_CODE);

		//When
		basicPDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock).setPdmCode(TEST_PMD_PRODUCT_CODE);
	}


}

