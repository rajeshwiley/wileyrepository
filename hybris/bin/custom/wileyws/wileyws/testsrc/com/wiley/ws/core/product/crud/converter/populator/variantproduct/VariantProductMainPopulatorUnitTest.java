package com.wiley.ws.core.product.crud.converter.populator.variantproduct;

import com.wiley.core.enums.ProductEditionFormat;
import com.wiley.core.enums.WileyAssortment;
import com.wiley.core.model.WileyPurchaseOptionProductModel;
import com.wiley.ws.core.product.crud.dto.WileyVariantProductWsDto;
import com.wiley.ws.core.product.dataintegration.common.dto.LocalizedBooleanSwgDTO;
import com.wiley.ws.core.product.dataintegration.common.util.LocalizedPopulatorUtil;
import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VariantProductMainPopulatorUnitTest
{
	private static final String ID = "variantProductId";
	private static final String ISBN = "1234567890";
	private static final String EDITION_FORMAT_DIGITAL = "DIGITAL";
	private static final String EDITION_FORMAT_INCORRECT = "incorrect";
	private static final Boolean CUSTOM = true;

	@InjectMocks
	private VariantProductMainPopulator testInstance;

	@Mock
	private LocalizedPopulatorUtil localizedPopulatorUtil;
	@Mock
	private WileyPurchaseOptionProductModel variantProductModel;
	private List<LocalizedBooleanSwgDTO> printOnDemand;
	private WileyVariantProductWsDto variantProductDto;

	@Before
	public void setUp()
	{
		variantProductDto = new WileyVariantProductWsDto();
		variantProductDto.setId(ID);
		variantProductDto.setIsbn10(ISBN);
		variantProductDto.setEditionFormat(EDITION_FORMAT_DIGITAL);
		variantProductDto.setCustom(CUSTOM);
		printOnDemand = Arrays.asList(givenLocalizedBooleanDto("en_US", true));
		variantProductDto.setPrintOnDemand(printOnDemand);
	}

	private LocalizedBooleanSwgDTO givenLocalizedBooleanDto(final String locale, final Boolean value)
	{
		LocalizedBooleanSwgDTO localizedBoolean = new LocalizedBooleanSwgDTO();
		localizedBoolean.setLocale(locale);
		localizedBoolean.setValue(value);
		return localizedBoolean;
	}

	@Test
	public void shouldPopulateMainVariantProductFields()
	{
		testInstance.populate(variantProductDto, variantProductModel);

		verify(variantProductModel).setCode(ID);
		verify(variantProductModel).setIsbn(ISBN);
		verify(variantProductModel).setEditionFormat(ProductEditionFormat.DIGITAL);
		verify(variantProductModel).setCustom(CUSTOM);
		verify(variantProductModel).setAssortments(eq(Collections.singletonList(WileyAssortment.B2CASSORTMENT)));
		verify(localizedPopulatorUtil).populateBooleans(eq(printOnDemand), any());
	}

	@Test
	public void shouldHandleCaseWhenEditionFormatIsIncorrect()
	{
		variantProductDto.setEditionFormat(EDITION_FORMAT_INCORRECT);

		testInstance.populate(variantProductDto, variantProductModel);

		verify(variantProductModel).setEditionFormat(null);
	}
}
