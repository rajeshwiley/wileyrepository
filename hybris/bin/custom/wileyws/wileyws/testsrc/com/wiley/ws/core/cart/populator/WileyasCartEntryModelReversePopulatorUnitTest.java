package com.wiley.ws.core.cart.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.util.PriceValue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyExternalDiscountModel;
import com.wiley.core.model.WileyProductModel;
import com.wiley.core.product.WileyProductService;
import com.wiley.ws.core.cart.dto.CreateCartEntryRequestWsDTO;
import com.wiley.integrations.order.dto.DiscountValueWsDTO;
import com.wiley.integrations.order.dto.PriceValueWsDTO;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyasCartEntryModelReversePopulatorUnitTest
{
	private static final String PRODUCT_CODE = "test_product_code";
	private static final String CURRENCY_ISO = "USD";
	private static final Double DISCOUNT_VALUE = 50.0;
	private static final Double PRICE_VALUE = 40.0;
	private static final String DISCOUNT_CODE = "test_discount_code";
	private static final Date DELIVERY_DATE = new Date();
	private static final String ADDITIONAL_INFO = "test_additional_info";
	private static final Integer QUANTITY = 3;
	private static final String BUSINESS_KEY = "test_business_key";
	private static final String BUSINESS_ITEM_ID = "test_business_item_id";

	@Spy
	@InjectMocks
	private WileyasCartEntryModelReversePopulator testInstance;

	@Mock
	private CartEntryModel targetMock;

	@Mock
	private CreateCartEntryRequestWsDTO sourceMock;

	@Mock
	private WileyProductService productService;

	@Mock
	private Converter<PriceValueWsDTO, PriceValue> wileyasPriceValueReverseConverter;

	@Mock
	private Converter<DiscountValueWsDTO, WileyExternalDiscountModel> wileyExternalDiscountReverseWsConverter;

	@Mock
	private WileyProductModel productMock;

	@Mock
	private WileyExternalDiscountModel externalDiscountsTargetMock;
	
	@Mock
	private CurrencyModel currencyMock;

	@Mock
	private CommonI18NService commonI18NServiceMock;
	
	private PriceValueWsDTO priceSource;
	private DiscountValueWsDTO discountSource;
	private PriceValue pricesTarget;

	@Before
	public void prepare()
	{
		testInstance.setWileyasPriceValueReverseConverter(wileyasPriceValueReverseConverter);
		testInstance.setWileyExternalDiscountReverseWsConverter(wileyExternalDiscountReverseWsConverter);

		priceSource = new PriceValueWsDTO();
		priceSource.setCurrency(CURRENCY_ISO);
		priceSource.setValue(PRICE_VALUE);

		discountSource = new DiscountValueWsDTO();
		discountSource.setCode(DISCOUNT_CODE);
		discountSource.setCurrency(CURRENCY_ISO);
		discountSource.setValue(DISCOUNT_VALUE);

		pricesTarget = new PriceValue(CURRENCY_ISO, PRICE_VALUE, false);
		
		when(externalDiscountsTargetMock.getGuid()).thenReturn(DISCOUNT_CODE);
		when(externalDiscountsTargetMock.getValue()).thenReturn(DISCOUNT_VALUE);
		when(externalDiscountsTargetMock.getCurrency()).thenReturn(currencyMock);

		productMock.setCode(PRODUCT_CODE);

		when(sourceMock.getProductCode()).thenReturn(PRODUCT_CODE);
		when(productService.getProductForCode(sourceMock.getProductCode())).thenReturn(productMock);
		when(commonI18NServiceMock.getCurrency(CURRENCY_ISO)).thenReturn(currencyMock);
	}

	@Test
	public void testCheckAllParameters()
	{
		// given
		optionalMockRules();

		//when
		testInstance.populate(sourceMock, targetMock);

		// then
		verifyMandatoryParameters();
		verifyOptionalParametersIsNotEmpty();
	}

	@Test
	public void testCheckOnlyMandatoryParameters()
	{
		// given
		when(sourceMock.getBusinessItemId()).thenReturn(null);
		when(sourceMock.getQuantity()).thenReturn(null);

		//when
		testInstance.populate(sourceMock, targetMock);

		// then
		verifyMandatoryParameters();
		verifyOptionalParametersIsEmpty();
	}

	@Test(expected = NullPointerException.class)
	public void testDtoArgumentIsNull()
	{
		testInstance.populate(null, targetMock);
	}

	@Test(expected = NullPointerException.class)
	public void testModelArgumentIsNull()
	{
		testInstance.populate(sourceMock, null);
	}

	@Test(expected = NullPointerException.class)
	public void testBothArgumentsAreNull()
	{
		testInstance.populate(null, null);
	}

	private void optionalMockRules()
	{
		when(sourceMock.getAdditionalInfo()).thenReturn(ADDITIONAL_INFO);
		when(sourceMock.getBusinessItemId()).thenReturn(BUSINESS_ITEM_ID);
		when(sourceMock.getBusinessKey()).thenReturn(BUSINESS_KEY);
		when(sourceMock.getDeliveryDate()).thenReturn(DELIVERY_DATE);
		when(sourceMock.getExternalDiscounts()).thenReturn(Arrays.asList(discountSource));
		when(sourceMock.getExternalPrices()).thenReturn(Arrays.asList(priceSource));
		when(sourceMock.getQuantity()).thenReturn(QUANTITY);

		when(wileyasPriceValueReverseConverter.convertAll(Arrays.asList(priceSource))).thenReturn(Arrays.asList(pricesTarget));
		when(wileyExternalDiscountReverseWsConverter.convertAll(Arrays.asList(discountSource)))
				.thenReturn(Arrays.asList(externalDiscountsTargetMock));
	}

	private void verifyOptionalParametersIsNotEmpty()
	{
		verify(targetMock).setExternalPriceValues(Arrays.asList(pricesTarget));
		verify(targetMock).setExternalDiscounts(Arrays.asList(externalDiscountsTargetMock));
		verify(targetMock).setBusinessKey(BUSINESS_KEY);
		verify(targetMock).setOriginalBusinessKey(BUSINESS_KEY);
		verify(targetMock).setNamedDeliveryDate(DELIVERY_DATE);
		verify(targetMock).setAdditionalInfo(ADDITIONAL_INFO);
		verify(targetMock).setQuantity(QUANTITY.longValue());
	}

	private void verifyOptionalParametersIsEmpty()
	{
		verify(targetMock).setExternalPriceValues(new ArrayList<>());
		verify(targetMock).setExternalDiscounts(new ArrayList<>());
		verify(targetMock).setBusinessKey(null);
		verify(targetMock).setOriginalBusinessKey(null);
		verify(targetMock).setNamedDeliveryDate(null);
		verify(targetMock).setAdditionalInfo(null);
		verify(targetMock).setQuantity(1L);
	}

	private void verifyMandatoryParameters()
	{
		verify(targetMock).setProduct(productMock);
	}
}
