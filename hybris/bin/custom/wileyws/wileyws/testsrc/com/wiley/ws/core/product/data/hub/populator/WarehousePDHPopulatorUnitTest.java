package com.wiley.ws.core.product.data.hub.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.WarehouseService;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.wiley.ws.core.product.data.hub.dto.ProductSwgDTO;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Test for {@link WarehousePDHPopulator}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WarehousePDHPopulatorUnitTest
{
	private static final String TEST_DEFAULT_WAREHOUSE = "default";
	private static final String TEST_DEFAULT_WAREHOUSE_WHITESPACED = "  ";

	@InjectMocks
	private WarehousePDHPopulator warehousePDHPopulator;

	@Mock
	private WarehouseService warehouseServiceMock;

	@Mock
	private ProductSwgDTO productMock;

	@Mock
	private ProductModel productModelMock;

	@Mock
	private WarehouseModel warehouseModelMock;

	@Test
	public void shouldSkipPopulatingDueToDefaultWarehouseIsNull()
	{
		//Given
		when(productMock.getDefaultWarehouse()).thenReturn(null);

		//When
		warehousePDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock, never()).setDefaultWarehouse(any());
	}

	@Test
	public void shouldSetNullDueToDefaultWarehouseIsEmptyString()
	{
		//Given
		when(productMock.getDefaultWarehouse()).thenReturn(StringUtils.EMPTY);

		//When
		warehousePDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock).setDefaultWarehouse(isNull(WarehouseModel.class));
	}


	@Test
	public void shouldSetNullDueToDefaultWarehouseIsWhitespaced()
	{
		//Given
		when(productMock.getDefaultWarehouse()).thenReturn(TEST_DEFAULT_WAREHOUSE_WHITESPACED);

		//When
		warehousePDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock).setDefaultWarehouse(isNull(WarehouseModel.class));
	}

	@Test
	public void shouldSetWarehouse()
	{
		//Given
		when(productMock.getDefaultWarehouse()).thenReturn(TEST_DEFAULT_WAREHOUSE);
		when(warehouseServiceMock.getWarehouseForCode(TEST_DEFAULT_WAREHOUSE)).thenReturn(warehouseModelMock);

		//When
		warehousePDHPopulator.populate(productMock, productModelMock);

		//Then
		verify(productModelMock).setDefaultWarehouse(warehouseModelMock);
	}


}
