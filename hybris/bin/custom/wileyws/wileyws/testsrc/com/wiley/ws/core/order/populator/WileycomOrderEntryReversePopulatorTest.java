package com.wiley.ws.core.order.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.product.UnitService;
import static org.mockito.Matchers.anyDouble;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.product.WileyProductService;
import com.wiley.ws.core.order.dto.OrderUpdateEntryRequestWsDTO;
import com.wiley.ws.core.order.service.WileycomExternalDiscountApplier;

import static com.wiley.core.constants.WileyCoreConstants.ONLINE_CATALOG_VERSION;


/**
 * Author Herman_Chukhrai (EPAM)
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileycomOrderEntryReversePopulatorTest
{
	@InjectMocks
	private WileycomOrderEntryReversePopulator populator = new WileycomOrderEntryReversePopulator();

	private OrderUpdateEntryRequestWsDTO nonEmptyOrderEntryDTO = new OrderUpdateEntryRequestWsDTO();
	private OrderUpdateEntryRequestWsDTO emptyOrderEntryDTO = new OrderUpdateEntryRequestWsDTO();

	@Mock
	private CatalogVersionService catalogVersionService;

	@Mock
	private WileyProductService productService;

	@Mock
	private OrderEntryModel orderEntryModel;
	@Mock
	private ProductModel productModel;
	@Mock
	private UnitModel unitModel;
	@Mock
	private CatalogVersionModel catalogVersionModel;
	@Mock
	private UnitService unitService;
	@Mock
	private WileycomExternalDiscountApplier wileycomExternalDiscountApplier;

	private static final Double BASE_PRICE = 1.0;
	private static final Double TOTAL_PRICE = 2.0;
	private static final Integer QUANTITY = 2;
	private static final String ISBN = "ISBN";
	private static final String SAP_PRODUCT_CODE = "SAP_PRODUCT_CODE";
	private static final String DEFAULT_UNIT_PIECES = "pieces";

	@Before
	public void setUp() throws Exception
	{
		//given
		initOrderEntryDTO();
		when(catalogVersionService.getCatalogVersion(anyString(), eq(ONLINE_CATALOG_VERSION))).thenReturn(catalogVersionModel);
		when(productService.getProductForIsbn(ISBN, catalogVersionModel)).thenReturn(productModel);
		when(unitService.getUnitForCode(DEFAULT_UNIT_PIECES)).thenReturn(unitModel);
	}

	private void initOrderEntryDTO()
	{
		nonEmptyOrderEntryDTO.setBasePrice(BASE_PRICE);
		nonEmptyOrderEntryDTO.setTotalPrice(TOTAL_PRICE);
		nonEmptyOrderEntryDTO.setQuantity(QUANTITY);
		nonEmptyOrderEntryDTO.setIsbn(ISBN);
		nonEmptyOrderEntryDTO.setSapProductCode(SAP_PRODUCT_CODE);
	}

	@Test
	public void shouldPopulateNonNullFields() throws Exception
	{
		//when
		populator.populate(nonEmptyOrderEntryDTO, orderEntryModel);
		//then
		verify(orderEntryModel).setBasePrice(BASE_PRICE);
		verify(orderEntryModel).setTotalPrice(TOTAL_PRICE);
		verify(orderEntryModel).setQuantity(new Long(QUANTITY));
		verify(orderEntryModel).setProduct(productModel);
		verify(orderEntryModel).setUnit(unitModel);
	}

	@Test
	public void shouldNotPopulateNullFields() throws Exception
	{
		//when
		populator.populate(emptyOrderEntryDTO, orderEntryModel);
		//then
		verify(orderEntryModel, never()).setBasePrice(anyDouble());
		verify(orderEntryModel, never()).setTotalPrice(anyDouble());
		verify(orderEntryModel, never()).setQuantity(anyLong());
		verify(orderEntryModel, never()).setProduct(Mockito.any());
	}
}