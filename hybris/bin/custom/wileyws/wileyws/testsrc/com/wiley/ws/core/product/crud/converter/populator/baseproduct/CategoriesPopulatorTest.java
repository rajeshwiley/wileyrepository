package com.wiley.ws.core.product.crud.converter.populator.baseproduct;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.category.WileyCategoryService;
import com.wiley.core.model.WileyProductModel;
import com.wiley.ws.core.product.crud.dto.WileyProductWsDto;
import com.wiley.ws.core.product.dataintegration.common.exception.DataImportValidationErrorsException;
import com.wiley.ws.core.product.crud.facade.WileyProductCrudCatalogVersionProvider;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CategoriesPopulatorTest
{
	private static final String SUBJECT_CATEGORY_ID = "subjectId";
	private static final String BRAND_CATEGORY_ID = "brandId";
	private static final String SERIE_CATEGORY_ID = "serieId";
	private static final String COURSE_CATEGORY_ID = "courseId";
	private CategoriesPopulator testInstance;
	@Mock
	private WileyCategoryService wileyCategoryService;
	@Mock
	private WileyProductCrudCatalogVersionProvider wileyProductCrudCatalogVersionProvider;

	@Mock
	private CatalogVersionModel mockCatalogVersion;
	@Mock
	private WileyProductModel wileyProduct;
	@Mock
	private WileyProductWsDto testDto;


	@Before
	public void setUp() throws Exception
	{
		testInstance = new CategoriesPopulator(wileyCategoryService, wileyProductCrudCatalogVersionProvider);
		when(wileyProductCrudCatalogVersionProvider.getCatalogVersion()).thenReturn(mockCatalogVersion);
	}

	@Test(expected = DataImportValidationErrorsException.class)
	public void shouldThrowExceptionIfNoCategoryWithSuchId()
	{
		//Given
		final WileyProductWsDto testDto = mock(WileyProductWsDto.class);
		final WileyProductModel wileyProduct = mock(WileyProductModel.class);
		final String notExistingCategoryId = "any";
		givenSubjectIds(testDto, notExistingCategoryId);
		when(wileyCategoryService.getCategoryForCode(mockCatalogVersion, notExistingCategoryId))
				.thenThrow(UnknownIdentifierException.class);
		//When
		testInstance.populate(testDto, wileyProduct);
	}

	@Test
	public void shouldPopulateSubjects()
	{
		//Given
		final String expectedCategoryCode = "cubj_id";
		final CategoryModel expectedCategory = givenCategory(expectedCategoryCode);

		givenSubjectIds(testDto, expectedCategoryCode);

		//When
		testInstance.populate(testDto, wileyProduct);
		//Then
		verify(wileyProduct).setSubjects(Collections.singletonList(expectedCategory));
	}

	@Test
	public void shouldPopulateCourses()
	{
		//Given
		final String expectedCategoryCode = "cubj_id";
		final CategoryModel expectedCategory = givenCategory(expectedCategoryCode);

		givenCourseIds(testDto, expectedCategoryCode);

		//When
		testInstance.populate(testDto, wileyProduct);
		//Then
		verify(wileyProduct).setCourses(Collections.singletonList(expectedCategory));
	}

	@Test
	public void shouldPopulateBrands()
	{
		//Given
		final String expectedCategoryCode = "cubj_id";
		final CategoryModel expectedCategory = givenCategory(expectedCategoryCode);

		givenBrandIds(testDto, expectedCategoryCode);

		//When
		testInstance.populate(testDto, wileyProduct);
		//Then
		verify(wileyProduct).setBrands(Collections.singletonList(expectedCategory));
	}

	@Test
	public void shouldPopulateSeries()
	{
		//Given
		final String expectedCategoryCode = "cubj_id";
		final CategoryModel expectedCategory = givenCategory(expectedCategoryCode);

		givenSeriesIds(testDto, expectedCategoryCode);
		//When
		testInstance.populate(testDto, wileyProduct);
		//Then
		verify(wileyProduct).setSeries(Collections.singletonList(expectedCategory));
	}

	@Test
	public void shouldPopulateSuperCategoriesInCorrectOrder()
	{
		//Given
		final CategoryModel subjectCategory = givenCategory(SUBJECT_CATEGORY_ID);
		final CategoryModel brandCategory = givenCategory(BRAND_CATEGORY_ID);
		final CategoryModel serieCategory = givenCategory(SERIE_CATEGORY_ID);
		final CategoryModel courseCategory = givenCategory(COURSE_CATEGORY_ID);

		givenSubjectIds(testDto, SUBJECT_CATEGORY_ID);
		givenBrandIds(testDto, BRAND_CATEGORY_ID);
		givenSeriesIds(testDto, SERIE_CATEGORY_ID);
		givenCourseIds(testDto, COURSE_CATEGORY_ID);

		//When
		testInstance.populate(testDto, wileyProduct);
		
		//Then
		InOrder inOrder = Mockito.inOrder(wileyProduct);
		inOrder.verify(wileyProduct).setSubjects(Collections.singletonList(subjectCategory));
		inOrder.verify(wileyProduct).setBrands(Collections.singletonList(brandCategory));
		inOrder.verify(wileyProduct).setSeries(Collections.singletonList(serieCategory));
		inOrder.verify(wileyProduct).setCourses(Collections.singletonList(courseCategory));
	}

	private void givenSubjectIds(final WileyProductWsDto dto, final String... subjectIds)
	{
		when(dto.getSubjectIds()).thenReturn(Arrays.asList(subjectIds));
	}

	private void givenCourseIds(final WileyProductWsDto dto, final String... courseIds)
	{
		when(dto.getCourseIds()).thenReturn(Arrays.asList(courseIds));
	}

	private void givenBrandIds(final WileyProductWsDto dto, final String... brandIds)
	{
		when(dto.getBrandIds()).thenReturn(Arrays.asList(brandIds));
	}

	private void givenSeriesIds(final WileyProductWsDto dto, final String... seriesIds)
	{
		when(dto.getSeriesIds()).thenReturn(Arrays.asList(seriesIds));
	}

	private CategoryModel givenCategory(final String categoryCode)
	{
		final CategoryModel categoryModelMock = mock(CategoryModel.class);
		when(wileyCategoryService.getCategoryForCode(mockCatalogVersion, categoryCode)).thenReturn(categoryModelMock);
		return categoryModelMock;
	}
}