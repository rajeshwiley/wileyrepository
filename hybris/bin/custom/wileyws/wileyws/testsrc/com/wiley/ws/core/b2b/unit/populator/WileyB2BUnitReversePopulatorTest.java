package com.wiley.ws.core.b2b.unit.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.apache.commons.lang.LocaleUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.address.WileyAddressService;
import com.wiley.ws.core.b2b.unit.dto.AddressSwgDTO;
import com.wiley.ws.core.b2b.unit.dto.B2BUnitSwgDTO;
import com.wiley.ws.core.b2b.unit.dto.LocalizedStringSwgDTO;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyB2BUnitReversePopulatorTest
{
	private static final String LOC_EN = "en";
	private static final String NAME_EN_VAL = "nameEn";
	private static final String LOC_FR = "fr";
	private static final String NAME_FR_VAL = "nameFr";
	private static final String SAP_ACCOUNT_NUMBER = "123";
	private static final String SALES_REP_CONTACT_NAME = "saleRepContactName";
	private static final String SALES_REP_CONTACT_EMAIL = "saleRepContactEmail";
	private static final String SALES_REP_CONTACT_PHONE = "saleRepContactPhone";
	private static final String SHIPPING_ADDRESS_ID = "sa1";
	private static final String BILLING_ADDRESS_ID = "ba1";
	private static final String NEW_ADDRESS_ID = "newAddressId";
	private static final Boolean ACTIVE = true;
	public static final String OWNER_ID = "123456789";

	@InjectMocks
	private WileyB2BUnitReversePopulator testInstance = new WileyB2BUnitReversePopulator();

	private B2BUnitSwgDTO notEmptyB2BUnitSwgDTO = new B2BUnitSwgDTO();
	private B2BUnitSwgDTO emptyB2BUnitSwgDTO = new B2BUnitSwgDTO();
	private AddressSwgDTO newAddress = new AddressSwgDTO();
	private AddressSwgDTO shippingAddress = new AddressSwgDTO();
	private AddressSwgDTO billingAddress = new AddressSwgDTO();
	private List<LocalizedStringSwgDTO> localizedNames;

	@Mock
	private WileyAddressService mockWileyAddressService;
	@Mock
	private Populator<AddressSwgDTO, AddressModel> mockAddressReversePopulator;
	@Mock
	private ModelService mockModelService;
	@Mock
	private EventService mockEventService;
	@Mock
	private B2BUnitModel mockB2BUnitModel;
	@Mock
	private AddressModel mockNewAddressModel;
	@Mock
	private AddressModel mockShippingAddressModel;
	@Mock
	private AddressModel mockBillingAddressModel;


	@Before
	public void setUp()
	{
		initNotEmptyB2BUnitSwgDTO();
		initAddresses();
		when(mockB2BUnitModel.getSapAccountNumber()).thenReturn(SAP_ACCOUNT_NUMBER);
		when(mockB2BUnitModel.getPk()).thenReturn(PK.fromLong(Long.parseLong(OWNER_ID)));
	}

	private void initNotEmptyB2BUnitSwgDTO()
	{
		localizedNames = Arrays.asList(givenLocalizedName(LOC_EN, NAME_EN_VAL), givenLocalizedName(LOC_FR, NAME_FR_VAL));
		notEmptyB2BUnitSwgDTO.setName(localizedNames);
		notEmptyB2BUnitSwgDTO.setSapAccountNumber(SAP_ACCOUNT_NUMBER);
		notEmptyB2BUnitSwgDTO.setSalesRepContactName(SALES_REP_CONTACT_NAME);
		notEmptyB2BUnitSwgDTO.setSalesRepContactEmail(SALES_REP_CONTACT_EMAIL);
		notEmptyB2BUnitSwgDTO.setSalesRepContactPhone(SALES_REP_CONTACT_PHONE);
		notEmptyB2BUnitSwgDTO.setActive(ACTIVE);
		notEmptyB2BUnitSwgDTO.setShippingAddresses(Arrays.asList(shippingAddress));
		notEmptyB2BUnitSwgDTO.setSoldToAddress(billingAddress);
	}

	private void initAddresses()
	{
		shippingAddress.setAddressId(SHIPPING_ADDRESS_ID);
		billingAddress.setAddressId(BILLING_ADDRESS_ID);
		newAddress.setAddressId(NEW_ADDRESS_ID);

		when(mockWileyAddressService.getAddressByExternalIdAndOwner(SHIPPING_ADDRESS_ID, OWNER_ID)).thenReturn(
				Optional.of(mockShippingAddressModel));
		when(mockWileyAddressService.getAddressByExternalIdAndOwner(BILLING_ADDRESS_ID, OWNER_ID)).thenReturn(
				Optional.of(mockBillingAddressModel));
		when(mockWileyAddressService.getAddressByExternalIdAndOwner(NEW_ADDRESS_ID, OWNER_ID)).thenReturn(Optional.empty());
		when(mockModelService.create(AddressModel.class)).thenReturn(mockNewAddressModel);
	}

	@Test
	public void shouldSetLocNameIfNotNull()
	{
		testInstance.populate(notEmptyB2BUnitSwgDTO, mockB2BUnitModel);
		verify(mockB2BUnitModel).setLocName(NAME_EN_VAL, LocaleUtils.toLocale(LOC_EN));
		verify(mockB2BUnitModel).setLocName(NAME_FR_VAL, LocaleUtils.toLocale(LOC_FR));
	}

	@Test
	public void shouldNotSetLocNameIfNull()
	{
		testInstance.populate(emptyB2BUnitSwgDTO, mockB2BUnitModel);
		verify(mockB2BUnitModel, never()).setLocName(anyString(), any(Locale.class));
		verify(mockB2BUnitModel, never()).setLocName(anyString(), any(Locale.class));
	}

	@Test
	public void shouldSetSalesRepContactNameIfNotNull()
	{
		testInstance.populate(notEmptyB2BUnitSwgDTO, mockB2BUnitModel);
		verify(mockB2BUnitModel).setSapSalesRepContactName(SALES_REP_CONTACT_NAME);
	}

	@Test
	public void shouldNotSetSalesRepContactNameIfNull()
	{
		testInstance.populate(emptyB2BUnitSwgDTO, mockB2BUnitModel);
		verify(mockB2BUnitModel, never()).setSapSalesRepContactName(anyString());
	}

	@Test
	public void shouldSetSapAccountNumberIfNotNull()
	{
		testInstance.populate(notEmptyB2BUnitSwgDTO, mockB2BUnitModel);
		verify(mockB2BUnitModel).setSapAccountNumber(SAP_ACCOUNT_NUMBER);
	}

	@Test
	public void shouldNotSetSapAccountNumberIfNull()
	{
		testInstance.populate(emptyB2BUnitSwgDTO, mockB2BUnitModel);
		verify(mockB2BUnitModel, never()).setSapAccountNumber(anyString());
	}


	@Test
	public void shouldSetSalesRepContactEmailIfNotNull()
	{
		testInstance.populate(notEmptyB2BUnitSwgDTO, mockB2BUnitModel);
		verify(mockB2BUnitModel).setSapSalesRepContactEmail(SALES_REP_CONTACT_EMAIL);
	}

	@Test
	public void shouldNotSetSalesRepContactEmailIfNull()
	{
		testInstance.populate(emptyB2BUnitSwgDTO, mockB2BUnitModel);
		verify(mockB2BUnitModel, never()).setSapSalesRepContactEmail(anyString());
	}

	@Test
	public void shouldSetSalesRepContactPhoneIfNotNull()
	{
		testInstance.populate(notEmptyB2BUnitSwgDTO, mockB2BUnitModel);
		verify(mockB2BUnitModel).setSapSalesRepContactPhone(SALES_REP_CONTACT_PHONE);
	}

	@Test
	public void shouldNotSetSalesRepContactPhoneIfNull()
	{
		testInstance.populate(emptyB2BUnitSwgDTO, mockB2BUnitModel);
		verify(mockB2BUnitModel, never()).setSapSalesRepContactPhone(anyString());
	}

	@Test
	public void shouldSetActiveIfNotNull()
	{
		testInstance.populate(notEmptyB2BUnitSwgDTO, mockB2BUnitModel);
		verify(mockB2BUnitModel).setActive(ACTIVE);
	}

	@Test
	public void shouldNotSetActiveIfNull()
	{
		testInstance.populate(emptyB2BUnitSwgDTO, mockB2BUnitModel);
		verify(mockB2BUnitModel, never()).setActive(anyBoolean());
	}

	@Test
	public void shouldUpdateShippingAddressesIfNotNull()
	{
		testInstance.populate(notEmptyB2BUnitSwgDTO, mockB2BUnitModel);
		verify(mockAddressReversePopulator).populate(shippingAddress, mockShippingAddressModel);
		verify(mockShippingAddressModel).setShippingAddress(true);
		verify(mockShippingAddressModel).setBillingAddress(false);
	}

	@Test
	public void shouldSetShippingAddressInB2BUnitIfItIsNull()
	{
		when(mockB2BUnitModel.getShippingAddress()).thenReturn(null);

		testInstance.populate(notEmptyB2BUnitSwgDTO, mockB2BUnitModel);
		verify(mockB2BUnitModel).setShippingAddress(mockShippingAddressModel);
	}

	@Test
	public void shouldSetShippingAddressInB2BUnitIfItIsNotInFeed()
	{
		notEmptyB2BUnitSwgDTO.setShippingAddresses(Arrays.asList(newAddress));
		when(mockB2BUnitModel.getShippingAddress()).thenReturn(mockShippingAddressModel);

		testInstance.populate(notEmptyB2BUnitSwgDTO, mockB2BUnitModel);
		verify(mockB2BUnitModel).setShippingAddress(mockNewAddressModel);
	}

	@Test
	public void shouldRemoveShippingAddressInB2BUnitIfItIsNotInFeed()
	{
		notEmptyB2BUnitSwgDTO.setShippingAddresses(Arrays.asList(newAddress));
		when(mockB2BUnitModel.getShippingAddress()).thenReturn(mockShippingAddressModel);
		when(mockB2BUnitModel.getShippingAddresses()).thenReturn(Arrays.asList(mockShippingAddressModel));

		testInstance.populate(notEmptyB2BUnitSwgDTO, mockB2BUnitModel);
		verify(mockModelService).removeAll(Arrays.asList(mockShippingAddressModel));
	}

	@Test
	public void shouldUpdateBillingAddressesIfNotNull()
	{
		testInstance.populate(notEmptyB2BUnitSwgDTO, mockB2BUnitModel);
		verify(mockAddressReversePopulator).populate(billingAddress, mockBillingAddressModel);
		verify(mockBillingAddressModel).setShippingAddress(false);
		verify(mockBillingAddressModel).setBillingAddress(true);
	}

	@Test
	public void shouldSetNewBillingAddressInB2BUnit()
	{
		testInstance.populate(notEmptyB2BUnitSwgDTO, mockB2BUnitModel);
		verify(mockB2BUnitModel).setBillingAddress(mockBillingAddressModel);
	}

	@Test
	public void shouldRemoveBillingAddressInB2BUnitIfItIsNotInFeed()
	{
		notEmptyB2BUnitSwgDTO.setSoldToAddress(newAddress);
		when(mockB2BUnitModel.getBillingAddress()).thenReturn(mockBillingAddressModel);

		testInstance.populate(notEmptyB2BUnitSwgDTO, mockB2BUnitModel);
		verify(mockModelService).remove(mockBillingAddressModel);
	}

	private LocalizedStringSwgDTO givenLocalizedName(final String loc, final String val)
	{
		LocalizedStringSwgDTO name = new LocalizedStringSwgDTO();
		name.setLoc(loc);
		name.setVal(val);
		return name;
	}
}
