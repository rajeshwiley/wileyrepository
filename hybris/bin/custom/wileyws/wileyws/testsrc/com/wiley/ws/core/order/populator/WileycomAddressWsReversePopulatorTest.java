package com.wiley.ws.core.order.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.i18n.WileyCommonI18NService;
import com.wiley.ws.core.order.dto.OrderAddressWsDTO;


/**
 * Author Herman_Chukhrai (EPAM)
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileycomAddressWsReversePopulatorTest
{
	private static final String ADDRESS_ID = "1234";
	private static final String COUNTRY = "PL";
	private static final String STATE = "KR";
	private static final String CITY = "Krakow";
	private static final String LINE_1 = "line1";
	private static final String LINE_2 = "line1";
	private static final String PHONE_NUMBER = "911";
	private static final String POST_CODE = "098-221";

	@InjectMocks
	private WileycomAddressWsReversePopulator testInstance;

	private OrderAddressWsDTO notEmptyAddressDTO = new OrderAddressWsDTO();
	private OrderAddressWsDTO emptyAddressDTOWithId = new OrderAddressWsDTO();
	private OrderAddressWsDTO emptyAddressDTO = new OrderAddressWsDTO();

	@Mock
	private AddressModel mockAddressModel;
	@Mock
	private CommonI18NService mockCommonI18NService;
	@Mock
	private WileyCommonI18NService wileyCommonI18NService;
	@Mock
	private CountryModel mockCountryModel;
	@Mock
	private RegionModel mockRegionModel;


	@Before
	public void setUp()
	{
		//given
		notEmptyAddressDTO.setAddressId(ADDRESS_ID);
		notEmptyAddressDTO.setCity(CITY);
		notEmptyAddressDTO.setCountry(COUNTRY);
		notEmptyAddressDTO.setLine1(LINE_1);
		notEmptyAddressDTO.setLine2(LINE_2);
		notEmptyAddressDTO.setPhoneNumber(PHONE_NUMBER);
		notEmptyAddressDTO.setPostcode(POST_CODE);
		notEmptyAddressDTO.setState(STATE);

		emptyAddressDTOWithId.setAddressId(ADDRESS_ID);

		when(mockCountryModel.getIsocode()).thenReturn(COUNTRY);

		when(mockCommonI18NService.getCountry(COUNTRY)).thenReturn(mockCountryModel);
		when(mockAddressModel.getCountry()).thenReturn(mockCountryModel);
		when(wileyCommonI18NService.getRegionForShortCode(mockCountryModel, STATE)).thenReturn(mockRegionModel);
	}


	@Test
	public void shouldPopulateNonNullFields() throws Exception
	{
		//when
		testInstance.populate(notEmptyAddressDTO, mockAddressModel);

		//then
		verify(mockAddressModel).setPostalcode(POST_CODE);
		verify(mockAddressModel).setCountry(mockCountryModel);
		verify(mockAddressModel).setRegion(mockRegionModel);
		verify(mockAddressModel).setTown(CITY);
		verify(mockAddressModel).setStreetname(LINE_1);
		verify(mockAddressModel).setStreetnumber(LINE_2);
		verify(mockAddressModel).setPhone1(PHONE_NUMBER);
	}

	@Test
	public void shouldNotPopulateNullFields() throws Exception
	{
		//when
		testInstance.populate(emptyAddressDTOWithId, mockAddressModel);

		//then
		verify(mockAddressModel, never()).setPostalcode(POST_CODE);
		verify(mockAddressModel, never()).setCountry(mockCountryModel);
		verify(mockAddressModel, never()).setRegion(mockRegionModel);
		verify(mockAddressModel, never()).setTown(CITY);
		verify(mockAddressModel, never()).setStreetname(LINE_1);
		verify(mockAddressModel, never()).setStreetnumber(LINE_2);
		verify(mockAddressModel, never()).setPhone1(PHONE_NUMBER);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowAnExceptionIfCountryIsNull()
	{
		when(mockAddressModel.getCountry()).thenReturn(null);
		testInstance.populate(notEmptyAddressDTO, mockAddressModel);
	}

	@Test(expected = UnknownIdentifierException.class)
	public void shouldThrowAnExceptionIfRegionNotFound()
	{
		when(wileyCommonI18NService.getRegionForShortCode(mockCountryModel, STATE)).thenThrow(
				UnknownIdentifierException.class);
		testInstance.populate(notEmptyAddressDTO, mockAddressModel);
	}

	@Test
	public void shouldResetRegionIfPreviousRegionNotApplicableForCountry()
	{
		//given
		notEmptyAddressDTO.setState(null);
		when(mockAddressModel.getRegion()).thenReturn(mockRegionModel);
		when(mockRegionModel.getIsocode()).thenReturn(STATE);
		when(wileyCommonI18NService.getRegionForShortCode(mockCountryModel, STATE)).thenThrow(
				UnknownIdentifierException.class);

		//when
		testInstance.populate(notEmptyAddressDTO, mockAddressModel);

		//than
		verify(mockAddressModel).setRegion((RegionModel) isNull());
	}

	@Test
	public void shouldResetRegionIfPreviousRegionIsNull()
	{
		//given
		notEmptyAddressDTO.setState(null);
		when(mockAddressModel.getRegion()).thenReturn(null);

		//when
		testInstance.populate(notEmptyAddressDTO, mockAddressModel);

		//than
		verify(mockAddressModel, never()).setRegion((RegionModel) isNull());
	}
}