package com.wiley.ws.core.product.dataintegration.common.util;

import de.hybris.bootstrap.annotations.UnitTest;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.function.BiConsumer;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.ws.core.product.dataintegration.common.dto.LocalizedBooleanSwgDTO;
import com.wiley.ws.core.product.dataintegration.common.dto.LocalizedLongStringSwgDTO;
import com.wiley.ws.core.product.dataintegration.common.dto.LocalizedStringSwgDTO;
import com.wiley.ws.core.product.dataintegration.common.util.impl.LocalizedPopulatorUtilImpl;

import static org.apache.commons.lang3.LocaleUtils.toLocale;


/**
 * Test for {@link LocalizedPopulatorUtilImpl}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class LocalizedPopulatorUtilImplUnitTest
{
	private static final String STRING_VALUE = "value";
	private static final Boolean BOOLEAN_VALUE = true;
	private static final String LOCALE_ISOCODE_US = "en_US";
	private static final Locale LOCALE_US = toLocale(LOCALE_ISOCODE_US);

	@InjectMocks
	private LocalizedPopulatorUtilImpl localizedPopulatorUtil;

	@Mock
	private BiConsumer<String, Locale> stringConsumer;

	@Mock
	private BiConsumer<Boolean, Locale> booleanConsumer;

	@Test(expected = NullPointerException.class)
	public void shouldThrowNPEForStringsNullConsumer()
	{
		//When
		localizedPopulatorUtil.populateStrings(null, null);

	}

	@Test
	public void shouldSkipPopulatingStrings()
	{
		//Given
		List<LocalizedStringSwgDTO> localizedStrings = null;

		//When
		localizedPopulatorUtil.populateStrings(localizedStrings, stringConsumer);

		//Then
		verify(stringConsumer, never()).accept(any(), any());
	}

	@Test
	public void shouldPopulateStrings()
	{
		//Given
		LocalizedStringSwgDTO localizedString = new LocalizedStringSwgDTO();
		localizedString.setLocale(LOCALE_ISOCODE_US);
		localizedString.setValue(STRING_VALUE);
		List<LocalizedStringSwgDTO> localizedStrings = Collections.singletonList(localizedString);

		//When
		localizedPopulatorUtil.populateStrings(localizedStrings, stringConsumer);

		//Then
		verify(stringConsumer).accept(eq(STRING_VALUE), eq(LOCALE_US));
	}

	@Test(expected = NullPointerException.class)
	public void shouldThrowNPEForLongStringsNullConsumer()
	{
		//When
		localizedPopulatorUtil.populateLongStrings(null, null);
	}

	@Test
	public void shouldSkipPopulatingLongStrings()
	{
		//Given
		List<LocalizedLongStringSwgDTO> localizedLongStrings = null;

		//When
		localizedPopulatorUtil.populateLongStrings(localizedLongStrings, stringConsumer);

		//Then
		verify(stringConsumer, never()).accept(any(), any());
	}

	@Test
	public void shouldPopulateLongStrings()
	{
		//Given
		LocalizedLongStringSwgDTO localizedLongString = new LocalizedLongStringSwgDTO();
		localizedLongString.setLocale(LOCALE_ISOCODE_US);
		localizedLongString.setValue(STRING_VALUE);
		List<LocalizedLongStringSwgDTO> localizedLongStrings = Collections.singletonList(localizedLongString);

		//When
		localizedPopulatorUtil.populateLongStrings(localizedLongStrings, stringConsumer);

		//Then
		verify(stringConsumer).accept(eq(STRING_VALUE), eq(LOCALE_US));
	}

	@Test(expected = NullPointerException.class)
	public void shouldThrowNPEForBooleanNullConsumer()
	{
		//When
		localizedPopulatorUtil.populateBooleans(null, null);
	}

	@Test
	public void shouldSkipPopulatingBooleans()
	{
		//Given
		List<LocalizedBooleanSwgDTO> localizedBooleans = null;

		//When
		localizedPopulatorUtil.populateBooleans(localizedBooleans, booleanConsumer);

		//Then
		verify(stringConsumer, never()).accept(any(), any());
	}

	@Test
	public void shouldPopulateBooleans()
	{
		//Given
		LocalizedBooleanSwgDTO localizedBoolean = new LocalizedBooleanSwgDTO();
		localizedBoolean.setLocale(LOCALE_ISOCODE_US);
		localizedBoolean.setValue(BOOLEAN_VALUE);
		List<LocalizedBooleanSwgDTO> localizedBooleans = Collections.singletonList(localizedBoolean);

		//When
		localizedPopulatorUtil.populateBooleans(localizedBooleans, booleanConsumer);

		//Then
		verify(booleanConsumer).accept(eq(BOOLEAN_VALUE), eq(LOCALE_US));
	}

}
