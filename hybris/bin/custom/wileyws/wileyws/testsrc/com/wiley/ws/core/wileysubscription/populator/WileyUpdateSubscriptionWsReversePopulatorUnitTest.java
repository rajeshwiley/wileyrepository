package com.wiley.ws.core.wileysubscription.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.converters.Populator;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileySubscriptionModel;
import com.wiley.core.util.TestUtils;
import com.wiley.ws.core.wileysubscription.dto.AbstractSubscriptionRequestWsDTO;
import com.wiley.ws.core.wileysubscription.dto.SubscriptionUpdateRequestWsDTO;


/**
 * Default unit test for {@link WileyUpdateSubscriptionWsReversePopulator}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyUpdateSubscriptionWsReversePopulatorUnitTest
{

	@InjectMocks
	private WileyUpdateSubscriptionWsReversePopulator wileyUpdateSubscriptionWsReversePopulator;

	// --------------------------- Mocks for injection

	@Mock
	private Populator<AbstractSubscriptionRequestWsDTO, WileySubscriptionModel> wileySubscriptionWsReversePopulator;

	// --------------------------- Test data

	@Mock
	private SubscriptionUpdateRequestWsDTO subscriptionUpdateRequestWsDTOMock;

	@Mock
	private WileySubscriptionModel wileySubscriptionModelMock;

	@Before
	public void setUp() throws Exception
	{
		when(wileySubscriptionModelMock.getHybrisRenewal()).thenReturn(false);
	}

	@Test
	public void testPopulateSuccessCase()
	{
		// Given
		when(subscriptionUpdateRequestWsDTOMock.getRenewable()).thenReturn(true);

		// When
		wileyUpdateSubscriptionWsReversePopulator.populate(subscriptionUpdateRequestWsDTOMock, wileySubscriptionModelMock);

		// Then
		verify(wileySubscriptionModelMock, never()).setHybrisRenewal(any());
		verify(wileySubscriptionModelMock).setRequireRenewal(true);
		verify(wileySubscriptionWsReversePopulator).populate(same(subscriptionUpdateRequestWsDTOMock),
				same(wileySubscriptionModelMock));
	}

	@Test
	public void testPopulateWhenHybrisRenewalFlagSetToTrue()
	{
		// Given
		when(wileySubscriptionModelMock.getHybrisRenewal()).thenReturn(true);

		// When
		TestUtils.expectException(IllegalStateException.class, () -> wileyUpdateSubscriptionWsReversePopulator
				.populate(subscriptionUpdateRequestWsDTOMock, wileySubscriptionModelMock));

		// Then
		verify(wileySubscriptionModelMock).getHybrisRenewal();
		verify(wileySubscriptionModelMock).getCode();
		verifyNoMoreInteractions(wileySubscriptionModelMock);
		verifyZeroInteractions(wileySubscriptionWsReversePopulator);
	}

	@Test
	public void testPopulateWhenRequireRenewalFlagIsNull()
	{
		// Given
		when(subscriptionUpdateRequestWsDTOMock.getRenewable()).thenReturn(null);

		// When
		wileyUpdateSubscriptionWsReversePopulator.populate(subscriptionUpdateRequestWsDTOMock, wileySubscriptionModelMock);

		// Then
		verify(wileySubscriptionModelMock, never()).setRequireRenewal(any());
	}

	@Test
	public void testPopulateWhenRequireRenewalFlagIsFalse()
	{
		// Given
		when(subscriptionUpdateRequestWsDTOMock.getRenewable()).thenReturn(false);

		// When
		wileyUpdateSubscriptionWsReversePopulator.populate(subscriptionUpdateRequestWsDTOMock, wileySubscriptionModelMock);

		// Then
		verify(wileySubscriptionModelMock).setRequireRenewal(eq(false));
	}

	@Test
	public void testPopulateWhenRequireRenewalFlagIsTrue()
	{
		// Given
		when(subscriptionUpdateRequestWsDTOMock.getRenewable()).thenReturn(true);

		// When
		wileyUpdateSubscriptionWsReversePopulator.populate(subscriptionUpdateRequestWsDTOMock, wileySubscriptionModelMock);

		// Then
		verify(wileySubscriptionModelMock).setRequireRenewal(eq(true));
	}

	@Test
	public void testPopulateWithIllegalParameters()
	{
		// When
		TestUtils.expectException(IllegalArgumentException.class,
				() -> wileyUpdateSubscriptionWsReversePopulator.populate(null, null));
		// Then
		verifyZeroInteractions(wileySubscriptionWsReversePopulator);

		// -----------------------------------------------

		// When
		TestUtils.expectException(IllegalArgumentException.class,
				() -> wileyUpdateSubscriptionWsReversePopulator.populate(subscriptionUpdateRequestWsDTOMock, null));
		// Then
		verifyZeroInteractions(wileySubscriptionWsReversePopulator);

		// -----------------------------------------------

		// When
		TestUtils.expectException(IllegalArgumentException.class,
				() -> wileyUpdateSubscriptionWsReversePopulator.populate(null, wileySubscriptionModelMock));
		// Then
		verifyZeroInteractions(wileySubscriptionWsReversePopulator, wileySubscriptionModelMock);
	}

}