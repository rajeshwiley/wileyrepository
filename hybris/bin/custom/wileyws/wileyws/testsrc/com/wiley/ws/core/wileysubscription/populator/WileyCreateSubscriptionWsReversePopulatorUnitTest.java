package com.wiley.ws.core.wileysubscription.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileySubscriptionModel;
import com.wiley.core.util.TestUtils;
import com.wiley.core.wileyws.order.WileycomWsOrderService;
import com.wiley.ws.core.wileysubscription.dto.AbstractSubscriptionRequestWsDTO;
import com.wiley.ws.core.wileysubscription.dto.SubscriptionCreateRequestWsDTO;


/**
 * Default unit test for {@link WileyCreateSubscriptionWsReversePopulator}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyCreateSubscriptionWsReversePopulatorUnitTest
{

	@Mock
	private WileycomWsOrderService wileycomWsOrderServiceMock;

	@Mock
	private KeyGenerator wileyb2cSubscriptionCodeGeneratorMock;

	@Mock
	private Populator<AbstractSubscriptionRequestWsDTO, WileySubscriptionModel> wileySubscriptionWsReversePopulatorMock;

	@InjectMocks
	private WileyCreateSubscriptionWsReversePopulator wileyCreateSubscriptionWsReversePopulator;

	// Test data

	@Mock
	private SubscriptionCreateRequestWsDTO subscriptionCreateRequestWsDTOMock;
	private static final String TEST_ISBN = "some_isbn";
	private static final String TEST_ORDER_ID = "some_order_id";

	@Mock
	private WileySubscriptionModel wileySubscriptionModelMock;

	@Mock
	private OrderModel orderModelMock;

	@Mock
	private AbstractOrderEntryModel orderEntryModelMock;

	@Mock
	private CustomerModel customerModelMock;

	@Mock
	private ProductModel productModelMock;
	private static final String TEST_PRODUCT_ISBN = "some_product_isbn";

	private static final String TEST_GENERATED_CODE = "some_generated_code";

	@Before
	public void setUp() throws Exception
	{
		when(subscriptionCreateRequestWsDTOMock.getIsbn()).thenReturn(TEST_ISBN);
		when(subscriptionCreateRequestWsDTOMock.getOrderId()).thenReturn(TEST_ORDER_ID);
		when(subscriptionCreateRequestWsDTOMock.getRenewable()).thenReturn(true);

		when(wileycomWsOrderServiceMock.findOrderByCode(eq(TEST_ORDER_ID))).thenReturn(orderModelMock);
		when(wileycomWsOrderServiceMock.getOrderEntryModelByISBN(eq(orderModelMock), eq(TEST_ISBN))).thenReturn(
				Optional.of(orderEntryModelMock));

		when(orderModelMock.getUser()).thenReturn(customerModelMock);

		when(orderEntryModelMock.getProduct()).thenReturn(productModelMock);

		when(productModelMock.getIsbn()).thenReturn(TEST_PRODUCT_ISBN);

		when(wileyb2cSubscriptionCodeGeneratorMock.generate()).thenReturn(TEST_GENERATED_CODE);
	}

	@Test
	public void testPopulateSuccessCase()
	{
		// Given
		// no changes in test data

		// When
		wileyCreateSubscriptionWsReversePopulator.populate(subscriptionCreateRequestWsDTOMock, wileySubscriptionModelMock);

		// Then
		verify(wileySubscriptionWsReversePopulatorMock).populate(same(subscriptionCreateRequestWsDTOMock),
				same(wileySubscriptionModelMock));
		verify(wileySubscriptionModelMock).setCustomer(same(customerModelMock));
		verify(wileySubscriptionModelMock).setOrderEntry(same(orderEntryModelMock));
		verify(wileySubscriptionModelMock).setProduct(same(productModelMock));
		verify(wileySubscriptionModelMock).setIsbn(eq(TEST_PRODUCT_ISBN));
		verify(wileySubscriptionModelMock).setRenewalEnabled(eq(true));
		verify(wileySubscriptionModelMock).setRequireRenewal(eq(true));
	}

	@Test
	public void testPopulateWhenRenewableParamSetToFalse()
	{
		// Given
		when(subscriptionCreateRequestWsDTOMock.getRenewable()).thenReturn(false);

		// When
		wileyCreateSubscriptionWsReversePopulator.populate(subscriptionCreateRequestWsDTOMock, wileySubscriptionModelMock);

		// Then
		verify(wileySubscriptionModelMock).setRenewalEnabled(eq(true));
		verify(wileySubscriptionModelMock).setRequireRenewal(eq(false));
	}

	@Test
	public void testPopulateWhenRenewableParamIsNull()
	{
		// Given
		when(subscriptionCreateRequestWsDTOMock.getRenewable()).thenReturn(null);

		// When
		wileyCreateSubscriptionWsReversePopulator.populate(subscriptionCreateRequestWsDTOMock, wileySubscriptionModelMock);

		// Then
		verify(wileySubscriptionModelMock).setRenewalEnabled(eq(true));
		verify(wileySubscriptionModelMock).setRequireRenewal(eq(true));
	}

	@Test
	public void testPopulateWhenOrderNotFound()
	{
		// Given
		when(wileycomWsOrderServiceMock.findOrderByCode(eq(TEST_ORDER_ID))).thenThrow(UnknownIdentifierException.class);

		// When
		TestUtils.expectException(UnknownIdentifierException.class, () -> wileyCreateSubscriptionWsReversePopulator
				.populate(subscriptionCreateRequestWsDTOMock, wileySubscriptionModelMock));

		// Then
		verifyZeroInteractions(wileySubscriptionWsReversePopulatorMock, wileySubscriptionModelMock,
				wileyb2cSubscriptionCodeGeneratorMock);
	}

	@Test
	public void testPopulateWhenOrderEntryNotFound()
	{
		// Given
		when(wileycomWsOrderServiceMock.getOrderEntryModelByISBN(eq(orderModelMock), eq(TEST_ISBN))).thenThrow(
				UnknownIdentifierException.class);

		// When
		TestUtils.expectException(UnknownIdentifierException.class, () -> wileyCreateSubscriptionWsReversePopulator
				.populate(subscriptionCreateRequestWsDTOMock, wileySubscriptionModelMock));

		// Then
		verifyZeroInteractions(wileySubscriptionWsReversePopulatorMock, wileySubscriptionModelMock,
				wileyb2cSubscriptionCodeGeneratorMock);
	}

	@Test
	public void testPopulateWithIllegalParameters()
	{
		// When
		TestUtils.expectException(IllegalArgumentException.class, () -> wileyCreateSubscriptionWsReversePopulator
				.populate(null, null));
		// Then
		verifyZeroInteractions(wileySubscriptionWsReversePopulatorMock, wileyb2cSubscriptionCodeGeneratorMock);

		// -----------------------------------------------------------------

		// When
		TestUtils.expectException(IllegalArgumentException.class, () -> wileyCreateSubscriptionWsReversePopulator
				.populate(subscriptionCreateRequestWsDTOMock, null));
		// Then
		verifyZeroInteractions(wileySubscriptionWsReversePopulatorMock, wileyb2cSubscriptionCodeGeneratorMock);

		// -----------------------------------------------------------------

		// When
		TestUtils.expectException(IllegalArgumentException.class, () -> wileyCreateSubscriptionWsReversePopulator
				.populate(null, wileySubscriptionModelMock));
		// Then
		verifyZeroInteractions(wileySubscriptionWsReversePopulatorMock, wileySubscriptionModelMock,
				wileyb2cSubscriptionCodeGeneratorMock);
	}

	@Test
	public void testPopulateWithIllegalState()
	{
		// Given
		when(subscriptionCreateRequestWsDTOMock.getIsbn()).thenReturn(null);
		when(subscriptionCreateRequestWsDTOMock.getOrderId()).thenReturn(TEST_ORDER_ID);

		// When
		TestUtils.expectException(IllegalStateException.class, () -> wileyCreateSubscriptionWsReversePopulator
				.populate(subscriptionCreateRequestWsDTOMock, wileySubscriptionModelMock));

		// Then
		verifyZeroInteractions(wileySubscriptionWsReversePopulatorMock, wileySubscriptionModelMock,
				wileyb2cSubscriptionCodeGeneratorMock);

		// -----------------------------------------------------------------

		// Given
		when(subscriptionCreateRequestWsDTOMock.getIsbn()).thenReturn(TEST_ISBN);
		when(subscriptionCreateRequestWsDTOMock.getOrderId()).thenReturn(null);

		// When
		TestUtils.expectException(IllegalStateException.class, () -> wileyCreateSubscriptionWsReversePopulator
				.populate(subscriptionCreateRequestWsDTOMock, wileySubscriptionModelMock));

		// Then
		verifyZeroInteractions(wileySubscriptionWsReversePopulatorMock, wileySubscriptionModelMock,
				wileyb2cSubscriptionCodeGeneratorMock);

		// -----------------------------------------------------------------

		// Given
		when(subscriptionCreateRequestWsDTOMock.getIsbn()).thenReturn(null);
		when(subscriptionCreateRequestWsDTOMock.getOrderId()).thenReturn(null);

		// When
		TestUtils.expectException(IllegalStateException.class, () -> wileyCreateSubscriptionWsReversePopulator
				.populate(subscriptionCreateRequestWsDTOMock, wileySubscriptionModelMock));

		// Then
		verifyZeroInteractions(wileySubscriptionWsReversePopulatorMock, wileySubscriptionModelMock,
				wileyb2cSubscriptionCodeGeneratorMock);
	}

	@Test
	public void testPopulateWithIllegalTypeOfUser()
	{
		// Given
		final UserModel simpleUserMock = mock(UserModel.class);
		when(orderModelMock.getUser()).thenReturn(simpleUserMock);

		// When
		TestUtils.expectException(IllegalStateException.class, () -> wileyCreateSubscriptionWsReversePopulator
				.populate(subscriptionCreateRequestWsDTOMock, wileySubscriptionModelMock));

		// Then
		verifyZeroInteractions(wileySubscriptionWsReversePopulatorMock, wileySubscriptionModelMock,
				wileyb2cSubscriptionCodeGeneratorMock);

		// -----------------------------------------------------------------

		// Given
		when(orderModelMock.getUser()).thenReturn(null);

		// When
		TestUtils.expectException(IllegalStateException.class, () -> wileyCreateSubscriptionWsReversePopulator
				.populate(subscriptionCreateRequestWsDTOMock, wileySubscriptionModelMock));

		// Then
		verifyZeroInteractions(wileySubscriptionWsReversePopulatorMock, wileySubscriptionModelMock,
				wileyb2cSubscriptionCodeGeneratorMock);
	}
}