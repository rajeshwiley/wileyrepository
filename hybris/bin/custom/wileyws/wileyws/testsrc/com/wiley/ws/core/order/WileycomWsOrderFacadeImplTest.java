package com.wiley.ws.core.order;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.orderhistory.model.OrderHistoryEntryModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.util.DiscountValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.wileyws.order.WileycomWsOrderHistoryService;
import com.wiley.core.wileyws.order.impl.WileycomWsOrderServiceImpl;
import com.wiley.ws.core.order.dto.DigitalProductAccessTokenWsDTO;
import com.wiley.ws.core.order.dto.DiscountWsDTO;
import com.wiley.ws.core.order.dto.OrderAddressWsDTO;
import com.wiley.ws.core.order.dto.OrderStatusWsDTO;
import com.wiley.ws.core.order.dto.OrderUpdateEntryRequestWsDTO;
import com.wiley.ws.core.order.dto.OrderUpdateRequestWsDTO;
import com.wiley.ws.core.order.dto.ShippingEventEntryWsDTO;
import com.wiley.ws.core.order.dto.ShippingEventWsDTO;
import com.wiley.ws.core.order.dto.StatusMessageWsDTO;
import com.wiley.ws.core.order.dto.UpdateMessageWsDTO;
import com.wiley.ws.core.order.exception.IsbnNotFoundException;
import com.wiley.ws.core.order.exception.OrderWsNotFoundException;
import com.wiley.ws.core.order.exception.SchoolWsNotFoundException;
import com.wiley.ws.core.order.impl.WileycomWsOrderFacadeImpl;

import static com.wiley.ws.core.order.WileycomWsOrderFacadeImplTest.DiscountConstants.DISCOUNT_1_ABSOLUTE;
import static com.wiley.ws.core.order.WileycomWsOrderFacadeImplTest.DiscountConstants.DISCOUNT_1_NAME;
import static com.wiley.ws.core.order.WileycomWsOrderFacadeImplTest.DiscountConstants.DISCOUNT_1_VALUE;
import static com.wiley.ws.core.order.WileycomWsOrderFacadeImplTest.DiscountConstants.DISCOUNT_2_ABSOLUTE;
import static com.wiley.ws.core.order.WileycomWsOrderFacadeImplTest.DiscountConstants.DISCOUNT_2_NAME;
import static com.wiley.ws.core.order.WileycomWsOrderFacadeImplTest.DiscountConstants.DISCOUNT_2_VALUE;
import static com.wiley.ws.core.order.WileycomWsOrderFacadeImplTest.OrderConstants.NONEXISTENT_ORDER_CODE;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;


/**
 * The test for {@link WileycomWsOrderFacadeImpl}
 */
@IntegrationTest
public class WileycomWsOrderFacadeImplTest extends AbstractWileyServicelayerTransactionalTest
{
	private static final String ORDER_CODE = "test_order1";
	private static final String ORDER_CODE_TWO = "test_order2";
	private static final String ORDER_CODE_THREE = "test_order3";
	private static final String ORDER_CODE_DIGITAL = "test_digital_order";

	interface ShippingConstants
	{
		String TRACKING_ID_TEST = "TRACKING_ID_TEST";
		String CARRIER_TEST = "CARRIER_TEST";
		String SHIPPING_DATE_TEST = "2001-01-07T16:39:57-08:00";
	}

	interface OrderConstants
	{
		String ISBN_WCOM_B2C_PHYSICAL = "111111111111";
		String ISBN_WDIGITAL_PRODUCT = "111111111112";
		String ISBN_WCOM_B2C_DIGITAL = "111111111113";
		String ISBN_NO_PRODUCT_IN_ORDER = "ISBN_NO_PRODUCT_IN_ORDER";
		String ORDER_COUNTRY = "RU";
		String ORDER_CURRENCY = "USD";
		String NON_EXISTING_SCHOOL_ID = "NON_EXISTING_SCHOOL_ID";
		String NON_EXISTING_UNIT_SAP_NUMBER = "NON_EXISTING_UNIT_SAP_NUMBER";
		String NONEXISTENT_ORDER_CODE = "NONEXISTENT_ORDER_CODE";
	}

	interface OrderEntryConstants
	{
		String UNIT_CODE = "pieces";
	}

	interface AddressConstants
	{
		String NEW_ADDRESS_ID = "new_generated_id";
		String DELIVERY_EXTERNAL_ADDRESS_ID = "123456789";
		String PAYMENT_EXTERNAL_ADDRESS_ID = "987456321";
		String POSTCODE = "191119";
		String COUNTRY_ISO_CODE = "RU";
		String CITY = "St. Petersburg";
		String STREET = "Voronezhskaya";
		String HOUSE = "5";
		String PHONE = "+7777777777";
	}

	interface DiscountConstants
	{
		String DISCOUNT_1_NAME = "DISCOUNT_1_NAME";
		double DISCOUNT_1_VALUE = 2.0d;
		boolean DISCOUNT_1_ABSOLUTE = true;

		String DISCOUNT_2_NAME = "DISCOUNT_2_NAME";
		double DISCOUNT_2_VALUE = 1.0d;
		boolean DISCOUNT_2_ABSOLUTE = false;
	}

	interface DigitalAccessToken
	{
		String TOKEN_1_ISBN = OrderConstants.ISBN_WCOM_B2C_DIGITAL;
		String TOKEN_1_TOKEN = "TOKEN_1_TOKEN";
		String TOKEN_2_ISBN = OrderConstants.ISBN_WCOM_B2C_PHYSICAL;
		String TOKEN_2_TOKEN = "TOKEN_2_TOKEN";
	}


	@Resource(name = "wileycomWsOrderFacade")
	private WileycomWsOrderFacadeImpl wsOrderFacade;

	@Resource(name = "wileycomWsOrderService")
	private WileycomWsOrderServiceImpl wileycomWsOrderService;

	@Resource
	private ModelService modelService;

	@Resource
	private WileycomWsOrderHistoryService wileycomWsOrderHistoryService;

	private OrderStatusWsDTO wsDTO;

	private ShippingEventWsDTO shippingEventWsDTO = new ShippingEventWsDTO();

	@Before
	public void setUp() throws Exception
	{
		importCsv("/wileyws/test/orders.impex", DEFAULT_ENCODING);
		StatusMessageWsDTO messageWsDTO = new StatusMessageWsDTO();
		messageWsDTO.setCode("11000");
		messageWsDTO.setMessage("awesome message");
		wsDTO = new OrderStatusWsDTO();
		wsDTO.setMessage(messageWsDTO);
	}

	private void setupShippingDto()
	{
		shippingEventWsDTO.setCarrier(ShippingConstants.CARRIER_TEST);
		shippingEventWsDTO.setShippingDate(ShippingConstants.SHIPPING_DATE_TEST);
		shippingEventWsDTO.setTrackingId(ShippingConstants.TRACKING_ID_TEST);
		shippingEventWsDTO.setEntries(setupShippingDtoEntries());
	}

	private List<ShippingEventEntryWsDTO> setupShippingDtoEntries()
	{
		return Arrays.asList(createShippingEntry(OrderConstants.ISBN_WCOM_B2C_DIGITAL, "2"),
				createShippingEntry(OrderConstants.ISBN_WCOM_B2C_PHYSICAL, "3"));
	}

	private ShippingEventEntryWsDTO createShippingEntry(final String isbn, final String shippedQuantity)
	{
		ShippingEventEntryWsDTO result = new ShippingEventEntryWsDTO();
		result.setIsbn(isbn);
		result.setShippedQuantity(shippedQuantity);
		return result;
	}

	@Test
	public void shouldChangeStatusAndAddHistoryEntry()
	{
		wsDTO.setStatus(OrderStatusWsDTO.StatusEnum.NOT_YET_PROCESSED);


		wsOrderFacade.updateStatusAndAddHistoryEntry(ORDER_CODE, wsDTO);

		modelService.detachAll();
		OrderModel orderModel = wileycomWsOrderService.findOrderByCode(ORDER_CODE);
		assertEquals(orderModel.getStatus(), OrderStatus.NOT_YET_PROCESSED);
		assertEquals(orderModel.getHistoryEntries().get(0).getOrder(), orderModel);
		assertEquals(orderModel.getHistoryEntries().get(0).getDescription(), "awesome message (11000)");
	}


	@Test(expected = OrderWsNotFoundException.class)
	public void shouldThrowOrderWsNotFoundException()
	{
		wsDTO.setStatus(OrderStatusWsDTO.StatusEnum.PARTIALLY_PROCESSED);

		wsOrderFacade.updateStatusAndAddHistoryEntry(NONEXISTENT_ORDER_CODE, wsDTO);
	}

	@Test
	public void testShippingConsignmentSaved()
	{
		setupShippingDto();
		OrderModel orderBeforeRequest = wileycomWsOrderService.findOrderByCode(ORDER_CODE);
		final int consignmentsNumberBeforerequest = orderBeforeRequest.getConsignments().size();

		wsOrderFacade.updateShipping(ORDER_CODE, shippingEventWsDTO);

		OrderModel orderModelAfterRequest = wileycomWsOrderService.findOrderByCode(ORDER_CODE);
		final int consignmentsNumberAfterRequest = orderModelAfterRequest.getConsignments().size();
		assertThat(consignmentsNumberAfterRequest - consignmentsNumberBeforerequest, is(1));
	}

	@Test(expected = IsbnNotFoundException.class)
	public void testShippingConsignmentNoISBNInOrder()
	{
		setupShippingDto();
		shippingEventWsDTO.setEntries(Arrays.asList(createShippingEntry(OrderConstants.ISBN_NO_PRODUCT_IN_ORDER, "10")));

		wsOrderFacade.updateShipping(ORDER_CODE, shippingEventWsDTO);
	}

	@Test
	public void testOrderUpdate() throws Exception
	{
		final OrderUpdateRequestWsDTO orderDTO = initOrderDTO();
		final OrderUpdateEntryRequestWsDTO entry1 = initOrderEntryDTO(OrderConstants.ISBN_WCOM_B2C_DIGITAL, 11);
		final OrderUpdateEntryRequestWsDTO entry2 = initOrderEntryDTO(OrderConstants.ISBN_WCOM_B2C_PHYSICAL, 22);
		orderDTO.setEntries(Arrays.asList(entry1, entry2));
		final UpdateMessageWsDTO message1 = initUpdateMessageDTO(OrderConstants.ISBN_WCOM_B2C_DIGITAL);
		final UpdateMessageWsDTO message2 = initUpdateMessageDTO(OrderConstants.ISBN_WCOM_B2C_PHYSICAL);
		orderDTO.setMessages(Arrays.asList(message1, message2));
		orderDTO.setDeliveryAddress(initAddress(AddressConstants.NEW_ADDRESS_ID));
		orderDTO.setPaymentAddress(initAddress(AddressConstants.NEW_ADDRESS_ID));
		createAndSetDiscounts(orderDTO);
		createAndSetDigitalTokens(orderDTO);

		wsOrderFacade.updateOrder(ORDER_CODE_TWO, orderDTO);

		modelService.detachAll();
		OrderModel orderModel = wileycomWsOrderService.findOrderByCode(ORDER_CODE_TWO);
		assertNotNull(orderModel);

		checkOrder(orderDTO, orderModel);
		checkAddress(orderModel.getPaymentAddress());
		checkAddress(orderModel.getDeliveryAddress());
		checkOrderHistory(orderModel);
		checkCartDiscount(orderModel, DISCOUNT_1_NAME, DISCOUNT_1_ABSOLUTE, DISCOUNT_1_VALUE);
		checkCartDiscount(orderModel, DISCOUNT_2_NAME, DISCOUNT_2_ABSOLUTE, DISCOUNT_2_VALUE);

		final List<AbstractOrderEntryModel> entries = orderModel.getEntries();
		assertTrue(CollectionUtils.isNotEmpty(entries));
		assertEquals(2, entries.size());

		checkOrderEntry(orderModel, OrderConstants.ISBN_WCOM_B2C_DIGITAL, 11, 2, 3);
		checkOrderEntry(orderModel, OrderConstants.ISBN_WCOM_B2C_PHYSICAL, 22, 2, 3);
		checkOrderEntryDiscounts(orderModel, OrderConstants.ISBN_WCOM_B2C_DIGITAL);
		checkOrderEntryDiscounts(orderModel, OrderConstants.ISBN_WCOM_B2C_PHYSICAL);
		checkOrderEntryToken(orderModel, DigitalAccessToken.TOKEN_1_ISBN, DigitalAccessToken.TOKEN_1_TOKEN);
		checkOrderEntryToken(orderModel, DigitalAccessToken.TOKEN_2_ISBN, DigitalAccessToken.TOKEN_2_TOKEN);
	}

	@Test
	public void testAddressUpdateWhenAddressExists() throws Exception
	{
		final OrderUpdateRequestWsDTO orderDTO = initOrderDTO();
		final OrderUpdateEntryRequestWsDTO entry1 = initOrderEntryDTO(OrderConstants.ISBN_WCOM_B2C_DIGITAL, 11);
		final OrderUpdateEntryRequestWsDTO entry2 = initOrderEntryDTO(OrderConstants.ISBN_WCOM_B2C_PHYSICAL, 22);
		orderDTO.setEntries(Arrays.asList(entry1, entry2));
		final UpdateMessageWsDTO message1 = initUpdateMessageDTO(OrderConstants.ISBN_WCOM_B2C_DIGITAL);
		final UpdateMessageWsDTO message2 = initUpdateMessageDTO(OrderConstants.ISBN_WCOM_B2C_PHYSICAL);
		orderDTO.setMessages(Arrays.asList(message1, message2));
		orderDTO.setDeliveryAddress(initAddress(AddressConstants.DELIVERY_EXTERNAL_ADDRESS_ID));
		orderDTO.setPaymentAddress(initAddress(AddressConstants.PAYMENT_EXTERNAL_ADDRESS_ID));
		createAndSetDiscounts(orderDTO);
		createAndSetDigitalTokens(orderDTO);

		OrderModel orderModel = updateOrder(orderDTO, ORDER_CODE_THREE);
		assertNotNull(orderModel);

		checkAddress(orderModel.getDeliveryAddress(), AddressConstants.DELIVERY_EXTERNAL_ADDRESS_ID, AddressConstants.STREET);
		checkAddress(orderModel.getPaymentAddress(), AddressConstants.PAYMENT_EXTERNAL_ADDRESS_ID, AddressConstants.STREET);

		final String newDeliveryLine1 = "new delivery line 1";
		final String newPaymentLine1 = "new payment line 1";
		orderDTO.getDeliveryAddress().setLine1(newDeliveryLine1);
		orderDTO.getPaymentAddress().setLine1(newPaymentLine1);

		OrderModel newOrderModel = updateOrder(orderDTO, ORDER_CODE_THREE);
		assertNotNull(newOrderModel);

		checkAddress(newOrderModel.getDeliveryAddress(), AddressConstants.DELIVERY_EXTERNAL_ADDRESS_ID, newDeliveryLine1);
		checkAddress(newOrderModel.getPaymentAddress(), AddressConstants.PAYMENT_EXTERNAL_ADDRESS_ID, newPaymentLine1);
	}

	@Test
	public void testDigitalOrderAddressUpdate() throws Exception
	{
		final OrderUpdateRequestWsDTO orderDTO = initOrderDTO();
		final OrderUpdateEntryRequestWsDTO entry1 = initOrderEntryDTO(OrderConstants.ISBN_WCOM_B2C_DIGITAL, 11);
		final OrderUpdateEntryRequestWsDTO entry2 = initOrderEntryDTO(OrderConstants.ISBN_WCOM_B2C_PHYSICAL, 22);
		orderDTO.setEntries(Arrays.asList(entry1, entry2));
		final UpdateMessageWsDTO message1 = initUpdateMessageDTO(OrderConstants.ISBN_WCOM_B2C_DIGITAL);
		final UpdateMessageWsDTO message2 = initUpdateMessageDTO(OrderConstants.ISBN_WCOM_B2C_PHYSICAL);
		orderDTO.setMessages(Arrays.asList(message1, message2));
		orderDTO.setPaymentAddress(initAddress(AddressConstants.PAYMENT_EXTERNAL_ADDRESS_ID));
		createAndSetDiscounts(orderDTO);
		createAndSetDigitalTokens(orderDTO);

		OrderModel orderModel = updateOrder(orderDTO, ORDER_CODE_DIGITAL);
		assertNotNull(orderModel);

		assertNull(orderModel.getDeliveryAddress());
		checkAddress(orderModel.getPaymentAddress(), AddressConstants.PAYMENT_EXTERNAL_ADDRESS_ID, AddressConstants.STREET);

		final String newPaymentLine1 = "new payment line 1";
		orderDTO.getPaymentAddress().setLine1(newPaymentLine1);

		OrderModel newOrderModel = updateOrder(orderDTO, ORDER_CODE_DIGITAL);
		assertNotNull(newOrderModel);

		checkAddress(newOrderModel.getPaymentAddress(), AddressConstants.PAYMENT_EXTERNAL_ADDRESS_ID, newPaymentLine1);
	}

	@Test
	public void testWhenOrderDeliveryWasEmpty() throws Exception
	{
		final OrderUpdateRequestWsDTO orderDTO = initOrderDTO();
		final OrderUpdateEntryRequestWsDTO entry1 = initOrderEntryDTO(OrderConstants.ISBN_WCOM_B2C_DIGITAL, 11);
		final OrderUpdateEntryRequestWsDTO entry2 = initOrderEntryDTO(OrderConstants.ISBN_WCOM_B2C_PHYSICAL, 22);
		orderDTO.setEntries(Arrays.asList(entry1, entry2));
		final UpdateMessageWsDTO message1 = initUpdateMessageDTO(OrderConstants.ISBN_WCOM_B2C_DIGITAL);
		final UpdateMessageWsDTO message2 = initUpdateMessageDTO(OrderConstants.ISBN_WCOM_B2C_PHYSICAL);
		orderDTO.setMessages(Arrays.asList(message1, message2));
		orderDTO.setDeliveryAddress(initAddress(AddressConstants.DELIVERY_EXTERNAL_ADDRESS_ID));
		orderDTO.setPaymentAddress(initAddress(AddressConstants.PAYMENT_EXTERNAL_ADDRESS_ID));
		createAndSetDiscounts(orderDTO);
		createAndSetDigitalTokens(orderDTO);

		OrderModel orderModel = updateOrder(orderDTO, ORDER_CODE_DIGITAL);
		assertNotNull(orderModel);

		checkAddress(orderModel.getDeliveryAddress(), AddressConstants.DELIVERY_EXTERNAL_ADDRESS_ID, AddressConstants.STREET);
		checkAddress(orderModel.getPaymentAddress(), AddressConstants.PAYMENT_EXTERNAL_ADDRESS_ID, AddressConstants.STREET);
	}

	private OrderModel updateOrder(final OrderUpdateRequestWsDTO orderDTO, final String orderCode)
	{
		wsOrderFacade.updateOrder(orderCode, orderDTO);

		modelService.detachAll();
		return wileycomWsOrderService.findOrderByCode(orderCode);
	}

	private void checkAddress(final AddressModel addressModel, final String externalId, final String line1)
	{
		assertNotNull(addressModel);
		assertEquals(externalId, addressModel.getExternalId());
		assertEquals(line1, addressModel.getLine1());
	}

	@Test
	public void testOrderUpdateWithTokensOnly() throws Exception
	{
		// given empty order with only digital token setted
		final OrderUpdateRequestWsDTO orderDTO = new OrderUpdateRequestWsDTO();
		createAndSetDigitalTokens(orderDTO);
		// and given filler orderModel in system with discounts and all other related information
		OrderModel orderModel = wileycomWsOrderService.findOrderByCode(ORDER_CODE);
		assertNotNull(orderModel);
		orderModel.setGlobalDiscountValues(initDiscountsCollection());
		orderModel.getEntries().forEach(e -> e.setDiscountValues(initDiscountsCollection()));
		modelService.save(orderModel);

		// when update order executed
		wsOrderFacade.updateOrder(ORDER_CODE, orderDTO);
		modelService.detachAll();
		orderModel = wileycomWsOrderService.findOrderByCode(ORDER_CODE);
		assertNotNull(orderModel);

		// then check that tokens were applied to order entries and other order and order entries fields were not wiped off
		checkOrderEntry(orderModel, DigitalAccessToken.TOKEN_1_ISBN, 2, 0, 0);
		checkOrderEntry(orderModel, DigitalAccessToken.TOKEN_2_ISBN, 3, 0, 0);
		checkOrderEntryDiscounts(orderModel, OrderConstants.ISBN_WCOM_B2C_DIGITAL);
		checkOrderEntryDiscounts(orderModel, OrderConstants.ISBN_WCOM_B2C_PHYSICAL);

		checkOrderEntryToken(orderModel, DigitalAccessToken.TOKEN_1_ISBN, DigitalAccessToken.TOKEN_1_TOKEN);
		checkOrderEntryToken(orderModel, DigitalAccessToken.TOKEN_2_ISBN, DigitalAccessToken.TOKEN_2_TOKEN);

		checkCartDiscount(orderModel, DISCOUNT_1_NAME, DISCOUNT_1_ABSOLUTE, DISCOUNT_1_VALUE);
		checkCartDiscount(orderModel, DISCOUNT_2_NAME, DISCOUNT_2_ABSOLUTE, DISCOUNT_2_VALUE);

		assertEquals("tepatel@wiley.com", orderModel.getUser().getUid());
		assertEquals("USD", orderModel.getCurrency().getIsocode());
		assertTrue(orderModel.getNet());
		assertEquals("wileyb2c", orderModel.getStore().getUid());
		assertEquals("EXPORTED_TO_ERP", orderModel.getStatus().getCode());
	}

	private List<DiscountValue> initDiscountsCollection()
	{
		final DiscountValue discountValue1 = new DiscountValue(DISCOUNT_1_NAME, DISCOUNT_1_VALUE, DISCOUNT_1_ABSOLUTE,
				OrderConstants.ORDER_CURRENCY);
		final DiscountValue discountValue2 = new DiscountValue(DISCOUNT_2_NAME, DISCOUNT_2_VALUE, DISCOUNT_2_ABSOLUTE,
				OrderConstants.ORDER_CURRENCY);
		return Arrays.asList(discountValue1, discountValue2);
	}

	private void checkOrderEntryToken(final OrderModel orderModel, final String productIsbn, final String token)
	{
		final AbstractOrderEntryModel entryModel = findEntryByIsbn(orderModel, productIsbn);
		assertEquals(token, entryModel.getDigitalProductAccessToken());
	}

	private void createAndSetDigitalTokens(final OrderUpdateRequestWsDTO orderDTO)
	{
		final DigitalProductAccessTokenWsDTO token1 = new DigitalProductAccessTokenWsDTO();
		token1.setIsbn(DigitalAccessToken.TOKEN_1_ISBN);
		token1.setToken(DigitalAccessToken.TOKEN_1_TOKEN);

		final DigitalProductAccessTokenWsDTO token2 = new DigitalProductAccessTokenWsDTO();
		token2.setIsbn(DigitalAccessToken.TOKEN_2_ISBN);
		token2.setToken(DigitalAccessToken.TOKEN_2_TOKEN);

		orderDTO.setDigitalProductAccessTokens(Arrays.asList(token1, token2));
	}

	private void createAndSetDiscounts(final OrderUpdateRequestWsDTO orderDTO)
	{
		final List<DiscountWsDTO> discounts = createDiscounts();
		orderDTO.setDiscounts(discounts);
	}

	private void createAndSetDiscounts(final OrderUpdateEntryRequestWsDTO entryDTO)
	{
		final List<DiscountWsDTO> discounts = createDiscounts();
		entryDTO.setDiscounts(discounts);
	}

	private List<DiscountWsDTO> createDiscounts()
	{
		final DiscountWsDTO discount1 = new DiscountWsDTO();
		discount1.setName(DISCOUNT_1_NAME);
		discount1.setValue(DISCOUNT_1_VALUE);
		discount1.setAbsolute(DISCOUNT_1_ABSOLUTE);

		final DiscountWsDTO discount2 = new DiscountWsDTO();
		discount2.setName(DiscountConstants.DISCOUNT_2_NAME);
		discount2.setValue(DiscountConstants.DISCOUNT_2_VALUE);
		discount2.setAbsolute(DiscountConstants.DISCOUNT_2_ABSOLUTE);

		return Arrays.asList(discount1, discount2);
	}

	private void checkOrderHistory(final OrderModel orderModel)
	{
		final List<OrderHistoryEntryModel> historyEntries = orderModel.getHistoryEntries();
		assertNotNull(historyEntries);
		final OrderHistoryEntryModel history = historyEntries.get(0);
		assertEquals(orderModel, history.getOrder());
		final OrderModel snapshot = history.getPreviousOrderVersion();
		assertNotNull(snapshot);
		assertEquals(new Double(0), snapshot.getTotalPrice());
		checkOrderEntry(snapshot, OrderConstants.ISBN_WDIGITAL_PRODUCT, 4, 0, 0);
		checkOrderEntry(snapshot, OrderConstants.ISBN_WCOM_B2C_PHYSICAL, 5, 0, 0);

		assertEquals("[INFO]: [Order entry updated] (Code: [111111111113], Product: [111111111113])\n"
				+ "[INFO]: [Order entry updated] (Code: [111111111111], Product: [111111111111])", history.getDescription());
	}

	private void checkOrder(final OrderUpdateRequestWsDTO orderDTO, final OrderModel orderModel)
	{
		assertEquals(orderDTO.getTotalPrice(), orderModel.getTotalPrice());
		assertEquals(orderDTO.getCountry(), orderModel.getCountry().getIsocode());
		assertEquals(orderDTO.getCurrency(), orderModel.getCurrency().getIsocode());
		assertEquals(66d, orderModel.getSubTotalWithoutDiscount(), Double.MIN_VALUE);
		assertEquals("66.0", orderModel.getSubTotalWithoutDiscount().toString());
	}

	private void checkOrderEntry(final OrderModel orderModel, final String productIsbn, final int quantity,
			final double basePrice, final double totalPrice)
	{

		final AbstractOrderEntryModel entryModel = findEntryByIsbn(orderModel, productIsbn);

		assertEquals(new Long(quantity), entryModel.getQuantity());
		assertEquals(new Double(basePrice), entryModel.getBasePrice());
		assertEquals(new Double(totalPrice), entryModel.getTotalPrice());
		assertNotNull(entryModel.getUnit());
		assertEquals(OrderEntryConstants.UNIT_CODE, entryModel.getUnit().getCode());
		assertNotNull(entryModel.getProduct());
		assertEquals(productIsbn, entryModel.getProduct().getIsbn());
		assertEquals(orderModel, entryModel.getOrder());
	}

	private void checkOrderEntryDiscounts(final OrderModel orderModel, final String productIsbn)
	{

		final AbstractOrderEntryModel entryModel = findEntryByIsbn(orderModel, productIsbn);
		checkCartEntryDiscount(entryModel, DISCOUNT_1_NAME, DISCOUNT_1_ABSOLUTE, DISCOUNT_1_VALUE);
		checkCartEntryDiscount(entryModel, DISCOUNT_2_NAME, DISCOUNT_2_ABSOLUTE, DISCOUNT_2_VALUE);

	}

	private static AbstractOrderEntryModel findEntryByIsbn(final OrderModel order, final String isbn)
	{
		final Optional<AbstractOrderEntryModel> entry = order.getEntries().stream()
				.filter(f -> f.getProduct().getIsbn().equals(isbn))
				.findFirst();
		if (entry.isPresent())
		{
			return entry.get();
		}
		else
		{
			final String message = "OrderEntry with product ISBN [" + isbn + "] wasn't found in collection";
			fail(message);
			throw new IsbnNotFoundException(order.getCode(), isbn);
		}
	}

	private void checkAddress(final AddressModel addressModel)
	{
		assertNotNull(addressModel);
		assertNotNull(addressModel.getCountry());
		assertEquals(AddressConstants.POSTCODE, addressModel.getPostalcode());
		assertEquals(AddressConstants.COUNTRY_ISO_CODE, addressModel.getCountry().getIsocode());
		assertEquals(AddressConstants.CITY, addressModel.getTown());
		assertEquals(AddressConstants.STREET, addressModel.getLine1());
		assertEquals(AddressConstants.HOUSE, addressModel.getLine2());
		assertEquals(AddressConstants.PHONE, addressModel.getPhone1());
	}

	@Test
	public void testCreateOrderWithNonExistingSchool() throws Exception
	{
		try
		{
			final OrderUpdateRequestWsDTO orderDTO = initOrderDTO();
			orderDTO.setSchoolId(OrderConstants.NON_EXISTING_SCHOOL_ID);
			wsOrderFacade.updateOrder(ORDER_CODE_TWO, orderDTO);
			fail("SchoolWsNotFoundException should be thrown");
		}
		catch (SchoolWsNotFoundException e)
		{
			/**
			 * In case of failure in the middle of processing transaction should completely rollback database modification,
			 * in this case order snapshot shouldn't be persisted in DB.
			 */
			modelService.detachAll();
			OrderModel orderModel = wileycomWsOrderService.findOrderByCode(ORDER_CODE_TWO);
			assertNotNull(orderModel);

			final Collection<OrderModel> snapshots = wileycomWsOrderHistoryService.getHistorySnapshots(orderModel);

			assertTrue(CollectionUtils.isEmpty(orderModel.getHistoryEntries()));
			assertTrue(CollectionUtils.isEmpty(snapshots));
		}
	}

	@Test(expected = UnknownIdentifierException.class)
	public void testUpdateOrderWithNonExistingUnit() throws Exception
	{
		final OrderUpdateRequestWsDTO orderDTO = initOrderDTO();
		orderDTO.setSapAccountNumber(OrderConstants.NON_EXISTING_UNIT_SAP_NUMBER);
		wsOrderFacade.updateOrder(ORDER_CODE_TWO, orderDTO);
	}

	private OrderAddressWsDTO initAddress(final String externalId)
	{
		final OrderAddressWsDTO orderAddressWsDTO = new OrderAddressWsDTO();
		orderAddressWsDTO.setAddressId(externalId);
		orderAddressWsDTO.setPostcode(AddressConstants.POSTCODE);
		orderAddressWsDTO.setCountry(AddressConstants.COUNTRY_ISO_CODE);
		orderAddressWsDTO.setCity(AddressConstants.CITY);
		orderAddressWsDTO.setLine1(AddressConstants.STREET);
		orderAddressWsDTO.setLine2(AddressConstants.HOUSE);
		orderAddressWsDTO.setPhoneNumber(AddressConstants.PHONE);
		return orderAddressWsDTO;
	}

	private OrderUpdateRequestWsDTO initOrderDTO()
	{
		final OrderUpdateRequestWsDTO orderDTO = new OrderUpdateRequestWsDTO();
		orderDTO.setTotalPrice(1.0);
		orderDTO.setTotalDiscount(2.0);
		orderDTO.setDeliveryCost(3.0);
		orderDTO.setTotalTax(4.0);
		orderDTO.setCountry(OrderConstants.ORDER_COUNTRY);
		orderDTO.setCurrency(OrderConstants.ORDER_CURRENCY);
		return orderDTO;
	}

	private OrderUpdateEntryRequestWsDTO initOrderEntryDTO(final String productIsbn, final int quantity)
	{
		final OrderUpdateEntryRequestWsDTO entry = new OrderUpdateEntryRequestWsDTO();
		entry.setSapProductCode(productIsbn);
		entry.setIsbn(productIsbn);
		entry.setQuantity(quantity);
		entry.setBasePrice(2.0);
		entry.setTotalPrice(3.0);
		createAndSetDiscounts(entry);
		return entry;
	}

	private UpdateMessageWsDTO initUpdateMessageDTO(final String productIsbn)
	{
		final UpdateMessageWsDTO messageDTO = new UpdateMessageWsDTO();
		messageDTO.setMessageType(UpdateMessageWsDTO.MessageType.INFO);
		messageDTO.setCode(productIsbn);
		messageDTO.setMessage("Order entry updated");
		messageDTO.setIsbn(productIsbn);
		return messageDTO;
	}

	private void checkCartEntryDiscount(final AbstractOrderEntryModel cartEntry, final String discountCode,
			final boolean absolute, final Double value)
	{
		final List<DiscountValue> discountValues = cartEntry.getDiscountValues();
		final Optional<DiscountValue> optionalDiscountValue = findDiscountValueByCode(discountCode, discountValues);

		Assert.assertTrue(String.format("DiscountValue with code [%s] should be in cart entry.", discountCode),
				optionalDiscountValue.isPresent());

		final DiscountValue discountValue = optionalDiscountValue.get();
		checkDiscountValueState(absolute, value, discountValue);
	}

	private void checkCartDiscount(final AbstractOrderModel orderModel, final String discountCode,
			final boolean absolute, final Double value)
	{
		final List<DiscountValue> discountValues = orderModel.getGlobalDiscountValues();
		final Optional<DiscountValue> optionalDiscountValue = findDiscountValueByCode(discountCode,
				discountValues);

		Assert.assertTrue(String.format("DiscountValue with code [%s] should be in cart", discountCode),
				optionalDiscountValue.isPresent());

		final DiscountValue discountValue = optionalDiscountValue.get();
		checkDiscountValueState(absolute, value, discountValue);
	}

	private Optional<DiscountValue> findDiscountValueByCode(final String discountCode, final List<DiscountValue> discountValues)
	{
		return discountValues.stream()
				.filter(discountValue -> discountCode.equals(discountValue.getCode()))
				.findFirst();
	}


	private void checkDiscountValueState(final boolean absolute, final Double value, final DiscountValue discountValue)
	{
		Assert.assertEquals("DiscountValue.absolute", absolute, discountValue.isAbsolute());
		Assert.assertEquals("DiscountValue.value", value, Double.valueOf(discountValue.getValue()));
	}

}
