package com.wiley.ws.core.wileysubscription.populator.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.paypal.hybris.enums.PaymentActionType;
import com.paypal.hybris.model.PaypalPaymentInfoModel;
import com.wiley.core.model.WileySubscriptionModel;
import com.wiley.ws.core.wileysubscription.dto.PaymentInfoUpdateRequestWsDTO;
import com.wiley.ws.core.wileysubscription.dto.WileySubscriptionAddressWsDTO;


/**
 * Default unit test
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PayPalPaymentInfoCreatingStrategyUnitTest
{

	@Mock
	private ModelService modelServiceMock;

	@Mock
	private Populator<WileySubscriptionAddressWsDTO, AddressModel> wileySubscriptionAddressWsReversePopulatorMock;

	@InjectMocks
	private PayPalPaymentInfoCreatingStrategy payPalPaymentInfoCreatingStrategy;

	// Test data

	@Mock
	private PaypalPaymentInfoModel paypalPaymentInfoModelMock;

	@Mock
	private WileySubscriptionModel wileySubscriptionModelMock;

	@Mock
	private CustomerModel customerModelMock;

	@Before
	public void setUp() throws Exception
	{
		when(wileySubscriptionModelMock.getCustomer()).thenReturn(customerModelMock);
	}

	@Test
	public void testIsSupportedPositiveCase()
	{
		// Given
		// no changes in test data

		// When
		final boolean supported = payPalPaymentInfoCreatingStrategy.isSupported(paypalPaymentInfoModelMock);

		// Then
		assertTrue(supported);
	}

	@Test
	public void testIsSupportedNegativeCase()
	{
		// Given
		// no changes in test data

		// When
		final boolean supported = payPalPaymentInfoCreatingStrategy.isSupported(mock(CreditCardPaymentInfoModel.class));

		// Then
		assertFalse(supported);
	}

	@Test
	public void testIsSupportedWithNullParameter()
	{
		// Given
		// no changes in test data

		// When
		final boolean supported = payPalPaymentInfoCreatingStrategy.isSupported(null);

		// Then
		assertFalse(supported);
	}

	@Test
	public void testGetOrCreateNewSupportedPaymentInfoPositiveCase()
	{
		// Given
		// no changes in test data

		// When
		final PaypalPaymentInfoModel actualPaymentInfo =
				payPalPaymentInfoCreatingStrategy.getOrCreateNewSupportedPaymentInfo(paypalPaymentInfoModelMock,
						wileySubscriptionModelMock);

		// Then
		assertSame(paypalPaymentInfoModelMock, actualPaymentInfo);
		verifyZeroInteractions(modelServiceMock, wileySubscriptionAddressWsReversePopulatorMock);
	}

	@Test
	public void testGetOrCreateNewSupportedPaymentInfoWhenPaymentInfoIsNotSupported()
	{
		// Given
		when(modelServiceMock.create(eq(PaypalPaymentInfoModel.class))).thenReturn(paypalPaymentInfoModelMock);
		final CreditCardPaymentInfoModel invalidPaymentInfo = mock(CreditCardPaymentInfoModel.class);

		// When
		final PaypalPaymentInfoModel actualPaymentInfo =
				payPalPaymentInfoCreatingStrategy.getOrCreateNewSupportedPaymentInfo(invalidPaymentInfo,
						wileySubscriptionModelMock);

		// Then
		assertSame(paypalPaymentInfoModelMock, actualPaymentInfo);

		verify(modelServiceMock).create(eq(PaypalPaymentInfoModel.class));
		verify(modelServiceMock).remove(eq(invalidPaymentInfo));
		verify(modelServiceMock, never()).save(eq(paypalPaymentInfoModelMock));

		verify(paypalPaymentInfoModelMock).setOwner(eq(wileySubscriptionModelMock));
		verify(paypalPaymentInfoModelMock).setUser(eq(customerModelMock));
		verify(wileySubscriptionModelMock).setPaymentInfo(eq(paypalPaymentInfoModelMock));

		verifyZeroInteractions(wileySubscriptionAddressWsReversePopulatorMock);
	}

	@Test
	public void testGetOrCreateNewSupportedPaymentInfoWhenParameterIsNull()
	{
		// Given
		when(modelServiceMock.create(eq(PaypalPaymentInfoModel.class))).thenReturn(paypalPaymentInfoModelMock);

		// When
		final PaypalPaymentInfoModel actualPaymentInfo =
				payPalPaymentInfoCreatingStrategy.getOrCreateNewSupportedPaymentInfo(null,
						wileySubscriptionModelMock);

		// Then
		assertSame(paypalPaymentInfoModelMock, actualPaymentInfo);

		verify(modelServiceMock).create(eq(PaypalPaymentInfoModel.class));
		verify(modelServiceMock, never()).remove(any(PaymentInfoModel.class));
		verify(modelServiceMock, never()).save(eq(paypalPaymentInfoModelMock));

		verify(paypalPaymentInfoModelMock).setOwner(eq(wileySubscriptionModelMock));
		verify(paypalPaymentInfoModelMock).setUser(eq(customerModelMock));
		verify(wileySubscriptionModelMock).setPaymentInfo(eq(paypalPaymentInfoModelMock));

		verifyZeroInteractions(wileySubscriptionAddressWsReversePopulatorMock);
	}

	@Test
	public void testGetPaymentInfoClass()
	{
		// Given
		// no changes in test data

		// When
		final Class<PaypalPaymentInfoModel> paymentInfoClass = payPalPaymentInfoCreatingStrategy.getPaymentInfoClass();

		// Then
		assertNotNull(paymentInfoClass);
		assertTrue(PaypalPaymentInfoModel.class.isAssignableFrom(paymentInfoClass));
	}

	@Test
	public void testPopulateWithNonNullFields()
	{
		// Given
		final AddressModel addressModelMock = mock(AddressModel.class);
		when(modelServiceMock.create(AddressModel.class)).thenReturn(addressModelMock);

		final PaymentInfoUpdateRequestWsDTO paymentInfoUpdateRequestWsDTOMock = mock(PaymentInfoUpdateRequestWsDTO.class);
		final WileySubscriptionAddressWsDTO addressWsDTOMock = mock(WileySubscriptionAddressWsDTO.class);

		when(paymentInfoUpdateRequestWsDTOMock.getPaymentAddress()).thenReturn(addressWsDTOMock);

		// When
		payPalPaymentInfoCreatingStrategy.populate(paymentInfoUpdateRequestWsDTOMock, paypalPaymentInfoModelMock);

		// Then
		verify(modelServiceMock).create(eq(AddressModel.class));

		verify(paypalPaymentInfoModelMock).setCode(eq("PAYPAL PAYMENT"));
		verify(paypalPaymentInfoModelMock).setPaymentAction(eq(PaymentActionType.SALE));
		verify(wileySubscriptionAddressWsReversePopulatorMock).populate(same(addressWsDTOMock), same(addressModelMock));
		verify(paypalPaymentInfoModelMock).setBillingAddress(same(addressModelMock));
		verify(addressModelMock).setOwner(paypalPaymentInfoModelMock);
	}

	@Test
	public void testPopulateWithNullFields()
	{
		// Given
		final PaymentInfoUpdateRequestWsDTO paymentInfoUpdateRequestWsDTOMock = mock(PaymentInfoUpdateRequestWsDTO.class,
				(Answer) invocation -> null);

		// When
		payPalPaymentInfoCreatingStrategy.populate(paymentInfoUpdateRequestWsDTOMock, paypalPaymentInfoModelMock);

		// Then
		verifyZeroInteractions(modelServiceMock, wileySubscriptionAddressWsReversePopulatorMock);
		verify(paypalPaymentInfoModelMock).setCode(eq("PAYPAL PAYMENT"));
		verify(paypalPaymentInfoModelMock).setPaymentAction(eq(PaymentActionType.SALE));
		verify(paypalPaymentInfoModelMock, never()).setBillingAddress(any());
	}

	@Test
	public void testPopulateWhenRequiredFieldsAlreadyExists()
	{
		// Given
		final PaymentInfoUpdateRequestWsDTO paymentInfoUpdateRequestWsDTOMock = mock(PaymentInfoUpdateRequestWsDTO.class);
		when(paypalPaymentInfoModelMock.getCode()).thenReturn("PAYPAL PAYMENT");
		when(paypalPaymentInfoModelMock.getPaymentAction()).thenReturn(PaymentActionType.SALE);

		// When
		payPalPaymentInfoCreatingStrategy.populate(paymentInfoUpdateRequestWsDTOMock, paypalPaymentInfoModelMock);

		// Then
		verifyZeroInteractions(modelServiceMock, wileySubscriptionAddressWsReversePopulatorMock);
		verify(paypalPaymentInfoModelMock, never()).setCode(any());
		verify(paypalPaymentInfoModelMock, never()).setPaymentAction(any());
	}

}