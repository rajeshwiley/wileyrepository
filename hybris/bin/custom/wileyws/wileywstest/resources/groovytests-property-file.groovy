/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 */

// HOST, PORT and SECURE_PORT initial values are specified in TestConfigFactory and can be overwritten by system properties

BASE_SITE = 'wsTest'
INTEGRATION_BASE_SITE = 'wsIntegrationTest'


DEFAULT_HTTP_URI = "http://${HOST}:${PORT}"
DEFAULT_HTTPS_URI = "https://${HOST}:${SECURE_PORT}"

BASE_PATH = "/${WEBROOT}/${VERSION}"
BASE_PATH_WITH_SITE = "/${WEBROOT}/${VERSION}/${BASE_SITE}"
BASE_PATH_WITH_INTEGRATION_SITE = "/${WEBROOT}/${VERSION}/${INTEGRATION_BASE_SITE}"

FULL_BASE_URI = DEFAULT_HTTP_URI + BASE_PATH_WITH_SITE
FULL_SECURE_BASE_URI = DEFAULT_HTTPS_URI + BASE_PATH_WITH_SITE

OAUTH2_TOKEN_URI = DEFAULT_HTTPS_URI
OAUTH2_TOKEN_ENDPOINT_PATH = "/${WEBROOT}/oauth/token"
OAUTH2_TOKEN_ENDPOINT_URI = OAUTH2_TOKEN_URI + OAUTH2_TOKEN_ENDPOINT_PATH


WILEY_B2C_BASE_PATH = '/wileyb2cstorefront'

SITE_MAP_URI = DEFAULT_HTTPS_URI
SITE_MAP_ENDPOINT_PATH = "${WILEY_B2C_BASE_PATH}/sitemap.xml"

CONTENT_SEARCH_URLS_URI = DEFAULT_HTTPS_URI
CONTENT_SEARCH_URLS_ENDPOINT_PATH = "${WILEY_B2C_BASE_PATH}/contentsearch/urls"

OAUTH2_CALLBACK_URI = "http://${HOST}:${PORT}/rest/oauth2_callback"

TRANSACTION_URI = DEFAULT_HTTPS_URI + BASE_PATH
TRANSACTION_ENDPOINT_PATH = "/transactions"
TRANSACTION_ENDPOINT_URI = TRANSACTION_URI + TRANSACTION_ENDPOINT_PATH

HTTP_WEBROOT = DEFAULT_HTTP_URI + "/${WEBROOT}"
HTTPS_WEBROOT = DEFAULT_HTTPS_URI + "/${WEBROOT}"


CLIENT_ID = 'mobile_android'
CLIENT_SECRET = 'secret'
CLIENT_CREDENTIALS_GRANT_TYPE = 'client_credentials'
TRUSTED_CLIENT_ID = 'trusted_client'
TRUSTED_CLIENT_SECRET = 'secret'
AGS_CLIENT_ID = 'ags'
AGS_CLIENT_SECRET = 'qeZt7UmeZ8RhwTQkZ8pKMfz8'
AGS_SCOPE = 'extended'
WEL_CLIENT_ID = 'wel'
WEL_CLIENT_SECRET = 'SR3pM3xzteunRddPQzdTKnn2'
WEL_SCOPE = 'extended'
ESB_CLIENT_ID = 'esb'
ESB_CLIENT_SECRET = 'Bu4sQjiNTbg59CyxUBViXHeX'
ESB_SCOPE = 'extended'
ETL_CLIENT_ID = 'etl'
ETL_CLIENT_SECRET = 'WXRYRiKgbeBFY5tpmI8AXw2T'
ETL_SCOPE = 'extended'
WEL_FULFILLMENT_CLIENT_ID='wel_fulfillment'
WEL_FULFILLMENT_CLIENT_SECRET='BXG5uqSHzVqVBzfyL8qVczyv'
WEL_FULFILLMENT_SCOPE = 'extended'

FAIL_ON_NAMING_CONVENTION_ERROR = false
JMETER_PROXY_RECORDING = false