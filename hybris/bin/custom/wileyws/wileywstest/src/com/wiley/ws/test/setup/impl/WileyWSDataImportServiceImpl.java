package com.wiley.ws.test.setup.impl;

import de.hybris.platform.commerceservices.setup.SetupImpexService;
import de.hybris.platform.commerceservices.setup.SetupSolrIndexerService;
import de.hybris.platform.commerceservices.setup.SetupSyncJobService;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.cronjob.model.TriggerModel;
import de.hybris.platform.servicelayer.cronjob.CronJobService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.impex.ImportService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Collection;
import java.util.function.Function;

import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wiley.ws.test.setup.WileyWSDataImportService;


public class WileyWSDataImportServiceImpl implements WileyWSDataImportService
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyWSDataImportService.class);
	private static final String ACTIVE_ATTRIBUTE = "active";
	private static final boolean FAIL_IF_MISSING = false;

	@Resource
	private ModelService modelService;

	@Resource
	private ImportService importService;

	@Resource
	private SetupImpexService setupImpexService;

	@Resource
	private SetupSyncJobService setupSyncJobService;

	@Resource
	private SetupSolrIndexerService setupSolrIndexerService;

	@Resource
	private CronJobService cronJobService;

	@Override
	public boolean deactivateCronJob(final String cronJobCode)
	{
		toggleTriggersState(cronJobCode, ExecutionType.CRON_JOB, ExecutionStateAction.DEACTIVATE);
		return setActiveItem(cronJobCode, cronJobService::getCronJob, false);
	}

	@Override
	public boolean activateCronJob(final String cronJobCode, final boolean activateTriggers)
	{
		if (activateTriggers)
		{
			toggleTriggersState(cronJobCode, ExecutionType.CRON_JOB, ExecutionStateAction.ACTIVATE);
		}
		return setActiveItem(cronJobCode, cronJobService::getCronJob, true);
	}

	/**
	 * Set active attribute for model
	 *
	 * @param id
	 * 		Item identificator
	 * @param itemRetrieval
	 * 		Function to get item
	 * @param isActive
	 * 		Flag value
	 * @return True if item was found and updated, otherwise false
	 */
	private boolean setActiveItem(final String id, final Function<String, ItemModel> itemRetrieval, final boolean isActive)
	{
		try
		{
			ItemModel item = itemRetrieval.apply(id);
			modelService.setAttributeValue(item, ACTIVE_ATTRIBUTE, isActive);
			modelService.save(item);
			LOG.info("Item with id={} was set to active={}", id, isActive);
			return true;
		}
		catch (UnknownIdentifierException e)
		{
			LOG.info("Item with id={} was skipped due to wasn't found", id);
			return false;
		}
	}

	private void toggleTriggersState(final String code, final ExecutionType type, final ExecutionStateAction action)
	{
		try
		{
			Collection<TriggerModel> triggers = null;
			if (type == ExecutionType.JOB)
			{
				triggers = cronJobService.getJob(code).getTriggers();
			}
			else if (ExecutionType.CRON_JOB == type)
			{
				triggers = cronJobService.getCronJob(code).getTriggers();
			}
			if (CollectionUtils.isNotEmpty(triggers))
			{
				boolean setActive = ExecutionStateAction.ACTIVATE == action;
				triggers
						.stream()
						.filter(trigger -> trigger.getActive() != setActive)
						.forEach(trigger -> setActiveItem(null, id -> trigger, setActive));
			}
		}
		catch (UnknownIdentifierException e)
		{
			LOG.info("Triggers for Item with code={} was skipped due to wasn't found", code);
		}
	}

	public enum ExecutionType
	{
		JOB, CRON_JOB
	}

	public enum ExecutionStateAction
	{
		ACTIVATE, DEACTIVATE
	}

}
