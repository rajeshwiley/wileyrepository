package com.wiley.ws.test.setup;

import de.hybris.platform.cronjob.model.CronJobModel;


/**
 * Data import service
 */
public interface WileyWSDataImportService
{
	/**
	 * Set {@link CronJobModel#ACTIVE} flag
	 *
	 * @param cronJobCode
	 * 		{@link CronJobModel#CODE}
	 * @return True if CronJob was found and updated, otherwise false
	 */
	boolean deactivateCronJob(String cronJobCode);

	/**
	 * Set {@link CronJobModel#ACTIVE} flag
	 *
	 * @param cronJobCode
	 * 		{@link CronJobModel#CODE}
	 * @param activateTriggers
	 * @return True if CronJob was found and updated, otherwise false
	 */
	boolean activateCronJob(String cronJobCode, boolean activateTriggers);


}
