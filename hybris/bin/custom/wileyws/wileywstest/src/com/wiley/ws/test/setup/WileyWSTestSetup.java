/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.ws.test.setup;

import de.hybris.platform.commerceservices.dataimport.impl.CoreDataImportService;
import de.hybris.platform.commerceservices.dataimport.impl.SampleDataImportService;
import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.commerceservices.setup.data.ImportData;
import de.hybris.platform.commerceservices.setup.events.CoreDataImportedEvent;
import de.hybris.platform.commerceservices.setup.events.SampleDataImportedEvent;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;
import de.hybris.platform.core.initialization.SystemSetupParameterMethod;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.CronJobService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.ws.test.constants.WileyWSTestConstants;


/**
 * The type Wiley ws test setup.
 */
@SystemSetup(extension = WileyWSTestConstants.EXTENSIONNAME)
public class WileyWSTestSetup extends AbstractSystemSetup
{
	/**
	 * The constant WS_INTEGRATION.
	 */
	public static final String WS_INTEGRATION = "wsIntegrationTest";
	/**
	 * The constant WS_TEST.
	 */
	public static final String WS_TEST = "wsTest";
	public static final String WEL = "wel";
	public static final String WILEY_COM = "wileycom";
	public static final String WILEY = "wiley";
	public static final String WILEY_AS = "as";
	private static final Logger LOG = Logger.getLogger(WileyWSTestSetup.class);
	private CoreDataImportService coreDataImportService;
	private SampleDataImportService sampleDataImportService;
	@Resource
	private CronJobService cronJobService;
	@Resource
	private WileyWSDataImportService wileyWSDataImportService;

	/**
	 * Gets initialization options.
	 *
	 * @return the initialization options
	 */
	@Override
	@SystemSetupParameterMethod
	public List<SystemSetupParameter> getInitializationOptions()
	{
		final List<SystemSetupParameter> params = new ArrayList<SystemSetupParameter>();

		params.add(createBooleanSystemSetupParameter(CoreDataImportService.IMPORT_CORE_DATA, "Import Core Data", true));
		params.add(createBooleanSystemSetupParameter(SampleDataImportService.IMPORT_SAMPLE_DATA, "Import Sample Data", true));
		params.add(createBooleanSystemSetupParameter(CoreDataImportService.ACTIVATE_SOLR_CRON_JOBS, "Activate Solr Cron Jobs",
				false));

		return params;
	}

	/**
	 * Create project data.
	 *
	 * @param context
	 * 		the context
	 */
	@SystemSetup(type = SystemSetup.Type.PROJECT, process = SystemSetup.Process.ALL)
	public void createProjectData(final SystemSetupContext context)
	{
		final List<ImportData> importData = new ArrayList<ImportData>();

		final ImportData wsIntegrationImportData = new ImportData();
		wsIntegrationImportData.setProductCatalogName(WS_TEST);
		wsIntegrationImportData.setContentCatalogNames(Arrays.asList(WS_TEST, WS_INTEGRATION));
		wsIntegrationImportData.setStoreNames(Arrays.asList(WS_TEST, WS_INTEGRATION));
		importData.add(wsIntegrationImportData);
		if (this.getBooleanSystemSetupParameter(context, CoreDataImportService.IMPORT_CORE_DATA))
		{
			getCoreDataImportService().execute(this, context, importData);
			getEventService().publishEvent(new CoreDataImportedEvent(context, importData));
			getSetupImpexService().importImpexFile(
					"/wileywstest/import/coredata/contentCatalogs/wsTestContentCatalog/cmsParagraphComponent-units.impex", true,
					false);
		}

		if (this.getBooleanSystemSetupParameter(context, SampleDataImportService.IMPORT_SAMPLE_DATA))
		{
			loadOptionalSampleData();

			getSampleDataImportService().execute(this, context, importData);
			getEventService().publishEvent(new SampleDataImportedEvent(context, importData));

			getSetupImpexService().importImpexFile("/wileywstest/import/sampledata/"
					+ "user-orders.impex", true, false);
			getSetupImpexService().importImpexFile("/wileywstest/import/sampledata/b2b-units.impex", true, false);
			getSetupImpexService().importImpexFile("/wileywstest/import/sampledata/oauth-client-test-configuration.impex",
					true, false);
			getSetupImpexService().importImpexFile("/wileywstest/import/sampledata/inventory.impex", true, false);
			getSetupImpexService()
					.importImpexFile("/wileywstest/import/sampledata/test-dynamic-bp-definitions.impex", true, false);
			getSetupImpexService().importImpexFile(
					"/wileywstest/import/sampledata/productCatalogs/welProductCatalog/products-freetrial.impex", true, false);
			loadIntegrationData();
			loadMultiDimensionalData();
			loadSiteMapData();
			loadAsSampleData();
			fullProductCatalogSyncAndIndexing();
			// load sample cart data after product catalog is synced up
			loadAsCartSampleData();
			loadAsOrderSampleData();
			synchronizeContentCatalog();
			executeCronJob("wileyb2c-ContentSearchConfigJob");
			executeCronJob("wileyb2c-SiteMapMediaJob");
		}
	}

	/**
	 * Load optional sample data.
	 */
	protected void loadOptionalSampleData()
	{
		final List<String> extensionNames = Registry.getCurrentTenant().getTenantSpecificExtensionNames();
		if (extensionNames.contains("acceleratorwebservicesaddon"))
		{
			getSetupImpexService().importImpexFile("/wileywstest/import/acceleratorwebservicesaddon/solr.impex", true, true);
		}
	}

	/**
	 * Load integration data.
	 */
	protected void loadIntegrationData()
	{
		final List<String> extensionNames = Registry.getCurrentTenant().getTenantSpecificExtensionNames();
		if (extensionNames.contains("cissampledata"))
		{
			getSetupImpexService().importImpexFile("/wileywstest/import/integration/cis-integration-data.impex", true);
		}

		if (extensionNames.contains("omssampledata"))
		{
			getSetupImpexService().importImpexFile("/wileywstest/import/integration/oms-integration-data.impex", true);
		}
	}

	/**
	 * Load multi dimensional data.
	 */
	protected void loadMultiDimensionalData()
	{
		getSetupImpexService().importImpexFile(
				"/wileywstest/import/sampledata/productCatalogs/wsTestProductCatalog/dimension-categories.impex", true, false);
		getSetupImpexService().importImpexFile(
				"/wileywstest/import/sampledata/productCatalogs/wsTestProductCatalog/dimension-products.impex", true, false);
		getSetupImpexService().importImpexFile(
				"/wileywstest/import/sampledata/productCatalogs/wsTestProductCatalog/dimension-products-prices.impex", true,
				false);
		getSetupImpexService().importImpexFile(
				"/wileywstest/import/sampledata/productCatalogs/wsTestProductCatalog/dimension-products-stock-levels.impex", true,
				false);
	}

	/**
	 *
	 */
	private void fullProductCatalogSyncAndIndexing()
	{
		getSetupSyncJobService().executeCatalogSyncJob(String.format("%sProductCatalog", WS_TEST));
		getSetupSyncJobService().executeCatalogSyncJob(String.format("%sProductCatalog", WEL));
		getSetupSyncJobService().executeCatalogSyncJob(String.format("%sProductCatalog", WILEY));
		getSetupSyncJobService().executeCatalogSyncJob(String.format("%sProductCatalog", WILEY_AS));
	}

	/**
	 * Gets core data import service.
	 *
	 * @return the core data import service
	 */
	public CoreDataImportService getCoreDataImportService()
	{
		return coreDataImportService;
	}

	/**
	 * Sets core data import service.
	 *
	 * @param coreDataImportService
	 * 		the core data import service
	 */
	@Required
	public void setCoreDataImportService(final CoreDataImportService coreDataImportService)
	{
		this.coreDataImportService = coreDataImportService;
	}

	/**
	 * Gets sample data import service.
	 *
	 * @return the sample data import service
	 */
	public SampleDataImportService getSampleDataImportService()
	{
		return sampleDataImportService;
	}

	/**
	 * Sets sample data import service.
	 *
	 * @param sampleDataImportService
	 * 		the sample data import service
	 */
	@Required
	public void setSampleDataImportService(final SampleDataImportService sampleDataImportService)
	{
		this.sampleDataImportService = sampleDataImportService;
	}

	protected void loadSiteMapData()
	{
		getSetupImpexService().importImpexFile(
				"/wileywstest/import/sampledata/productCatalogs/wileyProductCatalog/products.impex", true,
				false);
		getSetupImpexService().importImpexFile(
				"/wileywstest/import/sampledata/contentCatalogs/wileycomContentCatalog/cms-content.impex", true,
				false);
		getSetupImpexService().importImpexFile(
				"/wileywstest/import/sampledata/contentCatalogs/micrositesContentCatalog/cms-content.impex", true,
				false);
		getSetupImpexService().importImpexFile(
				"/wileywstest/import/sampledata/contentCatalogs/dtsContentCatalog/cms-content.impex", true,
				false);
	}

	protected void loadAsSampleData()
	{
		getSetupImpexService().importImpexFile(
				"/wileywstest/import/sampledata/productCatalogs/asProductCatalog/products.impex", true,
				false);
		getSetupImpexService().importImpexFile(
				"/wileywstest/import/sampledata/productCatalogs/asProductCatalog/prices.impex", true,
				false);
		getSetupImpexService().importImpexFile(
				"/wileywstest/import/sampledata/productCatalogs/asProductCatalog/discounts.impex", true,
				false);
		getSetupImpexService().importImpexFile(
				"/wileywstest/import/sampledata/productCatalogs/asProductCatalog/users.impex", true,
				false);
	}

	protected void loadAsCartSampleData()
	{
		getSetupImpexService().importImpexFile(
				"/wileywstest/import/sampledata/productCatalogs/asProductCatalog/carts.impex", true,
				false);
	}

	protected void loadAsOrderSampleData()
	{
		getSetupImpexService().importImpexFile(
				"/wileywstest/import/sampledata/productCatalogs/asProductCatalog/orders.impex", true,
				false);
	}

	private void synchronizeContentCatalog()
	{
		getSetupSyncJobService().executeCatalogSyncJob(String.format("%sContentCatalog", WILEY_COM));
	}

	private void executeCronJob(final String cronJobCode)
	{
		final CronJobModel cronJob = cronJobService.getCronJob(cronJobCode);
		final Boolean activateDeactivate = !cronJob.getActive();
		if (activateDeactivate)
		{
			wileyWSDataImportService.activateCronJob(cronJobCode, false);
		}
		cronJobService.performCronJob(cronJob, true);
		if (activateDeactivate)
		{
			wileyWSDataImportService.deactivateCronJob(cronJobCode);
		}
		LOG.info("CronJob with code={} was executed " + cronJobCode);
	}
}
