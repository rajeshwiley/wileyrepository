package com.wiley.ws.test.constants;

/**
 * The type Wiley ws test constants.
 */
@SuppressWarnings("PMD")
public final class WileyWSTestConstants extends GeneratedWileyWSTestConstants
{
	/**
	 * The constant EXTENSIONNAME.
	 */
	public static final String EXTENSIONNAME = "wileywstest";

	private WileyWSTestConstants()
	{
		//empty
	}


}
