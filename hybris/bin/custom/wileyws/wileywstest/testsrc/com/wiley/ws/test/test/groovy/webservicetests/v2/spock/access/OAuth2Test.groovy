/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 */

package com.wiley.ws.test.test.groovy.webservicetests.v2.spock.access

import com.wiley.ws.test.test.groovy.webservicetests.v2.spock.AbstractSpockFlowTest
import de.hybris.bootstrap.annotations.ManualTest
import groovyx.net.http.HttpResponseDecorator
import spock.lang.Ignore
import spock.lang.Unroll

import static groovyx.net.http.ContentType.JSON
import static groovyx.net.http.ContentType.URLENC
import static org.apache.http.HttpStatus.SC_BAD_REQUEST
import static org.apache.http.HttpStatus.SC_UNAUTHORIZED

@ManualTest
@Unroll
class OAuth2Test extends AbstractSpockFlowTest {

    static final def CLIENTS = [
            [
                    clientId    : getAgsClientId(),
                    clientSecret: getAgsClientSecret(),
                    scope       : getAgsScope()
            ],
            [
                    clientId    : getWelClientId(),
                    clientSecret: getWelClientSecret(),
                    scope       : getWelScope()
            ],
            [
                    clientId    : getEsbClientId(),
                    clientSecret: getEsbClientSecret(),
                    scope       : getEsbScope()
            ],
            [
                    clientId    : getEtlClientId(),
                    clientSecret: getEtlClientSecret(),
                    scope       : getEtlScope()
            ]
    ]

    static final def USERS = [
            [
                    username: CUSTOMER_USERNAME,
                    password: CUSTOMER_PASSWORD
            ],
            [
                    username: CUSTOMER_MANAGER_USERNAME,
                    password: CUSTOMER_MANAGER_PASSWORD
            ]
    ]

    def "Get token using client credentials : #params"() {
        expect:
        getOAuth2TokenUsingClientCredentials(restClient, params.clientId, params.clientSecret, params.scope)

        where:
        params << CLIENTS
    }


    @Ignore('password grand type is NOT supported. Only client_credentials.')
    def "Get token using password : #params"() {
        expect:
        getOAuth2TokenUsingPassword(restClient, params.clientId, params.clientSecret, params.username, params.password)

        where:
        params << [CLIENTS, USERS].combinations().collect { it[0] + it[1] }
    }


    @Ignore('refresh_scope grand type is NOT supported. Only client_credentials.')
    def "Refresh token : #params"() {
        given:
        def token = getOAuth2TokenUsingClientCredentials(restClient, params.clientId, params.clientSecret)

        expect:
        refreshOAuth2Token(restClient, token.refresh_token, params.clientId, params.clientSecret, params.redirectUri)

        where:
        params << CLIENTS
    }

    def "Get token with wrong client id : #params"() {
        when:
        HttpResponseDecorator response = restClient.post(
                uri: getOAuth2TokenUri(),
                path: getOAuth2TokenPath(),
                body: [
                        'grant_type'   : getClientCredentialsGrantType(),
                        'client_id'    : 'WRONG',
                        'client_secret': params.clientSecret
                ],
                contentType: JSON,
                requestContentType: URLENC)

        then:
        with(response) {
            status == SC_UNAUTHORIZED
            data.error == 'invalid_client'
            data.error_description == 'Bad client credentials'
        }

        where:
        params << CLIENTS
    }

    def "Get token with wrong client secret : #params"() {
        when:
        HttpResponseDecorator response = restClient.post(
                uri: getOAuth2TokenUri(),
                path: getOAuth2TokenPath(),
                body: [
                        'grant_type'   : getClientCredentialsGrantType(),
                        'client_id'    : params.clientId,
                        'client_secret': 'WRONG'
                ],
                contentType: JSON,
                requestContentType: URLENC)

        then:
        with(response) {
            status == SC_UNAUTHORIZED
            data.error == 'invalid_client'
            data.error_description == 'Bad client credentials'
        }

        where:
        params << CLIENTS
    }

    def "Get token with wrong grant type"() {
        when:
        HttpResponseDecorator response = restClient.post(
                uri: getOAuth2TokenUri(),
                path: getOAuth2TokenPath(),
                body: [
                        'grant_type'   : 'WRONG',
                        'client_id'    : getTrustedClientId(),
                        'client_secret': getTrustedClientSecret()
                ],
                contentType: JSON,
                requestContentType: URLENC)

        then:
        with(response) {
            status == SC_BAD_REQUEST
            data.error == 'unsupported_grant_type'
            data.error_description == 'Unsupported grant type: WRONG'
        }
    }

    def "Get token with wrong scope parameter"() {
        when:
        HttpResponseDecorator response = restClient.post(
                uri: getOAuth2TokenUri(),
                path: getOAuth2TokenPath(),
                body: [
                        'grant_type'   : getClientCredentialsGrantType(),
                        'client_id'    : getTrustedClientId(),
                        'client_secret': getTrustedClientSecret(),
                        'scope'        : 'WRONG'
                ],
                contentType: JSON,
                requestContentType: URLENC)

        then:
        with(response) {
            status == SC_BAD_REQUEST
            data.error == 'invalid_scope'
            data.error_description == 'Invalid scope: WRONG'
        }
    }

    // TODO: need to be checked in scope of https://jira.wiley.ru/browse/ECSC-17258
    @Ignore('Brute force counter doesnt work')
    def "Test brute force attack"() {
        given: "Registered user"
        authorizeClient(restClient)
        def customer = [
                'id'      : 'bruteforceuser@test.com',
                'password': CUSTOMER_PASSWORD
        ]

        when: "try to get token 5 times with wrong password"
        def errorData1 = getOAuth2TokenUsingPassword(restClient, getClientId(), getClientSecret(), customer.id, 'wrongPassword', false);
        def errorData2 = getOAuth2TokenUsingPassword(restClient, getClientId(), getClientSecret(), customer.id, 'wrongPassword', false);
        def errorData3 = getOAuth2TokenUsingPassword(restClient, getClientId(), getClientSecret(), customer.id, 'wrongPassword', false);
        def errorData4 = getOAuth2TokenUsingPassword(restClient, getClientId(), getClientSecret(), customer.id, 'wrongPassword', false);
        def errorData5 = getOAuth2TokenUsingPassword(restClient, getClientId(), getClientSecret(), customer.id, 'wrongPassword', false);
        def errorData6 = getOAuth2TokenUsingPassword(restClient, getClientId(), getClientSecret(), customer.id, customer.password, false);

        then: "user account will be disabled"
        errorData1.error == 'invalid_grant'
        errorData1.error_description == 'Bad credentials'
        errorData2.error == 'invalid_grant'
        errorData2.error_description == 'Bad credentials'
        errorData3.error == 'invalid_grant'
        errorData3.error_description == 'Bad credentials'
        errorData4.error == 'invalid_grant'
        errorData4.error_description == 'Bad credentials'
        errorData5.error == 'invalid_grant'
        errorData5.error_description == 'Bad credentials'
        // TODO: need to be update in scope of https://jira.wiley.ru/browse/ECSC-17258
        errorData6.errors[0].type == 'InvalidGrantError'
        errorData6.errors[0].message == 'User is disabled'
    }
}
