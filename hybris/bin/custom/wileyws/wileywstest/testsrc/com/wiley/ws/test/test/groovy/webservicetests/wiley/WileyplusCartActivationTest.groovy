package com.wiley.ws.test.test.groovy.webservicetests.wiley

import com.wiley.ws.test.test.groovy.webservicetests.v3.spock.AbstractSpockFlowTest
import de.hybris.bootstrap.annotations.ManualTest
import groovyx.net.http.HttpResponseDecorator
import org.junit.Ignore
import spock.lang.IgnoreIf
import spock.lang.Unroll

import static com.wiley.ws.test.test.groovy.webservicetests.wiley.TestValidationUtils.*
import static groovyx.net.http.ContentType.JSON
import static org.apache.http.HttpStatus.SC_BAD_REQUEST
import static org.apache.http.HttpStatus.SC_OK

@ManualTest
@Unroll
class WileyplusCartActivationTest extends AbstractSpockFlowTest {
    def static getCartActivationTemplate = {
        [
                ssoToken            : "abcd",
                country             : "US",
                language            : "en",
                currency            : "USD",
                registrationRequired: true,
                userId              : "usr12345",
                ficeCode            : "ficeCodeVal",
                purchaseOptionsDto  : [
                        purchaseOptionAvailable   : true,
                        regCodeActivationAvailable: true,
                        gracePeriodDuration       : 14,
                        gracePeriodUsed           : true,
                ],
                returnURL           : [
                        url    : "https://wiley.instructure.com/callback",
                        message: "Return to Canvas"
                ],
                continueURL         : [
                        url    : "https://inegration.wiley.com/service=lti",
                        message: "Continue to WileyPlus"
                ],
                course              : [
                        isbn          : "954EEGRP11123",
                        imageUrl      : "https://wileyplus.com/abc.png",
                        title         : "Fundamentals of accounting",
                        authors       : "Halliday",
                        productEdition: "7th"
                ],
                classSectionId      : "cls12345",
                extraInfo           : [
                        [
                                key  : "productType",
                                value: "E4"
                        ],
                        [
                                key  : "toolProviderVersion",
                                value: "E4"
                        ],
                        [
                                key  : "lmsId",
                                value: "CANVAS"
                        ],
                        [
                                key  : "consumerKey",
                                value: "rvtest"
                        ],
                        [
                                key  : "domainId",
                                value: "dmn1234"
                        ],
                        [
                                key  : "userRole",
                                value: "student"
                        ],
                        [
                                key  : "testDrive",
                                value: "no"
                        ],
                        [
                                key  : "ipRestriction",
                                value: "no"
                        ],
                        [
                                key  : "alaLimitedAccess",
                                value: "no"
                        ]
                ]
        ]

    }

    def static cartActivationRequest = { map -> map }
    def static purchaseOptions = { map -> map.get("purchaseOptionsDto") }
    def static wileyPlusCourse = { map -> map.get("course") }
    def static webLink = { map -> map.get("returnURL") }
    def static extraInfo = { map -> map.get("extraInfo")[0] }

    def static updateFieldAndGenerateBody(Map map = getCartActivationTemplate(), Closure path, Map updates) {
        TestValidationUtils.updateFieldAndGenerateBody(map, path, updates)
    }

    def setup() {
        authorizeEsbClient(restClient);
    }

    def "Should generate json with cart activation url"() {

        when:
        HttpResponseDecorator response = postWileyPlusCartActivationRequest(postBody)

        then:
        with(response) {
            status == SC_OK
        }

        where:
        postBody = generate(getCartActivationTemplate())
    }

    def "Should generate json with cart activation url if field #fieldName passes validation - #validationRule"() {

        when:
        HttpResponseDecorator response = postWileyPlusCartActivationRequest(updateFieldAndGenerateBody(fieldBlock, updates))

        then:
        with(response) {
            status == SC_OK
        }

        where:
        fieldBlock            | updates                                  | fieldName                            | validationRule
        cartActivationRequest | [ssoToken: LONG_STRING_255]              | "ssoToken"                           | VALIDATION_RULE_SIZE_255
        cartActivationRequest | [country: "US"]                          | "country"                            | PATTERN_COUNTRY
        cartActivationRequest | [language: "en"]                         | "language"                           | PATTERN_LANGUAGE
        cartActivationRequest | [currency: "USD"]                        | "currency"                           | PATTERN_CURRENCY
        cartActivationRequest | [registrationRequired: true]             | "registrationRequired"               | VALIDATION_RULE_BOOLEAN
        cartActivationRequest | [userId: LONG_STRING_255]                | "userId"                             | VALIDATION_RULE_SIZE_255
        cartActivationRequest | [ficeCode: LONG_STRING_255]              | "ficeCode"                           | VALIDATION_RULE_SIZE_255
        cartActivationRequest | [classSectionId: LONG_STRING_255]        | "classSectionId"                     | VALIDATION_RULE_SIZE_255

        purchaseOptions       | [purchaseOptionAvailable: false]         | "options.purchaseOptionAvailable"    | VALIDATION_RULE_SIZE_255
        purchaseOptions       | [regCodeActivationAvailable: true]       | "options.regCodeActivationAvailable" | VALIDATION_RULE_SIZE_255
        purchaseOptions       | [gracePeriodDuration: Integer.MAX_VALUE] | "options.gracePeriodDuration"        | VALIDATION_RULE_POSITIVE_INT
        purchaseOptions       | [gracePeriodDuration: 0]                 | "options.gracePeriodDuration"        | VALIDATION_RULE_POSITIVE_INT
        purchaseOptions       | [gracePeriodUsed: true]                  | "options.gracePeriodUsed"            | VALIDATION_RULE_SIZE_255

        wileyPlusCourse       | [isbn: LONG_STRING_255]                  | "course.isbn"                        | VALIDATION_RULE_SIZE_255
        wileyPlusCourse       | [imageUrl: LONG_STRING_255]              | "course.imageUrl"                    | VALIDATION_RULE_SIZE_255
        wileyPlusCourse       | [title: LONG_STRING_255]                 | "course.title"                       | VALIDATION_RULE_SIZE_255
        wileyPlusCourse       | [authors: LONG_STRING_255]               | "course.authors"                     | VALIDATION_RULE_SIZE_255
        wileyPlusCourse       | [productEdition: LONG_STRING_255]        | "course.productEdition"              | VALIDATION_RULE_SIZE_255

        webLink               | [url: LONG_STRING_255]                   | "webLink.url"                        | VALIDATION_RULE_SIZE_255
        webLink               | [message: LONG_STRING_255]               | "webLink.message"                    | VALIDATION_RULE_SIZE_255

        extraInfo             | [key: LONG_STRING_255]                   | "extraInfo.key"                      | VALIDATION_RULE_SIZE_255
        extraInfo             | [value: LONG_STRING_255]                 | "extraInfo.value"                    | VALIDATION_RULE_SIZE_255
    }

    def "Should return badRequest status if field #fieldName fails validation - #validationRule"() {

        when:
        HttpResponseDecorator response = postWileyPlusCartActivationRequest(updateFieldAndGenerateBody(fieldBlock, updates))

        then:
        with(response) {
            if (isNotEmpty(data)) println(data)
            status == SC_BAD_REQUEST
        }

        where:
        fieldBlock            | updates                               | fieldName                            | validationRule
        cartActivationRequest | [ssoToken: LONG_STRING_256]           | "ssoToken"                           | VALIDATION_RULE_SIZE_255
        cartActivationRequest | [country: "us"]                       | "country"                            | PATTERN_COUNTRY
        cartActivationRequest | [language: "EN"]                      | "language"                           | PATTERN_LANGUAGE
        cartActivationRequest | [currency: "usd"]                     | "currency"                           | PATTERN_CURRENCY
        cartActivationRequest | [registrationRequired: "tru"]         | "registrationRequired"               | VALIDATION_RULE_BOOLEAN
        cartActivationRequest | [userId: LONG_STRING_256]             | "userId"                             | VALIDATION_RULE_SIZE_255
        cartActivationRequest | [ficeCode: LONG_STRING_256]           | "ficeCode"                           | VALIDATION_RULE_SIZE_255
        cartActivationRequest | [classSectionId: LONG_STRING_256]     | "classSectionId"                     | VALIDATION_RULE_SIZE_255

        purchaseOptions       | [purchaseOptionAvailable: "a"]        | "options.purchaseOptionAvailable"    | VALIDATION_RULE_SIZE_255
        purchaseOptions       | [regCodeActivationAvailable: "a"]     | "options.regCodeActivationAvailable" | VALIDATION_RULE_SIZE_255
        purchaseOptions       | [gracePeriodDuration: Long.MAX_VALUE] | "options.gracePeriodDuration"        | VALIDATION_RULE_POSITIVE_INT
        purchaseOptions       | [gracePeriodDuration: -1]             | "options.gracePeriodDuration"        | VALIDATION_RULE_POSITIVE_INT
        purchaseOptions       | [gracePeriodUsed: "a"]                | "options.gracePeriodUsed"            | VALIDATION_RULE_SIZE_255

        wileyPlusCourse       | [isbn: LONG_STRING_256]               | "course.isbn"                        | VALIDATION_RULE_SIZE_255
        wileyPlusCourse       | [imageUrl: LONG_STRING_256]           | "course.imageUrl"                    | VALIDATION_RULE_SIZE_255
        wileyPlusCourse       | [title: LONG_STRING_256]              | "course.title"                       | VALIDATION_RULE_SIZE_255
        wileyPlusCourse       | [authors: LONG_STRING_256]            | "course.authors"                     | VALIDATION_RULE_SIZE_255
        wileyPlusCourse       | [productEdition: LONG_STRING_256]     | "course.productEdition"              | VALIDATION_RULE_SIZE_255

        webLink               | [url: LONG_STRING_256]                | "webLink.url"                        | VALIDATION_RULE_SIZE_255
        webLink               | [message: LONG_STRING_256]            | "webLink.message"                    | VALIDATION_RULE_SIZE_255

        extraInfo             | [key: LONG_STRING_256]                | "extraInfo.key"                      | VALIDATION_RULE_SIZE_255
        extraInfo             | [value: LONG_STRING_256]              | "extraInfo.value"                    | VALIDATION_RULE_SIZE_255
    }

    def "Should return badRequest status if mandatory field #fieldName is missed"() {

        when:
        HttpResponseDecorator response = postWileyPlusCartActivationRequest(updateFieldAndGenerateBody(fieldBlock, updates))

        then:
        with(response) {
            if (isNotEmpty(data)) println(data)
            status == SC_BAD_REQUEST
        }

        where:
        fieldBlock            | updates                | fieldName
        cartActivationRequest | [country: null]         | "country"
        cartActivationRequest | [ficeCode: null]       | "ficeCode"
        cartActivationRequest | [course: null]         | "course"
        cartActivationRequest | [classSectionId: null] | "classSectionId"
        cartActivationRequest | [extraInfo: null]      | "extraInfo"

        wileyPlusCourse       | [isbn: null]           | "course.isbn"
        wileyPlusCourse       | [imageUrl: null]       | "course.imageUrl"
        wileyPlusCourse       | [title: null]          | "course.title"
        wileyPlusCourse       | [authors: null]        | "course.authors"

        webLink               | [url: null]            | "webLink.url"

        extraInfo             | [key: null]            | "extraInfo.key"
        extraInfo             | [value: null]          | "extraInfo.value"
    }


    private HttpResponseDecorator postWileyPlusCartActivationRequest(String postBody) {
        HttpResponseDecorator response = restClient.post(
                path: getDefaultHttpsUri() + '/wileyws/v3/ucart/wileyPlus',
                body: postBody,
                contentType: JSON,
                requestContentType: JSON)
        response;
    }
}
