/**
 *
 */
package com.wiley.ws.test.test.groovy.webservicetests.v2.spock.orders

import de.hybris.bootstrap.annotations.ManualTest
import groovyx.net.http.HttpResponseDecorator
import spock.lang.Unroll

import static groovyx.net.http.ContentType.JSON
import static org.apache.http.HttpStatus.*

/**
 *
 *
 */
@ManualTest
@Unroll
class OrdersTest extends AbstractOrderTest {

    public static final String ORDER_GUID = 'testOrder'
    public static final String NON_EXISTING_ORDER_GUID = 'non-existing order guid'
    public static final String AS_SITE = "asSite"
    public static final String B2C_SITE = "wileyb2c"
    public static final String NON_EXISTING_SITE = "non-existing site"
    public static final String INCORRECT_LEVEL = 'incorrect level'


    def "External system requests an order details with level BASIC"() {
        when: "external system requests an order details"
        authorizeTrustedClient(restClient)

        HttpResponseDecorator response = restClient.get(
                path: getBasePathWithSite(AS_SITE) + '/orders/' + ORDER_GUID,
                headers: ['Fields-Set-Level': 'BASIC'],
                contentType: JSON,
                requestContentType: JSON
        )

        then: "it retrieves requested order details"
        with(response) {
            if (isNotEmpty(data) && isEmpty(data.errors)) println(data)
            status == SC_OK
            data.size() == 6
            isNotEmpty(data.status)
            isNotEmpty(data.id)
            isNotEmpty(data.code)
            isNotEmpty(data.type)
            isNotEmpty(data.userId)
            isNotEmpty(data.modificationTime)
        }
    }

    def "External system requests an order details with level DEFAULT"() {
        when: "external system requests an order details"
        authorizeTrustedClient(restClient)

        HttpResponseDecorator response = restClient.get(
                path: getBasePathWithSite(AS_SITE) + '/orders/' + ORDER_GUID,
                contentType: JSON,
                requestContentType: JSON
        )

        then: "it retrieves requested order details"
        with(response) {
            if (isNotEmpty(data) && isEmpty(data.errors)) println(data)
            status == SC_OK
            data.size() == 10
            data.entries[0].size() == 11
            isNotEmpty(data.status)
            isNotEmpty(data.id)
            isNotEmpty(data.code)
            isNotEmpty(data.type)
            isNotEmpty(data.country)
            isNotEmpty(data.currency)
            isNotEmpty(data.userId)
            isNotEmpty(data.entries)
            isNotEmpty(data.entries.orderId)
            isNotEmpty(data.entries.entryId)
            isNotEmpty(data.entries.status)
            isNotEmpty(data.entries.productCode)
            isNotEmpty(data.entries.baseProductCode)
            isNotEmpty(data.entries.businessItemId)
            isNotEmpty(data.entries.businessKey)
            isNotEmpty(data.entries.quantity)
            isNotEmpty(data.entries.additionalInfo)
            isNotEmpty(data.entries.statusModificationTime)
            isNotEmpty(data.entries.modificationTime)
            isNotEmpty(data.date)
            isNotEmpty(data.modificationTime)
        }
    }

    def "External system requests an order details with level FULL"() {
        when: "external system requests an order details"
        authorizeTrustedClient(restClient)

        HttpResponseDecorator response = restClient.get(
                path: getBasePathWithSite(AS_SITE) + '/orders/' + ORDER_GUID,
                headers: ['Fields-Set-Level': 'FULL'],
                contentType: JSON,
                requestContentType: JSON
        )

        then: "it retrieves requested order details"
        with(response) {
            if (isNotEmpty(data) && isEmpty(data.errors)) println(data)
            status == SC_OK
            data.size() == 31
            data.entries[0].size() == 21
            isNotEmpty(data.status)
            isNotEmpty(data.sourceSystem)
            isNotEmpty(data.placedByAgent)
            isNotEmpty(data.id)
            isNotEmpty(data.code)
            isNotEmpty(data.type)
            isNotEmpty(data.siteId)
            isNotEmpty(data.country)
            isNotEmpty(data.currency)
            isNotEmpty(data.userId)
            isNotEmpty(data.entries)
            isNotEmpty(data.entries.orderId)
            isNotEmpty(data.entries.entryId)
            isNotEmpty(data.entries.status)
            isNotEmpty(data.entries.productCode)
            isNotEmpty(data.entries.baseProductCode)
            isNotEmpty(data.entries.businessItemId)
            isNotEmpty(data.entries.businessKey)
            isNotEmpty(data.entries.quantity)
            isNotEmpty(data.entries.basePrice)
            isNotEmpty(data.entries.subtotalPrice)
            isNotEmpty(data.entries.totalPrice)
            isNotEmpty(data.entries.taxableTotalPrice)
            isNotEmpty(data.entries.externalPrices)
            isNotEmpty(data.entries.discounts)
            isNotEmpty(data.entries.externalDiscounts)
            isNotEmpty(data.entries.taxes)
            isNotEmpty(data.entries.deliveryDate)
            isNotEmpty(data.entries.cancelReason)
            isNotEmpty(data.entries.additionalInfo)
            isNotEmpty(data.entries.statusModificationTime)
            isNotEmpty(data.entries.modificationTime)
            isNotEmpty(data.paymentMode)
            isNotEmpty(data.paymentAddress)
            isNotEmpty(data.contactAddress)
            isNotEmpty(data.externalText)
            isNotEmpty(data.purchaseOrderNumber)
            isNotEmpty(data.userNotes)
            data.subtotal != null
            data.discounts != null
            data.externalDiscounts != null
            data.totalDiscounts != null
            isNotEmpty(data.taxNumber)
            data.taxNumberValidated != null
            isNotEmpty(data.taxNumberExpirationDate)
            data.totalTax != null
            data.taxCalculated != null
            data.totalPrice != null
            data.appliedCouponCodes != null
            isNotEmpty(data.date)
            isNotEmpty(data.modificationTime)
        }
    }

    def "External system requests an order details for non-existing baseSite"() {
        when: "external system requests an order details"
        authorizeTrustedClient(restClient)

        HttpResponseDecorator response = restClient.get(
                path: getBasePathWithSite(NON_EXISTING_SITE) + '/orders/' + ORDER_GUID,
                contentType: JSON,
                requestContentType: JSON
        )

        then: "it retrieves requested order details"
        with(response) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            status == SC_NOT_FOUND
            data.errors[0].type == "InvalidResourceError"
            data.errors[0].message == ("Cannot find site...")
        }
    }

    def "External system requests an non-existing order details"() {
        when: "external system requests an order details"
        authorizeTrustedClient(restClient)

        HttpResponseDecorator response = restClient.get(
                path: getBasePathWithSite(AS_SITE) + '/orders/' + NON_EXISTING_ORDER_GUID,
                contentType: JSON,
                requestContentType: JSON
        )

        then: "it retrieves requested order details"
        with(response) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            status == SC_NOT_FOUND
            data.errors[0].type == "UnknownResourceError"
            data.errors[0].message == ("Cannot find order with guid '" + NON_EXISTING_ORDER_GUID + "'")
        }
    }

    def "External system requests an order details which belong to different site"() {
        when: "external system requests an order details"
        authorizeTrustedClient(restClient)

        HttpResponseDecorator response = restClient.get(
                path: getBasePathWithSite(B2C_SITE) + '/orders/' + ORDER_GUID,
                contentType: JSON,
                requestContentType: JSON
        )

        then: "it retrieves requested order details"
        with(response) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            status == SC_NOT_FOUND
            data.errors[0].type == "UnknownResourceError"
            data.errors[0].message == ("Cannot find order with guid '" + ORDER_GUID + "'")
        }
    }


    def "External system requests an order details with non-existing level"() {
        when: "external system requests an order details"
        authorizeTrustedClient(restClient)

        HttpResponseDecorator response = restClient.get(
                path: getBasePathWithSite(AS_SITE) + '/orders/' + ORDER_GUID,
                headers: ['Fields-Set-Level': INCORRECT_LEVEL],
                contentType: JSON,
                requestContentType: JSON
        )

        then: "it retrieves requested order details"
        with(response) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            status == SC_BAD_REQUEST
            data.errors[0].type == "ConversionError"
            data.errors[0].message == ("Incorrect field:'" + INCORRECT_LEVEL + "'")
        }
    }
}
