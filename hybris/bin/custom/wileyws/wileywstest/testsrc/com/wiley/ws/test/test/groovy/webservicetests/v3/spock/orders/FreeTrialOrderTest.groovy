package com.wiley.ws.test.test.groovy.webservicetests.v3.spock.orders

import com.wiley.ws.test.test.groovy.webservicetests.v3.spock.AbstractSpockTransactionalTest
import de.hybris.bootstrap.annotations.ManualTest
import groovyx.net.http.HttpResponseDecorator
import spock.lang.Unroll

import static groovyx.net.http.ContentType.JSON
import static org.apache.http.HttpStatus.SC_OK

@ManualTest
@Unroll
class FreeTrialOrderTest extends AbstractSpockTransactionalTest {
    private static final END_POINT = "/wel/order/freetrial"
    private static final WRONG_PRODUCT_CODE = "wrong_code"
    private static final WRONG_EMAIL = "helloooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo@world.com"
    private static final RIGHT_VARIANT_PRODUCT_CODE = "WEL_WS_CPA_EBOOK_FREE_TRIAL_VARIANT"
    private static final RIGHT_BASE_PRODUCT_CODE = "WEL_WS_CPA_PRINT_FREE_TRIAL_PRODUCT"

    protected static final ORDER_VARIANT_SUCCESS_REQUEST = [
            'productCode': RIGHT_VARIANT_PRODUCT_CODE,
            'email'      : 'freeTreal@test.com',
            'password'   : '123456',
            'firstName'  : 'Hello',
            'lastName'   : 'World'
    ]

    protected static final ORDER_BASE_PRODUCT_SUCCESS_REQUEST = [
            'productCode': RIGHT_BASE_PRODUCT_CODE,
            'email'      : 'freeTreal@test.com',
            'password'   : '123456',
            'firstName'  : 'Hello',
            'lastName'   : 'World'
    ]
    protected static final ORDER_WRONG_CODE_REQUEST = [
            'productCode': WRONG_PRODUCT_CODE,
            'email'      : 'hello@world.com',
            'password'   : 'hello@world.com',
            'firstName'  : 'Hello',
            'lastName'   : 'World'
    ]
    protected static final ORDER_BLANK_PARAM_REQUEST = [
            'email'    : 'hello@world.com',
            'password' : 'hello@world.com',
            'firstName': 'Hello',
            'lastName' : 'World'
    ]
    protected static final ORDER_WRONG_EMAIL_REQUEST = [
            'productCode': RIGHT_VARIANT_PRODUCT_CODE,
            'email'      : WRONG_EMAIL,
            'password'   : '123456',
            'firstName'  : 'Hello',
            'lastName'   : 'World'
    ]
    protected static final ORDER_WRONG_NAME_REQUEST = [
            'productCode': RIGHT_VARIANT_PRODUCT_CODE,
            'email'      : 'freeTreal@test.com',
            'password'   : '123456',
            'firstName'  : 'HelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHello',
            'lastName'   : 'WorldWorldWorldWorldWorldWorldWorldWorldWorldWorldWorldWorldWorldWorldWorldWorldWorldWorldWorldWorldWorldWorldWorldWorld'
    ]
    protected static final ORDER_WRONG_PASSWORD_REQUEST = [
            'productCode': RIGHT_VARIANT_PRODUCT_CODE,
            'email'      : 'freeTreal@test.com',
            'password'   : 'wrong password',
            'firstName'  : 'Hello',
            'lastName'   : 'World'
    ]

    def "Wel client places a variant free trial order"() {
        given: "Wel client"
        authorizeWelClient(restClient)

        when: "Client requests free trial order "
        HttpResponseDecorator response = restClient.post(
                path: getBasePath() + END_POINT,
                body: postBody,
                contentType: responseFormat,
                requestContentType: requestFormat)

        then: "Gets 200 response and orderId"
        with(response) {
            println(data)
            status == SC_OK
            isNotEmpty(data.orderId)
            isNotEmpty(data.magicLink)
        }

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | ORDER_VARIANT_SUCCESS_REQUEST
    }

    def "Wel client places a success free trial order with base product"() {
        given: "Wel client"
        authorizeWelClient(restClient)

        when: "Client requests free trial order "
        HttpResponseDecorator response = restClient.post(
                path: getBasePath() + END_POINT,
                body: postBody,
                contentType: responseFormat,
                requestContentType: requestFormat)

        then: "Gets 200 response and orderId"
        with(response) {
            println(data)
            status == SC_OK
            isNotEmpty(data.orderId)
            isNotEmpty(data.magicLink)
        }

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | ORDER_BASE_PRODUCT_SUCCESS_REQUEST
    }

    def "Wel client places a free tril order twice"() {
        given: "Wel client"
        authorizeWelClient(restClient)

        when: "Client requests free trial order "
        HttpResponseDecorator response = restClient.post(
                path: getBasePath() + END_POINT,
                body: postBody,
                contentType: responseFormat,
                requestContentType: requestFormat)

        then: "Gets 200 response and orderId"
        with(response) {
            println(data)
            status == SC_OK
            isNotEmpty(data.orderId)
            isNotEmpty(data.magicLink)
        }

        when: "Client requests free trial order second time "
        response = restClient.post(
                path: getBasePath() + END_POINT,
                body: postBody,
                contentType: responseFormat,
                requestContentType: requestFormat)

        then: "Gets 400 response"
        with(response) {
            println(data)
            status == 400
            isNotEmpty(data.errors)
            data.errors[0].type == 'RepeatedFreeTrialOrderError'
            data.errors[0].message == ("User has already have order with " + RIGHT_VARIANT_PRODUCT_CODE + " code ")
        }

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | ORDER_VARIANT_SUCCESS_REQUEST
    }

    def "Wel client places a free trial order with wrong productCode"() {
        given: "Wel client"
        authorizeWelClient(restClient)

        when: "Client requests free trial order "
        HttpResponseDecorator response = restClient.post(
                path: getBasePath() + END_POINT,
                body: postBody,
                contentType: responseFormat,
                requestContentType: requestFormat)

        then: "Gets 400 response"
        with(response) {
            status == 400
            isNotEmpty(data.errors)
            data.errors[0].type == 'UnknownIdentifierError'
            data.errors[0].message == ("WileyFreeTrialVariantProduct with code '" + WRONG_PRODUCT_CODE + "' not found!")
        }

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | ORDER_WRONG_CODE_REQUEST
    }

    def "Wel client places a free trial order with empty product code"() {
        given: "Wel client"
        authorizeWelClient(restClient)

        when: "Client requests free trial order "
        HttpResponseDecorator response = restClient.post(
                path: getBasePath() + END_POINT,
                body: postBody,
                contentType: responseFormat,
                requestContentType: requestFormat)

        then: "Gets 400 response"
        with(response) {
            status == 400
            isNotEmpty(data.errors)
            data.errors[0].type == 'ValidationError'
            data.errors[0].message == 'This field is required.'
        }

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | ORDER_BLANK_PARAM_REQUEST
    }

    def "Wel client places a free trial order with wrong email"() {
        given: "Wel client"
        authorizeWelClient(restClient)

        when: "Client requests free trial order "
        HttpResponseDecorator response = restClient.post(
                path: getBasePath() + END_POINT,
                body: postBody,
                contentType: responseFormat,
                requestContentType: requestFormat)

        then: "Gets 400 response"
        with(response) {
            status == 400
            isNotEmpty(data.errors)
            data.errors[0].type == 'ValidationError'
            data.errors[0].message == 'Email address is missing or invalid.'
        }

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | ORDER_WRONG_EMAIL_REQUEST
    }
    def "Wel client places a free trial order with wrong password"() {
        given: "Wel client"
        authorizeWelClient(restClient)

        when: "Client requests free trial order "
        HttpResponseDecorator response = restClient.post(
                path: getBasePath() + END_POINT,
                body: ORDER_VARIANT_SUCCESS_REQUEST,
                contentType: responseFormat,
                requestContentType: requestFormat)

        then: "Gets 200 response and orderId"
        with(response) {
            println(data)
            status == SC_OK
            isNotEmpty(data.orderId)
            isNotEmpty(data.magicLink)
        }

        when: "Client requests free trial order "
        response = restClient.post(
                path: getBasePath() + END_POINT,
                body: postBody,
                contentType: responseFormat,
                requestContentType: requestFormat)

        then: "Gets 400 response"
        with(response) {
            status == 400
            isNotEmpty(data.errors)
            data.errors[0].type == 'BadCredentialsError'
            data.errors[0].message == 'Bad credentials'
        }

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | ORDER_WRONG_PASSWORD_REQUEST
    }

    def "Wel client places a free trial order with wrong name"() {
        given: "Wel client"
        authorizeWelClient(restClient)

        when: "Client requests free trial order "
        HttpResponseDecorator response = restClient.post(
                path: getBasePath() + END_POINT,
                body: postBody,
                contentType: responseFormat,
                requestContentType: requestFormat)

        then: "Gets 400 response"
        with(response) {
            status == 400
            isNotEmpty(data.errors)
            data.errors[0].type == 'ValidationError'
            data.errors[0].message == 'Combined length of first name and last name must be less than 255 characters.'
        }

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | ORDER_WRONG_NAME_REQUEST
    }

}
