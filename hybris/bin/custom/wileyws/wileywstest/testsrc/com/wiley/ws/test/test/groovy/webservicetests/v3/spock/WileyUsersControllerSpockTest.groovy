package com.wiley.ws.test.test.groovy.webservicetests.v3.spock

import de.hybris.bootstrap.annotations.ManualTest
import groovyx.net.http.HttpResponseDecorator
import spock.lang.Unroll

import java.util.regex.Pattern;

import static groovyx.net.http.ContentType.JSON

@ManualTest
@Unroll
class WileyUsersControllerSpockTest extends AbstractSpockTransactionalTest
{
    public static final int CREATED = 201
    public static final int UNAUTHORIZED = 401
    public static final int CONFLICT = 409
    public static final int UNPROCESSABLE_ENTITY = 422

    public static final int PASSWORD_VALIDATION_RULES_AMOUNT = 6
    public static final String CUSTOMER_ID_PATTERN = "\\b[0-9a-f]{8}\\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\\b[0-9a-f]{12}\\b"
    private Pattern customerIdPattern = Pattern.compile("/customers/" + CUSTOMER_ID_PATTERN)
    public static final String LOCATION = "Location"

    private static final CREATE_CUSTOMER_REQUEST = [
            "email"       :"example@example.com",
            "firstName"   :"John",
            "lastName"    :"Smith",
            "titleCode"   :"MR",
            "password"    :"12qwQW!@"
    ]

    private static final CREATE_CUSTOMER_WITH_MIN_DATA_REQUEST = [
            "email"       :"example@example.com",
            "firstName"   :null,
            "lastName"    :null,
            "titleCode"   :null,
            "password"    :"12qwQW!@"
    ]

    private static final CREATE_CUSTOMER_WITH_INVALID_EMAIL_REQUEST = [
            "email"       :"invalidEmail",
            "firstName"   :"John",
            "lastName"    :"Smith",
            "titleCode"   :"MR",
            "password"    :"12qwQW!@"
    ]

    private static final CREATE_CUSTOMER_WITH_INVALID_PASSWORD_REQUEST = [
            "email"       :"example@example.com",
            "firstName"   :"John",
            "lastName"    :"Smith",
            "titleCode"   :"MR",
            "password"    :""
    ]

    def "New customer is created by external authorized client: "() {
        given: "authorized client"
        authorizeWelFulfillmentClient(restClient)

        when: "authorized client sends new customer data "
        HttpResponseDecorator response = makeRequest(postBody, headers)

        then: "he receives 201 created with location"
        with(response) {
            status == CREATED
            isEmpty(data)
            response.containsHeader(LOCATION)
            customerIdPattern.matcher(response.getFirstHeader(LOCATION).getValue()).find()
        }

        where:
        postBody                | headers
        CREATE_CUSTOMER_REQUEST | ['ExternalApplicationId' : 'TestName',
                                   'ExternalCustomerId'    : 'TestId-12345']
    }

    def "New customer without optional fields is created by external authorized client: "() {
        given: "authorized client"
        authorizeWelFulfillmentClient(restClient)

        when: "authorized client sends new customer data "
        HttpResponseDecorator response = makeRequest(postBody, headers)

        then: "he receives 201 created with location"
        with(response) {
            status == CREATED
            isEmpty(data)
            response.containsHeader(LOCATION)
            customerIdPattern.matcher(response.getFirstHeader(LOCATION).getValue()).find()
        }

        where:
        postBody                              | headers
        CREATE_CUSTOMER_WITH_MIN_DATA_REQUEST | ['ExternalApplicationId' : null,
                                                 'ExternalCustomerId'    : null]
    }

    def "Attempt to create new customer by unauthorized client: "() {
        given: "unauthorized client"

        when: "unauthorized client sends new customer data "
        HttpResponseDecorator response = makeRequest(postBody, headers)

        then: "he receives 401 Unauthorized response"
        with(response) {
            status == UNAUTHORIZED
            isNotEmpty(data.errors)
            data.errors[0].type == "AccessDeniedError"
            println(data)
        }

        where:
        postBody                | headers
        CREATE_CUSTOMER_REQUEST | ['ExternalApplicationId' : 'TestName',
                                   'ExternalCustomerId'    : 'TestId-12345']
    }

    def "Attempt to create new customer with already existed email: "() {
        given: "authorized client"
        authorizeWelFulfillmentClient(restClient)

        when: "creating customer with already existed email"
        String customerId = createCustomer()
        HttpResponseDecorator response = makeRequest(postBody, headers)

        then: "he receives 409 Conflict"
        with(response) {
            status == CONFLICT
            isNotEmpty(data)
            data.errors[0].type == "DuplicateUidError"
            data.errors[0].message == CREATE_CUSTOMER_REQUEST.email
            data.errors[0].customerId == customerId
            println(data)
        }

        where:
        postBody                | headers
        CREATE_CUSTOMER_REQUEST | ['ExternalApplicationId' : null,
                                   'ExternalCustomerId'    : null]
    }

    private String createCustomer() {
        HttpResponseDecorator response = restClient.post(
                path: getBasePath() + "/customers",
                body: CREATE_CUSTOMER_REQUEST,
                requestContentType: JSON,
                headers: ['ExternalApplicationId' : 'TestName',
                          'ExternalCustomerId'    : 'TestId-12345']
        )

        return response.getFirstHeader(LOCATION).getValue().substring("/customers/".length())
    }

    def "Attempt to create new customer with invalid email: "() {
        given: "authorized client"
        authorizeWelFulfillmentClient(restClient)

        when: "authorized client sends new customer data "
        HttpResponseDecorator response = makeRequest(postBody, headers)

        then: "he receives 422 response"
        with(response) {
            status == UNPROCESSABLE_ENTITY
            isNotEmpty(data)
            data.errors[0].type == "ValidationError"
            data.errors[0].reason == "invalid"
            data.errors[0].message == "This field is not a valid email addresss."
            data.errors[0].subjectType == "parameter"
            data.errors[0].subject == "email"
            println(data.errors)
        }

        where:
        postBody                                   | headers
        CREATE_CUSTOMER_WITH_INVALID_EMAIL_REQUEST | ['ExternalApplicationId' : 'TestName',
                                                      'ExternalCustomerId'    : 'TestId-12345']
    }

    def "Attempt to create new customer with invalid password: "() {
        given: "authorized client"
        authorizeWelFulfillmentClient(restClient)

        when: "authorized client sends new customer data "
        HttpResponseDecorator response = makeRequest(postBody, headers)

        then: "he receives 422 response"
        with(response) {
            status == UNPROCESSABLE_ENTITY
            isNotEmpty(data)
            data.errors.size() == PASSWORD_VALIDATION_RULES_AMOUNT
            println(data)
        }

        where:
        postBody                                      | headers
        CREATE_CUSTOMER_WITH_INVALID_PASSWORD_REQUEST | ['ExternalApplicationId' : 'TestName',
                                                         'ExternalCustomerId'    : 'TestId-12345']
    }

    private HttpResponseDecorator makeRequest(def postBody, def headers) {
        restClient.post(
                path: getBasePath() + "/customers",
                body: postBody,
                requestContentType: JSON,
                contentType: JSON,
                headers: headers
        )
    }
}
