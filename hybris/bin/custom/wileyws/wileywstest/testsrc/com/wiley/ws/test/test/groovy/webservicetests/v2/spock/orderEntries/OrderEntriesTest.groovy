package com.wiley.ws.test.test.groovy.webservicetests.v2.spock.orderEntries

import com.wiley.ws.test.test.groovy.webservicetests.v2.spock.orders.AbstractOrderTest
import de.hybris.bootstrap.annotations.ManualTest
import groovy.json.JsonOutput
import groovyx.net.http.HttpResponseDecorator
import spock.lang.Unroll

import static groovyx.net.http.ContentType.JSON
import static org.apache.http.HttpStatus.*

@ManualTest
@Unroll
class OrderEntriesTest extends AbstractOrderTest {

    public static final String AS_SITE = "asSite"
    public static final String ORDER_ENTRY_ID = 'testOrderEntry'
    public static final String ORDER_ENTRY_ID_PROCESS_FAILED = 'testOrderEntryProcessFailed'
    public static final String ORDER_ENTRY_ID_PROCESS_RUNNING = 'testOrderEntryProcessRunning'
    public static final String NON_EXISTING_ORDER_ENTRY_ID = 'nonExistingOrderEntry'
    public static final String CART_ENTRY_ID = 'testCartEntry'
    public static final String INCORRECT_LEVEL = 'incorrect level'
    public static final String NON_EXISTING_SITE = "non-existing site"
    public static final String NON_EXISTING_STATUS = "non-existing status"
    public static final String STATUS_CANCELLED = "CANCELLED"
    public static final String STATUS_SUSPENDED = "SUSPENDED"
    public static final String ADDITIONAL_INFO_TEST_VALUE = "test value"

    public static final ORDER_ENTRY_UPDATE_BODY = [
            "status"        : STATUS_SUSPENDED,
            "additionalInfo": ADDITIONAL_INFO_TEST_VALUE
    ]

    def "External system wants to update order entry with status and additionalInfo attributes"() {
        when: "external system requests update of order entry details"
        authorizeTrustedClient(restClient)

        HttpResponseDecorator response = restClient.post(
                path: getBasePathWithSite(AS_SITE) + '/orderentries/' + ORDER_ENTRY_ID + '/info',
                body: ORDER_ENTRY_UPDATE_BODY,
                contentType: JSON,
                requestContentType: JSON
        )

        then: "it retrieves requested order details"
        with(response) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            status == SC_OK
            data.status == STATUS_SUSPENDED
            data.additionalInfo == ADDITIONAL_INFO_TEST_VALUE
        }
    }

    def "External system wants to update order entry for non-existing baseSite"() {
        when: "external system requests update of order entry details"
        authorizeTrustedClient(restClient)

        HttpResponseDecorator response = restClient.post(
                path: getBasePathWithSite(NON_EXISTING_SITE) + '/orderentries/' + ORDER_ENTRY_ID + '/info',
                body: ORDER_ENTRY_UPDATE_BODY,
                contentType: JSON,
                requestContentType: JSON
        )

        then: "it retrieves requested order details"
        with(response) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            status == SC_NOT_FOUND
            data.errors[0].type == "InvalidResourceError"
            data.errors[0].message == ("Cannot find site...")
        }
    }

    def "External system wants to update non-existing order entry"() {
        when: "external system requests update of order entry details"
        authorizeTrustedClient(restClient)

        HttpResponseDecorator response = restClient.post(
                path: getBasePathWithSite(AS_SITE) + '/orderentries/' + NON_EXISTING_ORDER_ENTRY_ID + '/info',
                body: ORDER_ENTRY_UPDATE_BODY,
                contentType: JSON,
                requestContentType: JSON
        )

        then: "it retrieves requested order details"
        with(response) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            status == SC_NOT_FOUND
            data.errors[0].type == "UnknownResourceError"
            data.errors[0].message == ("Cannot find entry with guid '" + NON_EXISTING_ORDER_ENTRY_ID + "'")
        }
    }

    def "External system wants to update order entry and use cartEntryId as orderEntryId"() {
        when: "external system requests update of order entry details"
        authorizeTrustedClient(restClient)

        HttpResponseDecorator response = restClient.post(
                path: getBasePathWithSite(AS_SITE) + '/orderentries/' + CART_ENTRY_ID + '/info',
                body: ORDER_ENTRY_UPDATE_BODY,
                contentType: JSON,
                requestContentType: JSON
        )

        then: "it retrieves requested order details"
        with(response) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            status == SC_NOT_FOUND
            data.errors[0].type == "UnknownResourceError"
            data.errors[0].message == ("Cannot find entry with guid '" + CART_ENTRY_ID + "'")
        }
    }

    def "External system wants to update order entry with payload parsing error"() {
        when: "external system requests update of order entry details"
        authorizeTrustedClient(restClient)
        String invalidBody = JsonOutput.toJson(ORDER_ENTRY_UPDATE_BODY).replaceFirst(/\}/, ']')

        HttpResponseDecorator response = restClient.post(
                path: getBasePathWithSite(AS_SITE) + '/orderentries/' + ORDER_ENTRY_ID + '/info',
                body: invalidBody,
                contentType: JSON,
                requestContentType: JSON
        )

        then: "it retrieves requested order details"
        with(response) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            status == SC_BAD_REQUEST
            data.errors[0].type == "HttpMessageNotReadableError"
            data.errors[0].message.startsWith("Could not read document: Unexpected close marker")
        }
    }

    def "External system wants to update order entry status with incorrect fieldsSetLevel"() {
        when: "external system requests update of an order entry status and additional info fields"
        authorizeTrustedClient(restClient)

        HttpResponseDecorator response = restClient.post(
                path: getBasePathWithSite(AS_SITE) + '/orderentries/' + ORDER_ENTRY_ID + '/info',
                body: ORDER_ENTRY_UPDATE_BODY,
                headers: ['Fields-Set-Level': INCORRECT_LEVEL],
                contentType: JSON,
                requestContentType: JSON
        )

        then: "it retrieves requested order details"
        with(response) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            status == SC_BAD_REQUEST
            data.errors[0].type == "ConversionError"
            data.errors[0].message == ("Incorrect field:'" + INCORRECT_LEVEL + "'")
        }
    }

    def "External system wants to update order entry with not existing status"() {
        when: "external system requests update of an order entry status and additional info fields"
        authorizeTrustedClient(restClient)

        HttpResponseDecorator response = restClient.post(
                path: getBasePathWithSite(AS_SITE) + '/orderentries/' + ORDER_ENTRY_ID + '/info',
                body: [
                        "status"        : NON_EXISTING_STATUS,
                        "additionalInfo": ADDITIONAL_INFO_TEST_VALUE
                ],
                contentType: JSON,
                requestContentType: JSON
        )

        then: "it retrieves requested order details"
        with(response) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            status == SC_BAD_REQUEST
            data.errors[0].type == "UnknownIdentifierError"
            data.errors[0].message == ("No enumeration value 'OrderStatus." + NON_EXISTING_STATUS + "' exists!")
        }
    }

    def "External system wants to update order entry with status CANCELLED"() {
        when: "external system requests update of an order entry status and additional info fields"
        authorizeTrustedClient(restClient)

        HttpResponseDecorator response = restClient.post(
                path: getBasePathWithSite(AS_SITE) + '/orderentries/' + ORDER_ENTRY_ID + '/info',
                body: [
                        "status"        : STATUS_CANCELLED,
                        "additionalInfo": ADDITIONAL_INFO_TEST_VALUE
                ],
                contentType: JSON,
                requestContentType: JSON
        )

        then: "it retrieves requested order details"
        with(response) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            status == SC_UNPROCESSABLE_ENTITY
            data.errors[0].type == "IllegalArgumentError"
            data.errors[0].message == ("Order status '" + STATUS_CANCELLED + "' sent.")
        }
    }

    def "External system wants to update order entry when business process is failed"() {
        when: "external system requests update of an order entry status and additional info fields"
        authorizeTrustedClient(restClient)

        HttpResponseDecorator response = restClient.post(
                path: getBasePathWithSite(AS_SITE) + '/orderentries/' + ORDER_ENTRY_ID_PROCESS_FAILED + '/info',
                body: [
                        "status"        : STATUS_SUSPENDED,
                        "additionalInfo": ADDITIONAL_INFO_TEST_VALUE
                ],
                contentType: JSON,
                requestContentType: JSON
        )

        then: "it retrieves requested order details"
        with(response) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            status == SC_SERVICE_UNAVAILABLE
            data.errors[0].type == "OrderFailedError"
            data.errors[0].message.startsWith("Order entry update rejected. Order fulfillment process failed for")
        }
    }

    def "External system wants to update order entry when business process is running"() {
        when: "external system requests update of an order entry status and additional info fields"
        authorizeTrustedClient(restClient)

        HttpResponseDecorator response = restClient.post(
                path: getBasePathWithSite(AS_SITE) + '/orderentries/' + ORDER_ENTRY_ID_PROCESS_RUNNING + '/info',
                body: [
                        "status"        : STATUS_SUSPENDED,
                        "additionalInfo": ADDITIONAL_INFO_TEST_VALUE
                ],
                contentType: JSON,
                requestContentType: JSON
        )

        then: "it retrieves requested order details"
        with(response) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            status == SC_SERVICE_UNAVAILABLE
            data.errors[0].type == "OrderProcessingError"
            data.errors[0].message.startsWith("Modification rejected. Active fulfillment process is running for order")
        }
    }
}
