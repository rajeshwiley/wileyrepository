package com.wiley.ws.test.test.groovy.webservicetests.v3.spock

import de.hybris.bootstrap.annotations.ManualTest
import groovyx.net.http.HttpResponseDecorator
import spock.lang.Unroll

import static groovyx.net.http.ContentType.JSON

@ManualTest
@Unroll
class WileyStockLevelControllerSpockTest extends AbstractSpockTransactionalTest {

    static final String NEW_INVENTIRY_ID = "newInventoryId"
    static final String EXISTED_INVENTIRY_ID = "testInventory1"

    protected static final INVENTORY_REQUEST = [
            "warehouseId"         : "wsTestWarehouse",
            "productId"           : "1934793",
            "quantity"            : "0",
            "nextAvailabilityDate": "2014-06-01",
            "sequenceId"          : "1489021796"
    ]

    protected static final INVENTORY_REQUEST_WITH_LOWER_SEQUENCE_ID = [
            "warehouseId"         : "wsTestWarehouse",
            "productId"           : "1934793",
            "quantity"            : "0",
            "nextAvailabilityDate": "2014-06-01",
            "sequenceId"          : "10000000"
    ]

    protected static final INVENTORY_REQUEST_WITH_MISSING_PRODUCT_ID = [
            "warehouseId"         : "wsTestWarehouse",
            "quantity"            : "0",
            "nextAvailabilityDate": "2014-06-01",
            "sequenceId"          : "1489021797"
    ]

    protected static final INVENTORY_REQUEST_WITH_INVALID_WAREHOUSE_ID = [
            "warehouseId"         : "invalidWarehouse",
            "productId"           : "1934793",
            "quantity"            : "0",
            "nextAvailabilityDate": "2014-06-01",
            "sequenceId"          : "1489021797"
    ]

    protected static final INVENTORY_REQUEST_WITH_ATTEMPT_REASSIGN_WAREHOUSE_ID = [
            "warehouseId"         : "wsTestWarehouse2",
            "productId"           : "1934793",
            "quantity"            : "0",
            "nextAvailabilityDate": "2014-06-01",
            "sequenceId"          : "1489021797"
    ]


    def "Trusted client create new inventory: "() {
        given: "trusted client"
        authorizeTrustedClient(restClient)

        when: "trusted client sends inventory with non-existing id "
        HttpResponseDecorator response = restClient.put(
                path: getBasePath() + "/inventory/" + NEW_INVENTIRY_ID,
                contentType: responseFormat,
                body: postBody,
                requestContentType: requestFormat)

        then: "he receives 201 response"
        with(response) {
            if (isNotEmpty(data) && isEmpty(data.errors)) println(data)
            status == 201
            isEmpty(data)
        }

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | INVENTORY_REQUEST
    }

    def "Trusted client update already existed inventory: "() {
        given: "trusted client"
        authorizeTrustedClient(restClient)

        when: "trusted client sends inventory with already existed id "
        HttpResponseDecorator response = restClient.put(
                path: getBasePath() + "/inventory/" + EXISTED_INVENTIRY_ID,
                contentType: responseFormat,
                body: postBody,
                requestContentType: requestFormat)

        then: "he receives 204 response"
        with(response) {
            if (isNotEmpty(data) && isEmpty(data.errors)) println(data)
            status == 204
            isEmpty(data)
        }

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | INVENTORY_REQUEST
    }

    def "Trusted client send inventory request with lower sequenceId: "() {
        given: "trusted client"
        authorizeTrustedClient(restClient)

        when: "trusted client sends inventory with lower sequenceId "
        HttpResponseDecorator response = restClient.put(
                path: getBasePath() + "/inventory/" + EXISTED_INVENTIRY_ID,
                contentType: responseFormat,
                body: postBody,
                requestContentType: requestFormat)

        then: "he receives 204 response"
        with(response) {
            if (isNotEmpty(data) && isEmpty(data.errors)) println(data)
            status == 204
            isEmpty(data)
        }

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | INVENTORY_REQUEST_WITH_LOWER_SEQUENCE_ID
    }

    def "Trusted client send inventory request with missing productId: "() {
        given: "trusted client"
        authorizeTrustedClient(restClient)

        when: "trusted client sends inventory with missing productId "
        HttpResponseDecorator response = restClient.put(
                path: getBasePath() + "/inventory/" + EXISTED_INVENTIRY_ID,
                contentType: responseFormat,
                body: postBody,
                requestContentType: requestFormat)

        then: "he receives 400 response"
        with(response) {
            if (isNotEmpty(data) && isEmpty(data.errors)) println(data)
            status == 400
            isNotEmpty(data.errors)
            data.errors[0].type == 'parsing'
            data.errors[0].reason == 'json_parse_error'
        }

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | INVENTORY_REQUEST_WITH_MISSING_PRODUCT_ID
    }

    def "Trusted client send inventory request with invalid warehouseId: "() {
        given: "trusted client"
        authorizeTrustedClient(restClient)

        when: "trusted client sends inventory with invalid warehouseId "
        HttpResponseDecorator response = restClient.put(
                path: getBasePath() + "/inventory/" + EXISTED_INVENTIRY_ID,
                contentType: responseFormat,
                body: postBody,
                requestContentType: requestFormat)

        then: "he receives 400 response"
        with(response) {
            if (isNotEmpty(data) && isEmpty(data.errors)) println(data)
            status == 400
            isNotEmpty(data.errors)
            data.errors[0].type == 'transformation'
            data.errors[0].reason == 'bad_reference_error'
            data.errors[0].message == 'The reference can\'t be resolved'
            data.errors[0].subjectType == 'StockLevel'
            data.errors[0].subject == 'warehouseId'
        }

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | INVENTORY_REQUEST_WITH_INVALID_WAREHOUSE_ID
    }

    def "Trusted client attempts reasign warehouseId: "() {
        given: "trusted client"
        authorizeTrustedClient(restClient)

        when: "trusted client sends inventory with attempt reasign warehouseId "
        HttpResponseDecorator response = restClient.put(
                path: getBasePath() + "/inventory/" + EXISTED_INVENTIRY_ID,
                contentType: responseFormat,
                body: postBody,
                requestContentType: requestFormat)

        then: "he receives 400 response"
        with(response) {
            if (isNotEmpty(data) && isEmpty(data.errors)) println(data)
            status == 400
            isNotEmpty(data.errors)
            data.errors[0].type == 'validation'
            data.errors[0].reason == 'immutable_field_error'
            data.errors[0].message == 'The field is immutable, it can\'t be changed after initialization'
            data.errors[0].subjectType == 'StockLevel'
            data.errors[0].subject == 'warehouseId'
        }

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | INVENTORY_REQUEST_WITH_ATTEMPT_REASSIGN_WAREHOUSE_ID
    }
}
