/**
 *
 */
package com.wiley.ws.test.test.groovy.webservicetests.v2.spock.ui.guitests


import com.wiley.ws.test.test.groovy.webservicetests.v2.spock.AbstractGUITest
import com.wiley.ws.test.test.groovy.webservicetests.v2.spock.ui.pages.OauthAllowRequestPage
import com.wiley.ws.test.test.groovy.webservicetests.v2.spock.ui.pages.OauthLoginPage


class OauthRequestTest extends AbstractGUITest {

    def "Allow oauth request"() {
        clearCookies()
        given: "user navigates to login page"
        via OauthAllowRequestPage, "mobile_android", AbstractGUITest.getHttpWebroot() + "/oauth2_callback"
        at OauthLoginPage

        and: "user logs in"
        login("democustomer", "1234")

        when: "he is on the accept request page"
        at OauthAllowRequestPage

        and: "clicks 'Accept'"
        authorize()


        then: "he is redirected to redirect url"
        driver.currentUrl.contains("code")
        !driver.currentUrl.contains("error=access_denied")
        !URLDecoder.decode(driver.currentUrl, "UTF-8").contains("error_description=User denied access")
    }

    def "Deny oauth request"() {
        clearCookies()
        given: "user navigates to login page"
        via OauthAllowRequestPage, "mobile_android", AbstractGUITest.getHttpWebroot() + "/oauth2_callback"
        at OauthLoginPage

        and: "user logs in"
        login("democustomer", "1234")

        when: "he is on the accept request page"
        at OauthAllowRequestPage

        and: "clicks 'Deny'"
        deny()


        then: "he is redirected to redirect url"
        driver.currentUrl.contains("error=access_denied")
        driver.currentUrl.contains("error_description=")
        URLDecoder.decode(driver.currentUrl, "UTF-8").contains("error_description=User denied access")
    }
}
