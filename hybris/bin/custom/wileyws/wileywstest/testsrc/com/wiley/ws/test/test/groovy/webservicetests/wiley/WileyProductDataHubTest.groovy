package com.wiley.ws.test.test.groovy.webservicetests.wiley

import com.wiley.ws.test.test.groovy.webservicetests.v2.spock.AbstractSpockFlowTest
import de.hybris.bootstrap.annotations.ManualTest
import groovyx.net.http.HttpResponseDecorator
import spock.lang.Unroll

import static groovyx.net.http.ContentType.JSON
import static org.apache.http.HttpStatus.SC_BAD_REQUEST
import static org.apache.http.HttpStatus.SC_CREATED
import static org.apache.http.HttpStatus.SC_NO_CONTENT

@ManualTest
@Unroll
class WileyProductDataHubTest extends AbstractSpockFlowTest{

    private static final String WILEY_PRODUCT_CODE = "WileyProductDataHubTest"
    private static final String WILEY_PRODUCT_SUBTYPE = "SUBSCRIPTION"
    private static final Boolean PRODUCT_PURCHASABLE = true
    private static final Boolean PRODUCT_COUNTABLE = true
    private static final String PRODUCT_EDITIONFORMAT = "DIGITAL"
    private static final String PRODUCT_EN_NAME = 'WileyProductDataHubTest name'
    private static final String PRODUCT_EN_DESCRIPTION = 'WileyProductDataHubTest summary'
    private static final String PRODUCT_EN_SUMMARY = 'WileyProductDataHubTest summary'
    private static final String PRODUCT_PRICTURE_URL = "https://wiley.com/noimage.svg"
    private static final String PRODUCT_PRICTURE_MIME = "image/svg+xml"
    private static final String PRODUCT_PRICTURE_ALTTEXT = "alttext"
    private static final String PRODUCT_ONLINEDATE = "2012-02-14T13:15:03-0800"
    private static final String PRODUCT_OFFLINEDATE = "2018-02-14T13:15:03-0800"
    private static final String PRODUCT_CATEGORY = "WCOM_CATEGORY"
    private static final String PRODUCT_REF_TYPE = "SIMILAR"
    private static final String PRODUCT_REF_TARGET = "WCOM_PRODUCT"
    private static final String PRODUCT_AUTHORS = "WileyProductDataHubTest author"
    private static final String PRODUCT_ISBN = "WileyProductDataHubTest isbn"
    private static final String PRODUCT_PRINTISSN = "WileyProductDataHubTest print issn"
    private static final String PRODUCT_ONLINEISSN = "WileyProductDataHubTest online issn"
    private static final String PRODUCT_DATEIMPRINT = "2013-02-14T13:15:03-0800"
    private static final String PRODUCT_RELEASEDATE = "2013-02-14T13:15:03-0800"
    private static final Boolean PRODUCT_PRINTONDEMAND = true
    private static final String PRODUCT_SUBSCRIPTION_TERM = "subscription_term_1_month";
    private static final String PRODUCT_DEFAULTWAREHOUSE = "Chiba"
    private static final Boolean PRODUCT_TEXTBOOK = true
    private static final String PRODUCT_TAXCATEGORY = "WileyProductDataHubTest taxcategory"
    private static final String PRODUCT_TAXTRANSACTIONTYPE = "WileyProductDataHubTest taxtransactiontype"
    private static final String PRODUCT_EXTERNALCOMPANY = "JWSL_UK"
    private static final String PRODUCT_PDMPRODUCTCODE = "WileyProductDataHubTest PDM code"
    private static final String PRODUCT_SAPPRODUCTCODE = "WileyProductDataHubTest Sap product code"
    private static final String PRODUCT_DIGITALCONTENTTYPE = "VITAL_SOURCE"
    private static final String PRODUCT_ASSORTMENTS = "b2cassortment"
    private static final String PRODUCT_READYFORPUBLICATION = true
    private static final double PRODUCT_PRICE = 5.5D
    private static final double PRODUCT_PRICE_US = 4.4D
    private static final String PRODUCT_PRICE_CURRENCY = "USD"
    private static final long PRODUCT_PRICE_MINQUANTITY = 1L

    protected static final REQUEST_400_SIZE = [
                    "code" : TestValidationUtils.LONG_STRING_256,
                    "countries"          : [
                            "US",
                            "GB"
                    ],
                    "purchasable"        : "true",
                    "countable"          : "true",
                    "subtype"            : "SUBSCRIPTION",
                    "editionFormat"      : "DIGITAL",
                    "name"               : [
                            [
                                    "loc": "en",
                                    "val": "WileyProductDataHubTest name"
                            ]
                    ],
                    "description"        : [
                            [
                                    "loc": "en",
                                    "val": "WileyProductDataHubTest summary"
                            ]
                    ],
                    "summary"            : [
                            [
                                    "loc": "en",
                                    "val": "WileyProductDataHubTest summary"
                            ]
                    ],
                    "picture"            : [
                            "url"    : "https://wiley.com/noimage.svg",
                            "mime"   : "image/svg+xml",
                            "altText": "alttext"
                    ],
                    "onlineDate"         : "2012-02-14T13:15:03-0800",
                    "offlineDate"        : "2018-02-14T13:15:03-0800",
                    "categories"         : [
                            "WCOM_CATEGORY"
                    ],
                    "productReferences"  : [
                            [
                                    "referenceType": "SIMILAR",
                                    "productCode"  : "WCOM_PRODUCT"
                            ]
                    ],
                    "authors"            : TestValidationUtils.LONG_STRING_256,
                    "isbn"               : TestValidationUtils.LONG_STRING_256,
                    "printIssn"          : TestValidationUtils.LONG_STRING_256,
                    "onlineIssn"         : TestValidationUtils.LONG_STRING_256,
                    "dateImprint"        : "2013-02-14T13:15:03-0800",
                    "releaseDate"        : "2013-02-14T13:15:03-0800",
                    "printOnDemand"      : "true",
                    "subscriptionTerms"  : [
                            "subscription_term_1_month"
                    ],
                    "defaultWarehouse"   : TestValidationUtils.LONG_STRING_256,
                    "textbook"           : "true",
                    "taxCategory"        : TestValidationUtils.LONG_STRING_256,
                    "taxTransactionType" : TestValidationUtils.LONG_STRING_256,
                    "externalCompany"    : TestValidationUtils.LONG_STRING_256,
                    "pdmProductCode"     : TestValidationUtils.LONG_STRING_256,
                    "sapProductCode"     : TestValidationUtils.LONG_STRING_256,
                    "digitalContentType" : "VITAL_SOURCE",
                    "assortments"        : [
                            "b2cassortment"
                    ],
                    "readyForPublication": "true",
                    "prices"             : [
                            [
                                    "price"           : "5.5",
                                    "currency"        : "USD",
                                    "minQuantity"     : "1",
                                    "subscriptionTerm": "subscription_term_1_month"
                            ],
                            [
                                    "price"           : "4.4",
                                    "country"         : "US",
                                    "currency"        : "USD",
                                    "minQuantity"     : "1",
                                    "subscriptionTerm": "subscription_term_1_month"
                            ]
                    ]

    ]

    def "Should create a new item of WileyProductModel"(){
        given:
            authorizeEsbClient(restClient);

        when: "Post product create or update request"
            HttpResponseDecorator response = restClient.post(
                path: getDefaultHttpsUri() + '/wileyws/v3/products/' + WILEY_PRODUCT_CODE,
                body: postBody,
                contentType: responseFormat,
                requestContentType: requestFormat)

        then: "Response is successful and Wiley Product was created or updated"
        with(response) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            status == SC_CREATED || status == SC_NO_CONTENT
        }

        where:
            requestFormat | responseFormat | postBody
            JSON          | JSON           | "{\"countries\": [\"US\", \"GB\"]," +
                                            "\"purchasable\": \"${PRODUCT_PURCHASABLE}\"," +
                                            "\"countable\": \"${PRODUCT_COUNTABLE}\"," +
                                            "\"subtype\": \"${WILEY_PRODUCT_SUBTYPE}\"," +
                                            "\"editionFormat\": \"${PRODUCT_EDITIONFORMAT}\"," +
                                            "\"name\": [{\"loc\": \"en\", \"val\": \"${PRODUCT_EN_NAME}\"}]," +
                                            "\"description\": [{\"loc\": \"en\", \"val\": \"${PRODUCT_EN_DESCRIPTION}\"}]," +
                                            "\"summary\": [{\"loc\": \"en\", \"val\": \"${PRODUCT_EN_SUMMARY}\"}]," +
                                            "\"picture\":  {\"url\" : \"${PRODUCT_PRICTURE_URL}\", \"mime\":\"${PRODUCT_PRICTURE_MIME}\", \"altText\":\"${PRODUCT_PRICTURE_ALTTEXT}\"}," +
                                            "\"onlineDate\": \"${PRODUCT_ONLINEDATE}\"," +
                                            "\"offlineDate\": \"${PRODUCT_OFFLINEDATE}\"," +
                                            "\"categories\": [\"${PRODUCT_CATEGORY}\"]," +
                                            "\"productReferences\": [{\"referenceType\":\"${PRODUCT_REF_TYPE}\", \"productCode\":\"${PRODUCT_REF_TARGET}\"}]," +
                                            "\"authors\": \"${PRODUCT_AUTHORS}\"," +
                                            "\"isbn\": \"${PRODUCT_ISBN}\"," +
                                            "\"printIssn\": \"${PRODUCT_PRINTISSN}\"," +
                                            "\"onlineIssn\": \"${PRODUCT_ONLINEISSN}\"," +
                                            "\"dateImprint\": \"${PRODUCT_DATEIMPRINT}\"," +
                                            "\"releaseDate\": \"${PRODUCT_RELEASEDATE}\"," +
                                            "\"printOnDemand\": \"${PRODUCT_PRINTONDEMAND}\"," +
                                            "\"subscriptionTerms\": [\"${PRODUCT_SUBSCRIPTION_TERM}\"]," +
                                            "\"defaultWarehouse\": \"${PRODUCT_DEFAULTWAREHOUSE}\"," +
                                            "\"textbook\": \"${PRODUCT_TEXTBOOK}\"," +
                                            "\"taxCategory\": \"${PRODUCT_TAXCATEGORY}\"," +
                                            "\"taxTransactionType\": \"${PRODUCT_TAXTRANSACTIONTYPE}\"," +
                                            "\"externalCompany\": \"${PRODUCT_EXTERNALCOMPANY}\"," +
                                            "\"pdmProductCode\": \"${PRODUCT_PDMPRODUCTCODE}\"," +
                                            "\"sapProductCode\": \"${PRODUCT_SAPPRODUCTCODE}\"," +
                                            "\"digitalContentType\": \"${PRODUCT_DIGITALCONTENTTYPE}\"," +
                                            "\"assortments\": [\"${PRODUCT_ASSORTMENTS}\"]," +
                                            "\"readyForPublication\": \"${PRODUCT_READYFORPUBLICATION}\"," +
                                            "\"prices\":   [" +
                                                "{\"price\": \"${PRODUCT_PRICE}\", \"currency\": \"${PRODUCT_PRICE_CURRENCY}\", \"minQuantity\": \"${PRODUCT_PRICE_MINQUANTITY}\", \"subscriptionTerm\": \"${PRODUCT_SUBSCRIPTION_TERM}\"}," +
                                                "{\"price\": \"${PRODUCT_PRICE_US}\", \"country\": \"US\", \"currency\": \"${PRODUCT_PRICE_CURRENCY}\", \"minQuantity\": \"${PRODUCT_PRICE_MINQUANTITY}\", \"subscriptionTerm\": \"${PRODUCT_SUBSCRIPTION_TERM}\"}" +
                                            "]}"
    }

    def "Create a new item of WileyProductModel (400, size)"(){
        given:
        authorizeEsbClient(restClient);

        when: "Post product create or update request"
        HttpResponseDecorator response = restClient.post(
                path: getDefaultHttpsUri() + '/wileyws/v3/products/' + WILEY_PRODUCT_CODE,
                body: postBody,
                contentType: responseFormat,
                requestContentType: requestFormat)

        then: "Response is successful and Wiley Product was created or updated"
        with(response) {
            if (isEmpty(data.errors)) println(data)
            status == SC_BAD_REQUEST
            TestValidationUtils.validateSizeErrors(["code": ["min": 0,"max": 255], "authors": ["min": 0,"max": 255], "isbn": ["min": 0,"max": 255], "printIssn": ["min": 0,"max": 255], "onlineIssn": ["min": 0,"max": 255], "defaultWarehouse": ["min": 0,"max": 255], "taxCategory": ["min": 0,"max": 255], "taxTransactionType": ["min": 0,"max": 255], "externalCompany": ["min": 0,"max": 255], "pdmProductCode": ["min": 0,"max": 255], "sapProductCode": ["min": 0,"max": 255]], data.errors)
        }

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | REQUEST_400_SIZE
    }

}