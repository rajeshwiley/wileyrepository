package com.wiley.ws.test.test.groovy.webservicetests.wiley

import com.wiley.ws.test.test.groovy.webservicetests.v3.spock.AbstractSpockFlowTest
import de.hybris.bootstrap.annotations.ManualTest
import groovyx.net.http.HttpResponseDecorator
import spock.lang.Unroll

import static com.wiley.ws.test.test.groovy.webservicetests.wiley.TestValidationUtils.*
import static groovyx.net.http.ContentType.JSON
import static org.apache.http.HttpStatus.*

@ManualTest
@Unroll
public class WileyB2BUnitUpdateTest extends AbstractSpockFlowTest {


    def static getB2BUnitTemplate = {
        [
                sapAccountNumber    : SAP_ACCOUNT_NUMBER,
                name                : [[loc: "en_US", val: "Rustic - new"]],
                salesRepContactName : "Ivan",
                salesRepContactEmail: "ivan@rustic.com",
                salesRepContactPhone: "123456",
                soldToAddress       :
                        [
                                addressId  : "billingAddressId-123",
                                postcode   : "TX 78228",
                                country    : "US",
                                state      : "TX",
                                city       : "San Antonio",
                                line1      : "Camino Santa Maria",
                                firstName  : "Ivan",
                                lastName   : "Petrov",
                                phoneNumber: "123554466"
                        ],
                shippingAddresses   :
                        [[
                                 addressId  : "shippingAddressId-123",
                                 postcode   : "TX 78228",
                                 country    : "US",
                                 state      : "TX",
                                 city       : "San Antonio",
                                 line1      : "Camino Santa Maria",
                                 firstName  : "Ivan",
                                 lastName   : "Petrov",
                                 phoneNumber: "123554466"
                         ]],
                active              : "true"
        ]
    }

    def static b2bUnit = { map -> map }

    def static address = { map -> map.get("soldToAddress") }

    def static name = { map -> map.get("name")[0] }

    def static updateFieldAndGenerateBody(Map map = getB2BUnitTemplate(), Closure path, Map updates) {
        TestValidationUtils.updateFieldAndGenerateBody(map, path, updates)
    }

    def setup() {
        authorizeEsbClient(restClient);
    }

    def "Should update B2BUnit"() {

        when: "Post b2bUnit update request"
        HttpResponseDecorator response = postB2BUnitUpdateRequest(postBody)

        then: "Response is successful and B2BUnit was updated"
        with(response) {
            if (isNotEmpty(data)) println(data)
            status == SC_NO_CONTENT
        }

        where:
        postBody = generate(getB2BUnitTemplate())
    }

    def "Should return 404 status if b2bUnit not found"() {
        when:
        HttpResponseDecorator response = postB2BUnitUpdateRequest(SAP_ACCOUNT_NUMBER_NOT_EXISTED, postBody)

        then:
        with(response) {
            if (isNotEmpty(data)) println(data)
            status == SC_NOT_FOUND
        }

        where:
        postBody = "{}"

    }

    def "Should reject if sapAccountNumber length in path exceeds 255"() {
        when:
        HttpResponseDecorator response = postB2BUnitUpdateRequest(LONG_STRING_256, postBody)

        then:
        with(response) {
            if (isNotEmpty(data)) println(data)
            status == SC_BAD_REQUEST
        }

        where:
        postBody = "{}"
    }

    def "Should handle if sapAccountNumber in path passes validation"() {
        when:
        HttpResponseDecorator response = postB2BUnitUpdateRequest(LONG_STRING_255, postBody)

        then:
        with(response) {
            if (isNotEmpty(data)) println(data)
            status == SC_NOT_FOUND
        }

        where:
        postBody = "{}"
    }

    def "Should reject if field #fieldName fails validation - #validationRule"() {

        when:
        HttpResponseDecorator response = postB2BUnitUpdateRequest(updateFieldAndGenerateBody(fieldBlock, updates))

        then:
        with(response) {
            if (isNotEmpty(data)) println(data)
            status == SC_BAD_REQUEST
        }

        where:
        fieldBlock | updates                                 | fieldName              | validationRule
        b2bUnit    | [sapAccountNumber: LONG_STRING_256]     | "sapAccountNumber"     | VALIDATION_RULE_SIZE_255
        b2bUnit    | [salesRepContactName: LONG_STRING_256]  | "salesRepContactName"  | VALIDATION_RULE_SIZE_255
        b2bUnit    | [salesRepContactEmail: LONG_STRING_256] | "salesRepContactEmail" | VALIDATION_RULE_SIZE_255
        b2bUnit    | [salesRepContactPhone: LONG_STRING_256] | "salesRepContactPhone" | VALIDATION_RULE_SIZE_255
        address    | [addressId: LONG_STRING_256]            | "addressID"            | VALIDATION_RULE_SIZE_255
        address    | [postcode: LONG_STRING_256]             | "postcode"             | VALIDATION_RULE_SIZE_255
        address    | [city: LONG_STRING_256]                 | "city"                 | VALIDATION_RULE_SIZE_255
        address    | [line1: LONG_STRING_256]                | "line1"                | VALIDATION_RULE_SIZE_255
        address    | [line2: LONG_STRING_256]                | "line2"                | VALIDATION_RULE_SIZE_255
        address    | [firstName: LONG_STRING_151]            | "firstName"            | VALIDATION_RULE_SIZE_150
        address    | [lastName: LONG_STRING_256]             | "lastName"             | VALIDATION_RULE_SIZE_255
        address    | [phoneNumber: LONG_STRING_256]          | "phoneNumber"          | VALIDATION_RULE_SIZE_255
        address    | [country: "USQ"]                        | "country"              | PATTERN_COUNTRY
        address    | [state: "ss"]                           | "state"                | PATTERN_STATE
        name       | [loc: "en_U"]                           | "name.loc"             | PATTERN_LOCALE
        name       | [val: LONG_STRING_256]                  | "name.val"             | VALIDATION_RULE_SIZE_255
    }

    def "Should update if field #fieldName passes validation - #validationRule"() {

        when:
        HttpResponseDecorator response = postB2BUnitUpdateRequest(updateFieldAndGenerateBody(fieldBlock, updates))

        then:
        with(response) {
            if (isNotEmpty(data)) println(data)
            status == SC_NO_CONTENT
        }

        where:
        fieldBlock | updates                                 | fieldName              | validationRule
        b2bUnit    | [sapAccountNumber: LONG_STRING_255]     | "sapAccountNumber"     | VALIDATION_RULE_SIZE_255
        b2bUnit    | [salesRepContactName: LONG_STRING_255]  | "salesRepContactName"  | VALIDATION_RULE_SIZE_255
        b2bUnit    | [salesRepContactEmail: LONG_STRING_255] | "salesRepContactEmail" | VALIDATION_RULE_SIZE_255
        b2bUnit    | [salesRepContactPhone: LONG_STRING_255] | "salesRepContactPhone" | VALIDATION_RULE_SIZE_255
        address    | [addressId: LONG_STRING_255]            | "addressId"            | VALIDATION_RULE_SIZE_255
        address    | [postcode: LONG_STRING_255]             | "postcode"             | VALIDATION_RULE_SIZE_255
        address    | [city: LONG_STRING_255]                 | "city"                 | VALIDATION_RULE_SIZE_255
        address    | [line1: LONG_STRING_255]                | "line1"                | VALIDATION_RULE_SIZE_255
        address    | [line2: LONG_STRING_255]                | "line2"                | VALIDATION_RULE_SIZE_255
        address    | [firstName: LONG_STRING_150]            | "firstName"            | VALIDATION_RULE_SIZE_150
        address    | [lastName: LONG_STRING_255]             | "lastName"             | VALIDATION_RULE_SIZE_255
        address    | [phoneNumber: LONG_STRING_255]          | "phoneNumber"          | VALIDATION_RULE_SIZE_255
        address    | [country: "US"]                         | "country"              | PATTERN_COUNTRY
        address    | [state: "VA"]                           | "state"                | PATTERN_STATE
        address    | [country: "CN", state: "42"]            | "state"                | PATTERN_STATE
        name       | [loc: "en_US"]                          | "name.loc"             | PATTERN_LOCALE
        name       | [val: LONG_STRING_255]                  | "name.val"             | VALIDATION_RULE_SIZE_255
    }

    def "Should reject if mandatory field #fieldName is missed"() {

        when:
        HttpResponseDecorator response = postB2BUnitUpdateRequest(updateFieldAndGenerateBody(fieldBlock, updates))

        then:
        with(response) {
            if (isNotEmpty(data)) println(data)
            status == SC_BAD_REQUEST
        }

        where:
        fieldBlock | updates           | fieldName
        address    | [addressId: null] | "addressID"
        address    | [postcode: null]  | "postcode"
        address    | [country: null]   | "country"
        address    | [city: null]      | "city"
        address    | [line1: null]     | "line1"
        name       | [loc: null]       | "name.loc"
        name       | [val: null]       | "name.val"
    }

    private HttpResponseDecorator postB2BUnitUpdateRequest(String sapAccountNumber, String postBody) {
        HttpResponseDecorator response = restClient.post(
                path: getDefaultHttpsUri() + '/wileyws/v3/b2b/accounts/' + sapAccountNumber,
                body: postBody,
                contentType: JSON,
                requestContentType: JSON)
        response;
    }

    private HttpResponseDecorator postB2BUnitUpdateRequest(String postBody) {
        postB2BUnitUpdateRequest(SAP_ACCOUNT_NUMBER, postBody);
    }

}



