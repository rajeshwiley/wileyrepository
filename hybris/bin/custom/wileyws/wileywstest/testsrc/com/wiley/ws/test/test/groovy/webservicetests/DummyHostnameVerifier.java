/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package com.wiley.ws.test.test.groovy.webservicetests;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;


/**
 * The type Dummy hostname verifier.
 */
public class DummyHostnameVerifier implements HostnameVerifier
{
	/**
	 * Verify boolean.
	 *
	 * @param hostname
	 * 		the hostname
	 * @param session
	 * 		the session
	 * @return the boolean
	 */
	@Override
	public boolean verify(final String hostname, final SSLSession session)
	{
		return true;
	}
}
