/**
 *
 */
package com.wiley.ws.test.test.groovy.webservicetests.v3.spock.orders

import com.wiley.ws.test.test.groovy.webservicetests.v3.spock.AbstractSpockTransactionalTest
import com.wiley.ws.test.test.groovy.webservicetests.wiley.TestValidationUtils
import de.hybris.bootstrap.annotations.ManualTest
import groovy.json.JsonSlurper
import groovyx.net.http.HttpResponseDecorator
import spock.lang.Unroll

import static groovyx.net.http.ContentType.JSON
import static org.apache.http.HttpStatus.*

/**
 *
 * Can not be run multiple times
 */
@ManualTest
@Unroll
class ShippingEventTest extends AbstractSpockTransactionalTest {

    static final String ORDER_CODE = "testOrder1"
    def static jsonSlurper = new JsonSlurper()

    def static SHIPPING_EVENT_REQUEST_VALID = jsonSlurper.parseText('''{
            "shippingDate": "2016-09-01T23:20:50-08:30",
            "trackingId"  : "TEST_TRACKING_ID_01",
            "carrier"     : "TEST_CARRIER_01",
            "entries"     : [
                    {
                            "isbn"           : "222222222222",
                            "shippedQuantity": 1
                    },
                    {
                            "isbn"           : "222222222221",
                            "shippedQuantity": 1
                    }
            ]
    }''')

    def static SHIPPING_EVENT_REQUEST_NO_QUANTITY = jsonSlurper.parseText('''{
    "shippingDate": "2002-10-02T10:00:00-05:00",
    "trackingId": "TEST_TRACKING_ID_03",
    "carrier": "TEST_CARRIER_03",
    "entries": [{
        "isbn": "222"
    }]
}''')

    def static SHIPPING_EVENT_REQUEST_NO_ISBN = jsonSlurper.parseText('''{
    "shippingDate": "2016-09-01T23:20:50-08:30",
    "trackingId": "TEST_TRACKING_ID_01",
    "carrier": "TEST_CARRIER_01",
    "entries": [{
        "isbn": "NO",
        "shippedQuantity": 1
    },
    {
        "isbn": "222222222221",
        "shippedQuantity": 1
    }]
}''')

    def static SHIPPING_EVENT_REQUEST_TOO_BIG_QUANTITY = jsonSlurper.parseText('''{
            "shippingDate": "2016-09-01T23:20:50-08:30",
            "trackingId"  : "TEST_TRACKING_ID_01",
            "carrier"     : "TEST_CARRIER_01",
            "entries"     : [
                    {
                            "isbn"           : "222222222222",
                            "shippedQuantity": 1000
                    },
                    {
                            "isbn"           : "222222222221",
                            "shippedQuantity": 1
                    }
            ]
}''')

    def static SHIPPING_EVENT_REQUEST_WITHOUT_REQUIRED_ATTRIBUTES = jsonSlurper.parseText('''{
    "entries": [{
        "isbn": "222",
        "shippedQuantity": 1
    }]
}''')


    def "Trusted client requests valid shipping event: #requestFormat"() {
        given: "trusted client"
        authorizeTrustedClient(restClient)

        when: "trusted client requests shipping event"
        HttpResponseDecorator response = restClient.put(
                path: getBasePath() + "/orders/" + ORDER_CODE + "/shipping",
                contentType: responseFormat,
                body: postBody,
                requestContentType: requestFormat)

        then: "he receives 200 response with empty body"
        with(response) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) printJsonData data
            status == SC_NO_CONTENT
            isEmpty(data)
        }

        cleanup: "restore order status"
//        TODO: add ability to restore initial order status to make test re-runnable @see AbstractCartTest.setStockStatus

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | SHIPPING_EVENT_REQUEST_VALID
    }

    def "Trusted client requests shipping event with no such isbn: #requestFormat"() {
        given: "trusted client"
        authorizeTrustedClient(restClient)

        when: "trusted client shipping event"
        HttpResponseDecorator responseNoIsbn = restClient.put(
                path: getBasePath() + "/orders/" + ORDER_CODE + "/shipping",
                contentType: responseFormat,
                body: postBody,
                requestContentType: requestFormat)

        then: "he receives 400 response with Isbn Not Found Error"
        with(responseNoIsbn) {
            if (isNotEmpty(data) && isEmpty(data.errors)) printJsonData data
            status == SC_BAD_REQUEST
            data.errors[0]['type'] == 'IsbnNotFoundError'
            data.errors[0]['message'] == 'Order entry not found for isbn given. Order code = [testOrder1], ISBN = [NO]'
        }

        cleanup: "restore order status"
//        TODO: add ability to restore initial order status to make test re-runnable @see AbstractCartTest.setStockStatus

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | SHIPPING_EVENT_REQUEST_NO_ISBN
    }

    def "Trusted client requests shipping event with too big shipped quantity: #requestFormat"() {
        given: "trusted client"
        authorizeTrustedClient(restClient)

        when: "trusted client shipping event"
        HttpResponseDecorator responseNoIsbn = restClient.put(
                path: getBasePath() + "/orders/" + ORDER_CODE + "/shipping",
                contentType: responseFormat,
                body: postBody,
                requestContentType: requestFormat)

        then: "he receives 400 response with Invalid Shipped Quantiyt Error"
        with(responseNoIsbn) {
            if (isNotEmpty(data) && isEmpty(data.errors)) printJsonData data
            status == SC_BAD_REQUEST
            data.errors[0]['type'] == 'WileycomWsOrderInvalidShippedQuantiytError'
            data.errors[0]['message'].contains("is greater than order product quantity")
        }

        cleanup: "restore order status"
//        TODO: add ability to restore initial order status to make test re-runnable @see AbstractCartTest.setStockStatus

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | SHIPPING_EVENT_REQUEST_TOO_BIG_QUANTITY
    }

    def "Trusted client requests shipping event with no such order: #requestFormat"() {
        given: "trusted client"
        authorizeTrustedClient(restClient)

        when: "trusted client requests shipping event"
        HttpResponseDecorator response = restClient.put(
                path: getBasePath() + "/orders/" + "NO_SUCH_ORDER" + "/shipping",
                contentType: responseFormat,
                body: postBody,
                requestContentType: requestFormat)

        then: "he receives 404 response with error order not found"
        with(response) {
            if (isNotEmpty(data) && isEmpty(data.errors)) printJsonData data
            status == SC_NOT_FOUND
            data.errors[0]['type'] == 'OrderWsNotFoundError'
            data.errors[0]['message'] == 'Order not found, order code = [NO_SUCH_ORDER]'
        }

        cleanup: "restore order status"
//        TODO: add ability to restore initial order status to make test re-runnable @see AbstractCartTest.setStockStatus

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | SHIPPING_EVENT_REQUEST_VALID
    }

    def "Trusted client requests shipping event without shipping quantity for entry: #requestFormat"() {
        given: "trusted client"
        authorizeTrustedClient(restClient)

        when: "trusted client requests shipping event"
        HttpResponseDecorator response = restClient.put(
                path: getBasePath() + "/orders/" + ORDER_CODE + "/shipping",
                contentType: responseFormat,
                body: postBody,
                requestContentType: requestFormat)

        then: "he receives 400 response with validation errors"
        with(response) {
            if (isNotEmpty(data) && isEmpty(data.errors)) printJsonData data
            status == SC_BAD_REQUEST
            def error = data.errors[0]
            error['message'] == 'The attribute [entries[0].shippedQuantity] must not be null.'
            error['reason'] == 'missing'
            error['subject'] == 'entries[0].shippedQuantity'
            error['subjectType'] == 'parameter'
            error['type'] == 'ValidationError'
        }

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | SHIPPING_EVENT_REQUEST_NO_QUANTITY
    }

    def "Trusted client requests shipping event without required attributes: #requestFormat"() {
        given: "trusted client"
        authorizeTrustedClient(restClient)

        when: "trusted client requests shipping event"
        HttpResponseDecorator response = restClient.put(
                path: getBasePath() + "/orders/" + ORDER_CODE + "/shipping",
                contentType: responseFormat,
                body: postBody,
                requestContentType: requestFormat)

        then: "he receives 400 response with validation errors"
        with(response) {
            if (isNotEmpty(data) && isEmpty(data.errors)) printJsonData data
            status == SC_BAD_REQUEST

            TestValidationUtils.validateRequiredAttributesErrors(['carrier', 'shippingDate', 'trackingId'], data.errors)
        }

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | SHIPPING_EVENT_REQUEST_WITHOUT_REQUIRED_ATTRIBUTES
    }
}
