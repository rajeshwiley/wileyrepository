/**
 *
 */
package com.wiley.ws.test.test.groovy.webservicetests.v2.spock.carts

import com.fasterxml.jackson.databind.util.ISO8601DateFormat
import de.hybris.bootstrap.annotations.ManualTest
import groovyx.net.http.HttpResponseDecorator
import spock.lang.Unroll

import static groovyx.net.http.ContentType.JSON
import static org.apache.http.HttpStatus.*

/**
 *
 * `
 */
@Unroll
@ManualTest
class AsAddCartEntryTest extends AbstractCartTest {
    public static final AS_SITE = 'asSite'
    public static final CART_TYPE = "GENERAL"
    public static final COUNTRY_EUR = "RU"
    public static final CURRENCY_EUR = "EUR"
    public static final COUNTRY_USD = "US"
    public static final CURRENCY_USD = "USD"
    public static final COUNTRY_GBP = "GB"
    public static final CURRENCY_GBP = "GBP"
    public static final NON_EXISTING_CUR = "GBX"

    public static final NON_EXISTING_CUSTOMER_ID = 'non-existing-customer'
    public static final NON_EXISTING_CART_ID = 'non-existing-cart'

    public static final HYBRIS_PRICE_USD = ["value": 11.1, "currency": "USD"]
    public static final HYBRIS_PRICE_EUR = ["value": 11.2, "currency": "EUR"]
    public static final HYBRIS_PRICE_GBP = ["value": 11.3, "currency": "GBP"]
    public static final HYBRIS_DISCOUNT_USD = ["code": "wstest_usd", "value": 1, "absolute": true, "currency": "USD"]
    public static final HYBRIS_DISCOUNT_EUR = ["code": "wstest_eur", "value": 2, "absolute": true, "currency": "EUR"]
    public static final HYBRIS_DISCOUNT_GBP = ["code": "wstest_gbp", "value": 3, "absolute": true, "currency": "GBP"]

    public static final EXTERNAL_PRICE_USD = ["value": 11.19, "currency": "USD"]
    public static final EXTERNAL_PRICE_EUR = ["value": 22.29, "currency": "EUR"]
    public static final EXTERNAL_PRICE_GBP = ["value": 33.39, "currency": "GBP"]
    public static final EXTERNAL_PRICE_NON_EXISTING_CUR = ["value": 10.00, "currency": NON_EXISTING_CUR]
    public static
    final EXTERNAL_DISCOUNT_USD = ["code": "wstest_usd_external", "value": 1.9, "absolute": true, "currency": "USD", "couponCodes": []]
    public static
    final EXTERNAL_DISCOUNT_EUR = ["code": "wstest_eur_external", "value": 1.8, "absolute": true, "currency": "EUR", "couponCodes": []]
    public static
    final EXTERNAL_DISCOUNT_GBP = ["code": "wstest_eur_external", "value": 1.7, "absolute": true, "currency": "GBP", "couponCodes": []]
    // TODO: absolute attribute can be skipped, but NPE is thrown at the moment
    public static
    final EXTERNAL_RELATIVE_DISCOUNT = ["code": "wstest_relative_external", "value": 10.00, "absolute": false, "couponCodes": []]
    public static
    final EXTERNAL_DISCOUNT_NON_EXISTING_CUR = ["code": "wstest_non_existing_external", "value": 10.00, "absolute": true, "currency": NON_EXISTING_CUR]

    // TODO: timezone should contain colon delimiter '-04:00'. Should be fixed in scope of ECSC-23643.
    public static final ISO_DATE_FORMAT = new ISO8601DateFormat()
    public static final DELIVERY_DATE_REQUEST = "2018-10-02T15:00:00-0400"
    public static final DELIVERY_DATE_REQUEST_DATE = ISO_DATE_FORMAT.parse(DELIVERY_DATE_REQUEST)
    public static final ADDITIONAL_INFO = "some additional info"

    public static final EXTERNAL_PRICES = [
            EXTERNAL_PRICE_USD,
            EXTERNAL_PRICE_EUR,
            EXTERNAL_PRICE_GBP
    ]
    public static final EXTERNAL_DISCOUNTS = [
            EXTERNAL_DISCOUNT_USD,
            EXTERNAL_DISCOUNT_EUR,
            EXTERNAL_DISCOUNT_GBP
    ]
    public static final List<LinkedHashMap<String, Serializable>> EXTERNAL_DISCOUNTS_WITH_RELATIVE = [
            EXTERNAL_DISCOUNT_USD,
            EXTERNAL_DISCOUNT_EUR,
            EXTERNAL_DISCOUNT_GBP,
            EXTERNAL_RELATIVE_DISCOUNT
    ]
    public static final EXTERNAL_PRICES_NON_EXISTING_CUR = [
            EXTERNAL_PRICE_NON_EXISTING_CUR
    ]
    public static final EXTERNAL_DISCOUNTS_NON_EXISTING_CUR = [
            EXTERNAL_DISCOUNT_NON_EXISTING_CUR
    ]

    public static final ADD_CART_ENTRY_BODY = [
            "productCode"      : PRODUCT_CODE,
            "businessItemId"   : BUSINESS_ITEM_ID,
            "quantity"         : QUANTITY,
            "deliveryDate"     : DELIVERY_DATE_REQUEST,
            "additionalInfo"   : ADDITIONAL_INFO,
            "externalPrices"   : EXTERNAL_PRICES,
            "externalDiscounts": EXTERNAL_DISCOUNTS
    ]

    public static final QUANTITY = 3
    public static final NON_COUNTABLE_MAX_QUANTITY = 1
    public static final BUSINESS_KEY = "D1A5279D-B27D-4CD4-A05E-EFDD53D08E8D-10.vp00008-X1"
    public static final BUSINESS_ITEM_ID = "10.1002"
    public static final PRODUCT_CODE = "wstest_vp001"
    public static final NON_EXISTING_PRODUCT_CODE = "non-existing-product"
    public static final NON_ACTIVE_PRODUCT_CODE = "wstest_vp_not_active"
    public static final NO_PRICES_PRODUCT_CODE = "wstest_vp_no_prices"
    // TODO: countable logic will be changed in scope of ECSC-23459
    public static final NON_COUNTABLE_PRODUCT_CODE = "wstest_vp002"
    public static final EXPECTED_SUBTOTAL_PRICE_USD = 11.19
    public static final EXPECTED_TOTAL_PRICE_USD = 2.49
    public static final EXPECTED_TOTAL_PRICE_WITH_REL_DISCOUNT_USD = 2.24
    public static final EXPECTED_SUBTOTAL_PRICE_EUR = 22.29
    public static final EXPECTED_TOTAL_PRICE_EUR = 10.89
    public static final EXPECTED_TOTAL_PRICE_WITH_REL_DISCOUNT_EUR = 9.8
    public static final EXPECTED_SUBTOTAL_PRICE_GBP = 33.39
    public static final EXPECTED_TOTAL_PRICE_GBP = 19.29
    public static final EXPECTED_TOTAL_PRICE_WITH_REL_DISCOUNT_GBP = 17.36

    public static final EXPECTED_SUBTOTAL_PRICE_NO_EXTERNAL_USD = 33.3
    public static final EXPECTED_TOTAL_PRICE_NO_EXTERNAL_USD = 30.3
    public static final EXPECTED_SUBTOTAL_PRICE_NO_EXTERNAL_EUR = 33.6
    public static final EXPECTED_TOTAL_PRICE_NO_EXTERNAL_EUR = 27.6
    public static final EXPECTED_SUBTOTAL_PRICE_NO_EXTERNAL_GBP = 33.9
    public static final EXPECTED_TOTAL_PRICE_NO_EXTERNAL_GBP = 24.9
    public static final LinkedHashMap<String, Serializable> CREATE_CART_ENTRY_BODY = [
            "productCode"   : PRODUCT_CODE,
            "businessItemId": BUSINESS_ITEM_ID,
            "businessKey"   : BUSINESS_KEY,
            "quantity"      : NON_COUNTABLE_MAX_QUANTITY,
            "deliveryDate"  : DELIVERY_DATE_REQUEST
    ]

    def "Registered customer add cart entry to cart with external discounts and prices: #currencyCode"() {
        given: "a registered customer"
        authorizeTrustedClient(restClient)

        def createdCartId = createCart(countryCode)

        when: "customer wants to add entry to cart"
        HttpResponseDecorator addEntryCartResponse = restClient.post(
                path: getBasePathWithSite() + '/users/' + CUSTOMER_USERNAME + '/carts/' + createdCartId + '/entries',
                body: ADD_CART_ENTRY_BODY,
                headers: ["Fields-Set-Level": "FULL"],
                contentType: JSON,
                requestContentType: JSON
        )

        then: "he retrieves added entry"
        with(addEntryCartResponse) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)

            deleteCart(createdCartId)

            status == SC_CREATED

            data.businessItemId == BUSINESS_ITEM_ID

            ISO_DATE_FORMAT.parse(data.deliveryDate) == DELIVERY_DATE_REQUEST_DATE
            data.additionalInfo == ADDITIONAL_INFO

            isNotEmpty(data.discounts)
            data.discounts.size() == 2
            data.discounts.any { discount ->
                discount == expectedDiscount1
            }
            data.discounts.any { discount ->
                discount == expectedDiscount2
            }
            data.externalDiscounts == EXTERNAL_DISCOUNTS
            data.externalPrices == EXTERNAL_PRICES

            isNotEmpty(data.entryId)
            data.orderId == createdCartId
            data.productCode == PRODUCT_CODE
            data.quantity == QUANTITY
            data.subtotalPrice == expectedSubtotalPrice
            data.totalPrice == expectedTotalPrice
        }

        where:
        currencyCode | countryCode | expectedDiscount1   | expectedDiscount2     | expectedSubtotalPrice       | expectedTotalPrice
        CURRENCY_USD | COUNTRY_USD | HYBRIS_DISCOUNT_USD | EXTERNAL_DISCOUNT_USD | EXPECTED_SUBTOTAL_PRICE_USD | EXPECTED_TOTAL_PRICE_USD
        CURRENCY_EUR | COUNTRY_EUR | HYBRIS_DISCOUNT_EUR | EXTERNAL_DISCOUNT_EUR | EXPECTED_SUBTOTAL_PRICE_EUR | EXPECTED_TOTAL_PRICE_EUR
        CURRENCY_GBP | COUNTRY_GBP | HYBRIS_DISCOUNT_GBP | EXTERNAL_DISCOUNT_GBP | EXPECTED_SUBTOTAL_PRICE_GBP | EXPECTED_TOTAL_PRICE_GBP

    }

    def "Registered customer add cart entry to cart with relative external discounts and prices: #currencyCode"() {
        given: "a registered customer"
        authorizeTrustedClient(restClient)

        def createdCartId = createCart(countryCode)

        when: "customer wants to add entry to cart"
        HttpResponseDecorator addEntryCartResponse = restClient.post(
                path: getBasePathWithSite() + '/users/' + CUSTOMER_USERNAME + '/carts/' + createdCartId + '/entries',
                body: [
                        "productCode"      : PRODUCT_CODE,
                        "businessItemId"   : BUSINESS_ITEM_ID,
                        "businessKey"      : BUSINESS_KEY,
                        "quantity"         : QUANTITY,
                        "deliveryDate"     : DELIVERY_DATE_REQUEST,
                        "externalPrices"   : EXTERNAL_PRICES,
                        "externalDiscounts": EXTERNAL_DISCOUNTS_WITH_RELATIVE
                ],
                headers: ["Fields-Set-Level": "FULL"],
                contentType: JSON,
                requestContentType: JSON
        )

        then: "he retrieves added entry"
        with(addEntryCartResponse) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)

            deleteCart(createdCartId)

            status == SC_CREATED

            isNotEmpty(data.discounts)
            data.discounts.size() == 3
            data.discounts.any { discount ->
                discount == expectedDiscount1
            }
            data.discounts.any { discount ->
                discount == expectedDiscount2
            }
            data.discounts.any { discount ->
                discount.code == expectedDiscount3.code
                discount.value == expectedDiscount3.value
                discount.absolute == expectedDiscount3.absolute
                // TODO: check if currency should be set for relative prices in response
                discount.currency == currencyCode
            }

            data.externalDiscounts == EXTERNAL_DISCOUNTS_WITH_RELATIVE
            data.externalPrices == EXTERNAL_PRICES

            data.subtotalPrice == expectedSubtotalPrice
            data.totalPrice == expectedTotalPrice
        }

        where:
        currencyCode | countryCode | expectedDiscount1   | expectedDiscount2     | expectedDiscount3          | expectedSubtotalPrice       | expectedTotalPrice
        CURRENCY_USD | COUNTRY_USD | HYBRIS_DISCOUNT_USD | EXTERNAL_DISCOUNT_USD | EXTERNAL_RELATIVE_DISCOUNT | EXPECTED_SUBTOTAL_PRICE_USD | EXPECTED_TOTAL_PRICE_WITH_REL_DISCOUNT_USD
        CURRENCY_EUR | COUNTRY_EUR | HYBRIS_DISCOUNT_EUR | EXTERNAL_DISCOUNT_EUR | EXTERNAL_RELATIVE_DISCOUNT | EXPECTED_SUBTOTAL_PRICE_EUR | EXPECTED_TOTAL_PRICE_WITH_REL_DISCOUNT_EUR
        CURRENCY_GBP | COUNTRY_GBP | HYBRIS_DISCOUNT_GBP | EXTERNAL_DISCOUNT_GBP | EXTERNAL_RELATIVE_DISCOUNT | EXPECTED_SUBTOTAL_PRICE_GBP | EXPECTED_TOTAL_PRICE_WITH_REL_DISCOUNT_GBP

    }

    def "Registered customer add cart entry to cart without external discounts and prices: #currencyCode"() {
        given: "a registered customer"
        authorizeTrustedClient(restClient)

        def createdCartId = createCart(countryCode)

        when: "customer wants to add entry to cart"
        HttpResponseDecorator addEntryCartResponse = restClient.post(
                path: getBasePathWithSite() + '/users/' + CUSTOMER_USERNAME + '/carts/' + createdCartId + '/entries',
                body: [
                        "productCode"   : PRODUCT_CODE,
                        "businessItemId": BUSINESS_ITEM_ID,
                        "businessKey"   : BUSINESS_KEY,
                        "quantity"      : QUANTITY,
                        "deliveryDate"  : DELIVERY_DATE_REQUEST
                ],
                headers: ["Fields-Set-Level": "FULL"],
                contentType: JSON,
                requestContentType: JSON
        )

        then: "he retrieves added entry"
        with(addEntryCartResponse) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)

            deleteCart(createdCartId)

            status == SC_CREATED

            data.businessItemId == BUSINESS_ITEM_ID
            data.businessKey == BUSINESS_KEY

            ISO_DATE_FORMAT.parse(data.deliveryDate) == DELIVERY_DATE_REQUEST_DATE

            isNotEmpty(data.discounts)
            data.discounts.size() == 1
            data.discounts[0] == expectedDiscount
            isEmpty(data.externalDiscounts)
            isEmpty(data.externalPrices)

            isNotEmpty(data.entryId)
            data.orderId == createdCartId
            data.productCode == PRODUCT_CODE
            data.quantity == QUANTITY
            data.subtotalPrice == expectedSubtotalPrice
            data.totalPrice == expectedTotalPrice
        }

        where:
        currencyCode | countryCode | expectedDiscount    | expectedSubtotalPrice                   | expectedTotalPrice
        CURRENCY_USD | COUNTRY_USD | HYBRIS_DISCOUNT_USD | EXPECTED_SUBTOTAL_PRICE_NO_EXTERNAL_USD | EXPECTED_TOTAL_PRICE_NO_EXTERNAL_USD
        CURRENCY_EUR | COUNTRY_EUR | HYBRIS_DISCOUNT_EUR | EXPECTED_SUBTOTAL_PRICE_NO_EXTERNAL_EUR | EXPECTED_TOTAL_PRICE_NO_EXTERNAL_EUR
        CURRENCY_GBP | COUNTRY_GBP | HYBRIS_DISCOUNT_GBP | EXPECTED_SUBTOTAL_PRICE_NO_EXTERNAL_GBP | EXPECTED_TOTAL_PRICE_NO_EXTERNAL_GBP

    }

    def "New cart entry is always created instead of merge with existing one"() {
        given: "a registered customer"
        authorizeTrustedClient(restClient)

        // creates new cart
        def createdCartId = createCart()

        when: "customer wants to add entry to cart"
        // adds cart entry
        HttpResponseDecorator addEntryCartResponse1 = restClient.post(
                path: getBasePathWithSite() + '/users/' + CUSTOMER_USERNAME + '/carts/' + createdCartId + '/entries',
                body: ADD_CART_ENTRY_BODY,
                contentType: JSON,
                requestContentType: JSON
        )

        then: "he retrieves added entry"
        with(addEntryCartResponse1) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            status == SC_CREATED
            isNotEmpty(data.entryId)
        }
        def addedEntry1 = addEntryCartResponse1.data

        // adds same cart entry again
        HttpResponseDecorator addEntryCartResponse2 = restClient.post(
                path: getBasePathWithSite() + '/users/' + CUSTOMER_USERNAME + '/carts/' + createdCartId + '/entries',
                body: ADD_CART_ENTRY_BODY,
                contentType: JSON,
                requestContentType: JSON
        )
        with(addEntryCartResponse2) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            status == SC_CREATED
            isNotEmpty(data.entryId)
        }
        def addedEntry2 = addEntryCartResponse1.data

        HttpResponseDecorator getUserCartsResponse = restClient.get(
                path: getBasePathWithSite() + '/users/' + CUSTOMER_USERNAME + '/carts',
                query: ["type": CART_TYPE],
                contentType: JSON,
                requestContentType: JSON
        )
        with(getUserCartsResponse) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)

            deleteCart(createdCartId)

            status == SC_OK

            def foundCart = data.find { cart ->
                cart.id == createdCartId
            }
            isNotEmpty(foundCart)
            foundCart.entries.count {
                entry -> entry == addedEntry1
            } == 1
            foundCart.entries.count {
                entry -> entry == addedEntry2
            } == 1
        }
    }

    def "New cart entry with same businessKey not created"() {
        given: "a registered customer"
        authorizeTrustedClient(restClient)

        // creates new cart
        def createdCartId = createCart()

        when: "customer wants to add entry with same businessKey to cart"
        def uniqueBusinessKey = UUID.randomUUID().toString()
        // adds cart entry
        HttpResponseDecorator addEntryCartResponse1 = restClient.post(
                path: getBasePathWithSite() + '/users/' + CUSTOMER_USERNAME + '/carts/' + createdCartId + '/entries',
                body: [
                        "productCode"      : PRODUCT_CODE,
                        "businessItemId"   : BUSINESS_ITEM_ID,
                        "businessKey"      : uniqueBusinessKey,
                        "quantity"         : QUANTITY,
                        "deliveryDate"     : DELIVERY_DATE_REQUEST,
                        "externalPrices"   : EXTERNAL_PRICES,
                        "externalDiscounts": EXTERNAL_DISCOUNTS
                ],
                headers: ["Enforce-Unique-Business-Key": true],
                contentType: JSON,
                requestContentType: JSON
        )

        then: "he retrieves added entry"
        with(addEntryCartResponse1) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            status == SC_CREATED
            isNotEmpty(data.entryId)
        }
        def addedEntry1 = addEntryCartResponse1.data

        // adds same cart entry again
        HttpResponseDecorator addEntryCartResponse2 = restClient.post(
                path: getBasePathWithSite() + '/users/' + CUSTOMER_USERNAME + '/carts/' + createdCartId + '/entries',
                body: [
                        "productCode"      : PRODUCT_CODE,
                        "businessItemId"   : BUSINESS_ITEM_ID,
                        "businessKey"      : uniqueBusinessKey,
                        "quantity"         : QUANTITY,
                        "deliveryDate"     : DELIVERY_DATE_REQUEST,
                        "externalPrices"   : EXTERNAL_PRICES,
                        "externalDiscounts": EXTERNAL_DISCOUNTS
                ],
                headers: ["Enforce-Unique-Business-Key": true],
                contentType: JSON,
                requestContentType: JSON
        )
        with(addEntryCartResponse2) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            status == SC_CONFLICT
            data.entryId == addedEntry1.entryId
        }

        HttpResponseDecorator getUserCartsResponse = restClient.get(
                path: getBasePathWithSite() + '/users/' + CUSTOMER_USERNAME + '/carts',
                query: ["type": CART_TYPE],
                contentType: JSON,
                requestContentType: JSON
        )
        with(getUserCartsResponse) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)

            deleteCart(createdCartId)

            status == SC_OK

            def foundCart = data.find { cart ->
                cart.id == createdCartId
            }
            isNotEmpty(foundCart)
            foundCart.entries.size() == 1
            foundCart.entries[0] == addedEntry1
        }
    }

    def "If entry of type '#entryType' with same businessKey is already exists with status #entryStatus then #responseStatus."() {
        given: "a registered customer"
        authorizeTrustedClient(restClient)

        when: "customer wants to add entry to cart"

        // creates new cart
        def createdCartId = createCart()

        // adds cart entry with same businessKey as existing MULTISTATUS cart and order entry
        HttpResponseDecorator addEntryCartResponse7 = restClient.post(
                path: getBasePathWithSite() + '/users/' + CUSTOMER_USERNAME + '/carts/' + createdCartId + '/entries',
                body: [
                        "productCode"   : PRODUCT_CODE,
                        "businessItemId": BUSINESS_ITEM_ID,
                        "businessKey"   : existingBusinessKey,
                        "quantity"      : QUANTITY,
                        "deliveryDate"  : DELIVERY_DATE_REQUEST
                ],
                headers: ["Enforce-Unique-Business-Key": true],
                contentType: JSON,
                requestContentType: JSON
        )

        then: "he retrieves 409"
        with(addEntryCartResponse7) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)

            deleteCart(createdCartId)

            status == responseStatus
        }

        where:
        existingBusinessKey             | entryType    | entryStatus | responseStatus
        "wscart-cancelled-businessKey"  | "CartEntry"  | "CANCELLED" | SC_CREATED
        "wsorder-cancelled-businessKey" | "OrderEntry" | "CANCELLED" | SC_CREATED
        "wscart-created-businessKey"    | "CartEntry"  | "CREATED"   | SC_CONFLICT
        "wsorder-created-businessKey"   | "OrderEntry" | "CREATED"   | SC_CONFLICT
        "wscart-empty-businessKey"      | "CartEntry"  | "empty"     | SC_CONFLICT
        "wsorder-empty-businessKey"     | "OrderEntry" | "empty"     | SC_CONFLICT
    }

    def "404 if customer not found"() {
        given: "a registered customer"
        authorizeTrustedClient(restClient)

        // creates new cart
        def createdCartId = createCart()

        when: "customer wants to add entry non-existing userId"
        // adds cart entry
        HttpResponseDecorator addEntryCartResponse1 = restClient.post(
                path: getBasePathWithSite() + '/users/' + NON_EXISTING_CUSTOMER_ID + '/carts/' + createdCartId + '/entries',
                body: CREATE_CART_ENTRY_BODY,
                contentType: JSON,
                requestContentType: JSON
        )

        then: "he retrieves 404 response"
        with(addEntryCartResponse1) {
            if (isNotEmpty(data) && isEmpty(data.errors)) println(data)

            deleteCart(createdCartId)

            status == SC_NOT_FOUND
            data.errors.size() == 1
            data.errors[0].type == "UnknownResourceError"
            data.errors[0].message == "Cannot find user with uid '" + NON_EXISTING_CUSTOMER_ID + "'"
        }
    }

    def "404 if cart does not exist"() {
        given: "a registered customer"
        authorizeTrustedClient(restClient)

        when: "customer wants to add entry to non-existing cart"

        // adds cart entry
        HttpResponseDecorator addEntryCartResponse1 = restClient.post(
                path: getBasePathWithSite() + '/users/' + CUSTOMER_USERNAME + '/carts/' + NON_EXISTING_CART_ID + '/entries',
                body: ADD_CART_ENTRY_BODY,
                contentType: JSON,
                requestContentType: JSON
        )

        then: "he retrieves 404 response"
        with(addEntryCartResponse1) {
            if (isNotEmpty(data) && isEmpty(data.errors)) println(data)
            status == SC_NOT_FOUND
            data.errors.size() == 1
            data.errors[0].type == "UnknownResourceError"
            data.errors[0].message == ("Cannot find cart with guid '" + NON_EXISTING_CART_ID + "'")
        }
    }

    def "400 if cart exists but does not belong to specified user"() {
        given: "a registered customer"
        authorizeTrustedClient(restClient)

        when: "customer wants to add entry to cart of other user"

        // creates new cart
        def createdCartId = createCart()

        // adds cart entry
        HttpResponseDecorator addEntryCartResponse1 = restClient.post(
                path: getBasePathWithSite() + '/users/' + CUSTOMER_USERNAME_2 + '/carts/' + createdCartId + '/entries',
                body: ADD_CART_ENTRY_BODY,
                contentType: JSON,
                requestContentType: JSON
        )

        then: "he retrieves 400 response"
        with(addEntryCartResponse1) {
            if (isNotEmpty(data) && isEmpty(data.errors)) println(data)

            deleteCart(createdCartId)

            status == SC_BAD_REQUEST
            data.errors.size() == 1
            data.errors[0].type == "BadRequestError"
            data.errors[0].message == ("Cart with guid '" + createdCartId + "' was found, but does not belong to the specified customer")
        }
    }

    def "400 if active product with the specified code was not found: #notFoundReason "() {
        given: "a registered customer"
        authorizeTrustedClient(restClient)

        when: "customer wants to add entry to cart of other user"

        // creates new cart
        def createdCartId = createCart()

        // adds cart entry
        HttpResponseDecorator addEntryCartResponse1 = restClient.post(
                path: getBasePathWithSite() + '/users/' + CUSTOMER_USERNAME + '/carts/' + createdCartId + '/entries',
                body: [
                        "productCode"   : productCode,
                        "businessItemId": BUSINESS_ITEM_ID,
                        "businessKey"   : BUSINESS_KEY,
                        "quantity"      : QUANTITY,
                        "deliveryDate"  : DELIVERY_DATE_REQUEST
                ],
                contentType: JSON,
                requestContentType: JSON
        )

        then: "he retrieves 400 response"
        with(addEntryCartResponse1) {
            if (isNotEmpty(data) && isEmpty(data.errors)) println(data)

            deleteCart(createdCartId)

            status == SC_BAD_REQUEST
            data.errors.size() == 1
            data.errors[0].type == expectedErrorType
            data.errors[0].message == expectedErrorMessage
        }

        where:
        productCode               | notFoundReason | expectedErrorType        | expectedErrorMessage
        NON_EXISTING_PRODUCT_CODE | "non-existing" | "UnknownIdentifierError" | "Product with code '" + NON_EXISTING_PRODUCT_CODE + "' not found!"
        NON_ACTIVE_PRODUCT_CODE   | "non-active"   | "ProductNotVisibleError" | "Product [" + NON_ACTIVE_PRODUCT_CODE + "] is not available because of Offline/Online date"
    }

    def "400 if wrong currency is provided in external prices"() {
        given: "a registered customer"
        authorizeTrustedClient(restClient)

        // creates new cart
        def createdCartId = createCart()

        when: "customer wants to add entry with non-existing currency code"
        // adds cart entry
        HttpResponseDecorator addEntryCartResponse1 = restClient.post(
                path: getBasePathWithSite() + '/users/' + CUSTOMER_USERNAME + '/carts/' + createdCartId + '/entries',
                body: [
                        "productCode"      : PRODUCT_CODE,
                        "businessItemId"   : BUSINESS_ITEM_ID,
                        "businessKey"      : BUSINESS_KEY,
                        "quantity"         : QUANTITY,
                        "deliveryDate"     : DELIVERY_DATE_REQUEST,
                        "externalPrices"   : externalPrices,
                        "externalDiscounts": externalDiscounts
                ],
                contentType: JSON,
                requestContentType: JSON
        )

        then: "he retrieves 400 response"
        with(addEntryCartResponse1) {
            if (isNotEmpty(data) && isEmpty(data.errors)) println(data)

            deleteCart(createdCartId)

            status == SC_BAD_REQUEST
            data.errors.size() == 1
            data.errors[0].type == "UnknownIdentifierError"
            data.errors[0].message == ("CurrencyModel with isocode '" + NON_EXISTING_CUR + "' not found!")
        }

        where:
        externalPrices                   | externalDiscounts
        EXTERNAL_PRICES_NON_EXISTING_CUR | EXTERNAL_DISCOUNTS
        EXTERNAL_PRICES                  | EXTERNAL_DISCOUNTS_NON_EXISTING_CUR

    }

    def "400 if quantity is greater than maximum allowed quantity for non-countable product"() {
        given: "a registered customer"
        authorizeTrustedClient(restClient)

        // creates new cart
        def createdCartId = createCart()

        when: "customer wants to add entry with quantity greater than maximum allowed quantity for non-countable product"
        // adds cart entry
        HttpResponseDecorator addEntryCartResponse1 = restClient.post(
                path: getBasePathWithSite() + '/users/' + CUSTOMER_USERNAME + '/carts/' + createdCartId + '/entries',
                body: [
                        "productCode"   : NON_COUNTABLE_PRODUCT_CODE,
                        "businessItemId": BUSINESS_ITEM_ID,
                        "businessKey"   : BUSINESS_KEY,
                        "quantity"      : NON_COUNTABLE_MAX_QUANTITY + 1,
                        "deliveryDate"  : DELIVERY_DATE_REQUEST
                ],
                contentType: JSON,
                requestContentType: JSON
        )

        then: "he retrieves 400 response"
        with(addEntryCartResponse1) {
            if (isNotEmpty(data) && isEmpty(data.errors)) println(data)

            deleteCart(createdCartId)

            status == SC_BAD_REQUEST
            data.errors.size() == 1
            data.errors[0].type == "CommerceCartModificationError"
            data.errors[0].message == "Product '" + NON_COUNTABLE_PRODUCT_CODE + "' cannot have quantity greater than " + NON_COUNTABLE_MAX_QUANTITY + "."
        }
    }

    def "201 if quantity is equal to maximum allowed quantity for non-countable product"() {
        given: "a registered customer"
        authorizeTrustedClient(restClient)

        // creates new cart
        def createdCartId = createCart()

        when: "customer wants to add entry with quantity equal to maximum allowed quantity for non-countable product"
        // adds cart entry
        HttpResponseDecorator addEntryCartResponse1 = restClient.post(
                path: getBasePathWithSite() + '/users/' + CUSTOMER_USERNAME + '/carts/' + createdCartId + '/entries',
                body: [
                        "productCode"   : NON_COUNTABLE_PRODUCT_CODE,
                        "businessItemId": BUSINESS_ITEM_ID,
                        "businessKey"   : BUSINESS_KEY,
                        "quantity"      : NON_COUNTABLE_MAX_QUANTITY,
                        "deliveryDate"  : DELIVERY_DATE_REQUEST
                ],
                contentType: JSON,
                requestContentType: JSON
        )

        then: "he retrieves 201 response"
        with(addEntryCartResponse1) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)

            deleteCart(createdCartId)

            status == SC_CREATED
        }
    }

    def "400 if no hybris prices were found for the product and no external prices provided"() {
        given: "a registered customer"
        authorizeTrustedClient(restClient)

        // creates new cart
        def createdCartId = createCart()

        when: "customer wants to add entry with product that has no price in hybris"
        // adds cart entry
        HttpResponseDecorator addEntryCartResponse1 = restClient.post(
                path: getBasePathWithSite() + '/users/' + CUSTOMER_USERNAME + '/carts/' + createdCartId + '/entries',
                body: [
                        "productCode"   : NO_PRICES_PRODUCT_CODE,
                        "businessItemId": BUSINESS_ITEM_ID,
                        "quantity"      : QUANTITY,
                        "deliveryDate"  : DELIVERY_DATE_REQUEST,
                        "externalPrices": [
                                // no USD external price
                                EXTERNAL_PRICE_EUR,
                                EXTERNAL_PRICE_GBP
                        ]
                ],
                contentType: JSON,
                requestContentType: JSON
        )

        then: "he retrieves 400 response"
        with(addEntryCartResponse1) {
            if (isNotEmpty(data) && isEmpty(data.errors)) println(data)

            deleteCart(createdCartId)

            status == SC_BAD_REQUEST
            data.errors.size() == 1
            data.errors[0].type == "IllegalStateError"
            data.errors[0].message =~ /Cart model .+ was not calculated due to: Cannot find price for currency USD/
        }
    }

    def "201 if no hybris prices were found for the product but external prices provided"() {
        given: "a registered customer"
        authorizeTrustedClient(restClient)

        // creates new cart
        def createdCartId = createCart()

        when: "customer wants to add entry with product that has no price in hybris"
        // adds cart entry
        HttpResponseDecorator addEntryCartResponse1 = restClient.post(
                path: getBasePathWithSite() + '/users/' + CUSTOMER_USERNAME + '/carts/' + createdCartId + '/entries',
                body: [
                        "productCode"   : NO_PRICES_PRODUCT_CODE,
                        "businessItemId": BUSINESS_ITEM_ID,
                        "businessKey"   : BUSINESS_KEY,
                        "quantity"      : NON_COUNTABLE_MAX_QUANTITY,
                        "deliveryDate"  : DELIVERY_DATE_REQUEST,
                        "externalPrices": [
                                // USD external price
                                EXTERNAL_PRICE_USD
                        ]
                ],
                contentType: JSON,
                requestContentType: JSON
        )

        then: "he retrieves 201 response"
        with(addEntryCartResponse1) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)

            deleteCart(createdCartId)

            status == SC_CREATED
        }
    }


    private String createCart() {
        return createCart(COUNTRY_USD)
    }

    private String createCart(String country) {
        HttpResponseDecorator createCartResponse = restClient.post(
                path: getBasePathWithSite() + '/users/' + CUSTOMER_USERNAME + '/carts',
                body: [
                        "type"   : CART_TYPE,
                        "country": country
                ],
                contentType: JSON,
                requestContentType: JSON
        )
        with(createCartResponse) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            status == SC_CREATED
            isNotEmpty(data.id)
        }
        return createCartResponse.data.id
    }

    private void deleteCart(String cartId) {

        HttpResponseDecorator deleteCartResponse = restClient.delete(
                path: getBasePathWithSite() + '/users/' + CUSTOMER_USERNAME + '/carts/' + cartId,
                headers: ["Fields-Set-Level": "FULL"],
                contentType: JSON,
                requestContentType: JSON
        )

    }

    @Override
    protected String getBaseSite() {
        return AS_SITE
    }
}

