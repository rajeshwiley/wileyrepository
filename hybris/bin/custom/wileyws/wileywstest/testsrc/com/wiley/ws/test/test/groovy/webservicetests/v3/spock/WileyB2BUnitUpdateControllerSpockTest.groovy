package com.wiley.ws.test.test.groovy.webservicetests.v3.spock

import de.hybris.bootstrap.annotations.ManualTest
import groovyx.net.http.HttpResponseDecorator
import spock.lang.Unroll

import static groovyx.net.http.ContentType.JSON


@ManualTest
@Unroll
class WileyB2BUnitUpdateControllerSpockTest extends AbstractSpockTransactionalTest{
    static final String SAP_ACCOUNT_NUMBER = "testSapAccountNumber"
    static final String TOO_LONG_SAP_ACCOUNT_NUMBER = "EXTERNAL_CODE".padLeft(257, "_");
    protected static final UPDATE_B2B_UNIT_REQUEST =  [
        "sapAccountNumber": "sapAccount_new2",
        "name": [[ "loc": "en", "val": "Services West - upd7" ] ],
        "salesRepContactName": "salesRepContactName333",
        "salesRepContactEmail": "salesRepContactEmail333",
        "salesRepContactPhone": "444",
        "soldToAddress": [
                "addressId":"test_new",
                "postcode":"post_777",
                "country":"US",
                "state":"VA",
                "city":"Krakow",
                "line1":"TestStr_222",
                "line2":"streetNumber1_222",
                "firstName":"firstName1_222",
                "lastName":"lastName222",
                "phoneNumber":"444"
        ],
        "shippingAddresses": [[
                                      "addressId":"test_333",
                                      "postcode":"post_111",
                                      "country":"US",
                                      "state":"VA",
                                      "city":"Krakow111",
                                      "line1":"TestStr_111",
                                      "line2":"streetNumber1_111",
                                      "firstName":"firstName1_111",
                                      "lastName":"lastName111",
                                      "phoneNumber":"222"
                              ]],
        "active": "true"
    ]
    protected static final INVALID_UPDATE_B2B_UNIT_REQUEST = [
            "sapAccountNumber": "sapAccount_new2",
            "name": [[ "loc": "en", "val": "Services West - upd7" ] ],
            "salesRepContactName": "salesRepContactName333",
            "salesRepContactEmail": "salesRepContactEmail333",
            "salesRepContactPhone": "444",
            "soldToAddress": [
                    "postcode":"post_777",
                    "country":"US",
                    "state":"VA",
                    "city":"Krakow",
                    "line1":"TestStr_222",
                    "line2":"streetNumber1_222",
                    "firstName":"firstName1_222",
                    "lastName":"lastName222",
                    "phoneNumber":"444"
            ],
            "shippingAddresses": [[
                                          "addressId":"test_333",
                                          "postcode":"post_111",
                                          "country":"US",
                                          "state":"VA",
                                          "city":"Krakow111",
                                          "line1":"TestStr_111",
                                          "line2":"streetNumber1_111",
                                          "firstName":"firstName1_111",
                                          "lastName":"lastName111",
                                          "phoneNumber":"222"
                                  ]],
            "active": "true"
    ]


    def "Trusted client requests B2BUnit update with too long SAP account number: "() {
        given: "trusted client"
        authorizeTrustedClient(restClient)

        when: "trusted client requests B2BUnit update"
        HttpResponseDecorator response = restClient.post(
                path: getBasePath() + "/b2b/accounts/" + TOO_LONG_SAP_ACCOUNT_NUMBER,
                contentType: responseFormat,
                body: postBody,
                requestContentType: requestFormat)

        then: "he receives 400 response"
        with(response) {
            if (isNotEmpty(data) && isEmpty(data.errors)) println(data)
            isNotEmpty(data.errors)
            data.errors[0].type == 'MethodConstraintViolationError'
            status == 400
        }

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | UPDATE_B2B_UNIT_REQUEST
    }

    def "Trusted client requests B2BUnit update with valid SAP account number and UPDATE_B2B_UNIT_REQUEST: "() {
        given: "trusted client"
        authorizeTrustedClient(restClient)

        when: "trusted client requests B2BUnit update"
        HttpResponseDecorator response = restClient.post(
                path: getBasePath() + "/b2b/accounts/" + SAP_ACCOUNT_NUMBER,
                contentType: responseFormat,
                body: postBody,
                requestContentType: requestFormat)

        then: "he receives 204 response"
        with(response) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            status == 204
            isEmpty(data)
        }

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | UPDATE_B2B_UNIT_REQUEST
    }

    def "Trusted client requests B2BUnit update with invalid UPDATE_B2B_UNIT_REQUEST: "() {
        given: "trusted client"
        authorizeTrustedClient(restClient)

        when: "trusted client requests B2BUnit update"
        HttpResponseDecorator response = restClient.post(
                path: getBasePath() + "/b2b/accounts/" + SAP_ACCOUNT_NUMBER,
                contentType: responseFormat,
                body: postBody,
                requestContentType: requestFormat)

        then: "he receives 400 response"
        with(response) {
            if (isNotEmpty(data) && isEmpty(data.errors)) println(data)
            isNotEmpty(data.errors)
            status == 400
        }

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | INVALID_UPDATE_B2B_UNIT_REQUEST
    }



}
