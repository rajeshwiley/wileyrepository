/**
 *
 */
package com.wiley.ws.test.test.groovy.webservicetests.v2.spock.carts

import com.wiley.ws.test.test.groovy.webservicetests.wiley.TestValidationUtils
import de.hybris.bootstrap.annotations.ManualTest
import groovy.json.JsonOutput
import groovyx.net.http.HttpResponseDecorator
import spock.lang.Unroll

import static groovyx.net.http.ContentType.JSON
import static org.apache.http.HttpStatus.*

/**
 *
 * `
 */
@Unroll
@ManualTest
class AsCartTest extends AbstractCartTest {
    public static final String CART_TYPE = "GENERAL"
    public static final CART_COUNTRY = "US"
    public static final CART_STATE = "WI"
    public static final WRONG_COUNTRY = "RU"
    public static final EXPECTED_CART_CURRENCY = "USD"
    public static final String NON_EXISTING_COUNTRY = "XX"
    public static final String NON_EXISTING_STATE = "YY"
    public static final String NON_EXISTING_CURRENCY = "TTT"

    public static final String NON_EXISTING_USER_ID = 'non-existing-userId'
    public static final String NON_EXISTING_CART_GUID = 'non-existing-guid'


    public static final boolean ENFORCE_SINGLE_CART_PER_TYPE = true;
    public static final boolean USER_AUTO_CREATION_STRATEGY = "ALM";

    public static final String CONTACT_POST_CODE = "contact post code"
    public static final String CONTACT_CITY = "contact city"
    public static final String CONTACT_LINE1 = "contact line1"
    public static final String CONTACT_LINE2 = "contact line2"
    public static final String CONTACT_ORGANIZATION = "contact organization"
    public static final String PAYMENT_POST_CODE = "payment post code"
    public static final String PAYMENT_CITY = "payment city"
    public static final String PAYMENT_LINE1 = "payment line1"
    public static final String PAYMENT_LINE2 = "payment line2"
    public static final String PAYMENT_ORGANIZATION = "payment organization"

    public static final CONTACT_ADDRESS = [
            "postcode"    : CONTACT_POST_CODE,
            "country"     : CART_COUNTRY,
            "state"       : CART_STATE,
            "city"        : CONTACT_CITY,
            "line1"       : CONTACT_LINE1,
            "line2"       : CONTACT_LINE2,
            "organization": CONTACT_ORGANIZATION
    ]
    public static final PAYMENT_ADDRESS = [
            "postcode"    : PAYMENT_POST_CODE,
            "country"     : CART_COUNTRY,
            "state"       : CART_STATE,
            "city"        : PAYMENT_CITY,
            "line1"       : PAYMENT_LINE1,
            "line2"       : PAYMENT_LINE2,
            "organization": PAYMENT_ORGANIZATION
    ]
    public static final CONTACT_ADDRESS_RESPONSE = [
            "postcode"    : CONTACT_POST_CODE,
            "country"     : CART_COUNTRY,
            "state"       : CART_STATE,
            "city"        : CONTACT_CITY,
            "line1"       : CONTACT_LINE1,
            "line2"       : CONTACT_LINE2,
            "organization": CONTACT_ORGANIZATION
    ]
    public static final PAYMENT_ADDRESS_RESPONSE = [
            "postcode"    : PAYMENT_POST_CODE,
            "country"     : CART_COUNTRY,
            "state"       : CART_STATE,
            "city"        : PAYMENT_CITY,
            "line1"       : PAYMENT_LINE1,
            "line2"       : PAYMENT_LINE2,
            "organization": PAYMENT_ORGANIZATION
    ]
    public static final VALID_BODY = [
            "type"          : CART_TYPE,
            "country"       : CART_COUNTRY,
            "contactAddress": CONTACT_ADDRESS,
            "paymentAddress": PAYMENT_ADDRESS
    ]

    def "Registered customer creates cart of type: #cartType"() {
        given: "a registered customer"
        authorizeTrustedClient(restClient)

        when: "customer wants to create cart"
        HttpResponseDecorator response = restClient.post(
                path: getBasePathWithSite() + '/users/' + CUSTOMER_USERNAME + '/carts',
                body: [
                        "type"          : cartType,
                        "country"       : CART_COUNTRY,
                        "contactAddress": CONTACT_ADDRESS,
                        "paymentAddress": PAYMENT_ADDRESS
                ],
                contentType: JSON,
                requestContentType: JSON
        )

        then: "he retrieves his cart"
        with(response) {
            if (isNotEmpty(data) && isEmpty(data.errors)) println(data)
            status == SC_CREATED
            isNotEmpty(data.id)
            isNotEmpty(data.code)
            data.type == cartType
            data.country == CART_COUNTRY
            data.currency == EXPECTED_CART_CURRENCY
            data.userId == CUSTOMER_USERNAME
//            no entries during cart creation
            isEmpty(data.entries)
//            contactAddress, paymentAddress and institution should NOT be populated for DEFAULT fields set level
            isEmpty(data.contactAddress)
            isEmpty(data.paymentAddress)
        }

        where:
        cartType << ["GENERAL", "AS_QUOTED", "AS_FUNDED"]
    }

    def "Registered customer prohibited to create two carts of type: #cartType"() {
        given: "a registered customer"
        authorizeTrustedClient(restClient)

        when: "customer wants to create two carts"
        HttpResponseDecorator responseWithFirstCart = restClient.post(
                path: getBasePathWithSite() + '/users/' + CUSTOMER_USERNAME + '/carts',
                body: [
                        "type"          : cartType,
                        "country"       : CART_COUNTRY,
                        "contactAddress": CONTACT_ADDRESS,
                        "paymentAddress": PAYMENT_ADDRESS
                ],
                headers: ['Fields-Set-Level': 'BASIC', 'Enforce-Single-Cart-Per-Type' : 'true'],
                contentType: JSON,
                requestContentType: JSON
        )

        HttpResponseDecorator responseWithConflictCart = restClient.post(
                path: getBasePathWithSite() + '/users/' + CUSTOMER_USERNAME + '/carts',
                body: [
                        "type"          : cartType,
                        "country"       : CART_COUNTRY,
                        "contactAddress": CONTACT_ADDRESS,
                        "paymentAddress": PAYMENT_ADDRESS
                ],
                headers: ['Fields-Set-Level': 'BASIC', 'Enforce-Single-Cart-Per-Type' : 'true'],
                contentType: JSON,
                requestContentType: JSON
        )


        then: "he retrieved the same cart"

        String firstCartCode = responseWithFirstCart.data.code
        with(responseWithFirstCart) {
            if (isNotEmpty(data) && isEmpty(data.errors)) println(data)
            isNotEmpty(data.code)
        }

        with(responseWithConflictCart) {
            if (isNotEmpty(data) && isEmpty(data.errors)) println(data)
            status == SC_CONFLICT
            data.code == firstCartCode
            isNotEmpty(data.code)
            data.type == cartType
        }

        where:
        cartType << ["GENERAL", "AS_QUOTED", "AS_FUNDED"]
    }

    def "Registered customer creates cart with BASIC level"() {
        given: "a registered customer"
        authorizeTrustedClient(restClient)

        when: "customer wants to create cart"

        HttpResponseDecorator response = restClient.post(
                path: getBasePathWithSite() + '/users/' + CUSTOMER_USERNAME + '/carts',
                body: VALID_BODY,
                headers: ['Fields-Set-Level': 'BASIC'],
                contentType: JSON,
                requestContentType: JSON
        )

        then: "he retrieves his cart"
        with(response) {
            if (isNotEmpty(data) && isEmpty(data.errors)) println(data)
            status == SC_CREATED
            isNotEmpty(data.id)
            isNotEmpty(data.code)
            data.type == CART_TYPE
//            country, currency should NOT be populated for BASIC fields set level
            isEmpty(data.country)
            isEmpty(data.currency)
            data.userId == CUSTOMER_USERNAME
//            no entries during cart creation
            isEmpty(data.entries)
//            contactAddress, paymentAddress and institution should NOT be populated for BASIC fields set level
            isEmpty(data.contactAddress)
            isEmpty(data.paymentAddress)
        }
    }


    def "Registered customer creates cart with FULL level"() {
        given: "a registered customer"
        authorizeTrustedClient(restClient)

        when: "customer wants to create cart"
        HttpResponseDecorator response = restClient.post(
                path: getBasePathWithSite() + '/users/' + CUSTOMER_USERNAME + '/carts',
                body: VALID_BODY,
                headers: ['Fields-Set-Level': 'FULL'],
                contentType: JSON,
                requestContentType: JSON
        )

        then: "he retrieves his cart"
        with(response) {
            if (isNotEmpty(data) && isEmpty(data.errors)) println(data)
            status == SC_CREATED
            isNotEmpty(data.id)
            isNotEmpty(data.code)
            data.type == CART_TYPE
//            country, currency should NOT be populated for BASIC fields set level
            data.country == CART_COUNTRY
            data.currency == EXPECTED_CART_CURRENCY
            data.userId == CUSTOMER_USERNAME
//            no entries during cart creation
            isEmpty(data.entries)
//            contactAddress, paymentAddress and institution should NOT be populated for BASIC fields set level
            data.contactAddress == CONTACT_ADDRESS_RESPONSE
            data.paymentAddress == PAYMENT_ADDRESS_RESPONSE
        }
    }

    def "Registered customer creates cart with invalid JSON"() {
        given: "a registered customer"
        authorizeTrustedClient(restClient)

        when: "customer wants to create cart with invalid JSON"
        String invalidJson = JsonOutput.toJson(VALID_BODY).replaceFirst(/\}/, ']')


        HttpResponseDecorator response = restClient.post(
                path: getBasePathWithSite() + '/users/' + CUSTOMER_USERNAME + '/carts',
                body: invalidJson,
                contentType: JSON,
                requestContentType: JSON
        )

        then: "he retrieves expected error"
        with(response) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            status == SC_BAD_REQUEST
            isNotEmpty(data.errors)
            data.errors[0]["type"] == "HttpMessageNotReadableError"
            data.errors[0]["message"].startsWith("Could not read document: Unexpected close marker")
        }
    }

    def "Registered customer creates cart with non-existing user"() {
        given: "a registered customer"
        authorizeTrustedClient(restClient)

        when: "customer wants to create cart with invalid JSON"
        HttpResponseDecorator response = restClient.post(
                path: getBasePathWithSite() + '/users/' + NON_EXISTING_USER_ID + '/carts',
                body: VALID_BODY,
                contentType: JSON,
                requestContentType: JSON
        )

        then: "he retrieves expected error"
        with(response) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            status == SC_NOT_FOUND
            isNotEmpty(data.errors)
            data.errors[0]["type"] == "UnknownResourceError"
            data.errors[0]["message"] == ("Cannot find user with uid '" + NON_EXISTING_USER_ID + "'")
        }
    }

    def "Registered customer creates cart with country field of the cart did not match the country of the payment address"() {
        given: "a registered customer"
        authorizeTrustedClient(restClient)

        when: "customer wants to create cart with invalid payment address country"
        final PAYMENT_ADDRESS_WRONG_COUNTRY = [
                "postcode"    : PAYMENT_POST_CODE,
                "country"     : WRONG_COUNTRY,
                "city"        : PAYMENT_CITY,
                "line1"       : PAYMENT_LINE1,
                "line2"       : PAYMENT_LINE2,
                "organization": PAYMENT_ORGANIZATION
        ]
        HttpResponseDecorator response = restClient.post(
                path: getBasePathWithSite() + '/users/' + CUSTOMER_USERNAME + '/carts',
                body: [
                        "type"          : CART_TYPE,
                        "country"       : CART_COUNTRY,
                        "contactAddress": CONTACT_ADDRESS,
                        "paymentAddress": PAYMENT_ADDRESS_WRONG_COUNTRY
                ],
                contentType: JSON,
                requestContentType: JSON
        )

        then: "he retrieves expected error"
        with(response) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            status == SC_UNPROCESSABLE_ENTITY
            isNotEmpty(data.errors)
            data.errors[0]["type"] == "ValidationError"
            data.errors[0]["message"] == "The country of the billing address must match the country field set for the cart."
            data.errors[0]["reason"] == "invalid"
            data.errors[0]["subject"] == "country"
            data.errors[0]["subjectType"] == "parameter"
        }
    }

    def "Registered customer creates cart with non-existing country field of the contact address"() {
        given: "a registered customer"
        authorizeTrustedClient(restClient)

        when: "customer wants to create cart with invalid contact address state"
        final CONTACT_ADDRESS_NON_EXISTING_COUNTRY = [
                "postcode"    : CONTACT_POST_CODE,
                "country"     : NON_EXISTING_COUNTRY,
                "state"       : CART_STATE,
                "city"        : CONTACT_CITY,
                "line1"       : CONTACT_LINE1,
                "line2"       : CONTACT_LINE2,
                "organization": CONTACT_ORGANIZATION
        ]
        HttpResponseDecorator response = restClient.post(
                path: getBasePathWithSite() + '/users/' + CUSTOMER_USERNAME + '/carts',
                body: [
                        "type"          : CART_TYPE,
                        "country"       : CART_COUNTRY,
                        "contactAddress": CONTACT_ADDRESS_NON_EXISTING_COUNTRY
                ],
                contentType: JSON,
                requestContentType: JSON
        )

        then: "he retrieves expected error"
        with(response) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            status == SC_BAD_REQUEST
            isNotEmpty(data.errors)
            data.errors[0]["type"] == "UnknownCountryError"
            data.errors[0]["message"] == "CountryModel with isocode '" + NON_EXISTING_COUNTRY + "' not found!"
            data.errors[0]["subject"] == "contactAddress.country"
        }
    }

    def "Registered customer creates cart with non-existing region field of the contact address"() {
        given: "a registered customer"
        authorizeTrustedClient(restClient)

        when: "customer wants to create cart with invalid contact address state"
        final CONTACT_ADDRESS_NON_EXISTING_REGIONY = [
                "postcode"    : CONTACT_POST_CODE,
                "country"     : CART_COUNTRY,
                "state"       : NON_EXISTING_STATE,
                "city"        : CONTACT_CITY,
                "line1"       : CONTACT_LINE1,
                "line2"       : CONTACT_LINE2,
                "organization": CONTACT_ORGANIZATION
        ]
        HttpResponseDecorator response = restClient.post(
                path: getBasePathWithSite() + '/users/' + CUSTOMER_USERNAME + '/carts',
                body: [
                        "type"          : CART_TYPE,
                        "country"       : CART_COUNTRY,
                        "contactAddress": CONTACT_ADDRESS_NON_EXISTING_REGIONY
                ],
                contentType: JSON,
                requestContentType: JSON
        )

        then: "he retrieves expected error"
        with(response) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            status == SC_BAD_REQUEST
            isNotEmpty(data.errors)
            data.errors[0]["type"] == "UnknownRegionError"
            data.errors[0]["message"] == "RegionModel with country, isocode '" + CART_COUNTRY + ", " + CART_COUNTRY + "-" + NON_EXISTING_STATE + "' not found!"
            data.errors[0]["subject"] == "contactAddress.state"
        }
    }

    def "Registered customer creates cart with non-existing currency field of the contact address"() {
        given: "a registered customer"
        authorizeTrustedClient(restClient)

        when: "customer wants to create cart with invalid contact address state"
        final CONTACT_ADDRESS = [
                "postcode"    : CONTACT_POST_CODE,
                "country"     : CART_COUNTRY,
                "state"       : CART_STATE,
                "city"        : CONTACT_CITY,
                "line1"       : CONTACT_LINE1,
                "line2"       : CONTACT_LINE2,
                "organization": CONTACT_ORGANIZATION
        ]
        HttpResponseDecorator response = restClient.post(
                path: getBasePathWithSite() + '/users/' + CUSTOMER_USERNAME + '/carts',
                body: [
                        "type"          : CART_TYPE,
                        "country"       : CART_COUNTRY,
                        "currency"      : NON_EXISTING_CURRENCY,
                        "contactAddress": CONTACT_ADDRESS
                ],
                contentType: JSON,
                requestContentType: JSON
        )

        then: "he retrieves expected error"
        with(response) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            status == SC_BAD_REQUEST
            isNotEmpty(data.errors)
            data.errors[0]["type"] == "UnknownIdentifierError"
            data.errors[0]["message"] == "CurrencyModel with isocode '" + NON_EXISTING_CURRENCY + "' not found!"
            data.errors[0]["subject"] == "currency"
        }
    }

    def "Registered customer creates cart with empty country field"() {
        given: "a registered customer"
        authorizeTrustedClient(restClient)

        when: "customer wants to create cart with empty country field of contact address and payment address"
        final CONTACT_ADDRESS_EMPTY_COUNTRY = [
                "postcode": CONTACT_POST_CODE
        ]
        final PAYMENT_ADDRESS_EMPTY_COUNTRY = [
                "postcode": PAYMENT_POST_CODE
        ]
        HttpResponseDecorator response = restClient.post(
                path: getBasePathWithSite() + '/users/' + CUSTOMER_USERNAME + '/carts',
                body: [
                        "type"          : CART_TYPE,
                        "country"       : CART_COUNTRY,
                        "contactAddress": CONTACT_ADDRESS_EMPTY_COUNTRY,
                        "paymentAddress": PAYMENT_ADDRESS_EMPTY_COUNTRY
                ],
                contentType: JSON,
                requestContentType: JSON
        )

        then: "he retrieves expected error"
        with(response) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            status == SC_BAD_REQUEST
            isNotEmpty(data.errors)
            data.errors.size() == 2

            TestValidationUtils.validateRequiredAttributesErrors(["contactAddress.country", "paymentAddress.country"], data.errors)

        }
    }

    def "Registered customer creates cart with non-2-letter state codes: #countryCode-#stateCode"() {
        given: "a registered customer"
        authorizeTrustedClient(restClient)

        when: "customer wants to create cart with non-2-letter contact address state"
        final CONTACT_ADDRESS_NON_ISO_STATE = [
                "country": countryCode,
                "state"  : stateCode
        ]
        HttpResponseDecorator response = restClient.post(
                path: getBasePathWithSite() + '/users/' + CUSTOMER_USERNAME + '/carts',
                body: [
                        "type"          : CART_TYPE,
                        "country"       : CART_COUNTRY,
                        "contactAddress": CONTACT_ADDRESS_NON_ISO_STATE
                ],
                headers: ['Fields-Set-Level': 'FULL'],
                contentType: JSON,
                requestContentType: JSON
        )

        then: "he retrieves his cart"
        with(response) {
            if (isNotEmpty(data) && isEmpty(data.errors)) println(data)
            status == SC_CREATED
            data.contactAddress == CONTACT_ADDRESS_NON_ISO_STATE
        }

        where:
        countryCode | stateCode
        "JP"        | "13"
        "AR"        | "C"
        "MX"        | "AGU"
    }

    def "Registered customer creates cart with non-existing state field of the contact address"() {
        given: "a registered customer"
        authorizeTrustedClient(restClient)

        when: "customer wants to create cart with invalid contact address state"
        final CONTACT_ADDRESS_NON_EXISTING_STATE = [
                "postcode"    : CONTACT_POST_CODE,
                "country"     : CART_COUNTRY,
                "state"       : NON_EXISTING_STATE,
                "city"        : CONTACT_CITY,
                "line1"       : CONTACT_LINE1,
                "line2"       : CONTACT_LINE2,
                "organization": CONTACT_ORGANIZATION
        ]
        HttpResponseDecorator response = restClient.post(
                path: getBasePathWithSite() + '/users/' + CUSTOMER_USERNAME + '/carts',
                body: [
                        "type"          : CART_TYPE,
                        "country"       : CART_COUNTRY,
                        "contactAddress": CONTACT_ADDRESS_NON_EXISTING_STATE
                ],
                contentType: JSON,
                requestContentType: JSON
        )

        then: "he retrieves expected error"
        with(response) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            status == SC_BAD_REQUEST
            isNotEmpty(data.errors)
            data.errors[0]["type"] == "UnknownRegionError"
            data.errors[0]["message"] == "RegionModel with country, isocode '" + CART_COUNTRY + ", " + CART_COUNTRY + "-" + NON_EXISTING_STATE + "' not found!"
        }
    }

    def "Registered customer creates cart with country field of the cart did not match the country of the contact address"() {
        given: "a registered customer"
        authorizeTrustedClient(restClient)

        when: "customer wants to create cart with invalid contact address country"
        final CONTACT_ADDRESS_WRONG_COUNTRY = [
                "postcode"    : CONTACT_POST_CODE,
                "country"     : WRONG_COUNTRY,
                "city"        : CONTACT_CITY,
                "line1"       : CONTACT_LINE1,
                "line2"       : CONTACT_LINE2,
                "organization": CONTACT_ORGANIZATION
        ]
        HttpResponseDecorator response = restClient.post(
                path: getBasePathWithSite() + '/users/' + CUSTOMER_USERNAME + '/carts',
                body: [
                        "type"          : CART_TYPE,
                        "country"       : CART_COUNTRY,
                        "contactAddress": CONTACT_ADDRESS_WRONG_COUNTRY,
                        "paymentAddress": PAYMENT_ADDRESS
                ],
                contentType: JSON,
                requestContentType: JSON
        )

        then: "cart is successfully craeted"
        with(response) {
            if (isNotEmpty(data) && isEmpty(data.errors)) println(data)
            status == SC_CREATED
        }
    }

    def "Registered customer creates cart with no payment and contact address"() {
        given: "a registered customer"
        authorizeTrustedClient(restClient)

        when: "customer wants to create cart without payment and contact addresses"
        HttpResponseDecorator response = restClient.post(
                path: getBasePathWithSite() + '/users/' + CUSTOMER_USERNAME + '/carts',
                body: [
                        "type"   : CART_TYPE,
                        "country": CART_COUNTRY
                ],
                contentType: JSON,
                requestContentType: JSON
        )

        then: "cart is successfully craeted"
        with(response) {
            if (isNotEmpty(data) && isEmpty(data.errors)) println(data)
            status == SC_CREATED
        }
    }

    def "Registered customer requests cart state with BASIC level"() {
        given: "a registered customer has cart"
        authorizeTrustedClient(restClient)

        HttpResponseDecorator cartCreationResponse = restClient.post(
                path: getBasePathWithSite() + '/users/' + CUSTOMER_USERNAME + '/carts',
                body: VALID_BODY,
                contentType: JSON,
                requestContentType: JSON
        )

        when: "customer wants to requests his cart state"
        HttpResponseDecorator cartStateRequestResponse = restClient.get(
                path: getBasePathWithSite() + '/users/' + CUSTOMER_USERNAME + '/carts/' + cartCreationResponse.data.id,
                query: ["fieldsSetLevel": 'BASIC'],
                contentType: JSON,
                requestContentType: JSON
        )

        then: "he retrieves his cart with BASIC level"
        with(cartStateRequestResponse) {
            if (isNotEmpty(data) && isEmpty(data.errors)) println(data)
            status == SC_OK
            data.size() == 5
            data.id == cartCreationResponse.data.id
            data.code == cartCreationResponse.data.code
            data.type == cartCreationResponse.data.type
            data.userId == cartCreationResponse.data.userId
            isEmpty(data.country)
            isEmpty(data.currency)
        }
    }

    def "Registered customer requests cart state with DEFAULT level"() {
        given: "a registered customer has cart"
        authorizeTrustedClient(restClient)

        HttpResponseDecorator cartCreationResponse = restClient.post(
                path: getBasePathWithSite() + '/users/' + CUSTOMER_USERNAME + '/carts',
                body: VALID_BODY,
                contentType: JSON,
                requestContentType: JSON
        )

        when: "customer wants to requests his cart state"
        HttpResponseDecorator cartStateRequestResponse = restClient.get(
                path: getBasePathWithSite() + '/users/' + CUSTOMER_USERNAME + '/carts/' + cartCreationResponse.data.id,
                contentType: JSON,
                requestContentType: JSON
        )

        then: "he retrieves his cart with DEFAULT level"
        with(cartStateRequestResponse) {
            if (isNotEmpty(data) && isEmpty(data.errors)) println(data)
            status == SC_OK
            data.size() > 5 && data.size() <= 9
            data.id == cartCreationResponse.data.id
            data.code == cartCreationResponse.data.code
            data.type == cartCreationResponse.data.type
            data.userId == cartCreationResponse.data.userId
            data.country == cartCreationResponse.data.country
            data.currency == cartCreationResponse.data.currency
        }
    }

    def "Registered customer requests cart state with FULL level"() {
        given: "a registered customer has cart"
        authorizeTrustedClient(restClient)

        HttpResponseDecorator cartCreationResponse = restClient.post(
                path: getBasePathWithSite() + '/users/' + CUSTOMER_USERNAME + '/carts',
                body: VALID_BODY,
                contentType: JSON,
                requestContentType: JSON
        )

        when: "customer wants to requests his cart state"
        HttpResponseDecorator cartStateRequestResponse = restClient.get(
                path: getBasePathWithSite() + '/users/' + CUSTOMER_USERNAME + '/carts/' + cartCreationResponse.data.id,
                query: ["fieldsSetLevel": 'FULL'],
                contentType: JSON,
                requestContentType: JSON
        )

        then: "he retrieves his cart with FULL level"
        with(cartStateRequestResponse) {
            if (isNotEmpty(data) && isEmpty(data.errors)) println(data)
            status == SC_OK
            data.size() >= 10
            data.id == cartCreationResponse.data.id
            data.code == cartCreationResponse.data.code
            data.type == cartCreationResponse.data.type
            data.userId == cartCreationResponse.data.userId
            data.country == cartCreationResponse.data.country
            data.currency == cartCreationResponse.data.currency
            data.subtotal == 0
            data.totalTax == 0
        }
    }

    def "Registered customer requests cart state with INCORRECT level"() {
        given: "a registered customer has cart"
        authorizeTrustedClient(restClient)
        String incorrect_level = "INCORRECT"

        HttpResponseDecorator cartCreationResponse = restClient.post(
                path: getBasePathWithSite() + '/users/' + CUSTOMER_USERNAME + '/carts',
                body: VALID_BODY,
                contentType: JSON,
                requestContentType: JSON
        )

        when: "customer wants to requests his cart state with incorrect level"
        HttpResponseDecorator cartStateRequestResponse = restClient.get(
                path: getBasePathWithSite() + '/users/' + CUSTOMER_USERNAME + '/carts/' + cartCreationResponse.data.id,
                query: ["fieldsSetLevel": incorrect_level],
                contentType: JSON,
                requestContentType: JSON
        )

        then: "he retrieves 400 error"
        with(cartStateRequestResponse) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            status == SC_BAD_REQUEST
            data.errors[0].type == "ConversionError"
            data.errors[0].message == ("Incorrect field:'" + incorrect_level + "'")
        }
    }

    def "Registered customer requests cart state for non-existing cart"() {
        given: "a registered customer doesn't have cart"
        authorizeTrustedClient(restClient)

        when: "customer wants to requests non-existing cart cart state"
        HttpResponseDecorator cartStateRequestResponse = restClient.get(
                path: getBasePathWithSite() + '/users/' + CUSTOMER_USERNAME + '/carts/' + NON_EXISTING_CART_GUID,
                contentType: JSON,
                requestContentType: JSON
        )

        then: "he retrieves 404 error"
        with(cartStateRequestResponse) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            status == SC_NOT_FOUND
            data.errors[0].type == "UnknownResourceError"
            data.errors[0].message == ("Cannot find cart with guid '" + NON_EXISTING_CART_GUID + "'")
        }
    }

    def "Unauthorized customer requests cart state"() {
        when: "Unauthorized customer wants to requests cart state"
        HttpResponseDecorator cartStateRequestResponse = restClient.get(
                path: getBasePathWithSite() + '/users/' + CUSTOMER_USERNAME + '/carts/' + NON_EXISTING_CART_GUID,
                contentType: JSON,
                requestContentType: JSON
        )

        then: "he retrieves 401 error"
        with(cartStateRequestResponse) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            status == SC_UNAUTHORIZED
            data.errors[0].type == "UnauthorizedError"
        }
    }

    def "External system deletes cart"() {
        when: "External service want to delete cart"
        authorizeTrustedClient(restClient)

        HttpResponseDecorator cartCreationResponse = restClient.post(
                path: getBasePathWithSite() + '/users/' + CUSTOMER_USERNAME + '/carts',
                body: VALID_BODY,
                contentType: JSON,
                requestContentType: JSON
        )

        HttpResponseDecorator cartDeleteRequestResponse = restClient.delete(
                path: getBasePathWithSite() + '/users/' + CUSTOMER_USERNAME + '/carts/' + cartCreationResponse.data.id,
                contentType: JSON,
                requestContentType: JSON
        )

        then: "Receives 204 error code without response body"
        with(cartDeleteRequestResponse) {
            if (isEmpty(data)) println(data)
            status == SC_NO_CONTENT
            data == null
        }
    }

    def "External system deletes non-existing cart"() {
        when: "External service want to delete non-existing cart"
        authorizeTrustedClient(restClient)

        HttpResponseDecorator cartDeleteRequestResponse = restClient.delete(
                path: getBasePathWithSite() + '/users/' + CUSTOMER_USERNAME + '/carts/' + NON_EXISTING_CART_GUID,
                contentType: JSON,
                requestContentType: JSON
        )

        then: "Receives 404 error code with message"
        with(cartDeleteRequestResponse) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            status == SC_NOT_FOUND
            data.errors[0].type == "UnknownResourceError"
            data.errors[0].message == ("Cannot find cart with guid '" + NON_EXISTING_CART_GUID + "'")
        }
    }

    def "External system deletes cart for non-existing user"() {
        when: "External service want to delete cart for non-existing user"
        authorizeTrustedClient(restClient)

        HttpResponseDecorator cartDeleteRequestResponse = restClient.delete(
                path: getBasePathWithSite() + '/users/' + NON_EXISTING_USER_ID + '/carts/' + NON_EXISTING_CART_GUID,
                contentType: JSON,
                requestContentType: JSON
        )

        then: "Receives 404 error code with message"
        with(cartDeleteRequestResponse) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            status == SC_NOT_FOUND
            isNotEmpty(data.errors)
            data.errors[0]["type"] == "UnknownResourceError"
            data.errors[0]["message"] == ("Cannot find user with uid '" + NON_EXISTING_USER_ID + "'")
        }
    }

    def "External system deletes cart that does not belong to the specified user"() {
        when: "External service want to delete cart that does not belong to the specified user"
        authorizeTrustedClient(restClient)

        HttpResponseDecorator cartCreationResponse = restClient.post(
                path: getBasePathWithSite() + '/users/' + CUSTOMER_USERNAME + '/carts',
                body: VALID_BODY,
                contentType: JSON,
                requestContentType: JSON
        )

        HttpResponseDecorator cartDeleteRequestResponse = restClient.delete(
                path: getBasePathWithSite() + '/users/' + CUSTOMER_USERNAME_2 + '/carts/' + cartCreationResponse.data.id,
                contentType: JSON,
                requestContentType: JSON
        )

        then: "Receives 400 error code with message"
        with(cartDeleteRequestResponse) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            status == SC_BAD_REQUEST
            isNotEmpty(data.errors)
            data.errors[0]["type"] == "BadRequestError"
            data.errors[0]["message"] == ("Cart with guid '" + cartCreationResponse.data.id + "' was found, but does not belong to the specified customer")
        }
    }
}

