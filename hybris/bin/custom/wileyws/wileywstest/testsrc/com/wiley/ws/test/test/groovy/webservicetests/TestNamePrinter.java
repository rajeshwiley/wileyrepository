/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package com.wiley.ws.test.test.groovy.webservicetests;

import java.io.PrintStream;

import org.junit.rules.TestWatcher;
import org.junit.runner.Description;


/**
 * The type Test name printer.
 */
public class TestNamePrinter extends TestWatcher
{
	private final PrintStream ps;
	private static final String FORMAT = "Test: %s.%s\n";

	/**
	 * Instantiates a new Test name printer.
	 *
	 * @param ps
	 * 		the ps
	 */
	public TestNamePrinter(final PrintStream ps)
	{
		this.ps = ps;
	}

	/**
	 * Starting.
	 *
	 * @param description
	 * 		the description
	 */
	@Override
	protected void starting(final Description description)
	{
		ps.printf(FORMAT, description.getClassName(), description.getMethodName());
	}


	/**
	 * Succeeded.
	 *
	 * @param description
	 * 		the description
	 */
	@Override
	protected void succeeded(final Description description)
	{
		System.out.println("SUCCEEDED");
	}

	/**
	 * Failed.
	 *
	 * @param e
	 * 		the e
	 * @param description
	 * 		the description
	 */
	@Override
	protected void failed(final Throwable e, final Description description)
	{
		System.out.println("FAILED");
		e.printStackTrace(ps);
	}
}
