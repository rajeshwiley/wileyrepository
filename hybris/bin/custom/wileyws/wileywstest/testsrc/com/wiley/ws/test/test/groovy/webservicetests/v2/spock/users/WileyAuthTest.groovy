package com.wiley.ws.test.test.groovy.webservicetests.v2.spock.users

import static groovyx.net.http.ContentType.*
import static org.apache.http.HttpStatus.*

import de.hybris.bootstrap.annotations.ManualTest

import spock.lang.Unroll
import groovyx.net.http.HttpResponseDecorator


@ManualTest
@Unroll
class WileyAuthTest extends AbstractUserTest {

    protected static final String CUSTOMER_PASSWORD_RIGHT = 'PAss1234!'
    protected static final String CUSTOMER_PASSWORD_WRONG = 'PAss12345'
    protected static final String TEST_USER_1 = 'user1'
    protected static final String TEST_USER_2 = 'user2'
    protected static final String TEST_SUBSCRIPYION_CODE_1 = 'testSubscriptionCode1'

    def "For entitlement system to give username_password and to receive from hybris valid when request: #requestFormat and response: #responseFormat"() {
        given: "user exists in the system"
        authorizeAgsClient(restClient)

        when: "Entitlement system  gives username_password as input "
        HttpResponseDecorator response = restClient.post(
                path: getBasePathWithSite() + '/users/' + TEST_USER_1 + "/auth",
                body: postBody,
                contentType: responseFormat,
                requestContentType: requestFormat)

        then: "and  entitlement system receives from hybris valid/not valid output"
        with(response) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            status == SC_OK
            data.passwordValid == true

        }

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | "{\"password\": \"${CUSTOMER_PASSWORD_RIGHT}\"}"
        XML           | XML            | "<user><password>${CUSTOMER_PASSWORD_RIGHT}</password></user>"

    }

    def "For entitlement system to give username_password and to receive from hybris not valid when request: #requestFormat and response: #responseFormat"() {
        given: "user exists in the system"
        authorizeAgsClient(restClient)



        when: "Entitlement system  gives username/password as input "
        HttpResponseDecorator response = restClient.post(
                path: getBasePathWithSite() + '/users/' + TEST_USER_1 + "/auth",
                body: postBody,
                contentType: responseFormat,
                requestContentType: requestFormat)

        then: "and  entitlement system receives from hybris valid/not valid output"
        with(response) {
            status == SC_OK
            data.passwordValid == false

        }

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | "{\"password\": \"${CUSTOMER_PASSWORD_WRONG}\"}"
        XML           | XML            | "<user><password>${CUSTOMER_PASSWORD_WRONG}</password></user>"

    }

    def "No existing user when request: #requestFormat and response: #responseFormat"() {
        given: "user exists in the system"
        authorizeAgsClient(restClient)

        when: "Entitlement system  gives username/password as input "
        HttpResponseDecorator response = restClient.post(
                path: getBasePathWithSite() + '/users/' + 'notExisting@email.com' + "/auth",
                body: postBody,
                contentType: responseFormat,
                requestContentType: requestFormat)

        then: "and  entitlement system receives from hybris valid/not valid output"
        with(response) {
            status == SC_BAD_REQUEST
            data.errors.size() > 0
            data.errors[0].type == 'UnknownIdentifierError'
            data.errors[0].message == 'Cannot find user with uid \'notExisting@email.com\''

        }

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | "{\"password\": \"${CUSTOMER_PASSWORD_WRONG}\"}"
        XML           | XML            | "<user><password>${CUSTOMER_PASSWORD_WRONG}</password></user>"

    }

    def "Get active subscriptions when request: #requestFormat and response: #responseFormat"() {
        given: "user with subscription exists in the system"
        authorizeAgsClient(restClient)

        when: "Entitlement system  gives username/password as input "
        HttpResponseDecorator response = restClient.post(
                path: getBasePathWithSite() + '/users/' + TEST_USER_1 + "/auth/subscription",
                body: postBody,
                contentType: responseFormat,
                requestContentType: requestFormat)

        then: "and  entitlement system receives from hybris subscription info"
        with(response) {

            if (isNotEmpty(data)) {
                println(data)
                status == SC_OK
                data.passwordValid == true
                isNotEmpty(data.subscriptions)



                data.subscriptions.size() > 0
                data.subscriptions[0].code == TEST_SUBSCRIPYION_CODE_1
                data.subscriptions[0].productCode == 'ags_legacy_1'
                String dateExpected = new String("2017-04-03");

                data.subscriptions[0].expires != null
                data.subscriptions[0].expires.toString().contains(dateExpected)
            }

        }

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | "{\"password\": \"${CUSTOMER_PASSWORD_RIGHT}\"}"
        XML           | XML            | "<user><password>${CUSTOMER_PASSWORD_RIGHT}</password></user>"

    }


    def "Get active subscriptions for user with no active subscription when request: #requestFormat and response: #responseFormat"() {
        given: "user with no subscription in the system"
        authorizeAgsClient(restClient)


        when: "Entitlement system  gives username/password as input "
        HttpResponseDecorator response = restClient.post(
                path: getBasePathWithSite() + '/users/' + TEST_USER_2 + "/auth/subscription",
                body: postBody,
                contentType: responseFormat,
                requestContentType: requestFormat)

        then: "and  entitlement system receives from hybris empty subscription info"
        with(response) {
            data.passwordValid == true
            if (isNotEmpty(data)) {
                println(data)
                status == SC_OK
                isEmpty(data.subscriptions)

            }
        }

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | "{\"password\": \"${CUSTOMER_PASSWORD_RIGHT}\"}"
        XML           | XML            | "<user><password>${CUSTOMER_PASSWORD_RIGHT}</password></user>"

    }


}
