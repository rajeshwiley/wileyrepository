/**
 *
 */
package com.wiley.ws.test.test.groovy.webservicetests.v2.spock.ui.pages

import com.wiley.ws.test.test.groovy.webservicetests.v2.spock.AbstractGUITest
import geb.Page
import org.apache.log4j.Logger

class OauthAllowRequestPage extends Page {

    final static Logger LOGGER = Logger.getLogger(OauthAllowRequestPage.class.getName());

    String convertToPath(Object[] args) {
        "?client_id=" + args[0] + "&response_type=code&redirect_uri=" + args[1]
    }

    static url = AbstractGUITest.getHttpsWebroot() + "/oauth/authorize"
    static at = {
        LOGGER.debug("at " + OauthAllowRequestPage.class.getName());
        waitFor { title.contains("Access Confirmation") }
    }
    static content = {
        denyButton { $("input", name: "deny") }
        authorizeButton { $("input", name: "authorize") }
    }

    void authorize() {
        waitFor { authorizeButton.displayed }
        authorizeButton.click()
    }

    void deny() {
        waitFor { denyButton.displayed }
        denyButton.click()
    }
}
