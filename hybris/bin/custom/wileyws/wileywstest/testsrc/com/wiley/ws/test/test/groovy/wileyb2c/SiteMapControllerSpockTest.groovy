package com.wiley.ws.test.test.groovy.wileyb2c

import static org.apache.http.HttpStatus.SC_OK

import de.hybris.bootstrap.annotations.ManualTest

import com.wiley.ws.test.test.groovy.webservicetests.v3.spock.AbstractSpockTest

import groovyx.net.http.HttpResponseDecorator
import spock.lang.Shared
import spock.lang.Unroll

@ManualTest
@Unroll
class SiteMapControllerSpockTest extends AbstractSpockTest {

	private static final HOME_PAGE_PATHS = []
	private static final PRODUCT_PAGE_PATHS = ["/Sitemap-product-p-SITEMAP_PRODUCT", "/Sitemap-product-1-p-SITEMAP_PRODUCT_1"]
	private static final CATEGORY_LANDING_PAGE_PATHS = ["/Sitemap+category-c-SITEMAP_CATEGORY"]
	private static final CONTENT_PAGE_PATHS = ["/testContentPage"]
	private static final CUSTOM_PAGE_PATHS = []
	private static final WRONG_CATEGORY_LANDING_PAGE_PATHS = ["/Sitemap-category-2-c-SITEMAP_CATEGORY_2"]
	private static final WRONG_PRODUCT_PAGE_PATHS = ["/Sitemap-product-unapproved-p-SITEMAP_PRODUCT_UNAPPROVED"]
	private static final WRONG_HOME_PAGE_PATHS = []
	private static final WRONG_CONTENT_PAGE_PATHS = []
	private static final WRONG_CUSTOM_PAGE_PATHS = []

	private static final LOCALE_PATHS = ["/en-us"]

	@Shared
	def sitemapResponse

	def "should return not empty sitemap.xml"() {
		when:
		HttpResponseDecorator response = restClient.get(
				uri: getSiteMapUri(),
				path: getSiteMapPath())

		then:
		response.status == SC_OK
		def responseData = response.data
		responseData.name() == 'sitemapindex'
	}

	def "should return not empty content search urls"() {
		when:
		HttpResponseDecorator response = restClient.get(
				uri: getContentSearchUrlsUri(),
				path: getContentSearchUrlsPath())

		then:
		response.status == SC_OK
		String textResponse = response.data.text
		isNotEmpty(textResponse)
	}

	def "sitemap should consist proper locations for all page types" () {
		when:
		HttpResponseDecorator response = restClient.get(
				uri: getSiteMapUri(),
				path: getSiteMapPath())

		then:
		def responseData = response.data
		def productPath = productLocation(responseData)
		isNotEmpty(productPath)
		def homepagePath= homepageLocation(responseData)
		isNotEmpty(homepagePath)
		def categoryLandingPath = categoryLandingLocation(responseData)
		isNotEmpty(categoryLandingPath)
		def contentPath = contentLocation(responseData)
		isNotEmpty(contentPath)
		def customPath = customLocation(responseData)
		isNotEmpty(customPath)
	}

	def "sitemap.xml entry #paths should consist #expectedPaths and not cosist #notExpectedPaths "() {
		setup:
		HttpResponseDecorator response = restClient.get(
				uri: getSiteMapUri(),
				path: getSiteMapPath())

		sitemapResponse = response.data
		def locations = "$paths"(sitemapResponse)

		when:
		def result = compareLocationsFromUrl(locations, expectedPaths, notExpectedPaths)

		then:
		result

		where:
		paths | expectedPaths | notExpectedPaths
		'homepageLocation'        | HOME_PAGE_PATHS             | WRONG_HOME_PAGE_PATHS
		'categoryLandingLocation' | CATEGORY_LANDING_PAGE_PATHS | WRONG_CATEGORY_LANDING_PAGE_PATHS
		'contentLocation'         | CONTENT_PAGE_PATHS          | WRONG_CONTENT_PAGE_PATHS
		'customLocation'          | CUSTOM_PAGE_PATHS           | WRONG_CUSTOM_PAGE_PATHS
	}



	def "contentsearch/urls used by internal Nutch crawler generated properly: "() {
		when:
		HttpResponseDecorator response = restClient.get(
				uri: getContentSearchUrlsUri(),
				path: getContentSearchUrlsPath())


		then:
		String textResponse = response.data.text
		def locations = textResponse.readLines()
		def expectedPaths = HOME_PAGE_PATHS + CATEGORY_LANDING_PAGE_PATHS + CONTENT_PAGE_PATHS + CUSTOM_PAGE_PATHS
		def notExpectedPaths = WRONG_CATEGORY_LANDING_PAGE_PATHS + WRONG_PRODUCT_PAGE_PATHS
		compareLocations(locations, expectedPaths, notExpectedPaths)
	}

	private compareLocationsFromUrl(urls, expectedPaths, notExpectedPaths) {
		def locs = []
		urls.each { url ->
			HttpResponseDecorator locResponse = restClient.get(uri: url)
			def locXmlResponseRootNode = locResponse.data
			def locNodes = locXmlResponseRootNode.url.loc.findAll()
			locs.addAll(locNodes.collect { it.text() })
		}

		compareLocations(locs, expectedPaths, notExpectedPaths)
	}

	private compareLocations(List locs, expectedPaths, notExpectedPaths) {
		def expectedLocations = []
		def notExpectedLocations= []
		LOCALE_PATHS.each { expectedLocalePath ->
			expectedPaths.each { expectedPath ->
				expectedLocations << getSiteMapUri() + getWileyb2cBasePath() + expectedLocalePath + expectedPath
			}
			notExpectedPaths.each { notExpectedPath ->
				notExpectedLocations << getSiteMapUri() + getWileyb2cBasePath() + expectedLocalePath + notExpectedPath
			}
		}
		assert locs.containsAll(expectedLocations)
		assert !locs.intersect(notExpectedLocations)
		true
	}

	def productLocation(sitemapResponse) {
		extractLocations(sitemapResponse, /.*PRODUCT-en_US-USD.*\.xml.*/)
	}

	def homepageLocation(sitemapResponse) {
		extractLocations(sitemapResponse, /.*HOMEPAGE-en_US-USD.*\.xml.*/)
	}

	def categoryLandingLocation(sitemapResponse) {
		extractLocations(sitemapResponse, /.*CATEGORYLANDING-en_US-USD.*\.xml.*/)
	}

	def contentLocation(sitemapResponse) {
		extractLocations(sitemapResponse, /.*CONTENT-en_US-USD.*\.xml.*/)
	}

	def customLocation(sitemapResponse) {
		extractLocations(sitemapResponse, /.*CUSTOM-en_US-USD.*\.xml.*/)
	}

	private extractLocations(sitemapResponse, pattern) {
		def locNodes = sitemapResponse.sitemap.loc.findAll().collect()
		def filteredNodes = locNodes.findAll{ it.text() ==~ pattern }.collect{ it.text() }
	}
}
