package com.wiley.ws.test.test.groovy.webservicetests.v3.spock

import de.hybris.bootstrap.annotations.ManualTest
import groovyx.net.http.HttpResponseDecorator
import spock.lang.Unroll


import static groovyx.net.http.ContentType.JSON


@ManualTest
@Unroll
class WileyCustomerUpdateControllerSpockTest extends AbstractSpockTransactionalTest{

    static final String CUSTOMER_ID = "orderhistoryuser@test.com"
    static final String TOO_LONG_CUSTOMER_ID = "EXTERNAL_CODE".padLeft(257, "_");
    protected static final CUSTOMER_UPDATE_REQUEST =  [
        'userId':'kokoko'
    ]
    protected static final INVALID_CUSTOMER_UPDATE_REQUEST = [
            'userId':"USER_ID".padLeft(257, "_")
    ]


    def "Trusted client requests customer update with too long customer ID: "() {
        given: "trusted client"
        authorizeTrustedClient(restClient)

        when: "trusted client requests customer update"
        HttpResponseDecorator response = restClient.post(
                path: getBasePath() + "/customers/" + TOO_LONG_CUSTOMER_ID,
                contentType: responseFormat,
                body: postBody,
                requestContentType: requestFormat)

        then: "he receives 400 response"
        with(response) {
            if (isNotEmpty(data) && isEmpty(data.errors)) println(data)
            isNotEmpty(data.errors)
            data.errors[0].type == 'MethodConstraintViolationError'
            status == 400
        }

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | CUSTOMER_UPDATE_REQUEST
    }

    def "Trusted client requests order status update with valid customer ID and CUSTOMER_UPDATE_REQUEST: "() {
        given: "trusted client"
        authorizeTrustedClient(restClient)

        when: "trusted client requests customer update"
        HttpResponseDecorator response = restClient.post(
                path: getBasePath() + "/customers/" + CUSTOMER_ID,
                contentType: responseFormat,
                body: postBody,
                requestContentType: requestFormat)

        then: "he receives 204 response"
        with(response) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            status == 204
            isEmpty(data)
        }

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | CUSTOMER_UPDATE_REQUEST
    }

    def "Trusted client requests order status update with invalid CUSTOMER_UPDATE_REQUEST: "() {
        given: "trusted client"
        authorizeTrustedClient(restClient)

        when: "trusted client requests customer update"
        HttpResponseDecorator response = restClient.post(
                path: getBasePath() + "/customers/" + CUSTOMER_ID,
                contentType: responseFormat,
                body: postBody,
                requestContentType: requestFormat)

        then: "he receives 400 response"
        with(response) {
            if (isNotEmpty(data) && isEmpty(data.errors)) println(data)
            isNotEmpty(data.errors)
            status == 400
        }

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | INVALID_CUSTOMER_UPDATE_REQUEST
    }



}
