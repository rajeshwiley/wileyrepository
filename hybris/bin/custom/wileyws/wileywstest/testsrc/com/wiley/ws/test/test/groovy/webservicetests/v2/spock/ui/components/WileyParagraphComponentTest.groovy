package com.wiley.ws.test.test.groovy.webservicetests.v2.spock.ui.components

import com.wiley.ws.test.test.groovy.webservicetests.v2.spock.AbstractGUITest
import com.wiley.ws.test.test.groovy.webservicetests.v3.spock.AbstractSpockTransactionalTest
import de.hybris.bootstrap.annotations.ManualTest
import groovyx.net.http.HttpResponseDecorator
import spock.lang.Unroll

import static groovyx.net.http.ContentType.*
import static org.apache.http.HttpStatus.*

@ManualTest
@Unroll
class WileyParagraphComponentTest extends AbstractSpockTransactionalTest {

    private static final PRIVACY_POLICY_COMPONENT_ID = "comp_00001KCC"
    private static final PRIVACY_POLICY_BAD_COMPONENT_ID = "privacy-policy-wiley-webbbbbbb"
    private static final BAD_COMPONENT = "AccountAddressBookComponent"
    private static final CONTEXT = "/content"
    private static final PARAGRAPHCOMPONENTS_PREFIX_URL = AbstractGUITest.getHttpsWebroot() + "/v2/wileyb2c/paragraphcomponents/"

    def "Anonymous client successfully gets the privacy policy page"() {
        given: "Wiley unauthorized client"

        when: "Anonymous client requests privacy policy page"
        HttpResponseDecorator response = restClient.get(
                path: PARAGRAPHCOMPONENTS_PREFIX_URL + PRIVACY_POLICY_COMPONENT_ID + CONTEXT,
                query: [lang: 'en_GB'],
                contentType: responseFormat)

        then: "Anonymous client receives the privacy policy page"
        with(response) {
            status == SC_OK
            println(data)
            isNotEmpty(data)
        }

        where:
        requestFormat | responseFormat
        HTML          | HTML
    }

    def "Anonymous client successfully gets the privacy policy page for en locale by default"() {
        given: "Wiley unauthorized client"

        when: "Anonymous client requests the privacy policy page without any lang parameter"
        HttpResponseDecorator response = restClient.get(
                path: PARAGRAPHCOMPONENTS_PREFIX_URL + PRIVACY_POLICY_COMPONENT_ID + CONTEXT,
                contentType: responseFormat)

        then: "Anonymous client receives the privacy policy page for en locale"
        with(response) {
            status == SC_OK
            println(data)
            isNotEmpty(data)
        }

        where:
        requestFormat | responseFormat
        HTML          | HTML
    }

    def "Anonymous client gets an error for bad language"() {
        given: "Wiley unauthorized client"

        when: "Anonymous client requests a page for bad language"
        HttpResponseDecorator response = restClient.get(
                path: PARAGRAPHCOMPONENTS_PREFIX_URL + PRIVACY_POLICY_COMPONENT_ID + CONTEXT,
                query: [lang: 'AAAA'],
                contentType: responseFormat,
                requestContentType: requestFormat)

        then: "Anonymous client receives UnsupportedLanguageError"
        with(response) {
            status == SC_BAD_REQUEST
            isNotEmpty(data)
            data.errors[0].type == 'UnsupportedLanguageError'
            data.errors[0].message == ("Language  AAAA is not supported")
        }

        where:
        requestFormat | responseFormat
        HTML          | JSON
    }

    def "Anonymous client gets an error for nonexistent component id"() {
        given: "Wiley unauthorized client"

        when: "Anonymous client requests privacy policy page"
        HttpResponseDecorator response = restClient.get(
                path: PARAGRAPHCOMPONENTS_PREFIX_URL + PRIVACY_POLICY_BAD_COMPONENT_ID + CONTEXT,
                query: [lang: 'en_GB'],
                contentType: responseFormat,
                requestContentType: requestFormat)

        then: "Anonymous client receives WileywsNotFoundError"
        with(response) {
            status == SC_NOT_FOUND
            isNotEmpty(data)
            data.errors[0].type == 'WileywsNotFoundError'
            data.errors[0].message == ("Could not find Paragraph Component with uid = " + PRIVACY_POLICY_BAD_COMPONENT_ID + " on wileyb2c.")
        }

        where:
        requestFormat | responseFormat
        HTML          | ANY
    }

    def "Anonymous client gets an error not for a CMSParagraphComponent"() {
        given: "Wiley unauthorized client"

        when: "Anonymous client requests a page other than CMSParagraphComponent"
        HttpResponseDecorator response = restClient.get(
                path: PARAGRAPHCOMPONENTS_PREFIX_URL + BAD_COMPONENT + CONTEXT,
                query: [lang: 'en_GB'],
                contentType: responseFormat,
                requestContentType: requestFormat)

        then: "Anonymous client receives ClassCastError"
        with(response) {
            status == SC_BAD_REQUEST
            isNotEmpty(data)
            data.errors[0].type == 'ClassCastError'
            data.errors[0].message == ("Component " + BAD_COMPONENT + " is not a Paragraph Component")
        }

        where:
        requestFormat | responseFormat
        HTML          | ANY
    }
}