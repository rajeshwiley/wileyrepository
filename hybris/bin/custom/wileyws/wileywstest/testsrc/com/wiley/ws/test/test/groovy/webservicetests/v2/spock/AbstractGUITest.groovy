/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 */

package com.wiley.ws.test.test.groovy.webservicetests.v2.spock

import com.wiley.ws.test.test.groovy.webservicetests.config.TestConfigFactory
import geb.spock.GebReportingSpec
import org.apache.commons.logging.LogFactory
import org.apache.log4j.PropertyConfigurator

abstract class AbstractGUITest extends GebReportingSpec {
    private static String LOG4J_PROPERTIES_CLASS_PATH = "wileywstest/log4j.properties";
    private static String COMMONS_LOGGING_LOGGER_ATTRIBUTE_NAME = "org.apache.commons.logging.Log";
    private static String COMMONS_LOGGING_LOGGER_ATTRIBUTE_VALUE = "org.apache.commons.logging.impl.Log4JLogger";

    protected static ConfigObject config = TestConfigFactory.createConfig("v2", "groovytests-property-file.groovy");

    static {
        initializeLogging();
    }

    /**
     * Method is used to configure commons-logging and log4j.
     * Commons-logging is used by RestClient, thus it should be called before RestClient or any other logger is created.
     * It overrides default configuration in platform/ext/core/resources/commons-logging.properties,
     * where CommonsHybrisLog4jWrapper is declared as Logger
     */
    def synchronized static initializeLogging() {
        if (LogFactory.factories.isEmpty()) {
            //configure only if logging hasn't been configured yet
            ClassLoader loader = Thread.currentThread().getContextClassLoader();
            LogFactory.getFactory().setAttribute(COMMONS_LOGGING_LOGGER_ATTRIBUTE_NAME, COMMONS_LOGGING_LOGGER_ATTRIBUTE_VALUE);
            URL url = loader.getResource(LOG4J_PROPERTIES_CLASS_PATH);
            PropertyConfigurator.configure(url);
        }
    }

    def setup() {
    }

    def cleanup() {
    }

    protected static final String getDefaultHttpUri() {
        return config.DEFAULT_HTTP_URI
    }

    protected static final String getDefaultHttpsUri() {
        return config.DEFAULT_HTTPS_URI
    }

    protected static final String getHttpsWebroot() {
        return config.HTTPS_WEBROOT
    }

    protected static final String getHttpWebroot() {
        return config.HTTP_WEBROOT
    }

    protected static final String getBasePath() {
        return config.BASE_PATH
    }

    protected static final String getBasePathWithSite() {
        return config.BASE_PATH_WITH_SITE
    }

    protected static final String getBasePathWithIntegrationSite() {
        return config.BASE_PATH_WITH_INTEGRATION_SITE
    }

    protected static final String getOAuth2TokenUri() {
        return config.OAUTH2_TOKEN_URI
    }

    protected static final String getOAuth2TokenPath() {
        return config.OAUTH2_TOKEN_ENDPOINT_PATH
    }

    protected static final String getClientId() {
        return config.CLIENT_ID
    }

    protected static final String getClientSecret() {
        return config.CLIENT_SECRET
    }

    protected static final String getClientRedirectUri() {
        return config.OAUTH2_CALLBACK_URI
    }

    protected static final String getTrustedClientId() {
        return config.TRUSTED_CLIENT_ID
    }

    protected static final String getTrustedClientSecret() {
        return config.TRUSTED_CLIENT_SECRET
    }
}
