/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package com.wiley.ws.test.test.groovy.webservicetests.docu;



/**
 * Event representing TestUtil method call
 */
public class TestUtilMethodInvEvent
{
	/**
	 * The enum Method inv event type.
	 */
	public enum MethodInvEventType
	{
		/**
		 * Get connection method inv event type.
		 */
		GET_CONNECTION,
		/**
		 * Get response method inv event type.
		 */
		GET_RESPONSE
	}

	private final MethodInvEventType type;
	private String resource;
	private String accept;
	private String method;
	private String response;

	/**
	 * Gets type.
	 *
	 * @return the type
	 */
	public MethodInvEventType getType()
	{
		return type;
	}

	/**
	 * Gets resource.
	 *
	 * @return the resource
	 */
	public String getResource()
	{
		return resource;
	}

	/**
	 * Gets response.
	 *
	 * @return the response
	 */
	public String getResponse()
	{
		return response;
	}

	/**
	 * Gets accept.
	 *
	 * @return the accept
	 */
	public String getAccept()
	{
		return accept;
	}

	/**
	 * Gets method.
	 *
	 * @return the method
	 */
	public String getMethod()
	{
		return method;
	}

	/**
	 * Instantiates a new Test util method inv event.
	 *
	 * @param resource
	 * 		the resource
	 * @param accept
	 * 		the accept
	 * @param method
	 * 		the method
	 */
	public TestUtilMethodInvEvent(final String resource, final String accept, final String method)
	{
		type = MethodInvEventType.GET_CONNECTION;
		this.resource = resource;
		this.accept = accept;
		this.method = method;
	}

	/**
	 * Instantiates a new Test util method inv event.
	 *
	 * @param response
	 * 		the response
	 */
	public TestUtilMethodInvEvent(final String response)
	{
		type = MethodInvEventType.GET_RESPONSE;
		this.response = response;
	}

	/**
	 * To string string.
	 *
	 * @return the string
	 */
/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "TestUtilMethodInvEvent [type=" + type + ", resource=" + resource + ", accept=" + accept + ", method=" + method
				+ ", response=" + response + "]";
	}


}
