/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 */

package com.wiley.ws.test.test.groovy.webservicetests.v3.spock

import com.wiley.ws.core.constants.WileyWSConstants
import groovyx.net.http.HttpResponseDecorator
import groovyx.net.http.RESTClient

import static groovyx.net.http.ContentType.JSON
import static org.apache.http.HttpStatus.SC_CREATED
import static org.apache.http.HttpStatus.SC_OK

abstract class AbstractSpockTransactionalTest extends AbstractSpockTest {

    private static String TRANSACTION_ID_PARAMETER = WileyWSConstants.TRANSACTION_ID_PARAMETER;

    def setup() {
        if (shouldUseTransaction()) {
            startTrustedClientTransaction(restClient)
        }
    }

    def cleanup() {
        if (shouldUseTransaction()) {
            rollbackTrustedClientTransaction(restClient)
        }
    }

    protected boolean shouldUseTransaction(){
        return true;
    }

    protected static final String getTransactionUri() {
        return config.TRANSACTION_URI
    }

    protected static final String getTransactionEndpointUri() {
        return config.TRANSACTION_ENDPOINT_URI
    }

    protected static final String getTransactionPath() {
        return config.TRANSACTION_ENDPOINT_PATH
    }

    protected HttpResponseDecorator startTransaction(RESTClient client) {
        HttpResponseDecorator response = client.post(
                uri: getTransactionEndpointUri(),
                contentType: JSON,
                requestContentType: JSON)

        with(response) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            assert status == SC_CREATED
            assert data.transactionId
        }

        return response
    }

    protected HttpResponseDecorator rollbackTransaction(RESTClient client) {
        HttpResponseDecorator response = client.put(
                uri: getTransactionEndpointUri() + "/" + getTransactionId(client) + "/rollback",
                contentType: JSON,
                requestContentType: JSON)

        with(response) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            assert status == SC_OK
            assert data.transactionId
        }

        return response
    }

    protected void addTransaction(RESTClient client, HttpResponseDecorator transactionResponse) {
        client.getHeaders().put(TRANSACTION_ID_PARAMETER, transactionResponse.responseData.transactionId)
    }

    protected getTransactionId(RESTClient client) {
        return client.getHeaders().get(TRANSACTION_ID_PARAMETER)
    }

    protected void removeTransactionId(RESTClient client) {
        client.getHeaders().remove(TRANSACTION_ID_PARAMETER)
    }

    protected void startTrustedClientTransaction(RESTClient client) {
        def HttpResponseDecorator transactionResponse = startTransaction(client)
        addTransaction(client, transactionResponse)
    }

    protected void rollbackTrustedClientTransaction(RESTClient client) {
        def transactionId = getTransactionId(client)
        if (transactionId != null) {
            rollbackTransaction(client)
            removeTransactionId(client)
        }
    }
}
