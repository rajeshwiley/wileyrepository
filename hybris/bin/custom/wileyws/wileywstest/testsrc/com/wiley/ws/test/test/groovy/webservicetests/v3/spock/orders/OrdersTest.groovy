/**
 *
 */
package com.wiley.ws.test.test.groovy.webservicetests.v3.spock.orders

import com.wiley.ws.test.test.groovy.webservicetests.v3.spock.AbstractSpockTransactionalTest
import de.hybris.bootstrap.annotations.ManualTest
import de.hybris.platform.core.enums.OrderStatus
import groovyx.net.http.HttpResponseDecorator
import spock.lang.Unroll

import static groovyx.net.http.ContentType.JSON
import static groovyx.net.http.ContentType.URLENC
import static org.apache.http.HttpStatus.SC_NO_CONTENT
import static org.apache.http.HttpStatus.SC_OK

/**
 *
 *
 */
@ManualTest
@Unroll
class OrdersTest extends AbstractSpockTransactionalTest {
    static final String ORDER_CODE = "testOrder1"
    static final String TOO_LONG_EXTERNAL_CODE = "EXTERNAL_CODE".padLeft(257, "_");
    protected static final ORDER_STATUS_UPDATE_REQUEST = [
            'status' : 'CANCELLED',
            'message': [
                    'code'   : 'test code',
                    'message': 'test message'
            ]
    ]
    protected static final INVALID_ORDER_STATUS_UPDATE_REQUEST = [
            'message': [
                    'code'   : 'test code',
                    'message': 'test message'
            ]
    ]

    protected static final ORDER_SHIPPING_UPDATE_REQUEST = [
                "shippingDate" : "2016-11-10T12:57:43+03:00",
                "trackingId"   : "TEST_TRACKING_ID_01",
                "carrier"      : "TEST_CARRIER_01",
                "entries" :
                [
                        [
                            "isbn" : "222222222221",
                            "shippedQuantity"   : 2
                        ]
                ]
    ]

    protected static final INVALID_ORDER_SHIPPING_UPDATE_REQUEST = [
            "trackingId"   : "TEST_TRACKING_ID_01",
            "carrier"      : "TEST_CARRIER_01",
            "entries" :
                    [
                            [
                                    "isbn" : "222222222221",
                                    "shippedQuantity"   : 2
                            ]
                    ]
    ]


    protected static final ORDER_UPDATE_REQUEST = [
        "country": "PL",
        "currency": "USD",

        "entries": [[
                        "sapProductCode": "sapWCOM_B2C_B2B_PRODUCT",
                        "isbn": "222222222222",
                        "quantity": 1,
                        "basePrice": 6,
                        "totalPrice": 18
                    ]
        ],

        "subtotal": 33.0,

        "deliveryAddress": [
            "addressId": "D1223415_123",
            "postcode": "D",
            "country": "BY",
            "city": "NY",
            "line1": "D",
            "line2": "D",
            "phoneNumber": "12999999"
        ],



        "deliveryCost": 3.0,
        "totalTax": 4.0,
        "totalPrice": 33.0

    ]


    protected static final INVALID_ORDER_UPDATE_REQUEST = [
            "country": "RU",
            "currency": "USD",


            "subtotal": 11.0,
            "totalDiscount": 2.0,
            "deliveryCost": 3.0,
            "totalTax": 4.0,
            "totalPrice": 5.0,
            "deliveryAddress": [
                    "postcode": "191119",
                    "country": "RU",
                    "city": "St. Petersburg",
                    "line1": "Voronezhskaya",
                    "line2": "5"
            ],
            "discounts": [
                    [
                            "name": "discount1",
                            "absolute": true,
                            "value": 3.0
                    ],
                    [
                            "name": "discount2",
                            "absolute": false,
                            "value": 5.0
                    ]
            ],
            "entries": [
                    [
                            "sapProductCode": "D1A5279D-B27D-4CD4-A05E-EFDD53D08E8D",
                            "isbn": "111111111111",
                            "quantity": 3,
                            "basePrice": 61,
                            "totalPrice": 17
                    ]
            ]
    ]


    def "Trusted client requests order update by code: #requestFormat"() {
        given: "trusted client"
        authorizeTrustedClient(restClient)

        String pathV2 = (getBasePathWithSite() + "/orders/" + ORDER_CODE).replace("/v3/", "/v2/")
        HttpResponseDecorator orderBeforeUpdate = restClient.get(
                path: pathV2,
                contentType: requestFormat,
                requestContentType: URLENC)
        with(orderBeforeUpdate) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            status == SC_OK
            data.status != OrderStatus.CANCELLED.toString()
        }

        when: "trusted client requests order by code"
        HttpResponseDecorator response = restClient.put(
                path: getBasePath() + "/orders/" + ORDER_CODE + "/status",
                contentType: responseFormat,
                body: postBody,
                requestContentType: requestFormat)

        then: "he receives 200 response with empty body"
        with(response) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            status == SC_NO_CONTENT
            isEmpty(data)
        }

        HttpResponseDecorator orderAfterUpdate = restClient.get(
                path: pathV2,
                contentType: requestFormat,
                requestContentType: URLENC)
        with(orderAfterUpdate) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            status == SC_OK
            data.status == OrderStatus.CANCELLED.toString()
        }

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | ORDER_STATUS_UPDATE_REQUEST
    }


    def "Trusted client requests order status update with too long orderCode: "() {
        given: "trusted client"
        authorizeTrustedClient(restClient)

        when: "trusted client requests order status update"
        HttpResponseDecorator response = restClient.put(
                path: getBasePath() + "/orders/" + TOO_LONG_EXTERNAL_CODE + "/status",
                contentType: responseFormat,
                body: postBody,
                requestContentType: requestFormat)

        then: "he receives 400 response and WileywsValidationError"
        with(response) {
            if (isNotEmpty(data) && isEmpty(data.errors)) println(data)
            isNotEmpty(data.errors)
            data.errors[0].type == 'MethodConstraintViolationError'
            status == 400
        }

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | ORDER_STATUS_UPDATE_REQUEST
    }

    def "Trusted client requests order status update with valid orderCode and ORDER_STATUS_UPDATE_REQUEST: "() {
        given: "trusted client"
        authorizeTrustedClient(restClient)

        when: "trusted client requests order status update"
        HttpResponseDecorator response = restClient.put(
                path: getBasePath() + "/orders/" + ORDER_CODE + "/status",
                contentType: responseFormat,
                body: postBody,
                requestContentType: requestFormat)

        then: "he receives 204 response"
        with(response) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            status == 204
            isEmpty(data)
        }

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | ORDER_STATUS_UPDATE_REQUEST
    }

    def "Trusted client requests order status update with invalid ORDER_STATUS_UPDATE_REQUEST: "() {
        given: "trusted client"
        authorizeTrustedClient(restClient)

        when: "trusted client requests order status update"
        HttpResponseDecorator response = restClient.put(
                path: getBasePath() + "/orders/" + ORDER_CODE + "/status",
                contentType: responseFormat,
                body: postBody,
                requestContentType: requestFormat)

        then: "he receives 400 response"
        with(response) {
            if (isNotEmpty(data) && isEmpty(data.errors)) println(data)
            isNotEmpty(data.errors)
            status == 400
        }

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | INVALID_ORDER_STATUS_UPDATE_REQUEST
    }



    def "Trusted client requests order shipping info update with valid orderCode and ORDER_SHIPPING_UPDATE_REQUEST: "() {
        given: "trusted client"
        authorizeTrustedClient(restClient)

        when: "trusted client requests shipping info update"
        HttpResponseDecorator response = restClient.put(
                path: getBasePath() + "/orders/" + ORDER_CODE + "/shipping",
                contentType: responseFormat,
                body: postBody,
                requestContentType: requestFormat)

        then: "he receives 204 response"
        with(response) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            status == 204
            isEmpty(data)
        }

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | ORDER_SHIPPING_UPDATE_REQUEST
    }

    def "Trusted client requests order shipping info update with invalid orderCode: "() {
        given: "trusted client"
        authorizeTrustedClient(restClient)

        when: "trusted client requests shipping info update"
        HttpResponseDecorator response = restClient.put(
                path: getBasePath() + "/orders/" + TOO_LONG_EXTERNAL_CODE + "/shipping",
                contentType: responseFormat,
                body: postBody,
                requestContentType: requestFormat)

        then: "he receives 400 response"
        with(response) {
            if (isNotEmpty(data) && isEmpty(data.errors)) println(data)
            isNotEmpty(data.errors)
            status == 400
        }

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | ORDER_SHIPPING_UPDATE_REQUEST
    }

    def "Trusted client requests order shipping info update with invalid ORDER_SHIPPING_UPDATE_REQUEST: "() {
        given: "trusted client"
        authorizeTrustedClient(restClient)

        when: "trusted client requests shipping info update"
        HttpResponseDecorator response = restClient.put(
                path: getBasePath() + "/orders/" + ORDER_CODE + "/shipping",
                contentType: responseFormat,
                body: postBody,
                requestContentType: requestFormat)

        then: "he receives 400 response"
        with(response) {
            if (isNotEmpty(data) && isEmpty(data.errors)) println(data)
            isNotEmpty(data.errors)
            status == 400
        }

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | INVALID_ORDER_SHIPPING_UPDATE_REQUEST
    }

    def "Trusted client requests order update with valid orderCode and ORDER_UPDATE_REQUEST: "() {
        given: "trusted client"
        authorizeTrustedClient(restClient)

        when: "trusted client requests order update"
        HttpResponseDecorator response = restClient.post(
                path: getBasePath() + "/orders/" + ORDER_CODE,
                contentType: responseFormat,
                body: postBody,
                requestContentType: requestFormat)

        then: "he receives 204 response"
        with(response) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            status == 204
            isEmpty(data)
        }

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | ORDER_UPDATE_REQUEST
    }

    def "Trusted client requests order update with too long orderCode: "() {
        given: "trusted client"
        authorizeTrustedClient(restClient)

        when: "trusted client requests order update"
        HttpResponseDecorator response = restClient.post(
                path: getBasePath() + "/orders/" + TOO_LONG_EXTERNAL_CODE,
                contentType: responseFormat,
                body: postBody,
                requestContentType: requestFormat)

        then: "he receives 400 response"
        with(response) {
            if (isNotEmpty(data) && isEmpty(data.errors)) println(data)
            isNotEmpty(data.errors)
            status == 400
        }

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | ORDER_UPDATE_REQUEST
    }

    def "Trusted client requests order update with invalid ORDER_UPDATE_REQUEST: "() {
        given: "trusted client"
        authorizeTrustedClient(restClient)

        when: "trusted client requests order update"
        HttpResponseDecorator response = restClient.post(
                path: getBasePath() + "/orders/" + ORDER_CODE,
                contentType: responseFormat,
                body: postBody,
                requestContentType: requestFormat)

        then: "he receives 400 response"
        with(response) {
            if (isNotEmpty(data) && isEmpty(data.errors)) println(data)
            isNotEmpty(data.errors)
            status == 400
        }

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | INVALID_ORDER_UPDATE_REQUEST
    }



}
