/**
 *
 */
package com.wiley.ws.test.test.groovy.webservicetests.v3.spock.transactions

import com.wiley.ws.core.constants.WileyWSConstants
import com.wiley.ws.core.order.dto.OrderStatusWsDTO
import com.wiley.ws.test.test.groovy.webservicetests.v3.spock.AbstractSpockTransactionalTest
import de.hybris.bootstrap.annotations.ManualTest
import de.hybris.platform.core.enums.OrderStatus
import groovyx.net.http.ContentType
import groovyx.net.http.HttpResponseDecorator
import spock.lang.Unroll

import static groovyx.net.http.ContentType.JSON
import static groovyx.net.http.ContentType.URLENC
import static org.apache.http.HttpStatus.*

/**
 *
 *
 */
@ManualTest
@Unroll
class TransactionsTest extends AbstractSpockTransactionalTest {
    public static final String ORDER_CODE = "testOrder1"
    public static final String ORIGINAL_STATUS = OrderStatus.CREATED.toString()
    public static final String NEW_STATUS = OrderStatusWsDTO.StatusEnum.CANCELLED.toString()
    public static final String NEW_STATUS_2 = OrderStatusWsDTO.StatusEnum.NOT_YET_PROCESSED.toString()
    public static final String NEW_STATUS_3 = OrderStatusWsDTO.StatusEnum.FAILED.toString()
    public static final int MILLISECONDS_IN_SECOND = 1000
    public static
    final MAXIMUM_TRANSACTION_LIVE_TIME_MILLISECONDS = WileyWSConstants.TRANSACTION_EXPIRATION_TIMEOUT_SECONDS * MILLISECONDS_IN_SECOND + WileyWSConstants.TRANSACTION_EXPIRATION_CLEANUP_INTERVAL_MILLISECONDS
    public static final MYSQL_ACQUIRE_DB_LOCK_TIMEOUT_SECONDS = 50
    public static final NORMAL_RESPONSE_TIMEOUT_SECONDS = 10

    @Override
    protected boolean shouldUseTransaction() {
        return false;
    }

    def "Old transactions are evicted if simultaneous transactions number grows beyond limit"() {
        given: "trusted client"
        authorizeTrustedClient(restClient)

        when: "tries to start more transactions then limit"
        def createTransactionResponses = [];
        int extraTransactionsNumber = 3;
        int totalTransactionsNumber = WileyWSConstants.TRANSACTION_MAXIMUM_SIZE + extraTransactionsNumber;

        for (int i = 0; i < totalTransactionsNumber; i++) {
            HttpResponseDecorator createTransactionResponse = startTransaction(restClient)
            createTransactionResponse.status == SC_CREATED
            createTransactionResponses.add(createTransactionResponse)
        }

        then: "firstly started transactions should already be evicted, i.e. should fail to rollback"
        for (int j = 0; j < totalTransactionsNumber; j++) {
            addTransaction(restClient, createTransactionResponses[j])

            def HttpResponseDecorator rollbackTransactionResponse = restClient.put(
                    uri: getTransactionEndpointUri() + "/rollback",
                    contentType: JSON,
                    requestContentType: JSON)

            // firstly started transactions should be evicted
            if (j < extraTransactionsNumber) {
                rollbackTransactionResponse.status == SC_REQUEST_TIMEOUT
                rollbackTransactionResponse.data.errors[0].message == ("Transaction not found: " + getTransactionId(restClient))
                rollbackTransactionResponse.data.errors[0].type == "WileywsTestTransactionNotFoundError"
            } else {
                rollbackTransactionResponse.status == SC_OK
                rollbackTransactionResponse.data.transactionId == getTransactionId(restClient)
            }
            removeTransactionId(restClient)
        }

        where:
        requestFormat | responseFormat | orderStatus
        JSON          | JSON           | NEW_STATUS
    }

    def "Transactional changes are isolated"() {
        given: "trusted client"
        authorizeTrustedClient(restClient)
        checkOrderStatus(ORDER_CODE, ORIGINAL_STATUS, responseFormat)

        when: "trusted updates order status inside transaction"
        def createTransactionResponse = startTransaction(restClient)
        addTransaction(restClient, createTransactionResponse)
        updateOrderStatus(ORDER_CODE, NEW_STATUS, requestFormat, responseFormat)
        checkOrderStatus(ORDER_CODE, NEW_STATUS, responseFormat)
        removeTransactionId(restClient)

        then: "original status should be returned without transaction"
        def createAnotherTransactionResponse = startTransaction(restClient)
        addTransaction(restClient, createAnotherTransactionResponse)
        checkOrderStatus(ORDER_CODE, ORIGINAL_STATUS, responseFormat)

        // rollback transaction explicitly to not wait for auto rollback and not affect other test cases
        addTransaction(restClient, createTransactionResponse)
        rollbackTrustedClientTransaction(restClient)

        where:
        requestFormat | responseFormat | orderStatus
        JSON          | JSON           | NEW_STATUS
    }

    def "Transactions are evicted and rolled back after timeout"() {
        //HTTPBuilder has no direct methods to add timeouts.  We have to add them to the HttpParams of the underlying HttpClient
        restClient.client.getParams().setParameter("http.connection.timeout", new Integer(NORMAL_RESPONSE_TIMEOUT_SECONDS * MILLISECONDS_IN_SECOND))
        restClient.client.getParams().setParameter("http.socket.timeout", new Integer(NORMAL_RESPONSE_TIMEOUT_SECONDS * MILLISECONDS_IN_SECOND))

        given: "trusted client"
        authorizeTrustedClient(restClient)
        checkOrderStatus(ORDER_CODE, ORIGINAL_STATUS, responseFormat)

        when: "trusted updates order status inside transaction"
        def createTransactionResponse = startTransaction(restClient)
        addTransaction(restClient, createTransactionResponse)
        updateOrderStatus(ORDER_CODE, NEW_STATUS, requestFormat, responseFormat)
        checkOrderStatus(ORDER_CODE, NEW_STATUS, responseFormat)
        removeTransactionId(restClient)

        then: "after timeout transactionId is no more valid"
        Thread.sleep(MAXIMUM_TRANSACTION_LIVE_TIME_MILLISECONDS)
        addTransaction(restClient, createTransactionResponse)
        checkOrderStatusShouldFail(ORDER_CODE, responseFormat)
        removeTransactionId(restClient)

        then: "Update of the same order in another transaction should NOT timeout trying to get lock"
        def createTransactionResponse2 = startTransaction(restClient)
        addTransaction(restClient, createTransactionResponse2)
        updateOrderStatus(ORDER_CODE, NEW_STATUS_2, requestFormat, responseFormat)
        checkOrderStatus(ORDER_CODE, NEW_STATUS_2, responseFormat)
        removeTransactionId(restClient)

        then: "original status should be returned without transaction"
        def createAnotherTransactionResponse = startTransaction(restClient)
        addTransaction(restClient, createAnotherTransactionResponse)
        checkOrderStatus(ORDER_CODE, ORIGINAL_STATUS, responseFormat)

        // rollback second transaction explicitly to not wait for auto rollback and not affect other test cases
        addTransaction(restClient, createTransactionResponse2)
        rollbackTrustedClientTransaction(restClient)
        removeTransactionId(restClient)

        where:
        requestFormat | responseFormat | orderStatus
        JSON          | JSON           | NEW_STATUS
    }

    // TODO: need to be enabled when problem with hanging transactions is solved
    /*def "Simultaneous updates of the same entities are not allowed"() {
        //HTTPBuilder has no direct methods to add timeouts.  We have to add them to the HttpParams of the underlying HttpClient
        restClient.client.getParams().setParameter("http.connection.timeout", new Integer(NORMAL_RESPONSE_TIMEOUT_SECONDS * MILLISECONDS_IN_SECOND))
        restClient.client.getParams().setParameter("http.socket.timeout", new Integer(NORMAL_RESPONSE_TIMEOUT_SECONDS * MILLISECONDS_IN_SECOND))

        given: "trusted client"
        authorizeTrustedClient(restClient)

        checkOrderStatus(ORDER_CODE, ORIGINAL_STATUS, responseFormat)

        when: "trusted updates order status inside transaction"
        def createTransactionResponse = startTransaction(restClient)
        addTransaction(restClient, createTransactionResponse)
        updateOrderStatus(ORDER_CODE, NEW_STATUS, requestFormat, responseFormat)
        checkOrderStatus(ORDER_CODE, NEW_STATUS, responseFormat)
        removeTransactionId(restClient)

        then: "Simultaneous update of the same order in another transaction should timeout trying to get lock"

        def createTransactionResponse2 = startTransaction(restClient)
        addTransaction(restClient, createTransactionResponse2)
        updateOrderStatusShouldTimeout(ORDER_CODE, NEW_STATUS_2, requestFormat, responseFormat)
        removeTransactionId(restClient)


        then: "first transaction should rollback properly"
        addTransaction(restClient, createTransactionResponse)
        rollbackTransaction(restClient)
        removeTransactionId(restClient)

        then: "after first transaction rolled back, second transaction should not lock anymore and return " + NEW_STATUS_2 + " status even though update call failed with timeout"
        addTransaction(restClient, createTransactionResponse2)
        checkOrderStatus(ORDER_CODE, NEW_STATUS_2, responseFormat)
        removeTransactionId(restClient)

        then: "second transaction should rollback properly"
        addTransaction(restClient, createTransactionResponse2)
        rollbackTransaction(restClient)
        removeTransactionId(restClient)

        where:
        requestFormat | responseFormat | orderStatus
        JSON          | JSON           | NEW_STATUS
    }

    def "Simultaneous updates of the same entities do not leave active transactions"() {

        //HTTPBuilder has no direct methods to add timeouts.  We have to add them to the HttpParams of the underlying HttpClient
        restClient.client.getParams().setParameter("http.connection.timeout", new Integer(NORMAL_RESPONSE_TIMEOUT_SECONDS * MILLISECONDS_IN_SECOND))
        restClient.client.getParams().setParameter("http.socket.timeout", new Integer(NORMAL_RESPONSE_TIMEOUT_SECONDS * MILLISECONDS_IN_SECOND))

        given: "trusted client"
        authorizeTrustedClient(restClient)

        checkOrderStatus(ORDER_CODE, ORIGINAL_STATUS, responseFormat)

        when: "trusted updates order status inside transaction"
        def createTransactionResponse1 = startTransaction(restClient)
        addTransaction(restClient, createTransactionResponse1)
        updateOrderStatus(ORDER_CODE, NEW_STATUS, requestFormat, responseFormat)
        checkOrderStatus(ORDER_CODE, NEW_STATUS, responseFormat)
        removeTransactionId(restClient)

        then: "Simultaneous update of the same order in another transaction should timeout trying to get lock"

        def createTransactionResponse2 = startTransaction(restClient)
        addTransaction(restClient, createTransactionResponse2)
        updateOrderStatusShouldTimeout(ORDER_CODE, NEW_STATUS_2, requestFormat, responseFormat)
        removeTransactionId(restClient)

        then: "Try to rollback 'hanging' transaction explicitly"
        addTransaction(restClient, createTransactionResponse2)
        rollbackTransaction(restClient)
        removeTransactionId(restClient)

        then: "after Mysql acquire lock timeout no hanging transactions left"
        Thread.sleep(MYSQL_ACQUIRE_DB_LOCK_TIMEOUT_SECONDS * MILLISECONDS_IN_SECOND)

        def createTransactionResponse3 = startTransaction(restClient)
        addTransaction(restClient, createTransactionResponse3)
        updateOrderStatus(ORDER_CODE, NEW_STATUS_3, requestFormat, responseFormat)
        checkOrderStatus(ORDER_CODE, NEW_STATUS_3, responseFormat)
        // rollback transaction explicitly to not wait for auto rollback and not affect other test cases
        rollbackTransaction(restClient)
        removeTransactionId(restClient)

        where:
        requestFormat | responseFormat | orderStatus
        JSON          | JSON           | NEW_STATUS
    }*/

    private HttpResponseDecorator updateOrderStatus(String orderCode, String newOrderStatus, ContentType requestFormat, ContentType responseFormat) {
        HttpResponseDecorator orderUpdateResponse = restClient.put(
                path: getBasePath() + "/orders/" + orderCode + "/status",
                contentType: responseFormat,
                body: [
                        'status' : newOrderStatus,
                        'message': [
                                'code'   : 'test code',
                                'message': 'test message'
                        ]
                ],
                requestContentType: requestFormat)
        with(orderUpdateResponse) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            status == SC_NO_CONTENT
            isEmpty(data)
        }
        return orderUpdateResponse
    }

    private void updateOrderStatusShouldTimeout(String orderCode, String newOrderStatus, ContentType requestFormat, ContentType responseFormat) {
        def failed = false;
        try {
            restClient.put(
                    path: getBasePath() + "/orders/" + orderCode + "/status",
                    contentType: responseFormat,
                    body: [
                            'status' : newOrderStatus,
                            'message': [
                                    'code'   : 'test code',
                                    'message': 'test message'
                            ]
                    ],
                    requestContentType: requestFormat)
        } catch (SocketTimeoutException e) {
            failed = true;
        }
        failed == true;
    }

    private void checkOrderStatus(String orderCode, String expectedOrderStatus, ContentType responseFormat) {
        String pathV2 = (getBasePathWithSite() + "/orders/" + orderCode).replace("/v3/", "/v2/")
        HttpResponseDecorator orderBeforeUpdate = restClient.get(
                path: pathV2,
                contentType: responseFormat,
                requestContentType: URLENC)
        with(orderBeforeUpdate) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            status == SC_OK
            data.status == expectedOrderStatus
        }
    }

    private void checkOrderStatusShouldFail(String orderCode, ContentType responseFormat) {
        String pathV2 = (getBasePathWithSite() + "/orders/" + orderCode).replace("/v3/", "/v2/")
        HttpResponseDecorator orderBeforeUpdate = restClient.get(
                path: pathV2,
                contentType: responseFormat,
                requestContentType: URLENC)
        with(orderBeforeUpdate) {
            status == SC_REQUEST_TIMEOUT
            data.errors[0].message == ("Transaction not found: " + getTransactionId(restClient))
            data.errors[0].type == "WileywsTestTransactionNotFoundError"
        }
    }
}
