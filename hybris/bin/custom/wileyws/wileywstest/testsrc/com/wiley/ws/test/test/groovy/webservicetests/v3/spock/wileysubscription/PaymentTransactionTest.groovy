package com.wiley.ws.test.test.groovy.webservicetests.v3.spock.wileysubscription

import com.wiley.core.enums.PaymentModeEnum
import com.wiley.ws.test.test.groovy.webservicetests.v3.spock.AbstractSpockTransactionalTest
import com.wiley.ws.test.test.groovy.webservicetests.wiley.TestValidationUtils
import de.hybris.bootstrap.annotations.ManualTest
import groovyx.net.http.HttpResponseDecorator
import org.junit.Assert
import spock.lang.Unroll

import static groovyx.net.http.ContentType.JSON
import static groovyx.net.http.ContentType.URLENC
import static org.apache.http.HttpStatus.*

/**
 * Created by Uladzimir_Barouski on 9/23/2016.
 */
/**
 * Spock test for {@link com.wiley.ws.core.v3.controller.WileySubscriptionController}
 */
@ManualTest
@Unroll
class PaymentTransactionTest extends AbstractSpockTransactionalTest {
    static final String WILEYSUBSCRIPTION_CODE = "testSubscriptionCode5";
    static final String WILEYSUBSCRIPTION_CODE_400 = TestValidationUtils.LONG_STRING_256;
    static final String WILEYSUBSCRIPTION_CODE_404 = "notExistedSubscriptionCode";
    protected static final PAYMENT_TRANSACTION_CREATE_REQUEST = [
            "amount"       : '10',
            "currency"     : 'USD',
            "date"         : '2001-07-04T12:08:56'+TIME_ZONE,
            "paymentStatus": 'CAPTURE',
            "paymentType"  : 'CARD',
            "paymentToken" : 'WPG TOKEN'
    ]

    protected static final PAYMENT_TRANSACTION_CREATE_REQUEST_400_PATTERN = [
            "amount"       : '10',
            "currency"     : 'USD',
            "date"         : '2001-07-04T12:08:56'+TIME_ZONE,
            "paymentStatus": 'UNEXPECTED_STATUS',
            "paymentType"  : 'CARD',
            "paymentToken" : 'WPG TOKEN'
    ]

    protected static final PAYMENT_TRANSACTION_CREATE_REQUEST_400_NULL = "{}"

    def "Trusted client requests payment transaction create by Wiley Subscription code"() {
        given: "trusted client"
        authorizeTrustedClient(restClient)

        when: "Trusted client requests payment transaction create by Wiley Subscription code"
        HttpResponseDecorator response = restClient.post(
                path: getBasePath() + "/subscriptions/" + WILEYSUBSCRIPTION_CODE + "/transactions",
                contentType: responseFormat,
                body: postBody,
                requestContentType: requestFormat)

        then: "he receives 201 response with empty body"
        with(response) {
            status == SC_CREATED
            isEmpty(data)

        }
        def query = " select * from {WileySubscriptionPaymentTransaction} where " +
                "{time}=CONVERT_TZ('2001-07-04 12:08:56.0',"+ OFFSET_IN_HOURS+",'+00:00') " +
                "AND {amount}='" + PAYMENT_TRANSACTION_CREATE_REQUEST.amount + "' " +
                "AND {type} in ({{select {PK} from {PaymentTransactionType} where {code}='" + PAYMENT_TRANSACTION_CREATE_REQUEST.paymentStatus + "'}}) " +
                "AND {paymentMode} in ({{select {pk} from {PaymentModeEnum} where {code}='" + PaymentModeEnum.CARD.getCode() + "'}}) " +
                "AND {currency} in ({{select {pk} from {Currency} where {isocode}='" + PAYMENT_TRANSACTION_CREATE_REQUEST.currency + "'}}) " +
                "AND {subscription} in ({{select {pk} from {WileySubscription} where {code}='" + WILEYSUBSCRIPTION_CODE + "'}}) " +
                "AND {paymentToken}='" + PAYMENT_TRANSACTION_CREATE_REQUEST.paymentToken + "'"
        HttpResponseDecorator checkResponse = restClient.post(
                path: getBasePath() + "/flexible-search",
                contentType: requestFormat,
                body: ["query": query],
                requestContentType: URLENC)
        with(checkResponse) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            status == SC_OK
            data["result"] == true
        }

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | PAYMENT_TRANSACTION_CREATE_REQUEST
    }

    def "Trusted client makes malformed request (400, null)"() {
        given: "trusted client"
        authorizeTrustedClient(restClient)

        when: "Trusted client makes malformed request"
        HttpResponseDecorator response = restClient.post(
                path: getBasePath() + "/subscriptions/" + WILEYSUBSCRIPTION_CODE + "/transactions",
                contentType: responseFormat,
                body: postBody,
                requestContentType: requestFormat)

        then: "he receives 400 response with errors"

        with(response) {
            if (isNotEmpty(data) && isEmpty(data.errors)) printJsonData data

            status == SC_BAD_REQUEST
            TestValidationUtils.validateRequiredAttributesErrors(["paymentStatus", "date", "currency", "paymentType", "amount"], response.data.errors)
        }

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | PAYMENT_TRANSACTION_CREATE_REQUEST_400_NULL
    }

    def "Not trusted client makes a request"() {
        given: "Not trusted client"

        when: "Not trusted client makes a request"
        HttpResponseDecorator response = restClient.post(
                path: getBasePath() + "/subscriptions/" + WILEYSUBSCRIPTION_CODE + "/transactions",
                contentType: responseFormat,
                body: postBody,
                requestContentType: requestFormat)

        then: "he receives 401 response with errors"
        with(response) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) {
                status == SC_UNAUTHORIZED
                data["errors"]["message"][0] == "Access is denied"
                data["errors"]["type"][0] == "AccessDeniedError"
            } else {
                Assert.fail();
            }
        }

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | PAYMENT_TRANSACTION_CREATE_REQUEST
    }

    def "Trusted client requests payment transaction for not existed Subscription code"() {
        given: "trusted client"
        authorizeTrustedClient(restClient)

        when: "Trusted client requests payment transaction for not existed Subscription code"
        HttpResponseDecorator response = restClient.post(
                path: getBasePath() + "/subscriptions/" + WILEYSUBSCRIPTION_CODE_404 + "/transactions",
                contentType: responseFormat,
                body: postBody,
                requestContentType: requestFormat)

        then: "he receives 404 response with errors"
        with(response) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) {
                status == SC_NOT_FOUND
                data.errors["message"][0] == "WileySubscription for code  'notExistedSubscriptionCode' not found!"
                data.errors["type"][0] == "WileywsNotFoundError"
            } else {
                Assert.fail();
            }
        }

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | PAYMENT_TRANSACTION_CREATE_REQUEST
    }

    def "Trusted client makes malformed request (400, pattern)"() {
        given: "trusted client"
        authorizeTrustedClient(restClient)

        when: "Trusted client requests payment transaction for not existed Subscription code"
        HttpResponseDecorator response = restClient.post(
                path: getBasePath() + "/subscriptions/" + WILEYSUBSCRIPTION_CODE + "/transactions",
                contentType: responseFormat,
                body: postBody,
                requestContentType: requestFormat)

        then: "he receives 400 response with errors"
        with(response) {
            if (isNotEmpty(data.errors)) {
                status == SC_BAD_REQUEST
                TestValidationUtils.validatePatternErrors(["paymentStatus": "CAPTURE|REFUND"], data.errors)
            } else {
                Assert.fail();
            }
        }

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | PAYMENT_TRANSACTION_CREATE_REQUEST_400_PATTERN
    }

    def "Trusted client makes malformed request (400, path variable size)"() {
        given: "trusted client"
        authorizeTrustedClient(restClient)

        when: "Trusted client requests payment transaction for not existed Subscription code"
        HttpResponseDecorator response = restClient.post(
                path: getBasePath() + "/subscriptions/" + WILEYSUBSCRIPTION_CODE_400 + "/transactions",
                contentType: responseFormat,
                body: postBody,
                requestContentType: requestFormat)

        then: "he receives 400 response with errors"
        with(response) {
            if (isNotEmpty(data.errors)) {
                status == SC_BAD_REQUEST
                data.errors[0]["type"] == "MethodConstraintViolationError"
            } else {
                Assert.fail();
            }
        }

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | PAYMENT_TRANSACTION_CREATE_REQUEST
    }
}

