package com.wiley.ws.test.test.groovy.webservicetests.v2.spock

import com.wiley.ws.test.test.groovy.webservicetests.v2.spock.access.AccessRightsTest
import com.wiley.ws.test.test.groovy.webservicetests.v2.spock.access.OAuth2Test
import com.wiley.ws.test.test.groovy.webservicetests.v2.spock.carts.*
import com.wiley.ws.test.test.groovy.webservicetests.v2.spock.catalogs.CatalogsResourceTest
import com.wiley.ws.test.test.groovy.webservicetests.v2.spock.customergroups.CustomerGroupsTest
import com.wiley.ws.test.test.groovy.webservicetests.v2.spock.errors.ErrorTest
import com.wiley.ws.test.test.groovy.webservicetests.v2.spock.export.ExportTest
import com.wiley.ws.test.test.groovy.webservicetests.v2.spock.filters.CartMatchingFilterTest
import com.wiley.ws.test.test.groovy.webservicetests.v2.spock.filters.UserMatchingFilterTest
import com.wiley.ws.test.test.groovy.webservicetests.v2.spock.flows.AddressBookFlow
import com.wiley.ws.test.test.groovy.webservicetests.v2.spock.flows.CartFlowTest
import com.wiley.ws.test.test.groovy.webservicetests.v2.spock.general.StateTest
import com.wiley.ws.test.test.groovy.webservicetests.v2.spock.misc.*
import com.wiley.ws.test.test.groovy.webservicetests.v2.spock.orders.OrdersTest
import com.wiley.ws.test.test.groovy.webservicetests.v2.spock.products.ProductResourceTest
import com.wiley.ws.test.test.groovy.webservicetests.v2.spock.products.ProductsStockTest
import com.wiley.ws.test.test.groovy.webservicetests.v2.spock.promotions.PromotionsTest
import com.wiley.ws.test.test.groovy.webservicetests.v2.spock.stores.StoresTest
import com.wiley.ws.test.test.groovy.webservicetests.v2.spock.ui.guitests.OauthRequestTest
import com.wiley.ws.test.test.groovy.webservicetests.v2.spock.users.*
import org.junit.BeforeClass
import org.junit.runner.RunWith
import org.junit.runners.Suite

@RunWith(Suite.class)
@Suite.SuiteClasses([AccessRightsTest, OAuth2Test, StateTest, CartDeliveryTest, CartMergeTest, CartEntriesTest, CartPromotionsTest,
        CartResourceTest, CartVouchersTest, GuestsTest, OrderPlacementTest, CatalogsResourceTest, CustomerGroupsTest, ErrorTest, ExportTest,
        AddressBookFlow, CartFlowTest, CardTypesTest, CurrenciesTest, DeliveryCountriesTest, LanguagesTest, TitlesTest, OrdersTest, ProductResourceTest, ProductsStockTest, PromotionsTest, SavedCartTest, SavedCartFullScenarioTest, StoresTest, UserAccountTest,
        UserAddressTest, UserOrdersTest, UserPaymentsTest, UsersResourceTest, CartMatchingFilterTest, UserMatchingFilterTest, OauthRequestTest])
class AllSpockTests {
    @BeforeClass
    public static void setUpClass() {
        //dummy setup class, if its not provided parent class is not created
    }
}
