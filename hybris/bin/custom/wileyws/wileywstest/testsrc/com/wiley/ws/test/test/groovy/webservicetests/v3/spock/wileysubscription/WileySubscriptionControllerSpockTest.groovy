package com.wiley.ws.test.test.groovy.webservicetests.v3.spock.wileysubscription

import com.wiley.ws.test.test.groovy.webservicetests.v3.spock.AbstractSpockTransactionalTest
import com.wiley.ws.test.test.groovy.webservicetests.wiley.TestValidationUtils
import de.hybris.bootstrap.annotations.ManualTest
import groovy.json.JsonSlurper
import groovyx.net.http.HttpResponseDecorator
import spock.lang.Unroll

import static groovyx.net.http.ContentType.JSON
import static org.apache.http.HttpStatus.*

/**
 * Default spock test for {@link com.wiley.ws.core.v3.controller.WileySubscriptionController}
 */
@ManualTest
@Unroll
class WileySubscriptionControllerSpockTest extends AbstractSpockTransactionalTest {

    static final String EXISTING_WILEYSUBSCRIPTION_CODE = "testSubscriptionCode1";
    static final String EXISTING_WILEYSUBSCRIPTION_CODE_400 = TestValidationUtils.LONG_STRING_256;
    static final String EXTERNAL_CODE = "EXTERNAL_CODE";
    static final String STRING_255_SYMBOLS = "EXTERNAL_CODE".padLeft(255, "_");
    static final String TOO_LONG_EXTERNAL_CODE = "EXTERNAL_CODE".padLeft(257, "_");

    private static final WILEY_CREATE_SUBSCRIPTION_REQUEST_BODY_200 = [
            "externalCode"         : "12341234",
            "isbn"                 : "9780471000053",
            "orderId"              : "orderWithSubscription",
            "subscriptionStatus"   : "ACTIVE",
            "subscriptionStartDate": "2013-09-29T18:46:19-0700",
            "subscriptionEndDate"  : "2013-09-29T18:46:19-0700",
            "nextBillingDate"      : "2013-09-29T18:46:19-0700",
            "billingPriceValue"    : 25.55,
            "billingPriceCurrency" : "USD",
            "billingFrequency"     : "FREE",
            "contractDurationValue": 5,
            "contractDurationUnit" : "DAY",
            "renewable"            : false,
            "paymentInfo"          : [
                    "paymentType"           : "CARD",
                    "paymentToken"          : "325435435435",
                    "creditCardType"        : "VISA",
                    "creditCardValidToMonth": 6,
                    "creditCardValidToYear" : 2020,
                    "paymentAddress"        : [
                            "postcode"   : "123123",
                            "country"    : "US",
                            "state"      : "CA",
                            "city"       : "SomeCity",
                            "line1"      : "Line 1",
                            "line2"      : "Line 2",
                            "firstName"  : "First name",
                            "lastName"   : "Last name",
                            "phoneNumber": "1231231235555"
                    ]
            ],
            "shippingAddress"      : [
                    "postcode"   : "45454545",
                    "country"    : "US",
                    "state"      : "CA",
                    "city"       : "AnotherCity",
                    "line1"      : "Another Line 1",
                    "line2"      : "Another Line 2",
                    "firstName"  : "Another First name",
                    "lastName"   : "Another Last name",
                    "phoneNumber": "333343554"
            ]
    ]

    private static final WILEY_CREATE_SUBSCRIPTION_REQUEST_BODY_400_NULL_VALIDATION = "{}"

    private static final WILEY_CREATE_SUBSCRIPTION_REQUEST_BODY_400_SIZE_VALIDATION = [
            "externalCode"         : "12341234",
            "isbn"                 : "isbn".padLeft(256, "_"),
            "orderId"              : "orderId".padLeft(256, "_"),
            "subscriptionStatus"   : "ACTIVE",
            "subscriptionStartDate": "2013-09-29T18:46:19-0700",
            "subscriptionEndDate"  : "2013-09-29T18:46:19-0700",
            "nextBillingDate"      : "2013-09-29T18:46:19-0700",
            "billingPriceValue"    : 25.55,
            "billingPriceCurrency" : "USD",
            "billingFrequency"     : "FREE",
            "contractDurationValue": 5,
            "contractDurationUnit" : "DAY",
            "renewable"            : false,
            "paymentInfo"          : [
                    "paymentType"           : "CARD",
                    "paymentToken"          : "325435435435",
                    "creditCardType"        : "VISA",
                    "creditCardValidToMonth": 6,
                    "creditCardValidToYear" : 2020,
                    "paymentAddress"        : [
                            "postcode"   : "123123",
                            "country"    : "US",
                            "state"      : "CA",
                            "city"       : "SomeCity",
                            "line1"      : "Line 1",
                            "line2"      : "Line 2",
                            "firstName"  : "First name",
                            "lastName"   : "Last name",
                            "phoneNumber": "1231231235555"
                    ]
            ],
            "shippingAddress"      : [
                    "postcode"   : "45454545",
                    "country"    : "US",
                    "state"      : "CA",
                    "city"       : "AnotherCity",
                    "line1"      : "Another Line 1",
                    "line2"      : "Another Line 2",
                    "firstName"  : "Another First name",
                    "lastName"   : "Another Last name",
                    "phoneNumber": "333343554"
            ]
    ]

    private static final WILEY_UPDATE_SUBSCRIPTION_REQUEST_BODY = [
            "externalCode"         : "12341234",
            "subscriptionStatus"   : "ACTIVE",
            "subscriptionStartDate": "2013-09-29T18:46:19-0700",
            "subscriptionEndDate"  : "2013-09-29T18:46:19-0700",
            "nextBillingDate"      : "2013-09-29T18:46:19-0700",
            "billingPriceValue"    : 25.55,
            "billingPriceCurrency" : "USD",
            "billingFrequency"     : "FREE",
            "contractDurationValue": 5,
            "contractDurationUnit" : "DAY",
            "renewable"            : false,
            "paymentInfo"          : [
                    "paymentType"           : "CARD",
                    "paymentToken"          : "325435435435",
                    "creditCardType"        : "VISA",
                    "creditCardValidToMonth": 6,
                    "creditCardValidToYear" : 2020,
                    "paymentAddress"        : [
                            "postcode"   : "123123",
                            "country"    : "US",
                            "state"      : "CA",
                            "city"       : "SomeCity",
                            "line1"      : "Line 1",
                            "line2"      : "Line 2",
                            "firstName"  : "First name",
                            "lastName"   : "Last name",
                            "phoneNumber": "1231231235555"
                    ]
            ],
            "shippingAddress"      : [
                    "postcode"   : "45454545",
                    "country"    : "US",
                    "state"      : "CA",
                    "city"       : "AnotherCity",
                    "line1"      : "Another Line 1",
                    "line2"      : "Another Line 2",
                    "firstName"  : "Another First name",
                    "lastName"   : "Another Last name",
                    "phoneNumber": "333343554"
            ]
    ]

    private static final WILEY_UPDATE_SUBSCRIPTION_REQUEST_BODY_400_SIZE_VALIDATION = [
            "externalCode"         : "externalCode".padLeft(256, "_"),
            "subscriptionStatus"   : "ACTIVE",
            "subscriptionStartDate": "2013-09-29T18:46:19-0700",
            "subscriptionEndDate"  : "2013-09-29T18:46:19-0700",
            "nextBillingDate"      : "2013-09-29T18:46:19-0700",
            "billingPriceValue"    : 25.55,
            "billingPriceCurrency" : "USD",
            "billingFrequency"     : "FREE",
            "contractDurationValue": 5,
            "contractDurationUnit" : "DAY",
            "renewable"            : false,
            "paymentInfo"          : [
                    "paymentType"           : "CARD",
                    "paymentToken"          : "325435435435",
                    "creditCardType"        : "VISA",
                    "creditCardValidToMonth": 6,
                    "creditCardValidToYear" : 2020,
                    "paymentAddress"        : [
                            "postcode"   : "123123",
                            "country"    : "US",
                            "state"      : "CA",
                            "city"       : "SomeCity",
                            "line1"      : "Line 1",
                            "line2"      : "Line 2",
                            "firstName"  : "First name",
                            "lastName"   : "Last name",
                            "phoneNumber": "1231231235555"
                    ]
            ],
            "shippingAddress"      : [
                    "postcode"   : "45454545",
                    "country"    : "US",
                    "state"      : "CA",
                    "city"       : "AnotherCity",
                    "line1"      : "Another Line 1",
                    "line2"      : "Another Line 2",
                    "firstName"  : "Another First name",
                    "lastName"   : "Another Last name",
                    "phoneNumber": "333343554"
            ]
    ]

    def "Trusted client requests WileySubscription creating (200 case): #requestFormat"() {
        given: "trusted client"
        authorizeTrustedClient(restClient)

        when: "trusted client requests WileySubscriptionCreate create"
        HttpResponseDecorator response = restClient.post(
                path: getBasePath() + "/subscriptions",
                contentType: responseFormat,
                body: postBody,
                requestContentType: requestFormat)

        then: "he receives 200 response with internalSubscriptionId"
        with(response) {
            if (isNotEmpty(data.errors)) println(data)
            status == SC_OK
            data.internalSubscriptionId =~ /WS-\w{8}/;
        }

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | WILEY_CREATE_SUBSCRIPTION_REQUEST_BODY_200
    }

    def "Trusted client requests WileySubscription creating (400 case, null validation exception): #requestFormat"() {
        given: "trusted client"
        authorizeTrustedClient(restClient)

        when: "trusted client requests WileySubscriptionCreate create"
        HttpResponseDecorator response = restClient.post(
                path: getBasePath() + "/subscriptions",
                contentType: responseFormat,
                body: postBody,
                requestContentType: requestFormat)

        then: "he receives 400 response with size validation errors"
        with(response) {
            if (isEmpty(data.errors)) println(data)
            status == SC_BAD_REQUEST
            TestValidationUtils.validateRequiredAttributesErrors(["contractDurationUnit", "subscriptionStartDate", "contractDurationValue", "isbn", "subscriptionStatus", "externalCode", "subscriptionEndDate", "orderId"], data.errors)
        }

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | WILEY_CREATE_SUBSCRIPTION_REQUEST_BODY_400_NULL_VALIDATION
    }

    def "Trusted client requests WileySubscription creating (400 case, size validation exception): #requestFormat"() {
        given: "trusted client"
        authorizeTrustedClient(restClient)

        when: "trusted client requests WileySubscriptionCreate create"
        HttpResponseDecorator response = restClient.post(
                path: getBasePath() + "/subscriptions",
                contentType: responseFormat,
                body: postBody,
                requestContentType: requestFormat)

        then: "he receives 400 response with size validation errors"
        with(response) {
            if (isEmpty(data.errors)) println(data)
            status == SC_BAD_REQUEST
            TestValidationUtils.validateSizeErrors(["isbn": [
                    "min": 0,
                    "max": 255
            ], "orderId"                                  : [
                    "min": 0,
                    "max": 255
            ]], data.errors)
        }

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | WILEY_CREATE_SUBSCRIPTION_REQUEST_BODY_400_SIZE_VALIDATION
    }

    def "Trusted client requests WileySubscription updating: #requestFormat"() {
        given: "trusted client"
        authorizeTrustedClient(restClient)

        when: "trusted client requests WileySubscriptionCreate update"
        HttpResponseDecorator response = restClient.post(
                path: getBasePath() + "/subscriptions/" + EXISTING_WILEYSUBSCRIPTION_CODE,
                contentType: responseFormat,
                body: postBody,
                requestContentType: requestFormat)

        then: "he receives 204 response with empty body"
        with(response) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            status == SC_NO_CONTENT
            isEmpty(data)
        }

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | WILEY_UPDATE_SUBSCRIPTION_REQUEST_BODY
    }

    def "Trusted client requests WileySubscription updating when requested subscription doesn't exist: #requestFormat"() {
        given: "trusted client"
        authorizeTrustedClient(restClient)

        when: "trusted client requests WileySubscriptionCreate update"
        HttpResponseDecorator response = restClient.post(
                path: getBasePath() + "/subscriptions/" + "not_existing_subscription_code",
                contentType: responseFormat,
                body: postBody,
                requestContentType: requestFormat)

        then: "he receives 404 response with empty body"
        with(response) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            status == SC_NOT_FOUND
            isEmpty(data)
        }

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | WILEY_UPDATE_SUBSCRIPTION_REQUEST_BODY
    }

    def "Trusted client requests WileySubscription updating (400 case, size validation exception): #requestFormat"() {
        given: "trusted client"
        authorizeTrustedClient(restClient)

        when: "trusted client requests WileySubscriptionCreate update"
        HttpResponseDecorator response = restClient.post(
                path: getBasePath() + "/subscriptions/" + EXISTING_WILEYSUBSCRIPTION_CODE,
                contentType: responseFormat,
                body: postBody,
                requestContentType: requestFormat)

        then: "he receives 400 response with size validation errors"
        with(response) {
            if (isEmpty(data.errors)) println(data)
            status == SC_BAD_REQUEST
            TestValidationUtils.validateSizeErrors(["externalCode": [
                    "min": 0,
                    "max": 255
            ]], data.errors)
        }

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | WILEY_UPDATE_SUBSCRIPTION_REQUEST_BODY_400_SIZE_VALIDATION
    }

    def "Trusted client requests WileySubscription updating (400, path variable size): #requestFormat"() {
        given: "trusted client"
        authorizeTrustedClient(restClient)

        when: "trusted client requests WileySubscriptionCreate update"
        HttpResponseDecorator response = restClient.post(
                path: getBasePath() + "/subscriptions/" + EXISTING_WILEYSUBSCRIPTION_CODE_400,
                contentType: responseFormat,
                body: postBody,
                requestContentType: requestFormat)

        then: "he receives 400 response with validation error"
        with(response) {
            if (isEmpty(data.errors)) println(data)
            status == SC_BAD_REQUEST
            data.errors[0]["type"] == "MethodConstraintViolationError"
        }

        where:
        requestFormat | responseFormat | postBody
        JSON          | JSON           | WILEY_UPDATE_SUBSCRIPTION_REQUEST_BODY
    }

    def "Trusted client requests WileySubscriptions: "() {
        given: "trusted client"
        authorizeTrustedClient(restClient)

        when: "trusted client requests WileySubscriptions"
        HttpResponseDecorator response = restClient.get(
                path: getBasePath() + "/subscriptions",
                query: [
                        externalCode: EXTERNAL_CODE
                ],
                contentType: responseFormat,
                requestContentType: requestFormat)

        then: "he receives 200 response and two corresponding records"
        with(response) {
            if (status != SC_OK && isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            status == SC_OK
            def element1 = new JsonSlurper().parseText('{ "internalSubscriptionId" : "testSubscriptionCode1" }');
            def element2 = new JsonSlurper().parseText('{ "internalSubscriptionId" : "testSubscriptionCode4" }');
            TestValidationUtils.validateRequiredResponseElements([element1, element2], data)
        }

        where:
        requestFormat | responseFormat
        JSON          | JSON
    }

    def "Trusted client requests WileySubscriptions with no such external code: "() {
        given: "trusted client"
        authorizeTrustedClient(restClient)

        when: "trusted client requests WileySubscriptions"
        HttpResponseDecorator response = restClient.get(
                path: getBasePath() + "/subscriptions",
                query: [
                        externalCode: 'NO_SUCH_CODE'
                ],
                contentType: responseFormat,
                requestContentType: requestFormat)

        then: "he receives 200 response and zero records"
        with(response) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            isEmpty(data)
            status == SC_OK
        }

        where:
        requestFormat | responseFormat
        JSON          | JSON
    }

    def "Trusted client requests WileySubscriptions with long but valid external code: "() {
        given: "trusted client"
        authorizeTrustedClient(restClient)

        when: "trusted client requests WileySubscriptions"
        HttpResponseDecorator response = restClient.get(
                path: getBasePath() + "/subscriptions",
                query: [
                        externalCode: STRING_255_SYMBOLS
                ],
                contentType: responseFormat,
                requestContentType: requestFormat)

        then: "he receives 200 response and zero records"
        with(response) {
            if (isNotEmpty(data) && isNotEmpty(data.errors)) println(data)
            isEmpty(data)
            status == SC_OK
        }

        where:
        requestFormat | responseFormat
        JSON          | JSON
    }

    def "Trusted client requests WileySubscriptions without external code: "() {
        given: "trusted client"
        authorizeTrustedClient(restClient)

        when: "trusted client requests WileySubscriptions"
        HttpResponseDecorator response = restClient.get(
                path: getBasePath() + "/subscriptions",
                contentType: responseFormat,
                requestContentType: requestFormat)

        then: "he receives 400 response and MissingServletRequestParameterError"
        with(response) {
            if (isNotEmpty(data) && isEmpty(data.errors)) println(data)
            isNotEmpty(data.errors)
            data.errors[0].type == 'MissingServletRequestParameterError'
            status == SC_BAD_REQUEST
        }

        where:
        requestFormat | responseFormat
        JSON          | JSON
    }

    def "Trusted client requests WileySubscriptions with too long external code: "() {
        given: "trusted client"
        authorizeTrustedClient(restClient)

        when: "trusted client requests WileySubscriptions"
        HttpResponseDecorator response = restClient.get(
                path: getBasePath() + "/subscriptions",
                query: [
                        externalCode: TOO_LONG_EXTERNAL_CODE
                ],
                contentType: responseFormat,
                requestContentType: requestFormat)

        then: "he receives 400 response and WileywsValidationError"
        with(response) {
            if (isNotEmpty(data) && isEmpty(data.errors)) println(data)
            isNotEmpty(data.errors)
            data.errors[0].type == 'MethodConstraintViolationError'
            status == SC_BAD_REQUEST
        }

        where:
        requestFormat | responseFormat
        JSON          | JSON
    }

}
