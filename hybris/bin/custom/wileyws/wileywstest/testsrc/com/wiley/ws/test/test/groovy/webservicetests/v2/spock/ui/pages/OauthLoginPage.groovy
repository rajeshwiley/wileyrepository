/**
 *
 */
package com.wiley.ws.test.test.groovy.webservicetests.v2.spock.ui.pages

import com.wiley.ws.test.test.groovy.webservicetests.v2.spock.AbstractGUITest
import geb.Page
import org.apache.log4j.Logger

/**
 * @author monika.morawiecka
 *
 */
class OauthLoginPage extends Page {

    final static Logger LOGGER = Logger.getLogger(OauthLoginPage.class.getName());

    static url = AbstractGUITest.getHttpsWebroot() + "/login.jsp"
    static at = {
        LOGGER.debug("at " + OauthAllowRequestPage.class.getName());
        waitFor { url == AbstractGUITest.getHttpsWebroot() + "/login.jsp" }
    }
    static content = {
        userField { $("input", name: contains("j_username")) }
        passwordField { $("input", name: contains("j_password")) }
        loginButton(to: OauthAllowRequestPage, toWait: true) { $("input", type: "submit") }
    }

    void login(String user, password) {
        LOGGER.debug("Logging in...")
        waitFor { userField }
        userField.value user
        waitFor { passwordField }
        passwordField.value password
        waitFor { loginButton.displayed }
        loginButton.click()
    }
}
