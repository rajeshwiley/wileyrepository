package com.wiley.ws.test.test.groovy.webservicetests

class DocumentationSuiteGuard {
    private static boolean isSuiteRunning = false;

    public static boolean isSuiteRunning() {
        return isSuiteRunning;
    }

    public static void setSuiteRunning(boolean isSuiteRunning) {
        this.isSuiteRunning = isSuiteRunning;
    }
}
