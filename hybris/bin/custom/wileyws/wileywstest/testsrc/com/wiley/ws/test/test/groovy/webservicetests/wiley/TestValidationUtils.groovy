package com.wiley.ws.test.test.groovy.webservicetests.wiley

import com.wiley.ws.test.test.groovy.webservicetests.v3.spock.AbstractSpockTest
import groovy.json.JsonOutput
import org.apache.commons.lang3.StringUtils

class TestValidationUtils {
    public static final String SAP_ACCOUNT_NUMBER = "testSapAccountNumber"
    public static final String SAP_ACCOUNT_NUMBER_NOT_EXISTED = "blabla1234"
    public static final String LONG_STRING_255 = StringUtils.repeat("a", 255)
    public static final String LONG_STRING_256 = StringUtils.repeat("a", 256)
    public static final String LONG_STRING_150 = StringUtils.repeat("a", 150)
    public static final String LONG_STRING_151 = StringUtils.repeat("a", 151)

    public static final String VALIDATION_RULE_SIZE_255 = "maxLength: 255"
    public static final String VALIDATION_RULE_SIZE_150 = "maxLength: 150"
    public static final String VALIDATION_RULE_BOOLEAN = "is boolean"
    public static final String VALIDATION_RULE_POSITIVE_INT = "is int32"
    public static final String PATTERN_COUNTRY = "pattern: ^[A-Z]{2}\$"
    public static final String PATTERN_STATE = "pattern: ^[0-9A-Z]{2}\$"
    public static final String PATTERN_LOCALE = "pattern: ^[a-z]{2}(_[A-Z]{2}(_.+)?)?\$"
    public static final String PATTERN_LANGUAGE = "pattern: ^[a-z]{2}\$"
    public static final String PATTERN_CURRENCY = "pattern: ^[a-z]{2}\$"

    /**
     * Generates json from map  
     * @param map
     * @return
     */
    def static String generate(Map map) {
        return JsonOutput.toJson(map).toString()
    }

    def static String updateFieldAndGenerateBody(Map map, Closure path, Map updates) {
        return generate(updateField(map, path, updates))
    }

    /**
     * Method is used to update some fields of map
     * @param map - root map
     * @param path - closure allows access subMap to be updated
     * @param updates - map represents fields should be updated
     * @return - updated rootMap 
     */
    def static Map updateField(Map map, Closure<Map> path, Map updates) {
        updateOrRemoveIfNull(path(map), updates)
        return map
    }

    def static Map updateOrRemoveIfNull(Map map, Map updates) {
        updates.forEach { key, value -> updateOrRemoveIfNull(map, key, value) }
        return map
    }

    /**
     * Updates filedValue for key if it isn't null, or remove otherwise 
     * @param map
     * @param fieldName
     * @param fieldValue
     * @return
     */
    def static Map updateOrRemoveIfNull(Map map, fieldName, fieldValue) {
        if (fieldValue == null) {
            map.remove(fieldName)
        } else {
            map.put(fieldName, fieldValue)
        }
        map
    }

    def static boolean validateRequiredAttributesErrors(List<String> requiredAttributes, responseErrors) {
        def result

        if(requiredAttributes.size() < responseErrors.size()) {
            result = false
            println 'Extra errors found in response. Required attributes are: ' + requiredAttributes
        } else {
            result = requiredAttributes.every { requiredAttribute ->
                def found =  responseErrors.any { error ->
                    return checkValidationEntry(error, requiredAttribute, 'The attribute [' + requiredAttribute + '] must not be null.', 'missing', 'parameter')
                }
                if (!found) println 'Error for required attribute [' + requiredAttribute + '] not found in response errors.'
                return found
            }
        }
        if (!result) AbstractSpockTest.printJsonData(responseErrors)
        return result
    }

    def static boolean validateSizeErrors(expectedErrorsForAttributes, responseErrors) {
        def result

        if(expectedErrorsForAttributes.size() < responseErrors.size()) {
            result = false
            println 'Extra errors found in response. Required attributes are: ' + expectedErrorsForAttributes
        } else {
            result = checkSizeErrors(expectedErrorsForAttributes, responseErrors);
        }
        if (!result) AbstractSpockTest.printJsonData(responseErrors)
        return result
    }

    def static boolean validatePatternErrors(expectedErrorsForAttributes, responseErrors) {
        def result

        if(expectedErrorsForAttributes.size() < responseErrors.size()) {
            result = false
            println 'Extra errors found in response. Required attributes are: ' + expectedErrorsForAttributes
        } else {
            result = checkPatternErrors(expectedErrorsForAttributes, responseErrors);
        }
        if (!result) AbstractSpockTest.printJsonData(responseErrors)
        return result
    }

    def static boolean validateRequiredResponseElements(requiredResponseElements, responseData) {
        def result

        if(requiredResponseElements.size() < responseData.size()) {
            result = false
            println 'Extra response elements found in response. Required are: ' + requiredResponseElements
        } else {
            result = requiredResponseElements.every { requiredResponseElement ->
                def found =  responseData.any { responseElement ->
                    return (responseElement == requiredResponseElement)
                }
                if (!found) println 'Required response element [' + requiredResponseElement + '] not found in response.'
                return found
            }
        }
        if (!result) AbstractSpockTest.printJsonData(responseData)
        return result
    }

    private static checkPatternErrors(expectedErrorsForAttributes, responseErrors) {
        for (expectedEntry in expectedErrorsForAttributes){
            def found =  responseErrors.any { error ->
                return checkValidationEntry(error, expectedEntry.key, 'The attribute [' + expectedEntry.key + '] must match the Regular Expression [' + expectedEntry.value + '].', 'invalid', 'parameter')
            }
            if (!found) println 'Error for required attribute [' + expectedEntry.key + '] not found in response errors.'
            return found
        }
        return false;
    }

    private static boolean checkSizeErrors(expectedErrorsForAttributes, responseErrors) {
        for (expectedEntry in expectedErrorsForAttributes){
            def found =  responseErrors.any { error ->
                return checkValidationEntry(error, expectedEntry.key, 'The number of characters in the attribute [' + expectedEntry.key + '] must between ' + expectedEntry.value.max + ' and ' + expectedEntry.value.min + ' (inclusive).', 'invalid', 'parameter')
            }
            if (!found) println 'Error for required attribute [' + expectedEntry.key + '] not found in response errors.'
            return found
        }
        return false;
    }

    private static boolean checkValidationEntry(error, subject, message, reason, subjectType, type) {
        return (error['subject'] == subject &&
                error['message'] == message &&
                error['reason'] == reason &&
                error['subjectType'] == subjectType &&
                error['type'] == type)
    }

    private static boolean checkValidationEntry(error, subject, message, reason, subjectType) {
        return checkValidationEntry(error, subject, message, reason, subjectType, 'ValidationError');
    }
}
