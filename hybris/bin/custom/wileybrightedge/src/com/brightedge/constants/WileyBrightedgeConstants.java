/*
 * BrightEdge Hybris Extension
 *
 * Copyright (c) 2015 BrightEdge Technologies 
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.brightedge.constants;

/**
 * Global class for all Brightedge constants. You can add global constants for your extension into this class.
 */
public final class WileyBrightedgeConstants extends GeneratedWileyBrightedgeConstants
{
	public static final String EXTENSIONNAME = "wileybrightedge";

	private WileyBrightedgeConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
