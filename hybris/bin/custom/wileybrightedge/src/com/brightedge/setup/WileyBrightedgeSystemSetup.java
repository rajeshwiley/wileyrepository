package com.brightedge.setup;

import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;

import java.util.Collections;
import java.util.List;

import com.brightedge.constants.WileyBrightedgeConstants;


/**
 * Import Brightedge specific report data for report cockpit.
 */
@SystemSetup(extension = WileyBrightedgeConstants.EXTENSIONNAME)
public class WileyBrightedgeSystemSetup extends AbstractSystemSetup
{
	@SystemSetup(type = SystemSetup.Type.PROJECT, process = SystemSetup.Process.ALL)
	public void createEssentialData(final SystemSetupContext context)
	{
		importImpexFile(context, "/jasperreports/jasperreports.impex");
	}

	@Override
	public List<SystemSetupParameter> getInitializationOptions()
	{
		return Collections.emptyList();
	}
}
