package com.brightedge.jalo;

import com.brightedge.constants.WileyBrightedgeConstants;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;

@SuppressWarnings("PMD")
public class WileyBrightedgeManager extends GeneratedWileyBrightedgeManager
{

	public static final WileyBrightedgeManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (WileyBrightedgeManager) em.getExtension(WileyBrightedgeConstants.EXTENSIONNAME);
	}

}
