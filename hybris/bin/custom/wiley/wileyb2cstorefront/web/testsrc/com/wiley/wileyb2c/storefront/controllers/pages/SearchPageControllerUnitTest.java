/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wileyb2c.storefront.controllers.pages;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractSearchPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractSearchPageController.ShowMode;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.ProductSearchFacade;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.search.facetdata.BreadcrumbData;
import de.hybris.platform.commerceservices.search.facetdata.FacetData;
import de.hybris.platform.commerceservices.search.facetdata.ProductSearchPageData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.Model;

import com.wiley.core.search.facetdata.Wileyb2cContentSearchPageData;
import com.wiley.facades.content.ContentData;
import com.wiley.facades.wileyb2c.search.solrfacetsearch.Wileyb2cContentSearchFacade;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SearchPageControllerUnitTest
{
	private static final int DEFAULT_PAGE_SIZE = 20;
	private static final int TEST_PAGE = 2;
	private static final String TEST_SEARCH_QUERY = "test search query";
	private static final String TEST_FACET_CODE = "test facet code";
	private static final String TEST_SORT_CODE = "test sort code";
	private static final ShowMode TEST_SHOW_MODE = ShowMode.Page;

	@Mock
	private Model model;

	@Mock(answer = Answers.RETURNS_DEEP_STUBS)
	private SearchPageData<?> searchPageData;

	// NOPMD : The variable is required for the test(used in the AbstractSearchPageController).
	@Mock
	private SiteConfigService siteConfigService;

	@Mock
	private ConfigurationService configurationService;

	@Mock
	private PageableData pageableDataMock;

	@Mock
	private SearchStateData searchStateDataMock;

	@Mock
	private ProductSearchPageData<SearchStateData, ProductData> productSearchPageDataMock, encodedProductSearchPageDataMock;

	@Mock
	private Wileyb2cContentSearchPageData<SearchStateData, ContentData> wileyb2cContentSearchPageDataMock,
			encodedWileyb2cContentSearchPageDataMock;

	@Mock
	private ProductSearchFacade<ProductData> productSearchFacadeMock;

	@Mock
	private Wileyb2cContentSearchFacade wileyb2cContentSearchFacadeMock;

	@Mock
	private FacetData<SearchStateData> refinedFacetMock;

	@Mock
	private List<FacetData<SearchStateData>> facetMocks, refinedFacetMocks;

	@Mock
	private Map<String, FacetData<SearchStateData>> selectedFacetMocks;

	@Mock
	private List<BreadcrumbData<SearchStateData>> breadcrumbMocks;

	@Spy
	@InjectMocks
	private final TestableSearchPageController controller = new TestableSearchPageController();

	@Before
	public void prepare()
	{
		controller.setProductSearchFacade(productSearchFacadeMock);
		refinedFacetMocks = Arrays.asList(refinedFacetMock);
	}

	//click http://electronics.local:9001/wileyb2cstorefront/Open-Catalogue/Cameras/Digital-Cameras/c/575
	@Test
	public void testPagedDataFlagsCallShowAllOver100ResultsOneDefaultPage()
	{
		BDDMockito.given(Integer.valueOf(searchPageData.getPagination().getNumberOfPages())).willReturn(Integer.valueOf(2));
		BDDMockito.given(Long.valueOf(searchPageData.getPagination().getTotalNumberOfResults()))
				.willReturn(Long.valueOf(AbstractSearchPageController.MAX_PAGE_LIMIT + 1));
		BDDMockito.given(Integer.valueOf(searchPageData.getPagination().getPageSize()))
				.willReturn(Integer.valueOf(DEFAULT_PAGE_SIZE));

		controller.populateModel(model, searchPageData, ShowMode.Page);

		Mockito.verify(model).addAttribute("searchPageData", searchPageData);
		Mockito.verify(model).addAttribute("isShowAllAllowed", Boolean.FALSE);
		Mockito.verify(model).addAttribute("isShowPageAllowed", Boolean.FALSE);
	}

	// click
	// http://electronics.local:9001/wileyb2cstorefront/Open-Catalogue/Cameras/
	// Digital-Cameras/c/575?q=:topRated:category:576
	@Test
	public void testPagedDataFlagsCallShowAllBelow100ResultsOneDefaultPage()
	{
		BDDMockito.given(Integer.valueOf(searchPageData.getPagination().getNumberOfPages())).willReturn(Integer.valueOf(2));
		BDDMockito.given(Long.valueOf(searchPageData.getPagination().getTotalNumberOfResults()))
				.willReturn(Long.valueOf(AbstractSearchPageController.MAX_PAGE_LIMIT - 1));
		BDDMockito.given(Integer.valueOf(searchPageData.getPagination().getPageSize()))
				.willReturn(Integer.valueOf(DEFAULT_PAGE_SIZE));

		controller.populateModel(model, searchPageData, ShowMode.Page);

		Mockito.verify(model).addAttribute("searchPageData", searchPageData);
		Mockito.verify(model).addAttribute("isShowAllAllowed", Boolean.TRUE);
		Mockito.verify(model).addAttribute("isShowPageAllowed", Boolean.FALSE);
	}

	// click
	// http://electronics.local:9001/wileyb2cstorefront/Open-Catalogue/Cameras/
	// Digital-Cameras/c/575?q=:topRated:category:576&show=All
	@Test
	public void testAlldDataFlagsCallShowAllBelow100ResultsOneDefaultPage()
	{
		BDDMockito.given(Integer.valueOf(searchPageData.getPagination().getNumberOfPages())).willReturn(Integer.valueOf(1));
		BDDMockito.given(Long.valueOf(searchPageData.getPagination().getTotalNumberOfResults()))
				.willReturn(Long.valueOf(AbstractSearchPageController.MAX_PAGE_LIMIT - 1));
		BDDMockito.given(Integer.valueOf(searchPageData.getPagination().getPageSize()))
				.willReturn(Integer.valueOf(AbstractSearchPageController.MAX_PAGE_LIMIT));

		controller.populateModel(model, searchPageData, ShowMode.All);

		Mockito.verify(model).addAttribute("searchPageData", searchPageData);
		Mockito.verify(model).addAttribute("isShowAllAllowed", Boolean.FALSE);
		Mockito.verify(model).addAttribute("isShowPageAllowed", Boolean.TRUE);
	}

	// click
	// http://electronics.local:9001/wileyb2cstorefront/Open-Catalogue/Cameras/
	// Digital-Cameras/c/575?q=:topRated:category:576&show=All
	@Test
	public void testAllDataFlagsCallShowAllBelowDefaultPageSizeResultsOneDefaultPage()
	{
		BDDMockito.given(Integer.valueOf(searchPageData.getPagination().getNumberOfPages())).willReturn(Integer.valueOf(1));
		BDDMockito.given(Long.valueOf(searchPageData.getPagination().getTotalNumberOfResults()))
				.willReturn(Long.valueOf(DEFAULT_PAGE_SIZE - 1));
		BDDMockito.given(Integer.valueOf(searchPageData.getPagination().getPageSize()))
				.willReturn(Integer.valueOf(AbstractSearchPageController.MAX_PAGE_LIMIT));

		controller.populateModel(model, searchPageData, ShowMode.Page);

		Mockito.verify(model).addAttribute("searchPageData", searchPageData);
		Mockito.verify(model).addAttribute("isShowAllAllowed", Boolean.FALSE);
		Mockito.verify(model).addAttribute("isShowPageAllowed", Boolean.FALSE);
	}

	@Test
	public void testPerformProductSearchSetsUseFacetViewMoreLimitToTrue()
	{
		// given
		Mockito.doReturn(TEST_SORT_CODE).when(controller).parseSortCode(TEST_SEARCH_QUERY);
		Mockito.doReturn(pageableDataMock).when(controller).createPageableData(TEST_PAGE, DEFAULT_PAGE_SIZE, TEST_SORT_CODE,
				TEST_SHOW_MODE);
		Mockito.doReturn(searchStateDataMock).when(controller).getSearchStateData(TEST_SEARCH_QUERY);
		Mockito.when(productSearchFacadeMock.textSearch(searchStateDataMock, pageableDataMock)).thenReturn(
				productSearchPageDataMock);
		Mockito.doReturn(encodedProductSearchPageDataMock).when(controller).encodeSearchPageData(
				productSearchPageDataMock);

		// when
		ProductSearchPageData<SearchStateData, ProductData> result =
				controller.performProductSearch(TEST_SEARCH_QUERY, TEST_PAGE, TEST_SHOW_MODE, DEFAULT_PAGE_SIZE);

		// then
		Assert.assertEquals(encodedProductSearchPageDataMock, result);
		Mockito.verify(controller).getSearchStateData(TEST_SEARCH_QUERY);
		Mockito.verify(controller, never()).getSearchStateData(eq(TEST_SEARCH_QUERY), anyBoolean());
	}

	@Test
	public void testPerformContentSearchSetsUseFacetViewMoreLimitToTrue()
	{
		// given
		Mockito.doReturn(pageableDataMock).when(controller).createPageableData(TEST_PAGE, DEFAULT_PAGE_SIZE, null,
				TEST_SHOW_MODE);
		Mockito.doReturn(searchStateDataMock).when(controller).getSearchStateData(TEST_SEARCH_QUERY);
		Mockito.when(wileyb2cContentSearchFacadeMock.textSearch(searchStateDataMock, pageableDataMock)).thenReturn(
				wileyb2cContentSearchPageDataMock);
		Mockito.doReturn(encodedWileyb2cContentSearchPageDataMock).when(controller).encodeWileySearchPageData(
				wileyb2cContentSearchPageDataMock);

		// when
		Wileyb2cContentSearchPageData<SearchStateData, ContentData> result =
				controller.performContentSearch(TEST_SEARCH_QUERY, TEST_PAGE, TEST_SHOW_MODE, DEFAULT_PAGE_SIZE);

		// then
		Assert.assertEquals(encodedWileyb2cContentSearchPageDataMock, result);
		Mockito.verify(controller).getSearchStateData(TEST_SEARCH_QUERY);
		Mockito.verify(controller, never()).getSearchStateData(eq(TEST_SEARCH_QUERY), anyBoolean());
	}

	@Test
	public void testGetAllFacetValuesSetsUseFacetViewMoreLimitToFalse()
	{
		// given
		Mockito.doReturn(searchStateDataMock).when(controller).getSearchStateData(TEST_SEARCH_QUERY, false);
		Mockito.doReturn(DEFAULT_PAGE_SIZE).when(controller).getSearchPageSize();

		Mockito.doReturn(pageableDataMock).when(controller).createPageableData(TEST_PAGE, DEFAULT_PAGE_SIZE, TEST_SORT_CODE,
				TEST_SHOW_MODE);

		Mockito.when(productSearchPageDataMock.getFacets()).thenReturn(facetMocks);
		Mockito.when(productSearchPageDataMock.getBreadcrumbs()).thenReturn(breadcrumbMocks);
		Mockito.when(productSearchFacadeMock.textSearch(searchStateDataMock, pageableDataMock)).thenReturn(
				productSearchPageDataMock);
		Mockito.doReturn(selectedFacetMocks).when(controller).convertBreadcrumbsToFacets(breadcrumbMocks);
		Mockito.doReturn(refinedFacetMocks).when(controller).refineFacets(facetMocks, selectedFacetMocks);

		// when
		controller.getAllFacetValues(TEST_SEARCH_QUERY, TEST_PAGE, TEST_SHOW_MODE,
				TEST_FACET_CODE, TEST_SORT_CODE, model);

		// then
		Mockito.verify(controller).getSearchStateData(TEST_SEARCH_QUERY, false);
		Mockito.verify(controller, never()).getSearchStateData(TEST_SEARCH_QUERY, true);
		Mockito.verify(controller, never()).getSearchStateData(TEST_SEARCH_QUERY);
		Mockito.verify(model).addAttribute(SearchPageController.FACET, refinedFacetMock);
	}

	public class TestableSearchPageController extends SearchPageController
	{
		@Override
		protected PageableData createPageableData(final int pageNumber, final int pageSize, final String sortCode,
				final ShowMode showMode)
		{
			return super.createPageableData(pageNumber, pageSize, sortCode, showMode);
		}

		@Override
		protected void populateModel(final Model model, final SearchPageData<?> searchPageData, final ShowMode showMode)
		{
			super.populateModel(model, searchPageData, showMode);
		}
	}
}