package com.wiley.wileyb2c.storefront.context.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.site.BaseSiteService;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.net.URL;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.wiley.facades.wileyb2c.order.Wileyb2cCartFacade;


/**
 * Default unit test for {@link Wileyb2cContextInformationLoaderImpl}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cContextInformationLoaderImplUnitTest
{

	@Mock
	private CMSSiteService cmsSiteServiceMock;

	@Mock
	private BaseSiteService baseSiteServiceMock;

	@Mock
	private Wileyb2cCartFacade wileyb2cCartFacadeMock;

	@InjectMocks
	private Wileyb2cContextInformationLoaderImpl wileyb2cContextInformationLoader;

	// Test Data

	private String defaultSiteUid = "someSiteUid";

	private String testUrl = "http://someUrl.com";

	@Mock
	private CMSSiteModel cmsSiteModelMock1;

	@Mock
	private CMSSiteModel cmsSiteModelMock2;

	@Before
	public void setUp() throws Exception
	{
		ReflectionTestUtils.setField(wileyb2cContextInformationLoader, "defaultSiteUid", defaultSiteUid);

		when(cmsSiteServiceMock.getSites()).thenReturn(Arrays.asList(cmsSiteModelMock1, cmsSiteModelMock2));
	}

	@Test
	public void testInitializeSiteFromRequestWhenMalformedURL() throws Exception
	{
		// Given
		// no changes in test data

		// When
		final CMSSiteModel result = wileyb2cContextInformationLoader.initializeSiteFromRequest("MalformedURL");

		// Then
		assertNull(result);
		verify(baseSiteServiceMock, never()).setCurrentBaseSite(any(CMSSiteModel.class), anyBoolean());
		verify(wileyb2cCartFacadeMock, never()).saveContinueUrl(any());
	}

	@Test
	public void testInitializeSiteFromRequestWhenCMSSiteNotFoundAndDefaultSiteExists() throws Exception
	{
		// Given
		when(cmsSiteServiceMock.getSiteForURL(any(URL.class))).thenThrow(new CMSItemNotFoundException("CmsSite was not found."));
		when(cmsSiteModelMock2.getUid()).thenReturn(defaultSiteUid);

		// When
		final CMSSiteModel result = wileyb2cContextInformationLoader.initializeSiteFromRequest(testUrl);

		// Then
		assertSame(cmsSiteModelMock2, result);
		verify(cmsSiteServiceMock).getSiteForURL(any(URL.class));
		verify(baseSiteServiceMock).setCurrentBaseSite(same(cmsSiteModelMock2), eq(true));
		verify(wileyb2cCartFacadeMock).saveContinueUrl(eq(null));
	}

	@Test
	public void testInitializeSiteFromRequestWhenCMSSiteNotFoundAndDefaultSiteNotExists() throws Exception
	{
		// Given
		when(cmsSiteServiceMock.getSiteForURL(any(URL.class))).thenThrow(new CMSItemNotFoundException("CmsSite was not found."));

		// When
		final CMSSiteModel result = wileyb2cContextInformationLoader.initializeSiteFromRequest(testUrl);

		// Then
		assertNull(result);
		verify(cmsSiteServiceMock).getSiteForURL(any(URL.class));
		verify(baseSiteServiceMock, never()).setCurrentBaseSite(any(CMSSiteModel.class), eq(true));
		verify(wileyb2cCartFacadeMock, never()).saveContinueUrl(any());
	}

	@Test
	public void testInitializeSiteFromRequestWhenDefaultSitePropertyIsNotSet() throws Exception
	{
		// Given
		when(cmsSiteServiceMock.getSiteForURL(any(URL.class))).thenThrow(new CMSItemNotFoundException("CmsSite was not found."));
		ReflectionTestUtils.setField(wileyb2cContextInformationLoader, "defaultSiteUid", null);

		// When
		try
		{
			wileyb2cContextInformationLoader.initializeSiteFromRequest(testUrl);
			fail("Expected " + IllegalStateException.class);
		}
		catch (IllegalStateException e)
		{
			// Then
			verify(baseSiteServiceMock, never()).setCurrentBaseSite(any(CMSSiteModel.class), eq(true));
			verify(wileyb2cCartFacadeMock, never()).saveContinueUrl(any());
		}
	}

	@Test
	public void testInitializeSiteFromRequestWhenCMSSiteIsFound() throws Exception
	{
		// Given
		when(cmsSiteServiceMock.getSiteForURL(eq(new URL(testUrl)))).thenReturn(cmsSiteModelMock1);

		// When
		final CMSSiteModel result = wileyb2cContextInformationLoader.initializeSiteFromRequest(testUrl);

		// Then
		assertSame(cmsSiteModelMock1, result);
		verify(cmsSiteServiceMock).getSiteForURL(any(URL.class));
		verify(baseSiteServiceMock).setCurrentBaseSite(same(cmsSiteModelMock1), eq(true));
		verify(wileyb2cCartFacadeMock).saveContinueUrl(eq(null));
	}
}