package com.wiley.wileyb2c.storefront.util;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantProductModel;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Created by Raman_Hancharou on 6/28/2017.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cPageTitleResolverUnitTest
{
	private static final String TITLE_WORD_SEPARATOR = " | ";
	private static final String PRODUCT_NAME = "product name";
	public static final String TEST_TITLE = "testTitle";
	public static final String TEST_SITE_NAME = "testSiteName";

	@Mock
	private CMSSiteService cmsSiteService;
	@Mock
	private CMSSiteModel cmsSiteMock;
	@Mock
	private ProductModel mockProductModel;
	@Mock
	private VariantProductModel mockVariantProductModel;

	@InjectMocks
	private Wileyb2cPageTitleResolver wileyb2cPageTitleResolver;

	@Before
	public void setup()
	{
		when(mockProductModel.getName()).thenReturn(PRODUCT_NAME);
		when(mockVariantProductModel.getBaseProduct()).thenReturn(mockProductModel);
	}

	@Test
	public void resolvePageTitleWhenSeoTitleIsNotEmpty()
	{
		String pageTitle = wileyb2cPageTitleResolver.resolveProductPageTitle(TEST_TITLE, mockProductModel);
		assertEquals("Should return SEO title if it is set.", TEST_TITLE, pageTitle);
	}

	@Test
	public void resolvePageTitleIfSeoTitleIsEmpty()
	{
		when(mockProductModel.getSeoTitleTag()).thenReturn(null);
		String pageTitle = wileyb2cPageTitleResolver.resolveProductPageTitle(StringUtils.EMPTY, mockProductModel);
		assertEquals("Should return OOTB page title if SEO title is not set for the product.", PRODUCT_NAME, pageTitle);
	}
	
	@Test
	public void resolvePageTitleWhenSeoTitleIsNotEmptyAndVariantPassed()
	{
		String pageTitle = wileyb2cPageTitleResolver.resolveProductPageTitle(TEST_TITLE, mockVariantProductModel);
		assertEquals("Should return SEO title if it is set.", TEST_TITLE, pageTitle);
	}

	@Test
	public void resolvePageTitleIfSeoTitleIsEmptyAndVariantPassed()
	{
		when(mockProductModel.getSeoTitleTag()).thenReturn(null);
		String pageTitle = wileyb2cPageTitleResolver.resolveProductPageTitle(StringUtils.EMPTY, mockVariantProductModel);
		assertEquals("Should return OOTB page title if SEO title is not set for the product.", PRODUCT_NAME, pageTitle);
	}

	@Test
	public void resolveHomepageTitleWhenTitleIsEmpty()
	{
		when(cmsSiteService.getCurrentSite()).thenReturn(cmsSiteMock);
		when(cmsSiteMock.getName()).thenReturn(TEST_SITE_NAME);

		String pageTitle = wileyb2cPageTitleResolver.resolveHomePageTitle(StringUtils.EMPTY);
		assertEquals(TEST_SITE_NAME, pageTitle);
	}

	@Test
	public void resolveHomepageTitleWhenTitleIsNotEmpty()
	{
		when(cmsSiteService.getCurrentSite()).thenReturn(cmsSiteMock);
		when(cmsSiteMock.getName()).thenReturn(TEST_SITE_NAME);

		String pageTitle = wileyb2cPageTitleResolver.resolveHomePageTitle(TEST_TITLE);
		assertEquals(TEST_TITLE + TITLE_WORD_SEPARATOR + TEST_SITE_NAME, pageTitle);
	}
}
