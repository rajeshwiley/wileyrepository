package com.wiley.wileyb2c.storefront.interceptors.beforeview;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.data.CountryData;
import static org.mockito.Mockito.when;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpMethod;
import org.springframework.web.servlet.ModelAndView;

import com.wiley.facades.common.LocalePopupData;
import com.wiley.facades.locale.WileyLocaleFacade;
import com.wiley.storefrontcommons.locale.impl.WileyLocaleFromCookieHandler;
import com.wiley.storefrontcommons.locale.impl.WileyLocaleFromGeolocationHandler;
import com.wiley.storefrontcommons.locale.impl.WileyLocaleFromRequestUrlHandler;
import com.wiley.storefrontcommons.security.cookie.EnhancedCookieGenerator;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;



@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyLocalePopupBeforeViewHandlerUnitTest
{
	private static final String COOKIE_NAME = "cookie name";
	private static final String COOKIE_PATH = "cookie path";
	private static final Integer COOKIE_MAX_AGE = 25;

	private static final String CURRENT_ENCODED_LOCALE = "current encoded locale";
	private static final String REQUEST_ENCODED_LOCALE = "encoded locale from request";
	private static final String COOKIE_ENCODED_LOCALE = "encoded locale from cookie";
	private static final String GEOLOCATION_ENCODED_LOCALE = "encoded locale from geolocation";

	private static final String SHOW_LOCALE_POPUP_ATTR = "showLocalePopup";
	private static final String LOCALE_POPUP_DATA_ATTR = "localePopupData";

	@Mock
	private EnhancedCookieGenerator wileyb2cCountryCookieGeneratorMock;

	@Mock
	private WileyLocaleFacade wileyLocaleFacadeMock;

	@Mock
	private WileyLocaleFromRequestUrlHandler wileyLocaleFromRequestUrlHandler;

	@Mock
	private WileyLocaleFromCookieHandler wileyLocaleFromCookieHandler;

	@Mock
	private WileyLocaleFromGeolocationHandler wileyLocaleFromGeolocationHandler;

	@Mock
	private CountryData currentCountryDataMock;

	@Mock
	private CountryData detectedCountryDataMock;

	@Mock
	private HttpServletRequest requestMock;

	@Mock
	private HttpServletResponse responseMock;

	private ModelAndView modelAndView;

	private WileyLocalePopupBeforeViewHandler handler;

	@Before
	public void setUp()
	{
		handler = new WileyLocalePopupBeforeViewHandler(wileyLocaleFacadeMock, wileyLocaleFromRequestUrlHandler,
				wileyLocaleFromCookieHandler, wileyLocaleFromGeolocationHandler, wileyb2cCountryCookieGeneratorMock);

		when(wileyb2cCountryCookieGeneratorMock.getCookieName()).thenReturn(COOKIE_NAME);
		when(wileyb2cCountryCookieGeneratorMock.getCookiePath()).thenReturn(COOKIE_PATH);
		when(wileyb2cCountryCookieGeneratorMock.getCookieMaxAge()).thenReturn(COOKIE_MAX_AGE);

		when(wileyLocaleFacadeMock.getCurrentEncodedLocale()).thenReturn(CURRENT_ENCODED_LOCALE);
		when(wileyLocaleFacadeMock.getCountryForEncodedLocale(CURRENT_ENCODED_LOCALE)).thenReturn(currentCountryDataMock);

		when(wileyLocaleFacadeMock.getCountryForEncodedLocale(REQUEST_ENCODED_LOCALE)).thenReturn(detectedCountryDataMock);
		when(wileyLocaleFacadeMock.getCountryForEncodedLocale(COOKIE_ENCODED_LOCALE)).thenReturn(detectedCountryDataMock);
		when(wileyLocaleFacadeMock.getCountryForEncodedLocale(GEOLOCATION_ENCODED_LOCALE)).thenReturn(detectedCountryDataMock);

		when(requestMock.getMethod()).thenReturn(HttpMethod.GET.name());

		when(wileyLocaleFromRequestUrlHandler.getEncodedLocale()).thenReturn(Optional.of(REQUEST_ENCODED_LOCALE));
		when(wileyLocaleFromCookieHandler.getEncodedLocale()).thenReturn(Optional.of(COOKIE_ENCODED_LOCALE));
		when(wileyLocaleFromGeolocationHandler.getEncodedLocale()).thenReturn(Optional.of(GEOLOCATION_ENCODED_LOCALE));

		modelAndView = new ModelAndView();
	}

	@Test
	public void shouldBeExecutedForGetMethod() throws Exception
	{
		when(requestMock.getMethod()).thenReturn(HttpMethod.POST.name());

		handler.beforeView(requestMock, responseMock, modelAndView);

		assertNull(modelAndView.getModelMap().get(SHOW_LOCALE_POPUP_ATTR));
		assertNull(modelAndView.getModelMap().get(LOCALE_POPUP_DATA_ATTR));
	}

	@Test
	public void shouldNotShowLocalePopupWhenNoEncodedLocaleInRequest() throws Exception
	{
		when(wileyLocaleFromRequestUrlHandler.getEncodedLocale()).thenReturn(Optional.empty());

		handler.beforeView(requestMock, responseMock, modelAndView);

		assertFalse((Boolean) modelAndView.getModelMap().get(SHOW_LOCALE_POPUP_ATTR));
		assertNull(modelAndView.getModelMap().get(LOCALE_POPUP_DATA_ATTR));
	}

	@Test
	public void shouldShowLocalePopupWhenEncodedLocaleFromCookieDiffersFromRequestOne() throws Exception
	{
		when(wileyLocaleFromCookieHandler.getEncodedLocale()).thenReturn(Optional.of(COOKIE_ENCODED_LOCALE));
		when(wileyLocaleFromGeolocationHandler.getEncodedLocale()).thenReturn(Optional.of(GEOLOCATION_ENCODED_LOCALE));
		
		handler.beforeView(requestMock, responseMock, modelAndView);

		assertTrue((Boolean) modelAndView.getModelMap().get(SHOW_LOCALE_POPUP_ATTR));
		final LocalePopupData localPopupData = getLocalPopupData(modelAndView);
		assertNotNull(localPopupData);
		assertFalse(localPopupData.getCurrentCountryGeodetected());
	}

	@Test
	public void shouldShowLocalePopupWhenEncodedLocaleFromGeolocationDiffersFromRequestOne() throws Exception
	{
		when(wileyLocaleFromCookieHandler.getEncodedLocale()).thenReturn(Optional.empty());
		when(wileyLocaleFromGeolocationHandler.getEncodedLocale()).thenReturn(Optional.of(GEOLOCATION_ENCODED_LOCALE));

		handler.beforeView(requestMock, responseMock, modelAndView);

		assertTrue((Boolean) modelAndView.getModelMap().get(SHOW_LOCALE_POPUP_ATTR));
		final LocalePopupData localPopupData = getLocalPopupData(modelAndView);
		assertNotNull(localPopupData);
		assertTrue(localPopupData.getCurrentCountryGeodetected());
	}


	@Test
	public void shouldPopulateCookiesSpecificDetails() throws Exception
	{
		handler.beforeView(requestMock, responseMock, modelAndView);

		final LocalePopupData localePopupData = getLocalPopupData(modelAndView);
		assertEquals(COOKIE_PATH, localePopupData.getCookieCountryCookiePath());
		assertEquals(COOKIE_NAME, localePopupData.getCookieCountryCookieName());
		assertEquals(COOKIE_MAX_AGE, localePopupData.getCookieCountryCookieMaxAge());
	}

	@Test
	public void shouldPopulateEncodedLocaleDetails() throws Exception
	{
		handler.beforeView(requestMock, responseMock, modelAndView);

		final LocalePopupData localePopupData = getLocalPopupData(modelAndView);
		assertEquals(CURRENT_ENCODED_LOCALE, localePopupData.getCurrentLocale());
		assertEquals(currentCountryDataMock, localePopupData.getCurrentCountry());
		assertEquals(COOKIE_ENCODED_LOCALE, localePopupData.getDetectedLocale());
		assertEquals(detectedCountryDataMock, localePopupData.getDetectedCountry());
	}
	
	private LocalePopupData getLocalPopupData(final ModelAndView modelAndView)
	{
		return (LocalePopupData) modelAndView.getModelMap().get(LOCALE_POPUP_DATA_ATTR);
	}

}