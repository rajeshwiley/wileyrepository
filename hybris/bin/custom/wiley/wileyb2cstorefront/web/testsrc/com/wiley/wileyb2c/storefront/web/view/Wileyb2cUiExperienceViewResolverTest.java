package com.wiley.wileyb2c.storefront.web.view;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.uiexperience.impl.DefaultUiExperienceService;
import de.hybris.platform.commerceservices.enums.UiExperienceLevel;
import de.hybris.platform.spring.ctx.TenantIgnoreXmlWebApplicationContext;

import java.util.Locale;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.web.servlet.View;

import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import static org.hamcrest.CoreMatchers.instanceOf;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cUiExperienceViewResolverTest
{
	public static final String REDIRECT_EN_US = "redirect:/en-us/";
	public static final String EN_US = "/en-us/";
	public static final String US = "us";

	@InjectMocks
	private Wileyb2cUiExperienceViewResolver resolver;

	@Mock
	private DefaultUiExperienceService uiExperienceServiceMock;

	@Mock
	private TenantIgnoreXmlWebApplicationContext contextMock;

	@Mock
	private DefaultListableBeanFactory beanFactoryMock;


	@Test
	public void resolveViewNameWithRedirectTest() throws Exception
	{
		when(uiExperienceServiceMock.getUiExperienceLevel()).thenReturn(UiExperienceLevel.DESKTOP);
		when(contextMock.getAutowireCapableBeanFactory()).thenReturn(beanFactoryMock);
		HomePageRedirectView testView = new HomePageRedirectView(EN_US, true, true);
		when(beanFactoryMock.initializeBean(Matchers.any(HomePageRedirectView.class), Matchers.eq(REDIRECT_EN_US))).thenReturn(
				testView);

		final View view = resolver.resolveViewName(REDIRECT_EN_US, new Locale(US));

		assertThat(view, instanceOf(HomePageRedirectView.class));
	}
}
