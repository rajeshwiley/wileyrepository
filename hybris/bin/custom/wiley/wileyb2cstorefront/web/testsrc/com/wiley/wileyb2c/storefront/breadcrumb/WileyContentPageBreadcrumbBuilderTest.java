package com.wiley.wileyb2c.storefront.breadcrumb;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static junit.framework.Assert.assertEquals;
import static org.apache.logging.log4j.util.Strings.EMPTY;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyContentPageBreadcrumbBuilderTest
{
	private static final String LAST_LINK_CLASS = "active";
	private static final String LAST_LINK_URL = "#";
	private static final String PAGE_NAME = "page";
	private static final String PAGE_URL = "/parentPageUrl_2/parentPageUrl_1/pageUrl/";
	private static final String PARENT_PAGE_NAME_1 = "parentPage_1";
	private static final String PARENT_PAGE_URL_1 = "/parentPageUrl_2/parentPageUrl_1/";
	private static final String PARENT_PAGE_NAME_2 = "parentPage_2";
	private static final String PARENT_PAGE_URL_2 = "/parentPageUrl_2/";


	private WileyContentPageBreadcrumbBuilder testInstance = new WileyContentPageBreadcrumbBuilder();

	@Mock
	private ContentPageModel mockPage;
	@Mock
	private ContentPageModel mockParentPage1;
	@Mock
	private ContentPageModel mockParentPage2;

	@Before
	public void setUp()
	{
		givenPage(mockPage, PAGE_NAME, PAGE_URL, mockParentPage1);
		givenPage(mockParentPage1, PARENT_PAGE_NAME_1, PARENT_PAGE_URL_1, mockParentPage2);
		givenPage(mockParentPage2, PARENT_PAGE_NAME_2, PARENT_PAGE_URL_2, null);
	}

	@Test
	public void shouldHaveLastBreadcrumbWithoutLink()
	{
		final List<Breadcrumb> breadcrumbs = testInstance.getBreadcrumbs(mockPage);
		verifyLastBreadcrumb(mockPage, breadcrumbs.get(2));
	}

	@Test
	public void shouldBuildBreadcrumbsAccordingToPageHierarchy()
	{
		final List<Breadcrumb> breadcrumbs = testInstance.getBreadcrumbs(mockPage);
		assertEquals(3, breadcrumbs.size());
		verifyBreadcrumb(mockParentPage2, breadcrumbs.get(0));
		verifyBreadcrumb(mockParentPage1, breadcrumbs.get(1));
		verifyLastBreadcrumb(mockPage, breadcrumbs.get(2));
	}

	@Test
	public void shouldIterateThroughPagesUntilParentOneIsNull()
	{
		when(mockParentPage1.getParentPage()).thenReturn(null);
		final List<Breadcrumb> breadcrumbs = testInstance.getBreadcrumbs(mockPage);
		assertEquals(2, breadcrumbs.size());
	}

	@Test
	public void shouldIterateThroughPagesUntilThereIsNoCycleDependency()
	{
		when(mockParentPage2.getParentPage()).thenReturn(mockPage);
		final List<Breadcrumb> breadcrumbs = testInstance.getBreadcrumbs(mockPage);
		assertEquals(3, breadcrumbs.size());
	}

	private void givenPage(final ContentPageModel page, final String pageName, final String pageUrl,
			final ContentPageModel parentPage)
	{
		when(page.getTitle()).thenReturn(pageName);
		when(page.getLabel()).thenReturn(pageUrl);
		when(page.getParentPage()).thenReturn(parentPage);
	}

	private void verifyLastBreadcrumb(final ContentPageModel page, final Breadcrumb breadcrumb)
	{
		assertEquals(page.getTitle(), breadcrumb.getName());
		assertEquals(LAST_LINK_CLASS, breadcrumb.getLinkClass());
		assertEquals(LAST_LINK_URL, breadcrumb.getUrl());
	}

	private void verifyBreadcrumb(final ContentPageModel page, final Breadcrumb breadcrumb)
	{
		assertEquals(page.getTitle(), breadcrumb.getName());
		assertEquals(EMPTY, breadcrumb.getLinkClass());
		assertEquals(page.getLabel(), breadcrumb.getUrl());
	}
}
