package com.wiley.wileyb2c.storefront.interceptors.beforeview;

import de.hybris.bootstrap.annotations.UnitTest;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.servlet.ModelAndView;

import com.wiley.core.wiley.session.storage.WileyFailedCartModificationsStorageService;


/**
 * @author Dzmitryi_Halahayeu
 */
@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class WileyCheckCartCalculationErrorsDuringCheckoutBeforeViewHandlerUnitTest
{
	private static final String CHECKOUT_SUMMARY_VIEW = "/checkout/summary";
	private static final String CART_VIEW = "/cart";
	@InjectMocks
	private WileyCheckCartCalculationErrorsDuringCheckoutBeforeViewHandler handler;
	@Mock
	private ModelAndView modelAndView;
	@Mock
	private WileyFailedCartModificationsStorageService wileyFailedCartModificationsStorageService;

	@Test
	public void whenCalculationIsNotFailedShouldDoNotRedirect() throws Exception
	{
		when(wileyFailedCartModificationsStorageService.isCartCalculationFailed()).thenReturn(false);

		handler.beforeView(null, null, modelAndView);

		verifyZeroInteractions(modelAndView);
	}


	@Test
	public void whenCalculationFailedAndViewIsNotCheckoutShouldDonNotRedirect() throws Exception
	{
		when(wileyFailedCartModificationsStorageService.isCartCalculationFailed()).thenReturn(true);
		when(modelAndView.getViewName()).thenReturn(CART_VIEW);

		handler.beforeView(null, null, modelAndView);

		verify(modelAndView, never())
				.setViewName(WileyCheckCartCalculationErrorsDuringCheckoutBeforeViewHandler.REDIRECT_TO_CART);
	}


	@Test
	public void whenCalculationFailedAndViewIsCheckoutShouldRedirect() throws Exception
	{
		when(wileyFailedCartModificationsStorageService.isCartCalculationFailed()).thenReturn(true);
		when(modelAndView.getViewName()).thenReturn(CHECKOUT_SUMMARY_VIEW);

		handler.beforeView(null, null, modelAndView);

		verify(modelAndView).setViewName(WileyCheckCartCalculationErrorsDuringCheckoutBeforeViewHandler.REDIRECT_TO_CART);
	}




}
