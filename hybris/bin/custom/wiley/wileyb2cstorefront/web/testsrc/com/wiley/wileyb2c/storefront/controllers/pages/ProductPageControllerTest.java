package com.wiley.wileyb2c.storefront.controllers.pages;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.storefront.data.MetaElementData;
import de.hybris.platform.acceleratorservices.urlresolver.SiteBaseUrlResolutionService;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;

import com.wiley.facades.cms.data.MarketingContentMenuItemData;
import com.wiley.facades.wileyb2c.cms.Wileyb2cCMSFacade;
import com.wiley.storefrontcommons.web.theme.StorefrontResourceBundleSource;
import com.wiley.wileyb2c.storefront.util.Wileyb2cValueParser;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ProductPageControllerTest
{
	private static final String MEDIA_URL = "/test_medial_url";
	private static final String PRODUCT_URL = "/test_product";
	private static final String DEFAULT_IMAGE = "img.missingProductImage.responsive.product";
	private static final String START_PART = "https://testsite:9002";
	private static final String META_KEY = "key";
	private static final String META_KEY_NAME = "og:title";
	private static final String META_KEY_SITE_NAME = "og:site_name";
	private static final String META_KEY_IMAGE = "og:image";
	private static final String META_KEY_DESCR = "og:description";
	private static final String META_KEY_URL = "og:url";
	private static final String META_KEY_TYPE = "og:type";
	private static final String META_KEY_ISBN = "books:isbn";
	private static final String META_KEY_DATE = "books:release_date";
	private static final String META_KEY_OG = "og";
	private static final String META_KEY_BOOK = "books";
	private static final String META_KEY_FB = "fb";
	private static final String META_VALUE = "value";
	private static final String META_VALUE_DESCR = "View more on Wiley.com";
	private static final String META_VALUE_SEO_DESCR = "seo_description";
	private static final String META_VALUE_ISBN = "978-1-119-43444-3";
	private static final String META_VALUE_SITE_NAME = "Wiley.com";
	private static final String META_VALUE_TYPE = "books.book";
	private static final String META_VALUE_OG = "http://ogp.me/ns#";
	private static final String META_VALUE_FB = "http://ogp.me/ns/fb#";
	private static final String META_VALUE_BOOK = "http://ogp.me/ns/books#";
	private static final String PRODUCT_NAME = "test_product_name";
	private static final String MODEL_PRODUCT = "product";
	private static final String MODEL_METATAGS = "openGraphTags";
	private static final String MODEL_PREFIXES = "prefixes";
	private static final String MODEL_PAGE = "cmsPage";
	private static final String MODEL_MARKETING_CONTENT_MENU_ITEMS = "marketingContentMenuItems";
	private static final int DATE_YEAR = 2017;
	private static final int DATE_DAY = 1;
	private static final int DATE_HOUR = 1;
	private static final int DATE_OTHER = 0;

	@Mock
	private Wileyb2cValueParser wileyb2cValueParser;

	@Mock
	private StorefrontResourceBundleSource storefrontResourceBundleSource;

	@Mock
	private CMSSiteService cmsSiteService;

	@Mock
	private SiteBaseUrlResolutionService siteBaseUrlResolutionService;

	@Mock
	private HttpServletRequest httpServletRequest;

	@Mock
	private AbstractPageModel pageMock;

	@Mock
	private ProductModel productMock;

	@Mock
	private Wileyb2cCMSFacade wileyb2cCMSFacade;

	@InjectMocks
	private ProductPageController productPageController = new ProductPageController();

	private Collection<ImageData> medias;
	private Model model;
	private Calendar calendar;
	private ProductData productData;

	@Before
	public void setUp()
	{
		BDDMockito.given(storefrontResourceBundleSource.getMessage(DEFAULT_IMAGE, null, Locale.US))
				.willReturn(DEFAULT_IMAGE);
		BDDMockito.given(cmsSiteService.getCurrentSite())
				.willReturn(null);
		BDDMockito.given(siteBaseUrlResolutionService.getMediaUrlForSite(null, true, MEDIA_URL))
				.willReturn(START_PART + MEDIA_URL);
		BDDMockito.given(siteBaseUrlResolutionService.getMediaUrlForSite(null, true, DEFAULT_IMAGE))
				.willReturn(START_PART + DEFAULT_IMAGE);
		BDDMockito.given(siteBaseUrlResolutionService.getWebsiteUrlForSite(null, true, PRODUCT_URL))
				.willReturn(START_PART + PRODUCT_URL);

		calendar = GregorianCalendar.getInstance(TimeZone.getTimeZone("UTC"));
		calendar.set(DATE_YEAR, Calendar.JANUARY, DATE_DAY);
		calendar.set(Calendar.HOUR, DATE_HOUR);
		calendar.set(Calendar.MINUTE, DATE_OTHER);
		calendar.set(Calendar.SECOND, DATE_OTHER);
		calendar.set(Calendar.ZONE_OFFSET, DATE_OTHER);

		model = new BindingAwareModelMap();
		productData = new ProductData();
		productData.setName(PRODUCT_NAME);
		productData.setUrl(PRODUCT_URL);
		productData.setIsbn13(META_VALUE_ISBN);
		productData.setPublicationDate(calendar.getTime());
		model.addAttribute(MODEL_PRODUCT, productData);
		model.addAttribute(MODEL_METATAGS, new LinkedList<MetaElementData>());
		model.addAttribute(MODEL_PAGE, pageMock);
	}

	@Test
	public void testGetDefaultImage()
	{
		medias = new ArrayList<>();

		String absoluteUrl = productPageController.getProductImageUrl(medias, Locale.US);
		Assert.assertNotNull(absoluteUrl);
		Assert.assertTrue(absoluteUrl.contains(DEFAULT_IMAGE));
	}

	@Test
	public void testGetMediaImage()
	{
		medias = new ArrayList<>();
		ImageData imageData = new ImageData();
		imageData.setUrl(MEDIA_URL);
		medias.add(imageData);

		String absoluteUrl = productPageController.getProductImageUrl(medias, Locale.US);
		Assert.assertNotNull(absoluteUrl);
		Assert.assertTrue(absoluteUrl.contains(MEDIA_URL));
	}

	@Test
	public void testMetaTagFaild()
	{
		List<MetaElementData> metadata = new ArrayList<>();

		productPageController.addMetaTagIfValueExist(metadata, META_KEY, new String());
		Assert.assertTrue(metadata.isEmpty());
	}

	@Test
	public void testMetaTagSuccess()
	{
		List<MetaElementData> metadataList = new ArrayList<>();

		productPageController.addMetaTagIfValueExist(metadataList, META_KEY, META_VALUE);
		Assert.assertFalse(metadataList.isEmpty());

		MetaElementData metadata = metadataList.get(metadataList.size() - 1);
		Assert.assertEquals(META_KEY, metadata.getName());
		Assert.assertEquals(META_VALUE, metadata.getContent());
	}

	@Test
	public void testMetaTagsToModel()
	{
		BDDMockito.given(httpServletRequest.getLocale()).willReturn(Locale.US);
		BDDMockito.given(wileyb2cValueParser.parseDescription(productData, null)).willReturn(META_VALUE_DESCR);

		productPageController.setUpOpenGraphMetaTags(model, httpServletRequest, null);
		final List<MetaElementData> metadataList = ((List<MetaElementData>) model.asMap().get(MODEL_METATAGS));

		Assert.assertFalse(metadataList.isEmpty());
		Assert.assertTrue(metadataList.size() == 8);
		Assert.assertTrue(isMetaElementIsPresent(metadataList, META_KEY_NAME, PRODUCT_NAME));
		Assert.assertTrue(isMetaElementIsPresent(metadataList, META_KEY_DESCR, META_VALUE_DESCR));
		Assert.assertTrue(isMetaElementIsPresent(metadataList, META_KEY_URL, START_PART + PRODUCT_URL));
		Assert.assertTrue(isMetaElementIsPresent(metadataList, META_KEY_SITE_NAME, META_VALUE_SITE_NAME));
		Assert.assertTrue(isMetaElementIsPresent(metadataList, META_KEY_IMAGE, START_PART + DEFAULT_IMAGE));
		Assert.assertTrue(isMetaElementIsPresent(metadataList, META_KEY_TYPE, META_VALUE_TYPE));
		Assert.assertTrue(isMetaElementIsPresent(metadataList, META_KEY_ISBN, META_VALUE_ISBN));
		MetaElementData data = metadataList.stream().filter(item -> item.getName().equals(META_KEY_DATE)).findFirst().get();
		Calendar calendarFromTag = getDateFromString(data.getContent());
		Assert.assertEquals(calendarFromTag.get(Calendar.MONTH), calendar.get(Calendar.MONTH));
		Assert.assertEquals(calendarFromTag.get(Calendar.YEAR), calendar.get(Calendar.YEAR));
		Assert.assertEquals(calendarFromTag.get(Calendar.DATE), calendar.get(Calendar.DATE));
		Assert.assertEquals(calendarFromTag.get(Calendar.HOUR), calendar.get(Calendar.HOUR));
		Assert.assertEquals(calendarFromTag.get(Calendar.MINUTE), calendar.get(Calendar.MINUTE));
		Assert.assertEquals(calendarFromTag.get(Calendar.SECOND), calendar.get(Calendar.SECOND));
		Assert.assertEquals(calendarFromTag.get(Calendar.ZONE_OFFSET), calendar.get(Calendar.ZONE_OFFSET));
	}

	@Test
	public void testOpenGraphPrefixes()
	{
		BDDMockito.given(httpServletRequest.getLocale()).willReturn(Locale.US);
		BDDMockito.given(wileyb2cValueParser.parseDescription(productData, null)).willReturn(META_VALUE_DESCR);

		productPageController.setUpOpenGraphMetaTags(model, httpServletRequest, null);
		final Map<String, String> prefixes = ((Map<String, String>) model.asMap().get(MODEL_PREFIXES));

		Assert.assertFalse(prefixes.isEmpty());
		Assert.assertTrue(prefixes.size() == 3);
		Assert.assertEquals(META_VALUE_OG, prefixes.get(META_KEY_OG));
		Assert.assertEquals(META_VALUE_BOOK, prefixes.get(META_KEY_BOOK));
		Assert.assertEquals(META_VALUE_FB, prefixes.get(META_KEY_FB));
	}

	@Test
	public void testPopulateSeoDescription()
	{
		BDDMockito.given(httpServletRequest.getLocale()).willReturn(Locale.US);
		BDDMockito.given(wileyb2cValueParser.parseDescription(productData, META_VALUE_SEO_DESCR))
				.willReturn(META_VALUE_SEO_DESCR);

		productPageController.setUpOpenGraphMetaTags(model, httpServletRequest, META_VALUE_SEO_DESCR);
		final List<MetaElementData> metadataList = ((List<MetaElementData>) model.asMap().get(MODEL_METATAGS));

		Assert.assertTrue(isMetaElementIsPresent(metadataList, META_KEY_DESCR, META_VALUE_SEO_DESCR));
	}

	@Test
	public void testSetUpContentMenu()
	{
		MarketingContentMenuItemData menuItem = new MarketingContentMenuItemData();

		menuItem.setTitle("Menu Item Title");
		menuItem.setAnchorLink("Menu Item Anchor Link");
		menuItem.setSlotPosition("Menu Item Slot Position");

		final Map<String, List<MarketingContentMenuItemData>> expectedContentMenuItems = new HashMap<>();
		expectedContentMenuItems.put("marketingContentMenuItems", Collections.singletonList(menuItem));

		BDDMockito.given(wileyb2cCMSFacade.getMarketingContentMenu(pageMock, productMock)).willReturn(expectedContentMenuItems);

		productPageController.setUpContentMenu(model, productMock);

		verify(wileyb2cCMSFacade, times(1)).getMarketingContentMenu(pageMock, productMock);

		final Map<String, List<MarketingContentMenuItemData>> actualMenuItems =
				(Map<String, List<MarketingContentMenuItemData>>) model.asMap().get(MODEL_MARKETING_CONTENT_MENU_ITEMS);

		Assert.assertEquals(actualMenuItems, expectedContentMenuItems);
	}

	private boolean isMetaElementIsPresent(final List<MetaElementData> metadataList, final String key, final String value)
	{
		return metadataList.stream()
				.filter(item -> item.getName().equals(key) && item.getContent().equals(value)).findFirst().isPresent();
	}

	private Calendar getDateFromString(final String formattedDate)
	{
		final DateFormat f = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
		f.setTimeZone(TimeZone.getDefault());
		f.setTimeZone(calendar.getTimeZone());
		Calendar result = GregorianCalendar.getInstance();
		try
		{
			Date date = f.parse(formattedDate);
			result.setTime(date);
			result.setTimeZone(TimeZone.getTimeZone("UTC"));
		}
		catch (ParseException e)
		{
			e.printStackTrace();
		}

		return result;
	}
}