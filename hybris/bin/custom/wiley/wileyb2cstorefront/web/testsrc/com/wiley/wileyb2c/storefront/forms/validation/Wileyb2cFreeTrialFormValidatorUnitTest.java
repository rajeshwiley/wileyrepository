package com.wiley.wileyb2c.storefront.forms.validation;


import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.internal.verification.VerificationModeFactory.times;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.strategies.impl.DefaultCustomerNameStrategy;



import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.validation.Errors;

import com.wiley.wileyb2c.storefront.forms.Wileyb2cFreeTrialForm;
import com.wiley.wileycom.storefrontcommons.validators.FieldValidator;




/**
 * Unit test for {@link Wileyb2cFreeTrialFormValidator}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cFreeTrialFormValidatorUnitTest
{
	private static final String SPACE_SEPARATOR = " ";

	@InjectMocks
	private Wileyb2cFreeTrialFormValidator wileyb2cFreeTrialFormValidator;

	@Mock
	private Errors errorsMock;
	@Mock
	private Wileyb2cFreeTrialForm freeTrialForm;

	@Mock
	private FieldValidator firstNameSizeFieldValidator;
	@Mock
	private FieldValidator sizeFieldValidator;
	@Mock
	private DefaultCustomerNameStrategy customerNameStrategy;


	@Before
	public void setUp()
	{
		Mockito.doNothing().when(firstNameSizeFieldValidator).validate(any(Errors.class), anyString(), anyString());
		Mockito.doNothing().when(sizeFieldValidator).validate(any(Errors.class), anyString(), anyString());
	}


	@Test
	public void testExceededSumMaxLength()
	{
		//Sum fields length will exceed acceptable MAX_LENGTH == 255
		final String firstName = "Smith";
		final String lastName = "John";
		Mockito.doReturn(firstName).when(freeTrialForm).getFirstName();
		Mockito.doReturn(lastName).when(freeTrialForm).getLastName();
		Mockito.doReturn(StringUtils.leftPad("*", Wileyb2cFreeTrialFormValidator.MAX_COMBINED_VALUE + 1, "*")).
				when(customerNameStrategy).getName(firstName, lastName);

		wileyb2cFreeTrialFormValidator.validate(freeTrialForm, errorsMock);

		Mockito.verify(errorsMock).rejectValue(Wileyb2cFreeTrialFormValidator.FIRST_NAME_FIELD_KEY,
				Wileyb2cFreeTrialFormValidator.NAME_COMBINED_LENGTH_ERROR_KEY);
		Mockito.verify(errorsMock).rejectValue(Wileyb2cFreeTrialFormValidator.LAST_NAME_FIELD_KEY,
				Wileyb2cFreeTrialFormValidator.NAME_COMBINED_LENGTH_ERROR_KEY);
	}

	@Test
	public void testPassValidation()
	{
		//Sum fields length won't exceed acceptable MAX_LENGTH == 255
		final String firstName = "Smith";
		final String lastName = "John";
		Mockito.doReturn(firstName).when(freeTrialForm).getFirstName();
		Mockito.doReturn(lastName).when(freeTrialForm).getLastName();
		Mockito.doReturn(StringUtils.leftPad("*", Wileyb2cFreeTrialFormValidator.MAX_COMBINED_VALUE, "*")).
				when(customerNameStrategy).getName(firstName, lastName);

		wileyb2cFreeTrialFormValidator.validate(freeTrialForm, errorsMock);

		Mockito.verify(errorsMock, times(0)).rejectValue(anyString(),
				eq(Wileyb2cFreeTrialFormValidator.NAME_COMBINED_LENGTH_ERROR_KEY));
	}
}
