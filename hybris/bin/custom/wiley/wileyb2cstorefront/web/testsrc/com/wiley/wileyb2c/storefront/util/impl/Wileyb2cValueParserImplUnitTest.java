package com.wiley.wileyb2c.storefront.util.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ProductData;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wiley.facades.product.util.ProductInfo;


/**
 * Default unit test for {@link Wileyb2cValueParserImpl}.
 */
@UnitTest
public class Wileyb2cValueParserImplUnitTest
{

	private Wileyb2cValueParserImpl wileyb2cValueParser;
	private static final String DESCRIPTION_BEFORE =
			"<div><a href=\"#\">link</a> <strong><p>asdasd</p> asdasda\"\"d&qout;asdasd&quot;</strong>";
	private static final String DESCRIPTION_AFTER = "link asdasd asdasdadasdasd";
	private static final String DESCRIPTION_DEFAULT = "View more on Wiley.com";
	private static final String DESCRIPTION_SEO = "Seo_Description";

	@Before
	public void setUp() throws Exception
	{
		wileyb2cValueParser = new Wileyb2cValueParserImpl();
	}

	@Test
	public void testParseKeyValuesValidParamWhenTwoProducts()
	{
		// Given
		final String key1 = "proDuct123_123";
		final String value1 = "qUanT_3";
		final String key2 = "pRoD_2_4";
		final String value2 = "QuNt_4_4";
		String testString = key1 + ":" + value1 + "|" + key2 + ":" + value2;

		// When
		final Map<String, String> result = wileyb2cValueParser.parseKeyValues(testString);

		// Then
		assertNotNull(result);
		assertEquals(2, result.size());

		assertEquals(value1, result.get(key1));
		assertEquals(value2, result.get(key2));
	}

	@Test
	public void testParseKeyValuesValidParamWhenOneProducts()
	{
		// Given
		final String key1 = "proDuct123_123";
		final String value1 = "qUanT_3";
		String testString = key1 + ":" + value1;

		// When
		final Map<String, String> result = wileyb2cValueParser.parseKeyValues(testString);

		// Then
		assertNotNull(result);
		assertEquals(1, result.size());

		assertEquals(value1, result.get(key1));
	}

	@Test
	public void testParseProductsWhen1Products() throws Exception
	{
		// Given
		final String key1 = "productisbn1";
		final Long quantity1 = 123L;
		final String subscriptionTerm1 = "subscriptionTerm1";
		String testString = key1 + ":" + quantity1 + ":" + subscriptionTerm1;

		// When
		final Map<String, ProductInfo> products = wileyb2cValueParser.parseProducts(testString);

		// Then
		assertNotNull(products);
		assertEquals(1, products.size());
		final ProductInfo productInfo = products.get(key1);
		assertNotNull(productInfo);
		assertEquals(quantity1, productInfo.getQuantity());
		assertEquals(subscriptionTerm1, productInfo.getSubscriptionTermCode());
	}

	@Test
	public void testParseProductsWhen2Products() throws Exception
	{
		// Given
		final String key1 = "productisbn1";
		final Long quantity1 = 123L;
		final String subscriptionTerm1 = "subscriptionTerm1";
		final String key2 = "productisbn2";
		final Long quantity2 = 112323L;
		final String subscriptionTerm2 = "subscriptionTerm2";
		String testString =
				key1 + ":" + quantity1 + ":" + subscriptionTerm1 + "|" + key2 + ":" + quantity2 + ":" + subscriptionTerm2;

		// When
		final Map<String, ProductInfo> products = wileyb2cValueParser.parseProducts(testString);

		// Then
		assertNotNull(products);
		assertEquals(2, products.size());
		final ProductInfo productInfo1 = products.get(key1);
		assertNotNull(productInfo1);
		assertEquals(quantity1, productInfo1.getQuantity());
		assertEquals(subscriptionTerm1, productInfo1.getSubscriptionTermCode());
		final ProductInfo productInfo2 = products.get(key2);
		assertNotNull(productInfo2);
		assertEquals(quantity2, productInfo2.getQuantity());
		assertEquals(subscriptionTerm2, productInfo2.getSubscriptionTermCode());
	}

	@Test
	public void testParseProductsWhen3Products() throws Exception
	{
		// Given
		final String key1 = "productisbn1";
		final Long quantity1 = 123L;
		final String subscriptionTerm1 = "subscriptionTerm1";
		final String key2 = "productisbn2";
		final Long quantity2 = 112323L;
		final String subscriptionTerm2 = "subscriptionTerm2";
		final String key3 = "productisbn3";
		final Long quantity3 = 1L;
		String testString =
				key1 + ":" + quantity1 + ":" + subscriptionTerm1 + "|" + key2 + ":" + quantity2 + ":" + subscriptionTerm2 + "|"
						+ key3 + ":" + quantity3;

		// When
		final Map<String, ProductInfo> products = wileyb2cValueParser.parseProducts(testString);

		// Then
		assertNotNull(products);
		assertEquals(3, products.size());
		final ProductInfo productInfo1 = products.get(key1);
		assertNotNull(productInfo1);
		assertEquals(quantity1, productInfo1.getQuantity());
		assertEquals(subscriptionTerm1, productInfo1.getSubscriptionTermCode());
		final ProductInfo productInfo2 = products.get(key2);
		assertNotNull(productInfo2);
		assertEquals(quantity2, productInfo2.getQuantity());
		assertEquals(subscriptionTerm2, productInfo2.getSubscriptionTermCode());
		final ProductInfo productInfo3 = products.get(key3);
		assertNotNull(productInfo3);
		assertEquals(quantity3, productInfo3.getQuantity());
		assertNull(productInfo3.getSubscriptionTermCode());

	}

	@Test
	public void testParseWhenParamIsNull()
	{
		checkIllegalArgumentExceptionKeyValue(null);
	}

	@Test
	public void testParseWhenInputStringHasWrongFormat()
	{
		checkIllegalArgumentExceptionKeyValue("");
		checkIllegalArgumentExceptionKeyValue("product2_234");
		checkIllegalArgumentExceptionKeyValue("product2_234|234_fj");
		checkIllegalArgumentExceptionKeyValue("product2_234:jfier:riiio_234");
		checkIllegalArgumentExceptionKeyValue("product2_234:jfier|riiio_234");
		checkIllegalArgumentExceptionKeyValue("produc:t2_234:jfier|riiio_234:234");
	}

	@Test
	public void testParseProductsWhenInputStringHasWrongFormat()
	{
		checkIllegalArgumentExceptionProducts("");
		checkIllegalArgumentExceptionProducts("product2_234");
		checkIllegalArgumentExceptionProducts("product2_234|234_fj");
		checkIllegalArgumentExceptionProducts("product2_234:123:riiio_234:123");
		checkIllegalArgumentExceptionProducts("product2_234:123|riiio_234");
		checkIllegalArgumentExceptionProducts("produc:123:jfier|riiio_234:rewr");
	}

	@Test
	public void testDefaultDescriptionMessage()
	{
		ProductData productData = new ProductData();
		final String testData = wileyb2cValueParser.parseDescription(productData, null);
		Assert.assertEquals(testData, DESCRIPTION_DEFAULT);
	}

	@Test
	public void testSeoDescriptionMessage()
	{
		ProductData productData = new ProductData();
		final String testData = wileyb2cValueParser.parseDescription(productData, DESCRIPTION_SEO);
		Assert.assertEquals(testData, DESCRIPTION_SEO);
	}

	@Test
	public void testDescripionMessage()
	{
		ProductData productData = new ProductData();
		productData.setDescription(DESCRIPTION_BEFORE);
		final String description = wileyb2cValueParser.parseDescription(productData, null);
		Assert.assertEquals(description, DESCRIPTION_AFTER);
	}

	private void checkIllegalArgumentExceptionKeyValue(final String testString)
	{
		try
		{
			// When
			wileyb2cValueParser.parseKeyValues(testString);
			fail("Expected " + IllegalArgumentException.class + " for input string: " + testString);
		}
		catch (IllegalArgumentException e)
		{
			// Then
			// Success
		}
		catch (Exception e)
		{
			fail("Expected " + IllegalArgumentException.class + " for input string: " + testString);

		}
	}

	private void checkIllegalArgumentExceptionProducts(final String testString)
	{
		try
		{
			// When
			wileyb2cValueParser.parseProducts(testString);
			fail("Expected " + IllegalArgumentException.class + " for input string: " + testString);
		}
		catch (IllegalArgumentException e)
		{
			// Then
			// Success
		}
		catch (Exception e)
		{
			fail("Expected " + IllegalArgumentException.class + " for input string: " + testString);

		}
	}
}