package com.wiley.wileyb2c.storefront.controllers.cms;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.servicelayer.services.impl.DefaultCMSComponentService;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.Model;

import com.wiley.core.model.components.WileyBrightcoveVideoComponentModel;
import com.wiley.facades.populators.bright.cover.WileyBrightCoverHelper;
import com.wiley.wileyb2c.storefront.controllers.ControllerConstants;

import junit.framework.Assert;


/**
 * Unit test for {@link WileyBrightcoveVideoComponentController}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyBrightcoveVideoComponentControllerTest
{
	private static final String COMPONENT_UID = "componentUid";
	private static final String TEST_COMPONENT_UID = "componentUID";
	private static final String TEST_TYPE_CODE = WileyBrightcoveVideoComponentModel._TYPECODE;
	private static final String TEST_TYPE_VIEW = ControllerConstants.Views.Cms.COMPONENT_PREFIX
			+ StringUtils.lowerCase(TEST_TYPE_CODE);
	private static final String ACCOUNT_ID = "accountId";
	private static final String DEFAULT_ACCOUNT_ID = "defaultAccountId";
	private static final String DEFAULT_PLAYER_ID = "defaultPlayerId";
	private static final String SPECIFIED_PLAYER_ID = "specifiedPlayerId";

	private static final String CSS_CLASSES = "cssClasses";
	private static final String COMPONENT = "component";
	public static final String STYLE_CLASS_1 = "styleClass1";
	public static final String STYLE_CLASS_2 = "styleClass2";


	@Mock
	private WileyBrightcoveVideoComponentModel wileyBrightcoveVideoComponentModel;
	@Mock
	private Model model;
	@Mock
	private HttpServletRequest request;
	@Mock
	private HttpServletResponse response;
	@Mock
	private DefaultCMSComponentService cmsComponentService;
	@Mock
	private WileyBrightCoverHelper wileyBrightCoverHelper;

	@InjectMocks
	private WileyBrightcoveVideoComponentController wileyBrightcoveVideoComponentController;

	@Before
	public void setUp()
	{
		given(wileyBrightcoveVideoComponentModel.getPlayerId()).willReturn(StringUtils.EMPTY);
		given(wileyBrightCoverHelper.getPropertyForCurrentSite(WileyBrightCoverHelper.ACCOUNT_ID)).willReturn(DEFAULT_ACCOUNT_ID);
		given(wileyBrightCoverHelper.getPropertyForCurrentSite(WileyBrightCoverHelper.PLAYER_ID)).willReturn(DEFAULT_PLAYER_ID);
		given(wileyBrightcoveVideoComponentModel.getCssClasses()).willReturn(Arrays.asList(STYLE_CLASS_1, STYLE_CLASS_2));
	}

	@Test
	public void testRenderComponentWhenPlayerIdNotSpecified() throws Exception
	{
		//when
		final String viewName = wileyBrightcoveVideoComponentController.handleComponent(request, response, model,
				wileyBrightcoveVideoComponentModel);
		//then
		verify(model, times(1)).addAttribute(ACCOUNT_ID, DEFAULT_ACCOUNT_ID);
		verify(model, times(1)).addAttribute(CSS_CLASSES, new String[] { STYLE_CLASS_1, STYLE_CLASS_2 });
		verify(wileyBrightcoveVideoComponentModel, times(1)).setPlayerId(DEFAULT_PLAYER_ID);
		Assert.assertEquals(TEST_TYPE_VIEW, viewName);
	}

	@Test
	public void testRenderComponentWhenPlayerIdSpecified() throws Exception
	{
		//given
		given(wileyBrightcoveVideoComponentModel.getPlayerId()).willReturn(SPECIFIED_PLAYER_ID);
		//when
		final String viewName = wileyBrightcoveVideoComponentController.handleComponent(request, response, model,
				wileyBrightcoveVideoComponentModel);
		//then
		verify(model, times(1)).addAttribute(ACCOUNT_ID, DEFAULT_ACCOUNT_ID);
		verify(model, times(1)).addAttribute(CSS_CLASSES, new String[] { STYLE_CLASS_1, STYLE_CLASS_2 });
		verify(wileyBrightcoveVideoComponentModel, never()).setPlayerId(anyString());
		Assert.assertEquals(TEST_TYPE_VIEW, viewName);
	}

	@Test
	public void testRenderComponentUid() throws Exception
	{
		given(request.getAttribute(COMPONENT_UID)).willReturn(TEST_COMPONENT_UID);
		given(cmsComponentService.getAbstractCMSComponent(TEST_COMPONENT_UID))
				.willReturn(wileyBrightcoveVideoComponentModel);

		final String viewName = wileyBrightcoveVideoComponentController.handleGet(request, response, model);
		verify(model, Mockito.times(1)).addAttribute(COMPONENT, wileyBrightcoveVideoComponentModel);
		verify(model, times(1)).addAttribute(ACCOUNT_ID, DEFAULT_ACCOUNT_ID);
		verify(model, times(1)).addAttribute(CSS_CLASSES, new String[] { STYLE_CLASS_1, STYLE_CLASS_2 });
		verify(wileyBrightcoveVideoComponentModel, times(1)).setPlayerId(DEFAULT_PLAYER_ID);
		Assert.assertEquals(TEST_TYPE_VIEW, viewName);
	}

	@Test(expected = AbstractPageController.HttpNotFoundException.class)
	public void testRenderComponentNotFound() throws Exception
	{
		given(request.getAttribute(COMPONENT_UID)).willReturn(null);
		given(request.getParameter(COMPONENT_UID)).willReturn(null);
		wileyBrightcoveVideoComponentController.handleGet(request, response, model);
	}

	@Test(expected = AbstractPageController.HttpNotFoundException.class)
	public void testRenderComponentNotFound2() throws Exception
	{
		given(request.getAttribute(COMPONENT_UID)).willReturn(null);
		given(request.getParameter(COMPONENT_UID)).willReturn(TEST_COMPONENT_UID);
		given(cmsComponentService.getSimpleCMSComponent(TEST_COMPONENT_UID)).willReturn(null);
		wileyBrightcoveVideoComponentController.handleGet(request, response, model);
	}

	@Test(expected = AbstractPageController.HttpNotFoundException.class)
	public void testRenderComponentNotFound3() throws Exception
	{
		given(request.getAttribute(COMPONENT_UID)).willReturn(TEST_COMPONENT_UID);
		given(cmsComponentService.getSimpleCMSComponent(TEST_COMPONENT_UID)).willReturn(null);
		given(cmsComponentService.getSimpleCMSComponent(TEST_COMPONENT_UID)).willThrow(new CMSItemNotFoundException(""));
		wileyBrightcoveVideoComponentController.handleGet(request, response, model);
	}
}
