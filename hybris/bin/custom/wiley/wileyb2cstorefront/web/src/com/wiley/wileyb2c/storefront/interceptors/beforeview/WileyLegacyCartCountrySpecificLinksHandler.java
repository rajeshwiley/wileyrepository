package com.wiley.wileyb2c.storefront.interceptors.beforeview;

import com.wiley.core.servicelayer.i18n.WileycomI18NService;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.interceptors.BeforeViewHandler;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;


public class WileyLegacyCartCountrySpecificLinksHandler implements BeforeViewHandler
{
	private static final String VIEW_CART_END_PARAM = "+parmhybrs(a0010):Y";
	
	@Value("${legacy.cart.my.account.link}")
	private String myAccountLink;

	@Value("${legacy.cart.view.cart.link}")
	private String viewCartLink;

	@Value("${website.wileyb2c.https}")
	private String wileyb2cUrl;

	@Resource
	private WileycomI18NService wileycomI18NService;

	@Override
	public void beforeView(final HttpServletRequest request, final HttpServletResponse response, final ModelAndView modelAndView)
			throws Exception
	{
		String returnUrl = wileyb2cUrl + getUrlPath(request);
		if (request.getAttribute(WebConstants.URL_ENCODING_ATTRIBUTES) != null)
		{
			returnUrl = wileyb2cUrl + request.getAttribute(WebConstants.URL_ENCODING_ATTRIBUTES) + getUrlPath(request);
		}
		String parmulr = createParmUrl(returnUrl);
		String baseUrl = wileycomI18NService.getCurrentCountry()
				.orElseThrow(() -> new IllegalStateException("No current country selected"))
				.getLegacyCart().getBaseUrl();
		String legacyMyAccountLink = baseUrl + myAccountLink + parmulr;
		String legacyViewCartLink = baseUrl + viewCartLink + parmulr + VIEW_CART_END_PARAM;

		modelAndView.addObject("legacyMyAccountLink", legacyMyAccountLink);
		modelAndView.addObject("legacyViewCartLink", legacyViewCartLink);
	}

	private String getUrlPath(final HttpServletRequest request) {
		StringBuilder path = new StringBuilder(request.getServletPath());
		if (StringUtils.isNotBlank(request.getQueryString())) {
			path.append("?").append(request.getQueryString());
		}
		return path.toString();
	}
	private String createParmUrl(final String url) throws UnsupportedEncodingException
	{
		String returnUrl = URLEncoder.encode(url, "UTF-8");
		String length = String.format("%03d", returnUrl.length());
		return String.format("+parmurl(l%s0):%s", length, returnUrl);
	}
}
