/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wileyb2c.storefront.controllers.pages.checkout.steps;

import de.hybris.platform.acceleratorservices.enums.CheckoutPciOptionEnum;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.CheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.PlaceOrderForm;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.voucher.data.VoucherData;
import de.hybris.platform.commercefacades.voucher.exceptions.VoucherOperationException;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.payment.AdapterException;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.facades.payment.PaymentAuthorizationResultData;
import com.wiley.facades.voucher.WileyCouponFacade;
import com.wiley.facades.wileyb2c.order.Wileyb2cCheckoutFacade;
import com.wiley.wileyb2c.storefront.controllers.ControllerConstants;
import com.wiley.wileyb2c.storefront.util.InventoryStatusUtil;
import com.wiley.wileycom.storefrontcommons.controllers.pages.checkout.steps.AbstractWileycomCheckoutStepController;
import com.wiley.wileycom.storefrontcommons.forms.WileycomVoucherForm;


@Controller
@RequestMapping(value = "/checkout/multi/summary")
@Profile(WileyCoreConstants.WILEY_PHASE2A_PROFILE_NAME)
public class SummaryCheckoutStepController extends AbstractWileycomCheckoutStepController
{
	private static final Logger LOG = LoggerFactory.getLogger(SummaryCheckoutStepController.class);

	private static final String SUMMARY = "summary";
	private static final String SUMMARY_PAGE_URL = "/checkout/multi/summary/view";

	protected static final String MASKING_CARD_NUMBER = "************";

	@Resource(name = "wileyb2cCheckoutFacade")
	private Wileyb2cCheckoutFacade wileyb2cCheckoutFacade;

	@Resource(name = "wileyb2cCheckoutCouponFacade")
	private WileyCouponFacade couponFacade;

	@RequestMapping(value = "/view", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	@PreValidateCheckoutStep(checkoutStep = SUMMARY)
	public String enterStep(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException,
			CommerceCartModificationException
	{
		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		wileyb2cCheckoutFacade.doRealTimeInventoryCheck(cartData);
		InventoryStatusUtil.addMessageIfNonInStockItem(cartData, model);

		model.addAttribute("cartData", cartData);
		model.addAttribute("allItems", cartData.getEntries());
		model.addAttribute("deliveryAddress", cartData.getDeliveryAddress());
		model.addAttribute("deliveryMode", cartData.getDeliveryMode());

		CCPaymentInfoData paymentInfo = cartData.getPaymentInfo();

		if (paymentInfo != null)
		{
			paymentInfo.setCardNumber(MASKING_CARD_NUMBER + paymentInfo.getCardNumber());
		}

		model.addAttribute("paymentInfo", paymentInfo);

		// Only request the security code if the SubscriptionPciOption is set to Default.
		final boolean requestSecurityCode = (CheckoutPciOptionEnum.DEFAULT.equals(getCheckoutFlowFacade()
				.getSubscriptionPciOption()));
		model.addAttribute("requestSecurityCode", Boolean.valueOf(requestSecurityCode));

		model.addAttribute(new PlaceOrderForm());
		model.addAttribute("voucherForm", new WileycomVoucherForm());
		addVoucherDataToPage(model);

		storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		model.addAttribute(WebConstants.BREADCRUMBS_KEY,
				getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.summary.breadcrumb"));
		model.addAttribute("metaRobots", "noindex,nofollow");
		setCheckoutStepLinksForModel(model, getCheckoutStep());
		return ControllerConstants.Views.Pages.MultiStepCheckout.CHECKOUT_SUMMARY_PAGE;
	}


	@RequestMapping(value = "/placeOrder")
	@RequireHardLogIn
	public String placeOrder(@ModelAttribute("placeOrderForm") final PlaceOrderForm placeOrderForm, final Model model,
			final HttpServletRequest request, final RedirectAttributes redirectModel) throws CMSItemNotFoundException,
			InvalidCartException, CommerceCartModificationException
	{
		final ValidationResults validationResults = getCheckoutStep().validate(redirectModel);
		if (getCheckoutStep().checkIfValidationErrors(validationResults))
		{
			return getCheckoutStep().onValidation(validationResults);
		}
		if (validateOrderForm(placeOrderForm, model))
		{
			return enterStep(model, redirectModel);
		}

		//Validate the cart
		if (validateCart(redirectModel))
		{
			// Invalid cart. Bounce back to the cart page.
			return REDIRECT_URL_CART;
		}

		// authorize, if failure occurs don't allow to place the order
		PaymentAuthorizationResultData authorizationResult = PaymentAuthorizationResultData.failure("Unexpected error");
		try
		{
			authorizationResult = wileyb2cCheckoutFacade.authorizePaymentAndProvideResult(placeOrderForm.getSecurityCode());
		}
		catch (final AdapterException ae)
		{
			// handle a case where a wrong paymentProvider configurations on the store
			// see getCommerceCheckoutService().getPaymentProvider()
			LOG.error(ae.getMessage(), ae);
		}
		if (!authorizationResult.isSuccess())
		{
			GlobalMessages.addMessage(model, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"checkout.placing.order.paymentFailed.authorization",
					new Object[] { authorizationResult.getStatus() });

			return enterStep(model, redirectModel);
		}

		final OrderData orderData;
		try
		{
			orderData = getCheckoutFacade().placeOrder();
		}
		catch (final Exception e)
		{
			LOG.error("Failed to place Order", e);
			GlobalMessages.addErrorMessage(model, "checkout.placeOrder.failed");
			return enterStep(model, redirectModel);
		}

		return redirectToOrderConfirmationPage(orderData);
	}

	@RequestMapping(value = "/redeemVoucher", method = RequestMethod.POST)
	public String redeemVoucherForCart(@Valid @ModelAttribute("voucherForm") final WileycomVoucherForm voucherForm,
			final BindingResult bindingResult, final Model model, final RedirectAttributes redirectAttributes)
			throws CMSItemNotFoundException, CommerceCartModificationException, VoucherOperationException
	{
		if (bindingResult.hasErrors())
		{
			return REDIRECT_PREFIX + SUMMARY_PAGE_URL;
		}

		final String discountCode = voucherForm.getDiscountCode().trim();

		if (getCartFacade().hasEntries())
		{
			if (!couponFacade.checkVoucherCode(discountCode))
			{
				//Code below is commented in scope of migration to new coupons: ECSC-21150
				//List<String> violationMessages = wileyVoucherFacade.getRestrictionViolationMessagesForCurrentCart(discountCode);
				List<String> violationMessages = new ArrayList<>();
				addEditedVoucherDataToPage(redirectAttributes, true, violationMessages.isEmpty() ? null : violationMessages);
			}
			else
			{
				couponFacade.removeVouchersFromCart();
				couponFacade.applyVoucher(discountCode);

				if (LOG.isDebugEnabled())
				{
					LOG.info("Apply voucher [" + discountCode + "]");
				}

				addEditedVoucherDataToPage(redirectAttributes, false, null);
			}
		}

		return REDIRECT_PREFIX + SUMMARY_PAGE_URL;
	}

	@RequestMapping(value = "/releaseVoucher", method = RequestMethod.POST)
	public String releaseVoucherForCart(final Model model) throws CommerceCartModificationException, CMSItemNotFoundException
	{
		couponFacade.removeVouchersFromCart();

		return REDIRECT_PREFIX + SUMMARY_PAGE_URL;
	}

	/**
	 * Validates the order form before to filter out invalid order states
	 *
	 * @param placeOrderForm
	 * 		The spring form of the order being submitted
	 * @param model
	 * 		A spring Model
	 * @return True if the order form is invalid and false if everything is valid.
	 */
	protected boolean validateOrderForm(final PlaceOrderForm placeOrderForm, final Model model)
	{
		final String securityCode = placeOrderForm.getSecurityCode();
		boolean invalid = false;

		if (!wileyb2cCheckoutFacade.isDigitalSessionCart())
		{
			if (getCheckoutFlowFacade().hasNoDeliveryAddress())
			{
				GlobalMessages.addErrorMessage(model, "checkout.deliveryAddress.notSelected");
				invalid = true;
			}

			if (getCheckoutFlowFacade().hasNoDeliveryMode() && !wileyb2cCheckoutFacade.getSupportedDeliveryModes().isEmpty())
			{
				GlobalMessages.addErrorMessage(model, "checkout.deliveryMethod.notSelected");
				invalid = true;
			}
		}

		if (!wileyb2cCheckoutFacade.isZeroTotalOrder() && getCheckoutFlowFacade().hasNoPaymentInfo())
		{
			GlobalMessages.addErrorMessage(model, "checkout.paymentMethod.notSelected");
			invalid = true;
		}
		else
		{
			// Only require the Security Code to be entered on the summary page if the SubscriptionPciOption is set to Default.
			if (CheckoutPciOptionEnum.DEFAULT.equals(getCheckoutFlowFacade().getSubscriptionPciOption())
					&& StringUtils.isBlank(securityCode))
			{
				GlobalMessages.addErrorMessage(model, "checkout.paymentMethod.noSecurityCode");
				invalid = true;
			}
		}

		if (!placeOrderForm.isTermsCheck())
		{
			GlobalMessages.addErrorMessage(model, "checkout.error.terms.not.accepted");
			invalid = true;
			return invalid;
		}
		final CartData cartData = getCheckoutFacade().getCheckoutCart();

		if (!cartData.isCalculated())
		{
			LOG.error(String.format("Cart %s has a calculated flag of FALSE, placement of order can't continue",
					cartData.getCode()));
			GlobalMessages.addErrorMessage(model, "checkout.error.cart.notcalculated");
			invalid = true;
		}

		return invalid;
	}

	@RequestMapping(value = "/back", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String back(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().previousStep();
	}

	@RequestMapping(value = "/next", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String next(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().nextStep();
	}

	protected CheckoutStep getCheckoutStep()
	{
		return getCheckoutStep(SUMMARY);
	}

	private void addVoucherDataToPage(final Model model)
	{
		final VoucherData voucher = couponFacade.getVoucherForCart();

		if (voucher != null)
		{
			model.addAttribute("voucherRedeemed", true);
			model.addAttribute("voucherCode", voucher.getVoucherCode());
			//Code below is commented in scope of migration to new coupons: ECSC-21150
			//model.addAttribute("voucherMessage", wileyVoucherFacade.formatVoucherDescription(voucher));
		}
	}

	private void addEditedVoucherDataToPage(final RedirectAttributes redirectAttributes,
			final boolean voucherDenied, final List<String> voucherViolationMessages)
	{
		redirectAttributes.addFlashAttribute("voucherDenied", voucherDenied);
		redirectAttributes.addFlashAttribute("voucherInputClass", voucherDenied ? "denied" : "accepted");
		redirectAttributes.addFlashAttribute("voucherViolationMessages", voucherViolationMessages);
	}
}
