package com.wiley.wileyb2c.storefront.checkout.steps.validation.impl;

import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.AbstractCheckoutStepValidator;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.facades.order.WileycomCheckoutFacade;
import com.wiley.facades.subscription.impl.Wileyb2cSubscriptionFacadeImpl;


public abstract class Wileyb2cAbstractCheckoutStepValidator extends AbstractCheckoutStepValidator
{
	protected static final Logger LOG = LoggerFactory.getLogger(Wileyb2cSubscriptionFacadeImpl.class);

	private WileycomCheckoutFacade wileycomCheckoutFacade;

	ValidationResults validateDigitalCart(final RedirectAttributes redirectAttributes)
	{
		if (getCheckoutFlowFacade().hasNoDeliveryAddress())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER,
					"checkout.multi.deliveryAddress.notprovided");
			return ValidationResults.REDIRECT_TO_DELIVERY_ADDRESS;
		}

		if (getCheckoutFlowFacade().hasNoDeliveryMode() && !wileycomCheckoutFacade.getSupportedDeliveryModes().isEmpty())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER,
					"checkout.multi.deliveryMethod.notprovided");
			return ValidationResults.REDIRECT_TO_DELIVERY_METHOD;
		}
		return null;
	}

	@Autowired
	public void setWileycomCheckoutFacade(final WileycomCheckoutFacade wileycomCheckoutFacade)
	{
		this.wileycomCheckoutFacade = wileycomCheckoutFacade;
	}
}
