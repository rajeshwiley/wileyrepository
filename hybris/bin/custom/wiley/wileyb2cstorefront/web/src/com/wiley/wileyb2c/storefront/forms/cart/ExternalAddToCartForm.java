package com.wiley.wileyb2c.storefront.forms.cart;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.wiley.wileyb2c.storefront.util.impl.Wileyb2cValueParserImpl;


/**
 * Describes parameters for adding product from external system.<br/>
 * https://confluence.wiley.ru/display/ECSC/P2+-+hy-IN-064+External+add+to+cart+v2
 */
public class ExternalAddToCartForm
{

	/**
	 * products to add to cart. It is a tokenized string with following format <productCode1>:<qty1>|<productCode2>:
	 * <qty2>.<br/>
	 * for subscription products subscription term should be provided as 3rd attribute, valid string: <productIsbn1>:
	 * <qty1>|<productIsbn2>:<qty2>|<subscriptionIsbn>:<qty3>:<subscriptionTermCode></>.<br/>
	 * The productCode should be a valid 13-digit ISBN
	 * (https://en.wikipedia.org/wiki/International_Standard_Book_Number).<br/>
	 * Examples 9992158107431:1|9604250590321:2 9992158107431:1|9604250590321:2|9604250590311:1:weekly_1
	 */
	@NotNull
	@Pattern(regexp = Wileyb2cValueParserImpl.STRICT_PRODUCTS_PATTERN)
	private String products;

	private String promoCode;

	private String returnURL;

	private String country;

	private Boolean cleanUpCart;

	private String additionalInfo;

	public String getProducts()
	{
		return products;
	}

	public ExternalAddToCartForm setProducts(final String products)
	{
		this.products = products;
		return this;
	}

	public String getPromoCode()
	{
		return promoCode;
	}

	public ExternalAddToCartForm setPromoCode(final String promoCode)
	{
		this.promoCode = promoCode;
		return this;
	}

	public String getReturnURL()
	{
		return returnURL;
	}

	public ExternalAddToCartForm setReturnURL(final String returnURL)
	{
		this.returnURL = returnURL;
		return this;
	}

	public String getCountry()
	{
		return country;
	}

	public ExternalAddToCartForm setCountry(final String country)
	{
		this.country = country;
		return this;
	}

	public Boolean getCleanUpCart()
	{
		return cleanUpCart;
	}

	public ExternalAddToCartForm setCleanUpCart(final Boolean cleanUpCart)
	{
		this.cleanUpCart = cleanUpCart;
		return this;
	}

	public String getAdditionalInfo()
	{
		return additionalInfo;
	}

	public ExternalAddToCartForm setAdditionalInfo(final String additionalInfo)
	{
		this.additionalInfo = additionalInfo;
		return this;
	}
}
