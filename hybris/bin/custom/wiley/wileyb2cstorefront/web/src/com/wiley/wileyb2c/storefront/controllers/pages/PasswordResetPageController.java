/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wileyb2c.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.ForgottenPwdForm;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commerceservices.customer.TokenInvalidatedException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.customer.ResetPasswordRedirectType;
import com.wiley.core.wileycom.exceptions.PasswordUpdateException;
import com.wiley.facades.customer.WileycomCustomerFacade;
import com.wiley.facades.wileyb2c.customer.Wileyb2cCustomerFacade;
import com.wiley.wileyb2c.storefront.controllers.ControllerConstants;
import com.wiley.wileycom.storefrontcommons.forms.WileycomUpdatePasswordForm;


/**
 * Controller for the forgotten password pages. Supports requesting a password reset email as well as changing the
 * password once you have got the token that was sent via email.
 */
@Controller
@Scope("tenant")
@RequestMapping(value = "/login/pw")
@Profile(WileyCoreConstants.WILEY_PHASE2A_PROFILE_NAME)
public class PasswordResetPageController extends AbstractPageController
{
	private static final Logger LOG = LoggerFactory.getLogger(PasswordResetPageController.class);

	private static final String REDIRECT_PWD_REQ_CONF = "redirect:/login/pw/request/external/conf";
	private static final String REDIRECT_LOGIN = "redirect:/login";
	private static final String REDIRECT_HOME = "redirect:/";
	private static final String UPDATE_PWD_CMS_PAGE = "updatePassword";
	private static final String EMAIL = "email";
	private static final String FORGOTTEN_PWD_INVALID_EMAIL = "forgottenPwd.email.invalid";
	private static final String FORGOTTEN_PWD_EMAIL_ACCOUNT_NOT_FOUND = "forgottenPwd.email.not.found";
	private static final String RESET_PASSWORD_CMS_PAGE_LABEL = "reset-password";
	
	/**
	 * Should be the same as default value of ResetPasswordRedirectType
	 */
	private static final String DEFAULT_REDIRECT_TYPE = "lp";

	@Resource(name = "customerFacade")
	private WileycomCustomerFacade customerFacade;

	@Resource
	private Wileyb2cCustomerFacade wileyb2cCustomerFacade;

	@Resource(name = "simpleBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder resourceBreadcrumbBuilder;

	@RequestMapping(value = "/request", method = RequestMethod.GET)
	public String getPasswordRequest(final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute(new ForgottenPwdForm());
		return ControllerConstants.Views.Fragments.Password.PASSWORD_RESET_REQUEST_POPUP;
	}

	@RequestMapping(value = "/request", method = RequestMethod.POST)
	public String passwordRequest(@Valid final ForgottenPwdForm form, final BindingResult bindingResult, final Model model,
			@RequestParam(defaultValue = DEFAULT_REDIRECT_TYPE, value = "t") final ResetPasswordRedirectType redirectType)
			throws CMSItemNotFoundException
	{
		if (bindingResult.hasErrors())
		{
			return ControllerConstants.Views.Fragments.Password.PASSWORD_RESET_REQUEST_FORM;
		}
		else
		{
			try
			{
				wileyb2cCustomerFacade.forgottenPassword(form.getEmail(), redirectType);
			}
			catch (final UnknownIdentifierException | IllegalArgumentException | IllegalStateException exc)
			{
				LOG.warn("Email {} does not exist in database", form.getEmail(), exc);
				GlobalMessages.addErrorMessage(model, FORGOTTEN_PWD_EMAIL_ACCOUNT_NOT_FOUND);
				bindingResult.rejectValue(EMAIL, FORGOTTEN_PWD_INVALID_EMAIL);
				return ControllerConstants.Views.Fragments.Password.PASSWORD_RESET_REQUEST_FORM;
			}
			return ControllerConstants.Views.Fragments.Password.FORGOT_PASSWORD_VALIDATION_MESSAGE;
		}
	}

	@RequestMapping(value = "/request/external", method = RequestMethod.GET)
	public String getExternalPasswordRequest(final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute(new ForgottenPwdForm());
		storeCmsPageInModel(model, getContentPageForLabelOrId(null));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(null));
		model.addAttribute(WebConstants.BREADCRUMBS_KEY, resourceBreadcrumbBuilder.getBreadcrumbs("forgottenPwd.title"));
		return ControllerConstants.Views.Pages.Password.PASSWORD_RESET_REQUEST;
	}

	@RequestMapping(value = "/request/external/conf", method = RequestMethod.GET)
	public String getExternalPasswordRequestConf(final Model model) throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(null));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(null));
		model.addAttribute(WebConstants.BREADCRUMBS_KEY, resourceBreadcrumbBuilder.getBreadcrumbs("forgottenPwd.title"));
		return ControllerConstants.Views.Pages.Password.PASSWORD_RESET_REQUEST_CONFIRMATION;
	}

	@RequestMapping(value = "/request/external", method = RequestMethod.POST)
	public String externalPasswordRequest(@Valid final ForgottenPwdForm form, final BindingResult bindingResult,
			final Model model, final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(null));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(null));
		model.addAttribute(WebConstants.BREADCRUMBS_KEY, resourceBreadcrumbBuilder.getBreadcrumbs("forgottenPwd.title"));

		if (bindingResult.hasErrors())
		{
			return ControllerConstants.Views.Pages.Password.PASSWORD_RESET_REQUEST;
		}
		else
		{
			try
			{
				customerFacade.forgottenPassword(form.getEmail());
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
						"account.confirmation.forgotten.password.link.sent");
			}
			catch (final UnknownIdentifierException unknownIdentifierException)
			{
				LOG.warn("Email: " + form.getEmail() + " does not exist in the database.");
			}
			return REDIRECT_PWD_REQ_CONF;
		}
	}

	@RequestMapping(value = "/change", method = RequestMethod.GET)
	public String getChangePassword(@RequestParam(required = false) final String token, final Model model)
			throws CMSItemNotFoundException
	{
		if (StringUtils.isBlank(token))
		{
			return REDIRECT_HOME;
		}
		final WileycomUpdatePasswordForm form = new WileycomUpdatePasswordForm();
		form.setToken(token);
		model.addAttribute(form);
		storeCmsPageInModel(model, getContentPageForLabelOrId(RESET_PASSWORD_CMS_PAGE_LABEL));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(UPDATE_PWD_CMS_PAGE));
		model.addAttribute(WebConstants.BREADCRUMBS_KEY, resourceBreadcrumbBuilder.getBreadcrumbs("updatePwd.title"));
		return ControllerConstants.Views.Pages.Password.PASSWORD_RESET_CHANGE_PAGE;
	}

	@RequestMapping(value = "/change", method = RequestMethod.POST)
	public String changePassword(@Valid final WileycomUpdatePasswordForm form,
			final BindingResult bindingResult, final Model model,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		validatePwdAndCheckPwdEqual(form, bindingResult);
		if (bindingResult.hasErrors())
		{
			prepareErrorMessage(model, UPDATE_PWD_CMS_PAGE);
			storeCmsPageInModel(model, getContentPageForLabelOrId(RESET_PASSWORD_CMS_PAGE_LABEL));
			return ControllerConstants.Views.Pages.Password.PASSWORD_RESET_CHANGE_PAGE;
		}
		if (!StringUtils.isBlank(form.getToken()))
		{
			try
			{
				customerFacade.updatePassword(form.getToken(), form.getPwd());
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
						"account.confirmation.password.updated");
			}
			catch (final TokenInvalidatedException e)
			{
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
						"updatePwd.token.invalidated");
			}
			catch (final PasswordUpdateException | IllegalArgumentException e)
			{
				if (LOG.isDebugEnabled())
				{
					LOG.debug("Password update process failed!", e);
				}
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
						"externalService.response.error");
			}
			catch (final RuntimeException e)
			{
				LOG.info(e.getMessage(), e);
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "updatePwd.token.invalid");
			}
		}
		return REDIRECT_LOGIN;
	}

	/**
	 * Prepares the view to display an error message
	 *
	 * @throws CMSItemNotFoundException
	 */
	protected void prepareErrorMessage(final Model model, final String page) throws CMSItemNotFoundException
	{
		GlobalMessages.addErrorMessage(model, "form.global.error");
		storeCmsPageInModel(model, getContentPageForLabelOrId(page));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(page));
	}

	private void validatePwdAndCheckPwdEqual(final WileycomUpdatePasswordForm form, final BindingResult bindingResult)
	{
		if (!form.getPwd().equals(form.getCheckPwd()))
		{
			bindingResult.rejectValue("checkPwd", "resetPwd.validation.checkPwd.equals", new Object[] {},
					"resetPwd.validation.checkPwd.equals");
		}
	}
}
