package com.wiley.wileyb2c.storefront.filters;

import de.hybris.platform.jalo.JaloSession;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.springframework.web.filter.GenericFilterBean;

import com.wiley.core.wileyb2c.jalo.Wileyb2cEurope1PriceFactory;


/**
 * Session filter to adjust europe price factory in session (supports jalo layer)
 */
public class Wileyb2cPriceFilter extends GenericFilterBean
{

	@Resource
	private Wileyb2cEurope1PriceFactory wileyb2cEurope1PriceFactory;


	@Override
	public void doFilter(final ServletRequest servletRequest, final ServletResponse servletResponse,
			final FilterChain filterChain)
			throws IOException, ServletException
	{
		JaloSession.getCurrentSession().setPriceFactory(wileyb2cEurope1PriceFactory);
		filterChain.doFilter(servletRequest, servletResponse);
	}
}
