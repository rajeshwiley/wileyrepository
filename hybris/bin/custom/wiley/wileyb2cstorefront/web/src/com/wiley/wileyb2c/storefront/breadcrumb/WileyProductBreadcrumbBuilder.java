package com.wiley.wileyb2c.storefront.breadcrumb;

import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.ProductBreadcrumbBuilder;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;


public class WileyProductBreadcrumbBuilder extends ProductBreadcrumbBuilder
{
	private static final String LAST_LINK_CLASS = "clickable";

	private List<String> categoryCodes;

	public List<Breadcrumb> getBreadcrumbs(final String productCode)
	{
		final ProductModel productModel = getProductService().getProductForCode(productCode);
		final List<Breadcrumb> breadcrumbs = new ArrayList<>();

		final Collection<CategoryModel> categoryModels = new ArrayList<>();

		final ProductModel baseProductModel = getBaseProduct(productModel);
		categoryModels.addAll(baseProductModel.getSupercategories());

		while (!categoryModels.isEmpty())
		{

			CategoryModel toDisplay = getCategoryForHierarchy(categoryModels);
			if (toDisplay == null)
			{
				toDisplay = processCategoryModels(categoryModels, null);
			}
			categoryModels.clear();
			if (toDisplay != null)
			{
				breadcrumbs.add(getCategoryBreadcrumb(toDisplay));
				categoryModels.addAll(toDisplay.getSupercategories());
			}
		}

		if (CollectionUtils.isNotEmpty(breadcrumbs))
		{
			breadcrumbs.get(0).setLinkClass(LAST_LINK_CLASS);
		}
		Collections.reverse(breadcrumbs);

		return breadcrumbs;
	}


	private CategoryModel getCategoryForHierarchy(final Collection<CategoryModel> allProductCategories)
	{
		for (String categoryCode : categoryCodes)
		{
			Optional<CategoryModel> category = allProductCategories
					.stream()
					.filter(productCategory -> categoryBelongsToTopLevelCategory(categoryCode, productCategory))
					.findFirst();
			if (category.isPresent())
			{
				return category.get();
			}
		}
		return null;
	}

	private boolean categoryBelongsToTopLevelCategory(final String superCategoryCode, final CategoryModel category)
	{
		boolean categoryIsATopLevelCategory = superCategoryCode.equals(category.getCode());

		if (categoryIsATopLevelCategory)
		{
			return true;
		}
		boolean categoryHastTopLevelCategoryInSuper =
				CollectionUtils.isNotEmpty(category.getAllSupercategories())
						&& category.getAllSupercategories()
						.stream()
						.anyMatch(
								superCategory -> superCategoryCode.equals(superCategory.getCode())
						);
		return categoryHastTopLevelCategoryInSuper;
	}

	@Required
	public void setCategoryCodes(final List<String> categoryCodes)
	{
		this.categoryCodes = categoryCodes;
	}
}
