package com.wiley.wileyb2c.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.forms.GuestForm;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.wileyb2c.storefront.controllers.ControllerConstants;


@Controller
@Profile(WileyCoreConstants.WILEY_PHASE2A_PROFILE_NAME)
@RequestMapping(value = "/paypal/login")
public class PayPalLoginPageController extends CheckoutLoginController
{
	public static final String PAY_PAL_CHECKOUT_URL = "/paypal/checkout/hop/expressCheckoutShortcut";

	private static final String PAY_PAL_AUTHENTICATED_CHECKOUT_URL = "/paypal/checkout/hop/authenticatedExpressCheckoutShortcut";

	@RequestMapping(value = "/guestWithPayPalRedirect", method = RequestMethod.POST)
	public String doAnonymousCheckout(final GuestForm form, final BindingResult bindingResult,
			final Model model, final RedirectAttributes redirectAttributes,
			final HttpSession session, final HttpServletRequest request, final HttpServletResponse response)
			throws CMSItemNotFoundException
	{
		return super.doAnonymousCheckout(form, bindingResult, model, session, request, response);
	}

	@Override
	protected String getView()
	{
		return ControllerConstants.Views.Pages.PayPal.PAY_PAL_LOGIN_PAGE;
	}

	@Override
	protected String getSuccessRedirect(final HttpServletRequest request, final HttpServletResponse response)
	{
		return PAY_PAL_AUTHENTICATED_CHECKOUT_URL;
	}
}
