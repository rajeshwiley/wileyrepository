package com.wiley.wileyb2c.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractSearchPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.core.model.order.OrderEntryModel;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.facades.wileyb2c.access.Wileyb2cDownloadDigitalProductFacade;
import com.wiley.facades.wileyb2c.customer.Wileyb2cCustomerFacade;
import com.wiley.facades.wileyb2c.order.Wileyb2cOrderFacade;

@Controller
@Profile(WileyCoreConstants.WILEY_PHASE2A_PROFILE_NAME)
@RequestMapping("/my-account")
public class AccountDigitalProductsPageController extends AbstractSearchPageController
{
	private static final String MY_DIGITAL_PRODUCTS_CMS_PAGE = "digital-products";
	private static final ShowMode SHOW_MODE = ShowMode.Page;

	private static final Logger LOG = Logger.getLogger(AccountDigitalProductsPageController.class);
	private static final String BY_DATE = "byDate";
	private static final String REDIRECT_TO_DIGITAL_PRODUCTS = "/my-account/digital-products";

	@Value("${account.digital.products.itemsPerPage}")
	private int itemsPerPage;

	@Value("${account.digital.products.purchaseDateFormat}")
	private String purchaseDateFormat;
	@Resource
	private Wileyb2cDownloadDigitalProductFacade wileyb2cDownloadDigitalProductFacade;

	@Resource
	private Wileyb2cCustomerFacade wileyb2cCustomerFacade;
	@Resource
	private Wileyb2cOrderFacade wileyb2cOrderFacade;

	/**
	 * Get current user purchased digital products.
	 *
	 * @param model
	 * 		the model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 * 		the cms item not found exception
	 */
	@RequestMapping(value = "/digital-products", method = RequestMethod.GET)
	@RequireHardLogIn
	public String digitalProducts(@RequestParam(value = "page", defaultValue = "0") final int page,
			final Model model) throws CMSItemNotFoundException
	{
		final PageableData pageableData = createPageableData(page,
				itemsPerPage, BY_DATE, SHOW_MODE);

		storeCmsPageInModel(model, getContentPageForLabelOrId(MY_DIGITAL_PRODUCTS_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MY_DIGITAL_PRODUCTS_CMS_PAGE));

		model.addAttribute("orderEntries", wileyb2cCustomerFacade.getDigitalOrderEntriesData(pageableData));

		return getViewForPage(model);
	}

	@RequestMapping(value = "/digital-products/access", method = RequestMethod.GET)
	@RequireHardLogIn
	public String redirectToDigitalProduct(@RequestParam(value = "orderCode") final String orderCode,
			@RequestParam(value = "entryNumber") final Integer entryNumber, final RedirectAttributes redirectAttributes)
	{
		try
		{
			OrderEntryModel orderEntry = wileyb2cOrderFacade.getOrderEntryByOrderCodeAndEntryNumber(
					orderCode, entryNumber);
			return REDIRECT_PREFIX + wileyb2cDownloadDigitalProductFacade.generateRedirectUrl(orderEntry);
		}
		catch (Exception e)
		{

			LOG.error(String.format("Error during access order entry  with entry number %s "
					+ "and order code %s", entryNumber, orderCode), e);
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"text.account.myDigitalProducts.access.failed");
			return REDIRECT_PREFIX + REDIRECT_TO_DIGITAL_PRODUCTS;
		}
	}

	@ModelAttribute("purchaseDateFormat")
	public String getPurchaseDateFormat()
	{
		return purchaseDateFormat;
	}
}
