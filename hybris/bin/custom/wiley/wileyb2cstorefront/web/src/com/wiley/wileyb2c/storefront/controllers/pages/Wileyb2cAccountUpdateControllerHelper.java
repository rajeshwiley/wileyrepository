package com.wiley.wileyb2c.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractCheckoutController;

import java.util.ArrayList;
import java.util.List;

public class Wileyb2cAccountUpdateControllerHelper
{

	private static final int USER_PROFILE_START_YEAR = 1930;
	private static final int USER_PROFILE_END_YEAR = 2030;


	public List<AbstractCheckoutController.SelectOption> getMonths()
	{
		final List<AbstractCheckoutController.SelectOption> months = new ArrayList<>();

		months.add(new AbstractCheckoutController.SelectOption("1", "Jan"));
		months.add(new AbstractCheckoutController.SelectOption("2", "Feb"));
		months.add(new AbstractCheckoutController.SelectOption("3", "Mar"));
		months.add(new AbstractCheckoutController.SelectOption("4", "Apr"));
		months.add(new AbstractCheckoutController.SelectOption("5", "May"));
		months.add(new AbstractCheckoutController.SelectOption("6", "Jun"));
		months.add(new AbstractCheckoutController.SelectOption("7", "Jul"));
		months.add(new AbstractCheckoutController.SelectOption("8", "Aug"));
		months.add(new AbstractCheckoutController.SelectOption("9", "Sep"));
		months.add(new AbstractCheckoutController.SelectOption("10", "Oct"));
		months.add(new AbstractCheckoutController.SelectOption("11", "Nov"));
		months.add(new AbstractCheckoutController.SelectOption("12", "Dec"));

		return months;
	}

	public List<AbstractCheckoutController.SelectOption> getYears()
	{
		final List<AbstractCheckoutController.SelectOption> years = new ArrayList<>();

		for (int i = USER_PROFILE_START_YEAR; i <= USER_PROFILE_END_YEAR; i++)
		{
			years.add(new AbstractCheckoutController.SelectOption(String.valueOf(i), String.valueOf(i)));
		}

		return years;
	}
}
