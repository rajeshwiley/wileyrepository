package com.wiley.wileyb2c.storefront.controllers.cms;

import de.hybris.platform.acceleratorcms.model.components.CMSTabParagraphContainerModel;
import de.hybris.platform.servicelayer.session.SessionService;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.wileyb2c.storefront.controllers.ControllerConstants;


@Controller("CMSTabParagraphContainerController")
@RequestMapping(value = ControllerConstants.Actions.Cms.CMS_TAB_PARAGRAPH_CONTAINER)
public class CMSTabParagraphContainerController extends AbstractCMSComponentController<CMSTabParagraphContainerModel>
{
	@Autowired
	private SessionService sessionService;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final CMSTabParagraphContainerModel container)
	{
		final String key = WileyCoreConstants.TAB_CONTAINER_TABS + container.getUid();
		final Object tabComponents = sessionService.getCurrentSession().getAttribute(key);
		sessionService.removeAttribute(key);
		if (tabComponents != null)
		{
			model.addAttribute("tabs", tabComponents);
		}
		else
		{
			model.addAttribute("tabs", container.getSimpleCMSComponents());
		}
		model.addAttribute("container", container);
	}
}
