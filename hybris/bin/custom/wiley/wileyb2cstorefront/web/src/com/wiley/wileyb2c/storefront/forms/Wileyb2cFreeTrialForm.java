package com.wiley.wileyb2c.storefront.forms;

/**
 * Represents free trial form for b2c customer
 */
public class Wileyb2cFreeTrialForm
{
	private String firstName;
	private String lastName;
	private String email;


	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(final String firstName)
	{
		this.firstName = firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(final String lastName)
	{
		this.lastName = lastName;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(final String email)
	{
		this.email = email;
	}
}
