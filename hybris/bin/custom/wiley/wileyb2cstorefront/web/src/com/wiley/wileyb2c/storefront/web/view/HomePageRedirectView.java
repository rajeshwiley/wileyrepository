package com.wiley.wileyb2c.storefront.web.view;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.view.RedirectView;


public class HomePageRedirectView extends RedirectView
{
	public HomePageRedirectView()
	{
	}

	public HomePageRedirectView(final String url)
	{
		super(url);
	}

	public HomePageRedirectView(final String url, final boolean contextRelative)
	{
		super(url, contextRelative);
	}

	public HomePageRedirectView(final String url, final boolean contextRelative, final boolean http10Compatible)
	{
		super(url, contextRelative, http10Compatible);
	}

	public HomePageRedirectView(final String url, final boolean contextRelative, final boolean http10Compatible,
			final boolean exposeModelAttributes)
	{
		super(url, contextRelative, http10Compatible, exposeModelAttributes);
	}

	@Override
	protected void sendRedirect(final HttpServletRequest request, final HttpServletResponse response, final String targetUrl,
			final boolean http10Compatible) throws IOException
	{
		final String modifiedTargetUrl;
		//as we here then need to remove last slash in targetUrl (ECSC-23183)
		if (targetUrl.length() > 1 && targetUrl.endsWith("/"))
		{
			//remove last char "/"
			int stopPosition = targetUrl.length() - 1;
			modifiedTargetUrl = targetUrl.substring(0, stopPosition);
		}
		else
		{
			//use original Url
			modifiedTargetUrl = targetUrl;
		}
		super.sendRedirect(request, response, modifiedTargetUrl, http10Compatible);
	}
}
