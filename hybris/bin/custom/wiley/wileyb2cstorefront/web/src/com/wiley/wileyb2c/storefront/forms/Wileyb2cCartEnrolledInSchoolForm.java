package com.wiley.wileyb2c.storefront.forms;

/**
 * Represents enrolled in school form in cart for b2c customer
 */
public class Wileyb2cCartEnrolledInSchoolForm
{
	private String school;
	private boolean isEnrolledInSchool;

	public String getSchool()
	{
		return school;
	}

	public boolean isEnrolledInSchool()
	{
		return isEnrolledInSchool;
	}

	public void setSchool(final String school)
	{
		this.school = school;
	}

	public void setEnrolledInSchool(final boolean enrolledInSchool)
	{
		isEnrolledInSchool = enrolledInSchool;
	}
}
