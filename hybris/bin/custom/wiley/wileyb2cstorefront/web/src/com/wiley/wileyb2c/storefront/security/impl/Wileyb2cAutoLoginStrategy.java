package com.wiley.wileyb2c.storefront.security.impl;

import de.hybris.platform.acceleratorstorefrontcommons.security.AutoLoginStrategy;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;

import com.wiley.facades.wileyb2c.customer.Wileyb2cCustomerFacade;
import com.wiley.storefrontcommons.security.impl.WileyAutoLoginStrategy;


/**
 * Default implementation of {@link AutoLoginStrategy}
 */
public class Wileyb2cAutoLoginStrategy extends WileyAutoLoginStrategy
{
	private Wileyb2cCustomerFacade customerFacade;

	@Override
	public void login(final String username, final String password, final HttpServletRequest request,
			final HttpServletResponse response)
	{
		final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
		token.setDetails(new WebAuthenticationDetails(request));
		try
		{
			final Authentication authentication = getAuthenticationManager().authenticate(token);
			SecurityContextHolder.getContext().setAuthentication(authentication);
			getCustomerFacade().loginSuccess();
			getGuidCookieStrategy().setCookie(request, response);
			getRememberMeServices().loginSuccess(request, response, token);
		}
		catch (final Exception e)
		{
			getRememberMeServices().loginFail(request, response);
			getGuidCookieStrategy().deleteCookie(request, response);
			SecurityContextHolder.getContext().setAuthentication(null);
			getCustomerFacade().loginFailure();
			LOG.error("Failure during autoLogin", e);
		}
	}

	public Wileyb2cCustomerFacade getCustomerFacade()
	{
		return customerFacade;
	}

	@Required
	public void setCustomerFacade(final Wileyb2cCustomerFacade customerFacade)
	{
		this.customerFacade = customerFacade;
	}
}
