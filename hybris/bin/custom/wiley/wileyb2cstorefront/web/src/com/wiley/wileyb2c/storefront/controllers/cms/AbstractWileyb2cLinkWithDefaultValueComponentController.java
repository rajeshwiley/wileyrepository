package com.wiley.wileyb2c.storefront.controllers.cms;

import de.hybris.platform.acceleratorstorefrontcommons.tags.Functions;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.ui.Model;

import com.wiley.core.model.Wileyb2cLinkWithDefaultValueComponentModel;


/**
 * @author Dzmitryi_Halahayeu
 */
abstract class AbstractWileyb2cLinkWithDefaultValueComponentController<T extends Wileyb2cLinkWithDefaultValueComponentModel>
		extends AbstractCMSComponentController<T>
{
	@Resource
	private Converter<ProductModel, ProductData> productUrlConverter;
	@Resource
	private Converter<CategoryModel, CategoryData> categoryUrlConverter;


	@Override
	protected void fillModel(final HttpServletRequest request, final Model model,
			final T component)
	{
		String redirectURL = getUrlValue();
		String homeText = getUrlText();
		final String url = StringUtils.isNoneEmpty(redirectURL) ? redirectURL : getUrl(component);
		String linkName = StringUtils.isNoneEmpty(homeText) ? homeText : component.getLinkName();
		model.addAttribute("redirectURL", url);
		model.addAttribute("linkName", linkName);
	}

	protected abstract String getUrlText();

	protected abstract String getUrlValue();

	protected String getUrl(final CMSLinkComponentModel component)
	{
		return Functions.getUrlForCMSLinkComponent(component, productUrlConverter, categoryUrlConverter);
	}

}
