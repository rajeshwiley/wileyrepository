package com.wiley.wileyb2c.storefront.controllers.cms;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.wiley.core.model.components.CookieConsentComponentModel;
import com.wiley.wileyb2c.storefront.controllers.ControllerConstants;


/**
 * @author Maksim_Kozich
 */
@Controller("CookieConsentComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.COOKIE_CONSENT_COMPONENT)
public class CookieConsentComponentController extends
		AbstractCMSComponentController<CookieConsentComponentModel>
{

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model,
			final CookieConsentComponentModel component)
	{
	}
}
