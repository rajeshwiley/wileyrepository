package com.wiley.wileyb2c.storefront.context.impl;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;

import java.net.MalformedURLException;
import java.net.URL;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.storefrontcommons.context.impl.WileyContextInformationLoaderImpl;
import com.wiley.facades.wileyb2c.order.Wileyb2cCartFacade;


/**
 * Loads cms site based on OOTB rules and, if the site is not found, the implementation uses default site.
 */
public class Wileyb2cContextInformationLoaderImpl extends WileyContextInformationLoaderImpl
{

	private static final Logger LOG = LoggerFactory.getLogger(Wileyb2cContextInformationLoaderImpl.class);

	@Resource
	private Wileyb2cCartFacade wileyb2cCartFacade;

	@Value("${" + WileyCoreConstants.WILEYB2CSTOREFRONT_DEFAULT_SITE_PROPERTY + "}")
	private String defaultSiteUid;

	@Override
	public CMSSiteModel initializeSiteFromRequest(final String absoluteURL)
	{

		if (LOG.isDebugEnabled())
		{
			LOG.debug(String.format("Initializing site for URL [%s].", absoluteURL));
		}

		final CMSSiteService cmsSiteService = getCMSSiteService();
		CMSSiteModel cmsSiteModel = null;
		try
		{
			final URL currentURL = new URL(absoluteURL);
			cmsSiteModel = cmsSiteService.getSiteForURL(currentURL);
			setCurrentSite(cmsSiteModel);
		}
		catch (final MalformedURLException e)
		{
			if (LOG.isDebugEnabled())
			{
				LOG.debug("Cannot find CMSSite associated with current URL ( " + absoluteURL
						+ " - check whether this is correct URL) !");
			}
		}
		catch (final CMSItemNotFoundException e)
		{
			if (LOG.isDebugEnabled())
			{
				LOG.debug("Cannot find CMSSite associated with current URL (" + absoluteURL + ")! Trying to use default.");
			}

			if (StringUtils.isEmpty(defaultSiteUid))
			{
				throw new IllegalStateException(
						String.format("Wrong configuration. Property [%s] is empty.",
								WileyCoreConstants.WILEYB2CSTOREFRONT_DEFAULT_SITE_PROPERTY));
			}

			cmsSiteModel = getDefaultCmsSite(cmsSiteService);

			if (cmsSiteModel != null)
			{
				setCurrentSite(cmsSiteModel);
			}
		}

		if (LOG.isDebugEnabled())
		{
			LOG.debug(String.format("Context has been initialized for site [%s]",
					cmsSiteModel != null ? cmsSiteModel.getUid() : null));
		}

		return cmsSiteModel;
	}

	private void setCurrentSite(final CMSSiteModel cmsSiteModel)
	{
		/* reset continue url if site is changed to use redirectUrl from site.
		See com.wiley.core.wileyb2c.order.impl.Wileyb2cCommerceCartServiceImpl.getContinueUrlForCart()*/
		wileyb2cCartFacade.saveContinueUrl(null);
		getBaseSiteService().setCurrentBaseSite(cmsSiteModel, true);
	}

	private CMSSiteModel getDefaultCmsSite(final CMSSiteService cmsSiteService)
	{
		return cmsSiteService.getSites().stream()
				.filter(site -> defaultSiteUid.equals(site.getUid()))
				.findFirst()
				.orElse(null);
	}
}
