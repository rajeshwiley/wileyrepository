package com.wiley.wileyb2c.storefront.controllers.misc;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.AbstractController;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.wiley.wileyb2c.storefront.controllers.ControllerConstants;


/**
 * Controller for web robots instructions
 */
@Controller
public class RobotsController extends AbstractController
{
	// Number of seconds in one day
	private static final String ONE_DAY = String.valueOf(60 * 60 * 24);

	@Value("${website.wileyb2c.https}")
	private String websiteUrl;

	@RequestMapping(value = "/robots.txt", method = RequestMethod.GET)
	public String getRobots(final HttpServletResponse response, final Model model)
	{
		model.addAttribute("websiteUrl", websiteUrl);
		// Add cache control header to cache response for a day
		response.setHeader("Cache-Control", "public, max-age=" + ONE_DAY);
		return ControllerConstants.Views.Pages.Misc.MISC_ROBOTS_PAGE;
	}
}
