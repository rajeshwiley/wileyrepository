/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wileyb2c.storefront.checkout.steps.validation.impl;

import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.facades.order.WileycomCheckoutFacade;


public class ResponsivePaymentCheckoutStepValidator extends Wileyb2cAbstractCheckoutStepValidator
{
	@Autowired
	private WileycomCheckoutFacade wileycomCheckoutFacade;

	@Override
	/**
	 * Note, that methods hasNoDeliveryAddress() and hasNoDeliveryMode()
	 * use OOTB hasShippingItems(), that checks
	 * entry.deliveryPointOfService property
	 */
	public ValidationResults validateOnEnter(final RedirectAttributes redirectAttributes)
	{
		if (!getCheckoutFlowFacade().hasValidCart())
		{
			LOG.info("Missing, empty or unsupported cart");
			return ValidationResults.REDIRECT_TO_CART;
		}

		if (!wileycomCheckoutFacade.isDigitalSessionCart())
		{
			final ValidationResults digitalCartValidationResult = validateDigitalCart(redirectAttributes);
			if (digitalCartValidationResult != null) {
				return digitalCartValidationResult;
			}
		}

		if (wileycomCheckoutFacade.isZeroTotalOrder())
		{
			return ValidationResults.REDIRECT_TO_SUMMARY;
		}

		return ValidationResults.SUCCESS;
	}
}
