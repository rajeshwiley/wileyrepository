/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wileyb2c.storefront.controllers;

import de.hybris.platform.acceleratorcms.model.components.CMSTabParagraphContainerModel;
import de.hybris.platform.acceleratorcms.model.components.CartSuggestionComponentModel;
import de.hybris.platform.acceleratorcms.model.components.CategoryFeatureComponentModel;
import de.hybris.platform.acceleratorcms.model.components.DynamicBannerComponentModel;
import de.hybris.platform.acceleratorcms.model.components.MiniCartComponentModel;
import de.hybris.platform.acceleratorcms.model.components.NavigationBarComponentModel;
import de.hybris.platform.acceleratorcms.model.components.ProductFeatureComponentModel;
import de.hybris.platform.acceleratorcms.model.components.ProductReferencesComponentModel;
import de.hybris.platform.acceleratorcms.model.components.PurchasedCategorySuggestionComponentModel;
import de.hybris.platform.acceleratorcms.model.components.SimpleResponsiveBannerComponentModel;
import de.hybris.platform.acceleratorcms.model.components.SubCategoryListComponentModel;
import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;
import de.hybris.platform.cms2lib.model.components.ProductCarouselComponentModel;

import com.wiley.core.model.WileyImageCarouselComponentModel;
import com.wiley.core.model.Wileyb2cContinueShoppingLinkComponentModel;
import com.wiley.core.model.Wileyb2cHomeLinkComponentModel;
import com.wiley.core.model.Wileyb2cSiteLogoComponentModel;
import com.wiley.core.model.components.CookieConsentComponentModel;
import com.wiley.core.model.components.WileyBootstrapRowComponentModel;
import com.wiley.core.model.components.WileyBrightcoveVideoComponentModel;
import com.wiley.core.model.components.WileyHeroBannerComponentModel;
import com.wiley.core.model.components.WileyProductListComponentModel;
import com.wiley.core.model.components.WileyPurchaseOptionProductListComponentModel;


/**
 */
public interface ControllerConstants
{
	/**
	 * Class with model name constants
	 */
	interface Model
	{
		interface Attributes
		{
			String CUSTOM_LINK = "plpCustomLink";
		}
	}

	/**
	 * Class with action name constants
	 */
	interface Actions
	{
		interface Cms
		{
			String PREFIX = "/view/";
			String SUFFIX = "Controller";

			/**
			 * Default CMS component controller
			 */
			String DEFAULT_CMS_COMPONENT = PREFIX + "DefaultCMSComponentController";

			/**
			 * CMS components that have specific handlers
			 */
			String PURCHASED_CATEGORY_SUGGESTION_COMPONENT =
					PREFIX + PurchasedCategorySuggestionComponentModel._TYPECODE + SUFFIX;
			String CART_SUGGESTION_COMPONENT = PREFIX + CartSuggestionComponentModel._TYPECODE + SUFFIX;
			String PRODUCT_REFERENCES_COMPONENT = PREFIX + ProductReferencesComponentModel._TYPECODE + SUFFIX;
			String PRODUCT_CAROUSEL_COMPONENT = PREFIX + ProductCarouselComponentModel._TYPECODE + SUFFIX;
			String MINI_CART_COMPONENT = PREFIX + MiniCartComponentModel._TYPECODE + SUFFIX;
			String PRODUCT_FEATURE_COMPONENT = PREFIX + ProductFeatureComponentModel._TYPECODE + SUFFIX;
			String CATEGORY_FEATURE_COMPONENT = PREFIX + CategoryFeatureComponentModel._TYPECODE + SUFFIX;
			String NAVIGATION_BAR_COMPONENT = PREFIX + NavigationBarComponentModel._TYPECODE + SUFFIX;
			String CMS_LINK_COMPONENT = PREFIX + CMSLinkComponentModel._TYPECODE + SUFFIX;
			String DYNAMIC_BANNER_COMPONENT = PREFIX + DynamicBannerComponentModel._TYPECODE + SUFFIX;
			String SUB_CATEGORY_LIST_COMPONENT = PREFIX + SubCategoryListComponentModel._TYPECODE + SUFFIX;
			String SIMPLE_RESPONSIVE_BANNER_COMPONENT = PREFIX + SimpleResponsiveBannerComponentModel._TYPECODE + SUFFIX;
			String WILEY_B2C_SITE_LOGO_COMPONENT = PREFIX + Wileyb2cSiteLogoComponentModel._TYPECODE + SUFFIX;
			String WILEY_B2C_HOME_LINK_COMPONENT = PREFIX + Wileyb2cHomeLinkComponentModel._TYPECODE + SUFFIX;
			String WILEY_IMAGE_CAROUSEL_COMPONENT = PREFIX + WileyImageCarouselComponentModel._TYPECODE + SUFFIX;
			String WILEY_HERO_BANNER_COMPONENT = PREFIX + WileyHeroBannerComponentModel._TYPECODE + SUFFIX;
			String WILEY_B2C_CONTINUE_SHOPPING_LINK_COMPONENT =
					PREFIX + Wileyb2cContinueShoppingLinkComponentModel._TYPECODE + SUFFIX;
			String WILEY_PRODUCT_LIST_COMPONENT =
					PREFIX + WileyProductListComponentModel._TYPECODE + SUFFIX;
			String WILEY_PURCHASE_OPTION_PRODUCT_LIST_COMPONENT =
					PREFIX + WileyPurchaseOptionProductListComponentModel._TYPECODE + SUFFIX;
			String COOKIE_CONSENT_COMPONENT =
					PREFIX + CookieConsentComponentModel._TYPECODE + SUFFIX;
			String WILEY_BRIGHTCOVE_VIDEO_COMPONENT =
					PREFIX + WileyBrightcoveVideoComponentModel._TYPECODE + SUFFIX;
			String CMS_TAB_PARAGRAPH_CONTAINER = PREFIX + CMSTabParagraphContainerModel._TYPECODE + SUFFIX;
			String WILEY_BOOTSTRAP_ROW_COMPONENT = PREFIX + WileyBootstrapRowComponentModel._TYPECODE + SUFFIX; 
		}
	}

	/**
	 * Class with view name constants
	 */
	interface Views
	{
		interface Cms
		{
			String COMPONENT_PREFIX = "cms/";
		}

		interface AjaxPages
		{
			String SELECT_REFRESH_ORDER_TOTAL = "pages/checkout/multi/ajaxCalls/select-refresh-order-total";
		}

		interface Pages
		{
			interface Account
			{
				String ACCOUNT_LOGIN_PAGE = "pages/account/accountLoginPage";
				String ACCOUNT_HOME_PAGE = "pages/account/accountHomePage";
				String ACCOUNT_ORDER_HISTORY_PAGE = "pages/account/accountOrderHistoryPage";
				String ACCOUNT_ORDER_PAGE = "pages/account/accountOrderPage";
				String ACCOUNT_PROFILE_PAGE = "pages/account/accountProfilePage";
				String ACCOUNT_PROFILE_EDIT_PAGE = "pages/account/accountProfileEditPage";
				String ACCOUNT_PROFILE_EMAIL_EDIT_PAGE = "pages/account/accountProfileEmailEditPage";
				String ACCOUNT_CHANGE_PASSWORD_PAGE = "pages/account/accountChangePasswordPage";
				String ACCOUNT_ADDRESS_BOOK_PAGE = "pages/account/accountAddressBookPage";
				String ACCOUNT_EDIT_ADDRESS_PAGE = "pages/account/accountEditAddressPage";
				String ACCOUNT_PAYMENT_INFO_PAGE = "pages/account/accountPaymentInfoPage";
				String ACCOUNT_ADD_CARD_PAGE = "pages/account/accountAddCardPage";
				String ACCOUNT_REGISTER_PAGE = "pages/account/accountRegisterPage";
				String ACCOUNT_PAYMENT_INFO_EDIT_BILLING_ADDRESS_PAGE = "pages/account/accountPaymentInfoEditBillingAddressPage";
			}

			interface Checkout
			{
				String CHECKOUT_REGISTER_PAGE = "pages/checkout/checkoutRegisterPage";
				String CHECKOUT_CONFIRMATION_PAGE = "pages/checkout/checkoutConfirmationPage";
				String CHECKOUT_LOGIN_PAGE = "pages/checkout/checkoutLoginPage";
				String CHECKOUT_CREATE_ACCOUNT_PAGE = "pages/checkout/checkoutCreateAccountPage";
			}

			interface Course
			{
				String COURSE_ORDER_CONFIRMATION_PAGE = "pages/course/courseOrderConfirmationPage";
			}

			interface MultiStepCheckout
			{
				String ADD_EDIT_DELIVERY_ADDRESS_PAGE = "pages/checkout/multi/addEditDeliveryAddressPage";
				String ADD_PAYMENT_METHOD_PAGE = "pages/checkout/multi/addPaymentMethodPage";
				String CHECKOUT_SUMMARY_PAGE = "pages/checkout/multi/checkoutSummaryPage";
				String HOSTED_ORDER_PAGE_ERROR_PAGE = "pages/checkout/multi/hostedOrderPageErrorPage";
				String HOSTED_ORDER_POST_PAGE = "pages/checkout/multi/hostedOrderPostPage";
				String SILENT_ORDER_POST_PAGE = "pages/checkout/multi/silentOrderPostPage";
				String GIFT_WRAP_PAGE = "pages/checkout/multi/giftWrapPage";
				String PAYMENT_OPTIONS_PAGE = "pages/checkout/multi/paymentOptions";
				String CHECKOUT_ORDER_TOTALS_FRAGMENT = "pages/checkout/multi/orderTotalsFragment";
				String CHECKOUT_DELIVERY_CART_FRAGMENT = "pages/checkout/multi/deliveryCartFragment";
				String CHECKOUT_ORDER_SUMMARY_SECTION = "pages/checkout/multi/orderSummaryFragment";
			}

			interface Password
			{
				String PASSWORD_RESET_CHANGE_PAGE = "pages/password/passwordResetChangePage";
				String PASSWORD_RESET_REQUEST = "pages/password/passwordResetRequestPage";
				String PASSWORD_RESET_REQUEST_CONFIRMATION = "pages/password/passwordResetRequestConfirmationPage";
			}

			interface Error
			{
				String ERROR_NOT_FOUND_PAGE = "pages/error/errorNotFoundPage";
			}

			interface Cart
			{
				String CART_PAGE = "pages/cart/cartPage";
			}

			interface StoreFinder
			{
				String STORE_FINDER_SEARCH_PAGE = "pages/storeFinder/storeFinderSearchPage";
				String STORE_FINDER_DETAILS_PAGE = "pages/storeFinder/storeFinderDetailsPage";
				String STORE_FINDER_VIEW_MAP_PAGE = "pages/storeFinder/storeFinderViewMapPage";
			}

			interface Misc
			{
				String MISC_ROBOTS_PAGE = "pages/misc/miscRobotsPage";
				String MISC_SITE_MAP_PAGE = "pages/misc/miscSiteMapPage";
			}

			interface Guest
			{
				String GUEST_ORDER_PAGE = "pages/guest/guestOrderPage";
				String GUEST_ORDER_ERROR_PAGE = "pages/guest/guestOrderErrorPage";
			}

			interface Product
			{
				String WRITE_REVIEW = "pages/product/writeReview";
				String ORDER_FORM = "pages/product/productOrderFormPage";
				String PURCHASE_OPTIONS_PAGE = "pages/product/wileyPlusPurchaseOptionsPage";
				String RELATED_PRODUCTS_CONTENT_RESULTS = "pages/product/relatedProductsContentResult";
			}

			interface PayPal
			{
				String PAY_PAL_LOGIN_PAGE = "pages/payPal/payPalCheckoutLoginPage";
				String PAY_PAL_CREATE_ACCOUNT_PAGE = "pages/payPal/payPalCheckoutCreateAccountLogin";
			}

			interface Search
			{
				String LOAD_MORE_RESULTS = "pages/search/searchAdditionalResults";
				String LOAD_MORE_CONTENT_RESULTS = "pages/search/searchAdditionalContentResult";
				String LOAD_MORE_FACET_VALUES = "pages/search/searchAdditionalFacetValues";
			}
		}

		interface Fragments
		{
			interface Cart
			{
				String ADD_TO_CART_POPUP = "fragments/cart/addToCartPopup";
				String MINI_CART_PANEL = "fragments/cart/miniCartPanel";
				String MINI_CART_ERROR_PANEL = "fragments/cart/miniCartErrorPanel";
				String CART_POPUP = "fragments/cart/cartPopup";
				String EXPAND_GRID_IN_CART = "fragments/cart/expandGridInCart";
			}

			interface Checkout
			{
				String TERMS_AND_CONDITIONS_POPUP = "fragments/checkout/termsAndConditionsPopup";
				String BILLING_ADDRESS_FORM = "fragments/checkout/billingAddressForm";
				String READ_ONLY_EXPANDED_ORDER_FORM = "fragments/checkout/readOnlyExpandedOrderForm";
			}

			interface Password
			{
				String PASSWORD_RESET_REQUEST_POPUP = "fragments/password/passwordResetRequestPopup";
				String PASSWORD_RESET_REQUEST_FORM = "fragments/password/passwordResetRequestForm";
				String FORGOT_PASSWORD_VALIDATION_MESSAGE = "fragments/password/forgotPasswordValidationMessage";
			}

			interface Product
			{
				String FUTURE_STOCK_POPUP = "fragments/product/futureStockPopup";
				String QUICK_VIEW_POPUP = "fragments/product/quickViewPopup";
				String ZOOM_IMAGES_POPUP = "fragments/product/zoomImagesPopup";
				String REVIEWS_TAB = "fragments/product/reviewsTab";
				String STORE_PICKUP_SEARCH_RESULTS = "fragments/product/storePickupSearchResults";
			}
		}
	}
}
