package com.wiley.wileyb2c.storefront.controllers.cms;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.wiley.core.model.components.WileyBootstrapRowComponentModel;
import com.wiley.facades.bootstrapgrid.WileyBootstrapGridFacade;
import com.wiley.wileyb2c.storefront.controllers.ControllerConstants;


@Controller("WileyBootstrapRowComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.WILEY_BOOTSTRAP_ROW_COMPONENT)
public class WileyBootstrapRowComponentController extends
		AbstractCMSComponentController<WileyBootstrapRowComponentModel>
{
	@Resource(name = "wileyBootstrapGridFacade")
	private WileyBootstrapGridFacade bootstrapGridFacade;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final WileyBootstrapRowComponentModel component)
	{
		model.addAttribute("fluidContainer", component.getFluidContainer());

		model.addAttribute("cssClasses", component.getCssClasses().stream().toArray(String[]::new));
		model.addAttribute("cssStyle", component.getCssStyle());
		model.addAttribute("columns", bootstrapGridFacade.getColumnsFromRowComponent(component));
	}
}
