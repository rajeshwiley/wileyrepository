package com.wiley.wileyb2c.storefront.interceptors.beforeview;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.checkout.steps.AbstractCheckoutStepController;
import de.hybris.platform.acceleratorstorefrontcommons.interceptors.BeforeViewHandler;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;

import com.wiley.core.wiley.session.storage.WileyFailedCartModificationsStorageService;


/**
 * @author Dzmitryi_Halahayeu
 */
public final class WileyCheckCartCalculationErrorsDuringCheckoutBeforeViewHandler implements BeforeViewHandler
{
	private static final String CHECKOUT_URL_PATTERN = "checkout";
	static final String REDIRECT_TO_CART = AbstractCheckoutStepController.REDIRECT_PREFIX + "/cart";

	@Resource
	private WileyFailedCartModificationsStorageService wileyFailedCartModificationsStorageService;

	@Override
	public void beforeView(final HttpServletRequest request, final HttpServletResponse response, final ModelAndView modelAndView)
			throws Exception
	{
		if (wileyFailedCartModificationsStorageService.isCartCalculationFailed() && modelAndView.getViewName().contains(
				CHECKOUT_URL_PATTERN))
		{
			modelAndView.setViewName(REDIRECT_TO_CART);
		}

	}
}
