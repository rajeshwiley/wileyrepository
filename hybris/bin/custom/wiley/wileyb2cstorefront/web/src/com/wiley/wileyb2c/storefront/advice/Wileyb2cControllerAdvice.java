package com.wiley.wileyb2c.storefront.advice;

import org.springframework.web.bind.annotation.ControllerAdvice;

import com.wiley.wileycom.storefrontcommons.advice.WileycomControllerAdvice;


@ControllerAdvice
public class Wileyb2cControllerAdvice extends WileycomControllerAdvice
{
	//Inherit functionality from WileycomControllerAdvice
}
