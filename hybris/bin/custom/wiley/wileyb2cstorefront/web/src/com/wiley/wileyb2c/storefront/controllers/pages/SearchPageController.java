/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wileyb2c.storefront.controllers.pages;

import de.hybris.platform.acceleratorcms.model.components.SearchBoxComponentModel;
import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.acceleratorservices.customer.CustomerLocationService;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.SearchBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractSearchPageController;
import de.hybris.platform.acceleratorstorefrontcommons.util.MetaSanitizerUtil;
import de.hybris.platform.acceleratorstorefrontcommons.util.XSSFilterUtil;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.servicelayer.services.CMSComponentService;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.ProductSearchFacade;
import de.hybris.platform.commercefacades.search.data.AutocompleteResultData;
import de.hybris.platform.commercefacades.search.data.SearchQueryData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commercefacades.storesession.StoreSessionFacade;
import de.hybris.platform.commercefacades.storesession.data.CurrencyData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commerceservices.search.facetdata.BreadcrumbData;
import de.hybris.platform.commerceservices.search.facetdata.FacetData;
import de.hybris.platform.commerceservices.search.facetdata.FacetRefinement;
import de.hybris.platform.commerceservices.search.facetdata.FacetValueData;
import de.hybris.platform.commerceservices.search.facetdata.ProductSearchPageData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;

import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sap.security.core.server.csi.XSSEncoder;
import com.wiley.core.search.facetdata.Wileyb2cContentSearchPageData;
import com.wiley.core.wileyb2c.i18n.Wileyb2cI18NService;
import com.wiley.facades.content.ContentData;
import com.wiley.facades.search.solrfacetsearch.WileyProductSearchFacade;
import com.wiley.facades.wileyb2c.search.solrfacetsearch.Wileyb2cContentSearchFacade;
import com.wiley.facades.wileycom.storesession.WileycomStoreSessionFacade;
import com.wiley.wileyb2c.storefront.controllers.ControllerConstants;
import com.wiley.wileyb2c.storefront.util.RequestUrlParamsHandler;
import com.wiley.wileycom.storefrontcommons.util.WileyRedirectUrlResolverUtil;
import com.wiley.wileycom.storefrontcommons.util.WileySearchUrlHelperUtil;

import static com.wiley.wileycom.storefrontcommons.constants.WileycomstorefrontcommonsConstants.CONTENT_SEARCH_BASE_PATH;
import static com.wiley.wileycom.storefrontcommons.constants.WileycomstorefrontcommonsConstants.CURRENT_PAGE_SIZE;
import static com.wiley.wileycom.storefrontcommons.constants.WileycomstorefrontcommonsConstants.ITEMS_PER_PAGE;
import static com.wiley.wileycom.storefrontcommons.constants.WileycomstorefrontcommonsConstants.LOAD_MORE_FACETS_VALUES_PATH;
import static com.wiley.wileycom.storefrontcommons.constants.WileycomstorefrontcommonsConstants.SEARCH_BASE_PATH;


@Controller
@Scope("tenant")
public class SearchPageController extends AbstractSearchPageController
{
	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(SearchPageController.class);

	private static final String COMPONENT_UID_PATH_VARIABLE_PATTERN = "{componentUid:.*}";
	private static final String FACET_SEPARATOR = ":";

	private static final String SEARCH_CMS_PAGE_ID = "productSearchPage";
	private static final String CONTENT_SEARCH_CMS_PAGE_ID = "contentSearchPage";
	private static final String NO_RESULTS_CMS_PAGE_ID = "searchEmpty";
	private static final String DEFAULT_SORT_OPTION = "relevance";
	public static final String ENABLED_TAB_PATH = "enabledTabPath";
	public static final String ACTIVE_TAB = "activeTab";
	public static final String CONTENT_TAB_ID = "CONTENT";
	public static final String PRODUCTS_TAB_ID = "PRODUCTS";
	public static final String TOTAL_NUMBER_OF_PRODUCT_RESULTS = "totalNumberOfProductResults";
	public static final String TOTAL_NUMBER_OF_CONTENT_RESULTS = "totalNumberOfContentResults";
	public static final String HIDE_CONTENT_TAB = "hideContentTab";
	public static final String HIDE_PRODUCTS_TAB = "hideProductsTab";
	public static final String USER_LOCATION = "userLocation";
	public static final String SEARCH_PAGE_DATA = "searchPageData";
	public static final String PAGE_TYPE = "pageType";
	public static final String FACET = "facet";
	public static final String EMPTY_FREE_TEXT_SEARCH = "isEmptyFreeTextSearch";
	private static final String SEARCH_HIGHLIGHT_OPEN = "<span class=\"search-highlight\">";
	private static final String SEARCH_HIGHLIGHT_CLOSE = "</span>";
	private static final String SEARCH_TEXT = "searchText";

	@Resource(name = "productSearchFacade")
	private ProductSearchFacade<ProductData> productSearchFacade;

	@Resource(name = "productNoFacetSearchFacade")
	private ProductSearchFacade<ProductData> productNoFacetSearchFacade;

	@Resource(name = "searchBreadcrumbBuilder")
	private SearchBreadcrumbBuilder searchBreadcrumbBuilder;

	@Resource(name = "customerLocationService")
	private CustomerLocationService customerLocationService;

	@Resource(name = "cmsComponentService")
	private CMSComponentService cmsComponentService;

	@Resource(name = "wileyb2cContentSearchFacade")
	private Wileyb2cContentSearchFacade wileyb2cContentSearchFacade;

	@Resource
	private WileycomStoreSessionFacade wileycomStoreSessionFacade;

	@Resource
	private StoreSessionFacade storeSessionFacade;

	@Resource
	private Wileyb2cI18NService wileyb2cI18NService;

	@Resource
	private WileyProductSearchFacade wileyb2cProductSearchFacade;

	@Resource
	private RequestUrlParamsHandler requestUrlParamsHandler;

	@RequestMapping(value = SEARCH_BASE_PATH, method = RequestMethod.GET, params = "!pq")
	public String textSearch(@RequestParam(value = "text", defaultValue = "") final String searchText,
			final HttpServletRequest request, final Model model) throws CMSItemNotFoundException
	{
		String redirection = WileyRedirectUrlResolverUtil.getLocalizedRedirect(request);
		if (StringUtils.isNotEmpty(redirection))
		{
			return redirection;
		}
		if (StringUtils.isNotBlank(searchText))
		{
			final PageableData pageableData = createPageableData(0, getSearchPageSize(), null, ShowMode.Page);
			final String encodedSearchText = XSSFilterUtil.filter(searchText);

			final SearchStateData searchState = getSearchStateData(encodedSearchText);

			ProductSearchPageData<SearchStateData, ProductData> searchPageData = productSearchFacade.textSearch(searchState,
					pageableData);
			searchPageData = encodeSearchPageData(searchPageData);

			final Wileyb2cContentSearchPageData<SearchStateData, ContentData> contentSearchPageData = performContentSearch(
					encodedSearchText, 0, ShowMode.Page, getSearchPageSize());

			if (searchPageData == null && contentSearchPageData == null)
			{
				storeCmsPageInModel(model, getContentPageForLabelOrId(NO_RESULTS_CMS_PAGE_ID));
			}
			else if (searchPageData != null && searchPageData.getKeywordRedirectUrl() != null)
			{
				// if the search engine returns a redirect, just
				return REDIRECT_PREFIX + searchPageData.getKeywordRedirectUrl();
			}
			else if (contentSearchPageData.getKeywordRedirectUrl() != null)
			{
				// if the search engine returns a redirect, just
				return REDIRECT_PREFIX + contentSearchPageData.getKeywordRedirectUrl();
			}
			else if (searchPageData != null && searchPageData.getPagination().getTotalNumberOfResults() == 0
					&& contentSearchPageData.getPagination().getTotalNumberOfResults() == 0)
			{
				model.addAttribute(SEARCH_PAGE_DATA, searchPageData);
				storeCmsPageInModel(model, getContentPageForLabelOrId(NO_RESULTS_CMS_PAGE_ID));
				updatePageTitle(encodedSearchText, model);
			}
			else
			{
				storeContinueUrl(request);
				populateModel(model, searchPageData, ShowMode.Page);
				storeCmsPageInModel(model, getContentPageForLabelOrId(SEARCH_CMS_PAGE_ID));
				updatePageTitle(encodedSearchText, model);
			}
			model.addAttribute(USER_LOCATION, customerLocationService.getUserLocation());
			getRequestContextData(request).setSearch(searchPageData);
			if (searchPageData != null)
			{
				model.addAttribute(
						WebConstants.BREADCRUMBS_KEY,
						searchBreadcrumbBuilder.getBreadcrumbs(null, encodedSearchText,
								CollectionUtils.isEmpty(searchPageData.getBreadcrumbs())));
			}
		}
		else
		{
			storeCmsPageInModel(model, getContentPageForLabelOrId(NO_RESULTS_CMS_PAGE_ID));
		}
		model.addAttribute(PAGE_TYPE, PageType.PRODUCTSEARCH.name());
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_FOLLOW);

		final String metaDescription = getMetaDescription(searchText);
		final String metaKeywords = MetaSanitizerUtil.sanitizeKeywords(searchText);
		setUpMetaData(model, metaKeywords, metaDescription);

		return getViewForPage(model);
	}

	protected SearchStateData getSearchStateData(final String encodedSearchText)
	{
		return getSearchStateData(encodedSearchText, true);
	}

	protected SearchStateData getSearchStateData(final String encodedSearchText, final boolean useFacetViewMoreLimit)
	{
		final SearchStateData searchState = new SearchStateData();
		final SearchQueryData searchQueryData = new SearchQueryData();
		searchQueryData.setValue(cleanupSolrSearchText(encodedSearchText));
		searchState.setQuery(searchQueryData);
		searchState.setUseFacetViewMoreLimit(useFacetViewMoreLimit);
		return searchState;
	}

	@RequestMapping(value = SEARCH_BASE_PATH, method = RequestMethod.GET, params = "pq")
	public String refineSearch(@RequestParam("pq") final String searchQuery,
			@RequestParam(value = "page", defaultValue = "0") final int page,
			@RequestParam(value = "size", defaultValue = "0") final int size,
			@RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
			@RequestParam(value = "text", required = false) final String searchText, final HttpServletRequest request,
			final Model model) throws CMSItemNotFoundException, UnsupportedEncodingException
	{
		String redirection = WileyRedirectUrlResolverUtil.getLocalizedRedirect(request);

		if (StringUtils.isNotEmpty(redirection))
		{
			return redirection;
		}

		final String searchQueryWithoutPage = WileySearchUrlHelperUtil.getCurrentSearchQueryWithoutPage(request);
		final Optional<Integer> pageSize = requestUrlParamsHandler.getPageSize(request, size);

		if (page < 0)
		{
			if (pageSize.isPresent())
			{
				return WileyRedirectUrlResolverUtil.getRedirectToPage(request, searchQueryWithoutPage);
			}

			return WileyRedirectUrlResolverUtil.getRedirectToPage(request,
					WileySearchUrlHelperUtil.getCurrentSearchQueryWithoutPageAndPageSize(request));
		}

		model.addAttribute(USER_LOCATION, customerLocationService.getUserLocation());
		model.addAttribute(ITEMS_PER_PAGE, requestUrlParamsHandler.getAllowedItemsPerPageSize());
		model.addAttribute(CURRENT_PAGE_SIZE, requestUrlParamsHandler.getCurrentOrDefaultPageSize());

		if (!pageSize.isPresent())
		{
			return WileyRedirectUrlResolverUtil.getRedirectToPage(request,
					WileySearchUrlHelperUtil.getCurrentSearchQueryWithoutPageSize(request));
		}

		final ProductSearchPageData<SearchStateData, ProductData> searchPageData =
				performProductSearch(searchQuery, page, showMode, pageSize.get());

		model.addAttribute("currentSearchQuery", searchQueryWithoutPage);
		model.addAttribute(SEARCH_TEXT, searchPageData.getFreeTextSearch());

		if (searchPageData.getKeywordRedirectUrl() != null)
		{
			return REDIRECT_PREFIX + searchPageData.getKeywordRedirectUrl();
		}

		final Wileyb2cContentSearchPageData<SearchStateData, ContentData> contentSearchPageData = performContentSearch(
				extractQuery(searchQuery), 0, ShowMode.Page, pageSize.get());

		if (contentSearchPageData.getKeywordRedirectUrl() != null)
		{
			return REDIRECT_PREFIX + contentSearchPageData.getKeywordRedirectUrl();
		}

		final int numberOfPages = searchPageData.getPagination().getNumberOfPages();

		if (numberOfPages > 0 && page > numberOfPages - 1)
		{
			return WileyRedirectUrlResolverUtil.getRedirectToPage(request, numberOfPages - 1);
		}

		populateModel(model, searchPageData, showMode);

		final long contentNumberOfResults = contentSearchPageData.getPagination().getTotalNumberOfResults();
		final long productsNumberOfResults = searchPageData.getPagination().getTotalNumberOfResults();

		if (productsNumberOfResults == 0)
		{
			if (contentNumberOfResults == 0 || !WileySearchUrlHelperUtil.isContentTabMayBeShown(request))
			{
				updatePageTitle(searchPageData.getFreeTextSearch(), model);
				storeCmsPageInModel(model, getContentPageForLabelOrId(NO_RESULTS_CMS_PAGE_ID));
			}
			else
			{
				return REDIRECT_PREFIX + WileySearchUrlHelperUtil.getEnabledSearchTabPath(request).substring(
						request.getContextPath().length());
			}
		}
		else
		{
			if (contentNumberOfResults == 0 || !WileySearchUrlHelperUtil.isContentTabMayBeShown(request))
			{
				model.addAttribute(HIDE_CONTENT_TAB, true);
			}
			else
			{
				populateTabsInfo(request, model);
			}
			model.addAttribute(ACTIVE_TAB, PRODUCTS_TAB_ID);
			model.addAttribute(TOTAL_NUMBER_OF_PRODUCT_RESULTS, productsNumberOfResults);
			model.addAttribute(TOTAL_NUMBER_OF_CONTENT_RESULTS, contentNumberOfResults);
			model.addAttribute(EMPTY_FREE_TEXT_SEARCH, isEmptyFreeTextSearch(searchPageData));
			storeContinueUrl(request);
			updatePageTitle(searchPageData.getFreeTextSearch(), model);
			storeCmsPageInModel(model, getContentPageForLabelOrId(SEARCH_CMS_PAGE_ID));
		}

		model.addAttribute(WebConstants.BREADCRUMBS_KEY, searchBreadcrumbBuilder.getBreadcrumbs(null, searchPageData));
		model.addAttribute(PAGE_TYPE, PageType.PRODUCTSEARCH.name());

		final String metaDescription = getMetaDescription(searchText);
		final String metaKeywords = MetaSanitizerUtil.sanitizeKeywords(searchText);
		setUpMetaData(model, metaKeywords, metaDescription);
		return getViewForPage(model);
	}

	@RequestMapping(value = CONTENT_SEARCH_BASE_PATH, method = RequestMethod.GET)
	public String refineContentSearch(@RequestParam(value = "cq", required = false) final String contentSearchQuery,
			@RequestParam("pq") final String productSearchQuery,
			@RequestParam(value = "page", defaultValue = "0") final int page,
			@RequestParam(value = "size", defaultValue = "0") final int size,
			@RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
			@RequestParam(value = "text", required = false) final String searchText, final HttpServletRequest request,
			final Model model) throws CMSItemNotFoundException, UnsupportedEncodingException
	{
		if (StringUtils.isEmpty(contentSearchQuery)
				&& StringUtils.isNotBlank(WileySearchUrlHelperUtil.getSearchText(productSearchQuery)))
		{
			return REDIRECT_PREFIX + WileySearchUrlHelperUtil.getContentSearchServletPath(request);
		}

		final String redirection = WileyRedirectUrlResolverUtil.getLocalizedRedirect(request);

		if (StringUtils.isNotEmpty(redirection))
		{
			return redirection;
		}

		final String searchQueryWithoutPage = WileySearchUrlHelperUtil.getCurrentSearchQueryWithoutPage(request);
		final Optional<Integer> pageSize = requestUrlParamsHandler.getPageSize(request, size);

		if (page < 0)
		{
			if (pageSize.isPresent())
			{
				return WileyRedirectUrlResolverUtil.getRedirectToPage(request, searchQueryWithoutPage);
			}

			return WileyRedirectUrlResolverUtil.getRedirectToPage(request,
					WileySearchUrlHelperUtil.getCurrentSearchQueryWithoutPageAndPageSize(request));
		}

		model.addAttribute(USER_LOCATION, customerLocationService.getUserLocation());
		model.addAttribute(ITEMS_PER_PAGE, requestUrlParamsHandler.getAllowedItemsPerPageSize());
		model.addAttribute(CURRENT_PAGE_SIZE, requestUrlParamsHandler.getCurrentOrDefaultPageSize());

		if (!pageSize.isPresent())
		{
			return WileyRedirectUrlResolverUtil.getRedirectToPage(request,
					WileySearchUrlHelperUtil.getCurrentSearchQueryWithoutPageSize(request));
		}

		final Wileyb2cContentSearchPageData<SearchStateData, ContentData> contentSearchPageData = performContentSearch(
				contentSearchQuery, page, ShowMode.Page, pageSize.get());
		final ProductSearchPageData<SearchStateData, ProductData> searchPageData = performProductSearch(productSearchQuery,
				page, showMode, pageSize.get());

		final int numberOfPages = contentSearchPageData.getPagination().getNumberOfPages();

		if (numberOfPages > 0 && page > numberOfPages - 1)
		{
			return WileyRedirectUrlResolverUtil.getRedirectToPage(request, numberOfPages - 1);
		}

		populateModel(model, contentSearchPageData, showMode);

		model.addAttribute("currentSearchQuery", searchQueryWithoutPage);
		model.addAttribute(SEARCH_PAGE_DATA, contentSearchPageData);
		model.addAttribute(SEARCH_TEXT, contentSearchPageData.getFreeTextSearch());

		// ECSC-25540 "did you mean" for products will be also shown on Content tab
		replaceContentSuggestionWithProductSuggestion(contentSearchPageData, searchPageData);

		final long contentNumberOfResults = contentSearchPageData.getPagination().getTotalNumberOfResults();
		final long productsNumberOfResults = searchPageData.getPagination().getTotalNumberOfResults();

		if (contentNumberOfResults == 0)
		{
			updatePageTitle(contentSearchPageData.getFreeTextSearch(), model);
			storeCmsPageInModel(model, getContentPageForLabelOrId(NO_RESULTS_CMS_PAGE_ID));
		}
		else
		{
			if (productsNumberOfResults == 0)
			{
				model.addAttribute(HIDE_PRODUCTS_TAB, true);
			}
			else
			{
				populateTabsInfo(request, model);
			}
			model.addAttribute(ACTIVE_TAB, CONTENT_TAB_ID);
			model.addAttribute(TOTAL_NUMBER_OF_PRODUCT_RESULTS, productsNumberOfResults);
			model.addAttribute(TOTAL_NUMBER_OF_CONTENT_RESULTS, contentNumberOfResults);
			model.addAttribute(EMPTY_FREE_TEXT_SEARCH, isEmptyFreeTextSearch(searchPageData));
			storeContinueUrl(request);

			updatePageTitle(contentSearchPageData.getFreeTextSearch(), model);
			storeCmsPageInModel(model, getContentPageForLabelOrId(CONTENT_SEARCH_CMS_PAGE_ID));
		}

		final String metaDescription = getMetaDescription(searchText);
		final String metaKeywords = MetaSanitizerUtil.sanitizeKeywords(searchText);
		setUpMetaData(model, metaKeywords, metaDescription);

		return getViewForPage(model);
	}

	private void replaceContentSuggestionWithProductSuggestion(
			final Wileyb2cContentSearchPageData<SearchStateData, ContentData> contentSearchPageData,
			final ProductSearchPageData<SearchStateData, ProductData> searchPageData)
	{
		contentSearchPageData.setSpellingSuggestion(searchPageData.getSpellingSuggestion());
	}

	private String getMetaDescription(@RequestParam(value = "text", required = false) final String searchText)
	{
		return MetaSanitizerUtil.sanitizeDescription(getMessageSource().getMessage(
				"search.meta.description.results", null, "search.meta.description.results", getI18nService().getCurrentLocale())
				+ " "
				+ searchText
				+ " "
				+ getMessageSource().getMessage("search.meta.description.on", null, "search.meta.description.on",
				getI18nService().getCurrentLocale()) + " " + getSiteName());
	}

	private void populateTabsInfo(final HttpServletRequest request, final Model model)
			throws UnsupportedEncodingException
	{
		final String enabledTabPath = WileySearchUrlHelperUtil.getEnabledSearchTabPath(request);
		model.addAttribute(ENABLED_TAB_PATH, enabledTabPath);
	}

	protected ProductSearchPageData<SearchStateData, ProductData> performProductSearch(final String searchQuery, final int page,
			final ShowMode showMode, final int pageSize)
	{
		final String sortCode = parseSortCode(searchQuery);
		final PageableData pageableData = createPageableData(page, pageSize, sortCode, showMode);
		final SearchStateData searchState = getSearchStateData(searchQuery);
		return encodeSearchPageData(productSearchFacade.textSearch(searchState, pageableData));
	}

	protected Wileyb2cContentSearchPageData<SearchStateData, ContentData> performContentSearch(final String searchQuery,
			final int page, final ShowMode showMode, final int pageSize)
	{
		final PageableData pageableData = createPageableData(page, pageSize, null, showMode);
		final SearchStateData searchState = getSearchStateData(searchQuery);
		return encodeWileySearchPageData(wileyb2cContentSearchFacade.textSearch(searchState, pageableData));
	}

	@RequestMapping(value = SEARCH_BASE_PATH + LOAD_MORE_FACETS_VALUES_PATH, method = RequestMethod.GET)
	public String getAllFacetValues(@RequestParam("pq") final String searchQuery,
			@RequestParam(value = "page", defaultValue = "0") final int page,
			@RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
			@RequestParam(value = "facetCode") final String facetCode,
			@RequestParam(value = "sort", required = false) final String sortCode,
			final Model model)
	{
		final SearchStateData searchState = getSearchStateData(searchQuery, false);
		searchState.setFacetCode(facetCode);

		final ProductSearchPageData<SearchStateData, ProductData> searchPageData = productSearchFacade.textSearch(searchState,
				createPageableData(page, getSearchPageSize(), sortCode, showMode));
		final List<FacetData<SearchStateData>> facets = refineFacets(searchPageData.getFacets(),
				convertBreadcrumbsToFacets(searchPageData.getBreadcrumbs()));
		model.addAttribute(FACET, facets.get(0));
		return ControllerConstants.Views.Pages.Search.LOAD_MORE_FACET_VALUES;
	}

	@ResponseBody
	@RequestMapping(value = SEARCH_BASE_PATH + "/facets", method = RequestMethod.GET)
	public FacetRefinement<SearchStateData> getFacets(@RequestParam("q") final String searchQuery,
			@RequestParam(value = "page", defaultValue = "0") final int page,
			@RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
			@RequestParam(value = "sort", required = false) final String sortCode)
	{
		final SearchStateData searchState = getSearchStateData(searchQuery);

		final ProductSearchPageData<SearchStateData, ProductData> searchPageData = productSearchFacade.textSearch(searchState,
				createPageableData(page, getSearchPageSize(), sortCode, showMode));
		final List<FacetData<SearchStateData>> facets = refineFacets(searchPageData.getFacets(),
				convertBreadcrumbsToFacets(searchPageData.getBreadcrumbs()));
		final FacetRefinement<SearchStateData> refinement = new FacetRefinement<>();
		refinement.setFacets(facets);
		refinement.setCount(searchPageData.getPagination().getTotalNumberOfResults());
		refinement.setBreadcrumbs(searchPageData.getBreadcrumbs());
		return refinement;
	}

	@ResponseBody
	@RequestMapping(value = SEARCH_BASE_PATH + "/autocomplete/"
			+ COMPONENT_UID_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	public AutocompleteResultData getAutocompleteSuggestions(@PathVariable final String componentUid,
			@RequestParam("term") final String term) throws CMSItemNotFoundException
	{
		final AutocompleteResultData resultData = new AutocompleteResultData();

		final SearchBoxComponentModel component = cmsComponentService.getSimpleCMSComponent(componentUid);
		final String cleanedTerm = cleanupSolrSearchText(term);
		if (component.isDisplaySuggestions())
		{
			resultData.setSuggestions(
					subList(productSearchFacade.getAutocompleteSuggestions(cleanedTerm), component.getMaxSuggestions()));
			resultData.getSuggestions().forEach(r -> r.setTerm(highlightSuggestion(r.getTerm(), cleanedTerm)));

		}

		if (component.isDisplayProducts())
		{
			final List<ProductData> results = productNoFacetSearchFacade.textSearch(cleanedTerm).getResults();
			resultData.setProducts(
					subList(results, component.getMaxProducts()));
			resultData.setShowSeeAllProducts(results.size() > component.getMaxProducts());
		}
		if (component.getDisplayPages())
		{
			final Wileyb2cContentSearchPageData<SearchStateData, ContentData> results = performContentSearch(cleanedTerm, 0,
					ShowMode.Page,
					component.getMaxPages());
			resultData.setPages(results.getResults());
			resultData.setShowSeeAllPages(results.getPagination().getTotalNumberOfResults() > component.getMaxPages());
		}

		return resultData;
	}

	private String highlightSuggestion(final String text, final String term)
	{
		final String lowerText = text.toLowerCase();
		final String lowerTerm = term.toLowerCase();
		if (lowerText.contains(lowerTerm))
		{
			final int i = lowerText.indexOf(lowerTerm);
			return text.substring(0, i) + SEARCH_HIGHLIGHT_OPEN + text.substring(i, i + term.length())
					+ SEARCH_HIGHLIGHT_CLOSE + text.substring(i + term.length());
		}
		return text;
	}

	protected String parseSortCode(final String searchQuery)
	{
		String[] qParams = searchQuery.split("\\|");
		return qParams.length > 1 ? qParams[1] : DEFAULT_SORT_OPTION;
	}

	private String extractQuery(final String searchQuery)
	{
		final int sortSeparatorIdx = searchQuery.indexOf("|");
		return searchQuery.substring(0,
				sortSeparatorIdx > 0 ? sortSeparatorIdx : searchQuery.length());
	}

	protected <E> List<E> subList(final List<E> list, final int maxElements)
	{
		if (CollectionUtils.isEmpty(list))
		{
			return Collections.emptyList();
		}

		if (list.size() > maxElements)
		{
			return list.subList(0, maxElements);
		}

		return list;
	}

	protected void updatePageTitle(final String searchText, final Model model)
	{
		storeContentPageTitleInModel(
				model,
				getPageTitleResolver().resolveContentPageTitle(
						getMessageSource().getMessage("search.meta.title", null, "search.meta.title",
								getI18nService().getCurrentLocale())
								+ " " + searchText));
	}

	@Override
	protected ProductSearchPageData<SearchStateData, ProductData> encodeSearchPageData(
			final ProductSearchPageData<SearchStateData, ProductData> searchPageData)
	{
		return encodeWileySearchPageData(searchPageData);
	}

	protected <T, M extends ProductSearchPageData<SearchStateData, T>> M encodeWileySearchPageData(
			final M searchPageData)
	{
		final SearchStateData currentQuery = searchPageData.getCurrentQuery();

		if (currentQuery != null)
		{
			try
			{
				final SearchQueryData query = currentQuery.getQuery();
				final String encodedQueryValue = XSSEncoder.encodeHTML(query.getValue());
				query.setValue(encodedQueryValue);
				currentQuery.setQuery(query);
				searchPageData.setCurrentQuery(currentQuery);
				searchPageData.setFreeTextSearch(XSSEncoder.encodeHTML(searchPageData.getFreeTextSearch()));

				final List<FacetData<SearchStateData>> facets = searchPageData.getFacets();
				if (CollectionUtils.isNotEmpty(facets))
				{
					for (final FacetData<SearchStateData> facetData : facets)
					{
						final List<FacetValueData<SearchStateData>> facetValueDatas = facetData.getValues();
						if (CollectionUtils.isNotEmpty(facetValueDatas))
						{
							for (final FacetValueData<SearchStateData> facetValueData : facetValueDatas)
							{
								final SearchStateData facetQuery = facetValueData.getQuery();
								final SearchQueryData queryData = facetQuery.getQuery();
								final String queryValue = queryData.getValue();
								if (StringUtils.isNotBlank(queryValue))
								{
									final String[] queryValues = queryValue.split(FACET_SEPARATOR);
									final StringBuilder queryValueBuilder = new StringBuilder();
									queryValueBuilder.append(XSSEncoder.encodeHTML(queryValues[0]));
									for (int i = 1; i < queryValues.length; i++)
									{
										queryValueBuilder.append(FACET_SEPARATOR).append(queryValues[i]);
									}
									queryData.setValue(queryValueBuilder.toString());
								}
							}
						}

						final List<FacetValueData<SearchStateData>> topFacetValueDatas = facetData.getTopValues();
						if (CollectionUtils.isNotEmpty(topFacetValueDatas))
						{
							for (final FacetValueData<SearchStateData> topFacetValueData : topFacetValueDatas)
							{
								final SearchStateData facetQuery = topFacetValueData.getQuery();
								final SearchQueryData queryData = facetQuery.getQuery();
								final String queryValue = queryData.getValue();
								if (StringUtils.isNotBlank(queryValue))
								{
									final String[] queryValues = queryValue.split(FACET_SEPARATOR);
									final StringBuilder queryValueBuilder = new StringBuilder();
									queryValueBuilder.append(XSSEncoder.encodeHTML(queryValues[0]));
									for (int i = 1; i < queryValues.length; i++)
									{
										queryValueBuilder.append(FACET_SEPARATOR).append(queryValues[i]);
									}
									queryData.setValue(queryValueBuilder.toString());
								}
							}
						}
					}
				}

			}
			catch (final UnsupportedEncodingException e)
			{
				if (LOG.isDebugEnabled())
				{
					LOG.debug("Error occured during Encoding the Search Page data values", e);
				}
			}
		}
		return searchPageData;
	}

	protected String cleanupSolrSearchText(final String text)
	{
		String cleanText = text.replaceAll("AND", "and").replaceAll("OR", "or");
		return wileyb2cProductSearchFacade.cleanupDashesFromSearchIsbn(cleanText);
	}

	private boolean isEmptyFreeTextSearch(final ProductSearchPageData searchPageData)
	{
		return searchPageData.getFreeTextSearch().isEmpty();
	}

	@Override
	protected int getSearchPageSize()
	{
		return super.getSearchPageSize();
	}

	@Override
	protected List<FacetData<SearchStateData>> refineFacets(final List<FacetData<SearchStateData>> facets,
			final Map<String, FacetData<SearchStateData>> selectedFacets)
	{
		return super.refineFacets(facets, selectedFacets);
	}

	@Override
	protected Map<String, FacetData<SearchStateData>> convertBreadcrumbsToFacets(
			final List<BreadcrumbData<SearchStateData>> breadcrumbs)
	{
		return super.convertBreadcrumbsToFacets(breadcrumbs);
	}

	@ModelAttribute("sessionCountry")
	public CountryData getSessionCountry()
	{
		return wileycomStoreSessionFacade.getSessionCountry();
	}

	@ModelAttribute("sessionCurrency")
	public CurrencyData getSessionCurrency()
	{
		return storeSessionFacade.getCurrentCurrency();
	}

	@ModelAttribute("optionalTaxShortMessage")
	public String getOptionalTaxShortMessage()
	{
		return wileyb2cI18NService.getCurrentCountryTaxShortMsg().orElse("");
	}

	@ModelAttribute("publicationDatePattern")
	public String getSessionPublicationDateFormat()
	{
		return wileyb2cI18NService.getCurrentDateFormat();
	}

	@ModelAttribute("optionalTaxTooltip")
	public String getOptionalTaxTooltip()
	{
		return wileyb2cI18NService.getCurrentCountryTaxTooltip().orElse("");
	}

	public ProductSearchFacade<ProductData> getProductSearchFacade()
	{
		return productSearchFacade;
	}

	public void setProductSearchFacade(
			final ProductSearchFacade<ProductData> productSearchFacade)
	{
		this.productSearchFacade = productSearchFacade;
	}
}
