package com.wiley.wileyb2c.storefront.controllers.cms;

import de.hybris.platform.acceleratorfacades.device.ResponsiveMediaFacade;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.wiley.core.model.components.WileyHeroBannerComponentModel;
import com.wiley.wileyb2c.storefront.controllers.ControllerConstants;


@Controller("WileyHeroBannerComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.WILEY_HERO_BANNER_COMPONENT)
public class WileyHeroBannerComponentController extends
		AbstractCMSComponentController<WileyHeroBannerComponentModel>
{
	@Resource(name = "responsiveMediaFacade")
	private ResponsiveMediaFacade responsiveMediaFacade;

	@Resource(name = "commerceCommonI18NService")
	private CommerceCommonI18NService commerceCommonI18NService;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model,
			final WileyHeroBannerComponentModel component)
	{
		final List<ImageData> mediaDataList = responsiveMediaFacade.getImagesFromMediaContainer(component
				.getMedia(commerceCommonI18NService.getCurrentLocale()));

		model.addAttribute("medias", mediaDataList);
		model.addAttribute("urlLink", component.getUrlLink());
		model.addAttribute("enhancedLink", component.getEnhancedLink());
		model.addAttribute("content", component.getContent());
		model.addAttribute("cssClasses", component.getCssClasses().toArray(new String[0]));
		model.addAttribute("cssStyle", component.getCssStyle());
	}
}
