package com.wiley.wileyb2c.storefront.controllers.pages;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.exceptions.ExternalSystemException;
import com.wiley.facades.wileyb2c.order.Wileyb2cGracePeriodCheckoutFacade;
import com.wiley.facades.wileyb2c.wileyplus.url.strategy.WileyplusPurchaseOptionsPageParseURLStrategy;
import com.wiley.facades.wileyb2c.wileyplus.url.strategy.exception.WileyplusChecksumMismatchException;
import com.wiley.facades.wileyplus.cart.activation.dto.CartActivationRequestDto;
import com.wiley.facades.wileyplus.cart.activation.dto.WileyPlusCourseDto;
import com.wiley.wileyb2c.storefront.forms.Wileyb2cUpdateProfileForm;
import com.wiley.wileyb2c.storefront.forms.validation.Wileyb2cUpdateProfileFormValidator;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractCheckoutController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.order.exceptions.CalculationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.net.URISyntaxException;
import java.util.List;


@Controller
@Profile(WileyCoreConstants.WILEY_PHASE2A_PROFILE_NAME)
@RequestMapping("/checkout")
public class Wileyb2cGracePeriodPageController extends AbstractPageController
{
	private static final Logger LOG = LoggerFactory.getLogger(Wileyb2cGracePeriodPageController.class);
	private static final String START_FREE_TRIAL_CMS_PAGE = "start-grace-period";
	private static final String ATTR_UPDATE_PROFILE_FORM = "updateProfileForm";
	private static final String ATTR_CART_ACTIVATION_INFO = "cartActivationInfo";
	private static final String ATTR_CHECKSUM = "checksum";
	private static final String ATTR_COURSE = "course";
	private static final String ORDER_CONFIRM_PAGE = REDIRECT_PREFIX + "/checkout/orderConfirmation/";

	@Resource
	private CustomerFacade wileyb2cCustomerFacade;

	@Resource
	private Wileyb2cGracePeriodCheckoutFacade wileyPlusCheckoutFacade;
	@Resource
	private Wileyb2cAccountUpdateControllerHelper wileyb2cAccountUpdateControllerHelper;

	@Resource
	private Wileyb2cUpdateProfileFormValidator wileyb2cUpdateProfileFormValidator;
	@Resource
	private WileyplusPurchaseOptionsPageParseURLStrategy wileyplusPurchaseOptionsPageURLParser;

	@RequestMapping(value = "/grace-period", method = RequestMethod.GET)
	@RequireHardLogIn
	public String showGracePeriodForm(@RequestParam final String cartActivationInfo,
			@RequestParam final String checksum, final Model model)
			throws CMSItemNotFoundException, URISyntaxException, WileyplusChecksumMismatchException
	{
		final CartActivationRequestDto cartActivationRequestDto =
				wileyplusPurchaseOptionsPageURLParser.parseCartActivationParams(cartActivationInfo, checksum);

		populatePageData(cartActivationRequestDto, model);

		model.addAttribute(ATTR_CART_ACTIVATION_INFO, cartActivationInfo);
		model.addAttribute(ATTR_CHECKSUM, checksum);

		return getViewForPage(model);
	}

	private void populatePageData(final CartActivationRequestDto cartActivationRequestDto,
			final Model model) throws CMSItemNotFoundException
	{

		final Wileyb2cUpdateProfileForm form = populateProfileForm();

		model.addAttribute(ATTR_UPDATE_PROFILE_FORM, form);
		model.addAttribute(ATTR_COURSE, cartActivationRequestDto.getCourse());
		storeCmsPageInModel(model, getContentPageForLabelOrId(START_FREE_TRIAL_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(START_FREE_TRIAL_CMS_PAGE));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
	}

	private Wileyb2cUpdateProfileForm populateProfileForm()
	{
		final Wileyb2cUpdateProfileForm form = new Wileyb2cUpdateProfileForm();
		final CustomerData currentCustomer = wileyb2cCustomerFacade.getCurrentCustomer();
		form.setEmail(currentCustomer.getDisplayUid());
		form.setFirstName(currentCustomer.getFirstName());
		form.setLastName(currentCustomer.getLastName());
		form.setGraduationMonth(currentCustomer.getGraduationMonth());
		form.setGraduationYear(currentCustomer.getGraduationYear());
		form.setStudentId(currentCustomer.getStudentId());
		form.setMajor(currentCustomer.getMajor());
		return form;
	}

	@RequestMapping(value = "/grace-period", method = RequestMethod.POST)
	@RequireHardLogIn
	public String submitGracePeriodForm(
			@ModelAttribute(ATTR_UPDATE_PROFILE_FORM) final Wileyb2cUpdateProfileForm updateProfileForm,
			@RequestParam final String cartActivationInfo,
			@RequestParam final String checksum,
			final BindingResult bindingResult, final Model model)
			throws CMSItemNotFoundException, URISyntaxException, WileyplusChecksumMismatchException
	{
		wileyb2cUpdateProfileFormValidator.validate(updateProfileForm, bindingResult);

		final CartActivationRequestDto cartActivationRequestDto =
				wileyplusPurchaseOptionsPageURLParser.parseCartActivationParams(cartActivationInfo, checksum);

		String returnAction = null;
		if (bindingResult.hasErrors())
		{
			returnAction = setErrorMessagesAndCMSPage(model, START_FREE_TRIAL_CMS_PAGE, "form.global.error",
					cartActivationInfo, cartActivationRequestDto.getCourse(), checksum);
		}
		else
		{
			String orderCode = null;
			if (updateCustomerProfile(updateProfileForm))
			{

				try
				{
					orderCode = wileyPlusCheckoutFacade.placeGracePeriodOrder(cartActivationRequestDto);
				}
				catch (CommerceCartModificationException | InvalidCartException | CalculationException e)
				{
					LOG.error("Unable to place Grace Period order", e);
				}
			}
			else
			{
				LOG.error("Unable to update customer profile before placing Grace Period order. ");
			}

			if (orderCode != null)
			{
				returnAction = ORDER_CONFIRM_PAGE + orderCode;
			}
			else
			{
				returnAction = setErrorMessagesAndCMSPage(model, START_FREE_TRIAL_CMS_PAGE,
						"text.startFreeTrial.error.place", cartActivationInfo, cartActivationRequestDto.getCourse(), checksum);
			}
		}

		return returnAction;
	}

	private boolean updateCustomerProfile(final Wileyb2cUpdateProfileForm updateProfileForm)
	{
		boolean result = true;
		final CustomerData currentCustomerData = getCustomerFacade().getCurrentCustomer();

		currentCustomerData.setFirstName(updateProfileForm.getFirstName());
		currentCustomerData.setLastName(updateProfileForm.getLastName());
		currentCustomerData.setStudentId(updateProfileForm.getStudentId());
		currentCustomerData.setMajor(updateProfileForm.getMajor());
		currentCustomerData.setGraduationMonth(updateProfileForm.getGraduationMonth());
		currentCustomerData.setGraduationYear(updateProfileForm.getGraduationYear());

		try
		{
			getCustomerFacade().updateProfile(currentCustomerData);
		}
		catch (final ExternalSystemException e)
		{
			LOG.error("Unable to update profile in external system", e);
		}
		catch (final DuplicateUidException e)
		{
			LOG.error("Unable to update profile", e);
			result = false;
		}
		return result;
	}

	private String setErrorMessagesAndCMSPage(final Model model, final String cmsPageLabelOrId,
			final String errorKey,
			final String cartActivationInfo,
			final WileyPlusCourseDto wileyPlusCourseDto,
			final String checksum) throws CMSItemNotFoundException
	{
		GlobalMessages.addErrorMessage(model, errorKey);
		storeCmsPageInModel(model, getContentPageForLabelOrId(cmsPageLabelOrId));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(cmsPageLabelOrId));
		model.addAttribute(ATTR_CART_ACTIVATION_INFO, cartActivationInfo);
		model.addAttribute(ATTR_CHECKSUM, checksum);
		model.addAttribute(ATTR_COURSE, wileyPlusCourseDto);
		return getViewForPage(model);
	}

	@ModelAttribute("months")
	public List<AbstractCheckoutController.SelectOption> getMonths()
	{
		return wileyb2cAccountUpdateControllerHelper.getMonths();
	}

	@ModelAttribute("years")
	public List<AbstractCheckoutController.SelectOption> getYears()
	{
		return wileyb2cAccountUpdateControllerHelper.getYears();
	}

}
