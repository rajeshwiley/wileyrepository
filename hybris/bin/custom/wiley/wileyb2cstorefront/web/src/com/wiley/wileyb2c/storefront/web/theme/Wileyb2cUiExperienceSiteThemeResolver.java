/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wileyb2c.storefront.web.theme;

import de.hybris.platform.cms2.model.site.CMSSiteModel;

import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.wileyb2c.basesite.theme.Wileyb2cThemeNameResolutionStrategy;
import com.wiley.storefrontcommons.web.theme.UiExperienceSiteThemeResolver;


/**
 * Resolve the spring theme name from the CMSSite.
 * The spring theme name is built from the CMSSite UID and the CMSSite Theme.
 */
public class Wileyb2cUiExperienceSiteThemeResolver extends UiExperienceSiteThemeResolver
{
	private Wileyb2cThemeNameResolutionStrategy wileyb2cThemeNameResolutionStrategy;

	protected String getThemeNameForSite(final CMSSiteModel site)
	{
		return wileyb2cThemeNameResolutionStrategy.resolveThemeName(site);
	}

	@Override
	public String getDefaultThemeName()
	{
		return wileyb2cThemeNameResolutionStrategy.getDefaultThemeName();
	}

	@Required
	public void setWileyb2cThemeNameResolutionStrategy(
			final Wileyb2cThemeNameResolutionStrategy wileyb2cThemeNameResolutionStrategy)
	{
		this.wileyb2cThemeNameResolutionStrategy = wileyb2cThemeNameResolutionStrategy;
	}
}
