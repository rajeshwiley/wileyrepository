package com.wiley.wileyb2c.storefront.interceptors.beforeview;

import de.hybris.platform.acceleratorstorefrontcommons.interceptors.BeforeViewHandler;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import com.wiley.facades.urlresolver.WileyUrlResolutionFacade;


/**
 * Created by Uladzimir_Barouski on 6/14/2017.
 */
public class WileyLocalizedUrlsBeforeViewHandler implements BeforeViewHandler
{
	public static final String EMPTY_STRING = "";
	@Resource
	private WileyUrlResolutionFacade wileyUrlResolutionFacade;

	private static final String REGIONAL_URLS = "regionalUrls";

	@Override
	public void beforeView(final HttpServletRequest request, final HttpServletResponse response, final ModelAndView modelAndView)
			throws Exception
	{
		if (modelAndView != null)
		{
			// according to HttpServletRequest specification getServletPath returns decoded string,
			// while getRequestURI and getContextPath - not decoded
			String notDecodedServletPath = request.getRequestURI().replaceFirst(request.getContextPath(), EMPTY_STRING);
			StringBuilder servletPath = new StringBuilder(notDecodedServletPath);
			final String queryString = request.getQueryString();
			if (StringUtils.isNotEmpty(queryString))
			{
				servletPath.append("?" + queryString);
			}
			modelAndView.addObject(REGIONAL_URLS,
					wileyUrlResolutionFacade.getAllLocalizedUrls(servletPath.toString(), request.isSecure()));
		}
	}
}
