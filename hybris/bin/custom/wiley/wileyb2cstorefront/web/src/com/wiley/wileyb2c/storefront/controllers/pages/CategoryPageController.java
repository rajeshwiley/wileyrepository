/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wileyb2c.storefront.controllers.pages;


import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commercefacades.storesession.StoreSessionFacade;
import de.hybris.platform.commercefacades.storesession.data.CurrencyData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commerceservices.search.facetdata.FacetRefinement;
import de.hybris.platform.commerceservices.search.facetdata.ProductCategorySearchPageData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.wiley.core.wileyb2c.i18n.Wileyb2cI18NService;
import com.wiley.facades.wileycom.storesession.WileycomStoreSessionFacade;
import com.wiley.wileyb2c.storefront.controllers.ControllerConstants;
import com.wiley.wileyb2c.storefront.util.RequestUrlParamsHandler;
import com.wiley.wileycom.storefrontcommons.controllers.pages.AbstractWileycomCategoryPageController;
import com.wiley.wileycom.storefrontcommons.util.WileyRedirectUrlResolverUtil;
import com.wiley.wileycom.storefrontcommons.util.WileySearchUrlHelperUtil;

import static com.wiley.wileycom.storefrontcommons.constants.WileycomstorefrontcommonsConstants.CURRENT_PAGE_SIZE;
import static com.wiley.wileycom.storefrontcommons.constants.WileycomstorefrontcommonsConstants.ITEMS_PER_PAGE;
import static com.wiley.wileycom.storefrontcommons.constants.WileycomstorefrontcommonsConstants.LOAD_MORE_FACETS_VALUES_PATH;


/**
 * Controller for a category page
 */
@Controller
@Scope("tenant")
@RequestMapping
public class CategoryPageController extends AbstractWileycomCategoryPageController
{
	protected static final Logger LOG = Logger.getLogger(CategoryPageController.class);
	private static final String CATEGORY_CODE_PATH_VARIABLE_PATTERN = "/**/*-c-{categoryCode:.*}";
	private static final String CATEGORY_PAGE = "CategoryPage";

	@Resource(name = "wileyb2cCategorySubcategoriesWithDescendantProductsConverter")
	private Converter<CategoryModel, CategoryData> categoryConverter;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Resource
	private WileycomStoreSessionFacade wileycomStoreSessionFacade;

	@Resource
	private StoreSessionFacade storeSessionFacade;

	@Resource
	private Wileyb2cI18NService wileyb2cI18NService;

	@Resource(name = "wileyb2cCustomPlpLinkUrlResolver")
	private UrlResolver<CategoryModel> wileyb2cCustomPlpLinkUrlResolver;

	@Resource
	private RequestUrlParamsHandler requestUrlParamsHandler;

	@RequestMapping(value = CATEGORY_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	public String category(@PathVariable("categoryCode") final String categoryCode,
			@RequestParam(value = "pq", required = false) final String searchQuery,
			@RequestParam(value = "page", defaultValue = "0") final int page,
			@RequestParam(value = "size", defaultValue = "0") final int size,
			@RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode, final Model model,
			final HttpServletRequest request, final HttpServletResponse response) throws UnsupportedEncodingException
	{
		final String searchQueryWithoutPage = WileySearchUrlHelperUtil.getCurrentSearchQueryWithoutPage(request);
		final Optional<Integer> pageSize = requestUrlParamsHandler.getPageSize(request, size);

		if (page < 0)
		{
			if (pageSize.isPresent())
			{
				return WileyRedirectUrlResolverUtil.getRedirectToPage(request, searchQueryWithoutPage);
			}

			return WileyRedirectUrlResolverUtil.getRedirectToPage(request,
					WileySearchUrlHelperUtil.getCurrentSearchQueryWithoutPageAndPageSize(request));
		}

		if (!pageSize.isPresent())
		{
			return WileyRedirectUrlResolverUtil.getRedirectToPage(request,
					WileySearchUrlHelperUtil.getCurrentSearchQueryWithoutPageSize(request));
		}

		final String sortCode = parseSortCode(searchQuery);
		final String resolvedUrl = getCustomLinkFromCategoryCode(categoryCode, response, request);

		model.addAttribute(ControllerConstants.Model.Attributes.CUSTOM_LINK, resolvedUrl);
		model.addAttribute("currentSearchQuery", searchQueryWithoutPage);
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.INDEX_NOFOLLOW);
		model.addAttribute(ITEMS_PER_PAGE, requestUrlParamsHandler.getAllowedItemsPerPageSize());
		model.addAttribute(CURRENT_PAGE_SIZE, requestUrlParamsHandler.getCurrentOrDefaultPageSize());

		final String redirection = performSearchAndGetResultsPage(categoryCode, searchQuery, page, pageSize.get(), showMode,
				sortCode, model, request, response, CATEGORY_PAGE);

		setSubCategoriesInModel(request, model);

		return redirection;
	}

	protected String getCustomLinkFromCategoryCode(final String categoryCode, final HttpServletResponse response,
			final HttpServletRequest request)
	{
		final CategoryModel category = getCommerceCategoryService().getCategoryForCode(categoryCode);
		final String resolvedUrlPath = wileyb2cCustomPlpLinkUrlResolver.resolve(category);
		return response.encodeURL(request.getContextPath() + resolvedUrlPath);
	}

	protected void setSubCategoriesInModel(final HttpServletRequest request, final Model model)
	{
		final SearchPageData searchPageData = getRequestContextData(request).getSearch();
		if (searchPageData instanceof ProductCategorySearchPageData)
		{
			final ProductCategorySearchPageData<?, ?, CategoryData> productCategorySearchPageData =
					(ProductCategorySearchPageData<?, ?, CategoryData>) searchPageData;
			List<CategoryData> subCategories = productCategorySearchPageData.getSubCategories();
			if (subCategories == null)
			{
				// Empty search result. Search has not been performed.
				final CategoryModel categoryModel = getRequestContextData(request).getCategory();
				if (categoryModel != null)
				{
					subCategories = categoryConverter.convert(categoryModel).getSubCategories();
				}
			}
			model.addAttribute("subCategories", subCategories);
		}
		else
		{
			final CategoryModel categoryModel = getRequestContextData(request).getCategory();
			if (categoryModel != null)
			{
				model.addAttribute("subCategories", categoryConverter.convert(categoryModel).getSubCategories());
			}
		}
	}

	@ResponseBody
	@RequestMapping(value = CATEGORY_CODE_PATH_VARIABLE_PATTERN + "/facets", method = RequestMethod.GET)
	public FacetRefinement<SearchStateData> getFacets(@PathVariable("categoryCode") final String categoryCode,
			@RequestParam(value = "q", required = false) final String searchQuery,
			@RequestParam(value = "page", defaultValue = "0") final int page,
			@RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
			@RequestParam(value = "sort", required = false) final String sortCode) throws UnsupportedEncodingException
	{
		return performSearchAndGetFacets(categoryCode, searchQuery, page, showMode, sortCode);
	}

	@RequestMapping(value = CATEGORY_CODE_PATH_VARIABLE_PATTERN + "/loadMore", method = RequestMethod.GET)
	public String loadMoreResults(@PathVariable("categoryCode") final String categoryCode,
			@RequestParam(value = "pq", required = false, defaultValue = "") final String searchQuery,
			@RequestParam(value = "page", defaultValue = "0") final int page,
			@RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
			final Model model) throws UnsupportedEncodingException, CMSItemNotFoundException
	{
		String sortCode = parseSortCode(searchQuery);
		SearchResultsData<ProductData> results = performSearchAndGetResultsData(categoryCode,
				searchQuery, page, showMode, sortCode);
		addProductListComponentToModel(model);
		model.addAttribute("searchPageData", results);
		return ControllerConstants.Views.Pages.Search.LOAD_MORE_RESULTS;
	}

	@RequestMapping(value = CATEGORY_CODE_PATH_VARIABLE_PATTERN + LOAD_MORE_FACETS_VALUES_PATH, method = RequestMethod.GET)
	public String getAllFacetValues(@PathVariable("categoryCode") final String categoryCode,
			@RequestParam(value = "pq", required = false, defaultValue = "") final String searchQuery,
			@RequestParam(value = "page", defaultValue = "0") final int page,
			@RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
			@RequestParam(value = "facetCode") final String facetCode,
			@RequestParam(value = "sort", required = false) final String sortCode,
			final Model model)
	{
		FacetRefinement<SearchStateData> searchStateDataFacetRefinement = performSearchAndGetFacetsWithFacetLimit(categoryCode,
				searchQuery, page, getSearchPageSize(), showMode, sortCode, CATEGORY_PAGE, facetCode);
		model.addAttribute("facet", searchStateDataFacetRefinement.getFacets().get(0));
		return ControllerConstants.Views.Pages.Search.LOAD_MORE_FACET_VALUES;
	}

	@ModelAttribute("sessionCountry")
	public CountryData getSessionCountry()
	{
		return wileycomStoreSessionFacade.getSessionCountry();
	}

	@ModelAttribute("sessionCurrency")
	public CurrencyData getSessionCurrency()
	{
		return storeSessionFacade.getCurrentCurrency();
	}

	@ModelAttribute("optionalTaxShortMessage")
	public String getOptionalTaxShortMessage()
	{
		return wileyb2cI18NService.getCurrentCountryTaxShortMsg().orElse("");
	}

	@ModelAttribute("optionalTaxTooltip")
	public String getOptionalTaxTooltip()
	{
		return wileyb2cI18NService.getCurrentCountryTaxTooltip().orElse("");
	}

	@ModelAttribute("publicationDatePattern")
	public String getSessionPublicationDateFormat()
	{
		return wileyb2cI18NService.getCurrentDateFormat();
	}

}
