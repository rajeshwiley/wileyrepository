package com.wiley.wileyb2c.storefront.interceptors.beforeview;

import de.hybris.platform.acceleratorstorefrontcommons.interceptors.BeforeViewHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.servlet.ModelAndView;


public class WileyHeaderLinksBeforeViewHandler implements BeforeViewHandler
{
	@Value("${website.wileyb2c.https}")
	private String wileyb2cUrl;

	@Value("${header.links.sitemap.url}")
	private String wileyb2cSitemapUrl;

	@Override
	public void beforeView(final HttpServletRequest request, final HttpServletResponse response, final ModelAndView modelAndView)
	{
		final String sitemapLinkUrl = wileyb2cUrl + wileyb2cSitemapUrl;
		modelAndView.addObject("sitemapLinkUrl", sitemapLinkUrl);
	}
}
