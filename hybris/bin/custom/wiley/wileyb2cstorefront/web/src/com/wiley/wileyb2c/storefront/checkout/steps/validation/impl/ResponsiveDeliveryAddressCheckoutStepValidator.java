/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wileyb2c.storefront.checkout.steps.validation.impl;

import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.facades.order.WileycomCheckoutFacade;


public class ResponsiveDeliveryAddressCheckoutStepValidator extends Wileyb2cAbstractCheckoutStepValidator
{
	@Autowired
	private WileycomCheckoutFacade wileycomCheckoutFacade;

	@Override
	/** OOTB logic: checkoutFacade.hasShippingItem() has been replaced with checking of digital */
	public ValidationResults validateOnEnter(final RedirectAttributes redirectAttributes)
	{
		if (!getCheckoutFlowFacade().hasValidCart())
		{
			return ValidationResults.REDIRECT_TO_CART;
		}

		if (wileycomCheckoutFacade.isDigitalSessionCart())
		{
			return ValidationResults.REDIRECT_TO_PAYMENT_TYPE;
		}

		return ValidationResults.SUCCESS;
	}
}