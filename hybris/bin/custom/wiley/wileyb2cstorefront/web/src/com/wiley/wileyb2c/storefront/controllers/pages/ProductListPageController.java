/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wileyb2c.storefront.controllers.pages;


import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commercefacades.storesession.StoreSessionFacade;
import de.hybris.platform.commercefacades.storesession.data.CurrencyData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commerceservices.search.facetdata.FacetRefinement;
import de.hybris.platform.commerceservices.url.UrlResolver;

import java.io.UnsupportedEncodingException;
import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.wiley.core.wileyb2c.i18n.Wileyb2cI18NService;
import com.wiley.facades.wileycom.storesession.WileycomStoreSessionFacade;
import com.wiley.wileyb2c.storefront.controllers.ControllerConstants;
import com.wiley.wileyb2c.storefront.util.RequestUrlParamsHandler;
import com.wiley.wileycom.storefrontcommons.controllers.pages.AbstractWileycomCategoryPageController;
import com.wiley.wileycom.storefrontcommons.util.WileyRedirectUrlResolverUtil;
import com.wiley.wileycom.storefrontcommons.util.WileySearchUrlHelperUtil;

import static com.wiley.wileycom.storefrontcommons.constants.WileycomstorefrontcommonsConstants.CURRENT_PAGE_SIZE;
import static com.wiley.wileycom.storefrontcommons.constants.WileycomstorefrontcommonsConstants.ITEMS_PER_PAGE;
import static com.wiley.wileycom.storefrontcommons.constants.WileycomstorefrontcommonsConstants.LOAD_MORE_FACETS_VALUES_PATH;
import static com.wiley.wileycom.storefrontcommons.constants.WileycomstorefrontcommonsConstants.SUBJECTS_CATEGORY;


/**
 * Controller for a category page
 */
@Controller
@Scope("tenant")
@RequestMapping(value = "/**/products/*")
public class ProductListPageController extends AbstractWileycomCategoryPageController
{
	public static final String PRODUCT_LIST_PAGE = "productListPage";
	protected static final Logger LOG = Logger.getLogger(ProductListPageController.class);
	protected static final String CATEGORY_CODE_PATH_VARIABLE_PATTERN = "/{categorySeoLabel:.*}-{categoryCode:.*}";

	@Resource(name = "wileyb2cDefaultPageCmsPageService")
	private CMSPageService wileyb2cDefaultPageCmsPageService;

	@Resource(name = "wileyb2cCustomPlpLinkUrlResolver")
	private UrlResolver<CategoryModel> wileyb2cCustomPlpLinkUrlResolver;

	@Resource
	private WileycomStoreSessionFacade wileycomStoreSessionFacade;

	@Resource
	private StoreSessionFacade storeSessionFacade;

	@Resource
	private Wileyb2cI18NService wileyb2cI18NService;

	@Resource
	private RequestUrlParamsHandler requestUrlParamsHandler;

	@RequestMapping(value = CATEGORY_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	public String productList(@PathVariable("categoryCode") final String categoryCode,
			@RequestParam(value = "pq", required = false) final String searchQuery,
			@RequestParam(value = "page", defaultValue = "0") final int page,
			@RequestParam(value = "size", defaultValue = "0") final int size,
			@RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode, final Model model,
			final HttpServletRequest request, final HttpServletResponse response) throws UnsupportedEncodingException
	{
		final String searchQueryWithoutPage = WileySearchUrlHelperUtil.getCurrentSearchQueryWithoutPage(request);
		final Optional<Integer> pageSize = requestUrlParamsHandler.getPageSize(request, size);

		if (page < 0)
		{
			if (pageSize.isPresent())
			{
				return WileyRedirectUrlResolverUtil.getRedirectToPage(request, searchQueryWithoutPage);
			}

			return WileyRedirectUrlResolverUtil.getRedirectToPage(request,
					WileySearchUrlHelperUtil.getCurrentSearchQueryWithoutPageAndPageSize(request));
		}

		if (!pageSize.isPresent())
		{
			return WileyRedirectUrlResolverUtil.getRedirectToPage(request,
					WileySearchUrlHelperUtil.getCurrentSearchQueryWithoutPageSize(request));
		}

		final String sortCode = parseSortCode(searchQuery);

		model.addAttribute("currentSearchQuery", searchQueryWithoutPage);
		model.addAttribute(ITEMS_PER_PAGE, requestUrlParamsHandler.getAllowedItemsPerPageSize());
		model.addAttribute(CURRENT_PAGE_SIZE, requestUrlParamsHandler.getCurrentOrDefaultPageSize());

		final String redirection = performSearchAndGetResultsPage(categoryCode, searchQuery, page, pageSize.get(), showMode,
				sortCode, model, request, response, PRODUCT_LIST_PAGE);
		final String metaRobots = isFollowMetaRobots(categoryCode, searchQuery) ?
				ThirdPartyConstants.SeoRobots.NOINDEX_FOLLOW : ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW;

		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, metaRobots);

		return redirection;
	}

	@RequestMapping(value = CATEGORY_CODE_PATH_VARIABLE_PATTERN + LOAD_MORE_FACETS_VALUES_PATH, method = RequestMethod.GET)
	public String getAllFacetValues(@PathVariable("categoryCode") final String categoryCode,
			@RequestParam(value = "pq", required = false, defaultValue = "") final String searchQuery,
			@RequestParam(value = "page", defaultValue = "0") final int page,
			@RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
			@RequestParam(value = "facetCode") final String facetCode,
			@RequestParam(value = "sort", required = false) final String sortCode,
			final Model model)
	{
		FacetRefinement<SearchStateData> searchStateDataFacetRefinement = performSearchAndGetFacetsWithFacetLimit(categoryCode,
				searchQuery, page, getSearchPageSize(), showMode, sortCode, PRODUCT_LIST_PAGE, facetCode);
		model.addAttribute("facet", searchStateDataFacetRefinement.getFacets().get(0));

		return ControllerConstants.Views.Pages.Search.LOAD_MORE_FACET_VALUES;
	}

	private boolean isFollowMetaRobots(final String categoryCode, final String searchQuery)
	{
		return SUBJECTS_CATEGORY.equals(categoryCode) && searchQuery == null;
	}

	@Override
	protected CMSPageService getCmsPageService()
	{
		return wileyb2cDefaultPageCmsPageService;
	}

	@Override
	public UrlResolver<CategoryModel> getCategoryModelUrlResolver()
	{
		return wileyb2cCustomPlpLinkUrlResolver;
	}

	@ModelAttribute("sessionCountry")
	public CountryData getSessionCountry()
	{
		return wileycomStoreSessionFacade.getSessionCountry();
	}

	@ModelAttribute("sessionCurrency")
	public CurrencyData getSessionCurrency()
	{
		return storeSessionFacade.getCurrentCurrency();
	}

	@ModelAttribute("optionalTaxShortMessage")
	public String getOptionalTaxShortMessage()
	{
		return wileyb2cI18NService.getCurrentCountryTaxShortMsg().orElse("");
	}

	@ModelAttribute("optionalTaxTooltip")
	public String getOptionalTaxTooltip()
	{
		return wileyb2cI18NService.getCurrentCountryTaxTooltip().orElse("");
	}

	@ModelAttribute("publicationDatePattern")
	public String getSessionPublicationDateFormat()
	{
		return wileyb2cI18NService.getCurrentDateFormat();
	}
}
