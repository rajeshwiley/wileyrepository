/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wileyb2c.storefront.filters;

import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.order.CommerceCartRestorationException;
import de.hybris.platform.core.model.order.CartModel;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wiley.core.util.Assert;
import com.wiley.storefrontcommons.filters.WileyCartRestorationFilter;


/**
 * Filter that the restores the user's cart. This is a spring configured filter that is executed by the
 * PlatformFilterChain.
 */
public class CartRestorationFilter extends WileyCartRestorationFilter
{
	private static final Logger LOG = LoggerFactory.getLogger(CartRestorationFilter.class);

	@Override
	public void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response,
			final FilterChain filterChain) throws IOException, ServletException
	{
		if (getUserService().isAnonymousUser(getUserService().getCurrentUser()))
		{
			if (getCartService().hasSessionCart()
					&& getBaseSiteService().getCurrentBaseSite().equals(
					getBaseSiteService().getBaseSiteForUID(getSiteForSessionCart().getUid())))
			{
				final String guid = getCartService().getSessionCart().getGuid();

				if (!StringUtils.isEmpty(guid))
				{
					getCartRestoreCookieGenerator().addCookie(response, guid);
				}
			}
			else if (request.getSession().isNew()
					|| (getCartService().hasSessionCart() && !getBaseSiteService().getCurrentBaseSite().equals(
					getBaseSiteService().getBaseSiteForUID(getSiteForSessionCart().getUid()))))
			{
				String cartGuid = null;

				if (request.getCookies() != null)
				{
					final String anonymousCartCookieName = getCartRestoreCookieGenerator().getCookieName();

					for (final Cookie cookie : request.getCookies())
					{
						if (anonymousCartCookieName.equals(cookie.getName()))
						{
							cartGuid = cookie.getValue();
							break;
						}
					}
				}

				if (!StringUtils.isEmpty(cartGuid))
				{
					getSessionService().setAttribute(WebConstants.CART_RESTORATION_SHOW_MESSAGE, Boolean.TRUE);
					try
					{
						getSessionService().setAttribute(
								WebConstants.CART_RESTORATION, getCartFacade().restoreSavedCart(cartGuid));
					}
					catch (final CommerceCartRestorationException e)
					{
						getSessionService().setAttribute(WebConstants.CART_RESTORATION_ERROR_STATUS,
								WebConstants.CART_RESTORATION_ERROR_STATUS);
						LOG.debug(String.format("Unable to restore a cart with GUID %s: %s", cartGuid, e.getMessage()), e);
					}
				}
			}

		}
		else
		{
			if ((!getCartService().hasSessionCart() && getSessionService().getAttribute(WebConstants.CART_RESTORATION) == null)
					|| (getCartService().hasSessionCart() && !getBaseSiteService().getCurrentBaseSite().equals(
					getBaseSiteService().getBaseSiteForUID(getSiteForSessionCart().getUid()))))
			{
				getSessionService().setAttribute(WebConstants.CART_RESTORATION_SHOW_MESSAGE, Boolean.TRUE);
				try
				{
					getSessionService().setAttribute(WebConstants.CART_RESTORATION, getCartFacade().restoreSavedCart(null));
				}
				catch (final CommerceCartRestorationException e)
				{
					getSessionService().setAttribute(WebConstants.CART_RESTORATION, WebConstants.CART_RESTORATION_ERROR_STATUS);
					LOG.debug("Unable to restore a cart: " + e.getMessage(), e);
				}
			}
		}

		filterChain.doFilter(request, response);
	}

	private BaseSiteModel getSiteForSessionCart()
	{
		final CartModel sessionCart = getCartService().getSessionCart();
		Assert.state(sessionCart != null, () -> "Session cart should not be null.");

		final BaseSiteModel site = sessionCart.getSite();
		Assert.state(site != null,
				() -> String.format("Site in session cart [%s] should not be null.", sessionCart.getCode()));

		return site;
	}
}
