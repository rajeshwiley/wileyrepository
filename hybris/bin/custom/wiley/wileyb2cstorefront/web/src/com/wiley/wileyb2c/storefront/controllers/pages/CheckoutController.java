/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wileyb2c.storefront.controllers.pages;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.enums.OrderType;
import com.wiley.core.util.Assert;
import com.wiley.core.wileycom.customer.exception.WileycomCustomerRegistrationConflictingIdException;
import com.wiley.core.wileycom.customer.exception.WileycomCustomerRegistrationFailureException;
import com.wiley.facades.wileycom.storesession.WileycomStoreSessionFacade;
import com.wiley.facades.order.WileycomCheckoutFacade;
import com.wiley.facades.wileyb2c.customer.Wileyb2cCustomerFacade;
import com.wiley.facades.wileyb2c.order.Wileyb2cOrderFacade;
import com.wiley.wileyb2c.storefront.controllers.ControllerConstants;
import com.wiley.wileycom.storefrontcommons.forms.WileycomGuestRegisterForm;
import de.hybris.platform.acceleratorfacades.flow.impl.SessionOverrideCheckoutFlowFacade;
import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractCheckoutController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.GuestRegisterValidator;
import de.hybris.platform.acceleratorstorefrontcommons.security.AutoLoginStrategy;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.order.CheckoutFacade;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;



import static com.wiley.storefrontcommons.constants.WileystorefrontcommonsConstants.FORWARD_TO_404;
import static de.hybris.platform.commercefacades.product.ProductOption.BASIC;
import static de.hybris.platform.commercefacades.product.ProductOption.FREE_TRIAL_SUBSCRIPTION_TERM;
import static de.hybris.platform.commercefacades.product.ProductOption.GALLERY;


/**
 * CheckoutController
 */
@Controller
@Scope("tenant")
@RequestMapping(value = "/checkout")
@Profile(WileyCoreConstants.WILEY_PHASE2A_PROFILE_NAME)
public class CheckoutController extends AbstractCheckoutController
{
	protected static final Logger LOG = Logger.getLogger(CheckoutController.class);
	/**
	 * We use this suffix pattern because of an issue with Spring 3.1 where a Uri value is incorrectly extracted if it
	 * contains on or more '.' characters. Please see https://jira.springsource.org/browse/SPR-6164 for a discussion on
	 * the issue and future resolution.
	 */
	private static final String ORDER_CODE_PATH_VARIABLE_PATTERN = "{orderCode:.*}";

	private static final String CHECKOUT_ORDER_CONFIRMATION_CMS_PAGE_LABEL = "orderConfirmation";
	private static final String COURSE_ORDER_CONFIRMATION_CMS_PAGE_LABEL = "courseOrderConfirmation";
	private static final String FREE_TRIAL_CONFIRMATION_CMS_PAGE = "free-trial-confirmation";
	private static final String CONTINUE_URL_KEY = "continueUrl";
	private static final String DIGITAL_PRODUCT_PAGE = "/my-account/digital-products";
	private static final List<ProductOption> PRODUCT_OPTIONS = Arrays.asList(BASIC, GALLERY, FREE_TRIAL_SUBSCRIPTION_TERM);
	public static final String WILEYCOM_GUEST_REGISTER_FORM = "wileycomGuestRegisterForm";

	@Autowired
	@Qualifier(value = "wileyb2cProductFacade")
	private ProductFacade wileyb2cProductFacade;

	@Resource(name = "wileyb2cOrderFacade")
	private Wileyb2cOrderFacade wileyb2cOrderFacade;

	@Resource(name = "checkoutFacade")
	private CheckoutFacade checkoutFacade;

	@Resource(name = "wileyb2cCustomerFacade")
	private Wileyb2cCustomerFacade wileyb2cCustomerFacade;

	@Resource(name = "guestRegisterValidator")
	private GuestRegisterValidator guestRegisterValidator;

	@Resource
	private WileycomCheckoutFacade wileycomCheckoutFacade;

	@Resource(name = "autoLoginStrategy")
	private AutoLoginStrategy autoLoginStrategy;

	@Resource
	WileycomStoreSessionFacade wileycomStoreSessionFacade;

	@ExceptionHandler(ModelNotFoundException.class)
	public String handleModelNotFoundException(final ModelNotFoundException exception, final HttpServletRequest request)
	{
		request.setAttribute("message", exception.getMessage());
		return FORWARD_TO_404;
	}

	@ModelAttribute("sessionCountry")
	public CountryData getSessionCountry()
	{
		return wileycomStoreSessionFacade.getSessionCountry();
	}

	@RequestMapping(method = RequestMethod.GET)
	public String checkout(final RedirectAttributes redirectModel)
	{
		if (getCheckoutFlowFacade().hasValidCart())
		{
			if (validateCart(redirectModel))
			{
				return REDIRECT_PREFIX + "/cart";
			}
			else
			{
				checkoutFacade.prepareCartForCheckout();
				return getCheckoutRedirectUrl();
			}
		}

		LOG.info("Missing, empty or unsupported cart");

		// No session cart or empty session cart. Bounce back to the cart page.
		return REDIRECT_PREFIX + "/cart";
	}

	@RequestMapping(value = "/orderConfirmation/" + ORDER_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	@RequireHardLogIn
	public String orderConfirmation(@PathVariable("orderCode") final String orderCode, final HttpServletRequest request,
			final Model model) throws CMSItemNotFoundException
	{
		SessionOverrideCheckoutFlowFacade.resetSessionOverrides();
		return processOrderCode(orderCode, model, request);
	}


	@RequestMapping(value = "/orderConfirmation/" + ORDER_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.POST)
	public String orderConfirmation(final WileycomGuestRegisterForm form, final BindingResult bindingResult,
			@PathVariable("orderCode") final String orderCode, final Model model, final HttpServletRequest request,
			final HttpServletResponse response, final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException
	{
 		getGuestRegisterValidator().validate(form, bindingResult);
		return processRegisterGuestUserRequest(form, bindingResult, model, request, response, redirectModel, orderCode);
	}

	/**
	 * Method used to determine the checkout redirect URL that will handle the checkout process.
	 *
	 * @return A <code>String</code> object of the URL to redirect to.
	 */
	protected String getCheckoutRedirectUrl()
	{
		if (getUserFacade().isAnonymousUser())
		{
			return REDIRECT_PREFIX + "/login/checkout";
		}

		// Default to the multi-step checkout
		return REDIRECT_PREFIX + "/checkout/multi";
	}

	protected String processRegisterGuestUserRequest(final WileycomGuestRegisterForm form, final BindingResult bindingResult,
			final Model model, final HttpServletRequest request, final HttpServletResponse response,
			final RedirectAttributes redirectModel, final String orderCode) throws CMSItemNotFoundException
	{
		if (bindingResult.hasErrors())
		{
			GlobalMessages.addErrorMessage(model, "form.global.error");
			return processOrderCode(form.getOrderCode(), model, request);
		}
		try
		{
			getCustomerFacade().changeGuestToCustomer(form.getPwd(), form.getOrderCode(), form.getAcceptPromotions());
			getAutoLoginStrategy().login(getCustomerFacade().getCurrentCustomer().getUid(), form.getPwd(), request, response);
			getSessionService().removeAttribute(WebConstants.ANONYMOUS_CHECKOUT);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
					"registration.confirmation.message.title");
		}
		catch (final DuplicateUidException exception)
		{
			return proceedWithGuestUserRegistrationFailure(request, model, redirectModel, form, exception,
					"guest.checkout.existingaccount.register.error");
		}
		catch (final WileycomCustomerRegistrationConflictingIdException exception)
		{
			return proceedWithGuestUserRegistrationFailure(request, model, redirectModel, form, exception,
					"registration.error.account.exists.title");
		}
		catch (final WileycomCustomerRegistrationFailureException exception)
		{
			return proceedWithGuestUserRegistrationFailure(request, model, redirectModel, form, exception,
					"registration.error.process.failed");
		}

		return REDIRECT_PREFIX + "/checkout/orderConfirmation/" + convertOrderGuidToCode(orderCode);
	}

	private String proceedWithGuestUserRegistrationFailure(final HttpServletRequest request, final Model model,
			final RedirectAttributes redirectModel, final WileycomGuestRegisterForm form, final Exception exception,
			final String errorMessage)
	{
		LOG.error("guest registration failed.", exception);
		GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, errorMessage,
				new Object[] { form.getUid() });
		model.addAttribute(new WileycomGuestRegisterForm());
		return REDIRECT_PREFIX + request.getHeader("Referer");
	}

	private String convertOrderGuidToCode(final String guid) {
		return wileyb2cOrderFacade.getOrderCodeForGUID(guid);
	}

	protected String processOrderCode(final String orderCode, final Model model, final HttpServletRequest request)
			throws CMSItemNotFoundException
	{
		final OrderData orderDetails = wileyb2cOrderFacade.getOrderDetailsForCode(orderCode);

		if (orderDetails.isGuestCustomer()
				&& !StringUtils.substringBefore(orderDetails.getUser().getUid(), "|").equals(
						getSessionService().getAttribute(WebConstants.ANONYMOUS_CHECKOUT_GUID)))
		{
			return getCheckoutRedirectUrl();
		}

		if (OrderType.REGISTRATION_CODE_ACIVATION.equals(orderDetails.getOrderType())
				|| OrderType.GRACE_PERIOD.equals(orderDetails.getOrderType()))
		{
			return setupDataForCourseOrderConfirmationPage(model, orderDetails);
		}
		if (OrderType.FREE_TRIAL.equals(orderDetails.getOrderType()))
		{
			return setupDataForFreeTrialOrderConfirmationPage(model, orderDetails);
		}
		//guests cannot place digital orders
		model.addAttribute("isDigitalOrder",
				!orderDetails.isGuestCustomer() && wileycomCheckoutFacade.isDigitalOrder(orderCode));
		model.addAttribute("allItems", orderDetails.getEntries());
		model.addAttribute("pageType", PageType.ORDERCONFIRMATION.name());

		if (OrderType.REGISTRATION_CODE_ACIVATION.equals(orderDetails.getOrderType())
				|| OrderType.GRACE_PERIOD.equals(orderDetails.getOrderType()))
		{
			return setupDataForCourseOrderConfirmationPage(model, orderDetails);
		}

		model.addAttribute("orderCode", orderCode);
		model.addAttribute("orderData", orderDetails);
		model.addAttribute("deliveryAddress", orderDetails.getDeliveryAddress());
		model.addAttribute("deliveryMode", orderDetails.getDeliveryMode());
		model.addAttribute("paymentInfo", orderDetails.getPaymentInfo());

		final String uid;

		if (orderDetails.isGuestCustomer() && !model.containsAttribute(WILEYCOM_GUEST_REGISTER_FORM))
		{
			uid = orderDetails.getPaymentInfo().getBillingAddress().getEmail();
			final WileycomGuestRegisterForm guestRegisterForm = new WileycomGuestRegisterForm();
			guestRegisterForm.setOrderCode(orderDetails.getGuid());
			guestRegisterForm.setUid(uid);
			model.addAttribute(WILEYCOM_GUEST_REGISTER_FORM, guestRegisterForm);
		}
		else
		{
			uid = orderDetails.getUser().getUid();
		}
		model.addAttribute("email", uid);
		final String continueUrl = getSessionService().getAttribute(WebConstants.CONTINUE_URL);
		model.addAttribute(CONTINUE_URL_KEY, (continueUrl != null && !continueUrl.isEmpty()) ? continueUrl : ROOT);

		setupPage(model, CHECKOUT_ORDER_CONFIRMATION_CMS_PAGE_LABEL);
		return ControllerConstants.Views.Pages.Checkout.CHECKOUT_CONFIRMATION_PAGE;
	}

	private String setupDataForCourseOrderConfirmationPage(final Model model, final OrderData orderDetails)
			throws CMSItemNotFoundException
	{
		model.addAttribute("allItems", orderDetails.getEntries());
		model.addAttribute("pageType", PageType.ORDERCONFIRMATION.name());
		model.addAttribute("gracePeriod", orderDetails.getPurchaseOptionGracePeriodDuration());
		model.addAttribute("orderType", orderDetails.getOrderType().getCode());
		final String continueUrl = orderDetails.getPurchaseOptionContinueURL();
		model.addAttribute(CONTINUE_URL_KEY,
					(continueUrl != null && !continueUrl.isEmpty()) ? continueUrl : DIGITAL_PRODUCT_PAGE);
		setupPage(model, COURSE_ORDER_CONFIRMATION_CMS_PAGE_LABEL);
		return ControllerConstants.Views.Pages.Course.COURSE_ORDER_CONFIRMATION_PAGE;
	}

	private String setupDataForFreeTrialOrderConfirmationPage(final Model model, final OrderData orderDetails)
			throws CMSItemNotFoundException
	{
		Assert.state(CollectionUtils.isNotEmpty(orderDetails.getEntries())
				&& orderDetails.getEntries().get(0).getProduct() != null,
				() -> "Free trial order  should contain entry with product");
		Assert.state(orderDetails.getOrderType() != null, () -> "OrderType shouldn't be null");
		model.addAttribute("pageType", PageType.ORDERCONFIRMATION.name());
		model.addAttribute("orderType", orderDetails.getOrderType().getCode());
		final String productCode = orderDetails.getEntries().get(0).getProduct().getCode();
		final ProductData product = wileyb2cProductFacade.getProductForCodeAndOptions(productCode, PRODUCT_OPTIONS);
		model.addAttribute(product);
		setupPage(model, FREE_TRIAL_CONFIRMATION_CMS_PAGE);
		return getViewForPage(model);
	}

	protected void setupPage(final Model model, final String pageLabel) throws CMSItemNotFoundException
	{
		final ContentPageModel cmsPage = getContentPageForLabelOrId(pageLabel);
		storeCmsPageInModel(model, cmsPage);
		setUpMetaDataForContentPage(model, cmsPage);
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
	}

	protected GuestRegisterValidator getGuestRegisterValidator()
	{
		return guestRegisterValidator;
	}

	protected AutoLoginStrategy getAutoLoginStrategy()
	{
		return autoLoginStrategy;
	}

	@Override
	protected Wileyb2cCustomerFacade getCustomerFacade()
	{
		return wileyb2cCustomerFacade;
	}
}
