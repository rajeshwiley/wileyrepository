package com.wiley.wileyb2c.storefront.renderer;

import de.hybris.platform.acceleratorcms.component.renderer.impl.CMSParagraphComponentRenderer;
import de.hybris.platform.cms2.model.contents.components.CMSParagraphComponentModel;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

import org.apache.commons.lang.StringUtils;


/**
 * CMSParagraphComponentRenderer for B2C
 */
public class Wileyb2cCMSParagraphComponentRenderer extends CMSParagraphComponentRenderer
{
	@Override
	public void renderComponent(final PageContext pageContext, final CMSParagraphComponentModel component)
			throws ServletException, IOException
	{
		// <div class="content cke-content">${content}</div>
		final JspWriter out = pageContext.getOut();

		out.write("<div class=\"content cke-content\">");
		out.write(component.getContent() == null ? StringUtils.EMPTY : component.getContent());
		out.write("</div>");
	}
}
