/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wileyb2c.storefront.security.impl;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wiley.storefrontcommons.security.impl.WileyCommerceRedirectStrategy;
import com.wiley.wileyb2c.storefront.security.StorefrontAuthenticationSuccessHandler;


/**
 * A redirect strategy used in
 * {@link StorefrontAuthenticationSuccessHandler} to handle express
 * checkout case
 */
public class Wileyb2cCommerceRedirectStrategy extends WileyCommerceRedirectStrategy
{
	@Override
	public void sendRedirect(final HttpServletRequest request, final HttpServletResponse response, final String url)
			throws IOException
	{
		String redirectUrl = url;

		if (Boolean.valueOf(getCheckoutFlowFacade().isExpressCheckoutEnabledForStore()))
		{
			redirectUrl = getExpressTargetUrl();
		}
		super.sendRedirect(request, response, redirectUrl);
	}
}
