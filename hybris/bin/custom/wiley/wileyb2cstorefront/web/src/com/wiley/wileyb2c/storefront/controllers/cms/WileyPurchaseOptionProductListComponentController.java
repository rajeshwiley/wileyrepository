package com.wiley.wileyb2c.storefront.controllers.cms;

import de.hybris.platform.commercefacades.product.data.ProductData;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.wiley.core.model.components.WileyPurchaseOptionProductListComponentModel;
import com.wiley.facades.cms.WileyPurchaseOptionProductListComponentFacade;
import com.wiley.wileyb2c.storefront.controllers.ControllerConstants;


/**
 * @author Maksim_Kozich
 */
@Controller("WileyPurchaseOptionProductListComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.WILEY_PURCHASE_OPTION_PRODUCT_LIST_COMPONENT)
public class WileyPurchaseOptionProductListComponentController extends
		AbstractCMSComponentController<WileyPurchaseOptionProductListComponentModel>
{

	@Resource(name = "wileyb2cPurchaseOptionProductListComponentFacade")
	private WileyPurchaseOptionProductListComponentFacade wileyb2cPurchaseOptionProductListComponentFacade;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model,
			final WileyPurchaseOptionProductListComponentModel component)
	{
		final List<ProductData> products = wileyb2cPurchaseOptionProductListComponentFacade.getComponentProducts(component);

		model.addAttribute("headline", component.getHeadline());

		model.addAttribute("products", products);
		model.addAttribute("cssClasses", component.getCssClasses().stream().toArray(String[]::new));
		model.addAttribute("actions", component.getActions());
		model.addAttribute("APROMCODE", component.getDiscount().getCode());
		if (component.getSchool() != null && component.getSchool().getFiceId() != null)
		{
			model.addAttribute("AFICECODE", component.getSchool().getFiceId());
		}
	}
}
