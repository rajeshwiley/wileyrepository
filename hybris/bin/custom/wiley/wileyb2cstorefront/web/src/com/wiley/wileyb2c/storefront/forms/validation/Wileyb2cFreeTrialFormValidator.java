package com.wiley.wileyb2c.storefront.forms.validation;

import de.hybris.platform.commerceservices.strategies.CustomerNameStrategy;


import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.wiley.wileyb2c.storefront.forms.Wileyb2cFreeTrialForm;
import com.wiley.wileycom.storefrontcommons.validators.FieldValidator;



/**
 * Free trial form validator
 */
public class Wileyb2cFreeTrialFormValidator implements Validator
{

	static final String FIRST_NAME_FIELD_KEY = "firstName";
	static final String FIRST_NAME_ERROR_KEY = "freetrial.firstName.invalid";
	static final String LAST_NAME_FIELD_KEY = "lastName";
	static final String LAST_NAME_ERROR_KEY = "freetrial.lastName.invalid";
	static final String NAME_COMBINED_LENGTH_ERROR_KEY = "freetrial.name.invalid";
	static final int  MAX_COMBINED_VALUE = 255;

	@Autowired
	private FieldValidator firstNameSizeFieldValidator;

	@Autowired
	private FieldValidator sizeFieldValidator;

	@Autowired
	private CustomerNameStrategy customerNameStrategy;

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return Wileyb2cFreeTrialForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object objectForm, final Errors errors)
	{
		firstNameSizeFieldValidator.validate(errors, FIRST_NAME_FIELD_KEY, FIRST_NAME_ERROR_KEY);
		sizeFieldValidator.validate(errors, LAST_NAME_FIELD_KEY, LAST_NAME_ERROR_KEY);

		Wileyb2cFreeTrialForm freeTrialForm = (Wileyb2cFreeTrialForm) objectForm;
		final String combinedName = customerNameStrategy.getName(freeTrialForm.getFirstName(), freeTrialForm.getLastName());
		if (StringUtils.isNotEmpty(combinedName) && combinedName.length() > MAX_COMBINED_VALUE)
		{
			errors.rejectValue(FIRST_NAME_FIELD_KEY, NAME_COMBINED_LENGTH_ERROR_KEY);
			errors.rejectValue(LAST_NAME_FIELD_KEY, NAME_COMBINED_LENGTH_ERROR_KEY);
		}
	}
}
