package com.wiley.wileyb2c.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.exceptions.ExternalSystemException;
import com.wiley.facades.subscription.Wileyb2cSubscriptionFacade;
import com.wiley.facades.wiley.visibility.WileyProductVisibilityFacade;
import com.wiley.facades.wileyb2c.customer.Wileyb2cCustomerFacade;
import com.wiley.facades.wileyb2c.order.Wileyb2cFreeTrialCheckoutFacade;
import com.wiley.wileyb2c.storefront.forms.Wileyb2cFreeTrialForm;
import com.wiley.wileyb2c.storefront.forms.validation.Wileyb2cFreeTrialFormValidator;

import static com.wiley.storefrontcommons.constants.WileystorefrontcommonsConstants.FORWARD_TO_404;
import static com.wiley.storefrontcommons.constants.WileystorefrontcommonsConstants.REDIRECT_TO_404;
import static de.hybris.platform.commercefacades.product.ProductOption.BASIC;
import static de.hybris.platform.commercefacades.product.ProductOption.FREE_TRIAL_SUBSCRIPTION_TERM;
import static de.hybris.platform.commercefacades.product.ProductOption.GALLERY;


/**
 * Controller for free trial page
 */
@Controller
@Profile(WileyCoreConstants.WILEY_PHASE2A_PROFILE_NAME)
@RequestMapping("/**/free-trial")
public class FreeTrialPageController extends AbstractPageController
{
	private static final Logger LOG = LoggerFactory.getLogger(FreeTrialPageController.class);

	private static final String FREE_TRIAL_CMS_PAGE = "start-free-trial";
	private static final String ATTRIBUTE_FREE_TRIAL_FORM = "wileyb2cFreeTrialForm";
	private static final String ORDER_CONFIRM_PAGE = REDIRECT_PREFIX + "/checkout/orderConfirmation/";
	private static final List<ProductOption> PRODUCT_OPTIONS = Arrays.asList(BASIC, GALLERY, FREE_TRIAL_SUBSCRIPTION_TERM);

	@Autowired
	private Wileyb2cFreeTrialFormValidator wileyb2cFreeTrialFormValidator;

	@Autowired
	private Wileyb2cCustomerFacade customerFacade;

	@Autowired
	private Wileyb2cSubscriptionFacade wileyb2cSubscriptionFacade;

	@Autowired
	private Wileyb2cFreeTrialCheckoutFacade wileyb2cFreeTrialCheckoutFacade;

	@Autowired
	@Qualifier(value = "wileyb2cProductFacade")
	private ProductFacade wileyb2cProductFacade;

	@Resource
	private WileyProductVisibilityFacade wileyProductVisibilityFacade;

	@RequireHardLogIn
	@RequestMapping(value = "/{freeTrialCode}", method = RequestMethod.GET)
	public String showPage(@PathVariable("freeTrialCode") final String freeTrialCode, final Model model)
			throws CMSItemNotFoundException
	{
		if (wileyProductVisibilityFacade.isProductVisible(freeTrialCode) && wileyb2cSubscriptionFacade.isProductFreeTrial(
				freeTrialCode))
		{
			final ProductData product = wileyb2cProductFacade.getProductForCodeAndOptions(freeTrialCode, PRODUCT_OPTIONS);
			final CustomerData currentCustomer = customerFacade.getCurrentCustomer();

			model.addAttribute(product);
			storeCmsPageInModel(model, getCmsPageService().getPageForId(FREE_TRIAL_CMS_PAGE));
			storeFreeTrialFormInModel(model, currentCustomer);
			boolean isCustomerHasSubscription = wileyb2cSubscriptionFacade.isCustomerHasSubscription(freeTrialCode,
					currentCustomer.getUid());
			storeInfoAboutUsedSubscriptionInModel(model, isCustomerHasSubscription);
			return getViewForPage(model);
		}
		else
		{
			return FORWARD_TO_404;
		}
	}

	@RequireHardLogIn
	@RequestMapping(value = "/{freeTrialCode}", method = RequestMethod.POST)
	public String submitFreeTrialForm(@PathVariable("freeTrialCode") final String freeTrialCode, final Model model,
			@ModelAttribute(ATTRIBUTE_FREE_TRIAL_FORM) final Wileyb2cFreeTrialForm wileyb2cFreeTrialForm,
			final BindingResult bindingResult)
			throws CMSItemNotFoundException
	{
		if (wileyProductVisibilityFacade.isProductVisible(freeTrialCode))
		{
			final ProductData product = wileyb2cProductFacade.getProductForCodeAndOptions(freeTrialCode, PRODUCT_OPTIONS);
			final CustomerData currentCustomer = customerFacade.getCurrentCustomer();

			boolean isCustomerHasSubscription = wileyb2cSubscriptionFacade.isCustomerHasSubscription(freeTrialCode,
					currentCustomer.getUid());
			if (isCustomerHasSubscription)
			{
				prepareTrialPage(model, true, wileyb2cFreeTrialForm, product, currentCustomer);
				return getViewForPage(model);
			}

			wileyb2cFreeTrialFormValidator.validate(wileyb2cFreeTrialForm, bindingResult);
			if (bindingResult.hasErrors())
			{
				GlobalMessages.addErrorMessage(model, "form.global.error");
				prepareTrialPage(model, false, wileyb2cFreeTrialForm, product, currentCustomer);
				return getViewForPage(model);
			}

			CustomerData customerData = new CustomerData();
			customerData.setFirstName(wileyb2cFreeTrialForm.getFirstName());
			customerData.setLastName(wileyb2cFreeTrialForm.getLastName());
			try
			{
				customerFacade.updateProfileIfChanged(customerData);
			}
			catch (DuplicateUidException e)
			{
				bindingResult.rejectValue("email", "registration.error.account.exists.title");
				prepareTrialPage(model, false, wileyb2cFreeTrialForm, product, currentCustomer);
				return getViewForPage(model);
			}
			catch (ExternalSystemException e)
			{
				LOG.debug("can not update user '" + wileyb2cFreeTrialForm.getEmail() + "' due to, " + e.getMessage(), e);
			}

			try
			{
				String orderCode = wileyb2cFreeTrialCheckoutFacade.placeFreeTrialOrder(freeTrialCode);
				return ORDER_CONFIRM_PAGE + orderCode;
			}
			catch (CommerceCartModificationException | InvalidCartException e)
			{
				LOG.debug("can not place free Trial '" + freeTrialCode + " for " + wileyb2cFreeTrialForm.getEmail() + "' due to, "
						+ e
						.getMessage(), e);
				GlobalMessages.addErrorMessage(model, "text.startFreeTrial.error.place");
			}
			prepareTrialPage(model, isCustomerHasSubscription, wileyb2cFreeTrialForm, product, currentCustomer);
			return getViewForPage(model);
		}
		else
		{
			return FORWARD_TO_404;
		}
	}

	private void prepareTrialPage(final Model model, boolean isCustomerHasSubscription,
			final Wileyb2cFreeTrialForm wileyb2cFreeTrialForm, final ProductData product, final CustomerData currentCustomer)
			throws CMSItemNotFoundException
	{
		storeInfoAboutUsedSubscriptionInModel(model, isCustomerHasSubscription);
		storeCmsPageInModel(model, getCmsPageService().getPageForId(FREE_TRIAL_CMS_PAGE));
		wileyb2cFreeTrialForm.setEmail(currentCustomer.getUid());
		model.addAttribute(wileyb2cFreeTrialForm);
		model.addAttribute(product);
	}

	private void storeFreeTrialFormInModel(final Model model, final CustomerData customerData)
	{
		final Wileyb2cFreeTrialForm wileyb2cFreeTrialForm = new Wileyb2cFreeTrialForm();
		wileyb2cFreeTrialForm.setFirstName(customerData.getFirstName());
		wileyb2cFreeTrialForm.setLastName(customerData.getLastName());
		wileyb2cFreeTrialForm.setEmail(customerData.getUid());
		model.addAttribute(wileyb2cFreeTrialForm);
	}

	private void storeInfoAboutUsedSubscriptionInModel(final Model model, boolean isCustomerHasSubscription)
	{
		model.addAttribute("isCustomerHasSubscription", isCustomerHasSubscription);
		if (isCustomerHasSubscription)
		{
			GlobalMessages.addErrorMessage(model, "text.startFreeTrial.error.alreadyUsed");
		}
	}

	@ExceptionHandler({ ModelNotFoundException.class, UnknownIdentifierException.class, IllegalArgumentException.class })
	public String handleModelNotFoundException(final RuntimeException exception, final HttpServletRequest request)
	{
		request.setAttribute("message", exception.getMessage());
		return REDIRECT_TO_404;
	}
}