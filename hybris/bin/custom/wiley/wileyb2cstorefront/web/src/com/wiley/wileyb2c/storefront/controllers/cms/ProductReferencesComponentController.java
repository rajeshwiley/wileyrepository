/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wileyb2c.storefront.controllers.cms;

import de.hybris.platform.acceleratorcms.model.components.ProductReferencesComponentModel;
import de.hybris.platform.commercefacades.product.data.ProductReferenceData;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.wiley.facades.wiley.util.WileySemanticUrlGenerator;
import com.wiley.facades.wileyb2c.product.Wileyb2cProductFacade;
import com.wiley.wileyb2c.storefront.controllers.ControllerConstants;


/**
 * Controller for CMS ProductReferencesComponent
 */
@Controller("ProductReferencesComponentController")
@Scope("tenant")
@RequestMapping(value = ControllerConstants.Actions.Cms.PRODUCT_REFERENCES_COMPONENT)
public class ProductReferencesComponentController extends AbstractCMSComponentController<ProductReferencesComponentModel>
{
	@Resource(name = "wileyb2cProductFacade")
	private Wileyb2cProductFacade wileyProductFacade;

	@Resource
	private WileySemanticUrlGenerator wileySemanticUrlGenerator;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final ProductReferencesComponentModel component)
	{
		final ProductModel currentProduct = getRequestContextData(request).getProduct();
		if (currentProduct != null)
		{
			final String sectionId = wileySemanticUrlGenerator.generateFor(component.getUid());
			final boolean isDisplayProductsDespiteRestrictions = component.isDisplayProductsDespiteRestrictions();

			final List<ProductReferenceData> productReferences = wileyProductFacade.getProductReferencesForCodes(
					Collections.singletonList(currentProduct.getCode()), component.getProductReferenceTypes(),
					isDisplayProductsDespiteRestrictions, component.getMaximumNumberProducts());

			model.addAttribute("title", component.getTitle());
			model.addAttribute("sectionId", String.format("%s-section", sectionId));
			model.addAttribute("productReferences", productReferences);
		}
	}

}
