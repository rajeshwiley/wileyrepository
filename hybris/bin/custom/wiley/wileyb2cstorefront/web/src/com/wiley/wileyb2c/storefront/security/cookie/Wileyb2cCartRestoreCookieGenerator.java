package com.wiley.wileyb2c.storefront.security.cookie;

import de.hybris.platform.store.services.BaseStoreService;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;

import com.wiley.storefrontcommons.security.cookie.CartRestoreCookieGenerator;


/**
 * Wileyb2c specific cart restore cookie generator.
 */
public class Wileyb2cCartRestoreCookieGenerator extends CartRestoreCookieGenerator
{

	@Resource
	private BaseStoreService baseStoreService;

	@Override
	public String getCookieName()
	{
		return StringUtils.deleteWhitespace(baseStoreService.getCurrentBaseStore().getUid()) + "-cart";
	}

}
