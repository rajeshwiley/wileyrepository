/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wileyb2c.storefront.controllers.misc;

import de.hybris.platform.acceleratorservices.constants.AcceleratorServicesConstants;
import de.hybris.platform.acceleratorservices.urlencoder.UrlEncoderService;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.AbstractController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.wiley.facades.locale.WileyLocaleFacade;
import com.wiley.facades.user.WileyUserFacade;
import com.wiley.facades.wileycom.storesession.WileycomStoreSessionFacade;
import com.wiley.facades.wileycom.urlencoder.WileycomEncoderFacade;
import com.wiley.storefrontcommons.filters.StorefrontFilter;
import com.wiley.storefrontcommons.security.cookie.EnhancedCookieGenerator;

import static com.wiley.storefrontcommons.constants.WileystorefrontcommonsConstants.REDIRECT_TO_404;
import static com.wiley.wileycom.storefrontcommons.constants.WileycomstorefrontcommonsConstants.ORIGINAL_CONTEXT_PATH;



/**
 * Controller for store session. Used to change the session country
 */
@Controller
@RequestMapping("/_s")
public class StoreSessionController extends AbstractController
{
	private static final Logger LOG = LoggerFactory.getLogger(StoreSessionController.class);

	@Resource(name = "wileyUserFacade")
	private WileyUserFacade wileyUserFacade;

	@Resource(name = "urlEncoderService")
	private UrlEncoderService urlEncoderService;

	@Resource
	private WileycomEncoderFacade wileycomEncoderFacade;

	@Resource
	private WileyLocaleFacade wileyLocaleFacade;

	@Resource
	private WileycomStoreSessionFacade wileycomStoreSessionFacade;

	@Resource
	private EnhancedCookieGenerator wileyb2cCountryCookieGenerator;

	@RequestMapping(value = "/country", method = { RequestMethod.GET, RequestMethod.POST })
	public String selectCountry(@RequestParam("code") final String isoCode,
			@RequestParam(name = "pageReferer", required = false) final String pageReferer,
			@RequestParam(name = "addLocaleCookie", required = false, defaultValue = "true") final boolean addLocaleCookie,
			final HttpServletRequest request, final HttpServletResponse response) throws IOException
	{

		Optional<String> externalSiteRedirectLink = wileycomStoreSessionFacade.getExternalSiteRedirect(isoCode);
		if (externalSiteRedirectLink.isPresent()) {
			LOG.debug("External site redirect occurs during changing country to [{}], redirecting to [{}]",
					isoCode, externalSiteRedirectLink.get());
			return REDIRECT_PREFIX + externalSiteRedirectLink.get();
		}


		wileycomStoreSessionFacade.setCurrentCountry(isoCode);
		wileyUserFacade.syncSessionCurrency();
		wileyUserFacade.syncSessionLanguage();

		String currentEncodedLocale = wileyLocaleFacade.getCurrentEncodedLocale();
		if (addLocaleCookie)
		{
			wileyb2cCountryCookieGenerator.addCookie(response, currentEncodedLocale);
		}
		if (urlEncoderService.isLanguageEncodingEnabled())
		{
			return getReturnRedirectUrlForUrlEncoding(request, pageReferer,
					AcceleratorServicesConstants.LANGUAGE_ENCODING, currentEncodedLocale);
		}
		else
		{
			return getReturnRedirectUrlWithoutReferer(request);
		}
	}

	protected String getReturnRedirectUrlWithoutReferer(final HttpServletRequest request)
	{
		final String originalReferer = (String) request.getSession().getAttribute(StorefrontFilter.ORIGINAL_REFERER);
		if (StringUtils.isNotBlank(originalReferer))
		{
			return REDIRECT_PREFIX + originalReferer;
		}

		final String referer = StringUtils.remove(request.getRequestURL().toString(), request.getServletPath());
		if (referer != null && !referer.isEmpty())
		{
			return REDIRECT_PREFIX + referer;
		}
		return REDIRECT_PREFIX + '/';
	}


	protected String getReturnRedirectUrlForUrlEncoding(final HttpServletRequest request, final String pageReferer,
			final String encodingAttributeName, final String currentValue) throws UnsupportedEncodingException
	{
		final String originalReferer = pageReferer != null ? pageReferer : (String) request.getSession().getAttribute(
				StorefrontFilter.ORIGINAL_REFERER);
		if (StringUtils.isNotBlank(originalReferer))
		{
			try
			{
				String url = wileycomEncoderFacade.substituteValue(originalReferer,
						(String) request.getAttribute(ORIGINAL_CONTEXT_PATH), encodingAttributeName, currentValue);
				return REDIRECT_PREFIX + url;
			}
			catch (URISyntaxException e)
			{
				LOG.error("wileycomEncoderFacade can't process Original Referer", e);
			}

		}

		String referer = StringUtils.remove(request.getRequestURL().toString(), request.getServletPath());
		if (!StringUtils.endsWith(referer, "/"))
		{
			referer = referer + "/";
		}
		if (referer != null && !referer.isEmpty())
		{
			try
			{
				String url = wileycomEncoderFacade.substituteValue(originalReferer,
						(String) request.getAttribute(ORIGINAL_CONTEXT_PATH), encodingAttributeName, currentValue);
				return REDIRECT_PREFIX + url;
			}
			catch (URISyntaxException e)
			{
				LOG.error("wileycomEncoderFacade can't process Original Referer", e);
			}
		}
		return REDIRECT_PREFIX + referer;
	}

	@ExceptionHandler(UnknownIdentifierException.class)
	public String handleUnknownIdentifierException(final UnknownIdentifierException exception, final HttpServletRequest request)
	{
		final Map<String, Object> currentFlashScope = RequestContextUtils.getOutputFlashMap(request);
		currentFlashScope.put(GlobalMessages.ERROR_MESSAGES_HOLDER, exception.getMessage());
		return REDIRECT_TO_404;
	}
}
