package com.wiley.wileyb2c.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.GuestForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.LoginForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.RegisterForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.GuestValidator;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.user.UserService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.wileycom.customer.exception.WileycomCustomerRegistrationConflictingIdException;
import com.wiley.core.wileycom.customer.exception.WileycomCustomerRegistrationFailureException;
import com.wiley.storefrontcommons.forms.validation.WileyRegistrationValidator;
import com.wiley.wileyb2c.storefront.controllers.ControllerConstants;


/**
 * Checkout Create Account Controller. Handles register for the checkout flow.
 */
@Controller
@Scope("tenant")
@RequestMapping("/login/checkout")
@Profile(WileyCoreConstants.WILEY_PHASE2A_PROFILE_NAME)
public class CheckoutCreateAccountController extends AbstractCheckoutLoginController
{
	private static final String CHECKOUT_REGISTER_URL = "/login/checkout/register";
	private static final String CHECKOUT_LOGIN_URL = "/login/checkout";
	private static final Logger LOG = Logger.getLogger(CheckoutCreateAccountController.class);

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "guestValidator")
	private GuestValidator guestValidator;
	
	@Resource(name = "httpSessionRequestCache")
	private HttpSessionRequestCache httpSessionRequestCache;

	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String checkoutRegister(@RequestParam(value = "error", defaultValue = "false") final boolean loginError,
			final HttpSession session, final Model model, final HttpServletRequest request) throws CMSItemNotFoundException
	{
		if (!model.containsAttribute("emailAddress"))
		{
			return REDIRECT_PREFIX + CHECKOUT_LOGIN_URL;
		}

		model.addAttribute(new RegisterForm());
		storeCmsPageInModel(model, getCmsPage());
		setUpMetaDataForContentPage(model, (ContentPageModel) getCmsPage());
		return getView();
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String doCheckoutRegister(final RegisterForm form, final BindingResult bindingResult, final Model model,
			final HttpServletRequest request, final HttpServletResponse response, final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException
	{
		final WileyRegistrationValidator registrationValidator = new WileyRegistrationValidator();
		registrationValidator.validate(form, bindingResult);
		// making email address available in the model to be displayed on the page even if there are validation errors
		model.addAttribute("emailAddress", form.getEmail());
		return processRegisterUserRequest(null, form, bindingResult, model, request, response, redirectModel);
	}

	@RequestMapping(value = "/createAccount", method = RequestMethod.POST)
	public String createAccount(final GuestForm guestForm, final BindingResult bindingResult, final Model model,
			final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		guestValidator.validate(guestForm, bindingResult);
		checkIfCustomerWithGivenEmailAlreadyExists(guestForm.getEmail(), bindingResult);
		if (bindingResult.hasErrors())
		{
			return handleGuestFormErrors(model, guestForm);
		}
		redirectAttributes.addFlashAttribute("emailAddress", guestForm.getEmail());

		return getRegisterRedirect();
	}

	@Override
	protected String processRegisterUserRequest(final String referer, final RegisterForm form, final BindingResult bindingResult,
			final Model model, final HttpServletRequest request, final HttpServletResponse response,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		try
		{
			return super.processRegisterUserRequest(referer, form, bindingResult, model, request, response, redirectModel);
		}
		catch (final WileycomCustomerRegistrationConflictingIdException exception)
		{
			LOG.info(exception.getMessage(), exception);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"registration.error.account.exists.title");
			return REDIRECT_PREFIX + "/login/checkout";
		}
		catch (final WileycomCustomerRegistrationFailureException exception)
		{
			LOG.info(exception.getMessage(), exception);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"registration.error.process.failed");
			return REDIRECT_PREFIX + "/login/checkout";
		}
	}

	@Override
	protected String getSuccessRedirect(final HttpServletRequest request, final HttpServletResponse response)
	{
		final SavedRequest savedRequest = getSavedRequest(request, response);
		if (savedRequest != null)
		{
			return savedRequest.getRedirectUrl();
		}
		else
		{
			return super.getSuccessRedirect(request, response);
		}
	}

	private SavedRequest getSavedRequest(final HttpServletRequest request, final HttpServletResponse response)
	{
		return httpSessionRequestCache.getRequest(request, response);
	}

	@Override
	protected AbstractPageModel getCmsPage() throws CMSItemNotFoundException
	{
		return this.getContentPageForLabelOrId("checkoutCreateAccountPage");
	}

	@Override
	protected String getView()
	{
		return ControllerConstants.Views.Pages.Checkout.CHECKOUT_CREATE_ACCOUNT_PAGE;
	}

	protected String getRegisterRedirect()
	{
		return REDIRECT_PREFIX + CHECKOUT_REGISTER_URL;
	}

	private String handleGuestFormErrors(final Model model, final GuestForm guestForm) throws CMSItemNotFoundException
	{
		model.addAttribute(guestForm);
		model.addAttribute(new LoginForm());
		model.addAttribute(new RegisterForm());
		model.addAttribute("suppressGuestCheckout", !wileyCheckoutFacade.isPhysicalSessionCart());

		GlobalMessages.addErrorMessage(model, "form.global.error");
		storeCmsPageInModel(model, getContentPageForLabelOrId("checkout-login"));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId("checkout-login"));
		return ControllerConstants.Views.Pages.Checkout.CHECKOUT_LOGIN_PAGE;
	}

	private void checkIfCustomerWithGivenEmailAlreadyExists(final String email, final BindingResult bindingResult)
	{
		try
		{
			final UserModel user = userService.getUserForUID(StringUtils.lowerCase(email));

			if (user != null)
			{
				bindingResult.rejectValue("email", "forgottenPwd.email.invalid");
			}
		}
		catch (final UnknownIdentifierException e)
		{
			LOG.info(String.format("No account exists with email address %s", email));
		}
	}
}