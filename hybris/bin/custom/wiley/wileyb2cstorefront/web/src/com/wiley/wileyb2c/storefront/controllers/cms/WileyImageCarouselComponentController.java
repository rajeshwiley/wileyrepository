package com.wiley.wileyb2c.storefront.controllers.cms;

import de.hybris.platform.acceleratorcms.model.components.SimpleResponsiveBannerComponentModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Optional;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.gson.Gson;
import com.wiley.core.model.WileyImageCarouselComponentModel;
import com.wiley.core.model.WileyImageCarouselConfigurationModel;
import com.wiley.core.model.components.WileyHeroBannerComponentModel;
import com.wiley.facades.wileyb2c.cms.WileyImageCarouselConfigData;
import com.wiley.wileyb2c.storefront.controllers.ControllerConstants;


@Controller("WileyImageCarouselComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.WILEY_IMAGE_CAROUSEL_COMPONENT)
public class WileyImageCarouselComponentController extends
		AbstractCMSComponentController<WileyImageCarouselComponentModel>
{
	@Resource(name = "wileyImageCarouselConfigConverter")
	private Converter<WileyImageCarouselConfigurationModel, WileyImageCarouselConfigData> wileyImageCarouselConfigConverter;

	private static final String HERO_BANNER_CAROUSEL_CLASS = "hero-banner-carousel";

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model,
			final WileyImageCarouselComponentModel component)
	{
		final WileyImageCarouselConfigurationModel configuration = component.getConfiguration();
		final Set<SimpleResponsiveBannerComponentModel> items = component.getItems();

		if (configuration != null)
		{
			WileyImageCarouselConfigData carouselConfigData =
					wileyImageCarouselConfigConverter.convert(configuration);
			model.addAttribute("carouselConfig", new Gson().toJson(carouselConfigData));
		}

		if (items != null)
		{
			Optional<SimpleResponsiveBannerComponentModel> item = items.stream()
					.filter(i -> i instanceof WileyHeroBannerComponentModel)
					.findFirst();

			item.ifPresent(e -> model.addAttribute("heroBannerCarouselClass", HERO_BANNER_CAROUSEL_CLASS));
		}
	}
}
