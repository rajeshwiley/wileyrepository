package com.wiley.wileyb2c.storefront.interceptors.beforeview;

import de.hybris.platform.acceleratorstorefrontcommons.interceptors.BeforeViewHandler;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.servlet.ModelAndView;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public class WileyDropDownDelayBeforeViewHandler implements BeforeViewHandler
{
	@Value("${wileyb2cstorefront.drop.down.open.delay}")
	private Integer openDelay;

	@Value("${wileyb2cstorefront.drop.down.close.delay}")
	private Integer closeDelay;

	@Override
	public void beforeView(final HttpServletRequest request,
			final HttpServletResponse response, final ModelAndView modelAndView)
	{
		final Map<String, Object> model = modelAndView.getModel();
		model.put("dropDownOpenDelay", openDelay);
		model.put("dropDownCloseDelay", closeDelay);
	}
}
