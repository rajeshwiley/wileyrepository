package com.wiley.wileyb2c.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.facades.wileyb2c.order.WileyRegCodeCheckoutFacade;
import com.wiley.wileyb2c.storefront.forms.RegCodeActivationForm;



/**
 * Controller for the RegCode activation page.
 */
@Controller
@Profile(WileyCoreConstants.WILEY_PHASE2A_PROFILE_NAME)
@RequestMapping(value = "checkout/regCode")
public class RegCodeActivationPageController extends AbstractPageController
{
	protected static final Logger LOG = LoggerFactory.getLogger(AbstractPageController.class);



	private static final String ACTIVATION_PAGE_LABEL = "regCodeActivation";
	private static final String CONFIRMATION_PAGE_LABEL = "regCodeConfirmation";

	private static final String REG_CODE_FIELD_ERROR = "regCodeFieldError";
	private static final String REG_CODE_ACTIVATION_FORM = "regCodeActivationForm";
	private static final String FORM_GLOBAL_ERROR_MESSAGE = "form.global.error";

	@Resource(name = "wileyRegCodeCheckoutFacade")
	private WileyRegCodeCheckoutFacade checkoutFacade;

	@RequestMapping(value = "/activate", method = RequestMethod.GET)
	public String showActivationPage(@ModelAttribute final RegCodeActivationForm regCodeActivationform,
			final BindingResult bindingResult, final Model model) throws CMSItemNotFoundException
	{
		bindingResult.addError((FieldError) model.asMap().get(REG_CODE_FIELD_ERROR));
		model.addAttribute(regCodeActivationform);
		storeCmsPageInModel(model, getCmsPage(ACTIVATION_PAGE_LABEL));
		return getViewForPage(model);
	}

	@RequestMapping(value = "/activate", method = RequestMethod.POST)
	public String activateRegCode(@Valid final RegCodeActivationForm form, final BindingResult bindingResult,
			final Model model, final HttpSession session)
			throws CMSItemNotFoundException
	{
		if (bindingResult.hasErrors())
		{
			GlobalMessages.addErrorMessage(model, FORM_GLOBAL_ERROR_MESSAGE);
			storeCmsPageInModel(model, getCmsPage(ACTIVATION_PAGE_LABEL));
			return getViewForPage(model);
		} else {
			session.setAttribute(REG_CODE_ACTIVATION_FORM, form);
			return REDIRECT_PREFIX + "/checkout/regCode/order";
		}

	}

	@RequestMapping(value = "/confirmation", method = RequestMethod.GET)
	@RequireHardLogIn
	public String showConfirmationPage(final Model model) throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getCmsPage(CONFIRMATION_PAGE_LABEL));
		return getViewForPage(model);

	}

	private AbstractPageModel getCmsPage(final String label) throws CMSItemNotFoundException
	{
		return getCmsPageService().getPageForLabelOrId(label);
	}
}
