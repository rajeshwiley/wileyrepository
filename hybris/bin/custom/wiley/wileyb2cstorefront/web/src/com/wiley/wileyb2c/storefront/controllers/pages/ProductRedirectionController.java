package com.wiley.wileyb2c.storefront.controllers.pages;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.wiley.facades.product.WileyProductFacade;

import static com.wiley.storefrontcommons.constants.WileystorefrontcommonsConstants.REDIRECT_TO_404;


@Controller
@RequestMapping(value = "/remtitle")
public class ProductRedirectionController
{
	private static final Logger LOG = LoggerFactory.getLogger(ProductRedirectionController.class);

	@Resource
	private WileyProductFacade wileyProductFacade;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<Void> getRedirectForProduct(@RequestParam("isbn") final String isbn,
			final UriComponentsBuilder uriBuilder)
	{
		final String productUrl = wileyProductFacade.findProductUrlForIsbnOrCode(isbn);
		final UriComponents uriComponents = uriBuilder
				.path(productUrl)
				.build(true);

		return ResponseEntity.status(HttpStatus.MOVED_PERMANENTLY).location(uriComponents.toUri()).build();
	}

	@ExceptionHandler(Exception.class)
	public String handleAnyException(final Exception exc)
	{
		LOG.warn("No product can be found for given isbn/code: " + exc.getMessage());
		return REDIRECT_TO_404;
	}
}
