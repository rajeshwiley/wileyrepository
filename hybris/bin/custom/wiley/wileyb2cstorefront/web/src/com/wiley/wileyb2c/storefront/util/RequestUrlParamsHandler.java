package com.wiley.wileyb2c.storefront.util;

import de.hybris.platform.servicelayer.session.SessionService;

import java.util.List;
import java.util.Optional;

import javax.annotation.Nonnull;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;

import com.wiley.wileycom.storefrontcommons.util.WileySearchUrlHelperUtil;


public class RequestUrlParamsHandler
{
	private static final String QUERY_PAGE_SIZE_PARAM_NAME = "size";
	private static final String CURRENT_PAGE_SIZE = "currentPageSize";

	@Value("#{'${wileyb2cstorefront.search.itemsPerPage}'.split(',')}")
	private List<Integer> itemsPerPage;

	@Resource
	private SessionService sessionService;

	/**
	 * Validate and return Page Size param as Optional.
	 *
	 * @param request
	 * @param pageSize
	 * @return optional page size or empty
	 * @see Optional
	 */
	@Nonnull
	public Optional<Integer> getPageSize(@Nonnull final HttpServletRequest request, final int pageSize)
	{
		final boolean hasQuerySizeParam = WileySearchUrlHelperUtil.requestHasQueryParam(request, QUERY_PAGE_SIZE_PARAM_NAME);

		if (hasQuerySizeParam)
		{
			return isPageSizeAllowed(pageSize) ? setAsDefaultAndReturn(pageSize) : Optional.empty();
		}

		return Optional.of(getCurrentOrDefaultPageSize());
	}

	/**
	 * Get default Page Size from session.
	 *
	 * @return optional page size or empty
	 */
	public int getCurrentOrDefaultPageSize()
	{
		final Optional<Integer> pageSize = Optional.ofNullable(sessionService.getAttribute(CURRENT_PAGE_SIZE));

		if (pageSize.isPresent())
		{
			return pageSize.get();
		}

		final int defaultPageSize = getMinAllowedPageSize();
		sessionService.setAttribute(CURRENT_PAGE_SIZE, defaultPageSize);

		return defaultPageSize;
	}

	private Optional<Integer> setAsDefaultAndReturn(final int pageSize)
	{
		sessionService.setAttribute(CURRENT_PAGE_SIZE, pageSize);
		return Optional.of(pageSize);
	}

	public List<Integer> getAllowedItemsPerPageSize()
	{
		return itemsPerPage;
	}

	private int getMinAllowedPageSize()
	{
		return itemsPerPage.stream().mapToInt(Integer::valueOf).min().getAsInt();
	}

	private int getMaxAllowedPageSize()
	{
		return itemsPerPage.stream().mapToInt(Integer::valueOf).max().getAsInt();
	}

	private boolean isPageSizeAllowed(final int pageSize)
	{
		return pageSize >= getMinAllowedPageSize() && pageSize <= getMaxAllowedPageSize();
	}
}
