package com.wiley.wileyb2c.storefront.forms.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;

import com.wiley.wileyb2c.storefront.forms.Wileyb2cUpdateProfileForm;
import com.wiley.wileycom.storefrontcommons.validators.WileycomUpdateProfileFormValidator;
import com.wiley.wileycom.storefrontcommons.validators.FieldValidator;


/**
 * Validates profile form for b2c customer
 */
public class Wileyb2cUpdateProfileFormValidator extends WileycomUpdateProfileFormValidator
{
	public static final String STUDENT_ID_FIELD_KEY = "studentId";
	public static final String STUDENT_ID_ERROR_KEY = "profile.studentId.invalid";
	public static final String MAJOR_FIELD_KEY = "major";
	public static final String MAJOR_ERROR_KEY = "profile.major.invalid";
	public static final String GRAD_MONTH_FIELD_KEY = "graduationMonth";
	public static final String GRAD_MONTH_ERROR_KEY = "profile.graduationMonth.invalid";
	public static final String GRAD_YEAR_FIELD_KEY = "graduationYear";
	public static final String GRAD_YEAR_ERROR_KEY = "profile.graduationYear.invalid";
	public static final String SCHOOL_FIELD_KEY = "school";
	public static final String SCHOOL_ERROR_KEY = "profile.school.invalid";

	@Autowired
	private FieldValidator optionalSizeFieldValidator;

	@Autowired
	private FieldValidator notNullFieldValidator;

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return Wileyb2cUpdateProfileForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		super.validate(object, errors);
		optionalSizeFieldValidator.validate(errors, STUDENT_ID_FIELD_KEY, STUDENT_ID_ERROR_KEY);
		optionalSizeFieldValidator.validate(errors, MAJOR_FIELD_KEY, MAJOR_ERROR_KEY);
		optionalSizeFieldValidator.validate(errors, GRAD_MONTH_FIELD_KEY, GRAD_MONTH_ERROR_KEY);
		optionalSizeFieldValidator.validate(errors, GRAD_YEAR_FIELD_KEY, GRAD_YEAR_ERROR_KEY);

		final Wileyb2cUpdateProfileForm updateProfileForm = (Wileyb2cUpdateProfileForm) object;
		if (updateProfileForm.getEnrolledInSchool())
		{
			notNullFieldValidator.validate(errors, SCHOOL_FIELD_KEY, SCHOOL_ERROR_KEY);
		}
	}
}