package com.wiley.wileyb2c.storefront.util;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;

import java.util.List;

import com.wiley.facades.product.data.InventoryStatusRecord;

import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


/**
 * Created by Mikhail_Asadchy on 7/26/2016.
 */
public final class InventoryStatusUtil
{
	private InventoryStatusUtil()
	{
	}

	public static boolean addMessageIfNonInStockItem(final CartModificationData cartModification,
			final RedirectAttributes redirectModel)
	{
		final ProductData product = cartModification.getEntry().getProduct();

		return addMessageIfNonInStockItem(product, redirectModel);
	}

	public static boolean addMessageIfNonInStockItem(final ProductData product, final RedirectAttributes redirectModel)
	{
		final List<InventoryStatusRecord> inventoryStatusRecords = product.getInventoryStatus();

		if (inventoryStatusRecords.stream().anyMatch(record -> record.getStatusCode() != StockLevelStatus.INSTOCK))
		{
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.INFO_MESSAGES_HOLDER,
					"basket.page.message.update.quantityExceeded", new Object[] { product.getName() });
			return true;
		}

		return false;
	}

	public static boolean addMessageIfNonInStockItem(final CartData cartData, final Model model)
	{
		final List<OrderEntryData> entries = cartData.getEntries();

		for (final OrderEntryData entry : entries)
		{
			addMessageIfNonInStockItem(entry.getProduct(), model);
		}

		return true;
	}

	private static boolean addMessageIfNonInStockItem(final ProductData product, final Model model)
	{
		final List<InventoryStatusRecord> inventoryStatusRecords = product.getInventoryStatus();

		if (inventoryStatusRecords != null && inventoryStatusRecords.stream().anyMatch(
				record -> record.getStatusCode() != StockLevelStatus.INSTOCK))
		{
			GlobalMessages.addMessage(model, GlobalMessages.INFO_MESSAGES_HOLDER, "basket.page.message.update.quantityExceeded",
					new Object[] { product.getName() });
			return true;
		}

		return false;
	}
}
