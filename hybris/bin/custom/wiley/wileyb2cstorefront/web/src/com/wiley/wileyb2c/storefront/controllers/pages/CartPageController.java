package com.wiley.wileyb2c.storefront.controllers.pages;

import de.hybris.platform.acceleratorfacades.flow.impl.SessionOverrideCheckoutFlowFacade;
import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.acceleratorservices.enums.CheckoutFlowEnum;
import de.hybris.platform.acceleratorservices.enums.CheckoutPciOptionEnum;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.voucher.data.VoucherData;
import de.hybris.platform.commercefacades.voucher.exceptions.VoucherOperationException;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.enumeration.EnumerationService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.facades.customer.data.SchoolData;
import com.wiley.facades.voucher.WileyCouponFacade;
import com.wiley.facades.wileyb2c.customer.Wileyb2cCustomerFacade;
import com.wiley.facades.wileyb2c.order.Wileyb2cCartFacade;
import com.wiley.wileyb2c.storefront.controllers.ControllerConstants;
import com.wiley.wileyb2c.storefront.forms.Wileyb2cCartEnrolledInSchoolForm;
import com.wiley.wileyb2c.storefront.util.InventoryStatusUtil;
import com.wiley.wileycom.storefrontcommons.controllers.pages.AbstractWileycomCartPageController;
import com.wiley.wileycom.storefrontcommons.forms.WileycomUpdateQuantityForm;
import com.wiley.wileycom.storefrontcommons.forms.WileycomVoucherForm;


/**
 * Controller for cart page
 */
@Controller
@Profile(WileyCoreConstants.WILEY_PHASE2A_PROFILE_NAME)
@Scope("tenant")
@RequestMapping(value = "/cart")
public class CartPageController extends AbstractWileycomCartPageController
{
	public static final String SHOW_CHECKOUT_STRATEGY_OPTIONS = "storefront.show.checkout.flows";
	public static final String ERROR_MSG_TYPE = "errorMsg";
	public static final String SUCCESSFUL_MODIFICATION_CODE = "success";

	protected static final Logger LOG = Logger.getLogger(CartPageController.class);

	private static final String CART_URL = "/cart";

	@Resource(name = "simpleBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder resourceBreadcrumbBuilder;

	@Resource(name = "enumerationService")
	private EnumerationService enumerationService;

	@Resource(name = "productVariantFacade")
	private ProductFacade productFacade;


	@Resource
	private Wileyb2cCustomerFacade wileyb2cCustomerFacade;

	@Resource
	private Wileyb2cCartFacade wileyb2cCartFacade;

	@Resource(name = "wileyb2cCouponFacade")
	private WileyCouponFacade wileyCouponFacade;

	@ModelAttribute("showCheckoutStrategies")
	public boolean isCheckoutStrategyVisible()
	{
		return getSiteConfigService().getBoolean(SHOW_CHECKOUT_STRATEGY_OPTIONS, false);
	}

	/*
	 * Display the cart page
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String showCart(final Model model) throws CMSItemNotFoundException, CommerceCartModificationException
	{
		prepareDataForPage(model);
		return ControllerConstants.Views.Pages.Cart.CART_PAGE;
	}

	@RequestMapping(method = RequestMethod.POST)
	public String doShowCart(final Model model, final RedirectAttributes redirectAttributes,
			@ModelAttribute("enrolledInSchoolForm") final Wileyb2cCartEnrolledInSchoolForm enrolledInSchoolForm)
			throws CommerceCartModificationException
	{
		if (enrolledInSchoolForm.isEnrolledInSchool())
		{
			wileyb2cCartFacade.persistSchoolInformationInCurrentOrder(enrolledInSchoolForm.getSchool());
		}

		return cartCheck(model, redirectAttributes);
	}

	/**
	 * Handle the '/cart/checkout' request url. This method checks to see if the cart is valid before allowing the
	 * checkout to begin. Note that this method does not require the user to be authenticated and therefore allows us to
	 * validate that the cart is valid without first forcing the user to login. The cart will be checked again once the
	 * user has logged in.
	 *
	 * @return The page to redirect to
	 */
	@RequestMapping(value = "/checkout", method = RequestMethod.GET)
	@RequireHardLogIn
	public String cartCheck(final Model model, final RedirectAttributes redirectModel) throws CommerceCartModificationException
	{
		SessionOverrideCheckoutFlowFacade.resetSessionOverrides();

		if (!getCartFacade().hasEntries())
		{
			LOG.info("Missing or empty cart");

			// No session cart or empty session cart. Bounce back to the cart page.
			return REDIRECT_PREFIX + CART_URL;
		}


		if (validateCart(redirectModel))
		{
			return REDIRECT_PREFIX + CART_URL;
		}

		// Redirect to the start of the checkout flow to begin the checkout process
		// We just redirect to the generic '/checkout' page which will actually select the checkout flow
		// to use. The customer is not necessarily logged in on this request, but will be forced to login
		// when they arrive on the '/checkout' page.
		return REDIRECT_PREFIX + "/checkout";
	}

	@RequestMapping(value = "/getProductVariantMatrix", method = RequestMethod.GET)
	@RequireHardLogIn
	public String getProductVariantMatrix(@RequestParam("productCode") final String productCode, final Model model)
	{

		final ProductData productData = productFacade.getProductForCodeAndOptions(productCode, Arrays.asList(ProductOption.BASIC,
				ProductOption.CATEGORIES, ProductOption.VARIANT_MATRIX_BASE, ProductOption.VARIANT_MATRIX_PRICE,
				ProductOption.VARIANT_MATRIX_MEDIA, ProductOption.VARIANT_MATRIX_STOCK, ProductOption.VARIANT_MATRIX_URL));

		model.addAttribute("product", productData);

		return ControllerConstants.Views.Fragments.Cart.EXPAND_GRID_IN_CART;
	}

	// This controller method is used to allow the site to force the visitor through a specified checkout flow.
	// If you only have a static configured checkout flow then you can remove this method.
	@RequestMapping(value = "/checkout/select-flow", method = RequestMethod.GET)
	@RequireHardLogIn
	public String initCheck(final Model model, final RedirectAttributes redirectModel,
			@RequestParam(value = "flow", required = false) final String flow,
			@RequestParam(value = "pci", required = false) final String pci) throws CommerceCartModificationException
	{
		SessionOverrideCheckoutFlowFacade.resetSessionOverrides();

		if (!getCartFacade().hasEntries())
		{
			LOG.info("Missing or empty cart");

			// No session cart or empty session cart. Bounce back to the cart page.
			return REDIRECT_PREFIX + CART_URL;
		}

		// Override the Checkout Flow setting in the session
		if (StringUtils.isNotBlank(flow))
		{
			final CheckoutFlowEnum checkoutFlow = enumerationService.getEnumerationValue(CheckoutFlowEnum.class,
					StringUtils.upperCase(flow));
			SessionOverrideCheckoutFlowFacade.setSessionOverrideCheckoutFlow(checkoutFlow);
		}

		// Override the Checkout PCI setting in the session
		if (StringUtils.isNotBlank(pci))
		{
			final CheckoutPciOptionEnum checkoutPci = enumerationService.getEnumerationValue(CheckoutPciOptionEnum.class,
					StringUtils.upperCase(pci));
			SessionOverrideCheckoutFlowFacade.setSessionOverrideSubscriptionPciOption(checkoutPci);
		}

		// Redirect to the start of the checkout flow to begin the checkout process
		// We just redirect to the generic '/checkout' page which will actually select the checkout flow
		// to use. The customer is not necessarily logged in on this request, but will be forced to login
		// when they arrive on the '/checkout' page.
		return REDIRECT_PREFIX + "/checkout";
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String updateCartQuantities(@RequestParam("entryNumber") final long entryNumber, final Model model,
			@Valid final WileycomUpdateQuantityForm form, final BindingResult bindingResult, final HttpServletRequest request,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		if (bindingResult.hasErrors())
		{
			for (final ObjectError error : bindingResult.getAllErrors())
			{
				if (error.getCode().equals("typeMismatch"))
				{
					GlobalMessages.addErrorMessage(model, "basket.error.quantity.invalid");
				}
				else
				{
					GlobalMessages.addErrorMessage(model, error.getDefaultMessage());
				}
			}
		}
		else if (getCartFacade().hasEntries())
		{
			try
			{
				final CartModificationData cartModification = getCartFacade().updateCartEntry(entryNumber,
						form.getQuantity().longValue());

				if (cartModification.getQuantity() == form.getQuantity().longValue())
				{
					// Success
					if (cartModification.getQuantity() == 0)
					{
						// Success in removing entry
						GlobalMessages.addFlashMessage(redirectModel,
								GlobalMessages.CONF_MESSAGES_HOLDER, "basket.page.message.remove");
					}
					else
					{
						InventoryStatusUtil.addMessageIfNonInStockItem(cartModification, redirectModel);

						// Success in update quantity
						GlobalMessages.addFlashMessage(redirectModel,
								GlobalMessages.CONF_MESSAGES_HOLDER, "basket.page.message.update");
					}
				}
				else if (cartModification.getQuantity() > 0)
				{
					// Less than successful
					GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
							"basket.page.message.update.reducedNumberOfItemsAdded.lowStock",
							new Object[] { cartModification.getEntry().getProduct().getName(),
									cartModification.getQuantity(), form.getQuantity(),
									request.getRequestURL().append(cartModification.getEntry().getProduct().getUrl()) });
				}
				else
				{
					// No more stock available
					GlobalMessages.addFlashMessage(
							redirectModel,
							GlobalMessages.ERROR_MESSAGES_HOLDER,
							"basket.page.message.update.reducedNumberOfItemsAdded.noStock",
							new Object[]
									{ cartModification.getEntry().getProduct().getName(),
											request.getRequestURL().append(cartModification.getEntry().getProduct().getUrl()) });
				}

				// Redirect to the cart page on update success so that the browser doesn't re-post again
				return REDIRECT_PREFIX + CART_URL;
			}
			catch (final CommerceCartModificationException ex)
			{
				LOG.warn("Couldn't update product with the entry number: " + entryNumber + ".", ex);
			}
		}
		prepareDataForPage(model);
		return ControllerConstants.Views.Pages.Cart.CART_PAGE;
	}

	@SuppressWarnings("boxing")
	@ResponseBody
	@RequestMapping(value = "/updateMultiD", method = RequestMethod.POST)
	public CartData updateCartQuantitiesMultiD(@RequestParam("entryNumber") final Integer entryNumber,
			@RequestParam("productCode") final String productCode, final Model model,
			@Valid final WileycomUpdateQuantityForm form, final BindingResult bindingResult)
	{
		if (bindingResult.hasErrors())
		{
			for (final ObjectError error : bindingResult.getAllErrors())
			{
				if (error.getCode().equals("typeMismatch"))
				{
					GlobalMessages.addErrorMessage(model, "basket.error.quantity.invalid");
				}
				else
				{
					GlobalMessages.addErrorMessage(model, error.getDefaultMessage());
				}
			}
		}
		else
		{
			try
			{
				final CartModificationData cartModification = getCartFacade().updateCartEntry(
						getOrderEntryData(form.getQuantity(), productCode, entryNumber));
				if (cartModification.getStatusCode().equals(SUCCESSFUL_MODIFICATION_CODE))
				{
					GlobalMessages.addMessage(
							model, GlobalMessages.CONF_MESSAGES_HOLDER, cartModification.getStatusMessage(), null);
				}
				else if (!model.containsAttribute(ERROR_MSG_TYPE))
				{
					GlobalMessages.addMessage(
							model, GlobalMessages.ERROR_MESSAGES_HOLDER, cartModification.getStatusMessage(), null);
				}
			}
			catch (final CommerceCartModificationException ex)
			{
				LOG.warn("Couldn't update product with the entry number: " + entryNumber + ".", ex);
			}

		}
		return getCartFacade().getSessionCart();
	}


	@RequestMapping(value = "/redeemVoucher", method = RequestMethod.POST)
	public String redeemVoucherForCart(@Valid @ModelAttribute("voucherForm") final WileycomVoucherForm voucherForm,
			final BindingResult bindingResult, final Model model, final RedirectAttributes redirectAttributes)
			throws CMSItemNotFoundException, CommerceCartModificationException, VoucherOperationException
	{
		if (bindingResult.hasErrors())
		{
			//Code below is commented in scope of migration to new coupons: ECSC-21150
			//List<String> violationMessages = wileyVoucherFacade.getRestrictionViolationMessagesForCurrentCart("");
			List<String> violationMessages = new ArrayList<>();
			addEditedVoucherDataToPage(redirectAttributes, true, violationMessages.isEmpty() ? null : violationMessages);
			return REDIRECT_PREFIX + CART_URL;
		}

		final String discountCode = voucherForm.getDiscountCode().trim();

		if (getCartFacade().hasEntries())
		{
			if (!wileyCouponFacade.checkVoucherCode(discountCode))
			{
				//Code below is commented in scope of migration to new coupons: ECSC-21150
				//List<String> violationMessages = wileyVoucherFacade.getRestrictionViolationMessagesForCurrentCart(discountCode);
				List<String> violationMessages = new ArrayList<>();
				addEditedVoucherDataToPage(redirectAttributes, true, violationMessages.isEmpty() ? null : violationMessages);
			}
			else
			{
				wileyCouponFacade.removeVouchersFromCart();
				wileyCouponFacade.applyVoucher(discountCode);

				if (LOG.isDebugEnabled())
				{
					LOG.info("Apply voucher [" + discountCode + "]");
				}

				addEditedVoucherDataToPage(redirectAttributes, false, null);
			}
		}

		return REDIRECT_PREFIX + CART_URL;
	}

	@RequestMapping(value = "/releaseVoucher", method = RequestMethod.POST)
	public String releaseVoucherForCart(final Model model) throws CommerceCartModificationException, CMSItemNotFoundException
	{
		wileyCouponFacade.removeVouchersFromCart();

		return REDIRECT_PREFIX + CART_URL;
	}

	@SuppressWarnings("boxing")
	protected OrderEntryData getOrderEntryData(final long quantity, final String productCode, final Integer entryNumber)
	{
		final OrderEntryData orderEntry = new OrderEntryData();
		orderEntry.setQuantity(quantity);
		orderEntry.setProduct(new ProductData());
		orderEntry.getProduct().setCode(productCode);
		orderEntry.setEntryNumber(entryNumber);
		return orderEntry;
	}

	@Override
	protected CartFacade getCartFacade()
	{
		return wileyb2cCartFacade;
	}

	@Override
	protected void prepareDataForPage(final Model model) throws CMSItemNotFoundException
	{
		super.prepareDataForPage(model);
		addSchoolDataToPage(model);
		addVoucherDataToPage(model);
		model.addAttribute(WebConstants.BREADCRUMBS_KEY, resourceBreadcrumbBuilder.getBreadcrumbs("breadcrumb.cart"));
		model.addAttribute("pageType", PageType.CART.name());
		model.addAttribute("isEducationalCart", wileyb2cCartFacade.isEducationalCart());
		model.addAttribute("voucherForm", new WileycomVoucherForm());
		showEntryRemovedGlobalMessage(model);
	}

	private void addSchoolDataToPage(final Model model)
	{
		String currentCustomerUid = wileyb2cCustomerFacade.getCurrentCustomer().getUid();

		SchoolData profileSchool = wileyb2cCustomerFacade.getSchoolForCustomerFromProfile(currentCustomerUid);
		SchoolData lastOrderSchool = wileyb2cCustomerFacade.getSchoolForCustomerFromLastOrder(currentCustomerUid);

		Wileyb2cCartEnrolledInSchoolForm schoolForm = new Wileyb2cCartEnrolledInSchoolForm();

		if (profileSchool != null)
		{
			schoolForm.setEnrolledInSchool(true);
			schoolForm.setSchool(profileSchool.getCode());
		}
		else if (lastOrderSchool != null)
		{
			schoolForm.setSchool(lastOrderSchool.getCode());
		}

		model.addAttribute("displayedSchools", wileyb2cCustomerFacade.getDisplayedSchoolsForCustomer(currentCustomerUid));
		model.addAttribute("enrolledInSchoolForm", schoolForm);
	}

	private void addVoucherDataToPage(final Model model)
	{
		if (getCartFacade().hasSessionCart())
		{
			final VoucherData voucher = wileyCouponFacade.getVoucherForCart();

			if (voucher != null)
			{
				model.addAttribute("voucherRedeemed", true);
				model.addAttribute("voucherCode", voucher.getVoucherCode());
				//Code below is commented in scope of migration to new coupons: ECSC-21150
				//model.addAttribute("voucherMessage", wileyVoucherFacade.formatVoucherDescription(voucher));
			}
		}
	}

	private void addEditedVoucherDataToPage(final RedirectAttributes redirectAttributes,
			final boolean voucherDenied, final List<String> voucherViolationMessages)
	{
		redirectAttributes.addFlashAttribute("voucherDenied", voucherDenied);
		redirectAttributes.addFlashAttribute("voucherInputClass", voucherDenied ? "denied" : "accepted");
		redirectAttributes.addFlashAttribute("voucherViolationMessages", voucherViolationMessages);
	}

}
