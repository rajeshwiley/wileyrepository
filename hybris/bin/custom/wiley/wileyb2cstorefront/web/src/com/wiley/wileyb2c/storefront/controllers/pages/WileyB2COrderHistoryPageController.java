package com.wiley.wileyb2c.storefront.controllers.pages;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.wileycom.storefrontcommons.controllers.pages.AbstractWileycomOrderHistoryPageController;
import de.hybris.platform.commercefacades.order.OrderFacade;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;

import javax.annotation.Resource;

@Controller
@Profile(WileyCoreConstants.WILEY_PHASE2A_PROFILE_NAME)
public class WileyB2COrderHistoryPageController extends AbstractWileycomOrderHistoryPageController
{
	@Resource
	private OrderFacade wileyb2cOrderFacade;
	@Override
	protected OrderFacade getOrderFacade()
	{
		return wileyb2cOrderFacade;
	}
}
