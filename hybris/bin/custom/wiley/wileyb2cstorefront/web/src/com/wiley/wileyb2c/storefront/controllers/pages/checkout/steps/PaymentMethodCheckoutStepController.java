/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wileyb2c.storefront.controllers.pages.checkout.steps;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.facades.payment.WileyPaymentFacade;
import com.wiley.facades.wileyb2c.order.Wileyb2cCheckoutFacade;
import com.wiley.wileycom.storefrontcommons.controllers.ControllerConstants;
import com.wiley.wileycom.storefrontcommons.controllers.pages.checkout.steps.WileycomPaymentMethodCheckoutStepController;
import com.wiley.wileycom.storefrontcommons.forms.WileycomBillingAddressForm;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.data.CartData;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;

import static de.hybris.platform.util.localization.Localization.getLocalizedString;


@Controller
@Profile(WileyCoreConstants.WILEY_PHASE2A_PROFILE_NAME)
public class PaymentMethodCheckoutStepController extends WileycomPaymentMethodCheckoutStepController
{
	private static final String PAYMENT_METHOD = "payment-method";

	@Resource(name = "wileyb2cCheckoutFacade")
	private Wileyb2cCheckoutFacade wileyb2cCheckoutFacade;

	@Resource
	private WileyPaymentFacade wileyb2cPaymentFacade;

	@Override
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@RequireHardLogIn
	@PreValidateCheckoutStep(checkoutStep = PAYMENT_METHOD)
	public String enterStep(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		setupAddPaymentPage(model);

		model.addAttribute("paymentInfos", getUserFacade().getCCPaymentInfos(true));

		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		wileyb2cCheckoutFacade.doRealTimeInventoryCheck(cartData);
		model.addAttribute("cartData", cartData);

		model.addAttribute("paypalCheckoutLabel", getLocalizedString("text.checkout.payWith"));
		model.addAttribute("payWithPayPalFlag", true);

		storeBillingInfoInModel(model, isBillingSameAsShipping(model, true),
				getBillingAddressForm(model, new WileycomBillingAddressForm()));
		return ControllerConstants.Views.Pages.MultiStepCheckout.PAYMENT_OPTIONS_PAGE;
	}

	@Override
	protected WileyPaymentFacade getWileyPaymentFacade()
	{
		return wileyb2cPaymentFacade;
	}

}
