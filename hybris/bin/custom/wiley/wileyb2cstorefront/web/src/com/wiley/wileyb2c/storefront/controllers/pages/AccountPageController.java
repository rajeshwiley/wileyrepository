/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wileyb2c.storefront.controllers.pages;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.exceptions.ExternalSystemException;
import com.wiley.facades.customer.data.SchoolData;
import com.wiley.facades.wileyb2c.customer.Wileyb2cCustomerFacade;
import com.wiley.wileyb2c.storefront.forms.Wileyb2cUpdateProfileForm;
import com.wiley.wileyb2c.storefront.forms.validation.Wileyb2cUpdateProfileFormValidator;
import com.wiley.wileycom.storefrontcommons.controllers.pages.AbstractWileycomAccountPageController;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractCheckoutController.SelectOption;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import java.util.List;

import static com.wiley.wileycom.storefrontcommons.controllers.ControllerConstants.EXTERNAL_SERVICE_ERROR_KEY;
import static com.wiley.wileycom.storefrontcommons.controllers.ControllerConstants.FORM_GLOBAL_ERROR_KEY;


/**
 * Controller for home page
 */
@Controller
@Profile(WileyCoreConstants.WILEY_PHASE2A_PROFILE_NAME)
@Scope("tenant")
@RequestMapping("/my-account")
public class AccountPageController extends AbstractWileycomAccountPageController
{
	private static final Logger LOG = LoggerFactory.getLogger(AccountPageController.class);

	@Resource
	private Wileyb2cUpdateProfileFormValidator wileyb2cUpdateProfileFormValidator;

	@Resource
	private Wileyb2cAccountUpdateControllerHelper wileyb2cAccountUpdateControllerHelper;

	@Resource
	private Wileyb2cCustomerFacade wileyb2cCustomerFacade;

	@RequestMapping(value = "/update-profile", method = RequestMethod.GET)
	@RequireHardLogIn
	public String editProfile(final Model model) throws CMSItemNotFoundException
	{
		final CustomerData customerData = getCustomerFacade().getCurrentCustomer();

		model.addAttribute("titleData", getUserFacade().getTitles());
		model.addAttribute("suffixData", getCustomerFacade().getNameSuffixes());
		model.addAttribute("schoolData", getSchoolData(customerData.getUid()));

		final Wileyb2cUpdateProfileForm updateProfileForm = new Wileyb2cUpdateProfileForm();
		updateProfileForm.setTitleCode(customerData.getTitleCode());
		updateProfileForm.setFirstName(customerData.getFirstName());
		updateProfileForm.setMiddleName(customerData.getMiddleName());
		updateProfileForm.setLastName(customerData.getLastName());
		updateProfileForm.setSuffixCode(customerData.getSuffixCode());
		updateProfileForm.setStudentId(customerData.getStudentId());
		updateProfileForm.setMajor(customerData.getMajor());
		updateProfileForm.setGraduationMonth(customerData.getGraduationMonth());
		updateProfileForm.setGraduationYear(customerData.getGraduationYear());
		if (customerData.getSchool() != null)
		{
			updateProfileForm.setSchool(customerData.getSchool().getCode());
			updateProfileForm.setEnrolledInSchool(true);
		}

		model.addAttribute("updateProfileForm", updateProfileForm);

		storeCmsPageInModel(model, getContentPageForLabelOrId(UPDATE_PROFILE_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(UPDATE_PROFILE_CMS_PAGE));

		model.addAttribute("breadcrumbs", getAccountBreadcrumbBuilder().getBreadcrumbs("text.account.profile"));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}

	@RequestMapping(value = "/update-profile", method = RequestMethod.POST)
	@RequireHardLogIn
	public String updateProfile(@ModelAttribute("updateProfileForm") final Wileyb2cUpdateProfileForm updateProfileForm,
			final BindingResult bindingResult, final Model model, final RedirectAttributes redirectAttributes)
			throws CMSItemNotFoundException
	{
		getWileyb2cUpdateProfileFormValidator().validate(updateProfileForm, bindingResult);

		String returnAction = REDIRECT_TO_UPDATE_PROFILE;
		final CustomerData currentCustomerData = getCustomerFacade().getCurrentCustomer();
		final CustomerData customerData = new CustomerData();
		customerData.setTitleCode(updateProfileForm.getTitleCode());
		customerData.setFirstName(updateProfileForm.getFirstName());
		customerData.setMiddleName(updateProfileForm.getMiddleName());
		customerData.setLastName(updateProfileForm.getLastName());
		customerData.setSuffixCode(updateProfileForm.getSuffixCode());
		customerData.setUid(currentCustomerData.getUid());
		customerData.setDisplayUid(currentCustomerData.getDisplayUid());
		customerData.setStudentId(updateProfileForm.getStudentId());
		customerData.setMajor(updateProfileForm.getMajor());
		customerData.setGraduationMonth(updateProfileForm.getGraduationMonth());
		customerData.setGraduationYear(updateProfileForm.getGraduationYear());
		if (updateProfileForm.getEnrolledInSchool())
		{
			SchoolData school = new SchoolData();
			school.setCode(updateProfileForm.getSchool());
			customerData.setSchool(school);
		}

		model.addAttribute("titleData", getUserFacade().getTitles());
		model.addAttribute("suffixData", getCustomerFacade().getNameSuffixes());
		model.addAttribute("schoolData", getSchoolData(customerData.getUid()));

		storeCmsPageInModel(model, getContentPageForLabelOrId(UPDATE_PROFILE_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(UPDATE_PROFILE_CMS_PAGE));

		if (bindingResult.hasErrors())
		{
			returnAction = setErrorMessagesAndCMSPage(model, UPDATE_PROFILE_CMS_PAGE, FORM_GLOBAL_ERROR_KEY);
		}
		else
		{
			try
			{
				getCustomerFacade().updateProfile(customerData);
				GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER,
						"text.account.profile.confirmationUpdated", null);

			}
			catch (ExternalSystemException e)
			{
				LOG.debug("can not update user '" + customerData.getUid() + "' due to, " + e.getMessage(), e);
				returnAction = setErrorMessagesAndCMSPage(model, UPDATE_PROFILE_CMS_PAGE, EXTERNAL_SERVICE_ERROR_KEY);
			}
			catch (final DuplicateUidException e)
			{
				bindingResult.rejectValue("email", "registration.error.account.exists.title");
				returnAction = setErrorMessagesAndCMSPage(model, UPDATE_PROFILE_CMS_PAGE, FORM_GLOBAL_ERROR_KEY);
			}
		}


		model.addAttribute("breadcrumbs", getAccountBreadcrumbBuilder().getBreadcrumbs("text.account.profile"));
		return returnAction;
	}
	
	
	@ModelAttribute("months")
	public List<SelectOption> getMonths()
	{
		return wileyb2cAccountUpdateControllerHelper.getMonths();
	}

	@ModelAttribute("years")
	public List<SelectOption> getYears()
	{
		return wileyb2cAccountUpdateControllerHelper.getYears();
	}

	private List<SchoolData> getSchoolData(final String customerUid)
	{
		return wileyb2cCustomerFacade.getDisplayedSchoolsForCustomer(customerUid);
	}

	public Wileyb2cUpdateProfileFormValidator getWileyb2cUpdateProfileFormValidator()
	{
		return wileyb2cUpdateProfileFormValidator;
	}
}
