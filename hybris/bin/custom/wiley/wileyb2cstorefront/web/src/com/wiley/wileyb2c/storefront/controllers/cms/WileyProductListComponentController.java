package com.wiley.wileyb2c.storefront.controllers.cms;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.commerceservices.url.UrlResolver;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.catalina.util.URLEncoder;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;

import com.wiley.core.model.components.WileyProductListComponentModel;
import com.wiley.facades.cms.WileyProductListComponentFacade;
import com.wiley.wileyb2c.storefront.controllers.ControllerConstants;


/**
 * @author Dzmitryi_Halahayeu
 */
@Controller("WileyProductListComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.WILEY_PRODUCT_LIST_COMPONENT)
public class WileyProductListComponentController extends
		AbstractCMSComponentController<WileyProductListComponentModel>
{

	@Resource(name = "wileyProductListComponentFacade")
	private WileyProductListComponentFacade wileyProductListComponentFacade;

	@Resource(name = "wileyb2cCustomPlpLinkUrlResolver")
	private UrlResolver<CategoryModel> wileyb2cCustomPlpLinkUrlResolver;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final WileyProductListComponentModel component)
	{
		Assert.notNull(component.getTotalProducts());

		final CategoryModel category = getRequestContextData(request).getCategory();
		final List<ProductData> products = wileyProductListComponentFacade.getComponentProducts(category, component);

		model.addAttribute("headline", component.getHeadline());
		if (component.isShowViewAllLink() && category != null)
		{
			model.addAttribute(ControllerConstants.Model.Attributes.CUSTOM_LINK,
					createCategoryUrl(category, component.getSearchQuery()));
		}

		Boolean showAddToCartModalWindow = false;

		if (products != null)
		{
			showAddToCartModalWindow = products.stream()
					.flatMap(p -> Optional.ofNullable(p.getVariantOptions()).map(List::stream).orElseGet(Stream::empty))
					.map(VariantOptionData::getAvailable)
					.filter(Objects::nonNull)
					.findFirst()
					.orElse(false);
		}

		model.addAttribute("products", products);
		model.addAttribute("showAddToCartModalWindow", showAddToCartModalWindow);
		model.addAttribute("cssClasses", component.getCssClasses().stream().toArray(String[]::new));
		model.addAttribute("actions", component.getActions());
	}

	private String createCategoryUrl(final CategoryModel category, final String searchQuery)
	{
		final StringBuilder categoryUrlBuilder = new StringBuilder(wileyb2cCustomPlpLinkUrlResolver.resolve(category));
		if (StringUtils.isNotEmpty(searchQuery))
		{
			categoryUrlBuilder.append("?pq=").append(new URLEncoder().encode(searchQuery));
		}
		return categoryUrlBuilder.toString();
	}
}
