package com.wiley.wileyb2c.storefront.controllers.cms;

import de.hybris.platform.servicelayer.config.ConfigurationService;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.wiley.core.model.Wileyb2cHomeLinkComponentModel;
import com.wiley.facades.wileyb2c.order.Wileyb2cCartFacade;
import com.wiley.wileyb2c.storefront.controllers.ControllerConstants;


/**
 * @author Dzmitryi_Halahayeu
 */
@Controller("Wileyb2cContinueShoppingLinkComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.WILEY_B2C_CONTINUE_SHOPPING_LINK_COMPONENT)
public class Wileyb2cContinueShoppingLinkComponentController extends
		AbstractWileyb2cLinkWithDefaultValueComponentController<Wileyb2cHomeLinkComponentModel>
{
	@Resource
	private Wileyb2cCartFacade wileyb2cCartFacade;
	@Resource
	private ConfigurationService configurationService;

	@Override
	protected String getUrlText()
	{
		return configurationService.getConfiguration().getString("cart.page.continue");
	}

	@Override
	protected String getUrlValue()
	{
		return wileyb2cCartFacade.getContinueUrl();
	}


}
