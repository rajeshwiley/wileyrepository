/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wileyb2c.storefront.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.security.web.PortResolverImpl;
import org.springframework.security.web.savedrequest.DefaultSavedRequest;
import org.springframework.security.web.savedrequest.SavedRequest;

/**
 * Success handler for PayPal to set the target url.
 */
public class PaypalStorefrontAuthenticationSuccessHandler extends StorefrontAuthenticationSuccessHandler
{
	private static final Logger LOG = Logger.getLogger(PaypalStorefrontAuthenticationSuccessHandler.class);

	@Override
	protected String determineTargetUrl(final HttpServletRequest request, final HttpServletResponse response)
	{
		super.setDefaultTargetUrl(getRedirectUrl(request));
		return super.determineTargetUrl(request, response);
	}

	private String getRedirectUrl(final HttpServletRequest request)
	{
		HttpSession session = request.getSession(false);
		if (session != null)
		{
			SavedRequest savedRequest = new DefaultSavedRequest(request, new PortResolverImpl());
			if (savedRequest != null)
			{
				return savedRequest.getRedirectUrl();
			}
		}

		return null;
	}
}
