package com.wiley.wileyb2c.storefront.controllers.pages;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.facades.payment.WileyPaymentFacade;
import com.wiley.wileycom.storefrontcommons.controllers.AbstractUpdateCardDetailsResponseController;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;

import javax.annotation.Resource;

@Controller
@Profile(WileyCoreConstants.WILEY_PHASE2A_PROFILE_NAME)
public class B2CUpdateCardDetailsResponseController extends AbstractUpdateCardDetailsResponseController
{
	@Resource
	private WileyPaymentFacade wileyb2cPaymentFacade;

	@Override
	protected WileyPaymentFacade getPaymentFacade()
	{
		return wileyb2cPaymentFacade;
	}
}
