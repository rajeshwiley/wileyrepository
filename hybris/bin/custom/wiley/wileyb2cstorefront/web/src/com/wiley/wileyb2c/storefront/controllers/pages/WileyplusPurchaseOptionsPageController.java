package com.wiley.wileyb2c.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;

import java.net.URISyntaxException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.regCode.exception.RegCodeInvalidException;
import com.wiley.core.regCode.exception.RegCodeValidationException;
import com.wiley.facades.product.WileyProductFacade;
import com.wiley.facades.wileyb2c.order.WileyRegCodeCheckoutFacade;
import com.wiley.facades.wileyb2c.product.WileyCourseProductFacade;
import com.wiley.facades.wileyb2c.product.data.WileyPlusCourseInfoData;
import com.wiley.facades.wileyb2c.wileyplus.url.strategy.WileyplusPurchaseOptionsPageParseURLStrategy;
import com.wiley.facades.wileyb2c.wileyplus.url.strategy.exception.WileyplusChecksumMismatchException;
import com.wiley.facades.wileyplus.cart.activation.dto.CartActivationRequestDto;
import com.wiley.facades.wileyplus.cart.activation.dto.WileyPlusCourseDto;
import com.wiley.wileyb2c.storefront.forms.RegCodeActivationForm;


/**
 * Returns purchase options page. Information about course and options are retrieved from session
 */
@Controller
@Profile(WileyCoreConstants.WILEY_PHASE2A_PROFILE_NAME)
@SessionAttributes("regCodeActivationForm")
public class WileyplusPurchaseOptionsPageController extends AbstractPageController
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyplusPurchaseOptionsPageController.class);

	private static final String ATTR_CART_ACTIVATION_INFO = "cartActivationInfo";
	private static final String ATTR_CHECKSUM = "checksum";

	private static final String COURSE_PURCHASE_OPTIONS_PAGE = "coursePurchaseOptions";

	private static final String ERROR_MESSAGE_INVALID_REG_CODE = "regCode.errorMessage.invalid";
	private static final String REG_CODE_FIELD_ERROR = "regCodeFieldError";
	private static final String PURCHASE_OPTION_TITLE_KEY =
			"wileypluscourse.purchase.option.instant-access.item-description.line1";
	private static final String EXTERNAL_SERVICE_RESPONSE_ERROR = "externalService.response.error";
	private static final String FORM_GLOBAL_ERROR = "form.global.error";

	private static final String ORDER_CONFIRM_PAGE = REDIRECT_PREFIX + "/checkout/orderConfirmation/";
	private static final String REGCODE_ORDER_REDIRECT = REDIRECT_PREFIX + "/checkout/regcode/course/order";
	private static final String PURCHASE_OPTIONS_REDIRECT = REDIRECT_PREFIX + "/ucart/wileyPlus/purchase/options";
	private static final String REG_CODE_ACTIVATION_FORM = "regCodeActivationForm";

	@Resource
	private WileyplusPurchaseOptionsPageParseURLStrategy wileyplusPurchaseOptionsPageURLParser;

	@Resource
	private WileyCourseProductFacade wileyb2cWileyCourseProductFacade;

	@Resource
	private WileyProductFacade wileyProductFacade;

	@Resource(name = "wileyRegCodeCheckoutFacade")
	private WileyRegCodeCheckoutFacade checkoutFacade;

	@ModelAttribute(REG_CODE_ACTIVATION_FORM)
	public RegCodeActivationForm initializeForm()
	{
		return new RegCodeActivationForm();
	}

	@RequestMapping(value = "/ucart/wileyPlus/purchase/options", method = RequestMethod.GET)
	public String purchaseOptions(@ModelAttribute final RegCodeActivationForm regCodeActivationForm,
			final BindingResult bindingResult, @ModelAttribute(ATTR_CART_ACTIVATION_INFO) final String cartActivationInfo,
			@ModelAttribute(ATTR_CHECKSUM) final String checksum, final Model model, final HttpServletRequest request,
			final HttpServletResponse response)
			throws CMSItemNotFoundException, URISyntaxException, WileyplusChecksumMismatchException
	{
		bindingResult.addError((FieldError) model.asMap().get(REG_CODE_FIELD_ERROR));
		model.addAttribute(regCodeActivationForm);
		CartActivationRequestDto cartActivationRequestDto =
				wileyplusPurchaseOptionsPageURLParser.parseCartActivationParams(cartActivationInfo, checksum);

		return renderPurchaseOptionsPage(cartActivationRequestDto, cartActivationInfo, checksum, model);
	}

	private String renderPurchaseOptionsPage(final CartActivationRequestDto cartActivationRequestDto,
			final String cartActivationInfo, final String checksum, final Model model)
			throws URISyntaxException, CMSItemNotFoundException
	{
		final WileyPlusCourseDto course = cartActivationRequestDto.getCourse();
		if (course == null || !wileyProductFacade.doesProductExistForIsbn(course.getIsbn()))
		{
			throw new IllegalStateException(String.format("Could not find WileyPlus course for isbn [%s]",
					course != null ? course.getIsbn() : null));
		}

		final WileyPlusCourseInfoData wileyPlusCourseInfoData = wileyb2cWileyCourseProductFacade.
				convertToWileyInfoData(cartActivationRequestDto);
		if (!model.containsAttribute(REG_CODE_ACTIVATION_FORM))
		{
			RegCodeActivationForm regCodeActivationform = new RegCodeActivationForm();
			model.addAttribute(regCodeActivationform);
		}
		model.addAttribute("purchaseOptions", wileyPlusCourseInfoData.getPurchaseOptions());
		model.addAttribute("course", wileyPlusCourseInfoData.getCourse());
		storeCmsPageInModel(model, getContentPageForLabelOrId(COURSE_PURCHASE_OPTIONS_PAGE));
		model.addAttribute(ATTR_CHECKSUM, checksum);
		model.addAttribute(ATTR_CART_ACTIVATION_INFO, cartActivationInfo);
		return getViewForPage(model);
	}

	@RequestMapping(value = "/ucart/wileyPlus/purchase/options", method = RequestMethod.POST)
	public String activateRegcode(@Valid final RegCodeActivationForm form, final BindingResult bindingResult,
			@RequestParam final String cartActivationInfo,
			@RequestParam final String checksum, final Model model, final HttpSession session,
			final RedirectAttributes redirectAttributes)
			throws WileyplusChecksumMismatchException, CMSItemNotFoundException, URISyntaxException
	{
		if (bindingResult.hasErrors())
		{
			model.addAttribute("regCodeAccordion", true);
			GlobalMessages.addErrorMessage(model, FORM_GLOBAL_ERROR);
			CartActivationRequestDto cartActivationRequestDto =
					wileyplusPurchaseOptionsPageURLParser.parseCartActivationParams(cartActivationInfo, checksum);
			return renderPurchaseOptionsPage(cartActivationRequestDto, cartActivationInfo, checksum, model);
		}
		else
		{
			session.setAttribute(REG_CODE_ACTIVATION_FORM, form);
			redirectAttributes.addAttribute(ATTR_CART_ACTIVATION_INFO, cartActivationInfo);
			redirectAttributes.addAttribute(ATTR_CHECKSUM, checksum);
			return REGCODE_ORDER_REDIRECT;
		}
	}

	@RequestMapping(value = "/checkout/regcode/course/order")
	@RequireHardLogIn
	public String activateCourseRegCode(
			@Valid @ModelAttribute(REG_CODE_ACTIVATION_FORM) final RegCodeActivationForm regCodeActivationForm,
			final BindingResult bindingResult,
			@ModelAttribute(ATTR_CART_ACTIVATION_INFO) final String cartActivationInfo,
			@ModelAttribute(ATTR_CHECKSUM) final String checksum, final Model model, final SessionStatus sessionStatus,
			final RedirectAttributes redirectAttributes)
			throws URISyntaxException, WileyplusChecksumMismatchException
	{
		sessionStatus.setComplete();
		if (bindingResult.hasErrors())
		{
			return handleInvalidForm(redirectAttributes, ERROR_MESSAGE_INVALID_REG_CODE, regCodeActivationForm.getRegCode(),
					cartActivationInfo, checksum);
		}
		String regCode = regCodeActivationForm.getRegCode();
		try
		{
			CartActivationRequestDto cartActivationRequestDto =
					wileyplusPurchaseOptionsPageURLParser.parseCartActivationParams(cartActivationInfo, checksum);
			String purchaseType = getMessageSource().getMessage(PURCHASE_OPTION_TITLE_KEY, null,
					getI18nService().getCurrentLocale());
			String orderCode = checkoutFacade.processRegistrationCodeForCourse(regCode, cartActivationRequestDto, purchaseType);

			return ORDER_CONFIRM_PAGE + orderCode;
		}
		catch (RegCodeInvalidException e)
		{
			LOG.warn(String.format("Error occurs during activation of RegCode [%s]", regCode), e);
			return handleInvalidForm(redirectAttributes, ERROR_MESSAGE_INVALID_REG_CODE, regCode,
					cartActivationInfo, checksum);
		}
		catch (RegCodeValidationException e)
		{
			LOG.warn(String.format("Error occurs during activation of RegCode [%s]", regCode), e);
			return handleServiceException(redirectAttributes, ERROR_MESSAGE_INVALID_REG_CODE, regCode,
					cartActivationInfo, checksum);
		}
	}

	private String handleServiceException(final RedirectAttributes redirectAttributes, final String messageKey,
			final String regCode, final String cartActivationInfo, final String checksum)
	{
		handleGlobalError(redirectAttributes, EXTERNAL_SERVICE_RESPONSE_ERROR, regCode, cartActivationInfo, checksum);
		return PURCHASE_OPTIONS_REDIRECT;
	}

	private String handleInvalidForm(final RedirectAttributes redirectAttributes, final String messageKey,
			final String regCode, final String cartActivationInfo, final String checksum)
	{
		final String fieldErrorMessage = getMessageSource().getMessage(messageKey, null, getI18nService().getCurrentLocale());
		FieldError fieldError = new FieldError(REG_CODE_ACTIVATION_FORM, "regCode", fieldErrorMessage);
		redirectAttributes.addFlashAttribute(REG_CODE_FIELD_ERROR, fieldError);

		handleGlobalError(redirectAttributes, FORM_GLOBAL_ERROR, regCode, cartActivationInfo, checksum);
		return PURCHASE_OPTIONS_REDIRECT;
	}

	private void handleGlobalError(final RedirectAttributes redirectAttributes, final String globalMessageKey,
			final String regCode, final String cartActivationInfo, final String checksum)
	{
		String errorMessage = getMessageSource().getMessage(globalMessageKey, null,
				getI18nService().getCurrentLocale());
		GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
				errorMessage);
		redirectAttributes.addAttribute(ATTR_CART_ACTIVATION_INFO, cartActivationInfo);
		redirectAttributes.addAttribute(ATTR_CHECKSUM, checksum);
		redirectAttributes.addFlashAttribute("invalidRegCode", regCode);
		redirectAttributes.addFlashAttribute("regCodeAccordion", true);
	}
}
