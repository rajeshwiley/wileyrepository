package com.wiley.wileyb2c.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractSearchPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;

import java.util.Collections;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.subscription.exception.SubscriptionUpdateException;
import com.wiley.facades.product.data.WileySubscriptionData;
import com.wiley.facades.product.data.WileySubscriptionPaymentTransactionData;
import com.wiley.facades.subscription.WileySubscriptionOption;
import com.wiley.facades.subscription.Wileyb2cSubscriptionFacade;
import com.wiley.facades.wileycom.user.WileycomUserFacade;

import static de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages.CONF_MESSAGES_HOLDER;
import static de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages.ERROR_MESSAGES_HOLDER;


/**
 * Wiley B2C Subscription Manage Page Controller.
 */
@Controller
@Profile(WileyCoreConstants.WILEY_PHASE2A_PROFILE_NAME)
@RequestMapping("/my-account/subscriptions")
public class Wileyb2cSubscriptionManagePageController extends AbstractSearchPageController
{
	private static final Logger LOG = LoggerFactory.getLogger(Wileyb2cSubscriptionManagePageController.class);
	private static final String SUBSCRIPTIONS_URL = "/my-account/subscriptions";
	private static final String MANAGE_SUBSCRIPTION_CMS_PAGE = "subscription";
	private static final String SUBSCRIPTION_LIST_WITH_PAYMENT_DATA_PAGE = "subscriptionListWithPaymentDataPage";
	private static final String SUBSCRIPTION_BILLINGS_CMS_PAGE = "subscriptionBillingActivitiesPage";
	private static final AbstractSearchPageController.ShowMode SHOW_MODE = AbstractSearchPageController.ShowMode.Page;

	@Value("account.manageSubscription.sortType")
	private String sortType;

	@Value("${account.manageSubscription.itemsPerPage}")
	private int itemsPerPage;

	@Value("${account.subscriptions.billing.sortType}")
	private String sortTypeBillingActivities;

	@Value("${account.subscriptions.billing.itemsPerPage}")
	private int itemsPerPageBillingActivities;

	@Resource
	private Wileyb2cSubscriptionFacade wileyb2cSubscriptionFacade;

	@Resource
	private WileycomUserFacade userFacade;

	@RequestMapping(method = RequestMethod.GET)
	@RequireHardLogIn
	public String showManageSubsriptionPage(@RequestParam(required = false, defaultValue = "0") final Integer page,
			final Model model) throws CMSItemNotFoundException
	{
		final SearchPageData<WileySubscriptionData> searchPageData =
				wileyb2cSubscriptionFacade.getSubscriptionPage(createPageableData(page, itemsPerPage, sortType, SHOW_MODE));
		populateModel(model, searchPageData, SHOW_MODE);

		return setupAndGetViewForPage(model, SUBSCRIPTION_LIST_WITH_PAYMENT_DATA_PAGE);
	}

	@RequestMapping(value = "{code}", method = RequestMethod.GET)
	@RequireHardLogIn
	public String showManageSubscriptionPage(@PathVariable("code") final String subscriptionCode, final Model model)
			throws CMSItemNotFoundException
	{
		final WileySubscriptionData subscriptionData;

		try
		{
			subscriptionData = wileyb2cSubscriptionFacade.getSubscriptionDataForCurrentUserWithOptions(
					subscriptionCode, Collections.singletonList(WileySubscriptionOption.ORDER_DATA));
		}
		catch (final IllegalArgumentException e)
		{
			LOG.debug("Unable to find subscriptions: {}", e.getMessage(), e);
			return REDIRECT_PREFIX + SUBSCRIPTIONS_URL;
		}

		model.addAttribute("subscriptionData", subscriptionData);
		model.addAttribute("subscriptionDeliveryAddresses", userFacade.getAddressBook());
		model.addAttribute("paymentInfos", userFacade.getCCPaymentInfos(true));

		return setupAndGetViewForPage(model, MANAGE_SUBSCRIPTION_CMS_PAGE);
	}

	@RequestMapping(value = "{code}/update-payment", method = RequestMethod.POST)
	@RequireHardLogIn
	public String updateSubscriptionPaymentInfo(@RequestParam("selectedPaymentMethodId") final String selectedPaymentMethodId,
			@PathVariable("code") final String subscriptionCode, final RedirectAttributes redirectAttributes)
			throws CMSItemNotFoundException
	{
		try
		{
			wileyb2cSubscriptionFacade.updateCreditCardPaymentInfo(subscriptionCode, selectedPaymentMethodId);
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER,
					"account.subscription.manage.paymentUpdate.success");
		}
		catch (final SubscriptionUpdateException e)
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"account.subscription.manage.paymentUpdate.error");
			LOG.debug("Unable to update CC payment info: " + e.getMessage(), e);
		}

		return REDIRECT_PREFIX + String.format("%s/%s", SUBSCRIPTIONS_URL, subscriptionCode);
	}

	@RequestMapping(value = "{code}/billing-activities", method = RequestMethod.GET)
	@RequireHardLogIn
	public String listBillingActivities(@RequestParam(value = "page", defaultValue = "0") final int page,
			@PathVariable("code") final String currentSubscriptionCode, final Model model) throws CMSItemNotFoundException
	{
		final WileySubscriptionData subscriptionData;
		final SearchPageData<WileySubscriptionPaymentTransactionData> subscriptionTransactions;

		try
		{
			final PageableData pageableData =
					createPageableData(page, itemsPerPageBillingActivities, sortTypeBillingActivities, SHOW_MODE);
			subscriptionTransactions =
					wileyb2cSubscriptionFacade.getSubscriptionBillingActivities(currentSubscriptionCode, pageableData);
			subscriptionData = wileyb2cSubscriptionFacade.getSubscriptionDataForCurrentUser(currentSubscriptionCode);
		}
		catch (final IllegalArgumentException e)
		{
			LOG.debug("Unable to list billing activities: " + e.getMessage(), e);
			return REDIRECT_PREFIX + SUBSCRIPTIONS_URL;
		}

		model.addAttribute("subscriptionData", subscriptionData);
		model.addAttribute("subscriptionTransactions", subscriptionTransactions);
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);

		return setupAndGetViewForPage(model, SUBSCRIPTION_BILLINGS_CMS_PAGE);
	}

	@RequireHardLogIn
	@RequestMapping(value = "autorenew", method = RequestMethod.POST)
	public String setAutoRenewSubscription(
			@RequestParam("currentSubscriptionCode") final String currentSubscriptionCode,
			@RequestParam(value = "subscriptionAutorenewal", defaultValue = "false", required = false) final Boolean autorenew,
			final RedirectAttributes redirectAttributes)
	{
		try
		{
			wileyb2cSubscriptionFacade.setSubscriptionRequireRenewal(currentSubscriptionCode, autorenew);
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER,
					"account.subscription.manage.autorenew.success");
		}
		catch (SubscriptionUpdateException e)
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"account.subscription.manage.autorenew.error");
			LOG.debug("Unable to set subscription renewal: " + e.getMessage(), e);
		}

		return REDIRECT_PREFIX + String.format("%s/%s", SUBSCRIPTIONS_URL, currentSubscriptionCode);
	}

	@RequestMapping(value = "updateDeliveryAddress", method = RequestMethod.POST)
	@RequireHardLogIn
	public String updateDeliveryAddress(@RequestParam("subscriptionCode") final String subscriptionCode,
			@RequestParam("selectedDeliveryAddressCode") final String deliveryAddressCode,
			final RedirectAttributes redirectAttributes)
	{
		if (wileyb2cSubscriptionFacade.updateDeliveryAddress(subscriptionCode, deliveryAddressCode))
		{
			GlobalMessages.addFlashMessage(redirectAttributes, CONF_MESSAGES_HOLDER,
					"account.subscription.manageSubscriptions.shipping.confirmationMessage");
		}
		else
		{
			GlobalMessages.addFlashMessage(redirectAttributes, ERROR_MESSAGES_HOLDER,
					"account.subscription.manageSubscriptions.shipping.errorMessage");
		}

		return REDIRECT_PREFIX + String.format("%s/%s", SUBSCRIPTIONS_URL, subscriptionCode);
	}

	private String setupAndGetViewForPage(final Model model, final String cmsPageLabel) throws CMSItemNotFoundException
	{
		final ContentPageModel contentPageModel = getContentPageForLabelOrId(cmsPageLabel);
		storeCmsPageInModel(model, contentPageModel);
		setUpMetaDataForContentPage(model, contentPageModel);

		return getViewForPage(model);
	}
}
