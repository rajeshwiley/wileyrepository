/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wileyb2c.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.GuestForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.LoginForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.RegisterForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.GuestValidator;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.user.UserService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.wileyb2c.storefront.controllers.ControllerConstants;
import com.wiley.wileyb2c.storefront.security.impl.WebHttpSessionRequestCache;
import com.wiley.wileycom.storefrontcommons.controllers.pages.AbstractWileycomLoginPageController;


/**
 * Login Controller. Handles login and register for the account flow.
 */
@Controller
@Scope("tenant")
@RequestMapping(value = "/login")
@Profile(WileyCoreConstants.WILEY_PHASE2A_PROFILE_NAME)
public class LoginPageController extends AbstractWileycomLoginPageController
{
	private static final Logger LOG = LoggerFactory.getLogger(LoginPageController.class);

	private WebHttpSessionRequestCache httpSessionRequestCache;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "guestValidator")
	private GuestValidator guestValidator;

	@Override
	protected String getView()
	{
		return ControllerConstants.Views.Pages.Account.ACCOUNT_LOGIN_PAGE;
	}

	@Override
	protected String getSuccessRedirect(final HttpServletRequest request, final HttpServletResponse response)
	{
		if (httpSessionRequestCache.getRequest(request, response) != null)
		{
			return httpSessionRequestCache.getRequest(request, response).getRedirectUrl();
		}
		return "/";
	}

	@Override
	protected AbstractPageModel getCmsPage() throws CMSItemNotFoundException
	{
		return getContentPageForLabelOrId("login");
	}


	@Resource(name = "httpSessionRequestCache")
	public void setHttpSessionRequestCache(final WebHttpSessionRequestCache accHttpSessionRequestCache)
	{
		this.httpSessionRequestCache = accHttpSessionRequestCache;
	}


	@RequestMapping(method = RequestMethod.GET)
	public String doLogin(@RequestHeader(value = "referer", required = false) final String referer,
			@RequestParam(value = "error", defaultValue = "false") final boolean loginError, final Model model,
			@RequestParam(value = "errorCode", defaultValue = "") final String loginErrorCode,
			final HttpServletRequest request, final HttpServletResponse response, final HttpSession session)
			throws CMSItemNotFoundException
	{
		if (!loginError)
		{
			storeReferer(referer, request, response);
		}
		return getDefaultLoginPage(loginError, loginErrorCode, session, model);
	}

	protected void storeReferer(final String referer, final HttpServletRequest request, final HttpServletResponse response)
	{
		if (StringUtils.isNotBlank(referer) && !StringUtils.endsWith(referer, "/login")
				&& StringUtils.contains(referer, request.getServerName()))
		{
			httpSessionRequestCache.saveRequestReferer(request, response);
		}
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String doRegister(@RequestHeader(value = "referer", required = false) final String referer, final RegisterForm form,
			final BindingResult bindingResult, final Model model, final HttpServletRequest request,
			final HttpServletResponse response, final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		getRegistrationValidator().validate(form, bindingResult);
		return processRegisterUserRequest(referer, form, bindingResult, model, request, response, redirectModel);
	}

	@RequestMapping(value = "/createAccount", method = RequestMethod.POST)
	public String createAccount(final GuestForm guestForm, final BindingResult bindingResult, final Model model,
			final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		guestValidator.validate(guestForm, bindingResult);
		checkIfCustomerWithGivenEmailAlreadyExists(guestForm.getEmail(), bindingResult);
		if (bindingResult.hasErrors())
		{
			return handleGuestFormErrors(model, guestForm);
		}
		redirectAttributes.addFlashAttribute("emailAddress", guestForm.getEmail());

		//TODO: Replace the url in registration page story.
		return "redirect:/login/checkout/register";
	}

	private String handleGuestFormErrors(final Model model, final GuestForm guestForm) throws CMSItemNotFoundException
	{
		model.addAttribute(guestForm);
		model.addAttribute(new LoginForm());
		model.addAttribute(new RegisterForm());
		GlobalMessages.addErrorMessage(model, "form.global.error");
		storeCmsPageInModel(model, getContentPageForLabelOrId("login"));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId("login"));
		return ControllerConstants.Views.Pages.Account.ACCOUNT_LOGIN_PAGE;
	}

	private void checkIfCustomerWithGivenEmailAlreadyExists(final String email, final BindingResult bindingResult)
	{
		try
		{
			final UserModel user = userService.getUserForUID(email);

			if (user != null)
			{
				bindingResult.rejectValue("email", "registration.error.account.exists.title");
			}
		}
		catch (final UnknownIdentifierException e)
		{
			LOG.info(String.format("No account exists with email address %s", email));
		}
	}
}
