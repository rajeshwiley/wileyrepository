package com.wiley.wileyb2c.storefront.forms;

import de.hybris.platform.acceleratorstorefrontcommons.forms.UpdateProfileForm;


/**
 * Represents profile form for b2b customer
 */
public class Wileyb2cUpdateProfileForm extends UpdateProfileForm
{
	private String loginId;
	private String email;
	private String middleName;
	private String suffixCode;
	private String studentId;
	private String major;
	private String graduationMonth;
	private String graduationYear;
	private String school;
	private boolean isEnrolledInSchool;

	/**
	 * @return the isEnrolledInSchool
	 */
	public boolean getEnrolledInSchool()
	{
		return isEnrolledInSchool;
	}

	/**
	 * @param isEnrolledInSchool the isEnrolledInSchool to set
	 */
	public void setEnrolledInSchool(boolean isEnrolledInSchool)
	{
		this.isEnrolledInSchool = isEnrolledInSchool;
	}

	/**
	 * @return the school
	 */
	public String getSchool()
	{
		return school;
	}

	/**
	 * @param school the school to set
	 */
	public void setSchool(final String school)
	{
		this.school = school;
	}

	/**
	 * @return the studentId
	 */
	public String getStudentId()
	{
		return studentId;
	}

	/**
	 * @param studentId the studentId to set
	 */
	public void setStudentId(final String studentId)
	{
		this.studentId = studentId;
	}

	/**
	 * @return the major
	 */
	public String getMajor()
	{
		return major;
	}

	/**
	 * @param major the major to set
	 */
	public void setMajor(final String major)
	{
		this.major = major;
	}

	/**
	 * @return the graduationYear
	 */
	public String getGraduationYear()
	{
		return graduationYear;
	}

	/**
	 * @return the graduationMonth
	 */
	public String getGraduationMonth()
	{
		return graduationMonth;
	}

	/**
	 * @param graduationMonth the graduationMonth to set
	 */
	public void setGraduationMonth(final String graduationMonth)
	{
		this.graduationMonth = graduationMonth;
	}

	/**
	 * @param graduationYear the graduationYear to set
	 */
	public void setGraduationYear(final String graduationYear)
	{
		this.graduationYear = graduationYear;
	}

	public String getLoginId()
	{
		return loginId;
	}

	public void setLoginId(final String loginId)
	{
		this.loginId = loginId;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(final String email)
	{
		this.email = email;
	}

	public String getMiddleName()
	{
		return middleName;
	}

	public void setMiddleName(final String middleName)
	{
		this.middleName = middleName;
	}

	public String getSuffixCode()
	{
		return suffixCode;
	}

	public void setSuffixCode(final String suffixCode)
	{
		this.suffixCode = suffixCode;
	}
}
