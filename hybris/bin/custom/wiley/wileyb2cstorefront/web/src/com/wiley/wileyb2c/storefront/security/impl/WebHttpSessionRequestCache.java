/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wileyb2c.storefront.security.impl;

import org.springframework.security.core.context.SecurityContextHolder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wiley.storefrontcommons.security.impl.WileyWebHttpSessionRequestCache;


/**
 * Extension of HttpSessionRequestCache that allows pass through of cookies from the current request. This is required
 * to allow the GUIDInterceptor to see the secure cookie written during authentication.
 * 
 * The <tt>RequestCache</tt> stores the <tt>SavedRequest</tt> in the HttpSession, this is then restored perfectly.
 * Unfortunately the saved request also hides new cookies that have been written since the saved request was created.
 * This implementation allows the current request's cookie values to override the cookies within the saved request.
 */
public class WebHttpSessionRequestCache extends WileyWebHttpSessionRequestCache
{
	public static final String SECURE_GUID_SESSION_KEY = "acceleratorSecureGUID";

	@Override
	public void saveRequest(final HttpServletRequest request, final HttpServletResponse response)
	{
		//this might be called while in ExceptionTranslationFilter#handleSpringSecurityException in this case
		// base implementation. Also in case of soft logged user when authentication is present but not sufficient
		// stores original request (e.g super impl)
		if (SecurityContextHolder.getContext().getAuthentication() == null || isGUIDMissing(request))
		{
			super.saveRequest(request, response);
		}
		else
		{
			saveRequestReferer(request, response);
		}
	}

	private boolean isGUIDMissing(final HttpServletRequest request)
	{
		return  request.getSession().getAttribute(SECURE_GUID_SESSION_KEY) == null;
	}
}
