package com.wiley.wileyb2c.storefront.controllers.pages.checkout.steps;


import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.facades.payment.WileyPaymentFacade;
import com.wiley.wileyb2c.storefront.controllers.ControllerConstants;
import com.wiley.wileycom.storefrontcommons.controllers.pages.checkout.steps.WileycomHopPaymentResponseController;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;

import javax.annotation.Resource;


@Controller
@Profile(WileyCoreConstants.WILEY_PHASE2A_PROFILE_NAME)
public class HopPaymentResponseController extends WileycomHopPaymentResponseController
{
	@Resource
	private WileyPaymentFacade wileyb2cPaymentFacade;

	@Override
	protected String getErrorPage()
	{
		return ControllerConstants.Views.Pages.MultiStepCheckout.HOSTED_ORDER_PAGE_ERROR_PAGE;
	}

	@Override
	protected WileyPaymentFacade getWileyPaymentFacade()
	{
		return wileyb2cPaymentFacade;
	}

}
