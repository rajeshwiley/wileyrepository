/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wileyb2c.storefront.checkout.steps.validation.impl;


import com.wiley.facades.order.WileycomCheckoutFacade;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


public class ResponsiveEnterCheckoutStepValidator extends Wileyb2cAbstractCheckoutStepValidator
{
	private static final Logger LOG = LoggerFactory.getLogger(ResponsiveEnterCheckoutStepValidator.class);

	@Autowired
	private WileycomCheckoutFacade wileycomCheckoutFacade;

	@Override
	public ValidationResults validateOnEnter(final RedirectAttributes redirectAttributes)
	{
		if (getCheckoutFlowFacade().hasValidCart())
		{
			return ValidationResults.SUCCESS;
		}
		LOG.info("Missing, empty or unsupported cart");
		return ValidationResults.FAILED;
	}

	@Override
	public ValidationResults validateOnExit()
	{
		if (wileycomCheckoutFacade.isDigitalSessionCart())
		{
			return ValidationResults.REDIRECT_TO_PAYMENT_TYPE;
		}
		return super.validateOnExit();
	}

}
