package com.wiley.wileyb2c.storefront.controllers.cms;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.wiley.core.model.components.WileyBrightcoveVideoComponentModel;
import com.wiley.facades.populators.bright.cover.WileyBrightCoverHelper;
import com.wiley.wileyb2c.storefront.controllers.ControllerConstants;


/**
 * Created by Raman_Hancharou on 5/16/2017.
 */
@Controller("WileyBrightcoveVideoComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.WILEY_BRIGHTCOVE_VIDEO_COMPONENT)
public class WileyBrightcoveVideoComponentController extends AbstractCMSComponentController<WileyBrightcoveVideoComponentModel>
{
	@Resource
	private WileyBrightCoverHelper wileyBrightCoverHelper;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model,
			final WileyBrightcoveVideoComponentModel component)
	{
		if (StringUtils.isEmpty(component.getPlayerId()))
		{
			component.setPlayerId(wileyBrightCoverHelper.getPropertyForCurrentSite(WileyBrightCoverHelper.PLAYER_ID));
		}
		model.addAttribute("cssClasses", component.getCssClasses().toArray(new String[component.getCssClasses().size()]));
		model.addAttribute("accountId", wileyBrightCoverHelper.getPropertyForCurrentSite(WileyBrightCoverHelper.ACCOUNT_ID));
	}

	@Override
	protected String getView(final WileyBrightcoveVideoComponentModel component)
	{
		return ControllerConstants.Views.Cms.COMPONENT_PREFIX + StringUtils.lowerCase(
				WileyBrightcoveVideoComponentModel._TYPECODE);
	}
}
