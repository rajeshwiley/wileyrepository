package com.wiley.wileyb2c.storefront.forms;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 * Form object for RegCode activation.
 */
public class RegCodeActivationForm implements Serializable
{
	@NotNull(message = "{regCode.errorMessage.invalid}")
	@Size(min = 1, max = 50, message = "{regCode.errorMessage.invalid}")
	private String regCode;

	public String getRegCode()
	{
		return regCode;
	}

	public void setRegCode(final String regCode)
	{
		this.regCode = regCode;
	}
}
