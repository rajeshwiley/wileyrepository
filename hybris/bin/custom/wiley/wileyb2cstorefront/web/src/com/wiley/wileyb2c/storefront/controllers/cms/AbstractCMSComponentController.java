/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wileyb2c.storefront.controllers.cms;

import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.wiley.storefrontcommons.controllers.cms.AbstractWileyCMSComponentController;
import com.wiley.wileyb2c.storefront.controllers.ControllerConstants;


/**
 * Abstract Controller for CMS Components
 */
public abstract class AbstractCMSComponentController<T extends AbstractCMSComponentModel>
		extends AbstractWileyCMSComponentController<T>
{

	protected String getView(final T component)
	{
		// build a jsp response based on the component type
		return ControllerConstants.Views.Cms.COMPONENT_PREFIX + StringUtils.lowerCase(getTypeCode(component));
	}

	/**
	 * This handler is designed to intercept Exceptions which can be handled by WileycomGlobalExceptionHandler
	 * to avoid 500 errors on storefronts for components
	 *
	 * @param request
	 * @param response
	 * @param exc
	 * @throws Exception
	 */
	@ExceptionHandler(Exception.class)
	public void componentErrorHandler(final HttpServletRequest request, final HttpServletResponse response,
			final Exception exc) throws Exception
	{
		LOG.error("Unhandled exception happened for component [" + ((T) request.getAttribute(COMPONENT)).getUid() + "]", exc);
	}
}
