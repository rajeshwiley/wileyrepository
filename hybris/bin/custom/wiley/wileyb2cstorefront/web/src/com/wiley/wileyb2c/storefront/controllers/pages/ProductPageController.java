/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wileyb2c.storefront.controllers.pages;

import de.hybris.platform.acceleratorfacades.futurestock.FutureStockFacade;
import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.acceleratorservices.storefront.data.MetaElementData;
import de.hybris.platform.acceleratorservices.urlresolver.SiteBaseUrlResolutionService;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.ProductBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.FutureStockForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.ReviewForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.ReviewValidator;
import de.hybris.platform.acceleratorstorefrontcommons.util.MetaSanitizerUtil;
import de.hybris.platform.acceleratorstorefrontcommons.util.XSSFilterUtil;
import de.hybris.platform.acceleratorstorefrontcommons.variants.VariantSortStrategy;
import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.BaseOptionData;
import de.hybris.platform.commercefacades.product.data.FutureStockData;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ImageDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.ProductReferenceData;
import de.hybris.platform.commercefacades.product.data.ReviewData;
import de.hybris.platform.commercefacades.storesession.StoreSessionFacade;
import de.hybris.platform.commercefacades.storesession.data.CurrencyData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.util.Config;
import de.hybris.platform.variants.model.VariantProductModel;

import java.io.UnsupportedEncodingException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Maps;
import com.wiley.core.model.WileyPurchaseOptionProductModel;
import com.wiley.core.wileyb2c.i18n.Wileyb2cI18NService;
import com.wiley.core.wileyb2c.product.Wileyb2cPurchaseOptionService;
import com.wiley.facades.wiley.visibility.WileyProductVisibilityFacade;
import com.wiley.facades.wileyb2c.cms.Wileyb2cCMSFacade;
import com.wiley.facades.wileyb2c.product.WileyJsonLdProductFacade;
import com.wiley.facades.wileyb2c.product.Wileyb2cProductFacade;
import com.wiley.facades.wileycom.storesession.WileycomStoreSessionFacade;
import com.wiley.storefrontcommons.web.theme.StorefrontResourceBundleSource;
import com.wiley.storefrontcommons.util.WileyMetaSanitizerUtil;
import com.wiley.wileyb2c.storefront.controllers.ControllerConstants;
import com.wiley.wileyb2c.storefront.util.Wileyb2cPageTitleResolver;
import com.wiley.wileyb2c.storefront.util.Wileyb2cValueParser;
import com.wiley.wileycom.storefrontcommons.util.WileyRedirectUrlResolverUtil;

import static com.wiley.facades.constants.WileyFacadesConstants.CAROUSEL_MAX_NUMBER_OF_PRODUCTS_DEFAULT_VALUE;
import static com.wiley.facades.constants.WileyFacadesConstants.CAROUSEL_MAX_NUMBER_OF_PRODUCTS_MESSAGE_CODE;
import static com.wiley.storefrontcommons.constants.WileystorefrontcommonsConstants.FORWARD_TO_404;
import static com.wiley.wileycom.storefrontcommons.constants.WileycomstorefrontcommonsConstants.MARKETING_CONTENT_MENU_ITEMS;


/**
 * Controller for product details page
 */
@Controller
@Scope("tenant")
@RequestMapping
public class ProductPageController extends AbstractPageController
{
	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(ProductPageController.class);
	private static final String MODEL_PRODUCT = "product";
	private static final String MODEL_PREFIXES = "prefixes";
	private static final String MODEL_METATAGS = "openGraphTags";
	private static final String DEFAULT_IMAGE = "img.missingProductImage.responsive.product";
	private static final ThreadLocal<Format> DATE_TO_ISO8601 =
			ThreadLocal.withInitial(() -> new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX"));

	/**
	 * We use this suffix pattern because of an issue with Spring 3.1 where a Uri value is incorrectly extracted if it
	 * contains on or more '.' characters. Please see https://jira.springsource.org/browse/SPR-6164 for a discussion on
	 * the issue and future resolution.
	 */
	private static final String PRODUCT_CODE_PATH_VARIABLE_PATTERN = "/**/*-p-{productCode:.*}";
	private static final String REVIEWS_PATH_VARIABLE_PATTERN = "{numberOfReviews:.*}";

	private static final String FUTURE_STOCK_ENABLED = "storefront.products.futurestock.enabled";
	private static final String STOCK_SERVICE_UNAVAILABLE = "basket.page.viewFuture.unavailable";
	private static final String NOT_MULTISKU_ITEM_ERROR = "basket.page.viewFuture.not.multisku";

	private static final String REFERENCE_TYPE_MESSAGE_CODE = "continue.shopping.popup.product.carousel.reference.type";
	private static final String REFERENCE_TYPE_DEFAULT_VALUE = "REFERENCE";

	private static final String CAROUSEL_DISPLAY_PRODUCT_TITLES_MESSAGE_CODE =
			"continue.shopping.popup.product.carousel.display.product.titles";
	private static final Boolean CAROUSEL_DISPLAY_PRODUCT_TITLES_DEFAULT_VALUE = Boolean.TRUE;
	private static final String CAROUSEL_DISPLAY_PRODUCT_AUTHORS_MESSAGE_CODE =
			"continue.shopping.popup.product.carousel.display.product.authors";
	private static final Boolean CAROUSEL_DISPLAY_PRODUCT_AUTHORS_DEFAULT_VALUE = Boolean.TRUE;
	private static final String CAROUSEL_DISPLAY_PRODUCT_PRICES_MESSAGE_CODE =
			"continue.shopping.popup.product.carousel.display.product.prices";
	private static final Boolean CAROUSEL_DISPLAY_PRODUCT_PRICES_DEFAULT_VALUE = Boolean.FALSE;

	@Resource(name = "productModelUrlResolver")
	private UrlResolver<ProductModel> productModelUrlResolver;

	@Resource(name = "accProductFacade")
	private ProductFacade productFacade;

	@Resource(name = "productService")
	private ProductService productService;

	@Resource(name = "productBreadcrumbBuilder")
	private ProductBreadcrumbBuilder productBreadcrumbBuilder;

	@Resource(name = "cmsPageService")
	private CMSPageService cmsPageService;

	@Resource(name = "variantSortStrategy")
	private VariantSortStrategy variantSortStrategy;

	@Resource(name = "reviewValidator")
	private ReviewValidator reviewValidator;

	@Resource(name = "futureStockFacade")
	private FutureStockFacade futureStockFacade;

	@Resource(name = "wileyb2cProductFacade")
	private Wileyb2cProductFacade wileyb2cProductFacade;

	@Resource(name = "wileyb2cCMSFacade")
	private Wileyb2cCMSFacade wileyb2cCMSFacade;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Resource
	private WileyProductVisibilityFacade wileyProductVisibilityFacade;

	@Resource
	private Wileyb2cPurchaseOptionService wileyb2cPurchaseOptionService;

	@Resource
	private WileycomStoreSessionFacade wileycomStoreSessionFacade;

	@Resource
	private StoreSessionFacade storeSessionFacade;

	@Resource
	private Wileyb2cI18NService wileyb2cI18NService;

	@Resource
	private Wileyb2cPageTitleResolver wileyb2cPageTitleResolver;

	@Resource(name = "wileyJsonLdProductFacade")
	private WileyJsonLdProductFacade wileyJsonLdProductFacade;

	@Resource(name = "messageSource")
	private StorefrontResourceBundleSource storefrontResourceBundleSource;

	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource(name = "siteBaseUrlResolutionService")
	private SiteBaseUrlResolutionService siteBaseUrlResolutionService;

	@Resource(name = "wileyb2cValueParser")
	private Wileyb2cValueParser wileyb2cValueParser;

	@RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	public String productDetail(@PathVariable("productCode") final String productCode, final Model model,
			final HttpServletRequest request, final HttpServletResponse response) throws CMSItemNotFoundException,
			UnsupportedEncodingException
	{
		final ProductModel productModel = productService.getProductForCode(productCode);
		final String redirection = getRedirection(productModel, request, response);

		if (StringUtils.isNotEmpty(redirection))
		{
			return redirection;
		}
		if (wileyProductVisibilityFacade.isProductVisible(productCode))
		{
			final ProductData productData = productFacade.getProductForCodeAndOptions(productCode,
					Arrays.asList(ProductOption.SEO_METADATA));
			updatePageTitle(productData.getSeoTitleTag(), productModel, model);
			List<ProductOption> extraOptions;
			if (configurationService.getConfiguration().
					getBoolean("wileyb2cstorefront.filtered.populators.enabled", false))
			{
				extraOptions = Arrays.asList(
						ProductOption.VARIANT_MATRIX_BASE,
						ProductOption.VARIANT_MATRIX_URL,
						ProductOption.VARIANT_MATRIX_MEDIA,
						ProductOption.EXCERPTS,
						ProductOption.BRIGHT_COVE_VIDEO, ProductOption.BACKGROUND_IMAGE, ProductOption.AUTHOR_INFOS,
						ProductOption.AUTHOR_INFOS_WITHOUT_ROLE, ProductOption.ENCODED_AUTHORS_LIST, ProductOption.DETAILS,
						ProductOption.VARIANT_PRODUCT_NAME, ProductOption.VISIBLE_REFERENCES, ProductOption.PRODUCT_SETS,
						ProductOption.PRODUCT_SET_COMPONENTS, ProductOption.PRODUCT_BUNDLES,
						ProductOption.IMPRINTS,
						ProductOption.SERIES);
			}
			else
			{
				extraOptions = Arrays.asList(
						ProductOption.EXCERPTS,
						ProductOption.BRIGHT_COVE_VIDEO,
						ProductOption.BACKGROUND_IMAGE,
						ProductOption.AUTHOR_INFOS,
						ProductOption.AUTHOR_INFOS_WITHOUT_ROLE,
						ProductOption.ENCODED_AUTHORS_LIST,
						ProductOption.DETAILS,
						ProductOption.VARIANT_PRODUCT_NAME,
						ProductOption.PRODUCT_SETS,
						ProductOption.PRODUCT_SET_COMPONENTS,
						ProductOption.PRODUCT_BUNDLES,
						ProductOption.SERIES
				);
			}
			populateProductDetailForDisplay(productModel, model, request, extraOptions);
			setUpContentMenu(model, productModel);

			model.addAttribute(new ReviewForm());

			final List<ProductReferenceData> productReferences = productFacade.getProductReferencesForCode(productCode,
					Arrays.asList(ProductReferenceTypeEnum.SIMILAR, ProductReferenceTypeEnum.ACCESSORIES),
					Arrays.asList(ProductOption.BASIC, ProductOption.PRICE), null);

			model.addAttribute("productReferences", productReferences);
			model.addAttribute("pageType", PageType.PRODUCT.name());
			model.addAttribute("futureStockEnabled", Config.getBoolean(FUTURE_STOCK_ENABLED, false));
			final String metaKeywords = WileyMetaSanitizerUtil.sanitizeKeywords(productModel.getKeywords());
			final String metaDescription = MetaSanitizerUtil.sanitizeDescription(productData.getSeoDescriptionTag());

			setUpMetaData(model, metaKeywords, metaDescription);
			setUpOpenGraphMetaTags(model, request, metaDescription);

			return getViewForPage(model);
		}
		else
		{
			return FORWARD_TO_404;
		}
	}

	@RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/orderForm", method = RequestMethod.GET)
	public String productOrderForm(@PathVariable("productCode") final String productCode, final Model model,
			final HttpServletRequest request, final HttpServletResponse response) throws CMSItemNotFoundException
	{
		if (wileyProductVisibilityFacade.isProductVisible(productCode))
		{
			final ProductData productData = productFacade.getProductForCodeAndOptions(productCode,
					Arrays.asList(ProductOption.SEO_METADATA));
			final ProductModel productModel = productService.getProductForCode(productCode);
			updatePageTitle(productData.getSeoTitleTag(), productModel, model);

			final List<ProductOption> extraOptions = Arrays.asList(ProductOption.VARIANT_MATRIX_BASE,
					ProductOption.VARIANT_MATRIX_PRICE, ProductOption.VARIANT_MATRIX_MEDIA, ProductOption.VARIANT_MATRIX_STOCK,
					ProductOption.URL);
			populateProductDetailForDisplay(productModel, model, request, extraOptions);

			if (!model.containsAttribute(WebConstants.MULTI_DIMENSIONAL_PRODUCT))
			{
				return REDIRECT_PREFIX + productModelUrlResolver.resolve(productModel);
			}

			return ControllerConstants.Views.Pages.Product.ORDER_FORM;
		}
		else
		{
			return FORWARD_TO_404;
		}
	}

	@RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/zoomImages", method = RequestMethod.GET)
	public String showZoomImages(@PathVariable("productCode") final String productCode,
			@RequestParam(value = "galleryPosition", required = false) final String galleryPosition, final Model model)
	{
		if (wileyProductVisibilityFacade.isProductVisible(productCode))
		{
			final ProductModel productModel = productService.getProductForCode(productCode);
			final ProductData productData = productFacade.getProductForOptions(productModel,
					Collections.singleton(ProductOption.GALLERY));
			final List<Map<String, ImageData>> images = getGalleryImages(productData);
			populateProductData(productData, model);
			if (galleryPosition != null)
			{
				try
				{
					model.addAttribute("zoomImageUrl", images.get(Integer.parseInt(galleryPosition)).get("zoom").getUrl());
				}
				catch (final IndexOutOfBoundsException | NumberFormatException e)
				{
					model.addAttribute("zoomImageUrl", "");
					LOG.debug("Unable to zoom: " + e.getMessage(), e);
				}
			}
			return ControllerConstants.Views.Fragments.Product.ZOOM_IMAGES_POPUP;
		}
		else
		{
			return FORWARD_TO_404;
		}
	}

	@RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/quickView", method = RequestMethod.GET)
	public String showQuickView(@PathVariable("productCode") final String productCode, final Model model,
			final HttpServletRequest request)
	{
		if (wileyProductVisibilityFacade.isProductVisible(productCode))
		{
			final ProductModel productModel = productService.getProductForCode(productCode);
			final ProductData productData = productFacade.getProductForOptions(productModel, Arrays.asList(ProductOption.BASIC,
					ProductOption.PRICE, ProductOption.SUMMARY, ProductOption.DESCRIPTION, ProductOption.CATEGORIES,
					ProductOption.PROMOTIONS, ProductOption.STOCK, ProductOption.REVIEW, ProductOption.VARIANT_FULL,
					ProductOption.DELIVERY_MODE_AVAILABILITY));

			sortVariantOptionData(productData);
			populateProductData(productData, model);
			getRequestContextData(request).setProduct(productModel);

			return ControllerConstants.Views.Fragments.Product.QUICK_VIEW_POPUP;
		}
		else
		{
			return FORWARD_TO_404;
		}
	}

	@RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/review", method = { RequestMethod.GET, RequestMethod.POST })
	public String postReview(@PathVariable final String productCode, final ReviewForm form, final BindingResult result,
			final Model model, final HttpServletRequest request, final RedirectAttributes redirectAttrs)
			throws CMSItemNotFoundException
	{
		if (wileyProductVisibilityFacade.isProductVisible(productCode))
		{
			getReviewValidator().validate(form, result);

			final ProductModel productModel = productService.getProductForCode(productCode);

			if (result.hasErrors())
			{
				final ProductData productData = productFacade.getProductForCodeAndOptions(productCode,
						Arrays.asList(ProductOption.SEO_METADATA));
				updatePageTitle(productData.getSeoTitleTag(), productModel, model);
				GlobalMessages.addErrorMessage(model, "review.general.error");
				model.addAttribute("showReviewForm", Boolean.TRUE);
				populateProductDetailForDisplay(productModel, model, request, Collections.EMPTY_LIST);
				storeCmsPageInModel(model, getPageForProduct(productModel));
				return getViewForPage(model);
			}

			final ReviewData review = new ReviewData();
			review.setHeadline(XSSFilterUtil.filter(form.getHeadline()));
			review.setComment(XSSFilterUtil.filter(form.getComment()));
			review.setRating(form.getRating());
			review.setAlias(XSSFilterUtil.filter(form.getAlias()));
			productFacade.postReview(productCode, review);
			GlobalMessages.addFlashMessage(redirectAttrs, GlobalMessages.CONF_MESSAGES_HOLDER,
					"review.confirmation.thank.you.title");

			return REDIRECT_PREFIX + productModelUrlResolver.resolve(productModel);
		}

		else
		{
			return FORWARD_TO_404;
		}
	}

	@RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/reviewhtml/" + REVIEWS_PATH_VARIABLE_PATTERN,
			method = RequestMethod.GET)
	public String reviewHtml(@PathVariable("productCode") final String productCode,
			@PathVariable("numberOfReviews") final String numberOfReviews, final Model model, final HttpServletRequest request)
	{
		if (wileyProductVisibilityFacade.isProductVisible(productCode))
		{
			final ProductModel productModel = productService.getProductForCode(productCode);
			final List<ReviewData> reviews;
			final ProductData productData = productFacade.getProductForOptions(productModel,
					Arrays.asList(ProductOption.BASIC, ProductOption.REVIEW));

			if ("all".equals(numberOfReviews))
			{
				reviews = productFacade.getReviews(productCode);
			}
			else
			{
				final int reviewCount = Math.min(Integer.parseInt(numberOfReviews), (productData.getNumberOfReviews() == null ?
						0 : productData.getNumberOfReviews().intValue()));
				reviews = productFacade.getReviews(productCode, Integer.valueOf(reviewCount));
			}

			getRequestContextData(request).setProduct(productModel);
			model.addAttribute("reviews", reviews);
			model.addAttribute("reviewsTotal", productData.getNumberOfReviews());
			model.addAttribute(new ReviewForm());

			return ControllerConstants.Views.Fragments.Product.REVIEWS_TAB;
		}
		else
		{
			return FORWARD_TO_404;
		}
	}

	@RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/writeReview", method = RequestMethod.GET)
	public String writeReview(@PathVariable final String productCode, final Model model)
			throws CMSItemNotFoundException
	{
		if (wileyProductVisibilityFacade.isProductVisible(productCode))
		{
			final ProductModel productModel = productService.getProductForCode(productCode);
			model.addAttribute(new ReviewForm());
			setUpReviewPage(model, productModel);
			return ControllerConstants.Views.Pages.Product.WRITE_REVIEW;
		}
		else
		{
			return FORWARD_TO_404;
		}
	}

	protected void setUpReviewPage(final Model model, final ProductModel productModel) throws CMSItemNotFoundException
	{
		final ProductData productData = productFacade.getProductForCodeAndOptions(productModel.getCode(),
				Arrays.asList(ProductOption.SEO_METADATA));
		final String metaKeywords = WileyMetaSanitizerUtil.sanitizeKeywords(productModel.getKeywords());
		final String metaDescription = MetaSanitizerUtil.sanitizeDescription(productData.getSeoDescriptionTag());
		setUpMetaData(model, metaKeywords, metaDescription);
		storeCmsPageInModel(model, getPageForProduct(productModel));
		model.addAttribute(MODEL_PRODUCT, productFacade.getProductForOptions(productModel, Arrays.asList(ProductOption.BASIC)));
		updatePageTitle(productData.getSeoTitleTag(), productModel, model);
	}

	protected void setUpContentMenu(final Model model, final ProductModel product)
	{
		if (model.containsAttribute(CMS_PAGE_MODEL))
		{
			final AbstractPageModel page = (AbstractPageModel) model.asMap().get(CMS_PAGE_MODEL);

			if (page != null)
			{
				model.addAttribute(MARKETING_CONTENT_MENU_ITEMS, wileyb2cCMSFacade.getMarketingContentMenu(page, product));
			}
		}
	}

	@RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/writeReview", method = RequestMethod.POST)
	public String writeReview(@PathVariable final String productCode, final ReviewForm form, final BindingResult result,
			final Model model, final HttpServletRequest request, final RedirectAttributes redirectAttrs)
			throws CMSItemNotFoundException
	{
		if (wileyProductVisibilityFacade.isProductVisible(productCode))
		{
			getReviewValidator().validate(form, result);

			final ProductModel productModel = productService.getProductForCode(productCode);

			if (result.hasErrors())
			{
				GlobalMessages.addErrorMessage(model, "review.general.error");
				populateProductDetailForDisplay(productModel, model, request, Collections.EMPTY_LIST);
				setUpReviewPage(model, productModel);
				return ControllerConstants.Views.Pages.Product.WRITE_REVIEW;
			}

			final ReviewData review = new ReviewData();
			review.setHeadline(XSSFilterUtil.filter(form.getHeadline()));
			review.setComment(XSSFilterUtil.filter(form.getComment()));
			review.setRating(form.getRating());
			review.setAlias(XSSFilterUtil.filter(form.getAlias()));
			productFacade.postReview(productCode, review);
			GlobalMessages.addFlashMessage(redirectAttrs, GlobalMessages.CONF_MESSAGES_HOLDER,
					"review.confirmation.thank.you.title");

			return REDIRECT_PREFIX + productModelUrlResolver.resolve(productModel);
		}
		else
		{
			return FORWARD_TO_404;
		}
	}

	@RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/futureStock", method = RequestMethod.GET)
	public String productFutureStock(@PathVariable("productCode") final String productCode, final Model model,
			final HttpServletRequest request, final HttpServletResponse response) throws CMSItemNotFoundException
	{
		if (wileyProductVisibilityFacade.isProductVisible(productCode))
		{
			final boolean futureStockEnabled = Config.getBoolean(FUTURE_STOCK_ENABLED, false);
			if (futureStockEnabled)
			{
				final List<FutureStockData> futureStockList = futureStockFacade.getFutureAvailability(productCode);
				if (futureStockList == null)
				{
					GlobalMessages.addErrorMessage(model, STOCK_SERVICE_UNAVAILABLE);
				}
				else if (futureStockList.isEmpty())
				{
					GlobalMessages.addInfoMessage(model, "product.product.details.future.nostock");
				}

				final ProductModel productModel = productService.getProductForCode(productCode);
				populateProductDetailForDisplay(productModel, model, request, Collections.EMPTY_LIST);
				model.addAttribute("futureStocks", futureStockList);

				return ControllerConstants.Views.Fragments.Product.FUTURE_STOCK_POPUP;
			}
			else
			{
				return ControllerConstants.Views.Pages.Error.ERROR_NOT_FOUND_PAGE;
			}
		}
		else
		{
			return FORWARD_TO_404;
		}
	}

	@ResponseBody
	@RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/grid/skusFutureStock", method =
			{ RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_VALUE)
	public final Map<String, Object> productSkusFutureStock(final FutureStockForm form, final Model model)
	{

		final String productCode = form.getProductCode();
		if (wileyProductVisibilityFacade.isProductVisible(productCode))
		{
			final List<String> skus = form.getSkus();
			final boolean futureStockEnabled = Config.getBoolean(FUTURE_STOCK_ENABLED, false);

			Map<String, Object> result = new HashMap<>();
			if (futureStockEnabled && CollectionUtils.isNotEmpty(skus) && StringUtils.isNotBlank(productCode))
			{
				final Map<String, List<FutureStockData>> futureStockData = futureStockFacade
						.getFutureAvailabilityForSelectedVariants(productCode, skus);

				if (futureStockData == null)
				{
					// future availability service is down, we show this to the user
					result = Maps.newHashMap();
					final String errorMessage = getMessageSource().getMessage(NOT_MULTISKU_ITEM_ERROR, null,
							getI18nService().getCurrentLocale());
					result.put(NOT_MULTISKU_ITEM_ERROR, errorMessage);
				}
				else
				{
					for (final Map.Entry<String, List<FutureStockData>> entry : futureStockData.entrySet())
					{
						result.put(entry.getKey(), entry.getValue());
					}
				}
			}
			return result;
		}
		else
		{
			return Collections.emptyMap();
		}
	}

	@RequestMapping(value = "/relatedProducts", method = RequestMethod.GET)
	public String relatedProducts(@RequestParam(value = "productCode") final List<String> productCodes, final Model model)
			throws CMSItemNotFoundException
	{
		final Integer maximumNumberProducts = configurationService.getConfiguration().getInteger(
				CAROUSEL_MAX_NUMBER_OF_PRODUCTS_MESSAGE_CODE,
				CAROUSEL_MAX_NUMBER_OF_PRODUCTS_DEFAULT_VALUE);

		final Boolean displayProductTitles = configurationService.getConfiguration().getBoolean(
				CAROUSEL_DISPLAY_PRODUCT_TITLES_MESSAGE_CODE, CAROUSEL_DISPLAY_PRODUCT_TITLES_DEFAULT_VALUE);
		final Boolean displayProductAuthors = configurationService.getConfiguration().getBoolean(
				CAROUSEL_DISPLAY_PRODUCT_AUTHORS_MESSAGE_CODE, CAROUSEL_DISPLAY_PRODUCT_AUTHORS_DEFAULT_VALUE);
		final Boolean displayProductPrices = configurationService.getConfiguration().getBoolean(
				CAROUSEL_DISPLAY_PRODUCT_PRICES_MESSAGE_CODE, CAROUSEL_DISPLAY_PRODUCT_PRICES_DEFAULT_VALUE);

		final String referenceType = configurationService.getConfiguration().getString(REFERENCE_TYPE_MESSAGE_CODE,
				REFERENCE_TYPE_DEFAULT_VALUE);

		final List<ProductReferenceTypeEnum> productReferenceTypes = Collections.singletonList(
				ProductReferenceTypeEnum.valueOf(referenceType));

		final List<ProductReferenceData> productReferences = wileyb2cProductFacade.getProductReferencesForCodes(productCodes,
				productReferenceTypes, false, maximumNumberProducts);

		model.addAttribute("maximumNumberProducts", maximumNumberProducts);
		model.addAttribute("displayProductTitles", displayProductTitles);
		model.addAttribute("displayProductAuthors", displayProductAuthors);
		model.addAttribute("displayProductPrices", displayProductPrices);
		model.addAttribute("productReferences", productReferences);

		return ControllerConstants.Views.Pages.Product.RELATED_PRODUCTS_CONTENT_RESULTS;
	}

	@ExceptionHandler(UnknownIdentifierException.class)
	public String handleUnknownIdentifierException(final UnknownIdentifierException exception, final HttpServletRequest request)
	{
		return handleNotFoundCase(exception, request);
	}

	@ExceptionHandler(CMSItemNotFoundException.class)
	public String handleCMSItemNotFoundException(final CMSItemNotFoundException exception, final HttpServletRequest request)
	{
		return handleNotFoundCase(exception, request);
	}

	private String handleNotFoundCase(final Exception exception, final HttpServletRequest request)
	{
		request.setAttribute("message", exception.getMessage());
		return FORWARD_TO_404;
	}

	protected void updatePageTitle(final String title, final ProductModel productModel, final Model model)
	{
		storeContentPageTitleInModel(model, wileyb2cPageTitleResolver.resolveProductPageTitle(title, productModel));
	}

	protected void populateProductDetailForDisplay(final ProductModel productModel, final Model model,
			final HttpServletRequest request, final List<ProductOption> extraOptions) throws CMSItemNotFoundException
	{
		getRequestContextData(request).setProduct(productModel);

		List<ProductOption> options;

		if (configurationService.getConfiguration().
				getBoolean("wileyb2cstorefront.filtered.populators.enabled", false))
		{
			options = new ArrayList<>(
					Arrays.asList(
							ProductOption.BASIC,
							ProductOption.URL,
							ProductOption.SUMMARY,
							ProductOption.DESCRIPTION,
							ProductOption.GALLERY,
							ProductOption.CATEGORIES,
							ProductOption.REVIEW,
							ProductOption.CLASSIFICATION,
							ProductOption.VARIANT_FULL,
							ProductOption.VOLUME_PRICES,
							ProductOption.DELIVERY_MODE_AVAILABILITY,
							ProductOption.DOES_PRODUCT_HAVE_QUANTITY,
							ProductOption.B2C_PRODUCT_TABS,
							ProductOption.PURCHASE_OPTION,
							ProductOption.COUNTABLE
					));
		}
		else
		{
			options = new ArrayList<>(
					Arrays.asList(ProductOption.BASIC,
							ProductOption.URL, ProductOption.SUMMARY, ProductOption.DESCRIPTION,
							ProductOption.GALLERY,
							ProductOption.CATEGORIES, ProductOption.REVIEW,
							ProductOption.PROMOTIONS,
							ProductOption.CLASSIFICATION,
							ProductOption.VARIANT_FULL, ProductOption.VOLUME_PRICES,
							ProductOption.PRICE_RANGE,
							ProductOption.DELIVERY_MODE_AVAILABILITY, ProductOption.DOES_PRODUCT_HAVE_QUANTITY,
							ProductOption.B2C_PRODUCT_TABS, ProductOption.PURCHASE_OPTION,
							ProductOption.COUNTABLE));
		}

		final List<ProductOption> nonCacheableOptions = new ArrayList<>(
				Arrays.asList(
						ProductOption.STOCK,
						ProductOption.PRICE,
						ProductOption.AVAILABLE
				));
        
		options.addAll(extraOptions);

		long start = System.currentTimeMillis();

		ProductData productData = wileyb2cProductFacade.getProductForCodeAndOptionsCached(productModel, options);

		productData = wileyb2cProductFacade.getProductDataForCodeAndOptions(productModel, productData, nonCacheableOptions);

		long finish = System.currentTimeMillis();
		LOG.debug("Product's populators elapsed time:" + (finish - start));

		sortVariantOptionData(productData);
		storeCmsPageInModel(model, getPageForProduct(productModel));
		populateProductData(productData, model);
		model.addAttribute(WebConstants.BREADCRUMBS_KEY, productBreadcrumbBuilder.getBreadcrumbs(productModel.getCode()));

		if (CollectionUtils.isNotEmpty(productData.getVariantMatrix()))
		{
			model.addAttribute(WebConstants.MULTI_DIMENSIONAL_PRODUCT,
					Boolean.valueOf(CollectionUtils.isNotEmpty(productData.getVariantMatrix())));
		}

		if (productModel instanceof WileyPurchaseOptionProductModel)
		{
			final String defaultImage = getDefaultImageLink(request);
			final String jsonLd = wileyJsonLdProductFacade
					.createJsonLdFromProduct(productData,
							(WileyPurchaseOptionProductModel) productModel,
							request.getRequestURL().toString(), defaultImage);
			model.addAttribute("jsonLdScript", jsonLd);
		}
	}

	private String getDefaultImageLink(final HttpServletRequest request)
	{
		final String url = String.format("%s://%s:%d", request.getScheme(), request.getServerName(), request.getServerPort());
		final String image = storefrontResourceBundleSource.getMessage(DEFAULT_IMAGE, null, request.getLocale());
		final String originalContextPath = (String) request.getAttribute("originalContextPath");
		if (StringUtils.isNotEmpty(originalContextPath))
		{
			return url + originalContextPath + image;
		}
		else
		{
			return url + image;
		}
	}

	protected void populateProductData(final ProductData productData, final Model model)
	{
		model.addAttribute("galleryImages", getGalleryImages(productData));
		model.addAttribute("backgroundImages", getBackgroundImages(productData));
		model.addAttribute(MODEL_PRODUCT, productData);
	}

	protected void sortVariantOptionData(final ProductData productData)
	{
		if (CollectionUtils.isNotEmpty(productData.getBaseOptions()))
		{
			for (final BaseOptionData baseOptionData : productData.getBaseOptions())
			{
				if (CollectionUtils.isNotEmpty(baseOptionData.getOptions()))
				{
					Collections.sort(baseOptionData.getOptions(), variantSortStrategy);
				}
			}
		}
	}

	protected List<Map<String, ImageData>> getGalleryImages(final ProductData productData)
	{
		final List<Map<String, ImageData>> galleryImages = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(productData.getImages()))
		{
			final List<ImageData> images = new ArrayList<>();
			for (final ImageData image : productData.getImages())
			{
				if (ImageDataType.GALLERY.equals(image.getImageType()))
				{
					images.add(image);
				}
			}
			Collections.sort(images, new Comparator<ImageData>()
			{
				@Override
				public int compare(final ImageData image1, final ImageData image2)
				{
					return image1.getGalleryIndex().compareTo(image2.getGalleryIndex());
				}
			});

			if (CollectionUtils.isNotEmpty(images))
			{
				int currentIndex = images.get(0).getGalleryIndex().intValue();
				Map<String, ImageData> formats = new HashMap<String, ImageData>();
				for (final ImageData image : images)
				{
					if (currentIndex != image.getGalleryIndex().intValue())
					{
						galleryImages.add(formats);
						formats = new HashMap<>();
						currentIndex = image.getGalleryIndex().intValue();
					}
					formats.put(image.getFormat(), image);
				}
				if (!formats.isEmpty())
				{
					galleryImages.add(formats);
				}
			}
		}
		return galleryImages;
	}

	protected Map<String, ImageData> getBackgroundImages(final ProductData productData)
	{
		final Map<String, ImageData> formats = new HashMap();
		final List<ImageData> backgroundImages = productData.getBackgroundImages();
		if (CollectionUtils.isNotEmpty(backgroundImages))
		{
			for (final ImageData image : backgroundImages)
			{
				formats.put(image.getFormat(), image);
			}
		}
		return formats;
	}

	private String getRedirection(final ProductModel productModel,
			final HttpServletRequest request, final HttpServletResponse response)
			throws UnsupportedEncodingException
	{
		String redirection = getVariantRedirection(productModel);
		if (StringUtils.isEmpty(redirection))
		{
			redirection = checkRequestUrl(request, response, productModelUrlResolver.resolve(productModel));
		}
		if (StringUtils.isEmpty(redirection))
		{
			redirection = WileyRedirectUrlResolverUtil.getLocalizedRedirect(request);
		}
		return redirection;
	}

	private String getVariantRedirection(final ProductModel productModel)
	{
		String redirection = null;
		if (!(productModel instanceof VariantProductModel))
		{
			final ProductModel firstVariant = wileyb2cPurchaseOptionService.getFirstPurchaseOptionVariant(productModel);
			if (firstVariant != null)
			{
				redirection = "redirect:" + productModelUrlResolver.resolve(firstVariant);
			}
			else
			{
				redirection = FORWARD_TO_404;
			}
		}
		return redirection;
	}

	protected ReviewValidator getReviewValidator()
	{
		return reviewValidator;
	}

	protected AbstractPageModel getPageForProduct(final ProductModel product) throws CMSItemNotFoundException
	{
		return cmsPageService.getPageForProduct(product);
	}

	protected Map<String, String> getOpenGraphNamespaces()
	{
		Map<String, String> namespaces = new HashMap<>();
		namespaces.put("books", "http://ogp.me/ns/books#");
		namespaces.put("fb", "http://ogp.me/ns/fb#");
		namespaces.put("og", "http://ogp.me/ns#");
		return namespaces;
	}

	protected void setUpOpenGraphMetaTags(final Model model, final HttpServletRequest request, final String seoDescription)
	{
		final ProductData productData = (ProductData) model.asMap().get(MODEL_PRODUCT);
		final List<MetaElementData> metadata = new LinkedList<>();

		addMetaTagIfValueExist(metadata, "og:title", productData.getName());
		addMetaTagIfValueExist(metadata, "og:type", "books.book");
		addMetaTagIfValueExist(metadata, "og:image", getProductImageUrl(productData.getImages(), request.getLocale()));
		addMetaTagIfValueExist(metadata, "og:url",
				siteBaseUrlResolutionService.getWebsiteUrlForSite(
						cmsSiteService.getCurrentSite(),
						true,
						productData.getUrl()));
		addMetaTagIfValueExist(metadata, "books:isbn", productData.getIsbn13());
		addMetaTagIfValueExist(metadata, "books:release_date", productData.getPublicationDate());
		addMetaTagIfValueExist(metadata, "og:description", wileyb2cValueParser.parseDescription(productData, seoDescription));
		addMetaTagIfValueExist(metadata, "og:site_name", "Wiley.com");

		model.addAttribute(MODEL_PREFIXES, getOpenGraphNamespaces());
		model.addAttribute(MODEL_METATAGS, metadata);
	}

	protected String getProductImageUrl(final Collection<ImageData> productImages, final Locale locale)
	{
		final CMSSiteModel currentSite = cmsSiteService.getCurrentSite();
		String url = null;

		if (CollectionUtils.isNotEmpty(productImages))
		{
			url = productImages.iterator().next().getUrl();
		}
		else
		{
			url = storefrontResourceBundleSource.getMessage(DEFAULT_IMAGE, null, locale);
		}

		return siteBaseUrlResolutionService.getMediaUrlForSite(currentSite, true, url);
	}

	protected void addMetaTagIfValueExist(final List<MetaElementData> metadata, final String tagKey, final String tagData)
	{
		if (StringUtils.isNotEmpty(tagData))
		{
			metadata.add(createMetaElement(tagKey, tagData));
		}
	}

	protected void addMetaTagIfValueExist(final List<MetaElementData> metadata, final String tagKey, final Date tagData)
	{
		if (tagData != null)
		{
			metadata.add(createMetaElement(tagKey, DATE_TO_ISO8601.get().format(tagData)));
		}
	}

	@ModelAttribute("sessionCountry")
	public CountryData getSessionCountry()
	{
		return wileycomStoreSessionFacade.getSessionCountry();
	}

	@ModelAttribute("sessionCurrency")
	public CurrencyData getSessionCurrency()
	{
		return storeSessionFacade.getCurrentCurrency();
	}

	@ModelAttribute("optionalTaxShortMessage")
	public String getOptionalTaxShortMessage()
	{
		return wileyb2cI18NService.getCurrentCountryTaxShortMsg().orElse("");
	}

	@ModelAttribute("optionalTaxTooltip")
	public String getOptionalTaxTooltip()
	{
		return wileyb2cI18NService.getCurrentCountryTaxTooltip().orElse("");
	}

	@ModelAttribute("publicationDatePattern")
	public String getSessionPublicationDateFormat()
	{
		return wileyb2cI18NService.getCurrentDateFormat();
	}
}
