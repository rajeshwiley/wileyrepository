package com.wiley.wileyb2c.storefront.util.impl;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import javax.annotation.Nonnull;

import org.apache.commons.lang.StringUtils;

import com.wiley.facades.product.util.ProductInfo;
import com.wiley.wileyb2c.storefront.util.Wileyb2cValueParser;


/**
 * Created by Aliaksei_Zlobich on 9/7/2016.
 */
public class Wileyb2cValueParserImpl implements Wileyb2cValueParser
{

	public static final String KEYS_AND_VALUES_PATTERN = "([^:|]+:[^:|]+)(\\|[^:|]+:[^:|]+)*";
	public static final String PRODUCTS_VALUES_PATTERN = "([^:|]+:[^:|]+(:[^:|]+)?)(\\|[^:|]+:[^:|]+(:[^:|]+)?)*";
	//This regex expression covers next points:
	//	selected all html tags into description;
	//	selected all '&quot;' words;
	//	selected " and ' characters.
	private static final String DESCRIPTION_PATTERN =
			"<\\/?\\w+((\\s+\\w+(\\s*=\\s*(?:\".*?\"|'.*?'|[\\^'\">\\s]+))?)+\\s*|\\s*)\\/?>|\\\"+|\\'+|&[a-zA-Z]+;";
	public static final String STRICT_KEYS_AND_VALUES_PATTERN = "^" + KEYS_AND_VALUES_PATTERN + "$";
	public static final String STRICT_PRODUCTS_PATTERN = "^" + PRODUCTS_VALUES_PATTERN + "$";

	private final Pattern keysAndValuesPattern = Pattern.compile(STRICT_KEYS_AND_VALUES_PATTERN);
	private final Pattern productsPattern = Pattern.compile(STRICT_PRODUCTS_PATTERN);
	private final Pattern numberPattern = Pattern.compile("^\\d+$");

	@Nonnull
	@Override
	public Map<String, String> parseKeyValues(@Nonnull final String inputString)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("inputString", inputString);

		final String trimmedInpuString = inputString.trim();
		if (!keysAndValuesPattern.matcher(trimmedInpuString).matches())
		{
			throw new IllegalArgumentException(
					String.format("Wrong pattern of input string [%s]. Expected [%s]", trimmedInpuString,
							STRICT_KEYS_AND_VALUES_PATTERN));
		}

		final String[] keyValuePairs = trimmedInpuString.split("\\|");

		Map<String, String> result = new HashMap<>(keyValuePairs.length);

		for (String keyValuePair : keyValuePairs)
		{
			final String[] keyAndValue = keyValuePair.split(":");
			result.put(keyAndValue[0], keyAndValue[1]);
		}

		return result;
	}

	@Nonnull
	@Override
	public Map<String, ProductInfo> parseProducts(@Nonnull final String inputString)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("inputString", inputString);

		final String trimmedInpuString = inputString.trim();
		if (!productsPattern.matcher(trimmedInpuString).matches())
		{
			throw new IllegalArgumentException(
					String.format("Wrong pattern of products string [%s]. Expected [%s]", trimmedInpuString,
							STRICT_PRODUCTS_PATTERN));
		}

		final String[] singleProductInfos = trimmedInpuString.split("\\|");

		Map<String, ProductInfo> result = new HashMap<>(singleProductInfos.length);

		for (String singleProductInfo : singleProductInfos)
		{
			final String[] splitArray = singleProductInfo.split(":");

			if (splitArray.length != 2 && splitArray.length != 3)
			{
				throw new IllegalArgumentException(
						String.format("Unexpected format for input product string [%s]", singleProductInfo));
			}

			final String quantityValue = splitArray[1];
			if (!numberPattern.matcher(quantityValue).matches())
			{
				throw new IllegalArgumentException(
						String.format("Quantity should be in number format, but was [%s]", quantityValue));
			}

			final long quantity = Long.parseLong(quantityValue);

			if (splitArray.length == 2)
			{

				result.put(splitArray[0], new ProductInfo(quantity));
			}
			else
			{
				result.put(splitArray[0], new ProductInfo(quantity, splitArray[2]));

			}
		}

		return result;
	}

	@Nonnull
	@Override
	public String parseDescription(@Nonnull final ProductData productData, final String seoDescription)
	{
		if (StringUtils.isNotEmpty(seoDescription))
		{
			return seoDescription;
		}
		if (StringUtils.isNotEmpty(productData.getDescription()))
		{
			return productData.getDescription().replaceAll(DESCRIPTION_PATTERN, "").trim();
		}
		return "View more on Wiley.com";
	}
}
