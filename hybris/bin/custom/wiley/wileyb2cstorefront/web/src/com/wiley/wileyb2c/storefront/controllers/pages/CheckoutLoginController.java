/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wileyb2c.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.GuestForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.GuestValidator;
import de.hybris.platform.acceleratorstorefrontcommons.security.GUIDCookieStrategy;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.facades.wileyb2c.order.Wileyb2cCheckoutFacade;
import com.wiley.wileyb2c.storefront.controllers.ControllerConstants;


/**
 * Checkout Login Controller. Handles login for the checkout flow.
 */
@Controller
@Scope("tenant")
@RequestMapping(value = "/login/checkout")
@Profile(WileyCoreConstants.WILEY_PHASE2A_PROFILE_NAME)
public class CheckoutLoginController extends AbstractCheckoutLoginController
{
	@Resource(name = "guidCookieStrategy")
	private GUIDCookieStrategy guidCookieStrategy;

	@Resource(name = "authenticationManager")
	private AuthenticationManager authenticationManager;

	@Resource(name = "guestValidator")
	private GuestValidator guestValidator;

	@Resource(name = "wileyb2cCheckoutFacade")
	private Wileyb2cCheckoutFacade wileyb2cCheckoutFacade;

	@Override
	protected AbstractPageModel getCmsPage() throws CMSItemNotFoundException
	{
		return getContentPageForLabelOrId("checkout-login");
	}

	@RequestMapping(method = RequestMethod.GET)
	@Override
	public String doCheckoutLogin(@RequestParam(value = "error", defaultValue = "false") final boolean loginError,
			final HttpSession session, final Model model, final HttpServletRequest request) throws CMSItemNotFoundException
	{
		return super.doCheckoutLogin(loginError, session, model, request);
	}

	@RequestMapping(value = "/guest", method = RequestMethod.POST)
	public String doAnonymousCheckout(final GuestForm form, final BindingResult bindingResult, final Model model,
			final HttpSession session, final HttpServletRequest request, final HttpServletResponse response)
					throws CMSItemNotFoundException
	{
		getGuestValidator().validate(form, bindingResult);
		final boolean suppressGuestCheckout = wileyCheckoutFacade.hasSessionCartDigitalProduct();
		if (suppressGuestCheckout)
		{
			GlobalMessages.addErrorMessage(model, "guest.checkout.forbidden.digital");
			return REDIRECT_PREFIX + "/cart";
		}

		final boolean isRegisteredUser = wileyb2cCheckoutFacade.isRegisteredUser(form.getEmail());
		if (isRegisteredUser)
		{
			bindingResult.rejectValue("email", "profile.email.invalid");
		}
		return processAnonymousCheckoutUserRequest(form, bindingResult, model, request, response);
	}

	@RequestMapping(value = "/guest", method = RequestMethod.GET)
	public String doAnonymousCheckout(@RequestParam(value = "error", defaultValue = "false") final boolean loginError,
			final HttpSession session, final Model model, final HttpServletRequest request) throws CMSItemNotFoundException
	{
		return doCheckoutLogin(loginError, session, model, request);
	}

	@Override
	protected String getView()
	{
		return ControllerConstants.Views.Pages.Checkout.CHECKOUT_LOGIN_PAGE;
	}

	protected GuestValidator getGuestValidator()
	{
		return guestValidator;
	}

	@Override
	protected GUIDCookieStrategy getGuidCookieStrategy()
	{
		return guidCookieStrategy;
	}

	protected AuthenticationManager getAuthenticationManager()
	{
		return authenticationManager;
	}
}
