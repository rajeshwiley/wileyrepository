package com.wiley.wileyb2c.storefront.util;

import de.hybris.platform.commercefacades.product.data.ProductData;

import java.util.Map;

import javax.annotation.Nonnull;

import com.wiley.facades.product.util.ProductInfo;


/**
 * Created by Aliaksei_Zlobich on 9/7/2016.
 */
public interface Wileyb2cValueParser
{

	@Nonnull
	Map<String, String> parseKeyValues(@Nonnull String inputString);

	@Nonnull
	Map<String, ProductInfo> parseProducts(@Nonnull String inputString);

	@Nonnull
	String parseDescription(@Nonnull ProductData productData, String  seoDescription);
}
