package com.wiley.wileyb2c.storefront.controllers.cms;

import de.hybris.platform.cms2.servicelayer.services.impl.DefaultCMSSiteService;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.wiley.core.model.Wileyb2cHomeLinkComponentModel;
import com.wiley.wileyb2c.storefront.controllers.ControllerConstants;


/**
 * @author Dzmitryi_Halahayeu
 */
@Controller("Wileyb2cHomeLinkComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.WILEY_B2C_HOME_LINK_COMPONENT)
public class Wileyb2cHomeLinkComponentController extends
		AbstractWileyb2cLinkWithDefaultValueComponentController<Wileyb2cHomeLinkComponentModel>
{
	@Resource
	private DefaultCMSSiteService cmsSiteService;

	protected String getUrlText()
	{
		return cmsSiteService.getCurrentSite().getHomeText();
	}

	protected String getUrlValue()
	{
		return cmsSiteService.getCurrentSite().getRedirectURL();
	}

}
