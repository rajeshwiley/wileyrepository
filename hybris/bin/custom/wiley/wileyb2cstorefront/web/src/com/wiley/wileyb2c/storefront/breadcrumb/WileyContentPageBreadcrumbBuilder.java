package com.wiley.wileyb2c.storefront.breadcrumb;

import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.ContentPageBreadcrumbBuilder;
import de.hybris.platform.cms2.model.pages.ContentPageModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;
import static org.apache.logging.log4j.util.Strings.EMPTY;


public class WileyContentPageBreadcrumbBuilder extends ContentPageBreadcrumbBuilder
{
	private static final String LAST_LINK_CLASS = "active";
	private static final String LAST_LINK_URL = "#";

	@Override
	public List<Breadcrumb> getBreadcrumbs(final ContentPageModel page)
	{
		validateParameterNotNull(page, "page mustn't be null");
		final List<ContentPageModel> pages = buildPagesHierarchy(page);
		final List<Breadcrumb> breadcrumbs = buildBreadcrumbs(pages);
		Collections.reverse(breadcrumbs);
		return breadcrumbs;
	}

	private List<Breadcrumb> buildBreadcrumbs(final List<ContentPageModel> pages)
	{
		final List<Breadcrumb> breadcrumbs = new ArrayList<>();
		final ContentPageModel firstPage = pages.get(0);
		breadcrumbs.add(new Breadcrumb(LAST_LINK_URL, getBreadcrumbName(firstPage), LAST_LINK_CLASS));
		pages.stream()
				.skip(1)
				.map(page -> new Breadcrumb(page.getLabel(), getBreadcrumbName(page), EMPTY))
				.collect(Collectors.toCollection(() -> breadcrumbs));
		return breadcrumbs;
	}

	private String getBreadcrumbName(final ContentPageModel page)
	{
		String breadcrumbName = page.getTitle();
		if (breadcrumbName == null)
		{
			breadcrumbName = page.getName();
		}
		return breadcrumbName;
	}

	private List<ContentPageModel> buildPagesHierarchy(final ContentPageModel page)
	{
		final List<ContentPageModel> pages = new ArrayList<>();
		ContentPageModel currentPage = page;
		while (currentPage != null && !pages.contains(currentPage))
		{
			pages.add(currentPage);
			currentPage = currentPage.getParentPage();
		}
		return pages;
	}
}
