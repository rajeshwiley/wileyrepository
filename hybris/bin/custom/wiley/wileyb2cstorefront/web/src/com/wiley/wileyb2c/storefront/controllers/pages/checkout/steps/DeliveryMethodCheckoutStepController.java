/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wileyb2c.storefront.controllers.pages.checkout.steps;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.data.CartData;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.facades.order.WileycomCheckoutFacade;
import com.wiley.facades.wileyb2c.order.Wileyb2cCheckoutFacade;
import com.wiley.wileyb2c.storefront.controllers.ControllerConstants;
import com.wiley.wileycom.storefrontcommons.controllers.pages.checkout.steps.AbstractWileycomDeliveryMethodCheckoutStepController;


@Controller
@Profile(WileyCoreConstants.WILEY_PHASE2A_PROFILE_NAME)
public class DeliveryMethodCheckoutStepController extends AbstractWileycomDeliveryMethodCheckoutStepController
{
	@Resource
	private WileycomCheckoutFacade wileycomCheckoutFacade;

	@Resource(name = "wileyb2cCheckoutFacade")
	private Wileyb2cCheckoutFacade wileyb2cCheckoutFacade;

	@RequestMapping(value = "/choose", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	@PreValidateCheckoutStep(checkoutStep = DELIVERY_METHOD)
	public String enterStep(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		prepareDataForPage(model);
		final CartData cartData = wileycomCheckoutFacade.getCheckoutCart();
		wileyb2cCheckoutFacade.doRealTimeInventoryCheck(cartData);
		model.addAttribute("cartData", cartData);
		return com.wiley.wileycom.storefrontcommons.controllers.
				ControllerConstants.Views.Pages.MultiStepCheckout.CHOOSE_DELIVERY_METHOD_PAGE;
	}

	@RequestMapping(value = "/select-refresh-order-total", method = RequestMethod.POST)
	@RequireHardLogIn
	public String doOrderTotalRefresh(@RequestParam("selectedDeliveryMethod") final String selectedDeliveryMethod,
			final Model model)
	{
		updateDeliveryMode(selectedDeliveryMethod); // should be invoked before cartData populated
		// todo (improvement) need to change the interface (param 'supportedDeliveryModes' is not needed anymore)
		wileycomCheckoutFacade.setDeliveryModeIfAvailable(null);

		final CartData cartData = wileycomCheckoutFacade.getCheckoutCart();
		wileyb2cCheckoutFacade.doRealTimeInventoryCheck(cartData);
		model.addAttribute("cartData", cartData);

		return ControllerConstants.Views.AjaxPages.SELECT_REFRESH_ORDER_TOTAL;
	}

	@Override
	protected void updateDeliveryMode(final String selectedDeliveryMethod)
	{
		if (StringUtils.isNotEmpty(selectedDeliveryMethod))
		{
			if (wileycomCheckoutFacade.isDeliveryModeAlreadySetInCart()
					&& !wileycomCheckoutFacade.isDeliveryModeSetInCartTheSameThan(selectedDeliveryMethod))
			{
				wileyb2cCheckoutFacade.removePaypalPaymentInformationFromSessionCart();
			}
			wileycomCheckoutFacade.setDeliveryMode(selectedDeliveryMethod);
		}
	}
}
