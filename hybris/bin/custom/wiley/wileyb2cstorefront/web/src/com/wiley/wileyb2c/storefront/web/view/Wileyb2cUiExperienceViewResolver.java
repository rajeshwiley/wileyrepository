package com.wiley.wileyb2c.storefront.web.view;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.AbstractController;

import java.util.Locale;

import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.AbstractView;

import com.wiley.storefrontcommons.web.view.WileyUiExperienceViewResolver;


public class Wileyb2cUiExperienceViewResolver extends WileyUiExperienceViewResolver
{
	@Override
	protected View createView(final String viewName, final Locale locale) throws Exception
	{
		if (!this.canHandle(viewName, locale))
		{
			return null;
		}
		else
		{
			if (viewName.startsWith("redirect:"))
			{
				String forwardUrl = viewName.substring("redirect:".length());
				if (AbstractController.ROOT.equals(forwardUrl))
				{
					HomePageRedirectView view = new HomePageRedirectView(forwardUrl, this.isRedirectContextRelative(),
							this.isRedirectHttp10Compatible());
					return this.applyLifecycleMethods(viewName, view);
				}
			}
			return super.createView(viewName, locale);
		}
	}

	private View applyLifecycleMethods(final String viewName, final AbstractView view)
	{
		return (View) this.getApplicationContext().getAutowireCapableBeanFactory().initializeBean(view, viewName);
	}
}
