package com.wiley.wileyb2c.storefront.controllers.cms;

import de.hybris.platform.acceleratorfacades.device.ResponsiveMediaFacade;
import de.hybris.platform.cms2.servicelayer.services.impl.DefaultCMSSiteService;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.core.model.media.MediaContainerModel;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.wiley.core.model.Wileyb2cSiteLogoComponentModel;
import com.wiley.wileyb2c.storefront.controllers.ControllerConstants;

/**
 * @author Dzmitryi_Halahayeu
 */
@Controller("Wileyb2cSiteLogoComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.WILEY_B2C_SITE_LOGO_COMPONENT)
public class Wileyb2cSiteLogoComponentContoller extends
		AbstractCMSComponentController<Wileyb2cSiteLogoComponentModel>
{
	@Resource
	private DefaultCMSSiteService cmsSiteService;

	@Resource(name = "responsiveMediaFacade")
	private ResponsiveMediaFacade responsiveMediaFacade;

	@Resource(name = "commerceCommonI18NService")
	private CommerceCommonI18NService commerceCommonI18NService;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model,
			final Wileyb2cSiteLogoComponentModel component)
	{
		//logo for legacy sites support
		MediaContainerModel logoImagesForSite = cmsSiteService.getCurrentSite().getResponsiveLogoImages();
		
		final List<ImageData> mediaDataList = responsiveMediaFacade.getImagesFromMediaContainer(
				logoImagesForSite != null ? logoImagesForSite : component.getMedia(commerceCommonI18NService.getCurrentLocale()));
		model.addAttribute("medias", mediaDataList);

		String logoUrl = cmsSiteService.getCurrentSite().getRedirectURL();
		model.addAttribute("urlLink", StringUtils.isNotEmpty(logoUrl) ? logoUrl : component.getUrlLink());

	}
}