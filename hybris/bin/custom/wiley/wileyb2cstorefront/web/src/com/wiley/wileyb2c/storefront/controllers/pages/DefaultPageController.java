/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wileyb2c.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.ContentPageBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.storesession.StoreSessionFacade;
import de.hybris.platform.commercefacades.storesession.data.CurrencyData;
import de.hybris.platform.commercefacades.user.data.CountryData;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UrlPathHelper;

import com.wiley.core.enums.RobotsMetaTag;
import com.wiley.core.strategies.robotsmetatag.RobotsMetatagResolverService;
import com.wiley.core.wileyb2c.i18n.Wileyb2cI18NService;
import com.wiley.facades.wileycom.storesession.WileycomStoreSessionFacade;
import com.wiley.storefrontcommons.util.WileyRobotsTagContentMapper;
import com.wiley.wileyb2c.storefront.controllers.ControllerConstants;
import com.wiley.wileycom.storefrontcommons.util.WileyRedirectUrlResolverUtil;


/**
 * Error handler to show a CMS managed error page. This is the catch-all controller that handles all GET requests that
 * are not handled by other controllers.
 */
@Controller
@Scope("tenant")
//@RequestMapping()
public class DefaultPageController extends AbstractPageController
{
	private static final String ERROR_CMS_PAGE = "notFound";

	private final UrlPathHelper urlPathHelper = new UrlPathHelper();

	@Resource(name = "simpleBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder resourceBreadcrumbBuilder;

	@Resource(name = "contentPageBreadcrumbBuilder")
	private ContentPageBreadcrumbBuilder contentPageBreadcrumbBuilder;

	@Resource(name = "robotsMetatagResolverService")
	private RobotsMetatagResolverService robotsMetatagResolverService;

	@Resource
	private WileycomStoreSessionFacade wileyb2cStoreSessionFacade;

	@Resource
	private StoreSessionFacade storeSessionFacade;

	@Resource
	private Wileyb2cI18NService wileyb2cI18NService;


	@RequestMapping(method = RequestMethod.GET)
	public String get(final Model model, final HttpServletRequest request, final HttpServletResponse response)
			throws CMSItemNotFoundException
	{
		// Check for CMS Page where label or id is like /page
		final ContentPageModel pageForRequest = getContentPageForRequest(request);
		if (pageForRequest != null && pageForRequest.getMasterTemplate().getFrontendTemplateName() != null)
		{
			//redirect to url with locale if it is missed
			String redirection = WileyRedirectUrlResolverUtil.getLocalizedRedirect(request);
			if (StringUtils.isNotEmpty(redirection))
			{
				return redirection;
			}
			storeCmsPageInModel(model, pageForRequest);
			setUpMetaDataForContentPage(model, pageForRequest);
			model.addAttribute(WebConstants.BREADCRUMBS_KEY, contentPageBreadcrumbBuilder.getBreadcrumbs(pageForRequest));
			resolveRobotsMetatag(model, pageForRequest);
			return getViewForPage(pageForRequest);
		}
		ContentPageModel notFoundPage = getContentPageForLabelOrId(ERROR_CMS_PAGE);
		// No page found - display the notFound page with error from controller
		resolveRobotsMetatag(model, notFoundPage);
		storeCmsPageInModel(model, notFoundPage);
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ERROR_CMS_PAGE));

		model.addAttribute(WebConstants.MODEL_KEY_ADDITIONAL_BREADCRUMB,
				resourceBreadcrumbBuilder.getBreadcrumbs("breadcrumb.not.found"));

		response.setStatus(HttpServletResponse.SC_NOT_FOUND);

		return ControllerConstants.Views.Pages.Error.ERROR_NOT_FOUND_PAGE;
	}

	@ModelAttribute("sessionCountry")
	public CountryData getSessionCountry()
	{
		return wileyb2cStoreSessionFacade.getSessionCountry();
	}

	@ModelAttribute("sessionCurrency")
	public CurrencyData getSessionCurrency()
	{
		return storeSessionFacade.getCurrentCurrency();
	}

	/**
	 * Lookup the CMS Content Page for this request.
	 *
	 * @param request
	 *           The request
	 * @return the CMS content page
	 */
	protected ContentPageModel getContentPageForRequest(final HttpServletRequest request)
	{
		// Get the path for this request.
		// Note that the path begins with a '/'
		final String lookupPathForRequest = urlPathHelper.getLookupPathForRequest(request);

		try
		{
			// Lookup the CMS Content Page by label. Note that the label value must begin with a '/'.
			return getCmsPageService().getPageForLabel(lookupPathForRequest);
		}
		catch (final CMSItemNotFoundException ignore)
		{
			// Ignore exception
		}
		return null;
	}

	private void resolveRobotsMetatag(final Model model, final ContentPageModel page)
	{
		String robotsMetaTag = null;

		if (robotsMetatagResolverService.needResolveByPageType(page))
		{
			robotsMetaTag = robotsMetatagResolverService.resolveByPageType(page);
		}
		else if (robotsMetatagResolverService.needResolveByPageUid(page))
		{
			robotsMetaTag = robotsMetatagResolverService.resolveByPageUid(page.getUid());
		}

		if (robotsMetaTag != null)
		{
			model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS,
					WileyRobotsTagContentMapper.getContentForTag(RobotsMetaTag.valueOf(robotsMetaTag)));
		}
	}

	@ModelAttribute("optionalTaxShortMessage")
	public String getOptionalTaxShortMessage()
	{
		return wileyb2cI18NService.getCurrentCountryTaxShortMsg().orElse("");
	}

	@ModelAttribute("optionalTaxTooltip")
	public String getOptionalTaxTooltip()
	{
		return wileyb2cI18NService.getCurrentCountryTaxTooltip().orElse("");
	}

	@ModelAttribute("publicationDatePattern")
	public String getSessionPublicationDateFormat()
	{
		return wileyb2cI18NService.getCurrentDateFormat();
	}
}
