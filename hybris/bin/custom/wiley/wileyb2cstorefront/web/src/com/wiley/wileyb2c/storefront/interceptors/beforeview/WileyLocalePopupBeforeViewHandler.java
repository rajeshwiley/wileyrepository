package com.wiley.wileyb2c.storefront.interceptors.beforeview;

import de.hybris.platform.acceleratorstorefrontcommons.interceptors.BeforeViewHandler;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.wiley.facades.common.LocalePopupData;
import com.wiley.facades.locale.WileyLocaleFacade;
import com.wiley.storefrontcommons.locale.WileyLocaleHandler;
import com.wiley.storefrontcommons.security.cookie.EnhancedCookieGenerator;
import com.wiley.wileycom.storefrontcommons.filters.WileycomLocaleFilter;


public class WileyLocalePopupBeforeViewHandler implements BeforeViewHandler
{
	private static final Logger LOG = LoggerFactory.getLogger(WileycomLocaleFilter.class);

	private final WileyLocaleFacade wileyLocaleFacade;
	private final WileyLocaleHandler wileyLocaleFromRequestUrlHandler;
	private final WileyLocaleHandler wileyLocaleFromCookieHandler;
	private final WileyLocaleHandler wileyLocaleFromGeolocationHandler;
	private final EnhancedCookieGenerator wileyb2cCountryCookieGenerator;


	@Autowired
	public WileyLocalePopupBeforeViewHandler(final WileyLocaleFacade wileyLocaleFacade,
			@Qualifier("wileyLocaleFromRequestUrlHandler") final WileyLocaleHandler wileyLocaleFromRequestUrlHandler,
			@Qualifier("wileyb2cLocaleFromCookieHandler") final WileyLocaleHandler wileyLocaleFromCookieHandler,
			@Qualifier("wileyLocaleFromGeolocationHandler") final WileyLocaleHandler wileyLocaleFromGeolocationHandler,
			@Qualifier("wileyb2cCountryCookieGenerator") final EnhancedCookieGenerator wileyb2cCountryCookieGenerator)
	{
		this.wileyLocaleFacade = wileyLocaleFacade;
		this.wileyLocaleFromRequestUrlHandler = wileyLocaleFromRequestUrlHandler;
		this.wileyLocaleFromCookieHandler = wileyLocaleFromCookieHandler;
		this.wileyLocaleFromGeolocationHandler = wileyLocaleFromGeolocationHandler;
		this.wileyb2cCountryCookieGenerator = wileyb2cCountryCookieGenerator;
	}

	@Override
	public void beforeView(final HttpServletRequest request, final HttpServletResponse response, final ModelAndView modelAndView)
			throws Exception
	{
		if (RequestMethod.GET.name().equalsIgnoreCase(request.getMethod()))
		{
			final Optional<String> encodedLocaleFromRequest = wileyLocaleFromRequestUrlHandler.getEncodedLocale();
			final Optional<String> encodedLocaleFromCookie = wileyLocaleFromCookieHandler.getEncodedLocale();
			final Optional<String> encodedLocaleFromGeolocation = wileyLocaleFromGeolocationHandler.getEncodedLocale();

			boolean showLocalePopup = false;
			boolean isCurrentCountryGeodetected = false;
			String encodedLocale = null;

			if (encodedLocaleFromRequest.isPresent())
			{
				if (encodedLocaleFromCookie.isPresent() && !encodedLocaleFromRequest.equals(encodedLocaleFromCookie))
				{
					isCurrentCountryGeodetected = false;
					showLocalePopup = true;
					encodedLocale = encodedLocaleFromCookie.get();
				}
				else if (!encodedLocaleFromCookie.isPresent() && encodedLocaleFromGeolocation.isPresent()
						&& !encodedLocaleFromRequest.equals(encodedLocaleFromGeolocation))
				{
					isCurrentCountryGeodetected = true;
					showLocalePopup = true;
					encodedLocale = encodedLocaleFromGeolocation.get();
				}
			}

			modelAndView.addObject("showLocalePopup", showLocalePopup);
			if (showLocalePopup)
			{
				final LocalePopupData localePopupData = new LocalePopupData();
				localePopupData.setCurrentCountryGeodetected(isCurrentCountryGeodetected);
				populateCookiesDetails(localePopupData);
				populateEncodedLocaleDetails(encodedLocale, localePopupData);
				modelAndView.addObject("localePopupData", localePopupData);
			}
		}
	}

	protected void populateCookiesDetails(final LocalePopupData localePopupData)
	{
		localePopupData.setCookieCountryCookieName(wileyb2cCountryCookieGenerator.getCookieName());
		localePopupData.setCookieCountryCookiePath(wileyb2cCountryCookieGenerator.getCookiePath());
		localePopupData.setCookieCountryCookieMaxAge(wileyb2cCountryCookieGenerator.getCookieMaxAge());
	}

	protected void populateEncodedLocaleDetails(final String encodedLocale, final LocalePopupData localePopupData)
	{
		try
		{
			final String currentEncodedLocale = wileyLocaleFacade.getCurrentEncodedLocale();
			localePopupData.setCurrentLocale(currentEncodedLocale);
			localePopupData.setDetectedLocale(encodedLocale);
			localePopupData.setCurrentCountry(wileyLocaleFacade.getCountryForEncodedLocale(currentEncodedLocale));
			localePopupData.setDetectedCountry(wileyLocaleFacade.getCountryForEncodedLocale(encodedLocale));
		}
		catch (UnknownIdentifierException | IllegalArgumentException e)
		{
			LOG.debug(String.format("Invalid Encoded Locale for encodedLocale : [%s]", encodedLocale), e);
		}
	}
}
