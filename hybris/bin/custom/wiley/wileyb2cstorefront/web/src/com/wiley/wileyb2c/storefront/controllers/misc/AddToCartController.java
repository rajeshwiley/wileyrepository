/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wileyb2c.storefront.controllers.misc;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.AddToCartForm;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartModificationStatus;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.subscriptionfacades.data.SubscriptionTermData;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.subscription.exception.SubscriptionUpdateException;
import com.wiley.facades.product.util.ProductInfo;
import com.wiley.facades.voucher.WileyCouponFacade;
import com.wiley.facades.wileyb2c.order.Wileyb2cCartFacade;
import com.wiley.facades.wileyb2c.product.WileyCourseProductFacade;
import com.wiley.facades.wileyb2c.wileyplus.url.strategy.WileyplusPurchaseOptionsPageParseURLStrategy;
import com.wiley.facades.wileyb2c.wileyplus.url.strategy.exception.WileyplusChecksumMismatchException;
import com.wiley.facades.wileycom.storesession.WileycomStoreSessionFacade;
import com.wiley.facades.wileyplus.cart.activation.dto.CartActivationRequestDto;
import com.wiley.wileyb2c.storefront.forms.cart.ExternalAddToCartForm;
import com.wiley.wileyb2c.storefront.util.InventoryStatusUtil;
import com.wiley.wileyb2c.storefront.util.Wileyb2cValueParser;
import com.wiley.wileycom.storefrontcommons.controllers.WileycomAddToCartController;


/**
 * Controller for Add to Cart functionality which is not specific to a certain page.
 */
@Controller
@Profile(WileyCoreConstants.WILEY_PHASE2A_PROFILE_NAME)
public class AddToCartController extends WileycomAddToCartController
{
	private static final Logger LOG = Logger.getLogger(AddToCartController.class);

	@Resource
	private WileyCourseProductFacade wileyCourseProductFacade;

	@Resource
	private Wileyb2cCartFacade wileyb2cCartFacade;

	@Resource
	private WileyCouponFacade wileyCouponFacade;

	@Resource
	private WileycomStoreSessionFacade wileycomStoreSessionFacade;

	@Resource
	private Wileyb2cValueParser wileyb2cValueParser;

	@Resource
	private WileyplusPurchaseOptionsPageParseURLStrategy wileyplusPurchaseOptionsPageURLParser;

	private static final String PRODUCT_TYPE_ISBN = "ISBN";

	protected static final String CART_PAGE_URL = "/cart";
	private static final String REDIRECT_TO_CART = REDIRECT_PREFIX + CART_PAGE_URL;
	private static final int DEFAULT_SUBSCRIPTION_QUANTITY = 1;

	@RequestMapping(value = CART_PAGE_URL + "/put", method = RequestMethod.POST)
	public String putToCart(@RequestParam("productCodePost") final String code, @Valid final AddToCartForm form,
			final BindingResult bindingErrors, final RedirectAttributes redirectAttributes, final HttpServletRequest request)
	{
		return addProductToCart(code, form, bindingErrors, redirectAttributes, request);
	}

	/**
	 * Add to Cart subscription upgrade from internal pages.
	 */
	@RequestMapping(value = "/cart/add/subscription/{code}", method = RequestMethod.GET)
	@RequireHardLogIn
	public String updateSubscriptionOptions(@PathVariable("code") final String subscriptionCode,
			@RequestParam("selectedOptionId") final String selectedOptionId,
			@RequestParam("productCode") final String productCode, final RedirectAttributes redirectAttributes,
			final HttpServletRequest request) throws CMSItemNotFoundException
	{
		CartModificationData cartModification = null;
		try
		{
			cartModification = wileyb2cCartFacade.addToCartBySubscriptionCodeAndTermId(subscriptionCode, selectedOptionId);
		}
		catch (final SubscriptionUpdateException ex)
		{
			LOG.info(ex.getMessage(), ex);
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"account.subscription.manage.paymentUpdate.error");
			return REDIRECT_PREFIX + String.format("/my-account/subscriptions/%s", subscriptionCode);
		}
		catch (final CommerceCartModificationException ex)
		{
			handleCommerceCartModificationException(ex, redirectAttributes, productCode, request);
		}
		catch (final UnknownIdentifierException ex)
		{
			handleUnknownIdentifierException(ex, redirectAttributes, productCode);
		}

		if (cartModification != null)
		{
			addCartModificationMessages(redirectAttributes, DEFAULT_SUBSCRIPTION_QUANTITY, cartModification);
		}
		return REDIRECT_TO_CART;
	}

	/**
	 * Add to Cart functionality from external pages.
	 */
	@RequestMapping(value = "/ucart/add", method =
	{ RequestMethod.GET, RequestMethod.POST })
	public String addToCartExternal(@Valid final ExternalAddToCartForm externalAddToCartForm, final BindingResult bindingResult,
			final RedirectAttributes redirectAttributes, final HttpServletRequest request, final HttpServletResponse response)
	{

		// if form has errors, show cart page
		if (bindingResult.hasErrors())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER, "basket.error.occurred");
			return REDIRECT_TO_CART;
		}

		// process parameter cleanUpCart.
		cleanUpCart(externalAddToCartForm);

		// parseKeyValues product and extraInfo params
		final Map<String, ProductInfo> productInfoMap = wileyb2cValueParser.parseProducts(externalAddToCartForm.getProducts());

		String additionalInfo = externalAddToCartForm.getAdditionalInfo();

		// add products to cart
		for (final Map.Entry<String, ProductInfo> entry : productInfoMap.entrySet())
		{
			final ProductInfo productInfo = entry.getValue();
			final String isbn = entry.getKey();

			CartModificationData cartModification = null;
			try
			{
				cartModification = wileyb2cCartFacade.addToCartByIsbn(isbn, productInfo, additionalInfo);
			}
			catch (final CommerceCartModificationException ex)
			{
				handleCommerceCartModificationException(ex, redirectAttributes, isbn, request);
			}
			catch (final UnknownIdentifierException ex)
			{
				handleUnknownIdentifierException(ex, redirectAttributes, isbn);
			}

			if (cartModification != null)
			{
				addCartModificationMessages(redirectAttributes, productInfo.getQuantity(), cartModification);
			}
		}

		// save continue shopping url
		wileyb2cCartFacade.saveContinueUrl(externalAddToCartForm.getReturnURL());

		if (StringUtils.isNotEmpty(externalAddToCartForm.getPromoCode()))
		{
			applyVoucher(externalAddToCartForm.getPromoCode(), redirectAttributes);
		}

		return REDIRECT_TO_CART;
	}

	/**
	 * Add WileyCourse to Cart functionality. !!! Hybris hosted controller. Don't remove it !!!
	 */
	@RequestMapping(value = CART_PAGE_URL + "/add/course", method = RequestMethod.POST)
	public String addWileyCourseToCart(@RequestParam(value = "productCode", required = true) final String productCode,
			@RequestParam(value = "cartActivationInfo", required = true) final String cartActivationInfo,
			@RequestParam(value = "checksum", required = true) final String checksum,
			@RequestParam(value = "purchaseType", required = true) final String purchaseType,
			@RequestParam(value = "qty", required = false, defaultValue = "1") final long qty,
			final RedirectAttributes redirectAttributes, final HttpServletRequest request)
	{
		try
		{
			final CartActivationRequestDto activationDto = extractActivationDto(cartActivationInfo, checksum);
			addCourseToCart(productCode, redirectAttributes, qty, request, activationDto, purchaseType);
		}
		catch (final UnknownIdentifierException | WileyplusChecksumMismatchException | URISyntaxException ex)
		{
			LOG.error("Product not found while adding to cart. Error message: " + ex.getMessage(), ex);
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER, "basket.error.occurred");
		}

		return REDIRECT_TO_CART;
	}

	@Override
	protected void addCartModificationMessages(final RedirectAttributes redirectAttributes, final long qty,
			final CartModificationData cartModification)
	{
		super.addCartModificationMessages(redirectAttributes, qty, cartModification);

		addSubscriptionTermModifiedMessage(redirectAttributes, cartModification);

		InventoryStatusUtil.addMessageIfNonInStockItem(cartModification, redirectAttributes);
	}

	private void addSubscriptionTermModifiedMessage(final RedirectAttributes redirectAttributes,
			final CartModificationData cartModification)
	{
		final OrderEntryData entry = cartModification.getEntry();
		if (entry != null && CommerceCartModificationStatus.SUCCESS.equals(cartModification.getStatusCode()))
		{
			final SubscriptionTermData originalSubscriptionTerm = entry.getOriginalSubscriptionTerm();
			final SubscriptionTermData subscriptionTerm = entry.getSubscriptionTerm();
			final ProductData product = entry.getProduct();
			Assert.notNull(product);

			if (originalSubscriptionTerm != null)
			{
				Assert.notNull(subscriptionTerm);
				// if subscription term was changed for product - display info message
				GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER,
						"basket.validation.subscription.term.changed", new Object[]
				{ originalSubscriptionTerm.getName(), product.getName(), subscriptionTerm.getName() });
			}
		}
	}

	private void applyVoucher(final String promoCode, final RedirectAttributes redirectAttributes)
	{
		//Code below is commented in scope of migration to new coupons: ECSC-21150
		//final List<String> violationMessages = wileyVoucherFacade.tryToApplyVoucher(promoCode);
		final List<String> violationMessages = new ArrayList<>();

		if (!violationMessages.isEmpty())
		{
			violationMessages.forEach(
					message -> GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER, message));
		}
	}

	private void cleanUpCart(final ExternalAddToCartForm externalAddToCartForm)
	{
		if (Boolean.TRUE.equals(externalAddToCartForm.getCleanUpCart()))
		{
			wileyb2cCartFacade.removeSessionCart();
		}
	}

	protected String addCourseToCart(final String code, final RedirectAttributes redirectAttributes, final long qty,
			final HttpServletRequest request, final CartActivationRequestDto activationRequestDto, final String purchaseType)
	{
		String redirectResult = REDIRECT_TO_CART;

		CartModificationData cartModification = null;
		try
		{
			cartModification = wileyb2cCartFacade.addCourseToCart(code, qty, purchaseType, activationRequestDto);
		}
		catch (final CommerceCartModificationException ex)
		{
			redirectResult = handleCommerceCartModificationException(ex, redirectAttributes, code, request);
		}
		catch (final UnknownIdentifierException ex)
		{
			handleUnknownIdentifierException(ex, redirectAttributes, code);
		}

		if (cartModification != null)
		{
			addCartModificationMessages(redirectAttributes, qty, cartModification);
		}
		return redirectResult;
	}

	private CartActivationRequestDto extractActivationDto(final String cartActivationInfo, final String checksum)
			throws URISyntaxException, WileyplusChecksumMismatchException
	{
		final CartActivationRequestDto cartActivationRequestDto = wileyplusPurchaseOptionsPageURLParser
				.parseCartActivationParams(cartActivationInfo, checksum);
		return cartActivationRequestDto;
	}
}
