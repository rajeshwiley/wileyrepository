package com.wiley.wileyb2c.storefront.interceptors.beforeview;

import de.hybris.platform.acceleratorstorefrontcommons.interceptors.BeforeViewHandler;
import de.hybris.platform.commercefacades.user.data.CountryData;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;

import com.wiley.core.wileycom.i18n.WileycomWorldRegionService;
import com.wiley.facades.locale.WileyLocaleFacade;
import com.wiley.facades.wileycom.storesession.WileycomStoreSessionFacade;

import static java.util.Comparator.comparing;


/**
 * Interceptor that setup session country to model
 */
public class SessionCountryBeforeViewHandler implements BeforeViewHandler
{
	private static final String EMPTY_ARRAY = "[]";
	private static final String ARRAY_DELIMITER = ",";
	private static final String ARRAY_COUNTRY_FORMAT = "{\"name\":\"%s\",\"isocode\":\"%s\"}";

	@Resource
	private WileycomStoreSessionFacade wileyb2cStoreSessionFacade;

	@Resource
	private WileycomWorldRegionService wileycomWorldRegionService;

	@Resource
	private WileyLocaleFacade wileyLocaleFacade;

	@Override
	public void beforeView(final HttpServletRequest request, final HttpServletResponse response, final ModelAndView modelAndView)
	{
		final CountryData currentCountry = wileyb2cStoreSessionFacade.getCurrentCountry();
		modelAndView.addObject("currentCountry", currentCountry);
		modelAndView.addObject("currentLocale", wileyLocaleFacade.getCurrentEncodedLocale());
		modelAndView.addObject("wileyCountryList", getCountries(currentCountry));
	}

	private String getCountries(@Nonnull final CountryData currentCountry)
	{
		final List<CountryData> countries = wileycomWorldRegionService.getCountries();
		return convertToArray(countries, currentCountry);
	}

	private String convertToArray(final List<CountryData> countries, final CountryData currentCountry)
	{
		if (countries.isEmpty())
		{
			return EMPTY_ARRAY;
		}

		final String countriesList = countries.stream()
				.filter(c -> !Objects.equals(c.getIsocode(), currentCountry.getIsocode()))
				.sorted(comparing(CountryData::getName))
				.map(c -> String.format(ARRAY_COUNTRY_FORMAT, c.getName(), c.getIsocode()))
				.collect(Collectors.joining(ARRAY_DELIMITER));

		return "[" + countriesList + "]";
	}
}
