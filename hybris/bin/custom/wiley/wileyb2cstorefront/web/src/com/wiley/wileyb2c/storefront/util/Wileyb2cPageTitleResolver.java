package com.wiley.wileyb2c.storefront.util;

import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.storefrontcommons.util.WileyPageTitleResolver;


/**
 * Created by Raman_Hancharou on 6/27/2017.
 */
public class Wileyb2cPageTitleResolver extends WileyPageTitleResolver
{
	private List<String> categoryCodes;

	@Override
	public String resolveHomePageTitle(final String title)
	{
		final CMSSiteModel currentSite = getCmsSiteService().getCurrentSite();
		final StringBuilder builder = new StringBuilder();
		if (!StringUtils.isEmpty(title))
		{
			builder.append(title).append(TITLE_WORD_SEPARATOR);
		}
		builder.append(currentSite.getName());
		return StringEscapeUtils.escapeHtml(builder.toString());
	}

	public String resolveProductPageTitle(final String title, final ProductModel product)
	{
		if (StringUtils.isNotEmpty(title))
		{
			return StringEscapeUtils.escapeHtml(title);
		}
		if (product instanceof VariantProductModel)
		{
			return super.resolveProductPageTitle(((VariantProductModel) product).getBaseProduct());
		}
		else
		{
			return super.resolveProductPageTitle(product);
		}
	}

	@Override
	protected List<CategoryModel> getCategoryPath(final ProductModel product)
	{
		Collection<CategoryModel> categoryModels = new ArrayList<>();
		List<CategoryModel> listOfCategories = new ArrayList<>();
		categoryModels.addAll(product.getSupercategories());

		if (categoryModels.isEmpty())
		{
			return Collections.emptyList();
		}

		buildCategoryPath(categoryModels, listOfCategories);

		return listOfCategories;
	}

	private void buildCategoryPath(final Collection<CategoryModel> categoryModels, final List<CategoryModel> listOfCategories)
	{
		CategoryModel toDisplay = getTopCategoryForHierarchy(categoryModels);
		if (toDisplay == null)
		{
			toDisplay = getCategoryForHierarchy(categoryModels);
		}

		if (toDisplay != null)
		{
			listOfCategories.add(toDisplay);
			buildCategoryPath(toDisplay.getSupercategories(), listOfCategories);
		}
	}

	protected CategoryModel getCategoryForHierarchy(final Collection<CategoryModel> categoryModels)
	{
		Optional<CategoryModel> firstCategoryModel = categoryModels
				.stream()
				.filter(categoryModel -> !(categoryModel instanceof ClassificationClassModel))
				.findFirst();

		if (firstCategoryModel.isPresent())
		{
			return firstCategoryModel.get();
		}

		return null;
	}

	private CategoryModel getTopCategoryForHierarchy(final Collection<CategoryModel> allProductCategories)
	{
		for (String categoryCode : categoryCodes)
		{
			Optional<CategoryModel> category = allProductCategories
					.stream()
					.filter(productCategory -> categoryBelongsToTopLevelCategory(categoryCode, productCategory))
					.findFirst();
			if (category.isPresent())
			{
				return category.get();
			}
		}
		return null;
	}

	private boolean categoryBelongsToTopLevelCategory(final String superCategoryCode, final CategoryModel category)
	{
		boolean categoryIsATopLevelCategory = superCategoryCode.equals(category.getCode());

		if (categoryIsATopLevelCategory)
		{
			return true;
		}
		boolean categoryHastTopLevelCategoryInSuper =
				CollectionUtils.isNotEmpty(category.getAllSupercategories())
						&& category.getAllSupercategories()
						.stream()
						.anyMatch(
								superCategory -> superCategoryCode.equals(superCategory.getCode())
						);
		return categoryHastTopLevelCategoryInSuper;
	}

	@Required
	public void setCategoryCodes(final List<String> categoryCodes)
	{
		this.categoryCodes = categoryCodes;
	}
}
