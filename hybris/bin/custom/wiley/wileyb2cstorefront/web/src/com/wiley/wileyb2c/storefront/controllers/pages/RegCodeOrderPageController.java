package com.wiley.wileyb2c.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.regCode.exception.RegCodeInvalidException;
import com.wiley.core.regCode.exception.RegCodeValidationException;
import com.wiley.facades.wileyb2c.order.WileyRegCodeCheckoutFacade;
import com.wiley.wileyb2c.storefront.forms.RegCodeActivationForm;

@Controller
@Profile(WileyCoreConstants.WILEY_PHASE2A_PROFILE_NAME)
@SessionAttributes("regCodeActivationForm")
public class RegCodeOrderPageController extends AbstractPageController
{
	private static final Logger LOG = Logger.getLogger(RegCodeOrderPageController.class);

	private static final String CHECKOUT_REG_CODE = "/checkout/regCode";
	private static final String ACTIVATE_PAGE = CHECKOUT_REG_CODE + "/activate";
	private static final String CONFIRMATION_PAGE = CHECKOUT_REG_CODE + "/confirmation";

	private static final String REG_CODE_FIELD_ERROR = "regCodeFieldError";
	private static final String REG_CODE_ACTIVATION_FORM = "regCodeActivationForm";
	private static final String REG_CODE = "regCode";
	private static final String FORM_GLOBAL_ERROR_MESSAGE = "form.global.error";
	private static final String FORM_GLOBAL_TECHNICAL_MESSSAGE = "externalService.response.error";
	private static final String REG_CODE_INVALID = "regCode.errorMessage.invalid";

	@Resource(name = "wileyRegCodeCheckoutFacade")
	private WileyRegCodeCheckoutFacade checkoutFacade;

	@ModelAttribute(REG_CODE_ACTIVATION_FORM)
	public RegCodeActivationForm initializeForm() {
		return new RegCodeActivationForm();
	}

	@RequestMapping(value = CHECKOUT_REG_CODE + "/order")
	@RequireHardLogIn
	public String order(@Valid @ModelAttribute(REG_CODE_ACTIVATION_FORM) final RegCodeActivationForm form,
			final BindingResult bindingResult, final SessionStatus sessionStatus, final RedirectAttributes redirectAttributes)
	{
		sessionStatus.setComplete();
		if (bindingResult.hasErrors())
		{
			handleGlobalError(redirectAttributes, StringUtils.EMPTY, FORM_GLOBAL_ERROR_MESSAGE);
			handleFieldError(redirectAttributes, REG_CODE_INVALID);
			return REDIRECT_PREFIX + ACTIVATE_PAGE;
		}
		String regCode = form.getRegCode();
		try
		{
			checkoutFacade.placeRegCodeOrder(regCode);
			return REDIRECT_PREFIX + CONFIRMATION_PAGE;
		}
		catch (RegCodeInvalidException e)
		{
			LOG.warn(String.format("Error occurs during activation of RegCode [%s]", regCode), e);
			handleGlobalError(redirectAttributes, StringUtils.EMPTY, FORM_GLOBAL_ERROR_MESSAGE);
			handleFieldError(redirectAttributes, REG_CODE_INVALID);
			return REDIRECT_PREFIX + ACTIVATE_PAGE;
		}
		catch (RegCodeValidationException e)
		{
			LOG.warn(String.format("Error occurs during activation of RegCode [%s]", regCode), e);
			handleGlobalError(redirectAttributes, regCode, FORM_GLOBAL_TECHNICAL_MESSSAGE);
			return REDIRECT_PREFIX + ACTIVATE_PAGE;
		}


	}

	private void handleGlobalError(final RedirectAttributes redirectAttributes, final String regCode,
									 final String globalErrorKey)
	{

		String globalErrorMessage = getMessageSource().getMessage(globalErrorKey, null,
				getI18nService().getCurrentLocale());
		GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
				globalErrorMessage);

		redirectAttributes.addFlashAttribute("invalidRegCode", regCode);
	}

	private void handleFieldError(final RedirectAttributes redirectAttributes, final String errorKey)
	{
		String errorMessage = getMessageSource().getMessage(errorKey, null,
				getI18nService().getCurrentLocale());
		redirectAttributes.addFlashAttribute(REG_CODE_FIELD_ERROR, new FieldError(
				REG_CODE_ACTIVATION_FORM,
				REG_CODE, errorMessage));
	}


}
