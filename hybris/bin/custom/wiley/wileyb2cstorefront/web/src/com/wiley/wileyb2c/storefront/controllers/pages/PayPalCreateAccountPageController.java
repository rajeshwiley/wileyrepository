package com.wiley.wileyb2c.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.forms.GuestForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.RegisterForm;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.wileyb2c.storefront.controllers.ControllerConstants;


@Controller
@Profile(WileyCoreConstants.WILEY_PHASE2A_PROFILE_NAME)
@RequestMapping(value = "/paypal/login")
public class PayPalCreateAccountPageController extends CheckoutCreateAccountController
{
	private static final String PAY_PAL_CHECKOUT_REGISTER_URL = "/paypal/login/registerWithPayPalRedirect";

	@RequestMapping(value = "/createAccountWithPayPalRedirect", method = RequestMethod.POST)
	public String doPayPalCreateAccount(final GuestForm guestForm, final BindingResult bindingResult,
			final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		return super.createAccount(guestForm, bindingResult, model, redirectAttributes);
	}

	@RequestMapping(value = "/registerWithPayPalRedirect", method = RequestMethod.GET)
	public String paypalCheckoutRegister(@RequestParam(value = "error", defaultValue = "false") final boolean loginError,
			final HttpSession session, final Model model, final HttpServletRequest request) throws CMSItemNotFoundException
	{
		return super.checkoutRegister(loginError, session, model, request);
	}

	@RequestMapping(value = "/registerWithPayPalRedirect", method = RequestMethod.POST)
	public String doPayPalCheckoutRegister(final RegisterForm form, final BindingResult bindingResult, final Model model,
			final HttpServletRequest request, final HttpServletResponse response, final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException
	{
		return super.doCheckoutRegister(form, bindingResult, model, request, response, redirectModel);
	}

	@Override
	protected String getSuccessRedirect(final HttpServletRequest request, final HttpServletResponse response)
	{
		return PayPalLoginPageController.PAY_PAL_CHECKOUT_URL;
	}

	@Override
	protected String getRegisterRedirect()
	{
		return REDIRECT_PREFIX + PAY_PAL_CHECKOUT_REGISTER_URL;
	}

	@Override
	protected String getView()
	{
		return ControllerConstants.Views.Pages.PayPal.PAY_PAL_CREATE_ACCOUNT_PAGE;
	}
}