ACC.cookieconsentcomponent = {

}

$(document).ready(function ()
{
	with (ACC.cookieconsentcomponent)
	{
		var popupContainerElement = $(".cookie-consent-component-container");
		if ($(popupContainerElement).length) {
			var configElement = $(".cookie-consent-component-config", popupContainerElement);

			var message = configElement.data('message');
			var dismiss = configElement.data('dismiss');
			var link = configElement.data('link');
			var href = configElement.data('href');
			var countryCode = configElement.data('countryCode');


			var config = {
				'layout' : 'cookie-consent-component-layout',
				'layouts' : {
					'cookie-consent-component-layout': '<div class="container-fluid cookie-popup-container"><div class="container">{{messagelink}}{{compliance}}</div></div>'
				},
				'content' : {
					'message' : message,
					'dismiss': dismiss,
					'link': link,
					'href': href
				},
				'static': true,
				'elements' : {
					'messagelink' : '<p>{{message}}<a id="cookieconsent:desc" aria-label="learn more about cookies" href="{{href}}" target="_blank" role="button" class="link-corner cc-link">{{link}}</a></p>',
					'dismiss' : '<a aria-label="dismiss cookie message" role="button" class="small-button cc-btn cc-dismiss">{{dismiss}}</a>'
				},
				'container' : popupContainerElement[0],
				'law' : {
					'countryCode' : countryCode
				}
			}

			window.cookieconsent.initialise(config);
		}
	}
});


