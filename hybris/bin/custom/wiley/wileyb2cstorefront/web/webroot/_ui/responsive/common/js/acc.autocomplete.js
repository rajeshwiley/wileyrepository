ACC.autocomplete = {

	_autoload: [
		"bindSearchAutocomplete"
	],

    updateActionAndSubmitForm: function (form, actionType) {
        var action = form.attr('action');
        if (actionType == 'content-search' && action.indexOf(actionType) < 0) {
        	action = action.substring(0, action.lastIndexOf('search')) + actionType;
            form.attr('action', action);
        }
        form.submit();
    },

	bindSearchAutocomplete: function () {
		// extend the default autocomplete widget, to solve issue on multiple instances of the searchbox component
		$.widget("custom.yautocomplete", $.ui.autocomplete, {
			_create: function () {

				// get instance specific options form the html data attr
				var option = this.element.data("options");
				// set the options to the widget
				this._setOptions({
					minLength: option.minCharactersBeforeRequest,
					displayProductImages: option.displayProductImages,
					delay: option.waitTimeBeforeRequest,
					autocompleteUrl: option.autocompleteUrl,
					source: this.source
				});

				// call the _super()
				$.ui.autocomplete.prototype._create.call(this);

			},
			options: {
				cache: {}// init cache per instance
			},
			_renderMenu: function (ul, items) {
				var that = this;
				$(ul).html($(".main-navigation-search-autocomplete-template").html());
				$(".search-related-content").hide();
				$(".related-content-products-section").hide();
				$(".related-content-other-section").hide();
				$(".link-view-more-products").hide();
				$(".link-view-more-other").hide();
				$.each(items, function (index, item) {
					that._renderItem(ul, item);
				});
				$('.main-navigation-search-autocomplete').on('mousedown', function (e) {
					if ($(e.target).hasClass('link-corner-products')) {
						e.preventDefault();
                        ACC.autocomplete.updateActionAndSubmitForm($(this).closest("form"), "search")
					} else if ($(e.target).hasClass('link-corner-other')) {
                        e.preventDefault();
                        ACC.autocomplete.updateActionAndSubmitForm($(this).closest("form"), "content-search")
                    }
				});
			},
			_renderItem: function (ul, item) {
				if (item.type === "autoSuggestion") {
                    $(".search-related-content").show();
					var appendTo = $(".search-list", ul);
                    var renderHtml = "<a  href='" + item.url + "'>" + item.value + "</a>";
                    return $("<div class='searchresults-item'>").data("item.autocomplete", item).append(renderHtml).appendTo(appendTo);
                }
				else if (item.type === "productResult") {
					$(".related-content-products-section").show();
					if (item.showSeeAllProducts) {
						$(".link-view-more-products").show();
					}
					var appendTo = $(".related-content-products", ul);
                    var renderHtml = "<a href='" + item.url + "'>" + item.value + "</a>";
					return $("<div class='searchresults-item'>").data("item.autocomplete", item).append(renderHtml).appendTo(appendTo);
				}
				else if (item.type === "contentResult") {
					$(".related-content-other-section").show();
					if (item.showSeeAllPages) {
						$(".link-view-more-other").show();
					}
					var appendTo = $(".related-content-other", ul);
                    var renderHtml = "<a  href='" + item.url + "'>" + item.value + "</a>";
                    return $("<div class='searchresults-item'>").data("item.autocomplete", item).append(renderHtml).appendTo(appendTo);
                }
			},
			source: function (request, response) {
				var self = this;
				var term = request.term.toLowerCase();
				if (term in self.options.cache) {
					return response(self.options.cache[term]);
				}

				$.getJSON(self.options.autocompleteUrl, {term: request.term}, function (data) {
					var autoSearchData = [];
					if (data.suggestions !== null) {
						$.each(data.suggestions, function (i, obj) {
							var highlightOpen = "<span class=\"search-highlight\">";
							var highlightClose = "</span>";
							var newTerm = obj.term;
                            if (newTerm.indexOf(highlightOpen) >= 0 && newTerm.indexOf(highlightClose) >= 0) {
                                newTerm = newTerm.replace(highlightOpen, "");
                                newTerm = newTerm.replace(highlightClose, "");
                            }
							autoSearchData.push({
								value: obj.term,
								url: ACC.config.encodedContextPath + "/search?pq=" + newTerm,
								type: "autoSuggestion"
							});
						});
					}
					if (data.products !== null) {
						$.each(data.products, function (i, obj) {
							autoSearchData.push({
								showSeeAllProducts: data.showSeeAllProducts,
								value: obj.name,
								code: obj.code,
								desc: obj.description,
								authors: obj.authorsWithoutRoles,
								manufacturer: obj.manufacturer,
								url: ACC.config.encodedContextPath + obj.url,
								price: obj.price !== null ? obj.price.formattedValue : "",
								type: "productResult",
								image: (obj.images !== null && self.options.displayProductImages) ? obj.images[0].url : null // prevent errors if obj.images = null
							});
						});
					}
					if (data.pages !== null) {
						$.each(data.pages, function (i, obj) {
							autoSearchData.push({
								showSeeAllPages: data.showSeeAllPages,
								value: obj.title,
								url: obj.url,
								type: "contentResult"
							});
						});
					}
					self.options.cache[term] = autoSearchData;
					return response(autoSearchData);
				});
			}

		});


		var $search = $(".js-site-search-input");
		var searchForm = $('form[name^="search_form_"]');

		if ($search.length > 0) {
			// Event handler before autocomplete widget initialization
			$search.keydown(function (e) {
				var items = $('.ui-menu-item');
				var lastIndex = items.length - 1;
				var activeClass = 'ui-state-focused';
				var activeItem = $('.ui-state-focused');
				var activeIndex = items.index(activeItem);


				function setFocus(next) {
					var nextIndex = next && (activeIndex < lastIndex) ? activeIndex + 1 : next ? 0 : activeIndex - 1;
					items.removeClass(activeClass);
					$(items.get(nextIndex)).addClass(activeClass);
				}

				if (e.keyCode === 13 || e.keyCode === 40 || e.keyCode === 38) { //Enter, ArrowDown or ArrowUp
					if (e.keyCode === 13) {
						if (activeItem.length) {
							var activeLink = activeItem.find('a');
							activeLink[0].click();
						} else {
                            e.preventDefault();
                            ACC.autocomplete.updateActionAndSubmitForm(searchForm, "search")
						}
					} else {
						setFocus(e.keyCode === 40)
					}
					e.preventDefault();
					e.stopPropagation();
				}

			});
			$search.blur(function () {
				$('.main-navigation-search-autocomplete').find('ui-state-focused').removeClass('ui-state-focused')
			})
			var appendTo = $search.closest("form");
			var auto = $search.yautocomplete({appendTo: appendTo, autoFocus: true});

			// use custom div instead of default ul
			var oldParent = auto.data('custom-yautocomplete').menu.element.parent();

			auto.data('custom-yautocomplete').menu = $(".main-navigation-search-autocomplete-template")
				.clone()
				.removeClass("main-navigation-search-autocomplete-template")
				.addClass("ui-autocomplete ui-front main-navigation-search-autocomplete")
				.appendTo(oldParent)
				.menu({
					role: null,
					items: ".searchresults-item, .link-view-more-products, .link-view-more-other",
					select: function (e) {
						var activeLink = $(this).find('.ui-state-focused').find('a');
						if (activeLink.length) {
							if ($(activeLink).hasClass('link-corner-products')) {
                                e.preventDefault();
                                ACC.autocomplete.updateActionAndSubmitForm(searchForm, "search")
							} else if ($(activeLink).hasClass('link-corner-other')) {
                                e.preventDefault();
                                ACC.autocomplete.updateActionAndSubmitForm(searchForm, "content-search")
                            } else {
								activeLink.click();
							}
						}

					}
				})
				.hide()
				.data("ui-menu");
		}

	}
};
