ACC.termsandconditions = {

	bindTermsAndConditionsLink: function() {
		$(document).on("click",".termsAndConditionsLink",function(e) {
			e.preventDefault();
			
			var termsAndConditionsURL = $(this).attr("href");
			$.get(termsAndConditionsURL, function(data){
				$("#termsAndConditionsPopup").replaceWith(data);
				$("#termsAndConditionsPopup").modal();
			})
			
		});
	}
}

$(function(){
	with(ACC.termsandconditions) {
		bindTermsAndConditionsLink();
	}
});
