ACC.facetsearch = {
    removeValuesFromUrl : function (values, currentUrl) {
        $.each(values, function (index, value) {
            //All spaces are replaced with +
			value = value.replace(/ /g, '+');
            //Double replace covers cases when our pattern is in the beginning or on the end of url
            currentUrl = currentUrl.replace(value, '').replace('::', ':')
        });
        return currentUrl;
    },

    getValueUrlsForCheckboxes : function (selectedItems) {
        return $.map(selectedItems, function (item) {
            return item.currentOptionUrl;
        });
    },

    submitCheckboxForm : function(checkbox) {
        if (checkbox) {
            var form =  $("<form method='get' name='checkboxFacetsForm'/>");
            var facetItem =  $(checkbox).closest('.facets-panel-item');
            var pq = facetItem.data('pq');
            var input = $('<input name="pq" type="hidden"/>');

            if (checkbox.checked) {
				facetItem.addClass('active');
			} else {
				facetItem.removeClass('active');
			}

            input.attr('value', pq);
            $(form).append(input).appendTo('body');
            $(form).submit();
        }
    }
};

ACC.facetsearch.modal = {
    getCurrentModal : function () {
        return $('#facetsListWindow:visible');
    },

    clearAllValues : function () {
        var modal = this.getCurrentModal();
        if (modal) {
			var selectedFacets = wiley.facets.getSelectedFacets();
            var selectedValues = ACC.facetsearch.getValueUrlsForCheckboxes(selectedFacets);
            var currentUrl = document.URL;
            currentUrl = ACC.facetsearch.removeValuesFromUrl(selectedValues, currentUrl);
            $(window.location).attr('href', currentUrl);
        }
    }
};