ACC.product = {

	_autoload: [
		"bindToAddToCartForm",
		"enableStorePickupButton",
		"enableAddToCartButton",
		"enableVariantSelectors",
		//"bindFacets"
	],


	bindFacets:function(){
		$(document).on("click",".js-show-facets",function(e){
			e.preventDefault();

			ACC.colorbox.open("Select Refinements",{
				href: "#product-facet",
				inline: true,
				width:"320px",
				onComplete: function(){

					$(document).on("click",".js-product-facet .js-facet-name",function(e){
						e.preventDefault();
						$(".js-product-facet  .js-facet").removeClass("active");
						$(this).parents(".js-facet").addClass("active");
						$.colorbox.resize()
					})
				},
				onClosed: function(){
					$(document).off("click",".js-product-facet .js-facet-name");
				}
			});
		});



		enquire.register("screen and (min-width:"+screenSmMax+")",  function() {
			$("#cboxClose").click();
		});


	},

	enableAddToCartButton: function ()
	{
		$('.js-add-to-cart').removeAttr("disabled");
	},
	
	enableVariantSelectors: function ()
	{
		$('.variant-select').removeAttr("disabled");
	},

	bindToAddToCartForm: function ()
	{
        var loader = $("<div class='wiley-loader'></div>");
		$('.add_to_cart_form').submit(function(e){
            $('body').append(loader);
			var addToCartForm = $(this);
			ACC.product.updateQuantityInAddToCartForm(addToCartForm);
			e.preventDefault();
			$.ajax({
				type: "POST",
				url: addToCartForm.attr("action"),
				data: addToCartForm.serialize(),
				contentType: "application/x-www-form-urlencoded",
				accept: "application/json",
				success: function(cartResult, statusText, xhr){
                    loader.detach();
					ACC.product.displayAddToCartPopup(cartResult, statusText, xhr, addToCartForm);
					},
				error: function(cartResult, statusText, xhr){
                    loader.detach();
					ACC.product.displayAddToCartErrorPopup(cartResult, statusText, xhr, addToCartForm)
				},
				xhrFields: {
					withCredentials: true
				}
			})
		});
	},
	
	updateQuantityInAddToCartForm: function ($form)
    {
     	var noOfProducts = $('#qty').val();
     	if (noOfProducts === undefined){
     		noOfProducts = $form.closest(".table-row-content").find(".product-quantity .product-item-spinner").val();
     	}
     	if (noOfProducts){
			$form.find(".productAQuantity").val(noOfProducts);
		}
    },

	bindToAddToCartStorePickUpForm: function ()
	{
		var addToCartStorePickUpForm = $('#colorbox #add_to_cart_storepickup_form');
		addToCartStorePickUpForm.ajaxForm({success: ACC.product.displayAddToCartPopup});
	},


	enableStorePickupButton: function ()
	{
		$('.js-pickup-in-store-button').removeAttr("disabled");
	},

	displayAddToCartPopupCarousel: function () {
		wiley.owlCarousel("#addToCartModalWnd .owl-carousel");
	},

	displayAddToCartErrorPopup: function (cartResult, statusText, xhr, formElement)
	{
		var errorData={};
		errorData.withErrors  = true;
		errorData.newlyAddedQuantity = 0;
		errorData.newlyAddedQuantityLabel = addToCartPopupItemsLabel;
		$("#addToCartModalWnd").html(Mustache.render(addToCartModalWndTemplate, errorData));
		$("#addToCartModalWnd").modal('show');
	}, 
	
	displayAddToCartPopup: function (cartResult, statusText, xhr, formElement)
	{
		$('#addToCartLayer').remove();

		var titleHeader = $('.addToCartTitle').html();

		var productCode = $('[name=AAISBN]', formElement).val();
		var quantityField = $('[name=PAQUANT]', formElement).val();

		var formData = [];
		formData = ACC.product.getFormData(formElement);

		var data = ACC.product.prepareDataForRendering(cartResult, formData);
		$("#addToCartModalWnd").html(Mustache.render(addToCartModalWndTemplate, data));
		$("#addToCartModalWnd").modal('show');
			
		var quantity = 1;
		if (quantityField != undefined)
		{
			quantity = quantityField;
		}

		$.ajax({
			type: "GET",
			url: ACC.product.getFormRelatedProductsUrl(formElement),
			accepts: "text/xml",
			success: function(data, statusText, xhr){
				wiley.updateMultipleSectionsAjax(data, ACC.product.displayAddToCartPopupCarousel)
			},
			error: function(data, statusText, xhr){
				console.log('error during ajax call for related products');
			}
		})


		ACC.track.trackAddToCart(productCode, quantity, cartResult.cartData);

	},
	
	prepareDataForRendering: function(addToCartResponse, formData){
		addToCartResponse.withErrors  = ACC.product.isListEmpty(addToCartResponse.entries) || !ACC.product.isListEmpty(addToCartResponse.errors);
		
		if (addToCartResponse.errors){
			addToCartResponse.errors.forEach(function(error, i){
				error.isCodeEmpty = !error.code;
				error.isIsbnEmpty = !error.isbn 
			});
		}

		var newlyAddedQuantity = 0;
		if (!ACC.product.isListEmpty(addToCartResponse.entries)){
			addToCartResponse.withEntries = true;
			addToCartResponse.entries.forEach(function(entry, i){
				var formEntryData = ACC.product.getFormEntryData(formData, entry.isbn);
				entry.mediumType = formEntryData.mediumType;
				entry.name =  jQuery('<p>' + formEntryData.name + '</p>').text();
				entry.authors = jQuery('<p>' + formEntryData.authors + '</p>').text();
				entry.image = formEntryData.image;
				entry.publicationDate = formEntryData.publicationDate;
				entry.formattedOriginalPrice = ACC.product.formatPrice(entry.originalPrice);
				entry.formattedDiscountedPrice = ACC.product.formatPrice(entry.discountedPrice);
				entry.originalTotal = ACC.product.formatPrice(entry.quantity * entry.originalPrice);
				entry.total = ACC.product.formatPrice(entry.quantity * entry.discountedPrice);
				entry.isOriginalPriceVisible = (entry.formattedOriginalPrice != entry.formattedDiscountedPrice);
				entry.isCountable = formEntryData.countable;
				entry.productSetComponents = ACC.product.getFormEntrySetsData(formData, entry.isbn);

				newlyAddedQuantity += entry.quantity;
			});
			
			addToCartResponse.totalQuantityLabel = addToCartResponse.totalQuantity > 0 ?
				ACC.product.getQuantityLabel(addToCartResponse.totalQuantity, addToCartPopupItemLabel, addToCartPopupItemsLabel) : "";
			addToCartResponse.subtotalPrice = ACC.product.formatPrice(addToCartResponse.subtotalPrice);
		}
		addToCartResponse.newlyAddedQuantity = newlyAddedQuantity;		
		addToCartResponse.newlyAddedQuantityLabel = ACC.product.getQuantityLabel(newlyAddedQuantity, addToCartPopupItemLabel, addToCartPopupItemsLabel);

		return addToCartResponse;
	},
	
	formatPrice: function(price){
		if (price){
			return price.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
		}
		return "";
	},
	
	isListEmpty: function(list){
		return !list || list.length == 0
	},

	getQuantityLabel: function(quantity, oneItemLabel, severalItemsLabel){
		return quantity == 1 ? oneItemLabel : severalItemsLabel;
	},

	getFormData: function(formElement){
		var formData = [];
		$('.js-add-to-cart-product, .js-add-to-cart-product-set-component', formElement).each(function (i, productSpan) {
			var productSpanData = $(productSpan).data();
			formData.push(productSpanData)
		});
		return formData;
	},

	getFormEntryData: function(formData, code){
		var result = [];
		if (formData && formData.length>0){
			formData.forEach(function(formEntryData, i){
				if (code == formEntryData.code){
					result = formEntryData;
				}
			});
		}
		return result;
	},

	getFormEntrySetsData: function(formData, code){
		var result = [];
		if (formData && formData.length>0){
			formData.forEach(function(formEntryData, i){
				if (code == formEntryData.parentCode){
					result.push(formEntryData);
				}
			});
		}
		return result;
	},

	getFormRelatedProductsUrl: function(formElement){
		var result = $(formElement).data('related-products-url');
		var codes = [];
		$('.js-add-to-cart-product', formElement).each(function(){
			codes.push($(this).data('code'))
		});
		result = result + '?productCode=' + codes.join(',');
    	return result;
	}

};
