ACC.validatePassword = {
	validatePassword: function (e)
		{
			var classes = {
				hasError: 'has-error',
				globalAlerts: 'global-alerts',
				mainHeader: 'main-header-container'
			};
			var selectors = {
				form: 'form',
				password: ':password',
				formCell: '.form-cell',
				hasError: '.' + classes.hasError,
				helpBlock: '.help-block',
				progress: '.progress',
				header: '.' + classes.mainHeader,
				globalAlerts: '.' + classes.globalAlerts
			};
			var pattern = new RegExp("^([a-zA-Z0-9]){5,32}$");
			var button = e.target;
			var $form = $(button).closest(selectors.form);
			var $input = $form.find(selectors.password).eq(-2);
			var $formCell = $input.closest(selectors.formCell);
			var $alertDiv = $(selectors.header).next();
			var message = $input.attr('data-error-message');
			var globalMessage = $input.attr('data-global-error-message');

			var passwordValue = $input.val();

			// if password valid
			if(pattern.test(passwordValue)){
				$formCell.removeClass(classes.hasError);
				$formCell.children(selectors.helpBlock).remove();
				return true;
			}else{
				// if password is invalid
				// clear password fields
				$form.find(selectors.password).val("");
				$form.find(selectors.progress).hide();

				//remove global alerts from earlier to avoid garbaging when user closes the error message
				$alertDiv.children().remove(selectors.globalAlerts);
				$alertDiv.prepend(ACC.validatePassword.buildMainError(globalMessage));


				// if error was not found previously
				if(!$formCell.hasClass(classes.hasError)){
					$formCell.addClass(classes.hasError);
					// build error message
					$formCell.append(ACC.validatePassword.buildDOM(message));
				}


				return false;
			}

		},
		buildDOM: function buildDOM (message){
        	return "<div class='help-block'><span id='pwd.errors'>"+message+"</span></div>";
        },
        buildMainError: function buildMainError(globalMessage){
			return "<div class='global-alerts'><div class='alert alert-danger alert-dismissable' role='validation-message'><button class='close' aria-label='close' data-dismiss='alert' type='button'></button>"+globalMessage+"</div></div>";
        }
};

