ACC.updateprofile = {

	_autoload: [
		"bindEnrolledInSchool",
		"bindUpdatePassword"
	],

	bindEnrolledInSchool: function (){
		
		if ($('#enrolledInSchool').is(':checked')) {
		$("#school-name").show();
		}
		else{
			$("#school-name").hide();
		}
			
		$('#enrolledInSchool').change(function() {
		     $("#school-name").toggle();
		}); 
	},

	bindUpdatePassword: function () {
		$('#updatePassword').on('click', function (e) {

			if(!ACC.validatePassword.validatePassword(e)){
				e.preventDefault();
			}else{
				var emailAddress = $('#emailAddress').data('email');
				$('#emailField').val(emailAddress);
				$('#titleField').val('notitle');
			}
		});
	}
};