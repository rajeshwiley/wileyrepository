ACC.resetPassword = {

	_autoload: [
		"bindResetPassword"
	],

	bindResetPassword: function () {
		$('#resetPassword').on('click', function (e) {

			if(!ACC.validatePassword.validatePassword(e)){
				e.preventDefault();
			}
		});
	}

};

