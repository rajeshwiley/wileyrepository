ACC.forgottenpassword = {

	_autoload: [
		"bindForgotPasswordLink",
		"bindResetPasswordSubmit"
	],

	bindForgotPasswordLink: function(){
		$(document).on("click",".js-password-forgotten",function(e){
			e.preventDefault();
			var passwordURL = $(this).attr("href");
			$.get(passwordURL,function(data){
				$("#formForgotPassword").replaceWith(data);
				$("#formForgotPassword").modal('toggle');
				$('#formForgotPassword').on('shown.bs.modal', function () {
					$('#myInput').focus()
				})
			})
		})
			
	},
	
	bindResetPasswordSubmit: function(){
		$(document).on("click", ".js-reset-password-submit", function(event){
			event.preventDefault();
			
			var forgottenPwdForm = $('#forgottenPwdForm');
			var resetPasswordUrl = forgottenPwdForm.attr("action");
			
			$.post(resetPasswordUrl, forgottenPwdForm.serialize(), function(data){
					
				if (data!= null && $(data).closest('#validEmail').length && $('#validEmail').length === 0)
				{
					$("#forgotten-modal-body").replaceWith(data);
					$("#forgotPasswordLink").replaceWith(data);
				}
				else
				{
					$("#forgotten-modal-body").replaceWith(data);
					$("#formForgotPassword").modal();
				}
					
			})
		})
	}

};