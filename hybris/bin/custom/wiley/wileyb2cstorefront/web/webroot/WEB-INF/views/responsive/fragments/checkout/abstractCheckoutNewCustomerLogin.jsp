<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="user" tagdir="/WEB-INF/tags/responsive/user" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="col-xs-12 col-sm-12">
    <div class="authorization-support-message-wr">
        <div class="support-message">
            <span><spring:theme code="login.needHelp"/></span>
            <spring:theme code="login.contactUs"/>
        </div>
    </div>

    <div class="create-account wrap">
        <user:register actionNameKey="checkout.login.registerAndCheckout" action="${registerAndCheckoutActionUrl}"/>
    </div>
</div>