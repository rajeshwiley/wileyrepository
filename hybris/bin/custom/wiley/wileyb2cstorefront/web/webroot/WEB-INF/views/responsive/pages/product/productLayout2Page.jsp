<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<template:page pageTitle="${pageTitle}" containerCSSClass="container-fluid" clearContainerClass="true">
    <div class="row">
        <div class="product-details-page">
            <div class="container">
                <cms:pageSlot position="Breadcrumb" var="comp" element="div">
                    <cms:component component="${comp}"/>
                </cms:pageSlot>
            </div>
            <product:productDetailsPanel/>
        </div>
    </div>
</template:page>
