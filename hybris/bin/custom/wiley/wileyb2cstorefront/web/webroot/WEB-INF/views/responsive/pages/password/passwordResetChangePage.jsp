<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="user" tagdir="/WEB-INF/tags/responsive/user" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<spring:theme code="resetPwd.title" var="pageTitle"/>

<template:page pageTitle="${pageTitle}">
	<cms:pageSlot position="TopContent" var="topContent" element="div">
		<cms:component component="${topContent}" />
	</cms:pageSlot>
</template:page>
