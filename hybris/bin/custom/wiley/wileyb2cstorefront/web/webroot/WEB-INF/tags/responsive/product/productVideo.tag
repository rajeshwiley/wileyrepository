<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ attribute name="video" required="true" type="com.wiley.facades.product.data.BrightCoveVideoData" %>

<c:if test="${not empty video}">
    <section id="pdp-video-section" class="page-section section-video">
        <div class="page-section-content">
            <h2 class="section-title">Product video</h2>
            <div class="pdp-video-component">
                <video data-video-id="${video.videoId}" data-account="${video.accountId}" data-player="${video.playerId}"
                       data-embed="default" class="video-js" controls=""
                       style="width: 100%; height: 100%; position: absolute; top: 0px; bottom: 0px; right: 0px; left: 0px;">
                </video>
                <script src="//players.brightcove.net/${video.accountId}/${video.playerId}_default/index.min.js"></script>
            </div>
        </div>
    </section>
</c:if>