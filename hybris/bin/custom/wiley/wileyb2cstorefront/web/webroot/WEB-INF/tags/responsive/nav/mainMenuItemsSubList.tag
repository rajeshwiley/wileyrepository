<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="menuItem" required="true" type="de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<c:set value="${not empty menuItem.children}" var="hasChildren"/>
<c:set value="true" var="linkWouldOpenInSameWindow"/>

<c:if test="${not empty menuItem.entries}">
    <c:set value="${menuItem.entries[0].item}" var="linkItem"/>

    <c:set value="${cms:urlFromComponent(linkItem)}" var="urlForSecondNode"/>
    <c:url value="${urlForSecondNode}" var="urlForSecondNodeResolved"/>

    <c:set value="${linkItem.target == null || linkItem.target == 'SAMEWINDOW'}" var="linkWouldOpenInSameWindow"/>
</c:if>

<c:if test="${empty urlForSecondNodeResolved}">
				<c:url value="#" var="urlForSecondNodeResolved"/>
</c:if>

<c:if test="${(menuItem.visible) and (ycommerce:evaluateRestrictions(menuItem.entries[0].item))}">
	
    <li class="dropdown-item ${hasChildren eq true ? 'dropdown-submenu' : ''}">
        <a href="${urlForSecondNodeResolved}" ${linkWouldOpenInSameWindow eq true ? '' : 'target="_blank"'}>
            ${menuItem.title}
        </a>
        <c:if test="${hasChildren}">
            <ul class="dropdown-items">
                <c:forEach items="${menuItem.children}" var="child">
                	<nav:mainMenuItemsSubList menuItem="${child}"/>
                </c:forEach>
            </ul>
        </c:if>
    </li>
    
</c:if>