<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<template:page pageTitle="${pageTitle}" containerCSSClass="container-fluid dts-page" clearContainerClass="true">
    <div class="row">
        <div class="container">
            <cms:pageSlot position="TopContent" var="comp">
                <cms:component component="${comp}"/>
            </cms:pageSlot>
            <section class="page-section">
                <div class="content">
                    <article class="row">
                        <div class="col-xs-12 col-sm-5">
                            <cms:pageSlot position="TopLeftContent" var="comp" element="div" class="preview-image-wrapper">
                                <cms:component component="${comp}"/>
                            </cms:pageSlot>
                        </div>
                        <div class="col-xs-12 col-sm-7">
                            <cms:pageSlot position="TopRightContent" var="comp">
                                <cms:component component="${comp}"/>
                            </cms:pageSlot>
                        </div>
                    </article>
                </div>
            </section>
			<cms:pageSlot position="BodyContent" var="comp" element="div" class="product-list-wrapper laze-loading collection-list">
				<cms:component component="${comp}"/>
			</cms:pageSlot>
        </div>
    </div>
</template:page>