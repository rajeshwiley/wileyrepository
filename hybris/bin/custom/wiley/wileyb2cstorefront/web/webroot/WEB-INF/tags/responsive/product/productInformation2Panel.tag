<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>

<c:if test="${not empty product.evaluationCopyLinks}">
    <div id="evaluation-copy-section" class="evaluation-copy-section">
        <h2 class="section-title">
            <spring:theme code="product.product.details.evaluation.copy"/>
        </h2>
        <c:forEach items="${product.evaluationCopyLinks}" var="link">
            <spring:theme code="product.companionSiteLink.${link.type}" arguments="${link.url}"/>
        </c:forEach>
    </div>
</c:if>
