<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="product-list wrap">
  <div class="product-list-item">
	<div class="product-list-image">
	  <product:productPrimaryImage product="${productData}" format="thumbnail"/>
	</div>
	<div class="row product-list-info">
	  <div class="col-xs-10 col-sm-9">
		<div class="product-list-title">${productData.name}</div>
		<div class="product-list-description">
		  <div class="product-list-author">${productData.authors}</div>
		  <div class="product-list-date"><fmt:formatDate value="${productData.dateImprint}" pattern="MMMM YYYY"/></div>
		  <div class="product-list-date">
		  	<spring:theme code="text.startFreeTrial.duration"/>&nbsp;${productData.subscriptionTerm.termOfServiceNumber}&nbsp;${productData.subscriptionTerm.termOfServiceFrequency.name}
		  </div>
		</div>
	  </div>
	</div>
  </div>
</div>
