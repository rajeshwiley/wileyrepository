<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="backgroundImages" required="true" type="java.util.Map" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

    <div data-image="${backgroundImages.desktop.url}" data-image-mobile="${backgroundImages.mobile.url}" class="container-image"></div>