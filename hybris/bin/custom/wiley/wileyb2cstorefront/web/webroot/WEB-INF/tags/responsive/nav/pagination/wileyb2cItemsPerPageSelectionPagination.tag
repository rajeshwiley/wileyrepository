<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="pagination-quantity">
  <span class="pagination-quantity-text"><spring:theme code="search.page.itemsPerPage"/></span>
		<div class="select-component">
				<select class="itemsPerPageLimiter" name="limit" name="itemsPerPageLimiter" aria-label="itemsPerPageLimiter">
						<c:forEach items="${itemsPerPage}" var="item">
								<c:choose>
									<c:when test="${item eq currentPageSize}">
										<option value="${item}" selected="selected">${item}</option>
									</c:when>
									<c:otherwise>
										<option value="${item}">${item}</option>
									</c:otherwise>
								</c:choose>
						</c:forEach>
				</select>
		</div>
</div>