
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>
<%@ taglib prefix="multi" tagdir="/WEB-INF/tags/responsive/checkout/multi" %>


<template:page pageTitle="${pageTitle}">
    <cms:pageSlot position="TopContent" var="feature" element="div">
            <cms:component component="${feature}"/>
    </cms:pageSlot>

    <cms:pageSlot position="SideContent" var="feature" element="div">
        <cms:component component="${feature}"/>
    </cms:pageSlot>

</template:page>