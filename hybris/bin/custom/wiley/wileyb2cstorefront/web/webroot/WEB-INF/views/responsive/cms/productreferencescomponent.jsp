<%@ page trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="component" tagdir="/WEB-INF/tags/shared/component" %>

<c:choose>
    <c:when test="${not empty productReferences and component.maximumNumberProducts > 0}">
        <div class="row carousel-default">
            <div class="col-xs-12 col-sm-12">
                <product:productReferences
                        sectionId="${sectionId}"
                        productReferences="${productReferences}"
                        title="${title}"
                        maximumNumberProducts="${component.maximumNumberProducts}"
                        displayProductTitles="${component.displayProductTitles}"
                        displayProductAuthors="true"
                        displayProductPrices="${component.displayProductPrices}"/>
            </div>
        </div>
    </c:when>
    <c:otherwise>
        <component:emptyComponent/>
    </c:otherwise>
</c:choose>
