<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="subscriptionData" required="true" type="com.wiley.facades.product.data.WileySubscriptionData" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi" %>

<div class="col-xs-12 col-sm-9 extend-subscription">
    <spring:theme code="account.subscription.manageSubscriptions.extendsSubscriptionLabel" text="Extend Subscription"/>
</div>
<div class="col-xs-12 col-sm-3">
	<div class="my-account-button-small">
		<button data-toggle="modal"
				data-target="#subscriptionOptionsModal"
                ${not empty subscriptionData.availableUpgradeSubscriptionTerms ? 'class="button button-main large"' : 'class="button button-main large disabled"'}>
            <spring:theme code="account.subscription.manageSubscriptions.upgradeOptions" text="Upgrade options"/>
		</button>
	</div>
</div>