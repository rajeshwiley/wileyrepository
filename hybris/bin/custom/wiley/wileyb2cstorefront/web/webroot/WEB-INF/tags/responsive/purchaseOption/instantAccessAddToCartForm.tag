<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ attribute name="formIndex" required="false" type="java.lang.String" %>
<%@ attribute name="code" required="true" type="java.lang.String" %>
<%@ attribute name="title" required="true" type="java.lang.String" %>
<%@ attribute name="imageUrl" required="true" type="java.lang.String" %>
<%@ attribute name="authors" required="true" type="java.lang.String" %>
<%@ attribute name="purchaseType" required="true" type="java.lang.String" %>

<c:set var="formIndex" value="${empty formIndex ? '0' : formIndex}"/>
<c:url value="/cart/add/course" var="addToCartUrl"/>

<form:form method="post" id="addToCartForm-${formIndex}" action="${addToCartUrl}">
	<input type="hidden" name="productCode" value="${code}"/>
	<input type="hidden" name="cartActivationInfo" value="${cartActivationInfo}"/>
	<input type="hidden" name="checksum" value="${checksum}"/>
	<input type="hidden" name="purchaseType" value="${purchaseType}"/>
	<button class="button button-main large">
		<spring:message code="wileypluscourse.purchase.option.instant-access.button.add-to-cart"/>
	</button>
</form:form>