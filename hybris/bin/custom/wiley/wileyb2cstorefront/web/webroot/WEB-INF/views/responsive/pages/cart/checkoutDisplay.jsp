<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<spring:url value="/cart" var="cartUrl" />

<form:form method="POST" action="${cartUrl}" commandName="enrolledInSchoolForm">
	<c:if test="${isEducationalCart}">
		<div class="enrolled-in-school-section">
			<div class="enrolled-in-school checkbox-component">
				<formElement:formCheckbox idKey="user.enrolledInSchool" labelKey="basket.iam.enrolled.school"
						path="enrolledInSchool"
					  	dataToggle="collapse" dataTarget="#enrolledInSchool"/>

				<div id="enrolledInSchool" class="inter-school ${enrolledInSchoolForm.enrolledInSchool ? 'collapse in' : 'collapse'}">
					<formElement:formSelectBox idKey="user.schoolSelection" labelKey="" skipBlank="true"
							path="school" mandatory="false" items="${displayedSchools}"/>
				</div>
			</div>
		</div>
	</c:if>

	<div class="order-button-group">
		<button type="submit" class="button button-main large checkout-button">
			<spring:theme code="basket.proceed.checkout" text="Proceed to Checkout"/>
		</button>
	</div>
</form:form>