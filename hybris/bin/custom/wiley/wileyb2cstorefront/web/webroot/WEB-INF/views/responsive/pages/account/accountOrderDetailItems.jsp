<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="order-common" tagdir="/WEB-INF/tags/addons/wileycomstorefrontcommons/responsive/order" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<div class="row">
	<div class="col-xs-12 col-sm-9">
		<h1 class="full-width-gray">
			<spring:theme code="text.account.order.title.details" text="Order Details" />
		</h1>
	</div>
	<div class="col-xs-12 col-sm-3">
		<div class="top-icons text-right"><a href="#" class="icon-print2" aria-label="<spring:theme code='product.share.print' text='Print'/>">&nbsp;</a></div>
	</div>
</div>

<spring:url value="/my-account/orders" var="ordersUrl"/>
<div class="account-orderdetail">
	<div class="account-orderdetail-overview">
		<ycommerce:testId code="orderDetail_overview_section">
			<order-common:accountOrderDetailsOverview order="${orderData}"/>
		</ycommerce:testId>
	</div>
</div>