<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<template:page pageTitle="${pageTitle}" containerCSSClass="container-fluid wiley-server-error-page" hideHeaderLinks="true">
	
	<div class="server-error">
		<div class="container">
			<cms:pageSlot position="MiddleContent" var="comp" >
				<cms:component component="${comp}"/>
			</cms:pageSlot>
		</div>
	</div>
</template:page>