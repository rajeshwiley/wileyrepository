<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="facets-panel-button visible-xs">Filter</div>
<div class="facets-panel-wrapper">
	<div id="product-facet" class="facets-panel-container  product-facet js-product-facet">
		<header>
			<span class="close-icon"></span>
			<h2><spring:theme code="search.refinement.facet.heading" text="Filters"/></h2>
			<c:choose>
				<c:when test="${not empty searchPageData.clearAllSelectedFiltersQuery}">
					<spring:url value="${searchPageData.clearAllSelectedFiltersQuery.url}" var="clearAllSelectedFiltersUrl"/>
					<i class="clear-facets active">
						<a href="${clearAllSelectedFiltersUrl}"><spring:theme code="search.nav.facetResetAll" text="Reset All"/></a>
					</i>
				</c:when>
				<c:otherwise>
					<i class="clear-facets">
						<spring:theme code="search.nav.facetResetAll" text="Reset All"/>
					</i>
				</c:otherwise>
			</c:choose>
			<div class="facets-expander">
				<i class="expand-facets" tabindex="0"><spring:theme code="search.nav.facetExpandAll" text="Expand All"/></i>
				<i class="collapse-facets"><spring:theme code="search.nav.facetCollapseAll" text="Collapse All"/></i>
			</div>
		</header>
		<div class="facets-panel">
			<nav:facetNavAppliedFilters pageData="${searchPageData}"/>
			<nav:facetNavRefinements pageData="${searchPageData}"/>
		</div>
		<div class="button-wrapper visible-xs">
			<button class="button-teal"><spring:theme code="search.nav.done.button" text="Done"/></button>
		</div>
	</div>
</div>
<div id="facetsListWindow" role="dialog" class="modal modal-teal modalWindow">
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="#" method="get">
				<div class="modal-header">
					<div class="modal-title"></div>
					<button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div class="modal-body">
					<div class="modal-wnd-list">
						<div class="facets-panel-search">
							<input type="search" placeholder="Search subject name" aria-label="Search facets">
							<div class="hidden-xs glyphicon glyphicon-search"></div>
						</div>
						<div class="facet-list facets-panel-list js-facet-list" id="facetsModalScrollArea">
							<div id="facetsModalContentArea"></div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="reset" class="button button-dark-gray"><spring:theme code="search.nav.facetClearAll" text="Clear All"/></button>
					<button type="button" data-dismiss="modal" class="button button-teal"><spring:theme code="search.nav.done.button" text="Done"/></button>
				</div>
			</form>
		</div>
	</div>
</div>