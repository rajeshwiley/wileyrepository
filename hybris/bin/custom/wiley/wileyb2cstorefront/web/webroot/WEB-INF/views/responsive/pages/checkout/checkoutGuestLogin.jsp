<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:set var="createAccountDataTarget" scope="request" value="/login/checkout/createAccount" />
<c:set var="guestCheckoutDataTarget" scope="request" value="/login/checkout/guest" />

<jsp:include page="../../fragments/checkout/abstractCheckoutGuestLogin.jsp" />