<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="orderData" required="true" type="de.hybris.platform.commercefacades.order.data.OrderData" %>
<%@ attribute name="showTax" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<%@ taglib prefix="multi-checkout-common"
	tagdir="/WEB-INF/tags/addons/wileycomstorefrontcommons/responsive/checkout/multi"%>

<c:url value="/my-account/orders" var="myaccounturl"></c:url>
<template:page pageTitle="${pageTitle}" containerCSSClass="checkout-order-confirmation wrap">
	<cms:pageSlot position="TopContent" var="feature" element="div">
			<cms:component component="${feature}"/>
	</cms:pageSlot>

	<div class="row">
		<div class="col-xs-12 col-sm-6">
			<div class="sub-title-block">
				<h2 class="icon-tick-green"><spring:theme code="checkout.orderConfirmation.thankYou"/></h2>
				<p>
					<spring:theme code="checkout.orderConfirmation.orderNumber" arguments="#,${orderData.code}" />
				</p>
				<p>
					<spring:theme code="checkout.orderConfirmation.copySentTo" arguments="${email}" />
				</p>
				<div class="support-message">
					<span class="progress-number"><spring:theme code="checkout.orderConfirmation.support.message"/></span>
					<spring:theme code="checkout.orderConfirmation.support.contact.us.message" />
				</div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-xs-12 col-sm-6 left-block">
	
			<cms:pageSlot position="SideContent" var="feature" element="div">
				<cms:component component="${feature}" />
			</cms:pageSlot>
	
			<div class="order-brief-section checkout-desktop wrap">
				<div class="print-section checkout-mobile">
					<a href="#" class="icon-print"></a>
				</div>
	
				<c:if test="${not empty deliveryAddress && !isDigitalOrder}">
					<div class="order-brief-top">
						<div class="order-brief-top-item">
							<div class="row">
								<div class="col-xs-12 col-sm-4 order-brief-top-title">
									<spring:theme code="checkout.pickup.items.to.be.shipped" text="Ship To:" />
								</div>
								<div class="col-xs-12 col-sm-8">
									<p class="user-name">
										<c:if test="${not empty deliveryAddress.title}">
											${fn:escapeXml(deliveryAddress.title)}&nbsp;
										</c:if>
										${fn:escapeXml(deliveryAddress.firstName)}&nbsp;${fn:escapeXml(deliveryAddress.lastName)}</p>
									<p>
										<c:if test="${not empty deliveryAddress.line1}">
											${fn:escapeXml(deliveryAddress.line1)},&nbsp;
										</c:if>
										<c:if test="${not empty deliveryAddress.line2}">
											${fn:escapeXml(deliveryAddress.line2)},&nbsp;
										</c:if>
										<c:if test="${not empty deliveryAddress.town}">
											${fn:escapeXml(deliveryAddress.town)},&nbsp;
										</c:if>
										<c:if test="${not empty deliveryAddress.region.name}">
											${fn:escapeXml(deliveryAddress.region.name)}
										</c:if>
									</p>
									<p>
										<c:if test="${not empty deliveryAddress.postalCode}">
											${fn:escapeXml(deliveryAddress.postalCode)},&nbsp;
										</c:if>
										<c:if test="${not empty deliveryAddress.country.name}">
											${fn:escapeXml(deliveryAddress.country.name)}
										</c:if>
									</p>
								</div>
							</div>
						</div>
					</div>
				</c:if>
	
				<div class="product-list ${empty deliveryAddress ? 'order-brief-top' : ''}">
					<c:forEach items="${orderData.entries}" var="entry">
						<c:if test="${entry.deliveryPointOfService == null}">
							<c:url value="${entry.product.url}" var="productUrl" />
							<div class="product-list-item">
								<div class="row product-list-info">
									<div class="col-xs-12 col-sm-9">
										<div class="product-list-image">
											<a href="${productUrl}">
												<product:productPrimaryImage product="${entry.product}" format="thumbnail" />
											</a>
										</div>
										<div class="product-list-title"><a href="${productUrl}">${entry.product.name}</a></div>
										<div class="product-list-description">
											<div class="product-list-qty">
												<spring:theme code="basket.page.qty" />:&nbsp;${entry.quantity}</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-3">
										<div class="product-item-price">
											<format:price priceData="${entry.basePrice}" displayFreeForZero="true" />
										</div>
									</div>
								</div>
							</div>
						</c:if>
					</c:forEach>
				</div>
	
				<div class="payment-type">
					<div class="row">
						<div class="col-xs-2 col-sm-2">
							<div class="payment-type-title">
								<spring:theme code="checkout.multi.payment" text="Payment:"></spring:theme>
							</div>
						</div>
						<div class="col-xs-10 col-sm-10">
							<div class="payment-type-info">
								<p>
									<c:if test="${not empty paymentInfo}">
										<c:choose>
											<c:when test="${paymentInfo.cardType eq 'PAYPAL'}">
												<spring:theme code="checkout.multi.payment.paypal" text="Paypal" />,
												<c:if test="${not empty paymentInfo.billingAddress}">
													${fn:escapeXml(paymentInfo.billingAddress.title)}${' '} ${fn:escapeXml(paymentInfo.billingAddress.firstName)}${' '} ${fn:escapeXml(paymentInfo.billingAddress.lastName)},
												</c:if>
											</c:when>
											<c:otherwise>
												${fn:escapeXml(paymentInfo.accountHolderName)},
												<c:if test="${not empty paymentInfo.billingAddress}">
													${fn:escapeXml(paymentInfo.billingAddress.title)}${' '}
												</c:if>
												${fn:escapeXml(paymentInfo.cardTypeData.name)}, ${fn:escapeXml(paymentInfo.cardNumber)}, ${paymentInfo.expiryMonth}/${paymentInfo.expiryYear},
											</c:otherwise>
										</c:choose>
										<c:if test="${not empty paymentInfo.billingAddress}">${fn:escapeXml(paymentInfo.billingAddress.line1)},
											<c:if test="${not empty paymentInfo.billingAddress.line2}">${fn:escapeXml(paymentInfo.billingAddress.line2)},</c:if>
											${fn:escapeXml(paymentInfo.billingAddress.town)}, ${fn:escapeXml(paymentInfo.billingAddress.region.name)}${' '}${fn:escapeXml(paymentInfo.billingAddress.postalCode)}, ${fn:escapeXml(paymentInfo.billingAddress.country.name)}
										</c:if>
									</c:if>
								</p>
							</div>
						</div>
					</div>
				</div>
	
				<div class="order-total-section">
					<div class="order-total-list">
						<div class="order-total-title">
							<spring:theme code="order.order.totals" />
						</div>
						<div class="order-total-item">
							<div class="row">
								<div class="col-xs-6 col-sm-6">
									<spring:theme code="basket.page.totals.subtotal" />
								</div>
								<div class="col-xs-6 col-sm-6 text-right price">
									<ycommerce:testId code="Order_Totals_Subtotal">
										<format:price priceData="${orderData.subTotalWithoutDiscount}" />
									</ycommerce:testId>
								</div>
							</div>
						</div>
						<c:if test="${orderData.totalDiscounts.value > 0}">
							<div class="order-total-item">
								<div class="row">
									<div class="col-xs-6 col-sm-6">
										<spring:theme code="basket.page.totals.discount" />
									</div>
									<div class="col-xs-6 col-sm-6 text-right price">
										<ycommerce:testId code="Order_Totals_Subtotal">
											-
											<format:price priceData="${orderData.totalDiscounts}" />
										</ycommerce:testId>
									</div>
								</div>
							</div>
						</c:if>
						<c:if test="${not empty orderData.appliedVouchers[0]}">
							<div class="order-total-item">
								<div class="row">
									<div class="col-xs-6 col-sm-6">
										<spring:theme code="text.voucher.discountCode" />:
									</div>
									<div class="col-xs-6 col-sm-6 text-right price">
										${orderData.appliedVouchers[0]}
									</div>
								</div>
							</div>
						</c:if>
						<c:if test="${showTax && orderData.taxCalculated}">
							<div class="order-total-item">
								<div class="row">
									<div class="col-xs-6 col-sm-6">${sessionCountry.taxName}:</div>
									<div class="col-xs-6 col-sm-6 text-right price">
										<format:price priceData="${orderData.totalTax}" />
									</div>
								</div>
							</div>
						</c:if>
						<multi-checkout-common:orderTotalsShippingItem abstractOrderData="${orderData}"/>
					</div>
					<div class="order-total-info order-total-title">
						<div class="row">
							<div class="col-xs-6 col-sm-6">
								<spring:theme code="basket.page.totals.total" />
							</div>
							<div class="col-xs-6 col-sm-6 text-right">
								<ycommerce:testId code="cart_totalPrice_label">
									<c:choose>
										<c:when test="${showTax}">
											<format:price priceData="${orderData.totalPriceWithTax}" />
										</c:when>
										<c:otherwise>
											<format:price priceData="${orderData.totalPrice}" />
										</c:otherwise>
									</c:choose>
								</ycommerce:testId>
							</div>
						</div>
					</div>
				</div>
			</div>
	
			<div class="buttons-section text-center">
				<a href="${myaccounturl}">
					<button class="button button-main large">
						<spring:theme code="header.link.account" />
					</button>
				</a>
			</div>
	
		</div>
		<div class="col-xs-12 col-sm-6">
			<div class="print-section checkout-desktop">
				<a href="#" class="icon-print">
					<spring:theme code="product.share.print" />
				</a>
			</div>
		</div>
	</div>
</template:page>