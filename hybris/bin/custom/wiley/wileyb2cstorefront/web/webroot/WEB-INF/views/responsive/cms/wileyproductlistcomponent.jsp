<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>

<c:if test="${not empty products}">
	<c:if test="${not empty headline}">
		<header class="wiley-product-list-component-header">
			<h1>${headline}</h1>
			<c:url value="${plpCustomLink}" var="plpCustomLinkUrl"/>
			<c:if test="${not empty plpCustomLinkUrl}">
				<span class="link-view-more right">
					<a class="link-corner" href="${plpCustomLinkUrl}"><spring:theme code="search.viewall" text="View all"/></a>
				</span>
			</c:if>
		</header>
	</c:if>
	<div class="wiley-product-list-component ${component.layout.code.concat(' ').concat(fn:join(cssClasses, ' '))}" style="${component.cssStyle}">

		<c:forEach items="${products}" var="product">
			<product:productListerItem product="${product}" showProductDescription="${component.showProductDescription}"
				showPurchaseOptions="${component.showPurchaseOptions}"/>
		</c:forEach>

	</div>
	<c:if test="${showAddToCartModalWindow eq true}">
		<product:addToCartPopupTemplate/>
	</c:if>
</c:if>