<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:url value="/paypal/login/registerWithPayPalRedirect" var="registerAndCheckoutActionUrl" scope="request" />

<jsp:include page="../../fragments/checkout/abstractCheckoutNewCustomerLogin.jsp" />