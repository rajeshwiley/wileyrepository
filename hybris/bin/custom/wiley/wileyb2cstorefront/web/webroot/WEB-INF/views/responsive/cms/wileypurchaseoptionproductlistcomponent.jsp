<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>

<section class="products-list-container">
	<c:if test="${not empty products}">
		<c:if test="${not empty headline}">
			<h3 class="products-title">${headline}</h3>
		</c:if>
		<div
			class="product-list ${component.layout.code.concat(' ').concat(fn:join(cssClasses, ' '))}"
			style="${component.cssStyle}">
			<c:forEach items="${products}" var="product">
				<product:productListerItem product="${product}" showDiscountedPrice="true" showSetComponents="true"/>
			</c:forEach>
		</div>
		<product:addToCartPopupTemplate />
	</c:if>
</section>