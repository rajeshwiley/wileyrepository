<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true"%>

<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<%@ attribute name="checkoutSteps" required="true" type="java.util.List" %>
<%@ attribute name="progressBarId" required="true" type="java.lang.String" %>

<div class="col-xs-12 col-sm-7">
	<div class="checkout-accordion">
		<c:forEach items="${checkoutSteps}" var="checkoutStep">
			<c:url value="${checkoutStep.url}" var="stepUrl"/>
			<c:if test="${progressBarId eq checkoutStep.progressBarId}">
				<c:set var="activeCheckoutStepIsCurrent" value="${true}"/>
			</c:if>
			<div class="checkout-accordion-item ${activeCheckoutStepIsCurrent ? 'current' : ''}">
				<div class="checkout-accordion-header">
					<a href="${stepUrl}">
						<span class="item-number">${checkoutStep.stepNumber}</span>
						<span class="item-text"><spring:theme code="checkout.multi.${checkoutStep.progressBarId}"/></span>
						<c:if test="${empty activeCheckoutStepIsCurrent}">
							<span class="icon-edit"></span>
						</c:if>
					</a>
				</div>
				<c:if test="${activeCheckoutStepIsCurrent}">
					<div class="checkout-accordion-content">
						<jsp:doBody/>
					</div>
					<c:set var="activeCheckoutStepIsCurrent" value="${false}"/>
				</c:if>
			</div>
		</c:forEach>
	</div>
</div>