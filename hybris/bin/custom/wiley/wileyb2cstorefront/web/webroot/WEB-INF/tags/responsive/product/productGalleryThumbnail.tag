<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ attribute name="galleryImages" required="true" type="java.util.List" %>

<div id="pdp-carousel-sync2">
	<c:forEach items="${galleryImages}" var="container" varStatus="varStatus">
		<c:set var="thumbClass" value="${not empty container.brightCoveVideo ? 'item brightcove-video' : 'item-image'}"/>
		<div class="item">
			<div class="thumbClass"><img class="lazyOwl" src="${container.thumbnail.url}" alt=""></div>
		</div>
	</c:forEach>
</div>