<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="content" tagdir="/WEB-INF/tags/responsive/content"%>

<div class="content-list">
	<content:contentSearchResultList searchPageData="${searchPageData}"/>
	<input type="hidden" name="_currentPage" value="${searchPageData.pagination.currentPage}"/>
	<input type="hidden" name="_numberOfPages" value="${searchPageData.pagination.numberOfPages}"/>
</div>