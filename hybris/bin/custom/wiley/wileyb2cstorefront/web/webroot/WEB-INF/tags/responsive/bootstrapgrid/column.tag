<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>

<%@ attribute name="column" required="true" type="com.wiley.bootstragrid.data.WileyBootstrapColumnData" %>

<%@ taglib prefix="c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri = "http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>

<c:set var="bootstrapClasses" value="${fn:join(column.bootstrapClasses, ' ')}"/>
<c:set var="cssClasses" value="${fn:join(column.cssClasses, ' ')}"/>
<c:set var="classes" value="${bootstrapClasses} ${cssClasses}"/>

<div class="${classes}" <c:if test = "${not empty column.cssStyle}">style="${column.cssStyle}"</c:if>>
    <c:forEach items="${column.componentUids}" var="componentUid" >
        <cms:component uid="${componentUid}" evaluateRestriction="true" element="div"/>
    </c:forEach>
</div>