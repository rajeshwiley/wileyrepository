<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>

<%@ attribute name="customer" required="true" type="de.hybris.platform.commercefacades.user.data.CustomerData" %>

<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${not empty fn:escapeXml(customer.title)}">
	${fn:escapeXml(customer.title)} ${' '}
</c:if>

<c:if test="${not empty fn:escapeXml(customer.firstName)}">
	${fn:escapeXml(customer.firstName)} ${' '}
</c:if>

<c:if test="${not empty fn:escapeXml(customer.lastName)}">
	${fn:escapeXml(customer.lastName)}
</c:if>

<c:if test="${not empty fn:escapeXml(customer.title) or not empty fn:escapeXml(customer.firstName) or not empty fn:escapeXml(customer.lastName)}">
	<br/>
</c:if>