<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>
<%@ attribute name="showDeliveryAddress" required="true" type="java.lang.Boolean" %>
<%@ attribute name="showPaymentInfo" required="false" type="java.lang.Boolean" %>
<%@ attribute name="showTax" required="false" type="java.lang.Boolean" %>
<%@ attribute name="showTaxEstimate" required="false" type="java.lang.Boolean" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<spring:url value="/checkout/multi/summary/placeOrder" var="placeOrderUrl"/>
<spring:url value="/checkout/multi/termsAndConditions" var="getTermsAndConditionsUrl"/>

<div class="col-xs-12 col-sm-4 col-sm-offset-1">
	<div class="order-brief-section checkout-desktop">
		<multi-checkout:deliveryCartItems cartData="${cartData}" showDeliveryAddress="${showDeliveryAddress}" />
		<multi-checkout:paymentInfo cartData="${cartData}" paymentInfo="${cartData.paymentInfo}" showPaymentInfo="${showPaymentInfo}" />
		<div class="order-total-section">
			<multi-checkout:orderTotalsSummary cartData="${cartData}" showTaxEstimate="${showTaxEstimate}" showTax="${showTax}" />
			<form:form action="${placeOrderUrl}" id="placeOrderForm1" commandName="placeOrderForm">
					<div class="order-terms-and-conds checkbox-component">
						<form:checkbox id="termsCondsMob" path="termsCheck"/>
						<spring:message code="checkout.summary.placeOrder.readTermsAndConditions.popup.link" var="termsAndConditionsPopupLink"/>
						<label for="termsCondsMob">
							<spring:theme code="checkout.summary.placeOrder.readTermsAndConditions" text="Terms and Conditions"/>
							&nbsp;<a class="termsAndConditionsLink" title="${termsAndConditionsPopupLink}" href="${getTermsAndConditionsUrl}">${termsAndConditionsPopupLink}</a>
						</label>
					</div>
					<button id="placeOrder" type="submit" class="button button-main large">
						<spring:theme code="checkout.summary.placeOrder" text="Place Order"/>
					</button>
			</form:form>
		</div>
	</div>
</div>
