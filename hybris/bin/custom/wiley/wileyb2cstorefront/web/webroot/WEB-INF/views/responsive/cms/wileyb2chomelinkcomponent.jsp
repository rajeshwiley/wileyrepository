<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:url value="${redirectURL}" var="encodedUrl" />

<c:choose>
	<c:when test="${empty encodedUrl || encodedUrl eq '#'}">
		<span class="empty-nav-item">${linkName}</span>
	</c:when>
	<c:otherwise>
		<a href="${encodedUrl}" title="${linkName}">${linkName}</a>
	</c:otherwise>
</c:choose>