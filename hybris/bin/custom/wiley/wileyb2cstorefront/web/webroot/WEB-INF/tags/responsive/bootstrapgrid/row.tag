<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="cssClasses" required="true" type="java.lang.String[]" %>
<%@ attribute name="cssStyle" required="false" type="java.lang.String" %>
<%@ attribute name="columns" required="true" type="java.util.List" %>

<%@ taglib prefix="c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri = "http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="bootstrapgrid" tagdir="/WEB-INF/tags/responsive/bootstrapgrid" %>

<div class="row ${fn:join(cssClasses, ' ')}" <c:if test="${not empty cssStyle}">style="${cssStyle}"</c:if>>
    <c:forEach var="column" items="${columns}">
        <bootstrapgrid:column column="${column}"/>
    </c:forEach>
</div>
