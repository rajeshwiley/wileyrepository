<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:set var="currentProduct" value="${product.productPurchaseOption}"/>



<div class="container">
    <div class="row product-details-wr">
        <div class="col-xs-12 col-sm-2">
            <!--Mobile title-->
            <h1 class="visible-xs">${product.name}</h1>
            <!--Gallery-->
            <product:productImagePanel galleryImages="${galleryImages}" product="${product}"/>
            <product:productExcerpts excerpts="${product.excerpts}" additionalClassForMainTag="hidden-xs"/>
            <product:productContentMenu />
        </div>
        <c:set var="hasPrice" value="${product.price ne null}"/>
        <c:set var="isJournal" value="false"/>
        <c:url var="authorSearchUrl" value="/search?pq=%7Crelevance%7Cauthor%3A"/>
        <div class="col-xs-12 col-sm-10 product-details-content">
            <section class="product-details-sidebar">
                <c:choose>
                    <c:when test="${currentProduct.purchaseOptionName eq 'Journal'}">
                        <div class="product-society-links">
                            <c:if test="${not empty product.societyLink}">
                                <c:choose>
                                    <c:when test="${product.societyLink.size() > 1}">
                                        <c:forEach items="${product.societyLink}" var="item">
                                            <p>
                                                    ${item.value}
                                            </p>
                                        </c:forEach>
                                    </c:when>
                                    <c:otherwise>
                                        <p>
                                                ${product.societyLink[0].value}
                                        </p>
                                    </c:otherwise>
                                </c:choose>
                            </c:if>
                            <c:if test="${not empty product.wileyPlusLink}">
                                <p>
                                    <a href="${product.wileyPlusLink.url}" target="_blank"
                                       class="wiley-plus-button link-corner visible-xs">
                                        <spring:theme code="product.product.details.wileyPlus.text"
                                                      text="Available on WileyPLUS"/>
                                    </a>
                                </p>
                            </c:if>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <c:set var="hasVariantMediaStores" value="false"/>
                        <c:if test="${not empty currentProduct.externalStores}">
                            <c:forEach items="${currentProduct.externalStores}" var="externalStore">
                                <c:if test="${externalStore.type eq 'GOOGLE_PLAY_STORE' or externalStore.type eq 'ITUNES_STORE'}">
                                    <c:set var="hasVariantMediaStores" value="true"/>
                                </c:if>
                            </c:forEach>
                        </c:if>
                        <c:choose>
                            <c:when test="${currentProduct.hasExternalStores}">
                                <c:if test="${hasVariantMediaStores}">
                                    <div class="product-add-to-cart">
                                        <div class="product-price-wr">
                                            <div class="type-of-book">
                                            <span class="hidden-xs"><spring:theme
                                                    code="product.details.purchase.option.type"/></span>
                                                <b>${currentProduct.purchaseOptionName}</b>
                                                <a role="menuitem" tabindex="0" aria-label="Purchase option description"
                                                   data-content='${fn:escapeXml(currentProduct.purchaseOptionDescription)}'
                                                   class="icon-info"></a>
                                            </div>
                                            <div class="add-to-cart">
                                                <div class="product-applications">
                                                    <c:forEach var="store" items="${currentProduct.externalStores}">
                                                        <c:choose>
                                                            <c:when test="${store.type eq 'ITUNES_STORE'}">
                                                                <a href="${store.url}" target="_blank">
                                                                    <img src="${commonResourcePath}/images/apple-app-store-icon@2x.png">
                                                                </a>
                                                            </c:when>
                                                            <c:when test="${store.type eq 'GOOGLE_PLAY_STORE'}">
                                                                <a href="${store.url}" target="_blank">
                                                                    <img src="${commonResourcePath}/images/google-play-badge@2x.png">
                                                                </a>
                                                            </c:when>
                                                        </c:choose>
                                                    </c:forEach>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </c:if>
                            </c:when>
                            <c:otherwise>
                                <div class="product-add-to-cart">
                                    <div class="type-of-book">
                                        <span class="hidden-xs"><spring:theme code="product.details.purchase.option.type"/></span>
                                        <b>${currentProduct.purchaseOptionName}</b>
                                    </div>

                                    <c:if test="${product.doesProductHaveQuantity and product.available and hasPrice and not currentProduct.hasExternalStores}">
                                        <div class="product-qty site-form">
                                            <div class="form-label">
                                                <label for="qty">
                                                <span class="hidden-xs">
                                                    <spring:theme code="text.quantity"/>:
                                                </span>
                                                </label>
                                            </div>
                                            <c:set var="qty" value="10"/>
                                            <div class="form-field">
                                                <div class="product-quantity">
                                                <span class="ui-spinner ui-corner-all ui-widget ui-widget-content"
                                                      style="height: 28px;">
                                                    <input id="qty" class="product-item-spinner ui-spinner-input"
                                                           aria-valuemin="0" autocomplete="off" role="spinbutton" aria-label="product quantity">
                                                </span>
                                                </div>
                                            </div>
                                        </div>
                                    </c:if>

                                    <div class="product-price-wr">
                                        <c:if test="${not currentProduct.hasExternalStores && not emptycurrentProductn.inventoryStatus && currentProduct.inventoryStatus.visible}">
                                            <p class="in-stock">
                                                <spring:theme
                                                        code="product.inventoryStatus.${currentProduct.inventoryStatus.statusCode}"/>
                                            </p>
                                        </c:if>
                                        <c:if test="${not currentProduct.hidePrice}">
                                            <p class="pr-price">${product.price.formattedValue}</p>
                                        </c:if>
                                        <c:if test="${product.price.optionalTaxShortMessage != null}">
                                        <span class="price-disclaimer">
                                            ${product.price.optionalTaxShortMessage}
                                            <a role="menuitem" tabindex="0" data-placement="bottom-right top-right"
                                               aria-label="information icon" data-container-class="wiley-tax-popover"
                                               data-content='${fn:escapeXml(product.price.optionalTaxTooltip)}'
                                               class="icon-info black"></a>
                                        </span>
                                        </c:if>
                                    </div>

                                    <div class="add-to-cart">
                                        <c:choose>
                                            <c:when test="${hasPrice and product.available}">
                                                <cms:pageSlot position="AddToCart" var="component">
                                                    <cms:component component="${component}"/>
                                                </cms:pageSlot>
                                            </c:when>
                                            <c:otherwise>
                                                <p>
                                                <span class="pr-not-available"> <spring:theme
                                                        code="product.not.available.message"/></span>
                                                </p>
                                            </c:otherwise>
                                        </c:choose>
                                    </div>
                                </div>
                            </c:otherwise>
                        </c:choose>
                    </c:otherwise>
                </c:choose>
                <product:productInformation2Panel/>
                <div>
                    <cms:pageSlot position="Banner" var="comp" element="div">
                        <cms:component component="${comp}"/>
                    </cms:pageSlot>
                </div>
            </section>
            <section class="product-details">
                <h1 class="hidden-xs">${product.name}</h1>
                <div class="product-summary">
                    <c:forEach items="${product.variantOptions}">
                        <c:if test="${option.purchaseOptionName eq 'Journal'}">
                            <c:set var="isJournal" value="true"/>
                        </c:if>
                    </c:forEach>
                    <p class="author">
                        <c:choose>
                            <c:when test="${isJournal || empty product.encodedAuthorsList}">
                                ${product.authors}
                            </c:when>
                            <c:otherwise>
                                <c:forTokens items="${product.authors}" delims="," var="author" varStatus="loop">
                                    <a class="product-authors" href='${authorSearchUrl}${product.encodedAuthorsList[loop.index]}'>
                                            ${author}
                                    </a><c:if test="${loop.index < fn:length(product.encodedAuthorsList) - 1}">, </c:if>
                                </c:forTokens>
                            </c:otherwise>
                        </c:choose>
                    </p>
                    <p>
                        <product:productDetailsPanelBook/>
                        <product:productDetailsPanelJournal/>
                    </p>
                </div>
            </section>
            <section class="choose-type-of-book">
                <product:productDetailsPurchaseOptions/>
                <div class="product-editions">
                    <product:productNextPrevEditionsPanel/>
                    <cms:component component="${comp}"/>
                </div>
                <product:productExcerpts excerpts="${product.excerpts}" additionalClassForMainTag="visible-xs"/>
            </section>
            <product:productInformation1Panel/>
            <cms:pageSlot position="MarketingInformation1" var="comp">
                <cms:component component="${comp}"/>
            </cms:pageSlot>
            <product:productInformation3Panel product="${product}"/>
            <product:productSeries/>
            <cms:pageSlot position="MarketingInformation2" var="comp">
                <c:if test="${not empty comp}">
                    <section class="page-section">
                        <cms:component component="${comp}"/>
                    </section>
                </c:if>
            </cms:pageSlot>
            <cms:pageSlot position="MarketingInformation3" var="comp">
                <c:if test="${not empty comp}">
                    <section class="page-section">
                        <cms:component component="${comp}"/>
                    </section>
                </c:if>
            </cms:pageSlot>
        </div>
    </div>
</div>
<product:addToCartPopupTemplate/>
