<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>

<c:if test="${purchaseOptions.regCodeActivationAvailable}">
	<div class="purchase-accordion-item wrap-inner-bordered">
		<div class="purchase-accordion-heading ${regCodeAccordion ? 'current' : ''}">
			<div class="heading-body">
				<div class="heading-left" data-toggle="collapse" data-parent="#accordion" href="#collapse1"><span class="heading-arrow"></span></div>
				<div class="heading-center" data-toggle="collapse" data-parent="#accordion" href="#collapse1" tabindex="0">
					<h2><spring:message code="wileypluscourse.purchase.option.registration-code.accordion-title" /></h2>
				</div>
				<spring:message code="wileypluscourse.purchase.option.registration-code.accordion.popover" var="accordion_popover_message" />
				<div class="heading-right"><span data-toggle="popover" data-content="${accordion_popover_message}" data-trigger="hover focus" class="popover-help" tabindex="0">&nbsp;</span></div>
			</div>
		</div>
		<div id="collapse1" class="panel-collapse collapse ${regCodeAccordion ? 'in' : ''}">
			<div class="purchase-accordion-body">
				<div class="registration-code">
					<div class="row">
						<div class="col-xs-12 col-sm-5">
							<div class="site-form">
								<c:url value="/ucart/wileyPlus/purchase/options" var="regCodeFormUrl"/>
								<form:form id="regCodeForm" method="post" commandName="regCodeActivationForm" action="${regCodeFormUrl}">
                                    <formElement:formInputBox idKey="regCode"
															  placeholder="wileypluscourse.purchase.option.registration-code.input-field.place-holder"
															  labelKey="" path="regCode" value="${invalidRegCode}"
															  ariaLabel="wileypluscourse.purchase.option.registration-code.input-field.place-holder"/>
                                    <input type="hidden" name="cartActivationInfo" value="${cartActivationInfo}"/>
                                    <input type="hidden" name="checksum" value="${checksum}"/>
									<div class="form-cell">
										<div class="form-field">
											<button class="button button-main large">
												<spring:message code="wileypluscourse.purchase.option.registration-code.button.continue" />
											</button>
										</div>
									</div>
								</form:form>
							</div>
						</div>
						<div class="col-xs-12 col-sm-7">
							<div class="row">
								<div class="col-xs-12 col-sm-5 col-sm-push-7">
									<div class="registration-code-info">
										<spring:message code="wileypluscourse.purchase.option.registration-code.button.info" />
									</div>
								</div>
								<div class="col-xs-12 col-sm-7 col-sm-pull-5">
									<img src="${commonResourcePath}/images/registration-code.png" alt="registration code" class="img-responsive">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</c:if>
