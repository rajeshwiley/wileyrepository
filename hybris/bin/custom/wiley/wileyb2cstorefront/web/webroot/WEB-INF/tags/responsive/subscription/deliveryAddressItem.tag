<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>

<%@ attribute name="deliveryAddress" required="true" type="de.hybris.platform.commercefacades.user.data.AddressData" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

${fn:escapeXml(deliveryAddress.line1)}
<c:if test="${not empty fn:escapeXml(deliveryAddress.line2)}">
	<c:if test="${not empty fn:escapeXml(deliveryAddress.line1)}">
		${", "}
	</c:if>
	${fn:escapeXml(deliveryAddress.line2)}
</c:if>
<c:if test="${not empty deliveryAddress.town}">
	${", "} ${fn:escapeXml(deliveryAddress.town)}
</c:if>
<c:if test="${not empty deliveryAddress.region}">
    ${", "} ${fn:escapeXml(deliveryAddress.region.name)}
</c:if>
<br/>
${fn:escapeXml(deliveryAddress.postalCode)}
<c:if test="${not empty deliveryAddress.country}">
	<c:if test="${not empty fn:escapeXml(deliveryAddress.postalCode)}">
		${", "}
	</c:if>
	${fn:escapeXml(deliveryAddress.country.name)}
</c:if>