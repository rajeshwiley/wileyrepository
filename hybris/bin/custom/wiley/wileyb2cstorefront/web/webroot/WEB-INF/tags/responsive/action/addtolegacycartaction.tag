<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="product" required="true" type="java.lang.Object"%>
<%@ attribute name="addToCartText" required="false" type="java.lang.String"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:if test="${empty addToCartText}">
	<spring:theme code="text.addToCart" var="addToCartText"/>
</c:if>
<c:url value="${sessionCountry.b2cLegacyCartUrl}" var="addToCartUrl"/>

<div class="addToCartTitle" style="display:none">
    <spring:theme code="basket.added.to.basket"/>
</div>
<spring:theme code="img.missingProductImage.responsive.product" text="/" var="missingImagePath"/>

<c:url value="/relatedProducts" var="relatedProductsUrl"/>
<form:form method="post" id="addToCartForm${product.code}" class="add_to_cart_form" action="${addToCartUrl}"
		data-related-products-url="${relatedProductsUrl}">
    <input type="hidden" class="country" name="ACOUNTRY" value="${sessionCountry.isocode}">
    <input type="hidden" class="currency" name="ACURRENCY" value="${sessionCurrency.isocode}">
    <c:if test="${not empty APROMCODE}">
    	<input type="hidden" name="APROMCODE" value="${APROMCODE}">
    </c:if>
    <c:if test="${not empty AFICECODE}">
    	<input type="hidden" name="AFICECODE" value="${AFICECODE}">
    </c:if>

    <c:if test="${not empty product.potentialPromotions}">
    	<input type="hidden" class="promocode" name="APROMCODE" value="${product.potentialPromotions[0].code}">
    </c:if>
    <input type="hidden" class="productAIsbn" name="AAISBN" value="${product.code}">

    <input type="hidden" class="productAQuantity" name="PAQUANT" value="1">
	<button type="submit" class="small-button add-to-cart-button js-add-to-cart">${addToCartText}</button>

	<c:set value="${ycommerce:productImageForTypeAndFormat(product, 'GALLERY', 'product')}" var="primaryImage"/>
	<c:set value="${primaryImage.url}" var="primaryImageUrl"/>
	<c:if test="${empty primaryImageUrl}">
		<c:url value="${missingImagePath}" var="primaryImageUrl" context="${originalContextPath ne null ? originalContextPath : ''}"/>
	</c:if>
	<fmt:formatDate value="${product.publicationDate}" pattern="${publicationDatePattern}" var="publicationDate"/>

	<span hidden class="js-add-to-cart-product"
		data-code="${product.code}"
		data-medium-type="${product.purchaseOptionName}"
		data-name="${product.name}"
		data-authors="${product.authorsWithoutRoles}"
		data-image="${primaryImageUrl}"
		data-publication-date="${publicationDate}"
		data-countable="${product.countable}">
	</span>
	
	<c:if test="${not empty product.productSetComponents}">
		<c:forEach items="${product.productSetComponents}" var="component">
			<fmt:formatDate value="${component.publicationDate}" pattern="${publicationDatePattern}" var="publicationDate"/>
			<span hidden class="js-add-to-cart-product-set-component"
            		data-parent-code="${product.code}"
            		data-code="${component.code}"
            		data-medium-type="${component.purchaseOptionName}"
            		data-name="${component.name}"
            		data-authors="${component.authors}"
            		data-image="${component.url}"
            		data-publication-date="${publicationDate}"
					data-countable="${component.countable}">
			</span>
		</c:forEach>
	</c:if>
</form:form>