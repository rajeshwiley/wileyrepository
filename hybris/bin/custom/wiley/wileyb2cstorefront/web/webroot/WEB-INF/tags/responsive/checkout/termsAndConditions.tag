<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div id="termsAndConditionsPopup" role="dialog" class="modal fade modalWindow">
   <div class="modal-dialog">
      <div class="modal-content">

         <div class="modal-header">
            <div class="row">
               <div class="col-xs-10 col-sm-10">
                  <div class="modal-title">
                     <spring:theme code="checkout.summary.placeOrder.termsAndConditions.popup.title"/>
                  </div>
               </div>
               <div class="col-xs-2 col-sm-2">
                  <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">&times;</span></button>
               </div>
            </div>
         </div>
		 <div class="modal-body terms-conditions">

			 <spring:url value="/checkout/multi/summary/view" var="summaryViewUrl"/>
			 <a name="top-anchor"></a>
			 
			 <cms:pageSlot position="TermsAndConditionsText" var="feature" element="div" class="termsAndConditions-section">
				<cms:component component="${feature}"/>
			 </cms:pageSlot>
		 </div>
               
      </div>
   </div>
</div>

