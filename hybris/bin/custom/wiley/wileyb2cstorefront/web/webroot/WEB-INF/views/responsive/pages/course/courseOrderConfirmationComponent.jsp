<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="course" tagdir="/WEB-INF/tags/responsive/course"%>

<c:if test="${orderType eq 'GRACE_PERIOD'}">
	<course:freeTrialConfirmation />
</c:if>
<c:if test="${orderType eq 'REGISTRATION_CODE_ACIVATION'}">
	<course:registrationCodeActivationConfirmation />
</c:if>
