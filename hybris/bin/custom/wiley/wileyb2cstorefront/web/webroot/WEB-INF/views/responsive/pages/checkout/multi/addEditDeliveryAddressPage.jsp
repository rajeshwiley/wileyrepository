<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/address"%>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<template:page pageTitle="${pageTitle}" hideHeaderLinks="true" containerCSSClass="checkout-shipping-information">
	<div class="row">
		<div class="col-xs-12 col-sm-12">
			<cms:pageSlot position="SideContent" var="feature" element="div" class="support-message">
				<cms:component component="${feature}"/>
			</cms:pageSlot>
			<h1 class="page-title icon-lock"><spring:theme code="checkout.multi.secure.checkout" text="Secure Checkout" /></h1>
		</div>
	</div>

		<div class="row">
			<multi-checkout:checkoutSteps checkoutSteps="${checkoutSteps}" progressBarId="${progressBarId}">
				<jsp:body>
					<ycommerce:testId code="checkoutStepOne">
						<multi-checkout:shipmentItemsSingleLine cartData="${cartData}" showDeliveryAddress="false" />
						<div class="checkout-accordion-content-wrap">
							<c:if test="${not empty deliveryAddresses}">
								<div class="checkout-shipping-address">
									<div class="checkout-content-subtitle"><spring:theme code="checkout.summary.shippingAddress" text="Shipping Address"></spring:theme></div>
									<div class="checkout-view-address-book">
										<button type="button" class="button button-main large"
												data-toggle="modal" data-target="#addressBookModelWnd">
											<spring:theme
													code="checkout.checkout.multi.deliveryAddress.viewAddressBook"
													text="Address Book" />
										</button>
									</div>
								</div>
							</c:if>


							<div class="checkout-shipping">
								<div class="checkout-indent">
									<div class="site-form">
										<div class="row">
											<div class="col-xs-12 col-sm-8">
												<address:addressFormSelector supportedCountries="${countries}"
																			 regions="${regions}" cancelUrl="${currentStepUrl}"
																										 country="${country}" />
											</div>
										</div>
									</div>
									<address:suggestedAddresses selectedAddressUrl="/checkout/multi/delivery-address/select" />
								</div>

								<multi-checkout:pickupGroups cartData="${cartData}" />
							</div>

							<div class="checkout-accordion-button-next">
								<button id="addressSubmit" type="button"
								class="button button-main large checkout-next"><spring:theme code="checkout.multi.deliveryAddress.continue" text="Next"/></button>
							</div>

						</div>

						<c:if test="${not empty deliveryAddresses}">
							<div id="addressBookModelWnd" role="dialog" class="modal fade modalWindow">
								<multi-checkout:selectAddressModal></multi-checkout:selectAddressModal>
							</div>
						</c:if>
					</ycommerce:testId>
				</jsp:body>

			</multi-checkout:checkoutSteps>

			<multi-checkout:checkoutOrderDetails cartData="${cartData}" showDeliveryAddress="false" showPaymentInfo="false" showTaxEstimate="false" showTax="false" />
		</div>
</template:page>
