<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="paymentInfo" required="true" type="de.hybris.platform.commercefacades.order.data.CCPaymentInfoData" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="account" tagdir="/WEB-INF/tags/responsive/account"%>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi" %>

  <c:set var="currentPaymentInfo" value="${subscriptionData.paymentInfo}"/>
  <div class="col-xs-3 col-sm-3">
	<p><b><spring:theme code="account.subscription.manage.paymentUpdate.label" text="Payment:"/></b></p>
  </div>
  <div class="col-xs-9 col-sm-6">
  	<c:choose>
  		<c:when test="${not empty paymentInfo}">
			<%-- Below is an exact copy from paymenyInfo.tag from multi-step checkout --%>
				<p>
					<c:choose>
						<c:when test="${paymentInfo.cardType eq 'PAYPAL'}">
							<spring:theme code="checkout.multi.payment.paypal" text="Paypal" />,
							<c:if test="${not empty paymentInfo.billingAddress}">
								${fn:escapeXml(paymentInfo.billingAddress.title)}${' '}
								${fn:escapeXml(paymentInfo.billingAddress.firstName)}${' '}
								${fn:escapeXml(paymentInfo.billingAddress.lastName)},
							</c:if>
						</c:when>
						<c:otherwise>
							<c:if test="${not empty paymentInfo.billingAddress}"> ${fn:escapeXml(paymentInfo.billingAddress.title)}</c:if>
							${fn:escapeXml(paymentInfo.accountHolderName)},
							<c:if test="${not empty paymentInfo.billingAddress}">
								${fn:escapeXml(paymentInfo.billingAddress.title)}${' '}
							</c:if>
							${fn:escapeXml(paymentInfo.cardTypeData.name)},
							${fn:escapeXml(paymentInfo.cardNumber)},
							${paymentInfo.expiryMonth}/${paymentInfo.expiryYear},
						</c:otherwise>
					</c:choose>
					<c:if test="${not empty paymentInfo.billingAddress}">${fn:escapeXml(paymentInfo.billingAddress.line1)},
						<c:if test="${not empty paymentInfo.billingAddress.line2}">${fn:escapeXml(paymentInfo.billingAddress.line2)},</c:if>
						${fn:escapeXml(paymentInfo.billingAddress.town)},
						${fn:escapeXml(paymentInfo.billingAddress.region.name)}${' '}${fn:escapeXml(paymentInfo.billingAddress.postalCode)},
						${fn:escapeXml(paymentInfo.billingAddress.country.name)}
					</c:if>
				</p>
  		</c:when>
  		<c:otherwise>
  			<p><spring:theme code="account.subscription.manage.paymentUpdate.absent" text="Not specified:"/></p>
  		</c:otherwise>
  	</c:choose>
  </div>
  <div class="col-xs-12 col-sm-3">
	<div class="my-account-button-small">
	<c:choose>
      		<c:when test="${not empty paymentInfos}">
	  <button data-toggle="modal" data-target="#savedCardSubscrpWnd" class="button button-main large">Update</button>
  		</c:when>
  		<c:otherwise>
	  <button data-toggle="modal" data-target="#savedCardSubscrpWnd" class="button button-main large" disabled="disabled">Update</button>
  		</c:otherwise>
  	</c:choose>
	</div>
  </div>

  <account:listOfCardsModal paymentInfos="${paymentInfos}"
  							chooseUrl="/my-account/subscriptions/${subscriptionData.code}/update-payment"
  							dialogId="savedCardSubscrpWnd"/>