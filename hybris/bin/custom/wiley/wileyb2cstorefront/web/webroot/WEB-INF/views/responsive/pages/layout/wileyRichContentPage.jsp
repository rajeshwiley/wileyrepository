<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>


<template:page pageTitle="${pageTitle}" containerCSSClass="container-fluid wiley-rich-content-page" clearContainerClass="true">
    <div class="row">

        <div class="hero-banner">
            <div class="simple-responsive-banner-component hero-banner-component">
                <cms:pageSlot position="TopContentBackground" var="comp">
                    <cms:component component="${comp}"/>
                </cms:pageSlot>
            </div>
            <div class="container">
                <cms:pageSlot position="TopContent" var="comp">
                    <article class="page-section">
                    <cms:component component="${comp}"/>
                    </article>
                </cms:pageSlot>
            </div>
        </div>

        <div class="container-fluid gray-bg teal-border">
            <div class="hero-banner-component">
                <cms:pageSlot position="MiddleContentBackground1" var="comp">
                    <cms:component component="${comp}"/>
                </cms:pageSlot>
            </div>
            <div class="container">
                <cms:pageSlot position="MiddleContent1" var="comp">
                    <cms:component component="${comp}"/>
                </cms:pageSlot>
            </div>
        </div>

        <div class="container">
            <div class="hero-banner-component" >
                <cms:pageSlot position="MiddleContentBackground2" var="comp">
                    <cms:component component="${comp}"/>
                </cms:pageSlot>
            </div>
            <div class="component-wrapper who-we-serve-component">
                <cms:pageSlot position="MiddleContent2" var="comp">
                    <cms:component component="${comp}"/>
                </cms:pageSlot>
            </div>
        </div>

        <div class="container">
            <div class="hero-banner-component" >
                <cms:pageSlot position="MiddleContentBackground3" var="comp">
                    <cms:component component="${comp}"/>
                </cms:pageSlot>
            </div>
            <div class="subjects-component component-wrapper">
                <cms:pageSlot position="MiddleContent3" var="comp">
                    <cms:component component="${comp}"/>
                </cms:pageSlot>
            </div>
        </div>

        <div class="container">
            <div class="hero-banner-component">
                <cms:pageSlot position="MiddleContentBackground4" var="comp">
                    <cms:component component="${comp}"/>
                </cms:pageSlot>
            </div>
            <div class="building-future-component component-wrapper">
                <cms:pageSlot position="MiddleContent4" var="comp">
                    <cms:component component="${comp}"/>
                </cms:pageSlot>
            </div>
        </div>

        <div class="container-fluid green-bg">
            <div class="hero-banner-component">
                <cms:pageSlot position="MiddleContentBackground5" var="comp">
                    <cms:component component="${comp}"/>
                </cms:pageSlot>
            </div>
            <c:set var="hasContentComponent" value="false"/>
            <cms:pageSlot position="MiddleContent5" var="comp">
                <c:if test="${not empty comp}">
                    <c:set var="hasContentComponent" value="true"/>
                </c:if>
            </cms:pageSlot>
            <div class="<c:if test="${hasContentComponent}">component-wrapper special-and-news-block-wrapper</c:if>">
                <cms:pageSlot position="MiddleContent5" var="comp">
                    <cms:component component="${comp}"/>
                </cms:pageSlot>
            </div>
        </div>

        <div class="container-fluid">
            <div class="hero-banner-component">
                <cms:pageSlot position="MiddleContentBackground6" var="comp">
                    <cms:component component="${comp}"/>
                </cms:pageSlot>
            </div>
            <c:set var="hasContentComponent" value="false"/>
            <cms:pageSlot position="MiddleContent6" var="comp">
                <c:if test="${not empty comp}">
                    <c:set var="hasContentComponent" value="true"/>
                </c:if>
            </cms:pageSlot>
            <div class="<c:if test="${hasContentComponent}">component-wrapper wiley-careers</c:if>">
                <cms:pageSlot position="MiddleContent6" var="comp">
                    <cms:component component="${comp}"/>
                </cms:pageSlot>
            </div>
        </div>
   </div>
</template:page>