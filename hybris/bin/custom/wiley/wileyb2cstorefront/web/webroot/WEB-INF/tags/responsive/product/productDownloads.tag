<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="downloadsTab" required="true" type="java.lang.String"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="tab-content-downloads">
    <div class="item">
        ${downloadsTab}
    </div>
</div>
