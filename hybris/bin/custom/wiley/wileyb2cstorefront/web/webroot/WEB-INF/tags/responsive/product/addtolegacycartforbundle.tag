<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ attribute name="bundle" required="true" type="com.wiley.facades.wileybundle.data.WileyBundleData" %>

<c:url value="${sessionCountry.b2cLegacyCartUrl}" var="addToCartUrl"/>

<spring:theme code="product.product.details.bundle.buyButtonText" var="addToCartText"/>
<spring:theme code="img.missingProductImage.responsive.product" text="/" var="missingImagePath"/>

<div class="addToCartTitle" style="display:none">
    <spring:theme code="basket.added.to.basket"/>
</div>

<c:url value="/relatedProducts" var="relatedProductsUrl"/>
<form:form method="post" id="addToCartForm${bundle.upsellProduct.code}" class="add_to_cart_form" action="${addToCartUrl}"
		data-related-products-url="${relatedProductsUrl}">
    <input type="hidden" class="country" name="ACOUNTRY" value="${sessionCountry.isocode}">
    <input type="hidden" class="currency" name="ACURRENCY" value="${sessionCurrency.isocode}">

    <c:if test="${not empty bundle.discountCode}">
		<input type="hidden" class="promocode" name="APROMCODE" value="${bundle.discountCode}">
	</c:if>

    <input type="hidden" id="productAIsbn" name="AAISBN" value="${bundle.sourceProduct.code}">
    <input type="hidden" id="productAQuantity" name="PAQUANT" value="1">
    <input type="hidden" id="productBIsbn" name="ABISBN" value="${bundle.upsellProduct.code}">
    <input type="hidden" id="productBQuantity" name="PBQUANT" value="1">

    <button type="submit" class="add-to-cart-button js-add-to-cart">${addToCartText}</button>

	<c:set value="${ycommerce:productImageForTypeAndFormat(bundle.sourceProduct, 'GALLERY', 'product')}" var="primaryImage"/>
	<c:set value="${primaryImage.url}" var="primaryImageUrl"/>
	<c:if test="${empty primaryImageUrl}">
		<c:url value="${missingImagePath}" var="primaryImageUrl" context="${originalContextPath ne null ? originalContextPath : ''}"/>
	</c:if>
	<fmt:formatDate value="${bundle.sourceProduct.publicationDate}" pattern="${publicationDatePattern}" var="publicationDate"/>
    <span hidden class="js-add-to-cart-product"
		data-code="${bundle.sourceProduct.code}"
		data-medium-type="${bundle.sourceProduct.purchaseOptionName}"
		data-name="${bundle.sourceProduct.name}"
		data-authors="${bundle.sourceProduct.authorsWithoutRoles}"
		data-image="${primaryImageUrl}"
		data-publication-date="${publicationDate}"
		data-countable="${bundle.sourceProduct.countable}">
	</span>

	<c:set value="${ycommerce:productImageForTypeAndFormat(bundle.upsellProduct, 'GALLERY', 'product')}" var="primaryImage"/>
	<c:set value="${primaryImage.url}" var="primaryImageUrl"/>
	<c:if test="${empty primaryImageUrl}">
		<c:url value="${missingImagePath}" var="primaryImageUrl" context="${originalContextPath ne null ? originalContextPath : ''}"/>
	</c:if>
	<fmt:formatDate value="${bundle.upsellProduct.publicationDate}" pattern="${publicationDatePattern}" var="publicationDate"/>
	<span hidden class="js-add-to-cart-product"
		data-code="${bundle.upsellProduct.code}"
		data-medium-type="${bundle.upsellProduct.purchaseOptionName}"
		data-name="${bundle.upsellProduct.name}"
		data-authors="${bundle.upsellProduct.authorsWithoutRoles}"
		data-image="${primaryImageUrl}"
		data-publication-date="${publicationDate}"
		data-countable="${bundle.upsellProduct.countable}">
	</span>
</form:form>