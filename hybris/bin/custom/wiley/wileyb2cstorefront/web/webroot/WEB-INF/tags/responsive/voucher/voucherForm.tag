<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>

<%@ attribute name="redeemVoucherUrl" required="true" type="java.lang.String" %>
<%@ attribute name="releaseVoucherUrl" required="true" type="java.lang.String" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>

<div class="order-promotion-code">
	<div class="promotion-field">
		<div class="site-form">

			<form:form method="POST" action="${redeemVoucherUrl}" modelAttribute="voucherForm">
				<div class="form-field input-group">
					<formElement:formInputBox idKey="cart.voucherCode" labelKey="" path="discountCode"
											  placeholder="basket.have.promotion.code" ariaLabel="basket.have.promotion.code" inputCSS="${voucherInputClass}"/>

					<div class="input-group-btn">
						<button type="submit" class="button button-main large">
							<spring:theme code="basket.apply" text="Apply"/>
						</button>
					</div>
				</div>
			</form:form>

			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<div class="row promo-message">
						<c:choose>
							<c:when test="${voucherRedeemed}">
								<div class="col-xs-12 col-sm-8">
									${voucherMessage}
								</div>
								<div class="col-xs-12 col-sm-4">
									<form:form method="POST" action="${releaseVoucherUrl}">
										<button type="submit" class="button button-outlined small">
											<spring:theme code="text.voucher.remove" text="Remove" />
										</button>
									</form:form>
								</div>
							</c:when>
							<c:when test="${voucherDenied}">
								<div class="col-xs-12 col-sm-12">
									<c:choose>
										<c:when test="${not empty voucherViolationMessages}" >
											<c:forEach items="${voucherViolationMessages}" var="violationMessage">
                                                <div><c:out value="${violationMessage}"/></div>
                                            </c:forEach>
										</c:when>
										<c:otherwise>
											<spring:theme code="text.voucher.error" text="Invalid Discount Code. Please try again."/>
										</c:otherwise>
									</c:choose>
								</div>
							</c:when>
						</c:choose>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
