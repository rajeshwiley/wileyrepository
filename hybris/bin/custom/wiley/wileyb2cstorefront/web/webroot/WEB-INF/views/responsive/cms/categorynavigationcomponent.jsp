<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>

<c:if test="${component.visible}">
    <c:forEach items="${component.navigationNode.children}" var="topLevelNode">
        <c:set value="true" var="linkWouldOpenInSameWindow"/>

        <c:if test="${not empty topLevelNode.entries}">
            <c:set value="${topLevelNode.entries[0].item}" var="linkItem"/>

            <c:set value="${cms:urlFromComponent(linkItem)}" var="urlForTopNode"/>
            <c:url value="${urlForTopNode}" var="urlForTopNodeResolved"/>

            <c:set value="${linkItem.target == null || linkItem.target == 'SAMEWINDOW'}" var="linkWouldOpenInSameWindow"/>
        </c:if>

        <c:if test="${empty urlForTopNodeResolved}">
            <c:url value="#" var="urlForTopNodeResolved"/>
        </c:if>

        <c:if test="${topLevelNode.visible}">
            <li>
                <a href="${urlForTopNodeResolved}" class="collapsed" ${linkWouldOpenInSameWindow eq true ? '' : 'target="_blank"'}>
                    ${topLevelNode.title}
                </a>
                <c:if test="${not empty topLevelNode.children}">
                    <div id="${topLevelNode.uid}" class="dropdown-menu collapse">
                        <ul class="dropdown-items">
                            <c:forEach items="${topLevelNode.children}" var="secondLevelNode">
                                <nav:mainMenuItems menuItem="${secondLevelNode}" wrapAfter="${component.wrapAfter}"/>
                            </c:forEach>
                        </ul>
                    </div>
                </c:if>
            </li>
        </c:if>
    </c:forEach>
</c:if>