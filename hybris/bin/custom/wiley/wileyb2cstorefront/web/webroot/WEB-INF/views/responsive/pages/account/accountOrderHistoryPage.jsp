<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="pagination" tagdir="/WEB-INF/tags/responsive/nav/pagination" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="wileycomProduct" tagdir="/WEB-INF/tags/addons/wileycomstorefrontcommons/responsive/product"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="wileycomNav" tagdir="/WEB-INF/tags/addons/wileycomstorefrontcommons/responsive/nav"%>

<spring:url value="/my-account/order/" var="orderDetailsUrl"/>
<c:set var="searchUrl" value="/my-account/orders?"/>

<c:if test="${empty searchPageData.results}">
    <div class="row">
        <div class="col-xs-12 col-sm-9">
                <h1 class="full-width-gray"><spring:theme code="text.account.orderHistory"/></h1>
        </div>
        <div class="col-xs-12 col-sm-9">
            <spring:theme code="text.account.orderHistory.noOrders" />
        </div>
    </div>
</c:if>
<c:if test="${not empty searchPageData.results}">
    <div class="row">
      <div class="col-xs-12 col-sm-8">
        <h1 class="full-width-gray"><spring:theme code="text.account.orderHistory"/></h1>
      </div>
      <div class="col-xs-12 col-sm-4">
        <div class="top-pagination">
          <div class="account-orderhistory-pagination">
            <wileycomNav:pagination searchPageData="${searchPageData}" searchUrl="${searchUrl}"/>
          </div>
        </div>
      </div>
    </div>
    <c:forEach items="${searchPageData.results}" var="order">
        <c:set value="${orderDetailsUrl}${order.code}" var="orderUrl"/>
        <div class="my-account-wrap wrap order-history">
          <div class="order-common-info">
            <div class="row">
              <div class="col-xs-12 col-sm-3"><spring:theme code="text.account.orderHistory.orderNumber"/>:
                  <c:choose>
                      <c:when test="${order.orderType == 'PURCHASE'}">
                        <a href="${orderUrl}">${order.code}</a>
                      </c:when>
                      <c:otherwise>
                         ${order.code}
                      </c:otherwise>
                  </c:choose>
              </div>
              <div class="col-xs-12 col-sm-3"><spring:theme code="text.account.orderHistory.orderDate"/>: <fmt:formatDate value="${order.placed}" pattern="${orderDateFormat}"/></div>
              <c:if test="${order.orderType == 'PURCHASE'}">
                <div class="col-xs-12 col-sm-3"><spring:theme code="text.account.orderHistory.orderTotal"/>: <format:price priceData="${order.total}" displayFreeForZero="false"/></div>
              </c:if>
              <c:if test="${order.orderType == 'PURCHASE' or order.orderType == 'FREE_TRIAL'}">
                <div class="col-xs-12 col-sm-3"><spring:theme code="text.account.orderHistory.orderStatus"/>: ${order.statusDisplay}</div>
			  </c:if>
            </div>
          </div>
          <div class="order-purchase">
            <div class="row">
              <div class="col-xs-12 col-sm-12"><spring:theme code="text.account.orderHistory.orderType"/>: <spring:theme code="text.account.orderHistory.orderType.${order.orderType}"/></div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12 col-sm-9">
              <div class="product-list subscription-product-list">

              <c:forEach items="${order.entries}" var="entry">
                <c:url value="${entry.product.url}" var="productUrl"/>
                <c:set value="false" var="productLinkAvailable"/><%-- TODO: A logic here should be added to distinguish wiley.com and Wileyplus products --%>
                  <div class="product-list-item">
                    <div class="product-list-image">
                        <c:choose>
                          <c:when test="${productLinkAvailable}">
                            <a href="${productUrl}">
                                <product:productPrimaryImage product="${entry.product}" format="thumbnail" />
                            </a>
                          </c:when>
                          <c:otherwise>
                             <product:productPrimaryImage product="${entry.product}" format="thumbnail" />
                          </c:otherwise>
                        </c:choose>
                    </div>
                    <div class="product-list-info">
                      <div class="product-list-title">
                          <c:choose>
                              <c:when test="${productLinkAvailable}">
                                  <a href="${productUrl}">
                                    ${entry.product.name}
                                  </a>
                              </c:when>
                              <c:otherwise>
                                    ${entry.product.name}
                              </c:otherwise>
                          </c:choose>
                      </div>
                      <div class="product-list-description">
                            <div class="description">
                              <div class="product-list-author">
                               <c:out value="${entry.product.authors}"/>
                            </div>
                            <div class="product-list-date">
                                <fmt:formatDate pattern="${productDateFormat}" value="${entry.product.dateImprint}"/>
                            </div>
                            <c:if test="${not empty entry.product.purchaseOptionType}">
                                <div class="product-list-date">
                                    ${entry.product.purchaseOptionType}
                                </div>
                            </c:if>
                            <div class="product-list-status">
                                <c:set var="entryStock" value="${entry.product.stock.stockLevelStatus.code}"/>

                                <c:forEach items="${entry.product.baseOptions}" var="option">
                                    <c:if test="${not empty option.selected and option.selected.url eq entry.product.url}">
                                        <c:forEach items="${option.selected.variantOptionQualifiers}" var="selectedOption">
                                            <c:set var="entryStock" value="${option.selected.stock.stockLevelStatus.code}"/>
                                        </c:forEach>
                                    </c:if>
                                </c:forEach>
                                <wileycomProduct:CartItemInventoryStatusBlock entry="${entry}" showReleaseDate="true"/>
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>
              </c:forEach>

              </div>
            </div>
            <div class="col-xs-12 col-sm-3 text-right">
              <div class="my-account-button-small">
                <c:if test="${order.orderType == 'PURCHASE'}">
                    <form method="GET" action="${orderUrl}">
                        <button href="${orderUrl}"  type="submit" class="button button-main large">
                            <spring:theme code="text.account.orderHistory.viewOrderDetails"/>
                        </button>
                    </form>
                </c:if>
              </div>
            </div>
          </div>
        </div>
    </c:forEach>
    <div class="row">
      <div class="col-sm-12">
         <wileycomNav:pagination searchPageData="${searchPageData}" searchUrl="${searchUrl}"/>
      </div>
    </div>
</c:if>