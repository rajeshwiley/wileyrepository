<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ attribute name="product" required="true"
              type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="action" tagdir="/WEB-INF/tags/responsive/action" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:forEach items="${product.productSets}" var="set">
    <c:set var="productSet" value="${set.productSet}"/>
    <c:set var="setComponents" value="${productSet.productSetComponents}"/>
    <section id="get-discount-section" class="get-discount page-section">
        <div class="get-discount-wr">
            <div class="get-discount-inner-wr">
                <div class="discount-description">
                    <div class="discount-item-description">
                        <c:url value="${productSet.url}" var="productSetUrl"/>
                        <div class="discount-item-image">
                            <a href="${productSetUrl}">
                                <product:productPrimaryReferenceImage product="${productSet}" format="thumbnail"/>
                            </a>
                        </div>
                        <h4 class="discount-title">
                                <spring:theme code="product.product.details.set.buy.set.of.n.items"
                                              arguments="${setComponents.size()}"/>
                        </h4>
                        <p class="discount-item-common">
                            <b><spring:theme code="product.product.details.set.thisItem"
                                             text="This item"/>:&nbsp;</b>${product.name}</p>
                        <div class="discount-items">
                            <c:forEach items="${setComponents}" var="component">
                                <c:if test="${product.code != component.code}">
                                    <div class="discount-item">
                                        <p class="discount-item-additional">
                                            <c:choose>
                                                <c:when test="${component.showPdpUrl}">
                                                    <c:url value="${component.url}" var="componentUrl"/>
                                                    <a href="${componentUrl}" class="">${component.name}</a>
                                                </c:when>
                                                <c:otherwise>
                                                    ${component.name}
                                                </c:otherwise>
                                            </c:choose>
                                        </p>
                                        <p class="discount-item-author">
                                            <i>
                                                (${component.variantType}&nbsp;<format:price priceData="${component.price}"/>)
                                            </i>
                                        </p>
                                    </div>
                                </c:if>
                            </c:forEach>
                        </div>
                    </div>
                </div>
                <div class="discount-price-info">
                    <div class="discount-item-price">
                        <c:if test="${not empty set.originalPrice}">
                            <p class="orign-price"><spring:theme code="product.product.details.set.originalPrice"
                                                                 text="Original Price"/>:<span><format:price
                                    priceData="${set.originalPrice}"/></span>
                            </p>
                        </c:if>
                        <p class="discount-price"><spring:theme code="product.product.details.set.purchasedTogether"
                                                               text="Purchased together"/>:<span><format:price
                                priceData="${productSet.price}"/></span>
                        </p>
                        <c:if test="${not empty set.save}">
                            <p class="saved-price"><spring:theme code="product.product.details.set.save"
                                                                 text="save"/>&nbsp;<format:price priceData="${set.save}"/></p>
                        </c:if>
                        <c:if test="${not empty productSet.price.optionalTaxShortMessage}">
                            <p class="price-disclaimer">
                                    ${productSet.price.optionalTaxShortMessage}
                                <a role="menuitem" tabindex="0" data-placement="bottom-right top-right"
                                   aria-label="information icon" data-container-class="wiley-tax-popover"
                                   data-content='${fn:escapeXml(productSet.price.optionalTaxTooltip)}'
                                   class="icon-info black"></a>
                            </p>
                        </c:if>
                        <spring:theme code="product.product.details.set.buy.all.now" var="addToCartText"/>
                        <action:addtolegacycartaction product="${productSet}" addToCartText="${addToCartText}"/>
                    </div>
                </div>
            </div>
        </div>
    </section>
</c:forEach>