<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>
<%@ taglib prefix='spring' uri='http://www.springframework.org/tags' %>

<c:if test="${purchaseOptions.gracePeriodDuration > 0}">
	<spring:message code="purchase.option.grace.period.name" arguments="${purchaseOptions.gracePeriodDuration}" var="displayedName"/>
	<spring:message code="purchase.option.grace.period.description1" var="displayedDescription1"/>
	<spring:message code="purchase.option.grace.period.description2" arguments="${purchaseOptions.gracePeriodDuration}" var="displayedDescription2"/>
	<spring:message code="purchase.option.grace.period.button.name" var="buttonName"/>
	
	<div class="purchase-accordion-item wrap-inner-bordered">
		<div class="purchase-accordion-heading">
			<div class="heading-body">
				<div class="heading-left" data-toggle="collapse" data-parent="#accordion" href="#collapse3"><span class="heading-arrow"></span></div>
				<div class="heading-center" data-toggle="collapse" data-parent="#accordion" href="#collapse3" tabindex="0">
					<h2>${displayedName}</h2>
				</div>
				<spring:message code="wileypluscourse.purchase.option.instant-access.accordion.popover" var="accordion_popover_message"/>
				<div class="heading-right"><span data-toggle="popover" data-content="${displayedDescription2}" data-trigger="hover focus" class="popover-help" tabindex="0">&nbsp;</span></div>
			</div>
		</div>
		<div id="collapse3" class="panel-collapse collapse">
			<div class="purchase-accordion-body">
				<div class="free-trial">
					<div class="purchase-option-description">
						<p>${displayedDescription1}</p>
						<p>${displayedDescription2}</p>
					</div>
					<c:url value="/checkout/grace-period" var="gracePeriodFormUrl"/>
					<form action="${gracePeriodFormUrl}">
					    <input type="hidden" name="cartActivationInfo" value="${cartActivationInfo}"/>
					    <input type="hidden" name="checksum" value="${checksum}"/>
						<button class="button button-main large" type="submit">${buttonName}</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</c:if>