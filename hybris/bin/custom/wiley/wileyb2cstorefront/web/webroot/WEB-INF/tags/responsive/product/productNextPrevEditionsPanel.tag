<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="isPrevExists" value="false"/>
<c:set var="isNextExists" value="false"/>

<c:forEach items="${product.productReferences}" var="productReference">
    <c:if test="${productReference.referenceType eq 'PREVIOUS_EDITION'}">
        <c:url value="${productReference.target.url}" var="prevUrl"/>
        <c:set var="isPrevExists" value="true"/>
    </c:if>
    <c:if test="${productReference.referenceType eq 'LATEST_EDITION'}">
        <c:url value="${productReference.target.url}" var="nextUrl"/>
        <c:set var="isNextExists" value="true"/>
    </c:if>
</c:forEach>

<c:if test="${isPrevExists or isNextExists}">

    <span><spring:theme code="product.editions.name"/></span>
    <c:choose>
        <c:when test="${isPrevExists}">
            <a href="${prevUrl}" title="" class="prev">
                <spring:theme code="product.previous.edition"/>
            </a>
        </c:when>
        <c:otherwise>
            <span class="prev"><spring:theme code="product.previous.edition"/></span>
        </c:otherwise>
    </c:choose>

    <c:choose>
        <c:when test="${isNextExists}">
            <a href="${nextUrl}" title="" class="next">
                <spring:theme code="product.latest.edition"/>
            </a>
        </c:when>
        <c:otherwise>
            <span  class="next"><spring:theme code="product.latest.edition"/></span>
        </c:otherwise>
    </c:choose>

</c:if>
