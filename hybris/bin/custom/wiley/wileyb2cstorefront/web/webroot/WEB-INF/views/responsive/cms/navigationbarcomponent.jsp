<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>

<c:set value="${component.styleClass} ${dropDownLayout}" var="bannerClasses"/>

<c:if test="${component.visible}">

    <li>
        <cms:component component="${component.link}" evaluateRestriction="true"/>

		<c:if test="${not empty component.navigationNode.children}">
			<div class="dropdown-menu">
				<div class="dropdown-items-wrapper">
					<nav:menuDropdownItems navNodes="${component.navigationNode.children}"/>
				</div>
			</div>
		</c:if>
	</li>
</c:if>