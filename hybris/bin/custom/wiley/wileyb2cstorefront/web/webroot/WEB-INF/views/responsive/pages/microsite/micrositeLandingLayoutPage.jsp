<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>

<template:page pageTitle="${pageTitle}" containerCSSClass="container-fluid" clearContainerClass="true">
	<div class="row">
		<div class="container">
			<cms:pageSlot position="Breadcrumb" var="feature">
				<cms:component component="${feature}"/>
			</cms:pageSlot>
		</div>
		<div class="hero-banner collection-banner">
	    	<div class="hero-banner-component">
			   <cms:pageSlot position="TopContentBackground" var="feature">
				   <cms:component component="${feature}"/>
			   </cms:pageSlot>
		    </div>
			<div class="container">
				<article class="section-description page-section">
					<cms:pageSlot position="TopContent" var="feature">
						<cms:component component="${feature}"/>
					</cms:pageSlot>
				</article>
			</div>
		</div>
		
		<div class="container page-content-wrapper">
			<cms:pageSlot position="BodyContent" var="feature">
				<cms:component component="${feature}"/>
			</cms:pageSlot>            
		</div>
	</div>
</template:page>
