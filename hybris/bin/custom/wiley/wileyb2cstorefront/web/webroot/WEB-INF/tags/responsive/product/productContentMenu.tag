<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>

<div class="page-sections-menu">
    <ul>
        <c:if test="${not empty product.description}">
            <li>
                <product:productContentMenuLinkItem link="#description-section" title="product.product.details.description"/>
            </li>
        </c:if>
        <c:if test="${not empty product.evaluationCopyLinks}">
            <li>
                <product:productContentMenuLinkItem link="#evaluation-copy-section"
                                                    title="product.product.details.evaluation.copy"/>
            </li>
        </c:if>
        <c:if test="${not empty product.instructorLinks or not empty product.studentLinks}">
            <li>
                <product:productContentMenuLinkItem link="#companion-sites-section" title="product.companionSiteLink.header"/>
            </li>
        </c:if>
        <c:if test="${not empty product.brightcoveVideo}">
            <li>
                <product:productContentMenuLinkItem link="#pdp-video-section" title="product.product.details.video"/>
            </li>
        </c:if>
        <c:if test="${not empty product.wileyBundles}">
            <c:forEach items="${product.wileyBundles}" var="bundle" varStatus="loop">
                <li>
                    <c:set var="bundleTitle">
                        <spring:theme code="product.product.details.bundle.title" arguments="${bundle.discountValue}"
                                      text="Buy Both and Save"/>
                    </c:set>
                    <product:productContentMenuLinkItem link="#get-discount-bundle-section-${loop.index}" title="${bundleTitle}"/>
                </li>
            </c:forEach>
        </c:if>
        <c:if test="${not empty product.productSets}">
            <c:forEach items="${product.productSets}" var="set">
                <li>
                    <product:productContentMenuLinkItem link="#get-discount-section" title="${set.productSet.name}"/>
                </li>
            </c:forEach>
        </c:if>

        <product:productMarketingContentMenuItems menuItems="${marketingContentMenuItems}"
                                                  marketingSlotName="MarketingInformation1"/>

        <c:if test="${not empty product.aboutAuthors}">
            <li>
                <product:productContentMenuLinkItem link="#author-section" title="product.product.details.aboutAuthors"/>
            </li>
        </c:if>
        <c:if test="${not empty product.requestPermissionsLink}">
            <li>
                <product:productContentMenuLinkItem link="#permission-section" title="product.product.details.permissions.title"/>
            </li>
        </c:if>
        <c:if test="${not empty product.tableOfContents}">
            <li>
                <product:productContentMenuLinkItem link="#content-section" title="product.product.details.tableOfContents"/>
            </li>
        </c:if>
        <c:if test="${not empty product.newToEdition}">
            <li>
                <product:productContentMenuLinkItem link="#new-to-this-edition-section"
                                                    title="product.product.details.newToEdition"/>
            </li>
        </c:if>
        <c:if test="${not empty product.wileyReviews}">
            <li>
                <product:productContentMenuLinkItem link="#reviews-section" title="product.product.details.reviews"/>
            </li>
        </c:if>
        <c:if test="${not empty product.relatedWebsites}">
            <li>
                <product:productContentMenuLinkItem link="#extra-section" title="product.product.details.extra"/>
            </li>
        </c:if>
        <c:if test="${not empty product.downloadsTab}">
            <li>
                <product:productContentMenuLinkItem link="#downloads-section" title="product.product.details.downloads"/>
            </li>
        </c:if>
        <c:if test="${not empty product.errata}">
            <li>
                <product:productContentMenuLinkItem link="#errata-section" title="product.product.details.errata"/>
            </li>
        </c:if>
        <c:if test="${not empty product.notes}">
            <li>
                <product:productContentMenuLinkItem link="#notes-section" title="product.product.details.notes"/>
            </li>
        </c:if>
        <c:if test="${not empty product.whatsNewToDisplay}">
            <li>
                <product:productContentMenuLinkItem link="#whats-new-section" title="product.product.details.whatsNew"/>
            </li>
        </c:if>
        <c:if test="${not empty product.pressRelease}">
            <li>
                <product:productContentMenuLinkItem link="#press-release-section"
                                                    title="product.product.details.pressRelease"/>
            </li>
        </c:if>
        <c:if test="${not empty product.series}">
            <li>
                <product:productContentMenuLinkItem link="#series-section" title="product.product.details.series"/>
            </li>
        </c:if>

        <product:productMarketingContentMenuItems menuItems="${marketingContentMenuItems}"
                                                  marketingSlotName="MarketingInformation2"/>

        <product:productMarketingContentMenuItems menuItems="${marketingContentMenuItems}"
                                                  marketingSlotName="MarketingInformation3"/>
    </ul>
</div>