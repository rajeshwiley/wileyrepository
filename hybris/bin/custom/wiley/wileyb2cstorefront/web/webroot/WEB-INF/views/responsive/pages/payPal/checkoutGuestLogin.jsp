<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:set var="createAccountDataTarget" scope="request" value="/paypal/login/createAccountWithPayPalRedirect" />
<c:set var="guestCheckoutDataTarget" scope="request" value="/paypal/login/guestWithPayPalRedirect" />

<jsp:include page="../../fragments/checkout/abstractCheckoutGuestLogin.jsp" />