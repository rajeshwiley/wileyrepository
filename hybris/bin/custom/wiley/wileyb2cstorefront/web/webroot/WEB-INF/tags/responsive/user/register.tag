<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="actionNameKey" required="true"
    type="java.lang.String"%>
<%@ attribute name="action" required="true" type="java.lang.String"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement"
    tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>

<spring:theme code="updatePwd.pwd.invalid" var="errorMessageVar"/>
<spring:theme code="form.global.error" var="globalErrorMessageVar"/>
<div class="site-form central-form">
    <div class="form-cell heading">
        <h1><spring:theme code="checkout.login.registerAndCheckout" /></h1>
    </div>

    <form:form method="post" commandName="registerForm" action="${action}">

        <form:hidden path="titleCode" id="titleField"/>
        <form:hidden path="email" id="emailField"/>

        <div class="form-cell">
            <%-- Disabled input to display the email address entered by the user on previous page. --%>

            <div class="form-label required">
                <label for="emailAddress"><spring:theme code="register.email"/></label>
            </div>
            <div class="form-field">
                <span id="emailAddress" type="email" data-email="${emailAddress}">${emailAddress}</span>
            </div>
        </div>

        <formElement:formInputBox idKey="firstName" placeholder="guest.register.firstname.placeholder"
            labelKey="register.firstName" path="firstName"
            mandatory="true" />

        <formElement:formInputBox idKey="lastName" placeholder="guest.register.lastname.placeholder"
            labelKey="register.lastName" path="lastName"
            mandatory="true" />

        <formElement:formPasswordBox idKey="password" labelKey="register.pwd"
            path="pwd" inputCSS="password-strength" mandatory="true" errorMessage="${errorMessageVar}"
             globalErrorMessage="${globalErrorMessageVar}"/>

        <formElement:formPasswordBox idKey="checkPwd"
            labelKey="register.checkPwd" path="checkPwd"
            mandatory="true" />

        <div class="form-cell">
            <div class="form-field checkbox-component">
                <input type="checkbox" id="accept-promotions">
                <label for="accept-promotions"><spring:theme code="guest.register.promotions.message" /></label>
            </div>
        </div>

        <input type="hidden" id="recaptchaChallangeAnswered"
            value="${requestScope.recaptchaChallangeAnswered}" />

        <div class="form-cell">
            <ycommerce:testId code="register_Register_button">
                <button type="submit" class="button button button-main large registerButton" id="createAccount">
                    <spring:theme code='${actionNameKey}' />
                </button>
            </ycommerce:testId>
        </div>

    </form:form>
</div>
