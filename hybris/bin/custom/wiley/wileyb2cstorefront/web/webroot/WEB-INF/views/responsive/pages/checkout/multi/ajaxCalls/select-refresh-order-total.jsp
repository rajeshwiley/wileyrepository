<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi"%>

<c:set var="checkoutShipmentList">
    <multi-checkout:shipmentItems cartData="${cartData}" showDeliveryAddress="true" />
</c:set>
<c:set var="orderDetailsSection">
    <multi-checkout:checkoutOrderDetails cartData="${cartData}"
    				showDeliveryAddress="true" showPaymentInfo="false"
    				showTaxEstimate="false" showTax="false" />
</c:set>
<elements>
    <element>
        <key><c:out value="orderDetailsSection"/></key>
        <value><c:out value="${orderDetailsSection}" escapeXml="true"/></value>
    </element>
    <element>
        <key><c:out value="checkoutShipmentList"/></key>
        <value><c:out value="${checkoutShipmentList}" escapeXml="true"/></value>
    </element>
</elements>
