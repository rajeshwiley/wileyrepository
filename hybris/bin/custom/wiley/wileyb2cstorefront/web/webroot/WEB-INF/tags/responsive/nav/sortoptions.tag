<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="searchPageData" required="true" type="de.hybris.platform.commerceservices.search.pagedata.SearchPageData" %>
<%@ attribute name="msgKey" required="false" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:set var="themeMsgKey" value="${not empty msgKey ? msgKey : 'search.page'}"/>
 <div class="sort-panel">
      <span class="sort-panel-title" aria-labelledby="sortOptions"><spring:theme code="search.page.sortTitle"/></span>
	  <div class="select-component sort-panel-component">
		 <form id="sortForm" name="sortForm" method="get" action="#" autocomplete="off">
			<select id="sortOptions" name="sort" class="form-control" aria-label="sortOptions">
				<c:forEach items="${searchPageData.sorts}" var="sort">
					<option value="${sort.code}" ${sort.selected? 'selected="selected"' : ''}>
						<c:choose>
							<c:when test="${not empty sort.name}">
								${sort.name}
							</c:when>
							<c:otherwise>
								<spring:theme code="${themeMsgKey}.sort.${sort.code}"/>
							</c:otherwise>
						</c:choose>
					</option>
				</c:forEach>
			</select>
		</form>
	  </div>
   </div>
