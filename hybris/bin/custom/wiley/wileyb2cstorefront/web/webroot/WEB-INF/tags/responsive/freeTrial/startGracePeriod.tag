<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>

<spring:url value="/checkout/grace-period" var="gracePeriodUrl"/>
<div class="container free-trial wrap">
	<div class="row">
		<div class="col-xs-12 col-sm-6">
			<h1 class="full-width-gray"><spring:theme code="text.startFreeTrial.title"/></h1>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-6 left-block">
			<div class="product-list wrap">
				<div class="product-list-item">
                    <div class="product-list-image">
					    <img src="${course.imageUrl}"/>
                    </div>
					<div class="row product-list-info">
						<div class="col-xs-10 col-sm-9">
							<div class="product-list-title"> ${course.title} </div>
							<div class="product-list-description">
								<div class="product-list-author">${course.authors}</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="site-form">
				<form:form id="updateProfileForm" method="post" action="${gracePeriodUrl}" commandName="updateProfileForm">
					<div class="form-wrap wrap">
						<div class="row">
						    <div class="col-xs-12 col-sm-6">
							    <formElement:formInputBox idKey="profile.firstName" labelKey="profile.firstName" path="firstName" inputCSS="text" mandatory="true" placeholder="profile.firstName.placeholder"/>
							</div>
							<div class="col-xs-12 col-sm-6">
                                <formElement:formInputBox idKey="profile.lastName" labelKey="profile.lastName" path="lastName" inputCSS="text" mandatory="true" placeholder="profile.lastName.placeholder"/>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-6">
								<formElement:formInputBox idKey="profile.email" labelKey="text.startFreeTrial.email" path="email" inputCSS="text" mandatory="true" placeholder="profile.email.placeholder" readonly="true" />
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-6">
                                <formElement:formInputBox idKey="profile.studentId" labelKey="profile.studentId" path="studentId" inputCSS="text" mandatory="false" placeholder="profile.studentId.placeholder"/>
							</div>
							<div class="col-xs-12 col-sm-6">
                                <formElement:formInputBox idKey="profile.major" labelKey="profile.major" path="major" inputCSS="text" mandatory="false" placeholder="profile.major.placeholder"/>
							</div>
						</div>
						<div class="row">
                              <div class="col-xs-12 col-sm-12">
                                  <div class="form-label required">
                                      <label for="month">
                                            <spring:theme code="profile.graduationDate"/>
                                            <span class="skip">
                                                <form:errors path="graduationMonth"/>
                                            </span>
                                      </label>
                                  </div>
                              </div>
                          </div>
                          <div class="row">
                              <div class="col-xs-12 col-sm-6">
                                  <div class="form-cell">
                                      <div class="form-field select-component select-form-element">
                                        <form:select id="month" path="graduationMonth" cssClass="form-control">
                                           <form:option value="" disabled="true" selected="true"><spring:theme code="profile.month.placeholder"/></form:option>
                                            <form:options items="${months}" itemValue="code" itemLabel="name"/>
                                        </form:select>
                                      </div>
                                  </div>
                              </div>
                              <div class="col-xs-12 col-sm-6">
                                  <div class="form-cell">
                                      <div class="form-field select-component select-form-element">
                                        <form:select id="year" path="graduationYear" cssClass="form-control">
                                           <form:option value="" disabled="true" selected="true"><spring:theme code="profile.year.placeholder"/></form:option>
                                            <form:options items="${years}" itemValue="code" itemLabel="name"/>
                                        </form:select>
                                      </div>
                                  </div>
                              </div>
						</div>
					</div>
					<div class="button-wrap">
						<button class="button button-main large"><spring:theme code="text.startFreeTrial.continueBtn"/></button>
					</div>
					<input type="hidden" name="cartActivationInfo" value="${cartActivationInfo}"/>
					<input type="hidden" name="checksum" value="${checksum}"/>
				</form:form>
			</div>
		</div>
	</div>
</div>