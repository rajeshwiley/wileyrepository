<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="freeTrial" tagdir="/WEB-INF/tags/responsive/freeTrial" %>

<c:set var="continueButtonDisabled" value="${isCustomerHasSubscription? 'disabled=&#39;disabled&#39;': ''}"/>

<div class="container free-trial wrap">
	<div class="row">
	  <div class="col-xs-12 col-sm-6">
		<h1 class="full-width-gray"><spring:theme code="text.startFreeTrial.title"/></h1>
	  </div>
	</div>
	<div class="row">
	  <div class="col-xs-12 col-sm-6 left-block">
	  
	  	<freeTrial:freeTrialSubscriptionDetails/>
	  
		<div class="site-form">
		  <form:form id="wileyb2cFreeTrialForm" method="post" commandName="wileyb2cFreeTrialForm">
			<div class="form-wrap wrap">
			  <div class="row">
				<div class="col-xs-12 col-sm-6">
				  <div class="form-cell">
					<formElement:formInputBox idKey="startFreeTrial.firstName" labelKey="text.startFreeTrial.firstName" path="firstName" inputCSS="text"
					  mandatory="true" placeholder="text.startFreeTrial.firstName.placeholder" />
				  </div>
				</div>
				<div class="col-xs-12 col-sm-6">
				  <div class="form-cell">
				    <formElement:formInputBox idKey="startFreeTrial.lastName" labelKey="text.startFreeTrial.lastName" path="lastName" inputCSS="text"
                    					  mandatory="true" placeholder="text.startFreeTrial.lastName.placeholder" />
				  </div>
				</div>
			  </div>
			  <div class="row">
				<div class="col-xs-12 col-sm-6">
				  <div class="form-cell">
					<formElement:formInputBox idKey="startFreeTrial.email" labelKey="text.startFreeTrial.email" path="email" inputCSS="text"
                                        					 readonly="true" disabled="true" mandatory="true" placeholder="text.startFreeTrial.email.placeholder" />
				  </div>
				</div>
			  </div>
			</div>
			<div class="button-wrap">
			  <button class="button button-main large" ${continueButtonDisabled}><spring:theme code="text.startFreeTrial.continueBtn"/></button>
			</div>
		  </form:form>
		</div>
	  </div>
	</div>
</div>