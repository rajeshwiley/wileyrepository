<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ attribute name="bundle" required="true" type="com.wiley.facades.wileybundle.data.WileyBundleData" %>
<%@ attribute name="index" required="true" %>

<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>

<c:set var="sourceProduct" value="${bundle.sourceProduct}"/>
<c:set var="upsellProduct" value="${bundle.upsellProduct}"/>

<c:url value="${upsellProduct.productPurchaseOption.url}" var="upsellProductUrl"/>

<section id="get-discount-bundle-section-${index}" class="get-discount get-discount-bundle page-section">
    <div class="get-discount-wr">
        <div class="get-discount-inner-wr">
            <div class="discount-description">
                <div class="discount-item-description">
                    <div class="discount-item-image">
                        <span class="item-image-wrapper">
                            <product:productPrimaryReferenceImage product="${sourceProduct}" format="thumbnail"/>
                        </span>
                        <a href="${upsellProductUrl}">
                            <product:productPrimaryReferenceImage product="${upsellProduct}" format="thumbnail"/>
                        </a>
                    </div>
                    <h4 class="discount-title">
                        <spring:theme code="product.product.details.bundle.title" arguments="${bundle.discountValue}"
                                      text="Buy Both and Save"/>
                    </h4>
                    <p class="discount-item-common">
                        <b><spring:theme code="product.product.details.bundle.thisItem"/>&nbsp;</b>${sourceProduct.name}
                    </p>
                    <p class="discount-item-additional">
                        <a href="${upsellProductUrl}">${upsellProduct.name}</a>&nbsp;<i>(${upsellProduct.productPurchaseOption.purchaseOptionName}&nbsp;${upsellProduct.price.formattedValue})</i>
                    </p>
                </div>
                <p class="discount-item-disclaimer">
                    <spring:theme code="product.product.details.bundle.discountDisclaimer"/>
                </p>
            </div>
            <div class="discount-price-info">
                <div class="discount-item-price">
                    <p class="orign-price">
                        <spring:theme code="product.product.details.bundle.originalPrice"/>
                        <span>${bundle.originalPriceFormatted}</span>
                    </p>
                    <p class="discount-price">
                        <spring:theme code="product.product.details.bundle.purchasedTogeather"/>
                        <span>${bundle.purchasedTogetherPriceFormatted}</span>
                    </p>
                    <c:if test="${bundle.discountedValueFormatted ne null}">
                        <p class="saved-price">
                            <spring:theme code="product.product.details.bundle.yousave"/>&nbsp;${bundle.discountedValueFormatted}
                        </p>
                    </c:if>
                    <c:if test="${not empty sourceProduct.price.optionalTaxShortMessage}">
                        <p class="price-disclaimer">
                                ${sourceProduct.price.optionalTaxShortMessage}
                            <a role="menuitem" tabindex="0" data-placement="bottom-right top-right" aria-label="information icon"
                               data-container-class="wiley-tax-popover"
                               data-content='${fn:escapeXml(sourceProduct.price.optionalTaxTooltip)}' class="icon-info black"></a>
                        </p>
                    </c:if>
                    <product:addtolegacycartforbundle bundle="${bundle}"/>
                </div>
            </div>
        </div>
    </div>
</section>