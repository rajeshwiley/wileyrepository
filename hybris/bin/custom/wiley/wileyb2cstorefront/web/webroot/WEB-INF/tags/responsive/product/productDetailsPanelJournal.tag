<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="isVolumeExist" value="${not empty product.volumeAndIssues}"/>
<c:set var="isIssnExist" value="${not empty product.printIssn}"/>
<c:set var="isOnlineIssnExist" value="${not empty product.onlineIssn}"/>
<c:set var="isFactorExist" value="${not empty product.impactFactor}"/>
<c:set var="isFactorFlagExist" value="${product.showImpactFactor}"/>
<c:set var="isFactorFlagEmpty" value="${empty product.showImpactFactor}"/>

<c:if test="${isVolumeExist}">
    <span>${product.volumeAndIssues}</span>
</c:if>
<c:if test="${isIssnExist}">
    <span><spring:theme code="product.product.details.printIssn"/>: ${product.printIssn}</span>
</c:if>
<c:if test="${isOnlineIssnExist}">
    <span><spring:theme code="product.product.details.onlineIssn"/>: ${product.onlineIssn}</span>
</c:if>
<c:if test="${isFactorFlagExist or isFactorFlagEmpty}">
<c:if test="${isFactorExist}">
    <span><spring:theme code="product.product.details.factor"/>: ${product.impactFactor}</span>
</c:if>
</c:if>