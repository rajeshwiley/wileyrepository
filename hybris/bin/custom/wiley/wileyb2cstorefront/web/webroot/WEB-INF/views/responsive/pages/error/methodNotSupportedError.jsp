<%@ page trimDirectiveWhitespaces="true" isErrorPage="true" %>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta name="viewport" content="width=device-width">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="format-detection" content="telephone=no">
    <title>Wiley</title>
    <link rel="stylesheet" type="text/css" media="all" href="${commonResourcePath}/css/style.css">
    <style>
        @media (min-width: 641px) {
            body, html {
                min-width: 0;
            }
        }
    </style>
</head>
<body>
<div class="wiley-server-error-page">
    <header class="header-light">
        <div class="container-fluid">
            <div class="container">
                <a href="${homePageUrl}" title="Wiley" class="logo">
                    <img src="${commonResourcePath}/images/logo.png" alt="Wiley">
                </a>
            </div>
        </div>
    </header>

    <main class="server-error">
         <div class="container">
             <h1>Client error (<b>405</b>)</h1>
             <p>
             	<span>Please try again later or contact <a href="mailto:info@wiley.com">info@wiley.com</a></span>
             	for further assistance.
             </p>
         </div>
    </main>

    <footer class="footer-light">
        <div class="container-fluid">
            <div class="container">
                <div class="footer-copyright-text">
                    <p>
                     <a href="${homePageUrl}/copyright">Copyright</a> &copy; 2000-${currentYear} by
                     <a href="${homePageUrl}">John Wiley & Sons, Inc.</a>, or related companies.
                     All rights reserved. Review our <a href="${homePageUrl}/privacy">privacy policy</a>.
                    </p>
                </div>
                <a href="${homePageUrl}" title="Wiley" class="logo">
                    <img src="${commonResourcePath}/images/logo.png" alt="Wiley">
                </a>
            </div>
        </div>
    </footer>
</div>
</body>
</html>