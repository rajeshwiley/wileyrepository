<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:url value="${component.policyLink.url}" var="policyLinkUrl" />
<div class="cookie-consent-component-container">
	<span
		class="cookie-consent-component-config"
		data-message="${component.message}"
		data-dismiss="${component.dismissButtonText}"
		data-link="${component.policyLink.linkName}"
		data-href="${policyLinkUrl}"
		data-country-code="${currentCountry.isocode}">
	</span>
</div>