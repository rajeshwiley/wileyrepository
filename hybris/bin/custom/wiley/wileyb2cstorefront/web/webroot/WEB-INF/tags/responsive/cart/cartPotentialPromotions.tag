<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>


<c:if test="${not empty cartData.potentialOrderPromotions}">
	<ul class="potentialPromotions">
        <c:forEach items="${cartData.potentialOrderPromotions}" var="promotion">
			<li>
				${promotion.description}
			</li>
        </c:forEach>
	</ul>
</c:if>
