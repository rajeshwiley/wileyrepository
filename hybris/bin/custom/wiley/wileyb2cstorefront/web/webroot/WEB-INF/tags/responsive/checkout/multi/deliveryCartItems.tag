<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>
<%@ attribute name="showDeliveryAddress" required="true" type="java.lang.Boolean" %>
<%@ attribute name="showPotentialPromotions" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="wileycomProduct" tagdir="/WEB-INF/tags/addons/wileycomstorefrontcommons/responsive/product"%>

<c:set var="hasShippedItems" value="${cartData.deliveryItemsQuantity > 0}" />
<c:set var="deliveryAddress" value="${cartData.deliveryAddress}"/>

<c:if test="${not hasShippedItems}">
	<spring:theme code="checkout.pickup.no.delivery.required"/>
</c:if>

<div class="order-brief-top">
	<c:if test="${hasShippedItems}">
		<c:choose>
			<c:when test="${showDeliveryAddress and not empty deliveryAddress}">
				<div class="order-brief-top-item">
					<div class="row">
						<div class="col-xs-12 col-sm-6 order-brief-top-title">
							<spring:theme code="checkout.pickup.items.to.be.shipped" text="Ship To:"/>
						</div>
						<div class="col-xs-12 col-sm-6">
							<p>
								<c:if test="${not empty deliveryAddress.title}">
									${fn:escapeXml(deliveryAddress.title)}&nbsp;
								</c:if>
								${fn:escapeXml(deliveryAddress.firstName)}&nbsp;${fn:escapeXml(deliveryAddress.lastName)}
							</p>
							<p>
								<c:if test="${ not empty deliveryAddress.line1 }">
									${fn:escapeXml(deliveryAddress.line1)},&nbsp;
								</c:if>
								<c:if test="${ not empty deliveryAddress.line2 }">
									${fn:escapeXml(deliveryAddress.line2)},&nbsp;
								</c:if>
								<c:if test="${not empty deliveryAddress.town }">
									${fn:escapeXml(deliveryAddress.town)},&nbsp;
								</c:if>
								<c:if test="${ not empty deliveryAddress.region.name }">
									${fn:escapeXml(deliveryAddress.region.name)}
								</c:if>
							</p>
							<p>
								<c:if test="${ not empty deliveryAddress.postalCode }">
									${fn:escapeXml(deliveryAddress.postalCode)},&nbsp;
								</c:if>
								<c:if test="${ not empty deliveryAddress.country.name }">
									${fn:escapeXml(deliveryAddress.country.name)}
								</c:if>
							</p>
						</div>
					</div>
				</div>
				<c:if test="${not empty cartData.deliveryMode}">
					<div class="order-brief-top-item">
						<div class="row">
							<div class="col-xs-12 col-sm-6 order-brief-top-title"><spring:theme code="checkout.multi.order.summary.delivery.method"/></div>
							<div class="col-xs-12 col-sm-6">${cartData.deliveryMode.name}</div>
						</div>
					</div>
				</c:if>
			</c:when>
			<c:otherwise>
				<div class="alternatetitle"><spring:theme code="checkout.pickup.items.to.be.delivered" /></div>
			</c:otherwise>
		</c:choose>
	</c:if>
</div>

<div class="product-list">
	<c:forEach items="${cartData.entries}" var="entry">
		<c:if test="${entry.deliveryPointOfService == null}">
			<c:url value="${entry.product.url}" var="productUrl"/>
			<div class="product-list-item">
	          <div class="row product-list-info">
	            <div class="col-xs-9 col-sm-9">
	              <div class="product-list-image"><a href="${productUrl}"><product:productPrimaryImage product="${entry.product}" format="thumbnail"/></a></div>
	              <div class="product-list-title"><a href="${productUrl}">${entry.product.name}</a></div>
	              <div class="product-list-description">
	                <div class="product-list-qty"><spring:theme code="basket.page.qty"/>&nbsp;${entry.quantity}</div>
					  <wileycomProduct:CartItemInventoryStatusBlock entry="${entry}" showReleaseDate="true"/>
	              </div>
	            </div>
	            <div class="col-xs-3 col-sm-3">
	              <div class="product-item-price"><format:price priceData="${entry.basePrice}" displayFreeForZero="true"/></div>
	            </div>
	          </div>
	        </div>
        </c:if>
	</c:forEach>
</div>


