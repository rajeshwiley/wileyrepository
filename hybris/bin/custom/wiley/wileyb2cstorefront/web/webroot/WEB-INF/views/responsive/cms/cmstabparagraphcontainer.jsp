<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="row">
	<div class="col-xs-12">
		<div class="common-tabs pdp-section">
			<ul class="nav nav-tabs" id="pdpTabsCollapsed">
				<c:forEach items="${tabs}" var="para" varStatus="loop">
			      <c:set var="paraUid" value="${para.uid}" />
				  <li class="${loop.index == 0 ? 'active' : ''}">
					<a href="#${paraUid}" data-toggle="tab"> ${para.title} </a>
				  </li>
				</c:forEach>
			</ul>
			<div class="tab-content">
				<c:forEach items="${tabs}" var="para" varStatus="loop">
				  <c:set var="paraUid" value="${para.uid}" />
				  <div id="${paraUid}" class="tab-pane fade ${loop.index == 0 ? 'in active' : ''}">
					${para.content}
				  </div>
				</c:forEach>
			</div>
		</div>
	</div>
</div>
