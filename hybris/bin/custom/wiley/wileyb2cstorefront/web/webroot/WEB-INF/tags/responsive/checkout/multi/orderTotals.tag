<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>
<%@ attribute name="showTax" required="false" type="java.lang.Boolean" %>
<%@ attribute name="showTaxEstimate" required="false" type="java.lang.Boolean" %>
<%@ attribute name="subtotalsCssClasses" required="false" type="java.lang.String" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<%@ taglib prefix="multi-checkout-common"
	tagdir="/WEB-INF/tags/addons/wileycomstorefrontcommons/responsive/checkout/multi"%>

<div class="order-total-list">
	<div class="order-total-title"><spring:theme code="order.order.totals"/></div>
	<div class="order-total-item">
		<div class="row">
			<div class="col-xs-6 col-sm-6"><spring:theme code="basket.page.totals.subtotal"/></div>
			<div class="col-xs-6 col-sm-6 text-right price">
				<ycommerce:testId code="Order_Totals_Subtotal">
					<format:price priceData="${cartData.subTotalWithoutDiscount}"/>
				</ycommerce:testId>
			</div>
		</div>
	</div>
	<c:if test="${not empty cartData.appliedVouchers[0]}">
		<div class="order-total-item">
			<div class="row">
				<div class="col-xs-4 col-sm-4"><spring:theme code="text.voucher.discountCode" text="Discount Code"/>:</div>
				<div class="col-xs-8 col-sm-8 text-right price">${cartData.appliedVouchers[0]}</div>
			</div>
		</div>
	</c:if>
	<c:if test="${cartData.totalDiscounts.value > 0}">
		<div class="order-total-item">
			<div class="row">
				<div class="col-xs-6 col-sm-6"><spring:theme code="basket.page.totals.discount"/></div>
				<div class="col-xs-6 col-sm-6 text-right price">
					<ycommerce:testId code="Order_Totals_Subtotal">
						-<format:price priceData="${cartData.totalDiscounts}"/>
					</ycommerce:testId>
				</div>
			</div>
		</div>
	</c:if>
	<c:if test="${showTax && cartData.taxCalculated}">
		<div class="order-total-item">
			<div class="row">
				<div class="col-xs-6 col-sm-6">${sessionCountry.taxName}:</div>
				<div class="col-xs-6 col-sm-6 text-right price">
					<format:price priceData="${cartData.totalTax}"/>
				</div>
			</div>
		</div>
	</c:if>
	<multi-checkout-common:orderTotalsShippingItem abstractOrderData="${cartData}"/>
</div>
<div class="order-total-info order-total-title">
	<div class="row">
		<div class="col-xs-6 col-sm-6"><spring:theme code="basket.page.totals.total"/></div>
		<div id="totalPriceValue" class="col-xs-6 col-sm-6 text-right">
			<ycommerce:testId code="cart_totalPrice_label">
				<c:choose>
					<c:when test="${showTax}">
						<format:price priceData="${cartData.totalPriceWithTax}"/>
					</c:when>
					<c:otherwise>
						<format:price priceData="${cartData.totalPrice}"/>
					</c:otherwise>
				</c:choose>
			</ycommerce:testId>
		</div>
	</div>
</div>
