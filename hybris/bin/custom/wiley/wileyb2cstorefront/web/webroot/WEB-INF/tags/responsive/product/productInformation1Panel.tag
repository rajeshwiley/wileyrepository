<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>

<c:if test="${not empty product.description}">
    <section id="description-section" class="page-section product-long-description">
        <h2 class="section-title">
            <spring:theme code="product.product.details.description"/>
        </h2>
        <div class="description">
                ${product.description}
        </div>
    </section>
</c:if>
<product:productCompanionSitesPanel product="${product}"/>
<product:productVideo video="${product.brightcoveVideo}"/>
<product:productSetsPanel product="${product}"/>
<c:forEach items="${product.wileyBundles}" var="bundle" varStatus="loop">
    <product:wileyBundle bundle="${bundle}" index="${loop.index}"/>
</c:forEach>
