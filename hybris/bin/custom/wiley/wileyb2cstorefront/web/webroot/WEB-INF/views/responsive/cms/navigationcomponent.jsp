<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>

<c:set value="${component.styleClass}" var="navigationClass" />

<c:if test="${component.visible}">

    <div class="${navigationClass}">
        <a href="javascript:void(0);"> <c:if test="${not empty component.navigationNode.title }">
                <c:out value="${component.navigationNode.title}" />
            </c:if>
        </a>
        <div class="dropdown-menu arrow my-account-container">
        <button class="close" tabindex="-1">&nbsp;</button>
        <div class="dropdown-items-wrapper" tabindex="0">
            <nav:menuDropdownItems navNodes="${component.navigationNode.children}"/>

            <div class="buttons">
                <a href="<c:url value="/logout"/>" class="button"><spring:theme code="header.link.logout" /></a>
            </div>
            </div>
        </div>
    </div>
</c:if>