<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="user" tagdir="/WEB-INF/tags/responsive/user" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>


<sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
	<div class="site-form central-form">
		<form:form method="post" commandName="guestForm">

				<div class="form-cell heading">
					<h1><spring:theme code="checkout.login.register.heading"/></h1>
				</div>

				<div class="form-cell">
					<div class="form-field">
						<div class="form-label required">
							<formElement:formInputBox idKey="email" labelKey="guest.email" inputCSS="form-field"
									path="email" mandatory="true"
									placeholder="checkout.login.register.placeholder.emailAddress"/>
						</div>
					</div>
				</div>

				<div class="form-cell">
					<button type="submit" class="button button-main large create-an-account-btn"
							data-target="${createAccountDataTarget}">
						<spring:theme code="checkout.login.register.buttonLabel.createAnAccount"/>
					</button>
				</div>

				<c:if test="${empty suppressGuestCheckout or suppressGuestCheckout eq false}">
					<div class="form-cell">
						<button type="submit" class="button button-outlined large checkout-as-guest-btn"
								data-target="${guestCheckoutDataTarget}">
							<spring:theme code="checkout.login.register.buttonLabel.checkoutAsGuest"/>
						</button>
					</div>
				</c:if>

		</form:form>
	</div>
</sec:authorize>