<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>

<div class="product-list-wrapper">
<c:set var="currentSearchUrl" value="${searchUrl}${currentSearchQuery}" />
<div class="search-results-top-bar">
   <nav:sortoptions searchPageData="${searchPageData}"/>
   <nav:pagination top="false" msgKey="search.page"
      	        showCurrentPageInfo="true"
      	        hideRefineButton="true"
      	        supportShowPaged="${isShowPageAllowed}"
      	        supportShowAll="${isShowAllAllowed}"
      	        searchPageData="${searchPageData}"
      	        searchUrl="${currentSearchUrl}"
      	        numberPagesShown="${numberPagesShown}" />
</div>
   <div class="products-list">
      <c:forEach items="${searchPageData.results}" var="product">
         <product:productListerItem product="${product}"/>
      </c:forEach>
   </div>
   <nav:pagination top="false" msgKey="search.page"
     	        showCurrentPageInfo="true"
     	        hideRefineButton="true"
     	        supportShowPaged="${isShowPageAllowed}"
     	        supportShowAll="${isShowAllAllowed}"
     	        searchPageData="${searchPageData}"
     	        searchUrl="${currentSearchUrl}"
     	        numberPagesShown="${numberPagesShown}" />
   <div class="addToCartTitle" style="display:none">
      <div class="add-to-cart-header">
         <div class="headline">
            <span class="headline-text">
               <spring:theme code="basket.added.to.basket"/>
            </span>
         </div>
      </div>
   </div>
   <product:addToCartPopupTemplate/>
</div>