<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:set var="isExistISBN" value="${not empty product.isbn13}"/>
<c:set var="isPublicationDateNotEmpty" value="${not empty product.publicationDate}"/>
<c:set var="isImprintsNotEmpty" value="${not empty product.imprints}"/>
<c:set var="isExistPages" value="${not empty product.pagesNumber}"/>

<c:if test="${isExistISBN}">
	<span><spring:theme code="product.product.details.isbn"/>: ${product.isbn13}</span>
</c:if>
<c:if test="${isPublicationDateNotEmpty}">
	<span>
		<fmt:formatDate value="${product.publicationDate}" pattern="MMMM yyyy"/>
	</span>
</c:if>
<c:if test="${isImprintsNotEmpty}">
	<span>
			${product.imprints}
	</span>
</c:if>
<c:if test="${isExistPages}">
	<span>${product.pagesNumber}&nbsp;<spring:theme code="product.product.details.pages"/></span>
</c:if>