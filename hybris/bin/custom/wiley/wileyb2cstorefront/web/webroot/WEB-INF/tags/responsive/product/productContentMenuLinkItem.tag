<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@attribute name="title" required="true" %>
<%@attribute name="link" required="true" %>

<c:set var="titleText"><spring:theme code='${title}'/></c:set>
<spring:theme code="product.product.details.link.template"
              arguments="${link};${titleText}"
              argumentSeparator=";"/>