<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="cartData" required="true"
	type="de.hybris.platform.commercefacades.order.data.CartData"%>
<%@ attribute name="showDeliveryAddress" required="true"
	type="java.lang.Boolean"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>

<c:set var="hasShippedItems"
	value="${cartData.deliveryItemsQuantity > 0}" />
<c:set var="deliveryAddress" value="${cartData.deliveryAddress}" />
<c:set var="firstShippedItem" value="true"></c:set>

<div id="checkoutShipmentList" class="checkout-shipment-list">
  <div class="checkout-content-subtitle">
    <spring:theme code="checkout.multi.shipment.items" arguments="${cartData.physicalItemsQuantity}"
        text="Shipment - ${cartData.physicalItemsQuantity} Item(s)" />
  </div>
  <div class="checkout-content-address">
    <c:if test="${showDeliveryAddress and not empty deliveryAddress}">
    	<p>
    		<c:if test="${not empty deliveryAddress.title}">
    			${fn:escapeXml(deliveryAddress.title)}&nbsp;
    		</c:if>
    		${fn:escapeXml(deliveryAddress.firstName)}&nbsp;${fn:escapeXml(deliveryAddress.lastName)}
    		<br>
    		<c:if test="${ not empty deliveryAddress.line1 }">
    			${fn:escapeXml(deliveryAddress.line1)},&nbsp;
    		</c:if>
    		<c:if test="${ not empty deliveryAddress.line2 }">
    			${fn:escapeXml(deliveryAddress.line2)},&nbsp;
    		</c:if>
    		<br>
    		<c:if test="${not empty deliveryAddress.town }">
    			${fn:escapeXml(deliveryAddress.town)},&nbsp;
    		</c:if>
    		<c:if test="${ not empty deliveryAddress.region.name }">
    			${fn:escapeXml(deliveryAddress.region.name)},&nbsp;
    		</c:if>
    		<c:if test="${ not empty deliveryAddress.postalCode }">
    			${fn:escapeXml(deliveryAddress.postalCode)},&nbsp;
    		</c:if>
    		<c:if test="${ not empty deliveryAddress.country.name }">
    			${fn:escapeXml(deliveryAddress.country.name)}
    		</c:if>
    		<c:if test="${ not empty deliveryAddress.phone }">
    			,&nbsp;${fn:escapeXml(deliveryAddress.phone)}
    		</c:if>
    	</p>
    </c:if>
  </div>
  <c:forEach items="${cartData.entries}" var="entry">
  	<c:if test="${not entry.product.isDigital}">
  		<c:url value="${entry.product.url}" var="productUrl" />
  		<div class="checkout-shipment-item">
  			<div class="checkout-shipment-title">${entry.product.name}</div>
  			<div class="checkout-shipment-qty">
  				<spring:theme code="checkout.multi.shipment.items.qty" />
  				&nbsp;${entry.quantity}
  			</div>
  			<c:if test="${not empty entry.estimatedDeliveryDays}">
  			    <div class="checkout-shipment-est-del">
  			        <spring:theme code="product.checkout.shipment.estimated.delivery" arguments="${entry.estimatedDeliveryDays}"/>
  			    </div>
  			</c:if>
  		</div>
  	</c:if>
  </c:forEach>
</div>