<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="purchaseOption" tagdir="/WEB-INF/tags/responsive/purchaseOption" %>

<c:url value="/ucart/wileyPlus/purchase/options" var="addToCartUrl"/>
<c:set var="courseBundles" value="${purchaseOptions.instantAccessPurchaseOption.courseBundles}"/>
<div class="purchase-accordion-item wrap-inner-bordered">
	<div class="purchase-accordion-heading">
		<div class="heading-body">
			<div class="heading-left" data-toggle="collapse" data-parent="#accordion" href="#collapse2"><span class="heading-arrow"></span></div>
			<div class="heading-center" data-toggle="collapse" data-parent="#accordion" href="#collapse2" tabindex="0">
				<h2><spring:message code="wileypluscourse.purchase.option.instant-access.accordion-title"/></h2>
			</div>
			<spring:message code="wileypluscourse.purchase.option.instant-access.accordion.popover" var="accordion_popover_message"/>
			<div class="heading-right"><span data-toggle="popover" data-content="${accordion_popover_message}" data-trigger="hover focus" class="popover-help" tabindex="0">&nbsp;</span></div>
		</div>
	</div>
	<div id="collapse2" class="panel-collapse collapse">
		<div class="purchase-accordion-body">
			<div class="instant-access">
				<div class="instant-access-item">
					<div class="row">
						<c:set var="purchaseType"><spring:message code="wileypluscourse.purchase.option.instant-access.item-description.line1"/></c:set>
						<div class="col-xs-12 col-sm-6">
							<div class="icon-tablet"></div>
							<div class="item-description">
								${purchaseType}
								<spring:message code="wileypluscourse.purchase.option.instant-access.item-description.popover" var="item_description_popover_message"/>
								<span data-toggle="popover"	data-content="${item_description_popover_message}" data-trigger="hover focus" class="popover-help" tabindex="0">&nbsp;</span><br>
								<spring:message code="wileypluscourse.purchase.option.instant-access.item-description.line2"/>
							</div>
						</div>
						<div class="col-xs-12 col-sm-3">
							<div class="item-price">
								<div class="numeric-bold regular"><format:price priceData="${courseBundles.coursePrice}" displayFreeForZero="false"/></div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-3 text-center">
                            <purchaseOption:instantAccessAddToCartForm code="${courseBundles.courseCode}" title="${course.title}"
                            		authors="${course.authors}" imageUrl="${course.imageUrl}" purchaseType="${purchaseType}"/>
						</div>
					</div>
				</div>
				<div class="promo-msg hidden-xs">
					<div class="row">
						<div class="col-xs-12 col-sm-12">
							<spring:message code="wileypluscourse.purchase.option.instant-access.promo.message"/>
						</div>
					</div>
				</div>
				<div class="discounted-list">
					<c:set var="courseSets" value="${courseBundles.bundles}"/>
					<c:forEach var="courseSet" items="${courseSets}" varStatus="loop">
						<div class="instant-access-item gray-super-light">
							<c:if test="${courseSet.mostPopular}">
								<div class="most-popular" aria-label="<spring:message code='wileypluscourse.purchase.option.instant-access.most.popular'/>"></div>
							</c:if>
							<div class="promo-msg visible-xs">
								<div class="row">
									<div class="col-xs-12 col-sm-12">
										<spring:message code="wileypluscourse.purchase.option.instant-access.promo.message"/>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12 col-sm-3">
									<div class="icon-tablet"></div>
									<div class="item-description">
										<spring:message code="${courseSet.mainComponentName}"/>
										<div class="numeric line-through"><format:price priceData="${courseSet.mainComponentPrice}" displayFreeForZero="false"/></div>
									</div>
								</div>
								<div class="col-xs-12 col-sm-1">
									<div class="icon-plus" aria-label="<spring:message code='wileypluscourse.purchase.option.instant-access.icon.plus'/>"></div>
								</div>
								<div class="col-xs-12 col-sm-2">
									<div class="icon-${courseSet.extraComponentNameKey}"></div>
									<c:set var="extraComponentAltText"><spring:message code="${courseSet.extraComponentAlttextKey}"/></c:set>
									<div class="item-description">
										<spring:message code="wileypluscourse.purchase.option.instant-access.discounted-list.${courseSet.extraComponentNameKey}-item-description"/>
										<span data-toggle="popover" data-content="${extraComponentAltText}"
                                         data-trigger="hover focus" class="popover-help" tabindex="0">&nbsp;</span>
										<div class="numeric line-through"><format:price priceData="${courseSet.extraComponentPrice}" displayFreeForZero="false"/></div>
										<div class="numeric dark-teal">
											<format:price priceData="${courseSet.extraComponentSaving}" displayFreeForZero="false"/>
										</div>
									</div>
								</div>
								<div class="col-xs-12 col-sm-3">
									<div class="item-price dark-teal">
										<div class="numeric-bold">
											<spring:message code="wileypluscourse.purchase.option.instant-access.discounted-list.discounted-price.message"/>
										</div>
										<div class="numeric-bold"><format:price priceData="${courseSet.discountedPriceValue}" displayFreeForZero="true"/></div>
										<div class="numeric">
											<spring:message code="wileypluscourse.purchase.option.instant-access.discounted-list.discounted-price" arguments="${courseSet.discountedPriceSaving.formattedValue}"/>
										</div>
									</div>
								</div>
								<c:set var="purchaseTypeForSet"><spring:message code="${courseSet.mainComponentName}"/> + <spring:message code="wileypluscourse.purchase.option.instant-access.discounted-list.${courseSet.extraComponentNameKey}-item-description"/></c:set>
								<div class="col-xs-12 col-sm-3 text-center">
									<purchaseOption:instantAccessAddToCartForm code="${courseSet.bundleCode}" purchaseType="${purchaseTypeForSet}"
											authors="${course.authors}" imageUrl="${course.imageUrl}" title="${course.title}" formIndex="${loop.index + 1}"/>
								</div>
							</div>
						</div>
					</c:forEach>
				</div>
			</div>
		</div>
	</div>
</div>
