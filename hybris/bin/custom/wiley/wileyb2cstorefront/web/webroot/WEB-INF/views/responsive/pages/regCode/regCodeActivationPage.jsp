<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>
<%@ taglib prefix="storepickup" tagdir="/WEB-INF/tags/responsive/storepickup" %>


<template:page pageTitle="${pageTitle}" containerCSSClass="container activate-registration-code wrap authorization-support-message-wr">

	<cms:pageSlot position="RegCode" var="feature">
		<cms:component component="${feature}" />
	</cms:pageSlot>
</template:page>


