<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>

<div id="forgotten-modal-body" class="modal-body">
			
	<common:globalMessages/>
			

    <div id="forgotPasswordMsg">
	   <spring:theme code="forgottenPwd.description"/>
	</div>
			
	<div class="site-form form-forgot-password">
		<c:url var="resetPasswordUrl" value="/login/pw/request"/>

		<form:form method="post" action="${resetPasswordUrl}" commandName="forgottenPwdForm">
		  <div class="row">
			<div class="col-xs-12 col-sm-9">
				<ycommerce:testId code="login_forgotPasswordEmail_input">
				   <formElement:formInputBox idKey="forgottenPwd.email" placeholder="login.emailPlaceholder" labelKey="forgottenPwd.email" path="email" mandatory="true"/>
				</ycommerce:testId>
			 </div>
			 <div class="col-xs-12 col-sm-3 text-right">
				<ycommerce:testId code="login_forgotPasswordSubmit_button">
				   <button class="button button-main large js-reset-password-submit" type="submit">
					  <spring:theme code="forgottenPwd.btn.title"/>
				   </button>
				</ycommerce:testId>
			 </div>
		  </div>
		</form:form>
	</div>

</div>