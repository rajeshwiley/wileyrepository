<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<template:page pageTitle="${pageTitle}" containerCSSClass="container-fluid" clearContainerClass="true">
    <div class="row">
        <div class="search-result-page">
            <nav:searchResultHeader searchPageData="${searchPageData}"
                                    totalNumberOfProductResults="${totalNumberOfProductResults}"
                                    totalNumberOfContentResults="${totalNumberOfContentResults}"/>
            <cms:pageSlot position="TopMarketingInformation" var="feature">
                <cms:component component="${feature}"/>
            </cms:pageSlot>
            <div class="search-result-content">
                <div class="container product-list-container">
                    <c:if test="${activeTab == 'PRODUCTS'}">
                        <cms:pageSlot position="ProductLeftRefinements" var="feature">
                            <cms:component component="${feature}"/>
                        </cms:pageSlot>
                    </c:if>
                    <div class="search-result-tabs-wrapper">
                        <div class="tab-content">
                            <div class="tab-pane fade active in">
                                <cms:pageSlot position="SearchResultsListSlot" var="feature">
                                    <cms:component component="${feature}"/>
                                </cms:pageSlot>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</template:page>
