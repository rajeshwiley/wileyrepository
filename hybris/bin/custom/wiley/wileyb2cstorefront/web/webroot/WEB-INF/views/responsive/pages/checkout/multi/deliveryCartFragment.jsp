<%@ page trimDirectiveWhitespaces="true"%>

<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi" %>

<multi-checkout:deliveryCart cartData="${cartData}" showDeliveryAddress="true"/>