<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="pageData" required="true" type="de.hybris.platform.commerceservices.search.facetdata.ProductSearchPageData" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>


<c:if test="${not empty pageData.breadcrumbs}">
		<div class="applied-facets">
			<c:forEach items="${pageData.breadcrumbs}" var="breadcrumb">
    			<c:url value="${breadcrumb.removeQuery.url}" var="removeQueryUrl"/>
	    		<div class="facets-panel-item">
    				<span><spring:theme code="${breadcrumb.facetValueName}" /><i>&nbsp;<a class="delete" href="${removeQueryUrl}" >Remove applied facet ${breadcrumb.facetValueName}</a></i></span>
    			</div>
    		</c:forEach>
		</div>

</c:if>
