<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>


<footer class="main-footer-container">
    <span tabindex="0" title="Back to top" class="back-to-top"></span>
    <div class="container">
        <cms:pageSlot position="Footer" var="feature">
            <cms:component component="${feature}"/>
        </cms:pageSlot>

        <div class="footer-social-links">
            <cms:pageSlot position="FooterSocialMediaSlot" var="feature">
                <cms:component component="${feature}"/>
            </cms:pageSlot>
        </div>
        <div class="main-footer-copyright">
            <div class="footer-copyright-text">
                <cms:pageSlot position="FooterCopyrightSlot" var="feature">
                    <cms:component component="${feature}"/>
                </cms:pageSlot>
            </div>
            <div class="footer-logo">
                <cms:pageSlot position="CompanyLogo" var="feature">
                    <cms:component component="${feature}"/>
                </cms:pageSlot>
            </div>
        </div>
    </div>
</footer>