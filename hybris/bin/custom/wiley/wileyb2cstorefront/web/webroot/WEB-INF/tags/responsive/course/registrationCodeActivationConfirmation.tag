<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:url value="${continueUrl}" var="getStartedUrl" />
<div class="container reg-code-confirmation wrap">
	<div class="row">
		<div class="col-xs-12 col-sm-6">
			<h1 class="full-width-gray"><spring:theme code="text.regCode.confirmation.activated" /></h1>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-6 left-block">
		<div class="product-list wrap">
				<c:forEach items="${allItems}" var="orderEntry">
					<div class="product-list-item">
						<div class="product-list-image">
						<img src="${orderEntry.product.purchaseOptionImageUrl}"/>
						</div>
						<div class="row product-list-info">
							<div class="col-xs-12 col-sm-9">
								<div class="product-list-title">${orderEntry.product.name}</div>
								<div class="product-list-description">
									<div class="product-list-author">${orderEntry.product.authors}</div>
								</div>
							</div>
						</div>
					</div>
				</c:forEach>
			</div>
			<div class="success-message icon-check wrap">
				<p><spring:theme code="text.regCode.confirmation.accepted" /></p>
			</div>
			<div class="buttons-wrap">
				<button onclick="location.href='${getStartedUrl}';" class="button button-main large">
				<spring:theme code="text.regCode.confirmation.continue" /></button>
			</div>
		</div>
		<div class="col-xs-12 col-sm-6"></div>
	</div>
</div>