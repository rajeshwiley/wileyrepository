<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/addons/wileycomstorefrontcommons/responsive/address"%>

<h1 class="full-width-gray">
    <spring:theme code="text.account.addressBook" text="Address Book" />
</h1>

<div class="simple-list my-account-wrap address-book">
    <ycommerce:testId code="addressBook_addNewAddress_button">
        <div class="wrap-inner-bordered">
            <div class="row">
                <div class="col-xs-12 col-sm-12 accountAddAddress">
                    <a href="add-address" class="button button-main large"><spring:theme
                            code="text.account.addressBook.addAddress" text="Add Address" /></a>
                </div>
            </div>
        </div>
    </ycommerce:testId>
    <c:choose>
        <c:when test="${empty addressData}">
            <h3>
                <spring:theme code="text.account.addressBook.noSavedAddresses" text="No Saved Addresses Found" />
            </h3>
        </c:when>
        <c:otherwise>
            <c:forEach items="${addressData}" var="address">
                <div class="wrap-inner-bordered">
                    <div class="row">
                        <div class="col-xs-12 col-sm-8">
                            <div class="simple-list-text">
                            	<address:viewAddressItem address="${address}" />
                            </div>
                        </div>
                        <div class="actions col-xs-12 col-sm-4 text-right">
                            <div class="row">
                                <div class="col-xs-8 col-sm-8">
                                    <div class="my-account-button-small">
                                        <c:if test="${not address.defaultAddress}">
                                            <ycommerce:testId code="addressBook_isDefault_button">
                                                <spring:url value="/my-account/set-default-address/${address.id}"
                                                    var="setDefaultAddressUrl" />
                                                <form action="${setDefaultAddressUrl}">
                                                    <button class="button button-outlined large" type="submit">
                                                        <spring:theme code="text.setDefault" text="Set as Default" />
                                                    </button>
                                                </form>
                                            </ycommerce:testId>
                                        </c:if>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-4">
                                    <ycommerce:testId code="addressBook_editAddress_button">
                                        <a class="button button-edit" aria-label="<spring:theme code='text.account.addressBook.edit.billing.address'/>" href="edit-address/${address.id}"></a>
                                    </ycommerce:testId>
                                    <ycommerce:testId code="addressBook_removeAddress_button">
                                        <a class="button button-remove"
                                            aria-label="<spring:theme code='text.account.addressBook.delete.payment'/>"
                                        	data-toggle="modal" data-target="#deleteAddressPopup-${address.id}"
                                            text="Delete Address"/>
                                        </a>
                                    </ycommerce:testId>
                                </div>
                            </div>
                        </div>
                        <spring:theme code="text.address.delete.popup.title" text="Delete Address" var="deletePopupTitle"/>
                		<address:deletePopup address="${address}" title="${deletePopupTitle}" />
                    </div>
                </div>
            </c:forEach>
        </c:otherwise>
    </c:choose>
</div>
