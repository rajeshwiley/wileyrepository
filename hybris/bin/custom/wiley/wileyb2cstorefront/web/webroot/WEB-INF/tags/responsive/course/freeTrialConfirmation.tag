<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="multi"
	tagdir="/WEB-INF/tags/responsive/checkout/multi"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>


<c:url value="${continueUrl}" var="getStartedUrl" />
<div class="container free-trial confirmation wrap">
	<div class="row">
		<div class="col-xs-12 col-sm-6">
			<h1 class="full-width-gray">
				<spring:theme code="text.freeTrial.confirmation.title" />
			</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-6 left-block">
			<div class="product-list wrap">
				<c:forEach items="${allItems}" var="orderEntry">
					<div class="product-list-item">
						<div class="product-list-image">
						<img src="${orderEntry.product.purchaseOptionImageUrl}"/>
						</div>
						<div class="row product-list-info">
							<div class="col-xs-12 col-sm-9">
								<div class="product-list-title">${orderEntry.product.name}</div>
								<div class="product-list-description">
									<div class="product-list-author">${orderEntry.product.authors}</div>
								</div>
							</div>
						</div>
					</div>
				</c:forEach>
			</div>
			<div class="success-message icon-check wrap">
				<p>
					<spring:theme
						code="text.freeTrial.confirmation.thankYouMessage" />
				</p>
				<p>
					<spring:theme code="text.freeTrial.confirmation.daysMessage"
						arguments="${gracePeriod}" />&nbsp;<a 
							href="https://hub.wiley.com/docs/support/wileyplus-grace-period-DOC-8102?referrer=wileyplus"><spring:theme
							code="text.freeTrial.confirmation.showMeHow" /></a>
				</p>
			</div>
			<div class="button-wrap">
				<button onclick="location.href='${getStartedUrl}';"
					class="button button-main large">
					<spring:theme code="text.freeTrial.confirmation.getStarted" />
				</button>
			</div>
		</div>
		<div class="col-xs-12 col-sm-6"></div>
	</div>
</div>