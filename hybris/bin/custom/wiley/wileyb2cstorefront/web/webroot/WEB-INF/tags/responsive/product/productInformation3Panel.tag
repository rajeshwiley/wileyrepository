<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>

<c:if test="${not empty product.aboutAuthors or not empty product.requestPermissionsLink}">
    <section id="author-section" class="page-section section-author-wr">
        <c:if test="${not empty product.aboutAuthors}">
            <div class="page-section-content section-author">
                <h2 data-toggle="collapse" data-target="#author-section-item" class="section-title collapsed">
                    <spring:theme code="product.product.details.aboutAuthors"/>
                </h2>
                <div id="author-section-item" class="collapse">
                        ${product.aboutAuthors}
                </div>
            </div>
        </c:if>
        <c:if test="${not empty product.requestPermissionsLink}">
            <div id="permission-section" class="section-permissions">
                <h2 data-toggle="collapse" data-target="#permission-section-item" class="section-title collapsed">
                    <spring:theme code="product.product.details.permissions.title"/>
                </h2>
                <div id="permission-section-item" class="collapse">
                    <p>
                        <spring:theme code="product.product.details.permissions.text"
                                      arguments="${product.requestPermissionsLink[0].url};target=\"_blank\""
                                      argumentSeparator=";"/>
                    </p>
                </div>
            </div>
        </c:if>
    </section>
</c:if>

<c:choose>
    <c:when test="${not empty product.tableOfContents}">
        <c:set var="activeTab" value="tableOfContents"/>
    </c:when>
    <c:when test="${not empty product.newToEdition}">
        <c:set var="activeTab" value="newToEdition"/>
    </c:when>
    <c:when test="${not empty product.wileyReviews}">
        <c:set var="activeTab" value="wileyReviews"/>
    </c:when>
    <c:when test="${not empty product.relatedWebsites}">
        <c:set var="activeTab" value="relatedWebsites"/>
    </c:when>
    <c:when test="${not empty product.downloadsTab}">
        <c:set var="activeTab" value="downloadsTab"/>
    </c:when>
    <c:when test="${not empty product.errata}">
        <c:set var="activeTab" value="errata"/>
    </c:when>
    <c:when test="${not empty product.notes}">
        <c:set var="activeTab" value="notes"/>
    </c:when>
    <c:when test="${not empty product.whatsNewToDisplay}">
        <c:set var="activeTab" value="whatsNewToDisplay"/>
    </c:when>
    <c:when test="${not empty product.pressRelease}">
        <c:set var="activeTab" value="pressRelease"/>
    </c:when>
    <c:otherwise>
        <c:set var="activeTab"/>
    </c:otherwise>
</c:choose>

<product:productTabs product="${product}" activeTab="${activeTab}"/>

