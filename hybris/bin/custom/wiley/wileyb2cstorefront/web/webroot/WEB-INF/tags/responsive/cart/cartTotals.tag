<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>
<%@ attribute name="showTax" required="false" type="java.lang.Boolean" %>
<%@ attribute name="showDeliveryCost" required="false" type="java.lang.Boolean" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="voucher" tagdir="/WEB-INF/tags/responsive/voucher" %>

<spring:url value="/cart/redeemVoucher" var="redeemVoucherUrl" />
<spring:url value="/cart/releaseVoucher" var="releaseVoucherUrl" />

<div class="order-total-list">
	<div class="order-total-item">
		<div class="row">
			<div class="col-xs-8 col-sm-8">
				<spring:theme code="basket.page.totals.subtotal"/>
			</div>
			<div class="col-xs-4 col-sm-4 text-right price"><format:price priceData="${cartData.subTotalWithoutDiscount}"/></div>
		</div>
	</div>

	<c:if test="${showDeliveryCost}">
		<div class="order-total-item">
			<div class="row">
				<div class="col-xs-8 col-sm-8"><spring:theme code="text.account.order.shipping" text="Shipping:"/></div>
				<div class="col-xs-4 col-sm-4 text-right price"><format:price priceData="${cartData.deliveryCost}" /></div>
			</div>
		</div>
		<div class="order-total-item">
			<div class="row">
				<div class="col-xs-8 col-sm-8"><spring:theme code="basket.qualify.free.shipping" text="You qualify for FREE Shipping"/>:</div>
				<div class="col-xs-4 col-sm-4 text-right price">Free</div>
			</div>
		</div>
  	</c:if>

  	<c:if test="${showTax}">
	  	<div class="order-total-item">
			<div class="row">
				<div class="col-xs-8 col-sm-8"><spring:theme code="basket.sales.taxes" text="Sales Tax"/>:</div>
				<div class="col-xs-4 col-sm-4 text-right price"><format:price priceData="${cartData.totalTax}"/></div>
			</div>
	  		</div>
  	</c:if>

  	<c:if test="${not empty cartData.appliedVouchers[0]}">
	   	<div class="order-total-item">
		  	<div class="row">
				<div class="col-xs-4 col-sm-4"><spring:theme code="text.voucher.discountCode" text="Discount Code"/>:</div>
				<div class="col-xs-8 col-sm-8 text-right price">${cartData.appliedVouchers[0]}</div>
			</div>
		</div>
  	</c:if>

  	<c:if test="${cartData.totalDiscounts.value > 0}">
	  	<div class="order-total-item">
			<div class="row">
				<div class="col-xs-8 col-sm-8"><spring:theme code="basket.order.discount" text="Order Discount"/></div>
				<div class="col-xs-4 col-sm-4 text-right price">-<format:price priceData="${cartData.totalDiscounts}"/></div>
			</div>
	  	</div>
  	</c:if>
</div>

<voucher:voucherForm redeemVoucherUrl="${redeemVoucherUrl}" releaseVoucherUrl="${releaseVoucherUrl}" />

<cart:cartPotentialPromotions cartData="${cartData}" />
<cart:cartPromotions cartData="${cartData}" />

<div class="order-total-info order-total-title">
	<div class="row">
		<div class="col-xs-6 col-sm-6"><spring:theme code="text.account.order.orderTotals" /></div>
		<div class="col-xs-6 col-sm-6 text-right"><format:price priceData="${cartData.subTotal}"/></div>
	</div>
</div>

<c:if test="${showTax}">
	<div class="order-included-tax">
		<spring:theme code="basket.your.order.includes" text="Your order includes&nbsp;"/>
		<format:price priceData="${cartData.totalTax}"/>
		<spring:theme code="basket.your.order.includes.tax" text="&nbsp;tax"/>
	</div>
</c:if>

