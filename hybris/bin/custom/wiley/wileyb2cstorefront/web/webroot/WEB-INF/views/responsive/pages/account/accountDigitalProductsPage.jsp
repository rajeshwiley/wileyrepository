<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="wileycomNav" tagdir="/WEB-INF/tags/addons/wileycomstorefrontcommons/responsive/nav"%>



<c:if test="${empty orderEntries.results}">
    <div class="row">
        <div class="col-xs-12 col-sm-9">
                <h1 class="full-width-gray"><spring:theme code="text.account.myDigitalProducts"/></h1>
        </div>
        <div class="col-xs-12 col-sm-9">
            <spring:theme code="text.account.myDigitalProducts.noProducts" />
        </div>
    </div>
</c:if>
<c:if test="${not empty orderEntries.results}">
<div class="row">
      <div class="col-xs-12 col-sm-8">
        <h1 class="full-width-gray"><spring:theme code="text.account.myDigitalProducts"/></h1>
      </div>
      <div class="col-xs-12 col-sm-4">
        <div class="top-pagination">
          <div class="account-orderhistory-pagination">
            <wileycomNav:pagination searchPageData="${orderEntries}" searchUrl="${searchUrl}"/>
          </div>
        </div>
      </div>
</div>
<p class="product-list-item" />
<div class="product-list subscription-product-list my-account-wrap wrap">
	<c:forEach items="${orderEntries.results}" var="entry" varStatus="status">
		<product:accountDigitalProduct entry="${entry}"/>
	</c:forEach>
</div>
<div class="row">
  <div class="col-sm-12">
     <wileycomNav:pagination searchPageData="${orderEntries}" searchUrl="${searchUrl}"/>
  </div>
</div>
</c:if>