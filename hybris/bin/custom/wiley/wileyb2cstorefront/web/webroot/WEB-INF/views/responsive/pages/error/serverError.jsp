<%@ page trimDirectiveWhitespaces="true" isErrorPage="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta name="viewport" content="width=device-width">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="format-detection" content="telephone=no">
    <title>
        <spring:theme code="server.error.page.title"/>
    </title>
    <link rel="stylesheet" type="text/css" media="all" href="${commonResourcePath}/css/style.css">
    <style>
        @media (min-width: 641px) {
            body, html {
                min-width: 0;
            }
        }
    </style>
</head>
<body>
<spring:theme code="server.error.logo.alt.text" var="altText"/>
<spring:theme code="server.error.logo.url" var="logoUrl"/>
<div class="wiley-server-error-page">
    <header class="header-light">
        <div class="container-fluid">
            <div class="container">
                <a href="${logoUrl}" title="${altText}" class="logo">
                    <img src="${commonResourcePath}/images/logo.png" alt="${altText}">
                </a>
            </div>
        </div>
    </header>

    <main class="server-error">
        <div class="container">
            <h1>
                <spring:theme code="server.error.content.title"/>
            </h1>
            <p>
                <spring:theme code="server.error.content.paragraph.1"/>
            </p>
            <p>
                <spring:theme code="server.error.content.paragraph.2"/>
            </p>
        </div>
    </main>

    <footer class="footer-light">
        <div class="container-fluid">
            <div class="container">
                <div class="footer-copyright-text">
                    <p>
                        <spring:theme code="server.error.content.copyright"
                                      arguments="<script>document.write(new Date().getFullYear())</script>"/>
                    </p>
                </div>
                <a href="${logoUrl}" title="${altText}" class="logo">
                    <img src="${commonResourcePath}/images/logo.png" alt="${altText}">
                </a>
            </div>
        </div>
    </footer>
</div>
</body>
</html>