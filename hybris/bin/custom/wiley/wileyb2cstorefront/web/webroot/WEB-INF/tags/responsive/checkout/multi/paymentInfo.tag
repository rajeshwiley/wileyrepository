<%@ attribute name="cartData" required="false" type="de.hybris.platform.commercefacades.order.data.CartData" %>
<%@ attribute name="paymentInfo" required="true" type="de.hybris.platform.commercefacades.order.data.CCPaymentInfoData" %>
<%@ attribute name="showPaymentInfo" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:if test="${not empty paymentInfo && showPaymentInfo}">
<div class="payment-type">
	<div class="row">
		<div class="col-xs-12 col-sm-2">
			<div class="payment-type-title"><spring:theme code="checkout.multi.payment" text="Payment:"></spring:theme></div>
		</div>
		<div class="col-xs-12 col-sm-10">
			<div class="payment-type-info">
				<c:choose>
					<c:when test="${paymentInfo.cardType eq 'PAYPAL'}">
						<spring:theme code="checkout.multi.payment.paypal" text="Paypal" />,
						<c:if test="${not empty paymentInfo.billingAddress}">
							${fn:escapeXml(paymentInfo.billingAddress.title)}${' '}
							${fn:escapeXml(paymentInfo.billingAddress.firstName)}${' '}
							${fn:escapeXml(paymentInfo.billingAddress.lastName)},
						</c:if>
					</c:when>
					<c:otherwise>
						<c:if test="${not empty paymentInfo.billingAddress}"> ${fn:escapeXml(paymentInfo.billingAddress.title)}</c:if>
						${fn:escapeXml(paymentInfo.accountHolderName)},
						<c:if test="${not empty paymentInfo.billingAddress}">
							${fn:escapeXml(paymentInfo.billingAddress.title)}${' '}
						</c:if>
						${fn:escapeXml(paymentInfo.cardTypeData.name)},
						${fn:escapeXml(paymentInfo.cardNumber)},
						${paymentInfo.expiryMonth}/${paymentInfo.expiryYear},
					</c:otherwise>
				</c:choose>
				<c:if test="${not empty paymentInfo.billingAddress}">${fn:escapeXml(paymentInfo.billingAddress.line1)},
					<c:if test="${not empty paymentInfo.billingAddress.line2}">${fn:escapeXml(paymentInfo.billingAddress.line2)},</c:if>
					${fn:escapeXml(paymentInfo.billingAddress.town)},
					${fn:escapeXml(paymentInfo.billingAddress.region.name)}${' '}${fn:escapeXml(paymentInfo.billingAddress.postalCode)},
					${fn:escapeXml(paymentInfo.billingAddress.country.name)}
				</c:if>
			</div>
		</div>
	</div>
</div>
</c:if>

