<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>

<%@attribute name="menuItems" required="true" type="java.util.HashMap" %>
<%@attribute name="marketingSlotName" required="true" %>

<c:if test="${menuItems.containsKey(marketingSlotName)}">
    <c:forEach var="item" items="${menuItems[marketingSlotName]}">
        <li>
            <product:productContentMenuLinkItem link="${item.anchorLink}" title="${item.title}"/>
        </li>
    </c:forEach>
</c:if>