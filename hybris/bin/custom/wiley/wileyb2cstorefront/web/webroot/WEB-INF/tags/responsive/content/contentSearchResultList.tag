<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="searchPageData" required="true" type="com.wiley.core.search.facetdata.Wileyb2cContentSearchPageData" %>

<c:forEach items="${searchPageData.results}" var="content">
	<article class="content-block">
		<header>
			<a href="${content.url}"><h4>${content.title}</h4></a>
		</header>
		<p>${content.content}</p>
		<a href="${content.url}">${content.url}</a>
	</article>
</c:forEach>
