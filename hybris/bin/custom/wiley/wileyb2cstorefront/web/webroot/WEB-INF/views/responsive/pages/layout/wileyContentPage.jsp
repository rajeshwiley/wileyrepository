<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>

<template:page pageTitle="${pageTitle}" containerCSSClass="container-fluid partnership-solution-page" clearContainerClass="true">
    <div class="row">
            <div class="container">
                <cms:pageSlot position="Breadcrumb" var="comp">
                    <cms:component component="${comp}"/>
                </cms:pageSlot>
            </div>
            <div class="hero-banner collection-banner">
                <div class="hero-banner-component">
                  <cms:pageSlot position="TopContentBackground" var="comp">
                      <cms:component component="${comp}"/>
                  </cms:pageSlot>
                </div>
                <div class="container">
                    <article class="section-description">
                        <cms:pageSlot position="TopContent" var="comp">
                            <cms:component component="${comp}"/>
                        </cms:pageSlot>
                    </article>
                </div>
            </div>
            <div class="container page-content-wrapper">
                <cms:pageSlot position="BodyContent" var="comp">
                    <cms:component component="${comp}"/>
                </cms:pageSlot>
            </div>
        </div>
</template:page>