<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ attribute name="showProductDescription" required="false" type="java.lang.Boolean" %>
<%@ attribute name="showProductDetails" required="false" type="java.lang.Boolean" %>
<%@ attribute name="showPurchaseOptions" required="false" type="java.lang.Boolean" %>
<%@ attribute name="showDiscountedPrice" required="false" type="java.lang.Boolean" %>
<%@ attribute name="showSetComponents" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="action" tagdir="/WEB-INF/tags/responsive/action" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="showProductDescription" value="${(empty showProductDescription) ? false : showProductDescription}"/>
<c:set var="showPurchaseOptions" value="${(empty showPurchaseOptions) ? true : showPurchaseOptions}"/>
<c:set var="showDiscountedPrice" value="${(empty showDiscountedPrice) ? false : showDiscountedPrice}"/>
<c:set var="showSetComponents" value="${(empty showSetComponents) ? false : showSetComponents}"/>

<spring:theme code="text.addToCart" var="addToCartText"/>
<spring:theme code="text.learnMore" var="learnMoreText"/>
<spring:theme code="text.type" var="typeText"/>
<spring:theme code="text.publishedOn" var="publishedOnText"/>
<spring:theme code="text.quantity" var="quantityText"/>
<spring:theme code="text.price" var="priceText"/>
<spring:theme code="text.originalPrice" var="originalPriceText"/>
<spring:theme code="text.discountedPrice" var="discountedPriceText"/>
<spring:theme code="text.setIncludes" var="setIncludesText"/>
<c:url value="${product.url}" var="productUrl"/>
<section class="product-item ">

    <div class="product-image">
        <c:choose>
            <c:when test="${empty product.showPdpUrl || product.showPdpUrl}">
                <a href="${productUrl}">
                    <product:productPrimaryReferenceImage product="${product}" format="thumbnail"/>
                </a>
            </c:when>
            <c:otherwise>
                <product:productPrimaryReferenceImage product="${product}" format="thumbnail"/>
            </c:otherwise>
        </c:choose>
        <c:if test="${product.showRelatedProductsDisclaimer}">
            <div class="product-teaser">
                <i class="icon-ok-circle"></i>
                <spring:theme code="text.relatedProductsDisclaimer"/>
            </div>
        </c:if>
    </div>
    <div class="product-content">
        <c:if test="${product.price.optionalTaxShortMessage != null}">
            <p class="price-disclaimer">
                    ${product.price.optionalTaxShortMessage}
                <a role="menuitem" tabindex="0" data-placement="bottom-right" aria-label="information icon"
                   data-container-class="wiley-tax-popover" data-content='${fn:escapeXml(product.price.optionalTaxTooltip)}'
                   class="icon-info"></a>
            </p>
        </c:if>
        <h3 class="product-title">
            <c:choose>
                <c:when test="${empty product.showPdpUrl || product.showPdpUrl}">
                    <a href="${productUrl}">${product.name}</a>
                </c:when>
                <c:otherwise>
                    ${product.name}
                </c:otherwise>
            </c:choose>
        </h3>
        <div class="product-author">${product.authorsWithoutRoles}</div>
        <c:if test="${showProductDescription}">
            <div class="product-description">${product.description}</div>
        </c:if>
        <c:if test="${product.showRelatedProductsDisclaimer}">
            <div class="product-teaser">
                <i class="icon-ok-circle"></i>
                <spring:theme code="text.relatedProductsDisclaimer"/>
            </div>
        </c:if>
        <c:if test="${showPurchaseOptions}">

            <div class="product-table-flexible">
                <div class="product-table-container">
                    <div class="product-table-body">
                        <c:forEach var="variantOption" items="${product.variantOptions}">
                            <c:set var="isAvailableForPurchase" value="${variantOption.available}"/>
                            <div class="product-table-row">
                                <div class="table-row-content">
                                    <div class="product-type">
                                        <div>
                                            <span class="product-type-name">
                                            	${variantOption.purchaseOptionName}
                                            	<c:if test="${not variantOption.hasExternalStores && not empty variantOption.inventoryStatus && variantOption.inventoryStatus.visible}">
                                                    <i class="product-badge"><spring:theme
                                                            code="product.inventoryStatus.${variantOption.inventoryStatus.statusCode}"/></i>
                                                </c:if>
                                                <c:if test="${not empty variantOption.purchaseOptionDescription}">
                                                    <span class="product-info-icon">&nbsp;<a role="menuitem" tabindex="0"
                                                                                             aria-label="information icon"
                                                                                             class="icon-info black"
                                                                                             data-content='${variantOption.purchaseOptionDescription.replaceAll("\'", "&#39;")}'></a></span>
                                                </c:if>
                                            </span>
                                        </div>
                                    </div>
                                    <c:choose>
                                        <c:when test="${not empty variantOption.publicationDate}">
                                            <fmt:formatDate value="${variantOption.publicationDate}"
                                                            pattern="${publicationDatePattern}" var="publicationDate"/>
                                            <div class="product-date">${publicationDate}</div>
                                        </c:when>
                                        <c:otherwise>
                                            <div class="product-date hidden-xs"></div>
                                        </c:otherwise>
                                    </c:choose>
                                    <c:choose>
                                        <c:when test="${not empty variantOption.studentPrice}">
                                            <ycommerce:testId code="searchPage_price_label_${variantOption.code}">
                                                <div class="product-price gray">
                                                    <c:if test="${not variantOption.hidePrice}">
                                                        <format:price priceData="${variantOption.priceData}"/>
                                                    </c:if>
                                                </div>
                                            </ycommerce:testId>
                                            <div class="product-price">
                                                <c:if test="${showDiscountedPrice}">
                                                    <ycommerce:testId code="searchPage_student_price_label_${variantOption.code}">
                                                        <c:if test="${not variantOption.hidePrice}">
                                                            <format:price priceData="${variantOption.studentPrice}"/>
                                                        </c:if>
                                                    </ycommerce:testId>
                                                </c:if>
                                            </div>
                                        </c:when>
                                        <c:otherwise>
                                            <div class="product-price gray"></div>
                                            <ycommerce:testId code="searchPage_price_label_${variantOption.code}">
                                                <div class="product-price">
                                                    <c:if test="${not variantOption.hidePrice}">
                                                        <format:price priceData="${variantOption.priceData}"/>
                                                    </c:if>
                                                </div>
                                            </ycommerce:testId>
                                        </c:otherwise>
                                    </c:choose>

                                    <div class="product-quantity">
                                        <c:if test="${variantOption.countable && not variantOption.hasExternalStores && isAvailableForPurchase}">
                                            <input class="product-item-spinner" aria-label="product quantity">
                                        </c:if>
                                    </div>

                                    <c:choose>
                                        <c:when test="${variantOption.hasExternalStores && variantOption.externalStores.size() == 1}">
                                            <div class="product-button">
                                                <a class="small-button learn-more-button"
                                                   href="${variantOption.externalStores[0].url}" target="_blank">
                                                    <spring:theme
                                                            code="product.button.wiley.web.link.${variantOption.externalStores[0].type}"/>
                                                </a>
                                            </div>
                                        </c:when>
                                        <c:when test="${variantOption.hasExternalStores}">
                                            <div class="product-button">
                                                <a class="small-button learn-more-button"
                                                   href="<c:url value="${variantOption.url}"/>"
                                                   target="_blank">${learnMoreText}</a>
                                            </div>
                                        </c:when>
                                        <c:when test="${isAvailableForPurchase}">
                                            <c:set var="product" value="${variantOption}" scope="request"/>
                                            <div class="product-button">
                                                <a class="small-button">
                                                    <action:actions element="div" parentComponent="${component}"/>
                                                </a>
                                            </div>
                                        </c:when>
                                        <c:otherwise>
                                            <div class="product-button">
                                                <span class="pr-not-available"><spring:theme
                                                        code="product.not.available.search.message"/></span>
                                            </div>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                                <c:if test="${showSetComponents && not empty variantOption.productSetComponents}">
                                    <div class="product-includes">
                                        <p>${setIncludesText}:
                                        <ul>
                                            <c:forEach items="${variantOption.productSetComponents}" var="component">
                                                <li>
                                                    <c:choose>
                                                        <c:when test="${component.showPdpUrl}">
                                                            <c:url value="${component.url}" var="componentUrl"/>
                                                            <a href="${componentUrl}" class="">${component.name}</a>
                                                        </c:when>
                                                        <c:otherwise>
                                                            ${component.name}
                                                        </c:otherwise>
                                                    </c:choose>
                                                </li>
                                            </c:forEach>
                                        </ul>
                                        </p>
                                    </div>
                                </c:if>
                            </div>
                        </c:forEach>
                    </div>
                </div>
            </div>
        </c:if>
    </div>
</section>






