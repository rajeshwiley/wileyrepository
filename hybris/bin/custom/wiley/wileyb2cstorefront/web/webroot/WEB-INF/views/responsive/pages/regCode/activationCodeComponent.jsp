<%@ taglib prefix="purchaseOption" tagdir="/WEB-INF/tags/responsive/purchaseOption"%>
<%@ taglib prefix='spring' uri='http://www.springframework.org/tags'%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>


<div col="row">
	<div col="col-sm-12">
		<div class="support-message"><span class="progress-number"><spring:theme code="regCode.activation.paragraph.part1"/></span><spring:theme code="regCode.activation.paragraph.part2"/></div>
		<div class="site-form central-form">
			<form:form method="post"  commandName="regCodeActivationForm">
				<div class="form-cell heading">
					<h1><spring:theme code="regCode.activation.buttonLabel"/></h1>
				</div>
				<div class="form-cell"><spring:theme code="regCode.activation.paragraph"/></div>
				<div class="form-cell">
					<div class="form-field">
						<formElement:formInputBox idKey="regCode" labelKey="regCode.activation.formLabel" path="regCode" value="${invalidRegCode}" placeholder="<registration code>" />
					</div>
				</div>
				<div class="form-cell">
					<button class="button button-main large" type="submit"><spring:theme code="regCode.activation.buttonLabel" /></button>
				</div>
			</form:form>
		</div>
	</div>
</div>
