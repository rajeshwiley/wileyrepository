<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%-- lazy owl attributes are added to avoid issue with carousel functionality --%>
<div class="container lazyOwl owl-loaded ${fn:join(cssClasses, ' ')}" id="${component.videoId}" style="${component.cssStyle}">
	<div style="display: block; position: relative; max-width: 100%; margin-bottom: 40px;">
		<div style="padding-top: 56.25%;">
			<video data-video-id="${component.videoId}" data-account="${accountId}" data-player="${component.playerId}" data-embed="default" class="video-js" controls=""
				style="width: 100%; height: 100%; position: absolute; top: 0px; bottom: 0px; right: 0px; left: 0px;">
			</video>
    		<script src="//players.brightcove.net/${accountId}/${component.playerId}_default/index.min.js"></script>
    	</div>
    </div>
</div>