<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:if test="${not empty product.series}">
    <section id="series-section" class="page-section">
        <h2 data-toggle="collapse" data-target="#series-section-item" class="section-title collapsed">
            <spring:theme code="product.product.details.series"/>
        </h2>
        <div id="series-section-item" class="series-section-blocks collapse">
            <c:forEach items="${product.series}" var="item">
                <c:url var="seriesUrl" value="${item.url}"/>
                <p><a href="${seriesUrl}">${item.name}</a></p>
            </c:forEach>
        </div>
    </section>
</c:if>