<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>

<div class="carousel-default">
		<div class="carousel-gallery">
			<div class="carousel">
				<div class="owl-carousel ${heroBannerCarouselClass}" <c:if test="${carouselConfig ne null}">data-component-model='${carouselConfig}'</c:if>>
					<c:forEach items="${component.items}" var="item">
						<div class="item">
							<cms:component component="${item}" evaluateRestriction="true"/>
						</div>
					</c:forEach>
				</div>
			</div>
		</div>
</div>