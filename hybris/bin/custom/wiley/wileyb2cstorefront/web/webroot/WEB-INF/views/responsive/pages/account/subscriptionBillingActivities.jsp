<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="wileycomNav" tagdir="/WEB-INF/tags/addons/wileycomstorefrontcommons/responsive/nav"%>
  
<div class="row">
	<div class="col-xs-12 col-sm-9">
		<h1 class="full-width-gray">
			<spring:theme code="text.account.manageSubscriptions.billingActivities.title"/>
		</h1>
	</div>
	<div class="col-xs-12 col-sm-3">
		<div class="title-tools text-right">
			<spring:url value="/my-account/subscriptions/${subscriptionData.code}" var="backLink"/>
			<a href="${backLink}">
				<spring:theme code="text.account.manageSubscriptions.billingActivities.return.link"/>
			</a>
		</div>
	</div>
</div>

<div class="my-account-wrap">
	<div class="row">
		<div class="col-xs-12">
			<div class="plan-name">
				<h3>${subscriptionData.name}</h3>
				<c:if test="${not empty subscriptionData.billingFrequency}">
					<p>${subscriptionData.billingFrequency.name}</p>
				</c:if>
			</div>
		</div>
	</div>

	<div class="col-xs-12">
		<c:choose>
			<c:when test="${not empty subscriptionTransactions and not empty subscriptionTransactions.results}">
				<div class="common-table">
					<div class="thead hidden-xs">
						<div class="row">
							<div class="col-sm-3"><spring:theme code="text.account.manageSubscriptions.billingActivities.billingDate"/></div>
							<div class="col-sm-3"><spring:theme code="text.account.manageSubscriptions.billingActivities.paymentAmount"/></div>
							<div class="col-sm-3"><spring:theme code="text.account.manageSubscriptions.billingActivities.paymentStatus"/></div>
							<div class="col-sm-3"><spring:theme code="text.account.manageSubscriptions.billingActivities.paymentDetails"/></div>
						</div>
					</div>
					<div class="tbody">
						<c:forEach items="${subscriptionTransactions.results}" var="billingRow">
							<div class="item">
								<div class="row">
									<div class="col-xs-12 col-sm-3">
										<div class="row">
											<div class="col-xs-6 thead visible-xs">
												<spring:theme code="text.account.manageSubscriptions.billingActivities.billingDate"/>
											</div>
											<div class="col-xs-6 col-sm-12">
												<fmt:formatDate value="${billingRow.billingDate}" pattern="MM/dd/yyyy" />
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-3">
										<div class="row">
											<div class="col-xs-6 thead visible-xs">
												<spring:theme code="text.account.manageSubscriptions.billingActivities.paymentAmount"/>
											</div>
											<div class="col-xs-6 col-sm-12">
												<c:out value="${billingRow.paymentAmount}" />
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-3">
										<div class="row">
											<div class="col-xs-6 thead visible-xs">
												<spring:theme code="text.account.manageSubscriptions.billingActivities.paymentStatus"/>
											</div>
											<div class="col-xs-6 col-sm-12">
												<spring:theme code="text.type.PaymentTransactionType.${billingRow.paymentStatus}"/>
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-3">
										<div class="row">
											<div class="col-xs-6 thead visible-xs">
												<spring:theme code="text.account.manageSubscriptions.billingActivities.paymentDetails"/>
											</div>
											<div class="col-xs-6 col-sm-12">
												<spring:theme code="text.type.PaymentModeEnum.${billingRow.paymentMode}"/>
												<c:if test="${billingRow.paymentMode eq 'CARD'}">
													, ${billingRow.creditCardMask}
												</c:if>
											</div>
										</div>
									</div>
								</div>
							</div>
						</c:forEach>
					</div>
				</div>
			</c:when>
			<c:otherwise>
				<p class="no-billing-found">
					<spring:theme code="text.account.manageSubscriptions.billingActivities.noBillingActivity"/>
				</p>
			</c:otherwise>
		</c:choose>
	</div>
</div>

<div class="row">
    <div class="col-sm-12">
        <wileycomNav:pagination searchPageData="${subscriptionTransactions}" searchUrl="${searchUrl}"/>
    </div>
</div>
