<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>


<h1 class="full-width-gray">
    <spring:theme code="text.account.profile.updateEmail" text="Update Email"/>
</h1>
<div class="my-account-wrap">
    <div class="row">
        <div class="col-xs-12 col-sm-4">
            <div class="site-form">
                <form:form action="${action}" method="post" commandName="updateEmailForm">
                    <formElement:formInputBox idKey="profile.email" labelKey="profile.email" path="email"
                        inputCSS="text" mandatory="true" />
                    <formElement:formInputBox idKey="profile.checkEmail" labelKey="profile.checkEmail" path="chkEmail"
                        inputCSS="text" mandatory="true" />
                    <formElement:formPasswordBox idKey="profile.pwd" labelKey="profile.pwd" path="password"
                        inputCSS="text form-control" mandatory="true" />

                    <input type="hidden" id="recaptchaChallangeAnswered"
                        value="${requestScope.recaptchaChallangeAnswered}" />
                    <div class="form_field-elements control-group js-recaptcha-captchaaddon"></div>

                    <div class="my-account-button-wrap">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <ycommerce:testId code="email_saveEmail_button">
                                    <button type="submit" class="button button-main large">
                                        <spring:theme code="text.account.profile.updateEmail.save" text="Update" />
                                    </button>
                                </ycommerce:testId>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <ycommerce:testId code="email_cancelEmail_button">
                                    <button type="button" class="button button-outlined large backToHome">
                                        <spring:theme code="text.account.profile.cancel" text="Cancel" />
                                    </button>
                                </ycommerce:testId>
                            </div>
                        </div>
                    </div>
                </form:form>
            </div>
        </div>
    </div>
</div>