<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>
	
<template:page pageTitle="${pageTitle}" containerCSSClass="shopping-cart-page wrap">
	<cart:cartValidation/>
	<cart:cartPickupValidation/>


		<c:if test="${not empty cartData.entries}">
		  <div class="row">
			  <div class="col-xs-12 col-sm-12">
				  <h1 class="page-title"><spring:theme code="basket.page.section.title" text="Shopping Cart" /></h1>
			  </div>
		  </div>
		  <div class="row">
		  	 <div class="col-xs-12 col-sm-7">
				<cms:pageSlot position="TopContent" var="feature">
					<cms:component component="${feature}"/>
				</cms:pageSlot>
			</div>
			<div class="col-xs-12 col-sm-4 col-sm-offset-1">
				<div class="order-brief-section">
				<div class="order-total-section">
				   <cms:pageSlot position="CenterLeftContentSlot" var="feature">
					   <cms:component component="${feature}"/>
				   </cms:pageSlot>
					<cms:pageSlot position="CenterRightContentSlot" var="feature">
						<cms:component component="${feature}"/>
					</cms:pageSlot>
				</div>
				</div>
			</div>
		</div>
		<cms:pageSlot position="BottomContentSlot" var="feature">
			<cms:component component="${feature}"/>
		</cms:pageSlot>
		</c:if>

		<c:if test="${empty cartData.entries}">
			<cms:pageSlot position="EmptyCartMiddleContent" var="feature" element="div">
				<cms:component component="${feature}"/>
			</cms:pageSlot>
		</c:if>

</template:page>