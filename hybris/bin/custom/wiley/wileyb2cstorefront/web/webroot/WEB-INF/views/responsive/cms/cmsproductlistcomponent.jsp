<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>

<c:set var="themeMsgKey" value="${not empty msgKey ? msgKey : 'search.page'}"/>

<div class="product-list-wrapper">
<c:choose>
	 <c:when test="${not empty searchPageData.results}">
	 <c:set var="currentSearchUrl" value="${searchUrl}${currentSearchQuery}" />
	 <div class="search-results-top-bar">
  	<nav:sortoptions searchPageData="${searchPageData}"/>
	 <nav:pagination top="false" msgKey="search.page"
  	        showCurrentPageInfo="true"
  	        hideRefineButton="true"
  	        supportShowPaged="${isShowPageAllowed}"
  	        supportShowAll="${isShowAllAllowed}"
  	        searchPageData="${searchPageData}"
  	        searchUrl="${currentSearchUrl}"
  	        numberPagesShown="${numberPagesShown}"/>
   </div>
	   <div class="products-list">
		  <c:forEach items="${searchPageData.results}" var="product" varStatus="status">
			 <product:productListerItem product="${product}"/>
		  </c:forEach>
	   </div>
	   <nav:pagination top="false" msgKey="search.page"
	        showCurrentPageInfo="true"
	        hideRefineButton="true"
	        supportShowPaged="${isShowPageAllowed}"
	        supportShowAll="${isShowAllAllowed}"
	        searchPageData="${searchPageData}"
	        searchUrl="${currentSearchUrl}"
	        numberPagesShown="${numberPagesShown}"/>

	   <div class="addToCartTitle" style="display:none">
		  <div class="add-to-cart-header">
			 <div class="headline">
				<span class="headline-text">
				   <spring:theme code="basket.added.to.basket"/>
				</span>
			 </div>
		  </div>
	   </div>
	 </c:when>
	 <c:otherwise>
	  <div class="container">
		<div class="empty-result">
		  <h5><spring:theme code="search.list.zero.products"/></h5>
		</div>
	  </div>
	 </c:otherwise>
 </c:choose>
 <product:addToCartPopupTemplate/>
</div>