<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="searchUrl" required="true" %>
<%@ attribute name="searchPageData" required="true" type="de.hybris.platform.commerceservices.search.pagedata.SearchPageData" %>
<%@ attribute name="numberPagesShown" required="true" type="java.lang.Integer" %>
<%@ attribute name="themeMsgKey" required="true" %>
<%@ attribute name="isNumberOfPagesShown" required="true" type="java.lang.Boolean" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<c:set var="hasPreviousPage" value="${searchPageData.pagination.currentPage > 0}"/>
<c:set var="hasNextPage" value="${(searchPageData.pagination.currentPage + 1) < searchPageData.pagination.numberOfPages}"/>

<ul class="pagination">
	<c:choose>
				<c:when test="${hasPreviousPage}">
					<li class="pagination-prev">
						<spring:url value="${searchUrl}" var="previousPageUrl" htmlEscape="true">
							<spring:param name="page" value="${searchPageData.pagination.currentPage - 1}"/>
						</spring:url>
						<ycommerce:testId code="searchResults_previousPage_link">
							<a title="Previous page" href="${previousPageUrl}"><spring:theme code="search.page.firstPage"/></a>
						</ycommerce:testId>
					</li>
				</c:when>
				<c:otherwise>
					<li class="pagination-prev disabled">
					  <span><spring:theme code="search.page.firstPage"/></span>
					</li>
				</c:otherwise>
	</c:choose>

 <li class="page-number">
   <spring:url value="${searchUrl}" var="previousPageUrl" htmlEscape="true">
 					<spring:param name="page" value="${searchPageData.pagination.currentPage + 1}"/>
 		</spring:url>
			<form class="page-number-form" action="${searchUrl}" autocomplete="off">
					<input type="text" name="page" aria-label="Page Number Input" value="${searchPageData.pagination.currentPage + 1}" maxlength="3">
			</form>
	</li>

	<c:choose>
  <c:when test="${hasNextPage}">
			<li class="pagination-next">
				<spring:url value="${searchUrl}" var="previousPageUrl" htmlEscape="true">
					<spring:param name="page" value="${searchPageData.pagination.currentPage + 1}"/>
				</spring:url>
				<ycommerce:testId code="searchResults_previousPage_link">
					<a title="Next page" href="${previousPageUrl}"><spring:theme code="search.page.lastPage"/></a>
				</ycommerce:testId>
			</li>
  </c:when>
  <c:otherwise>
			<li class="pagination-next disabled">
			  <span><spring:theme code="search.page.lastPage"/></span>
			</li>
  </c:otherwise>
 </c:choose>
	<c:if test="${isNumberOfPagesShown}">
			<li class="pagination-quantity-text">
				<spring:theme code="${themeMsgKey}.ofPages" arguments="${searchPageData.pagination.numberOfPages}"/>
			</li>
	</c:if>
</ul>