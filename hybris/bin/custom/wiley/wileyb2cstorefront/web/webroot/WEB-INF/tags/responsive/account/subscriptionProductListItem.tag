<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ attribute name="wileySubscriptionData" required="true" type="com.wiley.facades.product.data.WileySubscriptionData" %>

<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>
<%@ taglib prefix="account" tagdir="/WEB-INF/tags/responsive/account"%>

<c:url value="/my-account/subscriptions" var="subscriptionUrl" />

<div class="product-list-item">
    <div class="row">
        <div class="col-xs-12 col-sm-9">
            <div class="product-list-image">
                <c:choose>
                    <c:when test="${wileySubscriptionData.image ne null}">
                        <img src="${wileySubscriptionData.image}" alt="product image">
                    </c:when>
                    <c:otherwise>
                        <common:fallbackImage/>
                    </c:otherwise>
                </c:choose>
            </div>
            <div class="product-list-info">
                <div class="product-list-title">${wileySubscriptionData.name}</div>
                <div class="product-list-description">
                    <div class="description">

                        <c:set var="dateFormatPattern" value="MMM dd, yyyy"/>
                        <fmt:formatDate value="${wileySubscriptionData.startDate}" var="startDate" pattern="${dateFormatPattern}" />
                        <fmt:formatDate value="${wileySubscriptionData.endDate}" var="endDate" pattern="${dateFormatPattern}" />

                        <div><spring:theme code="account.subscription.manageSubscriptions.startDate" argumentSeparator=";" arguments="${startDate}"/></div>
                        <div><spring:theme code="account.subscription.manageSubscriptions.endDate" argumentSeparator=";" arguments="${endDate}"/></div>
                        <spring:theme code="subscription.displayStatus.${wileySubscriptionData.status.code}" var="subscriptionStatus"/>
                        <div class="product-status"><div><spring:theme code="account.subscription.manageSubscriptions.status" arguments="${subscriptionStatus}"/></div></div>
                    </div>
                    <account:subscriptionPaymentDetails paymentInfoData="${wileySubscriptionData.paymentInfo}"/>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-3 text-right">
            <div class="my-account-button-small">
                <a href="${subscriptionUrl}/${wileySubscriptionData.code}">
                    <button class="button button-main large">
                        <spring:theme code="account.subscription.manageSubscriptions.manageButton" />
                    </button>
                </a>
            </div>
        </div>
    </div>
</div>