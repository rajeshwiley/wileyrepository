<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="excerpts" required="true" type="java.util.List"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ attribute name="additionalClassForMainTag" required="false" %>
<c:set var="mobilePerspectiveClass">
	<c:if test="${additionalClassForMainTag eq 'visible-xs'}">-mobile</c:if>
</c:set>


<c:if test="${not empty excerpts}">
 <div class="dropdown-block read-excerpt ${additionalClassForMainTag}">
	<button data-toggle="collapse" data-target="#read-excerpt-files${mobilePerspectiveClass}" class="collapsed">Read an Excerpt</button>
	<nav id="read-excerpt-files${mobilePerspectiveClass}" class="collapse" style="height: 1px;">
		<c:forEach items="${excerpts}" var="excerpt">
			<a href="${excerpt.url}" target="_blank" title="">${excerpt.name}</a>
		</c:forEach>
	</nav>
  </div>
</c:if>