<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="searchPageData" required="true" type="de.hybris.platform.commerceservices.search.pagedata.SearchPageData" %>
<%@ attribute name="totalNumberOfProductResults" required="true" type="java.lang.Long" %>
<%@ attribute name="totalNumberOfContentResults" required="true" type="java.lang.Long" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>

<c:set var="freeTextSearch" value="${searchPageData != null ? searchPageData.freeTextSearch : null}"/>
<c:set var="spellingSuggestion" value="${searchPageData != null ? searchPageData.spellingSuggestion : null}"/>
<c:set var="totalNumberOfResults" value="${totalNumberOfProductResults + totalNumberOfContentResults}"/>

<div class="search-result-header">
    <div class="container">
        <nav:searchSpellingSuggestion spellingSuggestion="${spellingSuggestion}"/>
        <c:if test="${totalNumberOfResults > 0}">
            <ul class="nav nav-tabs search-result-tabs">
                <c:if test="${!hideProductsTab}">
                    <li class="${activeTab == 'PRODUCTS' ? 'active' : ''}">
                        <a ${activeTab == 'CONTENT' ? 'href="'.concat(enabledTabPath).concat('"') : ''}>
                            <spring:theme code="search.page.tab.products"/>
                            <span class="nav-tabs-results"><i class="hidden-xs">(</i><i>${totalNumberOfProductResults}</i> <spring:theme code="search.header.results"/><i class="hidden-xs">)</i>
							</span>
                        </a>
                    </li>
                </c:if>
                <c:if test="${!hideContentTab}">
                    <li class="${activeTab == 'CONTENT' ? 'active' : ''}">
                        <a ${activeTab == 'PRODUCTS' ? 'href="'.concat(enabledTabPath).concat('"') : ''}>
                            <spring:theme code="search.page.tab.content"/>
                            <span class="nav-tabs-results"><i class="hidden-xs">(</i><i>${totalNumberOfContentResults}</i> <spring:theme code="search.header.results"/><i class="hidden-xs">)</i>
                            </span>
                        </a>
                    </li>
                </c:if>
            </ul>
        </c:if>
    </div>
</div>