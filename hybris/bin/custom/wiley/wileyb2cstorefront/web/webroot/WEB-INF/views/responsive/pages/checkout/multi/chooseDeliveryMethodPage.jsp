<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi"%>
<%@ taglib prefix="wileycomMulti-checkout" tagdir="/WEB-INF/tags/addons/wileycomstorefrontcommons/responsive/checkout/multi"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>

<template:page pageTitle="${pageTitle}" hideHeaderLinks="true" containerCSSClass="checkout-shipping-method">

    <jsp:attribute name="pageScripts">
        <script type="text/javascript" src="${contextPath}/_ui/addons/wileystorefrontcommons/responsive/common/js/wileyAjaxCalls.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                function checkErrorCases() {
                    var sectionExists = $("#deliveryCostItem").length;
                    if (!sectionExists) {
                        $("#selectDeliveryMethodForm").html('');
                        $("#deliveryMethodsLabel").remove();
                    }
                }


               $(document).on("change","#selectDeliveryMethodForm",function(e){
               	  e.preventDefault();
               	  var payload = $(this).serialize();

               	  var url = ACC.config.encodedContextPath + "/checkout/multi/delivery-method/select-refresh-order-total/";
               	  $.post(url, payload,  function(data) {
                    wiley.updateMultipleSectionsAjax(data, checkErrorCases);
               	  }).fail(function (error){
               	    log.error('error during ajax call');
               	  });
               });
            });
        </script>
    </jsp:attribute>

    <jsp:body>
        <div class="row">
            <div class="col-xs-12 col-sm-12">
                <cms:pageSlot position="SideContent" var="feature" element="div" class="support-message">
                    <cms:component component="${feature}"/>
                </cms:pageSlot>
                <h1 class="page-title icon-lock"><spring:theme code="checkout.multi.secure.checkout" text="Secure Checkout" /></h1>
             </div>
        </div>
       		<div class="row">
       			<multi-checkout:checkoutSteps checkoutSteps="${checkoutSteps}"
       				progressBarId="${progressBarId}">
       				<jsp:body>
       				<div class="checkout-accordion-content-wrap">
       				    <form id="selectDeliveryMethodForm" action="${request.contextPath}/checkout/multi/delivery-method/select" method="get">
                            <ycommerce:testId code="checkoutStepTwo">
                                <div class="checkout-delivery-option-dates">
                                    <div class="row">
                                        <c:choose>
                                            <c:when test="${not empty deliveryMethods}">
                                                <div id="selectDeliveryMethodBlock" class="col-xs-12 col-sm-6">
                                                    <div id="deliveryMethodsLabel" class="checkout-content-subtitle">
                                                        <spring:theme code="b2c.checkout.summary.deliveryMode.selectDeliveryMethodForOrder" text="Delivery Options and Arrival Dates"></spring:theme>
                                                    </div>
                                                    <div class="checkout-delivery-list">
                                                        <multi-checkout:deliveryMethodSelector deliveryMethods="${deliveryMethods}" selectedDeliveryMethodId="${cartData.deliveryMode.externalCode}" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6">
                                                    <wileycomMulti-checkout:checkoutDeliveryMessage />
                                                </div>
                                            </c:when>
                                            <c:otherwise>
                                                <div class="col-xs-12 col-sm-6">
                                                    <wileycomMulti-checkout:checkoutDeliveryMessage />
                                                </div>
                                            </c:otherwise>
                                        </c:choose>
                                    </div>
                                </div>

                                <multi-checkout:shipmentItems cartData="${cartData}" showDeliveryAddress="true" />

                                <div class="checkout-accordion-button-next">
                                    <button id="deliveryMethodSubmit" type="button" class="button button-main large">
                                        <spring:theme code="checkout.multi.deliveryMethod.continue" text="Next Step" />
                                    </button>
                                </div>
                            </ycommerce:testId>
                        </form>
       				</div>
       			</jsp:body>
       			</multi-checkout:checkoutSteps>
       			<multi-checkout:checkoutOrderDetails cartData="${cartData}"
       				showDeliveryAddress="true" showPaymentInfo="false"
       				showTaxEstimate="false" showTax="false" />

       		</div>
    </jsp:body>
</template:page>


