<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="content" tagdir="/WEB-INF/tags/responsive/content"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="contentSearchTopPagination">
<c:set var="currentSearchUrl" value="${searchUrl}${currentSearchQuery}" />
<nav:pagination top="false" msgKey="search.page"
  	        showCurrentPageInfo="true"
  	        hideRefineButton="true"
  	        supportShowPaged="${isShowPageAllowed}"
  	        supportShowAll="${isShowAllAllowed}"
  	        searchPageData="${searchPageData}"
  	        searchUrl="${currentSearchUrl}"
  	        numberPagesShown="${numberPagesShown}" />
</div>
<div id="CONTENT">
	<div class="content-list">
   		<content:contentSearchResultList searchPageData="${searchPageData}"/>
   </div>
</div>
<div class="contentSearchBottomPagination">
<nav:pagination top="false" msgKey="search.page"
  	        showCurrentPageInfo="true"
  	        hideRefineButton="true"
  	        supportShowPaged="${isShowPageAllowed}"
  	        supportShowAll="${isShowAllAllowed}"
  	        searchPageData="${searchPageData}"
  	        searchUrl="${currentSearchUrl}"
  	        numberPagesShown="${numberPagesShown}" />
</div>