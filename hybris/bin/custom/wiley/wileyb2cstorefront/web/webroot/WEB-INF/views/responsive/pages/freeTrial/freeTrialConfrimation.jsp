<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="freeTrial" tagdir="/WEB-INF/tags/responsive/freeTrial" %>

<spring:url value="/my-account/digital-products" var="myDigitalProductsUrl"/>

<div class="container free-trial confirmation wrap">
	<div class="row">
		<div class="col-xs-12 col-sm-6">
			<h1 class="full-width-gray"><spring:theme code="text.subscription.freeTrial.confirmation.title"/></h1>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-6 left-block">

			<freeTrial:freeTrialSubscriptionDetails/>

			<div class="success-message icon-check wrap">
				<p><spring:theme code="text.subscription.freeTrial.thankYouMessage"/></p>
			</div>
			<div class="button-wrap">
				<form method="get" action="${myDigitalProductsUrl}">
					<button class="button button-main large">
						<spring:theme code="text.subscription.freeTrial.confirmation.getStarted"/>
					</button>
				</form>
			</div>
		</div>
		<div class="col-xs-12 col-sm-6">
		</div>
	</div>
</div>

