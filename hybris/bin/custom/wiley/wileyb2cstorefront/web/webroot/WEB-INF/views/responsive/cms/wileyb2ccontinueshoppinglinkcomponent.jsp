<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:url value="${redirectURL}" var="continueShoppingUrl"/>

<div class="product-list-buttons">
	<button class="button button-secondary small continueShoppingButton" data-continue-shopping-url="${continueShoppingUrl}">${linkName}</button>
</div>