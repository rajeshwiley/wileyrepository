<%@ taglib prefix="purchaseOption" tagdir="/WEB-INF/tags/responsive/purchaseOption"%>
<%@ taglib prefix='spring' uri='http://www.springframework.org/tags'%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row">
	<div class="col-xs-12 col-sm-9">
		<h1 class="full-width-gray">
			<spring:message code="wileypluscourse.purchase.option.page-title" />
		</h1>
		<div class="purchase-options-summary gray-super-light wrap">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<div class="product-list-image">
						<img src="${course.imageUrl}" alt="product image">
					</div>
					<div class="product-list-info">
						<div class="product-list-title">${course.title}</div>
						<div class="product-list-description">
							<div class="product-list-author">${course.authors}</div>
							<c:if test="${course.returnUrl != null}">
                            	<c:choose>
                            		<c:when test="${course.returnUrl.message != null}">
                            			<c:set var="returnUrlMessage" value="${course.returnUrl.message}"/>
                            		</c:when>
                            		<c:otherwise>
                            			<spring:theme
                            					var="returnUrlMessage"
                            					code="wileypluscourse.purchase.option.product.returnUrl.message"/>
                            		</c:otherwise>
                            	</c:choose>
                            	<div class="additional-inform">
                            		<spring:message code="wileypluscourse.purchase.option.product.additional-information" /> &nbsp;<a href="${course.returnUrl.url}">${returnUrlMessage}</a>
                            	</div>
                            </c:if>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="purchase-accordion" class="panel-group">
			<purchaseOption:registrationCode />
			<purchaseOption:instantAccess />
			<purchaseOption:gracePeriod />
			<%-- limited access is not implemented yet. It will be covered in https://jira.wiley.ru/browse/ECSC-8318 --%>
			<%--<purchaseOption:limitedAccess />--%>
		</div>
	</div>
	<div class="col-xs-12 col-sm-2 col-sm-offset-1">
		<div class="purchase-options-note">
			<div class="purchase-options-note-title">
				<spring:message code="wileypluscourse.purchase.option.note.title" />
			</div>
			<div class="purchase-options-note-description">
				<spring:message code="wileypluscourse.purchase.option.note.description" />
			</div>
		</div>
	</div>
</div>