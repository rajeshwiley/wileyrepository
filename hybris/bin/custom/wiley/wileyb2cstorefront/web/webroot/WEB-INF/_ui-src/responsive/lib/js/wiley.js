// put custom site specific js here. This file is similar to js files which we use for welstorefront and agsstorefront.
if (!wiley) {
	var wiley = {};
}

if (!ACC) {
	var ACC = {};
}

wiley.KEY_CODE_MAP = {
	space: 32,
	enter: 13,
	escape: 27,
	tab: 9,
	keydown: 40,
	up: 38
};
/* Check tablet and mobile devices */
wiley.checkDevice = {
	isDeviceIOS: function () {
		return navigator.userAgent.match(/(iPhone|iPod|iPad)/i);
	},
	isMobileSafari: function () {
		return navigator.userAgent.match(/(iPhone|iPod)/i) && !navigator.userAgent.match(/(CriOS|OPiOS|FxiOS|mercury)/i);
	},
	isAndroid: function () {
		return navigator.userAgent.match(/Android/i);
	},
	isWindows: function () {
		return navigator.userAgent.match(/(IEMobile|webOS|Lumia)/i);
	},
	isIE: function () {
		return navigator.userAgent.search(/MSIE/) > 0 || navigator.userAgent.search(/NET CLR /) > 0;
	},
	isBlackBerry: function () {
		return navigator.userAgent.match(/(BlackBerry|BB10; Touch)/i);
	},
	isOpera: function () {
		return navigator.userAgent.match(/Opera Mini/i);
	},
	isAny: function () {
		return (this.isDeviceIOS() || this.isAndroid() || this.isWindows() || this.isBlackBerry() || this.isOpera());
	}
};

wiley.isMobileView = function(){
	return $(window).width() <= 640;
};

/* End check tablet and mobile devices */

/* Viewport fix for iPad */
wiley.iPadViewport = function () {
	var viewport = $("meta[name='viewport']");

	if (navigator.userAgent.match(/iPad/i)) {
		viewport.attr("content", "width=1240, initial-scale=0, user-scalable=1");
	}
};
/* End viewport fix for iPad */

wiley.owlCarousel = function (selector) {
	var configurationDefault = {
		navText: '',
		responsive: {
			0: {
				items: 3,
				margin: 20,
				nav: false,
				dots: true
			},
			641: {
				items: 5,
				nav: true,
				dots: false
			}
		}
	};

	try {
		$(selector).each(function () {
			var configuration = $(this).data('component-model') || configurationDefault;
			$.extend(configuration, {navElement: 'a tabindex="0"'});
			$(this).owlCarousel(configuration);
			var navigation = $(this).find('.owl-nav:not(.disabled)');
			if (navigation.length) {
				navigation.find('a').keydown(function (e) {
					if (e.keyCode === 13) {
						$(this).trigger('click');
					}
				});
			} else {
				$(this).css({padding: 0})
			}
		});
		new Imager('.js-responsive-image');
	} catch (e) {
		console.log(e);
	}

};


wiley.mediaGallery = function () {
	var sync1 = $("#pdp-carousel-sync1");
	var sync2 = $("#pdp-carousel-sync2");
	var heroBannerCarousel = $('.hero-banner-carousel');
	var container = heroBannerCarousel.parents('.hero-banner');
	var featuredProductsGallery = $('.plcGridCarousel');
	var searchBtn = $('.main-navigation-search').find('button');
	var featuredProductsGalleryItems = featuredProductsGallery.find('.product-item');
	var featuredProductsGalleryLength = featuredProductsGalleryItems.length;

	wiley.owlCarousel(".owl-carousel");

	heroBannerCarousel.find('.item-inner').each(function (index, item) {
		var url = $(this).data('url');
		if (url) {
			$(item).addClass('link-item');
			$(this).click(function () {
				window.location.href = url;
			});
		}

	});

	function accessibilitySupport() {
		var nav = heroBannerCarousel.find('.owl-nav').not('.disabled');
		var prev = nav.find('.owl-prev').not('.disabled');
		var next = nav.find('.owl-next').not('.disabled');
		var nextFocusElement = container.next().find('a');

		function setFocus(el) {
			return el.length && el.focus();
		}

		heroBannerCarousel.on('keyup', function (e) {
			var keyCode = (e.keyCode || e.which);
			var target = $(e.target);
			var activeLink = $('.owl-item.active').find('a');
			if (!target.parents('.owl-item.active').length && !target.parents('.owl-nav').length) {
				e.preventDefault();
				setFocus(prev) || setFocus(activeLink) || setFocus(nextFocusElement);
			}
		});

		heroBannerCarousel.on('keydown', function (e) {
			var keyCode = (e.keyCode || e.which);
			var target = $(e.target);
			var activeLink = $('.owl-item.active').find('a');
			if (e.shiftKey && keyCode === 9) { // SHIFT TAB
				if (!target.parents('.owl-nav').length) {
					e.preventDefault();
					setFocus(prev) || searchBtn.focus();
				} else if (target.hasClass('owl-next')) {
					e.preventDefault();
					setFocus(activeLink) || setFocus(prev);
				} else if (target.hasClass('owl-prev')) {
					e.preventDefault();
					searchBtn.focus();
				}
			} else if (keyCode === 9) { // TAB
				if (!target.parents('.owl-nav').length) {
					e.preventDefault();
					setFocus(next) || setFocus(nextFocusElement);
				} else {
					if (target.hasClass('owl-prev')) {
						e.preventDefault();
						setFocus(activeLink) || setFocus(next) || setFocus(nextFocusElement);
					}
				}
			}
		});
	}


	function setActiveElement(index) {
		sync2.find('.item').removeClass('synced');
		sync2.find('.item').eq(index).addClass('synced');
	}

	if (wiley.isMobileView() && featuredProductsGalleryLength) {
		for (var i = 0; i < featuredProductsGalleryLength; i += 2) {
			featuredProductsGalleryItems.slice(i, i + 2).wrapAll("<div class='plcGridCarouselItem'></div>");
		}
		featuredProductsGallery.addClass('owl-carousel').owlCarousel({
			items: 1,
			nav: false,
			dots: true,
			smartSpeed: 1000
		});
	}

	if (sync1.length) {
		sync1.addClass('owl-carousel');
		sync1.owlCarousel({
			items: 1,
			nav: false,
			dots: false,
			video: true,
			onDrag: function () {
				sync1.data("zoomEnable", false);
			},
			onChange: function () {
				var activeItem = sync1.find('.owl-item.active');
				var player = activeItem.find('video');
				if (player.length) {
					player[0].pause();
				}
			},
			onInitialized: function () {
				var items = sync1.find('.owl-item');
				items.each(function (index, item) {
					wiley.startZoom(item, sync1)
				});
			},
			onDragged: function () {
				var activeItem = sync1.find('.owl-item.active');
				var index = activeItem.index();
				setActiveElement(index);
				sync1.data("zoomEnable", true);

			}
		});
	}

	if (sync2.length) {
		sync2.find('.item:first').addClass('synced');
		sync2.on("click", ".item", function (e) {
			var index = $(this).index();
			setActiveElement(index);
			sync1.trigger("to.owl.carousel", [index, 500, true]);
		});
	}
	accessibilitySupport();

}

wiley.startZoom = function (e) {
	$(e).zoom({
		url: $(e).find("img.lazyOwl").data("zoomImage"),
		touch: true,
		on: 'grab',
		duration: 300,
		magnify: 0.8
	});
};

wiley.showDatePicker = function () {
	var datepicker = $(".date-picker");
	var datepickerIcon;

	if (wiley.isMobileDevice()) {
		if (datepicker.length) {
			datepicker.each(function () {

				$(this).attr("type", "date");
				datepickerIcon = $(this).parent().find("label");

				datepickerIcon.unbind("click.datepicker");
				$(this).unbind("change.datepicker");
				$(this).datepicker("destroy");

				$(this).bind("blur.inputDate", function () {

					var dateValue = $(this).val();
					var newDate = new Date(dateValue);
					var formattedDate = newDate.getMonth() + 1 + "/" + newDate.getDate() + "/" + newDate.getFullYear();
					$(".date-desired").val(formattedDate);
				});
			});
		}

	} else {
		if (datepicker.length) {
			var dateToday = new Date();

			datepicker.each(function () {
				$(this).attr("type", "text");
				$(this).unbind("blur.inputDate");

				datepickerIcon = $(this).parent().find("label");

				$(this).datepicker({
					minDate: dateToday,
					showOtherMonths: true,
					selectOtherMonths: true
				});

				// toggle show/hide date picker by click icon
				datepickerIcon.bind("click.datepicker", function (event) {
					event.preventDefault();

					return $(this).datepicker("widget").is(":visible") ? $(this).off("focus").datepicker("hide") : $(this).on("focus").datepicker("show");
				});

				/* Add mask on keyboard changes */
				var maskBlock = $("<span></span>").text("MM/dd/yyyy").addClass("date-mask-string");
				var maskParentBlock = $(this).parent();
				var maskString = maskBlock.text();

				var maskChars = maskString.split("");
				maskBlock.empty();

				$.each(maskChars, function (i) {
					var span = document.createElement("span");
					span.innerText = maskChars[i];
					maskBlock.append(span);
				});

				function displayMask() {
					var countValues = datepicker.val().length;
					maskBlock.find("span").css("opacity", 1);
					maskBlock.find("span:lt(" + countValues + ")").css("opacity", 0);
				}

				function removeMask() {
					maskParentBlock.find(".date-mask-string").remove();
				}

				$(this).on("keyup", function () {
					if (maskParentBlock.children(".date-mask-string").length > 0) {
						displayMask();
					} else {
						maskParentBlock.append(maskBlock);
						displayMask();
					}
				});

				$(this).bind("change.datepicker", function (event) {
					removeMask();

					var datePickerValue = $(this).val();
					$(".date-desired").val(datePickerValue);

				});
				/* Add mask on keyboard changes */
			});
		}
	}
};

wiley.destroyLaptopCustomSelect = function () {
	var selects = $(".select-component");
	var select, selectComponent, uiSelectMenuButton;

	if (selects.length) {
		selects.each(function () {
			selectComponent = $(this);
			uiSelectMenuButton = selectComponent.find(".ui-selectmenu-button");

			if (uiSelectMenuButton.length) {
				selectComponent.find("select").selectmenu("destroy");
				wiley.displayCustomMobileSelect(selectComponent);
			}
		});
	}
};

wiley.destroyMobileCustomSelect = function () {
	var selects = $(".select-component");
	var em, select, selectComponent;

	if (selects.length) {
		selects.each(function () {
			selectComponent = $(this);
			em = selectComponent.find("em");

			if (em.length) {
				em.remove();
				selects.removeClass("device");
				wiley.displayCustomSelect(selectComponent);
			}
		});
	}
};

wiley.displayCustomMobileSelect = function (selectComponent) {
	var select = selectComponent.find("select");
	var em = selectComponent.find("em");
	var optionSelected;

	selectComponent.addClass("device");

	if (!em.length) {
		em = $("<em></em>");
		selectComponent.prepend(em);
	}

	optionSelected = select.find("option:selected");

	optionSelected.each(function () {
		var selectValue = $(this).text();
		var selectEm = $(this).closest(selectComponent).children("em");
		selectEm.text(selectValue);
	});
};

wiley.displayCustomSelect = function () {
	var selectComponent = $(".select-component");
	var select = selectComponent.find("select");

	if (wiley.isMobileDevice()) {

		wiley.displayCustomMobileSelect(selectComponent);

		selectComponent.on("change", "select", function () {
			wiley.displayCustomMobileSelect(selectComponent);
		});

		selectComponent.on("touchstart", "select", function () {
			$(this).closest(".select-component").addClass("select-open");
		});

		selectComponent.on("blur", "select", function () {
			$(this).closest(".select-component").removeClass("select-open");
		});

	} else {

		function handler() {
			select.selectmenu('close');
			$(window).off('scroll resize', handler);
		};

		select.selectmenu({
			width: 'inherit',
			open: function (event, ui) {
				var element = $(event.target);
				$(window).on('scroll resize', handler);
				element.closest('.select-component').addClass("select-open");
				$('.ps-scrollbar-container').perfectScrollbar("update"); // visible perfectScrollbar by click on menu button
			},

			close: function (event) {
				var element = $(event.target);
				element.closest('.select-component').removeClass("select-open");
			}
		}).selectmenu("menuWidget").addClass("ps-scrollbar-container always-visible").perfectScrollbar();
	}
};


wiley.collapsePurchaseOption = function () {
	var purchaseAccordion = $("#purchase-accordion");

	if (purchaseAccordion.length) {
		var purchaseAccordionItems = purchaseAccordion.find(".purchase-accordion-heading");
		purchaseAccordionItems.each(function () {
			$(this).on("click", ".heading-center, .heading-left", function (e) {
				$(this).closest(".purchase-accordion-heading").toggleClass("current");
			});
		});
	}

	$(document).on("keydown", ".heading-center", function (event) {
		var keycode = ( event.keyCode || event.which);
		if (keycode == '13') {
			$(this).trigger('click');
		}
	});
};

wiley.isMobileDevice = function () {
	return wiley.isMobileView() || wiley.checkDevice.isAny();
};

wiley.triggerPopoverArrow = function () {
	var popoverId = $(this).attr('aria-describedby');
	var arrow = $(this).parent().find('.arrow');

	if ($('#' + popoverId).length) {
		arrow.show();
	} else {
		arrow.hide();
	}
};

wiley.initPopover = function () {
	var popoverElements = $('[data-toggle="popover"]');
	var arrow, popoverWrap;

	if (popoverElements.length) {
		popoverElements.each(function () {
			$(this).popover({
				placement: "top",
				html: "true",
				container: ".purchase-options",
				animation: false
			});

			if (!$(this).parent('.popover-wrap').length) {
				popoverWrap = $("<span></span>").addClass("popover-wrap");
				$(this).wrap(popoverWrap);
				arrow = $("<span></span>").addClass("arrow");
				$(this).parent().prepend(arrow);
			}

			$(this).on('mouseenter mouseleave focusin focusout', wiley.triggerPopoverArrow);
		});
	}

	// popover on mobile view on PDP

	$(document).on('click', '.type-of-book-chooser .icon-info, .type-of-book-chooser .active', function (e) {
		e.stopPropagation();
		e.preventDefault();
	});

};

wiley.destroyPopover = function () {
	var popoverElements = $('[data-toggle="popover"]');

	if (popoverElements.length) {
		popoverElements.each(function () {
			$(this).popover('destroy');
			$(this).parent().find('.arrow').hide();
			$(this).off('mouseenter mouseleave focusin focusout', wiley.triggerPopoverArrow);
		});
	}
};

wiley.updateCheckboxes = function () {
	$.each($("input[type=checkbox]"), function (index, value) {
		$(value).prop("checked", $(value).prop('defaultChecked'));
	});
};

wiley.setStickyHeader = function setStickyHeader() {
	$('.main-sticky-header').affix({
		offset: {top: 0}
	});
};

wiley.imageChecker = function () {
	var errorImg = $(".product-list-image img");
	errorImg.on("error", function () {
		$(this).attr("src", "~/../responsive/common/images/no-image.png");
	});
};

wiley.setCustomVH = function () {
	var vh = window.innerHeight * 0.01;
// Then we set the value in the --vh custom property to the root of the document
	document.documentElement.style.setProperty('--vh', vh + 'px');
};

wiley.initMainSearch = function initMainSearch() {
	var $mainHeader = $('#main-header-container');
	var $navToggle = $('.navbar-toggle');
	var $search = $('.main-navigation-search');
	var $input = $search.find('input');
	var searchParams = window.location.search;
	var searchQueryParamKey = "pq=";
	var contentSearchQueryParamKey = "cq=";
	var isSearchResultPage = searchParams.indexOf(searchQueryParamKey) !== -1 || searchParams.indexOf(contentSearchQueryParamKey) !== -1;

	function toggleSearch($icon) {
		$search.toggleClass('open');
		$icon.toggleClass('active');
		if ($search.hasClass('open')) {
			// keep search result value on serach result page and clear on others
			if (!isSearchResultPage) {
				$input.val('');
			}
		}
	};


	$mainHeader.on("click", ".main-navigation-search-icon", function () {
		if (wiley.isMobileView()) {
			if (!$navToggle.hasClass('collapsed')) {
				$navToggle.trigger('click');
			}
			toggleSearch($(this));
		}
	});
	$input.focus(function () {
		$mainHeader.addClass('focus');
	});
	$input.blur(function () {
		$mainHeader.removeClass('focus');
	});

};

wiley.setMainNavigation = function () {
	var toggleBtnEl = $('<span class="toggle-dropdown collapsed"></span>');
	var mainHeader = $('#main-header-container');
	var navItemsContainer = $('.navigation-menu-items');
	var mainNav = $('.main-header-navigation');
	var navItems = navItemsContainer.find('>li');
	var mainNavbar = $('#main-header-navbar');
	var $countryDropdown = $('.select-country-dropdown');
	var $dropDownMenu = $('.dropdown-menu');
	var $navbarToggle = $('.navbar-toggle');
	var $searchBar = $('.main-navigation-search');
	var submenuClass = 'dropdown-submenu';
	var threeLevelsClass = 'three-levels';
	var hoverClass = 'hover';
	var collapseClass = 'collapsed';
	var delayMobileMenu = 0;
	var openMenuClass = 'in';
	var setTimeoutOpen, setTimeoutClose, setTimeoutOpenInner, setTimeoutCloseInner;
	var delayIn = wiley.delayIn, delayOut = wiley.delayOut;
	var scrollConfig = wiley.checkDevice.isAny() ? ['touch'] : ['click-rail', 'drag-scrollbar', 'wheel'];
	var initialized = navItemsContainer.hasClass('initialized');

	function setEventOnDevice() {
		if (!wiley.isMobileView() && wiley.checkDevice.isDeviceIOS()) {
			return 'click touchend';
		} else {
			return 'click';
		}
	}

	function setMobileTitle(name, list) {
		var title = $('<span class="dropdown-item-title"></span>');
		title.text(name);
		title.prependTo(list);
	}

	function setBackBtn(list) {
		var backBtn = $('<span class="back-btn visible-xs-inline-block icon-arrow-left"></span>');
		backBtn.prependTo(list);
	}

	function resetDropdown(dropdown, openMenuClass) {
		dropdown.removeClass(openMenuClass);
		dropdown.find('li.hover').removeClass(hoverClass);
	}

	function toggleSearch() {
		$searchBar.toggleClass('open');
		$('.main-navigation-search-icon').toggleClass('active');
	}


	function bindCloseMenu() {
		mainHeader.on("click", ".navbar-toggle", function () {
			var self = $(this);
			if ($countryDropdown.hasClass('open')) {
				$('.trigger-country-dropdown').trigger('click');
			}
			if ($searchBar.hasClass('open')) {
				toggleSearch();
				delayMobileMenu = 200; // wait till input is closed
			} else{
				delayMobileMenu = 0;
			}
			setTimeout(function(){
				wiley.setCustomVH();
				var mainMenuOpenLinks = mainNavbar.find('ul>li.hover');
				var openBtns = mainNavbar.find('toggle-dropdown').not('.collapsed');
				openBtns.addClass(collapseClass);
				mainMenuOpenLinks.removeClass(hoverClass);
				mainNavbar.find('.dropdown-menu.in').removeClass(openMenuClass);
				self.toggleClass(collapseClass);
				mainNavbar.toggleClass(openMenuClass);
				$('body, html').toggleClass("has-drawer");
			}, delayMobileMenu);


		});
	}

	function redirectTo(newLocation, target) {
		window.open(window.location.origin + newLocation, target || '_self');
	}

	function toggleSubMenu(li) {
		if (li.hasClass(threeLevelsClass)) {
			li.parent().find('>li.hover').removeClass(hoverClass);
			li.addClass(hoverClass);
		} else {
			if (li.hasClass(hoverClass)) {
				li.removeClass(hoverClass);
			} else {
				li.parent().find('>li.hover').removeClass(hoverClass);
				li.addClass(hoverClass);
			}
		}

	}

	// find the width of all columns
	function getLastLevelMenuWidth(innerList) {
		var subMenuColumns = $(innerList).find('.sub-list');
		var subMenuColumnsLength = subMenuColumns.length;
		var subMenuWidth = subMenuColumns[0].scrollWidth;
		return subMenuColumnsLength * subMenuWidth;
	}

	// find the height of the highest column
	function getLastLevelMenuHeight(innerList) {
		var subMenuColumns = $(innerList).find('.sub-list');
		var subMenuColumnsLength = subMenuColumns.length;
		var firstColumnHeight = subMenuColumns[0].scrollHeight;
		return (subMenuColumnsLength > 1 ? Math.max(firstColumnHeight, subMenuColumns[1].scrollHeight) : firstColumnHeight) +10;
	}

	function containsClass(target, names) {
		var classNames = $.isArray(names) ? names : [names];
		return classNames.some(function (name) {
			return target.hasClass(name);
		});
	}

	function setCustomScroll($target) {
		$target.perfectScrollbar({suppressScrollX: true, handlers: scrollConfig});
		$target.scrollTop(0);
		$target.perfectScrollbar('update');
	}

	function getDropDownWidth(firstCol, secondCol) {
		return firstCol ? firstCol.scrollWidth + secondCol.scrollWidth : secondCol.scrollWidth;
	}

	function openMenu(menuItem) {
		var parent = menuItem.parent();
		var dropdown = menuItem.find('>.dropdown-menu');
		var subMenu = dropdown.find('>.dropdown-items');
		var link = menuItem.find('a');
		//reset all hover classes
		parent.find('li.hover').removeClass(hoverClass);

		// reset height of the dropdown menu
		dropdown.height('auto');
		dropdown.width('auto');

		menuItem.addClass(hoverClass);
		setCustomScroll(subMenu);

	}

	function getDropDownHeight(firstColumn, parentColumn) {
		return firstColumn ? parentColumn.scrollHeight : 'auto';
	}

	function closeSubMenu(menuItem) {
		var dropdown = menuItem.parents('.dropdown-menu');
		var parents = menuItem.parents('.dropdown-items');
		var firstColumn = parents[1];// if 3 levels of menu
		var parentColumn = parents[0];
		dropdown.height(getDropDownHeight(firstColumn, parentColumn));
		dropdown.width(getDropDownWidth(firstColumn, parentColumn));
		menuItem.parent().find('li.hover').removeClass(hoverClass);
	}

	function openSubMenu(menuItem) {
		toggleSubMenu(menuItem);
		var dropdown = menuItem.parents('.dropdown-menu');
		var parents = menuItem.parents('.dropdown-items');
		var firstColumn = parents[1];// if 3 levels of menu
		var parentColumn = parents[0];
		dropdown.height(getDropDownHeight(firstColumn, parentColumn));
		var innerList = menuItem.find('.dropdown-items')[0];
		var innerWidth = menuItem.hasClass(threeLevelsClass) ? innerList.scrollWidth : getLastLevelMenuWidth(innerList);
		var innerHeight = menuItem.hasClass(threeLevelsClass) ? innerList.scrollHeight : getLastLevelMenuHeight(innerList);
		if (innerHeight > dropdown.height()) {
			dropdown.height(innerHeight);
		}
		var dropDownWidth = getDropDownWidth(firstColumn, parentColumn);
		dropdown.width(dropDownWidth + innerWidth);
		setCustomScroll(menuItem.find('ul').not('.sub-list'));
	}

	function isTopLevelLink(target) {
		return target.className.indexOf('collapsed') !== -1;
	}

	function isClickOutsideSelector($target) {
		return !$target.parents('.navigation-menu-items').length;
	}

	function accessibilitySupport() {
		$(document).on("click keyup", function (e) {
			if (!wiley.isMobileView()) {
				var keyCode = (e.keyCode || e.which);
				var $target = $(e.target);
				if (isClickOutsideSelector($target)) {
					var openedItem = navItemsContainer.find('>.dropdown-submenu.hover');
					if (openedItem.length) {
						openedItem.trigger('mouseleave');
					}
				}
			}
		});
		navItemsContainer
		/* For Accessibility tab navigation */
			.on('keydown', function (e) {
				if (!wiley.isMobileView()) {
					var target = e.target;
					var targetParent = $(target).parent();
					var keyCode = ( e.keyCode || e.which);
					if (keyCode === wiley.KEY_CODE_MAP.escape) {
						$(this).find('li').removeClass(hoverClass);
					}
					if (target.localName === 'a') {
						if (isTopLevelLink(target)) {
							if (keyCode === wiley.KEY_CODE_MAP.space) {
								if (targetParent.hasClass('hover')) {
									targetParent.trigger('mouseleave');
								} else {
									openMenu(targetParent);
								}
								e.preventDefault();
							}
						}
						if (!isTopLevelLink(target)) {
							if (e.shiftKey && keyCode === wiley.KEY_CODE_MAP.tab) {
								var openItem = targetParent.parent().find('.hover').not(targetParent);
								if (openItem.length) {
									openItem.removeClass(hoverClass);
									closeSubMenu(openItem);
								}
							} else if (keyCode === wiley.KEY_CODE_MAP.tab) {
								if (targetParent.hasClass('dropdown-submenu')) {
									if (!targetParent.hasClass('hover')) {
										if (targetParent.next('.dropdown-item').length || targetParent.prev('.dropdown-item').length) {
											targetParent.next().find('>a').focus();
											e.preventDefault();
										}
									}
								}
							}
							if (keyCode === wiley.KEY_CODE_MAP.space) {
								if (targetParent.hasClass('dropdown-submenu')) {
									if (targetParent.hasClass('hover')) {
										closeSubMenu(targetParent);
									} else {
										openSubMenu(targetParent);
									}
								}
								e.preventDefault();
							}
						}

					}
				}
			})
			.on('keyup', function (e) {
				if (!wiley.isMobileView()) {
					var target = e.target;
					var targetParent = $(target).parent();
					var keyCode = ( e.keyCode || e.which);
					var openedItem;
					if (target.localName === 'a') {
						if (e.shiftKey && keyCode === wiley.KEY_CODE_MAP.tab) {
							if (isTopLevelLink(target)) {
								openedItem = targetParent.find('.dropdown-menu>ul>li.hover').not(targetParent);
								if (openedItem.length) {
									closeSubMenu(openedItem);
								}
							}
							if (!isTopLevelLink(target)) {
								openedItem = targetParent.parent().find('li.hover');
								if (openedItem.length) {
									closeSubMenu(openedItem);
								}
							}
						}
						else if (keyCode === wiley.KEY_CODE_MAP.tab) {
							if (isTopLevelLink(target)) {
								targetParent.parent().find('li.hover').not(targetParent).removeClass('hover');
							}
							if (!isTopLevelLink(target)) {
								openedItem = targetParent.parent().find('li.hover').not(targetParent);
								if (openedItem.length) {
									closeSubMenu(openedItem);
								}
							}
						}
					}
				}
			});

	}

	function handleKeysAndMouseEvents() {
		navItemsContainer

		/* First level menu desktop */
			.on('mouseenter', '>.dropdown-submenu', function (e) {
				if (!wiley.isMobileView()) {
					var menuItem = $(this);
					var link = menuItem.find('>a');
					setTimeoutOpen = setTimeout(function () {
						openMenu(menuItem);
						// remove focus from other inputs
						if (wiley.checkDevice.isIE()) {
							link.focus();
						}
					}, delayIn);
				}
			})
			.on('mouseleave', '>.dropdown-submenu', function (e) {
				if (!wiley.isMobileView()) {
					clearTimeout(setTimeoutOpen);
					var self = $(this);
					setTimeoutClose = setTimeout(function () {
						resetDropdown(self, hoverClass);
					}, delayOut);
				}
			})

			/* Second and third level menu desktop */
			.on('mouseenter', '>li .dropdown-submenu:not(.hover)', function (e) {
				if (!wiley.isMobileView()) {
					var menuItem = $(this);
					clearTimeout(setTimeoutCloseInner);
					setTimeoutOpenInner = setTimeout(function () {
						openSubMenu(menuItem);
					}, delayIn);

				}
			})
			.on('mouseleave', '>li .dropdown-submenu', function (e) {
				if (!wiley.isMobileView()) {
					clearTimeout(setTimeoutOpenInner);
					if (!containsClass($(e.relatedTarget), ['dropdown-items-wrapper', 'ps-scrollbar-y', 'ps-scrollbar-y-rail'])) {
						var menuItem = $(this);
						setTimeoutCloseInner = setTimeout(function () {
							closeSubMenu(menuItem);
						}, delayOut);
					}
				}
			})
			/* Mobile */
			.on('click', '>.dropdown-submenu', function (e) {
				if (wiley.isMobileView()) {
					var menuItem = $(this);
					var dropdown = menuItem.find('>.dropdown-menu');
					if ($(e.target).hasClass('back-btn')) {
						if (menuItem.parent().hasClass('navigation-menu-items')) {
							resetDropdown(dropdown, openMenuClass);
						}
					} else {
						menuItem.addClass(hoverClass);
						dropdown.addClass(openMenuClass);
					}
					e.stopPropagation();
					e.preventDefault();
				}
			})
			.on('click', '>.dropdown-submenu .dropdown-submenu', function (e) {
				if (wiley.isMobileView()) {
					if ($(e.target).hasClass('back-btn')) {
						resetDropdown($(this), hoverClass);
					} else {
						toggleSubMenu($(this));
					}
					e.stopPropagation();
					e.preventDefault();
				}
			})

			/* Click on menu item without submenu with link inside should trigger the link */
			.on(setEventOnDevice(), 'li:not(.dropdown-submenu)', function (e) {
				var link = $(this).find('a');
				if (link.length && link.attr('href').length > 1) {
					var target = link.attr('target');
					e.stopPropagation();
					redirectTo(link.attr('href'), target);

				}
			})
			/* Click on link should prevent propagation if the item menu has submenu */
			.on(setEventOnDevice(), 'li>a', function (e) {
				if ($(this).attr('href').length > 1) {
					e.stopPropagation();
				} else {
					if (wiley.isMobileDevice()) {
						var parent = $(this).parent();
						if (parent.hasClass('dropdown-submenu')) {
							parent.trigger('mouseenter');
						}
					}
					return false;
				}
			});

	}

	function splitColumn(container, maxHeightMenu) {
		var listItem = '.dropdown-item',
			listClass = 'sub-list';
		var items = container.find(listItem);
		var itemHeight = 34;
		var columnLength = Math.floor(maxHeightMenu / itemHeight);
		var num_cols = items.length > columnLength ? 2 : 1;
		var items_per_col = [],
			min_items_per_col = Math.floor(items.length / num_cols),
			difference = items.length - (min_items_per_col * num_cols);
		for (var i = 0; i < num_cols; i++) {
			if (i < difference) {
				items_per_col[i] = min_items_per_col + 1;
			} else {
				items_per_col[i] = min_items_per_col;
			}
		}
		for (var i = 0; i < num_cols; i++) {
			container.append($('<ul></ul>').addClass(listClass));
			for (var j = 0; j < items_per_col[i]; j++) {
				var pointer = 0;
				for (var k = 0; k < i; k++) {
					pointer += items_per_col[k];
				}
				container.find('.sub-list').last().append(items[j + pointer]);
			}
		}

	};

	function setLinkCursor(link) {
		if (link.attr('href').length <= 1) {
			link.css('cursor', 'default');
		}
	}

	function setLinkAttrTitle(link) {
		if (link.parent().hasClass(submenuClass)) {
			// Accessibility support for screen readers like NVDA
			link.attr('title', 'with submenu');
		}
	}

	if (!initialized) {

		$.each(navItems, function (index, item) {
			var listItem = $(this);
			var menu = listItem.find('>.dropdown-menu');
			if (menu.length) {
				var innerList = menu.find('>.dropdown-items');
				var innerListItems = innerList.find('>.dropdown-item');
				var maxHeightMenu = parseInt(menu.css('max-height'), 10) - 28; // minus padding
				listItem.addClass(submenuClass);
				var innerLink = listItem.find('>a');
				setLinkAttrTitle(innerLink);
				setMobileTitle(innerLink.text(), menu);
				setBackBtn(menu);
				innerListItems.each(function () {//first level in dropdown
					var link = $(this);
					var innerLink = link.find('>a');
					setLinkCursor(innerLink);
					var titleName = innerLink.text();
					var wrapper = link.find('>.dropdown-items-wrapper');
					if (wrapper.length && wrapper.find('>ul').length) {
						var secondLevelList = wrapper.find('>ul');
						var initializedSubMenu = false;
						link.addClass(submenuClass);
						setLinkAttrTitle(innerLink);
						toggleBtnEl.clone().appendTo(link);
						$.each(secondLevelList, function () {
							var secondLevelNode = $(this);
							var secondLevelLinks = secondLevelNode.find('>li');
							var thirdLevelList = secondLevelNode.find('li ul');
							if (!thirdLevelList.length) {
								splitColumn(secondLevelNode, maxHeightMenu);
							}
							$.each(secondLevelLinks, function () {
								var innerLink = $(this).find('>a');
								setLinkCursor(innerLink);
								setLinkAttrTitle(innerLink);
								var thirdLevelList = $(this).find('>ul');
								if (thirdLevelList.length) {
									$(this).addClass(submenuClass);
									link.addClass(threeLevelsClass);
									toggleBtnEl.clone().appendTo($(this));
									splitColumn(thirdLevelList, maxHeightMenu);
									if (!initializedSubMenu) {
										setMobileTitle(titleName, wrapper);
										setBackBtn(wrapper);
										initializedSubMenu = true;
									}
								}
							});
						});
					}
				});

			}
		});
		bindCloseMenu();
		handleKeysAndMouseEvents();
		accessibilitySupport();
		navItemsContainer.addClass('initialized');

		if (navItems.length > 3) {
			mainNav.addClass('extended');
		}

		if (!wiley.isMobileView() && wiley.checkDevice.isAny()) {
			navItemsContainer.addClass('tablet');
		} else {
			navItemsContainer.removeClass('tablet');
		}

	} else {
		if (wiley.isMobileView()) {
			navItemsContainer.perfectScrollbar('destroy');
			$('.dropdown-items').perfectScrollbar('destroy');
			$dropDownMenu.removeClass('ps-container ps-theme-default ps-active-y');
			$dropDownMenu.removeClass('in');
			$dropDownMenu.removeAttr('style');
		} else {
			if (!$navbarToggle.hasClass('collapsed')) {
				$navbarToggle.trigger('click');
			}
			navItemsContainer.perfectScrollbar('destroy');
		}
	}
};

wiley.disableAutoZoomIOS = function disableAutoAoomIOS() {
	var $objHead = $('head');
	var viewport = $("meta[name='viewport']");
	var defaultContentAttr = navigator.userAgent.match(/iPad/i) ? "width=1240, initial-scale=0, user-scalable=1" : "width=device-width, initial-scale=1.0, user-scalable=1";
	var newContentAttr = navigator.userAgent.match(/iPad/i) ? "width=1240, initial-scale=0, user-scalable=0" : "width=device-width, initial-scale=1.0, user-scalable=0";


	var zoomDisable = function () {
		$objHead.find('meta[name=viewport]')
			.attr("content", newContentAttr);
	};

	var zoomEnable = function () {
		$objHead.find('meta[name=viewport]')
			.attr("content", defaultContentAttr);
	};

	if (wiley.checkDevice.isDeviceIOS()) {

		$("input, select")
			.on({
				'touchstart': function () {
					zoomDisable();
				}
			})
			.on({
				'touchend': function () {
					setTimeout(zoomEnable, 500);
				}
			});
	}
};

wiley.setCustomScroll = function () {
	if (!wiley.isMobileView()) {
		$('.modalWindow:not("#facetsListWindow") .modal-body, .main-navigation-search-autocomplete, #facetsModalScrollArea').perfectScrollbar({
				suppressScrollX: true,
				handlers: ['click-rail', 'drag-scrollbar', 'wheel', 'touch']
			}
		);
	}

};
wiley.initCountrySelect = function () {
	var $component = $('.main-navigation-select-country');
	var $countryDropdown = $('.select-country-dropdown');
	var $listContainer = $countryDropdown.find('ul');
	var $countrySearchInput = $('.country-search-input');
	var $selectedCountryInput = $('#selected-country-input');
	var $countryForm = $('#country-form');
	var $countryTriggerBtn = $('.trigger-country-dropdown');
	var setTimeoutOpen, setTimeoutClose;
	var delayIn = wiley.delayIn;
	var delayOut = wiley.delayOut;
	var notFoundTxt = 'There are no countries matched';
	var countryList = ACC.wileyCountryList ? ACC.wileyCountryList : [];

	function fillDropdownList(countries) {
		if (countries.length) {
			$listContainer.html(createElements(countries));
		} else {
			$listContainer.html('<p class="not-found">' + notFoundTxt + '</p>');
		}
	}

	function createElements(countries) {
		return countries.map(function (i) {
			return '<li tabindex="0" data-country-iso-code="' + i.isocode + '">' + i.name + '</li>';
		});
	}

	function isCountryDropdownOpen() {
		return $countryDropdown.hasClass('open');
	}

	function isClickOutsideSelector($target) {
		return !$target.parents('.main-navigation-select-country').length;
	}

	function toggleCountrySelector(e) {
		var keyCode = (e.keyCode || e.which);

		/** Accessibility support. Key codes: escape-27, enter-13, space-32  **/

		if (e.type !== 'keydown' || keyCode === 32 || keyCode === 13 || keyCode === 27) {
			e.preventDefault();
			isCountryDropdownOpen() ? closeDropdown() : openDropdown();
		}
	}

	function closeDropdown() {
		var value = $countrySearchInput.val();
		if (value) {
			$countrySearchInput.val('');
			fillDropdownList(countryList);
		}
		$listContainer.find('li.selected').removeClass('selected');
		$countrySearchInput.blur();
		$countryDropdown.removeClass('open');
		$countryTriggerBtn.removeClass('open');
	}

	function openDropdown() {
		$countryDropdown.addClass('open');
		$countryTriggerBtn.addClass('open');
		if (!wiley.isMobileView()) {
			$countrySearchInput.focus();
			$listContainer.perfectScrollbar({suppressScrollX: true});
			$listContainer.scrollTop(0);
			$listContainer.perfectScrollbar('update');
		}
	}

	function initCountrySearch() {
		$countrySearchInput.on('keyup', function (e) {
			var value = this.value;
			var keyCode = (e.keyCode || e.which);
			if (keyCode !== wiley.KEY_CODE_MAP.tab) {
				fillDropdownList(value ? filterCountries(value) : countryList);
			}
		});
	}

	function checkMultipleWordCountries(countryName, value) {
		var nameArr = countryName.split(' ');
		if (nameArr.length > 1) {
			return nameArr.some(function (s) {
				return s.indexOf(value) === 0;
			});
		}
	}

	function filterCountries(value) {
		value = value.toLocaleLowerCase();
		return countryList.filter(function (i) {
			var countryName = i.name.toLocaleLowerCase();
			var isMatched = countryName.indexOf(value) === 0;
			return isMatched ? isMatched : checkMultipleWordCountries(countryName, value);
		});
	}

	function sendCountryForm(el) {
		$selectedCountryInput.val(el.data("country-iso-code"));
		$countryForm.submit();
		closeDropdown();
	}

	function handler(e) {
		var keyCode = (e.keyCode || e.which);
		var $target = $(e.target);
		if (keyCode === wiley.KEY_CODE_MAP.enter) {
			$(this).find('li:focus').trigger('click');
		}
	}

	function keyBinding() {
		$(document)
			.on('click', '.select-country-dropdown li', function () {
				sendCountryForm($(this));
			})
			.on('mouseenter', '.trigger-country-dropdown', function (e) {
				if (!wiley.isMobileView()) {
					clearTimeout(setTimeoutClose);
					setTimeoutOpen = setTimeout(function () {
						openDropdown(e);
					}, delayIn);
				}
			})
			.on('mouseleave', '.main-navigation-select-country', function (e) {
				clearTimeout(setTimeoutOpen);
				setTimeoutClose = setTimeout(function () {
					if (!$(e.relatedTarget).parents('.main-navigation-select-country').length) {
						closeDropdown(e);
					}
				}, delayOut);
			})
			.on('keydown click', '.trigger-country-dropdown', function (e) {
				toggleCountrySelector(e);
			})
			.on('click', '.country-selected.visible-xs-inline-block', function (e) {
				closeDropdown(e);
			})
			.on("click keyup", function (e) {
				var keyCode = (e.keyCode || e.which);
				var $target = $(e.target);
				/** Desktop accessibility support. Key codes: escape-27 **/
				if (!wiley.isMobileView() && ((isClickOutsideSelector($target) || keyCode === 27) && isCountryDropdownOpen())) {
					closeDropdown();
				}
			});


		$countryDropdown.on('keydown', handler);
	}

	function initCountrySelect() {
		fillDropdownList(countryList);
		initCountrySearch();
		keyBinding();
		$component.addClass('initialized');
	}

	initCountrySelect();


};

wiley.initSelectCountryModalWindow = function () {
	var modalWnd = $('#selectCountryModalWnd');
	var cookieName = modalWnd.data('cookie-country-cookie-name');
	var cookiePath = modalWnd.data('cookie-country-cookie-path');
	var cookieMaxAgeSeconds = modalWnd.data('cookie-country-cookie-max-age');
	var currentLocale = modalWnd.data('current-locale');
	var cookieLocale = modalWnd.data('detected-locale');

	if (cookieLocale && currentLocale !== cookieLocale) {
		modalWnd.find('.changeLocationConfirmBtn').on('click', function () {
			var cookieExpirationDate = new Date();
			cookieExpirationDate.setSeconds(cookieExpirationDate.getSeconds() + cookieMaxAgeSeconds);
			$.cookie(cookieName, currentLocale, {'path': cookiePath, 'expires': cookieExpirationDate});
			modalWnd.modal('hide');
		});
		modalWnd.modal('show');
	}
};


wiley.setProductPopover = function () {
	var delayIn = wiley.delayIn;
	var delayOut = wiley.delayOut;

	function showPopover(icon, container, placements) {
		icon.popover("show");
		var $popover = $(container).find('.popover.in');
		var $popoverContent = $(container).find('.popover-content');
		var $arrow = $popover.find('.arrow');
		var iconPosition = icon.offset();
		var iconWidth = icon.width();
		var popoverLeftPosition = iconPosition.left;
		var popoverTopPosition = iconPosition.top;
		var popoverWidth = 440;
		var arrowPosition = iconPosition.left;

		if (wiley.isMobileView()) {
			$popover.hide();
			$arrow.css('left', arrowPosition);
			setTimeout(function () {
				$popover.show();
				$popover.addClass("popover-shadow");
			}, 0);
		} else if (!wiley.isMobileView()) {
			if (placements.desktop === 'bottom-right') {
				popoverLeftPosition = iconPosition.left - popoverWidth - 25;
				popoverTopPosition = iconPosition.top;
			}

			$popover.offset({'left': popoverLeftPosition + 20}).offset({'top': popoverTopPosition});

			if (icon.parents('.modal').length) {
				popoverWidth = 400;
				popoverLeftPosition = iconPosition.left - popoverWidth + iconWidth;
				popoverTopPosition = iconPosition.top - $popover.height() - 10;
				$popover.offset({'left': popoverLeftPosition}).offset({'top': popoverTopPosition});
			}
		}
		$popover.on('click', '[data-dismiss="popover"]', function (e) {
			setTimeout(function () {
				$(icon).popover('hide');
			}, delayOut);
		});
	}

	if (wiley.isMobileView()) {
		$(window).on('touchmove click', function (e) {
			if ($(e.target).hasClass("popover-shadow")) {
				$('.icon-info').popover('hide');
			}
		});
	}


	$('.icon-info').each(function (index, icon) {
		var container = $(this).data('container') || 'body';
		var containerClass = $(this).data('container-class') || '';
		var placement = $(this).data('placement') || 'bottom';
		var placementsArr = placement.split(' ');
		var placements = {
			desktop: placementsArr[0],
			mobile: placementsArr.length > 1 ? placementsArr[1] : placementsArr[0]
		};
		var setTimeoutConst;

		$(icon).popover({
			placement: placement,
			html: "true",
			container: container,
			animation: false,
			template: '<div class="popover ' + containerClass + '"><div class="arrow"></div><div class="popover-content"></div><button type="button" data-dismiss="popover" aria-label="Close" class="close"></button></div>',
			trigger: "manual"
		}).on("mouseenter", function () {
			var _this = this;
			setTimeoutConst = setTimeout(function () {
				showPopover($(_this), container, placements);
				$(".popover").on("mouseleave", function () {
					setTimeout(function () {
						$(icon).popover('hide');
					}, delayOut);
				});
			}, delayIn);

		}).on("mouseleave touchleave", function () {
			var _this = this;
			clearTimeout(setTimeoutConst);
			setTimeout(function () {
				if (!$(".popover.in:hover").length) {
					$(_this).popover("hide");
				}
			}, delayOut);
		}).on("keydown", function (e) {
			var _this = this;
			if (e.keyCode === wiley.KEY_CODE_MAP.enter) {
				if ($('.popover').hasClass('in')) {
					setTimeout(function () {
						$(this).popover("hide");
					}, delayOut);
				} else {
					showPopover($(this), container, placements);
				}
			} else if (e.keyCode === wiley.KEY_CODE_MAP.escape || e.keyCode === wiley.KEY_CODE_MAP.tab) {
				$(this).popover("hide");
			} else if (e.keyCode === wiley.KEY_CODE_MAP.keydown) {
				var focusableElements = $('.popover.in').find('select, input, textarea, button, a[href]').filter(':visible').not(':disabled');
				if (focusableElements.length) {
					focusableElements.first().focus();
				}
			}

			$(".popover.in").on("keydown", function (e) {
				if (e.keyCode === wiley.KEY_CODE_MAP.tab) {
					$(_this).popover('hide');
					$(_this).focus();
				}
			});
		});
	});

	if (wiley.checkDevice.isDeviceIOS()) {
		$('body').css({'cursor': 'pointer'})
	}
};


wiley.fluidContainerBackground = function () {
	var list = $(".fluid-container-image .container-image");
	var url;

	for (var i = 0; i < list.length; i++) {
		url = wiley.isMobileView() ? list[i].getAttribute('data-image-mobile') : list[i].getAttribute('data-image');
		if (url) {
			list[i].style.backgroundImage = "url('" + encodeURI(url) + "')";
		}
	}
};

wiley.constructBaseUrl = function () {
	var baseUrl = window.location.origin + window.location.pathname;
	if (!baseUrl.match(/\/$/)) {
		baseUrl = baseUrl + '/';
	}
	return baseUrl;
}

wiley.constructAllFacetValuesUrl = function (facetCode) {
	var baseUrl = wiley.constructBaseUrl();
	var requestParams = window.location.search;
	return baseUrl + 'loadMoreFacetValues' + requestParams + (requestParams ? "&" : "?") + "facetCode=" + facetCode;
}

wiley.initFacetPanel = function () {
	var facetPanel = $('.facets-panel-block');

	if (!facetPanel.length) {
		return;
	}

	if (wiley.isMobileView()) {
		wiley.setFacetsViewMoreMobile();
	} else {
		wiley.setFacetsViewMoreDesktop();
	}

	var facetPanelWrapper = $('.facets-panel-wrapper');
	var facetButton = $('.facets-panel-button');
	var facetesBlockTitle = $('.facets-panel-block-title');
	var facetesExpanderWrapper = $('.facets-expander');
	var facetesExpanderOn = $('.expand-facets');
	var facetesExpanderOff = $('.collapse-facets');
	var facetesTopPosition;
	var checkedElements = $('.facets-panel-list .js-facet-checkbox:checked');
	var productContainer = $('.products-list');
	var searchResult = $('.search-result-header');
	var mainHeaderContainer = $('#main-header-container');
	var mainHeader = $('.main-sticky-header');
	var searchPanel = $('.main-navigation-search');

	function togglePanel() {
		if (facetPanelWrapper.hasClass('open')) {
			facetPanelWrapper.find('.facets-panel-block-title.open').trigger('click')
		}
		facetPanelWrapper.toggleClass('open');
		$('body, html').toggleClass("has-drawer");

	}

	function closeAllFacets($this) {
		$this.parent().removeClass('active');
		$('.facets-panel-block-inner:visible').slideUp().siblings(facetesBlockTitle).removeClass('active');
		facetesExpanderOff.attr('tabindex', '-1');
		facetesExpanderOn.attr('tabindex', '0');
	}

	function openAllFacets($this) {
		$this.parent().addClass('active');
		$('.facets-panel-block-inner:hidden').slideDown().siblings(facetesBlockTitle).addClass('active');
		facetesExpanderOn.attr('tabindex', '-1');
		facetesExpanderOff.attr('tabindex', '0');
	}

	function bindHandlers() {
		facetesBlockTitle.on('click keyup keydown', function (e) {
			var keyCode = (e.keyCode || e.which);
			if (e.type === 'click' || (e.type === 'keydown' && (keyCode === 13 || keyCode === 32))) {
				e.preventDefault();
				if ($('.facets-panel-block-inner:hidden').length) {
					facetesExpanderWrapper.addClass('active');
				}

				$(this).siblings('.facets-panel-block-inner').slideToggle(500, 'swing');
				$(this).toggleClass('active');

				setTimeout(function () {
					if (!$('.facets-panel-block-inner:visible').length) {
						facetesExpanderWrapper.removeClass('active');
					}
				}, 600);
			}
			if (e.type === 'keydown' && keyCode === 9) {
				$(this).removeClass('focused');
			}
			if (e.type === 'keyup' && keyCode === 9) {
				$(this).addClass('focused');
			}
		});

		facetesExpanderOn.on('click keydown', function (e) {
			var keyCode = (e.keyCode || e.which);

			/** Accessibility support. Key codes: enter-13, space-32  **/
			if (e.type !== 'keydown' || keyCode === 13) {
				e.preventDefault();
				if ($(this).parent().hasClass('active')) {
					closeAllFacets($(this));
				} else {
					openAllFacets($(this));
				}

			}
		});

		facetesExpanderOff.on('click keydown', function (e) {
			var keyCode = (e.keyCode || e.which);
			/** Accessibility support. Key codes: enter-13  **/
			if (e.type !== 'keydown' || keyCode === 13) {
				e.preventDefault();
				closeAllFacets($(this))
			}
		});
	}

	function setPosition() {
		/* fix for the facets panel position */
		mainHeaderContainer.css({
			"height": mainHeader.height(),
			"margin-bottom": searchPanel.height()
		});
		if (searchResult.length) {
			facetesTopPosition = searchResult.outerHeight() + 70;
			facetPanelWrapper.css('top', '-' + facetesTopPosition + 'px');
		}
	}

	function openActiveSection() {
		if (checkedElements.length) {
			checkedElements.closest('.facets-panel-block-inner').slideDown();
			checkedElements.closest('.facets-panel-item').addClass('active');
		}
	}

	if (!wiley.isMobileView()) {
		$('body').addClass('page-wth-search-filters');
		setPosition();
		bindHandlers();
		openActiveSection();
	}

	facetButton.detach().insertBefore(productContainer);
	facetPanelWrapper.find('.close-icon').on('click', togglePanel);
	facetPanelWrapper.find('.button-teal').on('click', togglePanel);
	facetButton.on('click', togglePanel);

	$(document).on("click", ".facets-panel-list .js-facet-checkbox", function () {
		ACC.facetsearch.submitCheckboxForm(this);
	});

	$(document).on('click', '#facetsListWindow button[type="reset"]', function () {
		ACC.facetsearch.modal.clearAllValues();
	});

}

wiley.facets = {
	data: {},
	maxItems: 0,
	currentFacetCode: null,
	setModalTitle: function (title) {
		title = title.toLowerCase();
		if (title === 'price range') {
			return 'prices';
		}
		if (!title.match(/s$/)) {
			title = title + 's';
		}
		return title;
	},
	setPlaceholder: function (input, title) {
		title = title.toLowerCase();
		var placeholderMap = {
			'subject': 'subject name',
			'series': 'series name',
			'brands': 'brand name',
			'course': 'course name',
			'authors': 'author',
			'price range': 'price',
			'language': 'language',
			'types': 'type'
		};
		var placeholderName = placeholderMap[title] || title;
		var placeholder = 'Search ' + placeholderName;
		input.attr('placeholder', placeholder);
	},
	isSingleSelect: function (el) {
		return $(el).find('input[type=checkbox]').length === 0;
	},
	htmlTemplates: {
		singleSelect: '<a href="{{link}}" class=" facets-panel-item" title="{{{value}}}"><b class="facet-text">{{{highlightedValue}}}</b><i>{{count}}</i></a>',
		multiSelect: '<div class="facets-panel-item" data-pq="{{pq}}">' +
		'<label class="custom-checkbox">' +
		'<input type="checkbox" class="facet-checkbox js-facet-checkbox" {{#selected}}checked="checked"{{/selected}}/>' +
		'<span class="facet-label"><span class="facet-text" title="{{{value}}}">{{{highlightedValue}}}</span><i>{{count}}</i></label></div>'
	},
	getFacetsRows: function (data, template, searchValue) {
		for (var i = 0, rows = []; i < data.length; i += 3) {
			try {
				var rowString = '';
				var itemsInRow = [data[i], data[i + 1], data[i + 2]];
				itemsInRow.forEach(function (item) {
					if (item) {
						var highlightedValue = item.value;
						if (searchValue) {
							var theRegEx = new RegExp("(" + searchValue + ")", "igm");
							highlightedValue = item.value.replace(theRegEx, "<em class='highlighted'>$1</em>");
						}
						item.highlightedValue = highlightedValue;
						var itemStr = Mustache.render(template, item);
						rowString = rowString + itemStr;
					}
				});
				rows.push('<div class="values-row">' + rowString + '</div>');
			} catch (err) {
				console.log(err)
				rows = [];
			}
		}
		return rows;
	},
	getSelectedFacets: function (facetCode) {
		if (!facetCode) {
			facetCode = wiley.facets.currentFacetCode;
		}
		return wiley.facets.getCachedData(facetCode).filter(function (item) {
			return item.selected
		});
	},
	loadFacets: function (facetCode, async) {
		return $.ajax({
			url: wiley.constructAllFacetValuesUrl(facetCode),
			type: "get",
			cache: true,
			async: async,
			error: function (error) {
				console.log(error)
			}
		});
	},
	getCachedData: function (facetCode) {
		return wiley.facets.data[facetCode] || [];
	},
	hideModalFooterIfSingleSelect: function (modal, isSingleSelectFacet) {
		var modalFooter = modal.find('.modal-footer');
		isSingleSelectFacet ? modalFooter.hide() : modalFooter.css({'display': 'flex'});
	},
	bindModalWindowSearch: function (searchInput, response, template, clusterize) {
		searchInput.on('keydown', function (e) {
			if (e.which === 13) {
				e.preventDefault();
				e.stopPropagation();
				return false;
			}
		});

		searchInput.on('keyup', function (e) {
			if (e.which === (91 || 8 || 83 || 38 || 65 )) {
				return;
			}
			var value = searchInput.val();
			if (value) {
				try {
					var regex = new RegExp(value, "i");
					var filteredItems = response.filter(function (item) {
						return item.value.search(regex) >= 0
					});
					clusterize.update(wiley.facets.getFacetsRows(filteredItems, template, value));
					clusterize.refresh(true)
				} catch (err) {
					clusterize.clear();
				}

			} else {
				clusterize.update(wiley.facets.getFacetsRows(response, template));
				clusterize.refresh(true)
			}
		})
	},
};

wiley.setFacetsViewMoreDesktop = function () {
	var facetPanel = $('.facets-panel');
	var singleSelectTemplate = wiley.facets.htmlTemplates.singleSelect;
	var multiSelectTemplate = wiley.facets.htmlTemplates.multiSelect;

	var clusterize;
	var modalWindow = $('#facetsListWindow');

	$('.facets-panel-search').removeClass('show');
	$(facetPanel).off('click');
	$('.js-all-facet-values-link').off('click');

	$(facetPanel).on('click keydown', '.js-all-facet-values-link', function (e) {
		var keyCode = (e.keyCode || e.which);
		/** Accessibility support. Key codes: enter-13  **/
		if (e.type !== 'keydown' || keyCode === 13) {
			e.preventDefault();
			var viewMoreLink = $(this);
			var facet = viewMoreLink.parents('.js-facet');
			var facetCode = facet.data('facet-code');
			var title = facet.find('.facets-panel-block-title').text();
			var isSingleSelectFacet = wiley.facets.isSingleSelect(facet);
			var response;
			var modalTitle = modalWindow.find('.modal-title');
			var searchInput = modalWindow.find('.facets-panel-search').find('input');
			var contentArea = $('#facetsModalContentArea');
			var scrollArea = modalWindow.find('#facetsModalScrollArea');
			var template = isSingleSelectFacet ? singleSelectTemplate : multiSelectTemplate;
			if (!wiley.facets.getCachedData(facetCode).length) {
				wiley.facets.loadFacets(facetCode, false).then(function (data) {
					response = data
					wiley.facets.data[facetCode] = response;
				}, function (err) {
					console.log(err);
				});
			} else {
				response = wiley.facets.getCachedData(facetCode);
			}
			if (response) {
				wiley.facets.currentFacetCode = facetCode;
				contentArea.html(' ');
				searchInput.val('');
				modalWindow.modal('show');
				modalTitle.text(title).append('<i>' + response.length + ' ' + wiley.facets.setModalTitle(title) + '</i>');
				wiley.facets.setPlaceholder(searchInput, title);
				wiley.facets.hideModalFooterIfSingleSelect(modalWindow, isSingleSelectFacet);

				if (clusterize) {
					clusterize.clear();
					clusterize.update(wiley.facets.getFacetsRows(response, template));
					setTimeout(function () {
						scrollArea.scrollTop(0);
					}, 200);


				} else {
					clusterize = new Clusterize({
						rows: wiley.facets.getFacetsRows(response, template),
						scrollId: 'facetsModalScrollArea',
						contentId: 'facetsModalContentArea',
						rows_in_block: 20,
						no_data_text: 'No results',
						no_data_class: 'no-results'
					});
				}
				wiley.facets.bindModalWindowSearch(searchInput, response, template, clusterize);
			}
		}
	});

};
wiley.setFacetsViewMoreMobile = function (e) {
	var facetPanel = $('.facets-panel');
	var singleSelectTemplate = wiley.facets.htmlTemplates.singleSelect;
	var multiSelectTemplate = wiley.facets.htmlTemplates.multiSelect;
	var noResultTag = $('<div class="no-results">No results</div>');

	$(facetPanel).off('click');
	$('.js-all-facet-values-link').off('click');
	$(facetPanel).on('click', '.facets-panel-block-title', function () {
		var title = $(this).text();
		var $facet = $(this).parents('.js-facet');
		var itemList = $facet.find('.facets-panel-list');
		var facetCode = $facet.data('facet-code');
		var searchPanel = $facet.find('.facets-panel-search');
		var searchInput = searchPanel.find('input');
		var viewMoreLink = $facet.find('.js-all-facet-values-link');
		var template = wiley.facets.isSingleSelect($facet) ? singleSelectTemplate : multiSelectTemplate;
		var filteredItems = [];

		if (viewMoreLink.length) {
			searchPanel.addClass('show');
			if (!wiley.facets.maxItems) {
				wiley.facets.maxItems = $facet.find('.facets-panel-item').length
			}
			wiley.facets.setPlaceholder(searchInput, title);
		} else {
			searchPanel.removeClass('show');
		}
		function renderTemplate(count, searchValue, items, container, template) {
			for (var i = wiley.facets.maxItems * (count - 1); i < wiley.facets.maxItems * count; i++) {
				if (items[i]) {
					var highlightedValue = items[i].value;
					if (searchValue) {
						var theRegEx = new RegExp("(" + searchValue + ")", "igm");
						highlightedValue = items[i].value.replace(theRegEx, "<em class='highlighted'>$1</em>");
					}
					items[i].highlightedValue = highlightedValue;
					var itemStr = Mustache.render(template, items[i]);
					container.append(itemStr);
				}
			}
		}

		function setInitialState() {
			wiley.facets.loadFacets(facetCode, true).then(function (data) {
				wiley.facets.data[facetCode] = data;
				filteredItems = [].concat(wiley.facets.data[facetCode]);
				bindSearch(searchInput, filteredItems, itemList);
				showFacetsElements(1);
			}, function (err) {
				console.log(err);
			});
		}

		function reset() {
			itemList.html(' ');
			filteredItems = [].concat(wiley.facets.data[facetCode]);
			showFacetsElements();
			searchInput.val('');
		}

		function bindSearch(searchInput, response, itemList) {
			searchInput.on('keydown', function (e) {
				if (e.which === 13) {
					e.preventDefault();
					e.stopPropagation();
					return false;
				}
			});
			searchInput.on('keyup', function (e) {
				if (e.which === (91 || 8 || 83 || 38 || 65 )) {
					return;
				}
				var searchValue = searchInput.val();
				if (searchValue) {
					try {
						var regex = new RegExp(searchValue, "i");
						filteredItems = response.filter(function (item) {
							return item.value.search(regex) >= 0
						});
						if (filteredItems.length) {
							itemList.html(' ');
							showFacetsElements(null, searchValue);
						} else {
							showFacetsElements();
							itemList.html(noResultTag);
						}
					} catch (err) {
						console.log(err);
						showFacetsElements();
						itemList.html(noResultTag);
					}
				} else {
					reset();
				}
			})
		}

		function viewMore(count, searchValue, items, container, template) {
			renderTemplate(count, searchValue, items, container, template);
			if (items.length > wiley.facets.maxItems * count) {
				viewMoreLink.show()
			} else {
				viewMoreLink.hide()
			}
		}

		function closePanel(panel) {
			panel.removeClass('open');
			var facet = panel.parents('.js-facet');
			var facetCode = facet.data('facet-code');
			var loadedData = wiley.facets.data[facetCode];
			if (loadedData && loadedData.length) {
				var container = facet.find('.facets-panel-list');
				var template = wiley.facets.isSingleSelect(facet) ? singleSelectTemplate : multiSelectTemplate;
				container.html(' ');
				showFacetsElements(null, null, loadedData, container, template);
			}
		}

		function showFacetsElements(initialStep, searchValue, facets, container, tmpl) {
			var items = facets && facets.length ? facets : filteredItems;
			var count = 1;

			if (!container) {
				container = itemList
			}
			if (!tmpl) {
				tmpl = template;
			}

			viewMoreLink.off('click');
			if (initialStep) {
				count = initialStep;
			} else {
				viewMore(count, searchValue, items, container, tmpl);
			}
			viewMoreLink.on('click', function () {
				count++;
				viewMore(count, searchValue, items, container, tmpl);
			});
		}

		if ($(this).hasClass('open')) {
			$(this).removeClass('open');
			$facet.find('.facets-panel-block-inner').removeClass('open');
			viewMoreLink.off('click')
			if (wiley.facets.getCachedData(facetCode).length) {
				reset();
			}
		} else {
			var openedPanel = facetPanel.find('.facets-panel-block-title.open');
			if (openedPanel.length) {
				facetPanel.find('.facets-panel-block-inner').removeClass('open');
				closePanel(openedPanel);
			}
			$(this).addClass('open');
			$facet.find('.facets-panel-block-inner').addClass('open');
			if (viewMoreLink.length) {
				if (!wiley.facets.getCachedData(facetCode).length) {
					setInitialState();
				} else {
					reset();
					bindSearch(searchInput, wiley.facets.getCachedData(facetCode), itemList);
				}
			}

		}
	});

};

wiley.addLazyLoading = function (wrapper, container, element) {
	var listWrapper = $(wrapper);
	var windowHeight = $(window).height();
	if (!listWrapper.length) {
		return false;
	}

	function getUrlParam(name) {
		var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
		return results ? results[1] : null;
	}

	function setReqParams(requestParams, currentPage) {
		var delimiter = requestParams ? '&' : '?';
		var pageQueryKey = "page=";
		if (requestParams.indexOf('pq=') === -1) {
			requestParams = requestParams.replace('text=', 'pq='); // pass search text as query when query is missing
		}
		return pageReqParam ? requestParams.replace(pageQueryKey + pageReqParam, pageQueryKey + currentPage) : requestParams + delimiter + pageQueryKey + currentPage;
	}

	function renderResults(result, resultContainer) {
		resultContainer.append(result);
		isLoading = false;
		loading.detach();
		wiley.displayCustomSelect();
		wiley.setProductPopover();
	}

	function constructSearchUrl(params) {
		var baseUrl = wiley.constructBaseUrl();
		return baseUrl + 'loadMore' + params;
	}

	function loadResults() {
		isLoading = true;
		listWrapper.append(loading);
		currentPage += 1;
		var newReqParams = setReqParams(requestParams, currentPage);
		$.ajax({
			url: constructSearchUrl(newReqParams),
			type: "get",
			cache: true,
			async: true,
			error: function (data, status, error) {
				console.log('error', data, status, error);
				loading.detach();
			},
			success: function (result) {
				currentPage = parseInt($(result).find("input[name='_currentPage']").val());
				numberOfPages = parseInt($(result).find("input[name='_numberOfPages']").val());
				if (currentPage >= numberOfPages) {
					loading.detach();
					return;
				}
				renderResults(result, resultContainer);
				ACC.product.bindToAddToCartForm();
			}
		});
	}

	function isScrollBottom() {
		var lastElement = resultContainer.find(element).eq(-1);
		if (lastElement.length) {
			var lastPosition = lastElement.offset().top - windowHeight;
			return !isLoading && $(window).scrollTop() >= lastPosition;
		}
	}

	var requestParams = window.location.search;
	var loading = $("<div class='spinner'></div>");
	var resultContainer = $(container);
	var pageReqParam = getUrlParam('page');
	var currentPage = pageReqParam ? Number(pageReqParam) : 0;
	var isLoading = false;
	var numberOfPages = null;
	var delay = 200;

	$(window).on('scroll', function () {
		if (numberOfPages && currentPage >= numberOfPages - 1) {
			return;
		}

		// Throttling
		var isScrolling = false;
		if (!isScrolling) {
			setTimeout(function () {
				isScrolling = false;
				isScrollBottom() && loadResults();
			}, delay);
			isScrolling = true;
		}

	});
};

wiley.initDropdownComponentMobile = function () {
	if (wiley.isMobileView()) {
		var sideFacetPanel = $('.side-panel');
		var panelHeader = sideFacetPanel.find('header');
		var facetTitle = panelHeader.find('h4').text();
		var facetItems = sideFacetPanel.find('ul');
		var facetItemsLinks = facetItems.find('a');
		var facetsDropdown = $('.facets-dropdown-component');
		facetsDropdown.find('.dropdown-toggle').text(facetTitle);
		facetsDropdown.find('.dropdown-menu').append(facetItemsLinks);
	}
};

wiley.setBgImage = function () {
	$('.hero-banner').each(function (index, el) {
		var container = $(el);

		function setBg(img, element) {
			var src = img.attr('src');
			if (src) {
				src = encodeURI(src);
				var bgAttr = $(el).css('background-image');
				var newBg = window.location.origin + src;
				var oldBg = bgAttr && bgAttr.replace('url("', '').replace('")', '');
				if (newBg !== oldBg) {
					element.css('background-image', 'url(' + src + ')');
				}
			}
			img.hide()
		}

		if ($(this).find('.owl-carousel').length) {
			var images = container.find('.js-responsive-image');
			images.each(function (index, img) {
				var carouselItem = $(this).parents('.owl-item');
				setBg($(this), carouselItem);
				$(img).on('load', function () {
					setBg($(img), carouselItem);
				});
			});
		} else {
			var heroBannerContainer = container.find('.hero-banner-component');
			var img = heroBannerContainer.find('.js-responsive-image');
			setBg(img, $(el));
			img.on('load', function () {
				setBg(img, $(el));
			});

		}


	});
};

wiley.setSortOptionInUrl = function () {
	var delimiter = '|';
	var queryParamKey = '?pq=';
	var encodedDelimiter = encodeURIComponent(delimiter);
	var encodedColon = encodeURIComponent(':');
	var defaultSortOption = 'relevance';
	var isEmptyPage = $('body').hasClass('page-searchEmpty');
	var searchParams = decodeSpecialCharacters(window.location.search);
	var pqParamsArray = searchParams.split(delimiter);
	var sortOption = pqParamsArray[1];

	function isEmptyString(value) {
		return typeof(value) === 'string' && !value.trim();
	}

	function decodeSpecialCharacters(params) {
		return params.replace(/%7C/gi, delimiter);
	}

	function encodeSpecialCharacters(params) {
		return params.replace(/\|/gi, encodedDelimiter).replace(/:/gi, encodedColon);
	}

	function setNewSearchQueryParams(params) {
		window.location.search = encodeSpecialCharacters(params);
	}

	function updateUrlWithoutReloading(newUrl) {
		if (newUrl) {
			window.history.replaceState({}, '', newUrl)
		}
	}

	function updatePageReferrerForCountrySelector(newUrl) {
		if (newUrl) {
			$("#page-referer-input").val(newUrl)
		}
	}

	function setDefaultSortOptionInUrl() {
		if (searchParams.indexOf(queryParamKey) !== -1) {
			if (isEmptyPage) {
				return;
			}
			if (isEmptyString(sortOption)) {
				pqParamsArray[1] = defaultSortOption;
				var newUrl = window.location.origin + window.location.pathname + encodeSpecialCharacters(pqParamsArray.join(delimiter));
				updateUrlWithoutReloading(newUrl);
				updatePageReferrerForCountrySelector(newUrl);
			} else if (!sortOption) {
				var newUrl = window.location.href + encodedDelimiter + defaultSortOption;
				updateUrlWithoutReloading(newUrl);
				updatePageReferrerForCountrySelector(newUrl);
			}
		}
	}

	setDefaultSortOptionInUrl();

	$("#sortOptions").on("selectmenuselect change", function (event, ui) {
		var selectedSortOption = event.type === "change" ? $(this).find('option:selected')[0].value : ui.item.value;

		// pattern: ?pq=<searchText>|<sortOption>|<facet1>:<value1>|<facet2>:<value2>...|<facetN>:<valueN>
		var params = decodeSpecialCharacters(window.location.search);
		var pqParamsArray = params.split(delimiter);
		var sortOption = pqParamsArray[1];
		var isCategoryPage = $('body').hasClass('pageType-CategoryPage');

		// if category page without query parameters
		if (isCategoryPage && params.indexOf(queryParamKey) < 0) {
			setNewSearchQueryParams(queryParamKey + delimiter + selectedSortOption);

			// if search result page or category page with query parameters including sort option query parameter
		} else if (sortOption && sortOption !== selectedSortOption) {
			setNewSearchQueryParams(params.replace(sortOption, selectedSortOption));

			// if sort option query parameter is empty e.g. ?pq=||bestSeller:true
		} else if (isEmptyString(sortOption)) {
			pqParamsArray[1] = selectedSortOption;
			setNewSearchQueryParams(pqParamsArray.join(delimiter));
		}
	});

};

wiley.modalTabbing = function () {
	var tabbing = function (jqSelector) {
		var inputs = jqSelector.find('select, input, textarea, button, a[href]').filter(':visible').not(':disabled');
		var isFacetsWnd = function () {
			return jqSelector.attr('id') === 'facetsListWindow'
		};
		var activeIndex = isFacetsWnd() ? 1 : -1;

		//Focus to last button in the container.
		inputs.eq(activeIndex).focus();
		jqSelector.on('keydown', function (e) {
			if (e.which === 9) {
				/*redirect last tab to first input*/
				if (!e.shiftKey) {
					if (inputs[inputs.length - 1] === e.target) {
						e.preventDefault();
						inputs.first().focus();
					}
				}
				/*redirect first shift+tab to last input*/
				else {
					if (inputs[0] === e.target) {
						e.preventDefault();
						inputs.last().focus();
					}
				}
			}
			if (e.which === 27) {
				$(this).modal('hide');
			}
		});
	};

	$('.modalWindow').on('shown.bs.modal', function () {
		if ($(this).attr('id') === 'addToCartModalWnd') {
			wiley.setProductPopover();
		}
		if (!wiley.isMobileView()) {
			tabbing($(this));
		}
	})
};

wiley.submitSearchResultForm = function () {
	var searchForm = $('form[name^="search_form_"]');
	var delimiter = '|';

	function buildUrlSearchParams(name, value) {
		var defaultSortValue = 'relevance';
		value = value.replace(/\|+/g, ' ');
		return name + '=' + encodeURIComponent(value + delimiter + defaultSortValue);
	}

	searchForm.submit(function () {
		var searchInput = $("#js-site-search-input");
		var searchName = searchInput.attr('name');
		var searchValue = $.trim(searchInput.val());
		var searchUrl = searchForm.attr('action');
		if (searchValue) {
			window.location.href = window.location.origin + searchUrl + '?' + buildUrlSearchParams(searchName, searchValue)
		}
		return false
	});
};

// TODO: use wileyAjaxCalls.js from wileystorefrontcommons
wiley.updateMultipleSectionsAjax = function (data, callback) {
	var elements = $(data).find("element");

	$(elements).each(function (index) {
		var section = $(this);
		var sectionKey = section.find("key").html();
		var sectionValue = section.find("value");
		$("#" + sectionKey).replaceWith(sectionValue[0].innerText);
	});

	callback.call();
}

wiley.collapseElements = function (parent, button, content) {
	$(parent).each(function () {
		var targetButton = $(this).find(button);
		var targetContent = $(this).find(content);

		if (!targetButton.hasClass('collapsed')) {
			targetButton.addClass('collapsed');
			targetContent.collapse('hide');
		}
	});
};

wiley.customTabs = function () {
	var tabContainerItem = $(".common-tabs .nav-tabs li");
	var tabList = tabContainerItem.closest('ul');

	//Show scrollBar
	if (tabContainerItem.length > 4) {
		tabList.addClass("customScroll").perfectScrollbar();
		setTimeout(function () {
			tabList.perfectScrollbar('update');
		}, 1000);
	}

	//Use tabCollapse plugin for common-tabs
	$('#pdpTabsCollapsed').tabCollapse();

	//Add functionality to close sectionPermision
	if (wiley.isMobileView()) {

		$('#pdpTabsCollapsed-accordion').on('click', '.js-tabcollapse-panel-heading', function () {
			wiley.collapseElements(
				'.section-permissions',
				'#section-permissions-button',
				'#section-permissions-item'
			);
		});

		setTimeout(function () {
			wiley.collapseElements(
				'#pdpTabsCollapsed-accordion .panel',
				'.js-tabcollapse-panel-heading',
				'.panel-collapse'
			);
		}, 1000);


	}

};

wiley.searchResultPage = function () {
	var mainHeader = $('.main-sticky-header');
	var searchResultPage = $('.search-result-page');
	var searchResultHeader = $('.search-result-header');
	var searchPanel = $('.main-navigation-search');
	var headerHeight = mainHeader.height() + searchPanel.height();

	if (searchResultPage.length) {
		searchResultPage.css('padding-top', searchResultHeader.outerHeight() - 5);
		searchResultHeader.css('top', headerHeight - 5);
	}

	if (searchResultHeader.length) {
		$(window).scroll(function () {
			if ($(this).scrollTop() > 78) {
				searchResultHeader.addClass('sticky-tabs');
				mainHeader.addClass('no-shadow');
			} else {
				searchResultHeader.removeClass('sticky-tabs');
				mainHeader.removeClass('no-shadow');
			}
		});
	}

};

wiley.customUiElements = function () {
	var productItemSpinner = $('.product-item-spinner');
	var min = 1;
	var max = 10;
	var value = min;

	if (productItemSpinner.length) {
		productItemSpinner.spinner({
			min: min,
			max: max,
			change: function (event, ui) {
				var val = $(this).spinner("value");
				if (!val) {
					$(this).spinner("value", min);
				}
			}
		}).spinner("value", min);
		productItemSpinner.on('input', function (e) {
			var inputVal = $.trim($(this).val());
			if (inputVal && (isNaN(inputVal) || inputVal > max || inputVal < min)) { //Only numbers and not bigger than max
				$(this).prop("value", parseInt(value, 10)); //Back to the prev value
			} else {
				if (inputVal) {
					value = parseInt(inputVal, 10);
					$(this).val(value);
				}
			}
		})
	}
};

wiley.readExcerptDrpodown = function readExcerptDrpodown() {
	var $readExcerptDrpodown = $('.dropdown-block.read-excerpt');
	var $btn = $readExcerptDrpodown.find('button.collapsed');
	var $innerBlock = $readExcerptDrpodown.find('nav');

	function closeDropdown() {
		$innerBlock.removeClass('in');
		$btn.addClass('collapsed');
	}

	function scrollHandler() {
		closeDropdown();
		$(window).off('scroll', scrollHandler);
	};

	function clickHandler(e) {
		var target = $(e.target);
		if (!target.hasClass('read-excerpt') && !target.parents('.read-excerpt').length) {
			closeDropdown();
			$(window).off('click', clickHandler);
		}
	}

	$readExcerptDrpodown.click(function () {
		$(window).on('scroll', scrollHandler);
		$(window).on('click', clickHandler);
	});

};

wiley.setSectionsMenu = function setSectionsMenu() {
	try {
		var $menuSections = $('.page-sections-menu');
		if ($menuSections.length) {
			var $pointer = $('<span class="active-pointer"></span>').prependTo($menuSections);
			var menuSectionsHeight = $menuSections.innerHeight();
			var $parentMenu = $menuSections.parent();
			var $content = $('.product-details-content');
			var headerHeight = $('.main-sticky-header').height();
			var parentHeight = $parentMenu.height();
			var breadcrumbsHeight = $('.breadcrumbs').height();
			var $mainFooter = $('.main-footer-container');
			var mainFooterHeight = $mainFooter.outerHeight();
			var delay = 50;
			var menuSectionWidth = $menuSections.width();
			var minWidth = 1240;
			var topLimit = parentHeight - menuSectionsHeight + breadcrumbsHeight + 25;
			var page = $("html, body");
			var isAnimated = false;
			var setTimer;
			var sectionsMap = [];
			var activeLink;

			function truncateLinks(links) {
				if (links.length && !wiley.isMobileView()) {
					links.find('a').truncate({width: '360'});
				}
			}

			function setActiveLink(activeLink) {
				var $link = $menuSections.find('a[href="' + activeLink + '"]');
				if (!$link.hasClass('active')) {
					var liItem = $link.parent();
					$pointer.css('top', liItem.position().top + 9);
					$link.parents('ul').find('li.active').removeClass('active');
					liItem.addClass('active');
				}
			}

			function createSectionsMap($items, sectionsMap) {
				// create array sectionsMap with sections data: offsetTop and id
				$items.each(function (i, link) {
					var section = $(link).find('a').attr('href');
					if ($(section).length) {
						var item = {href: section, offsetTop: $(section).offset().top};
						//reorder 2 first sections if Evaluation copy is higher than Description
						if (sectionsMap.length === 1 && item.offsetTop < sectionsMap[i - 1].offsetTop) {
							sectionsMap[i] = sectionsMap[i - 1];
							sectionsMap[i - 1] = item;
							//$(link).css('order', 0);
						} else {
							sectionsMap.push(item);
						}
					} else {
						$(link).detach();
					}
				});
			}

			function init() {
				truncateLinks($menuSections);
				$menuSections.addClass('initialized');
				createSectionsMap($items, sectionsMap);
				activeLink = sectionsMap[0].href;
				setActiveLink(activeLink);
				bindClickHandler();
				bindScroll();
			}

			function findActiveSection() {
				var fromTop = window.pageYOffset;
				var activeSection = sectionsMap[0];
				sectionsMap.forEach(function (item) {
					if (item.offsetTop <= fromTop + headerHeight && item.offsetTop > activeSection.offsetTop) {
						activeSection = item;
					}
				});
				if (activeLink !== activeSection.href) {
					activeLink = activeSection.href;
					setActiveLink(activeLink);
				}
			}

			function bindScroll() {
				$(window).scroll(function (e) {
					if (!wiley.isMobileView()) {
						// Throttling
						var isScrolling = false;
						if (!isScrolling) {
							setTimeout(function () {
								isScrolling = false;

								!isAnimated && findActiveSection();

								if ($(this).scrollTop() > topLimit) {
									$menuSections.addClass('sticky');
								} else {
									$menuSections.removeClass('sticky');
								}

								if ($(window).scrollTop() > $(document).height() - mainFooterHeight - headerHeight - menuSectionsHeight) {
									$menuSections.addClass('bottom-limit');
								} else {
									$menuSections.removeClass('bottom-limit');
								}
								// vertical scroll
								if ($(window).width() < minWidth) {
									var left = $content[0].getBoundingClientRect().left + 35;
									var dif = left - menuSectionWidth;
									if (!$menuSections.hasClass('bottom-limit')) {
										if (dif < 0) {
											$menuSections.offset({left: dif + 50});
										} else {
											$menuSections.removeAttr('style');
										}
									}
								}
							}, delay);
							isScrolling = true;
						}
					}
				});
			}

			function bindClickHandler() {
				$menuSections.on('click', 'li', function (e) {
					if (!wiley.isMobileView()) {
						isAnimated = true;
						clearTimeout(setTimer);
						var parent = $(this).parent();
						var target = $(this).find('a').attr('href');
						$pointer.css('top', $(this).position().top + 9);
						parent.find('li.active').removeClass('active');
						$(this).addClass('active');
						if ($(target).length) {
							var targetPos = $(target).offset().top - headerHeight;
							page.stop().animate({
								scrollTop: targetPos
							}, {
								duration: 300, done: function () {
									setTimer = setTimeout(function () {
										isAnimated = false;
									}, 1500)
								}
							});
						}
						return false;
					}
				});
			}

			if (!wiley.isMobileView() && !$menuSections.hasClass('initialized')) {
				var $items = $menuSections.find('li');
				if ($items.length) {
					$(window).load(function () {
						init();
					});
				}
			}
		}
	} catch (e) {
		console.log(e);
	}
};

wiley.truncateDescription = function truncateDescription() {
	var btn = $('<div class="more-text-btn"><span tabindex="0">READ MORE</span></div>');
	var dots = $('<span class="more-text-dots">...</span>');
	var longDescription = $('.product-long-description');
	var maxLength = 1000;

	function cutString(text, maxLength, index, lastCharsAmount) {
		return text.substring(0, maxLength + (index - lastCharsAmount));
	}

	if (longDescription.length) {
		var innerContent = longDescription.find('.description');
		var text = innerContent.html();
		var length = $.trim(innerContent.text()).length;
		if (length > maxLength) {
			var newStr = text.substring(0, maxLength);
			var lastCharsAmount = 5; // to check for tags elements
			var lastChars = text.substring(maxLength - lastCharsAmount, maxLength);
			var ampersandIndex = lastChars.indexOf('&');
			var openTagIndex = lastChars.indexOf('<');
			var startIndex = openTagIndex !== -1 ? openTagIndex : ampersandIndex !== -1 ? ampersandIndex : null;
			if (startIndex) {
				newStr = cutString(text, maxLength, startIndex, lastCharsAmount);
			}
			innerContent.empty().html(newStr + '...');
			innerContent.append(btn);

			btn.on('click keydown', function (e) {
				var keyCode = (e.keyCode || e.which);
				/** Accessibility support. Key codes: enter-13  **/
				if (e.type === 'click' || keyCode === 13) {
					innerContent.empty().html(text);
				}
			});
		}
	}

};

wiley.paginationForm = function () {
	var paginationForm = $('.page-number-form');
	var paginationInput = paginationForm.find('input');

	function pageReloader(form, input) {
		var actionAttrValue = form.attr("action");
		var pageNumber = $(input).val() - 1;
		var url = window.location.href;

		if ($(input).val() != input.defaultValue) {
			var arr = url.split('?');

			if (actionAttrValue.length !== 0) {
				if (arr.length > 1 && arr[1] !== '') {
					window.location.href = actionAttrValue + "&page=" + pageNumber;
				} else {
					window.location.href = actionAttrValue + "?page=" + pageNumber;
				}
			} else {
				window.location.href = arr[0] + "?page=" + pageNumber;
			}
		}
	}

	if (paginationForm.length) {
		$(paginationInput).on("keydown", function (e) {
			var key = e.charCode || e.keyCode || 0;
			var keyValue = e.key;
			var numbers = /^[0-9]+$/;

			if (keyValue.match(numbers) || key == 8 || key == 9 || key == 46 || (key >= 37 && key <= 40)) {
				return true;
			} else {
				return false;
			}
		}).on("keyup", function () {
			if ($(this).val() == '0') {
				$(this).val(this.defaultValue);
			}
		}).on("focusout", function () {
			pageReloader(paginationForm, this);
		});

		$(paginationForm).on('keyup keypress', 'input[type="text"]', function (e) {
			if (e.which == 13) {
				e.preventDefault();
				pageReloader(paginationForm, this);
			}
		});

		function paginationOnAndroid(element) {
			var elValue = $(element).val();
			var numbers = /^[0-9]+$/;
			var newValue;

			if (elValue == '') {
				return true;
			} else if (elValue == '0') {
				$(element).val(element.defaultValue);
			} else if (!elValue.match(numbers)) {
				newValue = elValue.slice(0, -1);
				$(element).val(newValue);
			}
		}

		if (wiley.checkDevice.isAndroid()) {
			$(paginationInput).each(function () {
				this.oninput = function () {
					paginationOnAndroid(this)
				};
			});
		}
	}
};

wiley.getDiscount = function getDiscount() {
	var discountBlock = $('.get-discount');
	var more = 'VIEW MORE';
	var less = 'VIEW LESS';
	var btn = $('<div><span class="blue-link-btn" tabindex="0">' + more + '</span></div>');
	var wrapper = $('<div class="discount-container-extra"></div>');
	var minLength = 3;
	discountBlock.each(function (index, item) {
		var discountItems = $(item).find('.discount-item');
		var container = $(item).find('.discount-items');
		if (discountItems.length > minLength) {
			var hiddenElementsLength = discountItems.length - minLength;
			var hiddenElements = discountItems.slice(-hiddenElementsLength);
			var btnLink = btn.find('span');
			hiddenElements.wrapAll(wrapper);
			container.append(btn);
			var hiddenWrapper = container.find('.discount-container-extra');
			btnLink.on('click keydown', function (e) {
				var keyCode = (e.keyCode || e.which);
				/** Accessibility support. Key codes: enter-13  **/
				if (e.type === 'click' || keyCode === 13) {
					$(this).toggleClass('open');
					$(this).text($(this).hasClass('open') ? less : more);
					hiddenWrapper.slideToggle();
				}
			});
		}
	});
};

wiley.setItemsPerPage = function setItemsPerPage() {
	var selects = $('.itemsPerPageLimiter');
	var searchParams = window.location.search;
	var queryParamKey = 'size'; // Items per page
	var paramsArray = searchParams.split('&');

	function findParam(params, key) {
		var result;
		params.forEach(function (item) {
			if (item.indexOf(key) !== -1) {
				result = item;
			}
		});
		return result;
	}

	function updateParams(paramsArray) {
		if (paramsArray[0].indexOf('page') !== -1) {
			paramsArray.shift();
			return paramsArray.join('&');
		}
		else {
			return paramsArray.filter(function (item) {
				return item.indexOf('page') === -1
			}).join('&');
		}
	}

	function init() {
		if (selects.length) {
			selects.each(function (i, select) {
				var selectedOption = $(select).find('option:selected');
				if (selectedOption) {
					var value = parseInt(selectedOption[0].value, 10);
					if (!wiley.isMobileDevice()) {
						$(this).selectmenu("refresh");
					} else {
						var parent = $(select).parents('.select-component');
						var customInput = parent.find('em');
						customInput.text(value);
					}
				}
				$(select).on("selectmenuselect change", function (event, ui) {
					var sizeParam = findParam(paramsArray, queryParamKey);
					var selectedOptionVal = parseInt(selectedOption[0].value, 10);
					var newSelectedVal = event.type === "change" ? parseInt($(this).find('option:selected')[0].value, 10) : parseInt(ui.item.value, 10);
					if (sizeParam) {
						var value = sizeParam.split('=')[1];
						if (newSelectedVal !== parseInt(value, 10)) {
							var params = updateParams(paramsArray);
							window.location.search = params.replace(queryParamKey + "=" + value, queryParamKey + "=" + newSelectedVal);
						}
					} else {
						if (selectedOptionVal !== newSelectedVal) {
							var params = updateParams(paramsArray);
							var delimiter = params.length ? '&' : '?';
							window.location.search = params + delimiter + queryParamKey + '=' + newSelectedVal;
						}
					}
				});
			});
		}
	}

	init();
};

wiley.addSeeAllBtn = function addSeeAllBtn() {
	if (wiley.isMobileView()) {
		var container = $('.who-we-serve-blocks');
		var btn = $('<div class="more-text-btn shift visible-xs"><span>SEE ALL</span></div>');
		var elements = container.find('.who-we-serve-block');
		var maxLength = 4;
		var hiddenElementsLength = elements.length - maxLength;
		var hiddenElements = elements.slice(-hiddenElementsLength);
		hiddenElements.addClass('hidden-xs');
		container.append(btn);
		var link = btn.find('span');
		link.click(function () {
			hiddenElements.removeClass('hidden-xs');
			$(this).remove();
		});
	}
};

wiley.setPurchaseOptionsHeight = function setPurchaseOptionsHeight() {
	var tiles = $('.choose-type-of-book').find('.item-wr');
	if (tiles.length) {
		var maxHeight = Math.max.apply(null, tiles.map(function () {
			return $(this).innerHeight();
		}).get());
		tiles.height(maxHeight);
	}
};

wiley.setProductCarousel = function setProductCarousel() {
	var container = $('.related-products-section');
	var title = container.find('h4');
	var carousel = container.find('.carousel-gallery');
	var id = 'pdp-carousel-section-item';
	title.addClass('collapsed section-title');
	title.attr('data-toggle', "collapse");
	title.attr('data-target', "#" + id);
	carousel.attr('id', id);
	carousel.addClass('collapse');
};


wiley.initProductDetailsPage = function initProductDetailsPage() {

	if ($('.product-details-page').length) {
		wiley.setPurchaseOptionsHeight();
		wiley.setSectionsMenu();
		wiley.truncateDescription();
		wiley.getDiscount();
		wiley.readExcerptDrpodown();
		wiley.setProductCarousel();
	}

};

wiley.setHoverDelay = function setHoverDelay() {
	var defaultValue = 250;

	function getDelay(value) {
		if (wiley.isMobileDevice()) {
			return 0;
		}
		return value !== undefined && value >= 0 ? value : defaultValue;
	}

	wiley.delayIn = getDelay(ACC.dropDownOpenDelay);
	wiley.delayOut = getDelay(ACC.dropDownCloseDelay);
};

wiley.stickyFooter = function stickyFooter() {
	var mainContainer = $('body > main');
	var footerElement = $('.main-footer-container');
	var isMobilePDP = $('.product-add-to-cart').length === 1 && wiley.isMobileView();

	if (isMobilePDP) {
		footerElement.addClass('footerOnPDP');
	} else {
		footerElement.removeClass('footerOnPDP');
	}

	if (footerElement.length) {
		mainContainer.css('padding-bottom', footerElement.outerHeight() + 'px')
	}
};

wiley.backToTop = function backToTop() {
	var backToTop = $('.back-to-top');
	var mainFooterHeight = $('.main-footer-container').outerHeight();
	var isMobilePDP = $('.product-add-to-cart').length === 1 && wiley.isMobileView();

	if (wiley.checkDevice.isIE() && !wiley.isMobileView()) {
		backToTop.css('right', '20px');
	}
	function scrollToTop(e) {
		e.preventDefault();
		$('html, body').animate({scrollTop: 0}, 1000);
	}

	if (backToTop.length) {
		backToTop.on('click touchstart', function (e) {
			scrollToTop(e);
		});

		backToTop.on('keydown', function (e) {
			if (e.keyCode === 13 || e.keyCode === 27) {
				scrollToTop(e);
			}
		});

		function changePositionBtn(val) {
			val = val || 0;

			if (window.pageYOffset + window.innerHeight < $(document).height() - (mainFooterHeight - val)) {
				backToTop.addClass("fixed");
			} else {
				backToTop.removeClass("fixed");
			}
		}

		$(window).scroll(function () {
			if (window.pageYOffset > 0) {
				backToTop.fadeIn();
			} else {
				backToTop.fadeOut();
			}

			if (isMobilePDP) {
				var addToCartHeight = $('.product-add-to-cart').outerHeight();
				changePositionBtn(addToCartHeight);
				backToTop.addClass("upraised");
			} else {
				changePositionBtn();
			}
		});
	}
};

wiley.imageChecker();

$(document).ready(function () {
	wiley.setHoverDelay();
	wiley.stickyFooter();
	wiley.mediaGallery();
	wiley.setCustomScroll();
	wiley.iPadViewport();
	wiley.displayCustomSelect();
	wiley.collapsePurchaseOption();
	wiley.initPopover();
	wiley.setProductPopover();
	wiley.initCountrySelect();
	wiley.initMainSearch();
	wiley.initSelectCountryModalWindow();
	wiley.setCustomVH();
	wiley.setMainNavigation();
	wiley.fluidContainerBackground();
	wiley.initFacetPanel();
	wiley.initDropdownComponentMobile();
	wiley.setBgImage();
	wiley.setSortOptionInUrl();
	wiley.modalTabbing();
	wiley.submitSearchResultForm();
	wiley.setStickyHeader();
	wiley.searchResultPage();
	wiley.disableAutoZoomIOS();
	wiley.customUiElements();
	wiley.paginationForm();
	wiley.setItemsPerPage();
	wiley.addSeeAllBtn();
	wiley.initProductDetailsPage();
	wiley.customTabs();
	wiley.backToTop();

	$(window).on("orientationchange", function (e) {
		setTimeout(function () {
			if (!wiley.isMobileView()) {
				var facetPanelWrapper = $('.facets-panel-wrapper');
				if (facetPanelWrapper.hasClass('open')) {
					facetPanelWrapper.find('.facets-panel-block-title.open').trigger('click');
					$('facets-panel-wrapper').removeClass('open');
				}
				wiley.setFacetsViewMoreDesktop();
			} else {
				wiley.setCustomVH();
				wiley.setFacetsViewMoreMobile();
			}
			wiley.setSectionsMenu();
			wiley.stickyFooter();
			wiley.setBgImage();
			wiley.setMainNavigation();
			wiley.fluidContainerBackground();
			$('.modalWindow:visible').modal('hide');
			$('body, html').removeClass("has-drawer");

		}, 0);
	});
});

