<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<%--  AddOn Common CSS files --%>
<c:forEach items="${addOnCommonCssPaths}" var="addOnCommonCss">
	<link rel="stylesheet" type="text/css" media="all" href="${addOnCommonCss}"/>
</c:forEach>

<%--  Common CSS files --%>

<link rel="stylesheet" type="text/css" media="all" href="${commonResourcePath}/css/cookieconsent.min.css"/>
<link rel="stylesheet" type="text/css" media="all" href="${commonResourcePath}/css/style.css"/>
<link rel="stylesheet" type="text/css" media="all" href="${commonResourcePath}/css/pagination.css"/>

<%-- Theme CSS files --%>
<link rel="stylesheet" type="text/css" media="all" href="${themeResourcePath}/css/style.css"/>

<%--  AddOn Theme CSS files --%>
<c:forEach items="${addOnThemeCssPaths}" var="addOnThemeCss">
	<link rel="stylesheet" type="text/css" media="all" href="${addOnThemeCss}"/>
</c:forEach>
