<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
	

	<h1 class="full-width-gray"><spring:theme code="text.account.profile.updatePersonalDetails"/></h1>

    <div class="my-account-wrap personal-details">
        <div class="row">
          <div class="col-xs-12 col-sm-4">
                <div class="site-form">
                    <form:form method="post" commandName="updateProfileForm">
                    <formElement:formSelectBox idKey="profile.title" labelKey="profile.title" path="titleCode" mandatory="false" skipBlank="true" skipBlankMessageKey="form.select.empty" items="${titleData}"/>
                    <formElement:formInputBox idKey="profile.firstName" labelKey="profile.firstName" path="firstName" inputCSS="text" mandatory="true" placeholder="profile.firstName.placeholder"/>
                    <formElement:formInputBox idKey="profile.middleName" labelKey="profile.middleName" path="middleName" inputCSS="text" mandatory="false" placeholder="profile.middleName.placeholder"/>
                    <formElement:formInputBox idKey="profile.lastName" labelKey="profile.lastName" path="lastName" inputCSS="text" mandatory="true" placeholder="profile.lastName.placeholder"/>
                    <formElement:formSelectBox idKey="profile.suffix" labelKey="profile.suffix" path="suffixCode"  mandatory="false" skipBlank="false" skipBlankMessageKey="form.select.empty" items="${suffixData}"/>
                    <formElement:formInputBox idKey="profile.studentId" labelKey="profile.studentId" path="studentId" inputCSS="text" mandatory="false" placeholder="profile.studentId.placeholder"/>
                    <formElement:formInputBox idKey="profile.major" labelKey="profile.major" path="major" inputCSS="text" mandatory="false" placeholder="profile.major.placeholder"/>
                    <div class="row">
                      <div class="col-xs-12 col-sm-12">
                          <div class="form-label required">
                              <label for="month">
                                    <spring:theme code="profile.graduationDate"/>
                                    <span class="skip">
                                        <form:errors path="graduationMonth"/>
                                    </span>
                              </label>
                          </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-xs-12 col-sm-6">
                          <div class="form-cell">
                              <div class="form-field select-component select-form-element">
                                <form:select id="month" path="graduationMonth" cssClass="form-control">
						  		<form:option value="" disabled="true" selected="true"><spring:theme code="profile.month.placeholder"/></form:option>
                                    <form:options items="${months}" itemValue="code" itemLabel="name"/>
                                </form:select>
                              </div>
                          </div>
                      </div>
                      <div class="col-xs-12 col-sm-6">
                          <div class="form-cell">
                              <div class="form-field select-component select-form-element">
                                <form:select id="month" path="graduationYear" cssClass="form-control">
						  		<form:option value="" disabled="true" selected="true"><spring:theme code="profile.year.placeholder"/></form:option>
                                    <form:options items="${years}" itemValue="code" itemLabel="name"/>
                                </form:select>
                              </div>
                          </div>
                      </div>
                  </div>

                    <formElement:formCheckbox idKey="enrolledInSchool" labelKey="profile.enrolledInSchool" path="enrolledInSchool"/>
                    <div id="school-name">
                        <formElement:formSelectBox idKey="profile.school" labelKey="profile.school" path="school"  mandatory="false" items="${schoolData}" selectCSSClass="form-control"/>
                    </div>
                    <div class="my-account-button-wrap">
                        <div class="row">
                            <div class="col-xs-12 col-sm-7">
                                <button type="submit" class="button button-main large">
                                    <spring:theme code="text.account.profile.saveUpdates" text="Save Updates"/>
                                </button>
                            </div>
                            <div class="col-xs-12 col-sm-5">
                                <button type="button" class="button button-main large button-outlined backToHome">
                                    <spring:theme code="text.account.profile.cancel" text="Cancel"/>
                                </button>
                            </div>
                        </div>
                    </div>
                    </form:form>
                </div>
         </div>
       </div>
    </div>

