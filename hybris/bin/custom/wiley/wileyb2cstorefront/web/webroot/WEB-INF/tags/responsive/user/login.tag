<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="actionNameKey" required="true" type="java.lang.String"%>
<%@ attribute name="action" required="true" type="java.lang.String"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>

<div class="site-form central-form">
	<form:form action="${action}" method="post" commandName="loginForm">
		<c:if test="${not empty message}">
			<span class="has-error">
				<spring:theme code="${message}" />
			</span>
		</c:if>

			<div class="form-cell heading">
				<h1><spring:theme code="login.title" /></h1>
			</div>
            <formElement:formInputBox idKey="j_username" labelKey="login.email" path="j_username"
                mandatory="true" placeholder="login.emailPlaceholder" inputCSS="form-field"/>
            <c:if test="${param.error}">
                <div class="form-cell has-error">
                    <div class="help-block">
                        <spring:theme code="login.loginFailureCauseUser" />
                    </div>
                </div>
            </c:if>

            <formElement:formPasswordBox idKey="j_password" labelKey="login.password" path="j_password"
                mandatory="true" placeholder="login.passwordPlaceholder"/>
            <c:if test="${param.error}">
                <div class="form-cell has-error">
                    <div class="help-block">
                        <spring:theme code="login.loginFailureCausePassword" />
                    </div>
                </div>
            </c:if>


		<div class="form-cell" id="forgotPasswordLink">
			<div class="form-field">
				<ycommerce:testId code="login_forgotPassword_link">
					<a href="<c:url value='/login/pw/request'/>" class="js-password-forgotten"
							data-cbox-title="<spring:theme code="forgottenPwd.title"/>">
						<spring:theme code="login.link.forgottenPwd" />
					</a>
				</ycommerce:testId>
			</div>
		</div>

		<div class="form-cell">
			<ycommerce:testId code="loginAndCheckoutButton">
				<button type="submit" class="button button-main large">
					<spring:theme code="${actionNameKey}" />
				</button>
			</ycommerce:testId>
		</div>
	</form:form>
</div>
<div id="formForgotPassword"></div>