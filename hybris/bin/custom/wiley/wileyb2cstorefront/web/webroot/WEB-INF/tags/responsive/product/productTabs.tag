<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ attribute name="activeTab" required="true" type="java.lang.String" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>

<c:if test="${not empty activeTab}">
    <c:if test="${not empty product.tableOfContents}">
        <section id="content-section" class="page-section">
            <div class="page-section-content">
                <h2 data-toggle="collapse" data-target="#content-section-item" class="section-title collapsed">
                    <spring:theme code="product.product.details.tableOfContents"/>
                </h2>
                <div id="content-section-item" class="collapse">
                        ${product.tableOfContents}
                </div>
            </div>
        </section>
    </c:if>
    <c:if test="${not empty product.newToEdition}">
        <section id="new-to-this-edition-section" class="page-section">
            <div class="page-section-content">
                <h2 data-toggle="collapse" data-target="#new-to-this-edition-section-item" class="section-title collapsed">
                    <spring:theme code="product.product.details.newToEdition"/>
                </h2>
                <div id="new-to-this-edition-section-item" class="collapse">
                        ${product.newToEdition}
                </div>
            </div>
        </section>
    </c:if>
    <c:if test="${not empty product.wileyReviews}">
        <section id="reviews-section" class="page-section">
            <div class="page-section-content">
                <h2 data-toggle="collapse" data-target="#reviews-section-item" class="section-title collapsed">
                    <spring:theme code="product.product.details.reviews"/>
                </h2>
                <div id="reviews-section-item" class="collapse">
                        ${product.wileyReviews}
                </div>
            </div>
        </section>
    </c:if>
    <c:if test="${not empty product.relatedWebsites}">
        <section id="extra-section" class="page-section">
            <div class="page-section-content">
                <h2 data-toggle="collapse" data-target="#extra-section-item" class="section-title collapsed">
                    <spring:theme code="product.product.details.extra"/>
                </h2>
                <div id="extra-section-item" class="collapse">
                        ${product.relatedWebsites}
                </div>
            </div>
        </section>
    </c:if>
    <c:if test="${not empty product.downloadsTab}">
        <section id="downloads-section" class="page-section">
            <div class="page-section-content">
                <h2 data-toggle="collapse" data-target="#downloads-section-item" class="section-title collapsed">
                    <spring:theme code="product.product.details.downloads"/>
                </h2>
                <div id="downloads-section-item" class="downloads-section-content collapse">
                    <product:productDownloads downloadsTab="${product.downloadsTab}"/>
                </div>
            </div>
        </section>
    </c:if>
    <c:if test="${not empty product.errata}">
        <section id="errata-section" class="page-section">
            <div class="page-section-content">
                <h2 data-toggle="collapse" data-target="#errata-section-item" class="section-title collapsed">
                    <spring:theme code="product.product.details.errata"/>
                </h2>
                <div id="errata-section-item" class="collapse">
                        ${product.errata}
                </div>
            </div>
        </section>
    </c:if>
    <c:if test="${not empty product.notes}">
        <section id="notes-section" class="page-section">
            <div class="page-section-content">
                <h2 data-toggle="collapse" data-target="#notes-section-item" class="section-title collapsed">
                    <spring:theme code="product.product.details.notes"/>
                </h2>
                <div id="notes-section-item" class="collapse">
                        ${product.notes}
                </div>
            </div>
        </section>
    </c:if>
    <c:if test="${not empty product.whatsNewToDisplay}">
        <section id="whats-new-section" class="page-section">
            <div class="page-section-content">
                <h2 data-toggle="collapse" data-target="#whats-new-section-item" class="section-title collapsed">
                    <spring:theme code="product.product.details.whatsNew"/>
                </h2>
                <div id="whats-new-section-item" class="collapse">
                        ${product.whatsNewToDisplay}
                </div>
            </div>
        </section>
    </c:if>
    <c:if test="${not empty product.pressRelease}">
        <section id="press-release-section" class="page-section">
            <div class="page-section-content">
                <h2 data-toggle="collapse" data-target="#press-release-section-item" class="section-title collapsed">
                    <spring:theme code="product.product.details.pressRelease"/>
                </h2>
                <div id="press-release-section-item" class="collapse">
                        ${product.pressRelease}
                </div>
            </div>
        </section>
    </c:if>
</c:if>