<%@ page trimDirectiveWhitespaces="true"%>


<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi" %>

<%--<multi-checkout:orderTotals cartData="${cartData}" showTaxEstimate="false" showTax="true" />--%>
<multi-checkout:checkoutOrderSummarySection cartData="${cartData}"
                                            showDeliveryAddress="true"
                                            showPaymentInfo="false"
                                            showTax="true"
                                            showTaxEstimate="false"/>