<%@ page contentType="application/json" language="java" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<json:array items="${facet.values}" var="facetValue">
	<json:object>
		<json:property name="value" value="${facetValue.name}"/>
		<spring:theme code="search.nav.facetValueCount" arguments="${facetValue.count}" var="facetValueCount"/>
		<json:property name="count" value="${facetValueCount}"/>
		<spring:eval expression="T(de.hybris.platform.util.SafeURLEncoder).encode('|' + facet.code + ':' + facetValue.code)" var="encodedCurrentOptionUrl" />
		<json:property name="currentOptionUrl" value="${encodedCurrentOptionUrl}"/>
		<json:property name="pq" value="${facetValue.query.query.value}"/>
		<c:url value="${facetValue.query.url}" var="facetValueQueryUrl"/>
		<json:property name="link" value="${facetValueQueryUrl}"/>
		<json:property name="selected" value="${facetValue.selected}"/>
	</json:object>
</json:array>