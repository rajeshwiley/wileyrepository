<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<spring:url value="/my-account/digital-products" var="myDigitalProductsUrl"/>

<template:page pageTitle="${pageTitle}" containerCSSClass="container reg-code-confirmation wrap">
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <h1 class="full-width-gray"><spring:theme code="regCode.confirmation.pageTitle" /></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-6 left-block">
            <div class="success-message icon-check wrap">
                <p>
                    <spring:theme code="regCode.confirmation.confirmMessage" />
                </p>
            </div>
            <div class="buttons-wrap">
                <form method="get" action="${myDigitalProductsUrl}">
                    <button class="button button-main large">
                        <spring:theme code="regCode.confirmation.buttonLabel" />
                    </button>
                </form>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6">
        </div>
    </div>
</template:page>