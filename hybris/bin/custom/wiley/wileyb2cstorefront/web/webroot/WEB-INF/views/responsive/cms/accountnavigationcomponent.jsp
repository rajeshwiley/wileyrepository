<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>

<c:set var="servletPath" value="${requestScope['javax.servlet.forward.servlet_path']}" />

<c:if test="${navigationNode.visible}">
	<jsp:useBean id="linkVariants" class="java.util.HashMap" scope="request"/>
	<c:set target="${linkVariants}" property="/my-account/orders" value="${fn:split('/my-account/order,/my-account/orders', ',')}" />
	<c:set target="${linkVariants}" property="/my-account/address-book" value="${fn:split('/my-account/address-book,/my-account/add-address', ',')}" />

	<c:forEach items="${navigationNode.links}" var="link">
		<c:set var="linkUrl" value="${link.url}"/>
		<c:set var="selected" value=""/>

		<c:choose>
			<c:when test="${linkVariants.containsKey(linkUrl)}">
				<c:forEach items="${linkVariants.get(linkUrl)}" var="linkUrlVariant">
					<c:if test="${empty selected && fn:contains(servletPath, linkUrlVariant)}">
						<c:set var="selected" value="active"/>
					</c:if>
				</c:forEach>
			</c:when>
			<c:otherwise>
				<c:if test="${fn:contains(servletPath, linkUrl)}">
					<c:set var="selected" value="active"/>
				</c:if>
			</c:otherwise>
		</c:choose>

		<li class="navigation-link-item">
			<cms:component component="${link}" evaluateRestriction="true" class="${selected}"/>
		</li>
	</c:forEach>
</c:if>
