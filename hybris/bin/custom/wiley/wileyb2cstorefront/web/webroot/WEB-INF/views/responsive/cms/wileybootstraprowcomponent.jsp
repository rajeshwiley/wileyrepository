<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="bootstrapgrid" tagdir="/WEB-INF/tags/responsive/bootstrapgrid" %>
<c:choose>
    <c:when test="${not isBootstrapRowNested}">
        <c:set var="isBootstrapRowNested" value="true"/>

        <div class="${fluidContainer ? 'container-fluid' : 'container'}">
            <bootstrapgrid:row cssClasses="${cssClasses}" cssStyle="${cssStyle}" columns="${columns}"/>
        </div>

        <c:remove var="isBootstrapRowNested"/>
    </c:when>
    <c:otherwise>
        <bootstrapgrid:row cssClasses="${cssClasses}" cssStyle="${cssStyle}" columns="${columns}"/>
    </c:otherwise>
</c:choose>