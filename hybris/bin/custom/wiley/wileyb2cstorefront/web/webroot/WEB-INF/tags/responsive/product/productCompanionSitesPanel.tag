<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:if test="${not empty product.instructorLinks or not empty product.studentLinks}">
    <section id="companion-sites-section" class="page-section companion-sites">
        <h2 class="section-title"><spring:theme code="product.companionSiteLink.header"/></h2>
        <div>
            <c:if test="${not empty product.instructorLinks}">
                <div class="companion-sites-block">
                    <h5 class="section-sub-title"><spring:theme code="product.companionSiteLink.instructor.header"/></h5>
                    <c:forEach items="${product.instructorLinks}" var="link">
                        <p><spring:theme code="product.companionSiteLink.${link.type}" arguments="${link.url}"/></p>
                    </c:forEach>
                </div>
            </c:if>
            <c:if test="${not empty product.studentLinks}">
                <div class="companion-sites-block">
                    <h5 class="section-sub-title"><spring:theme code="product.companionSiteLink.student.header"/></h5>
                    <c:forEach items="${product.studentLinks}" var="link">
                        <p><spring:theme code="product.companionSiteLink.${link.type}" arguments="${link.url}"/></p>
                    </c:forEach>
                </div>
            </c:if>
        </div>
    </section>
</c:if>