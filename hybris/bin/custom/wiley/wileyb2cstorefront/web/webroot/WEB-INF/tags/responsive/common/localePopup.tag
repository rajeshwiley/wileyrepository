<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:url value="/_s/country" var="setCountryActionUrl" />
<spring:theme code="locale.selector.header" var="windowHeader" />
<spring:theme code="locale.selector.cancel" var="cancel" />
<spring:theme code="locale.selector.confirm" var="confirm" />
<spring:theme code="locale.selector.info.${localePopupData.currentCountryGeodetected?'geolocationDetected':'cookieDetected'}" 
	arguments="${localePopupData.detectedCountry.name},${localePopupData.currentCountry.name}" var="info" />

<div id="selectCountryModalWnd" role="dialog" class="modal fade modalWindow"
	data-cookie-country-cookie-name="${localePopupData.cookieCountryCookieName}"
	data-cookie-country-cookie-path="${localePopupData.cookieCountryCookiePath}"
	data-cookie-country-cookie-max-age="${localePopupData.cookieCountryCookieMaxAge}"
	data-current-locale="${localePopupData.currentLocale}"
	data-detected-locale="${localePopupData.detectedLocale}">
    <div class="modal-dialog">
        <div class="modal-content">
            <form:form class="country-location-form" id="country-location-form" action="${setCountryActionUrl}" method="post">
            	<input class="selected-country-input" type="hidden" name="code" value="${localePopupData.detectedCountry.isocode}"/>
            	<input type="hidden" name="addLocaleCookie" value="false"/>
                <div class="modal-header">
                    <div class="modal-title">${windowHeader}</div>
                    <button type="button" data-dismiss="modal" aria-label="Close" class="close">
                    	<span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="modal-wnd-list">
                        <p class="info">${info}</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="changeLocationCancelBtn button large button-dark-gray">${cancel}</button>
                    <button type="button" class="changeLocationConfirmBtn button large button-teal">${confirm}</button>
                </div>
            </form:form>
        </div>
    </div>
</div>