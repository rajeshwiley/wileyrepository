<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<template:page pageTitle="${pageTitle}" containerCSSClass="log-in wrap authorization-support-message-wr">

		<div class="row">
            <div class="col-xs-12 col-sm-12">
                <div class="support-message">
                    <span><spring:theme code="login.needHelp"/></span>
                    <spring:theme code="login.contactUs"/>
                </div>
            </div>
			<div class="col-xs-12 col-sm-6 left-block">
                <div class="row">
                    <div class="col-xs-12 col-sm-8 col-sm-offset-4 wrap">
                        <cms:pageSlot position="LeftContentSlot" var="feature">
                            <cms:component component="${feature}" />
                        </cms:pageSlot>
                    </div>
                </div>
			</div>
			<div class="col-xs-12 col-sm-6">
                <div class="row">
                    <div class="col-xs-12 col-sm-8">
                        <cms:pageSlot position="RightContentSlot" var="feature">
                            <cms:component component="${feature}" />
                        </cms:pageSlot>
                    </div>
                </div>
			</div>
		</div>

</template:page>
