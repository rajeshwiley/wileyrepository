<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@ attribute name="subscriptionCode" required="true" type="java.lang.String"%>
<%@ attribute name="deliveryAddresses" required="true" type="java.util.List"%>

<spring:url value="/my-account/subscriptions/updateDeliveryAddress" var="updateSubscriptionDeliveryAddressUrl"/>

<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<div class="row">
				<div class="col-xs-10 col-sm-10">
					<div class="modal-title">
						<spring:theme code="account.subscription.manageSubscriptions.addressBook" text="Address Book"/>
					</div>
				</div>
				<div class="col-xs-2 col-sm-2">
					<button type="button" data-dismiss="modal" aria-label="Close" class="close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			</div>
		</div>
		<div class="modal-body">
			<div class="modal-wnd-list">
				<c:forEach items="${deliveryAddresses}" var="deliveryAddress" varStatus="status">
					<form:form action="${updateSubscriptionDeliveryAddressUrl}" method="POST">
						<input type="hidden" name="subscriptionCode" value="${subscriptionCode}"/>
						<input type="hidden" name="selectedDeliveryAddressCode" value="${deliveryAddress.id}"/>
                        <div ${!status.last ? 'class="modal-wnd-list-item"' : 'class="modal-wnd-list-item-last"'}>
							<div class="row">
								<div class="col-xs-12 col-sm-8">
									<%-- <div class="modal-wnd-list-title"/> --%>
									<div class="modal-wnd-list-description">
										<c:if test="${not empty fn:escapeXml(deliveryAddress.firstName) or not empty fn:escapeXml(deliveryAddress.lastName)}">
											<p>
												<c:if test="${not empty fn:escapeXml(deliveryAddress.title)}">
													${fn:escapeXml(deliveryAddress.title)} ${' '}
												</c:if>
												${fn:escapeXml(deliveryAddress.firstName)} ${' '}
												${fn:escapeXml(deliveryAddress.lastName)}
											</p>
										</c:if>
										<p>${fn:escapeXml(deliveryAddress.line1)}</p>
										<c:if test="${not empty fn:escapeXml(deliveryAddress.line2)}">
											<p>${fn:escapeXml(deliveryAddress.line2)}</p>
										</c:if>
										<p>
											${fn:escapeXml(deliveryAddress.town)}
											<c:if test="${not empty fn:escapeXml(deliveryAddress.region.name)}">
												<c:if test="${not empty fn:escapeXml(deliveryAddress.town)}">
													${", "}
												</c:if>
												${fn:escapeXml(deliveryAddress.region.name)}
											</c:if>
										</p>
										<p>
											${fn:escapeXml(deliveryAddress.country.name)}
											${' '}
											${fn:escapeXml(deliveryAddress.postalCode)}
										</p>
									</div>
								</div>
								<div class="col-xs-12 col-sm-4">
									<div class="modal-wnd-item-button">
										<button type="submit" class="button button-main small">
											<spring:theme code="" text="Use this Address"/>
										</button>
									</div>
								</div>
							</div>
						</div>
					</form:form>
				</c:forEach>
			</div>
		</div>
	</div>
</div>