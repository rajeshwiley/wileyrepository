<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ attribute name="paymentInfoData" required="true" type="de.hybris.platform.commercefacades.order.data.CCPaymentInfoData" %>

<c:if test="${not empty paymentInfoData}">
    <div class="payment-details">
        <div class="payment-details-title"><spring:theme code="account.subscription.manageSubscriptions.paymentDetailsSectionTitle"/></div>
        <div>${paymentInfoData.accountHolderName}</div>
        <div>${paymentInfoData.cardNumber}</div>
        <c:if test="${not empty paymentInfoData.expiryMonth && not empty paymentInfoData.expiryYear}">
            <div>${paymentInfoData.expiryMonth}/${paymentInfoData.expiryYear}</div>
        </c:if>
    </div>
</c:if>
