<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>
<%@ taglib prefix="storepickup" tagdir="/WEB-INF/tags/responsive/storepickup" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>

<template:page pageTitle="${pageTitle}" containerCSSClass="container-fluid product-listing-page" clearContainerClass="true">
    <div class="row">

        <div class="page-content">
        <div class="container product-list-container">
        <c:if test="${not empty searchPageData.results}">
            <cms:pageSlot position="ProductLeftRefinements" var="feature">
                <cms:component component="${feature}"/>
            </cms:pageSlot>
        </c:if>
        <div>
            <div class="page-header">
                <div class="container">
                <cms:pageSlot position="Breadcrumb" var="comp" element="div">
                    <cms:component component="${comp}"/>
                </cms:pageSlot>
                <h1>${categoryName}</h1>
                </div>
                <cms:pageSlot position="MarketingInformation" var="feature">
                    <cms:component component="${feature}"/>
                </cms:pageSlot>
            </div>
            <cms:pageSlot position="ProductListSlot" var="feature">
                <cms:component component="${feature}"/>
            </cms:pageSlot>
        </div>

        </div>
        </div>
    </div>
</template:page>