<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>
<%@ taglib prefix="storepickup" tagdir="/WEB-INF/tags/responsive/storepickup" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<template:page pageTitle="${pageTitle}" containerCSSClass="container-fluid" clearContainerClass="true">
    <div class="row">
        <div class="category-product-list-page">
            <section class="container">
                <cms:pageSlot position="Breadcrumb" var="comp">
                    <cms:component component="${comp}"/>
                </cms:pageSlot>
                <cms:pageSlot position="TopContent" var="feature">
                    <cms:component component="${feature}"/>
                </cms:pageSlot>
            </section>
            <section class="container">
                <cms:pageSlot position="ProductListSlot" var="feature">
                    <cms:component component="${feature}"/>
                </cms:pageSlot>
            </section>
            <section class="container">
                <cms:pageSlot position="BottomContent" var="feature">
                    <cms:component component="${feature}"/>
                </cms:pageSlot>
            </section>
            <section class="related-series">
                <div class="container">
                    <cms:pageSlot position="RelatedCategoriesTitle" var="feature">
                        <cms:component component="${feature}"/>
                    </cms:pageSlot>
                    <div class="links">
                        <cms:pageSlot position="RelatedCategoriesBody" var="feature">
                            <cms:component component="${feature}"/>
                        </cms:pageSlot>
                    </div>
                </div>
            </section>
        </div>
    </div>
</template:page>
