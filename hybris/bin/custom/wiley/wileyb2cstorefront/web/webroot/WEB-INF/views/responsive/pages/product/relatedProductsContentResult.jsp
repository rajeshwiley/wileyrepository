<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="component" tagdir="/WEB-INF/tags/shared/component"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:set var="title">
	<spring:theme code="continue.shopping.popup.product.carousel.title" text="You may also like"/>
</c:set>
<c:set var="relatedProducts">
	<c:choose>
		<c:when test="${not empty productReferences and maximumNumberProducts > 0}">
			<div class="modal-wnd-carousel carousel-default carousel-modal">
				<product:productReferences
					productReferences="${productReferences}"
					title="${title}"
					maximumNumberProducts="${maximumNumberProducts}"
					displayProductTitles="${displayProductTitles}"
					displayProductAuthors="${displayProductAuthors}"
					displayProductPrices="${displayProductPrices}" />
			</div>
		</c:when>
		<c:otherwise>
			<component:emptyComponent/>
		</c:otherwise>
</c:choose>
</c:set>
<elements>
    <element>
        <key><c:out value="addToCartModalWnd .modal-wnd-related-products"/></key>
        <value><c:out value="${relatedProducts}" escapeXml="true"/></value>
    </element>
</elements>