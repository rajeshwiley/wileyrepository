<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ taglib prefix="account" tagdir="/WEB-INF/tags/responsive/account"%>
<%@ taglib prefix="wileycomNav" tagdir="/WEB-INF/tags/addons/wileycomstorefrontcommons/responsive/nav"%>

<c:set var="searchUrl" value="/my-account/subscriptions?"/>

<h1 class="full-width-gray">
    <spring:theme code="account.subscription.manageSubscriptions.pageTitle"/>
</h1>

<c:choose>
    <c:when test="${fn:length(searchPageData.results) > 0}">
        <div class="product-list subscription-product-list my-account-wrap wrap">
            <c:forEach items="${searchPageData.results}" var="subscription">
                <account:subscriptionProductListItem wileySubscriptionData="${subscription}" />
            </c:forEach>
            <div class="row">
                <div class="col-sm-12">
                    <wileycomNav:pagination searchPageData="${searchPageData}" searchUrl="${searchUrl}" />
                </div>
            </div>
        </div>
    </c:when>
    <c:otherwise>
        <div class="row">
            <div class="col-sm-12">
                <p class="no-billing-found">
                    <spring:theme code="account.subscription.manageSubscriptions.noSubscriptionsLabel" />
                </p>
            </div>
        </div>
    </c:otherwise>
</c:choose>

