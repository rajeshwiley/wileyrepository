<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>

<template:page pageTitle="${pageTitle}" containerCSSClass="container-fluid" clearContainerClass="true">
    <div class="row">
        <div class="category-landing-page">
            <div class="container">
                <cms:pageSlot position="Breadcrumb" var="comp">
                    <cms:component component="${comp}"/>
                </cms:pageSlot>
            </div>

            <div class="hero-banner">
                <cms:pageSlot position="TopContentBackground" var="comp">
                    <cms:component component="${comp}" class="hero-banner-component" element="div"/>
                </cms:pageSlot>
                <div class="container">
                    <article class="section-description page-section">
                        <cms:pageSlot position="TopContent" var="comp">
                            <cms:component component="${comp}"/>
                        </cms:pageSlot>
                        <div class="section-wrapper">
                            <c:set var="sectionHasComponent" value="false"/>
                            <cms:pageSlot position="TopLeftContent" var="comp">
                                <c:if test="${not empty comp}">
                                    <c:set var="sectionHasComponent" value="true"/>
                                </c:if>
                            </cms:pageSlot>
                            <div class="section-left <c:if test='${sectionHasComponent}'>section-video</c:if>">
                                <cms:pageSlot position="TopLeftContent" var="comp">
                                    <cms:component component="${comp}"/>
                                </cms:pageSlot>
                            </div>
                            <div class="section-right">
                                <cms:pageSlot position="TopRightContent" var="comp">
                                    <cms:component component="${comp}"/>
                                </cms:pageSlot>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
            <div class="container-fluid gray-bg">
                <section class="container featured-products">
                    <cms:pageSlot position="FeaturedContent" var="comp">
                        <cms:component component="${comp}"/>
                    </cms:pageSlot>
                </section>
            </div>
            <div class="container container-with-panel">
                <div class="side-panel">
                    <header>
                        <cms:pageSlot position="SubjectContent" var="comp">
                            <cms:component component="${comp}"/>
                        </cms:pageSlot>
                    </header>
                    <ul>
                        <c:forEach items="${subCategories}" var="category">
                            <li><a href="<c:url value="${category.url}"/>">${category.name}</a></li>
                        </c:forEach>
                    </ul>
                    <span class="link-view-more left"><a class="link-corner" href="${plpCustomLink}">
			            <spring:theme code="category.viewall" text="View all"/></a>
			        </span>
                </div>
                <div class="content-panel">
                    <div class="dropdown dropdown-component facets-dropdown-component visible-xs">
                        <button id="dropdownMenuButton" type="button" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false" class="dropdown-toggle">Dropdown button
                        </button>
                        <div aria-labelledby="dropdownMenuButton" class="dropdown-menu"></div>
                    </div>
                    <section class="preview-articles">
                        <cms:pageSlot position="MiddleContent1" var="comp">
                            <cms:component component="${comp}"/>
                        </cms:pageSlot>
                        <cms:pageSlot position="MiddleContent2" var="comp">
                            <cms:component component="${comp}"/>
                        </cms:pageSlot>
                        <cms:pageSlot position="MiddleContent3" var="comp">
                            <cms:component component="${comp}"/>
                        </cms:pageSlot>
                    </section>
                    <section class="products-preview-block">
                        <cms:pageSlot position="MiddleContent4TwoColumns" var="comp">
                            <cms:component component="${comp}"/>
                        </cms:pageSlot>
                    </section>
                    <section class="preview-articles">
                        <cms:pageSlot position="MiddleContent5" var="comp">
                            <cms:component component="${comp}"/>
                        </cms:pageSlot>
                    </section>
                    <section class="products-preview-block">
                        <cms:pageSlot position="MiddleContent6TwoColumns" var="comp">
                            <cms:component component="${comp}"/>
                        </cms:pageSlot>
                    </section>
                </div>
            </div>
            <div class="related-series">
                <div class="container">
                    <cms:pageSlot position="RelatedCategoriesTitle" var="feature">
                        <cms:component component="${feature}"/>
                    </cms:pageSlot>
                    <div class="links">
                        <cms:pageSlot position="RelatedCategoriesBody" var="feature">
                            <cms:component component="${feature}"/>
                        </cms:pageSlot>
                    </div>
                </div>
            </div>
        </div>
    </div>
</template:page>