<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="user" tagdir="/WEB-INF/tags/responsive/user"%>

<c:url value="/paypal/j_spring_security_check" var="loginActionUrl" />

<user:login actionNameKey="login.login" action="${loginActionUrl}" />
