<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true" %>
<%@ attribute name="pageTitle" required="false" rtexprvalue="true" %>
<%@ attribute name="metaDescription" required="false" %>
<%@ attribute name="metaKeywords" required="false" %>
<%@ attribute name="pageCss" required="false" fragment="true" %>
<%@ attribute name="pageScripts" required="false" fragment="true" %>

<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="analytics" tagdir="/WEB-INF/tags/shared/analytics" %>
<%@ taglib prefix="generatedVariables" tagdir="/WEB-INF/tags/shared/variables" %>
<%@ taglib prefix="debug" tagdir="/WEB-INF/tags/shared/debug" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="htmlmeta" uri="http://hybris.com/tld/htmlmeta"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>

<!DOCTYPE html>
<c:set var="prefixesToHead" value=""/>
<c:forEach items="${prefixes}" var="prefix">
	<c:set var="prefixesToHead" value="${prefixesToHead}${prefix.key}: ${prefix.value} "/>
</c:forEach>
<html lang="${currentLocale}"
	<c:if test="${not empty prefixes}">
		prefix="${prefixesToHead}"
	</c:if>>
<head>
	<title>
		${not empty pageTitle ? pageTitle : not empty cmsPage.title ? cmsPage.title : 'Wiley'}
	</title>
	<c:forEach items="${openGraphTags}" var="tag">
		<meta property="${tag.name}" content="${tag.content}" />
	</c:forEach>

	<%-- Meta Content --%>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width">
	<meta name="format-detection" content="telephone=no">
	<c:forEach items="${regionalUrls}" var="regionalUrl">
		<c:url value="/" var="siteRootUrl"/>
		<link rel="alternate" hreflang="${regionalUrl.locale}" href="${regionalUrl.url}" />
	</c:forEach>


	<%-- Additional meta tags --%>
	<htmlmeta:meta items="${metatags}"/>

	<%-- Favourite Icon --%>
	<spring:theme code="img.favIcon" text="/" var="favIconPath"/>
	<link rel="shortcut icon" type="image/x-icon" media="all" href="${contextPath}${favIconPath}" />

	<%-- CSS Files Are Loaded First as they can be downloaded in parallel --%>
	<template:styleSheets/>

	<%-- Inject any additional CSS required by the page --%>
	<jsp:invoke fragment="pageCss"/>
	<analytics:analytics/>
	<generatedVariables:generatedVariables/>

	<c:if test="${not empty jsonLdScript}">
		<script type="application/ld+json">${jsonLdScript}</script>
	</c:if>
</head>

<body class="${pageBodyCssClasses} ${cmsPageRequestContextData.liveEdit ? ' yCmsLiveEdit' : ''} language-${currentLanguage.isocode}">

	<%-- Inject the page body here --%>
	<jsp:doBody/>


	<form name="accessiblityForm">
		<input type="hidden" id="accesibility_refreshScreenReaderBufferField" name="accesibility_refreshScreenReaderBufferField" value=""/>
	</form>
	<div id="ariaStatusMsg" class="skip" role="status" aria-relevant="text" aria-live="polite"></div>

	<%-- Load JavaScript required by the site --%>
	<template:javaScript/>
	
	<%-- Inject any additional JavaScript required by the page --%>
	<jsp:invoke fragment="pageScripts"/>	

	<analytics:analyticsBottom />
	<cms:pageSlot position="ClosingBodyPlaceholderSlot" var="feature">
		<cms:component component="${feature}"/>
	</cms:pageSlot>
</body>

<debug:debugFooter/>

</html>
