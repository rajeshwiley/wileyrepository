<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>

<template:master pageTitle="${pageTitle}">

	<cms:pageSlot position="MainSlot" var="comp" element="div">
			<cms:component component="${comp}"/>
	</cms:pageSlot>

</template:master>

