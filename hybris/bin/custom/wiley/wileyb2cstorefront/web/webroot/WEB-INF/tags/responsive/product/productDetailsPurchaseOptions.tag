<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:set var="activeVariantProduct" value="${product.productPurchaseOption}"/>
<c:set var="wileyPlusProductExtStore"/>

<div class="product-type">
    <div class="type-of-book-chooser">
        <c:forEach items="${product.variantOptions}" var="option">
            <c:if test="${option.hasExternalStores}">
                <c:forEach items="${option.externalStores}" var="externalStore">
                    <c:if test="${externalStore.type eq 'WILEY_PLUS_STORE' and option.purchaseOptionName eq 'WileyPLUS'}">
                        <c:set var="title"><spring:theme code="product.button.wiley.web.link.${externalStore.type}"/></c:set>
                        <c:set var="wileyPlusProductExtStore">
                            <a href="${externalStore.url}" title="${title}" target="_blank" class="item-wr item-wr-link">
                                <div class="item">
                                    <span class="item-type">${title}</span>
                                </div>
                            </a>
                        </c:set>
                    </c:if>
                </c:forEach>
            </c:if>
            <c:if test="${option.purchaseOptionName != 'WileyPLUS'}" >
                <c:set var="activeOption" value="${option.code eq product.productPurchaseOption.code ? 'active' : 'option'}" />
                <c:url var="optionUrl" value="${option.url}" />
                <a class="item-wr ${activeOption}" href="${optionUrl}">
                    <div class="item">
                        <c:if test="${not option.hasExternalStores && not empty option.inventoryStatus && option.inventoryStatus.visible}">
                            <span class="item-stock">
                                <spring:theme code="product.inventoryStatus.${option.inventoryStatus.statusCode}"/>
                            </span>
                        </c:if>
                        <span class="item-type">${option.purchaseOptionName}
                            <c:if test="${not empty option.purchaseOptionDescription}">
                                <i role="menuitem" tabindex="0" aria-label="Purchase option description"
                                   data-content="${fn:escapeXml(option.purchaseOptionDescription)}"
                                   class="icon-info black" data-original-title="" title=""></i>
                            </c:if>
                        </span>
                        <c:if test="${not option.hidePrice}">
                            <span class="item-price">
                                    ${option.priceData.formattedValue}
                            </span>
                        </c:if>
                    </div>
                </a>
            </c:if>
        </c:forEach>
        <c:if test="${not empty wileyPlusProductExtStore}">
            ${wileyPlusProductExtStore}
        </c:if>
        <c:if test="${activeVariantProduct.hasExternalStores}">
            <c:forEach items="${activeVariantProduct.externalStores}" var="externalStore">
                <c:if test="${externalStore.type eq 'WOL_STORE' or externalStore.type eq 'REQUEST_QUOTE'
                        or (externalStore.type eq 'WILEY_PLUS_STORE' and activeVariantProduct.purchaseOptionName != 'WileyPLUS')}">
                    <c:set var="title"><spring:theme code="product.button.wiley.web.link.${externalStore.type}"/></c:set>
                    <a href="${externalStore.url}" title="${title}" target="_blank" class="item-wr item-wr-link">
                        <div class="item">
                            <span class="item-type">${title}</span>
                        </div>
                    </a>
                </c:if>
            </c:forEach>
        </c:if>
    </div>
</div>