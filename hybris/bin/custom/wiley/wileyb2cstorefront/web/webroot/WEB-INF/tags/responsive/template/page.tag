<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true"%>
<%@ attribute name="pageTitle" required="false" rtexprvalue="true"%>
<%@ attribute name="pageCss" required="false" fragment="true"%>
<%@ attribute name="pageScripts" required="false" fragment="true"%>
<%@ attribute name="hideHeaderLinks" required="false"%>
<%@ attribute name="containerCSSClass" required="false"%>
<%@ attribute name="clearContainerClass" required="false"%>

<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/responsive/common/header"%>
<%@ taglib prefix="footer" tagdir="/WEB-INF/tags/responsive/common/footer"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>

<template:master pageTitle="${pageTitle}">

	<jsp:attribute name="pageCss">
		<jsp:invoke fragment="pageCss" />
	</jsp:attribute>

	<jsp:attribute name="pageScripts">
		<jsp:invoke fragment="pageScripts" />
	</jsp:attribute>

	<jsp:body>
        <!-- Skip to content link -->
        <a class="skip-to-content-link" href="#maincontent" tabindex="1">Skip to main content</a>
        <!-- END Skip to content link -->
        <main data-currency-iso-code="${currentCurrency.isocode}">

			<header:header hideHeaderLinks="${hideHeaderLinks}" />
			<common:localePopup />
			<div class="main-page-container ${clearContainerClass ? '' : 'container '} ${containerCSSClass}">
                <!-- Skip to content link -->
                <a name="maincontent" id="maincontent"></a>
                <!-- END Skip to content link -->
				<common:globalMessages />
				<cart:cartRestoration />
				<jsp:doBody />
			</div>

			<footer:footer />
		</main>

	</jsp:body>

</template:master>
