<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>

<%@ attribute name="sectionId" %>
<%@ attribute name="productReferences" required="true" type="java.util.List" %>
<%@ attribute name="title" required="true" %>
<%@ attribute name="maximumNumberProducts" required="true" type="java.lang.Integer" %>
<%@ attribute name="displayProductTitles" required="true" type="java.lang.Boolean" %>
<%@ attribute name="displayProductAuthors" required="true" type="java.lang.Boolean" %>
<%@ attribute name="displayProductPrices" required="true" type="java.lang.Boolean" %>

<section id="${sectionId}" class="page-section related-products-section">
    <h4>${title}</h4>
    <div class="carousel-gallery">
        <div class="carousel">
            <div class="owl-carousel related-products-carousel">
                <c:forEach end="${maximumNumberProducts}" items="${productReferences}" var="productReference">
                    <c:url value="${productReference.target.url}" var="productUrl"/>
                    <div class="item">
                        <a ${productReference.target.showPdpUrl ? 'href="'.concat(productUrl).concat('"') : ''} class="item-link">
                            <span class="item-image-wrapper">
                                <product:productPrimaryReferenceImage product="${productReference.target}" format="product"/>
                            </span>
                            <c:if test="${displayProductTitles}">
                                <div class="item-title">${productReference.target.name}</div>
                            </c:if>
                        </a>
                        <c:if test="${displayProductAuthors}">
                            <p class="item-author">
                                    ${productReference.target.authors}
                            </p>
                        </c:if>
                        <c:if test="${displayProductPrices}">
                            <div class="priceContainer">
                                <format:fromPrice priceData="${productReference.target.price}"/>
                            </div>
                        </c:if>
                    </div>
                </c:forEach>
            </div>
        </div>
    </div>
</section>