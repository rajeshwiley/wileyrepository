<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>

<c:set var="errorMessageVar"><spring:theme code="updatePwd.pwd.invalid" /></c:set>
<h1 class="full-width-gray"><spring:theme code="text.account.profile.updatePasswordForm"/></h1>
<div class="update-password my-account-wrap">
	<div class="row">
		<div class="col-xs-12 col-sm-4">
			<div class="site-form">
				<form:form action="${action}" method="post" commandName="updatePasswordForm">
					<formElement:formPasswordBox idKey="currentPassword"
						labelKey="profile.currentPassword" path="currentPassword"
						mandatory="true" />
					<formElement:formPasswordBox idKey="newPassword"
						labelKey="profile.newPassword" path="newPassword"
						mandatory="true" errorMessage="${errorMessageVar}" />
					<formElement:formPasswordBox idKey="checkNewPassword"
						labelKey="profile.checkNewPassword" path="checkNewPassword"
						mandatory="true" />
					<div class="my-account-button-wrap">
						<div class="row">
							<div class="col-xs-12 col-sm-6">
								<button type="submit" class="button button-main large" id="updatePassword">
									<spring:theme code="updatePwd.submit" text="Update Password" />
								</button>
							</div>
							<div class="col-xs-12 col-sm-6">
								<button type="button" class="button button-outlined large backToHome">
									<spring:theme code="text.button.cancel" text="Cancel" />
								</button>
							</div>
						</div>
					</div>
				</form:form>
			</div>
		</div>
	</div>
</div>
