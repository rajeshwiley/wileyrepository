<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="hideHeaderLinks" required="false" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/responsive/common/header" %>

<cms:pageSlot position="TopHeaderSlot" var="component" element="div" class="container">
    <cms:component component="${component}"/>
</cms:pageSlot>

<c:set var="textHeaderMyAccount">
	<spring:theme code="text.header.my.account"/>
</c:set>

<header id="main-header-container" class="main-header-container">
    <div class="main-sticky-header">
        <div class="main-header-navigation">
            <div class="main-navigation-mobile-header">
                <cms:pageSlot position="SiteLogo" var="logo" limit="1" element="div" class="logo">
                    <cms:component component="${logo}"/>
                </cms:pageSlot>
                <span type="button" class="navbar-toggle collapsed visible-xs-inline-block">
                    <span class="patty"></span>
                </span>
                <span class="main-navigation-search-icon icon-search-black visible-xs-inline-block"></span>
                <span class="my-cart-link visible-xs-inline-block">
                    <a href="${legacyViewCartLink}" title="<spring:theme code="text.header.cart.link"/>"
                       class="shopping-cart-icon">Shopping cart</a>
                </span>
            </div>
            <nav id="main-header-navbar" role="navigation" class="collapse navbar-collapse navbar">
                <ul class="navigation-menu-items">
                    <cms:pageSlot position="NavigationBar" var="navBar" limit="1">
                        <cms:component component="${navBar}"/>
                    </cms:pageSlot>
                </ul>
                <cms:pageSlot position="CountrySelector" var="cmp">
                    <cms:component component="${cmp}"/>
                </cms:pageSlot>
                <ul class="navigation-links-items">
                    <cms:pageSlot position="HeaderLinks" var="link">
                        <cms:component component="${link}"/>
                    </cms:pageSlot>
                    <li class="navigation-link-item sitemap-link visible-xs">
																								<a href="${sitemapLinkUrl}" title="<spring:theme code="text.header.sitemap.link"/>">
																									<spring:theme code="text.header.sitemap.link"/>
																								</a>
																				</li>
                    <li class="navigation-link-item my-account-link">
                        <a href="${legacyMyAccountLink}" title="${textHeaderMyAccount}">${textHeaderMyAccount}</a>
                    </li>
                    <li class="navigation-link-item my-cart-link hidden-xs">
                        <a href="${legacyViewCartLink}" title="<spring:theme code="text.header.cart.link"/>"
                           class="shopping-cart-icon">Shopping Cart</a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="main-navigation-search">
            <div class="container">
                <cms:pageSlot position="SearchBox" var="component">
                    <cms:component component="${component}"/>
                </cms:pageSlot>
            </div>
        </div>
    </div>
</header>
