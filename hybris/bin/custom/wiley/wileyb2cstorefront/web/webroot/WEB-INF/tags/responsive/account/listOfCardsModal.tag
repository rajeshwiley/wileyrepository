<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="paymentInfos" required="true" type="java.util.List" %>
<%@ attribute name="chooseUrl" required="true" type="java.lang.String" %>
<%@ attribute name="dialogId" required="true" type="java.lang.String" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

	<c:url var="chooseCardUrl" value="${chooseUrl}"/>

	<div id="${dialogId}" role="dialog" class="modal fade modalWindow">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<div class="row">
						<div class="col-xs-10 col-sm-10">
							<div class="modal-title">
								<spring:theme code="checkout.multi.paymentMethod.addPaymentDetails.savedCards" />
							</div>
						</div>
						<div class="col-xs-2 col-sm-2">
							<button type="button" data-dismiss="modal" aria-label="Close" class="close">
							    <span aria-hidden="true">&times;</span>
							</button>
						</div>
					</div>
				</div>
				<div class="modal-body">
					<div class="modal-wnd-list">
						<c:forEach items="${paymentInfos}" var="paymentInfo" varStatus="status">
							<div ${!status.last ? 'class="modal-wnd-list-item"' : 'class="modal-wnd-list-item-last"'}>
								<div class="row">
									<form:form action="${chooseCardUrl}" method="POST">
										<div class="col-xs-12 col-sm-8">
											<input type="hidden" name="selectedPaymentMethodId" value="${paymentInfo.id}" />
											<div class="modal-wnd-list-title">
												<p>${fn:escapeXml(paymentInfo.billingAddress.firstName)}&nbsp; ${fn:escapeXml(paymentInfo.billingAddress.lastName)}</p>
											</div>
											<div class="modal-wnd-list-description">
												<p>${fn:escapeXml(paymentInfo.cardType)}</p>
												<p>${fn:escapeXml(paymentInfo.accountHolderName)}
												<p>************${fn:escapeXml(paymentInfo.cardNumber)}</p>
												<p>
													<spring:theme code="checkout.multi.paymentMethod.paymentDetails.expires" arguments="${fn:escapeXml(paymentInfo.expiryMonth)},${fn:escapeXml(paymentInfo.expiryYear)}" />
												</p>
												<c:if test="${not empty paymentInfo.billingAddress}">
												<p>${fn:escapeXml(paymentInfo.billingAddress.line1)}</p>
												<p>${fn:escapeXml(paymentInfo.billingAddress.town)}&nbsp; ${fn:escapeXml(paymentInfo.billingAddress.region.isocodeShort)}</p>
												<p>${fn:escapeXml(paymentInfo.billingAddress.postalCode)}&nbsp; ${fn:escapeXml(paymentInfo.billingAddress.country.isocode)}</p>
												</c:if>
											</div>
										</div>
										<div class="col-xs-12 col-sm-4">
											<div class="modal-wnd-item-button">
												<button type="submit" class="button button-main small" tabindex="${(status.count * 2) - 1}">
													<spring:theme code="checkout.multi.paymentMethod.addPaymentDetails.useThesePaymentDetails" />
												</button>
											</div>
										</div>
									</form:form>
								</div>
							</div>
						</c:forEach>
					</div>
				</div>
			</div>
		</div>
	</div>
