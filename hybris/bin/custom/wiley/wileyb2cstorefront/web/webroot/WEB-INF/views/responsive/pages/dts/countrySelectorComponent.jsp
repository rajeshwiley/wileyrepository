<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:url value="/_s/country" var="setCountryActionUrl" />

<span aria-label="Select country" class="main-navigation-select-country">
    <span tabindex="0" class="country-selected trigger-country-dropdown" title="selected country with dropdown">
        <i class="country-icon sprite sprite-${currentCountry.isocode}"></i><span>${currentCountry.name}</span>
    </span>
    <div class="select-country-dropdown">
        <span class="country-selected visible-xs-inline-block">
            <i class="country-icon sprite sprite-${currentCountry.isocode}"></i>
            <span>${currentCountry.name}</span>
        </span>
          <input placeholder="Type your Country" class="country-search-input" aria-label="Type your Country">
          <form:form id="country-form" action="${setCountryActionUrl}" method="post">
            <input id="selected-country-input" type="hidden" name="code">
           	<input id="page-referer-input" type="hidden" name="pageReferer" value="${sessionScope.originalReferer}"/>
            <ul role="menu"></ul>
          </form:form>
    </div>
</span>