<%@ page trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<c:url value="/search" var="searchUrl"/>
<c:url value="/search/autocomplete/${component.uid}" var="autocompleteUrl"/>

<form name="search_form_${component.uid}" method="get" action="${searchUrl}" autocomplete="off">
    <div class="input-group">
        <spring:theme code="search.placeholder" var="searchPlaceholder"/>
        <spring:theme code="search.button" var="searchButton"/>
        <ycommerce:testId code="header_search_input">
            <input
                    type="search"
                    id="js-site-search-input"
                    aria-label="site search"
                    class="form-control main-navigation-search-input js-site-search-input"
                    name="pq"
                    value="<c:if test='${not empty searchText}'>${searchText}</c:if>"
                    maxlength="100"
                    placeholder="${searchPlaceholder}"
                    data-options='{"autocompleteUrl" : "${autocompleteUrl}","minCharactersBeforeRequest" : "${component.minCharactersBeforeRequest}","waitTimeBeforeRequest" : "${component.waitTimeBeforeRequest}","displayProductImages" : ${component.displayProductImages}}'>
        </ycommerce:testId>

        <span class="input-group-btn">
            <button type="submit">${searchButton}</button>
        </span>
    </div>
</form>
<aside class="main-navigation-search-autocomplete-template" style="display: none">
    <section class="searchresults-section search-related-content suggestions">
        <h3><spring:theme code="search.suggestions"/></h3>
        <div class="search-list"></div>
    </section>
    <section class="searchresults-section related-content-products-section">
        <h3><spring:theme code="search.products"/></h3>
        <div class="related-content-products"></div>
        <span class="link-view-more-products">
            <a class="all-results-link link-corner-products"><spring:theme code="search.see.all.results"/></a>
        </span>
    </section>
    <section class="searchresults-section related-content-other-section">
        <h3><spring:theme code="search.other"/></h3>
        <div class="related-content-other"></div>
        <span class="link-view-more-other">
            <a class="all-results-link link-corner-other"><spring:theme code="search.see.all.results"/></a>
        </span>
    </section>
</aside>