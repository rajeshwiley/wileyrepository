<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="subscriptionData" required="true" type="com.wiley.facades.product.data.WileySubscriptionData" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:url var="upgradeOptionsUrl" value="/cart/add/subscription/${subscriptionData.code}"/>

<div id="subscriptionOptionsModal" role="dialog" class="modal fade modalWindow">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<div class="row">
					<div class="col-xs-10 col-sm-10">
						<div class="modal-title">
							<spring:theme code="checkout.multi.paymentMethod.addPaymentDetails.upgradeOptions" />
						</div>
					</div>
					<div class="col-xs-2 col-sm-2">
						<button type="button" data-dismiss="modal" aria-label="Close" class="close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				</div>
			</div>
			<div class="modal-body">
				<div class="modal-wnd-title"><h3>${subscriptionData.name}</h3></div>

				<div class="modal-wnd-list">

                    <%--Current Subscription Block--%>
						<div class="modal-wnd-list-item">
							<div class="row">
								<div class="col-xs-12 col-sm-8">
									<div class="modal-wnd-list-description">
										<c:if test="${!empty subscriptionData.billingPriceCurrency && !empty subscriptionData.billingPriceValue}">
											<p><spring:theme code="account.subscription.manageSubscriptions.priceTitle"/>: ${subscriptionData.billingPriceCurrency} ${subscriptionData.billingPriceValue}/${subscriptionData.billingFrequency.name}</p>
										</c:if>
										<c:if test="${!empty subscriptionData.billingFrequency || !empty subscriptionData.billingFrequency.name}">
											<p><spring:theme code="account.subscription.manageSubscriptions.billingFrequencyTitle"/>:  ${subscriptionData.billingFrequency.name}</p>
										</c:if>
										<c:if test="${!empty subscriptionData.contractDurationValue && !empty subscriptionData.contractDurationFrequency}">
											<p><spring:theme code="account.subscription.manageSubscriptions.contractDurationTitle"/>: ${subscriptionData.contractDurationValue}&nbsp;<spring:theme code="type.TermOfServiceFrequency.${subscriptionData.contractDurationFrequency}.name" /></p>
										</c:if>
									</div>
								</div>
								<div class="col-xs-12 col-sm-4">
									<div class="modal-wnd-item-current-sub"><b>Current Subscription</b></div>
								</div>
							</div>
						</div>
                    <%--Current Subscription Block--%>



                    <%--foreach--%>
					<c:forEach items="${subscriptionData.availableUpgradeSubscriptionTerms}" var="subscriptionTerm" varStatus="status">

						<div ${!status.last ? 'class="modal-wnd-list-item"' : 'class="modal-wnd-list-item-last"'}>
							<div class="row">
								<form action="${upgradeOptionsUrl}" method="GET">
									<input type="hidden" name="selectedOptionId" value="${subscriptionTerm.id}" />
									<input type="hidden" name="productCode" value="${subscriptionData.productCode}" />
									<div class="col-xs-12 col-sm-8">
										<div class="modal-wnd-list-description">
											<p><spring:theme code="account.subscription.manageSubscriptions.priceTitle"/>: ${subscriptionTerm.price.formattedValue}/<spring:theme code="type.TermOfServiceFrequency.${subscriptionTerm.termOfServiceFrequency.code}.display" /></p>
											<p><spring:theme code="account.subscription.manageSubscriptions.billingFrequencyTitle"/>: <spring:theme code="type.TermOfServiceFrequency.${subscriptionTerm.termOfServiceFrequency.code}.display" /></p>
											<p><spring:theme code="account.subscription.manageSubscriptions.contractDurationTitle"/>:  ${subscriptionTerm.termOfServiceNumber}&nbsp;${subscriptionTerm.termOfServiceFrequency.name}</p>
										</div>
									</div>
									<div class="col-xs-12 col-sm-4">
										<div class="modal-wnd-item-button">
											<button class="button button-main small" type="submit">
												<spring:theme code="account.subscription.manageSubscriptions.useThisSubscription" />
											</button>
										</div>
									</div>
								</form>
							</div>
						</div>

					</c:forEach>
					<%--foreach--%>



				</div>
			</div>
		</div>
	</div>
</div>
