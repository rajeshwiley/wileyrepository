<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="account" tagdir="/WEB-INF/tags/responsive/account"%>
<%@ taglib prefix="subscription" tagdir="/WEB-INF/tags/responsive/subscription" %>

<spring:theme code="account.subscription.date.pattern" text="MMM dd, yyyy hh:mm a" var="subscriptionDatePattern"/>
<spring:theme code="account.subscription.next.billing.date.pattern" text="MM/dd/yyyy" var="nextBillingDatePattern"/>

<div class="row">
	<div class="col-xs-12 col-sm-9">
		<h1 class="full-width-gray"><spring:theme code="text.account.manage.subscription.title"/></h1>
	</div>
	<div class="col-xs-12 col-sm-3">
		<spring:url value="/my-account/subscriptions" var="subscriptionListPageUrl"/>
		<div class="title-tools text-right"><a href="${subscriptionListPageUrl}"><spring:theme code="text.account.manage.subscription.backto"/></a></div>
	</div>
</div>
<div class="my-account-wrap">
	<div class="plan-name">
		<div class="row">
			<div class="col-sm-12">
				<h3>${subscriptionData.name}</h3>
				<c:if test="${not empty subscriptionData.contractDurationValue and not empty subscriptionData.contractDurationFrequency}">
					<spring:theme code="text.account.manage.subscription.contract.duration" text="Contract Duration"/>
					${": "} ${subscriptionData.contractDurationValue} ${' '} <spring:theme code="text.BillingServiceFrequency.${subscriptionData.contractDurationFrequency}.name"/>
					<c:if test="${not empty subscriptionData.billingFrequency.name and not empty subscriptionData.autoRenew}">
						${'; '}
					</c:if>
				</c:if>
				<c:if test="${not empty subscriptionData.billingFrequency.name}">
					<spring:theme code="text.account.manage.subscription.billing.frequency" text="Billing Frequency"/>
					${": "} ${subscriptionData.billingFrequency.name}
					<c:if test="${not empty subscriptionData.autoRenew}">
						${'; '}
					</c:if>
				</c:if>
				<c:if test="${not empty subscriptionData.autoRenew}">
					<spring:theme code="text.account.manage.subscription.autorenew" text="Auto Renewal"/>
					${": "} ${subscriptionData.autoRenew}
				</c:if>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-5">
			<div class="subscription-details">
				<div class="row">
					<div class="col-sm-12">
						<h3><spring:theme code="text.account.manage.subscription.details"/></h3>
					</div>
				</div>
				<c:if test="${not empty subscriptionData.billingPriceCurrency and not empty subscriptionData.billingPriceValue}">
					<div class="row">
						<div class="col-xs-6 col-sm-6"><spring:theme code="text.account.manage.subscription.price"/>:</div>
						<div class="col-xs-6 col-sm-6">
							${subscriptionData.billingPriceCurrency} ${subscriptionData.billingPriceValue}
							<c:if test="${not empty subscriptionData.contractDurationFrequency}">/</c:if>
							${subscriptionData.billingFrequency.name}
						</div>
					</div>
				</c:if>
				<c:if test="${not empty subscriptionData.contractDurationValue and not empty subscriptionData.contractDurationFrequency}">
					<div class="row">
						<div class="col-xs-6 col-sm-6"><spring:theme code="text.account.manage.subscription.contract.duration"/>:</div>
						<div class="col-xs-6 col-sm-6">
							${subscriptionData.contractDurationValue} ${' '} <spring:theme code="text.BillingServiceFrequency.${subscriptionData.contractDurationFrequency}.name"/>
						</div>
					</div>
				</c:if>
			</div>
		</div>
		<div class="col-xs-12 col-sm-5 col-sm-offset-2">
			<account:subscriptionOptionsModal subscriptionData="${subscriptionData}"/>
			<div class="subscription-status">
				<div class="row">
					<div class="col-sm-12">
						<h3><spring:theme code="text.account.subscription" text="Manage Subscriptions"/></h3>
					</div>
				</div>
				<c:if test="${not empty subscriptionData.status.code}">
					<div class="row">
						<div class="col-xs-3 col-sm-6"><spring:theme code="text.account.manage.subscription.status" text="Status"/>:</div>
						<div class="col-xs-9 col-sm-6 text-right"><spring:theme code="subscription.displayStatus.${subscriptionData.status.code}"/></div>
					</div>
				</c:if>
				<div class="row">
					<div class="col-sm-12">
						<div class="enrolled-in-school checkbox-component">
							<form:form id="subscriptionAutorenewalForm"
									   action="${encodedContextPath}/my-account/subscriptions/autorenew" method="post">
								<input name="currentSubscriptionCode" type="text" hidden value="${subscriptionData.code}"/>

								<c:if test="${subscriptionData.autoRenew == true}">
									<c:set var="subscriptionAutorenewalChecked" value="checked"/>
								</c:if>

								<c:if test="${subscriptionData.paymentInfo == null}">
									<c:set var="subscriptionAutorenewalDisabled" value="disabled"/>
								</c:if>

								<input ${subscriptionAutorenewalDisabled} type="checkbox" name="subscriptionAutorenewal"
																		  id="subscriptionAutorenewal" ${subscriptionAutorenewalChecked}/>
								<label for="subscriptionAutorenewal"><spring:theme code="account.subscription.manage.autorenew.checkbox"/></label>
							</form:form>

						</div>
					</div>
				</div>

				<c:if test="${not empty subscriptionData.endDate}">
					<div class="row">
						<div class="col-xs-6 col-sm-6">
							<spring:theme code="account.subscription.manageSubscriptions.service.end.date" text="Service End Date"/>:
						</div>
						<div class="col-xs-6 col-sm-6 text-right">
							<fmt:formatDate type="both" pattern="${subscriptionDatePattern}" value="${subscriptionData.endDate}"/>
						</div>
					</div>
				</c:if>

				<div class="row">
					<account:extendSubscription subscriptionData="${subscriptionData}"/>
				</div>
	         	<div class="row">
		            <account:subscriptionPaymentInfo paymentInfo="${subscriptionData.paymentInfo}"/>
		        </div>
				<c:if test="${not empty subscriptionData.deliveryAddress}">
					<div class="row">
						<div class="col-xs-3 col-sm-3 shipping">
                            <div class="bold"><spring:theme code="account.subscription.manageSubscriptions.shipTo" text="Ship to"/>:</div>
						</div>
						<div class="col-xs-9 col-sm-6 shipping">
							<c:if test="${not empty subscriptionData.customer}">
								<subscription:customerNameItem customer="${subscriptionData.customer}"/>
							</c:if>
							<c:if test="${not empty subscriptionData.deliveryAddress}">
								<subscription:deliveryAddressItem deliveryAddress="${subscriptionData.deliveryAddress}"/>
							</c:if>
						</div>
						<div class="col-xs-12 col-sm-3">
							<div class="my-account-button-small">
								<button data-toggle="modal"
									data-target="#addressBookModelWnd"
									${not empty subscriptionDeliveryAddresses ? 'class="button button-main large"' : 'class="button button-main large disabled"'}>
									<spring:theme code="account.subscription.manageSubscriptions.shipping.update" text="Update"/>
								</button>
							</div>
						</div>
					</div>
				</c:if>
			</div>
			<div class="subscription-activity">
				<div class="row">
					<div class="col-xs-3 col-sm-6">
						<h3><spring:theme code="text.account.manage.subscription.activity" text="Activity"/></h3>
					</div>
					<div class="col-xs-9 col-sm-6 text-right">
						<c:url var="orderUrl" value="/my-account/order/${subscriptionData.orderCode}"/>
						<div class="order-number">
							<spring:theme code="account.subscription.manageSubscriptions.order.number" text="Order Number"/>:&nbsp;
							<span><a href="${orderUrl}"><c:out value="${subscriptionData.orderCode}"/></a></span>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<spring:url value="/my-account/subscriptions/${subscriptionData.code}/billing-activities" var="billActLink"/>
						<a href="${billActLink}">
							<spring:theme code="account.subscription.manageSubscriptions.view.billing.activity" text="View Billing Activity"/>
						</a>
					</div>
				</div>
				<c:if test="${not empty subscriptionData.startDate}">
					<div class="row">
						<div class="col-xs-6 col-sm-6">
							<spring:theme code="account.subscription.manageSubscriptions.service.start.date" text="Service Start Date"/>:
						</div>
						<div class="col-xs-6 col-sm-6 text-right">
							<fmt:formatDate type="both" pattern="${subscriptionDatePattern}" value="${subscriptionData.startDate}"/>
						</div>
					</div>
				</c:if>
				<c:if test="${not empty subscriptionData.orderDate}">
					<div class="row">
						<div class="col-xs-6 col-sm-6">
							<spring:theme code="account.subscription.manageSubscriptions.ordered" text="Ordered"/>:
						</div>
						<div class="col-xs-6 col-sm-6 text-right">
							<fmt:formatDate type="both" pattern="${subscriptionDatePattern}" value="${subscriptionData.orderDate}"/>
						</div>
					</div>
				</c:if>
				<c:if test="${not empty subscriptionData.nextBillingDate}">
					<div class="row">
						<div class="col-xs-6 col-sm-6">
							<spring:theme code="account.subscription.manageSubscriptions.next.billing.date" text="Next Billing Date"/>:
						</div>
						<div class="col-xs-6 col-sm-6 text-right">
							<fmt:formatDate type="both" pattern="${nextBillingDatePattern}" value="${subscriptionData.nextBillingDate}"/>
						</div>
					</div>
				</c:if>
			</div>
		</div>
	</div>
</div>
<div id="addressBookModelWnd" role="dialog" class="modal fade modalWindow">
	<subscription:selectDeliveryAddressModalPopup subscriptionCode="${subscriptionData.code}" deliveryAddresses="${subscriptionDeliveryAddresses}"/>
</div>
