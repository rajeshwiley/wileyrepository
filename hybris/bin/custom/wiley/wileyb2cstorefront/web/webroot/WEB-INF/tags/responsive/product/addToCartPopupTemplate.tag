<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div id="addToCartModalWnd" role="dialog" class="modal fade modalWindow ">
</div>
<spring:theme code="product.add.to.cart.popup.item.label" var="addToCartPopupItemLabel"/>
<spring:theme code="product.add.to.cart.popup.items.label" var="addToCartPopupItemsLabel"/>
<c:url value="/contact" var="contactUrl"/>

<script type="text/javascript">
	addToCartPopupItemLabel = "${addToCartPopupItemLabel}";
	addToCartPopupItemsLabel= "${addToCartPopupItemsLabel}";
    addToCartModalWndTemplate = "\
		<div class='modal-dialog'>\
			<div class='modal-content'>\
			    <form method='action'>\
                    <div class='modal-header'>\
                        <div class='modal-title'><spring:theme code='addToCart.popup.text.title'/> {{newlyAddedQuantity}} {{newlyAddedQuantityLabel}}</div>\
                        <button type='button' data-dismiss='modal' aria-label='Close' class='close'><span aria-hidden='true'>&times;</span></button>\
                    </div>\
                    <div class='modal-body'>\
                        <div class='modal-wnd-list'>\
                        	{{#withErrors}}\
								<div class='modal-vnd-error'>\
									{{#errors}}\
										<p class='error'>\
											{{#isIsbnEmpty}}\
												{{#isCodeEmpty}}<spring:theme code='addToCart.popup.error.general'/>{{/isCodeEmpty}}\
												{{^isCodeEmpty}}<spring:theme code='addToCart.popup.error.with.code'/>{{/isCodeEmpty}}\
											{{/isIsbnEmpty}}\
											{{^isIsbnEmpty}}\
												{{#isCodeEmpty}}<spring:theme code='addToCart.popup.error.with.isbn'/>{{/isCodeEmpty}}\
												{{^isCodeEmpty}}<spring:theme code='addToCart.popup.error.with.codeAndIsbn'/>{{/isCodeEmpty}}\
											{{/isIsbnEmpty}}\
										</p>\
									{{/errors}}\
									{{^errors}}\
										<p class='error'>\
											<spring:theme code='addToCart.popup.error.general'/>\
										</p>\
									{{/errors}}\
									<p class='info'><spring:theme code='addToCart.popup.error.tryLater' arguments='${contactUrl}'/></p>\
								</div>\
							{{/withErrors}}\
							{{#entries}}\
								<div class='product-item'>\
									<div class='product-image'><img alt='' src='{{image}}'/></div>\
									<div class='product-content'>\
										<div class='product-price'>\
												{{#isOriginalPriceVisible}}\
												  <span class='old-price'>${sessionCurrency.symbol}&zwnj;{{originalTotal}}</span>\
												{{/isOriginalPriceVisible}}\
												<span class='price'>${sessionCurrency.symbol}&zwnj;{{total}}</span>\
										</div>\
										<h3 class='product-title'>{{name}}</h3>\
										<div class='product-author'>{{authors}}</div>\
										<div class='product-type'>\
											<span class='product-type-name'>{{mediumType}}</span>\
										</div>\
									</div>\
									{{#productSetComponents.length}}\
									<div class='product-sets'>\
										<p class='sets-title'><spring:theme code='addToCart.popup.text.set.includes'/></p>\
										<ul class='sets-list'>\
											{{#productSetComponents}}\
												<li>{{name}}</li>\
											{{/productSetComponents}}\
										</ul>\
									</div>\
									{{/productSetComponents.length}}\
								</div>\
							{{/entries}}\
                        </div>\
                        {{#withEntries}}\
							<div class='modal-wnd-subtotal'>\
								<spring:theme code='addToCart.popup.text.subtotal'/>\
								{{#totalQuantityLabel}}\
									<span class='item-count'>({{totalQuantity}} {{totalQuantityLabel}})</span>\
								{{/totalQuantityLabel}}<span class='item-price'>${sessionCurrency.symbol}&zwnj;{{subtotalPrice}}</span>\
								<c:if test="${not empty optionalTaxShortMessage}">\
										<span class='price-disclaimer'>${optionalTaxShortMessage}<a role='menuitem' tabindex='0'\
														data-placement='bottom-right top-right' aria-label='information icon' data-container='#addToCartModalWnd'\
														data-container-class='wiley-tax-popover' data-content='${fn:escapeXml(optionalTaxTooltip)}'\
														class='icon-info black'></a></span>\
								</c:if>\
							</div>\
						{{/withEntries}}\
						<div class='modal-wnd-related-products'>\
						</div>\
					</div>\
					<div class='modal-footer'>\
						<button data-dismiss='modal' class='button large button-dark-gray'><spring:theme code='addToCart.popup.button.continue'/></button>\
						<button type='reset' class='button large button-teal' onclick='location.href=\"${legacyViewCartLink}\"' ><spring:theme code='addToCart.popup.button.checkout'/></button>\
					</div>\
                </form>\
			</div>\
		</div>";
</script>
