<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="facetData" required="true" type="de.hybris.platform.commerceservices.search.facetdata.FacetData" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>


<c:if test="${not empty facetData.values}">
	<ycommerce:testId code="facetNav_title_${facetData.name}">
		<div class="js-facet" data-facet-code="${facetData.code}">
			<div class="facets-panel-block js-facet-name">
				<h3 class="facets-panel-block-title" tabindex="0" title="facets panel control"><spring:theme code="search.nav.facetTitle" arguments="${facetData.name}"/></h3>
				<div class="facets-panel-block-inner js-facet-values js-facet-form">
					<div class="facets-panel-search">
						<input type="search" placeholder="Search subject name" aria-label="Search facets">
						<div class="hidden-xs glyphicon glyphicon-search"></div>
					</div>
					<c:if test="${not empty facetData.topValues}">
						<div class="js-facet-list facets-panel-list js-facet-top-values">
							<c:forEach items="${facetData.topValues}" var="facetValue">
								<c:if test="${facetData.multiSelect}">
									<div class="facets-panel-item">
										<input type="hidden" name="pq" value="${facetValue.query.query.value}"/>
										<label class="custom-checkbox">
											<input class="facet-checkbox" type="checkbox" ${facetValue.selected ? 'checked="checked"' : ''} class="facet-checkbox" />
											<span class="facet-label">
												<span class="facet-text">
													${facetValue.name}
													<ycommerce:testId code="facetNav_count">
														<span class="facet-value-count"><i><spring:theme code="search.nav.facetValueCount" arguments="${facetValue.count}"/></i></span>
													</ycommerce:testId>
												</span>
											</span>
										</label>
									</div>
								</c:if>
								<c:if test="${not facetData.multiSelect}">
									<c:url value="${facetValue.query.url}" var="facetValueQueryUrl"/>
									<div class="facets-panel-item">
										<a href="${facetValueQueryUrl}">${facetValue.name}<i><spring:theme code="search.nav.facetValueCount" arguments="${facetValue.count}"/></i></a>
									</div>
								</c:if>
							</c:forEach>
						</div>
					</c:if>
					<div class="facet-list facets-panel-list js-facet-list <c:if test="${not empty facetData.topValues}">facet-list-hidden js-facet-list-hidden</c:if>">
						<c:forEach items="${facetData.values}" var="facetValue" varStatus="loop">
							<c:if test="${facetData.multiSelect}">
								<ycommerce:testId code="facetNav_selectForm">
								<div class="facets-panel-item" data-pq="${facetValue.query.query.value}">
									<spring:eval expression="T(de.hybris.platform.util.SafeURLEncoder).encode('|' + facetData.code + ':' + facetValue.code)" var="encodedCurrentOptionUrl" />
									<label class="custom-checkbox">
										<input type="checkbox" ${facetValue.selected ? 'checked="checked"' : ''}  class="facet-checkbox js-facet-checkbox" />
										<span class="facet-label">
											<span class="facet-text">
												${facetValue.name}
												<ycommerce:testId code="facetNav_count">
													<span class="facet-value-count"><i><spring:theme code="search.nav.facetValueCount" arguments="${facetValue.count}"/></i></span>
												</ycommerce:testId>
											</span>
										</span>
									</label>
								</div>
								</ycommerce:testId>
							</c:if>
							<c:if test="${not facetData.multiSelect}">
								<c:url value="${facetValue.query.url}" var="facetValueQueryUrl"/>
								<div class="facets-panel-item">
									<a href="${facetValueQueryUrl}">${facetValue.name}<i><spring:theme code="search.nav.facetValueCount" arguments="${facetValue.count}"/></i></a>
								</div>
							</c:if>
						</c:forEach>
					</div>

					<c:if test="${not empty facetData.topValues}">
						<span class="more js-more-facet-values">
							<a href="#" class="js-more-facet-values-link" ><spring:theme code="search.nav.facetShowMore_${facetData.code}" /></a>
						</span>
						<span class="less js-less-facet-values">
							<a href="#" class="js-less-facet-values-link"><spring:theme code="search.nav.facetShowLess_${facetData.code}" /></a>
						</span>
					</c:if>
					<c:if test="${facetData.hasMoreFacetValues}">
						<span class="all js-all-facet-values">
							<a href="#" class="read-more-link js-all-facet-values-link"><spring:theme code="search.nav.facetShowAll_${facetData.code}" text="See more"/></a>
						</span>
					</c:if>
				</div>
			</div>
		</div>
	</ycommerce:testId>
</c:if>
