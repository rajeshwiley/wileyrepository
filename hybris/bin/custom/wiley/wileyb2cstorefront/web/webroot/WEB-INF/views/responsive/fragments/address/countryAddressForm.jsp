<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/address"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:if test="${not empty country}">
	<div class="checkout-add-address-form">
		<div class="site-form">
			<form:form commandName="addressForm">
				<address:addressFormElements regions="${regions}"
					country="${country}" />
			</form:form>
		</div>
	</div>
</c:if>

