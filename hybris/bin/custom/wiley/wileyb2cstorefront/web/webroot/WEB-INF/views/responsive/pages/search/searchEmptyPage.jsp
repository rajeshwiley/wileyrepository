<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>

<template:page pageTitle="${pageTitle}" containerCSSClass="container-fluid" clearContainerClass="true">

	<c:url value="/" var="homePageUrl" />

	<div class="row">
		<div class="search-result-page">
			<nav:searchResultHeader searchPageData="${searchPageData}"
									totalNumberOfProductResults="0"
									totalNumberOfContentResults="0"/>
			<div class="search-result-content">
				<div class="container">
					<div class="empty-result">
						<cms:pageSlot position="TopContent" var="comp" element="div" class="searchEmptyPageTop">
							<cms:component component="${comp}"/>
						</cms:pageSlot>
						<%-- As we replace spaces with &#x20;, and default spring arguments separator is coma that we did not replace, so it is safe to use here space --%>
						<spring:theme code="search.page.emptyResults" argumentSeparator=" "
                        									  arguments="${searchPageData != null ? searchPageData.freeTextSearch : null}"/>
						<cms:pageSlot position="BottomContent" var="comp" element="div" class="searchEmptyPageBottom">
								<cms:component component="${comp}"/>
						</cms:pageSlot>
					</div>
			    </div>
			</div>
        </div>
    </div>
	
</template:page>
