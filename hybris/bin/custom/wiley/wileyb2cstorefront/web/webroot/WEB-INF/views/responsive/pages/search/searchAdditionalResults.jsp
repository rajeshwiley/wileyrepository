<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>

<div class="products-list">
	<c:forEach items="${searchPageData.results}" var="product">
		<product:productListerItem product="${product}"/>
	</c:forEach>
	<input type="hidden" name="_currentPage" value="${searchPageData.pagination.currentPage}"/>
	<input type="hidden" name="_numberOfPages" value="${searchPageData.pagination.numberOfPages}"/>

</div>