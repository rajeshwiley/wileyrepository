<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<c:set var="cssClasses" value="${fn:join(cssClasses, ' ')}"/>

<c:forEach items="${medias}" var="media">
	<c:choose>
		<c:when test="${empty imagerData}">
			<c:set var="imagerData">"${media.width}":"${media.url}"</c:set>
		</c:when>
		<c:otherwise>
			<c:set var="imagerData">${imagerData},"${media.width}":"${media.url}"</c:set>
		</c:otherwise>
	</c:choose>
	<c:if test="${empty altText}">
		<c:set var="altText" value="${media.altText}"/>
	</c:if>
</c:forEach>

<c:if test="${not empty urlLink}">
	<c:set var="urlLink">data-url="${urlLink}"</c:set>
</c:if>

<div class="item-inner ${cssClasses}" <c:if test="${not empty cssStyle}">${cssStyle}</c:if> ${urlLink}>
	<div class="simple-responsive-banner-component">
		<img class='responsive-image js-responsive-image' data-media='{${imagerData}}' alt='${altText}' title='${altText}' />
	</div>
	<div class="container">
		<article class="page-section">
			<c:if test="${not empty content}">
					<div class="wiley-slogan">${content}</div>
			</c:if>
			<cms:component component="${enhancedLink}"/>
		</article>
	</div>
</div>
