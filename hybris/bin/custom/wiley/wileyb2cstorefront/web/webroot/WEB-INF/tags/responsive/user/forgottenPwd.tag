<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="user" tagdir="/WEB-INF/tags/responsive/user" %>

<div id="formForgotPassword" role="dialog" class="modal fade modalWindow">
   <div class="modal-dialog">
      <div class="modal-content">

         <div class="modal-header">
            <div class="row">
               <div class="col-xs-10 col-sm-10">
                  <div class="modal-title">
                     <spring:theme code="forgottenPwd.title"/>
                  </div>
               </div>
               <div class="col-xs-2 col-sm-2">
                  <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">&times;</span></button>
               </div>
            </div>
         </div>

         <user:forgottenPwdForm/>
               
      </div>
   </div>
</div>