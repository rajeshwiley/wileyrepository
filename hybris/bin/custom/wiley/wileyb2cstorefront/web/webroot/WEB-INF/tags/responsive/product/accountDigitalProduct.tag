<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="entry" required="true" type="de.hybris.platform.commercefacades.order.data.OrderEntryData" %>

<div class="product-list-item">
		<div class="row">
			<div class="col-xs-12 col-sm-9">
				<div class="product-list-image"><product:productPrimaryImage product="${entry.product}" format="thumbnail"/></div>
				<div class="product-list-info">
					<div class="product-list-title">${entry.product.name}</div>
					<div class="product-list-description">
						<div class="description">
							<div><spring:theme code="text.account.myDigitalProducts.productId"/> ${entry.product.code}</div>
							<div>
								<spring:theme code="text.account.myDigitalProducts.purchased"/>
								<fmt:formatDate value="${entry.purchaseDate}" type="both" pattern="${purchaseDateFormat}" />
							</div>
							<c:if test="${entry.needAccessToken}">
								<div><spring:theme code="text.account.myDigitalProducts.accessCode"/>
								<c:choose>
									<c:when test="${not empty entry.digitalAccessToken}">
										${entry.digitalAccessToken}
									</c:when>
									<c:otherwise>
										<spring:theme code="text.account.myDigitalProducts.access.no.token"/>
									</c:otherwise>
								</c:choose>
								</div>
							</c:if>
						</div>
					</div>
				</div>
			</div>
			<c:if test="${entry.needAccessButton}">
				<div class="col-xs-12 col-sm-3 text-right">
					<div class="my-account-button-small">
						<a href="digital-products/access?orderCode=${entry.orderCode}&entryNumber=${entry.entryNumber}"
								class="button button-outlined large" target="_blank">
							<spring:theme code="text.account.myDigitalProducts.accessNow"/>
						</a>
					</div>
				 </div>
			</c:if>
		</div>
</div>