module.exports = function(grunt) {
	// Project configuration.
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		buildkit_path: '../../../../../buildkit/<%= pkg.name %>',
		watch: {
			less: {
				files: [
					'web/webroot/WEB-INF/_ui-src/shared/less/variableMapping.less',
					'web/webroot/WEB-INF/_ui-src/shared/less/generatedVariables.less',
					'web/webroot/WEB-INF/_ui-src/**/common/**/less/*',
					'web/webroot/WEB-INF/_ui-src/**/themes/**/less/*',
					'web/webroot/WEB-INF/_ui-src/responsive/lib/ybase-*/less/*'
				],
				tasks: ['less']
			},
			fonts: {
				files: ['web/webroot/WEB-INF/_ui-src/**/common/**/fonts/*'],
				tasks: ['sync:syncfonts']
			},
			fontsthemes: {
				files: ['web/webroot/WEB-INF/_ui-src/**/themes/**/fonts/*'],
				tasks: ['sync:syncfontsthemes']
			},
			ybasejs: {
				files: ['web/webroot/WEB-INF/_ui-src/responsive/lib/ybase-0.1.0/js/**/*.js'],
				tasks: ['sync:syncybase']
			},
			jquery: {
				files: ['web/webroot/WEB-INF/_ui-src/responsive/lib/jquery*.js'],
				tasks: ['sync:syncjquery']
			},
			customjs: {
				files: ['web/webroot/WEB-INF/_ui-src/responsive/lib/js/*'],
				tasks: ['sync:syncustomjs']
			},
			images: {
				files: ['web/webroot/WEB-INF/_ui-src/**/common/**/images/*'],
				tasks: ['sync:syncimages']
			},
			imagesthemes: {
				files: ['web/webroot/WEB-INF/_ui-src/**/themes/**/images/*'],
				tasks: ['sync:syncimagesthemes']
			},
			jade: {
				files: ['<%= buildkit_path %>/jade/**/*.jade'],
				tasks: ['jade', 'sync:synchtml', 'regex-replace'],
				options: {
					nospawn: true
				}
			}
		},

		less: {
			common: {
				files: [
					{
						expand: true,
						cwd: 'web/webroot/WEB-INF/_ui-src/',
						src: ['**/common/**/less/style.less'],
						dest: 'web/webroot/_ui/',
						ext: '.css',
						rename:function(dest,src){
							var nsrc = src.replace(new RegExp("less"),"css");
							return dest+nsrc;
						}
					}
				]
			},
			themes: {
				files: [
					{
						expand: true,
						cwd: 'web/webroot/WEB-INF/_ui-src/',
						src: ['**/themes/**/less/style.less'],
						dest: 'web/webroot/_ui/',
						ext: '.css',
						rename:function(dest,src){
							var nsrc = src.replace(new RegExp("/themes/(.*)/less"),"/theme-$1/css");
							return dest+nsrc;
						}
					}
				]
			}
		},

		jade: {
			compile: {
				options: {
					client: false,
					pretty: true
				},
				files: [ {
					cwd: "<%= buildkit_path %>/jade/",
					src: "*.jade",
					dest: "<%= buildkit_path %>/pages/",
					expand: true,
					ext: ".html"
				} ]
			}
		},

		'regex-replace': {
			baseTag: {
				src: ['web/webroot/_ui/responsive/common/pages/*'],
				actions: [
					{
						search: 'href="../../../hybris/bin/custom/wiley/wileyb2cstorefront/web/webroot/_ui/"',
						replace: 'href="../../../"'
					}
				]
			}
		},

		sync : {
			syncfonts: {
				files: [{
					expand: true,
					cwd: 'web/webroot/WEB-INF/_ui-src/',
					src: '**/common/**/fonts/*',
					dest: 'web/webroot/_ui/'
				}]
			},
			syncfontsthemes: {
				files: [{
					expand: true,
					cwd: 'web/webroot/WEB-INF/_ui-src/',
					src: '**/themes/**/fonts/*',
					dest: 'web/webroot/_ui/',
					rename:function(dest,src){
						var nsrc = src.replace(new RegExp("/themes/(.*)"),"/theme-$1");
						return dest+nsrc;
					}
				}]
			},
			syncybase: {
				files: [{
					cwd: 'web/webroot/WEB-INF/_ui-src/responsive/lib/ybase-0.1.0/js/',
					src: '**/*.js',
					dest: 'web/webroot/_ui/responsive/common/js'
				}]
			},
			syncjquery: {
				files: [{
					cwd: 'web/webroot/WEB-INF/_ui-src/responsive/lib',
					src: 'jquery*.js',
					dest: 'web/webroot/_ui/responsive/common/js'
				}]
			},
			syncustomjs: {
				files: [{
					cwd: 'web/webroot/WEB-INF/_ui-src/responsive/lib/js/',
					src: '*',
					dest: 'web/webroot/_ui/responsive/common/js'
				}]
			},
			syncimages: {
				files: [{
					expand: true,
					cwd: 'web/webroot/WEB-INF/_ui-src/',
					src: '**/common/**/images/*',
					dest: 'web/webroot/_ui/'
				}]
			},
			syncimagesthemes: {
				files: [{
					expand: true,
					cwd: 'web/webroot/WEB-INF/_ui-src/',
					src: '**/themes/**/images/*',
					dest: 'web/webroot/_ui/',
					rename:function(dest,src){
						var nsrc = src.replace(new RegExp("/themes/(.*)"),"/theme-$1");
						return dest+nsrc;
					}
				}]
			},
			synchtml: {
				files: [{
					expand: true,
					cwd: "<%= buildkit_path %>/pages/",
					src: "*.html",
					dest: 'web/webroot/_ui/responsive/common/pages/'
				}]
			}
		}

	});

	// Plugins
	grunt.loadNpmTasks("grunt-contrib-jade");
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-sync');
	grunt.loadNpmTasks('grunt-regex-replace');
	grunt.loadNpmTasks('grunt-contrib-watch');

	// Default task(s).
	grunt.registerTask('default', ['jade', 'less', 'sync', 'regex-replace', 'watch']);
};