package wileyintegrations.sabrix

import com.wiley.core.wileyb2c.externaltax.services.impl.Wileyb2cCalculateExternalTaxesStrategy
import com.wiley.core.i18n.WileyCountryService
import de.hybris.platform.catalog.CatalogVersionService
import de.hybris.platform.catalog.model.CatalogVersionModel
import de.hybris.platform.core.model.c2l.CountryModel
import de.hybris.platform.core.model.order.OrderEntryModel
import de.hybris.platform.core.model.order.OrderModel
import de.hybris.platform.core.model.product.ProductModel
import de.hybris.platform.core.model.user.AddressModel
import de.hybris.platform.externaltax.ExternalTaxDocument
import de.hybris.platform.product.ProductService
import de.hybris.platform.product.daos.UnitDao
import de.hybris.platform.servicelayer.i18n.daos.CurrencyDao
import de.hybris.platform.servicelayer.i18n.daos.RegionDao
import de.hybris.platform.servicelayer.model.ModelService
import de.hybris.platform.servicelayer.user.UserService
import de.hybris.platform.util.TaxValue


ModelService modelService = (ModelService)spring.getBean("modelService");
UserService userService = (UserService)spring.getBean("userService");
CatalogVersionService cvService = (CatalogVersionService)spring.getBean("catalogVersionService");
ProductService productService = (ProductService)spring.getBean("productService");
WileyCountryService countryService = (WileyCountryService)spring.getBean("wileyCountryService");
RegionDao regionDao = (RegionDao)spring.getBean("regionDao");
CurrencyDao currDao = (CurrencyDao)spring.getBean("currencyDao");
UnitDao unitDao = (UnitDao)spring.getBean("unitDao");

CatalogVersionModel cv = cvService.getCatalogVersion("wileyProductCatalog", "Online");
ProductModel product1 = productService.getProductForCode(cv, "CNCR")
ProductModel product2 = productService.getProductForCode(cv, "CNCR1")


CountryModel usa = countryService.findCountryByCode("US");
AddressModel customerAddr = modelService.create(AddressModel.class);
customerAddr.setCountry(usa);
customerAddr.setRegion(regionDao.findRegionsByCountryAndCode(usa, "US-NC"));
customerAddr.setTown("DURHAM")
customerAddr.setOwner(userService.getAdminUser())
modelService.save(customerAddr);

OrderModel order = modelService.create(OrderModel.class);
order.setUser(userService.getAdminUser());
order.setDate(new Date());
order.setCurrency(currDao.findCurrenciesByCode("USD").get(0));
order.setPaymentAddress(customerAddr);
order.setDeliveryAddress(customerAddr);

OrderEntryModel entry1 = modelService.create(OrderEntryModel.class);
OrderEntryModel entry2 = modelService.create(OrderEntryModel.class);
entry1.setOrder(order);
entry2.setOrder(order);
entry1.setTaxableTotalPrice(Double.valueOf(100));
entry2.setTaxableTotalPrice(Double.valueOf(200));
entry1.setQuantity(1);
entry2.setQuantity(1);
entry1.setProduct(product1);
entry2.setProduct(product2);
entry1.setUnit(unitDao.findUnitsByCode("pieces").iterator().next());
entry2.setUnit(unitDao.findUnitsByCode("pieces").iterator().next());
List<OrderEntryModel> entries = new ArrayList<OrderEntryModel>();
entries.add(entry1);
entries.add(entry2);
order.setEntries(entries);

modelService.saveAll(order, entry1, entry2);

Wileyb2cCalculateExternalTaxesStrategy strategy = spring.getBean("wileyb2cCalculateExternalTaxesStrategy");
ExternalTaxDocument document = strategy.calculateExternalTaxes(order);
for( Map.Entry<Integer, List<TaxValue>> mentry : document.getAllTaxes() ) {
    println("Taxes for "+mentry.getKey())
    for( TaxValue taxv : mentry.getValue()) {
        println(taxv.toString());
    }
}


