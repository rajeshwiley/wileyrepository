package com.wiley.integrations.vies.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.retry.RetryCallback;
import org.springframework.retry.RetryContext;
import org.springframework.retry.listener.RetryListenerSupport;


public class ViesLogRetryListener extends RetryListenerSupport
{
	private static final Logger LOG = LoggerFactory.getLogger(ViesLogRetryListener.class);

	@Override
	public <T, E extends Throwable> void onError(final RetryContext context, final RetryCallback<T, E> callback,
			final Throwable throwable)
	{
		LOG.debug("Unable to connect VIES server: {}", throwable.getMessage());
	}
}
