package com.wiley.integrations.alm.authentication.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
@JsonInclude(NON_NULL)
public class AlmAuthenticationImitateeDto
{
	@JsonProperty(value = "imitateeID")
	private String imitateeId;

	public String getImitateeId()
	{
		return imitateeId;
	}

	public void setImitateeId(final String imitateeId)
	{
		this.imitateeId = imitateeId;
	}

	@Override
	public String toString()
	{
		return MoreObjects.toStringHelper(this)
				.add("imitateeId", getImitateeId())
				.toString();
	}
}
