package com.wiley.integrations.utils;

import java.util.function.BiConsumer;


public final class PopulateUtils
{
    private PopulateUtils()
    {
    }

    public static <T, V> void populateFieldIfNotNull(final BiConsumer<T, V> populateOperation, final T target, final V value)
    {
        if (value != null) {
            populateOperation.accept(target, value);
        }
    }

}