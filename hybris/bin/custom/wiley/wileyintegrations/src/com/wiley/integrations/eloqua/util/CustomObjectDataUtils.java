package com.wiley.integrations.eloqua.util;

import java.util.Date;

import com.wiley.core.integration.eloqua.dto.FieldValueDto;


/**
 * Class contains util methods to work with CustomObjectDataDto.
 *
 * @author Aliaksei_Zlobich
 */
public final class CustomObjectDataUtils
{

	private CustomObjectDataUtils()
	{
	}

	public static FieldValueDto createFieldValue(final Integer id, final String value)
	{
		FieldValueDto fieldValueDto = new FieldValueDto();
		fieldValueDto.setId(id);
		fieldValueDto.setValue(value);
		return fieldValueDto;
	}

	public static int toUnixTime(final Date date)
	{
		return (int) (date.getTime() / 1000);
	}
}
