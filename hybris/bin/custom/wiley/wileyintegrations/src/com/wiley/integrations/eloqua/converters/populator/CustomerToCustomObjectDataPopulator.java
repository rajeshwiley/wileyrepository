package com.wiley.integrations.eloqua.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.List;

import javax.annotation.Nonnull;

import org.springframework.util.Assert;

import com.wiley.core.integration.eloqua.dto.CustomObjectDataDto;
import com.wiley.core.integration.eloqua.dto.FieldValueDto;
import com.wiley.integrations.eloqua.util.CustomObjectDataUtils;


/**
 * Populates CustomObjectData from Customer.
 *
 * @author Aliaksei_Zlobich
 */
public class CustomerToCustomObjectDataPopulator implements Populator<CustomerModel, CustomObjectDataDto>
{

	public static final int CONTACT_ID_FIELD_VALUE = 5285;
	public static final int CONTACT_EMAIL_FIELD_VALUE = 5262;

	@Override
	public void populate(@Nonnull final CustomerModel customer, @Nonnull final CustomObjectDataDto cdo)
			throws ConversionException
	{
		Assert.notNull(customer);
		Assert.notNull(cdo);

		final Integer eloquaId = customer.getEloquaId();

		Assert.notNull(eloquaId, "Eloqua id should be set to customer.");

		cdo.setContactId(eloquaId);
		final List<FieldValueDto> fieldValues = cdo.getFieldValues();
		addContactIdFieldValue(fieldValues, eloquaId);
		addContactEmailFieldValue(fieldValues, customer);
	}


	private void addContactIdFieldValue(final List<FieldValueDto> fieldValues, final Integer contactId)
	{
		fieldValues.add(CustomObjectDataUtils.createFieldValue(CONTACT_ID_FIELD_VALUE, String.valueOf(contactId)));
	}

	private void addContactEmailFieldValue(final List<FieldValueDto> fieldValues, final CustomerModel customer)
	{
		fieldValues.add(CustomObjectDataUtils.createFieldValue(CONTACT_EMAIL_FIELD_VALUE, customer.getContactEmail()));
	}
}
