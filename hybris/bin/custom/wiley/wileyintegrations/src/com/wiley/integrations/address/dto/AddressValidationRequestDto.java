package com.wiley.integrations.address.dto;

import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonInclude(JsonInclude.Include.NON_NULL)
public class AddressValidationRequestDto
{


	/**
	 * Identifier of the address. Used in the batch mode.
	 */
	@JsonProperty("ID")
	private String id;

	/**
	 * First line of the address.
	 */
	@JsonProperty("ADDRESS_LINE_1")
	private String addressLine1;

	/**
	 * Second line of the address.
	 */
	@JsonProperty("ADDRESS_LINE_2")
	private String addressLine2;

	/**
	 * Third line of the address.
	 */
	@JsonProperty("ADDRESS_LINE_3")
	private String addressLine3;

	/**
	 * City/town.
	 */
	@JsonProperty("CITY")
	private String city;

	/**
	 * The ISO 3166-2 code that specifies a state or province.
	 * Can consist of letters or numbers (for example, it is two-letter code for US).
	 * For US the ISO 3166-2 codes were extended with non-standard Armed Forces regions values, which were provided by Wiley.
	 * See https://jira.wiley.ru/browse/ECSC-5892 for details.
	 */
	@JsonProperty("REGION")
	@Pattern(regexp = "^[0-9A-Z]{1,3}$")
	private String region;

	/**
	 * County
	 */
	@JsonProperty("COUNTY")
	private String county;

	/**
	 * Zip/postal code.
	 */
	@JsonProperty("POSTAL_CODE")
	private String postalCode;

	/**
	 * Two-letter ISO 3166-1 alpha-2 code that specifies a country.
	 * See https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2 for details.
	 */
	@JsonProperty("COUNTRY")
	@Pattern(regexp = "^[A-Z]{2}$")
	private String country;

	public String getId()
	{
		return id;
	}

	public void setId(final String id)
	{
		this.id = id;
	}

	public String getAddressLine1()
	{
		return addressLine1;
	}

	public void setAddressLine1(final String addressLine1)
	{
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2()
	{
		return addressLine2;
	}

	public void setAddressLine2(final String addressLine2)
	{
		this.addressLine2 = addressLine2;
	}

	public String getAddressLine3()
	{
		return addressLine3;
	}

	public void setAddressLine3(final String addressLine3)
	{
		this.addressLine3 = addressLine3;
	}

	public String getCity()
	{
		return city;
	}

	public void setCity(final String city)
	{
		this.city = city;
	}

	public String getRegion()
	{
		return region;
	}

	public void setRegion(final String region)
	{
		this.region = region;
	}

	public String getCounty()
	{
		return county;
	}

	public void setCounty(final String county)
	{
		this.county = county;
	}

	public String getPostalCode()
	{
		return postalCode;
	}

	public void setPostalCode(final String postalCode)
	{
		this.postalCode = postalCode;
	}

	public String getCountry()
	{
		return country;
	}

	public void setCountry(final String country)
	{
		this.country = country;
	}
}
