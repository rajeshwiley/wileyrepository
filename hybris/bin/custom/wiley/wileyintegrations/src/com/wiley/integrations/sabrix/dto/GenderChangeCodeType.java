package com.wiley.integrations.sabrix.dto;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GenderChangeCodeType.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="GenderChangeCodeType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="ChangeFemale"/&gt;
 *     &lt;enumeration value="ChangeMale"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 */
@XmlType(name = "GenderChangeCodeType", namespace = "http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1")
@XmlEnum
public enum GenderChangeCodeType
{

	@XmlEnumValue("ChangeFemale")
	CHANGE_FEMALE("ChangeFemale"),
	@XmlEnumValue("ChangeMale")
	CHANGE_MALE("ChangeMale");
	private final String value;

	GenderChangeCodeType(String v)
	{
		value = v;
	}

	public String value()
	{
		return value;
	}

	public static GenderChangeCodeType fromValue(String v)
	{
		for (GenderChangeCodeType c : GenderChangeCodeType.values())
		{
			if (c.value.equals(v))
			{
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}

}
