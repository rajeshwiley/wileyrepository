package com.wiley.integrations.order.populator;

import com.wiley.integrations.order.dto.PriceValueWsDTO;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.util.PriceValue;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

public class WileyPriceValueWsPopulator implements Populator<PriceValue, PriceValueWsDTO>
{
	@Override
	public void populate(final PriceValue source, final PriceValueWsDTO target) throws ConversionException
	{
		validateParameterNotNullStandardMessage("source", source);
		validateParameterNotNullStandardMessage("target", target);

		target.setCurrency(source.getCurrencyIso());
		target.setValue(source.getValue());
	}
}
