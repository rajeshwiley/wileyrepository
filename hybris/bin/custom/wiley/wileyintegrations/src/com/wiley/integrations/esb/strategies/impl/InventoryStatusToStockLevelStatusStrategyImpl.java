package com.wiley.integrations.esb.strategies.impl;

import de.hybris.platform.basecommerce.enums.StockLevelStatus;

import com.wiley.integrations.esb.dto.verifyandcalculatecart.InventoryStatus;
import com.wiley.integrations.esb.strategies.InventoryStatusToStockLevelStatusStrategy;


/**
 * Created by Mikhail_Asadchy on 7/29/2016.
 */
public class InventoryStatusToStockLevelStatusStrategyImpl implements InventoryStatusToStockLevelStatusStrategy
{
	public StockLevelStatus getExternalInventoryStatus(final InventoryStatus.StatusEnum status)
	{
		switch (status)
		{
			case IN_STOCK:
				return StockLevelStatus.INSTOCK;
			case BACK_ORDER:
				return StockLevelStatus.BACK_ORDER;
			case PRE_ORDER:
				return StockLevelStatus.PRE_ORDER;
			case PRINT_ON_DEMAND:
				return StockLevelStatus.PRINT_ON_DEMAND;
			default:
				throw new IllegalArgumentException(String.format("Could not find external inventory status for [%s]", status));
		}
	}


}
