package com.wiley.integrations.b2b.unit.transformer;


import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import javax.annotation.Resource;

import com.wiley.integrations.b2b.unit.dto.AddressSwgDTO;

import org.apache.commons.lang.StringUtils;


public class AddressSwgDTOTransformer
{
	@Resource
	private CommonI18NService commonI18NService;

	public AddressModel transform(final AddressSwgDTO addressDTO, final boolean isShippingAddress,
			final boolean isBillingAddress)
	{
		if (addressDTO != null)
		{
			AddressModel address = new AddressModel();
			address.setExternalId(addressDTO.getAddressId());
			address.setPostalcode(addressDTO.getPostcode());
			address.setTown(addressDTO.getCity());
			//Setting street name also sets Line1 dynamic property
			address.setStreetname(addressDTO.getLine1());
			//Setting street number also sets Line2 dynamic property
			address.setStreetnumber(addressDTO.getLine2());
			address.setFirstname(addressDTO.getFirstName());
			address.setLastname(addressDTO.getLastName());
			address.setPhone1(addressDTO.getPhoneNumber());

			address.setBillingAddress(isBillingAddress);
			address.setShippingAddress(isShippingAddress);

			if (StringUtils.isNotBlank(addressDTO.getCountry()))
			{
				final CountryModel country = commonI18NService.getCountry(addressDTO.getCountry());
				address.setCountry(country);

				if (StringUtils.isNotBlank(addressDTO.getState()) && country != null)
				{
					final String regionIsocode = country.getIsocode() + "-" + addressDTO.getState();
					address.setRegion(commonI18NService.getRegion(country, regionIsocode));
				}
			}

			return address;
		}
		return null;
	}
}
