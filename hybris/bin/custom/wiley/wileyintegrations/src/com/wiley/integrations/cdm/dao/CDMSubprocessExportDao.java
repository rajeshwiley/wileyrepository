/**
 *
 */
package com.wiley.integrations.cdm.dao;

import de.hybris.platform.core.model.user.CustomerModel;

import java.util.List;

import com.wiley.fulfilmentprocess.model.CDMCustomerExportProcessModel;


/**
 * Interface for CDM export subprocess
 */
public interface CDMSubprocessExportDao {
  /**
   * Returns the CDM export subprocesses associated with the given customer.
   */
  List<CDMCustomerExportProcessModel> getCDMExportProcesses(CustomerModel customer);
}
