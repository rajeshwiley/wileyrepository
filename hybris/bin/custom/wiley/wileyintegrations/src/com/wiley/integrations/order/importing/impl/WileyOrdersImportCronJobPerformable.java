package com.wiley.integrations.order.importing.impl;

import com.wiley.core.model.WileyOrdersImportCronJobModel;
import com.wiley.integrations.order.importing.WileyBatchOrderImportFacade;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.commerceservices.impersonation.ImpersonationContext;
import de.hybris.platform.commerceservices.impersonation.ImpersonationService;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.user.UserService;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

public class WileyOrdersImportCronJobPerformable extends AbstractJobPerformable<WileyOrdersImportCronJobModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyOrdersImportCronJobPerformable.class);
	@Autowired
	private ImpersonationService impersonationService;
	@Autowired
	private CatalogVersionService catalogVersionService;
	@Autowired
	private UserService userService;
	@Autowired
	private MediaService mediaService;
	@Autowired
	private WileyBatchOrderImportFacade wileyBatchOrderImportFacade;

	@Override
	public PerformResult perform(final WileyOrdersImportCronJobModel cronJobModel)
	{

		final ImpersonationContext ctx = new ImpersonationContext();
		ctx.setSite(cronJobModel.getBaseSite());
		ctx.setUser(userService.getAnonymousUser());
		ctx.setCatalogVersions(Collections.singletonList(cronJobModel.getCatalogVersion()));
		byte[] orderData = mediaService.getDataFromMedia(cronJobModel.getOrders());
		List<String> errors;
		try
		{
			errors = impersonationService.executeInContext(ctx,
					() -> wileyBatchOrderImportFacade.importOrdersFromStream(new ByteArrayInputStream(orderData),
					ctx.getSite().getUid(), cronJobModel.getUserAutoCreationStrategy()));
		} catch (IOException e)
		{
			LOG.error("Unable to read Order JSON media: " + e.getMessage(), e);
			return new PerformResult(CronJobResult.ERROR, CronJobStatus.FINISHED);
		}
		if (CollectionUtils.isNotEmpty(errors))
		{
			StringBuilder notImportedOrderSummary = new StringBuilder();
			errors.forEach(error -> notImportedOrderSummary.append(error).append("\n"));
			LOG.error("Not imported orders count {}. Details:\n{}", errors.size(), notImportedOrderSummary);
			return new PerformResult(CronJobResult.FAILURE, CronJobStatus.FINISHED);
		} else
		{
			return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
		}
	}
}
