package com.wiley.integrations.sabrix.dto;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SearchQueryType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="SearchQueryType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="QueryString" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="FilterCriteria" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="SortCriteria" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/Meta/V1}SortCriteriaType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="QueryCode" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="RecordsToReturn" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1}NonNegativeIntegerType" minOccurs="0"/&gt;
 *         &lt;element name="ReturnTotalIndicator" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1}IndicatorType" minOccurs="0"/&gt;
 *         &lt;element name="PagingCriteria" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/Meta/V1}PagingCriteriaType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchQueryType", namespace = "http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/Meta/V1", propOrder = {
		"queryString",
		"filterCriteria",
		"sortCriteria",
		"queryCode",
		"recordsToReturn",
		"returnTotalIndicator",
		"pagingCriteria"
})
public class SearchQueryType
{

	@XmlElement(name = "QueryString")
	protected String queryString;
	@XmlElement(name = "FilterCriteria")
	protected String filterCriteria;
	@XmlElement(name = "SortCriteria")
	protected List<SortCriteriaType> sortCriteria;
	@XmlElement(name = "QueryCode")
	protected String queryCode;
	@XmlElement(name = "RecordsToReturn")
	@XmlSchemaType(name = "nonNegativeInteger")
	protected BigInteger recordsToReturn;
	@XmlElement(name = "ReturnTotalIndicator")
	protected Boolean returnTotalIndicator;
	@XmlElement(name = "PagingCriteria")
	protected PagingCriteriaType pagingCriteria;

	/**
	 * Gets the value of the queryString property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getQueryString()
	{
		return queryString;
	}

	/**
	 * Sets the value of the queryString property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setQueryString(String value)
	{
		this.queryString = value;
	}

	/**
	 * Gets the value of the filterCriteria property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getFilterCriteria()
	{
		return filterCriteria;
	}

	/**
	 * Sets the value of the filterCriteria property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setFilterCriteria(String value)
	{
		this.filterCriteria = value;
	}

	/**
	 * Gets the value of the sortCriteria property.
	 *
	 * <p>
	 * This accessor method returns a reference to the live list,
	 * not a snapshot. Therefore any modification you make to the
	 * returned list will be present inside the JAXB object.
	 * This is why there is not a <CODE>set</CODE> method for the sortCriteria property.
	 *
	 * <p>
	 * For example, to add a new item, do as follows:
	 * <pre>
	 *    getSortCriteria().add(newItem);
	 * </pre>
	 *
	 *
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link SortCriteriaType }
	 */
	public List<SortCriteriaType> getSortCriteria()
	{
		if (sortCriteria == null)
		{
			sortCriteria = new ArrayList<SortCriteriaType>();
		}
		return this.sortCriteria;
	}

	/**
	 * Gets the value of the queryCode property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getQueryCode()
	{
		return queryCode;
	}

	/**
	 * Sets the value of the queryCode property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setQueryCode(String value)
	{
		this.queryCode = value;
	}

	/**
	 * Gets the value of the recordsToReturn property.
	 *
	 * @return possible object is
	 * {@link BigInteger }
	 */
	public BigInteger getRecordsToReturn()
	{
		return recordsToReturn;
	}

	/**
	 * Sets the value of the recordsToReturn property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link BigInteger }
	 */
	public void setRecordsToReturn(BigInteger value)
	{
		this.recordsToReturn = value;
	}

	/**
	 * Gets the value of the returnTotalIndicator property.
	 *
	 * @return possible object is
	 * {@link Boolean }
	 */
	public Boolean isReturnTotalIndicator()
	{
		return returnTotalIndicator;
	}

	/**
	 * Sets the value of the returnTotalIndicator property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link Boolean }
	 */
	public void setReturnTotalIndicator(Boolean value)
	{
		this.returnTotalIndicator = value;
	}

	/**
	 * Gets the value of the pagingCriteria property.
	 *
	 * @return possible object is
	 * {@link PagingCriteriaType }
	 */
	public PagingCriteriaType getPagingCriteria()
	{
		return pagingCriteria;
	}

	/**
	 * Sets the value of the pagingCriteria property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link PagingCriteriaType }
	 */
	public void setPagingCriteria(PagingCriteriaType value)
	{
		this.pagingCriteria = value;
	}

}
