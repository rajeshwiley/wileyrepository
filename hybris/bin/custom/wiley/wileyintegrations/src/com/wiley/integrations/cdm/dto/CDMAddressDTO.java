/**
 *
 */
package com.wiley.integrations.cdm.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonInclude(JsonInclude.Include.NON_NULL)
public class CDMAddressDTO {

  @JsonProperty(value = "Id")
  private String id;
  @JsonProperty(value = "AddressType")
  private String addressType;
  @JsonProperty(value = "Addr_Type_CD")
  private String addrTypeCD;
  @JsonProperty(value = "AddressCleanseCode")
  private String addressCleanseCode;
  @JsonProperty(value = "AddressDeliveryConfidenceCode")
  private String addressDeliveryConfidenceCode;
  @JsonProperty(value = "AddressEndDate")
  private String addressEndDate;
  @JsonProperty(value = "AddressLine1")
  private String addressLine1;
  @JsonProperty(value = "AddressLine2")
  private String addressLine2;
  @JsonProperty(value = "AddressLine3")
  private String addressLine3;
  @JsonProperty(value = "AddressStartDate")
  private String addressStartDate;
  @JsonProperty(value = "Addr_Type_ECID")
  private String addrTypeECID;
  @JsonProperty(value = "City")
  private String city;
  @JsonProperty(value = "CountryCode")
  private String countryCode;
  @JsonProperty(value = "CountryName")
  private String countryName;
  @JsonProperty(value = "County")
  private String county;
  @JsonProperty(value = "PostBox")
  private String postBox;
  @JsonProperty(value = "Province")
  private String province;
  @JsonProperty(value = "State")
  private String state;
  @JsonProperty(value = "ZipCode")
  private String zipCode;
  @JsonProperty(value = "AddressCleaningFlag")
  private Boolean addressCleaningFlag;
  @JsonProperty(value = "InstitutionCd")
  private String institutionCd;
  @JsonProperty(value = "InstitutionName")
  private String institutionName;
  @JsonProperty(value = "DepartmentCd")
  private String departmentCd;
  @JsonProperty(value = "DepartmentName")
  private String departmentName;
  @JsonProperty(value = "Title")
  private String title;
  @JsonProperty(value = "FirstName")
  private String firstName;
  @JsonProperty(value = "LastName")
  private String lastName;
  @JsonProperty(value = "Suffix")
  private String suffix;
  @JsonProperty(value = "PhoneNumber")
  private String phoneNumber;
  @JsonProperty(value = "FaxNumber")
  private String faxNumber;

  /**
   * @return the id
   */
  public String getId() {
    return id;
  }

  /**
   * @param id
   *          the id to set
   */
  public void setId(final String id) {
    this.id = id;
  }

  /**
   * @return the addressType
   */
  public String getAddressType() {
    return addressType;
  }

  /**
   * @param addressType
   *          the addressType to set
   */
  public void setAddressType(final String addressType) {
    this.addressType = addressType;
  }

  /**
   * @return the addrTypeCD
   */
  public String getAddrTypeCD() {
    return addrTypeCD;
  }

  /**
   * @param addrTypeCD
   *          the addrTypeCD to set
   */
  public void setAddrTypeCD(final String addrTypeCD) {
    this.addrTypeCD = addrTypeCD;
  }

  /**
   * @return the addressCleanseCode
   */
  public String getAddressCleanseCode() {
    return addressCleanseCode;
  }

  /**
   * @param addressCleanseCode
   *          the addressCleanseCode to set
   */
  public void setAddressCleanseCode(final String addressCleanseCode) {
    this.addressCleanseCode = addressCleanseCode;
  }

  /**
   * @return the addressDeliveryConfidenceCode
   */
  public String getAddressDeliveryConfidenceCode() {
    return addressDeliveryConfidenceCode;
  }

  /**
   * @param addressDeliveryConfidenceCode
   *          the addressDeliveryConfidenceCode to set
   */
  public void setAddressDeliveryConfidenceCode(final String addressDeliveryConfidenceCode) {
    this.addressDeliveryConfidenceCode = addressDeliveryConfidenceCode;
  }

  /**
   * @return the addressEndDate
   */
  public String getAddressEndDate() {
    return addressEndDate;
  }

  /**
   * @param addressEndDate
   *          the addressEndDate to set
   */
  public void setAddressEndDate(final String addressEndDate) {
    this.addressEndDate = addressEndDate;
  }

  /**
   * @return the addressLine1
   */
  public String getAddressLine1() {
    return addressLine1;
  }

  /**
   * @param addressLine1
   *          the addressLine1 to set
   */
  public void setAddressLine1(final String addressLine1) {
    this.addressLine1 = addressLine1;
  }

  /**
   * @return the addressLine2
   */
  public String getAddressLine2() {
    return addressLine2;
  }

  /**
   * @param addressLine2
   *          the addressLine2 to set
   */
  public void setAddressLine2(final String addressLine2) {
    this.addressLine2 = addressLine2;
  }

  /**
   * @return the addressLine3
   */
  public String getAddressLine3() {
    return addressLine3;
  }

  /**
   * @param addressLine3
   *          the addressLine3 to set
   */
  public void setAddressLine3(final String addressLine3) {
    this.addressLine3 = addressLine3;
  }

  /**
   * @return the addressStartDate
   */
  public String getAddressStartDate() {
    return addressStartDate;
  }

  /**
   * @param addressStartDate
   *          the addressStartDate to set
   */
  public void setAddressStartDate(final String addressStartDate) {
    this.addressStartDate = addressStartDate;
  }

  /**
   * @return the addrTypeECID
   */
  public String getAddrTypeECID() {
    return addrTypeECID;
  }

  /**
   * @param addrTypeECID
   *          the addrTypeECID to set
   */
  public void setAddrTypeECID(final String addrTypeECID) {
    this.addrTypeECID = addrTypeECID;
  }

  /**
   * @return the city
   */
  public String getCity() {
    return city;
  }

  /**
   * @param city
   *          the city to set
   */
  public void setCity(final String city) {
    this.city = city;
  }

  /**
   * @return the countryCode
   */
  public String getCountryCode() {
    return countryCode;
  }

  /**
   * @param countryCode
   *          the countryCode to set
   */
  public void setCountryCode(final String countryCode) {
    this.countryCode = countryCode;
  }

  /**
   * @return the countryName
   */
  public String getCountryName() {
    return countryName;
  }

  /**
   * @param countryName
   *          the countryName to set
   */
  public void setCountryName(final String countryName) {
    this.countryName = countryName;
  }

  /**
   * @return the county
   */
  public String getCounty() {
    return county;
  }

  /**
   * @param county
   *          the county to set
   */
  public void setCounty(final String county) {
    this.county = county;
  }

  /**
   * @return the postBox
   */
  public String getPostBox() {
    return postBox;
  }

  /**
   * @param postBox
   *          the postBox to set
   */
  public void setPostBox(final String postBox) {
    this.postBox = postBox;
  }

  /**
   * @return the province
   */
  public String getProvince() {
    return province;
  }

  /**
   * @param province
   *          the province to set
   */
  public void setProvince(final String province) {
    this.province = province;
  }

  /**
   * @return the state
   */
  public String getState() {
    return state;
  }

  /**
   * @param state
   *          the state to set
   */
  public void setState(final String state) {
    this.state = state;
  }

  /**
   * @return the zipCode
   */
  public String getZipCode() {
    return zipCode;
  }

  /**
   * @param zipCode
   *          the zipCode to set
   */
  public void setZipCode(final String zipCode) {
    this.zipCode = zipCode;
  }

  /**
   * @return the addressCleaningFlag
   */
  public Boolean getAddressCleaningFlag() {
    return addressCleaningFlag;
  }

  /**
   * @param addressCleaningFlag
   *          the addressCleaningFlag to set
   */
  public void setAddressCleaningFlag(final Boolean addressCleaningFlag) {
    this.addressCleaningFlag = addressCleaningFlag;
  }

  /**
   * @return the institutionCd
   */
  public String getInstitutionCd() {
    return institutionCd;
  }

  /**
   * @param institutionCd
   *          the institutionCd to set
   */
  public void setInstitutionCd(final String institutionCd) {
    this.institutionCd = institutionCd;
  }

  /**
   * @return the institutionName
   */
  public String getInstitutionName() {
    return institutionName;
  }

  /**
   * @param institutionName
   *          the institutionName to set
   */
  public void setInstitutionName(final String institutionName) {
    this.institutionName = institutionName;
  }

  /**
   * @return the departmentCd
   */
  public String getDepartmentCd() {
    return departmentCd;
  }

  /**
   * @param departmentCd
   *          the departmentCd to set
   */
  public void setDepartmentCd(final String departmentCd) {
    this.departmentCd = departmentCd;
  }

  /**
   * @return the departmentName
   */
  public String getDepartmentName() {
    return departmentName;
  }

  /**
   * @param departmentName
   *          the departmentName to set
   */
  public void setDepartmentName(final String departmentName) {
    this.departmentName = departmentName;
  }

  /**
   * @return the title
   */
  public String getTitle() {
    return title;
  }

  /**
   * @param title
   *          the title to set
   */
  public void setTitle(final String title) {
    this.title = title;
  }

  /**
   * @return the firstName
   */
  public String getFirstName() {
    return firstName;
  }

  /**
   * @param firstName
   *          the firstName to set
   */
  public void setFirstName(final String firstName) {
    this.firstName = firstName;
  }

  /**
   * @return the lastName
   */
  public String getLastName() {
    return lastName;
  }

  /**
   * @param lastName
   *          the lastName to set
   */
  public void setLastName(final String lastName) {
    this.lastName = lastName;
  }

  /**
   * @return the suffix
   */
  public String getSuffix() {
    return suffix;
  }

  /**
   * @param suffix
   *          the suffix to set
   */
  public void setSuffix(final String suffix) {
    this.suffix = suffix;
  }

  /**
   * @return the phoneNumber
   */
  public String getPhoneNumber() {
    return phoneNumber;
  }

  /**
   * @param phoneNumber
   *          the phoneNumber to set
   */
  public void setPhoneNumber(final String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  /**
   * @return the faxNumber
   */
  public String getFaxNumber() {
    return faxNumber;
  }

  /**
   * @param faxNumber
   *          the faxNumber to set
   */
  public void setFaxNumber(final String faxNumber) {
    this.faxNumber = faxNumber;
  }


  @Override
  public String toString() {
    return "Address [id=" + id + ", addressType=" + addressType + ", addrTypeCD=" + addrTypeCD
        + ", addressCleanseCode=" + addressCleanseCode + ", addressDeliveryConfidenceCode="
        + addressDeliveryConfidenceCode + ", addressEndDate=" + addressEndDate + ", addressLine1="
        + addressLine1 + ", addressLine2=" + addressLine2 + ", addressLine3=" + addressLine3
        + ", addressStartDate=" + addressStartDate + ", addrTypeECID=" + addrTypeECID + ", city="
        + city + ", countryCode=" + countryCode + ", countryName=" + countryName + ", county="
        + county + ", postBox=" + postBox + ", province=" + province + ", state=" + state
        + ", zipCode=" + zipCode + ", addressCleaningFlag=" + addressCleaningFlag
        + ", institutionCd=" + institutionCd + ", institutionName=" + institutionName
        + ", departmentCd=" + departmentCd + ", departmentName=" + departmentName + ", title="
        + title + ", firstName=" + firstName + ", lastName=" + lastName + ", suffix=" + suffix
        + ", phoneNumber=" + phoneNumber + ", faxNumber=" + faxNumber + "]";
  }



}
