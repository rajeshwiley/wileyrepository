package com.wiley.integrations.promotioncode;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.voucher.VoucherService;

import java.util.Collection;
import java.util.Iterator;
import java.util.Optional;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.order.WileyCheckoutService;
import com.wiley.core.partner.WileyPartnerService;

import static org.springframework.util.Assert.notNull;


/**
 * Implements basic logic for resolving which discount code should be sent to 3-rd party system for integration
 */
public abstract class AbstractPromotionCodeProviderImpl implements PromotionCodeProvider
{
	private static final String STUDENT_DISCOUNT_CODE = "STUDENT";

	@Resource
	private VoucherService voucherService;

	@Autowired
	private WileyCheckoutService wileyCheckoutService;

	@Autowired
	private WileyPartnerService partnerService;


	/**
	 * Determines if student discount should be taken into account if applied to order
	 *
	 * @return boolean
	 */
	protected abstract boolean includeStudent();

	/**
	 * Gets a promotion code for the order according to the business rules.
	 *
	 * @param orderModel
	 * 		order
	 * @return null if no promotion code applied, or the promotion code otherwise
	 */
	@Override
	@Nonnull
	public Optional<String> getOrderPromotionCode(@Nonnull final OrderModel orderModel)
	{
		notNull(orderModel);

		//** 1 ** The voucher's code will be passed along if a voucher was applied.
		//        If more than one voucher was applied, the first voucher's code is sent.
		final Optional<String> appliedVoucherCodes = getVoucherCode(orderModel);
		if (appliedVoucherCodes.isPresent())
		{
			return Optional.of(appliedVoucherCodes.get());
		}

		//** 2 ** The partner's discount code will be passed along if the order was placed using a partner checkout flow.
		final Optional<String> partnerCode = getPartnerCode(orderModel);
		if (partnerCode.isPresent())
		{
			return Optional.of(partnerCode.get());
		}

		//** 3 ** The student's discount code will be passed along if the order was placed using a student checkout flow.
		// Currently Hybris uses the same discount code for all students.
		if (includeStudent())
		{
			final Optional<String> studentCode = getStudentCode(orderModel);
			if (studentCode.isPresent())
			{
				return Optional.of(studentCode.get());
			}
		}

		return Optional.empty();
	}

	private Optional<String> getVoucherCode(final OrderModel orderModel)
	{
		Optional<String> voucherCode = Optional.empty();

		final Collection<String> voucherCodesAppliedToOrder = voucherService.getAppliedVoucherCodes(orderModel);

		if (voucherCodesAppliedToOrder != null)
		{
			final Iterator<String> iterator = voucherCodesAppliedToOrder.iterator();
			if (iterator.hasNext())
			{
				voucherCode = Optional.ofNullable(iterator.next());
			}
		}
		return voucherCode;
	}

	private Optional<String> getPartnerCode(final AbstractOrderModel orderModel)
	{
		return partnerService.getPartnerCode(orderModel);
	}

	private Optional<String> getStudentCode(final AbstractOrderModel orderModel)
	{
		String studentCode = null;

		if (wileyCheckoutService.isStudentOrder(orderModel))
		{
			studentCode = STUDENT_DISCOUNT_CODE;
		}

		return Optional.ofNullable(studentCode);
	}
}
