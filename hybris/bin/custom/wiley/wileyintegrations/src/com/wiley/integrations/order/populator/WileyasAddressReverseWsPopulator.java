package com.wiley.integrations.order.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import javax.annotation.Resource;

import com.wiley.integrations.exceptions.carts.UnknownCountryException;
import com.wiley.integrations.exceptions.carts.UnknownRegionException;
import com.wiley.integrations.order.dto.AddressWsDTO;

import static com.wiley.integrations.utils.PopulateUtils.populateFieldIfNotNull;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


public class WileyasAddressReverseWsPopulator implements Populator<AddressWsDTO, AddressModel>
{
	@Resource
	private CommonI18NService commonI18NService;

	@Override
	public void populate(final AddressWsDTO source, final AddressModel target) throws ConversionException
	{
		validateParameterNotNullStandardMessage("source", source);
		validateParameterNotNullStandardMessage("target", target);

		populateFieldIfNotNull(AddressModel::setPostalcode, target, source.getPostcode());

		populateCountryAndRegion(target, source);

		populateFieldIfNotNull(AddressModel::setTown, target, source.getCity());
		populateFieldIfNotNull(AddressModel::setLine1, target, source.getLine1());
		populateFieldIfNotNull(AddressModel::setLine2, target, source.getLine2());
		populateFieldIfNotNull(AddressModel::setFirstname, target, source.getFirstName());
		populateFieldIfNotNull(AddressModel::setLastname, target, source.getLastName());
		populateFieldIfNotNull(AddressModel::setPhone1, target, source.getPhoneNumber());
		populateFieldIfNotNull(AddressModel::setEmail, target, source.getEmail());
		populateFieldIfNotNull(AddressModel::setCompany, target, source.getOrganization());
		populateFieldIfNotNull(AddressModel::setDepartment, target, source.getDepartment());
	}

	private void populateCountryAndRegion(final AddressModel target, final AddressWsDTO source)
	{
		String countryIsoCode = source.getCountry();
		if (countryIsoCode != null)
		{
			CountryModel countryModel;
			try
			{
				countryModel = getCommonI18NService().getCountry(countryIsoCode);
			}
			catch (UnknownIdentifierException ex)
			{
				throw new UnknownCountryException(ex.getMessage());
			}
			target.setCountry(countryModel);
			String regionIsoCodeShort = source.getState();
			if (regionIsoCodeShort != null)
			{
				String regionIsoCode = countryModel.getIsocode() + "-" + source.getState();
				RegionModel regionModel;
				try
				{
					regionModel = getCommonI18NService().getRegion(countryModel, regionIsoCode);
				}
				catch (UnknownIdentifierException ex)
				{
					throw new UnknownRegionException(ex.getMessage());
				}
				target.setRegion(regionModel);
			}
		}
	}

	public CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}
}
