package com.wiley.integrations.order.exception;

import com.wiley.integrations.order.dto.OrderEntryWsDTO;
import org.springframework.dao.DuplicateKeyException;

/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public class DuplicateEntryBusinessKeyException extends DuplicateKeyException
{
    private final OrderEntryWsDTO orderEntry;

    public DuplicateEntryBusinessKeyException(final OrderEntryWsDTO orderEntry, final String message)
    {
        super(message);
        this.orderEntry = orderEntry;
    }

    public OrderEntryWsDTO getOrderEntry()
    {
        return orderEntry;
    }
}
