package com.wiley.integrations.sabrix.dto;

import java.util.Objects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * List of established locations.
 *
 *
 * <p>Java class for EstablishmentsType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="EstablishmentsType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;all&gt;
 *         &lt;element name="BUYER_ROLE" type="{http://www.sabrix.com/services/taxcalculationservice/2011-09-01}EstablishmentsLocationType" minOccurs="0"/&gt;
 *         &lt;element name="MIDDLEMAN_ROLE" type="{http://www.sabrix.com/services/taxcalculationservice/2011-09-01}EstablishmentsLocationType" minOccurs="0"/&gt;
 *         &lt;element name="SELLER_ROLE" type="{http://www.sabrix.com/services/taxcalculationservice/2011-09-01}EstablishmentsLocationType" minOccurs="0"/&gt;
 *       &lt;/all&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EstablishmentsType", propOrder = {

})
public class EstablishmentsType
{

	@XmlElement(name = "BUYER_ROLE")
	protected EstablishmentsLocationType buyerrole;
	@XmlElement(name = "MIDDLEMAN_ROLE")
	protected EstablishmentsLocationType middlemanrole;
	@XmlElement(name = "SELLER_ROLE")
	protected EstablishmentsLocationType sellerrole;

	/**
	 * Gets the value of the buyerrole property.
	 *
	 * @return possible object is
	 * {@link EstablishmentsLocationType }
	 */
	public EstablishmentsLocationType getBUYERROLE()
	{
		return buyerrole;
	}

	/**
	 * Sets the value of the buyerrole property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link EstablishmentsLocationType }
	 */
	public void setBUYERROLE(EstablishmentsLocationType value)
	{
		this.buyerrole = value;
	}

	/**
	 * Gets the value of the middlemanrole property.
	 *
	 * @return possible object is
	 * {@link EstablishmentsLocationType }
	 */
	public EstablishmentsLocationType getMIDDLEMANROLE()
	{
		return middlemanrole;
	}

	/**
	 * Sets the value of the middlemanrole property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link EstablishmentsLocationType }
	 */
	public void setMIDDLEMANROLE(EstablishmentsLocationType value)
	{
		this.middlemanrole = value;
	}

	/**
	 * Gets the value of the sellerrole property.
	 *
	 * @return possible object is
	 * {@link EstablishmentsLocationType }
	 */
	public EstablishmentsLocationType getSELLERROLE()
	{
		return sellerrole;
	}

	/**
	 * Sets the value of the sellerrole property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link EstablishmentsLocationType }
	 */
	public void setSELLERROLE(EstablishmentsLocationType value)
	{
		this.sellerrole = value;
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		final EstablishmentsType that = (EstablishmentsType) o;
		return Objects.equals(buyerrole, that.buyerrole) &&
				Objects.equals(middlemanrole, that.middlemanrole) &&
				Objects.equals(sellerrole, that.sellerrole);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(buyerrole, middlemanrole, sellerrole);
	}
}
