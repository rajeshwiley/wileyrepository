package com.wiley.integrations.esb.transformer;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.CartModel;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.wiley.integrations.esb.dto.delivery.DeliveryAddressDto;
import com.wiley.integrations.esb.dto.delivery.DeliveryOptionRequestDto;
import com.wiley.integrations.esb.dto.delivery.OrderEntryDto;


/**
 * Created by Uladzimir_Barouski on 6/16/2016.
 */
public class CartToDeliveryOptionRequestDtoTransformer
{
	@Resource
	private Populator<CartModel, DeliveryOptionRequestDto> cartToDeliveryOptionRequestDtoSimplePopulator;

	@Resource
	private Populator<CartModel, DeliveryAddressDto> cartToDeliveryAddressDtoPopulator;

	@Resource
	private Populator<CartModel, List<OrderEntryDto>> cartToOrderEntryDtoListPopulator;

	@Nonnull
	public DeliveryOptionRequestDto transform(@Nonnull final CartModel cart)
	{
		Assert.notNull(cart);

		DeliveryOptionRequestDto deliveryOptionRequestDto = new DeliveryOptionRequestDto();

		// populate simple fields
		cartToDeliveryOptionRequestDtoSimplePopulator.populate(cart, deliveryOptionRequestDto);

		// populate delivery Address
		deliveryOptionRequestDto.setDeliveryAddress(new DeliveryAddressDto());
		cartToDeliveryAddressDtoPopulator.populate(cart, deliveryOptionRequestDto.getDeliveryAddress());

		//populate Order Entries
		deliveryOptionRequestDto.setEntries(new ArrayList<>());
		cartToOrderEntryDtoListPopulator.populate(cart, deliveryOptionRequestDto.getEntries());
		return deliveryOptionRequestDto;
	}
}
