package com.wiley.integrations.esb.subscription.dto;

import java.util.Objects;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * An address.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Wileyb2cSubscriptionDeliveryAddressRequestDto
{
	@NotNull
	@Size(min = 1, max = 255)
	private String postcode = null;

	@NotNull
	@Size(min = 1)
	@Pattern(regexp = "^[A-Z]{2}$")
	private String country = null;

	@Pattern(regexp = "^[0-9A-Z]{2}$")
	private String state = null;

	@NotNull
	@Size(min = 1, max = 255)
	private String city = null;

	@NotNull
	@Size(min = 1, max = 255)
	private String line1 = null;

	@Size(max = 255)
	private String line2 = null;
	
	@NotNull
	@Size(max = 255)
	private String firstName = null;

	@NotNull
	@Size(max = 255)
	private String lastName = null;

	@Size(max = 255)
	private String phoneNumber = null;

	public Wileyb2cSubscriptionDeliveryAddressRequestDto postcode(final String postcode)
	{
		this.postcode = postcode;
		return this;
	}

	/**
	 * Postal code.
	 *
	 * @return postcode
	 **/
	@JsonProperty(required = true, value = "postcode")
	public String getPostcode()
	{
		return postcode;
	}

	public void setPostcode(final String postcode)
	{
		this.postcode = postcode;
	}

	public Wileyb2cSubscriptionDeliveryAddressRequestDto country(final String country)
	{
		this.country = country;
		return this;
	}

	/**
	 * Two-letter ISO 3166-1 alpha-2 code that specifies a country. See https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2
	 * for details.
	 *
	 * @return country
	 **/
	@JsonProperty(required = true, value = "country")
	public String getCountry()
	{
		return country;
	}

	public void setCountry(final String country)
	{
		this.country = country;
	}

	public Wileyb2cSubscriptionDeliveryAddressRequestDto state(final String state)
	{
		this.state = state;
		return this;
	}

	/**
	 * Two-letter ISO code that specifies a state or province. The codes were derived from ISO 3166-2 codes, for
	 * non-standard Armed Forces regions values were provided by Wiley. See https://jira.wiley.ru/browse/ECSC-5892 for
	 * details.
	 *
	 * @return state
	 **/
	@JsonProperty(value = "state")
	public String getState()
	{
		return state;
	}

	public void setState(final String state)
	{
		this.state = state;
	}

	public Wileyb2cSubscriptionDeliveryAddressRequestDto city(final String city)
	{
		this.city = city;
		return this;
	}

	/**
	 * Destination city.
	 *
	 * @return city
	 **/
	@JsonProperty(required = true, value = "city")
	public String getCity()
	{
		return city;
	}

	public void setCity(final String city)
	{
		this.city = city;
	}

	public Wileyb2cSubscriptionDeliveryAddressRequestDto line1(final String line1)
	{
		this.line1 = line1;
		return this;
	}

	/**
	 * First line of the address.
	 *
	 * @return line1
	 **/
	@JsonProperty(required = true, value = "line1")
	public String getLine1()
	{
		return line1;
	}

	public void setLine1(final String line1)
	{
		this.line1 = line1;
	}

	public Wileyb2cSubscriptionDeliveryAddressRequestDto line2(final String line2)
	{
		this.line2 = line2;
		return this;
	}

	/**
	 * Second line of the address.
	 *
	 * @return line2
	 **/
	@JsonProperty(value = "line2")
	public String getLine2()
	{
		return line2;
	}

	public void setLine2(final String line2)
	{
		this.line2 = line2;
	}

	public Wileyb2cSubscriptionDeliveryAddressRequestDto firstName(final String firstName)
	{
		this.firstName = firstName;
		return this;
	}

	/**
	 * Addressee's first name.
	 *
	 * @return firstName
	 **/
	@JsonProperty(value = "firstName")
	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(final String firstName)
	{
		this.firstName = firstName;
	}

	public Wileyb2cSubscriptionDeliveryAddressRequestDto lastName(final String lastName)
	{
		this.lastName = lastName;
		return this;
	}

	/**
	 * Addressee's last name.
	 *
	 * @return lastName
	 **/
	@JsonProperty(value = "lastName")
	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(final String lastName)
	{
		this.lastName = lastName;
	}

	public Wileyb2cSubscriptionDeliveryAddressRequestDto phoneNumber(final String phoneNumber)
	{
		this.phoneNumber = phoneNumber;
		return this;
	}

	/**
	 * Phone number.
	 *
	 * @return phoneNumber
	 **/
	@JsonProperty(value = "phoneNumber")
	public String getPhoneNumber()
	{
		return phoneNumber;
	}

	public void setPhoneNumber(final String phoneNumber)
	{
		this.phoneNumber = phoneNumber;
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}

		if (o == null || getClass() != o.getClass())
		{
			return false;
		}

		final Wileyb2cSubscriptionDeliveryAddressRequestDto shippingAddress = (Wileyb2cSubscriptionDeliveryAddressRequestDto) o;

		return Objects.equals(this.postcode, shippingAddress.postcode) && Objects.equals(this.country, shippingAddress.country)
				&& Objects.equals(this.state, shippingAddress.state) && Objects.equals(this.city, shippingAddress.city)
				&& Objects.equals(this.line1, shippingAddress.line1) && Objects.equals(this.line2, shippingAddress.line2)
				&& Objects.equals(this.firstName, shippingAddress.firstName)
				&& Objects.equals(this.lastName, shippingAddress.lastName)
				&& Objects.equals(this.phoneNumber, shippingAddress.phoneNumber);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(postcode, country, state, city, line1, line2, firstName, lastName, phoneNumber);
	}

	@Override
	public String toString()
	{
		final StringBuilder sb = new StringBuilder();
		sb.append("class Wileyb2cSubscriptionDeliveryAddressRequestDto {\n");

		sb.append("    postcode: ").append(toIndentedString(postcode)).append("\n");
		sb.append("    country: ").append(toIndentedString(country)).append("\n");
		sb.append("    state: ").append(toIndentedString(state)).append("\n");
		sb.append("    city: ").append(toIndentedString(city)).append("\n");
		sb.append("    line1: ").append(toIndentedString(line1)).append("\n");
		sb.append("    line2: ").append(toIndentedString(line2)).append("\n");
		sb.append("    firstName: ").append(toIndentedString(firstName)).append("\n");
		sb.append("    lastName: ").append(toIndentedString(lastName)).append("\n");
		sb.append("    phoneNumber: ").append(toIndentedString(phoneNumber)).append("\n");
		sb.append("}");

		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces (except the first line).
	 */
	private String toIndentedString(final Object o)
	{
		if (o == null)
		{
			return "null";
		}

		return o.toString().replace("\n", "\n    ");
	}
}
