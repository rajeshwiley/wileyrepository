/**
 *
 */
package com.wiley.integrations.cdm.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.constraints.NotNull;

import com.wiley.integrations.cdm.dto.CDMAddressDTO;


public class AddressToCDMAddressPopulator implements Populator<AddressModel, CDMAddressDTO> {

  private String addressType;
  private String addressTypeCode;

  private static final String CREATE_CUSTOMER_DATE_FORMAT = "yyyy-MM-dd";

  /**
   * Populating the addressModel Data in CDMAddress for the request
   *
   */
  @Override
  public void populate(@NotNull final AddressModel source, @NotNull final CDMAddressDTO target)
      throws ConversionException {
    target.setAddressLine1(source.getLine1());
    target.setAddressLine2(source.getLine2());
    target.setAddressType(getAddressType());
    target.setAddrTypeCD(getAddressTypeCode());
    target.setAddressStartDate(formatDate(source.getCreationtime()));
    target.setCity(source.getTown());
    target.setCountryCode(source.getCountry().getIsocode3());
    target.setCountryName(source.getCountry().getName());
    addState(source.getRegion(), target);
    target.setZipCode(source.getPostalcode());
    target.setFirstName(source.getFirstname());
    target.setLastName(source.getLastname());
    target.setPhoneNumber(source.getPhone1());
  }

  private void addState(final RegionModel regionModel, final CDMAddressDTO target) {
    target.setState((regionModel != null) ? regionModel.getIsocodeShort() : null);
  }

  private String formatDate(final Date creationtime) {
    final DateFormat dateFormat = new SimpleDateFormat(CREATE_CUSTOMER_DATE_FORMAT);
    return dateFormat.format(creationtime);
  }

  /**
   * @return the addressType
   */
  public String getAddressType() {
    return addressType;
  }

  /**
   * @param addressType
   *          the addressType to set
   */
  public void setAddressType(final String addressType) {
    this.addressType = addressType;
  }


  /**
   * @return the addressCode
   */
  public String getAddressTypeCode() {
    return addressTypeCode;
  }

  /**
   * @param addressTypeCode
   *          the addressCode to set
   */
  public void setAddressTypeCode(final String addressTypeCode) {
    this.addressTypeCode = addressTypeCode;
  }
}
