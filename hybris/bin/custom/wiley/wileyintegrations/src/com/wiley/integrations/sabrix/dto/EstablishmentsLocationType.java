package com.wiley.integrations.sabrix.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Establishment for each location type (true or false).
 *
 *
 * <p>Java class for EstablishmentsLocationType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="EstablishmentsLocationType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;all&gt;
 *         &lt;element name="BILL_TO" type="{http://www.sabrix.com/services/taxcalculationservice/2011-09-01}BooleanType" minOccurs="0"/&gt;
 *         &lt;element name="BUYER_PRIMARY" type="{http://www.sabrix.com/services/taxcalculationservice/2011-09-01}BooleanType" minOccurs="0"/&gt;
 *         &lt;element name="MIDDLEMAN" type="{http://www.sabrix.com/services/taxcalculationservice/2011-09-01}BooleanType" minOccurs="0"/&gt;
 *         &lt;element name="ORDER_ACCEPTANCE" type="{http://www.sabrix.com/services/taxcalculationservice/2011-09-01}BooleanType" minOccurs="0"/&gt;
 *         &lt;element name="ORDER_ORIGIN" type="{http://www.sabrix.com/services/taxcalculationservice/2011-09-01}BooleanType" minOccurs="0"/&gt;
 *         &lt;element name="SELLER_PRIMARY" type="{http://www.sabrix.com/services/taxcalculationservice/2011-09-01}BooleanType" minOccurs="0"/&gt;
 *         &lt;element name="SHIP_FROM" type="{http://www.sabrix.com/services/taxcalculationservice/2011-09-01}BooleanType" minOccurs="0"/&gt;
 *         &lt;element name="SHIP_TO" type="{http://www.sabrix.com/services/taxcalculationservice/2011-09-01}BooleanType" minOccurs="0"/&gt;
 *         &lt;element name="SUPPLY" type="{http://www.sabrix.com/services/taxcalculationservice/2011-09-01}BooleanType" minOccurs="0"/&gt;
 *       &lt;/all&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EstablishmentsLocationType", propOrder = {

})
public class EstablishmentsLocationType
{

	@XmlElement(name = "BILL_TO")
	@XmlSchemaType(name = "anySimpleType")
	protected String billto;
	@XmlElement(name = "BUYER_PRIMARY")
	@XmlSchemaType(name = "anySimpleType")
	protected String buyerprimary;
	@XmlElement(name = "MIDDLEMAN")
	@XmlSchemaType(name = "anySimpleType")
	protected String middleman;
	@XmlElement(name = "ORDER_ACCEPTANCE")
	@XmlSchemaType(name = "anySimpleType")
	protected String orderacceptance;
	@XmlElement(name = "ORDER_ORIGIN")
	@XmlSchemaType(name = "anySimpleType")
	protected String orderorigin;
	@XmlElement(name = "SELLER_PRIMARY")
	@XmlSchemaType(name = "anySimpleType")
	protected String sellerprimary;
	@XmlElement(name = "SHIP_FROM")
	@XmlSchemaType(name = "anySimpleType")
	protected String shipfrom;
	@XmlElement(name = "SHIP_TO")
	@XmlSchemaType(name = "anySimpleType")
	protected String shipto;
	@XmlElement(name = "SUPPLY")
	@XmlSchemaType(name = "anySimpleType")
	protected String supply;

	/**
	 * Gets the value of the billto property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getBILLTO()
	{
		return billto;
	}

	/**
	 * Sets the value of the billto property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setBILLTO(String value)
	{
		this.billto = value;
	}

	/**
	 * Gets the value of the buyerprimary property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getBUYERPRIMARY()
	{
		return buyerprimary;
	}

	/**
	 * Sets the value of the buyerprimary property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setBUYERPRIMARY(String value)
	{
		this.buyerprimary = value;
	}

	/**
	 * Gets the value of the middleman property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getMIDDLEMAN()
	{
		return middleman;
	}

	/**
	 * Sets the value of the middleman property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setMIDDLEMAN(String value)
	{
		this.middleman = value;
	}

	/**
	 * Gets the value of the orderacceptance property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getORDERACCEPTANCE()
	{
		return orderacceptance;
	}

	/**
	 * Sets the value of the orderacceptance property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setORDERACCEPTANCE(String value)
	{
		this.orderacceptance = value;
	}

	/**
	 * Gets the value of the orderorigin property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getORDERORIGIN()
	{
		return orderorigin;
	}

	/**
	 * Sets the value of the orderorigin property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setORDERORIGIN(String value)
	{
		this.orderorigin = value;
	}

	/**
	 * Gets the value of the sellerprimary property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getSELLERPRIMARY()
	{
		return sellerprimary;
	}

	/**
	 * Sets the value of the sellerprimary property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setSELLERPRIMARY(String value)
	{
		this.sellerprimary = value;
	}

	/**
	 * Gets the value of the shipfrom property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getSHIPFROM()
	{
		return shipfrom;
	}

	/**
	 * Sets the value of the shipfrom property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setSHIPFROM(String value)
	{
		this.shipfrom = value;
	}

	/**
	 * Gets the value of the shipto property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getSHIPTO()
	{
		return shipto;
	}

	/**
	 * Sets the value of the shipto property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setSHIPTO(String value)
	{
		this.shipto = value;
	}

	/**
	 * Gets the value of the supply property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getSUPPLY()
	{
		return supply;
	}

	/**
	 * Sets the value of the supply property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setSUPPLY(String value)
	{
		this.supply = value;
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}

		final EstablishmentsLocationType that = (EstablishmentsLocationType) o;

		if (billto != null ? !billto.equals(that.billto) : that.billto != null)
		{
			return false;
		}
		if (buyerprimary != null ? !buyerprimary.equals(that.buyerprimary) : that.buyerprimary != null)
		{
			return false;
		}
		if (middleman != null ? !middleman.equals(that.middleman) : that.middleman != null)
		{
			return false;
		}
		if (orderacceptance != null ? !orderacceptance.equals(that.orderacceptance) : that.orderacceptance != null)
		{
			return false;
		}
		if (orderorigin != null ? !orderorigin.equals(that.orderorigin) : that.orderorigin != null)
		{
			return false;
		}
		if (sellerprimary != null ? !sellerprimary.equals(that.sellerprimary) : that.sellerprimary != null)
		{
			return false;
		}
		if (shipfrom != null ? !shipfrom.equals(that.shipfrom) : that.shipfrom != null)
		{
			return false;
		}
		if (shipto != null ? !shipto.equals(that.shipto) : that.shipto != null)
		{
			return false;
		}
		return supply != null ? supply.equals(that.supply) : that.supply == null;

	}

	@Override
	public int hashCode()
	{
		int result = billto != null ? billto.hashCode() : 0;
		result = 31 * result + (buyerprimary != null ? buyerprimary.hashCode() : 0);
		result = 31 * result + (middleman != null ? middleman.hashCode() : 0);
		result = 31 * result + (orderacceptance != null ? orderacceptance.hashCode() : 0);
		result = 31 * result + (orderorigin != null ? orderorigin.hashCode() : 0);
		result = 31 * result + (sellerprimary != null ? sellerprimary.hashCode() : 0);
		result = 31 * result + (shipfrom != null ? shipfrom.hashCode() : 0);
		result = 31 * result + (shipto != null ? shipto.hashCode() : 0);
		result = 31 * result + (supply != null ? supply.hashCode() : 0);
		return result;
	}
}
