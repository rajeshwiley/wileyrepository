package com.wiley.integrations.esb.transformer.b2baccounts;

import javax.annotation.Nonnull;

import org.springframework.util.Assert;

import com.wiley.integrations.esb.dto.b2baccounts.OwnershipResultResponseDto;


/**
 * Created by Raman_Hancharou on 8/9/2016.
 */
public class OwnershipResultResponseToBooleanTransformer
{

	@Nonnull
	public Boolean transform(@Nonnull final OwnershipResultResponseDto ownershipResultResponseDto)
	{
		Assert.notNull(ownershipResultResponseDto);
		Assert.notNull(ownershipResultResponseDto.getProductPurchased());
		return ownershipResultResponseDto.getProductPurchased();
	}

}
