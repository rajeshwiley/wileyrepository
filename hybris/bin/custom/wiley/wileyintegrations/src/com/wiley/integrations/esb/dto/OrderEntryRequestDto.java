package com.wiley.integrations.esb.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.common.base.MoreObjects;


/**
 * Dto for cart calculation and validation in ERP.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderEntryRequestDto
{
	@XmlElement(required = true)
	private Integer entryNumber;

	@XmlElement(required = true)
	private String productCode;

	@XmlElement(required = true)
	private Long quantity;

	@XmlElement(required = true)
	private String unitCode;

	@XmlElement(required = false)
	private Double weight;

	public Integer getEntryNumber()
	{
		return entryNumber;
	}

	public void setEntryNumber(final Integer entryNumber)
	{
		this.entryNumber = entryNumber;
	}

	public String getProductCode()
	{
		return productCode;
	}

	public void setProductCode(final String productCode)
	{
		this.productCode = productCode;
	}

	public Long getQuantity()
	{
		return quantity;
	}

	public void setQuantity(final Long quantity)
	{
		this.quantity = quantity;
	}

	public String getUnitCode()
	{
		return unitCode;
	}

	public void setUnitCode(final String unitCode)
	{
		this.unitCode = unitCode;
	}

	public Double getWeight()
	{
		return weight;
	}

	public void setWeight(final Double weight)
	{
		this.weight = weight;
	}

	@Override
	public String toString()
	{
		return MoreObjects.toStringHelper(this)
				.add("entryNumber", entryNumber)
				.add("productCode", productCode)
				.add("quantity", quantity)
				.add("unitCode", unitCode)
				.add("weight", weight)
				.toString();
	}
}
