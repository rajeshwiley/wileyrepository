package com.wiley.integrations.sabrix.transformer;

import de.hybris.platform.core.model.order.AbstractOrderModel;

import javax.annotation.Resource;

import com.wiley.integrations.sabrix.dto.TaxCalculationRequest;
import com.wiley.integrations.sabrix.dto.TaxCalculationRequestEBMType;
import com.wiley.integrations.sabrix.dto.UserDataType;
import com.wiley.integrations.sabrix.transformer.populators.OrderToTaxCalculationRequestPopulator;
import com.wiley.integrations.sabrix.transformer.populators.UserDataPopulator;


public class OrderToTaxCalculationRequestEBMTypeTransformer
{

	@Resource
	private UserDataPopulator userDataPopulator;
	@Resource
	private OrderToTaxCalculationRequestPopulator orderToTaxCalculationRequestPopulator;

	public TaxCalculationRequestEBMType transform(final AbstractOrderModel order)
	{
		TaxCalculationRequestEBMType topType = new TaxCalculationRequestEBMType();
		UserDataType userData = new UserDataType();
		userDataPopulator.populate(null, userData);
		topType.setUserData(userData);
		TaxCalculationRequest taxCalculationRequest = new TaxCalculationRequest();
		orderToTaxCalculationRequestPopulator.populate(order, taxCalculationRequest);
		topType.setTaxCalculationRequest(taxCalculationRequest);
		return topType;
	}


}
