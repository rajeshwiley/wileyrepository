package com.wiley.integrations.esb.converters.populator.order;

import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.Map;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.wiley.core.enums.PaymentModeEnum;
import com.wiley.core.exceptions.ExternalSystemException;
import com.wiley.core.model.ExternalDeliveryModeModel;
import com.wiley.core.servicelayer.i18n.WileycomI18NService;
import com.wiley.integrations.esb.dto.OrderRequestDto;
import com.wiley.integrations.esb.dto.verifyandcalculatecart.CartCalculationRequest;


/**
 * Populates simple fields of {@link OrderRequestDto}.
 */
public class CartToCartCalculationRequestSimplePopulator implements Populator<CartModel, CartCalculationRequest>
{

	@Resource
	private WileycomI18NService wileycomI18NService;

	@Resource
	private CommerceCommonI18NService commerceCommonI18NService;

	@Resource
	private Map<String, PaymentModeEnum> paymentModeMap;

	@Override
	public void populate(@Nonnull final CartModel cartModel, @Nonnull final CartCalculationRequest orderDto)
			throws ConversionException
	{
		Assert.notNull(cartModel);
		Assert.notNull(orderDto);

		final CustomerModel customerModel = (CustomerModel) cartModel.getUser();
		Assert.notNull(customerModel);

		orderDto.setUserId(customerModel.getContactEmail());
		orderDto.setCurrency(cartModel.getCurrency().getIsocode());

		populateCountry(orderDto, cartModel);
		populateLanguage(orderDto);
		populateDeliveryMode(cartModel, orderDto);
		populatePaymentMode(cartModel, orderDto);
		populateCreditCardType(cartModel, orderDto);
	}

	private void populateCreditCardType(final CartModel cartModel, final CartCalculationRequest orderDto)
	{
		final PaymentInfoModel paymentInfo = cartModel.getPaymentInfo();

		if (paymentInfo != null
				&& PaymentModeEnum.CARD.equals(paymentModeMap.get(paymentInfo.getItemtype()))
				&& paymentInfo instanceof CreditCardPaymentInfoModel)
		{
			orderDto.setCreditCardType(((CreditCardPaymentInfoModel) paymentInfo).getType().toString());
		}
	}

	private void populateLanguage(final CartCalculationRequest orderDto)
	{
		final LanguageModel currentLanguage = commerceCommonI18NService.getCurrentLanguage();

		if (currentLanguage != null)
		{
			orderDto.setLanguage(currentLanguage.getIsocode());
		}
	}

	private void populateCountry(final CartCalculationRequest orderDto, final CartModel cartModel)
	{
		CountryModel country = cartModel.getCountry();

		if (country == null)
		{
			country = wileycomI18NService.getCurrentCountry().orElse(null);
		}

		if (country != null)
		{
			orderDto.setCountry(country.getIsocode());
		}
	}

	private void populatePaymentMode(final CartModel cartModel, final CartCalculationRequest orderDto)
	{
		final PaymentInfoModel paymentInfo = cartModel.getPaymentInfo();
		if (paymentInfo != null)
		{
			orderDto.setPaymentOptionCode(paymentModeMap.get(paymentInfo.getItemtype()).getCode());
		}
	}

	private void populateDeliveryMode(final CartModel cartModel, final CartCalculationRequest orderDto)
	{
		final DeliveryModeModel deliveryMode = cartModel.getDeliveryMode();
		if (deliveryMode != null)
		{
			if (deliveryMode instanceof ExternalDeliveryModeModel)
			{
				orderDto.setDeliveryOptionCode(((ExternalDeliveryModeModel) deliveryMode).getExternalCode());
			}
			else
			{
				throw new ExternalSystemException(
						String.format("Cart [%s] has invalid delivery mode [%s]. Expected ExternalDeliveryMode.",
								cartModel.getCode(), deliveryMode));
			}
		}
	}

}
