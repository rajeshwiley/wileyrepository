package com.wiley.integrations.inventory.status;

import java.util.List;


/**
 * Created by Mikhail_Asadchy on 7/27/2016.
 */
public class InventoryCheckResponse
{
	private List<InventoryCheckResponseEntry> entries;

	public List<InventoryCheckResponseEntry> getEntries()
	{
		return entries;
	}

	public void setEntries(final List<InventoryCheckResponseEntry> entries)
	{
		this.entries = entries;
	}
}
