package com.wiley.integrations.wpg.transformer;


import java.util.Date;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.wiley.core.payment.request.WileyFollowOnRefundRequest;
import com.wiley.core.payment.strategies.SecurityHashGeneratorStrategy;
import com.wiley.core.payment.strategies.WPGRegionStrategy;
import com.wiley.core.payment.strategies.WPGVendorIdStrategy;
import com.wiley.core.payment.strategies.WileyTransactionIdGeneratorStrategy;
import com.wiley.integrations.wpg.dto.TokenRefundPaymentRequest;


public class FollowOnRefundRequestTransformer
{
	@Value("N")
	private String customerPresent;
	@Value("${payment.wpg.soap.userId}")
	private String userId;
	@Value("SOAP")
	private String method;
	@Value("WPG Refund Transaction")
	private String description;

	@Resource(name = "wpgSoapVendorIdStrategy")
	private WPGVendorIdStrategy vendorIdStrategy;
	@Autowired
	private WPGRegionStrategy regionStrategy;
	@Autowired
	private SecurityHashGeneratorStrategy wpgSoapSecurityHashGeneratorStrategy;
	@Autowired
	private WileyTransactionIdGeneratorStrategy transactionIdGeneratorStrategy;

	public TokenRefundPaymentRequest transform(final WileyFollowOnRefundRequest source)
	{
		final TokenRefundPaymentRequest target = new TokenRefundPaymentRequest();
		final String region = regionStrategy.getRegionByCurrency(source.getCurrency().getCurrencyCode());
		final Integer vendorId = vendorIdStrategy.getVendorId(source.getSiteId());

		target.setVendorId(vendorId);
		target.setTransId(transactionIdGeneratorStrategy.generateTransactionId());
		target.setUserId(getUserId());
		target.setMethod(getMethod());
		target.setCustomerPresent(getCustomerPresent());
		target.setToken(source.getRequestToken());
		target.setDescription(getDescription());
		target.setValue(source.getTotalAmount().toString());
		target.setCurrency(source.getCurrency().getCurrencyCode());
		target.setTimestamp(createTimestamp());
		target.setRegion(region);
		target.setSecurity(calculateSecurity(target, source.getSiteId()));

		return target;
	}

	private String createTimestamp()
	{
		return String.valueOf(new Date().getTime());
	}

	private String calculateSecurity(final TokenRefundPaymentRequest request, final String siteId)
	{
		StringBuilder params = new StringBuilder()
				.append(request.getVendorId())
				.append(request.getTransId())
				.append(request.getUserId())
				.append(request.getMethod())
				.append(request.getCustomerPresent())
				.append(request.getToken())
				.append(request.getDescription())
				.append(request.getValue())
				.append(request.getCurrency())
				.append(request.getTimestamp())
				.append(request.getRegion());

		return wpgSoapSecurityHashGeneratorStrategy.generateSecurityHash(siteId, params.toString());
	}

	public String getCustomerPresent()
	{
		return customerPresent;
	}

	public void setCustomerPresent(final String customerPresent)
	{
		this.customerPresent = customerPresent;
	}

	public String getUserId()
	{
		return userId;
	}

	public void setUserId(final String userId)
	{
		this.userId = userId;
	}

	public String getMethod()
	{
		return method;
	}

	public void setMethod(final String method)
	{
		this.method = method;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(final String description)
	{
		this.description = description;
	}
}
