package com.wiley.integrations.order.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.ws.rs.DefaultValue;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonInclude(JsonInclude.Include.NON_NULL)
public class DiscountValueWsDTO implements Serializable
{
	@Size(max = 255)
	private String code;

	@Min(0)
	@DefaultValue("0")
	private Double value;

	private Boolean absolute = Boolean.FALSE;

	@Pattern(regexp = "^[A-Z]{3}$")
	private String currency;

	@Size(max = 255)
	private String promoTypeGroup;

	@Size(max = 255)
	private String promoInstitution;

	private List<String> couponCodes;
	private Map<String, Object> anyProperties = new HashMap<>();

	/**
	 * Qualifier of the discount value provided by an external system. Can be displayed to the customer,
	 * or used for fulfillment purposes.
	 **/
	@JsonProperty("code")
	public String getCode()
	{
		return code;
	}

	public void setCode(final String code)
	{
		this.code = code;
	}

	/**
	 * Value of the discount (either absolute or relative). The en_US format is used for the value.
	 * minimum: 0.0
	 **/
	@JsonProperty("value")
	public Double getValue()
	{
		return value;
	}

	public void setValue(final Double value)
	{
		this.value = value;
	}

	/**
	 * Defines if the provided discount is absolute or relative.
	 **/
	@JsonProperty("absolute")
	public Boolean getAbsolute()
	{
		return absolute;
	}

	public void setAbsolute(final Boolean absolute)
	{
		this.absolute = absolute;
	}

	@JsonProperty("currency")
	public String getCurrency()
	{
		return currency;
	}

	public void setCurrency(final String currency)
	{
		this.currency = currency;
	}

	@JsonProperty("promoTypeGroup")
	public String getPromoTypeGroup()
	{
		return promoTypeGroup;
	}

	public void setPromoTypeGroup(final String promoTypeGroup)
	{
		this.promoTypeGroup = promoTypeGroup;
	}

	@JsonProperty("promoInstitution")
	public String getPromoInstitution()
	{
		return promoInstitution;
	}

	public void setPromoInstitution(final String promoInstitution)
	{
		this.promoInstitution = promoInstitution;
	}

	@JsonProperty("couponCodes")
	public List<String> getCouponCodes()
	{
		return couponCodes;
	}

	public void setCouponCodes(final List<String> couponCodes)
	{
		this.couponCodes = couponCodes;
	}

	@JsonAnyGetter
	public Map<String, Object> getAnyProperties()
	{
		return anyProperties;
	}

	@JsonAnySetter
	public void setAnyProperties(final String name, final Object value)
	{
		this.anyProperties.put(name, value);
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		DiscountValueWsDTO discountValue = (DiscountValueWsDTO) o;
		return Objects.equals(code, discountValue.code)
				&& Objects.equals(value, discountValue.value)
				&& Objects.equals(absolute, discountValue.absolute)
				&& Objects.equals(currency, discountValue.currency)
				&& Objects.equals(promoTypeGroup, discountValue.promoTypeGroup)
				&& Objects.equals(promoInstitution, discountValue.promoInstitution)
				&& Objects.equals(couponCodes, discountValue.couponCodes)
				&& Objects.equals(anyProperties, discountValue.anyProperties);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(code, value, absolute, currency, promoTypeGroup, promoInstitution, couponCodes, anyProperties);
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("class DiscountValue {\n");

		sb.append("    code: ").append(toIndentedString(code)).append("\n");
		sb.append("    value: ").append(toIndentedString(value)).append("\n");
		sb.append("    absolute: ").append(toIndentedString(absolute)).append("\n");
		sb.append("    currency: ").append(toIndentedString(currency)).append("\n");
		sb.append("    promoTypeGroup: ").append(toIndentedString(promoTypeGroup)).append("\n");
		sb.append("    promoInstitution: ").append(toIndentedString(promoInstitution)).append("\n");
		sb.append("    couponCodes: ").append(toIndentedString(couponCodes)).append("\n");
		sb.append("    anyProperties: ").append(toIndentedString(anyProperties)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(final Object o)
	{
		if (o == null)
		{
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}

