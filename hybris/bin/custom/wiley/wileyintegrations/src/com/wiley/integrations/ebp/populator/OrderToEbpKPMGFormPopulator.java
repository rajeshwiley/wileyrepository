package com.wiley.integrations.ebp.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.annotation.Nonnull;

import org.springframework.util.Assert;

import com.wiley.core.integration.ebp.dto.EbpAddProductsPayload;
import com.wiley.core.model.KpmgEnrollmentModel;
import com.wiley.integrations.ebp.dto.AddProductsArgNode;
import com.wiley.integrations.ebp.dto.KPMGOrderInfo;


public class OrderToEbpKPMGFormPopulator implements Populator<EbpAddProductsPayload, AddProductsArgNode>
{

	@Override
	public void populate(@Nonnull final EbpAddProductsPayload payload, @Nonnull final AddProductsArgNode addProductsArgNode)
			throws ConversionException
	{
		Assert.notNull(payload);
		Assert.notNull(addProductsArgNode);
		final OrderModel order = payload.getOrder();
		Assert.notNull(order);

		final KpmgEnrollmentModel kpmgForm = order.getKpmgEnrollment();
		if (kpmgForm != null)
		{
			final KPMGOrderInfo kpmgOrderInfo = new KPMGOrderInfo();

			kpmgOrderInfo.setStudentRegistrationID(kpmgForm.getRegistrationId());
			kpmgOrderInfo.setKpmgEmployeeID(kpmgForm.getEmployeeId());
			kpmgOrderInfo.setFunction(kpmgForm.getFunction().getCode());
			kpmgOrderInfo.setOffice(kpmgForm.getOffice().getName());
			kpmgOrderInfo.setFirstName(kpmgForm.getFirstName());
			kpmgOrderInfo.setLastName(kpmgForm.getLastName());
			kpmgOrderInfo.setEmailAddress(kpmgForm.getEmail());

			addProductsArgNode.setKpmgOrderInfo(kpmgOrderInfo);
		}
	}
}
