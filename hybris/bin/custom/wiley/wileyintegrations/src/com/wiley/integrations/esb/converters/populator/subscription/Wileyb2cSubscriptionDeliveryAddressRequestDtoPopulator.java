package com.wiley.integrations.esb.converters.populator.subscription;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.annotation.Nonnull;

import org.springframework.util.Assert;

import com.wiley.integrations.esb.subscription.dto.Wileyb2cSubscriptionDeliveryAddressRequestDto;


public class Wileyb2cSubscriptionDeliveryAddressRequestDtoPopulator
		implements Populator<AddressModel, Wileyb2cSubscriptionDeliveryAddressRequestDto>
{
	@Override
	public void populate(@Nonnull final AddressModel deliveryAddress,
			@Nonnull final Wileyb2cSubscriptionDeliveryAddressRequestDto requestDto) throws ConversionException
	{
		Assert.notNull(deliveryAddress);
		Assert.notNull(requestDto);

		if (deliveryAddress.getCountry() != null)
		{
			requestDto.setCountry(deliveryAddress.getCountry().getIsocode());
		}

		if (deliveryAddress.getRegion() != null)
		{
			requestDto.setState(deliveryAddress.getRegion().getIsocodeShort());
		}

		requestDto.setPostcode(deliveryAddress.getPostalcode());
		requestDto.setCity(deliveryAddress.getTown());
		requestDto.setLine1(deliveryAddress.getLine1());
		requestDto.setLine2(deliveryAddress.getLine2());
		requestDto.setFirstName(deliveryAddress.getFirstname());
		requestDto.setLastName(deliveryAddress.getLastname());
		requestDto.setPhoneNumber(deliveryAddress.getPhone1());
	}
}
