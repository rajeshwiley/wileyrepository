/**
 *
 */
package com.wiley.integrations.cdm.dto;

import com.fasterxml.jackson.annotation.JsonInclude;



@JsonInclude(JsonInclude.Include.NON_NULL)
public class CDMOrganizationDTO {

  private String organizationName;

  private String rawOrganizationName;

  /**
   * @return the organizationName
   */
  public String getOrganizationName() {
    return organizationName;
  }

  /**
   * @param organizationName
   *          the organizationName to set
   */
  public void setOrganizationName(final String organizationName) {
    this.organizationName = organizationName;
  }

  /**
   * @return the rawOrganizationName
   */
  public String getRawOrganizationName() {
    return rawOrganizationName;
  }

  /**
   * @param rawOrganizationName
   *          the rawOrganizationName to set
   */
  public void setRawOrganizationName(final String rawOrganizationName) {
    this.rawOrganizationName = rawOrganizationName;
  }


  @Override
  public String toString() {
    return "Organization [organizationName=" + organizationName + ", rawOrganizationName="
        + rawOrganizationName + "]";
  }



}
