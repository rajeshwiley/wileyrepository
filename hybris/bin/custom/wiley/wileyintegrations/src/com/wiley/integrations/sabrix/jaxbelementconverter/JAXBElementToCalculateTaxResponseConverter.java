package com.wiley.integrations.sabrix.jaxbelementconverter;


import javax.xml.bind.JAXBElement;

import org.springframework.core.convert.converter.Converter;

import com.wiley.integrations.sabrix.dto.TaxCalculationResponseEBMType;


public class JAXBElementToCalculateTaxResponseConverter
		implements Converter<JAXBElement<TaxCalculationResponseEBMType>, TaxCalculationResponseEBMType>
{
	@Override
	public TaxCalculationResponseEBMType convert(final JAXBElement<TaxCalculationResponseEBMType> source)
	{
		return source.getValue();
	}
}
