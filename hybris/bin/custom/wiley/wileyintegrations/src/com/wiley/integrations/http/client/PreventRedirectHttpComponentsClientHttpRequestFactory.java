package com.wiley.integrations.http.client;

import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;


/**
 * Extends {@link HttpComponentsClientHttpRequestFactory} for preventing redirect
 *
 * @author Dzmitryi_Halahayeu
 */
public class PreventRedirectHttpComponentsClientHttpRequestFactory extends HttpComponentsClientHttpRequestFactory
{
	@Override
	protected void postProcessHttpRequest(final HttpUriRequest request)
	{
		HttpParams httpParams = new BasicHttpParams();
		httpParams.setParameter(ClientPNames.HANDLE_REDIRECTS, false);
		request.setParams(httpParams);
		super.postProcessHttpRequest(request);
	}
}
