package com.wiley.integrations.users.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import com.wiley.integrations.users.data.WileyUserAuthenticateRequest;

/**
 * Created by Mikhail_Asadchy on 14.06.2016.
 */
public class WileyAuthenticationPopulator implements Populator<String, WileyUserAuthenticateRequest> {
    @Override
    public void populate(final String password, final WileyUserAuthenticateRequest wileyUserAuthenticateRequest)
            throws ConversionException {
        wileyUserAuthenticateRequest.getUserCredentials().setPassword(password);
    }
}
