package com.wiley.integrations.users.transformer;

import javax.annotation.Nonnull;

import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.util.Assert;

import com.wiley.core.integration.users.gateway.WileyUsersGateway;
import com.wiley.integrations.users.data.PasswordUpdateRequest;


/**
 * Created by Sergiy_Mishkovets on 7/13/2016.
 */
public class WileyUserPasswordUpdateRequestTransformer
{
	public PasswordUpdateRequest transform(@Header(WileyUsersGateway.HEADER_OLD_PASSWORD) final String oldPassword,
			@Nonnull @Payload final String newPassword)
	{
		Assert.notNull(newPassword);
		Assert.notNull(oldPassword);
		final PasswordUpdateRequest passwordUpdateRequest = new PasswordUpdateRequest();
		passwordUpdateRequest.setNewPassword(newPassword);
		passwordUpdateRequest.setOldPassword(oldPassword);
		return passwordUpdateRequest;
	}
}
