package com.wiley.integrations.esb.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonInclude(JsonInclude.Include.NON_NULL)
public class RegistrationCodeValidationResponseDto
{
	@JsonProperty("code")
	private String code;
	@JsonProperty("isbn")
	private String isbn;
	@JsonProperty("isBurned")
	private boolean isBurned;
	@JsonProperty("message")
	private String message;

	public boolean isBurned()
	{
		return isBurned;
	}

	public void setIsBurned(final boolean isBurned)
	{
		this.isBurned = isBurned;
	}

	public String getCode()
	{
		return code;
	}

	public void setCode(final String code)
	{
		this.code = code;
	}

	public String getIsbn()
	{
		return isbn;
	}

	public void setIsbn(final String isbn)
	{
		this.isbn = isbn;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage(final String message)
	{
		this.message = message;
	}


}
