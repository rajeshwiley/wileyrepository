package com.wiley.integrations.failedordersexport.integration;

import com.wiley.core.model.WileyOrderProcessModel;
import com.wiley.integrations.failedordersexport.providers.WileyFailedOrderProcessesProvisionDao;
import de.hybris.platform.store.BaseStoreModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Header;

import java.time.LocalDateTime;
import java.util.Collection;

public class WileyFailedOrderProcessesProvisionStrategyImpl implements WileyFailedOrderProcessesProvisionStrategy {

    private final WileyFailedOrderProcessesProvisionDao wileyFailedOrderProcessesProvisionDao;

    @Autowired
    public WileyFailedOrderProcessesProvisionStrategyImpl(
            final WileyFailedOrderProcessesProvisionDao wileyFailedOrderProcessesProvisionDao) {
        this.wileyFailedOrderProcessesProvisionDao = wileyFailedOrderProcessesProvisionDao;
    }

    @Override
    public Collection<WileyOrderProcessModel> getFailedOrders(
            final BaseStoreModel baseStore, @Header(value = "dateFrom", required = false) final LocalDateTime from,
            @Header(value = "dateTo", required = false) final LocalDateTime to) {
        return wileyFailedOrderProcessesProvisionDao.findFailedOrderProcesses(baseStore, from, to);
    }


}
