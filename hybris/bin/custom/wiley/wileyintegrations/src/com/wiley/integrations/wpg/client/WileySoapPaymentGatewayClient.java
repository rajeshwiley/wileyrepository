package com.wiley.integrations.wpg.client;

import java.util.Map;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.xml.internal.ws.client.BindingProviderProperties;
import com.wiley.integrations.wpg.dto.TokenRefundPaymentRequest;
import com.wiley.integrations.wpg.dto.TokenRefundPaymentResponse;
import com.wileypay.wpg_v1.WileyPaymentGatewayV1;
import com.wileypay.wpg_v1.WpgPort;


public class WileySoapPaymentGatewayClient
{
	private static final Logger LOG = LoggerFactory.getLogger(WileySoapPaymentGatewayClient.class);

	private String endpointUrl;
	private int requestTimeout;
	private int connectTimeout;

	public TokenRefundPaymentResponse tokenRefund(final TokenRefundPaymentRequest request)
	{
		WpgPort port = getSOAPPort();
		// request INOUT holders
		Holder<Integer> vendorID = new Holder<>(request.getVendorId());
		Holder<String> transID = new Holder<>(request.getTransId());
		Holder<String> timestamp = new Holder<>(request.getTimestamp());
		Holder<String> value = new Holder<>(request.getValue());

		//response holders
		Holder<Integer> returnCode = new Holder<>();
		Holder<String> returnMessage = new Holder<>();
		Holder<String> merchantResponse = new Holder<>();
		Holder<String> merchantID = new Holder<>();
		Holder<String> authCode = new Holder<>();
		Holder<String> acquirerID = new Holder<>();
		Holder<String> acquirerName = new Holder<>();
		Holder<String> bankID = new Holder<>();
		Holder<String> bankName = new Holder<>();
		Holder<String> maskedCardNumber = new Holder<>();
		Holder<String> cardExpiry = new Holder<>();
		Holder<String> operation = new Holder<>();


		LOG.debug("Invoking Token Refund operation on WPG SOAP Service. Request='{}'",
				ToStringBuilder.reflectionToString(request, ToStringStyle.MULTI_LINE_STYLE));

		port.tokenRefund(
				//request params
				vendorID,
				transID,
				request.getUserId(),
				request.getMethod(),
				request.getCustomerPresent(),
				request.getToken(),
				request.getDescription(),
				value,
				request.getCurrency(),
				timestamp,
				request.getRegion(),
				request.getSecurity(),
				//reponse params
				returnCode,
				returnMessage,
				merchantResponse,
				merchantID,
				authCode,
				acquirerID,
				acquirerName,
				bankID,
				bankName,
				maskedCardNumber,
				cardExpiry,
				operation
		);

		TokenRefundPaymentResponse response = new TokenRefundPaymentResponse();
		response.setReturnCode(returnCode.value);
		response.setReturnMessage(returnMessage.value);
		response.setTransId(transID.value);
		response.setMerchantResponse(merchantResponse.value);
		response.setMerchantId(merchantID.value);
		response.setValue(value.value);
		response.setAuthCode(authCode.value);
		response.setAcquirerId(acquirerID.value);
		response.setAcquirerName(acquirerName.value);
		response.setBankId(bankID.value);
		response.setBankName(bankName.value);
		response.setMaskedCardNumber(maskedCardNumber.value);
		response.setCardExpiry(cardExpiry.value);
		response.setOperation(operation.value);
		response.setVendorId(vendorID.value);
		response.setTimestamp(timestamp.value);

		LOG.debug("Invoked Token Refund operation on WPG SOAP Service. Response='{}'",
				ToStringBuilder.reflectionToString(response, ToStringStyle.MULTI_LINE_STYLE));

		return response;
	}

	protected WpgPort getSOAPPort()
	{
		WileyPaymentGatewayV1 paymentGateway = new WileyPaymentGatewayV1();
		WpgPort wpgSOAP = paymentGateway.getWpgSOAP();
		BindingProvider bindingProvider = (BindingProvider) wpgSOAP;
		Map<String, Object> requestContext = bindingProvider.getRequestContext();
		requestContext.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, getEndPointURL());
		requestContext.put(BindingProviderProperties.CONNECT_TIMEOUT, getConnectTimeout());
		requestContext.put(BindingProviderProperties.REQUEST_TIMEOUT, getRequestTimeout());

		return wpgSOAP;
	}

	public String getEndPointURL()
	{
		return endpointUrl;
	}

	public void setEndpointUrl(final String endpointUrl)
	{
		this.endpointUrl = endpointUrl;
	}

	public int getRequestTimeout()
	{
		return requestTimeout;
	}

	public void setRequestTimeout(final int requestTimeout)
	{
		this.requestTimeout = requestTimeout;
	}

	public int getConnectTimeout()
	{
		return connectTimeout;
	}

	public void setConnectTimeout(final int connectTimeout)
	{
		this.connectTimeout = connectTimeout;
	}
}

