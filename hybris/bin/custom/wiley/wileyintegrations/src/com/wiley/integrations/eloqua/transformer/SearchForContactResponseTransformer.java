package com.wiley.integrations.eloqua.transformer;

import java.util.List;
import java.util.Optional;

import javax.annotation.Nonnull;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.util.Assert;

import com.wiley.core.integration.eloqua.dto.ContactDto;
import com.wiley.integrations.eloqua.dto.SearchContactQueryResult;


/**
 * Used to transform response to one ContactDto.
 *
 * @author Aliaksei_Zlobich
 */
public class SearchForContactResponseTransformer
{

	/**
	 * Transforms SearchContactQueryResult to Optional&lt;ContactDto&gt;.
	 *
	 * @param queryResult
	 * 		the query result
	 * @return the object optional which contains ContactDto if Contact was found.
	 */
	@Nonnull
	public Optional<ContactDto> transform(@Nonnull final SearchContactQueryResult queryResult)
	{
		Assert.notNull(queryResult);

		final List<ContactDto> elements = queryResult.getElements();

		if (CollectionUtils.isNotEmpty(elements))
		{
			return Optional.of(elements.get(0));
		}
		else
		{
			return Optional.empty();
		}
	}

}
