package com.wiley.integrations.esb.dto.delivery;

import java.util.Objects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Created by Uladzimir_Barouski on 6/16/2016.
 */
@XmlRootElement(name = "shippingAddress")
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DeliveryAddressDto
{
	@JsonProperty("id")
	private String id;
	@JsonProperty("postcode")
	private String postcode;
	@JsonProperty("country")
	private String country;
	@JsonProperty("state")
	private String state;
	@JsonProperty("city")
	private String city;
	@JsonProperty("line1")
	private String line1;
	@JsonProperty("line2")
	private String line2;

	public String getState()
	{
		return state;
	}

	public void setState(final String state)
	{
		this.state = state;
	}

	public String getLine1()
	{
		return line1;
	}

	public void setLine1(final String line1)
	{
		this.line1 = line1;
	}

	public String getPostcode()
	{
		return postcode;
	}

	public void setPostcode(final String postcode)
	{
		this.postcode = postcode;
	}

	public String getLine2()
	{
		return line2;
	}

	public void setLine2(final String line2)
	{
		this.line2 = line2;
	}

	public String getCity()
	{
		return city;
	}

	public void setCity(final String city)
	{
		this.city = city;
	}

	public String getCountry()
	{
		return country;
	}

	public void setCountry(final String country)
	{
		this.country = country;
	}

	public String getId()
	{
		return id;
	}

	public void setId(final String id)
	{
		this.id = id;
	}

	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this);
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		final DeliveryAddressDto that = (DeliveryAddressDto) o;
		return Objects.equals(id, that.id)
				&& Objects.equals(postcode, that.postcode)
				&& Objects.equals(country, that.country)
				&& Objects.equals(state, that.state)
				&& Objects.equals(city, that.city)
				&& Objects.equals(line1, that.line1)
				&& Objects.equals(line2, that.line2);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(id, postcode, country, state, city, line1, line2);
	}
}
