package com.wiley.integrations.media;

import java.net.URI;

import javax.annotation.Nonnull;

import org.springframework.messaging.Message;

import reactor.util.Assert;


/**
 * @author Dzmitryi_Halahayeu
 */
public class WileyMediaResponseMessageTransformer
{
	private static final String LOCATION_HEADER = "Location";

	@Nonnull
	public String transform(@Nonnull final Message message)
	{
		URI location = (URI) message.getHeaders().get(LOCATION_HEADER);
		Assert.notNull(location, "Could not resolve location header");
		return location.toString();
	}
}
