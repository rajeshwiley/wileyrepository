/**
 *
 */
package com.wiley.integrations.cdm.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.List;

import com.wiley.fulfilmentprocess.model.CDMCustomerExportProcessModel;
import com.wiley.integrations.cdm.dao.CDMSubprocessExportDao;


public class CDMSubprocessExportDaoImpl implements CDMSubprocessExportDao
{

  private FlexibleSearchService flexibleSearchService;

  public static final String CUSTOMER_UID = "customerUid";

  @Override
  public List<CDMCustomerExportProcessModel> getCDMExportProcesses(final CustomerModel customer) {
    validateParameterNotNull(customer, "Customer must not be null!");

    StringBuilder queryString = new StringBuilder();

    queryString = queryString.append("SELECT {ep.").append(CDMCustomerExportProcessModel.PK)
        .append("} ").append("FROM {").append(CustomerModel._TYPECODE).append(" AS c ")
        .append("JOIN ").append(OrderModel._TYPECODE).append(" AS o ON {o.").append(OrderModel.USER)
        .append("}={c.").append(CustomerModel.PK).append("} JOIN ")
        .append(CDMCustomerExportProcessModel._TYPECODE).append(" AS ep ON {ep.")
        .append(CDMCustomerExportProcessModel.ORDER).append("}={o.").append(OrderModel.PK)
        .append("}} WHERE {c.").append(CustomerModel.UID).append("}=?customerUid");

    final FlexibleSearchQuery query = new FlexibleSearchQuery(queryString);
    query.addQueryParameter(CUSTOMER_UID, customer.getUid());
    final SearchResult<CDMCustomerExportProcessModel> result = flexibleSearchService.search(query);

    return result.getResult();
  }

  /**
   * @return the flexibleSearchService
   */
  public FlexibleSearchService getFlexibleSearchService() {
    return flexibleSearchService;
  }

  /**
   * @param flexibleSearchService
   *          the flexibleSearchService to set
   */
  public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService) {
    this.flexibleSearchService = flexibleSearchService;
  }
}
