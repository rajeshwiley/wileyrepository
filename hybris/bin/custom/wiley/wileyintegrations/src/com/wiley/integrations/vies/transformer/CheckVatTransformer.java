package com.wiley.integrations.vies.transformer;

import javax.annotation.Nonnull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import com.wiley.integrations.vies.dto.CheckVat;
import com.wiley.integrations.vies.marshaller.ViesMarshaller;
import org.springframework.xml.transform.StringResult;

public class CheckVatTransformer
{
	@Autowired
	private ViesMarshaller viesMarshaller;

	private static final Logger LOG = LoggerFactory.getLogger(CheckVatTransformer.class);

	@Nonnull
	public CheckVat transform(@Nonnull final Message message)
	{
		CheckVat checkVat = new CheckVat();
		checkVat.setCountryCode(message.getPayload().toString());
		checkVat.setVatNumber(message.getHeaders().get("vatNumber", String.class));

		StringResult result = new StringResult();
		viesMarshaller.marshal(checkVat, result);
		LOG.debug("Request VIES XML message: /n{} ", result);

		return checkVat;
	}
}

