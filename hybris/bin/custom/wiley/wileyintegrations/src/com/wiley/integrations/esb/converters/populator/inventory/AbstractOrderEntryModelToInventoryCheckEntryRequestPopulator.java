package com.wiley.integrations.esb.converters.populator.inventory;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.annotation.Nonnull;

import com.wiley.integrations.inventory.status.InventoryCheckEntryRequest;

import org.springframework.util.Assert;


/**
 * Created by Mikhail_Asadchy on 7/27/2016.
 */
public class AbstractOrderEntryModelToInventoryCheckEntryRequestPopulator
		implements Populator<AbstractOrderEntryModel, InventoryCheckEntryRequest>
{
	@Override
	public void populate(@Nonnull final AbstractOrderEntryModel source,
			@Nonnull final InventoryCheckEntryRequest target)
			throws ConversionException
	{
		Assert.notNull(source);
		Assert.notNull(target);
		Assert.notNull(source.getProduct());
		Assert.notNull(source.getQuantity());

		target.setSapProductCode(source.getProduct().getSapProductCode());
		target.setIsbn(source.getProduct().getIsbn());
		target.setQuantity(source.getQuantity().intValue());
	}
}
