package com.wiley.integrations.http.client;

import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.annotation.Resource;
import javax.net.ssl.SSLContext;

import org.apache.commons.lang.StringUtils;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.ResourceUtils;


public class WileySSLHttpClientFactory
{

	@Resource
	private ConfigurationService configurationService;

	private String keyStoreProperty;
	private String keyStorePasswordProperty;
	private String trustStoreProperty;
	private String trustStorePasswordProperty;

	public HttpClient createHttpClient()
			throws IOException, CertificateException, NoSuchAlgorithmException, KeyStoreException, UnrecoverableKeyException,
			KeyManagementException
	{
		String keyStorePath = getConfigurationService().getConfiguration().getString(keyStoreProperty);
		String keyStorePassword = getConfigurationService().getConfiguration().getString(keyStorePasswordProperty);
		String trustStorePath = getConfigurationService().getConfiguration().getString(trustStoreProperty);
		String trustStorePassword = getConfigurationService().getConfiguration().getString(trustStorePasswordProperty);


		SSLContextBuilder sslContextBuilder = SSLContextBuilder.create();
		if (StringUtils.isNotEmpty(keyStorePath))
		{
			sslContextBuilder.loadKeyMaterial(keyStore(keyStorePath, keyStorePassword.toCharArray()),
					keyStorePassword.toCharArray());
		}
		if (StringUtils.isNotEmpty(trustStorePath))
		{
			File trustStoreFile = ResourceUtils.getFile(trustStorePath);
			sslContextBuilder.loadTrustMaterial(trustStoreFile, trustStorePassword.toCharArray());
		}

		SSLContext sslContext = sslContextBuilder.build();
		return HttpClients.custom()
				.setSSLContext(sslContext)
				.build();
	}

	private KeyStore keyStore(final String file, final char[] password)
			throws KeyStoreException, IOException, CertificateException, NoSuchAlgorithmException
	{
		KeyStore keyStore = KeyStore.getInstance("JKS");
		File key = ResourceUtils.getFile(file);
		try (InputStream in = new FileInputStream(key))
		{
			keyStore.load(in, password);
		}
		return keyStore;
	}

	public ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setKeyStoreProperty(final String keyStoreProperty)
	{
		this.keyStoreProperty = keyStoreProperty;
	}

	@Required
	public void setKeyStorePasswordProperty(final String keyStorePasswordProperty)
	{
		this.keyStorePasswordProperty = keyStorePasswordProperty;
	}

	@Required
	public void setTrustStoreProperty(final String trustStoreProperty)
	{
		this.trustStoreProperty = trustStoreProperty;
	}

	@Required
	public void setTrustStorePasswordProperty(final String trustStorePasswordProperty)
	{
		this.trustStorePasswordProperty = trustStorePasswordProperty;
	}

	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}
}
