package com.wiley.integrations.inventory;

import de.hybris.platform.acceleratorservices.dataimport.batch.BatchHeader;


/**
 * Extend Hybris OOTB BatchHeader by warehouseCode field
 */
public class InventoryBatchHeader extends BatchHeader
{
	private String warehouseCode;

	private Long warehouseSequenceId;

	public String getWarehouseCode()
	{
		return warehouseCode;
	}

	public void setWarehouseCode(final String warehouseCode)
	{
		this.warehouseCode = warehouseCode;
	}

	public Long getWarehouseSequenceId()
	{
		return warehouseSequenceId;
	}

	public void setWarehouseSequenceId(final Long warehouseSequenceId)
	{
		this.warehouseSequenceId = warehouseSequenceId;
	}
}
