package com.wiley.integrations.b2b.unit.transformer;


import java.util.ArrayList;
import java.util.Collection;

import com.wiley.integrations.b2b.unit.dto.PoNumberValidationRequestSwgDTO;

import org.springframework.messaging.handler.annotation.Payload;


public class PoNumberValidationRequestTransformer
{
    public PoNumberValidationRequestSwgDTO transform(@Payload final Collection<String> poNumbers)
    {
        PoNumberValidationRequestSwgDTO poNumberValidationRequest = new PoNumberValidationRequestSwgDTO();
        poNumberValidationRequest.setPoNumbers(new ArrayList<>(poNumbers));
        return poNumberValidationRequest;
    }
}
