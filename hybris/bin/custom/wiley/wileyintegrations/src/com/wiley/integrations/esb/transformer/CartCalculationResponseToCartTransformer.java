package com.wiley.integrations.esb.transformer;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Collections;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import com.wiley.core.integration.ExternalCartModification;
import com.wiley.integrations.constants.WileyintegrationsConstants;
import com.wiley.integrations.esb.dto.verifyandcalculatecart.CartCalculationEntryResponse;
import com.wiley.integrations.esb.dto.verifyandcalculatecart.CartCalculationResponse;
import com.wiley.integrations.esb.service.CartModificationHandler;

import org.springframework.messaging.Message;
import org.springframework.util.Assert;


/**
 * Transforms OrderDto to Order. This transformer is used in ERP gateway for cart calculation and verification.<br/>
 * Transformer doesn't save changes to models, just populate values.
 */
public class CartCalculationResponseToCartTransformer
{

	@Resource
	private Populator<CartCalculationResponse, CartModel> cartCalculationResponseToCartSimplePopulator;

	@Resource
	private CartModificationHandler cartModificationHandler;

	@Resource
	private ModelService modelService;

	@Nonnull
	public List<ExternalCartModification> transform(@Nonnull final Message<CartCalculationResponse> message)
	{
		Assert.notNull(message);

		final CartModel cartModel = (CartModel) message.getHeaders().get(WileyintegrationsConstants.Esb.CART);
		final CartCalculationResponse orderDto = message.getPayload();

		Assert.notNull(cartModel);
		Assert.notNull(orderDto);

		// populate simple fields
		cartCalculationResponseToCartSimplePopulator.populate(orderDto, cartModel);

		List<ExternalCartModification> externalCartModifications = Collections.emptyList();

		final List<CartCalculationEntryResponse> entries = orderDto.getEntries();
		if (entries != null)
		{
			externalCartModifications = cartModificationHandler.applyOrderModificationToCart(cartModel, entries,
					orderDto.getMessages());
		}

		return externalCartModifications;
	}

}
