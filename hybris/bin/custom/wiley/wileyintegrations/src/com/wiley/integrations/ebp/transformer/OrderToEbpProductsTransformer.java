package com.wiley.integrations.ebp.transformer;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;

import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import com.wiley.core.integration.ebp.dto.EbpAddProductsPayload;
import com.wiley.integrations.ebp.dto.AddProductsArgNode;
import com.wiley.integrations.ebp.dto.EbpAddProductsRootNode;
import com.wiley.integrations.ebp.dto.OrderNode;
import com.wiley.integrations.ebp.dto.ProductNode;


/**
 * Created by Uladzimir_Barouski on 2/16/2016.
 */
public class OrderToEbpProductsTransformer
{
	private static final Logger LOG = LoggerFactory.getLogger(OrderToEbpProductsTransformer.class);

	@Resource
	private Populator<EbpAddProductsPayload, List<ProductNode>> orderToEbpProductListPopulator;
	@Resource
	private Populator<EbpAddProductsPayload, AddProductsArgNode> orderToEbpKPMGFormPopulator;
	@Resource
	private Populator<EbpAddProductsPayload, OrderNode> orderToEbpOrderInfoPopulator;

	@Nonnull
	public EbpAddProductsRootNode transform(@Nonnull final EbpAddProductsPayload payload)
	{
		Assert.notNull(payload);
		final OrderModel order = payload.getOrder();

		LOG.debug("Method transform order to EBP Order params: Order [{}]", order.getCode());

		AddProductsArgNode ebpAddProducts = new AddProductsArgNode();
		OrderNode orderInfo = new OrderNode();

		CustomerModel customer = (CustomerModel) order.getUser();
		ebpAddProducts.setUserName(customer.getUid());
		ebpAddProducts.setClientID(customer.getCustomerID());
		ebpAddProducts.setOrderInfo(orderInfo);
		orderToEbpProductListPopulator.populate(payload, ebpAddProducts.getProducts());
		orderToEbpOrderInfoPopulator.populate(payload, orderInfo);
		EbpAddProductsRootNode addProductsRootNode = new EbpAddProductsRootNode();
		addProductsRootNode.setArgNode(ebpAddProducts);
		orderToEbpKPMGFormPopulator.populate(payload, ebpAddProducts);

		return addProductsRootNode;
	}
}
