package com.wiley.integrations.esb.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.common.base.MoreObjects;


@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EsbAddressCreateRequestDto
{

	private String addressId;

	private String individualEcidNumber;

	@XmlElement(required = true)
	private String postcode;

	@XmlElement(required = true)
	private String country;

	private String state;

	@XmlElement(required = true)
	private String city;

	@XmlElement(required = true)
	private String line1;

	private String line2;

	private String firstName;

	private String lastName;

	private String phoneNumber;

	public String getIndividualEcidNumber()
	{
		return individualEcidNumber;
	}

	public void setIndividualEcidNumber(final String individualEcidNumber)
	{
		this.individualEcidNumber = individualEcidNumber;
	}

	public String getAddressId()
	{
		return addressId;
	}

	public void setAddressId(final String addressId)
	{
		this.addressId = addressId;
	}

	public String getPostcode()
	{
		return postcode;
	}

	public void setPostcode(final String postcode)
	{
		this.postcode = postcode;
	}

	public String getCountry()
	{
		return country;
	}

	public void setCountry(final String country)
	{
		this.country = country;
	}

	public String getState()
	{
		return state;
	}

	public void setState(final String state)
	{
		this.state = state;
	}

	public String getCity()
	{
		return city;
	}

	public void setCity(final String city)
	{
		this.city = city;
	}

	public String getLine1()
	{
		return line1;
	}

	public void setLine1(final String line1)
	{
		this.line1 = line1;
	}

	public String getLine2()
	{
		return line2;
	}

	public void setLine2(final String line2)
	{
		this.line2 = line2;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(final String firstName)
	{
		this.firstName = firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(final String lastName)
	{
		this.lastName = lastName;
	}

	public String getPhoneNumber()
	{
		return phoneNumber;
	}

	public void setPhoneNumber(final String phoneNumber)
	{
		this.phoneNumber = phoneNumber;
	}

	@Override
	public String toString()
	{
		return MoreObjects.toStringHelper(this)
				.add("individualEcidNumber", individualEcidNumber)
				.add("addressId", addressId)
				.add("postcode", postcode)
				.add("country", country)
				.add("state", state)
				.add("city", city)
				.add("line1", line1)
				.add("line2", line2)
				.add("firstName", firstName)
				.add("lastName", lastName)
				.add("phoneNumber", phoneNumber)
				.toString();
	}
}
