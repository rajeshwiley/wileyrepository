package com.wiley.integrations.order.service.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wiley.core.order.service.WileyOrderStateSerializationService;
import com.wiley.integrations.order.dto.OrderWsDTO;


public class WileyOrderWsDTOStateSerializationServiceImpl implements WileyOrderStateSerializationService
{
	@Resource
	private Converter<OrderModel, OrderWsDTO> wileyOrderWsConverter;

	@Resource
	private ObjectMapper jacksonObjectMapper;

	@Override
	public String serializeToOrderState(@Nonnull final OrderModel order)
	{
		OrderWsDTO orderWsDTO = wileyOrderWsConverter.convert(order);
		try
		{
			return jacksonObjectMapper.writeValueAsString(orderWsDTO);
		}
		catch (JsonProcessingException e)
		{
			throw new IllegalStateException("OrderWsDTO generation failed", e);
		}
	}

}