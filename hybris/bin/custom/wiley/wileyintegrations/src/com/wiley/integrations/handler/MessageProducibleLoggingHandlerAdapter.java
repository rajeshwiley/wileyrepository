package com.wiley.integrations.handler;

import org.springframework.integration.handler.LoggingHandler;
import org.springframework.messaging.Message;


/**
 * This adapter wraps {@link LoggingHandler} and adds availability to return incoming message to output chain.
 * Allows to use {@link LoggingHandler} within chain.
 */
public class MessageProducibleLoggingHandlerAdapter
{
	private LoggingHandler loggingHandler;

	public MessageProducibleLoggingHandlerAdapter(final LoggingHandler loggingHandler)
	{
		this.loggingHandler = loggingHandler;
	}

	public Message<?> handleMessage(final Message<?> message)
	{
		loggingHandler.handleMessage(message);
		return message;
	}

}
