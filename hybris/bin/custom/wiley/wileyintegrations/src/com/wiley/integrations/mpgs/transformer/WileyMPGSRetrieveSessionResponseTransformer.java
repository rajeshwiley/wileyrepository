package com.wiley.integrations.mpgs.transformer;


import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;

import com.wiley.core.mpgs.dto.retrievesession.MPGSRetrieveSessionResponseDTO;
import com.wiley.core.mpgs.response.WileyRetrieveSessionResponse;
import com.wiley.core.mpgs.services.WileyTransformationService;


public class WileyMPGSRetrieveSessionResponseTransformer
{
	@Autowired
	private WileyTransformationService wileyTransformationService;

	public WileyRetrieveSessionResponse transform(@Nonnull final Message<MPGSRetrieveSessionResponseDTO> message)
	{
		WileyRetrieveSessionResponse response = new WileyRetrieveSessionResponse();
		MPGSRetrieveSessionResponseDTO responseDTO = message.getPayload();

		response.setSessionId(responseDTO.getSession().getId());
		response.setStatus(wileyTransformationService.transformStatusIfSuccessful(responseDTO.getSession().getUpdateStatus()));
		return response;
	}
}