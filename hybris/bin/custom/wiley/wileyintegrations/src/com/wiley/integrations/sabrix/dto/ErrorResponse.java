package com.wiley.integrations.sabrix.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ErrorResponse complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="ErrorResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ErrorCode" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="ErrorDescription" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="ErrorData" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/&gt;
 *         &lt;element name="ErrorStack" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ErrorResponse", namespace = "http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/Meta/V1", propOrder = {
		"errorCode",
		"errorDescription",
		"errorData",
		"errorStack"
})
public class ErrorResponse
{

	@XmlElement(name = "ErrorCode")
	protected String errorCode;
	@XmlElement(name = "ErrorDescription")
	protected String errorDescription;
	@XmlElement(name = "ErrorData")
	protected Object errorData;
	@XmlElement(name = "ErrorStack")
	protected Object errorStack;

	/**
	 * Gets the value of the errorCode property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getErrorCode()
	{
		return errorCode;
	}

	/**
	 * Sets the value of the errorCode property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setErrorCode(String value)
	{
		this.errorCode = value;
	}

	/**
	 * Gets the value of the errorDescription property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getErrorDescription()
	{
		return errorDescription;
	}

	/**
	 * Sets the value of the errorDescription property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setErrorDescription(String value)
	{
		this.errorDescription = value;
	}

	/**
	 * Gets the value of the errorData property.
	 *
	 * @return possible object is
	 * {@link Object }
	 */
	public Object getErrorData()
	{
		return errorData;
	}

	/**
	 * Sets the value of the errorData property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link Object }
	 */
	public void setErrorData(Object value)
	{
		this.errorData = value;
	}

	/**
	 * Gets the value of the errorStack property.
	 *
	 * @return possible object is
	 * {@link Object }
	 */
	public Object getErrorStack()
	{
		return errorStack;
	}

	/**
	 * Sets the value of the errorStack property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link Object }
	 */
	public void setErrorStack(Object value)
	{
		this.errorStack = value;
	}

}
