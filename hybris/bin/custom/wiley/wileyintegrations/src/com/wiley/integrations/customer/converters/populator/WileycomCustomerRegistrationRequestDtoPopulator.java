package com.wiley.integrations.customer.converters.populator;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BUnitService;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.user.UserService;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import com.wiley.integrations.customer.dto.WileycomCustomerRegistrationRequestDto;


public class WileycomCustomerRegistrationRequestDtoPopulator
		implements Populator<CustomerModel, WileycomCustomerRegistrationRequestDto>
{
	private static final Logger LOGGER = LoggerFactory.getLogger(WileycomCustomerRegistrationRequestDtoPopulator.class);

	@Resource
	private UserService userService;

	@Resource
	private B2BUnitService<B2BUnitModel, B2BCustomerModel> b2bUnitService;

	@Override
	public void populate(@Nonnull final CustomerModel customerModel,
			@Nonnull final WileycomCustomerRegistrationRequestDto requestDto)
			throws ConversionException
	{
		Assert.notNull(customerModel);
		Assert.notNull(requestDto);

		populatePrincipalAttributes(customerModel, requestDto);
		populateNameAttributes(customerModel, requestDto);
		populateEducationAttributes(customerModel, requestDto);
		populateCustomerRole(customerModel, requestDto);
		populateBusinessAttributes(customerModel, requestDto);
		populateMiscellaneousAttributes(customerModel, requestDto);
	}

	private void populatePrincipalAttributes(final CustomerModel customerModel,
			final WileycomCustomerRegistrationRequestDto requestDto)
	{
		requestDto.setImmutableId(customerModel.getCustomerID());
		requestDto.setUserId(customerModel.getUid());
		requestDto.setEmail(customerModel.getContactEmail());
	}

	private void populateNameAttributes(final CustomerModel customerModel,
			final WileycomCustomerRegistrationRequestDto requestDto)
	{
		requestDto.setFirstName(customerModel.getFirstName());
		requestDto.setLastName(customerModel.getLastName());
		requestDto.setMiddleName(customerModel.getMiddleName());

		if (customerModel.getTitle() != null)
		{
			requestDto.setTitleCode(customerModel.getTitle().getCode());
		}

		if (customerModel.getSuffix() != null)
		{
			requestDto.setSuffixCode(customerModel.getSuffix().getCode());
		}
	}

	private void populateEducationAttributes(final CustomerModel customerModel,
			final WileycomCustomerRegistrationRequestDto requestDto)
	{
		requestDto.setStudentId(customerModel.getStudentId());
		requestDto.setMajor(customerModel.getMajor());

		if (customerModel.getGraduationYear() != null)
		{
			requestDto.setGraduationYear(Integer.parseInt(customerModel.getGraduationYear()));
		}

		if (customerModel.getGraduationMonth() != null)
		{
			requestDto.setGraduationMonth(Integer.parseInt(customerModel.getGraduationMonth()));
		}

		if (customerModel.getSchool() != null)
		{
			requestDto.setSchoolId(customerModel.getSchool().getCode());
		}
	}

	private void populateBusinessAttributes(final CustomerModel customerModel,
			final WileycomCustomerRegistrationRequestDto requestDto)
	{
		if (customerModel instanceof B2BCustomerModel)
		{
			B2BUnitModel b2BUnitModel = b2bUnitService.getParent((B2BCustomerModel) customerModel);
			requestDto.setOrganizationalSapAccountNumber(b2BUnitModel.getSapAccountNumber());
			requestDto.setOrganizationalEcidNumber(b2BUnitModel.getAccountEcidNumber());
		}
	}

	private void populateMiscellaneousAttributes(final CustomerModel customerModel,
			final WileycomCustomerRegistrationRequestDto requestDto)
	{
		requestDto.setSubscribeOnWileyPromos(customerModel.getAcceptPromotions());
	}

	private void populateCustomerRole(final CustomerModel customerModel,
			final WileycomCustomerRegistrationRequestDto requestDto)
	{
		if (customerModel instanceof B2BCustomerModel)
		{
			UserGroupModel adminGroupModel = userService.getUserGroupForUID("b2badmingroup");
			UserGroupModel customerGroupModel = userService.getUserGroupForUID("b2bcustomergroup");

			if (userService.isMemberOfGroup(customerModel, adminGroupModel))
			{
				requestDto.setRole(WileycomCustomerRegistrationRequestDto.RoleEnum.B2BADMIN);
			}
			else if (userService.isMemberOfGroup(customerModel, customerGroupModel))
			{
				requestDto.setRole(WileycomCustomerRegistrationRequestDto.RoleEnum.B2BUSER);
			}
		}
		else if (!StringUtils.isEmpty(customerModel.getStudentId()))
		{
			requestDto.setRole(WileycomCustomerRegistrationRequestDto.RoleEnum.STUDENT);
		}
		else
		{
			requestDto.setRole(WileycomCustomerRegistrationRequestDto.RoleEnum.INDIVIDUAL);
		}
	}
}
