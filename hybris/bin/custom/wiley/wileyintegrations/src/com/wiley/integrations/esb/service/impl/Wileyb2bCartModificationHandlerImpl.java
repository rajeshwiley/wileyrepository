package com.wiley.integrations.esb.service.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.product.UnitService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import com.wiley.core.integration.ExternalCartModification;
import com.wiley.core.order.data.CartModificationMessageType;
import com.wiley.core.product.WileyProductService;
import com.wiley.integrations.constants.WileyintegrationsConstants;
import com.wiley.integrations.esb.dto.verifyandcalculatecart.CalculationMessage;
import com.wiley.integrations.esb.dto.verifyandcalculatecart.CartCalculationEntryResponse;
import com.wiley.integrations.esb.service.CartModificationHandler;
import com.wiley.core.integration.esb.service.Wileyb2bCartEntryService;
import com.wiley.integrations.esb.service.Wileyb2bExternalDiscountApplier;
import com.wiley.integrations.esb.service.Wileyb2bExternalInventoryService;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.comparators.ComparableComparator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Default implementation.
 */
public class Wileyb2bCartModificationHandlerImpl implements CartModificationHandler
{

	private static final Logger LOG = LoggerFactory.getLogger(Wileyb2bCartModificationHandlerImpl.class);

	private static final String DEFAULT_PRODUCT_UNIT = "pieces";

	private String productUnit = DEFAULT_PRODUCT_UNIT;

	@Resource
	private CartService cartService;

	@Resource
	private WileyProductService productService;

	@Resource
	private UnitService unitService;

	@Resource
	private ModelService modelService;

	@Resource
	private Wileyb2bExternalInventoryService wileyb2bExternalInventoryService;

	@Resource
	private Wileyb2bExternalDiscountApplier wileyb2bExternalDiscountApplier;

	@Resource
	private Wileyb2bCartEntryService wileyb2bCartEntryService;

	@Override
	@Nonnull
	public List<ExternalCartModification> applyOrderModificationToCart(@Nonnull final CartModel cart,
			@Nonnull final List<CartCalculationEntryResponse> cartCalculationEntryResponse,
			@Nonnull final List<CalculationMessage> calculationMessages)
	{

		List<CartCalculationEntryResponse> entriesToAdd = getEntriesToAdd(cart, cartCalculationEntryResponse);

		List<CartCalculationEntryResponse> entriesToUpdate = getEntriesToUpdate(cart, cartCalculationEntryResponse);

		List<CartCalculationEntryResponse> entriesToRemove = getEntriesToRemove(cart, cartCalculationEntryResponse);


		// first initialization
		List<ExternalCartModification> resultList = Collections.emptyList();

		if (!entriesToAdd.isEmpty() || !entriesToUpdate.isEmpty() || !entriesToRemove.isEmpty())
		{
			List<ExternalCartModification> cartModifications = new ArrayList<>(
					entriesToAdd.size() + entriesToUpdate.size() + entriesToRemove.size());

			// add new entries to cart
			cartModifications.addAll(addCartEntries(cart, entriesToAdd, calculationMessages));

			// update existing entries
			cartModifications.addAll(updateCartEntries(cart, entriesToUpdate, calculationMessages));

			// remove existing entries
			cartModifications.addAll(removeCartEntries(cart, entriesToRemove, calculationMessages));

			resultList = cartModifications; // return a list with cart modifications.
		}

		return resultList;
	}

	private List<CartCalculationEntryResponse> getEntriesToRemove(final CartModel cart,
			final List<CartCalculationEntryResponse> cartCalculationEntryResponse)
	{
		List<AbstractOrderEntryModel> entries = cart.getEntries();

		List<CartCalculationEntryResponse> resultList = new ArrayList<>();

		// collect entries which are absent in response list but exist in cart entries
		if (CollectionUtils.isNotEmpty(entries))
		{
			entries.stream()
					.filter(cartEntry -> !doesExistInResponseEntries(cartEntry, cartCalculationEntryResponse))
					.map(this::createOrderEntryResponseToRemove)
					.collect(Collectors.toCollection(() -> resultList));
		}

		return resultList;
	}

	private List<CartCalculationEntryResponse> getEntriesToUpdate(final CartModel cart,
			final List<CartCalculationEntryResponse> cartCalculationEntryResponse)
	{
		List<AbstractOrderEntryModel> entries = cart.getEntries();

		int cartEntriesSize = entries != null ? entries.size() : 0;

		List<CartCalculationEntryResponse> resultList = new ArrayList<>(cartEntriesSize);

		// collect entries which exist in response list and exist in cart entries
		if (CollectionUtils.isNotEmpty(entries) && CollectionUtils.isNotEmpty(cartCalculationEntryResponse))
		{
			cartCalculationEntryResponse.stream()
					.filter(cartCalculationEntry -> doesExistInCart(cartCalculationEntry, cart))
					.collect(Collectors.toCollection(() -> resultList));
		}

		return resultList;
	}

	private List<CartCalculationEntryResponse> getEntriesToAdd(final CartModel cart,
			final List<CartCalculationEntryResponse> cartCalculationEntryResponse)
	{
		List<CartCalculationEntryResponse> resultList = new ArrayList<>();

		// collect entries which exist in response list but don't exist in cart entries
		if (CollectionUtils.isNotEmpty(cartCalculationEntryResponse))
		{
			cartCalculationEntryResponse.stream()
					.filter(cartCalculationEntry -> !doesExistInCart(cartCalculationEntry, cart))
					.collect(Collectors.toCollection(() -> resultList));
		}

		return resultList;
	}

	private boolean doesExistInCart(final CartCalculationEntryResponse cartCalculationEntry, final CartModel cart)
	{
		return wileyb2bCartEntryService.findCartEntryByIsbn(cart, cartCalculationEntry.getIsbn()).isPresent();
	}

	private List<ExternalCartModification> addCartEntries(final CartModel cart,
			final List<CartCalculationEntryResponse> entriesToAdd, final List<CalculationMessage> calculationMessages)
	{
		if (entriesToAdd.isEmpty())
		{
			return Collections.emptyList();
		}

		List<ExternalCartModification> resultList = new ArrayList<>(entriesToAdd.size());
		for (CartCalculationEntryResponse dto : entriesToAdd)
		{
			addCartEntry(resultList, cart, dto, calculationMessages);
		}

		return resultList;
	}

	private List<ExternalCartModification> updateCartEntries(final CartModel cart,
			final List<CartCalculationEntryResponse> entriesToUpdate, final List<CalculationMessage> calculationMessages)
	{
		if (entriesToUpdate.isEmpty())
		{
			return Collections.emptyList();
		}

		List<ExternalCartModification> resultList = new ArrayList<>(entriesToUpdate.size());

		// update needed entities at first
		for (CartCalculationEntryResponse dto : entriesToUpdate)
		{
			final Optional<CartEntryModel> cartEntry = wileyb2bCartEntryService.findCartEntryByIsbn(cart, dto.getIsbn());

			if (cartEntry.isPresent())
			{
				updateCartEntry(resultList, dto, cartEntry.get(), calculationMessages);
			}
		}

		return resultList;
	}

	private List<ExternalCartModification> removeCartEntries(final CartModel cart,
			final List<CartCalculationEntryResponse> entriesToRemove, final List<CalculationMessage> calculationMessages)
	{
		if (entriesToRemove.isEmpty())
		{
			return Collections.emptyList();
		}

		List<ExternalCartModification> resultList = new ArrayList<>(entriesToRemove.size());

		// then remove needed entries
		List<CartEntryModel> removedCartEntries = new ArrayList<>();
		for (CartCalculationEntryResponse dto : entriesToRemove)
		{
			final Optional<CartEntryModel> cartEntry = wileyb2bCartEntryService.findCartEntryByIsbn(cart, dto.getIsbn());

			if (cartEntry.isPresent())
			{
				removeCartEntry(resultList, dto, cartEntry.get(), calculationMessages);
				removedCartEntries.add(cartEntry.get());
			}
		}

		// remove cart entries from the cart's list
		final ArrayList<AbstractOrderEntryModel> newCartEntries = new ArrayList<>(cart.getEntries());
		newCartEntries.removeAll(removedCartEntries);
		cart.setEntries(newCartEntries);

		normalizeEntryNumbers(cart);

		return resultList;
	}

	private void addCartEntry(final List<ExternalCartModification> resultList, final CartModel cart,
			final CartCalculationEntryResponse dto,
			final List<CalculationMessage> calculationMessages)
	{
		final ProductModel product = productService.getProductForIsbn(dto.getIsbn());
		final Integer quantity = dto.getQuantity();
		final UnitModel unit = unitService.getUnitForCode(productUnit);

		final CartEntryModel cartEntry = cartService.addNewEntry(cart, product, quantity, unit);

		normalizePricesForEntry(dto, cartEntry);

		wileyb2bExternalInventoryService.updateInventoryStatuses(dto, cartEntry);

		wileyb2bExternalDiscountApplier.applyDiscounts(cartEntry, dto.getDiscounts());

		final Optional<CalculationMessage> calculationMessage = getCalculationMessageByIsbn(
				calculationMessages, dto.getIsbn());

		resultList.add(createExternalCartModification(cartEntry.getEntryNumber(), product.getCode(), quantity.longValue(),
				WileyintegrationsConstants.EXTERNAL_CART_MODIFICATION_ADDED_STATUS, calculationMessage));
	}

	private void updateCartEntry(final List<ExternalCartModification> resultList,
			final CartCalculationEntryResponse dto, final CartEntryModel cartEntry,
			final List<CalculationMessage> calculationMessages)
	{
		final ProductModel product = cartEntry.getProduct();

		// change quantity in existing cart entry
		cartEntry.setQuantity(dto.getQuantity().longValue());

		normalizePricesForEntry(dto, cartEntry);

		wileyb2bExternalInventoryService.updateInventoryStatuses(dto, cartEntry);

		wileyb2bExternalDiscountApplier.applyDiscounts(cartEntry, dto.getDiscounts());

		final Optional<CalculationMessage> calculationMessage = getCalculationMessageByIsbn(
				calculationMessages, dto.getIsbn());

		resultList.add(createExternalCartModification(cartEntry.getEntryNumber(), product.getCode(), cartEntry.getQuantity(),
				WileyintegrationsConstants.EXTERNAL_CART_MODIFICATION_UPDATED_STATUS, calculationMessage));
	}

	private void removeCartEntry(final List<ExternalCartModification> resultList,
			final CartCalculationEntryResponse dto, final CartEntryModel cartEntry,
			final List<CalculationMessage> calculationMessages)
	{
		final Integer originalEntryNumber = cartEntry.getEntryNumber();
		final ProductModel originalEntryProduct = cartEntry.getProduct();

		if (!modelService.isNew(cartEntry))
		{
			// remove only if cart entry is saved in db
			modelService.remove(cartEntry);
		}

		final Optional<CalculationMessage> calculationMessage = getCalculationMessageByIsbn(
				calculationMessages, dto.getIsbn());

		resultList.add(createExternalCartModification(originalEntryNumber, originalEntryProduct.getCode(), 0L,
				WileyintegrationsConstants.EXTERNAL_CART_MODIFICATION_REMOVED_STATUS, calculationMessage));
	}

	private void normalizeEntryNumbers(final CartModel cart)
	{
		final List<AbstractOrderEntryModel> entries = new ArrayList<>(cart.getEntries());
		Collections.sort(entries,
				new BeanComparator(AbstractOrderEntryModel.ENTRYNUMBER, new ComparableComparator()));
		int en = 0;
		for (AbstractOrderEntryModel entry : entries)
		{
			entry.setEntryNumber(en++);
		}
	}

	private ExternalCartModification createExternalCartModification(final Integer entryNumber, final String productCode,
			final Long quantity, final String statusCode, final Optional<CalculationMessage> calculationMessage)
	{
		ExternalCartModification.Builder externalCartModificationBuilder = ExternalCartModification.Builder.get()
				.withEntryNumber(entryNumber)
				.withProductCode(productCode)
				.withQuantity(quantity)
				.withStatusCode(statusCode);

		if (calculationMessage.isPresent())
		{
			externalCartModificationBuilder
					.withMessage(calculationMessage.get().getMessage())
					.withEventCode(calculationMessage.get().getCode());

			CalculationMessage.MessageTypeEnum messageType = calculationMessage.get().getMessageType();
			switch (messageType)
			{
				case INFO:
					externalCartModificationBuilder.withMessageType(CartModificationMessageType.INFO);
					break;
				case WARNING:
					externalCartModificationBuilder.withMessageType(CartModificationMessageType.CONF);
					break;
				case ERROR:
					externalCartModificationBuilder.withMessageType(CartModificationMessageType.ERROR);
					break;
				default:
					LOG.warn("Unknown message type [{}]. Using [error] message type.", messageType);
					externalCartModificationBuilder.withMessageType(CartModificationMessageType.ERROR);
			}
		}
		return externalCartModificationBuilder.build();
	}

	private void normalizePricesForEntry(final CartCalculationEntryResponse dto, final CartEntryModel cartEntry)
	{
		cartEntry.setBasePrice(dto.getBasePrice());
		cartEntry.setTotalPrice(dto.getTotalPrice());
	}

	private Optional<CalculationMessage> getCalculationMessageByIsbn(final List<CalculationMessage> calculationMessages,
			final String isbn)
	{
		Optional<CalculationMessage> result = Optional.empty();
		if (CollectionUtils.isNotEmpty(calculationMessages))
		{
			result = calculationMessages.stream()
					.filter(message -> isbn.equals(message.getIsbn()))
					.findFirst();
		}
		return result;
	}

	private boolean doesExistInResponseEntries(final AbstractOrderEntryModel cartEntry,
			final List<CartCalculationEntryResponse> cartCalculationEntryResponse)
	{
		ProductModel product = cartEntry.getProduct();
		String isbn = product.getIsbn();
		return cartCalculationEntryResponse.stream()
				.anyMatch(responseEntry -> isbn.equals(responseEntry.getIsbn()));
	}

	private CartCalculationEntryResponse createOrderEntryResponseToRemove(final AbstractOrderEntryModel cartEntry)
	{
		CartCalculationEntryResponse cartCalculationEntryResponse = new CartCalculationEntryResponse();
		cartCalculationEntryResponse.setIsbn(cartEntry.getProduct().getIsbn());
		cartCalculationEntryResponse.setQuantity(cartEntry.getQuantity().intValue());
		return cartCalculationEntryResponse;
	}
}
