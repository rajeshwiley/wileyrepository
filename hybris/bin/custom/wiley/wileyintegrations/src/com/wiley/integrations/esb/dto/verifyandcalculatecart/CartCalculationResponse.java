package com.wiley.integrations.esb.dto.verifyandcalculatecart;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wiley.integrations.esb.dto.common.Discount;

import org.apache.commons.lang3.builder.ToStringBuilder;


/**
 * The result of the order calculation.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CartCalculationResponse
{

	/**
	 * The full list of the order entries. The order entries from the request might be adjusted, new entries might be added.
	 * If an entry is absent in this list, it is considered as removed. Only one entry can exist for a given product code.
	 **/
	private List<CartCalculationEntryResponse> entries = new ArrayList<CartCalculationEntryResponse>();
	/**
	 * Sum of the totals of all the order entries.
	 * An entry's total is calculated as the entry's original price without any discount multipled by the entry's quantity.
	 * The en_US format is used for the value.
	 **/
	private Double subtotal = null;

	/**
	 * Arrays of applied global (order level) discounts
	 */
	private List<Discount> discounts = new ArrayList<>();

	/**
	 * Sum of all discounts (including any promotions and vouchers) applied to the order. The en_US format is used for the value.
	 **/
	private Double totalDiscount = null;
	/**
	 * Delivery cost for the chosen delivery option. The en_US format is used for the value.
	 **/
	private Double deliveryCost = null;
	/**
	 * Total tax for the order. The en_US format is used for the value.
	 **/
	private Double totalTax = null;
	/**
	 * Total price for the order. Total price is calculated as subtotal minus totalDiscount plus deliveryCost plus totalTax.
	 * The en_US format is used for the value.
	 **/
	private Double totalPrice = null;
	/**
	 * The list of messages for the user describing events occured on the calculation.
	 **/
	private List<CalculationMessage> messages = new ArrayList<CalculationMessage>();

	@JsonProperty("entries")
	public List<CartCalculationEntryResponse> getEntries()
	{
		return entries;
	}

	public void setEntries(final List<CartCalculationEntryResponse> entries)
	{
		this.entries = entries;
	}

	@JsonProperty("subtotal")
	public Double getSubtotal()
	{
		return subtotal;
	}

	public void setSubtotal(final Double subtotal)
	{
		this.subtotal = subtotal;
	}

	@JsonProperty("discounts")
	public List<Discount> getDiscounts()
	{
		return discounts;
	}

	public void setDiscounts(final List<Discount> discounts)
	{
		this.discounts = discounts;
	}

	@JsonProperty("totalDiscount")
	public Double getTotalDiscount()
	{
		return totalDiscount;
	}

	public void setTotalDiscount(final Double totalDiscount)
	{
		this.totalDiscount = totalDiscount;
	}

	@JsonProperty("deliveryCost")
	public Double getDeliveryCost()
	{
		return deliveryCost;
	}

	public void setDeliveryCost(final Double deliveryCost)
	{
		this.deliveryCost = deliveryCost;
	}

	@JsonProperty("totalTax")
	public Double getTotalTax()
	{
		return totalTax;
	}

	public void setTotalTax(final Double totalTax)
	{
		this.totalTax = totalTax;
	}

	@JsonProperty("totalPrice")
	public Double getTotalPrice()
	{
		return totalPrice;
	}

	public void setTotalPrice(final Double totalPrice)
	{
		this.totalPrice = totalPrice;
	}

	@JsonProperty("messages")
	public List<CalculationMessage> getMessages()
	{
		return messages;
	}

	public void setMessages(final List<CalculationMessage> messages)
	{
		this.messages = messages;
	}

	@Override
	public String toString()
	{
		return new ToStringBuilder(this)
				.append("entries", entries)
				.append("subtotal", subtotal)
				.append("discounts", discounts)
				.append("totalDiscount", totalDiscount)
				.append("deliveryCost", deliveryCost)
				.append("totalTax", totalTax)
				.append("totalPrice", totalPrice)
				.append("messages", messages)
				.toString();
	}
}

