package com.wiley.integrations.tax.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.math.BigDecimal;

import javax.annotation.Resource;

import com.wiley.core.externaltax.dto.TaxAddressDto;
import com.wiley.core.externaltax.dto.TaxCalculationRequestDto;
import com.wiley.core.externaltax.dto.TaxCalculationRequestEntryDto;
import com.wiley.integrations.tax.dto.TaxAddress;
import com.wiley.integrations.tax.dto.TaxCalculationRequest;
import com.wiley.integrations.tax.dto.TaxCalculationRequestEntry;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


public class TaxCalculationRequestPopulator implements Populator<TaxCalculationRequestDto, TaxCalculationRequest>
{

	@Resource
	private Converter<TaxAddressDto, TaxAddress> taxAddressConverter;
	@Resource
	private Converter<TaxCalculationRequestEntryDto, TaxCalculationRequestEntry> taxCalculationRequestEntryConverter;

	@Override
	public void populate(final TaxCalculationRequestDto source, final TaxCalculationRequest target) throws ConversionException
	{
		validateParameterNotNull(source, "source mustn't be null");
		validateParameterNotNull(target, "target mustn't be null");

		target.setSiteId(source.getSiteId());
		target.setDate(source.getDate());
		target.setCurrency(source.getCurrency());
		target.setPaymentAddress(convertAddress(source.getPaymentAddress()));
		target.setDeliveryAddress(convertAddress(source.getDeliveryAddress()));
		target.setEntries(taxCalculationRequestEntryConverter.convertAll(source.getEntries()));
		target.setHandlingCost(BigDecimal.valueOf(source.getHandlingCost()));
		target.setShippingCost(BigDecimal.valueOf(source.getShippingCost()));
	}

	private TaxAddress convertAddress(final TaxAddressDto taxAddressDto)
	{
		if (taxAddressDto != null)
		{
			return taxAddressConverter.convert(taxAddressDto);
		}
		return null;
	}
}
