package com.wiley.integrations.ediintegration.endpoints.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;


@XmlAccessorType(XmlAccessType.FIELD)
public class ShipToCustomer
{
	@XmlElement(name = "CustomerName")
	private String customerName;
	@XmlElement(name = "CustomerName2")
	private String customerName2;
	@XmlElement(name = "AddressLine1")
	private String addressLine1;
	@XmlElement(name = "AddressLine2")
	private String addressLine2;
	@XmlElement(name = "CityName")
	private String cityName;
	@XmlElement(name = "Country")
	private String country;
	@XmlElement(name = "StateProvince")
	private String stateProvince;
	@XmlElement(name = "ZipPostCode")
	private String zipPostCode;

	public String getCustomerName()
	{
		return customerName;
	}

	public void setCustomerName(final String customerName)
	{
		this.customerName = customerName;
	}

	public String getCustomerName2()
	{
		return customerName2;
	}

	public void setCustomerName2(final String customerName2)
	{
		this.customerName2 = customerName2;
	}

	public String getAddressLine1()
	{
		return addressLine1;
	}

	public void setAddressLine1(final String addressLine1)
	{
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2()
	{
		return addressLine2;
	}

	public void setAddressLine2(final String addressLine2)
	{
		this.addressLine2 = addressLine2;
	}

	public String getCityName()
	{
		return cityName;
	}

	public void setCityName(final String cityName)
	{
		this.cityName = cityName;
	}

	public String getCountry()
	{
		return country;
	}

	public void setCountry(final String country)
	{
		this.country = country;
	}

	public String getStateProvince()
	{
		return stateProvince;
	}

	public void setStateProvince(final String stateProvince)
	{
		this.stateProvince = stateProvince;
	}

	public String getZipPostCode()
	{
		return zipPostCode;
	}

	public void setZipPostCode(final String zipPostCode)
	{
		this.zipPostCode = zipPostCode;
	}
}
