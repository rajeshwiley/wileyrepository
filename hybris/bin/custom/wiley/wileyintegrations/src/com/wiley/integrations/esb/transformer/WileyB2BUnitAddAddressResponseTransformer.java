package com.wiley.integrations.esb.transformer;

import com.wiley.integrations.esb.dto.EsbB2BUnitAddressCreateResponseDto;
import org.springframework.messaging.Message;

import javax.annotation.Nonnull;


public class WileyB2BUnitAddAddressResponseTransformer
{
	@Nonnull
	public String transform(@Nonnull final Message<EsbB2BUnitAddressCreateResponseDto> message)
	{
		return message.getPayload().getAddressId();
	}

}
