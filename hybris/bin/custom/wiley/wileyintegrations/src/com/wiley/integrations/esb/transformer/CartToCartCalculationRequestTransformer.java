package com.wiley.integrations.esb.transformer;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.util.DiscountValue;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.util.Assert;

import com.wiley.core.model.WileyExternalDiscountModel;
import com.wiley.integrations.esb.dto.common.Address;
import com.wiley.integrations.esb.dto.verifyandcalculatecart.CartCalculationEntryRequest;
import com.wiley.integrations.esb.dto.verifyandcalculatecart.CartCalculationRequest;


/**
 * Transforms CartModel to corresponding OrderDto. This transformer is used in ERP gateway for cart calculation and verification.
 */
public class CartToCartCalculationRequestTransformer
{

	@Resource
	private Populator<CartModel, CartCalculationRequest> cartToCartCalculationRequestSimplePopulator;

	@Resource
	private Populator<CartEntryModel, CartCalculationEntryRequest> cartEntryToCartCalculationEntryRequestPopulator;

	@Resource
	private Populator<AddressModel, Address> addressModelToAddressDtoPopulator;
	
	@Resource
	private Converter<WileyExternalDiscountModel, DiscountValue> wileyExternalDiscountConverter;

	@Nonnull
	public CartCalculationRequest transform(@Nonnull final CartModel cart)
	{
		Assert.notNull(cart);

		CartCalculationRequest orderDto = createOrderDto();

		// populate simple fields
		cartToCartCalculationRequestSimplePopulator.populate(cart, orderDto);

		// populate order entries data
		populateOrderEntries(cart, orderDto);

		populateDeliveryAddress(cart, orderDto);

		populatePaymentAddress(cart, orderDto);

		populateDiscountCodes(cart, orderDto);

		return orderDto;
	}

	private void populateDiscountCodes(final CartModel cartModel, final CartCalculationRequest orderDto)
	{
		orderDto.setDiscountValues(new HashSet<>(wileyExternalDiscountConverter.convertAll(cartModel.getExternalDiscounts())));
	}

	private void populatePaymentAddress(final CartModel cart, final CartCalculationRequest orderDto)
	{
		final PaymentInfoModel paymentInfo = cart.getPaymentInfo();
		if (paymentInfo != null)
		{
			final AddressModel billingAddress = paymentInfo.getBillingAddress();
			if (billingAddress != null)
			{
				final Address paymentAddressDto = createAddressDto();
				addressModelToAddressDtoPopulator.populate(billingAddress, paymentAddressDto);
				orderDto.setPaymentAddress(paymentAddressDto);
			}
		}
	}

	private void populateDeliveryAddress(final CartModel cart, final CartCalculationRequest orderDto)
	{
		final AddressModel deliveryAddressModel = cart.getDeliveryAddress();
		if (deliveryAddressModel != null)
		{
			final Address deliveryAddressDto = createAddressDto();
			addressModelToAddressDtoPopulator.populate(deliveryAddressModel, deliveryAddressDto);
			orderDto.setDeliveryAddress(deliveryAddressDto);
		}
	}

	private void populateOrderEntries(final CartModel cart, final CartCalculationRequest orderDto)
	{
		final List<AbstractOrderEntryModel> entries = cart.getEntries();

		if (CollectionUtils.isNotEmpty(entries))
		{
			final List<CartCalculationEntryRequest> orderEntries = entries.stream()
					.map(orderEntry -> (CartEntryModel) orderEntry)
					.map(cartEntry -> {
						final CartCalculationEntryRequest orderEntryDto = createOrderEntryDto();
						cartEntryToCartCalculationEntryRequestPopulator.populate(cartEntry, orderEntryDto);
						return orderEntryDto;
					})
					.collect(Collectors.toList());
			orderDto.setEntries(orderEntries);
		}
		else
		{
			orderDto.setEntries(Collections.emptyList());
		}
	}

	private CartCalculationRequest createOrderDto()
	{
		return new CartCalculationRequest();
	}

	private CartCalculationEntryRequest createOrderEntryDto()
	{
		return new CartCalculationEntryRequest();
	}

	private Address createAddressDto()
	{
		return new Address();
	}

}
