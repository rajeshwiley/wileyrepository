package com.wiley.integrations.mpgs.transformer;

import com.wiley.core.mpgs.dto.retrievesession.MPGSRetrieveSessionRequestDTO;
import com.wiley.core.mpgs.request.WileyRetrieveSessionRequest;


public class WileyMPGSRetrieveSessionRequestTransformer
{
	public MPGSRetrieveSessionRequestDTO transform(final WileyRetrieveSessionRequest request)
	{
		return new MPGSRetrieveSessionRequestDTO();
	}
}
