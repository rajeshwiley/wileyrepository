package com.wiley.integrations.esb.converters.populator.discount;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.util.DiscountValue;

import javax.annotation.Nonnull;

import com.wiley.integrations.esb.dto.common.Discount;

import org.springframework.util.Assert;


/**
 * Populates {@link Discount} from {@link DiscountValue}
 */
public class DiscountValueToDiscountPopulator implements Populator<DiscountValue, Discount>
{
	@Override
	public void populate(@Nonnull final DiscountValue discountValue, @Nonnull final Discount discount) throws ConversionException
	{
		Assert.notNull(discountValue);
		Assert.notNull(discount);

		discount.setAbsolute(discountValue.isAbsolute());
		discount.setName(discountValue.getCode());
		discount.setValue(discountValue.getValue());
	}
}
