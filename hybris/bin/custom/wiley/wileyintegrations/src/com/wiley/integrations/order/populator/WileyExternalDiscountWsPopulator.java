package com.wiley.integrations.order.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import com.wiley.core.model.WileyExternalDiscountModel;
import com.wiley.integrations.order.dto.DiscountValueWsDTO;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


public class WileyExternalDiscountWsPopulator implements Populator<WileyExternalDiscountModel, DiscountValueWsDTO>
{
	@Override
	public void populate(final WileyExternalDiscountModel source, final DiscountValueWsDTO target) throws ConversionException
	{
		validateParameterNotNullStandardMessage("source", source);
		validateParameterNotNullStandardMessage("target", target);

		target.setCode(source.getCode());
		target.setValue(source.getValue());
		target.setAbsolute(source.getAbsolute());
		if (source.getCurrency() != null)
		{
			target.setCurrency(source.getCurrency().getIsocode());
		}
		target.setPromoTypeGroup(source.getPromoTypeGroup());
		target.setPromoInstitution(source.getPromoInstitution());
		target.setCouponCodes(source.getCouponCodes());
	}
}