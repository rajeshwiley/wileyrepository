package com.wiley.integrations.b2b.unit.transformer;


import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.wiley.integrations.b2b.unit.dto.PoNumberValidationResultSwgDTO;

import org.apache.commons.collections.CollectionUtils;

import static java.lang.Boolean.TRUE;


public class PoNumberValidationResponseTransformer
{
	public List<String> transform(final String stringResponseBody)
	{
		List<String> invalidPONumbers = Collections.emptyList();
		Type resultsListType = new TypeToken<List<PoNumberValidationResultSwgDTO>>() { } .getType();

		final List<PoNumberValidationResultSwgDTO> validationResults = new Gson().fromJson(stringResponseBody,
				resultsListType);

		if (CollectionUtils.isNotEmpty(validationResults))
		{
			invalidPONumbers = validationResults.stream()
					.filter(validationResult -> (validationResult != null) && !TRUE.equals(validationResult.getValid()))
					.map(PoNumberValidationResultSwgDTO::getPoNumber)
					.collect(Collectors.toList());
		}
		return invalidPONumbers;
	}

}
