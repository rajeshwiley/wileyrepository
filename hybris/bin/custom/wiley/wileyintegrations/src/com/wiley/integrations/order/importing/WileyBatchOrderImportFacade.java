package com.wiley.integrations.order.importing;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public interface WileyBatchOrderImportFacade
{
	List<String> importOrdersFromStream(InputStream orders, String baseSiteId, String userCreationStrategy)
			throws IOException;
}
