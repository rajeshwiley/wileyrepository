package com.wiley.integrations.users.transformer;

import javax.annotation.Nonnull;

import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;

import com.wiley.core.integration.users.gateway.WileyUsersGateway;
import com.wiley.integrations.users.data.UidUpdateRequest;


public class WileyUserUpdateUserIdRequestTransformer
{

	public UidUpdateRequest transform(@Nonnull @Payload final String newUid,
			@Header(WileyUsersGateway.HEADER_NEW_PASSWORD) final String password)
	{
		final UidUpdateRequest uidUpdateRequest = new UidUpdateRequest();
		uidUpdateRequest.setUserId(newUid);
		uidUpdateRequest.setPassword(password);
		return uidUpdateRequest;
	}
}
