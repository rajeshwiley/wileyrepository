package com.wiley.integrations.esb.transformer;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.util.DiscountValue;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import com.wiley.integrations.esb.dto.common.Address;
import com.wiley.integrations.esb.dto.common.Discount;
import com.wiley.integrations.esb.dto.order.OrderEntryRequest;
import com.wiley.integrations.esb.dto.order.OrderRequest;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;


/**
 * Transforms {@link OrderModel} to {@link OrderRequest}
 */
public class OrderToOrderRequestTransformer
{

	private static final Logger LOG = LoggerFactory.getLogger(OrderToOrderRequestTransformer.class);

	@Resource
	private Populator<AddressModel, Address> addressModelToAddressDtoPopulator;

	@Resource
	private Populator<OrderEntryModel, OrderEntryRequest> orderEntryRequestPopulator;

	@Resource
	private Populator<DiscountValue, Discount> discountPopulator;

	@Resource
	private Populator<OrderModel, OrderRequest> orderSimpleFieldsPopulator;

	public OrderRequest transform(@Nonnull final OrderModel orderModel)
	{
		Assert.notNull(orderModel);

		OrderRequest orderRequest = createOrderRequestInstance();

		orderSimpleFieldsPopulator.populate(orderModel, orderRequest);

		populateDeliveryAddress(orderModel, orderRequest);

		populatePaymentAddress(orderModel, orderRequest);

		populateEntries(orderModel, orderRequest);

		populateDiscounts(orderModel, orderRequest);

		populateExtraInfo(orderRequest);

		return orderRequest;
	}

	private void populateExtraInfo(final OrderRequest orderRequest)
	{
		// TODO: implementation to be defined
		orderRequest.setExtraInfo(null);
		LOG.error("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Method is not implemented!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
	}

	private void populateDiscounts(final OrderModel orderModel, final OrderRequest orderRequest)
	{
		final List<DiscountValue> globalDiscountValues = orderModel.getGlobalDiscountValues();
		if (CollectionUtils.isNotEmpty(globalDiscountValues))
		{
			orderRequest.setDiscounts(globalDiscountValues.stream()
					.map(this::convertDiscountValueToDiscount)
					.collect(Collectors.toList()));
		}
	}

	private Discount convertDiscountValueToDiscount(final DiscountValue discountValue)
	{
		final Discount discount = createDiscountInstance();
		discountPopulator.populate(discountValue, discount);
		return discount;
	}

	private void populateEntries(final OrderModel orderModel, final OrderRequest orderRequest)
	{
		final List<AbstractOrderEntryModel> entries = orderModel.getEntries();
		if (CollectionUtils.isNotEmpty(entries))
		{
			orderRequest.setEntries(entries.stream()
					.map(entry -> (OrderEntryModel) entry)
					.map(this::convertEntryModelToEntryRequest)
					.collect(Collectors.toList()));
		}
	}

	private OrderEntryRequest convertEntryModelToEntryRequest(final OrderEntryModel entryModel)
	{
		final OrderEntryRequest orderEntryRequest = createOrderEntryRequestInstance();
		orderEntryRequestPopulator.populate(entryModel, orderEntryRequest);
		return orderEntryRequest;
	}

	private void populatePaymentAddress(final OrderModel orderModel, final OrderRequest orderRequest)
	{
		final AddressModel paymentAddress = orderModel.getPaymentAddress();
		if (paymentAddress != null)
		{
			final Address paymentAddressDto = createAddressInstance();
			addressModelToAddressDtoPopulator.populate(paymentAddress, paymentAddressDto);
			orderRequest.setPaymentAddress(paymentAddressDto);
		}
	}

	private void populateDeliveryAddress(final OrderModel orderModel, final OrderRequest orderRequest)
	{
		final AddressModel deliveryAddress = orderModel.getDeliveryAddress();
		if (deliveryAddress != null)
		{
			final Address deliveryAddressDto = createAddressInstance();
			addressModelToAddressDtoPopulator.populate(deliveryAddress, deliveryAddressDto);
			orderRequest.setDeliveryAddress(deliveryAddressDto);
		}
	}

	private OrderEntryRequest createOrderEntryRequestInstance()
	{
		return new OrderEntryRequest();
	}

	private Address createAddressInstance()
	{
		return new Address();
	}

	private OrderRequest createOrderRequestInstance()
	{
		return new OrderRequest();
	}

	private Discount createDiscountInstance()
	{
		return new Discount();
	}
}
