package com.wiley.integrations.sabrix.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TargetSystemType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="TargetSystemType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TargetSystem" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1}SystemNameType" minOccurs="0"/&gt;
 *         &lt;element name="ServiceName" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1}NameType" minOccurs="0"/&gt;
 *         &lt;element name="EndPointURI" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1}URLAddressType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TargetSystemType", namespace = "http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/Meta/V1", propOrder = {
		"targetSystem",
		"serviceName",
		"endPointURI"
})
public class TargetSystemType
{

	@XmlElement(name = "TargetSystem")
	protected SystemNameType targetSystem;
	@XmlElement(name = "ServiceName")
	protected NameType serviceName;
	@XmlElement(name = "EndPointURI")
	protected String endPointURI;

	/**
	 * Gets the value of the targetSystem property.
	 *
	 * @return possible object is
	 * {@link SystemNameType }
	 */
	public SystemNameType getTargetSystem()
	{
		return targetSystem;
	}

	/**
	 * Sets the value of the targetSystem property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link SystemNameType }
	 */
	public void setTargetSystem(SystemNameType value)
	{
		this.targetSystem = value;
	}

	/**
	 * Gets the value of the serviceName property.
	 *
	 * @return possible object is
	 * {@link NameType }
	 */
	public NameType getServiceName()
	{
		return serviceName;
	}

	/**
	 * Sets the value of the serviceName property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link NameType }
	 */
	public void setServiceName(NameType value)
	{
		this.serviceName = value;
	}

	/**
	 * Gets the value of the endPointURI property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getEndPointURI()
	{
		return endPointURI;
	}

	/**
	 * Sets the value of the endPointURI property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setEndPointURI(String value)
	{
		this.endPointURI = value;
	}

}
