package com.wiley.integrations.ebp.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.google.common.base.MoreObjects;


/**
 * Created by Uladzimir_Barouski on 2/18/2016.
 */
@XmlRootElement(namespace = "www.efficientlearning.com/wsdl", name = "addProducts")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "addProducts")
public class EbpAddProductsRootNode
{

	@XmlElement(required = true, name = "arg0")
	private AddProductsArgNode argNode;

	public AddProductsArgNode getArgNode()
	{
		return argNode;
	}

	public void setArgNode(final AddProductsArgNode argNode)
	{
		this.argNode = argNode;
	}

	@Override
	public String toString()
	{
		final MoreObjects.ToStringHelper result = MoreObjects.toStringHelper(this)
			.add("argNode", argNode);
		return result.toString();
	}
}
