package com.wiley.integrations.tax.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import com.wiley.core.externaltax.dto.TaxValueDto;
import com.wiley.integrations.tax.dto.TaxValue;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


public class TaxValueDtoPopulator implements Populator<TaxValue, TaxValueDto>
{
	@Override
	public void populate(final TaxValue source, final TaxValueDto target) throws ConversionException
	{
		validateParameterNotNull(source, "source mustn't be null");
		validateParameterNotNull(target, "target mustn't be null");

		target.setCode(source.getCode());
		target.setValue(source.getValue().doubleValue());
	}
}
