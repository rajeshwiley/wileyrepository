package com.wiley.integrations.ediintegration.endpoints.jaxb.builder;

import java.util.List;

import com.wiley.integrations.ediintegration.endpoints.jaxb.PurchaseOrder;
import com.wiley.integrations.ediintegration.endpoints.jaxb.PurchaseOrderToWILEY;
import com.wiley.integrations.ediintegration.endpoints.jaxb.Routing;


public final class PurchaseOrderToWILEYBuilder
{
	private Routing routing;
	private List<PurchaseOrder> purchaseOrderList;

	private PurchaseOrderToWILEYBuilder()
	{
	}

	public static PurchaseOrderToWILEYBuilder createInstance()
	{
		return new PurchaseOrderToWILEYBuilder();
	}

	public PurchaseOrderToWILEYBuilder withRouting(final Routing routing)
	{
		this.routing = routing;
		return this;
	}

	public PurchaseOrderToWILEYBuilder withPurchaseOrderList(final List<PurchaseOrder> purchaseOrderList)
	{
		this.purchaseOrderList = purchaseOrderList;
		return this;
	}

	public PurchaseOrderToWILEYBuilder but()
	{
		return createInstance().withRouting(routing).withPurchaseOrderList(purchaseOrderList);
	}

	public PurchaseOrderToWILEY build()
	{
		PurchaseOrderToWILEY purchaseOrderToWILEY = new PurchaseOrderToWILEY();
		purchaseOrderToWILEY.setRouting(routing);
		purchaseOrderToWILEY.setPurchaseOrderList(purchaseOrderList);
		return purchaseOrderToWILEY;
	}
}
