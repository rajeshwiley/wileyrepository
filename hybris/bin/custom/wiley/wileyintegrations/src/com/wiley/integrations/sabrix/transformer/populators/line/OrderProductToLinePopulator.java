package com.wiley.integrations.sabrix.transformer.populators.line;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.wiley.core.product.WileyProductEditionFormatService;
import com.wiley.core.wileycom.ship.from.WileycomShipFromResolvingStrategy;
import com.wiley.integrations.sabrix.dto.IndataLineType;
import com.wiley.integrations.sabrix.dto.ZoneAddressType;
import com.wiley.integrations.sabrix.transformer.populators.AddressToZoneAddressTypePopulator;


/**
 * Author Herman_Chukhrai (EPAM)
 */
public class OrderProductToLinePopulator implements Populator<ProductModel, IndataLineType>
{
	//It looks strange, but it is approved by Fiona
	private static final String FAKE_NUMBER = "12345";
	@Resource
	private WileyProductEditionFormatService wileyProductEditionFormatService;
	@Resource
	private AddressToZoneAddressTypePopulator addressToZoneAddressTypePopulator;
	@Resource
	private WileycomShipFromResolvingStrategy wileycomShipFromResolvingStrategy;

	@Override
	public void populate(@Nonnull final ProductModel product, @Nonnull final IndataLineType indataLineType)
			throws ConversionException
	{
		boolean isDigital = wileyProductEditionFormatService.isDigitalProduct(product);
		ZoneAddressType shipFromAddressType = new ZoneAddressType();
		AddressModel shipFromAddress = wileycomShipFromResolvingStrategy.resolveShipFromAddress(product);
		addressToZoneAddressTypePopulator.populate(shipFromAddress, shipFromAddressType);

		if (isDigital)
		{
			indataLineType.setORDERORIGIN(shipFromAddressType);
		}

		indataLineType.setORDERACCEPTANCE(shipFromAddressType);
		indataLineType.setPARTNUMBER(FAKE_NUMBER);
		Assert.hasLength(product.getPdmCode());
		indataLineType.setPRODUCTCODE(product.getPdmCode());
		indataLineType.setSHIPFROM(shipFromAddressType);
		Assert.hasLength(product.getTaxTransactionType());
		indataLineType.setTRANSACTIONTYPE(product.getTaxTransactionType());
		indataLineType.setSELLERPRIMARY(shipFromAddressType);
	}
}
