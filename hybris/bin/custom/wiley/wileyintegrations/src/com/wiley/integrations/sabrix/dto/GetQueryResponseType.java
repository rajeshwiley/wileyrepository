package com.wiley.integrations.sabrix.dto;

import java.math.BigInteger;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetQueryResponseType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="GetQueryResponseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TotalRecords" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1}NonNegativeIntegerType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetQueryResponseType", namespace = "http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/Meta/V1", propOrder = {
		"totalRecords"
})
public class GetQueryResponseType
{

	@XmlElement(name = "TotalRecords")
	@XmlSchemaType(name = "nonNegativeInteger")
	protected BigInteger totalRecords;

	/**
	 * Gets the value of the totalRecords property.
	 *
	 * @return possible object is
	 * {@link BigInteger }
	 */
	public BigInteger getTotalRecords()
	{
		return totalRecords;
	}

	/**
	 * Sets the value of the totalRecords property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link BigInteger }
	 */
	public void setTotalRecords(BigInteger value)
	{
		this.totalRecords = value;
	}

}
