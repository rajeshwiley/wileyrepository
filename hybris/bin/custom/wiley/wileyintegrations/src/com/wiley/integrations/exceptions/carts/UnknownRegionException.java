package com.wiley.integrations.exceptions.carts;

/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public class UnknownRegionException extends RuntimeException
{
	public UnknownRegionException(final String message)
	{
		super(message);
	}
}
