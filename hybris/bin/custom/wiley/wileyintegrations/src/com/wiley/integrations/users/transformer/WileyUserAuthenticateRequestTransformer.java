package com.wiley.integrations.users.transformer;

import de.hybris.platform.core.model.user.UserModel;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;

import com.wiley.core.integration.users.gateway.WileyUsersGateway;
import com.wiley.integrations.users.data.UserCredentials;
import com.wiley.integrations.users.data.WileyUserAuthenticateRequest;
import com.wiley.integrations.users.populator.WileyAuthenticationPopulator;
import com.wiley.integrations.users.populator.WileyUserRequestPopulator;

public class WileyUserAuthenticateRequestTransformer
{
	@Resource
	private WileyAuthenticationPopulator wileyAuthenticationPopulator;

	@Resource
	private WileyUserRequestPopulator wileyUserRequestPopulator;


	public WileyUserAuthenticateRequest transform(@Nonnull @Payload final UserModel user,
			@Nonnull @Header(WileyUsersGateway.HEADER_NEW_PASSWORD) final String password)
	{
		final WileyUserAuthenticateRequest request = new WileyUserAuthenticateRequest();
		request.setUserCredentials(new UserCredentials());
		wileyUserRequestPopulator.populate(user, request);
		wileyAuthenticationPopulator.populate(password, request);

		return request;
	}
}
