package com.wiley.integrations.ediintegration.endpoints.transformers.converters;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.paypal.hybris.model.PaypalPaymentInfoModel;
import com.wiley.fulfilmentprocess.model.WileyExportProcessModel;
import com.wiley.core.integration.edi.EDIConstants;
import com.wiley.integrations.ediintegration.endpoints.jaxb.BillToCustomer;
import com.wiley.integrations.ediintegration.endpoints.jaxb.ItemsStructure;
import com.wiley.integrations.ediintegration.endpoints.jaxb.PurchaseOrder;
import com.wiley.integrations.ediintegration.endpoints.jaxb.ShipToCustomer;
import com.wiley.core.integration.edi.service.impl.WileyPurchaseOrderService;


public class PurchaseOrderPopulator	implements Populator<WileyExportProcessModel, PurchaseOrder>
{

	private static final Logger LOG = Logger.getLogger(PurchaseOrderPopulator.class);
	public static final String PP_HYBRIS_AUTH_CODE_VALUE = "0";


	@Resource
	private ItemsStructurePopulator itemsStructurePopulator;
	@Resource
	private BillToCustomerPopulator billToCustomerPopulator;
	@Resource
	private ShipToCustomerPopulator shipToCustomerPopulator;
	@Resource
	private WileyPurchaseOrderService wileyPurchaseOrderService;

	@Override
	public void populate(final WileyExportProcessModel exportProcess,
			final PurchaseOrder purchaseOrder) throws ConversionException
	{
		final OrderModel orderModel = exportProcess.getOrder();
		LOG.info("Converting orders with code: [" + orderModel.getCode() + "]");

		List<ItemsStructure> itemsStructures = new ArrayList<>();
		itemsStructurePopulator.populate(exportProcess, itemsStructures);

		ShipToCustomer shipToCustomer = new ShipToCustomer();
		shipToCustomerPopulator.populate(orderModel, shipToCustomer);

		BillToCustomer billToCustomer = new BillToCustomer();
		billToCustomerPopulator.populate(orderModel, billToCustomer);

		final String orderType = wileyPurchaseOrderService.getOrderType(exportProcess);
		purchaseOrder.setCurrencyCode(orderModel.getCurrency().getIsocode());

		purchaseOrder.setOrderType(orderType);
		purchaseOrder.setShippingChargeAmount(orderModel.getDeliveryCost());
		purchaseOrder.setBillToCustomer(billToCustomer);
		purchaseOrder.setShipToCustomer(shipToCustomer);
		purchaseOrder.setEmailAddress(orderModel.getUser().getUid());
		purchaseOrder.setTransactionID(wileyPurchaseOrderService.buildTransactionId(exportProcess));
		purchaseOrder.setHybrisAuthorizationCode(getHybrisAuthorizationCode(orderModel));

		purchaseOrder.setItemsStructures(itemsStructures);

		populatePaymentInfo(purchaseOrder, orderModel);
		purchaseOrder.setMarketOutlet(StringUtils.EMPTY);
		purchaseOrder.setSalespersonCode(StringUtils.EMPTY);
		purchaseOrder.setPromotionCode(StringUtils.EMPTY);
		purchaseOrder.setShipMethod(wileyPurchaseOrderService.buildShipMethod(orderModel));
	}

	private String getHybrisAuthorizationCode(final OrderModel orderModel)
	{
		if (orderModel.getPaymentInfo() != null && orderModel.getPaymentInfo() instanceof PaypalPaymentInfoModel) {
			return PP_HYBRIS_AUTH_CODE_VALUE;
		}
		return wileyPurchaseOrderService.buildHybrisAuthorizationCode(orderModel);
	}

	private void populatePaymentInfo(final PurchaseOrder purchaseOrder, final OrderModel orderModel)
	{
		if (orderModel.getPaymentInfo() instanceof CreditCardPaymentInfoModel) {
			CreditCardPaymentInfoModel paymentInfo = (CreditCardPaymentInfoModel) orderModel.getPaymentInfo();
			purchaseOrder.setCreditCardExpiry(wileyPurchaseOrderService.buildCreditCardExpiry(paymentInfo));
			purchaseOrder.setCreditCardType(wileyPurchaseOrderService.getCreditCardType(paymentInfo));
			purchaseOrder.setCreditCardToken(wileyPurchaseOrderService.getCCToken(orderModel));
		}
		if (orderModel.getPaymentInfo() instanceof PaypalPaymentInfoModel) {
			purchaseOrder.setCreditCardToken(wileyPurchaseOrderService.getCCToken(orderModel));
			purchaseOrder.setCreditCardType(EDIConstants.CARD_TYPE_PAYPAL);
		}
		else {
			LOG.warn("Cant convert payment info data, payment info doesn't contain CreditCard info");
		}
	}




}
