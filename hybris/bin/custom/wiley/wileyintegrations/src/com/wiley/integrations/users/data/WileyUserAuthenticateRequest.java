package com.wiley.integrations.users.data;

public class WileyUserAuthenticateRequest implements java.io.Serializable
{

	private UserCredentials userCredentials;

	public UserCredentials getUserCredentials()
	{
		return userCredentials;
	}

	public void setUserCredentials(final UserCredentials userCredentials)
	{
		this.userCredentials = userCredentials;
	}

}
