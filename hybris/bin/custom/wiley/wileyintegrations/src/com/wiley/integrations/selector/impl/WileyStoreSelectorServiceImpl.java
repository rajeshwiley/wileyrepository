package com.wiley.integrations.selector.impl;

import de.hybris.platform.store.BaseStoreModel;

import java.util.List;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.messaging.Message;

import com.wiley.integrations.selector.WileyStoreSelectorService;
import com.wiley.integrations.selector.strategies.WileyStoreSelectorStrategy;


/**
 * Created by Uladzimir_Barouski on 12/5/2016.
 */
public class WileyStoreSelectorServiceImpl implements WileyStoreSelectorService
{
	private static final Logger LOG = Logger.getLogger(WileyStoreSelectorServiceImpl.class);



	private List<WileyStoreSelectorStrategy> storeStrategyList;

	@Override
	public Optional<BaseStoreModel> getCurrentStore(final Message<?> message)
	{
		for (WileyStoreSelectorStrategy strategy : storeStrategyList)
		{
			Optional<BaseStoreModel> currentBaseStore = strategy.getCurrentBaseStore(message);
			if (currentBaseStore.isPresent())
			{
				return currentBaseStore;
			}
		}
		return Optional.empty();
	}


	@Required
	public void setStoreStrategyList(final List<WileyStoreSelectorStrategy> storeStrategyList)
	{
		this.storeStrategyList = storeStrategyList;
	}
}
