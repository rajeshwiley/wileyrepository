package com.wiley.integrations.esb.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.common.base.MoreObjects;


/**
 * Dto for cart calculation and validation in ERP.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderRequestDto
{
	private List<OrderEntryRequestDto> orderEntries;

	@XmlElement(required = true)
	private String customerId;

	@XmlElement(required = true)
	private String currency;

	@XmlElement(required = true)
	private String deliveryMode;

	@XmlElement(required = true)
	private String paymentMode;

	public List<OrderEntryRequestDto> getOrderEntries()
	{
		return orderEntries;
	}

	public void setOrderEntries(final List<OrderEntryRequestDto> orderEntries)
	{
		this.orderEntries = orderEntries;
	}

	public String getCustomerId()
	{
		return customerId;
	}

	public void setCustomerId(final String customerId)
	{
		this.customerId = customerId;
	}

	public String getCurrency()
	{
		return currency;
	}

	public void setCurrency(final String currency)
	{
		this.currency = currency;
	}

	public String getDeliveryMode()
	{
		return deliveryMode;
	}

	public void setDeliveryMode(final String deliveryMode)
	{
		this.deliveryMode = deliveryMode;
	}

	public String getPaymentMode()
	{
		return paymentMode;
	}

	public void setPaymentMode(final String paymentMode)
	{
		this.paymentMode = paymentMode;
	}

	@Override
	public String toString()
	{
		return MoreObjects.toStringHelper(this)
				.add("orderEntries", orderEntries)
				.add("customerId", customerId)
				.add("currency", currency)
				.add("deliveryMode", deliveryMode)
				.add("paymentMode", paymentMode)
				.toString();
	}
}
