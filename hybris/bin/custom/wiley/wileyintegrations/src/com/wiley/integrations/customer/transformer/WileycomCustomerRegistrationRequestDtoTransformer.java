package com.wiley.integrations.customer.transformer;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.CustomerModel;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.springframework.messaging.Message;
import org.springframework.util.Assert;

import com.wiley.integrations.customer.dto.WileycomCustomerRegistrationRequestDto;
import com.wiley.core.integration.customer.gateway.WileyCustomerGateway;


public class WileycomCustomerRegistrationRequestDtoTransformer
{
	@Resource
	Populator<CustomerModel, WileycomCustomerRegistrationRequestDto> wileyCustomerRegistrationRequestDtoPopulator;

	public WileycomCustomerRegistrationRequestDto transform(@Nonnull final Message message)
	{
		Assert.notNull(message);

		CustomerModel customerModel = (CustomerModel) message.getPayload();
		String password = (String) message.getHeaders().get(WileyCustomerGateway.PASSWORD);

		WileycomCustomerRegistrationRequestDto requestDto = new WileycomCustomerRegistrationRequestDto();
		wileyCustomerRegistrationRequestDtoPopulator.populate(customerModel, requestDto);
		requestDto.setPassword(password);

		return requestDto;
	}
}
