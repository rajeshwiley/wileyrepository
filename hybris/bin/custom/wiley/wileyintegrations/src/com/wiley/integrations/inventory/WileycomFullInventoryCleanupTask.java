package com.wiley.integrations.inventory;

import de.hybris.platform.acceleratorservices.dataimport.batch.BatchHeader;
import de.hybris.platform.acceleratorservices.dataimport.batch.task.CleanupTask;

import com.wiley.core.wileycom.stock.dao.WileycomStockLevelDao;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.util.Assert;


/**
 *
 * Created by Georgii_Gavrysh on 7/27/2016.
 */
public class WileycomFullInventoryCleanupTask extends CleanupTask
{

	private static final Logger LOG = Logger.getLogger(WileycomFullInventoryCleanupTask.class);

	private WileycomStockLevelDao wileycomStockLevelDao;

	/**
	 * remove all stock level records with sequenceId less sequenceId parameter for given warehouse
	 * @param header
	 * @return
	 */
	@Override
	public BatchHeader execute(final BatchHeader header)
	{
		removeObsoleteStocks((InventoryBatchHeader) header);
		return super.execute(header);
	}

	private void removeObsoleteStocks(final InventoryBatchHeader header)
	{
		final String warehouseCode = header.getWarehouseCode();
		final Long sequenceId = header.getSequenceId();
		Assert.isTrue(StringUtils.isNotBlank(warehouseCode));
		Assert.notNull(sequenceId);
		getWileycomStockLevelDao().removeObsoleteStockLevels(warehouseCode, sequenceId);
	}

	public WileycomStockLevelDao getWileycomStockLevelDao()
	{
		return wileycomStockLevelDao;
	}

	public void setWileycomStockLevelDao(final WileycomStockLevelDao wileycomStockLevelDao)
	{
		this.wileycomStockLevelDao = wileycomStockLevelDao;
	}
}
