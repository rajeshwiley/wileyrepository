package com.wiley.integrations.esb.dto.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;


/**
 * An order entry - an order line for a single product.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public abstract class AbstractOrderEntryRequest
{

	/**
	 * A unique alphanumeric value that identifies the product item across all the systems.
	 */
	private String sapProductCode;

	/**
	 * ISBN-13. See: https://en.wikipedia.org/wiki/International_Standard_Book_Number
	 */
	private String isbn;

	/**
	 * Quantity of the product in the order entry. Product pieces are considered as default measurement units.
	 */
	private Integer quantity;

	/**
	 * PO number that was specified for this order entry.
	 */
	private String purchaseOrderNumber;

	@JsonProperty("sapProductCode")
	public String getSapProductCode()
	{
		return sapProductCode;
	}

	public void setSapProductCode(final String sapProductCode)
	{
		this.sapProductCode = sapProductCode;
	}

	@JsonProperty("isbn")
	public String getIsbn()
	{
		return isbn;
	}

	public void setIsbn(final String isbn)
	{
		this.isbn = isbn;
	}

	@JsonProperty("quantity")
	public Integer getQuantity()
	{
		return quantity;
	}

	public void setQuantity(final Integer quantity)
	{
		this.quantity = quantity;
	}

	@JsonProperty("purchaseOrderNumber")
	public String getPurchaseOrderNumber()
	{
		return purchaseOrderNumber;
	}

	public void setPurchaseOrderNumber(final String purchaseOrderNumber)
	{
		this.purchaseOrderNumber = purchaseOrderNumber;
	}

	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this);
	}
}
