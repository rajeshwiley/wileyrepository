package com.wiley.integrations.failedordersexport.integration;

import com.wiley.core.event.models.WileyFailedOrderProcessesEvent;
import com.wiley.core.model.WileyOrderProcessModel;
import de.hybris.platform.servicelayer.event.EventService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Header;

import java.time.LocalDateTime;
import java.util.Collection;

public class WileyFailedOrdersExportMailTrigger {

    private static final Logger LOG = LoggerFactory.getLogger(WileyFailedOrdersExportMailTrigger.class);

    private final EventService eventService;

    @Autowired
    public WileyFailedOrdersExportMailTrigger(final EventService eventService) {
        this.eventService = eventService;
    }

    public void triggerSendEmailEvent(final Collection<WileyOrderProcessModel> failedProcesses,
                                      @Header(value = "dateFrom", required = false) final LocalDateTime from,
                                      @Header(value = "dateTo", required = false) final LocalDateTime to) {
        WileyFailedOrderProcessesEvent failedOrderProcessesEvent =
                new WileyFailedOrderProcessesEvent(failedProcesses, from, to);
        LOG.info("Triggering event {}", WileyFailedOrderProcessesEvent.class.getName());
        eventService.publishEvent(failedOrderProcessesEvent);
    }
}
