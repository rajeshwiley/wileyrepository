package com.wiley.integrations.wpg.transformer;

import de.hybris.platform.payment.dto.BillingInfo;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.wiley.core.payment.WileyHttpRequestParams;
import com.wiley.core.payment.request.WileySubscriptionAuthorizeRequest;
import com.wiley.core.payment.strategies.SecurityHashGeneratorStrategy;
import com.wiley.core.payment.strategies.WPGRegionStrategy;
import com.wiley.core.payment.strategies.WPGVendorIdStrategy;
import com.wiley.core.payment.strategies.WileyTransactionIdGeneratorStrategy;


public class AuthorizePaymentRequestTransformer
{
	private static final String METHOD_HTTP = "HTTP";

	@Value("${payment.wpg.authorize.operation}")
	private String operation;
	@Value("${payment.wpg.authorize.description}")
	private String description;
	@Value("${payment.wpg.http.auth.allowAvsFail}")
	private String allowAvsFail;
	@Autowired
	private WileyTransactionIdGeneratorStrategy transactionIdGeneratorStrategy;
	@Autowired
	private WPGRegionStrategy wpgRegionStrategy;
	@Autowired
	private ConfigurationService configurationService;
	@Autowired
	private SecurityHashGeneratorStrategy wpgHttpSecurityHashGeneratorStrategy;
	@Resource(name = "wpgHttpVendorIdStrategy")
	private WPGVendorIdStrategy vendorIdStrategy;

	public Map<String, String> transform(final WileySubscriptionAuthorizeRequest request)
	{

		final String region = wpgRegionStrategy.getRegionByCurrency(request.getCurrency().getCurrencyCode());
		final String siteId = request.getSite();
		final String vendorId = vendorIdStrategy.getVendorId(siteId).toString();

		Map<String, String> requestParamsMap = new HashMap<>();
		requestParamsMap.put(WileyHttpRequestParams.WPG_OPERATION, operation);
		requestParamsMap.put(WileyHttpRequestParams.WPG_TIMESTAMP, getTimestamp());

		requestParamsMap.put(WileyHttpRequestParams.WPG_VENDOR_ID, vendorId);
		requestParamsMap.put(WileyHttpRequestParams.WPG_TRANSACTION_ID,
				transactionIdGeneratorStrategy.generateTransactionId());
		requestParamsMap.put(WileyHttpRequestParams.WPG_METHOD, METHOD_HTTP);
		requestParamsMap.put(WileyHttpRequestParams.WPG_DESCRIPTION, description);
		requestParamsMap.put(WileyHttpRequestParams.WPG_VALUE, request.getTotalAmount().toString());
		requestParamsMap.put(WileyHttpRequestParams.WPG_CURRENCY, request.getCurrency().getCurrencyCode());
		requestParamsMap.put(WileyHttpRequestParams.WPG_REGION, region);
		requestParamsMap.put(WileyHttpRequestParams.WPG_TOKEN, request.getSubscriptionID());
		requestParamsMap.put(WileyHttpRequestParams.WPG_ALLOW_AVS_FAIL, allowAvsFail);

		BillingInfo address = request.getShippingInfo();
		requestParamsMap.put(WileyHttpRequestParams.WPG_ADDRESS, address.getStreet1());
		requestParamsMap.put(WileyHttpRequestParams.WPG_POSTCODE, address.getPostalCode());

		requestParamsMap.put(WileyHttpRequestParams.WPG_COUNTRY_CODE, address.getCountry());

		requestParamsMap.put(WileyHttpRequestParams.WPG_SECURITY,
				calculateTokenSettleSecurity(requestParamsMap, siteId));

		return requestParamsMap;
	}

	private String calculateTokenSettleSecurity(final Map<String, String> request, final String siteId)
	{
		StringBuilder builder = new StringBuilder();
		builder.append(request.get(WileyHttpRequestParams.WPG_TIMESTAMP));
		builder.append(request.get(WileyHttpRequestParams.WPG_VENDOR_ID));
		builder.append(request.get(WileyHttpRequestParams.WPG_TRANSACTION_ID));
		builder.append(request.get(WileyHttpRequestParams.WPG_METHOD));
		builder.append(request.get(WileyHttpRequestParams.WPG_DESCRIPTION));
		builder.append(request.get(WileyHttpRequestParams.WPG_VALUE));
		builder.append(request.get(WileyHttpRequestParams.WPG_CURRENCY));
		builder.append(request.get(WileyHttpRequestParams.WPG_REGION));
		builder.append(request.get(WileyHttpRequestParams.WPG_TOKEN));
		builder.append(request.get(WileyHttpRequestParams.WPG_ADDRESS));
		builder.append(request.get(WileyHttpRequestParams.WPG_POSTCODE));
		builder.append(request.get(WileyHttpRequestParams.WPG_COUNTRY_CODE));

		return wpgHttpSecurityHashGeneratorStrategy.generateSecurityHash(siteId, builder.toString());
	}

	public void setOperation(final String operation)
	{
		this.operation = operation;
	}

	public void setDescription(final String description)
	{
		this.description = description;
	}

	public String getTimestamp()
	{
		return String.valueOf(new Date().getTime());
	}
}
