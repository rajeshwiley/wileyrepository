
package com.wiley.integrations.vies.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "countryCode",
    "vatNumber"
})
@XmlRootElement(namespace = "urn:ec.europa.eu:taxud:vies:services:checkVat:types", name = "checkVat")
public class CheckVat {

    @XmlElement(namespace = "urn:ec.europa.eu:taxud:vies:services:checkVat:types", required = true)
    protected String countryCode;
    @XmlElement(namespace = "urn:ec.europa.eu:taxud:vies:services:checkVat:types", required = true)
    protected String vatNumber;

    /**
     * Gets the value of the countryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * Sets the value of the countryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryCode(final String value) {
        this.countryCode = value;
    }

    /**
     * Gets the value of the vatNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVatNumber() {
        return vatNumber;
    }

    /**
     * Sets the value of the vatNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVatNumber(final String value) {
        this.vatNumber = value;
    }

}
