package com.wiley.integrations.order.populator;

import com.wiley.integrations.order.dto.PriceValueWsDTO;
import de.hybris.platform.converters.impl.AbstractPopulatingConverter;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.util.PriceValue;
import org.apache.commons.lang.NotImplementedException;

/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public class WileyasPriceValueReverseWsConverter extends AbstractPopulatingConverter<PriceValueWsDTO, PriceValue>
{
    @Override
    public PriceValue convert(final PriceValueWsDTO source) throws ConversionException
    {
        return new PriceValue(source.getCurrency(), source.getValue(), true);
    }

    @Override
    protected PriceValue createTarget()
    {
        throw new NotImplementedException();
    }
}
