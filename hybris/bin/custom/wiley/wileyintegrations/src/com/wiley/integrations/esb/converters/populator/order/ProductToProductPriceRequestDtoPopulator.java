package com.wiley.integrations.esb.converters.populator.order;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.springframework.util.Assert;

import com.wiley.integrations.esb.dto.EsbProductPriceRequestDto;


public class ProductToProductPriceRequestDtoPopulator implements Populator<ProductModel, EsbProductPriceRequestDto>
{
	@Override
	public void populate(final ProductModel productModel, final EsbProductPriceRequestDto productPriceRequestDto)
			throws ConversionException
	{
		Assert.notNull(productModel);
		Assert.notNull(productPriceRequestDto);

		productPriceRequestDto.setProductCode(productModel.getCode());
	}
}