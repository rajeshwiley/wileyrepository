package com.wiley.integrations.sabrix.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SystemNameType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="SystemNameType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SystemID" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1}StringType"/&gt;
 *         &lt;element name="SystemName" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="IPAddress" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1}IPAddress" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemNameType", namespace = "http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1", propOrder = {
		"systemID",
		"systemName",
		"ipAddress"
})
public class SystemNameType
{

	@XmlElement(name = "SystemID", required = true)
	protected String systemID;
	@XmlElement(name = "SystemName")
	protected String systemName;
	@XmlElement(name = "IPAddress")
	protected String ipAddress;

	/**
	 * Gets the value of the systemID property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getSystemID()
	{
		return systemID;
	}

	/**
	 * Sets the value of the systemID property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setSystemID(String value)
	{
		this.systemID = value;
	}

	/**
	 * Gets the value of the systemName property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getSystemName()
	{
		return systemName;
	}

	/**
	 * Sets the value of the systemName property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setSystemName(String value)
	{
		this.systemName = value;
	}

	/**
	 * Gets the value of the ipAddress property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getIPAddress()
	{
		return ipAddress;
	}

	/**
	 * Sets the value of the ipAddress property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setIPAddress(String value)
	{
		this.ipAddress = value;
	}

}
