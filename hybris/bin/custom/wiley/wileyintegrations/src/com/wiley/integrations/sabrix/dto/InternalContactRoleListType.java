package com.wiley.integrations.sabrix.dto;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InternalContactRoleListType.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="InternalContactRoleListType"&gt;
 *   &lt;restriction base="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1}StringType"&gt;
 *     &lt;enumeration value="Editor"/&gt;
 *     &lt;enumeration value="MarketingManager"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 */
@XmlType(name = "InternalContactRoleListType", namespace = "http://xmlns.wiley.com/EnterpriseBusinessObjects/CommonInfratypes/Codelist/V1")
@XmlEnum
public enum InternalContactRoleListType
{

	@XmlEnumValue("Editor")
	EDITOR("Editor"),
	@XmlEnumValue("MarketingManager")
	MARKETING_MANAGER("MarketingManager");
	private final String value;

	InternalContactRoleListType(String v)
	{
		value = v;
	}

	public String value()
	{
		return value;
	}

	public static InternalContactRoleListType fromValue(String v)
	{
		for (InternalContactRoleListType c : InternalContactRoleListType.values())
		{
			if (c.value.equals(v))
			{
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}

}
