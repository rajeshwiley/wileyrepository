package com.wiley.integrations.inventory;

import de.hybris.platform.servicelayer.exceptions.SystemException;


public class WileycomInventoryBadSequenceException extends SystemException
{
	public WileycomInventoryBadSequenceException(final String message)
	{
		super(message);
	}
}
