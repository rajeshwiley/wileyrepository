package com.wiley.integrations.order.populator;

import de.hybris.platform.converters.impl.AbstractPopulatingConverter;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.util.TaxValue;

import java.util.UUID;

import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang3.StringUtils;

import com.wiley.integrations.order.dto.TaxValueWsDTO;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public class WileyasTaxValueReverseWsConverter extends AbstractPopulatingConverter<TaxValueWsDTO, TaxValue>
{
	@Override
	public TaxValue convert(final TaxValueWsDTO source) throws ConversionException
	{
		String code = source.getCode();
		if (StringUtils.isBlank(code))
		{
			code = UUID.randomUUID().toString();
		}
		return new TaxValue(code, source.getValue(), true, source.getCurrency());
	}

	@Override
	protected TaxValue createTarget()
	{
		throw new NotImplementedException();
	}
}
