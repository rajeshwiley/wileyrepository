package com.wiley.integrations.sabrix.dto;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ActionType.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ActionType"&gt;
 *   &lt;restriction base="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1}StringType"&gt;
 *     &lt;enumeration value="CREATE"/&gt;
 *     &lt;enumeration value="UPDATE"/&gt;
 *     &lt;enumeration value="CREATEUPDATE"/&gt;
 *     &lt;enumeration value="DELETE"/&gt;
 *     &lt;enumeration value="SEARCH"/&gt;
 *     &lt;enumeration value="GET"/&gt;
 *     &lt;enumeration value="MERGE"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 */
@XmlType(name = "ActionType", namespace = "http://xmlns.wiley.com/EnterpriseBusinessObjects/CommonInfratypes/Codelist/V1")
@XmlEnum
public enum ActionType
{

	CREATE,
	UPDATE,
	CREATEUPDATE,
	DELETE,
	SEARCH,
	GET,
	MERGE;

	public String value()
	{
		return name();
	}

	public static ActionType fromValue(String v)
	{
		return valueOf(v);
	}

}
