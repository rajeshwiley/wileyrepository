package com.wiley.integrations.esb.converters.populator.deliveryoption;

import de.hybris.platform.commerceservices.util.GuidKeyGenerator;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.i18n.I18NService;

import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.wiley.core.integration.esb.dto.DeliveryOptionResponseDto;
import com.wiley.core.model.ExternalDeliveryModeModel;


/**
 * Populator converts DeliveryOptionResponseDto to ExternalDeliveryModeModel
 */
public class DeliveryOptionToExternalDeliveryModePopulator
		implements Populator<DeliveryOptionResponseDto, ExternalDeliveryModeModel>
{
	@Resource
	private GuidKeyGenerator guidKeyGenerator;

	@Resource
	private I18NService i18NService;

	/**
	 * Populate the target instance with values from the source instance.
	 *
	 * @param deliveryOptionResponseDto
	 * 		the source object
	 * @param externalDeliveryModeModel
	 * 		the target to fill
	 * @throws ConversionException
	 * 		if an error occurs
	 */
	@Override
	public void populate(final DeliveryOptionResponseDto deliveryOptionResponseDto,
			final ExternalDeliveryModeModel externalDeliveryModeModel)
			throws ConversionException
	{
		Assert.notNull(deliveryOptionResponseDto);
		Assert.notNull(externalDeliveryModeModel);

		externalDeliveryModeModel.setExternalCode(deliveryOptionResponseDto.getCode());

		externalDeliveryModeModel.setCostValue(deliveryOptionResponseDto.getCost());
	}
}
