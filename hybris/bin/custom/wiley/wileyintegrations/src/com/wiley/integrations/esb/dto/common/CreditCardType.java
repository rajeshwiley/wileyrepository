package com.wiley.integrations.esb.dto.common;

/**
 * Credit card type
 */
public enum CreditCardType
{
	VISA,
	MASTER,
	AMEX,
	DISCOVER,
	DINERS
}
