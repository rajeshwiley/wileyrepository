package com.wiley.integrations.common.transformer;

import javax.annotation.Nonnull;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;


/**
 * Created by Sergiy_Mishkovets on 7/13/2016.
 */
public class NoContentResponseTransformer
{
	@Nonnull
	public Boolean transform(@Nonnull final ResponseEntity<String> responseEntity)
	{
		return HttpStatus.NO_CONTENT.equals(responseEntity.getStatusCode());
	}

}
