package com.wiley.integrations.sabrix.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for QuantityType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="QuantityType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DEFAULT" type="{http://www.sabrix.com/services/taxcalculationservice/2011-09-01}BooleanType" minOccurs="0"/&gt;
 *         &lt;element name="AMOUNT" type="{http://www.sabrix.com/services/taxcalculationservice/2011-09-01}NillableDecimalType" minOccurs="0"/&gt;
 *         &lt;element name="UOM" type="{http://www.sabrix.com/services/taxcalculationservice/2011-09-01}UOM" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QuantityType", propOrder = {
		"_default",
		"amount",
		"uom"
})
public class QuantityType
{

	@XmlElement(name = "DEFAULT")
	@XmlSchemaType(name = "anySimpleType")
	protected String _default;
	@XmlElement(name = "AMOUNT")
	@XmlSchemaType(name = "anySimpleType")
	protected String amount;
	@XmlElement(name = "UOM")
	protected String uom;

	/**
	 * Gets the value of the default property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getDEFAULT()
	{
		return _default;
	}

	/**
	 * Sets the value of the default property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setDEFAULT(String value)
	{
		this._default = value;
	}

	/**
	 * Gets the value of the amount property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getAMOUNT()
	{
		return amount;
	}

	/**
	 * Sets the value of the amount property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setAMOUNT(String value)
	{
		this.amount = value;
	}

	/**
	 * Gets the value of the uom property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getUOM()
	{
		return uom;
	}

	/**
	 * Sets the value of the uom property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setUOM(String value)
	{
		this.uom = value;
	}

}
