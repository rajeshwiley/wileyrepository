package com.wiley.integrations.ediintegration.endpoints.transformers;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.store.BaseStoreModel;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import com.wiley.fulfilmentprocess.model.WileyExportProcessModel;
import com.wiley.core.integration.edi.Headers;
import com.wiley.integrations.ediintegration.endpoints.jaxb.PurchaseOrder;
import com.wiley.integrations.ediintegration.endpoints.jaxb.PurchaseOrderToWILEY;
import com.wiley.integrations.ediintegration.endpoints.jaxb.Routing;
import com.wiley.integrations.ediintegration.endpoints.jaxb.builder.PurchaseOrderToWILEYBuilder;
import com.wiley.integrations.ediintegration.endpoints.transformers.converters.PurchaseOrderPopulator;

import static com.wiley.integrations.constants.WileyintegrationsConstants.BASE_STORE_KEY;
import static com.wiley.integrations.constants.WileyintegrationsConstants.LOCAL_DATE_TIME;
import static com.wiley.integrations.constants.WileyintegrationsConstants.PAYMENT_TYPE_KEY;



@Component("orderToJaxbBeanTransformer")
public class OrderToJaxbBeanTransformer
{
	private static final Logger LOG = Logger.getLogger(OrderToJaxbBeanTransformer.class);


	@Resource(name = "ediTransactionIdGenerator")
	private KeyGenerator keyGenerator;
	@Autowired
	private PurchaseOrderPopulator purchaseOrderPopulator;

	public Message<PurchaseOrderToWILEY> toJaxbBeansList(final Message<List<WileyExportProcessModel>> message)
	{
		BaseStoreModel baseStore = (BaseStoreModel) message.getHeaders().get(BASE_STORE_KEY);
		LocalDateTime localDateTime = (LocalDateTime) message.getHeaders().get(LOCAL_DATE_TIME);
		final String paymentType = String.valueOf(message.getHeaders().get(PAYMENT_TYPE_KEY));
		Routing routing = getRouting(Optional.of(baseStore), localDateTime, paymentType);
		List<WileyExportProcessModel> exportProcesses = new ArrayList<>(message.<WileyExportProcessModel> getPayload());
		List<PurchaseOrder> purchaseOrders = convertOrders(exportProcesses);
		return MessageBuilder.withPayload(PurchaseOrderToWILEYBuilder.createInstance()
				.withRouting(routing)
				.withPurchaseOrderList(purchaseOrders)
				.build()).copyHeaders(message.getHeaders())
				.setHeader(Headers.CONVERTED_PROCESSES, exportProcesses)
				.build();

	}

	List<PurchaseOrder> convertOrders(final List<WileyExportProcessModel> exportProcesses)
	{
		List<WileyExportProcessModel> errorProcesses = new ArrayList<>();
		List<PurchaseOrder> convertedOrders = exportProcesses.stream().map(exportProcess -> {
			OrderModel order = exportProcess.getOrder();
			if (order == null)
			{
				LOG.error("Export process: " + exportProcess.getCode() + " has no order attached");
				return null;
			}
			try
			{
				PurchaseOrder purchaseOrder = new PurchaseOrder();
				purchaseOrderPopulator.populate(exportProcess, purchaseOrder);
				return purchaseOrder;
			}
			catch (Exception e)
			{
				LOG.error("Order with code: " + order.getCode() + " failed to convert to PurchaseOrder JAXB bean", e);
				errorProcesses.add(exportProcess);
				return null;
			}
		}).filter(Objects::nonNull).collect(Collectors.toList());
		exportProcesses.removeAll(errorProcesses);
		return convertedOrders;
	}

	Routing getRouting(final Optional<BaseStoreModel> baseStoreModel, final LocalDateTime localDateTime,
			final String paymentType)
	{
		return new Routing(localDateTime,
				baseStoreModel.orElseThrow(() -> new IllegalArgumentException("No BaseStore provided")),
				keyGenerator.generate().toString(),
				paymentType);
	}
}
