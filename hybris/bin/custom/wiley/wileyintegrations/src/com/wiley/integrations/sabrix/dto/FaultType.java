package com.wiley.integrations.sabrix.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FaultType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="FaultType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/Meta/V1}EBMHeader" minOccurs="0"/&gt;
 *         &lt;element name="FaultMessage" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/Meta/V1}FaultMessageType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FaultType", namespace = "http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/Meta/V1", propOrder = {
		"ebmHeader",
		"faultMessage"
})
public class FaultType
{

	@XmlElement(name = "EBMHeader")
	protected EBMHeaderType ebmHeader;
	@XmlElement(name = "FaultMessage")
	protected FaultMessageType faultMessage;

	/**
	 * Gets the value of the ebmHeader property.
	 *
	 * @return possible object is
	 * {@link EBMHeaderType }
	 */
	public EBMHeaderType getEBMHeader()
	{
		return ebmHeader;
	}

	/**
	 * Sets the value of the ebmHeader property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link EBMHeaderType }
	 */
	public void setEBMHeader(EBMHeaderType value)
	{
		this.ebmHeader = value;
	}

	/**
	 * Gets the value of the faultMessage property.
	 *
	 * @return possible object is
	 * {@link FaultMessageType }
	 */
	public FaultMessageType getFaultMessage()
	{
		return faultMessage;
	}

	/**
	 * Sets the value of the faultMessage property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link FaultMessageType }
	 */
	public void setFaultMessage(FaultMessageType value)
	{
		this.faultMessage = value;
	}

}
