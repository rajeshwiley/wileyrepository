package com.wiley.integrations.sabrix.transformer.populators.line;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.math.BigDecimal;

import javax.annotation.Nonnull;

import com.wiley.integrations.sabrix.dto.IndataLineType;


/**
 * @author Dzmitryi_Halahayeu
 */
public class IdToLinePopulator implements Populator<Integer, IndataLineType>
{
	@Override
	public void populate(@Nonnull final Integer id, @Nonnull final IndataLineType indataLineType) throws ConversionException
	{
		indataLineType.setID(String.valueOf(id));
		indataLineType.setLINENUMBER(BigDecimal.valueOf(id));
	}
}
