package com.wiley.integrations.handler;

import com.wiley.core.exceptions.ExternalSystemValidationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.integration.handler.advice.AbstractRequestHandlerAdvice;
import org.springframework.messaging.Message;
import org.springframework.util.Assert;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;


/**
 * Advice for supporting payload validation on spring integration.
 */
public class WileycomDtoValidatorRequestHandlerAdvice extends AbstractRequestHandlerAdvice
{

	private static final Logger LOG = LoggerFactory.getLogger(WileycomDtoValidatorRequestHandlerAdvice.class);

	private Validator incomingPayloadValidator;

	private Validator outgoingPayloadValidator;

	@Override
	protected Object doInvoke(final ExecutionCallback executionCallback, final Object o, final Message<?> incomingMessage)
			throws Exception
	{
		Assert.notNull(executionCallback);
		Assert.notNull(incomingMessage);
		Assert.notNull(incomingMessage.getPayload());

		validatePayload(incomingMessage, incomingPayloadValidator);

		Message<?> outgoingMessage = (Message<?>) executionCallback.execute();

		if (outgoingMessage != null)
		{
			Assert.notNull(outgoingMessage.getPayload());
			validatePayload(outgoingMessage, outgoingPayloadValidator);
		}

		return outgoingMessage;
	}

	private void validatePayload(final Message<?> message, final Validator payloadValidator)
	{
		Object payload = message.getPayload();
		Class<?> payloadClass = payload.getClass();

		if (!payloadValidator.supports(payloadClass))
		{
			throw new IllegalStateException(String.format("Validator [%s] does not support payload class [%s]", payloadValidator,
					payloadClass));
		}

		BindingResult bindingResult = new BeanPropertyBindingResult(payload, payloadClass.getSimpleName());
		payloadValidator.validate(payload, bindingResult);

		logValidationResultsForPayload(payload, bindingResult);

		if (bindingResult.hasErrors())
		{
			throw new ExternalSystemValidationException(bindingResult.toString());
		}
	}

	private void logValidationResultsForPayload(final Object payload, final BindingResult bindingResult)
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Validation results for payload [{}]: {}", payload, bindingResult);
		}
	}

	@Required
	public void setIncomingPayloadValidator(final Validator incomingPayloadValidator)
	{
		this.incomingPayloadValidator = incomingPayloadValidator;
	}

	@Required
	public void setOutgoingPayloadValidator(final Validator outgoingPayloadValidator)
	{
		this.outgoingPayloadValidator = outgoingPayloadValidator;
	}
}
