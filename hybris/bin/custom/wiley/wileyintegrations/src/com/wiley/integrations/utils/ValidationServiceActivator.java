package com.wiley.integrations.utils;

import de.hybris.platform.validation.exceptions.HybrisConstraintViolation;
import de.hybris.platform.validation.services.ValidationService;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.Resource;
import javax.validation.ConstraintViolationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;


public class ValidationServiceActivator
{
	private static final Logger LOG = LoggerFactory.getLogger(ValidationServiceActivator.class);

	@Resource
	private ValidationService validationService;

	public <T> Message<T> validateMessage(final Message<T> message) throws ConstraintViolationException
	{
		final Set<HybrisConstraintViolation> violations = validationService.validate(message.getPayload());
		if (violations.size() > 0)
		{
			if (LOG.isDebugEnabled())
			{
				violations.stream().forEach(violation -> LOG.debug(violation.toString()));
			}
			throw new ConstraintViolationException("Error occurred during message validation", new HashSet(violations));
		}
		return message;
	}

}
