package com.wiley.integrations.ediintegration.endpoints.generator;

import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.util.localization.Localization;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.integration.file.FileNameGenerator;
import org.springframework.messaging.Message;

import static com.wiley.integrations.constants.WileyintegrationsConstants.BASE_STORE_KEY;
import static com.wiley.integrations.constants.WileyintegrationsConstants.DOT;
import static com.wiley.integrations.constants.WileyintegrationsConstants.EXPORT_TYPE_KEY;
import static com.wiley.integrations.constants.WileyintegrationsConstants.LOCAL_DATE_TIME;
import static com.wiley.integrations.constants.WileyintegrationsConstants.UNDERSCORE;


public class WileyExcelFileNameGenerator implements FileNameGenerator
{
	private static final Logger LOG = Logger.getLogger(WileyExcelFileNameGenerator.class);
	private String extension;
	private String dateFormat;

	public String getExtension()
	{
		return extension;
	}

	public void setExtension(final String extension)
	{
		this.extension = extension;
	}

	public String getDateFormat()
	{
		return dateFormat;
	}

	public void setDateFormat(final String dateFormat)
	{
		this.dateFormat = dateFormat;
	}

	@Override
	public String generateFileName(final Message<?> message)
	{


		BaseStoreModel baseStore = (BaseStoreModel) message.getHeaders().get(BASE_STORE_KEY);
		LocalDateTime localDateTime = (LocalDateTime) message.getHeaders().get(LOCAL_DATE_TIME);
		String exportType = String.valueOf(message.getHeaders().get(EXPORT_TYPE_KEY));
		final String siteShortcut = baseStore != null ? baseStore.getUid() : StringUtils.EMPTY;

		final String excelFileName = buildExcelFileName(siteShortcut, exportType, localDateTime);
		LOG.debug(String.format("Completed file name generation %s", excelFileName));
		return excelFileName;
	}

	/**
	 * Generates file name.
	 * File name pattern: Hybris_Order_summary_for_<SITE>_MM-dd-yyyy-hh-mm-ss.xls
	 *
	 * @param siteShortcut
	 * @return
	 */
	private String buildExcelFileName(final String siteShortcut, final String exportType, final LocalDateTime localDateTime)
	{
		String dateFormatted = DateTimeFormatter.ofPattern(getDateFormat()).format(localDateTime);

		final String firstPartFileName = getFirstPartFileName();
		StringBuilder filename = new StringBuilder(firstPartFileName)
				.append(siteShortcut)
				.append(UNDERSCORE)
				.append(exportType)
				.append(UNDERSCORE)
				.append(dateFormatted)
				.append(DOT)
				.append(getExtension());
		return filename.toString();
	}

	public String getFirstPartFileName()
	{
		return Localization.getLocalizedString("wiley.orders.report.file.name");
	}



}
