package com.wiley.integrations.selector;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import org.springframework.messaging.Message;


/**
 *
 */
public interface WileyStoreSelectorService
{
	Optional<BaseStoreModel> getCurrentStore(Message<?> message);
}
