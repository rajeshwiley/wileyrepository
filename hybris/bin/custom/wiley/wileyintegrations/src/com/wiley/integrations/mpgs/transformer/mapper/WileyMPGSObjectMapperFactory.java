package com.wiley.integrations.mpgs.transformer.mapper;


import org.springframework.integration.support.json.Jackson2JsonObjectMapper;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;


public final class WileyMPGSObjectMapperFactory
{

	private WileyMPGSObjectMapperFactory()
	{

	}

	public static Jackson2JsonObjectMapper getMapper()
	{
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		return new Jackson2JsonObjectMapper(mapper);
	}
}
