package com.wiley.integrations.customer.transformer;

import javax.annotation.Nonnull;

import org.springframework.util.Assert;

import com.wiley.integrations.customer.dto.WileycomForcedPasswordResetRequestDto;


public class WileycomCustomerForceResetPasswordRequestDtoTransformer
{
	public WileycomForcedPasswordResetRequestDto transform(@Nonnull final String newPassword)
	{
		Assert.notNull(newPassword);

		WileycomForcedPasswordResetRequestDto requestDto = new WileycomForcedPasswordResetRequestDto();
		requestDto.setNewPassword(newPassword);

		return requestDto;
	}
}
