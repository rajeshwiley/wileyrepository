package com.wiley.integrations.sabrix.dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for EBMHeaderType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="EBMHeaderType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="EBMName" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1}NameType" minOccurs="0"/&gt;
 *         &lt;element name="DateTime" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1}DateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="ActionCode" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/CommonInfratypes/Codelist/V1}ActionType" minOccurs="0"/&gt;
 *         &lt;element name="EnvironmentCode" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/CommonInfratypes/Codelist/V1}EnvironmentCodeType" minOccurs="0"/&gt;
 *         &lt;element name="SenderSystem" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1}SystemNameType" minOccurs="0"/&gt;
 *         &lt;element name="ServiceContext" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/Meta/V1}ServiceContextType" minOccurs="0"/&gt;
 *         &lt;element name="TargetSystem" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/Meta/V1}TargetSystemType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ServiceInstance" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/Meta/V1}ServiceInstanceType" minOccurs="0"/&gt;
 *         &lt;element name="CorrelationID" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="UserID" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="Password" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1}StringType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EBMHeaderType", namespace = "http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/Meta/V1", propOrder = {
		"ebmName",
		"dateTime",
		"actionCode",
		"environmentCode",
		"senderSystem",
		"serviceContext",
		"targetSystem",
		"serviceInstance",
		"correlationID",
		"userID",
		"password"
})
@XmlSeeAlso({
		TaxCalculationRequestEBMType.class,
		TaxCalculationResponseEBMType.class
})
public class EBMHeaderType
{

	@XmlElement(name = "EBMName")
	protected NameType ebmName;
	@XmlElement(name = "DateTime")
	@XmlSchemaType(name = "dateTime")
	protected XMLGregorianCalendar dateTime;
	@XmlElement(name = "ActionCode")
	@XmlSchemaType(name = "string")
	protected ActionType actionCode;
	@XmlElement(name = "EnvironmentCode", defaultValue = "NA")
	@XmlSchemaType(name = "string")
	protected EnvironmentCodeType environmentCode;
	@XmlElement(name = "SenderSystem")
	protected SystemNameType senderSystem;
	@XmlElement(name = "ServiceContext")
	protected ServiceContextType serviceContext;
	@XmlElement(name = "TargetSystem")
	protected List<TargetSystemType> targetSystem;
	@XmlElement(name = "ServiceInstance")
	protected ServiceInstanceType serviceInstance;
	@XmlElement(name = "CorrelationID")
	protected String correlationID;
	@XmlElement(name = "UserID")
	protected String userID;
	@XmlElement(name = "Password")
	protected String password;

	/**
	 * Gets the value of the ebmName property.
	 *
	 * @return possible object is
	 * {@link NameType }
	 */
	public NameType getEBMName()
	{
		return ebmName;
	}

	/**
	 * Sets the value of the ebmName property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link NameType }
	 */
	public void setEBMName(NameType value)
	{
		this.ebmName = value;
	}

	/**
	 * Gets the value of the dateTime property.
	 *
	 * @return possible object is
	 * {@link XMLGregorianCalendar }
	 */
	public XMLGregorianCalendar getDateTime()
	{
		return dateTime;
	}

	/**
	 * Sets the value of the dateTime property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link XMLGregorianCalendar }
	 */
	public void setDateTime(XMLGregorianCalendar value)
	{
		this.dateTime = value;
	}

	/**
	 * Gets the value of the actionCode property.
	 *
	 * @return possible object is
	 * {@link ActionType }
	 */
	public ActionType getActionCode()
	{
		return actionCode;
	}

	/**
	 * Sets the value of the actionCode property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link ActionType }
	 */
	public void setActionCode(ActionType value)
	{
		this.actionCode = value;
	}

	/**
	 * Gets the value of the environmentCode property.
	 *
	 * @return possible object is
	 * {@link EnvironmentCodeType }
	 */
	public EnvironmentCodeType getEnvironmentCode()
	{
		return environmentCode;
	}

	/**
	 * Sets the value of the environmentCode property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link EnvironmentCodeType }
	 */
	public void setEnvironmentCode(EnvironmentCodeType value)
	{
		this.environmentCode = value;
	}

	/**
	 * Gets the value of the senderSystem property.
	 *
	 * @return possible object is
	 * {@link SystemNameType }
	 */
	public SystemNameType getSenderSystem()
	{
		return senderSystem;
	}

	/**
	 * Sets the value of the senderSystem property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link SystemNameType }
	 */
	public void setSenderSystem(SystemNameType value)
	{
		this.senderSystem = value;
	}

	/**
	 * Gets the value of the serviceContext property.
	 *
	 * @return possible object is
	 * {@link ServiceContextType }
	 */
	public ServiceContextType getServiceContext()
	{
		return serviceContext;
	}

	/**
	 * Sets the value of the serviceContext property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link ServiceContextType }
	 */
	public void setServiceContext(ServiceContextType value)
	{
		this.serviceContext = value;
	}

	/**
	 * Gets the value of the targetSystem property.
	 *
	 * <p>
	 * This accessor method returns a reference to the live list,
	 * not a snapshot. Therefore any modification you make to the
	 * returned list will be present inside the JAXB object.
	 * This is why there is not a <CODE>set</CODE> method for the targetSystem property.
	 *
	 * <p>
	 * For example, to add a new item, do as follows:
	 * <pre>
	 *    getTargetSystem().add(newItem);
	 * </pre>
	 *
	 *
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link TargetSystemType }
	 */
	public List<TargetSystemType> getTargetSystem()
	{
		if (targetSystem == null)
		{
			targetSystem = new ArrayList<TargetSystemType>();
		}
		return this.targetSystem;
	}

	/**
	 * Gets the value of the serviceInstance property.
	 *
	 * @return possible object is
	 * {@link ServiceInstanceType }
	 */
	public ServiceInstanceType getServiceInstance()
	{
		return serviceInstance;
	}

	/**
	 * Sets the value of the serviceInstance property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link ServiceInstanceType }
	 */
	public void setServiceInstance(ServiceInstanceType value)
	{
		this.serviceInstance = value;
	}

	/**
	 * Gets the value of the correlationID property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getCorrelationID()
	{
		return correlationID;
	}

	/**
	 * Sets the value of the correlationID property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setCorrelationID(String value)
	{
		this.correlationID = value;
	}

	/**
	 * Gets the value of the userID property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getUserID()
	{
		return userID;
	}

	/**
	 * Sets the value of the userID property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setUserID(String value)
	{
		this.userID = value;
	}

	/**
	 * Gets the value of the password property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getPassword()
	{
		return password;
	}

	/**
	 * Sets the value of the password property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setPassword(String value)
	{
		this.password = value;
	}

}
