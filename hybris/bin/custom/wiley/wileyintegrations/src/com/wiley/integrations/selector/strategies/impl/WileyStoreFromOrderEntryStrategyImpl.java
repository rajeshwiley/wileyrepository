package com.wiley.integrations.selector.strategies.impl;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import javax.annotation.Nonnull;

import org.springframework.messaging.Message;


/**
 * Created by Uladzimir_Barouski on 12/19/2016.
 */
public class WileyStoreFromOrderEntryStrategyImpl extends WileyAbstractStoreSelectorStrategy
{
	@Nonnull
	@Override
	public Optional<BaseStoreModel> getCurrentBaseStore(@Nonnull final Message<?> message)
	{
		BaseSiteModel currentBaseSite = null;
		if (message.getPayload() instanceof OrderEntryModel) {
			currentBaseSite = ((OrderEntryModel) message.getPayload()).getOrder().getSite();
		}
		return findBaseStoreForSite(currentBaseSite);
	}
}
