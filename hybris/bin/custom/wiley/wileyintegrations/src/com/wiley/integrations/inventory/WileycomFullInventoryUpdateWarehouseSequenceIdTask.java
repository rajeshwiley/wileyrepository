package com.wiley.integrations.inventory;

import de.hybris.platform.acceleratorservices.dataimport.batch.BatchHeader;
import de.hybris.platform.acceleratorservices.dataimport.batch.HeaderTask;
import de.hybris.platform.ordersplitting.WarehouseService;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Resource;

import org.apache.log4j.Logger;


/**
 * Update warehouse sequenceId
 */
public class WileycomFullInventoryUpdateWarehouseSequenceIdTask implements HeaderTask
{
	private static final Logger LOG = Logger.getLogger(WileycomFullInventoryUpdateWarehouseSequenceIdTask.class);

	@Resource
	private WarehouseService warehouseService;

	@Resource
	private ModelService modelService;

	@Override
	public BatchHeader execute(final BatchHeader header) throws Exception
	{
		InventoryBatchHeader inventoryBatchHeader = (InventoryBatchHeader) header;
		Long newSequenceId = inventoryBatchHeader.getSequenceId();
		WarehouseModel warehouseModel = warehouseService.getWarehouseForCode(inventoryBatchHeader.getWarehouseCode());
		Long currSequenceId = warehouseModel.getSequenceId();
		if (currSequenceId == null || currSequenceId <= newSequenceId)
		{
			warehouseModel.setSequenceId(newSequenceId);
			modelService.save(warehouseModel);
		}
		else
		{
			LOG.error(String.format("Warehouse %s has newer sequenceId than %s.", warehouseModel.getCode(), newSequenceId));
		}
		return header;
	}
}
