package com.wiley.integrations.esb.service;

import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;

import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.wiley.integrations.esb.dto.common.Discount;


/**
 * It is in charge of applying external discounts to cart and cart entries.
 */
public interface Wileyb2bExternalDiscountApplier
{

	/**
	 * Applies external discount to cart. Clean up applied discounts.
	 *
	 * @param cartModel
	 * @param externalDiscounts
	 */
	void applyDiscounts(@Nonnull CartModel cartModel, @Nullable List<Discount> externalDiscounts);

	/**
	 * Applies external discount to cart entry. Clean up applied discounts.
	 *
	 * @param cartEntryModel
	 * @param externalDiscounts
	 */
	void applyDiscounts(@Nonnull CartEntryModel cartEntryModel, @Nullable List<Discount> externalDiscounts);

}
