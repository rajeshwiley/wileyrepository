package com.wiley.integrations.order.dto;

import java.util.Objects;

import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderWsDTO extends AbstractOrderWsDTO
{
	@Size(max = 255)
	private String status;
	@Size(max = 255)
	private String sourceSystem;
	@Size(max = 255)
	private String placedByAgent;

	/**
	 * The current status of the order (for example, CREATED, PAYMENT_CAPTURED).
	 * Possible statuses are defined in the following document: P4 - Order fulfillment process.
	 */
	@JsonProperty("status")
	public String getStatus()
	{
		return status;
	}

	public void setStatus(final String status)
	{
		this.status = status;
	}

	/**
	 * The code of the system where the order was initially created. Equals to "hybris" for the orders
	 * placed via the regular storefront.
	 */
	@JsonProperty("sourceSystem")
	public String getSourceSystem()
	{
		return sourceSystem;
	}

	public void setSourceSystem(final String sourceSystem)
	{
		this.sourceSystem = sourceSystem;
	}

	/**
	 * The unique identifier of the ASM or CS agent that placed the order. The ALM key (userId) is used
	 * as the identifier in scope of Phase 4.
	 */
	@JsonProperty("placedByAgent")
	public String getPlacedByAgent()
	{
		return placedByAgent;
	}

	public void setPlacedByAgent(final String placedByAgent)
	{
		this.placedByAgent = placedByAgent;
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		OrderWsDTO orderValue = (OrderWsDTO) o;
		return Objects.equals(status, orderValue.status)
				&& Objects.equals(sourceSystem, orderValue.sourceSystem)
				&& Objects.equals(placedByAgent, orderValue.placedByAgent);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(status, sourceSystem, placedByAgent);
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("class OrderWsDTO {\n");

		sb.append("    status: ").append(toIndentedString(status)).append("\n");
		sb.append("    sourceSystem: ").append(toIndentedString(sourceSystem)).append("\n");
		sb.append("    placedByAgent: ").append(toIndentedString(placedByAgent)).append("\n");
		sb.append("}").append("\n");
		sb.append(super.toString());
		return sb.toString();
	}


	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(final Object o)
	{
		if (o == null)
		{
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
