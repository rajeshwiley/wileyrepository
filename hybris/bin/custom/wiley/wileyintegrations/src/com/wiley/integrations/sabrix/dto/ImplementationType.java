package com.wiley.integrations.sabrix.dto;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ImplementationType.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ImplementationType"&gt;
 *   &lt;restriction base="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1}StringType"&gt;
 *     &lt;enumeration value="BPEL"/&gt;
 *     &lt;enumeration value="ESB"/&gt;
 *     &lt;enumeration value="Other"/&gt;
 *     &lt;enumeration value="JAVA"/&gt;
 *     &lt;enumeration value=".Net"/&gt;
 *     &lt;enumeration value="RPG"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 */
@XmlType(name = "ImplementationType", namespace = "http://xmlns.wiley.com/EnterpriseBusinessObjects/CommonInfratypes/Codelist/V1")
@XmlEnum
public enum ImplementationType
{

	BPEL("BPEL"),
	ESB("ESB"),
	@XmlEnumValue("Other")
	OTHER("Other"),
	JAVA("JAVA"),
	@XmlEnumValue(".Net")
	NET(".Net"),
	RPG("RPG");
	private final String value;

	ImplementationType(String v)
	{
		value = v;
	}

	public String value()
	{
		return value;
	}

	public static ImplementationType fromValue(String v)
	{
		for (ImplementationType c : ImplementationType.values())
		{
			if (c.value.equals(v))
			{
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}

}
