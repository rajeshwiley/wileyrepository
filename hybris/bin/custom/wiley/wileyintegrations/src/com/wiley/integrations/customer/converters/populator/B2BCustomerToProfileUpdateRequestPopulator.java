package com.wiley.integrations.customer.converters.populator;

import com.wiley.integrations.cdm.enums.CustomerRole;
import com.wiley.integrations.customer.dto.ProfileUpdateRequest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;


public class B2BCustomerToProfileUpdateRequestPopulator implements Populator<B2BCustomerModel, ProfileUpdateRequest>
{
	private static final String B2B_ADMIN_GROUP = "b2badmingroup";

	@Override
	public void populate(final B2BCustomerModel b2bCustomer, final ProfileUpdateRequest target) throws ConversionException
	{
		target.setActive(b2bCustomer.getActive().toString());

		if (isB2BAdminGroup(b2bCustomer))
		{
			target.setRole(CustomerRole.B2B_ADMIN.toString());
		}
		else
		{
			target.setRole(CustomerRole.B2B_USER.toString());
		}

		if (b2bCustomer.getDefaultB2BUnit() != null)
		{
			target.setSapAccountNumber(b2bCustomer.getDefaultB2BUnit().getSapAccountNumber());
			target.setOrganizationalEcidNumber(b2bCustomer.getDefaultB2BUnit().getAccountEcidNumber());
		}
	}

	private boolean isB2BAdminGroup(final B2BCustomerModel b2bCustomer)
	{
		return b2bCustomer.getGroups().stream().anyMatch(group -> group.getUid().equalsIgnoreCase(B2B_ADMIN_GROUP));
	}
}
