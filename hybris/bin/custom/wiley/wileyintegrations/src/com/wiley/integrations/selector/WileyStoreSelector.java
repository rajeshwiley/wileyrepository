package com.wiley.integrations.selector;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.integration.core.MessageSelector;
import org.springframework.messaging.Message;


/**
 * Check if current site from the list of accepted.
 */
public class WileyStoreSelector implements MessageSelector
{
	private static final Logger LOG = Logger.getLogger(WileyStoreSelector.class);

	private WileyStoreSelectorService wileyStoreSelectorService;

	private Set<String> storeUids;

	@Override
	public boolean accept(final Message<?> message)
	{
		Optional<BaseStoreModel> currentStore = wileyStoreSelectorService.getCurrentStore(message);
		if (currentStore.isPresent())
		{
			if (currentStore.isPresent() && storeUids.contains(currentStore.get().getUid()))
			{
				return true;
			}
			else
			{
				LOG.error("Endpoint disabled for Store" + currentStore.get().getUid());
			}
		}
		else
		{
			LOG.error("Current base store NOT found, Endpoint disabled");
		}
		return false;
	}

	@Required
	public void setStoreUids(final Set<String> storeUids)
	{
		this.storeUids = storeUids;
	}

	@Required
	public void setWileyStoreSelectorService(final WileyStoreSelectorService wileyStoreSelectorService)
	{
		this.wileyStoreSelectorService = wileyStoreSelectorService;
	}
}
