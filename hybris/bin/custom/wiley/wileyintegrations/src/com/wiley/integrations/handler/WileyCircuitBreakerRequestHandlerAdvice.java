package com.wiley.integrations.handler;

import java.util.Set;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.handler.advice.RequestHandlerCircuitBreakerAdvice;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHandlingException;

import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.HystrixCommandKey;
import com.netflix.hystrix.HystrixCommandProperties;
import com.netflix.hystrix.exception.HystrixBadRequestException;
import com.netflix.hystrix.exception.HystrixRuntimeException;
import com.wiley.core.exceptions.ExternalSystemInternalErrorException;
import com.wiley.core.wiley.circuitbreaker.helper.PropertiesSetterHelper;


/**
 * Created by Uladzimir_Barouski on 6/1/2016.
 */
public class WileyCircuitBreakerRequestHandlerAdvice extends RequestHandlerCircuitBreakerAdvice
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyCircuitBreakerRequestHandlerAdvice.class);

	@Resource
	private PropertiesSetterHelper propertiesSetterHelper;

	private String commandGroupKey;

	private String commandKey;

	private HystrixCommandProperties.Setter propertiesSetter;

	private Set<Class<Exception>> ignoreExceptionClasses;

	protected Object doInvoke(final ExecutionCallback callback, final Object target, final Message<?> message) throws Exception
	{
		final HystrixCommand.Setter commandSetter = HystrixCommand.Setter.withGroupKey(
				HystrixCommandGroupKey.Factory.asKey(getCommandGroupKey()))
				.andCommandKey(HystrixCommandKey.Factory.asKey(getCommandKey()))
				.andCommandPropertiesDefaults(propertiesSetter);

		final HystrixCommand<Object> command = new HystrixCommand<Object>(commandSetter)
		{
			@Override
			protected Object run() throws Exception
			{
				Object result;
				try
				{
					result = callback.execute();
				}
				catch (MessageHandlingException exception)
				{
					if (isIgnoredException(exception))
					{
						final Throwable ignoredException = exception.getCause();
						throw new HystrixBadRequestException(ignoredException.getMessage(), ignoredException);
					}
					else
					{
						throw exception;
					}
				}
				return result;
			}
		};

		Object result;
		try
		{
			result = command.execute();
		}
		catch (HystrixBadRequestException e)
		{
			// You can wrap the exception that you would like to throw in HystrixBadRequestException and retrieve it via
			// getCause(). The HystrixBadRequestException is intended for use cases such as reporting illegal arguments or
			// non-system failures that should not count against the failure metrics and should not trigger fallback logic.
			throw (Exception) e.getCause();
		}
		catch (HystrixRuntimeException e)
		{
			LOG.debug("An error occurs during command {} call. Error: {}", getCommandKey(), e.getMessage());
			throw new ExternalSystemInternalErrorException(String.format("The external system unreachable for command [%1$s]",
					getCommandKey()), e);
		}
		return result;
	}

	/**
	 * Set properties for @HystrixCommand
	 * All other properties are left default. Please find values here https://github.com/Netflix/Hystrix/wiki/Configuration
	 */
	@PostConstruct
	public void postConstruct()
	{
		configureCircuitBreaker();
	}

	private void configureCircuitBreaker()
	{
		propertiesSetter = HystrixCommandProperties.defaultSetter();
		propertiesSetterHelper.configureCircuitBreakerCommand(getCommandKey(), propertiesSetter);
	}

	private boolean isIgnoredException(final MessageHandlingException e)
	{
		boolean result = false;

		if (ignoreExceptionClasses != null)
		{
			result = ignoreExceptionClasses.stream().anyMatch(exception -> exception.isInstance(e.getCause()));
		}

		return result;
	}

	public void setIgnoreExceptionClasses(final Set<Class<Exception>> ignoreExceptionClasses)
	{
		this.ignoreExceptionClasses = ignoreExceptionClasses;
	}

	public void setCommandKey(final String commandKey)
	{
		this.commandKey = commandKey;
	}

	public void setCommandGroupKey(final String commandGroupKey)
	{
		this.commandGroupKey = commandGroupKey;
	}

	public String getCommandGroupKey()
	{
		return commandGroupKey;
	}

	public String getCommandKey()
	{
		return commandKey;
	}

	public HystrixCommandProperties.Setter getPropertiesSetter()
	{
		return propertiesSetter;
	}

	public Set<Class<Exception>> getIgnoreExceptionClasses()
	{
		return ignoreExceptionClasses;
	}
}
