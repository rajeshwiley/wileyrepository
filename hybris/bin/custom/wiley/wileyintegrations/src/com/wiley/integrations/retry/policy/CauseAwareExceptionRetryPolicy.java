package com.wiley.integrations.retry.policy;

import java.util.Map;

import org.springframework.classify.BinaryExceptionClassifier;
import org.springframework.retry.RetryContext;
import org.springframework.retry.policy.SimpleRetryPolicy;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public class CauseAwareExceptionRetryPolicy extends SimpleRetryPolicy
{
	private BinaryExceptionClassifier retryableClassifier;
	private BinaryExceptionClassifier wrapperRetryableClassifier;

	public CauseAwareExceptionRetryPolicy(final int maxAttempts,
			final Map<Class<? extends Throwable>, Boolean> retryableExceptions,
			final Map<Class<? extends Throwable>, Boolean> wrapperRetryableExceptions)
	{
		super(maxAttempts, retryableExceptions, false);
		this.retryableClassifier = new BinaryExceptionClassifier(retryableExceptions);
		this.wrapperRetryableClassifier = new BinaryExceptionClassifier(wrapperRetryableExceptions);
	}

	@Override
	public boolean canRetry(final RetryContext context)
	{
		final Throwable t = context.getLastThrowable();
		return (t == null || retryForException(t)) && context.getRetryCount() < getMaxAttempts();
	}

	private boolean retryForException(final Throwable t)
	{
		if (wrapperRetryableClassifier.classify(t))
		{
			final Throwable cause = t.getCause();
			return retryableClassifier.classify(cause);
		}
		return false;
	}
}
