package com.wiley.integrations.esb.dto.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;


/**
 * Key-value pair.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class KeyValue
{
	private String key;

	private String value;

	@JsonProperty("key")
	public String getKey()
	{
		return key;
	}

	public void setKey(final String key)
	{
		this.key = key;
	}

	@JsonProperty("value")
	public String getValue()
	{
		return value;
	}

	public void setValue(final String value)
	{
		this.value = value;
	}

	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this);
	}
}
