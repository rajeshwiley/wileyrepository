package com.wiley.integrations.eloqua.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.wiley.core.integration.eloqua.dto.ContactDto;


/**
 * Please see http://secure.eloqua.com/api/docs/Static/Rest/1.0/t_queryresult%601_24c8287fc592504efb13ea3582fbe2.htm
 *
 * @author Aliaksei_Zlobich
 */
@XmlRootElement(name = "QueryResult")
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SearchContactQueryResult
{
	private String type = "QueryResult";

	@XmlElement
	private List<ContactDto> elements;

	private Integer page;

	private Integer pageSize;

	private Integer total;

	public String getType()
	{
		return type;
	}

	public void setType(final String type)
	{
		this.type = type;
	}

	public List<ContactDto> getElements()
	{
		return elements;
	}

	public void setElements(final List<ContactDto> elements)
	{
		this.elements = elements;
	}

	public Integer getPage()
	{
		return page;
	}

	public void setPage(final Integer page)
	{
		this.page = page;
	}

	public Integer getPageSize()
	{
		return pageSize;
	}

	public void setPageSize(final Integer pageSize)
	{
		this.pageSize = pageSize;
	}

	public Integer getTotal()
	{
		return total;
	}

	public void setTotal(final Integer total)
	{
		this.total = total;
	}
}
