package com.wiley.integrations.eloqua.util;

import java.net.URI;

import org.apache.http.HttpHost;
import org.apache.http.client.AuthCache;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.protocol.HttpContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;


/**
 * Extended HttpRequestFactory to fix issue with basic authentication for rest template.<br/>
 * Please, see http://hc.apache.org/httpcomponents-client-ga/tutorial/html/authentication.html#d4e1023 Preemptive authentication
 *
 * @author Aliaksei_Zlobich
 */
public class BasicAuthHttpComponentsClientHttpRequestFactory extends HttpComponentsClientHttpRequestFactory
{

	private static final Logger LOG = LoggerFactory.getLogger(BasicAuthHttpComponentsClientHttpRequestFactory.class);

	@Override
	protected HttpContext createHttpContext(final HttpMethod httpMethod, final URI uri)
	{
		// http://hc.apache.org/httpcomponents-client-ga/tutorial/html/authentication.html#d4e1023 Preemptive authentication

		LOG.debug("Creating HttpClientContext: URI[host: {}, port: {}, scheme: {}].", uri.getHost(), uri.getPort(),
				uri.getScheme());

		// Create AuthCache instance
		AuthCache authCache = new BasicAuthCache();
		// Generate BASIC scheme object and add it to the local auth cache
		BasicScheme basicAuth = new BasicScheme();

		authCache.put(new HttpHost(uri.getHost(), uri.getPort(), uri.getScheme()), basicAuth);

		// Add AuthCache to the execution context
		HttpClientContext httpContext = HttpClientContext.create();
		httpContext.setAuthCache(authCache);

		return httpContext;
	}
}
