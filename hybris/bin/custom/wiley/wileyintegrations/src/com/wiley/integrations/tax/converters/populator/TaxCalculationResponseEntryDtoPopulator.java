package com.wiley.integrations.tax.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;

import com.wiley.core.externaltax.dto.TaxCalculationResponseEntryDto;
import com.wiley.core.externaltax.dto.TaxValueDto;
import com.wiley.integrations.tax.dto.TaxCalculationResponseEntry;
import com.wiley.integrations.tax.dto.TaxValue;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


public class TaxCalculationResponseEntryDtoPopulator
		implements Populator<TaxCalculationResponseEntry, TaxCalculationResponseEntryDto>
{
	@Resource
	private Converter<TaxValue, TaxValueDto> taxValueDtoConverter;


	@Override
	public void populate(final TaxCalculationResponseEntry source, final TaxCalculationResponseEntryDto target)
			throws ConversionException
	{
		validateParameterNotNull(source, "source mustn't be null");
		validateParameterNotNull(target, "target mustn't be null");

		target.setId(Integer.valueOf(source.getId()));
		target.setTaxes(taxValueDtoConverter.convertAll(source.getTaxes()));
	}
}
