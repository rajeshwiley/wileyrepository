package com.wiley.integrations.ediintegration.endpoints.mail;

import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.util.localization.Localization;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.mail.MailHeaders;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.messaging.Message;

import com.wiley.core.integration.edi.EDIConstants;
import com.wiley.fulfilmentprocess.model.WileyExportProcessModel;
import com.wiley.integrations.ediintegration.endpoints.generator.WileyExcelFileNameGenerator;

import static com.wiley.integrations.constants.WileyintegrationsConstants.BASE_STORE_KEY;
import static com.wiley.integrations.constants.WileyintegrationsConstants.DATE_TIME_FORMATTER;
import static com.wiley.integrations.constants.WileyintegrationsConstants.EXPORT_TYPE_KEY;
import static com.wiley.integrations.constants.WileyintegrationsConstants.LOCAL_DATE_TIME;
import static com.wiley.integrations.constants.WileyintegrationsConstants.PAYMENT_TYPE_KEY;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.SPACE;


/**
 * Class for adding headers related to emails.
 * Moved to java class so properties can be changed in hac at runtime.
 * If define them in xml configuration then they will be applied only after hybris restart.
 */
public class WileyExcelReportEmailHeaders
{
	private static final String MAIL_TO = "mailTo";
	private static final String PAYPAL_PAYMENT_TYPE_IDENTIFIER_PART = " PAYPAL";
	private static final Logger LOG = Logger.getLogger(WileyExcelReportEmailHeaders.class);
	@Autowired
	private WileyExcelFileNameGenerator wileyExcelFileNameGenerator;
	@Autowired
	private ConfigurationService configurationService;

	public static String getSiteShortcut(final Message<List<WileyExportProcessModel>> message)
	{
		BaseStoreModel baseStore = (BaseStoreModel) message.getHeaders().get(BASE_STORE_KEY);
		return baseStore != null ? baseStore.getUid() : org.apache.commons.lang.StringUtils.EMPTY;
	}

	public Message addHeadersForNoOrdersEmail(final Message<List<WileyExportProcessModel>> message)
	{
		LOG.debug(String.format("Adding headers for no orders email %s", getSiteShortcut(message)));
		return setHeaders(getNewMessageBuilder(message), message).build();
	}

	public Message addHeadersForEmailWithExcel(final Message<List<WileyExportProcessModel>> message) throws IOException
	{
		LOG.debug(String.format("Adding headers for email with excel %s", getSiteShortcut(message)));
		return setHeaders(MessageBuilder.fromMessage(message), message).setHeader(MailHeaders.ATTACHMENT_FILENAME,
				wileyExcelFileNameGenerator.generateFileName(message)).build();
	}

	private MessageBuilder setHeaders(final MessageBuilder builder, final Message<List<WileyExportProcessModel>> message)
	{
		final LocalDateTime localDate = (LocalDateTime) message.getHeaders().get(LOCAL_DATE_TIME);
		final String paymentType = String.valueOf(message.getHeaders().get(PAYMENT_TYPE_KEY));
		final String fromEmail = configurationService.getConfiguration().getString("wiley.from.email");
		final String formattedDate = DateTimeFormatter.ofPattern(DATE_TIME_FORMATTER).format(localDate);
		final String exportType = String.valueOf(message.getHeaders().get(EXPORT_TYPE_KEY));
		final String subject = buildMailSubject(getSiteShortcut(message), exportType, formattedDate, paymentType);
		return builder.setHeader(MailHeaders.SUBJECT, subject).setHeader(MailHeaders.FROM, fromEmail).setHeader(MailHeaders.TO,
				message.getHeaders().get(MAIL_TO)).setHeader(MailHeaders.REPLY_TO, fromEmail).setHeader(
				MailHeaders.MULTIPART_MODE, MimeMessageHelper.MULTIPART_MODE_RELATED);
	}

	private String buildMailSubject(final String siteShortcut, final String exportType, final String formattedDate,
			final String paymentType)
	{
		final String subjectFirstPart = Localization.getLocalizedString("wiley.orders.report.email.subject");
		final String subjectPattern = subjectFirstPart + SPACE + siteShortcut + SPACE + exportType + "%s" + SPACE + formattedDate;
		final String paymentTypePart = paymentType.equalsIgnoreCase(EDIConstants.PAYMENT_TYPE_PAYPAL) ?
				PAYPAL_PAYMENT_TYPE_IDENTIFIER_PART :
				EMPTY;
		final String subject = String.format(subjectPattern, paymentTypePart);
		LOG.debug(String.format("Setting headers %s", subject));
		return subject;
	}

	private MessageBuilder<String> getNewMessageBuilder(final Message<List<WileyExportProcessModel>> message)
	{
		final LocalDateTime localDate = (LocalDateTime) message.getHeaders().get(LOCAL_DATE_TIME);
		final String formattedDate = DateTimeFormatter.ofPattern(DATE_TIME_FORMATTER).format(localDate);
		final String body = Localization.getLocalizedString("wiley.orders.report.no.orders.email.body");
		return MessageBuilder.withPayload(body + SPACE + formattedDate);
	}
}
