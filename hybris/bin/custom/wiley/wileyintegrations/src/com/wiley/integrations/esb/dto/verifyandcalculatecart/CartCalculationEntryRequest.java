package com.wiley.integrations.esb.dto.verifyandcalculatecart;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.wiley.integrations.esb.dto.common.AbstractOrderEntryRequest;


/**
 * An order entry - an order line for a single product.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CartCalculationEntryRequest extends AbstractOrderEntryRequest
{
}

