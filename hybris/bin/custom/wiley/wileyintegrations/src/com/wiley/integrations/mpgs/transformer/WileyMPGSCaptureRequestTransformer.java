
package com.wiley.integrations.mpgs.transformer;


import de.hybris.platform.servicelayer.util.ServicesUtil;

import javax.annotation.Nonnull;

import com.wiley.core.mpgs.constants.WileyMPGSConstants;
import com.wiley.core.mpgs.dto.capture.MPGSCaptureRequestDTO;
import com.wiley.core.mpgs.dto.json.Transaction;
import com.wiley.core.mpgs.request.WileyCaptureRequest;


public class WileyMPGSCaptureRequestTransformer
{

	public MPGSCaptureRequestDTO transform(@Nonnull final WileyCaptureRequest request)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("request", request);

		MPGSCaptureRequestDTO captureRequest = new MPGSCaptureRequestDTO();
		captureRequest.setApiOperation(WileyMPGSConstants.API_OPERATION_CAPTURE);

		Transaction transaction = new Transaction();
		transaction.setAmount(request.getAmount());
		transaction.setCurrency(request.getCurrency());
		transaction.setReference(request.getTransactionReference());

		captureRequest.setTransaction(transaction);

		return captureRequest;
	}
}