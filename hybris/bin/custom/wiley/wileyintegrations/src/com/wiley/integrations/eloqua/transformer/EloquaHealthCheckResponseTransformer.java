package com.wiley.integrations.eloqua.transformer;

import javax.annotation.Nonnull;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.codahale.metrics.health.HealthCheck.Result;


/**
 * Returns HealthCheckResult based on response received from Eloqua end point
 */
public class EloquaHealthCheckResponseTransformer
{
	@Nonnull
	public Result transform(@Nonnull final ResponseEntity<String> responseEntity)
	{
		if (responseEntity == null) //NOSONAR
		{
			return Result.unhealthy("Response was null");
		}
		else if (HttpStatus.OK.equals(responseEntity.getStatusCode()))
		{
			return Result.healthy();
		}
		else
		{
			return Result.unhealthy(String.format("Response status code was %s", responseEntity.getStatusCode()));
		}
	}
}