/**
 *
 */
package com.wiley.integrations.cdm.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonInclude(JsonInclude.Include.NON_NULL)
public class CustomerBaseProfileDTO {

  @JsonProperty(value = "CustomerIdentification")
  private CDMCustomerIdentificationDTO customerIdentification;
  @JsonProperty(value = "CustomerTypeCode")
  private String customerTypeCode;
  @JsonProperty(value = "Parent_Org_Name")
  private String parentOrgName;

  /**
   * @return the customerIdentification
   */
  public CDMCustomerIdentificationDTO getCustomerIdentification() {
    return customerIdentification;
  }

  /**
   * @param customerIdentification
   *          the customerIdentification to set
   */
  public void setCustomerIdentification(final CDMCustomerIdentificationDTO customerIdentification) {
    this.customerIdentification = customerIdentification;
  }

  /**
   * @return the customerTypeCode
   */
  public String getCustomerTypeCode() {
    return customerTypeCode;
  }

  /**
   * @param customerTypeCode
   *          the customerTypeCode to set
   */
  public void setCustomerTypeCode(final String customerTypeCode) {
    this.customerTypeCode = customerTypeCode;
  }

  /**
   * @return the parentOrgName
   */
  public String getParentOrgName() {
    return parentOrgName;
  }

  /**
   * @param parentOrgName
   *          the parentOrgName to set
   */
  public void setParentOrgName(final String parentOrgName) {
    this.parentOrgName = parentOrgName;
  }


  @Override
  public String toString() {
    return "CustomerBaseProfileDTO [customerIdentification=" + customerIdentification
        + ", customerTypeCode=" + customerTypeCode + ", parent_Org_Name=" + parentOrgName + "]";
  }


}
