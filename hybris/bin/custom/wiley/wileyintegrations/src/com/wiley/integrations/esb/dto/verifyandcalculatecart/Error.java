package com.wiley.integrations.esb.dto.verifyandcalculatecart;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Description of a server error.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Error
{

	/**
	 * Error code.
	 **/
	private String code = null;
	/**
	 * Error description.
	 **/
	private String message = null;

	@JsonProperty("code")
	public String getCode()
	{
		return code;
	}

	public void setCode(final String code)
	{
		this.code = code;
	}

	@JsonProperty("message")
	public String getMessage()
	{
		return message;
	}

	public void setMessage(final String message)
	{
		this.message = message;
	}

	@Override
	public String toString()
	{
		return new ToStringBuilder(this)
				.append("code", code)
				.append("message", message)
				.toString();
	}
}

