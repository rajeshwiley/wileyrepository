package com.wiley.integrations.ebp.populator;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.voucher.VoucherService;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import com.paypal.hybris.model.PaypalPaymentInfoModel;
import com.wiley.core.category.WileyCategoryService;
import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.integration.ebp.dto.EbpAddProductsPayload;
import com.wiley.core.order.WileyCheckoutService;
import com.wiley.core.partner.WileyPartnerService;
import com.wiley.core.pin.service.PinService;
import com.wiley.core.price.WileyDiscountCalculationService;
import com.wiley.integrations.ebp.dto.OrderNode;
import com.wiley.integrations.ebp.enums.WileyEbpPaymentMethod;


/**
 * Populator for EBP Order node
 */
public class OrderToEbpOrderInfoPopulator implements Populator<EbpAddProductsPayload, OrderNode>
{
	private static final String EBP_SOURCE_SEPARATOR = ",";
	private static final String PRICING_TYPE_RETAIL = "retail";
	private static final String PRICING_TYPE_STUDENT = "student";
	private static final int DECIMAL_VALUES_ROUNDING_MODE = BigDecimal.ROUND_HALF_DOWN;
	private static final int DECIMAL_VALUES_PRECISION_SCALE = 2;

	@Resource
	private PinService pinService;
	@Resource
	private VoucherService voucherService;
	@Resource
	private WileyCheckoutService wileyCheckoutService;
	@Resource
	private WileyCategoryService categoryService;
	@Resource
	private SessionService sessionService;
	@Resource
	private UserService userService;
	@Autowired
	private WileyPartnerService partnerService;
	@Autowired
	private WileyDiscountCalculationService wileyDiscountCalculationService;

	/**
	 * Populate the target instance with values from the source instance.
	 *
	 * @param payload
	 * 		the source object
	 * @param orderNode
	 * 		the target to fill
	 * @throws ConversionException
	 * 		if an error occurs
	 */
	@Override
	public void populate(@Nonnull final EbpAddProductsPayload payload, @Nonnull final OrderNode orderNode)
			throws ConversionException
	{
		Assert.notNull(payload);
		Assert.notNull(orderNode);
		final OrderModel order = payload.getOrder();
		Assert.notNull(order);

		SimpleDateFormat fmt = new SimpleDateFormat(WileyCoreConstants.WILEY_EBP_DATE_TIME_FORMAT_NAME, Locale.US);

		final CurrencyModel currency = order.getCurrency();
		int roundingDigits =
				(currency != null && currency.getDigits() != null && currency.getDigits() != 0) ?
						currency.getDigits() : DECIMAL_VALUES_PRECISION_SCALE;
		final BigDecimal totalTax = roundDecimalValue(order.getTotalTax(), roundingDigits);
		final BigDecimal totalPrice = roundDecimalValue(order.getTotalPrice(), roundingDigits);
		final BigDecimal totalAmount = totalPrice.add(totalTax);
		final BigDecimal discountAmount =
				roundDecimalValue(wileyDiscountCalculationService.getTotalDiscount(order), roundingDigits);
		final BigDecimal shippingAmount = roundDecimalValue(order.getDeliveryCost(), roundingDigits);

		orderNode.setOrderNumber(order.getCode());
		orderNode.setOrderDateTime(fmt.format(order.getDate()));
		orderNode.setDiscountAmount(discountAmount);
		orderNode.setTaxAmount(totalTax);
		orderNode.setShippingAmount(shippingAmount);
		orderNode.setTotalAmount(totalAmount);
		orderNode.setManualOrder(isManualOrder(order.getPlacedBy()));
		if (order.getDeliveryMode() != null)
		{
			// delivery mode is not set for digital products
			orderNode.setShippingMethod(order.getDeliveryMode().getCode());
		}
		if (pinService.isPinUsedForOrder(order))
		{
			orderNode.setPin(pinService.getPinForOrder(order).getCode());
		}

		// Always false since Hybris does not manage tax exemption.
		orderNode.setTaxExempt(false);

		orderNode.setPricingType(wileyCheckoutService.isStudentOrder(order) ? PRICING_TYPE_STUDENT : PRICING_TYPE_RETAIL);

		populateCoupons(order, orderNode);
		populateSource(order, orderNode);
		populatePaymentMethod(order, orderNode);
	}

	private void populateCoupons(final OrderModel order, final OrderNode orderNode)
	{
		final List<String> coupons = orderNode.getCoupons();

		// The voucher's code will be passed along if a voucher was applied.
		// If more than one voucher was applied, all the values will be sent in separate fields.
		final Collection<String> voucherCodesAppliedToOrder = voucherService.getAppliedVoucherCodes(order);

		if (voucherCodesAppliedToOrder != null)
		{
			voucherCodesAppliedToOrder.forEach(voucherCode -> coupons.add(voucherCode));
		}

		// The partner's discount code will be passed along if the order was placed using a partner checkout flow.
		final Optional<String> partnerCode = partnerService.getPartnerCode(order);
		if (partnerCode.isPresent())
		{
			coupons.add(partnerCode.get());
		}
	}

	private void populateSource(final OrderModel order, final OrderNode orderNode)
	{
		List<String> source = new ArrayList<>();

		sessionService.executeInLocalView(new SessionExecutionBody()
		{
			@Override
			public void executeWithoutResult()
			{
				for (AbstractOrderEntryModel orderEntry : order.getEntries())
				{
					CategoryModel baseCategory = categoryService.getPrimaryWileyCategoryForProduct(orderEntry.getProduct());
					if (baseCategory != null && !source.contains(baseCategory.getName(Locale.ENGLISH)))
					{
						source.add(baseCategory.getName(Locale.ENGLISH));
					}
				}
			}
		}, userService.getAdminUser());

		orderNode.setSource(StringUtils.join(source, EBP_SOURCE_SEPARATOR));
	}

	private void populatePaymentMethod(final OrderModel order, final OrderNode orderNode)
	{
		if (wileyCheckoutService.isNonZeroOrder(order))
		{
			if (order.getPaymentInfo() instanceof PaypalPaymentInfoModel)
			{
				orderNode.setPaymentMethod(WileyEbpPaymentMethod.PAYPAL.getValue());
			}
			else
			{
				orderNode.setPaymentMethod(WileyEbpPaymentMethod.CREDIT_CARD.getValue());
			}
		}
		else
		{
			orderNode.setPaymentMethod(WileyEbpPaymentMethod.FREE.getValue());
		}
	}

	private BigDecimal roundDecimalValue(final double value, final int digits)
	{
		return BigDecimal.valueOf(value).setScale(
				digits != 0 ? digits : DECIMAL_VALUES_PRECISION_SCALE,
				DECIMAL_VALUES_ROUNDING_MODE);
	}

	private boolean isManualOrder(final UserModel userModel)
	{
		return userModel instanceof EmployeeModel;
	}
}
