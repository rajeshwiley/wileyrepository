package com.wiley.integrations.sabrix.dto;

import java.math.BigInteger;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PagingCriteriaType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="PagingCriteriaType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="StartRecord" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1}NonNegativeIntegerType" minOccurs="0"/&gt;
 *         &lt;element name="PagingSize" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1}NonNegativeIntegerType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PagingCriteriaType", namespace = "http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/Meta/V1", propOrder = {
		"startRecord",
		"pagingSize"
})
public class PagingCriteriaType
{

	@XmlElement(name = "StartRecord")
	@XmlSchemaType(name = "nonNegativeInteger")
	protected BigInteger startRecord;
	@XmlElement(name = "PagingSize")
	@XmlSchemaType(name = "nonNegativeInteger")
	protected BigInteger pagingSize;

	/**
	 * Gets the value of the startRecord property.
	 *
	 * @return possible object is
	 * {@link BigInteger }
	 */
	public BigInteger getStartRecord()
	{
		return startRecord;
	}

	/**
	 * Sets the value of the startRecord property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link BigInteger }
	 */
	public void setStartRecord(BigInteger value)
	{
		this.startRecord = value;
	}

	/**
	 * Gets the value of the pagingSize property.
	 *
	 * @return possible object is
	 * {@link BigInteger }
	 */
	public BigInteger getPagingSize()
	{
		return pagingSize;
	}

	/**
	 * Sets the value of the pagingSize property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link BigInteger }
	 */
	public void setPagingSize(BigInteger value)
	{
		this.pagingSize = value;
	}

}
