package com.wiley.integrations.sabrix.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for QuantitiesType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="QuantitiesType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="QUANTITY" type="{http://www.sabrix.com/services/taxcalculationservice/2011-09-01}QuantityType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QuantitiesType", propOrder = {
		"quantity"
})
public class QuantitiesType
{

	@XmlElement(name = "QUANTITY")
	protected List<QuantityType> quantity;

	/**
	 * Gets the value of the quantity property.
	 *
	 * <p>
	 * This accessor method returns a reference to the live list,
	 * not a snapshot. Therefore any modification you make to the
	 * returned list will be present inside the JAXB object.
	 * This is why there is not a <CODE>set</CODE> method for the quantity property.
	 *
	 * <p>
	 * For example, to add a new item, do as follows:
	 * <pre>
	 *    getQUANTITY().add(newItem);
	 * </pre>
	 *
	 *
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link QuantityType }
	 */
	public List<QuantityType> getQUANTITY()
	{
		if (quantity == null)
		{
			quantity = new ArrayList<QuantityType>();
		}
		return this.quantity;
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		final QuantitiesType that = (QuantitiesType) o;
		return Objects.equals(quantity, that.quantity);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(quantity);
	}
}
