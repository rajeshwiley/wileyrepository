package com.wiley.integrations.alm.authentication.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
@JsonInclude(NON_NULL)
public class AlmAuthenticationUserDto
{
	@JsonProperty(value = "userID", required = true)
	private String userId;

	public String getUserId()
	{
		return userId;
	}

	public void setUserId(final String userId)
	{
		this.userId = userId;
	}

	@Override
	public String toString()
	{
		return MoreObjects.toStringHelper(this)
				.add("userId", getUserId())
				.toString();
	}
}
