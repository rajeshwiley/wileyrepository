package com.wiley.integrations.sabrix.dto;

import java.util.Objects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Top level request for a tax calculation call.
 *
 *
 * <p>Java class for TaxCalculationRequest complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="TaxCalculationRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="INDATA" type="{http://www.sabrix.com/services/taxcalculationservice/2011-09-01}IndataType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TaxCalculationRequest", propOrder = {
		"indata"
})
public class TaxCalculationRequest
{

	@XmlElement(name = "INDATA", required = true)
	protected IndataType indata;

	/**
	 * Gets the value of the indata property.
	 *
	 * @return possible object is
	 * {@link IndataType }
	 */
	public IndataType getINDATA()
	{
		return indata;
	}

	/**
	 * Sets the value of the indata property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link IndataType }
	 */
	public void setINDATA(IndataType value)
	{
		this.indata = value;
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		final TaxCalculationRequest that = (TaxCalculationRequest) o;
		return Objects.equals(indata, that.indata);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(indata);
	}
}
