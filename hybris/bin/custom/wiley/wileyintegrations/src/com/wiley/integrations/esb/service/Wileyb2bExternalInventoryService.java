package com.wiley.integrations.esb.service;

import de.hybris.platform.core.model.order.CartEntryModel;

import javax.annotation.Nonnull;

import com.wiley.integrations.esb.dto.verifyandcalculatecart.CartCalculationEntryResponse;


/**
 * Service contains method for manipulating {@link com.wiley.core.model.ExternalInventoryModel}.
 */
public interface Wileyb2bExternalInventoryService
{

	/**
	 * Updates inventory statuses on cart entries.
	 *
	 * @param cartCalculationEntryResponse
	 * 		with received external inventory data.
	 * @param cartEntryModel
	 * 		target model for updating inventory.
	 */
	void updateInventoryStatuses(@Nonnull CartCalculationEntryResponse cartCalculationEntryResponse,
			@Nonnull CartEntryModel cartEntryModel);

}
