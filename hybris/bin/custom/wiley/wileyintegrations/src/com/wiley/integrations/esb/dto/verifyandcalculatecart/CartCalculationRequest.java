package com.wiley.integrations.esb.dto.verifyandcalculatecart;

import de.hybris.platform.util.DiscountValue;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wiley.integrations.esb.dto.common.Address;

import org.apache.commons.lang3.builder.ToStringBuilder;


/**
 * An order to be calculated.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CartCalculationRequest
{

	/**
	 * User's unique identifier. Generally, user emails are to be used as identifiers.
	 * Nevertheless, string literals are permitted to be used as identifiers too.
	 **/
	private String userId;

	private String country;

	private String currency;

	private String language;
	/**
	 * Code of the delivery option (priority code) that was chosen by the customer for this order.
	 **/
	private String deliveryOptionCode;

	private Address deliveryAddress;

	/**
	 * Code of the payment option that was chosen by the customer for this order.
	 */
	private String paymentOptionCode;

	private String creditCardType;

	private Address paymentAddress;

	/**
	 * List of the discount codes that were applied to this order.
	 **/
	private Set<DiscountValue> discountValues = new HashSet<DiscountValue>();

	/**
	 * List of the order entries.
	 **/
	private List<CartCalculationEntryRequest> entries = new ArrayList<CartCalculationEntryRequest>();

	@JsonProperty("userId")
	public String getUserId()
	{
		return userId;
	}

	public void setUserId(final String userId)
	{
		this.userId = userId;
	}

	@JsonProperty("country")
	public String getCountry()
	{
		return country;
	}

	public void setCountry(final String country)
	{
		this.country = country;
	}

	@JsonProperty("currency")
	public String getCurrency()
	{
		return currency;
	}

	public void setCurrency(final String currency)
	{
		this.currency = currency;
	}

	@JsonProperty("language")
	public String getLanguage()
	{
		return language;
	}

	public void setLanguage(final String language)
	{
		this.language = language;
	}

	@JsonProperty("deliveryOptionCode")
	public String getDeliveryOptionCode()
	{
		return deliveryOptionCode;
	}

	public void setDeliveryOptionCode(final String deliveryOptionCode)
	{
		this.deliveryOptionCode = deliveryOptionCode;
	}

	@JsonProperty("deliveryAddress")
	public Address getDeliveryAddress()
	{
		return deliveryAddress;
	}

	public void setDeliveryAddress(final Address deliveryAddress)
	{
		this.deliveryAddress = deliveryAddress;
	}

	@JsonProperty("paymentOptionCode")
	public String getPaymentOptionCode()
	{
		return paymentOptionCode;
	}

	public void setPaymentOptionCode(final String paymentOptionCode)
	{
		this.paymentOptionCode = paymentOptionCode;
	}

	@JsonProperty("creditCardType")
	public String getCreditCardType()
	{
		return creditCardType;
	}

	public void setCreditCardType(final String creditCardType)
	{
		this.creditCardType = creditCardType;
	}

	@JsonProperty("paymentAddress")
	public Address getPaymentAddress()
	{
		return paymentAddress;
	}

	public void setPaymentAddress(final Address paymentAddress)
	{
		this.paymentAddress = paymentAddress;
	}

	@JsonProperty("discountValues")
	public Set<DiscountValue> getDiscountValues()
	{
		return discountValues;
	}

	public void setDiscountValues(final Set<DiscountValue> discountValues)
	{
		this.discountValues = discountValues;
	}

	@JsonProperty("entries")
	public List<CartCalculationEntryRequest> getEntries()
	{
		return entries;
	}

	public void setEntries(final List<CartCalculationEntryRequest> entries)
	{
		this.entries = entries;
	}

	@Override
	public String toString()
	{
		return new ToStringBuilder(this)
				.append("userId", userId)
				.append("country", country)
				.append("currency", currency)
				.append("language", language)
				.append("deliveryOptionCode", deliveryOptionCode)
				.append("deliveryAddress", deliveryAddress)
				.append("paymentOptionCode", paymentOptionCode)
				.append("creditCardType", creditCardType)
				.append("paymentAddress", paymentAddress)
				.append("discountValues", discountValues)
				.append("entries", entries)
				.toString();
	}
}

