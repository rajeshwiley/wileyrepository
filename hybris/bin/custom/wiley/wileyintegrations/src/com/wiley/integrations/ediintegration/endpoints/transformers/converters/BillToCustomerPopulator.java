package com.wiley.integrations.ediintegration.endpoints.transformers.converters;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.Map;

import javax.annotation.Resource;

import com.wiley.integrations.ediintegration.endpoints.jaxb.BillToCustomer;


public class BillToCustomerPopulator implements Truncatable, Populator<OrderModel, BillToCustomer>
{

	@Resource(name = "addressFieldsLengthRestrictions")
	private Map<String, Integer> addressFieldsLengthRestrictions;

	@Override
	public void populate(final OrderModel orderModel, final BillToCustomer billToCustomer) throws ConversionException
	{
		final AddressModel address = orderModel.getPaymentAddress();
		if (address == null)
		{
			return;
		}
		billToCustomer.setAddressLine1(truncate(address.getLine1(), "addressLine1"));
		billToCustomer.setAddressLine2(truncate(address.getLine2(), "addressLine2"));
		billToCustomer.setCityName(truncate(address.getTown(), "cityName"));
		CountryModel country = address.getCountry();
		billToCustomer.setCountry(country.getNumeric());
		billToCustomer.setStateProvince(ConversionUtils.getStateProvinceFromAddress(address));
		billToCustomer.setZipPostCode(truncate(address.getPostalcode(), "zipPostCode"));
		billToCustomer.setCustomerName(truncate(address.getFirstname(), "customerName"));
		billToCustomer.setCustomerName2(truncate(address.getLastname(), "customerName2"));
	}

	@Override
	public Map<String, Integer> getAddressFieldsLengthRestrictions()
	{
		return addressFieldsLengthRestrictions;
	}

}
