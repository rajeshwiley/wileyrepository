package com.wiley.integrations.sabrix.transformer.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.annotation.Nonnull;

import com.wiley.integrations.sabrix.dto.IndataInvoiceType;


/**
 * @author Dzmitryi_Halahayeu
 */
public class InvoiceNumberToIndataInvoiceTypePopulator
		implements Populator<Integer, IndataInvoiceType>
{

	@Override
	public void populate(@Nonnull final Integer id, @Nonnull final IndataInvoiceType indataInvoiceType) throws ConversionException
	{
		indataInvoiceType.setINVOICENUMBER(String.valueOf(id));
	}
}
