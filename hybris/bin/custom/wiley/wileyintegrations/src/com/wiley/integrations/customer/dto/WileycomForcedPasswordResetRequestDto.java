package com.wiley.integrations.customer.dto;

import java.util.Objects;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * WileycomForcedPasswordResetRequestDto
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WileycomForcedPasswordResetRequestDto
{
	@NotNull
	@Size(min = 1, max = 255)
	private String newPassword = null;

	public WileycomForcedPasswordResetRequestDto newPassword(final String newPassword)
	{
		this.newPassword = newPassword;
		return this;
	}

	/**
	 * User's password.
	 *
	 * @return newPassword
	 **/
	@JsonProperty(required = true, value = "newPassword")
	public String getNewPassword()
	{
		return newPassword;
	}

	public void setNewPassword(final String newPassword)
	{
		this.newPassword = newPassword;
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}

		if (o == null || getClass() != o.getClass())
		{
			return false;
		}

		WileycomForcedPasswordResetRequestDto wileycomForcedPasswordResetRequestDto = (WileycomForcedPasswordResetRequestDto) o;
		return Objects.equals(this.newPassword, wileycomForcedPasswordResetRequestDto.newPassword);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(newPassword);
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("class WileycomForcedPasswordResetRequestDto {\n");

		sb.append("    newPassword: ").append(toIndentedString(newPassword)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(final Object o)
	{
		if (o == null)
		{
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}