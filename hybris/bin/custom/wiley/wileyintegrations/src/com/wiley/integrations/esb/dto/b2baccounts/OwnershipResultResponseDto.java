package com.wiley.integrations.esb.dto.b2baccounts;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.common.base.MoreObjects;

import org.codehaus.jackson.annotate.JsonProperty;


@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OwnershipResultResponseDto
{
	@JsonProperty("productPurchased")
	private boolean productPurchased;

	public boolean getProductPurchased()
	{
		return productPurchased;
	}

	public void setProductPurchased(final boolean productPurchased)
	{
		this.productPurchased = productPurchased;
	}

	@Override
	public String toString()
	{
		return MoreObjects.toStringHelper(this)
				.add("productPurchased", productPurchased)
				.toString();
	}
}
