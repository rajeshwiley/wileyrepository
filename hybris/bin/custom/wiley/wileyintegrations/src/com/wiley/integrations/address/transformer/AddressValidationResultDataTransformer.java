package com.wiley.integrations.address.transformer;

import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.springframework.messaging.Message;
import org.springframework.util.Assert;

import com.wiley.core.integration.address.dto.AddressDto;
import com.wiley.integrations.address.dto.AddressValidationResultDto;


public class AddressValidationResultDataTransformer
{
	@Resource
	private Converter<AddressValidationResultDto, AddressDto> addressValidationResultDtoAddressDataConverter;

	@Nonnull
	public List<AddressDto> transform(@Nonnull final Message<AddressValidationResultDto[]> message)
	{
		Assert.notNull(message, "Message can not be null");
		return addressValidationResultDtoAddressDataConverter.convertAll(Arrays.asList(message.getPayload()));
	}
}
