package com.wiley.integrations.esb.converters.populator.inventory;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import com.wiley.core.enums.ProductEditionFormat;
import com.wiley.integrations.esb.converters.populator.address.AddressModelToAddressDtoPopulator;
import com.wiley.integrations.esb.dto.common.Address;
import com.wiley.integrations.inventory.status.InventoryCheckEntryRequest;
import com.wiley.integrations.inventory.status.InventoryCheckRequest;

import org.springframework.util.Assert;


/**
 * Created by Mikhail_Asadchy on 7/27/2016.
 */
public class AbstractOrderModelToInventoryCheckRequestPopulator implements Populator<AbstractOrderModel, InventoryCheckRequest>
{
	@Resource
	private AddressModelToAddressDtoPopulator addressModelToAddressDtoPopulator;

	@Resource
	private AbstractOrderEntryModelToInventoryCheckEntryRequestPopulator
			abstractOrderEntryModelToInventoryCheckEntryRequestPopulator;

	@Override
	public void populate(@Nonnull final AbstractOrderModel source, @Nonnull final InventoryCheckRequest target)
			throws ConversionException
	{
		Assert.notNull(source);
		Assert.notNull(target);
		Assert.notNull(source.getCountry());
		Assert.notNull(source.getDeliveryMode());

		target.setCountry(source.getCountry().getIsocode());
		final Address deliveryAddress = new Address();
		addressModelToAddressDtoPopulator.populate(source.getDeliveryAddress(), deliveryAddress);
		target.setDeliveryAddress(deliveryAddress);
		target.setDeliveryOptionCode(source.getDeliveryMode().getCode());

		populateEntries(source, target);
	}

	private void populateEntries(final AbstractOrderModel source, final InventoryCheckRequest target)
	{
		final List<InventoryCheckEntryRequest> targetEntries = new ArrayList<>();
		final List<AbstractOrderEntryModel> entries = source.getEntries();
		for (final AbstractOrderEntryModel entryModel : entries) {
			final InventoryCheckEntryRequest inventoryCheckEntryRequest = new InventoryCheckEntryRequest();

			final ProductModel product = entryModel.getProduct();
			if (product.getSapProductCode() != null
					&& product.getIsbn() != null
					&& ProductEditionFormat.DIGITAL != product.getEditionFormat())
			{
				abstractOrderEntryModelToInventoryCheckEntryRequestPopulator.populate(entryModel, inventoryCheckEntryRequest);
				targetEntries.add(inventoryCheckEntryRequest);
			}
		}
		target.setEntries(targetEntries);
	}
}
