package com.wiley.integrations.esb.converters.populator.order;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.CoreAlgorithms;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.util.Assert;

import com.paypal.hybris.model.PaypalPaymentInfoModel;
import com.wiley.core.enums.PaymentModeEnum;
import com.wiley.core.model.SchoolModel;
import com.wiley.core.order.WileyCheckoutService;
import com.wiley.core.regCode.service.Wileyb2cRegCodeService;
import com.wiley.integrations.esb.dto.common.CreditCardType;
import com.wiley.integrations.esb.dto.common.PaymentOptionCode;
import com.wiley.integrations.esb.dto.order.OrderRequest;


/**
 * Populates simple fields of {@link OrderRequest} from {@link OrderModel}
 */
public class OrderModelToOrderRequestSimpleFieldsPopulator implements Populator<OrderModel, OrderRequest>
{

	@Resource
	private Wileyb2cRegCodeService wileyb2cRegCodeService;

	@Resource
	private WileyCheckoutService wileyCheckoutService;

	@Resource
	private Map<String, PaymentModeEnum> paymentModeMap;

	@Override
	public void populate(@Nonnull final OrderModel orderModel, @Nonnull final OrderRequest orderRequest)
			throws ConversionException
	{
		Assert.notNull(orderModel);
		Assert.notNull(orderRequest);

		Assert.isTrue(orderModel.getUser() instanceof CustomerModel);
		final CustomerModel customerModel = (CustomerModel) orderModel.getUser();
		Assert.notNull(customerModel);

		orderRequest.setCode(orderModel.getCode());

		populateUserId(orderRequest, customerModel);

		orderRequest.setUserGuest(CustomerType.GUEST.equals(customerModel.getType()));

		orderRequest.setImmutableUserId(customerModel.getCustomerID());

		populateSapAccountNumber(orderModel, orderRequest);

		populateSalesModel(orderModel, orderRequest);

		populateWebsite(orderModel, orderRequest);

		orderRequest.setOrderType(orderModel.getOrderType().toString());

		populateSchool(orderModel, orderRequest);

		populateRegCode(orderModel, orderRequest);

		populateCountry(orderModel, orderRequest);

		orderRequest.setCurrency(orderModel.getCurrency().getIsocode());

		populateLanguage(orderModel, orderRequest);

		populateDeliveryOptionCode(orderModel, orderRequest);

		populatePaymentOptionCode(orderModel, orderRequest);

		populatePaymentToken(orderModel, orderRequest);

		populatePaymentAuthorizationCode(orderModel, orderRequest);

		populateCreditCardType(orderModel, orderRequest);

		orderRequest.setTotalDiscount(orderModel.getTotalDiscounts());

		orderRequest.setDeliveryCost(orderModel.getDeliveryCost());

		orderRequest.setTotalTax(orderModel.getTotalTax());

		populateTotalPrice(orderModel, orderRequest);

		orderRequest.setDesiredShippingDate(orderModel.getDesiredShippingDate());

		populateTaxCalculationFailed(orderModel, orderRequest);

		orderRequest.setIndividualEcidNumber(customerModel.getEcidNumber());
	}

	private void populateTaxCalculationFailed(final OrderModel orderModel, final OrderRequest orderRequest)
	{
		final Boolean taxCalculated = orderModel.getTaxCalculated();
		Assert.notNull(taxCalculated, "taxCalculated is mandatory flag and cannot be null");

		boolean isTaxCalculationFailed = false;
		if (!taxCalculated && isGreaterThanZero(orderModel.getTotalPrice()))
		{
			isTaxCalculationFailed = true;
		}
		orderRequest.setTaxCalculationFailed(isTaxCalculationFailed);
	}

	private void populateTotalPrice(final OrderModel orderModel, final OrderRequest orderRequest)
	{
		final Double totalPrice = orderModel.getTotalPrice();
		final Double totalTax = orderModel.getTotalTax();

		if (totalPrice != null)
		{
			double totalPriceWithTax = totalPrice.doubleValue();
			if (totalTax != null)
			{
				totalPriceWithTax += totalTax.doubleValue();
			}
			orderRequest.setTotalPrice(CoreAlgorithms.round(totalPriceWithTax, 2));
		}
	}

	private void populateRegCode(final OrderModel orderModel, final OrderRequest orderRequest)
	{
		final Optional<String> optionalRegCode = wileyb2cRegCodeService.getRegCodeForOrder(orderModel);
		if (optionalRegCode.isPresent())
		{
			orderRequest.setRegCode(optionalRegCode.get());
		}
	}

	private void populateSchool(final OrderModel orderModel, final OrderRequest orderRequest)
	{
		final SchoolModel school = orderModel.getSchool();
		if (school != null)
		{
			orderRequest.setSchoolId(school.getCode());
		}
	}

	private void populateDeliveryOptionCode(final OrderModel orderModel, final OrderRequest orderRequest)
	{
		final DeliveryModeModel deliveryMode = orderModel.getDeliveryMode();
		if (deliveryMode != null)
		{
			orderRequest.setDeliveryOptionCode(deliveryMode.getCode());
		}
	}

	private void populateCountry(final OrderModel orderModel, final OrderRequest orderRequest)
	{
		final CountryModel country = orderModel.getCountry();
		Assert.notNull(country);
		orderRequest.setCountry(country.getIsocode());
	}

	private void populateWebsite(final OrderModel orderModel, final OrderRequest orderRequest)
	{
		final BaseSiteModel site = orderModel.getSite();
		if (site != null)
		{
			orderRequest.setWebsite(site.getUid());
		}
	}

	private void populateSalesModel(final OrderModel orderModel, final OrderRequest orderRequest)
	{
		final BaseSiteModel site = orderModel.getSite();
		if (site != null)
		{
			orderRequest.setSalesModel(site.getChannel().getCode());
		}
	}

	private void populateSapAccountNumber(final OrderModel orderModel, final OrderRequest orderRequest)
	{
		String sapAccountNumber = null;
		final UserModel customerModel = orderModel.getUser();
		if (customerModel instanceof B2BCustomerModel)
		{
			final B2BUnitModel unit = orderModel.getUnit();
			Assert.notNull(unit);
			sapAccountNumber = unit.getSapAccountNumber();
		}
		else
		{
			if (customerModel instanceof CustomerModel)
			{
				sapAccountNumber = ((CustomerModel) customerModel).getSapAccountNumber();
			}
		}
		orderRequest.setSapAccountNumber(sapAccountNumber);
	}

	private void populateLanguage(final OrderModel orderModel, final OrderRequest orderRequest)
	{
		final LanguageModel language = orderModel.getLanguage();
		if (language != null)
		{
			orderRequest.setLanguage(language.getIsocode());
		}
	}

	private void populateCreditCardType(final OrderModel orderModel, final OrderRequest orderRequest)
	{
		PaymentInfoModel paymentInfo = orderModel.getPaymentInfo();
		if (paymentInfo instanceof CreditCardPaymentInfoModel
				&& PaymentModeEnum.CARD.equals(paymentModeMap.get(paymentInfo.getItemtype())))
		{
			orderRequest.setCreditCardType(
					CreditCardType.valueOf(((CreditCardPaymentInfoModel) orderModel.getPaymentInfo()).getType().toString()));
		}
	}

	private void populatePaymentAuthorizationCode(final OrderModel orderModel, final OrderRequest orderRequest)
	{
		String paymentAuthorizationCode = null;
		// in case zero price order (e.g. voucher with 100% discount was applied) authorization code can't be found
		if (wileyCheckoutService.isNonZeroOrder(orderModel))
		{
			if (orderModel.getPaymentInfo() instanceof CreditCardPaymentInfoModel)
			{
				paymentAuthorizationCode = findPaymentAuthorizationCodeForCard(orderModel);

			}
			else if (orderModel.getPaymentInfo() instanceof PaypalPaymentInfoModel)
			{
				paymentAuthorizationCode = findPaymentAuthorizationCodeForPaypal(orderModel);
			}
		}
		orderRequest.setPaymentAuthorizationCode(paymentAuthorizationCode);
	}

	private String findPaymentAuthorizationCodeForPaypal(final OrderModel orderModel)
	{
		final String paymentAuthorizationCode;
		final Optional<PaymentTransactionEntryModel> optionalPaymentTransactionEntryModel =
				orderModel.getPaymentTransactions().stream()
						.filter(Objects::nonNull)
						.filter(transaction -> CollectionUtils.isNotEmpty(transaction.getEntries()))
						.flatMap(transaction -> transaction.getEntries().stream())
						.filter(entry -> PaymentTransactionType.AUTHORIZATION.equals(entry.getType()))
						.findFirst();

		final PaymentTransactionEntryModel paymentTransactionEntryModel = optionalPaymentTransactionEntryModel.orElseThrow(
				() -> new IllegalStateException(String.format("AUTHORIZATION Payment entry transaction wasn't found for order %s",
						orderModel.getCode())));
		paymentAuthorizationCode = paymentTransactionEntryModel.getRequestId();
		return paymentAuthorizationCode;
	}

	private String findPaymentAuthorizationCodeForCard(final OrderModel orderModel)
	{
		final String paymentAuthorizationCode;
		final Optional<PaymentTransactionEntryModel> optionalPaymentTransactionEntryModel =
				orderModel.getPaymentTransactions().stream()
						.filter(Objects::nonNull)
						.filter(transaction -> CollectionUtils.isNotEmpty(transaction.getEntries()))
						.flatMap(transaction -> transaction.getEntries().stream())
						.filter(entry -> PaymentTransactionType.AUTHORIZATION.equals(entry.getType())
								&& TransactionStatus.ACCEPTED.equals(TransactionStatus.valueOf(entry.getTransactionStatus())))
						.findFirst();

		final PaymentTransactionEntryModel paymentTransactionEntryModel = optionalPaymentTransactionEntryModel.orElseThrow(
				() -> new IllegalStateException(String.format("AUTHORIZATION Payment entry transaction wasn't found for order %s",
						orderModel.getCode())));
		paymentAuthorizationCode = paymentTransactionEntryModel.getWpgAuthCode();
		return paymentAuthorizationCode;
	}

	private void populatePaymentOptionCode(final OrderModel orderModel, final OrderRequest orderRequest)
	{
		if (isGreaterThanZero(orderModel.getTotalPrice()))
		{
			final String itemType = orderModel.getPaymentInfo().getItemtype();
			final PaymentModeEnum paymentMode = paymentModeMap.get(itemType);

			if (paymentMode == null)
			{
				throw new IllegalArgumentException(String.format("Could not find PaymentMode for PaymentInfo type [%s]",
						itemType));
			}

			PaymentOptionCode paymentOptionCode;
			switch (paymentMode)
			{
				case CARD:
					paymentOptionCode = PaymentOptionCode.CARD;
					break;
				case INVOICE:
					paymentOptionCode = PaymentOptionCode.INVOICE;
					break;
				case PAYPAL:
					paymentOptionCode = PaymentOptionCode.PAYPAL;
					break;
				default:
					throw new IllegalArgumentException(
							String.format("Could not find [%s] for payment mode [%s].", PaymentOptionCode.class.getName(),
									paymentMode));
			}
			orderRequest.setPaymentOptionCode(paymentOptionCode);
		}
	}

	private void populatePaymentToken(final OrderModel orderModel, final OrderRequest orderRequest)
	{
		if (orderModel.getPaymentInfo() instanceof CreditCardPaymentInfoModel)
		{
			orderRequest.setPaymentToken(((CreditCardPaymentInfoModel) orderModel.getPaymentInfo()).getSubscriptionId());
		}
		else if (orderModel.getPaymentInfo() instanceof PaypalPaymentInfoModel)
		{
			orderRequest.setPaymentToken(((PaypalPaymentInfoModel) orderModel.getPaymentInfo()).getToken());
		}
	}

	private void populateUserId(final OrderRequest orderRequest, final CustomerModel customerModel)
	{
		if (CustomerType.GUEST.equals(customerModel.getType()))
		{
			orderRequest.setUserId(customerModel.getContactEmail());
		}
		else
		{
			orderRequest.setUserId(customerModel.getUid());
		}
	}

	private boolean isGreaterThanZero(final Double totalPrice)
	{
		return totalPrice != null && totalPrice.compareTo(0.0d) > 0;
	}
}
