package com.wiley.integrations.tax.transformer;

import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.springframework.messaging.Message;

import com.wiley.core.externaltax.dto.TaxCalculationRequestDto;
import com.wiley.integrations.tax.dto.TaxCalculationRequest;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


public class TaxCalculationRequestTransformer
{
	@Resource
	private Converter<TaxCalculationRequestDto, TaxCalculationRequest> taxCalculationRequestConverter;

	@Nonnull
	public TaxCalculationRequest transform(@Nonnull final Message<TaxCalculationRequestDto> message)
	{
		validateParameterNotNullStandardMessage("message", message);
		return taxCalculationRequestConverter.convert(message.getPayload());
	}
}
