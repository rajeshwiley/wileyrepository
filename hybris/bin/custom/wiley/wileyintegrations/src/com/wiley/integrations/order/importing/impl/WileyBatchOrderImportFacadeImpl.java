package com.wiley.integrations.order.importing.impl;

import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.validation.exceptions.HybrisConstraintViolation;
import de.hybris.platform.validation.services.ValidationService;

import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Stopwatch;
import com.wiley.integrations.alm.strategy.AlmUserAutoCreationStrategy;
import com.wiley.integrations.order.dto.CreateOrderRequestWsDTO;
import com.wiley.integrations.order.dto.OrderWsDTO;
import com.wiley.integrations.order.importing.WileyBatchOrderImportFacade;
import com.wiley.integrations.order.importing.WileyOrderImportFacade;

import static com.wiley.core.constants.WileyCoreConstants.ALM_AUTO_CREATION_STRATEGY;


public class WileyBatchOrderImportFacadeImpl implements WileyBatchOrderImportFacade
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyBatchOrderImportFacadeImpl.class);
	@Autowired
	private ObjectMapper jacksonObjectMapper;

	@Autowired
	private WileyOrderImportFacade wileyOrderImportFacade;

	@Autowired
	private AlmUserAutoCreationStrategy almUserAutoCreationStrategy;

	@Autowired
	@Qualifier("wileyasUserService")
	private UserService userService;

	@Autowired
	private ValidationService validationService;

	@Override
	public List<String> importOrdersFromStream(final InputStream ordersStream, final String baseSiteId,
			final String userCreationStrategy)
			throws IOException
	{
		JsonParser parser = jacksonObjectMapper.getFactory().createParser(ordersStream);
		if (parser.nextToken() != JsonToken.START_ARRAY)
		{
			throw new IllegalArgumentException("Invalid file content. Data should be JSON array");
		}

		List<String> errors = new LinkedList<>();
		while (parser.nextToken() == JsonToken.START_OBJECT)
		{

			CreateOrderRequestWsDTO dto = jacksonObjectMapper.readValue(parser, CreateOrderRequestWsDTO.class);
			try
			{
				Stopwatch stopwatch = Stopwatch.createStarted();
				validateOrderDto(dto);
				String userId = dto.getUserId();

				if (shouldCreateUser(userCreationStrategy, userId))
				{
					LOG.info("User '{}' is not found. Will be created using strategy '{}'", userId,
							userCreationStrategy);
					almUserAutoCreationStrategy.createNewUserByAlmId(dto.getUserId());
				}
				OrderWsDTO importedOrder = wileyOrderImportFacade.importOrder(dto, baseSiteId);

				LOG.info("Imported order {} in {} ms", importedOrder.getId(),
						stopwatch.elapsed(TimeUnit.MILLISECONDS));
			}
			catch (Exception exc)
			{
				LOG.error("Unable to import order: " + dto.getId() + " Reason: " + exc.getMessage(), exc);
				errors.add(dto.getId() + "," + exc.getMessage());
			}
		}
		return errors;

	}

	private void validateOrderDto(final CreateOrderRequestWsDTO dto)
	{
		Set<HybrisConstraintViolation> violations = validationService.validate(dto);
		if (CollectionUtils.isNotEmpty(violations))
		{
			List<String> messages = violations
					.stream()
					.map(v -> "Invalid value: '"
							+ v.getConstraintViolation().getInvalidValue() + "' for field: " + v.getQualifier())
					.collect(Collectors.toList());
			throw new IllegalArgumentException(StringUtils.join(messages, "; "));
		}
	}

	private boolean shouldCreateUser(final String userCreationStrategy, final String userId)
	{
		return ALM_AUTO_CREATION_STRATEGY.equals(userCreationStrategy)
				&& StringUtils.isNotBlank(userId) && !userService.isUserExisting(userId);
	}
}
