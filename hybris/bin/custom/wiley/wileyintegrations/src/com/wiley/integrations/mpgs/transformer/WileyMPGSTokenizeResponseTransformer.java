package com.wiley.integrations.mpgs.transformer;



import de.hybris.platform.acceleratorservices.payment.cybersource.enums.CardTypeEnum;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;

import com.wiley.core.mpgs.dto.json.Card;
import com.wiley.core.mpgs.dto.token.MPGSTokenResponseDTO;
import com.wiley.core.mpgs.enums.MPGSSupportedCardType;
import com.wiley.core.mpgs.response.WileyTokenizationResponse;
import com.wiley.core.mpgs.services.WileyTransformationService;


public class WileyMPGSTokenizeResponseTransformer
{
	private static final int MONTH_START_INDEX = 0;
	private static final int MONTH_END_INDEX = 2;
	private static final int YEAR_START_INDEX = 2;

	@Autowired
	private WileyTransformationService wileyTransformationService;

	public WileyTokenizationResponse transform(@Nonnull final Message<MPGSTokenResponseDTO> message)
	{
		WileyTokenizationResponse tokenizationResult  = new WileyTokenizationResponse();
		MPGSTokenResponseDTO responseDto = message.getPayload();

		tokenizationResult.setStatus(wileyTransformationService.transformStatusIfSuccessful(responseDto.getResult()));

		if (responseDto.getResponse() != null)
		{
			tokenizationResult.setStatusDetails(responseDto.getResponse().getGatewayCode());
		}

		Card card = responseDto.getSourceOfFunds().getProvided().getCard();
		tokenizationResult.setCardNumber(card.getNumber());
		tokenizationResult.setCardScheme(transformCardSchemeName(card.getScheme()));
		tokenizationResult.setCardExpiryYear(card.getExpiry().substring(YEAR_START_INDEX));
		tokenizationResult.setCardExpiryMonth(card.getExpiry().substring(MONTH_START_INDEX, MONTH_END_INDEX));

		tokenizationResult.setToken(responseDto.getToken());

		return tokenizationResult;
	}

	private String transformCardSchemeName(@Nonnull final String cardScheme)
	{
		ServicesUtil.validateParameterNotNull(cardScheme, "[cardScheme] can't be null.");
		return cardScheme.equalsIgnoreCase(MPGSSupportedCardType.MASTER.getStringValue()) ?
				CardTypeEnum.master.name() :
				cardScheme;
	}

}