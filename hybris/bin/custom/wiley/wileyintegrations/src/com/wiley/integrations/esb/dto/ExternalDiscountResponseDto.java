package com.wiley.integrations.esb.dto;

import java.util.Locale;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.common.base.MoreObjects;
import com.wiley.integrations.adapters.xml.CommonUSDoubleAdapter;


/**
 * DTO contains information about applied external discounts, promotions and vouchers.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ExternalDiscountResponseDto
{
	private Map<Locale, String> message;

	@XmlJavaTypeAdapter(CommonUSDoubleAdapter.class)
	private Double appliedDiscountAmount;

	public Map<Locale, String> getMessage()
	{
		return message;
	}

	public void setMessage(final Map<Locale, String> message)
	{
		this.message = message;
	}

	public Double getAppliedDiscountAmount()
	{
		return appliedDiscountAmount;
	}

	public void setAppliedDiscountAmount(final Double appliedDiscountAmount)
	{
		this.appliedDiscountAmount = appliedDiscountAmount;
	}

	@Override
	public String toString()
	{
		return MoreObjects.toStringHelper(this)
				.add("message", message)
				.add("appliedDiscountAmount", appliedDiscountAmount)
				.toString();
	}
}
