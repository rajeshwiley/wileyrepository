package com.wiley.integrations.esb.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.common.base.MoreObjects;
import com.wiley.integrations.adapters.xml.CommonUSDoubleAdapter;


/**
 * DTO which indicates that a new entry should be added to cart.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AddOrderEntryModificationResponseDto extends AbstractOrderEntryModificationResponseDto
{

	@XmlElement(required = true)
	private String productCode;

	@XmlElement(required = true)
	private Long quantity;

	@XmlElement(required = true)
	@XmlJavaTypeAdapter(CommonUSDoubleAdapter.class)
	private Double productPrice;

	@XmlElement(required = true)
	@XmlJavaTypeAdapter(CommonUSDoubleAdapter.class)
	private Double totalPrice;

	@XmlElement(required = true)
	private String unitCode;

	private List<ExternalDiscountResponseDto> externalDiscounts;

	public String getProductCode()
	{
		return productCode;
	}

	public void setProductCode(final String productCode)
	{
		this.productCode = productCode;
	}

	public Long getQuantity()
	{
		return quantity;
	}

	public void setQuantity(final Long quantity)
	{
		this.quantity = quantity;
	}

	public Double getProductPrice()
	{
		return productPrice;
	}

	public void setProductPrice(final Double productPrice)
	{
		this.productPrice = productPrice;
	}

	public Double getTotalPrice()
	{
		return totalPrice;
	}

	public void setTotalPrice(final Double totalPrice)
	{
		this.totalPrice = totalPrice;
	}

	public String getUnitCode()
	{
		return unitCode;
	}

	public void setUnitCode(final String unitCode)
	{
		this.unitCode = unitCode;
	}

	public List<ExternalDiscountResponseDto> getExternalDiscounts()
	{
		return externalDiscounts;
	}

	public void setExternalDiscounts(final List<ExternalDiscountResponseDto> externalDiscounts)
	{
		this.externalDiscounts = externalDiscounts;
	}

	@Override
	public String toString()
	{
		return MoreObjects.toStringHelper(this)
				.add("productCode", productCode)
				.add("quantity", quantity)
				.add("productPrice", productPrice)
				.add("totalPrice", totalPrice)
				.add("unitCode", unitCode)
				.add("externalDiscounts", externalDiscounts)
				.toString();
	}
}
