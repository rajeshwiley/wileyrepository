package com.wiley.integrations.sabrix.transformer.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import com.wiley.core.model.ExternalCompanyModel;
import com.wiley.integrations.sabrix.dto.IndataInvoiceType;
import com.wiley.integrations.sabrix.dto.IndataType;
import com.wiley.integrations.sabrix.dto.TaxCalculationRequest;
import com.wiley.integrations.sabrix.dto.VersionType;

import org.apache.log4j.Logger;
import org.springframework.util.Assert;


/**
 * @author Dzmitryi_Halahayeu
 */
public class OrderToTaxCalculationRequestPopulator implements Populator<AbstractOrderModel, TaxCalculationRequest>
{

	private static final String COMPANY_ROLE = "S";
	private static final Logger LOG = Logger.getLogger(OrderToTaxCalculationRequestPopulator.class.getName());
	@Resource
	private OrdersItemsToIndataInvoiceTypePopulator ordersItemsToIndataInvoiceTypePopulator;
	@Resource
	private InvoiceNumberToIndataInvoiceTypePopulator invoiceNumberToIndataInvoiceTypePopulator;
	private String hostSystem;
	private String callingSystemNumber;

	@Override
	public void populate(final AbstractOrderModel order, final TaxCalculationRequest taxCalculationRequest)
			throws ConversionException
	{
		IndataType indata = new IndataType();
		taxCalculationRequest.setINDATA(indata);
		indata.setVersion(VersionType.G);
		indata.setCOMPANYROLE(COMPANY_ROLE);
		indata.setHOSTSYSTEM(hostSystem);
		indata.setCALLINGSYSTEMNUMBER(callingSystemNumber);

		Map<String, List<AbstractOrderEntryModel>> orderEntriesByExternalCompanyIds =
				order.getEntries().stream().collect(
						Collectors.groupingBy(abstractOrderEntryModel -> {
							ProductModel productModel = abstractOrderEntryModel.getProduct();
							ExternalCompanyModel externalCompany = productModel.getExternalCompany();
							Assert.notNull(externalCompany, "Could find external company for product " + productModel.getCode()
									+ ". External company is required for tax calculation");
							return externalCompany.getExternalId();
						}));
		int invoiceNumber = 0;
		for (List<AbstractOrderEntryModel> externalCompanyIdOrderEntriesPair : orderEntriesByExternalCompanyIds
				.values())
		{
			invoiceNumber++;
			IndataInvoiceType indataInvoiceType = new IndataInvoiceType();
			invoiceNumberToIndataInvoiceTypePopulator.populate(invoiceNumber, indataInvoiceType);
			ordersItemsToIndataInvoiceTypePopulator.populate(externalCompanyIdOrderEntriesPair, indataInvoiceType);
			indata.getINVOICE().add(indataInvoiceType);
		}
	}

	public void setHostSystem(final String hostSystem)
	{
		this.hostSystem = hostSystem;
	}

	public void setCallingSystemNumber(final String callingSystemNumber)
	{
		this.callingSystemNumber = callingSystemNumber;
	}
}
