package com.wiley.integrations.failedordersexport.integration;

import com.wiley.core.model.WileyOrderProcessModel;
import de.hybris.platform.store.BaseStoreModel;

import java.time.LocalDateTime;
import java.util.Collection;

public interface WileyFailedOrderProcessesProvisionStrategy {
    Collection<WileyOrderProcessModel> getFailedOrders(BaseStoreModel baseStore,
                                                       LocalDateTime from, LocalDateTime to);
}
