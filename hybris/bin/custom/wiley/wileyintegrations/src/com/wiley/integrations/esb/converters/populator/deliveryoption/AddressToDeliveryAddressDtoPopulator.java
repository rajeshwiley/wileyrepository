package com.wiley.integrations.esb.converters.populator.deliveryoption;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.annotation.Nonnull;

import org.springframework.util.Assert;

import com.wiley.integrations.esb.dto.delivery.DeliveryAddressDto;


public class AddressToDeliveryAddressDtoPopulator implements Populator<AddressModel, DeliveryAddressDto>
{
	@Override
	public void populate(@Nonnull final AddressModel address, @Nonnull final DeliveryAddressDto deliveryAddressDto)
			throws ConversionException
	{
		Assert.notNull(address);
		Assert.notNull(deliveryAddressDto);
		
		deliveryAddressDto.setId(address.getPk().toString());
		deliveryAddressDto.setPostcode(address.getPostalcode());
		deliveryAddressDto.setCountry(address.getCountry().getIsocode());
		if (address.getRegion() != null)
		{
			deliveryAddressDto.setState(address.getRegion().getIsocodeShort());
		}
		deliveryAddressDto.setCity(address.getTown());
		deliveryAddressDto.setLine1(address.getLine1());
		deliveryAddressDto.setLine2(address.getLine2());
	}
}
