package com.wiley.integrations.b2b.unit.dto;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * PoNumberValidationResultSwgDTO
 */
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-07-12T14:18:15.237Z")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PoNumberValidationResultSwgDTO
{

	private String poNumber = null;
	private Boolean valid = null;


	/**
	 * The purchase order number to validate.
	 **/
	public PoNumberValidationResultSwgDTO poNumber(final String poNumber)
	{
		this.poNumber = poNumber;
		return this;
	}

	@JsonProperty("poNumber")
	public String getPoNumber()
	{
		return poNumber;
	}

	public void setPoNumber(final String poNumber)
	{
		this.poNumber = poNumber;
	}


	/**
	 * True if the purchase order number was succesfully validated, false otherwise.
	 **/
	public PoNumberValidationResultSwgDTO valid(final Boolean valid)
	{
		this.valid = valid;
		return this;
	}

	@JsonProperty("valid")
	public Boolean getValid()
	{
		return valid;
	}

	public void setValid(final Boolean valid)
	{
		this.valid = valid;
	}


	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		PoNumberValidationResultSwgDTO poNumberValidationResult = (PoNumberValidationResultSwgDTO) o;
		return Objects.equals(this.poNumber, poNumberValidationResult.poNumber)
				&& Objects.equals(this.valid, poNumberValidationResult.valid);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(poNumber, valid);
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(final Object o)
	{
		if (o == null)
		{
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}

