package com.wiley.integrations.esb.transformer;

import com.wiley.core.integration.esb.EsbExternalAddressGateway;
import com.wiley.integrations.esb.dto.EsbAddressCreateRequestDto;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import org.springframework.messaging.Message;
import org.springframework.util.Assert;

import javax.annotation.Nonnull;
import java.util.List;


public class WileycomAddressRequestDtoTransformer
{

	private List<Populator<AddressModel, EsbAddressCreateRequestDto>> populators;

	@Nonnull
	public EsbAddressCreateRequestDto transform(@Nonnull final Message<AddressModel> message)
	{
		Assert.notNull(message);
		EsbAddressCreateRequestDto esbAddressCreateRequestDto = new EsbAddressCreateRequestDto();

		populateAddress(message, esbAddressCreateRequestDto);
		populateEcidNumber(message, esbAddressCreateRequestDto);

		return esbAddressCreateRequestDto;
	}

	private void populateAddress(@Nonnull final Message<AddressModel> message,
			final EsbAddressCreateRequestDto esbAddressCreateRequestDto)
	{
		AddressModel addressModel = message.getPayload();
		for (Populator<AddressModel, EsbAddressCreateRequestDto> populator : populators)
		{
			populator.populate(addressModel, esbAddressCreateRequestDto);
		}
	}

	private void populateEcidNumber(@Nonnull final Message<AddressModel> message,
			final EsbAddressCreateRequestDto esbAddressCreateRequestDto)
	{
		CustomerModel customerModel = (CustomerModel) message.getHeaders().get(
				EsbExternalAddressGateway.EXTERNAL_ADDRESS_CUSTOMER);
		if (customerModel != null) {
			esbAddressCreateRequestDto.setIndividualEcidNumber(customerModel.getEcidNumber());
		}
	}

	public List<Populator<AddressModel, EsbAddressCreateRequestDto>> getPopulators()
	{
		return populators;
	}

	public void setPopulators(final List<Populator<AddressModel, EsbAddressCreateRequestDto>> populators)
	{
		this.populators = populators;
	}
}
