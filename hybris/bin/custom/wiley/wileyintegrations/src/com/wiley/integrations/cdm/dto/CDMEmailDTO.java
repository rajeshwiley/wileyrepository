package com.wiley.integrations.cdm.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonInclude(JsonInclude.Include.NON_NULL)
public class CDMEmailDTO
{
	@JsonProperty(value = "EmailAddress")
	private String emailAddress;

	/**
	 * @return the emailAddress
	 */
	public String getEmailAddress() { return emailAddress; }

	/**
	 * @param emailAddress
	 *          the emailAddress to set
	 */
	public void setEmailAddress(final String emailAddress) { this.emailAddress = emailAddress; }

}
