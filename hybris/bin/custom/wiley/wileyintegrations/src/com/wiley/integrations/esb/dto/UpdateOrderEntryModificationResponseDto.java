package com.wiley.integrations.esb.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.common.base.MoreObjects;
import com.wiley.integrations.adapters.xml.CommonUSDoubleAdapter;


/**
 * DTO indicates that an existing cart entry should be modified.
 * Also the dto can be used to remove cart entry, if quantity set to 0.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UpdateOrderEntryModificationResponseDto extends AbstractOrderEntryModificationResponseDto
{
	private Integer entryNumber;

	private Long quantity;

	@XmlJavaTypeAdapter(CommonUSDoubleAdapter.class)
	private Double productPrice;

	@XmlJavaTypeAdapter(CommonUSDoubleAdapter.class)
	private Double totalPrice;

	private List<ExternalDiscountResponseDto> externalDiscounts;

	public Integer getEntryNumber()
	{
		return entryNumber;
	}

	public void setEntryNumber(final Integer entryNumber)
	{
		this.entryNumber = entryNumber;
	}

	public Long getQuantity()
	{
		return quantity;
	}

	public void setQuantity(final Long quantity)
	{
		this.quantity = quantity;
	}

	public Double getProductPrice()
	{
		return productPrice;
	}

	public void setProductPrice(final Double productPrice)
	{
		this.productPrice = productPrice;
	}

	public Double getTotalPrice()
	{
		return totalPrice;
	}

	public void setTotalPrice(final Double totalPrice)
	{
		this.totalPrice = totalPrice;
	}

	public List<ExternalDiscountResponseDto> getExternalDiscounts()
	{
		return externalDiscounts;
	}

	public void setExternalDiscounts(final List<ExternalDiscountResponseDto> externalDiscounts)
	{
		this.externalDiscounts = externalDiscounts;
	}

	@Override
	public String toString()
	{
		return MoreObjects.toStringHelper(this)
				.add("entryNumber", entryNumber)
				.add("quantity", quantity)
				.add("productPrice", productPrice)
				.add("totalPrice", totalPrice)
				.add("externalDiscounts", externalDiscounts)
				.toString();
	}
}
