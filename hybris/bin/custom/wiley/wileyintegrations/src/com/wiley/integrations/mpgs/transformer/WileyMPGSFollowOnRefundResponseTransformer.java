package com.wiley.integrations.mpgs.transformer;

import java.util.Currency;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;

import com.wiley.core.mpgs.dto.refund.MPGSFollowOnRefundResponseDTO;
import com.wiley.core.mpgs.response.WileyFollowOnRefundResponse;
import com.wiley.core.mpgs.services.WileyTransformationService;


public class WileyMPGSFollowOnRefundResponseTransformer
{

	@Autowired
	private WileyTransformationService wileyTransformationService;

	public WileyFollowOnRefundResponse transform(@Nonnull final Message<MPGSFollowOnRefundResponseDTO> message)
	{
		WileyFollowOnRefundResponse response = new WileyFollowOnRefundResponse();
		MPGSFollowOnRefundResponseDTO responseDTO = message.getPayload();
		response.setStatus(wileyTransformationService.transformStatusIfSuccessful(responseDTO.getResult()));
		response.setStatusDetails(responseDTO.getResponse().getGatewayCode());

		response.setRequestId(responseDTO.getTransaction().getId());
		response.setTotalAmount(responseDTO.getTransaction().getAmount());
		Currency currency = Currency.getInstance(responseDTO.getTransaction().getCurrency());
		response.setCurrency(currency);

		String transactionTime = responseDTO.getTimeOfRecord();
		if (transactionTime != null)
		{
			response.setTransactionCreatedTime(wileyTransformationService.transformStringToDate(transactionTime));
		}

		return response;
	}
}
