package com.wiley.integrations.customer.serviceactivator;

import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.HashSet;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.annotation.ServiceActivator;

import com.wiley.core.util.savedvalues.ItemModificationHistoryService;
import com.wiley.core.util.savedvalues.ItemModificationInfo;
import com.wiley.core.wileyas.user.service.WileyasUserService;
import com.wiley.integrations.users.data.UserModificationWsDto;


public class WileyUserProfileServiceActivator
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyUserProfileServiceActivator.class);

	@ServiceActivator
	public void update(final UserModificationWsDto userDto)
	{
		validateDto(userDto);
		if (!getWileyasUserService().isUserExisting(userDto.getUserId(), CustomerModel.class))
		{
			if (LOG.isWarnEnabled())
			{
				LOG.warn("Customer with id " + userDto.getUserId()
						+ " doesn't exist. Message " + userDto + " was ignored");
			}
			return;
		}

		final CustomerModel customer = (CustomerModel) getWileyasUserService().getUserForUID(userDto.getUserId());
		/* Values of model attributes are lazy loaded, have to load them on demand (e.g. in LOG.info()) to store them in
		backoffice history table. See WileyItemModificationHistoryServiceImpl.getOriginalValue(final ItemModel model,
		final String attribute, final ModelValueHistory history) */
		LOG.info("Customer [{}, {}, {}, {}, {}] is going to be updated.", customer.getUid(), customer.getFirstName(),
				customer.getLastName(), customer.getName(), customer.getEmailAddress());

		if (userDto.getFirstName() != null)
		{
			customer.setFirstName(userDto.getFirstName());
		}
		if (userDto.getLastName() != null)
		{
			customer.setLastName(userDto.getLastName());
		}
		if (userDto.getFirstName() != null || userDto.getLastName() != null)
		{
			// clear name to make it updated by WileyCustomerPrepareInterceptor
			customer.setName(null);
		}
		if (userDto.getEmail() != null)
		{
			customer.setEmailAddress(userDto.getEmail());
		}

		final ItemModificationInfo modificationInfo =
				getWileyItemModificationHistoryService().createModificationInfo(customer);
		getModelService().save(customer);
		getWileyItemModificationHistoryService().logModifications(modificationInfo);
	}

	private void validateDto(final UserModificationWsDto dto) throws ConstraintViolationException
	{
		final ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		final Validator validator = factory.getValidator();
		final Set<ConstraintViolation<UserModificationWsDto>> violations = validator.validate(dto);
		if (violations.size() > 0)
		{
			throw new ConstraintViolationException(new HashSet<>(violations));
		}
	}

	protected WileyasUserService getWileyasUserService()
	{
		// default bean initialization doesn't work stable in this case
		return Registry.getApplicationContext()
				.getBean(WileyasUserService.class, "wileyasUserService");
	}

	protected ModelService getModelService()
	{
		// default bean initialization doesn't work stable in this case
		return Registry.getApplicationContext()
				.getBean(ModelService.class, "modelService");
	}

	protected ItemModificationHistoryService getWileyItemModificationHistoryService()
	{
		// default bean initialization doesn't work stable in this case
		return Registry.getApplicationContext()
				.getBean(ItemModificationHistoryService.class, "wileyItemModificationHistoryService");
	}
}
