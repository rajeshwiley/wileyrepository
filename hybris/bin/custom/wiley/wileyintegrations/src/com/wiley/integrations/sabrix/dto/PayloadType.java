package com.wiley.integrations.sabrix.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PayloadType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="PayloadType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Payload" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PayloadType", namespace = "http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1", propOrder = {
		"payload"
})
public class PayloadType
{

	@XmlElement(name = "Payload", required = true)
	protected Object payload;

	/**
	 * Gets the value of the payload property.
	 *
	 * @return possible object is
	 * {@link Object }
	 */
	public Object getPayload()
	{
		return payload;
	}

	/**
	 * Sets the value of the payload property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link Object }
	 */
	public void setPayload(Object value)
	{
		this.payload = value;
	}

}
