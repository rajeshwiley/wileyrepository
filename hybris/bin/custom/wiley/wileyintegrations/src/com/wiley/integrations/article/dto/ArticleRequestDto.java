package com.wiley.integrations.article.dto;

import java.util.Objects;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;


@JsonInclude(JsonInclude.Include.NON_NULL)
public class ArticleRequestDto
{
	@JsonProperty(value = "id", required = true)
	@Size(max = 255)
	@NotNull
	private String id;
	@JsonProperty(value = "locale")
	@Pattern(regexp = "^[a-z]{2}(_[A-Z]{2}(_.+)?)?$")
	private String locale;

	public String getId()
	{
		return id;
	}

	public void setId(final String id)
	{
		this.id = id;
	}

	public String getLocale()
	{
		return locale;
	}

	public void setLocale(final String locale)
	{
		this.locale = locale;
	}

	@Override
	public String toString()
	{
		return MoreObjects.toStringHelper(this)
				.add("id", id)
				.add("locale", locale)
				.toString();
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		final ArticleRequestDto that = (ArticleRequestDto) o;
		return Objects.equals(id, that.id)
				&& Objects.equals(locale, that.locale);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(id, locale);
	}
}