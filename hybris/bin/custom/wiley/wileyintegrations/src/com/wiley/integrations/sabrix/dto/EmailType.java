package com.wiley.integrations.sabrix.dto;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EmailType.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="EmailType"&gt;
 *   &lt;restriction base="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1}StringType"&gt;
 *     &lt;enumeration value="PRMY_PERS_EMAIL"/&gt;
 *     &lt;enumeration value="SCNY_PERS_EMAIL"/&gt;
 *     &lt;enumeration value="PRMY_BUSN_EMAIL"/&gt;
 *     &lt;enumeration value="SCNY_BUSN_EMAIL"/&gt;
 *     &lt;enumeration value="RAW_BUSN_EMAIL"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 */
@XmlType(name = "EmailType", namespace = "http://xmlns.wiley.com/EnterpriseBusinessObjects/CommonInfratypes/Codelist/V1")
@XmlEnum
public enum EmailType
{

	PRMY_PERS_EMAIL,
	SCNY_PERS_EMAIL,
	PRMY_BUSN_EMAIL,
	SCNY_BUSN_EMAIL,
	RAW_BUSN_EMAIL;

	public String value()
	{
		return name();
	}

	public static EmailType fromValue(String v)
	{
		return valueOf(v);
	}

}
