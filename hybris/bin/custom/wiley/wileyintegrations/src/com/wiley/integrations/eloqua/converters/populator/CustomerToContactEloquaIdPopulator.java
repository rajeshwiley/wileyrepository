package com.wiley.integrations.eloqua.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.annotation.Nonnull;

import org.springframework.util.Assert;

import com.wiley.core.integration.eloqua.dto.ContactDto;


/**
 * Populates eloqua id.
 *
 * @author Aliaksei_Zlobich
 */
public class CustomerToContactEloquaIdPopulator implements Populator<CustomerModel, ContactDto>
{
	@Override
	public void populate(@Nonnull final CustomerModel customer, @Nonnull final ContactDto contact) throws ConversionException
	{
		Assert.notNull(customer);
		Assert.notNull(contact);

		contact.setId(customer.getEloquaId());
	}
}
