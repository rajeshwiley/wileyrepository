package com.wiley.integrations.b2b.unit.dto;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * An address.
 */
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-07-26T11:08:47.854Z")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AddressSwgDTO
{
	private String addressId = null;
	private String postcode = null;
	private String country = null;
	private String state = null;
	private String city = null;
	private String line1 = null;
	private String line2 = null;
	private String firstName = null;
	private String lastName = null;
	private String phoneNumber = null;


	/**
	 * Unique address identifier provided by SAP ERP.
	 **/
	public AddressSwgDTO addressId(final String addressId)
	{
		this.addressId = addressId;
		return this;
	}

	@JsonProperty("addressId")
	public String getAddressId()
	{
		return addressId;
	}

	public void setAddressId(final String addressId)
	{
		this.addressId = addressId;
	}


	/**
	 * Postal code.
	 **/
	public AddressSwgDTO postcode(final String postcode)
	{
		this.postcode = postcode;
		return this;
	}

	@JsonProperty("postcode")
	public String getPostcode()
	{
		return postcode;
	}

	public void setPostcode(final String postcode)
	{
		this.postcode = postcode;
	}


	/**
	 * Two-letter ISO 3166-1 alpha-2 code that specifies a country.
	 * See https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2 for details.
	 * For example, US
	 **/
	public AddressSwgDTO country(final String country)
	{
		this.country = country;
		return this;
	}

	@JsonProperty("country")
	public String getCountry()
	{
		return country;
	}

	public void setCountry(final String country)
	{
		this.country = country;
	}


	/**
	 * Two-letter ISO code that specifies a state or province.
	 * The codes were derived from ISO 3166-2 codes, for non-standard Armed Forces regions values were provided by Wiley.
	 * See https://jira.wiley.ru/browse/ECSC-5892 for details.
	 * For example, CA
	 **/
	public AddressSwgDTO state(final String state)
	{
		this.state = state;
		return this;
	}

	@JsonProperty("state")
	public String getState()
	{
		return state;
	}

	public void setState(final String state)
	{
		this.state = state;
	}


	/**
	 * Destination city.
	 **/
	public AddressSwgDTO city(final String city)
	{
		this.city = city;
		return this;
	}

	@JsonProperty("city")
	public String getCity()
	{
		return city;
	}

	public void setCity(final String city)
	{
		this.city = city;
	}


	/**
	 * First line of the address.
	 **/
	public AddressSwgDTO line1(final String line1)
	{
		this.line1 = line1;
		return this;
	}

	@JsonProperty("line1")
	public String getLine1()
	{
		return line1;
	}

	public void setLine1(final String line1)
	{
		this.line1 = line1;
	}


	/**
	 * Second line of the address.
	 **/
	public AddressSwgDTO line2(final String line2)
	{
		this.line2 = line2;
		return this;
	}

	@JsonProperty("line2")
	public String getLine2()
	{
		return line2;
	}

	public void setLine2(final String line2)
	{
		this.line2 = line2;
	}


	/**
	 **/
	public AddressSwgDTO firstName(final String firstName)
	{
		this.firstName = firstName;
		return this;
	}

	@JsonProperty("firstName")
	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(final String firstName)
	{
		this.firstName = firstName;
	}


	/**
	 **/
	public AddressSwgDTO lastName(final String lastName)
	{
		this.lastName = lastName;
		return this;
	}

	@JsonProperty("lastName")
	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(final String lastName)
	{
		this.lastName = lastName;
	}


	/**
	 * Phone number.
	 **/
	public AddressSwgDTO phoneNumber(final String phoneNumber)
	{
		this.phoneNumber = phoneNumber;
		return this;
	}

	@JsonProperty("phoneNumber")
	public String getPhoneNumber()
	{
		return phoneNumber;
	}

	public void setPhoneNumber(final String phoneNumber)
	{
		this.phoneNumber = phoneNumber;
	}


	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		AddressSwgDTO address = (AddressSwgDTO) o;
		return Objects.equals(this.addressId, address.addressId)
				&& Objects.equals(this.postcode, address.postcode)
				&& Objects.equals(this.country, address.country)
				&& Objects.equals(this.state, address.state)
				&& Objects.equals(this.city, address.city)
				&& Objects.equals(this.line1, address.line1)
				&& Objects.equals(this.line2, address.line2)
				&& Objects.equals(this.firstName, address.firstName)
				&& Objects.equals(this.lastName, address.lastName)
				&& Objects.equals(this.phoneNumber, address.phoneNumber);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(addressId, postcode, country, state, city, line1, line2, firstName, lastName, phoneNumber);
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("class Address {\n");

		sb.append("    addressId: ").append(toIndentedString(addressId)).append("\n");
		sb.append("    postcode: ").append(toIndentedString(postcode)).append("\n");
		sb.append("    country: ").append(toIndentedString(country)).append("\n");
		sb.append("    state: ").append(toIndentedString(state)).append("\n");
		sb.append("    city: ").append(toIndentedString(city)).append("\n");
		sb.append("    line1: ").append(toIndentedString(line1)).append("\n");
		sb.append("    line2: ").append(toIndentedString(line2)).append("\n");
		sb.append("    firstName: ").append(toIndentedString(firstName)).append("\n");
		sb.append("    lastName: ").append(toIndentedString(lastName)).append("\n");
		sb.append("    phoneNumber: ").append(toIndentedString(phoneNumber)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(final Object o)
	{
		if (o == null)
		{
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}

