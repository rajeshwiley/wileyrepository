package com.wiley.integrations.ediintegration.endpoints.jaxb;

import de.hybris.platform.store.BaseStoreModel;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.xml.bind.annotation.XmlElement;

import org.apache.commons.lang3.StringUtils;

import com.wiley.core.integration.edi.EDIConstants;


/**
 * Routing jaxb bean, also we should consider Routing as source of metadata info during Export chain.
 */
public class Routing
{
	private static final String RECEIVER_XML_ELEMENT_NAME = "ReceiverSAN";
	private static final String SENDER_SAN_XML_ELEMENT_NAME = "SenderSAN";
	private static final String TRANSACTION_BATCH_NUMBER_XML_ELEMENT_NAME = "TransactionBatchNumber";
	private static final String DATE_XML_ELEMENT_NAME = "Date";
	private static final String TIME_XML_ELEMENT_NAME = "Time";

	@XmlElement(name = RECEIVER_XML_ELEMENT_NAME)
	private String receiver;
	@XmlElement(name = TRANSACTION_BATCH_NUMBER_XML_ELEMENT_NAME)
	private String transactionBatchNumber;
	@XmlElement(name = DATE_XML_ELEMENT_NAME)
	private String date;
	@XmlElement(name = TIME_XML_ELEMENT_NAME)
	private String time;

	private LocalDateTime localDate;
	private String siteShortcut;
	private boolean isPayPalPayments;

	static final String DATE_FORMAT = "yyyy-MM-dd";
	static final String TIME_FORMAT = "HH:mm:ss";
	static final String DEFAULT_RECEIVER = "WILEY";


	public Routing()
	{
	}

	public Routing(final LocalDateTime localDate, final BaseStoreModel store, final String transactionBatchNumber,
			final String paymentType)
	{
		this.localDate = localDate;
		this.transactionBatchNumber = transactionBatchNumber;
		this.siteShortcut = store.getUid();
		this.date = DateTimeFormatter.ofPattern(DATE_FORMAT).format(localDate);
		this.isPayPalPayments = StringUtils.equalsIgnoreCase(paymentType, EDIConstants.PAYMENT_TYPE_PAYPAL);
		time = DateTimeFormatter.ofPattern(TIME_FORMAT).format(localDate);
		receiver = DEFAULT_RECEIVER;
	}

	public String getReceiver()
	{
		return receiver;
	}

	@XmlElement(name = SENDER_SAN_XML_ELEMENT_NAME)
	public String getSenderSAN()
	{
		return buildSenderSanName();
	}

	public String getTransactionBatchNumber()
	{
		return transactionBatchNumber;
	}


	public String getDate()
	{
		return date;
	}

	public String getTime()
	{
		return time;
	}

	public LocalDateTime getLocalDate() {
		return localDate;
	}

	public String getSiteShortcut()
	{
		return siteShortcut;
	}

	private String buildSenderSanName() {
		String senderSanValue = "HYB_" + siteShortcut.toUpperCase();
		if (isPayPalPayments()) {
			senderSanValue += "_" + EDIConstants.PAYMENT_TYPE_PAYPAL;
		}
		return senderSanValue;
	}

	public boolean isPayPalPayments()
	{
		return isPayPalPayments;
	}
}
