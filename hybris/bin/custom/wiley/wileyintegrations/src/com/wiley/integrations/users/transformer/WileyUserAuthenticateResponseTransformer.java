package com.wiley.integrations.users.transformer;

import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import com.wiley.integrations.users.data.WileyUserAuthenticateResponse;

public class WileyUserAuthenticateResponseTransformer
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyUserAuthenticateResponseTransformer.class);

	@Nonnull
	public Boolean transform(@Nonnull final WileyUserAuthenticateResponse wileyUserAuthenticateResponse)
	{
		Assert.notNull(wileyUserAuthenticateResponse);

		return isPasswordValid(wileyUserAuthenticateResponse);
	}

	private Boolean isPasswordValid(final WileyUserAuthenticateResponse response)
	{
		return response.isPasswordValid();
	}
}
