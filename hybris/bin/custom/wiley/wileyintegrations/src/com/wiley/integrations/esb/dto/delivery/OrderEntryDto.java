package com.wiley.integrations.esb.dto.delivery;

import java.util.Objects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Created by Uladzimir_Barouski on 6/16/2016.
 */
@XmlRootElement(name = "entries")
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderEntryDto
{
	@JsonProperty("productCode")
	private String productCode;
	@JsonProperty("quantity")
	private Integer quantity;
	@JsonProperty("shipFrom")
	private DeliveryAddressDto shipFrom;
	@JsonProperty("isbn")
	private String isbn;

	public String getProductCode()
	{
		return productCode;
	}

	public void setProductCode(final String productCode)
	{
		this.productCode = productCode;
	}

	public Integer getQuantity()
	{
		return quantity;
	}

	public void setQuantity(final Integer quantity)
	{
		this.quantity = quantity;
	}

	public DeliveryAddressDto getShipFrom()
	{
		return shipFrom;
	}

	public void setShipFrom(final DeliveryAddressDto shipFrom)
	{
		this.shipFrom = shipFrom;
	}

	public String getIsbn()
	{
		return isbn;
	}

	public void setIsbn(final String isbn)
	{
		this.isbn = isbn;
	}

	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this);
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		final OrderEntryDto that = (OrderEntryDto) o;
		return Objects.equals(productCode, that.productCode)
				&& Objects.equals(quantity, that.quantity)
				&& Objects.equals(isbn, that.isbn)
				&& Objects.equals(shipFrom, that.shipFrom);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(productCode, quantity, shipFrom, isbn);
	}
}
