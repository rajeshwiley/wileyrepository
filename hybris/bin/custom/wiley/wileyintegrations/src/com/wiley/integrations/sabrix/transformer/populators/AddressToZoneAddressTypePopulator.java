package com.wiley.integrations.sabrix.transformer.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.Set;

import javax.annotation.Nonnull;

import com.wiley.integrations.sabrix.dto.ZoneAddressType;

import org.springframework.stereotype.Component;


/**
 * @author Dzmitryi_Halahayeu
 */
@Component
public class AddressToZoneAddressTypePopulator implements Populator<AddressModel, ZoneAddressType>
{
	static final String US_ISO_CODE = "US";
	static final String CANADA_ISO_CODE = "CA";

	private Set<String> statesToCountiesSet;

	@Override
	public void populate(@Nonnull final AddressModel addressModel, @Nonnull final ZoneAddressType zoneAddressType)
			throws ConversionException
	{
		String isoCode = addressModel.getCountry().getIsocode();
		zoneAddressType.setCOUNTRY(isoCode);
		if (addressModel.getRegion() != null)
		{
			String stateCode = addressModel.getRegion().getIsocodeShort();
			if (isoCode.equals(CANADA_ISO_CODE))
			{
				zoneAddressType.setPROVINCE(stateCode);
			}
			if (isoCode.equals(US_ISO_CODE))
			{
				if (statesToCountiesSet.contains(stateCode))
				{
					zoneAddressType.setCOUNTRY(stateCode);
				}
				else
				{
					zoneAddressType.setSTATE(stateCode);
				}
			}
		}
		zoneAddressType.setCITY(addressModel.getTown());
		zoneAddressType.setPOSTCODE(addressModel.getPostalcode());
	}

	public void setStatesToCountiesSet(final Set<String> statesToCountiesSet)
	{
		this.statesToCountiesSet = statesToCountiesSet;
	}
}
