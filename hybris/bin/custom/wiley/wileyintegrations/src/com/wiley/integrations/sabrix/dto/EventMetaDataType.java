package com.wiley.integrations.sabrix.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Event meta data for data service broadcast event
 *
 * <p>Java class for EventMetaDataType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="EventMetaDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="EventType" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/CommonInfratypes/Codelist/V1}ActionType" minOccurs="0"/&gt;
 *         &lt;element name="EventObject" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1}StringType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EventMetaDataType", namespace = "http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/Meta/V1", propOrder = {
		"eventType",
		"eventObject"
})
public class EventMetaDataType
{

	@XmlElement(name = "EventType")
	@XmlSchemaType(name = "string")
	protected ActionType eventType;
	@XmlElement(name = "EventObject")
	protected String eventObject;

	/**
	 * Gets the value of the eventType property.
	 *
	 * @return possible object is
	 * {@link ActionType }
	 */
	public ActionType getEventType()
	{
		return eventType;
	}

	/**
	 * Sets the value of the eventType property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link ActionType }
	 */
	public void setEventType(ActionType value)
	{
		this.eventType = value;
	}

	/**
	 * Gets the value of the eventObject property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getEventObject()
	{
		return eventObject;
	}

	/**
	 * Sets the value of the eventObject property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setEventObject(String value)
	{
		this.eventObject = value;
	}

}
