package com.wiley.integrations.promotioncode;

/**
 * Provides promotion code for Eloqua system.
 * Please see
 * <a href="https://confluence.wiley.ru/display/ECSC/IDD%3A+Eloqua+integration">IDD: Eloqua integration</a>
 * for more details.
 */
public class EloquaPromotionCodeProviderImpl extends AbstractPromotionCodeProviderImpl
{
	/**
	 * According to business requirements "student" discount should not be sent to Eloqua
	 *
	 * @return if "student" discount should be included
	 */
	@Override
	protected boolean includeStudent()
	{
		return false;
	}
}
