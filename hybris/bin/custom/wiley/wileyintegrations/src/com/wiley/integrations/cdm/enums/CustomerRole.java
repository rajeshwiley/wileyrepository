package com.wiley.integrations.cdm.enums;

public enum CustomerRole
{
	INDIVIDUAL("individual"), STUDENT("student"), B2B_USER("b2buser"), B2B_ADMIN("b2badmin"), PROFESSOR("professor");

	private final String name;

	CustomerRole(final String name)
	{
		this.name = name;
	}

	@Override
	public String toString()
	{
		return this.name;
	}
}
