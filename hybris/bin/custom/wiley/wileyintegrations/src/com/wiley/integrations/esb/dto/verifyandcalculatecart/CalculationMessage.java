package com.wiley.integrations.esb.dto.verifyandcalculatecart;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.apache.commons.lang3.builder.ToStringBuilder;



/**
 * Description of an event occured on the order calculation.
 * The optional productCode field is used to reference the order entry that was modified as a result of the calculation event.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CalculationMessage
{

	/**
	 * Gets or Sets messageType
	 */
	public enum MessageTypeEnum
	{
		INFO,
		WARNING,
		ERROR
	}

	/**
	 * Code of the calculation event.
	 **/
	private String code;
	/**
	 * Message type
	 */
	private MessageTypeEnum messageType;
	/**
	 * Displayable description of the calculation event.
	 * The message should be localized using the provided in the request language and country.
	 **/
	private String message;

	/**
	 * A unique alphanumeric value that identifies the product item across all the systems.
	 */
	private String sapProductCode;

	/**
	 * ISBN-13. See: https://en.wikipedia.org/wiki/International_Standard_Book_Number
	 */
	private String isbn;

	@JsonProperty("code")
	public String getCode()
	{
		return code;
	}

	public void setCode(final String code)
	{
		this.code = code;
	}

	@JsonProperty("message_type")
	public MessageTypeEnum getMessageType()
	{
		return messageType;
	}

	public void setMessageType(final MessageTypeEnum messageType)
	{
		this.messageType = messageType;
	}

	@JsonProperty("message")
	public String getMessage()
	{
		return message;
	}

	public void setMessage(final String message)
	{
		this.message = message;
	}

	@JsonProperty("sapProductCode")
	public String getSapProductCode()
	{
		return sapProductCode;
	}

	public void setSapProductCode(final String sapProductCode)
	{
		this.sapProductCode = sapProductCode;
	}

	@JsonProperty("isbn")
	public String getIsbn()
	{
		return isbn;
	}

	public void setIsbn(final String isbn)
	{
		this.isbn = isbn;
	}

	@Override
	public String toString()
	{
		return new ToStringBuilder(this)
				.append("code", code)
				.append("messageType", messageType)
				.append("message", message)
				.append("sapProductCode", sapProductCode)
				.append("isbn", isbn)
				.toString();
	}
}

