package com.wiley.integrations.esb.transformer.lightningsource;

import javax.annotation.Nonnull;

import org.apache.commons.lang3.StringUtils;
import org.springframework.messaging.Message;

import com.wiley.core.exceptions.ExternalSystemException;
import com.wiley.integrations.lightningsource.LightningSourceResponseDto;


/**
 * Created by Georgii_Gavrysh on 9/8/2016.
 */
public class LightningSourceResponseDtoToStringTransformer
{
	public String transform(@Nonnull final Message<LightningSourceResponseDto> lightningSourceResponseDtoMessage)
	{
		final LightningSourceResponseDto lightningSourceResponseDto = lightningSourceResponseDtoMessage.getPayload();
		if (StringUtils.isNotEmpty(lightningSourceResponseDto.getErrorMessage()))
		{
			throw new ExternalSystemException("Response Error field is not empty.");
		}
		return lightningSourceResponseDto.getIPWSeBookDownloadURL();
	}
}
