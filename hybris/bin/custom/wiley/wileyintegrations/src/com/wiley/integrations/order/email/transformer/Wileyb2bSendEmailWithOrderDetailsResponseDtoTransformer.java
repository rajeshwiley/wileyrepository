package com.wiley.integrations.order.email.transformer;

import javax.annotation.Nonnull;

import com.wiley.integrations.order.email.dto.OrderDetailsResponseDto;

import org.springframework.messaging.Message;
import org.springframework.util.Assert;


/**
 * Spring integration transformer which transforms {@code Message} payload, an {@code OrderDetailsResponseDto}
 * to a boolean value by using its {@code requestProcessed} attribute's value.
 */
public class Wileyb2bSendEmailWithOrderDetailsResponseDtoTransformer
{
	public Boolean transform(@Nonnull final Message<OrderDetailsResponseDto> message)
	{
		Assert.notNull(message);

		return message.getPayload().getRequestProcessed();
	}
}
