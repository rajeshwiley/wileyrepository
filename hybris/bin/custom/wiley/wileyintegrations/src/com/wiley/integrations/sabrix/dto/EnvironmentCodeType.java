package com.wiley.integrations.sabrix.dto;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EnvironmentCodeType.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="EnvironmentCodeType"&gt;
 *   &lt;restriction base="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1}StringType"&gt;
 *     &lt;enumeration value="DEV"/&gt;
 *     &lt;enumeration value="SIT"/&gt;
 *     &lt;enumeration value="UAT"/&gt;
 *     &lt;enumeration value="QA"/&gt;
 *     &lt;enumeration value="PROD"/&gt;
 *     &lt;enumeration value="DR"/&gt;
 *     &lt;enumeration value="NA"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 */
@XmlType(name = "EnvironmentCodeType", namespace = "http://xmlns.wiley.com/EnterpriseBusinessObjects/CommonInfratypes/Codelist/V1")
@XmlEnum
public enum EnvironmentCodeType
{

	DEV,
	SIT,
	UAT,
	QA,
	PROD,
	DR,
	NA;

	public String value()
	{
		return name();
	}

	public static EnvironmentCodeType fromValue(String v)
	{
		return valueOf(v);
	}

}
