package com.wiley.integrations.order.populator;

import de.hybris.platform.converters.Converters;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.util.PriceValue;
import de.hybris.platform.util.TaxValue;
import de.hybris.platform.variants.model.VariantProductModel;

import javax.annotation.Resource;

import com.wiley.core.model.WileyExternalDiscountModel;
import com.wiley.integrations.order.dto.DiscountValueWsDTO;
import com.wiley.integrations.order.dto.OrderEntryWsDTO;
import com.wiley.integrations.order.dto.PriceValueWsDTO;
import com.wiley.integrations.order.dto.TaxValueWsDTO;

import static com.wiley.integrations.utils.PopulateUtils.populateFieldIfNotNull;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


public class WileyOrderEntryWsPopulator implements Populator<AbstractOrderEntryModel, OrderEntryWsDTO>
{
	@Resource
	private Converter<DiscountValue, DiscountValueWsDTO> wileyDiscountValueWsConverter;
	@Resource
	private Converter<WileyExternalDiscountModel, DiscountValueWsDTO> wileyExternalDiscountWsConverter;
	@Resource
	private Converter<PriceValue, PriceValueWsDTO> wileyPriceValueWsConverter;
	@Resource
	private Converter<TaxValue, TaxValueWsDTO> wileyTaxValueWsConverter;

	@Override
	public void populate(final AbstractOrderEntryModel source, final OrderEntryWsDTO target)
			throws ConversionException
	{
		validateParameterNotNullStandardMessage("source", source);
		validateParameterNotNullStandardMessage("target", target);

		target.setEntryId(source.getGuid());
		populateFieldIfNotNull((dest, order) ->
						populateFieldIfNotNull((dest2, user) -> dest2.setUserId(user.getUid()), dest, order.getUser()),
				target, source.getOrder());
		populateBaseProduct(source, target);
		target.setDeliveryDate(source.getNamedDeliveryDate());
		target.setAdditionalInfo(source.getAdditionalInfo());
		target.setBusinessKey(source.getOriginalBusinessKey());
		target.setBasePrice(source.getBasePrice());
		target.setSubtotalPrice(source.getSubtotalPrice());
		target.setTotalPrice(source.getTotalPrice());
		target.setTaxableTotalPrice(source.getTaxableTotalPrice());
		target.setBusinessItemId(source.getBusinessItemId());
		target.setStatusModificationTime(source.getStatusModifiedTime());
		target.setModificationTime(source.getModifiedtime());


		populateFieldIfNotNull((dest, value) -> dest.setOrderId(value.getGuid()), target, source.getOrder());
		populateFieldIfNotNull((dest, value) -> dest.setStatus(value.getCode()), target, source.getStatus());
		populateFieldIfNotNull((dest, value) -> dest.setProductCode(value.getCode()), target, source.getProduct());

		populateFieldIfNotNull((dest, value) -> dest.setQuantity(value.intValue()), target, source.getQuantity());
		populateFieldIfNotNull((dest, value) -> dest.setCancelReason(value.getCode()), target, source.getCancelReason());

		populateFieldIfNotNull((dest, value) ->
						dest.setDiscounts(Converters.convertAll(value, wileyDiscountValueWsConverter)),
				target, source.getDiscountValues());

		populateFieldIfNotNull((dest, value) ->
						dest.setExternalDiscounts(Converters.convertAll(value, wileyExternalDiscountWsConverter)),
				target, source.getExternalDiscounts());

		populateFieldIfNotNull((dest, value) ->
						dest.setTaxes(Converters.convertAll(value, wileyTaxValueWsConverter)),
				target, source.getTaxValues());

		populateFieldIfNotNull((dest, value) ->
						dest.setExternalPrices(Converters.convertAll(value, wileyPriceValueWsConverter)),
				target, source.getExternalPriceValues());

	}

	private void populateBaseProduct(final AbstractOrderEntryModel source, final OrderEntryWsDTO target)
	{
		final ProductModel product = source.getProduct();
		if (product instanceof VariantProductModel)
		{
			target.setBaseProductCode(((VariantProductModel) product).getBaseProduct().getCode());
		}
	}
}
