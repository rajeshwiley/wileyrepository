package com.wiley.integrations.tax.dto;

import java.util.Objects;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;


public class TaxAddress
{
	@JsonProperty("postcode")
	@Size(max = 255)
	private String postcode;

	@JsonProperty("country")
	@Pattern(regexp = "^[A-Z]{2}$")
	private String country;

	@JsonProperty("state")
	@Pattern(regexp = "^[0-9A-Z]{1,3}$")
	private String state;

	@JsonProperty("city")
	@Size(max = 255)
	private String city;

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		final TaxAddress that = (TaxAddress) o;
		return Objects.equals(postcode, that.postcode)
				&& Objects.equals(country, that.country)
				&& Objects.equals(state, that.state)
				&& Objects.equals(city, that.city);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(postcode, country, state, city);
	}

	/**
	 * Zip/postal code.
	 *
	 * @return postcode
	 **/
	public String getPostcode()
	{
		return postcode;
	}

	public void setPostcode(final String postcode)
	{
		this.postcode = postcode;
	}

	/**
	 * Get country
	 *
	 * @return country
	 **/
	public String getCountry()
	{
		return country;
	}

	public void setCountry(final String country)
	{
		this.country = country;
	}

	/**
	 * Get state
	 *
	 * @return state
	 **/
	public String getState()
	{
		return state;
	}

	public void setState(final String state)
	{
		this.state = state;
	}

	/**
	 * City/town.
	 *
	 * @return city
	 **/
	public String getCity()
	{
		return city;
	}

	public void setCity(final String city)
	{
		this.city = city;
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("class TaxAddress {\n");

		sb.append("    postcode: ").append(toIndentedString(postcode)).append("\n");
		sb.append("    country: ").append(toIndentedString(country)).append("\n");
		sb.append("    state: ").append(toIndentedString(state)).append("\n");
		sb.append("    city: ").append(toIndentedString(city)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(final Object o)
	{
		if (o == null)
		{
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}

