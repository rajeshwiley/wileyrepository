package com.wiley.integrations.hotfolder;

import java.io.File;
import java.util.ArrayList;

import org.springframework.integration.file.DefaultDirectoryScanner;
import org.springframework.integration.file.filters.AbstractFileListFilter;
import org.springframework.integration.file.filters.CompositeFileListFilter;
import org.springframework.integration.file.filters.FileListFilter;
import org.springframework.integration.file.filters.RegexPatternFileListFilter;


/**
 * Created by Georgii_Gavrysh on 8/17/2016.
 */
public class DefaultWileycomDirectoryScanner extends DefaultDirectoryScanner
{

	public DefaultWileycomDirectoryScanner(final String pattern)
	{
		ArrayList<FileListFilter<File>> filters = new ArrayList<>(2);
		filters.add(new NotEmptyAndCanReadFileListFilter());
		filters.add(new RegexPatternFileListFilter(pattern));
		setFilter(new CompositeFileListFilter<>(filters));
	}

	private static class NotEmptyAndCanReadFileListFilter extends AbstractFileListFilter<File>
	{
		@Override
		protected boolean accept(final File file)
		{
			return file.length() != 0 && file.canWrite();
		}
	}
}
