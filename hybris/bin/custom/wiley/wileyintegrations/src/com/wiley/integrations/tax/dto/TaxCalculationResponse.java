package com.wiley.integrations.tax.dto;

import java.util.List;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;


public class TaxCalculationResponse
{
	@JsonProperty("entries")
	@NotNull
	@Valid
	private List<TaxCalculationResponseEntry> entries;

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		final TaxCalculationResponse that = (TaxCalculationResponse) o;
		return Objects.equals(entries, that.entries)
				&& Objects.equals(handlingTaxes, that.handlingTaxes)
				&& Objects.equals(shippingTaxes, that.shippingTaxes);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(entries, handlingTaxes, shippingTaxes);
	}

	@JsonProperty("handlingTaxes")
	@NotNull
	@Valid
	private List<TaxValue> handlingTaxes;

	@JsonProperty("shippingTaxes")
	@NotNull
	@Valid
	private List<TaxValue> shippingTaxes;

	/**
	 * List of the tax estimates for the taxable cart/order entries.
	 *
	 * @return entries
	 **/
	public List<TaxCalculationResponseEntry> getEntries()
	{
		return entries;
	}

	public void setEntries(final List<TaxCalculationResponseEntry> entries)
	{
		this.entries = entries;
	}

	/**
	 * List of the tax values calculated for the handling service.
	 *
	 * @return handlingTaxes
	 **/
	public List<TaxValue> getHandlingTaxes()
	{
		return handlingTaxes;
	}

	public void setHandlingTaxes(final List<TaxValue> handlingTaxes)
	{
		this.handlingTaxes = handlingTaxes;
	}

	/**
	 * List of the tax values calculated for the shipping service.
	 *
	 * @return shippingTaxes
	 **/
	public List<TaxValue> getShippingTaxes()
	{
		return shippingTaxes;
	}

	public void setShippingTaxes(final List<TaxValue> shippingTaxes)
	{
		this.shippingTaxes = shippingTaxes;
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("class TaxCalculationResponse {\n");

		sb.append("    entries: ").append(toIndentedString(entries)).append("\n");
		sb.append("    handlingTaxes: ").append(toIndentedString(handlingTaxes)).append("\n");
		sb.append("    shippingTaxes: ").append(toIndentedString(shippingTaxes)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(final Object o)
	{
		if (o == null)
		{
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}

