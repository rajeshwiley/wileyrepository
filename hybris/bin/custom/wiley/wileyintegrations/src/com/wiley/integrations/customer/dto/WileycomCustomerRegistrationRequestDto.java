package com.wiley.integrations.customer.dto;

import java.util.Objects;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class WileycomCustomerRegistrationRequestDto
{
	/**
	 * Enumeration value that specifies the user's role.
	 *
	 * 'individual' - a regular B2C customer,
	 * 'student' - a student account,
	 * 'b2buser' - a regular B2B  customer,
	 * 'b2badmin'- a B2B admin,
	 * 'professor' is reserved for future.
	 */
	public enum RoleEnum
	{
		INDIVIDUAL("individual"),
		STUDENT("student"),
		B2BADMIN("b2badmin"),
		B2BUSER("b2buser"),
		PROFESSOR("professor");

		private String value;

		RoleEnum(final String value)
		{
			this.value = value;
		}

		@Override
		public String toString()
		{
			return String.valueOf(value);
		}
	}

	@NotNull
	@Size(min = 1)
	private String immutableId = null;

	@NotNull
	@Size(min = 1)
	private String userId = null;

	@NotNull
	@Size(min = 1)
	private String password = null;

	@NotNull
	@Size(min = 1)
	private String email = null;

	@NotNull
	@Size(min = 1)
	private String firstName = null;

	@NotNull
	@Size(min = 1)
	private String lastName = null;

	private String middleName = null;
	private String titleCode = null;
	private String suffixCode = null;
	private RoleEnum role = null;
	private String studentId = null;
	private Boolean enrolledInSchool = null;
	private String schoolId = null;
	private String major = null;
	private Integer graduationMonth = null;
	private Integer graduationYear = null;
	private Boolean subscribeOnWileyPromos = false;
	private String organizationalSapAccountNumber = null;
	private String organizationalEcidNumber = null;

	public WileycomCustomerRegistrationRequestDto immutableId(final String immutableId)
	{
		this.immutableId = immutableId;
		return this;
	}

	/**
	 * User's unique identifier that cannot be changed. This is an internal identifier assigned by Hybris.
	 *
	 * @return immutableId
	 **/
	@JsonProperty(required = true, value = "immutableId")
	public String getImmutableId()
	{
		return immutableId;
	}

	public void setImmutableId(final String immutableId)
	{
		this.immutableId = immutableId;
	}

	public WileycomCustomerRegistrationRequestDto userId(final String userId)
	{
		this.userId = userId;
		return this;
	}

	/**
	 * User's unique identifier. Generally, user emails are to be used as identifiers.
	 * Nevertheless, string literals are permitted to be used as identifiers too.
	 *
	 * @return userId
	 **/
	@JsonProperty(required = true, value = "userId")
	public String getUserId()
	{
		return userId;
	}

	public void setUserId(final String userId)
	{
		this.userId = userId;
	}

	public WileycomCustomerRegistrationRequestDto password(final String password)
	{
		this.password = password;
		return this;
	}

	/**
	 * User's password.
	 *
	 * @return password
	 **/
	@JsonProperty(required = true, value = "password")
	public String getPassword()
	{
		return password;
	}

	public void setPassword(final String password)
	{
		this.password = password;
	}

	public WileycomCustomerRegistrationRequestDto email(final String email)
	{
		this.email = email;
		return this;
	}

	/**
	 * User's email.
	 *
	 * @return email
	 **/
	@JsonProperty(required = true, value = "email")
	public String getEmail()
	{
		return email;
	}

	public void setEmail(final String email)
	{
		this.email = email;
	}

	public WileycomCustomerRegistrationRequestDto titleCode(final String titleCode)
	{
		this.titleCode = titleCode;
		return this;
	}

	/**
	 * Code of user's title
	 *
	 * @return titleCode
	 **/
	@JsonProperty(value = "titleCode")
	public String getTitleCode()
	{
		return titleCode;
	}

	public void setTitleCode(final String titleCode)
	{
		this.titleCode = titleCode;
	}

	public WileycomCustomerRegistrationRequestDto firstName(final String firstName)
	{
		this.firstName = firstName;
		return this;
	}

	/**
	 * User's first name.
	 *
	 * @return firstName
	 **/
	@JsonProperty(required = true, value = "firstName")
	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(final String firstName)
	{
		this.firstName = firstName;
	}

	public WileycomCustomerRegistrationRequestDto lastName(final String lastName)
	{
		this.lastName = lastName;
		return this;
	}

	/**
	 * User's last name.
	 *
	 * @return lastName
	 **/
	@JsonProperty(required = true, value = "lastName")
	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(final String lastName)
	{
		this.lastName = lastName;
	}

	public WileycomCustomerRegistrationRequestDto middleName(final String middleName)
	{
		this.middleName = middleName;
		return this;
	}

	/**
	 * User's middle name.
	 *
	 * @return middleName
	 **/
	@JsonProperty(value = "middleName")
	public String getMiddleName()
	{
		return middleName;
	}

	public void setMiddleName(final String middleName)
	{
		this.middleName = middleName;
	}

	public WileycomCustomerRegistrationRequestDto suffixCode(final String suffixCode)
	{
		this.suffixCode = suffixCode;
		return this;
	}

	/**
	 * Code of user's suffix
	 *
	 * @return suffixCode
	 **/
	@JsonProperty(value = "suffixCode")
	public String getSuffixCode()
	{
		return suffixCode;
	}

	public void setSuffixCode(final String suffixCode)
	{
		this.suffixCode = suffixCode;
	}

	public WileycomCustomerRegistrationRequestDto role(final RoleEnum role)
	{
		this.role = role;
		return this;
	}

	/**
	 * Enumeration value that specifies the user's role.
	 *
	 * 'individual' - a regular B2C customer,
	 * 'student' - a student account,
	 * 'b2buser' - a regular B2B  customer,
	 * 'b2badmin'- a B2B admin,
	 * 'professor' is reserved for future.
	 *
	 * @return role
	 **/
	@JsonProperty(value = "role")
	public RoleEnum getRole()
	{
		return role;
	}

	public void setRole(final RoleEnum role)
	{
		this.role = role;
	}

	public WileycomCustomerRegistrationRequestDto studentId(final String studentId)
	{
		this.studentId = studentId;
		return this;
	}

	/**
	 * If customer is a student this field should contain her/his student ID
	 *
	 * @return studentId
	 **/
	@JsonProperty(value = "studentId")
	public String getStudentId()
	{
		return studentId;
	}

	public void setStudentId(final String studentId)
	{
		this.studentId = studentId;
	}

	public WileycomCustomerRegistrationRequestDto enrolledInSchool(final Boolean enrolledInSchool)
	{
		this.enrolledInSchool = enrolledInSchool;
		return this;
	}

	/**
	 * A flag that customer is currently enrolled to school
	 *
	 * @return enrolledInSchool
	 **/
	@JsonProperty(value = "enrolledInSchool")
	public Boolean getEnrolledInSchool()
	{
		return enrolledInSchool;
	}

	public void setEnrolledInSchool(final Boolean enrolledInSchool)
	{
		this.enrolledInSchool = enrolledInSchool;
	}

	public WileycomCustomerRegistrationRequestDto schoolId(final String schoolId)
	{
		this.schoolId = schoolId;
		return this;
	}

	/**
	 * ID of school
	 *
	 * @return schoolId
	 **/
	@JsonProperty(value = "schoolId")
	public String getSchoolId()
	{
		return schoolId;
	}

	public void setSchoolId(final String schoolId)
	{
		this.schoolId = schoolId;
	}

	public WileycomCustomerRegistrationRequestDto major(final String major)
	{
		this.major = major;
		return this;
	}

	/**
	 * Major of study
	 *
	 * @return major
	 **/
	@JsonProperty(value = "major")
	public String getMajor()
	{
		return major;
	}

	public void setMajor(final String major)
	{
		this.major = major;
	}

	public WileycomCustomerRegistrationRequestDto graduationMonth(final Integer graduationMonth)
	{
		this.graduationMonth = graduationMonth;
		return this;
	}

	/**
	 * month of graduation
	 * minimum: 1.0
	 * maximum: 12.0
	 *
	 * @return graduationMonth
	 **/
	@JsonProperty(value = "graduationMonth")
	public Integer getGraduationMonth()
	{
		return graduationMonth;
	}

	public void setGraduationMonth(final Integer graduationMonth)
	{
		this.graduationMonth = graduationMonth;
	}

	public WileycomCustomerRegistrationRequestDto graduationYear(final Integer graduationYear)
	{
		this.graduationYear = graduationYear;
		return this;
	}

	/**
	 * year of graduation
	 * minimum: 1900.0
	 * maximum: 3000.0
	 *
	 * @return graduationYear
	 **/
	@JsonProperty(value = "graduationYear")
	public Integer getGraduationYear()
	{
		return graduationYear;
	}

	public void setGraduationYear(final Integer graduationYear)
	{
		this.graduationYear = graduationYear;
	}

	public WileycomCustomerRegistrationRequestDto subscribeOnWileyPromos(final Boolean subscribeOnWileyPromos)
	{
		this.subscribeOnWileyPromos = subscribeOnWileyPromos;
		return this;
	}

	/**
	 * Customer has subscribed for Wiley promotions and updates by email
	 *
	 * @return subscribeOnWileyPromos
	 **/
	@JsonProperty(value = "subscribeOnWileyPromos")
	public Boolean getSubscribeOnWileyPromos()
	{
		return subscribeOnWileyPromos;
	}

	public void setSubscribeOnWileyPromos(final Boolean subscribeOnWileyPromos)
	{
		this.subscribeOnWileyPromos = subscribeOnWileyPromos;
	}

	public WileycomCustomerRegistrationRequestDto organizationalSapAccountNumber(final String organizationalSapAccountNumber)
	{
		this.organizationalSapAccountNumber = organizationalSapAccountNumber;
		return this;
	}

	/**
	 * Unique identifier of the user's B2B unit (company). Applicable only for B2B customers.
	 *
	 * @return organizationalSapAccountNumber
	 **/
	@JsonProperty(value = "organizationalSapAccountNumber")
	public String getOrganizationalSapAccountNumber()
	{
		return organizationalSapAccountNumber;
	}

	public void setOrganizationalSapAccountNumber(final String organizationalSapAccountNumber)
	{
		this.organizationalSapAccountNumber = organizationalSapAccountNumber;
	}

	public WileycomCustomerRegistrationRequestDto organizationalEcidNumber(final String organizationalEcidNumber)
	{
		this.organizationalEcidNumber = organizationalEcidNumber;
		return this;
	}

	/**
	 * ECID of the user's B2B unit (company).
	 *
	 * @return organizationalEcidNumber
	 **/
	@JsonProperty(value = "organizationalEcidNumber")
	public String getOrganizationalEcidNumber()
	{
		return organizationalEcidNumber;
	}

	public void setOrganizationalEcidNumber(final String organizationalEcidNumber)
	{
		this.organizationalEcidNumber = organizationalEcidNumber;
	}


	@Override
	public boolean equals(final java.lang.Object o)
	{
		if (this == o)
		{
			return true;
		}

		if (o == null || getClass() != o.getClass())
		{
			return false;
		}

		WileycomCustomerRegistrationRequestDto registrationForm = (WileycomCustomerRegistrationRequestDto) o;

		return Objects.equals(this.immutableId, registrationForm.immutableId)
				&& Objects.equals(this.userId, registrationForm.userId)
				&& Objects.equals(this.password, registrationForm.password)
				&& Objects.equals(this.email, registrationForm.email)
				&& Objects.equals(this.titleCode, registrationForm.titleCode)
				&& Objects.equals(this.firstName, registrationForm.firstName)
				&& Objects.equals(this.lastName, registrationForm.lastName)
				&& Objects.equals(this.middleName, registrationForm.middleName)
				&& Objects.equals(this.suffixCode, registrationForm.suffixCode)
				&& Objects.equals(this.role, registrationForm.role)
				&& Objects.equals(this.studentId, registrationForm.studentId)
				&& Objects.equals(this.enrolledInSchool, registrationForm.enrolledInSchool)
				&& Objects.equals(this.schoolId, registrationForm.schoolId)
				&& Objects.equals(this.major, registrationForm.major)
				&& Objects.equals(this.graduationMonth, registrationForm.graduationMonth)
				&& Objects.equals(this.graduationYear, registrationForm.graduationYear)
				&& Objects.equals(this.subscribeOnWileyPromos, registrationForm.subscribeOnWileyPromos)
				&& Objects.equals(this.organizationalSapAccountNumber, registrationForm.organizationalSapAccountNumber)
				&& Objects.equals(this.organizationalEcidNumber, registrationForm.organizationalEcidNumber);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(immutableId, userId, password, email, titleCode, firstName, lastName, middleName, suffixCode, role,
				studentId, enrolledInSchool, schoolId, major, graduationMonth, graduationYear, subscribeOnWileyPromos,
				organizationalSapAccountNumber, organizationalEcidNumber);
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("class RegistrationForm {\n");

		sb.append("    immutableId: ").append(toIndentedString(immutableId)).append("\n");
		sb.append("    userId: ").append(toIndentedString(userId)).append("\n");
		sb.append("    password: ").append(toIndentedString(password)).append("\n");
		sb.append("    email: ").append(toIndentedString(email)).append("\n");
		sb.append("    titleCode: ").append(toIndentedString(titleCode)).append("\n");
		sb.append("    firstName: ").append(toIndentedString(firstName)).append("\n");
		sb.append("    lastName: ").append(toIndentedString(lastName)).append("\n");
		sb.append("    middleName: ").append(toIndentedString(middleName)).append("\n");
		sb.append("    suffixCode: ").append(toIndentedString(suffixCode)).append("\n");
		sb.append("    role: ").append(toIndentedString(role)).append("\n");
		sb.append("    studentId: ").append(toIndentedString(studentId)).append("\n");
		sb.append("    enrolledInSchool: ").append(toIndentedString(enrolledInSchool)).append("\n");
		sb.append("    schoolId: ").append(toIndentedString(schoolId)).append("\n");
		sb.append("    major: ").append(toIndentedString(major)).append("\n");
		sb.append("    graduationMonth: ").append(toIndentedString(graduationMonth)).append("\n");
		sb.append("    graduationYear: ").append(toIndentedString(graduationYear)).append("\n");
		sb.append("    subscribeOnWileyPromos: ").append(toIndentedString(subscribeOnWileyPromos)).append("\n");
		sb.append("    organizationalSapAccountNumber: ").append(toIndentedString(organizationalSapAccountNumber)).append("\n");
		sb.append("    organizationalEcidNumber: ").append(toIndentedString(organizationalEcidNumber)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(final java.lang.Object o)
	{
		if (o == null)
		{
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
