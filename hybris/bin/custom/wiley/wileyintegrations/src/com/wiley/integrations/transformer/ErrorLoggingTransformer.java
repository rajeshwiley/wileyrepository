package com.wiley.integrations.transformer;

import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.ErrorMessage;
import org.springframework.util.Assert;


public class ErrorLoggingTransformer
{

	private static final Logger LOG = LoggerFactory.getLogger(ErrorLoggingTransformer.class);
	 
	@Nonnull
	public Message log(@Nonnull final Message message)
	{
		Assert.notNull(message);
		
		//assumed that we are redirected here via error-chanel, the message is of error type
		final Throwable error = ((ErrorMessage) message).getPayload();
		//parameters to be logged according to
		//https://confluence.wiley.ru/pages/viewpage.action?spaceKey=ECSC&title=P2+-+hy-OUT-031+Upload+order
		final String serverErrorCode = error.getClass().getSimpleName();
		final String serverErrorMessage = error.getMessage();
		LOG.warn("code: " + serverErrorCode + ", message: " + serverErrorMessage);
		
		return message;
	}
}
