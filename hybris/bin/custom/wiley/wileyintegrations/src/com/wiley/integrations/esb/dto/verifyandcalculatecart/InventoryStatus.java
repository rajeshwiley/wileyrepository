package com.wiley.integrations.esb.dto.verifyandcalculatecart;

import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * One entry of inventory status for one product from request
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class InventoryStatus
{

	public enum StatusEnum
	{
		IN_STOCK,
		BACK_ORDER,
		PRE_ORDER,
		PRINT_ON_DEMAND;
	}

	/**
	 * Quantity of the product in the order entry. Product pieces are considered as default measurement units.
	 */
	private Integer quantity;

	/**
	 * status of the record
	 */
	private StatusEnum status;

	/**
	 * estimated delivery date. The format is as defined 'date-time' in RFC3339
	 * ("http://xml2rfc.ietf.org/public/rfc/html/rfc3339.html#anchor14")
	 */
	private Date estimatedDeliveryDate;

	@JsonProperty("quantity")
	public Integer getQuantity()
	{
		return quantity;
	}

	public void setQuantity(final Integer quantity)
	{
		this.quantity = quantity;
	}

	@JsonProperty("status")
	public StatusEnum getStatus()
	{
		return status;
	}

	public void setStatus(final StatusEnum status)
	{
		this.status = status;
	}

	@JsonProperty("estimatedDeliveryDate")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
	public Date getEstimatedDeliveryDate()
	{
		return estimatedDeliveryDate;
	}

	public void setEstimatedDeliveryDate(final Date estimatedDeliveryDate)
	{
		this.estimatedDeliveryDate = estimatedDeliveryDate;
	}

	@Override
	public String toString()
	{
		return new ToStringBuilder(this)
				.append("quantity", quantity)
				.append("status", status)
				.append("estimatedDeliveryDate", estimatedDeliveryDate)
				.toString();
	}
}
