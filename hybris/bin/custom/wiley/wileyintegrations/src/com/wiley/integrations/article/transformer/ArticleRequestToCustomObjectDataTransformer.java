package com.wiley.integrations.article.transformer;

import de.hybris.platform.converters.Populator;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.util.Assert;

import com.wiley.integrations.article.dto.ArticleRequestDto;


public class ArticleRequestToCustomObjectDataTransformer
{
	@Autowired
	private Populator<String, ArticleRequestDto> articleRequestToCustomObjectDataPopulator;

	@Nonnull
	public ArticleRequestDto transform(@Nonnull final Message<String> message)
	{
		Assert.notNull(message, "Message can not be null");

		final String id = message.getPayload();

		final ArticleRequestDto dto = new ArticleRequestDto();
		articleRequestToCustomObjectDataPopulator.populate(id, dto);
		return dto;
	}
}
