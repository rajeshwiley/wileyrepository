package com.wiley.integrations.promotioncode;

import de.hybris.platform.core.model.order.OrderModel;

import java.util.Optional;

import javax.annotation.Nonnull;


/**
 * Provides promotion code to be sent to 3-rd party systems. Implementation could differ slightly depending on particular system.
 */
public interface PromotionCodeProvider
{
	/**
	 * Provides promotion code applied to order for 3-rd party system integrations
	 *
	 * @param orderModel order
	 * @return Optional<String> String if order has promotion applied and null otherwise
	 */
	@Nonnull
	Optional<String> getOrderPromotionCode(@Nonnull OrderModel orderModel);
}
