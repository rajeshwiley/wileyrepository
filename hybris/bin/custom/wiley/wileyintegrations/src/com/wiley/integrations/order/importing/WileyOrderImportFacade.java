package com.wiley.integrations.order.importing;

import com.wiley.integrations.order.dto.CreateOrderRequestWsDTO;
import com.wiley.integrations.order.dto.OrderWsDTO;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import org.apache.commons.lang3.tuple.Pair;

import javax.annotation.Nonnull;


public interface WileyOrderImportFacade
{
	/**
	 * Imports order and create NOT RUNNING fulfilment process.
	 * In order to start returned process de.hybris.platform.processengine.BusinessProcessService#startProcess has to
	 * be called separately
	 * @param request order create request
	 * @param baseSiteId site to create this order for
	 * @return a pair of order dto and corresponding fulfillment process
	 */
	Pair<OrderWsDTO, OrderProcessModel> importOrderAndCreateFulfilment(@Nonnull CreateOrderRequestWsDTO request,
																		@Nonnull String baseSiteId);

	/**
	 * Imports order for given site
	 * @param request order create request
	 * @param baseSiteId site to create this order for
	 * @return
	 */
	OrderWsDTO importOrder(@Nonnull CreateOrderRequestWsDTO request, @Nonnull String baseSiteId);

}