package com.wiley.integrations.common.dto;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;


/**
 * Description of a server error.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WileycomErrorResponseDto
{
	private String code = null;
	private String message = null;

	@Override
	public boolean equals(final Object obj)
	{
		if (this == obj)
		{
			return true;
		}

		if (obj == null || getClass() != obj.getClass())
		{
			return false;
		}

		WileycomErrorResponseDto error = (WileycomErrorResponseDto) obj;
		return Objects.equals(this.code, error.code) && Objects.equals(this.message, error.message);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(code, message);
	}

	@Override
	public String toString()
	{
		return MoreObjects.toStringHelper(this)
				.add("code", code)
				.add("message", message)
				.toString();
	}

	@JsonProperty(value = "message", required = true)
	public String getMessage()
	{
		return message;
	}

	public void setMessage(final String message)
	{
		this.message = message;
	}

	@JsonProperty(value = "code", required = true)
	public String getCode()
	{
		return code;
	}

	public void setCode(final String code)
	{
		this.code = code;
	}
}
