package com.wiley.integrations.common.service;

import org.springframework.messaging.Message;


/**
 * This service is responsible for rethrowing exception form Message.
 */
public class ExceptionProducibleService
{

	public void rethrowException(final Message<Exception> message) throws Exception
	{
		throw message.getPayload();
	}

}
