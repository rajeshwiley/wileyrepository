package com.wiley.integrations.mpgs.transformer;

import de.hybris.platform.servicelayer.util.ServicesUtil;

import javax.annotation.Nonnull;

import com.wiley.core.mpgs.constants.WileyMPGSConstants;
import com.wiley.core.mpgs.dto.json.Transaction;
import com.wiley.core.mpgs.dto.refund.MPGSFollowOnRefundRequestDTO;
import com.wiley.core.payment.request.WileyFollowOnRefundRequest;


public class WileyMPGSFollowOnRefundRequestTransformer
{
	public MPGSFollowOnRefundRequestDTO transform(@Nonnull final WileyFollowOnRefundRequest request)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("request", request);

		Transaction transaction = new Transaction();
		transaction.setCurrency(request.getCurrency().getCurrencyCode());
		transaction.setAmount(request.getTotalAmount());
		transaction.setReference(request.getTransactionReference());

		MPGSFollowOnRefundRequestDTO requestDTO = new MPGSFollowOnRefundRequestDTO();
		requestDTO.setTransaction(transaction);
		requestDTO.setApiOperation(WileyMPGSConstants.API_OPERATAION_REFUND);

		return requestDTO;
	}
}
