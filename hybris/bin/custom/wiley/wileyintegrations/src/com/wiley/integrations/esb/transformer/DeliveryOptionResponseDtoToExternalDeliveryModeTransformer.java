package com.wiley.integrations.esb.transformer;

import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.springframework.messaging.Message;
import org.springframework.util.Assert;

import com.wiley.core.delivery.WileyDeliveryService;
import com.wiley.core.integration.esb.dto.DeliveryOptionResponseDto;
import com.wiley.core.model.ExternalDeliveryModeModel;
import com.wiley.core.wileycom.delivery.WileycomDeliveryDao;
import com.wiley.integrations.constants.WileyintegrationsConstants;
import com.wiley.integrations.esb.converters.populator.deliveryoption.DeliveryOptionToExternalDeliveryModePopulator;
import com.wiley.integrations.esb.dto.delivery.DeliveryOptionRequestDto;


/**
 * Transforms list of deliveryOptionResponseDto to list of externalDeliveryModeModel
 */
public class DeliveryOptionResponseDtoToExternalDeliveryModeTransformer
{

	@Resource
	private DeliveryOptionToExternalDeliveryModePopulator deliveryOptionToExternalDeliveryModePopulator;

	@Resource
	private ModelService modelService;

	@Resource
	private WileyDeliveryService wileyDeliveryService;

	@Resource
	private WileycomDeliveryDao wileycomDeliveryDao;

	@Nonnull
	public List<ExternalDeliveryModeModel> transform(@Nonnull final Message<DeliveryOptionResponseDto[]> message)
	{
		Assert.notNull(message);

		final List<ExternalDeliveryModeModel> result = new ArrayList<>();

		final DeliveryOptionRequestDto deliveryOptionRequestDto = (DeliveryOptionRequestDto) message.getHeaders().get(
				WileyintegrationsConstants.Esb.CART);
		final List<DeliveryOptionResponseDto> deliveryOptionResponseDtoList = new ArrayList<>(
				Arrays.asList(message.getPayload()));

		Assert.notNull(deliveryOptionRequestDto);
		Assert.notNull(deliveryOptionResponseDtoList);

		final List<ExternalDeliveryModeModel> externalDeliveryModes = findExternalDeliveryModes(deliveryOptionResponseDtoList);

		deliveryOptionResponseDtoList.stream().forEach((deliveryOptionResponseDto) ->
		{
			final ExternalDeliveryModeModel externalDeliveryModeModel = findExternalDeliveryModeByCode(externalDeliveryModes,
					deliveryOptionResponseDto.getCode());
			deliveryOptionToExternalDeliveryModePopulator.populate(deliveryOptionResponseDto, externalDeliveryModeModel);
			externalDeliveryModeModel.setCurrency(deliveryOptionRequestDto.getCurrency());
			result.add(externalDeliveryModeModel);
		});

		modelService.saveAll(externalDeliveryModes); // update models into database for consistency (jalo layer, model layer)

		return result;
	}

	private ExternalDeliveryModeModel findExternalDeliveryModeByCode(final List<ExternalDeliveryModeModel> externalDeliveryModes,
			final String code)
	{
		for (final ExternalDeliveryModeModel externalDeliveryMode : externalDeliveryModes)
		{
			if (externalDeliveryMode.getExternalCode().equalsIgnoreCase(code))
			{
				return externalDeliveryMode;
			}
		}
		throw new RuntimeException("Cannot find external delivery mode with code " + code);
	}

	private List<ExternalDeliveryModeModel> findExternalDeliveryModes(
			final List<DeliveryOptionResponseDto> deliveryOptionResponseDtoList)
	{
		final List<String> externalDeliveryModeCodes = deliveryOptionResponseDtoList.stream().map(
				DeliveryOptionResponseDto::getCode).collect(Collectors.toList());

		return wileycomDeliveryDao.getSupportedExternalDeliveryModes(externalDeliveryModeCodes);
	}
}
