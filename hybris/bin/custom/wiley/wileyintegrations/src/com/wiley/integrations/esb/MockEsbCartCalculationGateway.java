package com.wiley.integrations.esb;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;

import java.util.Collections;
import java.util.List;

import javax.annotation.Nonnull;

import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;

import com.wiley.core.integration.ExternalCartModification;
import com.wiley.core.integration.esb.EsbCartCalculationGateway;
import com.wiley.core.model.ExternalDeliveryModeModel;


/**
 * @author Dzmitryi_Halahayeu
 */
public class MockEsbCartCalculationGateway implements EsbCartCalculationGateway
{
	@Nonnull
	@Override
	public List<ExternalCartModification> verifyAndCalculateCart(@Nonnull final CartModel cartModel)
	{
		return Collections.emptyList();
	}

	@Override
	public List<ExternalDeliveryModeModel> getExternalDeliveryModes(@Nonnull final AbstractOrderModel orderModel)
	{
		return Collections.emptyList();
	}

	@Override
	public List<PriceInformation> getProductPriceInformations(@Nonnull @Payload final ProductModel productModel,
			@Nonnull @Header(SAP_ACCOUNT_NUMBER) final String sapAccountNumber,
			@Nonnull @Header(BUSINESS_PRICE_COUNTRY) final String sessionCountry,
			@Nonnull @Header(BUSINESS_PRICE_CURRENCY) final String sessionCurrency)
	{
		return Collections.emptyList();
	}
}
