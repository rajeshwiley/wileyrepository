package com.wiley.integrations.cache;

import org.springframework.integration.handler.advice.AbstractRequestHandlerAdvice;
import org.springframework.messaging.Message;


/**
 * Used for disabling cache and circuit breaker in tests
 *
 * @author Dzmitryi_Halahayeu
 */
public class EmptyRequestHandlerAdvice extends AbstractRequestHandlerAdvice
{
	@Override
	protected Object doInvoke(final ExecutionCallback executionCallback, final Object o, final Message<?> message)
			throws Exception
	{
		return executionCallback.execute();
	}
}
