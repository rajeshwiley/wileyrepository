package com.wiley.integrations.handler;

import java.io.IOException;

import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import com.wiley.core.exceptions.ExternalSystemBadRequestException;
import com.wiley.core.exceptions.ExternalSystemClientErrorException;
import com.wiley.core.exceptions.ExternalSystemConflictException;
import com.wiley.core.exceptions.ExternalSystemInternalErrorException;
import com.wiley.core.exceptions.ExternalSystemNotFoundException;
import com.wiley.core.exceptions.ExternalSystemUnauthorizedException;


public class ExternalSystemResponseErrorHandler extends DefaultResponseErrorHandler
{
	@Override
	public void handleError(final ClientHttpResponse response) throws IOException
	{
		try
		{
			super.handleError(response);
		}
		catch (HttpClientErrorException e)
		{
			final HttpStatus statusCode = e.getStatusCode();
			final String responseBody = e.getResponseBodyAsString();
			if (HttpStatus.BAD_REQUEST.equals(statusCode))
			{
				throw new ExternalSystemBadRequestException(responseBody, e);
			}
			else if (HttpStatus.UNAUTHORIZED.equals(statusCode) || HttpStatus.FORBIDDEN.equals(statusCode))
			{
				throw new ExternalSystemUnauthorizedException(responseBody, e);
			}
			else if (HttpStatus.NOT_FOUND.equals(statusCode))
			{
				throw new ExternalSystemNotFoundException(responseBody, e);
			}
			else if (HttpStatus.CONFLICT.equals(statusCode))
			{
				throw new ExternalSystemConflictException(responseBody, e);
			}
			else
			{
				throw new ExternalSystemClientErrorException(responseBody, e);
			}
		}
		catch (HttpServerErrorException e)
		{
			throw new ExternalSystemInternalErrorException(e.getResponseBodyAsString(), e);
		}
		
	}
}