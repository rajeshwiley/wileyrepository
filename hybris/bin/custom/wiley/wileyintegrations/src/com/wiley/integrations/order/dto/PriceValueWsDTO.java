package com.wiley.integrations.order.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonInclude(JsonInclude.Include.NON_NULL)
public class PriceValueWsDTO implements Serializable
{
	@Min(0)
	@NotNull
	private Double value;

	@NotNull
	@Pattern(regexp = "^[A-Z]{3}$")
	private String currency;
	private Map<String, Object> anyProperties = new HashMap<>();

	/**
	 * Value of the price. The en_US format is used for the value.
	 * minimum: 0.0
	 **/
	@JsonProperty("value")
	public Double getValue()
	{
		return value;
	}

	public void setValue(final Double value)
	{
		this.value = value;
	}

	@JsonProperty("currency")
	public String getCurrency()
	{
		return currency;
	}

	public void setCurrency(final String currency)
	{
		this.currency = currency;
	}

	@JsonAnyGetter
	public Map<String, Object> getAnyProperties()
	{
		return anyProperties;
	}

	@JsonAnySetter
	public void setAnyProperties(final String name, final Object value)
	{
		this.anyProperties.put(name, value);
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		PriceValueWsDTO priceValue = (PriceValueWsDTO) o;
		return Objects.equals(value, priceValue.value)
				&& Objects.equals(currency, priceValue.currency)
				&& Objects.equals(anyProperties, priceValue.anyProperties);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(value, currency, anyProperties);
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("class PriceValue {\n");

		sb.append("    value: ").append(toIndentedString(value)).append("\n");
		sb.append("    currency: ").append(toIndentedString(currency)).append("\n");
		sb.append("    anyProperties: ").append(toIndentedString(anyProperties)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(final Object o)
	{
		if (o == null)
		{
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}

