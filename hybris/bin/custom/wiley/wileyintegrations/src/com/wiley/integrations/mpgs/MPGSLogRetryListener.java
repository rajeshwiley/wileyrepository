package com.wiley.integrations.mpgs;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.retry.RetryCallback;
import org.springframework.retry.RetryContext;
import org.springframework.retry.listener.RetryListenerSupport;

public class MPGSLogRetryListener extends RetryListenerSupport
{
	private static final Logger LOG = LoggerFactory.getLogger(MPGSLogRetryListener.class);

	@Override
	public <T, E extends Throwable> void onError(final RetryContext context, final RetryCallback<T, E> callback,
			final Throwable throwable)
	{
		LOG.error("Unable to contact MPGS server: {}", throwable.getMessage());
	}
}
