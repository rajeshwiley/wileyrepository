package com.wiley.integrations.order.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.wiley.core.wileyas.user.service.WileyasUserService;
import com.wiley.integrations.order.dto.CreateOrderRequestWsDTO;

import static com.wiley.integrations.utils.PopulateUtils.populateFieldIfNotNull;

/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public class WileyasOrderReverseWsPopulator implements Populator<CreateOrderRequestWsDTO, OrderModel>
{
    @Resource
    private WileyasUserService wileyasUserService;

    @Override
    public void populate(final CreateOrderRequestWsDTO source, final OrderModel target) throws ConversionException
    {
        Assert.notNull(source, "Parameter source cannot be null.");
        Assert.notNull(target, "Parameter target cannot be null.");

        populateFieldIfNotNull((dest, value) -> dest.setPlacedBy(wileyasUserService.getUserForUID(value)),
                target, source.getPlacedByAgent());
    }
}
