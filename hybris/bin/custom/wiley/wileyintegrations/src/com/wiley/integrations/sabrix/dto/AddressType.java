package com.wiley.integrations.sabrix.dto;

import java.util.Objects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AddressType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="AddressType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.sabrix.com/services/taxcalculationservice/2011-09-01}CommonAddressType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ALL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AddressType", propOrder = {
		"all"
})
public class AddressType
		extends CommonAddressType
{

	@XmlElement(name = "ALL")
	protected String all;

	/**
	 * Gets the value of the all property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getALL()
	{
		return all;
	}

	/**
	 * Sets the value of the all property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setALL(String value)
	{
		this.all = value;
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		if (!super.equals(o))
		{
			return false;
		}
		final AddressType that = (AddressType) o;
		return Objects.equals(all, that.all);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(super.hashCode(), all);
	}
}
