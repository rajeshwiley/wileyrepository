package com.wiley.integrations.ediintegration.endpoints.jaxb;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class DoubleAdapter extends XmlAdapter<String, Double>
{
	@Override
	public Double unmarshal(final String v) throws Exception
	{
		return null;
	}

	@Override
	public String marshal(final Double value) throws Exception
	{
		return String.format("%1$,.2f", value == null ? new Double(0.00) : value);
	}
}
