package com.wiley.integrations.esb.converters.populator.order;

import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.util.DiscountValue;

import java.util.Collections;

import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import com.wiley.core.product.WileyProductService;
import com.wiley.core.regCode.exception.RegCodeInvalidException;
import com.wiley.core.regCode.exception.RegCodeValidationException;
import com.wiley.integrations.esb.dto.RegistrationCodeValidationResponseDto;


public class RegistrationCodeResponseToCartPopulator implements Populator<RegistrationCodeValidationResponseDto, CartModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(RegistrationCodeResponseToCartPopulator.class);

	private static final double HUNDRED_PERCENT = 100.0;

	@Autowired
	private WileyProductService wileyProductService;

	private CommerceCartService commerceCartService;

	@Override
	public void populate(@Nonnull final RegistrationCodeValidationResponseDto regCodeDto, @Nonnull final CartModel cartModel)
			throws ConversionException
	{
		Assert.notNull(regCodeDto);
		Assert.notNull(cartModel);

		if (regCodeDto.isBurned())
		{
			throw new RegCodeInvalidException(String.format(
					"Requested registration code %s is already burned and cannot be applied", regCodeDto.getCode()));
		}
		// In case of product by isbn not found we have UnknownIdentifierException as a root-cause
		// In case of something wrong with cart we have CommerceCartModificationException as a root-cause
		addProductToCart(cartModel, regCodeDto.getIsbn());
		applyDiscountToCart(cartModel, regCodeDto.getCode());
		setRegCodeToCart(cartModel, regCodeDto.getCode());
	}

	private void addProductToCart(final CartModel cart, final String isbn)
	{

		try
		{
			final ProductModel product = wileyProductService.getProductForIsbn(isbn);

			if (product == null)
			{
				throw new RegCodeInvalidException(String.format("Couldn't find product with isbn [%s]", isbn));
			}
			final CommerceCartParameter parameter = new CommerceCartParameter();
			parameter.setEnableHooks(true);
			parameter.setCart(cart);
			parameter.setQuantity(1L);
			parameter.setProduct(product);
			parameter.setUnit(product.getUnit());
			parameter.setCreateNewEntry(false);

			commerceCartService.addToCart(parameter);
		}
		catch (CommerceCartModificationException e)
		{
			throw new RegCodeValidationException(
					String.format("Error occurred while trying to add RegCode product with isbn [%s] to cart [%s]", isbn,
							cart.getCode()), e);
		}
		catch (UnknownIdentifierException e)
		{
			throw new RegCodeInvalidException(String.format("Couldn't find product with isbn [%s]", isbn), e);
		}

	}

	private void applyDiscountToCart(final CartModel cartModel, final String regCode)
	{
		DiscountValue discount = new DiscountValue(regCode, HUNDRED_PERCENT, false, null);
		cartModel.setGlobalDiscountValues(Collections.singletonList(discount));
	}

	private void setRegCodeToCart(final CartModel cartModel, final String regCode)
	{
		cartModel.setRegistrationCode(regCode);
	}

	@Required
	public void setCommerceCartService(final CommerceCartService commerceCartService)
	{
		this.commerceCartService = commerceCartService;
	}

}
