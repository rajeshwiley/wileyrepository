package com.wiley.integrations.order.populator;

import de.hybris.platform.basecommerce.enums.CancelReason;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.util.PriceValue;
import de.hybris.platform.util.TaxValue;

import java.util.List;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.Assert;

import com.wiley.core.model.WileyExternalDiscountModel;
import com.wiley.core.order.service.WileyOrderEntryService;
import com.wiley.core.wiley.restriction.WileyProductRestrictionService;
import com.wiley.core.wiley.restriction.exception.ProductNotVisibleException;
import com.wiley.integrations.order.dto.CreateOrderEntryRequestWsDTO;
import com.wiley.integrations.order.dto.DiscountValueWsDTO;
import com.wiley.integrations.order.dto.PriceValueWsDTO;
import com.wiley.integrations.order.dto.TaxValueWsDTO;

import static com.wiley.integrations.utils.PopulateUtils.populateFieldIfNotNull;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public class WileyasOrderEntryReverseWsPopulator implements Populator<CreateOrderEntryRequestWsDTO, OrderEntryModel>
{
	@Resource
	private ProductService productService;

	@Resource(name = "wileyasOrderEntryService")
	private WileyOrderEntryService wileyOrderEntryService;

	@Resource
	private Converter<TaxValueWsDTO, TaxValue> wileyasTaxValueReverseWsConverter;

	@Resource
	private Converter<PriceValueWsDTO, PriceValue> wileyasPriceValueReverseWsConverter;

	@Resource
	private Converter<DiscountValueWsDTO, WileyExternalDiscountModel> wileyExternalDiscountReverseWsConverter;

	@Resource
	private WileyProductRestrictionService wileyProductRestrictionService;

	@Resource
	private EnumerationService enumerationService;

	@Override
	public void populate(final CreateOrderEntryRequestWsDTO source, final OrderEntryModel target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		if (StringUtils.isBlank(source.getEntryId()))
		{
			target.setGuid(UUID.randomUUID().toString());
		}
		else
		{
			if (wileyOrderEntryService.isOrderEntryExistsForGuid(source.getEntryId()))
			{
				throw new AmbiguousIdentifierException("OrderEntry with id '" + source.getEntryId() + "' was found!");
			}
			target.setGuid(source.getEntryId());
		}
		populateFieldIfNotNull((dest, value) -> dest.setStatus(
				enumerationService.getEnumerationValue(OrderStatus.class, value)), target, source.getStatus());
		target.setProduct(findVisibleProductByCode(source.getProductCode()));
		target.setBusinessItemId(source.getBusinessItemId());
		if (!OrderStatus.CANCELLED.getCode().equals(source.getStatus()))
		{
			target.setBusinessKey(source.getBusinessKey());
		}
		target.setOriginalBusinessKey(source.getBusinessKey());
		populateFieldIfNotNull((dest, value) -> dest.setQuantity(Long.valueOf(value)), target, source.getQuantity());
		final List<PriceValue> priceValues = Converters.convertAll(source.getExternalPrices(),
				wileyasPriceValueReverseWsConverter);
		target.setExternalPriceValues(priceValues);
		populateFieldIfNotNull(
				(dest, value) -> dest
						.setExternalDiscounts(Converters.convertAll(value, wileyExternalDiscountReverseWsConverter)),
				target, source.getExternalDiscounts());
		populateFieldIfNotNull(
				(dest, value) -> dest.setTaxValues(Converters.convertAll(value, wileyasTaxValueReverseWsConverter)), target,
				source.getTaxes());
		target.setTotalPrice(source.getTotalPrice());
		target.setTaxableTotalPrice(source.getTaxableTotalPrice());
		target.setNamedDeliveryDate(source.getDeliveryDate());
		populateFieldIfNotNull((dest, value) -> dest.setCancelReason(
				enumerationService.getEnumerationValue(CancelReason.class, value)), target, source.getCancelReason());

		target.setAdditionalInfo(source.getAdditionalInfo());
	}

	private ProductModel findVisibleProductByCode(final String code)
	{
		final ProductModel product = productService.getProductForCode(code);
		if (!wileyProductRestrictionService.isAvailable(product))
		{
			throw new ProductNotVisibleException(
					"Product [" + product.getCode() + "] is not available because of Offline/Online date", null, null);
		}
		return product;
	}

}
