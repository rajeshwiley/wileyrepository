package com.wiley.integrations.eloqua.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.subscriptionservices.enums.SubscriptionStatus;
import de.hybris.platform.subscriptionservices.model.SubscriptionProductModel;
import de.hybris.platform.util.DiscountValue;

import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.wiley.core.model.WileySubscriptionModel;
import com.wiley.core.subscription.services.WileySubscriptionService;
import com.wiley.core.integration.eloqua.dto.CustomObjectDataDto;
import com.wiley.core.integration.eloqua.dto.FieldValueDto;
import com.wiley.integrations.eloqua.util.CustomObjectDataUtils;


/**
 * Populates CustomObjectData from OrderEntry and product which exists in this order entry.
 *
 * @author Aliaksei_Zlobich
 */
public class OrderEntryToCustomObjectDataPopulator implements Populator<OrderEntryModel, CustomObjectDataDto>
{

	public static final int PURCHASE_PRICE_FIELD_VALUE = 5269;
	public static final int PRODUCT_CODE_FIELD_VALUE = 5265;
	public static final int PRODUCT_NAME_FIELD_VALUE = 5267;
	public static final int RENEWAL_DATE_FIELD_VALUE = 5271;
	public static final int NET_PRICE_FIELD_VALUE = 13373;
	public static final int DISCOUNT_VALUE_FIELD_VALUE = 13376;

	@Resource
	private WileySubscriptionService wileySubscriptionService;

	@Override
	public void populate(@Nonnull final OrderEntryModel orderEntry, @Nonnull final CustomObjectDataDto cdo)
			throws ConversionException
	{
		Assert.notNull(orderEntry);
		Assert.notNull(cdo);

		final ProductModel product = orderEntry.getProduct();
		final List<FieldValueDto> fieldValues = cdo.getFieldValues();
		addPurchasePriceFieldValue(fieldValues, orderEntry);
		addProductCodeFieldValue(fieldValues, product);
		addProductNameFieldValue(fieldValues, product);
		addRenewalDateFieldValue(fieldValues, orderEntry);
		addNetPriceFieldValue(fieldValues, orderEntry);
		addDiscountValueFieldValue(fieldValues, orderEntry);
	}

	private void addPurchasePriceFieldValue(final List<FieldValueDto> fieldValues, final OrderEntryModel orderEntry)
	{

		final Double basePrice = orderEntry.getBasePrice();
		final Long quantity = orderEntry.getQuantity();
		if (basePrice != null && quantity != null)
		{
			final double purchasePrice = basePrice * quantity;
			fieldValues.add(CustomObjectDataUtils.createFieldValue(PURCHASE_PRICE_FIELD_VALUE, String.valueOf(purchasePrice)));
		}
	}

	private void addProductCodeFieldValue(final List<FieldValueDto> fieldValues, final ProductModel product)
	{
		fieldValues.add(CustomObjectDataUtils.createFieldValue(PRODUCT_CODE_FIELD_VALUE, product.getCode()));
	}

	private void addProductNameFieldValue(final List<FieldValueDto> fieldValues, final ProductModel product)
	{
		final String name = product.getName();
		if (name != null)
		{
			fieldValues.add(CustomObjectDataUtils.createFieldValue(PRODUCT_NAME_FIELD_VALUE, name));
		}
	}

	private void addRenewalDateFieldValue(final List<FieldValueDto> fieldValues, final OrderEntryModel orderEntry)
	{
		final OrderModel order = orderEntry.getOrder();
		final ProductModel product = orderEntry.getProduct();

		if (product instanceof SubscriptionProductModel)
		{
			final WileySubscriptionModel subscription = wileySubscriptionService.getSubscription(order.getUser(),
					SubscriptionStatus.ACTIVE);

			if (subscription != null)
			{
				if (!product.equals(subscription.getProduct()))
				{
					throw new IllegalStateException(
							String.format("Subscription [%s] is not assigned to SubscriptionProduct [%s].",
									subscription.getCode(), product.getCode()));
				}

				if (subscription.getExpirationDate() != null)
				{
					int expirationDate = CustomObjectDataUtils.toUnixTime(subscription.getExpirationDate());
					fieldValues.add(
							CustomObjectDataUtils.createFieldValue(RENEWAL_DATE_FIELD_VALUE, String.valueOf(expirationDate)));
				}
			}
		}
	}
	
	private void addNetPriceFieldValue(final List<FieldValueDto> fieldValues, final OrderEntryModel orderEntry)
	{
		fieldValues.add(CustomObjectDataUtils.createFieldValue(NET_PRICE_FIELD_VALUE,
				String.valueOf(orderEntry.getTotalPrice())));
	}

	private void addDiscountValueFieldValue(final List<FieldValueDto> fieldValues, final OrderEntryModel orderEntry)
	{
		double discountAmount = 0.0d;

		final List<DiscountValue> discountValues = orderEntry.getDiscountValues();
		if (discountValues != null)
		{
			for (final DiscountValue dValue : discountValues)
			{
				discountAmount += dValue.getAppliedValue();
			}
		}

		fieldValues.add(CustomObjectDataUtils.createFieldValue(DISCOUNT_VALUE_FIELD_VALUE, String.valueOf(discountAmount)));
	}

}
