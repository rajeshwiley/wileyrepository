/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2013 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package com.wiley.integrations.constants;

/**
 * Global class for all Wileyintegrations constants. You can add global constants for your extension into this class.
 */
public final class WileyintegrationsConstants extends GeneratedWileyintegrationsConstants
{
	public static final String EXTENSIONNAME = "wileyintegrations";
	public static final String RFC3339_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssXXX";

	private WileyintegrationsConstants()
	{
		//empty to avoid instantiating this constant class
	}

	/**
	 * The constant EDI_SUPPORTED_STORES_PROPERTY_PATTERN is used to define property name to configure
	 */
	public static final String EDI_SUPPORTED_STORES_PROPERTY_PATTERN = "%s.edi.supported.stores";

	/**
	 * The constant EDI_SUPPORTED_STORES_DEFAULT is used to define default EDI_SUPPORTED_STORES_PROPERTY_PATTERN value
	 */
	public static final String EDI_SUPPORTED_STORES_DEFAULT = "wel,ags";

	/**
	 * The constant EDI_SYSTEM_CODE.
	 */
	public static final String EDI_SYSTEM_CODE = "edi";

	/**
	 * The constant ELOQUA_SYSTEM_CODE.
	 */
	public static final String ELOQUA_SYSTEM_CODE = "eloqua";

	/**
	 * The constant CDM_SYSTEM_CODE.
	 */
	public static final String CDM_SYSTEM_CODE = "cdm";

	/**
	 * The constant WILEY_CORE_SYSTEM_CODE.
	 */
	public static final String WILEY_CORE_SYSTEM_CODE = "wileycore";

	public static final String BASE_STORE_KEY = "baseStore";

	public static final String PAYMENT_TYPE_KEY = "paymentType";

	public static final String LOCAL_DATE_TIME = "localDateTime";

	public static final String EXPORT_TYPE_KEY = "exportType";

	public static final String DOT = ".";

	public static final String UNDERSCORE = "_";

	public static final String DATE_TIME_FORMATTER = "MM/dd/yyyy";

	public interface Esb
	{
		String CART = "cart";
	}

	public static final String EXTERNAL_CART_MODIFICATION_UPDATED_STATUS = "updated";
	public static final String EXTERNAL_CART_MODIFICATION_REMOVED_STATUS = "deleted";
	public static final String EXTERNAL_CART_MODIFICATION_ADDED_STATUS = "new";

	public static final String EXCEL_DATE_COLUMN_FORMATTER = "MM/dd/yyyy'T'HH:mm:ssZ";

	public static final String CC_PO_NUMBER_FORMAT = "HB%s";
	public static final String PP_PO_NUMBER_FORMAT = "PHB%s";

	public static final String X_APPLICATION_ID = "integration.apigee.application.id";

	public static final String DEFAULT_UNIT_PIECES = "pieces";
}
