package com.wiley.integrations.sabrix.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TaxCalculationResponseEBMType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="TaxCalculationResponseEBMType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/Meta/V1}EBMHeaderType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="UserData" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/TaxEBO/V1}UserDataType"/&gt;
 *         &lt;element name="ServiceResponse" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/Meta/V1}ServiceResponseType" minOccurs="0"/&gt;
 *         &lt;element name="ErrorResponse" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/Meta/V1}ErrorResponse" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.sabrix.com/services/taxcalculationservice/2011-09-01}taxCalculationResponse" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TaxCalculationResponseEBMType", namespace = "http://xmlns.wiley.com/EnterpriseBusinessObjects/TaxEBO/V1", propOrder = {
		"userData",
		"serviceResponse",
		"errorResponse",
		"taxCalculationResponse"
})
public class TaxCalculationResponseEBMType
		extends EBMHeaderType
{

	@XmlElement(name = "UserData", required = true)
	protected UserDataType userData;
	@XmlElement(name = "ServiceResponse")
	protected ServiceResponseType serviceResponse;
	@XmlElement(name = "ErrorResponse")
	protected ErrorResponse errorResponse;
	@XmlElement(namespace = "http://www.sabrix.com/services/taxcalculationservice/2011-09-01")
	protected TaxCalculationResponse taxCalculationResponse;

	/**
	 * Gets the value of the userData property.
	 *
	 * @return possible object is
	 * {@link UserDataType }
	 */
	public UserDataType getUserData()
	{
		return userData;
	}

	/**
	 * Sets the value of the userData property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link UserDataType }
	 */
	public void setUserData(UserDataType value)
	{
		this.userData = value;
	}

	/**
	 * Gets the value of the serviceResponse property.
	 *
	 * @return possible object is
	 * {@link ServiceResponseType }
	 */
	public ServiceResponseType getServiceResponse()
	{
		return serviceResponse;
	}

	/**
	 * Sets the value of the serviceResponse property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link ServiceResponseType }
	 */
	public void setServiceResponse(ServiceResponseType value)
	{
		this.serviceResponse = value;
	}

	/**
	 * Gets the value of the errorResponse property.
	 *
	 * @return possible object is
	 * {@link ErrorResponse }
	 */
	public ErrorResponse getErrorResponse()
	{
		return errorResponse;
	}

	/**
	 * Sets the value of the errorResponse property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link ErrorResponse }
	 */
	public void setErrorResponse(ErrorResponse value)
	{
		this.errorResponse = value;
	}

	/**
	 * Gets the value of the taxCalculationResponse property.
	 *
	 * @return possible object is
	 * {@link TaxCalculationResponse }
	 */
	public TaxCalculationResponse getTaxCalculationResponse()
	{
		return taxCalculationResponse;
	}

	/**
	 * Sets the value of the taxCalculationResponse property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link TaxCalculationResponse }
	 */
	public void setTaxCalculationResponse(TaxCalculationResponse value)
	{
		this.taxCalculationResponse = value;
	}

}
