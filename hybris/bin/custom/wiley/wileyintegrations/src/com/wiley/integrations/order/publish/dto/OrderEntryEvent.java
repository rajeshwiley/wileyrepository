package com.wiley.integrations.order.publish.dto;

import de.hybris.platform.validation.annotations.NotEmpty;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.wiley.integrations.order.dto.OrderEntryWsDTO;


@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderEntryEvent implements Serializable
{
	@NotEmpty
	private String type;

	@NotEmpty
	private String userId;

	@NotEmpty
	private OrderEntryWsDTO entry;

	public String getType()
	{
		return type;
	}

	public void setType(final String type)
	{
		this.type = type;
	}

	public String getUserId()
	{
		return userId;
	}

	public void setUserId(final String userId)
	{
		this.userId = userId;
	}

	public OrderEntryWsDTO getEntry()
	{
		return entry;
	}

	public void setEntry(final OrderEntryWsDTO entry)
	{
		this.entry = entry;
	}
}
