package com.wiley.integrations.failedordersexport.providers.impl;

import com.wiley.core.model.WileyOrderProcessModel;
import com.wiley.core.util.WileyDateUtils;
import com.wiley.integrations.failedordersexport.providers.WileyFailedOrderProcessesProvisionDao;
import de.hybris.platform.processengine.enums.ProcessState;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.store.BaseStoreModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class WileyFailedOrderProcessesProvisionDaoImpl implements WileyFailedOrderProcessesProvisionDao {

    private static final Logger LOG = LoggerFactory.getLogger(WileyFailedOrderProcessesProvisionDaoImpl.class);

    private static final String IGNORE_TIME_LIMIT_PROPERTY = "integrations.failedordersexport.ignore.time.limit";

    private static final List<ProcessState> STATUSES = Arrays.asList(ProcessState.ERROR, ProcessState.FAILED);

    private final FlexibleSearchService flexibleSearchService;

    private final ConfigurationService configurationService;

    @Autowired
    public WileyFailedOrderProcessesProvisionDaoImpl(final FlexibleSearchService flexibleSearchService,
                                                     final ConfigurationService configurationService)
    {
        this.flexibleSearchService = flexibleSearchService;
        this.configurationService = configurationService;
    }

    @Override
    public Collection<WileyOrderProcessModel> findFailedOrderProcesses(final BaseStoreModel store,
                                                                       final LocalDateTime from,
                                                                       final LocalDateTime to)
    {
        Map<String, Object> arguments = prepareArguments(store, STATUSES, from, to);
        String searchQuery = prepareSearchQuery(from, to);
        SearchResult<WileyOrderProcessModel> failedOrderProcesses =
                flexibleSearchService.search(searchQuery, arguments);
        LOG.info("Getting failed fulfillment processes for Store [{}], items found since [{}] to: [{}]: {}",
                store.getUid(), from.toString(), to.toString(), failedOrderProcesses.getCount());
        return failedOrderProcesses.getCount() > 0 ? failedOrderProcesses.getResult() : Collections.emptyList();
    }

    private Map<String, Object> prepareArguments(final BaseStoreModel store, final List<ProcessState> statuses,
                                                 final LocalDateTime from, final LocalDateTime to)
    {
        final Map<String, Object> arguments = new LinkedHashMap<>();
        arguments.put("statuses", statuses);
        arguments.put("store", store.getUid());
        if (from != null) {
            arguments.put("fromDate", WileyDateUtils.convertToDate(from));
        }
        if (to != null) {
            arguments.put("toDate", WileyDateUtils.convertToDate(to));
        }
        return arguments;
    }


    private String prepareSearchQuery(final LocalDateTime from, final LocalDateTime to)
    {
        String searchQuery =
                "SELECT {order_process:PK} "
                        + "FROM {WileyOrderProcess! as order_process "
                        + "LEFT JOIN Order AS order ON {order_process:order} = {order:PK} "
                        + "LEFT JOIN BaseStore AS base_store ON {order:store} = {base_store:PK}} "
                        + "WHERE {base_store:uid} = ?store AND {order_process:state} in (?statuses)";

        if (!ignoreTimeLimit()) {
            if (from != null) {
                searchQuery += " AND {order_process.creationtime} >= ?fromDate";
            }
            if (to != null) {
                searchQuery += " AND {order_process.creationtime} < ?toDate";
            }
        }
        searchQuery += " ORDER BY {order_process:modifiedtime} DESC";
        return searchQuery;
    }

    private boolean ignoreTimeLimit()
    {
        return configurationService.getConfiguration().getBoolean(
                IGNORE_TIME_LIMIT_PROPERTY, false);
    }
}
