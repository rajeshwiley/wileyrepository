package com.wiley.integrations.esb.converters.populator.orderentry;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.annotation.Nonnull;

import com.wiley.integrations.esb.dto.common.AbstractOrderEntryRequest;

import org.springframework.util.Assert;


/**
 * Populates {@link AbstractOrderEntryRequest} from {@link AbstractOrderEntryModel}
 */
public class AbstractOrderEntryModelToAbstractOrderEntryRequestPopulator
		implements Populator<AbstractOrderEntryModel, AbstractOrderEntryRequest>
{
	@Override
	public void populate(@Nonnull final AbstractOrderEntryModel abstractOrderEntryModel,
			@Nonnull final AbstractOrderEntryRequest abstractOrderEntryRequest) throws ConversionException
	{
		Assert.notNull(abstractOrderEntryModel);
		Assert.notNull(abstractOrderEntryRequest);

		ProductModel product = abstractOrderEntryModel.getProduct();
		Assert.notNull(product);

		abstractOrderEntryRequest.setSapProductCode(product.getSapProductCode());
		abstractOrderEntryRequest.setIsbn(product.getIsbn());
		abstractOrderEntryRequest.setQuantity(abstractOrderEntryModel.getQuantity().intValue());
		abstractOrderEntryRequest.setPurchaseOrderNumber(abstractOrderEntryModel.getPoNumber());
	}
}
