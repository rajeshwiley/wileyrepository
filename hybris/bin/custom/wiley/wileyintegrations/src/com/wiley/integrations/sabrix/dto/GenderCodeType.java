package com.wiley.integrations.sabrix.dto;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GenderCodeType.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="GenderCodeType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Female"/&gt;
 *     &lt;enumeration value="Male"/&gt;
 *     &lt;enumeration value="Unreported"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 */
@XmlType(name = "GenderCodeType", namespace = "http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1")
@XmlEnum
public enum GenderCodeType
{

	@XmlEnumValue("Female")
	FEMALE("Female"),
	@XmlEnumValue("Male")
	MALE("Male"),
	@XmlEnumValue("Unreported")
	UNREPORTED("Unreported");
	private final String value;

	GenderCodeType(String v)
	{
		value = v;
	}

	public String value()
	{
		return value;
	}

	public static GenderCodeType fromValue(String v)
	{
		for (GenderCodeType c : GenderCodeType.values())
		{
			if (c.value.equals(v))
			{
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}

}
