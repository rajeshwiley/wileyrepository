/**
 *
 */
package com.wiley.integrations.cdm.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;



@JsonInclude(JsonInclude.Include.NON_NULL)
public class CDMSourceXREFDTO {
  @JsonProperty(value = "SourceCustomerID")
  private String sourceCustomerID;
  @JsonProperty(value = "SourceSystem")
  private final String sourceSystem = "HYBRIS";

  /**
   * @return the sourceCustomerID
   */
  public String getSourceCustomerID() {
    return sourceCustomerID;
  }

  /**
   * @param sourceCustomerID
   *          the sourceCustomerID to set
   */
  public void setSourceCustomerID(final String sourceCustomerID) {
    this.sourceCustomerID = sourceCustomerID;
  }

  /**
   * @return the sourceSystem
   */
  public String getSourceSystem() {
    return sourceSystem;
  }

  @Override
  public String toString() {
    return "SourceXREF [sourceCustomerID=" + sourceCustomerID + ", sourceSystem=" + sourceSystem
        + "]";
  }

}
