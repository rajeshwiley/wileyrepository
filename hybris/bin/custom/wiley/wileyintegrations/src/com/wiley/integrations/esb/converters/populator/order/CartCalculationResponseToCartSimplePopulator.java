package com.wiley.integrations.esb.converters.populator.order;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.util.TaxValue;

import java.util.Arrays;
import java.util.Collections;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import com.wiley.integrations.esb.dto.verifyandcalculatecart.CartCalculationResponse;
import com.wiley.integrations.esb.service.Wileyb2bExternalDiscountApplier;

import org.springframework.util.Assert;


/**
 * Populates cart's simple fields.
 */
public class CartCalculationResponseToCartSimplePopulator implements Populator<CartCalculationResponse, CartModel>
{

	@Resource
	private ModelService modelService;

	@Resource
	private Wileyb2bExternalDiscountApplier wileyb2bExternalDiscountApplier;

	@Override
	public void populate(@Nonnull final CartCalculationResponse orderDto, @Nonnull final CartModel cartModel)
			throws ConversionException
	{
		Assert.notNull(orderDto);
		Assert.notNull(cartModel);

		final CurrencyModel currency = cartModel.getCurrency();
		Assert.notNull(currency);

		cartModel.setSubtotal(orderDto.getSubtotal());

		populateDiscounts(cartModel, orderDto);

		populateCartTotalDiscount(orderDto, cartModel);

		populateDeliveryCost(orderDto, cartModel);

		populateCartTotalTax(orderDto, cartModel, currency);

		cartModel.setTotalPrice(orderDto.getTotalPrice());
	}

	private void populateCartTotalTax(final CartCalculationResponse orderDto, final CartModel cartModel,
			final CurrencyModel currency)
	{
		final Double totalTax = orderDto.getTotalTax();
		if (totalTax != null)
		{
			cartModel.setTotalTax(totalTax);
			final TaxValue taxValue = new TaxValue(generateCode("TAX", cartModel), totalTax, true, totalTax,
					currency.getIsocode());

			// apply(quantity,price,digits,priceIsNet,currencyIsoCode)
			cartModel.setTotalTaxValues(Arrays.asList(taxValue.apply(1, 0, currency.getDigits(), true, currency.getIsocode())));
			cartModel.setTaxCalculated(true);
		}
		else
		{
			// else reset total tax
			cartModel.setTotalTax(null);
			cartModel.setTotalTaxValues(Collections.emptyList());
			cartModel.setTaxCalculated(false);
		}
	}

	private void populateDiscounts(final CartModel cartModel, final CartCalculationResponse orderDto)
	{
		wileyb2bExternalDiscountApplier.applyDiscounts(cartModel, orderDto.getDiscounts());
	}

	private void populateCartTotalDiscount(final CartCalculationResponse orderDto, final CartModel cartModel)
	{
		cartModel.setTotalDiscounts(orderDto.getTotalDiscount());
	}

	private void populateDeliveryCost(final CartCalculationResponse orderDto, final CartModel cartModel)
	{
		cartModel.setDeliveryCost(orderDto.getDeliveryCost());
	}

	private String generateCode(final String prefix, final CartModel cart)
	{
		return prefix + cart.getCode();

	}
}
