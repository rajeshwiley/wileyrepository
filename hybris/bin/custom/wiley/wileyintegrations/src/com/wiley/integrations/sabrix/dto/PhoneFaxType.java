package com.wiley.integrations.sabrix.dto;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PhoneFaxType.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PhoneFaxType"&gt;
 *   &lt;restriction base="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1}StringType"&gt;
 *     &lt;enumeration value="PR"/&gt;
 *     &lt;enumeration value="SR"/&gt;
 *     &lt;enumeration value="PB"/&gt;
 *     &lt;enumeration value="SB"/&gt;
 *     &lt;enumeration value="PM"/&gt;
 *     &lt;enumeration value="SM"/&gt;
 *     &lt;enumeration value="PBF"/&gt;
 *     &lt;enumeration value="SBF"/&gt;
 *     &lt;enumeration value="HF"/&gt;
 *     &lt;enumeration value="RP"/&gt;
 *     &lt;enumeration value="RF"/&gt;
 *     &lt;enumeration value="PPE"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 */
@XmlType(name = "PhoneFaxType", namespace = "http://xmlns.wiley.com/EnterpriseBusinessObjects/CommonInfratypes/Codelist/V1")
@XmlEnum
public enum PhoneFaxType
{

	PR,
	SR,
	PB,
	SB,
	PM,
	SM,
	PBF,
	SBF,
	HF,
	RP,
	RF,
	PPE;

	public String value()
	{
		return name();
	}

	public static PhoneFaxType fromValue(String v)
	{
		return valueOf(v);
	}

}
