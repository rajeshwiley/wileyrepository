package com.wiley.integrations.users.data;

public class UserCredentials implements java.io.Serializable
{

	private String password;
	private String userId;

	public UserCredentials()
	{
		// default constructor
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(final String password)
	{
		this.password = password;
	}

	public String getUserId()
	{
		return userId;
	}

	public void setUserId(final String userId)
	{
		this.userId = userId;
	}


}
