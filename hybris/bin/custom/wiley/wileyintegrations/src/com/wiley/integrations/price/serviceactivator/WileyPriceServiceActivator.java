package com.wiley.integrations.price.serviceactivator;

import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.annotation.ServiceActivator;

import com.wiley.core.price.service.WileyPriceService;
import com.wiley.integrations.price.dto.PriceDto;


public class WileyPriceServiceActivator
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyPriceServiceActivator.class);

	@Resource
	private WileyPriceService wileyPriceService;

	@Resource
	private Converter<PriceDto, PriceRowModel> priceImportModelConverter;

	@ServiceActivator
	public void processPrice(final PriceDto priceDto)
	{
		wileyPriceService.savePriceUpdate(priceImportModelConverter.convert(priceDto));
	}
}
