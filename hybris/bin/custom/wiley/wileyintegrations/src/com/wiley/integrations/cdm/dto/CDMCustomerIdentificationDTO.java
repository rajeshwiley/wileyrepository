/**
 *
 */
package com.wiley.integrations.cdm.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;



@JsonInclude(JsonInclude.Include.NON_NULL)
public class CDMCustomerIdentificationDTO {

  @JsonProperty(value = "EnterpriseCustomerID")
  private String enterpriseCustomerID;
  @JsonProperty(value = "SourceXREF")
  private CDMSourceXREFDTO sourceXREF;

  /**
   * @return the enterpriseCustomerID
   */
  public String getEnterpriseCustomerID() {
    return enterpriseCustomerID;
  }

  /**
   * @param enterpriseCustomerID
   *          the enterpriseCustomerID to set
   */
  public void setEnterpriseCustomerID(final String enterpriseCustomerID) {
    this.enterpriseCustomerID = enterpriseCustomerID;
  }

  /**
   * @return the sourceXREF
   */
  public CDMSourceXREFDTO getSourceXREF() {
    return sourceXREF;
  }

  /**
   * @param sourceXREF
   *          the sourceXREF to set
   */
  public void setSourceXREF(final CDMSourceXREFDTO sourceXREF) {
    this.sourceXREF = sourceXREF;
  }


  @Override
  public String toString() {
    return "CustomerIdentification [enterpriseCustomerID=" + enterpriseCustomerID + ", sourceXREF="
        + sourceXREF + "]";
  }



}
