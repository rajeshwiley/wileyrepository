package com.wiley.integrations.wpg.dto;

public class TokenRefundPaymentResponse
{
	private Integer returnCode;
	private String returnMessage;
	private String transId;
	private String merchantResponse;
	private String merchantId;
	private String value;
	private String authCode;
	private String acquirerId;
	private String acquirerName;
	private String bankId;
	private String bankName;
	private String maskedCardNumber;
	private String cardExpiry;
	private String operation;
	private Integer vendorId;
	private String timestamp;

	public Integer getReturnCode()
	{
		return returnCode;
	}

	public void setReturnCode(final Integer returnCode)
	{
		this.returnCode = returnCode;
	}

	public String getReturnMessage()
	{
		return returnMessage;
	}

	public void setReturnMessage(final String returnMessage)
	{
		this.returnMessage = returnMessage;
	}

	public String getTransId()
	{
		return transId;
	}

	public void setTransId(final String transId)
	{
		this.transId = transId;
	}

	public String getMerchantResponse()
	{
		return merchantResponse;
	}

	public void setMerchantResponse(final String merchantResponse)
	{
		this.merchantResponse = merchantResponse;
	}

	public String getMerchantId()
	{
		return merchantId;
	}

	public void setMerchantId(final String merchantId)
	{
		this.merchantId = merchantId;
	}

	public String getValue()
	{
		return value;
	}

	public void setValue(final String value)
	{
		this.value = value;
	}

	public String getAuthCode()
	{
		return authCode;
	}

	public void setAuthCode(final String authCode)
	{
		this.authCode = authCode;
	}

	public String getAcquirerId()
	{
		return acquirerId;
	}

	public void setAcquirerId(final String acquirerId)
	{
		this.acquirerId = acquirerId;
	}

	public String getAcquirerName()
	{
		return acquirerName;
	}

	public void setAcquirerName(final String acquirerName)
	{
		this.acquirerName = acquirerName;
	}

	public String getBankId()
	{
		return bankId;
	}

	public void setBankId(final String bankId)
	{
		this.bankId = bankId;
	}

	public String getBankName()
	{
		return bankName;
	}

	public void setBankName(final String bankName)
	{
		this.bankName = bankName;
	}

	public String getMaskedCardNumber()
	{
		return maskedCardNumber;
	}

	public void setMaskedCardNumber(final String maskedCardNumber)
	{
		this.maskedCardNumber = maskedCardNumber;
	}

	public String getCardExpiry()
	{
		return cardExpiry;
	}

	public void setCardExpiry(final String cardExpiry)
	{
		this.cardExpiry = cardExpiry;
	}

	public String getOperation()
	{
		return operation;
	}

	public void setOperation(final String operation)
	{
		this.operation = operation;
	}

	public Integer getVendorId()
	{
		return vendorId;
	}

	public void setVendorId(final Integer vendorId)
	{
		this.vendorId = vendorId;
	}

	public String getTimestamp()
	{
		return timestamp;
	}

	public void setTimestamp(final String timestamp)
	{
		this.timestamp = timestamp;
	}
}
