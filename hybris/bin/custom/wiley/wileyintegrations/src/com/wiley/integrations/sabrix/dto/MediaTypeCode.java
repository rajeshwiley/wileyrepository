package com.wiley.integrations.sabrix.dto;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MediaTypeCode.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="MediaTypeCode"&gt;
 *   &lt;restriction base="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1}StringType"&gt;
 *     &lt;enumeration value="A"/&gt;
 *     &lt;enumeration value="F"/&gt;
 *     &lt;enumeration value="G"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 */
@XmlType(name = "MediaTypeCode", namespace = "http://xmlns.wiley.com/EnterpriseBusinessObjects/CommonInfratypes/Codelist/V1")
@XmlEnum
public enum MediaTypeCode
{

	A,
	F,
	G;

	public String value()
	{
		return name();
	}

	public static MediaTypeCode fromValue(String v)
	{
		return valueOf(v);
	}

}
