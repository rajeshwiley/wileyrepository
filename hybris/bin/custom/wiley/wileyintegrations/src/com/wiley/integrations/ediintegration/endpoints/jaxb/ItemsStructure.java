package com.wiley.integrations.ediintegration.endpoints.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * JAXB bean that represents order entry
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class ItemsStructure
{
	@XmlElement(name = "PurchaseOrderNumber")
	private String purchaseOrderNumber;
	@XmlElement(name = "ProductId")
	private String productId;
	@XmlElement(name = "Quantity")
	private Long quantity;
	@XmlElement(name = "Price")
	@XmlJavaTypeAdapter(DoubleAdapter.class)
	private Double price;
	@XmlElement(name = "Discount")
	@XmlJavaTypeAdapter(DoubleAdapter.class)
	private Double discount;
	@XmlElement(name = "TaxAmount")
	@XmlJavaTypeAdapter(DoubleAdapter.class)
	private Double taxAmount;
	@XmlElement(name = "SalesModel")
	private String salesModel;

	public String getPurchaseOrderNumber()
	{
		return purchaseOrderNumber;
	}

	public void setPurchaseOrderNumber(final String purchaseOrderNumber)
	{
		this.purchaseOrderNumber = purchaseOrderNumber;
	}

	public String getProductId()
	{
		return productId;
	}

	public void setProductId(final String productId)
	{
		this.productId = productId;
	}

	public Long getQuantity()
	{
		return quantity;
	}

	public void setQuantity(final Long quantity)
	{
		this.quantity = quantity;
	}

	public Double getPrice()
	{
		return price;
	}

	public void setPrice(final Double price)
	{
		this.price = price;
	}

	public Double getDiscount()
	{
		return discount;
	}

	public void setDiscount(final Double discount)
	{
		this.discount = discount;
	}

	public Double getTaxAmount()
	{
		return taxAmount;
	}

	public void setTaxAmount(final Double taxAmount)
	{
		this.taxAmount = taxAmount;
	}

	public String getSalesModel()
	{
		return salesModel;
	}

	public void setSalesModel(final String salesModel)
	{
		this.salesModel = salesModel;
	}
}
