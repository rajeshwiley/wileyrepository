package com.wiley.integrations.esb.transformer;

import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;

import javax.annotation.Resource;


public class Wileyb2bPriceRequestCurrencyEnricher
{
	@Resource
	private CommerceCommonI18NService commerceCommonI18NService;

	public String getCurrency()
	{
		String currencyIso = commerceCommonI18NService.getCurrentCurrency().getIsocode();

		if (currencyIso == null)
		{
			currencyIso = commerceCommonI18NService.getDefaultCurrency().getIsocode();
		}

		return currencyIso;
	}
}