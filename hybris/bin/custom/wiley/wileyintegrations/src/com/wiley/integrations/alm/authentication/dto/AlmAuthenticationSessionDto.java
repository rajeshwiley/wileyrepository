package com.wiley.integrations.alm.authentication.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
@JsonInclude(NON_NULL)
public class AlmAuthenticationSessionDto
{
	@JsonProperty(value = "user", required = true)
	private AlmAuthenticationUserDto user;

	@JsonProperty(value = "imitatee")
	private AlmAuthenticationImitateeDto imitatee;

	public AlmAuthenticationUserDto getUser()
	{
		return user;
	}

	public void setUser(final AlmAuthenticationUserDto user)
	{
		this.user = user;
	}

	public AlmAuthenticationImitateeDto getImitatee()
	{
		return imitatee;
	}

	public void setImitatee(final AlmAuthenticationImitateeDto imitatee)
	{
		this.imitatee = imitatee;
	}

	@Override
	public String toString()
	{
		return MoreObjects.toStringHelper(this)
				.add("user", getUser())
				.add("imitatee", getImitatee())
				.toString();
	}
}
