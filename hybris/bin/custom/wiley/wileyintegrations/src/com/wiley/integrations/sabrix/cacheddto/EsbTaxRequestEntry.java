package com.wiley.integrations.sabrix.cacheddto;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;


public class EsbTaxRequestEntry
{
	private String productCode;
	private Double totalAmount;
	private String transactionType;
	private EsbAddress shipFrom;

	public String getProductCode()
	{
		return productCode;
	}

	public void setProductCode(final String productCode)
	{
		this.productCode = productCode;
	}

	public Double getTotalAmount()
	{
		return totalAmount;
	}

	public void setTotalAmount(final Double totalAmount)
	{
		this.totalAmount = totalAmount;
	}

	public String getTransactionType()
	{
		return transactionType;
	}

	public void setTransactionType(final String transactionType)
	{
		this.transactionType = transactionType;
	}

	public EsbAddress getShipFrom()
	{
		return shipFrom;
	}

	public void setShipFrom(final EsbAddress shipFrom)
	{
		this.shipFrom = shipFrom;
	}

	@Override
	public int hashCode()
	{
		return Objects.hashCode(productCode, totalAmount, transactionType, shipFrom);
	}

	@Override
	public boolean equals(final Object obj)
	{
		if (this == obj)
		{
			return true;
		}
		if (obj == null || getClass() != obj.getClass())
		{
			return false;
		}
		final EsbTaxRequestEntry other = (EsbTaxRequestEntry) obj;
		return Objects.equal(this.productCode, other.productCode)
				&& Objects.equal(this.totalAmount, other.totalAmount)
				&& Objects.equal(this.transactionType, other.transactionType)
				&& Objects.equal(this.shipFrom, other.shipFrom);
	}

	@Override
	public String toString()
	{
		return MoreObjects.toStringHelper(this)
				.add("productCode", productCode)
				.add("totalAmount", totalAmount)
				.add("transactionType", transactionType)
				.add("shipFrom", shipFrom)
				.toString();
	}
}

