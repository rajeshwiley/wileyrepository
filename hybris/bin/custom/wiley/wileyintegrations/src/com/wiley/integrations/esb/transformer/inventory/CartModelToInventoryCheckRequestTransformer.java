package com.wiley.integrations.esb.transformer.inventory;

import de.hybris.platform.core.model.order.CartModel;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import com.wiley.integrations.esb.converters.populator.inventory.AbstractOrderModelToInventoryCheckRequestPopulator;
import com.wiley.integrations.inventory.status.InventoryCheckRequest;

import org.springframework.util.Assert;



/**
 * Created by Mikhail_Asadchy on 7/27/2016.
 */
public class CartModelToInventoryCheckRequestTransformer
{
	@Resource
	private AbstractOrderModelToInventoryCheckRequestPopulator abstractOrderModelToInventoryCheckRequestPopulator;

	@Nonnull
	public InventoryCheckRequest transform(@Nonnull final CartModel cartModel)
	{
		Assert.notNull(cartModel);

		final InventoryCheckRequest request = new InventoryCheckRequest();

		abstractOrderModelToInventoryCheckRequestPopulator.populate(cartModel, request);

		return request;
	}
}
