package com.wiley.integrations.eloqua.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.annotation.Nonnull;

import org.springframework.util.Assert;

import com.wiley.core.integration.eloqua.dto.ContactDto;


/**
 * Populates address data in Contact.
 *
 * @author Aliaksei_Zlobich
 */
public class AddressToContactPopulator implements Populator<AddressModel, ContactDto>
{
	@Override
	public void populate(@Nonnull final AddressModel address, @Nonnull final ContactDto contact) throws ConversionException
	{
		Assert.notNull(address);
		Assert.notNull(contact);

		if (address.getCountry() != null)
		{
			contact.setCountry(address.getCountry().getIsocode3());
		}
		contact.setBusinessPhone(address.getPhone1());
		contact.setAddress1(address.getLine1());
		contact.setAddress2(address.getLine2());
		contact.setCity(address.getTown());
		if (address.getRegion() != null)
		{
			contact.setProvince(address.getRegion().getIsocode());
		}
		contact.setPostalCode(address.getPostalcode());
	}
}
