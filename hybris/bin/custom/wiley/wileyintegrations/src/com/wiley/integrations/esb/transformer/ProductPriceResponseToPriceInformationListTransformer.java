package com.wiley.integrations.esb.transformer;

import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.util.PriceValue;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.Nonnull;

import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.messaging.Message;
import org.springframework.util.Assert;

import com.wiley.integrations.esb.dto.EsbProductPriceResponseDto;

import static com.wiley.core.integration.esb.EsbCartCalculationGateway.BUSINESS_PRICE_COUNTRY;
import static com.wiley.core.integration.esb.EsbCartCalculationGateway.BUSINESS_PRICE_CURRENCY;


public class ProductPriceResponseToPriceInformationListTransformer
{
	@Nonnull
	public List<PriceInformation> transform(@Nonnull final Message<EsbProductPriceResponseDto[]> message)
	{
		Assert.notNull(message);

		final String sessionCountry = (String) message.getHeaders().get(BUSINESS_PRICE_COUNTRY);
		final String sessionCurrency = (String) message.getHeaders().get(BUSINESS_PRICE_CURRENCY);

		final List<EsbProductPriceResponseDto> payload = Arrays.asList(message.getPayload());
		final List<EsbProductPriceResponseDto> priceListMatch = payload.stream()
				.filter(pricePart -> ObjectUtils.equals(pricePart.getCountry(),
						sessionCountry) && ObjectUtils.equals(pricePart.getCurrency(), sessionCurrency)
				).collect(Collectors.toList());
		Stream<EsbProductPriceResponseDto> filteredPriceStream = null;

		if (priceListMatch.size() > 0)
		{
			filteredPriceStream = priceListMatch.stream();
		}
		else
		{
			filteredPriceStream = payload.stream().filter(priceDto -> StringUtils.isBlank(priceDto.getCountry())
					&& ObjectUtils.equals(priceDto.getCurrency(), sessionCurrency));
		}

		final List<PriceInformation> filteredAndMapped = filteredPriceStream.
				map(priceResp -> new PriceInformation(new PriceValue(priceResp.getCurrency(), priceResp.getPrice(), true)))
			.collect(Collectors.toList());
		// This assert is temporary until the implementation by new Spec, when we can return multiple prices
		Assert.isTrue(filteredAndMapped.size() <= 1);

		return filteredAndMapped;
	}

}
