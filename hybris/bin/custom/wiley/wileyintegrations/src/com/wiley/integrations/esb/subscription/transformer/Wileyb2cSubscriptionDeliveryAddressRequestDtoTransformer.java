package com.wiley.integrations.esb.subscription.transformer;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.AddressModel;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.wiley.integrations.esb.subscription.dto.Wileyb2cSubscriptionDeliveryAddressRequestDto;


public class Wileyb2cSubscriptionDeliveryAddressRequestDtoTransformer
{
	@Resource(name = "wileyb2cSubscriptionDeliveryAddressRequestDtoPopulator")
	private Populator<AddressModel, Wileyb2cSubscriptionDeliveryAddressRequestDto> requestDtoPopulator;

	public Wileyb2cSubscriptionDeliveryAddressRequestDto transform(@Nonnull final AddressModel deliveryAddress)
	{
		Assert.notNull(deliveryAddress);

		Wileyb2cSubscriptionDeliveryAddressRequestDto requestDto = new Wileyb2cSubscriptionDeliveryAddressRequestDto();
		requestDtoPopulator.populate(deliveryAddress, requestDto);

		return requestDto;
	}
}
