package com.wiley.integrations.tax.dto;

import java.math.BigDecimal;
import java.util.Objects;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;


public class TaxCalculationRequestEntry
{
	@JsonProperty("id")
	@NotNull
	@Size(max = 255)
	private String id;

	@JsonProperty("amount")
	@NotNull
	@DecimalMin("0.0")
	private BigDecimal amount;

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		final TaxCalculationRequestEntry that = (TaxCalculationRequestEntry) o;
		return Objects.equals(id, that.id)
				&& Objects.equals(amount, that.amount)
				&& Objects.equals(codeType, that.codeType)
				&& Objects.equals(code, that.code)
				&& Objects.equals(productType, that.productType);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(id, amount, codeType, code);
	}

	@JsonProperty("codeType")
	@NotNull
	@Size(max = 255)
	private String codeType;

	@JsonProperty("code")
	@NotNull
	@Size(max = 255)
	private String code;

	@JsonProperty("productType")
	@NotNull
	@Size(max = 255)
	private String productType;

	/**
	 * An arbitrary identifier to bind the input entry to the tax calculation result.
	 *
	 * @return id
	 **/
	public String getId()
	{
		return id;
	}

	public void setId(final String id)
	{
		this.id = id;
	}

	/**
	 * Taxable amount.
	 * minimum: 0
	 *
	 * @return amount
	 **/
	public BigDecimal getAmount()
	{
		return amount;
	}

	public void setAmount(final BigDecimal amount)
	{
		this.amount = amount;
	}

	/**
	 * Type of the entry's code. On Phase 4 it can be \"isbn13\", \"clusterCode\", \"journalDhId\".
	 * In future, the list can be extended.
	 *
	 * @return codeType
	 **/
	public String getCodeType()
	{
		return codeType;
	}

	public void setCodeType(final String codeType)
	{
		this.codeType = codeType;
	}

	/**
	 * The item's code defined according to the type specified in the field *codeType*.
	 *
	 * @return code
	 **/
	public String getCode()
	{
		return code;
	}

	public void setCode(final String code)
	{
		this.code = code;
	}

	/**
	 * Type of the entry's product.
	 *
	 * @return productType
	 **/
	public String getProductType()
	{
		return productType;
	}

	public void setProductType(final String productType)
	{
		this.productType = productType;
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("class TaxCalculationRequestEntry {\n");

		sb.append("    id: ").append(toIndentedString(id)).append("\n");
		sb.append("    amount: ").append(toIndentedString(amount)).append("\n");
		sb.append("    codeType: ").append(toIndentedString(codeType)).append("\n");
		sb.append("    code: ").append(toIndentedString(code)).append("\n");
		sb.append("    productType: ").append(toIndentedString(productType)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(final Object o)
	{
		if (o == null)
		{
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}

