package com.wiley.integrations.esb.dto.order;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wiley.core.enums.WileyDigitalContentTypeEnum;
import com.wiley.integrations.esb.dto.common.AbstractOrderEntryRequest;
import com.wiley.integrations.esb.dto.common.Discount;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;


/**
 * An order entry - an order line for a single product.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderEntryRequest extends AbstractOrderEntryRequest
{
	/**
	 * List of the discount codes that were applied to this order.
	 */
	private List<Discount> discounts;

	/**
	 * Price of the single product item. The en_US format is used for the value.
	 */
	private Double basePrice;

	/**
	 * Price of all the product items in the entity (quantity x basePrice). The en_US format is used for the value.
	 */
	private Double totalPrice;

	/**
	 * It is an enumeration type specifies the duration of the contract.
	 * In case if term less than a year it has MONTHLY value,
	 * in case it more or equal to year, than it has YEARLY value,
	 * in case the term is zero the value is UNKNOWN.
	 */
	private String subscriptionTerm;

	/**
	 * Only for digital products. Defines, which content system provides access to the digital product.
	 * [VITAL_SOURCE, LIGHTNING_SOURCE, WILEY_DOWNLOAD]
	 */
	private WileyDigitalContentTypeEnum digitalContentType;
	

	@JsonProperty("discounts")
	public List<Discount> getDiscounts()
	{
		return discounts;
	}

	public void setDiscounts(final List<Discount> discounts)
	{
		this.discounts = discounts;
	}

	@JsonProperty("basePrice")
	public Double getBasePrice()
	{
		return basePrice;
	}

	public void setBasePrice(final Double basePrice)
	{
		this.basePrice = basePrice;
	}

	@JsonProperty("totalPrice")
	public Double getTotalPrice()
	{
		return totalPrice;
	}

	public void setTotalPrice(final Double totalPrice)
	{
		this.totalPrice = totalPrice;
	}
	
	@JsonProperty("subscriptionTerm")
	public String getSubscriptionTerm()
	{
		return subscriptionTerm;
	}

	public void setSubscriptionTerm(final String subscriptionTerm)
	{
		this.subscriptionTerm = subscriptionTerm;
	}

	@JsonProperty("digitalContentType")
	public WileyDigitalContentTypeEnum getDigitalContentType()
	{
		return digitalContentType;
	}

	public void setDigitalContentType(final WileyDigitalContentTypeEnum digitalContentType)
	{
		this.digitalContentType = digitalContentType;
	}

	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this);
	}
}
