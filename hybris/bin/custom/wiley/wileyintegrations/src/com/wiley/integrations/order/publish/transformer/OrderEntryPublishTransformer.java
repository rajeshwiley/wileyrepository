package com.wiley.integrations.order.publish.transformer;


import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;

import org.springframework.messaging.Message;

import com.wiley.integrations.order.dto.OrderEntryWsDTO;
import com.wiley.integrations.order.publish.dto.OrderEntryEvent;



public class OrderEntryPublishTransformer
{
	@Resource
	private Converter<AbstractOrderEntryModel, OrderEntryWsDTO> wileyOrderEntryWsConverter;

	public OrderEntryEvent transform(final Message<AbstractOrderEntryModel> message)
	{
		final String eventType = message.getHeaders().get("hybrisOrderEntryEventType").toString();
		final AbstractOrderEntryModel orderEntryModel = message.getPayload();
		OrderEntryEvent orderEntryEvent = new OrderEntryEvent();
		orderEntryEvent.setType(eventType);
		orderEntryEvent.setEntry(wileyOrderEntryWsConverter.convert(orderEntryModel));
		orderEntryEvent.setUserId(orderEntryModel.getOrder().getUser().getUid());

		return orderEntryEvent;
	}
}
