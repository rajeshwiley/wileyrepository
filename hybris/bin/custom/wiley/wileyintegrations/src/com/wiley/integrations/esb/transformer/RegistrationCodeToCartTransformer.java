package com.wiley.integrations.esb.transformer;

import de.hybris.platform.core.model.order.CartModel;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.messaging.Message;
import org.springframework.util.Assert;

import com.wiley.core.exceptions.ExternalSystemValidationException;
import com.wiley.core.integration.esb.RegistrationCodeGateway;
import com.wiley.core.regCode.exception.RegCodeValidationException;
import com.wiley.integrations.esb.converters.populator.order.RegistrationCodeResponseToCartPopulator;
import com.wiley.integrations.esb.dto.RegistrationCodeValidationResponseDto;


public class RegistrationCodeToCartTransformer
{
	@Resource(name = "registrationCodeResponseToCartPopulator")
	private RegistrationCodeResponseToCartPopulator responsePopulator;


	public CartModel transform(@Nonnull final Message<RegistrationCodeValidationResponseDto> message) throws Exception
	{
		Assert.notNull(message);

		final CartModel cartModel = (CartModel) message.getHeaders().get(RegistrationCodeGateway.CART_MODEL);
		final RegistrationCodeValidationResponseDto responseDto = message.getPayload();

		Assert.notNull(cartModel);
		Assert.notNull(responseDto);

		if (StringUtils.isNotEmpty(responseDto.getMessage()))
		{
			throw new RegCodeValidationException(String.format("Regcode validation service return error %s - %s",
					responseDto.getCode(), responseDto.getMessage()));
		}

		validateSuccessResponse(responseDto);
		responsePopulator.populate(responseDto, cartModel);

		return cartModel;
	}

	private void validateSuccessResponse(final RegistrationCodeValidationResponseDto responseDto)
	{
		if (responseDto.getCode() == null || responseDto.getIsbn() == null)
		{
			throw new ExternalSystemValidationException("Registration code validation response must have regCode and isbn");
		}
	}
}
