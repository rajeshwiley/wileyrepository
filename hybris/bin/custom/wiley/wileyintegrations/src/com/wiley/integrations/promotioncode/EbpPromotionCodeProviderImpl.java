package com.wiley.integrations.promotioncode;

/**
 * Provides promotion code for EBP system.
 * Please see
 * <a href="https://confluence.wiley.ru/display/ECSC/IDD%3A+EBP+integration.+Order+data">IDD: EBP integration. Order data</a>
 * for more details.
 */
public class EbpPromotionCodeProviderImpl extends AbstractPromotionCodeProviderImpl
{
	@Override
	protected boolean includeStudent()
	{
		return true;
	}
}
