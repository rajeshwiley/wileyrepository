package com.wiley.integrations.order.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;

import javax.annotation.Resource;

import com.wiley.core.model.WileyExternalDiscountModel;
import com.wiley.integrations.order.dto.DiscountValueWsDTO;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


public class WileyExternalDiscountReverseWsPopulator implements Populator<DiscountValueWsDTO, WileyExternalDiscountModel>
{
	@Resource
	private CommonI18NService commonI18NService;

	@Resource
	private KeyGenerator guidKeyGenerator;

	@Override
	public void populate(final DiscountValueWsDTO discountValueWsDTO, final WileyExternalDiscountModel externalDiscount)
			throws ConversionException
	{
		validateParameterNotNullStandardMessage("discountValueWsDTO", discountValueWsDTO);
		validateParameterNotNullStandardMessage("externalDiscount", externalDiscount);

		externalDiscount.setGuid(guidKeyGenerator.generate().toString());
		externalDiscount.setCode(discountValueWsDTO.getCode());
		externalDiscount.setAbsolute(discountValueWsDTO.getAbsolute());
		externalDiscount.setValue(discountValueWsDTO.getValue());
		externalDiscount.setCouponCodes(discountValueWsDTO.getCouponCodes());
		externalDiscount.setPromoInstitution(discountValueWsDTO.getPromoInstitution());
		externalDiscount.setPromoTypeGroup(discountValueWsDTO.getPromoTypeGroup());
		if (discountValueWsDTO.getCurrency() != null)
		{
			externalDiscount.setCurrency(commonI18NService.getCurrency(discountValueWsDTO.getCurrency()));
		}
	}
}
