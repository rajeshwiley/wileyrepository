package com.wiley.integrations.alm.authentication.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.retry.RetryCallback;
import org.springframework.retry.RetryContext;
import org.springframework.retry.listener.RetryListenerSupport;

/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public class AlmRetryListener extends RetryListenerSupport
{
	private static final Logger LOG = LoggerFactory.getLogger(AlmRetryListener.class);

	@Override
	public <T, E extends Throwable> void onError(final RetryContext context, final RetryCallback<T, E> callback,
			final Throwable throwable)
	{
		LOG.debug("Unable to connect ALM server: {}", throwable.getMessage());
	}
}
