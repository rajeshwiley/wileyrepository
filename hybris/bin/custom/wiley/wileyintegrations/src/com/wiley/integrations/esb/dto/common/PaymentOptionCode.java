package com.wiley.integrations.esb.dto.common;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;


/**
 * Payment option code.
 */
public enum PaymentOptionCode
{
	CARD("card"),
	INVOICE("invoice"),
	PAYPAL("paypal");

	private String value;

	PaymentOptionCode(final String value)
	{
		this.value = value;
	}

	@JsonCreator
	public static PaymentOptionCode forValue(final String value)
	{
		for (PaymentOptionCode optionCode : values())
		{
			if (optionCode.getValue().equals(value))
			{
				return optionCode;
			}
		}

		throw new IllegalArgumentException(
				String.format("Could not find [%s] for value [%s].", PaymentOptionCode.class.getName(), value));
	}

	@JsonValue
	public String toValue()
	{
		return this.getValue();
	}

	public String getValue()
	{
		return value;
	}
}
