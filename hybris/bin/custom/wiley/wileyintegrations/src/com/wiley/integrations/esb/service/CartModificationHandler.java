package com.wiley.integrations.esb.service;

import de.hybris.platform.core.model.order.CartModel;

import java.util.List;

import javax.annotation.Nonnull;

import com.wiley.core.integration.ExternalCartModification;
import com.wiley.integrations.esb.dto.verifyandcalculatecart.CalculationMessage;
import com.wiley.integrations.esb.dto.verifyandcalculatecart.CartCalculationEntryResponse;


/**
 * Handle Order modifications and apply this modification on cart.
 */
public interface CartModificationHandler
{

	/**
	 * Applies modifications to cart.<br/>
	 * Performs adding, updating, removing
	 *
	 * @param cart
	 * @param cartCalculationEntryResponse
	 * @param calculationMessages
	 * @return external cart modifications
	 */
	@Nonnull
	List<ExternalCartModification> applyOrderModificationToCart(@Nonnull CartModel cart,
			@Nonnull List<CartCalculationEntryResponse> cartCalculationEntryResponse,
			@Nonnull List<CalculationMessage> calculationMessages);

}
