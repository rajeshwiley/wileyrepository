package com.wiley.integrations.order.dto;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * An address.
 **/
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AddressWsDTO
{

	@Size(max = 255)
	private String postcode;
	@NotNull
	@Pattern(regexp = "^[A-Z]{2}$")
	private String country;
	@Pattern(regexp = "^[0-9A-Z]{1,3}$")
	private String state;
	@Size(max = 255)
	private String city;
	@Size(max = 255)
	private String line1;
	@Size(max = 255)
	private String line2;
	@Size(max = 255)
	private String firstName;
	@Size(max = 255)
	private String lastName;
	@Size(max = 255)
	private String phoneNumber;
	@Size(max = 255)
	private String email;
	@Size(max = 255)
	private String organization;
	@Size(max = 255)
	private String department;
	private Map<String, Object> anyProperties = new HashMap<>();

	/**
	 * Zip/postal code.
	 **/
	@JsonProperty("postcode")
	public String getPostcode()
	{
		return postcode;
	}

	public void setPostcode(final String postcode)
	{
		this.postcode = postcode;
	}

	@JsonProperty("country")
	public String getCountry()
	{
		return country;
	}

	public void setCountry(final String country)
	{
		this.country = country;
	}

	@JsonProperty("state")
	public String getState()
	{
		return state;
	}

	public void setState(final String state)
	{
		this.state = state;
	}

	/**
	 * City/town.
	 **/
	@JsonProperty("city")
	public String getCity()
	{
		return city;
	}

	public void setCity(final String city)
	{
		this.city = city;
	}

	/**
	 * First line of the address.
	 **/
	@JsonProperty("line1")
	public String getLine1()
	{
		return line1;
	}

	public void setLine1(final String line1)
	{
		this.line1 = line1;
	}

	/**
	 * Second line of the address.
	 **/
	@JsonProperty("line2")
	public String getLine2()
	{
		return line2;
	}

	public void setLine2(final String line2)
	{
		this.line2 = line2;
	}

	/**
	 * User's first name.
	 **/
	@JsonProperty("firstName")
	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(final String firstName)
	{
		this.firstName = firstName;
	}

	/**
	 * User's last name.
	 **/
	@JsonProperty("lastName")
	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(final String lastName)
	{
		this.lastName = lastName;
	}

	/**
	 * Phone number.
	 **/
	@JsonProperty("phoneNumber")
	public String getPhoneNumber()
	{
		return phoneNumber;
	}

	public void setPhoneNumber(final String phoneNumber)
	{
		this.phoneNumber = phoneNumber;
	}

	/**
	 * Email.
	 **/
	@JsonProperty("email")
	public String getEmail()
	{
		return email;
	}

	public void setEmail(final String email)
	{
		this.email = email;
	}

	/**
	 * User's organization/institution/university.
	 **/
	@JsonProperty("organization")
	public String getOrganization()
	{
		return organization;
	}

	public void setOrganization(final String organization)
	{
		this.organization = organization;
	}

	/**
	 * User's department.
	 **/
	@JsonProperty("department")
	public String getDepartment()
	{
		return department;
	}

	public void setDepartment(final String department)
	{
		this.department = department;
	}

	@JsonAnyGetter
	public Map<String, Object> getAnyProperties()
	{
		return anyProperties;
	}

	@JsonAnySetter
	public void setAnyProperties(final String name, final Object value)
	{
		this.anyProperties.put(name, value);
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		AddressWsDTO address = (AddressWsDTO) o;
		return Objects.equals(postcode, address.postcode)
				&& Objects.equals(country, address.country)
				&& Objects.equals(state, address.state)
				&& Objects.equals(city, address.city)
				&& Objects.equals(line1, address.line1)
				&& Objects.equals(line2, address.line2)
				&& Objects.equals(firstName, address.firstName)
				&& Objects.equals(lastName, address.lastName)
				&& Objects.equals(phoneNumber, address.phoneNumber)
				&& Objects.equals(email, address.email)
				&& Objects.equals(organization, address.organization)
				&& Objects.equals(department, address.department)
				&& Objects.equals(anyProperties, address.anyProperties);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(postcode, country, state, city, line1, line2, firstName, lastName, phoneNumber,
				email, organization, department, anyProperties);
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("class Address {\n");

		sb.append("    postcode: ").append(toIndentedString(postcode)).append("\n");
		sb.append("    country: ").append(toIndentedString(country)).append("\n");
		sb.append("    state: ").append(toIndentedString(state)).append("\n");
		sb.append("    city: ").append(toIndentedString(city)).append("\n");
		sb.append("    line1: ").append(toIndentedString(line1)).append("\n");
		sb.append("    line2: ").append(toIndentedString(line2)).append("\n");
		sb.append("    firstName: ").append(toIndentedString(firstName)).append("\n");
		sb.append("    lastName: ").append(toIndentedString(lastName)).append("\n");
		sb.append("    phoneNumber: ").append(toIndentedString(phoneNumber)).append("\n");
		sb.append("    email: ").append(toIndentedString(email)).append("\n");
		sb.append("    organization: ").append(toIndentedString(organization)).append("\n");
		sb.append("    department: ").append(toIndentedString(department)).append("\n");
		sb.append("    anyProperties: ").append(toIndentedString(anyProperties)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(final Object o)
	{
		if (o == null)
		{
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}

