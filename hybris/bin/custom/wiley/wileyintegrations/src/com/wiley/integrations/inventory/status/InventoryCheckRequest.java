package com.wiley.integrations.inventory.status;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.wiley.integrations.esb.dto.common.Address;


/**
 * Created by Mikhail_Asadchy on 7/27/2016.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class InventoryCheckRequest
{
	private String country;

	private Address deliveryAddress;

	private String deliveryOptionCode;

	private List<InventoryCheckEntryRequest> entries;

	public String getCountry()
	{
		return country;
	}

	public void setCountry(final String country)
	{
		this.country = country;
	}

	public Address getDeliveryAddress()
	{
		return deliveryAddress;
	}

	public void setDeliveryAddress(final Address deliveryAddress)
	{
		this.deliveryAddress = deliveryAddress;
	}

	public String getDeliveryOptionCode()
	{
		return deliveryOptionCode;
	}

	public void setDeliveryOptionCode(final String deliveryOptionCode)
	{
		this.deliveryOptionCode = deliveryOptionCode;
	}

	public List<InventoryCheckEntryRequest> getEntries()
	{
		return entries;
	}

	public void setEntries(final List<InventoryCheckEntryRequest> entries)
	{
		this.entries = entries;
	}
}
