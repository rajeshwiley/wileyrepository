package com.wiley.integrations.users.data;

public class WileyUserAuthenticateResponse implements java.io.Serializable
{
	private boolean passwordValid;

	public WileyUserAuthenticateResponse()
	{
		// default constructor
	}

	public boolean isPasswordValid()
	{
		return passwordValid;
	}

	public void setPasswordValid(final boolean passwordValid)
	{
		this.passwordValid = passwordValid;
	}
}
