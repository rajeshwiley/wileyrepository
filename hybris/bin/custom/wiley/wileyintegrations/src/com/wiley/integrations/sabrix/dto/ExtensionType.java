package com.wiley.integrations.sabrix.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ExtensionType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="ExtensionType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ExtendedArea" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExtensionType", namespace = "http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1", propOrder = {
		"extendedArea"
})
public class ExtensionType
{

	@XmlElement(name = "ExtendedArea", required = true)
	protected Object extendedArea;

	/**
	 * Gets the value of the extendedArea property.
	 *
	 * @return possible object is
	 * {@link Object }
	 */
	public Object getExtendedArea()
	{
		return extendedArea;
	}

	/**
	 * Sets the value of the extendedArea property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link Object }
	 */
	public void setExtendedArea(Object value)
	{
		this.extendedArea = value;
	}

}
