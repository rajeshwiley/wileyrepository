package com.wiley.integrations.sabrix.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * GetQueryType is used for single record query using unique key for the data. Used for existence check, getting entity data using primary key
 *
 * <p>Java class for GetQueryType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="GetQueryType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="QueryString" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="FilterCriteria" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="QueryCode" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1}StringType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetQueryType", namespace = "http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/Meta/V1", propOrder = {
		"queryString",
		"filterCriteria",
		"queryCode"
})
public class GetQueryType
{

	@XmlElement(name = "QueryString")
	protected String queryString;
	@XmlElement(name = "FilterCriteria")
	protected String filterCriteria;
	@XmlElement(name = "QueryCode")
	protected String queryCode;

	/**
	 * Gets the value of the queryString property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getQueryString()
	{
		return queryString;
	}

	/**
	 * Sets the value of the queryString property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setQueryString(String value)
	{
		this.queryString = value;
	}

	/**
	 * Gets the value of the filterCriteria property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getFilterCriteria()
	{
		return filterCriteria;
	}

	/**
	 * Sets the value of the filterCriteria property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setFilterCriteria(String value)
	{
		this.filterCriteria = value;
	}

	/**
	 * Gets the value of the queryCode property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getQueryCode()
	{
		return queryCode;
	}

	/**
	 * Sets the value of the queryCode property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setQueryCode(String value)
	{
		this.queryCode = value;
	}

}
