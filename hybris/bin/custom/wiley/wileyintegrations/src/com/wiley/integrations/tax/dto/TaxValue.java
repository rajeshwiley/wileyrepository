package com.wiley.integrations.tax.dto;

import java.math.BigDecimal;
import java.util.Objects;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;


public class TaxValue
{
	@JsonProperty("code")
	@NotNull
	@Size(max = 255)
	private String code;

	@JsonProperty("value")
	@NotNull
	@DecimalMin("0.0")
	private BigDecimal value;

	/**
	 * Qualifier of the tax value.
	 *
	 * @return code
	 **/
	public String getCode()
	{
		return code;
	}

	public void setCode(final String code)
	{
		this.code = code;
	}

	/**
	 * Value (amount) of the tax.
	 * minimum: 0
	 *
	 * @return value
	 **/
	public BigDecimal getValue()
	{
		return value;
	}

	@NotNull
	@DecimalMin("0.0")
	public void setValue(final BigDecimal value)
	{
		this.value = value;
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("class TaxValue {\n");

		sb.append("    code: ").append(toIndentedString(code)).append("\n");
		sb.append("    value: ").append(toIndentedString(value)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(final Object o)
	{
		if (o == null)
		{
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		final TaxValue taxValue = (TaxValue) o;
		return Objects.equals(code, taxValue.code)
				&& Objects.equals(value, taxValue.value);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(code, value);
	}
}

