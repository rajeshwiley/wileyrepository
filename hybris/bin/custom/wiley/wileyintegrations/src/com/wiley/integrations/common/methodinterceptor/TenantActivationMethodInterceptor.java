package com.wiley.integrations.common.methodinterceptor;

import de.hybris.platform.acceleratorservices.dataimport.batch.aop.TenantActivationAspect;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.Tenant;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.util.Assert;


/**
 * Modified copy of {@link TenantActivationAspect} logic for tenant activation.
 */
public class TenantActivationMethodInterceptor implements MethodInterceptor, BeanNameAware
{
	private static final Logger LOG = LoggerFactory.getLogger(TenantActivationMethodInterceptor.class);
	private final Tenant currentTenant;
	private String beanName;

	public TenantActivationMethodInterceptor()
	{
		currentTenant = Registry.getCurrentTenant();
		Assert.notNull(currentTenant, "Tenant should not be null");
	}

	/**
	 * Invokes a method and with an activated tenant. If no tenant is activated, the tenant set during initialization is
	 * used.
	 */
	@Override
	public Object invoke(final MethodInvocation methodInvocation) throws Throwable
	{
		if (Registry.hasCurrentTenant() && Registry.isCurrentTenant(currentTenant))
		{
			return methodInvocation.proceed();
		}

		// Thread does not have a tenant, setup our tenant on the thread
		Registry.setCurrentTenant(currentTenant);

		LOG.debug("Setting tenant {} on the current thread {} in {} bean", currentTenant, Thread.currentThread(), beanName);

		try
		{
			return methodInvocation.proceed();
		}
		finally
		{
			Registry.unsetCurrentTenant();
		}
	}

	@Override
	public void setBeanName(final String beanName)
	{
		this.beanName = beanName;
		LOG.info("Configured tenant {} for aspect in {} bean", currentTenant, beanName);
	}
}
