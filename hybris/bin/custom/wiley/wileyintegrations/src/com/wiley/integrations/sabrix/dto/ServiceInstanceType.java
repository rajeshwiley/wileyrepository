package com.wiley.integrations.sabrix.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ServiceInstanceType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="ServiceInstanceType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ProcessInstanceID" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="ServiceInstanceID" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="ComponentInstanceID" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="SenderCorrelationID" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="Implementation" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/CommonInfratypes/Codelist/V1}ImplementationType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceInstanceType", namespace = "http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/Meta/V1", propOrder = {
		"processInstanceID",
		"serviceInstanceID",
		"componentInstanceID",
		"senderCorrelationID",
		"implementation"
})
public class ServiceInstanceType
{

	@XmlElement(name = "ProcessInstanceID")
	protected String processInstanceID;
	@XmlElement(name = "ServiceInstanceID")
	protected String serviceInstanceID;
	@XmlElement(name = "ComponentInstanceID")
	protected String componentInstanceID;
	@XmlElement(name = "SenderCorrelationID")
	protected String senderCorrelationID;
	@XmlElement(name = "Implementation")
	@XmlSchemaType(name = "string")
	protected ImplementationType implementation;

	/**
	 * Gets the value of the processInstanceID property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getProcessInstanceID()
	{
		return processInstanceID;
	}

	/**
	 * Sets the value of the processInstanceID property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setProcessInstanceID(String value)
	{
		this.processInstanceID = value;
	}

	/**
	 * Gets the value of the serviceInstanceID property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getServiceInstanceID()
	{
		return serviceInstanceID;
	}

	/**
	 * Sets the value of the serviceInstanceID property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setServiceInstanceID(String value)
	{
		this.serviceInstanceID = value;
	}

	/**
	 * Gets the value of the componentInstanceID property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getComponentInstanceID()
	{
		return componentInstanceID;
	}

	/**
	 * Sets the value of the componentInstanceID property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setComponentInstanceID(String value)
	{
		this.componentInstanceID = value;
	}

	/**
	 * Gets the value of the senderCorrelationID property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getSenderCorrelationID()
	{
		return senderCorrelationID;
	}

	/**
	 * Sets the value of the senderCorrelationID property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setSenderCorrelationID(String value)
	{
		this.senderCorrelationID = value;
	}

	/**
	 * Gets the value of the implementation property.
	 *
	 * @return possible object is
	 * {@link ImplementationType }
	 */
	public ImplementationType getImplementation()
	{
		return implementation;
	}

	/**
	 * Sets the value of the implementation property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link ImplementationType }
	 */
	public void setImplementation(ImplementationType value)
	{
		this.implementation = value;
	}

}
