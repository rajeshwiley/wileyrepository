package com.wiley.integrations.common.aspect;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.aspectj.lang.JoinPoint;

import com.amazonaws.services.sqs.model.MessageAttributeValue;
import com.amazonaws.services.sqs.model.SendMessageBatchRequest;
import com.amazonaws.services.sqs.model.SendMessageBatchRequestEntry;


/**
 * Aspect was added due to target system couldn't process custom data types. For example, Spring Integration adds timestamp
 * header to the message with java.lang.Long type, that is transformed to SQS message attribute
 * with custom data type {@code Number.java.lang.Long}. To avoid issues during target system processing with such messages,
 * it was decided to transform types of such attributes into SQS String type.
 *
 * See https://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSDeveloperGuide/sqs-message-attributes.html
 */
public class SqsAttributesTypeTransformerAspect
{

	public void sendMessageBatch(final JoinPoint point)
	{
		SendMessageBatchRequest request = ((SendMessageBatchRequest) point.getArgs()[0]);
		if (request.getEntries() != null)
		{
			for (SendMessageBatchRequestEntry entry : request.getEntries())
			{
				transformType(entry, Arrays.asList("timestamp", "JdbcChannelMessageStore.CREATED_DATE"), "String");
			}
		}
	}

	private void transformType(final SendMessageBatchRequestEntry entry, final List<String> attributeKeys,
			final String targetSqsType)
	{
		Map<String, MessageAttributeValue> messageAttributes = entry.getMessageAttributes();
		if (messageAttributes == null)
		{
			return;
		}

		for (String attributeKey : attributeKeys)
		{
			MessageAttributeValue timestamp = messageAttributes.get(attributeKey);
			if (timestamp != null)
			{
				timestamp.setDataType(targetSqsType);
			}
		}
	}


}
