package com.wiley.integrations.wileycore.transformer;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.integration.transformer.GenericTransformer;
import org.springframework.messaging.MessagingException;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.oxm.XmlMappingException;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.xml.transform.StringSource;

import com.google.common.base.Preconditions;
import com.wiley.core.integration.wileycore.dto.Message;


/**
 * Transforms XML responses from Wiley Core to {@link Message} DTO objects.
 *
 * @author Gabor_Bata
 */
public class WileyCoreMessageTransformer implements GenericTransformer<String, Message>
{
	private static final Logger LOG = Logger.getLogger(WileyCoreMessageTransformer.class);

	private final Jaxb2Marshaller marshaller;

	public WileyCoreMessageTransformer(final Jaxb2Marshaller marshaller)
	{
		this.marshaller = Preconditions.checkNotNull(marshaller, "Marshaller must be set.");
	}

	@Override
	public Message transform(final String source)
	{
		Preconditions.checkState(StringUtils.isNotEmpty(source), "Source message should not be empty.");

		// Trimming the source message is required as Wiley Core responses contain unnecessary newlines
		StringSource stringSource = new StringSource(source.trim());
		try
		{
			return (Message) marshaller.unmarshal(stringSource);
		}
		catch (XmlMappingException e)
		{
			LOG.error("Could not transform XML message:" + source, e);
			throw new MessagingException(new GenericMessage<>(source), "Could not transform XML message", e);
		}
	}
}
