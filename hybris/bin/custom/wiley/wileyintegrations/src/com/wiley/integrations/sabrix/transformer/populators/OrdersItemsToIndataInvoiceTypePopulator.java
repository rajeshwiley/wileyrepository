package com.wiley.integrations.sabrix.transformer.populators;

import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.catalog.references.ProductReferenceService;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.util.Assert;

import com.wiley.core.enums.WileyProductSubtypeEnum;
import com.wiley.core.model.ExternalCompanyModel;
import com.wiley.core.product.WileyProductEditionFormatService;
import com.wiley.integrations.sabrix.dto.IndataInvoiceType;
import com.wiley.integrations.sabrix.dto.IndataLineType;
import com.wiley.integrations.sabrix.dto.ZoneAddressType;
import com.wiley.core.integration.sabrix.service.SetPriceSeparatorService;
import com.wiley.integrations.sabrix.transformer.populators.line.ExternalAddressToDeliveryCostLinePopulator;
import com.wiley.integrations.sabrix.transformer.populators.line.ExternalAddressToLinePopulator;
import com.wiley.integrations.sabrix.transformer.populators.line.IdToLinePopulator;
import com.wiley.integrations.sabrix.transformer.populators.line.ItemValueToLinePopulator;
import com.wiley.integrations.sabrix.transformer.populators.line.OrderItemToJournalLinePopulator;
import com.wiley.integrations.sabrix.transformer.populators.line.OrderItemToLinePopulator;
import com.wiley.integrations.sabrix.transformer.populators.line.OrderProductToLinePopulator;
import com.wiley.integrations.sabrix.transformer.populators.line.OrderToDeliveryCostLinePopulator;


/**
 * @author Dzmitryi_Halahayeu
 */
public class OrdersItemsToIndataInvoiceTypePopulator
		implements Populator<List<AbstractOrderEntryModel>, IndataInvoiceType>
{
	private static final String COMPANY_ROLE = "S";
	private static final String SABRIX_DATE_FORMAT = "yyyy-MM-dd";
	private static final String FORWARD_CALCULATION_DIRECTION = "F";
	private static final String IS_CREDIT = "false";
	private static final String IS_AUDITED = "false";
	//This value is used for marking it as optional
	public static final String INVOICE_NUMBER = "0";

	@Resource
	private AddressToZoneAddressTypePopulator addressToZoneAddressTypePopulator;
	@Resource
	private ItemValueToLinePopulator itemValueToLinePopulator;
	@Resource
	private IdToLinePopulator idToLinePopulator;
	@Resource
	private OrderItemToLinePopulator orderItemToLinePopulator;
	@Resource
	private OrderProductToLinePopulator orderProductToLinePopulator;
	@Resource
	private ExternalAddressToDeliveryCostLinePopulator externalAddressToDeliveryCostLinePopulator;
	@Resource
	private ExternalAddressToLinePopulator externalAddressToLinePopulator;
	@Resource
	private OrderToDeliveryCostLinePopulator orderToDeliveryCostLinePopulator;
	@Resource
	private OrderItemToJournalLinePopulator orderItemToJournalLinePopulator;
	@Resource
	private SetPriceSeparatorService setPriceSeparatorService;
	@Resource
	private ProductReferenceService productReferenceService;
	@Resource
	private WileyProductEditionFormatService wileyProductEditionFormatService;

	@Override
	public void populate(@Nonnull final List<AbstractOrderEntryModel> orderItems,
			@Nonnull final IndataInvoiceType indataInvoiceType)
			throws ConversionException
	{
		AbstractOrderModel order = orderItems.get(0).getOrder();
		ExternalCompanyModel externalCompany = orderItems.get(0).getProduct().getExternalCompany();
		Assert.notNull(externalCompany);
		long totalQuantity = order.getEntries().stream().mapToLong(AbstractOrderEntryModel::getQuantity).sum();
		indataInvoiceType.setEXTERNALCOMPANYID(externalCompany.getExternalId());
		ZoneAddressType billToAddress = new ZoneAddressType();
		addressToZoneAddressTypePopulator.populate(order.getPaymentAddress(), billToAddress);
		indataInvoiceType.setBILLTO(billToAddress);
		indataInvoiceType.setCALCULATIONDIRECTION(FORWARD_CALCULATION_DIRECTION);
		indataInvoiceType.setCOMPANYROLE(COMPANY_ROLE);
		indataInvoiceType.setCURRENCYCODE(order.getCurrency().getIsocode());
		indataInvoiceType.setINVOICENUMBER(INVOICE_NUMBER);
		indataInvoiceType.setINVOICEDATE(formatDate(order.getDate()));
		indataInvoiceType.setISCREDIT(IS_CREDIT);
		indataInvoiceType.setISAUDITED(IS_AUDITED);
		BigDecimal invoiceDeliveryCost = BigDecimal.ZERO;

		int index = 0;
		for (final AbstractOrderEntryModel orderEntry : orderItems)
		{
			BigDecimal orderEntryDeliveryCost = calculateOrderEntryDeliveryCost(order, totalQuantity, orderEntry);
			invoiceDeliveryCost = invoiceDeliveryCost.add(orderEntryDeliveryCost);

			final ProductModel product = orderEntry.getProduct();
			if (WileyProductSubtypeEnum.PRODUCT_SET.equals(product.getSubtype()))
			{
				final Collection<ProductModel> components = getComponents(product);
				final Double setPrice = orderEntry.getTaxableTotalPrice();
				final Map<ProductModel, Double> productPriceMap = setPriceSeparatorService.splitSetPriceOverProducts(components,
						setPrice);

				for (final Map.Entry<ProductModel, Double> entry : productPriceMap.entrySet())
				{
					IndataLineType indataLineType = createIndataLineType(index, externalCompany, orderEntry, entry.getKey(),
							entry.getValue());

					indataInvoiceType.getLINE().add(indataLineType);
					index++;
				}
			}
			else
			{
				final ProductModel productModel = orderEntry.getProduct();
				final Double totalPrice = orderEntry.getTaxableTotalPrice();

				IndataLineType indataLineType = createIndataLineType(index, externalCompany, orderEntry, productModel,
						totalPrice);
				indataInvoiceType.getLINE().add(indataLineType);
				index++;
			}
		}

		if (!wileyProductEditionFormatService.isDigitalCart(order) && invoiceDeliveryCost.compareTo(BigDecimal.ZERO) > 0)
		{
			invoiceDeliveryCost = invoiceDeliveryCost.setScale(2, BigDecimal.ROUND_HALF_UP);
			IndataLineType deliveryCostIndataLineType = new IndataLineType();
			idToLinePopulator.populate(index, deliveryCostIndataLineType);
			itemValueToLinePopulator.populate(invoiceDeliveryCost, deliveryCostIndataLineType);
			externalAddressToDeliveryCostLinePopulator.populate(externalCompany.getAddress(),
					deliveryCostIndataLineType);
			orderToDeliveryCostLinePopulator.populate(order, deliveryCostIndataLineType);
			indataInvoiceType.getLINE().add(deliveryCostIndataLineType);
		}
	}

	private IndataLineType createIndataLineType(final int index, final ExternalCompanyModel externalCompany,
			final AbstractOrderEntryModel order, final ProductModel product, final Double totalPrice)
	{
		IndataLineType indataLineType = new IndataLineType();
		idToLinePopulator.populate(index, indataLineType);
		externalAddressToLinePopulator.populate(externalCompany.getAddress(), indataLineType);
		itemValueToLinePopulator.populate(BigDecimal.valueOf(totalPrice), indataLineType);
		orderItemToLinePopulator.populate(order, indataLineType);
		orderProductToLinePopulator.populate(product, indataLineType);
		orderItemToJournalLinePopulator.populate(product, indataLineType);
		return indataLineType;
	}

	private Collection<ProductModel> getComponents(final ProductModel product)
	{
		final Collection<ProductReferenceModel> productReferences =
				productReferenceService.getProductReferencesForSourceProduct(product,
						ProductReferenceTypeEnum.PRODUCT_SET_COMPONENT, true);

		if (CollectionUtils.isNotEmpty(productReferences))
		{
			return productReferences.stream().map(ProductReferenceModel::getTarget).collect(
					Collectors.toList());
		}
		else
		{
			throw new IllegalStateException(
					String.format("Product references can't be found for product with isbn [%s]", product.getIsbn()));
		}
	}

	private BigDecimal calculateOrderEntryDeliveryCost(final AbstractOrderModel order, final long totalQuantity,
			final AbstractOrderEntryModel orderEntryModel)
	{
		return BigDecimal.valueOf(order.getDeliveryCost()).multiply(
				BigDecimal.valueOf(orderEntryModel.getQuantity())).divide(BigDecimal.valueOf(totalQuantity), 2,
				BigDecimal.ROUND_UP);
	}

	private String formatDate(final Date date)
	{
		return new SimpleDateFormat(SABRIX_DATE_FORMAT).format(date);
	}
}
