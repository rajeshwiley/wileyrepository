package com.wiley.integrations.users.data;

public class WileyUserSearchResponse implements java.io.Serializable
{

	private String firstName;
	private String lastName;
	private int code;
	private String message;

	public WileyUserSearchResponse()
	{
		// default constructor
	}

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(final String firstName)
	{
		this.firstName = firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(final String lastName)
	{
		this.lastName = lastName;
	}

	public int getCode()
	{
		return code;
	}

	public void setCode(final int code)
	{
		this.code = code;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage(final String message)
	{
		this.message = message;
	}

	@Override
	public String toString()
	{
		return "WileyUserSearchResponse [firstName=" + firstName + ", lastName="
				+ lastName + ", code=" + code + ", message=" + message + "]";
	}


}
