package com.wiley.integrations.customer.dto;

import com.fasterxml.jackson.annotation.JsonInclude;


@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProfileUpdateRequest
{
	private String active;
	private boolean enrolledInSchool;
	private String individualEcidNumber;
	private String titleCode;
	private String firstName;
	private String middleName;
	private String lastName;
	private String role;
	private String suffixCode;
	private String email;
	private String studentId;
	private String schoolId;
	private String major;
	private Integer graduationMonth;
	private Integer graduationYear;
	private String sapAccountNumber;
	private String organizationalEcidNumber;

	/**
	 * @return the active
	 */
	public String getActive()
	{
		return active;
	}

	/**
	 * @param active
	 *           the active to set
	 */
	public void setActive(final String active)
	{
		this.active = active;
	}

	/**
	 * @return the individualEcidNumber
	 */
	public String getIndividualEcidNumber()
	{
		return individualEcidNumber;
	}

	/**
	 * @param individualEcidNumber
	 *           the individualEcidNumber to set
	 */
	public void setIndividualEcidNumber(final String individualEcidNumber)
	{
		this.individualEcidNumber = individualEcidNumber;
	}

	/**
	 * @return the titleCode
	 */
	public String getTitleCode()
	{
		return titleCode;
	}

	/**
	 * @param titleCode
	 *           the titleCode to set
	 */
	public void setTitleCode(final String titleCode)
	{
		this.titleCode = titleCode;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName()
	{
		return firstName;
	}

	/**
	 * @param firstName
	 *           the firstName to set
	 */
	public void setFirstName(final String firstName)
	{
		this.firstName = firstName;
	}

	/**
	 * @return the middleName
	 */
	public String getMiddleName()
	{
		return middleName;
	}

	/**
	 * @param middleName
	 *           the middleName to set
	 */
	public void setMiddleName(final String middleName)
	{
		this.middleName = middleName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName()
	{
		return lastName;
	}

	/**
	 * @param lastName
	 *           the lastName to set
	 */
	public void setLastName(final String lastName)
	{
		this.lastName = lastName;
	}

	/**
	 * @return the role
	 */
	public String getRole()
	{
		return role;
	}

	/**
	 * @param role
	 *           the role to set
	 */
	public void setRole(final String role)
	{
		this.role = role;
	}

	/**
	 * @return the suffixCode
	 */
	public String getSuffixCode()
	{
		return suffixCode;
	}

	/**
	 * @param suffixCode
	 *           the suffixCode to set
	 */
	public void setSuffixCode(final String suffixCode)
	{
		this.suffixCode = suffixCode;
	}

	/**
	 * @return the email
	 */
	public String getEmail()
	{
		return email;
	}

	/**
	 * @param email
	 *           the email to set
	 */
	public void setEmail(final String email)
	{
		this.email = email;
	}

	/**
	 * @return the studentId
	 */
	public String getStudentId()
	{
		return studentId;
	}

	/**
	 * @param studentId
	 *           the studentId to set
	 */
	public void setStudentId(final String studentId)
	{
		this.studentId = studentId;
	}

	/**
	 * @return the enrolledInSchool
	 */
	public boolean isEnrolledInSchool()
	{
		return enrolledInSchool;
	}

	/**
	 * @param enrolledInSchool
	 *           the enrolledInSchool to set
	 */
	public void setEnrolledInSchool(final boolean enrolledInSchool)
	{
		this.enrolledInSchool = enrolledInSchool;
	}

	/**
	 * @return the major
	 */
	public String getMajor()
	{
		return major;
	}

	/**
	 * @param major
	 *           the major to set
	 */
	public void setMajor(final String major)
	{
		this.major = major;
	}

	/**
	 * @return the graduationMonth
	 */
	public Integer getGraduationMonth()
	{
		return graduationMonth;
	}

	/**
	 * @param graduationMonth
	 *           the graduationMonth to set
	 */
	public void setGraduationMonth(final Integer graduationMonth)
	{
		this.graduationMonth = graduationMonth;
	}

	/**
	 * @return the graduationYear
	 */
	public Integer getGraduationYear()
	{
		return graduationYear;
	}

	/**
	 * @param graduationYear
	 *           the graduationYear to set
	 */
	public void setGraduationYear(final Integer graduationYear)
	{
		this.graduationYear = graduationYear;
	}

	/**
	 * @return the sapAccountNumber
	 */
	public String getSapAccountNumber()
	{
		return sapAccountNumber;
	}

	/**
	 * @param sapAccountNumber
	 *           the sapAccountNumber to set
	 */
	public void setSapAccountNumber(final String sapAccountNumber)
	{
		this.sapAccountNumber = sapAccountNumber;
	}

	/**
	 * @return the organizationalEcidNumber
	 */
	public String getOrganizationalEcidNumber()
	{
		return organizationalEcidNumber;
	}

	/**
	 * @param organizationalEcidNumber
	 *           the organizationalEcidNumber to set
	 */
	public void setOrganizationalEcidNumber(final String organizationalEcidNumber)
	{
		this.organizationalEcidNumber = organizationalEcidNumber;
	}

	/**
	 * @return the schoolId
	 */
	public String getSchoolId()
	{
		return schoolId;
	}

	/**
	 * @param schoolId
	 *           the schoolId to set
	 */
	public void setSchoolId(final String schoolId)
	{
		this.schoolId = schoolId;
	}
}
