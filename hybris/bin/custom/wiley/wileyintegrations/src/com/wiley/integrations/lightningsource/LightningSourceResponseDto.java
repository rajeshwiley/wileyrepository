package com.wiley.integrations.lightningsource;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Created by Georgii_Gavrysh on 9/7/2016.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LightningSourceResponseDto
{
	@JsonProperty("IPWSeBookDownloadURL")
	private String iPWSeBookDownloadURL;

	@JsonProperty("Error")
	private String errorMessage;

	public String getIPWSeBookDownloadURL()
	{
		return iPWSeBookDownloadURL;
	}

	public void setIPWSeBookDownloadURL(final String iPWSeBookDownloadURL)
	{
		this.iPWSeBookDownloadURL = iPWSeBookDownloadURL;
	}

	public String getErrorMessage()
	{
		return errorMessage;
	}

	public void setErrorMessage(final String errorMessage)
	{
		this.errorMessage = errorMessage;
	}

	@Override
	public String toString()
	{
		return "LightningSourceResponseDto {iPWSeBookDownloadURL=[" + iPWSeBookDownloadURL
				+ "], errorMessage=[" + errorMessage + "]}";
	}
}
