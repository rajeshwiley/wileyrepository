package com.wiley.integrations.sabrix.dto;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the com.wiley.integrations.sabrix.dto package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class ObjectFactory
{

	private final static QName _EBMHeader_QNAME = new QName("http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/Meta/V1",
			"EBMHeader");
	private final static QName _Fault_QNAME = new QName("http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/Meta/V1",
			"Fault");
	private final static QName _TaxCalculationFaultEBM_QNAME = new QName(
			"http://xmlns.wiley.com/EnterpriseBusinessObjects/TaxEBO/V1", "TaxCalculationFaultEBM");
	private final static QName _TaxCalculationRequestEBM_QNAME = new QName(
			"http://xmlns.wiley.com/EnterpriseBusinessObjects/TaxEBO/V1", "TaxCalculationRequestEBM");
	private final static QName _TaxCalculationResponseEBM_QNAME = new QName(
			"http://xmlns.wiley.com/EnterpriseBusinessObjects/TaxEBO/V1", "TaxCalculationResponseEBM");
	private final static QName _TaxCalculationFault_QNAME = new QName(
			"http://www.sabrix.com/services/taxcalculationservice/2011-09-01", "taxCalculationFault");
	private final static QName _TaxCalculationRequest_QNAME = new QName(
			"http://www.sabrix.com/services/taxcalculationservice/2011-09-01", "taxCalculationRequest");
	private final static QName _TaxCalculationResponse_QNAME = new QName(
			"http://www.sabrix.com/services/taxcalculationservice/2011-09-01", "taxCalculationResponse");

	/**
	 * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.wiley.integrations.sabrix.dto
	 */
	public ObjectFactory()
	{
	}

	/**
	 * Create an instance of {@link EBMHeaderType }
	 */
	public EBMHeaderType createEBMHeaderType()
	{
		return new EBMHeaderType();
	}

	/**
	 * Create an instance of {@link FaultType }
	 */
	public FaultType createFaultType()
	{
		return new FaultType();
	}

	/**
	 * Create an instance of {@link ErrorResponse }
	 */
	public ErrorResponse createErrorResponse()
	{
		return new ErrorResponse();
	}

	/**
	 * Create an instance of {@link EventMetaDataType }
	 */
	public EventMetaDataType createEventMetaDataType()
	{
		return new EventMetaDataType();
	}

	/**
	 * Create an instance of {@link FaultMessageType }
	 */
	public FaultMessageType createFaultMessageType()
	{
		return new FaultMessageType();
	}

	/**
	 * Create an instance of {@link GetQueryResponseType }
	 */
	public GetQueryResponseType createGetQueryResponseType()
	{
		return new GetQueryResponseType();
	}

	/**
	 * Create an instance of {@link GetQueryType }
	 */
	public GetQueryType createGetQueryType()
	{
		return new GetQueryType();
	}

	/**
	 * Create an instance of {@link PagingCriteriaType }
	 */
	public PagingCriteriaType createPagingCriteriaType()
	{
		return new PagingCriteriaType();
	}

	/**
	 * Create an instance of {@link SearchQueryType }
	 */
	public SearchQueryType createSearchQueryType()
	{
		return new SearchQueryType();
	}

	/**
	 * Create an instance of {@link SearchResponseHeaderType }
	 */
	public SearchResponseHeaderType createSearchResponseHeaderType()
	{
		return new SearchResponseHeaderType();
	}

	/**
	 * Create an instance of {@link ServiceContextType }
	 */
	public ServiceContextType createServiceContextType()
	{
		return new ServiceContextType();
	}

	/**
	 * Create an instance of {@link ServiceInstanceType }
	 */
	public ServiceInstanceType createServiceInstanceType()
	{
		return new ServiceInstanceType();
	}

	/**
	 * Create an instance of {@link ServiceResponseType }
	 */
	public ServiceResponseType createServiceResponseType()
	{
		return new ServiceResponseType();
	}

	/**
	 * Create an instance of {@link SortCriteriaType }
	 */
	public SortCriteriaType createSortCriteriaType()
	{
		return new SortCriteriaType();
	}

	/**
	 * Create an instance of {@link TargetSystemType }
	 */
	public TargetSystemType createTargetSystemType()
	{
		return new TargetSystemType();
	}

	/**
	 * Create an instance of {@link TaxCalculationFaultEBMType }
	 */
	public TaxCalculationFaultEBMType createTaxCalculationFaultEBMType()
	{
		return new TaxCalculationFaultEBMType();
	}

	/**
	 * Create an instance of {@link TaxCalculationRequestEBMType }
	 */
	public TaxCalculationRequestEBMType createTaxCalculationRequestEBMType()
	{
		return new TaxCalculationRequestEBMType();
	}

	/**
	 * Create an instance of {@link TaxCalculationResponseEBMType }
	 */
	public TaxCalculationResponseEBMType createTaxCalculationResponseEBMType()
	{
		return new TaxCalculationResponseEBMType();
	}

	/**
	 * Create an instance of {@link UserDataType }
	 */
	public UserDataType createUserDataType()
	{
		return new UserDataType();
	}

	/**
	 * Create an instance of {@link ArrayOfNameValuePairType }
	 */
	public ArrayOfNameValuePairType createArrayOfNameValuePairType()
	{
		return new ArrayOfNameValuePairType();
	}

	/**
	 * Create an instance of {@link AuditFieldType }
	 */
	public AuditFieldType createAuditFieldType()
	{
		return new AuditFieldType();
	}

	/**
	 * Create an instance of {@link BinaryObjectType }
	 */
	public BinaryObjectType createBinaryObjectType()
	{
		return new BinaryObjectType();
	}

	/**
	 * Create an instance of {@link CodeType }
	 */
	public CodeType createCodeType()
	{
		return new CodeType();
	}

	/**
	 * Create an instance of {@link ExtensionType }
	 */
	public ExtensionType createExtensionType()
	{
		return new ExtensionType();
	}

	/**
	 * Create an instance of {@link IdentifierType }
	 */
	public IdentifierType createIdentifierType()
	{
		return new IdentifierType();
	}

	/**
	 * Create an instance of {@link NameType }
	 */
	public NameType createNameType()
	{
		return new NameType();
	}

	/**
	 * Create an instance of {@link NameValuePairType }
	 */
	public NameValuePairType createNameValuePairType()
	{
		return new NameValuePairType();
	}

	/**
	 * Create an instance of {@link PayloadType }
	 */
	public PayloadType createPayloadType()
	{
		return new PayloadType();
	}

	/**
	 * Create an instance of {@link SystemNameType }
	 */
	public SystemNameType createSystemNameType()
	{
		return new SystemNameType();
	}

	/**
	 * Create an instance of {@link TextType }
	 */
	public TextType createTextType()
	{
		return new TextType();
	}

	/**
	 * Create an instance of {@link TaxCalculationFault }
	 */
	public TaxCalculationFault createTaxCalculationFault()
	{
		return new TaxCalculationFault();
	}

	/**
	 * Create an instance of {@link TaxCalculationRequest }
	 */
	public TaxCalculationRequest createTaxCalculationRequest()
	{
		return new TaxCalculationRequest();
	}

	/**
	 * Create an instance of {@link TaxCalculationResponse }
	 */
	public TaxCalculationResponse createTaxCalculationResponse()
	{
		return new TaxCalculationResponse();
	}

	/**
	 * Create an instance of {@link AddressType }
	 */
	public AddressType createAddressType()
	{
		return new AddressType();
	}

	/**
	 * Create an instance of {@link AmountType }
	 */
	public AmountType createAmountType()
	{
		return new AmountType();
	}

	/**
	 * Create an instance of {@link AuthorityAttributeType }
	 */
	public AuthorityAttributeType createAuthorityAttributeType()
	{
		return new AuthorityAttributeType();
	}

	/**
	 * Create an instance of {@link CertificateLocationType }
	 */
	public CertificateLocationType createCertificateLocationType()
	{
		return new CertificateLocationType();
	}

	/**
	 * Create an instance of {@link CommonAddressType }
	 */
	public CommonAddressType createCommonAddressType()
	{
		return new CommonAddressType();
	}

	/**
	 * Create an instance of {@link ConvertedCurrencyAmountType }
	 */
	public ConvertedCurrencyAmountType createConvertedCurrencyAmountType()
	{
		return new ConvertedCurrencyAmountType();
	}

	/**
	 * Create an instance of {@link CurrencyConversionStepType }
	 */
	public CurrencyConversionStepType createCurrencyConversionStepType()
	{
		return new CurrencyConversionStepType();
	}

	/**
	 * Create an instance of {@link CurrencyConversionType }
	 */
	public CurrencyConversionType createCurrencyConversionType()
	{
		return new CurrencyConversionType();
	}

	/**
	 * Create an instance of {@link EstablishmentsLocationType }
	 */
	public EstablishmentsLocationType createEstablishmentsLocationType()
	{
		return new EstablishmentsLocationType();
	}

	/**
	 * Create an instance of {@link EstablishmentsType }
	 */
	public EstablishmentsType createEstablishmentsType()
	{
		return new EstablishmentsType();
	}

	/**
	 * Create an instance of {@link FeeType }
	 */
	public FeeType createFeeType()
	{
		return new FeeType();
	}

	/**
	 * Create an instance of {@link FlagAddressType }
	 */
	public FlagAddressType createFlagAddressType()
	{
		return new FlagAddressType();
	}

	/**
	 * Create an instance of {@link HostRequestInfoType }
	 */
	public HostRequestInfoType createHostRequestInfoType()
	{
		return new HostRequestInfoType();
	}

	/**
	 * Create an instance of {@link InclusiveTaxIndicatorsType }
	 */
	public InclusiveTaxIndicatorsType createInclusiveTaxIndicatorsType()
	{
		return new InclusiveTaxIndicatorsType();
	}

	/**
	 * Create an instance of {@link IndataInvoiceType }
	 */
	public IndataInvoiceType createIndataInvoiceType()
	{
		return new IndataInvoiceType();
	}

	/**
	 * Create an instance of {@link IndataInvoiceTypeResponse }
	 */
	public IndataInvoiceTypeResponse createIndataInvoiceTypeResponse()
	{
		return new IndataInvoiceTypeResponse();
	}

	/**
	 * Create an instance of {@link IndataLicensesDetailType }
	 */
	public IndataLicensesDetailType createIndataLicensesDetailType()
	{
		return new IndataLicensesDetailType();
	}

	/**
	 * Create an instance of {@link IndataLicensesType }
	 */
	public IndataLicensesType createIndataLicensesType()
	{
		return new IndataLicensesType();
	}

	/**
	 * Create an instance of {@link IndataLineType }
	 */
	public IndataLineType createIndataLineType()
	{
		return new IndataLineType();
	}

	/**
	 * Create an instance of {@link IndataLineTypeResponse }
	 */
	public IndataLineTypeResponse createIndataLineTypeResponse()
	{
		return new IndataLineTypeResponse();
	}

	/**
	 * Create an instance of {@link IndataType }
	 */
	public IndataType createIndataType()
	{
		return new IndataType();
	}

	/**
	 * Create an instance of {@link IndataTypeResponse }
	 */
	public IndataTypeResponse createIndataTypeResponse()
	{
		return new IndataTypeResponse();
	}

	/**
	 * Create an instance of {@link LocationNameType }
	 */
	public LocationNameType createLocationNameType()
	{
		return new LocationNameType();
	}

	/**
	 * Create an instance of {@link MessageType }
	 */
	public MessageType createMessageType()
	{
		return new MessageType();
	}

	/**
	 * Create an instance of {@link OutdataAdvisoriesType }
	 */
	public OutdataAdvisoriesType createOutdataAdvisoriesType()
	{
		return new OutdataAdvisoriesType();
	}

	/**
	 * Create an instance of {@link OutdataErrorType }
	 */
	public OutdataErrorType createOutdataErrorType()
	{
		return new OutdataErrorType();
	}

	/**
	 * Create an instance of {@link OutdataInvoiceType }
	 */
	public OutdataInvoiceType createOutdataInvoiceType()
	{
		return new OutdataInvoiceType();
	}

	/**
	 * Create an instance of {@link OutdataLicenseType }
	 */
	public OutdataLicenseType createOutdataLicenseType()
	{
		return new OutdataLicenseType();
	}

	/**
	 * Create an instance of {@link OutdataLicensesType }
	 */
	public OutdataLicensesType createOutdataLicensesType()
	{
		return new OutdataLicensesType();
	}

	/**
	 * Create an instance of {@link OutdataLineType }
	 */
	public OutdataLineType createOutdataLineType()
	{
		return new OutdataLineType();
	}

	/**
	 * Create an instance of {@link OutdataRequestStatusType }
	 */
	public OutdataRequestStatusType createOutdataRequestStatusType()
	{
		return new OutdataRequestStatusType();
	}

	/**
	 * Create an instance of {@link OutdataTaxSummaryType }
	 */
	public OutdataTaxSummaryType createOutdataTaxSummaryType()
	{
		return new OutdataTaxSummaryType();
	}

	/**
	 * Create an instance of {@link OutdataTaxType }
	 */
	public OutdataTaxType createOutdataTaxType()
	{
		return new OutdataTaxType();
	}

	/**
	 * Create an instance of {@link OutdataType }
	 */
	public OutdataType createOutdataType()
	{
		return new OutdataType();
	}

	/**
	 * Create an instance of {@link QuantitiesType }
	 */
	public QuantitiesType createQuantitiesType()
	{
		return new QuantitiesType();
	}

	/**
	 * Create an instance of {@link QuantityType }
	 */
	public QuantityType createQuantityType()
	{
		return new QuantityType();
	}

	/**
	 * Create an instance of {@link RegistrationsType }
	 */
	public RegistrationsType createRegistrationsType()
	{
		return new RegistrationsType();
	}

	/**
	 * Create an instance of {@link UomConversionType }
	 */
	public UomConversionType createUomConversionType()
	{
		return new UomConversionType();
	}

	/**
	 * Create an instance of {@link UserElementType }
	 */
	public UserElementType createUserElementType()
	{
		return new UserElementType();
	}

	/**
	 * Create an instance of {@link ZoneAddressType }
	 */
	public ZoneAddressType createZoneAddressType()
	{
		return new ZoneAddressType();
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link EBMHeaderType }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/Meta/V1", name = "EBMHeader")
	public JAXBElement<EBMHeaderType> createEBMHeader(EBMHeaderType value)
	{
		return new JAXBElement<EBMHeaderType>(_EBMHeader_QNAME, EBMHeaderType.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link FaultType }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/Meta/V1", name = "Fault")
	public JAXBElement<FaultType> createFault(FaultType value)
	{
		return new JAXBElement<FaultType>(_Fault_QNAME, FaultType.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link TaxCalculationFaultEBMType }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://xmlns.wiley.com/EnterpriseBusinessObjects/TaxEBO/V1", name = "TaxCalculationFaultEBM")
	public JAXBElement<TaxCalculationFaultEBMType> createTaxCalculationFaultEBM(TaxCalculationFaultEBMType value)
	{
		return new JAXBElement<TaxCalculationFaultEBMType>(_TaxCalculationFaultEBM_QNAME, TaxCalculationFaultEBMType.class, null,
				value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link TaxCalculationRequestEBMType }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://xmlns.wiley.com/EnterpriseBusinessObjects/TaxEBO/V1", name = "TaxCalculationRequestEBM")
	public JAXBElement<TaxCalculationRequestEBMType> createTaxCalculationRequestEBM(TaxCalculationRequestEBMType value)
	{
		return new JAXBElement<TaxCalculationRequestEBMType>(_TaxCalculationRequestEBM_QNAME, TaxCalculationRequestEBMType.class,
				null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link TaxCalculationResponseEBMType }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://xmlns.wiley.com/EnterpriseBusinessObjects/TaxEBO/V1", name = "TaxCalculationResponseEBM")
	public JAXBElement<TaxCalculationResponseEBMType> createTaxCalculationResponseEBM(TaxCalculationResponseEBMType value)
	{
		return new JAXBElement<TaxCalculationResponseEBMType>(_TaxCalculationResponseEBM_QNAME,
				TaxCalculationResponseEBMType.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link TaxCalculationFault }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://www.sabrix.com/services/taxcalculationservice/2011-09-01", name = "taxCalculationFault")
	public JAXBElement<TaxCalculationFault> createTaxCalculationFault(TaxCalculationFault value)
	{
		return new JAXBElement<TaxCalculationFault>(_TaxCalculationFault_QNAME, TaxCalculationFault.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link TaxCalculationRequest }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://www.sabrix.com/services/taxcalculationservice/2011-09-01", name = "taxCalculationRequest")
	public JAXBElement<TaxCalculationRequest> createTaxCalculationRequest(TaxCalculationRequest value)
	{
		return new JAXBElement<TaxCalculationRequest>(_TaxCalculationRequest_QNAME, TaxCalculationRequest.class, null, value);
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link TaxCalculationResponse }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://www.sabrix.com/services/taxcalculationservice/2011-09-01", name = "taxCalculationResponse")
	public JAXBElement<TaxCalculationResponse> createTaxCalculationResponse(TaxCalculationResponse value)
	{
		return new JAXBElement<TaxCalculationResponse>(_TaxCalculationResponse_QNAME, TaxCalculationResponse.class, null, value);
	}

}
