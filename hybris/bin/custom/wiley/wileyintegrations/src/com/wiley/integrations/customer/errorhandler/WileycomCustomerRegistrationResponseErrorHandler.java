package com.wiley.integrations.customer.errorhandler;

import java.io.IOException;

import org.springframework.http.client.ClientHttpResponse;

import com.wiley.core.exceptions.ExternalSystemConflictException;
import com.wiley.core.wileycom.customer.exception.WileycomCustomerRegistrationConflictingIdException;
import com.wiley.integrations.handler.ExternalSystemResponseErrorHandler;


public class WileycomCustomerRegistrationResponseErrorHandler extends ExternalSystemResponseErrorHandler
{
	@Override
	public void handleError(final ClientHttpResponse response) throws IOException
	{
		try
		{
			super.handleError(response);
		}
		catch (ExternalSystemConflictException exception)
		{
			throw new WileycomCustomerRegistrationConflictingIdException(exception);
		}
	}
}
