package com.wiley.integrations.tax.dto;

import java.util.List;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;


public class TaxCalculationResponseEntry
{
	@JsonProperty("id")
	@NotNull
	@Size(max = 255)
	private String id;

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		final TaxCalculationResponseEntry that = (TaxCalculationResponseEntry) o;
		return Objects.equals(id, that.id)
				&& Objects.equals(taxes, that.taxes);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(id, taxes);
	}

	@JsonProperty("taxes")
	@NotNull
	@Valid
	private List<TaxValue> taxes;

	/**
	 * An arbitrary identifier to bind the input entry to the tax calculation result.
	 *
	 * @return id
	 **/
	public String getId()
	{
		return id;
	}

	public void setId(final String id)
	{
		this.id = id;
	}

	/**
	 * List of the tax values calculated for the cart/order entry.
	 *
	 * @return taxes
	 **/
	public List<TaxValue> getTaxes()
	{
		return taxes;
	}

	public void setTaxes(final List<TaxValue> taxes)
	{
		this.taxes = taxes;
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("class TaxCalculationResponseEntry {\n");

		sb.append("    id: ").append(toIndentedString(id)).append("\n");
		sb.append("    taxes: ").append(toIndentedString(taxes)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(final Object o)
	{
		if (o == null)
		{
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}

