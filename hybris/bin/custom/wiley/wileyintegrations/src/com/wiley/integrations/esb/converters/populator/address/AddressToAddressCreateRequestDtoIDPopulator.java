package com.wiley.integrations.esb.converters.populator.address;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.springframework.util.Assert;

import com.wiley.integrations.esb.dto.EsbAddressCreateRequestDto;


public class AddressToAddressCreateRequestDtoIDPopulator implements Populator<AddressModel, EsbAddressCreateRequestDto>
{
	@Override
	public void populate(final AddressModel addressModel, final EsbAddressCreateRequestDto addressCreateRequestDto)
			throws ConversionException
	{
		Assert.notNull(addressModel);
		Assert.notNull(addressCreateRequestDto);

		addressCreateRequestDto.setAddressId(addressModel.getExternalId());
	}
}