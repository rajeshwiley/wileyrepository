package com.wiley.integrations.esb.converters.populator.address;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.springframework.util.Assert;

import com.wiley.integrations.esb.dto.EsbAddressCreateRequestDto;

public class AddressToAddressCreateRequestDtoPopulator implements Populator<AddressModel, EsbAddressCreateRequestDto>
{
	@Override
	public void populate(final AddressModel addressModel, final EsbAddressCreateRequestDto addressCreateRequestDto)
			throws ConversionException
	{
		Assert.notNull(addressModel);
		Assert.notNull(addressCreateRequestDto);

		addressCreateRequestDto.setPostcode(addressModel.getPostalcode());
		addressCreateRequestDto.setCountry(addressModel.getCountry().getIsocode());
		final RegionModel regionIso = addressModel.getRegion();
		if (regionIso != null)
		{
			addressCreateRequestDto.setState(regionIso.getIsocodeShort());
		}
		addressCreateRequestDto.setCity(addressModel.getTown());
		addressCreateRequestDto.setLine1(addressModel.getLine1());
		addressCreateRequestDto.setLine2(addressModel.getLine2());
		addressCreateRequestDto.setFirstName(addressModel.getFirstname());
		addressCreateRequestDto.setLastName(addressModel.getLastname());
		addressCreateRequestDto.setPhoneNumber(addressModel.getPhone1());
	}
}