package com.wiley.integrations.users.data;

import java.util.Objects;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserModificationWsDto
{
	@NotNull
	@Size(max = 255)
	private String userId;

	@Size(max = 255)
	private String firstName;

	@Size(max = 255)
	private String lastName;

	@Size(max = 255)
	private String email;

	/**
	 * The unique customer identifier
	 */
	@JsonProperty("userId")
	public String getUserId()
	{
		return userId;
	}

	public void setUserId(final String userId)
	{
		this.userId = userId;
	}

	/**
	 * The first name of the customer
	 */
	@JsonProperty("firstName")
	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(final String firstName)
	{
		this.firstName = firstName;
	}

	/**
	 * The last name of the customer
	 */
	@JsonProperty("lastName")
	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(final String lastName)
	{
		this.lastName = lastName;
	}

	/**
	 * The email of the customer
	 */
	@JsonProperty("email")
	public String getEmail()
	{
		return email;
	}

	public void setEmail(final String email)
	{
		this.email = email;
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		final UserModificationWsDto that = (UserModificationWsDto) o;
		return Objects.equals(userId, that.userId)
				&& Objects.equals(firstName, that.firstName)
				&& Objects.equals(lastName, that.lastName)
				&& Objects.equals(email, that.email);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(userId, firstName, lastName, email);
	}

	@Override
	public String toString()
	{
		return "UserModificationWsDto{"
				+ "userId='" + userId + '\''
				+ ", firstName='" + firstName + '\''
				+ ", lastName='" + lastName + '\''
				+ ", email='" + email + '\''
				+ '}';
	}
}
