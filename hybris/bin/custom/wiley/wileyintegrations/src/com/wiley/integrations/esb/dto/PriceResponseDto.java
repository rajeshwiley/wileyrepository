package com.wiley.integrations.esb.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.common.base.MoreObjects;
import com.wiley.integrations.esb.parsing.PriceResponseDoubleAdapter;


/**
 * Dto for delivery cost value in ERP.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PriceResponseDto
{
	@XmlElement(required = true)
	private String currencyIso;

	@XmlElement(required = true)
	@XmlJavaTypeAdapter(PriceResponseDoubleAdapter.class)
	private Double price;

	@XmlElement(required = true)
	private Boolean netto;

	public PriceResponseDto() { }

	public PriceResponseDto(final String currencyIso, final double price, final boolean netto)
	{
		this.currencyIso = currencyIso;
		this.price = price;
		this.netto = netto;
	}

	public String getCurrencyIso()
	{
		return currencyIso;
	}

	public void setCurrencyIso(final String currencyIso)
	{
		this.currencyIso = currencyIso;
	}

	public Double getPrice()
	{
		return price;
	}

	public void setPrice(final Double price)
	{
		this.price = price;
	}

	public Boolean getNetto()
	{
		return netto;
	}

	public void setNetto(final Boolean netto)
	{
		this.netto = netto;
	}

	@Override
	public String toString()
	{
		return MoreObjects.toStringHelper(this)
				.add("currencyIso", currencyIso)
				.add("price", price)
				.add("netto", netto)
				.toString();
	}
}
