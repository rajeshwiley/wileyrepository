package com.wiley.integrations.selector.strategies.impl;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import javax.annotation.Resource;

import com.wiley.core.store.WileyBaseStoreService;
import com.wiley.integrations.selector.strategies.WileyStoreSelectorStrategy;


/**
 * Created by Uladzimir_Barouski on 1/18/2017.
 */
public abstract class WileyAbstractStoreSelectorStrategy implements WileyStoreSelectorStrategy
{
	@Resource
	private WileyBaseStoreService wileyBaseStoreService;

	protected Optional<BaseStoreModel> findBaseStoreForSite(final BaseSiteModel currentBaseSite)
	{
		if (currentBaseSite != null)
		{
			return wileyBaseStoreService.getBaseStoreForBaseSite(currentBaseSite);
		}
		return Optional.empty();
	}
}
