package com.wiley.integrations.alm.strategy;

import org.springframework.security.core.userdetails.UserDetails;


public interface AlmUserAutoCreationStrategy
{
	UserDetails createNewUserByAlmId(String almId);
}
