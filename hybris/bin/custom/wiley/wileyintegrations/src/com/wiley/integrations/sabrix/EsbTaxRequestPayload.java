package com.wiley.integrations.sabrix;

import de.hybris.platform.core.model.order.AbstractOrderModel;

import com.wiley.integrations.sabrix.cacheddto.EsbTaxRequest;


public class EsbTaxRequestPayload
{
	private EsbTaxRequest cashedRequest;
	private AbstractOrderModel order;

	public EsbTaxRequestPayload(final EsbTaxRequest request, final AbstractOrderModel order)
	{
		this.cashedRequest = request;
		this.order = order;
	}

	public EsbTaxRequest getCashedRequest()
	{
		return cashedRequest;
	}

	public AbstractOrderModel getOrder()
	{
		return order;
	}


}
