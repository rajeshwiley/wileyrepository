package com.wiley.integrations.ediintegration.endpoints.jaxb;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlRootElement(name = "PurchaseOrderToWILEY")
@XmlType(propOrder = {"routing", "purchaseOrderList"})
public class PurchaseOrderToWILEY
{
	private Routing routing;
	private List<PurchaseOrder> purchaseOrderList;

	@XmlElement(name = "Routing")
	public Routing getRouting()
	{
		return routing;
	}
	public void setRouting(final Routing routing)
	{
		this.routing = routing;
	}
	@XmlElement(name = "PurchaseOrder")
	public List<PurchaseOrder> getPurchaseOrderList()
	{
		return purchaseOrderList;
	}

	public void setPurchaseOrderList(
			final List<PurchaseOrder> purchaseOrderList)
	{
		this.purchaseOrderList = purchaseOrderList;
	}
}
