package com.wiley.integrations.esb.transformer.inventory;

import de.hybris.platform.basecommerce.enums.StockLevelStatus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.wiley.core.product.data.ExternalInventoryStatus;
import com.wiley.core.product.data.ExternalInventoryStatusRecord;
import com.wiley.core.util.WileyDateUtils;
import com.wiley.integrations.esb.dto.verifyandcalculatecart.InventoryStatus;
import com.wiley.integrations.esb.strategies.InventoryStatusToStockLevelStatusStrategy;
import com.wiley.integrations.inventory.status.InventoryCheckResponse;
import com.wiley.integrations.inventory.status.InventoryCheckResponseEntry;



/**
 * Created by Mikhail_Asadchy on 7/29/2016.
 */
public class InventoryCheckResponseToMapTransformer
{
	@Resource
	private InventoryStatusToStockLevelStatusStrategy inventoryStatusToStockLevelStatusStrategy;

	@Nonnull
	public Map<String, ExternalInventoryStatus> transform(@Nonnull final InventoryCheckResponse inventoryCheckResponse)
	{
		Assert.notNull(inventoryCheckResponse);

		final Map<String, ExternalInventoryStatus> map = new HashMap<>();
		final List<InventoryCheckResponseEntry> entries = inventoryCheckResponse.getEntries();

		for (final InventoryCheckResponseEntry entry : entries)
		{
			final List<ExternalInventoryStatusRecord> inventoryStatusRecords = new ArrayList<>();
			final List<InventoryStatus> inventoryStatuses = entry.getStatuses();
			for (final InventoryStatus inventoryStatus : inventoryStatuses)
			{
				final ExternalInventoryStatusRecord record = convertInventoryStatusToInventoryStatusRecord(inventoryStatus);
				inventoryStatusRecords.add(record);
			}
			final ExternalInventoryStatus externalInventoryStatus = new ExternalInventoryStatus();
			externalInventoryStatus.setEstimatedDeliveryDays(getEstimatedDeliveryDate(inventoryStatusRecords));
			externalInventoryStatus.setRecordsList(inventoryStatusRecords);
			map.put(entry.getSapProductCode(), externalInventoryStatus);
		}

		return map;
	}

	@Nullable
	protected Long getEstimatedDeliveryDate(final List<ExternalInventoryStatusRecord> inventoryStatusRecords)
	{
		final List<Date> dates = getDates(inventoryStatusRecords);
		if (dates.size() > 0) {
			Collections.sort(dates);
			final long dateDiff = WileyDateUtils.getDateDiff(new Date(), dates.get(dates.size() - 1), TimeUnit.DAYS);
			if (dateDiff >= 0)
			{
				return dateDiff;
			}
		}
		return null;
	}

	protected ExternalInventoryStatusRecord convertInventoryStatusToInventoryStatusRecord(final InventoryStatus inventoryStatus)
	{
		final ExternalInventoryStatusRecord record = new ExternalInventoryStatusRecord();
		final StockLevelStatus externalInventoryStatus =
				inventoryStatusToStockLevelStatusStrategy.getExternalInventoryStatus(inventoryStatus.getStatus());
		record.setStatusCode(externalInventoryStatus);
		record.setQuantity(inventoryStatus.getQuantity().longValue());
		record.setAvailableDate(inventoryStatus.getEstimatedDeliveryDate());
		return record;
	}

	private List<Date> getDates(final List<ExternalInventoryStatusRecord> inventoryStatuses)
	{
		final List<Date> dates = new ArrayList<>();
		for (final ExternalInventoryStatusRecord inventoryStatus : inventoryStatuses)
		{
			final Date availableDate = inventoryStatus.getAvailableDate();
			if (availableDate != null)
			{
				dates.add(availableDate);
			}
		}
		return dates;
	}

}
