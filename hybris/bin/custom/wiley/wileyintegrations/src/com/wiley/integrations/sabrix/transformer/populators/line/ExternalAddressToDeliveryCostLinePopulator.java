package com.wiley.integrations.sabrix.transformer.populators.line;

import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import com.wiley.integrations.sabrix.dto.IndataLineType;
import com.wiley.integrations.sabrix.dto.ZoneAddressType;
import com.wiley.integrations.sabrix.transformer.populators.AddressToZoneAddressTypePopulator;


/**
 * @author Dzmitryi_Halahayeu
 */
public class ExternalAddressToDeliveryCostLinePopulator extends ExternalAddressToLinePopulator
{

	@Resource
	private AddressToZoneAddressTypePopulator addressToZoneAddressTypePopulator;

	@Override
	public void populate(@Nonnull final AddressModel externalCompanyAddress, @Nonnull final IndataLineType indataLineType)
			throws ConversionException
	{
		super.populate(externalCompanyAddress, indataLineType);
		ZoneAddressType shipFromAddress = new ZoneAddressType();
		addressToZoneAddressTypePopulator.populate(externalCompanyAddress, shipFromAddress);
		indataLineType.setORDERACCEPTANCE(shipFromAddress);
		indataLineType.setORDERORIGIN(shipFromAddress);
		indataLineType.setSHIPFROM(shipFromAddress);
		indataLineType.setSELLERPRIMARY(shipFromAddress);
	}
}
