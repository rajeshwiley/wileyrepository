package com.wiley.integrations.ediintegration.endpoints.transformers.converters;

import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.paypal.hybris.model.PaypalPaymentInfoModel;

import static com.wiley.integrations.constants.WileyintegrationsConstants.CC_PO_NUMBER_FORMAT;
import static com.wiley.integrations.constants.WileyintegrationsConstants.PP_PO_NUMBER_FORMAT;



public interface ConversionUtils
{
	Logger LOG = Logger.getLogger(ConversionUtils.class);

	static String getStateProvinceFromAddress(final AddressModel addressModel) {
		final Optional<RegionModel> regionModel = Optional.ofNullable(addressModel.getRegion());

		if (!regionModel.isPresent()) {
			LOG.warn("Address model doesn't have region, probably not US and not CA");
			return StringUtils.EMPTY;
		}
		return regionModel.get().getIsocodeShort();
	}

	static String buildPONumberFormat(final OrderModel orderModel)
	{
		final boolean isPPOrder = orderModel.getPaymentInfo() instanceof PaypalPaymentInfoModel;
		final String poNumberFormat = isPPOrder ? PP_PO_NUMBER_FORMAT : CC_PO_NUMBER_FORMAT;
		return String.format(poNumberFormat, orderModel.getCode());
	}

}
