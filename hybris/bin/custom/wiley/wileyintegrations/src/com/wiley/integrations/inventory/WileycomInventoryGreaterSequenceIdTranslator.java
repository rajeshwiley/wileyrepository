package com.wiley.integrations.inventory;

import de.hybris.platform.acceleratorservices.dataimport.batch.converter.GreaterSequenceIdTranslator;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloInvalidParameterException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 * Translator that will throw exception in case if new sequenceId is less than current
 * to prevent saving line in db.
 * This is a temporary solution throwing WileycomInventoryBadSequenceException if line should not be processed
 */
public class WileycomInventoryGreaterSequenceIdTranslator extends GreaterSequenceIdTranslator
{
	private static final Logger LOG = LoggerFactory.getLogger(WileycomInventoryGreaterSequenceIdTranslator.class);

	@Override
	public Object importValue(final String valueExpr, final Item toItem) throws JaloInvalidParameterException
	{
		Object result = super.importValue(valueExpr, toItem);
		if (this.wasUnresolved())
		{
			throw new WileycomInventoryBadSequenceException("Sequence id [" + valueExpr + "] is less than current");
		}
		return result;
	}
}
