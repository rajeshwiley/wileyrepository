/**
 *
 */
package com.wiley.integrations.cdm.dto;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;



@JsonInclude(JsonInclude.Include.NON_NULL)
public class CDMCustomerEBMDTO {

  @JsonProperty(value = "CustomerBaseProfile")
  private CustomerBaseProfileDTO customerBaseProfile;
  @JsonProperty(value = "Individual")
  private CDMIndividualDTO individual;
  @JsonProperty(value = "Organization")
  private CDMOrganizationDTO organization;
  @JsonProperty(value = "Address")
  private ArrayList<CDMAddressDTO> address;
  @JsonProperty(value = "Email")
  private CDMEmailDTO email;

  private CDMPhoneDTO phone;

  /**
   * @return the customerBaseProfile
   */
  public CustomerBaseProfileDTO getCustomerBaseProfile() {
    return customerBaseProfile;
  }

  /**
   * @param customerBaseProfile
   *          the customerBaseProfile to set
   */
  public void setCustomerBaseProfile(final CustomerBaseProfileDTO customerBaseProfile) {
    this.customerBaseProfile = customerBaseProfile;
  }

  /**
   * @return the individual
   */
  public CDMIndividualDTO getIndividual() {
    return individual;
  }

  /**
   * @param individual
   *          the individual to set
   */
  public void setIndividual(final CDMIndividualDTO individual) {
    this.individual = individual;
  }

  /**
   * @return the organization
   */
  public CDMOrganizationDTO getOrganization() {
    return organization;
  }

  /**
   * @param organization
   *          the organization to set
   */
  public void setOrganization(final CDMOrganizationDTO organization) {
    this.organization = organization;
  }

  /**
   * @return the address
   */
  public ArrayList<CDMAddressDTO> getAddress() {
    return address;
  }

  /**
   * @param address
   *          the address to set
   */
  public void setAddress(final ArrayList<CDMAddressDTO> address) {
    this.address = address;
  }

  /**
   * @return the phone
   */
  public CDMPhoneDTO getPhone() {
    return phone;
  }

  /**
   * @param phone
   *          the phone to set
   */
  public void setPhone(final CDMPhoneDTO phone) {
    this.phone = phone;
  }

  /**
   * @return the email
   */
  public CDMEmailDTO getEmail() { return email; }

  /**
   * @param email
   *          the email to set
   */
  public void setEmail(final CDMEmailDTO email) { this.email = email; }

  @Override
  public String toString() {
    return "CustomerEBM [customerBaseProfile=" + customerBaseProfile + ", individual=" + individual
        + ", organization=" + organization + ", address=" + address + ", phone=" + phone + "]";
  }

}
