package com.wiley.integrations.address.dto;

import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonInclude(JsonInclude.Include.NON_NULL)
public class AddressValidationResultDto
{


	/**
	 * Identifier of the address. Used in the batch mode.
	 */
	@JsonProperty("ID")
	private String id;

	/**
	 * Suggested building name.
	 */
	@JsonProperty("BUILDING_NAME")
	private String buildingName;

	/**
	 * Suggested building number.
	 */
	@JsonProperty("BUILDING_NUMBER")
	private String buildingNumber;

	/**
	 * Suggested sub-building number.
	 */
	@JsonProperty("SUB_BUILDING_NUMBER")
	private String subBuildingNumber;

	/**
	 * Suggested street number.
	 */
	@JsonProperty("STREET_NUMBER")
	private String streetNumber;

	/**
	 * First line of the address.
	 */
	@JsonProperty("ADDRESS_LINE_1")
	private String addressLine1;

	/**
	 * Second line of the address.
	 */
	@JsonProperty("ADDRESS_LINE_2")
	private String addressLine2;

	/**
	 * Third line of the address.
	 */
	@JsonProperty("ADDRESS_LINE_3")
	private String addressLine3;

	/**
	 * City/town.
	 */
	@JsonProperty("CITY")
	private String city;

	/**
	 * Suggested name of the region.
	 */
	@JsonProperty("REGION_NM")
	private String regionName;

	/**
	 * The ISO 3166-2 code that specifies a state or province.
	 * Can consist of letters or numbers (for example, it is two-letter code for US).
	 * For US the ISO 3166-2 codes were extended with non-standard Armed Forces regions values, which were provided by Wiley.
	 * See https://jira.wiley.ru/browse/ECSC-5892 for details.
	 */
	@JsonProperty("REGION_CD")
	@Pattern(regexp = "^[0-9A-Z]{1,3}$")
	private String region;

	/**
	 * Suggested post office box.
	 */
	@JsonProperty("PO_BOX")
	private String poBox;

	/**
	 * Suggested zip/postal code.
	 */
	@JsonProperty("POSTAL_CODE")
	private String postalCode;

	/**
	 * Suggested name of the country.
	 */
	@JsonProperty("COUNTRY_NAME")
	private String countryName;

	/**
	 * Two-letter ISO 3166-1 alpha-2 code that specifies a country.
	 * See https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2 for details.
	 */
	@JsonProperty("COUNTRY_ISO2")
	@Pattern(regexp = "^[A-Z]{2}$")
	private String country;

	/**
	 * Three-letter ISO 3166-1 alpha-3 code that specifies a country.
	 * See https://en.wikipedia.org/wiki/ISO_3166-1_alpha-3 for details.
	 */
	@JsonProperty("COUNTRY_ISO3")
	@Pattern(regexp = "^[A-Z]{2}$")
	private String countryIso3;

	/**
	 * A number from 0 to 5 that defines if delivery is possible to the suggested address.
	 * Used for Address Doctor internal purposes.
	 */
	@JsonProperty("AD_MOBILITY_SCORE")
	private int mobilityScore;

	/**
	 * An index defines the relevance of the suggested address.
	 */
	@JsonProperty("AD_RESULT_PERCENTAGE")
	private double resultPercentage;

	/**
	 * The code defines if the address is valid and if it was possible to provide any suggestions.
	 */
	@JsonProperty("AD_MATCH_CODE")
	@Pattern(regexp = "^[0-9A-Z]{2}$")
	private String matchCode;

	/**
	 * The code of the address type.
	 */
	@JsonProperty("AD_ADDRESS_TYPE")
	@Pattern(regexp = "^[A-Z]{1}$")
	private String addressType;

	/**
	 * The flag defines if the suggested address was successfully validated by Address Doctor.
	 */
	@JsonProperty("USER_OVERRIDE_FLAG")
	@Pattern(regexp = "^[A-Z]{1}$")
	private String userOverrideFlag;

	public String getId()
	{
		return id;
	}

	public void setId(final String id)
	{
		this.id = id;
	}

	public String getBuildingName()
	{
		return buildingName;
	}

	public void setBuildingName(final String buildingName)
	{
		this.buildingName = buildingName;
	}

	public String getBuildingNumber()
	{
		return buildingNumber;
	}

	public void setBuildingNumber(final String buildingNumber)
	{
		this.buildingNumber = buildingNumber;
	}

	public String getSubBuildingNumber()
	{
		return subBuildingNumber;
	}

	public void setSubBuildingNumber(final String subBuildingNumber)
	{
		this.subBuildingNumber = subBuildingNumber;
	}

	public String getStreetNumber()
	{
		return streetNumber;
	}

	public void setStreetNumber(final String streetNumber)
	{
		this.streetNumber = streetNumber;
	}

	public String getAddressLine1()
	{
		return addressLine1;
	}

	public void setAddressLine1(final String addressLine1)
	{
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2()
	{
		return addressLine2;
	}

	public void setAddressLine2(final String addressLine2)
	{
		this.addressLine2 = addressLine2;
	}

	public String getAddressLine3()
	{
		return addressLine3;
	}

	public void setAddressLine3(final String addressLine3)
	{
		this.addressLine3 = addressLine3;
	}

	public String getCity()
	{
		return city;
	}

	public void setCity(final String city)
	{
		this.city = city;
	}

	public String getRegionName()
	{
		return regionName;
	}

	public void setRegionName(final String regionName)
	{
		this.regionName = regionName;
	}

	public String getRegion()
	{
		return region;
	}

	public void setRegion(final String region)
	{
		this.region = region;
	}

	public String getPoBox()
	{
		return poBox;
	}

	public void setPoBox(final String poBox)
	{
		this.poBox = poBox;
	}

	public String getPostalCode()
	{
		return postalCode;
	}

	public void setPostalCode(final String postalCode)
	{
		this.postalCode = postalCode;
	}

	public String getCountryName()
	{
		return countryName;
	}

	public void setCountryName(final String countryName)
	{
		this.countryName = countryName;
	}

	public String getCountry()
	{
		return country;
	}

	public void setCountry(final String country)
	{
		this.country = country;
	}

	public String getCountryIso3()
	{
		return countryIso3;
	}

	public void setCountryIso3(final String countryIso3)
	{
		this.countryIso3 = countryIso3;
	}

	public int getMobilityScore()
	{
		return mobilityScore;
	}

	public void setMobilityScore(final int mobilityScore)
	{
		this.mobilityScore = mobilityScore;
	}

	public double getResultPercentage()
	{
		return resultPercentage;
	}

	public void setResultPercentage(final double resultPercentage)
	{
		this.resultPercentage = resultPercentage;
	}

	public String getMatchCode()
	{
		return matchCode;
	}

	public void setMatchCode(final String matchCode)
	{
		this.matchCode = matchCode;
	}

	public String getAddressType()
	{
		return addressType;
	}

	public void setAddressType(final String addressType)
	{
		this.addressType = addressType;
	}

	public String getUserOverrideFlag()
	{
		return userOverrideFlag;
	}

	public void setUserOverrideFlag(final String userOverrideFlag)
	{
		this.userOverrideFlag = userOverrideFlag;
	}
}
