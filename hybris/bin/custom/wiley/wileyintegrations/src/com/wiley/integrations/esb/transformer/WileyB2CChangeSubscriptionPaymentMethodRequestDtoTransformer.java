package com.wiley.integrations.esb.transformer;

import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;

import java.util.Map;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.springframework.messaging.Message;
import org.springframework.util.Assert;

import com.paypal.hybris.model.PaypalPaymentInfoModel;
import com.wiley.core.enums.PaymentModeEnum;
import com.wiley.integrations.esb.converters.populator.address.AddressToAddressCreateRequestDtoPopulator;
import com.wiley.integrations.esb.dto.EsbAddressCreateRequestDto;
import com.wiley.integrations.esb.dto.EsbB2CChangeSubscriptionMethodRequestDto;


public class WileyB2CChangeSubscriptionPaymentMethodRequestDtoTransformer
{

	private AddressToAddressCreateRequestDtoPopulator addressPopulator;

	@Resource
	private Map<String, PaymentModeEnum> paymentModeMap;

	@Nonnull
	public EsbB2CChangeSubscriptionMethodRequestDto transform(@Nonnull final Message<PaymentInfoModel> message)
	{
		Assert.notNull(message);
		final PaymentInfoModel paymentInfoModel = message.getPayload();
		final String paymentTypeCode = paymentModeMap.get(paymentInfoModel.getItemtype()).getCode();

		EsbB2CChangeSubscriptionMethodRequestDto esbAddressCreateRequestDto = new EsbB2CChangeSubscriptionMethodRequestDto();

		EsbAddressCreateRequestDto addressCreateRequestDto = new EsbAddressCreateRequestDto();
		addressPopulator.populate(paymentInfoModel.getBillingAddress(), addressCreateRequestDto);

		String paymentToken = extractPaymentToken(paymentInfoModel);


		esbAddressCreateRequestDto.setPaymentToken(paymentToken);
		esbAddressCreateRequestDto.setPaymentType(paymentTypeCode);
		esbAddressCreateRequestDto.setPaymentAddress(addressCreateRequestDto);

		return esbAddressCreateRequestDto;
	}

	private String extractPaymentToken(final PaymentInfoModel paymentInfoModel)
	{
		if (paymentInfoModel instanceof CreditCardPaymentInfoModel)
		{
			return ((CreditCardPaymentInfoModel) paymentInfoModel).getSubscriptionId();
		}
		else if (paymentInfoModel instanceof PaypalPaymentInfoModel)
		{
			return ((PaypalPaymentInfoModel) paymentInfoModel).getToken();
		}
		return null;
	}

	public AddressToAddressCreateRequestDtoPopulator getAddressPopulator()
	{
		return addressPopulator;
	}

	public void setAddressPopulator(
			final AddressToAddressCreateRequestDtoPopulator addressPopulator)
	{
		this.addressPopulator = addressPopulator;
	}
}
