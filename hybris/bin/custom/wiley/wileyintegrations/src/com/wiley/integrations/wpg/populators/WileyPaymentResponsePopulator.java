package com.wiley.integrations.wpg.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;


public class WileyPaymentResponsePopulator implements Populator<String, Map<String, String>>
{

	private static final int PARAM_NAME_GROUP = 1;
	private static final int PARAM_VALUE_GROUP = 2;

	@Value("${payment.wpg.response.valuesRegexp}")
	private String responseRegexp;

	private Pattern valuesPattern;

	@PostConstruct
	public void init()
	{
		valuesPattern = Pattern.compile(responseRegexp);
	}

	@Override
	public void populate(final String paymentResponseBody, final Map<String, String> responseMap)
			throws ConversionException
	{
		Matcher matcher = valuesPattern.matcher(paymentResponseBody);

		while (matcher.find())
		{
			try
			{
				responseMap.put(matcher.group(PARAM_NAME_GROUP), matcher.group(PARAM_VALUE_GROUP));
			}
			catch (IndexOutOfBoundsException | IllegalStateException e)
			{
				throw new ConversionException("Unexpected response format. Cannot match expected pattern", e);
			}
		}
		if (responseMap.size() == 0)
		{
			throw new ConversionException("Unexpected response format. Nothing can be parsed");
		}
	}

	void setResponseRegexp(final String responseRegexp)
	{
		this.responseRegexp = responseRegexp;
	}
}
