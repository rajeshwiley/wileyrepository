package com.wiley.integrations.tax.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;

import com.wiley.core.externaltax.dto.TaxCalculationResponseDto;
import com.wiley.core.externaltax.dto.TaxCalculationResponseEntryDto;
import com.wiley.core.externaltax.dto.TaxValueDto;
import com.wiley.integrations.tax.dto.TaxCalculationResponse;
import com.wiley.integrations.tax.dto.TaxCalculationResponseEntry;
import com.wiley.integrations.tax.dto.TaxValue;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


public class TaxCalculationResponseDtoPopulator implements Populator<TaxCalculationResponse, TaxCalculationResponseDto>
{

	@Resource
	private Converter<TaxValue, TaxValueDto> taxValueDtoConverter;
	@Resource
	private Converter<TaxCalculationResponseEntry, TaxCalculationResponseEntryDto> taxCalculationResponseEntryDtoConverter;

	@Override
	public void populate(final TaxCalculationResponse source,
			final TaxCalculationResponseDto target)
			throws ConversionException
	{
		validateParameterNotNull(source, "source mustn't be null");
		validateParameterNotNull(target, "target mustn't be null");

		target.setHandlingTaxes(taxValueDtoConverter.convertAll(source.getHandlingTaxes()));
		target.setShippingTaxes(taxValueDtoConverter.convertAll(source.getShippingTaxes()));
		target.setEntries(taxCalculationResponseEntryDtoConverter.convertAll(source.getEntries()));
	}
}
