/**
 *
 */
package com.wiley.integrations.cdm.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;



@JsonInclude(JsonInclude.Include.NON_NULL)
public class CDMIndividualDTO {

  @JsonProperty(value = "FullName")
  private String fullName;

  @JsonProperty(value = "FirstName")
  private String firstName;

  @JsonProperty(value = "LastName")
  private String lastName;

  /**
   * @return the fullName
   */
  public String getFullName() {
    return fullName;
  }

  /**
   * @param fullName
   *          the fullName to set
   */
  public void setFullName(final String fullName) {
    this.fullName = fullName;
  }

  /**
   * @return the firstName
   */
  public String getFirstName()
  {
    return firstName;
  }

  /**
   * @param firstName
   *          the fullName to set
   */
  public void setFirstName(final String firstName)
  {
    this.firstName = firstName;
  }

  /**
   * @return the lastName
   */
  public String getLastName()
  {
    return lastName;
  }

  /**
   * @param lastName
   *          the fullName to set
   */
  public void setLastName(final String lastName)
  {
    this.lastName = lastName;
  }

  @Override
  public String toString() {
    return "Individual [fullName=" + fullName + "]";
  }

}
