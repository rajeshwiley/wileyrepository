package com.wiley.integrations.esb.strategies;

import de.hybris.platform.basecommerce.enums.StockLevelStatus;

import com.wiley.integrations.esb.dto.verifyandcalculatecart.InventoryStatus;


/**
 * Created by Mikhail_Asadchy on 7/29/2016.
 */
public interface InventoryStatusToStockLevelStatusStrategy
{

	StockLevelStatus getExternalInventoryStatus(InventoryStatus.StatusEnum status);

}
