package com.wiley.integrations.sabrix.transformer.populators.line;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.wiley.integrations.sabrix.dto.IndataLineType;
import com.wiley.integrations.sabrix.dto.ZoneAddressType;
import com.wiley.integrations.sabrix.transformer.populators.AddressToZoneAddressTypePopulator;


/**
 * @author Dzmitryi_Halahayeu
 */
@Component
public class OrderToDeliveryCostLinePopulator implements Populator<AbstractOrderModel, IndataLineType>
{
	private static final String DELIVERY_COST_TRANSACTION_TYPE = "TS";
	private static final String DELIVERY_COST_INTERNATIONAL_NON_PRODUCT_CODE = "FreightIntl";
	private static final String DELIVERY_COST_LOCAL_NON_PRODUCT_CODE = "FreightDom";
	@Resource
	private AddressToZoneAddressTypePopulator addressToZoneAddressTypePopulator;

	@Override
	public void populate(final AbstractOrderModel order, final IndataLineType indataLineType)
			throws ConversionException
	{
		ZoneAddressType buyerPrimary = new ZoneAddressType();
		addressToZoneAddressTypePopulator.populate(order.getPaymentAddress(), buyerPrimary);
		indataLineType.setBUYERPRIMARY(buyerPrimary);
		ZoneAddressType shipTo = new ZoneAddressType();
		addressToZoneAddressTypePopulator.populate(order.getDeliveryAddress(), shipTo);
		indataLineType.setSHIPTO(shipTo);
		indataLineType.setNONPRODUCTTRANSACTION(
				indataLineType.getSHIPFROM().getCOUNTRY().equals(indataLineType.getSHIPTO().getCOUNTRY()) ?
						DELIVERY_COST_LOCAL_NON_PRODUCT_CODE :
						DELIVERY_COST_INTERNATIONAL_NON_PRODUCT_CODE);
		indataLineType.setTRANSACTIONTYPE(DELIVERY_COST_TRANSACTION_TYPE);
	}
}
