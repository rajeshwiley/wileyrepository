package com.wiley.integrations.esb.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.common.base.MoreObjects;


/**
 * DTO contains different kinds of cart modifications.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderModificationsResponseDto
{

	private List<AddOrderEntryModificationResponseDto> entriesToAdd;

	private List<UpdateOrderEntryModificationResponseDto> entriesToUpdate;

	public List<AddOrderEntryModificationResponseDto> getEntriesToAdd()
	{
		return entriesToAdd;
	}

	public void setEntriesToAdd(final List<AddOrderEntryModificationResponseDto> entriesToAdd)
	{
		this.entriesToAdd = entriesToAdd;
	}

	public List<UpdateOrderEntryModificationResponseDto> getEntriesToUpdate()
	{
		return entriesToUpdate;
	}

	public void setEntriesToUpdate(
			final List<UpdateOrderEntryModificationResponseDto> entriesToUpdate)
	{
		this.entriesToUpdate = entriesToUpdate;
	}

	@Override
	public String toString()
	{
		return MoreObjects.toStringHelper(this)
				.add("entriesToAdd", entriesToAdd)
				.add("entriesToUpdate", entriesToUpdate)
				.toString();
	}
}
