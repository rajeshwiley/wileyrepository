package com.wiley.integrations.mpgs.transformer;

import de.hybris.platform.servicelayer.util.ServicesUtil;

import org.apache.commons.lang.StringUtils;

import com.wiley.core.mpgs.dto.authorization.MPGSAuthorizationRequestDTO;
import com.wiley.core.mpgs.dto.json.Address;
import com.wiley.core.mpgs.dto.json.Billing;
import com.wiley.core.mpgs.dto.json.Order;
import com.wiley.core.mpgs.dto.json.Transaction;
import com.wiley.core.mpgs.dto.json.authorization.SourceOfFunds;
import com.wiley.core.mpgs.request.WileyAuthorizationRequest;


public class WileyMPGSAuthorizationRequestTransformer
{

	public MPGSAuthorizationRequestDTO transform(final WileyAuthorizationRequest request)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("request", request);
		final Order order = createOrderFromRequest(request);
		final SourceOfFunds sourceOfFunds = createSourceOfFundsFromRequest(request);
		final Billing billing = createBillingFromRequest(request);
		final Transaction transaction = createTransactionFromRequest(request);

		final MPGSAuthorizationRequestDTO authorizationRequest = new MPGSAuthorizationRequestDTO();
		authorizationRequest.setApiOperation(request.getApiOperation());
		authorizationRequest.setOrder(order);
		authorizationRequest.setSourceOfFunds(sourceOfFunds);
		authorizationRequest.setBilling(billing);
		authorizationRequest.setTransaction(transaction);

		return authorizationRequest;
	}

	private Transaction createTransactionFromRequest(final WileyAuthorizationRequest request)
	{
		final Transaction transaction = new Transaction();
		transaction.setReference(request.getTransactionReference());
		return transaction;
	}

	private Order createOrderFromRequest(final WileyAuthorizationRequest request)
	{
		final Order order = new Order();
		order.setAmount(request.getTotalAmount().toString());
		order.setCurrency(request.getCurrency());
		return order;
	}

	private SourceOfFunds createSourceOfFundsFromRequest(final WileyAuthorizationRequest request)
	{
		final SourceOfFunds sourceOfFunds = new SourceOfFunds();
		sourceOfFunds.setToken(request.getToken());
		return sourceOfFunds;
	}

	private Billing createBillingFromRequest(final WileyAuthorizationRequest request)
	{
		final Billing billing = new Billing();
		final Address billingAddress = new Address();
		billing.setAddress(billingAddress);
		billingAddress.setCity(request.getBillingCity());
		billingAddress.setCountry(request.getBillingCountry());
		billingAddress.setPostcodeZip(request.getBillingPostalCode());
		billingAddress.setStateProvince(request.getStateProvince());
		billingAddress.setStreet(request.getBillingStreet1());
		final String street2 = request.getBillingStreet2();
		billingAddress.setStreet2(StringUtils.isEmpty(street2) ? null : street2);
		return billing;
	}

}
