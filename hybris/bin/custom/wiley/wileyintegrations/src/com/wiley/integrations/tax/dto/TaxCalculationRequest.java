package com.wiley.integrations.tax.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;


public class TaxCalculationRequest
{
	@JsonProperty("siteId")
	@NotNull
	@Size(max = 255)
	private String siteId;

	@JsonProperty("date")
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@NotNull
	@Valid
	private Date date;

	@JsonProperty("currency")
	@NotNull
	@Pattern(regexp = "^[A-Z]{3}$")
	private String currency;

	@JsonProperty("paymentAddress")
	@NotNull
	@Valid
	private TaxAddress paymentAddress;

	@JsonProperty("deliveryAddress")
	@Valid
	private TaxAddress deliveryAddress;

	@JsonProperty("entries")
	@NotNull
	@Valid
	private List<TaxCalculationRequestEntry> entries;

	@JsonProperty("handlingCost")
	@NotNull
	@DecimalMin("0.0")
	private BigDecimal handlingCost;

	@JsonProperty("shippingCost")
	@NotNull
	@DecimalMin("0.0")
	private BigDecimal shippingCost;

	/**
	 * Hybris unique identifier of the site where the order is processed and taxes should be calculated.
	 *
	 * @return siteId
	 **/
	public String getSiteId()
	{
		return siteId;
	}

	public void setSiteId(final String siteId)
	{
		this.siteId = siteId;
	}

	/**
	 * The date that should be used to calculate taxes. The format is defined as per 'date-time' in RFC3339
	 * (http://xml2rfc.ietf.org/public/rfc/html/rfc3339.html#anchor14).
	 *
	 * @return date
	 **/
	public Date getDate()
	{
		return date;
	}

	public void setDate(final Date date)
	{
		this.date = date;
	}

	public TaxCalculationRequest currency(final String currency)
	{
		this.currency = currency;
		return this;
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		final TaxCalculationRequest that = (TaxCalculationRequest) o;
		return Objects.equals(siteId, that.siteId)
				&& Objects.equals(date, that.date)
				&& Objects.equals(currency, that.currency)
				&& Objects.equals(paymentAddress, that.paymentAddress)
				&& Objects.equals(deliveryAddress, that.deliveryAddress)
				&& Objects.equals(entries, that.entries)
				&& Objects.equals(handlingCost, that.handlingCost)
				&& Objects.equals(shippingCost, that.shippingCost);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(siteId, date, currency, paymentAddress, deliveryAddress, entries, handlingCost, shippingCost);
	}

	/**
	 * Get currency
	 *
	 * @return currency
	 **/
	public String getCurrency()
	{
		return currency;
	}

	public void setCurrency(final String currency)
	{
		this.currency = currency;
	}

	/**
	 * Get paymentAddress
	 *
	 * @return paymentAddress
	 **/
	public TaxAddress getPaymentAddress()
	{
		return paymentAddress;
	}

	public void setPaymentAddress(final TaxAddress paymentAddress)
	{
		this.paymentAddress = paymentAddress;
	}

	/**
	 * Get deliveryAddress
	 *
	 * @return deliveryAddress
	 **/
	public TaxAddress getDeliveryAddress()
	{
		return deliveryAddress;
	}

	public void setDeliveryAddress(final TaxAddress deliveryAddress)
	{
		this.deliveryAddress = deliveryAddress;
	}

	/**
	 * List of he taxable cart/order entries.
	 *
	 * @return entries
	 **/
	public List<TaxCalculationRequestEntry> getEntries()
	{
		return entries;
	}

	public void setEntries(final List<TaxCalculationRequestEntry> entries)
	{
		this.entries = entries;
	}

	/**
	 * The value of the handling fee.
	 * minimum: 0
	 *
	 * @return handlingCost
	 **/
	public BigDecimal getHandlingCost()
	{
		return handlingCost;
	}

	public void setHandlingCost(final BigDecimal handlingCost)
	{
		this.handlingCost = handlingCost;
	}

	/**
	 * The value of the shipping fee.
	 * minimum: 0
	 *
	 * @return shippingCost
	 **/
	public BigDecimal getShippingCost()
	{
		return shippingCost;
	}

	public void setShippingCost(final BigDecimal shippingCost)
	{
		this.shippingCost = shippingCost;
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("class TaxCalculationRequest {\n");

		sb.append("    siteId: ").append(toIndentedString(siteId)).append("\n");
		sb.append("    date: ").append(toIndentedString(date)).append("\n");
		sb.append("    currency: ").append(toIndentedString(currency)).append("\n");
		sb.append("    paymentAddress: ").append(toIndentedString(paymentAddress)).append("\n");
		sb.append("    deliveryAddress: ").append(toIndentedString(deliveryAddress)).append("\n");
		sb.append("    entries: ").append(toIndentedString(entries)).append("\n");
		sb.append("    handlingCost: ").append(toIndentedString(handlingCost)).append("\n");
		sb.append("    shippingCost: ").append(toIndentedString(shippingCost)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(final Object o)
	{
		if (o == null)
		{
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}

