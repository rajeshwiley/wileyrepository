package com.wiley.integrations.sabrix.dto;

import java.util.Objects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * App specific fields that are not sent to ONESOURCE
 *
 * <p>Java class for UserDataType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="UserDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SourceSystemAppID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="SourceSystemAppName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UniqueRequestIdentifier" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ContinueOnError" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserDataType", namespace = "http://xmlns.wiley.com/EnterpriseBusinessObjects/TaxEBO/V1", propOrder = {
		"sourceSystemAppID",
		"sourceSystemAppName",
		"uniqueRequestIdentifier",
		"continueOnError"
})
public class UserDataType
{

	@XmlElement(name = "SourceSystemAppID", required = true)
	protected String sourceSystemAppID;
	@XmlElement(name = "SourceSystemAppName")
	protected String sourceSystemAppName;
	@XmlElement(name = "UniqueRequestIdentifier", required = true)
	protected String uniqueRequestIdentifier;
	@XmlElement(name = "ContinueOnError")
	protected String continueOnError;

	/**
	 * Gets the value of the sourceSystemAppID property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getSourceSystemAppID()
	{
		return sourceSystemAppID;
	}

	/**
	 * Sets the value of the sourceSystemAppID property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setSourceSystemAppID(String value)
	{
		this.sourceSystemAppID = value;
	}

	/**
	 * Gets the value of the sourceSystemAppName property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getSourceSystemAppName()
	{
		return sourceSystemAppName;
	}

	/**
	 * Sets the value of the sourceSystemAppName property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setSourceSystemAppName(String value)
	{
		this.sourceSystemAppName = value;
	}

	/**
	 * Gets the value of the uniqueRequestIdentifier property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getUniqueRequestIdentifier()
	{
		return uniqueRequestIdentifier;
	}

	/**
	 * Sets the value of the uniqueRequestIdentifier property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setUniqueRequestIdentifier(String value)
	{
		this.uniqueRequestIdentifier = value;
	}

	/**
	 * Gets the value of the continueOnError property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getContinueOnError()
	{
		return continueOnError;
	}

	/**
	 * Sets the value of the continueOnError property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setContinueOnError(String value)
	{
		this.continueOnError = value;
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		final UserDataType that = (UserDataType) o;
		return Objects.equals(sourceSystemAppID, that.sourceSystemAppID) &&
				Objects.equals(sourceSystemAppName, that.sourceSystemAppName) &&
				Objects.equals(continueOnError, that.continueOnError);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(sourceSystemAppID, sourceSystemAppName, continueOnError);
	}
}
