package com.wiley.integrations.savedcart.transformer;


import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;

import com.wiley.core.wileyb2c.savedcart.exception.SubscriptionAlreadyRenewedException;
import com.wiley.integrations.savedcart.dto.SavedCartResponseSwgDTO;


public class SavedCartResponseSwgDTOTransformer
{
	@Resource
	private ModelService modelService;

	@Resource
	private CommonI18NService commonI18NService;

	public CartModel transform(final SavedCartResponseSwgDTO cartDTO)
	{
		if (Boolean.TRUE.equals(cartDTO.getSubscriptionAlreadyRenewed()))
		{
			throw new SubscriptionAlreadyRenewedException();
		}

		final CartModel cartModel = modelService.create(CartModel.class);
		cartModel.setCode(cartDTO.getCode());

		if (StringUtils.isNotBlank(cartDTO.getCountry()))
		{
			final CountryModel country = commonI18NService.getCountry(cartDTO.getCountry());
			cartModel.setCountry(country);
		}
		return cartModel;
	}
}
