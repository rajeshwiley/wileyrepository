package com.wiley.integrations.users.transformer;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.model.ModelService;

import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.integrations.users.data.WileyUserSearchResponse;


public class WileyUserSearchResponseTransformer
{
	@Autowired
	ModelService modelService;
	
	public CustomerModel transform(final WileyUserSearchResponse customerSearchResponse)
	{
		CustomerModel customer = modelService.create(CustomerModel.class);
		customer.setFirstName(customerSearchResponse.getFirstName());
		customer.setLastName(customerSearchResponse.getLastName());
		return customer;
	}
}
