package com.wiley.integrations.selector.strategies.impl;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import org.springframework.messaging.Message;


/**
 * Created by Uladzimir_Barouski on 12/7/2016.
 */
public class WileyStoreFromHeaderStrategyImpl extends WileyAbstractStoreSelectorStrategy
{

	public static final String HEADER_SITE_KEY = "site";

	@Override
	public Optional<BaseStoreModel> getCurrentBaseStore(final Message<?> message)
	{
		BaseSiteModel currentBaseSite = (BaseSiteModel) message.getHeaders().get(HEADER_SITE_KEY);
		return findBaseStoreForSite(currentBaseSite);
	}
}
