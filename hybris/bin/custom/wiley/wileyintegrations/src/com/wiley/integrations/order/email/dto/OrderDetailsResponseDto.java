package com.wiley.integrations.order.email.dto;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Generated with Swagger Codegen CLI tool.
 */
public class OrderDetailsResponseDto
{
	private Boolean requestProcessed = null;

	/**
	 * True if the request for the order details was successfully processed, false otherwise.
	 **/
	public OrderDetailsResponseDto requestProcessed(final Boolean requestProcessed) {
		this.requestProcessed = requestProcessed;
		return this;
	}


	@JsonProperty("requestProcessed")
	public Boolean getRequestProcessed() {
		return requestProcessed;
	}
	public void setRequestProcessed(final Boolean requestProcessed) {
		this.requestProcessed = requestProcessed;
	}


	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		OrderDetailsResponseDto orderDetailsResponseDto = (OrderDetailsResponseDto) o;
		return Objects.equals(requestProcessed, orderDetailsResponseDto.requestProcessed);
	}

	@Override
	public int hashCode() {
		return Objects.hash(requestProcessed);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class OrderDetailsResponseDto {\n");

		sb.append("    requestProcessed: ").append(toIndentedString(requestProcessed)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(final Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
