package com.wiley.integrations.esb.dto.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.apache.commons.lang3.builder.ToStringBuilder;


/**
 * Applied discount.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Discount
{

	/**
	 * name or code of a provided discount. Name should be in language specified int the request if it is presented.
	 * Otherwise English is a default language in the system.
	 */
	private String name;

	/**
	 * a flag that 'value' field contains an absolute value
	 */
	private Boolean absolute;

	/**
	 * a value of provided discount
	 */
	private Double value;

	@JsonProperty("name")
	public String getName()
	{
		return name;
	}

	public void setName(final String name)
	{
		this.name = name;
	}

	@JsonProperty("absolute")
	public Boolean getAbsolute()
	{
		return absolute;
	}

	public void setAbsolute(final Boolean absolute)
	{
		this.absolute = absolute;
	}

	@JsonProperty("value")
	public Double getValue()
	{
		return value;
	}

	public void setValue(final Double value)
	{
		this.value = value;
	}

	@Override
	public String toString()
	{
		return new ToStringBuilder(this)
				.append("name", name)
				.append("absolute", absolute)
				.append("value", value)
				.toString();
	}
}
