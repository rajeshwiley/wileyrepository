package com.wiley.integrations.inventory;

import de.hybris.platform.acceleratorservices.dataimport.batch.BatchHeader;
import de.hybris.platform.acceleratorservices.dataimport.batch.task.HeaderInitTask;
import de.hybris.platform.acceleratorservices.util.RegexParser;
import de.hybris.platform.ordersplitting.WarehouseService;
import de.hybris.platform.ordersplitting.model.WarehouseModel;

import javax.annotation.Resource;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Required;


/**
 * Extend Hybris OOTB HeaderInitTask by warehouseParser to add warehouse to batchHeader
 */
public class WileycomInventoryHeaderInitTask extends HeaderInitTask
{
	private RegexParser warehouseParser;

	@Resource
	private WarehouseService warehouseService;

	@Override
	public BatchHeader execute(final BatchHeader header)
	{
		InventoryBatchHeader inventoryBatchHeader = new InventoryBatchHeader();
		BeanUtils.copyProperties(header, inventoryBatchHeader);
		super.execute(inventoryBatchHeader);
		final String warehouseCode = warehouseParser.parse(inventoryBatchHeader.getFile().getName(), 1);
		//check if warehouse code valid
		final WarehouseModel warehouseModel = warehouseService.getWarehouseForCode(warehouseCode);
		inventoryBatchHeader.setWarehouseCode(warehouseModel.getCode());
		inventoryBatchHeader.setWarehouseSequenceId(warehouseModel.getSequenceId());
		return inventoryBatchHeader;
	}

	@Required
	public void setWarehouseParser(final RegexParser warehouseParser)
	{
		this.warehouseParser = warehouseParser;
	}
}
