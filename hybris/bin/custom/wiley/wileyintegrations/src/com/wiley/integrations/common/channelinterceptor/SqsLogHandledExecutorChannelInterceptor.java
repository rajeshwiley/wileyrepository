package com.wiley.integrations.common.channelinterceptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.support.ExecutorChannelInterceptor;


public class SqsLogHandledExecutorChannelInterceptor implements ExecutorChannelInterceptor
{
	private static final Logger LOG = LoggerFactory.getLogger(SqsLogHandledExecutorChannelInterceptor.class);
	private static final String ID = "id";
	private static final String MESSAGE_GROUP_ID = "message-group-id";

	@Override
	public Message<?> beforeHandle(final Message<?> message, final MessageChannel messageChannel,
			final MessageHandler messageHandler)
	{
		return message;
	}

	@Override
	public void afterMessageHandled(final Message<?> message, final MessageChannel messageChannel,
			final MessageHandler messageHandler, final Exception exception)
	{
		final Object id = message.getHeaders().get(ID);
		final Object messageGroupId = message.getHeaders().get(MESSAGE_GROUP_ID);
		if (exception != null)
		{
			LOG.error("SQS " + messageChannel + " - Failed to send message with channel message id="
							+ id + " and message-group-id=" + messageGroupId, exception);
		}
		else
		{
			LOG.info("SQS {} - Successfully sent message with id={} and message-group-id={}",
					messageChannel, id, messageGroupId);
		}
	}

	@Override
	public Message<?> preSend(final Message<?> message, final MessageChannel messageChannel)
	{
		return message;
	}

	@Override
	public void postSend(final Message<?> message, final MessageChannel messageChannel, final boolean b)
	{
		//ignore
	}

	@Override
	public void afterSendCompletion(final Message<?> message, final MessageChannel messageChannel, final boolean b,
			final Exception e)
	{
		//ignore
	}

	@Override
	public boolean preReceive(final MessageChannel messageChannel)
	{
		return true;
	}

	@Override
	public Message<?> postReceive(final Message<?> message, final MessageChannel messageChannel)
	{
		return message;
	}

	@Override
	public void afterReceiveCompletion(final Message<?> message, final MessageChannel messageChannel, final Exception e)
	{
		//ignore
	}
}
