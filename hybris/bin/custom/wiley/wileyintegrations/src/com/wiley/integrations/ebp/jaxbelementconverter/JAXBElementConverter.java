package com.wiley.integrations.ebp.jaxbelementconverter;

import javax.xml.bind.JAXBElement;

import org.springframework.core.convert.converter.Converter;

public class JAXBElementConverter<TARGET> implements Converter<JAXBElement<TARGET>, TARGET> {

	@Override
	public TARGET convert(final JAXBElement<TARGET> source) {
		return source.getValue();
	}
}
