package com.wiley.integrations.selector.strategies.impl;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.messaging.Message;


/**
 * Created by Uladzimir_Barouski on 12/7/2016.
 */
public class WileyStoreFromSessionStrategyImpl extends WileyAbstractStoreSelectorStrategy
{
	private BaseSiteService siteService;

	@Nonnull
	@Override
	public Optional<BaseStoreModel> getCurrentBaseStore(final Message<?> message)
	{
		BaseSiteModel currentBaseSite = siteService.getCurrentBaseSite();
		return findBaseStoreForSite(currentBaseSite);
	}

	@Required
	public void setSiteService(final BaseSiteService siteService)
	{
		this.siteService = siteService;
	}
}
