package com.wiley.integrations.b2b.unit.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * PoNumberValidationRequestSwgDTO
 */
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-07-12T14:18:15.237Z")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PoNumberValidationRequestSwgDTO
{

	private List<String> poNumbers = new ArrayList<>();


	/**
	 * Array of the purchase order numbers.
	 **/
	public PoNumberValidationRequestSwgDTO poNumbers(final List<String> poNumbers)
	{
		this.poNumbers = poNumbers;
		return this;
	}

	@JsonProperty("poNumbers")
	public List<String> getPoNumbers()
	{
		return poNumbers;
	}

	public void setPoNumbers(final List<String> poNumbers)
	{
		this.poNumbers = poNumbers;
	}


	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		PoNumberValidationRequestSwgDTO poNumberValidationRequestSwgDTO = (PoNumberValidationRequestSwgDTO) o;
		return Objects.equals(this.poNumbers, poNumberValidationRequestSwgDTO.poNumbers);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(poNumbers);
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(final Object o)
	{
		if (o == null)
		{
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}

