package com.wiley.integrations.invoice.transformer;

import javax.annotation.Nonnull;

import org.springframework.messaging.handler.annotation.Header;

import com.wiley.core.integration.invoice.dto.InvoiceDto;

import reactor.util.Assert;


public class InvoiceResponseToDtoTransformer
{
	@Nonnull
	public InvoiceDto transform(@Header("contentType") final String contentType, @Nonnull final byte[] binaryFile)
	{
		Assert.notNull(contentType, "Could not resolve contentType header");
		InvoiceDto invoiceDto = new InvoiceDto();
		invoiceDto.setMimeType(contentType);
		invoiceDto.setBinaryFile(binaryFile);

		return invoiceDto;
	}
}
