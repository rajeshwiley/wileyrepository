package com.wiley.integrations.failedordersexport.providers;

import com.wiley.core.model.WileyOrderProcessModel;
import de.hybris.platform.store.BaseStoreModel;

import java.time.LocalDateTime;
import java.util.Collection;

public interface WileyFailedOrderProcessesProvisionDao {

    Collection<WileyOrderProcessModel> findFailedOrderProcesses(BaseStoreModel store,
                                                                LocalDateTime from, LocalDateTime to);
}
