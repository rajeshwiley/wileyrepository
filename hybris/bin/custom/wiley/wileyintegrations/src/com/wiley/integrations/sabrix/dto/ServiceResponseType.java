package com.wiley.integrations.sabrix.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ServiceResponseType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="ServiceResponseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ServiceResponseStatus" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/CommonInfratypes/Codelist/V1}StatusType" minOccurs="0"/&gt;
 *         &lt;element name="ServiceResponseCode" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="ServiceResponseMesg" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1}StringType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceResponseType", namespace = "http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/Meta/V1", propOrder = {
		"serviceResponseStatus",
		"serviceResponseCode",
		"serviceResponseMesg"
})
public class ServiceResponseType
{

	@XmlElement(name = "ServiceResponseStatus")
	@XmlSchemaType(name = "string")
	protected StatusType serviceResponseStatus;
	@XmlElement(name = "ServiceResponseCode")
	protected String serviceResponseCode;
	@XmlElement(name = "ServiceResponseMesg")
	protected String serviceResponseMesg;

	/**
	 * Gets the value of the serviceResponseStatus property.
	 *
	 * @return possible object is
	 * {@link StatusType }
	 */
	public StatusType getServiceResponseStatus()
	{
		return serviceResponseStatus;
	}

	/**
	 * Sets the value of the serviceResponseStatus property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link StatusType }
	 */
	public void setServiceResponseStatus(StatusType value)
	{
		this.serviceResponseStatus = value;
	}

	/**
	 * Gets the value of the serviceResponseCode property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getServiceResponseCode()
	{
		return serviceResponseCode;
	}

	/**
	 * Sets the value of the serviceResponseCode property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setServiceResponseCode(String value)
	{
		this.serviceResponseCode = value;
	}

	/**
	 * Gets the value of the serviceResponseMesg property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getServiceResponseMesg()
	{
		return serviceResponseMesg;
	}

	/**
	 * Sets the value of the serviceResponseMesg property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setServiceResponseMesg(String value)
	{
		this.serviceResponseMesg = value;
	}

}
