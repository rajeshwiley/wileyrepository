package com.wiley.integrations.sabrix.cacheddto;

import java.util.List;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;


public class EsbTaxRequest
{

	private String externalCompanyId;
	private String currency;
	private EsbAddress billTo;
	private EsbAddress shipTo;
	List<EsbTaxRequestEntry> entries;

	public String getExternalCompanyId()
	{
		return externalCompanyId;
	}

	public void setExternalCompanyId(final String externalCompanyId)
	{
		this.externalCompanyId = externalCompanyId;
	}

	public String getCurrency()
	{
		return currency;
	}

	public void setCurrency(final String currency)
	{
		this.currency = currency;
	}

	public EsbAddress getBillTo()
	{
		return billTo;
	}

	public void setBillTo(final EsbAddress billTo)
	{
		this.billTo = billTo;
	}

	public EsbAddress getShipTo()
	{
		return shipTo;
	}

	public void setShipTo(final EsbAddress shipTo)
	{
		this.shipTo = shipTo;
	}

	public List<EsbTaxRequestEntry> getEntries()
	{
		return entries;
	}

	public void setEntries(final List<EsbTaxRequestEntry> entries)
	{
		this.entries = entries;
	}

	@Override
	public int hashCode()
	{
		return Objects.hashCode(externalCompanyId, currency, billTo, shipTo, entries);
	}

	@Override
	public boolean equals(final Object obj)
	{
		if (this == obj)
		{
			return true;
		}
		if (obj == null || getClass() != obj.getClass())
		{
			return false;
		}
		final EsbTaxRequest other = (EsbTaxRequest) obj;
		return Objects.equal(this.externalCompanyId, other.externalCompanyId)
				&& Objects.equal(this.currency, other.currency)
				&& Objects.equal(this.billTo, other.billTo)
				&& Objects.equal(this.shipTo, other.shipTo)
				&& Objects.equal(this.entries, other.entries);
	}

	@Override
	public String toString()
	{
		return MoreObjects.toStringHelper(this)
				.add("externalCompanyId", externalCompanyId)
				.add("currency", currency)
				.add("billTo", billTo)
				.add("shipTo", shipTo)
				.add("shipTo", shipTo)
				.add("entries", entries)
				.toString();
	}
}



