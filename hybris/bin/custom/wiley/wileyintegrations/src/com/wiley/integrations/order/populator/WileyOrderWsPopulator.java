package com.wiley.integrations.order.populator;

import com.wiley.integrations.order.dto.OrderWsDTO;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import static com.wiley.integrations.utils.PopulateUtils.populateFieldIfNotNull;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


public class WileyOrderWsPopulator implements Populator<OrderModel, OrderWsDTO>
{
    @Override
    public void populate(final OrderModel source, final OrderWsDTO target) throws ConversionException
    {
        validateParameterNotNullStandardMessage("source", source);
        validateParameterNotNullStandardMessage("target", target);

        populateFieldIfNotNull((dest, value) -> dest.setStatus(value.getCode()), target, source.getStatus());
        target.setSourceSystem(source.getSourceSystem());
        populateFieldIfNotNull((dest, value) -> dest.setPlacedByAgent(value.getUid()), target, source.getPlacedBy());
    }
}
