package com.wiley.integrations.order.publish.transformer;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.io.IOException;

import javax.annotation.Resource;

import org.springframework.messaging.Message;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wiley.core.order.service.WileyOrderStateSerializationService;
import com.wiley.integrations.order.dto.OrderWsDTO;
import com.wiley.integrations.order.publish.dto.OrderModificationWsDto;


public class WileyOrderPublishTransformer
{
	@Resource
	private Converter<OrderModel, OrderWsDTO> wileyOrderWsConverter;

	@Resource(name = "jacksonObjectMapper")
	private ObjectMapper objectMapper;

	@Resource
	private WileyOrderStateSerializationService wileyOrderStateSerializationService;

	public OrderModificationWsDto transform(final Message<Object[]> message)
	{

		final OrderModificationWsDto orderModificationWsDto = new OrderModificationWsDto();
		orderModificationWsDto.setCurrentState(wileyOrderWsConverter.convert((OrderModel) message.getPayload()[0]));

		if (message.getPayload()[1] != null)
		{
			String previousOrderState = (String) message.getPayload()[1];
			OrderWsDTO previousOrder = readJsonValue(previousOrderState);
			orderModificationWsDto.setPreviousState(previousOrder);
		}
		return orderModificationWsDto;
	}

	private OrderWsDTO readJsonValue(final String previousOrderState)
	{
		try
		{
			return objectMapper.readValue(previousOrderState, OrderWsDTO.class);
		}
		catch (IOException e)
		{
			throw new IllegalArgumentException("PreviousOrderState parameter in payload has errors during JSON parsing", e);
		}
	}
}
