package com.wiley.integrations.alm.authentication.transformer;

import javax.annotation.Nonnull;

import org.springframework.util.Assert;

import com.wiley.core.integration.alm.authentication.dto.AlmAuthenticationResponseDto;
import com.wiley.integrations.alm.authentication.dto.AlmAuthenticationSessionDto;

import static org.springframework.http.HttpStatus.OK;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public class AlmAuthenticationValidateResponseToDtoTransformer
{
	@Nonnull
	public AlmAuthenticationResponseDto transform(@Nonnull final AlmAuthenticationSessionDto sessionDto)
	{
		Assert.notNull(sessionDto);

		final AlmAuthenticationResponseDto responseDto = new AlmAuthenticationResponseDto();
		responseDto.setUserId(sessionDto.getUser().getUserId());
		if (sessionDto.getImitatee() != null)
		{
			responseDto.setImitateeId(sessionDto.getImitatee().getImitateeId());
		}
		responseDto.setHttpStatus(OK);
		return responseDto;
	}
}
