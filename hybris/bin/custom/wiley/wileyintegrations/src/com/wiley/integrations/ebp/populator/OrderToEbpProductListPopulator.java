package com.wiley.integrations.ebp.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.List;
import java.util.Optional;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import com.wiley.core.integration.ebp.dto.EbpAddProductsPayload;
import com.wiley.core.pin.service.PinService;
import com.wiley.integrations.promotioncode.PromotionCodeProvider;
import com.wiley.integrations.ebp.dto.ProductNode;


/**
 * Created by Uladzimir_Barouski on 2/16/2016.
 */
public class OrderToEbpProductListPopulator implements Populator<EbpAddProductsPayload, List<ProductNode>>
{
	private PromotionCodeProvider promotionCodeProvider;

	@Autowired
	private PinService pinService;

	@Override
	public void populate(@Nonnull final EbpAddProductsPayload payload, @Nonnull final List<ProductNode> productList)
			throws ConversionException
	{
		Assert.notNull(payload);
		final OrderModel order = payload.getOrder();
		Assert.notNull(order);
		Assert.notNull(productList);

		for (AbstractOrderEntryModel orderEntry: order.getEntries())
		{
			ProductNode product = new ProductNode();
			product.setProductName(orderEntry.getProduct().getName());
			//we send ISBN as product SKU according to http://confluence.wiley.ru/display/ECSC/IDD%3A+EBP+integration.+Order+data
			product.setProductSKU(orderEntry.getProduct().getIsbn());
			populateProductWithPromotionCode(product, order);
			populateProductWithPinCode(product, order);
			product.setQuantity(orderEntry.getQuantity());

			productList.add(product);
		}

	}

	private void populateProductWithPromotionCode(final ProductNode product, final OrderModel order)
	{
		final Optional<String> promotionCode = promotionCodeProvider.getOrderPromotionCode(order);
		if (promotionCode.isPresent())
		{
			product.setPromotionCode(promotionCode.get());
		}
	}

	private void populateProductWithPinCode(final ProductNode product, final OrderModel order)
	{
		if (pinService.isPinUsedForOrder(order))
		{
			product.setPinCode(pinService.getPinForOrder(order).getCode());
		}
	}

	public void setPromotionCodeProvider(final PromotionCodeProvider promotionCodeProvider)
	{
		this.promotionCodeProvider = promotionCodeProvider;
	}
}
