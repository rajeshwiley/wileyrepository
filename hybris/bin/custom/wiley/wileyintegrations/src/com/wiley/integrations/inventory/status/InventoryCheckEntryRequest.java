package com.wiley.integrations.inventory.status;

/**
 * Created by Mikhail_Asadchy on 7/27/2016.
 */
public class InventoryCheckEntryRequest
{
	private String sapProductCode;

	private String isbn;

	private Integer quantity;

	public String getSapProductCode()
	{
		return sapProductCode;
	}

	public void setSapProductCode(final String sapProductCode)
	{
		this.sapProductCode = sapProductCode;
	}

	public String getIsbn()
	{
		return isbn;
	}

	public void setIsbn(final String isbn)
	{
		this.isbn = isbn;
	}

	public Integer getQuantity()
	{
		return quantity;
	}

	public void setQuantity(final Integer quantity)
	{
		this.quantity = quantity;
	}
}
