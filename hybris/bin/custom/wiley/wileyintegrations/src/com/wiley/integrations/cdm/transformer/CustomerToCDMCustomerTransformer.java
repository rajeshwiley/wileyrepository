/**
 *
 */
package com.wiley.integrations.cdm.transformer;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.CustomerModel;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.util.Assert;

import com.wiley.integrations.cdm.dto.CDMCustomerDTO;


public class CustomerToCDMCustomerTransformer {
  private static final Logger LOG = LoggerFactory.getLogger(CustomerToCDMCustomerTransformer.class);


  @Resource
  private Populator<CustomerModel, CDMCustomerDTO> customerToCDMCustomerPopulator;

  @Nonnull
  public CDMCustomerDTO transform(@Nonnull final Message<CustomerModel> message) {
    final CustomerModel customer = message.getPayload();

    Assert.notNull(customer);


    LOG.debug("Method params: Customer [{}]", customer.getUid());

    final CDMCustomerDTO cdmCustomer = new CDMCustomerDTO();

    customerToCDMCustomerPopulator.populate(customer, cdmCustomer);

    return cdmCustomer;
  }
}
