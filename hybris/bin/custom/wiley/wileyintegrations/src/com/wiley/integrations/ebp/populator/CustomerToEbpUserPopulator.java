package com.wiley.integrations.ebp.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.Set;

import javax.annotation.Nonnull;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import com.wiley.core.integration.ebp.dto.EbpCreateUserPayload;
import com.wiley.integrations.ebp.dto.AddressNode;
import com.wiley.integrations.ebp.dto.UserNode;
import com.wiley.core.order.WileyCheckoutService;


public class CustomerToEbpUserPopulator implements Populator<EbpCreateUserPayload, UserNode>
{
	private static final Logger LOG = LoggerFactory.getLogger(CustomerToEbpUserPopulator.class);

	private WileyCheckoutService checkoutService;

	private Set<String> countriesOtherStateProvince;

	/**
	 * Defines countries that allowed to be populated into 'state' field
	 */
	private Set<String> countriesStateProvince;


	@Override
	public void populate(@Nonnull final EbpCreateUserPayload userPayload, @Nonnull final UserNode user) throws ConversionException
	{
		Assert.notNull(userPayload);
		Assert.notNull(user);

		final CustomerModel customerModel = userPayload.getCustomer();
		Assert.notNull(customerModel);
		LOG.debug("Method transform customer to EBP User params: Customer [{}]", customerModel.getUid());

		user.setFirstName(customerModel.getFirstName());
		user.setLastName(customerModel.getLastName());
		user.setEmailAddress(customerModel.getContactEmail());
		user.setUserName(customerModel.getUid());

		if (!StringUtils.isBlank(userPayload.getOldEmailAddress()))
		{
			user.setOldEmailAddress(userPayload.getOldEmailAddress());
		}

		if (LOG.isDebugEnabled())
		{
		AddressModel defaultShippingAddress = customerModel.getDefaultShipmentAddress();
		if (defaultShippingAddress != null)
		{
				LOG.debug(String.format(
						"Customer %s Default Delivery/Shipping Address PK: %s, Country: %s, Town: %s, "
								+ "StreetName: %s, Postalcode: %s",
						customerModel.getUid(),
						defaultShippingAddress.getPk().toString(), defaultShippingAddress.getCountry(),
						defaultShippingAddress.getTown(),
						defaultShippingAddress.getStreetname(), defaultShippingAddress.getPostalcode()));
			}
			AddressModel defaultPaymentAddress = customerModel.getDefaultPaymentAddress();
			if (defaultPaymentAddress != null)
			{

				LOG.debug(String.format(
						"Customer %s Default Billing Address PK: %s, Country: %s, Town: %s, StreetName: %s, Postalcode: %s",
						customerModel.getUid(),
						defaultPaymentAddress.getPk().toString(), defaultPaymentAddress.getCountry(),
						defaultPaymentAddress.getTown(),
						defaultPaymentAddress.getStreetname(), defaultPaymentAddress.getPostalcode()));
			}
		}

		final OrderModel order = userPayload.getOrder();
		if (order != null)
		{
			AddressModel orderShippingAddress = order.getDeliveryAddress();

			if (orderShippingAddress != null)
			{
				LOG.debug(String.format(
						"Order %s Delivery/Shipping Address PK: %s, Country: %s, Town: %s, StreetName: %s, Postalcode: %s",
						order.getCode(),
						orderShippingAddress.getPk().toString(), orderShippingAddress.getCountry(),
						orderShippingAddress.getTown(),
						orderShippingAddress.getStreetname(), orderShippingAddress.getPostalcode()));
				user.setShippingAddress(convertToEbpAddress(orderShippingAddress));
			}
			AddressModel orderPaymentAddress = order.getPaymentAddress();

			if (orderPaymentAddress != null)
			{
				LOG.debug(
						String.format("Order %s Payment Address PK: %s, Country: %s, Town: %s, StreetName: %s, Postalcode: %s",
								order.getCode(),
								orderPaymentAddress.getPk().toString(), orderPaymentAddress.getCountry(),
								orderPaymentAddress.getTown(),
								orderPaymentAddress.getStreetname(), orderPaymentAddress.getPostalcode()));
				user.setBillingAddress(convertToEbpAddress(orderPaymentAddress));
			}
		}

		populateUniversityData(userPayload, user);
	}

	private void populateUniversityData(final EbpCreateUserPayload userPayload, final UserNode user)
	{
		user.setEmployerOrUniversity(userPayload.getUniversity());
		user.setUniversityCountry(userPayload.getUniversityCountry());
		user.setUniversityState(userPayload.getUniversityState());

		//system should to send user details to EBP right after registration, obviously order is null
		if (userPayload.getOrder() != null)
		{
			//"A customer cannot be tagged as a student in Hybris, so this flag will be retrieved from the last order only."
			user.setIsStudent(checkoutService.isStudentOrder(userPayload.getOrder()));
		}
	}

	private AddressNode convertToEbpAddress(final AddressModel addressModel)
	{
		AddressNode address = new AddressNode();
		address.setCountry(addressModel.getCountry().getNumeric());
		address.setAddress1(addressModel.getLine1());
		address.setAddress2(addressModel.getLine2());
		address.setCity(addressModel.getTown());
		address.setZipCode(addressModel.getPostalcode());
		address.setPhoneNumber(addressModel.getPhone1());
		if (addressModel.getRegion() != null)
		{
			populateOtherStateValue(addressModel, address);
			populateStateValue(addressModel, address);
			if (addressModel.getRegion().getIsocodeShort() == null)
			{
				LOG.debug("Region IsocodeShort is empty for : Region [{}]", addressModel.getRegion().getIsocode());
			}
		}
		return address;
	}

	private void populateStateValue(final AddressModel addressModel, final AddressNode address)
	{
		if (countriesStateProvince.contains(addressModel.getCountry().getIsocode()))
		{
			address.setState(addressModel.getRegion().getIsocodeShort());
		}
	}

	private void populateOtherStateValue(final AddressModel addressModel, final AddressNode address)
	{
		if (countriesOtherStateProvince.contains(addressModel.getCountry().getIsocode()))
		{
			address.setOtherStateProvince(addressModel.getRegion().getIsocodeShort());
		}
	}

	public WileyCheckoutService getCheckoutService()
	{
		return checkoutService;
	}

	public void setCheckoutService(final WileyCheckoutService checkoutService)
	{
		this.checkoutService = checkoutService;
	}

	public Set<String> getCountriesOtherStateProvince()
	{
		return countriesOtherStateProvince;
	}

	public void setCountriesOtherStateProvince(final Set<String> countriesOtherStateProvince)
	{
		this.countriesOtherStateProvince = countriesOtherStateProvince;
	}

	public void setCountriesStateProvince(final Set<String> countriesStateProvince)
	{
		this.countriesStateProvince = countriesStateProvince;
	}
}
