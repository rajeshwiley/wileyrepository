package com.wiley.integrations.ebp.enums;

/**
 * Created by Raman_Hancharou on 4/18/2016.
 */
public enum WileyEbpPaymentMethod
{
	FREE("free"),
	CREDIT_CARD("credit card"),
	PAYPAL("paypal");

	private final String value;

	WileyEbpPaymentMethod(final String value)
	{
		this.value = value;
	}

	public String getValue()
	{
		return value;
	}
}
