package com.wiley.integrations.sabrix.dto;

import java.util.Objects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Tax Calculation Request enterprise business message to be used as input request for Tax Calculation Orchestration Service
 *
 *
 * <p>Java class for TaxCalculationRequestEBMType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="TaxCalculationRequestEBMType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/Meta/V1}EBMHeaderType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="UserData" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/TaxEBO/V1}UserDataType"/&gt;
 *         &lt;element ref="{http://www.sabrix.com/services/taxcalculationservice/2011-09-01}taxCalculationRequest"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlRootElement(namespace = "http://xmlns.wiley.com/EnterpriseBusinessObjects/TaxEBO/V1", name = "TaxCalculationRequestEBM")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TaxCalculationRequestEBM", namespace = "http://xmlns.wiley.com/EnterpriseBusinessObjects/TaxEBO/V1", propOrder = {
		"userData",
		"taxCalculationRequest"
})
public class TaxCalculationRequestEBMType
		extends EBMHeaderType
{

	@XmlElement(name = "UserData", required = true)
	protected UserDataType userData;
	@XmlElement(namespace = "http://www.sabrix.com/services/taxcalculationservice/2011-09-01", required = true)
	protected TaxCalculationRequest taxCalculationRequest;

	/**
	 * Gets the value of the userData property.
	 *
	 * @return possible object is
	 * {@link UserDataType }
	 */
	public UserDataType getUserData()
	{
		return userData;
	}

	/**
	 * Sets the value of the userData property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link UserDataType }
	 */
	public void setUserData(UserDataType value)
	{
		this.userData = value;
	}

	/**
	 * Gets the value of the taxCalculationRequest property.
	 *
	 * @return possible object is
	 * {@link TaxCalculationRequest }
	 */
	public TaxCalculationRequest getTaxCalculationRequest()
	{
		return taxCalculationRequest;
	}

	/**
	 * Sets the value of the taxCalculationRequest property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link TaxCalculationRequest }
	 */
	public void setTaxCalculationRequest(TaxCalculationRequest value)
	{
		this.taxCalculationRequest = value;
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		final TaxCalculationRequestEBMType that = (TaxCalculationRequestEBMType) o;
		return Objects.equals(userData, that.userData) &&
				Objects.equals(taxCalculationRequest, that.taxCalculationRequest);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(userData, taxCalculationRequest);
	}
}
