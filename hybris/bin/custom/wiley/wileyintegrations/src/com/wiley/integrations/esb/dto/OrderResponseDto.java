package com.wiley.integrations.esb.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.common.base.MoreObjects;
import com.wiley.integrations.adapters.xml.CommonUSDoubleAdapter;


/**
 * Dto for cart calculation and validation in ERP.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderResponseDto
{

	@XmlElement(required = true)
	@XmlJavaTypeAdapter(CommonUSDoubleAdapter.class)
	private Double deliveryCost;

	@XmlElement(required = true)
	@XmlJavaTypeAdapter(CommonUSDoubleAdapter.class)
	private Double paymentCost;

	@XmlElement(required = true)
	@XmlJavaTypeAdapter(CommonUSDoubleAdapter.class)
	private Double subtotal;

	@XmlElement(required = true)
	@XmlJavaTypeAdapter(CommonUSDoubleAdapter.class)
	private Double totalPrice;

	@XmlElement(required = true)
	@XmlJavaTypeAdapter(CommonUSDoubleAdapter.class)
	private Double totalDiscounts;

	@XmlElement(required = true)
	@XmlJavaTypeAdapter(CommonUSDoubleAdapter.class)
	private Double totalTax;

	private OrderModificationsResponseDto orderModifications;

	public Double getDeliveryCost()
	{
		return deliveryCost;
	}

	public void setDeliveryCost(final Double deliveryCost)
	{
		this.deliveryCost = deliveryCost;
	}

	public Double getPaymentCost()
	{
		return paymentCost;
	}

	public void setPaymentCost(final Double paymentCost)
	{
		this.paymentCost = paymentCost;
	}

	public Double getSubtotal()
	{
		return subtotal;
	}

	public void setSubtotal(final Double subtotal)
	{
		this.subtotal = subtotal;
	}

	public Double getTotalPrice()
	{
		return totalPrice;
	}

	public void setTotalPrice(final Double totalPrice)
	{
		this.totalPrice = totalPrice;
	}

	public Double getTotalDiscounts()
	{
		return totalDiscounts;
	}

	public void setTotalDiscounts(final Double totalDiscounts)
	{
		this.totalDiscounts = totalDiscounts;
	}

	public Double getTotalTax()
	{
		return totalTax;
	}

	public void setTotalTax(final Double totalTax)
	{
		this.totalTax = totalTax;
	}

	public OrderModificationsResponseDto getOrderModifications()
	{
		return orderModifications;
	}

	public void setOrderModifications(final OrderModificationsResponseDto orderModifications)
	{
		this.orderModifications = orderModifications;
	}


	@Override
	public String toString()
	{
		return MoreObjects.toStringHelper(this)
				.add("deliveryCost", deliveryCost)
				.add("paymentCost", paymentCost)
				.add("subtotal", subtotal)
				.add("totalPrice", totalPrice)
				.add("totalDiscounts", totalDiscounts)
				.add("totalTax", totalTax)
				.add("orderModifications", orderModifications)
				.toString();
	}
}
