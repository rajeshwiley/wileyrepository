package com.wiley.integrations.users.data;

/**
 * Created by Sergiy_Mishkovets on 7/13/2016.
 */
public class PasswordUpdateRequest
{
	private String newPassword;

	private String oldPassword;

	public String getOldPassword()
	{
		return oldPassword;
	}

	public void setOldPassword(final String oldPassword)
	{
		this.oldPassword = oldPassword;
	}

	public String getNewPassword()
	{
		return newPassword;
	}

	public void setNewPassword(final String newPassword)
	{
		this.newPassword = newPassword;
	}
}
