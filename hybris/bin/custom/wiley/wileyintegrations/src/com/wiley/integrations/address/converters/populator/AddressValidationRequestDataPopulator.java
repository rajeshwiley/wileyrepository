package com.wiley.integrations.address.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.annotation.Nonnull;

import org.springframework.util.Assert;

import com.wiley.core.integration.address.dto.AddressDto;
import com.wiley.integrations.address.dto.AddressValidationRequestDto;



public class AddressValidationRequestDataPopulator implements Populator<AddressDto, AddressValidationRequestDto>
{
	@Override
	public void populate(@Nonnull final AddressDto source, @Nonnull final AddressValidationRequestDto target)
			throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setCity(source.getTown());
		target.setAddressLine1(source.getLine1());
		target.setAddressLine2(source.getLine2());
		target.setRegion(source.getRegionIso2());
		target.setCountry(source.getCountryIso2());
		target.setPostalCode(source.getPostalCode());
	}
}
