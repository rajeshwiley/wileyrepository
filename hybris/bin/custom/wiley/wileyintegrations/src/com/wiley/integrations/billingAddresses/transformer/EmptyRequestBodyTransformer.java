package com.wiley.integrations.billingAddresses.transformer;

import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;


/**
 * Created by Mikhail_Asadchy on 7/13/2016.
 */
public class EmptyRequestBodyTransformer
{
	public Message transform(final Message incomingMessage)
	{
		return MessageBuilder.createMessage("", incomingMessage.getHeaders());
	}

}
