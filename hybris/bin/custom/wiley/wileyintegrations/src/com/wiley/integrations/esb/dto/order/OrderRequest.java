package com.wiley.integrations.esb.dto.order;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wiley.integrations.esb.dto.common.Address;
import com.wiley.integrations.esb.dto.common.CreditCardType;
import com.wiley.integrations.esb.dto.common.Discount;
import com.wiley.integrations.esb.dto.common.KeyValue;
import com.wiley.integrations.esb.dto.common.PaymentOptionCode;


/**
 * An order to be fullfilled.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderRequest
{

	/**
	 * Unique identifier of the order (cart).
	 */
	private String code;

	/**
	 * User's unique identifier. Generally, user emails are to be used as identifiers.
	 * Nevertheless, string literals are permitted to be used as identifiers too.
	 */
	private String userId;

	private String salesModel;

	/**
	 * Hybris Website ID. For example wileyb2b, wileyb2c
	 */
	private String website;

	/**
	 * Indicates whether an order was placed guest user
	 */
	private Boolean isUserGuest;

	/**
	 * Reflects how order was placed- regular purchase, WileyRegCode activation, grace period, limited access, test drive
	 */
	private String orderType;

	/**
	 * ID of school a customer is currently enrolled to
	 */
	private String schoolId;

	/**
	 * Customer's unique identifier that cannot be changed. This is an internal identifier assigned by Hybris.
	 */
	private String immutableUserId;

	/**
	 * Unique identifier of the user's B2B unit (company). Applicable only for B2B customers.
	 */
	private String sapAccountNumber;

	/**
	 * Two-letter ISO 3166-1 alpha-2 code that specifies a country.
	 * See https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2 for details.
	 */
	private String country;

	/**
	 * Three-letter ISO 4217 code that specifies a currency. See https://en.wikipedia.org/wiki/ISO_4217 for details.
	 */
	private String currency;

	/**
	 * Two-letter ISO 639 alpha-2 code that specifies a language. See https://en.wikipedia.org/wiki/ISO_639-1 for details.
	 */
	private String language;

	/**
	 * Code of the delivery option (priority code) that was chosen by the customer for this order.
	 */
	private String deliveryOptionCode;

	/**
	 * An address.
	 */
	private Address deliveryAddress;

	private String regCode;

	/**
	 * Code of the payment option that was chosen by the customer for this order.
	 */
	private PaymentOptionCode paymentOptionCode;

	/**
	 * if paymentOptionCode is selected as 'card' this field should contain credit card type in form
	 * how Wiley payment gateway provides it back.
	 */
	private CreditCardType creditCardType;

	/**
	 * An address.
	 */
	private Address paymentAddress;

	/**
	 * List of the order entries.
	 */
	private List<OrderEntryRequest> entries;

	/**
	 * List of the discount codes that were applied to this order.
	 */
	private List<Discount> discounts;

	/**
	 * Sum of all discounts (including any promotions and vouchers) applied to the order. The en_US locale is used for the value.
	 */
	private Double totalDiscount;

	/**
	 * Delivery cost for the chosen delivery option. The en_US locale is used for the value.
	 */
	private Double deliveryCost;

	/**
	 * Total tax for the order. The en_US locale is used for the value.
	 */
	private Double totalTax;

	/**
	 * Total price for the order. Total price is calculated as subtotal minus totalDiscount plus deliveryCost plus totalTax.
	 * The en_US locale is used for the value.
	 */
	private Double totalPrice;

	/**
	 * The PCI Compliant payment token which can be stored by the Vendor Application.
	 */
	private String paymentToken;

	/**
	 * The authorization code provided by the acquiring bank. Should be kept for reference and can be shown
	 */
	private String paymentAuthorizationCode;

	/**
	 * List of extra information in key-value format. It is supposed to pass additional field for unified cart interface
	 */
	private List<KeyValue> extraInfo;

	/**
	 * The date, when shipping is requested to start - input by end user. It is optional in general.
	 * Can be provided in case of B2B sales model only.
	 */
	private Date desiredShippingDate;

	/**
	 * Indicates that tax calculation failed during checkout process. Tax amount is zero for such cases.
	 */
	private boolean isTaxCalculationFailed;

	/**
	 * Customer's ECID. Optional field, it helps to avoid additional look up in CDM.
	 */
	private String individualEcidNumber;

	@JsonProperty("code")
	public String getCode()
	{
		return code;
	}

	public void setCode(final String code)
	{
		this.code = code;
	}

	@JsonProperty("userId")
	public String getUserId()
	{
		return userId;
	}

	public void setUserId(final String userId)
	{
		this.userId = userId;
	}

	@JsonProperty("salesModel")
	public String getSalesModel()
	{
		return salesModel;
	}

	public void setSalesModel(final String salesModel)
	{
		this.salesModel = salesModel;
	}

	@JsonProperty("website")
	public String getWebsite()
	{
		return website;
	}

	public void setWebsite(final String website)
	{
		this.website = website;
	}

	@JsonProperty("isUserGuest")
	public Boolean getUserGuest()
	{
		return isUserGuest;
	}

	public void setUserGuest(final Boolean userGuest)
	{
		isUserGuest = userGuest;
	}

	@JsonProperty("orderType")
	public String getOrderType()
	{
		return orderType;
	}

	public void setOrderType(final String orderType)
	{
		this.orderType = orderType;
	}

	@JsonProperty("schoolId")
	public String getSchoolId()
	{
		return schoolId;
	}

	public void setSchoolId(final String schoolId)
	{
		this.schoolId = schoolId;
	}

	@JsonProperty("immutableUserId")
	public String getImmutableUserId()
	{
		return immutableUserId;
	}

	public void setImmutableUserId(final String immutableUserId)
	{
		this.immutableUserId = immutableUserId;
	}

	@JsonProperty("sapAccountNumber")
	public String getSapAccountNumber()
	{
		return sapAccountNumber;
	}

	public void setSapAccountNumber(final String sapAccountNumber)
	{
		this.sapAccountNumber = sapAccountNumber;
	}

	@JsonProperty("country")
	public String getCountry()
	{
		return country;
	}

	public void setCountry(final String country)
	{
		this.country = country;
	}

	@JsonProperty("currency")
	public String getCurrency()
	{
		return currency;
	}

	public void setCurrency(final String currency)
	{
		this.currency = currency;
	}

	@JsonProperty("language")
	public String getLanguage()
	{
		return language;
	}

	public void setLanguage(final String language)
	{
		this.language = language;
	}

	@JsonProperty("deliveryOptionCode")
	public String getDeliveryOptionCode()
	{
		return deliveryOptionCode;
	}

	public void setDeliveryOptionCode(final String deliveryOptionCode)
	{
		this.deliveryOptionCode = deliveryOptionCode;
	}

	@JsonProperty("deliveryAddress")
	public Address getDeliveryAddress()
	{
		return deliveryAddress;
	}

	public void setDeliveryAddress(final Address deliveryAddress)
	{
		this.deliveryAddress = deliveryAddress;
	}

	@JsonProperty("regCode")
	public String getRegCode()
	{
		return regCode;
	}

	public void setRegCode(final String regCode)
	{
		this.regCode = regCode;
	}

	@JsonProperty("paymentOptionCode")
	public PaymentOptionCode getPaymentOptionCode()
	{
		return paymentOptionCode;
	}

	public void setPaymentOptionCode(final PaymentOptionCode paymentOptionCode)
	{
		this.paymentOptionCode = paymentOptionCode;
	}

	@JsonProperty("creditCardType")
	public CreditCardType getCreditCardType()
	{
		return creditCardType;
	}

	public void setCreditCardType(final CreditCardType creditCardType)
	{
		this.creditCardType = creditCardType;
	}

	@JsonProperty("paymentAddress")
	public Address getPaymentAddress()
	{
		return paymentAddress;
	}

	public void setPaymentAddress(final Address paymentAddress)
	{
		this.paymentAddress = paymentAddress;
	}

	@JsonProperty("entries")
	public List<OrderEntryRequest> getEntries()
	{
		return entries;
	}

	public void setEntries(final List<OrderEntryRequest> entries)
	{
		this.entries = entries;
	}

	@JsonProperty("discounts")
	public List<Discount> getDiscounts()
	{
		return discounts;
	}

	public void setDiscounts(final List<Discount> discounts)
	{
		this.discounts = discounts;
	}

	@JsonProperty("totalDiscount")
	public Double getTotalDiscount()
	{
		return totalDiscount;
	}

	public void setTotalDiscount(final Double totalDiscount)
	{
		this.totalDiscount = totalDiscount;
	}

	@JsonProperty("deliveryCost")
	public Double getDeliveryCost()
	{
		return deliveryCost;
	}

	public void setDeliveryCost(final Double deliveryCost)
	{
		this.deliveryCost = deliveryCost;
	}

	@JsonProperty("totalTax")
	public Double getTotalTax()
	{
		return totalTax;
	}

	public void setTotalTax(final Double totalTax)
	{
		this.totalTax = totalTax;
	}

	@JsonProperty("totalPrice")
	public Double getTotalPrice()
	{
		return totalPrice;
	}

	public void setTotalPrice(final Double totalPrice)
	{
		this.totalPrice = totalPrice;
	}

	@JsonProperty("paymentToken")
	public String getPaymentToken()
	{
		return paymentToken;
	}

	public void setPaymentToken(final String paymentToken)
	{
		this.paymentToken = paymentToken;
	}

	@JsonProperty("paymentAuthorizationCode")
	public String getPaymentAuthorizationCode()
	{
		return paymentAuthorizationCode;
	}

	public void setPaymentAuthorizationCode(final String paymentAuthorizationCode)
	{
		this.paymentAuthorizationCode = paymentAuthorizationCode;
	}

	@JsonProperty("extraInfo")
	public List<KeyValue> getExtraInfo()
	{
		return extraInfo;
	}

	public void setExtraInfo(final List<KeyValue> extraInfo)
	{
		this.extraInfo = extraInfo;
	}

	@JsonProperty("desiredShippingDate")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	public Date getDesiredShippingDate()
	{
		return desiredShippingDate;
	}

	public void setDesiredShippingDate(final Date desiredShippingDate)
	{
		this.desiredShippingDate = desiredShippingDate;
	}

	@JsonProperty("isTaxCalculationFailed")
	public boolean getTaxCalculationFailed()
	{
		return isTaxCalculationFailed;
	}

	public void setTaxCalculationFailed(final boolean taxCalculationFailed)
	{
		isTaxCalculationFailed = taxCalculationFailed;
	}

	@JsonProperty("individualEcidNumber")
	public String getIndividualEcidNumber()
	{
		return individualEcidNumber;
	}

	public void setIndividualEcidNumber(final String individualEcidNumber)
	{
		this.individualEcidNumber = individualEcidNumber;
	}

	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this);
	}
}
