package com.wiley.integrations.mpgs.transformer;


import de.hybris.platform.servicelayer.util.ServicesUtil;

import javax.annotation.Nonnull;

import org.apache.commons.lang.StringUtils;

import com.wiley.core.mpgs.dto.json.Address;
import com.wiley.core.mpgs.dto.json.Billing;
import com.wiley.core.mpgs.dto.json.Order;
import com.wiley.core.mpgs.dto.json.Session;
import com.wiley.core.mpgs.dto.json.SourceOfFunds;
import com.wiley.core.mpgs.dto.verify.MPGSVerifyRequestDTO;
import com.wiley.core.mpgs.request.WileyVerifyRequest;


public class WileyMPGSVerifyRequestTransformer
{
	@Nonnull
	public final MPGSVerifyRequestDTO transform(@Nonnull final WileyVerifyRequest request)
	{

		ServicesUtil.validateParameterNotNullStandardMessage("request", request);
		Order order = new Order();
		order.setCurrency(request.getCurrency());

		Session session = new Session();
		session.setId(request.getSessionId());

		SourceOfFunds sourceOfFunds = new SourceOfFunds();
		sourceOfFunds.setType(request.getSourceOfFundsType());

		MPGSVerifyRequestDTO verifyRequestDTO = new MPGSVerifyRequestDTO();
		verifyRequestDTO.setSession(session);
		verifyRequestDTO.setSourceOfFunds(sourceOfFunds);
		verifyRequestDTO.setApiOperation(request.getApiOperation());
		verifyRequestDTO.setBilling(createBillingFromRequest(request));
		verifyRequestDTO.setOrder(order);

		return verifyRequestDTO;
	}

	private Billing createBillingFromRequest(final WileyVerifyRequest request)
	{
		final Billing billing = new Billing();
		final Address billingAddress = new Address();
		billing.setAddress(billingAddress);
		billingAddress.setCity(request.getBillingCity());
		billingAddress.setCountry(request.getBillingCountry());
		billingAddress.setPostcodeZip(request.getBillingPostalCode());
		billingAddress.setStateProvince(request.getStateProvince());
		billingAddress.setStreet(request.getBillingStreet1());
		final String street2 = request.getBillingStreet2();
		billingAddress.setStreet2(StringUtils.isEmpty(street2) ? null : street2);
		return billing;
	}
}
