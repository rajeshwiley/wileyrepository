package com.wiley.integrations.order.populator;

import com.wiley.core.enums.OrderType;
import com.wiley.core.model.WileyExternalDiscountModel;
import com.wiley.core.order.strategies.calculation.WileyFindExternalPriceStrategy;
import com.wiley.core.payment.WileyPaymentModeService;
import com.wiley.integrations.exceptions.carts.UnknownCountryException;
import com.wiley.integrations.order.dto.AddressWsDTO;
import com.wiley.integrations.order.dto.CreateOrderEntryRequestWsDTO;
import com.wiley.integrations.order.dto.CreateOrderRequestWsDTO;
import com.wiley.integrations.order.dto.DiscountValueWsDTO;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.order.strategies.calculation.FindDiscountValuesStrategy;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.util.PriceValue;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static com.wiley.integrations.utils.PopulateUtils.populateFieldIfNotNull;

/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public class WileyasAbstractOrderReverseWsPopulator implements Populator<CreateOrderRequestWsDTO, AbstractOrderModel>
{
    private static final Logger LOG = LoggerFactory.getLogger(WileyasAbstractOrderReverseWsPopulator.class);

    @Resource
    private CommonI18NService commonI18NService;

    @Resource
    private WileyPaymentModeService wileyPaymentModeService;

    @Resource(name = "wileyasUserService")
    private UserService userService;

    @Resource
    private ProductService productService;

    @Resource
    private ModelService modelService;

    @Resource
    private WileyFindExternalPriceStrategy wileyFindExternalPriceStrategy;

    @Resource(name = "wileyFindExternalDiscountValuesStrategy")
    private FindDiscountValuesStrategy findDiscountValuesStrategy;

    @Resource
    private Converter<CreateOrderEntryRequestWsDTO, OrderEntryModel> wileyasOrderEntryReverseWsConverter;

    @Resource
    private Converter<AddressWsDTO, AddressModel> wileyasAddressReverseWsConverter;

    @Resource
    private Converter<DiscountValueWsDTO, WileyExternalDiscountModel> wileyExternalDiscountReverseWsConverter;

	@Resource
	private EnumerationService enumerationService;

    @Override
    public void populate(final CreateOrderRequestWsDTO source, final AbstractOrderModel target) throws ConversionException
    {
        Assert.notNull(source, "Parameter source cannot be null.");
        Assert.notNull(target, "Parameter target cannot be null.");
        if (StringUtils.isBlank(source.getId()))
        {
            target.setGuid(UUID.randomUUID().toString());
        }
        else
        {
            target.setGuid(source.getId());
        }

		populateFieldIfNotNull((dest, value) -> dest.setOrderType(
				enumerationService.getEnumerationValue(OrderType.class, value)), target, source.getType());
        populateFieldIfNotNull((dest, value) ->
                dest.setCountry(findCountryByCode(value)), target, source.getCountry());
        populateFieldIfNotNull((dest, value) ->
                dest.setCurrency(commonI18NService.getCurrency(value)), target, source.getCurrency());
        populateFieldIfNotNull((dest, value) ->
                dest.setUser(userService.getUserForUID(value)), target, source.getUserId());
        final List<CreateOrderEntryRequestWsDTO> entries = source.getEntries();
        target.setNet(true);
        if (CollectionUtils.isNotEmpty(entries))
        {
            final List<AbstractOrderEntryModel> convertedEntries = new ArrayList<>(entries.size());
            final Set<String> entryIds = new HashSet<>();
            BigDecimal subtotal = BigDecimal.ZERO;
            for (CreateOrderEntryRequestWsDTO entry : entries) {
                validateOrderEntryId(entry.getEntryId(), entryIds);
                final OrderEntryModel convertedEntry = populateEntry(entry, target);
                subtotal = subtotal.add(BigDecimal.valueOf(convertedEntry.getSubtotalPrice()));
                convertedEntries.add(convertedEntry);
            }
            target.setEntries(convertedEntries);
            target.setSubTotalWithoutDiscount(subtotal.doubleValue());
        }
        populateFieldIfNotNull((dest, value) ->
                dest.setPaymentMode(wileyPaymentModeService.getPaymentModeForCode(value)),
                target, source.getPaymentMode());
        populateFieldIfNotNull((dest, value) ->
                dest.setPaymentAddress(wileyasAddressReverseWsConverter.convert(value)),
                target, source.getPaymentAddress());
        if (target.getPaymentAddress() != null)
        {
            target.getPaymentAddress().setOwner(target);
        }
        populateFieldIfNotNull((dest, value) ->
                dest.setContactAddress(wileyasAddressReverseWsConverter.convert(value)),
                target, source.getContactAddress());
        if (target.getContactAddress() != null)
        {
            target.getContactAddress().setOwner(target);
        }
        target.setExternalText(source.getExternalText());
        target.setPurchaseOrderNumber(source.getPurchaseOrderNumber());
        target.setUserNotes(source.getUserNotes());

        populateOrderTotals(source, target);
        populateFieldIfNotNull((dest, value) ->
                        dest.setExternalDiscounts(Converters.convertAll(value, wileyExternalDiscountReverseWsConverter)),
                target, source.getExternalDiscounts());
        populateGlobalDiscountValues(target);

        target.setTaxNumber(source.getTaxNumber());
        target.setTaxNumberValidated(BooleanUtils.isTrue(source.isTaxNumberValidated()));
        target.setTaxNumberExpirationDate(source.getTaxNumberExpirationDate());
        target.setTaxCalculated(source.isTaxCalculated());
        target.setAppliedCouponCodes(source.getAppliedCouponCodes());
        target.setDate(source.getDate());
        target.setSourceSystem(source.getSourceSystem());
        target.setStatus(OrderStatus.CREATED);

        if  (source.getPaymentMode() != null)
        {
            populatePaymentInfo(target);
        }
    }

    private OrderEntryModel populateEntry(final CreateOrderEntryRequestWsDTO entry,
            final AbstractOrderModel target)
    {
        final OrderEntryModel convertedEntry = wileyasOrderEntryReverseWsConverter.convert(entry);
        convertedEntry.setOrder(target);

        populateUnit(entry.getProductCode(), convertedEntry);
        populateEntryTotals(convertedEntry);
        populateDiscountValues(target.getCurrency(), convertedEntry);

        return convertedEntry;
    }

    private void populateUnit(final String productCode, final OrderEntryModel convertedEntry)
    {
        final ProductModel product = productService.getProductForCode(productCode);
        if (product != null) {
            convertedEntry.setUnit(productService.getOrderableUnit(product));
        }
    }

    private void populateEntryTotals(final OrderEntryModel convertedEntry)
    {
        final PriceValue externalPrice = wileyFindExternalPriceStrategy.findExternalPrice(convertedEntry);
		if (externalPrice != null)
		{
            convertedEntry.setSubtotalPrice(externalPrice.getValue());
            convertedEntry.setBasePrice(convertedEntry.getSubtotalPrice() / convertedEntry.getQuantity());
        }
    }

    private void populateOrderTotals(final CreateOrderRequestWsDTO source, final AbstractOrderModel target)
    {
        target.setSubtotal(source.getSubtotal());
        target.setTotalDiscounts(source.getTotalDiscounts());
        target.setTotalTax(source.getTotalTax());
        target.setTotalPrice(source.getTotalPrice());
    }

    private void populateGlobalDiscountValues(final AbstractOrderModel target)
    {
        if (CollectionUtils.isNotEmpty(target.getExternalDiscounts()))
        {
            try
            {
                List<DiscountValue> discountValues = findDiscountValuesStrategy.findDiscountValues(target);
                target.setGlobalDiscountValues(prepareAppliedDiscountValues(target.getCurrency(), discountValues,
                        target.getSubtotal(), 1d));
            }
            catch (CalculationException e)
            {
                LOG.error("Cannot set appliedValues for Order [" + target.getCode() + "]", e);
            }
        }
    }

    private void populateDiscountValues(final CurrencyModel currency, final OrderEntryModel convertedEntry)
    {
        if (CollectionUtils.isNotEmpty(convertedEntry.getExternalDiscounts()))
        {
            try
            {
                final List<DiscountValue> discountValues = findDiscountValuesStrategy.findDiscountValues(convertedEntry);
                convertedEntry.setDiscountValues(prepareAppliedDiscountValues(currency, discountValues,
                        convertedEntry.getSubtotalPrice(), convertedEntry.getQuantity().doubleValue()));
            }
            catch (CalculationException e)
            {
                LOG.error("Cannot set appliedValues for OrderEntry [" + convertedEntry.getGuid() + "]", e);
            }
        }
    }

    private void populatePaymentInfo(final AbstractOrderModel target)
    {
        final PaymentInfoModel paymentInfo = modelService.create(PaymentInfoModel.class);
        target.setPaymentInfo(paymentInfo);

        paymentInfo.setUser(target.getUser());
        paymentInfo.setCode(target.getUser().getUid() + "_" + UUID.randomUUID());
    }

    private List<DiscountValue> prepareAppliedDiscountValues(final CurrencyModel currency, final List<DiscountValue> discounts,
            final double externalPrice, final double quantity)
    {
        final int digits = currency.getDigits();
        return DiscountValue.apply(quantity, externalPrice, digits, discounts, currency.getIsocode());
    }

    private CountryModel findCountryByCode(final String countryCode)
    {
        try
        {
            return commonI18NService.getCountry(countryCode);
        }
        catch (UnknownIdentifierException ex)
        {
            throw new UnknownCountryException(ex.getMessage());
        }
    }

    private void validateOrderEntryId(final String entryId, final Set<String> entryIds)
    {
        if (!entryIds.add(entryId))
        {
            throw new AmbiguousIdentifierException("OrderEntry with id '" + entryId + "' was found!");
        }
    }
}
