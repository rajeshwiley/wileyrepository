/**
 *
 */
package com.wiley.integrations.cdm.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;

import com.wiley.integrations.cdm.dto.CDMAddressDTO;
import com.wiley.integrations.cdm.dto.CDMCreateCustomerRequestEBMDTO;
import com.wiley.integrations.cdm.dto.CDMCustomerDTO;
import com.wiley.integrations.cdm.dto.CDMCustomerEBMDTO;
import com.wiley.integrations.cdm.dto.CDMCustomerIdentificationDTO;
import com.wiley.integrations.cdm.dto.CDMEmailDTO;
import com.wiley.integrations.cdm.dto.CDMIndividualDTO;
import com.wiley.integrations.cdm.dto.CDMSourceXREFDTO;
import com.wiley.integrations.cdm.dto.CustomerBaseProfileDTO;


public class CustomerToCDMCustomerPopulator implements Populator<CustomerModel, CDMCustomerDTO> {

  private static final String SINGLE_SPACE = " ";

  /**
   * The action code will be CREATE for create customer request.
   */
  private static final String ACTION_CODE = "CREATE";

  private static final String CREATE_CUSTOMER_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";

  private static final String CUSTOMER_TYPE_CODE = "I";

  @Resource(name = "addressToCDMBillingAddressPopulator")
  private Populator<AddressModel, CDMAddressDTO> addressToCDMBillingAddressPopulator;

  @Resource(name = "addressToCDMShippingAddressPopulator")
  private Populator<AddressModel, CDMAddressDTO> addressToCDMShippingAddressPopulator;

  /**
   * Converting CustomerModel to CDMCustomerDTO to generate request for CDM
   *
   */
  @Override
  public void populate(@NotNull final CustomerModel source, @NotNull final CDMCustomerDTO target)
      throws ConversionException {
    final CDMCreateCustomerRequestEBMDTO createCustomerRequestEBM = new CDMCreateCustomerRequestEBMDTO();

    createCustomerRequestEBM.setActionCode(ACTION_CODE);
    final DateFormat dateFormat = new SimpleDateFormat(CREATE_CUSTOMER_DATE_FORMAT);
    createCustomerRequestEBM.setDateTime(dateFormat.format(source.getCreationtime()));
    addCustomerEBM(source, createCustomerRequestEBM);

    target.setCreateCustomerRequestEBM(createCustomerRequestEBM);
  }

  private void addCustomerEBM(final CustomerModel source,
      final CDMCreateCustomerRequestEBMDTO createCustomerRequestEBM) {
    final CDMCustomerEBMDTO customerEBM = new CDMCustomerEBMDTO();

    addCustomerBaseProfile(source, customerEBM);
    addIndividual(source, customerEBM);
    addAddresses(source, customerEBM);
    addEmail(source, customerEBM);

    createCustomerRequestEBM.setCustomerEBM(customerEBM);
  }

  private void addAddresses(final CustomerModel source, final CDMCustomerEBMDTO customerEBM) {
    
    final ArrayList<CDMAddressDTO> addressList = new ArrayList<>();
    
    if (source.getDefaultShipmentAddress() != null)
    {
      // Add shipment address
      final CDMAddressDTO shipmentAddressDTO = new CDMAddressDTO();
      addressToCDMShippingAddressPopulator.populate(source.getDefaultShipmentAddress(),
          shipmentAddressDTO);
      addressList.add(shipmentAddressDTO);
    }

    if (source.getDefaultPaymentAddress() != null)
    {
      // Add payment address
      final CDMAddressDTO paymentAddressDTO = new CDMAddressDTO();
      addressToCDMBillingAddressPopulator.populate(source.getDefaultPaymentAddress(),
          paymentAddressDTO); 
      addressList.add(paymentAddressDTO);
  
      customerEBM.setAddress(addressList);
    }
  }

  private void addIndividual(final CustomerModel source, final CDMCustomerEBMDTO customerEBM) {
    final CDMIndividualDTO individual = new CDMIndividualDTO();
    individual.setFullName(source.getFirstName() + SINGLE_SPACE + source.getLastName());
    individual.setFirstName(source.getFirstName());
    individual.setLastName(source.getLastName());
    customerEBM.setIndividual(individual);
  }

  private void addCustomerBaseProfile(final CustomerModel source,
      final CDMCustomerEBMDTO customerEBM) {
    final CustomerBaseProfileDTO customerBaseProfile = new CustomerBaseProfileDTO();
    addCustomerIdentification(source, customerBaseProfile);
    customerBaseProfile.setCustomerTypeCode(CUSTOMER_TYPE_CODE);
    customerEBM.setCustomerBaseProfile(customerBaseProfile);
  }

  private void addCustomerIdentification(final CustomerModel source,
      final CustomerBaseProfileDTO customerBaseProfile) {
    final CDMCustomerIdentificationDTO customerIdentification = new CDMCustomerIdentificationDTO();
    addSourceXRef(source, customerIdentification);
    customerBaseProfile.setCustomerIdentification(customerIdentification);
  }

  private void addSourceXRef(final CustomerModel source,
      final CDMCustomerIdentificationDTO customerIdentification) {
    final CDMSourceXREFDTO sourceXREF = new CDMSourceXREFDTO();
    sourceXREF.setSourceCustomerID(source.getPk().getLongValueAsString());
    customerIdentification.setSourceXREF(sourceXREF);
  }

  private void addEmail(final CustomerModel source, final CDMCustomerEBMDTO customerEBM) {
    final CDMEmailDTO email = new CDMEmailDTO();
    email.setEmailAddress(source.getContactEmail());
    customerEBM.setEmail(email);
  }

}
