package com.wiley.integrations.eloqua.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.annotation.Nonnull;

import org.springframework.util.Assert;

import com.wiley.integrations.promotioncode.PromotionCodeProvider;
import com.wiley.core.integration.eloqua.dto.CustomObjectDataDto;
import com.wiley.core.integration.eloqua.dto.FieldValueDto;
import com.wiley.integrations.eloqua.util.CustomObjectDataUtils;


/**
 * Populates CustomObjectData from Order.
 *
 * @author Aliaksei_Zlobich
 */
public class OrderToCustomObjectDataPopulator implements Populator<OrderModel, CustomObjectDataDto>
{

	public static final int HYBRIS_ORDER_NUMBER_FIELD_VALUE = 5263;
	public static final int HYBRIS_STOREFRONT_FILED_VALUE = 5400;
	public static final int ORDER_DATE_FIELD_VALUE = 5264;
	public static final int DISCOUNT_CODE_FIELD_VALUE = 5268;

	private PromotionCodeProvider promotionCodeProvider;

	@Override
	public void populate(@Nonnull final OrderModel order, @Nonnull final CustomObjectDataDto cdo) throws ConversionException
	{
		Assert.notNull(order);
		Assert.notNull(cdo);

		final List<FieldValueDto> fieldValues = cdo.getFieldValues();

		addHybrisOrderNumberFieldValue(fieldValues, order);
		addHybrisStorefrontFieldValue(fieldValues, order);
		addOrderDateFieldValue(fieldValues, order);
		addPromotionCodeFieldValue(fieldValues, order);
	}

	private void addHybrisOrderNumberFieldValue(final List<FieldValueDto> fieldValues, final OrderModel order)
	{
		fieldValues.add(CustomObjectDataUtils.createFieldValue(HYBRIS_ORDER_NUMBER_FIELD_VALUE, order.getCode()));
	}

	private void addHybrisStorefrontFieldValue(final List<FieldValueDto> fieldValues, final OrderModel order)
	{
		final BaseStoreModel store = order.getStore();
		fieldValues.add(CustomObjectDataUtils.createFieldValue(HYBRIS_STOREFRONT_FILED_VALUE, store.getUid()));
	}

	private void addOrderDateFieldValue(final List<FieldValueDto> fieldValues, final OrderModel order)
	{
		final Date date = order.getDate();
		if (date != null)
		{
			final int orderDate = CustomObjectDataUtils.toUnixTime(date);
			fieldValues.add(CustomObjectDataUtils.createFieldValue(ORDER_DATE_FIELD_VALUE, String.valueOf(orderDate)));
		}
	}

	private void addPromotionCodeFieldValue(final List<FieldValueDto> fieldValues, final OrderModel order)
	{
		final Optional<String> promotionCode = promotionCodeProvider.getOrderPromotionCode(order);
		if (promotionCode.isPresent())
		{
			fieldValues.add(CustomObjectDataUtils.createFieldValue(DISCOUNT_CODE_FIELD_VALUE, promotionCode.get()));
		}
	}

	public void setPromotionCodeProvider(final PromotionCodeProvider promotionCodeProvider)
	{
		this.promotionCodeProvider = promotionCodeProvider;
	}
}
