package com.wiley.integrations.databind;

import java.text.DecimalFormat;
import java.text.FieldPosition;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import com.fasterxml.jackson.databind.util.ISO8601Utils;


/**
 * Copy of the com.fasterxml.jackson.databind.util.ISO8601DateFormat.
 * This class is created to use old date formatting logic used in Jackson library prior to v2.9.
 */
public class JsonISO8601DateFormat extends SimpleDateFormat
{
	private static final long serialVersionUID = 1L;

	public JsonISO8601DateFormat()
	{
		this.numberFormat = new DecimalFormat();
		this.calendar = new GregorianCalendar();
	}

	public StringBuffer format(final Date date, final StringBuffer toAppendTo, final FieldPosition fieldPosition)
	{
		toAppendTo.append(ISO8601Utils.format(date));
		return toAppendTo;
	}

	public Date parse(final String source, final ParsePosition pos)
	{
		try
		{
			return ISO8601Utils.parse(source, pos);
		}
		catch (ParseException var4)
		{
			return null;
		}
	}

	public Date parse(final String source) throws ParseException
	{
		return ISO8601Utils.parse(source, new ParsePosition(0));
	}

	public Object clone()
	{
		return this;
	}
}