package com.wiley.integrations.inventory;

import de.hybris.platform.acceleratorservices.dataimport.batch.BatchHeader;
import de.hybris.platform.acceleratorservices.dataimport.batch.converter.ImpexConverter;
import de.hybris.platform.acceleratorservices.dataimport.batch.task.ImpexTransformerTask;

import java.util.Map;


/**
 * Created by Uladzimir_Barouski on 7/22/2016.
 */
public class WileycomInventoryImpexTransformerTask extends ImpexTransformerTask
{

	@Override
	protected void buildReplacementSymbols(final Map<String, String> symbols, final BatchHeader header,
			final ImpexConverter converter)
	{
		super.buildReplacementSymbols(symbols, header, converter);
		if (header instanceof InventoryBatchHeader)
		{
			symbols.put("$WAREHOUSE$", ((InventoryBatchHeader) header).getWarehouseCode());
		}
	}
}
