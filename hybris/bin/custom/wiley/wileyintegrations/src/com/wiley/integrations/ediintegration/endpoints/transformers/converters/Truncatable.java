package com.wiley.integrations.ediintegration.endpoints.transformers.converters;

import java.util.Map;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;

public interface Truncatable
{
	default String truncate(final String value, final String key) {
		Map<String, Integer> restrictions = getAddressFieldsLengthRestrictions();
		if (restrictions == null || restrictions.get(key) == null || restrictions.get(key) < 1) {
			return value;
		}
		final Integer restriction = restrictions.get(key);
		if (isNotEmpty(value) && value.length() > restriction) {
			return value.substring(0, restriction);
		}
		return value;
	}

	Map<String, Integer> getAddressFieldsLengthRestrictions();
}
