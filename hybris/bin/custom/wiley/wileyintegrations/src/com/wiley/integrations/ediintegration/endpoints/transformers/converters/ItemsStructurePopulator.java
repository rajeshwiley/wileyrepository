package com.wiley.integrations.ediintegration.endpoints.transformers.converters;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.subscriptionservices.model.SubscriptionProductModel;
import de.hybris.platform.subscriptionservices.model.SubscriptionTermModel;
import de.hybris.platform.util.TaxValue;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.model.WileyGiftCardProductModel;
import com.wiley.core.price.WileyDiscountCalculationService;
import com.wiley.fulfilmentprocess.model.WileyExportProcessModel;
import com.wiley.integrations.ediintegration.endpoints.jaxb.ItemsStructure;
import com.wiley.integrations.ediintegration.endpoints.jaxb.builder.ItemsStructureBuilder;


/**
 * Converter to convert Order to ItemsStricture jaxb bean
 */
public class ItemsStructurePopulator implements Populator<WileyExportProcessModel, List<ItemsStructure>>
{
	private static final Logger LOG = Logger.getLogger(ItemsStructurePopulator.class);

	static final String MONTHLY_SUBSCRIPTION = "PNOW";
	static final String YEARLY_SUBSCRIPTION = "PTSUB";

	@Autowired
	private WileyDiscountCalculationService wileyDiscountCalculationService;

	@Override
	public void populate(final WileyExportProcessModel exportProcess,
			final List<ItemsStructure> itemsStructures) throws ConversionException
	{
		final OrderModel orderModel = exportProcess.getOrder();
		ItemsStructureBuilder itemsStructureBuilder = ItemsStructureBuilder.createInstance()
				.withPurchaseOrderNumber(ConversionUtils.buildPONumberFormat(orderModel));
		List<AbstractOrderEntryModel> orderEntriesForExport = orderModel.getEntries();

		itemsStructures.addAll(orderEntriesForExport.stream()
				.filter(entry -> !(entry.getProduct() instanceof WileyGiftCardProductModel))
				.map(entry -> itemsStructureBuilder.but()
						.withProductId(entry.getProduct().getIsbn())
						.withQuantity(entry.getQuantity())
						.withPrice(entry.getBasePrice())
						.withDiscount(wileyDiscountCalculationService.getEntryProportionalDiscount(entry).doubleValue())
						.withTaxAmount(calculateTaxValueForEntry(entry))
						.withSalesModel(getSalesModel(entry.getProduct()))
						.build()).collect(Collectors.toList()));
	}


	protected String getSalesModel(final ProductModel productModel)
	{
		if (productModel instanceof SubscriptionProductModel)
		{
			SubscriptionProductModel subscriptionProduct = (SubscriptionProductModel) productModel;
			SubscriptionTermModel subscriptionTerm = subscriptionProduct.getSubscriptionTerm();
			if (subscriptionTerm != null)
			{
				return getSubscriptionType(subscriptionTerm);
			}
			else
			{
				LOG.warn("Subscription term is null for SubscriptionProduct with code: [" + subscriptionProduct.getCode() + "]");
			}
		}
		return StringUtils.EMPTY;
	}

	protected String getSubscriptionType(final SubscriptionTermModel subscriptionTerm)
	{
		switch (subscriptionTerm.getSubscriptionTermFrequencyValue())
		{
			case MONTHLY:
				return MONTHLY_SUBSCRIPTION;
			case YEARLY:
				return YEARLY_SUBSCRIPTION;
			default:
				return StringUtils.EMPTY;
		}
	}


	private Double calculateTaxValueForEntry(final AbstractOrderEntryModel entry)
	{
		return entry.getTaxValues().stream().mapToDouble(TaxValue::getValue).sum();
	}
}
