package com.wiley.integrations.ebp.transformer;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.CustomerModel;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import com.wiley.core.integration.ebp.dto.EbpCreateUserPayload;
import com.wiley.integrations.ebp.dto.CreateUserArgNode;
import com.wiley.integrations.ebp.dto.EbpCreateUserRootNode;
import com.wiley.integrations.ebp.dto.UserNode;

/**
 * Created by Uladzimir_Barouski on 2/16/2016.
 */
public class CustomerToEbpUserTransformer
{
	private static final Logger LOG = LoggerFactory.getLogger(CustomerToEbpUserTransformer.class);
	@Resource
	private Populator<EbpCreateUserPayload, UserNode> customerToEbpUserPopulator;

	@Nonnull
	public EbpCreateUserRootNode transform(@Nonnull final EbpCreateUserPayload  ebpCreateUserPayload)
	{
		final CustomerModel customer = ebpCreateUserPayload.getCustomer();
		Assert.notNull(customer);

		UserNode user = new UserNode();
		customerToEbpUserPopulator.populate(ebpCreateUserPayload, user);
		
		CreateUserArgNode ebpUserAccount = new CreateUserArgNode();
		ebpUserAccount.setUser(user);
		ebpUserAccount.setClientID(customer.getCustomerID());

		EbpCreateUserRootNode createUserDto = new EbpCreateUserRootNode();
		createUserDto.setArg(ebpUserAccount);

		return createUserDto;
	}
}