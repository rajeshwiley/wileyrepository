package com.wiley.integrations.vies.marshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.DirectFieldAccessor;
import org.springframework.oxm.XmlMappingException;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.oxm.mime.MimeContainer;
import org.springframework.ws.soap.saaj.SaajSoapMessage;
import javax.xml.transform.Source;
import com.wiley.core.exceptions.ExternalSystemInternalErrorException;
import com.wiley.core.exceptions.ExternalSystemBadRequestException;

/**
 * Custom Jaxb2Marshaller implementation for soap fault message handling during unmarshalling.
 *
 */
public class ViesMarshaller extends Jaxb2Marshaller
{
    private static final Logger LOG = LoggerFactory.getLogger(ViesMarshaller.class);
    private static final String INVALID_INPUT = "INVALID_INPUT";

    @Override
    public Object unmarshal(final Source source) throws XmlMappingException
    {
        return super.unmarshal(source, null);
    }

    @Override
    public Object unmarshal(final Source source, final MimeContainer mimeContainer)
    {
        Object mimeMessage = new DirectFieldAccessor(mimeContainer).getPropertyValue("mimeMessage");
        Object unmarshalObject = null;
        if (mimeMessage instanceof SaajSoapMessage)
        {
            SaajSoapMessage soapMessage = (SaajSoapMessage) mimeMessage;
            String faultReason = soapMessage.getFaultReason();
            if (faultReason != null)
            {
                LOG.debug("VIES fault reason: {}", faultReason);
                if (INVALID_INPUT.equals(faultReason)) {
                    throw new ExternalSystemBadRequestException(faultReason);
                } else {
                    throw new ExternalSystemInternalErrorException(faultReason);
                }
            } else
            {
                unmarshalObject = super.unmarshal(source, mimeContainer);
            }
        }
        return unmarshalObject;
    }
}
