package com.wiley.integrations.address.transformer;

import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.springframework.messaging.Message;
import org.springframework.util.Assert;

import com.wiley.core.integration.address.dto.AddressDto;
import com.wiley.integrations.address.dto.AddressValidationRequestDto;


public class AddressValidationRequestDataTransformer
{
	@Resource
	private Converter<AddressDto, AddressValidationRequestDto> addressDataAddressValidationRequestDtoConverter;

	@Nonnull
	public AddressValidationRequestDto transform(@Nonnull final Message<AddressDto> message)
	{
		Assert.notNull(message, "Message can not be null");
		return addressDataAddressValidationRequestDtoConverter.convert(message.getPayload());
	}
}
