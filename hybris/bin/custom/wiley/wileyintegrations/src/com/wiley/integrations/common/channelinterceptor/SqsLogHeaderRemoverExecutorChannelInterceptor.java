package com.wiley.integrations.common.channelinterceptor;

import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.support.ExecutorChannelInterceptor;


public class SqsLogHeaderRemoverExecutorChannelInterceptor implements ExecutorChannelInterceptor
{
	private static final String JDBC_CHANNEL_MESSAGE_STORE_SAVED = "JdbcChannelMessageStore.SAVED";
	private static final String JSON_TYPE_ID = "json__TypeId__";

	@Override
	public Message<?> beforeHandle(final Message<?> message, final MessageChannel messageChannel,
			final MessageHandler messageHandler)
	{
		return MessageBuilder
				.fromMessage(message)
				.removeHeaders(JDBC_CHANNEL_MESSAGE_STORE_SAVED, JSON_TYPE_ID)
				.build();
	}

	@Override
	public void afterMessageHandled(final Message<?> message, final MessageChannel messageChannel,
			final MessageHandler messageHandler, final Exception exception)
	{
		//ignore
	}

	@Override
	public Message<?> preSend(final Message<?> message, final MessageChannel messageChannel)
	{
		return message;
	}

	@Override
	public void postSend(final Message<?> message, final MessageChannel messageChannel, final boolean b)
	{
		//ignore
	}

	@Override
	public void afterSendCompletion(final Message<?> message, final MessageChannel messageChannel, final boolean b,
			final Exception e)
	{
		//ignore
	}

	@Override
	public boolean preReceive(final MessageChannel messageChannel)
	{
		return true;
	}

	@Override
	public Message<?> postReceive(final Message<?> message, final MessageChannel messageChannel)
	{
		return message;
	}

	@Override
	public void afterReceiveCompletion(final Message<?> message, final MessageChannel messageChannel, final Exception e)
	{
		//ignore
	}
}
