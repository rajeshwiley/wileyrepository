/**
 *
 */
package com.wiley.integrations.cdm.dto;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonInclude(JsonInclude.Include.NON_NULL)
public class CDMCustomerDTO {


  @JsonProperty(value = "CreateCustomerRequestEBM")
  private CDMCreateCustomerRequestEBMDTO createCustomerRequestEBM;

  /**
   * @return the createCustomerRequestEBM
   */
  public CDMCreateCustomerRequestEBMDTO getCreateCustomerRequestEBM() {
    return createCustomerRequestEBM;
  }

  /**
   * @param createCustomerRequestEBM
   *          the createCustomerRequestEBM to set
   */
  public void setCreateCustomerRequestEBM(
      final CDMCreateCustomerRequestEBMDTO createCustomerRequestEBM) {
    this.createCustomerRequestEBM = createCustomerRequestEBM;
  }

  @Override
  public String toString() {
    return "CDMCustomerDTO [CreateCustomerRequestEBM=" + createCustomerRequestEBM + "]";
  }
}
