package com.wiley.integrations.sabrix.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ServiceContextType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="ServiceContextType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="BusinessProcessName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Activity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ServiceName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ComponentName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MethodName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceContextType", namespace = "http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/Meta/V1", propOrder = {
		"businessProcessName",
		"activity",
		"serviceName",
		"componentName",
		"methodName"
})
public class ServiceContextType
{

	@XmlElement(name = "BusinessProcessName")
	protected String businessProcessName;
	@XmlElement(name = "Activity")
	protected String activity;
	@XmlElement(name = "ServiceName")
	protected String serviceName;
	@XmlElement(name = "ComponentName")
	protected String componentName;
	@XmlElement(name = "MethodName")
	protected String methodName;

	/**
	 * Gets the value of the businessProcessName property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getBusinessProcessName()
	{
		return businessProcessName;
	}

	/**
	 * Sets the value of the businessProcessName property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setBusinessProcessName(String value)
	{
		this.businessProcessName = value;
	}

	/**
	 * Gets the value of the activity property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getActivity()
	{
		return activity;
	}

	/**
	 * Sets the value of the activity property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setActivity(String value)
	{
		this.activity = value;
	}

	/**
	 * Gets the value of the serviceName property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getServiceName()
	{
		return serviceName;
	}

	/**
	 * Sets the value of the serviceName property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setServiceName(String value)
	{
		this.serviceName = value;
	}

	/**
	 * Gets the value of the componentName property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getComponentName()
	{
		return componentName;
	}

	/**
	 * Sets the value of the componentName property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setComponentName(String value)
	{
		this.componentName = value;
	}

	/**
	 * Gets the value of the methodName property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getMethodName()
	{
		return methodName;
	}

	/**
	 * Sets the value of the methodName property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setMethodName(String value)
	{
		this.methodName = value;
	}

}
