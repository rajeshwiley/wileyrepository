package com.wiley.integrations.esb.dto.delivery;

import java.util.List;
import java.util.Objects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Created by Uladzimir_Barouski on 6/16/2016.
 */
@XmlRootElement(name = "Order")
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DeliveryOptionRequestDto
{
	@JsonProperty("country")
	private String country;
	@JsonProperty("currency")
	private String currency;
	@JsonProperty("language")
	private String language;
	@XmlElement
	@JsonProperty("deliveryAddress")
	private DeliveryAddressDto deliveryAddress;
	@XmlElement
	@JsonProperty("entries")
	private List<OrderEntryDto> entries;
	@JsonProperty("totalWeight")
	private Double totalWeight;
	@JsonProperty("deliveryOptionCode")
	private String deliveryOptionCode;

	public String getCountry()
	{
		return country;
	}

	public void setCountry(final String country)
	{
		this.country = country;
	}

	public String getCurrency()
	{
		return currency;
	}

	public void setCurrency(final String currency)
	{
		this.currency = currency;
	}

	public String getLanguage()
	{
		return language;
	}

	public void setLanguage(final String language)
	{
		this.language = language;
	}

	public DeliveryAddressDto getDeliveryAddress()
	{
		return deliveryAddress;
	}

	public void setDeliveryAddress(final DeliveryAddressDto deliveryAddress)
	{
		this.deliveryAddress = deliveryAddress;
	}

	public List<OrderEntryDto> getEntries()
	{
		return entries;
	}

	public void setEntries(final List<OrderEntryDto> entries)
	{
		this.entries = entries;
	}

	public Double getTotalWeight()
	{
		return totalWeight;
	}

	public void setTotalWeight(final Double totalWeight)
	{
		this.totalWeight = totalWeight;
	}

	public String getDeliveryOptionCode()
	{
		return deliveryOptionCode;
	}

	public void setDeliveryOptionCode(final String deliveryOptionCode)
	{
		this.deliveryOptionCode = deliveryOptionCode;
	}

	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this);
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		final DeliveryOptionRequestDto that = (DeliveryOptionRequestDto) o;
		return Objects.equals(country, that.country)
				&& Objects.equals(currency, that.currency)
				&& Objects.equals(language, that.language)
				&& Objects.equals(deliveryAddress, that.deliveryAddress)
				&& Objects.equals(entries, that.entries)
				&& Objects.equals(totalWeight, that.totalWeight)
				&& Objects.equals(deliveryOptionCode, that.deliveryOptionCode);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(country, currency, language, deliveryAddress, entries, totalWeight, deliveryOptionCode);
	}
}
