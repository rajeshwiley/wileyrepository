package com.wiley.integrations.mpgs.transformer;


import de.hybris.platform.servicelayer.util.ServicesUtil;

import javax.annotation.Nonnull;

import com.wiley.core.mpgs.dto.json.Session;
import com.wiley.core.mpgs.dto.json.SourceOfFunds;
import com.wiley.core.mpgs.dto.token.MPGSTokenRequestDTO;
import com.wiley.core.mpgs.request.WileyTokenizationRequest;


public class WileyMPGSTokenizeRequestTransformer
{

	public MPGSTokenRequestDTO transform(@Nonnull final WileyTokenizationRequest request)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("request", request);
		SourceOfFunds sourceOfFunds = new SourceOfFunds();
		sourceOfFunds.setType(request.getSourceOfFundsType());

		Session session = new Session();
		session.setId(request.getSessionId());

		MPGSTokenRequestDTO tokenRequest = new MPGSTokenRequestDTO();
		tokenRequest.setSession(session);
		tokenRequest.setSourceOfFunds(sourceOfFunds);

		return tokenRequest;
	}
}