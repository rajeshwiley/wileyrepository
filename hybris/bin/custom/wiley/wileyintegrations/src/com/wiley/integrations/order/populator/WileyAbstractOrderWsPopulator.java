package com.wiley.integrations.order.populator;

import de.hybris.platform.converters.Converters;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.util.DiscountValue;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;

import com.wiley.core.model.WileyExternalDiscountModel;
import com.wiley.integrations.order.dto.AbstractOrderWsDTO;
import com.wiley.integrations.order.dto.AddressWsDTO;
import com.wiley.integrations.order.dto.DiscountValueWsDTO;
import com.wiley.integrations.order.dto.OrderEntryWsDTO;

import static com.wiley.integrations.utils.PopulateUtils.populateFieldIfNotNull;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


public class WileyAbstractOrderWsPopulator implements Populator<AbstractOrderModel, AbstractOrderWsDTO>
{
	@Resource
	private Converter<AddressModel, AddressWsDTO> wileyAddressWsConverter;

	@Resource
	private Converter<AbstractOrderEntryModel, OrderEntryWsDTO> wileyOrderEntryWsConverter;

	@Resource
	private Converter<DiscountValue, DiscountValueWsDTO> wileyDiscountValueWsConverter;

	@Resource
	private Converter<WileyExternalDiscountModel, DiscountValueWsDTO> wileyExternalDiscountWsConverter;

	@Override
	public void populate(@NotNull final AbstractOrderModel source, @NotNull final AbstractOrderWsDTO target)
			throws ConversionException
	{
		validateParameterNotNullStandardMessage("source", source);
		validateParameterNotNullStandardMessage("target", target);

		target.setId(source.getGuid());
		target.setCode(source.getCode());
		target.setExternalText(source.getExternalText());
		target.setPurchaseOrderNumber(source.getPurchaseOrderNumber());
		target.setUserNotes(source.getUserNotes());
		target.setSubtotal(source.getSubtotal());
		target.setTaxNumber(source.getTaxNumber());
		target.setTaxNumberValidated(source.getTaxNumberValidated());
		target.setTaxNumberExpirationDate(source.getTaxNumberExpirationDate());
		target.setTotalDiscounts(source.getTotalDiscounts());

		target.setTotalTax(source.getTotalTax());
		target.setTaxCalculated(source.getTaxCalculated());
		target.setTotalPrice(source.getTotalPrice());
		target.setAppliedCouponCodes(source.getAppliedCouponCodes());
		target.setDate(source.getDate());
		target.setModificationTime(source.getModifiedtime());

		populateFieldIfNotNull((dest, value) -> dest.setType(value.getCode()), target, source.getOrderType());
		populateFieldIfNotNull((dest, value) -> dest.setSiteId(value.getUid()), target, source.getSite());
		populateFieldIfNotNull((dest, value) -> dest.setCountry(value.getIsocode()), target, source.getCountry());
		populateFieldIfNotNull((dest, value) -> dest.setCurrency(value.getIsocode()), target, source.getCurrency());
		populateFieldIfNotNull((dest, value) -> dest.setUserId(value.getUid()), target, source.getUser());

		populateFieldIfNotNull((dest, value) -> dest.setEntries(Converters.convertAll(value,
				wileyOrderEntryWsConverter)), target, source.getEntries());

		populateFieldIfNotNull((dest, value) -> dest.setPaymentMode(
				AbstractOrderWsDTO.PaymentModeEnum.valueOf(value.getCode().toUpperCase())), target,
				source.getPaymentMode());
		populateFieldIfNotNull((dest, value) -> dest.setPaymentAddress(wileyAddressWsConverter.convert(value)),
				target, source.getPaymentAddress());
		populateFieldIfNotNull((dest, value) -> dest.setContactAddress(wileyAddressWsConverter.convert(value)),
				target, source.getContactAddress());
		populateFieldIfNotNull((dest, value) ->
				dest.setDiscounts(Converters.convertAll(value,
						wileyDiscountValueWsConverter)), target, source.getGlobalDiscountValues());
		populateFieldIfNotNull((dest, value) ->
				dest.setExternalDiscounts(Converters.convertAll(value,
						wileyExternalDiscountWsConverter)), target, source.getExternalDiscounts());
	}
}
