package com.wiley.integrations.selector.strategies.impl;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import org.apache.commons.lang.reflect.FieldUtils;
import org.apache.log4j.Logger;
import org.springframework.messaging.Message;


/**
 * Created by Uladzimir_Barouski on 12/7/2016.
 */
public class WileyStoreFromOrderStrategyImpl extends WileyAbstractStoreSelectorStrategy
{
	private static final Logger LOG = Logger.getLogger(WileyStoreFromOrderStrategyImpl.class);

	@Override
	public Optional<BaseStoreModel> getCurrentBaseStore(final Message<?> message)
	{
		BaseSiteModel currentBaseSite = null;
		if (message.getPayload() instanceof OrderModel)
		{
			currentBaseSite = ((OrderModel) message.getPayload()).getSite();
		}
		else
		{
			Object payload = message.getPayload();
			try
			{
				OrderModel order = (OrderModel) FieldUtils.readDeclaredField(payload, "order", true);
				currentBaseSite = order.getSite();
			}
			catch (IllegalAccessException | IllegalArgumentException e)
			{
				LOG.warn("Payload does not contain an order: " + e.getMessage());
			}
		}

		return findBaseStoreForSite(currentBaseSite);
	}
}
