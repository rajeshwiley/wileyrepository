package com.wiley.integrations.sabrix.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for AuditFieldType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="AuditFieldType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CreatedUser" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="CreatedDateTime" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1}DateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="ModifiedUser" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="ModifiedDateTime" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1}DateTimeType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AuditFieldType", namespace = "http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1", propOrder = {
		"createdUser",
		"createdDateTime",
		"modifiedUser",
		"modifiedDateTime"
})
public class AuditFieldType
{

	@XmlElement(name = "CreatedUser")
	protected String createdUser;
	@XmlElement(name = "CreatedDateTime")
	@XmlSchemaType(name = "dateTime")
	protected XMLGregorianCalendar createdDateTime;
	@XmlElement(name = "ModifiedUser")
	protected String modifiedUser;
	@XmlElement(name = "ModifiedDateTime")
	@XmlSchemaType(name = "dateTime")
	protected XMLGregorianCalendar modifiedDateTime;

	/**
	 * Gets the value of the createdUser property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getCreatedUser()
	{
		return createdUser;
	}

	/**
	 * Sets the value of the createdUser property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setCreatedUser(String value)
	{
		this.createdUser = value;
	}

	/**
	 * Gets the value of the createdDateTime property.
	 *
	 * @return possible object is
	 * {@link XMLGregorianCalendar }
	 */
	public XMLGregorianCalendar getCreatedDateTime()
	{
		return createdDateTime;
	}

	/**
	 * Sets the value of the createdDateTime property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link XMLGregorianCalendar }
	 */
	public void setCreatedDateTime(XMLGregorianCalendar value)
	{
		this.createdDateTime = value;
	}

	/**
	 * Gets the value of the modifiedUser property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getModifiedUser()
	{
		return modifiedUser;
	}

	/**
	 * Sets the value of the modifiedUser property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setModifiedUser(String value)
	{
		this.modifiedUser = value;
	}

	/**
	 * Gets the value of the modifiedDateTime property.
	 *
	 * @return possible object is
	 * {@link XMLGregorianCalendar }
	 */
	public XMLGregorianCalendar getModifiedDateTime()
	{
		return modifiedDateTime;
	}

	/**
	 * Sets the value of the modifiedDateTime property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link XMLGregorianCalendar }
	 */
	public void setModifiedDateTime(XMLGregorianCalendar value)
	{
		this.modifiedDateTime = value;
	}

}
