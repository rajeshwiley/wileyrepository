package com.wiley.integrations.esb.converters.populator.deliveryoption;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.wiley.integrations.esb.dto.delivery.DeliveryOptionRequestDto;


/**
 * Created by Uladzimir_Barouski on 6/16/2016.
 */
public class CartToDeliveryOptionRequestDtoSimplePopulator implements Populator<CartModel, DeliveryOptionRequestDto>
{
	@Resource
	private CommonI18NService commonI18NService;

	@Override
	public void populate(final CartModel cartModel, final DeliveryOptionRequestDto deliveryOptionRequestDto)
			throws ConversionException
	{
		Assert.notNull(cartModel);
		Assert.notNull(deliveryOptionRequestDto);
		deliveryOptionRequestDto.setCurrency(cartModel.getCurrency().getIsocode());
		if (cartModel.getCountry() != null)
		{
			deliveryOptionRequestDto.setCountry(cartModel.getCountry().getIsocode());
		}
		deliveryOptionRequestDto.setLanguage(commonI18NService.getCurrentLanguage().getIsocode());
	}
}
