package com.wiley.integrations.esb.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.common.base.MoreObjects;


/**
 * Abstract dto to keep common part between cart modification types.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(JsonInclude.Include.NON_NULL)
public abstract class AbstractOrderEntryModificationResponseDto
{
	private String message; // reason of modification

	@XmlElement(required = true)
	private String statusCode;

	public String getMessage()
	{
		return message;
	}

	public void setMessage(final String message)
	{
		this.message = message;
	}

	public String getStatusCode()
	{
		return statusCode;
	}

	public void setStatusCode(final String statusCode)
	{
		this.statusCode = statusCode;
	}

	@Override
	public String toString()
	{
		return MoreObjects.toStringHelper(this)
				.add("message", message)
				.add("statusCode", statusCode)
				.toString();
	}
}
