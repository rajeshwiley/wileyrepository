package com.wiley.integrations.sabrix.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Contains data pertaining to a single license, consisting of a number-type pair.
 *
 *
 * <p>Java class for IndataLicensesDetailType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="IndataLicensesDetailType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="NUMBER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IndataLicensesDetailType", propOrder = {
		"number",
		"type"
})
public class IndataLicensesDetailType
{

	@XmlElement(name = "NUMBER")
	protected String number;
	@XmlElement(name = "TYPE")
	protected String type;

	/**
	 * Gets the value of the number property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getNUMBER()
	{
		return number;
	}

	/**
	 * Sets the value of the number property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setNUMBER(String value)
	{
		this.number = value;
	}

	/**
	 * Gets the value of the type property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getTYPE()
	{
		return type;
	}

	/**
	 * Sets the value of the type property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setTYPE(String value)
	{
		this.type = value;
	}

}
