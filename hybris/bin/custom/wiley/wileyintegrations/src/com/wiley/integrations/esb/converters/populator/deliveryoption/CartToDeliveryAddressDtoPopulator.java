package com.wiley.integrations.esb.converters.populator.deliveryoption;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.wiley.integrations.esb.dto.delivery.DeliveryAddressDto;


/**
 * Created by Uladzimir_Barouski on 6/16/2016.
 */
public class CartToDeliveryAddressDtoPopulator implements Populator<CartModel, DeliveryAddressDto>
{
	@Resource
	private AddressToDeliveryAddressDtoPopulator addressToDeliveryAddressDtoPopulator;

	
	@Override
	public void populate(final CartModel cartModel, final DeliveryAddressDto deliveryAddressDto) throws ConversionException
	{
		Assert.notNull(cartModel);
		Assert.notNull(deliveryAddressDto);

		//delivery address is required accrding to yaml,
		//Assert.notNull exception is expected if it's not defined
		addressToDeliveryAddressDtoPopulator.populate(cartModel.getDeliveryAddress(), deliveryAddressDto);
	}
}
