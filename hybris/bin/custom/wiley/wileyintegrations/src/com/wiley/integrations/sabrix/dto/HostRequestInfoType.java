package com.wiley.integrations.sabrix.dto;

import java.util.Objects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Contains pass-through elements used to identify the originating request from the ERP source.
 *
 *
 * <p>Java class for HostRequestInfoType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="HostRequestInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="HOST_REQUEST_ID" type="{http://www.sabrix.com/services/taxcalculationservice/2011-09-01}HostRequestIdType" minOccurs="0"/&gt;
 *         &lt;element name="HOST_REQUEST_LOG_ENTRY_ID" type="{http://www.sabrix.com/services/taxcalculationservice/2011-09-01}HostRequestLogEntryIdType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HostRequestInfoType", propOrder = {
		"hostrequestid",
		"hostrequestlogentryid"
})
public class HostRequestInfoType
{

	@XmlElement(name = "HOST_REQUEST_ID")
	protected String hostrequestid;
	@XmlElement(name = "HOST_REQUEST_LOG_ENTRY_ID")
	protected String hostrequestlogentryid;

	/**
	 * Gets the value of the hostrequestid property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getHOSTREQUESTID()
	{
		return hostrequestid;
	}

	/**
	 * Sets the value of the hostrequestid property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setHOSTREQUESTID(String value)
	{
		this.hostrequestid = value;
	}

	/**
	 * Gets the value of the hostrequestlogentryid property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getHOSTREQUESTLOGENTRYID()
	{
		return hostrequestlogentryid;
	}

	/**
	 * Sets the value of the hostrequestlogentryid property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setHOSTREQUESTLOGENTRYID(String value)
	{
		this.hostrequestlogentryid = value;
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		final HostRequestInfoType that = (HostRequestInfoType) o;
		return Objects.equals(hostrequestid, that.hostrequestid) &&
				Objects.equals(hostrequestlogentryid, that.hostrequestlogentryid);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(hostrequestid, hostrequestlogentryid);
	}
}
