package com.wiley.integrations.users.data;

public class UidUpdateRequest
{
	private String userId;

	private String password;

	public String getPassword()
	{
		return password;
	}

	public void setPassword(final String password)
	{
		this.password = password;
	}

	public String getUserId()
	{
		return userId;
	}

	public void setUserId(final String userId)
	{
		this.userId = userId;
	}
}
