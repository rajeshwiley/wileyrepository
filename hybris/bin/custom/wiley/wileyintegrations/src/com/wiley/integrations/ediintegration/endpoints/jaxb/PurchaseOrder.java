package com.wiley.integrations.ediintegration.endpoints.jaxb;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


@XmlAccessorType(XmlAccessType.FIELD)
public class PurchaseOrder
{
	@XmlElement(name = "BillToCustomer")
	private BillToCustomer billToCustomer;
	@XmlElement(name = "ShipToCustomer")
	private ShipToCustomer shipToCustomer;
	@XmlElement(name = "CurrencyCode")
	private String currencyCode;
	@XmlElement(name = "OrderType")
	private String orderType;
	@XmlElement(name = "ShippingChargeAmount")
	@XmlJavaTypeAdapter(DoubleAdapter.class)
	private Double shippingChargeAmount;
	@XmlElement(name = "ShipMethod")
	private String shipMethod;
	@XmlElement(name = "CreditCardToken")
	private String creditCardToken;
	@XmlElement(name = "CreditCardExpiry")
	private String creditCardExpiry;
	@XmlElement(name = "CreditCardType")
	private String creditCardType;
	@XmlElement(name = "PromotionCode")
	private String promotionCode;
	@XmlElement(name = "EmailAddress")
	private String emailAddress;
	@XmlElement(name = "TransactionID")
	private String transactionID;
	@XmlElement(name = "SalespersonCode")
	private String salespersonCode;
	@XmlElement(name = "MarketOutlet")
	private String marketOutlet;
	@XmlElement(name = "HybrisAuthorizationCode")
	private String hybrisAuthorizationCode;
	@XmlElement(name = "ItemsStructure")
	List<ItemsStructure> itemsStructures;

	public BillToCustomer getBillToCustomer()
	{
		return billToCustomer;
	}

	public void setBillToCustomer(final BillToCustomer billToCustomer)
	{
		this.billToCustomer = billToCustomer;
	}

	public ShipToCustomer getShipToCustomer()
	{
		return shipToCustomer;
	}

	public void setShipToCustomer(final ShipToCustomer shipToCustomer)
	{
		this.shipToCustomer = shipToCustomer;
	}

	public String getCurrencyCode()
	{
		return currencyCode;
	}

	public void setCurrencyCode(final String currencyCode)
	{
		this.currencyCode = currencyCode;
	}

	public String getOrderType()
	{
		return orderType;
	}

	public void setOrderType(final String orderType)
	{
		this.orderType = orderType;
	}

	public Double getShippingChargeAmount()
	{
		return shippingChargeAmount;
	}

	public void setShippingChargeAmount(final Double shippingChargeAmount)
	{
		this.shippingChargeAmount = shippingChargeAmount;
	}

	public String getShipMethod()
	{
		return shipMethod;
	}

	public void setShipMethod(final String shipMethod)
	{
		this.shipMethod = shipMethod;
	}

	public String getCreditCardToken()
	{
		return creditCardToken;
	}

	public void setCreditCardToken(final String creditCardToken)
	{
		this.creditCardToken = creditCardToken;
	}

	public String getCreditCardExpiry()
	{
		return creditCardExpiry;
	}

	public void setCreditCardExpiry(final String creditCardExpiry)
	{
		this.creditCardExpiry = creditCardExpiry;
	}

	public String getCreditCardType()
	{
		return creditCardType;
	}

	public void setCreditCardType(final String creditCardType)
	{
		this.creditCardType = creditCardType;
	}

	public String getPromotionCode()
	{
		return promotionCode;
	}

	public void setPromotionCode(final String promotionCode)
	{
		this.promotionCode = promotionCode;
	}

	public String getEmailAddress()
	{
		return emailAddress;
	}

	public void setEmailAddress(final String emailAddress)
	{
		this.emailAddress = emailAddress;
	}

	public String getTransactionID()
	{
		return transactionID;
	}

	public void setTransactionID(final String transactionID)
	{
		this.transactionID = transactionID;
	}

	public String getSalespersonCode()
	{
		return salespersonCode;
	}

	public void setSalespersonCode(final String salespersonCode)
	{
		this.salespersonCode = salespersonCode;
	}

	public String getMarketOutlet()
	{
		return marketOutlet;
	}

	public void setMarketOutlet(final String marketOutlet)
	{
		this.marketOutlet = marketOutlet;
	}

	public String getHybrisAuthorizationCode()
	{
		return hybrisAuthorizationCode;
	}

	public void setHybrisAuthorizationCode(final String hybrisAuthorizationCode)
	{
		this.hybrisAuthorizationCode = hybrisAuthorizationCode;
	}

	public List<ItemsStructure> getItemsStructures()
	{
		return itemsStructures;
	}

	public void setItemsStructures(final List<ItemsStructure> itemsStructures)
	{
		this.itemsStructures = itemsStructures;
	}
}
