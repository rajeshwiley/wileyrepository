package com.wiley.integrations.exceptions.carts;

/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public class UnknownCountryException extends RuntimeException
{
	public UnknownCountryException(final String message)
	{
		super(message);
	}
}
