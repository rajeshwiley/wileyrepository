package com.wiley.integrations.sabrix.transformer.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.util.Config;

import com.wiley.integrations.sabrix.dto.UserDataType;


/**
 * @author Dzmitryi_Halahayeu
 */
public class UserDataPopulator implements Populator<Void, UserDataType>
{

	private String senderId;

	@Override
	public void populate(final Void empty, final UserDataType userData) throws ConversionException
	{
		userData.setSourceSystemAppID(senderId);
		userData.setUniqueRequestIdentifier(Config.getParameter("sabrix.userData.uniqueRequestIdentifier"));
	}

	public void setSenderId(final String senderId)
	{
		this.senderId = senderId;
	}
}
