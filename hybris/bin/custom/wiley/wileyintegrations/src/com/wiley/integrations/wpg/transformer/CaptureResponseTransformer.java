package com.wiley.integrations.wpg.transformer;

import de.hybris.platform.payment.dto.TransactionStatus;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nonnull;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.util.Assert;

import com.wiley.core.payment.enums.WileyTransactionStatusEnum;
import com.wiley.core.payment.response.WileyCaptureResult;
import com.wiley.core.payment.strategies.SecurityHashGeneratorStrategy;
import com.wiley.integrations.wpg.populators.WileyPaymentResponsePopulator;


public class CaptureResponseTransformer
{
	public static final String KEY_SITE_ID = "_internalSiteId";
	public static final String SITE_HEADER = "siteId";

	private static final String KEY_OPERATION = "operation";
	private static final String KEY_RETURNCODE = "returnCode";
	private static final String KEY_RETURNMESSAGE = "returnMessage";
	private static final String KEY_VENDORID = "vendorID";
	private static final String KEY_MERCHANTRESPONSE = "merchantResponse";
	private static final String KEY_MERCHANTID = "merchantID";
	private static final String KEY_CSCRESULT = "CSCResult";
	private static final String KEY_ADDR_RESULT = "AVSAddrResult";
	private static final String KEY_POST_RESULT = "AVSPostResult";

	private static final String KEY_TRANSID = "transID";
	private static final String KEY_VALUE = "value";
	private static final String KEY_CURRENCY = "currency";
	private static final String KEY_AUTHCODE = "authCode";

	private static final String KEY_ACQUIRERID = "acquirerID";
	private static final String KEY_ACQUIRERNAME = "acquirerName";
	private static final String KEY_BANKID = "bankID";
	private static final String KEY_BANKNAME = "bankName";
	private static final String KEY_MASKEDCARDNUMBER = "maskedCardNumber";
	private static final String KEY_CARDEXPIRY = "cardExpiry";
	private static final String KEY_TIMESTAMP = "timestamp";
	private static final String KEY_SECURITY = "security";


	private static final Logger LOG = LoggerFactory.getLogger(CaptureResponseTransformer.class);

	@Autowired
	private SecurityHashGeneratorStrategy wpgHttpSecurityHashGeneratorStrategy;

	@Autowired
	private WileyPaymentResponsePopulator wileyPaymentResponsePopulator;

	public WileyCaptureResult transform(@Nonnull final Message<String> message)
	{
		Assert.notNull(message);

		final String response = message.getPayload();
		final String siteId = (String) message.getHeaders().get(SITE_HEADER);
		final Map<String, String> responseMap = new HashMap<>();

		wileyPaymentResponsePopulator.populate(response, responseMap);
		responseMap.put(KEY_SITE_ID, siteId);
		final WileyCaptureResult captureResult = new WileyCaptureResult();
		if (!verifyRequestSecurity(responseMap))
		{
			LOG.error("Wrong security token received from WPG for Token Settle operation");
			captureResult.setStatus(WileyTransactionStatusEnum.SECURITY_TOKEN_MISMATCH_FROM_WPG);
			captureResult.setTransactionStatus(TransactionStatus.ERROR);
		}
		else
		{
			String returnCodeString = responseMap.get(KEY_RETURNCODE);
			WileyTransactionStatusEnum status = WileyTransactionStatusEnum.findByCode(returnCodeString);
			captureResult.setStatus(status == null ? WileyTransactionStatusEnum.UNEXPECTED_CODE_FROM_WPG : status);

			populateTransactionStatus(captureResult, status);
			captureResult.setMerchantResponse(responseMap.get(KEY_MERCHANTRESPONSE));
			captureResult.setRequestId(responseMap.get(KEY_TRANSID));
			String value = responseMap.get(KEY_VALUE);
			if (StringUtils.isNotEmpty(value))
			{
				captureResult.setTotalAmount(new BigDecimal(value));
			}
			captureResult.setRequestTime(new Date());
		}
		return captureResult;
	}

	private void populateTransactionStatus(final WileyCaptureResult captureResult, final WileyTransactionStatusEnum status)
	{
		if (WileyTransactionStatusEnum.SUCCESS == status)
		{
			captureResult.setTransactionStatus(TransactionStatus.ACCEPTED);
		}
		else
		{
			captureResult.setTransactionStatus(TransactionStatus.REJECTED);
		}
	}

	private boolean verifyRequestSecurity(final Map<String, String> response)
	{
		StringBuilder builder = new StringBuilder();
		builder.append(getResponseValue(response, KEY_OPERATION));
		builder.append(getResponseValue(response, KEY_RETURNCODE));
		builder.append(getResponseValue(response, KEY_RETURNMESSAGE));
		builder.append(getResponseValue(response, KEY_VENDORID));
		builder.append(getResponseValue(response, KEY_TRANSID));
		builder.append(getResponseValue(response, KEY_VALUE));
		builder.append(getResponseValue(response, KEY_CURRENCY));
		builder.append(getResponseValue(response, KEY_MERCHANTRESPONSE));
		builder.append(getResponseValue(response, KEY_MERCHANTID));
		builder.append(getResponseValue(response, KEY_CSCRESULT));
		builder.append(getResponseValue(response, KEY_ADDR_RESULT));
		builder.append(getResponseValue(response, KEY_POST_RESULT));
		builder.append(getResponseValue(response, KEY_AUTHCODE));
		builder.append(getResponseValue(response, KEY_ACQUIRERID));
		builder.append(getResponseValue(response, KEY_ACQUIRERNAME));
		builder.append(getResponseValue(response, KEY_BANKID));
		builder.append(getResponseValue(response, KEY_BANKNAME));
		builder.append(getResponseValue(response, KEY_MASKEDCARDNUMBER));
		builder.append(getResponseValue(response, KEY_CARDEXPIRY));
		builder.append(getResponseValue(response, KEY_TIMESTAMP));

		String hash = wpgHttpSecurityHashGeneratorStrategy.generateSecurityHash(response.get(KEY_SITE_ID), builder.toString());
		return hash.equals(response.get(KEY_SECURITY));
	}

	private String getResponseValue(final Map<String, String> response, final String key)
	{
		String value = response.get(key);
		return value == null ? StringUtils.EMPTY : value;
	}
}
