package com.wiley.integrations.mpgs.transformer;


import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;

import com.wiley.core.mpgs.dto.authorization.MPGSAuthorizationResponseDTO;
import com.wiley.core.mpgs.dto.json.Response;
import com.wiley.core.mpgs.dto.json.Transaction;
import com.wiley.core.mpgs.response.WileyAuthorizationResponse;
import com.wiley.core.mpgs.services.WileyTransformationService;


public class WileyMPGSAuthorizationResponseTransformer
{
	@Autowired
	private WileyTransformationService wileyTransformationService;

	public WileyAuthorizationResponse transform(@Nonnull final Message<MPGSAuthorizationResponseDTO> message)
	{
		MPGSAuthorizationResponseDTO resultDto = message.getPayload();
		WileyAuthorizationResponse authorizationResponse = new WileyAuthorizationResponse();

		authorizationResponse.setStatus(wileyTransformationService.transformStatusIfSuccessful(resultDto.getResult()));

		Response response = resultDto.getResponse();
		authorizationResponse.setStatusDetails(response.getGatewayCode());

		Transaction transaction = resultDto.getTransaction();
		authorizationResponse.setTransactionId(transaction.getId());
		authorizationResponse.setCurrency(transaction.getCurrency());
		authorizationResponse.setTotalAmount(transaction.getAmount());

		String transactionTime = resultDto.getTimeOfRecord();
		if (transactionTime != null)
		{
			authorizationResponse.setTransactionCreatedTime(wileyTransformationService.transformStringToDate(transactionTime));
		}

		authorizationResponse.setToken(resultDto.getSourceOfFunds().getToken());

		return authorizationResponse;
	}
}
