package com.wiley.integrations.article.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.i18n.I18NService;

import java.util.Locale;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import com.wiley.integrations.article.dto.ArticleRequestDto;



public class ArticleRequestToCustomObjectDataPopulator implements Populator<String, ArticleRequestDto>
{
	@Autowired
	private I18NService i18nService;

	@Override
	public void populate(@Nonnull final String source, @Nonnull final ArticleRequestDto target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setId(source);
		final Locale locale = i18nService.getCurrentLocale();
		if (locale != null)
		{
			target.setLocale(locale.toString());
		}
	}
}
