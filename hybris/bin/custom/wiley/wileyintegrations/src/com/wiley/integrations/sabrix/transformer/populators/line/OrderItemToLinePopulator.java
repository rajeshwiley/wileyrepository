package com.wiley.integrations.sabrix.transformer.populators.line;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import com.wiley.core.product.WileyProductEditionFormatService;
import com.wiley.integrations.sabrix.dto.IndataLineType;
import com.wiley.integrations.sabrix.dto.ZoneAddressType;
import com.wiley.integrations.sabrix.transformer.populators.AddressToZoneAddressTypePopulator;


/**
 * @author Dzmitryi_Halahayeu
 */
public class OrderItemToLinePopulator implements Populator<AbstractOrderEntryModel, IndataLineType>
{
	@Resource
	private WileyProductEditionFormatService wileyProductEditionFormatService;
	@Resource
	private AddressToZoneAddressTypePopulator addressToZoneAddressTypePopulator;

	@Override
	public void populate(@Nonnull final AbstractOrderEntryModel orderEntryModel, @Nonnull final IndataLineType indataLineType)
			throws ConversionException
	{
		AbstractOrderModel order = orderEntryModel.getOrder();
		boolean isDigital = wileyProductEditionFormatService.isDigitalProduct(orderEntryModel.getProduct());

		if (order.getDeliveryAddress() == null || isDigital)
		{
			ZoneAddressType payToAddress = new ZoneAddressType();
			addressToZoneAddressTypePopulator.populate(order.getPaymentAddress(), payToAddress);
			indataLineType.setSHIPTO(payToAddress);
			indataLineType.setBUYERPRIMARY(payToAddress);
		}
		else
		{
			ZoneAddressType shipToAddress = new ZoneAddressType();
			addressToZoneAddressTypePopulator.populate(order.getDeliveryAddress(), shipToAddress);
			indataLineType.setSHIPTO(shipToAddress);
			indataLineType.setBUYERPRIMARY(shipToAddress);
		}
	}

}
