package com.wiley.integrations.ediintegration.endpoints.mail.excel;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.util.localization.Localization;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;

import com.wiley.core.integration.edi.Headers;
import com.wiley.core.integration.edi.dto.ExcelReportOrder;
import com.wiley.fulfilmentprocess.model.WileyExportProcessModel;
import com.wiley.integrations.ediintegration.endpoints.mail.WileyExcelReportEmailHeaders;
import com.wiley.integrations.ediintegration.endpoints.transformers.converters.ExcelReportOrderPopulator;

import static com.wiley.integrations.constants.WileyintegrationsConstants.EXCEL_DATE_COLUMN_FORMATTER;


public class WileyExcelReportHandler
{

	private static final Logger LOG = Logger.getLogger(WileyExcelReportHandler.class);
	private static final int FIRST_ROW_WITH_DATA = 1;
	private static final int FIRST_COLUMN_WITH_DATA = 0;
	private static final String FILE_CONTENT_ON_EXCEPTION = "Exception in excel file generation";
	private static final String USD_CURRENCY_CODE = "USD";
	private static final int COLUMN_WIDTH_ADJUSTMENT = 400;

	@Autowired
	private ExcelReportOrderPopulator excelReportOrderPopulator;



	@Resource(name = "excelReportColumns")
	private ArrayList<String> excelReportColumns;

	public Message createExcelReport(final Message<List<WileyExportProcessModel>> message)
	{
		final String siteShortcut = WileyExcelReportEmailHeaders.getSiteShortcut(message);
		LOG.info(String.format("Creating email with xls file for site %s", siteShortcut));

		List<WileyExportProcessModel> exportedProcesses = new ArrayList<>(message.<WileyExportProcessModel> getPayload());
		final HSSFWorkbook wb = crateExcelWorkbook(exportedProcesses,  siteShortcut);

		LOG.debug("Poi workbook created.");
		try (ByteArrayOutputStream excelOutput = new ByteArrayOutputStream())
		{
			LOG.debug(String.format("ByteArrayOutputStream is created for site %s", siteShortcut));
			wb.write(excelOutput);
			LOG.debug(String.format("HSSFWorkbook is written to ByteArrayOutputStream for site %s", siteShortcut));
			byte[] workbookByteArray = excelOutput.toByteArray();
			LOG.debug(String.format("ByteArrayOutputStream is converted to byte[] for site %s", siteShortcut));
			return MessageBuilder.withPayload(workbookByteArray).setHeader(Headers.CONVERTED_PROCESSES, exportedProcesses)
					.build();
		}
		catch (Exception ex)
		{
			LOG.debug(String.format("Creating email with xls file exception for site %s", siteShortcut), ex);
			return MessageBuilder.withPayload(FILE_CONTENT_ON_EXCEPTION).build();
		}
	}



	private HSSFWorkbook crateExcelWorkbook(final List<WileyExportProcessModel> exportedProcesses, final String siteShortcut)
	{

		LOG.debug(String.format("Start creating excel workbook for site %s", siteShortcut));
		HSSFWorkbook wb = new HSSFWorkbook();
		// writing the content from the exported orders
		List<WileyExportProcessModel> exportedProcessesUSD = exportedProcesses
				.stream()
				.filter(exportedProcess -> orderWithUSDCurrency(exportedProcess))
				.collect(Collectors.toList());
		HSSFSheet sheetUSD = wb.createSheet(Localization.getLocalizedString("wiley.orders.report.file.sheet1.name"));
		fillSheet(sheetUSD, exportedProcessesUSD, siteShortcut);

		List<WileyExportProcessModel> exportedProcessesAllOtherCurrencies = exportedProcesses.stream()
				.filter(exportProcess -> !orderWithUSDCurrency(exportProcess))
				.collect(Collectors.toList());



		HSSFSheet sheetAllOtherCurrencies = wb.createSheet(
				Localization.getLocalizedString("wiley.orders.report.file.sheet2.name"));
		fillSheet(sheetAllOtherCurrencies, exportedProcessesAllOtherCurrencies, siteShortcut);


		return wb;


	}

	private boolean orderWithUSDCurrency(final WileyExportProcessModel exportedProcess)
	{
		final OrderModel order = exportedProcess.getOrder();
		if (order != null)
		{
			final CurrencyModel currency = order.getCurrency();
			return currency != null
					&& USD_CURRENCY_CODE.equals(currency.getIsocode());
		}
		return false;
	}

	private void fillSheet(final HSSFSheet sheet, final List<WileyExportProcessModel> exportedProcesses,
			final String siteShortcut)
	{

		if (CollectionUtils.isNotEmpty(exportedProcesses))
		{
			fillColumnNames(sheet, siteShortcut);
			HSSFCellStyle currencyCellStyleForSheet = createCurrencyCellStyle(sheet.getWorkbook());
			// filling the content form the exported orders
			int rowNumber = FIRST_ROW_WITH_DATA;

			LOG.debug(String.format("Go through all exportedProcesses list for site %s", siteShortcut));
			for (WileyExportProcessModel exportProcessModel : exportedProcesses)
			{

				ExcelReportOrder excelReportOrder = new ExcelReportOrder();

				excelReportOrderPopulator.populate(exportProcessModel, excelReportOrder);

				HSSFRow row = sheet.createRow(rowNumber);
				LOG.debug(String.format("Row in workbook is created %s for oder %s", rowNumber,
						excelReportOrder.getPurchaseOrderNumber()));
				fillRowFromOrderData(row, excelReportOrder, currencyCellStyleForSheet);

				rowNumber++;
			}
			autoSizeColumns(sheet);
		}

		else
		{
			setMessageNoOrdersForCurrency(sheet, siteShortcut);
		}
		LOG.debug(String.format("Completed creating excel workbook for site %s", siteShortcut));

	}

	private void setMessageNoOrdersForCurrency(final HSSFSheet sheet, final String siteShortcut)
	{
		fillColumnNames(sheet, siteShortcut);
		autoSizeColumns(sheet);
		HSSFRow firstRow = sheet.createRow(FIRST_ROW_WITH_DATA);
		HSSFCell cell = firstRow.createCell(FIRST_COLUMN_WITH_DATA);
		String sheetBody = Localization.getLocalizedString("wiley.orders.report.no.orders.sheet");
		cell.setCellValue(sheetBody);
	}



	private void fillRowFromOrderData(final HSSFRow row,
			final ExcelReportOrder excelReportOrder, final HSSFCellStyle cs)
	{


		final String purchaseOrderNumber = excelReportOrder.getPurchaseOrderNumber();
		LOG.debug(String.format("Starting writing Excel row from ExcelReportOrder for order %s",
				purchaseOrderNumber));
		int columnNumber = FIRST_COLUMN_WITH_DATA;
		createStringTypeCell(excelReportOrder.getProductId(), row, columnNumber++);

		LOG.debug(String.format("ProductId cell is created for order %s", purchaseOrderNumber));

		SimpleDateFormat sdf = new SimpleDateFormat(EXCEL_DATE_COLUMN_FORMATTER, Locale.US);

		createStringTypeCell(sdf.format(excelReportOrder.getTransactionDate()), row, columnNumber++);
		LOG.debug(String.format("Transaction Date cell is created for order %s", purchaseOrderNumber));

		createStringTypeCell(sdf.format(excelReportOrder.getCreationTime()), row, columnNumber++);
		LOG.debug(String.format("Order Creation Date cell is created for order %s", purchaseOrderNumber));

		createStringTypeCell(purchaseOrderNumber, row, columnNumber++);
		LOG.debug(String.format("PurchaseOrderNumber cell is created for order %s", purchaseOrderNumber));

		createStringTypeCell(excelReportOrder.getOrderType(), row, columnNumber++);
		LOG.debug(String.format("OrderType cell is created for order %s", purchaseOrderNumber));

		createStringTypeCell(excelReportOrder.getCreditCardType(), row, columnNumber++);
		LOG.debug(String.format("CreditCardType cell is created for order %s", purchaseOrderNumber));

		createStringTypeCell(excelReportOrder.getCreditCardOwner(), row, columnNumber++);
		LOG.debug(String.format("CreditCardOwner cell is created for order %s", purchaseOrderNumber));

		createStringTypeCell(excelReportOrder.getAuthCode(), row, columnNumber++);
		LOG.debug(String.format("AuthCode cell is created for order %s", purchaseOrderNumber));

		createStringTypeCell(excelReportOrder.getCreditCardToken(), row, columnNumber++);
		LOG.debug(String.format("CreditCardToken cell is created for order %s", purchaseOrderNumber));

		createStringTypeCell(excelReportOrder.getOrderCurrency(), row, columnNumber++);
		LOG.debug(String.format("Currency cell is created for order %s", purchaseOrderNumber));

		createCurrencyCell(excelReportOrder.getOrderAmount(), row, columnNumber++, cs);
		LOG.debug(String.format("OrderAmount cell is created for order %s", purchaseOrderNumber));

		createCurrencyCell(excelReportOrder.getTax(), row, columnNumber++, cs);
		LOG.debug(String.format("Tax cell is created for order %s", purchaseOrderNumber));

		createCurrencyCell(excelReportOrder.getShipping(), row, columnNumber++, cs);
		LOG.debug(String.format("Shipping cell is created for order %s", purchaseOrderNumber));

		createCurrencyCell(excelReportOrder.getTotalDiscounts(), row, columnNumber++, cs);
		LOG.debug(String.format("TotalDiscounts cell is created for order %s", purchaseOrderNumber));

		createCurrencyCell(excelReportOrder.getGrandTotal(), row, columnNumber++, cs);
		LOG.debug(String.format("GrandTotal cell is created for order %s", purchaseOrderNumber));
		LOG.debug(String.format("Completed writing Excel row from ExcelReportOrder for %s",
				purchaseOrderNumber));

	}

	private void fillColumnNames(final HSSFSheet sheet, final String siteShortcut)
	{
		// creating header of the file with column titles
		LOG.debug(String.format("Starting writing excel column names for site %s", siteShortcut));
		HSSFRow firstRow = sheet.createRow(0);
		int column = 0;
		for (String columnName : excelReportColumns)
		{
			HSSFCell cell = firstRow.createCell(column);
			cell.setCellType(HSSFCell.CELL_TYPE_STRING);
			final String localizedString = Localization.getLocalizedString(columnName);
			cell.setCellValue(localizedString);
			column++;
		}
		LOG.debug(String.format("Completed writing excel column names  for site %s", siteShortcut));
	}

	/**
	 * Uses autoSizeColumn, which is a time consuming operation.
	 * This process can be relatively slow on large sheets,
	 * so this should normally only be called once per column, at the end of processing.
	 *
	 * @param sheet
	 */
	private void autoSizeColumns(final HSSFSheet sheet)
	{
		LOG.debug("Starting auto size columns");
		for (int columnNumber = 0; columnNumber < excelReportColumns.size(); columnNumber++)
		{
			// time consuming operation
			sheet.autoSizeColumn(columnNumber);
			sheet.setColumnWidth(columnNumber, sheet.getColumnWidth(columnNumber) + COLUMN_WIDTH_ADJUSTMENT);
		}
		LOG.debug("Completed auto size columns");
	}



	private HSSFCell createCurrencyCell(final Double value, final HSSFRow row, final int columnNumber, final HSSFCellStyle cs)
	{

		HSSFCell currencyCell = row.createCell(columnNumber);
		currencyCell.setCellValue(value);
		currencyCell.setCellStyle(cs);
		return currencyCell;
	}

	private HSSFCellStyle createCurrencyCellStyle(final HSSFWorkbook workbook)
	{
		HSSFCellStyle cs = workbook.createCellStyle();
		HSSFDataFormat df = workbook.createDataFormat();
		cs.setDataFormat(df.getFormat("#,##0.00"));
		return cs;
	}



	private HSSFCell createStringTypeCell(final String value, final HSSFRow row, final int columnNumber)
	{
		HSSFCell grandTotalCell = row.createCell(columnNumber);
		grandTotalCell.setCellType(HSSFCell.CELL_TYPE_STRING);
		grandTotalCell.setCellValue(value);
		return grandTotalCell;
	}

}