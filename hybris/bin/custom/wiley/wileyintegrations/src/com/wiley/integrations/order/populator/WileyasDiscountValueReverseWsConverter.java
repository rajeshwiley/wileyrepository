package com.wiley.integrations.order.populator;

import com.wiley.integrations.order.dto.DiscountValueWsDTO;
import de.hybris.platform.converters.impl.AbstractPopulatingConverter;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.util.DiscountValue;
import org.apache.commons.lang.NotImplementedException;

/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public class WileyasDiscountValueReverseWsConverter extends AbstractPopulatingConverter<DiscountValueWsDTO, DiscountValue>
{
    @Override
    public DiscountValue convert(final DiscountValueWsDTO source) throws ConversionException
    {
        return new DiscountValue(source.getCode(), source.getValue(), source.getAbsolute(), source.getCurrency());
    }

    @Override
    protected DiscountValue createTarget()
    {
        throw new NotImplementedException();
    }
}
