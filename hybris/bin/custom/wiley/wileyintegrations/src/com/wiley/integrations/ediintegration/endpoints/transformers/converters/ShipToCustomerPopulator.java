package com.wiley.integrations.ediintegration.endpoints.transformers.converters;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.Map;

import javax.annotation.Resource;

import com.wiley.integrations.ediintegration.endpoints.jaxb.ShipToCustomer;


public class ShipToCustomerPopulator implements Truncatable, Populator<OrderModel, ShipToCustomer>
{
	@Resource(name = "addressFieldsLengthRestrictions")
	private Map<String, Integer> addressFieldsLengthRestrictions;

	@Override
	public void populate(final OrderModel orderModel, final ShipToCustomer shipToCustomer) throws ConversionException
	{
		final AddressModel address = orderModel.getDeliveryAddress() == null ?
				orderModel.getPaymentAddress() : orderModel.getDeliveryAddress();
		if (address == null)
		{
			return;
		}

		shipToCustomer.setAddressLine1(truncate(address.getLine1(), "addressLine1"));
		shipToCustomer.setAddressLine2(truncate(address.getLine2(), "addressLine2"));
		shipToCustomer.setCityName(truncate(address.getTown(), "cityName"));
		CountryModel country = address.getCountry();
		shipToCustomer.setCountry(country.getNumeric());
		shipToCustomer.setStateProvince(ConversionUtils.getStateProvinceFromAddress(address));
		shipToCustomer.setZipPostCode(truncate(address.getPostalcode(), "zipPostCode"));
		shipToCustomer.setCustomerName(truncate(address.getFirstname(), "customerName"));
		shipToCustomer.setCustomerName2(truncate(address.getLastname(), "customerName2"));
	}

	@Override
	public Map<String, Integer> getAddressFieldsLengthRestrictions()
	{
		return addressFieldsLengthRestrictions;
	}
}
