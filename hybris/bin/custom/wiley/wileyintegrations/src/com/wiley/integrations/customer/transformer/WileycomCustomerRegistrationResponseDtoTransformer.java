package com.wiley.integrations.customer.transformer;

import javax.annotation.Nonnull;

import org.springframework.http.HttpStatus;
import org.springframework.messaging.Message;
import org.springframework.util.Assert;

import com.wiley.integrations.common.dto.WileycomErrorResponseDto;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.integration.http.HttpHeaders.STATUS_CODE;

public class WileycomCustomerRegistrationResponseDtoTransformer
{
	public Boolean transform(@Nonnull final Message<WileycomErrorResponseDto> message)
	{
		Assert.notNull(message);
		return CREATED.value() == ((HttpStatus) message.getHeaders().get(STATUS_CODE)).value();
	}
}
