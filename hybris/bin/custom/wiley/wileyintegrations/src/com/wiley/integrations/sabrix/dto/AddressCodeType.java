package com.wiley.integrations.sabrix.dto;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AddressCodeType.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AddressCodeType"&gt;
 *   &lt;restriction base="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1}StringType"&gt;
 *     &lt;enumeration value="Primary"/&gt;
 *     &lt;enumeration value="Mailing"/&gt;
 *     &lt;enumeration value="Physical"/&gt;
 *     &lt;enumeration value="Billing"/&gt;
 *     &lt;enumeration value="Shipping"/&gt;
 *     &lt;enumeration value="Raw"/&gt;
 *     &lt;enumeration value="Other"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 */
@XmlType(name = "AddressCodeType", namespace = "http://xmlns.wiley.com/EnterpriseBusinessObjects/CommonInfratypes/Codelist/V1")
@XmlEnum
public enum AddressCodeType
{

	@XmlEnumValue("Primary")
	PRIMARY("Primary"),
	@XmlEnumValue("Mailing")
	MAILING("Mailing"),
	@XmlEnumValue("Physical")
	PHYSICAL("Physical"),
	@XmlEnumValue("Billing")
	BILLING("Billing"),
	@XmlEnumValue("Shipping")
	SHIPPING("Shipping"),
	@XmlEnumValue("Raw")
	RAW("Raw"),
	@XmlEnumValue("Other")
	OTHER("Other");
	private final String value;

	AddressCodeType(String v)
	{
		value = v;
	}

	public String value()
	{
		return value;
	}

	public static AddressCodeType fromValue(String v)
	{
		for (AddressCodeType c : AddressCodeType.values())
		{
			if (c.value.equals(v))
			{
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}

}
