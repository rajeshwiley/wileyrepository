package com.wiley.integrations.esb.converters.populator.orderentry;

import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import com.wiley.core.model.ExternalInventoryModel;
import com.wiley.integrations.esb.dto.verifyandcalculatecart.InventoryStatus;
import com.wiley.integrations.esb.strategies.InventoryStatusToStockLevelStatusStrategy;

import org.springframework.util.Assert;


/**
 * Populates {@link ExternalInventoryModel}
 */
public class InventoryStatusToExternalInventoryPopulator implements Populator<InventoryStatus, ExternalInventoryModel>
{
	@Resource
	private InventoryStatusToStockLevelStatusStrategy inventoryStatusToStockLevelStatusStrategy;

	@Override
	public void populate(@Nonnull final InventoryStatus source, @Nonnull final ExternalInventoryModel target)
			throws ConversionException
	{
		Assert.notNull(source);
		Assert.notNull(target);

		target.setQuantity(source.getQuantity().longValue());
		final StockLevelStatus externalInventoryStatus = inventoryStatusToStockLevelStatusStrategy.getExternalInventoryStatus(
				source.getStatus());
		target.setStatus(externalInventoryStatus);
		target.setEstimatedDeliveryDate(source.getEstimatedDeliveryDate());
	}
}
