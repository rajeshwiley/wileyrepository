package com.wiley.integrations.esb.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.common.base.MoreObjects;


@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EsbB2CChangeSubscriptionMethodRequestDto
{

	@XmlElement(required = true)
	private String paymentType;

	private String paymentToken;

	private EsbAddressCreateRequestDto paymentAddress;

	public String getPaymentType()
	{
		return paymentType;
	}

	public void setPaymentType(final String paymentType)
	{
		this.paymentType = paymentType;
	}

	public String getPaymentToken()
	{
		return paymentToken;
	}

	public void setPaymentToken(final String paymentToken)
	{
		this.paymentToken = paymentToken;
	}

	public EsbAddressCreateRequestDto getPaymentAddress()
	{
		return paymentAddress;
	}

	public void setPaymentAddress(final EsbAddressCreateRequestDto paymentAddress)
	{
		this.paymentAddress = paymentAddress;
	}

	@Override
	public String toString()
	{
		return MoreObjects.toStringHelper(this)
				.add("paymentType", paymentType)
				.add("paymentToken", paymentToken)
				.add("paymentAddress", paymentAddress)
				.toString();
	}
}
