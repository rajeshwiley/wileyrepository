package com.wiley.integrations.esb.subscription.dto;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * The auto-renew flag for the subscription.
 */
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-09-19T12:49:23.788Z")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RenewableUpdateRequestSwgDTO
{
	/**
	 * Gets or Sets autorenew
	 */
	public enum AutorenewEnum
	{

		ENABLED("ENABLED"), DISABLED("DISABLED");

		private final String value;

		AutorenewEnum(final String value)
		{
			this.value = value;
		}

		@Override
		public String toString()
		{
			return String.valueOf(value);
		}
	}

	private AutorenewEnum autorenew = null;

	public RenewableUpdateRequestSwgDTO autorenew(final AutorenewEnum autorenew)
	{
		this.autorenew = autorenew;
		return this;
	}

	/**
	 * Autorenew Enum
	 *
	 * @return autorenew
	 **/
	@JsonProperty("autorenew")
	public AutorenewEnum getAutorenew()
	{
		return autorenew;
	}

	public void setAutorenew(final AutorenewEnum autorenew)
	{
		this.autorenew = autorenew;
	}


	@Override
	public boolean equals(final java.lang.Object object)
	{
		if (this == object)
		{
			return true;
		}
		if (object == null || getClass() != object.getClass())
		{
			return false;
		}
		final RenewableUpdateRequestSwgDTO renewableUpdateRequestSwgDTO = (RenewableUpdateRequestSwgDTO) object;
		return Objects.equals(this.autorenew, renewableUpdateRequestSwgDTO.autorenew);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(autorenew);
	}

	@Override
	public String toString()
	{
		final StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("class RenewableUpdateRequest {\n");

		stringBuilder.append("    autorenew: ").append(toIndentedString(autorenew)).append("\n");
		stringBuilder.append("}");
		return stringBuilder.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces (except the first line).
	 */
	private String toIndentedString(final java.lang.Object object)
	{
		if (object == null)
		{
			return "null";
		}
		return object.toString().replace("\n", "\n    ");
	}
}

