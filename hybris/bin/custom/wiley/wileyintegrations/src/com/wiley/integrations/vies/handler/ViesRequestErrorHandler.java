package com.wiley.integrations.vies.handler;

import org.springframework.messaging.support.ErrorMessage;


public class ViesRequestErrorHandler
{
    public ErrorMessage handleFailedRequest(final ErrorMessage errorMessage)
    {
        return errorMessage;
    }

}
