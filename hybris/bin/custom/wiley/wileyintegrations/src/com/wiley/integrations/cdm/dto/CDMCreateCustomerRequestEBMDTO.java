/**
 *
 */
package com.wiley.integrations.cdm.dto;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;



@JsonInclude(JsonInclude.Include.NON_NULL)
public class CDMCreateCustomerRequestEBMDTO {

  @JsonProperty(value = "DateTime")
  private String dateTime;
  @JsonProperty(value = "ActionCode")
  private String actionCode;
  @JsonProperty(value = "CustomerEBM")
  private CDMCustomerEBMDTO customerEBM;

  /**
   * @return the dateTime
   */
  public String getDateTime() {
    return dateTime;
  }

  /**
   * @param dateTime
   *          the dateTime to set
   */
  public void setDateTime(final String dateTime) {
    this.dateTime = dateTime;
  }

  /**
   * @return the actionCode
   */
  public String getActionCode() {
    return actionCode;
  }

  /**
   * @param actionCode
   *          the actionCode to set
   */
  public void setActionCode(final String actionCode) {
    this.actionCode = actionCode;
  }

  /**
   * @return the customerEBM
   */
  public CDMCustomerEBMDTO getCustomerEBM() {
    return customerEBM;
  }

  /**
   * @param customerEBM
   *          the customerEBM to set
   */
  public void setCustomerEBM(final CDMCustomerEBMDTO customerEBM) {
    this.customerEBM = customerEBM;
  }


  @Override
  public String toString() {
    return "CreateCustomerRequestEBM [DateTime=" + dateTime + ", ActionCode=" + actionCode
        + ", customerEBM=" + customerEBM + "]";
  }



}
