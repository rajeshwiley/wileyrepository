package com.wiley.integrations.address.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.annotation.Nonnull;

import org.springframework.util.Assert;

import com.wiley.core.integration.address.dto.AddressDto;
import com.wiley.integrations.address.dto.AddressValidationResultDto;



public class AddressValidationResultDataPopulator implements Populator<AddressValidationResultDto, AddressDto>
{
	@Override
	public void populate(@Nonnull final AddressValidationResultDto source, @Nonnull final AddressDto target)
			throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setTown(source.getCity());
		target.setLine1(source.getAddressLine1());
		target.setLine2(source.getAddressLine2());

		target.setRegionIso2(source.getRegion());
		target.setCountryIso2(source.getCountry());

		target.setPostalCode(source.getPostalCode());
		target.setRelevance(source.getResultPercentage());
		target.setDecision(source.getMatchCode());
	}
}
