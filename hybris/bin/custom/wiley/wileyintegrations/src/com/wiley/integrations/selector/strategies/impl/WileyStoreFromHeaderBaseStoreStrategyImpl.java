package com.wiley.integrations.selector.strategies.impl;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import javax.annotation.Nonnull;

import org.springframework.messaging.Message;

import com.wiley.integrations.selector.strategies.WileyStoreSelectorStrategy;


/**
 * Created by Uladzimir_Barouski on 1/17/2017.
 */
public class WileyStoreFromHeaderBaseStoreStrategyImpl implements WileyStoreSelectorStrategy
{
	public static final String HEADER_STORE_KEY = "baseStore";

	@Nonnull
	@Override
	public Optional<BaseStoreModel> getCurrentBaseStore(@Nonnull final Message<?> message)
	{
		BaseStoreModel currentBaseStore = (BaseStoreModel) message.getHeaders().get(HEADER_STORE_KEY);
		return Optional.ofNullable(currentBaseStore);
	}
}
