package com.wiley.integrations.esb.dto.verifyandcalculatecart;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wiley.integrations.esb.dto.common.Discount;

import org.apache.commons.lang3.builder.ToStringBuilder;


/**
 * An order entry - an order line for a single product.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CartCalculationEntryResponse
{

	private String sapProductCode;

	private String isbn;

	/**
	 * Quantity of the product in the order entry. Product pieces are considered as default measurement units.
	 * minimum: 1.0
	 **/
	private Integer quantity;

	/**
	 * List of inventory statuses for entry.
	 */
	private List<InventoryStatus> inventoryStatuses;

	/**
	 * Price of the single product item. The en_US format is used for the value.
	 **/
	private Double basePrice;

	/**
	 * Arrays of applied local (entry level) discounts
	 */
	private List<Discount> discounts = new ArrayList<>();

	/**
	 * Price of all the product items in the entity (quantity x basePrice). The en_US format is used for the value.
	 **/
	private Double totalPrice;

	@JsonProperty("sapProductCode")
	public String getSapProductCode()
	{
		return sapProductCode;
	}

	public void setSapProductCode(final String sapProductCode)
	{
		this.sapProductCode = sapProductCode;
	}

	@JsonProperty("isbn")
	public String getIsbn()
	{
		return isbn;
	}

	public void setIsbn(final String isbn)
	{
		this.isbn = isbn;
	}

	@JsonProperty("quantity")
	public Integer getQuantity()
	{
		return quantity;
	}

	public void setQuantity(final Integer quantity)
	{
		this.quantity = quantity;
	}

	@JsonProperty("inventoryStatuses")
	public List<InventoryStatus> getInventoryStatuses()
	{
		return inventoryStatuses;
	}

	public void setInventoryStatuses(
			final List<InventoryStatus> inventoryStatuses)
	{
		this.inventoryStatuses = inventoryStatuses;
	}

	@JsonProperty("basePrice")
	public Double getBasePrice()
	{
		return basePrice;
	}

	public void setBasePrice(final Double basePrice)
	{
		this.basePrice = basePrice;
	}

	@JsonProperty("discounts")
	public List<Discount> getDiscounts()
	{
		return discounts;
	}

	public void setDiscounts(final List<Discount> discounts)
	{
		this.discounts = discounts;
	}

	@JsonProperty("totalPrice")
	public Double getTotalPrice()
	{
		return totalPrice;
	}

	public void setTotalPrice(final Double totalPrice)
	{
		this.totalPrice = totalPrice;
	}

	@Override
	public String toString()
	{
		return new ToStringBuilder(this)
				.append("sapProductCode", sapProductCode)
				.append("isbn", isbn)
				.append("quantity", quantity)
				.append("inventoryStatuses", inventoryStatuses)
				.append("basePrice", basePrice)
				.append("discounts", discounts)
				.append("totalPrice", totalPrice)
				.toString();
	}
}

