package com.wiley.integrations.eloqua.transformer;

import de.hybris.platform.core.model.user.CustomerModel;

import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Used to prepare data for sending to Eloqua.
 *
 * @author Aliaksei_Zlobich
 */
public class SearchForContactRequestTransformer
{

	private static final Logger LOG = LoggerFactory.getLogger(SearchForContactRequestTransformer.class);

	/**
	 * Transforms CustomerModel to uid.
	 *
	 * @param customer
	 * 		the customer
	 * @return uid
	 */
	@Nonnull
	public String transform(@Nonnull final CustomerModel customer)
	{
		LOG.debug("Method params: customer [{}].", customer.getUid());
		return customer.getUid();
	}

}
