package com.wiley.integrations.esb.subscription.dto;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Description of a server error.
 */
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-09-19T12:49:23.788Z")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorSwgDTO
{
	private String code = null;
	private String message = null;

	public ErrorSwgDTO code(final String code)
	{
		this.code = code;
		return this;
	}

	/**
	 * Error code.
	 *
	 * @return code
	 **/
	@JsonProperty("code")
	public String getCode()
	{
		return code;
	}

	public void setCode(final String code)
	{
		this.code = code;
	}

	public ErrorSwgDTO message(final String message)
	{
		this.message = message;
		return this;
	}

	/**
	 * Error description.
	 *
	 * @return message
	 **/
	@JsonProperty("message")
	public String getMessage()
	{
		return message;
	}

	public void setMessage(final String message)
	{
		this.message = message;
	}


	@Override
	public boolean equals(final java.lang.Object object)
	{
		if (this == object)
		{
			return true;
		}
		if (object == null || getClass() != object.getClass())
		{
			return false;
		}
		final ErrorSwgDTO errorSwgDTO = (ErrorSwgDTO) object;
		return Objects.equals(this.code, errorSwgDTO.code) && Objects.equals(this.message, errorSwgDTO.message);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(code, message);
	}

	@Override
	public String toString()
	{
		final StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("class Error {\n");

		stringBuilder.append("    code: ").append(toIndentedString(code)).append("\n");
		stringBuilder.append("    message: ").append(toIndentedString(message)).append("\n");
		stringBuilder.append("}");
		return stringBuilder.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces (except the first line).
	 */
	private String toIndentedString(final java.lang.Object object)
	{
		if (object == null)
		{
			return "null";
		}
		return object.toString().replace("\n", "\n    ");
	}
}

