package com.wiley.integrations.bundles;

import de.hybris.platform.acceleratorservices.dataimport.batch.BatchHeader;
import de.hybris.platform.acceleratorservices.dataimport.batch.task.CleanupTask;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.util.Assert;

import com.wiley.core.model.WileyBundleModel;
import com.wiley.core.wileybundle.dao.WileyBundleDao;


public class WileyBundlesCleanupTask extends CleanupTask
{
	private WileyBundleDao wileyBundleDao;
	private ModelService modelService;

	/**
	 * remove all bundles with no sequenceId or sequenceId less than the one given in filename
	 *
	 * @param header
	 * @return
	 */
	@Override
	public BatchHeader execute(final BatchHeader header)
	{
		removeObsoleteBundles(header);
		return super.execute(header);
	}

	private void removeObsoleteBundles(final BatchHeader header)
	{
		final Long sequenceId = header.getSequenceId();
		Assert.notNull(sequenceId);
		final List<WileyBundleModel> obsoleteBundles = getWileyBundleDao().getObsoleteBundles(sequenceId.toString());
		if (CollectionUtils.isNotEmpty(obsoleteBundles))
		{
			getModelService().removeAll(obsoleteBundles);
		}
	}

	public WileyBundleDao getWileyBundleDao()
	{
		return wileyBundleDao;
	}

	public void setWileyBundleDao(final WileyBundleDao wileyBundleDao)
	{
		this.wileyBundleDao = wileyBundleDao;
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}
}
