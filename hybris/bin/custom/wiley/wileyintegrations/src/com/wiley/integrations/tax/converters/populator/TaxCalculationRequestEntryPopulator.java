package com.wiley.integrations.tax.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.math.BigDecimal;

import com.wiley.core.externaltax.dto.TaxCalculationRequestEntryDto;
import com.wiley.integrations.tax.dto.TaxCalculationRequestEntry;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


public class TaxCalculationRequestEntryPopulator implements Populator<TaxCalculationRequestEntryDto, TaxCalculationRequestEntry>
{
	@Override
	public void populate(final TaxCalculationRequestEntryDto source, final TaxCalculationRequestEntry target)
			throws ConversionException
	{
		validateParameterNotNull(source, "source mustn't be null");
		validateParameterNotNull(target, "target mustn't be null");

		target.setId(source.getId());
		target.setAmount(BigDecimal.valueOf(source.getAmount()));
		target.setCodeType(source.getCodeType());
		target.setCode(source.getCode());
		target.setProductType(source.getProductType());
	}
}
