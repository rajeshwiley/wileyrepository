package com.wiley.integrations.esb.converters.populator.orderentry;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.subscriptionservices.model.SubscriptionTermModel;

import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.wiley.core.enums.ProductEditionFormat;
import com.wiley.integrations.esb.dto.common.AbstractOrderEntryRequest;
import com.wiley.integrations.esb.dto.order.OrderEntryRequest;


/**
 * Populates simple fields of {@link OrderEntryRequest} from {@link OrderEntryModel}
 */
public class OrderEntryModelToOrderEntryRequestSimpleFieldsPopulator implements Populator<OrderEntryModel, OrderEntryRequest>
{

	@Resource
	private Populator<AbstractOrderEntryModel, AbstractOrderEntryRequest> abstractOrderEntryRequestPopulator;

	@Override
	public void populate(final OrderEntryModel orderEntryModel, final OrderEntryRequest orderEntryRequest)
			throws ConversionException
	{
		Assert.notNull(orderEntryModel);
		Assert.notNull(orderEntryRequest);

		abstractOrderEntryRequestPopulator.populate(orderEntryModel, orderEntryRequest);

		orderEntryRequest.setBasePrice(orderEntryModel.getBasePrice());
		orderEntryRequest.setTotalPrice(orderEntryModel.getTotalPrice());

		populateDigitalContentType(orderEntryModel, orderEntryRequest);

		populateSubscriptionTerm(orderEntryModel, orderEntryRequest);
	}

	private void populateDigitalContentType(final OrderEntryModel orderEntryModel, final OrderEntryRequest orderEntryRequest)
	{
		final ProductModel product = orderEntryModel.getProduct();
		if (ProductEditionFormat.DIGITAL.equals(product.getEditionFormat()))
		{
			orderEntryRequest.setDigitalContentType(product.getDigitalContentType());
		}
	}

	private void populateSubscriptionTerm(final OrderEntryModel orderEntryModel, final OrderEntryRequest orderEntryRequest)
	{
		final SubscriptionTermModel subscriptionTerm = orderEntryModel.getSubscriptionTerm();
		if (subscriptionTerm != null)
		{
			orderEntryRequest.setSubscriptionTerm(subscriptionTerm.getId());
		}
	}
}
