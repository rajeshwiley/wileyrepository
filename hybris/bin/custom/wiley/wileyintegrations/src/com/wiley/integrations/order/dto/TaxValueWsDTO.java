package com.wiley.integrations.order.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonInclude(JsonInclude.Include.NON_NULL)
public class TaxValueWsDTO implements Serializable
{
	@Size(max = 255)
	private String code;

	@NotNull
	@Min(0)
	private Double value;

	@NotNull
	@Pattern(regexp = "^[A-Z]{3}$")
	private String currency;
	private Map<String, Object> anyProperties = new HashMap<>();

	/**
	 * Qualifier of the tax value. A constant will be used for the taxes calculated using Vertex.
	 */

	@JsonProperty("code")
	public String getCode()
	{
		return code;
	}

	public void setCode(final String code)
	{
		this.code = code;
	}

	/**
	 * Value of the tax. Taxes are always absolute. The en_US format is used for the value.
	 */

	@JsonProperty("value")
	public Double getValue()
	{
		return value;
	}

	public void setValue(final Double value)
	{
		this.value = value;
	}

	/**
	 * The currency of the tax value. Three-letter ISO 4217 code.
	 */

	@JsonProperty("currency")
	public String getCurrency()
	{
		return currency;
	}

	public void setCurrency(final String currency)
	{
		this.currency = currency;
	}

	@JsonAnyGetter
	public Map<String, Object> getAnyProperties()
	{
		return anyProperties;
	}

	@JsonAnySetter
	public void setAnyProperties(final String name, final Object value)
	{
		this.anyProperties.put(name, value);
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		TaxValueWsDTO taxValue = (TaxValueWsDTO) o;
		return Objects.equals(code, taxValue.code)
				&& Objects.equals(value, taxValue.value)
				&& Objects.equals(currency, taxValue.currency)
				&& Objects.equals(anyProperties, taxValue.anyProperties);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(code, value, currency, anyProperties);
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("class DiscountValue {\n");

		sb.append("    code: ").append(toIndentedString(code)).append("\n");
		sb.append("    value: ").append(toIndentedString(value)).append("\n");
		sb.append("    currency: ").append(toIndentedString(currency)).append("\n");
		sb.append("    anyProperties: ").append(toIndentedString(anyProperties)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(final Object o)
	{
		if (o == null)
		{
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
