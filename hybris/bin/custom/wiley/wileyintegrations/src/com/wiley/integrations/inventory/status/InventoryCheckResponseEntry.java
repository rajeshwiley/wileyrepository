package com.wiley.integrations.inventory.status;

import java.util.List;

import com.wiley.integrations.esb.dto.verifyandcalculatecart.InventoryStatus;


/**
 * Created by Mikhail_Asadchy on 7/27/2016.
 */
public class InventoryCheckResponseEntry
{
	private String sapProductCode;

	private String isbn;

	private List<InventoryStatus> statuses;

	public String getSapProductCode()
	{
		return sapProductCode;
	}

	public void setSapProductCode(final String sapProductCode)
	{
		this.sapProductCode = sapProductCode;
	}

	public String getIsbn()
	{
		return isbn;
	}

	public void setIsbn(final String isbn)
	{
		this.isbn = isbn;
	}

	public List<InventoryStatus> getStatuses()
	{
		return statuses;
	}

	public void setStatuses(final List<InventoryStatus> statuses)
	{
		this.statuses = statuses;
	}
}
