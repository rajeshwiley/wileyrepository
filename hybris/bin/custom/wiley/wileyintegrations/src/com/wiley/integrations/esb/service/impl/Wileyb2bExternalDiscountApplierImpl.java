package com.wiley.integrations.esb.service.impl;

import de.hybris.platform.core.CoreAlgorithms;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.util.DiscountValue;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.wiley.integrations.esb.dto.common.Discount;
import com.wiley.integrations.esb.service.Wileyb2bExternalDiscountApplier;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.Assert;


/**
 * Default implementation of {@link Wileyb2bExternalDiscountApplier}
 */
public class Wileyb2bExternalDiscountApplierImpl implements Wileyb2bExternalDiscountApplier
{
	@Override
	public void applyDiscounts(@Nonnull final CartModel cartModel, @Nullable final List<Discount> externalDiscounts)
	{
		Assert.notNull(cartModel);

		final CurrencyModel currency = cartModel.getCurrency();
		Assert.notNull(currency);

		if (CollectionUtils.isNotEmpty(externalDiscounts))
		{
			List<DiscountValue> discountValueList = externalDiscounts.stream()
					.map(discount -> createDiscountValue(cartModel, currency, discount))
					.collect(Collectors.toList());

			cartModel.setGlobalDiscountValues(discountValueList);
		}
		else
		{
			cartModel.setGlobalDiscountValues(Collections.emptyList());
		}
	}

	@Override
	public void applyDiscounts(@Nonnull final CartEntryModel cartEntryModel, @Nullable final List<Discount> externalDiscounts)
	{
		Assert.notNull(cartEntryModel);

		final CartModel cartModel = cartEntryModel.getOrder();
		Assert.notNull(cartModel);
		final CurrencyModel currency = cartModel.getCurrency();
		Assert.notNull(currency);

		if (CollectionUtils.isNotEmpty(externalDiscounts))
		{
			List<DiscountValue> discountValueList = externalDiscounts.stream()
					.map(discount -> createDiscountValue(cartModel, currency, discount))
					.collect(Collectors.toList());

			final double totalPriceWithoutDiscount =
					CoreAlgorithms.round(cartEntryModel.getBasePrice() * cartEntryModel.getQuantity(), currency.getDigits());

			cartEntryModel.setDiscountValues(discountValueList);
		}
		else
		{
			cartEntryModel.setDiscountValues(Collections.emptyList());
		}
	}

	private DiscountValue createDiscountValue(final CartModel cartModel, final CurrencyModel currency, final Discount discount)
	{
		String code = StringUtils.isNotEmpty(discount.getName()) ? discount.getName() : generateCode("DISC", cartModel);
		boolean absolute = discount.getAbsolute() != null ? discount.getAbsolute() : true;
		double value = discount.getValue() != null ? discount.getValue() : 0.0;

		return new DiscountValue(code, value, absolute, currency.getIsocode());
	}

	private String generateCode(final String prefix, final CartModel cart)
	{
		return prefix + cart.getCode();
	}
}
