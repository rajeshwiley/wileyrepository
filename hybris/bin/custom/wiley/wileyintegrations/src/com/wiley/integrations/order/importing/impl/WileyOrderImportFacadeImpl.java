package com.wiley.integrations.order.importing.impl;

import com.wiley.core.order.WileyOrderFulfilmentProcessService;
import com.wiley.core.order.service.WileyOrderService;
import com.wiley.core.store.WileyBaseStoreService;
import com.wiley.integrations.order.dto.CreateOrderRequestWsDTO;
import com.wiley.integrations.order.dto.OrderWsDTO;
import com.wiley.integrations.order.importing.WileyOrderImportFacade;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.BaseStoreModel;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nonnull;
import javax.annotation.Resource;
import java.util.Optional;


public class WileyOrderImportFacadeImpl implements WileyOrderImportFacade
{

	@Resource
	private WileyOrderService wileyOrderService;

	@Resource
	private Converter<OrderModel, OrderWsDTO> wileyOrderWsConverter;

	@Resource
	private Converter<CreateOrderRequestWsDTO, OrderModel> wileyasOrderReverseWsConverter;

	@Resource
	private ModelService modelService;

	@Resource
	private BaseSiteService baseSiteService;

	@Resource
	private WileyBaseStoreService wileyBaseStoreService;

	@Resource
	private WileyOrderFulfilmentProcessService wileyOrderFulfilmentProcessService;

	@Override
	@Transactional
	public Pair<OrderWsDTO, OrderProcessModel> importOrderAndCreateFulfilment(@Nonnull final CreateOrderRequestWsDTO order,
														   @Nonnull final String baseSiteId)
	{
		final OrderModel orderModel = importOrderForSite(order, baseSiteId);
		final OrderProcessModel orderProcessModel =
				wileyOrderFulfilmentProcessService.startFulfilmentProcessForOrder(orderModel, false);
		final OrderWsDTO dto = wileyOrderWsConverter.convert(orderModel);
		return Pair.of(dto, orderProcessModel);
	}

	@Override
	@Transactional
	public OrderWsDTO importOrder(@Nonnull final CreateOrderRequestWsDTO request, @Nonnull final String baseSiteId)
	{
		final OrderModel orderModel = importOrderForSite(request, baseSiteId);
		return  wileyOrderWsConverter.convert(orderModel);
	}

	private OrderModel importOrderForSite(@Nonnull final CreateOrderRequestWsDTO order, @Nonnull final String baseSiteId)
	{
		final BaseSiteModel baseSite = baseSiteService.getBaseSiteForUID(baseSiteId);

		if (StringUtils.isNotBlank(order.getId()) && wileyOrderService.isOrderExistsForGuid(order.getId(), baseSite))
		{
			throw new AmbiguousIdentifierException("Order with id '" + order.getId() + "' was found!");
		}
		final OrderModel orderModel = wileyasOrderReverseWsConverter.convert(order);
		orderModel.setSite(baseSite);
		final Optional<BaseStoreModel> baseStore = wileyBaseStoreService.getBaseStoreForBaseSite(baseSite);
		baseStore.ifPresent(orderModel::setStore);
		modelService.save(orderModel);
		orderModel.setCalculated(true);
		modelService.save(orderModel);
		return orderModel;
	}
}
