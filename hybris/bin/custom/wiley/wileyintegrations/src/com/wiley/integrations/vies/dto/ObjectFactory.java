
package com.wiley.integrations.vies.dto;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.wiley.integrations.vies.dto package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private static final QName CHECK_VAT_RESPONSE_ADDRESS_QNAME =
            new QName("urn:ec.europa.eu:taxud:vies:services:checkVat:types", "address");
    private static final QName CHECK_VAT_RESPONSE_NAME_QNAME =
            new QName("urn:ec.europa.eu:taxud:vies:services:checkVat:types", "name");
    private static final QName CHECK_VAT_APPROX_RESPONSE_TRADER_COMPANY_TYPE_QNAME =
            new QName("urn:ec.europa.eu:taxud:vies:services:checkVat:types", "traderCompanyType");
    private static final QName CHECK_VAT_APPROX_RESPONSE_TRADER_NAME_QNAME =
            new QName("urn:ec.europa.eu:taxud:vies:services:checkVat:types", "traderName");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package:
     * com.wiley.integrations.vies.dto
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CheckVatResponse }
     * 
     */
    public CheckVatResponse createCheckVatResponse() {
        return new CheckVatResponse();
    }

    /**
     * Create an instance of {@link CheckVatApproxResponse }
     * 
     */
    public CheckVatApproxResponse createCheckVatApproxResponse() {
        return new CheckVatApproxResponse();
    }

    /**
     * Create an instance of {@link CheckVat }
     * 
     */
    public CheckVat createCheckVat() {
        return new CheckVat();
    }

    /**
     * Create an instance of {@link CheckVatApprox }
     * 
     */
    public CheckVatApprox createCheckVatApprox() {
        return new CheckVatApprox();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:ec.europa.eu:taxud:vies:services:checkVat:types", name = "address",
            scope = CheckVatResponse.class)
    public JAXBElement<String> createCheckVatResponseAddress(final String value) {
        return new JAXBElement<String>(CHECK_VAT_RESPONSE_ADDRESS_QNAME, String.class, CheckVatResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:ec.europa.eu:taxud:vies:services:checkVat:types", name = "name",
            scope = CheckVatResponse.class)
    public JAXBElement<String> createCheckVatResponseName(final String value) {
        return new JAXBElement<String>(CHECK_VAT_RESPONSE_NAME_QNAME, String.class, CheckVatResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:ec.europa.eu:taxud:vies:services:checkVat:types", name = "traderCompanyType",
            scope = CheckVatApproxResponse.class)
    public JAXBElement<String> createCheckVatApproxResponseTraderCompanyType(final String value) {
        return new JAXBElement<String>(CHECK_VAT_APPROX_RESPONSE_TRADER_COMPANY_TYPE_QNAME, String.class,
                CheckVatApproxResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:ec.europa.eu:taxud:vies:services:checkVat:types", name = "traderName",
            scope = CheckVatApproxResponse.class)
    public JAXBElement<String> createCheckVatApproxResponseTraderName(final String value) {
        return new JAXBElement<String>(CHECK_VAT_APPROX_RESPONSE_TRADER_NAME_QNAME, String.class,
                CheckVatApproxResponse.class, value);
    }

}
