package com.wiley.integrations.esb.converters.populator.deliveryoption;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.wiley.core.wileycom.ship.from.WileycomShipFromResolvingStrategy;
import com.wiley.integrations.esb.dto.delivery.DeliveryAddressDto;


public class CartEntryToDeliveryAddressDtoPopulator implements Populator<AbstractOrderEntryModel, DeliveryAddressDto>
{
	@Resource
	private AddressToDeliveryAddressDtoPopulator addressToDeliveryAddressDtoPopulator;

	@Resource
	private WileycomShipFromResolvingStrategy wileycomShipFromResolvingStrategy;
	
	
	@Override
	public void populate(final AbstractOrderEntryModel cartEntryModel, final DeliveryAddressDto deliveryAddressDto)
			throws ConversionException
	{
		Assert.notNull(cartEntryModel);
		Assert.notNull(deliveryAddressDto);

		final AddressModel deliveryAddress = wileycomShipFromResolvingStrategy
				.resolveShipFromAddress(cartEntryModel.getProduct());
		if (deliveryAddress != null)
		{
			addressToDeliveryAddressDtoPopulator.populate(deliveryAddress, deliveryAddressDto);
		}
	}
}
