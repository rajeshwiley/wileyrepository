package com.wiley.integrations.esb.converters.populator.order;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.springframework.util.Assert;

import com.wiley.integrations.esb.dto.EsbProductPriceRequestDto;


/**
 * Created by Georgii_Gavrysh on 6/30/2016.
 */
public class B2bUnitToProductDtoPopulator implements Populator<B2BUnitModel, EsbProductPriceRequestDto>
{
	@Override
	public void populate(final B2BUnitModel b2BUnitModel, final EsbProductPriceRequestDto esbProductPriceRequestDto)
			throws ConversionException
	{
		Assert.notNull(b2BUnitModel);
		Assert.notNull(esbProductPriceRequestDto);
		esbProductPriceRequestDto.setSapSalesArea(b2BUnitModel.getSapSalesArea());
		esbProductPriceRequestDto.setSapAccountNumber(b2BUnitModel.getSapAccountNumber());
	}
}
