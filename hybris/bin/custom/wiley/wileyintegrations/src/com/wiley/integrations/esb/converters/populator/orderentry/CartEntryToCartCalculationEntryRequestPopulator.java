package com.wiley.integrations.esb.converters.populator.orderentry;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import com.wiley.integrations.esb.dto.OrderEntryRequestDto;
import com.wiley.integrations.esb.dto.common.AbstractOrderEntryRequest;
import com.wiley.integrations.esb.dto.verifyandcalculatecart.CartCalculationEntryRequest;

import org.springframework.util.Assert;


/**
 * Populates {@link OrderEntryRequestDto}.
 */
public class CartEntryToCartCalculationEntryRequestPopulator implements Populator<CartEntryModel, CartCalculationEntryRequest>
{

	@Resource
	private Populator<AbstractOrderEntryModel, AbstractOrderEntryRequest> abstractOrderEntryRequestPopulator;

	@Override
	public void populate(@Nonnull final CartEntryModel cartEntryModel, @Nonnull final CartCalculationEntryRequest orderEntryDto)
			throws ConversionException
	{
		Assert.notNull(cartEntryModel);
		Assert.notNull(orderEntryDto);

		final ProductModel product = cartEntryModel.getProduct();
		Assert.notNull(product);

		abstractOrderEntryRequestPopulator.populate(cartEntryModel, orderEntryDto);
	}
}
