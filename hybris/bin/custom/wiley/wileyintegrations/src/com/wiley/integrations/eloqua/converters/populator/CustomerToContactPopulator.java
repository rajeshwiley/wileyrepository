package com.wiley.integrations.eloqua.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;

import org.springframework.util.Assert;

import com.wiley.core.integration.eloqua.dto.ContactDto;
import com.wiley.core.integration.eloqua.dto.FieldValueDto;
import com.wiley.integrations.eloqua.util.CustomObjectDataUtils;


/**
 * Populates customer data in Contact.
 *
 * @author Aliaksei_Zlobich
 */
public class CustomerToContactPopulator implements Populator<CustomerModel, ContactDto>
{

	/**
	 * Id of FieldValue in Eloqua. Lead source most recent.
	 */
	public static final int LEAD_SOURCE_MOST_RECENT_FIELD_ID = 100044;

	/**
	 * The value of the lead source most recent field should be equal 'Hybris' if contact is sent from hybris.
	 */
	public static final String LEAD_SOURCE_MOST_RECENT_FIELD_HYBRIS_VALUE = "Hybris";

	@Override
	public void populate(@Nonnull final CustomerModel customer, @Nonnull final ContactDto contact) throws ConversionException
	{
		Assert.notNull(customer);
		Assert.notNull(contact);

		contact.setEmailAddress(customer.getContactEmail());
		contact.setFirstName(customer.getFirstName());
		contact.setLastName(customer.getLastName());


		List<FieldValueDto> fieldValues = new ArrayList<>();

		/*
		As described in IDD we need to populate Contact with specific FieldValue
		which indicates that Contact is sent from hybris.
		 */
		fieldValues.add(
				CustomObjectDataUtils.createFieldValue(LEAD_SOURCE_MOST_RECENT_FIELD_ID,
						LEAD_SOURCE_MOST_RECENT_FIELD_HYBRIS_VALUE));

		contact.setFieldValues(fieldValues);
	}
}
