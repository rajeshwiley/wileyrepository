package com.wiley.integrations.wpg.transformer;

import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.dto.TransactionStatusDetails;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wiley.core.payment.enums.WileyTransactionStatusEnum;
import com.wiley.core.payment.response.WileyRefundResult;
import com.wiley.integrations.wpg.dto.TokenRefundPaymentResponse;


public class FollowOnRefundResponseTransformer
{
	private static final Logger LOG = LoggerFactory.getLogger(FollowOnRefundResponseTransformer.class);

	public WileyRefundResult transform(final TokenRefundPaymentResponse tokenRefundPaymentResponse)
	{
		final WileyRefundResult refundResult = new WileyRefundResult();
		final WileyTransactionStatusEnum transactionStatus = getWileyTransactionStatus(tokenRefundPaymentResponse);
		if (WileyTransactionStatusEnum.SUCCESS == transactionStatus)
		{
			refundResult.setTransactionStatus(TransactionStatus.ACCEPTED);
			refundResult.setTransactionStatusDetails(TransactionStatusDetails.SUCCESFULL);
		}
		else
		{
			LOG.error("Refund payment has failed. WPG return code='{}', WPG return message='{}'",
					tokenRefundPaymentResponse.getReturnCode(), tokenRefundPaymentResponse.getReturnMessage());
			refundResult.setTransactionStatus(TransactionStatus.REJECTED);
			refundResult.setTransactionStatusDetails(TransactionStatusDetails.BANK_DECLINE);
		}
		refundResult.setRequestId(tokenRefundPaymentResponse.getTransId());
		refundResult.setTotalAmount(getTotalAmount(tokenRefundPaymentResponse));
		refundResult.setMerchantResponse(tokenRefundPaymentResponse.getMerchantResponse());
		refundResult.setWpgTransactionStatus(transactionStatus);

		return refundResult;
	}

	private BigDecimal getTotalAmount(final TokenRefundPaymentResponse tokenRefundPaymentResponse)
	{
		BigDecimal totalAmount = null;
		if (tokenRefundPaymentResponse.getValue() != null)
		{
			totalAmount = new BigDecimal(tokenRefundPaymentResponse.getValue());
		}
		return totalAmount;
	}

	private WileyTransactionStatusEnum getWileyTransactionStatus(final TokenRefundPaymentResponse wpgResponse)
	{
		WileyTransactionStatusEnum status = WileyTransactionStatusEnum.UNEXPECTED_CODE_FROM_WPG;
		Integer returnCode = wpgResponse.getReturnCode();
		if (returnCode != null)
		{
			status = WileyTransactionStatusEnum.findByCode(returnCode.toString());
		}
		return status;
	}
}
