package com.wiley.integrations.order.populator;

import com.wiley.integrations.order.dto.AddressWsDTO;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import static com.wiley.integrations.utils.PopulateUtils.populateFieldIfNotNull;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

public class WileyAddressWsPopulator implements Populator<AddressModel, AddressWsDTO>
{
	@Override
	public void populate(final AddressModel source, final AddressWsDTO target) throws ConversionException
	{
		validateParameterNotNullStandardMessage("source", source);
		validateParameterNotNullStandardMessage("target", target);

		target.setPostcode(source.getPostalcode());
		target.setCity(source.getTown());
		target.setLine1(source.getLine1());
		target.setLine2(source.getLine2());
		target.setFirstName(source.getFirstname());
		target.setLastName(source.getLastname());
		target.setPhoneNumber(source.getPhone1());
		target.setEmail(source.getEmail());
		target.setOrganization(source.getCompany());
		target.setDepartment(source.getDepartment());

		populateFieldIfNotNull((dest, value) -> dest.setCountry(value.getIsocode()), target, source.getCountry());
		populateFieldIfNotNull((dest, value) -> dest.setState(value.getIsocodeShort()), target, source.getRegion());

	}
}
