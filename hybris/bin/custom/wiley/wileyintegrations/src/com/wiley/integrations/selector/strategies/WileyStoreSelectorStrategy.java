package com.wiley.integrations.selector.strategies;

import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import javax.annotation.Nonnull;

import org.springframework.messaging.Message;


/**
 * Created by Uladzimir_Barouski on 12/7/2016.
 */
public interface WileyStoreSelectorStrategy
{
	@Nonnull
	Optional<BaseStoreModel> getCurrentBaseStore(@Nonnull Message<?> message);
}
