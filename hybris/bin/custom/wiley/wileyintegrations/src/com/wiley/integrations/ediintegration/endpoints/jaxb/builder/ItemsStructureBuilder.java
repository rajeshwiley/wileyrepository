package com.wiley.integrations.ediintegration.endpoints.jaxb.builder;



import com.wiley.integrations.ediintegration.endpoints.jaxb.ItemsStructure;


/**
 * Builder class for ItemsStructure jaxb bean
 */
public final class ItemsStructureBuilder
{
	//PurchaseOrderNumber
	private String purchaseOrderNumber;
	//ProductId
	private String productId;
	//Quantity
	private Long quantity;
	//Price
	private Double price;
	//Discount
	private Double discount;
	//TaxAmount
	private Double taxAmount;
	//SalesModel
	private String salesModel;

	private ItemsStructureBuilder()
	{
	}

	public static ItemsStructureBuilder createInstance()
	{
		return new ItemsStructureBuilder();
	}

	public ItemsStructureBuilder withPurchaseOrderNumber(final String purchaseOrderNumber)
	{
		this.purchaseOrderNumber = purchaseOrderNumber;
		return this;
	}

	public ItemsStructureBuilder withProductId(final String productId)
	{
		this.productId = productId;
		return this;
	}

	public ItemsStructureBuilder withQuantity(final Long quantity)
	{
		this.quantity = quantity;
		return this;
	}

	public ItemsStructureBuilder withPrice(final Double price)
	{
		this.price = price;
		return this;
	}

	public ItemsStructureBuilder withDiscount(final Double discount)
	{
		this.discount = discount;
		return this;
	}

	public ItemsStructureBuilder withTaxAmount(final Double taxAmount)
	{
		this.taxAmount = taxAmount;
		return this;
	}

	public ItemsStructureBuilder withSalesModel(final String salesModel)
	{
		this.salesModel = salesModel;
		return this;
	}

	public ItemsStructureBuilder but()
	{
		return createInstance().withPurchaseOrderNumber(purchaseOrderNumber).withProductId(productId).withQuantity(quantity)
				.withPrice(price).withDiscount(discount).withTaxAmount(taxAmount).withSalesModel(salesModel);
	}

	public ItemsStructure build()
	{
		ItemsStructure itemsStructure = new ItemsStructure();
		itemsStructure.setPurchaseOrderNumber(purchaseOrderNumber);
		itemsStructure.setProductId(productId);
		itemsStructure.setQuantity(quantity);
		itemsStructure.setPrice(price);
		itemsStructure.setDiscount(discount);
		itemsStructure.setTaxAmount(taxAmount);
		itemsStructure.setSalesModel(salesModel);
		return itemsStructure;
	}
}
