package com.wiley.integrations.sabrix.cacheddto;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;


public class EsbAddress
{

	private String country;
	private String city;
	private String proviceOrState;
	private String postcode;
	private String geocode;

	public String getCountry()
	{
		return country;
	}

	public void setCountry(final String country)
	{
		this.country = country;
	}

	public String getCity()
	{
		return city;
	}

	public void setCity(final String city)
	{
		this.city = city;
	}

	public String getProviceOrState()
	{
		return proviceOrState;
	}

	public void setProviceOrState(final String proviceOrState)
	{
		this.proviceOrState = proviceOrState;
	}

	public String getPostcode()
	{
		return postcode;
	}

	public void setPostcode(final String postcode)
	{
		this.postcode = postcode;
	}

	public String getGeocode()
	{
		return geocode;
	}

	public void setGeocode(final String geocode)
	{
		this.geocode = geocode;
	}


	@Override
	public int hashCode()
	{
		return Objects.hashCode(country, city, proviceOrState, postcode, geocode);
	}

	@Override
	public boolean equals(final Object obj)
	{
		if (this == obj)
		{
			return true;
		}
		if (obj == null || getClass() != obj.getClass())
		{
			return false;
		}
		final EsbAddress other = (EsbAddress) obj;
		return Objects.equal(this.country, other.country)
				&& Objects.equal(this.city, other.city)
				&& Objects.equal(this.proviceOrState, other.proviceOrState)
				&& Objects.equal(this.postcode, other.postcode)
				&& Objects.equal(this.geocode, other.geocode);
	}


	@Override
	public String toString()
	{
		return MoreObjects.toStringHelper(this)
				.add("country", country)
				.add("city", city)
				.add("proviceOrState", proviceOrState)
				.add("postcode", postcode)
				.add("geocode", geocode)
				.toString();
	}
}
