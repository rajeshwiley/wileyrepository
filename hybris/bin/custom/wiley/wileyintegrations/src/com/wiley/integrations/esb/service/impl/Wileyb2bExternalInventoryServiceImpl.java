package com.wiley.integrations.esb.service.impl;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import com.wiley.core.model.ExternalInventoryModel;
import com.wiley.integrations.esb.dto.verifyandcalculatecart.CartCalculationEntryResponse;
import com.wiley.integrations.esb.dto.verifyandcalculatecart.InventoryStatus;
import com.wiley.integrations.esb.service.Wileyb2bExternalInventoryService;

import org.apache.commons.collections.CollectionUtils;


/**
 * Default implementation of {@link Wileyb2bExternalInventoryService}
 */
public class Wileyb2bExternalInventoryServiceImpl implements Wileyb2bExternalInventoryService
{

	@Resource
	private ModelService modelService;

	@Resource
	private Populator<InventoryStatus, ExternalInventoryModel> inventoryStatusToExternalInventoryPopulator;

	@Override
	public void updateInventoryStatuses(@Nonnull final CartCalculationEntryResponse cartCalculationEntryResponse,
			@Nonnull final CartEntryModel cartEntryModel)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("cartCalculationEntryResponse", cartCalculationEntryResponse);
		ServicesUtil.validateParameterNotNullStandardMessage("cartEntryModel", cartEntryModel);

		removeExistingExternalInventories(cartEntryModel);

		createAndSaveExternalInventories(cartEntryModel, cartCalculationEntryResponse);
	}

	private void removeExistingExternalInventories(final CartEntryModel cartEntry)
	{
		final Set<ExternalInventoryModel> externalInventories = cartEntry.getExternalInventories();
		if (CollectionUtils.isNotEmpty(externalInventories))
		{
			cartEntry.setExternalInventories(Collections.emptySet());
			modelService.removeAll(externalInventories);
		}
	}

	private void createAndSaveExternalInventories(final CartEntryModel cartEntry, final CartCalculationEntryResponse entry)
	{
		final List<InventoryStatus> inventoryStatusDtos = entry.getInventoryStatuses();
		if (CollectionUtils.isNotEmpty(inventoryStatusDtos))
		{
			Set<ExternalInventoryModel> externalInventoryModels = new HashSet<>(inventoryStatusDtos.size());
			for (InventoryStatus inventoryStatusDto : inventoryStatusDtos)
			{
				final ExternalInventoryModel externalInventoryModel = createAndPopulateExternalInventory(cartEntry,
						inventoryStatusDto);
				externalInventoryModels.add(externalInventoryModel);
			}

			modelService.saveAll(externalInventoryModels);
			cartEntry.setExternalInventories(externalInventoryModels);
		}
	}

	private ExternalInventoryModel createAndPopulateExternalInventory(final CartEntryModel cartEntry,
			final InventoryStatus inventoryStatusDto)
	{
		final ExternalInventoryModel externalInventoryModel = modelService.create(ExternalInventoryModel.class);

		// populate data
		inventoryStatusToExternalInventoryPopulator.populate(inventoryStatusDto, externalInventoryModel);
		externalInventoryModel.setOrderEntry(cartEntry);

		return externalInventoryModel;
	}
}
