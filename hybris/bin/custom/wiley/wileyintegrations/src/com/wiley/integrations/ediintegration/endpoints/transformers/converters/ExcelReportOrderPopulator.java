package com.wiley.integrations.ediintegration.endpoints.transformers.converters;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.util.DiscountValue;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.paypal.hybris.model.PaypalPaymentInfoModel;
import com.wiley.core.integration.edi.dto.ExcelReportOrder;
import com.wiley.fulfilmentprocess.model.WileyExportProcessModel;
import com.wiley.core.integration.edi.EDIConstants;
import com.wiley.core.integration.edi.service.impl.WileyPurchaseOrderService;


public class ExcelReportOrderPopulator implements Populator<WileyExportProcessModel, ExcelReportOrder>
{
	@Resource
	private WileyPurchaseOrderService wileyPurchaseOrderService;

	private static final Logger LOG = Logger.getLogger(ExcelReportOrderPopulator.class);

	@Override
	public void populate(final WileyExportProcessModel exportProcess,
			final ExcelReportOrder excelReport) throws ConversionException
	{

		final OrderModel orderModel = exportProcess.getOrder();
		final String code = orderModel.getCode();
		LOG.debug(String.format("Populator started: WileyExportProcessModel"
				+ " to ExcelReportOrder  for order %s", code));
		LOG.info("Converting order with code: [" + code + "]");

		final BaseSiteModel site = orderModel.getSite();
		if (site != null)
		{
			excelReport.setProductId(site.getUid());
		}
		LOG.debug(String.format("ProductId %s is set for order %s", excelReport.getProductId(), code));

		excelReport.setPurchaseOrderNumber(ConversionUtils.buildPONumberFormat(orderModel));
		LOG.debug(String.format("PurchaseOrderNumber %s is set for order %s", excelReport.getPurchaseOrderNumber(), code));
		if (orderModel.getPaymentInfo() instanceof CreditCardPaymentInfoModel)
		{
			CreditCardPaymentInfoModel paymentInfo = (CreditCardPaymentInfoModel) orderModel.getPaymentInfo();
			excelReport.setCreditCardType(wileyPurchaseOrderService.getCreditCardType(paymentInfo));

			LOG.debug(String.format("CreditCardType %s is set for order %s", excelReport.getCreditCardType(), code));
			excelReport.setCreditCardOwner(paymentInfo.getCcOwner());
			LOG.debug(String.format("CreditCardOwner %s is set for order %s", excelReport.getCreditCardOwner(), code));
		}
		else if (orderModel.getPaymentInfo() instanceof PaypalPaymentInfoModel) {
			excelReport.setCreditCardType(EDIConstants.CARD_TYPE_PAYPAL);
			LOG.debug(String.format("CreditCardType %s is set for order %s", excelReport.getCreditCardType(), code));
		}
		else
		{
			LOG.error(String.format("Can't convert payment info data, payment info doesn't contain CreditCard info for order %s",
					code));
		}
		excelReport.setCreditCardToken(wileyPurchaseOrderService.getCCToken(orderModel));
		LOG.debug(String.format("CreditCardToken %s is set for order %s", excelReport.getCreditCardToken(), code));
		excelReport.setAuthCode(wileyPurchaseOrderService.buildHybrisAuthorizationCode(orderModel));
		LOG.debug(String.format("AuthCode %s is set for order %s", excelReport.getAuthCode(), code));
		final String orderType = wileyPurchaseOrderService.getOrderType(exportProcess);
		excelReport.setOrderType(orderType);
		excelReport.setOrderCurrency(getCurrencyIsoCode(orderModel));
		excelReport.setTax(orderModel.getTotalTax());
		LOG.debug(String.format("Tax %s is set for order %s", excelReport.getTax(), code));
		excelReport.setShipping(orderModel.getDeliveryCost());
		LOG.debug(String.format("Shipping %s is set for order %s", excelReport.getShipping(), code));
		Double productLevelDiscounts = orderModel.getEntries().stream()
				.mapToDouble(entry -> entry.getDiscountValues().stream().mapToDouble(DiscountValue::getAppliedValue).sum())
				.sum();
		LOG.debug(String.format("ProductLevelDiscounts %s is calculated for order %s", productLevelDiscounts, code));
		final double orderAndProductLevelDiscounts = orderModel.getTotalDiscounts() + productLevelDiscounts;
		excelReport.setTotalDiscounts(orderAndProductLevelDiscounts);
		LOG.debug(String.format("TotalDiscounts %s are calculated for order %s", excelReport.getTotalDiscounts(), code));
		excelReport.setOrderAmount(orderModel.getSubTotalWithoutDiscount());
		LOG.debug(String.format("OrderAmount %s is calculated for order %s", excelReport.getOrderAmount(), code));
		excelReport.setGrandTotal(wileyPurchaseOrderService.getSettleOrRefundTransactionAmount(exportProcess).doubleValue());
		excelReport.setCreationTime(orderModel.getCreationtime());
		excelReport.setTransactionDate(exportProcess.getTransactionDate());
		LOG.debug(String.format("Populator ended: WileyExportProcessModel to ExcelReportOrder for order %s", code));
	}


	private String getCurrencyIsoCode(final OrderModel orderModel)
	{
		final CurrencyModel currency = orderModel.getCurrency();
		if (currency != null)
		{
			return currency.getIsocode();
		} else {
			LOG.error(String.format("Currency is null for order %s",
					orderModel.getCode()));

		}
		return null;
	}

}
