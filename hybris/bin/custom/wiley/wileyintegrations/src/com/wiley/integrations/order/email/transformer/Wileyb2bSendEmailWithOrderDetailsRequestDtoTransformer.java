package com.wiley.integrations.order.email.transformer;

import com.wiley.core.integration.order.email.Wileyb2bSendEmailWithOrderDetailsGateway;
import com.wiley.integrations.order.email.dto.OrderDetailsRequestDto;

import org.springframework.messaging.Message;
import org.springframework.util.Assert;


/**
 * Spring integration transformer which extracts userId from {@code Message} payload and orderId, email as header values
 * and produces {@code OrderDetailsRequestDto}.
 */
public class Wileyb2bSendEmailWithOrderDetailsRequestDtoTransformer
{

	public OrderDetailsRequestDto transform(final Message<String> message)
	{
		Assert.notNull(message);

		OrderDetailsRequestDto orderDetailsRequestDto = new OrderDetailsRequestDto();

		String userId = message.getPayload();
		Assert.notNull(userId);
		orderDetailsRequestDto.setUserId(userId);

		String email = (String) message.getHeaders().get(Wileyb2bSendEmailWithOrderDetailsGateway.EMAIL);
		Assert.notNull(email);
		orderDetailsRequestDto.setEmail(email);

		String orderId = (String) message.getHeaders().get(Wileyb2bSendEmailWithOrderDetailsGateway.ORDER_ID);
		Assert.notNull(orderId);
		orderDetailsRequestDto.setOrderId(orderId);

		return orderDetailsRequestDto;
	}
}
