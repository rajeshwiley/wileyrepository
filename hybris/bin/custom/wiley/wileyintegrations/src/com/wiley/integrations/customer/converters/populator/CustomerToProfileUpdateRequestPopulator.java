package com.wiley.integrations.customer.converters.populator;

import com.wiley.integrations.cdm.enums.CustomerRole;
import com.wiley.integrations.customer.dto.ProfileUpdateRequest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.apache.commons.lang.StringUtils;

import javax.annotation.Resource;


public class CustomerToProfileUpdateRequestPopulator implements Populator<CustomerModel, ProfileUpdateRequest>
{
	@Resource
	private B2BCustomerToProfileUpdateRequestPopulator b2bCustomerToProfileUpdateRequestPopulator;

	@Override
	public void populate(final CustomerModel source, final ProfileUpdateRequest target) throws ConversionException
	{
		target.setIndividualEcidNumber(source.getEcidNumber());

		addCommonDetails(source, target);
		addSchoolDetails(source, target);

		if (source instanceof B2BCustomerModel)
		{
			b2bCustomerToProfileUpdateRequestPopulator.populate((B2BCustomerModel) source, target);
		}
	}

	private void addCommonDetails(final CustomerModel source, final ProfileUpdateRequest target)
	{
		if (source.getTitle() != null)
		{
			target.setTitleCode(source.getTitle().getCode());
		}
		if (source.getSuffix() != null)
		{
			target.setSuffixCode(source.getSuffix().getCode());
		}

		target.setFirstName(source.getFirstName());
		target.setMiddleName(source.getMiddleName());
		target.setLastName(source.getLastName());
		target.setEmail(source.getContactEmail());
		target.setStudentId(source.getStudentId());
		target.setSapAccountNumber(source.getSapAccountNumber());
	}

	private void addSchoolDetails(final CustomerModel source, final ProfileUpdateRequest target)
	{
		if (source.getSchool() != null)
		{
			target.setSchoolId(source.getSchool().getCode());
			target.setEnrolledInSchool(Boolean.TRUE);
			target.setRole(CustomerRole.STUDENT.toString());
		}
		else
		{
			target.setRole(CustomerRole.INDIVIDUAL.toString());
		}

		target.setMajor(source.getMajor());
		target.setGraduationMonth(StringUtils.isNotEmpty(source.getGraduationMonth()) ?
				Integer.parseInt(source.getGraduationMonth()) : null);
		target.setGraduationYear(StringUtils.isNotEmpty(source.getGraduationYear()) ?
				Integer.parseInt(source.getGraduationYear()) : null);
	}
}
