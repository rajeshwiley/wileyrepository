package com.wiley.integrations.tax.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import com.wiley.core.externaltax.dto.TaxAddressDto;
import com.wiley.integrations.tax.dto.TaxAddress;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


public class TaxAddressPopulator implements Populator<TaxAddressDto, TaxAddress>
{

	@Override
	public void populate(final TaxAddressDto source, final TaxAddress target) throws ConversionException
	{
		validateParameterNotNull(source, "source mustn't be null");
		validateParameterNotNull(target, "target mustn't be null");

		target.setPostcode(source.getPostcode());
		target.setCountry(source.getCountry());
		target.setState(source.getState());
		target.setCity(source.getCity());
	}
}
