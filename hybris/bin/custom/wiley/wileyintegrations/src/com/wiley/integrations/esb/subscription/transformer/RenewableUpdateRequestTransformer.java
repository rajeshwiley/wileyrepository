package com.wiley.integrations.esb.subscription.transformer;

import org.springframework.messaging.handler.annotation.Payload;

import com.wiley.integrations.esb.subscription.dto.RenewableUpdateRequestSwgDTO;
import com.wiley.integrations.esb.subscription.dto.RenewableUpdateRequestSwgDTO.AutorenewEnum;


public class RenewableUpdateRequestTransformer
{

	public RenewableUpdateRequestSwgDTO transform(@Payload final boolean autorenew)
	{
		final RenewableUpdateRequestSwgDTO renewableUpdateRequest = new RenewableUpdateRequestSwgDTO();
		if (autorenew)
		{
			renewableUpdateRequest.setAutorenew(AutorenewEnum.ENABLED);
		}
		else
		{
			renewableUpdateRequest.setAutorenew(AutorenewEnum.DISABLED);
		}
		return renewableUpdateRequest;
	}
}
