package com.wiley.integrations.wpg.dto;

public class TokenRefundPaymentRequest
{
	private int vendorId;
	private String transId;
	private String userId;
	private String method;
	private String customerPresent;
	private String token;
	private String description;
	private String value;
	private String currency;
	private String timestamp;
	private String region;
	private String security;

	public int getVendorId()
	{
		return vendorId;
	}

	public void setVendorId(final int vendorId)
	{
		this.vendorId = vendorId;
	}

	public String getTransId()
	{
		return transId;
	}

	public String getUserId()
	{
		return userId;
	}

	public void setUserId(final String userId)
	{
		this.userId = userId;
	}

	public void setTransId(final String transId)
	{
		this.transId = transId;
	}

	public String getMethod()
	{
		return method;
	}

	public void setMethod(final String method)
	{
		this.method = method;
	}

	public String getCustomerPresent()
	{
		return customerPresent;
	}

	public void setCustomerPresent(final String customerPresent)
	{
		this.customerPresent = customerPresent;
	}

	public String getToken()
	{
		return token;
	}

	public void setToken(final String token)
	{
		this.token = token;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(final String description)
	{
		this.description = description;
	}

	public String getValue()
	{
		return value;
	}

	public void setValue(final String value)
	{
		this.value = value;
	}

	public String getCurrency()
	{
		return currency;
	}

	public void setCurrency(final String currency)
	{
		this.currency = currency;
	}

	public String getTimestamp()
	{
		return timestamp;
	}

	public void setTimestamp(final String timestamp)
	{
		this.timestamp = timestamp;
	}

	public String getRegion()
	{
		return region;
	}

	public void setRegion(final String region)
	{
		this.region = region;
	}

	public String getSecurity()
	{
		return security;
	}

	public void setSecurity(final String security)
	{
		this.security = security;
	}
}
