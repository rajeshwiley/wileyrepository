
package com.wiley.integrations.price.convereters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.product.UnitService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import org.apache.commons.lang3.StringUtils;

import com.wiley.integrations.constants.WileyintegrationsConstants;
import com.wiley.integrations.price.dto.PriceDto;


public class PriceImportPopulator implements Populator<PriceDto, PriceRowModel>
{

	private CommonI18NService commonI18NService;

	private UnitService unitService;

	@Override
	public void populate(final PriceDto priceDto, final PriceRowModel priceRowModel) throws ConversionException
	{
		ServicesUtil.validateParameterNotNull(priceDto, "priceDto object must not be null");

		priceRowModel.setNet(Boolean.TRUE);
		priceRowModel.setPrice(priceDto.getPrice());

		final UnitModel unit = unitService.getUnitForCode(WileyintegrationsConstants.DEFAULT_UNIT_PIECES);
		priceRowModel.setUnit(unit);

		try
		{
			priceRowModel.setCurrency(commonI18NService.getCurrency(priceDto.getCurrency()));
		}
		catch (final UnknownIdentifierException e)
		{
			throw new ConversionException("Can't convert currency " + e.getMessage());
		}

		if (StringUtils.isNotBlank(priceDto.getCountry()))
		{
			try
			{
				priceRowModel.setCountry(commonI18NService.getCountry(priceDto.getCountry()));
			}
			catch (final UnknownIdentifierException e)
			{
				throw new ConversionException("Can't convert country " + e.getMessage());
			}
		}

		priceRowModel.setProductId(priceDto.getProductId());
		priceRowModel.setMinqtd(Long.valueOf(priceDto.getMinQuantity()));
		priceRowModel.setStartTime(priceDto.getStartTime());
	}

	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

	public void setUnitService(final UnitService unitService)
	{
		this.unitService = unitService;
	}
}
