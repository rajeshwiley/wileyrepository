package com.wiley.integrations.eloqua.converters.populator;

import com.wiley.core.integration.eloqua.dto.ContactDto;
import com.wiley.core.integration.eloqua.dto.FieldValueDto;
import com.wiley.integrations.eloqua.util.CustomObjectDataUtils;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.Assert;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;


/**
 * Populates customer data in Contact.
 *
 * @author Aliaksei_Zlobich
 */
public class SubscriptionStatusContactPopulator implements Populator<Boolean, ContactDto>
{
	/**
	 * Id of Subscription status field. Used by Eloqua to determine subscription type (status).
	 */
	@Value("${eloqua.api.field_id.subscription_status}")
	private Integer subscriptionStatusFieldId;

	/**
	 * Indicates that customer will receive updates from Eloqua.
	 */
	public static final String SUBSCRIPTION_STATUS_FIELD_EXPLICIT_CONSENT_VALUE = "Explicit Consent";
	public static final String SUBSCRIPTION_STATUS_FIELD_EXPLICIT_CONSENT_ABSENT_VALUE = "No Response";

	@Override
	public void populate(@Nullable final Boolean subscribeToUpdates, @Nonnull final ContactDto contact) throws ConversionException
	{
		Assert.notNull(contact);
		if (subscribeToUpdates != null)
		{
			List<FieldValueDto> fieldValues = contact.getFieldValues();
			/*
			As described in IDD we need to populate Contact with specific FieldValue
			which indicates that Contact wishes to receive updates from Eloqua.
			*/

			fieldValues.add(CustomObjectDataUtils
					.createFieldValue(subscriptionStatusFieldId, createSubscribeValue(subscribeToUpdates)));
		}
	}

	private String createSubscribeValue(final Boolean subscribeToUpdates)
	{
		return Boolean.TRUE.equals(subscribeToUpdates) ?
				SUBSCRIPTION_STATUS_FIELD_EXPLICIT_CONSENT_VALUE : SUBSCRIPTION_STATUS_FIELD_EXPLICIT_CONSENT_ABSENT_VALUE;
	}
}