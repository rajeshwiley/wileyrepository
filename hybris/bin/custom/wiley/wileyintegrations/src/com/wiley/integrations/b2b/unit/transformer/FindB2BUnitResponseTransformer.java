package com.wiley.integrations.b2b.unit.transformer;


import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.user.AddressModel;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import com.wiley.integrations.b2b.unit.dto.LocalizedStringSwgDTO;
import com.wiley.integrations.b2b.unit.dto.B2BUnitSwgDTO;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.LocaleUtils;
import org.apache.log4j.Logger;


public class FindB2BUnitResponseTransformer
{
	private static final Logger LOG = Logger.getLogger(FindB2BUnitResponseTransformer.class);

	@Resource
	private AddressSwgDTOTransformer findB2bUnitAddressTransformer;

	public B2BUnitModel transform(final B2BUnitSwgDTO b2BUnitDTO)
	{
		if (b2BUnitDTO != null)
		{
			final B2BUnitModel b2BUnitModel = new B2BUnitModel();
			b2BUnitModel.setUid(b2BUnitDTO.getSapAccountNumber());
			b2BUnitModel.setSapAccountNumber(b2BUnitDTO.getSapAccountNumber());
			b2BUnitModel.setAccountEcidNumber(b2BUnitDTO.getAccountEcidNumber());
			b2BUnitModel.setSapSalesRepContactName(b2BUnitDTO.getSalesRepContactName());
			b2BUnitModel.setSapSalesRepContactEmail(b2BUnitDTO.getSalesRepContactEmail());
			b2BUnitModel.setSapSalesRepContactPhone(b2BUnitDTO.getSalesRepContactPhone());

			if (CollectionUtils.isNotEmpty(b2BUnitDTO.getName()))
			{
				b2BUnitDTO.getName().forEach(localizedNameDto -> setLocalizedName(b2BUnitModel, localizedNameDto));
			}

			final List<AddressModel> allAddresses = b2BUnitDTO.getShippingAddresses().stream()
					.map(addressDTO -> findB2bUnitAddressTransformer.transform(addressDTO, 
							true, false)).collect(Collectors.toList());

			if (CollectionUtils.isNotEmpty(allAddresses))
			{
				b2BUnitModel.setShippingAddress(allAddresses.get(0));
			}

			final AddressModel billingAddress = findB2bUnitAddressTransformer.transform(b2BUnitDTO.getSoldToAddress(), 
					false, true);

			if (billingAddress != null)
			{
				b2BUnitModel.setBillingAddress(billingAddress);
				allAddresses.add(billingAddress);
			}

			allAddresses.forEach(address -> address.setOwner(b2BUnitModel));

			b2BUnitModel.setAddresses(allAddresses);
			return b2BUnitModel;
		}
		return null;
	}

	private void setLocalizedName(final B2BUnitModel b2BUnitModel, final LocalizedStringSwgDTO localizedNameDto)
	{
		if (localizedNameDto != null && localizedNameDto.getLoc() != null)
		{
			b2BUnitModel.setLocName(localizedNameDto.getVal(), LocaleUtils.toLocale(localizedNameDto.getLoc()));
		}
		else
		{
			LOG.warn("Cannot set localized name " + localizedNameDto);
		}
	}
}
