package com.wiley.integrations.esb.dto.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.apache.commons.lang3.builder.ToStringBuilder;


/**
 * An address.<br/>
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Address
{

	private String addressId;

	/**
	 * Postal code.
	 **/
	private String postcode;
	/**
	 * Country
	 */
	private String country;
	/**
	 * State
	 */
	private String state;
	/**
	 * Destination city.
	 **/
	private String city;
	/**
	 * First line of the address.
	 **/
	private String line1;
	/**
	 * Second line of the address.
	 **/
	private String line2;

	/**
	 * Phone number.
	 */
	private String phoneNumber;

	@JsonProperty("addressId")
	public String getAddressId()
	{
		return addressId;
	}

	public void setAddressId(final String addressId)
	{
		this.addressId = addressId;
	}

	@JsonProperty("postcode")
	public String getPostcode()
	{
		return postcode;
	}

	public void setPostcode(final String postcode)
	{
		this.postcode = postcode;
	}


	@JsonProperty("country")
	public String getCountry()
	{
		return country;
	}

	public void setCountry(final String country)
	{
		this.country = country;
	}

	@JsonProperty("state")
	public String getState()
	{
		return state;
	}

	public void setState(final String state)
	{
		this.state = state;
	}

	@JsonProperty("city")
	public String getCity()
	{
		return city;
	}

	public void setCity(final String city)
	{
		this.city = city;
	}

	@JsonProperty("line1")
	public String getLine1()
	{
		return line1;
	}

	public void setLine1(final String line1)
	{
		this.line1 = line1;
	}

	@JsonProperty("line2")
	public String getLine2()
	{
		return line2;
	}

	public void setLine2(final String line2)
	{
		this.line2 = line2;
	}

	@JsonProperty("phoneNumber")
	public String getPhoneNumber()
	{
		return phoneNumber;
	}

	public void setPhoneNumber(final String phoneNumber)
	{
		this.phoneNumber = phoneNumber;
	}

	@Override
	public String toString()
	{
		return new ToStringBuilder(this)
				.append("addressId", addressId)
				.append("postcode", postcode)
				.append("country", country)
				.append("state", state)
				.append("city", city)
				.append("line1", line1)
				.append("line2", line2)
				.append("phoneNumber", phoneNumber)
				.toString();
	}
}

