package com.wiley.integrations.vies.transformer;

import javax.annotation.Nonnull;
import com.wiley.core.integration.vies.dto.CheckVatResponseDto;
import com.wiley.integrations.vies.dto.CheckVatResponse;
import com.wiley.integrations.vies.marshaller.ViesMarshaller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.xml.transform.StringResult;


public class CheckVatResponseTransformer
{
    @Autowired
    private ViesMarshaller viesMarshaller;

    private static final Logger LOG = LoggerFactory.getLogger(CheckVatResponseTransformer.class);

	public CheckVatResponseDto transform(@Nonnull final CheckVatResponse checkVatResponse)
	{
		CheckVatResponseDto checkVatResponseDto = new CheckVatResponseDto();
		checkVatResponseDto.setValid(checkVatResponse.isValid());

        StringResult result = new StringResult();
        viesMarshaller.marshal(checkVatResponse, result);
        LOG.debug("Response VIES XML message: /n{} ", result);

		return checkVatResponseDto;
	}

}
