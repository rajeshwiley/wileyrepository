package com.wiley.integrations.mpgs.transformer;


import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;

import com.wiley.core.mpgs.dto.verify.MPGSVerifyResponseDTO;
import com.wiley.core.mpgs.response.WileyVerifyResponse;
import com.wiley.core.mpgs.services.WileyTransformationService;


public class WileyMPGSVerifyResponseTransformer
{
	@Autowired
	private WileyTransformationService wileyTransformationService;

	public final WileyVerifyResponse transform(@Nonnull final Message<MPGSVerifyResponseDTO> message)
	{
		WileyVerifyResponse verifyResponse = new WileyVerifyResponse();
		MPGSVerifyResponseDTO responseDto = message.getPayload();

		verifyResponse.setStatus(wileyTransformationService.transformStatusIfSuccessful(responseDto.getResult()));
		verifyResponse.setStatusDetails(responseDto.getResponse().getGatewayCode());
		verifyResponse.setTransactionId(responseDto.getTransaction().getId());
		verifyResponse.setCurrency(responseDto.getTransaction().getCurrency());

		final String transactionTime = responseDto.getTimeOfRecord();
		if (transactionTime != null)
		{
			verifyResponse.setTimeOfRecord(wileyTransformationService.transformStringToDate(transactionTime));
		}

		return verifyResponse;
	}
}
