package com.wiley.integrations.price.dto;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;


public class PriceDto
{
	@NotBlank
	private String productId;

	@NotNull
	private Double price;

	@NotBlank
	private String currency;

	private String country;

	private Integer minQuantity = Integer.valueOf(1);

	private Date startTime;

	public String getProductId()
	{
		return productId;
	}

	public void setProductId(final String productId)
	{
		this.productId = productId;
	}

	public Double getPrice()
	{
		return price;
	}

	public void setPrice(final Double price)
	{
		this.price = price;
	}

	public String getCurrency()
	{
		return currency;
	}

	public void setCurrency(final String currency)
	{
		this.currency = currency;
	}

	public String getCountry()
	{
		return country;
	}

	public void setCountry(final String country)
	{
		this.country = country;
	}

	public Integer getMinQuantity()
	{
		return minQuantity;
	}

	public void setMinQuantity(final Integer minQuantity)
	{
		this.minQuantity = minQuantity;
	}

	public Date getStartTime()
	{
		return startTime;
	}

	public void setStartTime(final Date startTime)
	{
		this.startTime = startTime;
	}

	@Override
	public String toString()
	{
		final StringBuilder builder = new StringBuilder();
		builder.append("PriceDto [productId=");
		builder.append(productId);
		builder.append(", price=");
		builder.append(price);
		builder.append(", currency=");
		builder.append(currency);
		builder.append(", country=");
		builder.append(country);
		builder.append(", minQuantity=");
		builder.append(minQuantity);
		builder.append(", startTime=");
		builder.append(startTime);
		builder.append("]");
		return builder.toString();
	}

}
