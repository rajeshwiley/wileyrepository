package com.wiley.integrations.sabrix.transformer.populators.line;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.Map;

import javax.annotation.Nonnull;

import org.springframework.util.Assert;

import com.wiley.core.enums.WileyProductSubtypeEnum;
import com.wiley.integrations.sabrix.dto.IndataLineType;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


/**
 * @author Dzmitryi_Halahayeu
 */
public class OrderItemToJournalLinePopulator implements Populator<ProductModel, IndataLineType>
{
	static final String ONETIME_FULFILMENT_TYPE = "onetime";
	static final String SUBSCRIPTION_FULFILMENT_TYPE = "subscription";

	private Map<String, String> journalTaxCategory2MediaType;

	@Override
	public void populate(@Nonnull final ProductModel product, @Nonnull final IndataLineType indataLineType)
			throws ConversionException
	{
		validateParameterNotNullStandardMessage("product", product);
		validateParameterNotNullStandardMessage("indataLineType", indataLineType);

		String taxCategory = product.getTaxCategory();
		Assert.hasLength(taxCategory);
		if (journalTaxCategory2MediaType.containsKey(taxCategory))
		{
			indataLineType.setMEDIATYPE(journalTaxCategory2MediaType.get(taxCategory));

			indataLineType.setFULFILMENTTYPE(isSubscriptionFulfillmentType(product) ?
					SUBSCRIPTION_FULFILMENT_TYPE : ONETIME_FULFILMENT_TYPE);
		}
	}

	private boolean isSubscriptionFulfillmentType(@Nonnull final ProductModel product)
	{
		return WileyProductSubtypeEnum.SUBSCRIPTION.equals(product.getSubtype());
	}

	public void setJournalTaxCategory2MediaType(final Map<String, String> journalTaxCategory2MediaType)
	{
		this.journalTaxCategory2MediaType = journalTaxCategory2MediaType;
	}
}
