package com.wiley.integrations.alm.strategy.impl;

import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.validation.exceptions.HybrisConstraintViolation;
import de.hybris.platform.validation.services.ValidationService;
import static org.junit.Assert.assertNotNull;

import java.util.Optional;
import java.util.Set;

import javax.validation.constraints.NotNull;

import org.fest.util.Collections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import com.wiley.core.integration.alm.user.dto.UserDto;
import com.wiley.core.integration.alm.user.service.AlmUserService;
import com.wiley.integrations.alm.strategy.AlmUserAutoCreationStrategy;


public class AlmUserAutoCreationStrategyImpl implements AlmUserAutoCreationStrategy
{
	@Autowired
	@Qualifier("originalUidUserDetailsService")
	private UserDetailsService userDetailsService;

	@Autowired
	private Converter<UserDto, CustomerModel> wileyCustomerModelReverseConverter;

	@Autowired
	private CustomerAccountService customerAccountService;

	@Autowired
	private AlmUserService almUserGatewayService;

	@Autowired
	private ValidationService validationService;

	@Override
	public UserDetails createNewUserByAlmId(@NotNull final String almId)
	{
		assertNotNull(almId);
		UserDto userDto = almUserGatewayService.getUserData(almId);
		validateCustomerData(userDto);

		CustomerModel customerModel = wileyCustomerModelReverseConverter.convert(userDto);
		try
		{
			customerAccountService.register(customerModel, null);
		}
		catch (DuplicateUidException e)
		{
			throw new IllegalStateException("Create customer failed", e);
		}
		return userDetailsService.loadUserByUsername(almId);
	}

	protected void validateCustomerData(final UserDto userDto)
	{
		Set<HybrisConstraintViolation> constrainViolations = validationService.validate(userDto);
		if (!Collections.isEmpty(constrainViolations)) {
			Optional<HybrisConstraintViolation> firstViolation = constrainViolations.stream().findFirst();
			if (firstViolation.isPresent()) {
				throw new IllegalArgumentException(firstViolation.get().toString());
			}
		}
	}
}
