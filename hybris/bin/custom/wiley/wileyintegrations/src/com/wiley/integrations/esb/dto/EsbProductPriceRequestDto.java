package com.wiley.integrations.esb.dto;

import java.util.Objects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.common.base.MoreObjects;


@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EsbProductPriceRequestDto
{
	@XmlElement(required = true)
	private String productCode;

	@XmlElement(required = true)
	private String sapAccountNumber;

	@XmlElement(required = true)
	private String sapSalesArea;

	public String getProductCode()
	{
		return productCode;
	}

	public void setProductCode(final String productCode)
	{
		this.productCode = productCode;
	}

	public String getSapAccountNumber()
	{
		return sapAccountNumber;
	}

	public void setSapAccountNumber(final String sapAccountNumber)
	{
		this.sapAccountNumber = sapAccountNumber;
	}

	public String getSapSalesArea()
	{
		return sapSalesArea;
	}

	public void setSapSalesArea(final String sapSalesArea)
	{
		this.sapSalesArea = sapSalesArea;
	}

	@Override
	public String toString()
	{
		return MoreObjects.toStringHelper(this)
				.add("productCode", productCode)
				.add("sapAccountNumber", sapAccountNumber)
				.add("sapSalesArea", sapSalesArea)
				.toString();
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		final EsbProductPriceRequestDto that = (EsbProductPriceRequestDto) o;
		return Objects.equals(productCode, that.productCode)
				&& Objects.equals(sapAccountNumber, that.sapAccountNumber)
				&& Objects.equals(sapSalesArea, that.sapSalesArea);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(productCode, sapAccountNumber, sapSalesArea);
	}
}
