package com.wiley.integrations.oauth2.client;

import java.net.URI;

import com.wiley.core.exceptions.ExternalSystemAccessException;
import com.wiley.core.exceptions.ExternalSystemUnauthorizedException;

import org.slf4j.Logger;
import org.springframework.http.HttpMethod;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.resource.OAuth2AccessDeniedException;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.web.client.RequestCallback;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.ResponseExtractor;
import org.springframework.web.client.RestClientException;


/**
 * Rest template to handle access resource error handling
 */
public class WileyOAuth2RestTemplate extends OAuth2RestTemplate
{
	private static final Logger LOG = org.slf4j.LoggerFactory.getLogger(WileyOAuth2RestTemplate.class);

	public WileyOAuth2RestTemplate(final OAuth2ProtectedResourceDetails resource)
	{
		super(resource);
	}

	public WileyOAuth2RestTemplate(final OAuth2ProtectedResourceDetails resource,
			final OAuth2ClientContext context)
	{
		super(resource, context);
	}

	@Override
	protected <T> T doExecute(final URI url, final HttpMethod method, final RequestCallback requestCallback,
			final ResponseExtractor<T> responseExtractor)
			throws RestClientException
	{
		try
		{
			return super.doExecute(url, method, requestCallback, responseExtractor);
		}
		catch (ResourceAccessException e)
		{
			throw new ExternalSystemAccessException("External system is unreachable", e);
		}
		catch (OAuth2AccessDeniedException e)
		{
			if (e.getCause() instanceof ResourceAccessException)
			{

				throw new ExternalSystemAccessException("External system is unreachable for oauth access", e);
			}
			else
			{
				throw e;
			}
		}
		//OAuth2Exception is caught here explicitly because 401 and 403 errors
		//can be not passed by OAuth2RestTemplate to ExternalSystemResponseErrorHandler
		catch (OAuth2Exception e)
		{
			LOG.error("External system forbade the request", e);
			throw new ExternalSystemUnauthorizedException(e);
		}
	}
}
