package com.wiley.integrations.eloqua.transformer;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.util.Assert;

import com.wiley.core.integration.eloqua.dto.ContactDto;


/**
 * Transforms Customer and Address data to Contact data.<br/>
 * Mainly this transformer uses to create new contact or update existing contact in Eloqua.
 *
 * @author Aliaksei_Zlobich
 */
public class CustomerToContactTransformer
{

	private static final Logger LOG = LoggerFactory.getLogger(CustomerToContactTransformer.class);

	public static final String HEADER_CUSTOMER_ADDRESS = "customerAddress";
	public static final String HEADER_SUBSCRIBE_TO_UPDATES = "subscribeToUpdates";

	@Resource
	private Populator<AddressModel, ContactDto> addressToContactPopulator;

	@Resource
	private Populator<CustomerModel, ContactDto> customerToContactPopulator;

	@Resource
	private Populator<CustomerModel, ContactDto> customerToContactEloquaIdPopulator;

	@Resource
	private Populator<Boolean, ContactDto> subscriptionStatusContactPopulator;

	@Nonnull
	public ContactDto transform(@Nonnull final Message<CustomerModel> message)
	{
		final CustomerModel customer = message.getPayload();
		final AddressModel address = (AddressModel) message.getHeaders().get(HEADER_CUSTOMER_ADDRESS);
		final Boolean subscribeToUpdates = (Boolean) message.getHeaders().get(HEADER_SUBSCRIBE_TO_UPDATES);

		Assert.notNull(customer);

		LOG.debug("Method params: Customer [{}], Address [{}]", customer.getUid(),
				address != null ? address.getPk() : "Is Empty");

		final ContactDto contact = new ContactDto();

		customerToContactEloquaIdPopulator.populate(customer, contact);
		customerToContactPopulator.populate(customer, contact);
		subscriptionStatusContactPopulator.populate(subscribeToUpdates, contact);
		if (address != null)
		{
			addressToContactPopulator.populate(address, contact);
		}

		return contact;
	}

}
