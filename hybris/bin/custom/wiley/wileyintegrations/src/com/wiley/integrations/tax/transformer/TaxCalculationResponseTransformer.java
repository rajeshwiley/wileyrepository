package com.wiley.integrations.tax.transformer;

import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.springframework.messaging.Message;

import com.wiley.core.externaltax.dto.TaxCalculationResponseDto;
import com.wiley.integrations.tax.dto.TaxCalculationResponse;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


public class TaxCalculationResponseTransformer
{
	@Resource
	private Converter<TaxCalculationResponse, TaxCalculationResponseDto> taxCalculationResponseDtoConverter;

	@Nonnull
	public TaxCalculationResponseDto transform(@Nonnull final Message<TaxCalculationResponse> message)
	{
		validateParameterNotNullStandardMessage("message", message);
		return taxCalculationResponseDtoConverter.convert(message.getPayload());
	}
}
