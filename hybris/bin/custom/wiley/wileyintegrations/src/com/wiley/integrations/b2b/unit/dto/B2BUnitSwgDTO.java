package com.wiley.integrations.b2b.unit.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * The B2B unit (company) data.
 */
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-07-26T11:08:47.854Z")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class B2BUnitSwgDTO
{

	private String sapAccountNumber = null;
	private String accountEcidNumber = null;
	private List<LocalizedStringSwgDTO> name = new ArrayList<>();
	private String salesRepContactName = null;
	private String salesRepContactEmail = null;
	private String salesRepContactPhone = null;
	private AddressSwgDTO soldToAddress = null;
	private List<AddressSwgDTO> shippingAddresses = new ArrayList<>();


	/**
	 * Unique identifier of the customer's B2B unit (company). It is also known as Business Partner Number.
	 **/
	public B2BUnitSwgDTO sapAccountNumber(final String sapAccountNumber)
	{
		this.sapAccountNumber = sapAccountNumber;
		return this;
	}

	@JsonProperty("sapAccountNumber")
	public String getSapAccountNumber()
	{
		return sapAccountNumber;
	}

	public void setSapAccountNumber(final String sapAccountNumber)
	{
		this.sapAccountNumber = sapAccountNumber;
	}


	/**
	 **/
	public B2BUnitSwgDTO accountEcidNumber(final String accountEcidNumber)
	{
		this.accountEcidNumber = accountEcidNumber;
		return this;
	}

	@JsonProperty("accountEcidNumber")
	public String getAccountEcidNumber()
	{
		return accountEcidNumber;
	}

	public void setAccountEcidNumber(final String accountEcidNumber)
	{
		this.accountEcidNumber = accountEcidNumber;
	}


	/**
	 * The displayable name of B2B unit.
	 **/
	public B2BUnitSwgDTO name(final List<LocalizedStringSwgDTO> name)
	{
		this.name = name;
		return this;
	}

	@JsonProperty("name")
	public List<LocalizedStringSwgDTO> getName()
	{
		return name;
	}

	public void setName(final List<LocalizedStringSwgDTO> name)
	{
		this.name = name;
	}


	/**
	 * Wiley representative's name is a combination of first name and last name.
	 **/
	public B2BUnitSwgDTO salesRepContactName(final String salesRepContactName)
	{
		this.salesRepContactName = salesRepContactName;
		return this;
	}

	@JsonProperty("salesRepContactName")
	public String getSalesRepContactName()
	{
		return salesRepContactName;
	}

	public void setSalesRepContactName(final String salesRepContactName)
	{
		this.salesRepContactName = salesRepContactName;
	}


	/**
	 * Wiley representative's contact email.
	 **/
	public B2BUnitSwgDTO salesRepContactEmail(final String salesRepContactEmail)
	{
		this.salesRepContactEmail = salesRepContactEmail;
		return this;
	}

	@JsonProperty("salesRepContactEmail")
	public String getSalesRepContactEmail()
	{
		return salesRepContactEmail;
	}

	public void setSalesRepContactEmail(final String salesRepContactEmail)
	{
		this.salesRepContactEmail = salesRepContactEmail;
	}


	/**
	 * Wiley representative's phone number.
	 **/
	public B2BUnitSwgDTO salesRepContactPhone(final String salesRepContactPhone)
	{
		this.salesRepContactPhone = salesRepContactPhone;
		return this;
	}

	@JsonProperty("salesRepContactPhone")
	public String getSalesRepContactPhone()
	{
		return salesRepContactPhone;
	}

	public void setSalesRepContactPhone(final String salesRepContactPhone)
	{
		this.salesRepContactPhone = salesRepContactPhone;
	}


	/**
	 **/
	public B2BUnitSwgDTO soldToAddress(final AddressSwgDTO soldToAddress)
	{
		this.soldToAddress = soldToAddress;
		return this;
	}

	@JsonProperty("soldToAddress")
	public AddressSwgDTO getSoldToAddress()
	{
		return soldToAddress;
	}

	public void setSoldToAddress(final AddressSwgDTO soldToAddress)
	{
		this.soldToAddress = soldToAddress;
	}


	/**
	 * List of the shipping adddresses.
	 **/
	public B2BUnitSwgDTO shippingAddresses(final List<AddressSwgDTO> shippingAddresses)
	{
		this.shippingAddresses = shippingAddresses;
		return this;
	}

	@JsonProperty("shippingAddresses")
	public List<AddressSwgDTO> getShippingAddresses()
	{
		return shippingAddresses;
	}

	public void setShippingAddresses(final List<AddressSwgDTO> shippingAddresses)
	{
		this.shippingAddresses = shippingAddresses;
	}


	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		B2BUnitSwgDTO b2BUnitSwgDTO = (B2BUnitSwgDTO) o;
		return Objects.equals(this.sapAccountNumber, b2BUnitSwgDTO.sapAccountNumber)
				&& Objects.equals(this.accountEcidNumber, b2BUnitSwgDTO.accountEcidNumber)
				&& Objects.equals(this.name, b2BUnitSwgDTO.name)
				&& Objects.equals(this.salesRepContactName, b2BUnitSwgDTO.salesRepContactName)
				&& Objects.equals(this.salesRepContactEmail, b2BUnitSwgDTO.salesRepContactEmail)
				&& Objects.equals(this.salesRepContactPhone, b2BUnitSwgDTO.salesRepContactPhone)
				&& Objects.equals(this.soldToAddress, b2BUnitSwgDTO.soldToAddress)
				&& Objects.equals(this.shippingAddresses, b2BUnitSwgDTO.shippingAddresses);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(sapAccountNumber, accountEcidNumber, name, salesRepContactName, salesRepContactEmail,
				salesRepContactPhone, soldToAddress, shippingAddresses);
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("class B2BUnit {\n");

		sb.append("    sapAccountNumber: ").append(toIndentedString(sapAccountNumber)).append("\n");
		sb.append("    accountEcidNumber: ").append(toIndentedString(accountEcidNumber)).append("\n");
		sb.append("    name: ").append(toIndentedString(name)).append("\n");
		sb.append("    salesRepContactName: ").append(toIndentedString(salesRepContactName)).append("\n");
		sb.append("    salesRepContactEmail: ").append(toIndentedString(salesRepContactEmail)).append("\n");
		sb.append("    salesRepContactPhone: ").append(toIndentedString(salesRepContactPhone)).append("\n");
		sb.append("    soldToAddress: ").append(toIndentedString(soldToAddress)).append("\n");
		sb.append("    shippingAddresses: ").append(toIndentedString(shippingAddresses)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(final Object o)
	{
		if (o == null)
		{
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}

