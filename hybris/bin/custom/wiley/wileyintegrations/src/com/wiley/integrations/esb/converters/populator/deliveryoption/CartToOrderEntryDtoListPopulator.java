package com.wiley.integrations.esb.converters.populator.deliveryoption;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.wiley.integrations.esb.dto.delivery.DeliveryAddressDto;
import com.wiley.integrations.esb.dto.delivery.OrderEntryDto;


/**
 * Created by Uladzimir_Barouski on 6/16/2016.
 */
public class CartToOrderEntryDtoListPopulator implements Populator<CartModel, List<OrderEntryDto>>
{

	@Resource
	private Populator<AbstractOrderEntryModel, DeliveryAddressDto> cartEntryToDeliveryAddressDtoPopulator;

	@Override
	public void populate(final CartModel cartModel, final List<OrderEntryDto> orderEntryDtos) throws ConversionException
	{
		Assert.notNull(cartModel);
		Assert.notNull(orderEntryDtos);
		for (AbstractOrderEntryModel cartEntryModel: cartModel.getEntries())
		{
			OrderEntryDto orderEntryDto = new OrderEntryDto();
			final ProductModel product = cartEntryModel.getProduct();
			Assert.notNull(product);
			orderEntryDto.setProductCode(product.getCode());
			orderEntryDto.setQuantity(cartEntryModel.getQuantity().intValue());
			orderEntryDto.setShipFrom(new DeliveryAddressDto());
			cartEntryToDeliveryAddressDtoPopulator.populate(cartEntryModel, orderEntryDto.getShipFrom());
			orderEntryDto.setIsbn(product.getIsbn());
			orderEntryDtos.add(orderEntryDto);
		}
	}

}
