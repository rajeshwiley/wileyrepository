package com.wiley.integrations.esb.transformer;

import de.hybris.platform.util.PriceValue;

import javax.annotation.Nonnull;

import org.springframework.util.Assert;

import com.wiley.integrations.esb.dto.PriceResponseDto;


/**
 * Created by Raman_Hancharou on 5/11/2016.
 */
public class PriceDtoToPriceValueTransformer
{
	@Nonnull
	public PriceValue transform(@Nonnull final PriceResponseDto priceResponseDto)
	{
		Assert.notNull(priceResponseDto);

		return new PriceValue(priceResponseDto.getCurrencyIso(), priceResponseDto.getPrice(), priceResponseDto.getNetto());
	}
}
