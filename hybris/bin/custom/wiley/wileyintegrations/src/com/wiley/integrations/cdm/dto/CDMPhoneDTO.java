/**
 *
 */
package com.wiley.integrations.cdm.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.fasterxml.jackson.annotation.JsonInclude;




@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CDMPhoneDTO {

  private final String type = "Phone";

  private String phoneTypeCode;

  private String phoneNumber;

  /**
   * @return the phoneTypeCode
   */
  public String getPhoneTypeCode() {
    return phoneTypeCode;
  }

  /**
   * @param phoneTypeCode
   *          the phoneTypeCode to set
   */
  public void setPhoneTypeCode(final String phoneTypeCode) {
    this.phoneTypeCode = phoneTypeCode;
  }

  /**
   * @return the phoneNumber
   */
  public String getPhoneNumber() {
    return phoneNumber;
  }

  /**
   * @param phoneNumber
   *          the phoneNumber to set
   */
  public void setPhoneNumber(final String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  /**
   * @return the type
   */
  public String getType() {
    return type;
  }


  @Override
  public String toString() {
    return "CDMPhoneDTO [phoneTypeCode=" + phoneTypeCode + ", phoneNumber=" + phoneNumber + "]";
  }



}
