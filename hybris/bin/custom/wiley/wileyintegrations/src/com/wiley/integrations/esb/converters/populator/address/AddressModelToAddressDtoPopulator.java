package com.wiley.integrations.esb.converters.populator.address;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.annotation.Nonnull;

import com.wiley.integrations.esb.dto.common.Address;

import org.springframework.util.Assert;


/**
 * Populate Address Dto base on AddressModel
 */
public class AddressModelToAddressDtoPopulator implements Populator<AddressModel, Address>
{
	@Override
	public void populate(@Nonnull final AddressModel addressModel, @Nonnull final Address address) throws ConversionException
	{
		Assert.notNull(addressModel);
		Assert.notNull(address);

		address.setAddressId(addressModel.getExternalId());
		address.setPostcode(addressModel.getPostalcode());

		final CountryModel country = addressModel.getCountry();
		if (country != null)
		{
			address.setCountry(country.getIsocode());
		}

		final RegionModel region = addressModel.getRegion();
		if (region != null)
		{
			address.setState(region.getIsocodeShort());
		}

		address.setCity(addressModel.getTown());

		address.setLine1(addressModel.getLine1());
		address.setLine2(addressModel.getLine2());
		address.setPhoneNumber(addressModel.getPhone1());
	}
}
