package com.wiley.integrations.eloqua.util;

import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Value;


/**
 * Creates http client for eloqua rest requests.
 *
 * @author Aliaksei_Zlobich
 */
public class HttpClientFactoryBean implements FactoryBean<HttpClient>
{

	@Value("${eloqua.credentials.username}")
	private String username;

	@Value("${eloqua.credentials.password}")
	private String password;

	@Override
	public HttpClient getObject() throws Exception
	{
		final BasicCredentialsProvider credentialsProvider = new BasicCredentialsProvider();
		credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(username, password));

		return HttpClients.custom()
				.useSystemProperties()
				.setDefaultCredentialsProvider(credentialsProvider)
				.build();
	}

	@Override
	public Class<?> getObjectType()
	{
		return HttpClient.class;
	}

	@Override
	public boolean isSingleton()
	{
		return true;
	}
}
