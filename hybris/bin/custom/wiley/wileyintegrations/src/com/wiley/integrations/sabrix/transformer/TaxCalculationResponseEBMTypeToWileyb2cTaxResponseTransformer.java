package com.wiley.integrations.sabrix.transformer;

import java.math.BigDecimal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wiley.core.integration.sabrix.dto.Wileyb2cTaxResponse;
import com.wiley.core.externaltax.services.impl.TaxSystemResponseError;
import com.wiley.integrations.sabrix.dto.OutdataErrorType;
import com.wiley.integrations.sabrix.dto.OutdataInvoiceType;
import com.wiley.integrations.sabrix.dto.OutdataRequestStatusType;
import com.wiley.integrations.sabrix.dto.OutdataType;
import com.wiley.integrations.sabrix.dto.StatusType;
import com.wiley.integrations.sabrix.dto.TaxCalculationResponseEBMType;


public class TaxCalculationResponseEBMTypeToWileyb2cTaxResponseTransformer
{
	private static final Logger LOG = LoggerFactory.getLogger(
			TaxCalculationResponseEBMTypeToWileyb2cTaxResponseTransformer.class);

	public Wileyb2cTaxResponse transform(final TaxCalculationResponseEBMType sabrixResponse)
	{

		if (sabrixResponse.getServiceResponse().getServiceResponseStatus() != StatusType.SUCCESS)
		{
			handleServiceResponseError(sabrixResponse);
			throw new RuntimeException("Sabrix call failed");
		}

		OutdataType outdata = sabrixResponse.getTaxCalculationResponse().getOUTDATA();
		OutdataRequestStatusType requestStatus = outdata.getREQUESTSTATUS();
		if (!requestStatus.isISPARTIALSUCCESS() || !requestStatus.isISSUCCESS())
		{
			handleTaxCalculationResponseError(outdata.getREQUESTSTATUS().getERROR());
		}

		Wileyb2cTaxResponse response = new Wileyb2cTaxResponse();
		BigDecimal totalOrderTax = BigDecimal.ZERO;
		for (OutdataInvoiceType invoice : outdata.getINVOICE())
		{
			totalOrderTax = totalOrderTax.add(invoice.getTOTALTAXAMOUNT());
		}
		response.setOrderTotalTax(totalOrderTax.doubleValue());
		return response;
	}

	private void handleTaxCalculationResponseError(final List<OutdataErrorType> responseWithError)
	{
		String errorMessage = String.format("Sabrix tax calculation failed.The detailed error is: %s",
				makeDetailedTaxErrorFromTaxCalculationResponse(responseWithError));
		LOG.error(errorMessage);
		throw new TaxSystemResponseError(errorMessage);
	}

	private String makeDetailedTaxErrorFromTaxCalculationResponse(final List<OutdataErrorType> errors)
	{
		StringBuilder sb = new StringBuilder();
		for (OutdataErrorType outdataErrorType : errors)
		{
			sb.append("Error code: ").append(outdataErrorType.getCODE()).append(" Error description: ").append(
					outdataErrorType.getDESCRIPTION());
		}
		return sb.toString();
	}

	private void handleServiceResponseError(final TaxCalculationResponseEBMType responseWithError)
	{
		String errorMessage = String.format("Sabrix tax calculation failed. The reason is %s. The detailed error is: %s",
				responseWithError.getServiceResponse().getServiceResponseMesg(),
				makeDetailedErrorFromServiceResponse(responseWithError));
		LOG.error(errorMessage);
		throw new TaxSystemResponseError(errorMessage);
	}

	private String makeDetailedErrorFromServiceResponse(final TaxCalculationResponseEBMType responseWithError)
	{
		StringBuilder sb = new StringBuilder();
		if (responseWithError.getErrorResponse() != null)
		{
			if (responseWithError.getErrorResponse().getErrorCode() != null)
			{
				sb.append("ErrorCode: ").append(responseWithError.getErrorResponse().getErrorCode());
			}
			if (responseWithError.getErrorResponse().getErrorDescription() != null)
			{
				sb.append(" ErrorDescription: ").append(responseWithError.getErrorResponse().getErrorDescription());
			}
		}
		return sb.toString();
	}

}
