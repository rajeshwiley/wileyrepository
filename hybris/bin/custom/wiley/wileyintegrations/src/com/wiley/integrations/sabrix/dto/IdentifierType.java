package com.wiley.integrations.sabrix.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.NormalizedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for IdentifierType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="IdentifierType"&gt;
 *   &lt;simpleContent&gt;
 *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema&gt;normalizedString"&gt;
 *       &lt;attribute name="schemeAgencyID" type="{http://www.w3.org/2001/XMLSchema}normalizedString" /&gt;
 *       &lt;attribute name="schemeAgencyName" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="schemeID" type="{http://www.w3.org/2001/XMLSchema}normalizedString" /&gt;
 *       &lt;attribute name="schemeName" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="schemeVersionID" type="{http://www.w3.org/2001/XMLSchema}normalizedString" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/simpleContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IdentifierType", namespace = "http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1", propOrder = {
		"value"
})
public class IdentifierType
{

	@XmlValue
	@XmlJavaTypeAdapter(NormalizedStringAdapter.class)
	@XmlSchemaType(name = "normalizedString")
	protected String value;
	@XmlAttribute(name = "schemeAgencyID")
	@XmlJavaTypeAdapter(NormalizedStringAdapter.class)
	@XmlSchemaType(name = "normalizedString")
	protected String schemeAgencyID;
	@XmlAttribute(name = "schemeAgencyName")
	protected String schemeAgencyName;
	@XmlAttribute(name = "schemeID")
	@XmlJavaTypeAdapter(NormalizedStringAdapter.class)
	@XmlSchemaType(name = "normalizedString")
	protected String schemeID;
	@XmlAttribute(name = "schemeName")
	protected String schemeName;
	@XmlAttribute(name = "schemeVersionID")
	@XmlJavaTypeAdapter(NormalizedStringAdapter.class)
	@XmlSchemaType(name = "normalizedString")
	protected String schemeVersionID;

	/**
	 * Gets the value of the value property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getValue()
	{
		return value;
	}

	/**
	 * Sets the value of the value property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setValue(String value)
	{
		this.value = value;
	}

	/**
	 * Gets the value of the schemeAgencyID property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getSchemeAgencyID()
	{
		return schemeAgencyID;
	}

	/**
	 * Sets the value of the schemeAgencyID property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setSchemeAgencyID(String value)
	{
		this.schemeAgencyID = value;
	}

	/**
	 * Gets the value of the schemeAgencyName property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getSchemeAgencyName()
	{
		return schemeAgencyName;
	}

	/**
	 * Sets the value of the schemeAgencyName property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setSchemeAgencyName(String value)
	{
		this.schemeAgencyName = value;
	}

	/**
	 * Gets the value of the schemeID property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getSchemeID()
	{
		return schemeID;
	}

	/**
	 * Sets the value of the schemeID property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setSchemeID(String value)
	{
		this.schemeID = value;
	}

	/**
	 * Gets the value of the schemeName property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getSchemeName()
	{
		return schemeName;
	}

	/**
	 * Sets the value of the schemeName property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setSchemeName(String value)
	{
		this.schemeName = value;
	}

	/**
	 * Gets the value of the schemeVersionID property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getSchemeVersionID()
	{
		return schemeVersionID;
	}

	/**
	 * Sets the value of the schemeVersionID property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setSchemeVersionID(String value)
	{
		this.schemeVersionID = value;
	}

}
