package com.wiley.integrations.order.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.util.TaxValue;

import com.wiley.integrations.order.dto.TaxValueWsDTO;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


public class WileyTaxValueWsPopulator implements Populator<TaxValue, TaxValueWsDTO>
{
	@Override
	public void populate(final TaxValue source, final TaxValueWsDTO target) throws ConversionException
	{
		validateParameterNotNullStandardMessage("source", source);
		validateParameterNotNullStandardMessage("target", target);

		target.setCode(source.getCode());
		target.setValue(source.getValue());
		target.setCurrency(source.getCurrencyIsoCode());
	}
}
