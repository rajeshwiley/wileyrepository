package com.wiley.integrations.customer.transformer;

import com.wiley.integrations.customer.converters.populator.CustomerToProfileUpdateRequestPopulator;
import com.wiley.integrations.customer.dto.ProfileUpdateRequest;
import de.hybris.platform.core.model.user.CustomerModel;
import org.springframework.messaging.Message;

import javax.annotation.Nonnull;
import javax.annotation.Resource;


public class CustomerToProfileUpdateRequestTransformer
{
	@Resource
	private CustomerToProfileUpdateRequestPopulator customerToProfileUpdateRequestPopulator;

	public ProfileUpdateRequest transform(@Nonnull final Message<CustomerModel> message)
	{
 		final ProfileUpdateRequest profileUpdateRequest = new ProfileUpdateRequest();
		customerToProfileUpdateRequestPopulator.populate(message.getPayload(), profileUpdateRequest);

		return profileUpdateRequest;
	}
}
