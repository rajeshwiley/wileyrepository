package com.wiley.integrations.mpgs.transformer;



import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;

import com.wiley.core.mpgs.dto.capture.MPGSCaptureResponseDTO;
import com.wiley.core.mpgs.response.WileyCaptureResponse;
import com.wiley.core.mpgs.services.WileyTransformationService;


public class WileyMPGSCaptureResponseTransformer
{
	@Autowired
	private WileyTransformationService wileyTransformationService;

	public WileyCaptureResponse transform(@Nonnull final Message<MPGSCaptureResponseDTO> message)
	{
		WileyCaptureResponse captureResult = new WileyCaptureResponse();

		MPGSCaptureResponseDTO responseDto = message.getPayload();
		captureResult.setStatus(wileyTransformationService.transformStatusIfSuccessful(responseDto.getResult()));
		captureResult.setStatusDetails(responseDto.getResponse().getGatewayCode());

		captureResult.setTransactionAmount(responseDto.getTransaction().getAmount());
		captureResult.setTransactionId(responseDto.getTransaction().getId());
		captureResult.setTransactionCurrency(responseDto.getTransaction().getCurrency());

		final String transactionTime = responseDto.getTimeOfRecord();
		if (transactionTime != null)
		{
			captureResult.setTransactionCreatedTime(wileyTransformationService.transformStringToDate(transactionTime));
		}
		return captureResult;
	}
}