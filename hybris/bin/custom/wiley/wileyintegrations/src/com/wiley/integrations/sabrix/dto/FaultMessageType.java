package com.wiley.integrations.sabrix.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Fault Message for SOAP faults
 * FaultName - End user friendly fault name
 * FaultCode - Application or Product specific codes
 * FaultText - End user friendly fault Message
 * Severity - CRITICAL, ERROR, WARN, INFO
 *
 *
 * <p>Java class for FaultMessageType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="FaultMessageType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="FaultName" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="FaultCode" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="FaultText" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1}StringType" minOccurs="0"/&gt;
 *         &lt;element name="Severity" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/CommonInfratypes/Codelist/V1}SeverityType" minOccurs="0"/&gt;
 *         &lt;element name="Stack" type="{http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/DataTypes/V1}StringType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FaultMessageType", namespace = "http://xmlns.wiley.com/EnterpriseBusinessObjects/Common/Meta/V1", propOrder = {
		"faultName",
		"faultCode",
		"faultText",
		"severity",
		"stack"
})
public class FaultMessageType
{

	@XmlElement(name = "FaultName")
	protected String faultName;
	@XmlElement(name = "FaultCode")
	protected String faultCode;
	@XmlElement(name = "FaultText")
	protected String faultText;
	@XmlElement(name = "Severity")
	@XmlSchemaType(name = "string")
	protected SeverityType severity;
	@XmlElement(name = "Stack")
	protected String stack;

	/**
	 * Gets the value of the faultName property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getFaultName()
	{
		return faultName;
	}

	/**
	 * Sets the value of the faultName property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setFaultName(String value)
	{
		this.faultName = value;
	}

	/**
	 * Gets the value of the faultCode property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getFaultCode()
	{
		return faultCode;
	}

	/**
	 * Sets the value of the faultCode property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setFaultCode(String value)
	{
		this.faultCode = value;
	}

	/**
	 * Gets the value of the faultText property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getFaultText()
	{
		return faultText;
	}

	/**
	 * Sets the value of the faultText property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setFaultText(String value)
	{
		this.faultText = value;
	}

	/**
	 * Gets the value of the severity property.
	 *
	 * @return possible object is
	 * {@link SeverityType }
	 */
	public SeverityType getSeverity()
	{
		return severity;
	}

	/**
	 * Sets the value of the severity property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link SeverityType }
	 */
	public void setSeverity(SeverityType value)
	{
		this.severity = value;
	}

	/**
	 * Gets the value of the stack property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getStack()
	{
		return stack;
	}

	/**
	 * Sets the value of the stack property.
	 *
	 * @param value
	 * 		allowed object is
	 * 		{@link String }
	 */
	public void setStack(String value)
	{
		this.stack = value;
	}

}
