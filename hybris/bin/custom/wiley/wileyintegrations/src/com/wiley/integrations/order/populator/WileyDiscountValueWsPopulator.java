package com.wiley.integrations.order.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.promotionengineservices.model.AbstractRuleBasedPromotionActionModel;
import de.hybris.platform.promotionengineservices.promotionengine.report.dao.RuleBasedPromotionActionDao;
import de.hybris.platform.ruleengine.model.AbstractRuleEngineRuleModel;
import de.hybris.platform.ruleengineservices.model.AbstractRuleModel;
import de.hybris.platform.ruleengineservices.rule.services.RuleService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.util.DiscountValue;

import java.util.ArrayList;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wiley.core.discount.WileyExternalDiscountService;
import com.wiley.integrations.order.dto.DiscountValueWsDTO;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


public class WileyDiscountValueWsPopulator implements Populator<DiscountValue, DiscountValueWsDTO>
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyDiscountValueWsPopulator.class);

	@Resource
	private WileyExternalDiscountWsPopulator wileyExternalDiscountWsPopulator;

	@Resource
	private WileyExternalDiscountService wileyExternalDiscountService;

	@Resource
	private RuleBasedPromotionActionDao ruleBasedPromotionActionDao;

	@Resource
	private RuleService ruleService;

	@Override
	public void populate(final DiscountValue source, final DiscountValueWsDTO target) throws ConversionException
	{
		validateParameterNotNullStandardMessage("source", source);
		validateParameterNotNullStandardMessage("target", target);

		if (wileyExternalDiscountService.isWileyExternalDiscountExistedForGuid(source.getCode()))
		{
			wileyExternalDiscountWsPopulator.populate(
					wileyExternalDiscountService.getWileyExternalDiscountForGuid(source.getCode()), target);
		}
		else if (isPromotionActionExistedForGuid(source.getCode()))
		{
			populateFieldsFromDiscountValue(source, target);

			final AbstractRuleBasedPromotionActionModel action =
					ruleBasedPromotionActionDao.findRuleBasedPromotionByGuid(source.getCode());
			if (action.getUsedCouponCodes() != null)
			{
				target.setCouponCodes(new ArrayList<>(action.getUsedCouponCodes()));
			}

			final AbstractRuleModel rule = getPromotionRule(action);
			target.setPromoTypeGroup(rule.getTypeGroup());
			target.setPromoInstitution(rule.getInstitution());
		}
		else
		{
			populateFieldsFromDiscountValue(source, target);
		}
	}

	void populateFieldsFromDiscountValue(final DiscountValue source, final DiscountValueWsDTO target)
	{
		target.setCode(source.getCode());
		target.setValue(source.getValue());
		target.setCurrency(source.getCurrencyIsoCode());
		target.setAbsolute(source.isAbsolute());
	}

	private AbstractRuleModel getPromotionRule(final AbstractRuleBasedPromotionActionModel action)
	{
		final AbstractRuleEngineRuleModel abstractRuleEngineRule = action.getRule();
		return ruleService.getRuleForCode(abstractRuleEngineRule.getCode());
	}

	private boolean isPromotionActionExistedForGuid(final String guid)
	{
		try
		{
			return ruleBasedPromotionActionDao.findRuleBasedPromotionByGuid(guid) != null;
		}
		catch (UnknownIdentifierException | AmbiguousIdentifierException e)
		{
			LOG.debug("Promotion action does not exist for GUID '{}': {}", guid, e.getMessage());
			return false;
		}
	}
}
