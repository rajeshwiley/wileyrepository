package com.wiley.integrations.ediintegration.endpoints.generator;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.log4j.Logger;
import org.springframework.integration.file.FileNameGenerator;
import org.springframework.messaging.Message;

import com.wiley.integrations.ediintegration.endpoints.jaxb.Routing;

//TODO: [YG]Tests
public class WileyToEdiFilenameGenerator implements FileNameGenerator
{
	private static final Logger LOG = Logger.getLogger(WileyToEdiFilenameGenerator.class);

	private static final String ROUTING_HEADER_KEY = "routing";
	private static final String DOT = ".";
	private static final String UNDERSCORE = "_";
	private static final String PAYPAL_FILENAME_IDENTIFIER = "PP";

	private String extension;
	private String prefixDateFormat;
	private String bodyDateFormat;

	@Override
	public String generateFileName(final Message<?> message)
	{
		final Routing routing = (Routing) message.getHeaders().get(ROUTING_HEADER_KEY);
		return buildEdiFileName(routing);
	}

	private String buildEdiFileName(final Routing routing)
	{
		//YYYMMDD.TransacitionBatchNumber.WEL_YYYY-MM-dd-HH-mm-ss.new.xml
		//for PayPal PP part should be added - YYYMMDD.TransacitionBatchNumber.WEL_PP_YYYY-MM-dd-HH-mm-ss.new.xml
		LocalDateTime localDate = routing.getLocalDate();
		String filePrefixStringFormat = DateTimeFormatter.ofPattern(getPrefixDateFormat()).format(localDate);
		String bodyDate = DateTimeFormatter.ofPattern(getBodyDateFormat()).format(localDate);

		StringBuilder filename = new StringBuilder(filePrefixStringFormat)
				.append(DOT)
				.append(routing.getTransactionBatchNumber())
				.append(DOT)
				.append(routing.getSiteShortcut().toUpperCase())
				.append(UNDERSCORE);
		if (routing.isPayPalPayments()) {
			filename.append(PAYPAL_FILENAME_IDENTIFIER).append(UNDERSCORE);
		}
		filename.append(bodyDate)
			.append(DOT)
			.append(getExtension());

		LOG.info(String.format("Generating edi file with name: [%s]", filename));
		return filename.toString();
	}

	public String getExtension()
	{
		return extension;
	}

	public void setExtension(final String extension)
	{
		this.extension = extension;
	}

	public String getPrefixDateFormat()
	{
		return prefixDateFormat;
	}

	public void setPrefixDateFormat(final String prefixDateFormat)
	{
		this.prefixDateFormat = prefixDateFormat;
	}

	public String getBodyDateFormat()
	{
		return bodyDateFormat;
	}

	public void setBodyDateFormat(final String bodyDateFormat)
	{
		this.bodyDateFormat = bodyDateFormat;
	}
}
