package com.wiley.integrations.eloqua.transformer;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;

import java.util.ArrayList;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import com.wiley.core.integration.eloqua.dto.CustomObjectDataDto;


/**
 * Transforms OrderEntry to CustomObjectData.
 *
 * @author Aliaksei_Zlobich
 */
public class OrderEntryToCustomObjectDataTransformer
{

	private static final Logger LOG = LoggerFactory.getLogger(OrderEntryToCustomObjectDataTransformer.class);

	@Resource
	private Populator<CustomerModel, CustomObjectDataDto> customerToCustomObjectDataPopulator;

	@Resource
	private Populator<OrderEntryModel, CustomObjectDataDto> orderEntryToCustomObjectDataPopulator;

	@Resource
	private Populator<OrderModel, CustomObjectDataDto> orderToCustomObjectDataPopulator;

	@Nonnull
	public CustomObjectDataDto transform(@Nonnull final OrderEntryModel orderEntry)
	{
		Assert.notNull(orderEntry);
		LOG.debug("Method params: orderEntry [{}] with PK [{}].", orderEntry.getEntryNumber(), orderEntry.getPk());

		final OrderModel order = orderEntry.getOrder();
		final UserModel user = order.getUser();

		Assert.notNull(order, "OrderEntry should be assigned to order.");

		if (!(user instanceof CustomerModel))
		{
			throw new IllegalStateException("Order should be assigned to Customer.");
		}

		CustomerModel customer = (CustomerModel) user;

		CustomObjectDataDto cdo = new CustomObjectDataDto();
		cdo.setFieldValues(new ArrayList<>());

		orderToCustomObjectDataPopulator.populate(order, cdo);
		customerToCustomObjectDataPopulator.populate(customer, cdo);
		orderEntryToCustomObjectDataPopulator.populate(orderEntry, cdo);

		return cdo;
	}

}
