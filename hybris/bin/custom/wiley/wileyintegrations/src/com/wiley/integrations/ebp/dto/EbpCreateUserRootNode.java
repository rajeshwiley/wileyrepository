package com.wiley.integrations.ebp.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.google.common.base.MoreObjects;


/**
 * Created by Uladzimir_Barouski on 2/17/2016.
 */
@XmlRootElement(namespace = "www.efficientlearning.com/wsdl", name = "createUser")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createUser")
public class EbpCreateUserRootNode
{

	@XmlElement(required = true, name = "arg0")
	private CreateUserArgNode arg;

	public CreateUserArgNode getArg()
	{
		return arg;
	}

	public void setArg(final CreateUserArgNode arg)
	{
		this.arg = arg;
	}

	@Override
	public String toString()
	{
		return MoreObjects.toStringHelper(this)
			.add("arg", arg)
			.toString();
	}
}
