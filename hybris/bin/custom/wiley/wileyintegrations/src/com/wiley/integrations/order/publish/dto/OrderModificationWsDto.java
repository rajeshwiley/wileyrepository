package com.wiley.integrations.order.publish.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.wiley.integrations.order.dto.OrderWsDTO;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderModificationWsDto implements Serializable
{
    private OrderWsDTO previousState;

    @NotNull
    private OrderWsDTO currentState;

    public OrderWsDTO getPreviousState()
    {
        return previousState;
    }

    public void setPreviousState(final OrderWsDTO previousState)
    {
        this.previousState = previousState;
    }

    public OrderWsDTO getCurrentState()
    {
        return currentState;
    }

    public void setCurrentState(final OrderWsDTO currentState)
    {
        this.currentState = currentState;
    }
}
