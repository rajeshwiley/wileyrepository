package com.wiley.integrations.ediintegration.endpoints.transformers.converters;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TruncatableTest
{
	public static final String KEY_1 = "key1";
	public static final Integer KEY_1_VALUE = 15;
	private Truncatable testInstance = () -> {
		Map<String, Integer> truncatableMap = new HashMap<>();
		truncatableMap.put(KEY_1, KEY_1_VALUE);
		return truncatableMap;
	};


	@Test
	public void shouldTruncateFieldUpToRequiredLength() throws Exception
	{
		//Given
		String tooLongString = UUID.randomUUID().toString();
		assert tooLongString.length() > KEY_1_VALUE;
		//Testing
		final String result = testInstance.truncate(tooLongString, KEY_1);
		//Verify
		assertTrue(result.length() == KEY_1_VALUE);
		assertTrue(tooLongString.indexOf(result) == 0);
	}


	@Test
	public void shouldNotTruncateFieldIfLengthIsOk() throws Exception
	{
		//Given
		String goodString = "goodString";
		assert goodString.length() < KEY_1_VALUE;
		//Testing
		final String result = testInstance.truncate(goodString, KEY_1);
		//Verify
		assertEquals(goodString, result);
	}

	@Test
	public void shouldReturnValueIfTruncatableMapIsNull() throws Exception
	{
		//Given
		Truncatable testInstanceWithNullMap = () -> null;
		String testValue = "goodString";
		//Testing
		final String result = testInstanceWithNullMap.truncate(testValue, KEY_1);
		//Verify
		assertEquals(testValue, result);
	}

	@Test
	public void shouldReturnValueIfTruncatableValueIsLessThan1() throws Exception
	{
		//Given
		Truncatable testInstance = () -> {
			Map<String, Integer> truncatableMap = new HashMap<>();
			truncatableMap.put(KEY_1, 0);
			return truncatableMap;
		};
		String testValue = "goodString";
		//Testing
		final String result = testInstance.truncate(testValue, KEY_1);
		//Verify
		assertEquals(testValue, result);
	}
}