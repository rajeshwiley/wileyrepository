package com.wiley.integrations.wpg.transformer;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.dto.TransactionStatusDetails;
import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.payment.enums.WileyTransactionStatusEnum;
import com.wiley.core.payment.response.WileyRefundResult;
import com.wiley.integrations.wpg.dto.TokenRefundPaymentResponse;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class FollowOnRefundResponseTransformerTest
{
	public static final String TRANSACTION_ID = "transactionId";
	private static final String TOTAL_AMOUNT_STRING = "34.45";
	public static final String MERCHANT_RESPONSE = "merchantResponse";

	private FollowOnRefundResponseTransformer testedInstance = new FollowOnRefundResponseTransformer();

	private TokenRefundPaymentResponse source = new TokenRefundPaymentResponse();
	private WileyRefundResult target = new WileyRefundResult();

	@Test
	public void shouldSetSuccessfulStatus()
	{
		givenSuccessfulReturnCode();

		target = testedInstance.transform(source);

		assertEquals(TransactionStatus.ACCEPTED, target.getTransactionStatus());
		assertEquals(TransactionStatusDetails.SUCCESFULL, target.getTransactionStatusDetails());
	}

	@Test
	public void shouldSetErrorStatusWhenReturnCodeIsNull()
	{
		source.setReturnCode(null);

		target = testedInstance.transform(source);

		assertEquals(TransactionStatus.REJECTED, target.getTransactionStatus());
		assertEquals(TransactionStatusDetails.BANK_DECLINE, target.getTransactionStatusDetails());
	}

	@Test
	public void shouldSetErrorStatusWhenReturnCodeIsNotSuccessful()
	{
		Integer returnCode = Integer.parseInt(WileyTransactionStatusEnum.DECLINED.getCode());
		source.setReturnCode(returnCode);

		target = testedInstance.transform(source);

		assertEquals(TransactionStatus.REJECTED, target.getTransactionStatus());
		assertEquals(TransactionStatusDetails.BANK_DECLINE, target.getTransactionStatusDetails());
	}

	@Test
	public void shouldSetErrorStatusWhenReturnCodeIsUnexpected()
	{
		source.setReturnCode(Integer.MAX_VALUE);

		target = testedInstance.transform(source);

		assertEquals(TransactionStatus.REJECTED, target.getTransactionStatus());
		assertEquals(TransactionStatusDetails.BANK_DECLINE, target.getTransactionStatusDetails());
	}

	@Test
	public void shouldSetRequestId()
	{
		source.setTransId(TRANSACTION_ID);

		target = testedInstance.transform(source);

		assertEquals(TRANSACTION_ID, target.getRequestId());
	}

	@Test
	public void shouldSetTotalAmount()
	{
		source.setValue(TOTAL_AMOUNT_STRING);

		target = testedInstance.transform(source);

		assertEquals(new BigDecimal(TOTAL_AMOUNT_STRING), target.getTotalAmount());
	}

	@Test
	public void shouldSetMerchantResponse()
	{
		source.setMerchantResponse(MERCHANT_RESPONSE);

		target = testedInstance.transform(source);

		assertEquals(MERCHANT_RESPONSE, target.getMerchantResponse());
	}

	@Test
	public void shouldSetTransactionStatus()
	{
		givenSuccessfulReturnCode();

		target = testedInstance.transform(source);

		assertEquals(WileyTransactionStatusEnum.SUCCESS, target.getWpgTransactionStatus());
	}

	private void givenSuccessfulReturnCode()
	{
		Integer successfulCode = Integer.parseInt(WileyTransactionStatusEnum.SUCCESS.getCode());
		source.setReturnCode(successfulCode);
	}
}