package com.wiley.integrations.esb.converters.populator.order;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

import java.util.Arrays;
import java.util.Date;
import java.util.Map;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.paypal.hybris.model.PaypalPaymentInfoModel;
import com.wiley.core.enums.PaymentModeEnum;
import com.wiley.core.model.SchoolModel;
import com.wiley.core.order.WileyCheckoutService;
import com.wiley.core.regCode.service.Wileyb2cRegCodeService;
import com.wiley.integrations.esb.dto.order.OrderRequest;

import static com.wiley.core.enums.OrderType.FREE_TRIAL;
import static com.wiley.integrations.esb.dto.common.PaymentOptionCode.CARD;
import static com.wiley.integrations.esb.dto.common.PaymentOptionCode.INVOICE;
import static com.wiley.integrations.esb.dto.common.PaymentOptionCode.PAYPAL;
import static de.hybris.platform.commerceservices.enums.CustomerType.GUEST;
import static java.util.Collections.singletonList;


/**
 * Unit test for {@link com.wiley.integrations.esb.converters.populator.order.OrderModelToOrderRequestSimpleFieldsPopulator}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class OrderModelToOrderRequestSimpleFieldsPopulatorTest
{
	private static final String GET_CODE = "GET_CODE";
	private static final String GET_SCHOOL_ID = "GET_SCHOOL_ID";
	private static final String GET_UID = "GET_UID";
	private static final String GET_CUSTOMER_ID = "GET_CUSTOMER_ID";
	private static final String GET_CONTACT_EMAIL = "GET_CONTACT_EMAIL";
	private static final String GET_CURRENCY_ISOCODE = "getCurrencyIsocode";
	private static final String GET_COUNTRY_ISOCODE = "getCountryIsocode";
	private static final String GET_LANGUAGE_ISOCODE = "getLanguageIsocode";
	private static final String GET_SAP_ACCOUNT_NUMBER = "getSapAccountNumber";
	private static final String GET_B2B_SAP_ACCOUNT_NUMBER = "getB2BSapAccountNumber";
	private static final String GET_SITE_UID = "getSiteUid";
	private static final String GET_REG_CODE = "getRegCode";
	private static final String GET_DELIVERY_OPTION_CODE = "getDeliveryOptionCode";
	private static final String GET_SUBSCRIPTION_ID = "getSubscriptionId";
	private static final String GET_PAYPAL_TOKEN = "getPaypalToken";
	private static final String REQUEST_ID = "123654";
	private static final String WPG_AUTH_CODE = "WPG_AUTH_CODE";
	private static final String ECID_NUMBER = "ECID_NUMBER";

	private static final Double GET_TOTAL_DISCOUNTS = 16.0;
	private static final Double GET_DELIVERY_COST = 17.0;
	private static final Double GET_TOTAL_TAX = 1.15;
	private static final Double GET_TOTAL_PRICE = 1165.89;
	private static final String TOTAL_PRICE_WITH_TAX = "1167.04";

	private static final Date DATE = new Date();
	public static final String CREDIT_CARD_PAYMENT_INFO = "CreditCardPaymentInfo";
	public static final String PAYPAL_PAYMENT_INFO = "PaypalPaymentInfo";
	public static final String INVOICE_PAYMENT_INFO = "InvoicePaymentInfo";


	private OrderRequest orderRequest;

	@InjectMocks
	private OrderModelToOrderRequestSimpleFieldsPopulator populator = new OrderModelToOrderRequestSimpleFieldsPopulator();

	@Mock
	private OrderModel orderModelMock;

	@Mock
	private UserModel userModelMock;

	@Mock
	private CustomerModel customerModelMock;

	@Mock
	private B2BCustomerModel b2BCustomerModelMock;

	@Mock
	private CurrencyModel currencyModelMock;

	@Mock
	private B2BUnitModel b2BUnitModelMock;

	@Mock
	private BaseSiteModel baseSiteModelMock;

	@Mock
	private Wileyb2cRegCodeService wileyb2cRegCodeServiceMock;

	@Mock
	private CountryModel countryMock;

	@Mock
	private LanguageModel languageMock;

	@Mock
	private DeliveryModeModel deliveryModeMock;

	@Mock
	private PaymentInfoModel paymentInfoModelMock;

	@Mock
	private CreditCardPaymentInfoModel creditCardPaymentInfoModelMock;

	@Mock
	private PaypalPaymentInfoModel paypalPaymentInfoModelMock;

	@Mock
	private SchoolModel schoolModelMock;

	@Mock
	private PaymentTransactionEntryModel paymentTransactionEntryModelMock;

	@Mock
	private PaymentTransactionModel paymentTransactionModelMock;

	@Mock
	private WileyCheckoutService wileyCheckoutServiceMock;

	@Mock
	private Map<String, PaymentModeEnum> paymentModeMapMock;

	@Before
	public void setUp()
	{
		orderRequest = new OrderRequest();


		doReturn(GET_CODE).when(orderModelMock).getCode();

		doReturn(GET_TOTAL_DISCOUNTS).when(orderModelMock).getTotalDiscounts();

		doReturn(GET_DELIVERY_COST).when(orderModelMock).getDeliveryCost();

		doReturn(GET_TOTAL_TAX).when(orderModelMock).getTotalTax();

		doReturn(GET_TOTAL_PRICE).when(orderModelMock).getTotalPrice();

		doReturn(customerModelMock).when(orderModelMock).getUser();

		doReturn(FREE_TRIAL).when(orderModelMock).getOrderType();

		doReturn(Optional.of(GET_REG_CODE)).when(wileyb2cRegCodeServiceMock).getRegCodeForOrder(orderModelMock);

		//school
		doReturn(schoolModelMock).when(orderModelMock).getSchool();
		doReturn(GET_SCHOOL_ID).when(schoolModelMock).getCode();

		//country
		doReturn(countryMock).when(orderModelMock).getCountry();
		doReturn(GET_COUNTRY_ISOCODE).when(countryMock).getIsocode();

		//customer fields
		doReturn(GUEST).when(customerModelMock).getType();
		doReturn(GET_CONTACT_EMAIL).when(customerModelMock).getContactEmail();
		doReturn(GET_CUSTOMER_ID).when(customerModelMock).getCustomerID();
		doReturn(GET_SAP_ACCOUNT_NUMBER).when(customerModelMock).getSapAccountNumber();

		//currency fields
		doReturn(GET_CURRENCY_ISOCODE).when(currencyModelMock).getIsocode();
		doReturn(currencyModelMock).when(orderModelMock).getCurrency();

		//b2b unit
		doReturn(GET_B2B_SAP_ACCOUNT_NUMBER).when(b2BUnitModelMock).getSapAccountNumber();
		doReturn(b2BUnitModelMock).when(orderModelMock).getUnit();

		//base site
		doReturn(baseSiteModelMock).when(orderModelMock).getSite();
		doReturn(GET_SITE_UID).when(baseSiteModelMock).getUid();
		doReturn(SiteChannel.B2B).when(baseSiteModelMock).getChannel();

		//language
		doReturn(languageMock).when(orderModelMock).getLanguage();
		doReturn(GET_LANGUAGE_ISOCODE).when(languageMock).getIsocode();

		//deliveryMode
		doReturn(deliveryModeMock).when(orderModelMock).getDeliveryMode();
		doReturn(GET_DELIVERY_OPTION_CODE).when(deliveryModeMock).getCode();

		//paymentOptionCode
		doReturn(paymentInfoModelMock).when(orderModelMock).getPaymentInfo();
		doReturn(CREDIT_CARD_PAYMENT_INFO).when(paymentInfoModelMock).getItemtype();
		doReturn(PaymentModeEnum.CARD).when(paymentModeMapMock).get(CREDIT_CARD_PAYMENT_INFO);

		//transaction entry
		doReturn(PaymentTransactionType.AUTHORIZATION).when(paymentTransactionEntryModelMock).getType();
		doReturn(REQUEST_ID).when(paymentTransactionEntryModelMock).getRequestId();
		doReturn(WPG_AUTH_CODE).when(paymentTransactionEntryModelMock).getWpgAuthCode();
		doReturn(TransactionStatus.ACCEPTED.toString()).when(paymentTransactionEntryModelMock).getTransactionStatus();
		doReturn(Arrays.asList(paymentTransactionEntryModelMock)).when(paymentTransactionModelMock).getEntries();

		doReturn(DATE).when(orderModelMock).getDesiredShippingDate();

		doReturn(true).when(orderModelMock).getTaxCalculated();

		doReturn(ECID_NUMBER).when(customerModelMock).getEcidNumber();

		doReturn(true).when(wileyCheckoutServiceMock).isNonZeroOrder(orderModelMock);
	}

	@Test
	public void shouldPopulateAllFields()
	{
		// Given
		//@see setUp() method

		// When
		populate();

		// Then
		assertEquals(GET_CODE, orderRequest.getCode());
		assertEquals(GET_CONTACT_EMAIL, orderRequest.getUserId());
		assertEquals(true, orderRequest.getUserGuest());
		assertEquals(GET_CUSTOMER_ID, orderRequest.getImmutableUserId());
		assertEquals(SiteChannel.B2B.toString(), orderRequest.getSalesModel());
		assertEquals(GET_SITE_UID, orderRequest.getWebsite());
		assertEquals(FREE_TRIAL.toString(), orderRequest.getOrderType());
		assertEquals(GET_SCHOOL_ID, orderRequest.getSchoolId());
		assertEquals(GET_REG_CODE, orderRequest.getRegCode());
		assertEquals(GET_COUNTRY_ISOCODE, orderRequest.getCountry());
		assertEquals(GET_CURRENCY_ISOCODE, orderRequest.getCurrency());
		assertEquals(GET_LANGUAGE_ISOCODE, orderRequest.getLanguage());
		assertEquals(GET_DELIVERY_OPTION_CODE, orderRequest.getDeliveryOptionCode());
		assertEquals(CARD, orderRequest.getPaymentOptionCode());
		assertEquals(GET_TOTAL_DISCOUNTS, orderRequest.getTotalDiscount());
		assertEquals(GET_DELIVERY_COST, orderRequest.getDeliveryCost());
		assertEquals(GET_TOTAL_TAX, orderRequest.getTotalTax());
		assertEquals(TOTAL_PRICE_WITH_TAX, orderRequest.getTotalPrice().toString());
		assertEquals(DATE, orderRequest.getDesiredShippingDate());
		assertEquals(ECID_NUMBER, orderRequest.getIndividualEcidNumber());
		assertEquals(false, orderRequest.getTaxCalculationFailed());
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldFailOnNullModel()
	{
		// Given
		orderModelMock = null;

		// When
		populate();

		// Then
		//exception is thrown
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldFailOnNullRequest()
	{
		// Given
		orderRequest = null;

		// When
		populate();

		// Then
		//exception is thrown
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldFailOnNullCustomer()
	{
		// Given
		doReturn(null).when(orderModelMock).getUser();

		// When
		populate();

		// Then
		//exception is thrown
	}

	@Test
	public void shouldIdentifyTaxCalculationFailed()
	{
		// Given
		final double tax = 0.0;
		doReturn(tax).when(orderModelMock).getTotalTax();
		doReturn(false).when(orderModelMock).getTaxCalculated();

		// When
		populate();

		// Then
		assertTrue(orderRequest.getTaxCalculationFailed());
		//total price include zero taxes
		assertEquals(GET_TOTAL_PRICE, orderRequest.getTotalPrice().doubleValue(), Double.MIN_VALUE);
	}

	@Test
	public void shouldPopulateSapAccountNumberForB2bCustomer()
	{
		// Given
		doReturn(b2BCustomerModelMock).when(orderModelMock).getUser();

		// When
		populate();

		// Then
		assertEquals(GET_B2B_SAP_ACCOUNT_NUMBER, orderRequest.getSapAccountNumber());
	}

	@Test
	public void shouldPopulateSapAccountNumberForRegularCustomer()
	{
		// Given

		// When
		populate();

		// Then
		assertEquals(GET_SAP_ACCOUNT_NUMBER, orderRequest.getSapAccountNumber());
	}

	@Test
	public void shouldPopulatePaymentTokenCC()
	{
		// Given
		doReturn(creditCardPaymentInfoModelMock).when(orderModelMock).getPaymentInfo();
		doReturn(GET_SUBSCRIPTION_ID).when(creditCardPaymentInfoModelMock).getSubscriptionId();

		doReturn(CREDIT_CARD_PAYMENT_INFO).when(creditCardPaymentInfoModelMock).getItemtype();
		doReturn(PaymentModeEnum.CARD).when(paymentModeMapMock).get(CREDIT_CARD_PAYMENT_INFO);
		doReturn(de.hybris.platform.core.enums.CreditCardType.VISA).when(creditCardPaymentInfoModelMock).getType();

		doReturn(Arrays.asList(paymentTransactionModelMock)).when(orderModelMock).getPaymentTransactions();
		// When
		populate();

		// Then
		assertEquals(GET_SUBSCRIPTION_ID, orderRequest.getPaymentToken());
		assertEquals(com.wiley.integrations.esb.dto.common.CreditCardType.VISA, orderRequest.getCreditCardType());
		assertEquals(WPG_AUTH_CODE, orderRequest.getPaymentAuthorizationCode());
	}

	@Test
	public void shouldPopulatePaymentTokenPaypal()
	{
		// Given
		doReturn(paypalPaymentInfoModelMock).when(orderModelMock).getPaymentInfo();
		doReturn(GET_PAYPAL_TOKEN).when(paypalPaymentInfoModelMock).getToken();

		doReturn(PAYPAL_PAYMENT_INFO).when(paypalPaymentInfoModelMock).getItemtype();
		doReturn(PaymentModeEnum.PAYPAL).when(paymentModeMapMock).get(PAYPAL_PAYMENT_INFO);

		doReturn(Arrays.asList(paymentTransactionModelMock)).when(orderModelMock).getPaymentTransactions();

		// When
		populate();

		// Then
		assertEquals(GET_PAYPAL_TOKEN, orderRequest.getPaymentToken());
		assertEquals(null, orderRequest.getCreditCardType());
		assertEquals(REQUEST_ID, orderRequest.getPaymentAuthorizationCode());
	}

	@Test
	public void shouldNotFailInCaseOfZeroPriceCase()
	{
		// Given
		doReturn(false).when(wileyCheckoutServiceMock).isNonZeroOrder(orderModelMock);

		// When
		populate();

		// Then
		assertNull(orderRequest.getPaymentAuthorizationCode());
	}

	@Test(expected = IllegalStateException.class)
	public void shouldThrowExceptionInCaseOfMissedTransaction()
	{
		// Given
		final PaymentTransactionModel transactionMock = mock(PaymentTransactionModel.class);
		final PaymentTransactionEntryModel entryMock = mock(PaymentTransactionEntryModel.class);

		doReturn(creditCardPaymentInfoModelMock).when(orderModelMock).getPaymentInfo();
		doReturn(PaymentModeEnum.CARD).when(paymentModeMapMock).get(CREDIT_CARD_PAYMENT_INFO);
		doReturn(CREDIT_CARD_PAYMENT_INFO).when(creditCardPaymentInfoModelMock).getItemtype();
		doReturn(PaymentTransactionType.CREATE_SUBSCRIPTION).when(entryMock).getType();
		doReturn(singletonList(entryMock)).when(transactionMock).getEntries();
		doReturn(singletonList(transactionMock)).when(orderModelMock).getPaymentTransactions();

		// When
		populate();

		// Then
		// exception should be thrown
	}


	@Test
	public void shouldPopulatePaymentInfoModelInvoice()
	{
		// Given
		doReturn(paymentInfoModelMock).when(orderModelMock).getPaymentInfo();
		doReturn(INVOICE_PAYMENT_INFO).when(paymentInfoModelMock).getItemtype();
		doReturn(PaymentModeEnum.INVOICE).when(paymentModeMapMock).get(INVOICE_PAYMENT_INFO);

		// When
		populate();

		// Then
		assertEquals(INVOICE, orderRequest.getPaymentOptionCode());
	}

	@Test
	public void shouldPopulatePaymentInfoModelPaypal()
	{
		// Given
		doReturn(paymentInfoModelMock).when(orderModelMock).getPaymentInfo();
		doReturn(PAYPAL_PAYMENT_INFO).when(paymentInfoModelMock).getItemtype();
		doReturn(PaymentModeEnum.PAYPAL).when(paymentModeMapMock).get(PAYPAL_PAYMENT_INFO);

		// When
		populate();

		// Then
		assertEquals(PAYPAL, orderRequest.getPaymentOptionCode());
	}

	@Test
	public void shouldNotPopulatePaymentOptionAndCardTypeForZeroOrder()
	{
		// Given
		doReturn(null).when(orderModelMock).getPaymentInfo();
		doReturn(0.0d).when(orderModelMock).getTotalPrice();
		// When
		populate();

		// Then
		assertEquals(null, orderRequest.getPaymentOptionCode());
		assertEquals(null, orderRequest.getCreditCardType());

	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldFailForUnknownPaymentInfoModelType()
	{
		// Given
		doReturn(paymentInfoModelMock).when(orderModelMock).getPaymentInfo();
		doReturn("unknown payment info type").when(paymentInfoModelMock).getItemtype();
		doReturn(null).when(paymentModeMapMock).get("unknown payment info type");

		// When
		populate();

		// Then
		//exception is thrown
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldFailOnNullCountry()
	{
		// Given
		doReturn(null).when(orderModelMock).getCountry();

		// When
		populate();

		// Then
		//exception is thrown
	}

	@Test
	public void shouldPopulateIfUserIsNotGuest()
	{
		// Given
		doReturn(customerModelMock).when(orderModelMock).getUser();
		doReturn(null).when(customerModelMock).getType(); //not a guest
		doReturn(GET_UID).when(customerModelMock).getUid();

		// When
		populate();

		// Then
		assertEquals(GET_UID, orderRequest.getUserId());
	}


	private void populate()
	{
		populator.populate(orderModelMock, orderRequest);
	}
}