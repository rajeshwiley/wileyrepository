package com.wiley.integrations.wpg.transformer;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.payment.dto.TransactionStatus;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections.map.HashedMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.integration.support.MutableMessage;
import org.springframework.messaging.Message;

import com.wiley.core.payment.enums.WileyTransactionStatusEnum;
import com.wiley.core.payment.response.WileyCaptureResult;
import com.wiley.core.payment.strategies.SecurityHashGeneratorStrategy;
import com.wiley.integrations.wpg.populators.WileyPaymentResponsePopulator;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CaptureResponseTransformerTest
{
	private static final String SUCCESSFUL_RET_CODE = "0";
	private static final String FAILED_RET_CODE = "1";
	public static final String TEST_SITE_ID = "testSiteId";

	private static final String MERCHANT_RESPONSE = "merchantResponse";

	private static final String TRANS_ID = "transId";
	private static final String VALUE = "10.00";
	private static final String TIMESTAMP = "10002000";

	private static final String KEY_OPERATION = "operation";
	private static final String KEY_RETURNCODE = "returnCode";
	private static final String KEY_RETURNMESSAGE = "returnMessage";
	private static final String KEY_VENDORID = "vendorID";
	private static final String KEY_MERCHANTRESPONSE = "merchantResponse";
	private static final String KEY_MERCHANTID = "merchantID";
	private static final String KEY_CSCRESULT = "CSCResult";
	private static final String KEY_ADDR_RESULT = "AVSAddrResult";
	private static final String KEY_POST_RESULT = "AVSPostResult";

	private static final String KEY_TRANSID = "transID";
	private static final String KEY_VALUE = "value";
	private static final String KEY_CURRENCY = "currency";
	private static final String KEY_AUTHCODE = "authCode";

	private static final String KEY_ACQUIRERID = "acquirerID";
	private static final String KEY_ACQUIRERNAME = "acquirerName";
	private static final String KEY_BANKID = "bankID";
	private static final String KEY_BANKNAME = "bankName";
	private static final String KEY_MASKEDCARDNUMBER = "maskedCardNumber";
	private static final String KEY_CARDEXPIRY = "cardExpiry";
	private static final String KEY_TIMESTAMP = "timestamp";
	private static final String KEY_SECURITY = "security";
	private static final String CALCULATED_HASH = "calculatedHash";
	private static final String SITE_ID = "siteId";
	private static final String OPERATION = "operation";
	private static final String RETMESSAGE = "retmessage";
	private static final String VENDORID = "vendorid";
	private static final String CURRENCY = "currency";
	private static final String MERCHANT_ID = "merchantId";
	private static final String CS_CRESULT = "CSCresult";
	private static final String ADDR_RESULT = "addrResult";
	private static final String POST_RESULT = "postResult";
	private static final String AUTHCODE = "authcode";
	private static final String ACQ_ID = "acqId";
	private static final String ACQ_NAME = "acqName";
	private static final String BANK_ID = "bankId";
	private static final String BANK_NAME = "bankName";
	private static final String MASKED_CARD_NUMBER = "maskedCardNumber";
	private static final String CARD_EXPIRY = "cardExpiry";
	private static final String SUCCESSFUL_RESPONSE = "success";

	@Mock
	private SecurityHashGeneratorStrategy hashGeneratorStrategy;
	@Mock
	private WileyPaymentResponsePopulator wileyPaymentResponsePopulator;
	@InjectMocks
	private CaptureResponseTransformer underTest;

	private Map<String, String> responseMap = new LinkedHashMap<>();
	private WileyCaptureResult captureResult = new WileyCaptureResult();
	private String expectedHashBase;
	private Message<String> responseMessage;

	@Before
	public void setup()
	{

		responseMap.put(KEY_OPERATION, OPERATION);
		responseMap.put(KEY_RETURNCODE, SUCCESSFUL_RET_CODE);
		responseMap.put(KEY_RETURNMESSAGE, RETMESSAGE);
		responseMap.put(KEY_VENDORID, VENDORID);
		responseMap.put(KEY_TRANSID, TRANS_ID);
		responseMap.put(KEY_VALUE, VALUE);
		responseMap.put(KEY_CURRENCY, CURRENCY);
		responseMap.put(KEY_MERCHANTRESPONSE, MERCHANT_RESPONSE);
		responseMap.put(KEY_MERCHANTID, MERCHANT_ID);
		responseMap.put(KEY_CSCRESULT, CS_CRESULT);
		responseMap.put(KEY_ADDR_RESULT, ADDR_RESULT);
		responseMap.put(KEY_POST_RESULT, POST_RESULT);
		responseMap.put(KEY_AUTHCODE, AUTHCODE);
		responseMap.put(KEY_ACQUIRERID, ACQ_ID);
		responseMap.put(KEY_ACQUIRERNAME, ACQ_NAME);
		responseMap.put(KEY_BANKID, BANK_ID);
		responseMap.put(KEY_BANKNAME, BANK_NAME);
		responseMap.put(KEY_MASKEDCARDNUMBER, MASKED_CARD_NUMBER);
		responseMap.put(KEY_CARDEXPIRY, CARD_EXPIRY);
		responseMap.put(KEY_TIMESTAMP, TIMESTAMP);

		expectedHashBase = responseMap.values().stream().collect(Collectors.joining());

		responseMap.put(KEY_SECURITY, CALCULATED_HASH);

		Map<String, Object> headers = new HashedMap();
		headers.put(SITE_ID, TEST_SITE_ID);
		responseMessage = new MutableMessage<>(SUCCESSFUL_RESPONSE, headers);
		when(hashGeneratorStrategy.generateSecurityHash(TEST_SITE_ID, expectedHashBase)).thenReturn(CALCULATED_HASH);

		doAnswer(invocation ->
		{
			Object[] args = invocation.getArguments();
			((Map<String, String>) args[1]).putAll(responseMap);
			return null;
		}).when(wileyPaymentResponsePopulator).populate(eq(SUCCESSFUL_RESPONSE), any(Map.class));
	}



	@Test
	public void shouldPopulateSuccessfulResponseValues()
	{
		captureResult = underTest.transform(responseMessage);

		assertNotNull(captureResult.getRequestTime());
		assertEquals(new BigDecimal(VALUE), captureResult.getTotalAmount());
		assertEquals(TRANS_ID, captureResult.getRequestId());
		assertEquals(WileyTransactionStatusEnum.SUCCESS, captureResult.getStatus());
	}

	@Test
	public void shouldPopulateFailedResponseStatus()
	{
		when(hashGeneratorStrategy.generateSecurityHash(anyString(), anyString())).thenReturn(CALCULATED_HASH);

		responseMap.put(KEY_RETURNCODE, FAILED_RET_CODE);

		captureResult = underTest.transform(responseMessage);

		assertEquals(WileyTransactionStatusEnum.DECLINED, captureResult.getStatus());
		assertEquals(TransactionStatus.REJECTED, captureResult.getTransactionStatus());

	}

	@Test
	public void shouldTolerateMissingValues()
	{
		responseMap.clear();
		when(hashGeneratorStrategy.generateSecurityHash(anyString(), anyString())).thenReturn(CALCULATED_HASH);
		responseMap.put(KEY_SECURITY, CALCULATED_HASH);

		captureResult = underTest.transform(responseMessage);

		assertNotNull(captureResult.getRequestTime());
		assertNull(captureResult.getTotalAmount());
		assertNull(captureResult.getRequestId());
		assertEquals(WileyTransactionStatusEnum.UNEXPECTED_CODE_FROM_WPG, captureResult.getStatus());
	}

	@Test
	public void shouldHandleWrongSecurityTokenAndPopulateError()
	{
		responseMap.put(KEY_SECURITY, "otherHash");
		captureResult = underTest.transform(responseMessage);
		assertEquals(WileyTransactionStatusEnum.SECURITY_TOKEN_MISMATCH_FROM_WPG, captureResult.getStatus());
	}
}
