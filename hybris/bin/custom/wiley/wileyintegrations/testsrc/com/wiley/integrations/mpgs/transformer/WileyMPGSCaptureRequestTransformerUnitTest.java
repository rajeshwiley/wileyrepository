package com.wiley.integrations.mpgs.transformer;


import de.hybris.bootstrap.annotations.UnitTest;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.when;

import com.wiley.core.mpgs.constants.WileyMPGSConstants;
import com.wiley.core.mpgs.dto.capture.MPGSCaptureRequestDTO;
import com.wiley.core.mpgs.request.WileyCaptureRequest;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyMPGSCaptureRequestTransformerUnitTest
{
	private static final BigDecimal AMOUNT = new BigDecimal(100);
	private static final String CURRENCY = "USD";

	@Mock
	private WileyCaptureRequest request;

	@InjectMocks
	private WileyMPGSCaptureRequestTransformer testTransformer;

	@Test
	public void transformSuccess() throws Exception
	{
		when(request.getAmount()).thenReturn(AMOUNT);
		when(request.getCurrency()).thenReturn(CURRENCY);

		MPGSCaptureRequestDTO captureRequest = testTransformer.transform(request);

		assertEquals(WileyMPGSConstants.API_OPERATION_CAPTURE, captureRequest.getApiOperation());
		assertEquals(AMOUNT, captureRequest.getTransaction().getAmount());
		assertEquals(CURRENCY, captureRequest.getTransaction().getCurrency());
	}

	@Test(expected = IllegalArgumentException.class)
	public void transformPassNull() throws Exception
	{
		MPGSCaptureRequestDTO captureRequest = testTransformer.transform(null);
	}
}
