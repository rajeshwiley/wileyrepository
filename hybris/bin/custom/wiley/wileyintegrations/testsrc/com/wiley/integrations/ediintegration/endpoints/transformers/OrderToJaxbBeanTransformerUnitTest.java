package com.wiley.integrations.ediintegration.endpoints.transformers;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.store.BaseStoreModel;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.GenericMessage;

import com.wiley.fulfilmentprocess.model.WileyExportProcessModel;
import com.wiley.core.integration.edi.EDIConstants;
import com.wiley.core.integration.edi.Headers;
import com.wiley.integrations.ediintegration.endpoints.jaxb.PurchaseOrder;
import com.wiley.integrations.ediintegration.endpoints.jaxb.PurchaseOrderToWILEY;
import com.wiley.integrations.ediintegration.endpoints.transformers.converters.PurchaseOrderPopulator;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class OrderToJaxbBeanTransformerUnitTest
{
	private static final String BASE_STORE_KEY = "baseStore";
	private static final String LOCAT_DATE_TIME = "localDateTime";

	@Mock
	private KeyGenerator keyGenerator;
	@Mock
	private BaseStoreModel baseStore;
	@Mock
	private PurchaseOrderPopulator purchaseOrderPopulator;

	@InjectMocks
	private OrderToJaxbBeanTransformer testInstance = new OrderToJaxbBeanTransformer();

	@Before
	public void setUp() throws Exception
	{
		when(keyGenerator.generate()).thenReturn("900000000");
	}

	@Test
	public void testToJaxbBeansListVerifyRoutingIsNotNull() throws Exception
	{
		Message message = createMessage();
		//Given
		Message<PurchaseOrderToWILEY> verifyingOrder = testInstance.toJaxbBeansList(message);
		//Testing
		assertNotNull(verifyingOrder.getHeaders().get(Headers.CONVERTED_PROCESSES));
	}

	@Test
	public void testToJaxbBeansListVerifyOrdersPresentInResponseMessage() throws Exception
	{
		Message message = createMessage();
		//Given
		Message<PurchaseOrderToWILEY> verifyingOrder = testInstance.toJaxbBeansList(message);
		//Testing
		assertNotNull(verifyingOrder.getPayload().getRouting());
	}

	private Message createMessage()
	{
		Map<String, Object> headers = new HashMap<>();
		headers.put(BASE_STORE_KEY, baseStore);
		headers.put(LOCAT_DATE_TIME, LocalDateTime.now());
		return new GenericMessage<>(givenOrderExportProcessList(), headers);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetRoutingShouldThrowExceptionIfBaseStoreIsNull() throws Exception
	{
		testInstance.getRouting(Optional.empty(), LocalDateTime.now(), EDIConstants.PAYMENT_TYPE_CREDIT_CARD);
	}

	@Test
	public void testConvertOrdersShouldReturnOnlySuccessfullyConvertedOrders()
	{
		//Given
		final WileyExportProcessModel goodOrderProcess = givenGoodOrderExportProcess();
		final WileyExportProcessModel badOrderProcess = givenBadOrderExportProcess();
		final WileyExportProcessModel badOrderProcess2 = givenBadOrderExportProcess();
		List<WileyExportProcessModel> wileyExportProcesses =
				givenOrderExportProcessList(goodOrderProcess, badOrderProcess, badOrderProcess2);
		//Testing
		List<PurchaseOrder> resultOrders = testInstance.convertOrders(wileyExportProcesses);
		//Verify
		assertEquals(1, resultOrders.size());
	}

	private WileyExportProcessModel givenBadOrderExportProcess()
	{
		WileyExportProcessModel givenProcess = givenExportProcessWithOrder(givenOrder());
		doThrow(new NullPointerException()).when(purchaseOrderPopulator).populate(eq(givenProcess), any(PurchaseOrder.class));
		return givenProcess;
	}

	private WileyExportProcessModel givenGoodOrderExportProcess()
	{
		WileyExportProcessModel givenProcess = givenExportProcessWithOrder(givenOrder());
		doNothing().when(purchaseOrderPopulator).populate(eq(givenProcess), any(PurchaseOrder.class));
		return givenProcess;
	}

	private OrderModel givenOrder()
	{
		return mock(OrderModel.class);
	}

	private List<WileyExportProcessModel> givenOrderExportProcessList(final WileyExportProcessModel... processes)
	{
		return new ArrayList<>(Arrays.asList(processes));
	}

	private List<WileyExportProcessModel> givenOrderExportProcessList()
	{
		OrderModel orderModel = mock(OrderModel.class);
		BaseStoreModel baseStoreModel = givenBaseStoreModel();
		when(baseStoreModel.getUid()).thenReturn("AGS");
		when(orderModel.getStore()).thenReturn(baseStoreModel);
		WileyExportProcessModel processModel = mock(WileyExportProcessModel.class);

		when(processModel.getOrder()).thenReturn(orderModel);
		return Collections.singletonList(processModel);
	}

	private WileyExportProcessModel givenExportProcessWithOrder(final OrderModel orderModel)
	{
		WileyExportProcessModel wileyExportProcessModel = mock(WileyExportProcessModel.class);
		when(wileyExportProcessModel.getOrder()).thenReturn(orderModel);
		return wileyExportProcessModel;
	}

	private BaseStoreModel givenBaseStoreModel()
	{
		return mock(BaseStoreModel.class);
	}


}