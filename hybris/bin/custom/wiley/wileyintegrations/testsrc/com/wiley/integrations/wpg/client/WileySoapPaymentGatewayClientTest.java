package com.wiley.integrations.wpg.client;

import de.hybris.bootstrap.annotations.UnitTest;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.verify;

import java.io.IOException;

import javax.xml.ws.Holder;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.integrations.wpg.dto.TokenRefundPaymentRequest;
import com.wileypay.wpg_v1.WpgPort;

import static junit.framework.Assert.assertEquals;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileySoapPaymentGatewayClientTest
{
	private static final String USER_ID = "userId";
	private static final String METHOD = "SOAP";
	private static final String CUSTOMER_PRESENT = "customerPresent";
	private static final String TOKEN = "token";
	private static final String DESCRIPTION = "description";
	private static final String USD = "USD";
	private static final String REGION = "region";
	private static final String SECURITY = "security";
	private static final Integer VENDOR_ID = 210;
	public static final String TRANSACTION_ID = "transactionId";
	public static final String TOTAL_AMOUNT = "value";
	public static final String TIMESTAMP = "timestamp";

	@Spy
	private WileySoapPaymentGatewayClient testedInstance = new WileySoapPaymentGatewayClient();

	@Captor
	private ArgumentCaptor<Holder<Integer>> vendorIdCaptor;
	@Captor
	private ArgumentCaptor<Holder<String>> transIDCaptor;
	@Captor
	private ArgumentCaptor<Holder<String>> timestampCaptor;
	@Captor
	private ArgumentCaptor<Holder<String>> totalAmountCaptor;

	@Mock
	private WpgPort wpgPort;


	@Before
	public void setUp()
	{
		doReturn(wpgPort).when(testedInstance).getSOAPPort();
	}

	@Test
	public void shouldSendVendorId() throws IOException
	{
		TokenRefundPaymentRequest request = createTokenRequest();
		request.setVendorId(VENDOR_ID);

		testedInstance.tokenRefund(request);

		verifyTokenRefundInvokedAndCaptureArguments();
		assertEquals(VENDOR_ID, vendorIdCaptor.getValue().value);
	}

	@Test
	public void shouldSendTransactionId() throws IOException
	{
		TokenRefundPaymentRequest request = createTokenRequest();
		request.setTransId(TRANSACTION_ID);

		testedInstance.tokenRefund(request);

		verifyTokenRefundInvokedAndCaptureArguments();
		assertEquals(TRANSACTION_ID, transIDCaptor.getValue().value);
	}

	@Test
	public void shouldSendTotalAmount() throws IOException
	{
		TokenRefundPaymentRequest request = createTokenRequest();
		request.setValue(TOTAL_AMOUNT);

		testedInstance.tokenRefund(request);

		verifyTokenRefundInvokedAndCaptureArguments();
		assertEquals(TOTAL_AMOUNT, totalAmountCaptor.getValue().value);
	}

	@Test
	public void shouldSendTimestamp() throws IOException
	{
		TokenRefundPaymentRequest request = createTokenRequest();
		request.setTimestamp(TIMESTAMP);

		testedInstance.tokenRefund(request);

		verifyTokenRefundInvokedAndCaptureArguments();
		assertEquals(TIMESTAMP, timestampCaptor.getValue().value);
	}

	private void verifyTokenRefundInvokedAndCaptureArguments()
	{
		verify(wpgPort).tokenRefund(
				vendorIdCaptor.capture(),
				transIDCaptor.capture(),
				eq(USER_ID),
				eq(METHOD),
				eq(CUSTOMER_PRESENT),
				eq(TOKEN),
				eq(DESCRIPTION),
				totalAmountCaptor.capture(),
				eq(USD),
				timestampCaptor.capture(),
				eq(REGION),
				eq(SECURITY),
				any(Holder.class),
				any(Holder.class),
				any(Holder.class),
				any(Holder.class),
				any(Holder.class),
				any(Holder.class),
				any(Holder.class),
				any(Holder.class),
				any(Holder.class),
				any(Holder.class),
				any(Holder.class),
				any(Holder.class)
		);
	}

	private TokenRefundPaymentRequest createTokenRequest()
	{
		TokenRefundPaymentRequest request = new TokenRefundPaymentRequest();
		request.setUserId(USER_ID);
		request.setMethod(METHOD);
		request.setCustomerPresent(CUSTOMER_PRESENT);
		request.setToken(TOKEN);
		request.setDescription(DESCRIPTION);
		request.setCurrency(USD);
		request.setRegion(REGION);
		request.setSecurity(SECURITY);
		return request;
	}

}