package com.wiley.integrations.esb.converters.populator.subscription;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.user.AddressModel;

import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.integrations.esb.subscription.dto.Wileyb2cSubscriptionDeliveryAddressRequestDto;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cSubscriptionDeliveryAddressRequestDtoPopulatorTest
{
	private static final String POST_CODE = "post-code";
	private static final String COUNTRY = "country";
	private static final String STATE = "state";
	private static final String CITY = "city";
	private static final String LINE_1 = "line-1";
	private static final String LINE_2 = "line_2";
	private static final String FIRST_NAME = "first-name";
	private static final String LAST_NAME = "last-name";
	private static final String PHONE_NUMBER = "phone-number";

	private Wileyb2cSubscriptionDeliveryAddressRequestDtoPopulator wileyb2CSubscriptionDeliveryAddressRequestDtoPopulator
			= new Wileyb2cSubscriptionDeliveryAddressRequestDtoPopulator();

	private Wileyb2cSubscriptionDeliveryAddressRequestDto requestDto;

	@Mock
	private AddressModel addressModelMock;

	@Mock
	private CountryModel countryModelMock;

	@Mock
	private RegionModel regionModelMock;

	@Before
	public void setup()
	{
		requestDto = new Wileyb2cSubscriptionDeliveryAddressRequestDto();

		when(addressModelMock.getPostalcode()).thenReturn(POST_CODE);
		when(addressModelMock.getCountry()).thenReturn(countryModelMock);
		when(countryModelMock.getIsocode()).thenReturn(COUNTRY);
		when(addressModelMock.getRegion()).thenReturn(regionModelMock);
		when(regionModelMock.getIsocodeShort()).thenReturn(STATE);
		when(addressModelMock.getTown()).thenReturn(CITY);
		when(addressModelMock.getLine1()).thenReturn(LINE_1);
		when(addressModelMock.getLine2()).thenReturn(LINE_2);
		when(addressModelMock.getFirstname()).thenReturn(FIRST_NAME);
		when(addressModelMock.getLastname()).thenReturn(LAST_NAME);
		when(addressModelMock.getPhone1()).thenReturn(PHONE_NUMBER);
	}

	@Test
	public void shouldPopulatePostcode()
	{
		// When
		wileyb2CSubscriptionDeliveryAddressRequestDtoPopulator.populate(addressModelMock, requestDto);

		// Then
		assertEquals("Field postcode should have been populated!", POST_CODE, requestDto.getPostcode());
	}

	@Test
	public void shouldPopulateCountry()
	{
		// When
		wileyb2CSubscriptionDeliveryAddressRequestDtoPopulator.populate(addressModelMock, requestDto);

		// Then
		assertEquals("Field country should have been populated!", COUNTRY, requestDto.getCountry());
	}

	@Test
	public void shouldPopulateState()
	{
		// When
		wileyb2CSubscriptionDeliveryAddressRequestDtoPopulator.populate(addressModelMock, requestDto);

		// Then
		assertEquals("Field state should have been populated!", STATE, requestDto.getState());
	}

	@Test
	public void shouldPopulateCity()
	{
		// When
		wileyb2CSubscriptionDeliveryAddressRequestDtoPopulator.populate(addressModelMock, requestDto);

		// Then
		assertEquals("Field city should have been populated!", CITY, requestDto.getCity());
	}

	@Test
	public void shouldPopulateLine1()
	{
		// When
		wileyb2CSubscriptionDeliveryAddressRequestDtoPopulator.populate(addressModelMock, requestDto);

		// Then
		assertEquals("Field line1 should have been populated!", LINE_1, requestDto.getLine1());
	}

	@Test
	public void shouldPopulateLine2()
	{
		// When
		wileyb2CSubscriptionDeliveryAddressRequestDtoPopulator.populate(addressModelMock, requestDto);

		// Then
		assertEquals("Field line2 should have been populated!", LINE_2, requestDto.getLine2());
	}

	@Test
	public void shouldPopulateFirstName()
	{
		// When
		wileyb2CSubscriptionDeliveryAddressRequestDtoPopulator.populate(addressModelMock, requestDto);

		// Then
		assertEquals("Field first name should have been populated!", FIRST_NAME, requestDto.getFirstName());
	}

	@Test
	public void shouldPopulateLastName()
	{
		// When
		wileyb2CSubscriptionDeliveryAddressRequestDtoPopulator.populate(addressModelMock, requestDto);

		// Then
		assertEquals("Field last name should have been populated!", LAST_NAME, requestDto.getLastName());
	}

	@Test
	public void shouldPopulatePhoneNumber()
	{
		// When
		wileyb2CSubscriptionDeliveryAddressRequestDtoPopulator.populate(addressModelMock, requestDto);

		// Then
		assertEquals("Field phoneNumber should have been populated!", PHONE_NUMBER, requestDto.getPhoneNumber());
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldNotPopulateForNullModel()
	{
		// When
		wileyb2CSubscriptionDeliveryAddressRequestDtoPopulator.populate(null, requestDto);

		// Then
		fail("Exception should have been thrown for null parameter!");
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldNotPopulateForNullDto()
	{
		// When
		wileyb2CSubscriptionDeliveryAddressRequestDtoPopulator.populate(addressModelMock, null);

		// Then
		fail("Exception should have been thrown for null parameter!");
	}
}
