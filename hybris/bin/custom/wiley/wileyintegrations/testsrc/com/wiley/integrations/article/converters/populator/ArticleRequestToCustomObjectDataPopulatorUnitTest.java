package com.wiley.integrations.article.converters.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.i18n.I18NService;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Locale;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.integrations.article.dto.ArticleRequestDto;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ArticleRequestToCustomObjectDataPopulatorUnitTest
{
	private static final Locale LOCALE = Locale.getDefault();
	private static final String ID = "test_id";

	@Mock
	private I18NService i18nService;

	@InjectMocks
	private ArticleRequestToCustomObjectDataPopulator populator;

	@Before
	public void prepare()
	{
		when(i18nService.getCurrentLocale()).thenReturn(LOCALE);
	}

	@Test
	public void populateSuccessTest()
	{
		// given
		final ArticleRequestDto dto = new ArticleRequestDto();

		// when
		populator.populate(ID, dto);

		// then
		assertEquals(ID, dto.getId());
		assertEquals(LOCALE.toString(), dto.getLocale());
	}

	@Test
	public void populateNullLocaleSuccessTest()
	{
		// given
		final ArticleRequestDto dto = new ArticleRequestDto();
		when(i18nService.getCurrentLocale()).thenReturn(null);

		// when
		populator.populate(ID, dto);

		// then
		assertEquals(ID, dto.getId());
		assertEquals(null, dto.getLocale());
	}

	@Test(expected = IllegalArgumentException.class)
	public void populateNullDtoFailTest()
	{
		populator.populate(ID, null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void populateNullIdFailTest()
	{
		populator.populate(null, new ArticleRequestDto());
	}
}