package com.wiley.integrations.mpgs.transformer;

import de.hybris.bootstrap.annotations.UnitTest;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.mpgs.dto.token.MPGSTokenRequestDTO;
import com.wiley.core.mpgs.request.WileyTokenizationRequest;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyMPGSTokenizeRequestTransformerUnitTest

{
	public static final String TEST_SOURCE_OF_FUNDS = "TEST_SOURCE_OF_FUNDS";
	public static final String TEST_SESSION_ID = "TEST_SESSION_ID";

	@Mock
	private WileyTokenizationRequest request;

	@InjectMocks
	private WileyMPGSTokenizeRequestTransformer testTransformer;

	@Test
	public void transformSuccess() throws Exception
	{

		when(request.getSourceOfFundsType()).thenReturn(TEST_SOURCE_OF_FUNDS);
		when(request.getSessionId()).thenReturn(TEST_SESSION_ID);
		MPGSTokenRequestDTO tokenRequest = testTransformer.transform(request);
		assertEquals(TEST_SOURCE_OF_FUNDS, tokenRequest.getSourceOfFunds().getType());
		assertEquals(TEST_SESSION_ID, tokenRequest.getSession().getId());
	}

	@Test(expected = IllegalArgumentException.class)
	public void transformPassNull() throws Exception
	{
		MPGSTokenRequestDTO tokenRequest = testTransformer.transform(null);
	}

}
