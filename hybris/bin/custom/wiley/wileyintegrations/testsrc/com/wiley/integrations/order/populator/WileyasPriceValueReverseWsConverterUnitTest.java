package com.wiley.integrations.order.populator;

import com.wiley.integrations.order.dto.PriceValueWsDTO;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.util.PriceValue;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyasPriceValueReverseWsConverterUnitTest
{
    private static final double TEST_VALUE = 133.33D;
    private static final String TEST_CURRENCY = "USD";

    private WileyasPriceValueReverseWsConverter converter = new WileyasPriceValueReverseWsConverter();

    @Test
    public void convertTest()
    {
        final PriceValueWsDTO dto = new PriceValueWsDTO();
        dto.setValue(TEST_VALUE);
        dto.setCurrency(TEST_CURRENCY);

        final PriceValue result = converter.convert(dto);

        assertNotNull(result);
        assertEquals(TEST_VALUE, result.getValue(), 0.0D);
        assertEquals(TEST_CURRENCY, result.getCurrencyIso());
        assertTrue(result.isNet());
    }
}