package com.wiley.integrations.mpgs.transformer;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.payment.dto.TransactionStatus;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.messaging.Message;

import com.wiley.core.mpgs.constants.WileyMPGSConstants;
import com.wiley.core.mpgs.dto.json.Response;
import com.wiley.core.mpgs.dto.json.Transaction;
import com.wiley.core.mpgs.dto.verify.MPGSVerifyResponseDTO;
import com.wiley.core.mpgs.response.WileyVerifyResponse;
import com.wiley.core.mpgs.services.WileyTransformationService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyMPGSVerifyResponseTransformerUnitTest
{

	private static final String GATEWAY_CODE = "APPROVED";
	private static final String CURRENCY = "USD";
	public static final String  DATE = "2017-05-05T00:00:00.000Z";
	private static final String TRANSACTION_ID = "transactionID1234567";

	@Mock
	private Message<MPGSVerifyResponseDTO> message;

	@Mock
	private MPGSVerifyResponseDTO responseDto;

	@Mock
	private WileyTransformationService wileyTransformationService;

	@Mock
	private Response response;

	@Mock
	private Transaction transaction;

	@InjectMocks
	private WileyMPGSVerifyResponseTransformer testTransformer;

	@Test
	public void transformSuccess() throws Exception
	{
		// given
		when(message.getPayload()).thenReturn(responseDto);
		when(responseDto.getResult()).thenReturn(WileyMPGSConstants.RETURN_STATUS_SUCCESS);
		when(wileyTransformationService.transformStatusIfSuccessful(WileyMPGSConstants.RETURN_STATUS_SUCCESS)).thenReturn(
				TransactionStatus.ACCEPTED.name());
		when(responseDto.getResponse()).thenReturn(response);
		when(response.getGatewayCode()).thenReturn(GATEWAY_CODE);
		when(responseDto.getTransaction()).thenReturn(transaction);
		when(transaction.getId()).thenReturn(TRANSACTION_ID);
		when(transaction.getCurrency()).thenReturn(CURRENCY);
		when(responseDto.getTimeOfRecord()).thenReturn(DATE);
		when(wileyTransformationService.transformStringToDate(DATE)).thenReturn(new DateTime(DATE).toDate());

		// when
		WileyVerifyResponse verifyResponse = testTransformer.transform(message);

		// then
		assertEquals(TransactionStatus.ACCEPTED.name(), verifyResponse.getStatus());
		assertEquals(GATEWAY_CODE, verifyResponse.getStatusDetails());
		assertEquals(TRANSACTION_ID, verifyResponse.getTransactionId());
		assertEquals(CURRENCY, verifyResponse.getCurrency());
		assertNotNull(verifyResponse.getTimeOfRecord());

	}

	@Test(expected = NullPointerException.class)
	public void nullPointerExceptionIfErrorResult() throws Exception
	{

		WileyVerifyResponse wileyVerifyResponse = testTransformer.transform(null);
	}

}