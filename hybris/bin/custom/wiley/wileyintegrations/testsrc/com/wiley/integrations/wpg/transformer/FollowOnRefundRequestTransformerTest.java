package com.wiley.integrations.wpg.transformer;

import de.hybris.bootstrap.annotations.UnitTest;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Currency;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.payment.request.WileyFollowOnRefundRequest;
import com.wiley.core.payment.strategies.SecurityHashGeneratorStrategy;
import com.wiley.core.payment.strategies.WPGRegionStrategy;
import com.wiley.core.payment.strategies.WPGVendorIdStrategy;
import com.wiley.core.payment.strategies.WileyTransactionIdGeneratorStrategy;
import com.wiley.integrations.wpg.dto.TokenRefundPaymentRequest;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class FollowOnRefundRequestTransformerTest
{
	private static final String REQUEST_TOKEN = "requestToken";
	private static final String USD = "USD";
	private static final String PAYMENT_PROVIDER = "paymentProvider";
	private static final String SITE_ID = "siteId";
	private static final String MERCHANT_TRANSACTION_CODE = "MerchantCode";
	private static final String REQUEST_ID = "requestId";
	private static final Integer VENDOR_ID = 210;
	private static final String NEW_TRANSACTION_ID = "newTransactionId";
	private static final String USER_ID = "userId";
	private static final String SOAP_METHOD = "SOAP";
	private static final String CUSTOMER_PRESENT = "N";
	private static final String TOTAL_AMOUNT_AS_STRING = "34.45";
	private static final String ORDER_CODE = "orderCode";

	public static final String DESCRIPTION = "description";
	public static final String REGION_ID = "regionId";
	private static final java.lang.String SECURITY_HASH = "LJERLKGJE";

	private Currency currency = Currency.getInstance(USD);
	private BigDecimal totalAmount = new BigDecimal(TOTAL_AMOUNT_AS_STRING);

	@InjectMocks
	private FollowOnRefundRequestTransformer testedInstance = new FollowOnRefundRequestTransformer();

	@Mock
	private WPGVendorIdStrategy vendorIdStrategy;
	@Mock
	private WPGRegionStrategy regionStrategy;
	@Mock
	private SecurityHashGeneratorStrategy wpgHashStrategy;
	@Mock
	private WileyTransactionIdGeneratorStrategy transactionIdGeneratorStrategy;
	private WileyFollowOnRefundRequest source;
	private TokenRefundPaymentRequest target = new TokenRefundPaymentRequest();

	@Before
	public void setUp()
	{
		source = createCommandRequest();
	}

	@Test
	public void shouldSetVendorId()
	{
		when(vendorIdStrategy.getVendorId(SITE_ID)).thenReturn(VENDOR_ID);

		target = testedInstance.transform(source);

		assertEquals(VENDOR_ID, Integer.valueOf(target.getVendorId()));
	}

	@Test
	public void shouldSetTransactionId()
	{
		when(transactionIdGeneratorStrategy.generateTransactionId()).thenReturn(NEW_TRANSACTION_ID);

		target = testedInstance.transform(source);

		assertEquals(NEW_TRANSACTION_ID, target.getTransId());
	}

	@Test
	public void shouldSetUserId()
	{
		testedInstance.setUserId(USER_ID);

		target = testedInstance.transform(source);

		assertEquals(USER_ID, target.getUserId());
	}

	@Test
	public void shouldSetMethod()
	{
		testedInstance.setMethod(SOAP_METHOD);

		target = testedInstance.transform(source);

		assertEquals(SOAP_METHOD, target.getMethod());
	}

	@Test
	public void shouldSetCustomerPresent()
	{
		testedInstance.setCustomerPresent(CUSTOMER_PRESENT);

		target = testedInstance.transform(source);

		assertEquals(CUSTOMER_PRESENT, target.getCustomerPresent());
	}

	@Test
	public void shouldSetToken()
	{
		target = testedInstance.transform(source);

		assertEquals(REQUEST_TOKEN, target.getToken());
	}

	@Test
	public void shouldSetDescription()
	{
		testedInstance.setDescription(DESCRIPTION);

		target = testedInstance.transform(source);

		assertEquals(DESCRIPTION, target.getDescription());
	}

	@Test
	public void shouldSetTotalAmount()
	{
		target = testedInstance.transform(source);

		assertEquals(TOTAL_AMOUNT_AS_STRING, target.getValue());
	}

	@Test
	public void shouldSetCurrency()
	{
		target = testedInstance.transform(source);

		assertEquals(USD, target.getCurrency());
	}

	@Test
	public void shouldSetRegion()
	{
		when(regionStrategy.getRegionByCurrency(USD)).thenReturn(REGION_ID);

		target = testedInstance.transform(source);

		assertEquals(REGION_ID, target.getRegion());
	}

	@Test
	public void shouldSetTimestamp()
	{
		target = testedInstance.transform(source);

		assertNotNull(target.getTimestamp());
	}

	@Test
	public void shouldSetSecurity()
	{
		when(wpgHashStrategy.generateSecurityHash(eq(SITE_ID), anyString())).thenReturn(SECURITY_HASH);

		target = testedInstance.transform(source);

		assertEquals(SECURITY_HASH, target.getSecurity());
	}

	private WileyFollowOnRefundRequest createCommandRequest()
	{
		WileyFollowOnRefundRequest refundRequest = new WileyFollowOnRefundRequest(
				MERCHANT_TRANSACTION_CODE,
				REQUEST_ID,
				REQUEST_TOKEN,
				currency,
				totalAmount,
				PAYMENT_PROVIDER,
				SITE_ID,
				ORDER_CODE
		);
		return refundRequest;
	}
}