package com.wiley.integrations.order.populator;


import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.CancelReason;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.util.PriceValue;
import de.hybris.platform.util.TaxValue;
import de.hybris.platform.variants.model.VariantProductModel;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyExternalDiscountModel;
import com.wiley.integrations.order.dto.DiscountValueWsDTO;
import com.wiley.integrations.order.dto.OrderEntryWsDTO;
import com.wiley.integrations.order.dto.PriceValueWsDTO;
import com.wiley.integrations.order.dto.TaxValueWsDTO;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyOrderEntryWsPopulatorUnitTest
{
	private static final String ORDER_ID = "ORDER ID";
	private static final String USER_ID = "USER ID";
	private static final String ENTRY_ID = "ENTRY ID";
	private static final OrderStatus STATUS = OrderStatus.CHECKED_AUTHORIZED;
	private static final String PRODUCT_CODE = "PRODUCT CODE";
	private static final String BASE_PRODUCT_CODE = "BASE PRODUCT CODE";
	private static final String BUSINESS_ITEM_ID = "BUSINESS ITEM ID";
	private static final String ORIGINAL_BUSINESS_KEY = "ORIGINAL BUSINESS KEY";
	private static final String ADDITIONAL_INFO = "ADDITIONAL INFO";
	private static final Long QUANTITY = 20L;
	private static final Double BASE_PRICE = 200.50d;
	private static final Double SUBTOTAL_PRICE = 120.50d;
	private static final Double TOTAL_PRICE = 140.75d;
	private static final Double TAXABLE_TOTAL_PRICE = 140.75d;
	private static final CancelReason CANCEL_REASON = CancelReason.OTHER;
	private static final Date DELIVERY_DATE = new Date();
	private static final Date STATUS_MODIFICATION_TIME = new Date();
	private static final Date MODIFICATION_TIME = new Date();

	@InjectMocks
	private WileyOrderEntryWsPopulator testInstance;

	@Mock
	private Converter<DiscountValue, DiscountValueWsDTO> wileyDiscountValueWsConverter;
	@Mock
	private Converter<WileyExternalDiscountModel, DiscountValueWsDTO> wileyExternalDiscountWsConverter;
	@Mock
	private Converter<PriceValue, PriceValueWsDTO> wileyPriceValueWsConverter;
	@Mock
	private Converter<TaxValue, TaxValueWsDTO> wileyTaxValueWsConverter;


	@Mock
	private AbstractOrderEntryModel sourceMock;

	@Mock
	private AbstractOrderModel sourceOrderMock;

	@Mock
	private UserModel userModel;

	@Mock
	private ProductModel sourceBaseProductMock;
	@Mock
	private VariantProductModel sourceVariantProductMock;

	@Mock
	private OrderEntryWsDTO targetMock;

	@Mock
	private PriceValue externalPriceMock;
	@Mock
	private WileyExternalDiscountModel externalDiscountMock;
	@Mock
	private DiscountValue discountMock;
	@Mock
	private TaxValue taxValueMock;

	@Mock
	private PriceValueWsDTO externalPriceWsMock;
	@Mock
	private DiscountValueWsDTO externalDiscountWsMock;
	@Mock
	private DiscountValueWsDTO discountWsMock;
	@Mock
	private TaxValueWsDTO taxValueWsMock;

	@Test
	public void testPopulateFields()
	{
		// given

		when(sourceMock.getOrder()).thenReturn(sourceOrderMock);
		when(sourceOrderMock.getGuid()).thenReturn(ORDER_ID);
		when(sourceOrderMock.getUser()).thenReturn(userModel);
		when(userModel.getUid()).thenReturn(USER_ID);
		when(sourceMock.getStatus()).thenReturn(STATUS);
		when(sourceMock.getGuid()).thenReturn(ENTRY_ID);

		when(sourceMock.getProduct()).thenReturn(sourceVariantProductMock);
		when(sourceVariantProductMock.getCode()).thenReturn(PRODUCT_CODE);
		when(sourceVariantProductMock.getBaseProduct()).thenReturn(sourceBaseProductMock);
		when(sourceBaseProductMock.getCode()).thenReturn(BASE_PRODUCT_CODE);


		when(sourceMock.getBusinessItemId()).thenReturn(BUSINESS_ITEM_ID);

		when(sourceMock.getOriginalBusinessKey()).thenReturn(ORIGINAL_BUSINESS_KEY);
		when(sourceMock.getQuantity()).thenReturn(QUANTITY);
		when(sourceMock.getBasePrice()).thenReturn(BASE_PRICE);
		when(sourceMock.getSubtotalPrice()).thenReturn(SUBTOTAL_PRICE);
		when(sourceMock.getTotalPrice()).thenReturn(TOTAL_PRICE);
		when(sourceMock.getTaxableTotalPrice()).thenReturn(TAXABLE_TOTAL_PRICE);

		List<PriceValue> priceValues = Collections.singletonList(externalPriceMock);
		when(sourceMock.getExternalPriceValues()).thenReturn(priceValues);
		when(wileyPriceValueWsConverter.convert(externalPriceMock)).thenReturn(externalPriceWsMock);

		List<DiscountValue> discounts = Collections.singletonList(discountMock);
		when(sourceMock.getDiscountValues()).thenReturn(discounts);
		when(wileyDiscountValueWsConverter.convert(discountMock)).thenReturn(discountWsMock);


		List<WileyExternalDiscountModel> externalDiscounts = Collections.singletonList(externalDiscountMock);
		when(sourceMock.getExternalDiscounts()).thenReturn(externalDiscounts);
		when(wileyExternalDiscountWsConverter.convert(externalDiscountMock)).thenReturn(externalDiscountWsMock);

		List<TaxValue> taxes = Collections.singletonList(taxValueMock);
		when(sourceMock.getTaxValues()).thenReturn(taxes);
		when(wileyTaxValueWsConverter.convert(taxValueMock)).thenReturn(taxValueWsMock);

		when(sourceMock.getNamedDeliveryDate()).thenReturn(DELIVERY_DATE);
		when(sourceMock.getCancelReason()).thenReturn(CANCEL_REASON);
		when(sourceMock.getAdditionalInfo()).thenReturn(ADDITIONAL_INFO);
		when(sourceMock.getStatusModifiedTime()).thenReturn(STATUS_MODIFICATION_TIME);
		when(sourceMock.getModifiedtime()).thenReturn(MODIFICATION_TIME);

		// when
		testInstance.populate(sourceMock, targetMock);

		// then
		verify(targetMock).setOrderId(ORDER_ID);
		verify(targetMock).setUserId(USER_ID);
		verify(targetMock).setEntryId(ENTRY_ID);
		verify(targetMock).setStatus(STATUS.getCode());
		verify(targetMock).setProductCode(PRODUCT_CODE);
		verify(targetMock).setBaseProductCode(BASE_PRODUCT_CODE);
		verify(targetMock).setBusinessItemId(BUSINESS_ITEM_ID);
		verify(targetMock).setBusinessKey(ORIGINAL_BUSINESS_KEY);
		verify(targetMock).setQuantity(QUANTITY.intValue());
		verify(targetMock).setBasePrice(BASE_PRICE);
		verify(targetMock).setSubtotalPrice(SUBTOTAL_PRICE);
		verify(targetMock).setTotalPrice(TOTAL_PRICE);
		verify(targetMock).setTaxableTotalPrice(TAXABLE_TOTAL_PRICE);
		verify(targetMock).setExternalPrices(eq(Collections.singletonList(externalPriceWsMock)));
		verify(targetMock).setDiscounts(eq(Collections.singletonList(discountWsMock)));
		verify(targetMock).setExternalDiscounts(eq(Collections.singletonList(externalDiscountWsMock)));
		verify(targetMock).setTaxes(eq(Collections.singletonList(taxValueWsMock)));
		verify(targetMock).setDeliveryDate(DELIVERY_DATE);
		verify(targetMock).setCancelReason(CANCEL_REASON.getCode());
		verify(targetMock).setAdditionalInfo(ADDITIONAL_INFO);
		verify(targetMock).setStatusModificationTime(STATUS_MODIFICATION_TIME);
		verify(targetMock).setModificationTime(MODIFICATION_TIME);
	}

	@Test
	public void shouldTolerateAllNullFields()
	{
		// given
		OrderEntryModel emptyMock = mock(OrderEntryModel.class);
		when(emptyMock.getQuantity()).thenReturn(null);
		when(emptyMock.getTotalPrice()).thenReturn(null);
		when(emptyMock.getBasePrice()).thenReturn(null);
		when(emptyMock.getTaxableTotalPrice()).thenReturn(null);
		when(emptyMock.getSubtotalPrice()).thenReturn(null);
		when(emptyMock.getExternalDiscounts()).thenReturn(null);
		when(emptyMock.getExternalPriceValues()).thenReturn(null);
		when(emptyMock.getDiscountValues()).thenReturn(null);
		// when
		testInstance.populate(emptyMock, targetMock);

		// then
		verify(targetMock, never()).setOrderId(any());
		verify(targetMock, never()).setUserId(any());
		verify(targetMock).setEntryId(null);
		verify(targetMock, never()).setStatus(any());
		verify(targetMock, never()).setProductCode(any());
		verify(targetMock, never()).setBaseProductCode(any());
		verify(targetMock).setBusinessKey(null);
		verify(targetMock, never()).setQuantity(any());
		verify(targetMock).setBasePrice(null);
		verify(targetMock).setSubtotalPrice(null);
		verify(targetMock).setTotalPrice(null);
		verify(targetMock).setTaxableTotalPrice(null);
		verify(targetMock, never()).setExternalPrices(any());
		verify(targetMock, never()).setDiscounts(any());
		verify(targetMock, never()).setExternalDiscounts(any());
		verify(targetMock).setDeliveryDate(null);
		verify(targetMock, never()).setCancelReason(any());
		verify(targetMock).setAdditionalInfo(null);
		verify(targetMock).setStatusModificationTime(null);
		verify(targetMock).setModificationTime(null);

	}

	@Test(expected = IllegalArgumentException.class)
	public void testPopulateWithNullSource()
	{
		testInstance.populate(null, targetMock);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testPopulateWithNullTarget()
	{
		testInstance.populate(sourceMock, null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testPopulateWithNullSourceAndTarget()
	{
		testInstance.populate(null, null);
	}
}
