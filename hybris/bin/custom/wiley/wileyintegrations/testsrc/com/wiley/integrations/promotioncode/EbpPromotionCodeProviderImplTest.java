package com.wiley.integrations.promotioncode;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.voucher.VoucherService;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.order.WileyCheckoutService;
import com.wiley.core.partner.WileyPartnerService;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;


/**
 * Unit test for {@link EloquaPromotionCodeProviderImpl}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class EbpPromotionCodeProviderImplTest extends AbstractPromotionCodeProviderImplTest
{
	@InjectMocks
	private EbpPromotionCodeProviderImpl promotionCodeProviderMock;
	@Mock
	protected VoucherService voucherServiceMock;
	@Mock
	protected WileyPartnerService partnerServiceMock;
	@Mock
	private WileyCheckoutService checkoutServiceMock;

	
	protected PromotionCodeProvider getPromotionCodeProvider()
	{
		return promotionCodeProviderMock;
	}
	
	@Override
	@Test
	public void testStudentPromotionCode() throws Exception
	{
		// Given
		emulateNotVoucherFlow();
		emulateNotPartnerFlow();
		
		when(checkoutServiceMock.isStudentOrder(orderMock)).thenReturn(true);

		// When
		final Optional<String> promotionCode = getPromotionCodeProvider().getOrderPromotionCode(orderMock);

		// Then
		assertEquals("don't send student discount to eloqua", promotionCode.get(), "STUDENT");
	}

	@Test
	public void testVoucherPromotionCodeForEbp() throws Exception
	{
		super.testVoucherPromotionCode();
	}

	@Test
	public void testPartnerPromotionCodeForEbp() throws Exception
	{
		super.testPartnerPromotionCode();
	}

	@Override
	public WileyPartnerService getPartnerServiceMock()
	{
		return partnerServiceMock;
	}

	@Override
	public VoucherService getVoucherServiceMock()
	{
		return voucherServiceMock;
	}
}