package com.wiley.integrations.ebp.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.order.price.DiscountModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.europe1.enums.UserDiscountGroup;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.voucher.VoucherService;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.category.WileyCategoryService;
import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.integration.ebp.dto.EbpAddProductsPayload;
import com.wiley.core.model.WileyPartnerCompanyModel;
import com.wiley.core.order.WileyCheckoutService;
import com.wiley.core.partner.WileyPartnerService;
import com.wiley.core.pin.service.PinService;
import com.wiley.core.price.WileyDiscountCalculationService;
import com.wiley.integrations.ebp.dto.OrderNode;
import com.wiley.integrations.ebp.enums.WileyEbpPaymentMethod;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class OrderToEbpOrderInfoPopulatorUnitTest
{
	private static final String TEST_CODE = "test_code";
	private static final String TEST_DATE = "2011-04-17T15:10:00+0400";
	private static final String EBP_SOURCE_SEPARATOR = ",";
	private static final String TEST_VOUCHER_CODE = "test_voucher_code";
	private static final String TEST_PARTNER_CODE = "test_partner_code";
	private static final String CPA_CODE = "CPA";
	private static final String CMA_CODE = "CMA";
	private static final double TEST_TOTAL_DISCOUNT = 1.058888888;
	private static final double TEST_TOTAL_TAX = 2.0000212000030;
	private static final double TEST_DELIVERY_COST = 3.011111;
	private static final double TEST_TOTAL_PRICE = 4.07145001;
	private static final boolean TEST_IS_NON_ZERO_ORDER = true;
	private static final int DOUBLE_PRECISION_SCALE = 2;

	@Mock
	private WileyCheckoutService wileyCheckoutService;
	@Mock
	private PinService pinService;
	@Mock
	private EbpAddProductsPayload ebpAddProductsPayload;
	@Mock
	private VoucherService voucherService;
	@Mock
	private SessionService sessionService;
	@Mock
	private WileyCategoryService categoryService;
	@Mock
	private UserService userService;
	@Mock
	private OrderModel orderModelMock;
	@Mock
	private WileyPartnerCompanyModel wileyPartnerMock;
	@Mock
	private UserDiscountGroup userDiscountGroup;
	@Mock
	private AbstractOrderEntryModel entryCpaMock;
	@Mock
	private AbstractOrderEntryModel entryCmaMock;
	@Mock
	private DiscountModel appliedVoucherMock;
	@Mock
	private ProductModel productCpaMock;
	@Mock
	private ProductModel productCmaMock;
	@Mock
	private CategoryModel categoryCpaMock;
	@Mock
	private CategoryModel categoryCmaMock;
	@Mock
	private DeliveryModeModel deliveryModeModelMock;
	@Mock
	private CurrencyModel currencyModelMock;
	@Mock
	private EmployeeModel employeeModelMock;
	@Mock
	private WileyPartnerService partnerService;
	@Mock
	private WileyDiscountCalculationService wileyDiscountCalculationService;

	@InjectMocks
	private OrderToEbpOrderInfoPopulator testInstance;

	@Before
	public void setUp() throws Exception
	{
		when(orderModelMock.getDate()).thenReturn(new Date());
		when(orderModelMock.getDeliveryMode()).thenReturn(deliveryModeModelMock);
		when(voucherService.getAppliedVouchers(orderModelMock)).thenReturn(Arrays.asList(appliedVoucherMock));
		when(voucherService.getAppliedVoucherCodes(orderModelMock)).thenReturn(Arrays.asList(TEST_VOUCHER_CODE));
		when(partnerService.getPartnerCode(orderModelMock)).thenReturn(Optional.of(TEST_PARTNER_CODE));
		when(wileyCheckoutService.isNonZeroOrder(orderModelMock)).thenReturn(TEST_IS_NON_ZERO_ORDER);

		//mock order entries
		when(orderModelMock.getEntries()).thenReturn(Arrays.asList(entryCpaMock, entryCmaMock));
		when(entryCpaMock.getProduct()).thenReturn(productCpaMock);
		when(entryCmaMock.getProduct()).thenReturn(productCmaMock);

		when(ebpAddProductsPayload.getOrder()).thenReturn(orderModelMock);

		//mock primary categories
		when(categoryService.getPrimaryWileyCategoryForProduct(productCpaMock)).thenReturn(categoryCpaMock);
		when(categoryService.getPrimaryWileyCategoryForProduct(productCmaMock)).thenReturn(categoryCmaMock);
		when(categoryCpaMock.getName(Locale.ENGLISH)).thenReturn(CPA_CODE);
		when(categoryCmaMock.getName(Locale.ENGLISH)).thenReturn(CMA_CODE);
		when(orderModelMock.getCurrency()).thenReturn(currencyModelMock);
		when(currencyModelMock.getDigits()).thenReturn(DOUBLE_PRECISION_SCALE);

		Mockito.when(sessionService.executeInLocalView(Mockito.any(SessionExecutionBody.class), Mockito.any(EmployeeModel.class)))
				.thenAnswer(invocation -> {
					final SessionExecutionBody args = (SessionExecutionBody) invocation.getArguments()[0];
					return args.execute();
				});
	}

	@Test
	public void shouldPopulateManualOrderPropertyFromOrder() throws Exception
	{
		//Given
		when(orderModelMock.getPlacedBy()).thenReturn(employeeModelMock);
		final OrderNode orderNode = new OrderNode();
		//Testing
		testInstance.populate(ebpAddProductsPayload, orderNode);
		//Verify
		assertTrue(orderNode.isManualOrder());
	}

	@Test
	public void shouldPopulateTotalTaxWithRounding()
	{
		//Given
		when(orderModelMock.getTotalTax()).thenReturn(TEST_TOTAL_TAX);
		final OrderNode orderNode = new OrderNode();
		//When
		testInstance.populate(ebpAddProductsPayload, orderNode);
		//Then
		assertEquals(roundValue(TEST_TOTAL_TAX), orderNode.getTaxAmount());
	}

	@Test
	public void shouldPopulateTotalDiscount()
	{
		//Given
		when(wileyDiscountCalculationService.getTotalDiscount(orderModelMock)).thenReturn(TEST_TOTAL_DISCOUNT);
		final OrderNode orderNode = new OrderNode();
		//When
		testInstance.populate(ebpAddProductsPayload, orderNode);
		//Then
		assertEquals(roundValue(TEST_TOTAL_DISCOUNT), orderNode.getDiscountAmount());
	}

	@Test
	public void shouldPopulateTotalPriceWithRounding()
	{
		//Given
		when(orderModelMock.getTotalPrice()).thenReturn(TEST_TOTAL_PRICE);
		when(orderModelMock.getTotalTax()).thenReturn(TEST_TOTAL_TAX);

		final OrderNode orderNode = new OrderNode();
		//When
		testInstance.populate(ebpAddProductsPayload, orderNode);
		//Then
		assertEquals(roundValue(TEST_TOTAL_PRICE).add(roundValue(TEST_TOTAL_TAX)), orderNode.getTotalAmount());
	}

	@Test
	public void shouldPopulateDeliveryCostWithRounding()
	{
		//Given
		when(orderModelMock.getDeliveryCost()).thenReturn(TEST_DELIVERY_COST);
		final OrderNode orderNode = new OrderNode();
		//When
		testInstance.populate(ebpAddProductsPayload, orderNode);
		//Then
		assertEquals(roundValue(TEST_DELIVERY_COST), orderNode.getShippingAmount());
	}



	private BigDecimal roundValue(final double value)
	{
		return BigDecimal.valueOf(value).setScale(DOUBLE_PRECISION_SCALE, BigDecimal.ROUND_HALF_DOWN);
	}

	@Test
	public void shouldPopulateOrderProperties() throws Exception
	{
		Date testDate = new Date(getTime(TEST_DATE, WileyCoreConstants.WILEY_EBP_DATE_TIME_FORMAT_NAME));
		//Given
		when(orderModelMock.getCode()).thenReturn(TEST_CODE);
		when(orderModelMock.getDate()).thenReturn(testDate);

		final OrderNode orderNode = new OrderNode();
		//Testing
		testInstance.populate(ebpAddProductsPayload, orderNode);
		//Verify
		assertEquals(TEST_CODE, orderNode.getOrderNumber());
		assertEquals(testDate.getTime(),
				getTime(orderNode.getOrderDateTime(), WileyCoreConstants.WILEY_EBP_DATE_TIME_FORMAT_NAME));
		assertEquals(Arrays.asList(new String[] { TEST_VOUCHER_CODE, TEST_PARTNER_CODE }), orderNode.getCoupons());
		assertEquals(CPA_CODE + EBP_SOURCE_SEPARATOR + CMA_CODE, orderNode.getSource());
		assertEquals(WileyEbpPaymentMethod.CREDIT_CARD.getValue(), orderNode.getPaymentMethod());
	}

	@Test
	public void populateForNonPartnerOrder()
	{
		// Given
		when(partnerService.getPartnerCode(orderModelMock)).thenReturn(Optional.empty());

		// When
		final OrderNode orderNode = new OrderNode();
		testInstance.populate(ebpAddProductsPayload, orderNode);

		// Then
		assertEquals(Arrays.asList(new String[] { TEST_VOUCHER_CODE }), orderNode.getCoupons());
	}

	@Test
	public void shouldNotDuplicateCategoryCode()
	{
		//Given
		when(orderModelMock.getEntries()).thenReturn(Arrays.asList(entryCmaMock, entryCmaMock));
		final OrderNode orderNode = new OrderNode();
		//Testing
		testInstance.populate(ebpAddProductsPayload, orderNode);
		//Verify
		assertEquals(CMA_CODE, orderNode.getSource());
	}

	@Test
	public void shouldNotSetDeliveryModeForDigitalProduct() throws Exception
	{
		//Given
		when(orderModelMock.getDeliveryMode()).thenReturn(null);
		final OrderNode orderNode = new OrderNode();
		//Testing
		testInstance.populate(ebpAddProductsPayload, orderNode);
		//Verify
		assertNull(orderNode.getShippingMethod());
	}

	private long getTime(final String date, final String pattern) throws ParseException
	{
		SimpleDateFormat formatter = new SimpleDateFormat(pattern, Locale.US);
		Date resultDate = formatter.parse(date);
		return resultDate.getTime();
	}

}
