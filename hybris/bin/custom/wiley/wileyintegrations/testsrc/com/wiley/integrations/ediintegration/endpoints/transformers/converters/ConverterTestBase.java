package com.wiley.integrations.ediintegration.endpoints.transformers.converters;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.util.TaxValue;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mockito.Mock;

import com.wiley.core.model.WileyGiftCardProductModel;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class ConverterTestBase
{
	public static final String FIRST_NAME = "fname";
	public static final String LAST_NAME = "lname";
	public static final String ADDRESS_LINE_1 = "addr line 1";
	public static final String ADDRESS_LINE_2 = "addr line 2";

	public static final String COUNTRY_NUMERIC = "840";
	public static final String STATE = "21";
	public static final String POST_CODE = "postCode";
	public static final String ORDER_CODE = "0000001";

	@Mock
	protected OrderModel orderModel;
	@Mock
	private AddressModel paymentAddress;
	@Mock
	private CountryModel countryModel;
	@Mock
	private RegionModel regionModel;
	@Mock
	protected AbstractOrderEntryModel orderEntryModel;
	@Mock
	protected AbstractOrderEntryModel giftCardOrderEntryModel;
	@Mock
	private ProductModel productModel;
	@Mock
	private WileyGiftCardProductModel giftCardProductModel;
	@Mock
	private TaxValue taxValue;
	@Mock
	private DiscountValue discountValue;

	public OrderModel givenOrder()
	{
		when(orderModel.getCode()).thenReturn(ORDER_CODE);
		givenOrderModelWithPaymentAddress();
		givenOrderWithEntry();
		givenOrderWithSuccessfulPayment();
		return orderModel;
	}

	protected OrderModel givenOrderModelWithPaymentAddress()
	{
		when(orderModel.getPaymentAddress()).thenReturn(paymentAddress);
		when(paymentAddress.getFirstname()).thenReturn(FIRST_NAME);
		when(paymentAddress.getLastname()).thenReturn(LAST_NAME);
		when(paymentAddress.getLine1()).thenReturn(ADDRESS_LINE_1);
		when(paymentAddress.getLine2()).thenReturn(ADDRESS_LINE_2);

		when(paymentAddress.getRegion()).thenReturn(regionModel);
		when(regionModel.getIsocodeShort()).thenReturn(STATE);

		when(paymentAddress.getPostalcode()).thenReturn(POST_CODE);

		when(paymentAddress.getCountry()).thenReturn(countryModel);
		when(countryModel.getNumeric()).thenReturn(COUNTRY_NUMERIC);

		return orderModel;
	}

	public static final String PRODUCT_CODE = "productCode";
	public static final String PRODUCT_ISBN = "productISBN";
	public static final String GIFT_CARD_PRODUCT_ISBN = "giftCardProductISBN";

	public static final Long PRODUCT_ENTRY_QUANTITY = 15L;
	public static final Double PRODUCT_ENTRY_BASE_PRICE = 15.30;
	public static final Double PRODUCT_ENTRY_TAX_VALUE = 5.01;
	public static final Double PRODUCT_ENTRY_DISCOUNT_VALUE = 5.55;


	protected void givenOrderWithEntry()
	{
		givenProduct(productModel, PRODUCT_CODE, PRODUCT_ISBN);
		givenOrderEntryWithProduct(orderEntryModel, productModel);
		givenOrderWithEntries(orderEntryModel);
	}

	protected void givenOrderWithEntries()
	{
		givenOrderEntries();
		givenOrderWithEntries(orderEntryModel, giftCardOrderEntryModel);
	}

	protected void givenOrderEntries()
	{
		givenProduct(productModel, PRODUCT_CODE, PRODUCT_ISBN);
		givenProduct(giftCardProductModel, PRODUCT_CODE, GIFT_CARD_PRODUCT_ISBN);
		givenOrderEntryWithProduct(orderEntryModel, productModel);
		givenOrderEntryWithProduct(giftCardOrderEntryModel, giftCardProductModel);
	}

	protected OrderModel givenOrderWithEntries(final AbstractOrderEntryModel... entries)
	{
		when(orderModel.getEntries()).thenReturn(Arrays.asList(entries));
		return orderModel;
	}

	protected AbstractOrderEntryModel givenOrderEntryWithProduct(final AbstractOrderEntryModel orderEntryModel,
			final ProductModel productModel)
	{
		when(orderEntryModel.getProduct()).thenReturn(productModel);

		when(orderEntryModel.getQuantity()).thenReturn(PRODUCT_ENTRY_QUANTITY);
		when(orderEntryModel.getBasePrice()).thenReturn(PRODUCT_ENTRY_BASE_PRICE);
		when(orderEntryModel.getOrder()).thenReturn(orderModel);
		when(taxValue.getValue()).thenReturn(PRODUCT_ENTRY_TAX_VALUE);
		when(orderEntryModel.getTaxValues()).thenReturn(Collections.singletonList(taxValue));

		when(orderEntryModel.getDiscountValues()).thenReturn(Collections.singletonList(discountValue));
		when(discountValue.getAppliedValue()).thenReturn(PRODUCT_ENTRY_DISCOUNT_VALUE);
		return orderEntryModel;
	}

	protected ProductModel givenProduct(final ProductModel productModel, final String productCode,
			final String productIsbn)
	{
		when(productModel.getCode()).thenReturn(productCode);
		when(productModel.getIsbn()).thenReturn(productIsbn);
		return productModel;
	}


	public static final String PAYMENT_TRANSACTION_CODE = "PAYMENT_TRANSACTION_CODE";

	protected void givenOrderWithSuccessfulPayment()
	{
		List<PaymentTransactionModel> transactionModelList =
				Collections.singletonList(givenPaymentTransactionModelWithSuccessfulPayment());
		when(orderModel.getPaymentTransactions()).thenReturn(transactionModelList);
	}

	protected PaymentTransactionModel givenPaymentTransactionModelWithSuccessfulPayment()
	{
		PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);

		PaymentTransactionEntryModel successfulTransactionModel = mock(PaymentTransactionEntryModel.class);
		when(successfulTransactionModel.getType()).thenReturn(PaymentTransactionType.CAPTURE);
		when(successfulTransactionModel.getTransactionStatus()).thenReturn(TransactionStatus.ACCEPTED.name());
		when(successfulTransactionModel.getCode()).thenReturn(PAYMENT_TRANSACTION_CODE);

		PaymentTransactionEntryModel notSuccessfulTransactionModel = mock(PaymentTransactionEntryModel.class);
		when(notSuccessfulTransactionModel.getType()).thenReturn(PaymentTransactionType.AUTHORIZATION);
		when(notSuccessfulTransactionModel.getTransactionStatus()).thenReturn(TransactionStatus.REVIEW.name());

		when(paymentTransactionModel.getEntries()).thenReturn(Arrays.asList(successfulTransactionModel,
				notSuccessfulTransactionModel));
		return paymentTransactionModel;
	}

	public static final String CURRENCY_ISO = "USD";

	protected CurrencyModel givenCurrencyModel()
	{
		CurrencyModel currencyModel = mock(CurrencyModel.class);
		when(currencyModel.getIsocode()).thenReturn(CURRENCY_ISO);
		return currencyModel;
	}

	public static final String USER_UID = "superadmin";

	protected UserModel givenUser()
	{
		UserModel userModel = mock(UserModel.class);
		when(userModel.getUid()).thenReturn(USER_UID);
		return userModel;
	}

	protected void setAddressFieldsLengthRestrictions(final Populator populator)
			throws NoSuchFieldException, IllegalAccessException
	{
		Map<String, Integer> addressFieldsLengthRestrictions = new HashMap<>();
		addressFieldsLengthRestrictions.put("customerName", 30);
		addressFieldsLengthRestrictions.put("customerName2", 30);
		addressFieldsLengthRestrictions.put("addressLine1", 30);
		addressFieldsLengthRestrictions.put("addressLine2", 30);
		addressFieldsLengthRestrictions.put("cityName", 24);
		addressFieldsLengthRestrictions.put("zipPostCode", 10);

		Field field = BillToCustomerPopulator.class.getDeclaredField("addressFieldsLengthRestrictions");
		field.setAccessible(true);
		field.set(populator, addressFieldsLengthRestrictions);
	}
}
