package com.wiley.integrations.alm.strategy.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.spring.security.CoreUserDetails;
import de.hybris.platform.spring.security.CoreUserDetailsService;
import de.hybris.platform.validation.services.ValidationService;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.core.userdetails.UserDetails;

import com.wiley.core.integration.alm.user.dto.UserDto;
import com.wiley.core.integration.alm.user.service.AlmUserService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AlmUserAutoCreationStrategyUnitTest
{
	private static final String ALM_ID = "TEST_ALM_ID";

	@InjectMocks
	private AlmUserAutoCreationStrategyImpl testInstance;

	@Mock
	private CoreUserDetailsService coreUserDetailsService;

	@Mock
	private Converter<UserDto, CustomerModel> wileyCustomerModelReverseConverter;

	@Mock
	private CustomerAccountService customerAccountService;

	@Mock
	private AlmUserService almUserGatewayService;

	@Mock
	private ValidationService validationService;

	@Mock
	private UserDto userMock;

	@Mock
	private CustomerModel customerMock;

	@Mock
	private CoreUserDetails userDetailsMock;

	@Before
	public void prepare() throws DuplicateUidException
	{
		when(userDetailsMock.getUsername()).thenReturn(ALM_ID);

		when(almUserGatewayService.getUserData(ALM_ID)).thenReturn(userMock);
		when(validationService.validate(userMock)).thenReturn(Collections.emptySet());
		when(wileyCustomerModelReverseConverter.convert(userMock)).thenReturn(customerMock);
		doNothing().when(customerAccountService).register(customerMock, null);
		when(coreUserDetailsService.loadUserByUsername(ALM_ID)).thenReturn(userDetailsMock);
	}

	@Test
	public void createNewUserSuccessfulTest()
	{
		// given

		// when
		UserDetails result = testInstance.createNewUserByAlmId(ALM_ID);

		// then
		assertEquals(ALM_ID, result.getUsername());
	}

	@Test(expected = AssertionError.class)
	public void createNewUserNullParamTest()
	{
		testInstance.createNewUserByAlmId(null);
	}
}
