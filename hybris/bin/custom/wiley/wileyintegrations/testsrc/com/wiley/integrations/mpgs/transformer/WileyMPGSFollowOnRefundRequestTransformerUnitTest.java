package com.wiley.integrations.mpgs.transformer;

import de.hybris.bootstrap.annotations.UnitTest;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Currency;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.mpgs.constants.WileyMPGSConstants;
import com.wiley.core.mpgs.dto.refund.MPGSFollowOnRefundRequestDTO;
import com.wiley.core.payment.request.WileyFollowOnRefundRequest;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyMPGSFollowOnRefundRequestTransformerUnitTest
{
	public static final BigDecimal AMOUNT = BigDecimal.ONE;
	public static final String CURRENCY_CODE = "USD";

	@Mock
	private WileyFollowOnRefundRequest mockRequest;

	@InjectMocks
	@Spy
	private WileyMPGSFollowOnRefundRequestTransformer transformer = new WileyMPGSFollowOnRefundRequestTransformer();

	@Test
	public void successfulResult()
	{
		when(mockRequest.getTotalAmount()).thenReturn(AMOUNT);
		Currency currency = Currency.getInstance(CURRENCY_CODE);
		when(mockRequest.getCurrency()).thenReturn(currency);

		MPGSFollowOnRefundRequestDTO requestDTO = transformer.transform(mockRequest);

		assertEquals(WileyMPGSConstants.API_OPERATAION_REFUND, requestDTO.getApiOperation());
		assertEquals(AMOUNT, requestDTO.getTransaction().getAmount());
		assertEquals(CURRENCY_CODE, requestDTO.getTransaction().getCurrency());
	}

}
