package com.wiley.integrations.hotfolder;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTest;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.util.Config;
import de.hybris.platform.voucher.VoucherService;
import de.hybris.platform.voucher.model.DateRestrictionModel;
import de.hybris.platform.voucher.model.ProductRestrictionModel;
import de.hybris.platform.voucher.model.PromotionVoucherModel;
import de.hybris.platform.voucher.model.RestrictionModel;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.stream.Stream;

import javax.annotation.Resource;

import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;

import com.google.common.collect.Sets;
import com.wiley.core.AbstractWileyServicelayerTransactionalTest;


@IntegrationTest
public class WileycomDiscountCodesHotFolderIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	private static final String ENCODING = "UTF-8";
	private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

	private static final String CSV_FOR_TEST_VOUCHER_CREATE = "promotion-vouchers-1454347817.csv";
	private static final String CSV_FOR_TEST_VOUCHER_UPDATE = "promotion-vouchers-1454347818.csv";
	private static final String CSV_FOR_TEST_WITH_BAD_LINE = "promotion-vouchers-1454347800.csv";
	private static final String CSV_FOR_TEST_CORRUPTED = "promotion-vouchers-1454999800.csv";

	private static final SimpleDateFormat FROMATTER = new SimpleDateFormat(DATE_FORMAT);


	private String hotFolderPath;

	@Resource(name = "batchFilesWileycomDiscountCodesProc")
	private MessageChannel xmlProcFile;

	@Resource
	private VoucherService voucherService;

	@Resource
	private CommonI18NService commonI18NService;

	@Mock
	PromotionVoucherModel voucherMock1;
	@Mock
	ProductRestrictionModel productRestrictionModelMock1;
	@Mock
	DateRestrictionModel dateRestrictionModelMock1;
	@Mock
	PromotionVoucherModel voucherMock2;
	@Mock
	ProductRestrictionModel productRestrictionModelMock2;
	@Mock
	DateRestrictionModel dateRestrictionModelMock2;
	@Mock
	ProductModel productModelMock1;
	@Mock
	ProductModel productModelMock2;
	@Mock
	ProductModel productModelMock3;


	@Before
	public void setUp() throws IOException, ImpExException
	{
		MockitoAnnotations.initMocks(this);
		
		setHotFolderPath(Config.getString("wiley.impex.basefolder", null) + "/junit/promotion_voucher");
		FileUtils.cleanDirectory(new File(getHotFolderPath()));

		commonI18NService.setCurrentLanguage(commonI18NService.getLanguage("en"));

		importCsv("/wileycore/test/promotion_voucher/products.impex", ENCODING); //test products, vouchers will be linked to

		doReturn("222222222221").when(productModelMock1).getIsbn();
		doReturn("222222222222").when(productModelMock2).getIsbn();
		doReturn("222222222223").when(productModelMock3).getIsbn();
	}

	@Test
	public void checkNormalImport() throws Exception
	{
		// Given
		doReturn(1).when(voucherMock1).getRedemptionQuantityLimit();
		doReturn(1).when(voucherMock1).getRedemptionQuantityLimitPerUser();
		doReturn(12d).when(voucherMock1).getValue();
		doReturn("1111").when(voucherMock1).getDescription();
		doReturn("New Promotional Voucher1").when(voucherMock1).getName();
		doReturn(null).when(voucherMock1).getCurrency();
		doReturn("222-B5K6-EYCB-6GBP").when(voucherMock1).getVoucherCode();
		doReturn(1).when(voucherMock1).getPriority();
		doReturn(true).when(voucherMock1).getFreeShipping();
		doReturn(Sets.newHashSet(productRestrictionModelMock1, dateRestrictionModelMock1)).when(voucherMock1).getRestrictions();
		doReturn(Arrays.asList(productModelMock1, productModelMock3)).when(productRestrictionModelMock1).getProducts();
		doReturn(FROMATTER.parse("2015-12-22 10:22:33")).when(dateRestrictionModelMock1).getStartDate();
		doReturn(FROMATTER.parse("2015-12-22 12:22:33")).when(dateRestrictionModelMock1).getEndDate();

		doReturn(1).when(voucherMock2).getRedemptionQuantityLimit();
		doReturn(1).when(voucherMock2).getRedemptionQuantityLimitPerUser();
		doReturn(10d).when(voucherMock2).getValue();
		doReturn("Christmas Offer").when(voucherMock2).getDescription();
		doReturn("New Promotional Voucher3").when(voucherMock2).getName();
		doReturn(commonI18NService.getCurrency("USD")).when(voucherMock2).getCurrency();
		doReturn("CHRISTMAS").when(voucherMock2).getVoucherCode();
		doReturn(2).when(voucherMock2).getPriority();
		doReturn(false).when(voucherMock2).getFreeShipping();
		doReturn(Sets.newHashSet(productRestrictionModelMock2, dateRestrictionModelMock2)).when(voucherMock2).getRestrictions();
		doReturn(Arrays.asList(productModelMock1)).when(productRestrictionModelMock2).getProducts();
		doReturn(FROMATTER.parse("2015-12-22 10:22:33")).when(dateRestrictionModelMock2).getStartDate();
		doReturn(FROMATTER.parse("2015-12-27 12:22:33")).when(dateRestrictionModelMock2).getEndDate();

		assertEquals(0, getAllVouchers().length);

		// Testing
		runTesting(CSV_FOR_TEST_VOUCHER_CREATE);

		// Verify
		final Object[] vouchers = getAllVouchers();
		assertEquals(2, vouchers.length); //exactly 2 vouchers have been imported

		compareVouchers(voucherMock1, (PromotionVoucherModel) vouchers[0]);
		compareVouchers(voucherMock2, (PromotionVoucherModel) vouchers[1]);
		
		File archive = new File(hotFolderPath + "/archive");
		assertEquals(1, archive.list().length); //the CSV has been moved to archive folder
		File sourceCsvs = new File(hotFolderPath);
		assertEquals(0, sourceCsvs.listFiles(File::isFile).length); //...from promotion_voucher folder
		
		File processing = new File(hotFolderPath + "/processing");
		assertEquals(0, processing.list().length); //the CSV has been moved out from processing folder
	}
	
	@Test
	public void checkVoucherUpdate() throws Exception
	{
		// Given
		doReturn(1).when(voucherMock1).getRedemptionQuantityLimit();
		doReturn(1).when(voucherMock1).getRedemptionQuantityLimitPerUser();
		doReturn(12d).when(voucherMock1).getValue();
		doReturn("1111").when(voucherMock1).getDescription();
		doReturn("New Promotional Voucher1").when(voucherMock1).getName();
		doReturn(null).when(voucherMock1).getCurrency();
		doReturn("222-B5K6-EYCB-6GBP").when(voucherMock1).getVoucherCode();
		doReturn(1).when(voucherMock1).getPriority();
		doReturn(true).when(voucherMock1).getFreeShipping();
		doReturn(Sets.newHashSet(productRestrictionModelMock1, dateRestrictionModelMock1)).when(voucherMock1).getRestrictions();
		doReturn(Arrays.asList(productModelMock1, productModelMock3)).when(productRestrictionModelMock1).getProducts();
		doReturn(FROMATTER.parse("2015-12-22 10:22:33")).when(dateRestrictionModelMock1).getStartDate();
		doReturn(FROMATTER.parse("2015-12-22 12:22:33")).when(dateRestrictionModelMock1).getEndDate();

		doReturn(12).when(voucherMock2).getRedemptionQuantityLimit();
		doReturn(12).when(voucherMock2).getRedemptionQuantityLimitPerUser();
		doReturn(102d).when(voucherMock2).getValue();
		doReturn("Christmas Offer2").when(voucherMock2).getDescription();
		doReturn("New Promotional Voucher32").when(voucherMock2).getName();
		doReturn(commonI18NService.getCurrency("USD")).when(voucherMock2).getCurrency();
		doReturn("CHRISTMAS2").when(voucherMock2).getVoucherCode();
		doReturn(3).when(voucherMock2).getPriority();
		doReturn(true).when(voucherMock2).getFreeShipping();
		doReturn(Sets.newHashSet(productRestrictionModelMock2, dateRestrictionModelMock2)).when(voucherMock2).getRestrictions();
		doReturn(Arrays.asList(productModelMock2)).when(productRestrictionModelMock2).getProducts();
		doReturn(FROMATTER.parse("2015-12-23 10:22:33")).when(dateRestrictionModelMock2).getStartDate();
		doReturn(FROMATTER.parse("2015-12-28 12:22:33")).when(dateRestrictionModelMock2).getEndDate();
 
		assertEquals(0, getAllVouchers().length);

		// Testing
		runTesting(CSV_FOR_TEST_VOUCHER_CREATE);
		runTesting(CSV_FOR_TEST_VOUCHER_UPDATE);

		// Verify
		final Object[] vouchers = getAllVouchers();
		assertEquals(2, vouchers.length); //exactly 2 vouchers have been imported and updated

		compareVouchers(voucherMock1, (PromotionVoucherModel) vouchers[0]);
		compareVouchers(voucherMock2, (PromotionVoucherModel) vouchers[1]); //updated one

		File archive = new File(hotFolderPath + "/archive");
		assertEquals(2, archive.list().length); //both CSVs have been moved to archive folder
		File sourceCsvs = new File(hotFolderPath);
		assertEquals(0, sourceCsvs.listFiles(File::isFile).length); //...from promotion_voucher folder

		File processing = new File(hotFolderPath + "/processing");
		assertEquals(0, processing.list().length); //the CSV has been moved out from processing folder
	}

	@Test
	public void checkCsvWithBadLine() throws Exception
	{
		// Given	
		doReturn(1).when(voucherMock1).getRedemptionQuantityLimit();
		doReturn(1).when(voucherMock1).getRedemptionQuantityLimitPerUser();
		doReturn(12d).when(voucherMock1).getValue();
		doReturn("1111").when(voucherMock1).getDescription();
		doReturn("New Promotional Voucher1").when(voucherMock1).getName();
		doReturn(null).when(voucherMock1).getCurrency();
		doReturn("222-B5K6-EYCB-6GBP").when(voucherMock1).getVoucherCode();
		doReturn(1).when(voucherMock1).getPriority();
		doReturn(true).when(voucherMock1).getFreeShipping();
		doReturn(Sets.newHashSet(productRestrictionModelMock1, dateRestrictionModelMock1)).when(voucherMock1).getRestrictions();
		doReturn(Arrays.asList(productModelMock1, productModelMock3)).when(productRestrictionModelMock1).getProducts();
		doReturn(FROMATTER.parse("2015-12-22 10:22:33")).when(dateRestrictionModelMock1).getStartDate();
		doReturn(FROMATTER.parse("2015-12-22 12:22:33")).when(dateRestrictionModelMock1).getEndDate();

		assertEquals(0, getAllVouchers().length);

		// Testing
		runTesting(CSV_FOR_TEST_WITH_BAD_LINE);

		// Verify
		final Object[] vouchers = getAllVouchers();
		assertEquals(1, vouchers.length); //not all vouchers have been imported successfully

		compareVouchers(voucherMock1, (PromotionVoucherModel) vouchers[0]);

		File archive = new File(hotFolderPath + "/archive");
		assertEquals(1, archive.list().length); //move to archive
		File sourceCsvs = new File(hotFolderPath);
		assertEquals(0, sourceCsvs.listFiles(File::isFile).length); //...from promotion_voucher folder
		
		File processing = new File(hotFolderPath + "/processing");
		assertEquals(0, processing.list().length); //the CSV has been moved out from processing folder
	}


	protected void runTesting(final String csvFileForTest) throws IOException
	{
		File dest = copyFileToHotFolder("/wileycore/test/promotion_voucher/", csvFileForTest);

		xmlProcFile.send(MessageBuilder.withPayload(dest).build());
	}

	protected Object[] getAllVouchers()
	{
		return voucherService.getAllVouchers().toArray();
	}

	protected void compareVouchers(final PromotionVoucherModel expectedVoucher, final PromotionVoucherModel actualVoucher)
	{
		assertEquals(expectedVoucher.getRedemptionQuantityLimit(),        actualVoucher.getRedemptionQuantityLimit());
		assertEquals(expectedVoucher.getRedemptionQuantityLimitPerUser(), actualVoucher.getRedemptionQuantityLimitPerUser());
		assertEquals(expectedVoucher.getValue(), 						  actualVoucher.getValue());
		assertEquals(expectedVoucher.getDescription(), 					  actualVoucher.getDescription());
		assertEquals(expectedVoucher.getName(), 						  actualVoucher.getName());
		assertEquals(expectedVoucher.getCurrency(), 					  actualVoucher.getCurrency());
		assertEquals(expectedVoucher.getVoucherCode(), 					  actualVoucher.getVoucherCode());
		assertEquals(expectedVoucher.getPriority(), 					  actualVoucher.getPriority());
		assertEquals(expectedVoucher.getFreeShipping(), 				  actualVoucher.getFreeShipping());
		//compare restrictions
		assertEquals(expectedVoucher.getRestrictions().size(), actualVoucher.getRestrictions().size());

		final ArrayList<RestrictionModel> expectedRestrictions = new ArrayList<>();
		expectedRestrictions.addAll(expectedVoucher.getRestrictions());
		expectedRestrictions.sort(new RestrictionComparator());
		final ArrayList<RestrictionModel> actualRestrictions = new ArrayList<>();
		actualRestrictions.addAll(actualVoucher.getRestrictions());
		actualRestrictions.sort(new RestrictionComparator());

		final Iterator<RestrictionModel> iteratorForExpected = expectedRestrictions.iterator();
		final Iterator<RestrictionModel> iteratorForActual = actualRestrictions.iterator();
		while (iteratorForExpected.hasNext())
		{
			final RestrictionModel expected = iteratorForExpected.next();
			final RestrictionModel actual = iteratorForActual.next();
			assertEquals(expected.getRestrictionType(), actual.getRestrictionType());
			if (expected instanceof ProductRestrictionModel)
			{
				final Stream<String> expectedIsbns = ((ProductRestrictionModel) expected).getProducts().stream().map(
						product -> product.getIsbn());
				final Stream<String> actualIsbns = ((ProductRestrictionModel) actual).getProducts().stream().map(
						product -> product.getIsbn());
				assertArrayEquals(expectedIsbns.toArray(), actualIsbns.toArray());
			}
			if (expected instanceof DateRestrictionModel)
			{
				assertEquals(((DateRestrictionModel) expected).getStartDate(), ((DateRestrictionModel) actual).getStartDate());
				assertEquals(((DateRestrictionModel) expected).getEndDate(), ((DateRestrictionModel) actual).getEndDate());
			}
		}
	}


	protected File copyFileToHotFolder(final String path, final String fileName) throws IOException
	{
		InputStream inputStream = ServicelayerTest.class.getResourceAsStream(path + fileName);
		File dest = new File(getHotFolderPath() + "/processing/" + fileName);
		FileUtils.copyInputStreamToFile(inputStream, dest);
		return dest;
	}

	public String getHotFolderPath()
	{
		return hotFolderPath;
	}

	public void setHotFolderPath(final String hotFolderPath)
	{
		this.hotFolderPath = hotFolderPath;
	}


	private class RestrictionComparator implements Comparator<RestrictionModel>
	{
		@Override
		public int compare(final RestrictionModel a, final RestrictionModel b)
		{
			if (a.getClass().equals(b.getClass()))
			{
				if (a instanceof DateRestrictionModel)
				{
					final DateRestrictionModel dateA = (DateRestrictionModel) a;
					final DateRestrictionModel dateB = (DateRestrictionModel) b;
					if (dateA.getStartDate().equals(dateB.getStartDate()))
					{
						return dateA.getEndDate().compareTo(dateB.getEndDate());
					}
					else
					{
						return dateA.getStartDate().compareTo(dateB.getStartDate());
					}
				}
			}
			else
			{
				return (a instanceof DateRestrictionModel) ? 1 : -1;
			}
			return 0;
		}
	}
}
