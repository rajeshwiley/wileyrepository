package com.wiley.integrations.customer.serviceactivator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.validation.ConstraintViolationException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.util.savedvalues.ItemModificationHistoryService;
import com.wiley.core.util.savedvalues.ItemModificationInfo;
import com.wiley.core.wileyas.user.service.WileyasUserService;
import com.wiley.integrations.users.data.UserModificationWsDto;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyUserProfileServiceActivatorUnitTest
{
	private static final String USER_ID = "user_id";
	private static final String FIRST_NAME = "first_name";
	private static final String LAST_NAME = "last_name";
	private static final String EMAIL = "email";

	private UserModificationWsDto dto;

	@Mock
	private WileyasUserService wileyasUserServiceMock;
	@Mock
	private ModelService modelServiceMock;
	@Mock
	private CustomerModel customerModelMock;
	@Mock
	private ItemModificationHistoryService wileyItemModificationHistoryService;
	@Mock
	private ItemModificationInfo itemModificationInfoMock;

	private WileyUserProfileServiceActivator activator;

	@Before
	public void setUp()
	{
		// inject mocks doesn't work in this case
		activator = new WileyUserProfileServiceActivator()
		{
			@Override
			protected WileyasUserService getWileyasUserService()
			{
				return wileyasUserServiceMock;
			}

			@Override
			protected ModelService getModelService()
			{
				return modelServiceMock;
			}

			@Override
			protected ItemModificationHistoryService getWileyItemModificationHistoryService()
			{
				return wileyItemModificationHistoryService;
			}
		};

		dto = new UserModificationWsDto();
		dto.setUserId(USER_ID);
		dto.setFirstName(FIRST_NAME);
		dto.setLastName(LAST_NAME);
		dto.setEmail(EMAIL);
		when(customerModelMock.getFirstName()).thenReturn(FIRST_NAME);
		when(customerModelMock.getLastName()).thenReturn(LAST_NAME);
	}

	@Test
	public void testValidMaxSize()
	{
		when(wileyasUserServiceMock.isUserExisting(getLong255String(), CustomerModel.class)).thenReturn(false);

		dto.setUserId(getLong255String());
		dto.setFirstName(getLong255String());
		dto.setLastName(getLong255String());
		dto.setEmail(getLong255String());
		activator.update(dto);

		verify(wileyasUserServiceMock, never()).getUserForUID(getLong255String());
		verify(customerModelMock, never()).setFirstName(dto.getFirstName());
		verify(customerModelMock, never()).setLastName(dto.getLastName());
		verify(customerModelMock, never()).setName(any());
		verify(customerModelMock, never()).setEmailAddress(dto.getEmail());
		verify(modelServiceMock, never()).save(customerModelMock);
	}

	public void testObsoleteMessage()
	{
		when(wileyasUserServiceMock.isUserExisting(USER_ID, CustomerModel.class)).thenReturn(true);
		when(wileyasUserServiceMock.getUserForUID(USER_ID)).thenReturn(customerModelMock);

		activator.update(dto);

		verify(wileyasUserServiceMock, atLeastOnce()).getUserForUID(USER_ID);
		verify(customerModelMock, never()).setFirstName(dto.getFirstName());
		verify(customerModelMock, never()).setLastName(dto.getLastName());
		verify(customerModelMock, never()).setName(any());
		verify(customerModelMock, never()).setEmailAddress(dto.getEmail());
		verify(modelServiceMock, never()).save(customerModelMock);
	}

	@Test
	public void testValidMessage()
	{
		when(wileyasUserServiceMock.isUserExisting(USER_ID, CustomerModel.class)).thenReturn(true);
		when(wileyasUserServiceMock.getUserForUID(USER_ID)).thenReturn(customerModelMock);
		when(wileyItemModificationHistoryService.createModificationInfo(customerModelMock)).thenReturn(itemModificationInfoMock);

		activator.update(dto);

		verify(wileyasUserServiceMock, atLeastOnce()).getUserForUID(USER_ID);
		verify(customerModelMock, atLeastOnce()).getFirstName();
		verify(customerModelMock, atLeastOnce()).getLastName();
		verify(customerModelMock, atLeastOnce()).setFirstName(dto.getFirstName());
		verify(customerModelMock, atLeastOnce()).setLastName(dto.getLastName());
		verify(customerModelMock, atLeastOnce()).setName(any());
		verify(customerModelMock, atLeastOnce()).setEmailAddress(dto.getEmail());
		verify(modelServiceMock, atLeastOnce()).save(customerModelMock);
	}

	@Test
	public void testPartialUpdate()
	{
		dto.setFirstName(null);
		dto.setLastName(null);
		dto.setEmail(null);
		when(wileyasUserServiceMock.isUserExisting(USER_ID, CustomerModel.class)).thenReturn(true);
		when(wileyasUserServiceMock.getUserForUID(USER_ID)).thenReturn(customerModelMock);
		when(wileyItemModificationHistoryService.createModificationInfo(customerModelMock)).thenReturn(itemModificationInfoMock);

		activator.update(dto);

		verify(wileyasUserServiceMock, atLeastOnce()).getUserForUID(USER_ID);
		verify(customerModelMock, atLeastOnce()).getFirstName();
		verify(customerModelMock, atLeastOnce()).getLastName();
		verify(customerModelMock, never()).setFirstName(dto.getFirstName());
		verify(customerModelMock, never()).setLastName(dto.getLastName());
		verify(customerModelMock, never()).setName(any());
		verify(customerModelMock, never()).setEmailAddress(dto.getEmail());
		verify(modelServiceMock, atLeastOnce()).save(customerModelMock);
	}

	@Test(expected = ConstraintViolationException.class)
	public void testMandatoryUserId()
	{
		dto.setUserId(null);
		activator.update(dto);
	}

	@Test(expected = ConstraintViolationException.class)
	public void testInvalidUserIdSize()
	{
		dto.setUserId(getLong266String());
		activator.update(dto);
	}

	@Test(expected = ConstraintViolationException.class)
	public void testInvalidFirstNameSize()
	{
		dto.setFirstName(getLong266String());
		activator.update(dto);
	}

	@Test(expected = ConstraintViolationException.class)
	public void testInvalidLastNameSize()
	{
		dto.setLastName(getLong266String());
		activator.update(dto);
	}

	@Test(expected = ConstraintViolationException.class)
	public void testInvalidEmailSizeSize()
	{
		dto.setEmail(getLong266String());
		activator.update(dto);
	}

	private String getLong255String()
	{
		final StringBuilder sb = new StringBuilder();
		for (int i = 0; i < 255; i++)
		{
			sb.append("1");
		}
		return sb.toString();
	}

	private String getLong266String()
	{
		return getLong255String() + "1";
	}
}