package com.wiley.integrations.ediintegration.endpoints.transformers.converters;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.paypal.hybris.model.PaypalPaymentInfoModel;
import com.wiley.core.enums.CreditCardTypeCode;
import com.wiley.fulfilmentprocess.model.WileyExportProcessModel;
import com.wiley.core.integration.edi.EDIConstants;
import com.wiley.integrations.ediintegration.endpoints.jaxb.BillToCustomer;
import com.wiley.integrations.ediintegration.endpoints.jaxb.PurchaseOrder;
import com.wiley.integrations.ediintegration.endpoints.jaxb.ShipToCustomer;
import com.wiley.core.integration.edi.service.impl.WileyPurchaseOrderService;

import static java.util.Collections.singletonList;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PurchaseOrderPopulatorUnitTest extends ConverterTestBase
{

	@InjectMocks
	private PurchaseOrderPopulator testInstance = new PurchaseOrderPopulator();
	@Mock
	private ItemsStructurePopulator itemsStructurePopulator;
	@Mock
	private BillToCustomerPopulator billToCustomerPopulator;
	@Mock
	private ShipToCustomerPopulator shipToCustomerPopulator;
	@Mock
	private WileyPurchaseOrderService purchaseOrderService;

	private static final String TEST_CC_TOKEN = "T4012007494160026";
	private static final String HYBRIS_AUTHORIZATION_CODE = "HybrisAuthorizationCode";
	private static final String CREDIT_CARD_TYPE = CreditCardTypeCode.AX.toString();
	private static final String TEST_ORDER_TYPE = "CC";
	private static final Double DELIVERY_COST = 10.13;
	private static final String TRANSACTION_ID = "T234234234";
	private static final String CREDIT_CARD_EXPIRY = "0519";
	private static final String SHIP_METHOD_DONT = "DONT";


	@Test
	public void testPopulate()
	{
		//Given
		OrderModel givenOrder = givenOrder(givenCurrencyModel(), givenUser(),
				singletonList(givenPaymentTransactionModelWithSuccessfulPayment()));
		WileyExportProcessModel givenExportProcess = givenExportProcessWithOrder(givenOrder);
		when(purchaseOrderService.getOrderType(givenExportProcess)).thenReturn(TEST_ORDER_TYPE);
		when(purchaseOrderService.isOrderRefunded(givenExportProcess)).thenReturn(false);
		final CreditCardPaymentInfoModel paymentInfo = givenCreditCardPaymentInfoModel();
		when(givenOrder.getPaymentInfo()).thenReturn(paymentInfo);
		//Testing
		PurchaseOrder purchaseOrder = new PurchaseOrder();
		testInstance.populate(givenExportProcess, purchaseOrder);
		//Verify
		assertEquals(purchaseOrder.getCurrencyCode(), CURRENCY_ISO);
		assertEquals(purchaseOrder.getShippingChargeAmount(), DELIVERY_COST);
		assertEquals(purchaseOrder.getEmailAddress(), USER_UID);
		assertEquals(HYBRIS_AUTHORIZATION_CODE, purchaseOrder.getHybrisAuthorizationCode());
		assertEquals(TEST_CC_TOKEN, purchaseOrder.getCreditCardToken());
		assertEquals(TEST_ORDER_TYPE, purchaseOrder.getOrderType());

		verify(billToCustomerPopulator).populate(eq(givenOrder), any(BillToCustomer.class));
		verify(shipToCustomerPopulator).populate(eq(givenOrder), any(ShipToCustomer.class));
	}




	private CreditCardPaymentInfoModel givenCreditCardPaymentInfoModel()
	{
		CreditCardPaymentInfoModel creditCardPaymentInfoModel = mock(CreditCardPaymentInfoModel.class);
		when(purchaseOrderService.getCreditCardType(creditCardPaymentInfoModel)).thenReturn(CREDIT_CARD_TYPE);
		when(purchaseOrderService.buildCreditCardExpiry(creditCardPaymentInfoModel)).thenReturn(CREDIT_CARD_EXPIRY);
		return creditCardPaymentInfoModel;
	}

	@Test
	public void shouldSetCreditCardTypePPForPayPalOrders() {
		//Given
		OrderModel order = givenOrder(
				givenCurrencyModel(),
				givenUser(),
				singletonList(givenPaymentTransactionModelWithSuccessfulPayment())
		);
		WileyExportProcessModel exportProcess = givenExportProcessWithOrder(order);
		PaymentInfoModel paypalPaymentInfo = mock(PaypalPaymentInfoModel.class);
		when(order.getPaymentInfo()).thenReturn(paypalPaymentInfo);
		//When
		PurchaseOrder purchaseOrder = new PurchaseOrder();
		testInstance.populate(exportProcess, purchaseOrder);
		//Then
		assertEquals(EDIConstants.CARD_TYPE_PAYPAL, purchaseOrder.getCreditCardType());
	}

	@Test
	public void hybrisAuthorizationCodeShouldBeZeroStringForPayPal() {
		//Given
		OrderModel order = givenOrder(
				givenCurrencyModel(),
				givenUser(),
				singletonList(givenPaymentTransactionModelWithSuccessfulPayment())
		);
		WileyExportProcessModel exportProcess = givenExportProcessWithOrder(order);
		PaymentInfoModel paypalPaymentInfo = mock(PaypalPaymentInfoModel.class);
		when(order.getPaymentInfo()).thenReturn(paypalPaymentInfo);

		//When
		PurchaseOrder purchaseOrder = new PurchaseOrder();
		testInstance.populate(exportProcess, purchaseOrder);

		//Then
		assertEquals(PurchaseOrderPopulator.PP_HYBRIS_AUTH_CODE_VALUE, purchaseOrder.getHybrisAuthorizationCode());
	}


	private OrderModel givenOrder(final CurrencyModel currency, final UserModel user,
			final List<PaymentTransactionModel> paymentTransactions)
	{
		OrderModel orderModel = mock(OrderModel.class);
		when(purchaseOrderService.buildHybrisAuthorizationCode(orderModel)).thenReturn(HYBRIS_AUTHORIZATION_CODE);
		when(purchaseOrderService.getCCToken(orderModel)).thenReturn(TEST_CC_TOKEN);
		when(purchaseOrderService.buildShipMethod(orderModel)).thenReturn(SHIP_METHOD_DONT);

		when(orderModel.getCurrency()).thenReturn(currency);
		when(orderModel.getDeliveryCost()).thenReturn(DELIVERY_COST);
		when(orderModel.getUser()).thenReturn(user);
		when(orderModel.getPaymentTransactions()).thenReturn(paymentTransactions);
		return orderModel;

	}

	private WileyExportProcessModel givenExportProcessWithOrder(final OrderModel orderModel)
	{
		WileyExportProcessModel wileyExportProcessModel = mock(WileyExportProcessModel.class);
		when(wileyExportProcessModel.getOrder()).thenReturn(orderModel);
		when(purchaseOrderService.buildTransactionId(wileyExportProcessModel)).thenReturn(TRANSACTION_ID);
		return wileyExportProcessModel;
	}


}