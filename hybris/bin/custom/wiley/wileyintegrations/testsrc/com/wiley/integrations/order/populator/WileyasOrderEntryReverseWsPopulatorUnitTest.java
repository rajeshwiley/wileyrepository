package com.wiley.integrations.order.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.CancelReason;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.util.PriceValue;
import de.hybris.platform.util.TaxValue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyExternalDiscountModel;
import com.wiley.core.order.service.WileyOrderEntryService;
import com.wiley.core.wiley.restriction.WileyProductRestrictionService;
import com.wiley.integrations.order.dto.CreateOrderEntryRequestWsDTO;
import com.wiley.integrations.order.dto.DiscountValueWsDTO;
import com.wiley.integrations.order.dto.PriceValueWsDTO;
import com.wiley.integrations.order.dto.TaxValueWsDTO;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyasOrderEntryReverseWsPopulatorUnitTest
{
	private static final String TEST_ENTRY_ID = "test entry id";
	private static final String TEST_STATUS = "CREATED";
	private static final String TEST_PRODUCT_CODE = "prorduct code";
	private static final String TEST_BUSINESS_ITEM_ID = "business item id";
	private static final String TEST_BUSINESS_KEY = "business key";
	private static final Integer TEST_QUANTITY = 1;
	private static final PriceValueWsDTO TEST_PRICE_VALUE = new PriceValueWsDTO();
	private static final List<PriceValueWsDTO> TEST_PRICE_VALUES = Collections.singletonList(TEST_PRICE_VALUE);
	private static final DiscountValueWsDTO TEST_DISCOUNT_VALUE = new DiscountValueWsDTO();
	private static final List<DiscountValueWsDTO> TEST_DISCOUNT_VALUES = Collections.singletonList(TEST_DISCOUNT_VALUE);
	private static final TaxValueWsDTO TEST_TAX_VALUE = new TaxValueWsDTO();
	private static final List<TaxValueWsDTO> TEST_TAX_VALUES = Collections.singletonList(TEST_TAX_VALUE);
	private static final Double TEST_TOTAL_PRICE = 1D;
	private static final Double TEST_TAXABLE_TOTAL_PRICE = 2D;
	private static final Date TEST_DELIVERY_DATE = new Date();
	private static final String TEST_CANCEL_REASON = CancelReason.AUTHOR_REQUEST.toString();
	private static final String TEST_ADDITIONAL_INFO = "additional info";
	private static final PriceValue PRICE_VALUE = new PriceValue("", 1D, true);
	private static final TaxValue TAX_VALUE = new TaxValue("", 1D, true, "");

	@Mock
	private ProductService productService;

	@Mock
	private Converter<TaxValueWsDTO, TaxValue> wileyasTaxValueReverseWsConverter;

	@Mock
	private Converter<PriceValueWsDTO, PriceValue> wileyasPriceValueReverseWsConverter;

	@Mock
	private Converter<DiscountValueWsDTO, WileyExternalDiscountModel> wileyExternalDiscountReverseWsConverter;

	@Mock
	private ProductModel productModelMock;

	@Mock
	private WileyExternalDiscountModel wileyExternalDiscountModelMock;

	@Mock
	private WileyOrderEntryService wileyOrderEntryServiceMock;

	@Mock
	private WileyProductRestrictionService wileyProductRestrictionServiceMock;

	@Mock
	private EnumerationService enumerationServiceMock;

	@InjectMocks
	private WileyasOrderEntryReverseWsPopulator populator;

	@Before
	public void setUp()
	{
		when(enumerationServiceMock.getEnumerationValue(OrderStatus.class, TEST_STATUS))
				.thenReturn(OrderStatus.CREATED);
		when(enumerationServiceMock.getEnumerationValue(OrderStatus.class, OrderStatus.CANCELLED.getCode()))
				.thenReturn(OrderStatus.CANCELLED);
		when(enumerationServiceMock.getEnumerationValue(CancelReason.class, TEST_CANCEL_REASON))
				.thenReturn(CancelReason.AUTHOR_REQUEST);
	}

	@Test
	public void populateWithoutCancelStatusTest()
	{
		populateTest(true, TEST_STATUS);
	}

	@Test
	public void populateWithCancelStatusWithCancelReasonTest()
	{
		populateTest(true, OrderStatus.CANCELLED.getCode(), TEST_CANCEL_REASON);
	}

	@Test
	public void populateWithCancelStatusWithoutCancelReasonTest()
	{
		populateTest(true, OrderStatus.CANCELLED.getCode(), TEST_CANCEL_REASON);
	}


	@Test
	public void populateWithoutIdTest()
	{
		populateTest(false, TEST_STATUS);
	}

	public void populateTest(final boolean isIdExists, final String status)
	{
		final CreateOrderEntryRequestWsDTO source = prepareSource(isIdExists, status);
		final OrderEntryModel orderEntryMock = mock(OrderEntryModel.class);
		populator.populate(source, orderEntryMock);
		verifySource(orderEntryMock, isIdExists, status);
	}

	public void populateTest(final boolean isIdExists, final String status, final String cancelReason)
	{
		final CreateOrderEntryRequestWsDTO source = prepareSource(isIdExists, status, cancelReason);
		final OrderEntryModel orderEntryMock = mock(OrderEntryModel.class);
		populator.populate(source, orderEntryMock);
		verifySource(orderEntryMock, isIdExists, status, cancelReason);
	}

	private void verifySource(final OrderEntryModel orderEntryMock, final boolean isIdExists, final String status)
	{
		if (isIdExists)
		{
			verify(orderEntryMock, atLeastOnce()).setGuid(TEST_ENTRY_ID);
		}
		else
		{
			verify(orderEntryMock, atLeastOnce()).setGuid(anyString());
		}
		verify(orderEntryMock, atLeastOnce()).setProduct(productModelMock);
		verify(orderEntryMock, atLeastOnce()).setBusinessItemId(TEST_BUSINESS_ITEM_ID);
		verify(orderEntryMock, atLeastOnce()).setOriginalBusinessKey(TEST_BUSINESS_KEY);
		verify(orderEntryMock, atLeastOnce()).setExternalPriceValues(anyListOf(PriceValue.class));
		verify(orderEntryMock, atLeastOnce())
				.setExternalDiscounts(anyListOf(WileyExternalDiscountModel.class));
		verify(orderEntryMock, atLeastOnce()).setTaxValues(anyListOf(TaxValue.class));
		verify(orderEntryMock, atLeastOnce()).setTotalPrice(TEST_TOTAL_PRICE);
		verify(orderEntryMock, atLeastOnce()).setTaxableTotalPrice(TEST_TAXABLE_TOTAL_PRICE);
		verify(orderEntryMock, atLeastOnce()).setNamedDeliveryDate(TEST_DELIVERY_DATE);
		verify(orderEntryMock, atLeastOnce()).setAdditionalInfo(TEST_ADDITIONAL_INFO);
		if (OrderStatus.CANCELLED.getCode().equals(status))
		{
			verify(orderEntryMock, never()).setBusinessKey(anyString());
		}
		else
		{
			verify(orderEntryMock, atLeastOnce()).setBusinessKey(TEST_BUSINESS_KEY);
		}
		if (OrderStatus.CANCELLED.getCode().equals(status))
		{
			verify(orderEntryMock, atLeastOnce())
					.setStatus(OrderStatus.CANCELLED);
		}
		else
		{
			verify(orderEntryMock, atLeastOnce())
					.setStatus(OrderStatus.valueOf(status.toLowerCase()));
		}

	}

	private void verifySource(final OrderEntryModel orderEntryMock, final boolean isIdExists, final String status,
			final String cancelReason)
	{
		verifySource(orderEntryMock, isIdExists, status);
		if (OrderStatus.CANCELLED.getCode().equals(status))
		{
			verify(orderEntryMock, atLeastOnce())
					.setCancelReason(CancelReason.valueOf(cancelReason));
		}
		else
		{
			verify(orderEntryMock, never()).setCancelReason(any(CancelReason.class));
		}
	}

	private CreateOrderEntryRequestWsDTO prepareSource(final boolean isIdExists, final String status)
	{
		final CreateOrderEntryRequestWsDTO source = new CreateOrderEntryRequestWsDTO();
		if (isIdExists)
		{
			source.setEntryId(TEST_ENTRY_ID);
		}
		source.setProductCode(TEST_PRODUCT_CODE);
		source.setBusinessItemId(TEST_BUSINESS_ITEM_ID);
		source.setBusinessKey(TEST_BUSINESS_KEY);
		source.setQuantity(TEST_QUANTITY);
		source.setExternalPrices(TEST_PRICE_VALUES);
		source.setExternalDiscounts(TEST_DISCOUNT_VALUES);
		source.setTaxes(TEST_TAX_VALUES);
		source.setTotalPrice(TEST_TOTAL_PRICE);
		source.setTaxableTotalPrice(TEST_TAXABLE_TOTAL_PRICE);
		source.setDeliveryDate(TEST_DELIVERY_DATE);
		source.setAdditionalInfo(TEST_ADDITIONAL_INFO);

		when(wileyOrderEntryServiceMock.getOrderEntryForGUID(TEST_ENTRY_ID)).thenReturn(null);
		when(productService.getProductForCode(TEST_PRODUCT_CODE)).thenReturn(productModelMock);
		when(wileyasPriceValueReverseWsConverter.convert(TEST_PRICE_VALUE))
				.thenReturn(PRICE_VALUE);
		when(wileyExternalDiscountReverseWsConverter.convert(TEST_DISCOUNT_VALUE))
				.thenReturn(wileyExternalDiscountModelMock);
		when(wileyasTaxValueReverseWsConverter.convert(TEST_TAX_VALUE)).thenReturn(TAX_VALUE);
		when(wileyProductRestrictionServiceMock.isAvailable(productModelMock)).thenReturn(true);
		source.setStatus(status);
		return source;
	}

	private CreateOrderEntryRequestWsDTO prepareSource(final boolean isIdExists, final String status, final String cancelReason)
	{
		final CreateOrderEntryRequestWsDTO source = prepareSource(isIdExists, status);
		source.setCancelReason(cancelReason);
		return source;
	}

	@Test(expected = AmbiguousIdentifierException.class)
	public void populateAmbiguousIdentifierExceptionTest()
	{
		final CreateOrderEntryRequestWsDTO source = new CreateOrderEntryRequestWsDTO();
		final OrderEntryModel orderEntryMock1 = mock(OrderEntryModel.class);
		source.setEntryId(TEST_ENTRY_ID);

		when(wileyOrderEntryServiceMock.isOrderEntryExistsForGuid(TEST_ENTRY_ID)).thenReturn(true);

		populator.populate(source, orderEntryMock1);
	}


	@Test(expected = IllegalArgumentException.class)
	public void populateSourceNullTest()
	{
		final OrderEntryModel orderEntryMock = mock(OrderEntryModel.class);
		populator.populate(null, orderEntryMock);
	}

	@Test(expected = IllegalArgumentException.class)
	public void populateTargetNullTest()
	{
		populator.populate(new CreateOrderEntryRequestWsDTO(), null);
	}
}
