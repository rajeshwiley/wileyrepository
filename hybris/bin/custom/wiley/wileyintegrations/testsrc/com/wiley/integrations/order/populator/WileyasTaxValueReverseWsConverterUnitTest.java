package com.wiley.integrations.order.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.util.TaxValue;
import static org.junit.Assert.assertTrue;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.integrations.order.dto.TaxValueWsDTO;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyasTaxValueReverseWsConverterUnitTest
{
	private static final String TEST_CODE = "test code";
	private static final double TEST_VALUE = 133.33D;
	private static final String TEST_CURRENCY = "USD";

	private final WileyasTaxValueReverseWsConverter converter = new WileyasTaxValueReverseWsConverter();

	@Test
	public void convertTest()
	{
		final TaxValueWsDTO dto = new TaxValueWsDTO();
		dto.setCode(TEST_CODE);
		dto.setValue(TEST_VALUE);
		dto.setCurrency(TEST_CURRENCY);

		final TaxValue result = converter.convert(dto);

		Assert.assertNotNull(result);
		Assert.assertEquals(TEST_CODE, result.getCode());
		Assert.assertEquals(TEST_VALUE, result.getValue(), 0.0D);
		Assert.assertEquals(TEST_CURRENCY, result.getCurrencyIsoCode());
		assertTrue(result.isAbsolute());
	}

	@Test
	public void convertEmptyCodeTest()
	{
		convertWithNullOrEmptyCodeTest(StringUtils.EMPTY);
	}

	@Test
	public void convertNullCodeTest()
	{
		convertWithNullOrEmptyCodeTest(null);
	}

	private void convertWithNullOrEmptyCodeTest(final String code)
	{
		final TaxValueWsDTO dto = new TaxValueWsDTO();
		dto.setCode(code);
		dto.setValue(TEST_VALUE);
		dto.setCurrency(TEST_CURRENCY);

		final TaxValue result = converter.convert(dto);

		Assert.assertNotNull(result);
		Assert.assertTrue(StringUtils.isNotBlank(result.getCode()));
		Assert.assertEquals(TEST_VALUE, result.getValue(), 0.0D);
		Assert.assertEquals(TEST_CURRENCY, result.getCurrencyIsoCode());
		assertTrue(result.isAbsolute());
	}
}