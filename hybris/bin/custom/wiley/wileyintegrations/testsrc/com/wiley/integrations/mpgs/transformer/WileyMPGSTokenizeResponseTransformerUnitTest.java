package com.wiley.integrations.mpgs.transformer;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.payment.dto.TransactionStatus;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.messaging.Message;

import com.wiley.core.mpgs.constants.WileyMPGSConstants;
import com.wiley.core.mpgs.dto.json.Card;
import com.wiley.core.mpgs.dto.json.Provided;
import com.wiley.core.mpgs.dto.json.Response;
import com.wiley.core.mpgs.dto.json.SourceOfFunds;
import com.wiley.core.mpgs.dto.token.MPGSTokenResponseDTO;
import com.wiley.core.mpgs.response.WileyTokenizationResponse;
import com.wiley.core.mpgs.services.WileyTransformationService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyMPGSTokenizeResponseTransformerUnitTest
{
	private static final String TEST_GATEWAY_CODE = "TEST_GATEWAY_CODE";
	private static final String TEST_EXPIRY_YEAR = "23";
	private static final String TEST_EXPIRY_MONTH = "12";
	private static final String TEST_CARD_NUMBER = "TEST_CARD_NUMBER";
	private static final String MAESTRO = "MAESTRO";
	private static final String TEST_TOKEN = "TEST_TOKEN";

	@InjectMocks
	@Spy
	private WileyMPGSTokenizeResponseTransformer testTransformer;

	@Mock
	WileyTransformationService wileyTransformationService;

	@Mock
	private Message<MPGSTokenResponseDTO> message;

	@Mock
	private MPGSTokenResponseDTO responseDto;

	@Mock
	private Provided provided;

	@Mock
	private Response response;

	@Mock
	private SourceOfFunds sourceOfFunds;

	@Mock
	private Card card;

	@Before
	public void setUp() throws java.io.IOException
	{
		when(message.getPayload()).thenReturn(responseDto);
		when(wileyTransformationService.transformStatusIfSuccessful(WileyMPGSConstants.RETURN_STATUS_SUCCESS)).thenReturn(
				TransactionStatus.ACCEPTED.name());

		when(wileyTransformationService.transformStatusIfSuccessful(WileyMPGSConstants.RETURN_STATUS_ERROR)).thenReturn(
				WileyMPGSConstants.RETURN_STATUS_ERROR);
	}

	@Test
	public void transformResultSuccess() throws Exception
	{
		when(responseDto.getResult()).thenReturn(WileyMPGSConstants.RETURN_STATUS_SUCCESS);
		when(responseDto.getResponse()).thenReturn(response);
		when(response.getGatewayCode()).thenReturn(TEST_GATEWAY_CODE);
		when(responseDto.getSourceOfFunds()).thenReturn(sourceOfFunds);
		when(sourceOfFunds.getProvided()).thenReturn(provided);
		when(provided.getCard()).thenReturn(card);
		when(card.getExpiry()).thenReturn(TEST_EXPIRY_MONTH + TEST_EXPIRY_YEAR);
		when(card.getNumber()).thenReturn(TEST_CARD_NUMBER);
		when(card.getScheme()).thenReturn(MAESTRO);
		when(responseDto.getToken()).thenReturn(TEST_TOKEN);

		WileyTokenizationResponse wileyTokenizationResponse = testTransformer.transform(message);
		assertEquals(TransactionStatus.ACCEPTED.name(), wileyTokenizationResponse.getStatus());
		assertEquals(TEST_GATEWAY_CODE, wileyTokenizationResponse.getStatusDetails());
		assertEquals(TEST_EXPIRY_MONTH, wileyTokenizationResponse.getCardExpiryMonth());
		assertEquals(TEST_EXPIRY_YEAR, wileyTokenizationResponse.getCardExpiryYear());
		assertEquals(TEST_CARD_NUMBER, wileyTokenizationResponse.getCardNumber());
		assertEquals(MAESTRO, wileyTokenizationResponse.getCardScheme());
		assertEquals(TEST_TOKEN, wileyTokenizationResponse.getToken());
	}

	@Test(expected = NullPointerException.class)
	public void nullPointerExceptionIfErrorResult() throws Exception
	{
		when(responseDto.getResult()).thenReturn(WileyMPGSConstants.RETURN_STATUS_ERROR);

		WileyTokenizationResponse wileyTokenizationResponse = testTransformer.transform(message);
	}
}