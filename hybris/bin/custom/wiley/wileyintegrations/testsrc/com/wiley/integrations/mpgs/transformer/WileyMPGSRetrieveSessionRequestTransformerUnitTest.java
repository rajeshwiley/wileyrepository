package com.wiley.integrations.mpgs.transformer;

import de.hybris.bootstrap.annotations.UnitTest;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.mpgs.dto.retrievesession.MPGSRetrieveSessionRequestDTO;
import com.wiley.core.mpgs.request.WileyRetrieveSessionRequest;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyMPGSRetrieveSessionRequestTransformerUnitTest
{
	@Mock
	private WileyRetrieveSessionRequest mockRequest;

	@InjectMocks
	@Spy
	private WileyMPGSRetrieveSessionRequestTransformer transformer = new WileyMPGSRetrieveSessionRequestTransformer();

	@Test
	public void successfulResult()
	{
		MPGSRetrieveSessionRequestDTO requestDTO = transformer.transform(mockRequest);
		assertNotNull(requestDTO);
	}

}
