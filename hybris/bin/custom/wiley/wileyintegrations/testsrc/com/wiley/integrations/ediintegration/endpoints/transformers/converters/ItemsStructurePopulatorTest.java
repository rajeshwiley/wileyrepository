package com.wiley.integrations.ediintegration.endpoints.transformers.converters;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.subscriptionservices.model.SubscriptionProductModel;
import de.hybris.platform.subscriptionservices.model.SubscriptionTermModel;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.paypal.hybris.model.PaypalPaymentInfoModel;
import com.wiley.core.enums.SubscriptionTermFrequencyValue;
import com.wiley.core.price.WileyDiscountCalculationService;
import com.wiley.fulfilmentprocess.model.WileyExportProcessModel;
import com.wiley.integrations.ediintegration.endpoints.jaxb.ItemsStructure;

import static com.wiley.integrations.constants.WileyintegrationsConstants.CC_PO_NUMBER_FORMAT;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ItemsStructurePopulatorTest extends ConverterTestBase
{

	@InjectMocks
	private ItemsStructurePopulator testInstance = new ItemsStructurePopulator();
	@Mock
	private WileyDiscountCalculationService wileyDiscountCalculationService;

	@Before
	public void setUp() throws Exception
	{
		when(wileyDiscountCalculationService.getEntryProportionalDiscount(orderEntryModel))
				.thenReturn(BigDecimal.valueOf(PRODUCT_ENTRY_DISCOUNT_VALUE));
	}

	@Test
	public void testPopulate() throws Exception
	{
		//Given
		givenOrder();

		final WileyExportProcessModel givenExportProcess = givenExportProcessWithOrder(orderModel);
		//Testing
		List<ItemsStructure> structureList = new ArrayList<>();
		testInstance.populate(givenExportProcess, structureList);
		//Verify
		assertEquals(1, structureList.size());
		ItemsStructure itemsStructure = structureList.get(0);

		assertEquals(
				String.format(CC_PO_NUMBER_FORMAT, ORDER_CODE),
				itemsStructure.getPurchaseOrderNumber()
		);
		assertEquals(PRODUCT_ISBN, itemsStructure.getProductId());
		assertEquals(PRODUCT_ENTRY_QUANTITY, itemsStructure.getQuantity());
		assertEquals(PRODUCT_ENTRY_BASE_PRICE, itemsStructure.getPrice());
		assertEquals(PRODUCT_ENTRY_TAX_VALUE, itemsStructure.getTaxAmount());
		assertEquals(PRODUCT_ENTRY_DISCOUNT_VALUE, itemsStructure.getDiscount());
	}

	@Test
	public void poNumberShouldStartWithPHBForPayPalOrder() {
		//Given
		givenOrder();

		when(orderModel.getPaymentInfo()).thenReturn(mock(PaypalPaymentInfoModel.class));
		final WileyExportProcessModel givenExportProcess = givenExportProcessWithOrder(orderModel);
		//When
		List<ItemsStructure> structureList = new ArrayList<>();
		testInstance.populate(givenExportProcess, structureList);

		ItemsStructure itemsStructure = structureList.get(0);
		//Then
		assertTrue(itemsStructure.getPurchaseOrderNumber().startsWith("PHB"));
	}

	@Test
	public void poNumberShouldStartWithHBForCreditCardOrder() {
		//Given
		givenOrder();
		when(orderModel.getPaymentInfo()).thenReturn(mock(CreditCardPaymentInfoModel.class));
		final WileyExportProcessModel givenExportProcess = givenExportProcessWithOrder(orderModel);
		//When
		List<ItemsStructure> structureList = new ArrayList<>();
		testInstance.populate(givenExportProcess, structureList);

		ItemsStructure itemsStructure = structureList.get(0);
		//Then
		assertTrue(itemsStructure.getPurchaseOrderNumber().startsWith("HB"));
	}

	@Test
	public void testGiftCardIsNotIncludedInStructureList()
	{
		//Given
		givenOrder();
		givenOrderWithEntries();

		final WileyExportProcessModel givenExportProcess = givenExportProcessWithOrder(orderModel);
		//Testing
		List<ItemsStructure> structureList = new ArrayList<>();
		testInstance.populate(givenExportProcess, structureList);
		//Verify
		assertEquals(1, structureList.size());
		ItemsStructure itemsStructure = structureList.get(0);

		assertEquals(PRODUCT_ISBN, itemsStructure.getProductId());
	}

	@Test
	public void testGetSubscriptionTypeShouldReturnProperValueForYearlySubscription()
	{
		final SubscriptionTermModel yearlySubscriptionTerm = givenSubscriptionTermModel(SubscriptionTermFrequencyValue.YEARLY);
		//Testing
		final String yearlyResult = testInstance.getSubscriptionType(yearlySubscriptionTerm);
		//Verify
		assertEquals(ItemsStructurePopulator.YEARLY_SUBSCRIPTION, yearlyResult);
	}

	@Test
	public void testGetSubscriptionTypeShouldReturnProperValueForMonthlySubscription()
	{
		final SubscriptionTermModel monthlySubscriptionTerm = givenSubscriptionTermModel(SubscriptionTermFrequencyValue.MONTHLY);
		//Testing
		final String monthlyResult = testInstance.getSubscriptionType(monthlySubscriptionTerm);
		//Verify
		assertEquals(ItemsStructurePopulator.MONTHLY_SUBSCRIPTION, monthlyResult);
	}


	@Test
	public void testGetSalesModelShouldReturnEmptyStringIfSubscriptionTermIsNull()
	{
		//Given
		ProductModel givenProduct = givenProductWithSubscription(null);
		//Testing
		String result = testInstance.getSalesModel(givenProduct);
		//Verify
		assertEquals(StringUtils.EMPTY, result);
	}

	@Test
	public void testGetSalesModelShouldReturnEmptyStringIfProductIsNotSubscription()
	{
		//Given
		ProductModel givenProduct = mock(ProductModel.class);
		//Testing
		String result = testInstance.getSalesModel(givenProduct);
		//Verify
		assertEquals(StringUtils.EMPTY, result);
	}

	private SubscriptionProductModel givenProductWithSubscription(final SubscriptionTermModel subscriptionTerm)
	{
		SubscriptionProductModel subscriptionProduct = mock(SubscriptionProductModel.class);
		when(subscriptionProduct.getSubscriptionTerm()).thenReturn(subscriptionTerm);
		return subscriptionProduct;
	}

	private SubscriptionTermModel givenSubscriptionTermModel(final SubscriptionTermFrequencyValue frequencyValue)
	{
		SubscriptionTermModel subscriptionTerm = mock(SubscriptionTermModel.class);
		when(subscriptionTerm.getSubscriptionTermFrequencyValue()).thenReturn(frequencyValue);
		return subscriptionTerm;
	}

	private WileyExportProcessModel givenExportProcessWithOrder(final OrderModel orderModel)
	{
		WileyExportProcessModel wileyExportProcessModel = mock(WileyExportProcessModel.class);
		when(wileyExportProcessModel.getOrder()).thenReturn(orderModel);
		return wileyExportProcessModel;
	}

}