package com.wiley.integrations.esb.service.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.Set;

import com.wiley.core.model.ExternalInventoryModel;
import com.wiley.integrations.esb.dto.verifyandcalculatecart.CartCalculationEntryResponse;
import com.wiley.integrations.esb.dto.verifyandcalculatecart.InventoryStatus;

import org.hamcrest.core.IsCollectionContaining;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Default unit test for {@link Wileyb2bExternalInventoryServiceImpl}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2bExternalInventoryServiceImplUnitTest
{

	@Mock
	private ModelService modelServiceMock;

	@Mock
	private Populator<InventoryStatus, ExternalInventoryModel> inventoryStatusToExternalInventoryPopulatorMock;

	@InjectMocks
	private Wileyb2bExternalInventoryServiceImpl wileyb2bExternalInventoryService;

	@Mock
	private CartCalculationEntryResponse cartCalculationEntryResponseMock1;
	private final String orderEntryResponseProductIsbn1 = "productCode_1";

	@Mock
	private CartEntryModel cartEntryModelMock1;

	@Mock
	private ExternalInventoryModel externalInventoryModelMock1;

	@Mock
	private InventoryStatus inventoryStatusMock1;

	@Mock
	private ExternalInventoryModel newExternalInventoryModelMock1;


	@Before
	public void setUp() throws Exception
	{
		when(cartCalculationEntryResponseMock1.getIsbn()).thenReturn(orderEntryResponseProductIsbn1);

		when(modelServiceMock.create(eq(ExternalInventoryModel.class)))
				.thenReturn(newExternalInventoryModelMock1)
				.thenReturn(mock(ExternalInventoryModel.class));
	}

	@Test
	public void shouldUpdateExternalInventoriesSuccessfully()
	{
		// Given
		// there are existing inventories
		when(cartEntryModelMock1.getExternalInventories()).thenReturn(Collections.singleton(externalInventoryModelMock1));

		// received new inventories
		when(cartCalculationEntryResponseMock1.getInventoryStatuses()).thenReturn(Arrays.asList(inventoryStatusMock1));

		// When
		wileyb2bExternalInventoryService.updateInventoryStatuses(cartCalculationEntryResponseMock1, cartEntryModelMock1);

		// Then

		// verify removing of existing external inventories.
		verify(cartEntryModelMock1).setExternalInventories(eq(Collections.emptySet()));
		verify(modelServiceMock).removeAll(eq(Collections.singleton(externalInventoryModelMock1)));

		// verify creation of new external inventories.
		verify(modelServiceMock).create(eq(ExternalInventoryModel.class));

		verify(inventoryStatusToExternalInventoryPopulatorMock).populate(same(inventoryStatusMock1),
				same(newExternalInventoryModelMock1));

		verify(newExternalInventoryModelMock1).setOrderEntry(cartEntryModelMock1);

		verify(cartEntryModelMock1).setExternalInventories(
				(Set<ExternalInventoryModel>) argThat(IsCollectionContaining.hasItem(newExternalInventoryModelMock1)));
	}

}