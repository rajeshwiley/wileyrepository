package com.wiley.integrations.ediintegration.endpoints.transformers.converters;

import de.hybris.bootstrap.annotations.UnitTest;

import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.user.AddressModel;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;



import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ConversionUtilsTest
{
	public static final String EMPTY_STRING = "";
	public static final String ISO_CODE_SHORT = "TX";

	@Test
	public void verifyReturnsProperRegionCodeIfRegionIsNotNull() throws Exception
	{
		//Given
		AddressModel addressModel = givenAddressModelWithRegion(givenRegion(ISO_CODE_SHORT));
		//Testing
		String result = ConversionUtils.getStateProvinceFromAddress(addressModel);
		//Verify
		assertEquals(ISO_CODE_SHORT, result);
	}

	@Test
	public void verifyReturnsEmptyStringIfRegionIsNull()
	{
		//Given
		AddressModel addressModel = givenAddressModelWithRegion(null);
		//Testing
		String result = ConversionUtils.getStateProvinceFromAddress(addressModel);
		//Verify
		assertEquals(EMPTY_STRING, result);
	}


	private AddressModel givenAddressModelWithRegion(final RegionModel regionModel)
	{
		AddressModel addressModel = mock(AddressModel.class);
		when(addressModel.getRegion()).thenReturn(regionModel);
		return addressModel;
	}

	private RegionModel givenRegion(final String isoCodeShort)
	{
		RegionModel regionModel = mock(RegionModel.class);
		when(regionModel.getIsocodeShort()).thenReturn(isoCodeShort);
		return regionModel;
	}
}
