package com.wiley.integrations.article.transformer;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.converters.Populator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.messaging.Message;

import com.wiley.integrations.article.dto.ArticleRequestDto;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ArticleRequestToCustomObjectDataTransformerUnitTest
{
	private static final String PAYLOAD = "test_id";

	@Mock
	private Message<String> message;

	@Mock
	private Populator<String, ArticleRequestDto> articleRequestToCustomObjectDataPopulator;

	@InjectMocks
	private ArticleRequestToCustomObjectDataTransformer transformer;

	@Before
	public void prepare()
	{
		when(message.getPayload()).thenReturn(PAYLOAD);
		doAnswer(dto ->
		{
			final Object[] args = dto.getArguments();
			((ArticleRequestDto) args[1]).setId((String) args[0]);
			return null;
		}).when(articleRequestToCustomObjectDataPopulator).populate(eq(PAYLOAD), any(ArticleRequestDto.class));
	}

	@Test
	public void transformSuccessTest()
	{
		// when
		final ArticleRequestDto dto = transformer.transform(message);

		// then
		assertNotNull(dto);
		assertEquals(PAYLOAD, dto.getId());
	}

	@Test(expected = IllegalArgumentException.class)
	public void transformFailTest()
	{
		transformer.transform(null);
	}
}