package com.wiley.integrations.ediintegration;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.integration.edi.EDIConstants;
import com.wiley.core.integration.edi.WileyDailyReportExportGateway;
import com.wiley.core.integration.edi.job.DailyReportExportJob;
import com.wiley.core.integration.edi.strategy.EdiOrderProvisionStrategy;
import com.wiley.core.model.EdiExportCronJobModel;
import com.wiley.fulfilmentprocess.model.WileyExportProcessModel;

import static com.wiley.integrations.constants.WileyintegrationsConstants.EDI_SUPPORTED_STORES_DEFAULT;
import static com.wiley.integrations.constants.WileyintegrationsConstants.EDI_SUPPORTED_STORES_PROPERTY_PATTERN;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DailyReportExportJobTest
{
	private static final String WEL_STORE_CODE = "wel";
	private static final String AGS_STORE_CODE = "ags";
	private static final String RECIPIENT_LIST_PROP = "wiley.orders.daily.report.to.email.list";
	private static final String RECIPIENT = "test_recipient@mail.test";

	@InjectMocks
	private DailyReportExportJob testInstance = new DailyReportExportJob();

	@Mock
	private BaseStoreService baseStoreService;

	@Mock
	private EdiExportCronJobModel ediExportCronJobModel;

	@Mock
	private ConfigurationService configurationService;

	@Mock
	private Configuration configurationMock;

	@Mock
	private EdiOrderProvisionStrategy orderProvisionStrategy;

	@Mock
	private WileyDailyReportExportGateway gatewayMock;

	@Before
	public void setUp() throws Exception
	{
		final String ediSupportedStores = String.format(EDI_SUPPORTED_STORES_PROPERTY_PATTERN,
				EDIConstants.PAYMENT_TYPE_CREDIT_CARD.toLowerCase());

		when(configurationService.getConfiguration()).thenReturn(configurationMock);
		when(configurationMock.getString(eq(ediSupportedStores), anyString())).thenReturn(EDI_SUPPORTED_STORES_DEFAULT);
		when(ediExportCronJobModel.getPaymentType()).thenReturn(EDIConstants.PAYMENT_TYPE_CREDIT_CARD);
		BaseStoreModel welBaseStoreModel = new BaseStoreModel();
		welBaseStoreModel.setUid("wel");
		when(baseStoreService.getBaseStoreForUid(WEL_STORE_CODE)).thenReturn(welBaseStoreModel);
		BaseStoreModel agsBaseStoreModel = new BaseStoreModel();
		agsBaseStoreModel.setUid("ags");
		when(baseStoreService.getBaseStoreForUid(AGS_STORE_CODE)).thenReturn(agsBaseStoreModel);
	}

	@Test
	public void shouldEdiExportOrdersOnFilledData()
	{
		when(configurationMock.getString(RECIPIENT_LIST_PROP)).thenReturn(RECIPIENT);
		when(orderProvisionStrategy.removePartialRefundsProcesses(anyList())).thenReturn(
				Collections.singletonList(new WileyExportProcessModel()));
		testInstance.perform(ediExportCronJobModel);
		verify(gatewayMock, times(2)).exportOrders(any(), any(), any(), any(), any());
	}

	@Test
	public void shouldNotStartExportDueToNullRecipient()
	{
		when(configurationMock.getString(RECIPIENT_LIST_PROP)).thenReturn(null);
		testInstance.perform(ediExportCronJobModel);
		verify(gatewayMock, times(0)).exportOrders(any(), any(), any(), any(), any());
	}

	@Test
	public void shouldNotStartExportDueToEmptyRecipient()
	{
		when(configurationMock.getString(RECIPIENT_LIST_PROP)).thenReturn(StringUtils.EMPTY);
		testInstance.perform(ediExportCronJobModel);
		verify(gatewayMock, times(0)).exportOrders(any(), any(), any(), any(), any());
	}
}
