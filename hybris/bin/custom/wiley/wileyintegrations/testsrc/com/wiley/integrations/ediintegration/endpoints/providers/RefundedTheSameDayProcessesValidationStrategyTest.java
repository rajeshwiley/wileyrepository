package com.wiley.integrations.ediintegration.endpoints.providers;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.enums.ExportProcessType;
import com.wiley.core.integration.edi.strategy.impl.RefundedTheSameDayProcessesValidationStrategy;
import com.wiley.core.refund.WileyRefundService;
import com.wiley.fulfilmentprocess.model.WileyExportProcessModel;

@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class RefundedTheSameDayProcessesValidationStrategyTest
{

	@InjectMocks
	private RefundedTheSameDayProcessesValidationStrategy testInstance = new RefundedTheSameDayProcessesValidationStrategy();

	@Mock
	private WileyRefundService refundService;

	@Mock
	private OrderModel order1;

	@Mock
	private OrderModel order2;

	@Mock
	private OrderModel order3;

	@Before
	public void setUp() throws Exception
	{
		when(order1.getCode()).thenReturn("order1");
		when(order2.getCode()).thenReturn("order2");
		when(order3.getCode()).thenReturn("order3");
	}

	@Test
	public void getRejectedProcessesShouldNotRejectAnythingIfNoSettleAndRefundForTheSameOrder()
	{

		List<WileyExportProcessModel> list = Arrays.asList(
				givenOrderExportProcess(order1, ExportProcessType.REFUND),
				givenOrderExportProcess(order2, ExportProcessType.SETTLE),
				givenOrderExportProcess(order3, ExportProcessType.SETTLE)
		);
		Collection<WileyExportProcessModel> filteredList = testInstance.getRejectedProcesses(list);
		assertEquals(0, filteredList.size());
	}

	@Test
	public void getRejectedProcessesShouldRejectSettleAndRefundIfTheyWereMadeTheSameDay()
	{
		List<WileyExportProcessModel> list = Arrays.asList(
				givenOrderExportProcess(order1, ExportProcessType.REFUND),
				givenOrderExportProcess(order1, ExportProcessType.SETTLE)
		);
		Collection<WileyExportProcessModel> filteredList = testInstance.getRejectedProcesses(list);
		assertEquals(2, filteredList.size());
	}

	@Test
	public void getRejectedProcessesShouldNotRejectOrdersThatDoNotHaveSettlesAndRefundsTheSameDay()
	{
		List<WileyExportProcessModel> list = Arrays.asList(
				givenOrderExportProcess(order1, ExportProcessType.REFUND),
				givenOrderExportProcess(order2, ExportProcessType.SETTLE)
		);
		Collection<WileyExportProcessModel> filteredList = testInstance.getRejectedProcesses(list);
		assertEquals(0, filteredList.size());
	}

	private WileyExportProcessModel givenOrderExportProcess(final OrderModel order, final ExportProcessType exportProcessType) {
		final WileyExportProcessModel exportProcessModel = mock(WileyExportProcessModel.class);
		when(exportProcessModel.getOrder()).thenReturn(order);
		when(exportProcessModel.getExportType()).thenReturn(exportProcessType);
		when(refundService.isFullyRefundedOrder(order)).thenReturn(true);
		return exportProcessModel;
	}
}