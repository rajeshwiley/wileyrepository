package com.wiley.integrations.ediintegration.endpoints.jaxb;

import de.hybris.platform.store.BaseStoreModel;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;

import com.wiley.core.integration.edi.EDIConstants;


public class RoutingTest
{
	private LocalDateTime testDate;
	private Routing testInstance;
	private BaseStoreModel baseStoreModel;

	private static final String TRANSACTION_ID = "900000000";
	private static final String BASE_STORE_UID = "ags";
	private static final String SENDER_SAN_PREFIX = "HYB_";

	@Before
	public void setup() {
		testDate = LocalDateTime.now();
		baseStoreModel = mock(BaseStoreModel.class);
		when(baseStoreModel.getUid()).thenReturn(BASE_STORE_UID);
		testInstance = new Routing(testDate, baseStoreModel, TRANSACTION_ID, EDIConstants.PAYMENT_TYPE_CREDIT_CARD);
	}



	@Test
	public void testGetReceiver() throws Exception
	{
		//Testing
		assertEquals(Routing.DEFAULT_RECEIVER, testInstance.getReceiver());
	}

	@Test
	public void testGetTransactionBatchNumber() throws Exception
	{
		//Testing
		assertEquals(TRANSACTION_ID, testInstance.getTransactionBatchNumber());
	}

	@Test
	public void testGetDate() throws Exception
	{
		//Given
		String expectedDate = DateTimeFormatter.ofPattern(Routing.DATE_FORMAT).format(testDate);
		//Testing
		assertEquals(expectedDate, testInstance.getDate());
	}

	@Test
	public void testGetTime() throws Exception
	{
		//Given
		String expectedTime = DateTimeFormatter.ofPattern(Routing.TIME_FORMAT).format(testDate);
		//Testing
		assertEquals(expectedTime, testInstance.getTime());
	}

	@Test
	public void testGetSenderSANForCreditCard() {
		//Testing
		assertEquals(getExpectedSenderSAN(EDIConstants.PAYMENT_TYPE_CREDIT_CARD), testInstance.getSenderSAN());
	}

	@Test
	public void senderSanShouldContainPPInTheEndForPayPalPayments() {
		testInstance = new Routing(testDate, baseStoreModel, TRANSACTION_ID, EDIConstants.PAYMENT_TYPE_PAYPAL);
		assertEquals(getExpectedSenderSAN(EDIConstants.PAYMENT_TYPE_PAYPAL), testInstance.getSenderSAN());
	}

	@Test
	public void shouldSetIsPPPaymentIfPaymentTypeIsPayPal() {
		final Routing ppRouting = new Routing(testDate, baseStoreModel, TRANSACTION_ID, EDIConstants.PAYMENT_TYPE_PAYPAL);
		assertTrue(ppRouting.isPayPalPayments());
	}

	private String getExpectedSenderSAN(final String paymentType) {
		if (StringUtils.equals(EDIConstants.PAYMENT_TYPE_PAYPAL, paymentType)) {
			return (SENDER_SAN_PREFIX + BASE_STORE_UID + "_" + EDIConstants.PAYMENT_TYPE_PAYPAL).toUpperCase();
		}
		return (SENDER_SAN_PREFIX + BASE_STORE_UID).toUpperCase();
	}
}