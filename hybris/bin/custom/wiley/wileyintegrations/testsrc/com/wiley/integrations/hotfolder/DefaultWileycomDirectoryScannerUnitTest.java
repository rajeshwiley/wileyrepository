package com.wiley.integrations.hotfolder;

import de.hybris.bootstrap.annotations.UnitTest;
import static org.mockito.Mockito.when;

import java.io.File;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;


/**
 * Created by Georgii_Gavrysh on 8/18/2016.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultWileycomDirectoryScannerUnitTest
{
	private DefaultWileycomDirectoryScanner defaultWileycomDirectoryScanner = new DefaultWileycomDirectoryScanner(
			"full_inventory-(\\w+)-(\\d+)\\.csv");

	@Mock
	private File directoryMock;

	@Mock
	private File emptyFileMock;


	@Mock
	private File notEmptyCanWriteFileMock;

	@Mock
	private File notEmptyCannotWriteFileMock;

	@Before
	public void setup()
	{
		when(emptyFileMock.length()).thenReturn(0L);
		when(emptyFileMock.getName()).thenReturn("full_inventory-default-1.csv");

		when(notEmptyCannotWriteFileMock.length()).thenReturn(1L);
		when(notEmptyCannotWriteFileMock.canWrite()).thenReturn(false);
		when(notEmptyCannotWriteFileMock.getName()).thenReturn("full_inventory-default-1.csv");

		when(notEmptyCanWriteFileMock.length()).thenReturn(1L);
		when(notEmptyCanWriteFileMock.canWrite()).thenReturn(true);
		when(notEmptyCanWriteFileMock.getName()).thenReturn("full_inventory-default-1.csv");
	}

	@Test
	public void testNotEmptyFiles()
	{
		when(directoryMock.listFiles()).thenReturn(new File[] { notEmptyCanWriteFileMock, notEmptyCannotWriteFileMock,
				emptyFileMock });

		final List<File> filesResult = defaultWileycomDirectoryScanner.listFiles(directoryMock);

		assertThat(filesResult.size(), is(1));
		assertThat(notEmptyCanWriteFileMock, is(filesResult.get(0)));
	}
}
