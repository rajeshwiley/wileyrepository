package com.wiley.integrations.order.populator;


import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.util.DiscountValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.enums.OrderType;
import com.wiley.core.enums.PaymentModeEnum;
import com.wiley.core.model.WileyExternalDiscountModel;
import com.wiley.integrations.order.dto.AbstractOrderWsDTO;
import com.wiley.integrations.order.dto.AddressWsDTO;
import com.wiley.integrations.order.dto.DiscountValueWsDTO;
import com.wiley.integrations.order.dto.OrderEntryWsDTO;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyAbstractOrderWsPopulatorUnitTest
{
	private static final String ID = "ID";
	private static final String CODE = "CODE";
	private static final OrderType TYPE = OrderType.AS_FUNDED;
	private static final String SITE_ID = "asSite";
	private static final String COUNTRY_ISO = "COUNTRY CODE";
	private static final String CURRENCY_ISO = "CURRENCY ISO";
	private static final String USER_ID = "USER ID";
	private static final String EXTERNAL_TEXT = "EXTERNAL TEXT";
	private static final String PO_NUMBER = "PO NUMBER";
	private static final String TAX_NUMBER = "TAX NUMBER";
	private static final String PAYMENT_MODE = PaymentModeEnum.CARD.getCode();
	private static final String USER_NOTES = "USER NOTES";
	private static final List<String> APPLIED_COUPON_CODES = Arrays.asList("code1", "code2");
	private static final Double SUBTOTAL_PRICE = 120.50d;
	private static final Double TOTAL_PRICE = 140.75d;
	private static final Double TOTAL_TAX = 140.75d;
	private static final Double TOTAL_DISCOUNT = 11.75d;
	private static final Boolean TAX_NUMBER_VALIDATED = false;
	private static final Boolean TAX_CALCULATED = false;

	private static final Date TAX_EXPIRATION_DATE = new Date();
	private static final Date DATE = new Date();
	private static final Date MODIFICATION_TIME = new Date();

	@InjectMocks
	private WileyAbstractOrderWsPopulator testInstance;

	@Mock
	private Converter<DiscountValue, DiscountValueWsDTO> wileyDiscountValueWsConverter;
	@Mock
	private Converter<WileyExternalDiscountModel, DiscountValueWsDTO> wileyExternalDiscountWsConverter;
	@Mock
	private Converter<AddressModel, AddressWsDTO> wileyAddressWsConverter;
	@Mock
	private Converter<AbstractOrderEntryModel, OrderEntryWsDTO> wileyOrderEntryWsConverter;
	@Mock
	private AbstractOrderModel sourceMock;
	private AbstractOrderWsDTO target = new AbstractOrderWsDTO();

	@Mock
	private BaseSiteModel baseSiteMock;
	@Mock
	private AbstractOrderEntryModel orderEntryMock;

	@Mock
	private OrderEntryWsDTO orderEntryWsMock;

	@Mock
	private WileyExternalDiscountModel externalDiscountMock;
	@Mock
	private DiscountValue discountMock;


	@Mock
	private AddressModel paymentAddressMock;
	@Mock
	private AddressModel contactAddressMock;


	@Mock
	private DiscountValueWsDTO externalDiscountWsMock;
	@Mock
	private DiscountValueWsDTO discountWsMock;
	@Mock
	private AddressWsDTO paymentAddressWsMock;
	@Mock
	private AddressWsDTO contactAddressWsMock;


	@Mock
	private CountryModel countryMock;
	@Mock
	private CurrencyModel currencyMock;
	@Mock
	private UserModel userMock;
	@Mock
	private PaymentModeModel paymentModeMock;

	@Test
	public void testPopulateFields()
	{
		// given

		when(sourceMock.getGuid()).thenReturn(ID);
		when(sourceMock.getCode()).thenReturn(CODE);
		when(sourceMock.getOrderType()).thenReturn(TYPE);
		when(sourceMock.getSite()).thenReturn(baseSiteMock);
		when(baseSiteMock.getUid()).thenReturn(SITE_ID);

		when(sourceMock.getCountry()).thenReturn(countryMock);
		when(countryMock.getIsocode()).thenReturn(COUNTRY_ISO);

		when(sourceMock.getCurrency()).thenReturn(currencyMock);
		when(currencyMock.getIsocode()).thenReturn(CURRENCY_ISO);

		when(sourceMock.getUser()).thenReturn(userMock);
		when(userMock.getUid()).thenReturn(USER_ID);



		List<AbstractOrderEntryModel> entries = Collections.singletonList(orderEntryMock);
		when(sourceMock.getEntries()).thenReturn(entries);
		when(wileyOrderEntryWsConverter.convert(orderEntryMock)).thenReturn(orderEntryWsMock);

		when(sourceMock.getPaymentMode()).thenReturn(paymentModeMock);
		when(paymentModeMock.getCode()).thenReturn(PAYMENT_MODE);


		when(sourceMock.getPaymentAddress()).thenReturn(paymentAddressMock);
		when(wileyAddressWsConverter.convert(paymentAddressMock)).thenReturn(paymentAddressWsMock);

		when(sourceMock.getContactAddress()).thenReturn(contactAddressMock);
		when(wileyAddressWsConverter.convert(contactAddressMock)).thenReturn(contactAddressWsMock);

		when(sourceMock.getExternalText()).thenReturn(EXTERNAL_TEXT);
		when(sourceMock.getPurchaseOrderNumber()).thenReturn(PO_NUMBER);
		when(sourceMock.getUserNotes()).thenReturn(USER_NOTES);
		when(sourceMock.getAppliedCouponCodes()).thenReturn(APPLIED_COUPON_CODES);
		when(sourceMock.getSubtotal()).thenReturn(SUBTOTAL_PRICE);


		List<DiscountValue> discounts = Collections.singletonList(discountMock);
		when(sourceMock.getGlobalDiscountValues()).thenReturn(discounts);
		when(wileyDiscountValueWsConverter.convert(discountMock)).thenReturn(discountWsMock);


		List<WileyExternalDiscountModel> externalDiscounts = Collections.singletonList(externalDiscountMock);
		when(sourceMock.getExternalDiscounts()).thenReturn(externalDiscounts);
		when(wileyExternalDiscountWsConverter.convert(externalDiscountMock)).thenReturn(externalDiscountWsMock);

		when(sourceMock.getTotalDiscounts()).thenReturn(TOTAL_DISCOUNT);
		when(sourceMock.getTotalTax()).thenReturn(TOTAL_TAX);
		when(sourceMock.getTaxCalculated()).thenReturn(TAX_CALCULATED);
		when(sourceMock.getTotalPrice()).thenReturn(TOTAL_PRICE);
		when(sourceMock.getDate()).thenReturn(DATE);
		when(sourceMock.getTaxNumber()).thenReturn(TAX_NUMBER);
		when(sourceMock.getTaxNumberValidated()).thenReturn(TAX_NUMBER_VALIDATED);
		when(sourceMock.getTaxNumberExpirationDate()).thenReturn(TAX_EXPIRATION_DATE);
		when(sourceMock.getModifiedtime()).thenReturn(MODIFICATION_TIME);

		// when
		testInstance.populate(sourceMock, target);

		// then

		assertEquals(ID, target.getId());
		assertEquals(CODE, target.getCode());
		assertEquals(TYPE.getCode(), target.getType());
		assertEquals(SITE_ID, target.getSiteId());
		assertEquals(COUNTRY_ISO, target.getCountry());
		assertEquals(CURRENCY_ISO, target.getCurrency());
		assertEquals(USER_ID, target.getUserId());
		assertEquals(Collections.singletonList(orderEntryWsMock), target.getEntries());
		assertEquals(PAYMENT_MODE, target.getPaymentMode().toString());
		assertEquals(paymentAddressWsMock, target.getPaymentAddress());
		assertEquals(contactAddressWsMock, target.getContactAddress());
		assertEquals(EXTERNAL_TEXT, target.getExternalText());
		assertEquals(PO_NUMBER, target.getPurchaseOrderNumber());
		assertEquals(USER_NOTES, target.getUserNotes());
		assertEquals(APPLIED_COUPON_CODES, target.getAppliedCouponCodes());
		assertEquals(SUBTOTAL_PRICE, target.getSubtotal());
		assertEquals(Collections.singletonList(discountWsMock), target.getDiscounts());
		assertEquals(Collections.singletonList(externalDiscountWsMock), target.getExternalDiscounts());
		assertEquals(TOTAL_DISCOUNT, target.getTotalDiscounts());
		assertEquals(TAX_NUMBER, target.getTaxNumber());
		assertEquals(TAX_NUMBER_VALIDATED, target.isTaxNumberValidated());
		assertEquals(TAX_EXPIRATION_DATE, target.getTaxNumberExpirationDate());
		assertEquals(TOTAL_TAX, target.getTotalTax());
		assertEquals(TAX_CALCULATED, target.isTaxCalculated());
		assertEquals(TOTAL_PRICE, target.getTotalPrice());
		assertEquals(DATE, target.getDate());
		assertEquals(MODIFICATION_TIME, target.getModificationTime());

	}

	@Test
	public void shouldTolerateAllNullFields()
	{
		// given
		AbstractOrderModel emptyMock = mock(AbstractOrderModel.class);

		when(emptyMock.getEntries()).thenReturn(null);
		when(emptyMock.getAppliedCouponCodes()).thenReturn(null);
		when(emptyMock.getTaxNumberValidated()).thenReturn(null);
		when(emptyMock.getTaxCalculated()).thenReturn(null);
		when(emptyMock.getSubtotal()).thenReturn(null);
		when(emptyMock.getGlobalDiscountValues()).thenReturn(null);

		when(emptyMock.getExternalDiscounts()).thenReturn(null);

		when(emptyMock.getTotalDiscounts()).thenReturn(null);
		when(emptyMock.getTotalTax()).thenReturn(null);
		when(emptyMock.getTotalPrice()).thenReturn(null);

		// when
		testInstance.populate(emptyMock, target);

		// then
		assertNull(target.getId());
		assertNull(target.getCode());
		assertNull(target.getType());
		assertNull(target.getSiteId());
		assertNull(target.getCountry());
		assertNull(target.getCurrency());
		assertNull(target.getUserId());
		assertNull(target.getEntries());
		assertNull(target.getPaymentMode());
		assertNull(target.getPaymentAddress());
		assertNull(target.getContactAddress());
		assertNull(target.getExternalText());
		assertNull(target.getPurchaseOrderNumber());
		assertNull(target.getUserNotes());
		assertNull(target.getAppliedCouponCodes());
		assertNull(target.getSubtotal());
		assertNull(target.getDiscounts());
		assertNull(target.getExternalDiscounts());
		assertNull(target.getTotalDiscounts());
		assertNull(target.getTaxNumber());
		assertNull(target.isTaxNumberValidated());
		assertNull(target.getTaxNumberExpirationDate());
		assertNull(target.getTotalTax());
		assertNull(target.isTaxCalculated());
		assertNull(target.getTotalPrice());
		assertNull(target.getDate());
		assertNull(target.getModificationTime());

	}

	@Test(expected = IllegalArgumentException.class)
	public void testPopulateWithNullSource()
	{
		testInstance.populate(null, target);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testPopulateWithNullTarget()
	{
		testInstance.populate(sourceMock, null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testPopulateWithNullSourceAndTarget()
	{
		testInstance.populate(null, null);
	}
}
