package com.wiley.integrations.ediintegration.endpoints.service.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.paypal.hybris.model.PaypalPaymentInfoModel;
import com.wiley.core.enums.CreditCardTypeCode;
import com.wiley.core.enums.ExportProcessType;
import com.wiley.core.integration.edi.service.impl.WileyPurchaseOrderService;
import com.wiley.core.payment.transaction.PaymentTransactionService;
import com.wiley.fulfilmentprocess.model.WileyExportProcessModel;
import com.wiley.core.integration.ebp.enums.WileyShipMethod;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyPurchaseOrderServiceTest
{
	private static final String TEST_REQUEST_ID = "123456789";
	private static final String TEST_CC_TOKEN = "T4012007494160026";
	private static final String HYBRIS_AUTHORIZATION_CODE = "HybrisAuthorizationCode";
	private static final BigDecimal AMOUNT = BigDecimal.valueOf(25.0);
	private static final String SHIP_METHOD_FOR_ORDER_STANDARD = "standard";
	private static final String SHIP_METHOD_FOR_ORDER_NEXT_DAY = "next-day";
	private static final String SHIP_METHOD_FOR_ORDER_EXPRESS = "express";
	private static final String SHIP_METHOD_FOR_ORDER_INTERNATIONAL = "international";
	private static final String SHIP_METHOD_UNKNOWN = "SomeMethod";
	private static final Double DELIVERY_COST = 10.13;

	@InjectMocks
	private WileyPurchaseOrderService testInstance = new WileyPurchaseOrderService();

	@Mock
	private PaymentTransactionService paymentTransactionService;

	@Mock
	private DeliveryModeModel deliveryModeModel;

	@Test
	public void testBuildHybrisAuthorizationCodeForCardPayment()
	{
		//Given
		final OrderModel orderModel = createOrder(CreditCardPaymentInfoModel.class);
		PaymentTransactionEntryModel entry = buildAuthorizationPaymentTransactionEntry(orderModel,
				PaymentTransactionType.AUTHORIZATION);
		when(entry.getWpgAuthCode()).thenReturn(HYBRIS_AUTHORIZATION_CODE);
		//Testing
		String result = testInstance.buildHybrisAuthorizationCode(orderModel);
		//Verify
		assertEquals(HYBRIS_AUTHORIZATION_CODE, result);
	}

	@Test
	public void testBuildHybrisAuthorizationCodeForPayPalPayment()
	{
		//Given
		final OrderModel orderModel = createOrder(PaypalPaymentInfoModel.class);
		PaymentTransactionEntryModel entry = buildAuthorizationPaymentTransactionEntry(orderModel,
				PaymentTransactionType.AUTHORIZATION);
		when(entry.getRequestId()).thenReturn(HYBRIS_AUTHORIZATION_CODE);
		//Testing
		String result = testInstance.buildHybrisAuthorizationCode(orderModel);
		//Verify
		assertEquals(HYBRIS_AUTHORIZATION_CODE, result);
	}

	@Test
	public void testBuildHybrisAuthorizationCodeShouldReturnEmptyStringIfAuthTransactionEntryIsNull()
	{
		//Given
		final OrderModel orderModel = createOrder(CreditCardPaymentInfoModel.class);
		when(paymentTransactionService.getAcceptedTransactionEntry(orderModel,
				PaymentTransactionType.AUTHORIZATION)).thenReturn(null);
		//Testing
		String result = testInstance.buildHybrisAuthorizationCode(orderModel);
		//Verify
		assertEquals(StringUtils.EMPTY, result);
	}

	@Test
	public void testBuildHybrisAuthorizationCodeShouldReturnEmptyStringIfTransactionWpgAuthCodeIsNull()
	{
		//Given
		final OrderModel orderModel = createOrder(CreditCardPaymentInfoModel.class);
		PaymentTransactionEntryModel entry = buildAuthorizationPaymentTransactionEntry(orderModel,
				PaymentTransactionType.AUTHORIZATION);
		when(entry.getWpgAuthCode()).thenReturn(null);
		//Testing
		String result = testInstance.buildHybrisAuthorizationCode(orderModel);
		//Verify
		assertEquals(StringUtils.EMPTY, result);
	}

	@Test
	public void testBuildHybrisAuthorizationCodeShouldReturnEmptyStringIfTransactionWpgAuthCodeIsEmptyString()
	{
		//Given
		final OrderModel orderModel = createOrder(CreditCardPaymentInfoModel.class);
		PaymentTransactionEntryModel entry = buildAuthorizationPaymentTransactionEntry(orderModel,
				PaymentTransactionType.AUTHORIZATION);
		when(entry.getWpgAuthCode()).thenReturn(StringUtils.EMPTY);
		//Testing
		String result = testInstance.buildHybrisAuthorizationCode(orderModel);
		//Verify
		assertEquals(StringUtils.EMPTY, result);
	}

	@Test
	public void testBuildCreditCardExpiryShouldAddZeroIfMonthsNumberIsLessThanTen()
	{
		//Given
		CreditCardPaymentInfoModel infoModel = buildCreditCardPaymentInfoModel(null, "5", "19");
		//Testing
		final String result = testInstance.buildCreditCardExpiry(infoModel);
		//Expecting
		assertEquals("0519", result);
	}

	@Test
	public void testBuildCreditCardExpiryShouldUseOnlyLast2DigitsFromYear()
	{
		//Given
		CreditCardPaymentInfoModel infoModel = buildCreditCardPaymentInfoModel(null, "05", "2019");
		//Testing
		final String result = testInstance.buildCreditCardExpiry(infoModel);
		//Expecting
		assertEquals("0519", result);
	}

	@Test
	public void testBuildCreditCardExpiryShouldNotAddZeroIfMonthsNumberIsLessThanTen()
	{
		//Given
		CreditCardPaymentInfoModel infoModel = buildCreditCardPaymentInfoModel(null, "10", "19");
		//Testing
		final String result = testInstance.buildCreditCardExpiry(infoModel);
		//Expecting
		assertEquals("1019", result);
	}

	@Test
	public void testGetCreditCardType()
	{
		//Given
		CreditCardPaymentInfoModel infoModel = buildCreditCardPaymentInfoModel(null, "05", "2019");
		//Testing
		String result = testInstance.getCreditCardType(infoModel);
		//Verify
		assertEquals(CreditCardTypeCode.MC.name(), result);
	}

	@Test
	public void testGetSettleTransactionAmount()
	{
		//Given
		final OrderModel orderModel = createOrder(CreditCardPaymentInfoModel.class);
		WileyExportProcessModel givenExportProcess = givenExportProcessWithOrder(orderModel, ExportProcessType.SETTLE);
		final PaymentTransactionEntryModel captureEntry = buildPaymentTransactionEntry(orderModel,
				PaymentTransactionType.CAPTURE);
		//Testing
		BigDecimal result = testInstance.getSettleOrRefundTransactionAmount(givenExportProcess);
		//Verify
		assertEquals(AMOUNT, result);
	}

	@Test
	public void testGetRefundTransactionAmount()
	{
		//Given
		final OrderModel orderModel = createOrder(CreditCardPaymentInfoModel.class);
		WileyExportProcessModel givenExportProcess = givenExportProcessWithOrder(orderModel, ExportProcessType.REFUND);
		final PaymentTransactionEntryModel refundEntry = buildPaymentTransactionEntry(orderModel,
				PaymentTransactionType.REFUND_FOLLOW_ON);
		when(givenExportProcess.getTransactionEntry()).thenReturn(refundEntry);
		//Testing
		BigDecimal result = testInstance.getSettleOrRefundTransactionAmount(givenExportProcess);
		//Verify
		assertEquals(AMOUNT, result);
	}

	@Test
	public void testGetCCTokenForPayPalPayment()
	{
		//Given
		final OrderModel orderModel = createOrder(PaypalPaymentInfoModel.class);
		buildAuthorizationPaymentTransactionEntry(orderModel, PaymentTransactionType.AUTHORIZATION);
		//Setting paypal specific payment info
		PaypalPaymentInfoModel paypalPaymentInfo = mock(PaypalPaymentInfoModel.class);
		when(orderModel.getPaymentInfo()).thenReturn(paypalPaymentInfo);
		//Setting token specific for paypal payments
		final String testPaypalToken = "testPayPalToken";
		when(paypalPaymentInfo.getToken()).thenReturn(testPaypalToken);
		//Testing
		String resultToken = testInstance.getCCToken(orderModel);
		//Verify
		assertEquals(testPaypalToken, resultToken);
	}

	@Test
	public void testGetCCTokenForCardPayment()
	{
		//Given
		final OrderModel orderModel = createOrder(CreditCardPaymentInfoModel.class);
		buildAuthorizationPaymentTransactionEntry(orderModel, PaymentTransactionType.AUTHORIZATION);
		//Testing
		String ccToken = testInstance.getCCToken(orderModel);
		//Verify
		assertEquals(TEST_CC_TOKEN, ccToken);
	}

	@Test
	public void testGetOrderTypeCapture()
	{
		//Given
		final OrderModel orderModel = createOrder(CreditCardPaymentInfoModel.class);
		WileyExportProcessModel givenExportProcess = givenExportProcessWithOrder(orderModel, ExportProcessType.SETTLE);
		//Testing
		String ccToken = testInstance.getOrderType(givenExportProcess);
		//Verify
		assertEquals(WileyPurchaseOrderService.CC, ccToken);
	}

	@Test
	public void testGetOrderTypeRefund()
	{
		//Given
		final OrderModel orderModel = createOrder(CreditCardPaymentInfoModel.class);
		WileyExportProcessModel givenExportProcess = givenExportProcessWithOrder(orderModel, ExportProcessType.REFUND);
		//Testing
		String ccToken = testInstance.getOrderType(givenExportProcess);
		//Verify
		assertEquals(WileyPurchaseOrderService.CR, ccToken);
	}

	@Test
	public void testIsOrderRefundedCapture()
	{
		//Given
		final OrderModel orderModel = createOrder(CreditCardPaymentInfoModel.class);
		WileyExportProcessModel givenExportProcess = givenExportProcessWithOrder(orderModel, ExportProcessType.SETTLE);
		//Testing
		boolean isOrderRefunded = testInstance.isOrderRefunded(givenExportProcess);
		//Verify
		assertEquals(false, isOrderRefunded);
	}

	@Test
	public void testIsOrderRefundedRefund()
	{
		//Given
		final OrderModel orderModel = createOrder(CreditCardPaymentInfoModel.class);
		WileyExportProcessModel givenExportProcess = givenExportProcessWithOrder(orderModel, ExportProcessType.REFUND);
		//Testing
		boolean isOrderRefunded = testInstance.isOrderRefunded(givenExportProcess);
		//Verify
		assertEquals(true, isOrderRefunded);
	}

	@Test
	public void testBuildTransactionIdCapture()
	{
		//Given
		final OrderModel orderModel = createOrder(CreditCardPaymentInfoModel.class);
		WileyExportProcessModel givenExportProcess = givenExportProcessWithOrder(orderModel, ExportProcessType.REFUND);
		PaymentTransactionEntryModel paymentTransactionEntryModel = buildPaymentTransactionEntry(orderModel,
				PaymentTransactionType.CAPTURE);
		when(givenExportProcess.getTransactionEntry()).thenReturn(paymentTransactionEntryModel);
		//Testing
		String requestId = testInstance.buildTransactionId(givenExportProcess);
		//Verify
		assertEquals(TEST_REQUEST_ID, requestId);
	}

	@Test
	public void testBuildTransactionIdRefund()
	{
		//Given
		final OrderModel orderModel = createOrder(CreditCardPaymentInfoModel.class);
		WileyExportProcessModel givenExportProcess = givenExportProcessWithOrder(orderModel, ExportProcessType.REFUND);
		PaymentTransactionEntryModel paymentTransactionEntryModel = buildPaymentTransactionEntry(orderModel,
				PaymentTransactionType.REFUND_FOLLOW_ON);
		when(givenExportProcess.getTransactionEntry()).thenReturn(paymentTransactionEntryModel);
		//Testing
		String requestId = testInstance.buildTransactionId(givenExportProcess);
		//Verify
		assertEquals(TEST_REQUEST_ID, requestId);
	}

	@Test
	public void testBuildStandartShipMethod()
	{
		//Given
		final OrderModel orderModel = createOrder(CreditCardPaymentInfoModel.class);

		when(orderModel.getDeliveryMode()).thenReturn(deliveryModeModel);
		when(deliveryModeModel.getCode()).thenReturn(SHIP_METHOD_FOR_ORDER_STANDARD);
		//Should return standart ship method
		//Testing
		String shipMethod = testInstance.buildShipMethod(orderModel);
		//shipMethod
		assertEquals(WileyShipMethod.STND.name(), shipMethod);
	}

	@Test
	public void testBuildNextDayShipMethod()
	{
		//Given
		final OrderModel orderModel = createOrder(CreditCardPaymentInfoModel.class);

		when(orderModel.getDeliveryMode()).thenReturn(deliveryModeModel);
		when(deliveryModeModel.getCode()).thenReturn(SHIP_METHOD_FOR_ORDER_NEXT_DAY);
		//Should return next-day ship method
		//Testing
		String shipMethod = testInstance.buildShipMethod(orderModel);
		//shipMethod
		assertEquals(WileyShipMethod.NXTD.name(), shipMethod);
	}

	@Test
	public void testBuildExpressShipMethod()
	{
		//Given
		final OrderModel orderModel = createOrder(CreditCardPaymentInfoModel.class);

		when(orderModel.getDeliveryMode()).thenReturn(deliveryModeModel);
		when(deliveryModeModel.getCode()).thenReturn(SHIP_METHOD_FOR_ORDER_EXPRESS);
		//Should return express ship method
		//Testing
		String shipMethod = testInstance.buildShipMethod(orderModel);
		//shipMethod
		assertEquals(WileyShipMethod.EXPR.name(), shipMethod);
	}

	@Test
	public void testBuildInternationalShipMethod()
	{
		//Given
		final OrderModel orderModel = createOrder(CreditCardPaymentInfoModel.class);

		when(orderModel.getDeliveryMode()).thenReturn(deliveryModeModel);
		when(deliveryModeModel.getCode()).thenReturn(SHIP_METHOD_FOR_ORDER_INTERNATIONAL);
		//Should return express ship method
		//Testing
		String shipMethod = testInstance.buildShipMethod(orderModel);
		//shipMethod
		assertEquals(WileyShipMethod.INTL.name(), shipMethod);
	}

	@Test
	public void testBuildShipMethodIfCameUnknownShipMet()
	{
		//Given
		final OrderModel orderModel = createOrder(CreditCardPaymentInfoModel.class);

		when(orderModel.getDeliveryMode()).thenReturn(deliveryModeModel);
		when(deliveryModeModel.getCode()).thenReturn(SHIP_METHOD_UNKNOWN);
		//Should return DONT becouse ship method is unknown
		//Testing
		String shipMethod = testInstance.buildShipMethod(orderModel);
		//shipMethod
		assertEquals(WileyShipMethod.DONT.name(), shipMethod);
	}

	@Test
	public void testBuildShipMethodIfCameNullShipMet()
	{
		//Given
		final OrderModel orderModel = createOrder(CreditCardPaymentInfoModel.class);

		when(orderModel.getDeliveryMode()).thenReturn(deliveryModeModel);
		when(deliveryModeModel.getCode()).thenReturn(null);
		//Should return DONT becouse ship method is null
		//Testing
		String shipMethod = testInstance.buildShipMethod(orderModel);
		//shipMethod
		assertEquals(WileyShipMethod.DONT.name(), shipMethod);
	}

	private OrderModel createOrder(final Class<? extends PaymentInfoModel> paymentInfoType)
	{
		OrderModel orderModel = mock(OrderModel.class);
		when(orderModel.getDeliveryCost()).thenReturn(DELIVERY_COST);
		when(orderModel.getPaymentInfo()).thenReturn(mock(paymentInfoType));
		return orderModel;
	}

	private PaymentTransactionEntryModel buildAuthorizationPaymentTransactionEntry(final OrderModel orderModel,
			final PaymentTransactionType transactionType)
	{
		PaymentTransactionEntryModel transactionEntry = mock(PaymentTransactionEntryModel.class);
		when(transactionEntry.getType()).thenReturn(transactionType);
		when(transactionEntry.getRequestToken()).thenReturn(TEST_CC_TOKEN);
		when(paymentTransactionService.getAcceptedTransactionEntry(orderModel,
				transactionType)).thenReturn(transactionEntry);
		return transactionEntry;
	}

	private PaymentTransactionEntryModel buildPaymentTransactionEntry(final OrderModel orderModel,
			final PaymentTransactionType type)
	{
		PaymentTransactionEntryModel transactionEntry = mock(PaymentTransactionEntryModel.class);
		when(transactionEntry.getType()).thenReturn(type);
		when(transactionEntry.getAmount()).thenReturn(AMOUNT);
		when(transactionEntry.getRequestId()).thenReturn(TEST_REQUEST_ID);
		when(paymentTransactionService.getAcceptedTransactionEntry(orderModel,
				type)).thenReturn(transactionEntry);
		return transactionEntry;
	}

	private CreditCardPaymentInfoModel buildCreditCardPaymentInfoModel(final String ccNumber, final String validToMonth,
			final String validToYear)
	{
		CreditCardPaymentInfoModel creditCardPaymentInfoModel = mock(CreditCardPaymentInfoModel.class);
		when(creditCardPaymentInfoModel.getValidToMonth()).thenReturn(validToMonth);
		when(creditCardPaymentInfoModel.getValidToYear()).thenReturn(validToYear);
		when(creditCardPaymentInfoModel.getType()).thenReturn(CreditCardType.MASTER);
		return creditCardPaymentInfoModel;
	}

	private WileyExportProcessModel givenExportProcessWithOrder(final OrderModel orderModel,
			final ExportProcessType exportProcessType)
	{
		WileyExportProcessModel wileyExportProcessModel = mock(WileyExportProcessModel.class);
		when(wileyExportProcessModel.getOrder()).thenReturn(orderModel);
		when(wileyExportProcessModel.getExportType()).thenReturn(exportProcessType);
		return wileyExportProcessModel;
	}

}
