package com.wiley.integrations.mpgs.transformer;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.payment.dto.TransactionStatus;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.messaging.Message;

import com.wiley.core.mpgs.constants.WileyMPGSConstants;
import com.wiley.core.mpgs.dto.authorization.MPGSAuthorizationResponseDTO;
import com.wiley.core.mpgs.dto.json.Response;
import com.wiley.core.mpgs.dto.json.Transaction;
import com.wiley.core.mpgs.dto.json.authorization.SourceOfFunds;
import com.wiley.core.mpgs.response.WileyAuthorizationResponse;
import com.wiley.core.mpgs.services.WileyTransformationService;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.TimeZone;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyMPGSAuthorizationResponseTransformerUnitTest
{
	public static final String TEST_GATEWAY_CODE = "TEST_GATEWAY_CODE";
	public static final String TEST_TRANSACTION_ID = "TEST_TRANSACTION_ID";
	public static final String TEST_CURRENCY = "AUD";
	public static final String TEST_TOKEN = "TEST_TOKEN";
	public static final String TEST_DATE = "2017-12-05T09:22:07.309Z";
	public static final BigDecimal TEST_AMOUNT = new BigDecimal("100");
	public static final String TIMED_OUT = "TIMED_OUT";
	public static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
	@InjectMocks
	private WileyMPGSAuthorizationResponseTransformer testTransformer;

	@Mock
	private WileyTransformationService wileyTransformationService;

	@Mock
	private Message<MPGSAuthorizationResponseDTO> message;

	@Mock
	private MPGSAuthorizationResponseDTO resultDto;

	@Mock
	private Response response;

	@Mock
	private Transaction transaction;

	@Mock
	private SourceOfFunds sourceOfFunds;

	@Before
	public void setUp()
	{
		when(message.getPayload()).thenReturn(resultDto);
		when(wileyTransformationService.transformStatusIfSuccessful(WileyMPGSConstants.RETURN_STATUS_SUCCESS)).thenReturn(
				TransactionStatus.ACCEPTED.name());
		when(wileyTransformationService.transformStatusIfSuccessful(WileyMPGSConstants.RETURN_STATUS_ERROR)).thenReturn(
				WileyMPGSConstants.RETURN_STATUS_ERROR);
		when(wileyTransformationService.transformStatusIfSuccessful(WileyMPGSConstants.RETURN_STATUS_FAILURE)).thenReturn(
				WileyMPGSConstants.RETURN_STATUS_FAILURE);
		when(wileyTransformationService.transformStatusIfSuccessful(WileyMPGSConstants.RETURN_STATUS_UNKNOWN)).thenReturn(
				WileyMPGSConstants.RETURN_STATUS_UNKNOWN);
		when(resultDto.getError()).thenReturn(null);
		when(resultDto.getResponse()).thenReturn(response);
		when(resultDto.getTransaction()).thenReturn(transaction);
		when(transaction.getId()).thenReturn(TEST_TRANSACTION_ID);
		when(transaction.getAmount()).thenReturn(TEST_AMOUNT);
		when(transaction.getCurrency()).thenReturn(TEST_CURRENCY);
		when(resultDto.getTimeOfRecord()).thenReturn(TEST_DATE);
		when(wileyTransformationService.transformStringToDate(TEST_DATE)).thenReturn(new DateTime(TEST_DATE).toDate());
		when(resultDto.getSourceOfFunds()).thenReturn(sourceOfFunds);
		when(sourceOfFunds.getToken()).thenReturn(TEST_TOKEN);
	}

	@Test
	public void transformResultSuccess() throws Exception
	{
		when(resultDto.getResult()).thenReturn(WileyMPGSConstants.RETURN_STATUS_SUCCESS);
		when(response.getGatewayCode()).thenReturn(TEST_GATEWAY_CODE);

		WileyAuthorizationResponse authorizeResponse = testTransformer.transform(message);

		assertEquals(TransactionStatus.ACCEPTED.name(), authorizeResponse.getStatus());
		assertEquals(TEST_GATEWAY_CODE, authorizeResponse.getStatusDetails());
		assertEquals(TEST_TRANSACTION_ID, authorizeResponse.getTransactionId());
		assertEquals(TEST_CURRENCY, authorizeResponse.getCurrency());
		assertEquals(TEST_AMOUNT, authorizeResponse.getTotalAmount());
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
		sdf.setTimeZone(TimeZone.getTimeZone("0"));
		String responseDateFormat = sdf.format(authorizeResponse.getTransactionCreatedTime());
		assertEquals(TEST_DATE, responseDateFormat);
		assertEquals(TEST_TOKEN, authorizeResponse.getToken());
	}

	@Test(expected = NullPointerException.class)
	public void nullPointerExceptionIfErrorResult() throws Exception
	{
		when(resultDto.getResult()).thenReturn(WileyMPGSConstants.RETURN_STATUS_ERROR);
		when(resultDto.getResponse()).thenReturn(null);
		when(resultDto.getTransaction()).thenReturn(null);
		when(transaction.getId()).thenThrow(NullPointerException.class);
		when(transaction.getAmount()).thenThrow(NullPointerException.class);
		when(transaction.getCurrency()).thenThrow(NullPointerException.class);
		when(resultDto.getTimeOfRecord()).thenReturn(null);
		when(resultDto.getSourceOfFunds()).thenReturn(null);
		when(sourceOfFunds.getToken()).thenThrow(NullPointerException.class);

		WileyAuthorizationResponse response = testTransformer.transform(message);
	}

	@Test
	public void transformResultFailure() throws Exception
	{
		when(resultDto.getResult()).thenReturn(WileyMPGSConstants.RETURN_STATUS_FAILURE);
		when(response.getGatewayCode()).thenReturn(TIMED_OUT);

		WileyAuthorizationResponse authorizeResponse = testTransformer.transform(message);

		assertEquals(WileyMPGSConstants.RETURN_STATUS_FAILURE, authorizeResponse.getStatus());
		assertEquals(TIMED_OUT, authorizeResponse.getStatusDetails());
		assertEquals(TEST_TRANSACTION_ID, authorizeResponse.getTransactionId());
		assertEquals(TEST_CURRENCY, authorizeResponse.getCurrency());
		assertEquals(TEST_AMOUNT, authorizeResponse.getTotalAmount());
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
		sdf.setTimeZone(TimeZone.getTimeZone("0"));
		String responseDateFormat = sdf.format(authorizeResponse.getTransactionCreatedTime());
		assertEquals(TEST_DATE, responseDateFormat);
		assertEquals(TEST_TOKEN, authorizeResponse.getToken());
	}

	@Test
	public void transformResultUnknown() throws Exception
	{
		when(resultDto.getResult()).thenReturn(WileyMPGSConstants.RETURN_STATUS_UNKNOWN);
		when(response.getGatewayCode()).thenReturn(TIMED_OUT);

		WileyAuthorizationResponse authorizeResponse = testTransformer.transform(message);

		assertEquals(WileyMPGSConstants.RETURN_STATUS_UNKNOWN, authorizeResponse.getStatus());
		assertEquals(TIMED_OUT, authorizeResponse.getStatusDetails());
		assertEquals(TEST_TRANSACTION_ID, authorizeResponse.getTransactionId());
		assertEquals(TEST_CURRENCY, authorizeResponse.getCurrency());
		assertEquals(TEST_AMOUNT, authorizeResponse.getTotalAmount());
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
		sdf.setTimeZone(TimeZone.getTimeZone("0"));
		String responseDateFormat = sdf.format(authorizeResponse.getTransactionCreatedTime());
		assertEquals(TEST_DATE, responseDateFormat);
		assertEquals(TEST_TOKEN, authorizeResponse.getToken());
	}
}
