package com.wiley.integrations.order.publish.transformer;


import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.servicelayer.ServicelayerTest;
import de.hybris.platform.util.CSVReader;

import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wiley.core.model.WileyOrderProcessModel;
import com.wiley.integrations.order.dto.OrderWsDTO;

import static org.fest.assertions.Assertions.assertThat;


/**
 * Test should cover cases, when {@link WileyOrderProcessModel#PREVIOUSORDERSTATE} deserialization could bring issues.
 * For example, if type of DTO attribute was changed, then previousOrderState deserialization will bring exception.
 * That exception will cause runtime failure of business process
 * hence this test should reveal some issues at earlier stages during continuous integration.
 *
 * testPreviousOrderStateDump.csv file contains examples of serialized OrderWsDTO exported from test environment.
 * Test makes deserialization of csv examples into current version of {@link OrderWsDTO}.
 */
@IntegrationTest
public class WileyOrderPublishPreviousOrderStateIntegrationTest extends ServicelayerTest
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyOrderPublishPreviousOrderStateIntegrationTest.class);

	@Resource(name = "jacksonObjectMapper")
	private ObjectMapper objectMapper;

	@Test
	public void shouldParseDump() throws IOException
	{
		try (
				InputStream stream = this.getClass().getResourceAsStream(
						"/test/order/WileyOrderPublishPreviousOrderStateIntegrationTest/testPreviousOrderStateDump.csv")
		)
		{
			CSVReader csvReader = new CSVReader(stream, "UTF-8");

			List<Exception> parserExceptions = new LinkedList<>();
			while (csvReader.readNextLine())
			{
				Map<Integer, String> line = csvReader.getLine();
				String previousOrderState = line.get(0);
				try
				{
					objectMapper.readValue(previousOrderState, OrderWsDTO.class);
				}
				catch (IOException e)
				{
					parserExceptions.add(e);
					LOG.error(String.format("Failed to parse %s line", csvReader.getCurrentLineNumber()), e);
				}
			}

			assertThat(parserExceptions).isEmpty();
			csvReader.close();
		}

	}

}
