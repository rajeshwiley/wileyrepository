package com.wiley.integrations.order.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.wileyas.user.service.WileyasUserService;
import com.wiley.integrations.order.dto.CreateOrderRequestWsDTO;

/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyasOrderReverseWsPopulatorUnitTest
{
    private static final String TEST_USER_ID = "test user id";

    @Mock
    private WileyasUserService wileyasUserServiceMock;

    @InjectMocks
    private WileyasOrderReverseWsPopulator populator;

    @Test
    public void populateTest()
    {
        final CreateOrderRequestWsDTO source = new CreateOrderRequestWsDTO();
        final OrderModel orderMock = Mockito.mock(OrderModel.class);
        final UserModel userMock = Mockito.mock(UserModel.class);
        source.setPlacedByAgent(TEST_USER_ID);

        when(wileyasUserServiceMock.getUserForUID(TEST_USER_ID)).thenReturn(userMock);

        populator.populate(source, orderMock);

        verify(wileyasUserServiceMock, atLeastOnce()).getUserForUID(TEST_USER_ID);
        verify(orderMock, atLeastOnce()).setPlacedBy(userMock);
    }

    @Test(expected = IllegalArgumentException.class)
    public void populateSourceNullTest()
    {
        final OrderModel orderMock = Mockito.mock(OrderModel.class);
        populator.populate(null, orderMock);
    }

    @Test(expected = IllegalArgumentException.class)
    public void populateTargetNullTest()
    {
        populator.populate(new CreateOrderRequestWsDTO(), null);
    }
}