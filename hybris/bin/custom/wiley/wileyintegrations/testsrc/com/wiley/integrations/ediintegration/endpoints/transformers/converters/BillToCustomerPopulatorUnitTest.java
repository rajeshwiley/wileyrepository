package com.wiley.integrations.ediintegration.endpoints.transformers.converters;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.integrations.ediintegration.endpoints.jaxb.BillToCustomer;

import static org.junit.Assert.assertEquals;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class BillToCustomerPopulatorUnitTest extends ConverterTestBase
{
	private BillToCustomerPopulator testInstance = new BillToCustomerPopulator();

	@Before
	public void setUp() throws Exception
	{
		setAddressFieldsLengthRestrictions(testInstance);
	}

	@Test
	public void testPopulate() throws Exception
	{
		//Given
		OrderModel testOrder = givenOrder();
		//Testing
		BillToCustomer billToCustomer = new BillToCustomer();
		testInstance.populate(testOrder, billToCustomer);
		//Verify
		assertEquals(ADDRESS_LINE_1, billToCustomer.getAddressLine1());
		assertEquals(ADDRESS_LINE_2, billToCustomer.getAddressLine2());
		assertEquals(FIRST_NAME, billToCustomer.getCustomerName());
		assertEquals(LAST_NAME, billToCustomer.getCustomerName2());
		assertEquals(COUNTRY_NUMERIC, billToCustomer.getCountry());
		assertEquals(STATE, billToCustomer.getStateProvince());
		assertEquals(POST_CODE, billToCustomer.getZipPostCode());
	}
}