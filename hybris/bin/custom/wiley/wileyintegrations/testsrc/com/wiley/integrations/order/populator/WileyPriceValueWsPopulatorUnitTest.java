package com.wiley.integrations.order.populator;


import com.wiley.integrations.order.dto.PriceValueWsDTO;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.util.PriceValue;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyPriceValueWsPopulatorUnitTest
{
	private static final Double VALUE = 100.0d;
	private static final String CURRENCY = "CUR";

	@InjectMocks
	private WileyPriceValueWsPopulator testInstance;

	@Mock
	private PriceValue sourceMock;

	@Mock
	private PriceValueWsDTO targetMock;


	@Test
	public void testPopulate()
	{
		// given
		when(sourceMock.getValue()).thenReturn(VALUE);
		when(sourceMock.getCurrencyIso()).thenReturn(CURRENCY);

		// when
		testInstance.populate(sourceMock, targetMock);

		// then
		verify(targetMock).setValue(VALUE);
		verify(targetMock).setCurrency(CURRENCY);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testPopulateWithNullSource()
	{
		testInstance.populate(null, targetMock);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testPopulateWithNullTarget()
	{
		testInstance.populate(sourceMock, null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testPopulateWithNullSourceAndTarget()
	{
		testInstance.populate(null, null);
	}
}
