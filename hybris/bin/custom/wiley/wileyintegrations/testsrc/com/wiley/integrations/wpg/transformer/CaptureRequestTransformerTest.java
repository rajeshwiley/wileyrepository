package com.wiley.integrations.wpg.transformer;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Map;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.payment.WileyHttpRequestParams;
import com.wiley.core.payment.request.WileyCaptureRequest;
import com.wiley.core.payment.strategies.SecurityHashGeneratorStrategy;
import com.wiley.core.payment.strategies.WPGRegionStrategy;
import com.wiley.core.payment.strategies.WPGVendorIdStrategy;
import com.wiley.core.payment.strategies.WileyTransactionIdGeneratorStrategy;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CaptureRequestTransformerTest
{
	private static final String TEST_DESCRIPTION = "testDescription";
	private static final String TEST_OPERATION = "testOperation";
	private static final String TEST_TRANSACTION_ID = "mockTransactionId";
	private static final String TEST_SITE_ID = "testSiteId";
	private static final Integer TEST_VENDOR_ID = 210;
	private static final String TEST_SECURITY_HASH = "testSecurityHash";
	private static final String TEST_REGION = "testRegion";
	private static final String TEST_AMOUNT = "10.02";
	private static final String TEST_TOKEN = "testToken";
	private static final String TEST_AUTH_CODE = "testAuthCode";
	private static final String HTTP = "HTTP";
	private static final String CURRENCY_CODE = "USD";
	private static final String TEST_TIMESTAMP = "testTimestamp";
	private static final String TEST_HASH_BASE = TEST_TIMESTAMP + TEST_VENDOR_ID
			+ TEST_TRANSACTION_ID + HTTP + TEST_DESCRIPTION
			+ TEST_AMOUNT + CURRENCY_CODE + TEST_REGION + TEST_TOKEN + TEST_AUTH_CODE;
	@Mock
	private WileyTransactionIdGeneratorStrategy mockTransactionIdGeneratorStrategy;
	@Mock
	private WPGRegionStrategy mockWpgRegionStrategy;
	@Mock
	private ConfigurationService mockConfigurationService;
	@Mock
	private SecurityHashGeneratorStrategy mockWpgHashStrategy;
	@Mock
	private Configuration mockConfiguration;
	@Mock
	private WileyCaptureRequest mockCaptureRequest;
	@Mock
	private WPGVendorIdStrategy vendorIdStrategy;

	private Currency currency = Currency.getInstance(CURRENCY_CODE);

	private Map<String, String> resultMap;

	@InjectMocks
	@Spy
	private CaptureRequestTransformer underTest = new CaptureRequestTransformer();

	@Before
	public void setup()
	{
		underTest.setDescription(TEST_DESCRIPTION);
		underTest.setOperation(TEST_OPERATION);

		doReturn(TEST_TIMESTAMP).when(underTest).getTimestamp();

		when(mockTransactionIdGeneratorStrategy.generateTransactionId()).thenReturn(TEST_TRANSACTION_ID);
		when(mockWpgRegionStrategy.getRegionByCurrency(CURRENCY_CODE)).thenReturn(TEST_REGION);
		when(mockWpgHashStrategy.generateSecurityHash(TEST_SITE_ID, TEST_HASH_BASE)).thenReturn(TEST_SECURITY_HASH);
		when(mockConfigurationService.getConfiguration()).thenReturn(mockConfiguration);
		when(vendorIdStrategy.getVendorId(TEST_SITE_ID)).thenReturn(TEST_VENDOR_ID);

		setupCaptureRequest();
	}

	private void setupCaptureRequest()
	{
		when(mockCaptureRequest.getCurrency()).thenReturn(currency);
		when(mockCaptureRequest.getTotalAmount()).thenReturn(new BigDecimal(TEST_AMOUNT));
		when(mockCaptureRequest.getRequestToken()).thenReturn(TEST_TOKEN);
		when(mockCaptureRequest.getAuthCode()).thenReturn(TEST_AUTH_CODE);
		when(mockCaptureRequest.getSite()).thenReturn(TEST_SITE_ID);
	}

	@Test
	public void shouldPopulateRequestValues()
	{
		resultMap = underTest.transform(mockCaptureRequest);

		assertNotEquals(0, resultMap.size());
		assertEquals(TEST_OPERATION, getRequestParam(WileyHttpRequestParams.WPG_OPERATION));
		assertEquals(TEST_TIMESTAMP, getRequestParam(WileyHttpRequestParams.WPG_TIMESTAMP));
		assertEquals(TEST_VENDOR_ID.toString(), getRequestParam(WileyHttpRequestParams.WPG_VENDOR_ID));
		assertEquals(TEST_TRANSACTION_ID, getRequestParam(WileyHttpRequestParams.WPG_TRANSACTION_ID));
		assertEquals(HTTP, getRequestParam(WileyHttpRequestParams.WPG_METHOD));
		assertEquals(TEST_DESCRIPTION, getRequestParam(WileyHttpRequestParams.WPG_DESCRIPTION));
		assertEquals(TEST_AMOUNT, getRequestParam(WileyHttpRequestParams.WPG_VALUE));
		assertEquals(CURRENCY_CODE, getRequestParam(WileyHttpRequestParams.WPG_CURRENCY));
		assertEquals(TEST_REGION, getRequestParam(WileyHttpRequestParams.WPG_REGION));
		assertEquals(TEST_TOKEN, getRequestParam(WileyHttpRequestParams.WPG_TOKEN));
		assertEquals(TEST_AUTH_CODE, getRequestParam(WileyHttpRequestParams.WPG_AUTH_CODE));
		assertEquals(TEST_SECURITY_HASH, getRequestParam(WileyHttpRequestParams.WPG_SECURITY));

	}

	private String getRequestParam(final String paramKey)
	{
		return resultMap.get(paramKey);
	}
}
