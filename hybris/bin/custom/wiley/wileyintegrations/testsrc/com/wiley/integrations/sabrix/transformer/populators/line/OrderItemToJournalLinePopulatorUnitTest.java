package com.wiley.integrations.sabrix.transformer.populators.line;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.enums.WileyProductSubtypeEnum;
import com.wiley.core.model.WileyProductModel;
import com.wiley.integrations.sabrix.dto.IndataLineType;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class OrderItemToJournalLinePopulatorUnitTest
{
	private static final String P_JOURNAL_TAX_CATEGORY = "pJournal";
	private static final String P_BOOKS_TAX_CATEGORY = "pBooks";
	private static final String PRINT_JOURNAL_MEDIA_TYPE = "A";
	@InjectMocks
	private OrderItemToJournalLinePopulator orderItemToJournalLinePopulator;
	@Mock
	private Map<String, String> journalTaxCategory2MediaType;

	@Test
	public void shouldSetMediaTypeAndFulfilmentTypeOneSourceIfJournalProduct()
	{
		ProductModel product = prepareProduct(P_JOURNAL_TAX_CATEGORY, new ProductModel(), true);
		when(journalTaxCategory2MediaType.get(P_JOURNAL_TAX_CATEGORY)).thenReturn(PRINT_JOURNAL_MEDIA_TYPE);
		IndataLineType indataLineType = new IndataLineType();

		orderItemToJournalLinePopulator.populate(product, indataLineType);

		assertEquals(indataLineType.getFULFILMENTTYPE(), OrderItemToJournalLinePopulator.ONETIME_FULFILMENT_TYPE);
		assertEquals(indataLineType.getMEDIATYPE(), PRINT_JOURNAL_MEDIA_TYPE);
	}

	@Test
	public void shouldSetMediaTypeAndFulfilmentTypeSubscriptionIfJournalSubscriptionProductType()
	{
		ProductModel product = prepareProduct(P_JOURNAL_TAX_CATEGORY, new WileyProductModel(), true);
		product.setSubtype(WileyProductSubtypeEnum.SUBSCRIPTION);
		when(journalTaxCategory2MediaType.get(P_JOURNAL_TAX_CATEGORY)).thenReturn(PRINT_JOURNAL_MEDIA_TYPE);
		IndataLineType indataLineType = new IndataLineType();

		orderItemToJournalLinePopulator.populate(product, indataLineType);

		assertEquals(indataLineType.getFULFILMENTTYPE(), OrderItemToJournalLinePopulator.SUBSCRIPTION_FULFILMENT_TYPE);
		assertEquals(indataLineType.getMEDIATYPE(), PRINT_JOURNAL_MEDIA_TYPE);
	}

	@Test
	public void shouldNotSetMediaTypeAndFulfilmentTypeIfNonJournalProduct()
	{
		ProductModel product = prepareProduct(P_BOOKS_TAX_CATEGORY, new ProductModel(), false);
		IndataLineType indataLineType = new IndataLineType();

		orderItemToJournalLinePopulator.populate(product, indataLineType);

		assertNull(indataLineType.getFULFILMENTTYPE());
		assertNull(indataLineType.getMEDIATYPE());
	}

	private ProductModel prepareProduct(final String taxCategory, final ProductModel model, final boolean isJournalProduct)
	{
		model.setTaxCategory(taxCategory);
		when(journalTaxCategory2MediaType.containsKey(taxCategory)).thenReturn(isJournalProduct);
		return model;
	}
}
