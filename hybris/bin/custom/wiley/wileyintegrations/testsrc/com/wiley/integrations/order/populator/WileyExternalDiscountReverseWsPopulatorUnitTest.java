package com.wiley.integrations.order.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyExternalDiscountModel;
import com.wiley.integrations.order.dto.DiscountValueWsDTO;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyExternalDiscountReverseWsPopulatorUnitTest
{
	private static final String ISO_CODE = "USD";
	private static final String CODE = "test_code";
	private static final Double VALUE = 12.34;
	private static final String GUID = "094c59d5-17d9-43f7-9461-89049e32d896";

	@Spy
	@InjectMocks
	private WileyExternalDiscountReverseWsPopulator testInstance;

	private DiscountValueWsDTO source = new DiscountValueWsDTO();

	private WileyExternalDiscountModel target = new WileyExternalDiscountModel();

	@Mock
	private CurrencyModel currencyMock;

	@Mock
	private KeyGenerator keyGeneratorMock;

	@Mock
	private ModelService modelServiceMock;

	@Mock
	private CommonI18NService commonI18NServiceMock;

	@Before
	public void prepare()
	{
		when(modelServiceMock.create(WileyExternalDiscountModel.class)).thenReturn(target);
		when(keyGeneratorMock.generate()).thenReturn(GUID);
		when(commonI18NServiceMock.getCurrency(ISO_CODE)).thenReturn(currencyMock);

		when(currencyMock.getIsocode()).thenReturn(ISO_CODE);

		source.setCode(CODE);
		source.setCurrency(ISO_CODE);
		source.setValue(VALUE);
	}

	@Test
	public void shouldPopulateExternalDiscountFields()
	{
		//When
		testInstance.populate(source, target);

		//Then
		assertEquals(GUID, target.getGuid());
		assertEquals(CODE, target.getCode());
		assertEquals(VALUE, target.getValue());
		assertEquals(ISO_CODE, target.getCurrency().getIsocode());
	}

	@Test
	public void shouldNotPopulateCurrencyIfIsoCodeIsNull()
	{
		//Given
		source.setCurrency(null);

		//When
		testInstance.populate(source, target);

		//Then
		assertNull(target.getCurrency());
	}


	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowAnExceptionWhenSourceIsNull()
	{
		testInstance.populate(null, target);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowAnExceptionWhenTargetIsNull()
	{
		testInstance.populate(source, null);
	}
}
