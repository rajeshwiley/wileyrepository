package com.wiley.integrations.alm.authentication.transformer;

import de.hybris.bootstrap.annotations.UnitTest;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.integration.alm.authentication.dto.AlmAuthenticationResponseDto;
import com.wiley.integrations.alm.authentication.dto.AlmAuthenticationImitateeDto;
import com.wiley.integrations.alm.authentication.dto.AlmAuthenticationSessionDto;
import com.wiley.integrations.alm.authentication.dto.AlmAuthenticationUserDto;

import static org.springframework.http.HttpStatus.OK;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AlmAuthenticationValidateResponseToDtoTransformerUnitTest
{
	private static final String USER_ID = "USER_ID";
	private static final String IMITATEE_ID = "IMITATEE_ID";

	private final AlmAuthenticationValidateResponseToDtoTransformer transformer =
			new AlmAuthenticationValidateResponseToDtoTransformer();

	@Test
	public void transformImitateeIdNotNullTest()
	{
		final AlmAuthenticationSessionDto sessionDto = new AlmAuthenticationSessionDto();
		final AlmAuthenticationUserDto user = new AlmAuthenticationUserDto();
		final AlmAuthenticationImitateeDto imitatee = new AlmAuthenticationImitateeDto();

		user.setUserId(USER_ID);
		imitatee.setImitateeId(IMITATEE_ID);
		sessionDto.setUser(user);
		sessionDto.setImitatee(imitatee);

		final AlmAuthenticationResponseDto transform = transformer.transform(sessionDto);

		assertNotNull(transform);
		assertEquals(USER_ID, transform.getUserId());
		assertEquals(IMITATEE_ID, transform.getImitateeId());
		assertEquals(OK, transform.getHttpStatus());
	}

	@Test
	public void transformImitateeIdIsNullTest()
	{
		final AlmAuthenticationSessionDto sessionDto = new AlmAuthenticationSessionDto();
		final AlmAuthenticationUserDto user = new AlmAuthenticationUserDto();

		user.setUserId(USER_ID);
		sessionDto.setUser(user);

		final AlmAuthenticationResponseDto transform = transformer.transform(sessionDto);

		assertNotNull(transform);
		assertEquals(USER_ID, transform.getUserId());
		assertNull(transform.getImitateeId());
		assertEquals(OK, transform.getHttpStatus());
	}

	@Test(expected = IllegalArgumentException.class)
	public void transformNullSessionDtoFailedTest()
	{
		transformer.transform(null);
	}
}