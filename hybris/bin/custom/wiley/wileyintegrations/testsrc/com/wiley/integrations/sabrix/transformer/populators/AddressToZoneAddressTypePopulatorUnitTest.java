package com.wiley.integrations.sabrix.transformer.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.user.AddressModel;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import java.util.Set;

import com.wiley.integrations.sabrix.dto.ZoneAddressType;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AddressToZoneAddressTypePopulatorUnitTest
{
	private static final String GB_COUNTRY = "GB";
	private static final String TOWN = "town";
	private static final String POSTAL_CODE = "postalCode";
	private static final String REGION_CODE = "ON";
	@InjectMocks
	private AddressToZoneAddressTypePopulator addressToZoneAddressTypePopulator;
	@Mock
	private Set<String> statesToCountiesSet;

	@Test
	public void shouldNotSetStateOrProvinceIfNoRegion()
	{
		AddressModel addressModel = prepareAddressModel(GB_COUNTRY);
		ZoneAddressType zoneAddressType = new ZoneAddressType();

		addressToZoneAddressTypePopulator.populate(addressModel, zoneAddressType);

		assertCountryCityAndPostalCode(zoneAddressType, GB_COUNTRY);
		assertNull(zoneAddressType.getSTATE());
		assertNull(zoneAddressType.getPROVINCE());
	}

	private void assertCountryCityAndPostalCode(final ZoneAddressType zoneAddressType, final String country)
	{
		assertEquals(zoneAddressType.getPOSTCODE(), POSTAL_CODE);
		assertEquals(zoneAddressType.getCITY(), TOWN);
		assertEquals(zoneAddressType.getCOUNTRY(), country);
	}

	@Test
	public void shouldSetProvinceIfCanadaCountry()
	{
		AddressModel addressModel = prepareAddressModel(AddressToZoneAddressTypePopulator.CANADA_ISO_CODE);
		populateRegion(addressModel, REGION_CODE);
		ZoneAddressType zoneAddressType = new ZoneAddressType();

		addressToZoneAddressTypePopulator.populate(addressModel, zoneAddressType);

		assertCountryCityAndPostalCode(zoneAddressType, AddressToZoneAddressTypePopulator.CANADA_ISO_CODE);
		assertEquals(zoneAddressType.getPROVINCE(), REGION_CODE);
		assertNull(zoneAddressType.getSTATE());
	}

	@Test
	public void shouldSetStateIfUSCountry()
	{
		AddressModel addressModel = prepareAddressModel(AddressToZoneAddressTypePopulator.US_ISO_CODE);
		populateRegion(addressModel, REGION_CODE);
		ZoneAddressType zoneAddressType = new ZoneAddressType();

		addressToZoneAddressTypePopulator.populate(addressModel, zoneAddressType);

		assertCountryCityAndPostalCode(zoneAddressType, AddressToZoneAddressTypePopulator.US_ISO_CODE);
		assertNull(zoneAddressType.getPROVINCE());
		assertEquals(zoneAddressType.getSTATE(), REGION_CODE);
	}

	@Test
	public void shouldSetCountryFromRegionIfUSCountryAndStateFromStatesToCountiesSet()
	{
		AddressModel addressModel = prepareAddressModel(AddressToZoneAddressTypePopulator.US_ISO_CODE);
		populateRegion(addressModel, REGION_CODE);
		ZoneAddressType zoneAddressType = new ZoneAddressType();
		when(statesToCountiesSet.contains(REGION_CODE)).thenReturn(true);

		addressToZoneAddressTypePopulator.populate(addressModel, zoneAddressType);

		assertCountryCityAndPostalCode(zoneAddressType, REGION_CODE);
		assertNull(zoneAddressType.getPROVINCE());
		assertNull(zoneAddressType.getSTATE());
	}

	private void populateRegion(final AddressModel addressModel, final String isocodeShort)
	{
		RegionModel regionModel = new RegionModel();
		regionModel.setIsocodeShort(isocodeShort);
		addressModel.setRegion(regionModel);
	}

	private AddressModel prepareAddressModel(final String country)
	{
		AddressModel addressModel = new AddressModel();
		addressModel.setTown(TOWN);
		addressModel.setPostalcode(POSTAL_CODE);
		CountryModel countryModel = new CountryModel();
		countryModel.setIsocode(country);
		addressModel.setCountry(countryModel);
		return addressModel;
	}

}
