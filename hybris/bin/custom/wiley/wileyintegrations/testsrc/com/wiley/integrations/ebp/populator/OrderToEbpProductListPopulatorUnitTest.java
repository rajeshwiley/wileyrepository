package com.wiley.integrations.ebp.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.PinModel;
import com.wiley.core.pin.service.PinService;
import com.wiley.core.integration.ebp.dto.EbpAddProductsPayload;
import com.wiley.integrations.ebp.dto.ProductNode;
import com.wiley.integrations.promotioncode.EbpPromotionCodeProviderImpl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;


/**
 * Created by Uladzimir_Barouski on 2/18/2016.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class OrderToEbpProductListPopulatorUnitTest
{
	private static final String PRODUCT_1_NAME = "product 1";
	private static final String PRODUCT_1_ISBN = "product 1 isbn";
	private static final Long PRODUCT_1_QUANTITY = 2L;
	private static final String PRODUCT_2_NAME = "product 2";
	private static final String PRODUCT_2_ISBN = "product 2 isbn";
	private static final Long PRODUCT_2_QUANTITY = 8L;

	private static final String PRODUCT_NAME = "productName";
	private static final String PRODUCT_SKU = "productSKU";
	private static final String PRODUCT_QUANTITY = "quantity";
	private static final String PRODUCT_EXPIRATION_DATE = "productExpirationDate";
	public static final String PIN_CODE = "pinCode";

	@Mock
	private OrderModel orderMock;
	@Mock
	private EbpAddProductsPayload ebpAddProductsPayload;
	@Mock(answer = Answers.RETURNS_DEEP_STUBS)
	private AbstractOrderEntryModel entry1Mock;
	@Mock(answer = Answers.RETURNS_DEEP_STUBS)
	private AbstractOrderEntryModel entry2Mock;
	@Mock
	private EbpPromotionCodeProviderImpl promotionCodeProvider;
	@Mock
	private PinService pinService;
	@InjectMocks
	private OrderToEbpProductListPopulator testPopulator;

	@Before
	public void setUp() throws Exception
	{
		when(ebpAddProductsPayload.getOrder()).thenReturn(orderMock);
		when(orderMock.getEntries()).thenReturn(Arrays.asList(entry1Mock, entry2Mock));
		when(entry1Mock.getProduct().getName()).thenReturn(PRODUCT_1_NAME);
		when(entry1Mock.getProduct().getIsbn()).thenReturn(PRODUCT_1_ISBN);
		doReturn(PRODUCT_1_QUANTITY).when(entry1Mock).getQuantity();
		when(entry2Mock.getProduct().getName()).thenReturn(PRODUCT_2_NAME);
		when(entry2Mock.getProduct().getIsbn()).thenReturn(PRODUCT_2_ISBN);
		doReturn(PRODUCT_2_QUANTITY).when(entry2Mock).getQuantity();

		when(promotionCodeProvider.getOrderPromotionCode(orderMock)).thenReturn(Optional.empty());
		when(pinService.isPinUsedForOrder(orderMock)).thenReturn(false);
	}

	@Test
	public void testPopulate() throws Exception
	{
		//given
		List<ProductNode> productList = new ArrayList<ProductNode>();

		//testing
		testPopulator.populate(ebpAddProductsPayload, productList);

		//verify
		assertThat(productList, contains(
				allOf(
						hasProperty(PRODUCT_NAME, equalTo(PRODUCT_1_NAME)),
						hasProperty(PRODUCT_SKU, equalTo(PRODUCT_1_ISBN)),
						hasProperty(PRODUCT_QUANTITY, equalTo(PRODUCT_1_QUANTITY)),
						hasProperty(PRODUCT_EXPIRATION_DATE, nullValue()),
						hasProperty(PIN_CODE, nullValue())),
				allOf(
						hasProperty(PRODUCT_NAME, equalTo(PRODUCT_2_NAME)),
						hasProperty(PRODUCT_SKU, equalTo(PRODUCT_2_ISBN)),
						hasProperty(PRODUCT_QUANTITY, equalTo(PRODUCT_2_QUANTITY)),
						hasProperty(PRODUCT_EXPIRATION_DATE, nullValue()),
						hasProperty(PIN_CODE, nullValue()))
		));
	}

	@Test
	public void testPopulatePin()
	{
		// Given
		when(pinService.isPinUsedForOrder(orderMock)).thenReturn(true);
		final PinModel pinModel = new PinModel();
		pinModel.setCode("pinpin");
		when(pinService.getPinForOrder(orderMock)).thenReturn(pinModel);

		// When
		List<ProductNode> productList = new ArrayList<ProductNode>();
		testPopulator.populate(ebpAddProductsPayload, productList);

		// Then
		assertEquals("PIN code does not match", productList.get(0).getPinCode(), "pinpin");
	}
}
