package com.wiley.integrations.esb.converters.populator.orderentry;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.subscriptionservices.model.SubscriptionTermModel;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.enums.ProductEditionFormat;
import com.wiley.core.enums.WileyDigitalContentTypeEnum;
import com.wiley.integrations.esb.dto.common.AbstractOrderEntryRequest;
import com.wiley.integrations.esb.dto.order.OrderEntryRequest;


/**
 * Unit test for {@link OrderEntryModelToOrderEntryRequestSimpleFieldsPopulator}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class OrderEntryModelToOrderEntryRequestSimpleFieldsPopulatorUnitTest
{
	private static final String ID = "ID";
	private static final WileyDigitalContentTypeEnum CONTENT_TYPE_VALUE = WileyDigitalContentTypeEnum.VITAL_SOURCE;
	
	
	@Mock
	private Populator<AbstractOrderEntryModel, AbstractOrderEntryRequest> abstractOrderEntryRequestPopulatorMock;

	@Mock
	private OrderEntryModel orderEntryModelMock;
	
	@Mock
	private OrderEntryRequest orderEntryRequestMock;
	
	@Mock
	private SubscriptionTermModel subscriptionTermModelMock;
	
	@Mock
	private ProductModel productModelMock;
	
	@InjectMocks
	private OrderEntryModelToOrderEntryRequestSimpleFieldsPopulator populator;

	@Before
	public void setUp()
	{
		doReturn(productModelMock).when(orderEntryModelMock).getProduct();
	}

	@Test
	public void shouldPopulateSubscriptionTerm()
	{
		// Given
		doReturn(subscriptionTermModelMock).when(orderEntryModelMock).getSubscriptionTerm();
		doReturn(ID).when(subscriptionTermModelMock).getId();

		// When
		populate();

		// Then

		verify(orderEntryRequestMock).setSubscriptionTerm(ID);
	}

	@Test
	public void shouldNotPopulateSubscriptionTermForNonSubscriptionProducts()
	{
		// Given
		
		// When
		populate();

		// Then
		verify(orderEntryRequestMock, never()).setSubscriptionTerm(any());
	}

	@Test
	public void shouldPopulateDigitalContentType()
	{
		// Given
		doReturn(ProductEditionFormat.DIGITAL).when(productModelMock).getEditionFormat();
		doReturn(CONTENT_TYPE_VALUE).when(productModelMock).getDigitalContentType();

		// When
		populate();

		// Then
		verify(orderEntryRequestMock).setDigitalContentType(CONTENT_TYPE_VALUE);
	}
	
	@Test
	public void shouldNotPopulateDigitalContentTypeForNonDigital()
	{
		// Given
		doReturn(ProductEditionFormat.PHYSICAL).when(productModelMock).getEditionFormat();

		// When
		populate();

		// Then
		verify(orderEntryRequestMock, never()).setDigitalContentType(any());
	}

	
	protected void populate()
	{
		populator.populate(orderEntryModelMock, orderEntryRequestMock);
	}
}