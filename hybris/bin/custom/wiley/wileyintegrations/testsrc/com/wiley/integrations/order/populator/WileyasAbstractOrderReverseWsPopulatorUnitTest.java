package com.wiley.integrations.order.populator;

import com.wiley.core.enums.OrderType;
import com.wiley.core.model.WileyExternalDiscountModel;
import com.wiley.core.order.strategies.calculation.WileyFindExternalPriceStrategy;
import com.wiley.core.payment.WileyPaymentModeService;
import com.wiley.integrations.exceptions.carts.UnknownCountryException;
import com.wiley.integrations.order.dto.AddressWsDTO;
import com.wiley.integrations.order.dto.CreateOrderEntryRequestWsDTO;
import com.wiley.integrations.order.dto.CreateOrderRequestWsDTO;
import com.wiley.integrations.order.dto.DiscountValueWsDTO;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.order.strategies.calculation.FindDiscountValuesStrategy;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.util.PriceValue;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyasAbstractOrderReverseWsPopulatorUnitTest
{
    private static final String TEST_ID = "test id";
    private static final String TEST_TYPE = "general";
    private static final String TEST_COUNTRY = "US";
    private static final String TEST_CURRENCY = "USD";
    private static final Integer TEST_DIGITS = 2;
    private static final String TEST_PRODUCT_CODE = "test product code";
    private static final String TEST_USER_ID = "user id";
    private static final String TEST_ENTRY_ID = "entry id";
    private static final CreateOrderEntryRequestWsDTO TEST_ENTRY = new CreateOrderEntryRequestWsDTO();
    private static final List<CreateOrderEntryRequestWsDTO> TEST_ENTRIES =
            Collections.singletonList(TEST_ENTRY);
    private static final String TEST_PAYMENT_MODE = "card";
    private static final AddressWsDTO TEST_PAYMENT_ADDRESS = new AddressWsDTO();
    private static final AddressWsDTO TEST_CONTACT_ADDRESS = new AddressWsDTO();
    private static final String TEST_EXTERNAL_TEXT = "external text";
    private static final String TEST_PURCHASE_ORDER_NUMBER = "purchase order number";
    private static final String TEST_USER_NOTES = "user notes";
    private static final Double TEST_SUBTOTAL = 10d;
    private static final DiscountValueWsDTO TEST_DISCOUNT_VALUE_WS_DTO = new DiscountValueWsDTO();
    private static final List<DiscountValueWsDTO> TEST_EXTERNAL_DISCOUNTS =
            Collections.singletonList(TEST_DISCOUNT_VALUE_WS_DTO);
    private static final Double TEST_TOTAL_DISCOUNTS = 2d;
    private static final String TEST_TAX_NUMBER = "tax number";
    private static final Boolean TEST_IS_TAX_NUMBER_VALIDATED = Boolean.TRUE;
    private static final Date TEST_TAX_NUMBER_EXPIRATION_DATE = new Date();
    private static final Double TEST_TOTAL_TAX = 3d;
    private static final Boolean TEST_IS_TAX_CALCULATED = Boolean.FALSE;
    private static final Double TEST_TOTAL_PRICE = 4d;
    private static final Collection<String> TEST_APPLIED_COUPON_CODES = new ArrayList<>();
    private static final Date TEST_DATE = new Date();
    private static final String TEST_SOURCE_SYSTEM = "source system";
    private static final DiscountValue TEST_DISCOUNT_VALUE = new DiscountValue("", 1d, true, "USD");
    private static final PriceValue TEST_PRICE_VALUE = new PriceValue("", 1d, true);
    private static final Double TEST_ENTRY_SUBTOTAL = 5d;
    private static final Long TEST_QUANTITY = 2L;

    @Mock
    private CommonI18NService commonI18NServiceMock;

    @Mock
    private WileyPaymentModeService wileyPaymentModeServiceMock;

    @Mock
    private UserService userService;

    @Mock
    private ProductService productServiceMock;

    @Mock
    private ModelService modelServiceMock;

    @Mock
    private EnumerationService enumerationServiceMock;

    @Mock
    private CountryModel countryModelMock;

    @Mock
    private CurrencyModel currencyModelMock;

    @Mock
    private CustomerModel userModelMock;

    @Mock
    private PaymentModeModel paymentModeModelMock;

    @Mock
    private AddressModel addressModelMock;

    @Mock
    private OrderEntryModel orderEntryModelMock;

    @Mock
    private ProductModel productModelMock;

    @Mock
    private UnitModel unitModelMock;

    @Mock
    private PaymentInfoModel paymentInfoModelMock;

    @Mock
    private FindDiscountValuesStrategy findDiscountValuesStrategy;

    @Mock
    private Converter<CreateOrderEntryRequestWsDTO, OrderEntryModel> wileyasOrderEntryReverseWsConverter;

    @Mock
    private WileyExternalDiscountModel wileyExternalDiscountModelMock;

    @Mock
    private WileyFindExternalPriceStrategy wileyFindExternalPriceStrategyMock;

    @Mock
    private Converter<AddressWsDTO, AddressModel> wileyasAddressReverseWsConverter;

    @Mock
    private Converter<DiscountValueWsDTO, WileyExternalDiscountModel> wileyExternalDiscountReverseWsConverter;

    @InjectMocks
    private WileyasAbstractOrderReverseWsPopulator populator;

    @Test
    public void populateTestWithIdAndWithPaymentMode() throws CalculationException
    {
        populateTest(true, true);
    }

    @Test
    public void populateTestWithoutIdAndWithoutPaymentMode() throws CalculationException
    {
        populateTest(false, false);
    }

    @Test
    public void populateTestWithIdAndWithoutPaymentMode() throws CalculationException
    {
        populateTest(true, false);
    }

    @Test
    public void populateTestWithoutIdAndWithPaymentMode() throws CalculationException
    {
        populateTest(false, true);
    }

    @Before
	public void setUp()
	{
		TEST_ENTRY.setProductCode(TEST_PRODUCT_CODE);
		TEST_ENTRY.setEntryId(TEST_ENTRY_ID);
	}

    public void populateTest(final boolean isIdExists, final boolean paymentModeExists) throws CalculationException
    {
        final CreateOrderRequestWsDTO source = new CreateOrderRequestWsDTO();
        final AbstractOrderModel orderMock = mock(AbstractOrderModel.class);

        if (isIdExists)
        {
            source.setId(TEST_ID);
        }

        source.setType(TEST_TYPE);
        source.setCountry(TEST_COUNTRY);
        source.setCurrency(TEST_CURRENCY);
        source.setUserId(TEST_USER_ID);
        source.setEntries(TEST_ENTRIES);

        if (paymentModeExists)
        {
            source.setPaymentMode(TEST_PAYMENT_MODE);
        }

        source.setPaymentAddress(TEST_PAYMENT_ADDRESS);
        source.setContactAddress(TEST_CONTACT_ADDRESS);
        source.setExternalText(TEST_EXTERNAL_TEXT);
        source.setPurchaseOrderNumber(TEST_PURCHASE_ORDER_NUMBER);
        source.setUserNotes(TEST_USER_NOTES);
        source.setSubtotal(TEST_SUBTOTAL);
        source.setExternalDiscounts(TEST_EXTERNAL_DISCOUNTS);
        source.setTotalDiscounts(TEST_TOTAL_DISCOUNTS);
        source.setTaxNumber(TEST_TAX_NUMBER);
        source.setTaxNumberValidated(TEST_IS_TAX_NUMBER_VALIDATED);
        source.setTaxNumberExpirationDate(TEST_TAX_NUMBER_EXPIRATION_DATE);
        source.setTotalTax(TEST_TOTAL_TAX);
        source.setTaxCalculated(TEST_IS_TAX_CALCULATED);
        source.setTotalPrice(TEST_TOTAL_PRICE);
        source.setAppliedCouponCodes(TEST_APPLIED_COUPON_CODES);
        source.setDate(TEST_DATE);
        source.setSourceSystem(TEST_SOURCE_SYSTEM);

        when(commonI18NServiceMock.getCountry(TEST_COUNTRY)).thenReturn(countryModelMock);
        when(commonI18NServiceMock.getCurrency(TEST_CURRENCY)).thenReturn(currencyModelMock);
        when(userService.getUserForUID(TEST_USER_ID)).thenReturn(userModelMock);
        when(wileyasOrderEntryReverseWsConverter.convert(TEST_ENTRY)).thenReturn(orderEntryModelMock);
        when(productServiceMock.getProductForCode(TEST_PRODUCT_CODE)).thenReturn(productModelMock);
        when(productServiceMock.getOrderableUnit(productModelMock)).thenReturn(unitModelMock);
        when(wileyFindExternalPriceStrategyMock.findExternalPrice(orderEntryModelMock)).thenReturn(TEST_PRICE_VALUE);
        when(orderEntryModelMock.getSubtotalPrice()).thenReturn(TEST_ENTRY_SUBTOTAL);
        when(orderEntryModelMock.getQuantity()).thenReturn(TEST_QUANTITY);
        when(wileyPaymentModeServiceMock.getPaymentModeForCode(TEST_PAYMENT_MODE))
                .thenReturn(paymentModeModelMock);
        when(wileyasAddressReverseWsConverter.convert(TEST_PAYMENT_ADDRESS))
                .thenReturn(addressModelMock);
        when(orderMock.getPaymentAddress()).thenReturn(addressModelMock);
        when(orderMock.getContactAddress()).thenReturn(addressModelMock);
        when(wileyExternalDiscountReverseWsConverter.convert(TEST_DISCOUNT_VALUE_WS_DTO))
                .thenReturn(wileyExternalDiscountModelMock);
        when(orderMock.getExternalDiscounts()).thenReturn(
                Collections.singletonList(wileyExternalDiscountModelMock));
        when(findDiscountValuesStrategy.findDiscountValues(orderMock)).thenReturn(Collections.singletonList(TEST_DISCOUNT_VALUE));
        when(orderMock.getCurrency()).thenReturn(currencyModelMock);
        when(orderMock.getSubtotal()).thenReturn(TEST_SUBTOTAL);
        when(currencyModelMock.getIsocode()).thenReturn(TEST_CURRENCY);
        when(currencyModelMock.getDigits()).thenReturn(TEST_DIGITS);
        when(modelServiceMock.create(PaymentInfoModel.class)).thenReturn(paymentInfoModelMock);
        when(enumerationServiceMock.getEnumerationValue(OrderType.class, TEST_TYPE)).thenReturn(OrderType.valueOf(TEST_TYPE));
        when(orderMock.getUser()).thenReturn(userModelMock);
        when(userModelMock.getUid()).thenReturn(anyString());

        populator.populate(source, orderMock);

        if (isIdExists)
        {
            verify(orderMock, atLeastOnce()).setGuid(TEST_ID);
        }
        else
        {
            verify(orderMock, atLeastOnce()).setGuid(anyString());
        }

        verify(orderMock, atLeastOnce()).setOrderType(OrderType.valueOf(TEST_TYPE));
        verify(orderMock, atLeastOnce()).setCountry(countryModelMock);
        verify(orderMock, atLeastOnce()).setCurrency(currencyModelMock);
        verify(orderMock, atLeastOnce()).setUser(userModelMock);
        verify(wileyasOrderEntryReverseWsConverter, atLeastOnce()).convert(TEST_ENTRY);
        verify(productServiceMock, atLeastOnce()).getProductForCode(TEST_PRODUCT_CODE);
        verify(productServiceMock, atLeastOnce()).getOrderableUnit(productModelMock);
        verify(orderEntryModelMock, atLeastOnce()).setOrder(orderMock);
        verify(orderEntryModelMock, atLeastOnce()).setUnit(unitModelMock);
        verify(orderMock, atLeastOnce()).setEntries(anyListOf(AbstractOrderEntryModel.class));

        if (paymentModeExists)
        {
            verify(orderMock, atLeastOnce()).setPaymentMode(paymentModeModelMock);
        }

        verify(orderMock, atLeastOnce()).setPaymentAddress(addressModelMock);
        verify(orderMock, atLeastOnce()).setContactAddress(addressModelMock);
        verify(orderMock, atLeastOnce()).setExternalText(TEST_EXTERNAL_TEXT);
        verify(orderMock, atLeastOnce()).setPurchaseOrderNumber(TEST_PURCHASE_ORDER_NUMBER);
        verify(orderMock, atLeastOnce()).setUserNotes(TEST_USER_NOTES);
        verify(orderMock, atLeastOnce()).setSubtotal(TEST_SUBTOTAL);
        verify(orderMock, atLeastOnce()).setExternalDiscounts(anyListOf(WileyExternalDiscountModel.class));
        verify(orderMock, atLeastOnce()).setGlobalDiscountValues(anyListOf(DiscountValue.class));
        verify(orderMock, atLeastOnce()).setTotalDiscounts(TEST_TOTAL_DISCOUNTS);
        verify(orderMock, atLeastOnce()).setTaxNumber(TEST_TAX_NUMBER);
        verify(orderMock, atLeastOnce()).setTaxNumberValidated(TEST_IS_TAX_NUMBER_VALIDATED);
        verify(orderMock, atLeastOnce()).setTaxNumberExpirationDate(TEST_TAX_NUMBER_EXPIRATION_DATE);
        verify(orderMock, atLeastOnce()).setTotalTax(TEST_TOTAL_TAX);
        verify(orderMock, atLeastOnce()).setTaxCalculated(TEST_IS_TAX_CALCULATED);
        verify(orderMock, atLeastOnce()).setTotalPrice(TEST_TOTAL_PRICE);
        verify(orderMock, atLeastOnce()).setAppliedCouponCodes(TEST_APPLIED_COUPON_CODES);
        verify(orderMock, atLeastOnce()).setDate(TEST_DATE);
        verify(orderMock, atLeastOnce()).setSourceSystem(TEST_SOURCE_SYSTEM);
        verify(orderMock, atLeastOnce()).setSubTotalWithoutDiscount(TEST_ENTRY_SUBTOTAL);
        verify(orderMock, atLeastOnce()).getContactAddress();
        verify(orderMock, atLeastOnce()).getContactAddress();
        verify(orderMock, atLeastOnce()).setStatus(OrderStatus.CREATED);
        verify(addressModelMock, atLeastOnce()).setOwner(orderMock);
        verify(orderEntryModelMock, atLeastOnce()).setSubtotalPrice(TEST_PRICE_VALUE.getValue());
        verify(orderEntryModelMock, atLeastOnce()).setBasePrice(TEST_ENTRY_SUBTOTAL / TEST_QUANTITY);

        if (paymentModeExists)
        {
            verify(modelServiceMock, atLeastOnce()).create(PaymentInfoModel.class);
            verify(paymentInfoModelMock, atLeastOnce()).setUser(userModelMock);
            verify(paymentInfoModelMock, atLeastOnce()).setCode(anyString());
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void populateSourceNullTest()
    {
        final AbstractOrderModel orderMock = mock(AbstractOrderModel.class);
        populator.populate(null, orderMock);
    }

    @Test(expected = IllegalArgumentException.class)
    public void populateTargetNullTest()
    {
        populator.populate(new CreateOrderRequestWsDTO(), null);
    }

    @Test(expected = UnknownCountryException.class)
    public void populateUnknownCountryTest()
    {
        final CreateOrderRequestWsDTO source = new CreateOrderRequestWsDTO();
        source.setCountry(TEST_COUNTRY);
		when(commonI18NServiceMock.getCountry(TEST_COUNTRY)).thenThrow(UnknownIdentifierException.class);
        final AbstractOrderModel orderMock = mock(AbstractOrderModel.class);
        populator.populate(source, orderMock);
    }

	@Test(expected = AmbiguousIdentifierException.class)
	public void populateAmbiguousEntryIdentifierTest()
	{
		final CreateOrderRequestWsDTO source = new CreateOrderRequestWsDTO();
		final List<CreateOrderEntryRequestWsDTO> testEntries = new ArrayList<>(2);
		when(wileyasOrderEntryReverseWsConverter.convert(TEST_ENTRY)).thenReturn(orderEntryModelMock);
		when(orderEntryModelMock.getGuid()).thenReturn(TEST_ENTRY_ID);
		testEntries.add(TEST_ENTRY);
		testEntries.add(TEST_ENTRY);
		source.setEntries(testEntries);
		final AbstractOrderModel orderMock = mock(AbstractOrderModel.class);
		populator.populate(source, orderMock);
	}
}