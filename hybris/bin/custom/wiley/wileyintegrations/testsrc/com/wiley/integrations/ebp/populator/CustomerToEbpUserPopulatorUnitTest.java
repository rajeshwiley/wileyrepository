package com.wiley.integrations.ebp.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

import java.util.Set;

import org.apache.commons.lang.math.RandomUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.integration.ebp.dto.EbpCreateUserPayload;
import com.wiley.core.order.WileyCheckoutService;
import com.wiley.integrations.ebp.dto.AddressNode;
import com.wiley.integrations.ebp.dto.UserNode;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CustomerToEbpUserPopulatorUnitTest
{
	private static final String CUSTOMER_FIRST_NAME = "first name";
	private static final String CUSTOMER_LAST_NAME = "last name";
	private static final String CUSTOMER_UID = "uid";
	private static final String COUNTRY_ISO_US = "US";
	private static final String COUNTRY_ISO_CA = "CA";
	private static final String COUNTRY_ISO_NOT_US_CA = "NOT US CA";
	private static final String UNIVERSITY_NAME = "UNIVERSITY_NAME";
	private static final String UNIVERSITY_STATE = "UNIVERSITY_STATE";
	private static final String UNIVERSITY_COUNTRY = "UNIVERSITY_COUNTRY";
	private static final String BILLING_COUNTRY_CODE = "000";
	private static final String BILLING_ADDRESS_LINE_1 = "billing line 1";
	private static final String BILLING_ADDRESS_LINE_2 = "billing line 2";
	private static final String BILLING_TOWN = "town";
	private static final String BILLING_POSTAL_CODE = "00000";
	private static final String BILLING_PHONE = "000000";
	private static final String BILLING_REGION_CODE = "00";

	private static final String SHIPPING_COUNTRY_CODE = "111";
	private static final String SHIPPING_ADDRESS_LINE_1 = "shipping line 1";
	private static final String SHIPPING_ADDRESS_LINE_2 = "shipping line 2";
	private static final String SHIPPING_TOWN = "town";
	private static final String SHIPPING_POSTAL_CODE = "11111";
	private static final String SHIPPING_PHONE = "111111";
	private static final String SHIPPING_REGION_CODE = "11";
	private static final String CUSTOMER_EMAIL = "email";


	@Mock(answer = Answers.RETURNS_DEEP_STUBS)
	private CustomerModel customerMock;

	@Mock(answer = Answers.RETURNS_DEEP_STUBS)
	private OrderModel orderMock;

	@Mock(answer = Answers.RETURNS_DEEP_STUBS)
	private EbpCreateUserPayload ebpCreateUserPayloadMock;
	@Mock
	private UserNode userNodeMock;
	@Mock(answer = Answers.RETURNS_DEEP_STUBS)
	private AddressModel defaultShippingAddressMock;
	@Mock
	private RegionModel shippingRegionMock;
	@Mock(answer = Answers.RETURNS_DEEP_STUBS)
	private AddressModel defaultBillingAddressMock;
	@Mock
	private RegionModel billingRegionMock;
	@Mock
	private WileyCheckoutService checkoutService;

	@Mock
	private Set<String> otherStateProvince;

	@InjectMocks
	private CustomerToEbpUserPopulator testPopulator;

	@Mock
	private Set<String> countriesStateProvince;

	@Before
	public void setUp() throws Exception
	{
		when(ebpCreateUserPayloadMock.getCustomer()).thenReturn(customerMock);
		when(ebpCreateUserPayloadMock.getUniversity()).thenReturn(UNIVERSITY_NAME);
		when(ebpCreateUserPayloadMock.getUniversityCountry()).thenReturn(UNIVERSITY_COUNTRY);
		when(ebpCreateUserPayloadMock.getUniversityState()).thenReturn(UNIVERSITY_STATE);
		when(ebpCreateUserPayloadMock.getOrder()).thenReturn(orderMock);
		when(customerMock.getFirstName()).thenReturn(CUSTOMER_FIRST_NAME);
		when(customerMock.getLastName()).thenReturn(CUSTOMER_LAST_NAME);
		when(customerMock.getUid()).thenReturn(CUSTOMER_UID);
		when(customerMock.getContactEmail()).thenReturn(CUSTOMER_EMAIL);
		when(otherStateProvince.contains(COUNTRY_ISO_CA)).thenReturn(true);
		setUpAddress(BILLING_COUNTRY_CODE, BILLING_ADDRESS_LINE_1, BILLING_ADDRESS_LINE_2, BILLING_TOWN,
				BILLING_POSTAL_CODE, BILLING_PHONE, defaultBillingAddressMock, billingRegionMock);

		setUpAddress(SHIPPING_COUNTRY_CODE, SHIPPING_ADDRESS_LINE_1, SHIPPING_ADDRESS_LINE_2, SHIPPING_TOWN,
				SHIPPING_POSTAL_CODE, SHIPPING_PHONE, defaultShippingAddressMock, shippingRegionMock);
		when(defaultBillingAddressMock.getPk()).thenReturn(PK.createFixedUUIDPK(0, RandomUtils.nextLong()));
		when(defaultShippingAddressMock.getPk()).thenReturn(PK.createFixedUUIDPK(0, RandomUtils.nextLong()));
		when(countriesStateProvince.contains(eq("US"))).thenReturn(true);
	}

	@Test
	public void testPopulateUniversityData() throws Exception
	{
		when(orderMock.getDeliveryAddress()).thenReturn(defaultShippingAddressMock);
		when(orderMock.getPaymentAddress()).thenReturn(defaultBillingAddressMock);

		when(defaultBillingAddressMock.getCountry().getIsocode()).thenReturn(COUNTRY_ISO_NOT_US_CA);
		when(defaultShippingAddressMock.getCountry().getIsocode()).thenReturn(COUNTRY_ISO_NOT_US_CA);

		UserNode userNode = new UserNode();
		testPopulator.populate(ebpCreateUserPayloadMock, userNode);
		assertEquals(UNIVERSITY_NAME, userNode.getEmployerOrUniversity());
		assertEquals(UNIVERSITY_COUNTRY, userNode.getUniversityCountry());
		assertEquals(UNIVERSITY_STATE, userNode.getUniversityState());
	}

	@Test
	public void testPopulateForNotUSandCAAddress() throws Exception
	{
		when(orderMock.getDeliveryAddress()).thenReturn(defaultShippingAddressMock);
		when(orderMock.getPaymentAddress()).thenReturn(defaultBillingAddressMock);

		when(defaultBillingAddressMock.getCountry().getIsocode()).thenReturn(COUNTRY_ISO_NOT_US_CA);

		when(defaultShippingAddressMock.getCountry().getIsocode()).thenReturn(COUNTRY_ISO_NOT_US_CA);

		UserNode userNode = new UserNode();
		testPopulator.populate(ebpCreateUserPayloadMock, userNode);
		assertEquals(CUSTOMER_FIRST_NAME, userNode.getFirstName());
		assertEquals(CUSTOMER_LAST_NAME, userNode.getLastName());
		assertEquals(CUSTOMER_EMAIL, userNode.getEmailAddress());
		assertEquals(CUSTOMER_UID, userNode.getUserName());
		//check shipping address
		assertAddress(SHIPPING_COUNTRY_CODE, SHIPPING_ADDRESS_LINE_1, SHIPPING_ADDRESS_LINE_2, SHIPPING_TOWN,
				SHIPPING_POSTAL_CODE, SHIPPING_PHONE, userNode.getShippingAddress());

		//check billing address
		assertAddress(BILLING_COUNTRY_CODE, BILLING_ADDRESS_LINE_1, BILLING_ADDRESS_LINE_2, BILLING_TOWN,
				BILLING_POSTAL_CODE, BILLING_PHONE, userNode.getBillingAddress());
	}

	@Test
	public void testPopulateUSSpecific() throws Exception
	{
		when(orderMock.getDeliveryAddress()).thenReturn(defaultShippingAddressMock);
		when(orderMock.getPaymentAddress()).thenReturn(defaultBillingAddressMock);

		when(billingRegionMock.getIsocodeShort()).thenReturn(BILLING_REGION_CODE);
		when(defaultBillingAddressMock.getCountry().getIsocode()).thenReturn(COUNTRY_ISO_US);

		when(shippingRegionMock.getIsocodeShort()).thenReturn(SHIPPING_REGION_CODE);
		when(defaultShippingAddressMock.getCountry().getIsocode()).thenReturn(COUNTRY_ISO_US);

		UserNode userNode = new UserNode();
		testPopulator.populate(ebpCreateUserPayloadMock, userNode);
		//check shipping address
		assertNotNull(userNode.getShippingAddress());
		assertEquals(SHIPPING_COUNTRY_CODE, userNode.getShippingAddress().getCountry());
		assertEquals(SHIPPING_REGION_CODE, userNode.getShippingAddress().getState());
		assertNull(userNode.getShippingAddress().getOtherStateProvince());

		//check billing address
		assertNotNull(userNode.getBillingAddress());
		assertEquals(BILLING_COUNTRY_CODE, userNode.getBillingAddress().getCountry());
		assertEquals(BILLING_REGION_CODE, userNode.getBillingAddress().getState());
		assertNull(userNode.getBillingAddress().getOtherStateProvince());
	}

	@Test
	public void testPopulateCASpecific() throws Exception
	{
		when(orderMock.getDeliveryAddress()).thenReturn(defaultShippingAddressMock);
		when(orderMock.getPaymentAddress()).thenReturn(defaultBillingAddressMock);

		when(billingRegionMock.getIsocodeShort()).thenReturn(BILLING_REGION_CODE);
		when(defaultBillingAddressMock.getCountry().getIsocode()).thenReturn(COUNTRY_ISO_CA);

		when(shippingRegionMock.getIsocodeShort()).thenReturn(SHIPPING_REGION_CODE);
		when(defaultShippingAddressMock.getCountry().getIsocode()).thenReturn(COUNTRY_ISO_CA);

		UserNode userNode = new UserNode();
		testPopulator.populate(ebpCreateUserPayloadMock, userNode);

		//check shipping address
		assertNotNull(userNode.getShippingAddress());
		assertEquals(SHIPPING_COUNTRY_CODE, userNode.getShippingAddress().getCountry());
		assertEquals(SHIPPING_REGION_CODE, userNode.getShippingAddress().getOtherStateProvince());
		assertNull(userNode.getShippingAddress().getState());

		//check billing address
		assertNotNull(userNode.getBillingAddress());
		assertEquals(BILLING_COUNTRY_CODE, userNode.getBillingAddress().getCountry());
		assertEquals(BILLING_REGION_CODE, userNode.getBillingAddress().getOtherStateProvince());
		assertNull(userNode.getBillingAddress().getState());
	}



	@Test
	public void testPopulateCustomerWithNullOrder() throws Exception
	{
		when(ebpCreateUserPayloadMock.getOrder()).thenReturn(null);

		UserNode userNode = new UserNode();
		testPopulator.populate(ebpCreateUserPayloadMock, userNode);
		assertEquals(CUSTOMER_FIRST_NAME, userNode.getFirstName());
		assertEquals(CUSTOMER_LAST_NAME, userNode.getLastName());
		assertEquals(CUSTOMER_EMAIL, userNode.getEmailAddress());
		assertEquals(CUSTOMER_UID, userNode.getUserName());
		assertEquals(null, userNode.getBillingAddress());
		assertEquals(null, userNode.getShippingAddress());


	}

	@Test
	public void testPopulateOdlEmailAddress()
	{
		when(ebpCreateUserPayloadMock.getOldEmailAddress()).thenReturn("test@test.test");
		when(orderMock.getDeliveryAddress()).thenReturn(null);
		when(orderMock.getPaymentAddress()).thenReturn(null);
		UserNode userNode = new UserNode();
		testPopulator.populate(ebpCreateUserPayloadMock, userNode);

		assertEquals("test@test.test", userNode.getOldEmailAddress());
	}


	private void setUpAddress(final String countryCode, final String address1, final String address2, final String city,
			final String postalCode, final String phone, final AddressModel addressMock, final RegionModel regionMock)
	{
		when(addressMock.getRegion()).thenReturn(regionMock);
		when(addressMock.getCountry().getNumeric()).thenReturn(countryCode);
		when(addressMock.getLine1()).thenReturn(address1);
		when(addressMock.getLine2()).thenReturn(address2);
		when(addressMock.getTown()).thenReturn(city);
		when(addressMock.getPostalcode()).thenReturn(postalCode);
		when(addressMock.getPhone1()).thenReturn(phone);
	}

	private void assertAddress(final String countryCode, final String address1, final String address2, final String city,
			final String postalCode, final String phone, final AddressNode address)
	{
		assertNotNull(address);
		assertEquals(countryCode, address.getCountry());
		assertEquals(address1, address.getAddress1());
		assertEquals(address2, address.getAddress2());
		assertEquals(city, address.getCity());
		assertEquals(postalCode, address.getZipCode());
		assertEquals(phone, address.getPhoneNumber());
		assertNull(address.getState());
		assertNull(address.getOtherStateProvince());
	}
}
