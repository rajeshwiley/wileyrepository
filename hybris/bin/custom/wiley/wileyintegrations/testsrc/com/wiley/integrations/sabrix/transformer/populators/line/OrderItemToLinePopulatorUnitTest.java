package com.wiley.integrations.sabrix.transformer.populators.line;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.product.WileyProductEditionFormatService;
import com.wiley.integrations.sabrix.dto.IndataLineType;
import com.wiley.integrations.sabrix.dto.ZoneAddressType;
import com.wiley.integrations.sabrix.transformer.populators.AddressToZoneAddressTypePopulator;


/**
 * Created by Uladzimir_Barouski on 9/16/2016.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class OrderItemToLinePopulatorUnitTest
{
	@InjectMocks
	private OrderItemToLinePopulator orderItemToJournalLinePopulator;
	@Mock
	private AddressToZoneAddressTypePopulator addressToZoneAddressTypePopulatorMock;

	@Mock
	private WileyProductEditionFormatService wileyProductEditionFormatService;
	@Mock
	private AbstractOrderEntryModel orderEntryMock;
	@Mock
	private AbstractOrderModel orderMock;


	@Before
	public void setUp()
	{
		when(orderEntryMock.getOrder()).thenReturn(orderMock);
		doNothing().when(addressToZoneAddressTypePopulatorMock).populate(any(AddressModel.class), any(ZoneAddressType.class));
	}

	@Test
	public void testPopulatePhysicalEntryWhenCartHasDeliveryAddress() throws Exception
	{
		AddressModel shippingAddress = new AddressModel();
		AddressModel billingAddress = new AddressModel();
		when(orderMock.getDeliveryAddress()).thenReturn(shippingAddress);
		when(orderMock.getPaymentAddress()).thenReturn(billingAddress);
		when(wileyProductEditionFormatService.isDigitalProduct(orderEntryMock.getProduct())).thenReturn(false);

		orderItemToJournalLinePopulator.populate(orderEntryMock, new IndataLineType());

		verify(addressToZoneAddressTypePopulatorMock).populate(shippingAddress, new ZoneAddressType());
		verify(addressToZoneAddressTypePopulatorMock, never()).populate(billingAddress, new ZoneAddressType());

	}

	@Test
	public void testPopulatePhysicalEntryWhenCartNoDeliveryAddress() throws Exception
	{
		AddressModel shippingAddress = null;
		AddressModel billingAddress = new AddressModel();
		when(orderMock.getDeliveryAddress()).thenReturn(shippingAddress);
		when(orderMock.getPaymentAddress()).thenReturn(billingAddress);
		when(wileyProductEditionFormatService.isDigitalProduct(orderEntryMock.getProduct())).thenReturn(false);

		orderItemToJournalLinePopulator.populate(orderEntryMock, new IndataLineType());

		verify(addressToZoneAddressTypePopulatorMock, never()).populate(shippingAddress, new ZoneAddressType());
		verify(addressToZoneAddressTypePopulatorMock).populate(billingAddress, new ZoneAddressType());
	}

	@Test
	public void testPopulateDigitalEntry() throws Exception
	{
		AddressModel shippingAddress = new AddressModel();
		AddressModel billingAddress = new AddressModel();
		when(orderMock.getDeliveryAddress()).thenReturn(shippingAddress);
		when(orderMock.getPaymentAddress()).thenReturn(billingAddress);
		when(wileyProductEditionFormatService.isDigitalProduct(orderEntryMock.getProduct())).thenReturn(true);

		orderItemToJournalLinePopulator.populate(orderEntryMock, new IndataLineType());

		verify(addressToZoneAddressTypePopulatorMock, never()).populate(shippingAddress, new ZoneAddressType());
		verify(addressToZoneAddressTypePopulatorMock).populate(billingAddress, new ZoneAddressType());
	}
}