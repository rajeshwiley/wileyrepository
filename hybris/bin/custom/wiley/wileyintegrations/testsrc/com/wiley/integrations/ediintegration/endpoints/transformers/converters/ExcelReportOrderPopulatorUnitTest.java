package com.wiley.integrations.ediintegration.endpoints.transformers.converters;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.util.DiscountValue;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.enums.CreditCardTypeCode;
import com.wiley.core.integration.edi.dto.ExcelReportOrder;
import com.wiley.core.payment.transaction.PaymentTransactionService;
import com.wiley.fulfilmentprocess.model.WileyExportProcessModel;
import com.wiley.core.integration.edi.service.impl.WileyPurchaseOrderService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExcelReportOrderPopulatorUnitTest extends ConverterTestBase
{
	private static final String TEST_HYBRIS_AUTHORIZATION_CODE = "HybrisAuthorizationCode";
	private static final String TEST_CREDIT_CARD_TOKEN = "test_credit_card_owner";
	private static final String TEST_PRODUCT_ID = "ags";
	private static final String TEST_ORDER_CODE = "test_order_code";
	private static final String PO_NUMBER_FORMAT = "HB%s";
	private static final String TEST_CREDIT_CARD_OWNER = "test_credit_card_owner";
	private static final Double DELIVERY_COST = 10.13;
	private static final Double TAX_COST = 2.13;
	private static final Date TRANSACTION_DATE = Date.from(Instant.now());

	private static final double DISCOUNT_ENTRY_1 = 1.22d;
	private static final double DISCOUNT_1_ENTRY_2 = 1.33d;
	private static final double DISCOUNT_2_ENTRY_2 = 3.25d;
	private static final Double SUBTOTAL_WITHOUT_DISCOUNT = 12.13;
	private static final Double ORDER_LEVEL_DISCOUNTS = 1.13;
	private static final String TEST_ISO_CODE = "USD";
	private static final String TEST_ORDER_TYPE = "CC";
	private static final String CREDIT_CARD_TYPE = CreditCardTypeCode.AX.toString();
	private static final Double CAPTURE_AMOUNT =
			SUBTOTAL_WITHOUT_DISCOUNT + ORDER_LEVEL_DISCOUNTS + DISCOUNT_ENTRY_1 + DISCOUNT_2_ENTRY_2 + TAX_COST + DELIVERY_COST;
	private static final String ORDER_TYPE = "CC";


	@InjectMocks
	private ExcelReportOrderPopulator testInstance = new ExcelReportOrderPopulator();
	@Mock
	private PaymentTransactionService paymentTransactionService;
	@Mock
	private WileyPurchaseOrderService purchaseOrderService;
	@Mock
	private BaseSiteModel site;
	@Mock
	private CurrencyModel currencyModel;

	@Test
	public void testPopulate()
	{
		//Given
		OrderModel givenOrder = givenOrder();
		WileyExportProcessModel givenExportProcess = givenExportProcessWithOrder(givenOrder);
		//Testing
		ExcelReportOrder excelReportOrder = new ExcelReportOrder();
		when(purchaseOrderService.getSettleOrRefundTransactionAmount(givenExportProcess)).thenReturn(
				BigDecimal.valueOf(CAPTURE_AMOUNT));
		when(purchaseOrderService.getOrderType(givenExportProcess)).thenReturn(TEST_ORDER_TYPE);
		when(purchaseOrderService.isOrderRefunded(givenExportProcess)).thenReturn(false);

		testInstance.populate(givenExportProcess, excelReportOrder);
		//Verify

		assertEquals(DELIVERY_COST, excelReportOrder.getShipping());
		assertEquals(TAX_COST, excelReportOrder.getTax());
		assertEquals(SUBTOTAL_WITHOUT_DISCOUNT, excelReportOrder.getOrderAmount());
		assertEquals(CAPTURE_AMOUNT, excelReportOrder.getGrandTotal());
		assertEquals(ORDER_TYPE, excelReportOrder.getOrderType());
		assertEquals(TEST_HYBRIS_AUTHORIZATION_CODE, excelReportOrder.getAuthCode());
		assertEquals(TRANSACTION_DATE, excelReportOrder.getTransactionDate());

		assertEquals(TEST_CREDIT_CARD_OWNER, excelReportOrder.getCreditCardOwner());
		assertEquals(TEST_CREDIT_CARD_TOKEN, excelReportOrder.getCreditCardToken());
		assertEquals(TEST_ISO_CODE, excelReportOrder.getOrderCurrency());
		assertEquals(TEST_ORDER_TYPE, excelReportOrder.getOrderType());
		assertEquals(CREDIT_CARD_TYPE, excelReportOrder.getCreditCardType());
		assertEquals(TEST_PRODUCT_ID, excelReportOrder.getProductId());
		assertEquals(String.format(PO_NUMBER_FORMAT, TEST_ORDER_CODE), excelReportOrder.getPurchaseOrderNumber());
		final Double discountOrderAndProductLevel =
				ORDER_LEVEL_DISCOUNTS + DISCOUNT_ENTRY_1 + DISCOUNT_1_ENTRY_2 + DISCOUNT_2_ENTRY_2;
		assertEquals(discountOrderAndProductLevel, excelReportOrder.getTotalDiscounts());
	}

	private CreditCardPaymentInfoModel givenCreditCardPaymentInfoModel(final String ccOwner)
	{
		CreditCardPaymentInfoModel creditCardPaymentInfoModel = mock(CreditCardPaymentInfoModel.class);
		when(purchaseOrderService.getCreditCardType(creditCardPaymentInfoModel)).thenReturn(CREDIT_CARD_TYPE);
		when(creditCardPaymentInfoModel.getCcOwner()).thenReturn(ccOwner);
		when(creditCardPaymentInfoModel.getType()).thenReturn(CreditCardType.AMEX);
		return creditCardPaymentInfoModel;
	}

	public OrderModel givenOrder()
	{



		OrderModel orderModel = mock(OrderModel.class);
		when(purchaseOrderService.buildHybrisAuthorizationCode(orderModel)).thenReturn(TEST_HYBRIS_AUTHORIZATION_CODE);
		when(purchaseOrderService.getCCToken(orderModel)).thenReturn(TEST_CREDIT_CARD_TOKEN);
		when(orderModel.getCode()).thenReturn(TEST_ORDER_CODE);
		when(orderModel.getCurrency()).thenReturn(currencyModel);
		when(currencyModel.getIsocode()).thenReturn(TEST_ISO_CODE);
		when(orderModel.getDeliveryCost()).thenReturn(DELIVERY_COST);
		when(orderModel.getTotalTax()).thenReturn(TAX_COST);
		when(orderModel.getSubTotalWithoutDiscount()).thenReturn(SUBTOTAL_WITHOUT_DISCOUNT);
		when(orderModel.getTotalDiscounts()).thenReturn(ORDER_LEVEL_DISCOUNTS);
		final CreditCardPaymentInfoModel paymentInfo = givenCreditCardPaymentInfoModel(TEST_CREDIT_CARD_OWNER);
		when(orderModel.getPaymentInfo()).thenReturn(paymentInfo);
		when(orderModel.getSite()).thenReturn(site);
		when(site.getUid()).thenReturn(TEST_PRODUCT_ID);



		createEntries(orderModel);
		return orderModel;
	}

	private void createEntries(final OrderModel orderModel)
	{
		ExcelReportOrder excelReportOrder = new ExcelReportOrder();
		AbstractOrderEntryModel orderEntry1 = mock(AbstractOrderEntryModel.class);
		DiscountValue discountValue = mock(DiscountValue.class);
		when(discountValue.getAppliedValue()).thenReturn(DISCOUNT_ENTRY_1);
		when(orderEntry1.getDiscountValues()).thenReturn(Collections.singletonList(discountValue));

		DiscountValue discountValue2 = mock(DiscountValue.class);
		DiscountValue discountValue3 = mock(DiscountValue.class);
		List<DiscountValue> discountValueList = new ArrayList<DiscountValue>();
		discountValueList.add(discountValue2);
		discountValueList.add(discountValue3);
		when(discountValue2.getAppliedValue()).thenReturn(DISCOUNT_1_ENTRY_2);
		when(discountValue3.getAppliedValue()).thenReturn(DISCOUNT_2_ENTRY_2);

		AbstractOrderEntryModel orderEntry2 = mock(AbstractOrderEntryModel.class);
		when(orderEntry2.getDiscountValues()).thenReturn(discountValueList);
		List<AbstractOrderEntryModel> orderEntryList = new ArrayList<AbstractOrderEntryModel>();
		orderEntryList.add(orderEntry1);
		orderEntryList.add(orderEntry2);
		when(orderModel.getEntries()).thenReturn(orderEntryList);
	}

	private WileyExportProcessModel givenExportProcessWithOrder(final OrderModel orderModel)
	{
		WileyExportProcessModel wileyExportProcessModel = mock(WileyExportProcessModel.class);
		when(wileyExportProcessModel.getOrder()).thenReturn(orderModel);
		when(wileyExportProcessModel.getTransactionDate()).thenReturn(TRANSACTION_DATE);
		return wileyExportProcessModel;
	}

}