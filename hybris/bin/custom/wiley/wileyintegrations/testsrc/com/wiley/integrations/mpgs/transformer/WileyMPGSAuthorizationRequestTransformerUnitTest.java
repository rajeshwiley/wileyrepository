package com.wiley.integrations.mpgs.transformer;

import de.hybris.bootstrap.annotations.UnitTest;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.mpgs.constants.WileyMPGSConstants;
import com.wiley.core.mpgs.dto.authorization.MPGSAuthorizationRequestDTO;
import com.wiley.core.mpgs.request.WileyAuthorizationRequest;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyMPGSAuthorizationRequestTransformerUnitTest
{
	private static final String TEST_CURRENCY = "AUD";
	private static final String TEST_CITY = "New Jersey";
	private static final String TEST_COUNTRY = "USA";
	private static final String TEST_POSTAL_CODE = "07030";
	private static final String TEST_STATE = "US-NJ";
	private static final String TEST_STREET = "111 River St";
	private static final String TEST_TOTAL_AMOUNT = "2000.0";
	private static final String TEST_TOKEN = "TEST_TOKEN";

	@Mock
	private WileyAuthorizationRequest request;

	@InjectMocks
	private WileyMPGSAuthorizationRequestTransformer testTransformer;

	@Test
	public void transformSuccess() throws Exception
	{
		when(request.getApiOperation()).thenReturn(WileyMPGSConstants.API_OPERATION_AUTHORIZE);
		when(request.getCurrency()).thenReturn(TEST_CURRENCY);
		when(request.getBillingCity()).thenReturn(TEST_CITY);
		when(request.getBillingCountry()).thenReturn(TEST_COUNTRY);
		when(request.getBillingPostalCode()).thenReturn(TEST_POSTAL_CODE);
		when(request.getStateProvince()).thenReturn(TEST_STATE);
		when(request.getBillingStreet1()).thenReturn(TEST_STREET);
		when(request.getBillingStreet2()).thenReturn(TEST_STREET);
		when(request.getTotalAmount()).thenReturn(new BigDecimal(TEST_TOTAL_AMOUNT));
		when(request.getToken()).thenReturn(TEST_TOKEN);

		MPGSAuthorizationRequestDTO authorizeRequest = testTransformer.transform(request);

		assertEquals(WileyMPGSConstants.API_OPERATION_AUTHORIZE, authorizeRequest.getApiOperation());
		assertEquals(TEST_CURRENCY, authorizeRequest.getOrder().getCurrency());
		assertEquals(TEST_CITY, authorizeRequest.getBilling().getAddress().getCity());
		assertEquals(TEST_COUNTRY, authorizeRequest.getBilling().getAddress().getCountry());
		assertEquals(TEST_POSTAL_CODE, authorizeRequest.getBilling().getAddress().getPostcodeZip());
		assertEquals(TEST_STATE, authorizeRequest.getBilling().getAddress().getStateProvince());
		assertEquals(TEST_STREET, authorizeRequest.getBilling().getAddress().getStreet());
		assertEquals(TEST_STREET, authorizeRequest.getBilling().getAddress().getStreet2());
		assertEquals(TEST_TOTAL_AMOUNT, authorizeRequest.getOrder().getAmount());
		assertEquals(TEST_TOKEN, authorizeRequest.getSourceOfFunds().getToken());
	}

	@Test(expected = IllegalArgumentException.class)
	public void transformPassNull() throws Exception
	{
		MPGSAuthorizationRequestDTO authorizeRequest = testTransformer.transform(null);
	}
}
