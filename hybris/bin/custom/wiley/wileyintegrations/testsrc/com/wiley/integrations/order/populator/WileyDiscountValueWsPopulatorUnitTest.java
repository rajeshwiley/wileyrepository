package com.wiley.integrations.order.populator;


import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.promotionengineservices.model.AbstractRuleBasedPromotionActionModel;
import de.hybris.platform.promotionengineservices.promotionengine.report.dao.RuleBasedPromotionActionDao;
import de.hybris.platform.ruleengine.model.AbstractRuleEngineRuleModel;
import de.hybris.platform.ruleengineservices.model.AbstractRuleModel;
import de.hybris.platform.ruleengineservices.rule.services.RuleService;
import de.hybris.platform.util.DiscountValue;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.discount.WileyExternalDiscountService;
import com.wiley.core.model.WileyExternalDiscountModel;
import com.wiley.integrations.order.dto.DiscountValueWsDTO;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyDiscountValueWsPopulatorUnitTest
{
	private static final String CODE = "CODE";
	private static final String EXTERNAL_DISCOUNT_GUID = "12345-external-discount";
	private static final String PROMOTION_DISCOUNT_GUID = "12345-promotion-discount";

	private static final Double VALUE = 100.0d;
	private static final Boolean ABSOLUTE = true;
	private static final String CURRENCY = "CUR";
	private static final String PROMO_TYPE_GROUP = "promoTypeGroup";
	private static final String PROMO_INSTITUTION = "promoInstitution";
	private static final String RULE_CODE = "ruleCode";
	private static final Collection<String> COUPON_CODES = Arrays.asList("COUPON-1", "COUPON-2");

	@InjectMocks
	private WileyDiscountValueWsPopulator testInstance;


	@Mock
	private WileyExternalDiscountService wileyExternalDiscountServiceMock;

	@Mock
	private WileyExternalDiscountWsPopulator wileyExternalDiscountWsPopulatorMock;

	@Mock
	private RuleService ruleServiceMock;

	@Mock
	private RuleBasedPromotionActionDao ruleBasedPromotionActionDaoMock;


	@Mock
	private WileyExternalDiscountModel wileyExternalDiscountMock;

	@Mock
	private AbstractRuleBasedPromotionActionModel actionMock;

	@Mock
	private AbstractRuleEngineRuleModel abstractRuleEngineRuleMock;

	@Mock
	private AbstractRuleModel ruleMock;

	@Mock
	private DiscountValue discountValueMock;
	
	private DiscountValueWsDTO discountDto;

	@Before
	public void setUp()
	{
		discountDto = new DiscountValueWsDTO();

		when(discountValueMock.isAbsolute()).thenReturn(ABSOLUTE);
		when(discountValueMock.getCode()).thenReturn(CODE);
		when(discountValueMock.getValue()).thenReturn(VALUE);
		when(discountValueMock.getCurrencyIsoCode()).thenReturn(CURRENCY);

		when(wileyExternalDiscountServiceMock.isWileyExternalDiscountExistedForGuid(EXTERNAL_DISCOUNT_GUID)).thenReturn(true);
		when(wileyExternalDiscountServiceMock.getWileyExternalDiscountForGuid(EXTERNAL_DISCOUNT_GUID)).thenReturn(
				wileyExternalDiscountMock);

		when(ruleBasedPromotionActionDaoMock.findRuleBasedPromotionByGuid(PROMOTION_DISCOUNT_GUID)).thenReturn(actionMock);
		when(actionMock.getRule()).thenReturn(abstractRuleEngineRuleMock);
		when(actionMock.getUsedCouponCodes()).thenReturn(COUPON_CODES);
		
		when(abstractRuleEngineRuleMock.getCode()).thenReturn(RULE_CODE);
		when(ruleServiceMock.getRuleForCode(RULE_CODE)).thenReturn(ruleMock);
		
		when(ruleMock.getTypeGroup()).thenReturn(PROMO_TYPE_GROUP);
		when(ruleMock.getInstitution()).thenReturn(PROMO_INSTITUTION);
	}

	@Test
	public void shouldPopulateFromDiscountValue()
	{
		// when
		testInstance.populate(discountValueMock, discountDto);

		// then
		assertEquals(ABSOLUTE, discountDto.getAbsolute());
		assertEquals(CODE, discountDto.getCode());
		assertEquals(VALUE, discountDto.getValue());
		assertEquals(CURRENCY, discountDto.getCurrency());
	}

	@Test
	public void shouldPopulateFromWileyExternalDiscountIfItIsExisted()
	{
		// given
		when(discountValueMock.getCode()).thenReturn(EXTERNAL_DISCOUNT_GUID);

		// when
		testInstance.populate(discountValueMock, discountDto);

		// then
		verify(wileyExternalDiscountWsPopulatorMock).populate(wileyExternalDiscountMock, discountDto);
	}

	@Test
	public void shouldPopulateFromDiscountValueAndPromotionIfTheyAreExisted()
	{
		// given
		when(discountValueMock.getCode()).thenReturn(PROMOTION_DISCOUNT_GUID);

		// when
		testInstance.populate(discountValueMock, discountDto);

		// then
		assertEquals(ABSOLUTE, discountDto.getAbsolute());
		assertEquals(PROMOTION_DISCOUNT_GUID, discountDto.getCode());
		assertEquals(VALUE, discountDto.getValue());
		assertEquals(CURRENCY, discountDto.getCurrency());

		assertEquals(COUPON_CODES, discountDto.getCouponCodes());
		assertEquals(PROMO_TYPE_GROUP, discountDto.getPromoTypeGroup());
		assertEquals(PROMO_INSTITUTION, discountDto.getPromoInstitution());
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowExceptionWhenSourceIsNull()
	{
		testInstance.populate(null, discountDto);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowExceptionWhenTargetIsNull()
	{
		testInstance.populate(discountValueMock, null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowExceptionWhenSourceAndTargetAreNull()
	{
		testInstance.populate(null, null);
	}
}
