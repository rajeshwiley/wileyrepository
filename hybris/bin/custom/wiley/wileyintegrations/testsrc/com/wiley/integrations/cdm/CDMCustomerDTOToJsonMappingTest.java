/**
 *
 */
package com.wiley.integrations.cdm;

import de.hybris.bootstrap.annotations.UnitTest;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wiley.integrations.cdm.dto.CDMAddressDTO;
import com.wiley.integrations.cdm.dto.CDMCreateCustomerRequestEBMDTO;
import com.wiley.integrations.cdm.dto.CDMCustomerDTO;
import com.wiley.integrations.cdm.dto.CDMCustomerEBMDTO;
import com.wiley.integrations.cdm.dto.CDMCustomerIdentificationDTO;
import com.wiley.integrations.cdm.dto.CDMIndividualDTO;
import com.wiley.integrations.cdm.dto.CDMSourceXREFDTO;
import com.wiley.integrations.cdm.dto.CustomerBaseProfileDTO;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CDMCustomerDTOToJsonMappingTest {
  @Test
  public void testCustomerDTOToJson()
      throws JsonGenerationException, JsonMappingException, IOException, URISyntaxException {
    final ObjectMapper mapper = new ObjectMapper();

    final CDMCustomerDTO cdmCustomerDTO = createCDMCustomerDTO();

    final Date date = new Date();
    final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
    dateFormat.format(date);

    final String jsonString = mapper.writeValueAsString(cdmCustomerDTO);
    final String testContactJson = Files.lines(Paths.get(getClass()
        .getResource(
            "/wileyfulfilmentprocess/test/integration/CDMGatewayIntegrationTest/test_cdm_request_Hybris.json")
        .toURI())).collect(Collectors.joining());

    Assert.assertEquals(jsonString, testContactJson);
  }

  @Test
  public void testRequestCustomerDTOIndividualAndJson()
      throws JsonGenerationException, JsonMappingException, IOException, URISyntaxException {
    final ObjectMapper mapper = new ObjectMapper();

    final CDMCustomerDTO cdmCustomerDTO = new CDMCustomerDTO();

    final CDMCreateCustomerRequestEBMDTO createCustomerRequestEBM = new CDMCreateCustomerRequestEBMDTO();
    createCustomerRequestEBM.setActionCode("CREATE");

    final CDMCustomerEBMDTO customerEBM = new CDMCustomerEBMDTO();
    final CDMIndividualDTO individual = createIndividual();
    customerEBM.setIndividual(individual);
    createCustomerRequestEBM.setCustomerEBM(customerEBM);

    cdmCustomerDTO.setCreateCustomerRequestEBM(createCustomerRequestEBM);

    final String jsonString = mapper.writeValueAsString(cdmCustomerDTO);

    final String testContactJson = Files.lines(Paths.get(getClass()
        .getResource(
            "/wileyfulfilmentprocess/test/integration/CDMGatewayIntegrationTest/request_Individual_JSON.json")
        .toURI())).collect(Collectors.joining());
    Assert.assertEquals(jsonString, testContactJson);

  }

  @Test
  public void testRequestCustomerBaseProfileDTOJson()
      throws JsonGenerationException, JsonMappingException, IOException, URISyntaxException {
    final ObjectMapper mapper = new ObjectMapper();

    final CDMCustomerDTO cdmCustomerDTO = new CDMCustomerDTO();

    final CDMCreateCustomerRequestEBMDTO createCustomerRequestEBM = new CDMCreateCustomerRequestEBMDTO();
    createCustomerRequestEBM.setActionCode("CREATE");

    final CDMCustomerEBMDTO customerEBM = new CDMCustomerEBMDTO();

    final CustomerBaseProfileDTO baseProfileDTO = createCustomerBaseProfile();
    customerEBM.setCustomerBaseProfile(baseProfileDTO);

    createCustomerRequestEBM.setCustomerEBM(customerEBM);

    cdmCustomerDTO.setCreateCustomerRequestEBM(createCustomerRequestEBM);

    final String jsonString = mapper.writeValueAsString(cdmCustomerDTO);

    final String testContactJson = Files.lines(Paths.get(getClass()
        .getResource(
            "/wileyfulfilmentprocess/test/integration/CDMGatewayIntegrationTest/request_CustomerBaseProfile.json")
        .toURI())).collect(Collectors.joining());
    Assert.assertEquals(jsonString, testContactJson);

  }

  /**
   * @param cdmCustomerDTO
   */
  private void getAddressDetailsForRequest(final CDMCustomerDTO cdmCustomerDTO) {
    final CDMCreateCustomerRequestEBMDTO createCustomerRequestEBM = new CDMCreateCustomerRequestEBMDTO();
    createCustomerRequestEBM.setActionCode("CREATE");

    final CDMCustomerEBMDTO customerEBM = new CDMCustomerEBMDTO();


    final CDMAddressDTO shipmentAddressDTO = createShipmentAddress();
    final CDMAddressDTO paymentAddressDTO = createPaymentAddress();

    final ArrayList<CDMAddressDTO> addressList = new ArrayList<>();
    addressList.add(shipmentAddressDTO);
    addressList.add(paymentAddressDTO);
    customerEBM.setAddress(addressList);
    createCustomerRequestEBM.setCustomerEBM(customerEBM);

    cdmCustomerDTO.setCreateCustomerRequestEBM(createCustomerRequestEBM);
  }



  @Test
  public void testCustomerAddressDTOJson()
      throws JsonGenerationException, JsonMappingException, IOException, URISyntaxException {
    final ObjectMapper mapper = new ObjectMapper();

    final CDMCustomerDTO cdmCustomerDTO = new CDMCustomerDTO();
    getAddressDetailsForRequest(cdmCustomerDTO);

    final String jsonString = mapper.writeValueAsString(cdmCustomerDTO);

    final String testContactJson = Files.lines(Paths.get(getClass()
        .getResource(
            "/wileyfulfilmentprocess/test/integration/CDMGatewayIntegrationTest/request_Address_JSON.json")
        .toURI())).collect(Collectors.joining());
    Assert.assertEquals(jsonString, testContactJson);
  }


  private CDMCustomerDTO createCDMCustomerDTO() {
    final CDMCustomerDTO cdmCustomerDTO = new CDMCustomerDTO();

    final CDMCreateCustomerRequestEBMDTO createCustomerRequestEBM = createCustomerRequestEBMDTO();
    cdmCustomerDTO.setCreateCustomerRequestEBM(createCustomerRequestEBM);

    return cdmCustomerDTO;
  }


  private CDMCreateCustomerRequestEBMDTO createCustomerRequestEBMDTO() {
    final CDMCreateCustomerRequestEBMDTO createCustomerRequestEBM = new CDMCreateCustomerRequestEBMDTO();
    createCustomerRequestEBM.setActionCode("CREATE");

    final CDMCustomerEBMDTO customerEBM = createCustomerEBM();
    createCustomerRequestEBM.setCustomerEBM(customerEBM);

    return createCustomerRequestEBM;
  }

  private CDMCustomerEBMDTO createCustomerEBM() {
    final CDMCustomerEBMDTO customerEBM = new CDMCustomerEBMDTO();

    final CustomerBaseProfileDTO customerBaseProfile = createCustomerBaseProfile();
    customerEBM.setCustomerBaseProfile(customerBaseProfile);

    final CDMIndividualDTO individual = createIndividual();
    customerEBM.setIndividual(individual);

    final CDMAddressDTO shipmentAddressDTO = createShipmentAddress();
    final CDMAddressDTO paymentAddressDTO = createPaymentAddress();

    final ArrayList<CDMAddressDTO> addressList = new ArrayList<>();
    addressList.add(shipmentAddressDTO);
    addressList.add(paymentAddressDTO);
    customerEBM.setAddress(addressList);
    return customerEBM;
  }

  private CDMIndividualDTO createIndividual() {
    final CDMIndividualDTO individual = new CDMIndividualDTO();
    individual.setFullName("Gurpreet" + " " + "Singh");
    return individual;
  }

  private CustomerBaseProfileDTO createCustomerBaseProfile() {
    final CustomerBaseProfileDTO customerBaseProfile = new CustomerBaseProfileDTO();
    final CDMCustomerIdentificationDTO customerIdentification = new CDMCustomerIdentificationDTO();
    final CDMSourceXREFDTO sourceXREF = new CDMSourceXREFDTO();
    sourceXREF.setSourceCustomerID("asd854rewq531");
    customerIdentification.setSourceXREF(sourceXREF);
    customerBaseProfile.setCustomerIdentification(customerIdentification);
    customerBaseProfile.setCustomerTypeCode("I");
    return customerBaseProfile;
  }

  private CDMAddressDTO createPaymentAddress() {
    final CDMAddressDTO paymentAddressDTO = new CDMAddressDTO();
    paymentAddressDTO.setAddressLine1("Pawia street");
    paymentAddressDTO.setAddressLine2("Pawia street");
    paymentAddressDTO.setCountryCode("IND");
    paymentAddressDTO.setCountryName("India");
    paymentAddressDTO.setState("Jharkhand");
    paymentAddressDTO.setZipCode("831008");
    paymentAddressDTO.setFirstName("Gur");
    paymentAddressDTO.setLastName("Singh");
    paymentAddressDTO.setPhoneNumber("9008036644");
    return paymentAddressDTO;
  }

  private CDMAddressDTO createShipmentAddress() {
    final CDMAddressDTO shipmentAddressDTO = new CDMAddressDTO();
    shipmentAddressDTO.setAddressLine1("Pawia street");
    shipmentAddressDTO.setAddressLine2("Pawia street");
    shipmentAddressDTO.setCountryCode("PL");
    shipmentAddressDTO.setCountryName("Poland");
    shipmentAddressDTO.setState("KRK");
    shipmentAddressDTO.setZipCode("33-149");
    shipmentAddressDTO.setFirstName("Gurpreet");
    shipmentAddressDTO.setLastName("Singh");
    shipmentAddressDTO.setPhoneNumber("729395175");
    return shipmentAddressDTO;
  }

}
