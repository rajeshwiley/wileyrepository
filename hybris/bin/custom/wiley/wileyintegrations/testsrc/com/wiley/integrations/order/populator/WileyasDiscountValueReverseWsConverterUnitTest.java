package com.wiley.integrations.order.populator;

import com.wiley.integrations.order.dto.DiscountValueWsDTO;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.util.DiscountValue;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertTrue;

/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyasDiscountValueReverseWsConverterUnitTest
{
    private static final String TEST_CODE = "test code";
    private static final double TEST_VALUE = 133.33D;
    private static final String TEST_CURRENCY = "USD";

    final WileyasDiscountValueReverseWsConverter converter = new WileyasDiscountValueReverseWsConverter();

    @Test
    public void convertTest()
    {
        final DiscountValueWsDTO dto = new DiscountValueWsDTO();
        dto.setCode(TEST_CODE);
        dto.setValue(TEST_VALUE);
        dto.setCurrency(TEST_CURRENCY);
        dto.setAbsolute(Boolean.TRUE);

        final DiscountValue result = converter.convert(dto);

        Assert.assertNotNull(result);
        Assert.assertEquals(TEST_CODE, result.getCode());
        Assert.assertEquals(TEST_VALUE, result.getValue(), 0.0D);
        Assert.assertEquals(TEST_CURRENCY, result.getCurrencyIsoCode());
        assertTrue(result.isAbsolute());
    }
}