package com.wiley.integrations.esb.converters.populator.address;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.user.AddressModel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.integrations.esb.dto.EsbAddressCreateRequestDto;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


/**
 * Created by Georgii_Gavrysh on 7/19/2016.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AddressToAddressCreateRequestDtoPopulatorTest
{
	@InjectMocks
	AddressToAddressCreateRequestDtoPopulator testedPopulator;

	@Mock
	private AddressModel addressModelMock;

	@Mock
	private CountryModel countryModelMock;

	@Mock
	private RegionModel regionModelMock;

	@Mock
	private EsbAddressCreateRequestDto esbAddressCreateRequestDtoMock;

	@Before
	public void setUp()
	{
		when(addressModelMock.getCountry()).thenReturn(countryModelMock);
		when(addressModelMock.getRegion()).thenReturn(regionModelMock);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAddressModelNullError()
	{
		addressModelMock = null;
		testedPopulator.populate(addressModelMock, esbAddressCreateRequestDtoMock);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEsbAddressCreateRequestDtoNullError()
	{
		esbAddressCreateRequestDtoMock = null;
		testedPopulator.populate(addressModelMock, esbAddressCreateRequestDtoMock);
	}

	@Test
	public void testSuccess()
	{
		setupValidAddressModel();

		testedPopulator.populate(addressModelMock, esbAddressCreateRequestDtoMock);

		verify(esbAddressCreateRequestDtoMock).setPostcode("11111");
		verify(esbAddressCreateRequestDtoMock).setCountry("US");
		verify(esbAddressCreateRequestDtoMock).setState("US_AL");
		verify(esbAddressCreateRequestDtoMock).setCity("New York");
		verify(esbAddressCreateRequestDtoMock).setLine1("Some street");
		verify(esbAddressCreateRequestDtoMock).setLine2("Building 1");
		verify(esbAddressCreateRequestDtoMock).setFirstName("John");
		verify(esbAddressCreateRequestDtoMock).setLastName("Johnson");
		verify(esbAddressCreateRequestDtoMock).setPhoneNumber("1234567890");
	}

	@Test
	public void testRegionNull()
	{
		setupValidAddressModel();
		when(addressModelMock.getRegion()).thenReturn(null);

		testedPopulator.populate(addressModelMock, esbAddressCreateRequestDtoMock);

		assertThat(null, is(esbAddressCreateRequestDtoMock.getState()));
	}

	private void setupValidAddressModel()
	{
		when(addressModelMock.getPostalcode()).thenReturn("11111");
		when(countryModelMock.getIsocode()).thenReturn("US");
		when(regionModelMock.getIsocodeShort()).thenReturn("US_AL");
		when(addressModelMock.getTown()).thenReturn("New York");
		when(addressModelMock.getLine1()).thenReturn("Some street");
		when(addressModelMock.getLine2()).thenReturn("Building 1");
		when(addressModelMock.getFirstname()).thenReturn("John");
		when(addressModelMock.getLastname()).thenReturn("Johnson");
		when(addressModelMock.getPhone1()).thenReturn("1234567890");
	}

}
