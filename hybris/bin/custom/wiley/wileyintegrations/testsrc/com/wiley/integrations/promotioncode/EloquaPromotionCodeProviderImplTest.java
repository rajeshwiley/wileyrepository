package com.wiley.integrations.promotioncode;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.voucher.VoucherService;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.partner.WileyPartnerService;

import static org.junit.Assert.assertEquals;


/**
 * Unit test for {@link EloquaPromotionCodeProviderImpl}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class EloquaPromotionCodeProviderImplTest extends AbstractPromotionCodeProviderImplTest
{
	@InjectMocks
	private EloquaPromotionCodeProviderImpl promotionCodeProviderMock;
	@Mock
	protected VoucherService voucherServiceMock;
	@Mock
	private WileyPartnerService partnerServiceMock;

	
	
	protected PromotionCodeProvider getPromotionCodeProvider()
	{
		return promotionCodeProviderMock;
	}

	@Override
	@Test
	public void testStudentPromotionCode() throws Exception
	{
		// Given
		emulateNotVoucherFlow();
		emulateNotPartnerFlow();

		// When
		final Optional<String> promotionCode = promotionCodeProviderMock.getOrderPromotionCode(orderMock);

		// Then
		assertEquals("don't send student discount to eloqua", promotionCode, Optional.empty());
	}

	@Test
	public void testVoucherPromotionCodeForEloqua() throws Exception
	{
		super.testVoucherPromotionCode();
	}

	@Test
	public void testPartnerPromotionCodeForEloqua() throws Exception
	{
		super.testPartnerPromotionCode();
	}

	
	@Override
	public WileyPartnerService getPartnerServiceMock()
	{
		return partnerServiceMock;
	}

	@Override
	public VoucherService getVoucherServiceMock()
	{
		return voucherServiceMock;
	}
}