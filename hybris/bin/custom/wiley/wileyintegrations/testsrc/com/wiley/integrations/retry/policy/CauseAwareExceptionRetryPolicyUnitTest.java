package com.wiley.integrations.retry.policy;

import de.hybris.bootstrap.annotations.UnitTest;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.retry.RetryContext;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CauseAwareExceptionRetryPolicyUnitTest
{
	private Map<Class<? extends Throwable>, Boolean> retryableExceptions = new HashMap<>();

	private Map<Class<? extends Throwable>, Boolean> wrapperRetryableExceptions = new HashMap<>();

	@Mock
	private RetryContext contextMock;

	private CauseAwareExceptionRetryPolicy policy;

	@Test
	public void throwableNullTest()
	{
		when(contextMock.getLastThrowable()).thenReturn(null);
		when(contextMock.getRetryCount()).thenReturn(1);
		policy = createNewPolicyInstance();

		boolean result = policy.canRetry(contextMock);

		assertTrue(result);
	}

	@Test
	public void maxAttemptsTest()
	{
		when(contextMock.getLastThrowable()).thenReturn(null);
		when(contextMock.getRetryCount()).thenReturn(4);
		policy = createNewPolicyInstance();

		boolean result = policy.canRetry(contextMock);

		assertFalse(result);
	}

	@Test
	public void wrapperRetryableClassifierEmptyTest()
	{
		when(contextMock.getLastThrowable()).thenReturn(new Exception());
		when(contextMock.getRetryCount()).thenReturn(1);
		policy = createNewPolicyInstance();

		boolean result = policy.canRetry(contextMock);

		assertFalse(result);
	}

	@Test
	public void retryableClassifierEmptyTest()
	{
		when(contextMock.getLastThrowable()).thenReturn(new Exception());
		when(contextMock.getRetryCount()).thenReturn(1);
		wrapperRetryableExceptions.put(Exception.class, true);
		policy = createNewPolicyInstance();

		boolean result = policy.canRetry(contextMock);
		assertFalse(result);
	}

	@Test
	public void fullTest()
	{
		when(contextMock.getLastThrowable()).thenReturn(new Exception(new Exception()));
		when(contextMock.getRetryCount()).thenReturn(1);
		wrapperRetryableExceptions.put(Exception.class, true);
		retryableExceptions.put(Exception.class, true);
		policy = createNewPolicyInstance();

		boolean result = policy.canRetry(contextMock);
		assertTrue(result);
	}

	private CauseAwareExceptionRetryPolicy createNewPolicyInstance()
	{
		return new CauseAwareExceptionRetryPolicy(3, retryableExceptions, wrapperRetryableExceptions);
	}

}