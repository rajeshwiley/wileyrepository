package com.wiley.integrations.ebp.transformer;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.integration.ebp.dto.EbpAddProductsPayload;
import com.wiley.integrations.ebp.dto.AddProductsArgNode;
import com.wiley.integrations.ebp.dto.EbpAddProductsRootNode;
import com.wiley.integrations.ebp.dto.KPMGOrderInfo;
import com.wiley.integrations.ebp.dto.OrderNode;
import com.wiley.integrations.ebp.dto.ProductNode;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


/**
 * Created by Uladzimir_Barouski on 2/18/2016.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class OrderToEbpProductsTransformerUnitTest
{
	private static final String TEST_UID = "test UID";
	private static final String TEST_CUSTOMER_ID = "test Customer ID";

	@Mock
	private EbpAddProductsPayload ebpAddProductsPayload;
	@Mock
	private OrderModel orderMock;
	@Mock
	private CustomerModel customerMock;
	@Mock
	private ProductModel productMock;
	@Mock
	private KPMGOrderInfo kpmgOrderInfoMock;
	@Mock
	private Populator<EbpAddProductsPayload, List<ProductNode>> orderToEbpProductListPopulator;
	@Mock
	private Populator<EbpAddProductsPayload, AddProductsArgNode> orderToEbpKPMGFormPopulator;
	@Mock
	private Populator<EbpAddProductsPayload, OrderNode> orderToEbpOrderInfoPopulator;
	@InjectMocks
	private OrderToEbpProductsTransformer testTransformer;

	@Before
	public void setUp() throws Exception
	{
		UserModel user = new UserModel();
		when(orderMock.getUser()).thenReturn(customerMock);
		when(customerMock.getUid()).thenReturn(TEST_UID);
		when(customerMock.getCustomerID()).thenReturn(TEST_CUSTOMER_ID);
		when(ebpAddProductsPayload.getOrder()).thenReturn(orderMock);
		doNothing().when(orderToEbpProductListPopulator).populate(eq(ebpAddProductsPayload),
				Arrays.asList(any(ProductNode.class)));
		doNothing().when(orderToEbpOrderInfoPopulator).populate(eq(ebpAddProductsPayload), any(OrderNode.class));
		doNothing().when(orderToEbpKPMGFormPopulator).populate(ebpAddProductsPayload, new AddProductsArgNode());
	}

	@Test
	public void testTransform() throws Exception
	{
		EbpAddProductsRootNode requestMessage = testTransformer.transform(ebpAddProductsPayload);
		assertEquals(TEST_UID, requestMessage.getArgNode().getUserName());
		assertEquals(TEST_CUSTOMER_ID, requestMessage.getArgNode().getClientID());
		verify(orderToEbpProductListPopulator, times(1)).populate(eq(ebpAddProductsPayload),
				Arrays.asList(any(ProductNode.class)));
		verify(orderToEbpOrderInfoPopulator, times(1)).populate(eq(ebpAddProductsPayload), any(OrderNode.class));
		assertNull(requestMessage.getArgNode().getPassword());
	}
}
