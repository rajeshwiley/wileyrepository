package com.wiley.integrations.esb.transformer.lightningsource;

import de.hybris.bootstrap.annotations.UnitTest;
import static org.mockito.Mockito.when;

import org.hamcrest.MatcherAssert;
import org.hamcrest.core.Is;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.messaging.Message;

import com.wiley.core.exceptions.ExternalSystemException;
import com.wiley.integrations.lightningsource.LightningSourceResponseDto;


/**
 * Created by Georgii_Gavrysh on 9/8/2016.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class LightningSourceResponseDtoToStringTransformerUnitTest
{
	@InjectMocks
	private LightningSourceResponseDtoToStringTransformer testedTransformer;

	@Mock
	private Message<LightningSourceResponseDto> lightningSourceResponseDtoMessageMock;

	@Mock
	private LightningSourceResponseDto lightningSourceResponseDtoMock;


	@Before
	public void setUp()
	{
		when(lightningSourceResponseDtoMessageMock.getPayload()).thenReturn(lightningSourceResponseDtoMock);
	}

	@Test
	public void testSuccess()
	{
		when(lightningSourceResponseDtoMock.getIPWSeBookDownloadURL()).thenReturn("hhtp://link");
		when(lightningSourceResponseDtoMock.getErrorMessage()).thenReturn(null);

		final String link = testedTransformer.transform(lightningSourceResponseDtoMessageMock);

		MatcherAssert.assertThat(link, Is.is("hhtp://link"));
	}

	@Test(expected = ExternalSystemException.class)
	public void testErrorMessage()
	{
		when(lightningSourceResponseDtoMock.getIPWSeBookDownloadURL()).thenReturn("hhtp://link");
		when(lightningSourceResponseDtoMock.getErrorMessage()).thenReturn("Error");

		testedTransformer.transform(lightningSourceResponseDtoMessageMock);
	}

}
