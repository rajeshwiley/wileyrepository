package com.wiley.integrations.esb.converters.populator.orderentry;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.StockLevelStatus;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.ExternalInventoryModel;
import com.wiley.integrations.esb.dto.verifyandcalculatecart.InventoryStatus;
import com.wiley.integrations.esb.strategies.InventoryStatusToStockLevelStatusStrategy;
import com.wiley.integrations.esb.strategies.impl.InventoryStatusToStockLevelStatusStrategyImpl;

import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


/**
 * Default unit test for {@link InventoryStatusToExternalInventoryPopulator}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class InventoryStatusToExternalInventoryPopulatorUnitTest
{

	@InjectMocks
	private InventoryStatusToExternalInventoryPopulator inventoryStatusToExternalInventoryPopulator;

	// Test Data
	@Mock
	private InventoryStatus inventoryStatusMock;

	@Mock
	private ExternalInventoryModel externalInventoryModelMock;

	@Spy
	private InventoryStatusToStockLevelStatusStrategy inventoryStatusToStockLevelStatusStrategy
		= new InventoryStatusToStockLevelStatusStrategyImpl();

	@Test
	public void shouldPopulateExternalInventoryWithInStockStatus()
	{
		// Given
		final Date expectedDate = mock(Date.class);

		final InventoryStatus.StatusEnum status = InventoryStatus.StatusEnum.IN_STOCK;
		initInventoryStatusMock(
				5, // quantity
				status,
				expectedDate
		);

		// When
		inventoryStatusToExternalInventoryPopulator.populate(inventoryStatusMock, externalInventoryModelMock);

		// Then
		verify(externalInventoryModelMock).setQuantity(eq(5L));
		verify(externalInventoryModelMock).setStatus(eq(StockLevelStatus.INSTOCK));
		verify(externalInventoryModelMock).setEstimatedDeliveryDate(same(expectedDate));
	}

	@Test
	public void shouldPopulateExternalInventoryWithPreOrderStatus()
	{
		// Given
		final Date expectedDate = mock(Date.class);

		initInventoryStatusMock(
				25, // quantity
				InventoryStatus.StatusEnum.PRE_ORDER,
				expectedDate
		);

		// When
		inventoryStatusToExternalInventoryPopulator.populate(inventoryStatusMock, externalInventoryModelMock);

		// Then
		verify(externalInventoryModelMock).setQuantity(eq(25L));
		verify(externalInventoryModelMock).setStatus(eq(StockLevelStatus.PRE_ORDER));
		verify(externalInventoryModelMock).setEstimatedDeliveryDate(same(expectedDate));
	}

	@Test
	public void shouldPopulateExternalInventoryWithBackOrderStatus()
	{
		// Given
		final Date expectedDate = mock(Date.class);

		initInventoryStatusMock(
				1, // quantity
				InventoryStatus.StatusEnum.BACK_ORDER,
				expectedDate
		);

		// When
		inventoryStatusToExternalInventoryPopulator.populate(inventoryStatusMock, externalInventoryModelMock);

		// Then
		verify(externalInventoryModelMock).setQuantity(eq(1L));
		verify(externalInventoryModelMock).setStatus(eq(StockLevelStatus.BACK_ORDER));
		verify(externalInventoryModelMock).setEstimatedDeliveryDate(same(expectedDate));
	}

	@Test
	public void shouldPopulateExternalInventoryWithPODStatus()
	{
		// Given
		final Date expectedDate = mock(Date.class);

		initInventoryStatusMock(
				33, // quantity
				InventoryStatus.StatusEnum.PRINT_ON_DEMAND,
				expectedDate
		);

		// When
		inventoryStatusToExternalInventoryPopulator.populate(inventoryStatusMock, externalInventoryModelMock);

		// Then
		verify(externalInventoryModelMock).setQuantity(eq(33L));
		verify(externalInventoryModelMock).setStatus(eq(StockLevelStatus.PRINT_ON_DEMAND));
		verify(externalInventoryModelMock).setEstimatedDeliveryDate(same(expectedDate));
	}

	private void initInventoryStatusMock(final int expectedQuantity, final InventoryStatus.StatusEnum expectedStatus,
			final Date expectedDate)
	{
		when(inventoryStatusMock.getQuantity()).thenReturn(expectedQuantity);
		when(inventoryStatusMock.getStatus()).thenReturn(expectedStatus);
		when(inventoryStatusMock.getEstimatedDeliveryDate()).thenReturn(expectedDate);
	}

}