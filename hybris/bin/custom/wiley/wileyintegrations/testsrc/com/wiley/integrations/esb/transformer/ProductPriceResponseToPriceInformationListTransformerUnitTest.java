package com.wiley.integrations.esb.transformer;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.jalo.order.price.PriceInformation;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;

import com.wiley.integrations.esb.dto.EsbProductPriceResponseDto;

import static com.wiley.core.integration.esb.EsbCartCalculationGateway.BUSINESS_PRICE_COUNTRY;
import static com.wiley.core.integration.esb.EsbCartCalculationGateway.BUSINESS_PRICE_CURRENCY;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.number.IsCloseTo.closeTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;


/**
 * Created by Georgii_Gavrysh on 7/11/2016.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ProductPriceResponseToPriceInformationListTransformerUnitTest
{

	@InjectMocks
	private ProductPriceResponseToPriceInformationListTransformer testedTransformer;

	@Mock
	private Message<EsbProductPriceResponseDto[]> messageMock;

	@Mock
	private MessageHeaders messageHeadersMock;

	@Test
	public void testOneRecord()
	{
		final String sessionCurrency = "USD";
		final String sessionCountry = "US";
		when(messageMock.getHeaders()).thenReturn(messageHeadersMock);
		when(messageHeadersMock.get(BUSINESS_PRICE_CURRENCY)).thenReturn(sessionCurrency);
		when(messageHeadersMock.get(BUSINESS_PRICE_COUNTRY)).thenReturn(sessionCountry);
		when(messageMock.getPayload()).thenReturn(new EsbProductPriceResponseDto[]{createDto("USD", "US", 1.0)});
		final List<PriceInformation> transformResult = testedTransformer.transform(messageMock);

		assertThat(transformResult.size(), is(1));
		final PriceInformation priceInformation = transformResult.get(0);
		assertThat(priceInformation.getPriceValue().getCurrencyIso(), is("USD"));
		assertThat(priceInformation.getPriceValue().getValue(), closeTo(1.0, 0.001));
	}

	@Test
	public void testTwoRecords()
	{
		final String sessionCurrency = "EUR";
		final String sessionCountry = "UK";
		when(messageMock.getHeaders()).thenReturn(messageHeadersMock);
		when(messageHeadersMock.get(BUSINESS_PRICE_CURRENCY)).thenReturn(sessionCurrency);
		when(messageHeadersMock.get(BUSINESS_PRICE_COUNTRY)).thenReturn(sessionCountry);
		when(messageMock.getPayload()).thenReturn(new EsbProductPriceResponseDto[]{
				createDto("USD", "US", 1.0),
				createDto("EUR", "UK", 2.0)
		});
		final List<PriceInformation> transformResult = testedTransformer.transform(messageMock);

		assertThat(transformResult.size(), is(1));
		final PriceInformation priceInformation = transformResult.get(0);
		assertThat(priceInformation.getPriceValue().getCurrencyIso(), is("EUR"));
		assertThat(priceInformation.getPriceValue().getValue(), closeTo(2.0, 0.001));
	}

	@Test
	public void testNoSuchCurrency()
	{
		final String sessionCurrency = "NO_CURRENCY";
		final String sessionCountry = "UK";
		when(messageMock.getHeaders()).thenReturn(messageHeadersMock);
		when(messageHeadersMock.get(BUSINESS_PRICE_CURRENCY)).thenReturn(sessionCurrency);
		when(messageHeadersMock.get(BUSINESS_PRICE_COUNTRY)).thenReturn(sessionCountry);
		when(messageMock.getPayload()).thenReturn(new EsbProductPriceResponseDto[]{
				createDto("USD", "US", 1.0),
				createDto("EUR", "UK", 2.0),
				createDto("", "UK", 3.0),
		});
		final List<PriceInformation> transformResult = testedTransformer.transform(messageMock);

		assertThat(transformResult.size(), is(0));
	}

	@Test
	public void testNoCountryRecordEmptyCountry()
	{
		final String sessionCurrency = "USD";
		final String sessionCountry = "NO_COUNTRY";
		when(messageMock.getHeaders()).thenReturn(messageHeadersMock);
		when(messageHeadersMock.get(BUSINESS_PRICE_CURRENCY)).thenReturn(sessionCurrency);
		when(messageHeadersMock.get(BUSINESS_PRICE_COUNTRY)).thenReturn(sessionCountry);
		when(messageMock.getPayload()).thenReturn(new EsbProductPriceResponseDto[]{
				createDto("USD", "US", 1.0),
				createDto("EUR", "UK", 2.0),
				createDto("USD", "", 3.0),
		});
		final List<PriceInformation> transformResult = testedTransformer.transform(messageMock);

		assertThat(transformResult.size(), is(1));
		final PriceInformation priceInformation = transformResult.get(0);
		assertThat(priceInformation.getPriceValue().getCurrencyIso(), is("USD"));
		assertThat(priceInformation.getPriceValue().getValue(), closeTo(3.0, 0.001));
	}

	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testTwoReturnedRecordsError()
	{
		final String sessionCurrency = "USD";
		final String sessionCountry = "US";
		when(messageMock.getHeaders()).thenReturn(messageHeadersMock);
		when(messageHeadersMock.get(BUSINESS_PRICE_CURRENCY)).thenReturn(sessionCurrency);
		when(messageHeadersMock.get(BUSINESS_PRICE_COUNTRY)).thenReturn(sessionCountry);
		when(messageMock.getPayload()).thenReturn(new EsbProductPriceResponseDto[]{
				createDto("USD", "US", 1.0),
				createDto("USD", "US", 3.0),
		});
		testedTransformer.transform(messageMock);
	}

	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testNoCountryTwoRecordsError()
	{
		final String sessionCurrency = "USD";
		final String sessionCountry = "NO_COUNTRY";
		when(messageMock.getHeaders()).thenReturn(messageHeadersMock);
		when(messageHeadersMock.get(BUSINESS_PRICE_CURRENCY)).thenReturn(sessionCurrency);
		when(messageHeadersMock.get(BUSINESS_PRICE_COUNTRY)).thenReturn(sessionCountry);
		when(messageMock.getPayload()).thenReturn(new EsbProductPriceResponseDto[]{
				createDto("USD", "US", 1.0),
				createDto("EUR", "UK", 2.0),
				createDto("USD", "", 3.0),
				createDto("USD", "", 4.0),
		});
		testedTransformer.transform(messageMock);
	}

	private EsbProductPriceResponseDto createDto(final String currency, final String country, final Double price)
	{
		EsbProductPriceResponseDto result = new EsbProductPriceResponseDto();
		result.setCountry(country);
		result.setCurrency(currency);
		result.setPrice(price);
		return result;
	}
}
