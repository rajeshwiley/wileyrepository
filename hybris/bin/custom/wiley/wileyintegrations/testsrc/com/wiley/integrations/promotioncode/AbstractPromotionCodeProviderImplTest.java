package com.wiley.integrations.promotioncode;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.voucher.VoucherService;

import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

import org.mockito.Mock;

import com.wiley.core.partner.WileyPartnerService;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;


/**
 * Common logic for {@link PromotionCodeProvider} implementations unit testing.
 */
public abstract class AbstractPromotionCodeProviderImplTest
{
	private static final String PARTNER_DISCOUNT = "meaningfulPartnerDiscount";
	private static final String TEST_VOUCHER = "testVoucher";

	@Mock
	protected OrderModel orderMock;

	protected abstract WileyPartnerService getPartnerServiceMock();

	protected abstract VoucherService getVoucherServiceMock();

	protected abstract PromotionCodeProvider getPromotionCodeProvider();

	protected abstract void testStudentPromotionCode() throws Exception;

	protected void testVoucherPromotionCode() throws Exception
	{
		// Given
		emulateNotPartnerFlow();
		when(getVoucherServiceMock().getAppliedVoucherCodes(orderMock)).thenReturn(Arrays.asList(TEST_VOUCHER));

		// When
		final Optional<String> promotionCode = getPromotionCodeProvider().getOrderPromotionCode(orderMock);

		// Then
		assertEquals("voucher code should be sent", promotionCode.get(), "testVoucher");
	}

	protected void testPartnerPromotionCode() throws Exception
	{
		// Given
		emulateNotVoucherFlow();
		when(getPartnerServiceMock().getPartnerCode(orderMock)).thenReturn(Optional.of(PARTNER_DISCOUNT));

		// When
		final Optional<String> promotionCode = getPromotionCodeProvider().getOrderPromotionCode(orderMock);

		// Then
		assertEquals("partner discount code should be sent", promotionCode.get(), PARTNER_DISCOUNT);
	}

	protected void emulateNotVoucherFlow()
	{
		when(getVoucherServiceMock().getAppliedVoucherCodes(orderMock)).thenReturn(Collections.emptyList());
	}

	protected void emulateNotPartnerFlow()
	{
		when(getPartnerServiceMock().getPartnerCode(orderMock)).thenReturn(Optional.empty());
	}
}
