package com.wiley.integrations.mpgs.transformer;

import de.hybris.bootstrap.annotations.UnitTest;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.messaging.Message;

import com.wiley.core.mpgs.constants.WileyMPGSConstants;
import com.wiley.core.mpgs.dto.json.Session;
import com.wiley.core.mpgs.dto.retrievesession.MPGSRetrieveSessionResponseDTO;
import com.wiley.core.mpgs.response.WileyRetrieveSessionResponse;
import com.wiley.core.mpgs.services.WileyTransformationService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyMPGSRetrieveSessionResponseTransformerUnitTest
{

	private static final String SESSION_ID = "SESSION_ID";
	@Mock
	private Message<MPGSRetrieveSessionResponseDTO> message;
	@Mock
	private MPGSRetrieveSessionResponseDTO responseDto;
	@Mock
	private Session session;
	@Mock
	private WileyTransformationService wileyTransformationService;


	@InjectMocks
	@Spy
	private WileyMPGSRetrieveSessionResponseTransformer transformer = new WileyMPGSRetrieveSessionResponseTransformer();

	@Before
	public void setUp()
	{
		when(message.getPayload()).thenReturn(responseDto);
	}

	@Test
	public void successfulResult()
	{
		when(responseDto.getSession()).thenReturn(session);
		when(session.getId()).thenReturn(SESSION_ID);
		when(session.getUpdateStatus()).thenReturn(WileyMPGSConstants.RETURN_STATUS_SUCCESS);
		when(wileyTransformationService.transformStatusIfSuccessful(any())).thenReturn(WileyMPGSConstants.RETURN_STATUS_SUCCESS);

		WileyRetrieveSessionResponse wileyRetrieveSessionResponse = transformer.transform(message);

		assertEquals(WileyMPGSConstants.RETURN_STATUS_SUCCESS, wileyRetrieveSessionResponse.getStatus());
		assertEquals(SESSION_ID, wileyRetrieveSessionResponse.getSessionId());
	}

	@Test(expected = NullPointerException.class)
	public void nullPointerExceptionIfErrorResult()
	{
		WileyRetrieveSessionResponse wileyRetrieveSessionResponse = transformer.transform(message);
	}
}
