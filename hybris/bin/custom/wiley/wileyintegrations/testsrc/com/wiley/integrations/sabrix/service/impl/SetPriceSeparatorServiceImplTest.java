package com.wiley.integrations.sabrix.service.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.price.CommercePriceService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.util.PriceValue;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.integration.sabrix.service.impl.SetPriceSeparatorServiceImpl;

import static java.util.Arrays.asList;


/**
 * Author Herman_Chukhrai (EPAM)
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SetPriceSeparatorServiceImplTest
{

	@InjectMocks
	private SetPriceSeparatorServiceImpl setPriceSeparatorService = new SetPriceSeparatorServiceImpl();

	@Mock
	private ProductModel product1Mock;
	@Mock
	private ProductModel product2Mock;
	@Mock
	private ProductModel product3Mock;
	@Mock
	private CommercePriceService commercePriceServiceMock;
	private static final String CURRENCY_ISO = "USD";

	@Test
	public void testSplitSetPriceOver2Products() throws Exception
	{
		//given
		when(commercePriceServiceMock.getFromPriceForProduct(product1Mock)).thenReturn(createPrice(80d));
		when(commercePriceServiceMock.getFromPriceForProduct(product2Mock)).thenReturn(createPrice(60d));
		final Double setPrice = 70d;

		//when
		final Map<ProductModel, Double> productPriceMap = setPriceSeparatorService.splitSetPriceOverProducts(
				asList(product1Mock, product2Mock), setPrice);

		//then
		assertEquals(new Double(40d), productPriceMap.get(product1Mock));
		assertEquals(new Double(30d), productPriceMap.get(product2Mock));
		final Double pricesSum = productPriceMap.values().stream().reduce(Double::sum).get();
		assertEquals(setPrice, pricesSum);
	}

	@Test
	public void testSplitSetPriceOver3Products() throws Exception
	{
		//given
		when(commercePriceServiceMock.getFromPriceForProduct(product1Mock)).thenReturn(createPrice(80d));
		when(commercePriceServiceMock.getFromPriceForProduct(product2Mock)).thenReturn(createPrice(60d));
		when(commercePriceServiceMock.getFromPriceForProduct(product3Mock)).thenReturn(createPrice(40d));
		final Double setPrice = 90d;

		//when
		final Map<ProductModel, Double> productPriceMap = setPriceSeparatorService.splitSetPriceOverProducts(
				asList(product1Mock, product2Mock, product3Mock), setPrice);

		//then
		assertEquals(new Double(40d), productPriceMap.get(product1Mock));
		assertEquals(new Double(30d), productPriceMap.get(product2Mock));
		assertEquals(new Double(20d), productPriceMap.get(product3Mock));
		final Double pricesSum = productPriceMap.values().stream().reduce(Double::sum).get();
		assertEquals(setPrice, pricesSum);
	}

	@Test
	public void testFractionalSplit() throws Exception
	{
		//given
		when(commercePriceServiceMock.getFromPriceForProduct(product1Mock)).thenReturn(createPrice(80d));
		when(commercePriceServiceMock.getFromPriceForProduct(product2Mock)).thenReturn(createPrice(60d));
		final Double setPrice = 103d;

		//when
		final Map<ProductModel, Double> productPriceMap = setPriceSeparatorService.splitSetPriceOverProducts(
				asList(product1Mock, product2Mock), setPrice);

		//then
		assertEquals(58.86d, productPriceMap.get(product1Mock), 0.005d);
		assertEquals(44.14d, productPriceMap.get(product2Mock), 0.005d);
		final Double pricesSum = productPriceMap.values().stream().reduce(Double::sum).get();
		assertEquals(setPrice, pricesSum);
	}

	@Test
	public void testSplitOverZeroPrice() throws Exception
	{
		//given
		when(commercePriceServiceMock.getFromPriceForProduct(product1Mock)).thenReturn(createPrice(0d));
		when(commercePriceServiceMock.getFromPriceForProduct(product2Mock)).thenReturn(createPrice(60d));
		final Double setPrice = 60d;

		//when
		final Map<ProductModel, Double> productPriceMap = setPriceSeparatorService.splitSetPriceOverProducts(
				asList(product1Mock, product2Mock), setPrice);

		//then
		assertEquals(new Double(0), productPriceMap.get(product1Mock));
		assertEquals(new Double(60), productPriceMap.get(product2Mock));
		final Double pricesSum = productPriceMap.values().stream().reduce(Double::sum).get();
		assertEquals(setPrice, pricesSum);
	}

	@Test
	public void testSetPriceEqualsZero() throws Exception
	{
		//given
		when(commercePriceServiceMock.getFromPriceForProduct(product1Mock)).thenReturn(createPrice(0d));
		when(commercePriceServiceMock.getFromPriceForProduct(product2Mock)).thenReturn(createPrice(0d));
		final Double setPrice = 0d;

		//when
		final Map<ProductModel, Double> productPriceMap = setPriceSeparatorService.splitSetPriceOverProducts(
				asList(product1Mock, product2Mock), setPrice);

		//then
		assertEquals(new Double(0), productPriceMap.get(product1Mock));
		assertEquals(new Double(0), productPriceMap.get(product2Mock));
		final Double pricesSum = productPriceMap.values().stream().reduce(Double::sum).get();
		assertEquals(setPrice, pricesSum);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testNullProductsProvided() throws Exception
	{
		//given
		final Double setPrice = 0d;

		//when
		setPriceSeparatorService.splitSetPriceOverProducts(null, setPrice);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testNullSetPriceProvided() throws Exception
	{
		//given
		final Double setPrice = null;

		//when
		setPriceSeparatorService.splitSetPriceOverProducts(Collections.emptyList(), setPrice);
	}

	private static PriceInformation createPrice(final double price)
	{
		return new PriceInformation(new PriceValue(CURRENCY_ISO, price, true));
	}
}