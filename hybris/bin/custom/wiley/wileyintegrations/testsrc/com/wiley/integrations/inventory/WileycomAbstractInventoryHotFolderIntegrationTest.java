package com.wiley.integrations.inventory;

import de.hybris.platform.basecommerce.enums.InStockStatus;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.WarehouseService;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.ServicelayerTest;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.stock.StockService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.annotation.Resource;

import org.apache.commons.io.FileUtils;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;


/**
 * Created by Georgii Gavrysh on 7/21/2016.
 */
public abstract class WileycomAbstractInventoryHotFolderIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	public static final String PRODUCT_1 = "INVENTORY_TEST_PRODUCT_1";
	public static final String PRODUCT_2 = "INVENTORY_TEST_PRODUCT_2";
	public static final String PRODUCT_3 = "INVENTORY_TEST_PRODUCT_3";
	public static final String PRODUCT_4 = "INVENTORY_TEST_PRODUCT_4";
	public static final String WAREHOUSE_1 = "default";
	public static final String NON_EXISTING_WAREHOUSE = "nonExistingWarehouse";
	public static final String CATALOG_ID = "wileyProductCatalog";
	protected String destFileMask;
	protected String hotFolderPath;

	@Resource
	StockService stockService;

	@Resource
	ProductService productService;

	@Resource
	WarehouseService warehouseService;

	@Resource
	ModelService modelService;

	protected Long copyFileToHotFolder(final String path, final String warehouse) throws IOException
	{
		Long sequenceId = System.currentTimeMillis() / 1000L;
		InputStream inputStream = ServicelayerTest.class.getResourceAsStream(path);
		File dest = new File(String.format(destFileMask, warehouse, sequenceId));
		FileUtils.copyInputStreamToFile(inputStream, dest);
		return sequenceId;
	}

	protected void validateStockLevelUpdated(final String productCode, final WarehouseModel warehouseModel, final Long sequenceId)
	{
		ProductModel productModel = productService.getProductForCode(productCode);
		StockLevelModel stockLevel = stockService.getStockLevel(productModel, warehouseModel);
		assertNotNull(stockLevel);
		assertEquals(sequenceId, stockLevel.getSequenceId());
		assertEquals(111, stockLevel.getAvailable());
		assertEquals(true, stockLevel.getPrimary());
		assertEquals(InStockStatus.NOTSPECIFIED, stockLevel.getInStockStatus());
	}

	protected void validateStockLevelNotUpdated(final WarehouseModel warehouseModel)
	{
		validateStockLevelNotUpdated(PRODUCT_1, warehouseModel);
		validateStockLevelNotUpdated(PRODUCT_2, warehouseModel);
		validateStockLevelNotUpdated(PRODUCT_3, warehouseModel);
		validateStockLevelNotUpdated(PRODUCT_4, warehouseModel);
	}

	protected void validateStockLevelNotUpdated(final String productCode, final WarehouseModel warehouseModel)
	{
		ProductModel productModel = productService.getProductForCode(productCode);
		StockLevelModel stockLevel = stockService.getStockLevel(productModel, warehouseModel);
		assertNotNull(stockLevel);
		assertEquals(1000, stockLevel.getAvailable());
		assertEquals(InStockStatus.NOTSPECIFIED, stockLevel.getInStockStatus());
	}
}
