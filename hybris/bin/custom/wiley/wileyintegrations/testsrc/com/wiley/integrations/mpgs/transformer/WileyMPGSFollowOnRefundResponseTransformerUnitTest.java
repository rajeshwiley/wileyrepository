package com.wiley.integrations.mpgs.transformer;

import de.hybris.bootstrap.annotations.UnitTest;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.messaging.Message;

import com.wiley.core.mpgs.constants.WileyMPGSConstants;
import com.wiley.core.mpgs.dto.json.Response;
import com.wiley.core.mpgs.dto.json.Transaction;
import com.wiley.core.mpgs.dto.refund.MPGSFollowOnRefundResponseDTO;
import com.wiley.core.mpgs.response.WileyFollowOnRefundResponse;
import com.wiley.core.mpgs.services.WileyTransformationService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyMPGSFollowOnRefundResponseTransformerUnitTest
{
	public static final BigDecimal AMOUNT = BigDecimal.ONE;
	private static final String SUCCESS_RESULT = "SUCCESS";
	public static final String CURRENCY_CODE = "USD";
	public static final String GATEWAY_CODE = "GATEWAY_CODE";
	private static final String TRANSACTION_ID = "TRANSACTION_ID";

	@Mock
	private WileyTransformationService wileyTransformationService;
	@Mock
	private Message<MPGSFollowOnRefundResponseDTO> message;
	@Mock
	private MPGSFollowOnRefundResponseDTO responseDTO;
	@Mock
	private Transaction transaction;
	@Mock
	private Response response;

	@InjectMocks
	@Spy
	private WileyMPGSFollowOnRefundResponseTransformer transformer = new WileyMPGSFollowOnRefundResponseTransformer();

	@Before
	public void setUp()
	{
		when(message.getPayload()).thenReturn(responseDTO);
	}

	@Test
	public void successfulResult()
	{
		when(responseDTO.getResult()).thenReturn(SUCCESS_RESULT);
		when(wileyTransformationService.transformStatusIfSuccessful(any())).thenReturn(SUCCESS_RESULT);
		when(responseDTO.getTransaction()).thenReturn(transaction);
		when(transaction.getAmount()).thenReturn(AMOUNT);
		when(transaction.getCurrency()).thenReturn(CURRENCY_CODE);
		when(transaction.getId()).thenReturn(TRANSACTION_ID);
		when(responseDTO.getResponse()).thenReturn(response);
		when(response.getGatewayCode()).thenReturn(GATEWAY_CODE);

		WileyFollowOnRefundResponse response = transformer.transform(message);

		assertEquals(TRANSACTION_ID, response.getRequestId());
		assertEquals(AMOUNT, response.getTotalAmount());
		assertEquals(CURRENCY_CODE, response.getCurrency().getCurrencyCode());
		assertEquals(SUCCESS_RESULT, response.getStatus());
		assertEquals(GATEWAY_CODE, response.getStatusDetails());
	}

	@Test(expected = NullPointerException.class)
	public void nullPointerExceptionIfErrorResult()
	{
		when(responseDTO.getResult()).thenReturn(WileyMPGSConstants.RETURN_STATUS_ERROR);

		WileyFollowOnRefundResponse response = transformer.transform(message);
	}
}
