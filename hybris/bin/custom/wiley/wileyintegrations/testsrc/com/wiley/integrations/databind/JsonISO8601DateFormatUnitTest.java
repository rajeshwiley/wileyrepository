package com.wiley.integrations.databind;

import de.hybris.bootstrap.annotations.UnitTest;
import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.time.ZonedDateTime;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class JsonISO8601DateFormatUnitTest
{
	public static final String DATE_STRING = "2012-04-01T16:49:17Z";

	private JsonISO8601DateFormat testInstance;

	@Before
	public void beforeTest()
	{
		testInstance = new JsonISO8601DateFormat();
	}

	@Test
	public void testParseAndFormatDate() throws ParseException
	{
		ZonedDateTime zonedDate = ZonedDateTime.parse(DATE_STRING);
		Date date = Date.from(zonedDate.toInstant());

		assertEquals(DATE_STRING, testInstance.format(date));
		assertEquals(date, testInstance.parse(DATE_STRING));
	}
}