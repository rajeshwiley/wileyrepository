package com.wiley.integrations.inventory;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.acceleratorservices.dataimport.batch.BatchException;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.util.Config;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.io.File;
import java.io.IOException;

import javax.annotation.Resource;

import static org.fest.assertions.Assertions.assertThat;

import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandlingException;
import org.springframework.messaging.support.MessageBuilder;


/**
 * Created by Georgii Gavrysh on 7/21/2016.
 */
@IntegrationTest
public class WileycomDeltaInventoryHotFolderIntegrationTest extends WileycomAbstractInventoryHotFolderIntegrationTest
{

	@Resource(name = "batchFilesWileycomDeltaInventoryProc")
	MessageChannel xmlProcFile;

	@Before
	public void cleanUp() throws IOException
	{
		hotFolderPath = Config.getString("wiley.impex.basefolder", null) + "/junit/inventory/";
		destFileMask = hotFolderPath + "processing/delta_inventory-%s-%s.csv";
		FileUtils.cleanDirectory(new File(hotFolderPath));
	}

	@Test
	public void checkImportingOfFileWithNewInventory() throws Exception
	{
		importCsv("/wileycore/test/sampledata/wileycom/products-stocklevels.impex", DEFAULT_ENCODING);
		Long sequenceId = copyFileToHotFolder("/wileycore/test/inventory/delta/delta_inventory-default-1.csv", WAREHOUSE_1);
		WarehouseModel warehouseModel = warehouseService.getWarehouseForCode(WAREHOUSE_1);
		File file = new File(String.format(destFileMask, WAREHOUSE_1, sequenceId));
		xmlProcFile.send(MessageBuilder.withPayload(file).build());
		validateStockLevelUpdated(PRODUCT_1, warehouseModel, sequenceId);
		validateStockLevelUpdated(PRODUCT_2, warehouseModel, sequenceId);
	}

	@Test
	public void checkImportingOfFileWithNonExistingProduct() throws Exception
	{
		importCsv("/wileycore/test/sampledata/wileycom/products-stocklevels.impex", DEFAULT_ENCODING);
		Long sequenceId = copyFileToHotFolder("/wileycore/test/inventory/delta/delta_inventory-default-2.csv", WAREHOUSE_1);
		File file = new File(String.format(destFileMask, WAREHOUSE_1, sequenceId));
		xmlProcFile.send(MessageBuilder.withPayload(file).build());
		File archive = new File(hotFolderPath + "/archive");
		assertEquals(1, archive.list().length);
		WarehouseModel warehouseModel = warehouseService.getWarehouseForCode(WAREHOUSE_1);
		validateStockLevelNotUpdated(warehouseModel);
	}

	@Test
	public void checkImportingOfFileWithNonExistingWarehouse() throws Exception
	{
		Long sequenceId = copyFileToHotFolder("/wileycore/test/inventory/delta/delta_inventory-nonExistingWarehouse-3.csv",
				NON_EXISTING_WAREHOUSE);

		File file = new File(String.format(destFileMask, NON_EXISTING_WAREHOUSE, sequenceId));
		try
		{
			xmlProcFile.send(MessageBuilder.withPayload(file).build());
		}
		catch (MessageHandlingException e)
		{
			assertThat(e.getCause()).isInstanceOf(BatchException.class).hasMessage(
					String.format("WarehouseModel with code '%s' not found!", NON_EXISTING_WAREHOUSE));
		}

	}


	@Test
	public void checkImportingOfFileWithOldSequenceId() throws Exception
	{
		Long sequenceId = copyFileToHotFolder("/wileycore/test/inventory/delta/delta_inventory-default-1.csv", WAREHOUSE_1);

		//update warehose with newer secuenceId
		WarehouseModel warehouseModel = warehouseService.getWarehouseForCode(WAREHOUSE_1);
		warehouseModel.setSequenceId(sequenceId + 100);
		modelService.save(warehouseModel);

		File file = new File(String.format(destFileMask, WAREHOUSE_1, sequenceId));
		xmlProcFile.send(MessageBuilder.withPayload(file).build());
		File archive = new File(hotFolderPath + "/archive");
		File error = new File(hotFolderPath + "/error");
		assertNull(error.list());
		assertEquals(1, archive.list().length);
		validateStockLevelNotUpdated(warehouseModel);
	}
}
