package com.wiley.integrations.ediintegration.endpoints.generator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.store.BaseStoreModel;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.GenericMessage;

import static com.wiley.integrations.constants.WileyintegrationsConstants.BASE_STORE_KEY;
import static com.wiley.integrations.constants.WileyintegrationsConstants.EXPORT_TYPE_KEY;
import static com.wiley.integrations.constants.WileyintegrationsConstants.LOCAL_DATE_TIME;
import static com.wiley.integrations.constants.WileyintegrationsConstants.UNDERSCORE;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyExcelFileNameGeneratorTest
{

	private static final String EXTENSION = "xls";
	private static final String DATE_FORMAT = "MM-dd-yyyy-HH-mm-ss";
	public static final String SITE_SHORTCUT = "ags";
	public static final String FIRST_PART_FILE_NAME = "Hybris_Order_summary_for_";
	public static final String EXPORT_TYPE_PART_NAME = "shippable";

	private WileyExcelFileNameGenerator fileNameGenerator = new WileyExcelFileNameGenerator();

	@Mock
	private BaseStoreModel baseStore;

	@Before
	public void setUp()
	{

		when(baseStore.getUid()).thenReturn(SITE_SHORTCUT);
		fileNameGenerator.setDateFormat(DATE_FORMAT);
		fileNameGenerator.setExtension(EXTENSION);
	}

	@Test
	public void testBuildExcleFileName() throws Exception
	{
		//Given
		//07-05-2015:13:01:40
		WileyExcelFileNameGenerator fileNameGeneratorSpy = Mockito.spy(fileNameGenerator);
		Message<?> message = createMessage();
		Mockito.doReturn(FIRST_PART_FILE_NAME).when(fileNameGeneratorSpy)
				.getFirstPartFileName();
		final String expectedFileName = FIRST_PART_FILE_NAME
				+ SITE_SHORTCUT
				+ UNDERSCORE
				+ EXPORT_TYPE_PART_NAME
				+ UNDERSCORE
				+ "05-07-2015-13-01-40.xls";

		//Testing
		assertEquals(expectedFileName, fileNameGeneratorSpy.generateFileName(message));
	}

	private Message<?> createMessage()
	{
		Map<String, Object> map = new HashMap<>();
		map.put(BASE_STORE_KEY, baseStore);
		map.put(LOCAL_DATE_TIME, LocalDateTime.of(2015, Month.MAY, 7, 13, 01, 40));
		map.put(EXPORT_TYPE_KEY, EXPORT_TYPE_PART_NAME);
		return new GenericMessage("", map);
	}
}