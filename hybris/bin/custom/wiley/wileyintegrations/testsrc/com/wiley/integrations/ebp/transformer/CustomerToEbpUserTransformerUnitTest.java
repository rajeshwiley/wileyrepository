package com.wiley.integrations.ebp.transformer;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.user.CustomerModel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.integration.ebp.dto.EbpCreateUserPayload;
import com.wiley.integrations.ebp.dto.EbpCreateUserRootNode;
import com.wiley.integrations.ebp.dto.UserNode;
import com.wiley.integrations.ebp.populator.CustomerToEbpUserPopulator;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


/**
 * Created by Uladzimir_Barouski on 2/19/2016.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CustomerToEbpUserTransformerUnitTest
{

	private static final String TEST_CUSTOMER_ID = "test Customer ID";
	private static final String TEST_UNIVERSITY = "test university text";

	@Mock
	private CustomerModel customerMock;

	@Mock
	private CustomerToEbpUserPopulator customerToEbpUserPopulatorMock;
	@Mock
	private EbpCreateUserPayload ebpCreateUserPayload;
	@InjectMocks
	private CustomerToEbpUserTransformer testTransformer;

	@Before
	public void setUp() throws Exception
	{
		when(customerMock.getCustomerID()).thenReturn(TEST_CUSTOMER_ID);
//		when(ebpCreateUserPayload.getCustomer()).thenReturn(customerMock);
		doNothing().when(customerToEbpUserPopulatorMock).populate(eq(ebpCreateUserPayload), any(UserNode.class));
	}

	@Test
	public void testTransform() throws Exception
	{
		when(ebpCreateUserPayload.getCustomer()).thenReturn(customerMock);

		EbpCreateUserRootNode requestMessage = testTransformer.transform(ebpCreateUserPayload);
		assertEquals(TEST_CUSTOMER_ID, requestMessage.getArg().getClientID());
		verify(customerToEbpUserPopulatorMock, times(1)).populate(eq(ebpCreateUserPayload), any(UserNode.class));
	}
}