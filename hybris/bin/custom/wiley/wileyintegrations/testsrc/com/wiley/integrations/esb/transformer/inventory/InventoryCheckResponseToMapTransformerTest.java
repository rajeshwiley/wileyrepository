package com.wiley.integrations.esb.transformer.inventory;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.product.data.ExternalInventoryStatus;
import com.wiley.core.product.data.ExternalInventoryStatusRecord;
import com.wiley.integrations.esb.dto.verifyandcalculatecart.InventoryStatus;
import com.wiley.integrations.esb.strategies.InventoryStatusToStockLevelStatusStrategy;
import com.wiley.integrations.inventory.status.InventoryCheckResponse;
import com.wiley.integrations.inventory.status.InventoryCheckResponseEntry;



@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class InventoryCheckResponseToMapTransformerTest
{
	private static final String PRODUCT_1_SAP_CODE = "PRODUCT_1_SAP_CODE";

	@InjectMocks
	private InventoryCheckResponseToMapTransformer inventoryCheckResponseToMapTransformer;


	@Mock
	private InventoryStatusToStockLevelStatusStrategy inventoryStatusToStockLevelStatusStrategy;

	@Test
	public void successEstimatedDeliveryDate() throws Exception
	{
		// given
		final List<ExternalInventoryStatusRecord> inventoryStatusList = new ArrayList<>();

		final ExternalInventoryStatusRecord status = new ExternalInventoryStatusRecord();
		final ExternalInventoryStatusRecord status2 = new ExternalInventoryStatusRecord();
		final ExternalInventoryStatusRecord status3 = new ExternalInventoryStatusRecord();
		final ExternalInventoryStatusRecord status4 = new ExternalInventoryStatusRecord();
		status.setStatusCode(StockLevelStatus.INSTOCK);
		status.setAvailableDate(new Date(2017, 11, 17));
		status2.setStatusCode(StockLevelStatus.BACK_ORDER);
		status2.setAvailableDate(new Date(2017, 11, 18));
		status3.setStatusCode(StockLevelStatus.PRE_ORDER);
		status3.setAvailableDate(new Date(2017, 11, 16));
		status4.setStatusCode(StockLevelStatus.LOWSTOCK);

		inventoryStatusList.add(status);
		inventoryStatusList.add(status2);
		inventoryStatusList.add(status3);
		inventoryStatusList.add(status4);


		// when
		final Long dateDiff = inventoryCheckResponseToMapTransformer.getEstimatedDeliveryDate(inventoryStatusList);

		// then
		Assert.assertNotNull(dateDiff);
	}

	@Test
	public void entriesWithoutDatesEstimatedDeliveryDate() throws Exception
	{

		// given
		final List<ExternalInventoryStatusRecord> inventoryStatusList = new ArrayList<>();

		final ExternalInventoryStatusRecord status = new ExternalInventoryStatusRecord();
		final ExternalInventoryStatusRecord status2 = new ExternalInventoryStatusRecord();
		final ExternalInventoryStatusRecord status3 = new ExternalInventoryStatusRecord();
		final ExternalInventoryStatusRecord status4 = new ExternalInventoryStatusRecord();
		status.setStatusCode(StockLevelStatus.INSTOCK);
		status2.setStatusCode(StockLevelStatus.BACK_ORDER);
		status3.setStatusCode(StockLevelStatus.PRE_ORDER);
		status4.setStatusCode(StockLevelStatus.LOWSTOCK);
		inventoryStatusList.add(status);
		inventoryStatusList.add(status2);
		inventoryStatusList.add(status3);
		inventoryStatusList.add(status4);

		// when
		final Long dateDiff = inventoryCheckResponseToMapTransformer.getEstimatedDeliveryDate(inventoryStatusList);

		// then
		Assert.assertNull(dateDiff);
	}


	@Test
	public void testInventoryCheckResponseToMapTransformer()
	{
		//given
		final InventoryStatus inventoryStatus = new InventoryStatus();
		inventoryStatus.setEstimatedDeliveryDate(new Date(2017, 11, 17));
		inventoryStatus.setQuantity(5);
		inventoryStatus.setStatus(InventoryStatus.StatusEnum.IN_STOCK);

		when(inventoryStatusToStockLevelStatusStrategy.getExternalInventoryStatus(inventoryStatus.getStatus())).
				thenReturn(StockLevelStatus.INSTOCK);


		//when
		final ExternalInventoryStatusRecord externalInventoryStatusRecord = inventoryCheckResponseToMapTransformer.
				convertInventoryStatusToInventoryStatusRecord(inventoryStatus);


		//then
		assertEquals(inventoryStatus.getQuantity().longValue(), externalInventoryStatusRecord.getQuantity().longValue());
		assertEquals(inventoryStatus.getEstimatedDeliveryDate(), externalInventoryStatusRecord.getAvailableDate());
		verify(inventoryStatusToStockLevelStatusStrategy).getExternalInventoryStatus(inventoryStatus.getStatus());
	}

	@Test
	public void testConvertInventoryStatusToInventoryStatusRecord()
	{
		//given
		final InventoryCheckResponse inventoryCheckResponse = new InventoryCheckResponse();
		final List<InventoryCheckResponseEntry> inventoryCheckResponseEntries = new ArrayList<>();

		final InventoryCheckResponseEntry status = new InventoryCheckResponseEntry();
		final List<InventoryStatus> inventoryStatuses = new ArrayList<>();

		final InventoryStatus inventoryStatus = new InventoryStatus();
		inventoryStatus.setEstimatedDeliveryDate(new Date(2017, 11, 17));
		inventoryStatus.setQuantity(5);
		inventoryStatus.setStatus(InventoryStatus.StatusEnum.IN_STOCK);

		inventoryStatuses.add(inventoryStatus);
		status.setStatuses(inventoryStatuses);
		status.setSapProductCode(PRODUCT_1_SAP_CODE);

		inventoryCheckResponseEntries.add(status);

		inventoryCheckResponse.setEntries(inventoryCheckResponseEntries);

		when(inventoryStatusToStockLevelStatusStrategy.getExternalInventoryStatus(inventoryStatus.getStatus())).
				thenReturn(StockLevelStatus.INSTOCK);


		//when
		final Map<String, ExternalInventoryStatus> statusMap = inventoryCheckResponseToMapTransformer.
				transform(inventoryCheckResponse);


		//then
		assertNotNull(statusMap.get(PRODUCT_1_SAP_CODE));
		final ExternalInventoryStatusRecord externalInventoryStatusRecord = statusMap.get(PRODUCT_1_SAP_CODE).
				getRecordsList().get(0);
		assertEquals(inventoryStatus.getQuantity().longValue(), externalInventoryStatusRecord.getQuantity().longValue());
		assertEquals(inventoryStatus.getEstimatedDeliveryDate(), externalInventoryStatusRecord.getAvailableDate());
		verify(inventoryStatusToStockLevelStatusStrategy).getExternalInventoryStatus(inventoryStatus.getStatus());
	}
}
