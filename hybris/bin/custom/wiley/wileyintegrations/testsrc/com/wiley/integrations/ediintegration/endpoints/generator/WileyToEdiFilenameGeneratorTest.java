package com.wiley.integrations.ediintegration.endpoints.generator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.store.BaseStoreModel;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.GenericMessage;

import com.wiley.integrations.ediintegration.endpoints.jaxb.Routing;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyToEdiFilenameGeneratorTest
{
	private static final String ROUTING_HEADER_KEY = "routing";
	private static final String EXTENSION = "new";
	private static final String BODY_DATE_FORMAT = "YYYY-MM-dd-HH-mm-ss";
	private static final String PREFIX_DATE_FORMAT = "YYYYMMdd";

	private WileyToEdiFilenameGenerator testedInstance = new WileyToEdiFilenameGenerator();

	@Mock
	private BaseStoreModel baseStore;

	@Before
	public void setUp()
	{
		when(baseStore.getUid()).thenReturn("ags");
		testedInstance.setPrefixDateFormat(PREFIX_DATE_FORMAT);
		testedInstance.setBodyDateFormat(BODY_DATE_FORMAT);
		testedInstance.setExtension(EXTENSION);
	}

	@Test
	public void testBuildEdiFileName() throws Exception
	{
		//Given
		//07.05.1991:00.40
		Routing routing = new Routing(LocalDateTime.of(1991, Month.MAY, 7, 0, 40), baseStore, "", "CC"); //TODO Fix

		Message<?> message = createMessage(routing);

		final String expectedFileName = "19910507."
				+ routing.getTransactionBatchNumber()
				+ "." + routing.getSiteShortcut().toUpperCase()
				+ "_1991-05-07-00-40-00.new";
		//Testing
		assertEquals(expectedFileName, testedInstance.generateFileName(message));
	}

	//TODO: Test to check PP filename is needed

	private Message<?> createMessage(final Routing routing)
	{
		Map<String, Object> map = new HashMap<>();
		map.put(ROUTING_HEADER_KEY, routing);
		return new GenericMessage("", map);
	}
}