package com.wiley.integrations.mpgs.transformer;

import de.hybris.bootstrap.annotations.UnitTest;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.mpgs.dto.verify.MPGSVerifyRequestDTO;
import com.wiley.core.mpgs.request.WileyVerifyRequest;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyMPGSVerifyRequestTransformerUnitTest
{

	private static final String CURRENCY = "USD";
	private static final String SESSION_ID = "sessionID12345";
	private static final String TYPE = "CARD";
	private static final String API_OPERATION = "VERIFY";
	private static final String BILLING_CITY = "Boston";
	private static final String BILLING_COUNTRY = "USA";
	private static final String POSTAL_CODE = "1234567";
	private static final String BILLING_STATE = "BILLING_STATE";
	private static final String BILLING_STREET1 =  "Billing Street 1";
	private static final String BILLING_STREET2 =  "Billing Street 2";


	@Mock
	private WileyVerifyRequest request;

	@InjectMocks
	private WileyMPGSVerifyRequestTransformer testTransformer;

	@Test
	public void transformSuccess() throws Exception
	{
        // given
		when(request.getCurrency()).thenReturn(CURRENCY);
        when(request.getSessionId()).thenReturn(SESSION_ID);
        when(request.getSourceOfFundsType()).thenReturn(TYPE);
        when(request.getApiOperation()).thenReturn(API_OPERATION);
		when(request.getBillingCity()).thenReturn(BILLING_CITY);
		when(request.getBillingCountry()).thenReturn(BILLING_COUNTRY);
		when(request.getBillingPostalCode()).thenReturn(POSTAL_CODE);
		when(request.getStateProvince()).thenReturn(BILLING_STATE);
		when(request.getBillingStreet1()).thenReturn(BILLING_STREET1);
		when(request.getBillingStreet2()).thenReturn(BILLING_STREET2);

		// when
		MPGSVerifyRequestDTO verifyRequestDTO = testTransformer.transform(request);

		// then
		assertEquals(CURRENCY, verifyRequestDTO.getOrder().getCurrency());
		assertEquals(SESSION_ID, verifyRequestDTO.getSession().getId());
		assertEquals(TYPE, verifyRequestDTO.getSourceOfFunds().getType());
		assertEquals(API_OPERATION, verifyRequestDTO.getApiOperation());

		assertEquals(BILLING_CITY, verifyRequestDTO.getBilling().getAddress().getCity());
		assertEquals(BILLING_COUNTRY, verifyRequestDTO.getBilling().getAddress().getCountry());
		assertEquals(POSTAL_CODE, verifyRequestDTO.getBilling().getAddress().getPostcodeZip());
		assertEquals(BILLING_STATE, verifyRequestDTO.getBilling().getAddress().getStateProvince());
		assertEquals(BILLING_STREET1, verifyRequestDTO.getBilling().getAddress().getStreet());
		assertEquals(BILLING_STREET2, verifyRequestDTO.getBilling().getAddress().getStreet2());
	}


	@Test(expected = IllegalArgumentException.class)
	public void transformPassNull() throws Exception
	{
		MPGSVerifyRequestDTO verifyRequest = testTransformer.transform(null);
	}

}
