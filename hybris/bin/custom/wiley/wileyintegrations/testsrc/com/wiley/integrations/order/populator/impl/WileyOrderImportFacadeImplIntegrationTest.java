package com.wiley.integrations.order.populator.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.util.DiscountValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Before;
import org.junit.Test;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.order.service.WileyOrderService;
import com.wiley.integrations.order.dto.AddressWsDTO;
import com.wiley.integrations.order.dto.CreateOrderEntryRequestWsDTO;
import com.wiley.integrations.order.dto.CreateOrderRequestWsDTO;
import com.wiley.integrations.order.dto.DiscountValueWsDTO;
import com.wiley.integrations.order.dto.OrderWsDTO;
import com.wiley.integrations.order.dto.PriceValueWsDTO;
import com.wiley.integrations.order.dto.TaxValueWsDTO;
import com.wiley.integrations.order.importing.WileyOrderImportFacade;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
@IntegrationTest
public class WileyOrderImportFacadeImplIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	private static final String BASE_SITE_UID = "testSite";

	private static final String ID = "test id";
	private static final String TYPE = "GENERAL";
	private static final String COUNTRY = "US";
	private static final String USD_CURRENCY = "USD";
	private static final String EUR_CURRENCY = "EUR";
	private static final String USER_ID = "createOrderTestUser";
	private static final String PAYMENT_MODE = "card";
	private static final String EXTERNAL_TEXT = "external text";
	private static final String PURCHASE_ORDER_NUMBER = "purchase order number";
	private static final String USER_NOTES = "user notes";
	private static final Double SUBTOTAL = 228.9;
	private static final Double SUBTOTAL_WITHOUT_DISCOUNT = 235d;
	private static final Double TOTAL_DISCOUNTS = 28.74;
	private static final String TAX_NUMBER = "tax number";
	private static final Boolean TAX_NUMBER_VALIDATED = true;
	private static final Date TAX_NUMBER_EXPIRATION_DATE = new Date();
	private static final Double TOTAL_TAX = 20.5;
	private static final Boolean TAX_CALCULATED = true;
	private static final Double TOTAL_PRICE = 200.16;
	private static final Date DATE = new Date();
	private static final String SOURCE_SYSTEM = "source system";
	private static final String PLACED_BY_AGENT = "placedByAgent";

	private static final String FIRST_ENTRY_PREFIX = "first-";
	private static final String SECOND_ENTRY_PREFIX = "second-";
	private static final String ENTRY_ENTRY_ID = "entry id";
	private static final String ENTRY_STATUS = "CREATED";
	private static final String ENTRY_PRODUCT_CODE = "createOrderTestProduct";
	private static final String ENTRY_BUSINESS_ITEM_ID = "business item id";
	private static final String ENTRY_BUSINESS_KEY = "business key";
	private static final Integer ENTRY_QUANTITY = 1;
	private static final Integer ENTRY_QUANTITY_2 = 2;
	private static final Double ENTRY_TOTAL_PRICE = 80.7;
	private static final Double ENTRY_TOTAL_PRICE_2 = 148.2;
	private static final Double ENTRY_TAXABLE_TOTAL_PRICE = 80.7;
	private static final Double ENTRY_TAXABLE_TOTAL_PRICE_2 = 148.2;
	private static final Date ENTRY_DELIVERY_DATE = new Date();
	private static final String ENTRY_ADDITIONAL_INFO = "additional info";

	private static final Double DISCOUNT_VALUE = 1.9;
	private static final Double DISCOUNT_VALUE_2 = 2.3;
	private static final Double DISCOUNT_VALUE_3 = 6.5;
	private static final Double DISCOUNT_VALUE_4 = 10d;
	private static final String DISCOUNT_CODE = "discount code";
	private static final String DISCOUNT_PROMO_INSTITUTION = "promo institution";
	private static final String DISCOUNT_PROMO_TYPE_GROUP = "promo type group";
	private static final int DISCOUNT_VALUE_SIZE = 1;
	private static final int GLOBAL_DISCOUNT_VALUE_SIZE = 2;
	private static final Double APPLIED_VALUE = 6.5;
	private static final Double APPLIED_VALUE_2 = 22.24;

	private static final Double TAX_VALUE = 7.6;
	private static final Double TAX_VALUE_2 = 12.9;
	private static final String TAX_CODE = "tax code";

	private static final Double PRICE_VALUE = 83d;
	private static final Double PRICE_VALUE_2 = 152d;
	private static final Double ENTRY_BASE_PRICE = 83d;
	private static final Double ENTRY_BASE_PRICE_2 = 76d;

	private static final String PAYMENT_ADDRESS_PREFIX = "payment - ";
	private static final String CONTACT_ADDRESS_PREFIX = "contact - ";
	private static final String ADDRESS_POSTCODE = "address postcode";
	private static final String ADDRESS_CITY = "address city";
	private static final String ADDRESS_LINE1 = "address line1";
	private static final String ADDRESS_LINE2 = "address line2";
	private static final String ADDRESS_FIRST_NAME = "address first name";
	private static final String ADDRESS_LAST_NAME = "address last name";
	private static final String ADDRESS_PHONE_NUMBER = "address phone number";
	private static final String ADDRESS_EMAIL = "address email";
	private static final String ADDRESS_ORGANIZATION = "address organization";
	private static final String ADDRESS_DEPARTMENT = "address department";
	private static final String ADDRESS_STATE = "AZ";

	private static final String FIRST_CODE = "first code";
	private static final String SECOND_CODE = "second code";
	private static final String THIRD_CODE = "third code";

	@Resource
	private WileyOrderImportFacade wileyOrderImportFacade;

	@Resource
	private BaseSiteService baseSiteService;

	@Resource
	private WileyOrderService wileyOrderService;

	@Before
	public void setUp() throws Exception
	{
		importCsv("/wileyintegrations/test/WileyWsOrderFacadeImplIntegrationTest/create-order_test.impex",
				DEFAULT_ENCODING);
		baseSiteService.setCurrentBaseSite("testSite", true);
	}

	@Test
	public void createOrderTestWithFulfilment()
	{
		final Pair<OrderWsDTO, OrderProcessModel> result =
				wileyOrderImportFacade.importOrderAndCreateFulfilment(createOrderRequestWsDTO(), BASE_SITE_UID);

		final OrderWsDTO dto = result.getKey();
		final OrderProcessModel process = result.getValue();

		assertNotNull(dto);
		assertNotNull(process);

		final OrderModel orderModel = wileyOrderService
				.getOrderForGUID(ID, baseSiteService.getBaseSiteForUID(BASE_SITE_UID));

		assertNotNull(orderModel);

		assertDiscountValues(orderModel);
		assertTotals(orderModel);
	}

	@Test
	public void createOrderTestWithoutFulfilment()
	{
		final OrderWsDTO result =
				wileyOrderImportFacade.importOrder(createOrderRequestWsDTO(), BASE_SITE_UID);


		assertNotNull(result);

		final OrderModel orderModel = wileyOrderService
				.getOrderForGUID(ID, baseSiteService.getBaseSiteForUID(BASE_SITE_UID));

		assertNotNull(orderModel);

		assertDiscountValues(orderModel);
		assertTotals(orderModel);
	}

	private void assertTotals(final OrderModel orderModel)
	{
		assertEquals(TOTAL_DISCOUNTS, orderModel.getTotalDiscounts());
		assertEquals(SUBTOTAL, orderModel.getSubtotal());
		assertEquals(SUBTOTAL_WITHOUT_DISCOUNT, orderModel.getSubTotalWithoutDiscount());
		assertEquals(TOTAL_TAX, orderModel.getTotalTax());
		assertEquals(TOTAL_PRICE, orderModel.getTotalPrice());
		assertEquals(ENTRY_BASE_PRICE_2, orderModel.getEntries().get(0).getBasePrice());
		assertEquals(ENTRY_BASE_PRICE, orderModel.getEntries().get(1).getBasePrice());
	}

	private void assertDiscountValues(final OrderModel orderModel)
	{
		final List<DiscountValue> globalDiscounts = orderModel.getGlobalDiscountValues();
		final List<AbstractOrderEntryModel> entries = orderModel.getEntries();
		assertEquals(GLOBAL_DISCOUNT_VALUE_SIZE, globalDiscounts.size());
		assertEquals(APPLIED_VALUE, Double.valueOf(globalDiscounts.get(0).getAppliedValue()));
		assertEquals(APPLIED_VALUE_2, Double.valueOf(globalDiscounts.get(1).getAppliedValue()));
		assertEquals(DISCOUNT_VALUE_SIZE, entries.get(0).getDiscountValues().size());
		assertEquals(DISCOUNT_VALUE_SIZE, entries.get(1).getDiscountValues().size());
	}

	private CreateOrderRequestWsDTO createOrderRequestWsDTO()
	{
		final CreateOrderRequestWsDTO dto = new CreateOrderRequestWsDTO();
		dto.setId(ID);
		dto.setType(TYPE);
		dto.setCountry(COUNTRY);
		dto.setCurrency(USD_CURRENCY);
		dto.setEntries(createOrderEntryRequestWsDTOs());
		dto.setUserId(USER_ID);
		dto.setPaymentMode(PAYMENT_MODE);
		dto.setPaymentAddress(createPaymentAddress());
		dto.setContactAddress(createContactAddress());
		dto.setExternalText(EXTERNAL_TEXT);
		dto.setExternalText(EXTERNAL_TEXT);
		dto.setPurchaseOrderNumber(PURCHASE_ORDER_NUMBER);
		dto.setUserNotes(USER_NOTES);
		dto.setSubtotal(SUBTOTAL);
		final List<DiscountValueWsDTO> externalDiscounts = new ArrayList<>();
		externalDiscounts.add(createExternalDiscount(true, DISCOUNT_CODE, USD_CURRENCY, DISCOUNT_VALUE_3));
		externalDiscounts.add(createExternalDiscount(true, DISCOUNT_CODE, EUR_CURRENCY, DISCOUNT_VALUE_2));
		externalDiscounts.add(createExternalDiscount(false, DISCOUNT_CODE, null, DISCOUNT_VALUE_4));
		dto.setExternalDiscounts(externalDiscounts);
		dto.setTotalDiscounts(TOTAL_DISCOUNTS);
		dto.setTaxNumber(TAX_NUMBER);
		dto.setTaxNumberValidated(TAX_NUMBER_VALIDATED);
		dto.setTaxNumberExpirationDate(TAX_NUMBER_EXPIRATION_DATE);
		dto.setTotalTax(TOTAL_TAX);
		dto.setTaxCalculated(TAX_CALCULATED);
		dto.setTotalPrice(TOTAL_PRICE);
		dto.setAppliedCouponCodes(createAppliedCouponCodes());
		dto.setDate(DATE);
		dto.setSourceSystem(SOURCE_SYSTEM);
		dto.setPlacedByAgent(PLACED_BY_AGENT);
		return dto;
	}

	private List<String> createAppliedCouponCodes()
	{
		final List<String> appliedCouponCodes = new ArrayList<>(3);
		appliedCouponCodes.add(FIRST_CODE);
		appliedCouponCodes.add(SECOND_CODE);
		appliedCouponCodes.add(THIRD_CODE);
		return appliedCouponCodes;
	}

	private DiscountValueWsDTO createExternalDiscount(boolean absolute, final String code, final String currency,
			final Double value)
	{
		final DiscountValueWsDTO discount = new DiscountValueWsDTO();

		discount.setAbsolute(absolute);
		discount.setCode(code);
		discount.setCurrency(currency);
		discount.setValue(value);
		discount.setPromoInstitution(DISCOUNT_PROMO_INSTITUTION);
		discount.setPromoTypeGroup(DISCOUNT_PROMO_TYPE_GROUP);

		return discount;
	}

	private AddressWsDTO createContactAddress()
	{
		return createAddress(CONTACT_ADDRESS_PREFIX);
	}

	private AddressWsDTO createPaymentAddress()
	{
		return createAddress(PAYMENT_ADDRESS_PREFIX);
	}

	private AddressWsDTO createAddress(final String prefix)
	{
		final AddressWsDTO paymentAddress = new AddressWsDTO();
		paymentAddress.setPostcode(prefix + ADDRESS_POSTCODE);
		paymentAddress.setCity(prefix + ADDRESS_CITY);
		paymentAddress.setLine1(prefix + ADDRESS_LINE1);
		paymentAddress.setLine2(prefix + ADDRESS_LINE2);
		paymentAddress.setFirstName(prefix + ADDRESS_FIRST_NAME);
		paymentAddress.setLastName(prefix + ADDRESS_LAST_NAME);
		paymentAddress.setPhoneNumber(prefix + ADDRESS_PHONE_NUMBER);
		paymentAddress.setEmail(prefix + ADDRESS_EMAIL);
		paymentAddress.setOrganization(prefix + ADDRESS_ORGANIZATION);
		paymentAddress.setDepartment(prefix + ADDRESS_DEPARTMENT);
		paymentAddress.setCountry(COUNTRY);
		paymentAddress.setState(ADDRESS_STATE);

		return paymentAddress;
	}

	private List<CreateOrderEntryRequestWsDTO> createOrderEntryRequestWsDTOs()
	{
		final List<CreateOrderEntryRequestWsDTO> entries = new ArrayList<>();

		CreateOrderEntryRequestWsDTO entry = createOrderEntry(FIRST_ENTRY_PREFIX);
		entry.setQuantity(ENTRY_QUANTITY_2);
		entry.getExternalPrices().get(0).setValue(PRICE_VALUE_2);
		entry.setTotalPrice(ENTRY_TOTAL_PRICE_2);
		entry.setTaxableTotalPrice(ENTRY_TAXABLE_TOTAL_PRICE_2);
		final List<DiscountValueWsDTO> externalDiscounts = new ArrayList<>();
		externalDiscounts.add(createExternalDiscount(true, DISCOUNT_CODE, USD_CURRENCY, DISCOUNT_VALUE));
		externalDiscounts.add(createExternalDiscount(true, DISCOUNT_CODE, EUR_CURRENCY, DISCOUNT_VALUE_2));
		entry.setExternalDiscounts(externalDiscounts);
		final List<TaxValueWsDTO> taxes = new ArrayList<>();
		taxes.add(createTax(TAX_VALUE_2));
		entry.setTaxes(taxes);

		CreateOrderEntryRequestWsDTO entry2 = createOrderEntry(SECOND_ENTRY_PREFIX);
		entry2.setQuantity(ENTRY_QUANTITY);
		entry2.setTotalPrice(ENTRY_TOTAL_PRICE);
		entry2.setTaxableTotalPrice(ENTRY_TAXABLE_TOTAL_PRICE);
		final List<DiscountValueWsDTO> externalDiscounts2 = new ArrayList<>();
		externalDiscounts2.add(createExternalDiscount(true, DISCOUNT_CODE, USD_CURRENCY, DISCOUNT_VALUE_3));
		externalDiscounts2.add(createExternalDiscount(true, DISCOUNT_CODE, EUR_CURRENCY, DISCOUNT_VALUE_2));
		entry2.setExternalDiscounts(externalDiscounts2);
		final List<TaxValueWsDTO> taxes2 = new ArrayList<>();
		taxes2.add(createTax(TAX_VALUE));
		entry2.setTaxes(taxes2);

		entries.add(entry);
		entries.add(entry2);
		return entries;
	}

	private CreateOrderEntryRequestWsDTO createOrderEntry(final String prefix)
	{
		final CreateOrderEntryRequestWsDTO entry = new CreateOrderEntryRequestWsDTO();
		entry.setEntryId(prefix + ENTRY_ENTRY_ID);
		entry.setStatus(ENTRY_STATUS);
		entry.setProductCode(prefix + ENTRY_PRODUCT_CODE);
		entry.setBusinessItemId(prefix + ENTRY_BUSINESS_ITEM_ID);
		entry.setBusinessKey(prefix + ENTRY_BUSINESS_KEY);
		entry.setQuantity(ENTRY_QUANTITY);
		entry.setExternalPrices(createExternalPrices(PRICE_VALUE));
		entry.setDeliveryDate(ENTRY_DELIVERY_DATE);
		entry.setAdditionalInfo(prefix + ENTRY_ADDITIONAL_INFO);
		return entry;
	}

	private TaxValueWsDTO createTax(final Double value)
	{
		final TaxValueWsDTO taxDto = new TaxValueWsDTO();

		taxDto.setCurrency(USD_CURRENCY);
		taxDto.setValue(value);
		taxDto.setCode(TAX_CODE);

		return taxDto;
	}

	private List<PriceValueWsDTO> createExternalPrices(final Double priceValue)
	{
		final List<PriceValueWsDTO> priceValues = new ArrayList<>();
		final PriceValueWsDTO priceDto = new PriceValueWsDTO();
		priceValues.add(priceDto);

		priceDto.setCurrency(USD_CURRENCY);
		priceDto.setValue(priceValue);

		return priceValues;
	}
}