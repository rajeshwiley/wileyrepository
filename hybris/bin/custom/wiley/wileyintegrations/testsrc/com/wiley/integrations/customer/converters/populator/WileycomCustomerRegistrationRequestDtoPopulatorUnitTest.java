package com.wiley.integrations.customer.converters.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BUnitService;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.servicelayer.user.UserService;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

import com.wiley.core.model.NameSuffixModel;
import com.wiley.core.model.SchoolModel;
import com.wiley.integrations.customer.dto.WileycomCustomerRegistrationRequestDto;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileycomCustomerRegistrationRequestDtoPopulatorUnitTest
{
	private static final String IMMUTABLE_ID = "immutable-id";
	private static final String USER_ID = "user-id";
	private static final String EMAIL = "email";

	private static final String FIRST_NAME = "first-name";
	private static final String LAST_NAME = "last-name";
	private static final String MIDDLE_NAME = "middle-name";
	private static final String TITLE = "title";
	private static final String SUFFIX = "suffix";

	private static final String STUDENT_ID = "student-id";
	private static final String MAJOR = "major";
	private static final String SCHOOL_ID = "school-id";
	private static final String GRADUATION_YEAR = "2000";
	private static final String GRADUATION_MONTH = "6";

	private static final String SAP_NUMBER = "sap-number";
	private static final String ECID_NUMBER = "ecid-number";

	private static final String B2B_ADMIN_GROUP_UID = "b2b-admin-group-uid";
	private static final String B2B_CUSTOMER_GROUP_UID = "b2b-customer-group-uid";

	@InjectMocks
	private WileycomCustomerRegistrationRequestDtoPopulator wileycomCustomerRegistrationRequestDtoPopulator;

	@Mock
	private CustomerModel customerModelMock;

	@Mock
	private B2BCustomerModel b2BCustomerModelMock;

	@Mock
	private B2BUnitModel b2BUnitModelMock;

	@Mock
	private B2BUnitService<B2BUnitModel, B2BCustomerModel> b2bUnitServiceMock;

	@Mock
	private TitleModel titleModelMock;

	@Mock
	private NameSuffixModel nameSuffixModelMock;

	@Mock
	private SchoolModel schoolModelMock;

	@Mock
	private UserService userServiceMock;

	@Mock
	private UserGroupModel b2bAdminGroupModelMock;

	@Mock
	private UserGroupModel b2bCustomerGroupModelMock;

	@Before
	public void setup()
	{
		when(b2bAdminGroupModelMock.getUid()).thenReturn(B2B_ADMIN_GROUP_UID);
		when(b2bCustomerGroupModelMock.getUid()).thenReturn(B2B_CUSTOMER_GROUP_UID);
		when(b2bUnitServiceMock.getParent(eq(b2BCustomerModelMock))).thenReturn(b2BUnitModelMock);
		when(userServiceMock.getUserGroupForUID(eq("b2badmingroup"))).thenReturn(b2bAdminGroupModelMock);
		when(userServiceMock.getUserGroupForUID(eq("b2bcustomergroup"))).thenReturn(b2bCustomerGroupModelMock);
	}

	@Test
	public void shouldPopulatePrincipalAttributes()
	{
		// Given
		when(customerModelMock.getCustomerID()).thenReturn(IMMUTABLE_ID);
		when(customerModelMock.getUid()).thenReturn(USER_ID);
		when(customerModelMock.getContactEmail()).thenReturn(EMAIL);

		// When
		WileycomCustomerRegistrationRequestDto requestDto = new WileycomCustomerRegistrationRequestDto();
		wileycomCustomerRegistrationRequestDtoPopulator.populate(customerModelMock, requestDto);

		// Then
		Assert.assertEquals("Immutable ID field should have been populated!", requestDto.getImmutableId(), IMMUTABLE_ID);
		Assert.assertEquals("User ID field should have been populated!", requestDto.getUserId(), USER_ID);
		Assert.assertEquals("Email field should have been populated!", requestDto.getEmail(), EMAIL);
	}

	@Test
	public void shouldPopulateNameAttributes()
	{
		// Given
		when(customerModelMock.getFirstName()).thenReturn(FIRST_NAME);
		when(customerModelMock.getLastName()).thenReturn(LAST_NAME);
		when(customerModelMock.getMiddleName()).thenReturn(MIDDLE_NAME);
		when(customerModelMock.getTitle()).thenReturn(titleModelMock);
		when(titleModelMock.getCode()).thenReturn(TITLE);
		when(customerModelMock.getSuffix()).thenReturn(nameSuffixModelMock);
		when(nameSuffixModelMock.getCode()).thenReturn(SUFFIX);

		// When
		WileycomCustomerRegistrationRequestDto requestDto = new WileycomCustomerRegistrationRequestDto();
		wileycomCustomerRegistrationRequestDtoPopulator.populate(customerModelMock, requestDto);

		// Then
		Assert.assertEquals("First name field should have been populated!", requestDto.getFirstName(), FIRST_NAME);
		Assert.assertEquals("Last name field should have been populated!", requestDto.getLastName(), LAST_NAME);
		Assert.assertEquals("Middle name field should have been populated!", requestDto.getMiddleName(), MIDDLE_NAME);
		Assert.assertEquals("Title code field should have been populated!", requestDto.getTitleCode(), TITLE);
		Assert.assertEquals("Suffix code field should have been populated!", requestDto.getSuffixCode(), SUFFIX);
	}

	@Test
	public void shouldPopulateEducationAttributes()
	{
		// Given
		when(customerModelMock.getStudentId()).thenReturn(STUDENT_ID);
		when(customerModelMock.getMajor()).thenReturn(MAJOR);
		when(customerModelMock.getSchool()).thenReturn(schoolModelMock);
		when(schoolModelMock.getCode()).thenReturn(SCHOOL_ID);
		when(customerModelMock.getGraduationMonth()).thenReturn(GRADUATION_MONTH);
		when(customerModelMock.getGraduationYear()).thenReturn(GRADUATION_YEAR);

		// When
		WileycomCustomerRegistrationRequestDto requestDto = new WileycomCustomerRegistrationRequestDto();
		wileycomCustomerRegistrationRequestDtoPopulator.populate(customerModelMock, requestDto);

		// Then
		Assert.assertEquals("Student ID field should have been populated!", requestDto.getStudentId(), STUDENT_ID);
		Assert.assertEquals("Major field should have been populated!", requestDto.getMajor(), MAJOR);
		Assert.assertEquals("School ID field should have been populated!", requestDto.getSchoolId(), SCHOOL_ID);
		Assert.assertEquals("Graduation month field should have been populated!", requestDto.getGraduationMonth().intValue(),
				Integer.parseInt(GRADUATION_MONTH));
		Assert.assertEquals("Graduation year field should have been populated!", requestDto.getGraduationYear().intValue(),
				Integer.parseInt(GRADUATION_YEAR));
	}

	@Test
	public void shouldPopulateBusinessAttributes()
	{
		// Given
		when(b2BUnitModelMock.getSapAccountNumber()).thenReturn(SAP_NUMBER);
		when(b2BUnitModelMock.getAccountEcidNumber()).thenReturn(ECID_NUMBER);

		// When
		WileycomCustomerRegistrationRequestDto requestDto = new WileycomCustomerRegistrationRequestDto();
		wileycomCustomerRegistrationRequestDtoPopulator.populate(b2BCustomerModelMock, requestDto);

		// Then
		Assert.assertEquals("Organizational SAP account number field should have been populated!",
				requestDto.getOrganizationalSapAccountNumber(), SAP_NUMBER);
		Assert.assertEquals("Organizational ECID number field should have been populated!",
				requestDto.getOrganizationalEcidNumber(), ECID_NUMBER);
	}

	@Test
	public void shouldPopulateMiscellaneousAttributes()
	{
		// Given
		when(customerModelMock.getAcceptPromotions()).thenReturn(true);

		// When
		WileycomCustomerRegistrationRequestDto customerRequestDto = new WileycomCustomerRegistrationRequestDto();
		customerRequestDto.setSubscribeOnWileyPromos(false);
		wileycomCustomerRegistrationRequestDtoPopulator.populate(customerModelMock, customerRequestDto);

		// Then
		Assert.assertTrue("Accept promotions field should have been true!", customerRequestDto.getSubscribeOnWileyPromos());
	}

	@Test
	public void shouldPopulateRoleWithB2BAdmin()
	{
		// Given
		when(userServiceMock.isMemberOfGroup(eq(b2BCustomerModelMock), eq(b2bAdminGroupModelMock))).thenReturn(true);

		// When
		WileycomCustomerRegistrationRequestDto requestDto = new WileycomCustomerRegistrationRequestDto();
		wileycomCustomerRegistrationRequestDtoPopulator.populate(b2BCustomerModelMock, requestDto);

		// Then
		Assert.assertNotNull("User role should have been non null!", requestDto.getRole());
		Assert.assertEquals("User role should have been B2B admin!", requestDto.getRole(),
				WileycomCustomerRegistrationRequestDto.RoleEnum.B2BADMIN);
	}

	@Test
	public void shouldPopulateRoleWithB2BCustomer()
	{
		// Given
		when(userServiceMock.isMemberOfGroup(eq(b2BCustomerModelMock), eq(b2bCustomerGroupModelMock))).thenReturn(true);

		// When
		WileycomCustomerRegistrationRequestDto requestDto = new WileycomCustomerRegistrationRequestDto();
		wileycomCustomerRegistrationRequestDtoPopulator.populate(b2BCustomerModelMock, requestDto);

		// Then
		Assert.assertNotNull("User role should have been non null!", requestDto.getRole());
		Assert.assertEquals("User role should have been B2B user!", requestDto.getRole(),
				WileycomCustomerRegistrationRequestDto.RoleEnum.B2BUSER);
	}

	@Test
	public void shouldPopulateRoleWithStudent()
	{
		// Given
		when(customerModelMock.getStudentId()).thenReturn(STUDENT_ID);

		// When
		WileycomCustomerRegistrationRequestDto requestDto = new WileycomCustomerRegistrationRequestDto();
		wileycomCustomerRegistrationRequestDtoPopulator.populate(customerModelMock, requestDto);

		// Then
		Assert.assertNotNull("User role should have been non null!", requestDto.getRole());
		Assert.assertEquals("User role should have been student!", requestDto.getRole(),
				WileycomCustomerRegistrationRequestDto.RoleEnum.STUDENT);
	}

	@Test
	public void shouldPopulateRoleWithIndividual()
	{
		// When
		WileycomCustomerRegistrationRequestDto requestDto = new WileycomCustomerRegistrationRequestDto();
		wileycomCustomerRegistrationRequestDtoPopulator.populate(customerModelMock, requestDto);

		// Then
		Assert.assertNotNull("User role should have been non null!", requestDto.getRole());
		Assert.assertEquals("User role should have been individual!", requestDto.getRole(),
				WileycomCustomerRegistrationRequestDto.RoleEnum.INDIVIDUAL);
	}
}
