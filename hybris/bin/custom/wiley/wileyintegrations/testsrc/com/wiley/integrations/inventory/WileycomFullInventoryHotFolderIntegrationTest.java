package com.wiley.integrations.inventory;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.acceleratorservices.dataimport.batch.BatchException;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.regioncache.CacheController;
import de.hybris.platform.util.Config;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.io.File;
import java.io.IOException;

import javax.annotation.Resource;

import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandlingException;
import org.springframework.messaging.support.MessageBuilder;

import static org.fest.assertions.Assertions.assertThat;


/**
 * Created by Uladzimir_Barouski on 7/21/2016.
 */
@IntegrationTest
public class WileycomFullInventoryHotFolderIntegrationTest extends WileycomAbstractInventoryHotFolderIntegrationTest
{
	@Resource(name = "batchFilesWileycomFullInventoryProc")
	MessageChannel xmlProcFile;

	@Resource(name = "defaultCacheController")
	CacheController cacheController;

	@Before
	public void cleanUp() throws IOException
	{
		hotFolderPath = Config.getString("wiley.impex.basefolder", null) + "/junit/inventory/";
		destFileMask = hotFolderPath + "processing/full_inventory-%s-%s.csv";
		FileUtils.cleanDirectory(new File(hotFolderPath));
	}

	@Test
	public void checkImportingOfFileWithNewInventory() throws Exception
	{
		importCsv("/wileycore/test/sampledata/wileycom/products-stocklevels.impex", DEFAULT_ENCODING);
		Long sequenceId = copyFileToHotFolder("/wileycore/test/inventory/full_inventory-default-1.csv", WAREHOUSE_1);

		ProductModel productModel = productService.getProductForCode(PRODUCT_1);
		WarehouseModel warehouseModel = warehouseService.getWarehouseForCode(WAREHOUSE_1);
		File file = new File(String.format(destFileMask, WAREHOUSE_1, sequenceId));
		xmlProcFile.send(MessageBuilder.withPayload(file).build());
		//we need clear cache here because we use direct JDBC call instead of modelservice for StockLevels remove
		cacheController.getRegions().forEach(r -> cacheController.clearCache(r));
		assertEquals(sequenceId, warehouseService.getWarehouseForCode(WAREHOUSE_1).getSequenceId());
		validateStockLevelUpdated(PRODUCT_1, warehouseModel, sequenceId);
		validateStockLevelUpdated(PRODUCT_2, warehouseModel, sequenceId);
		//validate stock was removed
		validateStockLevelRemoved(PRODUCT_3, warehouseModel);
		validateStockLevelRemoved(PRODUCT_4, warehouseModel);
	}

	@Test
	public void checkImportingOfFileWithNonExistingProduct() throws Exception
	{
		importCsv("/wileycore/test/sampledata/wileycom/products-stocklevels.impex", DEFAULT_ENCODING);
		Long sequenceId = copyFileToHotFolder("/wileycore/test/inventory/full_inventory-default-2.csv", WAREHOUSE_1);

		WarehouseModel warehouseModel = warehouseService.getWarehouseForCode(WAREHOUSE_1);
		File file = new File(String.format(destFileMask, WAREHOUSE_1, sequenceId));
		xmlProcFile.send(MessageBuilder.withPayload(file).build());
		//we need clear cache here because we use direct JDBC call instead of modelservice for StockLevels remove
		cacheController.getRegions().forEach(r -> cacheController.clearCache(r));
		assertEquals(sequenceId, warehouseService.getWarehouseForCode(WAREHOUSE_1).getSequenceId());
		File archive = new File(hotFolderPath + "/archive");
		assertEquals(1, archive.list().length);
		validateStockLevelRemoved(PRODUCT_1, warehouseModel);
		validateStockLevelRemoved(PRODUCT_2, warehouseModel);
		validateStockLevelRemoved(PRODUCT_3, warehouseModel);
		validateStockLevelRemoved(PRODUCT_4, warehouseModel);
	}

	@Test
	public void checkImportingOfFileWithNonExistingWarehouse() throws Exception
	{
		Long sequenceId = copyFileToHotFolder("/wileycore/test/inventory/full_inventory-nonExistingWarehouse-3.csv",
				NON_EXISTING_WAREHOUSE);

		File file = new File(String.format(destFileMask, NON_EXISTING_WAREHOUSE, sequenceId));
		try
		{
			xmlProcFile.send(MessageBuilder.withPayload(file).build());
		}
		catch (MessageHandlingException e)
		{
			assertThat(e.getCause()).isInstanceOf(BatchException.class).hasMessage(
					String.format("WarehouseModel with code '%s' not found!", NON_EXISTING_WAREHOUSE));
		}

	}


	@Test
	public void checkImportingOfFileWithOldSequenceId() throws Exception
	{
		Long sequenceId = copyFileToHotFolder("/wileycore/test/inventory/full_inventory-default-1.csv", WAREHOUSE_1);

		ProductModel productModel = productService.getProductForCode(PRODUCT_1);
		//update warehose with newer secuenceId
		WarehouseModel warehouseModel = warehouseService.getWarehouseForCode(WAREHOUSE_1);
		warehouseModel.setSequenceId(sequenceId + 100);
		modelService.save(warehouseModel);

		File file = new File(String.format(destFileMask, WAREHOUSE_1, sequenceId));
		xmlProcFile.send(MessageBuilder.withPayload(file).build());
		File archive = new File(hotFolderPath + "/archive");
		File error = new File(hotFolderPath + "/error");
		assertNull(error.list());
		assertEquals(1, archive.list().length);
		validateStockLevelNotUpdated(warehouseModel);
	}

	private void validateStockLevelRemoved(final String productCode, final WarehouseModel warehouseModel)
	{
		ProductModel productModel2 = productService.getProductForCode(productCode);
		assertNull(stockService.getStockLevel(productModel2, warehouseModel));
	}

}
