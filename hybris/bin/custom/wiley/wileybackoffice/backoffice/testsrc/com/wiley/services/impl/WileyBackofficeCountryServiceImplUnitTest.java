package com.wiley.services.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.store.BaseStoreModel;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.i18n.WileyCommonI18NService;
import com.wiley.core.servicelayer.i18n.WileycomI18NService;
import com.wiley.widgets.data.CountryInternalData;

import static com.wiley.core.constants.WileyCoreConstants.CURRENT_COUNTRY_SESSION_ATTR;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyBackofficeCountryServiceImplUnitTest
{
	private static final String COUNTRY_CODE = "US";
	private static final String COUNTRY_NAME = "United States";
	@InjectMocks
	private WileyBackofficeCountryServiceImpl wileyBackofficeCountryService;
	@Mock
	private WileycomI18NService wileycomI18NServiceMock;
	@Mock
	private CommonI18NService commonI18NServiceMock;
	@Mock
	private CountryModel countryMock;
	@Mock
	private SessionService sessionServiceMock;
	@Mock
	private WileyCommonI18NService wileyCommonI18NServiceMock;
	@Mock
	private BaseStoreModel baseStoreModelMock;

	@Before
	public void setUp() throws Exception
	{
		when(countryMock.getIsocode()).thenReturn(COUNTRY_CODE);
		when(countryMock.getName()).thenReturn(COUNTRY_NAME);
	}

	@Test
	public void getCurrentCountry()
	{
		when(wileycomI18NServiceMock.getCurrentCountry()).thenReturn(Optional.of(countryMock));

		final CountryInternalData currentCountry = wileyBackofficeCountryService.getCurrentCountry();

		assertEquals(currentCountry.getIsocode(), COUNTRY_CODE);
		assertEquals(currentCountry.getName(), COUNTRY_NAME);
	}

	@Test
	public void getAllCountries()
	{
		when(commonI18NServiceMock.getAllCountries()).thenReturn(Collections.singletonList(countryMock));

		final List<CountryInternalData> allCountries = wileyBackofficeCountryService.getAllCountries();

		assertEquals(allCountries.size(), 1);
		assertEquals(allCountries.get(0).getIsocode(), COUNTRY_CODE);
		assertEquals(allCountries.get(0).getName(), COUNTRY_NAME);
	}

	@Test
	public void setCurrentCountry()
	{
		final CurrencyModel currency = new CurrencyModel();

		when(wileyCommonI18NServiceMock.getDefaultCurrency(countryMock, baseStoreModelMock)).thenReturn(currency);

		wileyBackofficeCountryService.setCurrentCountry(countryMock, baseStoreModelMock);

		verify(sessionServiceMock).setAttribute(CURRENT_COUNTRY_SESSION_ATTR, countryMock);
		verify(commonI18NServiceMock).setCurrentCurrency(currency);
	}
}
