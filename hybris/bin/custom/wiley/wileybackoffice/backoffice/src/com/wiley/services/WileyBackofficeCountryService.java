/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 */
package com.wiley.services;


import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.List;

import com.wiley.widgets.data.CountryInternalData;


public interface WileyBackofficeCountryService
{

	CountryInternalData getCurrentCountry();

	List<CountryInternalData> getAllCountries();

	List<CountryInternalData> getAllCountries(final BaseStoreModel baseStoreModel);

	void setCurrentCountry(final CountryModel currentCountry, final BaseStoreModel baseStoreModel);
}
