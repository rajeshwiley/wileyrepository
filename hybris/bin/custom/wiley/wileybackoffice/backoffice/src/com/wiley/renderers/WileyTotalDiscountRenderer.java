package com.wiley.renderers;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.omsbackoffice.renderers.TotalPriceWithTaxRenderer;
import de.hybris.platform.util.DiscountValue;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Div;

import com.hybris.cockpitng.components.Editor;
import com.hybris.cockpitng.core.config.impl.jaxb.editorarea.AbstractPanel;
import com.hybris.cockpitng.core.config.impl.jaxb.editorarea.Attribute;
import com.hybris.cockpitng.core.config.impl.jaxb.editorarea.CustomPanel;
import com.hybris.cockpitng.dataaccess.facades.type.DataAttribute;
import com.hybris.cockpitng.dataaccess.facades.type.DataType;
import com.hybris.cockpitng.dataaccess.facades.type.TypeFacade;
import com.hybris.cockpitng.dataaccess.facades.type.exceptions.TypeNotFoundException;
import com.hybris.cockpitng.engine.WidgetInstanceManager;
import com.hybris.cockpitng.util.YTestTools;
import com.hybris.cockpitng.widgets.editorarea.renderer.impl.DefaultEditorAreaPanelRenderer;


public class WileyTotalDiscountRenderer extends DefaultEditorAreaPanelRenderer
{
	private static final Logger LOG = LoggerFactory.getLogger(TotalPriceWithTaxRenderer.class);
	private static final String ORDER = "Order";
	private static final String QUALIFIER = "totalDiscounts";
	private static final String LABEL = "customersupportbackoffice.order.details.orderdiscount";
	private TypeFacade typeFacade;
	private Double totalDiscountAmount;

	public void render(Component component, AbstractPanel abstractPanelConfiguration, Object object, DataType dataType,
			WidgetInstanceManager widgetInstanceManager)
	{
		if (abstractPanelConfiguration instanceof CustomPanel && object instanceof AbstractOrderModel)
		{
			this.totalDiscountAmount = this.getOrderTotalDiscount((AbstractOrderModel) object);

			try
			{
				Attribute attribute = new Attribute();
				attribute.setLabel(LABEL);
				attribute.setQualifier(QUALIFIER);
				attribute.setReadonly(Boolean.TRUE);
				DataType order = this.getTypeFacade().load(ORDER);
				boolean canReadObject = this.getPermissionFacade().canReadInstanceProperty(order.getClazz(), QUALIFIER);
				if (canReadObject)
				{
					this.createAttributeRenderer().render(component, attribute, order.getClazz(), order, widgetInstanceManager);
				}
				else
				{
					Div attributeContainer = new Div();
					attributeContainer.setSclass("yw-editorarea-tabbox-tabpanels-tabpanel-groupbox-ed");
					this.renderNotReadableLabel(attributeContainer, attribute, dataType,
							this.getLabelService().getAccessDeniedLabel(attribute));
					attributeContainer.setParent(component);
				}
			}
			catch (TypeNotFoundException exception)
			{
				LOG.warn(exception.getMessage());
			}
		}
	}

	protected Editor createEditor(DataType genericType, WidgetInstanceManager widgetInstanceManager, Attribute attribute,
			Object object)
	{
		DataAttribute genericAttribute = genericType.getAttribute(attribute.getQualifier());
		if (genericAttribute == null)
		{
			return null;
		}
		else
		{
			String qualifier = genericAttribute.getQualifier();
			String referencedModelProperty = "Order." + attribute.getQualifier();
			Editor editor = new Editor();
			editor.setReadOnly(Boolean.TRUE);
			editor.setLocalized(Boolean.FALSE);
			editor.setWidgetInstanceManager(widgetInstanceManager);
			editor.setType(this.resolveEditorType(genericAttribute));
			editor.setOptional(!genericAttribute.isMandatory());
			YTestTools.modifyYTestId(editor, "editor_Order." + qualifier);
			editor.setAttribute("parentObject", object);
			editor.setWritableLocales(this.getPermissionFacade().getWritableLocalesForInstance(object));
			editor.setReadableLocales(this.getPermissionFacade().getReadableLocalesForInstance(object));
			if (genericAttribute.isLocalized())
			{
				editor.addParameter("localizedEditor.attributeDescription", this.getAttributeDescription(genericType, attribute));
			}

			editor.setProperty(referencedModelProperty);
			if (StringUtils.isNotBlank(attribute.getEditor()))
			{
				editor.setDefaultEditor(attribute.getEditor());
			}

			editor.setPartOf(genericAttribute.isPartOf());
			editor.setOrdered(genericAttribute.isOrdered());
			editor.afterCompose();
			editor.setSclass("ye-default-editor-readonly");
			editor.setInitialValue(this.totalDiscountAmount);
			return editor;
		}
	}

	private Double getOrderTotalDiscount(AbstractOrderModel abstractOrderModel)
	{
		double orderDiscount = abstractOrderModel.getTotalDiscounts() != null ? abstractOrderModel.getTotalDiscounts() : 0.0d;
		return orderDiscount + abstractOrderModel.getEntries().stream()
				.mapToDouble(entry -> entry.getDiscountValues().stream()
						.mapToDouble(DiscountValue::getAppliedValue)
						.sum())
				.sum();
	}

	private TypeFacade getTypeFacade()
	{
		return this.typeFacade;
	}

	@Required
	public void setTypeFacade(TypeFacade typeFacade)
	{
		this.typeFacade = typeFacade;
	}
}
