package com.wiley.pcmbackoffice.widgets.controller;

import de.hybris.platform.basecommerce.enums.RefundReason;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.refund.OrderRefundEntry;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.util.DiscountValue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.InputEvent;
import org.zkoss.zk.ui.event.SelectEvent;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.Doublebox;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Row;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.impl.InputElement;

import com.hybris.backoffice.i18n.BackofficeLocaleService;
import com.hybris.backoffice.widgets.notificationarea.NotificationService;
import com.hybris.backoffice.widgets.notificationarea.event.NotificationEvent.Level;
import com.hybris.cockpitng.annotations.SocketEvent;
import com.hybris.cockpitng.annotations.ViewEvent;
import com.hybris.cockpitng.core.events.CockpitEventQueue;
import com.hybris.cockpitng.core.events.impl.DefaultCockpitEvent;
import com.hybris.cockpitng.util.DefaultWidgetController;
import com.wiley.core.payment.strategies.AmountToPayCalculationStrategy;
import com.wiley.core.refund.OrderRefundException;
import com.wiley.core.refund.OrderRefundRequest;
import com.wiley.core.refund.WileyRefundService;
import com.wiley.pcmbackoffice.dto.OrderEntryToRefundDto;


public class RefundOrderController extends DefaultWidgetController
{
	private static final Logger LOGGER = LoggerFactory.getLogger(RefundOrderController.class);
	private static final long serialVersionUID = 1L;

	private static final String IN_SOCKET = "inputObject";
	private static final String CONFIRM_ID = "confirmrefund";

	private static final String UNDO_ID = "undorefund";
	private static final String COMPLETED = "completed";
	private static final String EVENT_ON_CLICK = "onClick";
	private static final String EVENT_ON_CHECK = "onCheck";

	private static final String WILEYBACKOFFICE_REFUNDORDER_CONFIRM_MSG
			= "wileybackoffice.refundorder.confirm.msg";
	private static final String WILEYBACKOFFICE_REFUNDORDER_CONFIRM_TITLE
			= "wileybackoffice.refundorder.confirm.title";
	private static final String WILEYBACKOFFICE_REFUNDORDER_ERROR_REASON
			= "wileybackoffice.refundorder.error.reason";
	private static final String WILEYBACKOFFICE_REFUNDORDER_MISSING_SELECTED_LINE
			= "wileybackoffice.refundorder.missing.selectedLine";
	private static final String WILEYBACKOFFICE_REFUNDORDER_QUANTITY_INVALID
			= "wileybackoffice.refundorder.error.qtyrefunded.invalid";
	private static final String WILEYBACKOFFICE_REFUNDORDER_CONFIRM_ERROR
			= "wileybackoffice.refundorder.confirm.error";
	private static final String REFUNDORDER_CONFIRM_ICON
			= "oms-widget-cancelorder-confirm-icon";
	private static final String WILEYBACKOFFICE_REFUNDORDER_CONFIRM_SUCCESS
			= "wileybackoffice.refundorder.confirm.success";
	private static final String WILEYBACKOFFICE_REFUNDORDER_MAXREFUNDAMOUNT_EXCEEDED
			= "wileybackoffice.refundorder.maxrefundamount.exceeded";

	private static final int COLUMN_INDEX_MAX_QUANTITY = 5;
	private static final int COLUMN_INDEX_EXPECTED_QUANTITY = 6;
	private static final int COLUMN_INDEX_CANCEL_REASON = 7;
	private static final int COLUMN_INDEX_CANCEL_COMMENT = 8;

	private static final String EVENT_ON_SELECT = "onSelect";
	private static final String EVENT_ON_CHANGING = "onChanging";
	private static final String EVENT_ON_CHANGE = "onChange";
	private static final String EVENT_ON_LATER_CUSTOM_CHANGE = "onLaterCustomChange";
	private static final String EVENT_ON_CUSTOM_CHANGE = "onCustomChange";
	private static final String EVENT_OBJECTS_UPDATED = "objectsUpdated";
	private static final String NOTIFICATION_EVENT_TYPE = "JustMessage";
	private final List<String> refundReasons = new ArrayList();
	private transient List<AbstractOrderEntryModel> orderRefundableEntries;
	private transient Set<OrderEntryToRefundDto> orderEntriesToRefund;
	private OrderModel orderModel;
	@Wire
	private Textbox orderNumber;
	@Wire
	private Textbox customerName;
	@Wire
	private Doublebox taxes;
	@Wire
	private Doublebox orderDiscount;
	@Wire
	private Doublebox deliveryCosts;
	@Wire
	private Doublebox originalOrderTotal;
	@Wire
	private Doublebox refundAmount;

	@Wire
	private Combobox globalRefundReasons;
	@Wire
	private Textbox globalRefundComment;
	@Wire
	private Grid orderEntries;
	@Wire
	private Checkbox globalRefundEntriesSelection;

	@WireVariable
	private transient BackofficeLocaleService cockpitLocaleService;
	@WireVariable
	private transient EnumerationService enumerationService;
	@WireVariable
	private transient ModelService modelService;
	@WireVariable
	private transient CockpitEventQueue cockpitEventQueue;
	@WireVariable
	private transient NotificationService notificationService;
	@WireVariable
	private transient WileyRefundService refundService;
	@WireVariable
	private transient AmountToPayCalculationStrategy amountToPayCalculationStrategy;

	public RefundOrderController()
	{
	}

	@ViewEvent(
			componentID = CONFIRM_ID,
			eventName = EVENT_ON_CLICK
	)
	public void confirmRefund()
	{
		validateRequest();
		showMessageBox();
	}

	@SocketEvent(
			socketId = IN_SOCKET
	)
	public void initRefundOrderForm(final OrderModel orderModel)
	{
		setOrderModel(orderModel);
		setWidgetTitle(getLabel(WILEYBACKOFFICE_REFUNDORDER_CONFIRM_TITLE) + " " + getOrderModel().getCode());

		refundReasons.clear();
		globalRefundEntriesSelection.setChecked(false);
		orderNumber.setValue(getOrderModel().getCode());
		customerName.setValue(getOrderModel().getUser().getDisplayName());
		taxes.setValue(getOrderModel().getTotalTax());
		orderDiscount.setValue(getOrderTotalDiscount(getOrderModel()));
		deliveryCosts.setValue(getOrderModel().getDeliveryCost());
		originalOrderTotal.setValue(getOriginalOrderTotal());
		refundAmount.setValue(getMaxRefundAmount());

		getRefundReasons().forEach((reason) -> refundReasons
				.add(getEnumerationService().getEnumerationName(reason, getLocale())));
		globalRefundReasons.setModel(new ListModelArray(refundReasons));
		orderEntriesToRefund = new HashSet();

		orderRefundableEntries = getOrderRefundService().getRefundableEntries(getOrderModel().getEntries());

		orderRefundableEntries
				.forEach((entry) -> orderEntriesToRefund.add(
						new OrderEntryToRefundDto(entry, refundReasons, 0L)));

		getOrderEntries().setModel(new ListModelList(orderEntriesToRefund));
		getOrderEntries().renderAll();
		addListeners();
	}

	private Double getOrderTotalDiscount(OrderModel order)
	{
		double orderDiscount = order.getTotalDiscounts() != null ? order.getTotalDiscounts() : 0.0d;
		return orderDiscount + order.getEntries().stream()
				.mapToDouble(entry -> entry.getDiscountValues().stream()
						.mapToDouble(DiscountValue::getAppliedValue)
						.sum())
				.sum();
	}

	private double getOriginalOrderTotal()
	{
		return amountToPayCalculationStrategy.getOrderAmountToPay(getOrderModel()).doubleValue();
	}

	private double getMaxRefundAmount()
	{
		return getOrderRefundService().getMaxRefundAmount(getOrderModel()).doubleValue();
	}

	private List<RefundReason> getRefundReasons()
	{
		return getEnumerationService().getEnumerationValues(RefundReason.class);
	}

	@ViewEvent(componentID = UNDO_ID, eventName = EVENT_ON_CLICK)
	public void reset()
	{
		globalRefundReasons.setSelectedItem(null);
		globalRefundComment.setValue(StringUtils.EMPTY);
		initRefundOrderForm(getOrderModel());
	}

	private void addListeners()
	{
		List<Component> rows = getOrderEntriesGridRows();
		Iterator rowsIterator = rows.iterator();

		while (rowsIterator.hasNext())
		{
			Component row = (Component) rowsIterator.next();
			Iterator columns = row.getChildren().iterator();

			while (columns.hasNext())
			{
				Component myComponent = (Component) columns.next();
				if (myComponent instanceof Checkbox)
				{
					myComponent.addEventListener(EVENT_ON_CHECK, (event) -> {
						updateRow((Row) event.getTarget().getParent());
					});
				}
				else if (myComponent instanceof Combobox)
				{
					myComponent.addEventListener(EVENT_ON_CUSTOM_CHANGE, (event) -> {
						Events.echoEvent(EVENT_ON_LATER_CUSTOM_CHANGE, myComponent, event.getData());
					});
					myComponent.addEventListener(EVENT_ON_LATER_CUSTOM_CHANGE, (event) -> {
						Clients.clearWrongValue(myComponent);
						myComponent.invalidate();
						handleIndividualRefundReason(event);
					});
				}
				else if (myComponent instanceof Intbox)
				{
					myComponent.addEventListener(EVENT_ON_CHANGE, (event) -> {
						autoSelect(event);
						((OrderEntryToRefundDto) ((Row) event.getTarget().getParent()).getValue())
								.setExpectedQuantity(Long.valueOf(((InputEvent) event).getValue()));
					});
				}
				else if (myComponent instanceof Textbox)
				{
					myComponent.addEventListener(EVENT_ON_CHANGING, (event) -> {
						autoSelect(event);
						((OrderEntryToRefundDto) ((Row) event.getTarget().getParent()).getValue())
								.setRefundOrderEntryComment(((InputEvent) event).getValue());
					});
				}
			}
		}

		globalRefundReasons.addEventListener(EVENT_ON_SELECT, this::handleGlobalRefundReason);
		globalRefundComment.addEventListener(EVENT_ON_CHANGING, this::handleGlobalCancelComment);
		globalRefundEntriesSelection.addEventListener(EVENT_ON_CHECK, (event) -> {
			selectAllEntries();
		});
	}

	private void applyToGrid(final Object data, final int childrenIndex)
	{
		getOrderEntriesGridRows()
				.stream()
				.filter((entry) -> ((Checkbox) entry.getChildren().iterator().next()).isChecked())
				.forEach((entry) -> applyToRow(data, childrenIndex, entry));
	}

	private void applyToRow(final Object data, final int column, final Component row)
	{
		int index = 0;

		for (Iterator children = row.getChildren().iterator(); children.hasNext(); ++index)
		{
			Component myComponent = (Component) children.next();
			if (index == column)
			{
				if (myComponent instanceof Checkbox && data != null)
				{
					((Checkbox) myComponent).setChecked((Boolean) data);
				}

				if (myComponent instanceof Combobox)
				{
					if (data == null)
					{
						((Combobox) myComponent).setSelectedItem(null);
					}
					else
					{
						((Combobox) myComponent).setSelectedIndex((Integer) data);
					}
				}
				else if (myComponent instanceof Intbox)
				{
					((Intbox) myComponent).setValue((Integer) data);
				}
				else if (myComponent instanceof Textbox)
				{
					((Textbox) myComponent).setValue((String) data);
				}
			}
		}

	}

	private void autoSelect(final Event event)
	{
		((Checkbox) event.getTarget().getParent().getChildren().iterator().next()).setChecked(true);
	}

	private OrderRefundRequest buildRefundRequest()
	{
		if (getOrderModel() != null)
		{
			List<OrderRefundEntry> orderRefundEntries = new ArrayList();
			getOrderEntriesGridRows()
					.stream()
					.filter((entry) -> ((Checkbox) entry.getFirstChild()).isChecked())
					.forEach((entry) -> createOrderRefundEntry(orderRefundEntries, ((Row) entry).getValue()));

			OrderRefundRequest orderRefundRequest = new OrderRefundRequest(getOrderModel(), orderRefundEntries,
					BigDecimal.valueOf(refundAmount.doubleValue()));
			return orderRefundRequest;
		}
		else
		{
			return null;
		}
	}

	private void createOrderRefundEntry(final List<OrderRefundEntry> orderCancelEntries, final Object entry)
	{
		OrderEntryToRefundDto orderEntryToRefund = (OrderEntryToRefundDto) entry;
		OrderRefundEntry orderRefundEntry = new OrderRefundEntry(orderEntryToRefund.getOrderEntry(),
				orderEntryToRefund.getExpectedQuantity(), orderEntryToRefund.getRefundOrderEntryComment(),
				orderEntryToRefund.getSelectedReason());
		orderCancelEntries.add(orderRefundEntry);
	}

	private int getReasonIndex(final RefundReason cancelReason)
	{
		int index = 0;
		String myReason = getEnumerationService().getEnumerationName(cancelReason, getLocale());

		for (Iterator refundReasonIterator = refundReasons.iterator(); refundReasonIterator.hasNext(); ++index)
		{
			String reason = (String) refundReasonIterator.next();
			if (myReason.equals(reason))
			{
				break;
			}
		}

		return index;
	}

	private Optional<RefundReason> getSelectedRefundReason(final Event event)
	{
		Optional<RefundReason> result = Optional.empty();
		if (!((SelectEvent) event).getSelectedItems().isEmpty())
		{
			Object selectedValue = ((Comboitem) ((SelectEvent) event).getSelectedItems().iterator().next()).getValue();
			result = matchingComboboxRefundReason(selectedValue.toString());
		}

		return result;
	}

	private void handleGlobalCancelComment(final Event event)
	{
		applyToGrid(((InputEvent) event).getValue(), COLUMN_INDEX_CANCEL_COMMENT);
		getOrderEntriesGridRows()
				.stream()
				.filter((entry) -> ((Checkbox) entry.getChildren().iterator().next()).isChecked())
				.forEach((entry) -> {
					OrderEntryToRefundDto myEntry = ((Row) entry).getValue();
					myEntry.setRefundOrderEntryComment(((InputEvent) event).getValue());
				});
	}

	private void handleGlobalRefundReason(final Event event)
	{
		Optional<RefundReason> refundReason = getSelectedRefundReason(event);
		if (refundReason.isPresent())
		{
			applyToGrid(getReasonIndex(refundReason.get()), COLUMN_INDEX_CANCEL_REASON);
			getOrderEntriesGridRows()
					.stream()
					.filter((entry) -> ((Checkbox) entry.getChildren().iterator().next()).isChecked())
					.forEach((entry) -> {
						OrderEntryToRefundDto myEntry = ((Row) entry).getValue();
						myEntry.setSelectedReason(refundReason.get());
					});
		}
	}

	private void handleIndividualRefundReason(final Event event)
	{
		Optional<RefundReason> refundReason = getCustomSelectedRefundReason(event);
		if (refundReason.isPresent())
		{
			autoSelect(event);
			((OrderEntryToRefundDto) ((Row) event.getTarget().getParent()).getValue())
					.setSelectedReason(refundReason.get());
		}
	}

	private void updateRow(final Row row)
	{
		OrderEntryToRefundDto orderEntry = row.getValue();

		if (!((Checkbox) row.getChildren().iterator().next()).isChecked())
		{
			applyToRow(0, COLUMN_INDEX_EXPECTED_QUANTITY, row);
			applyToRow(null, COLUMN_INDEX_CANCEL_REASON, row);
			applyToRow(null, COLUMN_INDEX_CANCEL_COMMENT, row);

			orderEntry.setExpectedQuantity(0L);
			orderEntry.setSelectedReason(null);
			orderEntry.setRefundOrderEntryComment(null);
		}
		else
		{
			Long quantity = orderEntry.getOrderEntry().getQuantity();
			applyToRow(quantity.intValue(), COLUMN_INDEX_EXPECTED_QUANTITY, row);
			applyToRow(globalRefundReasons.getSelectedIndex(), COLUMN_INDEX_CANCEL_REASON, row);
			applyToRow(globalRefundComment.getValue(), COLUMN_INDEX_CANCEL_COMMENT, row);

			orderEntry.setExpectedQuantity(quantity);
			Optional<RefundReason> reason = matchingComboboxRefundReason(
					globalRefundReasons.getSelectedItem() != null ?
							globalRefundReasons.getSelectedItem().getLabel() : null);
			orderEntry.setSelectedReason(reason.orElse(null));
			orderEntry.setRefundOrderEntryComment(globalRefundComment.getValue());
		}
	}

	private Optional<RefundReason> getCustomSelectedRefundReason(final Event event)
	{
		Optional<RefundReason> reason = Optional.empty();
		if (event.getTarget() instanceof Combobox)
		{
			Object selectedValue = event.getData();
			reason = matchingComboboxRefundReason(selectedValue.toString());
		}

		return reason;
	}

	private Optional<RefundReason> matchingComboboxRefundReason(final String refundReasonLabel)
	{
		return getRefundReasons()
				.stream()
				.filter((reason) -> getEnumerationService().getEnumerationName(reason,
						getLocale()).equals(refundReasonLabel))
				.findFirst();
	}

	private void processRefund(final Event obj)
	{
		if (Button.YES.event.equals(obj.getName()))
		{
			try
			{
				getOrderRefundService().processRefund(buildRefundRequest());
				getNotificationService().notifyUser(StringUtils.EMPTY, NOTIFICATION_EVENT_TYPE, Level.SUCCESS,
						new Object[] { getLabel(WILEYBACKOFFICE_REFUNDORDER_CONFIRM_SUCCESS) });
			}
			catch (OrderRefundException orderRefundException)
			{
				LOGGER.error(orderRefundException.getMessage(), orderRefundException);
				getNotificationService().notifyUser(StringUtils.EMPTY, NOTIFICATION_EVENT_TYPE, Level.FAILURE,
						new Object[] { getLabel(WILEYBACKOFFICE_REFUNDORDER_CONFIRM_ERROR) });
			}

			OrderModel orderModel = getModelService().get(getOrderModel().getPk());
			orderModel.getEntries().forEach((entry) -> getCockpitEventQueue()
					.publishEvent(new DefaultCockpitEvent(EVENT_OBJECTS_UPDATED, entry, null)));
			sendOutput(CONFIRM_ID, COMPLETED);
		}
	}

	protected void selectAllEntries()
	{
		applyToGrid(Boolean.TRUE, 0);
		Iterator orderEntryGridRows = getOrderEntriesGridRows().iterator();

		while (orderEntryGridRows.hasNext())
		{
			Component row = (Component) orderEntryGridRows.next();
			Component firstComponent = row.getChildren().iterator().next();
			if (firstComponent instanceof Checkbox)
			{
				((Checkbox) firstComponent).setChecked(globalRefundEntriesSelection.isChecked());
			}

			updateRow((Row) row);
			if (globalRefundEntriesSelection.isChecked())
			{
				int cancellableQuantity = Integer.parseInt(((Label) row.getChildren()
						.get(COLUMN_INDEX_MAX_QUANTITY)).getValue());
				applyToRow(cancellableQuantity, COLUMN_INDEX_EXPECTED_QUANTITY, row);
			}
		}

		if (globalRefundEntriesSelection.isChecked())
		{
			orderEntriesToRefund.forEach((entry) -> {
				entry.setExpectedQuantity(entry.getOrderEntry().getQuantity());
			});
		}

	}

	protected void showMessageBox()
	{
		Messagebox.show(
				getLabel(WILEYBACKOFFICE_REFUNDORDER_CONFIRM_MSG),
				getLabel(WILEYBACKOFFICE_REFUNDORDER_CONFIRM_TITLE) + " " + getOrderModel().getCode(),
				new Button[] { Button.NO, Button.YES },
				REFUNDORDER_CONFIRM_ICON,
				this::processRefund);
	}

	protected Component targetFieldToApplyValidation(final String stringToValidate,
			final int indexLabelToCheck,
			final int indexTargetComponent)
	{
		Iterator orderEntryGridRows = getOrderEntriesGridRows().iterator();

		while (orderEntryGridRows.hasNext())
		{
			Component component = (Component) orderEntryGridRows.next();
			Label label = (Label) component.getChildren().get(indexLabelToCheck);
			if (label.getValue().equals(stringToValidate))
			{
				return component.getChildren().get(indexTargetComponent);
			}
		}

		return null;
	}

	private Long getMaxQuantityToRefund(final OrderEntryToRefundDto entry)
	{
		return entry.getOrderEntry().getQuantity();
	}

	protected void validateOrderEntry(final OrderEntryToRefundDto entry)
	{
		if (entry.getExpectedQuantity() > getMaxQuantityToRefund(entry) || entry.getExpectedQuantity() < 0)
		{
			InputElement quantity = (InputElement) targetFieldToApplyValidation(
					entry.getOrderEntry().getProduct().getCode(), 1, COLUMN_INDEX_EXPECTED_QUANTITY);
			throw new WrongValueException(quantity, getLabel(WILEYBACKOFFICE_REFUNDORDER_QUANTITY_INVALID));
		}
		else if (entry.getSelectedReason() == null && entry.getExpectedQuantity() > 0L)
		{
			Combobox reason = (Combobox) targetFieldToApplyValidation(entry.getOrderEntry().getProduct().getCode(),
					1, COLUMN_INDEX_CANCEL_REASON);
			throw new WrongValueException(reason, getLabel(WILEYBACKOFFICE_REFUNDORDER_ERROR_REASON));
		}
	}

	protected void validateRequest()
	{
		for (final Component row : this.getOrderEntriesGridRows())
		{
			if (((Checkbox) row.getChildren().iterator().next()).isChecked())
			{
				InputElement expectedQuantity = (InputElement) row.getChildren().get(COLUMN_INDEX_EXPECTED_QUANTITY);
				if (expectedQuantity.getRawValue().equals(0))
				{
					throw new WrongValueException(expectedQuantity,
							this.getLabel(WILEYBACKOFFICE_REFUNDORDER_QUANTITY_INVALID));
				}
			}
		}

		double refundAmountValue = getRefundAmountValue();
		if (refundAmountValue <= 0 ||
				refundAmountValue > getMaxRefundAmount())
		{
			throw new WrongValueException(refundAmount,
					getLabel(WILEYBACKOFFICE_REFUNDORDER_MAXREFUNDAMOUNT_EXCEEDED));
		}
		ListModelList<OrderEntryToRefundDto> modelList = (ListModelList) getOrderEntries().getModel();
		if (modelList.stream().allMatch((entry) -> entry.getExpectedQuantity() == 0L))
		{
			throw new WrongValueException(globalRefundEntriesSelection,
					getLabel(WILEYBACKOFFICE_REFUNDORDER_MISSING_SELECTED_LINE));
		}
		else
		{
			modelList.forEach(this::validateOrderEntry);
		}
	}

	private double getRefundAmountValue()
	{
		double refundAmountValue;
		try
		{
			refundAmountValue = refundAmount.doubleValue();
		}
		catch (WrongValueException ex)
		{
			throw new WrongValueException(refundAmount,
					getLabel(WILEYBACKOFFICE_REFUNDORDER_MAXREFUNDAMOUNT_EXCEEDED));
		}
		return refundAmountValue;
	}

	protected List<Component> getOrderEntriesGridRows()
	{
		return getOrderEntries().getRows().getChildren();
	}

	protected Locale getLocale()
	{
		return getCockpitLocaleService().getCurrentLocale();
	}

	protected BackofficeLocaleService getCockpitLocaleService()
	{
		return this.cockpitLocaleService;
	}

	protected Grid getOrderEntries()
	{
		return this.orderEntries;
	}

	protected OrderModel getOrderModel()
	{
		return this.orderModel;
	}

	public void setOrderModel(OrderModel orderModel)
	{
		this.orderModel = orderModel;
	}

	protected EnumerationService getEnumerationService()
	{
		return this.enumerationService;
	}

	protected ModelService getModelService()
	{
		return this.modelService;
	}

	protected CockpitEventQueue getCockpitEventQueue()
	{
		return this.cockpitEventQueue;
	}

	public WileyRefundService getOrderRefundService()
	{
		return refundService;
	}

	protected NotificationService getNotificationService()
	{
		return this.notificationService;
	}
}
