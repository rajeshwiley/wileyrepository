package com.wiley.widgets.controller;

import de.hybris.platform.adaptivesearch.data.AbstractAsBoostItemConfiguration;
import de.hybris.platform.adaptivesearch.data.AbstractAsItemConfiguration;
import de.hybris.platform.adaptivesearch.data.AsConfigurationHolder;
import de.hybris.platform.adaptivesearch.data.AsDocumentData;
import de.hybris.platform.adaptivesearch.data.AsPromotedItem;
import de.hybris.platform.adaptivesearch.data.AsSearchProfileResult;
import de.hybris.platform.adaptivesearch.data.AsSearchResultData;
import de.hybris.platform.adaptivesearch.model.AbstractAsSearchConfigurationModel;
import de.hybris.platform.adaptivesearchbackoffice.data.SearchResultData;
import de.hybris.platform.adaptivesearchbackoffice.widgets.searchresultbrowser.DocumentModel;
import de.hybris.platform.adaptivesearchbackoffice.widgets.searchresultbrowser.SearchResultBrowserViewModel;
import de.hybris.platform.core.PK;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.Iterator;
import java.util.Optional;

import org.apache.commons.collections.CollectionUtils;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.select.annotation.WireVariable;


/**
 * Changes the logic of OOTB de.hybris.platform.adaptivesearchbackoffice.widgets.searchresultbrowser.SearchResultBrowserViewModel
 * controller to get base product pk from returned variant product
 */
public class WileySearchResultBrowserViewModel extends SearchResultBrowserViewModel
{
	@WireVariable
	private ModelService modelService;

	@Init
	public void init()
	{
		setPageSize(20);
	}

	protected void populateResults(final SearchResultData searchResult)
	{
		setResultActionsEnabled(false);
		getPromotedItems().clear();
		getDefaultResults().clear();
		if (this.canPopulateResults(searchResult))
		{
			AsSearchResultData asSearchResult = searchResult.getAsSearchResult();
			AsSearchProfileResult searchProfileResult = asSearchResult.getSearchProfileResult();
			Optional<AbstractAsSearchConfigurationModel> searchConfiguration = this.resolveSearchConfiguration(
					searchResult);
			setResultActionsEnabled(searchConfiguration.isPresent());
			int index = 0;

			for (Iterator var7 = asSearchResult.getResults().iterator(); var7.hasNext(); ++index)
			{
				AsDocumentData document = (AsDocumentData) var7.next();
				Float score = this.extractScoreFromDocument(document);
				PK pk = this.extractPkFromDocument(document);
				Object o = modelService.get(pk);
				if (o instanceof VariantProductModel)
				{
					pk = ((VariantProductModel) o).getBaseProduct().getPk();
				}
				AsConfigurationHolder<AsPromotedItem, AbstractAsBoostItemConfiguration>
						promotedItemHolder = (AsConfigurationHolder) searchProfileResult.getPromotedItems().get(pk);
				boolean promoted = this.isPromoted(asSearchResult, promotedItemHolder);
				String promotedItemUid =
						promoted ? ((AsPromotedItem) promotedItemHolder.getConfiguration()).getUid() : null;
				boolean highlighted = this.isHighlighted(asSearchResult, promotedItemHolder);
				boolean showOnTop = this.isShowOnTop(asSearchResult, promotedItemHolder);
				boolean fromSearchProfile = promoted && this.isConfigurationFromSearchProfile(
						(AbstractAsItemConfiguration) promotedItemHolder.getConfiguration(),
						searchResult.getNavigationContext());
				boolean fromSearchConfiguration =
						promoted && searchConfiguration.isPresent() && this.isConfigurationFromSearchConfiguration(
								(AbstractAsItemConfiguration) promotedItemHolder.getConfiguration(),
								(AbstractAsSearchConfigurationModel) searchConfiguration.get());
				boolean override = fromSearchConfiguration && CollectionUtils.isNotEmpty(
						promotedItemHolder.getReplacedConfigurations());
				boolean overrideFromSearchProfile = override && this.isConfigurationFromSearchProfile(
						(AbstractAsItemConfiguration) promotedItemHolder.getReplacedConfigurations().get(0),
						searchResult.getNavigationContext());
				DocumentModel result = new DocumentModel();
				result.setIndex(index);
				result.setScore(score);
				result.setPk(pk);
				result.setDocument(document);
				result.setPromoted(promoted);
				result.setHighlight(highlighted);
				result.setShowOnTop(showOnTop);
				result.setPromotedItemUid(promotedItemUid);
				result.setFromSearchProfile(fromSearchProfile);
				result.setFromSearchConfiguration(fromSearchConfiguration);
				result.setCurrent(fromSearchConfiguration);
				result.setOverride(override);
				result.setOverrideFromSearchProfile(overrideFromSearchProfile);
				result.setStyleClass(this.buildResultStyleClass(result));
				result.setActionsEnabled(isResultActionsEnabled());
				if (result.isShowOnTop())
				{
					getPromotedItems().add(result);
				}
				else
				{
					getDefaultResults().add(result);
				}
			}

		}
	}

}
