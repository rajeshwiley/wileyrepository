package com.wiley.widgets.data;

import java.io.Serializable;


/**
 * @author Dzmitryi_Halahayeu
 */
public class CountryInternalData implements Serializable
{
	private String name;
	private String isocode;

	public String getName()
	{
		return name;
	}

	public void setName(final String name)
	{
		this.name = name;
	}

	public String getIsocode()
	{
		return isocode;
	}

	public void setIsocode(final String isocode)
	{
		this.isocode = isocode;
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}

		final CountryInternalData that = (CountryInternalData) o;

		if (name != null ? !name.equals(that.name) : that.name != null)
		{
			return false;
		}
		return isocode != null ? isocode.equals(that.isocode) : that.isocode == null;
	}

	@Override
	public int hashCode()
	{
		int result = name != null ? name.hashCode() : 0;
		result = 31 * result + (isocode != null ? isocode.hashCode() : 0);
		return result;
	}
}
