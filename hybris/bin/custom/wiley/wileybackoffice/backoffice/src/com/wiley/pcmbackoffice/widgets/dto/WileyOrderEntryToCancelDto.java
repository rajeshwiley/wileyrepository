package com.wiley.pcmbackoffice.widgets.dto;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.omsbackoffice.dto.OrderEntryToCancelDto;

import java.util.List;


public class WileyOrderEntryToCancelDto extends OrderEntryToCancelDto
{
	public WileyOrderEntryToCancelDto(final AbstractOrderEntryModel orderEntry,
			final List<String> reasons, final Long quantityAvailableToCancel, final String deliveryModeName)
	{
		super(orderEntry, reasons, quantityAvailableToCancel, deliveryModeName);
	}

	@Override
	public int compareTo(final OrderEntryToCancelDto orderEntryToCancelDto)
	{
		return this.getOrderEntry().getProduct().getCode().compareTo(
				orderEntryToCancelDto.getOrderEntry().getProduct().getCode());
	}
}
