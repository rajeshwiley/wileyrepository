package com.wiley.widgets.searchadapters.conditions.products;

import de.hybris.platform.category.model.CategoryModel;

import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Required;

import com.hybris.backoffice.navigation.NavigationNode;
import com.hybris.backoffice.widgets.advancedsearch.impl.AdvancedSearchData;
import com.hybris.backoffice.widgets.searchadapters.conditions.SearchConditionAdapter;
import com.hybris.cockpitng.core.config.impl.jaxb.hybris.advancedsearch.FieldType;
import com.hybris.cockpitng.search.data.ValueComparisonOperator;


public class SupercategoriesConditionAdapter extends SearchConditionAdapter
{
	private String categoryPropertyName;
	private ValueComparisonOperator operator;

	public SupercategoriesConditionAdapter()
	{
	}

	public boolean canHandle(NavigationNode node)
	{
		return node.getData() instanceof CategoryModel;
	}

	public void addSearchCondition(AdvancedSearchData searchData, NavigationNode node)
	{
		CategoryModel category = (CategoryModel) node.getData();
		FieldType fieldType = createFieldType(categoryPropertyName);
		searchData.addCondition(fieldType, operator, category.getCode());
	}

	protected static FieldType createFieldType(String propertyName)
	{
		FieldType fieldType = new FieldType();

		fieldType.setDisabled(Boolean.FALSE);
		fieldType.setSelected(Boolean.TRUE);
		fieldType.setName(propertyName);

		return fieldType;
	}

	protected Stream<CategoryModel> appendToStream(Stream<CategoryModel> stream, CategoryModel categoryToAdd)
	{
		return Stream.concat(stream, Stream.of(categoryToAdd));
	}

	@Required
	public void setCategoryPropertyName(String categoryPropertyName)
	{
		this.categoryPropertyName = categoryPropertyName;
	}

	@Required
	public void setOperator(ValueComparisonOperator operator)
	{
		this.operator = operator;
	}
}