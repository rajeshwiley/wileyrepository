package com.wiley.widgets.controller;

import de.hybris.platform.adaptivesearch.strategies.AsSearchProvider;
import de.hybris.platform.adaptivesearchbackoffice.widgets.navigationcontext.CatalogVersionModel;
import de.hybris.platform.adaptivesearchbackoffice.widgets.navigationcontext.NavigationContextController;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.site.BaseSiteService;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.zkoss.zk.ui.event.SelectEvent;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Comboitem;

import com.hybris.cockpitng.annotations.ViewEvent;


public class WileyNavigationContextController extends NavigationContextController
{
	@WireVariable
	private transient BaseSiteService baseSiteService;

	@Override
	@ViewEvent(
			componentID = "indexConfigurationSelector",
			eventName = "onSelect"
	)
	public void onIndexConfigurationChanged(final SelectEvent<Comboitem, String> event)
	{
		String facetSearchConfigName = (String) ((Comboitem) event.getReference()).getValue();

		// get base site for index configuration
		Collection<BaseSiteModel> allBaseSites = baseSiteService.getAllBaseSites();

		for (BaseSiteModel baseSiteModel : allBaseSites)
		{
			if (baseSiteModel.getSolrFacetSearchConfiguration() != null && baseSiteModel.getSolrFacetSearchConfiguration()
					.getName().equals(facetSearchConfigName))
			{
				baseSiteService.setCurrentBaseSite(baseSiteModel, false);
			}
		}

		super.onIndexConfigurationChanged(event);
	}

	//OOTB code
	//changed sorting of the catalogVersions
	@Override
	protected List<CatalogVersionModel> findCatalogVersions(final String indexConfiguration,
			final String indexType)
	{
		if (indexConfiguration != null && indexType != null)
		{
			AsSearchProvider searchProvider = this.asSearchProviderFactory.getSearchProvider();
			List<de.hybris.platform.catalog.model.CatalogVersionModel> catalogVersions =
					searchProvider.getSupportedCatalogVersions(indexConfiguration,
							indexType);
			return catalogVersions.stream().filter(this::isValidCatalogVersion).sorted(
					Comparator.comparing(de.hybris.platform.catalog.model.CatalogVersionModel::getItemtype)).map(
					this::convertCatalogVersion).collect(Collectors.toList());
		}
		return Collections.emptyList();
	}
}
