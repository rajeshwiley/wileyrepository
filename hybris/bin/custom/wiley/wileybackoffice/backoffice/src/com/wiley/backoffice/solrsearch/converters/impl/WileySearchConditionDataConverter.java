package com.wiley.backoffice.solrsearch.converters.impl;

import de.hybris.platform.solrfacetsearch.enums.SolrPropertiesTypes;

import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Required;

import com.hybris.backoffice.solrsearch.converters.impl.DefaultSearchConditionDataConverter;
import com.hybris.backoffice.solrsearch.dataaccess.SolrSearchCondition;
import com.hybris.cockpitng.search.data.ValueComparisonOperator;


/**
 * OOTB copy of DefaultSearchConditionDataConverter
 */
public class WileySearchConditionDataConverter extends DefaultSearchConditionDataConverter
{
	private Set<ValueComparisonOperator> fqApplicableOperators;
	private Set<SolrPropertiesTypes> fqApplicablePropertiesTypes;

	protected boolean isFQApplicableCondition(SolrSearchCondition condition)
	{
		if (condition.isFilterQueryCondition())
		{
			return true;
		}
		else if (!condition.isNestedCondition())
		{
			//ECSC-29258: The only customization: making an attribute type uppercased to make valueOf() to work correctly.
			String attributeType = condition.getAttributeType();
			SolrPropertiesTypes type = SolrPropertiesTypes.valueOf(
					attributeType == null ? null : attributeType.toUpperCase());

			Optional<SolrSearchCondition.ConditionValue> anyNotEqualsOperator = condition.getConditionValues().stream().filter(
					(cv) -> {
						return !fqApplicableOperators.contains(cv.getComparisonOperator());
					}).findAny();

			return !anyNotEqualsOperator.isPresent() && fqApplicablePropertiesTypes.contains(type);
		}
		else
		{
			return false;
		}
	}

	@Required
	public void setFqApplicableOperators(Set<ValueComparisonOperator> fqApplicableOperators)
	{
		this.fqApplicableOperators = fqApplicableOperators;
		super.setFqApplicableOperators(fqApplicableOperators);
	}

	@Required
	public void setFqApplicablePropertiesTypes(Set<SolrPropertiesTypes> fqApplicablePropertiesTypes)
	{
		this.fqApplicablePropertiesTypes = fqApplicablePropertiesTypes;
		super.setFqApplicablePropertiesTypes(fqApplicablePropertiesTypes);
	}
}