package com.wiley.pcmbackoffice.dto;

import de.hybris.platform.basecommerce.enums.RefundReason;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;

import java.util.List;
import java.util.Objects;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.zkoss.zul.ListModelArray;


public class OrderEntryToRefundDto implements Comparable<OrderEntryToRefundDto>
{
	private AbstractOrderEntryModel orderEntry;
	private Long expectedQuantity;
	private String refundOrderEntryComment;
	private RefundReason selectedReason;
	private ListModelArray<String> refundReasonsModel;

	public OrderEntryToRefundDto(AbstractOrderEntryModel orderEntry, List<String> reasons, Long expectedQuantity)
	{
		this.orderEntry = orderEntry;
		this.expectedQuantity = expectedQuantity;
		this.refundReasonsModel = new ListModelArray(reasons);
	}

	public int compareTo(OrderEntryToRefundDto orderEntryToCancelDto)
	{
		return Objects.compare(
				getOrderEntry().getProduct().getCode(),
				orderEntryToCancelDto.getOrderEntry().getProduct().getCode(),
				String::compareTo);
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o)
		{
			return true;
		}

		if (o == null || getClass() != o.getClass())
		{
			return false;
		}

		OrderEntryToRefundDto that = (OrderEntryToRefundDto) o;

		return new EqualsBuilder()
				.append(orderEntry, that.orderEntry)
				.append(expectedQuantity, that.expectedQuantity)
				.append(refundOrderEntryComment, that.refundOrderEntryComment)
				.append(selectedReason, that.selectedReason)
				.append(refundReasonsModel, that.refundReasonsModel)
				.isEquals();
	}

	@Override
	public int hashCode()
	{
		return new HashCodeBuilder(17, 37)
				.append(orderEntry)
				.append(expectedQuantity)
				.append(refundOrderEntryComment)
				.append(selectedReason)
				.append(refundReasonsModel)
				.toHashCode();
	}

	public void setCancelReasonsModel(ListModelArray<String> cancelReasonsModel)
	{
		this.refundReasonsModel = cancelReasonsModel;
	}

	public AbstractOrderEntryModel getOrderEntry()
	{
		return this.orderEntry;
	}

	public void setOrderEntry(AbstractOrderEntryModel orderEntry)
	{
		this.orderEntry = orderEntry;
	}

	public RefundReason getSelectedReason()
	{
		return this.selectedReason;
	}

	public void setSelectedReason(RefundReason selectedReason)
	{
		this.selectedReason = selectedReason;
	}

	public ListModelArray<String> getRefundReasonsModel()
	{
		return this.refundReasonsModel;
	}

	public String getRefundOrderEntryComment()
	{
		return this.refundOrderEntryComment;
	}

	public void setRefundOrderEntryComment(String refundOrderEntryComment)
	{
		this.refundOrderEntryComment = refundOrderEntryComment;
	}

	public Long getExpectedQuantity()
	{
		return this.expectedQuantity;
	}

	public void setExpectedQuantity(Long expectedQuantity)
	{
		this.expectedQuantity = expectedQuantity;
	}
}
