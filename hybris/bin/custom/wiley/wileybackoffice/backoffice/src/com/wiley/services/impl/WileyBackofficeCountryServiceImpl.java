
/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 */
package com.wiley.services.impl;


import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.i18n.WileyCommonI18NService;
import com.wiley.core.servicelayer.i18n.WileycomI18NService;
import com.wiley.services.WileyBackofficeCountryService;
import com.wiley.widgets.data.CountryInternalData;

import static com.wiley.core.constants.WileyCoreConstants.CURRENT_COUNTRY_SESSION_ATTR;


public class WileyBackofficeCountryServiceImpl implements WileyBackofficeCountryService
{
	private WileycomI18NService wileycomI18NService;
	private CommonI18NService commonI18NService;

	private SessionService sessionService;

	private WileyCommonI18NService wileyCommonI18NService;

	@Override
	public CountryInternalData getCurrentCountry()
	{
		final CountryModel countryModel = wileycomI18NService.getCurrentCountry()
				.orElseGet(() -> setDefaultCurrentCountry());
		return convertCountry(countryModel);
	}

	public CountryModel setDefaultCurrentCountry()
	{
		final CountryModel defaultCountry = wileycomI18NService.getDefaultCountry();

		sessionService.setAttribute(CURRENT_COUNTRY_SESSION_ATTR,
				commonI18NService.getCountry(defaultCountry.getIsocode()));

		return defaultCountry;
	}


	@Override
	public List<CountryInternalData> getAllCountries()
	{
		return commonI18NService.getAllCountries().stream().map(this::convertCountry).sorted(
				Comparator.comparing(CountryInternalData::getName)).collect(
				Collectors.toList());
	}

	@Override
	public List<CountryInternalData> getAllCountries(final BaseStoreModel baseStoreModel)
	{
		return baseStoreModel.getDeliveryCountries().stream().map(this::convertCountry).sorted(
				Comparator.comparing(CountryInternalData::getName)).collect(
				Collectors.toList());
	}


	private CountryInternalData convertCountry(final CountryModel countryModel)
	{
		final CountryInternalData countryInternalData = new CountryInternalData();
		countryInternalData.setName(countryModel.getName());
		countryInternalData.setIsocode(countryModel.getIsocode());
		return countryInternalData;
	}

	@Override
	public void setCurrentCountry(final CountryModel country, final BaseStoreModel baseStoreModel)
	{
		sessionService.setAttribute(CURRENT_COUNTRY_SESSION_ATTR, country);
		commonI18NService.setCurrentCurrency(wileyCommonI18NService.getDefaultCurrency(country, baseStoreModel));
	}

	@Required
	public void setWileycomI18NService(final WileycomI18NService wileycomI18NService)
	{
		this.wileycomI18NService = wileycomI18NService;
	}

	@Required
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	@Required
	public void setWileyCommonI18NService(final WileyCommonI18NService wileyCommonI18NService)
	{
		this.wileyCommonI18NService = wileyCommonI18NService;
	}
}
