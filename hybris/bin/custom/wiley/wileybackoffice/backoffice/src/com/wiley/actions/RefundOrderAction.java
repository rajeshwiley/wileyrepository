package com.wiley.actions;

import de.hybris.platform.core.model.order.OrderModel;

import javax.annotation.Resource;

import com.hybris.cockpitng.actions.ActionContext;
import com.hybris.cockpitng.actions.ActionResult;
import com.hybris.cockpitng.actions.CockpitAction;
import com.hybris.cockpitng.engine.impl.AbstractComponentWidgetAdapterAware;
import com.wiley.core.refund.WileyRefundService;

import static com.wiley.constants.WileybackofficeConstants.SOCKET_OUT_CONTEXT;


public class RefundOrderAction extends AbstractComponentWidgetAdapterAware
		implements CockpitAction<OrderModel, OrderModel>
{
	@Resource
	private WileyRefundService wileyRefundService;

	@Override
	public ActionResult<OrderModel> perform(final ActionContext<OrderModel> actionContext)
	{
		this.sendOutput(SOCKET_OUT_CONTEXT, actionContext.getData());
		return new ActionResult<>(ActionResult.SUCCESS);
	}

	@Override
	public boolean canPerform(final ActionContext<OrderModel> actionContext)
	{
		return wileyRefundService.isEligibleForRefund(actionContext.getData());
	}
}