package com.wiley.widgets.controller;

import de.hybris.platform.adaptivesearchbackoffice.data.NavigationContextData;
import de.hybris.platform.adaptivesearchbackoffice.data.SearchContextData;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.SelectEvent;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.ListModelList;

import com.hybris.cockpitng.annotations.SocketEvent;
import com.hybris.cockpitng.annotations.ViewEvent;
import com.hybris.cockpitng.util.DefaultWidgetController;
import com.wiley.core.store.WileyBaseStoreService;
import com.wiley.services.WileyBackofficeCountryService;
import com.wiley.widgets.data.CountryInternalData;


public class WileySearchContextController extends DefaultWidgetController
{
	public static final String DEFAULT_COUNTRY_ISO_CODE = "US";
	protected static final String NAVIGATION_CONTEXT_KEY = "navigationContext";
	protected static final String SEARCH_CONTEXT_KEY = "searchContext";
	private final ListModelList<CountryInternalData> countriesModel = new ListModelList();
	protected Combobox countrySelector;
	@WireVariable
	protected transient SessionService sessionService;
	@WireVariable
	private WileyBackofficeCountryService wileyBackofficeCountryService;
	@WireVariable
	private WileyBaseStoreService wileyBaseStoreService;
	@WireVariable
	private transient BaseSiteService baseSiteService;
	@WireVariable
	private transient CommonI18NService commonI18NService;

	public ListModelList<CountryInternalData> getCountriesModel()
	{
		return countriesModel;
	}

	public NavigationContextData getNavigationContext()
	{
		return this.getModel().getValue(NAVIGATION_CONTEXT_KEY, NavigationContextData.class);
	}

	public void setNavigationContext(final NavigationContextData navigationContext)
	{
		this.getModel().put(NAVIGATION_CONTEXT_KEY, navigationContext);
	}

	public SearchContextData getSearchContext()
	{
		return this.getModel().getValue(SEARCH_CONTEXT_KEY, SearchContextData.class);
	}

	public void setSearchContext(final SearchContextData searchContext)
	{
		this.getModel().put(SEARCH_CONTEXT_KEY, searchContext);
	}

	@SocketEvent(
			socketId = NAVIGATION_CONTEXT_KEY
	)
	public void updateSearchContext(final NavigationContextData navigationContextData)
	{
		this.setNavigationContext(navigationContextData);
		this.updateSelectors(this.getSearchContext());
		this.countrySelector.setDisabled(false);
		selectDefaultCountry();
		setCurrencyAndLanguageFromCountryAndSendSearchContext(DEFAULT_COUNTRY_ISO_CODE, true);
	}

	private void selectDefaultCountry()
	{
		Optional<CountryInternalData> defaultCountry = findDefaultCountry();
		countriesModel.setSelection(
				defaultCountry.isPresent() ? Collections.singleton(defaultCountry.get()) : Collections.emptyList());
	}

	private Optional<CountryInternalData> findDefaultCountry()
	{
		return countriesModel.stream().filter(
				country -> DEFAULT_COUNTRY_ISO_CODE.equals(country.getIsocode())).findFirst();
	}

	public void initialize(final Component component)
	{
		this.initializeSelectors();
		component.addEventListener("onCreate", (event) -> {
			SearchContextData searchContext = this.getSearchContext();
			this.updateSelectors(searchContext);
			this.sendSearchContext(searchContext);
		});
	}

	protected void initializeSelectors()
	{
		SearchContextData searchContext = new SearchContextData();
		searchContext.setCurrency(null);
		searchContext.setLanguage(null);
		this.setSearchContext(searchContext);
		this.countrySelector.setModel(this.countriesModel);
		this.countrySelector.setDisabled(true);
	}

	protected void sendSearchContext(final SearchContextData searchContext)
	{
		if (searchContext != null)
		{
			SearchContextData clonedSearchContext = new SearchContextData();
			clonedSearchContext.setLanguage(searchContext.getLanguage());
			clonedSearchContext.setCurrency(searchContext.getCurrency());
			this.sendOutput(SEARCH_CONTEXT_KEY, clonedSearchContext);
		}
		else
		{
			this.sendOutput(SEARCH_CONTEXT_KEY, null);
		}

	}

	protected void updateSelectors(final SearchContextData searchContext)
	{
		this.sessionService.executeInLocalView(new SessionExecutionBody()
		{
			public void executeWithoutResult()
			{
				BaseSiteModel baseSiteModel = baseSiteService.getCurrentBaseSite();
				List<CountryInternalData> allCountries;
				if (Objects.nonNull(baseSiteModel))
				{
					BaseStoreModel currentBaseStoreModel = wileyBaseStoreService.acquireBaseStoreByBaseSite(
							baseSiteModel);
					allCountries = wileyBackofficeCountryService.getAllCountries(currentBaseStoreModel);
				}
				else
				{
					allCountries = wileyBackofficeCountryService.getAllCountries();
				}
				countriesModel.clear();
				countriesModel.addAll(allCountries);
			}
		});
	}

	@ViewEvent(
			componentID = "countrySelector",
			eventName = "onSelect"
	)
	public void onCountryChanged(final SelectEvent<Comboitem, String> event)
	{
		String selectedCountryIso = (String) event.getReference().getValue();

		setCurrencyAndLanguageFromCountryAndSendSearchContext(selectedCountryIso, false);
	}

	private void setCurrencyAndLanguageFromCountryAndSendSearchContext(final String countryIso,
			final boolean forceSendSearchContext)
	{
		BaseSiteModel currentBaseSite = baseSiteService.getCurrentBaseSite();
		if (currentBaseSite == null)
		{
			return;
		}
		SearchContextData searchContext = this.getSearchContext();
		BaseStoreModel currentBaseStoreModel = wileyBaseStoreService.acquireBaseStoreByBaseSite(currentBaseSite);
		final CountryModel country = commonI18NService.getCountry(countryIso);
		wileyBackofficeCountryService.setCurrentCountry(country, currentBaseStoreModel);

		String languageIso = country.getDefaultLanguage().getIsocode();
		String currencyIso = commonI18NService.getCurrentCurrency().getIsocode();

		if (forceSendSearchContext || searchContextChanged(searchContext, languageIso, currencyIso))
		{
			setLanguageAndCurrencyAndSendSearchContext(searchContext, languageIso, currencyIso);
		}

	}

	private void setLanguageAndCurrencyAndSendSearchContext(final SearchContextData searchContext, final String languageIso,
			final String currencyIso)
	{
		searchContext.setCurrency(currencyIso);
		searchContext.setLanguage(languageIso);
		this.sendSearchContext(searchContext);
	}

	private boolean searchContextChanged(final SearchContextData searchContext, final String languageIso,
			final String currencyIso)
	{
		return searchContext != null && (!Objects.equals(searchContext.getLanguage(), languageIso) || !Objects.equals(
				searchContext.getCurrency(), currencyIso));
	}
}
