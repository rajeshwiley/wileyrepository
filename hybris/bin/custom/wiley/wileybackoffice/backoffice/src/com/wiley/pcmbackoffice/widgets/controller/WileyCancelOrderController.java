package com.wiley.pcmbackoffice.widgets.controller;

import de.hybris.platform.basecommerce.enums.CancelReason;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.ordercancel.OrderCancelEntry;
import de.hybris.platform.ordercancel.OrderCancelException;
import de.hybris.platform.ordercancel.OrderCancelRequest;
import de.hybris.platform.ordercancel.OrderCancelService;
import de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CancellationException;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.InputEvent;
import org.zkoss.zk.ui.event.SelectEvent;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Column;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Row;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.impl.InputElement;

import com.hybris.backoffice.i18n.BackofficeLocaleService;
import com.hybris.backoffice.widgets.notificationarea.NotificationService;
import com.hybris.backoffice.widgets.notificationarea.event.NotificationEvent.Level;
import com.hybris.cockpitng.annotations.SocketEvent;
import com.hybris.cockpitng.annotations.ViewEvent;
import com.hybris.cockpitng.core.events.CockpitEventQueue;
import com.hybris.cockpitng.core.events.impl.DefaultCockpitEvent;
import com.hybris.cockpitng.util.DefaultWidgetController;
import com.wiley.core.enums.OrderType;
import com.wiley.pcmbackoffice.widgets.dto.WileyOrderEntryToCancelDto;


/**
 * Based on OOTB CancelOrderController
 * OrderEntryToCancelDto changed on WileyOrderEntryCancelDto to fix bug with compareTo
 */
public class WileyCancelOrderController extends DefaultWidgetController
{
	private static final Logger LOGGER = LoggerFactory.getLogger(WileyCancelOrderController.class);
	private static final long serialVersionUID = 1L;
	private static final String IN_SOCKET = "inputObject";
	private static final String CONFIRM_ID = "confirmcancellation";
	private static final Object COMPLETED = "completed";
	private static final String CUSTOMERSUPPORTBACKOFFICE_CANCELORDER_PICKUP = "customersupportbackoffice.cancelorder.pickup";
	private static final String FIRST_COLUMN_WIDTH = "50px";
	private static final String CUSTOMERSUPPORTBACKOFFICE_CANCELORDER_CONFIRM_TITLE =
			"customersupportbackoffice.cancelorder.confirm.title";
	private static final String CUSTOMERSUPPORTBACKOFFICE_CANCELORDER_CONFIRM_ERROR =
			"customersupportbackoffice.cancelorder.confirm.error";
	private static final String CUSTOMERSUPPORTBACKOFFICE_CANCELORDER_CONFIRM_MSG =
			"customersupportbackoffice.cancelorder.confirm.msg";
	private static final String CUSTOMERSUPPORTBACKOFFICE_CANCELORDER_ERROR_QTYCANCELLED_INVALID =
			"customersupportbackoffice.cancelorder.error.qtycancelled.invalid";
	private static final String CUSTOMERSUPPORTBACKOFFICE_CANCELORDER_MISSING_QUANTITY =
			"customersupportbackoffice.cancelorder.missing.quantity";
	private static final String CUSTOMERSUPPORTBACKOFFICE_CANCELORDER_ERROR_REASON =
			"customersupportbackoffice.cancelorder.error.reason";
	private static final String CUSTOMERSUPPORTBACKOFFICE_CANCELORDER_MISSING_SELECTED_LINE =
			"customersupportbackoffice.cancelorder.missing.selectedLine";
	private static final String CANCELORDER_CONFIRM_ICON = "oms-widget-cancelorder-confirm-icon";
	private static final String CAPTURE_PAYMENT_ON_CONSIGNMENT = "warehousing.capturepaymentonconsignment";
	private static final int COLUMN_INDEX_PENDING_QUANTITY = 4;
	private static final int COLUMN_INDEX_CANCEL_QUANTITY = 5;
	private static final int COLUMN_INDEX_CANCEL_REASON = 6;
	private static final int COLUMN_INDEX_CANCEL_COMMENT = 7;
	private static final String JUST_MESSAGE = "JustMessage";
	private static final String PARTIAL_PRE_ORDER_CANCELLATION_IS_NOT_ALLOWED = "Partial pre-order cancellation is not allowed";
	private final List<String> cancelReasons = new ArrayList();
	private transient Map<AbstractOrderEntryModel, Long> orderCancellableEntries;
	private transient Set<WileyOrderEntryToCancelDto> orderEntriesToCancel;
	private OrderModel orderModel;
	@Wire
	private Textbox orderNumber;
	@Wire
	private Textbox customerName;
	@Wire
	private Combobox globalCancelReasons;
	@Wire
	private Textbox globalCancelComment;
	@Wire
	private Grid orderEntries;
	@Wire
	private Checkbox globalCancelEntriesSelection;
	@WireVariable
	private transient BackofficeLocaleService cockpitLocaleService;
	@WireVariable("wileyOrderCancelService")
	private transient OrderCancelService orderCancelService;
	@WireVariable
	private transient EnumerationService enumerationService;
	@WireVariable
	private transient ModelService modelService;
	@WireVariable
	private transient CockpitEventQueue cockpitEventQueue;
	@WireVariable
	private transient UserService userService;
	@WireVariable
	private transient List<ConsignmentStatus> notCancellableConsignmentStatus;
	@WireVariable
	private transient NotificationService notificationService;
	@WireVariable
	private transient ConfigurationService configurationService;

	@ViewEvent(
			componentID = CONFIRM_ID,
			eventName = "onClick"
	)
	public void confirmCancellation()
	{
		if (this.validateRequest())
		{
			this.showMessageBox();
		}
	}

	private boolean isFullOrderCancellation()
	{
		return getOrderEntriesGridRows().stream().allMatch(
				row ->
				{
					InputElement cancelQty = (InputElement) row.getChildren().get(COLUMN_INDEX_CANCEL_QUANTITY);
					Label pendingQty = (Label) row.getChildren().get(COLUMN_INDEX_PENDING_QUANTITY);
					return pendingQty.getValue().equals(String.valueOf(cancelQty.getRawValue())) &&
							((Checkbox) row.getChildren().iterator().next()).isChecked();
				});
	}

	@SocketEvent(
			socketId = IN_SOCKET
	)
	public void initCancellationOrderForm(OrderModel inputObject)
	{
		this.cancelReasons.clear();
		this.globalCancelEntriesSelection.setChecked(false);
		this.setOrderModel(inputObject);
		this.getWidgetInstanceManager().setTitle(
				this.getWidgetInstanceManager().getLabel(CUSTOMERSUPPORTBACKOFFICE_CANCELORDER_CONFIRM_TITLE) + " " + this
						.getOrderModel().getCode());
		this.orderNumber.setValue(this.getOrderModel().getCode());
		this.customerName.setValue(this.getOrderModel().getUser().getDisplayName());
		Locale locale = this.getLocale();
		this.getEnumerationService().getEnumerationValues(CancelReason.class).forEach(
				reason -> this.cancelReasons.add(this.getEnumerationService().getEnumerationName(reason, locale)));
		this.globalCancelReasons.setModel(new ListModelArray(this.cancelReasons));
		this.orderEntriesToCancel = new HashSet();
		this.orderCancellableEntries = this.getOrderCancelService().getAllCancelableEntries(this.getOrderModel(),
				this.getUserService().getCurrentUser());
		if (!this.orderCancellableEntries.isEmpty())
		{
			if (this.getConfigurationService().getConfiguration().getBoolean(CAPTURE_PAYMENT_ON_CONSIGNMENT,
					Boolean.FALSE))
			{
				Map<Integer, Long> consignmentSums = this.getOrderModel().getConsignments().stream().filter(
						c -> !this.getNotCancellableConsignmentStatus().contains(c.getStatus())).map(
						ConsignmentModel::getConsignmentEntries).flatMap(Collection::stream).collect(
						Collectors
								.toMap(o -> o.getOrderEntry().getEntryNumber(), ConsignmentEntryModel::getQuantity, Long::sum));
				this.orderCancellableEntries.forEach((entry, cancellableQty) -> {
					if (consignmentSums.get(entry.getEntryNumber()) != null)
					{
						this.orderEntriesToCancel.add(new WileyOrderEntryToCancelDto(entry, this.cancelReasons,
								consignmentSums.get(entry.getEntryNumber()),
								this.determineDeliveryMode(entry)));
					}

				});
			}
			else
			{
				this.orderCancellableEntries.forEach((entry, cancellableQty) -> this.orderEntriesToCancel
						.add(new WileyOrderEntryToCancelDto(entry, this.cancelReasons, cancellableQty,
								this.determineDeliveryMode(entry)))
				);
			}
		}

		this.getOrderEntries().setModel(new ListModelList(this.orderEntriesToCancel));

		//Updating width of first column as a workaround for OOTB bug, ECSC-29710.
		Column firstColumn = (Column) getOrderEntries().getColumns().getChildren().get(0);
		firstColumn.setWidth(FIRST_COLUMN_WIDTH);

		this.getOrderEntries().renderAll();
		this.addListeners();
	}

	private String determineDeliveryMode(AbstractOrderEntryModel orderEntry)
	{
		String deliveryModeResult;
		if (orderEntry.getDeliveryMode() != null)
		{
			deliveryModeResult = orderEntry.getDeliveryMode().getName();
		}
		else if (orderEntry.getDeliveryPointOfService() != null)
		{
			deliveryModeResult = this.getLabel(CUSTOMERSUPPORTBACKOFFICE_CANCELORDER_PICKUP);
		}
		else
		{
			deliveryModeResult = orderEntry.getOrder().getDeliveryMode() != null ?
					(orderEntry.getOrder().getDeliveryMode().getName() != null ?
							orderEntry.getOrder().getDeliveryMode().getName() :
							orderEntry.getOrder().getDeliveryMode().getCode()) :
					null;
		}

		return deliveryModeResult;
	}

	@ViewEvent(
			componentID = "undocancellation",
			eventName = "onClick"
	)
	public void reset()
	{
		this.globalCancelReasons.setSelectedItem(null);
		this.globalCancelComment.setValue("");
		this.initCancellationOrderForm(this.getOrderModel());
	}

	private void addListeners()
	{
		List<Component> rows = this.getOrderEntries().getRows().getChildren();

		for (final Component row : rows)
		{

			for (final Component myComponent : row.getChildren())
			{
				if (myComponent instanceof Checkbox)
				{
					myComponent.addEventListener("onCheck", event -> this.handleRow((Row) event.getTarget().getParent()));
				}
				else if (myComponent instanceof Combobox)
				{
					myComponent.addEventListener("onCustomChange",
							event -> Events.echoEvent("onLaterCustomChange", myComponent, event.getData()));
					myComponent.addEventListener("onLaterCustomChange", event -> {
						Clients.clearWrongValue(myComponent);
						myComponent.invalidate();
						this.handleIndividualCancelReason(event);
					});
				}
				else if (myComponent instanceof Intbox)
				{
					myComponent.addEventListener("onChange", event -> {
						this.autoSelect(event);
						((WileyOrderEntryToCancelDto) ((Row) event.getTarget().getParent()).getValue()).setQuantityToCancel(
								Long.valueOf(((InputEvent) event).getValue()));
					});
				}
				else if (myComponent instanceof Textbox)
				{
					myComponent.addEventListener("onChanging", event -> {
						this.autoSelect(event);
						((WileyOrderEntryToCancelDto) ((Row) event.getTarget().getParent()).getValue())
								.setCancelOrderEntryComment(((InputEvent) event).getValue());
					});
				}
			}
		}

		this.globalCancelReasons.addEventListener("onSelect", this::handleGlobalCancelReason);
		this.globalCancelComment.addEventListener("onChanging", this::handleGlobalCancelComment);
		this.globalCancelEntriesSelection.addEventListener("onCheck", event -> this.selectAllEntries());
	}

	private void applyToGrid(Object data, int childrenIndex)
	{
		this.getOrderEntriesGridRows().stream().filter(entry -> ((Checkbox) entry.getChildren().iterator().next()).isChecked())
				.forEach(entry -> this.applyToRow(data, childrenIndex, entry));
	}

	private void applyToRow(Object data, int childrenIndex, Component row)
	{
		int index = 0;

		for (Iterator iterator = row.getChildren().iterator(); iterator.hasNext(); ++index)
		{
			Component myComponent = (Component) iterator.next();
			if (index == childrenIndex)
			{
				if (myComponent instanceof Checkbox && data != null)
				{
					((Checkbox) myComponent).setChecked((Boolean) data);
				}
				else if (myComponent instanceof Combobox)
				{
					if (data == null)
					{
						((Combobox) myComponent).setSelectedItem(null);
					}
					else
					{
						((Combobox) myComponent).setSelectedIndex((Integer) data);
					}
				}
				else if (myComponent instanceof Intbox)
				{
					((Intbox) myComponent).setValue((Integer) data);
				}
				else if (myComponent instanceof Textbox)
				{
					((Textbox) myComponent).setValue((String) data);
				}
			}
		}

	}

	private void autoSelect(Event event)
	{
		((Checkbox) event.getTarget().getParent().getChildren().iterator().next()).setChecked(true);
	}

	private OrderCancelRequest buildCancelRequest()
	{
		if (this.getOrderModel() != null)
		{
			List<OrderCancelEntry> orderCancelEntries = new ArrayList();
			this.getOrderEntriesGridRows().stream().filter(entry -> ((Checkbox) entry.getFirstChild()).isChecked()).forEach(
					entry -> this.createOrderCancelEntry(orderCancelEntries, ((Row) entry).getValue()));
			OrderCancelRequest orderCancelRequest = new OrderCancelRequest(this.getOrderModel(), orderCancelEntries);
			orderCancelRequest.setCancelReason(
					this.matchingComboboxCancelReason(this.globalCancelReasons.getValue()).orElse(null));
			orderCancelRequest.setNotes(this.globalCancelComment.getValue());
			return orderCancelRequest;
		}
		else
		{
			return null;
		}
	}

	private void createOrderCancelEntry(List<OrderCancelEntry> orderCancelEntries, Object entry)
	{
		WileyOrderEntryToCancelDto orderEntryToCancel = (WileyOrderEntryToCancelDto) entry;
		OrderCancelEntry orderCancelEntry = new OrderCancelEntry(orderEntryToCancel.getOrderEntry(),
				orderEntryToCancel.getQuantityToCancel(), orderEntryToCancel.getCancelOrderEntryComment(),
				orderEntryToCancel.getSelectedReason());
		orderCancelEntries.add(orderCancelEntry);
	}

	private int getReasonIndex(CancelReason cancelReason)
	{
		int index = 0;
		String myReason = this.getEnumerationService().getEnumerationName(cancelReason,
				this.getCockpitLocaleService().getCurrentLocale());

		for (Iterator var5 = this.cancelReasons.iterator(); var5.hasNext(); ++index)
		{
			String reason = (String) var5.next();
			if (myReason.equals(reason))
			{
				break;
			}
		}

		return index;
	}

	private Optional<CancelReason> getSelectedCancelReason(Event event)
	{
		Optional<CancelReason> result = Optional.empty();
		if (!((SelectEvent) event).getSelectedItems().isEmpty())
		{
			Object selectedValue = ((Comboitem) ((SelectEvent) event).getSelectedItems().iterator().next()).getValue();
			result = this.matchingComboboxCancelReason(selectedValue.toString());
		}

		return result;
	}

	private void handleGlobalCancelComment(Event event)
	{
		this.applyToGrid(((InputEvent) event).getValue(), COLUMN_INDEX_CANCEL_COMMENT);
		this.getOrderEntriesGridRows().stream().filter(entry -> ((Checkbox) entry.getChildren().iterator().next()).isChecked())
				.forEach(entry -> {
					WileyOrderEntryToCancelDto myEntry = ((Row) entry).getValue();
					myEntry.setCancelOrderEntryComment(((InputEvent) event).getValue());
				});
	}

	private void handleGlobalCancelReason(Event event)
	{
		Optional<CancelReason> cancelReason = this.getSelectedCancelReason(event);
		if (cancelReason.isPresent())
		{
			this.applyToGrid(this.getReasonIndex(cancelReason.get()), COLUMN_INDEX_CANCEL_REASON);
			this.getOrderEntriesGridRows().stream().filter(
					entry -> ((Checkbox) entry.getChildren().iterator().next()).isChecked()).forEach(entry -> {
				WileyOrderEntryToCancelDto myEntry = ((Row) entry).getValue();
				myEntry.setSelectedReason(cancelReason.get());
			});
		}

	}

	private void handleIndividualCancelReason(Event event)
	{
		Optional<CancelReason> cancelReason = this.getCustomSelectedCancelReason(event);
		if (cancelReason.isPresent())
		{
			this.autoSelect(event);
			((WileyOrderEntryToCancelDto) ((Row) event.getTarget().getParent()).getValue()).setSelectedReason(
					cancelReason.get());
		}

	}

	private void handleRow(Row row)
	{
		WileyOrderEntryToCancelDto myEntry = row.getValue();
		if (!((Checkbox) row.getChildren().iterator().next()).isChecked())
		{
			this.applyToRow(0, COLUMN_INDEX_CANCEL_QUANTITY, row);
			this.applyToRow(null, COLUMN_INDEX_CANCEL_REASON, row);
			this.applyToRow(null, COLUMN_INDEX_CANCEL_COMMENT, row);
			myEntry.setQuantityToCancel(0L);
			myEntry.setSelectedReason(null);
			myEntry.setCancelOrderEntryComment(null);
		}
		else
		{
			this.applyToRow(this.globalCancelReasons.getSelectedIndex(), COLUMN_INDEX_CANCEL_REASON, row);
			this.applyToRow(this.globalCancelComment.getValue(), COLUMN_INDEX_CANCEL_COMMENT, row);
			Optional<CancelReason> reason = this.matchingComboboxCancelReason(this.globalCancelReasons.getSelectedItem() != null ?
					this.globalCancelReasons.getSelectedItem().getLabel() :
					null);

			int cancellableQuantity = getCancellableQuantity(row);
			myEntry.setQuantityToCancel((long) cancellableQuantity);
			this.applyToRow(cancellableQuantity, COLUMN_INDEX_CANCEL_QUANTITY, row);
			myEntry.setSelectedReason(reason.orElse(null));
			myEntry.setCancelOrderEntryComment(this.globalCancelComment.getValue());
		}

	}

	private int getCancellableQuantity(final Row row)
	{
		int cancellableQuantity = 0;
		String pendingQuantityValue = ((Label) row.getChildren().get(COLUMN_INDEX_PENDING_QUANTITY)).getValue();
		if (!StringUtils.isEmpty(pendingQuantityValue))
		{
			cancellableQuantity = Integer.parseInt(pendingQuantityValue);
		}
		return cancellableQuantity;
	}

	private Optional<CancelReason> getCustomSelectedCancelReason(Event event)
	{
		Optional<CancelReason> reason = Optional.empty();
		if (event.getTarget() instanceof Combobox)
		{
			Object selectedValue = event.getData();
			reason = this.matchingComboboxCancelReason(selectedValue.toString());
		}

		return reason;
	}

	private Optional<CancelReason> matchingComboboxCancelReason(String cancelReasonLabel)
	{
		return this.getEnumerationService().getEnumerationValues(CancelReason.class).stream().filter(
				reason -> this.getEnumerationService().getEnumerationName(reason, this.getLocale()).equals(cancelReasonLabel))
				.findFirst();
	}

	private void processCancellation(Event obj)
	{
		if (Button.YES.event.equals(obj.getName()))
		{
			try
			{
				OrderCancelRecordEntryModel orderCancelRecordEntry = this.getOrderCancelService().requestOrderCancel(
						this.buildCancelRequest(), this.getUserService().getCurrentUser());
				switch (orderCancelRecordEntry.getCancelResult().ordinal())
				{
					case 1:
					case 2:
						this.getNotificationService().notifyUser("", JUST_MESSAGE, Level.SUCCESS,
								this.getLabel("customersupportbackoffice.cancelorder.confirm.success"));
						break;
					case 3:
						this.getNotificationService().notifyUser("", JUST_MESSAGE, Level.FAILURE,
								this.getLabel(CUSTOMERSUPPORTBACKOFFICE_CANCELORDER_CONFIRM_ERROR));
				}
			}
			catch (OrderCancelException | CancellationException var3)
			{
				LOGGER.info(var3.getMessage(), var3);
				this.getNotificationService().notifyUser("", JUST_MESSAGE, Level.FAILURE,
						this.getLabel(CUSTOMERSUPPORTBACKOFFICE_CANCELORDER_CONFIRM_ERROR));
			}

			OrderModel object = this.getModelService().get(this.getOrderModel().getPk());
			object.getEntries().forEach(entry -> this.getCockpitEventQueue()
					.publishEvent(new DefaultCockpitEvent("objectsUpdated", entry, (Object) null)));
			this.sendOutput(CONFIRM_ID, COMPLETED);
		}

	}

	private void selectAllEntries()
	{
		this.applyToGrid(Boolean.TRUE, 0);

		for (final Component row : this.getOrderEntriesGridRows())
		{
			Component firstComponent = row.getChildren().iterator().next();
			if (firstComponent instanceof Checkbox)
			{
				((Checkbox) firstComponent).setChecked(this.globalCancelEntriesSelection.isChecked());
			}

			this.handleRow((Row) row);
			if (this.globalCancelEntriesSelection.isChecked())
			{
				int cancellableQuantity = Integer.parseInt(((Label) row.getChildren().get(COLUMN_INDEX_PENDING_QUANTITY))
						.getValue());
				this.applyToRow(cancellableQuantity, COLUMN_INDEX_CANCEL_QUANTITY, row);
			}
		}

		if (this.globalCancelEntriesSelection.isChecked())
		{
			this.orderEntriesToCancel.forEach(
					entry -> entry.setQuantityToCancel(this.orderCancellableEntries.get(entry.getOrderEntry())));
		}

	}

	private void showMessageBox()
	{
		Messagebox.show(this.getLabel(CUSTOMERSUPPORTBACKOFFICE_CANCELORDER_CONFIRM_MSG),
				this.getLabel(CUSTOMERSUPPORTBACKOFFICE_CANCELORDER_CONFIRM_TITLE) + " " + this.getOrderModel().getCode(),
				new Button[] { Button.NO, Button.YES }, CANCELORDER_CONFIRM_ICON,
				this::processCancellation);
	}

	private Component targetFieldToApplyValidation(String stringToValidate, int indexLabelToCheck, int indexTargetComponent)
	{

		for (final Component component : this.getOrderEntriesGridRows())
		{
			Label label = (Label) component.getChildren().get(indexLabelToCheck);
			if (label.getValue().equals(stringToValidate))
			{
				return component.getChildren().get(indexTargetComponent);
			}
		}

		return null;
	}

	private void validateOrderEntry(WileyOrderEntryToCancelDto entry)
	{
		InputElement quantity;
		if (entry.getQuantityToCancel() > this.orderCancellableEntries.get(entry.getOrderEntry()))
		{
			quantity = (InputElement) this.targetFieldToApplyValidation(entry.getOrderEntry().getProduct().getCode(), 1,
					COLUMN_INDEX_CANCEL_QUANTITY);
			throw new WrongValueException(quantity,
					this.getLabel(CUSTOMERSUPPORTBACKOFFICE_CANCELORDER_ERROR_QTYCANCELLED_INVALID));
		}
		else if (entry.getSelectedReason() != null && entry.getQuantityToCancel() == 0L)
		{
			quantity = (InputElement) this.targetFieldToApplyValidation(entry.getOrderEntry().getProduct().getCode(), 1,
					COLUMN_INDEX_CANCEL_QUANTITY);
			throw new WrongValueException(quantity,
					this.getLabel(CUSTOMERSUPPORTBACKOFFICE_CANCELORDER_MISSING_QUANTITY));
		}
		else if (entry.getSelectedReason() == null && entry.getQuantityToCancel() > 0L)
		{
			Combobox reason = (Combobox) this.targetFieldToApplyValidation(entry.getOrderEntry().getProduct().getCode(), 1,
					COLUMN_INDEX_CANCEL_REASON);
			throw new WrongValueException(reason, this.getLabel(CUSTOMERSUPPORTBACKOFFICE_CANCELORDER_ERROR_REASON));
		}
	}

	private boolean validateRequest()
	{
		for (final Component row : this.getOrderEntriesGridRows())
		{
			if (((Checkbox) row.getChildren().iterator().next()).isChecked())
			{
				InputElement cancelQty = (InputElement) row.getChildren().get(COLUMN_INDEX_CANCEL_QUANTITY);
				if (cancelQty.getRawValue().equals(0))
				{
					throw new WrongValueException(cancelQty,
							this.getLabel(CUSTOMERSUPPORTBACKOFFICE_CANCELORDER_MISSING_QUANTITY));
				}
			}
		}

		ListModelList<WileyOrderEntryToCancelDto> modelList = (ListModelList) this.getOrderEntries().getModel();
		if (modelList.stream().allMatch(entry -> entry.getQuantityToCancel() == 0L))
		{
			throw new WrongValueException(this.globalCancelEntriesSelection,
					this.getLabel(CUSTOMERSUPPORTBACKOFFICE_CANCELORDER_MISSING_SELECTED_LINE));
		}
		else
		{
			modelList.forEach(this::validateOrderEntry);
		}

		if (OrderType.PRE_ORDER.equals(getOrderModel().getOrderType()) && !isFullOrderCancellation())
		{
			Messagebox.show(PARTIAL_PRE_ORDER_CANCELLATION_IS_NOT_ALLOWED, "", Messagebox.OK, Messagebox.ERROR);
			return false;
		}
		return true;
	}

	private List<Component> getOrderEntriesGridRows()
	{
		return this.getOrderEntries().getRows().getChildren();
	}

	private Locale getLocale()
	{
		return this.getCockpitLocaleService().getCurrentLocale();
	}

	private BackofficeLocaleService getCockpitLocaleService()
	{
		return this.cockpitLocaleService;
	}

	private Grid getOrderEntries()
	{
		return this.orderEntries;
	}

	private OrderModel getOrderModel()
	{
		return this.orderModel;
	}

	public void setOrderModel(OrderModel orderModel)
	{
		this.orderModel = orderModel;
	}

	private OrderCancelService getOrderCancelService()
	{
		return this.orderCancelService;
	}

	private EnumerationService getEnumerationService()
	{
		return this.enumerationService;
	}

	private ModelService getModelService()
	{
		return this.modelService;
	}

	private CockpitEventQueue getCockpitEventQueue()
	{
		return this.cockpitEventQueue;
	}

	private UserService getUserService()
	{
		return this.userService;
	}

	private ConfigurationService getConfigurationService()
	{
		return this.configurationService;
	}

	private List<ConsignmentStatus> getNotCancellableConsignmentStatus()
	{
		return this.notCancellableConsignmentStatus;
	}

	private NotificationService getNotificationService()
	{
		return this.notificationService;
	}
}

