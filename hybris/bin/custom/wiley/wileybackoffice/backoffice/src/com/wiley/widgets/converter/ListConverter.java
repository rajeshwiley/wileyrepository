package com.wiley.widgets.converter;

import de.hybris.platform.adaptivesearchbackoffice.common.HTMLSanitizer;

import java.util.List;

import org.zkoss.bind.BindContext;
import org.zkoss.bind.Converter;
import org.zkoss.zk.ui.Component;


public class ListConverter implements Converter
{
	@Override
	public Object coerceToUi(final Object o, final Component component, final BindContext bindContext)
	{
		final List<String> list = (List<String>) o;
		return list == null ? null : HTMLSanitizer.sanitizeHTML(String.join(", ", list));
	}

	@Override
	public Object coerceToBean(final Object o, final Component component, final BindContext bindContext)
	{
		final List<String> list = (List<String>) o;
		return list == null ? null : HTMLSanitizer.sanitizeHTML(String.join(", ", list));
	}
}
