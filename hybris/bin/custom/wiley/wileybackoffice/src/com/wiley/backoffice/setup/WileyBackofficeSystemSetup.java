package com.wiley.backoffice.setup;

import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;
import de.hybris.platform.core.initialization.SystemSetupParameterMethod;

import java.util.ArrayList;
import java.util.List;

import com.wiley.constants.WileybackofficeConstants;


@SystemSetup(extension = WileybackofficeConstants.EXTENSIONNAME)
public class WileyBackofficeSystemSetup extends AbstractSystemSetup
{
	private static final String IMPORT_SAMPLE_DATA = "importSampleData";

	/**
	 * Generates the Dropdown and Multi-select boxes for the project data import
	 *
	 * @return the initialization options
	 */
	@Override
	@SystemSetupParameterMethod
	public List<SystemSetupParameter> getInitializationOptions()
	{
		final List<SystemSetupParameter> params = new ArrayList<SystemSetupParameter>();

		// Parameters to choose what kind of data should be updated
		params.add(createBooleanSystemSetupParameter(IMPORT_SAMPLE_DATA, "Import Sample Data", false));
		// Add more Parameters here as you require

		return params;
	}

	@SystemSetup(extension = WileybackofficeConstants.EXTENSIONNAME, type = SystemSetup.Type.ESSENTIAL,
			process = SystemSetup.Process.ALL)
	public void createEssentialData(final SystemSetupContext context)
	{
		importImpexFile(context, "/wileybackoffice/essentialdata/wileycustomersupportmanagergroup.impex");
	}

	@SystemSetup(extension = WileybackofficeConstants.EXTENSIONNAME, type = SystemSetup.Type.PROJECT, process = SystemSetup.Process.INIT)
	public void createProjectData(final SystemSetupContext context)
	{
		if (getBooleanSystemSetupParameter(context, IMPORT_SAMPLE_DATA))
		{
			importImpexFile(context, "/wileybackoffice/sampledata/wileycustomersupportmanageruser.impex");
		}

		importImpexFile(context, "/wileybackoffice/projectdata/ECSC-27958-disable-backoffice-solr-search.impex");
		importImpexFile(context, "/wileybackoffice/projectdata/ECSC-29542-optimize-solr-backoffice-category-search.impex");
	}

}
