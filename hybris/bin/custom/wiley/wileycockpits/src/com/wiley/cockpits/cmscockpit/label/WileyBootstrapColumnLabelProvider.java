package com.wiley.cockpits.cmscockpit.label;

import de.hybris.platform.cockpit.services.label.AbstractModelLabelProvider;

import com.wiley.core.model.components.bootstrapgrid.WileyBootstrapColumnModel;


public class WileyBootstrapColumnLabelProvider extends AbstractModelLabelProvider<WileyBootstrapColumnModel>
{
	@Override
	protected String getItemLabel(final WileyBootstrapColumnModel model)
	{
		final int componentsCount = model.getComponents() == null ? 0 : model.getComponents().size();
		return String.format("Column [%d component(s)]", componentsCount);
	}

	@Override
	protected String getItemLabel(final WileyBootstrapColumnModel model, final String languageIso)
	{
		return getItemLabel(model);
	}

	@Override
	protected String getItemDescription(final WileyBootstrapColumnModel wileyBootstrapColumnModel)
	{
		return "";
	}

	@Override
	protected String getItemDescription(final WileyBootstrapColumnModel wileyBootstrapColumnModel, final String s)
	{
		return "";
	}

	@Override
	protected String getIconPath(final WileyBootstrapColumnModel wileyBootstrapColumnModel)
	{
		return null;
	}

	@Override
	protected String getIconPath(final WileyBootstrapColumnModel wileyBootstrapColumnModel, final String s)
	{
		return null;
	}
}