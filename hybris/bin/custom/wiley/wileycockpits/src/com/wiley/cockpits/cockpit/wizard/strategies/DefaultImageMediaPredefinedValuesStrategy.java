/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.cockpits.cockpit.wizard.strategies;

import de.hybris.platform.cockpit.session.impl.CreateContext;
import de.hybris.platform.cockpit.wizards.generic.strategies.PredefinedValuesStrategy;
import de.hybris.platform.core.model.media.MediaFolderModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.media.MediaService;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Required;


/**
 * The type Default image media predefined values strategy.
 */
public class DefaultImageMediaPredefinedValuesStrategy implements PredefinedValuesStrategy
{
	private MediaService mediaService;
	private String mediaFolderName;

	/**
	 * Gets predefined values.
	 *
	 * @param paramCreateContext
	 * 		the param create context
	 * @return the predefined values
	 */
	@Override
	public Map<String, Object> getPredefinedValues(final CreateContext paramCreateContext)
	{
		final Map<String, Object> ret = new HashMap<String, Object>();

		final MediaFolderModel mediaFolder = findMediaFolder();
		if (mediaFolder != null)
		{
			ret.put(MediaModel._TYPECODE + "." + MediaModel.FOLDER, mediaFolder);
		}
		return ret;
	}

	/**
	 * Find media folder media folder model.
	 *
	 * @return the media folder model
	 */
	protected MediaFolderModel findMediaFolder()
	{
		return getMediaService().getFolder(getMediaFolderName());
	}

	/**
	 * Gets media service.
	 *
	 * @return the media service
	 */
	protected MediaService getMediaService()
	{
		return mediaService;
	}

	/**
	 * Sets media service.
	 *
	 * @param mediaService
	 * 		the media service
	 */
	@Required
	public void setMediaService(final MediaService mediaService)
	{
		this.mediaService = mediaService;
	}

	/**
	 * Gets media folder name.
	 *
	 * @return the media folder name
	 */
	protected String getMediaFolderName()
	{
		return mediaFolderName;
	}

	/**
	 * Sets media folder name.
	 *
	 * @param mediaFolderName
	 * 		the media folder name
	 */
	@Required
	public void setMediaFolderName(final String mediaFolderName)
	{
		this.mediaFolderName = mediaFolderName;
	}
}
