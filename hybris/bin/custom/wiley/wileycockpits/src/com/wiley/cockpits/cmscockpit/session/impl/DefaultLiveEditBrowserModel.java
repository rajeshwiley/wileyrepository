/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.cockpits.cmscockpit.session.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cms2.misc.UrlUtils;
import de.hybris.platform.cms2.model.preview.PreviewDataModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.admin.CMSAdminSiteService;
import de.hybris.platform.cmscockpit.components.liveedit.impl.DefaultLiveEditViewModel;
import de.hybris.platform.cmscockpit.events.impl.CmsUrlChangeEvent;
import de.hybris.platform.cmscockpit.session.impl.LiveEditBrowserModel;
import de.hybris.platform.cockpit.components.contentbrowser.AbstractContentBrowser;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.session.UISessionUtils;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.zkoss.spring.SpringUtil;
import org.zkoss.zk.ui.Executions;


/**
 * Represents a browser area model for <p>Live Edit Perspective</p>
 */
public class DefaultLiveEditBrowserModel extends LiveEditBrowserModel
{
	private static final Logger LOG = Logger.getLogger(DefaultLiveEditBrowserModel.class);

	private DefaultLiveEditViewModel viewModel;
	private String currentUrl = StringUtils.EMPTY;
	private CMSSiteModel activeSite;
	private UserModel frontendUser;
	private String frontendSessionId;
	private String relatedPagePk;

	//flags
	private boolean previewDataActive = false;

	//services
	private CMSAdminSiteService adminSiteService;
	private I18NService i18nService;
	private ModelService modelService;
	private CatalogVersionModel actiaveCatalogVersion;
	private UserService userService;

	/**
	 * Blacklist items.
	 *
	 * @param indexes
	 * 		the indexes
	 */
	@Override
	public void blacklistItems(final Collection<Integer> indexes)
	{
		// do nothing
	}

	/**
	 * Clear preview page if any.
	 */
	@Override
	public void clearPreviewPageIfAny()
	{
		final PreviewDataModel previewData = this.getViewModel().getCurrentPreviewData();
		if (previewData != null)
		{
			previewData.setPage(null);
			getModelService().save(previewData);
		}
		else
		{
			if (LOG.isDebugEnabled())
			{
				LOG.debug("Cannot retrieve current preview mode!");
			}
		}
	}

	/**
	 * Sets all frontend attributes transferred to WCMSCockpit.
	 *
	 * @param cmsUrlChangeEvent
	 * 		the cms url change event
	 */
	@Override
	public void setFrontendAttributes(final CmsUrlChangeEvent cmsUrlChangeEvent)
	{
		setCurrentUrl(cmsUrlChangeEvent.getUrl());
		setRelatedPagePk(cmsUrlChangeEvent.getRelatedPagePk());
		setFrontendUser(retriveCurrentFrontendUser(cmsUrlChangeEvent.getFrontendUserUid()));
		setFrontentSessionId(cmsUrlChangeEvent.getJaloSessionUid());
	}


	/**
	 * Clone object.
	 *
	 * @return the object
	 * @throws CloneNotSupportedException
	 * 		the clone not supported exception
	 */
	@Override
	public Object clone() throws CloneNotSupportedException //NOSONAR
	{
		// Isn't intended to be use here.
		return null;
	}

	/**
	 * Collapse.
	 */
	@Override
	public void collapse()
	{
		// Isn't intended to be use here.
	}


	/**
	 * Create view component abstract content browser.
	 *
	 * @return the abstract content browser
	 */
	@Override
	public AbstractContentBrowser createViewComponent()
	{
		return new DefaultLiveEditContentBrowser();
	}

	/**
	 * Fire mode change.
	 *
	 * @param content
	 * 		the content
	 */
	@Override
	public void fireModeChange(final AbstractContentBrowser content)
	{
		DefaultLiveEditContentBrowser currentContentBrowser = null;
		if (content instanceof DefaultLiveEditContentBrowser)
		{
			currentContentBrowser = (DefaultLiveEditContentBrowser) content;
			currentContentBrowser.fireModeChanged();
		}
	}

	/**
	 * Fire toggle preview data mode.
	 *
	 * @param contentBrowser
	 * 		the content browser
	 */
	public void fireTogglePreviewDataMode(final DefaultLiveEditContentBrowser contentBrowser)
	{
		previewDataActive = !previewDataActive;

		contentBrowser.firePreviewDataModeChanged();

	}

	/**
	 * Fire toggle preview data mode.
	 *
	 * @param contentBrowser
	 * 		the content browser
	 * @param previewSectionActive
	 * 		the preview section active
	 */
	public void fireTogglePreviewDataMode(final DefaultLiveEditContentBrowser contentBrowser, final boolean previewSectionActive)
	{
		previewDataActive = previewSectionActive;
		contentBrowser.firePreviewDataModeChanged();
	}

	/**
	 * Gets actiave catalog version.
	 *
	 * @return the actiave catalog version
	 */
	@Override
	public CatalogVersionModel getActiaveCatalogVersion()
	{
		return actiaveCatalogVersion;
	}

	/**
	 * Gets active site.
	 *
	 * @return the active site
	 */
	@Override
	public CMSSiteModel getActiveSite()
	{
		return activeSite;
	}

	/**
	 * Gets cms admin site service.
	 *
	 * @return the cms admin site service
	 */
	@Override
	protected CMSAdminSiteService getCMSAdminSiteService()
	{
		if (this.adminSiteService == null)
		{
			this.adminSiteService = (CMSAdminSiteService) SpringUtil.getBean("cmsAdminSiteService");
		}
		return this.adminSiteService;
	}

	/**
	 * Gets current url.
	 *
	 * @return the current url
	 */
	@Override
	public String getCurrentUrl()
	{
		return currentUrl;
	}

	/**
	 * Gets frontend user.
	 *
	 * @return the frontend user
	 */
	@Override
	public UserModel getFrontendUser()
	{
		return frontendUser;
	}

	/**
	 * Retrieves internationalization service
	 *
	 * @return internationalization service
	 */
	protected I18NService getI18NService()
	{
		if (this.i18nService == null)
		{
			this.i18nService = (I18NService) SpringUtil.getBean("i18nService");
		}
		return this.i18nService;
	}

	/**
	 * Gets item.
	 *
	 * @param index
	 * 		the index
	 * @return the item
	 */
	@Override
	public TypedObject getItem(final int index)
	{
		// Isn't intended to be use here.
		return null;
	}

	/**
	 * Gets items.
	 *
	 * @return the items
	 */
	@Override
	public List<TypedObject> getItems()
	{
		// Isn't intended to be use here.
		return null; //NOSONAR
	}

	/**
	 * Gets label.
	 *
	 * @return the label
	 */
	@Override
	public String getLabel()
	{
		return "Live Edit Browser";
	}

	/**
	 * Retrieves model service
	 *
	 * @return model service
	 */
	protected ModelService getModelService()
	{
		if (this.modelService == null)
		{
			this.modelService = (ModelService) SpringUtil.getBean("modelService");
		}
		return this.modelService;
	}

	/**
	 * Gets preview data.
	 *
	 * @return the preview data
	 */
	@Override
	public PreviewDataModel getPreviewData()
	{
		PreviewDataModel previewData = this.getViewModel().getCurrentPreviewData();

		// create a new one if null or invalid
		if (previewData == null || UISessionUtils.getCurrentSession().getModelService().isRemoved(previewData))
		{
			previewData = UISessionUtils.getCurrentSession().getModelService().create(PreviewDataModel.class);
			previewData.setLanguage(getI18NService().getLanguage(UISessionUtils.getCurrentSession().getGlobalDataLanguageIso()));
			previewData.setTime(null);
			previewData.setUser(frontendUser);
			this.getViewModel().setCurrentPreviewData(previewData);
		}
		return previewData;
	}

	/**
	 * Gets view model.
	 *
	 * @return the view model
	 */
	@Override
	public DefaultLiveEditViewModel getViewModel()
	{
		if (this.viewModel == null)
		{
			this.viewModel = newDefaultLiveEditViewModel();
		}
		return this.viewModel;
	}

	/**
	 * New default live edit view model default live edit view model.
	 *
	 * @return the default live edit view model
	 */
	protected DefaultLiveEditViewModel newDefaultLiveEditViewModel()
	{
		return new DefaultLiveEditViewModel();
	}

	/**
	 * Is advanced header dropdown sticky boolean.
	 *
	 * @return the boolean
	 */
	@Override
	public boolean isAdvancedHeaderDropdownSticky()
	{
		return true;
	}

	/**
	 * Is advanced header dropdown visible boolean.
	 *
	 * @return the boolean
	 */
	@Override
	public boolean isAdvancedHeaderDropdownVisible()
	{
		return isPreviewDataVisible();
	}

	/**
	 * Is collapsed boolean.
	 *
	 * @return the boolean
	 */
	@Override
	public boolean isCollapsed()
	{
		// Isn't intended to be use here.
		return false;
	}

	/**
	 * Is duplicatable boolean.
	 *
	 * @return the boolean
	 */
	@Override
	public boolean isDuplicatable()
	{
		return false;
	}

	/**
	 * Is preview data visible boolean.
	 *
	 * @return the boolean
	 */
	@Override
	public boolean isPreviewDataVisible()
	{
		return previewDataActive;
	}

	/**
	 * On cms perpsective init event.
	 */
	@Override
	public void onCmsPerpsectiveInitEvent()
	{
		getCMSAdminSiteService().setActiveSite(getActiveSite());
		getCMSAdminSiteService().setActiveCatalogVersion(getActiaveCatalogVersion());

	}

	/**
	 * Refresh.
	 */
	@Override
	public void refresh()
	{
		getViewModel().clearPreviewInformation();
		updateItems();
	}

	/**
	 * Remove items.
	 *
	 * @param indexes
	 * 		the indexes
	 */
	@Override
	public void removeItems(final Collection<Integer> indexes)
	{
		// do nothing

	}

	/**
	 * Sets actiave catalog version.
	 *
	 * @param actiaveCatalogVersion
	 * 		the actiave catalog version
	 */
	@Override
	public void setActiaveCatalogVersion(final CatalogVersionModel actiaveCatalogVersion)
	{
		if (this.actiaveCatalogVersion == null || this.actiaveCatalogVersion != null && !this.actiaveCatalogVersion.equals(
				actiaveCatalogVersion))
		{
			this.actiaveCatalogVersion = actiaveCatalogVersion;
			this.viewModel.setWelcomePanelVisible(actiaveCatalogVersion == null);
		}
	}

	/**
	 * Sets active site.
	 *
	 * @param activeSite
	 * 		the active site
	 */
	@Override
	public void setActiveSite(final CMSSiteModel activeSite)
	{
		this.activeSite = activeSite;
	}

	/**
	 * Sets current site.
	 *
	 * @param site
	 * 		the site
	 */
	@Override
	public void setCurrentSite(final CMSSiteModel site)
	{
		getViewModel().setSite(site);
	}

	/**
	 * Sets current url.
	 *
	 * @param currentUrl
	 * 		the current url
	 */
	@Override
	public void setCurrentUrl(final String currentUrl)
	{
		final HttpServletRequest httpServletRequest = (HttpServletRequest) Executions.getCurrent().getNativeRequest();
		this.currentUrl = UrlUtils.extractHostInformationFromRequest(httpServletRequest, currentUrl);
		getViewModel().setCurrentUrl(this.currentUrl);
	}

	/**
	 * Sets frontend user.
	 *
	 * @param frontendUser
	 * 		the frontend user
	 */
	@Override
	public void setFrontendUser(final UserModel frontendUser)
	{
		this.frontendUser = frontendUser;

	}

	/**
	 * Retrive current frontend user user model.
	 *
	 * @param frontendUserUid
	 * 		the frontend user uid
	 * @return the user model
	 */
	@Override
	protected UserModel retriveCurrentFrontendUser(final String frontendUserUid)
	{
		UserModel ret = null;
		if (StringUtils.isNotEmpty(frontendUserUid))
		{
			SessionContext ctx = null;
			try
			{
				ctx = JaloSession.getCurrentSession().createLocalSessionContext();
				ctx.setAttribute("disableRestrictions", Boolean.TRUE);
				ret = getUserService().getUser(frontendUserUid);
			}
			finally
			{
				if (ctx != null)
				{
					JaloSession.getCurrentSession().removeLocalSessionContext();
				}
			}
		}
		return ret;
	}

	/**
	 * Gets user service.
	 *
	 * @return the user service
	 */
	@Override
	protected UserService getUserService()
	{
		if (this.userService == null)
		{
			this.userService = (UserService) SpringUtil.getBean("userService");
		}
		return this.userService;
	}

	/**
	 * Sets preview data.
	 *
	 * @param previewData
	 * 		the preview data
	 */
	@Override
	public void setPreviewData(final PreviewDataModel previewData)
	{
		this.getViewModel().setCurrentPreviewData(previewData);
	}

	/**
	 * Update items.
	 */
	@Override
	public void updateItems()
	{
		this.fireItemsChanged();
	}

	/**
	 * Gets frontent session id.
	 *
	 * @return the frontent session id
	 */
	@Override
	public String getFrontentSessionId()
	{
		return frontendSessionId;
	}

	/**
	 * Sets frontent session id.
	 *
	 * @param frontentSessionId
	 * 		the frontent session id
	 */
	@Override
	public void setFrontentSessionId(final String frontentSessionId)
	{
		this.frontendSessionId = frontentSessionId;
	}

	/**
	 * Gets related page pk.
	 *
	 * @return the related page pk
	 */
	@Override
	public String getRelatedPagePk()
	{
		return relatedPagePk;
	}

	/**
	 * Sets related page pk.
	 *
	 * @param relatedPagePk
	 * 		the related page pk
	 */
	@Override
	public void setRelatedPagePk(final String relatedPagePk)
	{
		this.relatedPagePk = relatedPagePk;
	}
}
