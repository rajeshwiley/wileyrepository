package com.wiley.cockpits.cmscockpit.components.contentbrowser;

import de.hybris.platform.cmscockpit.components.contentbrowser.CMSContentEditorInjector;
import de.hybris.platform.cmscockpit.components.contentbrowser.ComponentInjectorHelper;
import de.hybris.platform.cmscockpit.session.impl.CmsCockpitPerspective;
import de.hybris.platform.cockpit.events.impl.ItemChangedEvent;
import de.hybris.platform.cockpit.model.editor.AdditionalReferenceEditorListener;
import de.hybris.platform.cockpit.model.editor.EditorHelper;
import de.hybris.platform.cockpit.model.meta.ObjectTemplate;
import de.hybris.platform.cockpit.model.meta.PropertyDescriptor;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.services.config.UIConfigurationException;
import de.hybris.platform.cockpit.services.config.WizardConfiguration;
import de.hybris.platform.cockpit.services.values.ValueHandlerException;
import de.hybris.platform.cockpit.session.UICockpitPerspective;
import de.hybris.platform.cockpit.session.UISessionUtils;
import de.hybris.platform.cockpit.util.ViewUpdateUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Div;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Toolbarbutton;

import com.wiley.cockpits.cmscockpit.session.impl.WileyEditorRowRenderer;


/**
 * This code is mostly copy-pasted OOTB CMSContentEditorInjector with a few cosmetic refactorings
 * added showRemoveButton parameter support to avoid showing remove button.
 */
public class WileyCMSContentEditorInjector extends CMSContentEditorInjector
{
	private static final String PROMPT_BTN_CONFIRM = "Confirm";
	private static final Logger LOG = LoggerFactory.getLogger(WileyCMSContentEditorInjector.class);
	private static final String PARAM_SKIP_SHOW_REMOVE_BUTTON = "showRemoveButton";
	private static final String PROMPT_ARE_YOU_SURE = "Are you sure?";

	@Override
	protected void injectList(final HtmlBasedComponent parent, final Map<String, ?> params)
	{
		if (getItem() == null) {
			LOG.warn("Not injecting 'list'. Reason: No item available.");
		} else {
			List<PropertyDescriptor> propertyDescriptors = new ArrayList();
			Object object = params.get("value");
			if (StringUtils.isNotBlank((String) object)) {
				PropertyDescriptor propDescr = this.getTypeService().getPropertyDescriptor((String) object);
				if (propDescr != null) {
					propertyDescriptors.add(propDescr);
				}
			}

			if (propertyDescriptors.isEmpty()) {
				propertyDescriptors.addAll(
						ComponentInjectorHelper.getEditorPropertyDescriptors(this.getItem(),
								this.getTypeService(), this.getCMSAdminComponentService()));
			}

			Collections.sort(propertyDescriptors, this.getPropertyListComparator());

			for (final PropertyDescriptor descriptor : propertyDescriptors) {
				final Div edCnt = new Div();
				parent.appendChild(edCnt);
				this.renderProperty(descriptor, edCnt);
				ViewUpdateUtils.setUpdateCallback(edCnt, (component, ctx) ->
				{
					if (ViewUpdateUtils.isMatchingItemPropertyContext(
							ctx, WileyCMSContentEditorInjector.this.getItem(), descriptor)) {
						edCnt.getChildren().clear();
						WileyCMSContentEditorInjector.this.renderProperty(descriptor, edCnt);
						return false;
					} else {
						return true;
					}
				});
			}


		}
	}

	boolean showRemoveButton(final PropertyDescriptor propDescr) {
		String propDescrStr = propDescr.getQualifier();
		Map attributes = this.getConfig().getParameterMap(propDescrStr);
		Boolean rawShowRemoveButton = MapUtils.getBoolean(attributes, PARAM_SKIP_SHOW_REMOVE_BUTTON);
		return rawShowRemoveButton == null ? true : rawShowRemoveButton;
	}

	void renderProperty(final PropertyDescriptor propDescr, final HtmlBasedComponent parent) {
		String propDescrStr = propDescr.getQualifier();
		if (this.getConfig().isEditorVisible(propDescrStr)
				&& "REFERENCE".equals(propDescr.getEditorType()) && !propDescr.isLocalized()) {
			try {
				Object rawValue = this.getValueService().getValue(this.getItem(), propDescr);
				if (PropertyDescriptor.Multiplicity.SINGLE.equals(propDescr.getMultiplicity())) {

					if (rawValue instanceof TypedObject) {
						List<HtmlBasedComponent> captionComponenets = new ArrayList<>();
						if (showRemoveButton(propDescr)) {
							Toolbarbutton removeBtn = createRemoveButton(propDescr, event ->
							{
								if (Messagebox.show(PROMPT_ARE_YOU_SURE, PROMPT_BTN_CONFIRM, 48,
										"z-msgbox z-msgbox-question") == 16) {
									WileyCMSContentEditorInjector.this.setValue(propDescr, null);
									event.stopPropagation();
									final ItemChangedEvent cockpitEvent = new ItemChangedEvent(null,
											WileyCMSContentEditorInjector.this.getItem(), Collections.emptyList(),
											ItemChangedEvent.ChangeType.CHANGED);
									UISessionUtils.getCurrentSession().sendGlobalEvent(
											cockpitEvent);
								}

							});
							captionComponenets.add(removeBtn);
						} else
						{
							LOG.debug("Property [{}] is specified with custom flag [{}], check configuration",
									propDescrStr, PARAM_SKIP_SHOW_REMOVE_BUTTON);
						}
						this.injectReference((TypedObject) rawValue, parent, captionComponenets);


					} else if (rawValue != null) {
						LOG.warn("Reference value '" + propDescr + "' ignored. Reason: Expected: 'TypedObject' but found '"
								+ rawValue.getClass().getSimpleName() + "'.");
					}
				} else if (rawValue instanceof Collection) {
					Toolbarbutton removeBtn;
					if (this.isGroupCollections()) {
						final Div barDiv = new Div();
						Hbox horizontalContent = new Hbox();
						horizontalContent.setSclass("contentEditorGroupHeader");
						horizontalContent.setWidth("100%");
						horizontalContent.setWidths("100%,none");
						parent.appendChild(horizontalContent);
						barDiv.setSclass("contentEditorGroupHeaderLabelCnt");
						Div labelDiv = new Div();
						barDiv.appendChild(labelDiv);
						labelDiv.setSclass("contentEditorGroupHeaderLabel");
						String valueTypeCode = UISessionUtils.getCurrentSession().getTypeService().getValueTypeCode(propDescr);
						final ObjectTemplate template =
								UISessionUtils.getCurrentSession().getTypeService().getObjectTemplate(valueTypeCode);
						removeBtn = null;
						if (this.getUiAccessRightService()
								.isWritable(this.getItem().getType(), this.getItem(), propDescr, false)) {
							try {
								final WizardConfiguration wizardConfiguration = this.getConfigurationService()
										.getComponentConfiguration(template, "wizardConfig", WizardConfiguration.class);
								removeBtn = new Toolbarbutton("", "/cmscockpit/images/add_btn.gif");
								removeBtn.setTooltiptext(Labels.getLabel("cmscockpit.create_item"));
								removeBtn.setVisible(wizardConfiguration.isCreateMode() || wizardConfiguration.isSelectMode());
								removeBtn.addEventListener("onClick", event ->
								{
									if (UISessionUtils.getCurrentSession()
											.getCurrentPerspective() instanceof CmsCockpitPerspective) {
										WileyCMSContentEditorInjector.this.openCreateNewReferenceWizard(
												barDiv.getPage().getFirstRoot(), template,
												WileyCMSContentEditorInjector.this.getItem(), propDescr, wizardConfiguration);
									} else {
										LOG.warn("Can not create item. Reason: Current perspective not a CmsCockpitPerspective.");
									}

								});
							} catch (UIConfigurationException var13) {
								LOG.warn("A configuration is missing for template" + template.getCode() + "!", var13);
							}
						}

						String text = ComponentInjectorHelper.getPropertyLabel(propDescr);
						Label itemLabel = new Label(text);
						labelDiv.appendChild(itemLabel);
						horizontalContent.appendChild(barDiv);
						if (removeBtn != null) {
							horizontalContent.appendChild(removeBtn);
						}
					}

					final List<TypedObject> collValues = new ArrayList();
					collValues.addAll((Collection) rawValue);
					final Map<String, String> attributes = this.getConfig().getParameterMap(propDescrStr);
					if (StringUtils.isNotBlank(this.getConfig().getEditorCode(propDescrStr)) && !Boolean.TRUE.equals(
							MapUtils.getBoolean(attributes, "skipCustomEditor"))) {
						Map<String, Object> customParams = new HashMap();
						customParams.put("viewStatePersistenceProvider", this.getLocationInfoObject());
						AdditionalReferenceEditorListener additionalReferenceEditorListener = (item) -> activateItemInPopupEditor(
								attributes, item);
						customParams.put(AdditionalReferenceEditorListener.class.getName(), additionalReferenceEditorListener);
						customParams.putAll(attributes);
						EditorHelper.createEditor(this.getItem(), propDescr, parent, getValueContainer(), isAutoPersist(),
								this.getConfig().getEditorCode(propDescrStr), customParams);
					} else {
						Iterator var21 = ((Collection) rawValue).iterator();

						while (var21.hasNext()) {
							final Object element = var21.next();
							if (element instanceof TypedObject) {
								List<HtmlBasedComponent> captionComponents = new ArrayList();
								if (this.getUiAccessRightService().isWritable(this.getItem().getType(), this.getItem(),
										propDescr, false)) {
									if (showRemoveButton(propDescr)) {
										removeBtn = createRemoveButton(propDescr, event ->
										{
											if (Messagebox.show(PROMPT_ARE_YOU_SURE, "Confirm", 48,
													"z-msgbox z-msgbox-question") == Messagebox.YES) {
												if (collValues.remove(element)) {
													WileyCMSContentEditorInjector.this.setValue(propDescr, collValues,
															WileyCMSContentEditorInjector.this.isAutoPersist(), null);
												}

												event.stopPropagation();
											}

										});
										captionComponents.add(removeBtn);
									}
								}

								this.injectReference((TypedObject) element, parent, captionComponents);
							} else {
								LOG.error("Reference value '" + propDescr
										+ "' ignored. Reason: Expected: 'TypedObject' but found '"
										+ (element == null ? "null" : element.getClass().getSimpleName()) + "'.");
							}
						}
					}
				} else {
					LOG.error("Reference value '" + propDescr + "' ignored. Reason: Expected collection.");
				}
			} catch (ValueHandlerException var14) {
				LOG.error("Could not get value for property '" + propDescr + "', reason: ", var14);
			}
		}

	}
	
	private void activateItemInPopupEditor(final Map<String, String> attributes, final TypedObject item) 
	{
		final Boolean forceEditInPopup = MapUtils.getBoolean(attributes, WileyEditorRowRenderer.FORCE_EDIT_IN_POPUP);
		if (forceEditInPopup != null && forceEditInPopup && UISessionUtils.getCurrentSession() != null)
		{
			UICockpitPerspective currentPerspective = UISessionUtils.getCurrentSession().getCurrentPerspective();
			currentPerspective.activateItemInPopupEditor(item);
		}
  }

	private Toolbarbutton createRemoveButton(final PropertyDescriptor propDescr, final EventListener onClickListener)
	{
		Toolbarbutton removeBtn = new Toolbarbutton("", "/cmscockpit/images/cnt_elem_remove_action.png");
		removeBtn.setTooltiptext(Labels.getLabel("general.remove"));
		removeBtn.addEventListener("onClick", onClickListener);
		return removeBtn;
	}

	@Override
	protected void injectProperty(final HtmlBasedComponent parent, final Map<String, ?> params)
	{
		super.injectProperty(parent, params);
	}

}
