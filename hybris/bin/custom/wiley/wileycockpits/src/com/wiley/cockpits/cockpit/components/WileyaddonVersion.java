package com.wiley.cockpits.cockpit.components;

/**
 * ZK wiley addon version class
 */
public final class WileyaddonVersion
{
	public static final String UID = "1.0";

	private WileyaddonVersion()
	{
		//not called
	}
}
