package com.wiley.cockpits.b2badmincockpit.components.browserarea.impl;

import de.hybris.platform.cockpit.components.listview.AbstractListViewAction;
import de.hybris.platform.cockpit.components.listview.ListViewAction;
import de.hybris.platform.cockpit.wizards.Wizard;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Menupopup;

import com.wiley.cockpits.b2badmincockpit.components.browserarea.WileyTypeViewSpecificButtonAction;


public class WileyB2BCreateAdminUserButtonAction extends AbstractListViewAction implements WileyTypeViewSpecificButtonAction
{
	private List<String> enabledForTypes;

	@Autowired
	private UserService userService;

	@Override
	public List<String> getEnabledForTypes()
	{
		return enabledForTypes;
	}

	public void setEnabledForTypes(final List<String> enabledForTypes)
	{
		this.enabledForTypes = enabledForTypes;
	}

	protected boolean alwaysEnabled = true;

	public boolean isAlwaysEnabled()
	{
		return this.alwaysEnabled;
	}

	public void setAlwaysEnabled(boolean alwaysEnabled)
	{
		this.alwaysEnabled = true;
	}

	public String getImageURI(final ListViewAction.Context context)
	{
		return "cockpit/images/create_new_user.png";
	}

	public EventListener getMultiSelectEventListener(final ListViewAction.Context context)
	{
		Assert.notNull(context);
		return new EventListener()
		{
			public void onEvent(final Event event) throws Exception
			{
				if (!("onClick".equals(event.getName())))
				{
					return;
				}
				Wizard.show("createAdminUserWizard");
			}
		};
	}

	public EventListener getEventListener(final ListViewAction.Context context)
	{
		return null;
	}

	public String getMultiSelectImageURI(final ListViewAction.Context context)
	{
		UserModel currentuser = userService.getCurrentUser();
		if (userService.isAdmin(currentuser) && enabledForCurrentType(context))
		{
			return "cockpit/images/create_new_user.png";
		}
		return null;
	}

	public String getTooltip(final ListViewAction.Context context)
	{
		return Labels.getLabel("browserarea.item.createadminuser.action.tooltip");
	}

	@Override
	public Menupopup getPopup(final Context paramContext)
	{
		return null;
	}

	@Override
	public Menupopup getContextPopup(final Context paramContext)
	{
		return null;
	}

	@Override
	protected void doCreateContext(final Context arg0)
	{
	}
}