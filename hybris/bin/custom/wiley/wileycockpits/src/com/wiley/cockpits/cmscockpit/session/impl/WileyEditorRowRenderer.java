package com.wiley.cockpits.cmscockpit.session.impl;

import de.hybris.platform.cockpit.components.sectionpanel.SectionPanel;
import de.hybris.platform.cockpit.components.sectionpanel.SectionRow;
import de.hybris.platform.cockpit.events.impl.ItemChangedEvent;
import de.hybris.platform.cockpit.model.editor.AdditionalReferenceEditorListener;
import de.hybris.platform.cockpit.model.editor.EditorHelper;
import de.hybris.platform.cockpit.model.editor.EditorListener;
import de.hybris.platform.cockpit.model.editor.ListUIEditor;
import de.hybris.platform.cockpit.model.editor.ReferenceUIEditor;
import de.hybris.platform.cockpit.model.editor.UIEditor;
import de.hybris.platform.cockpit.model.meta.ObjectTemplate;
import de.hybris.platform.cockpit.model.meta.PropertyDescriptor;
import de.hybris.platform.cockpit.model.meta.PropertyEditorDescriptor;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.model.meta.impl.ClassAttributePropertyDescriptor;
import de.hybris.platform.cockpit.model.meta.impl.ItemAttributePropertyDescriptor;
import de.hybris.platform.cockpit.services.values.ObjectValueContainer;
import de.hybris.platform.cockpit.session.AdvancedBrowserModel;
import de.hybris.platform.cockpit.session.BrowserModel;
import de.hybris.platform.cockpit.session.EditorAreaListener;
import de.hybris.platform.cockpit.session.PopupEditorAreaController;
import de.hybris.platform.cockpit.session.UICockpitPerspective;
import de.hybris.platform.cockpit.session.UIEditorArea;
import de.hybris.platform.cockpit.session.UISessionUtils;
import de.hybris.platform.cockpit.session.impl.BaseUICockpitPerspective;
import de.hybris.platform.cockpit.session.impl.CreateContext;
import de.hybris.platform.cockpit.session.impl.EditorPropertyRow;
import de.hybris.platform.cockpit.session.impl.EditorRowRenderer;
import de.hybris.platform.cockpit.util.TypeTools;
import de.hybris.platform.cockpit.util.UITools;
import de.hybris.platform.core.model.type.AttributeDescriptorModel;
import de.hybris.platform.core.model.type.MapTypeModel;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zul.Label;


/**
 * Render updates editor area row in case of some field was chanaged
 */
public class WileyEditorRowRenderer extends EditorRowRenderer
{

	private static final Logger LOG = LoggerFactory.getLogger(WileyEditorRowRenderer.class);
	private static final String DEPENDENT_PROPERTY_TO_REFRESH_KEY = "dependentPropertyToRefresh";
	public static final String SHOW_EDIT_BUTTON = "showEditButton";
	public static final String FORCE_EDIT_IN_POPUP = "forceEditInPopup";

	//CHECKSTYLE:OFF
	@Override
	protected void renderSingleEditor(final SectionPanel panel, final SectionRow sectionRow, Component parent,
			final String isoCode, Map<String, Object> ctx, List<String> readableLangIsos, final List<String> writeableLangIsos)
	{
		if (sectionRow instanceof EditorPropertyRow)
		{
			if (readableLangIsos != null && !readableLangIsos.contains(isoCode))
			{
				Label label = new Label(Labels.getLabel("editor.cataloglanguage.hidden", new String[] { isoCode }));
				label.setStyle("color:#ccc;");
				label.setParent(parent);
				return;
			}

			final Map parameters = ((EditorPropertyRow) sectionRow).getRowConfiguration().getParameters();
			final UIEditorArea editorArea = (UIEditorArea) ctx.get("editorArea");
			final TypedObject currentObject = (TypedObject) ctx.get("currentObject");
			ObjectValueContainer currentObjectValues = (ObjectValueContainer) ctx.get("valueContainer");
			if (currentObjectValues == null)
			{
				LOG.error("No value container set in context.");
				return;
			}

			EditorPropertyRow editorPropertyRow = (EditorPropertyRow) sectionRow;
			final PropertyDescriptor propertyDescriptor = editorPropertyRow.getRowConfiguration().getPropertyDescriptor();
			PropertyEditorDescriptor edDescr = editorPropertyRow.getRowConfiguration().getEditorDescriptor();
			if (edDescr == null)
			{
				Collection label = this.getEditorFactory().getMatchingEditorDescriptors(propertyDescriptor.getEditorType());
				edDescr = label.isEmpty() ? null : (PropertyEditorDescriptor) label.iterator().next();
			}

			if (edDescr != null)
			{
				final ObjectValueContainer.ObjectValueHolder valueHolder = currentObjectValues.getValue(propertyDescriptor,
						isoCode);
				Object currentValue = valueHolder.getCurrentValue();
				String preferredEditor;
				if (panel.isEditMode())
				{
					preferredEditor = "readonly";
				}
				else
				{
					preferredEditor = editorPropertyRow.getRowConfiguration().getEditor();
				}

				UIEditor _editor = null;
				if (preferredEditor != null && !preferredEditor.isEmpty())
				{
					_editor = edDescr.createUIEditor(preferredEditor);
				}

				if (_editor == null)
				{
					_editor = this.checkMap(currentValue, propertyDescriptor) ?
							edDescr.createUIEditor("mapEditor") :
							(this.checkSingle(propertyDescriptor) ? edDescr.createUIEditor("single") : edDescr.createUIEditor(
									PropertyDescriptor.Multiplicity.RANGE.equals(propertyDescriptor.getMultiplicity()) ?
											"range" :
											"multi"));
				}

				if (_editor != null)
				{
					_editor.setOptional(!PropertyDescriptor.Occurrence.REQUIRED.equals(propertyDescriptor.getOccurence()));
					_editor.setEditable(
							sectionRow.isEditable() && (writeableLangIsos == null || writeableLangIsos.contains(isoCode)));
					if (_editor instanceof ListUIEditor)
					{
						((ListUIEditor) _editor).setAvailableValues(EditorHelper
								.filterValues(propertyDescriptor,
										this.getTypeService().getAvailableValues(propertyDescriptor, currentObject)));
					}

					final UIEditor editor = _editor;

					EditorListener editorListener = new EditorListener()
					{
						public void actionPerformed(String actionCode)
						{
							if ("open_external_clicked".equals(actionCode))
							{
								Object nextIso = editor.getValue();
								if (nextIso instanceof Collection && editor instanceof ReferenceUIEditor)
								{
									ObjectTemplate langIndex = TypeTools.getValueTypeAsObjectTemplate(propertyDescriptor,
											getTypeService());
									langIndex = EditorHelper.processVariantTypeCheck(langIndex, currentObject, propertyDescriptor,
											getTypeService());
									UICockpitPerspective perspective = UISessionUtils.getCurrentSession().getCurrentPerspective();
									EditorHelper.tryOpenContextAreaEditor(perspective, langIndex, (Collection) nextIso,
											currentObject, propertyDescriptor, parameters);
								}
							}
							else if (!"escape_pressed".equals(actionCode) && !"cancel_clicked".equals(actionCode))
							{
								if ("enter_pressed".equals(actionCode))
								{
									String nextIso1 = null;
									if (writeableLangIsos != null)
									{
										int langIndex1 = writeableLangIsos.indexOf(isoCode);
										if (langIndex1 < writeableLangIsos.size() - 1)
										{
											nextIso1 = (String) writeableLangIsos.get(langIndex1 + 1);
										}
									}

									panel.focusNext(sectionRow, nextIso1);
								}
							}
							else
							{
								editor.setValue(valueHolder.getOriginalValue());
							}

						}

						public void valueChanged(Object value)
						{
							Object editorValue = value;
							if (value instanceof Collection && PropertyDescriptor.Multiplicity.SET.equals(
									propertyDescriptor.getMultiplicity()))
							{
								editorValue = new HashSet((Collection) value);
							}

							valueHolder.setLocalValue(editorValue);
							if (valueHolder.isModified())
							{
								valueHolder.setModified(true);
							}

							if (editorArea != null && currentObject != null)
							{
								editorArea.doOnChange(propertyDescriptor);
							}

						}
					};
					CreateContext createContext = null;
					if (_editor instanceof ReferenceUIEditor)
					{
						ReferenceUIEditor customParameters = (ReferenceUIEditor) _editor;
						createContext = EditorHelper.applyReferenceRelatedAttributes(customParameters, propertyDescriptor,
								parameters, currentObject, currentValue, isoCode, editorArea, UISessionUtils.getCurrentSession());
					}

					HashMap customParameters1 = new HashMap(parameters);
					customParameters1.put("attributeQualifier", propertyDescriptor.getQualifier());
					customParameters1.put("languageIso", isoCode);
					if (!this.allowOverlap() && !parameters.containsKey(SHOW_EDIT_BUTTON))
					{
						customParameters1.put(SHOW_EDIT_BUTTON, String.valueOf(editorArea != null && !(editorArea
								.getEditorAreaController() instanceof PopupEditorAreaController)));
					}

					customParameters1.put(AdditionalReferenceEditorListener.class.getName(),
							new AdditionalReferenceEditorListener()
							{
								public void openItemRequestPerformed(TypedObject item)
								{
									activateItemInPopupEditor(item, parameters.containsKey(FORCE_EDIT_IN_POPUP));
								}
							});
					if (createContext != null)
					{
						try
						{
							customParameters1.put("createContext", createContext.clone());
						}
						catch (CloneNotSupportedException var28)
						{
							LOG.error("Error while cloning create context: ", var28);
						}
					}

					if (propertyDescriptor instanceof ClassAttributePropertyDescriptor)
					{
						customParameters1.put("propertyDescriptor", propertyDescriptor);
					}

					customParameters1.put("propInfo", propertyDescriptor.getQualifier());
					customParameters1.put("currentObject", ctx.get("currentObject"));

					if (parameters.containsKey(DEPENDENT_PROPERTY_TO_REFRESH_KEY))
					{
						String propertyQualifier = (String) parameters.get(DEPENDENT_PROPERTY_TO_REFRESH_KEY);
						createRefresheableListener(editorArea, propertyDescriptor, propertyQualifier);
					}

					final HtmlBasedComponent editorView = _editor.createViewComponent(currentValue, customParameters1,
							editorListener);
					if (UISessionUtils.getCurrentSession().isUsingTestIDs())
					{
						String focusListenerStr = "ENUM".equals(_editor.getEditorType()) ? "dropdown" : "input";
						String sectionName = ctx.containsKey("sectionName") ? (String) ctx.get("sectionName") : "unknown";
						String cockpitID = "EditorArea_" + sectionName + "_" + (propertyDescriptor.getName() == null ?
								"" :
								propertyDescriptor.getName().replaceAll(" ", "")) + "_" + focusListenerStr;
						UITools.applyTestID(editorView, cockpitID);
					}

					parent.appendChild(editorView);
					SectionRow.FocusListener focusListener = createFocusListener(editor, editorView, panel, sectionRow);
					sectionRow.setFocusListener(focusListener, writeableLangIsos != null ? isoCode : null);
					if (writeableLangIsos != null && writeableLangIsos.indexOf(isoCode) == 0)
					{
						sectionRow.setFocusListener(focusListener, null);
					}
				}
			}
			else
			{
				Label label1 = new Label(
						"no editor for " + editorPropertyRow.getRowConfiguration().getPropertyDescriptor().getEditorType());
				label1.setStyle("color:red");
				label1.setParent(parent);
			}
		}

	}

	protected void activateItemInPopupEditor(TypedObject item, final boolean forceActivateInPopup) {
		UICockpitPerspective currentPerspective = UISessionUtils.getCurrentSession().getCurrentPerspective();
		if(this.allowOverlap() || forceActivateInPopup) {
			currentPerspective.activateItemInPopupEditor(item);
		} else if(currentPerspective instanceof BaseUICockpitPerspective && !((BaseUICockpitPerspective)currentPerspective).isPopupEditorOpen()) {
			currentPerspective.activateItemInPopupEditor(item);
		}

	}
	//CHECKSTYLE:ON



	/**
	 * OOTB piece
	 */
	private SectionRow.FocusListener createFocusListener(final UIEditor editor, final HtmlBasedComponent editorView,
			final SectionPanel panel, final SectionRow sectionRow)
	{
		return new SectionRow.FocusListener()
		{
			public void onFocus()
			{
				if (editor.isInline())
				{
					editor.setFocus((HtmlBasedComponent) editorView, false);
					panel.setSelectedRow(sectionRow);
				}
				else
				{
					panel.focusNext(sectionRow, (String) null);
				}

			}
		};
	}


	private void createRefresheableListener(final UIEditorArea editorArea, final PropertyDescriptor dependentPropertyDescriptor,
			final String changedPropertyQualifier)
	{

		EditorAreaListener newListener = new EditorAreaListener()
		{

			public void currentObjectChanged(final TypedObject previous, final TypedObject current)
			{ // no handle
			}

			public void nextItemClicked(final TypedObject currentObj)
			{ // no handle
			}

			public void valuesStored(final ObjectValueContainer valueContainer)
			{ // no handle
			}

			public void valuesUpdated(final ObjectValueContainer valueContainer)
			{ // no handle
			}

			public void browseItemClicked(final TypedObject currentObj)
			{ // no handle
			}

			public void previousItemClicked(final TypedObject currentObj)
			{ // no handle
			}

			public void currentObjectUpdated()
			{ // no handle
			}

			public void valueChanged(final ObjectValueContainer valueContainer,
					final PropertyDescriptor changedPropertyDescriptor)
			{

				LOG.debug(
						"Sending global event on change of [{}] property. [{}] is xml configured qualifier. [{}] is "
								+ "PropertyDescriptor for configured qualifier.", changedPropertyDescriptor.getQualifier(),
						changedPropertyDescriptor, dependentPropertyDescriptor.getQualifier());

				if (changedPropertyDescriptor.getQualifier().equals(changedPropertyQualifier))

				{
					UISessionUtils.getCurrentSession().sendGlobalEvent(new ItemChangedEvent(this, editorArea.getCurrentObject(),
							Collections.singleton(dependentPropertyDescriptor)));
					hideContextBrowserArea(editorArea);
				}


			}

		};
		editorArea.addEditorAreaListener(newListener);
		LOG.debug("Listener added");
	}

	private void hideContextBrowserArea(final UIEditorArea editorArea)
	{
		BrowserModel adv = editorArea.getPerspective().getBrowserArea().getFocusedBrowser();
		if (adv instanceof AdvancedBrowserModel && ((AdvancedBrowserModel) adv).isContextVisible())
		{
			((AdvancedBrowserModel) adv).setContextVisible(false);
		}
	}

	/**
	 * OOTB copy from {@link EditorRowRenderer}
	 */
	private boolean checkMap(final Object currentValue, final PropertyDescriptor propertyDescriptor)
	{
		if (propertyDescriptor instanceof ItemAttributePropertyDescriptor && currentValue instanceof Map)
		{
			AttributeDescriptorModel firstAttributeDescriptor =
					((ItemAttributePropertyDescriptor) propertyDescriptor).getFirstAttributeDescriptor();
			return firstAttributeDescriptor != null && firstAttributeDescriptor.getAttributeType() instanceof MapTypeModel;
		}
		else
		{
			return false;
		}
	}

	/**
	 * OOTB copy from {@link EditorRowRenderer}
	 */
	private boolean allowOverlap()
	{
		boolean allowOverlap = false;

		try
		{
			allowOverlap = Boolean.parseBoolean(
					UITools.getCockpitParameter("default.popUpEditor.allowOverlap", Executions.getCurrent()));
		}
		catch (Exception e)
		{
			LOG.warn("Cannot read popupEditorOverlapping property: " + e.getMessage());
		}

		return allowOverlap;
	}

	/**
	 * OOTB copy from {@link EditorRowRenderer}
	 */
	private boolean checkSingle(final PropertyDescriptor propertyDescriptor)
	{
		PropertyDescriptor.Multiplicity multi = propertyDescriptor.getMultiplicity();
		return multi == null || multi.equals(PropertyDescriptor.Multiplicity.SINGLE);
	}
}
