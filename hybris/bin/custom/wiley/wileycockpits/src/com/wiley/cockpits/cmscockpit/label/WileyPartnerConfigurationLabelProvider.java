package com.wiley.cockpits.cmscockpit.label;

import com.wiley.core.model.WileyPartnerCompanyModel;
import com.wiley.core.model.WileyPartnerConfigurationModel;
import de.hybris.platform.category.model.CategoryModel;
import org.apache.commons.collections.CollectionUtils;

public class WileyPartnerConfigurationLabelProvider extends WileyBaseLabelProvider<WileyPartnerConfigurationModel>
{
	private static final String EMPTY_PLACEHOLDER = "<none>";
	private static final String PARTNER_URL_FORMAT = "/partner/%s/%s/";
	@Override
	protected String getItemLabel(final WileyPartnerConfigurationModel model)
	{
		CategoryModel category = model.getCategory();
		String categoryCode = category.getCode();
		String partnerCode = EMPTY_PLACEHOLDER;

		if (CollectionUtils.isNotEmpty(category.getWileyPartners())) {
			WileyPartnerCompanyModel partnerCompany = category.getWileyPartners().iterator().next();
			partnerCode = partnerCompany.getUid();
		}
		return String.format(PARTNER_URL_FORMAT, partnerCode, categoryCode);
	}
}
