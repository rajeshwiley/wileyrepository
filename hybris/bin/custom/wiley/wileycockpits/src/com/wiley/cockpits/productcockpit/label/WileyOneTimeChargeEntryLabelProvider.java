package com.wiley.cockpits.productcockpit.label;

import de.hybris.platform.cockpit.services.label.AbstractModelLabelProvider;
import de.hybris.platform.servicelayer.i18n.L10NService;
import de.hybris.platform.subscriptionservices.model.OneTimeChargeEntryModel;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;


/**
 * OOTB_CODE
 * We need to create this class because OOTB one 
 * (de.hybris.platform.subscriptioncockpits.services.label.impl.OneTimeChargeEntryLabelProvider)
 * is in subscription extension which is not used
 */
public class WileyOneTimeChargeEntryLabelProvider extends AbstractModelLabelProvider<OneTimeChargeEntryModel>
{
	@Resource
	private L10NService l10nService;

	protected String getItemLabel(final OneTimeChargeEntryModel model)
	{
		String price = "<null>";
		String currency = "<null>";
		String billingEvent = "<null>";
		String name = null;
		if (model != null)
		{
			if (model.getPrice() != null)
			{
				NumberFormat decimalFormat = DecimalFormat.getInstance();
				price = decimalFormat.format(model.getPrice().doubleValue());
			}

			if (model.getCurrency() != null)
			{
				currency = model.getCurrency().getSymbol();
			}

			if (model.getBillingEvent() != null)
			{
				billingEvent = model.getBillingEvent().getNameInCart();
			}

			if (StringUtils.isNotEmpty(model.getName()))
			{
				name = model.getName();
			}
		}

		String itemLabel;
		if (StringUtils.isEmpty(name))
		{
			itemLabel = l10nService.getLocalizedString("cockpit.onetimechargeentry.noname",
					new Object[] { currency, price, billingEvent });
		}
		else
		{
			itemLabel = l10nService.getLocalizedString("cockpit.onetimechargeentry.name",
					new Object[] { name, currency, price, billingEvent });
		}

		return itemLabel;
	}

	@Override
	protected String getItemLabel(final OneTimeChargeEntryModel oneTimeChargeEntryModel, final String s)
	{
		return null;
	}

	@Override
	protected String getItemDescription(final OneTimeChargeEntryModel oneTimeChargeEntryModel)
	{
		return null;
	}

	@Override
	protected String getItemDescription(final OneTimeChargeEntryModel oneTimeChargeEntryModel, final String s)
	{
		return null;
	}

	@Override
	protected String getIconPath(final OneTimeChargeEntryModel oneTimeChargeEntryModel)
	{
		return null;
	}

	@Override
	protected String getIconPath(final OneTimeChargeEntryModel oneTimeChargeEntryModel, final String s)
	{
		return null;
	}

}
