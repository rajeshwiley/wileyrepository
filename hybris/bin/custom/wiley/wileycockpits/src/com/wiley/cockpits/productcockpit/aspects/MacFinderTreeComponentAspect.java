package com.wiley.cockpits.productcockpit.aspects;


import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.security.PrincipalGroup;
import de.hybris.platform.jalo.user.User;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.configuration.Configuration;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;


/**
 * This Aspects allows to disable drag and drop feature for particular userGroups.
 *
 */
@Aspect
@Configurable
public class MacFinderTreeComponentAspect
{
	private static final Logger LOG = LoggerFactory.getLogger(MacFinderTreeComponentAspect.class);

	@Autowired
	private ConfigurationService configurationService;

	@Around("execution(* de.hybris.platform.productcockpit.components.macfinder.MacFinderTreeComponent.checkOpenDropContext(..))")
	public void checkOpenDropContext(final ProceedingJoinPoint pjp) throws Throwable
	{
		LOG.info("MacFinderTreeComponentAspect is taking action..");

		User currentUser = JaloSession.getCurrentSession().getUser();
		List<String> currentUserGroups = prepareUserGroupsForCurrentUser(currentUser);
		List<String> restrictedUserGroups = prepareRestrictedUserGroups();

		if (!currentUserGroups.isEmpty() && !restrictedUserGroups.isEmpty())
		{
			if (CollectionUtils.containsAny(restrictedUserGroups, currentUserGroups))
			{
				LOG.info("Current user [{}] has insufficient access rights to move catalogs.", currentUser.getUid());
				throw new RuntimeException(
						"You do not have sufficient rights to perform this operation or configuration is wrong!");
			}
		}

		pjp.proceed();
	}

	private List<String> prepareRestrictedUserGroups()
	{
		Configuration configuration = configurationService.getConfiguration();
		String usergroups = configuration.getString("cockpit.dragAndDrop.restricted.usergroups");

		if (usergroups == null)
		{
			return Collections.emptyList();
		}
		return Arrays.asList(usergroups.split("\\s*,\\s*"));
	}

	private List<String> prepareUserGroupsForCurrentUser(final User currentUser)
	{
		List<String> groups = new ArrayList<String>();
		for (PrincipalGroup pg : currentUser.getAllGroups())
		{
			groups.add(pg.getUid());
		}

		return groups;
	}
}