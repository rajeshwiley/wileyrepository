package com.wiley.cockpits.cmscockpit.components;

import de.hybris.platform.cmscockpit.components.listview.impl.CmsElementSyncAction;
import de.hybris.platform.cockpit.services.sync.SynchronizationService;

import org.springframework.beans.factory.annotation.Required;
import org.zkoss.zk.ui.event.EventListener;

import com.wiley.cockpits.cmscockpit.services.WileySynchronizationService;


/**
 * This Action is designed to show sync status it cannot trigger synchronization.
 *
 * Created by Uladzimir_Barouski on 4/5/2017.
 */
public class WileySynchronizationAction extends CmsElementSyncAction
{

	private WileySynchronizationService synchronizationService;

	@Override
	public EventListener getEventListener(final Context context)
	{
		return null;
	}

	@Override
	public EventListener getMultiSelectEventListener(final Context context)
	{
		return null;
	}

	@Override
	public String getImageURI(final Context context)
	{
		//the icon should be shown only if user doesn't have sync permission
		if (!synchronizationService.canSync(context.getItem()))
		{
			return super.getImageURI(context);
		}
		return null;
	}

	@Required
	public void setSynchronizationService(final WileySynchronizationService synchronizationService)
	{
		this.synchronizationService = synchronizationService;
	}

	@Override
	protected SynchronizationService getSynchronizationService()
	{
		return synchronizationService;
	}
}
