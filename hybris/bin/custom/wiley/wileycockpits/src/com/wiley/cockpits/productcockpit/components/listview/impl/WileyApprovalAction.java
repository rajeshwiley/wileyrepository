package com.wiley.cockpits.productcockpit.components.listview.impl;

import de.hybris.platform.cockpit.components.listview.ListViewAction;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.productcockpit.components.listview.impl.ApprovalAction;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.zkoss.zul.Menupopup;

import com.wiley.cockpits.constants.WileyCockpitsConstants;

public class WileyApprovalAction extends ApprovalAction
{
	@Autowired
	private UserService userService;

	@Override
	public Menupopup getPopup(final ListViewAction.Context context)
	{
		Menupopup menuPopup = createMenupopup(context, false);
		return menuPopup;
	}

	@Override
	public Menupopup getMultiSelectPopup(final ListViewAction.Context context)
	{
		Menupopup menuPopup = createMenupopup(context, true);
		return menuPopup;
	}

	private Menupopup createMenupopup(final ListViewAction.Context context, final boolean isMultiSelectPopup)
	{
		Set<UserGroupModel> userGroupModelSet = userService.getAllUserGroupsForUser(userService.getCurrentUser());
		for (UserGroupModel userGroupModel : userGroupModelSet)
		{
			// Create an empty menupop or without approval status menuitem
			if (userGroupModel.getUid().equalsIgnoreCase(WileyCockpitsConstants.PRODUCT_EDITOR_GROUP))
			{
				return new Menupopup();
			}
		}
		if (isMultiSelectPopup)
		{
			return super.getMultiSelectPopup(context);
		}
		return super.getPopup(context);
	}

}
