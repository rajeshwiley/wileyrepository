package com.wiley.cockpits.cmscockpit.label;

import com.wiley.core.model.WileyDiscountRowModel;

public class WileyDiscountRowLabelProvider extends WileyBaseLabelProvider<WileyDiscountRowModel>
{
	@Override
	protected String getItemLabel(final WileyDiscountRowModel model)
	{

		String label = "";
		if (model.getProduct() != null)
		{
			label = model.getProduct().getCode();
		} else if (model.getPg() != null)
		{
			label = model.getPg().getCode();
		}
		return label;
	}

}
