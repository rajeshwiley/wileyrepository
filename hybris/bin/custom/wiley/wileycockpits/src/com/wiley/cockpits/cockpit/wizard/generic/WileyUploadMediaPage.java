package com.wiley.cockpits.cockpit.wizard.generic;

import de.hybris.platform.cockpit.wizards.generic.UploadMediaPage;
import de.hybris.platform.core.model.media.MediaModel;


/**
 * Created by Sergiy_Mishkovets on 11/3/2017.
 */
public class WileyUploadMediaPage extends UploadMediaPage
{
	@Override
	protected void updateMediaModel(final MediaModel currentMediaModel)
	{
		currentMediaModel.setRealFileName(this.getMedia().getName());
		super.updateMediaModel(currentMediaModel);
	}
}
