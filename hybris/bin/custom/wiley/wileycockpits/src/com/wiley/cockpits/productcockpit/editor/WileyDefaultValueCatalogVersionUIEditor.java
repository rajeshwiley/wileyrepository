package com.wiley.cockpits.productcockpit.editor;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cockpit.model.editor.EditorListener;
import de.hybris.platform.cockpit.model.referenceeditor.simple.impl.DefaultSimpleReferenceUIEditor;
import de.hybris.platform.cockpit.session.UISessionUtils;
import de.hybris.platform.core.Registry;

import java.util.Map;

import org.zkoss.zk.ui.HtmlBasedComponent;

import com.wiley.cockpits.constants.WileyCockpitsConstants;


public class WileyDefaultValueCatalogVersionUIEditor extends DefaultSimpleReferenceUIEditor
{

	private static final String CATALOG_VERSION_NAME = "Staged";

	@Override
	public HtmlBasedComponent createViewComponent(final Object initialValue, final Map<String, ?> parameters,
			final EditorListener listener)
	{
		Object defaultValue = initialValue;
		if (parameters.containsKey(WileyCockpitsConstants.DEFAULT_PARAMETER))
		{
			final CatalogVersionService catalogVersionService =
					(CatalogVersionService) Registry.getApplicationContext().getBean("catalogVersionService");
			final CatalogVersionModel catalogVersionModel = catalogVersionService.getCatalogVersion(
					String.valueOf(parameters.get(WileyCockpitsConstants.DEFAULT_PARAMETER)), CATALOG_VERSION_NAME);
			defaultValue = UISessionUtils.getCurrentSession().getTypeService().wrapItem(catalogVersionModel
			);
			listener.valueChanged(defaultValue);
		}
		return super.createViewComponent(defaultValue, parameters, listener);
	}

}
