package com.wiley.cockpits.b2badmincockpit.wizards.impl;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2badmincockpit.wizards.impl.B2BCommonOrganizationWizardPage;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.catalog.model.CompanyModel;
import de.hybris.platform.cockpit.wizards.Message;
import de.hybris.platform.site.BaseSiteService;

import java.util.ArrayList;
import java.util.List;

import com.wiley.core.b2b.unit.service.WileyB2BUnitExternalService;
import com.wiley.core.b2b.unit.service.exception.B2BUnitNotFoundException;
import com.wiley.core.b2b.unit.service.exception.FindB2BUnitExternalSystemException;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;


public class WileyB2BCommonOrganizationWizardPage extends B2BCommonOrganizationWizardPage
{

	private static final Logger LOG = Logger.getLogger(WileyB2BCommonOrganizationWizardPage.class);
	private static final String B2BUNIT_PARAMETER_NAME = "b2bUnit";
	private static final String UID = "uid";

	@Autowired
	private WileyB2BUnitExternalService wileyB2BUnitExternalService;

	@Autowired
	private BaseSiteService baseSiteService;

	private String siteUid;

	@Override
	public List<Message> validate()
	{
		List<Message> validationMessages = new ArrayList<>();
		final String unitUid = (String) getAttribute(UID);

		if (StringUtils.isNotBlank(unitUid))
		{
			CompanyModel unit = getB2bUnitService().getUnitForUid(unitUid);
			if (unit != null)
			{
				validationMessages.add(new Message(Message.ERROR, "The unit id you entered already exists in hybris.", UID));
			}
			else
			{
				try
				{
					BaseSiteModel site = baseSiteService.getBaseSiteForUID(siteUid);
					B2BUnitModel b2bUnit = wileyB2BUnitExternalService.find(unitUid, site);
					this.setAttribute(B2BUNIT_PARAMETER_NAME, b2bUnit);
				}
				catch (B2BUnitNotFoundException e)
				{
					LOG.error("The unit id could not be found in ERP.");
					validationMessages.add(new Message(Message.ERROR, "The unit id you entered could not be found in ERP.", UID));
				}
				catch (FindB2BUnitExternalSystemException e)
				{
					LOG.error("The ERP lookup service is temporary unavailable.");
					validationMessages.add(new Message(Message.ERROR, "The ERP lookup service is temporary unavailable.", UID));
				}
			}
		}

		return validationMessages;
	}

	@Required
	public void setSiteUid(final String siteUid)
	{
		this.siteUid = siteUid;
	}
}
