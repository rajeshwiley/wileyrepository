package com.wiley.cockpits.productcockpit.wizard.resolvers;

import de.hybris.platform.cockpit.wizards.generic.DefaultValueResolver;
import de.hybris.platform.product.VariantsService;

import javax.annotation.Resource;

public class WileyasPredefinedVariantTypeValueResolver implements DefaultValueResolver
{
    @Resource
    private VariantsService variantsService;

    @Override
    public Object getValue(final String value)
    {
        return variantsService.getVariantTypeForCode(value);
    }
}
