package com.wiley.cockpits.cmscockpit.services.impl;

import de.hybris.platform.cms2.constants.GeneratedCms2Constants;
import de.hybris.platform.cms2.enums.CmsApprovalStatus;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cmscockpit.services.impl.CmsCockpitServiceImpl;
import de.hybris.platform.cockpit.model.meta.TypedObject;

import java.util.Iterator;
import java.util.Locale;

import com.google.common.base.Preconditions;


public class WileyCmsCockpitServiceImpl extends CmsCockpitServiceImpl
{
	//OOTB code. added only cloning of pageLabel and pageParent. see #cloneLabelHierarchy
	@Override
	public TypedObject clonePageFirstLevel(final TypedObject page, final String name)
	{
		AbstractPageModel clonedPage = null;

		try
		{
			if (page != null && page.getObject() instanceof AbstractPageModel)
			{
				AbstractPageModel originalPage = (AbstractPageModel) page.getObject();
				clonedPage = getModelService().create(originalPage.getClass());
				Preconditions.checkArgument(clonedPage.getClass() == originalPage.getClass());
				clonedPage.setUid(getGenericRandomNameProducer()
						.generateSequence(GeneratedCms2Constants.TC.ABSTRACTPAGE, "page"));
				clonedPage.setDefaultPage(Boolean.FALSE);
				clonedPage.setOnlyOneRestrictionMustApply(originalPage.isOnlyOneRestrictionMustApply());
				clonedPage.setApprovalStatus(CmsApprovalStatus.CHECK);
				clonedPage.setMasterTemplate(originalPage.getMasterTemplate());
				clonedPage.setCatalogVersion(originalPage.getCatalogVersion());
				clonedPage.setRestrictions(originalPage.getRestrictions());
				if (name == null)
				{
					clonedPage.setName(originalPage.getName());
				}
				else
				{
					clonedPage.setName(name);
				}

				Iterator languageIterator = getSystemService().getAvailableLanguageIsos().iterator();

				while (languageIterator.hasNext())
				{
					String iso = (String) languageIterator.next();
					Locale loc = getLocalizationService().getLocaleByString(iso);
					clonedPage.setTitle(originalPage.getTitle(loc), loc);
				}

				if (clonedPage instanceof ContentPageModel)
				{
					//customization - add pageLabel and pageParent (OOTB was label only)
					cloneLabelHierarchy((ContentPageModel) originalPage, (ContentPageModel) clonedPage);
				}
				cloneContentSlots(originalPage, clonedPage);
			}

			if (clonedPage != null)
			{
				getModelService().save(clonedPage);
			}
		}
		catch (Exception exc)
		{
			LOG.error(exc.getMessage(), exc);
			return null;
		}

		return typeService.wrapItem(clonedPage);
	}

	private void cloneLabelHierarchy(final ContentPageModel originalPage, final ContentPageModel clonedPage)
	{
		clonedPage.setLabel(originalPage.getLabel());
		clonedPage.setPageLabel(originalPage.getPageLabel());
		clonedPage.setParentPage(originalPage.getParentPage());
	}
}
