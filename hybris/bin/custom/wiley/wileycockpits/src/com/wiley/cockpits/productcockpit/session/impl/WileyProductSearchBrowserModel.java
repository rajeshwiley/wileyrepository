package com.wiley.cockpits.productcockpit.session.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cockpit.model.advancedsearch.AdvancedSearchModel;
import de.hybris.platform.cockpit.model.advancedsearch.impl.DefaultAdvancedSearchModel;
import de.hybris.platform.cockpit.model.meta.ObjectTemplate;
import de.hybris.platform.cockpit.model.search.ExtendedSearchResult;
import de.hybris.platform.cockpit.model.search.Query;
import de.hybris.platform.cockpit.session.UISessionUtils;
import de.hybris.platform.productcockpit.session.impl.DefaultProductSearchBrowserModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;


public class WileyProductSearchBrowserModel extends DefaultProductSearchBrowserModel
{

	private Map<String, List<String>> itemtypeByCatalog;

	@Override
	protected ExtendedSearchResult doSearchInternal(final Query query)
	{
		AdvancedSearchModel advancedSearchModel = getAdvancedSearchModel();
		if (advancedSearchModel != null && advancedSearchModel instanceof DefaultAdvancedSearchModel)
		{
			Collection<CatalogVersionModel> selectedCatalogVersions = getSelectedCatalogVersions();
			DefaultAdvancedSearchModel defaultAdvancedSearchModel = (DefaultAdvancedSearchModel) advancedSearchModel;

			List<String> itemtypes = Collections.emptyList();

			if (CollectionUtils.isNotEmpty(selectedCatalogVersions))
			{
				itemtypes = itemtypeByCatalog.get(selectedCatalogVersions.iterator().next().getCatalog().getId());
			}

			if (CollectionUtils.isNotEmpty(itemtypes))
			{
				ArrayList<ObjectTemplate> templates = new ArrayList<>();
				itemtypes.forEach(itemtype -> templates.add(getObjectTemplateByItemtype(itemtype)));
				defaultAdvancedSearchModel.setTypes(templates);
			}
		}
		return super.doSearchInternal(query);
	}

	private ObjectTemplate getObjectTemplateByItemtype(final String itemtype)
	{
		return UISessionUtils.getCurrentSession().getTypeService().getObjectTemplate(itemtype);
	}

	public void setItemtypeByCatalog(final Map<String, List<String>> itemtypeByCatalog)
	{
		this.itemtypeByCatalog = itemtypeByCatalog;
	}
}
