package com.wiley.cockpits.b2badmincockpit.wizards.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BCustomerService;
import de.hybris.platform.b2b.services.B2BUnitService;
import de.hybris.platform.cockpit.wizards.Message;
import de.hybris.platform.cockpit.wizards.Wizard;
import de.hybris.platform.cockpit.wizards.WizardPage;
import de.hybris.platform.cockpit.wizards.exception.WizardConfirmationException;
import de.hybris.platform.cockpit.wizards.impl.DefaultPageController;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.impl.UniqueAttributesInterceptor;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;
import org.zkoss.zul.Messagebox;

import com.wiley.core.customer.strategy.WileyCustomerRegistrationStrategy;
import com.wiley.core.exceptions.ExternalSystemAccessException;
import com.wiley.core.exceptions.ExternalSystemBadRequestException;
import com.wiley.core.exceptions.ExternalSystemInternalErrorException;
import com.wiley.core.exceptions.ExternalSystemValidationException;
import com.wiley.core.wileycom.customer.exception.WileycomCustomerRegistrationFailureException;


public class WileyB2BAdminUserWizardPageController extends DefaultPageController
{
	private static final Logger LOG = Logger.getLogger(WileyB2BAdminUserWizardPageController.class);
	private static final String UNITID_ATTRIBUTE = "unitId";
	private static final String UID_ATTRIBUTE = "uid";
	private static final String PASSWORD_ATTRIBUTE = "password";
	private static final String FIRSTNAME_ATTRIBUTE = "firstName";
	private static final String LASTNAME_ATTRIBUTE = "lastName";
	private static final String EMAIL_ATTRIBUTE = "email";
	private static final String B2B_SITE = "wileyb2b";

	private Set<String> attributeSets;

	@Autowired
	private TransactionTemplate transactionTemplate;

	@Autowired
	private UserService userService;

	@Autowired
	private ModelService modelService;

	@Autowired
	private B2BUnitService b2bUnitService;

	@Autowired
	private B2BCustomerService b2bcustomerService;

	@Autowired
	private WileyCustomerRegistrationStrategy wileyCustomerRegistrationStrategy;
	
	@Autowired
	private BaseSiteService baseSiteService;


	public void done(final Wizard wizard, final WizardPage page) throws WizardConfirmationException
	{
		createAdminUser(wizard);
	}

	private void createAdminUser(final Wizard wizard)
	{
		transactionTemplate.execute(new TransactionCallbackWithoutResult()
		{
			@Override
			protected void doInTransactionWithoutResult(final TransactionStatus transactionStatus)
			{
				try
				{
					createB2BCustomer(wizard);
					Messagebox.show("B2B Admin successfully created!", "Item Created", 1, "z-msgbox z-msgbox-information");
				}
				catch (DuplicateUidException | WileycomCustomerRegistrationFailureException e)
				{
					LOG.error("Error Registering Customer on external system", e);
					transactionStatus.setRollbackOnly();
				}
				catch (InterruptedException e)
				{
					LOG.error(e.getMessage());
					// Restore interrupted state...
					Thread.currentThread().interrupt();
				}
			}
		});

	}

	private void createB2BCustomer(final Wizard wizard)
			throws InterruptedException, DuplicateUidException, WileycomCustomerRegistrationFailureException
	{
		HashMap<String, Object> b2bAdminAttributes = (HashMap<String, Object>) getAttributesForPage("adminUser", wizard);
		UserGroupModel b2bAdminGroup = userService.getUserGroupForUID("b2badmingroup");
		B2BCustomerModel b2bAdmin = (B2BCustomerModel) modelService.create(B2BCustomerModel.class);
		B2BUnitModel b2bUnit = (B2BUnitModel) b2bUnitService
				.getUnitForUid((String) b2bAdminAttributes.get(UNITID_ATTRIBUTE));
		String password = (String) b2bAdminAttributes.get(PASSWORD_ATTRIBUTE);

		b2bAdmin.setUid((String) b2bAdminAttributes.get(UID_ATTRIBUTE));
		b2bAdmin.setEmail((String) b2bAdminAttributes.get(EMAIL_ATTRIBUTE));
		b2bAdmin.setFirstName((String) b2bAdminAttributes.get(FIRSTNAME_ATTRIBUTE));
		b2bAdmin.setLastName((String) b2bAdminAttributes.get(LASTNAME_ATTRIBUTE));
		b2bAdmin.setGroups(Collections.singleton((PrincipalGroupModel) b2bAdminGroup));

		b2bUnitService.setCurrentUnit(b2bAdmin, b2bUnit);
		b2bcustomerService.addMember(b2bAdmin, b2bUnit);
		userService.setPasswordWithDefaultEncoding(b2bAdmin, (String) b2bAdminAttributes.get(PASSWORD_ATTRIBUTE));
		
		baseSiteService.setCurrentBaseSite(B2B_SITE, true);

		try
		{
			registerCustomerInExternalWileycomSystems(b2bAdmin, password);
			registerCustomerInHybrisSystem(b2bAdmin, password);
		}
		catch (ExternalSystemAccessException | ExternalSystemInternalErrorException | ExternalSystemBadRequestException
				| ExternalSystemValidationException exception)
		{
			String message = "Wiley external customer registration failed due to malformed request or server is unavailable!";
			LOG.error(message);
			Messagebox.show("The user registration service is temporary unavailable.", "Service Unavailable", 1,
					"z-msgbox z-msgbox-error");
			throw new WileycomCustomerRegistrationFailureException(message, exception);
		}
	}

	private void registerCustomerInHybrisSystem(final B2BCustomerModel b2bAdmin, final String password)
			throws DuplicateUidException
	{
		if (password != null)
		{
			userService.setPasswordWithDefaultEncoding(b2bAdmin, password);
		}
		try
		{
			modelService.save(b2bAdmin);
		}
		catch (final ModelSavingException e)
		{
			if (e.getCause() instanceof InterceptorException && ((InterceptorException) e.getCause()).getInterceptor()
					.getClass().equals(UniqueAttributesInterceptor.class))
			{
				LOG.error("Duplicate Uid found for Id: " + b2bAdmin.getUid() + e.getMessage(), e);
			}
			else
			{
				throw new DuplicateUidException();
			}
		}
		catch (final AmbiguousIdentifierException e)
		{
			LOG.error("Duplicate Uid found for Id: " + b2bAdmin.getUid() + e.getMessage());
		}
	}

	private void registerCustomerInExternalWileycomSystems(final CustomerModel customerModel, final String password)
	{
		wileyCustomerRegistrationStrategy.register(customerModel, password);
	}

	public boolean validate(final Wizard wizard, final WizardPage page)
	{
		WileyAbstractB2BAdminUserWizardPage currentPage = (WileyAbstractB2BAdminUserWizardPage) page;
		List<Message> validationMessages = new ArrayList<>();
		final Predicate validationPredicate = new Predicate()
		{
			public boolean evaluate(final Object object)
			{
				return UID_ATTRIBUTE.equals(((Message) object).getComponentId());

			}
		};

		validationMessages.addAll(currentPage.validate());

		for (Message message : currentPage.validateAttributes())
		{
			boolean hasAlreadyMsgForAttribute = CollectionUtils.exists(validationMessages, validationPredicate);
			if (hasAlreadyMsgForAttribute)
			{
				continue;
			}
			validationMessages.add(message);

		}

		for (Iterator<String> wizardPageIterator = getAttributeSets().iterator(); wizardPageIterator.hasNext();)
		{
			String pageId = (String) wizardPageIterator.next();

			Map<String, Object> otherPageAttributes = getAttributesForPage(pageId, wizard);

			if ((currentPage.getAttribute(UID_ATTRIBUTE) == null) || (currentPage.getId().equals(pageId))
					|| (otherPageAttributes == null)
					|| (!currentPage.getAttribute(UID_ATTRIBUTE).equals(otherPageAttributes.get(UID_ATTRIBUTE))))
			{
				continue;
			}
			currentPage.setAttribute(UID_ATTRIBUTE, "");
			validationMessages.add(new Message(Message.ERROR, "The Uid you selected already exists.", UID_ATTRIBUTE));

		}

		boolean isValid = validationMessages.isEmpty();

		if (!isValid)
		{
			for (Iterator<Message> messageIterator = validationMessages.iterator(); messageIterator.hasNext();)
			{
				Message message = (Message) messageIterator.next();
				wizard.addMessage(message);
			}
		}

		return isValid;
	}

	protected Set<String> getAttributeSets()
	{
		return attributeSets;
	}

	public void setAttributeSets(final Set<String> attributeSets)
	{
		this.attributeSets = attributeSets;
	}

	protected Map<String, Object> getAttributesForPage(final String pageId, final Wizard wizard)
	{
		return ((Map<String, Object>) wizard.getWizardContext().getAttribute(pageId));
	}

}
