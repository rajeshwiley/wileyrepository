package com.wiley.cockpits.cockpit.components;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.lang.Objects;
import org.zkoss.zk.ui.AbstractComponent;
import org.zkoss.zk.ui.ext.client.Inputable;


/**
 * ZK component for CKEditor
 */
public class WileyCKEditor extends AbstractComponent
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyCKEditor.class);

	private String value;
	private String height;
	private String toolbarSet;
	private String txtByClient;
	private String languageIsocode;
	private boolean inline;
	private List<String> additionalContentsCss;

	public WileyCKEditor(final String languageIsocode)
	{
		this.languageIsocode = languageIsocode;
		this.value = StringUtils.EMPTY;

		if (LOG.isDebugEnabled())
		{
			LOG.debug("Created WileyCKEditor with languageIsocode=[{}]", this.languageIsocode);
		}
	}

	public String getValue()
	{
		return this.value;
	}

	public void setValue(final String value)
	{
		final String safeValue = StringUtils.defaultIfEmpty(value, StringUtils.EMPTY);

		if (!safeValue.equals(this.value))
		{
			this.value = safeValue;
			if (this.txtByClient == null || !Objects.equals(this.txtByClient, safeValue))
			{
				this.txtByClient = null;
				this.smartUpdate("value", safeValue);
			}
		}
	}

	public String getToolbarSet()
	{
		return this.toolbarSet;
	}

	public void setToolbarSet(final String toolbarSet)
	{
		String toolbarSetNonEmpty = StringUtils.EMPTY.equals(toolbarSet) ? null : toolbarSet;

		if (!Objects.equals(toolbarSetNonEmpty, this.toolbarSet))
		{
			this.toolbarSet = toolbarSetNonEmpty;
			this.smartUpdate("toolbarset", toolbarSetNonEmpty);
		}
	}

	public String getHeight()
	{
		return this.height;
	}

	public void setHeight(final String height)
	{
		String heightNonEmpty = StringUtils.EMPTY.equals(height) ? null : height;

		if (!Objects.equals(heightNonEmpty, this.height))
		{
			this.height = heightNonEmpty;
			this.smartUpdate("height", heightNonEmpty);
		}
	}

	protected Object newExtraCtrl()
	{
		return (Inputable) text ->
		{
			WileyCKEditor.this.txtByClient = text;

			try
			{
				WileyCKEditor.this.setValue(text);
			}
			finally
			{
				WileyCKEditor.this.txtByClient = null;
			}
		};
	}

	public String getLanguageIsocode()
	{
		return languageIsocode;
	}

	public void setLanguageIsocode(final String languageIsocode)
	{
		this.languageIsocode = languageIsocode;
	}

	public boolean isInline()
	{
		return inline;
	}

	public void setInline(final boolean inline)
	{
		this.inline = inline;
	}

	public List<String> getAdditionalContentsCss()
	{
		return additionalContentsCss;
	}

	public void setAdditionalContentsCss(final List<String> additionalContentsCss)
	{
		this.additionalContentsCss = additionalContentsCss;
	}
}
