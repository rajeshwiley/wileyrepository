/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.cockpits.constants;

/**
 * Global class for all WileyCockpits constants. You can add global constants for your extension into this class.
 */
public final class WileyCockpitsConstants extends GeneratedWileyCockpitsConstants
{
	/**
	 * The constant EXTENSIONNAME.
	 */
	public static final String EXTENSIONNAME = "wileycockpits";
	public static final String PRODUCT_EDITOR_GROUP = "producteditorgroup";

	public static final String DEFAULT_PARAMETER = "defaultValue";
	public static final String CURRENT_OBJECT_PARAMETER = "currentObject";
	private WileyCockpitsConstants()
	{
		// empty
	}
}
