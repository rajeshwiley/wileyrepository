/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.cockpits.cmscockpit.browser.filters;

import de.hybris.platform.cockpit.model.search.Query;
import de.hybris.platform.util.localization.Localization;



/**
 * The type Desktop ui experience browser filter.
 */
public class DesktopUiExperienceBrowserFilter extends AbstractUiExperienceFilter
{
	private static final String DESKTOP_UI_EXPERIENCE_LABEL_KEY = "desktop.ui.experience.label.key";

	/**
	 * Exclude boolean.
	 *
	 * @param item
	 * 		the item
	 * @return the boolean
	 */
	@Override
	public boolean exclude(final Object item)
	{

		return false;
	}

	/**
	 * Filter query.
	 *
	 * @param query
	 * 		the query
	 */
	@Override
	public void filterQuery(final Query query)
	{
		//empty because DESKTOP pages are displayed as default 
	}


	/**
	 * Gets label.
	 *
	 * @return the label
	 */
	@Override
	public String getLabel()
	{
		return Localization.getLocalizedString(DESKTOP_UI_EXPERIENCE_LABEL_KEY);
	}

}
