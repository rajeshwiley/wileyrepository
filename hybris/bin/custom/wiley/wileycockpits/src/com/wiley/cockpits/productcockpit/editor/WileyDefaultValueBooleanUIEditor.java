package com.wiley.cockpits.productcockpit.editor;

import de.hybris.platform.cockpit.model.editor.EditorListener;
import de.hybris.platform.cockpit.model.editor.impl.DefaultBooleanUIEditor;

import java.util.Map;
import java.util.Objects;

import org.zkoss.zk.ui.HtmlBasedComponent;

import com.wiley.cockpits.constants.WileyCockpitsConstants;


public class WileyDefaultValueBooleanUIEditor extends DefaultBooleanUIEditor
{
	private static final String TRUE = "true";

	@Override
	public HtmlBasedComponent createViewComponent(final Object initialValue, final Map<String, ? extends Object> parameters,
			final EditorListener listener)
	{
		Object defaultValue = initialValue;
		if (parameters.containsKey(WileyCockpitsConstants.DEFAULT_PARAMETER) && Objects.isNull(
				parameters.get(WileyCockpitsConstants.CURRENT_OBJECT_PARAMETER)))
		{
			defaultValue = TRUE.equals(parameters.get(WileyCockpitsConstants.DEFAULT_PARAMETER));
			listener.valueChanged(defaultValue);
		}
		return super.createViewComponent(defaultValue, parameters, listener);
	}
}
