package com.wiley.cockpits.cmscockpit.components;

import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;
import de.hybris.platform.cmscockpit.components.listview.impl.AbstractCmscockpitListViewAction;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.session.UISession;
import de.hybris.platform.cockpit.session.UISessionUtils;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Menupopup;


/**
 * Created by Uladzimir_Barouski on 3/22/2017.
 */
public class WileySharedComponentAction extends AbstractCmscockpitListViewAction
{
	private static final int MIN_PAGES_COUNT_TO_SHOW_SHARED_ICON = 1;

	@Override
	protected void doCreateContext(final Context context)
	{

	}

	@Override
	public String getImageURI(final Context context)
	{
		if (CollectionUtils.isNotEmpty(this.getRelatedPages(context.getItem())))
		{
			return "cmscockpit/images/related_pages_list.png";
		}
		return null;
	}

	private List<TypedObject> getRelatedPages(final TypedObject component)
	{
		Object object = component.getObject();
		List relatedPages = Collections.EMPTY_LIST;
		if (object instanceof AbstractCMSComponentModel)
		{
			List pages = ((AbstractCMSComponentModel) object).getRelatedPages();

			if (pages.size() > MIN_PAGES_COUNT_TO_SHOW_SHARED_ICON)
			{
				relatedPages = getCurrentSession().getTypeService().wrapItems(pages);
			}
		}
		return relatedPages;
	}

	@Override
	public EventListener getEventListener(final Context context)
	{
		return null;
	}

	@Override
	public Menupopup getPopup(final Context context)
	{
		Menupopup ret = new Menupopup();
		Iterator relatedPagesIterator = this.getRelatedPages(context.getItem()).iterator();

		while (relatedPagesIterator.hasNext())
		{
			final TypedObject openWizardItem = (TypedObject) relatedPagesIterator.next();
			Menuitem mitem = new Menuitem();
			String objectTextLabel = getCurrentSession().getLabelService().getObjectTextLabelForTypedObject(openWizardItem);
			mitem.setLabel(objectTextLabel);
			ret.appendChild(mitem);
		}
		return ret;
	}

	@Override
	public Menupopup getContextPopup(final Context context)
	{
		return null;
	}

	@Override
	public String getTooltip(final Context context)
	{
		return Labels.getLabel("cmscockpit.show.pages");
	}

	public UISession getCurrentSession()
	{
		return UISessionUtils.getCurrentSession();
	}
}
