package com.wiley.cockpits.b2badmincockpit.components.browserarea.impl;

import de.hybris.platform.b2badmincockpit.components.browserarea.impl.B2BOrganizationButtonAction;
import de.hybris.platform.cockpit.components.listview.ListViewAction;

import java.util.List;

import com.wiley.cockpits.b2badmincockpit.components.browserarea.WileyTypeViewSpecificButtonAction;


public class WileyB2BOrganizationButtonAction extends B2BOrganizationButtonAction implements WileyTypeViewSpecificButtonAction
{
	private List<String> enabledForTypes;

	@Override
	public List<String> getEnabledForTypes()
	{
		return enabledForTypes;
	}

	public void setEnabledForTypes(final List<String> enabledForTypes)
	{
		this.enabledForTypes = enabledForTypes;
	}

	@Override
	public String getMultiSelectImageURI(final ListViewAction.Context context)
	{
		if (enabledForCurrentType(context))
		{
			return super.getMultiSelectImageURI(context);
		}
		return null;
	}
}
