package com.wiley.cockpits.cmscockpit.label;

import de.hybris.platform.cockpit.services.label.AbstractModelLabelProvider;

public abstract class WileyBaseLabelProvider<T> extends AbstractModelLabelProvider<T>
{

	@Override
	protected String getItemLabel(final T item, final String lang)
	{
		return getItemLabel(item);
	}

	@Override
	protected String getItemDescription(final T item)
	{
		return null;
	}

	@Override
	protected String getItemDescription(final T item, final String lang)
	{
		return null;
	}

	@Override
	protected String getIconPath(final T item)
	{
		return null;
	}

	@Override
	protected String getIconPath(final T item, final String lang)
	{
		return null;
	}
}
