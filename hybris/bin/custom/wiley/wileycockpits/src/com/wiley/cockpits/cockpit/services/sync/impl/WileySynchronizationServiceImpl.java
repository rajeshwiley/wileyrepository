package com.wiley.cockpits.cockpit.services.sync.impl;

import de.hybris.platform.catalog.jalo.CatalogManager;
import de.hybris.platform.catalog.jalo.CatalogVersion;
import de.hybris.platform.catalog.jalo.SyncItemJob;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.services.sync.impl.SynchronizationServiceImpl;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.cronjob.jalo.Job;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.jalo.type.TypeManager;
import de.hybris.platform.jalo.user.User;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.wiley.cockpits.cmscockpit.services.WileySynchronizationService;


/**
 * OOTB_CODE overrides SynchronizationServiceImpl to allow see sync status for users without synchronization permissions.
 *
 * Created by Uladzimir_Barouski on 4/5/2017.
 */
public class WileySynchronizationServiceImpl
    extends SynchronizationServiceImpl
    implements WileySynchronizationService
{
	@Override
	protected SyncInfo getSyncInfoSimple(final TypedObject object)
	{
		int status = 0;
		HashSet affectedItems = new HashSet();
		SyncInfo ret = new WileySyncInfo();
		CatalogVersion sourceCatalogVersion = this.retrieveCatalogVersion(object);
		if (sourceCatalogVersion == null)
		{
			status = -1;
		}
		else
		{
			List synchronizationRules = sourceCatalogVersion.getSynchronizations();
			if (synchronizationRules != null && !synchronizationRules.isEmpty() && this.isTypeInRootTypes(
					object.getType().getCode(), synchronizationRules))
			{
				if (!this.isVersionSynchronizedAtLeastOnce(synchronizationRules))
				{
					status = 2;
				}
				else
				{
					Iterator iter = synchronizationRules.iterator();

					while (iter.hasNext())
					{
						Object rawObject = iter.next();
						if (rawObject instanceof SyncItemJob)
						{
							SyncItemJob syncTask = (SyncItemJob) rawObject;
							//removed OOTB_CODE if(!this.checkUserRight(syncTask))
							CatalogVersion targetCatalogVersion = syncTask.getTargetVersion();
							status = this.isSynchronized(this.extractJaloItem(object.getObject()), targetCatalogVersion, syncTask,
									(Item) null, affectedItems);
							if (status == 1)
							{
								break;
							}
						}
					}
				}
			}
			else
			{
				status = -1;
			}
		}

		ret.setSyncStatus(status);
		ret.setAffectedItems(affectedItems);
		return ret;
	}

	/**
	 * OOTB_CODE
	 *
	 * @param sourceItem
	 * @param targetVersion
	 * @param syncTask
	 * @param targetItem
	 * @param affectedItemsReturn
	 * @return
	 */
	private int isSynchronized(final Item sourceItem, final CatalogVersion targetVersion, final SyncItemJob syncTask,
			final Item targetItem, final Set<TypedObject> affectedItemsReturn)
	{
		byte ret = 0;
		Item targetCounterPart = null;
		if (targetItem == null)
		{
			SessionContext secondSyncTime = null;

			try
			{
				secondSyncTime = JaloSession.getCurrentSession().createLocalSessionContext();
				secondSyncTime.setAttribute("disableRestrictions", this.isSearchRestrictionDisabled());
				targetCounterPart = CatalogManager.getInstance().getCounterpartItem(sourceItem, targetVersion);
			}
			finally
			{
				if (secondSyncTime != null)
				{
					JaloSession.getCurrentSession().removeLocalSessionContext();
				}

			}
		}
		else
		{
			targetCounterPart = targetItem;
		}

		if (targetCounterPart == null)
		{
			ret = 1;
		}
		else
		{
			if (this.getCatalogManager().getLastSyncModifiedTime(syncTask, sourceItem, targetCounterPart) == null)
			{
				ret = 1;
			}
			else
			{
				long secondSyncTime1 = this.getCatalogManager().getLastSyncModifiedTime(syncTask, sourceItem, targetCounterPart)
						.getTime();
				if (secondSyncTime1 < sourceItem.getModificationTime().getTime())
				{
					ret = 1;
				}
			}

			if (affectedItemsReturn != null)
			{
				affectedItemsReturn.add(this.getTypeService().wrapItem(targetCounterPart));
			}
		}

		return ret;
	}

	/**
	 * OOTB_CODE
	 *
	 * @param sourceObject
	 * @return
	 */
	private Item extractJaloItem(final Object sourceObject)
	{
		return sourceObject instanceof Item ?
				(Item) sourceObject :
				(sourceObject instanceof ItemModel ? (Item) this.getModelService().getSource((ItemModel) sourceObject) : null);
	}

	/**
	 * OOTB_CODE
	 *
	 * @param typeCode
	 * @param syncJobs
	 * @return
	 */

	private boolean isTypeInRootTypes(final String typeCode, final List<Job> syncJobs)
	{
		Iterator var4 = syncJobs.iterator();

		while (var4.hasNext())
		{
			Job job = (Job) var4.next();
			SyncItemJob catalogVersionSynJob = (SyncItemJob) job;
			ComposedType objectType = TypeManager.getInstance().getComposedType(typeCode);
			List rootTypes = catalogVersionSynJob.getRootTypes();
			Iterator var9 = rootTypes.iterator();

			while (var9.hasNext())
			{
				ComposedType composedType = (ComposedType) var9.next();
				if (composedType.isAssignableFrom(objectType))
				{
					return true;
				}
			}
		}

		return false;
	}

	/**
	 * OOTB_CODE
	 *
	 * @param job
	 * @return
	 */
	private boolean checkUserRightToTargetVersion(final SyncItemJob job)
	{
		JaloSession jaloSession = JaloSession.getCurrentSession();
		User currentUser = jaloSession.getUser();
		if (currentUser.isAdmin())
		{
			return true;
		}
		else
		{
			SessionContext ctx = jaloSession.getSessionContext();
			return getCatalogManager().canSync(ctx, currentUser, job);
		}
	}

	protected class WileySyncInfo extends SyncInfo
	{
	}

	/**
	 * Custom method which checks if user are able sync item
	 *
	 * @param object
	 * 		TypedObject
	 * @return true if object can be synced
	 */
	@Override
	public boolean canSync(final TypedObject object)
	{
		CatalogVersion sourceCatalogVersion = this.retrieveCatalogVersion(object);
		if (sourceCatalogVersion != null)
		{
			List synchronizationRules = sourceCatalogVersion.getSynchronizations();
			if (synchronizationRules != null && !synchronizationRules.isEmpty() && this.isTypeInRootTypes(
					object.getType().getCode(), synchronizationRules))
			{
				Iterator iter = synchronizationRules.iterator();
				while (iter.hasNext())
				{
					Object rawObject = iter.next();
					if (rawObject instanceof SyncItemJob)
					{
						SyncItemJob syncTask = (SyncItemJob) rawObject;
						if (checkUserRightToTargetVersion(syncTask))
						{
							return true;
						}
					}
				}
			}
		}
		return false;
	}
}
