package com.wiley.cockpits.cmscockpit.aspects;

import de.hybris.platform.cockpit.events.CockpitEvent;
import de.hybris.platform.cockpit.events.impl.ItemChangedEvent;
import de.hybris.platform.cockpit.model.meta.PropertyDescriptor;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.model.meta.impl.ItemAttributePropertyDescriptor;
import de.hybris.platform.cockpit.session.UISessionUtils;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

import com.wiley.core.components.interceptors.WileyPreparePurchaseOptionProductListComponentInterceptor;
import com.wiley.core.model.components.WileyPurchaseOptionProductListComponentModel;


/**
 * This aspect is created to refresh discount rows section in WCMS on update items of
 * {@link WileyPurchaseOptionProductListComponentModel}. Discounts are added/deleted
 * using {@link WileyPreparePurchaseOptionProductListComponentInterceptor}
 *
 * @author Dzmitryi_Halahayeu
 */
@Aspect
public class WileyTypeToolsAspect
{

	@Around("execution(* de.hybris.platform.cockpit.session.impl.UISessionImpl.sendGlobalEvent(..))")
	public void addDiscountRowsToItemChangeEventsForProducts(final ProceedingJoinPoint jp) throws Throwable
	{
		final CockpitEvent cockpitEvent = (CockpitEvent) jp.getArgs()[0];
		if (cockpitEvent instanceof ItemChangedEvent)
		{
			final ItemChangedEvent itemChangedEvent = (ItemChangedEvent) cockpitEvent;
			final TypedObject typedObject = itemChangedEvent.getItem();
			if (typedObject != null
					&& WileyPurchaseOptionProductListComponentModel._TYPECODE.equals(typedObject.getType().getCode()))
			{
				final Set<PropertyDescriptor> properties = itemChangedEvent.getProperties();
				if (CollectionUtils.isNotEmpty(properties))
				{

					//Don't use lambda for this check. Aspects doesn't support lambdas
					boolean containsItemsProperty = false;
					for (final PropertyDescriptor propertyDescriptor : properties)
					{
						if (propertyDescriptor instanceof ItemAttributePropertyDescriptor
								&& WileyPurchaseOptionProductListComponentModel.ITEMS.equals(
								((ItemAttributePropertyDescriptor) propertyDescriptor).getAttributeQualifier()))
						{
							containsItemsProperty = true;
							break;
						}
					}

					if (containsItemsProperty)
					{
						final PropertyDescriptor discountRowsPropertyDescriptor =
								UISessionUtils.getCurrentSession().getTypeService().getPropertyDescriptor(
										WileyPurchaseOptionProductListComponentModel._TYPECODE + "."
												+ WileyPurchaseOptionProductListComponentModel.DISCOUNTROWS);
						final Set<PropertyDescriptor> newProperties = new HashSet<>();
						newProperties.addAll(properties);
						newProperties.add(discountRowsPropertyDescriptor);

						final ItemChangedEvent newItemChangedItem = new ItemChangedEvent(itemChangedEvent.getSource(),
								itemChangedEvent.getItem(), newProperties,
								itemChangedEvent.getChangeType(),
								itemChangedEvent.getParameters());
						if (jp.getArgs().length == 2)
						{
							final boolean immediate = (boolean) jp.getArgs()[1];
							jp.proceed(new Object[] { newItemChangedItem, immediate });
							return;
						}
						else
						{
							jp.proceed(new Object[] { newItemChangedItem });
							return;
						}
					}

				}
			}
		}
		jp.proceed();
	}

}
