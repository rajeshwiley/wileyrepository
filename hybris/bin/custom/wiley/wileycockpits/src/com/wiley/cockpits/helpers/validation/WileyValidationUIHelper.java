package com.wiley.cockpits.helpers.validation;

import de.hybris.platform.cockpit.helpers.validation.ValidationUIHelper;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Menuitem;


/**
 * Created by Uladzimir_Barouski on 7/4/2017.
 */
public class WileyValidationUIHelper extends ValidationUIHelper
{
	private static final Logger LOG = Logger.getLogger(WileyValidationUIHelper.class);

	@Value("${backoffice.base.url}")
	private String baseUrl;

	/**
	 * Override OOTB_CODE to use base url from property.
	 *
	 * @param constraintPk
	 * @return
	 */
	@Override
	public Menuitem createViewConstraintMenuItem(final String constraintPk)
	{
		Menuitem menuItem = new Menuitem(Labels.getLabel("validation.menupopup.view.constraint"));
		menuItem.addEventListener("onClick", new EventListener() {
			public void onEvent(final Event event) throws Exception
			{
				StringBuilder baseUrlBuilder = new StringBuilder();
				//if url from property not empty, otherwise OOTB logic will be used
				if (StringUtils.isNotEmpty(baseUrl))
				{
					baseUrlBuilder.append(baseUrl);
				}
				else
				{
					Execution current = Executions.getCurrent();
					baseUrlBuilder.append(current.getScheme()).append("://");
					baseUrlBuilder.append(current.getServerName());
					baseUrlBuilder.append(":").append(current.getLocalPort());
				}
				baseUrlBuilder.append("/admincockpit/index.zul");
				StringBuilder urlBuilder = new StringBuilder();
				urlBuilder.append("?").append("persp");
				urlBuilder.append("=").append("admincockpit.perspective.admincockpit");
				urlBuilder.append("&").append("events");
				urlBuilder.append("=").append("activation");
				urlBuilder.append("&").append("act-item");
				urlBuilder.append("=").append(constraintPk);
				if (LOG.isDebugEnabled())
				{
					LOG.debug("URL for Open in AdminCockpit edit event: " + baseUrlBuilder.toString() + urlBuilder.toString());
				}

				Executions.getCurrent().sendRedirect(baseUrlBuilder.toString() + urlBuilder.toString(), "_blank");
			}
		});
		return menuItem;
	}
}
