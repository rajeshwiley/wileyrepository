package com.wiley.cockpits.services.config.impl;

import de.hybris.platform.cockpit.services.config.DefaultUIRole;
import de.hybris.platform.cockpit.services.config.UIRole;
import de.hybris.platform.cockpit.services.config.impl.DefaultUIConfigurationService;
import de.hybris.platform.core.model.user.UserModel;

import java.util.List;
import java.util.stream.Collectors;


/**
 * UIConfigurationService to override cockpit user roles logic
 */
public class WileyUIConfigurationService extends DefaultUIConfigurationService
{
	/**
	 * OOTB uses all groups of the user (from the whole hierarchy) and check them against cockpit configuration.
	 * Overriden method below gets groups assigned to the user without hierarchy and without any configuration check.
	 *
	 * @param user
	 * 		User
	 * @return UI roles
	 */
	@Override
	public List<UIRole> getPossibleRoles(final UserModel user)
	{
		UIRole fallbackRole = this.getFallbackRole();
		return user.getGroups().stream()
				.filter(group -> !group.getUid().equals(fallbackRole.getName()) && !group.getUid().equals(user.getUid()))
				.map(group -> new DefaultUIRole(group.getUid()))
				.collect(Collectors.toList());
	}

}
