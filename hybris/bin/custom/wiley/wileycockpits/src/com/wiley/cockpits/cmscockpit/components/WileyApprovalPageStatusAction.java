package com.wiley.cockpits.cmscockpit.components;

import de.hybris.platform.cms2.enums.CmsApprovalStatus;
import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cmscockpit.components.listview.impl.ApprovalPageStatusAction;
import de.hybris.platform.cockpit.components.notifier.Notification;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.services.sync.SynchronizationService;
import de.hybris.platform.cockpit.session.UISessionUtils;
import de.hybris.platform.cockpit.util.UITools;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.workflow.WorkflowProcessingService;
import de.hybris.platform.workflow.WorkflowService;
import de.hybris.platform.workflow.WorkflowTemplateService;
import de.hybris.platform.workflow.model.WorkflowActionModel;
import de.hybris.platform.workflow.model.WorkflowModel;
import de.hybris.platform.workflow.model.WorkflowTemplateModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Menupopup;

import com.wiley.core.cms.service.WileyCMSService;


/**
 * Created by Anton_Lukyanau on 1/31/2017.
 */
public class WileyApprovalPageStatusAction extends ApprovalPageStatusAction
{
	private static final Logger LOG = Logger.getLogger(WileyApprovalPageStatusAction.class);
	private static final String START_WORKFLOW_MENU_ITEM = "start.workflow.menu.item";
	private static final String START_WORKFLOW_BUSY_MESSAGE = "start.workflow";
	public static final int NOT_SYNCED_STATUS = 1;
	public static final int INIT_STATUS = 2;

	@Resource
	private WileyCMSService wileyCMSService;
	@Resource
	private WorkflowTemplateService workflowTemplateService;
	@Resource
	private ModelService modelService;
	@Resource
	private WorkflowProcessingService workflowProcessingService;
	@Resource
	private UserService userService;
	@Resource
	private SynchronizationService synchronizationService;
	@Resource
	private SessionService sessionService;

	private WorkflowService workflowService;

	private List<String> contentEditorGroups;

	private Map<String, String> workflows;

	@Override
	public Menupopup getPopup(final Context context)
	{
		Menupopup popupMenu = null;
		if (context.getItem().getObject() instanceof AbstractPageModel && isEditor())
		{
			AbstractPageModel page = (AbstractPageModel) context.getItem().getObject();

			String workflowCode = workflows.get(page.getCatalogVersion().getCatalog().getId());
			if (StringUtils.isNotEmpty(workflowCode))
			{
				if (popupMenu == null)
				{
					popupMenu = new Menupopup();
				}
				WorkflowTemplateModel workflowTemplate = this.workflowTemplateService.getWorkflowTemplateForCode(
						workflowCode);
				Menuitem menuItem = createMenuItem(context, popupMenu, workflowTemplate);
				if (UISessionUtils.getCurrentSession().isUsingTestIDs())
				{
					String id = UISessionUtils.getCurrentSession().getLabelService().getObjectTextLabel(context.getItem());
					id = id.replaceAll("\\W", "") + "_" + this.getClass().getSimpleName();
					id = id + "." + workflowCode;
					id = "Menuitem_" + id;
					UITools.applyTestID(menuItem, id);
				}
			}
		}
		else
		{
			popupMenu = super.getPopup(context);
		}
		return popupMenu;
	}

	private boolean isEditor()
	{
		return userService.getCurrentUser().getAllGroups()
				.stream()
				.anyMatch(group -> contentEditorGroups.contains(group.getUid()));
	}

	private Menuitem createMenuItem(final Context context, final Menupopup popupMenu,
			final WorkflowTemplateModel workflowTemplate)
	{
		Menuitem menuItem = new Menuitem(Labels.getLabel(START_WORKFLOW_MENU_ITEM));
		UITools.addBusyListener(menuItem, "onClick", new EventListener()
		{
			public void onEvent(final Event event) throws Exception
			{
				startWorkflow(workflowTemplate, (AbstractPageModel) context.getItem().getObject());
			}
		}, (String) null, START_WORKFLOW_BUSY_MESSAGE);
		menuItem.setParent(popupMenu);
		return menuItem;
	}

	private void startWorkflow(final WorkflowTemplateModel workflowTemplate, final AbstractPageModel page)
	{
		String workflowTitle = workflowTemplate.getName() + " - " + page.getName();
		List<ItemModel> attachmets = collectAttachments(page);
		if (CollectionUtils.isNotEmpty(attachmets))
		{
			final WorkflowModel workflow = this.workflowService.createWorkflow(workflowTitle, workflowTemplate, attachmets,
					userService.getCurrentUser());
			modelService.save(workflow);
			for (final WorkflowActionModel action : workflow.getActions())
			{
				modelService.save(action);
			}

			this.workflowProcessingService.startWorkflow(workflow);
			attachmets.stream().forEach(attachment -> doChangeStatus(attachment));
			UISessionUtils.getCurrentSession().getCurrentPerspective().getNotifier().setNotification(
					new Notification(Labels.getLabel("start.workflow.success.title"),
							Labels.getLabel("start.workflow.success.message")));
		}
		else
		{
			UISessionUtils.getCurrentSession().getCurrentPerspective().getNotifier().setNotification(
					new Notification(Labels.getLabel("start.workflow.error.title"),
							Labels.getLabel("start.workflow.error.message")));
		}
	}

	private List<ItemModel> collectAttachments(final AbstractPageModel page)
	{
		List<ItemModel> components = new ArrayList<>();
		List<AbstractCMSComponentModel> changedComponents = wileyCMSService.findNotSyncComponentsForPage(page);
		if (CollectionUtils.isNotEmpty(changedComponents))
		{
			List<AbstractPageModel> changedPages = changedComponents.stream()
					.map(c -> c.getRelatedPages())
					.flatMap(List::stream)
					.filter(p -> p.getCatalogVersion().equals(page.getCatalogVersion()))
					.distinct()
					.collect(Collectors.toList());
			components.addAll(changedPages);
			components.addAll(changedComponents);
		}
		else
		{
			int pageSyncStatus = getPageSyncStatus(page);
			if (pageSyncStatus == NOT_SYNCED_STATUS || pageSyncStatus == INIT_STATUS)
			{
				components.add(page);
			}
		}
		return components;
	}


	private int getPageSyncStatus(final AbstractPageModel page)
	{
		TypedObject typedObject = UISessionUtils.getCurrentSession().getTypeService().wrapItem(page);
		return sessionService.executeInLocalView(new SessionExecutionBody()
		{
			@Override
			public Object execute()
			{
				return synchronizationService.isObjectSynchronized(typedObject);
			}
		}, userService.getAdminUser());
	}

	private void doChangeStatus(final ItemModel item)
	{
		if (item instanceof AbstractPageModel)
		{
			TypedObject typedObject = UISessionUtils.getCurrentSession().getTypeService().wrapItem(item);
			sessionService.executeInLocalView(new SessionExecutionBody()
			{
				@Override
				public void executeWithoutResult()
				{
					doChangeStatus(typedObject, CmsApprovalStatus.CHECK.getCode());
				}
			}, userService.getAdminUser());
		}
	}

	@Required
	public void setContentEditorGroups(final List<String> contentEditorGroups)
	{
		this.contentEditorGroups = contentEditorGroups;
	}

	@Required
	public void setWorkflows(final Map<String, String> workflows)
	{
		this.workflows = workflows;
	}

	@Required
	public void setWorkflowService(final WorkflowService workflowService)
	{
		this.workflowService = workflowService;
	}
}