package com.wiley.cockpits.productcockpit.wizard.resolvers;

import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.cockpit.wizards.generic.DefaultValueResolver;

public class WileyasPredefinedStatusValueResolver implements DefaultValueResolver
{
    @Override
    public Object getValue(final String value)
    {
        return ArticleApprovalStatus.valueOf(value);
    }
}
