package com.wiley.cockpits.b2badmincockpit.wizards.impl;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2badmincockpit.wizards.impl.B2BOrganizationWizardPageController;
import de.hybris.platform.cockpit.wizards.Wizard;
import de.hybris.platform.cockpit.wizards.WizardPage;
import de.hybris.platform.cockpit.wizards.exception.WizardConfirmationException;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;
import org.zkoss.zul.Messagebox;


public class WileyB2BOrganizationWizardPageController extends B2BOrganizationWizardPageController
{
	private static final Logger LOG = Logger.getLogger(WileyB2BOrganizationWizardPageController.class);
	private static final String B2BUNIT_PARAMETER_NAME = "b2bUnit";
	private static final String COMMON_PARAMETER_NAME = "common";
	private static final String UID = "uid";

	@Autowired
	private TransactionTemplate transactionTemplate;

	@Override
	public void done(final Wizard wizard, final WizardPage page) throws WizardConfirmationException
	{
		createOrganizationInTransaction(wizard);
		try
		{
			Messagebox.show("B2B Unit successfully created!", "Item Created", 1, "z-msgbox z-msgbox-information");
		}
		catch (InterruptedException e)
		{
			LOG.error(e.getMessage());
			// Restore interrupted state...
			Thread.currentThread().interrupt();
		}
	}

	private void createOrganizationInTransaction(final Wizard wizard)
	{
		transactionTemplate.execute(new TransactionCallbackWithoutResult()
		{
			@Override
			protected void doInTransactionWithoutResult(final TransactionStatus transactionStatus)
			{
				saveUnit(wizard);
			}
		});
	}

	private void saveUnit(final Wizard wizard)
	{
		HashMap<String, Object> commonAttributes = (HashMap<String, Object>) getAttributesForPage(COMMON_PARAMETER_NAME, wizard);
		B2BUnitModel unit = (B2BUnitModel) commonAttributes.get(B2BUNIT_PARAMETER_NAME);
		if (unit != null)
		{
			unit.setUid((String) commonAttributes.get(UID));
			LOG.info("Saving b2bUnit with ID " + unit.getUid());
			getModelService().attach(unit);
			getModelService().save(unit);
		}
	}
}
