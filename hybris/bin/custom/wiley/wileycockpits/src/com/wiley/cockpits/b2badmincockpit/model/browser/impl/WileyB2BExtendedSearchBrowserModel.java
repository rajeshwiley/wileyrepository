package com.wiley.cockpits.b2badmincockpit.model.browser.impl;

import de.hybris.platform.cockpit.model.browser.impl.DefaultExtendedSearchBrowserModel;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;


public class WileyB2BExtendedSearchBrowserModel extends DefaultExtendedSearchBrowserModel
{
	private List<String> hideCreateButtonForTypes;

	public List<String> getHideCreateButtonForTypes()
	{
		return hideCreateButtonForTypes;
	}

	public void setHideCreateButtonForTypes(final List<String> hideCreateButtonForTypes)
	{
		this.hideCreateButtonForTypes = hideCreateButtonForTypes;
	}

	@Override
	public boolean isShowCreateButton()
	{
		boolean showCreateButton = super.isShowCreateButton();
		if (CollectionUtils.isNotEmpty(hideCreateButtonForTypes) && getRootType() != null)
		{
			showCreateButton = !hideCreateButtonForTypes.contains(getRootType().getCode());
		}
		return showCreateButton;
	}
}
