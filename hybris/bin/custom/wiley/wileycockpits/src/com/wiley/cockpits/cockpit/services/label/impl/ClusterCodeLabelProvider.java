package com.wiley.cockpits.cockpit.services.label.impl;

import com.wiley.core.model.ClusterCodeModel;
import de.hybris.platform.cockpit.services.label.AbstractModelLabelProvider;
import org.apache.commons.lang.StringUtils;

public class ClusterCodeLabelProvider extends AbstractModelLabelProvider<ClusterCodeModel> {

    private static final String DELIMETER = " / ";

    @Override
    protected String getItemLabel(final ClusterCodeModel clusterCode) {
        StringBuilder label = new StringBuilder();

        label.append(clusterCode.getCurrency().getIsocode());
        label.append(DELIMETER);
        label.append(clusterCode.getClusterCode());
        label.append(DELIMETER);
        label.append(clusterCode.getCatalogVersion().getCatalog().getName());
        label.append(DELIMETER);
        label.append(clusterCode.getCatalogVersion().getVersion());
        
        return label.toString();
    }

    
    @Override
    protected String getItemLabel(final ClusterCodeModel clusterCodeModel, final String s) {
        return this.getItemLabel(clusterCodeModel);
    }

    @Override
    protected String getItemDescription(final ClusterCodeModel clusterCodeModel) {
        return StringUtils.EMPTY;
    }

    @Override
    protected String getItemDescription(final ClusterCodeModel clusterCodeModel, final String s) {
        return StringUtils.EMPTY;
    }

    @Override
    protected String getIconPath(final ClusterCodeModel clusterCodeModel) {
        return null;
    }

    @Override
    protected String getIconPath(final ClusterCodeModel clusterCodeModel, final String s) {
        return null;
    }
}

