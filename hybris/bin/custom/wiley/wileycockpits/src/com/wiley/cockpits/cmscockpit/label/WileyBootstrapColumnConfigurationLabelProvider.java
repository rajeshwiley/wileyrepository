package com.wiley.cockpits.cmscockpit.label;

import de.hybris.platform.cockpit.services.label.AbstractModelLabelProvider;

import com.wiley.core.model.components.bootstrapgrid.WileyBootstrapColumnConfigurationModel;


public class WileyBootstrapColumnConfigurationLabelProvider
		extends AbstractModelLabelProvider<WileyBootstrapColumnConfigurationModel>
{
	@Override
	protected String getItemLabel(final WileyBootstrapColumnConfigurationModel model)
	{
		if (model.getVisible())
		{
			return String.format("%s: [width = %d, offset %d]", model.getBreakpoint().getQualifier(),
					model.getWidth(), model.getOffset());
		}
		else
		{
			return String.format("%s: [invisible]", model.getBreakpoint().getQualifier());
		}

	}

	@Override
	protected String getItemLabel(final WileyBootstrapColumnConfigurationModel model, final String languageIso)
	{
		return getItemLabel(model);
	}

	@Override
	protected String getItemDescription(final WileyBootstrapColumnConfigurationModel wileyBootstrapColumnConfigurationModel)
	{
		return null;
	}

	@Override
	protected String getItemDescription(final WileyBootstrapColumnConfigurationModel wileyBootstrapColumnConfigurationModel,
			final String s)
	{
		return null;
	}

	@Override
	protected String getIconPath(final WileyBootstrapColumnConfigurationModel wileyBootstrapColumnConfigurationModel)
	{
		return null;
	}

	@Override
	protected String getIconPath(final WileyBootstrapColumnConfigurationModel wileyBootstrapColumnConfigurationModel,
			final String s)
	{
		return null;
	}
}