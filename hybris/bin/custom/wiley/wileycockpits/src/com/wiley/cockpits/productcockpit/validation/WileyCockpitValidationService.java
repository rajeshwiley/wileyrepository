package com.wiley.cockpits.productcockpit.validation;

import de.hybris.platform.cockpit.services.validation.impl.DefaultCockpitValidationService;
import de.hybris.platform.cockpit.services.validation.pojos.CockpitValidationDescriptor;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.validation.exceptions.HybrisConstraintViolation;
import de.hybris.platform.validation.model.constraints.ConstraintGroupModel;

import java.util.Collection;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.validation.service.ConstraintGroupService;


public class WileyCockpitValidationService extends DefaultCockpitValidationService
{
	private ConstraintGroupService constraintGroupService;

	@Override
	public Set<CockpitValidationDescriptor> validateModel(final ItemModel newItemModel)
	{

		final Set<HybrisConstraintViolation> violations;
		final Collection<ConstraintGroupModel> itemConstraints
				= constraintGroupService.getCatalogSpecificConstraintGroupForItem(newItemModel);
		if (CollectionUtils.isNotEmpty(itemConstraints)) {
			violations = getValidationService().validate(newItemModel, itemConstraints);
		} else {
			violations = getValidationService().validate(newItemModel, new Class[0]);
		}
		return convertToValidationDescriptors(violations);
	}

	@Required
	public void setConstraintGroupService(final ConstraintGroupService constraintGroupService)
	{
		this.constraintGroupService = constraintGroupService;
	}
}
