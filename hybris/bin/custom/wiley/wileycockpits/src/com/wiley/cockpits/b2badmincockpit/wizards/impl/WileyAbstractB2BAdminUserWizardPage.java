package com.wiley.cockpits.b2badmincockpit.wizards.impl;

import de.hybris.platform.cockpit.wizards.Message;
import de.hybris.platform.cockpit.wizards.impl.DefaultPage;
import de.hybris.platform.cockpit.wizards.impl.DefaultWizardContext;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

public class WileyAbstractB2BAdminUserWizardPage extends DefaultPage
{

	protected ArrayList<String> attributesToValidate;
	protected boolean valid;

	private static final Map<String, String> ATTRIBUTE_NAMES = new HashMap<String, String>()
	{
		private static final long serialVersionUID = 1L;

		{
			put("unitId", "B2B unit id");
			put("uid", "user id");
			put("password", "password");
			put("firstName", "first name");
			put("lastName", "last name");
			put("email", "email");
		}
	};

	public ArrayList<String> getAttributesToValidate()
	{
		return this.attributesToValidate;
	}

	public void setAttributesToValidate(final ArrayList<String> attributesToValidate)
	{
		this.attributesToValidate = attributesToValidate;
	}

	public Map<String, Object> getAttributes()
	{
		Map<String, Object> attributes = (Map<String, Object>) getWizard().getWizardContext().getAttribute(getId());
		Map<String, Object> newAttributes = new HashMap<>();

		if (attributes != null)
		{
			newAttributes.putAll(attributes);
		}

		return newAttributes;
	}

	public void setAttribute(final String attributeName, final Object value)
	{
		Map<String, Object> attributes = getAttributes();
		Map<String, Object> newAttributes = new HashMap<>();

		if (attributes != null)
		{
			newAttributes.putAll(attributes);
		}

		newAttributes.put(attributeName, value);

		((DefaultWizardContext) getWizard().getWizardContext()).setAttribute(getId(), newAttributes);
	}

	public Object getAttribute(final String attributeName)
	{
		return getAttributes().get(attributeName);
	}

	protected List<Message> validateAttributes()
	{
		List<Message> validationMessages = new ArrayList<>();

		if (this.attributesToValidate != null)
		{
			Iterator<String> attributeIterator = this.attributesToValidate.iterator();
			boolean continueLoop = true;
			while (attributeIterator.hasNext() && continueLoop)
			{
				String attributeName = (String) attributeIterator.next();
				boolean valid;
				if (getAttribute(attributeName) instanceof String)
				{
					valid = StringUtils.isNotBlank((String) getAttribute(attributeName));
				}
				else
				{
					valid = getAttribute(attributeName) != null;
				}

				if (!valid)
				{
					validationMessages.add(new Message(Message.ERROR,
							"Enter a value for " + ATTRIBUTE_NAMES.get(attributeName) + ".", attributeName));
					continueLoop = false;
				}
			}

		}

		return validationMessages;
	}

	public List<Message> validate()
	{
		List<Message> validationMessages = new ArrayList<>();

		return validationMessages;
	}

}
