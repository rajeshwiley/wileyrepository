package com.wiley.cockpits.b2badmincockpit.components.browserarea;


import de.hybris.platform.cockpit.components.listview.ListViewAction;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;


public interface WileyTypeViewSpecificButtonAction
{
	List<String> getEnabledForTypes();

	default boolean enabledForCurrentType(final ListViewAction.Context context)
	{
		return CollectionUtils.isNotEmpty(getEnabledForTypes())
				&& getEnabledForTypes().contains(context.getBrowserModel().getRootType().getCode());
	}
}
