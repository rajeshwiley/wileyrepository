package com.wiley.cockpits.cmscockpit.services;

import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.services.sync.SynchronizationService;


/**
 * Created by Uladzimir_Barouski on 4/5/2017.
 */
public interface WileySynchronizationService extends SynchronizationService
{
	boolean canSync(TypedObject object);
}
