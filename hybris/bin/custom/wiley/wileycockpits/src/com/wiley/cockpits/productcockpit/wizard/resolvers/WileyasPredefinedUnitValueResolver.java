package com.wiley.cockpits.productcockpit.wizard.resolvers;

import de.hybris.platform.cockpit.wizards.generic.DefaultValueResolver;
import de.hybris.platform.product.UnitService;

import javax.annotation.Resource;

public class WileyasPredefinedUnitValueResolver implements DefaultValueResolver
{
    @Resource
    private UnitService unitService;

    @Override
    public Object getValue(final String value)
    {
        return unitService.getUnitForCode(value);
    }
}
