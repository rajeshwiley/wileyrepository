package com.wiley.cockpits.cockpit.model.listview.aspect;

import de.hybris.platform.jalo.JaloItemNotFoundException;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;


/**
 * This Aspects wraps isCellEditable method of DefaultTableModel by try catch to avoid errors on ContextAreaTable rendering for
 * multi types list objects.
 *
 * Created by Uladzimir_Barouski on 3/29/2017.
 */
@Aspect
public class WileyContextAreaTableModelAspect
{
	@Around("execution( boolean de.hybris.platform.cockpit.model.listview.impl.DefaultTableModel.isCellEditable(..)) ")
	public Object isCellEditableAdvice(final ProceedingJoinPoint jp) throws Throwable
	{
		try
		{
			return jp.proceed();
		}
		catch (JaloItemNotFoundException e)
		{
			return false;
		}
	}
}
