package com.wiley.cockpits.cockpit.wizard.media;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cms2.servicelayer.services.admin.CMSAdminSiteService;
import de.hybris.platform.cockpit.session.UISessionUtils;
import de.hybris.platform.cockpit.wizards.media.UploadMediaFirstPage;

import java.util.Map;

import org.zkoss.util.media.Media;


public class WileyUploadMediaFirstPage extends UploadMediaFirstPage
{
	private static final String MEDIA_CATALOG_VERSION_KEY = "Media.catalogVersion";
	private CMSAdminSiteService cmsAdminSiteService;

	@Override
	protected Map<String, Object> getMediaPredefinedAttributes(final Media media)
	{
		Map<String, Object> predefinedAttributes = super.getMediaPredefinedAttributes(media);
		if (isContextCatalogVersionEmpty(predefinedAttributes))
		{
			setActiveCatalogInContext(predefinedAttributes);
		}
		return predefinedAttributes;
	}

	private void setActiveCatalogInContext(final Map<String, Object> predefinedAttributes)
	{
		CatalogVersionModel activeCatalogVersion = cmsAdminSiteService.getActiveCatalogVersion();
		predefinedAttributes.put(MEDIA_CATALOG_VERSION_KEY,
				UISessionUtils.getCurrentSession().getTypeService().wrapItem(activeCatalogVersion));
	}

	private boolean isContextCatalogVersionEmpty(final Map<String, Object> predefinedAttributes)
	{
		return predefinedAttributes.get(MEDIA_CATALOG_VERSION_KEY) == null;
	}

	public CMSAdminSiteService getCmsAdminSiteService()
	{
		return cmsAdminSiteService;
	}

	public void setCmsAdminSiteService(final CMSAdminSiteService cmsAdminSiteService)
	{
		this.cmsAdminSiteService = cmsAdminSiteService;
	}

}
