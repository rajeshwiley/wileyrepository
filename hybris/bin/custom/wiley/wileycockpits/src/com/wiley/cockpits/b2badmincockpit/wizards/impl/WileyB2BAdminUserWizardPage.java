package com.wiley.cockpits.b2badmincockpit.wizards.impl;

import de.hybris.platform.b2b.services.B2BCustomerService;
import de.hybris.platform.b2b.services.B2BUnitService;
import de.hybris.platform.catalog.model.CompanyModel;
import de.hybris.platform.cockpit.wizards.Message;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class WileyB2BAdminUserWizardPage extends WileyAbstractB2BAdminUserWizardPage
{

	private static final Logger LOG = Logger.getLogger(WileyB2BAdminUserWizardPage.class);
	private static final String PASSWORD_REGEX = "^[a-zA-Z0-9]{5,32}$";
	private static final String UNITID_ATTRIBUTE = "unitId";
	private static final String UID_ATTRIBUTE = "uid";
	private static final String PASSWORD_ATTRIBUTE = "password";
	private static final String FIRSTNAME_ATTRIBUTE = "firstName";
	private static final String LASTNAME_ATTRIBUTE = "lastName";
	private static final String EMAIL_ATTRIBUTE = "email";
	private static final int UID_LENGTH = 256;
	private static final int FIRST_NAME_LENGTH = 150;
	private static final int LAST_NAME_LENGTH = 255;

	@Autowired
	private B2BCustomerService b2bCustomerService;

	@Autowired
	private B2BUnitService b2bUnitService;

	@Autowired
	private UserService userService;

	public List<Message> validate()
	{
		List<Message> validationMessages = new ArrayList<>();
		String unitId = (String) getAttribute(UNITID_ATTRIBUTE);
		String customerUid = (String) getAttribute(UID_ATTRIBUTE);
		String password = (String) getAttribute(PASSWORD_ATTRIBUTE);
		String firstName = (String) getAttribute(FIRSTNAME_ATTRIBUTE);
		String lastName = (String) getAttribute(LASTNAME_ATTRIBUTE);
		String email = (String) getAttribute(EMAIL_ATTRIBUTE);

		if (StringUtils.isNotBlank(unitId))
		{
			CompanyModel unit = b2bUnitService.getUnitForUid(unitId);
			if (unit == null)
			{
				validationMessages.add(
						new Message(Message.ERROR, "The B2B unit id you entered does not exists in hybris.", UNITID_ATTRIBUTE));
			}
		}

		if (StringUtils.isNotBlank(customerUid))
		{
			if (customerUid.length() > UID_LENGTH)
			{
				validationMessages.add(new Message(Message.ERROR, "The user id you entered is too long.", UID_ATTRIBUTE));
			}

			try
			{
				UserModel admin = userService.getUserForUID(customerUid);

				if (admin != null)
				{
					validationMessages
							.add(new Message(Message.ERROR, "The user id you entered already exists in hybris.", UID_ATTRIBUTE));
				}
			}
			catch (final UnknownIdentifierException e)
			{
				LOG.info(String.format("No account exists with id %s", customerUid));
			}
		}

		if (StringUtils.isNotBlank(password))
		{
			Pattern pattern = Pattern.compile(PASSWORD_REGEX);
			Matcher matcher = pattern.matcher(password);
			if (!matcher.matches())
			{
				validationMessages.add(new Message(Message.ERROR,
						"Password must be alphanumeric (no special characters) between 5 and 32 characters long.",
						PASSWORD_ATTRIBUTE));
			}
		}

		if (StringUtils.isNotBlank(firstName))
		{
			if (firstName.length() > FIRST_NAME_LENGTH)
			{
				validationMessages
						.add(new Message(Message.ERROR, "The first name you entered is too long.", FIRSTNAME_ATTRIBUTE));
			}
		}

		if (StringUtils.isNotBlank(lastName))
		{
			if (lastName.length() > LAST_NAME_LENGTH)
			{
				validationMessages
						.add(new Message(Message.ERROR, "The last name you entered is too long.", LASTNAME_ATTRIBUTE));
			}
		}

		if (StringUtils.isNotBlank(email))
		{
			if (!org.apache.commons.validator.routines.EmailValidator.getInstance().isValid(email))
			{
				validationMessages.add(new Message(Message.ERROR, "The email you entered is not valid.", EMAIL_ATTRIBUTE));
			}
		}
		return validationMessages;
	}
}
