package com.wiley.cockpits.cmscockpit.wizard.strategies;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cms2.servicelayer.services.admin.CMSAdminSiteService;
import de.hybris.platform.cockpit.session.impl.CreateContext;
import de.hybris.platform.cockpit.wizards.generic.strategies.PredefinedValuesStrategy;
import org.springframework.beans.factory.annotation.Required;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;


public class WileyCatalogAwareItemPredefinedValuesStrategy implements PredefinedValuesStrategy
{
	private String itemType;
	private String catalogId;

	@Resource
	private CMSAdminSiteService cmsAdminSiteService;
	@Resource
	private CatalogVersionService catalogVersionService;

	@Override
	public Map<String, Object> getPredefinedValues(final CreateContext createContext)
	{
		final Map<String, Object> ret = new HashMap<>();
		ret.put(buildPropertyKey(itemType), getCatalogVersion());
		return ret;
	}

	protected CatalogVersionModel getCatalogVersion()
	{
		final CatalogVersionModel activeCatalogVersion = cmsAdminSiteService.getActiveCatalogVersion();
		return catalogVersionService.getCatalogVersion(catalogId, activeCatalogVersion.getVersion());
	}

	private String buildPropertyKey(final String itemType)
	{
		return itemType + ".catalogVersion";
	}

	@Required
	public void setItemType(final String itemType)
	{
		this.itemType = itemType;
	}

	public String getItemType()
	{
		return itemType;
	}

	@Required
	public void setCatalogId(final String catalogId)
	{
		this.catalogId = catalogId;
	}

	public String getCatalogId()
	{
		return catalogId;
	}
}
