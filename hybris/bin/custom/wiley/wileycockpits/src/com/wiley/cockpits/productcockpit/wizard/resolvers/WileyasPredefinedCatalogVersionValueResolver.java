package com.wiley.cockpits.productcockpit.wizard.resolvers;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.cockpit.wizards.generic.DefaultValueResolver;

import javax.annotation.Resource;

public class WileyasPredefinedCatalogVersionValueResolver implements DefaultValueResolver
{
    private static final String CATALOG_VERSION = "Staged";

    @Resource
    private CatalogVersionService catalogVersionService;

    @Override
    public Object getValue(final String value)
    {
        return catalogVersionService.getCatalogVersion(value, CATALOG_VERSION);
    }
}
