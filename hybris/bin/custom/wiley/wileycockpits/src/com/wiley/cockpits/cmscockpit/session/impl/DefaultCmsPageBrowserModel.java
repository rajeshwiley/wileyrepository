/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.cockpits.cmscockpit.session.impl;

import de.hybris.platform.cms2.servicelayer.services.admin.CMSAdminComponentService;
import de.hybris.platform.cms2.servicelayer.services.admin.CMSAdminContentSlotService;
import de.hybris.platform.cms2.servicelayer.services.admin.CMSAdminSiteService;
import de.hybris.platform.cmscockpit.components.contentbrowser.CmsPageMainAreaEditComponentFactory;
import de.hybris.platform.cmscockpit.components.contentbrowser.CmsPageMainAreaPersonalizeComponentFactory;
import de.hybris.platform.cmscockpit.services.CmsCockpitService;
import de.hybris.platform.cmscockpit.session.impl.CmsPageBrowserModel;
import de.hybris.platform.cockpit.components.contentbrowser.AbstractContentBrowser;
import de.hybris.platform.cockpit.components.contentbrowser.MainAreaComponentFactory;
import de.hybris.platform.cockpit.events.CockpitEvent;
import de.hybris.platform.cockpit.events.impl.ItemChangedEvent;
import de.hybris.platform.cockpit.model.listview.impl.SectionTableModel;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.session.BrowserSectionModel;
import de.hybris.platform.cockpit.session.Lockable;
import de.hybris.platform.cockpit.session.UISessionUtils;
import de.hybris.platform.cockpit.session.impl.AbstractBrowserArea;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.wiley.cockpits.cmscockpit.components.contentbrowser.DefaultCmsPageContentBrowser;
import com.wiley.cockpits.cmscockpit.components.contentbrowser.DefaultCmsPageMainAreaPreviewComponentFactory;


/**
 * Default {@link CmsPageBrowserModel} for accelerator cockpits.
 */
public class DefaultCmsPageBrowserModel extends CmsPageBrowserModel
{
	private List<MainAreaComponentFactory> viewModes = null;
	private TypedObject page;

	/**
	 * Instantiates a new Default cms page browser model.
	 *
	 * @param cmsAdminSiteService
	 * 		the cms admin site service
	 * @param cmsCockpitService
	 * 		the cms cockpit service
	 * @param modelService
	 * 		the model service
	 * @param cmsAdminComponentService
	 * 		the cms admin component service
	 * @param cmsAdminContentSlotService
	 * 		the cms admin content slot service
	 */
	public DefaultCmsPageBrowserModel(final CMSAdminSiteService cmsAdminSiteService, final CmsCockpitService cmsCockpitService,
			final ModelService modelService, final CMSAdminComponentService cmsAdminComponentService,
			final CMSAdminContentSlotService cmsAdminContentSlotService)
	{
		super(cmsAdminSiteService, cmsCockpitService, modelService, cmsAdminComponentService, cmsAdminContentSlotService);
	}

	/**
	 * Gets available view modes.
	 *
	 * @return the available view modes
	 */
	@Override
	public List<MainAreaComponentFactory> getAvailableViewModes()
	{
		if (viewModes == null)
		{
			viewModes = new ArrayList<MainAreaComponentFactory>();
			viewModes.add(newCmsPageMainAreaEditComponentFactory());
			viewModes.add(newDefaultCmsPageMainAreaPreviewComponentFactory());
			viewModes.add(newCmsPageMainAreaPersonalizeComponentFactory());
		}
		return viewModes;
	}

	/**
	 * Clone object.
	 *
	 * @return the object
	 * @throws CloneNotSupportedException
	 * 		the clone not supported exception
	 */
	@Override
	public Object clone() throws CloneNotSupportedException //NOSONAR
	{
		final DefaultCmsPageBrowserModel browserModel = newDefaultCmsPageBrowserModel();
		browserModel.setCurrentPageObject(getCurrentPageObject());
		browserModel.createProperViewModel();
		browserModel.setViewMode(getViewMode());
		return browserModel;
	}

	/**
	 * New default cms page browser model default cms page browser model.
	 *
	 * @return the default cms page browser model
	 */
	protected DefaultCmsPageBrowserModel newDefaultCmsPageBrowserModel()
	{
		return new DefaultCmsPageBrowserModel(cmsAdminSiteService, cmsCockpitService, modelService, cmsAdminComponentService,
				cmsAdminContentSlotService);
	}

	/**
	 * New cms page main area edit component factory cms page main area edit component factory.
	 *
	 * @return the cms page main area edit component factory
	 */
	protected CmsPageMainAreaEditComponentFactory newCmsPageMainAreaEditComponentFactory()
	{
		return new CmsPageMainAreaEditComponentFactory();
	}

	/**
	 * New default cms page main area preview component factory default cms page main area preview component factory.
	 *
	 * @return the default cms page main area preview component factory
	 */
	protected DefaultCmsPageMainAreaPreviewComponentFactory newDefaultCmsPageMainAreaPreviewComponentFactory()
	{
		return new DefaultCmsPageMainAreaPreviewComponentFactory(getCurrentPageObject());
	}

	/**
	 * New cms page main area personalize component factory cms page main area personalize component factory.
	 *
	 * @return the cms page main area personalize component factory
	 */
	protected CmsPageMainAreaPersonalizeComponentFactory newCmsPageMainAreaPersonalizeComponentFactory()
	{
		return new CmsPageMainAreaPersonalizeComponentFactory();
	}

	/**
	 * Create view component abstract content browser.
	 *
	 * @return the abstract content browser
	 */
	@Override
	public AbstractContentBrowser createViewComponent()
	{
		return new DefaultCmsPageContentBrowser();
	}

	/**
	 * Is back button visible boolean.
	 *
	 * @return the boolean
	 */
	@Override
	public boolean isBackButtonVisible()
	{
		return DefaultCmsPageMainAreaPreviewComponentFactory.VIEWMODE_ID.equals(getViewMode());
	}

	/**
	 * On cockpit event.
	 *
	 * @param event
	 * 		the event
	 */
	@Override
	public void onCockpitEvent(final CockpitEvent event)
	{
		// make sure newly created item is selected
		if (event instanceof ItemChangedEvent)
		{
			final ItemChangedEvent changedEvent = (ItemChangedEvent) event;
			switch (changedEvent.getChangeType())
			{
				case CREATED:
					final TypedObject createdItem = changedEvent.getItem();
					if (changedEvent.getSource() instanceof BrowserSectionModel)
					{

						final BrowserSectionModel sectionModel = (BrowserSectionModel) changedEvent.getSource();
						final List<TypedObject> sectionItems = sectionModel.getItems();

						if (sectionItems != null && !sectionItems.isEmpty())
						{
							final int itemIndex = sectionItems.indexOf(createdItem);
							if (itemIndex != -1)
							{
								sectionModel.update();
								sectionModel.setSelectedIndex(itemIndex);

								getContentEditorSection().setRootItem(createdItem);
								getContentEditorSection().setVisible(true);
								getContentEditorSection().update();
							}
						}
					}
					else if (changedEvent.getSource() == null)
					{
						//createProperViewModel();
						for (final BrowserSectionModel sectionModel : getBrowserSectionModels())
						{
							if (sectionModel.getItems().contains(createdItem))
							{
								final int selectedIndex = sectionModel.getItems().indexOf(createdItem);
								sectionModel.setSelectedIndex(selectedIndex);
							}
						}
						getContentEditorSection().setRootItem(createdItem);
						getContentEditorSection().setVisible(true);
						getContentEditorSection().update();
						updateItems();
					}
					break;

				case REMOVED:

					// if a page is deleted make sure any related struct tabs are closed
					if (changedEvent.getItem().equals(getCurrentPageObject()))
					{
						getArea().close(this);
					}

					if (changedEvent.getSource() instanceof SectionTableModel)
					{
						final BrowserSectionModel sectionModel = ((SectionTableModel) changedEvent.getSource()).getModel();

						final List<TypedObject> sectionItems = sectionModel.getItems();
						if (sectionItems != null && !sectionItems.isEmpty() && sectionItems.contains(changedEvent.getItem()))
						{
							final int removedIndex = sectionItems.indexOf(changedEvent.getItem());
							if (sectionModel.getSelectedIndex() != null)
							{
								if (removedIndex < sectionModel.getSelectedIndex().intValue())
								{
									sectionModel.setSelectedIndex(sectionModel.getSelectedIndex().intValue() - 1);
								}
								else if (removedIndex == sectionModel.getSelectedIndex().intValue())
								{
									sectionModel.setSelectedIndexes(Collections.EMPTY_LIST);
								}
							}
							removeComponentFromSlot((TypedObject) sectionModel.getRootItem(), changedEvent.getItem());
							sectionModel.update();
						}

						if (getContentEditorSection().getRootItem() != null && getContentEditorSection().getRootItem().equals(
								changedEvent.getItem()))
						{
							getContentEditorSection().setRootItem(null);
							getContentEditorSection().setVisible(false);
						}

						final AbstractBrowserArea area =
								(AbstractBrowserArea) UISessionUtils.getCurrentSession().getCurrentPerspective().getBrowserArea();
						final AbstractContentBrowser content = area.getCorrespondingContentBrowser(this);
						if (content != null && content.getToolbarComponent() != null)
						{
							content.getToolbarComponent().update();
						}

					}
					break;

				case CHANGED:
					for (final BrowserSectionModel sectionModel : getBrowserSectionModels())
					{
						if (sectionModel.equals(event.getSource()))
						{
							continue;
						}
						final List<TypedObject> sectionItems = sectionModel.getItems();
						final TypedObject changedItem = changedEvent.getItem();
						if (sectionItems.contains(changedItem))
						{
							final TypedObject typedObject = sectionItems.get(sectionItems.indexOf(changedItem));
							modelService.refresh(typedObject.getObject());
							sectionModel.update();
						}
						if ((sectionModel.getRootItem() != null && sectionModel.getRootItem().equals(changedItem)))
						{
							final TypedObject rootItem = (TypedObject) sectionModel.getRootItem();
							final ItemModel itemModel = (ItemModel) rootItem.getObject();
							modelService.refresh(itemModel);
							if (sectionModel instanceof Lockable)
							{
								getContentEditorSection().setReadOnly(((Lockable) sectionModel).isLocked());
							}
							sectionModel.update();
						}
					}
					break;

				default:
					//					TODO: find out what is the correct way to handle other cases
			}
		}
	}


	protected void createProperViewModel()
	{
		if (getViewMode() != null && CmsPageMainAreaEditComponentFactory.VIEW_MODE_ID.equals(getViewMode()))
		{
			createAndInitializeFlatOrStructureView();
		}
	}

	/**
	 * Sets given page as am active <p> Note: </p>
	 *
	 * @param page
	 * 		given page
	 */
	@Override
	public void setCurrentPageObject(final TypedObject page)
	{
		this.page = page;
	}

	/**
	 * Returns current page <p> Note: </p>
	 *
	 * @return current page
	 */
	@Override
	public TypedObject getCurrentPageObject()
	{
		return this.page;
	}


}
