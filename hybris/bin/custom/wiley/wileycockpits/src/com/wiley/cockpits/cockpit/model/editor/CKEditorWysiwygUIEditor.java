package com.wiley.cockpits.cockpit.model.editor;

import de.hybris.platform.cockpit.model.editor.EditorListener;
import de.hybris.platform.cockpit.model.editor.impl.AbstractUIEditor;
import de.hybris.platform.cockpit.session.UISessionUtils;
import de.hybris.platform.cockpit.util.UITools;

import java.util.Collections;
import java.util.Map;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.displaytag.util.HtmlTagUtil;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Html;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Toolbarbutton;
import org.zkoss.zul.Vbox;
import org.zkoss.zul.Window;
import org.zkoss.zul.impl.InputElement;

import com.wiley.cockpits.cockpit.components.WileyCKEditor;


/**
 * CKEditor update
 */
public class CKEditorWysiwygUIEditor extends AbstractUIEditor
{
	private static final Logger LOG = Logger.getLogger(CKEditorWysiwygUIEditor.class);
	private static final String DEFAULT_EDITOR_HEIGHT = "350px";
	private static final String DEFAULT_POPUP_EDITOR_WIDTH = "75%";
	private static final String DEFAULT_POPUP_EDITOR_HEIGHT = "450px";
	private static final int DEFAULT_EDITOR_ROWS = 8;
	private static final int MAX_LABEL_LENGTH = 50;
	private static final String PREVIEW_BTN = "/cockpit/images/icon_func_preview_available.png";
	private static final String OPEN_BTN = "/cockpit/images/icon_func_inlinetext_openwysiwyg.png";
	private static final String REVERT_BTN = "/cockpit/images/icon_func_inlinetext_revert.png";
	private static final String EMPTY_STRING_REGEXP = "^[\\s\\u00A0]*$";
	private static final String HTML_TAGS_REGEXP = "(.*<(a|img|div|iframe|embed|audio|video).*>)+";
	private static final String WILEYB2C_STYLE_CSS = "/cmscockpit/cmscockpit/css/style.css";
	public static final String EVENT_ONCLICK = "onClick";
	private boolean inline = false;

	private WileyCKEditor createWileyCKEditor()
	{
		WileyCKEditor wileyCKEditor = new WileyCKEditor(UISessionUtils.getCurrentSession().getLanguageIso());
		wileyCKEditor.setAdditionalContentsCss(Collections.singletonList(WILEYB2C_STYLE_CSS));
		return wileyCKEditor;
	}

	private HtmlBasedComponent createWysiwygInline(final String html, final EditorListener listener,
			final Map<String, ?> parameters)
	{
		final Div parentComponent = new Div();
		String width = StringUtils.defaultIfEmpty((String) parameters.get("editorWidth"), "100%");
		parentComponent.setWidth(width);

		final WileyCKEditor wileyCKEditor = this.createWileyCKEditor();
		String height = StringUtils.defaultIfEmpty((String) parameters.get("editorHeight"), DEFAULT_EDITOR_HEIGHT);
		wileyCKEditor.setHeight(height);

		final String wileyCKEditorToolbarConfig = (String) parameters.get("wileyCKEditorToolbarConfig");
		if (wileyCKEditorToolbarConfig != null)
		{
			wileyCKEditor.setToolbarSet(wileyCKEditorToolbarConfig);
		}
		else if (this.getDefaultToolbarConfig() != null)
		{
			wileyCKEditor.setToolbarSet(this.getDefaultToolbarConfig());
		}

		wileyCKEditor.setInline(true);
		wileyCKEditor.setValue(html);
		wileyCKEditor.addEventListener("onUser", event ->
		{
			String currentValue = (String) event.getData();
			wileyCKEditor.setValue(currentValue);
			String strippedConvertedText = HtmlTagUtil.stripHTMLTags(currentValue);
			String unescapedConvertedText = StringEscapeUtils.unescapeHtml(strippedConvertedText);
			if (this.isEmptyValue(unescapedConvertedText) && !currentValue.matches(HTML_TAGS_REGEXP))
			{
				currentValue = null;
			}

			this.setValue(currentValue);
			listener.valueChanged(currentValue);
		});
		if (this.isEditable())
		{
			parentComponent.appendChild(wileyCKEditor);
		}
		else
		{
			parentComponent.appendChild(this.createReadonlyRepresentation(html, width, height));
		}

		return parentComponent;
	}

	private Div createReadonlyRepresentation(final String html, final String width, final String height)
	{
		final Div readonlyDiv = new Div();
		readonlyDiv.setSclass("wysiwyg-readonly");
		readonlyDiv.setHeight(height);
		readonlyDiv.setWidth(width);
		final Html contentHtml = new Html(html);
		readonlyDiv.appendChild(contentHtml);
		return readonlyDiv;
	}

	private Window createPopupWysiwyg(final Textbox textbox, final EditorListener listener,
			final Map<String, ?> parameters)
	{
		final Window ret = new Window();
		ret.setVisible(false);
		String label = Labels.getLabel("general.edit");
		if (!this.isEditable())
		{
			label = label + " - " + Labels.getLabel("general.writeprotected");
			ret.setClosable(true);
		}

		ret.setTitle(label);
		ret.setShadow(false);
		String width = StringUtils.defaultIfEmpty((String) parameters.get("editorWidth"), DEFAULT_POPUP_EDITOR_WIDTH);
		ret.setWidth(width);

		final Vbox box = new Vbox();
		ret.appendChild(box);
		box.setHeights("none,25px");
		box.setWidth("100%");
		final WileyCKEditor wileyCKEditor = this.createWileyCKEditor();

		String height = StringUtils.defaultIfEmpty((String) parameters.get("editorHeight"), DEFAULT_POPUP_EDITOR_HEIGHT);
		wileyCKEditor.setHeight(height);

		final String wileyCKEditorToolbarConfig = (String) parameters.get("wileyCKEditorToolbarConfig");
		if (wileyCKEditorToolbarConfig != null)
		{
			wileyCKEditor.setToolbarSet(wileyCKEditorToolbarConfig);
		}
		else
		{
			String defaultToolbarConfig = this.getDefaultToolbarConfig();

			if (defaultToolbarConfig != null)
			{
				wileyCKEditor.setToolbarSet(defaultToolbarConfig);
			}
		}

		Div buttonFrame;
		if (this.isEditable())
		{
			wileyCKEditor.setParent(box);
		}
		else
		{
			buttonFrame = this.createReadonlyRepresentation(textbox.getText(), "600px", "400px");
			buttonFrame.setParent(box);
		}

		wileyCKEditor.setValue(textbox.getText());
		if (this.isEditable())
		{
			buttonFrame = new Div();
			buttonFrame.setSclass("wizardButtonFrame");
			final Hbox buttonBox = new Hbox();
			buttonBox.setWidth("100%");
			final Div leftDiv = new Div();
			final Div rightDiv = new Div();
			rightDiv.setAlign("right");
			final Button okBtn = new Button(Labels.getLabel("general.ok"));
			okBtn.setTooltiptext(Labels.getLabel("general.ok"));
			okBtn.setAction("onclick: triggerFieldUpdate('" + wileyCKEditor.getId() + "');");
			wileyCKEditor.addEventListener("onUser", event ->
			{
				String currentValue = (String) event.getData();
				wileyCKEditor.setValue(currentValue);
				String strippedConvertedText = HtmlTagUtil.stripHTMLTags(currentValue);
				String unescapedConvertedText = StringEscapeUtils.unescapeHtml(strippedConvertedText);
				if (isEmptyValue(unescapedConvertedText)
						&& !currentValue.matches(HTML_TAGS_REGEXP))
				{
					currentValue = null;
				}
				this.setValue(currentValue);
				listener.valueChanged(currentValue);
				listener.actionPerformed("force_save_clicked");
				textbox.setText(currentValue);
				ret.detach();

			});
			final Button cancelBtn = new Button(Labels.getLabel("general.cancel"));
			cancelBtn.setSclass("cancelButton");
			cancelBtn.setTooltiptext(Labels.getLabel("general.cancel"));
			cancelBtn.addEventListener(EVENT_ONCLICK, event ->
			{
				ret.detach();
				listener.actionPerformed("cancel_clicked");
			});
			UITools.applyTestID(leftDiv, "wizardCancelButton");
			UITools.applyTestID(rightDiv, "wizardDoneButton");
			leftDiv.appendChild(cancelBtn);
			rightDiv.appendChild(okBtn);
			buttonBox.appendChild(leftDiv);
			buttonBox.appendChild(rightDiv);
			buttonFrame.appendChild(buttonBox);
			buttonFrame.setParent(box);
		}

		return ret;
	}

	public HtmlBasedComponent createViewComponent(final Object initialValue, final Map<String, ?> parameters,
			final EditorListener listener)
	{
		if (parameters != null && !parameters.isEmpty())
		{
			this.inline = parameters.get("inline") != null && Boolean.parseBoolean((String) parameters.get("inline"));
		}

		if (this.inline)
		{
			return this.createWysiwygInline((String) initialValue, listener, parameters);
		}
		else
		{
			String value = null;
			if (initialValue instanceof String)
			{
				value = (String) initialValue;
			}
			else if (initialValue != null)
			{
				LOG.error("Initial value not of type String.");
			}

			final Div editorView = new Div();
			final Textbox contentComponent = new Textbox(value);
			contentComponent.setRows(DEFAULT_EDITOR_ROWS);
			final Object rows = parameters == null ? null : parameters.get("rows");
			if (rows instanceof String)
			{
				try
				{
					contentComponent.setRows(Integer.parseInt((String) rows));
				}
				catch (final Exception var13)
				{
					LOG.error(var13.getMessage(), var13);
				}
			}

			final String attQual = (String) (parameters != null ? parameters.get("attributeQualifier") : null);
			final EventListener onElementClick = event ->
			{
				final Window win = this.createPopupWysiwyg(contentComponent, listener, parameters);
				win.setParent(editorView.getRoot());
				win.doHighlighted();
			};
			if (this.isEditable())
			{
				contentComponent.setParent(editorView);
				editorView.setSclass("wysiwygEditor");
				final WysiwygButtonContainer editorContainer1 = this.createViewComponentInternal(
						contentComponent, listener, parameters, onElementClick);
				editorContainer1.setParent(editorView);
				this.setTestId(editorView, attQual);
				return editorView;
			}
			else
			{
				final Hbox editorContainer = new Hbox();
				editorContainer.setWidth("100%");
				editorContainer.setWidths(",22px");
				editorContainer.setParent(editorView);
				final Label label = new Label();
				label.setMaxlength(MAX_LABEL_LENGTH);
				label.setValue(value);
				label.setParent(editorContainer);
				final Toolbarbutton btnPreview = new Toolbarbutton("", PREVIEW_BTN);
				btnPreview.setParent(editorContainer);
				btnPreview.addEventListener(EVENT_ONCLICK, onElementClick);
				this.setTestId(editorView, attQual);
				return editorView;
			}
		}
	}

	private static boolean isEmptyValue(final String convertedString)
	{
		return convertedString.matches(EMPTY_STRING_REGEXP);
	}

	public String getEditorType()
	{
		return "TEXT";
	}

	public boolean isInline()
	{
		return this.inline;
	}

	public void setFocus(final HtmlBasedComponent rootEditorComponent, final boolean selectAll)
	{
		Events.echoEvent(EVENT_ONCLICK, rootEditorComponent, null);
	}

	private String getDefaultToolbarConfig()
	{
		return UITools.getCockpitParameter("default.wileyCKEditorToolbarConfig", Executions.getCurrent());
	}

	private void setTestId(final HtmlBasedComponent component, final String attQual)
	{
		if (UISessionUtils.getCurrentSession().isUsingTestIDs())
		{
			String id = "Wysiwyg_";
			if (attQual != null)
			{
				id = id + attQual.replaceAll("\\W", "");
			}

			UITools.applyTestID(component, id);
		}

	}

	private WysiwygButtonContainer createViewComponentInternal(final InputElement inputElement,
			final EditorListener listener, final Map<String, ?> parameters, final EventListener openListener)
	{
		this.parseInitialInputString(parameters);
		if (!this.isEditable())
		{
			if (Executions.getCurrent().getUserAgent().contains("MSIE"))
			{
				inputElement.setReadonly(true);
			}
			else
			{
				inputElement.setDisabled(true);
			}
		}

		this.inEditMode = false;
		final WysiwygButtonContainer wysiwygButtonContainer =
				new WysiwygButtonContainer(() ->
				{
					this.inEditMode = false;
					this.setValue(this.initialEditValue);
					listener.valueChanged(this.initialEditValue);
					inputElement.setRawValue(this.getValue());
					listener.actionPerformed("cancel_clicked");
				}, openListener);
		inputElement.addEventListener("onFocus", event ->
		{
			final Object initialValue = inputElement.getRawValue();
			if (initialValue instanceof String && StringUtils.isEmpty((String) initialValue))
			{
				this.initialEditValue = null;
			}
			else
			{
				this.initialEditValue = inputElement.getRawValue();
			}

			wysiwygButtonContainer.showRevertButton(true);
			this.inEditMode = true;
		});
		inputElement.addEventListener("onBlur", event ->
		{
			final boolean valueUnchanged;
			if (inputElement.getRawValue() != null)
			{
				valueUnchanged = inputElement.getRawValue().equals(this.getValue());
			}
			else
			{
				valueUnchanged = this.getValue() == null;
			}

			if (!valueUnchanged)
			{
				if (inputElement.getRawValue() != null && !StringUtils.isEmpty(inputElement.getRawValue().toString()))
				{
					this.setValue(inputElement.getRawValue());
				}
				else
				{
					this.setValue(null);
				}

				this.fireValueChanged(listener);
			}

			wysiwygButtonContainer.showRevertButton(false);
		});
		inputElement.addEventListener("onChange", event ->
		{
			final boolean valueUnchanged;
			if (inputElement.getRawValue() != null)
			{
				valueUnchanged = inputElement.getRawValue().equals(this.getValue());
			}
			else
			{
				valueUnchanged = this.getValue() == null;
			}

			if (!valueUnchanged)
			{
				if (inputElement.getRawValue() != null && !StringUtils.isEmpty(inputElement.getRawValue().toString()))
				{
					this.setValue(inputElement.getRawValue());
				}
				else
				{
					this.setValue(null);
				}

				this.fireValueChanged(listener);
			}

		});
		inputElement.addEventListener("onOK", event ->
		{
			final boolean valueUnchanged;
			if (inputElement.getRawValue() != null)
			{
				valueUnchanged = inputElement.getRawValue().equals(this.getValue());
			}
			else
			{
				valueUnchanged = this.getValue() == null;
			}

			if (!valueUnchanged)
			{
				if (inputElement.getRawValue() != null && !StringUtils.isEmpty(inputElement.getRawValue().toString()))
				{
					this.setValue(inputElement.getRawValue());
				}
				else
				{
					this.setValue(null);
				}

				this.fireValueChanged(listener);
				listener.actionPerformed("enter_pressed");
			}

		});
		inputElement.addEventListener("onCancel", event ->
		{
			inputElement.setRawValue(this.initialEditValue);
			this.setValue(this.initialEditValue);
			this.fireValueChanged(listener);
			listener.actionPerformed("escape_pressed");
		});
		return wysiwygButtonContainer;
	}

	private static class WysiwygButtonContainer extends Div
	{
		Image openBtn = null;
		Image revertBtn = null;

		WysiwygButtonContainer(final CancelListener cancelListener, final EventListener openListener)
		{
			this.setSclass("wysiwygButtons");
			this.revertBtn = new Image(REVERT_BTN);
			this.revertBtn.setSclass("revertEditorButton");
			this.revertBtn.setVisible(false);
			this.revertBtn.setTooltiptext(Labels.getLabel("editor.button.cancel.tooltip"));
			this.revertBtn.addEventListener(EVENT_ONCLICK, event -> cancelListener.cancelPressed());
			this.appendChild(this.revertBtn);
			this.openBtn = new Image(OPEN_BTN);
			this.openBtn.setSclass("openEditorButton");
			this.openBtn.setVisible(true);
			this.openBtn.setTooltiptext(Labels.getLabel("editor.edit.tooltip"));
			this.openBtn.addEventListener(EVENT_ONCLICK, openListener);
			this.appendChild(this.openBtn);
		}

		void showRevertButton(final boolean visible)
		{
			this.revertBtn.setVisible(visible);
		}
	}
}