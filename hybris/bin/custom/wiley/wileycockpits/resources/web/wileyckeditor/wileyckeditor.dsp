<%@ taglib uri="/WEB-INF/tld/web/core.dsp.tld" prefix="c" %>
<c:set var="self" value="${requestScope.arg.self}"/>
<c:set var="edid" value="${self.uuid}!ed"/>

<div id="${self.uuid}"
	z.type="com.wiley.cockpits.cockpit.components.WileyCKEditor"
	z.val="${c:escapeXML(self.value)}"
	${c:attr('z.height', self.height)}
	${c:attr('z.languageIsocode', self.languageIsocode)}
	${c:attr('z.inline', self.inline)}>
	<div id="${self.uuid}!data" style="display:none">${self.value}</div>
	<c:forEach items="${self.additionalContentsCss}" var="url" varStatus="stat">
		<c:set var="urls" value="${stat.index == 0 ? '' : ','}${url}" />
	</c:forEach>
	<div id="${self.uuid}!additionalContentsCss" style="display:none">${urls}</div>
	<div id="${self.uuid}!toolbar" style="display:none">${self.toolbarSet}</div>
</div>