
CKEDITOR.editorConfig = function(config) {

	config.defaultLanguage  = 'en';
    config.extraPlugins = 'widget,lineutils,glyphicons';

	//Format dropdown config
	config.format_h1 = { element: 'h1' };
	config.format_h2 = { element: 'h2' };
	config.format_h3 = { element: 'h3' };
	config.format_h4 = { element: 'h4', attributes: { 'class': 'sg-title-h4' } };
	config.format_h5 = { element: 'h5', attributes: { 'class': 'sg-title-h5' } };
	config.format_h6 = { element: 'h6', attributes: { 'class': 'sg-title-h6' } };
	config.format_bodycopy1 = { element: 'p', styles: { 'font-size': '22px' } };
	config.format_bodycopy2 = { element: 'p', styles: { 'font-size': '16px' } };
	config.format_bodycopy3 = { element: 'p', styles: { 'font-size': '14px' } };
	config.format_maintext  = { element: 'p' };
	config.format_maintext_on_banners = { element: 'p', attributes: { 'class': 'banner-heading-text' } };
	config.format_heading_on_banners  = { element: 'div', attributes: { 'class': 'banner-heading' } };
	config.format_tags = 'p;h1;h2;h3;h4;h5;h6;bodycopy1;bodycopy2;bodycopy3;maintext;maintext_on_banners;heading_on_banners';

	//Fonts dropdown config
	config.font_names = "Open Sans; Source Serif; Source Serif Pro";

	//Font Size dropdown config
	config.fontSize_sizes = '14/14px; 15/15px; 16/16px; 18/18px; 20/20px; 24/24px; 26/26px; 28/28px; 32/32px; 36/36px; 42/42px';

	//Color toolbar config
	config.colorButton_enableMore = true;
	config.colorButton_enableAutomatic = false;
	config.colorButton_colors = getColors();
	config.colorButton_colorsPerRow = 4
};

function getColors() {
	var colors = "";

	//Primary color palette
	colors += '000000,'; //Black
	colors += '272829,'; //Dark Gray 1
	colors += '404142,'; //Dark Gray 2
	colors += 'FFFFFF,'; //White
	colors += 'EB1730,'; //Red
	colors += 'F07183,'; //Red 60%
	colors += 'F7B7BF,'; //Red 30%
	colors += 'FCE6E9,'; //Red 10%
	colors += '1AA6B1,'; //Teal
	colors += '78C9CF,'; //Teal 60%
	colors += 'BAE3E7,'; //Teal 30%
	colors += 'E6F5F6,'; //Teal 10%

	//Accent color
	colors += 'FF6C00,'; //Orange
	colors += 'FDA46A,'; //Orange 60%
	colors += 'FED1B3,'; //Orange 30%
	colors += 'FFEFE5,'; //Orange 10%

	// Icons, Secondary buttons
	colors += '0E3D5D,'; // Normal
	colors += '0C3D5D,'; // Dark blue
	colors += '025274,'; // Hover

	// Primary buttons, Links
	colors += '009CA9,'; // Normal
	colors += '00B1C2,'; // Hover

	// Secondary (Alerts/Errors)
	colors += 'BD1125,'; // Red
	colors += 'A41022,'; // Error message
	colors += 'FFECA4,'; // Text highlights

	// Shades of gray
	colors += '414245,'; // Titles, Addition text
	colors += 'C2C3C6,'; // Titles, Nonactive elements
	colors += 'D8D9DA,'; // Borders, dividers
	colors += 'EFEFF0'; // Backgrounds

	return colors;
}



