zkWileyCKEditor = {};

zkWileyCKEditor.init = function (ed) {

	var config = {};
 	config.allowedContent = true;

	//icon set on toolbar
	var toolbar = $e(ed.id, 'toolbar').innerHTML;
	if (toolbar) {
		config.toolbar = eval(toolbar);
	}

	//language
	var languageIsocode =  getZKAttr(ed, 'languageIsocode')
	if (languageIsocode) {
		config.language = languageIsocode;
	}

	//height
	var height =  getZKAttr(ed, 'height')
	if (height) {
		config.height = height;
	}

	//contentsCss
	var additionalContentsCssList = $e(ed, 'additionalContentsCss').innerHTML.split(',');
	if (additionalContentsCssList.length) {
		config.contentsCss = additionalContentsCssList;
	}

	//CKEditor instance creation
	var ckeditor = CKEDITOR.appendTo(ed, config, getZKAttr(ed, 'val'));
	setZKAttr(ed, 'ckeditorId',ckeditor.name);

	//change handlers
	ckeditor.on('change', function(e) {
		$e(ed.id, 'data').innerHTML = e.editor.getData();
	});
	ckeditor.on('key', function(e) {
		$e(ed.id, 'data').innerHTML = e.editor.getData();
	});

	//save data on blur change
	var inline =  getZKAttr(ed, 'inline')
	if (inline == "true") {
		ckeditor.on('blur', function(e) {
			triggerFieldUpdate(ed.id);
		});
	}


	fixFocusForCKEditorPopup(ckeditor);

}

zkWileyCKEditor.cleanup = function(ed) {
	CKEDITOR.instances[getZKAttr(ed, 'ckeditorId')].destroy();
}

function triggerFieldUpdate(id) {
	zkau.send({uuid: id, cmd: 'onUser', data: [$e(id, 'data').innerHTML] });
}

function fixFocusForCKEditorPopup (ckeditor) {
	ckeditor.on("dialogShow", function(event) {
		zkau._modals.push('modalpopup');
	});

	ckeditor.on("dialogHide", function(event) {
		zkau._modals.pop();
	});
}
