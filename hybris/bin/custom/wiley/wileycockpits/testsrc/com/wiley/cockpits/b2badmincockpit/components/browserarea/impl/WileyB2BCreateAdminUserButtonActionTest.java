package com.wiley.cockpits.b2badmincockpit.components.browserarea.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.cockpit.components.listview.ListViewAction;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyB2BCreateAdminUserButtonActionTest
{
	private static final String CREATE_USER_IMAGE_URL = "cockpit/images/create_new_user.png";

	@InjectMocks
	private WileyB2BCreateAdminUserButtonAction testedInstance;

	@Mock
	private UserService userService;

	@Mock
	private UserModel user;

	@Mock(answer = Answers.RETURNS_DEEP_STUBS)
	private ListViewAction.Context context;

	@Before
	public void setUp()
	{
		when(userService.getCurrentUser()).thenReturn(user);
	}

	@Test
	public void shouldReturnButtonImageUrl()
	{
		when(userService.isAdmin(user)).thenReturn(true);
		givenButtonIsEnabledForCurrentTypeView();

		final String imageURI = testedInstance.getMultiSelectImageURI(context);

		assertEquals(CREATE_USER_IMAGE_URL, imageURI);
	}

	@Test
	public void shouldReturnNullImageUrlIfNotEnabledForCurrentType()
	{
		when(userService.isAdmin(user)).thenReturn(true);

		final String imageURI = testedInstance.getMultiSelectImageURI(context);

		assertNull(imageURI);
	}

	@Test
	public void shouldReturnNullIfUserIsNotAdmin()
	{
		when(userService.isAdmin(user)).thenReturn(false);
		givenButtonIsEnabledForCurrentTypeView();

		final String imageURI = testedInstance.getMultiSelectImageURI(context);

		assertNull(imageURI);
	}

	private void givenButtonIsEnabledForCurrentTypeView()
	{
		testedInstance.setEnabledForTypes(Arrays.asList(B2BCustomerModel._TYPECODE));
		when(context.getBrowserModel().getRootType().getCode()).thenReturn(B2BCustomerModel._TYPECODE);
	}
}