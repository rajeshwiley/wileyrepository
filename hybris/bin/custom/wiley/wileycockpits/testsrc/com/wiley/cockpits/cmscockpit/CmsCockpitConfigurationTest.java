/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.cockpits.cmscockpit;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.basecommerce.util.BaseCommerceBaseTest;
import de.hybris.platform.cockpit.model.meta.DefaultEditorFactory;
import de.hybris.platform.cockpit.model.meta.DefaultPropertyEditorDescriptor;
import de.hybris.platform.cockpit.model.meta.PropertyEditorDescriptor;
import de.hybris.platform.core.Registry;
import de.hybris.platform.spring.ctx.ScopeTenantIgnoreDocReader;

import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.core.io.DefaultResourceLoader;


/**
 * The type Cms cockpit configuration test.
 */
@IntegrationTest
public class CmsCockpitConfigurationTest extends BaseCommerceBaseTest
{
	private static ApplicationContext applicationContext;

	/**
	 * Tests setup.
	 */
	@BeforeClass
	public static void testsSetup()
	{
		Registry.setCurrentTenantByID("junit");
		final GenericApplicationContext context = new GenericApplicationContext();
		context.setResourceLoader(new DefaultResourceLoader(Registry.class.getClassLoader()));
		context.setClassLoader(Registry.class.getClassLoader());
		context.getBeanFactory().setBeanClassLoader(Registry.class.getClassLoader());
		context.setParent(Registry.getGlobalApplicationContext());
		final XmlBeanDefinitionReader xmlReader = new XmlBeanDefinitionReader(context);
		xmlReader.setBeanClassLoader(Registry.class.getClassLoader());
		xmlReader.setDocumentReaderClass(ScopeTenantIgnoreDocReader.class);
		xmlReader.loadBeanDefinitions(getSpringConfigurationLocations());
		context.refresh();
		applicationContext = context;
	}

	/**
	 * Verify classes exist.
	 */
	@Test
	public void verifyClassesExist()
	{
		final DefaultEditorFactory factory = (DefaultEditorFactory) applicationContext.getBean("acceleratorEditorFactory");
		for (PropertyEditorDescriptor descriptor : factory.getAllEditorDescriptors())
		{
			DefaultPropertyEditorDescriptor defaultDescriptor = (DefaultPropertyEditorDescriptor) descriptor;
			for (final String editorClazz : defaultDescriptor.getEditors().values())
			{
				try
				{
					Class.forName(editorClazz);
				}
				catch (ClassNotFoundException e)
				{
					Assert.fail(String.format("Class %s used in wileycockpits/cmscockpit configuration does not exist",
							editorClazz));
				}
			}
		}
	}

	/**
	 * Destroy application context.
	 */
	@After
	public void destroyApplicationContext()
	{
		if (applicationContext != null)
		{
			((GenericApplicationContext) applicationContext).destroy();
			applicationContext = null;
		}
	}

	/**
	 * Get spring configuration locations string [ ].
	 *
	 * @return the string [ ]
	 */
	protected static String[] getSpringConfigurationLocations()
	{
		return new String[] { "cmscockpit/cmscockpit-spring-configs.xml", //
				"classpath:/wileycockpits/cmscockpit/spring/import.xml" };
	}

}
