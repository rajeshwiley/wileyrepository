package com.wiley.cockpits.cmscockpit.components;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cockpit.components.listview.ListViewAction;
import de.hybris.platform.cockpit.model.meta.TypedObject;
import de.hybris.platform.cockpit.services.label.LabelService;
import de.hybris.platform.cockpit.services.meta.TypeService;
import de.hybris.platform.cockpit.session.UISession;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Menupopup;



/**
 * Created by Uladzimir_Barouski on 4/7/2017.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileySharedComponentActionUnitTest
{
	private static final String ICON_URL = "cmscockpit/images/related_pages_list.png";
	private static final String PAGE_1_LABEL = "Page 1 label";
	private static final String PAGE_2_LABEL = "Page 2 label";
	@Mock
	private ListViewAction.Context contextMock;

	@Mock
	private AbstractCMSComponentModel componentMock;

	@Mock
	private AbstractPageModel pageModel1, pageModel2;

	@Mock
	private TypedObject componentTypedObjectMock, pageObject1, pageObject2;

	@Mock
	private UISession currentSessionMock;

	@Mock
	private TypeService typeServiceMock;

	@Mock
	private LabelService labelServiceMock;

	@Spy
	@InjectMocks
	private WileySharedComponentAction wileySharedComponentAction;

	private List<AbstractPageModel> pages;
	private List<TypedObject> relatedPages;

	@Before
	public void setUp() throws Exception
	{
		when(contextMock.getItem()).thenReturn(componentTypedObjectMock);
		when(componentTypedObjectMock.getObject()).thenReturn(componentMock);
		doReturn(currentSessionMock).when(wileySharedComponentAction).getCurrentSession();
		when(currentSessionMock.getTypeService()).thenReturn(typeServiceMock);
	}

	@Test
	public void getImageURIWhenPagesMoreThenOne() throws Exception
	{
		//Given
		pages = Arrays.asList(pageModel1, pageModel2);
		relatedPages = Arrays.asList(pageObject1, pageObject2);
		setupPages();
		//When
		String icon = wileySharedComponentAction.getImageURI(contextMock);
		//Then
		assertNotNull(icon);
		assertEquals(ICON_URL, icon);
	}

	@Test
	public void getImageURIWhenPagesLessThenOne() throws Exception
	{
		//Given
		pages = Arrays.asList(pageModel1);
		relatedPages = Arrays.asList(pageObject1);
		setupPages();
		//When
		String icon = wileySharedComponentAction.getImageURI(contextMock);
		//Then
		assertNull(icon);
	}

	@Test
	public void getMenuPopup() throws Exception
	{
		//Given
		when(currentSessionMock.getLabelService()).thenReturn(labelServiceMock);
		when(labelServiceMock.getObjectTextLabelForTypedObject(pageObject1)).thenReturn(PAGE_1_LABEL);
		when(labelServiceMock.getObjectTextLabelForTypedObject(pageObject2)).thenReturn(PAGE_2_LABEL);
		pages = Arrays.asList(pageModel1, pageModel2);
		relatedPages = Arrays.asList(pageObject1, pageObject2);
		setupPages();
		//When
		Menupopup menu = wileySharedComponentAction.getPopup(contextMock);
		//Then
		assertNotNull(menu);
		assertEquals(PAGE_1_LABEL, ((Menuitem) menu.getChildren().get(0)).getLabel());
		assertEquals(PAGE_2_LABEL, ((Menuitem) menu.getChildren().get(1)).getLabel());
	}

	private void setupPages()
	{
		when(componentMock.getRelatedPages()).thenReturn(pages);
		when(currentSessionMock.getTypeService().wrapItems(pages)).thenReturn(relatedPages);
	}

}