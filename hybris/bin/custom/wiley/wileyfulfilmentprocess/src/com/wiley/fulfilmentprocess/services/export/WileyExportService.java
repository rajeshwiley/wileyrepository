package com.wiley.fulfilmentprocess.services.export;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;

import com.wiley.core.enums.ExportProcessType;


public interface WileyExportService
{
	/**
	 * Starts sub export process to external system specified by exportSystemCode
	 */
	void startExportProcess(OrderModel order, PaymentTransactionEntryModel transactionEntry,
			ExportProcessType exportProcessType, String exportSystemCode, OrderProcessModel parentProcess);
}
