/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.fulfilmentprocess.actions.order;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.processengine.action.AbstractAction;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.task.RetryLaterException;

import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.wiley.core.enums.ProductEditionFormat;
import com.wiley.core.model.WileyOrderProcessModel;
import com.wiley.core.order.service.WileyOrderService;
import com.wiley.core.payment.transaction.PaymentTransactionService;
import com.wiley.core.pin.service.PinIntegrationService;


/**
 * Validates the order as follows:
 * Order status as per the precondition
 * Delivery address and delivery mode (only applicable to orders w/ physical products)
 * Availability of order entries
 * Order calculation status
 * Check of the order total against the authorized amount
 * PIN validation (if applicable)
 */
public class WileyCheckOrderAction extends AbstractAction<WileyOrderProcessModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyCheckOrderAction.class);

	@Autowired
	private CommonI18NService commonI18NService;
	@Autowired
	private PaymentTransactionService paymentTransactionService;
	@Autowired
	private PinIntegrationService pinIntegrationService;
	@Autowired
	private WileyOrderService wileyOrderService;

	@Value("${core.system.fulfillment.pin.validate.enable}")
	private boolean pinValidateEnabled;

	@Override
	public Set<String> getTransitions()
	{
		return AbstractSimpleDecisionAction.Transition.getStringValues();
	}

	@Override
	public String execute(final WileyOrderProcessModel orderProcessModel) throws RetryLaterException, Exception
	{
		return this.executeWileyCheckOrderAction(orderProcessModel).toString();
	}

	public AbstractSimpleDecisionAction.Transition executeWileyCheckOrderAction(
			final WileyOrderProcessModel wileyOrderProcessModel)
			throws RetryLaterException, Exception
	{
		final OrderModel order = wileyOrderProcessModel.getOrder();
		if (order == null)
		{
			LOG.error("Missing the order, exiting the process");
			return AbstractSimpleDecisionAction.Transition.NOK;
		}

		if (!simpleCheckOrder(order))
		{
			return failWileyOrder(order);
		}
		// Check delivery mode and delivery address for order with physical products
		if (containsPhysicalProducts(order) && !checkDeliveryOptions(order))
		{
			LOG.error("Missing DeliveryOptions for order {}, exiting the process", order.getCode());
			return failWileyOrder(order);
		}

		if (pinValidateEnabled && pinUsed(order) && !validatePin(order))
		{
			return failWileyOrder(order);
		}

		if (checkAuthorizedAmountIsValid(order))
		{
			setOrderStatus(order, OrderStatus.CHECKED_AUTHORIZED);
			return AbstractSimpleDecisionAction.Transition.OK;
		}
		else
		{
			return failWileyOrder(order);
		}

	}

	protected boolean checkAuthorizedAmountIsValid(final OrderModel order)
	{
		if (!wileyOrderService.isOrderEligibleForPayment(order))
		{
			//payment method does not require any authorization
			return true;
		}

		double totalAuthorized = paymentTransactionService.calculateTotalAuthorized(order);
		double amountToAuthorize = calculateOrderTotal(order);
		if (Math.abs(totalAuthorized - amountToAuthorize) <= 2 * Double.MIN_VALUE)
		{
			return true;
		}
		else
		{
			LOG.error("Authorized payment amount: {} is not equal to order total amount: {}", totalAuthorized,
					amountToAuthorize);
			return false;
		}
	}

	/**
	 * Method sets orderStatus to CHECKED_INVALID and returns NOK transition
	 */
	protected AbstractSimpleDecisionAction.Transition failWileyOrder(final OrderModel orderModel)
	{
		setOrderStatus(orderModel, OrderStatus.CHECKED_INVALID);
		return AbstractSimpleDecisionAction.Transition.NOK;
	}

	/** It is just copy from DefaultCheckOrderService except delivery stuff */
	protected boolean simpleCheckOrder(final OrderModel order)
	{
		if (!order.getCalculated())
		{
			LOG.error("Order {} must be calculated", order.getCode());
			return false;
		}
		if (CollectionUtils.isEmpty(order.getEntries()))
		{
			LOG.error("Order {} must have at least single entry", order.getCode());
			return false;
		}
		else if (!validateOrderStatus(order))
		{
			LOG.error("Order {} comes to fulfilment is in wrong status: {}", order.getCode(), order.getStatus());
			return false;
		}
		return true;
	}

	protected boolean validateOrderStatus(final OrderModel order)
	{
		return order.getStatus() == OrderStatus.CREATED || order.getStatus() == OrderStatus.PAYMENT_AUTHORIZED;
	}

	/**
	 * The assumption is that both totalPrice and totalTax are rounded in calculationService
	 */
	protected double calculateOrderTotal(final OrderModel order)
	{
		// the following line calculates in similar way as in WileyCreateSubscriptionRequestStrategy.getTotalPriceWithTax()
		double totalPrice = order.getTotalPrice() != null ? order.getTotalPrice() : 0D;
		double totalTax = order.getTotalTax() != null ? order.getTotalTax() : 0D;
		return commonI18NService.roundCurrency(totalPrice + totalTax, order.getCurrency().getDigits());
	}

	private boolean containsPhysicalProducts(final OrderModel order)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("order", order);
		for (AbstractOrderEntryModel orderEntry : order.getEntries())
		{
			ProductModel product = orderEntry.getProduct();
			ServicesUtil.validateParameterNotNullStandardMessage("product", product);
			if (ProductEditionFormat.PHYSICAL.equals(product.getEditionFormat()))
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * Check delivery options boolean.
	 * In case of error during shipping methods request user should be able to place order without assigned shipping.
	 *
	 * @param order
	 * 		the order
	 * @return the boolean
	 */
	private boolean checkDeliveryOptions(final OrderModel order)
	{
		if (order.getDeliveryAddress() == null)
		{
			for (final AbstractOrderEntryModel entry : order.getEntries())
			{
				if (entry.getDeliveryPointOfService() == null && entry.getDeliveryAddress() == null)
				{
					// Order and Entry have no delivery address and some entries are not for pickup
					return false;
				}
			}
		}

		return true;
	}

	private boolean pinUsed(final OrderModel orderModel)
	{
		return pinIntegrationService.isPinUsedForOrderPlacement(orderModel);
	}

	private boolean validatePin(final OrderModel orderModel)
	{
		return pinIntegrationService.validatePinForOrder(orderModel);
	}

	public void setPinValidateEnabled(final boolean pinValidateEnabled)
	{
		this.pinValidateEnabled = pinValidateEnabled;
	}
}
