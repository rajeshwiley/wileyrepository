/**
 * 
 */
package com.wiley.fulfilmentprocess.actions.cdm;

import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.task.RetryLaterException;

import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wiley.fulfilmentprocess.model.CDMCustomerExportProcessModel;

/**
 * Action aggregates invalid transitions, logs an error.
 */
public class CDMFailedNotificationAction extends AbstractProceduralAction<CDMCustomerExportProcessModel>
{
  private static final Logger LOG = LoggerFactory.getLogger(CDMCustomerExportProcessModel.class);

  @Override
  public void executeAction(@Nonnull final CDMCustomerExportProcessModel businessProcess) throws RetryLaterException, Exception
  {
    ServicesUtil.validateParameterNotNullStandardMessage("businessProcess", businessProcess);

    final String standartMessage = "Process [{}] failed to export customer [{}] to the system [{}].";

    if (businessProcess.getParentProcess() == null)
    {
      LOG.error(standartMessage, businessProcess.getCode(), businessProcess.getOrder().getCode(),
          businessProcess.getSystemCode());
    }
    else
    {
      final String extendedMessage = standartMessage + " Parent business process [{}].";

      LOG.error(extendedMessage, businessProcess.getCode(), businessProcess.getOrder().getCode(),
          businessProcess.getSystemCode(),
          businessProcess.getParentProcess().getCode());
    }
  }
}
