/**
 *
 */
package com.wiley.fulfilmentprocess.strategy.export.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.wiley.core.payment.transaction.PaymentTransactionService;
import com.wiley.core.product.impl.DefaultWileyProductEditionFormatService;
import com.wiley.fulfilmentprocess.strategy.export.WileyDecideExportSubprocessStartStrategy;


public class EDISubprocessStartStrategyImpl implements WileyDecideExportSubprocessStartStrategy
{
	private static final Logger LOG = Logger.getLogger(EDISubprocessStartStrategyImpl.class);

	@Resource
	private PaymentTransactionService paymentTransactionService;

	@Resource
	private DefaultWileyProductEditionFormatService defaultWileyProductEditionFormatService;

	@Override
	public boolean shouldStartExportSystem(final OrderModel order)
	{
		PaymentTransactionEntryModel captureTransactionEntry =
				paymentTransactionService.getAcceptedTransactionEntry(order, PaymentTransactionType.CAPTURE);

		if (captureTransactionEntry != null || (order.getWileyPartner() != null
				&& !defaultWileyProductEditionFormatService.isDigitalCart(order)))
		{
			return true;
		}
		LOG.debug("Don't export to EDI system, because of 0$ no partner order OR 0$ partner order with only digital "
				+ "products");
		return false;
	}

}
