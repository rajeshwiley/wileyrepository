package com.wiley.fulfilmentprocess.actions.order;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.processengine.action.AbstractAction;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.task.RetryLaterException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.paypal.hybris.constants.PaypalConstants;
import com.wiley.core.mpgs.services.WileyMPGSPaymentProviderService;
import com.wiley.core.mpgs.services.WileyMPGSPaymentService;
import com.wiley.core.payment.WileyPaymentService;
import com.wiley.core.payment.impl.WileyPayPalPaymentService;


/**
 * The TakePayment step captures the payment transaction.
 */
public class TakePaymentAction extends AbstractAction<OrderProcessModel>
{
	private static final Logger LOG = Logger.getLogger(TakePaymentAction.class);

	@Autowired
	private WileyPaymentService wileyWPGPaymentService;
	@Autowired
	private WileyPayPalPaymentService wileyPayPalPaymentService;

	@Autowired
	private WileyMPGSPaymentService wileyMPGSPaymentService;

	@Autowired
	private WileyMPGSPaymentProviderService wileyMPGSPaymentProviderService;

	@Override
	public Set<String> getTransitions()
	{
		return AbstractSimpleDecisionAction.Transition.getStringValues();
	}

	@Override
	public String execute(final OrderProcessModel orderProcessModel) throws RetryLaterException, Exception
	{
		return this.executeWileyTakePaymentAction(orderProcessModel).toString();
	}

	/**
	 * Execute action transition.
	 *
	 * @param process the process
	 * @return the transition
	 */
	public AbstractSimpleDecisionAction.Transition executeWileyTakePaymentAction(final OrderProcessModel process)
	{
		final OrderModel order = process.getOrder();
		final List<Boolean> captureResults = new ArrayList<>();
		for (final PaymentTransactionModel txn : order.getPaymentTransactions())
		{
			capturePayment(captureResults, txn);
		}

		boolean allPaymentsCaptured = !captureResults.isEmpty()
				&& captureResults.stream().reduce(true, (r1, r2) -> r1 && r2);

		if (allPaymentsCaptured)
		{
			setOrderStatus(order, OrderStatus.PAYMENT_CAPTURED);
			return AbstractSimpleDecisionAction.Transition.OK;
		}
		else
		{
			setOrderStatus(order, OrderStatus.PAYMENT_NOT_CAPTURED);
			return AbstractSimpleDecisionAction.Transition.NOK;
		}
	}

	protected void capturePayment(final List<Boolean> captureResults, final PaymentTransactionModel txn)
	{
		if (isPayPalTransaction(txn))
		{
			capturePayPalPayment(captureResults, txn);
		}
		else if (wileyMPGSPaymentProviderService.isMPGSProviderGroup(txn.getPaymentProvider()))
		{
			captureMPGSPayment(captureResults, txn);
		}
		else
		{
			captureWPGPayment(captureResults, txn);
		}
	}


	protected boolean isPayPalTransaction(final PaymentTransactionModel txn)
	{
		return txn.getPaymentProvider().equalsIgnoreCase(PaypalConstants.PAYMENT_PROVIDER_NAME);
	}

	private void captureWPGPayment(final List<Boolean> captureResults, final PaymentTransactionModel txn)
	{
		final Optional<PaymentTransactionEntryModel> captureEntry = wileyWPGPaymentService.capture(txn);
		addCaptureResult(captureResults, captureEntry);
	}


	private void captureMPGSPayment(final List<Boolean> captureResults, final PaymentTransactionModel txn)
	{
		final Optional<PaymentTransactionEntryModel> captureEntry = wileyMPGSPaymentService.capture(txn);
		addCaptureResult(captureResults, captureEntry);
	}


	private void capturePayPalPayment(final List<Boolean> captureResults, final PaymentTransactionModel txn)
	{
		final Optional<PaymentTransactionEntryModel> paypalCaptureEntry = wileyPayPalPaymentService.capture(txn);
		addCaptureResult(captureResults, paypalCaptureEntry);
	}

	private void addCaptureResult(final List<Boolean> captureResults, final Optional<PaymentTransactionEntryModel> captureEntry)
	{
		if (captureEntry.isPresent())
		{
			final PaymentTransactionModel txn = captureEntry.get().getPaymentTransaction();
			final AbstractOrderModel order = txn.getOrder();
			if (TransactionStatus.ACCEPTED.name().equals(captureEntry.get().getTransactionStatus()))
			{
				if (LOG.isDebugEnabled())
				{
					LOG.debug("The payment transaction has been captured. Order: " + order.getCode()
							+ ". Txn: " + txn.getCode());
				}
				captureResults.add(Boolean.TRUE);
			}
			else
			{
				LOG.error("The payment transaction capture has failed. Order: " + order.getCode()
						+ ". Txn: " + txn.getCode());
				captureResults.add(Boolean.FALSE);
			}
		}
	}

	public WileyMPGSPaymentProviderService getWileyMPGSPaymentProviderService() {
		return this.wileyMPGSPaymentProviderService;
	}
}
