package com.wiley.fulfilmentprocess.actions.eloqua;

import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.task.RetryLaterException;

import java.util.Collections;
import java.util.Set;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wiley.fulfilmentprocess.actions.export.AbstractExportSubProcessEndAction;
import com.wiley.fulfilmentprocess.model.EloquaOrderExportProcessModel;


/**
 * Action sends notification to parent business process.
 *
 * @author Aliaksei_Zlobich
 */
public class EloquaProcessEndAction extends AbstractExportSubProcessEndAction<EloquaOrderExportProcessModel>
{

	private static final Logger LOG = LoggerFactory.getLogger(EloquaProcessEndAction.class);

	@Resource
	private BusinessProcessService businessProcessService;

	@Override
	public Transition executeAction(@Nonnull final EloquaOrderExportProcessModel businessProcess)
			throws RetryLaterException, Exception
	{
		ServicesUtil.validateParameterNotNullStandardMessage("businessProcess", businessProcess);

		LOG.debug("Method parameters: businessProcess [{}].", businessProcess.getCode());

		businessProcess.setDone(true);
		save(businessProcess);

		sendSubProcessCompletedEvent(businessProcess);

		return Transition.OK;
	}

	@Override
	public Set<String> getTransitions()
	{
		return Collections.singleton(Transition.OK.toString());
	}
}
