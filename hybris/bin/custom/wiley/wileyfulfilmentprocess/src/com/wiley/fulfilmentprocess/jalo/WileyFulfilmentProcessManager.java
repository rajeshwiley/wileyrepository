/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.fulfilmentprocess.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;

import org.apache.log4j.Logger;

import com.wiley.fulfilmentprocess.constants.WileyFulfilmentProcessConstants;


/**
 * The type Wiley fulfilment process manager.
 */
@SuppressWarnings("PMD")
public class WileyFulfilmentProcessManager extends GeneratedWileyFulfilmentProcessManager
{
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(WileyFulfilmentProcessManager.class.getName());

	/**
	 * Gets instance.
	 *
	 * @return the instance
	 */
	public static final WileyFulfilmentProcessManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (WileyFulfilmentProcessManager) em.getExtension(WileyFulfilmentProcessConstants.EXTENSIONNAME);
	}

}
