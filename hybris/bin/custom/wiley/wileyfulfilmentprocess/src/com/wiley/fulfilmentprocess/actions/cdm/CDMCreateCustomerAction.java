package com.wiley.fulfilmentprocess.actions.cdm;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.task.RetryLaterException;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wiley.core.integration.cdm.service.WileyCdmCustomerService;
import com.wiley.fulfilmentprocess.model.CDMCustomerExportProcessModel;
import com.wiley.core.integration.cdm.dto.CDMCreateUserResponse;


public class CDMCreateCustomerAction extends AbstractSimpleDecisionAction<CDMCustomerExportProcessModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(CDMCreateCustomerAction.class);

	@Resource
	private WileyCdmCustomerService wileyCdmCustomerService;

	@Override
	@Nonnull
	public Transition executeAction(@Nonnull final CDMCustomerExportProcessModel businessProcess)
			throws RetryLaterException, Exception
	{
		ServicesUtil.validateParameterNotNullStandardMessage("businessProcess", businessProcess);

		LOG.debug("Method params: CDMCustomerExportProcessModel [{}]", businessProcess.getCode());

		final OrderModel order = businessProcess.getOrder();
		final CustomerModel customer = (CustomerModel) order.getUser();

		CDMCreateUserResponse response = null;

		try
		{
			response = wileyCdmCustomerService.createCDMCustomer(customer, order.getSite());
			logResponseStatus(response);
		}
		catch (final Exception e)
		{
			LOG.error("Exception occurred during creating of CDM contact. Error message: {}.",
					e.getMessage());
			return logAndReturnTransition(Transition.NOK);
		}

		return logAndReturnTransition(Transition.OK);
	}

	private void logResponseStatus(final CDMCreateUserResponse response)
	{
		LOG.debug("Response from CDM create customer: {}.", response);
	}

	private Transition logAndReturnTransition(final Transition transition)
	{
		LOG.debug("Transition to return: {}.", transition);
		return transition;
	}
}
