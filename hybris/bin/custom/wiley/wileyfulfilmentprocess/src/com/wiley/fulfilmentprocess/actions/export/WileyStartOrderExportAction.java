package com.wiley.fulfilmentprocess.actions.export;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.wiley.core.enums.ExportProcessType;
import com.wiley.core.enums.OrderType;
import com.wiley.core.payment.transaction.PaymentTransactionService;
import com.wiley.fulfilmentprocess.services.export.WileyExportService;
import com.wiley.fulfilmentprocess.strategy.export.WileyDecideExportSubprocessStartStrategy;

import static com.wiley.integrations.constants.WileyintegrationsConstants.CDM_SYSTEM_CODE;
import static com.wiley.integrations.constants.WileyintegrationsConstants.EDI_SYSTEM_CODE;
import static com.wiley.integrations.constants.WileyintegrationsConstants.ELOQUA_SYSTEM_CODE;
import static com.wiley.integrations.constants.WileyintegrationsConstants.WILEY_CORE_SYSTEM_CODE;


/**
 * Creates subprocesses that will export the order to different external systems.
 */
public class WileyStartOrderExportAction extends AbstractProceduralAction<OrderProcessModel>
{
	private static final Logger LOG = Logger.getLogger(WileyStartOrderExportAction.class);

	@Resource
	private PaymentTransactionService paymentTransactionService;
	@Resource
	private WileyExportService wileyExportService;
	@Resource(name = "cdmSubProcessStartStrategy")
	private WileyDecideExportSubprocessStartStrategy cdmSubProcessStartStrategy;
	@Resource(name = "ediSubProcessStartStrategy")
	private WileyDecideExportSubprocessStartStrategy ediSubProcessStartStrategy;
	@Resource(name = "coreSubProcessStartStrategy")
	private WileyDecideExportSubprocessStartStrategy coreSubProcessStartStrategy;



	@Override
	public void executeAction(final OrderProcessModel process) throws Exception
	{
		if (LOG.isInfoEnabled())
		{
			LOG.info("Process: " + process.getCode() + " in step " + getClass());
		}
		final OrderModel order = process.getOrder();
		if (order == null)
		{
			LOG.error("Can't find order for process: [" + process.getCode() + "]");
			return;
		}
		PaymentTransactionEntryModel captureTransactionEntry =
				paymentTransactionService.getAcceptedTransactionEntry(order, PaymentTransactionType.CAPTURE);

		// Start Eloqua export sub process
		if (order.getOrderType() != OrderType.FREE_TRIAL_WEB_SERVICE)
		{

			wileyExportService.startExportProcess(order, captureTransactionEntry,
					ExportProcessType.SETTLE, ELOQUA_SYSTEM_CODE, process);
		}

		// Start EDI export sub process
		if (getEdiSubProcessStartStrategy().shouldStartExportSystem(order))
		{
			wileyExportService.startExportProcess(order, captureTransactionEntry,
					ExportProcessType.SETTLE, EDI_SYSTEM_CODE, process);
		}

		// Start wileycore export sub process
		if (coreSubProcessStartStrategy.shouldStartExportSystem(order))
		{
			wileyExportService.startExportProcess(order, captureTransactionEntry,
					ExportProcessType.SETTLE, WILEY_CORE_SYSTEM_CODE, process);
		}

		// Start CDM export sub process
		if (getCdmSubProcessStartStrategy().shouldStartExportSystem(order))
		{
			wileyExportService.startExportProcess(order, captureTransactionEntry,
					ExportProcessType.SETTLE, CDM_SYSTEM_CODE, process);
		}
	}

	/**
	 * @return the cdmSubProcessStartStrategy
	 */
	public WileyDecideExportSubprocessStartStrategy getCdmSubProcessStartStrategy()
	{
		return cdmSubProcessStartStrategy;
	}

	/**
	 * @param cdmSubProcessStartStrategy
	 * 		the cdmSubProcessStartStrategy to set
	 */
	public void setCdmSubProcessStartStrategy(final
	WileyDecideExportSubprocessStartStrategy cdmSubProcessStartStrategy)
	{
		this.cdmSubProcessStartStrategy = cdmSubProcessStartStrategy;
	}

	/**
	 * @return the ediSubProcessStartStrategy
	 */
	public WileyDecideExportSubprocessStartStrategy getEdiSubProcessStartStrategy()
	{
		return ediSubProcessStartStrategy;
	}

	/**
	 * @param ediSubProcessStartStrategy
	 * 		the ediSubProcessStartStrategy to set
	 */
	public void setEdiSubProcessStartStrategy(final
	WileyDecideExportSubprocessStartStrategy ediSubProcessStartStrategy)
	{
		this.ediSubProcessStartStrategy = ediSubProcessStartStrategy;
	}

	public WileyDecideExportSubprocessStartStrategy getCoreSubProcessStartStrategy()
	{
		return coreSubProcessStartStrategy;
	}

	public void setCoreSubProcessStartStrategy(
			final WileyDecideExportSubprocessStartStrategy coreSubProcessStartStrategy)
	{
		this.coreSubProcessStartStrategy = coreSubProcessStartStrategy;
	}
}
