/**
 * 
 */
package com.wiley.fulfilmentprocess.strategy.export.impl;

import de.hybris.platform.core.model.order.OrderModel;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;

import com.wiley.fulfilmentprocess.strategy.export.WileyDecideExportSubprocessStartStrategy;


/**
 * Strategy for evaluating triggering
 * eCORE pin integration system export.
 * Pin activation is implemented as hybris business process chain
 * that makes calls to eCORE.
 */
public class CoreSubprocessStartStrategyImpl implements WileyDecideExportSubprocessStartStrategy
{
  private static final Logger LOG = Logger.getLogger(CoreSubprocessStartStrategyImpl.class);
  

  @Value("${core.system.fulfillment.pin.activate.enable}")
  private boolean pinActivationEnabled;

  @Override
  public boolean shouldStartExportSystem(final OrderModel order)
  {
    LOG.debug(String.format("Export to wiley CORE for order %s enabled=%s", order.getCode(), pinActivationEnabled));
    return pinActivationEnabled;
  }

}
