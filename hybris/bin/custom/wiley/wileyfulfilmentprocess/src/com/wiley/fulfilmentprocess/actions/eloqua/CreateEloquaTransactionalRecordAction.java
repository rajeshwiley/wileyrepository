package com.wiley.fulfilmentprocess.actions.eloqua;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.search.restriction.SearchRestrictionService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.task.RetryLaterException;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wiley.core.integration.eloqua.EloquaGateway;
import com.wiley.fulfilmentprocess.model.EloquaOrderExportProcessModel;


/**
 * Action starts process of sending Order data to Eloqua.<br/>
 * Uses Spring Integration approach.
 *
 * @author Aliaksei_Zlobich
 */
public class CreateEloquaTransactionalRecordAction extends AbstractSimpleDecisionAction<EloquaOrderExportProcessModel>
{

	private static final Logger LOG = LoggerFactory.getLogger(CreateEloquaTransactionalRecordAction.class);

	@Resource
	private EloquaGateway eloquaGateway;

	@Resource
	private SearchRestrictionService searchRestrictionService;

	@Override
	public Transition executeAction(final EloquaOrderExportProcessModel businessProcess)
			throws RetryLaterException, Exception
	{
		searchRestrictionService.disableSearchRestrictions();

		ServicesUtil.validateParameterNotNullStandardMessage("businessProcess", businessProcess);

		LOG.debug("Method params: EloquaOrderExportProcess [{}]", businessProcess.getCode());

		final OrderModel order = businessProcess.getOrder();

		final List<AbstractOrderEntryModel> orderEntries = order.getEntries();

		for (AbstractOrderEntryModel entry : orderEntries)
		{
			try
			{
				eloquaGateway.createEloquaTransactionalRecord((OrderEntryModel) entry);
			}
			catch (Exception e)
			{
				LOG.error("Exception occurred during creating of Eloqua Transactional Record. Error message: {}.",
						e.getMessage());
				return logAndReturnTransition(Transition.NOK);
			}
		}

		return logAndReturnTransition(Transition.OK);
	}

	private Transition logAndReturnTransition(final Transition transition)
	{
		LOG.debug("Transition to return: {}.", transition);
		return transition;
	}
}
