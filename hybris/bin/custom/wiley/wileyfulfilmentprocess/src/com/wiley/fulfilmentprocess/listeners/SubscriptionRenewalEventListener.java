package com.wiley.fulfilmentprocess.listeners;

import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.model.WileySubscriptionModel;
import com.wiley.core.subscription.events.SubscriptionRenewalEvent;
import com.wiley.fulfilmentprocess.constants.WileyFulfilmentProcessConstants;
import com.wiley.fulfilmentprocess.model.WileySubscriptionProcessModel;


/**
 * Created by Raman_Hancharou on 12/17/2015.
 */
public class SubscriptionRenewalEventListener extends AbstractEventListener<SubscriptionRenewalEvent>
{

	private BusinessProcessService businessProcessService;

	/**
	 * Gets business process service.
	 *
	 * @return the business process service
	 */
	protected BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	/**
	 * Sets business process service.
	 *
	 * @param businessProcessService
	 * 		the business process service
	 */
	@Required
	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}


	@Override
	protected void onEvent(final SubscriptionRenewalEvent event)
	{
		final WileySubscriptionModel wileySubscription = event.getSubscription();
		ServicesUtil.validateParameterNotNull(wileySubscription, "WileySubscription can not be null");

		WileySubscriptionProcessModel wileySubscriptionProcess = getBusinessProcessService().createProcess(
				wileySubscription.getCode() + "_" + System.currentTimeMillis(),
				WileyFulfilmentProcessConstants.SUBSCRIPTION_RENEWAL_PROCESS_NAME);
		wileySubscriptionProcess.setWileySubscription(wileySubscription);
		getBusinessProcessService().startProcess(wileySubscriptionProcess);
	}
}
