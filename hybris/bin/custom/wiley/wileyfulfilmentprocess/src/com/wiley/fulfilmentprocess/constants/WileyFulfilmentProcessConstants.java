/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.fulfilmentprocess.constants;

/**
 * The type Wiley fulfilment process constants.
 */
public final class WileyFulfilmentProcessConstants extends GeneratedWileyFulfilmentProcessConstants
{
	/**
	 * The constant CONSIGNMENT_SUBPROCESS_END_EVENT_NAME.
	 */
	public static final String CONSIGNMENT_SUBPROCESS_END_EVENT_NAME = "ConsignmentSubprocessEnd";

	/**
	 * The constant ORDER_PROCESS_NAME.
	 */
	public static final String ORDER_PROCESS_NAME = "order-process";

	/**
	 * The constant ORDER_PROCESS_NAME_WEL.
	 */
	public static final String ORDER_PROCESS_NAME_WEL = "wel-order-process";

	/**
	 * The constant SUBSCRIPTION_RENEWAL_PROCESS_NAME.
	 */
	public static final String SUBSCRIPTION_RENEWAL_PROCESS_NAME = "ags-subscription-renewal-process";

	/**
	 * The constant CONSIGNMENT_SUBPROCESS_NAME.
	 */
	public static final String CONSIGNMENT_SUBPROCESS_NAME = "consignment-process";

	/**
	 * The constant WAIT_FOR_WAREHOUSE.
	 */
	public static final String WAIT_FOR_WAREHOUSE = "WaitForWarehouse";

	/**
	 * The constant CONSIGNMENT_PICKUP.
	 */
	public static final String CONSIGNMENT_PICKUP = "ConsignmentPickup";

	/**
	 * The constant CONSIGNMENT_COUNTER.
	 */
	public static final String CONSIGNMENT_COUNTER = "CONSIGNMENT_COUNTER";

	/**
	 * The constant PARENT_PROCESS.
	 */
	public static final String PARENT_PROCESS = "PARENT_PROCESS";

	/**
	 * The constant WILEY_EXPORT_SUBPROCESS_END_EVENT_NAME.
	 */
	public static final String WILEY_EXPORT_SUBPROCESS_END_EVENT_NAME = "WileyExportSubprocessEnd";

	/**
	 * The constant ELOQUA_ORDER_EXPORT_SUBPROCESS_NAME.
	 */
	public static final String ELOQUA_ORDER_EXPORT_SUBPROCESS_NAME = "export-order-to-eloqua-process";

	/**
	 * The constant CDM_CUSTOMER_EXPORT_SUBPROCESS_NAME.
	 */
	public static final String CDM_ORDER_EXPORT_SUBPROCESS_NAME = "export-customer-to-cdm-process";

	/**
	 * The constant WILEY_CORE_ORDER_EXPORT_SUBPROCESS_NAME.
	 */
	public static final String WILEY_CORE_ORDER_EXPORT_SUBPROCESS_NAME = "export-order-to-wileycore-process";

	/**
	 * The constant CREATE_EBP_USER_PROCESS_NAME.
	 */
	public static final String CREATE_EBP_USER_PROCESS_NAME = "create-ebp-user-process";

	/**
	 * The constant EDI_EXPORT_SUBPROCESS_NAME.
	 */
	public static final String EDI_EXPORT_SUBPROCESS_NAME = "export-subprocess";

	public static final int EBP_RESPONSE_SUCCESS_CODE_200 = 200;
	public static final int EBP_RESPONSE_SUCCESS_CODE_201 = 201;
}
