package com.wiley.fulfilmentprocess.listeners;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.event.NewCustomerEvent;
import com.wiley.fulfilmentprocess.constants.WileyFulfilmentProcessConstants;
import com.wiley.fulfilmentprocess.model.CreateEbpUserProcessModel;


/**
 * Listener for starting process of creation user in ebp.
 */
public class NewCustomerEventListener extends AbstractEventListener<NewCustomerEvent>
{
	private BusinessProcessService businessProcessService;

	@Override
	protected void onEvent(@Nonnull final NewCustomerEvent newCustomerEvent)
	{
		final CustomerModel customerModel = newCustomerEvent.getCustomerModel();
		ServicesUtil.validateParameterNotNull(customerModel, "CustomerModel can not be null");

		CreateEbpUserProcessModel createEbpUserProcessModel = getBusinessProcessService().createProcess(
				WileyFulfilmentProcessConstants.CREATE_EBP_USER_PROCESS_NAME + "_" + customerModel.getUid() + "_"
						+ System.currentTimeMillis(),
				WileyFulfilmentProcessConstants.CREATE_EBP_USER_PROCESS_NAME);

		createEbpUserProcessModel.setCustomer(customerModel);
		createEbpUserProcessModel.setOldEmailAddress(newCustomerEvent.getOldEmailAddress());

		getBusinessProcessService().startProcess(createEbpUserProcessModel);
	}

	/**
	 * Gets business process service.
	 *
	 * @return the business process service
	 */
	protected BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	/**
	 * Sets business process service.
	 *
	 * @param businessProcessService
	 * 		the business process service
	 */
	@Required
	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}
}
