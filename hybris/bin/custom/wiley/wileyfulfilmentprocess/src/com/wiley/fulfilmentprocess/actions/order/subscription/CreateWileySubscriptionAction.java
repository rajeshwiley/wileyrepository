package com.wiley.fulfilmentprocess.actions.order.subscription;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.search.restriction.SearchRestrictionService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.task.RetryLaterException;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.subscription.services.WileySubscriptionService;


public class CreateWileySubscriptionAction extends AbstractSimpleDecisionAction<OrderProcessModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(CreateWileySubscriptionAction.class);

	@Autowired
	private WileySubscriptionService wileySubscriptionService;

	@Autowired
	private SearchRestrictionService searchRestrictionService;

	public void setWileySubscriptionService(final WileySubscriptionService wileySubscriptionService)
	{
		this.wileySubscriptionService = wileySubscriptionService;
	}

	@Override
	public Transition executeAction(final OrderProcessModel process)
			throws RetryLaterException, Exception
	{
		searchRestrictionService.disableSearchRestrictions();
		final OrderModel order = process.getOrder();
		ServicesUtil.validateParameterNotNull(process, "Process can not be null");
		ServicesUtil.validateParameterNotNull(order, "Order can not be null");

		try
		{
			List<AbstractOrderEntryModel> entries = order.getEntries();
			if (entries != null && !entries.isEmpty())
			{
				for (AbstractOrderEntryModel entry : order.getEntries())
				{
					final ProductModel product = entry.getProduct();
					if (wileySubscriptionService.createSubscriptionFromOrder(entry) == null)
					{
						LOG.warn("Product with code {} isn't a subscription!", product.getCode());
					}
					else
					{
						LOG.info("Subscription with code {} has been successfully saved!", product.getCode());
					}
				}
				LOG.info("Order {}: status changed to COMPLETED", order.getCode());
				setOrderStatus(order, OrderStatus.SUBSCRIPTION_CREATED);
				return Transition.OK;
			}

			LOG.error("There are no entries in order. Order {}:  status changed to PROCESSING_ERROR", order.getCode());
			setOrderStatus(order, OrderStatus.PROCESSING_ERROR);
			return Transition.NOK;
		}
		catch (Exception e)
		{
			LOG.error("Error during subscription creation. Order {}: status changed to PROCESSING_ERROR", order.getCode(), e);
			setOrderStatus(order, OrderStatus.PROCESSING_ERROR);
			return Transition.NOK;
		}
	}
}
