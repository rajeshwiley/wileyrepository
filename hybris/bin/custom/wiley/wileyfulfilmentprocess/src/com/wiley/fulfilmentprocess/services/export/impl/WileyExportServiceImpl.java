package com.wiley.fulfilmentprocess.services.export.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Map;

import javax.annotation.Resource;

import org.jgroups.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wiley.core.enums.ExportProcessType;
import com.wiley.fulfilmentprocess.model.WileyExportProcessModel;
import com.wiley.fulfilmentprocess.services.export.WileyExportService;


public class WileyExportServiceImpl implements WileyExportService
{
	private static final String UNDERSCORE = "_";
	private static final Logger LOG = LoggerFactory.getLogger(WileyExportServiceImpl.class);

	@Resource
	private BusinessProcessService businessProcessService;
	@Resource
	private ModelService modelService;

	private Map<String, String> systemToProcessDefinitionNameMap;

	@Override
	public void startExportProcess(final OrderModel order, final PaymentTransactionEntryModel transactionEntry,
			final ExportProcessType exportProcessType, final String exportSystemCode, final OrderProcessModel parentProcess)
	{
		final String businessProcessDefinitionName = systemToProcessDefinitionNameMap.get(exportSystemCode);

		if (businessProcessDefinitionName != null)
		{
			final String exportProcessName = buildExportProcessName(order, exportProcessType, exportSystemCode);
			final WileyExportProcessModel exportProcess = businessProcessService.createProcess(
					exportProcessName, businessProcessDefinitionName);
			exportProcess.setOrder(order);
			exportProcess.setSystemCode(exportSystemCode);
			exportProcess.setExportType(exportProcessType);
			exportProcess.setParentProcess(parentProcess);
			exportProcess.setTransactionEntry(transactionEntry);

			if (transactionEntry != null)
			{
				exportProcess.setTransactionDate(transactionEntry.getCreationtime());
			}

			modelService.save(exportProcess);
			businessProcessService.startProcess(exportProcess);
		}
		else
		{
			LOG.error("Business process is not configured for export system [{}]. Please check WileyExportService configuration.",
					exportSystemCode);
		}

	}

	private String buildExportProcessName(final OrderModel order,
			final ExportProcessType exportProcessType, final String exportSystemCode)
	{
		return new StringBuilder()
				.append(exportSystemCode)
				.append(UNDERSCORE)
				.append(exportProcessType)
				.append(UNDERSCORE)
				.append(order.getCode())
				.append(UNDERSCORE)
				.append(UUID.randomUUID().toString()).toString();
	}

	public void setSystemToProcessDefinitionNameMap(
			final Map<String, String> systemToProcessDefinitionNameMap)
	{
		this.systemToProcessDefinitionNameMap = systemToProcessDefinitionNameMap;
	}
}
