package com.wiley.fulfilmentprocess.actions.export;

import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.task.RetryLaterException;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.wiley.fulfilmentprocess.model.WileyExportProcessModel;


/**
 * Finishes the export subprocess.
 */
public class WileyExportProcessEndAction extends AbstractExportSubProcessEndAction<WileyExportProcessModel>
{
	private static final Logger LOG = Logger.getLogger(WileyExportProcessEndAction.class);

	@Resource
	private BusinessProcessService businessProcessService;

	@Override
	public Transition executeAction(final WileyExportProcessModel process) throws RetryLaterException, Exception
	{
		LOG.info("Process: " + process.getCode() + " in step " + getClass());

		if (process.isOrderExported())
		{
			process.setDone(true);
			save(process);
			LOG.info("Process: " + process.getCode() + " wrote DONE marker");
			sendSubProcessCompletedEvent(process);
			return Transition.OK;
		}
		else {
			LOG.error("Process: " + process.getCode() + " failed to export order "
					+ process.getParentProcess().getOrder().getCode() + " to the " + process.getSystemCode() + " system");
			return Transition.NOK;
		}
	}
}
