package com.wiley.fulfilmentprocess.actions.order.wel;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.task.RetryLaterException;

import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.mpgs.services.WileyMPGSPaymentService;


public class AuthorizePreOrderAction extends AbstractSimpleDecisionAction<OrderProcessModel>
{

	@Autowired
	private WileyMPGSPaymentService wileyMPGSPaymentService;

	@Override
	public Transition executeAction(final OrderProcessModel orderProcessModel)
		throws RetryLaterException, Exception
	{
		OrderModel order = orderProcessModel.getOrder();

		PaymentTransactionEntryModel authorizeEntry = wileyMPGSPaymentService.authorize(order);
		final boolean isAuthorizedSuccessfully = TransactionStatus.ACCEPTED.name()
				.equals(authorizeEntry.getTransactionStatus());
		if (isAuthorizedSuccessfully)
		{
			setOrderStatus(order, OrderStatus.PAYMENT_AUTHORIZED);
			return Transition.OK;
		}
		setOrderStatus(order, OrderStatus.PAYMENT_NOT_AUTHORIZED);
		return Transition.NOK;
	}
}
