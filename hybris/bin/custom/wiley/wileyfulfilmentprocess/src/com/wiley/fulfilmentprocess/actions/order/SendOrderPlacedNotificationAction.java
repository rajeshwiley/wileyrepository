package com.wiley.fulfilmentprocess.actions.order;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.events.OrderPlacedEvent;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractAction;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.task.RetryLaterException;

import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * The type Send order placed notification action.
 */
public class SendOrderPlacedNotificationAction extends AbstractAction<OrderProcessModel>
{
	private static final Logger LOG = Logger.getLogger(SendOrderPlacedNotificationAction.class);

	public static final String TAKE_PAYMENT = "TAKE_PAYMENT";
	public static final String SKIP_PAYMENT = "SKIP_PAYMENT";

	@Autowired
	private EventService eventService;

	@Autowired
	private CommonI18NService commonI18NService;

	/**
	 * Execute action.
	 *
	 * @param process
	 * 		the process
	 */
	@Override
	public final String execute(final OrderProcessModel process) throws RetryLaterException, Exception
	{
		getEventService().publishEvent(new OrderPlacedEvent(process));
		LOG.info("Process: " + process.getCode() + " in step " + getClass());
		final OrderModel order = process.getOrder();
		return isZeroOrder(order) ? SKIP_PAYMENT : TAKE_PAYMENT;
	}


	@Override
	public Set<String> getTransitions()
	{
		return AbstractAction.createTransitions(TAKE_PAYMENT, SKIP_PAYMENT);
	}

	private boolean isZeroOrder(final OrderModel order) {
		return Math.abs(calculateOrderTotal(order)) <= 2 * Double.MIN_VALUE;
	}


	/**
	 * The assumption is that both totalPrice and totalTax are rounded in calculationService
	 */
	private double calculateOrderTotal(final OrderModel order)
	{
		// the following line calculates in similar way as in WileyCreateSubscriptionRequestStrategy.getTotalPriceWithTax()
		double totalPrice = order.getTotalPrice() != null ? order.getTotalPrice().doubleValue() : 0D;
		double totalTax = order.getTotalTax() != null ? order.getTotalTax().doubleValue() : 0D;
		return commonI18NService.roundCurrency(totalPrice + totalTax, order.getCurrency().getDigits());
	}

	/**
	 * Gets event service.
	 *
	 * @return the event service
	 */
	protected EventService getEventService()
	{
		return eventService;
	}
}
