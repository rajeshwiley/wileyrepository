/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.fulfilmentprocess.strategy.impl;

import de.hybris.platform.commerceservices.stock.CommerceStockService;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.strategy.AbstractSplittingStrategy;

import org.springframework.beans.factory.annotation.Required;


/**
 * The type Split by available count.
 */
public class SplitByAvailableCount extends AbstractSplittingStrategy
{
	private CommerceStockService commerceStockService;

	/**
	 * Gets grouping object.
	 *
	 * @param orderEntry
	 * 		the order entry
	 * @return the grouping object
	 */
	@Override
	public Object getGroupingObject(final AbstractOrderEntryModel orderEntry)
	{
		if (orderEntry.getDeliveryPointOfService() != null)
		{
			final Long stock = getCommerceStockService().getStockLevelForProductAndPointOfService(orderEntry.getProduct(),
					orderEntry.getDeliveryPointOfService());
			return Boolean.valueOf(stock == null || stock.longValue() >= orderEntry.getQuantity().longValue());
		}
		else
		{
			Long stock = Long.valueOf(0);
			if (orderEntry.getOrder().getStore() != null)
			{
				stock = getCommerceStockService().getStockLevelForProductAndBaseStore(orderEntry.getProduct(),
						orderEntry.getOrder().getStore());
			}
			return Boolean.valueOf(stock == null || stock.longValue() >= orderEntry.getQuantity().longValue());
		}
	}

	/**
	 * After splitting.
	 *
	 * @param groupingObject
	 * 		the grouping object
	 * @param createdOne
	 * 		the created one
	 */
	@Override
	public void afterSplitting(final Object groupingObject, final ConsignmentModel createdOne)
	{
		//nothing to do		
	}

	/**
	 * Gets commerce stock service.
	 *
	 * @return the commerce stock service
	 */
	protected CommerceStockService getCommerceStockService()
	{
		return commerceStockService;
	}

	/**
	 * Sets commerce stock service.
	 *
	 * @param commerceStockService
	 * 		the commerce stock service
	 */
	@Required
	public void setCommerceStockService(final CommerceStockService commerceStockService)
	{
		this.commerceStockService = commerceStockService;
	}
}
