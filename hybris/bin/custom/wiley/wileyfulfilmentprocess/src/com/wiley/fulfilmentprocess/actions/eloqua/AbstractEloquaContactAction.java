package com.wiley.fulfilmentprocess.actions.eloqua;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;

import com.wiley.fulfilmentprocess.model.EloquaOrderExportProcessModel;


/**
 * Abstract action for Create and Update EloquaContactActions
 */
public abstract class AbstractEloquaContactAction
    extends AbstractSimpleDecisionAction<EloquaOrderExportProcessModel> {
  protected AddressModel getPaymentAddress(final OrderModel order, final CustomerModel customer) {

    final AddressModel address = null != order.getDeliveryAddress() ? order.getDeliveryAddress() :
      null != order.getPaymentAddress() ? order.getPaymentAddress() :
        customer.getDefaultPaymentAddress();
    return address;
  }
}
