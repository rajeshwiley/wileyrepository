package com.wiley.fulfilmentprocess.actions.eloqua;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.task.RetryLaterException;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wiley.fulfilmentprocess.model.EloquaOrderExportProcessModel;
import com.wiley.core.integration.eloqua.EloquaGateway;
import com.wiley.core.integration.eloqua.dto.ContactDto;


/**
 * Action starts process of creation new Contact in Eloqua. Stores the contactId as eloquaId.<br/>
 * Uses Spring Integration approach.
 *
 * @author Aliaksei_Zlobich
 */
public class CreateEloquaContactAction extends AbstractEloquaContactAction
{

	private static final Logger LOG = LoggerFactory.getLogger(CreateEloquaContactAction.class);

	@Resource
	private EloquaGateway eloquaGateway;

	@Override
	@Nonnull
	public Transition executeAction(@Nonnull final EloquaOrderExportProcessModel businessProcess)
			throws RetryLaterException, Exception
	{
		ServicesUtil.validateParameterNotNullStandardMessage("businessProcess", businessProcess);

		LOG.debug("Method params: EloquaOrderExportProcess [{}]", businessProcess.getCode());

		final OrderModel order = businessProcess.getOrder();
		final CustomerModel customer = (CustomerModel) order.getUser();

		ContactDto eloquaContact;

		try
		{
			eloquaContact = eloquaGateway.createEloquaContact(customer, getPaymentAddress(order, customer),
					order.getSubscribeToUpdates(),
					order.getSite());
		}
		catch (Exception e)
		{
			LOG.error("Exception occurred during creating of Eloqua contact. Error message: {}.", e.getMessage());
			return logAndReturnTransition(Transition.NOK);
		}

		customer.setEloquaId(eloquaContact.getId());
		getModelService().save(customer);

		return logAndReturnTransition(Transition.OK);
	}

	private Transition logAndReturnTransition(final Transition transition)
	{
		LOG.debug("Transition to return: {}.", transition);
		return transition;
	}
}
