package com.wiley.fulfilmentprocess.actions.order.wel;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.task.RetryLaterException;

import java.util.Collections;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.enums.OrderType;
import com.wiley.core.wel.preorder.service.WelPreOrderService;


public class IsPreOrderAction extends AbstractSimpleDecisionAction<OrderProcessModel>
{
	@Autowired
	WelPreOrderService welPreOrderService;

	@Autowired
	private CatalogVersionService catalogVersionService;

	@Override
	public Transition executeAction(final OrderProcessModel orderProcessModel)
			throws RetryLaterException, Exception
	{
		OrderModel order = orderProcessModel.getOrder();
		setSessionCatalogVersion(order);
		if (welPreOrderService.isPreOrder(order))
		{
			order.setStatus(OrderStatus.PREORDER);
			order.setOrderType(OrderType.PRE_ORDER);
			getModelService().save(order);
			return Transition.OK;
		}
		return Transition.NOK;
	}

	private void setSessionCatalogVersion(final OrderModel order)
	{
		final List<AbstractOrderEntryModel> entries = order.getEntries();
		if (CollectionUtils.isNotEmpty(entries)) {
			final CatalogVersionModel catalogVersion = entries.get(0).getProduct().getCatalogVersion();
			catalogVersionService.setSessionCatalogVersions(Collections.singletonList(catalogVersion));
		}
	}
}
