package com.wiley.fulfilmentprocess.actions.order.subscription.renewal;

import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wiley.fulfilmentprocess.model.WileySubscriptionProcessModel;


/**
 * Created by Raman_Hancharou on 12/17/2015.
 */
public class OrderFailedNotificationAction extends AbstractProceduralAction<WileySubscriptionProcessModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(OrderFailedNotificationAction.class);

	@Override
	public void executeAction(final WileySubscriptionProcessModel process)
	{
		ServicesUtil.validateParameterNotNull(process, "Process can not be null");
		LOG.error("Renewal subscription process {} has been failed", process.getCode());
	}

}