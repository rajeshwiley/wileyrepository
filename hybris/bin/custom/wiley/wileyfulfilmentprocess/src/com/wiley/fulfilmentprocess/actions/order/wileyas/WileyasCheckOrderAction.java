package com.wiley.fulfilmentprocess.actions.order.wileyas;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.task.RetryLaterException;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.base.Preconditions;
import com.wiley.core.model.WileyOrderProcessModel;
import com.wiley.core.order.PaymentActonType;
import com.wiley.core.order.PendingPaymentActon;
import com.wiley.core.order.service.WileyOrderService;
import com.wiley.core.wiley.order.WileyOrderPaymentService;
import com.wiley.fulfilmentprocess.actions.eloqua.EloquaProcessStartAction;
import com.wiley.fulfilmentprocess.actions.order.WileyCheckOrderAction;


public class WileyasCheckOrderAction extends WileyCheckOrderAction
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyasCheckOrderAction.class);
	@Autowired
	private WileyOrderPaymentService wileyOrderPaymentService;
	@Autowired
	private WileyOrderService wileyOrderService;

	@Override
	public Set<String> getTransitions()
	{
		return AbstractSimpleDecisionAction.Transition.getStringValues();
	}

	@Override
	public String execute(final WileyOrderProcessModel orderProcessModel) throws RetryLaterException, Exception
	{
		return this.executeWileyasCheckOrderAction(orderProcessModel).toString();
	}

	public WileyasCheckOrderAction.Transition executeWileyasCheckOrderAction(final WileyOrderProcessModel wileyOrderProcessModel)
	{
		final OrderModel order = wileyOrderProcessModel.getOrder();
		Preconditions.checkNotNull(order, "Order process "
				+ wileyOrderProcessModel.getCode() + " should not be null");

		if (wileyOrderService.isExternalOrder(order))
		{
			return Transition.SKIP_PAYMENT;
		}

		if (!isOrderValid(order))
		{
			return failWileyasOrder(order);
		}

		setOrderStatus(order, OrderStatus.CHECKED_VALID);
		if (!wileyOrderProcessModel.getProcessPayment())
		{
			return Transition.SKIP_PAYMENT;
		}
		return Transition.OK;
	}

	/**
	 * Method sets orderStatus to CHECKED_INVALID and returns NOK transition
	 */
	protected Transition failWileyasOrder(final OrderModel orderModel)
	{
		setOrderStatus(orderModel, OrderStatus.CHECKED_INVALID);
		return Transition.NOK;
	}

	@Override
	protected boolean validateOrderStatus(final OrderModel order)
	{
		return true;
	}

	protected boolean isOrderValid(final OrderModel order)
	{

		return simpleCheckOrder(order) && checkPendingPayments(order);
	}

	private boolean checkPendingPayments(final OrderModel order)
	{
		if (wileyOrderService.isOrderEligibleForPayment(order))
		{
			Collection<PendingPaymentActon> pendingPaymentActions =
					wileyOrderPaymentService.getPendingPaymentActions(order);

			for (PendingPaymentActon pendingPaymentAction : pendingPaymentActions)
			{
				if (pendingPaymentAction.getAction() == PaymentActonType.AUTHORIZE)
				{
					LOG.error("Order {} is pending authorization for amount {}. Unable to fulfill this order",
							order.getCode(), pendingPaymentAction.getAmount());
					return false;
				}
			}
		}
		return true;

	}

	/**
	 * The enum Transition.
	 */
	public enum Transition
	{
		OK,
		NOK,
		/**
		 * if should skip payment and refund steps.
		 */
		SKIP_PAYMENT;

		/**
		 * Gets string values.
		 *
		 * @return the string values
		 */
		public static Set<String> getStringValues()
		{
			final Set<String> res = new HashSet<String>();

			for (final EloquaProcessStartAction.Transition transition : EloquaProcessStartAction.Transition.values())
			{
				res.add(transition.toString());
			}
			return res;
		}
	}
}
