package com.wiley.fulfilmentprocess.actions.order.subscription.renewal;

import de.hybris.platform.commerceservices.impersonation.ImpersonationContext;
import de.hybris.platform.commerceservices.impersonation.ImpersonationService;
import de.hybris.platform.commerceservices.order.CommerceCheckoutService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.processengine.model.BusinessProcessParameterModel;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.tx.Transaction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.fulfilmentprocess.model.WileySubscriptionProcessModel;


/**
 * Created by Raman_Hancharou on 12/17/2015.
 */
public class PlaceOrderAction extends AbstractSimpleDecisionAction<WileySubscriptionProcessModel>
{

	private static final Logger LOG = LoggerFactory.getLogger(PlaceOrderAction.class);

	private CommerceCheckoutService commerceCheckoutService;
	private ImpersonationService impersonationService;
	private EventService eventService;

	@Override
	public Transition executeAction(final WileySubscriptionProcessModel process)
	{
		ServicesUtil.validateParameterNotNull(process, "Process can not be null");
		try
		{
			final BusinessProcessParameterModel clonedCartParameter = processParameterHelper.getProcessParameterByName(process,
					"cart");
			final CartModel cart = (CartModel) clonedCartParameter.getValue();
			this.modelService.refresh(cart);

			final ImpersonationContext context = new ImpersonationContext();
			context.setOrder(cart);
			context.setLanguage(cart.getUser().getSessionLanguage());
			final OrderModel orderModel = getImpersonationService().executeInContext(context,
					new ImpersonationService.Executor<OrderModel, ImpersonationService.Nothing>()
					{
						@Override
						public OrderModel execute()
						{
							final OrderModel orderModel;
							try
							{
								Transaction.current().enableDelayedStore(false);
								orderModel = getCommerceCheckoutService().placeOrder(cart);
							}
							catch (final InvalidCartException e)
							{
								throw new RuntimeException(e.getMessage(), e);
							}
							return orderModel;
						}
					});
			getModelService().save(orderModel);
			LOG.info("Process {}: order {} has been successfully placed", process.getCode(), orderModel.getCode());
			return Transition.OK;
		}
		catch (Exception e)
		{
			LOG.error("Renewal subscription process {} has been failed. Reason: {}", process.getCode(), e);
			return Transition.NOK;
		}
	}

	protected CommerceCheckoutService getCommerceCheckoutService()
	{
		return commerceCheckoutService;
	}

	@Required
	public void setCommerceCheckoutService(final CommerceCheckoutService commerceCheckoutService)
	{
		this.commerceCheckoutService = commerceCheckoutService;
	}

	protected ImpersonationService getImpersonationService()
	{
		return impersonationService;
	}

	@Required
	public void setImpersonationService(final ImpersonationService impersonationService)
	{
		this.impersonationService = impersonationService;
	}

	protected EventService getEventService()
	{
		return eventService;
	}

	@Required
	public void setEventService(final EventService eventService)
	{
		this.eventService = eventService;
	}
}
