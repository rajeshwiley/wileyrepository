package com.wiley.fulfilmentprocess.actions.order;

import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.task.RetryLaterException;

import com.wiley.core.model.WileyOrderProcessModel;


public class WileyDeactivateProcessAction extends AbstractSimpleDecisionAction<WileyOrderProcessModel>
{
	@Override
	public Transition executeAction(final WileyOrderProcessModel process) throws RetryLaterException, Exception
	{
		process.setActiveFor(null);
		modelService.save(process);
		return Transition.OK;
	}
}
