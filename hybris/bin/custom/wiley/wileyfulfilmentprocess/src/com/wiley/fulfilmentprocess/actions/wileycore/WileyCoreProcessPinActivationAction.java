package com.wiley.fulfilmentprocess.actions.wileycore;

import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;

import javax.annotation.Resource;

import com.wiley.core.pin.service.PinIntegrationService;
import com.wiley.fulfilmentprocess.model.WileyCoreOrderExportProcessModel;


/**
 * Notifies Wiley Core external system for orders placed with pin activation
 */
public class WileyCoreProcessPinActivationAction extends AbstractSimpleDecisionAction<WileyCoreOrderExportProcessModel>
{

	@Resource
	private PinIntegrationService pinIntegrationService;

	@Override
	public Transition executeAction(final WileyCoreOrderExportProcessModel wileyCoreOrderExportProcessModel)
	{
		return pinIntegrationService.activatePinForOrder(wileyCoreOrderExportProcessModel.getOrder()) ?
				Transition.OK :
				Transition.NOK;
	}

}
