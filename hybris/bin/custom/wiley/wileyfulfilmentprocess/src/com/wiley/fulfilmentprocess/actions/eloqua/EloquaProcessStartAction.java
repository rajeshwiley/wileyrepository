package com.wiley.fulfilmentprocess.actions.eloqua;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.processengine.action.AbstractAction;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.task.RetryLaterException;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.Nonnull;

import com.wiley.fulfilmentprocess.model.EloquaOrderExportProcessModel;


/**
 * Action checks if hybris system knows about Eloqua Contact assigned to Customer.
 *
 * @author Aliaksei_Zlobich
 */
public class EloquaProcessStartAction extends AbstractAction<EloquaOrderExportProcessModel>
{

	/**
	 * The enum Transition.
	 */
	public enum Transition
	{

		/**
		 * if customer has eloqua id assigned to him/her.
		 */
		ELOQUA_ID_EXISTS,

		/**
		 * if customer has no eloqua id assinged to him/her.
		 */
		ELOQUA_ID_NOT_EXIST;

		/**
		 * Gets string values.
		 *
		 * @return the string values
		 */
		public static Set<String> getStringValues()
		{
			final Set<String> res = new HashSet<String>();

			for (final Transition transition : Transition.values())
			{
				res.add(transition.toString());
			}
			return res;
		}
	}

	@Override
	public String execute(@Nonnull final EloquaOrderExportProcessModel businessProcess) throws RetryLaterException, Exception
	{
		ServicesUtil.validateParameterNotNullStandardMessage("businessProcess", businessProcess);

		final OrderModel order = businessProcess.getOrder();
		final UserModel user = order.getUser();

		if (!(user instanceof CustomerModel))
		{
			throw new IllegalStateException("Order should be assigned to Customer.");
		}

		CustomerModel customer = (CustomerModel) user;

		final Transition transition =
				customer.getEloquaId() != null ? Transition.ELOQUA_ID_EXISTS : Transition.ELOQUA_ID_NOT_EXIST;

		return transition.toString();
	}

	@Override
	public Set<String> getTransitions()
	{
		return Transition.getStringValues();
	}
}
