package com.wiley.fulfilmentprocess.actions.order.wel;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.task.RetryLaterException;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wiley.core.integration.ebp.dto.EbpAddProductsPayload;
import com.wiley.core.integration.ebp.dto.EbpCreateUserPayload;
import com.wiley.core.integration.ebp.EbpGateway;
import com.wiley.core.integration.ebp.dto.MessageWSResponse;

import static com.wiley.fulfilmentprocess.constants.WileyFulfilmentProcessConstants.EBP_RESPONSE_SUCCESS_CODE_200;
import static com.wiley.fulfilmentprocess.constants.WileyFulfilmentProcessConstants.EBP_RESPONSE_SUCCESS_CODE_201;


/**
 * Submits the order information to EBP backend system.
 */
public class NotifyEBPAction extends AbstractSimpleDecisionAction<OrderProcessModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(NotifyEBPAction.class);

	@Resource
	private EbpGateway ebpGateway;

	@Override
	public Transition executeAction(final OrderProcessModel orderProcessModel)
			throws RetryLaterException, Exception
	{
		final OrderModel order = orderProcessModel.getOrder();
		try
		{
			final MessageWSResponse createUserResponse = ebpGateway.createEbpUser(createEbpCreateUserPayload(order))
					.getResponse();
			final int createUserResponseCode = createUserResponse.getResponseCode();
			if (createUserResponseCode != EBP_RESPONSE_SUCCESS_CODE_200
					&& createUserResponseCode != EBP_RESPONSE_SUCCESS_CODE_201)
			{
				LOG.error("EBP create User fail. Service response message: {}", createUserResponse.getStatusMessage(),
						"EBP create User - response code: ", createUserResponseCode);
				return Transition.NOK;
			}
		}
		catch (Exception ex)
		{

			LOG.error("Method createEbpUser failed", ex);
			return Transition.NOK;
		}
		try
		{
			final MessageWSResponse addProductResponse = ebpGateway.addProducts(createEbpAddProductsPayload(order)).getResponse();
			final int addProductResponseCode = addProductResponse.getResponseCode();
			if (addProductResponseCode != EBP_RESPONSE_SUCCESS_CODE_200)
			{
				LOG.error("EBP add Products fail. Service response message: {}", addProductResponse.getStatusMessage(),
						"EBP add Products - response code: ", addProductResponseCode);
				return Transition.NOK;
			}
		}
		catch (Exception ex)
		{
			LOG.error("Method addProducts failed", ex);
			return Transition.NOK;
		}
		return Transition.OK;
	}

	private EbpAddProductsPayload createEbpAddProductsPayload(final OrderModel order)
	{
		EbpAddProductsPayload ebpAddProductsPayload = new EbpAddProductsPayload();
		ebpAddProductsPayload.setOrder(order);

		return ebpAddProductsPayload;
	}

	/**
	 * Create EbpCreateUserPayload object. Fill with data from order.
	 *
	 * @param order
	 * @return
	 */
	private EbpCreateUserPayload createEbpCreateUserPayload(final OrderModel order)
	{
		//
		// note: customer and university data is populated from order, but the order itself presents in EbpCreateUserPayload
		// maybe it redundant
		//

		EbpCreateUserPayload ebpCreateUserPayload = new EbpCreateUserPayload();
		ebpCreateUserPayload.setOrder(order);
		ebpCreateUserPayload.setCustomer((CustomerModel) order.getUser());

		ebpCreateUserPayload.setUniversity(order.getUniversity());
		ebpCreateUserPayload.setUniversityState(order.getUniversityState());
		ebpCreateUserPayload.setUniversityCountry(order.getUniversityCountry());

		return ebpCreateUserPayload;
	}
}
