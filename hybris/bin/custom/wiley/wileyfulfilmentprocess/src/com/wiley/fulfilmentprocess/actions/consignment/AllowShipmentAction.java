/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.fulfilmentprocess.actions.consignment;

import de.hybris.platform.commerceservices.model.PickUpDeliveryModeModel;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.processengine.action.AbstractAction;
import de.hybris.platform.warehouse.Process2WarehouseAdapter;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


/**
 * The type Allow shipment action.
 */
public class AllowShipmentAction extends AbstractAction<ConsignmentProcessModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(AllowShipmentAction.class);
	private Process2WarehouseAdapter process2WarehouseAdapter;

	/**
	 * The enum Transition.
	 */
	public enum Transition
	{
		/**
		 * Delivery transition.
		 */
		DELIVERY,
		/**
		 * Pickup transition.
		 */
		PICKUP,
		/**
		 * Cancel transition.
		 */
		CANCEL,
		/**
		 * Error transition.
		 */
		ERROR;

		/**
		 * Gets string values.
		 *
		 * @return the string values
		 */
		public static Set<String> getStringValues()
		{
			final Set<String> res = new HashSet<String>();

			for (final Transition transition : Transition.values())
			{
				res.add(transition.toString());
			}
			return res;
		}
	}

	/**
	 * Execute string.
	 *
	 * @param process
	 * 		the process
	 * @return the string
	 */
	@Override
	public String execute(final ConsignmentProcessModel process)
	{
		final ConsignmentModel consignment = process.getConsignment();
		if (consignment != null)
		{
			try
			{
				// Check if the Order is Cancelled
				if (OrderStatus.CANCELLED.equals(consignment.getOrder().getStatus()) || OrderStatus.CANCELLING.equals(
						consignment.getOrder().getStatus()))
				{
					return Transition.CANCEL.toString();
				}
				else
				{
					getProcess2WarehouseAdapter().shipConsignment(process.getConsignment());
					if (consignment.getDeliveryMode() instanceof PickUpDeliveryModeModel)
					{
						return Transition.PICKUP.toString();
					}
					else
					{
						return Transition.DELIVERY.toString();
					}
				}
			}
			catch (final Exception e)
			{
				LOG.warn("Error while executing an AllowShipmentAction: " + e.getMessage());
				return Transition.ERROR.toString();
			}
		}
		return Transition.ERROR.toString();
	}

	/**
	 * Gets process 2 warehouse adapter.
	 *
	 * @return the process 2 warehouse adapter
	 */
	protected Process2WarehouseAdapter getProcess2WarehouseAdapter()
	{
		return process2WarehouseAdapter;
	}

	/**
	 * Sets process 2 warehouse adapter.
	 *
	 * @param process2WarehouseAdapter
	 * 		the process 2 warehouse adapter
	 */
	@Required
	public void setProcess2WarehouseAdapter(final Process2WarehouseAdapter process2WarehouseAdapter)
	{
		this.process2WarehouseAdapter = process2WarehouseAdapter;
	}

	/**
	 * Gets transitions.
	 *
	 * @return the transitions
	 */
	@Override
	public Set<String> getTransitions()
	{
		return Transition.getStringValues();
	}
}
