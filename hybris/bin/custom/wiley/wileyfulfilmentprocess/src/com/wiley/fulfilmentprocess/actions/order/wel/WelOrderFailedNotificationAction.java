package com.wiley.fulfilmentprocess.actions.order.wel;

import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.processengine.model.ProcessTaskLogModel;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.task.RetryLaterException;

import java.util.Collection;
import java.util.Objects;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.event.OrderHasBeenFailedEvent;


public class WelOrderFailedNotificationAction extends AbstractProceduralAction<OrderProcessModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(WelOrderFailedNotificationAction.class);
	public static final String ORDER_FAILED_NOTIFICATION_ACTION = "orderFailedNotification";

	@Autowired
	private EventService eventService;

	@Override
	public void executeAction(final OrderProcessModel orderProcessModel) throws RetryLaterException, Exception
	{
		final boolean orderFailedNotificationWasNotSent = isOrderFailedNotificationWasNotSent(orderProcessModel);
		if (orderFailedNotificationWasNotSent) {
			eventService.publishEvent(new OrderHasBeenFailedEvent(orderProcessModel));
		}
		LOG.error("Process {} of order fulfilment has been failed", orderProcessModel.getCode());
	}

	private boolean isOrderFailedNotificationWasNotSent(final OrderProcessModel orderProcessModel)
	{
		Collection<ProcessTaskLogModel> taskLogs = orderProcessModel.getTaskLogs();
		final Optional<ProcessTaskLogModel> anyOrderFailedNotification = taskLogs.stream().filter(
				task -> Objects.equals(ORDER_FAILED_NOTIFICATION_ACTION, task.getActionId())).findAny();
		return !anyOrderFailedNotification.isPresent();
	}
}
