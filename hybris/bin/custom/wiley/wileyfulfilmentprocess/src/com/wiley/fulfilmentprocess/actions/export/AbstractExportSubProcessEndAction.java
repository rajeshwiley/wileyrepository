package com.wiley.fulfilmentprocess.actions.export;

import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.processengine.model.BusinessProcessModel;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import com.wiley.fulfilmentprocess.constants.WileyFulfilmentProcessConstants;
import com.wiley.fulfilmentprocess.model.WileyExportProcessModel;

import static java.util.Objects.nonNull;


/**
 * Base class for actions which are responsible for sending notification to parent process.
 */
public abstract class AbstractExportSubProcessEndAction<T extends WileyExportProcessModel> extends AbstractSimpleDecisionAction<T>
{

	private static final Logger LOG = LoggerFactory.getLogger(AbstractExportSubProcessEndAction.class);

	@Resource
	private BusinessProcessService businessProcessService;

	protected void sendSubProcessCompletedEvent(@Nonnull final WileyExportProcessModel exportSubProcess)
	{
		Assert.notNull(exportSubProcess, "Parameter exportSubProcess cannot be null.");

		BusinessProcessModel parentProcess = exportSubProcess.getParentProcess();

		if (nonNull(parentProcess))
		{
			final String event =
					parentProcess.getCode() + "_" + WileyFulfilmentProcessConstants.WILEY_EXPORT_SUBPROCESS_END_EVENT_NAME;

			getBusinessProcessService().triggerEvent(event);
			LOG.debug("Triggering event: {}.", event);
		}
		else
		{
			LOG.debug(
					"SubProcessCompletedEvent was not sent to parent process because ExportSubProcess [{}]"
							+ " had no assigned parent process.",
					exportSubProcess.getCode());
		}
	}

	public BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}
}
