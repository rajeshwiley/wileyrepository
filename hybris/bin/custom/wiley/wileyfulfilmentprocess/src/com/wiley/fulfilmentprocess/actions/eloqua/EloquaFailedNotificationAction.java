package com.wiley.fulfilmentprocess.actions.eloqua;

import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.task.RetryLaterException;

import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wiley.fulfilmentprocess.model.EloquaOrderExportProcessModel;


/**
 * Action aggregates invalid transitions, logs an error.
 *
 * @author Aliaksei_Zlobich
 */
public class EloquaFailedNotificationAction extends AbstractProceduralAction<EloquaOrderExportProcessModel>
{

	private static final Logger LOG = LoggerFactory.getLogger(EloquaFailedNotificationAction.class);

	@Override
	public void executeAction(@Nonnull final EloquaOrderExportProcessModel businessProcess) throws RetryLaterException, Exception
	{
		ServicesUtil.validateParameterNotNullStandardMessage("businessProcess", businessProcess);

		final String standartMessage = "Process [{}] failed to export order [{}] to the system [{}].";

		if (businessProcess.getParentProcess() == null)
		{
			LOG.error(standartMessage, businessProcess.getCode(), businessProcess.getOrder().getCode(),
					businessProcess.getSystemCode());
		}
		else
		{
			final String extendedMessage = standartMessage + " Parent business process [{}].";

			LOG.error(extendedMessage, businessProcess.getCode(), businessProcess.getOrder().getCode(),
					businessProcess.getSystemCode(),
					businessProcess.getParentProcess().getCode());
		}
	}
}
