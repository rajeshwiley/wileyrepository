package com.wiley.fulfilmentprocess.actions.wileycore;

import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;

import javax.annotation.Resource;

import com.wiley.core.pin.service.PinIntegrationService;
import com.wiley.fulfilmentprocess.model.WileyCoreOrderExportProcessModel;


/**
 * Validates the order if it was placed by pin activation
 */
public class WileyCoreProcessStartAction extends AbstractSimpleDecisionAction<WileyCoreOrderExportProcessModel>
{

	@Resource
	private PinIntegrationService pinIntegrationService;

	@Override
	public Transition executeAction(final WileyCoreOrderExportProcessModel wileyCoreOrderExportProcessModel)
	{
		return pinIntegrationService.isPinUsedForOrderPlacement(wileyCoreOrderExportProcessModel.getOrder()) ?
				Transition.OK :
				Transition.NOK;
	}

}
