package com.wiley.fulfilmentprocess.actions.order.wileycom;

import com.wiley.core.exceptions.ExternalSystemException;
import com.wiley.core.wileycom.customer.service.WileycomCustomerService;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import org.apache.log4j.Logger;

import javax.annotation.Resource;
import com.wiley.core.customer.service.WileyCustomerAccountService;


public class WileycomUpdateUserProfileAction extends AbstractProceduralAction<OrderProcessModel>
{
	private static final Logger LOG = Logger.getLogger(WileycomUpdateUserProfileAction.class);

	@Resource
	private WileyCustomerAccountService wileyCustomerAccountService;
	@Resource
	private WileycomCustomerService wileycomCustomerService;

	@Override
	public void executeAction(final OrderProcessModel orderProcessModel) throws DuplicateUidException
	{
		final OrderModel order = orderProcessModel.getOrder();
		final CustomerModel customer = (CustomerModel) order.getUser();

		if (orderContainsTextbook(order) && schoolWasUpdated(order, customer))
		{
			customer.setSchool(order.getSchool());
			if (CustomerType.GUEST.equals(customer.getType()))
			{
				try
				{
				wileyCustomerAccountService.updateCustomerProfile(customer);
				}
				catch (ExternalSystemException externalSystemException)
		    {
		      LOG.error("CDM profile update has failed, Hybris customer update profile skipped");
		      LOG.error(externalSystemException);
		    }
				LOG.info("Updated school in customer profile");
			}
			else
			{
				getModelService().refresh(customer);
			}
		}
	}

	private boolean orderContainsTextbook(final OrderModel order)
	{
		return order.getEntries().stream().anyMatch(
				entry -> Boolean.TRUE.equals(entry.getProduct().getTextbook()));
	}

	private boolean schoolWasUpdated(final OrderModel order, final CustomerModel customer)
	{
		boolean schoolWasUpdated = false;
		if (order.getSchool() == null && customer.getSchool() != null)
		{
			schoolWasUpdated = true;
		}
		else if (order.getSchool() != null && customer.getSchool() == null)
		{
			schoolWasUpdated = true;
		}
		else if (order.getSchool() != null
				&& customer.getSchool() != null
				&& !order.getSchool().getCode().equals(customer.getSchool().getCode()))
		{
			schoolWasUpdated = true;
		}
		return schoolWasUpdated;
	}
}
