package com.wiley.fulfilmentprocess.actions.order.wel;

import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.task.RetryLaterException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.event.PreOrderPlacementEvent;


public class PreOrderNotificationAction extends AbstractProceduralAction<OrderProcessModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(PreOrderNotificationAction.class);

	@Autowired
	private EventService eventService;

	@Override
	public void executeAction(final OrderProcessModel orderProcessModel) throws RetryLaterException, Exception
	{
		eventService.publishEvent(new PreOrderPlacementEvent(orderProcessModel));
	}
}
