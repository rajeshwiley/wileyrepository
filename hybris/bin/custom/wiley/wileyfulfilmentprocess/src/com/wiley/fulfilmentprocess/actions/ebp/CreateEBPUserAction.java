package com.wiley.fulfilmentprocess.actions.ebp;

import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.task.RetryLaterException;

import javax.annotation.Resource;

import com.wiley.core.integration.ebp.EbpGateway;
import com.wiley.core.integration.ebp.dto.EbpCreateUserPayload;
import com.wiley.core.integration.ebp.dto.MessageWSResponse;
import com.wiley.fulfilmentprocess.model.CreateEbpUserProcessModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.wiley.fulfilmentprocess.constants.WileyFulfilmentProcessConstants.EBP_RESPONSE_SUCCESS_CODE_200;
import static com.wiley.fulfilmentprocess.constants.WileyFulfilmentProcessConstants.EBP_RESPONSE_SUCCESS_CODE_201;


/**
 * Submits the user details to EBP backend system.
 */
public class CreateEBPUserAction extends AbstractSimpleDecisionAction<CreateEbpUserProcessModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(CreateEBPUserAction.class);

	@Resource
	private EbpGateway ebpGateway;

	@Override
	public Transition executeAction(final CreateEbpUserProcessModel createEbpUserProcessModel)
			throws RetryLaterException, Exception
	{

		EbpCreateUserPayload userPayload = new EbpCreateUserPayload();
		fillPayload(createEbpUserProcessModel, userPayload);

		final MessageWSResponse createUserResponse = ebpGateway.createEbpUser(userPayload).getResponse();

		final int createUserResponseCode = createUserResponse.getResponseCode();

		if (createUserResponseCode != EBP_RESPONSE_SUCCESS_CODE_200 && createUserResponseCode != EBP_RESPONSE_SUCCESS_CODE_201)
		{
			LOG.error("EBP create User fail. Service response message: {}", createUserResponse.getStatusMessage(),
					"EBP create User - response code: ", createUserResponseCode);
			return Transition.NOK;
		}
		return Transition.OK;
	}

	private void fillPayload(final CreateEbpUserProcessModel createEbpUserProcessModel, final EbpCreateUserPayload userPayload)
	{
		userPayload.setCustomer(createEbpUserProcessModel.getCustomer());
		userPayload.setOldEmailAddress(createEbpUserProcessModel.getOldEmailAddress());
	}
}
