package com.wiley.fulfilmentprocess.actions.wileycore;

import com.wiley.fulfilmentprocess.actions.export.AbstractExportSubProcessEndAction;
import com.wiley.fulfilmentprocess.model.WileyCoreOrderExportProcessModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import javax.annotation.Nonnull;
import javax.annotation.Resource;
import java.util.Collections;
import java.util.Set;


public class WileyCoreProcessEndAction extends AbstractExportSubProcessEndAction<WileyCoreOrderExportProcessModel>
{
	@Resource
	private BusinessProcessService businessProcessService;

	@Override
	public Transition executeAction(@Nonnull final WileyCoreOrderExportProcessModel businessProcess)
			throws Exception
	{
		ServicesUtil.validateParameterNotNullStandardMessage("businessProcess", businessProcess);
		businessProcess.setDone(true);
		save(businessProcess);
		sendSubProcessCompletedEvent(businessProcess);
		return Transition.OK;
	}

	@Override
	public Set<String> getTransitions()
	{
		return Collections.singleton(Transition.OK.toString());
	}
}
