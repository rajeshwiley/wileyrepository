package com.wiley.fulfilmentprocess.actions.order.wileycom;

import de.hybris.platform.orderprocessing.events.OrderPlacedEvent;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.task.RetryLaterException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * The type Send order placed notification action.
 */
public class WileycomSendOrderPlacedNotificationAction extends AbstractProceduralAction<OrderProcessModel>
{
	private static final Logger LOG = Logger.getLogger(WileycomSendOrderPlacedNotificationAction.class);

	@Autowired
	private EventService eventService;

	@Override
	public void executeAction(final OrderProcessModel process)
			throws RetryLaterException, Exception
	{
		eventService.publishEvent(new OrderPlacedEvent(process));
		LOG.info("Process: " + process.getCode() + " in step " + getClass());
	}
}
