package com.wiley.fulfilmentprocess.listeners;

import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.event.StartExportProcessEvent;
import com.wiley.fulfilmentprocess.services.export.WileyExportService;


/**
 * Listens for startExportProcesses events.
 */
public class StartExportProcessesEventListener extends AbstractEventListener<StartExportProcessEvent>
{
	private static final Logger LOG = LoggerFactory.getLogger(StartExportProcessesEventListener.class);

	private WileyExportService wileyExportService;

	@Override
	protected void onEvent(final StartExportProcessEvent event)
	{
		LOG.debug("Starting export to external systems after getting an event for order {}", event.getOrder().getCode());
		getWileyExportService().startExportProcess(event.getOrder(), event.getTransactionEntry(),
				event.getExportProcessType(), event.getExportSystemCode(), event.getBusinessProcess());
	}

	public WileyExportService getWileyExportService()
	{
		return wileyExportService;
	}

	@Required
	public void setWileyExportService(final WileyExportService wileyExportService)
	{
		this.wileyExportService = wileyExportService;
	}
}
