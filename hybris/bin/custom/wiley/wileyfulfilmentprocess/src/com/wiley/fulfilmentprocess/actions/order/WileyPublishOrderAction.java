package com.wiley.fulfilmentprocess.actions.order;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;

import javax.annotation.Resource;

import com.wiley.core.integration.order.WileyOrdersGateway;
import com.wiley.core.model.WileyOrderProcessModel;


public class WileyPublishOrderAction extends AbstractSimpleDecisionAction<WileyOrderProcessModel>
{
	@Resource
	private WileyOrdersGateway wileyOrdersGateway;

	@Override
	public Transition executeAction(final WileyOrderProcessModel process) throws Exception
	{
		final OrderModel order = process.getOrder();
		wileyOrdersGateway.publish(order, process.getPreviousOrderState());
		setOrderStatus(order, OrderStatus.EXPORTED_TO_ERP);
		return Transition.OK;
	}
}
