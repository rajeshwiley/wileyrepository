package com.wiley.fulfilmentprocess.actions.order.subscription.renewal;

import de.hybris.platform.commerceservices.impersonation.ImpersonationContext;
import de.hybris.platform.commerceservices.impersonation.ImpersonationService;
import de.hybris.platform.commerceservices.order.CommerceCheckoutService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.processengine.model.BusinessProcessParameterModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.task.RetryLaterException;
import de.hybris.platform.tx.Transaction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.fulfilmentprocess.model.WileySubscriptionProcessModel;


/**
 * Created by Raman_Hancharou on 12/17/2015.
 */
public class CalculateCartAction extends AbstractSimpleDecisionAction<WileySubscriptionProcessModel>
{

	private static final Logger LOG = LoggerFactory.getLogger(CalculateCartAction.class);

	private CommerceCheckoutService commerceCheckoutService;
	private ImpersonationService impersonationService;

	private CommerceCheckoutService getCommerceCheckoutService()
	{
		return commerceCheckoutService;
	}

	@Required
	public void setCommerceCheckoutService(final CommerceCheckoutService commerceCheckoutService)
	{
		this.commerceCheckoutService = commerceCheckoutService;
	}

	protected ImpersonationService getImpersonationService()
	{
		return impersonationService;
	}

	@Required
	public void setImpersonationService(final ImpersonationService impersonationService)
	{
		this.impersonationService = impersonationService;
	}

	@Override
	public Transition executeAction(final WileySubscriptionProcessModel process)
			throws RetryLaterException, Exception
	{
		ServicesUtil.validateParameterNotNull(process, "Process can not be null");
		try
		{
			final BusinessProcessParameterModel clonedCartParameter = processParameterHelper.getProcessParameterByName(process,
					"cart");
			final CartModel cart = (CartModel) clonedCartParameter.getValue();
			getModelService().refresh(cart);

			final ImpersonationContext context = new ImpersonationContext();
			context.setOrder(cart);
			getModelService().save(
					getImpersonationService().executeInContext(context,
							new ImpersonationService.Executor<CartModel, ImpersonationService.Nothing>()
							{
								@Override
								public CartModel execute()
								{
									Transaction.current().enableDelayedStore(false);
									getCommerceCheckoutService().calculateCart(cart);
									return cart;
								}
							}));
			LOG.info("Subscription renewal procces {}: cart {} has been recalculated. ", process.getCode(), cart.getCode());
			return Transition.OK;
		}
		catch (Exception e)
		{
			LOG.error("Subscription renewal procces {} has been failed. Reason {} ", process.getCode(), e);
			return Transition.NOK;
		}
	}
}
