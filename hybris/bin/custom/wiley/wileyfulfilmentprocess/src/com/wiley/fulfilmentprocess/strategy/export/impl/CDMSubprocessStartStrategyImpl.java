/**
 * 
 */
package com.wiley.fulfilmentprocess.strategy.export.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import com.wiley.fulfilmentprocess.model.CDMCustomerExportProcessModel;
import com.wiley.fulfilmentprocess.strategy.export.WileyDecideExportSubprocessStartStrategy;
import com.wiley.integrations.cdm.dao.CDMSubprocessExportDao;

public class CDMSubprocessStartStrategyImpl implements WileyDecideExportSubprocessStartStrategy
{
  private static final Logger LOG = Logger.getLogger(CDMSubprocessStartStrategyImpl.class);
  
  private CDMSubprocessExportDao cdmSubprocessExportDao;

  @Override
  public boolean shouldStartExportSystem(final OrderModel order) 
  {
    UserModel user = order.getUser();
    
    if (!(user instanceof CustomerModel))
    {
      throw new IllegalStateException("Order should be assigned to Customer.");
    }
    
    CustomerModel customer = (CustomerModel) user;
    List<CDMCustomerExportProcessModel> exportProcesses = getCdmSubprocessExportDao().getCDMExportProcesses(customer);

    // Check whether the CDM export process is already executed for the customer.
    if (CollectionUtils.isEmpty(exportProcesses))
    {
      LOG.info("CDM export process not found for customer. CDM process will be started.");
      return true;
    }
    
    LOG.info("CDM export process found for customer. CDM process will not be started.");
    return false;
  }
  
  
  /**
   * @return the cdmSubprocessExportDao
   */
  public CDMSubprocessExportDao getCdmSubprocessExportDao() {
    return cdmSubprocessExportDao;
  }

  /**
   * @param cdmSubprocessExportDao the cdmSubprocessExportDao to set
   */
  public void setCdmSubprocessExportDao(final CDMSubprocessExportDao cdmSubprocessExportDao) {
    this.cdmSubprocessExportDao = cdmSubprocessExportDao;
  }
  
}
