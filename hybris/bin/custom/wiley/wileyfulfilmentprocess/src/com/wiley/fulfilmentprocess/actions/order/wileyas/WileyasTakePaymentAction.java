package com.wiley.fulfilmentprocess.actions.order.wileyas;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.task.RetryLaterException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.order.PaymentActonType;
import com.wiley.core.order.PendingPaymentActon;
import com.wiley.core.order.WileyCheckoutService;
import com.wiley.core.order.service.WileyOrderService;
import com.wiley.core.wiley.order.WileyOrderPaymentService;
import com.wiley.fulfilmentprocess.actions.eloqua.EloquaProcessStartAction;
import com.wiley.fulfilmentprocess.actions.order.TakePaymentAction;


public class WileyasTakePaymentAction extends TakePaymentAction
{

	private static final Double ZERO = 0.0D;

	@Autowired
	private WileyOrderService wileyOrderService;
	@Autowired
	private WileyOrderPaymentService wileyOrderPaymentService;
	@Autowired
	private WileyCheckoutService wileyCheckoutService;

	private static final Logger LOG = LoggerFactory.getLogger(WileyasTakePaymentAction.class);

	@Override
	public Set<String> getTransitions()
	{
		return AbstractSimpleDecisionAction.Transition.getStringValues();
	}

	@Override
	public String execute(final OrderProcessModel orderProcessModel) throws RetryLaterException, Exception
	{
		return this.executeWileyasTakePaymentAction(orderProcessModel).toString();
	}

	/**
	 * Execute action transition.
	 *
	 * @param process the process
	 * @return the transition
	 */
	public Transition executeWileyasTakePaymentAction(final OrderProcessModel process)
	{
		final OrderModel order = process.getOrder();

		markEntriesWithZeroPrice(order);
		if (isZeroOrder(order) || !wileyOrderService.isOrderEligibleForPayment(order))
		{
			return getSuccessTransition(order);
		}

		if (!validateOrder(order))
		{
			return failOrder(order);
		}

		PaymentTransactionModel captureTransaction = getPendingCaptureTransaction(order);

		if (captureSingleTransaction(order, captureTransaction))
		{
			return getSuccessTransition(order);
		}
		else
		{
			return failOrder(order);
		}
	}

	private Transition getSuccessTransition(final OrderModel order)
	{
		if (wileyOrderService.isNewOrder(order))
		{
			return Transition.SKIP_REFUND;
		}
		return Transition.OK;
	}

	private boolean captureSingleTransaction(final OrderModel order, final PaymentTransactionModel captureTransaction)
	{
		boolean captureResult = true;
		if (captureTransaction != null)
		{
			List<Boolean> result = new ArrayList<>();
			capturePayment(result, captureTransaction);

			if (Boolean.TRUE.equals(result.get(0)))
			{
				setOrderStatus(order, OrderStatus.PAYMENT_CAPTURED);
			}
			else
			{
				captureResult = false;
			}
		}
		return captureResult;
	}

	private PaymentTransactionModel getPendingCaptureTransaction(final OrderModel order)
	{
		Collection<PendingPaymentActon> pendingPaymentActions = wileyOrderPaymentService.getPendingPaymentActions(order);
		PendingPaymentActon action = pendingPaymentActions
				.stream()
				.filter(pa -> pa.getAction() == PaymentActonType.CAPTURE)
				.findFirst().orElse(null);
		if (action != null)
		{
			LOG.info("Found pending payment CAPTURE for order '{}', amount '{}'", order.getCode(), action.getAmount());
			return action.getTransaction();
		}
		else
		{
			setOrderEntriesStatus(order, OrderStatus.PAYMENT_CAPTURED);
			LOG.info("No pending payment CAPTURE found for order '{}'", order.getCode());
			return null;
		}
	}

	@Override
	protected void setOrderStatus(final OrderModel order, final OrderStatus orderStatus)
	{
		super.setOrderStatus(order, orderStatus);
		setOrderEntriesStatus(order, orderStatus);
	}

	private void setOrderEntriesStatus(final OrderModel order, final OrderStatus orderStatus)
	{
		order.getEntries().forEach(entry ->
		{
			if (entry.getStatus() != OrderStatus.CREATED)
			{
				LOG.warn("OrderEntry for order [{}] was not in expected status [{}]",
						entry.getOrder().getCode(), OrderStatus.CREATED);
			}
			if (!isZeroEntry(entry))
			{
				entry.setStatus(orderStatus);
			}
		});
		getModelService().saveAll(order.getEntries());
	}

	private Transition failOrder(final OrderModel order)
	{
		setOrderStatus(order, OrderStatus.PAYMENT_FAILED);
		return Transition.NOK;
	}

	private boolean validateOrder(final OrderModel orderModel)
	{
		boolean validPayment = CollectionUtils.isNotEmpty(orderModel.getPaymentTransactions());
		for (final PaymentTransactionModel txn : orderModel.getPaymentTransactions())
		{
			validPayment = validPayment && (getWileyMPGSPaymentProviderService().isMPGSProviderGroup(txn.getPaymentProvider())
					|| isPayPalTransaction(txn));
		}
		return validPayment;
	}

	protected void markEntriesWithZeroPrice(final OrderModel orderModel)
	{
		orderModel.getEntries().forEach(entry -> {
			if (OrderStatus.CANCELLED != entry.getStatus() && (isZeroEntry(entry) || isZeroOrder(orderModel)))
			{
				entry.setStatus(OrderStatus.PAYMENT_WAIVED);
			}
		});
		getModelService().saveAll(orderModel.getEntries());
	}

	private boolean isZeroOrder(final OrderModel orderModel)
	{
		return !wileyCheckoutService.isNonZeroOrder(orderModel);
	}

	private boolean isZeroEntry(final AbstractOrderEntryModel entry)
	{
		return entry.getTotalPrice().equals(ZERO);
	}

	/**
	 * The enum Transition.
	 */
	public enum Transition
	{
		OK,
		NOK,
		/**
		 * if should skip refund step.
		 */
		SKIP_REFUND;

		/**
		 * Gets string values.
		 *
		 * @return the string values
		 */
		public static Set<String> getStringValues()
		{
			final Set<String> res = new HashSet<String>();

			for (final EloquaProcessStartAction.Transition transition : EloquaProcessStartAction.Transition.values())
			{
				res.add(transition.toString());
			}
			return res;
		}
	}
}
