package com.wiley.fulfilmentprocess.actions.order.wel;

import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.task.RetryLaterException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wiley.core.model.WileyOrderProcessModel;
import com.wiley.fulfilmentprocess.actions.order.WileyCheckOrderAction;


/**
 * Wel class for custom logic during order check.
 */
public class WelCheckOrderAction extends WileyCheckOrderAction
{
	private static final Logger LOG = LoggerFactory.getLogger(WelCheckOrderAction.class);


	@Override
	public AbstractSimpleDecisionAction.Transition executeWileyCheckOrderAction(
			final WileyOrderProcessModel wileyOrderProcessModel)
			throws RetryLaterException, Exception
	{
		return super.executeWileyCheckOrderAction(wileyOrderProcessModel);
	}
}
