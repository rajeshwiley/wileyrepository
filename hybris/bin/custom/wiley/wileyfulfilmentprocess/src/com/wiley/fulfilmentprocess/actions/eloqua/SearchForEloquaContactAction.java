package com.wiley.fulfilmentprocess.actions.eloqua;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.processengine.action.AbstractAction;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.task.RetryLaterException;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wiley.fulfilmentprocess.model.EloquaOrderExportProcessModel;
import com.wiley.core.integration.eloqua.EloquaGateway;
import com.wiley.core.integration.eloqua.dto.ContactDto;


/**
 * Action tries to find Eloqua Contact by customer's email address.<br/>
 * Stores the contactId as eloquaId if the contact was found.<br/>
 * Uses Spring Integration approach.
 *
 * @author Aliaksei_Zlobich
 */
public class SearchForEloquaContactAction extends AbstractAction<EloquaOrderExportProcessModel>
{

	/**
	 * The enum Transition.
	 */
	public enum Transition
	{

		/**
		 * if customer was found.
		 */
		FOUND,

		/**
		 * if customer was not found.
		 */
		NOT_FOUND,

		/**
		 * if some error occurred during search.
		 */
		FAILURE;

		/**
		 * Gets string values.
		 *
		 * @return the string values
		 */
		public static Set<String> getStringValues()
		{
			final Set<String> res = new HashSet<String>();

			for (final Transition transition : Transition.values())
			{
				res.add(transition.toString());
			}
			return res;
		}
	}

	private static final Logger LOG = LoggerFactory.getLogger(SearchForEloquaContactAction.class);

	@Resource
	private EloquaGateway eloquaGateway;

	@Override
	@Nonnull
	public String execute(@Nonnull final EloquaOrderExportProcessModel businessProcess) throws RetryLaterException, Exception
	{
		ServicesUtil.validateParameterNotNullStandardMessage("businessProcess", businessProcess);

		LOG.debug("Method params: EloquaOrderExportProcess [{}]", businessProcess.getCode());


		final OrderModel order = businessProcess.getOrder();
		final CustomerModel customerModel = (CustomerModel) order.getUser();

		Optional<ContactDto> optional;

		try
		{
			optional = eloquaGateway.searchForEloquaContact(customerModel, order.getSite());
		}
		catch (Exception e)
		{
			LOG.error("Searching for Eloqua contact was failed. Exception: {}.", e.getMessage());
			return logAndPrepareTransition(Transition.FAILURE);
		}

		if (optional.isPresent())
		{
			updateCustomer(customerModel, optional.get());
			return logAndPrepareTransition(Transition.FOUND);
		}
		else
		{
			LOG.debug("Eloqua Contact was not found for Customer [{}].", customerModel.getUid());
			return logAndPrepareTransition(Transition.NOT_FOUND);
		}
	}

	private void updateCustomer(final CustomerModel customerModel, final ContactDto contactDto)
	{
		customerModel.setEloquaId(contactDto.getId());
		getModelService().save(customerModel);
	}

	private String logAndPrepareTransition(final Transition transition)
	{
		LOG.debug("Transition to return: {}.", transition);
		return String.valueOf(transition);
	}

	@Override
	public Set<String> getTransitions()
	{
		return Transition.getStringValues();
	}
}
