/**
 * 
 */
package com.wiley.fulfilmentprocess.strategy.export;

import de.hybris.platform.core.model.order.OrderModel;

/**
 * The strategy to decide whether the given system should be exported.
 */
public interface WileyDecideExportSubprocessStartStrategy 
{
  boolean shouldStartExportSystem(OrderModel order);
}
