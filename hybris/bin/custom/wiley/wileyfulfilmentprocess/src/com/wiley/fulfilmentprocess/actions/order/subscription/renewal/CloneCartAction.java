package com.wiley.fulfilmentprocess.actions.order.subscription.renewal;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.task.RetryLaterException;

import java.util.Collections;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.model.WileySubscriptionModel;
import com.wiley.fulfilmentprocess.model.WileySubscriptionProcessModel;


/**
 * Created by Raman_Hancharou on 12/17/2015.
 */
public class CloneCartAction extends AbstractSimpleDecisionAction<WileySubscriptionProcessModel>
{

	private static final Logger LOG = LoggerFactory.getLogger(CloneCartAction.class);

	private CartService cartService;
	private TypeService typeService;
	private UserService userService;


	protected CartService getCartService()
	{
		return cartService;
	}

	@Required
	public void setCartService(final CartService cartService)
	{
		this.cartService = cartService;
	}

	protected TypeService getTypeService()
	{
		return typeService;
	}

	@Required
	public void setTypeService(final TypeService typeService)
	{
		this.typeService = typeService;
	}

	protected UserService getUserService()
	{
		return userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	@Override
	public Transition executeAction(final WileySubscriptionProcessModel process)
			throws RetryLaterException, Exception
	{
		ServicesUtil.validateParameterNotNull(process, "Process can not be null");
		try
		{
			WileySubscriptionModel wileySubscription = process.getWileySubscription();
			ServicesUtil.validateParameterNotNull(wileySubscription, "WileySubscription can not be null");
			AbstractOrderEntryModel orderEntry = wileySubscription.getOrderEntry();
			ServicesUtil.validateParameterNotNull(wileySubscription, "WileySubscription can not be null");
			AbstractOrderModel order = orderEntry.getOrder();
			getUserService().setCurrentUser(order.getUser());
			final CartModel clone = getCartService().clone(getTypeService().getComposedTypeForClass(CartModel.class),
					getTypeService().getComposedTypeForClass(CartEntryModel.class), order, makeCartCodeFromOrder(order));
			clone.setPaymentAddress(order.getPaymentAddress());
			clone.setPaymentInfo(order.getPaymentInfo());
			clone.setStatus(OrderStatus.CREATED);
			clone.setAllPromotionResults(Collections.EMPTY_SET);
			clone.setPaymentTransactions(Collections.EMPTY_LIST);
			clone.setCalculated(false);
			getModelService().save(clone);
			processParameterHelper.setProcessParameter(process, "cart", clone);
			LOG.info("Subscription renewal process {}: order {} has been successfully created", process.getCode(),
					order.getCode());
			return Transition.OK;
		}
		catch (Exception e)
		{
			LOG.error(String.format("Subscription renewal process %s has been failed. Reason %s", process.getCode(), e), e);
			return Transition.NOK;
		}
	}

	private String makeCartCodeFromOrder(final AbstractOrderModel order)
	{
		return order.getCode() + "clonedCart";
	}

}
