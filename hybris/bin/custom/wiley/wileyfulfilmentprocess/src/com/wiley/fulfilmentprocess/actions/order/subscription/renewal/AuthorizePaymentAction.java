package com.wiley.fulfilmentprocess.actions.order.subscription.renewal;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.processengine.model.BusinessProcessParameterModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wiley.core.payment.WileyPaymentService;
import com.wiley.fulfilmentprocess.model.WileySubscriptionProcessModel;

public class AuthorizePaymentAction extends AbstractSimpleDecisionAction<WileySubscriptionProcessModel>
{

	private static final Logger LOG = LoggerFactory.getLogger(AuthorizePaymentAction.class);
	private static final String CART = "cart";

	@Resource(name = "wileyWPGPaymentService")
	private WileyPaymentService wpgPaymentService;

	@Override
	public Transition executeAction(final WileySubscriptionProcessModel process)
	{
		ServicesUtil.validateParameterNotNull(process, "Process can not be null");
		final BusinessProcessParameterModel clonedCartParameter = processParameterHelper.getProcessParameterByName(process,
				CART);
		final CartModel cart = (CartModel) clonedCartParameter.getValue();
		this.modelService.refresh(cart);

		if (authorizePayment(cart))
		{
			LOG.info("Process {}: payment authorization has been successfully passed", process.getCode());
			processParameterHelper.setProcessParameter(process, CART, cart);
			return Transition.OK;
		}
		else
		{
			cart.setStatus(OrderStatus.PAYMENT_NOT_AUTHORIZED);
			getModelService().save(cart);
			LOG.error("Process {}: payment authorization has been failed", process.getCode());
			return Transition.NOK;
		}
	}

	private boolean authorizePayment(final CartModel cart)
	{
		PaymentTransactionEntryModel authorizeEntry = wpgPaymentService.authorize(cart);
		return TransactionStatus.ACCEPTED.name().equals(authorizeEntry.getTransactionStatus());
	}
}
