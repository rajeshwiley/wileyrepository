package com.wiley.fulfilmentprocess.actions.order;

import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class WileyOrderFailedNotificationAction extends AbstractProceduralAction<OrderProcessModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyOrderFailedNotificationAction.class);

	@Override
	public void executeAction(final OrderProcessModel process)
	{
		LOG.error("Process {} of order fulfilment has been failed", process.getCode());
	}

}
