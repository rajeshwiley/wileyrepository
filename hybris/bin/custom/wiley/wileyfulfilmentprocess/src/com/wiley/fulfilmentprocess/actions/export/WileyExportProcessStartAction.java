package com.wiley.fulfilmentprocess.actions.export;

import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.task.RetryLaterException;

import com.wiley.fulfilmentprocess.model.WileyExportProcessModel;


/**
 * An empty action that may contain some preparation for the export. Required to support the "repair process" functionality in
 * hMC.
 */
public class WileyExportProcessStartAction extends AbstractProceduralAction<WileyExportProcessModel>
{
	@Override
	public void executeAction(final WileyExportProcessModel process) throws RetryLaterException, Exception
	{
	}
}
