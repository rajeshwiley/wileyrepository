package com.wiley.fulfilmentprocess.actions.order.wileycom;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.task.RetryLaterException;

import javax.annotation.Resource;

import com.wiley.core.exceptions.ExternalSystemException;
import com.wiley.core.integration.esb.EsbOrderGateway;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WileycomExportToERPAction extends AbstractSimpleDecisionAction<OrderProcessModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(WileycomExportToERPAction.class);
	
	@Resource
	EsbOrderGateway esbOrderGateway;
	
	
	@Override
	public Transition executeAction(final OrderProcessModel orderProcessModel)
			throws RetryLaterException, Exception
	{
		Transition status;
		final OrderModel order = orderProcessModel.getOrder();
		try
		{
			esbOrderGateway.sendOrder(order);
			setOrderStatus(order, OrderStatus.EXPORTED_TO_ERP);
			status = Transition.OK;
		}
		catch (ExternalSystemException e)
		{
			LOG.warn("Order upload to fulfillment system failed with error: " + e.getMessage());
			setOrderStatus(order, OrderStatus.EXPORT_TO_ERP_FAILED);
			status = Transition.NOK;
		}
		return status;
	}
}
