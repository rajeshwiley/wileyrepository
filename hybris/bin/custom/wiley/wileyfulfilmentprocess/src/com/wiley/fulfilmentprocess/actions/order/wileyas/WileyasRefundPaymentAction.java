package com.wiley.fulfilmentprocess.actions.order.wileyas;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;

import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.refund.WileyRefundService;


public class WileyasRefundPaymentAction extends AbstractSimpleDecisionAction<OrderProcessModel>
{

	@Autowired
	private WileyRefundService wileyRefundService;

	@Override
	public Transition executeAction(final OrderProcessModel process)
	{
		final OrderModel order = process.getOrder();

		final boolean refundPaymentSucceeded = wileyRefundService.processRefundForModifiedOrder(order);

		if (refundPaymentSucceeded)
		{
			setOrderStatus(order, OrderStatus.PAYMENT_REFUNDED);
			return Transition.OK;
		}
		else
		{
			return failOrder(order);
		}
	}

	private Transition failOrder(final OrderModel order)
	{
		setOrderStatus(order, OrderStatus.PAYMENT_FAILED);
		return Transition.NOK;
	}
}
