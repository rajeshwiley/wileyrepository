package com.wiley.fulfilmentprocess.actions.eloqua;

import com.wiley.fulfilmentprocess.model.EloquaOrderExportProcessModel;
import com.wiley.core.integration.eloqua.EloquaGateway;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.task.RetryLaterException;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Action updates the Eloqua contact with hybris data.<br/>
 * Uses Spring Integration approach.
 *
 * @author Aliaksei_Zlobich
 */
public class UpdateEloquaContactAction extends AbstractEloquaContactAction
{


	private static final Logger LOG = LoggerFactory.getLogger(UpdateEloquaContactAction.class);

	@Resource
	private EloquaGateway eloquaGateway;

	@Override
	public Transition executeAction(final EloquaOrderExportProcessModel businessProcess)
			throws RetryLaterException, Exception
	{
		ServicesUtil.validateParameterNotNullStandardMessage("businessProcess", businessProcess);

		LOG.debug("Method params: EloquaOrderExportProcess [{}]", businessProcess.getCode());

		final OrderModel order = businessProcess.getOrder();
		final CustomerModel customer = (CustomerModel) order.getUser();

		Transition transition = Transition.NOK;

		try
		{
			eloquaGateway.updateEloquaContact(customer, getPaymentAddress(order, customer), order.getSubscribeToUpdates(),
					order.getSite());
			transition = Transition.OK;
		}
		catch (Exception e)
		{
			LOG.error("Exception occurred during creating of Eloqua contact. Error message: {}.", e.getMessage());
		}

		LOG.debug("Transition to return: {}.", transition);
		return transition;
	}
}
