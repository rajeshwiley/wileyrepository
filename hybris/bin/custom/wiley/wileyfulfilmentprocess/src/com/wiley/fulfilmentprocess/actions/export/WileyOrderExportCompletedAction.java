package com.wiley.fulfilmentprocess.actions.export;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.task.RetryLaterException;

import org.apache.log4j.Logger;

import com.wiley.fulfilmentprocess.model.WileyExportProcessModel;


/**
 * Checks whether the order was exported to all specified external systems.
 */
public class WileyOrderExportCompletedAction extends AbstractSimpleDecisionAction<OrderProcessModel>
{
	private static final Logger LOG = Logger.getLogger(WileyOrderExportCompletedAction.class);

	@Override
	public Transition executeAction(final OrderProcessModel process) throws RetryLaterException, Exception
	{
		LOG.info("Process: " + process.getCode() + " in step " + getClass());
		LOG.info("Process: " + process.getCode() + " is checking for  " + process.getExportProcesses().size()
				+ " subprocess results");

		for (final WileyExportProcessModel subprocess : process.getExportProcesses())
		{
			if (!subprocess.isDone())
			{
				LOG.info("Process: " + process.getCode() + " found incomplete subprocess " + subprocess.getCode());
				return Transition.NOK;
			}
			LOG.info("Process: " + process.getCode() + " found complete subprocess " + subprocess.getCode());
		}

		final OrderModel orderModel = process.getOrder();
		final BaseSiteModel siteModel = orderModel.getSite();
		LOG.info(String.format("%S: order %s was successfully processed", siteModel.getUid(), orderModel.getCode()));

		LOG.info("Process: " + process.getCode() + " found all subprocesses complete");
		setOrderStatus(process.getOrder(), OrderStatus.COMPLETED);
		return Transition.OK;
	}
}
