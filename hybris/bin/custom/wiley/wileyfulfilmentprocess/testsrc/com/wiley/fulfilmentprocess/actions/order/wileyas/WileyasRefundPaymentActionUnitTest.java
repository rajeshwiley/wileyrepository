package com.wiley.fulfilmentprocess.actions.order.wileyas;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.orderhistory.model.OrderHistoryEntryModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction.Transition;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.enums.PaymentModeEnum;
import com.wiley.core.refund.WileyRefundService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyasRefundPaymentActionUnitTest
{
	private static final String NOT_HYBRIS = "NotHybris";
	private static final String HYBRIS = "hybris";

	@Mock
	private WileyRefundService wileyRefundServiceMock;
	@Mock
	private ModelService modelServiceMock;
	@InjectMocks
	private WileyasRefundPaymentAction refundAction = new WileyasRefundPaymentAction();

	private OrderModel testOrder = new OrderModel();
	private OrderProcessModel testProcess = new OrderProcessModel();
	private OrderHistoryEntryModel testHistoryEntry = new OrderHistoryEntryModel();
	private List<OrderHistoryEntryModel> testHistoryEntries = new ArrayList<>(Arrays.asList(testHistoryEntry));

	@Before
	public void setUp()
	{
		testOrder.setHistoryEntries(testHistoryEntries);
		testOrder.setSourceSystem(HYBRIS);
		testOrder.setPaymentMode(new PaymentModeModel());
		testOrder.getPaymentMode().setCode(PaymentModeEnum.CARD.getCode());
		testProcess.setOrder(testOrder);
		when(wileyRefundServiceMock.processRefundForModifiedOrder(testOrder)).thenReturn(true);


	}

	@Test
	public void shouldExecuteRefundForModifiedOrder()
	{
		Transition result = refundAction.executeAction(testProcess);
		verify(wileyRefundServiceMock).processRefundForModifiedOrder(testOrder);
		assertEquals(OrderStatus.PAYMENT_REFUNDED, testOrder.getStatus());
		assertEquals(Transition.OK, result);
	}

	@Test
	public void shouldFailOrderForFailedRefund()
	{
		when(wileyRefundServiceMock.processRefundForModifiedOrder(testOrder)).thenReturn(false);
		Transition result = refundAction.executeAction(testProcess);
		assertEquals(Transition.NOK, result);
		assertEquals(OrderStatus.PAYMENT_FAILED, testOrder.getStatus());
	}
}
