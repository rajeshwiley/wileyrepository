package com.wiley.fulfilmentprocess.actions.export;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.enums.ExportProcessType;
import com.wiley.core.payment.transaction.PaymentTransactionService;
import com.wiley.fulfilmentprocess.services.export.WileyExportService;
import com.wiley.fulfilmentprocess.strategy.export.WileyDecideExportSubprocessStartStrategy;

import static com.wiley.integrations.constants.WileyintegrationsConstants.CDM_SYSTEM_CODE;
import static com.wiley.integrations.constants.WileyintegrationsConstants.EDI_SYSTEM_CODE;
import static com.wiley.integrations.constants.WileyintegrationsConstants.ELOQUA_SYSTEM_CODE;
import static com.wiley.integrations.constants.WileyintegrationsConstants.WILEY_CORE_SYSTEM_CODE;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyStartOrderExportActionTest
{
	@InjectMocks
	private WileyStartOrderExportAction testInstance = new WileyStartOrderExportAction();

	@Mock
	private OrderProcessModel mockOrderProcessModel;
	@Mock
	private PaymentTransactionService mockPaymentTransactionService;
	@Mock
	private WileyExportService mockWileyExportService;
	@Mock
	private BusinessProcessService mockBusinessProcessService;
	@Mock
	private ConfigurationService mockConfigurationService;
	@Mock
	private OrderModel mockOrderModel;
	@Mock
	private PaymentTransactionEntryModel mockPaymentTransactionEntryModel;
	@Mock
	private PaymentTransactionModel mockPaymentTransactionModel;
	@Mock(name = "cdmSubProcessStartStrategy")
	private WileyDecideExportSubprocessStartStrategy mockCDMSubProcessStartStrategy;
	@Mock(name = "ediSubProcessStartStrategy")
	private WileyDecideExportSubprocessStartStrategy mockEDISubProcessStartStrategy;
	@Mock(name = "coreSubProcessStartStrategy")
	private WileyDecideExportSubprocessStartStrategy mockCORESubProcessStartStrategy;


	@Before
	public void setUp()
	{

		given(mockOrderProcessModel.getOrder()).willReturn(mockOrderModel);
		given(mockPaymentTransactionService.getAcceptedTransactionEntry(mockOrderModel, PaymentTransactionType.CAPTURE))
				.willReturn(mockPaymentTransactionEntryModel);
		given(mockPaymentTransactionEntryModel.getPaymentTransaction()).willReturn(mockPaymentTransactionModel);
	}

	@Test
	public void shouldDoNothingIfOrderIsNull() throws Exception
	{
		given(mockOrderProcessModel.getOrder()).willReturn(null);

		testInstance.executeAction(mockOrderProcessModel);
		verify(mockWileyExportService, never()).startExportProcess(any(OrderModel.class),
				any(PaymentTransactionEntryModel.class), eq(ExportProcessType.SETTLE),
				any(String.class), any(OrderProcessModel.class));
	}

	@Test
	public void shouldStartExportProcessToAllSystems() throws Exception
	{

		given(Boolean.valueOf(mockCORESubProcessStartStrategy.shouldStartExportSystem(mockOrderModel)))
				.willReturn(Boolean.TRUE);
		given(Boolean.valueOf(mockCDMSubProcessStartStrategy.shouldStartExportSystem(mockOrderModel)))
				.willReturn(Boolean.TRUE);
		given(Boolean.valueOf(mockEDISubProcessStartStrategy.shouldStartExportSystem(mockOrderModel)))
				.willReturn(Boolean.TRUE);

		testInstance.executeAction(mockOrderProcessModel);

		verify(mockWileyExportService).startExportProcess(eq(mockOrderModel), eq(mockPaymentTransactionEntryModel),
				eq(ExportProcessType.SETTLE), eq(ELOQUA_SYSTEM_CODE), eq(mockOrderProcessModel));

		verify(mockWileyExportService).startExportProcess(eq(mockOrderModel), eq(mockPaymentTransactionEntryModel),
				eq(ExportProcessType.SETTLE), eq(EDI_SYSTEM_CODE), eq(mockOrderProcessModel));

		verify(mockWileyExportService).startExportProcess(eq(mockOrderModel), eq(mockPaymentTransactionEntryModel),
				eq(ExportProcessType.SETTLE), eq(WILEY_CORE_SYSTEM_CODE), eq(mockOrderProcessModel));

		verify(mockWileyExportService).startExportProcess(eq(mockOrderModel), eq(mockPaymentTransactionEntryModel),
				eq(ExportProcessType.SETTLE), eq(CDM_SYSTEM_CODE), eq(mockOrderProcessModel));
	}

	@Test
	public void shouldNotStartEdiExportForZeroTransactionOrders() throws Exception
	{
		given(Boolean.valueOf(mockCDMSubProcessStartStrategy.shouldStartExportSystem(mockOrderModel)))
				.willReturn(any(Boolean.class));
		given(Boolean.valueOf(mockEDISubProcessStartStrategy.shouldStartExportSystem(mockOrderModel)))
				.willReturn(Boolean.FALSE);

		testInstance.executeAction(mockOrderProcessModel);

		verify(mockWileyExportService, never()).startExportProcess(eq(mockOrderModel),
				isNull(PaymentTransactionEntryModel.class),
				eq(ExportProcessType.SETTLE), eq(EDI_SYSTEM_CODE), eq(mockOrderProcessModel));
	}

	@Test
	public void shouldNotStartPinActivation() throws Exception
	{
		given(Boolean.valueOf(mockCORESubProcessStartStrategy.shouldStartExportSystem(mockOrderModel)))
				.willReturn(Boolean.FALSE);
		given(Boolean.valueOf(mockCDMSubProcessStartStrategy.shouldStartExportSystem(mockOrderModel)))
				.willReturn(Boolean.TRUE);
		given(Boolean.valueOf(mockEDISubProcessStartStrategy.shouldStartExportSystem(mockOrderModel)))
				.willReturn(Boolean.TRUE);

		testInstance.executeAction(mockOrderProcessModel);

		verify(mockWileyExportService).startExportProcess(eq(mockOrderModel), eq(mockPaymentTransactionEntryModel),
				eq(ExportProcessType.SETTLE), eq(ELOQUA_SYSTEM_CODE), eq(mockOrderProcessModel));

		verify(mockWileyExportService).startExportProcess(eq(mockOrderModel), eq(mockPaymentTransactionEntryModel),
				eq(ExportProcessType.SETTLE), eq(EDI_SYSTEM_CODE), eq(mockOrderProcessModel));

		verify(mockWileyExportService, never()).startExportProcess(eq(mockOrderModel), eq(mockPaymentTransactionEntryModel),
				eq(ExportProcessType.SETTLE), eq(WILEY_CORE_SYSTEM_CODE), eq(mockOrderProcessModel));

		verify(mockWileyExportService).startExportProcess(eq(mockOrderModel), eq(mockPaymentTransactionEntryModel),
				eq(ExportProcessType.SETTLE), eq(CDM_SYSTEM_CODE), eq(mockOrderProcessModel));
	}

}
