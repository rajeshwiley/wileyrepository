package com.wiley.fulfilmentprocess.test;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.Registry;
import de.hybris.platform.processengine.definition.ProcessDefinitionFactory;
import de.hybris.platform.processengine.enums.ProcessState;
import de.hybris.platform.processengine.impl.DefaultBusinessProcessService;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.processengine.model.ProcessTaskModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.spring.ctx.ScopeTenantIgnoreDocReader;
import de.hybris.platform.task.RetryLaterException;
import de.hybris.platform.task.TaskModel;
import de.hybris.platform.task.impl.DefaultTaskService;
import de.hybris.platform.testframework.HybrisJUnit4Test;
import de.hybris.platform.testframework.TestUtils;
import de.hybris.platform.util.Utilities;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.io.ClassPathResource;

import com.wiley.fulfilmentprocess.test.actions.TestActionTemp;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.fail;

@IntegrationTest
public class AgsOrderProcessFlowTest extends HybrisJUnit4Test
{

	private static final Logger LOG = Logger.getLogger(AgsOrderProcessFlowTest.class);

	private static TaskServiceStub taskServiceStub;

	private static DefaultBusinessProcessService processService;
	private static ProcessDefinitionFactory definitonFactory;
	private static ModelService modelService;

	/**
	 * Prepare.
	 *
	 * @throws Exception
	 * 		the exception
	 */
	@BeforeClass
	public static void prepare() throws Exception //NOPMD
	{
		Registry.activateStandaloneMode();
		Utilities.setJUnitTenant();
		LOG.debug("Preparing...");



		final ApplicationContext appCtx = Registry.getApplicationContext();

		assertTrue("Application context of type " + appCtx.getClass() + " is not a subclass of "
				+ ConfigurableApplicationContext.class, appCtx instanceof ConfigurableApplicationContext);

		final ConfigurableApplicationContext applicationContext = (ConfigurableApplicationContext) appCtx;
		final ConfigurableListableBeanFactory beanFactory = applicationContext.getBeanFactory();
		assertTrue("Bean Factory of type " + beanFactory.getClass() + " is not of type " + BeanDefinitionRegistry.class,
				beanFactory instanceof BeanDefinitionRegistry);
		final XmlBeanDefinitionReader xmlReader = new XmlBeanDefinitionReader((BeanDefinitionRegistry) beanFactory);
		xmlReader.setDocumentReaderClass(ScopeTenantIgnoreDocReader.class);
		xmlReader.loadBeanDefinitions(new ClassPathResource(
				"/wileyfulfilmentprocess/test/wileyfulfilmentprocess-spring-test.xml"));
		xmlReader.loadBeanDefinitions(new ClassPathResource("/wileyfulfilmentprocess/test/process/ags-order-process-spring.xml"));

		modelService = (ModelService) getBean("modelService");
		processService = (DefaultBusinessProcessService) getBean("businessProcessService");
		definitonFactory = processService.getProcessDefinitionFactory();

		LOG.warn("Prepare Process Definition factory...");
		definitonFactory.add("classpath:/wileyfulfilmentprocess/process/ags-order-process.xml");
		LOG.warn("loaded 'ags-order-process':" + definitonFactory.getProcessDefinition("ags-order-process") + " in factory "
				+ definitonFactory);

		taskServiceStub = appCtx.getBean(TaskServiceStub.class);
		processService.setTaskService(taskServiceStub);
	}

	/**
	 * Sets actions.
	 */
	@Before
	public void setupActions()
	{
		setResultForAction("test.wileyCheckOrderAction", "OK");
		setThrowExceptionForAction("test.wileyCheckOrderAction", false);
		setResultForAction("test.takePaymentAction", "OK");
		setResultForAction("test.createWileySubscriptionAction", "OK");
		setResultForAction("test.wileyOrderFailedNotificationAction", "OK");
		setResultForAction("test.sendOrderPlacedNotificationAction", "TAKE_PAYMENT");
		setResultForAction("test.startOrderExportAction", "OK");
		setResultForAction("test.orderExportCompletedAction", "OK");
		setResultForAction("test.startOrderExportAction", "OK");
	}

	/**
	 * Remove process definitions.
	 */
	@AfterClass
	public static void removeProcessDefinitions()
	{
		LOG.debug("cleanup...");


		final ApplicationContext appCtx = Registry.getApplicationContext();

		assertTrue("Application context of type " + appCtx.getClass() + " is not a subclass of "
				+ ConfigurableApplicationContext.class, appCtx instanceof ConfigurableApplicationContext);

		final ConfigurableApplicationContext applicationContext = (ConfigurableApplicationContext) appCtx;
		final ConfigurableListableBeanFactory beanFactory = applicationContext.getBeanFactory();
		assertTrue("Bean Factory of type " + beanFactory.getClass() + " is not of type " + BeanDefinitionRegistry.class,
				beanFactory instanceof BeanDefinitionRegistry);
		final XmlBeanDefinitionReader xmlReader = new XmlBeanDefinitionReader((BeanDefinitionRegistry) beanFactory);
		xmlReader.loadBeanDefinitions(new ClassPathResource(
				"/wileyfulfilmentprocess/test/process/ags-order-process-spring-cleanup.xml"));

		processService.setTaskService(appCtx.getBean(DefaultTaskService.class));
		definitonFactory = null;
		processService = null;
	}

	/**
	 * Reset services.
	 */
	@After
	public void resetServices()
	{
		final List<TaskModel> tasks = taskServiceStub.cleanup();
		final StringBuffer msg = new StringBuffer();
		for (final TaskModel task : tasks)
		{
			final ProcessTaskModel processTask = (ProcessTaskModel) task;

			msg.append(processTask.getAction()).append(", ");
		}
	}


	/**
	 * Test exception in ags order process.
	 *
	 * @throws InterruptedException
	 * 		the interrupted exception
	 */
	@Test
	public void testExceptionInAgsOrderProcess() throws InterruptedException
	{
		setThrowExceptionForAction("test.wileyCheckOrderAction", true);
		final BusinessProcessModel process = createProcess("ags-order-process");
		TestUtils.disableFileAnalyzer(400);
		try
		{
			assertStep(process, "checkOrder");
		}
		finally
		{
			TestUtils.enableFileAnalyzer();
		}
		Thread.sleep(1000);

		modelService.refresh(process);
		assertEquals("Process state", ProcessState.ERROR, process.getProcessState());
	}

	/**
	 * Test ags order process when checkOrder action returns CHECKORDER_INVALID transition.
	 *
	 * @throws InterruptedException
	 * 		the interrupted exception
	 */
	@Test
	public void testAgsOrderProcessWhenCheckOrderInvalid() throws InterruptedException
	{
		setResultForAction("test.wileyCheckOrderAction", "NOK");
		final BusinessProcessModel process = createProcess("ags-order-process");

		assertStep(process, "checkOrder");
		assertStep(process, "orderFailedNotification");

		Thread.sleep(1000);

		modelService.refresh(process);
		assertEquals("Process state", ProcessState.FAILED, process.getProcessState());
	}

	/**
	 * Test ags order process when checkOrder action returns CHECKED_VALID transition.
	 *
	 * @throws InterruptedException
	 * 		the interrupted exception
	 */
	@Test
	public void testAgsOrderProcessWhenCheckOrderAuthorized() throws InterruptedException
	{
		setResultForAction("test.wileyCheckOrderAction", "OK");
		final BusinessProcessModel process = createProcess("ags-order-process");

		assertStep(process, "checkOrder");
		assertStep(process, "createSubscription");
		assertStep(process, "sendOrderPlacedNotification");
		assertStep(process, "waitForTakePayment");
		assertStep(process, "takePayment");
		assertStep(process, "exportOrder");
		assertStep(process, "waitForOrderExport");
		assertStep(process, "isOrderExportCompleted");
		Thread.sleep(1000);

		modelService.refresh(process);
		assertEquals("Process state", ProcessState.SUCCEEDED, process.getProcessState());
	}


	/**
	 * Test ags order process when createSubscription action returns NOK transition.
	 *
	 * @throws InterruptedException
	 * 		the interrupted exception
	 */
	@Test
	public void testAgsOrderProcessWhenCreateSubscriptionFailed() throws InterruptedException
	{
		setResultForAction("test.wileyCheckOrderAction", "OK");
		setResultForAction("test.createWileySubscriptionAction", "NOK");
		final BusinessProcessModel process = createProcess("ags-order-process");

		assertStep(process, "checkOrder");
		assertStep(process, "createSubscription");
		assertStep(process, "orderFailedNotification");
		Thread.sleep(1000);

		modelService.refresh(process);
		assertEquals("Process state", ProcessState.FAILED, process.getProcessState());
	}

	/**
	 * Create process business process model.
	 *
	 * @param processName
	 * 		the process name
	 * @return the business process model
	 */
	protected BusinessProcessModel createProcess(final String processName)
	{
		final String id = "Test" + (new Date()).getTime();
		final BusinessProcessModel process = processService.startProcess(id, processName);
		assertProcessState(process, ProcessState.RUNNING);
		modelService.save(process);
		return process;
	}

	/**
	 * Sets result for action.
	 *
	 * @param action
	 * 		the action
	 * @param result
	 * 		the result
	 */
	protected void setResultForAction(final String action, final String result)
	{
		final TestActionTemp tmp = (TestActionTemp) getBean(action);
		tmp.setResult(result);
	}

	/**
	 * Sets throw exception for action.
	 *
	 * @param action
	 * 		the action
	 * @param throwException
	 * 		the throw exception
	 */
	protected void setThrowExceptionForAction(final String action, final boolean throwException)
	{
		final TestActionTemp tmp = (TestActionTemp) getBean(action);
		tmp.setThrowException(throwException);
	}


	/**
	 * Assert step.
	 *
	 * @param process
	 * 		the process
	 * @param bean
	 * 		the bean
	 * @throws InterruptedException
	 * 		the interrupted exception
	 */
	protected void assertStep(final BusinessProcessModel process, final String bean) throws InterruptedException
	{
		LOG.info("assertStep action = " + bean);

		try
		{
			final ProcessTaskModel processTaskModel = taskServiceStub.runProcessTask(bean);

			if (processTaskModel == null)
			{
				final StringBuffer found = new StringBuffer();

				for (final TaskModel task : taskServiceStub.getTasks())
				{
					if (task instanceof ProcessTaskModel)
					{
						found.append(((ProcessTaskModel) task).getAction()).append("; ");
					}
				}

				assertNotNull("No task found for bean " + bean + ", action(s): " + found, processTaskModel);
			}


		}
		catch (final RetryLaterException e)
		{
			fail(e.toString());
		}

	}

	/**
	 * Gets bean.
	 *
	 * @param name
	 * 		the name
	 * @return the bean
	 */
	protected static Object getBean(final String name)
	{
		return Registry.getApplicationContext().getBean(name);
	}

	/**
	 * Assert process state.
	 *
	 * @param process
	 * 		the process
	 * @param state
	 * 		the state
	 */
	protected void assertProcessState(final BusinessProcessModel process, final ProcessState state)
	{
		modelService.refresh(process);
		assertEquals("Process state", state, process.getState());
	}
}
