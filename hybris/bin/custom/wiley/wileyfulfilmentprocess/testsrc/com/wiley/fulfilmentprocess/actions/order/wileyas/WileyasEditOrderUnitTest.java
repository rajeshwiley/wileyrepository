package com.wiley.fulfilmentprocess.actions.order.wileyas;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.paypal.hybris.constants.PaypalConstants;
import com.wiley.core.mpgs.constants.WileyMPGSConstants;
import com.wiley.core.mpgs.services.WileyMPGSPaymentProviderService;
import com.wiley.core.mpgs.services.WileyMPGSPaymentService;
import com.wiley.core.order.PaymentActonType;
import com.wiley.core.order.PendingPaymentActon;
import com.wiley.core.order.WileyCheckoutService;
import com.wiley.core.order.service.WileyOrderService;
import com.wiley.core.payment.WileyPaymentService;
import com.wiley.core.payment.impl.WileyPayPalPaymentService;
import com.wiley.core.wiley.order.WileyOrderPaymentService;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyasEditOrderUnitTest
{
	private static final String HYBRIS = "hybris";
	public static final String PAYMENT_PROVIDER = "MPGS";

	private final OrderProcessModel testProcess = new OrderProcessModel();
	private final OrderModel testOrder = new OrderModel();
	private final OrderEntryModel testEntry1 = new OrderEntryModel();
	private final OrderEntryModel testEntry2 = new OrderEntryModel();
	private final PaymentTransactionModel testCCTxn = new PaymentTransactionModel();
	private final PaymentTransactionModel testPaypalTxn = new PaymentTransactionModel();
	private final PendingPaymentActon testPendingAction = new PendingPaymentActon();

	@Mock
	private ModelService modelService;

	@Mock
	private WileyPayPalPaymentService wileyPayPalPaymentService;

	@Mock
	private WileyMPGSPaymentService wileyMPGSPaymentService;

	@Mock
	private WileyOrderService wileyOrderService;

	@Mock
	private WileyCheckoutService wileyCheckoutService;
	@Mock
	private WileyOrderPaymentService wileyOrderPaymentService;
	@Mock
	private WileyMPGSPaymentProviderService wileyMPGSPaymentProviderService;

	@InjectMocks
	private final WileyasTakePaymentAction testInstance = new WileyasTakePaymentAction();

	@Before
	public void setUp() throws Exception
	{
		testPendingAction.setAction(PaymentActonType.CAPTURE);
		when(wileyOrderPaymentService.getPendingPaymentActions(testOrder))
				.thenReturn(Collections.singletonList(testPendingAction));

		testEntry1.setTotalPrice(1.0D);
		testEntry2.setTotalPrice(1.0D);
		testOrder.setEntries(Arrays.asList(testEntry1, testEntry2));
		testOrder.getEntries().forEach(entry -> entry.setStatus(OrderStatus.CREATED));
		testOrder.setSourceSystem(HYBRIS);

		testProcess.setOrder(testOrder);
		testOrder.setStatus(OrderStatus.CREATED);

		testOrder.setPaymentTransactions(Arrays.asList(testCCTxn, testPaypalTxn));

		testCCTxn.setPaymentProvider(WileyMPGSConstants.MPGS_PAYMENT_PROVIDER);
		testCCTxn.setOrder(testOrder);

		testPaypalTxn.setPaymentProvider(PaypalConstants.PAYMENT_PROVIDER_NAME);
		testPaypalTxn.setOrder(testOrder);

		emulateCaptureResult(wileyPayPalPaymentService, testPaypalTxn, TransactionStatus.ACCEPTED);
		emulateCaptureResult(wileyMPGSPaymentService, testCCTxn, TransactionStatus.ACCEPTED);

		when(wileyOrderService.isOrderEligibleForPayment(any())).thenReturn(true);
		when(wileyOrderService.isNewOrder(any())).thenReturn(false);
		when(wileyCheckoutService.isNonZeroOrder(any())).thenReturn(true);
		when(wileyMPGSPaymentProviderService.isMPGSProviderGroup(PAYMENT_PROVIDER)).thenReturn(true);
	}

	@Test
	public void shouldUpdateStatusAfterPaidOrderEdit() throws Exception
	{
		//Given
		testPendingAction.setTransaction(testCCTxn);
		//When
		String result = testInstance.execute(testProcess);
		//Then
		verify(wileyMPGSPaymentService).capture(testCCTxn);
		assertEquals(OrderStatus.PAYMENT_CAPTURED, testOrder.getStatus());
		testOrder.getEntries().forEach(entry -> assertEquals(OrderStatus.PAYMENT_CAPTURED, entry.getStatus()));
		assertEquals(WileyasTakePaymentAction.Transition.OK.toString(), result);

		markEntriesUpdated(testOrder);
		testOrder.getEntries().forEach(entry -> assertEquals(OrderStatus.CREATED, entry.getStatus()));
	}

	@Test
	public void shouldRollbackStatusAfterPaidOrderEditSecondPayment() throws Exception
	{
		//Given
		testPendingAction.setTransaction(testCCTxn);
		//When
		String result = testInstance.execute(testProcess);
		//Then
		verify(wileyMPGSPaymentService).capture(testCCTxn);
		assertEquals(OrderStatus.PAYMENT_CAPTURED, testOrder.getStatus());
		testOrder.getEntries().forEach(entry -> assertEquals(OrderStatus.PAYMENT_CAPTURED, entry.getStatus()));
		assertEquals(WileyasTakePaymentAction.Transition.OK.toString(), result);

		markEntriesUpdated(testOrder);
		testOrder.getEntries().forEach(entry -> assertEquals(OrderStatus.CREATED, entry.getStatus()));

		//When
		String resultRepeat = testInstance.execute(testProcess);
		//Then
		assertEquals(OrderStatus.PAYMENT_CAPTURED, testOrder.getStatus());
		testOrder.getEntries().forEach(entry -> assertEquals(OrderStatus.PAYMENT_CAPTURED, entry.getStatus()));
		assertEquals(WileyasTakePaymentAction.Transition.OK.toString(), resultRepeat);
	}

	@Test
	public void shouldRollbackNonZeroStatusAfterPaidOrderEditSecondPayment() throws Exception
	{
		//Given
		when(wileyCheckoutService.isNonZeroOrder(any())).thenReturn(false);
		testPendingAction.setTransaction(testCCTxn);
		List<AbstractOrderEntryModel> entries = testOrder.getEntries();
		entries.forEach(r ->
		{
			r.setStatus(OrderStatus.CREATED);
			r.setTotalPrice(0.0D);
		});
		//When
		String result = testInstance.execute(testProcess);
		assertEquals(WileyasTakePaymentAction.Transition.OK.toString(), result);
		//Then
		assertEquals(OrderStatus.CREATED, testOrder.getStatus());
		testOrder.getEntries().forEach(entry -> assertEquals(OrderStatus.PAYMENT_WAIVED, entry.getStatus()));

		//Given
		when(wileyCheckoutService.isNonZeroOrder(any())).thenReturn(true);
		entries.forEach(r ->
		{
			r.setStatus(OrderStatus.CREATED);
			r.setTotalPrice(30.0D);
		});

		//When
		String resultRepeat = testInstance.execute(testProcess);
		verify(wileyMPGSPaymentService).capture(testCCTxn);
		//Then
		verify(wileyMPGSPaymentService).capture(testCCTxn);
		assertEquals(OrderStatus.PAYMENT_CAPTURED, testOrder.getStatus());
		testOrder.getEntries().forEach(entry -> assertEquals(OrderStatus.PAYMENT_CAPTURED, entry.getStatus()));
		assertEquals(WileyasTakePaymentAction.Transition.OK.toString(), resultRepeat);
	}

	private void markEntriesUpdated(final OrderModel orderModel)
	{
		for (AbstractOrderEntryModel entryModel : orderModel.getEntries())
		{
			if (!OrderStatus.CANCELLED.equals(entryModel.getStatus()))
			{
				entryModel.setStatus(OrderStatus.CREATED);
			}
		}
	}

	private void emulateCaptureResult(final WileyPaymentService paymentService,
			final PaymentTransactionModel txn, final TransactionStatus expectedStatus)
	{
		when(paymentService.capture(txn)).thenAnswer((Answer<Optional<PaymentTransactionEntryModel>>) invocationOnMock ->
		{
			PaymentTransactionEntryModel entry = new PaymentTransactionEntryModel();
			entry.setPaymentTransaction(txn);
			entry.setTransactionStatus(expectedStatus.name());
			return Optional.of(entry);
		});
	}
}