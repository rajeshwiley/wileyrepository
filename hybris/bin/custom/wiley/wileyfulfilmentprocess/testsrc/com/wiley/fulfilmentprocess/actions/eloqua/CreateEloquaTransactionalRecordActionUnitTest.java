package com.wiley.fulfilmentprocess.actions.eloqua;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.search.restriction.SearchRestrictionService;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.integration.eloqua.EloquaGateway;
import com.wiley.fulfilmentprocess.model.EloquaOrderExportProcessModel;


/**
 * Default unit test for {@link CreateEloquaTransactionalRecordAction}.
 *
 * @author Aliaksei_Zlobich
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CreateEloquaTransactionalRecordActionUnitTest
{

	@Mock
	private EloquaGateway eloquaGatewayMock;

	@Mock
	private SearchRestrictionService searchRestrictionService;

	@InjectMocks
	private CreateEloquaTransactionalRecordAction createEloquaTransactionalRecordAction;

	// Test data

	@Mock
	private EloquaOrderExportProcessModel eloquaOrderExportProcessModelMock;

	@Mock
	private OrderModel orderModelMock;

	@Mock
	private OrderEntryModel orderEntryModelMock;

	@Before
	public void setUp() throws Exception
	{
		when(eloquaOrderExportProcessModelMock.getOrder()).thenReturn(orderModelMock);
		when(orderModelMock.getEntries()).thenReturn(Arrays.asList(orderEntryModelMock));
	}

	@Test
	public void shouldReturnOK() throws Exception
	{
		// Given
		// no changes in test data

		// When
		final AbstractSimpleDecisionAction.Transition transition = createEloquaTransactionalRecordAction.executeAction(
				eloquaOrderExportProcessModelMock);

		// Then
		assertEquals("Returned transition should be equal expected.", AbstractSimpleDecisionAction.Transition.OK, transition);
		verify(eloquaGatewayMock).createEloquaTransactionalRecord(eq(orderEntryModelMock));
	}

	@Test
	public void shouldReturnNORIfSomeErrorOccurredDuringOfCreationOfTransactionalRecord() throws Exception
	{
		// Given
		when(eloquaGatewayMock.createEloquaTransactionalRecord(any(OrderEntryModel.class))).thenThrow(Exception.class);

		// When

		final AbstractSimpleDecisionAction.Transition transition = createEloquaTransactionalRecordAction.executeAction(
				eloquaOrderExportProcessModelMock);

		// Then
		assertEquals("Returned transition should be equal expected.", AbstractSimpleDecisionAction.Transition.NOK, transition);
		verify(eloquaGatewayMock).createEloquaTransactionalRecord(eq(orderEntryModelMock));
	}
}