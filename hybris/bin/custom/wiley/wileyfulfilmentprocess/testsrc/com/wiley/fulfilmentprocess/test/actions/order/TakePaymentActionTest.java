package com.wiley.fulfilmentprocess.test.actions.order;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.mpgs.services.WileyMPGSPaymentProviderService;
import com.wiley.core.order.service.WileyOrderService;
import com.wiley.core.payment.WileyPaymentService;
import com.wiley.core.payment.impl.WileyPayPalPaymentService;
import com.wiley.core.payment.impl.WileyWPGPaymentServiceImpl;
import com.wiley.fulfilmentprocess.actions.order.TakePaymentAction;

import static de.hybris.platform.processengine.action.AbstractSimpleDecisionAction.Transition.NOK;
import static de.hybris.platform.processengine.action.AbstractSimpleDecisionAction.Transition.OK;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TakePaymentActionTest
{
	private static final String PAYMENT_PROVIDER_CARD = "CARD";
	private static final String PAYMENT_PROVIDER_PAY_PAL = "PayPal";

	@Mock
	private WileyWPGPaymentServiceImpl wpgPaymentService;
	@Mock
	private WileyPayPalPaymentService payPalPaymentService;
	@Mock
	private WileyMPGSPaymentProviderService wileyMPGSPaymentProviderService;


	@Mock
	private ModelService mockModelService;

	@Mock
	private OrderProcessModel mockOrderProcess;

	@Mock
	private OrderModel mockOrder;
	@Mock
	private WileyOrderService wileyOrderService;

	@InjectMocks
	private TakePaymentAction underTest = new TakePaymentAction();
	@Mock
	private PaymentTransactionModel mockCardTransactionTwo;
	@Mock
	private PaymentTransactionModel mockCardTransactionOne;
	@Mock
	private PaymentTransactionModel mockPayPalTransaction;

	@Mock
	private PaymentTransactionEntryModel mockCardCaptureEntryOne;
	@Mock
	private PaymentTransactionEntryModel mockCardCaptureEntryTwo;
	@Mock
	private PaymentTransactionEntryModel mockPayPalCaptureEntry;


	@Before
	public void setup()
	{
		when(mockOrderProcess.getOrder()).thenReturn(mockOrder);
		when(mockOrder.getPaymentTransactions()).thenReturn(
				Arrays.asList(mockCardTransactionOne, mockCardTransactionTwo, mockPayPalTransaction));
		when(wileyOrderService.isOrderEligibleForPayment(any())).thenReturn(true);

		initPaymentTransaction(mockCardTransactionOne, mockCardCaptureEntryOne, PAYMENT_PROVIDER_CARD);
		initPaymentTransaction(mockCardTransactionTwo, mockCardCaptureEntryTwo, PAYMENT_PROVIDER_CARD);
		initPaymentTransaction(mockPayPalTransaction, mockPayPalCaptureEntry, PAYMENT_PROVIDER_PAY_PAL);
	}

	private void initPaymentTransaction(final PaymentTransactionModel transaction,
			final PaymentTransactionEntryModel captureEntry, final String paymentProvider)
	{
		when(transaction.getOrder()).thenReturn(mockOrder);
		when(transaction.getPaymentProvider()).thenReturn(paymentProvider);

		when(captureEntry.getPaymentTransaction()).thenReturn(transaction);
		when(captureEntry.getTransactionStatus()).thenReturn(TransactionStatus.ACCEPTED.name());

		when(getPaymentService(paymentProvider).capture(transaction)).thenReturn(Optional.of(captureEntry));
	}

	private WileyPaymentService getPaymentService(final String paymentProvider)
	{
		WileyPaymentService paymentService = wpgPaymentService;
		if (PAYMENT_PROVIDER_PAY_PAL.equals(paymentProvider))
		{
			paymentService = payPalPaymentService;
		}
		return paymentService;
	}

	@Test
	public void shouldReturnOkOnAllSuccessfulCaptures()
	{
		invokeActionAndVerifyResult(OK, OrderStatus.PAYMENT_CAPTURED);
	}

	@Test
	public void shouldReturnNokOnFirstUnsuccessfulCapture()
	{
		when(mockCardCaptureEntryOne.getTransactionStatus()).thenReturn(TransactionStatus.ERROR.name());

		invokeActionAndVerifyResult(NOK, OrderStatus.PAYMENT_NOT_CAPTURED);

	}

	@Test
	public void shouldReturnNokOnSecondUnsuccessfulCapture()
	{
		when(mockCardCaptureEntryOne.getTransactionStatus()).thenReturn(TransactionStatus.ERROR.name());

		invokeActionAndVerifyResult(NOK, OrderStatus.PAYMENT_NOT_CAPTURED);

	}

	@Test
	public void shouldReturnNokOnPayPalUnsuccessfulCapture()
	{
		when(mockPayPalCaptureEntry.getTransactionStatus()).thenReturn(TransactionStatus.ERROR.name());

		invokeActionAndVerifyResult(NOK, OrderStatus.PAYMENT_NOT_CAPTURED);

	}

	@Test
	public void shouldReturnNokOnNoCaptureAttempts()
	{
		when(wpgPaymentService.capture(mockCardTransactionTwo)).thenReturn(Optional.empty());
		when(wpgPaymentService.capture(mockCardTransactionOne)).thenReturn(Optional.empty());
		when(payPalPaymentService.capture(mockPayPalTransaction)).thenReturn(Optional.empty());

		invokeActionAndVerifyResult(NOK, OrderStatus.PAYMENT_NOT_CAPTURED);

	}

	@Test
	public void shouldIgnoreNotAttemptedCapture()
	{
		when(wpgPaymentService.capture(mockCardTransactionOne)).thenReturn(Optional.empty());

		invokeActionAndVerifyResult(OK, OrderStatus.PAYMENT_CAPTURED);

	}



	private void invokeActionAndVerifyResult(final AbstractSimpleDecisionAction.Transition expectedTransition,
			final OrderStatus status)
	{
		AbstractSimpleDecisionAction.Transition transition = underTest.executeWileyTakePaymentAction(mockOrderProcess);
		assertEquals(expectedTransition, transition);

		verify(mockOrder).setStatus(status);
		verify(mockModelService).save(mockOrder);
	}

}
