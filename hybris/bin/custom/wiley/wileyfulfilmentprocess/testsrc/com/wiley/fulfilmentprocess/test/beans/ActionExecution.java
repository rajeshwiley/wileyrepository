/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.fulfilmentprocess.test.beans;

import de.hybris.platform.processengine.action.AbstractAction;
import de.hybris.platform.processengine.model.BusinessProcessModel;


/**
 * The type Action execution.
 */
public class ActionExecution
{
	private final BusinessProcessModel process;
	private final AbstractAction action;

	/**
	 * Instantiates a new Action execution.
	 *
	 * @param process
	 * 		the process
	 * @param action
	 * 		the action
	 */
	public ActionExecution(final BusinessProcessModel process, final AbstractAction action)
	{
		this.process = process;
		this.action = action;
	}

	/**
	 * Gets action.
	 *
	 * @return the action
	 */
	public AbstractAction getAction()
	{
		return action;
	}

	/**
	 * Gets process.
	 *
	 * @return the process
	 */
	public BusinessProcessModel getProcess()
	{
		return process;
	}

}
