package com.wiley.integration.eloqua.converters.populator;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;



import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.doReturn;

import com.wiley.core.integration.eloqua.dto.ContactDto;
import com.wiley.core.integration.eloqua.dto.FieldValueDto;
import com.wiley.integrations.eloqua.converters.populator.SubscriptionStatusContactPopulator;



@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SubscriptionStatusContactPopulatorTest
{
	private static final int SUBSCRIPTION_STATUS_FIELD_ID = 100311;
	
	private static final String SUBSCRIPTION_AGREE = "Explicit Consent";
	private static final String SUBSCRIPTION_DISAGREE = "No Response";
	
	private SubscriptionStatusContactPopulator subscriptionStatusContactPopulator;
	
	@Mock
	private ContactDto contactDtoMock;

	private List<FieldValueDto> fieldValues;
	
	@Before
	public void setUp()
	{
		subscriptionStatusContactPopulator = new SubscriptionStatusContactPopulator();

		fieldValues = new ArrayList<FieldValueDto>();
		doReturn(fieldValues).when(contactDtoMock).getFieldValues();

		ReflectionTestUtils.setField(subscriptionStatusContactPopulator, "subscriptionStatusFieldId",
				SUBSCRIPTION_STATUS_FIELD_ID);
	}
	
	@Test
	public void shouldPopulateAgreedToSubscription()
	{
		// Given
		Boolean subscribeToUpdates = Boolean.TRUE;
	
		// Testing
		subscriptionStatusContactPopulator.populate(subscribeToUpdates, contactDtoMock);
	
		// Verify
		final FieldValueDto fieldValueDto = fieldValues.get(0);
		assertNotNull(fieldValueDto);
		assertTrue(SUBSCRIPTION_STATUS_FIELD_ID == fieldValueDto.getId());
		assertEquals(SUBSCRIPTION_AGREE, fieldValueDto.getValue());
	}
	
	@Test
	public void shouldPopulateDisagreedToSubscription()
	{
		// Given
		Boolean subscribeToUpdates = Boolean.FALSE;

		// Testing
		subscriptionStatusContactPopulator.populate(subscribeToUpdates, contactDtoMock);

		// Verify
		final FieldValueDto fieldValueDto = fieldValues.get(0);
		assertNotNull(fieldValueDto);
		assertTrue(SUBSCRIPTION_STATUS_FIELD_ID == fieldValueDto.getId());
		assertEquals(SUBSCRIPTION_DISAGREE, fieldValueDto.getValue());
	}
}