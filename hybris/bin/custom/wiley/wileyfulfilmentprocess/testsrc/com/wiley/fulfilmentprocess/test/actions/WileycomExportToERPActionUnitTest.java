package com.wiley.fulfilmentprocess.test.actions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.exceptions.ExternalSystemException;
import com.wiley.core.integration.esb.EsbOrderGateway;
import com.wiley.fulfilmentprocess.actions.order.wileycom.WileycomExportToERPAction;

import static de.hybris.platform.processengine.action.AbstractSimpleDecisionAction.Transition;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileycomExportToERPActionUnitTest
{
	@Mock
	private OrderProcessModel orderProcessModelMock;

	@Mock
	private OrderModel orderMock;
	
	@Mock
	private ModelService modelService;

	@Mock
	private EsbOrderGateway esbOrderGatewayMock;

	@InjectMocks
	private WileycomExportToERPAction testAction;
	

	@Before
	public void setUp()
	{
		doReturn(orderMock).when(orderProcessModelMock).getOrder();
	}
	
	
	@Test
	public void shouldCheckValidOrder()
	{
		
		// Given
		Transition status = null;
	
		// When
		try
		{
			status = testAction.executeAction(orderProcessModelMock);
		}
		catch (Exception ex)
		{
			Assert.fail("Success flow without exception " + ex.getClass() + " expected");
		}

		// Then
		Assert.assertEquals(Transition.OK, status);
		checkStatusChange(OrderStatus.EXPORTED_TO_ERP);
	}

	@Test
	public void shouldCatchExternalSystemException()
	{
		// Given
		Transition status = null;
		doThrow(ExternalSystemException.class).when(esbOrderGatewayMock).sendOrder(orderMock);

		// When
		try
		{
			status = testAction.executeAction(orderProcessModelMock);
		}
		catch (Exception ex)
		{
			Assert.fail("The flow without exception " + ex.getClass() + " expected");
		}

		// Then
		Assert.assertEquals(Transition.NOK, status);
		checkStatusChange(OrderStatus.EXPORT_TO_ERP_FAILED);
	}
	
	private void checkStatusChange(final OrderStatus expectedStatus)
	{
		Mockito.verify(orderMock).setStatus(Matchers.eq(expectedStatus));
		Mockito.verify(modelService).save(orderMock);
	}
}
