package com.wiley.fulfilmentprocess.services.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.enums.ExportProcessType;
import com.wiley.fulfilmentprocess.model.WileyExportProcessModel;
import com.wiley.fulfilmentprocess.services.export.impl.WileyExportServiceImpl;


/**
 * Default unit test {@link WileyExportServiceImpl}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyExportServiceImplUnitTest {

  @Mock
  private BusinessProcessService businessProcessServiceMock;

  @Mock
  private ModelService modelServiceMock;

  @InjectMocks
  private WileyExportServiceImpl wileyExportServiceImpl;

  // Test data

  private static final String EDI = "edi";
  private static final String ELOQUA = "eloqua";
  private static final String CDM = "cdm";
  private static final String WILEY_CORE = "wileycore";

  private static final String EDI_SUBPROCESS = "ediSubprocess";
  private static final String ELOQUA_SUBPROCESS = "eloquaSubprocess";
  private static final String CDM_SUBPROCESS = "cdmSubprocess";
  private static final String WILEY_CORE_SUBPROCESS = "wileycoreSubprocess";

  @Mock
  private OrderModel orderModelMock;

  @Mock
  private OrderProcessModel orderProcessModelMock;

  @Mock
  private WileyExportProcessModel wileyEdiExportProcessModelMock;

  @Mock
  private WileyExportProcessModel wileyEloquaExportProcessModelMock;

  @Mock
  private WileyExportProcessModel wileyCDMExportProcessModelMock;

  @Mock
  private WileyExportProcessModel wileyCoreExportProcessModelMock;

  @Before
  public void setUp() throws Exception {
    initSystemToProcessDefinitionNameMap();

    // init configuration service
    when(businessProcessServiceMock.createProcess(anyString(), eq(EDI_SUBPROCESS))).thenReturn(wileyEdiExportProcessModelMock);
    when(businessProcessServiceMock.createProcess(anyString(), eq(ELOQUA_SUBPROCESS)))
        .thenReturn(wileyEloquaExportProcessModelMock);
    when(businessProcessServiceMock.createProcess(anyString(), eq(CDM_SUBPROCESS))).thenReturn(wileyCDMExportProcessModelMock);
    when(businessProcessServiceMock.createProcess(anyString(), eq(WILEY_CORE_SUBPROCESS)))
        .thenReturn(wileyCoreExportProcessModelMock);
  }

  private void initSystemToProcessDefinitionNameMap() {
    final Map<String, String> systemToProcessDefinitionNameMap = new HashMap<>();
    systemToProcessDefinitionNameMap.put(EDI, EDI_SUBPROCESS);
    systemToProcessDefinitionNameMap.put(ELOQUA, ELOQUA_SUBPROCESS);
    systemToProcessDefinitionNameMap.put(CDM, CDM_SUBPROCESS);
    systemToProcessDefinitionNameMap.put(WILEY_CORE, WILEY_CORE_SUBPROCESS);
    wileyExportServiceImpl.setSystemToProcessDefinitionNameMap(systemToProcessDefinitionNameMap);
  }

  // Tests for checking creation of the processes
  @Test
  public void shouldCreateEDIExportProcessWhenProcessIsEDI() {
    wileyExportServiceImpl.startExportProcess(orderModelMock, null, ExportProcessType.SETTLE, EDI, orderProcessModelMock);

    verify(businessProcessServiceMock).createProcess(anyString(), eq(EDI_SUBPROCESS));
  }

  @Test
  public void shouldCreateEloquaExportProcessWhenProcessIsEloqua() {
    wileyExportServiceImpl.startExportProcess(orderModelMock, null, ExportProcessType.SETTLE, ELOQUA, orderProcessModelMock);

    verify(businessProcessServiceMock).createProcess(anyString(), eq(ELOQUA_SUBPROCESS));
  }

  @Test
  public void shouldCreateCDMExportProcessWhenProcessIsCDM() {
    wileyExportServiceImpl.startExportProcess(orderModelMock, null, ExportProcessType.SETTLE, CDM, orderProcessModelMock);

    verify(businessProcessServiceMock).createProcess(anyString(), eq(CDM_SUBPROCESS));
  }

  @Test
  public void shouldCreateWileyCoreExportProcessWhenProcessIsEDI() {
    wileyExportServiceImpl.startExportProcess(orderModelMock, null, ExportProcessType.SETTLE, WILEY_CORE, orderProcessModelMock);

    verify(businessProcessServiceMock).createProcess(anyString(), eq(WILEY_CORE_SUBPROCESS));
  }

  // Tests for checking the saving of the processes
  @Test
  public void shouldSaveEDIProcessWhenProcessIsEDI() {
    wileyExportServiceImpl.startExportProcess(orderModelMock, null, ExportProcessType.SETTLE, EDI, orderProcessModelMock);

    verify(modelServiceMock).save(same(wileyEdiExportProcessModelMock));
  }

  @Test
  public void shouldSaveEloquaProcessWhenProcessIsEloqua() {
    wileyExportServiceImpl.startExportProcess(orderModelMock, null, ExportProcessType.SETTLE, ELOQUA, orderProcessModelMock);

    verify(modelServiceMock).save(same(wileyEloquaExportProcessModelMock));
  }

  @Test
  public void shouldSaveCDMProcessWhenProcessIsCDM() {
    wileyExportServiceImpl.startExportProcess(orderModelMock, null, ExportProcessType.SETTLE, CDM, orderProcessModelMock);

    verify(modelServiceMock).save(same(wileyCDMExportProcessModelMock));
  }

  @Test
  public void shouldSaveWileyCoreProcessWhenProcessIsWileyCore() {
    wileyExportServiceImpl.startExportProcess(orderModelMock, null, ExportProcessType.SETTLE, WILEY_CORE, orderProcessModelMock);

    verify(modelServiceMock).save(same(wileyCoreExportProcessModelMock));
  }

  // Tests to check only the EDI process is started and no other process is started
  @Test
  public void shouldStartEDIExportProcessWhenProcessIsEDI() {
    wileyExportServiceImpl.startExportProcess(orderModelMock, null, ExportProcessType.SETTLE, EDI, orderProcessModelMock);

    verify(businessProcessServiceMock).startProcess(same(wileyEdiExportProcessModelMock));
  }

  @Test
  public void shouldNotStartEloquaExportProcessWhenProcessIsEDI() {
    wileyExportServiceImpl.startExportProcess(orderModelMock, null, ExportProcessType.SETTLE, EDI, orderProcessModelMock);

    verify(businessProcessServiceMock, never()).startProcess(same(wileyEloquaExportProcessModelMock));
  }

  @Test
  public void shouldNotStartCDMExportProcessWhenProcessIsEDI() {
    wileyExportServiceImpl.startExportProcess(orderModelMock, null, ExportProcessType.SETTLE, EDI, orderProcessModelMock);

    verify(businessProcessServiceMock, never()).startProcess(same(wileyCDMExportProcessModelMock));
  }

  @Test
  public void shouldNotStartWileyCoreExportProcessWhenProcessIsEDI() {
    wileyExportServiceImpl.startExportProcess(orderModelMock, null, ExportProcessType.SETTLE, EDI, orderProcessModelMock);

    verify(businessProcessServiceMock, never()).startProcess(same(wileyCoreExportProcessModelMock));
  }

  // Tests to check only the Eloqua process is started and no other process is started
  @Test
  public void shouldStartEloquaExportProcessWhenProcessIsEloqua() {
    wileyExportServiceImpl.startExportProcess(orderModelMock, null, ExportProcessType.SETTLE, ELOQUA, orderProcessModelMock);

    verify(businessProcessServiceMock).startProcess(same(wileyEloquaExportProcessModelMock));
  }

  @Test
  public void shouldNotStartEDIExportProcessWhenProcessIsEloqua() {
    wileyExportServiceImpl.startExportProcess(orderModelMock, null, ExportProcessType.SETTLE, ELOQUA, orderProcessModelMock);

    verify(businessProcessServiceMock, never()).startProcess(same(wileyEdiExportProcessModelMock));
  }

  @Test
  public void shouldNotStartCDMExportProcessWhenProcessIsEloqua() {
    wileyExportServiceImpl.startExportProcess(orderModelMock, null, ExportProcessType.SETTLE, ELOQUA, orderProcessModelMock);

    verify(businessProcessServiceMock, never()).startProcess(same(wileyCDMExportProcessModelMock));
  }

  @Test
  public void shouldNotStartWileyCoreExportProcessWhenProcessIsEloqua() {
    wileyExportServiceImpl.startExportProcess(orderModelMock, null, ExportProcessType.SETTLE, ELOQUA, orderProcessModelMock);

    verify(businessProcessServiceMock, never()).startProcess(same(wileyCoreExportProcessModelMock));
  }

  //Tests to check only the CDM process is started and no other process is started
  @Test
  public void shouldStartCDMExportProcessWhenProcessIsCDM() {
    wileyExportServiceImpl.startExportProcess(orderModelMock, null, ExportProcessType.SETTLE, CDM, orderProcessModelMock);

    verify(businessProcessServiceMock).startProcess(same(wileyCDMExportProcessModelMock));
  }

  @Test
  public void shouldNotStartEDIExportProcessWhenProcessIsCDM() {
    wileyExportServiceImpl.startExportProcess(orderModelMock, null, ExportProcessType.SETTLE, CDM, orderProcessModelMock);

    verify(businessProcessServiceMock, never()).startProcess(same(wileyEdiExportProcessModelMock));
  }

  @Test
  public void shouldNotStartEloquaExportProcessWhenProcessIsCDM() {
    wileyExportServiceImpl.startExportProcess(orderModelMock, null, ExportProcessType.SETTLE, CDM, orderProcessModelMock);

    verify(businessProcessServiceMock, never()).startProcess(same(wileyEloquaExportProcessModelMock));
  }

  @Test
  public void shouldNotStartWileyCoreExportProcessWhenProcessIsCDM() {
    wileyExportServiceImpl.startExportProcess(orderModelMock, null, ExportProcessType.SETTLE, CDM, orderProcessModelMock);

    verify(businessProcessServiceMock, never()).startProcess(same(wileyCoreExportProcessModelMock));
  }

  //Tests to check only the WileyCore process is started and no other process is started
  @Test
  public void shouldStartWileyCoreExportProcessWhenProcessIsWileyCore() {
    wileyExportServiceImpl.startExportProcess(orderModelMock, null, ExportProcessType.SETTLE, WILEY_CORE, orderProcessModelMock);

    verify(businessProcessServiceMock).startProcess(same(wileyCoreExportProcessModelMock));
  }

  @Test
  public void shouldNotStartEDIExportProcessWhenProcessIsWileyCore() {
    wileyExportServiceImpl.startExportProcess(orderModelMock, null, ExportProcessType.SETTLE, WILEY_CORE, orderProcessModelMock);

    verify(businessProcessServiceMock, never()).startProcess(same(wileyEdiExportProcessModelMock));
  }

  @Test
  public void shouldNotStartEloquaExportProcessWhenProcessIsWileyCore() {
    wileyExportServiceImpl.startExportProcess(orderModelMock, null, ExportProcessType.SETTLE, WILEY_CORE, orderProcessModelMock);

    verify(businessProcessServiceMock, never()).startProcess(same(wileyEloquaExportProcessModelMock));
  }

  @Test
  public void shouldNotStartCDMExportProcessWhenProcessIsWileyCore() {
    wileyExportServiceImpl.startExportProcess(orderModelMock, null, ExportProcessType.SETTLE, WILEY_CORE, orderProcessModelMock);

    verify(businessProcessServiceMock, never()).startProcess(same(wileyCDMExportProcessModelMock));
  }

  // Tests to check no more interactions of export process after starting the process
  @Test
  public void shouldHaveNoMoreInteractionsAfterEDIProcessIsStarted() {
    wileyExportServiceImpl.startExportProcess(orderModelMock, null, ExportProcessType.SETTLE, EDI, orderProcessModelMock);

    verify(businessProcessServiceMock).createProcess(anyString(), eq(EDI_SUBPROCESS));
    verify(modelServiceMock).save(same(wileyEdiExportProcessModelMock));
    verify(businessProcessServiceMock).startProcess(same(wileyEdiExportProcessModelMock));

    verifyNoMoreInteractions(businessProcessServiceMock);
  }

  @Test
  public void shouldHaveNoMoreInteractionsAfterEloquaProcessIsStarted() {
    wileyExportServiceImpl.startExportProcess(orderModelMock, null, ExportProcessType.SETTLE, ELOQUA, orderProcessModelMock);

    verify(businessProcessServiceMock).createProcess(anyString(), eq(ELOQUA_SUBPROCESS));
    verify(modelServiceMock).save(same(wileyEloquaExportProcessModelMock));
    verify(businessProcessServiceMock).startProcess(same(wileyEloquaExportProcessModelMock));

    verifyNoMoreInteractions(businessProcessServiceMock);
  }

  @Test
  public void shouldHaveNoMoreInteractionsAfterCDMProcessIsStarted() {
    wileyExportServiceImpl.startExportProcess(orderModelMock, null, ExportProcessType.SETTLE, CDM, orderProcessModelMock);

    verify(businessProcessServiceMock).createProcess(anyString(), eq(CDM_SUBPROCESS));
    verify(modelServiceMock).save(same(wileyCDMExportProcessModelMock));
    verify(businessProcessServiceMock).startProcess(same(wileyCDMExportProcessModelMock));

    verifyNoMoreInteractions(businessProcessServiceMock);
  }

  @Test
  public void shouldHaveNoMoreInteractionsAfterWileyCoreProcessIsStarted() {
    wileyExportServiceImpl.startExportProcess(orderModelMock, null, ExportProcessType.SETTLE, WILEY_CORE, orderProcessModelMock);

    verify(businessProcessServiceMock).createProcess(anyString(), eq(WILEY_CORE_SUBPROCESS));
    verify(modelServiceMock).save(same(wileyCoreExportProcessModelMock));
    verify(businessProcessServiceMock).startProcess(same(wileyCoreExportProcessModelMock));

    verifyNoMoreInteractions(businessProcessServiceMock);
  }

}
