package com.wiley.fulfilmentprocess.test.actions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.CoreAlgorithms;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.order.payment.InvoicePaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyDouble;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Collections;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.Lists;
import com.wiley.core.enums.ProductEditionFormat;
import com.wiley.core.model.WileyOrderProcessModel;
import com.wiley.core.order.service.WileyOrderService;
import com.wiley.core.payment.transaction.PaymentTransactionService;
import com.wiley.core.pin.service.PinIntegrationService;
import com.wiley.core.pin.service.PinService;
import com.wiley.fulfilmentprocess.actions.order.WileyCheckOrderAction;


@UnitTest
@RunWith(MockitoJUnitRunner.class)

public class WileyCheckOrderActionUnitTest
{
	private static final Double ORDER_AMOUNT = 60.0;
	private static final Double ORDER_TAXES = 6.66;

	@Mock
	private WileyOrderProcessModel processsMock;

	@Mock
	private OrderModel orderMock;

	@Mock
	private CurrencyModel currencyMock;

	@Mock
	private AbstractOrderEntryModel orderEntry;

	@Mock
	private PaymentTransactionModel transactionMock;

	@Mock
	private PaymentTransactionEntryModel transEntryMock;

	@Mock
	private PaymentTransactionModel transactionMock2;

	@Mock
	private PaymentTransactionEntryModel transEntryMock2;

	@Mock
	private ModelService modelService;

	@Mock
	private CommonI18NService commonI18NService;

	@Mock
	private PinService pinService;

	@Mock
	private PaymentInfoModel pinfoMock;

	@Mock
	private InvoicePaymentInfoModel invoicePaymentInfo;

	@Mock
	private PaymentTransactionService paymentTransactionService;

	@Mock
	private ProductModel productMock;

	@Mock
	private DeliveryModeModel deliveryModeMock;

	@Mock
	private PinIntegrationService pinIntegrationService;

	@Mock
	private WileyOrderService wileyOrderService;

	@InjectMocks
	private WileyCheckOrderAction testAction;


	@Before
	public void setUp()
	{
		// Mock the Happy-path
		when(processsMock.getOrder()).thenReturn(orderMock);
		when(orderMock.getCode()).thenReturn("code");
		when(orderMock.getCalculated()).thenReturn(true);
		when(orderMock.getTotalPrice()).thenReturn(ORDER_AMOUNT);
		when(orderMock.getTotalTax()).thenReturn(ORDER_TAXES);
		when(orderMock.getEntries()).thenReturn(Collections.singletonList(orderEntry));
		when(orderMock.getPaymentInfo()).thenReturn(pinfoMock);
		when(orderMock.getPaymentTransactions()).thenReturn(Collections.singletonList(transactionMock));
		when(orderMock.getCurrency()).thenReturn(currencyMock);
		when(orderEntry.getProduct()).thenReturn(productMock);
		when(currencyMock.getDigits()).thenReturn(2);
		when(transactionMock.getEntries()).thenReturn(Collections.singletonList(transEntryMock));
		when(transactionMock.getOrder()).thenReturn(orderMock);
		when(transEntryMock.getTransactionStatus()).thenReturn(TransactionStatus.ACCEPTED.name());
		when(transEntryMock.getType()).thenReturn(PaymentTransactionType.AUTHORIZATION);
		when(transEntryMock.getAmount()).thenReturn(BigDecimal.valueOf(ORDER_AMOUNT + ORDER_TAXES));

		when(paymentTransactionService.calculateTotalAuthorized(orderMock)).thenReturn(ORDER_AMOUNT + ORDER_TAXES);

		when(pinIntegrationService.isPinUsedForOrderPlacement(orderMock)).thenReturn(false);

		when(transEntryMock.getPaymentTransaction()).thenReturn(transactionMock);
		when(transactionMock2.getOrder()).thenReturn(orderMock);
		when(transEntryMock2.getPaymentTransaction()).thenReturn(transactionMock2);
		when(wileyOrderService.isOrderEligibleForPayment(any())).thenReturn(true);

		when(commonI18NService.roundCurrency(anyDouble(), anyInt())).thenAnswer(invocation ->
		{
			Double value = (Double) invocation.getArguments()[0];
			Integer digits = (Integer) invocation.getArguments()[1];
			// CoreAlgorithm is exactly what commonI18NService uses
			return CoreAlgorithms.round(value, digits);
		});
	}

	@Test
	public void shouldCheckValidOrder()
	{
		try
		{
			when(orderMock.getStatus()).thenReturn(OrderStatus.PAYMENT_AUTHORIZED);
			Assert.assertEquals(AbstractSimpleDecisionAction.Transition.OK,
					testAction.executeWileyCheckOrderAction(processsMock));
			checkStatusChange(OrderStatus.CHECKED_AUTHORIZED);
		}
		catch (Exception e)
		{
			Assert.fail("Exception during checking: " + e.getMessage());
		}
	}

	@Test
	public void shouldCheckPartlyAuthorizedOrder() throws Exception
	{
		givenTotalAuthorized(ORDER_AMOUNT + ORDER_TAXES - 0.01);
		when(orderMock.getStatus()).thenReturn(OrderStatus.PAYMENT_AUTHORIZED);
		try
		{
			Assert.assertEquals(AbstractSimpleDecisionAction.Transition.NOK,
					testAction.executeWileyCheckOrderAction(processsMock));
			checkStatusChange(OrderStatus.CHECKED_INVALID);
		}
		catch (Exception e)
		{
			Assert.fail("Exception during checking: " + e.getMessage());
		}

	}

	@Test
	public void shouldCheckNotAuthorizedNotZeroOrder() throws Exception
	{
		try
		{
			when(orderMock.getStatus()).thenReturn(OrderStatus.CREATED);
			givenTotalAuthorized(0D);
			Assert.assertEquals(AbstractSimpleDecisionAction.Transition.NOK,
					testAction.executeWileyCheckOrderAction(processsMock));
			checkStatusChange(OrderStatus.CHECKED_INVALID);
		}
		catch (Exception e)
		{
			Assert.fail("Exception during checking");
		}
	}

	@Test
	public void shouldCheckDoubleAuthorizedNotZeroOrder() throws Exception
	{
		try
		{
			when(orderMock.getStatus()).thenReturn(OrderStatus.PAYMENT_AUTHORIZED);
			when(orderMock.getPaymentTransactions()).thenReturn(Lists.newArrayList(transactionMock2, transactionMock));
			when(transactionMock.getEntries()).thenReturn(Collections.singletonList(transEntryMock));
			when(transactionMock2.getEntries()).thenReturn(Collections.singletonList(transEntryMock2));
			when(transEntryMock.getTransactionStatus()).thenReturn(TransactionStatus.ACCEPTED.name());
			when(transEntryMock.getType()).thenReturn(PaymentTransactionType.AUTHORIZATION);
			when(transEntryMock2.getTransactionStatus()).thenReturn(TransactionStatus.REJECTED.name());
			when(transEntryMock2.getType()).thenReturn(PaymentTransactionType.AUTHORIZATION);

			Assert.assertEquals(AbstractSimpleDecisionAction.Transition.OK,
					testAction.executeWileyCheckOrderAction(processsMock));
			checkStatusChange(OrderStatus.CHECKED_AUTHORIZED);
		}
		catch (Exception e)
		{
			Assert.fail("Exception during checking");
		}
	}

	@Test
	public void shouldCheckMultipleAuthorizationsNotZeroOrder() throws Exception
	{
		try
		{
			when(orderMock.getStatus()).thenReturn(OrderStatus.PAYMENT_AUTHORIZED);
			Assert.assertEquals(AbstractSimpleDecisionAction.Transition.OK,
					testAction.executeWileyCheckOrderAction(processsMock));
			checkStatusChange(OrderStatus.CHECKED_AUTHORIZED);
		}
		catch (Exception e)
		{
			Assert.fail("Exception during checking");
		}
	}

	@Test
	public void shouldCheckOneCentRounding() throws Exception
	{
		try
		{
			when(orderMock.getStatus()).thenReturn(OrderStatus.PAYMENT_AUTHORIZED);
			when(orderMock.getPaymentTransactions()).thenReturn(Lists.newArrayList(transactionMock2, transactionMock));
			BigDecimal total = new BigDecimal(ORDER_AMOUNT).add(BigDecimal.valueOf(ORDER_TAXES)).add(BigDecimal.valueOf(0.001D));
			givenTotalAuthorized(total.doubleValue());

			Assert.assertEquals(AbstractSimpleDecisionAction.Transition.NOK,
					testAction.executeWileyCheckOrderAction(processsMock));
			checkStatusChange(OrderStatus.CHECKED_INVALID);
		}
		catch (Exception e)
		{
			Assert.fail("Exception during checking");
		}
	}

	@Test
	public void shouldCheckInvalidOrder()
	{
		try
		{
			when(orderMock.getEntries()).thenReturn(Collections.emptyList());
			Assert.assertEquals(AbstractSimpleDecisionAction.Transition.NOK,
					testAction.executeWileyCheckOrderAction(processsMock));
			checkStatusChange(OrderStatus.CHECKED_INVALID);

			when(orderMock.getCalculated()).thenReturn(false);
			Assert.assertEquals(AbstractSimpleDecisionAction.Transition.NOK,
					testAction.executeWileyCheckOrderAction(processsMock));

			when(processsMock.getOrder()).thenReturn(null);
			Assert.assertEquals(AbstractSimpleDecisionAction.Transition.NOK,
					testAction.executeWileyCheckOrderAction(processsMock));
		}
		catch (Exception e)
		{
			Assert.fail("Exception during checking");
		}
	}

	@Test
	public void shouldCheckOrderWithPhysicalProductAndNullableDeliveryMode()
	{
		try
		{
			when(productMock.getEditionFormat()).thenReturn(ProductEditionFormat.PHYSICAL);
			when(orderMock.getDeliveryMode()).thenReturn(null);
			Assert.assertEquals(AbstractSimpleDecisionAction.Transition.NOK,
					testAction.executeWileyCheckOrderAction(processsMock));
			checkStatusChange(OrderStatus.CHECKED_INVALID);
		}
		catch (Exception e)
		{
			Assert.fail("Exception during checking");
		}
	}

	@Test
	public void shouldCheckOrderWithPhysicalProductAndNullableDeliveryAddress()
	{
		try
		{
			when(productMock.getEditionFormat()).thenReturn(ProductEditionFormat.PHYSICAL);
			when(orderMock.getDeliveryMode()).thenReturn(deliveryModeMock);
			Assert.assertEquals(AbstractSimpleDecisionAction.Transition.NOK,
					testAction.executeWileyCheckOrderAction(processsMock));
			checkStatusChange(OrderStatus.CHECKED_INVALID);
		}
		catch (Exception e)
		{
			Assert.fail("Exception during checking");
		}
	}

	@Test
	public void shouldCheckOrderWithPinInvalidPinActivation()
	{
		when(pinIntegrationService.isPinUsedForOrderPlacement(orderMock)).thenReturn(true);
		when(pinIntegrationService.validatePinForOrder(orderMock)).thenReturn(false);
		when(orderMock.getStatus()).thenReturn(OrderStatus.PAYMENT_AUTHORIZED);
		testAction.setPinValidateEnabled(true);
		try
		{
			Assert.assertEquals(AbstractSimpleDecisionAction.Transition.NOK,
					testAction.executeWileyCheckOrderAction(processsMock));
			checkStatusChange(OrderStatus.CHECKED_INVALID);
		}
		catch (Exception e)
		{
			Assert.fail("Exception during checking");
		}
	}


	@Test
	public void shouldCheckOrderWithPinValidationDisabled()
	{
		when(orderMock.getStatus()).thenReturn(OrderStatus.PAYMENT_AUTHORIZED);
		testAction.setPinValidateEnabled(false);
		try
		{
			Assert.assertEquals(AbstractSimpleDecisionAction.Transition.OK,
					testAction.executeWileyCheckOrderAction(processsMock));
			checkStatusChange(OrderStatus.CHECKED_AUTHORIZED);
			Mockito.verify(pinIntegrationService, Mockito.never()).isPinUsedForOrderPlacement(orderMock);
			Mockito.verify(pinIntegrationService, Mockito.never()).validatePinForOrder(orderMock);
		}
		catch (Exception e)
		{
			Assert.fail("Exception during checking");
		}
	}

	@Test
	public void shouldCheckOrderWithPinValidPinActivation()
	{
		when(pinIntegrationService.isPinUsedForOrderPlacement(orderMock)).thenReturn(true);
		when(pinIntegrationService.validatePinForOrder(orderMock)).thenReturn(true);
		when(orderMock.getStatus()).thenReturn(OrderStatus.CREATED);
		testAction.setPinValidateEnabled(true);
		try
		{
			Assert.assertEquals(AbstractSimpleDecisionAction.Transition.OK,
					testAction.executeWileyCheckOrderAction(processsMock));
			checkStatusChange(OrderStatus.CHECKED_AUTHORIZED);
		}
		catch (Exception e)
		{
			Assert.fail("Exception during checking");
		}
	}

	@Test
	public void shouldNotCheckAuthAmountForInvoiceOrder() throws Exception
	{
		when(orderMock.getStatus()).thenReturn(OrderStatus.CREATED);
		when(orderMock.getPaymentInfo()).thenReturn(invoicePaymentInfo);
		Assert.assertEquals(AbstractSimpleDecisionAction.Transition.OK, testAction.executeWileyCheckOrderAction(processsMock));
		checkStatusChange(OrderStatus.CHECKED_AUTHORIZED);
	}

	private void checkStatusChange(final OrderStatus expectedStatus)
	{
		Mockito.verify(orderMock).setStatus(Matchers.eq(expectedStatus));
		Mockito.verify(modelService).save(orderMock);
	}

	private void givenTotalAuthorized(double value)
	{
		when(paymentTransactionService.calculateTotalAuthorized(orderMock)).thenReturn(value);
	}

}
