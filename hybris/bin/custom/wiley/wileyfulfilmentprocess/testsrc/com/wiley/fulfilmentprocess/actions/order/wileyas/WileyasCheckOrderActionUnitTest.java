package com.wiley.fulfilmentprocess.actions.order.wileyas;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyDouble;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyOrderProcessModel;
import com.wiley.core.order.PaymentActonType;
import com.wiley.core.order.PendingPaymentActon;
import com.wiley.core.order.service.WileyOrderService;
import com.wiley.core.payment.transaction.PaymentTransactionService;
import com.wiley.core.wiley.order.WileyOrderPaymentService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyasCheckOrderActionUnitTest
{

	private static final double TOTAL_PRICE = 1.99;
	private static final double TOTAL_TAX = .99;
	private static final double TOTAL_TO_PAY = TOTAL_PRICE + TOTAL_TAX;
	private static final String NOT_HYBRIS = "NotHybris";
	private static final String HYBRIS = "hybris";
	private final WileyOrderProcessModel testOrderProcess = new WileyOrderProcessModel();
	private final OrderModel testOrder = new OrderModel();
	private final OrderEntryModel firstEntry = new OrderEntryModel();
	private final PendingPaymentActon pendingPaymentActon = new PendingPaymentActon();

	@Mock
	private ModelService modelService;
	@Mock
	private PaymentTransactionService paymentTransactionService;
	@Mock
	private CommonI18NService commonI18NService;
	@Mock
	private WileyOrderService wileyOrderService;
	@Mock
	private WileyOrderPaymentService wileyOrderPaymentService;

	@InjectMocks
	private WileyasCheckOrderAction testInstance = new WileyasCheckOrderAction();

	@Before
	public void setUp() throws Exception
	{

		//Only Success order state should be pre-configured
		testOrder.setCalculated(true);
		testOrder.setTotalPrice(TOTAL_PRICE);
		testOrder.setTotalTax(TOTAL_TAX);
		testOrder.setEntries(Arrays.asList(firstEntry));
		testOrder.setStatus(OrderStatus.CREATED);
		testOrder.setSourceSystem(HYBRIS);

		CurrencyModel orderCurrency = new CurrencyModel();
		orderCurrency.setDigits(2);
		testOrder.setCurrency(orderCurrency);

		testOrderProcess.setOrder(testOrder);
		testOrderProcess.setProcessPayment(true);

		when(wileyOrderPaymentService.getPendingPaymentActions(testOrder))
				.thenReturn(Collections.singletonList(pendingPaymentActon));

		when(wileyOrderService.isOrderEligibleForPayment(any())).thenReturn(true);
		when(wileyOrderService.isExternalOrder(any())).thenReturn(false);
		when(paymentTransactionService.calculateTotalAuthorized(testOrder)).thenReturn(TOTAL_TO_PAY);
		when(commonI18NService.roundCurrency(anyDouble(), anyInt())).thenReturn(TOTAL_TO_PAY);

	}

	@Test
	public void shouldFailOrderIfOrderIsNotCalculated() throws Exception
	{
		//Given
		testOrder.setCalculated(false);
		//When
		String result = testInstance.execute(testOrderProcess);
		//Then
		Assert.assertEquals(WileyasCheckOrderAction.Transition.NOK.toString(), result);
		Assert.assertEquals(OrderStatus.CHECKED_INVALID, testOrder.getStatus());
	}

	@Test
	public void shouldFailOrderIfOrderHasNoEntries() throws Exception
	{
		//Given
		testOrder.setEntries(Collections.emptyList());
		//When
		String result = testInstance.execute(testOrderProcess);
		//Then
		Assert.assertEquals(WileyasCheckOrderAction.Transition.NOK.toString(), result);
		Assert.assertEquals(OrderStatus.CHECKED_INVALID, testOrder.getStatus());
	}

	@Test
	public void shouldFailOrderIfAuthorizationPending() throws Exception
	{
		//Given
		pendingPaymentActon.setAction(PaymentActonType.AUTHORIZE);
		//When
		String result = testInstance.execute(testOrderProcess);
		//Then
		Assert.assertEquals(WileyasCheckOrderAction.Transition.NOK.toString(), result);
		Assert.assertEquals(OrderStatus.CHECKED_INVALID, testOrder.getStatus());
	}

	@Test
	public void statusShouldChangeToCheckedValidIfOrderIsValid() throws Exception
	{
		pendingPaymentActon.setAction(PaymentActonType.CAPTURE);
		//When
		String result = testInstance.execute(testOrderProcess);
		//Then
		Assert.assertEquals(WileyasCheckOrderAction.Transition.OK.toString(), result);
		Assert.assertEquals(OrderStatus.CHECKED_VALID, testOrder.getStatus());
	}

	@Test
	public void statusShouldChangeToCheckedValidIfOrderIsValidAndNoPendingPayments() throws Exception
	{
		when(wileyOrderPaymentService.getPendingPaymentActions(testOrder)).thenReturn(Collections.emptyList());
		//When
		String result = testInstance.execute(testOrderProcess);
		//Then
		Assert.assertEquals(WileyasCheckOrderAction.Transition.OK.toString(), result);
		Assert.assertEquals(OrderStatus.CHECKED_VALID, testOrder.getStatus());
	}

	@Test
	public void actionShouldBeSkippedForNonHybrisOrder() throws Exception
	{
		//Given
		when(wileyOrderService.isExternalOrder(any())).thenReturn(true);
		//When
		String result = testInstance.execute(testOrderProcess);
		//Then
		Assert.assertEquals(WileyasCheckOrderAction.Transition.SKIP_PAYMENT.toString(), result);
		Assert.assertEquals(OrderStatus.CREATED, testOrder.getStatus());
	}

	@Test
	public void actionShouldBeSkippedForNonProcessedPaymentOrder() throws Exception
	{
		//Given
		testOrderProcess.setProcessPayment(false);
		//When
		String result = testInstance.execute(testOrderProcess);
		//Then
		Assert.assertEquals(WileyasCheckOrderAction.Transition.SKIP_PAYMENT.toString(), result);
		Assert.assertEquals(OrderStatus.CHECKED_VALID, testOrder.getStatus());
	}
}
