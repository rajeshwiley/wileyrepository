/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.fulfilmentprocess.test.actions;

import de.hybris.platform.core.Registry;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.processengine.action.AbstractAction;
import de.hybris.platform.processengine.model.BusinessProcessModel;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;


/**
 * The type Test action temp.
 *
 * @param <T>
 * 		the type parameter
 */
public class TestActionTemp<T extends BusinessProcessModel> extends AbstractAction<T>
{
	private static final Logger LOG = Logger.getLogger(TestActionTemp.class);

	private String result = "OK";
	private boolean throwException = false;

	/**
	 * Gets result.
	 *
	 * @return the result
	 */
	public String getResult()
	{
		return result;
	}

	/**
	 * Sets result.
	 *
	 * @param result
	 * 		the result
	 */
	public void setResult(final String result)
	{
		this.result = result;
	}

	/**
	 * Sets throw exception.
	 *
	 * @param throwException
	 * 		the throw exception
	 */
	public void setThrowException(final boolean throwException)
	{
		this.throwException = throwException;
	}

	/**
	 * Execute string.
	 *
	 * @param process
	 * 		the process
	 * @return the string
	 * @throws Exception
	 * 		the exception
	 */
	@Override
	public String execute(final T process) throws Exception //NOPMD
	{
		// This call actually puts -this- into a queue.
		try
		{
			if (throwException)
			{
				throw new RuntimeException("Error");
			}
		}
		finally
		{
			//getQueueService().actionExecuted(getProcess(process), this);
		}

		LOG.info(result);
		return result;
	}

	/**
	 * Gets transitions.
	 *
	 * @return the transitions
	 */
	@Override
	public Set<String> getTransitions()
	{
		final Set<String> res = new HashSet<String>();
		res.add(result);
		return res;
	}

	/**
	 * Gets business process service.
	 *
	 * @return the business process service
	 */
	protected BusinessProcessService getBusinessProcessService()
	{
		return (BusinessProcessService) Registry.getApplicationContext().getBean("businessProcessService");
	}
}
