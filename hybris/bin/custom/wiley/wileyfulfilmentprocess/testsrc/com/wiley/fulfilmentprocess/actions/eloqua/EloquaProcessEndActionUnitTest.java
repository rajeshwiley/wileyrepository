package com.wiley.fulfilmentprocess.actions.eloqua;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.model.ModelService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.fulfilmentprocess.constants.WileyFulfilmentProcessConstants;
import com.wiley.fulfilmentprocess.model.EloquaOrderExportProcessModel;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


/**
 * Default unit test for {@link EloquaProcessEndAction}.
 *
 * @author Aliaksei_Zlobich
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class EloquaProcessEndActionUnitTest
{

	@Mock
	private BusinessProcessService businessProcessServiceMock;
	@Mock
	private ModelService mockModelService;

	@InjectMocks
	private EloquaProcessEndAction eloquaProcessEndAction;

	// Test data

	@Mock
	private EloquaOrderExportProcessModel eloquaOrderExportProcessModelMock;

	@Test
	public void shouldSendEventToParentBusinessProcess() throws Exception
	{
		// Given
		String parentProcessCode = "test12345";
		OrderProcessModel parentProcess = mock(OrderProcessModel.class);
		when(parentProcess.getCode()).thenReturn(parentProcessCode);
		when(eloquaOrderExportProcessModelMock.getParentProcess()).thenReturn(parentProcess);

		// When
		eloquaProcessEndAction.executeAction(eloquaOrderExportProcessModelMock);

		// Then
		verify(businessProcessServiceMock).triggerEvent(eq(
				parentProcessCode + "_" + WileyFulfilmentProcessConstants.WILEY_EXPORT_SUBPROCESS_END_EVENT_NAME));
	}

	@Test
	public void shouldNotSendEventToParentProcessIfNotSubProcess() throws Exception
	{
		// Given
		when(eloquaOrderExportProcessModelMock.getParentProcess()).thenReturn(null);

		// When
		eloquaProcessEndAction.executeAction(eloquaOrderExportProcessModelMock);

		// Then
		verify(businessProcessServiceMock, never()).triggerEvent(anyString());
	}

}