/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.fulfilmentprocess.test.actions.consignmentfulfilment;

import de.hybris.platform.processengine.model.BusinessProcessModel;

import org.apache.log4j.Logger;

import com.wiley.fulfilmentprocess.constants.WileyFulfilmentProcessConstants;
import com.wiley.fulfilmentprocess.test.actions.TestActionTemp;


/**
 * The type Abstract test cons action temp.
 */
public abstract class AbstractTestConsActionTemp extends TestActionTemp
{
	private static final Logger LOG = Logger.getLogger(AbstractTestConsActionTemp.class);

	/**
	 * Execute string.
	 *
	 * @param process
	 * 		the process
	 * @return the string
	 * @throws Exception
	 * 		the exception
	 */
	@Override
	public String execute(final BusinessProcessModel process) throws Exception //NOPMD
	{
		//getQueueService().actionExecuted(getParentProcess(process), this);
		LOG.info(getResult());
		return getResult();
	}


	/**
	 * Gets parent process.
	 *
	 * @param process
	 * 		the process
	 * @return the parent process
	 */
	public BusinessProcessModel getParentProcess(final BusinessProcessModel process)
	{
		final String parentCode = (String) getProcessParameterValue(process, WileyFulfilmentProcessConstants.PARENT_PROCESS);
		return getBusinessProcessService().getProcess(parentCode);
	}
}
