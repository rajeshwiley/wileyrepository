/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.fulfilmentprocess.test;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.FraudStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.fraud.model.FraudReportModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.servicelayer.time.TimeService;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;

import org.junit.Before;
import org.junit.Test;

import com.wiley.fulfilmentprocess.actions.order.ScheduleForCleanUpAction;

import junit.framework.Assert;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


/**
 * The type Schedule for clean up action test.
 */
@UnitTest
public class ScheduleForCleanUpActionTest
{
	private ScheduleForCleanUpAction action = null;
	private OrderProcessModel orderProcess = null;
	private final Integer minPeriodWaitingForCleanUp = Integer.valueOf(10);

	/**
	 * Sets .
	 */
	@Before
	public void setup()
	{
		prepareAction();
		prepareOrderProcess();
	}

	/**
	 * Prepare action.
	 */
	protected void prepareAction()
	{
		action = new ScheduleForCleanUpAction();
		action.setTimeService(mockTimeService());
		action.setMinPeriodWaitingForCleanUpInSeconds(minPeriodWaitingForCleanUp);
	}

	/**
	 * Prepare order process.
	 */
	protected void prepareOrderProcess()
	{
		final OrderModel order = new OrderModel();
		order.setFraudReports(new HashSet<FraudReportModel>());
		orderProcess = new OrderProcessModel();
		orderProcess.setOrder(order);
	}

	/**
	 * Mock time service time service.
	 *
	 * @return the time service
	 */
	protected TimeService mockTimeService()
	{
		final TimeService mockedTimeService = mock(TimeService.class);
		when(mockedTimeService.getCurrentTime()).thenReturn(new Date());
		return mockedTimeService;
	}

	/**
	 * Test order should be cleaned.
	 */
	@Test
	public void testOrderShouldBeCleaned()
	{
		//given
		final Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(Calendar.SECOND, -minPeriodWaitingForCleanUp.intValue() * 5);
		final Date timestamp = calendar.getTime();
		final FraudReportModel fraudReport = new FraudReportModel();
		fraudReport.setTimestamp(timestamp);
		fraudReport.setStatus(FraudStatus.FRAUD);
		orderProcess.getOrder().getFraudReports().add(fraudReport);
		//when
		final AbstractSimpleDecisionAction.Transition result = action.executeAction(orderProcess);
		//then
		Assert.assertEquals(AbstractSimpleDecisionAction.Transition.OK, result);
	}

	/**
	 * Test order should not be cleaned.
	 */
	@Test
	public void testOrderShouldNotBeCleaned()
	{
		//given
		final Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(Calendar.SECOND, (int) (minPeriodWaitingForCleanUp.intValue() * 0.5));
		final Date timestamp = calendar.getTime();
		final FraudReportModel fraudReport = new FraudReportModel();
		fraudReport.setTimestamp(timestamp);
		fraudReport.setStatus(FraudStatus.FRAUD);
		orderProcess.getOrder().getFraudReports().add(fraudReport);
		//when
		final AbstractSimpleDecisionAction.Transition result = action.executeAction(orderProcess);
		//then
		Assert.assertEquals(AbstractSimpleDecisionAction.Transition.NOK, result);
	}


}
