package com.wiley.fulfilmentprocess.actions.order;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.integration.order.WileyOrdersGateway;
import com.wiley.core.model.WileyOrderProcessModel;


@RunWith(MockitoJUnitRunner.class)
public class WileyPublishOrderActionUnitTest
{

	private WileyOrderProcessModel testOrderProcess = new WileyOrderProcessModel();
	private OrderModel testOrder = new OrderModel();

	@Mock
	private WileyOrdersGateway wileyOrdersGateway;
	@Mock
	private ModelService modelService;

	@InjectMocks
	private final WileyPublishOrderAction testInstance = new WileyPublishOrderAction();

	@Before
	public void setUp() throws Exception
	{
		testOrderProcess.setOrder(testOrder);
	}

	@Test
	public void shouldPerformOrderExport() throws Exception
	{
		//When
		testInstance.executeAction(testOrderProcess);
		//Then
		verify(wileyOrdersGateway, atLeastOnce()).publish(testOrder, null);

	}

	@Test
	public void shouldSetOrderStatusAsExported2ErpIfOrderExportedSuccessfully() throws Exception
	{
		//When
		testInstance.executeAction(testOrderProcess);
		//Then
		assertEquals(OrderStatus.EXPORTED_TO_ERP, testOrder.getStatus());
	}

}