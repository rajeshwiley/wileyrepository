package com.wiley.fulfilmentprocess.actions.eloqua;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.fulfilmentprocess.model.EloquaOrderExportProcessModel;
import com.wiley.core.integration.eloqua.EloquaGateway;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


/**
 * Default unit test for {@link UpdateEloquaContactAction}.
 *
 * @author Aliaksei_Zlobich
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class UpdateEloquaContactActionUnitTest
{

	private static final double TOTAL = 10.0d;
	@Mock
	private EloquaGateway eloquaGatewayMock;

	@InjectMocks
	private UpdateEloquaContactAction updateEloquaContactAction;

	// Test data

	@Mock
	private EloquaOrderExportProcessModel eloquaOrderExportProcessModelMock;

	@Mock
	private OrderModel orderModelMock;

	@Mock
	private CustomerModel customerModelMock;

	@Mock
	private AddressModel addressModelMock;

	@Mock
	private BaseSiteModel siteModelMock;

	@Mock
	private AddressModel defaultPaymentAddressMock;
	
	// needs new boolean object to test if subscribeToUpdates status is gotten from order.
	private static final Boolean SUBSCRIBE_TO_UPDATES = new Boolean(true);

	@Before
	public void setUp() throws Exception
	{
		when(eloquaOrderExportProcessModelMock.getOrder()).thenReturn(orderModelMock);
		when(orderModelMock.getUser()).thenReturn(customerModelMock);
		when(orderModelMock.getSite()).thenReturn(siteModelMock);
		when(orderModelMock.getPaymentAddress()).thenReturn(addressModelMock);
		when(orderModelMock.getSubscribeToUpdates()).thenReturn(SUBSCRIBE_TO_UPDATES);
		when(orderModelMock.getTotalPrice()).thenReturn(TOTAL);
	}

	@Test
	public void shouldReturnOKAndUpdateEloquaContact() throws Exception
	{
		// Given
		// no changes in test data

		// When
		final AbstractSimpleDecisionAction.Transition transition = updateEloquaContactAction.executeAction(
				eloquaOrderExportProcessModelMock);

		// Then
		assertEquals("Returned transition should be equal expected.", AbstractSimpleDecisionAction.Transition.OK, transition);
	}

	@Test
	public void shouldReturnNORIfSomeExceptionOccurredDuringContactUpdating() throws Exception
	{
		// Given
		when(eloquaGatewayMock
				.updateEloquaContact(any(CustomerModel.class), any(AddressModel.class), anyBoolean(), any(BaseSiteModel.class)))
				.thenThrow(
						Exception.class);

		// When
		final AbstractSimpleDecisionAction.Transition transition = updateEloquaContactAction.executeAction(
				eloquaOrderExportProcessModelMock);

		// Then
		assertEquals("Returned transition should be equal expected.", AbstractSimpleDecisionAction.Transition.NOK, transition);
	}

	@Test
	public void shouldSendExpectedParamsToEloquaGateway() throws Exception
	{
		// Given

		// When
		updateEloquaContactAction.executeAction(eloquaOrderExportProcessModelMock);

		// Then
		verify(eloquaGatewayMock).updateEloquaContact(same(customerModelMock), same(addressModelMock), eq(SUBSCRIBE_TO_UPDATES),
				same(siteModelMock));
	}


	@Test
	public void shouldUseDefaultAddressIfAbsent() throws Exception
	{
		// Given
		doReturn(null).when(orderModelMock).getPaymentAddress();
		doReturn(defaultPaymentAddressMock).when(customerModelMock).getDefaultPaymentAddress();

		// Testing
		updateEloquaContactAction.executeAction(eloquaOrderExportProcessModelMock);

		// Verify
		verify(eloquaGatewayMock).updateEloquaContact(same(customerModelMock), eq(defaultPaymentAddressMock),
				same(SUBSCRIBE_TO_UPDATES), same(siteModelMock));
	}

}