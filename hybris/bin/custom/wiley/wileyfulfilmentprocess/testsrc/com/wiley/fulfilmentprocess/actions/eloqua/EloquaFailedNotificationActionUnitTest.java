package com.wiley.fulfilmentprocess.actions.eloqua;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.fulfilmentprocess.model.EloquaOrderExportProcessModel;

import static org.mockito.Mockito.when;


/**
 * Default unit test for {@link EloquaFailedNotificationAction}.
 *
 * @author Aliaksei_Zlobich
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class EloquaFailedNotificationActionUnitTest
{

	private EloquaFailedNotificationAction eloquaFailedNotificationAction;

	// Test data

	@Mock
	private EloquaOrderExportProcessModel eloquaOrderExportProcessModelMock;
	private static final String PROCESS_CODE = "testProcessCode";

	@Mock
	private OrderModel orderModelMock;
	private static final String ORDER_CODE = "order123";

	@Mock
	private OrderProcessModel parentProcessModelMock;
	private static final String PARENT_PROCESS_CODE = "parentTest12345";

	@Before
	public void setUp()
	{
		when(orderModelMock.getCode()).thenReturn(ORDER_CODE);

		// set up parent business process
		when(parentProcessModelMock.getCode()).thenReturn(PARENT_PROCESS_CODE);
		when(eloquaOrderExportProcessModelMock.getParentProcess()).thenReturn(parentProcessModelMock);

		// set up eloqua business process
		when(eloquaOrderExportProcessModelMock.getCode()).thenReturn(PROCESS_CODE);
		when(eloquaOrderExportProcessModelMock.getOrder()).thenReturn(orderModelMock);

		eloquaFailedNotificationAction = new EloquaFailedNotificationAction();
	}

	@Test
	public void testExecuteAction() throws Exception
	{
		// Given
		// no changes in test data

		// When
		eloquaFailedNotificationAction.executeAction(eloquaOrderExportProcessModelMock);

		// Then
		// should execute without errors.
	}
}