package com.wiley.fulfilmentprocess.actions.eloqua;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.fulfilmentprocess.model.EloquaOrderExportProcessModel;
import com.wiley.core.integration.eloqua.EloquaGateway;
import com.wiley.core.integration.eloqua.dto.ContactDto;

import static junit.framework.Assert.assertEquals;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


/**
 * Default unit test for {@link SearchForEloquaContactAction}.
 *
 * @author Aliaksei_Zlobich
 */
@RunWith(MockitoJUnitRunner.class)
public class SearchForEloquaContactActionUnitTest
{

	@Mock
	private EloquaGateway eloquaGatewayMock;

	@Mock
	private ModelService modelServiceMock;

	@InjectMocks
	private SearchForEloquaContactAction searchForEloquaContactAction;

	// Test data
	@Mock
	private ContactDto contactDtoMock;

	@Mock
	private EloquaOrderExportProcessModel eloquaOrderExportProcessModelMock;

	@Mock
	private OrderModel orderModelMock;

	@Mock
	private CustomerModel customerModelMock;

	@Mock
	private BaseSiteModel siteModelMock;

	@Before
	public void setUp() throws Exception
	{
		when(eloquaOrderExportProcessModelMock.getOrder()).thenReturn(orderModelMock);
		when(orderModelMock.getSite()).thenReturn(siteModelMock);
		when(orderModelMock.getUser()).thenReturn(customerModelMock);
	}

	@Test
	public void shouldSaveEloquaIdIfContactWasFound() throws Exception
	{
		// Given
		when(eloquaGatewayMock.searchForEloquaContact(eq(customerModelMock), eq(siteModelMock))).thenReturn(
				Optional.of(contactDtoMock));

		// When
		final String transition = searchForEloquaContactAction.execute(eloquaOrderExportProcessModelMock);

		// Then
		assertEquals("Should be equal expected transition.", SearchForEloquaContactAction.Transition.FOUND.toString(),
				transition);
		verify(customerModelMock).setEloquaId(anyInt());
		verify(modelServiceMock).save(eq(customerModelMock));
	}

	@Test
	public void shouldNotSaveEloquaIdAndReturnNotFoundIfContactWasNotFound() throws Exception
	{
		// Given
		when(eloquaGatewayMock.searchForEloquaContact(eq(customerModelMock), eq(siteModelMock))).thenReturn(Optional.empty());

		// When
		final String transition = searchForEloquaContactAction.execute(eloquaOrderExportProcessModelMock);

		// Then
		assertEquals("Should be equal expected transition.", SearchForEloquaContactAction.Transition.NOT_FOUND.toString(),
				transition);
		verify(customerModelMock, never()).setEloquaId(anyInt());
		verify(modelServiceMock, never()).save(eq(customerModelMock));
	}

	@Test
	public void shouldReturnFailureIfSomeExceptionOccurredDuringRequestToEloqua() throws Exception
	{
		// Given
		when(eloquaGatewayMock.searchForEloquaContact(eq(customerModelMock), eq(siteModelMock))).thenThrow(Exception.class);

		// When
		final String transition = searchForEloquaContactAction.execute(eloquaOrderExportProcessModelMock);

		// Then
		assertEquals("Should be equal expected transition.", SearchForEloquaContactAction.Transition.FAILURE.toString(),
				transition);
		verify(customerModelMock, never()).setEloquaId(anyInt());
		verify(modelServiceMock, never()).save(eq(customerModelMock));
	}

}