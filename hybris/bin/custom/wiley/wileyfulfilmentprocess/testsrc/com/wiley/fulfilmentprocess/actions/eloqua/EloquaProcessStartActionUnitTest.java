package com.wiley.fulfilmentprocess.actions.eloqua;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.fulfilmentprocess.model.EloquaOrderExportProcessModel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


/**
 * Default unit test for {@link EloquaProcessStartAction}.
 *
 * @author Aliaksei_Zlobich
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class EloquaProcessStartActionUnitTest
{

	private EloquaProcessStartAction eloquaProcessStartAction;

	// Test data

	@Mock
	private EloquaOrderExportProcessModel eloquaOrderExportProcessModelMock;

	@Mock
	private OrderModel orderModelMock;

	@Mock
	private CustomerModel customerModelMock;

	@Before
	public void setUp() throws Exception
	{
		when(eloquaOrderExportProcessModelMock.getOrder()).thenReturn(orderModelMock);

		when(orderModelMock.getUser()).thenReturn(customerModelMock);

		eloquaProcessStartAction = new EloquaProcessStartAction();
	}

	@Test
	public void shouldReturnEloquaIdExistsTransition() throws Exception
	{
		// Given
		when(customerModelMock.getEloquaId()).thenReturn(1);

		// When
		final String transition = eloquaProcessStartAction.execute(eloquaOrderExportProcessModelMock);

		// Then
		checkTransition(EloquaProcessStartAction.Transition.ELOQUA_ID_EXISTS, transition);
	}

	@Test
	public void shouldReturnEloquaIdNotExistTransition() throws Exception
	{
		// Given
		when(customerModelMock.getEloquaId()).thenReturn(null);

		// When
		final String transition = eloquaProcessStartAction.execute(eloquaOrderExportProcessModelMock);

		// Then
		checkTransition(EloquaProcessStartAction.Transition.ELOQUA_ID_NOT_EXIST, transition);
	}

	@Test
	public void shouldThrowIllegalStateExceptionIfOrderIsNotAssignedToCustomer() throws Exception
	{
		// Given
		reset(orderModelMock);
		when(orderModelMock.getUser()).thenReturn(new UserModel());

		// When
		try
		{
			eloquaProcessStartAction.execute(eloquaOrderExportProcessModelMock);
			fail("Expected IllegalStateException.");
		}
		catch (IllegalStateException e)
		{
			// success
		}

		// Then
		verify(orderModelMock).getUser();
	}

	private void checkTransition(final EloquaProcessStartAction.Transition expectedTransition, final String transition)
	{
		assertEquals("Transition should equal expected.", expectedTransition.toString(), transition);
	}
}