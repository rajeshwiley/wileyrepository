package com.wiley.fulfilmentprocess.actions.order;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyOrderProcessModel;


@RunWith(MockitoJUnitRunner.class)
public class WileyDeactivateProcessActionUnitTest
{
	@Mock
	private WileyOrderProcessModel orderProcessMock;

	@Mock
	private ModelService modelServiceMock;

	@InjectMocks
	private WileyDeactivateProcessAction testInstance;

	@Test
	public void shouldChangeBusinessProcess() throws Exception
	{
		//When
		testInstance.executeAction(orderProcessMock);
		//Then
		verify(orderProcessMock).setActiveFor(isNull(OrderModel.class));
		verify(modelServiceMock).save(orderProcessMock);
	}

}