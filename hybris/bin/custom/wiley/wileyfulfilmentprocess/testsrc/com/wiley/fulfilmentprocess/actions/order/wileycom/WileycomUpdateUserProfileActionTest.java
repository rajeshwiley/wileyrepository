package com.wiley.fulfilmentprocess.actions.order.wileycom;

import com.wiley.core.customer.service.WileyCustomerAccountService;
import com.wiley.core.model.SchoolModel;
import com.wiley.core.wileycom.customer.service.WileycomCustomerService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.servicelayer.model.ModelService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileycomUpdateUserProfileActionTest
{
	private static final String FIRST_SCHOOL_CODE = "School#29";
	private static final String SECOND_SCHOOL_CODE = "school#123123";
	private static final String CUSTOMER_ID = "customerId";

	@InjectMocks
	private WileycomUpdateUserProfileAction testedInstance;

	@Mock
	private WileyCustomerAccountService wileyCustomerAccountService;
	@Mock
	private WileycomCustomerService wileycomCustomerService;
	@Mock
	private ModelService modelService;
	@Mock
	private OrderProcessModel orderProcess;
	@Mock
	private OrderModel order;
	@Mock
	private CustomerModel customer;
	@Mock
	private SchoolModel firstSchool;
	@Mock
	private SchoolModel secondSchool;
	@Mock(answer = Answers.RETURNS_DEEP_STUBS)
	private AbstractOrderEntryModel textBookOrderEntry;
	@Mock(answer = Answers.RETURNS_DEEP_STUBS)
	private AbstractOrderEntryModel notATextbookOrderEntry;
	@Mock(answer = Answers.RETURNS_DEEP_STUBS)
	private AbstractOrderEntryModel textbookNotSpecifiedOrderEntry;

	@Before
	public void setUp()
	{
		when(orderProcess.getOrder()).thenReturn(order);
		when(order.getUser()).thenReturn(customer);
		when(customer.getCustomerID()).thenReturn(CUSTOMER_ID);
		when(firstSchool.getCode()).thenReturn(FIRST_SCHOOL_CODE);
		when(secondSchool.getCode()).thenReturn(SECOND_SCHOOL_CODE);
		setUpOrderEntries();
	}

	private void setUpOrderEntries()
	{
		when(order.getEntries()).thenReturn(
				Arrays.asList(notATextbookOrderEntry, textBookOrderEntry, textbookNotSpecifiedOrderEntry));
		when(textBookOrderEntry.getProduct().getTextbook()).thenReturn(Boolean.TRUE);
		when(notATextbookOrderEntry.getProduct().getTextbook()).thenReturn(Boolean.FALSE);
		when(textbookNotSpecifiedOrderEntry.getProduct().getTextbook()).thenReturn(null);
	}

	@Test
	public void shouldNotUpdateProfileIfOrderDoesNotContainAnyTextbook() throws DuplicateUidException
	{
		//Given
		when(order.getEntries()).thenReturn(Arrays.asList(notATextbookOrderEntry, textbookNotSpecifiedOrderEntry));
		when(textBookOrderEntry.getProduct().getTextbook()).thenReturn(Boolean.TRUE);
		when(customer.getSchool()).thenReturn(null);
		when(order.getSchool()).thenReturn(firstSchool);
		//When
		testedInstance.executeAction(orderProcess);
		//Then
		verifyZeroInteractions(wileyCustomerAccountService, wileycomCustomerService);
	}

	@Test
	public void shouldNotUpdateSchoolWhenSchoolsAreNull() throws DuplicateUidException
	{
		//Given
		when(customer.getSchool()).thenReturn(null);
		when(order.getSchool()).thenReturn(null);
		//When
		testedInstance.executeAction(orderProcess);
		//Then
		verifyZeroInteractions(wileyCustomerAccountService, wileycomCustomerService);
	}

	@Test
	public void shouldNotUpdateSchoolWhenSchoolsAreSame() throws DuplicateUidException
	{
		//Given
		when(customer.getSchool()).thenReturn(firstSchool);
		when(order.getSchool()).thenReturn(firstSchool);
		//When
		testedInstance.executeAction(orderProcess);
		//Then
		verifyZeroInteractions(wileyCustomerAccountService, wileycomCustomerService);
	}

	@Test
	public void shouldUpdateCustomerProfileWithoutCallingExternalUpdateForGuestCheckout() throws DuplicateUidException
	{
		//Given
		when(customer.getType()).thenReturn(CustomerType.GUEST);
		when(customer.getSchool()).thenReturn(firstSchool);
		when(order.getSchool()).thenReturn(secondSchool);
		//When
		testedInstance.executeAction(orderProcess);
		//Then
		verifyZeroInteractions(wileycomCustomerService);
		verify(wileyCustomerAccountService).updateCustomerProfile(customer);
	}

}