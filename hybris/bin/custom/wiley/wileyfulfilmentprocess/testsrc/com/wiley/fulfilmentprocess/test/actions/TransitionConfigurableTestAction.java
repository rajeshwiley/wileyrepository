package com.wiley.fulfilmentprocess.test.actions;

import de.hybris.platform.processengine.model.BusinessProcessModel;

import java.util.Collections;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;


/**
 * Test action that allows to configure supported transitions via spring configuration.
 *
 * @author Aliaksei_Zlobich
 */
public class TransitionConfigurableTestAction<T extends BusinessProcessModel> extends TestActionTemp<T>
{
	private Set<String> supportedTransitions = Collections.emptySet();

	@Override
	public Set<String> getTransitions()
	{
		return CollectionUtils.isNotEmpty(supportedTransitions) ? supportedTransitions : super.getTransitions();
	}

	public Set<String> getSupportedTransitions()
	{
		return supportedTransitions;
	}

	public void setSupportedTransitions(final Set<String> supportedTransitions)
	{
		this.supportedTransitions = supportedTransitions;
	}
}
