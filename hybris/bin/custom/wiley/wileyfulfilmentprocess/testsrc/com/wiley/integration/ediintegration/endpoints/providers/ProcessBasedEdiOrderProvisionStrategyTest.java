package com.wiley.integration.ediintegration.endpoints.providers;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.store.BaseStoreModel;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.enums.ExportProcessType;
import com.wiley.fulfilmentprocess.model.WileyExportProcessModel;
import com.wiley.core.integration.edi.EDIConstants;
import com.wiley.core.integration.edi.dao.EdiOrderDao;
import com.wiley.core.integration.edi.strategy.EdiProcessValidatingStrategy;
import com.wiley.core.integration.edi.strategy.impl.ProcessBasedEdiOrderProvisionStrategy;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ProcessBasedEdiOrderProvisionStrategyTest
{
	@Mock
	private EdiOrderDao ediOrderDao;
	@Mock
	private EdiProcessValidatingStrategy refundedTheSameDayProcessesValidationStrategy;
	@Mock
	private BusinessProcessService businessProcessService;
	@Mock
	private ModelService modelService;

	@Spy
	@InjectMocks
	private ProcessBasedEdiOrderProvisionStrategy testInstance;
	@Mock
	private OrderModel order1;
	@Mock
	private OrderModel order2;
	@Mock
	private OrderModel order3;
	@Mock
	private BaseStoreModel store;

	@Before
	public void setUp() throws Exception
	{
		when(order1.getCode()).thenReturn("order1");
		when(order2.getCode()).thenReturn("order2");
		when(order3.getCode()).thenReturn("order3");

		List<WileyExportProcessModel> dummyProcessList =
				Collections.singletonList(givenOrderExportProcess(order1, ExportProcessType.SETTLE));
		when(ediOrderDao.getExportProcessesReadyForExport(any(), any()))
				.thenReturn(dummyProcessList);
		when(ediOrderDao.getZeroDollarPhysicalOrderProcessesReadyForExport(any()))
				.thenReturn(dummyProcessList);
	}

	@Test
	public void getExportProcessesReadyForExportShouldFinishRejectedProcesses()
	{

		final WileyExportProcessModel settle = givenOrderExportProcess(order1, ExportProcessType.SETTLE);
		final WileyExportProcessModel refund = givenOrderExportProcess(order1, ExportProcessType.REFUND);
		givenRejectedProcesses(settle, refund);

		testInstance.getExportProcessesReadyForExport(store, EDIConstants.PAYMENT_TYPE_CREDIT_CARD);

		verify(testInstance).markOrdersExported(anyListOf(WileyExportProcessModel.class));
	}

	@Test
	public void getExportProcessesReadyForExportShouldNotFinishAnyProcessesIfNoRejected()
	{

		givenRejectedProcesses();

		testInstance.getExportProcessesReadyForExport(store, EDIConstants.PAYMENT_TYPE_CREDIT_CARD);

		verify(testInstance, never()).markOrdersExported(anyListOf(WileyExportProcessModel.class));
	}

	@Test
	public void getZeroDollarPhysicalPartnerOrderProcessesReadyForExportShouldReturnProcesses()
	{
		List<WileyExportProcessModel> processes = testInstance.getZeroDollarPhysicalPartnerOrderProcessesReadyForExport(store,
				EDIConstants.PAYMENT_TYPE_CREDIT_CARD);

		assertFalse(processes.isEmpty());
		assertEquals(order1, processes.get(0).getOrder());
	}

	@Test
	public void getZeroDollarPhysicalPartnerOrderProcessesReadyForExportShouldReturnEmptyList()
	{
		List<WileyExportProcessModel> processes = testInstance.getZeroDollarPhysicalPartnerOrderProcessesReadyForExport(store,
				EDIConstants.PAYMENT_TYPE_PAYPAL);

		assertTrue(processes.isEmpty());
	}

	private void givenRejectedProcesses(final WileyExportProcessModel... exportProcesses)
	{
		when(refundedTheSameDayProcessesValidationStrategy.getRejectedProcesses(any()))
				.thenReturn(Arrays.asList(exportProcesses));
	}

	private WileyExportProcessModel givenOrderExportProcess(final OrderModel order, final ExportProcessType exportProcessType)
	{
		final WileyExportProcessModel exportProcessModel = mock(WileyExportProcessModel.class);
		when(exportProcessModel.getOrder()).thenReturn(order);
		when(exportProcessModel.getExportType()).thenReturn(exportProcessType);
		return exportProcessModel;
	}


}
