package com.wiley.fulfilmentprocess.actions.eloqua;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.servicelayer.model.ModelService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.fulfilmentprocess.model.EloquaOrderExportProcessModel;
import com.wiley.core.integration.eloqua.EloquaGateway;
import com.wiley.core.integration.eloqua.dto.ContactDto;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


/**
 * Default unit test for {@link CreateEloquaContactAction}.
 *
 * @author Aliaksei_Zlobich
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CreateEloquaContactActionUnitTest
{

	@Mock
	private EloquaGateway eloquaGatewayMock;

	@Mock
	private ModelService modelServiceMock;

	@InjectMocks
	private CreateEloquaContactAction createEloquaContactAction;

	// Test data

	@Mock
	private EloquaOrderExportProcessModel eloquaOrderExportProcessModelMock;

	@Mock
	private OrderModel orderModelMock;

	@Mock
	private CustomerModel customerModelMock;

	@Mock
	private AddressModel addressModelMock;
	
	@Mock
	private AddressModel defaultPaymentAddressMock;

	@Mock
	private BaseSiteModel siteModelMock;

	// needs new boolean object to test if subscribeToUpdates status is gotten from order.
	private static final Boolean SUBSCRIBE_TO_UPDATES = new Boolean(true);

	@Before
	public void setUp() throws Exception
	{
		when(eloquaOrderExportProcessModelMock.getOrder()).thenReturn(orderModelMock);
		when(orderModelMock.getUser()).thenReturn(customerModelMock);
		when(orderModelMock.getSite()).thenReturn(siteModelMock);
		when(orderModelMock.getPaymentAddress()).thenReturn(addressModelMock);
		when(orderModelMock.getSubscribeToUpdates()).thenReturn(SUBSCRIBE_TO_UPDATES);
	}

	@Test
	public void shouldReturnOKAndSaveEloquaIdIfContactCreated() throws Exception
	{
		// Given
		final int contactId = 234;

		ContactDto contactDtoStub = new ContactDto();
		contactDtoStub.setId(contactId);

		when(eloquaGatewayMock
				.createEloquaContact(any(CustomerModel.class), any(AddressModel.class), anyBoolean(), any(BaseSiteModel.class)))
				.thenReturn(
						contactDtoStub);

		// When
		final AbstractSimpleDecisionAction.Transition transition = createEloquaContactAction.executeAction(
				eloquaOrderExportProcessModelMock);

		// Then
		assertEquals("Returned transition should be equal expected.", AbstractSimpleDecisionAction.Transition.OK, transition);
		verify(customerModelMock).setEloquaId(eq(contactId));
		verify(modelServiceMock).save(eq(customerModelMock));
	}

	@Test
	public void shouldReturnNOKAndDoNotChangeEloquaIdIfExceptionOccurred() throws Exception
	{
		// Given
		when(eloquaGatewayMock
				.createEloquaContact(any(CustomerModel.class), any(AddressModel.class), anyBoolean(), any(BaseSiteModel.class)))
				.thenThrow(
						Exception.class);

		// When
		final AbstractSimpleDecisionAction.Transition transition = createEloquaContactAction.executeAction(
				eloquaOrderExportProcessModelMock);

		// Then
		assertEquals("Returned transition should be equal expected.", AbstractSimpleDecisionAction.Transition.NOK, transition);
		verify(customerModelMock, never()).setEloquaId(anyInt());
		verify(modelServiceMock, never()).save(eq(customerModelMock));
	}

	@Test
	public void shouldSendExpectedParamsToEloquaGateway() throws Exception
	{
		// Given
		ContactDto contactDtoStub = new ContactDto();

		when(eloquaGatewayMock
				.createEloquaContact(any(CustomerModel.class), any(AddressModel.class), anyBoolean(), any(BaseSiteModel.class)))
				.thenReturn(
						contactDtoStub);

		// When
		createEloquaContactAction.executeAction(eloquaOrderExportProcessModelMock);

		// Then
		verify(eloquaGatewayMock).createEloquaContact(same(customerModelMock), same(addressModelMock), same(SUBSCRIBE_TO_UPDATES),
				same(siteModelMock));
	}
	
	@Test
	public void shouldUseDefaultAddressIfAbsent() throws Exception
	{
		// Given
		when(eloquaGatewayMock.createEloquaContact(any(CustomerModel.class), any(AddressModel.class), anyBoolean(),
				any(BaseSiteModel.class))).thenReturn(
				new ContactDto());
		doReturn(null).when(orderModelMock).getPaymentAddress();
		doReturn(defaultPaymentAddressMock).when(customerModelMock).getDefaultPaymentAddress();
		
		// Testing
		createEloquaContactAction.executeAction(eloquaOrderExportProcessModelMock);
	
		// Verify
		verify(eloquaGatewayMock).createEloquaContact(same(customerModelMock), eq(defaultPaymentAddressMock),
				same(SUBSCRIBE_TO_UPDATES), same(siteModelMock));
	}
}