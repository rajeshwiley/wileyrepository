package com.wiley.fulfilmentprocess.test;

import com.wiley.fulfilmentprocess.test.actions.TestActionTemp;
import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.Registry;
import de.hybris.platform.processengine.definition.ProcessDefinitionFactory;
import de.hybris.platform.processengine.enums.ProcessState;
import de.hybris.platform.processengine.impl.DefaultBusinessProcessService;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.processengine.model.ProcessTaskModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.spring.ctx.ScopeTenantIgnoreDocReader;
import de.hybris.platform.task.RetryLaterException;
import de.hybris.platform.task.TaskModel;
import de.hybris.platform.task.impl.DefaultTaskService;
import de.hybris.platform.testframework.HybrisJUnit4Test;
import de.hybris.platform.util.Utilities;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.io.ClassPathResource;

import java.util.Date;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.fail;


/**
 * Integration test to check export-order-to-eloqua process flow.
 *
 * @author Aliaksei_Zlobich
 */
@IntegrationTest
public class EloquaOrderExportProcessFlowTest extends HybrisJUnit4Test
{

	private static final Logger LOG = Logger.getLogger(EloquaOrderExportProcessFlowTest.class);

	private static TaskServiceStub taskServiceStub;

	private static DefaultBusinessProcessService processService;
	private static ProcessDefinitionFactory definitonFactory;
	private static ModelService modelService;

	/**
	 * Prepare.
	 *
	 * @throws Exception
	 * 		the exception
	 */
	@BeforeClass
	public static void prepare() throws Exception
	{
		Registry.activateStandaloneMode();
		Utilities.setJUnitTenant();
		LOG.debug("Preparing...");



		final ApplicationContext appCtx = Registry.getApplicationContext();

		assertTrue("Application context of type " + appCtx.getClass() + " is not a subclass of "
				+ ConfigurableApplicationContext.class, appCtx instanceof ConfigurableApplicationContext);

		final ConfigurableApplicationContext applicationContext = (ConfigurableApplicationContext) appCtx;
		final ConfigurableListableBeanFactory beanFactory = applicationContext.getBeanFactory();

		assertTrue("Bean Factory of type " + beanFactory.getClass() + " is not of type " + BeanDefinitionRegistry.class,
				beanFactory instanceof BeanDefinitionRegistry);

		final XmlBeanDefinitionReader xmlReader = new XmlBeanDefinitionReader((BeanDefinitionRegistry) beanFactory);
		xmlReader.setDocumentReaderClass(ScopeTenantIgnoreDocReader.class);
		xmlReader.loadBeanDefinitions(new ClassPathResource(
				"/wileyfulfilmentprocess/test/wileyfulfilmentprocess-spring-test.xml"));
		xmlReader.loadBeanDefinitions(new ClassPathResource(
				"/wileyfulfilmentprocess/test/process/export-order-to-eloqua-process-spring.xml"));

		modelService = (ModelService) getBean("modelService");
		processService = (DefaultBusinessProcessService) getBean("businessProcessService");
		definitonFactory = processService.getProcessDefinitionFactory();

		LOG.warn("Prepare Process Definition factory...");
		definitonFactory.add("classpath:/wileyfulfilmentprocess/process/export-order-to-eloqua-process.xml");

		taskServiceStub = appCtx.getBean(TaskServiceStub.class);
		processService.setTaskService(taskServiceStub);
	}

	/**
	 * Sets actions.
	 */
	@Before
	public void setupActions()
	{
		setResultForAction("eloquaProcessStartAction", "ELOQUA_ID_EXISTS");
		setResultForAction("searchForEloquaContactAction", "FOUND");
		setResultForAction("createEloquaContactAction", "OK");
		setResultForAction("updateEloquaContactAction", "OK");
		setResultForAction("createEloquaTransactionalRecordAction", "OK");
		setResultForAction("eloquaProcessEndAction", "OK");
		setResultForAction("eloquaFailedNotificationAction", "OK");
	}

	/**
	 * Remove process definitions.
	 */
	@AfterClass
	public static void removeProcessDefinitions()
	{
		LOG.debug("cleanup...");


		final ApplicationContext appCtx = Registry.getApplicationContext();

		assertTrue("Application context of type " + appCtx.getClass() + " is not a subclass of "
				+ ConfigurableApplicationContext.class, appCtx instanceof ConfigurableApplicationContext);

		final ConfigurableApplicationContext applicationContext = (ConfigurableApplicationContext) appCtx;
		final ConfigurableListableBeanFactory beanFactory = applicationContext.getBeanFactory();
		assertTrue("Bean Factory of type " + beanFactory.getClass() + " is not of type " + BeanDefinitionRegistry.class,
				beanFactory instanceof BeanDefinitionRegistry);
		final XmlBeanDefinitionReader xmlReader = new XmlBeanDefinitionReader((BeanDefinitionRegistry) beanFactory);
		xmlReader.loadBeanDefinitions(new ClassPathResource(
				"/wileyfulfilmentprocess/test/process/ags-order-process-spring-cleanup.xml"));
		xmlReader.loadBeanDefinitions(new ClassPathResource(
				"/wileyfulfilmentprocess/test/process/export-order-to-eloqua-process-springcleanup.xml"));

		processService.setTaskService(appCtx.getBean(DefaultTaskService.class));
		definitonFactory = null;
		processService = null;
	}

	/**
	 * Reset services.
	 */
	@After
	public void resetServices()
	{
		final List<TaskModel> tasks = taskServiceStub.cleanup();
		final StringBuffer msg = new StringBuffer();
		for (final TaskModel task : tasks)
		{
			final ProcessTaskModel processTask = (ProcessTaskModel) task;

			msg.append(processTask.getAction()).append(", ");
		}
	}


	/**
	 * Success flow.<br/>
	 * CustomerModel has eloquaId and sending order data to Eloqua was completed successfully.
	 *
	 * @throws InterruptedException
	 * 		the interrupted exception
	 */
	@Test
	public void testSuccessFlow1() throws InterruptedException
	{
		// Given
		// no changes in test data

		// When
		final BusinessProcessModel process = createProcess("export-order-to-eloqua-process");

		assertStep(process, "eloquaProcessStart");
		assertStep(process, "updateEloquaContact");
		assertStep(process, "createEloquaTransactionalRecord");
		assertStep(process, "eloquaProcessEnd");

		// Then
		Thread.sleep(1000);

		modelService.refresh(process);
		assertEquals("Process state", ProcessState.SUCCEEDED, process.getProcessState());
	}

	/**
	 * Success flow.<br/>
	 * CustomerModel doesn't have eloquaId and Contact doesn't exist in Eloqua.<br/>
	 * New Contact was created in Eloqua and sending order data to eloqua was completed successfully.
	 *
	 * @throws InterruptedException
	 * 		the interrupted exception
	 */
	@Test
	public void testSuccessFlow2() throws InterruptedException
	{
		// Given
		setResultForAction("eloquaProcessStartAction", "ELOQUA_ID_NOT_EXIST");
		setResultForAction("searchForEloquaContactAction", "NOT_FOUND");

		// When
		final BusinessProcessModel process = createProcess("export-order-to-eloqua-process");

		assertStep(process, "eloquaProcessStart");
		assertStep(process, "searchForEloquaContact");
		assertStep(process, "createEloquaContact");
		assertStep(process, "createEloquaTransactionalRecord");
		assertStep(process, "eloquaProcessEnd");

		// Then
		Thread.sleep(1000);

		modelService.refresh(process);
		assertEquals("Process state", ProcessState.SUCCEEDED, process.getProcessState());
	}

	/**
	 * Success flow.<br/>
	 * CustomerModel doesn't have eloquaId but Contact exists in Eloqua.<br/>
	 * Contact was updated in Eloqua and sending order data to eloqua was completed successfully.
	 *
	 * @throws InterruptedException
	 * 		the interrupted exception
	 */
	@Test
	public void testSuccessFlow3() throws InterruptedException
	{
		// Given
		setResultForAction("eloquaProcessStartAction", "ELOQUA_ID_NOT_EXIST");

		// When
		final BusinessProcessModel process = createProcess("export-order-to-eloqua-process");

		assertStep(process, "eloquaProcessStart");
		assertStep(process, "searchForEloquaContact");
		assertStep(process, "updateEloquaContact");
		assertStep(process, "createEloquaTransactionalRecord");
		assertStep(process, "eloquaProcessEnd");

		// Then
		Thread.sleep(1000);

		modelService.refresh(process);
		assertEquals("Process state", ProcessState.SUCCEEDED, process.getProcessState());
	}

	/**
	 * Failure flow.<br/>
	 * CustomerModel has eloquaId but sending order data to Eloqua was failed.
	 *
	 * @throws InterruptedException
	 * 		the interrupted exception
	 */
	@Test
	public void testFailureFlow1() throws InterruptedException
	{
		// Given
		setResultForAction("createEloquaTransactionalRecordAction", "NOK");

		// When
		final BusinessProcessModel process = createProcess("export-order-to-eloqua-process");

		assertStep(process, "eloquaProcessStart");
		assertStep(process, "updateEloquaContact");
		assertStep(process, "createEloquaTransactionalRecord");
		assertStep(process, "eloquaFailedNotification");

		// Then
		Thread.sleep(1000);

		modelService.refresh(process);
		assertEquals("Process state", ProcessState.ERROR, process.getProcessState());
	}

	/**
	 * Failure flow.<br/>
	 * CustomerModel doesn't have eloquaId and Contact doesn't exist in Eloqua.<br/>
	 * Creating of new Eloqua Contact was failed.
	 *
	 * @throws InterruptedException
	 * 		the interrupted exception
	 */
	@Test
	public void testFailureFlow2() throws InterruptedException
	{
		// Given
		setResultForAction("eloquaProcessStartAction", "ELOQUA_ID_NOT_EXIST");
		setResultForAction("searchForEloquaContactAction", "NOT_FOUND");
		setResultForAction("createEloquaContactAction", "NOK");

		// When
		final BusinessProcessModel process = createProcess("export-order-to-eloqua-process");

		assertStep(process, "eloquaProcessStart");
		assertStep(process, "searchForEloquaContact");
		assertStep(process, "createEloquaContact");
		assertStep(process, "eloquaFailedNotification");

		// Then
		Thread.sleep(1000);

		modelService.refresh(process);
		assertEquals("Process state", ProcessState.ERROR, process.getProcessState());
	}

	/**
	 * Failure flow.<br/>
	 * CustomerModel doesn't have eloquaId but Contact exists in Eloqua.<br/>
	 * Updating of Eloqua Contact was failed.
	 *
	 * @throws InterruptedException
	 * 		the interrupted exception
	 */
	@Test
	public void testFailureFlow3() throws InterruptedException
	{
		// Given
		setResultForAction("eloquaProcessStartAction", "ELOQUA_ID_NOT_EXIST");
		setResultForAction("updateEloquaContactAction", "NOK");

		// When
		final BusinessProcessModel process = createProcess("export-order-to-eloqua-process");

		assertStep(process, "eloquaProcessStart");
		assertStep(process, "searchForEloquaContact");
		assertStep(process, "updateEloquaContact");
		assertStep(process, "eloquaFailedNotification");

		// Then
		Thread.sleep(1000);

		modelService.refresh(process);
		assertEquals("Process state", ProcessState.ERROR, process.getProcessState());
	}

	/**
	 * Failure flow.<br/>
	 * CustomerModel doesn't have eloquaId and Contact doesn't exist in Eloqua.<br/>
	 * New Contact was created in Eloqua but sending order data to eloqua was failed.
	 *
	 * @throws InterruptedException
	 * 		the interrupted exception
	 */
	@Test
	public void testFailureFlow4() throws InterruptedException
	{
		// Given
		setResultForAction("eloquaProcessStartAction", "ELOQUA_ID_NOT_EXIST");
		setResultForAction("searchForEloquaContactAction", "NOT_FOUND");
		setResultForAction("createEloquaTransactionalRecordAction", "NOK");

		// When
		final BusinessProcessModel process = createProcess("export-order-to-eloqua-process");

		assertStep(process, "eloquaProcessStart");
		assertStep(process, "searchForEloquaContact");
		assertStep(process, "createEloquaContact");
		assertStep(process, "createEloquaTransactionalRecord");
		assertStep(process, "eloquaFailedNotification");

		// Then
		Thread.sleep(1000);

		modelService.refresh(process);
		assertEquals("Process state", ProcessState.ERROR, process.getProcessState());
	}

	/**
	 * Failure flow.<br/>
	 * CustomerModel doesn't have eloquaId but Contact exists in Eloqua.<br/>
	 * Contact was updated in Eloqua but sending order data to eloqua was failed.
	 *
	 * @throws InterruptedException
	 * 		the interrupted exception
	 */
	@Test
	public void testFailureFlow5() throws InterruptedException
	{
		// Given
		setResultForAction("eloquaProcessStartAction", "ELOQUA_ID_NOT_EXIST");
		setResultForAction("createEloquaTransactionalRecordAction", "NOK");

		// When
		final BusinessProcessModel process = createProcess("export-order-to-eloqua-process");

		assertStep(process, "eloquaProcessStart");
		assertStep(process, "searchForEloquaContact");
		assertStep(process, "updateEloquaContact");
		assertStep(process, "createEloquaTransactionalRecord");
		assertStep(process, "eloquaFailedNotification");

		// Then
		Thread.sleep(1000);

		modelService.refresh(process);
		assertEquals("Process state", ProcessState.ERROR, process.getProcessState());
	}

	/**
	 * Failure flow.<br/>
	 * CustomerModel doesn't have eloquaId and searching of contact in Eloqua was failed.
	 *
	 * @throws InterruptedException
	 * 		the interrupted exception
	 */
	@Test
	public void testFailureFlow6() throws InterruptedException
	{
		// Given
		setResultForAction("eloquaProcessStartAction", "ELOQUA_ID_NOT_EXIST");
		setResultForAction("searchForEloquaContactAction", "FAILURE");

		// When
		final BusinessProcessModel process = createProcess("export-order-to-eloqua-process");

		assertStep(process, "eloquaProcessStart");
		assertStep(process, "searchForEloquaContact");
		assertStep(process, "eloquaFailedNotification");

		// Then
		Thread.sleep(1000);

		modelService.refresh(process);
		assertEquals("Process state", ProcessState.ERROR, process.getProcessState());
	}

	/**
	 * Create process business process model.
	 *
	 * @param processName
	 * 		the process name
	 * @return the business process model
	 */
	protected BusinessProcessModel createProcess(final String processName)
	{
		final String id = "Test" + (new Date()).getTime();
		final BusinessProcessModel process = processService.startProcess(id, processName);
		assertProcessState(process, ProcessState.RUNNING);
		modelService.save(process);
		return process;
	}

	/**
	 * Sets result for action.
	 *
	 * @param action
	 * 		the action
	 * @param result
	 * 		the result
	 */
	protected void setResultForAction(final String action, final String result)
	{
		final TestActionTemp tmp = (TestActionTemp) getBean(action);
		tmp.setResult(result);
	}

	/**
	 * Assert step.
	 *
	 * @param process
	 * 		the process
	 * @param bean
	 * 		the bean
	 * @throws InterruptedException
	 * 		the interrupted exception
	 */
	protected void assertStep(final BusinessProcessModel process, final String bean) throws InterruptedException
	{
		LOG.info("assertStep action = " + bean);

		try
		{
			final ProcessTaskModel processTaskModel = taskServiceStub.runProcessTask(bean);

			if (processTaskModel == null)
			{
				final StringBuffer found = new StringBuffer();

				for (final TaskModel task : taskServiceStub.getTasks())
				{
					if (task instanceof ProcessTaskModel)
					{
						found.append(((ProcessTaskModel) task).getAction()).append("; ");
					}
				}

				assertNotNull("No task found for bean " + bean + ", action(s): " + found, processTaskModel);
			}


		}
		catch (final RetryLaterException e)
		{
			fail(e.toString());
		}

	}

	/**
	 * Gets bean.
	 *
	 * @param name
	 * 		the name
	 * @return the bean
	 */
	protected static Object getBean(final String name)
	{
		return Registry.getApplicationContext().getBean(name);
	}

	/**
	 * Assert process state.
	 *
	 * @param process
	 * 		the process
	 * @param state
	 * 		the state
	 */
	protected void assertProcessState(final BusinessProcessModel process, final ProcessState state)
	{
		modelService.refresh(process);
		assertEquals("Process state", state, process.getState());
	}
}
