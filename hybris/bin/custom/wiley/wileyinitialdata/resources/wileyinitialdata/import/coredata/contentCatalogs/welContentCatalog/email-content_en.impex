#
# Import the CMS content for the WEL site emails
#
$contentCatalog = welContentCatalog
$contentCV = catalogVersion(CatalogVersion.catalog(Catalog.id[default = $contentCatalog]), CatalogVersion.version[default = Staged])[default = $contentCatalog:Staged]

# Import config properties into impex macros for modulegen
UPDATE GenericItem[processor = de.hybris.platform.commerceservices.impex.impl.ConfigPropertyImportProcessor]; pk[unique = true]
$emailResource = $config-emailResourceValue
$defaultFromEmail = customerService@wiley.com
#% if: (!("$config-wiley.from.email".isEmpty()) && !("$config-wiley.from.email".contains("config-wiley.from.email")))
$defaultFromEmail = $config-wiley.from.email
#% endif:

# Language
$lang = en

# CMS components and Email velocity templates
UPDATE RendererTemplate; code[unique = true]; description[lang = $lang]; templateScript[lang = $lang, translator = de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]
; wel-BannerComponent-template                      ; "CMSBannerComponent Template"                      ; $emailResource/email-bannerComponentTemplate.vm
; wel-CMSImageComponent-template                    ; "CMSImageComponent Template"                       ; $emailResource/email-cmsImageComponentTemplate.vm
; wel-CMSLinkComponent-template                     ; "CMSLinkComponent Template"                        ; $emailResource/email-cmsLinkComponentTemplate.vm
; wel-CMSParagraphComponent-template                ; "CMSParagraphComponent Template"                   ; $emailResource/email-cmsParagraphComponentTemplate.vm
; wel-SimpleBannerComponent-template                ; "CMSSimpleBannerComponent Template"                ; $emailResource/email-bannerComponentTemplate.vm
; wel_Email_Customer_Registration_Body              ; "Customer Registration Email Body"                 ; $emailResource/email-customerRegistrationBody.vm
; wel_Email_Customer_Registration_Subject           ; "Customer Registration Email Subject"              ; $emailResource/email-customerRegistrationSubject.vm
; wel_Email_Delivery_Sent_Body                      ; "Delivery Sent Email Body"                         ; $emailResource/email-deliverySentBody.vm
; wel_Email_Delivery_Sent_Subject                   ; "Delivery Sent Email Subject"                      ; $emailResource/email-deliverySentSubject.vm
; wel_Email_Forgotten_Password_Body                 ; "Forgotten Password Email Body"                    ; $emailResource/email-wel-forgottenPasswordBody.vm
; wel_Email_Forgotten_Password_Subject              ; "Forgotten Password Email Subject"                 ; $emailResource/email-forgottenPasswordSubject.vm
; wel_Email_NotPickedUpConsignment_Canceled_Body    ; "Not Picked Up Consignment Canceled Email Body"    ; $emailResource/email-notPickedUpConsignmentCanceledBody.vm
; wel_Email_NotPickedUpConsignment_Canceled_Subject ; "Not Picked Up Consignment Canceled Email Subject" ; $emailResource/email-notPickedUpConsignmentCanceledSubject.vm
; wel_Email_Order_Cancelled_Body                    ; "Order Cancelled Email Body"                       ; $emailResource/email-orderCancelledBody.vm
; wel_Email_Order_Cancelled_Subject                 ; "Order Cancelled Email Subject"                    ; $emailResource/email-orderCancelledSubject.vm
; wel_Email_Order_Collection_Reminder_Body          ; "Order Collection Reminder Body"                   ; $emailResource/email-orderCollectionReminderBody.vm
; wel_Email_Order_Collection_Reminder_Subject       ; "Order Collection Reminder Subject"                ; $emailResource/email-orderCollectionReminderSubject.vm
; wel_Email_Order_Confirmation_Body                 ; "Order Confirmation Email Body"                    ; $emailResource/email-wel-orderConfirmationBody.vm
; wel_Email_Order_Confirmation_Subject              ; "Order Confirmation Email Subject"                 ; $emailResource/email-wel-orderConfirmationSubject.vm
; wel_Email_Order_Move_To_CS_Body                   ; "Order Move To CS Body"                            ; $emailResource/email-orderMoveToCsBody.vm
; wel_Email_Order_Move_To_CS_Subject                ; "Order Move To CS Subject"                         ; $emailResource/email-orderMoveToCsSubject.vm
; wel_Email_Order_Partially_Canceled_Body           ; "Order Partially Canceled Email Body"              ; $emailResource/email-orderPartiallyCanceledBody.vm
; wel_Email_Order_Partially_Canceled_Subject        ; "Order Partially Canceled Email Subject"           ; $emailResource/email-orderPartiallyCanceledSubject.vm
; wel_Email_Order_Partially_Refunded_Body           ; "Order Partially Refunded Email Body"              ; $emailResource/email-orderPartiallyRefundedBody.vm
; wel_Email_Order_Partially_Refunded_Subject        ; "Order Partially Refunded Email Subject"           ; $emailResource/email-orderPartiallyRefundedSubject.vm
; wel_Email_Order_Refund_Body                       ; "Order Refund Email Body"                          ; $emailResource/email-wel-orderRefundBody.vm
; wel_Email_Order_Refund_Subject                    ; "Order Refund Email Subject"                       ; $emailResource/email-orderRefundSubject.vm
; wel_Email_Ready_For_Pickup_Body                   ; "Ready For Pickup Email Body"                      ; $emailResource/email-readyForPickupBody.vm
; wel_Email_Ready_For_Pickup_Subject                ; "Ready For Pickup Email Subject"                   ; $emailResource/email-readyForPickupSubject.vm
; wel_Email_Abandon_Cart_Subject                    ; "Abandon Cart Subject"                             ; $emailResource/email-wel-abandonCartSubject.vm
; wel_Email_Abandon_Cart_Body                       ; "Abandon Cart Body"                                ; $emailResource/email-wel-abandonCartBody.vm
; wel_Email_Failed_Order_Subject                    ; "Failed Order Subject"                             ; $emailResource/email-wel-failedOrderSubject.vm
; wel_Email_Failed_Order_Body                       ; "Failed Order Body"                                ; $emailResource/email-wel-failedOrderBody.vm
; wel_Email_Pre_Order_Placement_Subject             ; "Pre Order Placement Subject"                      ; $emailResource/email-wel-preOrderPlacementSubject.vm
; wel_Email_Pre_Order_Placement_Body                ; "Pre Order Placement Body"                         ; $emailResource/email-wel-preOrderPlacementBody.vm

# Email Pages
UPDATE EmailPage; $contentCV[unique = true]; uid[unique = true]; fromEmail[lang = $lang]; fromName[lang = $lang]
; ; CustomerRegistrationEmail    ; "customerservices@hybris.com" ; "Customer Services Team"
; ; DeliverySentEmail            ; "customerservices@hybris.com" ; "Customer Services Team"
; ; ForgottenPasswordEmail       ; $defaultFromEmail             ; "Wiley Customer Service"
; ; OrderCancelledEmail          ; "customerservices@hybris.com" ; "Customer Services Team"
; ; OrderCollectionReminderEmail ; "customerservices@hybris.com" ; "Customer Services Team"
; ; OrderConfirmationEmail       ;  $defaultFromEmail            ; "Wiley Customer Service"
; ; OrderConfirmationEmail       ; $defaultFromEmail             ; "Wiley Customer Service"
; ; OrderMoveToCsEmail           ; "customerservices@hybris.com" ; "Customer Services Team"
; ; OrderPartiallyCanceledEmail  ; "customerservices@hybris.com" ; "Customer Services Team"
; ; OrderPartiallyRefundedEmail  ; "customerservices@hybris.com" ; "Customer Services Team"
; ; OrderRefundEmail             ; $defaultFromEmail             ; "Wiley Customer Service"
; ; ReadyForPickupEmail          ; "customerservices@hybris.com" ; "Customer Services Team"
; ; AbandonCartEmail             ;  $defaultFromEmail            ; "Wiley Customer Service"
; ; AbandonCartEmail             ; $defaultFromEmail             ; "Wiley Customer Service"
; ; OrderHasBeenFailedEmail      ; $defaultFromEmail             ; "Wiley Customer Service"
; ; PreOrderPlacementEmail       ; $defaultFromEmail             ; "Wiley Customer Service"
