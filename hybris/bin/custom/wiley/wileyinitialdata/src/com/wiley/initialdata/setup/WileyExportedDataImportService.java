package com.wiley.initialdata.setup;

import de.hybris.platform.commerceservices.dataimport.AbstractDataImportService;
import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.commerceservices.setup.data.ImportData;
import de.hybris.platform.core.initialization.SystemSetupContext;

import java.util.List;


/**
 * Overridden to load site specific impexes during init/update
 */
public class WileyExportedDataImportService extends AbstractDataImportService
{
	public static final String IMPORT_EXPORTED_DATA = "importExportedData";
	public static final String EXPORTED_DATA = "exporteddata";

	@Override
	public void execute(final AbstractSystemSetup systemSetup, final SystemSetupContext context,
			final List<ImportData> importData)
	{
		final boolean importSampleData = systemSetup.getBooleanSystemSetupParameter(context, IMPORT_EXPORTED_DATA);

		if (importSampleData)
		{
			for (final ImportData data : importData)
			{
				importAllData(systemSetup, context, data, true);
			}
		}
	}

	@Override
	protected void importCommonData(final String extensionName)
	{
		getSetupImpexService().importImpexFile(
				String.format("/%s/import/" + EXPORTED_DATA + "/common/importscript.impex", extensionName), false);
	}

	@Override
	protected void importProductCatalog(final String extensionName, final String productCatalogName)
	{
		getSetupImpexService().importImpexFile(
				String.format("/%s/import/" + EXPORTED_DATA + "/productCatalogs/%sProductCatalog/importscript.impex",
						extensionName,
						productCatalogName), false);
	}

	@Override
	protected void importContentCatalog(final String extensionName, final String contentCatalogName)
	{
		getSetupImpexService().importImpexFile(
				String.format("/%s/import/" + EXPORTED_DATA + "/contentCatalogs/%sContentCatalog/importscript.impex",
						extensionName, contentCatalogName), false);
	}

	@Override
	protected void importStore(final String extensionName, final String storeName, final String productCatalogName)
	{
		// TODO: implement if required
	}

	@Override
	protected void importJobs(final String extensionName, final String storeName)
	{
		// TODO: implement if required
	}

	@Override
	protected void importSolrIndex(final String extensionName, final String storeName)
	{
		// TODO: implement if required
	}
}