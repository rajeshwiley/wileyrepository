package com.wiley.initialdata.setup;

import de.hybris.platform.commerceservices.dataimport.impl.SampleDataImportService;
import de.hybris.platform.core.initialization.SystemSetupContext;

import java.util.Map;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;

import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Value;

import com.google.common.collect.ImmutableMap;


/**
 * Overridden to load site specific impexes during init/update
 */
public class WileySiteAwareSampleDataImportService extends SampleDataImportService
{
	@Value("${wileyinitialdata.site.ags}")
	private String agsSiteName;

	@Value("${wileyinitialdata.site.wel}")
	private String welSiteName;

	private static final String[] AGS_SPECIFIC = {
			"/%s/import/sampledata/productCatalogs/%sProductCatalog/billing-plans.impex",
			"/%s/import/sampledata/productCatalogs/%sProductCatalog/subscription-terms.impex",
			"/%s/import/sampledata/productCatalogs/%sProductCatalog/subscription-categories.impex",
			"/%s/import/sampledata/productCatalogs/%sProductCatalog/subscription-products.impex",
			"/%s/import/sampledata/productCatalogs/%sProductCatalog/subscription-products-price.impex"
	};

	private static final String[] WEL_SPECIFIC = {
		"/%s/import/sampledata/productCatalogs/%sProductCatalog/partners.impex"
	};

	private static final String[] AGS_STORE_SPECIFIC = {
			"/%s/import/sampledata/stores/%s/test-cart.impex"
	};

	private static final String[] WEL_STORE_SPECIFIC = {
			
	};

	private Map<String, String[]> siteSpecificProductImpex;
	private Map<String, String[]> siteSpecificStoreImpex;

	public void importCommerceOrgData(final SystemSetupContext context)
	{
		final String extensionName = context.getExtensionName();

		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/commerceorg/user-groups.impex", extensionName), false);
	}

	@Override
	/** The assumption is productCatalogName equals to siteName */
	protected void importProductCatalog(final String extensionName, final String productCatalogName)
	{
		super.importProductCatalog(extensionName, productCatalogName);

		if (MapUtils.isNotEmpty(siteSpecificProductImpex) && siteSpecificProductImpex.containsKey(productCatalogName))
		{
			importSiteSpecificData(extensionName, siteSpecificProductImpex.get(productCatalogName), productCatalogName);
		}

		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/productCatalogs/%sProductCatalog/products-discounts.impex", extensionName,
						productCatalogName), false);
	}

	@Override
	/** The assumption is productCatalogName equals to siteName */
	protected void importStore(final String extensionName, final String storeName, final String productCatalogName)
	{
		super.importStore(extensionName, storeName, productCatalogName);
		if (MapUtils.isNotEmpty(siteSpecificStoreImpex) && siteSpecificStoreImpex.containsKey(productCatalogName))
		{
			importSiteSpecificData(extensionName, siteSpecificStoreImpex.get(productCatalogName), storeName);
		}
	}

	private void importSiteSpecificData(final String extensionName, @NotNull final String[] impexNames, final String
			siteSpecificKey)
	{
		for (String impexName : impexNames)
		{
			getSetupImpexService().importImpexFile(
					String.format(impexName, extensionName, siteSpecificKey), false);
		}
	}

	@PostConstruct
	private void afterSetProperties()
	{
		siteSpecificProductImpex = ImmutableMap.of(agsSiteName, AGS_SPECIFIC, welSiteName, WEL_SPECIFIC);
		siteSpecificStoreImpex = ImmutableMap.of(agsSiteName, AGS_STORE_SPECIFIC, welSiteName, WEL_STORE_SPECIFIC);
	}
}