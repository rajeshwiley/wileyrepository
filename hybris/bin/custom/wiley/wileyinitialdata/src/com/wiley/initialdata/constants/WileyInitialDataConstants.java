/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.initialdata.constants;

/**
 * Global class for all WileyInitialData constants.
 */
public final class WileyInitialDataConstants extends GeneratedWileyInitialDataConstants
{
	/**
	 * The constant EXTENSIONNAME.
	 */
	public static final String EXTENSIONNAME = "wileyinitialdata";

	private WileyInitialDataConstants()
	{
		//empty
	}
}
