/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package com.wiley.initialdata.setup.events;

import de.hybris.platform.commerceservices.setup.data.ImportData;
import de.hybris.platform.commerceservices.setup.events.AbstractDataImportEvent;
import de.hybris.platform.core.initialization.SystemSetupContext;

import java.util.List;


/**
 * Event fired after exported data has been imported
 */
public class ExportedDataImportedEvent extends AbstractDataImportEvent
{
	public ExportedDataImportedEvent(final SystemSetupContext context, final List<ImportData> importData)
	{
		super(context, importData);
	}
}
