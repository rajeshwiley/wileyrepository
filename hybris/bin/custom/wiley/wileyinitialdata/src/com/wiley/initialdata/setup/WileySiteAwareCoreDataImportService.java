package com.wiley.initialdata.setup;

import de.hybris.platform.commerceservices.dataimport.impl.CoreDataImportService;

import java.util.Map;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;

import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Value;

import com.google.common.collect.ImmutableMap;


public class WileySiteAwareCoreDataImportService extends CoreDataImportService
{

	@Value("${wileyinitialdata.site.ags}")
	private String agsSiteName;


	private static final String[] AGS_SPECIFIC = {
			"/%s/import/coredata/productCatalogs/%sProductCatalog/subscription-core-data.impex"
	};

	private Map<String, String[]> siteSpecificProductImpex;

	@Override
	/** The assumption is productCatalogName equals to siteName */
	protected void importProductCatalog(final String extensionName, final String productCatalogName)
	{
		super.importProductCatalog(extensionName, productCatalogName);

		if (MapUtils.isNotEmpty(siteSpecificProductImpex) && siteSpecificProductImpex.containsKey(productCatalogName))
		{
			importSiteSpecificData(extensionName, siteSpecificProductImpex.get(productCatalogName), productCatalogName);
		}

	}

	/**
	 * This method is called only if siteSpecificData is not empty
	 */
	private void importSiteSpecificData(final String extensionName, @NotNull final String[] impexesNames, final String
			productCatalogName)
	{
		for (final String impexName : impexesNames)
		{
			getSetupImpexService().importImpexFile(
					String.format(impexName, extensionName, productCatalogName), false);
		}
	}

	@Override
	protected void importCommonData(final String extensionName)
	{
		super.importCommonData(extensionName);
		getSetupImpexService().importImpexFile(String.format("/%s/import/coredata/common/customers.impex", extensionName),
				true);
		getSetupImpexService().importImpexFile(
				String.format("/%s/import/coredata/common/projectdata-templates.impex", extensionName),
				true);
	}

	@PostConstruct
	private void afterSetProperties()
	{
		siteSpecificProductImpex = ImmutableMap.of(agsSiteName, AGS_SPECIFIC);
	}

}
