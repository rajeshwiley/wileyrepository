/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.initialdata.setup;

import de.hybris.platform.commerceservices.dataimport.impl.CoreDataImportService;
import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.commerceservices.setup.data.ImportData;
import de.hybris.platform.commerceservices.setup.events.CoreDataImportedEvent;
import de.hybris.platform.commerceservices.setup.events.SampleDataImportedEvent;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetup.Process;
import de.hybris.platform.core.initialization.SystemSetup.Type;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;
import de.hybris.platform.core.initialization.SystemSetupParameterMethod;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.beans.factory.annotation.Value;

import com.wiley.initialdata.constants.WileyInitialDataConstants;
import com.wiley.initialdata.setup.events.ExportedDataImportedEvent;


/**
 * This class provides hooks into the system's initialization and update processes.
 *
 * @see "https://wiki.hybris.com/display/release4/Hooks+for+Initialization+and+Update+Process"
 */
@SystemSetup(extension = WileyInitialDataConstants.EXTENSIONNAME)
public class InitialDataSystemSetup extends AbstractSystemSetup
{
	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(InitialDataSystemSetup.class);

	/**
	 * The constant SITES.
	 */
	@Value("${wileyinitialdata.sites}")
	private String listOfSites;

	private static final String IMPORT_SITE_DATA_PREFIX = "site_";
	private static final String IMPORT_CORE_DATA = "importCoreData";
	private static final String IMPORT_SAMPLE_DATA = "importSampleData";
	private static final String IMPORT_EXPORTED_DATA = "importExportedData";
	private static final String ACTIVATE_SOLR_CRON_JOBS = "activateSolrCronJobs";

	private CoreDataImportService coreDataImportService;
	private WileySiteAwareSampleDataImportService sampleDataImportService;
	private WileyExportedDataImportService exportedDataImportService;

	/**
	 * Generates the Dropdown and Multi-select boxes for the project data import
	 *
	 * @return the initialization options
	 */
	@Override
	@SystemSetupParameterMethod
	public List<SystemSetupParameter> getInitializationOptions()
	{
		final List<SystemSetupParameter> params = new ArrayList<SystemSetupParameter>();

		// Parameters to choose which site should be updated
		String[] sites = StringUtils.split(listOfSites, ',');
		for (String site : sites)
		{
			params.add(createBooleanSystemSetupParameter(IMPORT_SITE_DATA_PREFIX + site, site, true));
		}

		// Parameters to choose what kind of data should be updated
		params.add(createBooleanSystemSetupParameter(IMPORT_CORE_DATA, "Import Core Data", true));
		params.add(createBooleanSystemSetupParameter(IMPORT_SAMPLE_DATA, "Import Sample Data", true));
		params.add(createBooleanSystemSetupParameter(IMPORT_EXPORTED_DATA, "Import Exported Data", true));
		params.add(createBooleanSystemSetupParameter(ACTIVATE_SOLR_CRON_JOBS, "Activate Solr Cron Jobs", false));
		// Add more Parameters here as you require

		return params;
	}

	/**
	 * Implement this method to create initial objects. This method will be called by system creator during
	 * initialization and system update. Be sure that this method can be called repeatedly.
	 *
	 * @param context
	 * 		the context provides the selected parameters and values
	 */
	@SystemSetup(type = Type.ESSENTIAL, process = Process.ALL)
	public void createEssentialData(final SystemSetupContext context)
	{
		// Add Essential Data here as you require
	}

	/**
	 * Implement this method to create data that is used in your project. This method will be called during the system
	 * initialization.
	 *
	 * @param context
	 * 		the context provides the selected parameters and values
	 */
	@SystemSetup(type = Type.PROJECT, process = Process.ALL)
	public void createProjectData(final SystemSetupContext context)
	{
		final List<ImportData> importData = new ArrayList<ImportData>();

		String[] sites = StringUtils.split(listOfSites, ',');

		for (String site : sites)
		{
			boolean doImportSite = getBooleanSystemSetupParameter(context, IMPORT_SITE_DATA_PREFIX + site);
			if (doImportSite)
			{
				final ImportData wileyImportData = new ImportData();
				wileyImportData.setProductCatalogName(site);
				wileyImportData.setContentCatalogNames(Arrays.asList(site));
				wileyImportData.setStoreNames(Arrays.asList(site));
				importData.add(wileyImportData);
			}
		}

		getCoreDataImportService().execute(this, context, importData);
		getEventService().publishEvent(new CoreDataImportedEvent(context, importData));

		getSampleDataImportService().execute(this, context, importData);
		getSampleDataImportService().importCommerceOrgData(context);
		getEventService().publishEvent(new SampleDataImportedEvent(context, importData));

		getExportedDataImportService().execute(this, context, importData);
		getEventService().publishEvent(new ExportedDataImportedEvent(context, importData));
	}

	/**
	 * Gets core data import service.
	 *
	 * @return the core data import service
	 */
	public CoreDataImportService getCoreDataImportService()
	{
		return coreDataImportService;
	}

	/**
	 * Sets core data import service.
	 *
	 * @param coreDataImportService
	 * 		the core data import service
	 */
	@Required
	public void setCoreDataImportService(final CoreDataImportService coreDataImportService)
	{
		this.coreDataImportService = coreDataImportService;
	}

	/**
	 * Gets sample data import service.
	 *
	 * @return the sample data import service
	 */
	public WileySiteAwareSampleDataImportService getSampleDataImportService()
	{
		return sampleDataImportService;
	}

	/**
	 * Sets sample data import service.
	 *
	 * @param sampleDataImportService
	 * 		the sample data import service
	 */
	@Required
	public void setSampleDataImportService(final WileySiteAwareSampleDataImportService sampleDataImportService)
	{
		this.sampleDataImportService = sampleDataImportService;
	}

	/**
	 * Gets exported data import service.
	 *
	 * @return the exported data import service
	 */
	public WileyExportedDataImportService getExportedDataImportService()
	{
		return exportedDataImportService;
	}

	/**
	 * Sets exported data import service.
	 *
	 * @param exportedDataImportService
	 * 		the exported data import service
	 */
	@Required
	public void setExportedDataImportService(final WileyExportedDataImportService exportedDataImportService)
	{
		this.exportedDataImportService = exportedDataImportService;
	}
}
