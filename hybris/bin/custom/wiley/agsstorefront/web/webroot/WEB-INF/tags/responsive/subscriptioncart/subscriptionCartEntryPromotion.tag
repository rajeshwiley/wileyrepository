<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>
<%@ attribute name="cartEntry" required="true" type="de.hybris.platform.commercefacades.order.data.OrderEntryData" %>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<c:if test="${ycommerce:doesPotentialPromotionExistForOrderEntry(cartData, entry.entryNumber)}">
    <c:forEach items="${cartData.potentialProductPromotions}" var="promotion">
        <c:set var="displayed" value="false"/>
        <c:forEach items="${promotion.consumedEntries}" var="consumedEntry">
            <c:if test="${not displayed && consumedEntry.orderEntryNumber == entry.entryNumber && not empty promotion.description}">
                <c:set var="displayed" value="true"/>

                <div class="promo">
                    <ycommerce:testId code="cart_potentialPromotion_label">
                        ${promotion.description}
                    </ycommerce:testId>
                </div>
            </c:if>
        </c:forEach>
    </c:forEach>
</c:if>

<c:if test="${ycommerce:doesAppliedPromotionExistForOrderEntry(cartData, cartEntry.entryNumber)}">
    <c:forEach items="${cartData.appliedProductPromotions}" var="promotion">
        <c:set var="displayed" value="false"/>
        <c:forEach items="${promotion.consumedEntries}" var="consumedEntry">
            <c:if test="${not displayed && consumedEntry.orderEntryNumber == cartEntry.entryNumber}">
                <c:set var="displayed" value="true"/>
                <div class="promo">
                    <ycommerce:testId code="cart_appliedPromotion_label">
                        ${promotion.description}
                    </ycommerce:testId>
                </div>
            </c:if>
        </c:forEach>
    </c:forEach>
</c:if>