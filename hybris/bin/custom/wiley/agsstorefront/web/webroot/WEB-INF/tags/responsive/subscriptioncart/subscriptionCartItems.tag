<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="wileycommerce" uri="http://hybris.com/tld/wileycommercetags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="storepickup" tagdir="/WEB-INF/tags/responsive/storepickup" %>
<%@ taglib prefix="subscriptioncart" tagdir="/WEB-INF/tags/responsive/subscriptioncart" %>



<div class="subscriptions-container">
    <div class="subscriptions-header">
        <div class="subscriptions-header-item item-item">
            <spring:theme code="basket.page.title" />
        </div>
        <div class="subscriptions-header-item item-price">
            <spring:theme code="basket.page.price" />
        </div>
        <div class="subscriptions-header-item item-qty">
            <spring:theme code="basket.page.qty" />
        </div>
        <div class="subscriptions-header-item item-total">
            <spring:theme code="basket.page.total" />
        </div>
        <div class="subscriptions-header-item item-action"></div>
    </div>

    <div class="subscriptions-content">
        <c:forEach items="${cartData.entries}" var="entry">
            <c:url value="${entry.product.externalUrl}" var="productUrl"/>
            <c:set value="${wileycommerce:getEntryForBillingEvent(entry,'paynow')}" var="payNowPrice"/>
            <div class="subscription">
                <div class="subscription-info">
                    <h3 class="subscription-title">
                        <ycommerce:testId code="cart_product_name">
                            <a href="${productUrl}" class="subscription-title-link">${entry.product.name}</a>
                        </ycommerce:testId>
                    </h3>
                    <div class="subscription-description">${entry.product.description}</div>
                    <subscriptioncart:subscriptionCartEntryPromotion cartData="${cartData}" cartEntry="${entry}"/>
                </div>
                <div class="subscription-price-item item-action item-action-mobile">
                    <div class="subscription-price-name"></div>
                    <div class="subscription-price-value">
                        <button type="button" class="link remove-link remove-entry-button" id="removeEntry_${entry.entryNumber}"><spring:theme code="basket.page.remove"/></button>
                    </div>
                </div>
                  <div class="subscription-price-item item-price">
                    <div class="subscription-price-name"><spring:theme code="basket.page.price"/>:</div>
                    <c:if test="${not empty payNowPrice}">
                        <div class="subscription-price-value"><format:price priceData="${payNowPrice.basePrice}" displayFreeForZero="true"/></div>
                    </c:if>
                  </div>
                  <div class="subscription-price-item item-qty">
                    <div class="subscription-price-name"><spring:theme code="basket.page.qty"/>:</div>
                    <div class="subscription-price-value">${entry.quantity}</div>
                  </div>
                  <div class="subscription-price-item item-total">
                    <div class="subscription-price-name"><spring:theme code="basket.page.total"/>:</div>
                    <c:if test="${not empty payNowPrice}">
                        <div class="subscription-price-value"><format:price priceData="${payNowPrice.totalPrice}" displayFreeForZero="true"/></div>
                    </c:if>
                  </div>
                  <div class="subscription-price-item item-action">
                    <div class="subscription-price-name"></div>
                    <div class="subscription-price-value">
                        <c:if test="${entry.updateable}" >
                        <ycommerce:testId code="cart_product_removeProduct">
                              <button type="button" class="link remove-link remove-item remove-entry-button"  id="removeEntry_${entry.entryNumber}"><spring:theme code="basket.page.remove"/></button>
                        </ycommerce:testId>
                             <c:url value="/cart/update" var="cartUpdateFormAction" />
                             <form:form id="updateCartForm${entry.entryNumber}" action="${cartUpdateFormAction}" method="post" commandName="updateQuantityForm${entry.entryNumber}">
                                 <input type="hidden" name="entryNumber" value="${entry.entryNumber}"/>
                                 <input type="hidden" name="productCode" value="${entry.product.code}"/>
                                 <input type="hidden" name="initialQuantity" value="${entry.quantity}"/>
                                 <form:input cssClass="form-control update-entry-quantity-input" type="hidden" size="1" id="quantity_${entry.entryNumber}" path="quantity" />
                             </form:form>

                        </c:if>
                    </div>
                  </div>
            </div>
        </c:forEach>
    </div>
</div>
<storepickup:pickupStorePopup />
