<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="step" required="false" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<ul class="progress-line">
  <li class="step  ${1 <= step ? 'step-completed':'none'}">
    <span class="step-number"><spring:theme code="checkout.multi.breadcrumb.step.1"/></span>
    <span class="step-name step-name-desktop"><spring:theme code="checkout.multi.breadcrumb.create.account.or.login"/></span>
    <span class="step-name step-name-mobile"><spring:theme code="checkout.multi.breadcrumb.log.in"/></span>
  </li>
  <li class="step  ${2 <= step ? 'step-completed':'none'}">
    <span class="step-number"><spring:theme code="checkout.multi.breadcrumb.step.2"/></span>
    <span class="step-name"><spring:theme code="checkout.multi.breadcrumb.billing"/></span>
  </li>
  <li class="step  ${3 <= step ? 'step-completed':'none'}">
    <span class="step-number"><spring:theme code="checkout.multi.breadcrumb.step.3"/></span>
    <span class="step-name"><spring:theme code="checkout.multi.breadcrumb.payment"/></span>
  </li>
  <li class="step  ${4 <= step ? 'step-completed':'none'}">
    <span class="step-number"><spring:theme code="checkout.multi.breadcrumb.step.4"/></span>
    <span class="step-name"><spring:theme code="checkout.multi.breadcrumb.order.confirmation"/></span>
  </li>
</ul>