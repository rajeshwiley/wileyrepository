<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:if test="${not empty adobeAnalyticsScriptURL}">
    <script type="text/javascript">_satellite.pageBottom();</script>
</c:if>
