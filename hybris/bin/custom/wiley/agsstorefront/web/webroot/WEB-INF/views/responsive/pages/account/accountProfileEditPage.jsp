<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>

<div class="main-content">
    <div class="panel-heading">
        <h3 class="panel-title text-center text-uppercase">
            <span><spring:theme code="profile.updateProfile.caption" /></span>
        </h3>
    </div>
    <div class="panel-body">
        <div class="site-form update-profile-form">
            <common:globalMessages />
            <div class="form-title">
                <spring:theme code="profile.updateProfile.caption" />
            </div>
            <form:form action="${action}" method="post" commandName="wileyUpdateProfileForm">
                <div class="block-form">
                    <div class="form-cell">
                        <formElement:formInputBox idKey="firstName" labelKey="profile.firstName" path="firstName"
                            labelCSS="form-label" mandatory="true" />
                    </div>
                    <div class="form-cell">
                        <formElement:formInputBox idKey="lastName" labelKey="profile.lastName" path="lastName"
                            labelCSS="form-label" mandatory="true" />
                    </div>
                    <div class="form-cell">
                        <formElement:formInputBox idKey="emailUid" labelKey="profile.email" path="emailUid"
                            labelCSS="form-label" mandatory="true" />
                    </div>
                </div>
                <div class="form-buttons">
                    <div class="text-right">
                        <a href="${wileyEmailPreferenceLink}" class="link"> <spring:theme
                                code="profile.updateProfile.emailpref.link" text="Email preferece" />
                        </a>
                    </div>
                    <div class="text-right ">
                        <button type="submit" class="button form-button">
                            <spring:theme code="profile.updateProfile.submit" text="Save" />
                        </button>
                    </div>
                </div>
            </form:form>
        </div>
    </div>
</div>