<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>

<template:page pageTitle="${pageTitle}">

<div class="wrapper">
    <ul class="progress-line">
        <li class="step step-completed"><span class="step-number">1</span><span class="step-name"><spring:theme code="pinActivation.breadcrumb.step.one"/></span></li>
        <li class="step"><span class="step-number">2</span>
            <span class="step-name step-name-desktop"><spring:theme code="pinActivation.breadcrumb.step.two"/></span>
            <span class="step-name step-name-mobile"><spring:theme code="pinActivation.breadcrumb.step.two.mobile"/></span>
        </li>
        <li class="step"><span class="step-number">3</span><span class="step-name"><spring:theme code="pinActivation.breadcrumb.step.three"/></span></li>
    </ul>

    <div class="feedback-title">
        <cms:pageSlot position="TopContent" var="contactInfoComponent">
            <cms:component component="${contactInfoComponent}"/>
        </cms:pageSlot>
    </div>

    <h1 class="page-title"><spring:theme code="pinActivation.pagetitle"/></h1>
    <div class="site-form activate-pin-form">
        <div class="form-title"><spring:theme code="pinActivation.formtitle"/></div>
        <form:form method="post" commandName="pinActivationForm">
            <div class="form-cell">
                <div class="form-field">
                    <formElement:formInputBox idKey="pin" labelKey="pinActivation.formlabel" path="pin" value="${invalidPin}"/>
                </div>
            </div>
            <div class="form-buttons">
                <button type="submit" class="button button-submit form-button"><spring:theme code="pinActivation.submitbutton"/></button>
            </div>
        </form:form>
        <div class="form-description">
            <p><spring:theme code="pinActivation.formdescription"/></p>
        </div>
    </div>
</div>



</template:page>