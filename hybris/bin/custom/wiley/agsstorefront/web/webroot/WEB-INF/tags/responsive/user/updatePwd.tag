<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>


<div class="login-page">
	<h1 class="page-title"><spring:theme code="text.account.profile.resetPassword"/></h1>

	<div class="site-form reset-password-form">
		<div class="form-title">
			<spring:theme code="text.account.profile.resetPasswordTitle"/>
		</div>
		<div class="form-description">
			<spring:theme code="text.account.profile.resetPasswordDescription"/>
			<div class="user-email">
				<spring:theme code="text.account.profile.resetPasswordEmail" arguments="${email}"/>
			</div>
		</div>
		<form:form method="post" commandName="wileyUpdatePwdForm">
			<form:hidden path="redirectType" value="${redirectType}"/>
			<div class="form-cell">
				<formElement:formPasswordBox idKey="updatePwd.pwd" labelKey="updatePwd.pwd" path="pwd" labelCSS="form-label required" inputCSS="form-control" mandatory="true" />
			</div>
			<div class="form-cell">
				<formElement:formPasswordBox idKey="updatePwd.checkPwd" labelKey="updatePwd.checkPwd" path="checkPwd" labelCSS="form-label required" inputCSS="form-control" mandatory="true" />
			</div>
			<div class="form-buttons">
				<button type="submit" class="button button-submit form-button">
					<spring:theme code="text.account.profile.resetPasswordSubmit"/>
				</button>
			</div>
		</form:form>
	</div>
	<div class="form-required"><spring:theme code="text.account.profile.resetPasswordRequired"/></div>
</div>

