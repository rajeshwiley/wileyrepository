<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:if test="${not empty currencies}">
	<c:url value="/_s/currency" var="setCurrencyActionUrl" />
	<form:form action="${setCurrencyActionUrl}" method="post" id="currency-form">
		<div id="currency-selector" class="currency-selector dropdown">
			<ycommerce:testId code="header_currency_select">
				<button type="button" id="currency-selector-menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="dropdown-toggle">
					<span class="dropdown-text">
						<c:forEach items="${currencies}" var="curr">
							${curr.isocode == currentCurrency.isocode ? curr.name : ''}
						</c:forEach>
					</span>
				</button>
				<ul aria-labelledby="currency-selector-menu" class="dropdown-menu">
					<c:forEach items="${currencies}" var="curr">
						<li data-value="${curr.isocode}" ${curr.isocode == currentCurrency.isocode ? 'class="current"' : ''}>
							<a><c:out value="${curr.symbol} ${curr.isocode}" /></a>
						</li>
					</c:forEach>
				</ul>
				<input type="hidden" name="code" value="${currentCurrency.isocode}"/>
			</ycommerce:testId>
		</div>
	</form:form>
</c:if>


