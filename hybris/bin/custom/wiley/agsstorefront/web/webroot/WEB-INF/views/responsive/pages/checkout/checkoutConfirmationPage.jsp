<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>
<%@ taglib prefix="multi" tagdir="/WEB-INF/tags/addons/b2ccheckoutaddon/responsive/checkout/multi" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="format-common" tagdir="/WEB-INF/tags/addons/welagsstorefrontcommons/shared/format"%>
<%@ taglib prefix="wileycommerce" uri="http://hybris.com/tld/wileycommercetags" %>

<template:page pageTitle="${pageTitle}" hideHeaderLinks="false">

	<%--<c:url value="${continueUrl}" var="continueShoppingUrl" scope="session"/>--%>

	<c:set value="${wileycommerce:getOrderForBillingEvent(orderData,'paynow')}" var="payNowPrice" />

	<div class="confirmation-page">
		  <cms:pageSlot position="CheckoutBreadcrumbs" var="feature">
			  <c:set var="step" value="4" scope="request"/>
			  <cms:component component="${feature}" />
          </cms:pageSlot>

          <div class="row-mobile">
            <cms:pageSlot position="TopContent" var="feature" element="div">
                <cms:component component="${feature}"/>
            </cms:pageSlot>
          </div>
          <div class="row-mobile">
            <cms:pageSlot position="SideContent" var="feature" element="div">
        	    <cms:component component="${feature}"/>
		    </cms:pageSlot>
          </div>
          <div class="table-order">
            <div class="table-order-row">
              <div class="table-order-head"><spring:theme code="text.account.order.orderDetails.orderNumber" arguments="${orderData.code}"/>
              	&nbsp;<format-common:formattedDate date="${orderData.created}"/>
              </div>
              <div class="table-order-head">&nbsp;</div>
              <div class="table-order-head"><spring:theme code="checkout.orderConfirmation.quantity"/></div>
              <div class="table-order-head"><spring:theme code="checkout.orderConfirmation.total"/></div>
            </div>

            <c:forEach items="${orderData.entries}" var="entry">
            	<c:set value="${wileycommerce:getEntryForBillingEvent(entry,'paynow')}" var="payNowPriceEntry"/>
                <c:url value="${externalBaseUrl}" var="productUrl"/>
	            <div class="table-order-row subscription-row">
	              <div class="table-order-cell"><a href="${productUrl}">${entry.product.name}</a></div>
	              <div class="table-order-cell">&nbsp;</div>
	              <div class="table-order-cell">${entry.quantity}</div>
	              <div class="table-order-cell"><format:price priceData="${payNowPriceEntry.totalPrice}" displayFreeForZero="true"/></div>
	            </div>
	            <div class="table-order-row-mobile">
	              <div class="table-order-row">
	                <div class="table-order-cell"><spring:theme code="checkout.orderConfirmation.quantity.mobile" arguments="${entry.quantity}"/></div>
	              </div>
	              <div class="table-order-row">
	                <div class="table-order-cell"><spring:theme code="checkout.orderConfirmation.total.mobile"/>&nbsp;<format:price priceData="${payNowPriceEntry.totalPrice}" displayFreeForZero="true"/></div>
	              </div>
	            </div>
            </c:forEach>

            <div class="table-order-row-group">
              <div class="table-order-row">
                <div class="table-order-cell"></div>
                <div class="table-order-cell"></div>
                <div class="table-order-cell"><spring:theme code="checkout.orderConfirmation.subtotal"/></div>
                <div class="table-order-cell"><format:price priceData="${payNowPrice.subTotalWithoutDiscount}" /></div>
              </div>
                <c:choose>
                    <c:when test="${payNowPrice.totalDiscounts.value.doubleValue() > 0}">
                    <div class="table-order-row">
                        <div class="table-order-cell"></div>
                        <div class="table-order-cell"></div>
                        <div class="table-order-cell">
                            <span class="table-order-cell-desktop"><spring:theme code="checkout.orderConfirmation.validDiscountCode"/></span>
                            <span class="table-order-cell-mobile"><spring:theme code="checkout.orderConfirmation.discountCode"/></span>
                        </div>
                        <div class="table-order-cell">
                            <c:out value="${voucherCode}"/>
                        </div>
                    </div>
                    </c:when>
                </c:choose>
              <c:if test="${payNowPrice.totalDiscounts.value.doubleValue() > 0}">
                  <div class="table-order-row">
                    <div class="table-order-cell"></div>
                    <div class="table-order-cell"></div>
                    <div class="table-order-cell"><spring:theme code="checkout.orderConfirmation.discount"/></div>
                    <div class="table-order-cell">&#45;<format:price priceData="${payNowPrice.totalDiscounts}"/></div>
                  </div>
			  </c:if>
			  <c:if test="${orderData.taxAvailable}">
	              <div class="table-order-row">
	                <div class="table-order-cell"></div>
	                <div class="table-order-cell"></div>
	                <div class="table-order-cell"><spring:theme code="checkout.orderConfirmation.tax"/></div>
	                <div class="table-order-cell"><format:price priceData="${payNowPrice.totalTax}"/></div>
	              </div>
              </c:if>
              <div class="table-order-row total-row">
                <div class="table-order-cell"></div>
                <div class="table-order-cell"></div>
                <div class="table-order-cell"><spring:theme code="checkout.orderConfirmation.total.mobile"/></div>
                <div class="table-order-cell"><format:price priceData="${payNowPrice.totalPriceWithTax}" /></div>
              </div>
            </div>
          </div>
          <div class="form-buttons">
            <button type="submit" class="button form-button continueShoppingButton" data-continue-shopping-url="${magiclink}"><spring:theme code="checkout.orderConfirmation.start.searching.button"/></button>
          </div>
        </div>

</template:page>

