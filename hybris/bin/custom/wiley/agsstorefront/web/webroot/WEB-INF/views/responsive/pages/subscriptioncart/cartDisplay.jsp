<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="subscriptioncart" tagdir="/WEB-INF/tags/responsive/subscriptioncart" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>


<c:if test="${not empty cartData.entries}">
    <c:url value="/cart/checkout" var="checkoutUrl" scope="session" />
    <div class="subscriptions-title">
        <h2 class="title">
            <spring:theme code="basket.title.mycart" />
            <span class="cart-id">
            	<spring:theme code="basket.page.cartId"/>&nbsp;${cartData.code}
            </span>
        </h2>
        <div class="subscriptions-action-buttons">
            <button class="button form-button checkoutButton" data-checkout-url="${checkoutUrl}">
                <spring:theme code="checkout.checkout" />
            </button>
        </div>
    </div>
    <subscriptioncart:subscriptionCartItems cartData="${cartData}" />
</c:if>
