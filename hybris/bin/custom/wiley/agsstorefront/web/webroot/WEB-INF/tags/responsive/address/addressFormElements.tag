<%@ attribute name="tabindex" required="false" type="java.lang.Integer"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

	<input type="hidden" id="customerHasSavedAddress" value="${not empty wileyAddressForm.firstName}"/>
	<input type="hidden" id="countryHasRegions" value="${not empty regions}"/>

	<c:set var="cityLabelKey" value="${wileyAddressForm.countryIso == 'US' ? 'address.townCity' : 'address.cityProvince'}"/>
	<c:set var="zipCodeLabelKey" value="${wileyAddressForm.countryIso == 'US' ? 'address.zipcode' : 'address.postcode'}"/>

	<div class="row">
	  <div class="col-sm-12 col-md-6 form-cell">
		<formElement:formInputBox idKey="addressFirstName" labelKey="address.firstName"
			labelCSS="form-label" isRequred="true" adjustToTwoColLayout="true" path="firstName" mandatory="true" tabindex="0"/>
	  </div>
	  <div class="col-sm-12 col-md-6 form-cell">
		<formElement:formInputBox idKey="addressSurname" labelKey="address.surname" path="lastName"
			labelCSS="form-label" isRequred="true" adjustToTwoColLayout="true" mandatory="true" tabindex="0" />
	  </div>
	</div>

	<div class="row">
	  <div id="countrySelector" class="col-sm-12 col-md-6 form-cell">
		<formElement:formSelectBox idKey="address.country" labelCSS="form-label" isRequred="true"
			adjustToTwoColLayout="true" labelKey="address.country" path="countryIso" mandatory="true"
			skipBlank="true" skipBlankMessageKey="address.selectCountry"
			items="${countries}" itemValue="isocode" tabindex="0" />
	  </div>
	  <div class="col-sm-12 col-md-6 form-cell">
		<formElement:formInputBox idKey="addressLine1" labelKey="address.address1" path="line1"
			labelCSS="form-label" isRequred="true" adjustToTwoColLayout="true" mandatory="true" tabindex="0" />
	  </div>
	</div>

	<div class="row">
	  <div class="col-sm-12 col-md-6 form-cell">
		<formElement:formInputBox idKey="addressLine2" labelKey="address.address2" path="line2"
			labelCSS="form-label" adjustToTwoColLayout="true" mandatory="false" tabindex="0"/>
	  </div>

	  <div class="col-sm-12 col-md-6 form-cell">
		<formElement:formInputBox idKey="addressTownCity" labelKey="${cityLabelKey}" path="townCity"
			labelCSS="form-label" isRequred="true" adjustToTwoColLayout="true" mandatory="true" tabindex="0" />
	  </div>
	</div>

	<div class="row">
		<c:if test="${not empty regions}">
			<div class="col-sm-12 col-md-6 form-cell">
				<formElement:formSelectBox idKey="addressRegion" labelKey="address.state" path="regionIso" mandatory="true" skipBlank="false"
					labelCSS="form-label" isRequred="true" adjustToTwoColLayout="true"
					items="${regions}" itemValue="${useShortRegionIso ? 'isocodeShort' : 'isocode'}"
					selectedValue="${wileyAddressForm.regionIso}" tabindex="0" />
			</div>
		</c:if>

	  <div class="col-sm-12 col-md-6 form-cell">
		<formElement:formInputBox idKey="addressPostcode" labelKey="${zipCodeLabelKey}" path="postcode"
			labelCSS="form-label" isRequred="true" adjustToTwoColLayout="true" mandatory="true" tabindex="0" />
	  </div>
	</div>

	<div class="row">
	  <div class="col-sm-12 col-md-6 form-cell">
		<formElement:formInputBox idKey="addressPhone" labelKey="address.phone" path="phone"
			labelCSS="form-label" isRequred="true" adjustToTwoColLayout="true" mandatory="true" tabindex="0" />
	  </div>
	</div>
