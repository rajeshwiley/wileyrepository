<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/address"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<form:form method="post" commandName="wileyAddressForm">
	<address:addressFormElements/>
	<div class="row subscribeToUpdates">
		<div class="col-sm-12 col-md-6 form-cell">
			<div class="form-buttons">
				<div class="checkbox-component">
					<form:checkbox path="subscribeToUpdates" id="subscribeToUpdates" tabindex="10"/>
					<label class="left-side" for="subscribeToUpdates"><spring:theme code="checkout.updates.subscribe" /></label>
				</div>
			</div>
		</div>
	</div>

</form:form>

