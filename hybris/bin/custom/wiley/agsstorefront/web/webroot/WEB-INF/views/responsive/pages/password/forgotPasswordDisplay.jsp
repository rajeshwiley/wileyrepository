<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>

<h1 class="page-title">
    <spring:theme code="forgottenPwd.caption" />
</h1>

<div class="site-form forgot-password-form">
    <div class="form-title">
        <spring:theme code="forgottenPwd.subcaption" />
    </div>
    <div class="form-description">
        <p><spring:theme code="forgottenPwd.description" /></p>
        <p><spring:theme code="forgottenPwd.description2" /></p>
    </div>
    <form:form method="post" commandName="forgottenPwdForm">
        <div class="form-cell">
            <formElement:formInputBox idKey="forgottenPwd.email" labelKey="forgottenPwd.email" path="email"
                mandatory="true" labelCSS="form-label" />
        </div>
        <div class="form-cell">
            <button class="button button-submit form-button" type="submit">
                <spring:theme code="forgottenPwd.submit" />
            </button>
        </div>
    </form:form>
</div>