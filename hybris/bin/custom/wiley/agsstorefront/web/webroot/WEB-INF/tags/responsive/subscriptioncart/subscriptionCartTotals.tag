<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData"%>
<%@ attribute name="showTaxEstimate" required="false" type="java.lang.Boolean"%>
<%@ attribute name="showTax" required="false" type="java.lang.Boolean"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart"%>
<%@ taglib prefix="wileycommerce" uri="http://hybris.com/tld/wileycommercetags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<c:set value="${wileycommerce:getOrderForBillingEvent(cartData,'paynow')}" var="payNowPrice" />

<div class="subscriptions-subtotal">
	<div class="subscriptions-subtotal-items">
	    <div class="subscriptions-subtotal-item item-subtotal">
	        <div class="subscriptions-subtotal-name">
	            <spring:theme code="basket.page.totals.subtotal" />
	        </div>
	        <div class="subscriptions-subtotal-value">
	            <format:price priceData="${payNowPrice.subTotalWithoutDiscount}" />
	        </div>
	    </div>
	    <c:if test="${!empty cartData.appliedVouchers}">
            <div class="subscriptions-subtotal-item item-discount-code">
                <div class="subscriptions-subtotal-name discount-code-desktop">
                    <spring:theme code="basket.page.totals.discount.code"/>
                </div>
                <div class="subscriptions-subtotal-value">
                    <div class="voucher-list__item">
                        <spring:url value='/cart/voucher/remove' var="voucherRemoveFormAction"/>
                        <form:form commandName="voucherForm" action="${voucherRemoveFormAction}" method="post">
                            <span id="voucher-code-${voucher.code}" class="js-release-voucher voucher-list__item-box">
                                ${voucher.code}
                                <form:input id="discountCodeValue" path="discountCodeValue" hidden="hidden" value="${voucher.code}" type="text"/>
                                <span id="voucher-remove-button" class="glyphicon glyphicon-remove js-release-voucher-remove-btn voucher-list__item-remove"/>
                            </span>
                        </form:form>
                    </div>
                </div>
            </div>
        </c:if>
	    <c:choose>
	        <c:when test="${payNowPrice.totalDiscounts.value.doubleValue() > 0}">
	            <div class="subscriptions-subtotal-item item-discount">
	                <div class="subscriptions-subtotal-name">
	                    <spring:theme code="basket.page.totals.discount" />
	                </div>
	                <div class="subscriptions-subtotal-value">
	                    -<format:price priceData="${payNowPrice.totalDiscounts}" />
	                </div>
	            </div>
	        </c:when>
	    </c:choose>
	    <sec:authorize access="!hasRole('ROLE_ANONYMOUS')">
	    <c:if test="${cartData.taxAvailable}">
		    <div class="subscriptions-subtotal-item item-taxes">
		        <div class="subscriptions-subtotal-name">
		            <spring:theme code="basket.page.totals.netTax" />
		        </div>
		        <div class="subscriptions-subtotal-value">
		            <format:price priceData="${payNowPrice.totalTax}" />
		        </div>
		    </div>
	    </c:if>
	    </sec:authorize>
	    <div class="subscriptions-subtotal-item item-total">
	        <div class="subscriptions-subtotal-name">
	            <spring:theme code="basket.page.totals.total" />
	        </div>
	        <div class="subscriptions-subtotal-value">
	            <format:price priceData="${payNowPrice.totalPriceWithTax}" />
	        </div>
	    </div>
	</div>
    <c:url value="/cart/checkout" var="checkoutUrl" scope="session" />
	<div class="subscriptions-action-buttons">
	    <button class="button form-button button-continue checkoutButton" data-checkout-url="${checkoutUrl}">
	        <spring:theme code="checkout.checkout" />
	    </button>
	</div>
</div>
