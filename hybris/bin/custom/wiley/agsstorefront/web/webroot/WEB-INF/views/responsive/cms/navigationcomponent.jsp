<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>


<c:if test="${component.visible}">
 <c:if test="${not empty component.navigationNode }">
    <div class="aside-menu" id="aside-menu">
        <div class="panel-heading">
            <h3 class="panel-title text-center text-uppercase">
                <span> ${component.navigationNode.title} </span>
            </h3>
        </div>
        <div class="panel-body">
            <nav class="navigation">
                <c:forEach items="${component.navigationNode.links}" var="link">
                    <c:url value="${link.url}/" var="path" />
                    <c:set value="${ requestScope['javax.servlet.forward.request_uri'] == path ? 'current':'' }"
                        var="selected" />
                    <a href="${path}" class="navigation-item ${selected}"><span>${link.linkName}</span></a>
                </c:forEach>
            </nav>
        </div>
    </div>
  </c:if>
</c:if>
