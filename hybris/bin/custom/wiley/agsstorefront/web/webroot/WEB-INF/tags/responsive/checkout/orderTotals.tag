<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>

<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="order-details-info">
    <div class="subtotal"><span><spring:theme code="basket.page.totals.subtotal"/></span><span><format:price priceData="${cartData.subTotalWithoutDiscount}"/></span></div>
    <c:if test="${cartData.totalDiscounts.value > 0}">
        <div class="discount"><span><spring:theme code="basket.page.totals.discount"/></span><span>-<format:price priceData="${cartData.totalDiscounts}"/></span></div>
    </c:if>
    <c:if test="${cartData.taxAvailable}">
        <div class="tax"><span><spring:theme code="basket.page.totals.netTax"/></span><span><format:price priceData="${cartData.totalTax}"/></span></div>
    </c:if>
    <div class="total"><span><spring:theme code="basket.page.totals.total"/></span><span><format:price priceData="${cartData.totalPriceWithTax}"/></span></div>
</div>