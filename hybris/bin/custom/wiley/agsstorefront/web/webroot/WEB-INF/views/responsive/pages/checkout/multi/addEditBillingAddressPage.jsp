<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/address"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="checkout" tagdir="/WEB-INF/tags/responsive/checkout" %>

<template:page pageTitle="${pageTitle}" hideHeaderLinks="true">

    <c:url var="returnToCart" value="/cart" />
	
		  <cms:pageSlot position="CheckoutBreadcrumbs" var="feature">
			  <c:set var="step" value="2" scope="request"/>
			  <cms:component component="${feature}" />
          </cms:pageSlot>

        <div class="content">   
         <div class="text-left">
	          <cms:pageSlot position="TopContent" var="feature">
				<cms:component component="${feature}"/>
			</cms:pageSlot>
          </div>
		  
          <div class="billing-address">
            <div class="billing-address-form">
              <div class="site-form">
                <div class="form-title"><spring:theme code="text.account.paymentDetails.billingAddress"/></div>
					<form:form method="post" commandName="wileyAddressForm" action="${request.contextPath}/checkout/multi/billing-address/add">
						<div id="i18nAddressForm" class="i18nAddressForm">
							<form:hidden path="addressId" />
							<address:addressFormElements />
							<div class="row">
								<div class="col-sm-12 col-md-6 form-cell">
									<div class="form-buttons">
										<div class="checkbox-component">
											<form:checkbox path="subscribeToUpdates" id="subscribeToUpdates" tabindex="10"/>
											<label class="left-side" for="subscribeToUpdates"><spring:theme code="checkout.updates.subscribe" /></label>
										</div>
									</div>
								</div>
							</div>
						</div>
					</form:form>
              </div>
              <div class="form-required">Required</div>
            </div>
            <div class="order-details">
              <div class="form-title">Order Details</div>
              <checkout:orderTotals cartData="${cartData}"/>
              <div class="form-buttons"><span class="link-control"><a href="${returnToCart}" title="" class="go-to-card" tabindex="11">Cancel and return to cart</a></span><span class="btn-control">
                    <button id="saveBilling" type="submit" class="button form-button" tabindex="12">Continue</button></span></div>
            </div>
          </div>
        </div>
      
</template:page>
