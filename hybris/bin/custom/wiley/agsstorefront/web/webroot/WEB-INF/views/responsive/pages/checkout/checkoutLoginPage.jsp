<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<template:page pageTitle="${pageTitle}" hideHeaderLinks="true">
<div class="login-page">
		  <cms:pageSlot position="CheckoutBreadcrumbs" var="feature">
			  <c:set var="step" value="1" scope="request"/>
			  <cms:component component="${feature}" />
          </cms:pageSlot>

	<cms:pageSlot position="TopContentSlot" var="feature">
		<cms:component component="${feature}"/>
	</cms:pageSlot>

	<h1 class="page-title"><spring:theme code="register.login.new.customer" /></h1>

	<div id="accordion" role="tablist" aria-multiselectable="false" class="row panel-group">
		<div class="col-sm-6 col-md-6 panel">
			<cms:pageSlot position="LeftContentSlot" var="feature">
				<cms:component component="${feature}" />
			</cms:pageSlot>
		</div>

		<div class="col-sm-6 col-md-6 panel">
			<cms:pageSlot position="RightContentSlot" var="feature">
				<cms:component component="${feature}" />
			</cms:pageSlot>
		</div>
	</div>

    <div class="form-required laptop">Required</div>
</div>
</template:page>