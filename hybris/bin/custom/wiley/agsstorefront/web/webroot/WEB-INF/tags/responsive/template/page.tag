<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true"%>
<%@ attribute name="pageTitle" required="false" rtexprvalue="true"%>
<%@ attribute name="pageCss" required="false" fragment="true"%>
<%@ attribute name="pageScripts" required="false" fragment="true"%>
<%@ attribute name="hideHeaderLinks" required="false"%>
<%@ attribute name="isTwoColumnLayout" required="false" type="java.lang.Boolean" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="header"
	tagdir="/WEB-INF/tags/responsive/common/header"%>
<%@ taglib prefix="footer"
	tagdir="/WEB-INF/tags/responsive/common/footer"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart" %>

<template:master pageTitle="${pageTitle}">

	<jsp:attribute name="pageCss">
		<jsp:invoke fragment="pageCss" />
	</jsp:attribute>

	<jsp:attribute name="pageScripts">
		<jsp:invoke fragment="pageScripts" />
	</jsp:attribute>

	<jsp:body>

		<div class="wrapper ${isTwoColumnLayout ? 'two-column-layout' : ''}">
			<main data-currency-iso-code="${currentCurrency.isocode}">
				<spring:theme code="text.skipToContent" var="skipToContent" />
				<a href="#skip-to-content" class="skiptocontent" data-role="none">${skipToContent}</a>
				<spring:theme code="text.skipToNavigation" var="skipToNavigation" />
				<a href="#skiptonavigation" class="skiptonavigation" data-role="none">${skipToNavigation}</a>

					<header:header hideHeaderLinks="${hideHeaderLinks}" />

					<a id="skip-to-content"></a>

					<div class="content" ${isTwoColumnLayout ? 'id="swipe-area"' : ''}>
						<c:if test="${not isTwoColumnLayout}">
							<common:globalMessages />
						</c:if>
						<cart:cartRestoration />
						<jsp:doBody />
					</div>
			</main>
			<c:if test="${isTwoColumnLayout}">
				<div class="loader-container">
					<div class="loader-text">Loading...</div>
				</div>
			</c:if>
		</div>

		<footer:footer />

	</jsp:body>

</template:master>
