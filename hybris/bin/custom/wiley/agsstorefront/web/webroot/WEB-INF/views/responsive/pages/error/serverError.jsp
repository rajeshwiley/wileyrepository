<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<!DOCTYPE html>
<html>
<head>

<title>Server Error Page | AGS Site</title>

<c:url var="uiUrl" value="/_ui" />
<link href="${uiUrl}/addons/welagsstorefrontcommons/responsive/common/css/welagsstorefrontcommons.css" media="all" type="text/css" rel="stylesheet">
<link href="${uiUrl}/responsive/theme-blue/css/style.css" media="all" type="text/css" rel="stylesheet">

</head>
<body>

	<c:url var="homePageUrl" value="/" />
	<c:set var="homePageUrlTitle" value="Wiley AGS" />

	<div class="wrapper">

		<div class="header">
			<div class="header-container">
				<div class="header-title">
					<a href="${homePageUrl}" title="${homePageUrlTitle}"><img src="${uiUrl}/responsive/theme-blue/images/logo-header.png" alt="Header Logo"></a>
				</div>
			</div>
		</div>

		<h2 style="font-family: Arial, Helvetica, sans-serif;">&nbsp;</h2>
		<h1 class="page-title"
			style="font-style: inherit; border: 0px; font-family: 'Liberation Sans', sans-serif, Helvetica; font-size: 16pt; font-weight: 300; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; clear: both; line-height: 1.09091; color: rgb(187, 189, 192); text-transform: uppercase; text-align: center;">
			SERVER ERROR (500) <strong> <br>
			</strong>
		</h1>
		<h1 class="page-title"
			style="border: 0px; font-family: 'Liberation Sans', sans-serif, Helvetica; font-size: 16pt; font-style: inherit; font-weight: 300; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; clear: both; line-height: 1.09091; color: rgb(187, 189, 192); text-transform: uppercase; text-align: center;">
			<p style="color: rgb(0, 0, 0); font-family: Arial, Verdana, sans-serif; font-size: 12px; line-height: normal; text-align: start; text-transform: none;">&nbsp;</p>
			<div class="page-content"
				style="color: rgb(0, 0, 0); font-family: Arial, Verdana, sans-serif; font-size: 12px; line-height: normal; text-align: start; text-transform: none; border: 0px; margin: 0px auto 48px; outline: 0px; padding: 0px 30px; vertical-align: baseline; max-width: 100%; word-wrap: break-word;">
				<p
					style="color: rgb(51, 51, 51); font-family: inherit; font-size: 16px; line-height: 24px; border: 0px; border-image-source: initial; border-image-slice: initial; border-image-width: initial; border-image-outset: initial; border-image-repeat: initial; font-style: inherit; font-weight: inherit; margin: 0px 0px 24px; outline: 0px; padding: 0px; vertical-align: baseline; text-align: center;">We're
					experiencing a server error.</p>
				<p
					style="color: rgb(51, 51, 51); font-family: inherit; font-size: 16px; line-height: 24px; border: 0px; border-image-source: initial; border-image-slice: initial; border-image-width: initial; border-image-outset: initial; border-image-repeat: initial; font-style: inherit; font-weight: inherit; margin: 0px 0px 24px; outline: 0px; padding: 0px; vertical-align: baseline; text-align: center;">
					Please try again later or contact&nbsp;<a href="mailto:info@wiley.com">info@wiley.com</a>&nbsp;
				</p>
			</div>
		</h1>
		<p>&nbsp;</p>
	</div>
	<div class="footer">
		<div class="footer-right-side">
			<div class="footer-logo">
				<a href="${homePageUrl}" title="${homePageUrlTitle}"><img src="${uiUrl}/responsive/theme-blue/images/logo-footer.png" alt="Footer Logo"></a>
			</div>
		</div>
	</div>
</body>
</html>