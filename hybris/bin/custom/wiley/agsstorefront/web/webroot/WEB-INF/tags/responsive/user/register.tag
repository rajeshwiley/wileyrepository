<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="actionNameKey" required="true"
	type="java.lang.String"%>
<%@ attribute name="action" required="true" type="java.lang.String"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>

    
  <div role="tab" id="headingOne" class="panel-heading">
    <div class="form-title">
		<spring:theme code="register.create.account" text="Create a New Account" />
		<a role="button" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="collapsed form-title-toggle"></a>
    </div>
  </div>
  <div id="collapseOne" role="tabpanel" aria-labelledby="headingOne" class="panel-collapse collapse in">
    <div class="panel-body">
      <div class="container-fluid create-account">
	      	<form:form method="post" commandName="wileyRegisterForm" action="${action}">
		        <div class="row">
		          <div class="col-sm-6 col-md-6 form-cell">
                     <formElement:formInputBox idKey="register.firstName"
                        labelKey="register.firstName" path="firstName" labelCSS="form-label required"
                        mandatory="true" />
		          </div>
		          <div class="col-sm-6 col-md-6 form-cell">
                    <formElement:formInputBox idKey="register.lastName"
                            labelKey="register.lastName" path="lastName" labelCSS="form-label required"
                            mandatory="true" />
		          </div>
				</div>
				<div class="row">
		          <div class="col-sm-6 col-md-6 form-cell">
                    <formElement:formInputBox idKey="register.email"
                        labelKey="register.email" path="email" labelCSS="form-label required"
                        mandatory="true" />
		          </div>
		          <div class="col-sm-6 col-md-6 form-cell">
                     <formElement:formInputBox idKey="register.confirmEmail"
                        labelKey="register.confirmEmail" path="confirmEmail" labelCSS="form-label required"
                        mandatory="true" />
		          </div>
				</div>
				<div class="row">
		          <div class="col-sm-6 col-md-6 form-cell">
                  <formElement:formPasswordBox idKey="password" labelKey="register.pwd"
                          path="pwd"  labelCSS="form-label required" mandatory="true" />
		          </div>
		          <div class="col-sm-6 col-md-6 form-cell">
                    <formElement:formPasswordBox idKey="register.checkPwd"
                            labelKey="register.checkPwd" path="checkPwd"  labelCSS="form-label required"
                            mandatory="true" />
		          </div>
		        </div>
                <div class="row">
                    <div class="form-required mobile">Required</div>
                </div>
		        <div class="row">
		          <div class="col-md-12 form-buttons">
		            <button type="submit" class="button form-button pull-right"><spring:theme code="register.button.create.account" text="Create Account" /></button>
		          </div>
		        </div>
	        </form:form>
      </div>
    </div>
  </div>
