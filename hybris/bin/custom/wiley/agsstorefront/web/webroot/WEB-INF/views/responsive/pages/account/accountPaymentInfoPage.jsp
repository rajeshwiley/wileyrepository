<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/address"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<style>
.edit-payment .subscribeToUpdates{
	display:none;
}
</style>
<div class="main-content">
  <div class="panel-heading">
	<h3 class="panel-title text-center text-uppercase"><span><spring:theme code="text.account.profile.EditPayment"/></span></h3>
  </div>
  <div class="panel-body">
  	<div class="edit-payment">
	  <common:globalMessages />
	  <div class="edit-payment-info">
		<div class="edit-payment-form">
		  <div class="site-form">
            <div class="form-title"><spring:theme code="text.account.profile.EditPayment"/></div>
			<form:form method="post" commandName="wileyAddressForm">
				<form:hidden path="addressId" class="add_edit_delivery_address_id"
					status="${not empty suggestedAddresses ? 'hasSuggestedAddresses' : ''}" />
				<input type="hidden" name="bill_state" id="address.billstate" />

				<div id="i18nAddressForm" class="i18nAddressForm">
					<address:addressFormElements />
				</div>
			</form:form>
		  </div>
		  <div class="form-required"><spring:theme code="form.field.required"/></div>
		</div>
		
		<c:forEach items="${paymentInfoData}" var="paymentInfo">
			<c:if test="${paymentInfo.defaultPaymentInfo}">
				<div class="edit-payment-card">
					<table class="card-info">
						<tr>
							<th>Card Type</th>
							<th>Card Number</th>
						</tr>
						<tr>
							<td>
								<span title="${fn:escapeXml(paymentInfo.cardTypeData.name)}" class="card-type ${fn:escapeXml(paymentInfo.cardTypeData.code)}">${fn:escapeXml(paymentInfo.cardTypeData.name)}</span>
							</td>
							<td class="card-number">xxxx-xxxx-${fn:escapeXml(paymentInfo.cardNumber)}</td>
						</tr>
					</table>
					<table class="card-expirience">
						<tr>
							<th colspan="2">Expiration Date</th>
						</tr>
						<tr>
							<td class="date">${fn:escapeXml(paymentInfo.expiryMonth)}&#47;${fn:escapeXml(paymentInfo.expiryYear)}</td>
							<td class="td-button">
								<c:url value="/my-account/update-card-details" var="updateCardUrl"/>
								<form:form commandName = "update-card-details" action="${updateCardUrl}" method="get">
									<button type="submit" class="button form-button"><spring:theme code="text.account.profile.updateCreditCard"/></button>
								</form:form>
							</td>
						</tr>
					</table>
				</div>
			</c:if>
		</c:forEach>
		
	  </div>
	  <div class="form-buttons">
		<button id="saveBilling" type="submit" class="button form-button"><spring:theme code="text.button.save"/></button>
	  </div>
	</div>
  </div>
</div>
