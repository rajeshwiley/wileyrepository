<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>

<div class="footer">
	<div class="footer-left-side">
		<cms:pageSlot position="Footer" var="feature">
			<cms:component component="${feature}"/>
		</cms:pageSlot>
	</div>
	<div class="footer-right-side">
		<cms:pageSlot position="CompanyLogo" var="logo" limit="1">
			<cms:component component="${logo}" class="footer-logo"  element="div"/>
		</cms:pageSlot>
	</div>
</div>
