<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="account" tagdir="/WEB-INF/tags/responsive/account"%>

<div class="site-form subscription-form">
    <common:globalMessages />
    <h2 class="title">
        <spring:theme code="text.account.subscription" />
    </h2>
    <c:choose>
        <c:when test="${not empty wileySubscriptionData}">
            <form:form action="${action}" method="post" commandName="wileySubscriptionData">
                <input type="hidden" id="code" name="code" value="${wileySubscriptionData.code}" />

                <div class="table">
                    <div class="table-head">
                        <div class="table-head-cell">
                            <spring:theme code="text.account.subscription.endDate" />
                        </div>
                        <div class="table-head-cell">
                            <spring:theme code="text.account.subscription.name" />
                        </div>
                        <div class="table-head-cell">
                            <spring:theme code="text.account.subscription.price" />
                        </div>
                    </div>
                    <div class="table-row">
                        <div class="table-column">
                            <div class="table-head-cell">
                                <spring:theme code="text.account.subscription.endDate" />
                            </div>
                            <fmt:formatDate value="${wileySubscriptionData.endDate}" var="formattedDate"
                                            pattern="MM/dd/yyyy" />
                            <div class="table-cell">${formattedDate}</div>
                        </div>
                        <div class="table-column">
                            <div class="table-head-cell">
                                <spring:theme code="text.account.subscription.name" />
                            </div>
                            <div class="table-cell">${wileySubscriptionData.name}</div>
                        </div>
                        <div class="table-column">
                            <div class="table-head-cell">
                                <spring:theme code="text.account.subscription.price" />
                            </div>
                            <div class="table-cell">
                                <format:price priceData="${wileySubscriptionData.price}" />
                            </div>
                        </div>
                    </div>
                </div>

                <c:if test="${wileySubscriptionData.renewalEnabled}">
                    <div class="form-buttons">
                        <div class="checkbox-component pull-left">
                            <form:checkbox path="autoRenew" id="auto_renew"
                                           disabled="${wileySubscriptionData.renewalInProgress}"/>
                            <label class="right-side"
                                   for="auto_renew"><spring:theme code="text.account.subscription.autorenew" /></label>
                        </div>
                    </div>
                </c:if>
            </form:form>
        </c:when>
        <c:otherwise>
            <div>
                <spring:theme code="text.account.subscription.no.active" />
            </div>
        </c:otherwise>
    </c:choose>
</div>
