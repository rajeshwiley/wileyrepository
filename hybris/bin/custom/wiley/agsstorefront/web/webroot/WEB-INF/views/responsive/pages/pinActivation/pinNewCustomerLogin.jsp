<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="user" tagdir="/WEB-INF/tags/responsive/user" %>
<c:url value="/pin/login/register" var="pinRegisterActionUrl" />
<user:register actionNameKey="register.submit" action="${pinRegisterActionUrl}"/>
