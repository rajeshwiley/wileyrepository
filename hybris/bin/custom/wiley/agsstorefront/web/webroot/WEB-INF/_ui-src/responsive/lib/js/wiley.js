if (!wiley) {
	var wiley = {};
}

wiley.initExpandingDiscountCode = function() {
	$('.discount-code').each(function () {
		var $this = $(this);
		var discountCodeForm = $this.find('.discount-code-form');
		var discountCodeToggle = $this.find('.discount-code-toggle');

		discountCodeToggle.on('click', function () {
			var discountCodeFormHeight = discountCodeForm.height();

			if (discountCodeFormHeight === 0) {
				var discountCodeFormAutoHeight = discountCodeForm.css('height', 'auto').height();

				discountCodeForm
					.height(discountCodeFormHeight)
					.animate({height: discountCodeFormAutoHeight}, 500, function () {
						discountCodeToggle.addClass('expanded');
					});
			} else {
				discountCodeForm
					.stop()
					.animate({height: '0'}, 500, function () {
						discountCodeToggle.removeClass('expanded');
					});
			}
		});
	});
};

wiley.initAccordionForLoginPage = function() {
	var panelGroup = $('.panel-group');
	var panelCollapse = panelGroup.find('.panel-collapse');
	var panelTitle = panelGroup.find('.form-title');
	var errorsCount = 0;
	var siteForm;
	var _thisCollapsed;

	panelGroup.on('click', '.collapsed', function(e) {
		e.preventDefault();
	});

	panelCollapse.each(function() {
		var _this = $(this);

		errorsCount = _this.find('.has-error').length;

		if(errorsCount === 0) {
			_this.removeClass('in');

			siteForm = _this.closest('.panel');
			_thisCollapsed = siteForm.find('.collapsed');
			_thisCollapsed.attr('data-toggle', 'collapse');
		}
	});

	panelTitle.on('click', 'a', function() {
		var _this = $(this);

		_this.closest('.panel-collapse').removeClass('in');
		_this.attr('data-toggle', 'collapse');
	});
};

wiley.initAsideMenuSwiping = function() {
	var swipeArea = $('#swipe-area');
	var asideMenu = $('#aside-menu');
	var navigation = asideMenu.find('.navigation');
	var navigationItems = navigation.find('.navigation-item');
	var wrapper = swipeArea.closest('.wrapper');

	if(swipeArea.length && navigationItems.length > 1) {
		wiley.swipe(swipeArea, switchPage);
	}

	function switchPage(swipeDirection) {
		var currentPageIndex = navigation.find('.current').index();

		if ((currentPageIndex > 0) && (swipeDirection == 'right')) {
			wrapper.addClass('loading');
			window.location = $(navigationItems[currentPageIndex-1]).attr('href') || '';
		} else if ((currentPageIndex < navigationItems.length-1) && (swipeDirection == 'left')) {
			wrapper.addClass('loading');
			window.location = $(navigationItems[currentPageIndex+1]).attr('href') || '';
		}
	}
};

wiley.swipe = function(swipeArea, callback) {
	var fingerCount = 0;
	var startX = 0;
	var startY = 0;
	var curX = 0;
	var curY = 0;
	var deltaX = 0;
	var deltaY = 0;
	var horzDiff = 0;
	var vertDiff = 0;
	var minLength = 72; // the shortest distance the user may swipe
	var swipeLength = 0;
	var swipeAngle = null;
	var swipeDirection = null;

	swipeArea
		.on('touchstart', function(event) {
			// get the total number of fingers touching the screen
			fingerCount = event.originalEvent.touches.length;
			// since we're looking for a swipe (single finger) and not a gesture (multiple fingers),
			// check that only one finger was used
			if (wiley.isMobileView && fingerCount == 1) {
				// get the coordinates of the touch
				startX = event.originalEvent.touches[0].pageX;
				startY = event.originalEvent.touches[0].pageY;
			} else {
				// more than one finger touched so cancel
				touchCancel(event);
			}
		})
		.on('touchmove', function(event) {
			if (wiley.isMobileView && event.originalEvent.touches.length == 1) {
				curX = event.originalEvent.touches[0].pageX;
				curY = event.originalEvent.touches[0].pageY;
			} else {
				touchCancel(event);
			}
		})
		.on('touchend', function(event) {
			// check to see if more than one finger was used and that there is an ending coordinate
			if (wiley.isMobileView && fingerCount == 1 && curX != 0) {
				// use the Distance Formula to determine the length of the swipe
				swipeLength = Math.round(Math.sqrt(Math.pow(curX - startX,2) + Math.pow(curY - startY,2)));
				// if the user swiped more than the minimum length, perform the appropriate action
				if (swipeLength >= minLength) {
					caluculateAngle();
					determineSwipeDirection(event);

					if(typeof callback == 'function') {
						callback(swipeDirection);
					}
				}
			}

			touchCancel(event);
		})
		.on('touchcancel', touchCancel(event));

	function touchCancel(event) {
		// reset the variables back to default values
		fingerCount = 0;
		startX = 0;
		startY = 0;
		curX = 0;
		curY = 0;
		deltaX = 0;
		deltaY = 0;
		horzDiff = 0;
		vertDiff = 0;
		swipeLength = 0;
		swipeAngle = null;
		swipeDirection = null;
	}

	function caluculateAngle() {
		var X = startX-curX;
		var Y = curY-startY;
		var Z = Math.round(Math.sqrt(Math.pow(X,2)+Math.pow(Y,2))); //the distance - rounded - in pixels
		var r = Math.atan2(Y,X); //angle in radians (Cartesian system)

		swipeAngle = Math.round(r*180/Math.PI); //angle in degrees

		if (swipeAngle < 0) {
			swipeAngle =  360 - Math.abs(swipeAngle);
		}
	}

	function determineSwipeDirection(event) {
		if ((swipeAngle <= 45) && (swipeAngle >= 0) || (swipeAngle <= 360) && (swipeAngle >= 315)) {
			event.preventDefault();
			swipeDirection = 'left';
		} else if ((swipeAngle >= 135) && (swipeAngle <= 225)) {
			event.preventDefault();
			swipeDirection = 'right';
		}
	}
};

$(document).ready(function() {
	wiley.isMobileView = ($(window).width() <= 640);

	wiley.initExpandingDiscountCode();
	wiley.initAccordionForLoginPage();
	wiley.initAsideMenuSwiping();
});

$(window).on('resize', function() {
	wiley.isMobileView = ($(window).width() <= 640);
});