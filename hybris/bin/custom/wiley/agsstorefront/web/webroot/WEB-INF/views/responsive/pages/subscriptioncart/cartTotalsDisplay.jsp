<%@ taglib prefix="subscriptioncart" tagdir="/WEB-INF/tags/responsive/subscriptioncart" %>

<!-- Verified that there's a pre-existing bug regarding the setting of showTax; created issue  -->
<subscriptioncart:subscriptionCartTotals cartData="${cartData}" showTax="false"/>
