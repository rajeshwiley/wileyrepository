<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>

<template:page pageTitle="${pageTitle}" hideHeaderLinks="true">
	<div class="row simple-page">
		<cms:pageSlot position="TopContent" var="feature" element="div" class="col-md-12">
			<cms:component component="${feature}"/>
		</cms:pageSlot>
	</div>
</template:page>