<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>

<div class="main-content">
  <div class="panel-heading">
	<h3 class="panel-title text-center text-uppercase"><span><spring:theme code="text.account.profile.updatePasswordForm"/></span></h3>
  </div>
  <div class="panel-body">
	<div class="site-form change-password-form">
	  <common:globalMessages />
	  <div class="form-title"><spring:theme code="text.account.profile.updatePasswordForm"/></div>
	  <form:form action="${action}" method="post" commandName="updatePasswordForm">
	     <div class="block-form">
			<div class="form-cell">
				<formElement:formPasswordBox idKey="currentPassword"
							labelKey="profile.currentPassword" labelCSS="form-label" path="currentPassword"
							mandatory="true" />
			</div>
			<div class="form-cell">
				<formElement:formPasswordBox idKey="newPassword"
							labelKey="profile.newPassword" labelCSS="form-label" path="newPassword"/>
			</div>
			<div class="form-cell">
				<formElement:formPasswordBox idKey="checkNewPassword"
							labelKey="profile.checkNewPassword" labelCSS="form-label" path="checkNewPassword"
							mandatory="true" />
			</div>
		</div>
		<div class="form-buttons">
		  <div class="text-right ">
			<button type="submit" class="button form-button">
			  <spring:theme code="updatePwd.submit" text="Save"/>
			</button>
		  </div>
		</div>
	  </form:form>
	</div>
  </div>
</div>