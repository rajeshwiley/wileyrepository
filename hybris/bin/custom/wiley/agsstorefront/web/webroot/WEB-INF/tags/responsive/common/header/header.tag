<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="hideHeaderLinks" required="false" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>

<div class="header">
	
	<cms:pageSlot position="TopHeaderSlot" var="component">
		<cms:component component="${component}"/>
	</cms:pageSlot>
	
	<div class="header-container">
		<div class="header-left-side"></div>
		<cms:pageSlot position="SiteLogo" var="logo" element="div" class="header-title">
			<cms:component component="${logo}" />
		</cms:pageSlot>
        <c:set var="visible" value="${(empty hideHeaderLinks) ? true : not hideHeaderLinks}" />
		<div class="header-right-side">
		<div class="currency-selector dropdown">
		<cms:pageSlot position="CurrencySelector" var="component" >
           <cms:component component="${component}" />
        </cms:pageSlot>
        </div>
		<c:if test="${visible}">
			<sec:authorize access="!hasRole('ROLE_ANONYMOUS')">
				<a href="<c:url value='/logout'/>" class="logout-link">
					<spring:theme code="header.link.logout" />
				</a>
			</sec:authorize>
		</c:if>
		</div>
	</div>
</div>
