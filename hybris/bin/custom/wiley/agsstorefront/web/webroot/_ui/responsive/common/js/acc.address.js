ACC.address = {

	_autoload: [
		"bindToChangeAddressButton",
		"bindCreateUpdateAddressForm",
		"bindSuggestedDeliveryAddresses",
		"bindCountrySpecificAddressForms",
		"showAddressFormButtonPanel",
		"bindViewAddressBook",
		"bindToColorboxClose",
		"showRemoveAddressFromBookConfirmation",
		"backToListAddresses"
	],

	spinner: $("<img src='" + ACC.config.commonResourcePath + "/images/spinner.gif' />"),
	addressID: '',

	handleChangeAddressButtonClick: function ()
	{


		ACC.address.addressID = ($(this).data("address")) ? $(this).data("address") : '';
		$('#summaryDeliveryAddressFormContainer').show();
		$('#summaryOverlayViewAddressBook').show();
		$('#summaryDeliveryAddressBook').hide();


		$.getJSON(getDeliveryAddressesUrl, ACC.address.handleAddressDataLoad);
		return false;
	},

	handleAddressDataLoad: function (data)
	{
		ACC.address.setupDeliveryAddressPopupForm(data);

		// Show the delivery address popup
		ACC.colorbox.open("",{
		 	inline: true,
			href: "#summaryDeliveryAddressOverlay",
			overlayClose: false,
			onOpen: function (){
				// empty address form fields
				ACC.address.emptyAddressForm();
				$(document).on('change', '#saveAddress', function ()
				{
					var saveAddressChecked = $(this).prop('checked');
					$('#defaultAddress').prop('disabled', !saveAddressChecked);
					if (!saveAddressChecked)
					{
						$('#defaultAddress').prop('checked', false);
					}
				});
			}
		});

	},

	setupDeliveryAddressPopupForm: function (data)
	{
		// Fill the available delivery addresses
		$('#summaryDeliveryAddressBook').html($('#deliveryAddressesTemplate').tmpl({addresses: data}));
		// Handle selection of address
		$('#summaryDeliveryAddressBook button.use_address').click(ACC.address.handleSelectExistingAddressClick);
		// Handle edit address
		$('#summaryDeliveryAddressBook button.edit').click(ACC.address.handleEditAddressClick);
		// Handle set default address
		$('#summaryDeliveryAddressBook button.default').click(ACC.address.handleDefaultAddressClick);
	},

	emptyAddressForm: function ()
	{
		var options = {
			url: getDeliveryAddressFormUrl,
			data: {addressId: ACC.address.addressID, createUpdateStatus: ''},
			type: 'GET',
			success: function (data)
			{
				$('#summaryDeliveryAddressFormContainer').html(data);
				ACC.address.bindCreateUpdateAddressForm();
			}
		};

		$.ajax(options);
	},

	handleSelectExistingAddressClick: function ()
	{
		var addressId = $(this).attr('data-address');
		$.postJSON(setDeliveryAddressUrl, {addressId: addressId}, ACC.address.handleSelectExitingAddressSuccess);
		return false;
	},

	handleEditAddressClick: function ()
	{

		$('#summaryDeliveryAddressFormContainer').show();
		$('#summaryOverlayViewAddressBook').show();
		$('#summaryDeliveryAddressBook').hide();

		var addressId = $(this).attr('data-address');
		var options = {
			url: getDeliveryAddressFormUrl,
			data: {addressId: addressId, createUpdateStatus: ''},
			target: '#summaryDeliveryAddressFormContainer',
			type: 'GET',
			success: function (data)
			{
				ACC.address.bindCreateUpdateAddressForm();
				ACC.colorbox.resize();
			},
			error: function (xht, textStatus, ex)
			{
				alert("Failed to update cart. Error details [" + xht + ", " + textStatus + ", " + ex + "]");
			}
		};

		$(this).ajaxSubmit(options);
		return false;
	},

	handleDefaultAddressClick: function ()
	{
		var addressId = $(this).attr('data-address');
		var options = {
			url: setDefaultAddressUrl,
			data: {addressId: addressId},
			type: 'GET',
			success: function (data)
			{
				ACC.address.setupDeliveryAddressPopupForm(data);
			},
			error: function (xht, textStatus, ex)
			{
				alert("Failed to update address book. Error details [" + xht + ", " + textStatus + ", " + ex + "]");
			}
		};

		$(this).ajaxSubmit(options);
		return false;
	},

	handleSelectExitingAddressSuccess: function (data)
	{
		if (data != null)
		{
			ACC.refresh.refreshPage(data);
			ACC.colorbox.close();
		}
		else
		{
			alert("Failed to set delivery address");
		}
	},

	bindCreateUpdateAddressForm: function ()
	{
		$('.create_update_address_form').each(function ()
		{
			var options = {
				type: 'POST',
				beforeSubmit: function ()
				{
					$('#checkout_delivery_address').block({ message: ACC.address.spinner });
				},
				success: function (data)
				{
					$('#summaryDeliveryAddressFormContainer').html(data);
					var status = $('.create_update_address_id').attr('status');
					if (status != null && "success" === status.toLowerCase())
					{
						ACC.refresh.getCheckoutCartDataAndRefreshPage();
						ACC.colorbox.close();
					}
					else
					{
						ACC.address.bindCreateUpdateAddressForm();
						ACC.colorbox.resize();
					}
				},
				error: function (xht, textStatus, ex)
				{
					alert("Failed to update cart. Error details [" + xht + ", " + textStatus + ", " + ex + "]");
				},
				complete: function ()
				{
					$('#checkout_delivery_address').unblock();
				}
			};

			$(this).ajaxForm(options);
		});
	},

	refreshDeliveryAddressSection: function (data)
	{
		$('.summaryDeliveryAddress').replaceWith($('#deliveryAddressSummaryTemplate').tmpl(data));

	},

	bindSuggestedDeliveryAddresses: function ()
	{
		var status = $('.add_edit_delivery_address_id').attr('status');
		if (status != null && "hasSuggestedAddresses" == status)
		{
			ACC.address.showSuggestedAddressesPopup();
		}
	},

	showSuggestedAddressesPopup: function ()
	{

		ACC.colorbox.open("",{
			href: "#popup_suggested_delivery_addresses",
			inline: true,
			overlayClose: false,
			width: 525
		});
	},

	bindCountrySpecificAddressForms: function (){
		
		$(document).on("change",'#countrySelector select', function (){
			var countryIsocode = $(this).val();
			
			var options = $('#wileyAddressForm').serialize();
			ACC.address.displayCountrySpecificAddressForm(options, ACC.address.showAddressFormButtonPanel, countryIsocode);
		});
		
		$(document).on("change", "#addressRegion", function(){
			var countryIsocode = $("#countrySelector select option:selected").val();
			ACC.address.updateTaxInCart(countryIsocode);
		});
		
		$("#wileyAddressForm").find('input:text').each(function(index, element){
			if(this.id != 'addressLine2'){
				$(document).on("focusout", "#" + this.id, function()
				{
					ACC.address.saveNewAddressAndCalculateTax();
				});
			}
		});
	},

	showAddressFormButtonPanel: function ()
	{
		if ($('#countrySelector :input').val() !== '')
		{
			$('#addressform_button_panel').show();
		}
	},

	bindToColorboxClose: function ()
	{
		$(document).on("click", ".closeColorBox", function ()
		{
			ACC.colorbox.close();
		})
	},


	displayCountrySpecificAddressForm: function (options, callback, countryIsocode)
	{
		$.ajax({
			url: ACC.config.encodedContextPath + '/my-account/addressform',
			async: true,
			data: options,
			dataType: "html"
		}).done(function (data)
				{
					if (ACC.address.isResponseLoginPage(data)){
						ACC.address.makeRedirectToLoginPage();
					} else {
						$("#i18nAddressForm").html($(data).html());
						if (typeof callback == 'function')
						{
							callback.call();
						}
						ACC.address.updateTaxInCart(countryIsocode);
					}
				});
	},
	
	updateTaxInCart: function (countryIsocode)
	{

		var customerHasSavedAddress = $("#customerHasSavedAddress").val();
		if(customerHasSavedAddress != 'true'){
			ACC.address.saveNewAddressAndCalculateTax();
		} 
		else if(ACC.address.orderDetailsElementExists())
		{
			var regionIsocode = '';
			var countryHasRegions = $("#countryHasRegions").val();
			if(countryHasRegions == 'true'){
				regionIsocode = $("#addressRegion option:selected").val();
			}
			var options = {
				'countryIsocode': countryIsocode,
				'regionIsocode': regionIsocode
			};
			$.ajax({
				url: ACC.config.encodedContextPath + '/checkout/multi/billing-address/updateTax',
				async: true,
				data: options,
				dataType: "html",
			}).done(function (data, status, xhr)
				{
					if (ACC.address.isResponseLoginPage(data)){
						ACC.address.makeRedirectToLoginPage();
					} else {
						var responseRegion = xhr.getResponseHeader("regionIsocode"),
						countryIsocode = $("#countrySelector select option:selected").val(),
						currentRegion = $("#addressRegion").find('option:selected').val();
						if (countryHasRegions == 'false' || currentRegion == '' || responseRegion == currentRegion) {
							$("div.order-details-info").replaceWith(data);
						}
					}
				})
		}
	},
	
	saveNewAddressAndCalculateTax: function()
	{
		var customerHasSavedAddress = $("#customerHasSavedAddress").val();
		if(customerHasSavedAddress != 'true'){
			if(ACC.address.addressIsComplete()){
				$.ajax({
				url: ACC.config.encodedContextPath + '/checkout/multi/billing-address/saveAddressAndCalculateTax',
				async: true,
				type: "POST",
				data: $('#wileyAddressForm').serialize(),
				dataType: "html",
				}).done(function (data)
					{
						if (ACC.address.isResponseLoginPage(data)){
							ACC.address.makeRedirectToLoginPage();
						} else {
							$("div.order-details-info").replaceWith(data);
							$("#customerHasSavedAddress").val("true");
						}
					})
				}
			
		}
	},
	
	addressIsComplete: function()
	{
		var isCompete = true;
		$("#wileyAddressForm").find('input:text, select').each(function(index, element){
			if(this.value === '' && this.id != 'addressLine2'){
				isCompete = false;
			}
		});
		return isCompete;
	},
	
	orderDetailsElementExists: function()
	{
		return $("div.order-details-info").length > 0;
	},
	
	isResponseLoginPage: function(data){
		return $(data).find("#wileyRegisterForm").length || $(data).find("#loginForm").length
	},
	
	makeRedirectToLoginPage: function(){
		window.location.href =  ACC.config.encodedContextPath + '/login/checkout';
	},

	bindToChangeAddressButton: function ()
	{
		$(document).on("click", '.summaryDeliveryAddress .editButton', ACC.address.handleChangeAddressButtonClick);
	},

	bindViewAddressBook: function ()
	{

		$(document).on("click",".js-address-book",function(e){
			e.preventDefault();

			ACC.colorbox.open("Saved Addresses",{
				href: "#addressbook",
				inline: true,
				width:"320px",
			});
			
		})

		
		$(document).on("click", '#summaryOverlayViewAddressBook', function ()
		{
			$('#summaryDeliveryAddressFormContainer').hide();
			$('#summaryOverlayViewAddressBook').hide();
			$('#summaryDeliveryAddressBook').show();
			ACC.colorbox.resize();
		});
	},
	
	showRemoveAddressFromBookConfirmation: function ()
	{
		$(document).on("click", ".removeAddressFromBookButton", function ()
		{
			var addressId = $(this).data("addressId");
			var popupTitle = $(this).data("popupTitle");

			ACC.colorbox.open(popupTitle,{
				inline: true,
				height: false,
				href: "#popup_confirm_address_removal_" + addressId,
				onComplete: function ()
				{

					$(this).colorbox.resize();
				}
			});

		})
	},

	backToListAddresses: function(){
		$(".addressBackBtn").on("click", function(){
			var sUrl = $(this).data("backToAddresses");
			window.location = sUrl;
		});
	}
};

