ACC.subscription = {
	_autoload: [
		"bindAutoRenew"
	],

	bindAutoRenew: function (){
		$(document).on("change",'#auto_renew', function () {
			var renewCheckbox = $(this);
			var renewForm = $(this.form);
			var url = renewForm.attr('action');
			$.post(url, renewForm.serialize(),  function(data) {
				$('.site-form.subscription-form').parent().html(data);
			});
			renewCheckbox.attr("disabled", true);
		})
	}
};
