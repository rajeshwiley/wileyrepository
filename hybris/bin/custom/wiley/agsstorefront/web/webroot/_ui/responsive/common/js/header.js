$(document).ready(function(){
	$("#currency-selector").on('click', 'li', function(e) {
		var $this = $(this);
		var selectedValue = $this.data('value');

		$(e.delegateTarget).find('.dropdown-text').html($this.text());

		$this.siblings().removeClass('current');
		$this.addClass('current');
	});
});

