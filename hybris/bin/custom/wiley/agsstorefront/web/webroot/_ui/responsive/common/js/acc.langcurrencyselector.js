ACC.langcurrency = {

  	_autoload: [
  		"bindLangCurrencySelector"
  	],

  	bindLangCurrencySelector: function (){

  		$('#lang-selector').change(function(){
  			$('#lang-form').submit();
  		});

    $('#currency-selector .dropdown-menu li').on('click', function (event) {
        var currency = $(this).data().value;
            $("#currency-form input[name='code']").val(currency);
            $('#currency-form').submit();
        });
  	}
};