/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.ags.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commerceservices.customer.TokenInvalidatedException;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.core.wileycom.exceptions.PasswordUpdateException;
import com.wiley.facades.welags.customer.WelAgsCustomerFacade;
import com.wiley.welags.storefrontcommons.constants.WelagsstorefrontcommonsConstants;
import com.wiley.welags.storefrontcommons.controllers.pages.AbstractWelAgsPasswordResetPageController;
import com.wiley.welags.storefrontcommons.forms.WileyUpdatePwdForm;
import com.wiley.welags.storefrontcommons.forms.validation.groups.WileyUpdatePwdFormValidationOrder;


/**
 * Controller for the forgotten password pages. Supports requesting a password reset email as well as changing the
 * password once you have got the token that was sent via email.
 */
@Controller
@Scope("tenant")
@RequestMapping(value = "/login/pw")
public class PasswordResetPageController extends AbstractWelAgsPasswordResetPageController
{
	private static final Logger LOG = LogManager.getLogger(PasswordResetPageController.class);

	private static final String MSG_UNABLE_CHANGE_PASSWORD = "Unable to change password";

	@Resource(name = "customerFacade")
	private WelAgsCustomerFacade customerFacade;

	@RequestMapping(value = "/change", method = RequestMethod.POST)
	public String changePassword(
			@Validated(value = { WileyUpdatePwdFormValidationOrder.class }) final WileyUpdatePwdForm form,
			final BindingResult bindingResult, final Model model, final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException
	{
		validatePwdAndCheckPwdEqual(form, bindingResult);
		if (bindingResult.hasErrors())
		{
			prepareErrorMessage(model, UPDATE_PWD_CMS_PAGE);
			storeCmsPageInModel(model, getContentPageForLabelOrId(RESET_PASSWORD_CMS_PAGE_LABEL));
			storeEmailInModel(form.getToken(), model);
			return WelagsstorefrontcommonsConstants.Views.Pages.Password.PASSWORD_RESET_CHANGE_PAGE;
		}
		if (!StringUtils.isBlank(form.getToken()))
		{
			try
			{
				customerFacade.updatePassword(form.getToken(), form.getPwd());
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
						"account.confirmation.password.updated");
			}
			catch (final TokenInvalidatedException e)
			{
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
						"updatePwd.token.invalidated");
				LOG.debug(MSG_UNABLE_CHANGE_PASSWORD, e);
			}
			catch (final IllegalArgumentException | PasswordUpdateException e)
			{
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "updatePwd.token.invalid");
				LOG.debug(MSG_UNABLE_CHANGE_PASSWORD, e);
			}
			catch (final RuntimeException e)
			{
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "updatePwd.token.invalid");
				LOG.error(MSG_UNABLE_CHANGE_PASSWORD, e);
			}
		}
		switch (form.getRedirectType())
		{
			case ch:
				return REDIRECT_CHECKOUT_LOGIN;
			case pl:
				return REDIRECT_PIN_LOGIN;
			default:
				return REDIRECT_LOGIN;
		}
	}

	@Override
	protected Logger getLogger()
	{
		return LOG;
	}
}
