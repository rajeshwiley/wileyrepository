package com.wiley.ags.storefront.voucher.impl;


import de.hybris.platform.commercefacades.voucher.exceptions.VoucherOperationException;
import de.hybris.platform.commercefacades.voucher.impl.DefaultVoucherFacade;
import de.hybris.platform.voucher.model.VoucherModel;


public class AgsVoucherFacade extends DefaultVoucherFacade
{
	/**
	 * Checking state of cart after redeem last voucher
	 *
	 * @param lastVoucherCode
	 * @param lastVoucher
	 */
	@Override
	protected void checkCartAfterApply(final String lastVoucherCode, final VoucherModel lastVoucher)
			throws VoucherOperationException
	{
		// for current realization we do not need to check if voucher amount is grater than a cart totalAmount
	}
}
