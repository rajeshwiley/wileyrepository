/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.ags.storefront.controllers.cms;

import de.hybris.platform.acceleratorcms.model.components.MiniCartComponentModel;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.product.data.PriceData;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.wiley.welags.storefrontcommons.constants.WelagsstorefrontcommonsConstants;


/**
 * Controller for CMS MiniCartComponent
 */
@Controller("MiniCartComponentController")
@Scope("tenant")
@RequestMapping(value = WelagsstorefrontcommonsConstants.Actions.Cms.MINI_CART_COMPONENT)
public class MiniCartComponentController extends AbstractCMSComponentController<MiniCartComponentModel>
{
	/**
	 * The constant TOTAL_PRICE.
	 */
	public static final String TOTAL_PRICE = "totalPrice";
	/**
	 * The constant TOTAL_ITEMS.
	 */
	public static final String TOTAL_ITEMS = "totalItems";
	/**
	 * The constant TOTAL_DISPLAY.
	 */
	public static final String TOTAL_DISPLAY = "totalDisplay";
	/**
	 * The constant TOTAL_NO_DELIVERY.
	 */
	public static final String TOTAL_NO_DELIVERY = "totalNoDelivery";
	/**
	 * The constant SUB_TOTAL.
	 */
	public static final String SUB_TOTAL = "subTotal";

	@Resource(name = "cartFacade")
	private CartFacade cartFacade;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final MiniCartComponentModel component)
	{
		final CartData cartData = cartFacade.getMiniCart();
		model.addAttribute(SUB_TOTAL, cartData.getSubTotal());
		if (cartData.getDeliveryCost() != null)
		{
			final PriceData withoutDelivery = cartData.getDeliveryCost();
			withoutDelivery.setValue(cartData.getTotalPrice().getValue().subtract(cartData.getDeliveryCost().getValue()));
			model.addAttribute(TOTAL_NO_DELIVERY, withoutDelivery);
		}
		else
		{
			model.addAttribute(TOTAL_NO_DELIVERY, cartData.getTotalPrice());
		}
		model.addAttribute(TOTAL_PRICE, cartData.getTotalPrice());
		model.addAttribute(TOTAL_DISPLAY, component.getTotalDisplay());
		model.addAttribute(TOTAL_ITEMS, cartData.getTotalUnitCount());
	}
}
