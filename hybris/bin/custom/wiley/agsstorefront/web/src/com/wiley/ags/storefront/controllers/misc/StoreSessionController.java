/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.ags.storefront.controllers.misc;

import de.hybris.platform.acceleratorservices.uiexperience.UiExperienceService;
import de.hybris.platform.acceleratorservices.urlencoder.UrlEncoderService;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.AbstractController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.commercefacades.storesession.StoreSessionFacade;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.wiley.facades.user.WileyUserFacade;

import static com.wiley.storefrontcommons.constants.WileystorefrontcommonsConstants.REDIRECT_TO_404;


/**
 * Controller for store session. Used to change the session language, currency and experience level.
 */
@Controller
@Scope("tenant")
@RequestMapping("/_s")
public class StoreSessionController extends AbstractController
{
	private static final Logger LOG = Logger.getLogger(StoreSessionController.class);

	@Resource(name = "storeSessionFacade")
	private StoreSessionFacade storeSessionFacade;

	@Resource(name = "wileyUserFacade")
	private WileyUserFacade wileyUserFacade;

	@Resource(name = "uiExperienceService")
	private UiExperienceService uiExperienceService;

	@Resource(name = "enumerationService")
	private EnumerationService enumerationService;

	@Resource(name = "urlEncoderService")
	private UrlEncoderService urlEncoderService;

	/**
	 * Select currency string.
	 *
	 * @param isoCode
	 * 		the iso code
	 * @param request
	 * 		the request
	 * @return the string
	 */
	@RequestMapping(value = "/currency", method = { RequestMethod.GET, RequestMethod.POST })
	public String selectCurrency(@RequestParam("code") final String isoCode, final HttpServletRequest request)
	{
		storeSessionFacade.setCurrentCurrency(isoCode);
		wileyUserFacade.syncSessionCurrency();
		return getReturnRedirectUrl(request);
	}

	/**
	 * Sets hide ui experience level override prompt.
	 *
	 * @param request
	 * 		the request
	 * @param flag
	 * 		the flag
	 */
	protected void setHideUiExperienceLevelOverridePrompt(final HttpServletRequest request, final boolean flag)
	{
		request.getSession().setAttribute("hideUiExperienceLevelOverridePrompt", Boolean.valueOf(flag));
	}

	/**
	 * Gets return redirect url.
	 *
	 * @param request
	 * 		the request
	 * @return the return redirect url
	 */
	protected String getReturnRedirectUrl(final HttpServletRequest request)
	{
		final String referer = request.getHeader("Referer");
		if (referer != null && !referer.isEmpty())
		{
			return REDIRECT_PREFIX + referer;
		}
		return REDIRECT_PREFIX + '/';
	}

	/**
	 * Handle unknown identifier exception string.
	 *
	 * @param exception
	 * 		the exception
	 * @param request
	 * 		the request
	 * @return the string
	 */
	@ExceptionHandler(UnknownIdentifierException.class)
	public String handleUnknownIdentifierException(final UnknownIdentifierException exception, final HttpServletRequest request)
	{
		final Map<String, Object> currentFlashScope = RequestContextUtils.getOutputFlashMap(request);
		currentFlashScope.put(GlobalMessages.ERROR_MESSAGES_HOLDER, exception.getMessage());
		return REDIRECT_TO_404;
	}
}
