package com.wiley.ags.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.order.InvalidCartException;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.wiley.facades.welags.pin.exception.PinOperationException;
import com.wiley.welags.storefrontcommons.form.PinActivationForm;
import com.wiley.welags.storefrontcommons.validators.WelAgsPinActivationValidator;

import static com.wiley.welags.storefrontcommons.constants.WelagsstorefrontcommonsConstants.Keys.FORM_GLOBAL_ERROR_MESSAGE;
import static com.wiley.welags.storefrontcommons.constants.WelagsstorefrontcommonsConstants.Keys.PIN_FIELD_ERROR;



/**
 * Controller for the PIN Activation page.
 */
@Controller
@RequestMapping(value = "/pin/activate")
public class PinActivationPageController extends AbstractPageController
{
	@Resource
	private WelAgsPinActivationValidator pinActivationValidator;
	
    @ModelAttribute("pinActivationForm")
    public PinActivationForm initializeForm() {
        return new PinActivationForm();
    }

    @RequestMapping(method = RequestMethod.GET)
    public String showPinActivationPage(@ModelAttribute final PinActivationForm pinActivationForm,
            final BindingResult bindingResult, final Model model) throws CMSItemNotFoundException
    {
        bindingResult.addError((FieldError) model.asMap().get(PIN_FIELD_ERROR));
        setUpMetaDataForContentPage(model, (ContentPageModel) getCmsPage());
        model.addAttribute(pinActivationForm);
        storeCmsPageInModel(model, getCmsPage());
        return getViewForPage(model);
    }

    @RequestMapping(method = RequestMethod.POST)
    public String activatePin(@Valid final PinActivationForm form, final BindingResult bindingResult, final Model model,
            final HttpSession session)
            throws CMSItemNotFoundException, InvalidCartException, PinOperationException
    {
    	pinActivationValidator.validate(form, bindingResult);
        if (bindingResult.hasErrors())
        {
            GlobalMessages.addErrorMessage(model, FORM_GLOBAL_ERROR_MESSAGE);
            storeCmsPageInModel(model, getCmsPage());
            return getViewForPage(model);
        }
        else
        {
            session.setAttribute("pinActivationForm", form);
            return REDIRECT_PREFIX + "/pin/order";
        }
    }

    protected AbstractPageModel getCmsPage() throws CMSItemNotFoundException {
        return this.getContentPageForLabelOrId("pinActivation");
    }
}
