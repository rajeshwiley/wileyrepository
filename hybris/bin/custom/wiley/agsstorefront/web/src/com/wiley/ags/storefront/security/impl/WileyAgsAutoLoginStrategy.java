package com.wiley.ags.storefront.security.impl;

import de.hybris.platform.acceleratorstorefrontcommons.security.AutoLoginStrategy;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.WebAuthenticationDetails;

import com.wiley.storefrontcommons.security.impl.WileyAutoLoginStrategy;
import com.wiley.welags.storefrontcommons.security.WelAgsAuthenticationStrategy;


/**
 * Default implementation of {@link AutoLoginStrategy}
 */
public class WileyAgsAutoLoginStrategy extends WileyAutoLoginStrategy
{
	private WelAgsAuthenticationStrategy authenticationStrategy;

	@Override
	public void login(final String username, final String password, final HttpServletRequest request,
			final HttpServletResponse response)
	{
		final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
		token.setDetails(new WebAuthenticationDetails(request));
		try
		{
			final Authentication authentication = getAuthenticationManager().authenticate(token);
			getAuthenticationStrategy().setSpringSecurityToken(authentication);
			getCustomerFacade().loginSuccess();
			getGuidCookieStrategy().setCookie(request, response);
			getRememberMeServices().loginSuccess(request, response, token);
		}
		catch (final Exception e)
		{
			getAuthenticationStrategy().setSpringSecurityToken(null);
			LOG.error("Failure during autoLogin", e);
		}
	}

	public WelAgsAuthenticationStrategy getAuthenticationStrategy()
	{
		return authenticationStrategy;
	}
	
	@Required
	public void setAuthenticationStrategy(
			final WelAgsAuthenticationStrategy authenticationStrategy)
	{
		this.authenticationStrategy = authenticationStrategy;
	}
}
