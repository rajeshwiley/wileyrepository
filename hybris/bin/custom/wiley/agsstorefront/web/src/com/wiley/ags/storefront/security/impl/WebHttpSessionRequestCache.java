/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.ags.storefront.security.impl;

import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.savedrequest.DefaultSavedRequest;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.security.web.util.UrlUtils;

import com.wiley.storefrontcommons.security.impl.WileyWebHttpSessionRequestCache;


/**
 * Extension of HttpSessionRequestCache that allows pass through of cookies from the current request. This is required
 * to allow the GUIDInterceptor to see the secure cookie written during authentication.
 *
 * The <tt>RequestCache</tt> stores the <tt>SavedRequest</tt> in the HttpSession, this is then restored perfectly.
 * Unfortunately the saved request also hides new cookies that have been written since the saved request was created.
 * This implementation allows the current request's cookie values to override the cookies within the saved request.
 */
public class WebHttpSessionRequestCache extends WileyWebHttpSessionRequestCache
{
	@Override
	public void saveRequest(final HttpServletRequest request, final HttpServletResponse response)
	{
		//this might be called while in ExceptionTranslationFilter#handleSpringSecurityException in this case base implementation
		if (SecurityContextHolder.getContext().getAuthentication() == null)
		{
			if (logger.isDebugEnabled())
			{
				logger.debug("WebHttp: no authentication ", new RuntimeException("trace"));
			}
			super.saveRequest(request, response);
		}
		else
		{
			final SavedRequest savedBefore = getRequest(request, response);
			if (savedBefore != null)//to not override request saved by ExceptionTranslationFilter#handleSpringSecurityException
			{
				return;
			}

			if (getRequestMatcher().matches(request))
			{
				final DefaultSavedRequest savedRequest = new DefaultSavedRequest(request, getPortResolver())
				{
					private final String referer = request.getHeader(REFERER);
					private final String contextPath = request.getContextPath();

					@Override
					public String getRedirectUrl()
					{
						return calculateRelativeRedirectUrl(contextPath, referer);
					}
				};

				if (isCreateSessionAllowed() || request.getSession(false) != null)
				{
					request.getSession().setAttribute(SAVED_REQUEST, savedRequest);
					if (logger.isDebugEnabled())
					{
						logger.debug("WebHttp: DefaultSavedRequest added to Session: " + savedRequest, new RuntimeException(
								"trace"));
					}
				}
			}
			else
			{
				logger.debug("Request not saved as configured RequestMatcher did not match");
			}
		}
	}

	/**
	 * Calculate relative redirect url string.
	 *
	 * @param contextPath
	 * 		the context path
	 * @param url
	 * 		the url
	 * @return the string
	 */
	public String calculateRelativeRedirectUrl(final String contextPath, final String url)
	{
		if (UrlUtils.isAbsoluteUrl(url))
		{
			String relUrl = url.substring(url.indexOf("://") + 3);
			String modifiedContextPath = (StringUtils.isNotEmpty(contextPath)) ? contextPath : SLASH;
			final String urlEncodingAttributes = getSessionService().getAttribute(WebConstants.URL_ENCODING_ATTRIBUTES);
			if (urlEncodingAttributes != null && !url.contains(urlEncodingAttributes)
					&& modifiedContextPath.contains(urlEncodingAttributes))
			{
				modifiedContextPath = StringUtils.remove(modifiedContextPath, urlEncodingAttributes);
			}

			if (SLASH.equals(modifiedContextPath))
			{
				relUrl = relUrl.substring(relUrl.indexOf(modifiedContextPath));
			}
			else
			{
				relUrl = relUrl.substring(relUrl.indexOf(modifiedContextPath) + modifiedContextPath.length());
			}

			return (StringUtils.isEmpty(relUrl)) ? "/" : relUrl;
		}
		else
		{
			return url;
		}
	}

}
