package com.wiley.ags.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractSearchPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.wiley.facades.product.data.WileySubscriptionData;
import com.wiley.facades.subscription.WileySubscriptionFacade;
import com.wiley.welags.storefrontcommons.constants.WelagsstorefrontcommonsConstants;


/**
 * Created by Uladzimir_Barouski on 12/15/2015.
 */
@Controller
@RequestMapping("/my-account")
public class AccountSubscriptionPageController extends AbstractSearchPageController
{

	private static final String MANAGE_SUBSCRIPTION_CMS_PAGE = "manage-subscription";

	private static final Logger LOG = Logger.getLogger(AccountSubscriptionPageController.class);

	@Autowired
	private ResourceBreadcrumbBuilder accountBreadcrumbBuilder;

	@Autowired
	private WileySubscriptionFacade wileySubscriptionFacade;

	/**
	 * Get Subscriptions.
	 *
	 * @param model
	 * 		the model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 * 		the cms item not found exception
	 */
	@RequestMapping(value = "/manage-subscription", method = RequestMethod.GET)
	@RequireHardLogIn
	public String subscriptionDetails(final Model model) throws CMSItemNotFoundException
	{
		setPageInfo(model, MANAGE_SUBSCRIPTION_CMS_PAGE);
		WileySubscriptionData subscription;
		subscription = wileySubscriptionFacade.getActiveSubscription();
		if (subscription != null && subscription.isRenewalInProgress())
		{
			GlobalMessages.addInfoMessage(model, "text.account.subscription.renewalInProgress");
		}

		model.addAttribute("wileySubscriptionData", subscription);

		return getViewForPage(model);
	}

	/**
	 * Updates subscription.
	 *
	 * @param code
	 * 		the subscription code
	 * @param autoRenew
	 * 		the subscription auto-renewal attribute
	 * @param model
	 * 		the model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 * 		the cms item not found exception
	 */
	@RequestMapping(value = "/manage-subscription", method = RequestMethod.POST)
	@RequireHardLogIn
	public String updateAutoRenew(@RequestParam final String code,
			@RequestParam(required = false, defaultValue = "false") final boolean autoRenew,
			final Model model) throws CMSItemNotFoundException
	{
		WileySubscriptionData subscriptionData = null;
		try
		{
			subscriptionData = wileySubscriptionFacade.updateAutoRenew(code, autoRenew);
			if (subscriptionData.isAutoRenew() != autoRenew)
			{
				GlobalMessages.addErrorMessage(model, "text.account.subscription.autorenew.error");
			}
		}
		catch (UnknownIdentifierException e)
		{
			LOG.error(e.getMessage());
			GlobalMessages.addErrorMessage(model, "text.account.subscription.autorenew.error");
		}
		model.addAttribute("wileySubscriptionData", subscriptionData);
		return WelagsstorefrontcommonsConstants.Views.Fragments.Account.ACCOUNT_SUBSCRIPTION;
	}

	private void setPageInfo(final Model model, final String cmsPageLabelOrId)
			throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(cmsPageLabelOrId));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(cmsPageLabelOrId));
	}
}
