/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.ags.storefront.controllers.cms;

import de.hybris.platform.acceleratorcms.model.components.PurchasedCategorySuggestionComponentModel;
import de.hybris.platform.acceleratorcms.model.components.SimpleSuggestionComponentModel;
import de.hybris.platform.commercefacades.product.data.ProductData;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.wiley.facades.suggestion.SimpleSuggestionFacade;
import com.wiley.welags.storefrontcommons.constants.WelagsstorefrontcommonsConstants;


/**
 * Controller for CMS PurchasedCategorySuggestionComponent
 */
@Controller("PurchasedCategorySuggestionComponentController")
@Scope("tenant")
@RequestMapping(value = WelagsstorefrontcommonsConstants.Actions.Cms.PURCHASED_CATEGORY_SUGGESTION_COMPONENT)
public class PurchasedCategorySuggestionComponentController extends
		AbstractCMSComponentController<PurchasedCategorySuggestionComponentModel>
{
	@Resource(name = "simpleSuggestionFacade")
	private SimpleSuggestionFacade simpleSuggestionFacade;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model,
			final PurchasedCategorySuggestionComponentModel component)
	{
		final List<ProductData> productSuggestions = simpleSuggestionFacade
				.getReferencesForPurchasedInCategory(component.getCategory().getCode(), component.getProductReferenceTypes(),
						component.isFilterPurchased(), component.getMaximumNumberProducts());

		model.addAttribute("title", component.getTitle());
		model.addAttribute("suggestions", productSuggestions);
	}

	@Override
	protected String getView(final PurchasedCategorySuggestionComponentModel component)
	{
		return WelagsstorefrontcommonsConstants.Views.Cms.COMPONENT_PREFIX
				+ StringUtils.lowerCase(SimpleSuggestionComponentModel._TYPECODE);
	}
}
