package com.wiley.ags.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractSearchPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.AddressForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.UpdateEmailForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.UpdatePasswordForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.EmailValidator;
import de.hybris.platform.acceleratorstorefrontcommons.forms.verification.AddressVerificationResultHandler;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.address.AddressVerificationFacade;
import de.hybris.platform.commercefacades.address.data.AddressVerificationResult;
import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.order.CheckoutFacade;
import de.hybris.platform.commercefacades.order.OrderFacade;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.OrderHistoryData;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.TitleData;
import de.hybris.platform.commercefacades.user.exceptions.PasswordMismatchException;
import de.hybris.platform.commerceservices.address.AddressVerificationDecision;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.commerceservices.util.ResponsiveUtils;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.SmartValidator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.facades.welags.customer.WelAgsCustomerFacade;
import com.wiley.welags.storefrontcommons.constants.WelagsstorefrontcommonsConstants;
import com.wiley.welags.storefrontcommons.forms.WelAgsAddressForm;
import com.wiley.welags.storefrontcommons.forms.WileyUpdateProfileForm;
import com.wiley.welags.storefrontcommons.forms.validation.WileyPasswordValidator;
import com.wiley.welags.storefrontcommons.forms.validation.WileyUpdateProfileFormValidator;
import com.wiley.welags.storefrontcommons.security.WelAgsAuthenticationStrategy;


/**
 * Controller for my account pages
 */
@Controller
@RequestMapping("/my-account")
public class AccountPageController extends AbstractSearchPageController
{
	// Internal Redirects
	private static final String REDIRECT_TO_ADDRESS_BOOK_PAGE = REDIRECT_PREFIX + "/my-account/address-book";
	private static final String REDIRECT_TO_PAYMENT_INFO_PAGE = REDIRECT_PREFIX + "/my-account/payment-details";
	private static final String REDIRECT_TO_EDIT_ADDRESS_PAGE = REDIRECT_PREFIX + "/my-account/edit-address/";
	private static final String REDIRECT_TO_UPDATE_EMAIL_PAGE = REDIRECT_PREFIX + "/my-account/update-email";
	private static final String REDIRECT_TO_UPDATE_PROFILE = REDIRECT_PREFIX + "/my-account/update-profile";
	private static final String REDIRECT_TO_PASSWORD_UPDATE_PAGE = REDIRECT_PREFIX + "/my-account/update-password";
	private static final String REDIRECT_TO_ORDER_HISTORY_PAGE = REDIRECT_PREFIX + "/my-account/orders";

	/**
	 * We use this suffix pattern because of an issue with Spring 3.1 where a Uri value is incorrectly extracted if it
	 * contains on or more '.' characters. Please see https://jira.springsource.org/browse/SPR-6164 for a discussion on
	 * the issue and future resolution.
	 */
	private static final String ORDER_CODE_PATH_VARIABLE_PATTERN = "{orderCode:.*}";
	private static final String ADDRESS_CODE_PATH_VARIABLE_PATTERN = "{addressCode:.*}";

	// CMS Pages
	private static final String ACCOUNT_CMS_PAGE = "account";
	private static final String PROFILE_CMS_PAGE = "profile";
	private static final String UPDATE_PASSWORD_CMS_PAGE = "updatePassword";
	private static final String UPDATE_PROFILE_CMS_PAGE = "update-profile";
	private static final String UPDATE_EMAIL_CMS_PAGE = "update-email";
	private static final String ADDRESS_BOOK_CMS_PAGE = "address-book";
	private static final String ADD_EDIT_ADDRESS_CMS_PAGE = "add-edit-address";
	private static final String PAYMENT_DETAILS_CMS_PAGE = "payment-details";
	private static final String ORDER_HISTORY_CMS_PAGE = "orders";
	private static final String ORDER_DETAIL_CMS_PAGE = "order";

	// Other model attributes
	private static final String EMAIL_PREFERENCE_LINK_ATTR = "wileyEmailPreferenceLink";

	private static final Logger LOG = Logger.getLogger(AccountPageController.class);

	@Autowired
	private ConfigurationService configurationService;

	@Autowired
	private I18NFacade i18NFacade;
	@Autowired
	private UserFacade userFacade;
	@Resource(name = "welAgsCustomerFacade")
	private WelAgsCustomerFacade customerFacade;
	@Autowired
	private OrderFacade orderFacade;
	@Autowired
	private CheckoutFacade acceleratorCheckoutFacade;

	@Autowired
	private ResourceBreadcrumbBuilder accountBreadcrumbBuilder;

	@Autowired
	private AddressVerificationFacade addressVerificationFacade;
	@Autowired
	private AddressVerificationResultHandler addressVerificationResultHandler;

	@Autowired
	private WileyUpdateProfileFormValidator wileyUpdateProfileFormValidator;
	@Autowired
	private EmailValidator emailValidator;
	@Resource(name = "wileyAddressValidator")
	private SmartValidator addressValidator;
	@Autowired
	private WileyPasswordValidator wileyPasswordValidator;
	@Resource(name = "welAgsAuthenticationStrategy")
	private WelAgsAuthenticationStrategy authenticationStrategy;

	/**
	 * Gets billing countries.
	 *
	 * @return the countries
	 */
	@ModelAttribute("billingCountries")
	public Collection<CountryData> getBillingCountries()
	{
		return acceleratorCheckoutFacade.getBillingCountries();
	}

	/**
	 * Gets delivery countries.
	 *
	 * @return the countries
	 */
	@ModelAttribute("countries")
	public Collection<CountryData> getDeliveryCountries()
	{
		return acceleratorCheckoutFacade.getDeliveryCountries();
	}

	/**
	 * Gets titles.
	 *
	 * @return the titles
	 */
	@ModelAttribute("titles")
	public Collection<TitleData> getTitles()
	{
		return userFacade.getTitles();
	}

	/**
	 * Gets country data map.
	 *
	 * @return the country data map
	 */
	@ModelAttribute("countryDataMap")
	public Map<String, CountryData> getCountryDataMap()
	{
		final Map<String, CountryData> countryDataMap = new HashMap<>();
		for (final CountryData countryData : getBillingCountries())
		{
			countryDataMap.put(countryData.getIsocode(), countryData);
		}
		return countryDataMap;
	}


	/**
	 * Gets country address form.
	 *
	 * @param wileyAddressForm
	 * 		the addressForm
	 * @param model
	 * 		the model
	 * @return the country address form
	 */
	@RequestMapping(value = "/addressform", method = RequestMethod.GET)
	public String getCountryAddressForm(final WelAgsAddressForm wileyAddressForm, final Model model)
	{
		final String countryIsoCode = wileyAddressForm.getCountryIso();
		model.addAttribute("billingCountries", getBillingCountries());
		model.addAttribute("regions", i18NFacade.getRegionsForCountryIso(countryIsoCode));
		model.addAttribute("wileyAddressForm", wileyAddressForm);

		return WelagsstorefrontcommonsConstants.Views.Fragments.Account.COUNTRY_ADDRESS_FORM;
	}

	/**
	 * Account string.
	 *
	 * @param model
	 * 		the model
	 * @param redirectModel
	 * 		the redirect model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 * 		the cms item not found exception
	 */
	//@RequestMapping(method = RequestMethod.GET)
	@RequireHardLogIn
	@Deprecated
	public String account(final Model model, final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		if (ResponsiveUtils.isResponsive())
		{
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "system.error.page.not.found",
					null);
			return REDIRECT_PREFIX + "/";
		}
		storeCmsPageInModel(model, getContentPageForLabelOrId(ACCOUNT_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ACCOUNT_CMS_PAGE));
		model.addAttribute("breadcrumbs", accountBreadcrumbBuilder.getBreadcrumbs(null));
		model.addAttribute("metaRobots", "noindex,nofollow");
		return getViewForPage(model);
	}

	/**
	 * Orders string.
	 *
	 * @param page
	 * 		the page
	 * @param showMode
	 * 		the show mode
	 * @param sortCode
	 * 		the sort code
	 * @param model
	 * 		the model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 * 		the cms item not found exception
	 */
	//@RequestMapping(value = "/orders", method = RequestMethod.GET)
	@RequireHardLogIn
	@Deprecated
	public String orders(@RequestParam(value = "page", defaultValue = "0") final int page,
			@RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
			@RequestParam(value = "sort", required = false) final String sortCode, final Model model)
			throws CMSItemNotFoundException
	{
		// Handle paged search results
		final PageableData pageableData = createPageableData(page, 5, sortCode, showMode);
		final SearchPageData<OrderHistoryData> searchPageData = orderFacade.getPagedOrderHistoryForStatuses(pageableData);
		populateModel(model, searchPageData, showMode);

		storeCmsPageInModel(model, getContentPageForLabelOrId(ORDER_HISTORY_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ORDER_HISTORY_CMS_PAGE));
		model.addAttribute("breadcrumbs", accountBreadcrumbBuilder.getBreadcrumbs("text.account.orderHistory"));
		model.addAttribute("metaRobots", "noindex,nofollow");
		return getViewForPage(model);
	}

	/**
	 * Order string.
	 *
	 * @param orderCode
	 * 		the order code
	 * @param model
	 * 		the model
	 * @param redirectModel
	 * 		the redirect model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 * 		the cms item not found exception
	 */
	//@RequestMapping(value = "/order/" + ORDER_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	@RequireHardLogIn
	@Deprecated
	public String order(@PathVariable("orderCode") final String orderCode, final Model model,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		try
		{
			final OrderData orderDetails = orderFacade.getOrderDetailsForCode(orderCode);
			model.addAttribute("orderData", orderDetails);

			final List<Breadcrumb> breadcrumbs = accountBreadcrumbBuilder.getBreadcrumbs(null);
			breadcrumbs.add(new Breadcrumb("/my-account/orders", getMessageSource().getMessage("text.account.orderHistory", null,
					getI18nService().getCurrentLocale()), null));
			breadcrumbs.add(new Breadcrumb("#", getMessageSource().getMessage("text.account.order.orderBreadcrumb", new Object[]
					{ orderDetails.getCode() }, "Order {0}", getI18nService().getCurrentLocale()), null));
			model.addAttribute("breadcrumbs", breadcrumbs);

		}
		catch (final UnknownIdentifierException e)
		{
			LOG.warn("Attempted to load a order that does not exist or is not visible", e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "system.error.page.not.found",
					null);
			return REDIRECT_TO_ORDER_HISTORY_PAGE;
		}
		storeCmsPageInModel(model, getContentPageForLabelOrId(ORDER_DETAIL_CMS_PAGE));
		model.addAttribute("metaRobots", "noindex,nofollow");
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ORDER_DETAIL_CMS_PAGE));
		return getViewForPage(model);
	}

	/**
	 * Profile string.
	 *
	 * @param model
	 * 		the model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 * 		the cms item not found exception
	 */
	//@RequestMapping(value = "/profile", method = RequestMethod.GET)
	@RequireHardLogIn
	@Deprecated
	public String profile(final Model model) throws CMSItemNotFoundException
	{
		final List<TitleData> titles = userFacade.getTitles();

		final CustomerData customerData = customerFacade.getCurrentCustomer();
		if (customerData.getTitleCode() != null)
		{
			model.addAttribute("title", findTitleForCode(titles, customerData.getTitleCode()));
		}

		model.addAttribute("customerData", customerData);

		setPageInfo(model, PROFILE_CMS_PAGE);

		model.addAttribute("breadcrumbs", accountBreadcrumbBuilder.getBreadcrumbs("text.account.profile"));
		model.addAttribute("metaRobots", "noindex,nofollow");
		return getViewForPage(model);
	}

	/**
	 * Find title for code title data.
	 *
	 * @param titles
	 * 		the titles
	 * @param code
	 * 		the code
	 * @return the title data
	 */
	protected TitleData findTitleForCode(final List<TitleData> titles, final String code)
	{
		if (code != null && !code.isEmpty() && titles != null && !titles.isEmpty())
		{
			for (final TitleData title : titles)
			{
				if (code.equals(title.getCode()))
				{
					return title;
				}
			}
		}
		return null;
	}

	/**
	 * Edit email string.
	 *
	 * @param model
	 * 		the model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 * 		the cms item not found exception
	 */
	//@RequestMapping(value = "/update-email", method = RequestMethod.GET)
	@RequireHardLogIn
	@Deprecated
	public String editEmail(final Model model) throws CMSItemNotFoundException
	{
		final CustomerData customerData = customerFacade.getCurrentCustomer();
		final UpdateEmailForm updateEmailForm = new UpdateEmailForm();
		updateEmailForm.setEmail(customerData.getDisplayUid());

		model.addAttribute("updateEmailForm", updateEmailForm);

		setPageInfo(model, UPDATE_EMAIL_CMS_PAGE);

		model.addAttribute("breadcrumbs", accountBreadcrumbBuilder.getBreadcrumbs("text.account.profile"));
		model.addAttribute("metaRobots", "noindex,nofollow");
		return getViewForPage(model);
	}

	/**
	 * Update email string.
	 *
	 * @param updateEmailForm
	 * 		the update email form
	 * @param bindingResult
	 * 		the binding result
	 * @param model
	 * 		the model
	 * @param redirectAttributes
	 * 		the redirect attributes
	 * @return the string
	 * @throws CMSItemNotFoundException
	 * 		the cms item not found exception
	 */
	//@RequestMapping(value = "/update-email", method = RequestMethod.POST)
	@RequireHardLogIn
	@Deprecated
	public String updateEmail(final UpdateEmailForm updateEmailForm, final BindingResult bindingResult, final Model model,
			final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		emailValidator.validate(updateEmailForm, bindingResult);
		String returnAction = REDIRECT_TO_UPDATE_EMAIL_PAGE;

		if (!bindingResult.hasErrors() && !updateEmailForm.getEmail().equals(updateEmailForm.getChkEmail()))
		{
			bindingResult.rejectValue("chkEmail", "validation.checkEmail.equals", new Object[] {},
					"validation.checkEmail.equals");
		}

		if (bindingResult.hasErrors())
		{
			returnAction = setErrorMessagesAndCMSPage(model, UPDATE_EMAIL_CMS_PAGE);
		}
		else
		{
			try
			{
				customerFacade.changeUid(updateEmailForm.getEmail(), updateEmailForm.getPassword());
				GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER,
						"text.account.profile.confirmationUpdated", null);

				authenticationStrategy.refreshSpringSecurityToken();
			}
			catch (final DuplicateUidException e)
			{
				bindingResult.rejectValue("email", "profile.email.unique");
				returnAction = setErrorMessagesAndCMSPage(model, UPDATE_EMAIL_CMS_PAGE);
			}
			catch (final PasswordMismatchException passwordMismatchException)
			{
				bindingResult.rejectValue("password", "profile.currentPassword.invalid");
				returnAction = setErrorMessagesAndCMSPage(model, UPDATE_EMAIL_CMS_PAGE);
			}
		}

		return returnAction;
	}

	/**
	 * Sets error messages and cms page.
	 *
	 * @param model
	 * 		the model
	 * @param cmsPageLabelOrId
	 * 		the cms page label or id
	 * @return the error messages and cms page
	 * @throws CMSItemNotFoundException
	 * 		the cms item not found exception
	 */
	protected String setErrorMessagesAndCMSPage(final Model model, final String cmsPageLabelOrId) throws CMSItemNotFoundException
	{
		GlobalMessages.addErrorMessage(model, "form.global.error");
		setPageInfo(model, cmsPageLabelOrId);

		// todo: do we need breadcrumbs?
		model.addAttribute("breadcrumbs", accountBreadcrumbBuilder.getBreadcrumbs("text.account.profile"));
		return getViewForPage(model);
	}

	private void setPageInfo(final Model model, final String cmsPageLabelOrId) throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(cmsPageLabelOrId));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(cmsPageLabelOrId));
	}

	/**
	 * Edit profile string.
	 *
	 * @param model
	 * 		the model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 * 		the cms item not found exception
	 */
	@RequestMapping(value = "/update-profile", method = RequestMethod.GET)
	@RequireHardLogIn
	public String editProfile(final Model model) throws CMSItemNotFoundException
	{
		final CustomerData customerData = customerFacade.getCurrentCustomer();
		final WileyUpdateProfileForm updateProfileForm = new WileyUpdateProfileForm();
		updateProfileForm.setFirstName(customerData.getFirstName());
		updateProfileForm.setLastName(customerData.getLastName());
		updateProfileForm.setEmailUid(customerData.getDisplayUid());

		model.addAttribute("wileyUpdateProfileForm", updateProfileForm);

		setPageInfo(model, UPDATE_PROFILE_CMS_PAGE);

		model.addAttribute(EMAIL_PREFERENCE_LINK_ATTR, getEmailPreferenceLink());

		// should we remove the next two lines ?
		model.addAttribute("breadcrumbs", accountBreadcrumbBuilder.getBreadcrumbs("text.account.profile"));
		model.addAttribute("metaRobots", "noindex,nofollow");
		return getViewForPage(model);
	}

	/**
	 * Updates profile string.
	 *
	 * @param wileyUpdateProfileForm
	 * 		the update profile form
	 * @param bindingResult
	 * 		the binding result
	 * @param model
	 * 		the model
	 * @param redirectAttributes
	 * 		the redirect attributes
	 * @return the string
	 * @throws CMSItemNotFoundException
	 * 		the cms item not found exception
	 */
	@RequestMapping(value = "/update-profile", method = RequestMethod.POST)
	@RequireHardLogIn
	public String updateProfile(final WileyUpdateProfileForm wileyUpdateProfileForm, final BindingResult bindingResult,
			final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		wileyUpdateProfileFormValidator.validate(wileyUpdateProfileForm, bindingResult);
		String returnAction = REDIRECT_TO_UPDATE_PROFILE;

		final CustomerData customerData = new CustomerData();
		customerData.setFirstName(wileyUpdateProfileForm.getFirstName());
		customerData.setLastName(wileyUpdateProfileForm.getLastName());
		customerData.setUid(wileyUpdateProfileForm.getEmailUid());
		customerData.setDisplayUid(wileyUpdateProfileForm.getEmailUid());

		setPageInfo(model, UPDATE_PROFILE_CMS_PAGE);

		model.addAttribute(EMAIL_PREFERENCE_LINK_ATTR, getEmailPreferenceLink());

		if (bindingResult.hasErrors())
		{
			returnAction = setErrorMessagesAndCMSPage(model, UPDATE_PROFILE_CMS_PAGE);
		}
		else
		{
			try
			{
				customerFacade.updateCustomerProfile(customerData);
				GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER,
						"text.account.profile.confirmationUpdated", null);

				authenticationStrategy.refreshSpringSecurityToken();
			}
			catch (final DuplicateUidException e)
			{
				bindingResult.rejectValue("emailUid", "registration.error.account.exists.title");
				returnAction = setErrorMessagesAndCMSPage(model, UPDATE_PROFILE_CMS_PAGE);
			}
		}

		// todo: do we need breadcrumbs?
		model.addAttribute("breadcrumbs", accountBreadcrumbBuilder.getBreadcrumbs("text.account.profile"));
		return returnAction;
	}

	/**
	 * Update password string.
	 *
	 * @param model
	 * 		the model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 * 		the cms item not found exception
	 */
	@RequestMapping(value = "/update-password", method = RequestMethod.GET)
	@RequireHardLogIn
	public String updatePassword(final Model model) throws CMSItemNotFoundException
	{
		final UpdatePasswordForm updatePasswordForm = new UpdatePasswordForm();

		model.addAttribute("updatePasswordForm", updatePasswordForm);

		storeCmsPageInModel(model, getContentPageForLabelOrId(UPDATE_PASSWORD_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(UPDATE_PASSWORD_CMS_PAGE));

		model.addAttribute("breadcrumbs", accountBreadcrumbBuilder.getBreadcrumbs("text.account.profile.updatePasswordForm"));
		model.addAttribute("metaRobots", "noindex,nofollow");
		return getViewForPage(model);
	}

	/**
	 * Update password string.
	 *
	 * @param updatePasswordForm
	 * 		the update password form
	 * @param bindingResult
	 * 		the binding result
	 * @param model
	 * 		the model
	 * @param redirectAttributes
	 * 		the redirect attributes
	 * @return the string
	 * @throws CMSItemNotFoundException
	 * 		the cms item not found exception
	 */
	@RequestMapping(value = "/update-password", method = RequestMethod.POST)
	@RequireHardLogIn
	public String updatePassword(final UpdatePasswordForm updatePasswordForm, final BindingResult bindingResult,
			final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		wileyPasswordValidator.validate(updatePasswordForm, bindingResult);
		if (!bindingResult.hasErrors())
		{
			if (updatePasswordForm.getNewPassword().equals(updatePasswordForm.getCheckNewPassword()))
			{
				try
				{
					customerFacade.changePassword(updatePasswordForm.getCurrentPassword(), updatePasswordForm.getNewPassword());
				}
				catch (final PasswordMismatchException localException)
				{
					bindingResult.rejectValue("currentPassword", "profile.currentPassword.invalid", new Object[] {},
							"profile.currentPassword.invalid");
				}
			}
			else
			{
				bindingResult.rejectValue("checkNewPassword", "validation.checkPwd.equals", new Object[] {},
						"validation.checkPwd.equals");
			}
		}

		if (bindingResult.hasErrors())
		{
			GlobalMessages.addErrorMessage(model, "form.global.error");
			setPageInfo(model, UPDATE_PASSWORD_CMS_PAGE);

			model.addAttribute("breadcrumbs", accountBreadcrumbBuilder.getBreadcrumbs("text.account.profile.updatePasswordForm"));
			return getViewForPage(model);
		}
		else
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER,
					"text.account.confirmation.password.updated", null);
			return REDIRECT_TO_PASSWORD_UPDATE_PAGE;
		}
	}

	/**
	 * Gets address book.
	 *
	 * @param model
	 * 		the model
	 * @return the address book
	 * @throws CMSItemNotFoundException
	 * 		the cms item not found exception
	 */
	//@RequestMapping(value = "/address-book", method = RequestMethod.GET)
	@RequireHardLogIn
	@Deprecated
	public String getAddressBook(final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute("addressData", userFacade.getAddressBook());

		storeCmsPageInModel(model, getContentPageForLabelOrId(ADDRESS_BOOK_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ADDRESS_BOOK_CMS_PAGE));
		model.addAttribute("breadcrumbs", accountBreadcrumbBuilder.getBreadcrumbs("text.account.addressBook"));
		model.addAttribute("metaRobots", "noindex,nofollow");
		return getViewForPage(model);
	}

	/**
	 * Add address string.
	 *
	 * @param model
	 * 		the model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 * 		the cms item not found exception
	 */
	//@RequestMapping(value = "/add-address", method = RequestMethod.GET)
	@RequireHardLogIn
	@Deprecated
	public String addAddress(final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute("countryData", acceleratorCheckoutFacade.getDeliveryCountries());
		model.addAttribute("titleData", userFacade.getTitles());
		final AddressForm wileyAddressForm = getPreparedAddressForm();
		model.addAttribute("wileyAddressForm", wileyAddressForm);
		model.addAttribute("addressBookEmpty", Boolean.valueOf(userFacade.isAddressBookEmpty()));
		model.addAttribute("isDefaultAddress", Boolean.FALSE);
		storeCmsPageInModel(model, getContentPageForLabelOrId(ADD_EDIT_ADDRESS_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ADD_EDIT_ADDRESS_CMS_PAGE));

		final List<Breadcrumb> breadcrumbs = accountBreadcrumbBuilder.getBreadcrumbs(null);
		breadcrumbs.add(new Breadcrumb("/my-account/address-book", getMessageSource().getMessage("text.account.addressBook", null,
				getI18nService().getCurrentLocale()), null));
		breadcrumbs.add(new Breadcrumb("#", getMessageSource().getMessage("text.account.addressBook.addEditAddress", null,
				getI18nService().getCurrentLocale()), null));
		model.addAttribute("breadcrumbs", breadcrumbs);
		model.addAttribute("metaRobots", "noindex,nofollow");
		return getViewForPage(model);
	}

	/**
	 * Gets prepared address form.
	 *
	 * @return the prepared address form
	 */
	protected WelAgsAddressForm getPreparedAddressForm()
	{
		final CustomerData currentCustomerData = customerFacade.getCurrentCustomer();
		final WelAgsAddressForm wileyAddressForm = new WelAgsAddressForm();
		wileyAddressForm.setFirstName(currentCustomerData.getFirstName());
		wileyAddressForm.setLastName(currentCustomerData.getLastName());
		wileyAddressForm.setTitleCode(currentCustomerData.getTitleCode());
		return wileyAddressForm;
	}

	/**
	 * Add address string.
	 *
	 * @param wileyAddressForm
	 * 		the address form
	 * @param bindingResult
	 * 		the binding result
	 * @param model
	 * 		the model
	 * @param redirectModel
	 * 		the redirect model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 * 		the cms item not found exception
	 */
	//@RequestMapping(value = "/add-address", method = RequestMethod.POST)
	@RequireHardLogIn
	@Deprecated
	public String addAddress(final WelAgsAddressForm wileyAddressForm, final BindingResult bindingResult, final Model model,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		addressValidator.validate(wileyAddressForm, bindingResult);
		if (bindingResult.hasErrors())
		{
			GlobalMessages.addErrorMessage(model, "form.global.error");
			storeCmsPageInModel(model, getContentPageForLabelOrId(ADD_EDIT_ADDRESS_CMS_PAGE));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ADD_EDIT_ADDRESS_CMS_PAGE));
			setUpAddressFormAfterError(wileyAddressForm, model);
			return getViewForPage(model);
		}

		final AddressData newAddress = new AddressData();
		newAddress.setTitleCode(wileyAddressForm.getTitleCode());
		newAddress.setFirstName(wileyAddressForm.getFirstName());
		newAddress.setLastName(wileyAddressForm.getLastName());
		newAddress.setLine1(wileyAddressForm.getLine1());
		newAddress.setLine2(wileyAddressForm.getLine2());
		newAddress.setTown(wileyAddressForm.getTownCity());
		newAddress.setPostalCode(wileyAddressForm.getPostcode());
		newAddress.setBillingAddress(false);
		newAddress.setShippingAddress(true);
		newAddress.setVisibleInAddressBook(true);
		newAddress.setCountry(i18NFacade.getCountryForIsocode(wileyAddressForm.getCountryIso()));

		if (wileyAddressForm.getRegionIso() != null && !StringUtils.isEmpty(wileyAddressForm.getRegionIso()))
		{
			newAddress.setRegion(i18NFacade.getRegion(wileyAddressForm.getCountryIso(), wileyAddressForm.getRegionIso()));
		}

		if (userFacade.isAddressBookEmpty())
		{
			newAddress.setDefaultAddress(true);
			newAddress.setVisibleInAddressBook(true);
		}
		else
		{
			newAddress.setDefaultAddress(
					wileyAddressForm.getDefaultAddress() != null && wileyAddressForm.getDefaultAddress().booleanValue());
		}

		final AddressVerificationResult<AddressVerificationDecision> verificationResult =
				addressVerificationFacade.verifyAddressData(newAddress);
		final boolean addressRequiresReview = addressVerificationResultHandler.handleResult(verificationResult, newAddress,
				model, redirectModel, bindingResult, addressVerificationFacade.isCustomerAllowedToIgnoreAddressSuggestions(),
				"checkout.multi.address.added");

		model.addAttribute("regions", i18NFacade.getRegionsForCountryIso(wileyAddressForm.getCountryIso()));
		model.addAttribute("country", wileyAddressForm.getCountryIso());
		model.addAttribute("edit", Boolean.TRUE);
		model.addAttribute("isDefaultAddress", Boolean.valueOf(isDefaultAddress(wileyAddressForm.getAddressId())));

		if (addressRequiresReview)
		{
			storeCmsPageInModel(model, getContentPageForLabelOrId(ADD_EDIT_ADDRESS_CMS_PAGE));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ADD_EDIT_ADDRESS_CMS_PAGE));
			return getViewForPage(model);
		}

		userFacade.addAddress(newAddress);


		GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "account.confirmation.address.added",
				null);

		return REDIRECT_TO_EDIT_ADDRESS_PAGE + newAddress.getId();
	}

	/**
	 * Sets up address form after error.
	 *
	 * @param wileyAddressForm
	 * 		the address form
	 * @param model
	 * 		the model
	 */
	protected void setUpAddressFormAfterError(final WelAgsAddressForm wileyAddressForm, final Model model)
	{
		model.addAttribute("countryData", acceleratorCheckoutFacade.getDeliveryCountries());
		model.addAttribute("titleData", userFacade.getTitles());
		model.addAttribute("addressBookEmpty", Boolean.valueOf(userFacade.isAddressBookEmpty()));
		model.addAttribute("isDefaultAddress", Boolean.valueOf(isDefaultAddress(wileyAddressForm.getAddressId())));
		if (wileyAddressForm.getCountryIso() != null)
		{
			model.addAttribute("regions", i18NFacade.getRegionsForCountryIso(wileyAddressForm.getCountryIso()));
			model.addAttribute("country", wileyAddressForm.getCountryIso());
		}
	}

	/**
	 * Edit address string.
	 *
	 * @param addressCode
	 * 		the address code
	 * @param model
	 * 		the model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 * 		the cms item not found exception
	 */
	//@RequestMapping(value = "/edit-address/" + ADDRESS_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	@RequireHardLogIn
	@Deprecated
	public String editAddress(@PathVariable("addressCode") final String addressCode, final Model model)
			throws CMSItemNotFoundException
	{
		final WelAgsAddressForm wileyAddressForm = new WelAgsAddressForm();
		model.addAttribute("countryData", acceleratorCheckoutFacade.getDeliveryCountries());
		model.addAttribute("titleData", userFacade.getTitles());
		model.addAttribute("wileyAddressForm", wileyAddressForm);
		model.addAttribute("addressBookEmpty", Boolean.valueOf(userFacade.isAddressBookEmpty()));

		for (final AddressData addressData : userFacade.getAddressBook())
		{
			if (addressData.getId() != null && addressData.getId().equals(addressCode))
			{
				model.addAttribute("regions", i18NFacade.getRegionsForCountryIso(addressData.getCountry().getIsocode()));
				model.addAttribute("country", addressData.getCountry().getIsocode());
				model.addAttribute("addressData", addressData);
				wileyAddressForm.setAddressId(addressData.getId());
				wileyAddressForm.setTitleCode(addressData.getTitleCode());
				wileyAddressForm.setFirstName(addressData.getFirstName());
				wileyAddressForm.setLastName(addressData.getLastName());
				wileyAddressForm.setLine1(addressData.getLine1());
				wileyAddressForm.setLine2(addressData.getLine2());
				wileyAddressForm.setTownCity(addressData.getTown());
				wileyAddressForm.setPostcode(addressData.getPostalCode());
				wileyAddressForm.setCountryIso(addressData.getCountry().getIsocode());
				if (addressData.getRegion() != null && !StringUtils.isEmpty(addressData.getRegion().getIsocode()))
				{
					wileyAddressForm.setRegionIso(addressData.getRegion().getIsocode());
				}

				if (isDefaultAddress(addressData.getId()))
				{
					wileyAddressForm.setDefaultAddress(Boolean.TRUE);
					model.addAttribute("isDefaultAddress", Boolean.TRUE);
				}
				else
				{
					wileyAddressForm.setDefaultAddress(Boolean.FALSE);
					model.addAttribute("isDefaultAddress", Boolean.FALSE);
				}
				break;
			}
		}

		storeCmsPageInModel(model, getContentPageForLabelOrId(ADD_EDIT_ADDRESS_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ADD_EDIT_ADDRESS_CMS_PAGE));

		final List<Breadcrumb> breadcrumbs = accountBreadcrumbBuilder.getBreadcrumbs(null);
		breadcrumbs.add(new Breadcrumb("/my-account/address-book", getMessageSource().getMessage("text.account.addressBook", null,
				getI18nService().getCurrentLocale()), null));
		breadcrumbs.add(new Breadcrumb("#", getMessageSource().getMessage("text.account.addressBook.addEditAddress", null,
				getI18nService().getCurrentLocale()), null));
		model.addAttribute("breadcrumbs", breadcrumbs);
		model.addAttribute("metaRobots", "noindex,nofollow");
		model.addAttribute("edit", Boolean.TRUE);
		return getViewForPage(model);
	}

	/**
	 * Method checks if address is set as default
	 *
	 * @param addressId
	 * 		- identifier for address to check
	 * @return true if address is default, false if address is not default
	 */
	protected boolean isDefaultAddress(final String addressId)
	{
		final AddressData defaultAddress = userFacade.getDefaultAddress();
		return (defaultAddress != null && defaultAddress.getId() != null && defaultAddress.getId().equals(addressId));
	}

	/**
	 * Edit address string.
	 *
	 * @param wileyAddressForm
	 * 		the address form
	 * @param bindingResult
	 * 		the binding result
	 * @param model
	 * 		the model
	 * @param redirectModel
	 * 		the redirect model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 * 		the cms item not found exception
	 */
	//@RequestMapping(value = "/edit-address/" + ADDRESS_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.POST)
	@RequireHardLogIn
	@Deprecated
	public String editAddress(final WelAgsAddressForm wileyAddressForm, final BindingResult bindingResult, final Model model,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		model.addAttribute("supportedCountries", getBillingCountries());
		addressValidator.validate(wileyAddressForm, bindingResult);

		if (bindingResult.hasErrors())
		{
			GlobalMessages.addErrorMessage(model, "form.global.error");
			storeCmsPageInModel(model, getContentPageForLabelOrId(ADD_EDIT_ADDRESS_CMS_PAGE));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ADD_EDIT_ADDRESS_CMS_PAGE));
			setUpAddressFormAfterError(wileyAddressForm, model);
			return getViewForPage(model);
		}

		model.addAttribute("metaRobots", "noindex,nofollow");

		final AddressData newAddress = new AddressData();
		newAddress.setId(wileyAddressForm.getAddressId());
		newAddress.setFirstName(wileyAddressForm.getFirstName());
		newAddress.setLastName(wileyAddressForm.getLastName());
		newAddress.setLine1(wileyAddressForm.getLine1());
		newAddress.setLine2(wileyAddressForm.getLine2());
		newAddress.setTown(wileyAddressForm.getTownCity());
		newAddress.setPostalCode(wileyAddressForm.getPostcode());
		newAddress.setPhone(wileyAddressForm.getPhone());
		newAddress.setBillingAddress(true);
		newAddress.setShippingAddress(true);
		newAddress.setDefaultAddress(true);
		newAddress.setVisibleInAddressBook(true);
		newAddress.setCountry(i18NFacade.getCountryForIsocode(wileyAddressForm.getCountryIso()));

		if (wileyAddressForm.getRegionIso() != null && !StringUtils.isEmpty(wileyAddressForm.getRegionIso()))
		{
			newAddress.setRegion(i18NFacade.getRegion(wileyAddressForm.getCountryIso(), wileyAddressForm.getRegionIso()));
		}

		if (Boolean.TRUE.equals(wileyAddressForm.getDefaultAddress()) || userFacade.getAddressBook().size() <= 1)
		{
			newAddress.setDefaultAddress(true);
			newAddress.setVisibleInAddressBook(true);
		}

		final AddressVerificationResult<AddressVerificationDecision> verificationResult =
				addressVerificationFacade.verifyAddressData(newAddress);
		final boolean addressRequiresReview = addressVerificationResultHandler.handleResult(verificationResult, newAddress,
				model, redirectModel, bindingResult, addressVerificationFacade.isCustomerAllowedToIgnoreAddressSuggestions(),
				"checkout.multi.address.updated");

		model.addAttribute("regions", i18NFacade.getRegionsForCountryIso(wileyAddressForm.getCountryIso()));
		model.addAttribute("edit", Boolean.TRUE);
		model.addAttribute("isDefaultAddress", Boolean.valueOf(isDefaultAddress(wileyAddressForm.getAddressId())));

		if (addressRequiresReview)
		{
			storeCmsPageInModel(model, getContentPageForLabelOrId(ADD_EDIT_ADDRESS_CMS_PAGE));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ADD_EDIT_ADDRESS_CMS_PAGE));
			return getViewForPage(model);
		}

		userFacade.editAddress(newAddress);

		GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "account.confirmation.address.updated",
				null);
		return REDIRECT_TO_EDIT_ADDRESS_PAGE + newAddress.getId();
	}

	/**
	 * Remove address string.
	 *
	 * @param addressCode
	 * 		the address code
	 * @param redirectModel
	 * 		the redirect model
	 * @return the string
	 */
	//@RequestMapping(value = "/remove-address/" + ADDRESS_CODE_PATH_VARIABLE_PATTERN, method =
	//		{ RequestMethod.GET, RequestMethod.POST })
	@RequireHardLogIn
	@Deprecated
	public String removeAddress(@PathVariable("addressCode") final String addressCode, final RedirectAttributes redirectModel)
	{
		final AddressData addressData = new AddressData();
		addressData.setId(addressCode);
		userFacade.removeAddress(addressData);

		GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
				"account.confirmation.address.removed");
		return REDIRECT_TO_ADDRESS_BOOK_PAGE;
	}

	/**
	 * Sets default address.
	 *
	 * @param addressCode
	 * 		the address code
	 * @param redirectModel
	 * 		the redirect model
	 * @return the default address
	 */
	//@RequestMapping(value = "/set-default-address/" + ADDRESS_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	@RequireHardLogIn
	@Deprecated
	public String setDefaultAddress(@PathVariable("addressCode") final String addressCode, final RedirectAttributes redirectModel)
	{
		final AddressData addressData = new AddressData();
		addressData.setDefaultAddress(true);
		addressData.setVisibleInAddressBook(true);
		addressData.setId(addressCode);
		userFacade.setDefaultAddress(addressData);
		GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
				"account.confirmation.default.address.changed");
		return REDIRECT_TO_ADDRESS_BOOK_PAGE;
	}

	/**
	 * Payment details string.
	 *
	 * @param model
	 * 		the model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 * 		the cms item not found exception
	 */
	@RequestMapping(value = "/payment-details", method = RequestMethod.GET)
	@RequireHardLogIn
	public String paymentDetails(final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute("billingCountries", getBillingCountries());
		model.addAttribute("titleData", userFacade.getTitles());
		final WelAgsAddressForm wileyAddressForm = new WelAgsAddressForm();
		model.addAttribute("wileyAddressForm", wileyAddressForm);
		model.addAttribute("addressBookEmpty", Boolean.valueOf(userFacade.isAddressBookEmpty()));
		model.addAttribute("isDefaultAddress", Boolean.TRUE);

		AddressData addressData = customerFacade.getCurrentCustomer().getDefaultBillingAddress();
		if (addressData != null)
		{
			model.addAttribute("regions", i18NFacade.getRegionsForCountryIso(addressData.getCountry().getIsocode()));
			model.addAttribute("country", addressData.getCountry().getIsocode());
			model.addAttribute("addressData", addressData);
			wileyAddressForm.setAddressId(addressData.getId());
			wileyAddressForm.setFirstName(addressData.getFirstName());
			wileyAddressForm.setLastName(addressData.getLastName());
			wileyAddressForm.setLine1(addressData.getLine1());
			wileyAddressForm.setLine2(addressData.getLine2());
			wileyAddressForm.setTownCity(addressData.getTown());
			wileyAddressForm.setPostcode(addressData.getPostalCode());
			wileyAddressForm.setPhone(addressData.getPhone());
			wileyAddressForm.setCountryIso(addressData.getCountry().getIsocode());
			if (addressData.getRegion() != null && !StringUtils.isEmpty(addressData.getRegion().getIsocode()))
			{
				wileyAddressForm.setRegionIso(addressData.getRegion().getIsocode());
			}

		}
		else
		{
			String defaultCauntryIsocode = i18NFacade.getCountryForIsocode(WileyCoreConstants.DEFAULT_COUNTRY_ISO_CODE)
					.getIsocode();
			wileyAddressForm.setCountryIso(defaultCauntryIsocode);
			model.addAttribute("country", defaultCauntryIsocode);
			model.addAttribute("regions", i18NFacade.getRegionsForCountryIso(defaultCauntryIsocode));
		}
		model.addAttribute("edit", Boolean.TRUE);

		model.addAttribute("customerData", customerFacade.getCurrentCustomer());
		model.addAttribute("paymentInfoData", userFacade.getCCPaymentInfos(true));
		storeCmsPageInModel(model, getContentPageForLabelOrId(PAYMENT_DETAILS_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ADD_EDIT_ADDRESS_CMS_PAGE));
		model.addAttribute("breadcrumbs", accountBreadcrumbBuilder.getBreadcrumbs("text.account.paymentDetails"));
		model.addAttribute("metaRobots", "noindex,nofollow");
		return getViewForPage(model);

	}

	/**
	 * Edit address on payment details.
	 *
	 * @param wileyAddressForm
	 * 		the address form
	 * @param bindingResult
	 * 		the binding result
	 * @param model
	 * 		the model
	 * @param redirectModel
	 * 		the redirect model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 * 		the cms item not found exception
	 */
	@RequestMapping(value = "/payment-details", method = RequestMethod.POST)
	@RequireHardLogIn
	public String editPaymentDetails(final WelAgsAddressForm wileyAddressForm, final BindingResult bindingResult,
			final Model model, final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		model.addAttribute("billingCountries", getBillingCountries());
		addressValidator.validate(wileyAddressForm, bindingResult);

		if (bindingResult.hasErrors())
		{
			GlobalMessages.addErrorMessage(model, "form.global.error");
			storeCmsPageInModel(model, getContentPageForLabelOrId(PAYMENT_DETAILS_CMS_PAGE));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(PAYMENT_DETAILS_CMS_PAGE));
			setUpAddressFormAfterError(wileyAddressForm, model);
			return getViewForPage(model);
		}

		model.addAttribute("metaRobots", "noindex,nofollow");

		final AddressData newAddress = new AddressData();
		newAddress.setId(wileyAddressForm.getAddressId());
		newAddress.setFirstName(wileyAddressForm.getFirstName());
		newAddress.setLastName(wileyAddressForm.getLastName());
		newAddress.setLine1(wileyAddressForm.getLine1());
		newAddress.setLine2(wileyAddressForm.getLine2());
		newAddress.setTown(wileyAddressForm.getTownCity());
		newAddress.setPostalCode(wileyAddressForm.getPostcode());
		newAddress.setPhone(wileyAddressForm.getPhone());
		newAddress.setBillingAddress(true);
		newAddress.setShippingAddress(false);
		newAddress.setDefaultAddress(true);
		newAddress.setVisibleInAddressBook(true);
		newAddress.setCountry(i18NFacade.getCountryForIsocode(wileyAddressForm.getCountryIso()));

		if (wileyAddressForm.getRegionIso() != null && !StringUtils.isEmpty(wileyAddressForm.getRegionIso()))
		{
			newAddress.setRegion(i18NFacade.getRegion(wileyAddressForm.getCountryIso(), wileyAddressForm.getRegionIso()));
		}

		model.addAttribute("regions", i18NFacade.getRegionsForCountryIso(wileyAddressForm.getCountryIso()));
		model.addAttribute("edit", Boolean.TRUE);
		model.addAttribute("isDefaultAddress", Boolean.valueOf(isDefaultAddress(wileyAddressForm.getAddressId())));

		if (StringUtils.isNotBlank(newAddress.getId()))
		{
			userFacade.editAddress(newAddress);
		}
		else
		{
			userFacade.addAddress(newAddress);
		}

		GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
				"account.confirmation.address.updated",
				null);
		return REDIRECT_TO_PAYMENT_INFO_PAGE;

	}

	/**
	 * Sets default payment details.
	 *
	 * @param paymentInfoId
	 * 		the payment info id
	 * @return the default payment details
	 */
	//@RequestMapping(value = "/set-default-payment-details", method = RequestMethod.POST)
	@RequireHardLogIn
	@Deprecated
	public String setDefaultPaymentDetails(@RequestParam final String paymentInfoId)
	{
		CCPaymentInfoData paymentInfoData = null;
		if (StringUtils.isNotBlank(paymentInfoId))
		{
			paymentInfoData = userFacade.getCCPaymentInfoForCode(paymentInfoId);
		}
		userFacade.setDefaultPaymentInfo(paymentInfoData);
		return REDIRECT_TO_PAYMENT_INFO_PAGE;
	}

	/**
	 * Remove payment method string.
	 *
	 * @param paymentMethodId
	 * 		the payment method id
	 * @param redirectAttributes
	 * 		the redirect attributes
	 * @return the string
	 * @throws CMSItemNotFoundException
	 * 		the cms item not found exception
	 */
	//@RequestMapping(value = "/remove-payment-method", method = RequestMethod.POST)
	@RequireHardLogIn
	@Deprecated
	public String removePaymentMethod(@RequestParam(value = "paymentInfoId") final String paymentMethodId,
			final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		userFacade.unlinkCCPaymentInfo(paymentMethodId);
		GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER,
				"text.account.profile.paymentCart.removed");
		return REDIRECT_TO_PAYMENT_INFO_PAGE;
	}

	private String getEmailPreferenceLink()
	{
		return configurationService.getConfiguration().getString("agsstorefront.emailpreference.link");
	}
}
