package com.wiley.ags.storefront.controllers.pages;

import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;

import static com.wiley.storefrontcommons.constants.WileystorefrontcommonsConstants.FORWARD_TO_404;


@ControllerAdvice
public class AgsControllerAdvice
{
	private static final Logger LOG = LoggerFactory.getLogger(AgsControllerAdvice.class);

	@Autowired
	private StringTrimmerEditor stringTrimmer;
	
	@ExceptionHandler(UnknownIdentifierException.class)
	public String itemNotFoundHandler(final UnknownIdentifierException exception)
	{
		LOG.warn("Access to nonexistent item: {}", exception.getMessage());
		return FORWARD_TO_404;
	}

	@InitBinder
	public void initBinder(final WebDataBinder binder) 
	{
	  binder.registerCustomEditor(String.class, stringTrimmer);
	}
}
 