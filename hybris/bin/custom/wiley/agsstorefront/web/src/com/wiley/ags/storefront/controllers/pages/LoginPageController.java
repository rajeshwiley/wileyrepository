/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.ags.storefront.controllers.pages;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.welags.storefrontcommons.constants.WelagsstorefrontcommonsConstants;
import com.wiley.welags.storefrontcommons.controllers.pages.AbstractWileyLoginPageController;
import com.wiley.welags.storefrontcommons.forms.WileyRegisterForm;


/**
 * Login Controller. Handles login and register for the account flow.
 */
@Controller
@Scope("tenant")
@RequestMapping(value = "/login")
public class LoginPageController extends AbstractWileyLoginPageController
{

	private static final String MY_ACCOUNT_UPDATE_PROFILE = "/my-account/update-profile";

	private HttpSessionRequestCache httpSessionRequestCache;


	@Override
	protected String getView()
	{
		return WelagsstorefrontcommonsConstants.Views.Pages.Account.ACCOUNT_LOGIN_PAGE;
	}

	@Override
	protected String getSuccessRedirect(final HttpServletRequest request, final HttpServletResponse response)
	{
		if (httpSessionRequestCache.getRequest(request, response) != null)
		{
			return httpSessionRequestCache.getRequest(request, response).getRedirectUrl();
		}
		return MY_ACCOUNT_UPDATE_PROFILE;
	}

	@Override
	protected AbstractPageModel getCmsPage() throws CMSItemNotFoundException
	{
		return getContentPageForLabelOrId("login");
	}


	/**
	 * Sets http session request cache.
	 *
	 * @param accHttpSessionRequestCache
	 * 		the acc http session request cache
	 */
	@Resource(name = "httpSessionRequestCache")
	public void setHttpSessionRequestCache(final HttpSessionRequestCache accHttpSessionRequestCache)
	{
		this.httpSessionRequestCache = accHttpSessionRequestCache;
	}

	/**
	 * Do login string.
	 *
	 * @param referer
	 * 		the referer
	 * @param loginError
	 * 		the login error
	 * @param model
	 * 		the model
	 * @param request
	 * 		the request
	 * @param response
	 * 		the response
	 * @param session
	 * 		the session
	 * @return the string
	 * @throws CMSItemNotFoundException
	 * 		the cms item not found exception
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String doLogin(@RequestHeader(value = "referer", required = false) final String referer,
			@RequestParam(value = "error", defaultValue = "false") final boolean loginError,
			@RequestParam(value = "disabled", defaultValue = "false") final boolean isDisabledUser, final Model model,
			final HttpServletRequest request, final HttpServletResponse response, final HttpSession session)
			throws CMSItemNotFoundException
	{
		if (!loginError)
		{

			storeReferer(referer, request, response);

		}
		return getDefaultLoginPage(loginError, isDisabledUser, session, model);
	}

	/**
	 * Store referer.
	 *
	 * @param referer
	 * 		the referer
	 * @param request
	 * 		the request
	 * @param response
	 * 		the response
	 */
	protected void storeReferer(final String referer, final HttpServletRequest request, final HttpServletResponse response)
	{
		if (StringUtils.isNotBlank(referer) && !StringUtils.endsWith(referer, "/login")
				&& StringUtils.contains(referer, request.getServerName()))
		{
			httpSessionRequestCache.saveRequest(request, response);
		}
	}

	/**
	 * Do register string.
	 *
	 * @param referer
	 * 		the referer
	 * @param form
	 * 		the form
	 * @param bindingResult
	 * 		the binding result
	 * @param model
	 * 		the model
	 * @param request
	 * 		the request
	 * @param response
	 * 		the response
	 * @param redirectModel
	 * 		the redirect model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 * 		the cms item not found exception
	 */
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String doRegister(@RequestHeader(value = "referer", required = false) final String referer,
			final WileyRegisterForm form,
			final BindingResult bindingResult, final Model model, final HttpServletRequest request,
			final HttpServletResponse response, final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		getRegistrationValidator().validate(form, bindingResult);
		return processRegisterUserRequest(referer, form, bindingResult, model, request, response, redirectModel);
	}

}
