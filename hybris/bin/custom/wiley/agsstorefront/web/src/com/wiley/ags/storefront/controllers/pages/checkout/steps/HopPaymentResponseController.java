/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.ags.storefront.controllers.pages.checkout.steps;

import com.wiley.core.enums.PaymentModeEnum;
import com.wiley.facades.welags.order.WelAgsCheckoutFacade;
import com.wiley.welags.storefrontcommons.controllers.ControllerConstants;
import com.wiley.welags.storefrontcommons.controllers.exceptions.OrderAlreadyPlacedException;
import com.wiley.welags.storefrontcommons.controllers.pages.checkout.steps.PaymentMethodCheckoutStepController;
import de.hybris.platform.acceleratorfacades.payment.data.PaymentSubscriptionResultData;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;


@RequestMapping(value = "/checkout/multi/hop")
public class HopPaymentResponseController extends PaymentMethodCheckoutStepController
{
	private static final Logger LOG = LoggerFactory.getLogger(HopPaymentResponseController.class);

	@RequestMapping(value = "/response", method = RequestMethod.POST)
	@RequireHardLogIn
	public String doHandleHopResponse(final Model model, final RedirectAttributes redirectAttributes,
			final HttpServletRequest request)
	{
		if (!getCheckoutFacade().hasCheckoutCart())
		{
			LOG.error("Cart is empty. Order has already been placed");
			throw new OrderAlreadyPlacedException();
		}
		final Map<String, String> resultMap = getRequestParameterMap(request);


		try
		{
			final PaymentSubscriptionResultData paymentSubscriptionResultData = getPaymentFacade().completeHopCreateSubscription(
					resultMap, true);


			if (paymentSubscriptionResultData.isSuccess() && paymentSubscriptionResultData.getStoredCard() != null
					&& StringUtils.isNotBlank(paymentSubscriptionResultData.getStoredCard().getSubscriptionId()))
			{
				final CCPaymentInfoData newPaymentSubscription = paymentSubscriptionResultData.getStoredCard();

				// The OOB implementation is to set the first payment info as default.
				// The change is made to set the latest payment info as default.
				getUserFacade().setDefaultPaymentInfo(newPaymentSubscription);

				getCheckoutFacade().setPaymentDetails(newPaymentSubscription.getId());
				checkoutFacade.setPaymentMode(PaymentModeEnum.CARD);

				return getCheckoutStep().nextStep();
			}
			else
			{
				LOG.error("Failed to create authorization.  Please check the log files for more information");
				String errorDescription = getMessageSource().getMessage("checkout.placing.order.paymentFailed.error.description."
								+ paymentSubscriptionResultData.getResultCode(), null,
						getI18nService().getCurrentLocale());
				GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
						"checkout.placing.order.paymentFailed",
						new Object[]
								{ getContactMail(), getContactPhone(),
										errorDescription,
										paymentSubscriptionResultData.getResultCode() });

				final WelAgsCheckoutFacade checkoutFacade = getWelAgsCheckoutFacade();
				if (checkoutFacade.isDigitalSessionCart() && checkoutFacade.isNonZeroPriceCart())
				{
					return getCheckoutStep().getCheckoutGroup().getValidationResultsMap().get(
							ValidationResults.REDIRECT_TO_DELIVERY_ADDRESS.name());
				}

				return back(redirectAttributes);
			}
		}
		catch (final Exception ex)
		{

			LOG.error(String.format("Failed to create payment subscription reason [%1$s]. ",
					ex.getMessage()), ex);
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"checkout.placing.order.exception", new Object[]
							{ getContactMail(), getContactPhone() });
			return REDIRECT_URL_CART;
		}
	}

	@RequestMapping(value = "/error", method = RequestMethod.GET)
	public String doHostedOrderPageError(@RequestParam(required = true) final String decision,
			@RequestParam(required = true) final String reasonCode,
			final Model model, final RedirectAttributes redirectAttributes)
			throws CMSItemNotFoundException
	{
		String redirectUrl = REDIRECT_URL_ADD_PAYMENT_METHOD;
		final ValidationResults validationResults = getCheckoutStep().validate(redirectAttributes);
		if (getCheckoutStep().checkIfValidationErrors(validationResults))
		{
			redirectUrl = getCheckoutStep().onValidation(validationResults);
		}
		model.addAttribute("decision", decision);
		model.addAttribute("reasonCode", reasonCode);
		model.addAttribute("redirectUrl", redirectUrl.replace(REDIRECT_PREFIX, ""));
		model.addAttribute(WebConstants.BREADCRUMBS_KEY,
				getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.hostedOrderPageError.breadcrumb"));
		storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		setCheckoutStepLinksForModel(model, getCheckoutStep());

		GlobalMessages.addErrorMessage(model, "checkout.multi.hostedOrderPageError.globalError");

		return ControllerConstants.Views.Pages.MultiStepCheckout.HOSTED_ORDER_PAGE_ERROR_PAGE;
	}

}
