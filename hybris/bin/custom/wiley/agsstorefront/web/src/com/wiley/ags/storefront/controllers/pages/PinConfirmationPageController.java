package com.wiley.ags.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.order.OrderFacade;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.wiley.facades.order.WileyMagicLinkFacade;
import com.wiley.welags.storefrontcommons.constants.WelagsstorefrontcommonsConstants;


/**
 * Controller for the PIN Confirmation Page.
 */
@Controller
@RequestMapping(value = "/pin/confirmation")
public class PinConfirmationPageController extends AbstractPageController
{

	private static final String ERROR_CMS_PAGE = "notFound";

	@Resource
	private OrderFacade orderFacade;

	@Resource
	private UserFacade userFacade;

	@Resource(name = "agsMagicLinkFacade")
	private WileyMagicLinkFacade magicLinkFacade;


	@RequestMapping(method = RequestMethod.GET)
	public String showPinConfirmationPage(@RequestParam(value = "orderCode", required = false) final String orderCode,
			final Model model, final HttpServletRequest request, final HttpServletResponse response)
			throws CMSItemNotFoundException
	{
		if (StringUtils.isNotEmpty(orderCode))
		{
			try
			{
				OrderData orderData = orderFacade.getOrderDetailsForCode(orderCode);
				validateOrderAccessRights(orderData);
				model.addAttribute("magiclink", magicLinkFacade.getMagicLink());
				setUpMetaDataForContentPage(model, (ContentPageModel) getCmsPage());
				storeCmsPageInModel(model, getCmsPage());
				return getViewForPage(model);
			}
			catch (final IllegalAccessException | UnknownIdentifierException e)
			{
				return respondErrorPage(model, response);
			}
		}
		else
		{
			return respondErrorPage(model, response);
		}

	}

	private String respondErrorPage(final Model model, final HttpServletResponse response) throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(ERROR_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ERROR_CMS_PAGE));

		response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);

		return WelagsstorefrontcommonsConstants.Views.Pages.Error.ERROR_NOT_FOUND_PAGE;
	}

	private void validateOrderAccessRights(final OrderData orderData) throws IllegalAccessException
	{
		if (userFacade.isAnonymousUser() || !getUser().getUid().equals(orderData.getUser().getUid()))
		{
			throw new IllegalAccessException("User is not allowed to request this information.");
		}
	}

	protected AbstractPageModel getCmsPage() throws CMSItemNotFoundException
	{
		return this.getContentPageForLabelOrId("pinConfirmation");
	}
}
