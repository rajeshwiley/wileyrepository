package com.wiley.ags.storefront.controllers.cms;

import java.time.LocalDateTime;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.wiley.core.model.components.WileyFooterComponentModel;
import com.wiley.welags.storefrontcommons.constants.WelagsstorefrontcommonsConstants;



/**
 * Controller for CMS WileyFooterComponent
 */
@Controller("WileyFooterComponentController")
@Scope("tenant")
@RequestMapping(value = WelagsstorefrontcommonsConstants.Actions.Cms.WILEY_FOOTER_COMPONENT)
public class WileyFooterComponentController extends AbstractCMSComponentController<WileyFooterComponentModel>
{
	private static final String CURRENT_YEAR = "${CURRENT_YEAR}";

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final WileyFooterComponentModel component)
	{
		LocalDateTime timePoint = LocalDateTime.now();

		String extendedNotice = component.getExtendedNotice();

		String extendedNoticeWithDate = extendedNotice.replace(CURRENT_YEAR, String.valueOf(timePoint.getYear()));

		model.addAttribute("extendedNoticeWithCurrentYear", extendedNoticeWithDate);

	}
}
