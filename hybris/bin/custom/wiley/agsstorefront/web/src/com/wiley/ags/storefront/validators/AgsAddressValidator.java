package com.wiley.ags.storefront.validators;

import java.util.HashMap;
import java.util.Map;

import com.wiley.welags.storefrontcommons.forms.validation.AbstractWelAgsAddressValidator;

import static com.wiley.welags.storefrontcommons.forms.validation.AbstractWelAgsAddressValidator.WileyAddressField.COUNTRY;
import static com.wiley.welags.storefrontcommons.forms.validation.AbstractWelAgsAddressValidator.WileyAddressField.FIRSTNAME;
import static com.wiley.welags.storefrontcommons.forms.validation.AbstractWelAgsAddressValidator.WileyAddressField.LASTNAME;
import static com.wiley.welags.storefrontcommons.forms.validation.AbstractWelAgsAddressValidator.WileyAddressField.LINE1;
import static com.wiley.welags.storefrontcommons.forms.validation.AbstractWelAgsAddressValidator.WileyAddressField.LINE2;
import static com.wiley.welags.storefrontcommons.forms.validation.AbstractWelAgsAddressValidator.WileyAddressField.PHONE;
import static com.wiley.welags.storefrontcommons.forms.validation.AbstractWelAgsAddressValidator.WileyAddressField.POSTCODE;
import static com.wiley.welags.storefrontcommons.forms.validation.AbstractWelAgsAddressValidator.WileyAddressField.PROVINCE;
import static com.wiley.welags.storefrontcommons.forms.validation.AbstractWelAgsAddressValidator.WileyAddressField.TOWN;
import static com.wiley.welags.storefrontcommons.forms.validation.AbstractWelAgsAddressValidator.WileyAddressField.ZIPCODE;


public class AgsAddressValidator extends AbstractWelAgsAddressValidator
{
	private static final int NAME_FIELD_LENGTH = 35;
	private static final int ADDRESS_LINE_MAX_FIELD_LENGTH = 35;
	private static final int ZIPCODE_MAX_FIELD_LENGTH = 9;
	private static final int POSTCODE_MAX_FIELD_LENGTH = 15;
	private static final int TOWN_CITY_FIELD_LENGTH = 35;
	private static final int PHONE_FIELD_LENGTH = 15;

	private static final Map<WileyAddressField, Integer> MAX_LENGTHS = new HashMap<>();

	static
	{
		MAX_LENGTHS.put(FIRSTNAME, Integer.valueOf(NAME_FIELD_LENGTH));
		MAX_LENGTHS.put(LASTNAME, Integer.valueOf(NAME_FIELD_LENGTH));
		MAX_LENGTHS.put(LINE1, Integer.valueOf(ADDRESS_LINE_MAX_FIELD_LENGTH));
		MAX_LENGTHS.put(LINE2, Integer.valueOf(ADDRESS_LINE_MAX_FIELD_LENGTH));
		MAX_LENGTHS.put(PROVINCE, Integer.valueOf(TOWN_CITY_FIELD_LENGTH));
		MAX_LENGTHS.put(TOWN, Integer.valueOf(TOWN_CITY_FIELD_LENGTH));
		MAX_LENGTHS.put(POSTCODE, Integer.valueOf(POSTCODE_MAX_FIELD_LENGTH));
		MAX_LENGTHS.put(ZIPCODE, Integer.valueOf(ZIPCODE_MAX_FIELD_LENGTH));
		MAX_LENGTHS.put(PHONE, Integer.valueOf(PHONE_FIELD_LENGTH));
		MAX_LENGTHS.put(COUNTRY, Integer.valueOf(MAX_FIELD_LENGTH));
	}

	@Override
	protected Map<WileyAddressField, Integer> getMaxLengths()
	{
		return MAX_LENGTHS;
	}
}
