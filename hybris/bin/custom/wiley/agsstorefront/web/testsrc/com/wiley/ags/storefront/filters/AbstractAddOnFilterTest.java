/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.ags.storefront.filters;

import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.io.File;
import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.mockito.Answers;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.wiley.storefrontcommons.filters.AcceleratorAddOnFilter;
import com.wiley.storefrontcommons.filters.AcceleratorAddOnFilter.ExtensionAccessor;


/**
 * The type Abstract add on filter test.
 */
public abstract class AbstractAddOnFilterTest
{
	private static final Logger LOG = Logger.getLogger(AbstractAddOnFilterTest.class);

	/**
	 * The constant STOREFRONT_NAME.
	 */
	protected static final String STOREFRONT_NAME = "agsstorefront";
	/**
	 * The constant ADDONTWO_NAME.
	 */
	protected static final String ADDONTWO_NAME = "addontwo";
	/**
	 * The constant UI_FOLDER.
	 */
	protected static final String UI_FOLDER = "/web/webroot/_ui";
	/**
	 * The constant WEB_INF_FOLDER.
	 */
	protected static final String WEB_INF_FOLDER = "/webroot/WEB-INF";
	/**
	 * The Root sandbox dir.
	 */
	protected final File rootSandboxDir = new File(System.getProperty("java.io.tmpdir"), "sandbox");
	/**
	 * The Web target resource.
	 */
	protected File webTargetResource;
	/**
	 * The Add on source resource.
	 */
	protected File addOnSourceResource;
	/**
	 * The Web extension physical path.
	 */
	protected File webExtensionPhysicalPath;

	/**
	 * The Request.
	 */
	@Mock(answer = Answers.RETURNS_DEEP_STUBS)
	protected HttpServletRequest request;
	/**
	 * The Response.
	 */
	@Mock
	protected HttpServletResponse response;
	/**
	 * The Filter chain.
	 */
	@Mock
	protected FilterChain filterChain;
	/**
	 * The Extension accessor.
	 */
	@Mock(answer = Answers.RETURNS_DEEP_STUBS)
	protected ExtensionAccessor extensionAccessor;
	/**
	 * The Configuration service.
	 */
	@Mock(answer = Answers.RETURNS_DEEP_STUBS)
	protected ConfigurationService configurationService;
	/**
	 * The Filter.
	 */
	@Spy
	@InjectMocks
	protected final AcceleratorAddOnFilter filter = new AcceleratorAddOnFilter();

	/**
	 * Prepare.
	 *
	 * @throws IOException
	 * 		the io exception
	 */
	@Before
	public void prepare() throws IOException
	{
		//log("Starting ...");
		MockitoAnnotations.initMocks(this);

		webTargetResource = createWebTargetDir();
		addOnSourceResource = new File(rootSandboxDir, ADDONTWO_NAME + "/acceleratoraddon" + getFolder());

		FileUtils.forceMkdir(webTargetResource);
		FileUtils.forceMkdir(addOnSourceResource);

		LOG.info("Created sandbox dirs");

		setAddOnFilterActive(true);

		final File addOnExtensionPhysicalPath = new File(rootSandboxDir, ADDONTWO_NAME);
		webExtensionPhysicalPath = createWebCtxPhysicalPath();

		Mockito.doReturn(webExtensionPhysicalPath).when(extensionAccessor).getExtensionDir(STOREFRONT_NAME);
		Mockito.doReturn(addOnExtensionPhysicalPath).when(extensionAccessor).getExtensionDir(ADDONTWO_NAME);
		Mockito.doReturn("/addons/").when(request).getAttribute("javax.servlet.include.servlet_path");
	}

	/**
	 * Create web ctx physical path file.
	 *
	 * @return the file
	 */
	protected File createWebCtxPhysicalPath()
	{
		return new File(rootSandboxDir, STOREFRONT_NAME);
	}


	/**
	 * Create web target dir file.
	 *
	 * @return the file
	 */
	protected File createWebTargetDir()
	{
		return new File(rootSandboxDir, STOREFRONT_NAME + "/web" + getFolder() + "/addons/" + ADDONTWO_NAME);
	}

	/**
	 * Gets folder.
	 *
	 * @return the folder
	 */
	protected abstract String getFolder();


	/**
	 * Do after.
	 */
	@After
	public void doAfter()
	{
		try
		{
			clearUpStructure();
		}
		catch (final Exception e)
		{
			LOG.info("Failed to clear up temp add on structure " + e.getMessage());
		}

	}

	private void clearUpStructure() throws IOException, InterruptedException
	{
		if (rootSandboxDir.exists())
		{
			LOG.info("About to clear sandbox " + rootSandboxDir);
			FileUtils.forceDelete(rootSandboxDir);

			LOG.info("Sandbox exists - " + rootSandboxDir.exists());
		}
	}

	/**
	 * Wait a second.
	 *
	 * @throws InterruptedException
	 * 		the interrupted exception
	 */
	protected void waitASecond() throws InterruptedException
	{
		Thread.sleep(1000);
	}

	/**
	 * Create resource.
	 *
	 * @param rootDir
	 * 		the root dir
	 * @param relativePath
	 * 		the relative path
	 * @param fileName
	 * 		the file name
	 * @throws IOException
	 * 		the io exception
	 */
	protected void createResource(final File rootDir, final String relativePath, final String fileName) throws IOException
	{
		createResourceWithContent(rootDir, relativePath, fileName, "test");
	}

	/**
	 * Create resource with content.
	 *
	 * @param rootDir
	 * 		the root dir
	 * @param relativePath
	 * 		the relative path
	 * @param fileName
	 * 		the file name
	 * @param content
	 * 		the content
	 * @throws IOException
	 * 		the io exception
	 */
	protected void createResourceWithContent(final File rootDir, final String relativePath, final String fileName,
			final String content) throws IOException
	{
		final File dir = new File(rootDir, relativePath);
		FileUtils.forceMkdir(dir);

		FileUtils.write(new File(dir, fileName), content);
	}

	/**
	 * Verify file not created.
	 *
	 * @param rootDir
	 * 		the root dir
	 * @param relativePath
	 * 		the relative path
	 * @throws IOException
	 * 		the io exception
	 */
	protected void verifyFileNotCreated(final File rootDir, final String relativePath) throws IOException
	{
		final File dir = new File(rootDir, relativePath);
		Assert.assertFalse("File " + dir + " should not exists", dir.exists());
	}


	/**
	 * Verify file created with content.
	 *
	 * @param rootDir
	 * 		the root dir
	 * @param relativePath
	 * 		the relative path
	 * @param fileName
	 * 		the file name
	 * @param content
	 * 		the content
	 * @throws IOException
	 * 		the io exception
	 */
	protected void verifyFileCreatedWithContent(final File rootDir, final String relativePath, final String fileName,
			final String content) throws IOException
	{
		final File dir = new File(rootDir, relativePath);
		Assert.assertTrue("Directory " + dir + " should exists", dir.exists());

		Assert.assertEquals(content, FileUtils.readFileToString(new File(dir, fileName)));
	}

	/**
	 * Verify file created.
	 *
	 * @param rootDir
	 * 		the root dir
	 * @param relativePath
	 * 		the relative path
	 * @param fileName
	 * 		the file name
	 * @throws IOException
	 * 		the io exception
	 */
	protected void verifyFileCreated(final File rootDir, final String relativePath, final String fileName) throws IOException
	{
		verifyFileCreatedWithContent(rootDir, relativePath, fileName, "test");
	}


	/**
	 * Sets add on filter active.
	 *
	 * @param active
	 * 		the active
	 */
	protected void setAddOnFilterActive(final boolean active)
	{
		BDDMockito.given(
				Boolean.valueOf(configurationService.getConfiguration().getBoolean(
						AcceleratorAddOnFilter.ADDON_FILTER_ACTIVE_PROPERTY, false))).willReturn(Boolean.valueOf(active));
	}

	/**
	 * Prepare request.
	 *
	 * @param remotePath
	 * 		the remote path
	 */
	protected abstract void prepareRequest(String remotePath);


	/**
	 * Prepare local context path request.
	 *
	 * @param remotePath
	 * 		the remote path
	 */
	protected void prepareLocalContextPathRequest(final String remotePath)
	{

		Mockito.doReturn(remotePath).when(request).getRequestURI();
	}
}
