package com.wiley.ags.storefront.controllers.pages;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.storefront.util.PageTitleResolver;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.cms2.data.PagePreviewCriteriaData;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.cms2.servicelayer.services.CMSPreviewService;
import de.hybris.platform.commerceservices.security.SecureToken;
import de.hybris.platform.commerceservices.security.SecureTokenService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.core.customer.ResetPasswordRedirectType;
import com.wiley.welags.storefrontcommons.forms.WileyUpdatePwdForm;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PasswordResetPageControllerTest {

    private static final String PWD = "somePwd";
    private static final String CHECK_PWD_VALID = "somePwd";
    private static final String CHECK_PWD_INVALID = "anotherPwd";
    private static final String TOKEN = "token";
    private static final String EMAIL = "email@test.com";
    private static final String EMAIL_KEY = "email";

    @InjectMocks
    private PasswordResetPageController underTest = new PasswordResetPageController();

    private WileyUpdatePwdForm form;
    private SecureToken secureToken;

    @Mock
    private BindingResult mockBindingResult;
    @Mock
    private Model mockModel;
    @Mock
    private RedirectAttributes mockRedirectModel;
    @Mock
    private CMSPageService mockCmsPageService;
    @Mock
    private CMSPreviewService cmsPreviewService;
    @Mock
    private SecureTokenService mockSecureTokenService;
    @Mock
    private ContentPageModel mockContentPageModel;
    @Mock
    private PageTitleResolver mockPageTitleResolver;
    @Mock
    private ResourceBreadcrumbBuilder mockResourceBreadcrumbBuilder;

    @Before
    public void setUp() throws CMSItemNotFoundException {
        form = new WileyUpdatePwdForm();
        secureToken = new SecureToken(EMAIL, 0);
        when(mockSecureTokenService.decryptData(TOKEN)).thenReturn(secureToken);
        when(mockCmsPageService.getPageForLabelOrId(anyString(), any(PagePreviewCriteriaData.class))).thenReturn(
                mockContentPageModel);
        givenWeHaveForm(PWD, CHECK_PWD_INVALID, TOKEN);
    }

    @Test
    public void shouldNotAddPwdConfirmationErrorIfOtherErrorsExist() throws CMSItemNotFoundException {
        when(mockBindingResult.hasErrors()).thenReturn(true);
        underTest.changePassword(form, mockBindingResult, mockModel, mockRedirectModel);
        verify(mockBindingResult, never()).rejectValue("checkPwd", "validation.checkPwd.equals", new Object[] {},
                "validation.checkPwd.equals");
    }

    @Test
    public void shouldAddPwdConfirmationErrorIfNoOtherErrors() throws CMSItemNotFoundException {
        underTest.changePassword(form, mockBindingResult, mockModel, mockRedirectModel);
        verify(mockBindingResult).rejectValue("checkPwd", "validation.checkPwd.equals", new Object[] {},
                "validation.checkPwd.equals");
    }

    @Test
    public void shouldNotAddPwdConfirmationErrorIfPasswordsAreEqual() throws CMSItemNotFoundException {
        givenWeHaveForm(PWD, CHECK_PWD_VALID, TOKEN);
        underTest.changePassword(form, mockBindingResult, mockModel, mockRedirectModel);
        verify(mockBindingResult, never()).rejectValue("checkPwd", "validation.checkPwd.equals", new Object[] {},
                "validation.checkPwd.equals");
    }

    @Test
    public void shouldSaveEmailInModelForChangePassword() throws CMSItemNotFoundException {
        when(mockBindingResult.hasErrors()).thenReturn(true);
        underTest.changePassword(form, mockBindingResult, mockModel, mockRedirectModel);
        verify(mockModel).addAttribute(EMAIL_KEY, EMAIL);
    }

    @Test
    public void shouldSaveEmailInModeForGetChangePassword() throws CMSItemNotFoundException {
        when(mockBindingResult.hasErrors()).thenReturn(true);
        underTest.getChangePassword(TOKEN, ResetPasswordRedirectType.lp, mockModel);
        verify(mockModel).addAttribute(EMAIL_KEY, EMAIL);
    }

    private void givenWeHaveForm(final String pwd, final String checkPwd, final String token) {
        form.setPwd(pwd);
        form.setCheckPwd(checkPwd);
        form.setToken(token);
        form.setRedirectType(ResetPasswordRedirectType.getDefault());
    }
}
