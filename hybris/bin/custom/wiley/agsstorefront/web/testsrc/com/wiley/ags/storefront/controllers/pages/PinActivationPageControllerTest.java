package com.wiley.ags.storefront.controllers.pages;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.storefront.util.PageTitleResolver;
import de.hybris.platform.cms2.data.PagePreviewCriteriaData;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.model.pages.PageTemplateModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.cms2.servicelayer.services.CMSPreviewService;
import de.hybris.platform.order.InvalidCartException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.support.BindingAwareModelMap;

import com.wiley.facades.welags.order.WelAgsCheckoutFacade;
import com.wiley.facades.welags.pin.exception.PinOperationException;
import com.wiley.welags.storefrontcommons.form.PinActivationForm;
import com.wiley.welags.storefrontcommons.validators.WelAgsPinActivationValidator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;


/**
 * PIN activation page controller test.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PinActivationPageControllerTest
{
	/**
	 * The constant CMS_PAGE_MODEL.
	 */
	public static final String CMS_PAGE_MODEL = "cmsPage";
	private static final String VIEW_FOR_PAGE = "pinActivationPage.jsp";
	private static final String VIEW_PREFIX = "pages/";
	private static final String TITLE_FOR_PAGE = "PIN Activation Test Title";
	private static final String REDIRECT = "redirect:";
	private static final String PIN_ORDER_PAGE = "/pin/order";

	@Spy
	private final Model page = new BindingAwareModelMap();

	@Spy
	private final MockHttpSession session = new MockHttpSession();

	@InjectMocks
	private final PinActivationPageController controller = Mockito.spy(new PinActivationPageController());

	@Mock
	private ContentPageModel contentPageModel;
	@Mock
	private CMSPageService cmsPageService;
	@Mock
	private CMSPreviewService cmsPreviewService;
	@Mock
	private PageTitleResolver pageTitleResolver;
	@Mock
	private PageTemplateModel pageTemplateModel;
	@Mock
	private AbstractPageModel abstractPageModel;
	@Mock
	private PinActivationForm pinActivationForm;
	@Mock
	private BindingResult bindingResult;
	@Mock
	private WelAgsCheckoutFacade wileyCheckoutFacade;
	@Mock
	private WelAgsPinActivationValidator pinActivationValidator;


	@Before
	public void prepare() throws CMSItemNotFoundException
	{
		given(cmsPageService.getPageForLabelOrId(anyString(), any(PagePreviewCriteriaData.class))).willReturn(contentPageModel);
		given(pageTitleResolver.resolveContentPageTitle(anyString())).willReturn(TITLE_FOR_PAGE);
		given(Boolean.valueOf(page.containsAttribute(CMS_PAGE_MODEL))).willReturn(Boolean.TRUE);
		given(page.asMap().get(CMS_PAGE_MODEL)).willReturn(abstractPageModel);
		given(abstractPageModel.getMasterTemplate()).willReturn(pageTemplateModel);
		given(cmsPageService.getFrontendTemplateName(pageTemplateModel)).willReturn(VIEW_FOR_PAGE);
	}

	@Test
	public void shouldGetPinActivationForm() throws CMSItemNotFoundException
	{
		final String pinActivationPage = controller.showPinActivationPage(pinActivationForm, bindingResult, page);

		assertEquals(VIEW_PREFIX + VIEW_FOR_PAGE, pinActivationPage);
	}

	@Test
	public void shouldActivatePin() throws CMSItemNotFoundException, InvalidCartException, PinOperationException
	{
		final String view = controller.activatePin(pinActivationForm, bindingResult, page, session);
		
		verify(pinActivationValidator).validate(pinActivationForm, bindingResult);
		verify(bindingResult).hasErrors();
		verify(session).setAttribute(eq("pinActivationForm"), any(PinActivationForm.class));
		assertFalse(bindingResult.hasErrors());
		assertEquals(REDIRECT + PIN_ORDER_PAGE, view);
	}

	@Test
	public void shouldNotActivatePinAsPinIsInvalid() throws CMSItemNotFoundException, InvalidCartException, PinOperationException
	{
		given(Boolean.valueOf(bindingResult.hasErrors())).willReturn(Boolean.TRUE);

		final String view = controller.activatePin(pinActivationForm, bindingResult, page, session);

		verify(pinActivationValidator).validate(pinActivationForm, bindingResult);
		verify(session, never()).setAttribute(eq("pinActivationForm"), any(PinActivationForm.class));
		verify(bindingResult).hasErrors();
		assertEquals(VIEW_PREFIX + VIEW_FOR_PAGE, view);
		assertTrue(bindingResult.hasErrors());
	}
}
