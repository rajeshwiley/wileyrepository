UPDATE GenericItem[processor = de.hybris.platform.commerceservices.impex.impl.ConfigPropertyImportProcessor]; pk[unique = true]
#
# Import the Product Catalog and Classification Catalog
#
$lang = en
$productCatalog = asProductCatalog
$resourceFolder = /wileyasinitialdata/import/coredata/productCatalogs/$productCatalog
$csvFolder = $resourceFolder/csv
$classificationCatalog = $config-wileyas.classification.system.id
$classificationCatalogVersionNum = $config-wileyas.classification.system.version
$classificationCatalogVersion = catalogVersion(catalog(id[default = $classificationCatalog]), version[default = $classificationCatalogVersionNum])[default = $classificationCatalog:$classificationCatalogVersionNum, forceWrite = true, unique = true, allownull = true]
$classificationSystemVersion = systemVersion(catalog(id[default = $classificationCatalog]), version[default = $classificationCatalogVersionNum])[default = $classificationCatalog:$classificationCatalogVersionNum, forceWrite = true, unique = true, allownull = true]

# Create Catalog
UPDATE Catalog; id[unique = true]; name[lang = $lang]
; $productCatalog ; "AS Product Catalog"

INSERT_UPDATE ClassificationSystem; id[unique = true]; name[lang = $lang]
; $classificationCatalog ; "AS Classification Catalog"

INSERT_UPDATE ClassificationClass[disable.interceptor.beans = 'wileyCategorySolrRelatedChangesTimePrepareInterceptor']; $classificationCatalogVersion[unique = true]; code[unique = true]; name[lang = $lang]
; ; journal  ; "Journal"
; ; cover    ; "Cover"
; ; video    ; "Video"
; ; length   ; "Length"
; ; editing  ; "Editing"
; ; service  ; "Service"
; ; language ; "Language"

INSERT_UPDATE ClassificationAttributeValue; $classificationSystemVersion[unique = true]; code[unique = true]; name[lang = $lang]
; ; front            ; "Front"
; ; back             ; "Back"
; ; frontInside      ; "Front Inside"
; ; backInside       ; "Back Inside"
; ; frontispiece     ; "Frontispiece"
; ; basic            ; "Basic"
; ; advanced         ; "Advanced"
; ; less500words     ; "Fewer than 500 words"
; ; 500_1500words    ; "500 - 1500 words"
; ; 1501_3500words   ; "1501 - 3500 words"
; ; 3501_6000words   ; "3501 - 6000 words"
; ; 6001_8000words   ; "6001 - 8000 words"
; ; 8001_10000words  ; "8001 - 10000 words"
; ; 10001_12000words ; "10001 - 12000 words"
; ; 12001_15000words ; "12001 - 15000 words"
; ; 15001_18000words ; "15001 - 18000 words"
; ; more18000words   ; "More than 18000 words"
; ; english          ; "English"
; ; portuguese       ; "Portuguese"
; ; chinese          ; "Chinese"
; ; spanish          ; "Spanish"
; ; short            ; "Short"
; ; long             ; "Long"

# Insert Journal classification Attribute values
INSERT_UPDATE ClassificationAttributeValue; code[unique = true]; name[lang = $lang]; $classificationSystemVersion[unique = true]
#% impex.includeExternalData(ImpExManager.class.getResourceAsStream("$csvFolder/journal-values.csv"), "UTF-8", 1)

INSERT_UPDATE ClassificationAttributeUnit; code[unique = true]; name[lang = $lang]; $classificationSystemVersion[unique = true]
; days ; "days" ;

# Insert classification attributes
INSERT_UPDATE ClassificationAttribute; $classificationSystemVersion[unique = true]; code[unique = true]; name[lang=$lang]
; ; journal            ; "Journal"
; ; coverPosition      ; "Cover Position"
; ; editingLevel       ; "Editing Level"
; ; turnaroundTime     ; "Turnaround Time"
; ; wordCountRange     ; "Word Count"
; ; manuscriptLanguage ; "Manuscript Language"
; ; videoDuration      ; "Video Duration"