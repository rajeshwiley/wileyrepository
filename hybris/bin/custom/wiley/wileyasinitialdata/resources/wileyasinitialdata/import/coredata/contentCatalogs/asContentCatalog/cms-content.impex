#
# Import the CMS content for the site
#
$contentCatalog = asContentCatalog
$contentCV = catalogVersion(CatalogVersion.catalog(Catalog.id[default = $contentCatalog]), CatalogVersion.version[default = Staged])[default = $contentCatalog:Staged]
$siteResource = jar:com.wiley.wileyas.initialdata.setup.InitialDataSystemSetup&/wileyasinitialdata/import/sampledata/contentCatalogs/$contentCatalog

# Import config properties into impex macros
UPDATE GenericItem[processor = de.hybris.platform.commerceservices.impex.impl.ConfigPropertyImportProcessor]; pk[unique = true]
$jarResourceCms = $config-jarResourceCmsValue
$wileyasBaseUrl = $config-wileyas.base.url

# Create PageTemplates
# These define the layout for pages
# "FrontendTemplateName" is used to define the JSP that should be used to render the page for pages with multiple possible layouts.
# "RestrictedPageTypes" is used to restrict templates to page types
INSERT_UPDATE PageTemplate; $contentCV[unique = true]; uid[unique = true]; name; frontendTemplateName; restrictedPageTypes(code); active[default = true]
; ; AccountPageTemplate                  ; Account Page Template                     ; account/accountLayoutPage                 ; ContentPage ; false ;
; ; OrderEditingPageTemplate             ; Order Editing Page Template               ; account/accountOrderEditingPage           ; ContentPage ; false ;
; ; CartPageTemplate                     ; Cart Page Template                        ; cart/cartPage                             ; ContentPage ; false ;

# Templates without a frontendTemplateName
; ; MultiStepCheckoutSummaryPageTemplate ; Multi Step Checkout Summary Page Template ;                                           ; ContentPage ; false ;
; ; OrderConfirmationPageTemplate        ; Order Confirmation Page Template          ; checkout/checkoutConfirmationPageNoPayPal ; ContentPage ; false ;
; ; ErrorPageTemplate                    ; Error Page Template                       ;                                           ; ContentPage ; false ;

# Add Velocity templates that are in the CMS Cockpit. These give a better layout for editing pages
# The FileLoaderValueTranslator loads a File into a String property. The templates could also be inserted in-line in this file.
UPDATE PageTemplate; $contentCV[unique = true]; uid[unique = true]; velocityTemplate[translator = de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]
; ; CartPageTemplate                     ; $jarResourceCms/structure-view/as/structure_cartPageTemplate.vm
; ; AccountPageTemplate                  ; $jarResourceCms/structure-view/as/structure_accountPageTemplate.vm
; ; OrderEditingPageTemplate             ; $jarResourceCms/structure-view/as/structure_orderEditingPageTemplate.vm
; ; ErrorPageTemplate                    ; $jarResourceCms/structure-view/as/structure_errorPageTemplate.vm
; ; MultiStepCheckoutSummaryPageTemplate ; $jarResourceCms/structure-view/as/structure_multiStepCheckoutSummaryPageTemplate.vm
; ; OrderConfirmationPageTemplate        ; $jarResourceCms/structure-view/as/structure_orderConfirmationPageTemplate.vm

# Create ContentSlotNames
# Each PageTemplate has a number of ContentSlotNames, with a list of valid components for the slot.
# There are a standard set of slots and a number of specific slots for each template.
# Standard slots are SiteLogo, HeaderLinks, MiniCart and NavigationBar (that all appear in the Header), and the Footer.

# Error Page Template
INSERT_UPDATE ContentSlotName; name[unique = true]; template(uid, $contentCV)[unique = true][default = 'ErrorPageTemplate']; validComponentTypes(code); compTypeGroup(code)
; SiteLogo               ; ; ; logo
; HeaderLinks            ; ; ; headerlinks
; SearchBox              ; ; ; searchbox
; MiniCart               ; ; ; minicart
; NavigationBar          ; ; ; navigation
; MiddleContent          ; ; CMSParagraphComponent
; BottomContent          ; ; ; wide
; SideContent            ; ; ; narrow
; Footer                 ; ; ; footer
; TopHeaderSlot          ; ; ; wide
; BottomHeaderSlot       ; ; ; wide
; PlaceholderContentSlot ; ; ;
; FooterNavigationSlot   ; ; ; wileyFooter
; FooterCopyrightSlot    ; ; ; wileyFooter
; CompanyLogoSlot        ; ; ; wileyLogo

# Category and Landing Pages are various layouts for a Category Landing page
# Landing Pages are also good layouts for Homepages or general Content Pages

# Cart Page Template
INSERT_UPDATE ContentSlotName; name[unique = true]; template(uid, $contentCV)[unique = true][default = 'CartPageTemplate']; validComponentTypes(code); compTypeGroup(code)
; SiteLogo                   ; ; ; wileyLogo
; TopContent                 ; ; ; wide
; EmptyCartMiddleContentSlot ; ; CMSParagraphComponent
; Footer                     ; ; ; footer
; TopHeaderSlot              ; ; ; wide
; CenterLeftContentSlot      ; ; ; narrow
; CenterRightContentSlot     ; ; ; narrow
; BottomContentSlot          ; ; ; wide
; PlaceholderContentSlot     ; ; ;
; FooterNavigationSlot       ; ; ; wileyFooter
; FooterCopyrightSlot        ; ; ; wileyFooter
; CompanyLogoSlot            ; ; ; wileyLogo
; EmptyCartBottomContentSlot ; ; ; wide

# Multi Step Checkout Summary Page Templates
INSERT_UPDATE ContentSlotName; name[unique = true]; template(uid, $contentCV)[unique = true][default = 'MultiStepCheckoutSummaryPageTemplate']; validComponentTypes(code); compTypeGroup(code)
; SiteLogo               ; ; ; wileyLogo
; SideContent            ; ; ; narrow
; Footer                 ; ; ; footer
; TopHeaderSlot          ; ; ; wide
; PlaceholderContentSlot ; ; ;
; FooterNavigationSlot   ; ; ; wileyFooter
; FooterCopyrightSlot    ; ; ; wileyFooter
; CompanyLogoSlot        ; ; ; wileyLogo
; RightContent           ; ; ;

# Order Confirmation Page Template
INSERT_UPDATE ContentSlotName; name[unique = true]; template(uid, $contentCV)[unique = true][default = 'OrderConfirmationPageTemplate']; validComponentTypes(code); compTypeGroup(code)
; SiteLogo               ; ;                      ; wileyLogo
; SideContent            ; ; JspIncludeComponent  ; narrow
; TopContent             ; ;                      ; wide
; Footer                 ; ;                      ; footer
; TopHeaderSlot          ; ;                      ; wide
; PlaceholderContentSlot ; ;                      ;
; FooterNavigationSlot   ; ;                      ; wileyFooter
; FooterCopyrightSlot    ; ;                      ; wileyFooter
; CompanyLogoSlot        ; ;                      ; wileyLogo

# Account Page Template
# Template used for all of the Account pages
INSERT_UPDATE ContentSlotName; name[unique = true]; template(uid, $contentCV)[unique = true][default = 'AccountPageTemplate']; validComponentTypes(code); compTypeGroup(code)
; SiteLogo               ; ; ; wileyLogo
; TopContent             ; ; ; wide
; SideContent            ; ; ; narrow
; BodyContent            ; ; ; wide
; BottomContent          ; ; ; wide
; Footer                 ; ; ; footer
; TopHeaderSlot          ; ; ; wide
; PlaceholderContentSlot ; ; ;
; FooterNavigationSlot   ; ; ; wileyFooter
; FooterCopyrightSlot    ; ; ; wileyFooter
; CompanyLogoSlot        ; ; ; wileyLogo

# Order Editing Page Template
INSERT_UPDATE ContentSlotName; name[unique = true]; template(uid, $contentCV)[unique = true][default = 'OrderEditingPageTemplate']; validComponentTypes(code); compTypeGroup(code)
; TopHeaderSlot        ; ; ; wide
; SiteLogo             ; ; ; wileyLogo
; SideContent          ; ; ; narrow
; FooterNavigationSlot ; ; ; wileyFooter
; FooterCopyrightSlot  ; ; ; wileyFooter
; CompanyLogoSlot      ; ; ; wileyLogo

# Create Content Slots
INSERT_UPDATE ContentSlot; $contentCV[unique = true]; uid[unique = true]; name; active
; ; SiteLogoSlot               ; Site Logo Slot                  ; true
; ; HomepageNavLinkSlot        ; Default Homepage Link           ; true
; ; MiniCartSlot               ; Mini Cart                       ; true
; ; NavigationBarSlot          ; Navigation Bar                  ; true
; ; TabsSlot                   ; Tabs                            ; true
; ; TopContentSlot             ; Top Content                     ; true
; ; SideContentSlot            ; Side Content                    ; true
; ; BottomContentSlot          ; Bottom Content                  ; true
; ; FeatureContentSlot         ; Feature Content                 ; true
; ; FooterSlot                 ; Footer                          ; true
; ; HeaderLinksSlot            ; Header links                    ; true
; ; SearchBoxSlot              ; Search Box                      ; true
; ; VariantSelectorSlot        ; Variant Selector                ; true
; ; AddToCartSlot              ; Add To Cart                     ; true
; ; UpSellingSlot              ; Up Selling                      ; true
; ; CrossSellingSlot           ; Cross Selling                   ; true
; ; TopHeaderSlot              ; Top Header                      ; true
; ; BottomHeaderSlot           ; Bottom Header                   ; true
; ; ProductLeftRefinements     ; Refinements                     ; true
; ; ProductGridSlot            ; Product List                    ; true
; ; ProductListSlot            ; Product Grid                    ; true
; ; SearchResultsListSlot      ; Search Result List              ; true
; ; SearchResultsGridSlot      ; Search Result Grid              ; true
; ; MiddleContentSlot          ; Middle Content Slot             ; true
; ; LeftContentSlot            ; Left Content Slot               ; true
; ; RightContentSlot           ; Right Content Slot              ; true
; ; CenterContentSlot          ; Center Content Slot             ; true
; ; CenterLeftContentSlot      ; Center Left Content Slot        ; true
; ; CenterRightContentSlot     ; Center Right Content Slot       ; true
; ; EmptyCartMiddleContentSlot ; Empty CartMiddle Content Slot   ; true
; ; PlaceholderContentSlot     ; Placeholder for Addon tag files ; true
; ; FooterCopyrightSlot        ; Footer Copyright Slot           ; true
; ; FooterNavigationSlot       ; Footer Navigation Slot          ; true
; ; CompanyLogoSlot            ; Company Logo Slot               ; true
; ; EmptyCartBottomContentSlot ; Empty Cart Bottom Content Slot  ; true


INSERT_UPDATE ContentSlotForTemplate; $contentCV[unique = true]; uid[unique = true]; position[unique = true]; pageTemplate(uid, $contentCV)[unique = true][default = 'CartPageTemplate']; contentSlot(uid, $contentCV)[unique = true]; allowOverwrite
; ; SiteLogo-CartPage                   ; SiteLogo                   ; ; SiteLogoSlot               ; true
; ; Footer-CartPage                     ; Footer                     ; ; FooterSlot                 ; true
; ; TopHeaderSlot-CartPage              ; TopHeaderSlot              ; ; TopHeaderSlot              ; true
; ; BottomContentSlot-CartPage          ; BottomContentSlot          ; ; BottomContentSlot          ; true
; ; CenterRightContentSlot-CartPage     ; CenterRightContentSlot     ; ; CenterRightContentSlot     ; true
; ; CenterLeftContentSlot-CartPage      ; CenterLeftContentSlot      ; ; CenterLeftContentSlot      ; true
; ; EmptyCartMiddleContentSlot-CartPage ; EmptyCartMiddleContentSlot ; ; EmptyCartMiddleContentSlot ; true
; ; PlaceholderContentSlot-CartPage     ; PlaceholderContentSlot     ; ; PlaceholderContentSlot     ; true
; ; FooterNavigationSlot-CartPage       ; FooterNavigationSlot       ; ; FooterNavigationSlot       ; true
; ; CompanyLogoSlot-CartPage            ; CompanyLogoSlot            ; ; CompanyLogoSlot            ; true
; ; FooterCopyrightSlot-CartPage        ; FooterCopyrightSlot        ; ; FooterCopyrightSlot        ; true
; ; EmptyCartBottomContentSlot-CartPage ; EmptyCartBottomContentSlot ; ; EmptyCartBottomContentSlot ; true

# Bind Content Slots to Multi Step Checkout Summary Page Templates
INSERT_UPDATE ContentSlotForTemplate; $contentCV[unique = true]; uid[unique = true]; position[unique = true]; pageTemplate(uid, $contentCV)[unique = true][default = 'MultiStepCheckoutSummaryPageTemplate']; contentSlot(uid, $contentCV)[unique = true]; allowOverwrite
; ; SiteLogo-MultiStepCheckoutSummaryPage               ; SiteLogo               ; ; SiteLogoSlot           ; true
; ; HomepageLink-MultiStepCheckoutSummaryPage           ; HomepageNavLink        ; ; HomepageNavLinkSlot    ; true
; ; Footer-MultiStepCheckoutSummaryPage                 ; Footer                 ; ; FooterSlot             ; true
; ; TopHeaderSlot-MultiStepCheckoutSummaryPage          ; TopHeaderSlot          ; ; TopHeaderSlot          ; true
; ; PlaceholderContentSlot-MultiStepCheckoutSummaryPage ; PlaceholderContentSlot ; ; PlaceholderContentSlot ; true
; ; FooterNavigationSlot-MultiStepCheckoutSummaryPage   ; FooterNavigationSlot   ; ; FooterNavigationSlot   ; true
; ; CompanyLogoSlot-MultiStepCheckoutSummaryPage        ; CompanyLogoSlot        ; ; CompanyLogoSlot        ; true
; ; FooterCopyrightSlot-MultiStepCheckoutSummaryPage    ; FooterCopyrightSlot    ; ; FooterCopyrightSlot    ; true

INSERT_UPDATE ContentSlotForTemplate; $contentCV[unique = true]; uid[unique = true]; position[unique = true]; pageTemplate(uid, $contentCV)[unique = true][default = 'OrderConfirmationPageTemplate']; contentSlot(uid, $contentCV)[unique = true]; allowOverwrite
; ; SiteLogo-OrderConfirmationPage               ; SiteLogo               ; ; SiteLogoSlot           ; true
; ; HomepageLink-OrderConfirmationPage           ; HomepageNavLink        ; ; HomepageNavLinkSlot    ; true
; ; Footer-OrderConfirmationPage                 ; Footer                 ; ; FooterSlot             ; true
; ; TopHeaderSlot-OrderConfirmationPage          ; TopHeaderSlot          ; ; TopHeaderSlot          ; true
; ; PlaceholderContentSlot-OrderConfirmationPage ; PlaceholderContentSlot ; ; PlaceholderContentSlot ; true
; ; FooterNavigationSlot-OrderConfirmationPage   ; FooterNavigationSlot   ; ; FooterNavigationSlot   ; true
; ; CompanyLogoSlot-OrderConfirmationPage        ; CompanyLogoSlot        ; ; CompanyLogoSlot        ; true
; ; FooterCopyrightSlot-OrderConfirmationPage    ; FooterCopyrightSlot    ; ; FooterCopyrightSlot    ; true

INSERT_UPDATE ContentSlotForTemplate; $contentCV[unique = true]; uid[unique = true]; position[unique = true]; pageTemplate(uid, $contentCV)[unique = true][default = 'AccountPageTemplate']; contentSlot(uid, $contentCV)[unique = true]; allowOverwrite
; ; SiteLogo-AccountPage               ; SiteLogo               ; ; SiteLogoSlot           ; true
; ; HomepageLink-AccountPage           ; HomepageNavLink        ; ; HomepageNavLinkSlot    ; true
; ; Footer-AccountPage                 ; Footer                 ; ; FooterSlot             ; true
; ; TopHeaderSlot-AccountPage          ; TopHeaderSlot          ; ; TopHeaderSlot          ; true
; ; PlaceholderContentSlot-AccountPage ; PlaceholderContentSlot ; ; PlaceholderContentSlot ; true
; ; FooterNavigationSlot-AccountPage   ; FooterNavigationSlot   ; ; FooterNavigationSlot   ; true
; ; CompanyLogoSlot-AccountPage        ; CompanyLogoSlot        ; ; CompanyLogoSlot        ; true
; ; FooterCopyrightSlot-AccountPage    ; FooterCopyrightSlot    ; ; FooterCopyrightSlot    ; true

INSERT_UPDATE ContentSlotForTemplate; $contentCV[unique = true]; uid[unique = true]; position[unique = true]; pageTemplate(uid, $contentCV)[unique = true][default = 'OrderEditingPageTemplate']; contentSlot(uid, $contentCV)[unique = true]; allowOverwrite
; ; TopHeaderSlot-OrderEditingPage        ; TopHeaderSlot        ; ; TopHeaderSlot        ; true
; ; SiteLogo-OrderEditingPage             ; SiteLogo             ; ; SiteLogoSlot         ; true
; ; SideContent-OrderEditingPage          ; SideContent          ; ; SideContentSlot      ; true
; ; FooterNavigationSlot-OrderEditingPage ; FooterNavigationSlot ; ; FooterNavigationSlot ; true
; ; FooterCopyrightSlot-OrderEditingPage  ; FooterCopyrightSlot  ; ; FooterCopyrightSlot  ; true
; ; CompanyLogoSlot-OrderEditingPage      ; CompanyLogoSlot      ; ; CompanyLogoSlot      ; true

INSERT_UPDATE ContentSlotForTemplate; $contentCV[unique = true]; uid[unique = true]; position[unique = true]; pageTemplate(uid, $contentCV)[unique = true][default = 'ErrorPageTemplate']; contentSlot(uid, $contentCV)[unique = true]; allowOverwrite
; ; SiteLogo-ErrorPage               ; SiteLogo               ; ; SiteLogoSlot           ; true
; ; HomepageLink-ErrorPage           ; HomepageNavLink        ; ; HomepageNavLinkSlot    ; true
; ; NavigationBar-ErrorPage          ; NavigationBar          ; ; NavigationBarSlot      ; true
; ; MiniCart-ErrorPage               ; MiniCart               ; ; MiniCartSlot           ; true
; ; Footer-ErrorPage                 ; Footer                 ; ; FooterSlot             ; true
; ; HeaderLinks-ErrorPage            ; HeaderLinks            ; ; HeaderLinksSlot        ; true
; ; SearchBox-ErrorPage              ; SearchBox              ; ; SearchBoxSlot          ; true
; ; TopHeaderSlot-ErrorPage          ; TopHeaderSlot          ; ; TopHeaderSlot          ; true
; ; BottomHeaderSlot-ErrorPage       ; BottomHeaderSlot       ; ; BottomHeaderSlot       ; true
; ; PlaceholderContentSlot-ErrorPage ; PlaceholderContentSlot ; ; PlaceholderContentSlot ; true
; ; FooterNavigationSlot-ErrorPage   ; FooterNavigationSlot   ; ; FooterNavigationSlot   ; true
; ; CompanyLogoSlot-ErrorPage        ; CompanyLogoSlot        ; ; CompanyLogoSlot        ; true
; ; FooterCopyrightSlot-ErrorPage    ; FooterCopyrightSlot    ; ; FooterCopyrightSlot    ; true

# Create Content Pages

# Preview Image for use in the CMS Cockpit for special ContentPages
INSERT_UPDATE Media; $contentCV[unique = true]; code[unique = true]; mime; realfilename; @media[translator = de.hybris.platform.impex.jalo.media.MediaDataTranslator][forceWrite = true]
; ; ContentPageModel__function_preview ; text/gif ; ContentPageModel__function_preview.gif ; $jarResourceCms/preview-images/ContentPageModel__function_preview.gif

# Functional Content Pages
INSERT_UPDATE ContentPage; $contentCV[unique = true]; uid[unique = true]; name; masterTemplate(uid, $contentCV); label; defaultPage[default = 'true']; approvalStatus(code)[default = 'approved']; homepage[default = 'false']; previewImage(code, $contentCV)[default = 'ContentPageModel__function_preview']
; ; multiStepCheckoutSummaryPage ; Multi Checkout Summary Page    ; MultiStepCheckoutSummaryPageTemplate ; multiStepCheckoutSummary
; ; orderConfirmationPage        ; Order Confirmation Page        ; OrderConfirmationPageTemplate        ; orderConfirmation
; ; cartPage                     ; Cart Page                      ; CartPageTemplate                     ; cart ; ; ; true
; ; order                        ; Order Details Page             ; AccountPageTemplate                  ; order
; ; orders                       ; Order History Page             ; AccountPageTemplate                  ; orders
; ; orderEditingPage             ; Order Editing Page             ; OrderEditingPageTemplate             ; orderEditingPage
; ; notFound                     ; 404 Error Page                 ; ErrorPageTemplate                    ; notFound

# CMS Assisted Service Component
INSERT_UPDATE AssistedServiceComponent; $contentCV[unique = true]; uid[unique = true]; name; &componentRef
; ; AssistedServiceComponent ; Assisted Service Component ; AssistedServiceComponent

#
INSERT_UPDATE SimpleResponsiveBannerComponent; $contentCV[unique = true]; uid[unique = true]; urlLink; &componentRef
; ; HeaderLogoComponent ; ; HeaderLogoComponent

# Content Slots
INSERT_UPDATE ContentSlot; $contentCV[unique = true]; uid[unique = true]; active; cmsComponents(&componentRef)
; ; SiteLogoSlot  ; true ; HeaderLogoComponent


INSERT_UPDATE CMSNavigationNode; uid[unique = true]; $contentCV[unique = true]; name; parent(uid, $contentCV);
; root           ; ; root           ;                ;
; CommonRootNode ; ; CommonRootNode ; root           ;
; WileyAsNavNode ; ; WileyAS Site   ; CommonRootNode ;

INSERT_UPDATE CMSLinkComponent; $contentCV[unique = true]; uid[unique = true]; url; target(code)[default = 'newWindow']
; ; WileyComLink                    ; "https://www.wiley.com/"                                          ;
; ; WileyOnlineLibraryLink          ; "http://onlinelibrary.wiley.com/"                                 ;
; ; WileyPrivacyPolicyLink          ; "https://www.wiley.com/privacy"                                   ;
; ; WileyTermsAndConditionsLink     ; "https://www.wiley.com/terms-of-use"                              ;
; ; WileyContactUsLink              ; "https://hub.wiley.com/docs/DOC-11761?referrer=authorservices"    ;
; ; WileyHelpLink                   ; "https://hub.wiley.com/community/support/authorservices"          ;

INSERT_UPDATE CMSNavigationNode; $contentCV[unique = true]; uid[unique = true]; parent(uid, $contentCV); links(uid, $contentCV)
; ; WileyComNode                ; WileyAsNavNode  ; WileyComLink
; ; WileyOnlineLibraryNode      ; WileyAsNavNode  ; WileyOnlineLibraryLink
; ; WileyPrivacyPolicyNode      ; WileyAsNavNode  ; WileyPrivacyPolicyLink
; ; WileyTermsAndConditionsNode ; WileyAsNavNode  ; WileyTermsAndConditionsLink
; ; WileyContactUsNode          ; WileyAsNavNode  ; WileyContactUsLink
; ; WileyHelpNode               ; WileyAsNavNode  ; WileyHelpLink

INSERT_UPDATE CMSParagraphComponent; $contentCV[unique = true]; uid[unique = true]; name; &componentRef
; ; CopyrightParagraphComponent ; Copyright Paragraph Component ; CopyrightParagraphComponent

INSERT_UPDATE SimpleResponsiveBannerComponent; $contentCV[unique = true]; uid[unique = true]; name; urlLink; &componentRef
; ; CompanyLogoComponent ; Company Logo Component ; $wileyasBaseUrl ; CompanyLogoComponent

INSERT_UPDATE FooterComponent; $contentCV[unique = true]; uid[unique = true]; navigationNodes(uid, $contentCV);  wrapAfter; &componentRef
; ; FooterNavigationComponent ; WileyComNode, WileyOnlineLibraryNode, WileyPrivacyPolicyNode, WileyTermsAndConditionsNode, WileyContactUsNode, WileyHelpNode ; 2 ;FooterNavigationComponent

# Create Content Slots
UPDATE ContentSlot; $contentCV[unique = true]; uid[unique = true]; cmsComponents(&componentRef)
; ; FooterCopyrightSlot  ; CopyrightParagraphComponent
; ; FooterNavigationSlot ; FooterNavigationComponent
; ; CompanyLogoSlot      ; CompanyLogoComponent

# Cart Page
INSERT_UPDATE CMSParagraphComponent; $contentCV[unique = true]; uid[unique = true]; name; &componentRef
; ; EmptyCartParagraphComponent ; Empty Cart Paragraph Component ; EmptyCartParagraphComponent

INSERT_UPDATE JspIncludeComponent; $contentCV[unique = true]; uid[unique = true]; name; page; actions(uid, $contentCV); &componentRef
; ; CartComponent       ; Cart Display Component        ; cartDisplay.jsp       ; ; CartComponent
; ; CartTotalsComponent ; Cart Totals Display Component ; cartTotalsDisplay.jsp ; ; CartTotalsComponent
; ; CheckoutComponent   ; Checkout Display Component    ; checkoutDisplay.jsp   ; ; CheckoutComponent

INSERT_UPDATE ProductCarouselComponent; $contentCV[unique = true]; uid[unique = true]; name; popup ; &componentRef
; ; WileyasProductCarouselComponent ; Product Carousel Component ; false ; WileyasProductCarouselComponent

INSERT_UPDATE CartSuggestionComponent; $contentCV[unique = true]; uid[unique = true]; name; productReferenceTypes(code); maximumNumberProducts; filterPurchased; &componentRef
; ; CartSuggestions ; Cart Suggestions ; CROSSELLING ; 20 ; true ; CartSuggestions

INSERT_UPDATE ContentSlot; $contentCV[unique = true]; uid[unique = true]; name; active; cmsComponents(&componentRef)
; ; TopContent-cartPage                 ; Top Content Slot for Cart Page               ; true ; CartComponent
; ; EmptyCartMiddleContentSlot-cartPage ; Empty Cart Middle Content Slot for Cart Page ; true ; EmptyCartParagraphComponent
; ; CenterLeftContentSlot-cartPage      ; Center Left Content Slot for Cart Page       ; true ;
; ; CenterRightContentSlot-cartPage     ; Center Right Content Slot for Cart Page      ; true ; CartTotalsComponent
; ; BottomContentSlot-cartPage          ; Bottom Content Slot for Cart Page            ; true ; CheckoutComponent, CartSuggestions
; ; EmptyCartBottomContentSlot-cartPage ; Empty Cart Bottom Content Slot for Cart Page ; true ; CheckoutComponent, WileyasProductCarouselComponent

INSERT_UPDATE ContentSlotForPage; $contentCV[unique = true]; uid[unique = true]; position[unique = true]; page(uid, $contentCV)[unique = true][default = 'cartPage']; contentSlot(uid, $contentCV)[unique = true]
; ; TopContentSlotForCartPage             ; TopContent                 ; ; TopContent-cartPage
; ; EmptyCartMiddleContentSlotForCartPage ; EmptyCartMiddleContentSlot ; ; EmptyCartMiddleContentSlot-cartPage
; ; CenterLeftContentSlotForCartPage      ; CenterLeftContentSlot      ; ; CenterLeftContentSlot-cartPage
; ; CenterRightContentSlotForCartPage     ; CenterRightContentSlot     ; ; CenterRightContentSlot-cartPage
; ; BottomContentSlotForCartPage          ; BottomContentSlot          ; ; BottomContentSlot-cartPage
; ; EmptyCartBottomContentSlotForCartPage ; EmptyCartBottomContentSlot ; ; EmptyCartBottomContentSlot-cartPage

INSERT_UPDATE ContentSlot; $contentCV[unique = true]; uid[unique = true]; name; active
; ; SideContent-multiStepCheckoutSummaryPage ; Side Slot for multiStepCheckoutSummaryPage ; true
; ; RightContent-multiStepCheckoutSummaryPage      ; Area with Checkout Summary Data       ; true

INSERT_UPDATE ContentSlotForPage; $contentCV[unique = true]; uid[unique = true]; position[unique = true]; page(uid, $contentCV)[unique = true]; contentSlot(uid, $contentCV)[unique = true]
; ; Side-multiStepCheckoutSummaryPage ; SideContent ; multiStepCheckoutSummaryPage ; SideContent-multiStepCheckoutSummaryPage
; ; Right-multiStepCheckoutSummaryPage ; RightContent ; multiStepCheckoutSummaryPage ; RightContent-multiStepCheckoutSummaryPage


#Order Confirmation Page
INSERT_UPDATE JspIncludeComponent; $contentCV[unique = true]; uid[unique = true]; name; page; actions(uid, $contentCV); &componentRef
; ; orderConfirmationMessageComponent ; Order Confirmation Message Component ; /WEB-INF/views/responsive/pages/checkout/orderConfirmationMessage.jsp ; ; orderConfirmationMessageComponent

INSERT_UPDATE ContentSlot; $contentCV[unique = true]; uid[unique = true]; name; active; cmsComponents(&componentRef)
; ; SideContentSlot-OrderConfirmationPage ; Side Content Slot for Order Confirmation Page ; true ; orderConfirmationMessageComponent

INSERT_UPDATE ContentSlotForPage; $contentCV[unique = true]; uid[unique = true]; position[unique = true]; page(uid, $contentCV)[unique = true][default = 'orderConfirmationPage']; contentSlot(uid, $contentCV)[unique = true]
; ; SideContent-OrderConfirmationPage ; SideContent ; ; SideContentSlot-OrderConfirmationPage

INSERT_UPDATE ContentSlot; $contentCV[unique = true]; uid[unique = true]; name; active
; ; SideContent-orderEditingPage ; Side Slot for orderEditingPage ; true

INSERT_UPDATE ContentSlotForPage; $contentCV[unique = true]; uid[unique = true]; position[unique = true]; page(uid, $contentCV)[unique = true]; contentSlot(uid, $contentCV)[unique = true]
; ; Side-orderEditingPage ; SideContent ; orderEditingPage ; SideContent-orderEditingPage



# Orders page (account order history)
INSERT_UPDATE JspIncludeComponent; $contentCV[unique = true]; uid[unique = true]; name; page; actions(uid, $contentCV); &componentRef
; ; AccountOrderHistoryComponent ; Account History Component ; accountOrderHistoryPage.jsp ; ; AccountOrderHistoryComponent

INSERT_UPDATE ContentSlot; $contentCV[unique = true]; uid[unique = true]; name; active; cmsComponents(&componentRef)
; ; BodyContent-orders ; Body Content Slot for My Account Order History ; true ; AccountOrderHistoryComponent

INSERT_UPDATE ContentSlotForPage; $contentCV[unique = true]; uid[unique = true]; position[unique = true]; page(uid, $contentCV)[unique = true][default = 'orders']; contentSlot(uid, $contentCV)[unique = true]
; ; BodyContent-orders ; BodyContent ; ; BodyContent-orders


# Account Order Details
INSERT_UPDATE JspIncludeComponent; $contentCV[unique = true]; uid[unique = true]; name; page; actions(uid, $contentCV); &componentRef
; ; AccountOrderHistoryDetailsComponent ; Account Order History Details Component ; accountOrderHistoryDetailsPage.jsp ; ; AccountOrderHistoryDetailsComponent

# ContentSlot
INSERT_UPDATE ContentSlot; $contentCV[unique = true]; uid[unique = true]; name; active; cmsComponents(&componentRef)
; ; BodyContent-orderdetail   ; Body Content Slot for My Account Order Details   ; true ; AccountOrderHistoryDetailsComponent
; ; SideContent-orderdetail   ; Side Content Slot for My Account Order Details   ; true ;
; ; TopContent-orderdetail    ; Top Content Slot for My Account Order Details    ; true ;
; ; BottomContent-orderdetail ; Bottom Content Slot for My Account Order Details ; true ;

# ContentSlotForPage
INSERT_UPDATE ContentSlotForPage; $contentCV[unique = true]; uid[unique = true]; position[unique = true]; page(uid, $contentCV)[unique = true][default = 'order']; contentSlot(uid, $contentCV)[unique = true]
; ; BodyContent-orderdetail   ; BodyContent   ; ; BodyContent-orderdetail
; ; SideContent-orderdetail   ; SideContent   ; ; SideContent-orderdetail
; ; TopContent-orderdetail    ; TopContent    ; ; TopContent-orderdetail
; ; BottomContent-orderdetail ; BottomContent ; ; BottomContent-orderdetail

INSERT_UPDATE CMSParagraphComponent; $contentCV[unique = true]; uid[unique = true];
; ; WileyErrorPageTextComponent ;

# Create ContentSlots
INSERT_UPDATE ContentSlot; $contentCV[unique = true]; uid[unique = true]; name; active; cmsComponents(uid, $contentCV);;;
; ; MiddleContent-Errorpage ; Middle Content Slot for Error Page ; true ; WileyErrorPageTextComponent ; ; ;

# Create Content Slots for Page
INSERT_UPDATE ContentSlotForPage; $contentCV[unique = true]; uid[unique = true]; position[unique = true]; page(uid, $contentCV)[unique = true][default = 'notFound']; contentSlot(uid, $contentCV)[unique = true]
; ; MiddleContent-Errorpage ; MiddleContent ; ; MiddleContent-Errorpage

INSERT_UPDATE ComponentTypeGroups2ComponentType; source(code)[unique = true]; target(code)[unique = true]
; wide ; CartSuggestionComponent