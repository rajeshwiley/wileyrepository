package com.wiley.wileyas.initialdata.setup;

import java.util.Set;


/**
 * Data import service
 */
public interface WileyasDataImportService
{

	/**
	 * Import of common data
	 *
	 * @param basePath
	 * 		ImpEx base path
	 * @return True if import was successful
	 */
	boolean importCommonData(final String basePath);

	/**
	 * Import of product data
	 *
	 * @param basePath
	 * 		ImpEx base path
	 * @param productCatalogId
	 * 		Catalog id
	 * @return True if import was successful
	 */
	boolean importProductData(final String basePath, final String productCatalogId);

	/**
	 * Import of content data
	 *
	 * @param basePath
	 * 		ImpEx base path
	 * @param contentCatalogId
	 * 		Catalog id
	 * @return True if import was successful
	 */
	boolean importContentData(final String basePath, final String contentCatalogId);

	/**
	 * Import of content data
	 *
	 * @param basePath
	 * 		ImpEx base path
	 * @param productCatalogIds
	 * 		Product catalog ids
	 * @param contentCatalogIds
	 * 		Content catalog ids  @return True if import was successful
	 */
	boolean importSynchronizationData(final String basePath, final Set<String> productCatalogIds,
			final Set<String> contentCatalogIds);

	/**
	 * Import of store data
	 *
	 * @param basePath
	 * 		ImpEx base path
	 * @param storeUid
	 * 		Store uid
	 * @return True if import was successful
	 */
	boolean importStoreData(final String basePath, final String storeUid);

	/**
	 * Synchronize catalog
	 *
	 * @param catalogId
	 * 		Catalog id
	 * @return True if sync was executed
	 */
	boolean synchronizeCatalog(final String catalogId);

	/**
	 * Import of cart data
	 *
	 * @param basePath
	 * 		ImpEx base path
	 * @return True if import was successful
	 */
	boolean importCartData(final String basePath);

	boolean importPromotions(final String basePath);

	boolean importArchiveProductData(final String basePath, final String catalogId, final String fileName);

	boolean importUsersData(final String basePath);

	boolean importOrdersData();
}
