/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wileyas.initialdata.setup;

import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetup.Process;
import de.hybris.platform.core.initialization.SystemSetup.Type;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;
import de.hybris.platform.core.initialization.SystemSetupParameterMethod;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.util.StopWatch;

import com.wiley.core.wiley.service.WileyJobsDataImportService;
import com.wiley.wileyas.initialdata.constants.WileyasInitialDataConstants;


/**
 * This class provides hooks into the system's initialization and update processes.
 *
 * @see "https://wiki.hybris.com/display/release4/Hooks+for+Initialization+and+Update+Process"
 */
@SystemSetup(extension = WileyasInitialDataConstants.EXTENSIONNAME)
public class InitialDataSystemSetup extends AbstractSystemSetup
{
	private static final String DEACTIVATE_JOBS = "deactivateJobs";
	private static final String IMPORT_CORE_DATA = "importCoreData";
	private static final String IMPORT_SAMPLE_DATA = "importSampleData";
	private static final String IMPORT_PROD_DATA = "importProdData";
	private static final String IMPORT_UAT_DATA = "importUatData";
	private static final String IMPORT_UAT2_DATA = "importUat2Data";
	private static final String IMPORT_PERF_DATA = "importPerfData";
	private static final String IMPORT_USER_ORDER_DATA = "importUserOrderData";
	private static final String ACTIVATE_JOBS = "activateJobs";

	@Resource
	private WileyasDataImportService wileyasDataImportService;

	@Resource
	private WileyJobsDataImportService wileyJobsDataImportService;

	/**
	 * Generates the Dropdown and Multi-select boxes for the project data import
	 */
	@Override
	@SystemSetupParameterMethod
	public List<SystemSetupParameter> getInitializationOptions()
	{
		final List<SystemSetupParameter> params = new ArrayList<>();
		params.add(createBooleanSystemSetupParameter(DEACTIVATE_JOBS, "Deactivate Jobs", true));
		params.add(createBooleanSystemSetupParameter(IMPORT_CORE_DATA, "Import Core Data", true));
		params.add(createBooleanSystemSetupParameter(IMPORT_SAMPLE_DATA, "Import Sample Data", true));
		params.add(createBooleanSystemSetupParameter(IMPORT_UAT_DATA, "Import UAT Data", false));
		params.add(createBooleanSystemSetupParameter(IMPORT_UAT2_DATA, "Import UAT2 Data", false));
		params.add(createBooleanSystemSetupParameter(IMPORT_PERF_DATA, "Import Perf Data", false));
		params.add(createBooleanSystemSetupParameter(IMPORT_PROD_DATA, "Import Prod Data", false));
		params.add(createBooleanSystemSetupParameter(IMPORT_USER_ORDER_DATA, "Import User & Order Data", false));
		params.add(createBooleanSystemSetupParameter(ACTIVATE_JOBS, "Activate Jobs", true));
		return params;
	}

	/**
	 * Implement this method to create data that is used in your project. This method will be called during the system
	 * initialization.
	 *
	 * @param context
	 * 		the context provides the selected parameters and values
	 */
	@SystemSetup(type = Type.PROJECT, process = Process.ALL)
	public void createProjectData(final SystemSetupContext context)
	{
		boolean syncCatalogs = false;
		boolean executeSolrProductIndexing = false;
		final StopWatch stopWatch = new StopWatch();

		if (getBooleanSystemSetupParameter(context, DEACTIVATE_JOBS))
		{
			deactivateJobs(context, stopWatch);
		}

		if (getBooleanSystemSetupParameter(context, IMPORT_CORE_DATA))
		{
			runNextStep(() -> wileyasDataImportService.importCommonData("/wileyasinitialdata/import/coredata"),
					"Import common data in /wileyasinitialdata/import/coredata", context, stopWatch);
			runNextStep(() -> wileyasDataImportService
							.importProductData("/wileyasinitialdata/import/coredata", "asProductCatalog"),
					"Import asProductCatalog data in /wileyasinitialdata/import/coredata", context, stopWatch);
			runNextStep(() -> wileyasDataImportService
							.importContentData("/wileyasinitialdata/import/coredata", "asContentCatalog"),
					"Import asContentCatalog data in /wileyasinitialdata/import/coredata", context, stopWatch);

			runNextStep(() -> wileyasDataImportService.importSynchronizationData("/wileyasinitialdata/import/coredata",
					Collections.singleton("asProductCatalog"), Collections.singleton("asContentCatalog")),
					"Import synchronizationData (asProductCatalog, asContentCatalog) data in /wileyasinitialdata/import/coredata",
					context, stopWatch);

			runNextStep(() -> wileyasDataImportService.importStoreData("/wileyasinitialdata/import/coredata", "asStore"),
					"Import asStore data in /wileyasinitialdata/import/coredata", context, stopWatch);
		}

		if (getBooleanSystemSetupParameter(context, IMPORT_SAMPLE_DATA))
		{
			runNextStep(() -> wileyasDataImportService.importCommonData("/wileyasinitialdata/import/sampledata"),
					"Import common data in /wileyasinitialdata/import/sampledata", context, stopWatch);
			runNextStep(() -> wileyasDataImportService
							.importProductData("/wileyasinitialdata/import/sampledata", "asProductCatalog"),
					"Import asProductCatalog data in /wileyasinitialdata/import/sampledata", context, stopWatch);
			runNextStep(() -> wileyasDataImportService
							.importContentData("/wileyasinitialdata/import/sampledata", "asContentCatalog"),
					"Import asContentCatalog data in /wileyasinitialdata/import/sampledata", context, stopWatch);
			syncCatalogs(context, stopWatch);

			// cart data should be imported after catalog sync as Online version of products is used
			runNextStep(() -> wileyasDataImportService.importCartData("/wileyasinitialdata/import/sampledata"),
					"Import cart data in /wileyasinitialdata/import/sampledata", context, stopWatch);
			runNextStep(() -> wileyasDataImportService.importPromotions("/wileyasinitialdata/import/sampledata"),
					"Import promotions in /wileyasinitialdata/import/sampledata", context, stopWatch);
			executeSolrProductIndexing = true;
		}

		if (getBooleanSystemSetupParameter(context, IMPORT_UAT_DATA))
		{
			runNextStep(() -> wileyasDataImportService.importCommonData("/wileyasinitialdata/import/uatdata"),
					"Import common data in /wileyasinitialdata/import/uatdata", context, stopWatch);
			runNextStep(() -> wileyasDataImportService
							.importProductData("/wileyasinitialdata/import/uatdata", "asProductCatalog"),
					"Import asProductCatalog data in /wileyasinitialdata/import/uatdata", context, stopWatch);
			syncCatalogs = true;
			executeSolrProductIndexing = true;
		}

		if (getBooleanSystemSetupParameter(context, IMPORT_UAT2_DATA))
		{
			runNextStep(() -> wileyasDataImportService.importCommonData("/wileyasinitialdata/import/uat2data"),
					"Import common data in /wileyasinitialdata/import/uat2data", context, stopWatch);
			runNextStep(() -> wileyasDataImportService
							.importProductData("/wileyasinitialdata/import/uat2data", "asProductCatalog"),
					"Import asProductCatalog data in /wileyasinitialdata/import/uat2data", context, stopWatch);
			syncCatalogs = true;
			executeSolrProductIndexing = true;
		}

		if (getBooleanSystemSetupParameter(context, IMPORT_PERF_DATA))
		{
			runNextStep(() -> wileyasDataImportService.importCommonData("/wileyasinitialdata/import/perfdata"),
					"Import common data in /wileyasinitialdata/import/perfdata", context, stopWatch);
			runNextStep(() -> wileyasDataImportService.importArchiveProductData("/wileyasinitialdata/import/perfdata",
					"asProductCatalog", "import_perfdata.zip"),
					"Import asProductCatalog data in  /wileyasinitialdata/import/perfdata", context, stopWatch);
			syncCatalogs = true;
			executeSolrProductIndexing = true;
		}

		if (getBooleanSystemSetupParameter(context, IMPORT_PROD_DATA))
		{
			runNextStep(() -> wileyasDataImportService.importCommonData("/wileyasinitialdata/import/proddata"),
					"Import common data in /wileyasinitialdata/import/proddata", context, stopWatch);
			runNextStep(() -> wileyasDataImportService
							.importProductData("/wileyasinitialdata/import/proddata", "asProductCatalog"),
					"Import asProductCatalog data in /wileyasinitialdata/import/proddata", context, stopWatch);
			syncCatalogs = true;
			executeSolrProductIndexing = true;
		}

		if (getBooleanSystemSetupParameter(context, IMPORT_USER_ORDER_DATA))
		{
			runNextStep(() -> wileyasDataImportService.importUsersData("/wileyasinitialdata/import/importscript/"),
					"Import users data", context, stopWatch);
			runNextStep(() -> wileyasDataImportService
					.importOrdersData(), "Import orders data", context, stopWatch);
		}

		if (syncCatalogs)
		{
			syncCatalogs(context, stopWatch);
		}

		if (executeSolrProductIndexing)
		{
			executeSolrProductIndexing(context, stopWatch);
		}

		if (getBooleanSystemSetupParameter(context, ACTIVATE_JOBS))
		{
			activateJobs(context, stopWatch);
		}
	}

	private void runNextStep(final Runnable runnable, final String stepName, final SystemSetupContext context,
			final StopWatch stopWatch)
	{
		logInfo(context, stepName + " has started");
		stopWatch.start(stepName);
		runnable.run();
		stopWatch.stop();
		logInfo(context, stepName + " done in " + stopWatch.getLastTaskTimeMillis() + " ms");
	}

	private void executeSolrProductIndexing(final SystemSetupContext context, final StopWatch stopWatch)
	{
		runNextStep(() -> wileyJobsDataImportService
						.executeCronJob("full-wileyasIndex-cronJob", false),
				"Execution of full-wileyasIndex-cronJob cron job", context, stopWatch);
	}

	private void syncCatalogs(final SystemSetupContext context, final StopWatch stopWatch)
	{
		runNextStep(() -> wileyasDataImportService.synchronizeCatalog("asProductCatalog"),
				"Synchronization of asProductCatalog", context, stopWatch);
		runNextStep(() -> wileyasDataImportService.synchronizeCatalog("asContentCatalog"),
				"Synchronization of asContentCatalog", context, stopWatch);
	}

	private void deactivateJobs(final SystemSetupContext context, final StopWatch stopWatch)
	{
		runNextStep(() -> wileyJobsDataImportService.deactivateJob("sync asProductCatalog:Staged->Online"),
				"Deactivation of 'sync asProductCatalog:Staged->Online' job", context, stopWatch);
		runNextStep(() -> wileyJobsDataImportService.deactivateCronJob("update-wileyasIndex-cronJob"),
				"Deactivation of update-wileyasIndex-cronJob cron job", context, stopWatch);
		runNextStep(() -> wileyJobsDataImportService.deactivateCronJob("full-wileyasIndex-cronJob"),
				"Deactivation of full-wileyasIndex-cronJob cron job", context, stopWatch);
		runNextStep(() -> wileyJobsDataImportService.deactivateCronJob("asSiteCartRemovalJob"),
				"Deactivation of asSiteCartRemovalJob cron job", context, stopWatch);
	}

	private void activateJobs(final SystemSetupContext context, final StopWatch stopWatch)
	{
		runNextStep(() -> wileyJobsDataImportService.activateJob("sync asProductCatalog:Staged->Online"),
				"Activation of 'sync asProductCatalog:Staged->Online' job", context, stopWatch);
		runNextStep(() -> wileyJobsDataImportService.activateCronJob("update-wileyasIndex-cronJob", true),
				"Activation of update-wileyasIndex-cronJob cron job", context, stopWatch);
		runNextStep(() -> wileyJobsDataImportService.activateCronJob("full-wileyasIndex-cronJob", true),
				"Activation of full-wileyasIndex-cronJob cron job", context, stopWatch);
		runNextStep(() -> wileyJobsDataImportService.activateCronJob("asSiteCartRemovalJob", true),
				"Activation of asSiteCartRemovalJob cron job", context, stopWatch);
	}
}
