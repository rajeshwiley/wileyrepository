/*
 * [y] hybris Platform
 * 
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information of SAP 
 * Hybris ("Confidential Information"). You shall not disclose such 
 * Confidential Information and shall use it only in accordance with the 
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.wiley.wileyas.initialdata.constants;

/**
 * Global class for all Wileyasinitialdata constants. You can add global constants for your extension into this class.
 */
public final class WileyasInitialDataConstants
{
	public static final String EXTENSIONNAME = "wileyasinitialdata";

	private WileyasInitialDataConstants()
	{
		//empty to avoid instantiating this constant class
	}

}
