package com.wiley.wileyas.initialdata.setup.impl;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.commerceservices.setup.SetupImpexService;
import de.hybris.platform.commerceservices.setup.SetupSolrIndexerService;
import de.hybris.platform.commerceservices.setup.SetupSyncJobService;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.impex.model.ImpExMediaModel;
import de.hybris.platform.servicelayer.cronjob.CronJobService;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.impex.ImportConfig;
import de.hybris.platform.servicelayer.impex.ImportResult;
import de.hybris.platform.servicelayer.impex.impl.MediaBasedImpExResource;
import de.hybris.platform.servicelayer.impex.impl.StreamBasedImpExResource;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.site.BaseSiteService;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.Set;
import java.util.UUID;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.apache.commons.lang.CharEncoding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.impex.WileyImportService;
import com.wiley.core.model.WileyOrdersImportCronJobModel;
import com.wiley.wileyas.initialdata.setup.WileyasDataImportService;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;
import static de.hybris.platform.util.CSVConstants.DEFAULT_ENCODING;


public class WileyasDataImportServiceImpl implements WileyasDataImportService
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyasDataImportServiceImpl.class);
	private static final boolean FAIL_IF_MISSING = false;
	private static final String IMPORT_EMPLOYEES = "importEmployees";
	private static final String IMPORT_CUSTOMERS = "importCustomers";

	@Resource
	private SetupImpexService setupImpexService;

	@Resource
	private SetupSyncJobService setupSyncJobService;

	@Resource
	private SetupSolrIndexerService setupSolrIndexerService;

	@Resource
	private WileyImportService wileyImportService;

	@Resource
	private MediaService mediaService;

	@Resource
	private ModelService modelService;

	@Value("${wiley.users.import.dir}")
	private String usersImportDir;

	@Value("${wiley.orders.import.dir}")
	private String ordersImportDir;

	@Resource
	private CatalogVersionService catalogVersionService;

	@Resource
	private BaseSiteService baseSiteService;

	@Resource
	private CronJobService cronJobService;

	@Override
	public boolean importCommonData(final String basePath)
	{
		setupImpexService.importImpexFile(basePath + "/common/countries.impex", FAIL_IF_MISSING);
		setupImpexService.importImpexFile(basePath + "/common/common-data.impex", FAIL_IF_MISSING);
		setupImpexService.importImpexFile(basePath + "/common/promotions-data.impex", FAIL_IF_MISSING);
		setupImpexService.importImpexFile(basePath + "/common/user-groups.impex", FAIL_IF_MISSING);
		setupImpexService.importImpexFile(basePath + "/common/users.impex", FAIL_IF_MISSING);
		setupImpexService.importImpexFile(basePath + "/common/projectdata-templates.impex", FAIL_IF_MISSING);
		setupImpexService.importImpexFile(basePath + "/common/tnsMerchantId.impex", FAIL_IF_MISSING);
		return true;
	}

	@Override
	public boolean importProductData(final String basePath, final String productCatalogId)
	{
		final String catalogBasePath = basePath + "/productCatalogs/" + productCatalogId;
		setupImpexService.importImpexFile(catalogBasePath + "/catalog.impex", FAIL_IF_MISSING);
		setupImpexService.importImpexFile(catalogBasePath + "/products.impex", FAIL_IF_MISSING);
		setupImpexService.importImpexFile(catalogBasePath + "/prices.impex", FAIL_IF_MISSING);
		return true;
	}

	@Override
	public boolean importContentData(final String basePath, final String contentCatalogId)
	{
		final String catalogBasePath = basePath + "/contentCatalogs/" + contentCatalogId;
		setupImpexService.importImpexFile(catalogBasePath + "/catalog.impex", FAIL_IF_MISSING);
		setupImpexService.importImpexFile(catalogBasePath + "/cms-content.impex", FAIL_IF_MISSING);
		setupImpexService.importImpexFile(catalogBasePath + "/email-content.impex", FAIL_IF_MISSING);
		return true;
	}

	@Override
	public boolean importSynchronizationData(final String basePath, final Set<String> productCatalogIds,
			final Set<String> contentCatalogIds)
	{
		productCatalogIds.forEach(productCatalogId -> setupSyncJobService.createProductCatalogSyncJob(productCatalogId));
		contentCatalogIds.forEach(contentCatalogId -> setupSyncJobService.createContentCatalogSyncJob(contentCatalogId));

		setupImpexService.importImpexFile(basePath + "/common/sync.impex", FAIL_IF_MISSING);
		return true;
	}

	@Override
	public boolean importStoreData(final String basePath, final String storeUid)
	{
		final String storeBasePath = basePath + "/stores/" + storeUid;
		setupImpexService.importImpexFile(storeBasePath + "/store.impex", FAIL_IF_MISSING);
		setupImpexService.importImpexFile(storeBasePath + "/site.impex", FAIL_IF_MISSING);
		setupImpexService.importImpexFile(storeBasePath + "/jobs.impex", FAIL_IF_MISSING);
		setupImpexService.importImpexFile(storeBasePath + "/solr.impex", FAIL_IF_MISSING);
		setupSolrIndexerService.createSolrIndexerCronJobs(storeUid + "Index");
		setupImpexService.importImpexFile(storeBasePath + "/solrtrigger.impex", FAIL_IF_MISSING);
		return true;
	}

	@Override
	public boolean synchronizeCatalog(final String catalogId)
	{
		final PerformResult syncCronJobResult = setupSyncJobService.executeCatalogSyncJob(catalogId);
		return !isSyncRerunNeeded(syncCronJobResult);
	}

	/**
	 * Determines if the sync job needs to be rerun
	 *
	 * @param syncCronJobResult
	 * 		the sync job result
	 * @return true is the sync job is <tt>null</tt> or unsuccessful
	 */
	public boolean isSyncRerunNeeded(final PerformResult syncCronJobResult)
	{
		return syncCronJobResult == null
				|| (CronJobStatus.FINISHED.equals(syncCronJobResult.getStatus())
				&& !CronJobResult.SUCCESS.equals(syncCronJobResult.getResult()));
	}

	@Override
	public boolean importCartData(final String basePath)
	{
		setupImpexService.importImpexFile(basePath + "/common/cart-data.impex", FAIL_IF_MISSING);
		return true;
	}

	@Override
	public boolean importPromotions(final String basePath)
	{
		setupImpexService.importImpexFile(basePath + "/common/promotions.impex", FAIL_IF_MISSING);
		return true;
	}

	public boolean importArchiveProductData(@Nonnull final String basePath,
			@Nonnull final String catalogId, @Nonnull final String fileName)
	{
		validateParameterNotNullStandardMessage("basePath", basePath);
		validateParameterNotNullStandardMessage("catalogId", catalogId);
		validateParameterNotNullStandardMessage("fileName", fileName);

		final String dataArchivePath = basePath + "/productCatalogs/" + catalogId + "/" + fileName;
		LOG.info("Starting import of archive files. Data file {}", dataArchivePath);

		final ImportConfig importConfig = new ImportConfig();
		final InputStream dataArchive = this.getClass().getResourceAsStream(dataArchivePath);
		if (dataArchive == null)
		{
			LOG.info("Archive import skipped due to file absence: data archive exists={}", false);
			return false;
		}
		importConfig.setScript(new StreamBasedImpExResource(dataArchive, DEFAULT_ENCODING));
		importConfig.setRemoveOnSuccess(false);

		final ImportResult importResult = wileyImportService.importData(importConfig);

		final String importCronJobCode = importResult.getCronJob().getCode();
		if (importResult.isSuccessful())
		{
			LOG.info("Successfully finished {} import, import cron job code={}", dataArchivePath,
					importCronJobCode);
			return true;
		}
		else
		{
			LOG.error("Finished import with error, files are {}, see import cronjob with code={}", dataArchivePath,
					importCronJobCode);
			return false;
		}
	}

	public boolean importUsersData(final String basePath)
	{
		File csAgentsFile = new File(usersImportDir + "/employees.csv");
		File customersFile = new File(usersImportDir + "/customers.csv");
		if (csAgentsFile.exists() && customersFile.exists())
		{
			setupImpexService.importImpexFile(basePath + "prepareMedias.impex", FAIL_IF_MISSING);
			return importUsersFile(basePath, IMPORT_EMPLOYEES) && importUsersFile(basePath, IMPORT_CUSTOMERS);
		}
		return true;
	}

	private boolean importUsersFile(final String basePath, final String mediaCode)
	{
		final StreamBasedImpExResource resource = new StreamBasedImpExResource(
				this.getClass().getResourceAsStream(basePath + mediaCode + ".impex"), CharEncoding.UTF_8);
		final ImportConfig importConfig = new ImportConfig();
		importConfig.setEnableCodeExecution(true);
		importConfig.setLegacyMode(false);
		importConfig.setScript(resource);
		MediaBasedImpExResource importFileResource = new MediaBasedImpExResource(
				(ImpExMediaModel) mediaService.getMedia(mediaCode));
		importConfig.setReferencedData(Collections.singletonList(importFileResource));
		ImportResult importResult = wileyImportService.importData(importConfig);
		if (importResult.isSuccessful())
		{
			LOG.info("Successfully finished users data import.");
			return true;
		}
		else
		{
			LOG.error("Finished users data import with error.");
			return false;
		}
	}

	@Override
	public boolean importOrdersData()
	{
		File ordersFile = new File(ordersImportDir + "/orders.json");
		if (ordersFile.exists())
		{
			MediaModel media = new MediaModel();
			media.setFolder(mediaService.getFolder("files"));
			media.setMime("application/octet-stream");
			media.setCode(UUID.randomUUID().toString());
			modelService.save(media);
			try (FileInputStream fis = new FileInputStream(ordersFile))
			{
				mediaService.setStreamForMedia(media, fis);
			}
			catch (FileNotFoundException e)
			{
				return true;
			}
			catch (IOException e)
			{
				LOG.error("Unable to import orders data", e);
				return false;
			}

			WileyOrdersImportCronJobModel wileyOrdersImportCronJobModel = getWileyOrdersImportCronJob(media);
			cronJobService.performCronJob(wileyOrdersImportCronJobModel, true);
			return CronJobResult.SUCCESS.equals(wileyOrdersImportCronJobModel.getResult());
		}
		return true;
	}

	private WileyOrdersImportCronJobModel getWileyOrdersImportCronJob(final MediaModel mediaModel)
	{
		WileyOrdersImportCronJobModel wileyOrdersImportCronJobModel = modelService.create(WileyOrdersImportCronJobModel.class);
		wileyOrdersImportCronJobModel.setOrders(mediaModel);
		wileyOrdersImportCronJobModel.setJob(cronJobService.getJob("wileyOrdersImportCronJobPerformable"));
		wileyOrdersImportCronJobModel.setBaseSite(baseSiteService.getBaseSiteForUID(WileyCoreConstants.AS_SITE_ID));
		wileyOrdersImportCronJobModel.setCatalogVersion(
				catalogVersionService.getCatalogVersion("asProductCatalog", WileyCoreConstants.STAGED_CATALOG_VERSION));
		wileyOrdersImportCronJobModel.setUserAutoCreationStrategy(WileyCoreConstants.ALM_AUTO_CREATION_STRATEGY);
		modelService.save(wileyOrdersImportCronJobModel);
		return wileyOrdersImportCronJobModel;
	}
}
