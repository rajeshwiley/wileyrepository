package com.wiley.facades.welags.order.populator;

import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import javax.annotation.Resource;

import com.wiley.core.product.WileyProductEditionFormatService;


public class WelAgsIsDigitalCartPopulator implements Populator<AbstractOrderModel, CartData>
{
	@Resource
	private WileyProductEditionFormatService wileyProductEditionFormatService;

	@Override
	public void populate(final AbstractOrderModel orderModel, final CartData cartData)
			throws ConversionException
	{
		ServicesUtil.validateParameterNotNull(orderModel, "OderModel mustn't be null");
		ServicesUtil.validateParameterNotNull(cartData, "CartData mustn't be null");

		cartData.setDigitalCart(wileyProductEditionFormatService.isDigitalCart(orderModel));
	}
}
