package com.wiley.facades.order.converters.populator;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.commercefacades.order.data.WileyPaymentInfoData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

public class WileyPaymentInfoPopulator implements Populator<PaymentInfoModel, WileyPaymentInfoData>
{
	@Override
	public void populate(final PaymentInfoModel source, final WileyPaymentInfoData target) throws ConversionException
	{
		validateParameterNotNullStandardMessage("PaymentInfoModel", source);
		validateParameterNotNullStandardMessage("WileyPaymentInfoData", target);

		if (source instanceof CreditCardPaymentInfoModel)
		{
			final CreditCardPaymentInfoModel cardPaymentInfoModel = (CreditCardPaymentInfoModel) source;
			target.setCreditCardNumber(cardPaymentInfoModel.getNumber());
			target.setCreditCardType(cardPaymentInfoModel.getType().name());
		}
	}
}
