package com.wiley.facades.populators;

import com.wiley.core.price.WileyCommercePriceService;
import de.hybris.platform.commercefacades.product.converters.populator.ProductPricePopulator;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;

public class WileyProductPricePopulator extends ProductPricePopulator
{
	@Autowired
	private WileyCommercePriceService wileyCommercePriceService;

	@Override
	public void populate(final ProductModel productModel, final ProductData productData) throws ConversionException
	{
		PriceData priceData = getProductPriceData(productModel, false);
		if (priceData != null)
		{
			productData.setPrice(priceData);
		} else
		{
			productData.setPurchasable(Boolean.FALSE);

		}
	}

	protected PriceData getProductPriceData(final ProductModel productModel, final boolean ignoreSetVariant)
	{
		final PriceDataType priceType;

		PriceInformation info = wileyCommercePriceService.getStartingAtPrice(productModel, ignoreSetVariant);

		if (info != null)
		{
			priceType = PriceDataType.FROM;
		} else
		{
			priceType = PriceDataType.BUY;
			info = getCommercePriceService().getFromPriceForProduct(productModel);
		}

		return info == null ? null : createPriceData(priceType, info);
	}

	protected PriceData createPriceData(final PriceDataType priceType, final PriceInformation info)
	{
		return getPriceDataFactory().create(priceType,
				BigDecimal.valueOf(info.getPriceValue().getValue()), info.getPriceValue().getCurrencyIso());
	}

	protected WileyCommercePriceService getWileyCommercePriceService()
	{
		return wileyCommercePriceService;
	}
}
