package com.wiley.facades.wileyb2c.product;

import com.wiley.facades.wileyb2c.product.data.WileyPlusCourseInfoData;
import com.wiley.facades.wileyplus.cart.activation.dto.CartActivationRequestDto;


public interface WileyCourseProductFacade
{
	WileyPlusCourseInfoData convertToWileyInfoData(CartActivationRequestDto cartActivationRequestDto);

}
