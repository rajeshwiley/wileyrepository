package com.wiley.facades.wileyb2c.search.solrfacetsearch.converters.populator;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commercefacades.search.solrfacetsearch.converters.populator.SolrSearchStatePopulator;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.commerceservices.url.UrlResolver;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.annotation.Resource;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;


public class Wileyb2cSolrSearchStatePopulator extends SolrSearchStatePopulator
{
	private static final Logger LOG = Logger.getLogger(Wileyb2cSolrSearchStatePopulator.class);
	private static final String PRODUCT_LIST_PAGE = "productListPage";

	@Resource(name = "commerceCategoryService")
	private CommerceCategoryService commerceCategoryService;
	@Resource(name = "wileyb2cCustomPlpLinkUrlResolver")
	private UrlResolver<CategoryModel> wileyb2cCustomPlpLinkUrlResolver;

	@Override
	public void populate(final SolrSearchQueryData source, final SearchStateData target)
	{
		target.setQuery(getSearchQueryConverter().convert(source));

		if (source.getCategoryCode() != null)
		{
			if (PRODUCT_LIST_PAGE.equals(source.getPageType()))
			{
				populateProductListPageUrl(source, target);
			}
			else
			{
				populateCategorySearchUrl(source, target);
			}
		}
		else
		{
			populateFreeTextSearchUrl(source, target);
		}
	}

	private void populateProductListPageUrl(final SolrSearchQueryData source, final SearchStateData target)
	{
		target.setUrl(getProductListPageUrl(source) + buildUrlQueryString(source, target));
	}

	private String getProductListPageUrl(final SolrSearchQueryData source)
	{
		final CategoryModel category = commerceCategoryService.getCategoryForCode(source.getCategoryCode());
		return wileyb2cCustomPlpLinkUrlResolver.resolve(category);
	}

	@Override
	protected String buildUrlQueryString(final SolrSearchQueryData source, final SearchStateData target)
	{
		final String searchQueryParam = target.getQuery().getValue();
		if (StringUtils.isNotBlank(searchQueryParam))
		{
			try
			{
				return "?pq=" + URLEncoder.encode(searchQueryParam, "UTF-8");
			}
			catch (final UnsupportedEncodingException e)
			{
				LOG.error("Unsupported encoding (UTF-8). Fallback to html escaping.", e);
				return "?pq=" + StringEscapeUtils.escapeHtml(searchQueryParam);
			}
		}
		return "";
	}
}
