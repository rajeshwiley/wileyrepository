package com.wiley.facades.user.converters.populator;

import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.util.ServicesUtil;


/**
 * Created by Maksim_Kozich on 19.03.2018.
 */
public class WileyCountryTaxExemptionDetailsPopulator implements Populator<CountryModel, CountryData>
{
	@Override
	public void populate(final CountryModel source, final CountryData target) throws ConversionException
	{
		ServicesUtil.validateParameterNotNullStandardMessage("cartModel", source);
		ServicesUtil.validateParameterNotNullStandardMessage("cartData", target);

		target.setApplicableTaxExemption(source.getApplicableTaxExemption());

	}
}
