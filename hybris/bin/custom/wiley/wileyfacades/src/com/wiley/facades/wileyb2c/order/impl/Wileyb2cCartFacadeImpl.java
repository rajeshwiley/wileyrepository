package com.wiley.facades.wileyb2c.order.impl;

import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.impl.DefaultCartFacade;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.subscriptionservices.model.SubscriptionTermModel;

import java.util.List;
import java.util.Optional;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.wiley.core.model.SchoolModel;
import com.wiley.core.model.WileySubscriptionModel;
import com.wiley.core.product.WileyProductService;
import com.wiley.core.product.WileySubscriptionTermService;
import com.wiley.core.subscription.services.WileySubscriptionService;
import com.wiley.core.wileyb2c.order.Wileyb2cCommerceCartService;
import com.wiley.core.wileycom.customer.service.WileycomCustomerAccountService;
import com.wiley.facades.product.util.ProductInfo;
import com.wiley.facades.wileyb2c.order.Wileyb2cCartFacade;
import com.wiley.facades.wileyplus.cart.activation.dto.CartActivationRequestDto;


public class Wileyb2cCartFacadeImpl extends DefaultCartFacade implements Wileyb2cCartFacade
{
	@Resource
	private WileycomCustomerAccountService wileycomCustomerAccountService;

	private WileyProductService wileyProductService;

	@Resource
	private WileySubscriptionTermService wileySubscriptionTermService;

	@Resource
	private WileySubscriptionService wileySubscriptionService;

	@Resource
	private Populator<CartActivationRequestDto, AbstractOrderEntryModel> wileyb2cOrderEntryCoursePopulator;

	@Resource
	private Populator<ProductModel, AbstractOrderEntryModel> wileyb2cSubscriptionProductEntryPopulator;

	private Wileyb2cCommerceCartService wileyb2cCommerceCartService;

	/**
	 * Add wiley course to cart
	 *
	 * @param productCode
	 * 		the product code
	 * @param qty
	 * 		the quantity
	 * @param purchaseType
	 * 		the type of purchase
	 * @param activationRequestDto
	 * 		the course
	 * @return the cart modification data
	 * @throws CommerceCartModificationException
	 * 		the commerce cart modification exception
	 */
	@Override
	@Transactional
	public CartModificationData addCourseToCart(@Nonnull final String productCode, final long qty,
			@Nonnull final String purchaseType, @Nonnull final CartActivationRequestDto activationRequestDto)
			throws CommerceCartModificationException
	{
		Assert.notNull(productCode);
		Assert.notNull(purchaseType);
		Assert.notNull(activationRequestDto);

		final ProductModel product = getProductService().getProductForCode(productCode);
		final CartModel cartModel = getCartService().getSessionCart();
		final CommerceCartParameter parameter = new CommerceCartParameter();
		parameter.setEnableHooks(true);
		parameter.setCart(cartModel);
		parameter.setQuantity(qty);
		parameter.setProduct(product);
		parameter.setUnit(product.getUnit());
		parameter.setCreateNewEntry(false);

		final CommerceCartModification modification = getCommerceCartService().addToCart(parameter);

		AbstractOrderEntryModel entry = modification.getEntry();
		if (entry != null)
		{
			wileyb2cOrderEntryCoursePopulator.populate(activationRequestDto, entry);
			entry.getProductSummary().setSummary(purchaseType);
			getModelService().save(entry);
		}
		return getCartModificationConverter().convert(modification);
	}

	@Override
	public boolean isEducationalCart()
	{
		boolean result = false;
		final CartService cartService = getCartService();
		if (cartService.hasSessionCart())
		{
			final List<AbstractOrderEntryModel> entries = cartService.getSessionCart().getEntries();
			result = entries.stream().anyMatch(entry -> entry.getProduct().getTextbook());
		}
		return result;
	}

	@Override
	public void persistSchoolInformationInCurrentOrder(@Nonnull final String schoolUid)
	{
		Optional<SchoolModel> schoolModel = wileycomCustomerAccountService.getSchoolByCode(schoolUid);

		if (schoolModel.isPresent())
		{
			AbstractOrderModel orderModel = getCartService().getSessionCart();
			orderModel.setSchool(schoolModel.get());
			getModelService().save(orderModel);
		}
	}

	@Nonnull
	@Override
	public CartModificationData addToCartByIsbn(@Nonnull final String isbn, @Nonnull final ProductInfo productInfo,
			@Nonnull final String additionalInfo)
			throws CommerceCartModificationException
	{
		final ProductModel product = wileyProductService.getProductForIsbn(isbn);
		final String termCode = productInfo.getSubscriptionTermCode();
		final Optional<SubscriptionTermModel> subscriptionTerm = wileySubscriptionTermService.findSubscriptionTerm(product,
				termCode);

		return addToCart(product, productInfo.getQuantity(), subscriptionTerm, additionalInfo);
	}

	@Nonnull
	@Override
	public CartModificationData addToCartBySubscriptionCodeAndTermId(@Nonnull final String wileySubscriptionCode,
			@Nonnull final String subscriptionTermId)
			throws CommerceCartModificationException
	{
		final WileySubscriptionModel subscriptionModel = wileySubscriptionService.getSubscriptionByCode(wileySubscriptionCode);
		final ProductModel product = subscriptionModel.getProduct();
		final Optional<SubscriptionTermModel> subscriptionTerm = wileySubscriptionTermService.findSubscriptionTerm(product,
				subscriptionTermId);

		return addToCart(product, 1, subscriptionTerm, StringUtils.EMPTY);
	}

	@Override
	public void saveContinueUrl(@Nullable final String continueUrl)
	{
		final CartService cartService = getCartService();
		if (cartService.hasSessionCart())
		{
			wileyb2cCommerceCartService.saveContinueUrlForCart(continueUrl, cartService.getSessionCart());
		}
	}

	@Override
	public String getContinueUrl()
	{
		if (getCartService().hasSessionCart())
		{
			return getCartService().getSessionCart().getCartPageContinueUrl();
		}
		else
		{
			return null;
		}
	}

	@Override
	public void setProductService(final ProductService productService)
	{
		super.setProductService(productService);
		this.wileyProductService = (WileyProductService) productService;
	}

	private CartModificationData addToCart(final ProductModel productModel, final long quantity,
			final Optional<SubscriptionTermModel> subscriptionTerm, final String additionalInfo)
			throws CommerceCartModificationException
	{
		ServicesUtil.validateParameterNotNullStandardMessage("productModel", productModel);
		ServicesUtil.validateParameterNotNullStandardMessage("additionalInfo", additionalInfo);

		final CartModel cartModel = getCartService().getSessionCart();

		final CommerceCartParameter parameter = new CommerceCartParameter();
		parameter.setEnableHooks(true);
		parameter.setCart(cartModel);
		parameter.setQuantity(quantity);
		parameter.setProduct(productModel);
		parameter.setUnit(productModel.getUnit());
		parameter.setCreateNewEntry(false);
		parameter.setAdditionalInfo(additionalInfo);
		if (subscriptionTerm.isPresent())
		{
			parameter.setSubscriptionTerm(subscriptionTerm.get());
		}

		final CommerceCartModification modification = getCommerceCartService().addToCart(parameter);
		AbstractOrderEntryModel entry = modification.getEntry();
		if (entry != null && wileySubscriptionTermService.isSubscriptionProduct(productModel))
		{
			wileyb2cSubscriptionProductEntryPopulator.populate(productModel, entry);
			getModelService().save(entry);
		}

		return getCartModificationConverter().convert(modification);
	}

	@Override
	public void setCommerceCartService(final CommerceCartService commerceCartService)
	{
		super.setCommerceCartService(commerceCartService);
		this.wileyb2cCommerceCartService = (Wileyb2cCommerceCartService) commerceCartService;
	}
}
