package com.wiley.facades.wileyb2c.populator;

import de.hybris.platform.commercefacades.search.data.SearchQueryData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.converters.Populator;

import javax.validation.constraints.NotNull;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


/**
 * Created by Maksim_Kozich on 20.03.17.
 */
public class Wileyb2cSolrSearchQueryNoTermsEncoderPopulator implements Populator<SolrSearchQueryData, SearchQueryData>
{
	private Populator<SolrSearchQueryData, SearchQueryData> solrSearchQueryEncoderPopulator;

	protected Populator<SolrSearchQueryData, SearchQueryData> getSolrSearchQueryEncoderPopulator()
	{
		return solrSearchQueryEncoderPopulator;
	}

	@Required
	public void setSolrSearchQueryEncoderPopulator(
			final Populator<SolrSearchQueryData, SearchQueryData> solrSearchQueryEncoderPopulator)
	{
		this.solrSearchQueryEncoderPopulator = solrSearchQueryEncoderPopulator;
	}

	@Override
	public void populate(@NotNull final SolrSearchQueryData source, @NotNull final SearchQueryData target)
	{
		validateParameterNotNullStandardMessage("source", source);
		validateParameterNotNullStandardMessage("target", target);

		// Build the new query data without terms
		final SolrSearchQueryData solrSearchQueryNoTermsData = cloneSearchQueryData(source);
		if (CollectionUtils.isNotEmpty(solrSearchQueryNoTermsData.getFilterTerms()))
		{
			solrSearchQueryNoTermsData.getFilterTerms().clear();
		}

		solrSearchQueryEncoderPopulator.populate(solrSearchQueryNoTermsData, target);
	}

	/**
	 * Shallow clone of the source SearchQueryData
	 *
	 * @param source
	 * 		the instance to clone
	 * @return the shallow clone
	 */
	protected SolrSearchQueryData cloneSearchQueryData(final SolrSearchQueryData source)
	{
		final SolrSearchQueryData target = createSearchQueryData();
		target.setFreeTextSearch(source.getFreeTextSearch());
		target.setCategoryCode(source.getCategoryCode());
		target.setSort(source.getSort());
		target.setFilterTerms(source.getFilterTerms());
		return target;
	}

	protected SolrSearchQueryData createSearchQueryData()
	{
		return new SolrSearchQueryData();
	}
}
