package com.wiley.facades.wileycom.user.impl;

import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.springframework.transaction.annotation.Transactional;

import com.wiley.core.exceptions.ExternalSystemException;
import com.wiley.core.wileycom.customer.WileycomExternalAddressService;
import com.wiley.facades.user.impl.WileyUserFacadeImpl;
import com.wiley.facades.wileycom.user.WileycomUserFacade;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


/**
 * Facade contains Wiley B2B and B2C sites specific methods
 */
public class WileycomUserFacadeImpl extends WileyUserFacadeImpl implements WileycomUserFacade
{

	@Resource
	WileycomExternalAddressService wileycomExternalAddressService;

	@Override
	public void addAddress(final AddressData addressData)
	{
		validateParameterNotNullStandardMessage("addressData", addressData);

		final CustomerModel currentCustomer = getCurrentUserForCheckout();

		final boolean makeThisAddressTheDefault = addressData.isDefaultAddress()
				|| (currentCustomer.getDefaultShipmentAddress() == null && addressData.isVisibleInAddressBook());

		// Create the new address model
		final AddressModel newAddress = getModelService().create(AddressModel.class);
		getAddressReversePopulator().populate(addressData, newAddress);

		try
		{
			//send new address to external system and save to db
			wileycomExternalAddressService.addShippingAddress(currentCustomer, newAddress, makeThisAddressTheDefault);
		}
		catch (ExternalSystemException | IllegalArgumentException ex)
		{
			getModelService().detach(newAddress);
			throw ex;
		}

		// Update the address ID in the newly created address
		addressData.setId(newAddress.getPk().toString());
	}

	/**
	 * Add address to database even in case of external system exception
	 *
	 * @param addressData
	 * 		the address data
	 */
	@Override
	public void addAddressFromCheckout(@Nonnull final AddressData addressData)
	{
		validateParameterNotNullStandardMessage("addressData", addressData);

		final CustomerModel currentCustomer = getCurrentUserForCheckout();

		final boolean makeThisAddressTheDefault = addressData.isDefaultAddress()
				|| (currentCustomer.getDefaultShipmentAddress() == null && addressData.isVisibleInAddressBook());

		// Create the new address model
		final AddressModel newAddress = getModelService().create(AddressModel.class);
		getAddressReversePopulator().populate(addressData, newAddress);

		wileycomExternalAddressService.addAddressFromCheckout(currentCustomer, newAddress, makeThisAddressTheDefault);

		// Update the address ID in the newly created address
		addressData.setId(newAddress.getPk().toString());

	}

	@Override
	public void removeAddress(final AddressData addressData)
	{
		validateParameterNotNullStandardMessage("addressData", addressData);
		final CustomerModel currentCustomer = getCurrentUserForCheckout();
		for (final AddressModel addressModel : getCustomerAccountService().getAddressBookEntries(currentCustomer))
		{
			if (addressData.getId().equals(addressModel.getPk().getLongValueAsString()))
			{
				wileycomExternalAddressService.deleteAddress(currentCustomer, addressModel);
				getCustomerAccountService().deleteAddressEntry(currentCustomer, addressModel);
				break;
			}
		}
	}

	@Override
	public void unlinkCCPaymentInfo(final String id)
	{
		validateParameterNotNullStandardMessage("id", id);
		final CustomerModel currentCustomer = (CustomerModel) getUserService().getCurrentUser();
		for (final CreditCardPaymentInfoModel creditCardPaymentInfo : getCustomerAccountService().getCreditCardPaymentInfos(
				currentCustomer, false))
		{
			if (creditCardPaymentInfo.getPk().toString().equals(id))
			{
				wileycomExternalAddressService.deleteAddress(currentCustomer, creditCardPaymentInfo.getBillingAddress());
				getCustomerAccountService().unlinkCCPaymentInfo(currentCustomer, creditCardPaymentInfo);
				break;
			}
		}
		updateDefaultPaymentInfo(currentCustomer);
	}

	@Override
	public void editAddressFromCheckout(@Nonnull final AddressData addressData)
	{
		validateParameterNotNullStandardMessage("addressData", addressData);

		final CustomerModel currentCustomer = getCurrentUserForCheckout();
		final AddressModel addressModel = getCustomerAccountService().getAddressForCode(currentCustomer, addressData.getId());
		addressModel.setRegion(null);
		getAddressReversePopulator().populate(addressData, addressModel);

		wileycomExternalAddressService.updateShippingAddress(currentCustomer, addressModel, addressData.isDefaultAddress());
	}

	@Override
	public void editAddress(final AddressData addressData)
	{
		validateParameterNotNullStandardMessage("addressData", addressData);
		final CustomerModel currentCustomer = getCurrentUserForCheckout();
		final AddressModel addressModel = getCustomerAccountService().getAddressForCode(currentCustomer, addressData.getId());
		addressModel.setRegion(null);
		getAddressReversePopulator().populate(addressData, addressModel);
		try
		{
			//send new address to external system and save to db
			wileycomExternalAddressService.updateShippingAddress(currentCustomer, addressModel, addressData.isDefaultAddress());
		}
		catch (ExternalSystemException ese)
		{
			getModelService().detach(addressModel);
			throw ese;
		}
	}

	@Override
	@Transactional
	public void updateCCPaymentInfo(@Nonnull final CCPaymentInfoData paymentInfo)
	{
		validateParameterNotNullStandardMessage("paymentInfo", paymentInfo);
		validateParameterNotNullStandardMessage("paymentInfoID", paymentInfo.getId());

		final CustomerModel currentCustomer = (CustomerModel) getUserService().getCurrentUser();
		final CreditCardPaymentInfoModel paymentInfoModel = getCustomerAccountService().getCreditCardPaymentInfoForCode(
				currentCustomer, paymentInfo.getId());

		getCardPaymentInfoReversePopulator().populate(paymentInfo, paymentInfoModel);
		getModelService().save(paymentInfoModel);

		if (paymentInfoModel.getBillingAddress() != null)
		{
			final AddressModel billingAddress = paymentInfoModel.getBillingAddress();

			getModelService().save(billingAddress);
			wileycomExternalAddressService.updateBillingAddress(currentCustomer, billingAddress, false);
			getModelService().refresh(paymentInfoModel);
		}
	}
}
