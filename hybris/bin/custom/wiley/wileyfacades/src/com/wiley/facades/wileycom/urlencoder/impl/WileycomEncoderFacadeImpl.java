package com.wiley.facades.wileycom.urlencoder.impl;

import de.hybris.platform.acceleratorfacades.urlencoder.data.UrlEncoderData;
import de.hybris.platform.acceleratorfacades.urlencoder.impl.DefaultUrlEncoderFacade;
import de.hybris.platform.acceleratorservices.urlencoder.attributes.UrlEncodingAttributeManager;

import java.net.URISyntaxException;
import java.util.List;

import javax.annotation.Nonnull;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.utils.URIBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;
import com.wiley.facades.wileycom.urlencoder.WileycomEncoderFacade;

import static de.hybris.platform.acceleratorservices.constants.AcceleratorServicesConstants.LANGUAGE_ENCODING;


public class WileycomEncoderFacadeImpl extends DefaultUrlEncoderFacade implements WileycomEncoderFacade
{
	private static final Logger LOG = LoggerFactory.getLogger(WileycomEncoderFacadeImpl.class);
	private static final String SLASH_SEPARATOR = "/";

	/**
	 * {@inheritDoc}
	 */
	@Nonnull
	@Override
	public String substituteValue(@Nonnull final String uri, @Nonnull final String originalContextPath,
			@Nonnull final String encodingAttributeName, @Nonnull final String value) throws URISyntaxException
	{
		Preconditions.checkNotNull(uri, "uri parameter should not be null");
		Preconditions.checkNotNull(originalContextPath, "originalContextPath parameter should not be null");
		Preconditions.checkNotNull(encodingAttributeName, "encodingAttributeName parameter should not be null");
		Preconditions.checkNotNull(value, "value parameter should not be null");
		LOG.debug("[{}] url with [{}] context path should be updated with [{}] value for attribute [{}]", uri,
				originalContextPath, value, encodingAttributeName);

		final URIBuilder uriBuilder = new URIBuilder(uri);
		String path = uriBuilder.getPath();
		if (!SLASH_SEPARATOR.equals(originalContextPath)) //UrlEncoderFilter set context to '/' if context path is empty
		{
			path = StringUtils.remove(path, originalContextPath);
		}

		final String[] splitUri = path.split(SLASH_SEPARATOR);
		LOG.debug("Splitting uri to [{}]", splitUri);

		int splitUrlCounter = 1; //empty String prior leading slash has index 0
		for (final UrlEncoderData urlEncoderData : getCurrentUrlEncodingData())
		{
			String name = urlEncoderData.getAttributeName();
			if (splitUrlCounter < splitUri.length && encodingAttributeName.equals(name))
			{
				LOG.debug("Changing from [{}] to [{}] for [{}] attribute ", splitUri[splitUrlCounter], value, name);
				splitUri[splitUrlCounter] = value;
				break;
			}
			splitUrlCounter++;

		}

		final StringBuilder adjustedPath = new StringBuilder();
		adjustedPath.append(originalContextPath);
		adjustedPath.append(StringUtils.join(splitUri, SLASH_SEPARATOR));
		if (path.endsWith(SLASH_SEPARATOR))
		{
			adjustedPath.append(SLASH_SEPARATOR);
		}
		uriBuilder.setPath(adjustedPath.toString());
		return uriBuilder.build().toString();
	}

	/**
	 * OOTB_CODE
	 *
	 * @param uri
	 * @param contextPath
	 * 		context with encoding attributes
	 * @return
	 */
	@Override
	public String calculateAndUpdateUrlEncodingData(final String uri, final String contextPath)
	{
		final List<UrlEncoderData> urlEncodingAttributes = getCurrentUrlEncodingData();
		final String[] splitUrl = org.apache.commons.lang.StringUtils.split(uri, "/");
		int splitUrlCounter = ArrayUtils.isNotEmpty(splitUrl) && org.apache.commons.lang.StringUtils.remove(contextPath, "/")
				.equals(splitUrl[0]) ? 1 : 0;

		final StringBuilder patternSb = new StringBuilder();
		for (final UrlEncoderData urlEncoderData : urlEncodingAttributes)
		{
			String tempValue;
			if (LANGUAGE_ENCODING.equals(urlEncoderData.getAttributeName()))
			{
				//For language/encodedLocale attribute it isn't required to retrieve value here. 
				//Current value is defined and saved in WileycomLocaleFilter 
				tempValue = getCurrentValue(LANGUAGE_ENCODING);
				urlEncoderData.setCurrentValue(tempValue);
			}
			else
			{
				tempValue = urlEncoderData.getCurrentValue();
				if (splitUrlCounter < splitUrl.length)
				{
					tempValue = splitUrl[splitUrlCounter];
					if (!isValid(urlEncoderData.getAttributeName(), tempValue))
					{
						tempValue = urlEncoderData.getDefaultValue();
						writeDebugLog("Encoding attributes are absent. Injecting current value :  [" + tempValue + "]");
					}
					urlEncoderData.setCurrentValue(tempValue);
					splitUrlCounter++;
				}
			}

			if (patternSb.length() != 0)
			{
				patternSb.append('/');
			}
			patternSb.append(tempValue);

		}
		return patternSb.toString();
	}

	private String getCurrentValue(final String attributeName)
	{
		String attributeValue = null;
		final UrlEncodingAttributeManager attributeManager = getUrlEncoderService().getUrlEncodingAttrManagerMap()
				.get(attributeName);
		if (attributeManager != null)
		{
			attributeValue = attributeManager.getCurrentValue();
		}
		return attributeValue;
	}
}
