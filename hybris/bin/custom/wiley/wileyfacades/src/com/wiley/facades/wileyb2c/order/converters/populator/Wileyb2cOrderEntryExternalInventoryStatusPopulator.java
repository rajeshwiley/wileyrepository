package com.wiley.facades.wileyb2c.order.converters.populator;

import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.util.Assert;

import com.wiley.core.product.data.ExternalInventoryStatus;
import com.wiley.core.product.data.ExternalInventoryStatusRecord;
import com.wiley.facades.product.data.InventoryStatusRecord;



public class Wileyb2cOrderEntryExternalInventoryStatusPopulator implements Populator<ExternalInventoryStatus, OrderEntryData>
{
	@Override
	public void populate(final ExternalInventoryStatus source, final OrderEntryData target)
			throws ConversionException
	{
		Assert.notNull(source);
		Assert.notNull(target.getProduct());
		final List<ExternalInventoryStatusRecord> statuses = source.getRecordsList();
		Assert.notEmpty(statuses);
		target.getProduct().setInventoryStatus(populateStatuses(statuses));
		if (source.getEstimatedDeliveryDays() != null) {
			target.setEstimatedDeliveryDays(source.getEstimatedDeliveryDays());
		}
	}

	protected List<InventoryStatusRecord> populateStatuses(final List<ExternalInventoryStatusRecord> statuses)
	{
		return statuses.stream().map((externalRecord) ->
		{
			final InventoryStatusRecord record = new InventoryStatusRecord();
			record.setAvailableDate(externalRecord.getAvailableDate());
			record.setQuantity(externalRecord.getQuantity());
			record.setSortOrder(externalRecord.getSortOrder());
			record.setStatusCode(externalRecord.getStatusCode());
			return record;
		}).collect(Collectors.toList());
	}
}
