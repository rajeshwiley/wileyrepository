package com.wiley.facades.populators;

import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.PaymentInfoDataPopulator;
import de.hybris.platform.acceleratorservices.payment.data.PaymentInfoData;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.i18n.I18NService;

import org.springframework.beans.factory.annotation.Autowired;


public class WileycomPaymentInfoDataPopulator extends PaymentInfoDataPopulator
{
	@Autowired
	private EnumerationService enumerationService;
	@Autowired
	private I18NService i18NService;

	@Override
	public void populate(final CreditCardPaymentInfoModel source, final PaymentInfoData target) throws ConversionException
	{
		if (source == null)
		{
			return;
		}

		super.populate(source, target);

		String paymentTypeDisplayName = enumerationService.getEnumerationName(source.getType(), i18NService.getCurrentLocale());
		target.setDisplayName(paymentTypeDisplayName);
	}

	public EnumerationService getEnumerationService()
	{
		return enumerationService;
	}

	public void setEnumerationService(final EnumerationService enumerationService)
	{
		this.enumerationService = enumerationService;
	}
}
