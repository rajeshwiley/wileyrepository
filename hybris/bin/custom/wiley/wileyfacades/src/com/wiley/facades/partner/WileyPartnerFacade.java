package com.wiley.facades.partner;

import de.hybris.platform.europe1.enums.UserDiscountGroup;
import de.hybris.platform.commercefacades.user.data.UserGroupData;

import java.util.Map;


public interface WileyPartnerFacade
{
	/**
	 *
	 * Find Wiley Partner Company Model by ID and converts it to WileyPartnerCompanyData
	 *
	 * @param partnerId
	 * @return
	 */
	WileyPartnerCompanyData getWileyPartnerCompany(String partnerId);

	/**
	 *
	 * Checks if category belongs to the partner
	 *
	 * @param partnerCompany
	 * @param categoryCode
	 * @return
	 */
	boolean partnerHasCategory(WileyPartnerCompanyData partnerCompany, String categoryCode);

	String getPartnerDiscountGroupCode(String partnerId);

	Map<String, String> getPartnersNamesByCategoryCode(String categoryCode);

	UserDiscountGroup getPartnerDiscountGroupForId(String partnerId);

	String getPartnerNameForId(String partnerId);

	/**
	 * true if partnerCompany belongs to kpmgPartner User Group, otherwise returns false
	 * @param partnerCompany
	 * @param userGroupUid
     * @return
     */
	boolean partnerBelongsToUserGroup(UserGroupData partnerCompany, String userGroupUid);

	boolean isKPMGPartner(UserGroupData partnerCompany);

	boolean isSpecialPartner(UserGroupData partnerCompany);
}
