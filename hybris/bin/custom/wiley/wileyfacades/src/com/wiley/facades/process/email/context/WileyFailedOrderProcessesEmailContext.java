package com.wiley.facades.process.email.context;

import com.wiley.fulfilmentprocess.model.WileyFailedOrderProcessesReportingEmailProcessModel;
import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

public class WileyFailedOrderProcessesEmailContext
        extends AbstractEmailContext<WileyFailedOrderProcessesReportingEmailProcessModel> {

    private static final Logger LOG = LoggerFactory.getLogger(WileyFailedOrderProcessesEmailContext.class);

    private static final String PATTERN_NOTIFY_FAILED_FILFILLMENT_PROCESSES = "%s.notify.failed.filfillment.processes";

    @Autowired
    private ConfigurationService configurationService;

    private List<WileyFailedOrderProcessEntry> entries;

    private String siteUid;

    private String dateFrom;

    private String dateTo;

    private String mailTo;

    public List<WileyFailedOrderProcessEntry> getEntries() {
        return entries;
    }

    public String getSiteUid() {
        return siteUid;
    }

    public String getDateFrom() {
        return dateFrom;
    }

    public String getDateTo() {
        return dateTo;
    }

    @Override
    protected BaseSiteModel getSite(final WileyFailedOrderProcessesReportingEmailProcessModel businessProcessModel) {
        return businessProcessModel.getSite();
    }

    @Override
    protected CustomerModel getCustomer(
            final WileyFailedOrderProcessesReportingEmailProcessModel businessProcessModel) {
        return null;
    }

    @Override
    protected LanguageModel getEmailLanguage(
            final WileyFailedOrderProcessesReportingEmailProcessModel businessProcessModel) {
        return null;
    }

    @Override
    public void init(final WileyFailedOrderProcessesReportingEmailProcessModel businessProcessModel,
                     final EmailPageModel emailPageModel) {
        super.init(businessProcessModel, emailPageModel);
        siteUid = getSite(businessProcessModel).getUid();
        dateFrom = businessProcessModel.getDateFrom().format(DateTimeFormatter.ISO_DATE_TIME);
        dateTo = businessProcessModel.getDateTo().format(DateTimeFormatter.ISO_DATE_TIME);
        mailTo = configurationService.getConfiguration().getString(buildToEmailConfigurationProperty(), null);

        if (mailTo == null) {
            LOG.warn(
                    String.format("No recipient set for WileyFailedOrderProcessesEmail, email will not be sent. "
                                    + "To fix this issue, set proper email address as a property: [%s]",
                            buildToEmailConfigurationProperty())
            );
        }

        entries = businessProcessModel.getFailedProcesses().stream().map(failedProcess -> {
            //TODO: Extract to converter;
            OrderModel order = failedProcess.getOrder();
            UserModel user = order.getUser();

            WileyFailedOrderProcessEntry entry = new WileyFailedOrderProcessEntry();
            entry.processCode = failedProcess.getCode();
            entry.orderNumber = order.getCode();
            entry.displayName = user.getDisplayName();
            entry.customerEmail = user.getEmailAddress();
            entry.cartType = order.getOrderType().getCode();
            return entry;
        }).collect(Collectors.toList());
    }

    @Override
    public String getEmail() {
        return mailTo;
    }

    @Override
    public String getDisplayName() {
        return mailTo;
    }

    public static class WileyFailedOrderProcessEntry {
        private String processCode;
        private String orderNumber;
        private String displayName;
        private String customerEmail;
        private String cartType;

        public String getProcessCode() {
            return processCode;
        }

        public String getOrderNumber() {
            return orderNumber;
        }

        public String getDisplayName() {
            return displayName;
        }

        public String getCustomerEmail() {
            return customerEmail;
        }

        public String getCartType() {
            return cartType;
        }
    }

    private String buildToEmailConfigurationProperty() {
        return String.format(PATTERN_NOTIFY_FAILED_FILFILLMENT_PROCESSES, siteUid);
    }

}
