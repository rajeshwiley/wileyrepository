package com.wiley.facades.as.address.converters.populator;

import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.RegionData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import javax.annotation.Nonnull;

import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import com.wiley.core.i18n.WileyCommonI18NService;
import com.wiley.core.i18n.WileyCountryService;
import com.wiley.core.integration.address.dto.AddressDto;



public class AddressDataPopulator implements Populator<AddressDto, AddressData>
{
	private WileyCountryService wileyCountryService;

	private WileyCommonI18NService wileyCommonI18NService;

	private Converter<CountryModel, CountryData> countryConverter;

	private Converter<RegionModel, RegionData> regionConverter;

	@Override
	public void populate(@Nonnull final AddressDto source, @Nonnull final AddressData target)
			throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setTown(source.getTown());
		target.setLine1(source.getLine1());
		target.setLine2(source.getLine2());
		target.setPostalCode(source.getPostalCode());

		CountryModel countryModel = null;
		if (!StringUtils.isEmpty(source.getCountryIso2()))
		{
			final String countryIso2 = source.getCountryIso2();
			try
			{
				countryModel = wileyCountryService.findCountryByCode(countryIso2);
				target.setCountry(countryConverter.convert(countryModel));
			}
			catch (final UnknownIdentifierException e)
			{
				throw new ConversionException("No country with the code " + countryIso2 + " found.", e);
			}
			catch (final AmbiguousIdentifierException e)
			{
				throw new ConversionException("More than one country with the code " + countryIso2 + " found.", e);
			}
		}

		if (countryModel != null && !StringUtils.isEmpty(source.getRegionIso2()))
		{
			final String regionIso2 = source.getRegionIso2();
			try
			{
				final RegionModel regionModel = wileyCommonI18NService.getRegionForShortCode(countryModel, regionIso2);
				target.setRegion(regionConverter.convert(regionModel));
			}
			catch (final UnknownIdentifierException e)
			{
				throw new ConversionException("No region with the code " + regionIso2 + " found.", e);
			}
			catch (final AmbiguousIdentifierException e)
			{
				throw new ConversionException("More than one region with the code " + regionIso2 + " found.", e);
			}
		}
	}

	public void setWileyCountryService(final WileyCountryService wileyCountryService)
	{
		this.wileyCountryService = wileyCountryService;
	}

	public void setWileyCommonI18NService(final WileyCommonI18NService wileyCommonI18NService)
	{
		this.wileyCommonI18NService = wileyCommonI18NService;
	}

	public void setCountryConverter(
			final Converter<CountryModel, CountryData> countryConverter)
	{
		this.countryConverter = countryConverter;
	}

	public void setRegionConverter(
			final Converter<RegionModel, RegionData> regionConverter)
	{
		this.regionConverter = regionConverter;
	}
}
