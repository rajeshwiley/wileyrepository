/**
 *
 */
package com.wiley.facades.populators;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commerceservices.product.CommerceProductService;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.Collection;

import org.apache.commons.lang.StringUtils;


public class WileyProductUrlPopulator implements Populator<AbstractOrderEntryModel, OrderEntryData> 
{
    private String externalCategoryCode;
    private CommerceProductService commerceProductService;

    @Override
    public void populate(final AbstractOrderEntryModel source, final OrderEntryData target)
      throws ConversionException 
    {
        ProductModel product = source.getProduct();
    
        if (isExternalProduct(product)) 
        {
            target.getProduct().setUrl(StringUtils.EMPTY);
        }
    }

    private boolean isExternalProduct(final ProductModel product) 
    {
        Collection<CategoryModel> superCategories = null;

        if (product instanceof VariantProductModel) 
        {
            superCategories = getCommerceProductService()
                .getSuperCategoriesExceptClassificationClassesForProduct(
                    ((VariantProductModel) product).getBaseProduct());
        } 
        else 
        {
            superCategories = getCommerceProductService()
                .getSuperCategoriesExceptClassificationClassesForProduct(product);
        }

        return superCategories.stream()
            .filter(category -> category.getCode().equals(externalCategoryCode)).findAny().isPresent();
    }

    public String getExternalCategoryCode() 
    {
        return externalCategoryCode;
    }

    public void setExternalCategoryCode(final String externalCategoryCode) 
    {
        this.externalCategoryCode = externalCategoryCode;
    }
  
    public CommerceProductService getCommerceProductService() 
    {
        return commerceProductService;
    }
  
    public void setCommerceProductService(final CommerceProductService commerceProductService) 
    {
        this.commerceProductService = commerceProductService;
    }
}
