package com.wiley.facades.user;

import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;

import java.util.List;

public interface WileyUserFacade extends UserFacade
{
	/**
	 * Gets All addresses for current user
	 *
	 * @return list of addressData
	 */
	List<AddressData> getPaymentAddressesForCurrentUser();

}
