package com.wiley.facades.wileycom.order.converters.populators;

import de.hybris.platform.commercefacades.order.converters.populator.CreditCardPaymentInfoPopulator;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.util.Config;


/**
 * Created by Mikhail_Asadchy on 9/28/2016.
 */
public class WileycomCreditCardPaymentInfoPopulator extends CreditCardPaymentInfoPopulator
{
	private static final String MASKING_CARD_NUMBER = Config.getParameter("maskedCardNumber.pattern");

	@Override
	public void populate(final CreditCardPaymentInfoModel source, final CCPaymentInfoData target)
	{
		super.populate(source, target);

		target.setCardNumber(MASKING_CARD_NUMBER + source.getNumber());
	}
}
