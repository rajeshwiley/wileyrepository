package com.wiley.facades.wileyb2c.populator;

import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.enums.PaymentModeEnum;
import com.wiley.core.model.WileySubscriptionPaymentTransactionModel;
import com.wiley.facades.product.data.WileySubscriptionPaymentTransactionData;


public class Wileyb2cSubscriptionBillingActivitiesPopulator implements Populator<WileySubscriptionPaymentTransactionModel,
		WileySubscriptionPaymentTransactionData>
{
	public static final String CARD_MASK = "**** ";

	@Autowired
	private PriceDataFactory priceDataFactory;
 
	@Override
	public void populate(final WileySubscriptionPaymentTransactionModel source,
			final WileySubscriptionPaymentTransactionData target) throws ConversionException
	{
		target.setCode(source.getCode());
		target.setBillingDate(source.getTime());
		target.setPaymentStatus(source.getType());
		target.setPaymentMode(source.getPaymentMode());
		
		populatePaymentAmount(source, target);
		populatePaymentDetails(source, target);
	}

	protected void populatePaymentAmount(final WileySubscriptionPaymentTransactionModel source,
			final WileySubscriptionPaymentTransactionData target)
	{
		final String formattedPrice = priceDataFactory.create(PriceDataType.FROM, new BigDecimal(source.getAmount()),
				source.getCurrency()).getFormattedValue();
		target.setPaymentAmount(formattedPrice);
	}

	protected void populatePaymentDetails(final WileySubscriptionPaymentTransactionModel source,
			final WileySubscriptionPaymentTransactionData target)
	{
		final PaymentModeEnum paymentMode = source.getPaymentMode();
		if (PaymentModeEnum.CARD.equals(paymentMode))
		{
			final StringBuilder result = new StringBuilder();
			final String paymentToken = source.getPaymentToken();
			if (paymentToken != null)
			{
				//displaying only last 4 digits of the card
				final int startIndex = paymentToken.length() - 4;
				if (startIndex >= 0)
				{
					result.append(CARD_MASK);
					result.append(paymentToken.substring(startIndex));
				}
			}
			target.setCreditCardMask(result.toString());
		}
	}
}