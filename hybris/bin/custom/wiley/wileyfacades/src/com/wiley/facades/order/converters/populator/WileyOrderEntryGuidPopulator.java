package com.wiley.facades.order.converters.populator;

import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;


public class WileyOrderEntryGuidPopulator implements Populator<AbstractOrderEntryModel, OrderEntryData>
{

	@Override
	public void populate(final AbstractOrderEntryModel source, final OrderEntryData target)
			throws ConversionException
	{
		target.setGuid(source.getGuid());
	}
}
