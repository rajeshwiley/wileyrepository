package com.wiley.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.subscriptionfacades.data.SubscriptionTermData;
import de.hybris.platform.subscriptionservices.model.SubscriptionTermModel;


public class WileyB2CSubscriptionTermPopulator implements Populator<SubscriptionTermModel, SubscriptionTermData>
{
	@Override
	public void populate(final SubscriptionTermModel subscriptionTermModel,
			final SubscriptionTermData subscriptionTermData)
			throws ConversionException
	{
		subscriptionTermData.setId(subscriptionTermModel.getId());
		subscriptionTermData.setName(subscriptionTermModel.getName());
	}
}
