package com.wiley.facades.wileyb2c.search.converters.populator;

import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ImageDataType;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.commercefacades.search.converters.populator.SearchResultVariantOptionsProductPopulator;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.enums.WileyProductLifecycleEnum;
import com.wiley.core.enums.WileyWebLinkTypeEnum;
import com.wiley.core.wileyb2c.i18n.Wileyb2cI18NService;
import com.wiley.facades.product.data.ExternalStoreData;


/**
 * @author Dzmitryi_Halahayeu
 */
public class Wileyb2cSearchResultProductPopulator extends SearchResultVariantOptionsProductPopulator
{
	private String purchaseOptionNameProperty;

	private String purchaseOptionDescriptionProperty;

	private String variantProductUrlProperty;

	private String publicationDateProperty;

	private String hasExternalStoresProperty;

	private String externalStoreTypeProperty;

	private String externalStoreUrlProperty;

	private String countableProperty;

	private String lifecycleStatusProperty;

	private Wileyb2cI18NService wileyb2cI18NService;

	private String authorProperty;

	private String isAvailable;

	@Override
	protected List<VariantOptionData> getVariantOptions(final List<SearchResultValueData> variants,
			final Set<String> variantTypeAttributes, final String rollupProperty)
	{
		final List<VariantOptionData> variantOptions = super.getVariantOptions(variants, variantTypeAttributes, rollupProperty);
		final Iterator<SearchResultValueData> iterator = variants.iterator();
		for (final VariantOptionData variantOptionData : variantOptions)
		{
			final SearchResultValueData resultValueData = iterator.next();
			variantOptionData.setPurchaseOptionName((String) resultValueData.getValues().get(purchaseOptionNameProperty));
			variantOptionData.setPurchaseOptionDescription(
					(String) resultValueData.getValues().get(purchaseOptionDescriptionProperty));
			populatePublicationDate(variantOptionData, resultValueData);
			populateExternalStores(resultValueData, variantOptionData);
			variantOptionData.setUrl((String) resultValueData.getValues().get(variantProductUrlProperty));
			variantOptionData.setCountable((Boolean) resultValueData.getValues().get(countableProperty));

			variantOptionData.setName(getValue(resultValueData, "name"));
			populatePromotions(resultValueData, variantOptionData);
			populateAuthors(resultValueData, variantOptionData);
			populateLifecycleStatus(resultValueData, variantOptionData);
			variantOptionData.setImages(createImageData(resultValueData));

			populateProductAvailability(resultValueData, variantOptionData);
		}
		return variantOptions;
	}

	private void populateProductAvailability(final SearchResultValueData resultValueData,
			final VariantOptionData variantOptionData)
	{
		final Boolean isAvailalbe = (Boolean) resultValueData.getValues().get(isAvailable);
		variantOptionData.setAvailable(isAvailalbe);
	}

	protected void populatePublicationDate(final VariantOptionData variantOptionData, final SearchResultValueData resultValueData)
	{
		//We need to use multi value field due to bug https://issues.apache.org/jira/browse/SOLR-7495
		final ArrayList<Date> dates = (ArrayList<Date>) resultValueData.getValues().get(publicationDateProperty);
		if (CollectionUtils.size(dates) > 0)
		{
			variantOptionData.setPublicationDate(dates.get(0));
		}
	}

	protected void populateExternalStores(final SearchResultValueData resultValueData, final VariantOptionData variantOptionData)
	{
		variantOptionData.setHasExternalStores((Boolean) resultValueData.getValues().get(hasExternalStoresProperty));
		boolean isHidePrice = false;

		final String externalStoreType = (String) resultValueData.getValues().get(externalStoreTypeProperty);
		if (StringUtils.isNotEmpty(externalStoreType))
		{
			final String externalStoreUrl = (String) resultValueData.getValues().get(externalStoreUrlProperty);
			final ExternalStoreData externalStoreData = new ExternalStoreData();
			externalStoreData.setType(externalStoreType);
			externalStoreData.setUrl(externalStoreUrl);
			variantOptionData.setExternalStores(Collections.singletonList(externalStoreData));
			isHidePrice = WileyWebLinkTypeEnum.REQUEST_QUOTE.getCode().equals(externalStoreType);
		}
		variantOptionData.setHidePrice(isHidePrice);
	}

	private void populateLifecycleStatus(final SearchResultValueData resultValueData, final VariantOptionData variantOptionData)
	{
		String lifecycleStatusValue = getValue(resultValueData, lifecycleStatusProperty);
		if (StringUtils.isNotBlank(lifecycleStatusValue))
		{
			variantOptionData.setLifecycleStatus(WileyProductLifecycleEnum.valueOf(lifecycleStatusValue));
		}
	}

	protected void populatePromotions(final SearchResultValueData source, final VariantOptionData target)
	{
		final String promotionCode = this.<String> getValue(source, "primaryPromotionCode");
		if (StringUtils.isNotEmpty(promotionCode))
		{
			final String primaryPromotionBannerUrl = this.<String> getValue(source, "primaryPromotionBanner");
			target.setPotentialPromotions(
					Collections.singletonList(createPromotionData(promotionCode, primaryPromotionBannerUrl)));
		}
	}

	protected void populateAuthors(final SearchResultValueData source, final VariantOptionData target)
	{
		if (MapUtils.isNotEmpty(source.getValues()) && source.getValues().get(authorProperty)
				!= null)
		{
			String authors = ((List<String>) source.getValues().get(authorProperty)).stream().collect(
					Collectors.joining(", "));
			target.setAuthorsWithoutRoles(authors);
		}
	}

	@Override
	protected void populatePrices(final SearchResultValueData source, final ProductData target)
	{
		superPopulatePrices(source, target);

		if (target.getPrice() == null)
		{
			target.setPrice(new PriceData());
		}

		Optional<String> currentCountryTaxShortMessage = wileyb2cI18NService.getCurrentCountryTaxShortMsg();
		Optional<String> currentCountryTaxTooltip = wileyb2cI18NService.getCurrentCountryTaxTooltip();
		target.getPrice().setOptionalTaxShortMessage(currentCountryTaxShortMessage.orElse(null));
		target.getPrice().setOptionalTaxTooltip(currentCountryTaxTooltip.orElse(null));
	}

	@Override
	protected void addImageData(final SearchResultValueData source, final String imageFormat, final List<ImageData> images)
	{
		final String mediaFormatQualifier = getImageFormatMapping().getMediaFormatQualifierForImageFormat(imageFormat);
		if (mediaFormatQualifier != null && !mediaFormatQualifier.isEmpty())
		{
			addImageData(source, imageFormat, mediaFormatQualifier, ImageDataType.GALLERY, images);
		}
	}

	protected void superPopulatePrices(final SearchResultValueData source, final ProductData target)
	{
		super.populatePrices(source, target);
	}

	@Required
	public void setPurchaseOptionNameProperty(final String purchaseOptionNameProperty)
	{
		this.purchaseOptionNameProperty = purchaseOptionNameProperty;
	}

	@Required
	public void setPurchaseOptionDescriptionProperty(final String purchaseOptionDescriptionProperty)
	{
		this.purchaseOptionDescriptionProperty = purchaseOptionDescriptionProperty;
	}

	@Required
	public void setVariantProductUrlProperty(final String variantProductUrl)
	{
		this.variantProductUrlProperty = variantProductUrl;
	}

	@Required
	public void setPublicationDateProperty(final String publicationDateProperty)
	{
		this.publicationDateProperty = publicationDateProperty;
	}

	@Required
	public void setHasExternalStoresProperty(final String hasExternalStoresProperty)
	{
		this.hasExternalStoresProperty = hasExternalStoresProperty;
	}

	@Required
	public void setCountableProperty(final String countableProperty)
	{
		this.countableProperty = countableProperty;
	}

	@Required
	public void setWileyb2cI18NService(final Wileyb2cI18NService wileyb2cI18NService)
	{
		this.wileyb2cI18NService = wileyb2cI18NService;
	}

	@Required
	public void setAuthorProperty(final String authorProperty)
	{
		this.authorProperty = authorProperty;
	}

	@Required
	public void setLifecycleStatusProperty(final String lifecycleStatusProperty)
	{
		this.lifecycleStatusProperty = lifecycleStatusProperty;
	}

	@Required
	public void setExternalStoreTypeProperty(final String externalStoreTypeProperty)
	{
		this.externalStoreTypeProperty = externalStoreTypeProperty;
	}

	@Required
	public void setExternalStoreUrlProperty(final String externalStoreUrlProperty)
	{
		this.externalStoreUrlProperty = externalStoreUrlProperty;
	}

	@Required
	public void setIsAvailable(final String isAvailable)
	{
		this.isAvailable = isAvailable;
	}
}
