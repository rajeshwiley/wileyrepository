package com.wiley.facades.wileyb2c.populator;

import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import junit.framework.Assert;


public class Wileyb2cCountryDefaultLanguagePopulator implements Populator<CountryModel, CountryData>
{
	@Override
	public void populate(final CountryModel countryModel, final CountryData countryData) throws ConversionException
	{
		Assert.assertNotNull(countryModel);
		Assert.assertNotNull(countryData);
				
		countryData.setDefaultLanguage(countryModel.getDefaultLanguage().getIsocode());
	}
}
