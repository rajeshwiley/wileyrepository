package com.wiley.facades.locale;

import de.hybris.platform.commercefacades.user.data.CountryData;

//TODO: JavaDocs
public interface WileyLocaleFacade
{
	String getDefaultEncodedLocale();
	String getCurrentEncodedLocale();
	CountryData getCountryForEncodedLocale(String encodedLocale);
	void setCurrentEncodedLocale(String encodedLocale);
}
