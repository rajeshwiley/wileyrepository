package com.wiley.facades.order.converters.populator;

import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.order.strategies.WileyOrderCancelabilityStrategy;


public class WileyOrderEntryCancellablePopulator implements Populator<AbstractOrderEntryModel, OrderEntryData>
{

	private static final Logger LOG = LoggerFactory.getLogger(WileyOrderEntryCancellablePopulator.class);

	private final WileyOrderCancelabilityStrategy wileyOrderCancelabilityStrategy;

	@Autowired
	public WileyOrderEntryCancellablePopulator(
			final WileyOrderCancelabilityStrategy wileyOrderCancelabilityStrategy)
	{
		this.wileyOrderCancelabilityStrategy = wileyOrderCancelabilityStrategy;
	}

	@Override
	public void populate(final AbstractOrderEntryModel source, final OrderEntryData target)
			throws ConversionException
	{
		if (target.getStatus() == null)
		{
			LOG.error("Can't populate order cancellability, no entry status defined yet");
			return;
		}

		AbstractOrderModel abstractOrder = source.getOrder();

		if (abstractOrder instanceof OrderModel)
		{
			target.setCancelable(
					wileyOrderCancelabilityStrategy.isCancelableOrderEntry((OrderModel) abstractOrder, target.getStatus())
			);
		}
	}
}
