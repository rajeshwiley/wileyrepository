package com.wiley.facades.wileycom.storesession.impl;

import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.locale.WileyLocaleService;
import com.wiley.core.model.WileyExternalSiteLinkModel;
import com.wiley.core.model.WorldRegionModel;
import com.wiley.core.servicelayer.i18n.WileycomI18NService;
import com.wiley.core.wileycom.i18n.WileycomWorldRegionService;
import com.wiley.core.wileycom.storesession.WileycomCountryStoreSessionService;
import com.wiley.facade.user.data.WorldRegionData;
import com.wiley.facades.storesession.impl.WileyStoreSessionFacadeImpl;
import com.wiley.facades.wileycom.storesession.WileycomStoreSessionFacade;


/**
 *
 */
public class WileycomStoreSessionFacadeImpl  extends WileyStoreSessionFacadeImpl implements WileycomStoreSessionFacade
{

	private static final Logger LOG = LoggerFactory.getLogger(WileycomStoreSessionFacadeImpl.class);

	@Resource
	private Converter<CountryModel, CountryData> wileycomCountryConverter;

	@Resource
	private Converter<WorldRegionModel, WorldRegionData> wileycomWorldRegionConverter;

	@Resource
	private WileycomWorldRegionService wileycomWorldRegionService;
	
	@Resource
	private WileycomI18NService wileycomI18NService;

	@Resource
	private WileyLocaleService wileyLocaleService;

	@Resource
	private BaseStoreService baseStoreService;

	private WileycomCountryStoreSessionService wileycomCountryStoreSessionService;

	@Override
	public CountryData getSessionCountry()
	{
		Optional<CountryModel> country = wileycomI18NService.getCurrentCountry();

		if (country.isPresent())
		{
			return getWileycomCountryConverter().convert(country.get());
		}

		return null;
	}

	@Override
	public Optional<String> setCurrentCountry(@Nonnull final String countryIsocode)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("countryIsocode", countryIsocode);

		Optional<String> result = Optional.empty();

		final Optional<CountryModel> optionalCountry = wileycomCountryStoreSessionService.updateCurrentCountryIfRequired(
				Optional.of(countryIsocode), true);

		if (optionalCountry.isPresent())
		{
			CountryModel countryModel = optionalCountry.get();
			String encodedLocale = wileyLocaleService.getEncodedLocale(countryModel, countryModel.getDefaultLanguage());
			wileyLocaleService.setEncodedLocale(encodedLocale);
			result = Optional.of(countryModel.getIsocode());
		}

		return result;
	}
	
	@Override
	public CountryData getCurrentCountry()
	{
		Optional<CountryModel> currentCountry = wileycomI18NService.getCurrentCountry();
		if (currentCountry.isPresent())
		{
			return wileycomCountryConverter.convert(currentCountry.get());
		}
		return null;
	}

	@Override
	public List<CountryData> getCounties()
	{
		final Collection<CountryModel> allCountries = getCommerceCommonI18NService().getAllCountries();
		return Converters.convertAll(allCountries, wileycomCountryConverter);
	}

	@Override
	public List<WorldRegionData> getWorldRegions()
	{
		List<WorldRegionModel> allWorldRegions = wileycomWorldRegionService.getWorldRegions();
		return Converters.convertAll(allWorldRegions, wileycomWorldRegionConverter);
	}

	public Converter<CountryModel, CountryData> getWileycomCountryConverter()
	{
		return wileycomCountryConverter;
	}

	public void setWileycomCountryConverter(
			final Converter<CountryModel, CountryData> wileycomCountryConverter)
	{
		this.wileycomCountryConverter = wileycomCountryConverter;
	}

	public WileycomI18NService getWileycomI18NService()
	{
		return wileycomI18NService;
	}

	public void setWileycomI18NService(final WileycomI18NService wileycomI18NService)
	{
		this.wileycomI18NService = wileycomI18NService;
	}

	@Required
	public void setWileycomCountryStoreSessionService(
			final WileycomCountryStoreSessionService wileycomCountryStoreSessionService)
	{
		this.wileycomCountryStoreSessionService = wileycomCountryStoreSessionService;
	}

	@Override
	public Optional<String> getExternalSiteRedirect(final String countryIsoCode) {
		BaseStoreModel currentBaseStore = baseStoreService.getCurrentBaseStore();
		if (CollectionUtils.isEmpty(currentBaseStore.getExternalLinks())) {
			return Optional.empty();
		}

		Optional<WileyExternalSiteLinkModel> externalSiteLink = currentBaseStore.getExternalLinks()
				.stream()
				.filter(link -> StringUtils.equals(countryIsoCode, link.getCountry().getIsocode()))
				.findFirst();

		return externalSiteLink.map(WileyExternalSiteLinkModel::getUrl);
	}
}
