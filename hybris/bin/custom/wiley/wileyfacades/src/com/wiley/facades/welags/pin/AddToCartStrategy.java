package com.wiley.facades.welags.pin;

import de.hybris.platform.core.model.order.CartModel;

import com.wiley.core.model.PinModel;
import com.wiley.facades.welags.pin.exception.PinOperationException;

/**
 * Interface for strategies which place the product into the cart.
 *
 * @author Robert_Farkas
 * @author Gergo_Fodi
 */
public interface AddToCartStrategy
{
	/**
	 * Placing the product into the cart.
	 *
	 * @param cartModel The cart model.
	 * @param pinModel The pin model containing the product code.
	 * @throws PinOperationException
	 */
	void addProductToCart(CartModel cartModel, PinModel pinModel) throws PinOperationException;
}
