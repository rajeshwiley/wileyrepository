package com.wiley.facades.product.converters.populator;

import de.hybris.platform.commercefacades.product.converters.populator.AbstractProductImagePopulator;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ImageDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;



/**
 * Wileycom populator for {@link ProductModel#getPicture()} field
 */
public class WileycomProductPicturePopulator extends AbstractProductImagePopulator<ProductModel, ProductData>
{

	private static final String PRODUCT_DATA_FORMAT = "product";
	private static final String THUMBNAIL_DATA_FORMAT = "thumbnail";
	private static final Integer DEFAULT_DATA_GALLERY_INDEX = 0;

	@Override
	public void populate(final ProductModel productModel, final ProductData productData) throws ConversionException
	{
		if (productModel.getPicture() != null)
		{
			List<ImageData> imageList = new ArrayList<>();
			if (CollectionUtils.isNotEmpty(productData.getImages()))
			{
				imageList.addAll(productData.getImages());
			}

			imageList.add(convertMedia(productModel.getPicture(), ImageDataType.GALLERY, PRODUCT_DATA_FORMAT));
			imageList.add(convertMedia(productModel.getPicture(), ImageDataType.PRIMARY, THUMBNAIL_DATA_FORMAT));

			productData.setImages(imageList);
		}



	}

	private ImageData convertMedia(final MediaModel picture, final ImageDataType imageDataType, final String format)
	{
		ImageData imageData = getImageConverter().convert(picture);
		imageData.setGalleryIndex(DEFAULT_DATA_GALLERY_INDEX);
		imageData.setImageType(imageDataType);
		imageData.setFormat(format);
		return imageData;
	}
}
