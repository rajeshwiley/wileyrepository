package com.wiley.facades.wileycom.product.converters.populators;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.Optional;

import javax.annotation.Nonnull;

import org.springframework.util.Assert;

import com.wiley.core.enums.WileyProductSubtypeEnum;


/**
 * Populates basic product fields for wileycom.
 */
public class WileycomExtendedProductBasicPopulator implements Populator<ProductModel, ProductData>
{

	@Override
	public void populate(@Nonnull final ProductModel productModel, @Nonnull final ProductData productData)
			throws ConversionException
	{
		Assert.notNull(productModel);
		Assert.notNull(productData);

		// TODO : [ECSC-20075] commented out code which uses obsolete attribute. Should be deleted
//		productData.setAuthors(productModel.getAuthors());
		productData.setIsbn(productModel.getIsbn());
		productData.setSapProductCode(productModel.getSapProductCode());
		productData.setSubtype(Optional.ofNullable(productModel.getSubtype()).map(WileyProductSubtypeEnum::getCode).orElse(null));
	}
}
