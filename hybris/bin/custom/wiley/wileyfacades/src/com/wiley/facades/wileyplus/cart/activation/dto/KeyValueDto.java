package com.wiley.facades.wileyplus.cart.activation.dto;

import java.util.Objects;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;



/**
 * A key-value pair. It is used for passing non ecommerce parameters
 **/

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2016-08-09T10:25:39.625+02:00")

public class KeyValueDto
{

	@JsonIgnore
	private String key = null;
	@JsonIgnore
	private String value = null;

	/**
	 **/
	public KeyValueDto key(final String key)
	{
		this.key = key;
		return this;
	}

	@NotNull
	@Size(max = 255)
	@JsonGetter("k")
	public String getKey()
	{
		return key;
	}

	@JsonSetter("key")
	public void setKey(final String key)
	{
		this.key = key;
	}

	@JsonSetter("k")
	public void setK(final String k)
	{
		this.key = k;
	}

	/**
	 **/
	public KeyValueDto value(final String value)
	{
		this.value = value;
		return this;
	}

	@NotNull
	@Size(max = 255)
	@JsonGetter("v")
	public String getValue()
	{
		return value;
	}

	@JsonSetter("value")
	public void setValue(final String value)
	{
		this.value = value;
	}

	@JsonSetter("v")
	public void setV(final String v)
	{
		this.value = v;
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		KeyValueDto keyValueDto = (KeyValueDto) o;
		return Objects.equals(key, keyValueDto.key)
				&& Objects.equals(value, keyValueDto.value);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(key, value);
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("class KeyValue {\n");

		sb.append("    key: ").append(toIndentedString(key)).append("\n");
		sb.append("    value: ").append(toIndentedString(value)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(final Object o)
	{
		if (o == null)
		{
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}

