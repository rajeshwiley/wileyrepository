package com.wiley.facades.product;

import com.wiley.facades.product.data.FreeTrialData;


/**
 * Created by Uladzimir_Barouski on 3/17/2016.
 */
public interface WileyFreeTrialProductFacade
{
	FreeTrialData getFreeTrialProductByCode(String code);

	FreeTrialData getFreeTrialForProduct(String productCode);
}
