package com.wiley.facades.wileyb2c.populator;

import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CountryModel;

import org.springframework.beans.factory.annotation.Value;


/**
 * Created by Maksim_Kozich on 6/5/2016.
 */
public class Wileyb2cCountryLegacyCartUrlPopulator implements Populator<CountryModel, CountryData>
{
	@Value("${legacy.cart.add.to.cart.link}")
	private String legacyAddToCartLink;


	@Override
	public void populate(final CountryModel source, final CountryData target)
	{
		if (source.getLegacyCart() != null && source.getLegacyCart().getBaseUrl() != null)
		{
			target.setB2cLegacyCartUrl(source.getLegacyCart().getBaseUrl() + legacyAddToCartLink);
		}
	}

	public void setLegacyAddToCartLink(final String legacyAddToCartLink)
	{
		this.legacyAddToCartLink = legacyAddToCartLink;
	}
}
