package com.wiley.facades.wileycom.customer.converters;

import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.springframework.util.Assert;

import com.wiley.core.model.NameSuffixModel;
import com.wiley.facades.customer.data.NameSuffixData;


public class WileycomNameSuffixConverter implements Converter<NameSuffixModel, NameSuffixData>
{
	@Override
	public NameSuffixData convert(final NameSuffixModel source) throws ConversionException
	{
		return convert(source, new NameSuffixData());
	}

	@Override
	public NameSuffixData convert(final NameSuffixModel source, final NameSuffixData target)
			throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		target.setCode(source.getCode());
		target.setName(source.getName());
		return target;
	}
}
