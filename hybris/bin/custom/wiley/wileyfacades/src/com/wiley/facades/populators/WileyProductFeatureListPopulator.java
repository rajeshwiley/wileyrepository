package com.wiley.facades.populators;

import de.hybris.platform.catalog.enums.ClassificationAttributeTypeEnum;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.commercefacades.product.converters.populator.ProductFeatureListPopulator;
import de.hybris.platform.commercefacades.product.data.ClassificationData;
import de.hybris.platform.commercefacades.product.data.FeatureData;
import de.hybris.platform.commercefacades.product.data.ProductData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.wiley.core.enums.ClassAttributeAssignmentTypeEnum;


/**
 * Product Features List Populator
 *
 */
public class WileyProductFeatureListPopulator<SOURCE extends FeatureList, TARGET extends ProductData>
    extends ProductFeatureListPopulator {

  @Override
  protected List<ClassificationData> buildClassificationDataList(final FeatureList source) {
    final List<ClassificationData> result = new ArrayList<ClassificationData>();
    final Map<String, ClassificationData> map = new HashMap<String, ClassificationData>();

    for (final Feature feature : source.getFeatures()) {
      if (feature.getValues() != null && !feature.getValues().isEmpty()) {
        final ClassAttributeAssignmentModel classAttributeAssignment = feature
            .getClassAttributeAssignment();
        // check type of classAttributeAssignment
        if (classAttributeAssignment.getType() == ClassAttributeAssignmentTypeEnum.INCLUDES
            && classAttributeAssignment.getAttributeType() == ClassificationAttributeTypeEnum.BOOLEAN
            && StringUtils.isNotBlank(classAttributeAssignment.getDetails())) {
          final ClassificationData classificationData;
          final ClassificationClassModel classificationClass = classAttributeAssignment
              .getClassificationClass();


          final String classificationClassCode = classificationClass.getCode();
          if (map.containsKey(classificationClassCode)) {
            classificationData = map.get(classificationClassCode);
          } else {
            classificationData = (ClassificationData) getClassificationConverter().convert(
                classificationClass);

            map.put(classificationClassCode, classificationData);
            result.add(classificationData);
          }

          // Create the feature
          final FeatureData newFeature = (FeatureData) getFeatureConverter().convert(feature);

          // Add the feature to the classification
          if (classificationData.getFeatures() == null) {
            classificationData.setFeatures(new ArrayList<FeatureData>(1));
          }
          classificationData.getFeatures().add(newFeature);
        }
      }
    }

    return result.isEmpty() ? null : result;
  }
}
