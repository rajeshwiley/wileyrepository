package com.wiley.facades.wileyb2c.wileyplus.url.strategy;


import java.net.URISyntaxException;

import com.wiley.facades.wileyb2c.wileyplus.url.strategy.exception.WileyplusChecksumMismatchException;
import com.wiley.facades.wileyplus.cart.activation.dto.CartActivationRequestDto;


public interface WileyplusPurchaseOptionsPageParseURLStrategy
{
	/**
	 * Decodes Base64 encoded Cart Activation Query Parameters
	 * Validates checksum and throws WileyplusChecksumMismatchException if mismatch found
	 * Populates CartActivationRequestDto with parameters from decoded query string
	 *
	 * @param encodedCartActivationInfo
	 * @param checksum
	 * @return
	 * @throws URISyntaxException
	 * @throws WileyplusChecksumMismatchException
	 */
	CartActivationRequestDto parseCartActivationParams(String encodedCartActivationInfo, String checksum)
			throws URISyntaxException, WileyplusChecksumMismatchException;
}
