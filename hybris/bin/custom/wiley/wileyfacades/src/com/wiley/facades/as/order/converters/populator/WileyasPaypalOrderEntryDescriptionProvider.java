package com.wiley.facades.as.order.converters.populator;

import com.paypal.hybris.facade.product.PaypalOrderEntryDescriptionProvider;
import com.wiley.facades.order.data.WileyArticleData;

import de.hybris.platform.commercefacades.order.data.OrderEntryData;


public class WileyasPaypalOrderEntryDescriptionProvider implements PaypalOrderEntryDescriptionProvider
{
	@Override
	public String getEntryDescription(final OrderEntryData entry)
	{
		final WileyArticleData articleData = entry.getArticle();
		if (articleData != null)
		{
			return articleData.getJournalName() + ". " + articleData.getSummary();
		}
		else
		{
			return entry.getProduct().getDescription();
		}
	}
}
