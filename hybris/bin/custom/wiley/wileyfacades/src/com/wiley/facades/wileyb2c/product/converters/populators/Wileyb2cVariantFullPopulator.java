package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.platform.commercefacades.product.converters.populator.VariantFullPopulator;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.model.WileyPurchaseOptionProductModel;
import com.wiley.facades.constants.WileyFacadesConstants;
import com.wiley.facades.wiley.visibility.WileyProductVisibilityFacade;


public class Wileyb2cVariantFullPopulator extends VariantFullPopulator<ProductModel, ProductData>
{
	private static final VariantOptionComparator OPTION_COMPARATOR = new VariantOptionComparator();

	@Autowired
	@Qualifier("wileyb2cProductVisibilityFacade")
	private WileyProductVisibilityFacade wileyProductVisibilityFacade;

	private SessionService sessionService;

	@Override
	public void populate(final ProductModel productModel, final ProductData productData) throws ConversionException
	{
		ProductModel baseProduct = productModel;
		final boolean isPurchaseOptionVariant = productModel instanceof WileyPurchaseOptionProductModel;
		if (isPurchaseOptionVariant)
		{
			baseProduct = ((WileyPurchaseOptionProductModel) productModel).getBaseProduct();
		}
		super.populate(baseProduct, productData);
		sortAndFilterVariantOptionBySequence(productData);
		if (isPurchaseOptionVariant)
		{
			populateCurrentPurchaseOption(productData);
		}
	}

	private void populateCurrentPurchaseOption(final ProductData productData)
	{
		if (CollectionUtils.isNotEmpty(productData.getVariantOptions()))
		{
			final Optional<VariantOptionData> currentOption = productData.getVariantOptions()
					.stream()
					.filter(option -> option.getCode().equals(productData.getCode())).findFirst();
			if (currentOption.isPresent())
			{
				productData.setProductPurchaseOption(currentOption.get());
			}
			else
			{
				throw new IllegalArgumentException("There is no variant purchase options"
						+ " for current variant code " + productData.getCode());
			}
		}
	}

	private void sortAndFilterVariantOptionBySequence(final ProductData productData)
	{
		final List<VariantOptionData> variantOptions = productData.getVariantOptions();
		if (CollectionUtils.isNotEmpty(variantOptions))
		{
			List<VariantOptionData> sortedOptions = variantOptions.stream()
					.filter(variant -> isOptionVisible(variant))
					.sorted(OPTION_COMPARATOR)
					.collect(Collectors.toList());
			productData.setVariantOptions(sortedOptions);

		}
	}

	private boolean isOptionVisible(final VariantOptionData variant)
	{
		boolean isVisible = true;
		if (!getIgnoreVisibility())
		{
			isVisible = wileyProductVisibilityFacade.isProductVisible(variant.getCode());
		}
		if (isVisible)
		{
			isVisible = isOptionInList(variant);
		}
		return isVisible;
	}

	private boolean isOptionInList(final VariantOptionData variant)
	{
		List<String> variantOptionList = sessionService.getAttribute(
				WileyFacadesConstants.PURHASE_OPRIONS_LOCAL_VIEW_PARAM_NAME);
		if (CollectionUtils.isNotEmpty(variantOptionList) && !variantOptionList.contains(variant.getCode()))
		{
			return false;
		}
		return true;
	}

	private boolean getIgnoreVisibility()
	{
		return BooleanUtils.isTrue(sessionService.getAttribute(WileyFacadesConstants.IGNORE_VISIBILITY_LOCAL_VIEW_PARAM_NAME));
	}


	private static final class VariantOptionComparator implements Comparator<VariantOptionData>
	{

		@Override
		public int compare(final VariantOptionData first, final VariantOptionData second)
		{
			Integer firstSeq =
					first.getPurchaseOptionSequenceNumber() != null ?
							first.getPurchaseOptionSequenceNumber() : Integer.MAX_VALUE;
			Integer secondSeq =
					second.getPurchaseOptionSequenceNumber() != null ?
							second.getPurchaseOptionSequenceNumber() : Integer.MAX_VALUE;
			return firstSeq.compareTo(secondSeq);
		}
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}
}
