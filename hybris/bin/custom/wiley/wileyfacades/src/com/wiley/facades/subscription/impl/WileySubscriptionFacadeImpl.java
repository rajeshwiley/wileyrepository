package com.wiley.facades.subscription.impl;

import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.order.CartService;
import de.hybris.platform.product.PriceService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.subscriptionservices.enums.SubscriptionStatus;

import java.math.BigDecimal;
import java.util.List;

import javax.annotation.Resource;

import org.fest.util.Collections;

import com.wiley.core.model.WileySubscriptionModel;
import com.wiley.core.subscription.services.WileySubscriptionService;
import com.wiley.facades.product.data.WileySubscriptionData;
import com.wiley.facades.subscription.WileySubscriptionFacade;



/**
 * Facade for subscriptions. Default implementation of {@link WileySubscriptionFacade}.
 *
 * Created by Uladzimir_Barouski on 12/16/2015.
 */
public class WileySubscriptionFacadeImpl implements WileySubscriptionFacade
{
	private WileySubscriptionService subscriptionService;
	private PriceService priceService;
	private PriceDataFactory priceDataFactory;
	private UserService userService;
	private Converter<WileySubscriptionModel, WileySubscriptionData> subscriptionConverter;

	@Resource
	private CartService cartService;


	/**
	 * @param userService
	 * 		the userService to set
	 */
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}


	/**
	 * @return the subscriptionConverter
	 */
	public Converter<WileySubscriptionModel, WileySubscriptionData> getSubscriptionConverter()
	{
		return subscriptionConverter;
	}


	/**
	 * @param subscriptionConverter
	 * 		the subscriptionConverter to set
	 */
	public void setSubscriptionConverter(
			final Converter<WileySubscriptionModel, WileySubscriptionData> subscriptionConverter)
	{
		this.subscriptionConverter = subscriptionConverter;
	}


	@Override
	public WileySubscriptionData getActiveSubscription()
	{
		UserModel userModel = userService.getCurrentUser();
		WileySubscriptionModel subscription = subscriptionService.getSubscription(userModel, SubscriptionStatus.ACTIVE);
		return populateSubscriptionData(subscription);
	}



	@Override
	public WileySubscriptionData updateAutoRenew(final String code, final boolean autoRenew)
	{
		WileySubscriptionModel subscription = subscriptionService.getSubscriptionByCode(code);
		if (subscription == null)
		{
			throw new UnknownIdentifierException(String.format("The subscription for code %s not found.", code));
		}
		if (subscription.getRenewalEnabled())
		{
			subscription.setRequireRenewal(autoRenew);
			subscriptionService.updateAutoRenew(subscription);
		}
		WileySubscriptionData subscriptionData = populateSubscriptionData(subscription);
		return subscriptionData;
	}

	@Override
	public boolean hasCurrentUserActiveSubscription()
	{
		final WileySubscriptionModel subscription = subscriptionService.getSubscription(userService.getCurrentUser(),
				SubscriptionStatus.ACTIVE);

		return subscription != null;
	}

	@Override
	public boolean hasCurrentUserAnySubscriptionInCart()
	{
		boolean hasAnySubscription = false;

		if (cartService.hasSessionCart())
		{
			hasAnySubscription = subscriptionService.doesAnySubscriptionProductExistInCart(cartService.getSessionCart());
		}

		return hasAnySubscription;
	}

	/**
	 * Populates Subscriprion Data from Subscription Model
	 *
	 * @param subscription
	 * 		the subscription model
	 * @return populated Subscription Data object if subscription model exist
	 */
	private WileySubscriptionData populateSubscriptionData(final WileySubscriptionModel subscription)
	{
		if (subscription != null)
		{
			final WileySubscriptionData subscriptionData = getSubscriptionConverter().convert(subscription);
			subscriptionData.setPrice(getSubscriptionPrice(subscription.getProduct()));
			subscriptionData.setRenewalInProgress(subscriptionService.isSubscriptionInProgress(subscription.getCode()));
			return subscriptionData;
		}
		return null;
	}

	/**
	 * Get actual subscription price for current user.
	 *
	 * @param subscriptionProduct
	 * @return PriceData
	 */
	private PriceData getSubscriptionPrice(final ProductModel subscriptionProduct)
	{
		final List<PriceInformation> prices = priceService.getPriceInformationsForProduct(subscriptionProduct);

		if (!Collections.isEmpty(prices))
		{
			final PriceInformation price = prices.iterator().next();
			final BigDecimal priceValue = BigDecimal.valueOf(price.getPriceValue().getValue());
			return priceDataFactory.create(PriceDataType.BUY,
					priceValue, price.getPriceValue().getCurrencyIso());
		}

		return null;
	}

	public PriceService getPriceService()
	{
		return priceService;
	}

	public void setPriceService(final PriceService priceService)
	{
		this.priceService = priceService;
	}

	public PriceDataFactory getPriceDataFactory()
	{
		return priceDataFactory;
	}

	public void setPriceDataFactory(final PriceDataFactory priceDataFactory)
	{
		this.priceDataFactory = priceDataFactory;
	}

	public WileySubscriptionService getSubscriptionService()
	{
		return subscriptionService;
	}

	public void setSubscriptionService(final WileySubscriptionService subscriptionService)
	{
		this.subscriptionService = subscriptionService;
	}

	public void setCartService(final CartService cartService)
	{
		this.cartService = cartService;
	}
}
