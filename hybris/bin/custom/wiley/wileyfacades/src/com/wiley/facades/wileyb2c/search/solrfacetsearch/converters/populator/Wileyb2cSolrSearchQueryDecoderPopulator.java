package com.wiley.facades.wileyb2c.search.solrfacetsearch.converters.populator;

import de.hybris.platform.commercefacades.search.data.SearchQueryData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryTermData;
import de.hybris.platform.converters.Populator;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;


public class Wileyb2cSolrSearchQueryDecoderPopulator implements Populator<SearchQueryData, SolrSearchQueryData>
{

	public void populate(final SearchQueryData source, final SolrSearchQueryData target)
	{
		if (source != null && StringUtils.isNotEmpty(source.getValue()))
		{
			final String[] split = source.getValue().split("\\|");

			if (split.length > 0)
			{
				target.setFreeTextSearch(split[0]);
			}
			if (split.length > 1)
			{
				target.setSort(split[1]);
			}

			final List<SolrSearchQueryTermData> terms = new ArrayList<>();

			for (int i = 2; i < split.length; i++)
			{
				final SolrSearchQueryTermData termData = new SolrSearchQueryTermData();
				String[] termDataKeyValue = split[i].split(":");
				termData.setKey(termDataKeyValue[0]);
				termData.setValue(termDataKeyValue[1]);
				terms.add(termData);
			}

			target.setFilterTerms(terms);
		}
	}
}
