package com.wiley.facades.as.order;

import de.hybris.platform.basecommerce.enums.CancelReason;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.ordercancel.OrderCancelException;

import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public interface WileyasCancelOrderFacade {

    /**
     * Requests complete cancel operation on an Order.
     *
     * @param orderCode
     *          The id for the order to cancel
     * @param cancelReason
     *          The reason of cancellation
     * @return OrderCancelRecordEntryData that represents the request and the
     *         result of cancel operation.
     * @throws de.hybris.platform.ordercancel.OrderCancelException
     */
    CancelResult cancelOrder(String orderCode, @Nullable CancelReason cancelReason);

    List<CancelReason> getOrderCancelReasons();

    boolean isCancellable(String orderCode);

    void cancelOrderEntry(@Nonnull String orderCode, @Nonnull Integer entryNumber,
                          @Nullable CancelReason cancelReason) throws CalculationException, OrderCancelException;

    OrderData getOrderDetailsAfterEntryCancelled(String code, Integer entryNumber);

    boolean hasCancellableEntry(OrderEntryData orderEntryData);

}
