package com.wiley.facades.checkout.enums;

/**
 * All available breadcrumbs for ags and wel sites
 */
public enum BreadcrumbItem
{
	LOGIN,
	BILLING_AND_SHIPPING,
	BILLING,
	STUDENT_VERIFICATION,
	PAYMENT,
	ORDER_REVIEW,
	ORDER_CONFIRMATION;

	public String getLabelKey()
	{
		return "checkout.multi.breadcrumb." + this.name();
	}
}

