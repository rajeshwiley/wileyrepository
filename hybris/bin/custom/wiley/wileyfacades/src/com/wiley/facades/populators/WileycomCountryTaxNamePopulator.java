package com.wiley.facades.populators;

import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CountryModel;


/**
 * Created by Georgii_Gavrysh on 8/24/2016.
 */
public class WileycomCountryTaxNamePopulator implements Populator<CountryModel, CountryData>
{

	@Override
	public void populate(final CountryModel source, final CountryData target)
	{
		target.setTaxName(source.getTaxName());
	}
}
