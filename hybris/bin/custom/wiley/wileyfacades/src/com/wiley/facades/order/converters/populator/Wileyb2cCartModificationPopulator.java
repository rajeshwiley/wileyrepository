package com.wiley.facades.order.converters.populator;

import de.hybris.platform.commercefacades.order.converters.populator.CartModificationPopulator;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.subscriptionfacades.data.SubscriptionTermData;
import de.hybris.platform.subscriptionservices.model.SubscriptionTermModel;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang.StringUtils;

import com.wiley.core.product.WileySubscriptionTermService;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


/**
 * Custom CartModificationPopulator for B2C site.
 */
public class Wileyb2cCartModificationPopulator extends CartModificationPopulator
{
	@Resource
	private Converter<SubscriptionTermModel, SubscriptionTermData> subscriptionTermConverter;

	@Resource
	private WileySubscriptionTermService wileySubscriptionTermService;

	@Override
	public void populate(@NotNull final CommerceCartModification source, @NotNull final CartModificationData target)
	{
		validateParameterNotNullStandardMessage("source", source);
		validateParameterNotNullStandardMessage("target", target);

		super.populate(source, target);

		final AbstractOrderEntryModel entry = source.getEntry();
		final OrderEntryData entryData = target.getEntry();
		if (entry != null && entryData != null)
		{
			final String originalSubscriptionId = entry.getOriginalSubscriptionId();
			if (StringUtils.isNotBlank(originalSubscriptionId))
			{
				final SubscriptionTermModel originalSubscription = wileySubscriptionTermService.getSubscriptionTerm(
						originalSubscriptionId);
				entryData.setOriginalSubscriptionTerm(subscriptionTermConverter.convert(originalSubscription));
			}
			final SubscriptionTermModel subscriptionTerm = entry.getSubscriptionTerm();
			if (subscriptionTerm != null)
			{
				entryData.setSubscriptionTerm(subscriptionTermConverter.convert(subscriptionTerm));
			}
		}
	}
}
