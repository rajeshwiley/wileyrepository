package com.wiley.facades.payment;

import java.util.List;


public class PaymentAuthorizationResultData
{
	private boolean isSuccess;
	private String status;
	private List<String> errors;
	private String errorCode;

	private PaymentMethod payment;

	public enum PaymentMethod
	{
		WPG,
		PAYPAL,
		MPGS
	}

	public static PaymentAuthorizationResultData success()
	{
		return new PaymentAuthorizationResultData(true, null, null, null, null)
		{
			@Override
			public String getStatus()
			{
				throw new UnsupportedOperationException("Get status is not supported by SUCCESS authorization result");
			}

			@Override
			public List<String> getErrors()
			{
				throw new UnsupportedOperationException("Get errors is not supported by SUCCESS authorization result");
			}
		};
	}

	public static PaymentAuthorizationResultData failure(final String status, final String errorCode, final PaymentMethod payment)
	{
		return new PaymentAuthorizationResultData(false, status, null, errorCode, payment);
	}

	public static PaymentAuthorizationResultData failure(final String status)
	{
		return new PaymentAuthorizationResultData(false, status, null, null, null);
	}

	public static PaymentAuthorizationResultData failure(final String status, final List<String> errors,
			final PaymentMethod payment)
	{
		return new PaymentAuthorizationResultData(false, status, errors, null, payment);
	}


	PaymentAuthorizationResultData(final boolean isSuccess, final String status, final List<String> errors,
			final String errorCode, final PaymentMethod payment)
	{
		this.isSuccess = isSuccess;
		this.status = status;
		this.errors = errors;
		this.errorCode = errorCode;
		this.payment = payment;
	}

	public boolean isSuccess()
	{
		return isSuccess;
	}

	public String getStatus()
	{
		return status;
	}

	public List<String> getErrors()
	{
		return errors;
	}

	public String getErrorCode()
	{
		return errorCode;
	}

	public PaymentMethod getPaymentMethod()
	{
		return payment;
	}
}