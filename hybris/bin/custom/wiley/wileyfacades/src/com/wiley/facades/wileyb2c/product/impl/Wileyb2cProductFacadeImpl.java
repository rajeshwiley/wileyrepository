package com.wiley.facades.wileyb2c.product.impl;

import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.commercefacades.product.data.ProductReferenceData;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;

import com.wiley.facades.product.impl.DefaultWileyProductFacade;
import com.wiley.facades.wileyb2c.product.Wileyb2cProductFacade;

import static com.wiley.facades.constants.WileyFacadesConstants.PRODUCT_REFERENCES_OPTIONS;


/**
 * Contains methods for getting product data-objects of wileyb2c site
 */
public class Wileyb2cProductFacadeImpl extends DefaultWileyProductFacade implements Wileyb2cProductFacade
{
	@Override
	public List<ProductReferenceData> getProductReferencesForCodes(final List<String> productCodes,
			final List<ProductReferenceTypeEnum> productReferenceTypes, final boolean isDisplayRelatedProductsDespiteRestriction,
			final int limit)
	{
		List<ProductReferenceData> result = new ArrayList<>();

		if (CollectionUtils.isNotEmpty(productCodes))
		{
			final Set<String> referenceTargetCodesSet = new HashSet<>();
			for (int i = 0; i < productCodes.size() && referenceTargetCodesSet.size() < limit; i++)
			{
				result.addAll(
						getProductReferencesForCode(productCodes.get(i), referenceTargetCodesSet, productReferenceTypes,
								isDisplayRelatedProductsDespiteRestriction, limit));
			}
		}

		return result;
	}


	protected List<ProductReferenceData> getProductReferencesForCode(final String code, final Set<String> referenceTargetCodesSet,
			final List<ProductReferenceTypeEnum> productReferenceTypes, final boolean isDisplayRelatedProductsDespiteRestriction,
			final Integer limit)
	{
		List<ProductReferenceData> result = new ArrayList<>();

		// get product references without limit as they might me filtered out after that
		List<ProductReferenceData> productReferencesForCode = getProductReferencesForCode(code, productReferenceTypes,
				PRODUCT_REFERENCES_OPTIONS, null);
		List<ProductReferenceData> visibleProductReferencesForCode = filterVisibleReferenceProducts(productReferencesForCode,
				isDisplayRelatedProductsDespiteRestriction);

		for (ProductReferenceData productReference : visibleProductReferencesForCode)
		{
			String referenceTargetCode = productReference.getTarget().getCode();
			if (referenceTargetCodesSet.add(referenceTargetCode))
			{
				result.add(productReference);
			}
			if (referenceTargetCodesSet.size() >= limit)
			{
				break;
			}
		}
		return result;
	}
}