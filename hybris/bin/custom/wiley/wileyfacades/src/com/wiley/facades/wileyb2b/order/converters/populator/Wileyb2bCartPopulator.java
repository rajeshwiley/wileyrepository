package com.wiley.facades.wileyb2b.order.converters.populator;

import de.hybris.platform.b2bacceleratorfacades.order.data.B2BPaymentTypeData;
import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.commercefacades.order.converters.populator.ExtendedCartPopulator;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.promotions.result.PromotionOrderResults;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;

import com.wiley.core.model.WileyExternalDiscountModel;
import com.wiley.facades.product.data.DiscountData;


/**
 * Wileyb2b specific cart populator.
 */
public class Wileyb2bCartPopulator extends ExtendedCartPopulator
{

	@Resource
	private Converter<CheckoutPaymentType, B2BPaymentTypeData> paymentTypeDataConverter;

	@Resource
	private Converter<WileyExternalDiscountModel, DiscountData> wileyb2bExternalDiscountToDiscountDataConverter;

	@Override
	public void populate(final CartModel source, final CartData target)
	{
		super.populate(source, target);

		if (source.getPaymentType() != null)
		{
			target.setPaymentType(paymentTypeDataConverter.convert(source.getPaymentType()));
		}
		target.setUserNotes(source.getUserNotes());
		target.setDesiredShippingDate(source.getDesiredShippingDate());
		target.setSubTotalWithoutDiscount(createPrice(source, source.getSubtotal()));

		target.setDiscountData(
				Converters.convertAll(source.getExternalDiscounts(), wileyb2bExternalDiscountToDiscountDataConverter));
	}

	@Override
	protected Double calcTotalWithTax(final AbstractOrderModel source)
	{
		/*According to IDD https://confluence.wiley.ru/display/ECSC/P2+-+hy-029+External+cart+calculation
		 * Order's total price already includes total tax.*/
		return source.getTotalPrice();
	}

	@Override
	protected void addPromotions(final AbstractOrderModel source, final AbstractOrderData prototype)
	{
		addPromotions(source, null, prototype);
	}

	@Override
	protected void addPromotions(final AbstractOrderModel source, final PromotionOrderResults promoOrderResults,
			final AbstractOrderData prototype)
	{
		final double productsDiscountsAmount = getProductsDiscountsAmount(source);
		final double orderDiscountsAmount = getOrderDiscountsAmount(source);

		prototype.setProductDiscounts(createPrice(source, productsDiscountsAmount));
		prototype.setOrderDiscounts(createPrice(source, orderDiscountsAmount));
		prototype.setTotalDiscounts(createPrice(source, source.getTotalDiscounts()));
	}

	@Override
	protected void addTotals(final AbstractOrderModel source, final AbstractOrderData prototype)
	{
		prototype.setTotalPrice(createPrice(source, source.getTotalPrice()));
		prototype.setTotalTax(createPrice(source, source.getTotalTax()));
		prototype.setSubTotal(createPrice(source, source.getSubtotal()));
		// todo[foundation_phase2] ability to support 'showDeliveryCost' flag on b2b (for cart entry and order entry)
		prototype.setDeliveryCost(source.getDeliveryMode() != null ? createPrice(source, source.getDeliveryCost()) : null);
		prototype.setTotalPriceWithTax((createPrice(source, calcTotalWithTax(source))));
	}
}
