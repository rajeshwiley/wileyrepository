package com.wiley.facades.cms.impl;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.ProductSearchFacade;
import de.hybris.platform.commercefacades.search.data.SearchQueryData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.search.facetdata.ProductCategorySearchPageData;
import de.hybris.platform.commerceservices.search.facetdata.ProductSearchPageData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.model.WileyProductModel;
import com.wiley.core.model.components.WileyProductListComponentModel;
import com.wiley.facades.cms.WileyProductListComponentFacade;
import com.wiley.facades.wiley.visibility.WileyProductVisibilityFacade;


/**
 * Created by Uladzimir_Barouski on 5/22/2017.
 */
public class WileyProductListComponentFacadeImpl implements WileyProductListComponentFacade
{
	protected static final List<ProductOption> PRODUCT_OPTIONS = Arrays.asList(ProductOption.BASIC,
			ProductOption.URL, ProductOption.DESCRIPTION, ProductOption.GALLERY, ProductOption.AUTHOR_INFOS_WITHOUT_ROLE,
			ProductOption.VARIANT_FULL, ProductOption.VARIANT_MATRIX_PRICE, ProductOption.SHOW_RELATED_PRODUCTS_DISCLAIMER);

	private ProductFacade productFacade;

	private ProductSearchFacade<ProductData> productSearchFacade;

	private WileyProductVisibilityFacade wileyProductVisibilityFacade;

	@Override
	public List<ProductData> getComponentProducts(final CategoryModel category, final WileyProductListComponentModel component)
	{

		final List<ProductData> products = new ArrayList<>();
		final List<ProductData> manualProducts = collectLinkedProducts(component);
		products.addAll(manualProducts);

		final int numberOfProductsToFind = component.getTotalProducts() - manualProducts.size();
		if (StringUtils.isNotEmpty(component.getSearchQuery()) && numberOfProductsToFind > 0)
		{
			final List<ProductData> searchProducts = collectSearchProducts(category, component.getSearchQuery(),
					numberOfProductsToFind);
			products.addAll(collectUniqSearchProducts(manualProducts, searchProducts));
		}

		return products;
	}

	private List<ProductData> collectUniqSearchProducts(final List<ProductData> manualProducts,
			final List<ProductData> searchProducts)
	{
		final List<ProductData> uniqSearchProducts = new ArrayList<>(searchProducts);
		if (CollectionUtils.isNotEmpty(searchProducts))
		{
			for (ProductData searchProduct : searchProducts)
			{
				for (ProductData manualProduct : manualProducts)
				{
					if (manualProduct.getCode().equals(searchProduct.getBaseProduct()))
					{
						uniqSearchProducts.remove(searchProduct);
					}
				}
			}
		}
		return uniqSearchProducts;
	}

	private List<ProductData> collectSearchProducts(final CategoryModel category, final String searchQuery,
			int numberOfProductsToFind)
	{
		final List<ProductData> products = new ArrayList<>();

		final SearchQueryData searchQueryData = new SearchQueryData();
		searchQueryData.setValue(searchQuery);

		final SearchStateData searchState = new SearchStateData();
		searchState.setQuery(searchQueryData);

		final PageableData pageableData = new PageableData();
		pageableData.setPageSize(numberOfProductsToFind);

		if (category != null)
		{
			final ProductCategorySearchPageData<SearchStateData, ProductData, CategoryData> searchResult =
					productSearchFacade.categorySearch(category.getCode(), searchState, pageableData);
			if (searchResult != null && searchResult.getResults() != null)
			{
				products.addAll(searchResult.getResults());
			}
		}
		else
		{
			final ProductSearchPageData<SearchStateData, ProductData> searchResult = productSearchFacade.textSearch(
					searchState,
					pageableData);
			if (searchResult != null && searchResult.getResults() != null)
			{
				products.addAll(searchResult.getResults());
			}
		}
		return products;
	}

	private List<ProductData> collectLinkedProducts(final WileyProductListComponentModel component)
	{
		final List<ProductData> products = new ArrayList<>();
		final List<ProductModel> manualProducts = filterProductsByVisibility(component.getProducts());
		if (CollectionUtils.isNotEmpty(manualProducts))
		{
			final List<ProductModel> productsToAdd = manualProducts.size() > component.getTotalProducts() ?
					manualProducts.subList(0, component.getTotalProducts()) : manualProducts;
			products.addAll(productsToAdd.stream()
					.map(product -> productFacade.getProductForCodeAndOptions(product.getCode(), PRODUCT_OPTIONS))
					.collect(Collectors.toList()));
		}
		return products;
	}

	private List<ProductModel> filterProductsByVisibility(final List<WileyProductModel> products)
	{
		return products.stream()
				.map(product -> wileyProductVisibilityFacade.filterRestrictedProductVariants(product))
				.filter(productWithFilteredVariant -> CollectionUtils.isNotEmpty(productWithFilteredVariant.getVariants()))
				.collect(Collectors.toList());
	}

	@Required
	public void setProductFacade(final ProductFacade productFacade)
	{
		this.productFacade = productFacade;
	}

	@Required
	public void setProductSearchFacade(
			final ProductSearchFacade<ProductData> productSearchFacade)
	{
		this.productSearchFacade = productSearchFacade;
	}

	@Required
	public void setWileyProductVisibilityFacade(final WileyProductVisibilityFacade wileyProductVisibilityFacade)
	{
		this.wileyProductVisibilityFacade = wileyProductVisibilityFacade;
	}
}
