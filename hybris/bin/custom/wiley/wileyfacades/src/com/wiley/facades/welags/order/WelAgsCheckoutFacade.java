package com.wiley.facades.welags.order;

import de.hybris.platform.commercefacades.order.data.DeliveryModeData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.List;

import com.wiley.core.enums.OrderType;
import com.wiley.facades.order.WileyCheckoutFacade;
import com.wiley.facades.product.exception.RepeatedFreeTrialOrderException;
import com.wiley.facades.welags.pin.exception.PinOperationException;


public interface WelAgsCheckoutFacade extends WileyCheckoutFacade
{
	boolean isPaymentAuthorized();

	void updateCountryInAddress(String countryIsocode, boolean isShippingSameAsBilling);

	void updateTaxInCart(String countryIsocode, String regionIsocode, boolean isShippingSameAsBilling);

	void updateBillingAndShippingAddresses(AddressData billingAddress, AddressData shippingAddress,
			boolean treatBillingAsShipping);

	void updateDefaultAddress(AddressData addressData);

	/**
	 * Retrieves available delivery countries for persisted session cart for country code.
	 * If country code is incorrect, returns empty list
	 *
	 * @return - available delivery modes
	 */
	List<DeliveryModeData> getPotentialDeliveryModesForCountry(String countryIsoCode);

	/**
	 * Checks if current cart price is not Zero
	 *
	 * @return - true if current cart total is Zero
	 */
	boolean isNonZeroPriceCart();

	/**
	 * Returns true if passed cart associated with the cart code contains only digital entries. False in other case.
	 *
	 * @return - boolean
	 */
	boolean isDigitalCart(String cartCode);

	boolean isZeroOrder(String cartCode);

	boolean isPreOrderCart();

	boolean isPreOrderOrder(String orderCode);

	/**
	 * Method marks customer's session cart to subscribe the customer to updates during fulfilment.<br/>
	 * Does nothing if session cart doesn't exist.
	 */
	void subscribeCurrentCustomerToUpdates();

	/**
	 * Method marks customer's session cart to unsubscribe the customer from updates during fulfilment.<br/>
	 * Does nothing if session cart doesn't exist.
	 */
	void unsubscribeCurrentCustomerFromUpdates();

	void subscribeToGMACInfo(boolean subscribe);

	/**
	 * @param countryIsoForDeliveryModes
	 */
	void setDeliveryModeIfNotAvailable(String countryIsoForDeliveryModes);

	/**
	 * Set appropriate delivery mode if session cart exists and delivery mode is not set to cart.<br/>
	 * Using default shipping address of current user to find delivery mode.
	 */
	void setDeliveryModeIfNotAvailable();

	/**
	 * Place a PIN order.
	 *
	 * @param pinCode
	 * 		the PIN code
	 * @param orderType
	 * 		the OrderType
	 * @return the order code, can be null
	 * @throws IllegalAccessException
	 * 		is thrown when Anonymous user is used to place order
	 * @throws InvalidCartException
	 * 		is thrown by underlying cart validator
	 * @throws PinOperationException
	 * 		is thrown when PIN cannot be used for the given cart
	 */
	String placePinOrder(String pinCode, OrderType orderType)
			throws IllegalAccessException, InvalidCartException, PinOperationException;

	/**
	 * Place a Free Trial order.
	 *
	 * @param freeTrialCode
	 * 		the Free Trial code
	 * @return placed order model
	 * @throws IllegalAccessException
	 * 		is thrown when Anonymous user is used to place order
	 * @throws InvalidCartException
	 * 		is thrown by underlying cart validator
	 * @throws UnknownIdentifierException
	 * 		is thrown when Free Trial product with passed freeTrialCode not found
	 * @throws CommerceCartModificationException
	 * 		is thrown by addToCartCommerceStrategy
	 * @throws RepeatedFreeTrialOrderException
	 * 		is thrown when current user has already have order with the same free trial product
	 */
	OrderModel placeFreeTrialOrder(String freeTrialCode, boolean subscribeToUpdates, OrderType type)
			throws UnknownIdentifierException, IllegalAccessException, InvalidCartException, CommerceCartModificationException,
			RepeatedFreeTrialOrderException;

	/**
	 * Method checks is delivery address is set to session cart.
	 *
	 * @return true is session cart exists and delivery address is assigned to the cart otherwise false.
	 */
	boolean isDeliveryAddressSetToCart();

	/**
	 * @return breadcrumb labels for current checkout flow
	 */
	List<String> getBreadcrumbLabels();

	/**
	 * @param cartCode
	 * @return breadcrumb labels for current checkout flow
	 */
	List<String> getBreadcrumbLabels(String cartCode);

	boolean clearCartForPreOrderProduct(String productCode);
}
