package com.wiley.facades.wileyb2b.customer;


import com.wiley.facades.customer.WileycomCustomerFacade;


/**
 * Wiley B2B specific customer facade interface.
 */
public interface Wileyb2bCustomerFacade extends WileycomCustomerFacade
{
}
