//Created comparator for ECSC-29467.
package com.wiley.facades.populators.comparators;

import de.hybris.platform.commercefacades.product.data.ProductData;

import java.util.Comparator;



public class FreeTrialComparator implements Comparator<ProductData> {
    public int compare(final ProductData freeTrial0, final ProductData freeTrial1) {
        
        return freeTrial0.getName().compareTo(freeTrial1.getName());
    }

}