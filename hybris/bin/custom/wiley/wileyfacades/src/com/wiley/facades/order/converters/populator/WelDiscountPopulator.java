/**
 *
 */
package com.wiley.facades.order.converters.populator;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.europe1.enums.UserDiscountGroup;

import java.util.Optional;

import org.springframework.util.Assert;

import com.wiley.core.model.WileyPartnerCompanyModel;


public class WelDiscountPopulator implements Populator<AbstractOrderModel, AbstractOrderData>
{
	@Override
	public void populate(final AbstractOrderModel source, final AbstractOrderData target)
	{

		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		final Optional<WileyPartnerCompanyModel> optionalWileyPartner = Optional.ofNullable(source.getWileyPartner());
		optionalWileyPartner.ifPresent(partner -> target.setPartnerId(partner.getUid()));

		final Optional<UserDiscountGroup> optionalDiscountGroup = Optional.ofNullable(source.getDiscountGroup());
		optionalDiscountGroup.ifPresent(discountGroup -> target.setDiscountGroupCode(discountGroup.getCode()));

	}
}
