package com.wiley.facades.wiley.session.impl;

import de.hybris.platform.commercefacades.order.data.CartModificationData;

import java.util.List;

import javax.annotation.Resource;

import com.wiley.core.wiley.session.storage.WileyFailedCartModificationsStorageService;
import com.wiley.facades.wiley.session.WileyFailedModificationsFacade;


/**
 * @author Dzmitryi_Halahayeu
 */
public class WileyFailedModificationsFacadeImpl implements WileyFailedModificationsFacade
{
	@Resource
	private WileyFailedCartModificationsStorageService wileyFailedCartModificationsStorageService;

	@Override
	public List<CartModificationData> popAll()
	{
		return wileyFailedCartModificationsStorageService.popAllData();
	}
}
