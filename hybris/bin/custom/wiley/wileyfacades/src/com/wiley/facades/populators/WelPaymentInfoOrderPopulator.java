package com.wiley.facades.populators;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.WileyPaymentInfoData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;


public class WelPaymentInfoOrderPopulator extends WileyPaymentInfoOrderPopulator
		implements Populator<AbstractOrderModel, AbstractOrderData>
{

	@Override
	public void populate(final AbstractOrderModel abstractOrderModel, final AbstractOrderData abstractOrderData)
			throws ConversionException
	{
		if (abstractOrderData.getWileyPaymentInfo() == null)
		{
			abstractOrderData.setWileyPaymentInfo(new WileyPaymentInfoData());
		}
		if (abstractOrderModel.getPaymentInfo() != null && abstractOrderModel.getPaymentInfo().getBillingAddress() != null)
		{
			final AddressModel billingAddress = abstractOrderModel.getPaymentInfo().getBillingAddress();
			abstractOrderData.getWileyPaymentInfo().setBillingAddress(addressConverter.convert(billingAddress));
		}
		populatePaymentType(abstractOrderModel, abstractOrderData);
	}

}
