package com.wiley.facades.order.converters.populator;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.model.WileyTnsMerchantModel;
import com.wiley.core.mpgs.services.strategies.WileyMPGSMerchantIdStrategy;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


public class WileyAbstractOrderHostedSessionScriptUrlPopulator implements Populator<AbstractOrderModel, AbstractOrderData>
{
	private WileyMPGSMerchantIdStrategy wileyMPGSMerchantIdStrategy;

	@Override
	public void populate(final AbstractOrderModel source, final AbstractOrderData target) throws ConversionException
	{
		validateParameterNotNullStandardMessage("source", source);
		validateParameterNotNullStandardMessage("target", target);
		final WileyTnsMerchantModel wileyTnsMerchantModel = wileyMPGSMerchantIdStrategy.getMerchant(source);
		target.setHostedSessionScriptUrl(
				wileyMPGSMerchantIdStrategy.getHostedSessionScriptUrl(wileyTnsMerchantModel.getId()));
	}

	@Required
	public void setWileyMPGSMerchantIdStrategy(
			final WileyMPGSMerchantIdStrategy wileyMPGSMerchantIdStrategy)
	{
		this.wileyMPGSMerchantIdStrategy = wileyMPGSMerchantIdStrategy;
	}
}
