package com.wiley.facades.wel.order.impl;


import com.paypal.hybris.facade.PayPalCheckoutFacade;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;


import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;

import com.paypal.hybris.facade.data.PayPalPaymentResultData;
import com.paypal.hybris.model.PaypalPaymentInfoModel;
import com.wiley.core.mpgs.services.WileyMPGSMessageService;
import com.wiley.core.mpgs.services.WileyMPGSPaymentService;
import com.wiley.core.product.WileyProductEditionFormatService;
import com.wiley.facades.checkout.enums.BreadcrumbItem;
import com.wiley.facades.payment.PaymentAuthorizationResultData;
import com.wiley.facades.welags.order.impl.WelAgsCheckoutFacadeImpl;

import static com.wiley.facades.payment.PaymentAuthorizationResultData.success;


public class WelCheckoutFacadeImpl extends WelAgsCheckoutFacadeImpl
{
	@Autowired
	private WileyMPGSPaymentService wileyMPGSPaymentService;
	@Autowired
	private WileyMPGSMessageService wileyMPGSErrorService;
	@Resource
	private WileyProductEditionFormatService wileyProductEditionFormatService;
	@Autowired
	private PayPalCheckoutFacade payPalCheckoutFacade;

	@Override
	@Nonnull
	public PaymentAuthorizationResultData authorizePaymentAndProvideResult(final String securityCode)
	{
		final CartModel cartModel = getCart();

		if (checkIfCurrentUserIsTheCartUser())
		{
			if (isZeroTotalOrder())
			{
				return PaymentAuthorizationResultData.success();
			}
			final PaymentInfoModel infoModel = cartModel.getPaymentInfo();
			if (infoModel instanceof PaypalPaymentInfoModel)
			{
				final PayPalPaymentResultData payPalPaymentResultData = payPalCheckoutFacade.authorizePaymentAndProvideResult(
						securityCode);

				return payPalPaymentResultData.isSuccess() ? success() : buildPaymentAuthorizationResultData(
						payPalPaymentResultData, PaymentAuthorizationResultData.PaymentMethod.PAYPAL);
			}
			else
			{
				final PaymentTransactionEntryModel authorizeEntry = wileyMPGSPaymentService.authorize(cartModel);
				final boolean isAuthorizedSuccessfully = TransactionStatus.ACCEPTED.name()
						.equals(authorizeEntry.getTransactionStatus());
				return isAuthorizedSuccessfully ?
						PaymentAuthorizationResultData.success() :
						PaymentAuthorizationResultData.failure(authorizeEntry.getTransactionStatus(),
								wileyMPGSErrorService.getAuthorizationStorefrontMessageKey(authorizeEntry),
								PaymentAuthorizationResultData.PaymentMethod.MPGS);
			}
		}

		return PaymentAuthorizationResultData.failure(TransactionStatus.ERROR.name());
	}


	protected List<String> getBreadcrumbLabels(final AbstractOrderModel abstractOrderModel)
	{
		List<BreadcrumbItem> breadcrumbLabels = new ArrayList<>();
		breadcrumbLabels.add(BreadcrumbItem.LOGIN);
		breadcrumbLabels.add(wileyProductEditionFormatService.isDigitalCart(abstractOrderModel) ?
				BreadcrumbItem.BILLING :
				BreadcrumbItem.BILLING_AND_SHIPPING);
		if (wileyCheckoutService.isStudentOrder(abstractOrderModel))
		{
			breadcrumbLabels.add(BreadcrumbItem.STUDENT_VERIFICATION);
		}
		breadcrumbLabels.add(BreadcrumbItem.PAYMENT);
		breadcrumbLabels.add(BreadcrumbItem.ORDER_REVIEW);
		breadcrumbLabels.add(BreadcrumbItem.ORDER_CONFIRMATION);
		return breadcrumbLabels.stream().map(BreadcrumbItem::getLabelKey).collect(Collectors.toList());
	}


}
