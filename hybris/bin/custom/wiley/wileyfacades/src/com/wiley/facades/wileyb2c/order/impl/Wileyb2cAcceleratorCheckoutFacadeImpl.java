package com.wiley.facades.wileyb2c.order.impl;

import de.hybris.platform.acceleratorfacades.order.impl.DefaultAcceleratorCheckoutFacade;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.voucher.VoucherService;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;

import com.wiley.core.product.WileyProductEditionFormatService;


public class Wileyb2cAcceleratorCheckoutFacadeImpl extends DefaultAcceleratorCheckoutFacade
{
	@Resource
	private WileyProductEditionFormatService wileyProductEditionFormatService;

	@Resource
	private VoucherService voucherService;

	@Override
	public boolean hasShippingItems()
	{
  	  CartModel cart = getCart();
  	  if (cart != null && CollectionUtils.isNotEmpty(cart.getEntries()))
  	  {
    		if (!wileyProductEditionFormatService.isDigitalCart(cart))
    		{
    			return true;
    		}
    	}
  	  return false;
	}

	@Override
	protected void afterPlaceOrder(final CartModel cartModel, final OrderModel orderModel)
	{
		// Hybris default implementation doesn't create a voucher invalidation record,
		// which is needed for global and per user voucher redeem quantity limit validation.
		voucherService.afterOrderCreation(orderModel, cartModel);

		super.afterPlaceOrder(cartModel, orderModel);
	}
}
