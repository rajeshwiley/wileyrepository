package com.wiley.facades.as.i18n.comparators;

import de.hybris.platform.commercefacades.user.data.RegionData;

import java.util.Comparator;


public class RegionIsocodeComparator implements Comparator<RegionData>
{
	private static final Comparator<RegionData> COMPARATOR =
			Comparator.comparing(RegionData::getIsocode, Comparator.nullsLast(Comparator.naturalOrder()));

	@Override
	public int compare(final RegionData o1, final RegionData o2)
	{
		return COMPARATOR.compare(o1, o2);
	}
}
