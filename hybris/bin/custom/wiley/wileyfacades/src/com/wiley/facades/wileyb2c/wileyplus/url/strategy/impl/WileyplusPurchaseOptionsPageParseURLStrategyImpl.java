package com.wiley.facades.wileyb2c.wileyplus.url.strategy.impl;


import java.io.IOException;
import java.net.URISyntaxException;

import javax.annotation.Resource;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wiley.facades.wileyb2c.wileyplus.url.strategy.WileyplusPurchaseOptionsPageChecksumStrategy;
import com.wiley.facades.wileyb2c.wileyplus.url.strategy.WileyplusPurchaseOptionsPageParseURLStrategy;
import com.wiley.facades.wileyb2c.wileyplus.url.strategy.exception.WileyplusChecksumMismatchException;
import com.wiley.facades.wileyplus.cart.activation.dto.CartActivationRequestDto;


public class WileyplusPurchaseOptionsPageParseURLStrategyImpl implements WileyplusPurchaseOptionsPageParseURLStrategy
{
	private static final Logger LOG = Logger.getLogger(WileyplusPurchaseOptionsPageParseURLStrategyImpl.class);

	@Resource
	private WileyplusPurchaseOptionsPageChecksumStrategy wileyplusPurchaseOptionsPageChecksumStrategy;

	@Override
	public CartActivationRequestDto parseCartActivationParams(final String encodedCartActivationInfo, final String checksum)
			throws URISyntaxException, WileyplusChecksumMismatchException
	{
		final String cartActivationInfo = new String(Base64.decodeBase64(encodedCartActivationInfo));
		wileyplusPurchaseOptionsPageChecksumStrategy.validateChecksum(cartActivationInfo, checksum);

		try
		{
			return new ObjectMapper().readValue(cartActivationInfo, CartActivationRequestDto.class);
		}
		catch (IOException e)
		{
			LOG.error(e);
		}
		return null;
	}
}
