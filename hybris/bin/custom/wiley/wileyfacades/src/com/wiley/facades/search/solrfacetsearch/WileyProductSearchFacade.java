package com.wiley.facades.search.solrfacetsearch;

public interface WileyProductSearchFacade
{
	String cleanupDashesFromSearchIsbn(String isbn);
}
