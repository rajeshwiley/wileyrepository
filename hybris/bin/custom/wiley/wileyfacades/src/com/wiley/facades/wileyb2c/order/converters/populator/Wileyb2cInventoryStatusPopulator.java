/**
 * 
 */
package com.wiley.facades.wileyb2c.order.converters.populator;

import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commerceservices.stock.CommerceStockService;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.wiley.facades.product.data.InventoryStatusRecord;
import com.wiley.facades.wileyb2c.util.Wileyb2cProductInventoryStatusCalculationUtils;


/**
 * Populates Inventory Status for the order entry
 *
 */
public class Wileyb2cInventoryStatusPopulator implements Populator<AbstractOrderEntryModel, OrderEntryData>
{
	@Resource(name = "commerceStockService")
	private CommerceStockService stockService;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;
	
	
	@Override
	public void populate(@Nonnull final AbstractOrderEntryModel source, @Nonnull final OrderEntryData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		//define parameters required for Inventory Status calculation
		final ProductModel product = source.getProduct();
		final Long requestedQuantity = source.getQuantity();
		final Long availableQuantity = stockService.getStockLevelForProductAndBaseStore(product,
				getBaseStoreService().getCurrentBaseStore());

		//calculation logic moved to strategy
		final List<InventoryStatusRecord> inventoryStatusForThisEntry = Wileyb2cProductInventoryStatusCalculationUtils
			.calculateInventoryStatusForCartProduct(product, requestedQuantity, availableQuantity);
		
		
		target.getProduct().setInventoryStatus(inventoryStatusForThisEntry);
	}

	protected CommerceStockService getStockService()
	{
		return stockService;
	}

	protected BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}
}
