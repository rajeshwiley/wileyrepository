/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.facades.process.email.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.util.Config;

import java.text.SimpleDateFormat;
import java.util.Map;

import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.product.WileyProductEditionFormatService;


/**
 * Velocity context for a order notification email.
 */
public class OrderNotificationEmailContext extends AbstractEmailContext<OrderProcessModel>
{
	private static final String WEL_EFFICIENTLEARNING_URL_HTTP = "wel.efficientlearning.url.http";
	private WileyProductEditionFormatService editionFormatService;
	private Converter<OrderModel, OrderData> orderConverter;
	private Map<String, String> siteAwareLinks;
	private String externalBaseUrl = "#";
	private OrderData orderData;
	private String orderCreationDate;
	private Boolean isPhysicalOrder;
	private String efficientLearningLink;


	/**
	 * Init.
	 *
	 * @param orderProcessModel
	 * 		the order process model
	 * @param emailPageModel
	 * 		the email page model
	 */
	@Override
	public void init(final OrderProcessModel orderProcessModel, final EmailPageModel emailPageModel)
	{
		super.init(orderProcessModel, emailPageModel);
		orderData = getOrderConverter().convert(orderProcessModel.getOrder());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		setOrderCreationDate(sdf.format(orderData.getCreated()));
		setIsPhysicalOrder(Boolean.valueOf(!getEditionFormatService().isDigitalCart(orderProcessModel.getOrder())));
		setEfficientLearningLink(Config.getParameter(WEL_EFFICIENTLEARNING_URL_HTTP));
		// set external base url
		if (siteAwareLinks.containsKey(getSite(orderProcessModel).getUid()))
		{
			externalBaseUrl = siteAwareLinks.get(getSite(orderProcessModel).getUid());
		}
	}

	/**
	 * Gets site.
	 *
	 * @param orderProcessModel
	 * 		the order process model
	 * @return the site
	 */
	@Override
	protected BaseSiteModel getSite(final OrderProcessModel orderProcessModel)
	{
		return orderProcessModel.getOrder().getSite();
	}

	/**
	 * Gets customer.
	 *
	 * @param orderProcessModel
	 * 		the order process model
	 * @return the customer
	 */
	@Override
	protected CustomerModel getCustomer(final OrderProcessModel orderProcessModel)
	{
		return (CustomerModel) orderProcessModel.getOrder().getUser();
	}

	/**
	 * Gets order converter.
	 *
	 * @return the order converter
	 */
	protected Converter<OrderModel, OrderData> getOrderConverter()
	{
		return orderConverter;
	}

	public String getExternalBaseUrl()
	{
		return externalBaseUrl;
	}

	/**
	 * Sets order converter.
	 *
	 * @param orderConverter
	 * 		the order converter
	 */
	@Required
	public void setOrderConverter(final Converter<OrderModel, OrderData> orderConverter)
	{
		this.orderConverter = orderConverter;
	}

	/**
	 * Gets order.
	 *
	 * @return the order
	 */
	public OrderData getOrder()
	{
		return orderData;
	}

	/**
	 * Gets email language.
	 *
	 * @param orderProcessModel
	 * 		the order process model
	 * @return the email language
	 */
	@Override
	protected LanguageModel getEmailLanguage(final OrderProcessModel orderProcessModel)
	{
		return orderProcessModel.getOrder().getLanguage();
	}

	/**
	 * @return the orderCreationDate
	 */
	public String getOrderCreationDate()
	{
		return orderCreationDate;
	}

	/**
	 * @param orderCreationDate
	 * 		the orderCreationDate to set
	 */
	public void setOrderCreationDate(final String orderCreationDate)
	{
		this.orderCreationDate = orderCreationDate;
	}

	/**
	 * @return the isPhysicalOrder
	 */
	public Boolean getIsPhysicalOrder()
	{
		return isPhysicalOrder;
	}

	/**
	 * @param isPhysicalOrder
	 * 		the isPhysicalOrder to set
	 */
	public void setIsPhysicalOrder(final Boolean isPhysicalOrder)
	{
		this.isPhysicalOrder = isPhysicalOrder;
	}

	/**
	 * @return the editionFormatService
	 */
	public WileyProductEditionFormatService getEditionFormatService()
	{
		return editionFormatService;
	}

	/**
	 * @param editionFormatService
	 * 		the editionFormatService to set
	 */
	public void setEditionFormatService(final WileyProductEditionFormatService editionFormatService)
	{
		this.editionFormatService = editionFormatService;
	}

	/**
	 * Gets efficient learning link.
	 *
	 * @return the efficient learning link
	 */
	public String getEfficientLearningLink()
	{
		return efficientLearningLink;
	}

	/**
	 * Sets efficient learning link.
	 *
	 * @param efficientLearningLink
	 * 		the efficient learning link
	 */
	public void setEfficientLearningLink(final String efficientLearningLink)
	{
		this.efficientLearningLink = efficientLearningLink;
	}
	
	 public void setSiteAwareLinks(final Map<String, String> siteAwareLinks)
	  {
	    this.siteAwareLinks = siteAwareLinks;
	  }

}
