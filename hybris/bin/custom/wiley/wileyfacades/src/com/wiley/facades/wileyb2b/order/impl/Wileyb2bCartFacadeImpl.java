package com.wiley.facades.wileyb2b.order.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.impl.DefaultCartFacade;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.util.DiscountValue;

import java.util.Collections;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.wiley.core.integration.ExternalCartModification;
import com.wiley.core.integration.ExternalCartModificationsStorageService;
import com.wiley.core.wileyb2b.order.impl.Wileyb2bCommerceCartCalculationStrategy;
import com.wiley.facades.order.data.CartModificationMessage;
import com.wiley.facades.wileyb2b.order.Wileyb2bCartFacade;


/**
 * Created by Aliaksei_Zlobich on 6/30/2016.
 */
public class Wileyb2bCartFacadeImpl extends DefaultCartFacade implements Wileyb2bCartFacade
{

	@Resource
	private ExternalCartModificationsStorageService externalCartModificationsStorageService;

	@Resource
	private Converter<ExternalCartModification, CartModificationMessage> wileyb2bCartModificationMessageConverter;

	@Resource
	private Wileyb2bCommerceCartCalculationStrategy wileyb2bCommerceCartCalculationStrategy;

	@Resource
	private ModelService modelService;

	@Nonnull
	@Override
	public List<CartModificationMessage> popCartModificationMessages()
	{
		List<CartModificationMessage> resultList = Collections.emptyList();

		if (getCartService().hasSessionCart())
		{
			final List<ExternalCartModification> externalCartModifications = externalCartModificationsStorageService.popAll(
					getCartService().getSessionCart());

			resultList = Converters.convertAll(externalCartModifications, wileyb2bCartModificationMessageConverter);
		}

		Assert.notNull(resultList);
		return resultList;
	}

	@Override
	public void setVoucherCodeInCart(final String voucherCode)
	{
		Assert.notNull(voucherCode, "Voucher code must be set");
		if (getCartService().hasSessionCart())
		{
			CartModel cart = getCartService().getSessionCart();
			updateAndRecalculateCart(cart, voucherCode, () -> Collections.singletonList(voucherCode));
		}
	}

	@Override
	public void removeVoucherCodeFromCart(final String voucherCode)
	{
		Assert.notNull(voucherCode, "Voucher code must be set");
		if (getCartService().hasSessionCart())
		{
			CartModel cart = getCartService().getSessionCart();
			/* TODO getExternalDiscounts should be used instead of getExternalDiscountValues
			updateAndRecalculateCart(cart, voucherCode, () ->
					cart.getExternalDiscountValues().stream().map(discount -> discount.getCode())
					.filter(code -> !voucherCode.equals(code))
					.collect(Collectors.toList()));
			*/

		}
	}

	private void setCartUnit(final CartModel cart)
	{
		UserModel customer = getUserService().getCurrentUser();
		if (customer instanceof B2BCustomerModel)
		{
			cart.setUnit(((B2BCustomerModel) customer).getDefaultB2BUnit());
		}
	}

	@Override
	public CartModificationData addToCart(final String code, final long quantity) throws CommerceCartModificationException
	{
		final ProductModel product = getProductService().getProductForCode(code);
		final CartModel cartModel = getCartService().getSessionCart();
		if (cartModel.getUnit() == null)
		{
			setCartUnit(cartModel);
		}
		final UserModel user = getUserService().getCurrentUser();
		final CommerceCartParameter parameter = new CommerceCartParameter();

		parameter.setEnableHooks(true);
		parameter.setCart(cartModel);
		parameter.setQuantity(quantity);
		parameter.setProduct(product);
		parameter.setUnit(product.getUnit());
		parameter.setCreateNewEntry(false);
		parameter.setUser(user);

		final CommerceCartModification modification = getCommerceCartService().addToCart(parameter);

		return getCartModificationConverter().convert(modification);
	}

	//TODO: this method and setting/removing voucher code logic should be rewritten in scope of b2b reanimating. 
	//For now current impl is outdated
	private void updateAndRecalculateCart(final CartModel cart, final String voucherCode, final Supplier<List<String>> supplier)
	{
		updateExternalDisocuntCodes(cart, supplier);
		CommerceCartParameter commerceCartParameter = new CommerceCartParameter();
		commerceCartParameter.setEnableHooks(true);
		commerceCartParameter.setCart(cart);
		wileyb2bCommerceCartCalculationStrategy.recalculateCart(commerceCartParameter);
		updateExternalDisocuntCodes(cart, () -> Stream.concat(
				cart.getEntries().stream().flatMap(entry -> entry.getDiscountValues().stream()),
				cart.getGlobalDiscountValues().stream())
				.map(discount -> discount.getCode())
				.filter(code -> voucherCode.equals(code))
				.collect(Collectors.toList()));
	}

	private void updateExternalDisocuntCodes(final CartModel cart, final Supplier<List<String>> supplier)
	{
		modelService.refresh(cart);
		final String values = String.join("|", supplier.get());
		final List<DiscountValue> discountValuesList = (List<DiscountValue>) DiscountValue
				.parseDiscountValueCollection(values);
		// TODO externalDiscountCode, externalDiscountValues are outdated. 
		//cart.setExternalDiscountValues(discountValuesList);
		modelService.save(cart);
	}
}
