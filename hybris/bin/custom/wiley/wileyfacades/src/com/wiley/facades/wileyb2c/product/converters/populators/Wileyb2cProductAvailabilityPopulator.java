package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.platform.commercefacades.product.converters.populator.AbstractProductPopulator;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;


import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import com.wiley.core.wiley.restriction.WileyProductRestrictionService;


public class Wileyb2cProductAvailabilityPopulator extends AbstractProductPopulator<ProductModel, ProductData>
{
	private WileyProductRestrictionService wileyProductRestrictionService;

	@Override
	public void populate(@Nonnull final ProductModel productModel, @Nonnull final ProductData productData)
			throws ConversionException
	{
		Assert.notNull(productModel);
		Assert.notNull(productData);

		productData.setAvailable(wileyProductRestrictionService.isAvailable(productModel));
	}

	@Required
	public void setWileyProductRestrictionService(final WileyProductRestrictionService wileyProductRestrictionService)
	{
		this.wileyProductRestrictionService = wileyProductRestrictionService;
	}
}
