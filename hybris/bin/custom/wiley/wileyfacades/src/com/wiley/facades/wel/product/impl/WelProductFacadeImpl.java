package com.wiley.facades.wel.product.impl;

import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.FeatureData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.VariantMatrixElementData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.enums.UserDiscountGroup;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.variants.model.VariantProductModel;
import de.hybris.platform.variants.model.VariantValueCategoryModel;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.model.WileyProductModel;
import com.wiley.core.model.WileyProductVariantSetModel;
import com.wiley.core.model.cms.CMSProductAttributeModel;
import com.wiley.core.product.WileyProductComparisonService;
import com.wiley.core.wiley.classification.WileyClassificationService;
import com.wiley.facades.constants.WileyFacadesConstants;
import com.wiley.facades.converters.WileyProductCompareTableCFAConverter;
import com.wiley.facades.partner.WileyPartnerFacade;
import com.wiley.facades.partner.strategies.WileyPartnerStrategy;
import com.wiley.facades.product.data.CFALevelData;
import com.wiley.facades.product.data.CompareTableData;
import com.wiley.facades.product.data.ProductComparisonData;
import com.wiley.facades.product.data.WileyProductListCFAData;
import com.wiley.facades.product.impl.DefaultWileyProductFacade;
import com.wiley.facades.product.node.ProductNode;
import com.wiley.facades.product.node.ProductRootNode;
import com.wiley.facades.wel.product.WelProductFacade;


/**
 * Contains methods for getting product data-objects of wel site
 */
public class WelProductFacadeImpl extends DefaultWileyProductFacade implements WelProductFacade
{
	protected static final Logger LOG = Logger.getLogger(WelProductFacadeImpl.class);

	private static final String WEL_CFA_SUPPLEMENTS = "WEL_CFA_SUPPLEMENTS";
	private static final String CFA_LEVEL_CATEGORY_REGEX = "WEL_CFA_LEVEL(.*)";
	private static final String EBOOK_PRINT_REGEX = "(?i)(\\(\\s*ebook\\s*\\)|\\(\\s*print\\s*\\))";
	private static final String COMPARISON_DATE_FORMAT = "MMMMM yyyy";
	private static final String MSG_NO_SUPPLEMENTS_FOR_CATEGORY = "There is no supplements for category ";

	private WileyProductCompareTableCFAConverter productCompareTableConverter;
	private ProductFacade productFacade;
	private WileyProductComparisonService productComparisonService;

	@Autowired
	private WileyPartnerFacade wileyPartnerFacade;

	@Autowired
	private WileyPartnerStrategy wileyPartnerStrategy;

	@Autowired
	private WileyClassificationService wileyClassificationService;

	@Resource
	private Converter<ClassAttributeAssignmentModel, FeatureData> wileyFeatureFromAssignmentConverter;

	/**
	 * Gets product list cfa data.
	 *
	 * @return the product list cfa data
	 */
	@Override
	public WileyProductListCFAData getProductListCFAData(final String categoryCode)
	{
		final WileyProductListCFAData cfaProductListData = new WileyProductListCFAData();
		cfaProductListData.setCompareTableData(getCompareTableData(categoryCode));
		cfaProductListData.setGridData(getGridData(categoryCode));
		return cfaProductListData;
	}

	@Override
	public Double getPriceForDefaultSetProduct(final ProductRootNode rootNode)
	{
		return getPriceForDefaultVariant(rootNode, productNodes ->
				productNodes.stream().filter(node -> node.isSetProduct()).findFirst());
	}

	protected Double getPriceForDefaultVariant(final ProductRootNode rootNode,
			final Function<List<ProductNode>, Optional<ProductNode>> variantSelector)
	{
		if (rootNode.getProduct() != null)
		{
			final List<ProductNode> productNodes = rootNode.getProduct().getNodes();
			if (CollectionUtils.isNotEmpty(productNodes))
			{
				// Variants in product can:
				// - have one level (just parts or just types)
				final List<ProductNode> firstLevelVariants = productNodes;
				// - have two levels. First level is types, second level is parts. Parts belong to types.
				//                    Variants assumed to be ordered, default one should have minimal index.
				final List<ProductNode> secondLevelVariants = firstLevelVariants.get(0).getNodes();
				final List<ProductNode> variants = secondLevelVariants != null ? secondLevelVariants : firstLevelVariants;
				if (CollectionUtils.isNotEmpty(variants))
				{
					final Optional<ProductNode> defaultVariant = variantSelector.apply(variants);
					if (defaultVariant.isPresent())
					{
						return defaultVariant.get().getValue().getPrice();
					}
				}
			}
		}
		return null;
	}

	@Override
	public Double getPriceForDefaultPart(final ProductRootNode rootNode)
	{
		return getPriceForDefaultVariant(rootNode, productNodes -> Optional.ofNullable(productNodes.get(0)));
	}

	/**
	 * Returns CompareTableData object for CFA Compare table
	 *
	 * @param categoryCode
	 * @return compare table data
	 */
	private CompareTableData getCompareTableData(final String categoryCode)
	{
		final List<ProductData> productDataList = new ArrayList<>();
		final List<WileyProductModel> products = getWileyProductService().getLatestWileyProductsFromCategoryRoot(categoryCode);
		for (final WileyProductModel productModel : products)
		{
			if (getWileyProductRestrictionService().isVisible(productModel))
			{
				final ProductModel productWithFilteredVariants =
						getWileyProductRestrictionService().filterRestrictedProductVariants(
								productModel);
				final ProductData productData = productFacade.getProductForOptions(productWithFilteredVariants,
						Arrays.asList(ProductOption.BASIC,
								ProductOption.PRICE, ProductOption.CATEGORIES, ProductOption.CLASSIFICATION,
								ProductOption.VARIANT_FULL,
								ProductOption.PRICE_RANGE, ProductOption.VARIANT_MATRIX_BASE, ProductOption.VARIANT_MATRIX_URL,
								ProductOption.VARIANT_MATRIX_PRICE, ProductOption.VARIANT_MATRIX_NEXT_VERSION));

				productDataList.add(productData);
			}
		}
		final CompareTableData compareTableData = productCompareTableConverter.convert(productDataList);
		return compareTableData;
	}

	private Map<String, CFALevelData> getGridData(final String categoryCode)
	{
		List<WileyProductModel> products = getWileyProductService().getWileyProductsFromCategoryRoot(categoryCode, null)
				.stream()
				.filter(product -> getWileyProductRestrictionService().isVisible(product)).collect(Collectors.toList());
		Date latestDate = getWileyProductService().getLatestExamDate(products);

		Locale currentLocale = getCommonI18NService().getLocaleForLanguage(getCommonI18NService().getCurrentLanguage());
		SimpleDateFormat fmt = new SimpleDateFormat(COMPARISON_DATE_FORMAT, currentLocale);

		Map<String, CFALevelData> gridData = createMapForGridData(fmt.format(latestDate));
		List<WileyProductModel> wileyProducts = getWileyProductService().getWileyProductsFromCategoryRoot(WEL_CFA_SUPPLEMENTS,
				latestDate);
		for (final WileyProductModel product : wileyProducts)
		{
			if (getWileyProductRestrictionService().isVisible(product))
			{
				populateProductToGridData(product, gridData);
			}
		}
		amendProductsNamesInGrid(gridData);
		return gridData;
	}

	/**
	 * Method builds product name using template %Level Name% %Product Name%
	 * For variant products name eq base product name. it's achieved by using VARIANT_PRODUCT_NAME populator
	 *
	 * @param gridData
	 */
	private void amendProductsNamesInGrid(final Map<String, CFALevelData> gridData)
	{
		for (CFALevelData cfaLevelData : gridData.values())
		{
			final String categoryName = cfaLevelData.getLevelName();
			amendProductsNames(categoryName, cfaLevelData.getAddons());
			amendProductsNames(categoryName, cfaLevelData.getSupplements());
		}
	}

	private void amendProductsNames(final String categoryName, final List<ProductData> productsList)
	{
		for (ProductData productData : productsList)
		{
			final String productLevelName = categoryName + " " + productData.getName();
			productData.setName(productLevelName);
		}
	}

	private Map<String, CFALevelData> createMapForGridData(final String formattedDate)
	{
		Map<String, CFALevelData> gridData = new TreeMap<>();
		CategoryModel levelCategory = getCategoryService().getCategoryForCode(WileyCoreConstants.WEL_CFA_LEVEL_CATEGORY_CODE);
		Collection<CategoryModel> levelCategoriesList = levelCategory.getCategories();
		for (CategoryModel categoryModel : levelCategoriesList)
		{
			if (categoryModel instanceof VariantValueCategoryModel)
			{
				gridData.put(categoryModel.getCode(), createCFALevelData(categoryModel.getName(), formattedDate));
			}
		}
		return gridData;
	}

	private CFALevelData createCFALevelData(final String levelName, final String formattedDate)
	{
		final CFALevelData data = new CFALevelData();
		data.setExamDate(formattedDate);
		data.setLevelName(levelName);
		data.setAddons(new ArrayList<>());
		data.setSupplements(new ArrayList<>());
		return data;
	}

	private void populateProductToGridData(final WileyProductModel product, final Map<String, CFALevelData> gridData)
	{
		for (final VariantProductModel variant : product.getVariants())
		{
			if (getWileyProductRestrictionService().isVisible(variant))
			{
				for (final CategoryModel category : variant.getSupercategories())
				{
					populateProductCfaToGrid(gridData, variant, category);
				}
			}
		}
	}

	private void populateProductCfaToGrid(final Map<String, CFALevelData> gridData,
										  final VariantProductModel variant, final CategoryModel category) {
		final CFALevelData cfaLevelData = gridData.get(category.getCode());
		if (cfaLevelData != null)
		{
			final ProductData productData = getProductForOptions(variant, Arrays.asList(ProductOption.BASIC,
					ProductOption.PRICE, ProductOption.SUMMARY, ProductOption.FREE_TRIAL_REFERENCE,
					ProductOption.VARIANT_PRODUCT_NAME));
			if (getWileyProductService().isAddon(variant))
			{
				checkPriceAndAddProductToList(cfaLevelData.getAddons(), productData);
			}
			else
			{
				checkPriceAndAddProductToList(cfaLevelData.getSupplements(), productData);
			}
		}
	}

	/**
	 * Check if product list contains product with base product like base product of newProduct.
	 * In negative case newProduct will add to product list.
	 * In posiive case newProduct replace product with the same baseProduct if it has less price
	 *
	 * @param products
	 * 		the products
	 * @param newProduct
	 * 		the new product
	 */
	protected void checkPriceAndAddProductToList(final List<ProductData> products, final ProductData newProduct)
	{
		final int index = indexOfProductWithTheSameBaseProduct(products, newProduct);
		if (index == -1)
		{
			products.add(newProduct);
		}
		else
		{
			final ProductData productFromList = products.get(index);
			products.set(index, getProductWithMinPrice(productFromList, newProduct));
		}

	}

	/**
	 * Returns index of product with base product like base product of newProduct.
	 * -1 if such product not found
	 *
	 * @param products
	 * 		the products
	 * @param newProduct
	 * 		the new product
	 * @return index of product with the same base product
	 */
	private int indexOfProductWithTheSameBaseProduct(final List<ProductData> products, final ProductData newProduct)
	{
		for (final ProductData product : products)
		{
			if (product.getBaseProduct().equals(newProduct.getBaseProduct()))
			{
				return products.indexOf(product);
			}
		}
		return -1;
	}

	/**
	 * Returns product with min price.
	 *
	 * @param product1
	 * 		the first product
	 * @param product2
	 * 		the second product
	 * @return productData with min price
	 */
	private ProductData getProductWithMinPrice(final ProductData product1, final ProductData product2)
	{
		if (product2.getPrice() != null && product1.getPrice() != null)
		{
			final BigDecimal price1 = product1.getPrice().getValue();
			final BigDecimal price2 = product2.getPrice().getValue();
			if (price1 != null && price2 != null)
			{
				return price1.doubleValue() < price2.doubleValue() ? product1 : product2;
			}
		}
		return product1;
	}

	/**
	 * Gets partner products by categoryCode and partnerId.
	 * If partner has UNA_PARTNER DiscountGroup, than products will be sorted by name
	 * Otherwise products will be sorted by max variant price
	 *
	 * @param categoryCode
	 * 		the category code
	 * @param partnerId
	 * 		the partner id
	 * @return the partner products by category and partner
	 */
	@Override
	@Nonnull
	public List<ProductData> getPartnerProductsByCategoryAndPartner(@Nonnull final String categoryCode,
			@Nonnull final String partnerId)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("categoryCode", categoryCode);
		ServicesUtil.validateParameterNotNullStandardMessage("partnerId", partnerId);

		CategoryModel categoryModel = getCategoryService().getCategoryForCode(categoryCode);
		List<ProductModel> products = getWileyProductService().getProductsForCategoryExcludingSubcategories(categoryModel);
		List<ProductData> productDataList = new ArrayList<>();

		for (ProductModel productModel : products)
		{
			ProductModel productModelForDisplay = productModel instanceof WileyProductVariantSetModel ?
					((WileyProductVariantSetModel) productModel).getBaseProduct() :
					productModel;
			final ProductData productData = productFacade.getProductForOptions(productModelForDisplay,
					Arrays.asList(ProductOption.BASIC, ProductOption.SUMMARY, ProductOption.PRICE,
							ProductOption.CATEGORIES,
							ProductOption.CLASSIFICATION, ProductOption.VARIANT_FULL, ProductOption.PRICE_RANGE,
							ProductOption.VARIANT_MATRIX_BASE, ProductOption.VARIANT_MATRIX_URL,
							ProductOption.VARIANT_MATRIX_PRICE));

			createDataForSetProduct(productModel, productData);
			productDataList.add(productData);
		}
		mergeEbookPrintCFAProducts(productDataList);
		if (StringUtils.equalsIgnoreCase(partnerId, WileyFacadesConstants.GENERIC_PARTNER_PAGE_URL))
		{
			sortProductsByMaxVariantPrice(productDataList);
			return productDataList;
		}
		else
		{
			final UserDiscountGroup userDiscountGroup = wileyPartnerFacade.getPartnerDiscountGroupForId(partnerId);
			sortBasedOnUserDiscountGroup(productDataList, userDiscountGroup);

			return getDiscountedProductData(productDataList, userDiscountGroup);
		}
	}

	private void createDataForSetProduct(final ProductModel productModel, final ProductData productData)
	{
		if (productModel instanceof WileyProductVariantSetModel)
		{
			ProductData setProductData = productFacade.getProductForOptions(productModel,
					Arrays.asList(ProductOption.CATEGORIES));
			getProductWithoutVariantsOptions(productData, setProductData.getTypeVariantCategoryCode(),
					TypeCheck.VARIANT_VALUE_CATEGORY);
			productData.setCode(productModel.getCode());
		}
	}

	/**
	 * Get Discounted ProductData based on User Discount Group
	 *
	 * @param productDataList
	 * @param userDiscountGroup
	 * @return
	 */
	private List<ProductData> getDiscountedProductData(final List<ProductData> productDataList,
			final UserDiscountGroup userDiscountGroup)
	{
		List<ProductData> finalProductDataList = new ArrayList<>();
		if (userDiscountGroup != null)
		{
			finalProductDataList = wileyPartnerStrategy.getDiscountedPartnerProducts(productDataList, userDiscountGroup);
		}
		if (!finalProductDataList.isEmpty())
		{
			return finalProductDataList;
		}
		else
		{
			//case when partner has discount group but doesn't have filled discount rows
			return productDataList;
		}
	}

	/**
	 * Sort based on User Discount Group information
	 *
	 * @param productDataList
	 * @param userDiscountGroup
	 */
	private void sortBasedOnUserDiscountGroup(final List<ProductData> productDataList, final UserDiscountGroup userDiscountGroup)
	{
		if (UserDiscountGroup.UNA_PARTNER.equals(userDiscountGroup))
		{
			sortProductsByName(productDataList);
		}
		else
		{
			sortProductsByMaxVariantPrice(productDataList);
		}
	}

	private void mergeEbookPrintCFAProducts(final List<ProductData> productDataList)
	{
		final Map<String, List<ProductData>> cfaLevelProductsMap = groupVariantProductsByCfaLevelAndBaseProduct(productDataList);
		for (final Map.Entry<String, List<ProductData>> entry : cfaLevelProductsMap.entrySet())
		{
			final ProductData mergedProduct = mergeProducts(entry.getValue());
			final String cfaLevel = getCfaLevel(mergedProduct);
			removeCFALevelOptions(mergedProduct, cfaLevel);
			productDataList.add(mergedProduct);
		}
	}

	private void removeCFALevelOptions(final ProductData mergedProduct, final String variantCategoryCode)
	{
		if (CollectionUtils.isNotEmpty(mergedProduct.getVariantMatrix()))
		{
			final List variantMatrix = mergedProduct.getVariantMatrix().stream()
					.filter(e -> e.getVariantValueCategory().getCode().equals(variantCategoryCode)).collect(Collectors.toList());
			mergedProduct.setVariantMatrix(variantMatrix);
		}
	}

	private ProductData mergeProducts(final List<ProductData> sameCfaLevelProducts)
	{
		// If we have print and ebook both for same level then return any one of them and remove ebook and print from its name.
		if (sameCfaLevelProducts.size() > 1)
		{
			return createNewMergedProduct(sameCfaLevelProducts);
		}
		// If we have either ebook or print then remove ebook/print radio button and return the same product.

		ProductData productData = sameCfaLevelProducts.get(0);
		productData.setMultidimensional(false);
		return getProductWithoutVariantsOptions(productData, productData.getCode(), TypeCheck.VARIANT_OPTION);
	}

	private enum TypeCheck
	{
		VARIANT_OPTION, VARIANT_VALUE_CATEGORY
	}

	private ProductData getProductWithoutVariantsOptions(final ProductData productData, final String code, final TypeCheck type)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("productData", productData);
		final List<VariantMatrixElementData> variantMatrix = productData.getVariantMatrix();
		if (variantMatrix != null)
		{
			for (final Iterator<VariantMatrixElementData> iterator = variantMatrix.iterator(); iterator.hasNext();)
			{
				VariantMatrixElementData element = iterator.next();
				if (TypeCheck.VARIANT_OPTION == type && !element.getVariantOption().getCode().equals(code))
				{
					iterator.remove();
				}

				if (TypeCheck.VARIANT_VALUE_CATEGORY == type && !element.getVariantValueCategory().getCode().equals(code))
				{
					iterator.remove();
				}
			}
		}
		return productData;
	}

	private ProductData createNewMergedProduct(final List<ProductData> sameCfaLevelProducts)
	{
		ProductData mergedProductData = sameCfaLevelProducts.get(0);
		final String newName = mergedProductData.getName().replaceAll(EBOOK_PRINT_REGEX, "");
		mergedProductData.setName(newName);
		mergedProductData.setMultidimensional(true);
		return mergedProductData;
	}

	private Map<String, List<ProductData>> groupVariantProductsByCfaLevelAndBaseProduct(final List<ProductData> products)
	{
		final Map<String, List<ProductData>> groupedVariantProductsMap = new HashMap<>();
		for (final Iterator<ProductData> iterator = products.iterator(); iterator.hasNext();)
		{
			final ProductData product = iterator.next();
			if (product.getVariantType() == null)
			{
				final String cfaLevel = getCfaLevel(product);
				if (!StringUtils.isEmpty(cfaLevel))
				{
					final String cfaLevelAndBaseProduct = cfaLevel + product.getBaseProduct();
					List<ProductData> cfaProducts = groupedVariantProductsMap.get(cfaLevelAndBaseProduct);
					if (cfaProducts == null)
					{
						cfaProducts = new ArrayList<>();
					}
					cfaProducts.add(product);
					groupedVariantProductsMap.put(cfaLevelAndBaseProduct, cfaProducts);
					iterator.remove();
				}
			}
		}
		return groupedVariantProductsMap;
	}

	private String getCfaLevel(final ProductData product)
	{
		for (final CategoryData category : product.getCategories())
		{
			if (category.getCode().matches(CFA_LEVEL_CATEGORY_REGEX))
			{
				return category.getCode();
			}
		}
		return null;
	}

	private void sortProductsByMaxVariantPrice(final List<ProductData> productDataList)
	{
		final Comparator<ProductData> maxPriceComparator = (final ProductData p1, final ProductData p2) ->
		{
			PriceData price1 = p1.getPriceRange().getMaxPrice();
			PriceData price2 = p2.getPriceRange().getMaxPrice();
			if (price1 != null && price2 != null)
			{
				return price1.getValue().compareTo(price2.getValue());
			}
			else
			{
				return 0;
			}
		};

		Collections.sort(productDataList, maxPriceComparator.reversed());
	}

	private void sortProductsByName(final List<ProductData> productDataList)
	{
		Comparator<ProductData> nameComparator =
				(ProductData p1, ProductData p2) -> p1.getName().compareTo(p2.getName());
		Collections.sort(productDataList, nameComparator);
	}

	@Override
	public ProductComparisonData getComparisonProductsForCategory(final String categoryCode,
			final List<ProductOption> productOptions, final Integer maxNumberOfProducts)
	{
		final CategoryModel categoryModel = getCategoryService().getCategoryForCode(categoryCode);
		final List<ProductModel> products = getProductComparisonService()
				.getProductsForComparison(categoryModel, maxNumberOfProducts);
		return createProductComparisonData(products, productOptions, true);
	}

	@Override
	public ProductComparisonData getComparisonProductsForCategories(final String baseProductCategoryCode,
			final String variantValueCategoryCode, final List<ProductOption> productOptions, final Integer maxNumberOfProducts)
	{
		final List<ProductModel> products = getProductComparisonService()
				.getProductsForComparison(baseProductCategoryCode, variantValueCategoryCode, maxNumberOfProducts);
		return createProductComparisonData(products, productOptions, true);
	}


	@Override
	public ProductComparisonData getComparisonProductData(final List<? extends ProductModel> products,
			final List<ProductOption> productOptions, final List<CMSProductAttributeModel> attributes)
	{
		final List<ProductModel> filteredProducts = products.stream()
				.filter(this::isProductVisible).collect(Collectors.toList());
		final ProductComparisonData productComparisonData = createProductComparisonData(filteredProducts, productOptions, false);
		amendNameForProducts(productComparisonData.getProducts());
		Set<String> featureCodesFromProducts = getFeatureCodesFromProducts(productComparisonData.getProducts());
		List<FeatureData> featuresFromCms = attributes.stream()
				.filter(attribute -> Boolean.TRUE.equals(attribute.isVisible()))
				.map(attribute -> wileyFeatureFromAssignmentConverter.convert(attribute.getAssignment()))
				.collect(Collectors.toList());
		List<FeatureData> filteredFeaturesFromCms = featuresFromCms.stream()
				.filter(feature -> featureCodesFromProducts.contains(feature.getCode()))
				.collect(Collectors.toList());
		productComparisonData.setFeatures(filteredFeaturesFromCms);
		return productComparisonData;
	}

	private Set<String> getFeatureCodesFromProducts(final List<ProductData> products)
	{
		final Set<String> featureCodes = new HashSet<>();
		for (ProductData product : products)
		{
			product.getIncludeFeatures().stream().forEach(feature -> featureCodes.add(feature.getCode()));
		}
		return featureCodes;
	}

	@Override
	public List<ProductData> getSupplementsProductData(final List<? extends ProductModel> products,
			final List<ProductOption> productOptions)
	{
		List<ProductData> productDataList = products.stream()
				.filter(product -> getWileyProductRestrictionService().isVisible(product))
				.map(product -> getProductForCodeAndOptions(product.getCode(), productOptions))
				.collect(Collectors.toList());
		amendNameForProducts(productDataList);
		return productDataList;
	}

	private void amendNameForProducts(final List<ProductData> productsList)
	{
		for (ProductData productData : productsList)
		{
			if (StringUtils.isNotEmpty(productData.getLevelVariantCategoryCode()))
			{
				CategoryModel levelCategory = getCategoryService().getCategoryForCode(productData.getLevelVariantCategoryCode());
				final String productLevelName = levelCategory.getName() + " " + productData.getName();
				productData.setName(productLevelName);
			}
		}
	}

	private ProductComparisonData createProductComparisonData(final List<? extends ProductModel> products,
			final List<ProductOption> productOptions, boolean needFeatures)
	{
		final List<ProductData> result = new ArrayList<>();
		final List<FeatureData> features = new ArrayList<>();

		final ProductComparisonData productComparisonData = new ProductComparisonData();

		for (final ProductModel productModel : products)
		{
			final ProductData productData = getProductConverter().convert(productModel);
			getProductConfiguredPopulator().populate(productModel, productData, productOptions);

			result.add(productData);
			if (needFeatures)
			{
				addFeatures(productData, features);
			}
		}


		productComparisonData.setProducts(result);
		productComparisonData.setFeatures(features);

		return productComparisonData;
	}

	private boolean isProductVisible(final ProductModel product)
	{
		return CollectionUtils.isNotEmpty(product.getFeatures().stream()
				.filter(feature -> wileyClassificationService.isFeatureVisible(feature)).collect(Collectors.toList()))
				&& getWileyProductRestrictionService().isVisible(product);
	}

	@Override
	public List<ProductData> getSupplementProductsForCategory(final String categoryCode,
			final List<ProductOption> productOptions)
	{
		final CategoryModel categoryModel = getCategoryService().getCategoryForCode(categoryCode);
		List<ProductData> productDataList = new ArrayList<>();
		try
		{
			CategoryModel supplementsCategory = getSupplementsCategoryFor(categoryModel);
			if (supplementsCategory != null) {
				productDataList = createProductsDataList(supplementsCategory.getProducts(), productOptions);
			}
		}
		catch (final NoSuchElementException e)
		{
			LOG.debug(MSG_NO_SUPPLEMENTS_FOR_CATEGORY + categoryModel.getName());
		}
		return productDataList;
	}

	@Override
	public List<ProductData> getSupplementProductsForCategories(final String baseProductCategoryCode,
			final String variantValueCategoryCode, final List<ProductOption> productOptions)
	{
		final CategoryModel baseProductCategoryModel = getCategoryService().getCategoryForCode(baseProductCategoryCode);
		List<ProductData> productDataList = new ArrayList<>();
		try
		{
			final CategoryModel supplementsCategory = getSupplementsCategoryFor(baseProductCategoryModel);
			if (supplementsCategory != null) {
                final List<ProductModel> products = getWileyProductService().getWileyProductsFromCategories(
                        supplementsCategory.getCode(), variantValueCategoryCode);
                productDataList = createProductsDataList(getWileyProductService().filterProducts(products), productOptions);
            }
		}
		catch (final NoSuchElementException e)
		{
			LOG.debug(MSG_NO_SUPPLEMENTS_FOR_CATEGORY + baseProductCategoryModel.getName());
		}
		return productDataList;
	}

	private List<ProductData> createProductsDataList(final List<ProductModel> products,
			final List<ProductOption> productOptions)
	{
		final List<ProductModel> filteredProducts = products
				.stream()
				.filter(product -> getWileyProductRestrictionService().isVisible(product)).collect(Collectors.toList());
		return filteredProducts.stream().map(product -> getProductForOptions(product, productOptions))
				.collect(Collectors.toList());
	}

	private CategoryModel getSupplementsCategoryFor(final CategoryModel category)
	{
		return category.getAllSubcategories().stream().filter(categoryModel -> categoryModel.getCode().matches(".*_SUPPLEMENTS"))
				.findFirst().orElse(null);
	}

	private void addFeatures(final ProductData productData, final List<FeatureData> featuresToAdd)
	{
		final List<FeatureData> features = productData.getIncludeFeatures();

		if (features != null && !features.isEmpty())
		{
			List<String> addedFeatureCodeList = featuresToAdd.stream()
					.map(FeatureData::getCode).collect(Collectors.toList());
			featuresToAdd.addAll(features.stream()
					.filter(feature -> !addedFeatureCodeList.contains(feature.getCode()))
					.collect(Collectors.toList()));
		}
	}

	/**
	 * Gets product compare table converter.
	 *
	 * @return the product compare table converter
	 */
	public WileyProductCompareTableCFAConverter getProductCompareTableConverter()
	{
		return productCompareTableConverter;
	}

	/**
	 * Sets product compare table converter.
	 *
	 * @param productCompareTableConverter
	 * 		the product compare table converter
	 */
	@Required
	public void setProductCompareTableConverter(
			final WileyProductCompareTableCFAConverter productCompareTableConverter)
	{
		this.productCompareTableConverter = productCompareTableConverter;
	}

	/**
	 * Gets product facade.
	 *
	 * @return the product facade
	 */
	public ProductFacade getProductFacade()
	{
		return productFacade;
	}

	/**
	 * Sets product facade.
	 *
	 * @param productFacade
	 * 		the product facade
	 */
	@Required
	public void setProductFacade(final ProductFacade productFacade)
	{
		this.productFacade = productFacade;
	}

	/**
	 * @return the productComparisonService
	 */
	public WileyProductComparisonService getProductComparisonService()
	{
		return productComparisonService;
	}

	/**
	 * @param productComparisonService
	 * 		the productComparisonService to set
	 */
	public void setProductComparisonService(final WileyProductComparisonService productComparisonService)
	{
		this.productComparisonService = productComparisonService;
	}
}
