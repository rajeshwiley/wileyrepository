package com.wiley.facades.wileyb2c.wileyplus.url.strategy;


import java.net.MalformedURLException;
import java.net.URISyntaxException;

import com.wiley.facades.wileyplus.cart.activation.dto.CartActivationRequestDto;


public interface WileyplusPurchaseOptionsPageBuildURLStrategy
{
	/**
	 * Generates URL which will be invoked from WileyPlus to access Purchase Options Page
	 * It contains the following query parameters:
	 * cartActivationInfo - base64 encoded Cart Activation Request parameters
	 * checksum - MD5 checksum of cartActivationInfo with secret key
	 *
	 * @param cartActivationRequestDto
	 * @return generated Purchase Order Page URL
	 * @throws URISyntaxException
	 * @throws MalformedURLException
	 */
	String buildURL(CartActivationRequestDto cartActivationRequestDto)
			throws URISyntaxException, MalformedURLException;

}
