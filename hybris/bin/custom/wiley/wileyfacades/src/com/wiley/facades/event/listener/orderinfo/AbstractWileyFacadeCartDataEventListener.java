package com.wiley.facades.event.listener.orderinfo;

import com.wiley.core.event.facade.WileyOneObjectFacadeEvent;
import com.wiley.core.order.WileyCartService;
import com.wiley.facades.order.helper.WileyPopulateOrderInfoHelper;
import de.hybris.platform.acceleratorservices.payment.data.OrderInfoData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;

/**
 * Created by Georgii_Gavrysh on 12/12/2016.
 */
public abstract class AbstractWileyFacadeCartDataEventListener
{
    @Resource
    private WileyCartService wileyCartService;

    private Converter<CartModel, CartData> cartConverter;

    private WileyPopulateOrderInfoHelper wileyPopulateOrderInfoHelper;

    public void populateOrderInfoData(final WileyOneObjectFacadeEvent<OrderInfoData> event)
    {
        final CartModel cartModel = getWileyCartService().getSessionCart();
        final CartData cartData = getCartConverter().convert(cartModel);
        getWileyPopulateOrderInfoHelper().populateOrderInfoData(cartModel, cartData, event.getParameter());
    }

    public WileyCartService getWileyCartService() {
        return wileyCartService;
    }

    public void setWileyCartService(final WileyCartService wileyCartService) {
        this.wileyCartService = wileyCartService;
    }

    public Converter<CartModel, CartData> getCartConverter() {
        return cartConverter;
    }

    public void setCartConverter(final Converter<CartModel, CartData> cartConverter) {
        this.cartConverter = cartConverter;
    }

    public void setWileyPopulateOrderInfoHelper(final WileyPopulateOrderInfoHelper wileyPopulateOrderInfoHelper) {
        this.wileyPopulateOrderInfoHelper = wileyPopulateOrderInfoHelper;
    }

    public WileyPopulateOrderInfoHelper getWileyPopulateOrderInfoHelper() {
        return wileyPopulateOrderInfoHelper;
    }
}
