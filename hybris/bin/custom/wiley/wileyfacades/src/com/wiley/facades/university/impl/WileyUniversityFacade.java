package com.wiley.facades.university.impl;

import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;

import com.wiley.core.model.UniversityModel;
import com.wiley.core.university.services.UniversityService;
import com.wiley.facades.populators.UniversityPopulator;
import com.wiley.facades.university.UniversityFacade;
import com.wiley.facades.university.data.UniversityData;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


/**
 * Default implementation of {@link UniversityFacade}
 */
public class WileyUniversityFacade implements UniversityFacade
{

	@Resource
	CommonI18NService commonI18NService;

	@Resource
	UniversityPopulator universityPopulator;

	@Resource
	UniversityService universityService;

	/**
	 * Retrieves all universities for persisted country and region isocodes.
	 * If country code and region code are incorrect, returns emply list
	 *
	 * @return - list of populated universities
	 */
	@NotNull
	public List<UniversityData> getUniversitiesByCountryAndRegionIso(@NotNull final String countryIso,
			@NotNull final String regionIso)
	{
		validateParameterNotNullStandardMessage("countryIso", countryIso);
		validateParameterNotNullStandardMessage("regionIso", regionIso);
		final CountryModel country = commonI18NService.getCountry(countryIso);
		final RegionModel region = commonI18NService.getRegion(country, regionIso);
		final List<UniversityModel> universities = universityService.getUniversitiesByRegion(region);
		final List<UniversityData> universityDataList = populateUniversities(universities);
		universityDataList.sort(Comparator.comparing(university -> university.getName()));
		return universityDataList;
	}

	public CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	public UniversityPopulator getUniversityPopulator()
	{
		return universityPopulator;
	}

	private List<UniversityData> populateUniversities(final List<UniversityModel> universities)
	{
		final List<UniversityData> universityDataList = new ArrayList<>();
		for (UniversityModel university : universities)
		{
			UniversityData universityData = new UniversityData();
			universityPopulator.populate(university, universityData);
			universityDataList.add(universityData);
		}
		return universityDataList;
	}
}
