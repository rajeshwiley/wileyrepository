package com.wiley.facades.subscription;

import com.wiley.facades.product.data.WileySubscriptionData;



/**
 * Facade for subscription.
 *
 * Created by Uladzimir_Barouski on 12/16/2015.
 */
public interface WileySubscriptionFacade
{

	/**
	 * Method returns {@link de.hybris.platform.subscriptionservices.enums.SubscriptionStatus#ACTIVE ACTIVE} user subscription or
	 * null if user doesn't have active subscriptions
	 */
	WileySubscriptionData getActiveSubscription();

	/**
	 * Facade to update WileySubscription autoRenew attribute
	 *
	 * @param code
	 * @param autoRenew
	 * @return WileySubscriptionData
	 */
	WileySubscriptionData updateAutoRenew(String code, boolean autoRenew);

	/**
	 * Checks if current user has active subscription.
	 *
	 * @return true if user has active subscription else false.
	 */
	boolean hasCurrentUserActiveSubscription();

	/**
	 * Method checks if any {@link de.hybris.platform.subscriptionservices.model.SubscriptionProductModel SubscriptionProduct}
	 * exists in a session cart of current user.
	 *
	 * @return return true if session cart has at least one any SubscriptionProduct.
	 */
	boolean hasCurrentUserAnySubscriptionInCart();

}
