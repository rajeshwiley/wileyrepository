package com.wiley.facades.student;

import java.util.List;

import com.wiley.facades.student.data.StudentVerificationData;
import com.wiley.facades.university.data.UniversityData;


/**
 * Student facade interface. Facade for working with student discounts flow logic.
 */
public interface WileyStudentVerificationFacade
{

	/**
	 * Saves studentVerificationData passed from
	 * storefront in cart.
	 */
	void saveStudentVerification(StudentVerificationData studentVerificationData);

	/**
	 * Retrieves all universities for persisted country and region isocodes.
	 * Adds Other item to returned list
	 *
	 * @param countryIso
	 * @param regionIso
	 * @return - list of populated universities
	 */
	List<UniversityData> getUniversitiesByCountryAndRegionIso(String countryIso, String regionIso);

}
