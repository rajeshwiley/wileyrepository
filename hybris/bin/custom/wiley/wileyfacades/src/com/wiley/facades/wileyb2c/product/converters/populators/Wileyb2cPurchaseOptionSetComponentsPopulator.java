package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.List;

import org.springframework.beans.factory.annotation.Required;


/**
 * Created by Uladzimir_Barouski on 6/5/2017.
 */
public class Wileyb2cPurchaseOptionSetComponentsPopulator implements Populator<ProductModel, VariantOptionData>
{
	private Converter<ProductModel, List<ProductData>> productSetConverter;

	@Override
	public void populate(final ProductModel source, final VariantOptionData target) throws ConversionException
	{
		ServicesUtil.validateParameterNotNullStandardMessage("productModel", source);
		ServicesUtil.validateParameterNotNullStandardMessage("variantOptionData", target);

		target.setProductSetComponents(productSetConverter.convert(source));
	}

	@Required
	public void setProductSetConverter(
			final Converter<ProductModel, List<ProductData>> productSetConverter)
	{
		this.productSetConverter = productSetConverter;
	}
}
