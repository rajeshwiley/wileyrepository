package com.wiley.facades.wileyb2c.search.solrfacetsearch.converters.populator;

import de.hybris.platform.commercefacades.search.converters.populator.SearchResultProductPopulator;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;

import com.wiley.facades.content.ContentData;


/**
 * @author Dzmitryi_Halahayeu
 */
public class Wileyb2cSearchResultContentPopulator implements Populator<SearchResultValueData, ContentData>
{
	public static final String BOOST = "boost";
	public static final String TITLE = "title";
	public static final String URL = "url";
	public static final String ID = "id";
	public static final String META_DESCRIPTION = "metaDescription";
	public static final String STRIPPED_CONTENT = "strippedContent";
	private int contentMaxSize;

	@Override
	public void populate(final SearchResultValueData source, final ContentData contentData)
			throws ConversionException
	{
		contentData.setBoost(getValue(source, BOOST));
		contentData.setContent(getContent(source));
		contentData.setTitle(getValue(source, TITLE));
		contentData.setUrl(getValue(source, URL));
		contentData.setId(getValue(source, ID));
	}

	private String getContent(final SearchResultValueData source)
	{
		String content = getValue(source, META_DESCRIPTION);
		final String strippedContent = getValue(source, STRIPPED_CONTENT);
		if (StringUtils.isEmpty(content) && StringUtils.isNotEmpty(strippedContent))
		{
			content = StringUtils.substring(strippedContent, 0, contentMaxSize);
			if (StringUtils.substring(strippedContent, contentMaxSize,  contentMaxSize + 1).matches("\\w"))
			{
				content = StringUtils.substring(content, 0, content.lastIndexOf(" ")).trim();
			}
			content = content + "...";
		}
		return content;
	}

	/**
	 * OOTB_CODE Based on {@link SearchResultProductPopulator#getValue(SearchResultValueData, String)}
	 * @param source
	 * @param propertyName
	 * @param <T>
	 * @return
	 */
	protected <T> T getValue(final SearchResultValueData source, final String propertyName)
	{
		if (source.getValues() == null)
		{
			return null;
		}

		// DO NOT REMOVE the cast (T) below, while it should be unnecessary it is required by the javac compiler
		return (T) source.getValues().get(propertyName);
	}

	@Value("${search.content.max.size}")
	public void setContentMaxSize(final int contentMaxSize)
	{
		this.contentMaxSize = contentMaxSize;
	}
}
