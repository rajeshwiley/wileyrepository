package com.wiley.facades.partner.impl;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import com.wiley.core.enums.KpmgFunction;
import com.wiley.core.model.KpmgEnrollmentModel;
import com.wiley.core.model.KpmgOfficeModel;
import com.wiley.core.partner.WileyKPMGEnrollmentService;
import com.wiley.facades.partner.WileyKPMGEnrollmentFacade;
import com.wiley.facades.partner.kpmg.KPMGEnrollmentData;
import com.wiley.facades.partner.kpmg.KPMGFunctionData;
import com.wiley.facades.partner.kpmg.KPMGOfficeData;


public class WileyKPMGEnrollmentFacadeImpl implements WileyKPMGEnrollmentFacade
{
	private static final Logger LOG = Logger.getLogger(WileyKPMGEnrollmentFacadeImpl.class);

	@Resource
	private CartService cartService;

	@Resource
	private ModelService modelService;

	@Resource
	private Converter<KPMGEnrollmentData, KpmgEnrollmentModel> kpmgEnrollmentDataConverter;

	@Resource
	private WileyKPMGEnrollmentService wileyKpmgEnrollmentService;

	@Override
	public List<KPMGFunctionData> getFunctions()
	{
		return Arrays.asList(KpmgFunction.values()).stream()
				.map(this::createKPMGFunctionData)
				.collect(Collectors.toList());
	}

	@Override
	public List<KPMGOfficeData> getOffices(final String partnerId)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("partnerId", partnerId);
		List<KPMGOfficeData> offices = null;
		List<KpmgOfficeModel> officeModels = wileyKpmgEnrollmentService.getOffices(partnerId);
		if (CollectionUtils.isNotEmpty(officeModels))
		{
			offices = officeModels.stream()
					.map(this::createOfficeData)
					.collect(Collectors.toList());
		}
		else
		{
			offices = Collections.emptyList();
		}
		return offices;
	}

	@Override
	public KPMGOfficeData createOfficeData(final String code)
	{
		KPMGOfficeData officeData = new KPMGOfficeData();
		officeData.setCode(code);
		return officeData;
	}

	@Override
	public KPMGFunctionData createKPMGFunctionData(final String code, final String name)
	{
		KPMGFunctionData functionData = new KPMGFunctionData();
		functionData.setCode(code);
		functionData.setName(name);
		return functionData;
	}

	@Override
	public void saveEnrollmentDataToCurrentCart(final KPMGEnrollmentData enrollmentData)
	{
		final CartModel sessionCart = cartService.getSessionCart();
		KpmgEnrollmentModel enrollmentModel = kpmgEnrollmentDataConverter.convert(enrollmentData);
		sessionCart.setKpmgEnrollment(enrollmentModel);
		modelService.saveAll(enrollmentModel, sessionCart);
	}

	private KPMGOfficeData createOfficeData(final KpmgOfficeModel officeModel)
	{
		KPMGOfficeData officeData = createOfficeData(officeModel.getCode());
		officeData.setName(officeModel.getName());
		return officeData;
	}

	private KPMGFunctionData createKPMGFunctionData(final KpmgFunction kpmgFunction)
	{
		return createKPMGFunctionData(kpmgFunction.toString(), kpmgFunction.getCode());
	}
}
