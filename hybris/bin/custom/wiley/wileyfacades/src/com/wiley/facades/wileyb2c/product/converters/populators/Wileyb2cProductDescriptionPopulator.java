/**
 *
 */
package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.platform.commercefacades.product.converters.populator.AbstractProductPopulator;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.apache.commons.lang.StringUtils;


public class Wileyb2cProductDescriptionPopulator<SOURCE extends ProductModel, TARGET extends ProductData>
		extends AbstractProductPopulator<SOURCE, TARGET>
{

	@Override
	public void populate(final ProductModel source, final ProductData target) throws ConversionException
	{
		final String descriptionEncrichedString = safeToString(
				getProductAttribute(source, ProductModel.DESCRIPTIONENRICHED));
		if (StringUtils.isNotEmpty(descriptionEncrichedString))
		{
			target.setDescription(descriptionEncrichedString);
		}
		else
		{
			target.setDescription(safeToString(getProductAttribute(source, ProductModel.DESCRIPTION)));
		}

	}
}
