package com.wiley.facades.ags.voucher.impl;

import de.hybris.platform.commercefacades.voucher.exceptions.VoucherOperationException;

import com.wiley.facades.voucher.impl.WileyCouponFacadeImpl;
import com.wiley.facades.welags.voucher.exception.InvalidVoucherException;
import com.wiley.facades.welags.voucher.exception.VoucherCannotBeAppliedException;


public class AgsCouponFacadeImpl extends WileyCouponFacadeImpl
{
    @Override
    public void applyVoucher(final String voucherCode) throws VoucherOperationException {
        if (!checkVoucherCode(voucherCode))
        {
            throw new InvalidVoucherException("Checking voucher code returned false for " + voucherCode);
        }
        if (!canApplyNewVoucher(voucherCode))
        {
            throw new VoucherCannotBeAppliedException(voucherCode + " voucher cannot be applied");
        }
        // remove applied vouchers
        removeVouchersFromCart();
        // redeem voucher
        super.applyVoucher(voucherCode);
    }
}
