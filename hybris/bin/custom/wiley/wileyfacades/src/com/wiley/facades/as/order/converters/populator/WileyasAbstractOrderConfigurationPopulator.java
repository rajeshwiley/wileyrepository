package com.wiley.facades.as.order.converters.populator;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import com.wiley.core.enums.PaymentModeEnum;
import com.wiley.facades.order.converters.populator.WileyAbstractOrderConfigurationPopulator;

import static com.wiley.facades.constants.WileyFacadesConstants.CURRENT_PAYMENT_OPTION_NAME;


public class WileyasAbstractOrderConfigurationPopulator extends WileyAbstractOrderConfigurationPopulator
{
	@Override
	public void populate(final AbstractOrderModel abstractOrderModel, final AbstractOrderData abstractOrderData)
			throws ConversionException
	{
		super.populate(abstractOrderModel, abstractOrderData);

		abstractOrderData.getOrderConfiguration().getPermittedPaymentModes().add(CURRENT_PAYMENT_OPTION_NAME);
		abstractOrderData.getOrderConfiguration().getPermittedPaymentModes().remove(PaymentModeEnum.PAYPAL.getCode());
	}
}
