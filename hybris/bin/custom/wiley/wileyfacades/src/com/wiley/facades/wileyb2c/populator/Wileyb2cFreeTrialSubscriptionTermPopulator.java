package com.wiley.facades.wileyb2c.populator;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.subscriptionfacades.data.SubscriptionTermData;
import de.hybris.platform.subscriptionservices.model.SubscriptionTermModel;

import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.model.WileyProductModel;
import com.wiley.core.wileyb2c.subscription.services.Wileyb2cFreeTrialSubscriptionTermCheckingStrategy;
import com.wiley.core.wileyb2c.subscription.services.Wileyb2cSubscriptionService;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


/**
 * Populates product with subscriptionTerm info if it corresponds to free trial
 *
 * @param <SOURCE>
 * @param <TARGET>
 */
public class Wileyb2cFreeTrialSubscriptionTermPopulator<SOURCE extends WileyProductModel, TARGET extends ProductData>
		implements Populator<SOURCE, TARGET>
{
	@Autowired
	private Wileyb2cSubscriptionService wileyb2cSubscriptionService;

	@Autowired
	private Wileyb2cFreeTrialSubscriptionTermCheckingStrategy freeTrialSubscriptionTermCheckingStrategy;

	@Autowired
	private Converter<SubscriptionTermModel, SubscriptionTermData> subscriptionTermConverter;

	@Override
	public void populate(final SOURCE productModel, final TARGET productData) throws ConversionException
	{
		validateParameterNotNull(productModel, "productModel must not be null");
		validateParameterNotNull(productData, "productData must not be null");

		if (wileyb2cSubscriptionService.isProductSubscription(productModel))
		{
			for (final SubscriptionTermModel subscriptionTerm : productModel.getSubscriptionTerms())
			{
				if (freeTrialSubscriptionTermCheckingStrategy.isFreeTrial(subscriptionTerm))
				{
					productData.setSubscriptionTerm(subscriptionTermConverter.convert(subscriptionTerm));
					break;
				}
			}
		}
	}
}
