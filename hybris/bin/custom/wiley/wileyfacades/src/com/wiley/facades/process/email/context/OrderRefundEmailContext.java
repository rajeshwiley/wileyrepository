/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.facades.process.email.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.returns.model.ReturnProcessModel;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.util.Config;

import java.math.BigDecimal;

import com.wiley.facades.populators.WileyPriceDataFactory;


/**
 * Velocity context for a order refund email.
 */
public class OrderRefundEmailContext extends AbstractEmailContext<ReturnProcessModel>
{
	private static final String WEL_EFFICIENTLEARNING_URL_HTTP = "wel.efficientlearning.url.http";
	private WileyPriceDataFactory priceDataFactory;
	private PriceData refundAmount;
	private String orderCode;
	private String efficientLearningLink;
	private String userUID;


	/**
	 * Init.
	 *
	 * @param returnProcessModel
	 * 		the order process model
	 * @param emailPageModel
	 * 		the email page model
	 */
	@Override
	public void init(final ReturnProcessModel returnProcessModel, final EmailPageModel emailPageModel)
	{
		super.init(returnProcessModel, emailPageModel);
		ReturnRequestModel returnRequest = returnProcessModel.getReturnRequest();
		OrderModel orderModel = returnRequest.getOrder();
		orderCode = orderModel.getCode();
		userUID = orderModel.getUser().getUid();
		refundAmount = priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(returnRequest.getRefundAmount()),
				orderModel.getCurrency());
		efficientLearningLink = Config.getParameter(WEL_EFFICIENTLEARNING_URL_HTTP);
	}

	/**
	 * Gets site.
	 *
	 * @param returnProcessModel
	 * 		the order process model
	 * @return the site
	 */
	@Override
	protected BaseSiteModel getSite(final ReturnProcessModel returnProcessModel)
	{
		return returnProcessModel.getReturnRequest().getOrder().getSite();
	}

	/**
	 * Gets customer.
	 *
	 * @param returnProcessModel
	 * 		the order process model
	 * @return the customer
	 */
	@Override
	protected CustomerModel getCustomer(final ReturnProcessModel returnProcessModel)
	{
		return (CustomerModel) returnProcessModel.getReturnRequest().getOrder().getUser();
	}

	/**
	 * Gets email language.
	 *
	 * @param returnProcessModel
	 * 		the order process model
	 * @return the email language
	 */
	@Override
	protected LanguageModel getEmailLanguage(final ReturnProcessModel returnProcessModel)
	{
		return returnProcessModel.getReturnRequest().getOrder().getLanguage();
	}

	/**
	 * Gets refund amount.
	 *
	 * @return the refund amount
	 */
	public PriceData getRefundAmount()
	{
		return refundAmount;
	}

	public String getEfficientLearningLink()
	{
		return efficientLearningLink;
	}

	public void setEfficientLearningLink(final String efficientLearningLink)
	{
		this.efficientLearningLink = efficientLearningLink;
	}

	public String getOrderCode()
	{
		return orderCode;
	}


	public WileyPriceDataFactory getPriceDataFactory()
	{
		return priceDataFactory;
	}

	public void setPriceDataFactory(final WileyPriceDataFactory priceDataFactory)
	{
		this.priceDataFactory = priceDataFactory;
	}

	public String getUserUID()
	{
		return userUID;
	}
}
