package com.wiley.facades.wileyb2c.order;

import com.wiley.facades.wileyplus.cart.activation.dto.CartActivationRequestDto;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.order.exceptions.CalculationException;


public interface Wileyb2cGracePeriodCheckoutFacade
{
	/**
	 * Adds specified product to cart and immediately places an order.
	 *
	 * @param activationRequestDto
	 * 		WileyPLUS cart activation request DTO
	 * @return placed order code
	 */
	String placeGracePeriodOrder(CartActivationRequestDto activationRequestDto)
			throws CommerceCartModificationException, InvalidCartException, CalculationException;

}
