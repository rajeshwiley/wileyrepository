package com.wiley.facades.event.listener.payment;

import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.request.AbstractRequestPopulator;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionRequest;
import de.hybris.platform.acceleratorservices.payment.data.PaymentData;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.i18n.I18NService;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.facades.product.data.InventoryStatusRecord;

public abstract class AbstractBasketProductAvailabilityWPGHttpRequestPopulator
		extends AbstractRequestPopulator<CreateSubscriptionRequest, PaymentData>
{
	@Autowired
	private EnumerationService enumerationService;
	@Autowired
	private I18NService i18NService;

	private static final String WPG_CUSTOM_BASKET_PREFIX = "WPG_CUSTOM_BASKET_";
	private static final String WPG_CUSTOM_BASKET_ENTRY_AVAILABILITY = "_availability_";
	private static final String WPG_CUSTOM_BASKET_ENTRY_AVAILABILITY_COUNT = "count";
	private static final String WPG_CUSTOM_BASKET_ENTRY_AVAILABILITY_QUANTITY = "_quantity";
	private static final String WPG_CUSTOM_BASKET_ENTRY_AVAILABILITY_DATE = "_date";
	private static final String WPG_CUSTOM_BASKET_ENTRY_AVAILABILITY_CODE = "_code";
	private final SimpleDateFormat desiredShippingDateFormat = new SimpleDateFormat("MM/dd/yyyy");

	protected void populatePaymentData(final PaymentData target, final Integer orderEntryIndex,
			final List<InventoryStatusRecord> inventoryStatusRecords)
	{
		addRequestQueryParam(target, WPG_CUSTOM_BASKET_PREFIX + orderEntryIndex
						+ WPG_CUSTOM_BASKET_ENTRY_AVAILABILITY + WPG_CUSTOM_BASKET_ENTRY_AVAILABILITY_COUNT,
				String.valueOf(inventoryStatusRecords.size()));

		for (int orderEntryAvailabilityIndex = 0;
			 orderEntryAvailabilityIndex < inventoryStatusRecords.size(); orderEntryAvailabilityIndex++)
		{
			final String availabilityPrefixName =
					WPG_CUSTOM_BASKET_PREFIX + orderEntryIndex + WPG_CUSTOM_BASKET_ENTRY_AVAILABILITY;
			final String statusCodeName = enumerationService.getEnumerationName(
					inventoryStatusRecords.get(orderEntryAvailabilityIndex).getStatusCode(),
					i18NService.getCurrentLocale());

			final Long quantity = inventoryStatusRecords.get(orderEntryAvailabilityIndex).getQuantity();
			if (quantity != null)
			{
				addRequestQueryParam(target,
						availabilityPrefixName + orderEntryAvailabilityIndex + WPG_CUSTOM_BASKET_ENTRY_AVAILABILITY_QUANTITY,
						String.valueOf(quantity));
			}
			final Date availableDate = inventoryStatusRecords.get(orderEntryAvailabilityIndex).getAvailableDate();
			if (availableDate != null)
			{
				addRequestQueryParam(target,
						availabilityPrefixName + orderEntryAvailabilityIndex + WPG_CUSTOM_BASKET_ENTRY_AVAILABILITY_DATE,
						desiredShippingDateFormat.format(availableDate));
			}
			addRequestQueryParam(target, availabilityPrefixName + orderEntryAvailabilityIndex
					+ WPG_CUSTOM_BASKET_ENTRY_AVAILABILITY_CODE, statusCodeName);
		}
	}
}
