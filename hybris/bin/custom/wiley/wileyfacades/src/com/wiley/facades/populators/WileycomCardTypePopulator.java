/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package com.wiley.facades.populators;

import de.hybris.platform.commercefacades.order.converters.populator.CardTypePopulator;
import de.hybris.platform.commercefacades.order.data.CardTypeData;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.i18n.I18NService;

import org.springframework.beans.factory.annotation.Autowired;


/**
 * Wiley Converter implementation for {@link CreditCardType} as source
 * and {@link CardTypeData} as target type with addition displayName parameter
 */
public class WileycomCardTypePopulator extends CardTypePopulator
{
	@Autowired
	private EnumerationService enumerationService;
	@Autowired
	private I18NService i18NService;

	@Override
	public void populate(final CreditCardType source, final CardTypeData target)
	{
		super.populate(source, target);

		String paymentTypeDisplayName = enumerationService.getEnumerationName(source, i18NService.getCurrentLocale());
		target.setDisplayName(paymentTypeDisplayName);
	}

	public EnumerationService getEnumerationService()
	{
		return enumerationService;
	}

	public void setEnumerationService(final EnumerationService enumerationService)
	{
		this.enumerationService = enumerationService;
	}

	public I18NService getI18NService()
	{
		return i18NService;
	}

	public void setI18NService(final I18NService i18NService)
	{
		this.i18NService = i18NService;
	}
}
