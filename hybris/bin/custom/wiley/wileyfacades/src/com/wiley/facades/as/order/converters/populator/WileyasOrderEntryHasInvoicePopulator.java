package com.wiley.facades.as.order.converters.populator;

import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import javax.annotation.Resource;

import com.wiley.core.checkout.configuration.WileyOrderConfigurationService;
import com.wiley.core.enums.OrderType;
import com.wiley.core.enums.PaymentModeEnum;
import com.wiley.core.model.WileyOrderConfigurationModel;


public class WileyasOrderEntryHasInvoicePopulator implements Populator<AbstractOrderEntryModel, OrderEntryData>
{
	private static final Set<OrderStatus> ORDER_STATUSES = new HashSet<>(
			Arrays.asList(OrderStatus.INVOICE_PENDING, OrderStatus.INVOICE_OVERDUE, OrderStatus.PAYMENT_CAPTURED));

	@Resource
	private WileyOrderConfigurationService wileyOrderConfigurationService;

	@Override
	public void populate(final AbstractOrderEntryModel source, final OrderEntryData target) throws ConversionException
	{
		target.setHasInvoice(
				isEntryInState(source) && isOrderPaidByInvoice(source.getOrder()) && isCorrectOrderType(source.getOrder())
						&& !isHidePaymentDetails(source.getOrder()));
	}

	private boolean isEntryInState(final AbstractOrderEntryModel orderEntryModel)
	{
		return ORDER_STATUSES.contains(orderEntryModel.getStatus());
	}

	private boolean isOrderPaidByInvoice(final AbstractOrderModel orderModel)
	{
		return Objects.nonNull(orderModel.getPaymentMode()) && (PaymentModeEnum.INVOICE.getCode().equals(
				orderModel.getPaymentMode().getCode()) || PaymentModeEnum.PROFORMA
				.getCode().equals(orderModel.getPaymentMode().getCode()));

	}

	private boolean isCorrectOrderType(final AbstractOrderModel orderModel)
	{
		return !OrderType.AS_FUNDED.equals(orderModel.getOrderType());
	}

	private boolean isHidePaymentDetails(final AbstractOrderModel orderModel)
	{
		Optional<WileyOrderConfigurationModel> wileyOrderConfigurationModel =
				wileyOrderConfigurationService.getOrderConfiguration(orderModel);
		if (wileyOrderConfigurationModel.isPresent())
		{
			return wileyOrderConfigurationModel.get().getHidePaymentDetails();
		}
		return false;
	}

}
