package com.wiley.facades.populators;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.WileyPaymentInfoData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.InvoicePaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;

import com.paypal.hybris.model.PaypalPaymentInfoModel;


public class WileyPaymentInfoOrderPopulator implements Populator<AbstractOrderModel, AbstractOrderData>
{

	public static final String PAYMENT_TYPE_PAYPAL = "PAYPAL";
	public static final String PAYMENT_TYPE_INVOICE = "INVOICE";
	public static final String PAYMENT_TYPE_ZERO_ORDER = "ZERO_ORDER";
	public static final String PAYMENT_TYPE_CARD = "CARD";

	@Resource
	protected Converter<AddressModel, AddressData> addressConverter;

	@Override
	public void populate(final AbstractOrderModel abstractOrderModel, final AbstractOrderData abstractOrderData)
			throws ConversionException
	{
		if (abstractOrderData.getWileyPaymentInfo() == null)
		{
			abstractOrderData.setWileyPaymentInfo(new WileyPaymentInfoData());
		}
		if (abstractOrderModel.getPaymentAddress() != null)
		{
			abstractOrderData.getWileyPaymentInfo()
					.setBillingAddress(addressConverter.convert(abstractOrderModel.getPaymentAddress()));
		}
		populatePaymentType(abstractOrderModel, abstractOrderData);
	}

	protected void populatePaymentType(final AbstractOrderModel abstractOrderModel, final AbstractOrderData abstractOrderData) {
		PaymentInfoModel paymentInfo = abstractOrderModel.getPaymentInfo();
		WileyPaymentInfoData wileyPaymentInfoData = abstractOrderData.getWileyPaymentInfo();
		if (paymentInfo == null) {
			wileyPaymentInfoData.setPaymentType(PAYMENT_TYPE_ZERO_ORDER);
		}
		else if (paymentInfo instanceof PaypalPaymentInfoModel) {
			wileyPaymentInfoData.setPaymentType(PAYMENT_TYPE_PAYPAL);
		}
		else if (paymentInfo instanceof CreditCardPaymentInfoModel) {
			wileyPaymentInfoData.setPaymentType(PAYMENT_TYPE_CARD);
		}
		else if (paymentInfo instanceof InvoicePaymentInfoModel) {
			wileyPaymentInfoData.setPaymentType(PAYMENT_TYPE_INVOICE);
		}
	}
}
