package com.wiley.facades.populators;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import com.wiley.core.model.WileyFreeTrialProductModel;
import com.wiley.core.product.WileyFreeTrialProductService;


/**
 * Populates a product with a link to corresponding "Free Trial" product 
 */
public class WileyFreeTrialReferencePopulator implements Populator<ProductModel, ProductData>
{
	private UrlResolver<ProductModel> productModelUrlResolver;
	private WileyFreeTrialProductService freeTrialProductService;
	
	@Override
	public void populate(@Nonnull final ProductModel source, @Nonnull final ProductData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		WileyFreeTrialProductModel relatedFreeTrialProduct = freeTrialProductService.getRelatedFreeTrialForProduct(source);
		if (relatedFreeTrialProduct != null)
		{
			target.setFreeTrialUrl(getProductModelUrlResolver().resolve(relatedFreeTrialProduct));
			target.setFreeTrialButtonLabel(relatedFreeTrialProduct.getPromt());
		} //else: the product hasn't corresponding "Free Trial" product, just ignore - ProductData is not populated
	}


	protected UrlResolver<ProductModel> getProductModelUrlResolver()
	{
		return productModelUrlResolver;
	}

	@Required
	public void setProductModelUrlResolver(final UrlResolver<ProductModel> productModelUrlResolver)
	{
		this.productModelUrlResolver = productModelUrlResolver;
	}

	public WileyFreeTrialProductService getFreeTrialProductService()
	{
		return freeTrialProductService;
	}

	@Required
	public void setFreeTrialProductService(final WileyFreeTrialProductService freeTrialProductService)
	{
		this.freeTrialProductService = freeTrialProductService;
	}
}
