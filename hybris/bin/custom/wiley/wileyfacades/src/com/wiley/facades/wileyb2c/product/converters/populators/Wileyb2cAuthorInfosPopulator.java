package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.platform.commercefacades.product.converters.populator.AbstractProductPopulator;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.model.AuthorInfoModel;
import com.wiley.facades.wileyb2c.product.converters.populators.helper.Wileyb2cProductCollectionAttributesHelper;


/**
 * Created by Uladzimir_Barouski on 2/23/2017.
 */
public class Wileyb2cAuthorInfosPopulator extends AbstractProductPopulator<ProductModel, ProductData>
{
	private Wileyb2cProductCollectionAttributesHelper wileyb2cProductCollectionAttributesHelper;

	@Override
	public void populate(final ProductModel productModel, final ProductData productData) throws ConversionException
	{
		ServicesUtil.validateParameterNotNullStandardMessage("productModel", productModel);
		ServicesUtil.validateParameterNotNullStandardMessage("productData", productData);

		final List<AuthorInfoModel> authorInformation =
				(List<AuthorInfoModel>) wileyb2cProductCollectionAttributesHelper.getProductAttribute(
						productModel, ProductModel.AUTHORINFOS);
		if (CollectionUtils.isNotEmpty(authorInformation))
		{
			String authorInfos = authorInformation
					.stream()
					.map(author -> getAuthorInfo(author))
					.collect(Collectors.joining(", "));
			productData.setAuthors(authorInfos);
		}
	}

	protected String getAuthorInfo(final AuthorInfoModel authorInfoModel)
	{
		StringBuilder authorInfo = new StringBuilder().append(authorInfoModel.getName());
		if (StringUtils.isNotEmpty(authorInfoModel.getRole()))
		{
			authorInfo.append(" (").append(authorInfoModel.getRole()).append(")");
		}

		return authorInfo.toString();
	}

	@Required
	public void setWileyb2cProductCollectionAttributesHelper(
			final Wileyb2cProductCollectionAttributesHelper wileyb2cProductCollectionAttributesHelper)
	{
		this.wileyb2cProductCollectionAttributesHelper = wileyb2cProductCollectionAttributesHelper;
	}
}
