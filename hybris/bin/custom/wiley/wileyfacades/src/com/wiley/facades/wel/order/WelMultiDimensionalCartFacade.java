package com.wiley.facades.wel.order;

import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;

import java.util.List;


/**
 * Facade extends functionality of {@link CartFacade}. The facade provides interface for working with multi-dimensional products
 *
 * @author Aliaksei_Zlobich
 */
public interface WelMultiDimensionalCartFacade extends CartFacade
{

	/**
	 * Add order entries to the session cart.
	 *
	 * @param cartEntries
	 * 		cart entries
	 * @return list of cart modification data
	 * @throws CommerceCartModificationException
	 * 		thrown if the cart cannot be modified
	 */
	List<CartModificationData> addOrderEntryList(List<OrderEntryData> cartEntries) throws CommerceCartModificationException;

	/**
	 * Add order entries to the given cart model.
	 *
	 * @param cartCode
	 * 		the cart code
	 * @param cartEntries
	 * 		cart entries
	 * @return list of cart modification data
	 * @throws CommerceCartModificationException
	 * 		thrown if the cart cannot be modified
	 */
	List<CartModificationData> addOrderEntryList(String cartCode, List<OrderEntryData> cartEntries)
			throws CommerceCartModificationException;

	/**
	 * Add order entries to the given cart model.
	 *
	 * @param productSku
	 * 		product code
	 * @param entryNumber
	 * 		entry number
	 * @param quantity
	 * 		quantity
	 * @return if product type change was successful
	 * @throws CommerceCartModificationException
	 * 		thrown if the cart cannot be modified
	 */
	boolean changeInCartProductType(String productSku, int entryNumber, long quantity) throws CommerceCartModificationException;

	/**
	 * Search and remove carts where Entries do not have products
	 */
	void searchAndRemoveBrokenCarts();

	/**
	 * Checks if current cart has Student coupon applied
	 */
	boolean hasStudentCoupon();

	/**
	 * Removes all entries from session cart
	 */
	void clearSessionCart();
}
