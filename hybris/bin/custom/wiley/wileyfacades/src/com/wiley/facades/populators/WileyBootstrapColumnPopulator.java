package com.wiley.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Required;

import com.wiley.bootstragrid.data.WileyBootstrapColumnData;
import com.wiley.core.model.components.bootstrapgrid.WileyBootstrapColumnConfigurationModel;
import com.wiley.core.model.components.bootstrapgrid.WileyBootstrapColumnModel;


public class WileyBootstrapColumnPopulator implements Populator<WileyBootstrapColumnModel, WileyBootstrapColumnData>
{
	private static final String BOOTSTRAP_CLASS_TEMPLATE = "col-%s-%d";
	private static final String BOOTSTRAP_CLASS_WITH_OFFSET_TEMPLATE = "col-%s-offset-%d";
	private static final String BOOTSTRAP_CLASS_HIDDEN_TEMPLATE = "hidden-%s";
	private Map<String, String> bootstrapClassMapping;

	@Override
	public void populate(final WileyBootstrapColumnModel model, final WileyBootstrapColumnData data) throws ConversionException
	{
		data.setCssClasses(
				Optional.ofNullable(model.getCssClasses())
						.orElseGet(Collections::emptyList)
						.stream()
						.toArray(String[]::new));

		data.setCssStyle(model.getCssStyle());

		data.setComponentUids(
				Optional.ofNullable(model.getComponents())
						.orElseGet(Collections::emptyList)
						.stream()
						.map(c -> c.getUid())
						.toArray(String[]::new));

		data.setBootstrapClasses(
				Optional.ofNullable(model.getConfigurations())
						.orElseGet(Collections::emptySet)
						.stream()
						.map(this::convertToBootstrapClasses)
						.filter(Objects::nonNull)
						.toArray(String[]::new));
	}

	protected String convertToBootstrapClasses(final WileyBootstrapColumnConfigurationModel model)
	{
		final String suffix = getBootstrapClassMapping().get(model.getBreakpoint().getQualifier());
		if (suffix == null)
		{
			return null;
		}

		if (model.getVisible())
		{
			StringBuilder sb = new StringBuilder(String.format(BOOTSTRAP_CLASS_TEMPLATE, suffix, model.getWidth()));
			sb = sb.append(" ").append(String.format(BOOTSTRAP_CLASS_WITH_OFFSET_TEMPLATE, suffix, model.getOffset()));
			return sb.toString();
		}
		else
		{
			return String.format(BOOTSTRAP_CLASS_HIDDEN_TEMPLATE, suffix);
		}
	}

	public Map<String, String> getBootstrapClassMapping()
	{
		return bootstrapClassMapping;
	}

	@Required
	public void setBootstrapClassMapping(final Map<String, String> bootstrapClassMapping)
	{
		this.bootstrapClassMapping = bootstrapClassMapping;
	}
}
