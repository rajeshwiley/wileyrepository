package com.wiley.facades.converters;

import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.FeatureData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.VariantMatrixElementData;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.util.localization.Localization;
import de.hybris.platform.variants.model.VariantValueCategoryModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PropertyComparator;
import org.springframework.util.Assert;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.product.WileyProductService;
import com.wiley.facades.product.data.CompareTableCellData;
import com.wiley.facades.product.data.CompareTableData;
import com.wiley.facades.product.data.CompareTableFeatureRowData;
import com.wiley.facades.product.data.CompareTableLevelRowData;


/**
 * Created by Uladzimir_Barouski on 1/21/2016.
 */
public class WileyProductCompareTableCFAConverter implements Converter<List<ProductData>, CompareTableData>
{

	@Autowired
	private WileyProductService wileyProductService;

	@Autowired
	private CategoryService categoryService;

	private static final String PRICE_VALUE_FIELD = "priceRange.maxPrice.value";
	private static final String ROW_TITLE_FIELD = "rowTitle";
	private static final String PLP_TITLE_PREORDER_PRODUCT = "plp.title.preorder.product";
	private static final String PLP_TITLE_ACTIVE_PRODUCT = "plp.title.active.product";

	@Override
	public CompareTableData convert(final List<ProductData> productDataList) throws ConversionException
	{
		return convert(productDataList, new CompareTableData());
	}

	@Override
	public CompareTableData convert(final List<ProductData> productDataList, final CompareTableData compareTableData)
			throws ConversionException
	{
		return new ConverterRunner(productDataList, compareTableData).invoke();
	}

	private class ConverterRunner
	{
		private final List<ProductData> productDataList;
		private final CompareTableData compareTableData;
		private List<CompareTableLevelRowData> activeProductLevels;
		private List<CompareTableLevelRowData> nextVersionProductLevels;
		private Map<String, Map<String, List<CompareTableCellData>>> footerProductData;
		private Collection<CategoryModel> levelCategoriesList;

		ConverterRunner(final List<ProductData> productDataList, final CompareTableData compareTableData)
		{
			this.productDataList = productDataList;
			this.compareTableData = compareTableData;

			activeProductLevels = new ArrayList<CompareTableLevelRowData>();
			nextVersionProductLevels = new ArrayList<CompareTableLevelRowData>();

			footerProductData = new TreeMap<String, Map<String, List<CompareTableCellData>>>();

			levelCategoriesList = getLevelCategoriesList();
		}

		public CompareTableData invoke()
		{
			Assert.notNull(productDataList, "Parameter productDataList cannot be null.");
			Assert.notNull(compareTableData, "Parameter compareTableData cannot be null.");
			//sort products by maxprice decrease
			PropertyComparator productPriceComparator = new PropertyComparator(PRICE_VALUE_FIELD, true, false);
			Collections.sort(productDataList, productPriceComparator);
			populateTableHeader();
			populateTableLevels();
			populateFeatureOptions();
			return compareTableData;
		}

		/**
		 * Populates header for CFA Compare table
		 */
		private void populateTableHeader()
		{
			Map<String, String> tableHeader = new LinkedHashMap<String, String>();
			Map<String, ProductData> tableHeaderProductData = new LinkedHashMap<String, ProductData>();
			for (ProductData productData : productDataList)
			{
				tableHeader.put(productData.getCode(), productData.getName());
				tableHeaderProductData.put(productData.getCode(), productData);
			}
			compareTableData.setHeader(tableHeader);
			compareTableData.setHeaderProductData(tableHeaderProductData);
		}

		private void populateTableLevels()
		{
			for (CategoryModel levelSubCategory : levelCategoriesList)
			{
				addProductLevel(levelSubCategory, PLP_TITLE_ACTIVE_PRODUCT, false);
				addProductLevel(levelSubCategory, PLP_TITLE_PREORDER_PRODUCT, true);
			}

			joinHeaderRows();

			sortRowsByName();

			compareTableData.setLevels(activeProductLevels);
			compareTableData.setFooterProductData(footerProductData);
		}

		private void joinHeaderRows()
		{
			activeProductLevels.addAll(nextVersionProductLevels);
		}

		private void addProductLevel(final CategoryModel levelSubCategory, final String title, final boolean nextVersion)
		{
			addRowForCategory(levelSubCategory, title, nextVersion);
			addFooterLevelLinkForCategory(levelSubCategory, title, nextVersion);
		}

		private void addRowForCategory(final CategoryModel levelSubCategory, final String title, final boolean nextVersion)
		{
			CompareTableLevelRowData productLevelRow = getRowLevelForCategory(levelSubCategory, title, nextVersion);
			if (nextVersion)
			{
				nextVersionProductLevels.add(productLevelRow);
			}
			else
			{
				activeProductLevels.add(productLevelRow);
			}
		}

		private void sortRowsByName()
		{
			PropertyComparator rowTitleComparator = new PropertyComparator(ROW_TITLE_FIELD, true, true);
			Collections.sort(activeProductLevels, rowTitleComparator);
		}

		/**
		 * Populates features compare section for CFA Compare table
		 */
		private void populateFeatureOptions()
		{
			Map<String, CompareTableFeatureRowData> compareTableFeatureData =
					new LinkedHashMap<String, CompareTableFeatureRowData>();
			for (ProductData productData : productDataList)
			{
				List<FeatureData> features = productData.getIncludeFeatures();
				if (CollectionUtils.isNotEmpty(features))
				{
					for (FeatureData featureData : features)
					{
						CompareTableFeatureRowData featureRowData = compareTableFeatureData.get(featureData.getCode());
						Map<String, Boolean> featuresRow = null;
						if (featureRowData == null)
						{
							featuresRow = new HashMap<String, Boolean>();
							featureRowData = new CompareTableFeatureRowData();
							featureRowData.setFeatureName(featureData.getName());
							featureRowData.setFeatureAvailability(featuresRow);
						}
						else
						{
							featuresRow = featureRowData.getFeatureAvailability();
						}
						featuresRow.put(productData.getCode(), true);
						compareTableFeatureData.put(featureData.getCode(), featureRowData);
					}
				}
			}
			compareTableData.setCompareOptions(compareTableFeatureData);
		}

		private CompareTableLevelRowData getRowLevelForCategory(final CategoryModel levelSubCategory, final String rowTitle,
				final boolean nextVersion)
		{
			CompareTableLevelRowData levelRow = new CompareTableLevelRowData();
			levelRow.setRowTitle(String.format("%s %S", Localization.getLocalizedString(rowTitle), levelSubCategory.getName()));
			levelRow.setPricesData(getRowPrices(levelSubCategory, nextVersion));

			return levelRow;
		}

		private Map<String, PriceData> getRowPrices(final CategoryModel levelSubCategory, final boolean nextVersion)
		{
			Map<String, PriceData> pricesDataMap = new HashMap<String, PriceData>();

			for (ProductData productData : productDataList)
			{
				VariantMatrixElementData variantElement = getVariantMatrixElement(levelSubCategory, productData, nextVersion);
				addPrice(variantElement, productData, pricesDataMap);
			}
			return pricesDataMap;
		}

		private void addPrice(final VariantMatrixElementData variantElement, final ProductData productData,
				final Map<String, PriceData> pricesDataMap)
		{
			if (variantElement != null)
			{
				pricesDataMap.put(productData.getCode(), variantElement.getVariantOption().getPriceData());
			}
		}

		private void addFooterLevelLinkForCategory(final CategoryModel levelSubCategory, final String rowTitle,
				final boolean nextVersion)
		{

			final String activeProductTitle = Localization.getLocalizedString(rowTitle);

			for (ProductData productData : productDataList)
			{
				VariantMatrixElementData variantElement = getVariantMatrixElement(levelSubCategory, productData, nextVersion);
				addLevelLinkMappingToFooter(levelSubCategory, activeProductTitle, productData, variantElement);
			}
		}

		private void addLevelLinkMappingToFooter(final CategoryModel levelSubCategory, final String productTitle,
				final ProductData productData,
				final VariantMatrixElementData variantElement)
		{
			if (variantElement != null)
			{
				List<CompareTableCellData> productLevelsCell = getProductLevelsCell(productTitle, productData);
				CompareTableCellData productLevel = new CompareTableCellData();
				productLevel.setLevel(levelSubCategory.getName());
				productLevel.setUrl(variantElement.getVariantOption().getUrl());
				productLevelsCell.add(productLevel);
			}
		}

		private Map<String, List<CompareTableCellData>> getFooterRow(final String productTitle)
		{
			footerProductData.computeIfAbsent(productTitle, footerRow -> new TreeMap<>());
			return footerProductData.get(productTitle);
		}

		private List<CompareTableCellData> getProductLevelsCell(final String productTitle, final ProductData productData)
		{
			Map<String, List<CompareTableCellData>> footerRow = getFooterRow(productTitle);
			return footerRow.computeIfAbsent(productData.getCode(), footerCell -> new ArrayList<>());
		}

		private VariantMatrixElementData getVariantMatrixElement(final CategoryModel levelSubCategory,
				final ProductData productData, final boolean nextVersion)
		{
			VariantMatrixElementData variantElement = getVariantElementWithDefaultPrice(productData.getVariantMatrix(),
					levelSubCategory.getCode(), null);

			final boolean currentProductContainsNextVersion =
					variantElement != null && variantElement.getVariantOption() != null && CollectionUtils.isNotEmpty(
							variantElement.getVariantOption().getPreOrderNodes());

			if (nextVersion && currentProductContainsNextVersion)
			{
				final ProductData nextVersionProduct = variantElement.getVariantOption().getPreOrderNodes().get(0);
				variantElement = getVariantElementWithDefaultPrice(nextVersionProduct.getVariantMatrix(),
						levelSubCategory.getCode(), null);
			} else if (nextVersion) {
				variantElement = null;
			}
			return variantElement;
		}

		private Collection<CategoryModel> getLevelCategoriesList()
		{
			CategoryModel levelVariantCategory = categoryService.getCategoryForCode(
					WileyCoreConstants.WEL_CFA_LEVEL_CATEGORY_CODE);
			Collection<CategoryModel> levelCategoriesList = levelVariantCategory.getCategories();
			levelCategoriesList = levelCategoriesList.stream()
					.filter(VariantValueCategoryModel.class::isInstance)
					.collect(Collectors.toList());
			return levelCategoriesList;
		}

		/**
		 * Returns Variant Element with min price for passed levelCode and variantElementList.
		 *
		 * @param variantElementsList
		 * @param levelCode
		 * @return VariantMatrixElementData
		 */
		private VariantMatrixElementData getVariantElementWithDefaultPrice(
				final List<VariantMatrixElementData> variantElementsList,
				final String levelCode,
				final VariantMatrixElementData levelElement)
		{
			VariantMatrixElementData levelElementWithDefaultPrice = levelElement;
			if (CollectionUtils.isNotEmpty(variantElementsList))
			{
				for (VariantMatrixElementData variantElement : variantElementsList)
				{
					// we work only with level categories
					if (!WileyCoreConstants.WEL_CFA_LEVEL_CATEGORY_CODE.equals(
							variantElement.getParentVariantCategory().getCode())
							|| levelCode.equals(variantElement.getVariantValueCategory().getCode()))
					{
						if (variantElement.getIsLeaf())
						{
							levelElementWithDefaultPrice = getVariantElementWithDefaultPriceData(levelElementWithDefaultPrice,
									variantElement);
						}
						else
						{
							levelElementWithDefaultPrice = getVariantElementWithDefaultPrice(variantElement.getElements(),
									levelCode,
									levelElementWithDefaultPrice);
						}
					}
				}
			}
			return levelElementWithDefaultPrice;
		}

		private VariantMatrixElementData getVariantElementWithDefaultPriceData(final VariantMatrixElementData v1,
				final VariantMatrixElementData v2)
		{

			if (v1 != null && v2 != null)
			{
				return v1.getVariantValueCategory().getSequence() < v2.getVariantValueCategory().getSequence() ? v1 : v2;
			}
			return v1 != null ? v1 : v2;

		}
	}
}
