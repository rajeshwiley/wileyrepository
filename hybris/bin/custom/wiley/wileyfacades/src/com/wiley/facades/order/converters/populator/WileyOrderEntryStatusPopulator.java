package com.wiley.facades.order.converters.populator;

import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

public class WileyOrderEntryStatusPopulator implements Populator<AbstractOrderEntryModel, OrderEntryData>
{

    @Override
    public void populate(final AbstractOrderEntryModel source, final OrderEntryData target)
            throws ConversionException
    {
        if (source.getStatus() != null)
        {
            target.setStatus(source.getStatus());
        }
    }
}
