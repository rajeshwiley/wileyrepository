package com.wiley.facades.wileycom.customer.converters.populators;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.commercefacades.order.converters.populator.ConsignmentPopulator;
import de.hybris.platform.commercefacades.order.data.ConsignmentData;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import javax.validation.constraints.NotNull;

import org.springframework.util.Assert;


/**
 * Created by Mikhail_Asadchy on 8/11/2016.
 */
public class WileycomConsignmentPopulator extends ConsignmentPopulator
{
	@Override
	public void populate(@NotNull final ConsignmentModel source, @NotNull final ConsignmentData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setCode(source.getCode());
		target.setTrackingID(source.getTrackingID());
		target.setStatus(source.getStatus());

		if (ConsignmentStatus.SHIPPED.equals(source.getStatus()) || ConsignmentStatus.READY_FOR_PICKUP.equals(source.getStatus()))
		{
			target.setStatusDate(source.getShippingDate());
		}
		if (source.getDeliveryPointOfService() != null)
		{
			target.setDeliveryPointOfService(getPointOfServiceConverter().convert(source.getDeliveryPointOfService()));
		}
		if (source.getShippingAddress() != null)
		{
			target.setShippingAddress(getAddressConverter().convert(source.getShippingAddress()));
		}
	}
}
