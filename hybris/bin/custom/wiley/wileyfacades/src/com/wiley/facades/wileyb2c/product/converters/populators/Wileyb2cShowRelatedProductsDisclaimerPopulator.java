package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.catalog.references.ProductReferenceService;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.variants.model.VariantProductModel;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;


/**
 * Created by Raman_Hancharou on 6/13/2017.
 */
public class Wileyb2cShowRelatedProductsDisclaimerPopulator implements Populator<ProductModel, ProductData>
{

	private ProductReferenceService productReferenceService;

	@Override
	public void populate(final ProductModel productModel, final ProductData productData) throws ConversionException
	{
		ServicesUtil.validateParameterNotNullStandardMessage("productModel", productModel);
		ServicesUtil.validateParameterNotNullStandardMessage("productData", productData);

		boolean showRelatedProductsDisclaimer = false;

		for (VariantProductModel purchaseOptionProductModel : productModel.getVariants())
		{
			if (CollectionUtils.isNotEmpty(productReferenceService
					.getProductReferencesForSourceProduct(purchaseOptionProductModel, ProductReferenceTypeEnum.RELATED,
							Boolean.TRUE)))
			{
				showRelatedProductsDisclaimer = true;
				break;
			}
		}
		productData.setShowRelatedProductsDisclaimer(showRelatedProductsDisclaimer);
	}

	@Required
	public void setProductReferenceService(
			final ProductReferenceService productReferenceService)
	{
		this.productReferenceService = productReferenceService;
	}
}
