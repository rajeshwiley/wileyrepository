/**
 * 
 */
package com.wiley.facades.order.converters.populator;

import de.hybris.platform.commercefacades.order.converters.populator.CreditCardPaymentInfoPopulator;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;

public class WELCreditCardPaymentInfoPopulator extends CreditCardPaymentInfoPopulator
{
  public static final int YEARS_TO_ADD = 2000;
  public static final int DIGTIS_TO_DISPLAY = 4;
  
  @Override
  public void populate(final CreditCardPaymentInfoModel source, final CCPaymentInfoData target) 
  {
    super.populate(source, target);
    
    target.setCardNumber(target.getCardNumber().substring(target.getCardNumber().length() - DIGTIS_TO_DISPLAY));
    target.setExpiryYear(target.getExpiryYear());
  }
}
