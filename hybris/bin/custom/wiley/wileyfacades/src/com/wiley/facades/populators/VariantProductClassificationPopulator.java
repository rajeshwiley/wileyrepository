package com.wiley.facades.populators;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.variants.model.VariantProductModel;


public class VariantProductClassificationPopulator extends ProductClassificationPopulator
{
	@Override
	public void populate(final ProductModel productModel, final ProductData productData) throws ConversionException
	{
		if (productModel instanceof VariantProductModel)
		{
			super.populate(((VariantProductModel) productModel).getBaseProduct(), productData);
		}
		else
		{
			super.populate(productModel, productData);
		}
	}
}
