package com.wiley.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import javax.annotation.Nonnull;

import com.wiley.facades.wileyb2c.product.data.WebLinkData;
import com.wiley.facades.wileyb2c.product.data.WileyPlusCourseData;
import com.wiley.facades.wileyb2c.product.data.WileyPlusCourseInfoData;
import com.wiley.facades.wileyplus.cart.activation.dto.CartActivationRequestDto;
import com.wiley.facades.wileyplus.cart.activation.dto.WebLinkDto;
import com.wiley.facades.wileyplus.cart.activation.dto.WileyPlusCourseDto;

import org.apache.commons.lang3.StringUtils;


/**
 * Created by Anton_Lukyanau on 8/23/2016.
 */
public class Wileyb2cWileyPlusCourseDataPopulator implements Populator<CartActivationRequestDto, WileyPlusCourseInfoData>
{
	@Override
	public void populate(@Nonnull final CartActivationRequestDto cartActivationRequestDto,
			@Nonnull final WileyPlusCourseInfoData wileyPlusCourseInfoData)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("cartActivationRequestDto", cartActivationRequestDto);
		ServicesUtil.validateParameterNotNullStandardMessage("wileyPlusCourseInfoData", wileyPlusCourseInfoData);

		final WileyPlusCourseDto course = cartActivationRequestDto.getCourse();
		ServicesUtil.validateParameterNotNullStandardMessage("cartActivationRequestDto.course", wileyPlusCourseInfoData);

		WebLinkDto returnUrlDto = cartActivationRequestDto.getReturnURL();
		WebLinkData returnUrl = null;
		if (returnUrlDto != null) {
			returnUrl = new WebLinkData();
			returnUrl.setUrl(returnUrlDto.getUrl());
			returnUrl.setMessage(returnUrlDto.getMessage());
		}

		final WileyPlusCourseData wileyPlusCourseData = new WileyPlusCourseData();
		if (course != null) { //NOSONAR: getter is annotated as @NotNull
			wileyPlusCourseData.setAuthors(course.getAuthors());
			wileyPlusCourseData.setImageUrl(course.getImageUrl());
			wileyPlusCourseData.setIsbn(course.getIsbn());
			wileyPlusCourseData.setTitle(course.getTitle());
			wileyPlusCourseData.setReturnUrl(returnUrl);
		} else {
			wileyPlusCourseData.setAuthors(StringUtils.EMPTY);
			wileyPlusCourseData.setImageUrl(StringUtils.EMPTY);
			wileyPlusCourseData.setIsbn(StringUtils.EMPTY);
			wileyPlusCourseData.setTitle(StringUtils.EMPTY);
		}
		wileyPlusCourseInfoData.setCourse(wileyPlusCourseData);
	}
}
