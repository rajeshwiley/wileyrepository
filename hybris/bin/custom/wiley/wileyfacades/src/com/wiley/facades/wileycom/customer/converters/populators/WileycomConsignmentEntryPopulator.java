package com.wiley.facades.wileycom.customer.converters.populators;

import de.hybris.platform.commercefacades.order.data.ConsignmentData;
import de.hybris.platform.commercefacades.order.data.ConsignmentEntryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.validation.constraints.NotNull;


/**
 * Created by Mikhail_Asadchy on 8/11/2016.
 */
public class WileycomConsignmentEntryPopulator implements Populator<ConsignmentEntryModel, ConsignmentEntryData>
{
	private Converter<ConsignmentModel, ConsignmentData> consignmentConverter;

	@Override
	public void populate(@NotNull final ConsignmentEntryModel source, @NotNull final ConsignmentEntryData target)
	{
		target.setConsignment(consignmentConverter.convert(source.getConsignment()));
	}

	public Converter<ConsignmentModel, ConsignmentData> getConsignmentConverter()
	{
		return consignmentConverter;
	}

	public void setConsignmentConverter(
			final Converter<ConsignmentModel, ConsignmentData> consignmentConverter)
	{
		this.consignmentConverter = consignmentConverter;
	}
}
