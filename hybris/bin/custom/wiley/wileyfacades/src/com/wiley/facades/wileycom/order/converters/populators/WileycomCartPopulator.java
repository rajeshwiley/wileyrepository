package com.wiley.facades.wileycom.order.converters.populators;

import de.hybris.platform.commercefacades.order.converters.populator.AbstractOrderPopulator;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.core.model.order.CartModel;

import javax.annotation.Resource;

import com.wiley.core.enums.ProductEditionFormat;
import com.wiley.facades.wileycom.abstractOrder.WileycomShowDeliveryCostAbstractOrderPopulator;


public final class WileycomCartPopulator extends AbstractOrderPopulator<CartModel, CartData>
{
	@Resource
	private WileycomShowDeliveryCostAbstractOrderPopulator wileycomShowDeliveryCostAbstractOrderPopulator;

	@Override
	public void populate(final CartModel source, final CartData target)
	{
		target.setSubTotalWithoutDiscount(createPrice(source, source.getSubTotalWithoutDiscount()));
		target.setTaxCalculated(source.getTaxCalculated());
		long nonDigitalItemsCount = calculateNonDigitalItems(source);
		target.setPhysicalItemsQuantity(nonDigitalItemsCount);
		target.setDigitalCart(nonDigitalItemsCount == 0);
		wileycomShowDeliveryCostAbstractOrderPopulator.populate(source, target);
	}

	private long calculateNonDigitalItems(final CartModel source)
	{
		return source.getEntries()
				.stream()
				.filter(entry -> entry.getProduct().getEditionFormat() != ProductEditionFormat.DIGITAL)
				.count();
	}

}
