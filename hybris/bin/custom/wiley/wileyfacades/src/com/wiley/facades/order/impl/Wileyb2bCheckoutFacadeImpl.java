package com.wiley.facades.order.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BUnitService;
import de.hybris.platform.b2bacceleratorfacades.checkout.data.PlaceOrderData;
import de.hybris.platform.b2bacceleratorfacades.exception.EntityValidationException;
import de.hybris.platform.b2bacceleratorfacades.order.data.B2BCommentData;
import de.hybris.platform.b2bacceleratorfacades.order.data.B2BDaysOfWeekData;
import de.hybris.platform.b2bacceleratorfacades.order.data.B2BPaymentTypeData;
import de.hybris.platform.b2bacceleratorfacades.order.data.ScheduledCartData;
import de.hybris.platform.b2bacceleratorfacades.order.data.TriggerData;
import de.hybris.platform.b2bacceleratorfacades.order.impl.DefaultB2BCheckoutFacade;
import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.b2bcommercefacades.company.data.B2BCostCenterData;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.DeliveryModeData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.InvoicePaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;

import java.text.ParseException;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Nullable;
import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.base.Preconditions;
import com.wiley.core.b2b.unit.service.WileyB2BUnitExternalService;
import com.wiley.core.model.ExternalDeliveryModeModel;
import com.wiley.core.order.WileyB2BCheckoutService;
import com.wiley.core.util.WileyDateUtils;
import com.wiley.facades.order.Wileyb2bCheckoutFacade;
import com.wiley.facades.wileycom.order.WileycomCheckoutFacadeImpl;

import static de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType.CARD;
import static de.hybris.platform.payment.dto.TransactionStatus.ACCEPTED;
import static de.hybris.platform.payment.enums.PaymentTransactionType.AUTHORIZATION;
import static de.hybris.platform.util.localization.Localization.getLocalizedString;


public class Wileyb2bCheckoutFacadeImpl extends WileycomCheckoutFacadeImpl implements Wileyb2bCheckoutFacade
{
	public static final String DATE_PATTERN = "MM/dd/yyyy";
	private static final Logger LOG = Logger.getLogger(Wileyb2bCheckoutFacadeImpl.class);
	private static final String CART_CHECKOUT_DELIVERYADDRESS_INVALID = "cart.deliveryAddress.invalid";
	private static final String CART_CHECKOUT_DELIVERYMODE_INVALID = "cart.deliveryMode.invalid";
	private static final String CART_CHECKOUT_PAYMENTINFO_EMPTY = "cart.paymentInfo.empty";
	private static final String CART_CHECKOUT_NOT_CALCULATED = "cart.not.calculated";
	private static final String CHECKOUT_CART_NULL = "Checkout Cart is null";
	private static final String CART_CHECKOUT_TRANSACTION_NOT_AUTHORIZED = "cart.transation.notAuthorized";
	private static final String CART_CHECKOUT_TERM_UNCHECKED = "cart.term.unchecked";
	private static final String CART_CHECKOUT_NO_QUOTE_DESCRIPTION = "cart.no.quote.description";

	@Autowired
	private WileyB2BCheckoutService wileyB2BCheckoutService;

	@Autowired
	private WileyB2BUnitExternalService wileyB2BUnitExternalService;

	@Autowired
	private DefaultB2BCheckoutFacade wileyOOTBB2BCheckoutFacade; // it's needed to keep OOTB behavior.

	//TODO-1808 review if it's the right bean we need for this class
	@Resource
	private B2BUnitService<B2BUnitModel, B2BCustomerModel> defaultB2BUnitService;

	@Override
	public boolean authorizePayment(final String securityCode)
	{
		final CartModel cartModel = getCart();
		boolean result = false;
		if (cartModel != null)
		{
			if (checkIfCurrentUserIsTheCartUser() && cartModel.getPaymentInfo() instanceof InvoicePaymentInfoModel)
			{
				result = true;
			}
			else
			{
				result = !(getCart().getPaymentInfo() instanceof CreditCardPaymentInfoModel) || super.authorizePayment(
						securityCode);
			}
		}
		return result;
	}

	private void checkValidCheckoutCart()
	{
		final CartData cartData = getCheckoutCart();

		if (!cartData.isCalculated())
		{
			throw new EntityValidationException(getLocalizedString(CART_CHECKOUT_NOT_CALCULATED));
		}

		if (!isDigitalSessionCart())
		{
			if (cartData.getDeliveryAddress() == null)
			{
				throw new EntityValidationException(getLocalizedString(CART_CHECKOUT_DELIVERYADDRESS_INVALID));
			}

			if (cartData.getDeliveryMode() == null && !getSupportedDeliveryModes().isEmpty())
			{
				throw new EntityValidationException(getLocalizedString(CART_CHECKOUT_DELIVERYMODE_INVALID));
			}
		}

		final boolean cardPaymentType = cartData.getPaymentType() != null
				&& CARD.getCode().equals(cartData.getPaymentType().getCode());

		if (cardPaymentType && cartData.getPaymentInfo() == null)
		{
			throw new EntityValidationException(getLocalizedString(CART_CHECKOUT_PAYMENTINFO_EMPTY));
		}
	}

	@Override
	public void savePoNumberInCart(final CartData cartData)
	{
		final CartModel cartModel = getCart();

		if (cartModel == null)
		{
			LOG.error(CHECKOUT_CART_NULL);
			return;
		}

		if (CollectionUtils.isNotEmpty(cartData.getEntries()))
		{
			setOrderEntriesPoNumber(cartModel.getEntries(), cartData.getEntries());
		}
	}

	private void setOrderEntriesPoNumber(final List<AbstractOrderEntryModel> cartModelEntries,
			final List<OrderEntryData> cartDataEntries)
	{
		for (AbstractOrderEntryModel modelEntry : cartModelEntries)
		{
			for (OrderEntryData entry : cartDataEntries)
			{
				if (entry.getEntryNumber().equals(modelEntry.getEntryNumber()))
				{
					modelEntry.setPoNumber(entry.getPoNumber());
					getModelService().save(modelEntry);
				}
			}
		}
	}

	@Override
	public List<String> getInvalidPoNumbers(final CartData cartData)
	{
		List<String> invalidPoNumbers = Collections.emptyList();

		final CartModel cartModel = getCart();
		Preconditions.checkNotNull(cartModel, CHECKOUT_CART_NULL);

		final UserModel user = cartModel.getUser();
		if (!(user instanceof B2BCustomerModel))
		{
			LOG.error("Expected an instance of CustomerModel. Invalid user: " + user.getUid());
		}
		else
		{
			final B2BUnitModel defaultB2BUnit = ((B2BCustomerModel) user).getDefaultB2BUnit();
			Preconditions.checkNotNull(defaultB2BUnit, "Default B2B Unit should not be null");
			final String sapAccountNumber = defaultB2BUnit.getSapAccountNumber();

			if (StringUtils.isNoneBlank(sapAccountNumber))
			{
				final List<String> poNumbers = cartData.getEntries().stream()
						.map(OrderEntryData::getPoNumber)
						.collect(Collectors.toList());

				invalidPoNumbers = wileyB2BUnitExternalService.validatePONumbers(sapAccountNumber, poNumbers);
			}
			else
			{
				LOG.error("Unable to verify PO numbers. SAP account number is not set for the user: " + user.getUid());
			}

		}

		return invalidPoNumbers;
	}

	@Override
	public void saveOrderMessage(final String orderMessage)
	{
		final CartModel cartModel = getCart();
		if (cartModel == null)
		{
			LOG.error(CHECKOUT_CART_NULL);
			return;
		}
		cartModel.setUserNotes(orderMessage);
		getModelService().save(cartModel);
	}

	@Override
	public void setDesiredShippingDate(@Nullable final String desiredShippingDateString)
	{
		if (StringUtils.isEmpty(desiredShippingDateString))
		{
			return;
		}

		try
		{
			Date desiredShippingDate = WileyDateUtils.convertDateStringToDate(desiredShippingDateString, DATE_PATTERN);
			wileyB2BCheckoutService.setDesiredShippingDate(desiredShippingDate);
		}
		catch (ParseException e)
		{
			LOG.error("Can't convert string: " + desiredShippingDateString + " to date.", e);
		}
	}

	protected DeliveryModeData convert(final DeliveryModeModel deliveryModeModel)
	{
		DeliveryModeData deliveryModeData = getDeliveryModeConverter().convert(deliveryModeModel);
		if (deliveryModeModel instanceof ExternalDeliveryModeModel)
		{
			deliveryModeData.setExternalCode(((ExternalDeliveryModeModel) deliveryModeModel).getExternalCode());
		}
		return deliveryModeData;
	}

	@Override
	public <T extends AbstractOrderData> T placeOrder(final PlaceOrderData placeOrderData)
			throws InvalidCartException
	{
		// term must be checked
		if (!placeOrderData.getTermsCheck().equals(Boolean.TRUE))
		{
			throw new EntityValidationException(getLocalizedString(CART_CHECKOUT_TERM_UNCHECKED));
		}

		final CartModel cart = getCart();
		if (isCardPaymentType(cart.getPaymentType()) && !isZeroTotalOrder() && !cartHasAuthorizedTransaction(cart))
		{
			// FIXME - change error message
			throw new EntityValidationException(getLocalizedString(CART_CHECKOUT_TRANSACTION_NOT_AUTHORIZED));
		}

		checkValidCheckoutCart();

		// validate quote negotiation
		if (placeOrderData.getNegotiateQuote() != null && placeOrderData.getNegotiateQuote().equals(Boolean.TRUE))
		{
			if (StringUtils.isBlank(placeOrderData.getQuoteRequestDescription()))
			{
				throw new EntityValidationException(getLocalizedString(CART_CHECKOUT_NO_QUOTE_DESCRIPTION));
			}
			else
			{
				final B2BCommentData b2BComment = new B2BCommentData();
				b2BComment.setComment(placeOrderData.getQuoteRequestDescription());

				final CartData cartData = new CartData();
				cartData.setB2BComment(b2BComment);

				updateCheckoutCart(cartData);
			}
		}

		return (T) super.placeOrder();
	}

	private boolean cartHasAuthorizedTransaction(final CartModel cart)
	{
		for (final PaymentTransactionModel transaction : cart.getPaymentTransactions())
		{
			for (final PaymentTransactionEntryModel entry : transaction.getEntries())
			{
				if (AUTHORIZATION.equals(entry.getType()) && ACCEPTED.name().equals(entry.getTransactionStatus()))
				{
					return true;
				}
			}
		}

		return false;
	}

	@Override
	public List<? extends B2BCostCenterData> getVisibleCostCenters()
	{
		return wileyOOTBB2BCheckoutFacade.getVisibleCostCenters();
	}

	@Override
	public List<B2BCostCenterData> getActiveVisibleCostCenters()
	{
		return wileyOOTBB2BCheckoutFacade.getActiveVisibleCostCenters();
	}

	@Override
	public CartData updateCheckoutCart(final CartData cartData)
	{
		return wileyOOTBB2BCheckoutFacade.updateCheckoutCart(cartData);
	}

	@Override
	public CartData setCostCenterForCart(final String costCenterCode, final String orderCode)
	{
		return wileyOOTBB2BCheckoutFacade.setCostCenterForCart(costCenterCode, orderCode);
	}

	@Override
	public List<B2BPaymentTypeData> getPaymentTypes()
	{
		return wileyOOTBB2BCheckoutFacade.getPaymentTypes();
	}

	@Override
	public List<B2BPaymentTypeData> getPaymentTypesForCheckoutSummary()
	{
		return wileyOOTBB2BCheckoutFacade.getPaymentTypesForCheckoutSummary();
	}

	@Override
	public boolean setPaymentTypeSelectedForCheckout(final String paymentType)
	{
		return wileyOOTBB2BCheckoutFacade.setPaymentTypeSelectedForCheckout(paymentType);
	}

	@Override
	public boolean setPurchaseOrderNumber(final String purchaseOrderNumber)
	{
		return wileyOOTBB2BCheckoutFacade.setPurchaseOrderNumber(purchaseOrderNumber);
	}

	@Override
	public boolean setQuoteRequestDescription(final String quoteRequestDescription)
	{
		return wileyOOTBB2BCheckoutFacade.setQuoteRequestDescription(quoteRequestDescription);
	}

	@Override
	public List<B2BDaysOfWeekData> getDaysOfWeekForReplenishmentCheckoutSummary()
	{
		return wileyOOTBB2BCheckoutFacade.getDaysOfWeekForReplenishmentCheckoutSummary();
	}

	@Override
	public ScheduledCartData scheduleOrder(final TriggerData trigger)
	{
		return wileyOOTBB2BCheckoutFacade.scheduleOrder(trigger);
	}

	@Override
	public void createCartFromOrder(final String orderCode)
	{
		wileyOOTBB2BCheckoutFacade.createCartFromOrder(orderCode);
	}

	@Override
	public List<? extends CommerceCartModification> validateSessionCart() throws CommerceCartModificationException
	{
		return wileyOOTBB2BCheckoutFacade.validateSessionCart();
	}

	@Override
	public void setDefaultPaymentTypeForCheckout()
	{
		wileyOOTBB2BCheckoutFacade.setDefaultPaymentTypeForCheckout();
	}

	@Override
	protected void beforePlaceOrder(final CartModel cartModel)
	{
		super.beforePlaceOrder(cartModel);

		final boolean isQuoteOrder = !cartModel.getB2bcomments().isEmpty();
		if (isQuoteOrder)
		{
			cartModel.setStatus(OrderStatus.PENDING_QUOTE);
		}
		else
		{
			cartModel.setStatus(OrderStatus.CREATED);
		}
	}

	@Override
	public void setPaymentAddressFromUnit()
	{
		final CartModel cartModel = getCart();
		Preconditions.checkNotNull(cartModel, CHECKOUT_CART_NULL);

		final B2BCustomerModel user = (B2BCustomerModel) cartModel.getUser();
		B2BUnitModel unit = defaultB2BUnitService.getParent(user);
		Preconditions.checkNotNull(unit, "B2B unit is not set for user: " + user.getUid());
		AddressModel unitDefaultBillingAddress = unit.getBillingAddress();
		if (unitDefaultBillingAddress == null)
		{
			return;
		}
		setPaymentAddress(unitDefaultBillingAddress);
	}

	private boolean isCardPaymentType(final CheckoutPaymentType paymentType)
	{
		return paymentType != null && CARD.getCode().equals(paymentType.getCode());
	}

	public void setDefaultB2BUnitService(final B2BUnitService<B2BUnitModel, B2BCustomerModel> defaultB2BUnitService)
	{
		this.defaultB2BUnitService = defaultB2BUnitService;
	}

	//TODO-1808 chech if we really need this getter (it's better to remove it than to leave or use instead of field)
	public B2BUnitService<B2BUnitModel, B2BCustomerModel> getDefaultB2BUnitService()
	{
		return defaultB2BUnitService;
	}
}
