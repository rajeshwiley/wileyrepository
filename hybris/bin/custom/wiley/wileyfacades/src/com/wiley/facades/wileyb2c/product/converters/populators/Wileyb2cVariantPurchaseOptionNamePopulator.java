package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.variants.model.VariantProductModel;

import javax.annotation.Nonnull;

import com.wiley.core.model.WileyPurchaseOptionProductModel;


/**
 * Created by Uladzimir_Barouski on 6/9/2017.
 */
public class Wileyb2cVariantPurchaseOptionNamePopulator implements Populator<VariantProductModel, VariantOptionData>
{
	@Override
	public void populate(@Nonnull  final VariantProductModel source, @Nonnull final VariantOptionData target)
			throws ConversionException
	{
		if (source instanceof WileyPurchaseOptionProductModel)
		{
			target.setName(((VariantProductModel) source).getBaseProduct().getName());
		}
		else
		{
			target.setName(source.getName());
		}
	}
}
