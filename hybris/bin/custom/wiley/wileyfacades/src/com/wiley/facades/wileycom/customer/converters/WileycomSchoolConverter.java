package com.wiley.facades.wileycom.customer.converters;

import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.springframework.util.Assert;

import com.wiley.core.model.SchoolModel;
import com.wiley.facades.customer.data.SchoolData;


public class WileycomSchoolConverter implements Converter<SchoolModel, SchoolData>
{
  @Override
  public SchoolData convert(final SchoolModel source) throws ConversionException
  {
    return convert(source, new SchoolData());
  }

  @Override
  public SchoolData convert(final SchoolModel source, final SchoolData target)
      throws ConversionException
  {
    Assert.notNull(source, "Parameter source cannot be null.");
    target.setCode(source.getCode());
    target.setName(source.getName());
    return target;
  }
}
