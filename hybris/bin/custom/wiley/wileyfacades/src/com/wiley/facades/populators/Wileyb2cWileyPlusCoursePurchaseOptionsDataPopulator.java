package com.wiley.facades.populators;

import de.hybris.platform.converters.Populator;

import javax.annotation.Nonnull;

import org.springframework.util.Assert;

import com.wiley.facades.wileyb2c.product.data.CoursePurchaseOptionsData;
import com.wiley.facades.wileyb2c.product.data.InstantAccessPurchaseOptionData;
import com.wiley.facades.wileyb2c.product.data.WileyPlusCourseInfoData;
import com.wiley.facades.wileyplus.cart.activation.dto.CartActivationRequestDto;
import com.wiley.facades.wileyplus.cart.activation.dto.PurchaseOptionsDto;


/**
 * Populates {@link WileyPlusCourseInfoData} by purchase options based on {@link CartActivationRequestDto}
 */
public class Wileyb2cWileyPlusCoursePurchaseOptionsDataPopulator
		implements Populator<CartActivationRequestDto, WileyPlusCourseInfoData>
{

	@Override
	public void populate(@Nonnull final CartActivationRequestDto cartActivationRequestDto,
			@Nonnull final WileyPlusCourseInfoData wileyPlusCourseInfoData)

	{
		Assert.notNull(cartActivationRequestDto);
		Assert.notNull(wileyPlusCourseInfoData);

		final PurchaseOptionsDto purchaseOptionsDto = cartActivationRequestDto.getPurchaseOptionsDto();

		final CoursePurchaseOptionsData coursePurchaseOptionsData = new CoursePurchaseOptionsData();
		if (purchaseOptionsDto != null)
		{
			// populate free-trial
			coursePurchaseOptionsData.setGracePeriodDuration(purchaseOptionsDto.getGracePeriodDuration());
			coursePurchaseOptionsData.setGracePeriodUsed(purchaseOptionsDto.getGracePeriodUsed());

			// populate regcode
			coursePurchaseOptionsData.setRegCodeActivationAvailable(purchaseOptionsDto.getRegCodeActivationAvailable());
		}
		// populate instant access
		populateInstantAccessPurchaseOption(coursePurchaseOptionsData);

		wileyPlusCourseInfoData.setPurchaseOptions(coursePurchaseOptionsData);
	}

	private void populateInstantAccessPurchaseOption(final CoursePurchaseOptionsData coursePurchaseOptionsData)
	{
		final InstantAccessPurchaseOptionData instantAccessPurchaseOptionData = new InstantAccessPurchaseOptionData();
		instantAccessPurchaseOptionData.setPurchaseOptionAvailable(true);
		coursePurchaseOptionsData.setInstantAccessPurchaseOption(instantAccessPurchaseOptionData);
	}
}
