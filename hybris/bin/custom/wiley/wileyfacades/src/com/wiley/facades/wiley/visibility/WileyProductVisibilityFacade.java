package com.wiley.facades.wiley.visibility;

import de.hybris.platform.core.model.product.ProductModel;


/**
 * @author Dzmitryi_Halahayeu
 */
public interface WileyProductVisibilityFacade
{
	boolean isProductVisible(String productCode);

	ProductModel filterRestrictedProductVariants(ProductModel product);
}
