package com.wiley.facades.subscription.impl;

import de.hybris.platform.commercefacades.converter.ConfigurablePopulator;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.exceptions.ExternalSystemException;
import com.wiley.core.model.WileyProductModel;
import com.wiley.core.model.WileySubscriptionModel;
import com.wiley.core.model.WileySubscriptionPaymentTransactionModel;
import com.wiley.core.product.WileyProductService;
import com.wiley.core.subscription.exception.SubscriptionUpdateException;
import com.wiley.core.wileyb2c.subscription.services.Wileyb2cFreeTrialService;
import com.wiley.core.wileyb2c.subscription.services.Wileyb2cSubscriptionService;
import com.wiley.facades.product.data.WileySubscriptionData;
import com.wiley.facades.product.data.WileySubscriptionPaymentTransactionData;
import com.wiley.facades.subscription.WileySubscriptionOption;
import com.wiley.facades.subscription.Wileyb2cSubscriptionFacade;


/**
 * Facade for subscriptions on B2C. Default implementation of {@link Wileyb2cSubscriptionFacade}.
 */
public class Wileyb2cSubscriptionFacadeImpl implements Wileyb2cSubscriptionFacade
{
	private static final Logger LOG = LoggerFactory.getLogger(Wileyb2cSubscriptionFacadeImpl.class);

	@Autowired
	private Converter<WileySubscriptionModel, WileySubscriptionData> wileyb2cSubscriptionDataConverter;

	@Autowired
	private Converter<AbstractOrderEntryModel, OrderEntryData> wileyb2cCartEntryConverter;

	@Resource
	private ConfigurablePopulator<WileySubscriptionModel, WileySubscriptionData, WileySubscriptionOption>
			wileyb2cSubscriptionConfiguredPopulator;

	@Autowired
	private CustomerAccountService customerAccountService;

	@Autowired
	private UserService userService;

	@Autowired
	private Wileyb2cSubscriptionService wileyb2cSubscriptionService;

	@Autowired
	private Wileyb2cFreeTrialService wileyb2cFreeTrialService;

	@Autowired
	private WileyProductService productService;

	@Autowired
	private Converter<WileySubscriptionPaymentTransactionModel, WileySubscriptionPaymentTransactionData>
			wileyb2cSubscriptionPaymentTransactionDataConverter;

	@Autowired
	private CartService cartService;

	@Override
	public SearchPageData<WileySubscriptionData> getSubscriptionPage(final PageableData pageableData)
	{
		final SearchPageData<WileySubscriptionModel> subscriptionPage =
				wileyb2cSubscriptionService.getSubscriptionPage(pageableData);

		return convertPageData(subscriptionPage, wileyb2cSubscriptionDataConverter);
	}

	@Override
	public void updateCreditCardPaymentInfo(final String subscriptionCode, final String paymentInfoId)
	{
		try
		{
			final WileySubscriptionModel subscription = wileyb2cSubscriptionService.getSubscriptionByCode(subscriptionCode);
			final CreditCardPaymentInfoModel ccPaymentInfoModel = customerAccountService.getCreditCardPaymentInfoForCode(
					(CustomerModel) userService.getCurrentUser(), paymentInfoId);

			wileyb2cSubscriptionService.updatePaymentInfo(subscription, ccPaymentInfoModel);
		}
		catch (IllegalArgumentException | UnknownIdentifierException | AmbiguousIdentifierException | ModelSavingException e)
		{
			throw new SubscriptionUpdateException("Internal subscription delivery address update process failed!", e);
		}
		catch (ExternalSystemException e)
		{
			throw new SubscriptionUpdateException("External subscription delivery address update process failed!", e);
		}

	}

	@Override
	public void setSubscriptionRequireRenewal(final String subscriptionCode, final boolean autorenew)
	{
		final WileySubscriptionModel subscription = wileyb2cSubscriptionService.getSubscriptionByCode(subscriptionCode);
		wileyb2cSubscriptionService.autorenewSubscription(subscription, autorenew);
	}

	@Override
	public void updatePaypalPaymentInfo(final String subscriptionCode, final String paymentInfoId)
			throws SubscriptionUpdateException
	{
		throw new UnsupportedOperationException("Paypal is not supported for this version");
	}

	// copypasted code. Seems like universal code
	protected <S, T> SearchPageData<T> convertPageData(final SearchPageData<S> source, final Converter<S, T> converter)
	{
		final SearchPageData<T> result = new SearchPageData<T>();
		result.setPagination(source.getPagination());
		result.setSorts(source.getSorts());
		result.setResults(Converters.convertAll(source.getResults(), converter));
		return result;
	}

	@Override
	public WileySubscriptionData getSubscriptionDataForCurrentUser(final String subscriptionCode) throws IllegalArgumentException
	{
		WileySubscriptionModel subscription = internalGetWileySubscriptionModel(subscriptionCode);
		UserModel user = userService.getCurrentUser();

		checkSubscriptionForUser(subscription, user);

		return wileyb2cSubscriptionDataConverter.convert(subscription);
	}

	@Override
	public WileySubscriptionData getSubscriptionDataForCurrentUserWithOptions(@Nonnull final String subscriptionCode,
			@Nonnull final List<WileySubscriptionOption> options) throws IllegalArgumentException
	{
		ServicesUtil.validateParameterNotNullStandardMessage("subscriptionCode", subscriptionCode);
		ServicesUtil.validateParameterNotNullStandardMessage("options", options);

		WileySubscriptionModel subscription = internalGetWileySubscriptionModel(subscriptionCode);
		UserModel user = userService.getCurrentUser();

		checkSubscriptionForUser(subscription, user);

		final WileySubscriptionData subscriptionData = wileyb2cSubscriptionDataConverter.convert(subscription);
		wileyb2cSubscriptionConfiguredPopulator.populate(subscription, subscriptionData, options);

		return subscriptionData;
	}

	@Override
	public boolean updateDeliveryAddress(final String subscriptionCode, final String deliveryAddressCode)
	{
		final WileySubscriptionModel subscription = wileyb2cSubscriptionService.getSubscriptionByCode(subscriptionCode);
		final AddressModel deliveryAddress =
				customerAccountService.getAddressForCode((CustomerModel) userService.getCurrentUser(), deliveryAddressCode);

		try
		{
			wileyb2cSubscriptionService.updateDeliveryAddress(subscription, deliveryAddress);
		}
		catch (SubscriptionUpdateException e)
		{
			LOG.error("Updating subscription delivery address failed!", e);
			return false;
		}

		return true;
	}

	@Override
	public boolean isCustomerHasSubscription(final String productCode, final String customerUid)
	{
		final WileyProductModel product = productService.getWileyProductForCode(productCode);
		final CustomerModel customer = (CustomerModel) userService.getUserForUID(customerUid);
		return wileyb2cSubscriptionService.isCustomerHasSubscription(product, customer);
	}

	@Override
	public boolean isProductSubscription(final String productCode)
	{
		final WileyProductModel product = productService.getWileyProductForCode(productCode);
		return wileyb2cSubscriptionService.isProductSubscription(product);
	}

	@Override
	public boolean isProductFreeTrial(final String productCode)
	{
		final WileyProductModel product = productService.getWileyProductForCode(productCode);
		return wileyb2cFreeTrialService.isProductFreeTrial(product);
	}

	@Override
	public SearchPageData<WileySubscriptionPaymentTransactionData> getSubscriptionBillingActivities(
			final String subscriptionCode, final PageableData pageableData)
	{
		final SearchPageData<WileySubscriptionPaymentTransactionModel> subscriptionPage =
				wileyb2cSubscriptionService.getSubscriptionBillingActivities(subscriptionCode, pageableData);

		return convertPageData(subscriptionPage, wileyb2cSubscriptionPaymentTransactionDataConverter);
	}

	private void checkSubscriptionForUser(final WileySubscriptionModel subscription, final UserModel user)
	{
		if (subscriptionCustomerNameNotAvailable(subscription, user) || subscriptionCustomerNameDoesntMatch(subscription, user))
		{
			throw new IllegalArgumentException("The given subscription is not associated with the current customer!");
		}
	}

	private WileySubscriptionModel internalGetWileySubscriptionModel(final String subscriptionCode)
	{
		final WileySubscriptionModel subscription;
		try
		{
			subscription = wileyb2cSubscriptionService.getSubscriptionByCode(subscriptionCode);
		}
		catch (UnknownIdentifierException | AmbiguousIdentifierException e)
		{
			throw new IllegalArgumentException("No subscription found for code: [" + subscriptionCode + "]", e);
		}
		return subscription;
	}

	private boolean subscriptionCustomerNameNotAvailable(final WileySubscriptionModel subscription,
			final UserModel user)
	{
		if (user == null
				|| user.getUid() == null
				|| subscription.getCustomer() == null
				|| subscription.getCustomer().getUid() == null)
		{
			return true;
		}
		return false;
	}

	private boolean subscriptionCustomerNameDoesntMatch(final WileySubscriptionModel subscription,
			final UserModel user)
	{
		if (!user.getUid().equals(subscription.getCustomer().getUid()))
		{
			return true;
		}
		return false;
	}

	public void setWileyb2cSubscriptionDataConverter(
			final Converter<WileySubscriptionModel, WileySubscriptionData> wileyb2cSubscriptionDataConverter)
	{
		this.wileyb2cSubscriptionDataConverter = wileyb2cSubscriptionDataConverter;
	}
}
