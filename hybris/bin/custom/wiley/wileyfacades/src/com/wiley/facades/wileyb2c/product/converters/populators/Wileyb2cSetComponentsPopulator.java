package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.catalog.references.ProductReferenceService;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.ConfigurablePopulator;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.model.WileyPurchaseOptionProductModel;
import com.wiley.core.wileyb2c.classification.Wileyb2cClassificationService;


/**
 * Created by Uladzimir_Barouski on 6/5/2017.
 */
public class Wileyb2cSetComponentsPopulator implements Populator<ProductModel, List<ProductData>>
{
	@Resource
	private ProductReferenceService productReferenceService;
	@Resource
	private Wileyb2cClassificationService wileyb2cClassificationService;
	private ConfigurablePopulator<ProductModel, ProductData, ProductOption> configurablePopulator;

	protected static final List<ProductOption> PRODUCT_SET_COMPONENT_OPTIONS = Arrays.asList(ProductOption.URL,
			ProductOption.PRICE, ProductOption.VARIANT_PRODUCT_NAME, ProductOption.SHOW_PDP_URL);

	@Override
	public void populate(@Nonnull final ProductModel source, @Nonnull final List<ProductData> target) throws ConversionException
	{
		ServicesUtil.validateParameterNotNullStandardMessage("productModel", source);
		ServicesUtil.validateParameterNotNullStandardMessage("productDataList", target);
		Collection<ProductReferenceModel> components = productReferenceService.getProductReferencesForSourceProduct(source,
				ProductReferenceTypeEnum.PRODUCT_SET_COMPONENT, true);
		for (ProductReferenceModel component : components)
		{
			ProductModel componentProduct = component.getTarget();
			ProductData componentData = new ProductData();
			configurablePopulator.populate(componentProduct, componentData, PRODUCT_SET_COMPONENT_OPTIONS);
			componentData.setVariantType(getComponentType(componentProduct));
			target.add(componentData);
		}
	}

	private String getComponentType(final ProductModel productModel)
	{
		if (productModel instanceof WileyPurchaseOptionProductModel)
		{
			final ClassificationClassModel classificationClass = wileyb2cClassificationService.resolveClassificationClass(
					productModel);
			return classificationClass.getName();
		}
		return null;
	}

	@Required
	public void setConfigurablePopulator(
			final ConfigurablePopulator<ProductModel, ProductData, ProductOption> configurablePopulator)
	{
		this.configurablePopulator = configurablePopulator;
	}
}
