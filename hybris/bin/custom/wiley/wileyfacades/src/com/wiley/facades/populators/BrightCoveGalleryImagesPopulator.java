package com.wiley.facades.populators;

import de.hybris.platform.commercefacades.product.converters.populator.ProductGalleryImagesPopulator;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.model.BrightCoveMediaContainerModel;
import com.wiley.facades.populators.bright.cover.WileyBrightCoverHelper;


public class BrightCoveGalleryImagesPopulator<SOURCE extends ProductModel, TARGET extends ProductData> extends
		ProductGalleryImagesPopulator<SOURCE, TARGET>
{
	private WileyBrightCoverHelper wileyBrightCoverHelper;

	@Override
	public void populate(final SOURCE productModel, final TARGET productData) throws ConversionException
	{
		final List<MediaContainerModel> mediaContainers = new ArrayList<>();
		collectMediaContainers(productModel, mediaContainers);
		int galleryIndex = 0;
		for (MediaContainerModel mediaContainer : mediaContainers)
		{
			if (mediaContainer instanceof BrightCoveMediaContainerModel)
			{
				ImageData brightCoveImageData = wileyBrightCoverHelper.createBrightCoveVideoDataForGallery(
						((BrightCoveMediaContainerModel) mediaContainer).getVideoId(), galleryIndex);
				if (productData.getImages() == null)
				{
					productData.setImages(new ArrayList<ImageData>());
				}
				productData.getImages().add(brightCoveImageData);
			}
			galleryIndex++;
		}
	}

	@Required
	public void setWileyBrightCoverHelper(final WileyBrightCoverHelper wileyBrightCoverHelper)
	{
		this.wileyBrightCoverHelper = wileyBrightCoverHelper;
	}
}
