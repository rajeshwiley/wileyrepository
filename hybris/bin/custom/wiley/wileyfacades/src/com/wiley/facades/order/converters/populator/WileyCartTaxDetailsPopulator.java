package com.wiley.facades.order.converters.populator;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


public class WileyCartTaxDetailsPopulator implements Populator<AbstractOrderModel, AbstractOrderData>
{

	@Override
	public void populate(final AbstractOrderModel source, final AbstractOrderData target) throws ConversionException
	{
		validateParameterNotNullStandardMessage("source", source);
		validateParameterNotNullStandardMessage("target", target);

		target.setTaxNumber(source.getTaxNumber());
		target.setTaxNumberExpirationDate(source.getTaxNumberExpirationDate());
		target.setTaxCalculated(source.getTaxCalculated());
		target.setTaxNumberValidated(source.getTaxNumberValidated());
	}

}
