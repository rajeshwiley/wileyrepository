package com.wiley.facades.wileyws.subscriptions.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import com.wiley.core.model.WileySubscriptionModel;
import com.wiley.facades.wileyws.subscriptions.dto.SubscriptionSearchResult;


/**
 * Created by Georgii_Gavrysh on 10/5/2016.
 */
public class SubscriptionSearchResultPopulator implements Populator<WileySubscriptionModel, SubscriptionSearchResult>
{
	@Override
	public void populate(final WileySubscriptionModel wileySubscriptionModel,
			final SubscriptionSearchResult subscriptionSearchResult)
			throws ConversionException
	{
		ServicesUtil.validateParameterNotNullStandardMessage("wileySubscriptionModel", wileySubscriptionModel);
		subscriptionSearchResult.setInternalSubscriptionId(wileySubscriptionModel.getCode());
	}
}
