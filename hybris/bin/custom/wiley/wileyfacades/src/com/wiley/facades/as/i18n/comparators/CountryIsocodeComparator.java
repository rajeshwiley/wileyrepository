package com.wiley.facades.as.i18n.comparators;

import de.hybris.platform.commercefacades.user.data.CountryData;

import java.util.Comparator;


public class CountryIsocodeComparator implements Comparator<CountryData>
{
	private static final Comparator<CountryData> COMPARATOR =
			Comparator.comparing(CountryData::getIsocode, Comparator.nullsLast(Comparator.naturalOrder()));

	@Override
	public int compare(final CountryData o1, final CountryData o2)
	{
		return COMPARATOR.compare(o1, o2);
	}
}
