package com.wiley.facades.welags.pin;

import com.wiley.facades.welags.pin.exception.PinOperationException;


/**
 * PIN facade interface.
 *
 * @author Gabor_Bata
 */
public interface PinFacade
{
	/**
	 * Apply PIN to the given cart
	 *
	 * @param cartCode
	 * 		the cart code
	 * @param pinCode
	 * 		PIN identifier
	 * @throws PinOperationException
	 * 		if PIN could not applied to cart
	 * @throws IllegalArgumentException
	 * 		if parameter code is null or empty
	 */
	void applyPin(String cartCode, String pinCode) throws PinOperationException;

	/**
	 * Activates PIN for the given order model.
	 *
	 * @param orderCode
	 * 		the order code
	 * @param pinCode
	 * 		the PIN code
	 * @throws PinOperationException
	 * 		if PIN could not be activated
	 * @throws IllegalArgumentException
	 * 		if parameter code is null or empty
	 */
	void activatePin(String orderCode, String pinCode) throws PinOperationException;
}
