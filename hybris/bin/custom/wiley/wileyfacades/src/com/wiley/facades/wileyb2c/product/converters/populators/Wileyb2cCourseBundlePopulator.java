package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.support.PropertyComparator;
import org.springframework.util.Assert;

import com.wiley.core.product.WileyProductService;
import com.wiley.core.wileyb2c.product.Wileyb2cCourseProductService;
import com.wiley.facades.populators.WileyProductPricePopulator;
import com.wiley.facades.wileyb2c.product.data.CourseBundleData;
import com.wiley.facades.wileyb2c.product.data.CourseBundlesData;
import com.wiley.facades.wileyb2c.product.data.WileyPlusCourseInfoData;
import com.wiley.facades.wileyplus.cart.activation.dto.CartActivationRequestDto;


/**
 * Created by Uladzimir_Barouski on 8/24/2016.
 */
public class Wileyb2cCourseBundlePopulator implements Populator<CartActivationRequestDto, WileyPlusCourseInfoData>
{
	private static final Logger LOG = Logger.getLogger(Wileyb2cCourseBundlePopulator.class);
	private static final PropertyComparator BUNDLE_COMPARATOR = new PropertyComparator("mostPopular", true, false);
	private static final boolean ACTIVE_ONLY = true;

	@Resource
	private WileyProductService productService;

	@Resource
	private WileyProductPricePopulator wileyb2cProductPricePopulator;

	@Resource
	private Wileyb2cCourseProductService wileyb2cCourseProductService;

	@Resource
	private Converter<ProductModel, CourseBundleData> wileyb2cCourseBundleEntryConverter;

	@Override
	public void populate(@Nonnull final CartActivationRequestDto source, @Nonnull final WileyPlusCourseInfoData target)
			throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		Assert.notNull(target.getPurchaseOptions());
		Assert.notNull(target.getPurchaseOptions().getInstantAccessPurchaseOption());

		final ProductModel courseProduct = productService.getProductForIsbn(source.getCourse().getIsbn());
		final List<ProductModel> bundleProducts = getBundleProducts(courseProduct);
		final List<CourseBundleData> bundleDataList = new ArrayList<>();
		for (ProductModel bundleProduct : bundleProducts)
		{
			try
			{
				final CourseBundleData bundleData = wileyb2cCourseBundleEntryConverter.convert(bundleProduct);
				bundleDataList.add(bundleData);
			}
			catch (ConversionException e)
			{
				LOG.error(e);
			}
		}
		if (!bundleDataList.isEmpty())
		{
			Collections.sort(bundleDataList, BUNDLE_COMPARATOR);
			bundleDataList.get(0).setMostPopular(true);
		}

		CourseBundlesData courseBundlesData = new CourseBundlesData();
		courseBundlesData.setCourseCode(courseProduct.getCode());
		courseBundlesData.setBundles(bundleDataList);
		populateCourseBundlePrice(courseProduct, courseBundlesData);

		target.getPurchaseOptions().getInstantAccessPurchaseOption().setCourseBundles(courseBundlesData);
	}

	private void populateCourseBundlePrice(final ProductModel courseProduct, final CourseBundlesData courseBundlesData)
	{
		ProductData courseProductData = new ProductData();
		wileyb2cProductPricePopulator.populate(courseProduct, courseProductData);
		courseBundlesData.setCoursePrice(courseProductData.getPrice());
	}


	private List<ProductModel> getBundleProducts(final ProductModel courseProduct)
	{
		List<ProductModel> bundleProducts = null;
		try
		{
			final ProductModel eprofProduct = wileyb2cCourseProductService.getReferencedEprofProduct(courseProduct);
			bundleProducts = wileyb2cCourseProductService.getReferencedBundleProducts(eprofProduct, courseProduct);
		}
		catch (IllegalArgumentException | IllegalStateException | NullPointerException e)
		{
			LOG.error("Cource product or referenced products has unexpected database state", e);
			bundleProducts = Collections.emptyList();
		}

		return bundleProducts;
	}
}
