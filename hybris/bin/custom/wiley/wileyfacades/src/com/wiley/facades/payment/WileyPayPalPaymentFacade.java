package com.wiley.facades.payment;

import de.hybris.platform.acceleratorfacades.payment.PaymentFacade;

import com.paypal.hybris.data.SetExpressCheckoutRequestData;
import com.paypal.hybris.data.SetExpressCheckoutResultData;


/**
 * Created by Raman_Hancharou on 8/19/2016.
 */
public interface WileyPayPalPaymentFacade extends PaymentFacade
{
	/**
	 * Removes delivery address from session cart
	 */
	void removeDeliveryAddressFromCart();

	/**
	 * Resolves a given URL to a full URL including server and port, etc.
	 *
	 * @param responseUrl
	 * 		- the URL to resolve
	 * @param isSecure
	 * 		- flag to indicate whether the final URL should use a secure connection or not.
	 * @return a full URL including HTTP protocol, server, port, path etc.
	 */
	String getFullResponseUrl(String responseUrl, boolean isSecure);

	/**
	 * Prepare required paypal data
	 *
	 * @param requestData
	 * @return required paypal data
	 */
	SetExpressCheckoutResultData preparePaypalPayment(SetExpressCheckoutRequestData requestData);
}
