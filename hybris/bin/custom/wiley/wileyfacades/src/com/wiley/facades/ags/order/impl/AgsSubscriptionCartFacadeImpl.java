/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.facades.ags.order.impl;

import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.subscriptionfacades.order.impl.DefaultSubscriptionCartFacade;

import javax.annotation.Nonnull;

import com.wiley.facades.ags.order.AgsSubscriptionCartFacade;


/**
 * Default subscription cart facade.
 */
public class AgsSubscriptionCartFacadeImpl extends DefaultSubscriptionCartFacade implements AgsSubscriptionCartFacade
{
	@Override
	public CartModificationData addToCart(@Nonnull final String cartCode, @Nonnull final String code, final long quantity)
			throws CommerceCartModificationException
	{
		final ProductModel product = getProductService().getProductForCode(code);

		final String xml = getProductAsXML(product);

		final CommerceCartModification modification = getSubscriptionCommerceCartService().addToCart(getCartForCode(cartCode),
				product, quantity, product.getUnit(), false, xml);

		return getCartModificationConverter().convert(modification);
	}

	private CartModel getCartForCode(final String cartCode)
	{
		return getCommerceCartService().getCartForCodeAndUser(cartCode, getUserService().getCurrentUser());
	}
}
