/**
 *
 */
package com.wiley.facades.populators;

import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.commercefacades.product.data.FeatureData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.springframework.util.Assert;


/**
 * Class to populate Feature with custom fields like document, details, type and
 */
public class WileyFeaturePopulator implements Populator<Feature, FeatureData>
{
	private Converter<ClassAttributeAssignmentModel, FeatureData> wileyFeatureConverter;


	public void setWileyFeatureConverter(
			final Converter<ClassAttributeAssignmentModel, FeatureData> wileyFeatureConverter)
	{
		this.wileyFeatureConverter = wileyFeatureConverter;
	}

	@Override
	public void populate(final Feature source, final FeatureData target)
	{

		Assert.notNull(source, "Feature cannot be null.");
		Assert.notNull(target, "FeatureData cannot be null.");

		final ClassAttributeAssignmentModel classAttributeAssignment = source
				.getClassAttributeAssignment();

		wileyFeatureConverter.convert(classAttributeAssignment, target);
	}
}
