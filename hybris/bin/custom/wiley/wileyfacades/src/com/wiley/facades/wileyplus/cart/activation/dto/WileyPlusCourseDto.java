package com.wiley.facades.wileyplus.cart.activation.dto;

import java.util.Objects;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;



/**
 * an object incapsulate all necessary information about a course
 **/

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2016-08-09T10:25:39.625+02:00")

public class WileyPlusCourseDto
{

	private String isbn = null;
	@JsonIgnore
	private String imageUrl = null;
	@JsonIgnore
	private String title = null;
	@JsonIgnore
	private String authors = null;
	@JsonIgnore
	private String productEdition = null;

	/**
	 * ISBN-13. See: https://en.wikipedia.org/wiki/International_Standard_Book_Number
	 **/
	public WileyPlusCourseDto isbn(final String isbn)
	{
		this.isbn = isbn;
		return this;
	}

	@NotNull
	@Size(max = 255)
	@JsonProperty("isbn")
	public String getIsbn()
	{
		return isbn;
	}

	public void setIsbn(final String isbn)
	{
		this.isbn = isbn;
	}

	/**
	 **/
	public WileyPlusCourseDto imageUrl(final String imageUrl)
	{
		this.imageUrl = imageUrl;
		return this;
	}

	@NotNull
	@Size(max = 255)
	@JsonGetter("iu")
	public String getImageUrl()
	{
		return imageUrl;
	}

	@JsonSetter("imageUrl")
	public void setImageUrl(final String imageUrl)
	{
		this.imageUrl = imageUrl;
	}

	@JsonSetter("iu")
	public void setIu(final String iu)
	{
		this.imageUrl = iu;
	}

	/**
	 **/
	public WileyPlusCourseDto title(final String title)
	{
		this.title = title;
		return this;
	}

	@NotNull
	@Size(max = 255)
	@JsonGetter("tt")
	public String getTitle()
	{
		return title;
	}

	@JsonSetter("title")
	public void setTitle(final String title)
	{
		this.title = title;
	}

	@JsonSetter("tt")
	public void setTt(final String tt)
	{
		this.title = tt;
	}

	/**
	 **/
	public WileyPlusCourseDto authors(final String authors)
	{
		this.authors = authors;
		return this;
	}

	@NotNull
	@Size(max = 255)
	@JsonGetter("au")
	public String getAuthors()
	{
		return authors;
	}

	@JsonSetter("authors")
	public void setAuthors(final String authors)
	{
		this.authors = authors;
	}

	@JsonSetter("au")
	public void setAu(final String au)
	{
		this.authors = au;
	}

	/**
	 **/
	public WileyPlusCourseDto productEdition(final String productEdition)
	{
		this.productEdition = productEdition;
		return this;
	}

	@Size(max = 255)
	@JsonGetter("pe")
	public String getProductEdition()
	{
		return productEdition;
	}

	@JsonSetter("productEdition")
	public void setProductEdition(final String productEdition)
	{
		this.productEdition = productEdition;
	}

	@JsonSetter("pe")
	public void setPe(final String pe)
	{
		this.productEdition = pe;
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		WileyPlusCourseDto wileyPlusCourseDTO = (WileyPlusCourseDto) o;
		return Objects.equals(isbn, wileyPlusCourseDTO.isbn)
				&& Objects.equals(imageUrl, wileyPlusCourseDTO.imageUrl)
				&& Objects.equals(title, wileyPlusCourseDTO.title)
				&& Objects.equals(authors, wileyPlusCourseDTO.authors)
				&& Objects.equals(productEdition, wileyPlusCourseDTO.productEdition);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(isbn, imageUrl, title, authors, productEdition);
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("class WileyPlusCourse {\n");

		sb.append("    isbn: ").append(toIndentedString(isbn)).append("\n");
		sb.append("    imageUrl: ").append(toIndentedString(imageUrl)).append("\n");
		sb.append("    title: ").append(toIndentedString(title)).append("\n");
		sb.append("    authors: ").append(toIndentedString(authors)).append("\n");
		sb.append("    productEdition: ").append(toIndentedString(productEdition)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(final Object o)
	{
		if (o == null)
		{
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}

