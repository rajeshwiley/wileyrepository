package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.enums.UserDiscountGroup;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.List;

import javax.annotation.Nonnull;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import com.wiley.facades.constants.WileyFacadesConstants;
import com.wiley.facades.product.converters.populator.ProductStudentDiscountPopulator;

import static java.util.Objects.nonNull;


/**
 * Populates price with applied student discount for base product and product variants. It uses the first student discount<br/>
 * to calculate price for students.
 */
public class Wileyb2cProductStudentDiscountPopulator extends ProductStudentDiscountPopulator
{

	private static final Logger LOG = LoggerFactory.getLogger(Wileyb2cProductStudentDiscountPopulator.class);

	private SessionService sessionService;

	@Override
	public void populate(@Nonnull final ProductModel productModel, @Nonnull final ProductData productData)
			throws ConversionException
	{
		Assert.notNull(productModel);
		Assert.notNull(productData);

		final CurrencyModel currentCurrency = commerceCommonI18NService.getCurrentCurrency();

		LOG.debug("Method parameters: productModel [{}], productData [{}]. Current currency [{}].", productModel.getCode(),
				productData, currentCurrency.getIsocode());

		UserDiscountGroup studentDiscountGroup = getSessionService().getAttribute(
				WileyFacadesConstants.USER_GROUP_LOCAL_VIEW_PARAM_NAME);

		final List<DiscountValue> discounts = wileyProductDiscountService.getPotentialDiscountForCurrentCustomer(
				productModel, studentDiscountGroup);

		if (CollectionUtils.isNotEmpty(discounts))
		{
			LOG.debug("Populating student prices for base product " + productModel.getCode());
			DiscountValue studentDiscount = discounts.get(0);
			// populating information about student discount
			productData.setStudentDiscount(discountValueConverter.convert(studentDiscount));

			//<editor-fold desc="Populating price for single product (product without variants)">
			if (nonNull(productData.getPrice()))
			{
				populateStudentDiscountPriceForBaseProduct(productData, currentCurrency, studentDiscount);
			}
			//</editor-fold>
		}

		//<editor-fold desc="Populating price for variants">
		populateStudentDiscountPriceForVariants(productModel, productData, currentCurrency, studentDiscountGroup);
		//</editor-fold>
	}

	private void populateStudentDiscountPriceForVariants(final ProductModel productModel, final ProductData productData,
			final CurrencyModel currentCurrency, final UserDiscountGroup studentDiscountGroup)
	{
		for (VariantProductModel variantProduct : productModel.getVariants())
		{
			final List<DiscountValue> discounts = wileyProductDiscountService.getPotentialDiscountForCurrentCustomer(
					variantProduct, studentDiscountGroup);

			if (CollectionUtils.isNotEmpty(discounts))
			{
				LOG.debug("Populating student price for variant product " + variantProduct.getCode());
				DiscountValue studentDiscount = discounts.get(0);
				final List<VariantOptionData> variantProducts = productData.getVariantOptions();
				variantProducts.stream()
						.filter(variantOption -> variantOption.getPriceData() != null && variantOption.getCode()
								.equals(variantProduct.getCode()))
						.forEach(variantOption ->
						{
							final PriceData studentPrice = createPriceDataWithAppliedDiscount(variantOption.getPriceData(),
									currentCurrency, studentDiscount);
							variantOption.setStudentPrice(studentPrice);
						});
			}
		}
	}

	public SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}
}
