package com.wiley.facades.populators;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.variants.model.VariantCategoryModel;
import de.hybris.platform.variants.model.VariantProductModel;
import de.hybris.platform.variants.model.VariantValueCategoryModel;

import java.util.Collection;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.category.WileyCategoryService;
import com.wiley.core.enums.VariantCategoryType;


/**
 * Wiley Variant Category Data Product Populator.
 */
public class WileyVariantCategoryProductPopulator implements Populator<ProductModel, ProductData>
{
	@Autowired
	private WileyCategoryService wileyCategoryService;

	@Override
	public void populate(final ProductModel source, final ProductData target) throws ConversionException
	{

		if (source instanceof VariantProductModel)
		{

			final CategoryModel category = wileyCategoryService.getPrimaryWileyCategoryForProduct(source);
			final boolean isCFACategory = wileyCategoryService.isCFACategory(category);
			target.setCFACategory(isCFACategory);

			Collection<CategoryModel> supercategories = source.getSupercategories();
			for (CategoryModel categoryModel : supercategories)
			{
				if (categoryModel instanceof VariantValueCategoryModel)
				{
					populateCategoryCOde(target, isCFACategory, categoryModel);
				}
			}

		}
	}

	private void populateCategoryCOde(final ProductData target, boolean isCFACategory, final CategoryModel categoryModel) {
		//VARIANT_VALUE_CATEGORY can have only one Supercategory
		Optional<CategoryModel> rootCategory = categoryModel.getSupercategories().stream().findFirst();

		if (rootCategory.isPresent()
				&& rootCategory.get() instanceof VariantCategoryModel)
		{
			 final VariantCategoryModel rootCategoryModel = (VariantCategoryModel) rootCategory.get();
			if (isCFACategory && VariantCategoryType.LEVEL == rootCategoryModel.getVariantCategoryType())
			{
				target.setLevelVariantCategoryCode(categoryModel.getCode());
			} else if (VariantCategoryType.PART == rootCategoryModel.getVariantCategoryType())
			{
				target.setPartVariantCategoryCode(categoryModel.getCode());
			}
			else if (VariantCategoryType.TYPE == rootCategoryModel.getVariantCategoryType())
			{
				target.setTypeVariantCategoryCode(categoryModel.getCode());
			}
		}
	}


}
