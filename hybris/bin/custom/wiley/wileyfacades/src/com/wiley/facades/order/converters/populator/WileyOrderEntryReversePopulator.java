package com.wiley.facades.order.converters.populator;

import de.hybris.platform.commercefacades.order.data.DeliveryModeData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;


/**
 * Author Maksim_Kozich (EPAM)
 */
public class WileyOrderEntryReversePopulator implements Populator<OrderEntryData, AbstractOrderEntryModel>
{
	private Converter<ProductData, ProductModel> productReverseConverter;
	private Converter<DeliveryModeData, DeliveryModeModel> deliveryModeReverseConverter;
	private Converter<PointOfServiceData, PointOfServiceModel> pointOfServiceReverseConverter;

	@Override
	public void populate(final OrderEntryData source, final AbstractOrderEntryModel target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		addCommon(source, target);
		addProduct(source, target);
		addTotals(source, target);
		addDeliveryMode(source, target);
	}

	protected void addDeliveryMode(final OrderEntryData source, final AbstractOrderEntryModel target)
	{
		if (source.getDeliveryMode() != null)
		{
			target.setDeliveryMode(getDeliveryModeReverseConverter().convert(source.getDeliveryMode()));
		}

		if (source.getDeliveryPointOfService() != null)
		{
			target.setDeliveryPointOfService(getPointOfServiceReverseConverter().convert(source.getDeliveryPointOfService()));
		}
	}

	protected void addCommon(final OrderEntryData source, final AbstractOrderEntryModel target)
	{
		target.setEntryNumber(source.getEntryNumber());
		target.setQuantity(source.getQuantity());
	}

	protected void addProduct(final OrderEntryData source, final AbstractOrderEntryModel target)
	{
		target.setProduct(getProductReverseConverter().convert(source.getProduct()));
	}

	protected void addTotals(final OrderEntryData source, final AbstractOrderEntryModel target)
	{
		if (source.getBasePrice() != null)
		{
			target.setBasePrice(source.getBasePrice().getValue().doubleValue());
		}
		if (source.getTotalPrice() != null)
		{
			target.setTotalPrice(source.getTotalPrice().getValue().doubleValue());
		}
	}

	protected Converter<DeliveryModeData, DeliveryModeModel> getDeliveryModeReverseConverter()
	{
		return deliveryModeReverseConverter;
	}

	@Required
	public void setDeliveryModeReverseConverter(final Converter<DeliveryModeData, DeliveryModeModel> deliveryModeReverseConverter)
	{
		this.deliveryModeReverseConverter = deliveryModeReverseConverter;
	}

	protected Converter<ProductData, ProductModel> getProductReverseConverter()
	{
		return productReverseConverter;
	}

	@Required
	public void setProductReverseConverter(final Converter<ProductData, ProductModel> productReverseConverter)
	{
		this.productReverseConverter = productReverseConverter;
	}

	protected Converter<PointOfServiceData, PointOfServiceModel> getPointOfServiceReverseConverter()
	{
		return pointOfServiceReverseConverter;
	}

	@Required
	public void setPointOfServiceReverseConverter(
			final Converter<PointOfServiceData, PointOfServiceModel> pointOfServiceReverseConverter)
	{
		this.pointOfServiceReverseConverter = pointOfServiceReverseConverter;
	}
}
