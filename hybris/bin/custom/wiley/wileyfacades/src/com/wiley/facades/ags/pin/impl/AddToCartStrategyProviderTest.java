package com.wiley.facades.ags.pin.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.site.BaseSiteService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.facades.welags.pin.AddToCartStrategy;
import com.wiley.facades.welags.pin.impl.AddToCartStrategyProvider;
import com.wiley.facades.wel.pin.impl.WelAddToCartStrategy;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * Unit test for {@link AddToCartStrategyProvider}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AddToCartStrategyProviderTest
{
	private static final String TEST_SITE_ID_AGS = "ags";
	private static final String TEST_SITE_ID_WEL = "wel";

	@Mock
	private BaseSiteModel baseSiteModelMock;

	@Mock
	private BaseSiteService baseSiteServiceMock;

	@Mock
	private AgsAddToCartStrategy agsAddToCartStrategy;

	@Mock
	private WelAddToCartStrategy welAddToCartStrategy;

	@InjectMocks
	private AddToCartStrategyProvider addToCartStrategyProvider;

	@Before
	public void setUp()
	{
		when(baseSiteServiceMock.getCurrentBaseSite()).thenReturn(baseSiteModelMock);
	}

	@Test
	public void shouldReturnAgsAddToCartStrategyWhenSiteIsAGS()
	{
		when(baseSiteModelMock.getUid()).thenReturn(TEST_SITE_ID_AGS);

		AddToCartStrategy strategy = addToCartStrategyProvider.getStrategyForPinWorkFlow();
		assertTrue(strategy instanceof AgsAddToCartStrategy);
	}

	@Test
	public void shouldReturnWelAddToCartStrategyWhenSiteIsNotAGS()
	{
		when(baseSiteModelMock.getUid()).thenReturn(TEST_SITE_ID_WEL);

		AddToCartStrategy strategy = addToCartStrategyProvider.getStrategyForPinWorkFlow();
		assertTrue(strategy instanceof WelAddToCartStrategy);
	}
}
