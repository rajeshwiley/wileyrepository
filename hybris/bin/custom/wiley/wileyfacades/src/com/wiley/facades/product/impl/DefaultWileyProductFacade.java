package com.wiley.facades.product.impl;

import com.wiley.core.locale.WileyLocaleService;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.ProductReferenceData;
import de.hybris.platform.commercefacades.product.impl.DefaultProductFacade;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.model.WileyProductModel;
import com.wiley.core.price.service.WileyPriceService;
import com.wiley.core.product.WileyProductService;
import com.wiley.core.wiley.restriction.WileyProductRestrictionService;
import com.wiley.facades.as.product.data.ProductPricesData;
import com.wiley.facades.product.WileyProductFacade;
import org.springframework.cache.annotation.Cacheable;


/**
 * Interface contains methods for getting product data-objects
 *
 * Created by Raman_Hancharou on 1/19/2016.
 */
public class DefaultWileyProductFacade extends DefaultProductFacade<ProductModel> implements WileyProductFacade
{
	protected static final Logger LOG = Logger.getLogger(DefaultWileyProductFacade.class);

	private WileyProductRestrictionService wileyProductRestrictionService;

	private static final int ISBN10_LENGTH = 10;

	@Autowired
	private WileyProductService wileyProductService;

	@Autowired
	private CategoryService categoryService;

	@Resource
	private WileyPriceService wileyPriceService;

	@Resource
	private Converter<PriceRowModel, PriceData> priceConverter;

	private Converter<ProductModel, ProductData> productUrlConverter;

	@Resource(name = "wileyLocaleService")
	private WileyLocaleService wileyLocaleService;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Override
	public String findProductUrlForIsbnOrCode(final String isbn)
	{
		ProductModel productModel;
		if (StringUtils.length(isbn) == ISBN10_LENGTH)
		{
			productModel = wileyProductService.getProductForIsbn(isbn);
		}
		else
		{
			productModel = wileyProductService.getProductForCode(isbn);
		}
		final ProductData productData = getProductUrlConverter().convert(productModel);
		return productData.getUrl();
	}

	@Override
	public List<ProductData> getProductsForCategory(@Nonnull final String categoryCode, final List<ProductOption> productOptions)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("categoryCode", categoryCode);

		List<ProductData> result = new ArrayList<>();

		List<WileyProductModel> productList = wileyProductService.getWileyProductsFromCategoryRoot(categoryCode, null).stream()
				.filter(product -> wileyProductRestrictionService.isVisible(product)).collect(Collectors.toList());

		productList.stream().forEachOrdered(product -> result.add(getProductForOptions(product, productOptions)));

		return result;
	}

	@Override
	public boolean doesProductExistForIsbn(@Nonnull final String isbn)
	{
		boolean result;
		try
		{
			result = wileyProductService.getProductForIsbn(isbn) != null;
		}
		catch (UnknownIdentifierException e)
		{
			result = false;
		}
		return result;
	}

	/**
	 * Filters only visible productReferences if displayProductsDespiteRestrictions set to false.
	 * Returns all productReferences but marks visible products
	 * with flag showPdpUrl if displayProductsDespiteRestrictions set to true
	 *
	 * @param productReferences
	 * 		- collection of productReferences
	 * @param displayProductsDespiteRestrictions
	 * 		- the flag is used to mark visible products in returned collection
	 * @return filtered list of product references
	 */
	@Override
	public List<ProductReferenceData> filterVisibleReferenceProducts(final List<ProductReferenceData> productReferences,
			final boolean displayProductsDespiteRestrictions)
	{
		final List<ProductReferenceData> resultProductReferences = new ArrayList<>();

		for (final ProductReferenceData data : productReferences)
		{
			final ProductData target = data.getTarget();
			final boolean isVisible = isVisible(target.getCode());
			if (isVisible)
			{
				resultProductReferences.add(data);
				target.setShowPdpUrl(Boolean.TRUE);
			}
			else
			{
				if (displayProductsDespiteRestrictions)
				{
					resultProductReferences.add(data);
					target.setShowPdpUrl(Boolean.FALSE);
				}
			}
		}
		return resultProductReferences;
	}

	@Override
	public boolean isVisible(@Nonnull final String productCode)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("productCode", productCode);
		final ProductModel productModel = wileyProductService.getProductForCode(productCode);
		return wileyProductRestrictionService.isVisible(productModel);
	}

	@Override
	public ProductPricesData getProductPrices(final String productCode, final String currencyIsoCode)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("productCode", productCode);
		ServicesUtil.validateParameterNotNullStandardMessage("currencyIsoCode", currencyIsoCode);

		final ProductModel productModel = wileyProductService.getProductForCode(productCode);
		CurrencyModel currencyModel = getCommonI18NService().getCurrency(currencyIsoCode);

		ProductPricesData result = new ProductPricesData();
		result.setStackable(productModel.getStackablePrice());

		final List<PriceRowModel> priceRows = wileyPriceService.getPricesForProduct(productModel, currencyModel);
		result.setPrices(priceConverter.convertAll(priceRows));

		return result;
	}

	public WileyProductService getWileyProductService()
	{
		return wileyProductService;
	}

	@Required
	public void setWileyProductRestrictionService(
			final WileyProductRestrictionService wileyProductRestrictionService)
	{
		this.wileyProductRestrictionService = wileyProductRestrictionService;
	}

	public WileyProductRestrictionService getWileyProductRestrictionService()
	{
		return wileyProductRestrictionService;
	}

	public CategoryService getCategoryService()
	{
		return categoryService;
	}

	public Converter<ProductModel, ProductData> getProductUrlConverter()
	{
		return productUrlConverter;
	}

	public void setProductUrlConverter(final Converter<ProductModel, ProductData> productUrlConverter)
	{
		this.productUrlConverter = productUrlConverter;
	}

	@Override
	@Cacheable(
			value = "wileyProductDataPDPCacheRegion",
			key = "#productModel.code + #root.target.getWileyLocaleService().getCurrentEncodedLocale()",
			unless = "#result == null",
			condition = "#root.target.getConfigurationService().getConfiguration().getBoolean("
                    + "'wileyproductdatapdpcacheregion.enabled', false)"
	)
	public ProductData getProductForCodeAndOptionsCached(final ProductModel productModel, final List<ProductOption> options)
	{
		LOG.debug("The cache of ProductData " + productModel.getCode() + ", was missed, we are in the method.");
		return getProductForOptions(productModel, options);
	}

	@Override
	public ProductData getProductDataForCodeAndOptions(final ProductModel productModel, final ProductData productData,
													   final List<ProductOption> options)
    {
		getProductConfiguredPopulator().populate(productModel, productData, options);
		return productData;
	}

	public ConfigurationService getConfigurationService() {
		return configurationService;
	}

	public WileyLocaleService getWileyLocaleService() {
		return wileyLocaleService;
	}
}
