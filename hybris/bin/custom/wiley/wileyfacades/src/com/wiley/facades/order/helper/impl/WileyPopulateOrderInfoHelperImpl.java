package com.wiley.facades.order.helper.impl;

import com.wiley.core.order.WileyCheckoutService;
import com.wiley.facades.order.helper.WileyPopulateOrderInfoHelper;
import com.wiley.facades.product.data.DiscountData;
import de.hybris.platform.acceleratorservices.payment.data.OrderEntryInfoData;
import de.hybris.platform.acceleratorservices.payment.data.OrderInfoData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.FeatureData;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.i18n.I18NService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Georgii_Gavrysh on 12/12/2016.
 */
public class WileyPopulateOrderInfoHelperImpl implements WileyPopulateOrderInfoHelper
{
    private static final Logger LOG = Logger.getLogger(WileyPopulateOrderInfoHelperImpl.class);
    protected static final String CART_IMAGE_FORMAT = "cartIcon";
    protected static final String PRICE_FORMAT = "%.2f";

    @Resource
    private WileyCheckoutService wileyCheckoutService;

    @Resource
    private CommerceCommonI18NService commerceCommonI18NService;

    @Resource
    private CommonI18NService commonI18NService;

    @Resource
    private I18NService i18NService;

    @Override
    public void populateOrderInfoData(final CartModel cartModel, final CartData cartData, final OrderInfoData orderInfoData) {
        if (LOG.isDebugEnabled()) {
            logCartModelInformation(cartModel);
        }
        if (cartData != null)
        {

            orderInfoData.setOrderNumber(cartData.getCode());

            CurrencyModel currency = cartModel.getCurrency();
            orderInfoData.setCurrency(currency.getIsocode());
            orderInfoData.setCurrencySymbol(currency.getSymbol());

            if (cartData.getTotalPriceWithTax() != null)
            {
                String totalPriceWithTax = getPrice(currency, cartData.getTotalPriceWithTax().getValue());
                orderInfoData.setOrderTotal(totalPriceWithTax);

            }

            if (cartData.getSubTotal() != null)
            {
                String subtotal = getPrice(currency, cartData.getSubTotal().getValue());
                orderInfoData.setSubtotal(subtotal);

            }

            if (cartData.getSubTotalWithoutDiscount() != null)
            {
                String subTotalWithoutDiscount = getPrice(currency, cartData.getSubTotalWithoutDiscount().getValue());
                orderInfoData.setSubtotalWithoutDiscount(subTotalWithoutDiscount);

            }

            if (cartData.getTotalDiscounts() != null)
            {
                String discounts = getPrice(currency, cartData.getTotalDiscounts().getValue());
                orderInfoData.setDiscount(discounts);
            }

            if (cartData.getDiscountData() != null)
            {
                cartData.getDiscountData().stream().map(DiscountData::getCode)
                        .findFirst().ifPresent(orderInfoData::setDiscountCode);
            }

            if (cartData.getAppliedVouchers() != null)
            {
                cartData.getAppliedVouchers().stream().findFirst().ifPresent(orderInfoData::setDiscountCode);
            }

            if (cartData.getTotalTax() != null)
            {
                String totalTax = getPrice(currency, cartData.getTotalTax().getValue());
                orderInfoData.setTaxAmount(totalTax);

            }

            if (cartData.getDeliveryCost() != null)
            {
                String deliveryCost = formatPrice(cartData.getDeliveryCost().getValue().doubleValue());
                orderInfoData.setShippingCost(deliveryCost);

            }

            if (CollectionUtils.isNotEmpty(cartData.getEntries()))
            {
                populateOrderEntryInfoData(cartData.getEntries(), orderInfoData);
            }

            final boolean studentOrder = wileyCheckoutService.isStudentOrder(cartModel);
            orderInfoData.setStudentFlow(studentOrder);


            final boolean orderWithoutShipping = wileyCheckoutService.isOrderWithoutShipping(cartModel);
            orderInfoData.setOrderWithoutShipping(orderWithoutShipping);

        }


        if (LOG.isDebugEnabled()) {
            logOrderInfoDataInformation(orderInfoData);
        }
    }

    private String getPrice(final CurrencyModel currency, final BigDecimal price) {
        return formatPrice(commonI18NService.roundCurrency(price.doubleValue(), currency.getDigits()));
    }

    private String formatPrice(final double price) {
        return String.format(PRICE_FORMAT, Double.valueOf(price));
    }

    private void populateOrderEntryInfoData(final List<OrderEntryData> entries, final OrderInfoData orderInfoData) {
        final List<OrderEntryInfoData> orderInfoEntries = new ArrayList<>();

        for (OrderEntryData entry : entries) {
            ProductData productData = entry.getProduct();
            final OrderEntryInfoData orderEntryInfo = new OrderEntryInfoData();
            orderEntryInfo.setName(productData.getName());
            orderEntryInfo.setDescription(productData.getDescription());
            orderEntryInfo.setImageUrl(getImageUrl(productData));
            orderEntryInfo.setFeatures(getFeatureNames(productData));
            orderEntryInfo.setSapCode(entry.getProduct().getSapProductCode());

            if (entry.getQuantity() != null) {
                orderEntryInfo.setQuantity(String.valueOf(entry.getQuantity()));
            }

            if (entry.getBasePrice() != null) {
                orderEntryInfo.setPrice(formatPrice(entry.getBasePrice().getValue().doubleValue()));
            }

            if (entry.getTotalPrice() != null) {
                orderEntryInfo.setTotal(formatPrice(entry.getTotalPrice().getValue().doubleValue()));
            }

            orderInfoEntries.add(orderEntryInfo);
        }
        orderInfoData.setOrderEntries(orderInfoEntries);
    }

    private List<String> getFeatureNames(final ProductData productData) {
        if (CollectionUtils.isNotEmpty(productData.getIncludeFeatures())) {
            return productData.getIncludeFeatures().stream()
                    .map(FeatureData::getName)
                    .collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    protected String getImageUrl(final ProductData productData) {
        if (CollectionUtils.isNotEmpty(productData.getImages())) {
            return productData.getImages().stream()
                    .filter(image -> CART_IMAGE_FORMAT.equals(image.getFormat()))
                    .findFirst()
                    .map(ImageData::getUrl)
                    .orElse(StringUtils.EMPTY);
        }
        return StringUtils.EMPTY;
    }

    private void logCartModelInformation(final CartModel cartModel)
    {
        final UserModel user = cartModel.getUser();
        LOG.debug(String.format(
                "Creating authorization request for CartModel with code %s, total price %s, user %s,"
                        + " total tax %s, subtotal %s, delivery cost: %s, isNet %s",
                cartModel.getCode(), cartModel.getTotalPrice(), user != null ? user.getUid() : "",
                cartModel.getTotalTax(), cartModel.getSubtotal(), cartModel.getDeliveryCost(),
                cartModel.getNet()));
    }

    private void logOrderInfoDataInformation(final OrderInfoData orderInfoData)
    {
        StringBuilder sb = new StringBuilder();
        sb.append("Cart Number ").append(orderInfoData.getOrderNumber())
                .append(" with Currency Isocode ").append(orderInfoData.getCurrencySymbol());
        sb.append(", TotalPriceWithTax ").append(orderInfoData.getOrderTotal());
        sb.append(", Subtotal ,").append(orderInfoData.getSubtotal());
        sb.append(", SubtotalWithoutDiscount ").append(orderInfoData.getSubtotalWithoutDiscount());
        sb.append(", Discounts ").append(orderInfoData.getDiscount());
        sb.append(", TaxAmount ").append(orderInfoData.getTaxAmount());
        sb.append(", Discounts ").append(orderInfoData.getDiscount());
        sb.append(", ShippingCost ").append(orderInfoData.getShippingCost());
        sb.append(", StudentFlow ").append(orderInfoData.isStudentFlow());
        sb.append(", OrderWithoutShipping ").append(orderInfoData.isOrderWithoutShipping());
        LOG.debug(sb.toString());
    }


    private void logOrderEntryInformation(final OrderEntryInfoData orderEntryData, final String productCode,
                                          final String orderNumber)
    {
        StringBuilder sb = new StringBuilder();
        sb.append("Cart number ").append(orderNumber).append(", Product Code ").append(productCode)
                .append(", Base Price ").append(orderEntryData.getPrice());
        sb.append(", Total entry price ").append(orderEntryData.getTotal());
        sb.append(", Entry quantity ,").append(orderEntryData.getQuantity());
        LOG.debug(sb.toString());
    }

}
