package com.wiley.facades.wileyb2b.order.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.annotation.Nonnull;

import org.springframework.util.Assert;

import com.wiley.core.integration.ExternalCartModification;
import com.wiley.facades.order.data.CartModificationMessage;


/**
 * Populator for CartModificationMessage
 */
public class CartModificationMessagePopulator implements Populator<ExternalCartModification, CartModificationMessage>
{
	@Override
	public void populate(@Nonnull final ExternalCartModification externalCartModification,
			@Nonnull final CartModificationMessage cartModificationMessage)
			throws ConversionException
	{
		Assert.notNull(externalCartModification);
		Assert.notNull(cartModificationMessage);

		cartModificationMessage.setType(externalCartModification.getMessageType());
		cartModificationMessage.setMessage(externalCartModification.getMessage());
	}
}
