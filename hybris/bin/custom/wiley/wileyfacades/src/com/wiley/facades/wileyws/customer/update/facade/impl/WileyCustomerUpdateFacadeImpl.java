package com.wiley.facades.wileyws.customer.update.facade.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.function.Consumer;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;

import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import com.wiley.core.customer.service.WileyCustomerAccountService;
import com.wiley.facades.wileyws.customer.update.dto.CustomerUpdateRequest;
import com.wiley.facades.wileyws.customer.update.facade.WileyCustomerUpdateFacade;


/**
 * Default implementation of {@link WileyCustomerUpdateFacade}.
 * Created by Csanad_Toth-Benedek on 7/26/2016.
 */
public class WileyCustomerUpdateFacadeImpl implements WileyCustomerUpdateFacade
{
	@Resource
	private WileyCustomerAccountService customerAccountService;

	@Resource
	private UserService userService;

	@Override
	public void updateCustomer(@NotNull final String customerId, @NotNull final CustomerUpdateRequest customerUpdateRequest)
			throws DuplicateUidException
	{
		Assert.notNull(customerId, "customerId can not be null");
		Assert.notNull(customerUpdateRequest, "customerUpdateRequest can not be null");

		final UserModel userModel = userService.getUserForUID(customerId);
		Assert.state(userModel instanceof CustomerModel, "User must be a customer");
		final CustomerModel customerModel = (CustomerModel) userModel;

		if (customerModel instanceof B2BCustomerModel)
		{
			setAttributeForUpdate(customerUpdateRequest.getEmail(), value -> ((B2BCustomerModel) customerModel).setEmail(value));
		}
		else
		{
			setAttributeForUpdate(customerUpdateRequest.getIndividualEcidNumber(), value -> customerModel.setEcidNumber(value));
			setAttributeForUpdate(customerUpdateRequest.getIndividualSapAccountNumber(),
					value -> customerModel.setSapAccountNumber(value));
		}

		setAttributeForUpdate(customerUpdateRequest.getFirstName(), value -> customerModel.setFirstName(value));
		setAttributeForUpdate(customerUpdateRequest.getLastName(), value -> customerModel.setLastName(value));

		customerAccountService.updateCustomerProfile(customerModel);
	}

	private void setAttributeForUpdate(final String valueToUpdate, final Consumer<String> consumer)
	{
		if (!StringUtils.isEmpty(valueToUpdate))
		{
			consumer.accept(valueToUpdate);
		}
	}
}
