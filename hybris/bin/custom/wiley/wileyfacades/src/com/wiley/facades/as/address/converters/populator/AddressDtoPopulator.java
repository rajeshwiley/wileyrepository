package com.wiley.facades.as.address.converters.populator;

import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.annotation.Nonnull;

import org.springframework.util.Assert;

import com.wiley.core.integration.address.dto.AddressDto;



public class AddressDtoPopulator implements Populator<AddressData, AddressDto>
{
	@Override
	public void populate(@Nonnull final AddressData source, @Nonnull final AddressDto target)
			throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setTown(source.getTown());
		target.setLine1(source.getLine1());
		target.setLine2(source.getLine2());

		if (source.getRegion() != null)
		{
			target.setRegionIso2(source.getRegion().getIsocodeShort());
		}
		if (source.getCountry() != null)
		{
			target.setCountryIso2(source.getCountry().getIsocode());
		}

		target.setPostalCode(source.getPostalCode());
	}
}
