package com.wiley.facades.order.converters.populator;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.product.ProductModel;

import com.paypal.hybris.facade.order.converters.populator.PayPalDeliveryOrderGroupPopulator;
import com.wiley.core.product.WileyProductEditionFormatService;


public class WileyDeliveryOrderEntryGroupPopulator extends PayPalDeliveryOrderGroupPopulator
{

	private WileyProductEditionFormatService wileyProductEditionFormatService;

	@Override
	public long sumDeliveryItemsQuantity(final AbstractOrderModel source)
	{
		long sum = 0;
		for (final AbstractOrderEntryModel entryModel : source.getEntries())
		{
			ProductModel productModel = entryModel.getProduct();
			boolean notDigitalProduct = !wileyProductEditionFormatService.isDigitalProduct(productModel);
			if (notDigitalProduct && entryModel.getDeliveryPointOfService() == null)
			{
				sum += entryModel.getQuantity().longValue();
			}
		}
		return sum;
	}

	public void setWileyProductEditionFormatService(final WileyProductEditionFormatService wileyProductEditionFormatService)
	{
		this.wileyProductEditionFormatService = wileyProductEditionFormatService;
	}
}
