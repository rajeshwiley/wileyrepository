package com.wiley.facades.wileycom.urlencoder;

import de.hybris.platform.acceleratorfacades.urlencoder.UrlEncoderFacade;

import java.net.URISyntaxException;

import javax.annotation.Nonnull;


/**
 * UrlEncoderFacade extension that adds functionality to handle encoding URL attribute
 */
public interface WileycomEncoderFacade extends UrlEncoderFacade
{
	/**
	 * Substitute encoder attribute value in <code>uri</code>.
	 *
	 * @param uri
	 * 		Uri
	 * @param originalContextPath
	 * 		Request context path. Context path will be ignored in <code>uri</code> part. Should not include encoder attributes
	 * @param encodingAttributeName
	 * 		Name that corresponds to attribute manager.
	 * 		See <code>urlEncoderService.urlEncodingAttributeManagerMap</code> bean property.
	 * @param value
	 * 		Value that need to be placed on the URI
	 * @return Calculated URI
	 */
	@Nonnull
	String substituteValue(@Nonnull String uri, @Nonnull String originalContextPath,
			@Nonnull String encodingAttributeName, @Nonnull String value) throws URISyntaxException;

}
