package com.wiley.facades.wileyb2c.cms.wileyimagecarouselcomponent;

import de.hybris.platform.commercefacades.product.ImageFormatMapping;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.model.WileyImageCarouselBreakpointConfigurationModel;
import com.wiley.core.model.WileyImageCarouselConfigurationModel;
import com.wiley.facades.wileyb2c.cms.WileyImageCarouselBreakpointConfigData;
import com.wiley.facades.wileyb2c.cms.WileyImageCarouselConfigData;


public class WileyImageCarouselConfigPopulator
		implements Populator<WileyImageCarouselConfigurationModel, WileyImageCarouselConfigData>
{

	private final WileyImageCarouselBreakpointConfigPopulator wileyImageCarouselBreakpointConfigPopulator;

	private ImageFormatMapping imageFormatMapping;

	@Autowired
	public WileyImageCarouselConfigPopulator(
			final WileyImageCarouselBreakpointConfigPopulator wileyImageCarouselBreakpointConfigPopulator)
	{
		this.wileyImageCarouselBreakpointConfigPopulator = wileyImageCarouselBreakpointConfigPopulator;
	}

	@Override
	public void populate(final WileyImageCarouselConfigurationModel source,
			final WileyImageCarouselConfigData target) throws ConversionException
	{
		target.setItems(source.getItems());
		target.setMargin(source.getMargin());
		target.setLoop(source.getLoop());
		target.setCenter(source.getCenter());
		target.setMouseDrag(source.getMouseDrag());
		target.setTouchDrag(source.getTouchDrag());
		target.setPullDrag(source.getPullDrag());
		target.setFreeDrag(source.getFreeDrag());
		target.setStagePadding(source.getStagePadding());
		target.setNav(source.getNav());
		target.setNavText(StringUtils.EMPTY);
		target.setRewind(source.getRewind());
		target.setDots(source.getDots());
		target.setAutoplay(source.getAutoplay());
		target.setAutoplayTimeout(source.getAutoplayTimeout());
		target.setAutoplayHoverPause(source.getAutoplayHoverPause());

		final Map<String, WileyImageCarouselBreakpointConfigData> breakpointsMap =
				convertBreakpoints(source.getBreakpointConfigurations());
		target.setResponsive(breakpointsMap);
	}

	private Map<String, WileyImageCarouselBreakpointConfigData> convertBreakpoints(
			final Collection<WileyImageCarouselBreakpointConfigurationModel> breakpointConfigurations)
	{
		if (CollectionUtils.isNotEmpty(breakpointConfigurations))
		{
			Map<String, WileyImageCarouselBreakpointConfigData> resultMap = new HashMap<>();
			breakpointConfigurations.forEach(breakpoint ->
			{
				String responsiveImageFormatQualifier = breakpoint.getBreakpoint().getQualifier();
				final String key = imageFormatMapping.getMediaFormatQualifierForImageFormat(responsiveImageFormatQualifier);
				WileyImageCarouselBreakpointConfigData breakpointData = new WileyImageCarouselBreakpointConfigData();
				wileyImageCarouselBreakpointConfigPopulator.populate(breakpoint, breakpointData);
				resultMap.put(key, breakpointData);
			});
			return resultMap;
		}
		return null;
	}

	@Required
	public void setImageFormatMapping(final ImageFormatMapping imageFormatMapping)
	{
		this.imageFormatMapping = imageFormatMapping;
	}
}
