package com.wiley.facades.as.i18n.comparators;

import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.RegionData;

import java.util.Comparator;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Required;


public class AddressComparator implements Comparator<AddressData>, InitializingBean
{
	private Comparator<CountryData> countryDataComparator;

	private Comparator<RegionData> regionDataComparator;

	private static final Comparator<String> STRING_NULL_FRIENDLY_COMPARATOR = Comparator.nullsLast(Comparator.naturalOrder());

	private Comparator<AddressData> addressComparator;

	@Override
	public int compare(final AddressData o1, final AddressData o2)
	{
		return addressComparator.compare(o1, o2);
	}

	@Override
	public void afterPropertiesSet()
	{
		addressComparator = Comparator
				.comparing(AddressData::getCountry, Comparator.nullsLast(getCountryDataComparator()))
				.thenComparing(AddressData::getRegion, Comparator.nullsLast(getRegionDataComparator()))
				.thenComparing(AddressData::getFirstName, STRING_NULL_FRIENDLY_COMPARATOR)
				.thenComparing(AddressData::getLastName, STRING_NULL_FRIENDLY_COMPARATOR)
				.thenComparing(AddressData::getLine1, STRING_NULL_FRIENDLY_COMPARATOR)
				.thenComparing(AddressData::getLine2, STRING_NULL_FRIENDLY_COMPARATOR)
				.thenComparing(AddressData::getTown, STRING_NULL_FRIENDLY_COMPARATOR)
				.thenComparing(AddressData::getPostalCode, STRING_NULL_FRIENDLY_COMPARATOR)
				.thenComparing(AddressData::getPhone, STRING_NULL_FRIENDLY_COMPARATOR)
				.thenComparing(AddressData::getEmail, STRING_NULL_FRIENDLY_COMPARATOR);
	}

	public Comparator<CountryData> getCountryDataComparator()
	{
		return countryDataComparator;
	}

	@Required
	public void setCountryDataComparator(
			final Comparator<CountryData> countryDataComparator)
	{
		this.countryDataComparator = countryDataComparator;
	}

	public Comparator<RegionData> getRegionDataComparator()
	{
		return regionDataComparator;
	}

	@Required
	public void setRegionDataComparator(final Comparator<RegionData> regionDataComparator)
	{
		this.regionDataComparator = regionDataComparator;
	}
}
