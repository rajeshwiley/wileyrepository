package com.wiley.facades.as.address.impl;

import de.hybris.platform.commercefacades.address.data.AddressVerificationResult;
import de.hybris.platform.commercefacades.address.impl.DefaultAddressVerificationFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.address.AddressVerificationDecision;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Collections;

import javax.annotation.Nonnull;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.enums.PaymentModeEnum;
import com.wiley.core.integration.address.dto.AddressDto;
import com.wiley.core.integration.address.dto.AddressVerificationResultDto;
import com.wiley.core.integration.address.service.WileyasAddressVerificationService;
import com.wiley.core.order.WileyCartService;
import com.wiley.facades.as.address.WileyasAddressVerificationFacade;


public class WileyasAddressVerificationFacadeImpl extends DefaultAddressVerificationFacade
		implements WileyasAddressVerificationFacade
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyasAddressVerificationFacadeImpl.class);

	private WileyasAddressVerificationService wileyasAddressVerificationService;
	private Converter<AddressDto, AddressData> addressDtoConverter;
	private Converter<AddressData, AddressDto> addressDtoReverseConverter;
	private WileyCartService wileyCartService;

	@Override
	public AddressVerificationResult<AddressVerificationDecision> verifyAddressData(final AddressData addressData)
	{
		AddressVerificationResult<AddressVerificationDecision> result = new AddressVerificationResult<>();

		try
		{
			final AddressVerificationResultDto verificationResult = wileyasAddressVerificationService
					.verifyAddress(addressDtoReverseConverter.convert(addressData));
			result.setDecision(verificationResult.getDecision());
			result.setSuggestedAddresses(addressDtoConverter.convertAll(verificationResult.getSuggestedAddresses()));
			result.setSkipAddressDoctor(isSkipPopup(verificationResult));
			result.setHasSuggestions(hasSuggestions(verificationResult));
		}
		catch (Exception e)
		{
			/*
			 According to AC4: (Address Doctor is unavailable or wrong response codes)
			 In case of any AddressDoctor-related issue, system should silently skip showing of
			 the suggestion pop-up.
			*/
			result.setDecision(AddressVerificationDecision.UNKNOWN);
			result.setSkipAddressDoctor(true);
			result.setSuggestedAddresses(Collections.emptyList());
			LOG.error("Unable to verify an address: " + e.getMessage(), e);
		}

		return result;
	}

	@Override
	public boolean shouldCallSuggestionDoctor(final String paymentMode)
	{
		return isNotPayPalPaymentMode(paymentMode);
	}

	@Override
	public boolean shouldCallCheckoutCartAddressValidation()
	{
		final PaymentModeModel paymentMode = wileyCartService.getSessionCart().getPaymentMode();
		return paymentMode == null ? true : isNotPayPalPaymentMode(paymentMode.getCode());
	}

	protected boolean isNotPayPalPaymentMode(final String paymentMode)
	{
		return StringUtils.isEmpty(paymentMode) || !PaymentModeEnum.PAYPAL.getCode().equals(paymentMode);
	}

	@Required
	public void setWileyasAddressVerificationService(
			final WileyasAddressVerificationService wileyasAddressVerificationService)
	{
		this.wileyasAddressVerificationService = wileyasAddressVerificationService;
	}

	@Required
	public void setAddressDtoConverter(
			final Converter<AddressDto, AddressData> addressDtoConverter)
	{
		this.addressDtoConverter = addressDtoConverter;
	}

	@Required
	public void setAddressDtoReverseConverter(
			final Converter<AddressData, AddressDto> addressDtoReverseConverter)
	{
		this.addressDtoReverseConverter = addressDtoReverseConverter;
	}

	@Required
	public void setWileyCartService(final WileyCartService wileyCartService)
	{
		this.wileyCartService = wileyCartService;
	}

	protected boolean hasSuggestions(@Nonnull final AddressVerificationResultDto verificationResult)
	{
		return AddressVerificationDecision.REVIEW.equals(verificationResult.getDecision())
				&& CollectionUtils.isNotEmpty(verificationResult.getSuggestedAddresses());
	}

	protected boolean isSkipPopup(@Nonnull final AddressVerificationResultDto verificationResult)
	{
		return AddressVerificationDecision.UNKNOWN.equals(verificationResult.getDecision())
				|| (AddressVerificationDecision.REVIEW.equals(verificationResult.getDecision())
				&& CollectionUtils.isEmpty(verificationResult.getSuggestedAddresses()));
	}
}
