package com.wiley.facades.subscription;

import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;

import java.util.List;

import javax.annotation.Nonnull;

import com.wiley.core.subscription.exception.SubscriptionUpdateException;
import com.wiley.facades.product.data.WileySubscriptionData;
import com.wiley.facades.product.data.WileySubscriptionPaymentTransactionData;


public interface Wileyb2cSubscriptionFacade
{
	/**
	 * Retrieves subscription page for the current user
	 *
	 * @param pageableData
	 */
	SearchPageData<WileySubscriptionData> getSubscriptionPage(PageableData pageableData);

	/**
	 * Method updates {@link de.hybris.platform.jalo.order.payment.PaymentInfo paymentInfo}
	 * from subscribe with {@link java.lang.String subscribeId}.
	 *
	 * @param subscriptionCode
	 * 		subscribe id
	 * @param paymentInfoId
	 * 		the id of new paymentInfo. It must be connected to currentuser account
	 */
	void updateCreditCardPaymentInfo(String subscriptionCode, String paymentInfoId) throws SubscriptionUpdateException;

	void setSubscriptionRequireRenewal(String subscriptionCode, boolean value) throws SubscriptionUpdateException;

	void updatePaypalPaymentInfo(String subscriptionCode, String paymentInfoId) throws SubscriptionUpdateException;

	/**
	 * Get the specified subscription's data limited for the current user.
	 *
	 * @param subscriptionCode - The given subscription's code
	 * @return - The referenced subscription data object
	 */
	WileySubscriptionData getSubscriptionDataForCurrentUser(String subscriptionCode) throws IllegalArgumentException;

	/**
	 * Get the specified subscription's data limited for the current user.
	 *
	 * @param subscriptionCode
	 * 		- The given subscription's code
	 * @param options
	 * 		- options for populating data
	 * @return - The referenced subscription data object
	 */
	WileySubscriptionData getSubscriptionDataForCurrentUserWithOptions(@Nonnull String subscriptionCode,
			@Nonnull List<WileySubscriptionOption> options) throws IllegalArgumentException;


	SearchPageData<WileySubscriptionPaymentTransactionData> getSubscriptionBillingActivities(String subscriptionCode,
			PageableData pageableData);

	/**
	 * Update subscription with given delivery address.
	 *
	 * @param subscriptionCode
	 * @param deliveryAddressCode
	 */
	boolean updateDeliveryAddress(String subscriptionCode, String deliveryAddressCode);

	/**
	 * Checks if customer has subscription for product
	 *
	 * @param productCode
	 * @param customerUid
	 * @return
	 */
	boolean isCustomerHasSubscription(String productCode, String customerUid);

	/**
	 * Checks if product has subscription subtype and at least one subscription term
	 * @param productCode
	 * @return
	 */
	boolean isProductSubscription(String productCode);

	/**
	 * Checks if product has subscription subtype and free trial subscription term
	 * @param productCode
	 * @return
	 */
	boolean isProductFreeTrial(String productCode);

}
