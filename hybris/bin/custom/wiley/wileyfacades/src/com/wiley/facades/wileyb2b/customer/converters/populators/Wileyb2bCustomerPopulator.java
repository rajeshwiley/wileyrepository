package com.wiley.facades.wileyb2b.customer.converters.populators;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BUnitService;
import de.hybris.platform.b2bcommercefacades.company.data.B2BUnitData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;


public class Wileyb2bCustomerPopulator implements Populator<CustomerModel, CustomerData>
{

	private B2BUnitService<B2BUnitModel, UserModel> b2bUnitService;
	private Converter<AddressModel, AddressData> addressConverter;

	@Override
	public void populate(final CustomerModel source, final CustomerData target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		if (source instanceof B2BCustomerModel)
		{
			target.setEmail(((B2BCustomerModel) source).getEmail());
			final B2BCustomerModel customer = (B2BCustomerModel) source;
			final B2BUnitModel parent = getB2bUnitService().getParent(customer);

			final B2BUnitData b2BUnitData = new B2BUnitData();

			b2BUnitData.setUid(parent.getUid());
			b2BUnitData.setName(parent.getLocName());
			b2BUnitData.setActive(parent.getActive());
			if (parent.getBillingAddress() != null)
			{
				b2BUnitData.setBillingAddress(getAddressConverter().convert(parent.getBillingAddress()));
			}

			target.setUnit(b2BUnitData);
		}
	}

	@Required
	public void setB2bUnitService(final B2BUnitService<B2BUnitModel, UserModel> b2bUnitService)
	{
		this.b2bUnitService = b2bUnitService;
	}

	public B2BUnitService<B2BUnitModel, UserModel> getB2bUnitService()
	{
		return b2bUnitService;
	}

	protected Converter<AddressModel, AddressData> getAddressConverter()
	{
		return addressConverter;
	}

	@Required
	public void setAddressConverter(final Converter<AddressModel, AddressData> addressConverter)
	{
		this.addressConverter = addressConverter;
	}
}
