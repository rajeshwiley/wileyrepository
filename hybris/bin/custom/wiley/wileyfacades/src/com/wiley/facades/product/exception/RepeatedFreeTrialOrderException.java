package com.wiley.facades.product.exception;

import de.hybris.platform.servicelayer.exceptions.BusinessException;


/**
 * Throws when customer has already have order with the same free trial exception
 */
public class RepeatedFreeTrialOrderException extends BusinessException
{
	public RepeatedFreeTrialOrderException(final String message)
	{
		super(message);
	}
}
