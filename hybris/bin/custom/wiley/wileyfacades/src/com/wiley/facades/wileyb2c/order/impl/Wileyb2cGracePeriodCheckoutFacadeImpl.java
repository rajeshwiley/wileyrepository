package com.wiley.facades.wileyb2c.order.impl;

import com.wiley.core.enums.OrderType;
import com.wiley.core.product.WileyProductService;
import com.wiley.facades.wileyb2c.order.Wileyb2cGracePeriodCheckoutFacade;
import com.wiley.facades.wileyplus.cart.activation.dto.CartActivationRequestDto;
import com.wiley.facades.wileyplus.cart.activation.dto.WebLinkDto;
import com.wiley.facades.wileyplus.cart.activation.dto.WileyPlusCourseDto;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.order.CommerceCheckoutService;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.commerceservices.service.data.CommerceOrderResult;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.order.CalculationService;
import de.hybris.platform.order.CartFactory;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.testframework.Transactional;
import de.hybris.platform.util.DiscountValue;

import javax.annotation.Resource;
import java.util.Collections;

public class Wileyb2cGracePeriodCheckoutFacadeImpl implements Wileyb2cGracePeriodCheckoutFacade
{
	private static final String GRACE_PERIOD_DISCOUNT = "Grace Period Discount";

	@Resource
	private CartFactory wileyCartFactory;

	@Resource
	private ModelService modelService;

	@Resource
	private CommerceCartService wileyb2cCommerceCartService;

	@Resource
	private WileyProductService productService;

	@Resource
	private CommerceCheckoutService commerceCheckoutService;

	@Resource
	private CalculationService calculationService;

	@Resource
	private Populator<CartActivationRequestDto, AbstractOrderEntryModel> wileyb2cOrderEntryCoursePopulator;

	@Override
	@Transactional
	public String placeGracePeriodOrder(final CartActivationRequestDto activationRequestDto)
			throws CommerceCartModificationException, InvalidCartException, CalculationException
	{
		WileyPlusCourseDto course = activationRequestDto.getCourse();
		ProductModel productModel = productService.getProductForIsbn(course.getIsbn());

		final CartModel cartModel = createCart(activationRequestDto);
		addGracePeriodCourse(productModel, activationRequestDto, cartModel);
		return placeGracePeriodOrder(cartModel);
	}

	private CartModel createCart(final CartActivationRequestDto activationRequestDto)
	{
		final CartModel cartModel = wileyCartFactory.createCart();
		WebLinkDto continueURL = activationRequestDto.getContinueURL();
		if (continueURL != null) {
			cartModel.setConfirmationPageContinueUrl(continueURL.getUrl());
		}
		cartModel.setOrderType(OrderType.GRACE_PERIOD);
		return cartModel;
	}

	private String placeGracePeriodOrder(final CartModel cartModel) throws InvalidCartException
	{
		CommerceCheckoutParameter placeOrderParameter = new CommerceCheckoutParameter();
		placeOrderParameter.setCart(cartModel);
		placeOrderParameter.setEnableHooks(true);
		CommerceOrderResult commerceOrderResult = commerceCheckoutService.placeOrder(placeOrderParameter);
		String orderCode = commerceOrderResult.getOrder().getCode();
		modelService.remove(cartModel);
		return orderCode;
	}

	private void addGracePeriodCourse(final ProductModel productModel, final CartActivationRequestDto activationRequestDto,
									  final CartModel cartModel)
			throws CommerceCartModificationException, CalculationException
	{
		CommerceCartParameter parameter = new CommerceCartParameter();
		parameter.setCart(cartModel);
		parameter.setProduct(productModel);
		parameter.setUnit(productModel.getUnit());
		parameter.setQuantity(1L);
		CommerceCartModification commerceCartModification = wileyb2cCommerceCartService.addToCart(parameter);

		AbstractOrderEntryModel entry = commerceCartModification.getEntry();
		wileyb2cOrderEntryCoursePopulator.populate(activationRequestDto, entry);

		applyDiscount(cartModel);

		calculationService.calculateTotals(cartModel, true);

	}

	private void applyDiscount(final CartModel cartModel)
	{
		DiscountValue discount = new DiscountValue(GRACE_PERIOD_DISCOUNT, 100.0, false, null);
		cartModel.setGlobalDiscountValues(Collections.singletonList(discount));
	}
}
