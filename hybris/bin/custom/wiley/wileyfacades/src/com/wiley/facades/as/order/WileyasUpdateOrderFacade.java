package com.wiley.facades.as.order;

import com.wiley.facades.wiley.order.dto.OrderUpdateRequestDTO;
import de.hybris.platform.commercefacades.order.data.OrderData;

public interface WileyasUpdateOrderFacade {

    OrderData updateOrder(OrderUpdateRequestDTO orderUpdateRequestDTO, boolean saveChanges);

    boolean isEditable(String orderCode);
}
