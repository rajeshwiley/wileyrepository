package com.wiley.facades.wileyb2c.access;

import de.hybris.platform.core.model.order.OrderEntryModel;

import javax.annotation.Nonnull;


/**
 * Generates URL to access digital product
 *
 * @author Dzmitryi_Halahayeu
 */
public interface Wileyb2cDownloadDigitalProductFacade
{
	String generateRedirectUrl(@Nonnull OrderEntryModel orderEntryModel) throws Exception;
}
