package com.wiley.facades.populators;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.category.WileyCategoryService;


/**
 * Contains Partner specific data.
 */
public class ProductPartnerPopulator  implements Populator<ProductModel, ProductData>
{

	@Autowired
	private WileyCategoryService categoryService;

	public static final String WEL_PARTNERS_CATEGORY = "WEL_PARTNERS_CATEGORY";

	@Override
	public void populate(final ProductModel productModel, final ProductData productData) throws ConversionException
	{
		final boolean isPartnerOnly =
				getTopLevelCategories(productModel)
						.stream()
						.noneMatch(
								category -> !StringUtils.equals(WEL_PARTNERS_CATEGORY, category.getCode())
						);
		productData.setPartnerOnly(isPartnerOnly);
	}

	private List<CategoryModel> getTopLevelCategories(final ProductModel productModel) {
		Collection<CategoryModel> categories = productModel.getSupercategories();

		List<CategoryModel> topLevelCategories = new ArrayList<>();
		for (CategoryModel category : categories) {
			topLevelCategories.addAll(categoryService.getTopLevelCategoriesForCategory(category));
		}
		return topLevelCategories;
	}
}
