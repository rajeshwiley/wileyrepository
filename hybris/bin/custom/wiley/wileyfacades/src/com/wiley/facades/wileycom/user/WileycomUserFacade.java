package com.wiley.facades.wileycom.user;

import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;

import javax.annotation.Nonnull;


/**
 * Created by Raman_Hancharou on 7/13/2016.
 */
public interface WileycomUserFacade extends UserFacade
{

	/**
	 * Add address to database even in case of external system exception
	 *
	 * @param addressData
	 * 		the address data
	 */
	void addAddressFromCheckout(@Nonnull AddressData addressData);

	/**
	 * Updates address in database even in case of external system exception
	 *
	 * @param addressData
	 * 		the address data
	 */
	void editAddressFromCheckout(@Nonnull AddressData addressData);
}
