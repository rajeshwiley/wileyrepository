package com.wiley.facades.order.converters.populator;

import de.hybris.platform.commercefacades.order.converters.populator.OrderEntryPopulator;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import com.wiley.core.product.WileyCountableProductService;


/**
 * Extends implementation of {@link OrderEntryPopulator} for WEL site.
 *
 * @author Aliaksei_Zlobich
 */
public class WileyQuantityAvailabilityOrderEntryPopulator implements Populator<AbstractOrderEntryModel, OrderEntryData>
{

	private WileyCountableProductService wileyCountableProductService;

	@Override
	public void populate(@Nonnull final AbstractOrderEntryModel source, @Nonnull final OrderEntryData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		if (source.getProduct() != null)
		{
			target.setDoesProductHaveQuantity(wileyCountableProductService.canProductHaveQuantity(source.getProduct()));
		}
	}


	@Required
	public void setWileyCountableProductService(final WileyCountableProductService wileyCountableProductService)
	{
		this.wileyCountableProductService = wileyCountableProductService;
	}
}
