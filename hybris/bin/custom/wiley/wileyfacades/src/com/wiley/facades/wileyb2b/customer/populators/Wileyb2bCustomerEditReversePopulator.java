package com.wiley.facades.wileyb2b.customer.populators;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import com.wiley.facades.wileyb2b.customer.converters.populators.Wileyb2bCustomerReversePopulator;


public class Wileyb2bCustomerEditReversePopulator extends Wileyb2bCustomerReversePopulator
		implements Populator<CustomerData, B2BCustomerModel>
{
	@Override
	public void populate(final CustomerData source, final B2BCustomerModel target) throws ConversionException
	{
		String email = target.getEmail();
		super.populate(source, target);
		//Needed because we don't want to override the existing value with null in the database
		if (target.getEmail() == null)
		{
			target.setEmail(email);
		}
	}
}
