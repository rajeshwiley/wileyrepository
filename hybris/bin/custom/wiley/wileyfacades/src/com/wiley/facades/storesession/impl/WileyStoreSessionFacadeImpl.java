package com.wiley.facades.storesession.impl;

import de.hybris.platform.commercefacades.storesession.impl.DefaultStoreSessionFacade;

import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.storesession.WileyStoreSessionService;
import com.wiley.facades.storesession.WileyStoreSessionFacade;


/**
 * Wiley store session facade implementation
 */
public class WileyStoreSessionFacadeImpl extends DefaultStoreSessionFacade implements WileyStoreSessionFacade
{

	@Autowired
	private WileyStoreSessionService wileyStoreSessionService;

	@Override
	public void setCurrentCurrency(final String isocode, final boolean recalculateCart)
	{
		wileyStoreSessionService.setCurrentCurrency(isocode, recalculateCart);
	}
}
