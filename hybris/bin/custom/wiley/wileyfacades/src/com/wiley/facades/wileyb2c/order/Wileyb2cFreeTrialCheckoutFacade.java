package com.wiley.facades.wileyb2c.order;

import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.order.InvalidCartException;


/**
 * Created by Uladzimir_Barouski on 9/20/2016.
 */
public interface Wileyb2cFreeTrialCheckoutFacade
{
	/**
	 * Place free trial order and update customer info
	 *
	 * @param code FreeTrial product code
	 * @return submitted order code
	 */
	String placeFreeTrialOrder(String code) throws CommerceCartModificationException, InvalidCartException;
}
