package com.wiley.facades.wileyb2c.customer.converters.populators;

import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.annotation.Nonnull;

import org.springframework.util.Assert;

import com.wiley.core.enums.WileyDigitalContentTypeEnum;


/**
 * @author Dzmitryi_Halahayeu
 */
public class Wileyb2cOrderEntryDigitalAccessParametersPopulator
		implements Populator<AbstractOrderEntryModel, OrderEntryData>
{
	@Override
	public void populate(@Nonnull final AbstractOrderEntryModel source, @Nonnull final OrderEntryData target)
			throws ConversionException
	{
		Assert.notNull(source.getOrder());
		Assert.notNull(source.getProduct());
		target.setOrderCode(source.getOrder().getCode());
		target.setDigitalAccessToken(source.getDigitalProductAccessToken());
		target.setNeedAccessButton(isNeedAccessButton(source));
		target.setNeedAccessToken(isVitalSourceEntitlement(source));
	}

	private boolean isVitalSourceEntitlement(@Nonnull final AbstractOrderEntryModel source)
	{
		return WileyDigitalContentTypeEnum.VITAL_SOURCE.equals(source.getProduct().getDigitalContentType());
	}

	private boolean isNeedAccessButton(@Nonnull final AbstractOrderEntryModel source)
	{
		return source.getProduct().getDigitalContentType() != null;
	}
}
