package com.wiley.facades.user.impl;

import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.impl.DefaultUserFacade;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.order.WileyCartService;
import com.wiley.facades.order.WileyCheckoutFacade;
import com.wiley.facades.user.WileyUserFacade;


/**
 * UserFacadeImpl to restrict persisting session language and currency only for
 * non anonymous users.
 */
public class WileyUserFacadeImpl extends DefaultUserFacade implements WileyUserFacade
{
	@Autowired
	private WileyCartService wileyCartService;

	@Autowired
	private WileyCheckoutFacade wileyCheckoutFacade;

	@Override
	public void syncSessionLanguage()
	{
		if (!isAnonymousUser())
		{
			super.syncSessionLanguage();
		}
	}

	@Override
	public void syncSessionCurrency()
	{
		if (!isAnonymousUser())
		{
			super.syncSessionCurrency();
		}
	}

	@Override
	public List<AddressData> getPaymentAddressesForCurrentUser()
	{
		final CustomerModel currentCustomer = getCurrentUserForCheckout();
		final List<AddressModel> allAddresses = getCustomerAccountService().getAllAddressEntries(currentCustomer);

		Stream<AddressModel> stream = allAddresses.stream()
				.filter(address -> Boolean.TRUE.equals(address.getVisibleInAddressBook()));

		if (!wileyCheckoutFacade.getCheckoutCart().getOrderConfiguration().isPermitChangePaymentCountry())
		{
			final CartModel sessionCart = wileyCartService.getSessionCart();
			ServicesUtil.validateParameterNotNull(sessionCart.getCountry(), "CountryModel cannot be null");
			final String isocode = sessionCart.getCountry().getIsocode();
			stream = stream.filter(address -> address.getCountry() != null)
					.filter(address -> isocode.equals(address.getCountry().getIsocode()));
		}
		return getAddressConverter().convertAll(stream.collect(Collectors.toList()));
	}
}
