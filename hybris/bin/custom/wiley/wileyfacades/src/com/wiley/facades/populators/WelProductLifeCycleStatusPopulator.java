package com.wiley.facades.populators;

import de.hybris.platform.commercefacades.product.converters.populator.AbstractProductPopulator;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.annotation.Resource;

import com.wiley.core.enums.WileyProductLifecycleEnum;
import com.wiley.core.wel.preorder.strategy.ProductLifecycleStatusStrategy;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;



public class WelProductLifeCycleStatusPopulator<SOURCE extends ProductModel, TARGET extends ProductData>
		extends AbstractProductPopulator<SOURCE, TARGET>
{
	@Resource(name = "welProductLifecycleStatusStrategy")
	private ProductLifecycleStatusStrategy welProductLifecycleStatusStrategy;

	@Override
	public void populate(final SOURCE productModel, final TARGET productData) throws ConversionException
	{
		validateParameterNotNull(productModel, "productModel can't be null");
		validateParameterNotNull(productData, "productData can't be null");

		WileyProductLifecycleEnum lifecycleStatus = welProductLifecycleStatusStrategy.getLifecycleStatusForProduct(productModel);
		productData.setLifecycleStatus(lifecycleStatus);
	}
}
