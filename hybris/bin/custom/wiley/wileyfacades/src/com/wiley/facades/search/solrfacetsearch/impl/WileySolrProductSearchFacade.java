package com.wiley.facades.search.solrfacetsearch.impl;

import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commercefacades.search.solrfacetsearch.impl.DefaultSolrProductSearchFacade;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;

import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;

import com.wiley.facades.search.solrfacetsearch.WileyProductSearchFacade;


public class WileySolrProductSearchFacade extends DefaultSolrProductSearchFacade implements WileyProductSearchFacade
{
	private static final int VALID_ISBN_LENGTH = 13;

	private Pattern isbnStrategyPattern;

	@Override
	protected SolrSearchQueryData decodeState(final SearchStateData searchState, final String categoryCode)
	{
		final SolrSearchQueryData searchQueryData = super.decodeState(searchState, categoryCode);
		searchQueryData.setPageType(searchState.getPageType());
		searchQueryData.setFacetCode(searchState.getFacetCode());
		searchQueryData.setUseFacetViewMoreLimit(searchState.getUseFacetViewMoreLimit());
		searchQueryData.setFacetLimit(searchState.getFacetLimit());

		return searchQueryData;
	}

	@Override
	public String cleanupDashesFromSearchIsbn(final String searchText)
	{
		String[] searchParts = searchText.split("\\|");
		final int searchTextIndex = 0;
		if (isTextMatchesToIsbn(searchParts[searchTextIndex]))
		{
			final String isbnWithoutDashes = searchParts[searchTextIndex].replaceAll("[\\u2013-]", StringUtils.EMPTY);
			if (isbnWithoutDashes.length() == VALID_ISBN_LENGTH)
			{
				searchParts[searchTextIndex] = isbnWithoutDashes;
			}
		}
		return String.join("|", searchParts);
	}

	private boolean isTextMatchesToIsbn(final String searchPart)
	{
		return isbnStrategyPattern.matcher(searchPart).matches();
	}

	@Value("${search.isbn13WithDashes.pattern}")
	public void setIsbnStrategyPattern(final String pattern)
	{
		this.isbnStrategyPattern = Pattern.compile(pattern);
	}
}
