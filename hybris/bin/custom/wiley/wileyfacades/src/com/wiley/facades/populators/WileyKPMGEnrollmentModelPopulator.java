package com.wiley.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.annotation.Resource;

import com.wiley.core.enums.KpmgFunction;
import com.wiley.core.model.KpmgEnrollmentModel;
import com.wiley.core.model.KpmgOfficeModel;
import com.wiley.core.partner.WileyKPMGEnrollmentService;
import com.wiley.facades.partner.kpmg.KPMGEnrollmentData;


public class WileyKPMGEnrollmentModelPopulator implements Populator<KPMGEnrollmentData, KpmgEnrollmentModel>
{
	@Resource
	private WileyKPMGEnrollmentService wileyKpmgEnrollmentService;

	@Override
	public void populate(final KPMGEnrollmentData source, final KpmgEnrollmentModel target)
			throws ConversionException
	{
		target.setRegistrationId(source.getRegistrationId());
		target.setEmployeeId(source.getEmployeeId());
		target.setFirstName(source.getFirstName());
		target.setLastName(source.getLastName());
		target.setEmail(source.getEmail());

		if (source.getFunction() != null && source.getFunction().getCode() != null)
		{
			final KpmgFunction function = KpmgFunction.valueOf(source.getFunction().getCode());
			target.setFunction(function);
		}

		if (source.getOffice() != null && source.getOffice().getCode() != null)
		{
			KpmgOfficeModel office = wileyKpmgEnrollmentService.getOfficeByCode(source.getOffice().getCode());
			target.setOffice(office);
		}

	}
}
