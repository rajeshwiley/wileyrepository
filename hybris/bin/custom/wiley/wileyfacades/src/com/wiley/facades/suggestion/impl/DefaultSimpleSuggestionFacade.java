/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.facades.suggestion.impl;

import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.suggestion.SimpleSuggestionService;
import com.wiley.facades.suggestion.SimpleSuggestionFacade;


/**
 * Default implementation of {@link SimpleSuggestionFacade}.
 */
public class DefaultSimpleSuggestionFacade implements SimpleSuggestionFacade
{
	private UserService userService;
	private CategoryService categoryService;
	private ProductService productService;
	private Converter<ProductModel, ProductData> productConverter;
	private SimpleSuggestionService simpleSuggestionService;
	private CartService cartService;

	/**
	 * Gets references for purchased in category.
	 *
	 * @param categoryCode
	 * 		the category code
	 * @param referenceTypes
	 * 		the reference types
	 * @param excludePurchased
	 * 		the exclude purchased
	 * @param limit
	 * 		the limit
	 * @return the references for purchased in category
	 */
	@Override
	public List<ProductData> getReferencesForPurchasedInCategory(final String categoryCode,
			final List<ProductReferenceTypeEnum> referenceTypes, final boolean excludePurchased, final Integer limit)
	{
		final UserModel user = getUserService().getCurrentUser();
		final CategoryModel category = getCategoryService().getCategoryForCode(categoryCode);

		final List<ProductModel> suggestions = getSimpleSuggestionService().getReferencesForPurchasedInCategory(category,
				referenceTypes, user, excludePurchased, limit);

		return Converters.convertAll(suggestions, getProductConverter());
	}

	/**
	 * Gets references for products.
	 *
	 * @param productCodes
	 * 		the product codes
	 * @param referenceTypes
	 * 		the reference types
	 * @param excludePurchased
	 * 		the exclude purchased
	 * @param limit
	 * 		the limit
	 * @return the references for products
	 */
	@Override
	public List<ProductData> getReferencesForProducts(final Set<String> productCodes,
			final List<ProductReferenceTypeEnum> referenceTypes, final boolean excludePurchased, final Integer limit)
	{
		final UserModel user = getUserService().getCurrentUser();

		final Set<ProductModel> products = new HashSet<ProductModel>();
		for (final String productCode : productCodes)
		{
			final ProductModel product = getProductService().getProductForCode(productCode);
			products.addAll(getAllBaseProducts(product));
		}

		final List<ProductModel> suggestions = getSimpleSuggestionService().getReferencesForProducts(new LinkedList<ProductModel>(
				products), referenceTypes, user, excludePurchased, limit);

		return Converters.convertAll(suggestions, getProductConverter());
	}

	/**
	 * Gets suggestions for products in cart.
	 *
	 * @param referenceTypes
	 * 		the reference types
	 * @param excludePurchased
	 * 		the exclude purchased
	 * @param limit
	 * 		the limit
	 * @return the suggestions for products in cart
	 */
	@Override
	public List<ProductData> getSuggestionsForProductsInCart(final List<ProductReferenceTypeEnum> referenceTypes,
			final boolean excludePurchased, final Integer limit)
	{
		if (getCartService().hasSessionCart())
		{
			final Set<ProductModel> products = new HashSet<ProductModel>();
			for (final AbstractOrderEntryModel entry : getCartService().getSessionCart().getEntries())
			{
				products.addAll(getAllBaseProducts(entry.getProduct()));
			}
			return Converters.convertAll(getSimpleSuggestionService().getReferencesForProducts(new LinkedList<ProductModel>(
							products), referenceTypes, getUserService().getCurrentUser(), excludePurchased, limit),
					getProductConverter());
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * Gets all base products.
	 *
	 * @param productModel
	 * 		the product model
	 * @return the all base products
	 */
	protected Set<ProductModel> getAllBaseProducts(final ProductModel productModel)
	{
		final Set<ProductModel> allBaseProducts = new HashSet<ProductModel>();

		ProductModel currentProduct = productModel;
		allBaseProducts.add(currentProduct);

		while (currentProduct instanceof VariantProductModel)
		{
			currentProduct = ((VariantProductModel) currentProduct).getBaseProduct();

			if (currentProduct != null)
			{
				allBaseProducts.add(currentProduct);
			}
		}
		return allBaseProducts;
	}

	/**
	 * Gets references for purchased in category.
	 *
	 * @param categoryCode
	 * 		the category code
	 * @param referenceType
	 * 		the reference type
	 * @param excludePurchased
	 * 		the exclude purchased
	 * @param limit
	 * 		the limit
	 * @return the references for purchased in category
	 */
	@SuppressWarnings("deprecation")
	@Deprecated
	@Override
	public List<ProductData> getReferencesForPurchasedInCategory(final String categoryCode,
			final ProductReferenceTypeEnum referenceType, final boolean excludePurchased, final Integer limit)
	{
		final UserModel user = getUserService().getCurrentUser();
		final CategoryModel category = getCategoryService().getCategoryForCode(categoryCode);

		final List<ProductModel> suggestions = getSimpleSuggestionService().getReferencesForPurchasedInCategory(category, user,
				referenceType, excludePurchased, limit);

		return Converters.convertAll(suggestions, getProductConverter());
	}

	/**
	 * Gets user service.
	 *
	 * @return the user service
	 */
	protected UserService getUserService()
	{
		return userService;
	}

	/**
	 * Sets user service.
	 *
	 * @param userService
	 * 		the user service
	 */
	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	/**
	 * Gets category service.
	 *
	 * @return the category service
	 */
	protected CategoryService getCategoryService()
	{
		return categoryService;
	}

	/**
	 * Sets category service.
	 *
	 * @param categoryService
	 * 		the category service
	 */
	@Required
	public void setCategoryService(final CategoryService categoryService)
	{
		this.categoryService = categoryService;
	}

	/**
	 * Gets product service.
	 *
	 * @return the product service
	 */
	protected ProductService getProductService()
	{
		return productService;
	}

	/**
	 * Sets product service.
	 *
	 * @param productService
	 * 		the product service
	 */
	@Required
	public void setProductService(final ProductService productService)
	{
		this.productService = productService;
	}

	/**
	 * Gets product converter.
	 *
	 * @return the product converter
	 */
	protected Converter<ProductModel, ProductData> getProductConverter()
	{
		return productConverter;
	}

	/**
	 * Sets product converter.
	 *
	 * @param productConverter
	 * 		the product converter
	 */
	@Required
	public void setProductConverter(final Converter<ProductModel, ProductData> productConverter)
	{
		this.productConverter = productConverter;
	}

	/**
	 * Gets simple suggestion service.
	 *
	 * @return the simple suggestion service
	 */
	protected SimpleSuggestionService getSimpleSuggestionService()
	{
		return simpleSuggestionService;
	}

	/**
	 * Sets simple suggestion service.
	 *
	 * @param simpleSuggestionService
	 * 		the simple suggestion service
	 */
	@Required
	public void setSimpleSuggestionService(final SimpleSuggestionService simpleSuggestionService)
	{
		this.simpleSuggestionService = simpleSuggestionService;
	}

	/**
	 * Gets cart service.
	 *
	 * @return the cart service
	 */
	protected CartService getCartService()
	{
		return cartService;
	}

	/**
	 * Sets cart service.
	 *
	 * @param cartService
	 * 		the cart service
	 */
	@Required
	public void setCartService(final CartService cartService)
	{
		this.cartService = cartService;
	}
}
