package com.wiley.facades.populators;

import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.stock.CommerceStockService;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.Collections;
import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.facades.product.data.InventoryStatusRecord;



/**
 * Populates {@link ProductData} with {@link InventoryStatusRecord} property
 */
public class ProductInventoryStatusPDPPopulator implements Populator<ProductModel, ProductData>
{
	@Autowired
	private CommerceStockService commerceStockService;
	@Autowired
	private BaseStoreService baseStoreService;

	private static final Logger LOG = Logger.getLogger(ProductInventoryStatusPDPPopulator.class);

	@Override
	public void populate(final ProductModel productModel, final ProductData productData) throws ConversionException
	{
		final BaseStoreModel baseStore = baseStoreService.getCurrentBaseStore();
		final Long stockLevel = commerceStockService.getStockLevelForProductAndBaseStore(productModel, baseStore);
		StockLevelStatus stockLevelStatus = commerceStockService.getStockLevelStatusForProductAndBaseStore(productModel,
				baseStore);

		// TODO : [ECSC-20075] commented out code which uses obsolete attribute. Should be deleted
/*		if (productModel.getReleaseDate() != null && productModel.getReleaseDate().after(new Date()))
		{
			stockLevelStatus = StockLevelStatus.PRE_ORDER;
		}
		else*/
		if (stockLevelStatus.equals(StockLevelStatus.OUTOFSTOCK))
		{
			stockLevelStatus = calculateOutOfStockLevelStatus(productModel, stockLevel);
		}

		final InventoryStatusRecord inventoryStatusRecord = createInventoryStatusRecord(stockLevel, null,
				stockLevelStatus);
		productData.setInventoryStatus(Collections.singletonList(inventoryStatusRecord));
	}

	private StockLevelStatus calculateOutOfStockLevelStatus(final ProductModel productModel, final Long stockLevel)
	{
		if (productModel.getPrintOnDemand() && stockLevel == 0)
		{
			return StockLevelStatus.PRINT_ON_DEMAND;
		}
		else if (stockLevel == 0)
		{
			return StockLevelStatus.BACK_ORDER;
		}
		LOG.warn(productModel.getCode() + " is OUTOFSTOCK, stockLevel = " + stockLevel);
		return null;
	}

	private InventoryStatusRecord createInventoryStatusRecord(final Long quantity,
			final Date availableDate, final StockLevelStatus statusCode)
	{
		final InventoryStatusRecord inventoryStatusRecord = new InventoryStatusRecord();
		inventoryStatusRecord.setQuantity(quantity);
		inventoryStatusRecord.setAvailableDate(availableDate);
		inventoryStatusRecord.setStatusCode(statusCode);
		return inventoryStatusRecord;
	}

	public void setCommerceStockService(final CommerceStockService commerceStockService)
	{
		this.commerceStockService = commerceStockService;
	}

	public void setBaseStoreService(final BaseStoreService baseStoreService)
	{
		this.baseStoreService = baseStoreService;
	}
}
