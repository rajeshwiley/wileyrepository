/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.facades.populators;

import de.hybris.platform.catalog.enums.ClassificationAttributeTypeEnum;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.classification.features.FeatureValue;
import de.hybris.platform.commercefacades.product.data.FeatureData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.enums.ClassAttributeAssignmentTypeEnum;


/**
 * Populates {@link ProductData} with include features
 *
 * @param <SOURCE>
 * 		the type parameter
 * @param <TARGET>
 * 		the type parameter
 */
public class ProductIncludeFeaturesPopulator<SOURCE extends FeatureList, TARGET extends ProductData> implements
		Populator<SOURCE, TARGET>
{
	private Converter<Feature, FeatureData> featureConverter;

	protected Converter<Feature, FeatureData> getFeatureConverter()
	{
		return featureConverter;
	}

	/**
	 * Sets feature converter.
	 *
	 * @param featureConverter
	 * 		the feature converter
	 */
	@Required
	public void setFeatureConverter(final Converter<Feature, FeatureData> featureConverter)
	{
		this.featureConverter = featureConverter;
	}

	@Override
	public void populate(final SOURCE source, final TARGET target) throws ConversionException
	{
		target.setIncludeFeatures(buildIncludeFeaturesList(source));
	}

	protected List<FeatureData> buildIncludeFeaturesList(final FeatureList source)
	{
		final List<FeatureData> result = new ArrayList();
		final List<Feature> features = source.getFeatures();

		if (CollectionUtils.isNotEmpty(features))
		{
			for (final Feature feature : features)
			{
				final ClassAttributeAssignmentModel classAttributeAssignment = feature.getClassAttributeAssignment();
				final FeatureValue featureValue = feature.getValue();
				if (classAttributeAssignment != null
						&& ClassAttributeAssignmentTypeEnum.INCLUDES == classAttributeAssignment.getType()
						&& ClassificationAttributeTypeEnum.BOOLEAN == classAttributeAssignment.getAttributeType()
						&& featureValue != null && Boolean.TRUE.equals(featureValue.getValue()))
				{
					// Create the feature
					final FeatureData newFeature = getFeatureConverter().convert(feature);

					// Add the feature to the result
					result.add(newFeature);
				}
			}
		}

		return result.isEmpty() ? null : result;
	}
}
