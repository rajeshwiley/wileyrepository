/**
 *
 */
package com.wiley.facades.wileycom.order;

import com.google.common.base.Preconditions;
import de.hybris.platform.commercefacades.i18n.comparators.CountryComparator;
import de.hybris.platform.commercefacades.order.CheckoutFacade;
import de.hybris.platform.commercefacades.order.data.DeliveryModeData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.store.BaseStoreModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import com.paypal.hybris.model.PaypalPaymentInfoModel;
import com.wiley.core.address.WileyAddressService;
import com.wiley.core.customer.service.WileyCustomerAccountService;
import com.wiley.core.model.ExternalDeliveryModeModel;
import com.wiley.core.model.WorldRegionModel;
import com.wiley.core.order.WileyCommerceCheckoutService;
import com.wiley.core.payment.WileyPaymentService;
import com.wiley.core.product.WileyProductEditionFormatService;
import com.wiley.core.servicelayer.i18n.WileycomI18NService;
import com.wiley.core.wileycom.order.WileycomDeliveryService;
import com.wiley.facades.order.WileycomCheckoutFacade;
import com.wiley.facades.order.converters.populator.ExternalDeliveryModeReversePopulator;
import com.wiley.facades.order.data.ExternalDeliveryModeData;
import com.wiley.facades.order.impl.WileyCheckoutFacadeImpl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


public class WileycomCheckoutFacadeImpl extends WileyCheckoutFacadeImpl implements WileycomCheckoutFacade
{
	protected static final Logger LOG = Logger.getLogger(WileycomCheckoutFacadeImpl.class);

	@Autowired
	private CheckoutFacade payPalCheckoutFacade;

	@Autowired
	private WileyCustomerAccountService wileyCustomerAccountService;

	@Autowired
	private Populator<AddressData, AddressModel> addressReversePopulator;

	private WileyCommerceCheckoutService wileyCommerceCheckoutService;

	private WileycomDeliveryService wileycomDeliveryService;

	@Autowired
	private ExternalDeliveryModeReversePopulator externalDeliveryModeReversePopulator;

	@Autowired
	private WileyAddressService wileyAddressService;

	@Autowired
	private WileyPaymentService wileyWPGPaymentService;

	@Autowired
	private WileyProductEditionFormatService wileyProductEditionFormatService;

	@Autowired
	private WileycomI18NService wileycomI18NService;

	@Autowired
	private ModelService modelService;

	@Autowired
	private Converter<ExternalDeliveryModeModel, ExternalDeliveryModeData> externalDeliveryModeConverter;


	@Override
	public boolean authorizePayment(final String securityCode)
	{
		final CartModel cartModel = getCart();
		if (checkIfCurrentUserIsTheCartUser())
		{
			final PaymentInfoModel infoModel = cartModel.getPaymentInfo();
			if (infoModel instanceof PaypalPaymentInfoModel)
			{
				return payPalCheckoutFacade.authorizePayment(securityCode);
			}
			else
			{
				final PaymentTransactionEntryModel authorizeEntry = wileyWPGPaymentService.authorize(cartModel);
				return TransactionStatus.ACCEPTED.name().equals(authorizeEntry.getTransactionStatus());
			}
		}
		return super.authorizePayment(securityCode);
	}


	@Override
	public void setNewPaymentAddress(@Nonnull final AddressData addressData)
	{
		if (checkIfCurrentUserIsTheCartUser())
		{
			createAddressModel(addressData);
			setPaymentAddress(addressData.getId().toString());
		}
	}

	@Override
	public void setPaymentAddress(@Nonnull final String addressId)
	{
		final Optional<AddressModel> addressModel = wileyAddressService.getAddressById(addressId);
		Preconditions.checkArgument(addressModel.isPresent(), "address data can not be null");
		final CartModel cartModel = getCart();
		validateParameterNotNullStandardMessage("session cart", cartModel);
		if (checkIfCurrentUserIsTheCartUser())
		{
			setPaymentAddress(addressModel.get());
		}
	}

	private void createAddressModel(@Nonnull final AddressData addressData)
	{
		if (hasCheckoutCart())
		{
			final AddressModel addressModel = modelService.create(AddressModel.class);
			addressReversePopulator.populate(addressData, addressModel);
			addressModel.setOwner(getCart());
			modelService.save(addressModel);
			addressData.setId(addressModel.getPk().toString());
		}
	}

	private void recalculateCart(final CartModel cartModel)
	{
		final CommerceCheckoutParameter commerceCheckoutParameter = new CommerceCheckoutParameter();
		commerceCheckoutParameter.setCart(cartModel);
		getCommerceCheckoutService().calculateCart(commerceCheckoutParameter);
	}

	@Override
	public boolean isRegisteredUser(final String uid)
	{
		return wileyCustomerAccountService.isRegisteredUser(uid);
	}

	/**
	 * Set cheapest delivery mode if the cart has one or more supported delivery modes
	 *
	 * @param supportedDeliveryModes
	 * 		list of supported delivery modes
	 * @return true if successful
	 */
	@Override
	public boolean setDeliveryModeIfAvailable(final List<? extends DeliveryModeData> supportedDeliveryModes)
	{
		final CartModel cartModel = getCart();
		if (cartModel != null)
		{
			// validate delivery mode if already exists
			final CommerceCheckoutParameter parameter = new CommerceCheckoutParameter();
			parameter.setEnableHooks(true);
			parameter.setCart(cartModel);

			final List<ExternalDeliveryModeModel> deliveryModeModelList =
					wileycomDeliveryService.getSupportedExternalDeliveryModeListForOrder(cartModel);
			parameter.setSupportedDeliveryModes(deliveryModeModelList);

			wileyCommerceCheckoutService.validateDeliveryMode(parameter);

			if (cartModel.getDeliveryMode() == null)
			{
				if (!deliveryModeModelList.isEmpty())
				{
					return setDeliveryMode(wileycomDeliveryService.getDefaultDeliveryMode(deliveryModeModelList));
				}
			}
			return true;
		}
		return false;
	}

	private List<ExternalDeliveryModeModel> convertDeliveryModes(final List<ExternalDeliveryModeData> supportedDeliveryModes)
	{
		final List<ExternalDeliveryModeModel> result = new ArrayList<>(supportedDeliveryModes.size());
		for (final ExternalDeliveryModeData externalDeliveryModeData : supportedDeliveryModes)
		{
			final ExternalDeliveryModeModel externalDeliveryModeModel = modelService.create(ExternalDeliveryModeModel.class);
			externalDeliveryModeReversePopulator.populate(externalDeliveryModeData, externalDeliveryModeModel);
			result.add(externalDeliveryModeModel);
		}
		return result;
	}

	@Override
	public boolean hasSessionCartDigitalProduct()
	{
		final CartModel cartModel = getCart();
		if (cartModel != null)
		{
			return wileyProductEditionFormatService.hasCartDigitalProduct(cartModel);
		}
		return false;
	}


	@Override
	public boolean isPhysicalSessionCart()
	{
		final CartModel cartModel = getCart();
		if (cartModel != null)
		{
			return wileyProductEditionFormatService.isPhysicalCart(cartModel);
		}
		return false;
	}

	@Override
	public List<? extends DeliveryModeData> getSupportedDeliveryModes()
	{
		final List<ExternalDeliveryModeData> result = new ArrayList<>();
		final CartModel cartModel = getCart();
		if (cartModel != null)
		{
			final Collection<ExternalDeliveryModeModel> supportedDeliveryModes =
					wileycomDeliveryService.getSupportedExternalDeliveryModeListForOrder(cartModel);


			for (final ExternalDeliveryModeModel deliveryModeModel : supportedDeliveryModes)
			{
				result.add(externalDeliveryModeConverter.convert(deliveryModeModel));
			}
		}
		return result;
	}

	/**
	 * Set the delivery mode on the cart Checks if the deliveryMode code is supported. If the code is not supported it
	 * does not get set and a false is returned.
	 *
	 * @param deliveryMode
	 * 		the delivery mode
	 * @return true if successful
	 */
	private boolean setDeliveryMode(final ExternalDeliveryModeModel deliveryMode)
	{
		validateParameterNotNullStandardMessage("deliveryMode", deliveryMode);

		final CartModel cartModel = getCart();
		if (cartModel != null)
		{
			final CommerceCheckoutParameter parameter = new CommerceCheckoutParameter();
			parameter.setEnableHooks(true);
			parameter.setCart(cartModel);
			parameter.setDeliveryMode(deliveryMode);
			return wileyCommerceCheckoutService.setDeliveryMode(parameter);
		}
		return false;
	}

	@Override
	public boolean setDeliveryMode(final String deliveryModeCode)
	{
		validateParameterNotNullStandardMessage("deliveryModeCode", deliveryModeCode);
		final CartModel cartModel = getCart();

		if (cartModel != null)
		{
			final List<ExternalDeliveryModeModel> deliveryModes =
					wileycomDeliveryService.getSupportedExternalDeliveryModeListForOrder(cartModel);
			if (CollectionUtils.isNotEmpty(deliveryModes))
			{
				final ExternalDeliveryModeModel selectedDeliveryMode =
						wileycomDeliveryService.findDeliveryModeByExternalCode(deliveryModeCode, deliveryModes)
								.orElseGet(() -> wileycomDeliveryService.getDefaultDeliveryMode(deliveryModes));
				return setDeliveryMode(selectedDeliveryMode);
			}
		}

		return false;
	}

	@Override
	public WileycomCheckoutFacade.ExpressCheckoutResult performExpressCheckout()
	{
		if (isExpressCheckoutEnabledForStore())
		{
			if (hasShippingItems())
			{
				if (!setDefaultDeliveryAddressForCheckout())
				{
					return WileycomCheckoutFacade.ExpressCheckoutResult.ERROR_DELIVERY_ADDRESS;
				}

				if (!setCheapestDeliveryModeForCheckout())
				{
					return WileycomCheckoutFacade.ExpressCheckoutResult.ERROR_CHEAPEST_DELIVERY_MODE;
				}
			}
			//Cart has Pickup Items ONLY
			else if (!setDeliveryModeIfAvailable())
			{
				return WileycomCheckoutFacade.ExpressCheckoutResult.ERROR_DELIVERY_MODE;
			}

			if (!setDefaultPaymentInfoForCheckout())
			{
				return WileycomCheckoutFacade.ExpressCheckoutResult.ERROR_PAYMENT_INFO;
			}

			return WileycomCheckoutFacade.ExpressCheckoutResult.SUCCESS;
		}

		return WileycomCheckoutFacade.ExpressCheckoutResult.ERROR_NOT_AVAILABLE;
	}

	@Override
	public boolean isExpressCheckoutEnabledForStore()
	{
		if (getBaseStoreService().getCurrentBaseStore() != null)
		{
			return BooleanUtils.isTrue(getBaseStoreService().getCurrentBaseStore().getExpressCheckoutEnabled());
		}
		return false;
	}


	private Optional<DeliveryModeModel> maybeDeliveryModeFromCart()
	{
		return Optional.ofNullable(getCart())
				.map(CartModel::getDeliveryMode);
	}

	@Override
	public boolean isDeliveryModeAlreadySetInCart()
	{
		return maybeDeliveryModeFromCart()
				.isPresent();
	}

	@Override
	public boolean isDeliveryModeSetInCartTheSameThan(final String deliveryMode)
	{
		return maybeDeliveryModeFromCart()
				.map(DeliveryModeModel::getCode)
				.filter(deliveryMode::equals)
				.isPresent();
	}

	@Override
	public boolean isDigitalOrder(final String orderCode)
	{
		Assert.hasLength(orderCode, "The [orderCode] is mandatory to perform isDigitalOrder.");
		BaseStoreModel baseStoreModel = getBaseStoreService().getCurrentBaseStore();
		OrderModel orderModel = getCustomerAccountService().getOrderForCode(orderCode, baseStoreModel);

		return wileyProductEditionFormatService.isDigitalCart(orderModel);
	}

	protected DeliveryModeData convert(final DeliveryModeModel deliveryModeModel)
	{
		final DeliveryModeData deliveryModeData = getDeliveryModeConverter().convert(deliveryModeModel);
		if (deliveryModeModel instanceof ExternalDeliveryModeModel)
		{
			deliveryModeData.setExternalCode(((ExternalDeliveryModeModel) deliveryModeModel).getExternalCode());
		}
		return deliveryModeData;
	}

	public void setWileyCommerceCheckoutService(final WileyCommerceCheckoutService wileyCommerceCheckoutService)
	{
		super.setWileyCommerceCheckoutService(wileyCommerceCheckoutService);
		this.wileyCommerceCheckoutService = wileyCommerceCheckoutService;
	}

	@Required
	public void setWileycomDeliveryService(final WileycomDeliveryService wileycomDeliveryService)
	{
		super.setDeliveryService(wileycomDeliveryService);
		this.wileycomDeliveryService = wileycomDeliveryService;
	}

	@Override
	public List<CountryData> getDeliveryCountries()
	{

		final Optional<CountryModel> currentCountry = wileycomI18NService.getCurrentCountry();

		if (currentCountry.isPresent())
		{
			final WorldRegionModel worldRegion = currentCountry.get().getWorldRegion();
			if (worldRegion != null)
			{
				final String currentRegionCode = worldRegion.getCode();

				final List<CountryModel> countriesForOrder = getDeliveryService().getDeliveryCountriesForOrder(null);
				if (!countriesForOrder.isEmpty())
				{
					final List<CountryModel> result = countriesForOrder.stream()
							.filter(country -> (country.getWorldRegion() != null && country.getWorldRegion().getCode().
									equals(currentRegionCode))).collect(Collectors.toList());
					final List<CountryData> countries = Converters.convertAll(result, getCountryConverter());
					Collections.sort(countries, CountryComparator.INSTANCE);
					return countries;
				}
			}
		}
		return Collections.emptyList();
	}

	@Override
	public boolean hasShippingItems()
	{
		CartModel cart = getCart();
		if (cart != null && CollectionUtils.isNotEmpty(cart.getEntries()))
		{
			if (!wileyProductEditionFormatService.isDigitalCart(cart))
			{
				return true;
			}
		}
		return false;
	}

	@Override
	public List<CountryData> getBillingCountries()
	{
		return getDeliveryCountries();
	}
}
