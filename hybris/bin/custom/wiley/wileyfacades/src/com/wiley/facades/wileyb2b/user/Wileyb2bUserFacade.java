package com.wiley.facades.wileyb2b.user;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2bcommercefacades.company.impl.DefaultB2BUserFacade;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.apache.commons.lang.StringUtils;
import org.springframework.util.Assert;

import com.wiley.core.customer.strategy.WileyCustomerRegistrationStrategy;
import com.wiley.core.exceptions.ExternalSystemException;
import com.wiley.core.users.strategy.WileycomChangeUserUidStrategy;
import com.wiley.core.wileyb2b.service.WileyB2BCommerceUserService;
import com.wiley.core.wileycom.customer.service.WileycomCustomerAccountService;
import com.wiley.core.wileycom.customer.service.WileycomCustomerService;
import com.wiley.core.wileycom.exceptions.PasswordUpdateException;
import com.wiley.facades.wileyb2b.customer.populators.Wileyb2bCustomerEditReversePopulator;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


public class Wileyb2bUserFacade extends DefaultB2BUserFacade
{
	@Resource
	private WileycomCustomerService wileycomCustomerService;

	@Resource(name = "wileycomChangeUserUidStrategy")
	private WileycomChangeUserUidStrategy wileycomChangeUserUidStrategy;

	@Resource(name = "wileycomCustomerRegistrationStrategy")
	private WileyCustomerRegistrationStrategy wileyCustomerRegistrationStrategy;

	@Resource
	private WileyB2BCommerceUserService wileyB2BCommerceUserService;

	@Resource
	Wileyb2bCustomerEditReversePopulator wileyb2bCustomerEditReversePopulator;

	@Resource
	private WileycomCustomerAccountService wileycomCustomerAccountService;

	@Resource
	private Converter<CustomerData, B2BCustomerModel> wileyb2bCustomerReverseConverter;

	@Override
	public void updateCustomer(@Nonnull final CustomerData customerData)
	{
		validateParameterNotNullStandardMessage("customerData", customerData);
		Assert.hasText(customerData.getFirstName(), "The field [FirstName] cannot be empty");
		Assert.hasText(customerData.getLastName(), "The field [LastName] cannot be empty");

		final boolean createUser = StringUtils.isEmpty(customerData.getUid());
		final B2BCustomerModel b2BCustomerModelConverted = wileyb2bCustomerReverseConverter.convert(customerData);
		final B2BCustomerModel customerModel = wileyB2BCommerceUserService.createCustomer(b2BCustomerModelConverted, createUser);
		if (StringUtils.isEmpty(customerData.getEmail()))
		{
			customerData.setEmail(customerModel.getUid());
		}
		// todo[known issue] oldPassword can not be provided yet, as new interface requires
		try
		{
			changeCustomerUid(customerData, customerModel, createUser, null);
		}
		catch (DuplicateUidException e)
		{
			throw new RuntimeException(e);
		}

		wileyb2bCustomerEditReversePopulator.populate(customerData, customerModel);
		wileyB2BCommerceUserService.saveCustomer(customerModel, createUser);
	}

	@Override
	public void disableCustomer(final String uid)
	{
		final B2BCustomerModel customerModel = getCustomerModelForUid(uid);
		if (customerModel.getActive())
		{
			try
			{
				super.disableCustomer(uid);
			}
			catch (final ExternalSystemException e)
			{
				super.enableCustomer(uid);
				throw e;
			}
		}
	}

	@Override
	public void enableCustomer(final String uid)
	{
		final B2BCustomerModel customerModel = getCustomerModelForUid(uid);
		if (!customerModel.getActive())
		{
			try
			{
				super.enableCustomer(uid);
			}
			catch (final ExternalSystemException e)
			{
				super.disableCustomer(uid);
				throw e;
			}
		}
	}

	@Override
	public void resetCustomerPassword(final String uid, final String updatedPassword)
	{
		try
		{
			wileycomCustomerAccountService.externalResetCustomerPassword(uid, updatedPassword);
		}
		catch (IllegalArgumentException exception)
		{
			throw new PasswordUpdateException("External reset password process failed!", exception);
		}
	}

	private boolean isUidChanged(final String existingUid, final String newUid)
	{
		return !StringUtils.equals(existingUid, newUid);
	}

	private void changeCustomerUid(@Nonnull final CustomerData customerData, final B2BCustomerModel customerModel,
			final boolean createUser, final String password) throws DuplicateUidException
	{
		if (!createUser && isUidChanged(customerModel.getUid(), customerData.getEmail()))
		{
			// todo (current task) problem
			wileycomChangeUserUidStrategy.changeUid(customerModel, customerData.getEmail(), password);
		}
	}

	private B2BCustomerModel getCustomerModelForUid(final String uid)
	{
		return this.getUserService().getUserForUID(uid, B2BCustomerModel.class);
	}
}
