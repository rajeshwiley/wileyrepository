package com.wiley.facades.wileyb2c.order;

import com.wiley.facades.wileyplus.cart.activation.dto.CartActivationRequestDto;


public interface WileyRegCodeCheckoutFacade
{
	/**
	 * Place an order with the product associated with the registration code.
	 *
	 * @param regCode
	 * 		the registration code entered by the user
	 * @return the order code
	 */
	String placeRegCodeOrder(String regCode);

	/**
	 * Performs expess checkout and making order based on passed registartion code
	 *
	 * @param regCode
	 * 		The registration code entered by user
	 * @param activationRequestDto
	 * @param activationRequestDto
	 * 		purchaseOptionType
	 * @return the created order code or throw RegCodeValidationException
	 */
	String processRegistrationCodeForCourse(String regCode, CartActivationRequestDto activationRequestDto,
			String purchaseOptionType);

}
