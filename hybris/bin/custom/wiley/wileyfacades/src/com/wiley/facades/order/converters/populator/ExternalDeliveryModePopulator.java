package com.wiley.facades.order.converters.populator;

import de.hybris.platform.commercefacades.order.converters.populator.DeliveryModePopulator;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.math.BigDecimal;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import com.wiley.core.model.ExternalDeliveryModeModel;
import com.wiley.core.wileycom.util.date.WileycomDateService;
import com.wiley.facades.order.data.ExternalDeliveryModeData;


/**
 * Created by Raman_Hancharou on 6/15/2016.
 */
public class ExternalDeliveryModePopulator implements Populator<ExternalDeliveryModeModel, ExternalDeliveryModeData>
{
	@Resource
	private DeliveryModePopulator deliveryModePopulator;

	@Resource
	private PriceDataFactory priceDataFactory;

	@Autowired
	private WileycomDateService wileycomDateService;

	/**
	 * Populate the target instance with values from the source instance.
	 *
	 * @param source
	 * 		the source object
	 * @param target
	 * 		the target to fill
	 * @throws ConversionException
	 * 		if an error occurs
	 */
	@Override
	public void populate(@Nonnull final ExternalDeliveryModeModel source,
			@Nonnull final ExternalDeliveryModeData target)
			throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		deliveryModePopulator.populate(source, target);

		target.setExternalCode(source.getExternalCode());
		PriceData priceData = priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(source.getCostValue()),
				source.getCurrency());
		target.setDeliveryCost(priceData);
	}

	public WileycomDateService getWileycomDateService()
	{
		return wileycomDateService;
	}
}
