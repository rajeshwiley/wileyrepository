package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.validation.constraints.NotNull;

import org.springframework.util.Assert;

import com.wiley.core.model.AuthorInfoModel;
import com.wiley.facades.wileyb2c.product.data.PersonJsonLdDto;


/**
 * Person JSON LD populator
 *
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public class Wileyb2cPersonJsonLdPopulator implements Populator<AuthorInfoModel, PersonJsonLdDto>
{
	private static final String TYPE_VALUE = "Person";

	@Override
	public void populate(@NotNull final AuthorInfoModel source,
			@NotNull final PersonJsonLdDto target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setType(TYPE_VALUE);
		target.setName(source.getName());
	}
}
