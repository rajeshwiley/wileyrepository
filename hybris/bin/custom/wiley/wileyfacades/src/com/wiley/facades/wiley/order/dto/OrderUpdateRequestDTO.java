package com.wiley.facades.wiley.order.dto;

import de.hybris.platform.commercefacades.user.data.AddressData;

import java.util.Date;

import com.wiley.core.enums.PaymentModeEnum;


/**
 * The result of the order changes which took part on storefront. In case of order change (count of entries, payment address,
 * payment type, etc) we expect to have full reculculated order back (totalPrice, totalTax, totalDiscount, etc)
 *
 * Author Maksim_kozich
 */
public class OrderUpdateRequestDTO
{
	private String orderCode;

	private AddressData paymentAddress;

	private String taxNumber;

	private Date taxNumberExpirationDate;

	private PaymentModeEnum paymentMode;

	private String sessionId;

	private String userNotes;

	private String purchaseOrderNumber;

	public String getOrderCode()
	{
		return orderCode;
	}

	public void setOrderCode(final String orderCode)
	{
		this.orderCode = orderCode;
	}

	public AddressData getPaymentAddress()
	{
		return paymentAddress;
	}

	public void setPaymentAddress(final AddressData paymentAddress)
	{
		this.paymentAddress = paymentAddress;
	}

	public String getTaxNumber()
	{
		return taxNumber;
	}

	public void setTaxNumber(final String taxNumber)
	{
		this.taxNumber = taxNumber;
	}

	public Date getTaxNumberExpirationDate()
	{
		return taxNumberExpirationDate;
	}

	public void setTaxNumberExpirationDate(final Date taxNumberExpirationDate)
	{
		this.taxNumberExpirationDate = taxNumberExpirationDate;
	}

	public PaymentModeEnum getPaymentMode()
	{
		return paymentMode;
	}

	public void setPaymentMode(final PaymentModeEnum paymentMode)
	{
		this.paymentMode = paymentMode;
	}

	public String getSessionId()
	{
		return sessionId;
	}

	public void setSessionId(final String sessionId)
	{
		this.sessionId = sessionId;
	}

	public String getUserNotes()
	{
		return userNotes;
	}

	public void setUserNotes(final String userNotes)
	{
		this.userNotes = userNotes;
	}

	public String getPurchaseOrderNumber()
	{
		return purchaseOrderNumber;
	}

	public void setPurchaseOrderNumber(final String purchaseOrderNumber)
	{
		this.purchaseOrderNumber = purchaseOrderNumber;
	}
}
