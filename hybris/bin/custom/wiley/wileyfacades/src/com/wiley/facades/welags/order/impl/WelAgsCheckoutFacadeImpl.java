package com.wiley.facades.welags.order.impl;

import com.paypal.hybris.facade.PayPalCheckoutFacade;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.order.data.DeliveryModeData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.externaltax.ExternalTaxesService;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.security.auth.AuthenticationException;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.store.BaseStoreModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.paypal.hybris.facade.data.PayPalPaymentResultData;
import com.paypal.hybris.model.PaypalPaymentInfoModel;
import com.wiley.core.cart.WileyCartFactory;
import com.wiley.core.customer.service.WileyCustomerAccountService;
import com.wiley.core.delivery.WileyDeliveryService;
import com.wiley.core.enums.OrderType;
import com.wiley.core.enums.WileyProductLifecycleEnum;
import com.wiley.core.externaltax.strategies.WileyTaxAddressStrategy;
import com.wiley.core.i18n.WileyCountryService;
import com.wiley.core.order.WileyCheckoutService;
import com.wiley.core.order.WileyCommerceCheckoutService;
import com.wiley.core.product.WileyFreeTrialProductService;
import com.wiley.core.product.WileyFreeTrialVariantProductService;
import com.wiley.core.product.WileyProductEditionFormatService;
import com.wiley.core.product.WileyProductService;
import com.wiley.core.wel.preorder.service.WelPreOrderService;
import com.wiley.core.wel.preorder.strategy.ProductLifecycleStatusStrategy;
import com.wiley.facades.checkout.enums.BreadcrumbItem;
import com.wiley.facades.order.impl.WileyCheckoutFacadeImpl;
import com.wiley.facades.payment.PaymentAuthorizationResultData;
import com.wiley.facades.product.exception.RepeatedFreeTrialOrderException;
import com.wiley.facades.wel.order.WelMultiDimensionalCartFacade;
import com.wiley.facades.welags.order.WelAgsCheckoutFacade;
import com.wiley.facades.welags.pin.PinFacade;
import com.wiley.facades.welags.pin.exception.PinOperationException;

import static com.wiley.facades.payment.PaymentAuthorizationResultData.failure;
import static com.wiley.facades.payment.PaymentAuthorizationResultData.success;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


public class WelAgsCheckoutFacadeImpl extends WileyCheckoutFacadeImpl implements WelAgsCheckoutFacade
{
	private static final Logger LOG = LoggerFactory.getLogger(WelAgsCheckoutFacadeImpl.class);

	private static final String ORDER_NOT_FOUND_FOR_USER_AND_BASE_STORE =
			"Order with guid %s not found for current user in current BaseStore";

	@Autowired
	private ExternalTaxesService externalTaxesService;
	@Resource(name = "welAgsTaxAddressStrategy")
	private WileyTaxAddressStrategy taxAddressStrategy;
	@Autowired
	private UserFacade userFacade;
	@Resource(name = "welAgsCustomerFacade")
	private CustomerFacade customerFacade;
	@Autowired
	protected WileyCheckoutService wileyCheckoutService;

	private WileyDeliveryService wileyDeliveryService;
	@Autowired
	private WileyCountryService wileyCountryService;
	@Autowired
	private WileyCartFactory cartFactory;
	@Autowired
	private WelPreOrderService welPreOrderService;

	private PinFacade pinFacade;
	@Autowired
	private WelMultiDimensionalCartFacade welMultiDimensionalCartFacade;
	@Autowired
	private WileyFreeTrialVariantProductService wileyFreeTrialVariantProductService;
	@Autowired
	private WileyFreeTrialProductService wileyFreeTrialProductService;
	@Autowired
	private CommerceCartService commerceCartService;
	@Autowired
	private WileyProductService wileyProductService;
	@Resource
	private WileyCommerceCheckoutService welAgsCommerceCheckoutService;
	@Resource
	private SessionService sessionService;
	@Resource
	private WileyProductEditionFormatService wileyProductEditionFormatService;
	@Resource(name = "welProductLifecycleStatusStrategy")
	private ProductLifecycleStatusStrategy welProductLifecycleStatusStrategy;
	@Autowired
	private PayPalCheckoutFacade payPalCheckoutFacade;



	@Override
	public boolean isPaymentAuthorized()
	{
		CartModel cart = getCart();
		for (PaymentTransactionModel transaction : cart.getPaymentTransactions())
		{
			for (PaymentTransactionEntryModel entry : transaction.getEntries())
			{
				if (PaymentTransactionType.AUTHORIZATION.equals(entry.getType()) && TransactionStatus.ACCEPTED.name()
						.equals(entry.getTransactionStatus()))
				{
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public boolean authorizePayment(final String securityCode)
	{
		throw new UnsupportedOperationException("Separate Authorize payment invocation is not yet supported");
	}

	@Override
	@Nonnull
	public PaymentAuthorizationResultData authorizePaymentAndProvideResult(final String securityCode)
	{
		PaymentAuthorizationResultData paymentAuthorizationResultData = failure(TransactionStatus.ERROR.name());
		final CartModel cartModel = getCart();
		if (checkIfCurrentUserIsTheCartUser())
		{
			paymentAuthorizationResultData = success();
			final PaymentInfoModel infoModel = cartModel.getPaymentInfo();
			if (infoModel instanceof PaypalPaymentInfoModel)
			{

				final PayPalPaymentResultData payPalPaymentResultData = payPalCheckoutFacade.authorizePaymentAndProvideResult(
						securityCode);
				if (payPalPaymentResultData.isSuccess())
				{
					paymentAuthorizationResultData = success();
				}
				else
				{
					paymentAuthorizationResultData = buildPaymentAuthorizationResultData(payPalPaymentResultData,
							PaymentAuthorizationResultData.PaymentMethod.PAYPAL);
				}
			}
		}
		return paymentAuthorizationResultData;
	}

	@Override
	public void updateCountryInAddress(final String countryIsocode, final boolean isShippingSameAsBilling)
	{
		CartModel cart = getCart();
		cart.setDeliveryMode(null);
		getModelService().save(cart);
		updateCountryAndRegionInAddressForTax(cart, countryIsocode, null, isShippingSameAsBilling);
	}

	@Override
	public void updateTaxInCart(final String countryIsocode, final String regionIsocode, final boolean isShippingSameAsBilling)
	{
		CartModel cart = getCart();

		updateCountryAndRegionInAddressForTax(cart, countryIsocode, regionIsocode, isShippingSameAsBilling);

		getExternalTaxesService().calculateExternalTaxes(cart);
	}

	private void updateCountryAndRegionInAddressForTax(final CartModel cart, final String countryIsocode,
			final String regionIsocode, final boolean isShippingSameAsBilling)
	{
		final AddressModel addressForTax = isShippingSameAsBilling ?
				taxAddressStrategy.resolvePaymentAddress(cart) :
				taxAddressStrategy.resolveDeliveryAddress(cart);
		if (addressForTax != null)
		{
			final CountryModel country = getCommonI18NService().getCountry(countryIsocode);
			addressForTax.setCountry(country);

			if (StringUtils.isNotEmpty(regionIsocode))
			{
				final RegionModel region = getCommonI18NService().getRegion(country, regionIsocode);
				addressForTax.setRegion(region);
			}
			else
			{
				addressForTax.setRegion(null);
			}

			getModelService().save(addressForTax);
		}
	}

	@Override
	/** Updates profile with billing and shipping address and update cart with delivery address according rules:
	 *  - billingAddress must not be null and profile.billing is updated always
	 *  - if 'treatBillingAsShipping' == true then don't update profile.shipping, but profile.getDefaultShipping=billing
	 *  - if 'treatBillingAsShipping' == false and shipping != null, then update profile.shipping and support different values for
	 *          profile.defaultShipping and profile.defaultPayment
	 *  - if shipping != null (could be empty but not null), then update cart.deliveryAddress depending on treatBillingAsShipping
	 */
	public void updateBillingAndShippingAddresses(final AddressData billingAddress, final AddressData shippingAddress,
			final boolean treatBillingAsShipping)
	{
		addOrUpdateAddressInProfile(billingAddress);
		AddressModel defaultBilling = getUserService().getCurrentUser().getDefaultPaymentAddress();
		if (!treatBillingAsShipping && shippingAddress != null)
		{
			if (shippingAddress.getId().equals(billingAddress.getId()))
			{
				shippingAddress.setId(StringUtils.EMPTY);
			}
			addOrUpdateAddressInProfile(shippingAddress);
			rollbackDefaultBillingAddress(defaultBilling);
		}
		AddressData cartDeliveryAddress = shippingAddress != null ? chooseEffectiveDeliveryAddress(treatBillingAsShipping) : null;
		// recalculation of cart should be in the following method
		setDeliveryAddress(cartDeliveryAddress);
	}

	@Override
	public void updateDefaultAddress(final AddressData addressData)
	{
		ServicesUtil.validateParameterNotNull(addressData, "Address Data can't be null");
		if (addressData.isDefaultAddress())
		{
			if (addressData.isBillingAddress() && addressData.isShippingAddress())
			{
				addOrUpdateAddressInProfile(addressData);
			}
			else if (addressData.isBillingAddress())
			{
				AddressModel defaultShipmentAddress = getUserService().getCurrentUser().getDefaultShipmentAddress();
				addOrUpdateAddressInProfile(addressData);
				rollbackDefaultShippingAddress(defaultShipmentAddress);
			}
			else if (addressData.isShippingAddress())
			{
				AddressModel defaultBillingAddress = getUserService().getCurrentUser().getDefaultPaymentAddress();
				addOrUpdateAddressInProfile(addressData);
				rollbackDefaultBillingAddress(defaultBillingAddress);
			}
		}
		else
		{
			LOG.debug("Address data with id {} isn't default, no update", addressData.getId());
		}
	}

	@Override
	public List<DeliveryModeData> getPotentialDeliveryModesForCountry(final String countryIsoCode)
	{
		final AbstractOrderModel abstractOrderModel = getCart();
		final CountryModel country = wileyCountryService.findCountryByCode(countryIsoCode);
		if (country == null)
		{
			return Collections.emptyList();
		}
		List<DeliveryModeModel> deliveryModeModels = new ArrayList<>(
				getDeliveryService().getSupportedDeliveryModes(abstractOrderModel.getStore(),
						abstractOrderModel.getCurrency(), country)
		);
		getDeliveryService().sortDeliveryModes(deliveryModeModels, abstractOrderModel);
		return deliveryModeModels.stream().map(this::convert).collect(Collectors.toList());
	}

	@Override
	public boolean setDeliveryMode(final String deliveryModeCode)
	{
		validateParameterNotNullStandardMessage("deliveryModeCode", deliveryModeCode);

		final CartModel cartModel = getCart();
		if (cartModel != null)
		{
			final DeliveryModeModel deliveryModeModel = getDeliveryService().getDeliveryModeForCode(deliveryModeCode);
			if (deliveryModeModel != null)
			{
				final CommerceCheckoutParameter parameter = new CommerceCheckoutParameter();
				parameter.setEnableHooks(true);
				parameter.setCart(cartModel);
				parameter.setDeliveryMode(deliveryModeModel);
				return getCommerceCheckoutService().setDeliveryMode(parameter);
			}
		}

		return false;
	}

	@Override
	public void setDeliveryModeIfNotAvailable()
	{
		if (getCartService().hasSessionCart())
		{
			final AddressModel deliveryAddress = getUserService().getCurrentUser().getDefaultShipmentAddress();

			if (deliveryAddress != null && deliveryAddress.getCountry() != null)
			{
				setDeliveryModeIfNotAvailable(deliveryAddress.getCountry().getIsocode());
			}
		}
	}

	@Override
	public void setDeliveryModeIfNotAvailable(final String countryIsoCode)
	{
		final CartModel cartModel = getCart();

		if (cartModel != null && cartModel.getDeliveryMode() == null)
		{
			final List<? extends DeliveryModeData> availableDeliveryModes = getPotentialDeliveryModesForCountry(
					countryIsoCode);
			if (!availableDeliveryModes.isEmpty())
			{
				setDeliveryMode(availableDeliveryModes.get(0).getCode());
			}
		}
	}

	@Override
	public boolean isDigitalSessionCart()
	{
		CartModel cart = getCart();
		validateParameterNotNullStandardMessage("session cart", cart);
		return wileyProductEditionFormatService.isDigitalCart(cart);
	}

	@Override
	public boolean isPreOrderCart()
	{
		CartModel cart = getCart();
		if (cart != null)
		{
			return welPreOrderService.isPreOrder(cart);
		}
		return false;
	}

	@Override
	public boolean isPreOrderOrder(final String orderCode)
	{
		validateParameterNotNull("orderCode must not be null", orderCode);
		AbstractOrderModel abstractOrderModel = getOrderByCode(orderCode);
		return welPreOrderService.isPreOrder(abstractOrderModel);

	}


	@Override
	public void setPaymentAddress(final AddressModel addressModel)
	{
		final CommerceCheckoutParameter commerceCartParameter = new CommerceCheckoutParameter();
		commerceCartParameter.setCart(getCart());
		commerceCartParameter.setAddress(addressModel);
		welAgsCommerceCheckoutService.setPaymentAddress(commerceCartParameter);
	}

	@Override
	public boolean isDigitalCart(@Nonnull final String cartCode)
	{
		validateParameterNotNullStandardMessage("cart code", cartCode);
		return wileyProductEditionFormatService.isDigitalCart(getCartByCode(cartCode));
	}

	@Override
	public boolean isNonZeroPriceCart()
	{
		CartModel cart = getCart();
		return wileyCheckoutService.isNonZeroOrder(cart);
	}

	@Override
	public boolean isZeroOrder(final String orderCode)
	{
		AbstractOrderModel orderModel = getOrderByCode(orderCode);
		return !wileyCheckoutService.isNonZeroOrder(orderModel);
	}

	@Override
	public void subscribeCurrentCustomerToUpdates()
	{
		if (getCartService().hasSessionCart())
		{
			final CartModel sessionCart = getCartService().getSessionCart();
			sessionCart.setSubscribeToUpdates(true);
			getModelService().save(sessionCart);
		}
	}

	@Override
	public void unsubscribeCurrentCustomerFromUpdates()
	{
		if (getCartService().hasSessionCart())
		{
			final CartModel sessionCart = getCartService().getSessionCart();
			sessionCart.setSubscribeToUpdates(false);
			getModelService().save(sessionCart);
		}
	}

	@Override
	public void subscribeToGMACInfo(final boolean subscribe)
	{
		if (getCartService().hasSessionCart())
		{
			final CartModel sessionCart = getCartService().getSessionCart();
			sessionCart.setSubscribeGmacInfo(subscribe);
			getModelService().save(sessionCart);
		}
	}

	@Override
	@Transactional(rollbackFor = { AuthenticationException.class, InvalidCartException.class, PinOperationException.class })
	public String placePinOrder(final String pinCode, final OrderType orderType)
			throws IllegalAccessException, InvalidCartException, PinOperationException
	{
		String orderCode;
		if (userFacade.isAnonymousUser())
		{
			throw new IllegalAccessException("Anonymous user is not allowed to activate pin.");
		}
		final CartModel cartModel = cartFactory.createCartWithType(orderType);
		if (cartModel != null)
		{
			pinFacade.applyPin(cartModel.getCode(), pinCode);
			beforePlaceOrder(cartModel);
			OrderModel orderModel = placeOrder(cartModel);
			if (orderModel != null)
			{
				getModelService().refresh(orderModel);
				orderCode = orderModel.getCode();
				pinFacade.activatePin(orderModel.getCode(), pinCode);
			}
			else
			{
				throw new InvalidCartException("Could not place order.");
			}
			getModelService().remove(cartModel);
		}
		else
		{
			throw new InvalidCartException("Could not create cart for PIN order.");
		}
		return orderCode;
	}

	/**
	 * Place a Free Trial order.
	 *
	 * @param freeTrialCode
	 * 		the Free Trial code
	 * @return placed order code
	 * @throws IllegalAccessException
	 * 		is thrown when Anonymous user is used to place order
	 * @throws InvalidCartException
	 * 		is thrown by underlying cart validator
	 * @throws UnknownIdentifierException
	 * 		is thrown when Free Trial product with passed freeTrialCode not found
	 * @throws CommerceCartModificationException
	 * 		is thrown by addToCartCommerceStrategy
	 * @throws RepeatedFreeTrialOrderException
	 * 		is thrown when current user has already have order with the same free trial product
	 */
	@Override
	@Transactional(rollbackFor = { IllegalAccessException.class, InvalidCartException.class, UnknownIdentifierException.class,
			CommerceCartModificationException.class, RepeatedFreeTrialOrderException.class })
	public OrderModel placeFreeTrialOrder(final String freeTrialCode, boolean subscribeToUpdates, final OrderType type)
			throws UnknownIdentifierException, IllegalAccessException, InvalidCartException, CommerceCartModificationException,
			RepeatedFreeTrialOrderException
	{
		if (userFacade.isAnonymousUser())
		{
			throw new IllegalAccessException("Anonymous user is not allowed to place free trial order.");
		}
		if (wileyCheckoutService.isFreeTrialWasOrderedByCurrentUser(freeTrialCode))
		{
			throw new RepeatedFreeTrialOrderException(String.format("User has already have order with %s code ", freeTrialCode));
		}

		ProductModel productModel;
		try
		{
			productModel = wileyFreeTrialProductService.getFreeTrialProductByCode(freeTrialCode);
		}
		catch (UnknownIdentifierException e)
		{
			productModel = wileyFreeTrialVariantProductService.getFreeTrialVariantProductByCode(freeTrialCode);
		}

		final CartModel cartModel = cartFactory.createCartWithType(type);
		OrderModel orderModel;
		if (cartModel != null) //NOSONAR
		{
			cartModel.setSubscribeToUpdates(subscribeToUpdates);
			addFreeTrialVariantProductToCart(cartModel, productModel);
			beforePlaceOrder(cartModel);
			orderModel = placeOrder(cartModel);
			if (orderModel != null)
			{
				getModelService().refresh(orderModel);
			}
			else
			{
				throw new InvalidCartException("Could not place order.");
			}
			getModelService().remove(cartModel);
		}
		else
		{
			throw new InvalidCartException("Could not create cart for Free Trail order.");
		}
		return orderModel;
	}

	protected void addFreeTrialVariantProductToCart(final CartModel cartModel,
			final ProductModel productModel) throws CommerceCartModificationException
	{
		final OrderEntryData orderEntry = new OrderEntryData();
		orderEntry.setQuantity(1L);
		orderEntry.setProduct(new ProductData());
		orderEntry.getProduct().setCode(productModel.getCode());
		welMultiDimensionalCartFacade.addOrderEntryList(cartModel.getCode(), Arrays.asList(orderEntry));
	}

	@Override
	public boolean isDeliveryAddressSetToCart()
	{
		if (getCartService().hasSessionCart())
		{
			final CartModel sessionCart = getCartService().getSessionCart();
			return sessionCart.getDeliveryAddress() != null;
		}
		return false;
	}

	@Override
	public boolean isStudentFlow()
	{
		CartModel sessionCart = getCart();
		return wileyCheckoutService.isStudentOrder(sessionCart);
	}

	@Override
	public boolean isStudentFlow(final String cartCode)
	{
		validateParameterNotNull(cartCode, "Cart code must not be null");
		return wileyCheckoutService.isStudentOrder(getCartByCode(cartCode));
	}

	@Override
	public boolean isCartHasStudentVerification()
	{
		CartModel sessionCart = getCart();
		validateParameterNotNull(sessionCart, "Cart must not be null");
		return wileyCheckoutService.isCartHasStudentVerification(sessionCart);
	}

	@Override
	public boolean isPaypalCheckout()
	{
		return wileyCheckoutService.isPaypalCheckout();
	}

	@Override
	public List<String> getBreadcrumbLabels(@Nonnull final String orderCode)
	{
		validateParameterNotNullStandardMessage("cart code", orderCode);
		return getBreadcrumbLabels(getOrderByCode(orderCode));
	}

	/**
	 * @return breadcrumb labels for current checkout flow
	 */
	@Override
	public List<String> getBreadcrumbLabels()
	{
		return getBreadcrumbLabels(getCartService().getSessionCart());
	}

	@Override
	protected WileyDeliveryService getDeliveryService()
	{
		return wileyDeliveryService;
	}

	public void setWileyDeliveryService(final WileyDeliveryService wileyDeliveryService)
	{
		this.wileyDeliveryService = wileyDeliveryService;
	}

	private AddressData chooseEffectiveDeliveryAddress(boolean treatBillingAsShipping)
	{
		if (treatBillingAsShipping)
		{
			return customerFacade.getCurrentCustomer().getDefaultBillingAddress();
		}
		else
		{
			return customerFacade.getCurrentCustomer().getDefaultShippingAddress();
		}
	}

	private void addOrUpdateAddressInProfile(final AddressData addressData)
	{
		if (StringUtils.isNotBlank(addressData.getId())
				&& userFacade.getAddressForCode(addressData.getId()) != null)
		{
			userFacade.editAddress(addressData);
		}
		else
		{
			userFacade.addAddress(addressData);
		}
	}

	/**
	 * We have to do that rollback because Hybris saves together defaultPayment(billing) and defaultShipment(delivery) addresses
	 * with the same address-item.
	 * However, for WEL-checkout we need to have different billing/shipping addresses to prepopulate them separately on
	 * checkout step page.
	 * So that after shipping saved, we have to set default billing(payment) lost on previous step.
	 *
	 * @param defaultBillingAddress
	 * 		Previous billing address
	 */
	private void rollbackDefaultBillingAddress(final AddressModel defaultBillingAddress)
	{
		final UserModel currentUser = getUserService().getCurrentUser();
		currentUser.setDefaultPaymentAddress(defaultBillingAddress);
		getModelService().save(currentUser);
	}

	private void rollbackDefaultShippingAddress(final AddressModel defaultShipmentAddress)
	{
		final UserModel currentUser = getUserService().getCurrentUser();
		currentUser.setDefaultShipmentAddress(defaultShipmentAddress);
		getModelService().save(currentUser);
	}

	private AbstractOrderModel getCartByCode(final String cartCode)
	{
		return commerceCartService.getCartForCodeAndUser(cartCode, getUserService().getCurrentUser());
	}

	private AbstractOrderModel getOrderByCode(@Nonnull final String code)
	{
		validateParameterNotNullStandardMessage("orderCode", code);
		final BaseStoreModel baseStoreModel = getBaseStoreService().getCurrentBaseStore();
		OrderModel orderModel;
		if (getCheckoutCustomerStrategy().isAnonymousCheckout())
		{
			orderModel = getCustomerAccountService().getOrderDetailsForGUID(code, baseStoreModel);
		}
		else
		{
			try
			{
				orderModel = getCustomerAccountService().getOrderForCode((CustomerModel) getUserService().getCurrentUser(), code,
						baseStoreModel);
			}
			catch (final ModelNotFoundException e)
			{
				throw new UnknownIdentifierException(String.format(ORDER_NOT_FOUND_FOR_USER_AND_BASE_STORE, code));
			}
		}
		if (orderModel == null)
		{
			throw new UnknownIdentifierException(String.format(ORDER_NOT_FOUND_FOR_USER_AND_BASE_STORE, code));
		}
		return orderModel;
	}

	protected List<String> getBreadcrumbLabels(final AbstractOrderModel abstractOrderModel)
	{
		List<BreadcrumbItem> breadcrumbLabels = new ArrayList<>();
		breadcrumbLabels.add(BreadcrumbItem.LOGIN);
		breadcrumbLabels.add(wileyProductEditionFormatService.isDigitalCart(abstractOrderModel) ?
				BreadcrumbItem.BILLING :
				BreadcrumbItem.BILLING_AND_SHIPPING);
		if (wileyCheckoutService.isStudentOrder(abstractOrderModel))
		{
			breadcrumbLabels.add(BreadcrumbItem.STUDENT_VERIFICATION);
		}
		if (wileyCheckoutService.isNonZeroOrder(abstractOrderModel))
		{
			breadcrumbLabels.add(BreadcrumbItem.PAYMENT);
		}
		breadcrumbLabels.add(BreadcrumbItem.ORDER_CONFIRMATION);
		return breadcrumbLabels.stream().map(BreadcrumbItem::getLabelKey).collect(Collectors.toList());
	}

	/**
	 * Checks if transferred product or session cart's product are pre-order products. If exists at least one pre-order product,
	 * session cart is cleared and 'true' is returned.
	 *
	 * @param productCode
	 */
	@Override
	public boolean clearCartForPreOrderProduct(final String productCode)
	{
		validateParameterNotNull("productCode must not be null", productCode);

		WileyProductLifecycleEnum currentProductStatus = welProductLifecycleStatusStrategy.getLifecycleStatusForProduct(
				productCode);

		if ((currentProductStatus == WileyProductLifecycleEnum.PRE_ORDER && getCartFacade().hasEntries()) || isPreOrderCart())
		{
			welMultiDimensionalCartFacade.clearSessionCart();
			return true;
		}
		return false;
	}

	public ExternalTaxesService getExternalTaxesService()
	{
		return externalTaxesService;
	}

	public void setExternalTaxesService(final ExternalTaxesService externalTaxesService)
	{
		this.externalTaxesService = externalTaxesService;
	}

	protected WileyCustomerAccountService getWileyCustomerAccountService()
	{
		return (WileyCustomerAccountService) super.getCustomerAccountService();
	}

	public PinFacade getPinFacade()
	{
		return pinFacade;
	}

	public void setPinFacade(final PinFacade pinFacade)
	{
		this.pinFacade = pinFacade;
	}

	public void setWileyProductEditionFormatService(final WileyProductEditionFormatService wileyProductEditionFormatService)
	{
		this.wileyProductEditionFormatService = wileyProductEditionFormatService;
	}
}
