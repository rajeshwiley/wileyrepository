package com.wiley.facades.wel.order.impl;

import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.order.impl.DefaultCartFacade;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.voucher.data.VoucherData;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.order.WileyMultiDimensionalCommerceCartService;
import com.wiley.facades.voucher.WileyCouponFacade;
import com.wiley.facades.wel.order.WelMultiDimensionalCartFacade;


/**
 * Default implementation of {@link WelMultiDimensionalCartFacade}.
 * This facade used for operations with Multi dimensional products.
 *
 * @author Aliaksei_Zlobich
 */
public class WelMultiDimensionalCartFacadeImpl extends DefaultCartFacade implements WelMultiDimensionalCartFacade
{
	protected static final Logger LOG = Logger.getLogger(WelMultiDimensionalCartFacadeImpl.class);

	private static final Integer MINIMUM_SINGLE_SKU_ADD_CART = 0;

	private WileyMultiDimensionalCommerceCartService wileyMultiDimensionalCommerceCartService;

	@Resource
	private WileyCouponFacade welCouponFacade;

	@Override
	public List<CartModificationData> addOrderEntryList(final List<OrderEntryData> cartEntries) throws
			CommerceCartModificationException
	{
		return addOrderEntryList(getCartService().getSessionCart().getCode(), cartEntries);
	}

	@Override
	public boolean hasStudentCoupon()
	{
		boolean hasStudentCoupon = false;
		List<VoucherData> vouchers = welCouponFacade.getVouchersForCart();
		if (vouchers != null && !vouchers.isEmpty())
		{
			hasStudentCoupon = vouchers.stream().anyMatch(
					voucher -> WileyCoreConstants.STUDENT_COUPON_CODE.equals(voucher.getCode()));
		}
		return hasStudentCoupon;
	}

	@Override
	public List<CartModificationData> addOrderEntryList(final String cartCode, final List<OrderEntryData> cartEntries)
			throws CommerceCartModificationException
	{
		List<CommerceCartParameter> cartParameters = new ArrayList<>(cartEntries.size());

		final CartModel cartModel = getCartForCode(cartCode);

		for (final OrderEntryData cartEntry : cartEntries)
		{
			if (isValidEntry(cartEntry))
			{
				final ProductModel product = getProductService().getProductForCode(cartEntry.getProduct().getCode());
				final CommerceCartParameter parameter = new CommerceCartParameter();
				parameter.setEnableHooks(true);
				parameter.setProduct(product);
				parameter.setQuantity(cartEntry.getQuantity());
				parameter.setUnit(product.getUnit());
				boolean isExistingEntry = cartModel.getEntries()
						.stream()
						.anyMatch(e -> product.equals(e.getProduct()));
				parameter.setCreateNewEntry(!isExistingEntry);
				parameter.setPartnerId(cartEntry.getPartnerId());
				parameter.setApplyStudentDiscount(cartEntry.isApplyStudentDiscount());
				if (cartEntry.isReplaceEntry() && cartEntry.getEntryNumber() != null)
				{
					parameter.setReplaceEntry(true);
					parameter.setEntryNumber(cartEntry.getEntryNumber());
				}
				cartParameters.add(parameter);
			}
		}

		final List<CommerceCartModification> modifications = getWileyMultiDimensionalCommerceCartService()
				.addToCart(cartModel, cartParameters); // throws CommerceCartModificationException
		if (LOG.isDebugEnabled())
		{
			logAddingToCart(cartModel, modifications);
		}
		return Converters.convertAll(modifications, getCartModificationConverter());
	}

	@Override
	public boolean changeInCartProductType(final String productSku, final int entryNumber, final long quantity)
			throws CommerceCartModificationException
	{
		String partnerId = getSessionCart().getPartnerId();
		boolean applyStudentDiscount = hasStudentCoupon();
		CartModificationData cartModificationData = updateCartEntry(entryNumber, 0);
		boolean succeed = cartModificationData.getQuantity() == 0;
		if (succeed)
		{
			final OrderEntryData orderEntry = new OrderEntryData();
			orderEntry.setQuantity(quantity);
			orderEntry.setProduct(new ProductData());
			orderEntry.getProduct().setCode(productSku);
			orderEntry.setPartnerId(partnerId);
			orderEntry.setApplyStudentDiscount(applyStudentDiscount);
			orderEntry.setReplaceEntry(true);
			orderEntry.setEntryNumber(entryNumber);

			final List<OrderEntryData> orderEntries = Collections.singletonList(orderEntry);

			addOrderEntryList(orderEntries);
		}
		return succeed;
	}

	private void logAddingToCart(final CartModel cartModel, final List<CommerceCartModification> modifications)
	{
		StringBuilder sb = new StringBuilder();
		final UserModel user = cartModel.getUser();
		sb.append("Customer ").append(user != null ? user.getUid() : "")
				.append(" added to cart ").append(cartModel.getCode());
		for (CommerceCartModification modification : modifications)
		{
			final AbstractOrderEntryModel entry = modification.getEntry();
			final ProductModel product = entry != null ? entry.getProduct() : null;
			sb.append(" Product Code ").append(product != null ? product.getCode() : "");
			sb.append(" Product Quantity ").append(modification.getQuantityAdded());
		}
		LOG.debug(sb.toString());
	}

	protected boolean isValidEntry(final OrderEntryData cartEntry)
	{
		return (cartEntry.getProduct() != null && cartEntry.getProduct().getCode() != null) && cartEntry.getQuantity() != null
				&& cartEntry.getQuantity() >= MINIMUM_SINGLE_SKU_ADD_CART;
	}

	private CartModel getCartForCode(final String cartCode)
	{
		return wileyMultiDimensionalCommerceCartService.getCartForCodeAndUser(cartCode, getUserService().getCurrentUser());
	}

	@Override
	public void clearSessionCart()
	{
		final CartModel sessionCart = getCartService().getSessionCart();

		final CommerceCartParameter parameter = new CommerceCartParameter();
		parameter.setEnableHooks(true);
		parameter.setCart(sessionCart);

		getCommerceCartService().removeAllEntries(parameter);
	}

	public WileyMultiDimensionalCommerceCartService getWileyMultiDimensionalCommerceCartService()
	{
		return wileyMultiDimensionalCommerceCartService;
	}

	public void setWileyMultiDimensionalCommerceCartService(
			final WileyMultiDimensionalCommerceCartService wileyMultiDimensionalCommerceCartService)
	{
		this.wileyMultiDimensionalCommerceCartService = wileyMultiDimensionalCommerceCartService;
	}

	public void searchAndRemoveBrokenCarts()
	{
		getWileyMultiDimensionalCommerceCartService().searchAndRemoveBrokenCarts(getUserService().getCurrentUser());
	}
}
