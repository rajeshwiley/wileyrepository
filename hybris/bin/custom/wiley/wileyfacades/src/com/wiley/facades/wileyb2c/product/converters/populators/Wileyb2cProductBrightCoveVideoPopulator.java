package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.platform.commercefacades.product.converters.populator.AbstractProductPopulator;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.facades.populators.bright.cover.WileyBrightCoverHelper;


/**
 * @author Dzmitryi_Halahayeu
 */
public class Wileyb2cProductBrightCoveVideoPopulator extends AbstractProductPopulator<ProductModel, ProductData>
{
	private WileyBrightCoverHelper wileyBrightCoverHelper;

	@Override
	public void populate(final ProductModel productModel, final ProductData productData) throws ConversionException
	{
		ServicesUtil.validateParameterNotNullStandardMessage("productModel", productModel);
		ServicesUtil.validateParameterNotNullStandardMessage("productData", productData);

		final String brightcoveVideoId = safeToString(getProductAttribute(productModel, ProductModel.BRIGHTCOVEVIDEOID));
		if (StringUtils.isNoneEmpty(brightcoveVideoId))
		{
			productData.setBrightcoveVideo(wileyBrightCoverHelper.createBrightCoveVideo(brightcoveVideoId));
		}
	}

	@Required
	public void setWileyBrightCoverHelper(final WileyBrightCoverHelper wileyBrightCoverHelper)
	{
		this.wileyBrightCoverHelper = wileyBrightCoverHelper;
	}
}
