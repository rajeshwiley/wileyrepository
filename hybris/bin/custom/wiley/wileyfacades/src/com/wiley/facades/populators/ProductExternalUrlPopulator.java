package com.wiley.facades.populators;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.Map;

import org.springframework.beans.factory.annotation.Required;


public class ProductExternalUrlPopulator implements Populator<ProductModel, ProductData>
{
	private Map<String, String> catalogAwareLinks;

	@Override
	public void populate(final ProductModel productModel, final ProductData productData) throws ConversionException
	{
		String catalogId = productModel.getCatalogVersion().getCatalog().getId();
		if (catalogAwareLinks.containsKey(catalogId))
		{
			productData.setExternalUrl(catalogAwareLinks.get(catalogId));
		}
	}

	@Required
	public void setCatalogAwareLinks(final Map<String, String> catalogAwareLinks)
	{
		this.catalogAwareLinks = catalogAwareLinks;
	}

}
