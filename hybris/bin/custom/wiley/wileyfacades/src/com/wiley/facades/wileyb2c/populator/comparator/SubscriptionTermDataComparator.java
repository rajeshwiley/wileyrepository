package com.wiley.facades.wileyb2c.populator.comparator;

import de.hybris.platform.subscriptionfacades.data.SubscriptionTermData;

import java.math.BigDecimal;
import java.util.Comparator;

import javax.annotation.Nonnull;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;
import static java.lang.String.format;


/**
 * Author Herman_Chukhrai (EPAM)
 */
public final class SubscriptionTermDataComparator implements Comparator<SubscriptionTermData>
{
	@Override
	public int compare(@Nonnull final SubscriptionTermData term1, @Nonnull final SubscriptionTermData term2)
	{
		validateParameterNotNullStandardMessage("term1", term1);
		validateParameterNotNullStandardMessage("term2", term2);

		final Integer orderNumber1 = getOrderNumber(term1);
		final Integer orderNumber2 = getOrderNumber(term2);
		final BigDecimal price1 = getPriceValue(term1);
		final BigDecimal price2 = getPriceValue(term2);

		final int compare = orderNumber1.compareTo(orderNumber2);
		return compare == 0 ? price2.compareTo(price1) : compare;
	}

	private static BigDecimal getPriceValue(final SubscriptionTermData subscriptionTermData)
	{
		try
		{
			return subscriptionTermData.getPrice().getValue();
		}
		catch (Exception e)
		{
			throw new IllegalArgumentException(
					format("Price value wasn't found for subscription term with id [%s]", subscriptionTermData.getId()), e);
		}
	}

	private static Integer getOrderNumber(final SubscriptionTermData subscriptionTermData)
	{
		try
		{
			return subscriptionTermData.getBillingPlan().getBillingTime().getOrderNumber();
		}
		catch (Exception e)
		{
			throw new IllegalArgumentException(
					format("Order can't be defined for subscription term with id [%s]", subscriptionTermData.getId()), e);
		}
	}
}
