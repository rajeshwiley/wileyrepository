package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import com.wiley.core.model.WileyPurchaseOptionProductModel;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;



public class Wileyb2cProductSeriesPopulator implements Populator<ProductModel, ProductData>
{

	@Resource(name = "categoryUrlConverter")
	private Converter<CategoryModel, CategoryData> categoryUrlConverter;

	@Override
	public void populate(final ProductModel productModel, final ProductData productData) throws ConversionException
	{
		validateParameterNotNullStandardMessage("productModel", productModel);
		validateParameterNotNullStandardMessage("productData", productData);

		Collection<CategoryModel> productSeries = getSeriesForProduct(productModel);
		productData.setSeries(getSeries(productSeries));
	}

	private Collection<CategoryModel> getSeriesForProduct(final ProductModel productModel)
	{
		if (productModel instanceof WileyPurchaseOptionProductModel)
		{
			ProductModel baseProduct = ((WileyPurchaseOptionProductModel) productModel).getBaseProduct();
			return baseProduct.getSeries();
		}
		return productModel.getSeries();

	}

	private List<CategoryData> getSeries(final Collection<CategoryModel> categoryModelCollection)
	{
		return categoryModelCollection.stream().map(category -> categoryUrlConverter.convert(category)).collect(
				Collectors.toList());
	}

}