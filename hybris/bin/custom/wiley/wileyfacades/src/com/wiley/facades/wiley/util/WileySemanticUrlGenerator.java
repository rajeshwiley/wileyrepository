package com.wiley.facades.wiley.util;

import java.text.Normalizer;
import java.util.Locale;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;


public class WileySemanticUrlGenerator
{
	private static final Pattern NON_LATIN = Pattern.compile("[^\\w-]");
	private static final Pattern WHITESPACE = Pattern.compile("[\\s]");
	private static final Pattern WITHOUT_EDGE_DASHES = Pattern.compile("(^-|-$)");

	/**
	 * Generate semantic URL for input string.
	 *
	 * @param input
	 * 		input string for generation
	 * @return generated semantic url string or null if input string is empty
	 */
	public String generateFor(final String input)
	{
		if (StringUtils.isEmpty(input))
		{
			return StringUtils.EMPTY;
		}

		final String inputWithoutWhitespaces = WHITESPACE.matcher(input).replaceAll("-");
		final String normalized = Normalizer.normalize(inputWithoutWhitespaces, Normalizer.Form.NFD);
		final String semanticUrl = NON_LATIN.matcher(normalized).replaceAll(StringUtils.EMPTY);

		return WITHOUT_EDGE_DASHES.matcher(semanticUrl).replaceAll(StringUtils.EMPTY).toLowerCase(Locale.ENGLISH);
	}
}
