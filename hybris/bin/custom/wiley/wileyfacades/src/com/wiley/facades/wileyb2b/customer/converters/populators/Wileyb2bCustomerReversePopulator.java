package com.wiley.facades.wileyb2b.customer.converters.populators;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2bcommercefacades.company.converters.populators.B2BCustomerReversePopulator;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.Collections;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;

import com.google.common.base.Preconditions;
import com.wiley.facades.wileycom.customer.converters.populators.WileycomCustomerReversePopulator;


public class Wileyb2bCustomerReversePopulator extends B2BCustomerReversePopulator
{
	@Resource
	private WileycomCustomerReversePopulator wileycomCustomerReversePopulator;

	@Override
	public void populate(final CustomerData source, final B2BCustomerModel target) throws ConversionException
	{
		super.populate(source, target);
		wileycomCustomerReversePopulator.populate(source, target);
		populateUid(source, target);
	}

	@Override
	protected void populateUid(final CustomerData source, final B2BCustomerModel target)
	{
		String email = source.getEmail();
		Preconditions.checkArgument(StringUtils.isNotBlank(email));
		target.setOriginalUid(email);
		target.setUid(email.toLowerCase());
	}

	@Override
	protected void populateDefaultUnit(final CustomerData source, final B2BCustomerModel target)
	{
		if (target.getGroups() == null) {
			target.setGroups(Collections.emptySet());
		}
		super.populateDefaultUnit(source, target);
	}
}
