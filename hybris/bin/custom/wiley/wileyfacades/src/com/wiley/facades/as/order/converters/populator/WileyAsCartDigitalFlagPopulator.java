package com.wiley.facades.as.order.converters.populator;

import com.wiley.core.product.WileyProductEditionFormatService;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.springframework.beans.factory.annotation.Autowired;

public class WileyAsCartDigitalFlagPopulator implements Populator<CartModel, CartData>
{

	@Autowired
	private WileyProductEditionFormatService wileyProductEditionFormatService;
	@Override
	public void populate(final CartModel cartModel, final CartData cartData) throws ConversionException
	{
		cartData.setDigitalCart(wileyProductEditionFormatService.isDigitalCart(cartModel));
	}
}
