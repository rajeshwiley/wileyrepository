package com.wiley.facades.wileyb2c.order.converters.populator;

import com.wiley.core.model.WileyProductSummaryModel;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.apache.commons.lang3.StringUtils;


/**
 * Created by Anton_Lukyanau on 8/24/2016.
 */
public class Wileyb2cCourseInfoPopulator implements Populator<AbstractOrderEntryModel, OrderEntryData>
{

	@Override
	public void populate(final AbstractOrderEntryModel source, final OrderEntryData target)
			throws ConversionException
	{
		WileyProductSummaryModel productSummary = source.getProductSummary();
		if (productSummary != null)
		{
			if (StringUtils.isNotEmpty(productSummary.getName()))
			{
				target.getProduct().setName(productSummary.getName());
			}
			if (StringUtils.isNotEmpty(productSummary.getAuthors()))
			{
				target.getProduct().setAuthors(productSummary.getAuthors());
			}
			if (StringUtils.isNotEmpty(productSummary.getPictureUrl()))
			{
				target.getProduct().setPurchaseOptionImageUrl(productSummary.getPictureUrl());
			}
			if (StringUtils.isNotEmpty(productSummary.getSummary()))
			{
				target.getProduct().setPurchaseOptionType(productSummary.getSummary());
			}
		}
	}
}
