package com.wiley.facades.payment.impl;

import com.paypal.hybris.data.SetExpressCheckoutRequestData;
import com.paypal.hybris.data.SetExpressCheckoutResultData;
import com.paypal.hybris.facade.impl.PayPalPaymentFacade;
import com.wiley.core.enums.PaymentModeEnum;
import com.wiley.facades.payment.WileyPayPalPaymentFacade;
import de.hybris.platform.commercefacades.order.CheckoutFacade;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.PaymentModeService;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Resource;


/**
 * Created by Raman_Hancharou on 8/19/2016.
 */
public class WileyPayPalPaymentFacadeImpl extends PayPalPaymentFacade implements WileyPayPalPaymentFacade
{
	@Resource
	private CheckoutFacade checkoutFacade;

	@Resource
	private UserFacade userFacade;

	@Resource
	private CartService cartService;
	@Resource
	private ModelService modelService;
	@Resource
	private PaymentModeService paymentModeService;

	@Override
	public SetExpressCheckoutResultData preparePaypalPayment(final SetExpressCheckoutRequestData requestData)
	{
		CartModel sessionCart = cartService.getSessionCart();
		sessionCart.setPaymentMode(paymentModeService.getPaymentModeForCode(PaymentModeEnum.PAYPAL.getCode()));
		modelService.save(sessionCart);

		return super.preparePaypalPayment(requestData);
	}

	@Override
	public void removeDeliveryAddressFromCart()
	{
		final AddressData previousSelectedAddress = checkoutFacade.getCheckoutCart().getDeliveryAddress();
		if (previousSelectedAddress != null && !previousSelectedAddress.isVisibleInAddressBook())
		{ // temporary address should be removed
			userFacade.removeAddress(previousSelectedAddress);
		}
		checkoutFacade.removeDeliveryAddress();
	}

	public void setCheckoutFacade(final CheckoutFacade checkoutFacade)
	{
		this.checkoutFacade = checkoutFacade;
	}
}
