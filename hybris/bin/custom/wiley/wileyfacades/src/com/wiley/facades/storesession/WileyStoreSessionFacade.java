package com.wiley.facades.storesession;

import de.hybris.platform.commercefacades.storesession.StoreSessionFacade;
import de.hybris.platform.core.model.order.CartModel;


/**
 * Wiley store session facade extends default store session facade by additional logic during currency change
 */
public interface WileyStoreSessionFacade extends StoreSessionFacade
{

	/**
	 * Set currenct currency to session and optionally calculate cart.
	 *
	 * @param isocode
	 * 		Currency isocode
	 * @param recalculateCart
	 * 		If true then cart calculation is executed at the end, otherwise {@link CartModel#CALCULATED} is set to false
	 */
	void setCurrentCurrency(String isocode, boolean recalculateCart);
}
