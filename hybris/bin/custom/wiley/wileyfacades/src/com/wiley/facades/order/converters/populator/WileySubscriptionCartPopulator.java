/**
 *
 */
package com.wiley.facades.order.converters.populator;

import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.subscriptionfacades.data.OrderPriceData;
import de.hybris.platform.subscriptionfacades.order.converters.populator.SubscriptionCartPopulator;

import java.util.List;

import javax.annotation.Nonnull;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


public class WileySubscriptionCartPopulator extends SubscriptionCartPopulator
{
	private WileySubscriptionOrderPopulatorUtil wileySubscriptionUtil;

	@Override
	public List<OrderPriceData> buildOrderPrices(final AbstractOrderModel source)
	{
		return wileySubscriptionUtil.fillOrderPriceDataByCustomFields(source, super.buildOrderPrices(source));
	}

	@Override
	public void populate(@Nonnull final CartModel source, @Nonnull final CartData target) throws ConversionException
	{
		validateParameterNotNullStandardMessage("source", source);
		validateParameterNotNullStandardMessage("target", target);

		super.populate(source, target);

		addSubtotalWithoutDiscount(source, target);
	}

	private void addSubtotalWithoutDiscount(@Nonnull final CartModel source, @Nonnull final CartData target)
	{
		target.setSubTotalWithoutDiscount(createPrice(source, source.getSubTotalWithoutDiscount()));
	}

	/**
	 * @return the wileySubscriptionUtil
	 */
	public WileySubscriptionOrderPopulatorUtil getWileySubscriptionUtil()
	{
		return wileySubscriptionUtil;
	}

	/**
	 * @param wileySubscriptionUtil
	 * 		the wileySubscriptionUtil to set
	 */
	public void setWileySubscriptionUtil(final WileySubscriptionOrderPopulatorUtil wileySubscriptionUtil)
	{
		this.wileySubscriptionUtil = wileySubscriptionUtil;
	}

}