package com.wiley.facades.populators.bright.cover;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.product.data.ImageDataType;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.site.BaseSiteService;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.facades.product.data.BrightCoveVideoData;


/**
 * @author Dzmitryi_Halahayeu
 */
public class WileyBrightCoverHelper
{
	public static final String PLAYER_ID = "integration.brightcove.<SITE>.playerid";
	public static final String PLAYER_KEY = "integration.brightcove.<SITE>.playerkey";
	public static final String ACCOUNT_ID = "integration.brightcove.<SITE>.accountid";

	private static final String VIDEO_FORMAT = "brightCoveVideo";

	private BaseSiteService baseSiteService;
	private ConfigurationService configurationService;

	public BrightCoveVideoData createBrightCoveVideoDataForGallery(final String videoId, final int galleryIndex)
	{
		final BrightCoveVideoData videoData = createBrightCoveVideo(videoId);
		videoData.setGalleryIndex(galleryIndex);
		videoData.setFormat(VIDEO_FORMAT);
		videoData.setImageType(ImageDataType.GALLERY);
		return videoData;
	}

	public BrightCoveVideoData createBrightCoveVideo(final String videoId)
	{
		final BrightCoveVideoData videoData = new BrightCoveVideoData();
		videoData.setVideoId(videoId);
		videoData.setPlayerId(getPropertyForCurrentSite(PLAYER_ID));
		videoData.setPlayerKey(getPropertyForCurrentSite(PLAYER_KEY));
		videoData.setAccountId(getPropertyForCurrentSite(ACCOUNT_ID));
		return videoData;
	}

	public String getPropertyForCurrentSite(final String propertyKeyTemplate)
	{
		final BaseSiteModel currentBaseSite = getBaseSiteService().getCurrentBaseSite();
		return getPropertyForSite(propertyKeyTemplate, currentBaseSite.getUid());
	}

	private String getPropertyForSite(final String propertyKeyTemplate, final String siteId)
	{
		final String propertyKey = StringUtils.replace(propertyKeyTemplate, "<SITE>", siteId);
		return configurationService.getConfiguration().getString(propertyKey);
	}



	public BaseSiteService getBaseSiteService()
	{
		return baseSiteService;
	}

	@Required
	public void setBaseSiteService(final BaseSiteService baseSiteService)
	{
		this.baseSiteService = baseSiteService;
	}

	public ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

}
