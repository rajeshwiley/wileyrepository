package com.wiley.facades.populators;

import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.i18n.I18NService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.context.MessageSource;
import org.springframework.util.Assert;

import com.wiley.core.enums.WileyProductLifecycleEnum;
import com.wiley.core.wel.preorder.service.WelPreOrderService;
import com.wiley.core.wel.preorder.strategy.ProductLifecycleStatusStrategy;

import static org.apache.commons.lang3.StringUtils.EMPTY;


public class WelPreOrderProductNodePopulator implements Populator<ProductModel, ProductData>
{
	private WelPreOrderService welPreOrderService;
	private VariantProductUrlPopulator variantProductUrlPopulator;
	private WelProductLifeCycleStatusPopulator productLifeCycleStatusPopulator;
	private ProductLifecycleStatusStrategy welProductLifecycleStatusStrategy;
	private MessageSource messageSource;
	private I18NService i18NService;

	@Override
	public void populate(final ProductModel productModel, final ProductData productData) throws ConversionException
	{
		Assert.notNull(productModel, "productModel must not be null");
		Assert.notNull(productData, "productData must not be null");

		final WileyProductLifecycleEnum lifecycleStatusOfProduct =
				welProductLifecycleStatusStrategy.getLifecycleStatusForProduct(productModel);
		final boolean isProductActive = lifecycleStatusOfProduct == WileyProductLifecycleEnum.ACTIVE;
		final boolean isProductPreOrder = lifecycleStatusOfProduct == WileyProductLifecycleEnum.PRE_ORDER;
		boolean isProductWithPreOrderReference = isProductWithPreOrderReference(productModel, isProductActive, isProductPreOrder);

		if (!isProductWithPreOrderReference)
		{
			return;
		}
		else if (isProductActive)
		{
			createProductNodeForPreOrderProductFromActiveProduct(productModel, productData);
		}
		else if (isProductPreOrder)
		{
			createProductNodeForActiveProductFromPreOrderProduct(productModel, productData);
		}
		createPreOrderProductNode(productModel, productData);
		sortPreOrderProductNodes(productData);
	}

	private boolean isProductWithPreOrderReference(final ProductModel productModel,
			final boolean isProductActive, final boolean isProductPreOrder)
	{
		boolean isProductWithPreOrderReference = false;
		if (isProductActive && CollectionUtils.isNotEmpty(productModel.getProductReferences()))
		{
			isProductWithPreOrderReference = productModel.getProductReferences().stream().anyMatch(
					productReferenceData -> productReferenceData.getReferenceType() == ProductReferenceTypeEnum.NEXT_VERSION);
		}
		else if (isProductPreOrder)
		{
			isProductWithPreOrderReference = getWelPreOrderService().getActiveProductForPreOrderProduct(
					productModel).isPresent();
		}
		return isProductWithPreOrderReference;
	}

	private void createProductNodeForPreOrderProductFromActiveProduct(final ProductModel activeProductModel,
			final ProductData activeProductData)
	{
		final Collection<ProductReferenceModel> activeProductReferences = activeProductModel.getProductReferences();
		activeProductReferences.
				stream().
				filter(productReference -> productReference.getReferenceType() == ProductReferenceTypeEnum.NEXT_VERSION).
				forEach(productReference -> {
					final ProductModel preOrderProductModel = productReference.getTarget();
					createPreOrderProductNode(preOrderProductModel, activeProductData);
				});
	}

	private void createProductNodeForActiveProductFromPreOrderProduct(final ProductModel preOrderProductModel,
			final ProductData preOrderProductData)
	{
		final Optional<ProductModel> activeProductModel = getWelPreOrderService()
				.getActiveProductForPreOrderProduct(preOrderProductModel);
		if (activeProductModel.isPresent()) {
			createPreOrderProductNode(activeProductModel.get(), preOrderProductData);
		}
	}

	private void createPreOrderProductNode(final ProductModel productModel, final ProductData productData)
	{
		final ProductData productNode = new ProductData();
		productNode.setCode(productModel.getCode());
		productNode.setDescription(productModel.getDescription());
		productLifeCycleStatusPopulator.populate(productModel, productNode);
		productNode.setPreOrderText(getPreOrderText(productModel));
		variantProductUrlPopulator.populate(productModel, productNode);
		if (productData.getPreOrderNodes() == null)
		{
			productData.setPreOrderNodes(new ArrayList<>(Collections.singletonList(productNode)));
		}
		else
		{
			productData.getPreOrderNodes().add(productNode);
		}
	}

	private String getPreOrderText(final ProductModel productModel)
	{
		return productModel.getLifecycleStatus() == WileyProductLifecycleEnum.PRE_ORDER ?
				getProductReferenceDescription(productModel) :
				EMPTY;
	}

	private String getProductReferenceDescription(final ProductModel preOrderProductModel)
	{
		final Optional<ProductModel> activeProductModel = getWelPreOrderService()
				.getActiveProductForPreOrderProduct(preOrderProductModel);
		if (!activeProductModel.isPresent()) {
			return getDefaultPreOrderText();
		}

		final Collection<ProductReferenceModel> productReferences = activeProductModel.get().getProductReferences();
		final Optional<ProductReferenceModel> optionalProductReference = productReferences.stream().filter(
				productReferenceModel -> productReferenceModel.getReferenceType() == ProductReferenceTypeEnum.NEXT_VERSION)
				.findFirst();
		return optionalProductReference.isPresent() && StringUtils.isNotEmpty(optionalProductReference.get().getDescription()) ?
				optionalProductReference.get().getDescription() :
				getDefaultPreOrderText();
	}

	private String getDefaultPreOrderText()
	{
		return messageSource.getMessage("pdp.radio.preorder.product", null, "pdp.radio.preorder.product",
				i18NService.getCurrentLocale());
	}

	private void sortPreOrderProductNodes(final ProductData currentProductData)
	{
		List<ProductData> sortedNodes = currentProductData.getPreOrderNodes().stream().sorted((node1, node2) -> {
			return node1.getLifecycleStatus() == WileyProductLifecycleEnum.ACTIVE ? -1 : 1;
		}).collect(Collectors.toList());
		currentProductData.setPreOrderNodes(sortedNodes);
	}

	public WelPreOrderService getWelPreOrderService()
	{
		return welPreOrderService;
	}

	@Required
	public void setWelPreOrderService(final WelPreOrderService welPreOrderService)
	{
		this.welPreOrderService = welPreOrderService;
	}

	@Required
	public void setVariantProductUrlPopulator(final VariantProductUrlPopulator variantProductUrlPopulator)
	{
		this.variantProductUrlPopulator = variantProductUrlPopulator;
	}

	@Required
	public void setWelProductLifecycleStatusStrategy(
			final ProductLifecycleStatusStrategy welProductLifecycleStatusStrategy)
	{
		this.welProductLifecycleStatusStrategy = welProductLifecycleStatusStrategy;
	}

	@Required
	public void setProductLifeCycleStatusPopulator(
			final WelProductLifeCycleStatusPopulator productLifeCycleStatusPopulator)
	{
		this.productLifeCycleStatusPopulator = productLifeCycleStatusPopulator;
  }
  
	public void setMessageSource(final MessageSource messageSource)
	{
		this.messageSource = messageSource;
	}

	@Required
	public void setI18NService(final I18NService i18NService)
	{
		this.i18NService = i18NService;
	}
}
