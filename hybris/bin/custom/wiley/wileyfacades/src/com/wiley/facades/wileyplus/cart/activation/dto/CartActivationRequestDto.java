package com.wiley.facades.wileyplus.cart.activation.dto;

import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;


public class CartActivationRequestDto
{

	@JsonIgnore
	private String ssoToken = null;
	@JsonIgnore
	private String country = null;
	@JsonIgnore
	private String language = null;
	@JsonIgnore
	private String currency = null;
	@JsonIgnore
	private Boolean registrationRequired = null;
	@JsonIgnore
	private String userId = null;
	@JsonIgnore
	private String ficeCode = null;
	@JsonIgnore
	private PurchaseOptionsDto purchaseOptionsDto = null;
	@JsonIgnore
	private WileyPlusCourseDto course = null;
	@JsonIgnore
	private String classSectionId = null;
	@JsonIgnore
	private WebLinkDto returnURL = null;
	@JsonIgnore
	private WebLinkDto continueURL = null;
	@JsonIgnore
	private String additionalInfo = null;

	/**
	 * SSO token for login on hybris side
	 **/
	public CartActivationRequestDto ssoToken(final String ssoToken)
	{
		this.ssoToken = ssoToken;
		return this;
	}

	@Size(max = 255)
	@JsonGetter("st")
	public String getSsoToken()
	{
		return ssoToken;
	}

	@JsonSetter("ssoToken")
	public void setSsoToken(final String ssoToken)
	{
		this.ssoToken = ssoToken;
	}

	@JsonSetter("st")
	public void setSt(final String st)
	{
		this.ssoToken = st;
	}

	/**
	 * Two-letter ISO 3166-1 alpha-2 code that specifies a country.
	 * See https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2 for details.
	 **/
	public CartActivationRequestDto country(final String country)
	{
		this.country = country;
		return this;
	}

	@NotNull
	@Pattern(regexp = "^[A-Z]{2}$")
	@JsonGetter("cn")
	public String getCountry()
	{
		return country;
	}

	@JsonSetter("country")
	public void setCountry(final String country)
	{
		this.country = country;
	}

	@JsonSetter("cn")
	public void setCn(final String cn)
	{
		this.country = cn;
	}

	/**
	 * Two-letter ISO 639 alpha-2 code that specifies a language. See https://en.wikipedia.org/wiki/ISO_639-1 for details.
	 **/
	public CartActivationRequestDto language(final String language)
	{
		this.language = language;
		return this;
	}

	@Pattern(regexp = "^[a-z]{2}$")
	@JsonGetter("ln")
	public String getLanguage()
	{
		return language;
	}

	@JsonSetter("language")
	public void setLanguage(final String language)
	{
		this.language = language;
	}

	@JsonSetter("ln")
	public void setLn(final String ln)
	{
		this.language = ln;
	}

	/**
	 * Three-letter ISO 4217 code that specifies a currency. See https://en.wikipedia.org/wiki/ISO_4217 for details.
	 **/
	public CartActivationRequestDto currency(final String currency)
	{
		this.currency = currency;
		return this;
	}

	@NotNull
	@Pattern(regexp = "^[A-Z]{3}$")
	@JsonGetter("crn")
	public String getCurrency()
	{
		return currency;
	}

	@JsonSetter("currency")
	public void setCurrency(final String currency)
	{
		this.currency = currency;
	}

	@JsonSetter("crn")
	public void setCrn(final String crn)
	{
		this.currency = crn;
	}

	/**
	 * a flag that customer's registration is required.
	 * Should be true for Standalone flow and it is supposed to be false for others
	 **/
	public CartActivationRequestDto registrationRequired(final Boolean registrationRequired)
	{
		this.registrationRequired = registrationRequired;
		return this;
	}


	@JsonGetter("rr")
	public Boolean getRegistrationRequired()
	{
		return registrationRequired;
	}

	@JsonSetter("registrationRequired")
	public void setRegistrationRequired(final Boolean registrationRequired)
	{
		this.registrationRequired = registrationRequired;
	}

	@JsonSetter("rr")
	public void setRr(final Boolean rr)
	{
		this.registrationRequired = rr;
	}

	/**
	 * for Standalone send actual email id, for LMS/blackboard this is a face email address
	 **/
	public CartActivationRequestDto userId(final String userId)
	{
		this.userId = userId;
		return this;
	}
	
	@Size(max = 255)
	@JsonGetter("uid")
	public String getUserId()
	{
		return userId;
	}

	@JsonSetter("userId")
	public void setUserId(final String userId)
	{
		this.userId = userId;
	}

	@JsonSetter("uid")
	public void setUid(final String uid)
	{
		this.userId = uid;
	}

	/**
	 * FICE code of school/college/university
	 **/
	public CartActivationRequestDto ficeCode(final String ficeCode)
	{
		this.ficeCode = ficeCode;
		return this;
	}

	@NotNull
	@Size(max = 255)
	@JsonGetter("fc")
	public String getFiceCode()
	{
		return ficeCode;
	}

	@JsonSetter("ficeCode")
	public void setFiceCode(final String ficeCode)
	{
		this.ficeCode = ficeCode;
	}

	@JsonSetter("fc")
	public void setFc(final String fc)
	{
		this.ficeCode = fc;
	}

	/**
	 **/
	public CartActivationRequestDto purchaseOptions(final PurchaseOptionsDto purchaseOptionsDto)
	{
		this.purchaseOptionsDto = purchaseOptionsDto;
		return this;
	}

	@Valid
	@JsonGetter("po")
	public PurchaseOptionsDto getPurchaseOptionsDto()
	{
		return purchaseOptionsDto;
	}

	@JsonSetter("purchaseOptionsDto")
	public void setPurchaseOptionsDto(final PurchaseOptionsDto purchaseOptionsDto)
	{
		this.purchaseOptionsDto = purchaseOptionsDto;
	}

	@JsonSetter("po")
	public void setPo(final PurchaseOptionsDto po)
	{
		this.purchaseOptionsDto = po;
	}

	/**
	 **/
	public CartActivationRequestDto course(final WileyPlusCourseDto course)
	{
		this.course = course;
		return this;
	}

	@NotNull
	@Valid
	@JsonGetter("crs")
	public WileyPlusCourseDto getCourse()
	{
		return course;
	}

	@JsonSetter("course")
	public void setCourse(final WileyPlusCourseDto course)
	{
		this.course = course;
	}

	@JsonSetter("crs")
	public void setCrs(final WileyPlusCourseDto crs)
	{
		this.course = crs;
	}

	/**
	 **/
	public CartActivationRequestDto classSectionId(final String classSectionId)
	{
		this.classSectionId = classSectionId;
		return this;
	}
	
	@NotNull
	@Size(max = 255)
	@JsonGetter("csi")
	public String getClassSectionId()
	{
		return classSectionId;
	}

	@JsonSetter("classSectionId")
	public void setClassSectionId(final String classSectionId)
	{
		this.classSectionId = classSectionId;
	}

	@JsonSetter("csi")
	public void setCsi(final String csi)
	{
		this.classSectionId = csi;
	}

	/**
	 **/
	public CartActivationRequestDto returnURL(final WebLinkDto returnURL)
	{
		this.returnURL = returnURL;
		return this;
	}

	@Valid
	@JsonGetter("ru")
	public WebLinkDto getReturnURL()
	{
		return returnURL;
	}

	@JsonSetter("returnURL")
	public void setReturnURL(final WebLinkDto returnURL)
	{
		this.returnURL = returnURL;
	}

	@JsonSetter("ru")
	public void setRu(final WebLinkDto ru)
	{
		this.returnURL = ru;
	}

	/**
	 **/
	public CartActivationRequestDto continueURL(final WebLinkDto continueURL)
	{
		this.continueURL = continueURL;
		return this;
	}

	@Valid
	@JsonGetter("cnu")
	public WebLinkDto getContinueURL()
	{
		return continueURL;
	}

	@JsonSetter("continueURL")
	public void setContinueURL(final WebLinkDto continueURL)
	{
		this.continueURL = continueURL;
	}

	@JsonSetter("cnu")
	public void setCnu(final WebLinkDto cnu)
	{
		this.continueURL = cnu;
	}

	/**
	 * List of extra information in key-value format. It is supposed to pass additional field for unified cart interface.
	 * Currently we know that here will be passed: LmsId, ProductType, ToolProviderVersion
	 **/
	public CartActivationRequestDto additionalInfo(final String additionalInfo)
	{
		this.additionalInfo = additionalInfo;
		return this;
	}

	@NotEmpty
	@Valid
	@JsonGetter("ei")
	public String getAdditionalInfo()
	{
		return additionalInfo;
	}

	@JsonSetter("extraInfo")
	public void setAdditionalInfo(final String additionalInfo)
	{
		this.additionalInfo = additionalInfo;
	}

	@JsonSetter("ei")
	public void setEi(final String ei)
	{
		this.additionalInfo = ei;
	}


	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		CartActivationRequestDto cartActivationRequestDto = (CartActivationRequestDto) o;
		return Objects.equals(ssoToken, cartActivationRequestDto.ssoToken)
				&& Objects.equals(country, cartActivationRequestDto.country)
				&& Objects.equals(language, cartActivationRequestDto.language)
				&& Objects.equals(currency, cartActivationRequestDto.currency)
				&& Objects.equals(registrationRequired, cartActivationRequestDto.registrationRequired)
				&& Objects.equals(userId, cartActivationRequestDto.userId)
				&& Objects.equals(ficeCode, cartActivationRequestDto.ficeCode)
				&& Objects.equals(purchaseOptionsDto, cartActivationRequestDto.purchaseOptionsDto)
				&& Objects.equals(course, cartActivationRequestDto.course)
				&& Objects.equals(classSectionId, cartActivationRequestDto.classSectionId)
				&& Objects.equals(returnURL, cartActivationRequestDto.returnURL)
				&& Objects.equals(continueURL, cartActivationRequestDto.continueURL)
				&& Objects.equals(additionalInfo, cartActivationRequestDto.additionalInfo);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(ssoToken, country, language, currency, registrationRequired, userId, ficeCode, purchaseOptionsDto,
				course, classSectionId, returnURL, continueURL, additionalInfo);
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("class CartActivationRequest {\n");

		sb.append("    ssoToken: ").append(toIndentedString(ssoToken)).append("\n");
		sb.append("    country: ").append(toIndentedString(country)).append("\n");
		sb.append("    language: ").append(toIndentedString(language)).append("\n");
		sb.append("    currency: ").append(toIndentedString(currency)).append("\n");
		sb.append("    registrationRequired: ").append(toIndentedString(registrationRequired)).append("\n");
		sb.append("    userId: ").append(toIndentedString(userId)).append("\n");
		sb.append("    ficeCode: ").append(toIndentedString(ficeCode)).append("\n");
		sb.append("    purchaseOptions: ").append(toIndentedString(purchaseOptionsDto)).append("\n");
		sb.append("    course: ").append(toIndentedString(course)).append("\n");
		sb.append("    classSectionId: ").append(toIndentedString(classSectionId)).append("\n");
		sb.append("    returnURL: ").append(toIndentedString(returnURL)).append("\n");
		sb.append("    continueURL: ").append(toIndentedString(continueURL)).append("\n");
		sb.append("    additionalInfo: ").append(toIndentedString(additionalInfo)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(final Object o)
	{
		if (o == null)
		{
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}

