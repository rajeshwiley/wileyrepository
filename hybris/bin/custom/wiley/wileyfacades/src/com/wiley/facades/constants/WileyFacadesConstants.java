/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.facades.constants;

import de.hybris.platform.commercefacades.product.ProductOption;

import java.util.Arrays;
import java.util.List;


/**
 * Global class for all WileyFacades constants.
 */
@SuppressWarnings("PMD")
public final class WileyFacadesConstants extends GeneratedWileyFacadesConstants
{
	/**
	 * The constant EXTENSIONNAME.
	 */
	public static final String EXTENSIONNAME = "wileyfacades";

	public static final Integer PDP_INCLUDE_LIST_TWO_COLUMN_MIN_SIZE = 4;

	public static final String GENERIC_PARTNER_PAGE_URL = "preferred";

	public static final String USER_GROUP_LOCAL_VIEW_PARAM_NAME = "userGroup";

	public static final String IGNORE_VISIBILITY_LOCAL_VIEW_PARAM_NAME = "ignoreVisibility";

	public static final String PURHASE_OPRIONS_LOCAL_VIEW_PARAM_NAME = "purchaseOptionList";

	public static final String CURRENT_PAYMENT_OPTION_NAME = "current";

	public static final List<ProductOption> PRODUCT_REFERENCES_OPTIONS = Arrays.asList(ProductOption.BASIC, ProductOption.PRICE,
			ProductOption.AUTHOR_INFOS, ProductOption.IMAGES, ProductOption.GALLERY, ProductOption.VARIANT_MATRIX_URL,
			ProductOption.URL, ProductOption.VARIANT_PRODUCT_NAME);

	public static final String CAROUSEL_MAX_NUMBER_OF_PRODUCTS_MESSAGE_CODE =
			"continue.shopping.popup.product.carousel.max.number.of.products";
	public static final Integer CAROUSEL_MAX_NUMBER_OF_PRODUCTS_DEFAULT_VALUE = 5;

	public static final String ORDER_NOT_FOUND_FOR_USER_AND_BASE_STORE =
			"Order with guid %s not found for current user in current BaseStore";

	private WileyFacadesConstants()
	{
		// Prevent instantiation
	}
}
