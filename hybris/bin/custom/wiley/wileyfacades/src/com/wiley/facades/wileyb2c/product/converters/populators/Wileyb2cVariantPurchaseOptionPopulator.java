package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ImageDataType;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;

import com.wiley.core.enums.WileyWebLinkTypeEnum;
import com.wiley.core.model.WileyPurchaseOptionProductModel;
import com.wiley.core.model.WileyWebLinkModel;
import com.wiley.core.wileyb2c.classification.Wileyb2cClassificationAttributes;
import com.wiley.core.wileyb2c.classification.Wileyb2cClassificationService;
import com.wiley.facades.wileyb2c.product.converters.populators.helper.Wileyb2cAuthorPopulatorHelper;


public class Wileyb2cVariantPurchaseOptionPopulator implements Populator<VariantProductModel, VariantOptionData>
{
	private static final Integer DEFAULT_DATA_GALLERY_INDEX = 0;
	private static final String NORMAL = "normal";
	private static final String PRODUCT = "product";

	@Resource
	private CommerceCommonI18NService commerceCommonI18NService;

	@Resource
	private Wileyb2cClassificationService wileyb2cClassificationService;

	@Resource
	private Wileyb2cClassificationService wileyb2cSolrIndexClassificationService;

	@Resource
	private Wileyb2cAuthorPopulatorHelper wileyb2cAuthorPopulatorHelper;

	@Resource
	private Converter<MediaModel, ImageData> imageConverter;

	@Override
	public void populate(final VariantProductModel variantProductModel, final VariantOptionData variantOptionData)
			throws ConversionException
	{

		if (variantProductModel instanceof WileyPurchaseOptionProductModel)
		{
			variantOptionData.setCode(variantProductModel.getCode());
			final Locale currentLocale = commerceCommonI18NService.getCurrentLocale();

			final ClassificationClassModel classificationClass = wileyb2cClassificationService.resolveClassificationClass(
					variantProductModel);
			variantOptionData.setPurchaseOptionName(classificationClass.getName(currentLocale));
			variantOptionData.setPurchaseOptionDescription(
					classificationClass.getDescription(currentLocale));
			variantOptionData.setPurchaseOptionSequenceNumber(classificationClass.getSequenceNumber());
			variantOptionData.setPublicationDate(wileyb2cSolrIndexClassificationService
					.resolveAttributeValue(variantProductModel, Wileyb2cClassificationAttributes.PUBLICATION_DATE));
			variantOptionData.setCountable(variantProductModel.getCountable());
			variantOptionData.setAuthorsWithoutRoles(wileyb2cAuthorPopulatorHelper.getAuthorsWithoutRoles(variantProductModel));
			isHidePrice(variantProductModel, variantOptionData);
			populateImages(variantProductModel, variantOptionData);
		}
	}

	private void isHidePrice(final VariantProductModel variantProductModel, final VariantOptionData variantOptionData)
	{
		Collection<WileyWebLinkModel> listExternalStores = variantProductModel.getExternalStores();
		boolean isHidePrice = listExternalStores.stream().anyMatch(store ->
				WileyWebLinkTypeEnum.REQUEST_QUOTE.equals(store.getType()));
		variantOptionData.setHidePrice(isHidePrice);
	}

	private void populateImages(final VariantProductModel source, final VariantOptionData target)
	{
		final ProductModel baseProduct = source.getBaseProduct();
		final List<MediaContainerModel> galleryImage = baseProduct.getGalleryImages();
		final MediaContainerModel externalImage = baseProduct.getExternalImage();
		final List<ImageData> images = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(galleryImage))
		{
			for (MediaContainerModel mediaContainerModel : galleryImage)
			{
				addImages(mediaContainerModel.getMedias(), images);
			}
		}
		else if (externalImage != null)
		{
			addImages(externalImage.getMedias(), images);
		}
		target.setImages(images);
	}

	private void addImages(final Collection<MediaModel> medias, final List<ImageData> images)
	{
		for (MediaModel media : medias)
		{
			images.add(convertMedia(media));
		}
	}

	private ImageData convertMedia(final MediaModel picture)
	{
		String format = picture.getMediaFormat().getQualifier();
		if (NORMAL.equals(format))
		{
			format = PRODUCT;
		}

		final ImageData imageData = imageConverter.convert(picture);
		imageData.setGalleryIndex(DEFAULT_DATA_GALLERY_INDEX);
		imageData.setImageType(ImageDataType.GALLERY);
		imageData.setFormat(format);
		return imageData;
	}
}
