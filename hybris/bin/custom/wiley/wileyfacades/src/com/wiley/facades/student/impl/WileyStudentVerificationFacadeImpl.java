package com.wiley.facades.student.impl;


import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.List;
import java.util.Optional;

import javax.annotation.Nonnull;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.i18n.WileyCountryService;
import com.wiley.core.model.UniversityModel;
import com.wiley.core.university.services.UniversityService;
import com.wiley.facades.student.WileyStudentVerificationFacade;
import com.wiley.facades.student.data.StudentVerificationData;
import com.wiley.facades.university.UniversityFacade;
import com.wiley.facades.university.data.UniversityData;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


/**
 * Default implementation of {@link WileyStudentVerificationFacade}
 */
public class WileyStudentVerificationFacadeImpl implements WileyStudentVerificationFacade
{
	@Autowired
	private CartService cartService;

	@Autowired
	private ModelService modelService;

	@Autowired
	private UniversityService universityService;

	@Autowired
	private UniversityFacade universityFacade;
	
	@Autowired
	private WileyCountryService countryService;
	

	public void saveStudentVerification(final StudentVerificationData studentVerificationData)
	{

		String universityName = null;
		String universityState = null;
		String universityCountry = null;
		if (WileyCoreConstants.USA_COUNTRY_ISO_CODE.equals(studentVerificationData.getCountryIso()))
		{
			final String universityCode = studentVerificationData.getUniversityCode();
			if (WileyCoreConstants.UNIVERSITY_OTHER.equals(universityCode))
			{
				universityName = WileyCoreConstants.UNIVERSITY_OTHER;
			}
			else
			{
				if (universityCode != null)
				{
					Optional<UniversityModel> universityModelOptional = universityService.getUniversityByCode(universityCode);
					if (universityModelOptional.isPresent())
					{
						universityName = universityModelOptional.get().getName();
						universityState = universityModelOptional.get().getRegion().getName();
						universityCountry = universityModelOptional.get().getCountry().getNumeric();
					}
				}
			}
		}
		else
		{
			universityName = studentVerificationData.getUniversityTextField();
			universityCountry = countryService.findCountryByCode(studentVerificationData.getCountryIso()).getNumeric();
		}
		Assert.isTrue(cartService.hasSessionCart(), "Session cart should no be empty");
		Assert.isTrue(StringUtils.isNotBlank(universityName), "University name should not be blank");
		final CartModel sessionCart = cartService.getSessionCart();
		sessionCart.setUniversity(universityName);
		sessionCart.setUniversityState(universityState);
		sessionCart.setUniversityCountry(universityCountry);
		modelService.save(sessionCart);
	}

	/**
	 * Retrieves all universities for persisted country and region isocodes.
	 * Adds Other item to returned list
	 *
	 * @param countryIso
	 * @param regionIso
	 * @return - list of populated universities
	 */
	@Override
	@Nonnull
	public List<UniversityData> getUniversitiesByCountryAndRegionIso(@Nonnull final String countryIso, @Nonnull final String
			regionIso)
	{
		validateParameterNotNullStandardMessage("countryIso", countryIso);
		validateParameterNotNullStandardMessage("regionIso", regionIso);
		List<UniversityData> universityDataList = universityFacade.getUniversitiesByCountryAndRegionIso(countryIso, regionIso);
		universityDataList.add(createUniversityWithOtherName());
		return universityDataList;
	}

	private UniversityData createUniversityWithOtherName()
	{
		final UniversityData universityData = new UniversityData();
		universityData.setCode(WileyCoreConstants.UNIVERSITY_OTHER);
		universityData.setName(WileyCoreConstants.UNIVERSITY_OTHER);
		return universityData;
	}
}

