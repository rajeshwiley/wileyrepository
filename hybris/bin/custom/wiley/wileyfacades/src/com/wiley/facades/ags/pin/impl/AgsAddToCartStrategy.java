package com.wiley.facades.ags.pin.impl;

import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.order.CartModel;

import javax.annotation.Resource;

import com.wiley.core.model.PinModel;
import com.wiley.facades.ags.order.AgsSubscriptionCartFacade;
import com.wiley.facades.welags.pin.AddToCartStrategy;
import com.wiley.facades.welags.pin.exception.PinOperationException;

import static com.wiley.facades.welags.pin.exception.PinOperationExceptionReason.INVALID_CART;

/**
 * Implementation of {@link AddToCartStrategy} for AGS.
 *
 * @author Robert_Farkas
 * @author Gergo_Fodi
 */
public class AgsAddToCartStrategy implements AddToCartStrategy
{
	@Resource(name = "agsSubscriptionCartFacade")
	private AgsSubscriptionCartFacade agsCartFacade;

	@Override
	public void addProductToCart(final CartModel cartModel, final PinModel pinModel) throws PinOperationException
	{
		try
		{
			final long qty = 1L;
			agsCartFacade.addToCart(cartModel.getCode(), pinModel.getProductCode(), qty);
		}
		catch (CommerceCartModificationException e)
		{
			throw new PinOperationException(INVALID_CART, e);
		}
	}
}
