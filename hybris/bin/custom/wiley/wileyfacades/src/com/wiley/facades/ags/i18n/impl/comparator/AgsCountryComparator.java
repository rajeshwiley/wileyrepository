package com.wiley.facades.ags.i18n.impl.comparator;

import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commerceservices.util.AbstractComparator;

import org.apache.commons.lang3.StringUtils;


public class AgsCountryComparator extends AbstractComparator<CountryData>
{
    private String defaultCountryIsocode;

	@Override
	protected int compareInstances(final CountryData country1, final CountryData country2)
	{
		if (StringUtils.equalsIgnoreCase(defaultCountryIsocode, country1.getIsocode()))
		{
			if (StringUtils.equalsIgnoreCase(defaultCountryIsocode, country2.getIsocode()))
			{
				return EQUAL;
			}
			else
			{
				return BEFORE;
			}
		}
		else if (StringUtils.equalsIgnoreCase(defaultCountryIsocode, country2.getIsocode()))
		{
			return AFTER;
		}
		else
		{
			int result = compareValues(country1.getName(), country2.getName(), false);
			if (EQUAL == result)
			{
				result = compareValues(country1.getIsocode(), country2.getIsocode(), true);
			}
			return result;
		}
	}

	public String getDefaultCountryIsocode()
	{
		return defaultCountryIsocode;
	}

	public void setDefaultCountryIsocode(final String defaultCountryIsocode)
	{
		this.defaultCountryIsocode = defaultCountryIsocode;
	}
}