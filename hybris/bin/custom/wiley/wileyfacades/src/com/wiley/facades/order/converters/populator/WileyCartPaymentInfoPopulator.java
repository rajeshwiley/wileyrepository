package com.wiley.facades.order.converters.populator;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.WileyPaymentInfoData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;

public class WileyCartPaymentInfoPopulator implements Populator<CartModel, CartData>
{
	@Resource
	private Converter<PaymentInfoModel, WileyPaymentInfoData> wileyPaymentInfoConverter;

	@Override
	public void populate(final CartModel cartModel, final CartData cartData) throws ConversionException
	{
		validateParameterNotNullStandardMessage("CartModel", cartModel);
		validateParameterNotNullStandardMessage("CartData", cartData);

		final PaymentInfoModel paymentInfoModel = cartModel.getPaymentInfo();

		if (paymentInfoModel != null)
		{
			final WileyPaymentInfoData wileyPaymentInfoData = wileyPaymentInfoConverter.convert(paymentInfoModel);
			cartData.setWileyPaymentInfo(wileyPaymentInfoData);
		}
	}
}
