package com.wiley.facades.wel.product;

import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;

import com.wiley.core.model.cms.CMSProductAttributeModel;
import com.wiley.facades.product.WileyProductFacade;
import com.wiley.facades.product.data.ProductComparisonData;
import com.wiley.facades.product.data.WileyProductListCFAData;
import com.wiley.facades.product.node.ProductRootNode;


/**
 * Interface contains methods for getting product data-objects of wel site
 */
public interface WelProductFacade extends WileyProductFacade
{
	/**
	 * Populates data for PLP page
	 *
	 * @param categoryCode
	 * 		The category code
	 * @return WileyProductListCFAData product list cfa data
	 */
	WileyProductListCFAData getProductListCFAData(String categoryCode);

	/**
	 * Calculates price of default set variant to display on PDP or PLP.
	 * Traverses nodes to get product set.
	 *
	 * @param rootNode
	 * 		- ordered collection of variants of a product
	 * @return set's price if it present, null otherwise
	 */
	Double  getPriceForDefaultSetProduct(ProductRootNode rootNode);

	Double getPriceForDefaultPart(ProductRootNode rootNode);

	/**
	 * Returns ProductComparisonData for specified category
	 *
	 * @param categoryCode
	 * @param productOptions
	 * @param maxNumberOfProducts
	 * @return
	 */
	ProductComparisonData getComparisonProductsForCategory(String categoryCode, List<ProductOption> productOptions,
			Integer maxNumberOfProducts);

	/**
	 * Returns ProductComparisonData for specified baseCategory and variantValueCategory
	 *
	 * @param baseProductCategoryCode
	 * @param variantValueCategoryCode
	 * @param productOptions
	 * @param maxNumberOfProducts
	 * @return
	 */
	ProductComparisonData getComparisonProductsForCategories(String baseProductCategoryCode,
			String variantValueCategoryCode, List<ProductOption> productOptions, Integer maxNumberOfProducts);

	/**
	 * Returns ProductComparisonData for specified products
	 *
	 * @param products
	 * @param productOptions
	 * @return
	 */
	ProductComparisonData getComparisonProductData(List<? extends ProductModel> products,
			List<ProductOption> productOptions, List<CMSProductAttributeModel> attributes);

	/**
	 * @param products
	 * @param productOptions
	 * @return
	 */
	List<ProductData> getSupplementsProductData(List<? extends ProductModel> products,
			List<ProductOption> productOptions);

	/**
	 * Return supplements list for category
	 *
	 * @param productOptions
	 * @return
	 */
	List<ProductData> getSupplementProductsForCategory(String categoryCode,
			List<ProductOption> productOptions);

	/**
	 * Return supplements list for baseCategory and variantValueCategory
	 *
	 * @param baseCategoryCode
	 * @param variantValueCategoryCode
	 * @param productOptions
	 * @return
	 */
	List<ProductData> getSupplementProductsForCategories(String baseCategoryCode,
			String variantValueCategoryCode, List<ProductOption> productOptions);

	/**
	 * Gets partner products by categoryCode and partnerId. If partner has
	 * UNA_PARTNER DiscountGroup, than products will be sorted by name Otherwise
	 * products will be sorted by max variant price
	 *
	 * @param categoryCode
	 * 		the category code
	 * @param partnerId
	 * 		the partner id
	 * @return the partner products by category and partner
	 */
	List<ProductData> getPartnerProductsByCategoryAndPartner(String categoryCode, String partnerId);
}
