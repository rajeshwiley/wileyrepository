package com.wiley.facades.order.converters.populator;

import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.order.WileyCheckoutService;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;


public class WileyTaxAvailabilityPopulator implements Populator<AbstractOrderModel, AbstractOrderData>
{
	@Autowired
	private WileyCheckoutService wileyCheckoutService;

	@Override
	public void populate(final AbstractOrderModel source, final AbstractOrderData target) throws ConversionException
	{
		target.setTaxAvailable(wileyCheckoutService.isTaxAvailable(source));
	}
}
