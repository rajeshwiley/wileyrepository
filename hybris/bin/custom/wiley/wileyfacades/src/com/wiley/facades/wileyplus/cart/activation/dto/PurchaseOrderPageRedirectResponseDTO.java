package com.wiley.facades.wileyplus.cart.activation.dto;

public class PurchaseOrderPageRedirectResponseDTO
{
	private String url;

	public String getUrl()
	{
		return url;
	}

	public void setUrl(final String url)
	{
		this.url = url;
	}
}
