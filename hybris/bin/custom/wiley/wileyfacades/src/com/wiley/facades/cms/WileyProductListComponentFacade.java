package com.wiley.facades.cms;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.ProductData;

import java.util.List;

import com.wiley.core.model.components.WileyProductListComponentModel;


/**
 * Created by Uladzimir_Barouski on 5/22/2017.
 */
public interface WileyProductListComponentFacade
{
	List<ProductData> getComponentProducts(CategoryModel category, WileyProductListComponentModel component);
}
