package com.wiley.facades.populators;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.wileyb2c.i18n.Wileyb2cI18NService;


public class Wileyb2cProductPricePopulator extends WileyProductPricePopulator
{

	private Wileyb2cI18NService wileyb2cI18NService;

	@Override
	public void populate(final ProductModel productModel, final ProductData productData)
	{
		super.populate(productModel, productData);

		if (productData.getPrice() != null)
		{
			Optional<String> currentCountryTaxShortMessage = getWileyb2cI18NService().getCurrentCountryTaxShortMsg();
			Optional<String> currentCountryTaxTooltip = getWileyb2cI18NService().getCurrentCountryTaxTooltip();
			productData.getPrice().setOptionalTaxShortMessage(currentCountryTaxShortMessage.orElse(null));
			productData.getPrice().setOptionalTaxTooltip(currentCountryTaxTooltip.orElse(null));
		}
	}

	protected Wileyb2cI18NService getWileyb2cI18NService()
	{
		return wileyb2cI18NService;
	}

	@Required
	public void setWileyb2cI18NService(final Wileyb2cI18NService wileyb2cI18NService)
	{
		this.wileyb2cI18NService = wileyb2cI18NService;
	}

}
