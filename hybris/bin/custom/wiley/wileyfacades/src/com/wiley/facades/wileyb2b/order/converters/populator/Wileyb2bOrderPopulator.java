package com.wiley.facades.wileyb2b.order.converters.populator;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.promotions.result.PromotionOrderResults;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.wiley.core.model.WileyExternalDiscountModel;
import com.wiley.facades.product.data.DiscountData;
import com.wiley.facades.wileycom.order.converters.populators.WileycomOrderPopulator;


/**
 * Wileyb2b specific order populator
 */
public class Wileyb2bOrderPopulator extends WileycomOrderPopulator
{
	@Resource
	private Converter<WileyExternalDiscountModel, DiscountData> wileyb2bExternalDiscountToDiscountDataConverter;

	@Override
	protected void addDetails(final OrderModel source, final OrderData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setUserNotes(source.getUserNotes());

		super.addDetails(source, target);
	}

	@Override
	public void populate(final OrderModel source, final OrderData target)
	{
		super.populate(source, target);

		target.setDesiredShippingDate(source.getDesiredShippingDate());
		target.setSubTotalWithoutDiscount(createPrice(source, source.getSubtotal()));

		target.setDiscountData(
				Converters.convertAll(source.getExternalDiscounts(), wileyb2bExternalDiscountToDiscountDataConverter));
	}

	@Override
	protected Double calcTotalWithTax(final AbstractOrderModel source)
	{
		/*According to IDD https://confluence.wiley.ru/display/ECSC/P2+-+hy-029+External+cart+calculation
		 * Order's total price already includes total tax.*/
		return source.getTotalPrice();
	}

	@Override
	protected void addPromotions(final AbstractOrderModel source, final AbstractOrderData prototype)
	{
		addPromotions(source, null, prototype);
	}

	@Override
	protected void addPromotions(final AbstractOrderModel source, final PromotionOrderResults promoOrderResults,
			final AbstractOrderData prototype)
	{
		final double productsDiscountsAmount = getProductsDiscountsAmount(source);
		final double orderDiscountsAmount = getOrderDiscountsAmount(source);

		prototype.setProductDiscounts(createPrice(source, productsDiscountsAmount));
		prototype.setOrderDiscounts(createPrice(source, orderDiscountsAmount));
		prototype.setTotalDiscounts(createPrice(source, source.getTotalDiscounts()));
	}

	@Override
	protected void addTotals(final AbstractOrderModel source, final AbstractOrderData prototype)
	{
		prototype.setTotalPrice(createPrice(source, source.getTotalPrice()));
		prototype.setTotalTax(createPrice(source, source.getTotalTax()));
		prototype.setSubTotal(createPrice(source, source.getSubtotal()));
		prototype.setDeliveryCost(source.getDeliveryMode() != null ? createPrice(source, source.getDeliveryCost()) : null);
		prototype.setTotalPriceWithTax((createPrice(source, calcTotalWithTax(source))));
	}
}
