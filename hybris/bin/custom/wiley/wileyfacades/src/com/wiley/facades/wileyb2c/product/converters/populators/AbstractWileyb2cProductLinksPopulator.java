package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.platform.commercefacades.product.converters.populator.AbstractProductPopulator;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.enums.WileyWebLinkTypeEnum;
import com.wiley.core.model.WileyWebLinkModel;
import com.wiley.core.wileyb2c.weblinks.WileyWebLinkService;
import com.wiley.facades.wileyb2c.product.data.WebLinkData;


abstract class AbstractWileyb2cProductLinksPopulator extends AbstractProductPopulator<ProductModel, ProductData>
{
	private static final String EMPTY_URL = "#";

	private WileyWebLinkService wileyWebLinkService;

	protected List<WebLinkData> collectLinksOfTypes(final Collection<WileyWebLinkModel> links,
			final List<WileyWebLinkTypeEnum> types)
	{
		List<WebLinkData> linkData = new ArrayList<>();
		for (WileyWebLinkTypeEnum type : types)
		{
			final Optional<WileyWebLinkModel> linkOfType = links.stream().filter(link -> type == link.getType()).findFirst();
			if (linkOfType.isPresent())
			{
				linkData.add(convertToWebLinkData(linkOfType.get()));
			}
		}
		return linkData;
	}

	protected WebLinkData getLinkOfType(final Collection<WileyWebLinkModel> links, final WileyWebLinkTypeEnum type) {
		List<WebLinkData> linkDatas = collectLinksOfTypes(links, Collections.singletonList(type));
		WebLinkData linkData = null;
		if (CollectionUtils.isNotEmpty(linkDatas)) {
			linkData = linkDatas.get(0);
		}
		return linkData;
	}

	private WebLinkData convertToWebLinkData(final WileyWebLinkModel webLinkModel)
	{
		final WebLinkData data = new WebLinkData();
		final String webLinkUrl = wileyWebLinkService.getWebLinkUrl(webLinkModel);
		data.setUrl(webLinkUrl != null ? webLinkUrl : EMPTY_URL);
		data.setType(webLinkModel.getType());
		return data;
	}

	@Required
	public void setWileyWebLinkService(final WileyWebLinkService wileyWebLinkService)
	{
		this.wileyWebLinkService = wileyWebLinkService;
	}
}
