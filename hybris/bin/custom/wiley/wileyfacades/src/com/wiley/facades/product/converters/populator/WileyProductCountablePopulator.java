package com.wiley.facades.product.converters.populator;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import com.wiley.core.product.WileyCountableProductService;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


public class WileyProductCountablePopulator implements Populator<ProductModel, ProductData>
{
	private WileyCountableProductService wileyCountableProductService;

	@Override
	public void populate(final ProductModel source, final ProductData target) throws ConversionException
	{
		validateParameterNotNull(source, "product model mustn't be null");
		validateParameterNotNull(target, "product data mustn't be null");

		target.setCountable(wileyCountableProductService.canProductHaveQuantity(source));
	}

	public void setWileyCountableProductService(final WileyCountableProductService wileyCountableProductService)
	{
		this.wileyCountableProductService = wileyCountableProductService;
	}


}
