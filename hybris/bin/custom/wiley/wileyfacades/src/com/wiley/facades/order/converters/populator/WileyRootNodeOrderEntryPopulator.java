package com.wiley.facades.order.converters.populator;

import de.hybris.platform.commercefacades.order.converters.populator.OrderEntryPopulator;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.Arrays;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.wiley.core.wiley.restriction.WileyProductRestrictionService;
import com.wiley.facades.converters.ProductNodesConverter;


/**
 * Extends implementation of {@link OrderEntryPopulator} for WEL site.
 */
public class WileyRootNodeOrderEntryPopulator implements Populator<AbstractOrderEntryModel, OrderEntryData>
{

	@Resource(name = "productNodesConverter")
	private ProductNodesConverter productNodesConverter;

	@Resource(name = "wileyProductVariantFacade")
	private ProductFacade productFacade;

	@Resource
	private WileyProductRestrictionService wileyProductRestrictionService;

	@Override
	public void populate(@Nonnull final AbstractOrderEntryModel source, @Nonnull final OrderEntryData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		final ProductModel product = source.getProduct();
		setProductRootNode(target, product);
	}

	private void setProductRootNode(@Nonnull final OrderEntryData target, final ProductModel product)
	{
		if (product != null && product instanceof VariantProductModel)
		{
			ProductModel baseProductModel = ((VariantProductModel) product).getBaseProduct();

			final ProductData baseProductData = productFacade.getProductForOptions(baseProductModel,
					Arrays.asList(ProductOption.BASIC, ProductOption.CATEGORIES, ProductOption.VARIANT_MATRIX_BASE));

			target.setProductRootNode(productNodesConverter.convert(baseProductData));
		}
	}
}
