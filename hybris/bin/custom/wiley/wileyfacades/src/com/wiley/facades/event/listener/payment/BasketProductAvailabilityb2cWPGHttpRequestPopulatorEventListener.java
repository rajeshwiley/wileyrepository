package com.wiley.facades.event.listener.payment;

import com.wiley.core.event.facade.payment.BasketProductAvailabilityb2cWPGHttpRequestPopulatorEvent;
import com.wiley.facades.product.data.InventoryStatusRecord;
import com.wiley.facades.wileyb2c.order.Wileyb2cCartFacade;
import com.wiley.facades.wileyb2c.order.Wileyb2cCheckoutFacade;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionRequest;
import de.hybris.platform.acceleratorservices.payment.data.OrderEntryInfoData;
import de.hybris.platform.acceleratorservices.payment.data.PaymentData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.context.ApplicationListener;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Georgii_Gavrysh on 12/20/2016.
 */
public class BasketProductAvailabilityb2cWPGHttpRequestPopulatorEventListener
        extends AbstractBasketProductAvailabilityWPGHttpRequestPopulator
        implements ApplicationListener<BasketProductAvailabilityb2cWPGHttpRequestPopulatorEvent>
{
    @Resource
    private Wileyb2cCartFacade wileyb2cCartFacade;

    @Resource
    private Wileyb2cCheckoutFacade wileyb2cCheckoutFacade;

    @Override
    public void onApplicationEvent(final BasketProductAvailabilityb2cWPGHttpRequestPopulatorEvent event) {
        populate(event.getFirstParameter(), event.getSecondParameter());
    }

    @Override
    public void populate(final CreateSubscriptionRequest source, final PaymentData target)
            throws ConversionException
    {
        final List<OrderEntryInfoData> orderEntries = source.getOrderInfoData().getOrderEntries();
        if (CollectionUtils.isNotEmpty(orderEntries))
        {
            addBasketEntries(orderEntries, target);
        }
    }

    private void addBasketEntries(final List<OrderEntryInfoData> orderEntries, final PaymentData target)
    {
        final CartData cartData = wileyb2cCartFacade.getSessionCart();
        wileyb2cCheckoutFacade.doRealTimeInventoryCheck(cartData);

        final List<OrderEntryData> cartDataEntries = cartData.getEntries();

        for (int orderEntryIndex = 1; orderEntryIndex <= orderEntries.size(); orderEntryIndex++)
        {
            final OrderEntryInfoData orderEntryInfo = orderEntries.get(orderEntryIndex - 1);

            final List<InventoryStatusRecord> inventoryStatusRecords =
                    findCartEntry(cartDataEntries, orderEntryInfo.getSapCode());
            if (CollectionUtils.isNotEmpty(inventoryStatusRecords))
            {
                this.populatePaymentData(target, orderEntryIndex, inventoryStatusRecords);
            }
        }
    }

    private List<InventoryStatusRecord> findCartEntry(final List<OrderEntryData> cartDataEntries, final String sapCode)
    {
        for (final OrderEntryData cartDataEntry : cartDataEntries)
        {
            final ProductData product = cartDataEntry.getProduct();
            // I've raised a question if this assert should be here in this ticket (https://jira.wiley.ru/browse/ECSC-13471)
            Assert.notNull(product);

            if (product.getSapProductCode().equals(sapCode)) {
                return product.getInventoryStatus();
            }
        }
        return null;
    }

}
