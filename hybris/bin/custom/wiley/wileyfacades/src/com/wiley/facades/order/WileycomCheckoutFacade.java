/**
 *
 */
package com.wiley.facades.order;

import de.hybris.platform.commercefacades.order.data.DeliveryModeData;
import de.hybris.platform.commercefacades.user.data.AddressData;

import java.util.List;

import javax.annotation.Nonnull;


public interface WileycomCheckoutFacade extends WileyCheckoutFacade
{

	enum ExpressCheckoutResult
	{
		SUCCESS, ERROR_NOT_AVAILABLE,
		ERROR_DELIVERY_ADDRESS,
		ERROR_DELIVERY_MODE,
		ERROR_CHEAPEST_DELIVERY_MODE,
		ERROR_PAYMENT_INFO
	}

	/**
	 * Returns true if session cart has a digital entry. False in other case.
	 * In contrast of {@link #isDigitalSessionCart() isDigitalSessionCart} it is enough to have a single digital product
	 * entry in Cart to return true
	 *
	 * @return - boolean
	 */
	boolean hasSessionCartDigitalProduct();

	/**
	 * Check if cart in current session keeps only and only physical products
	 *
	 * @return true, if only physical products in the cart
	 */
	boolean isPhysicalSessionCart();


	/**
	 * Method performs the following actions:
	 * 	- create new AddressModel based on AddressData
	 * 	- sets this address as default payment one for a Customer
	 * 	- sets this address into a cart
	 * 
	 * @param addressData
	 */
	void setNewPaymentAddress(@Nonnull AddressData addressData);

	/**
	 * Method performs the following actions:
	 * 	- get addressModel by addressId
	 * 	- sets this address as default payment one for a Customer
	 * 	- sets this address into a cart
	 * 
	 * @param addressId
	 */
	void setPaymentAddress(@Nonnull String addressId);

	/**
	 * Checks if user already registered
	 *
	 * @param uid
	 * @return boolean value
	 */
	boolean isRegisteredUser(String uid);

	/**
	 * Set cheapest delivery mode if the cart has one or more supported delivery modes
	 *
	 * @param supportedDeliveryModes
	 * 		list of supported delivery modes
	 * @return true if successful
	 */
	boolean setDeliveryModeIfAvailable(List<? extends DeliveryModeData> supportedDeliveryModes);

	/**
	 * Checks if the current store is eligible for express checkout option
	 *
	 * @return true if checkout if express checkout of available for store
	 */
	boolean isExpressCheckoutEnabledForStore();

	/**
	 * Checks the required conditions and performs the express checkout
	 *
	 * @return the result of the operation
	 */
	ExpressCheckoutResult performExpressCheckout();
	
	/**
	 * A method to check if delivery mode is already set in session Cart.
	 * @return true or false respectively
	 */
	boolean isDeliveryModeAlreadySetInCart();

	/**
	 * A method to check if the provided deliveryMode is already set in the session Cart.
	 * @param deliveryMode
	 * @return true or false respectively
	 */
	boolean isDeliveryModeSetInCartTheSameThan(String deliveryMode);

	/**
	 * Check whether the specified Order contains only digital products so there is no shipping required.
	 * @param orderCode the order to check
	 * @return true if order is not digital
	 */
	boolean isDigitalOrder(String orderCode);

}
