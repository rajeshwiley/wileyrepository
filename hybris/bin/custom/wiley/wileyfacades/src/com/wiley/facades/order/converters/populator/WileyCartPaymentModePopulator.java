package com.wiley.facades.order.converters.populator;

import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


public class WileyCartPaymentModePopulator implements Populator<CartModel, CartData>
{

	@Override
	public void populate(final CartModel cartModel, final CartData cartData) throws ConversionException
	{
		validateParameterNotNullStandardMessage("CartModel", cartModel);
		validateParameterNotNullStandardMessage("CartData", cartData);

		cartData.setPaymentMode(cartModel.getPaymentMode() != null ? cartModel.getPaymentMode().getCode() : null);
	}
}
