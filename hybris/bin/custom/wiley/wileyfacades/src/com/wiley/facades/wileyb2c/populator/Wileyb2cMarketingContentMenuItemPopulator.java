package com.wiley.facades.wileyb2c.populator;

import de.hybris.platform.acceleratorcms.model.components.ProductReferencesComponentModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import com.wiley.facades.cms.data.MarketingContentMenuItemData;
import com.wiley.facades.wiley.util.WileySemanticUrlGenerator;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


public class Wileyb2cMarketingContentMenuItemPopulator
		implements Populator<ProductReferencesComponentModel, MarketingContentMenuItemData>
{
	@Resource
	private WileySemanticUrlGenerator wileySemanticUrlGenerator;

	@Override
	public void populate(@Nonnull final ProductReferencesComponentModel source,
			@Nonnull final MarketingContentMenuItemData target) throws ConversionException
	{
		validateParameterNotNullStandardMessage("source", source);
		validateParameterNotNullStandardMessage("target", target);

		target.setTitle(source.getTitle());
		target.setAnchorLink(String.format("#%s-section", wileySemanticUrlGenerator.generateFor(source.getUid())));
	}
}