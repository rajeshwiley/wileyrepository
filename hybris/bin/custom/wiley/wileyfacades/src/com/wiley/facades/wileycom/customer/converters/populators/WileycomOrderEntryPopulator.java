package com.wiley.facades.wileycom.customer.converters.populators;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.commercefacades.order.converters.populator.OrderEntryPopulator;
import de.hybris.platform.commercefacades.order.data.ConsignmentEntryData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Set;

import javax.validation.constraints.NotNull;


/**
 * Created by Mikhail_Asadchy on 8/9/2016.
 */
public class WileycomOrderEntryPopulator extends OrderEntryPopulator
{
	private Converter<ConsignmentEntryModel, ConsignmentEntryData> consignmentEntryConverter;

	@Override
	public void populate(@NotNull final AbstractOrderEntryModel source, @NotNull final OrderEntryData target)
	{
		super.populate(source, target);

		populateConsignments(source, target);
	}

	private void populateConsignments(@NotNull final AbstractOrderEntryModel source, @NotNull final OrderEntryData target)
	{
		final Long totalShippedQuantity = calculateTotalShipped(source);
		target.setTotalShipped(totalShippedQuantity);

		final Long quantity = source.getQuantity() == null ? Long.valueOf(0) : source.getQuantity();
		populateConsignmentStatus(target, totalShippedQuantity, quantity);

		target.setConsignmentEntries(Converters.convertAll(source.getConsignmentEntries(), getConsignmentEntryConverter()));
	}

	private void populateConsignmentStatus(final OrderEntryData target, final long totalShippedQuantity, final long quantity)
	{
		if (quantity == 0)
		{
			target.setConsignmentStatus(ConsignmentStatus.CANCELLED);
		}
		else if (totalShippedQuantity == 0)
		{
			target.setConsignmentStatus(ConsignmentStatus.WAITING);
		}
		else if (totalShippedQuantity >= quantity)
		{
			target.setConsignmentStatus(ConsignmentStatus.SHIPPED);
		}
		else
		{
			target.setConsignmentStatus(ConsignmentStatus.PARTIALLY_SHIPPED);
		}
	}

	private Long calculateTotalShipped(final AbstractOrderEntryModel source)
	{
		final Set<ConsignmentEntryModel> consignmentEntries = source.getConsignmentEntries();
		Long totalShippedQuantity = 0L;
		if (consignmentEntries != null)
		{
			for (final ConsignmentEntryModel consignmentEntry : consignmentEntries)
			{
				totalShippedQuantity += consignmentEntry.getShippedQuantity();
			}
		}

		return totalShippedQuantity;
	}

	public Converter<ConsignmentEntryModel, ConsignmentEntryData> getConsignmentEntryConverter()
	{
		return consignmentEntryConverter;
	}

	public void setConsignmentEntryConverter(
			final Converter<ConsignmentEntryModel, ConsignmentEntryData> consignmentEntryConverter)
	{
		this.consignmentEntryConverter = consignmentEntryConverter;
	}
}
