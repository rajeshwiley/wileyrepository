package com.wiley.facades.wileyws.subscriptions.dto;

/**
 * Created by Georgii_Gavrysh on 10/5/2016.
 */
public class SubscriptionSearchResult
{
	private String internalSubscriptionId;

	public String getInternalSubscriptionId()
	{
		return internalSubscriptionId;
	}

	public void setInternalSubscriptionId(final String internalSubscriptionId)
	{
		this.internalSubscriptionId = internalSubscriptionId;
	}
}
