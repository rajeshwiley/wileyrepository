package com.wiley.facades.wileycom.order.converters.populators;

import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.CartModel;

import java.util.List;

import com.wiley.facades.product.data.InventoryStatusRecord;

import org.springframework.util.Assert;


public class WileycomHasOutOfStockCartPopulator implements Populator<CartModel, CartData>
{
	@Override
	public void populate(final CartModel source, final CartData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setHasOutOfStockItems(hasOutOfStockItems(target));
	}

	private static Boolean hasOutOfStockItems(final CartData cartData)
	{
		final List<OrderEntryData> entries = cartData.getEntries();
		for (final OrderEntryData entry : entries)
		{
			final List<InventoryStatusRecord> inventoryStatusRecords = entry.getProduct().getInventoryStatus();
			if (inventoryStatusRecords != null && inventoryStatusRecords.stream().anyMatch(
					record -> record.getStatusCode() != StockLevelStatus.INSTOCK))
			{
				return true;
			}
		}
		return false;
	}

}
