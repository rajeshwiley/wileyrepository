package com.wiley.facades.wileyb2c.order.impl;

import de.hybris.platform.commercefacades.order.impl.DefaultOrderFacade;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.OrderService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.store.BaseStoreModel;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import com.wiley.facades.wileyb2c.order.Wileyb2cOrderFacade;


/**
 * Wiley B2C specific Order Facade Implementation
 */
public class Wileyb2cOrderFacadeImpl extends DefaultOrderFacade implements Wileyb2cOrderFacade
{
	private static final String ORDER_NOT_FOUND_FOR_USER_AND_BASE_STORE =
			"Order with guid %s not found for current user in current BaseStore";
	@Resource
	private OrderService orderService;

	@Override
	public String getOrderCodeForGUID(final String guid)
	{
		final OrderModel orderModel = getCustomerAccountService().getGuestOrderForGUID(guid,
				getBaseStoreService().getCurrentBaseStore());
		if (orderModel == null)
		{
			throw new UnknownIdentifierException(String.format(ORDER_NOT_FOUND_FOR_USER_AND_BASE_STORE, guid));
		}
		return orderModel.getCode();
	}

	@Override
	public OrderEntryModel getOrderEntryByOrderCodeAndEntryNumber(@Nonnull final String orderCode,
			@Nonnull final Integer entryNumber)
	{
		final CustomerModel currentCustomer = (CustomerModel) getUserService().getCurrentUser();
		final BaseStoreModel currentBaseStore = getBaseStoreService().getCurrentBaseStore();

		OrderModel order = getCustomerAccountService().getOrderForCode(currentCustomer, orderCode, currentBaseStore);
		return orderService.getEntryForNumber(order, entryNumber);
	}
}
