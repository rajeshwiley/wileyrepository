package com.wiley.facades.wileyb2b.customer.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2bacceleratorfacades.customer.impl.DefaultB2BCustomerFacade;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.exceptions.CalculationException;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import com.wiley.core.customer.ResetPasswordRedirectType;
import com.wiley.facades.customer.WileycomCustomerFacade;
import com.wiley.facades.customer.data.NameSuffixData;
import com.wiley.facades.customer.data.SchoolData;
import com.wiley.facades.wileyb2b.customer.Wileyb2bCustomerFacade;


/**
 * Wiley B2B specific customer facade implementation.
 *
 * This class extends the OOTB {@link DefaultB2BCustomerFacade}
 * and delegates additional "wileycom" specific calls to {@link WileycomCustomerFacade}.
 */
public class Wileyb2bCustomerFacadeImpl extends DefaultB2BCustomerFacade implements Wileyb2bCustomerFacade
{
	private static final Logger LOG = LoggerFactory.getLogger(Wileyb2bCustomerFacadeImpl.class);

	@Resource(name = "wileycomCustomerFacade")
	private WileycomCustomerFacade wileycomCustomerFacade;

	@Override
	public void loginSuccess()
	{
		loginSuccess(true);
	}


	@Override
	public void loginSuccess(final boolean recalculateCart)
	{
		final CustomerData userData = getCurrentCustomer();

		// First thing to do is to try to change the user on the session cart
		if (getCartService().hasSessionCart())
		{
			getCartService().changeCurrentCartUser(getUserService().getCurrentUser());
		}

		//NOTE: OOTB implentation has currency restoration here, but now currency is being set by country filters/handlers

		// Calculate the cart after setting everything up
		if (getCartService().hasSessionCart())
		{
			final CartModel sessionCart = getCartService().getSessionCart();

			// Clean the existing info on the cart if it does not beling to the current user
			getCartCleanStrategy().cleanCart(sessionCart);
			if (recalculateCart)
			{
				try
				{
					final CommerceCartParameter parameter = new CommerceCartParameter();
					parameter.setEnableHooks(true);
					parameter.setCart(sessionCart);
					getCommerceCartService().recalculateCart(parameter);
				}
				catch (final CalculationException ex)
				{
					LOG.error("Failed to recalculate order [" + sessionCart.getCode() + "]", ex);
				}
			}
			else
			{
				sessionCart.setCalculated(false);
				getModelService().save(sessionCart);
			}
		}
	}

	@Override
	public void forgottenPassword(final String uid, final ResetPasswordRedirectType type)
	{
		Assert.hasText(uid, "The field [uid] cannot be empty");
		final CustomerModel customerModel = getUserService().getUserForUID(uid.toLowerCase(),
				CustomerModel.class);
		Assert.state(customerModel instanceof B2BCustomerModel);
		wileycomCustomerFacade.forgottenPassword(uid, type);

	}

	@Override
	public List<NameSuffixData> getNameSuffixes()
	{
		return wileycomCustomerFacade.getNameSuffixes();
	}

	@Override
	public List<SchoolData> getSchoolNames()
	{
		return wileycomCustomerFacade.getSchoolNames();
	}

	@Override
	public List<SchoolData> getSchoolNamesWithLimitation()
	{
		return wileycomCustomerFacade.getSchoolNamesWithLimitation();
	}

	@Override
	public void updateProfile(final CustomerData customerData) throws DuplicateUidException
	{
		Assert.isNull(customerData.getEmail(), "Email should not be modified.");
		wileycomCustomerFacade.updateProfile(customerData);
	}
}