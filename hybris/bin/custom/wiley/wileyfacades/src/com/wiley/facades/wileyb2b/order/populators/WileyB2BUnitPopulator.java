package com.wiley.facades.wileyb2b.order.populators;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2bcommercefacades.company.data.B2BUnitData;
import de.hybris.platform.b2bcommercefacades.company.converters.populators.B2BUnitPopulator;
import de.hybris.platform.commercefacades.user.data.AddressData;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;


/**
 * Wiley B2B populator, uses default implementation B2BUnitPopulator and additional field conversion
 */
public class WileyB2BUnitPopulator extends B2BUnitPopulator
{

	@Autowired
	private B2BUnitPopulator defaultB2BUnitPopulator;

	@Override
	public void populate(final B2BUnitModel source, final B2BUnitData target)
	{
		defaultB2BUnitPopulator.populate(source, target);

		if (source.getBillingAddress() != null)
		{
			target.setBillingAddress(getAddressConverter().convert(source.getBillingAddress()));
		}

		target.setSapSalesRepContactName(source.getSapSalesRepContactName());
		target.setSapSalesRepContactPhone(source.getSapSalesRepContactPhone());
		target.setSapSalesRepContactEmail(source.getSapSalesRepContactEmail());

		populateShippingAddresses(target);
	}

	private void populateShippingAddresses(final B2BUnitData target)
	{
		List<AddressData> allAddresses = target.getAddresses();

		if (allAddresses != null)
		{
			List<AddressData> shippingAddresses = allAddresses.stream().filter(address -> address.isShippingAddress())
					.collect(Collectors.toList());
			target.setShippingAddresses(shippingAddresses);
		}

	}
}
