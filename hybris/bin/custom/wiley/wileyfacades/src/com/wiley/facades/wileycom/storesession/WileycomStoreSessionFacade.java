package com.wiley.facades.wileycom.storesession;

import de.hybris.platform.commercefacades.user.data.CountryData;

import java.util.List;
import java.util.Optional;

import javax.annotation.Nonnull;

import com.wiley.facade.user.data.WorldRegionData;
import com.wiley.facades.storesession.WileyStoreSessionFacade;

/**
 * B2C Session country facade
 */
public interface WileycomStoreSessionFacade extends WileyStoreSessionFacade
{
	/**
	 * Get session country
	 *
	 * @return {@link CountryData} object or null, if no country found
	 */
	CountryData getSessionCountry();

	/**
	 * Sets session country
	 *
	 * @param countryIsocode
	 * @return Current country isocode that is set to session. In case country was
	 *         not updated, {@link Optional#EMPTY} is returned.
	 */
	Optional<String> setCurrentCountry(@Nonnull String countryIsocode);

	/**
	 * Returns current country data object
	 *
	 * @return
	 */
	CountryData getCurrentCountry();

	/**
	 * Returns list of world region data objects
	 *
	 * @return
	 */
	List<WorldRegionData> getWorldRegions();

	/**
	 * Returns list of country data objects
	 *
	 * @return
	 */
	List<CountryData> getCounties();

	/**
	 * Returns external site link for redirect for country iso code
	 * @param countryIsoCode - country iso
	 * @return - regirect link if present
	 */
	Optional<String> getExternalSiteRedirect(String countryIsoCode);
}
