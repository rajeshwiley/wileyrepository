package com.wiley.facades.populators;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import com.wiley.core.model.WileyPurchaseOptionProductModel;


public class WileyProductPageUrlPopulator implements Populator<ProductModel, ProductData>
{
	@Autowired
	private UrlResolver productModelUrlResolver;

	@Override
	public void populate(final ProductModel source, final ProductData target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		String productPageUrl = source.getProductPageUrl();

		if (source instanceof WileyPurchaseOptionProductModel && productPageUrl == null)
		{
			productPageUrl = ((WileyPurchaseOptionProductModel) source).getBaseProduct().getProductPageUrl();
		}

		if (productPageUrl == null)
		{
			productPageUrl = productModelUrlResolver.resolve(source);
		}

		target.setProductPageUrl(productPageUrl);
	}
}