package com.wiley.facades.wileyb2c.product;

import de.hybris.platform.commercefacades.product.data.ProductData;

import javax.validation.constraints.NotNull;

import com.wiley.core.model.WileyPurchaseOptionProductModel;


/**
 * Facade for JSON LD
 *
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public interface WileyJsonLdProductFacade
{
	String createJsonLdFromProduct(@NotNull ProductData productData, @NotNull WileyPurchaseOptionProductModel productModel,
			@NotNull String url, String defaultImage);
}
