package com.wiley.facades.price.converters.populator;

import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.math.BigDecimal;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


public class PricePopulator implements Populator<PriceRowModel, PriceData>
{
	@Override
	public void populate(final PriceRowModel priceRowModel, final PriceData priceData) throws ConversionException
	{
		validateParameterNotNull(priceRowModel, "Price row model mustn't be null");
		validateParameterNotNull(priceData, "Price data mustn't be null");

		priceData.setValue(BigDecimal.valueOf(priceRowModel.getPrice()));
		priceData.setCurrencyIso(priceRowModel.getCurrency().getIsocode());
		priceData.setMinQuantity(priceRowModel.getMinqtd());
	}
}
