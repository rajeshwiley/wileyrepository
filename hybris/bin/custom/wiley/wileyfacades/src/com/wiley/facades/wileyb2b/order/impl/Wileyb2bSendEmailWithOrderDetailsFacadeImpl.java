package com.wiley.facades.wileyb2b.order.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import javax.annotation.Resource;

import com.google.common.base.Preconditions;
import com.wiley.core.wileyb2b.order.Wileyb2bSendEmailWithOrderDetailsService;
import com.wiley.core.wileycom.customer.service.WileycomCustomerAccountService;
import com.wiley.facades.wileyb2b.order.Wileyb2bSendEmailWithOrderDetailsFacade;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class Wileyb2bSendEmailWithOrderDetailsFacadeImpl implements Wileyb2bSendEmailWithOrderDetailsFacade
{

	private static final Logger LOG = LoggerFactory.getLogger(Wileyb2bSendEmailWithOrderDetailsFacadeImpl.class);

	@Resource
	private Wileyb2bSendEmailWithOrderDetailsService wileyb2bSendEmailWithOrderDetailsService;

	@Resource
	private WileycomCustomerAccountService wileyb2bCustomerAccountService;

	@Resource
	private BaseStoreService baseStoreService;

	@Resource
	private UserService userService;

	@Override
	public boolean sendEmailWithOrderDetails(final String orderCode, final String email)
	{
		Preconditions.checkArgument(StringUtils.isNotEmpty(orderCode), "Parameter orderCode can not be null or empty");
		Preconditions.checkArgument(StringUtils.isNotEmpty(email), "Parameter email can not be null or empty");

		BaseStoreModel baseStore = baseStoreService.getCurrentBaseStore();
		B2BCustomerModel b2BCustomer = (B2BCustomerModel) userService.getCurrentUser();

		OrderModel order = null;
		try
		{
			order = wileyb2bCustomerAccountService.getOrderForCode(orderCode, baseStore);
		}
		catch (ModelNotFoundException e)
		{
			throw new UnknownIdentifierException(
					String.format("Order with [%s] code not found for [%s] user", orderCode, b2BCustomer.getUid()), e);
		}

		return wileyb2bSendEmailWithOrderDetailsService.sendEmailWithOrderDetails(b2BCustomer, order, email);
	}
}
