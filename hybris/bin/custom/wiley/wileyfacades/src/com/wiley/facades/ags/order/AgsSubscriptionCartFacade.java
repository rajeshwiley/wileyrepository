package com.wiley.facades.ags.order;

import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.subscriptionfacades.order.SubscriptionCartFacade;

import javax.annotation.Nonnull;


/**
 * Facade extends functionality of {@link SubscriptionCartFacade}.
 *
 * @author Robert_Farkas
 * @author Gergo_Fodi
 */
public interface AgsSubscriptionCartFacade extends SubscriptionCartFacade
{
	/**
	 * Add order entries to the given cart model.
	 *
	 * @param cartCode
	 * 			the cart code
	 * @param code
	 *           code of product to add
	 * @param quantity
	 *           the quantity of the product
	 * @return the cart modification data that includes a statusCode and the actual quantity added to the cart
	 * @throws CommerceCartModificationException
	 *            if the cart cannot be modified
	 */
	CartModificationData addToCart(@Nonnull String cartCode, @Nonnull String code, long quantity)
			throws CommerceCartModificationException;
}
