package com.wiley.facades.payment.impl;

import com.google.common.base.Preconditions;
import com.wiley.core.address.WileyAddressService;
import com.wiley.core.customer.service.WileyCustomerAccountService;
import com.wiley.core.model.PendingBillingAddressModel;
import com.wiley.facades.payment.WileyPaymentFacade;
import com.wiley.core.payment.WileyHttpRequestParams;
import com.wiley.core.payment.constants.PaymentConstants;
import com.wiley.core.payment.impl.WileyAcceleratorPaymentServiceImpl;
import de.hybris.platform.acceleratorfacades.payment.data.PaymentSubscriptionResultData;
import de.hybris.platform.acceleratorfacades.payment.impl.DefaultPaymentFacade;
import de.hybris.platform.acceleratorservices.payment.data.PaymentData;
import de.hybris.platform.acceleratorservices.payment.data.PaymentSubscriptionResultItem;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.order.CommerceCartCalculationStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.model.ModelService;
import org.springframework.beans.factory.annotation.Required;

import javax.annotation.Resource;
import java.util.Map;
import java.util.Optional;


public class WileyPaymentFacadeImpl extends DefaultPaymentFacade implements WileyPaymentFacade
{
	private WileyAcceleratorPaymentServiceImpl validatePaymentService;

	@Resource
	private WileyCustomerAccountService wileyCustomerAccountService;

	@Resource
	private WileyAddressService wileyAddressService;

	@Resource
	private ModelService modelService;

	@Resource
	private CartService cartService;

	@Resource
	private Populator<AddressData, AddressModel> addressReversePopulator;

	private CommerceCartCalculationStrategy commerceCartCalculationStrategy;


	public PaymentData beginHopValidatePayment(final String responseUrl, final String merchantCallbackUrl)
	{
		final AddressModel paymentAddress = getDefaultPaymentAddress(getCurrentUserForCheckout());
		return internalBeginHopValidatePayment(responseUrl, merchantCallbackUrl, paymentAddress);
	}

	@Override
	public PaymentData beginHopValidatePayment(final String responseUrl, final String merchantCallbackUrl,
											   final AddressData billingAddress)
	{
		final CustomerModel customerModel = getCurrentUserForCheckout();

		PendingBillingAddressModel addressModel = modelService.create(PendingBillingAddressModel.class);
		addressReversePopulator.populate(billingAddress, addressModel);

		final String fullResponseUrl = getFullResponseUrl(responseUrl, true);
		final String fullMerchantCallbackUrl = getFullResponseUrl(merchantCallbackUrl, true);
		final String siteName = getCurrentSiteName();
		final CreditCardPaymentInfoModel defaultCC = (CreditCardPaymentInfoModel) customerModel.getDefaultPaymentInfo();

		PaymentData paymentData = getValidatePaymentService().beginHopCreatePaymentSubscription(siteName, fullResponseUrl,
				fullMerchantCallbackUrl, customerModel, defaultCC, addressModel);

		String transactionId = paymentData.getParameters().get(WileyHttpRequestParams.WPG_TRANSACTION_ID);
		addressModel.setOwner(customerModel);
		addressModel.setTransactionId(transactionId);
		addressModel.setDefault(billingAddress.isDefaultAddress());

		savePendingBillingAddress(customerModel, addressModel);

		return paymentData;
	}

	public PaymentData beginHopValidatePayment(final String responseUrl, final String merchantCallbackUrl,
											   final String addressId)
	{
		final Optional<AddressModel> paymentAddress = wileyAddressService.getAddressById(addressId);
		Preconditions.checkArgument(paymentAddress.isPresent(), "payment address");
		updatePaymentAddressInCart(paymentAddress.get());
		PaymentData paymentData = internalBeginHopValidatePayment(responseUrl, merchantCallbackUrl, paymentAddress.get());
		wileyAddressService.updateAddressWithTransactionId(paymentAddress.get(), getTransactionIdFromPaymentData(paymentData));
		return paymentData;
	}

	private void savePendingBillingAddress(final CustomerModel customer, final PendingBillingAddressModel address)
	{
		AddressModel oldPendingAddress = customer.getPendingBillingAddress();
		if (oldPendingAddress != null)
		{
			modelService.remove(oldPendingAddress);
		}
		customer.setPendingBillingAddress(address);
		modelService.saveAll(customer, address);
	}

	private void updatePaymentAddressInCart(final AddressModel paymentAddress)
	{
		if (cartService.hasSessionCart())
		{
			CartModel cart = cartService.getSessionCart();
			modelService.refresh(cart);
			cart.setPaymentAddress(paymentAddress);
			modelService.save(cart);
			CommerceCartParameter commerceCartParameter = new CommerceCartParameter();
			commerceCartParameter.setEnableHooks(true);
			commerceCartParameter.setCart(cart);
			commerceCartCalculationStrategy.calculateCart(commerceCartParameter);
		}
	}

	@Override
	public PaymentSubscriptionResultData completeHopValidatePayment(final Map<String, String> parameters,
																	final boolean saveInAccount)
	{
		final CustomerModel customerModel = getCurrentUserForCheckout();

		final PaymentSubscriptionResultItem paymentSubscriptionResultItem;
		if (isValidateWithoutCheckout(parameters)) {
			paymentSubscriptionResultItem = getValidatePaymentService()
					.completeHopCreatePaymentSubscriptionWithoutCheckout(customerModel, parameters);
		} else
		{
			paymentSubscriptionResultItem = getValidatePaymentService().completeHopCreatePaymentSubscription(
					customerModel, saveInAccount, parameters);
		}
		if (paymentSubscriptionResultItem != null)
		{
			return getPaymentSubscriptionResultDataConverter().convert(paymentSubscriptionResultItem);
		}

		return null;
	}

	public boolean isValidateWithoutCheckout(final Map<String, String> parameters)
	{
		CustomerModel customerModel = getCurrentUserForCheckout();
		String operation = parameters.get(PaymentConstants.WPG.OPERATION_KEY);
		String transactionID = parameters.get(PaymentConstants.WPG.TRANSACTION_KEY);
		if (PaymentConstants.WPG.OPERATION_VALIDATE.equals(operation))
		{
			AddressModel pendingBillingAddress = customerModel.getPendingBillingAddress();
			return pendingBillingAddress != null && pendingBillingAddress.getTransactionId().equals(transactionID);
		}
		return false;
	}

	@Override
	protected AddressModel getDefaultPaymentAddress(final CustomerModel customerModel)
	{
		return wileyCustomerAccountService.getPaymentAddress(customerModel);
	}

	private PaymentData internalBeginHopValidatePayment(final String responseUrl, final String merchantCallbackUrl,
														final AddressModel paymentAddress)
	{
		final String fullResponseUrl = getFullResponseUrl(responseUrl, true);
		final String fullMerchantCallbackUrl = getFullResponseUrl(merchantCallbackUrl, true);
		final String siteName = getCurrentSiteName();
		final CustomerModel customerModel = getCurrentUserForCheckout();
		final CreditCardPaymentInfoModel defaultCC = (CreditCardPaymentInfoModel) customerModel.getDefaultPaymentInfo();

		return getValidatePaymentService().beginHopCreatePaymentSubscription(siteName, fullResponseUrl,
				fullMerchantCallbackUrl, customerModel, defaultCC, paymentAddress);
	}

	private String getTransactionIdFromPaymentData(final PaymentData paymentData)
	{
		String transactionId = null;
		if (paymentData != null && paymentData.getParameters() != null)
		{
			transactionId = paymentData.getParameters().get(WileyHttpRequestParams.WPG_TRANSACTION_ID);
		}
		return transactionId;
	}


	protected WileyAcceleratorPaymentServiceImpl getValidatePaymentService()
	{
		return validatePaymentService;
	}

	public void setValidatePaymentService(final WileyAcceleratorPaymentServiceImpl validatePaymentService)
	{
		this.validatePaymentService = validatePaymentService;
	}

	@Required
	public void setCommerceCartCalculationStrategy(final CommerceCartCalculationStrategy commerceCartCalculationStrategy)
	{
		this.commerceCartCalculationStrategy = commerceCartCalculationStrategy;
	}
}