package com.wiley.facades.converters;

import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.ProductReferenceData;
import de.hybris.platform.commercefacades.product.data.VariantMatrixElementData;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.facades.product.node.ProductNode;
import com.wiley.facades.product.node.ProductNodeValue;
import com.wiley.facades.product.node.ProductRootNode;

import static java.util.Objects.nonNull;


public class ProductNodesConverter implements Converter<ProductData, ProductRootNode> {
    private static final String WEL_CFA_TYPE_CODE = "WEL_CFA_TYPE";

    public static final String NODE_VIEW_TYPE_RADIO = "radio";
    public static final String NODE_VIEW_TYPE_CHECKBOX = "checkbox";
    public static final String NODE_VIEW_TYPE_HIDDEN = "hidden";

    private static final String NODE_VIEW_NAME_PRODUCT = "product";
    private static final String NODE_VIEW_NAME_SKU = "sku";
    private static final String NODE_VIEW_NAME_SET = "set";

    private ProductFacade productFacade;

    @Override
    public ProductRootNode convert(final ProductData productData) throws ConversionException {
        return convert(productData, new ProductRootNode());
    }

    @Override
    public ProductRootNode convert(final ProductData productData, final ProductRootNode productRootNode)
            throws ConversionException {
        Assert.notNull(productData, "Parameter productData cannot be null.");
        Assert.notNull(productRootNode, "Parameter productNodes cannot be null.");
        productRootNode.setProduct(buildProductNode(productData));
        productRootNode.setAddons(buildProductNode());
        populateProductNode(productData, productRootNode.getProduct());
        populateAddonsNode(productData, productRootNode.getAddons());
        return productRootNode;
    }

    private void populateProductNode(final ProductData productData, final ProductNode node) {
        if (productData.getVariantMatrix() != null) {
            node.setNodes(buildProductNodes(productData.getVariantMatrix()));
        } else {
            node.setValue(buildProductNodeValue(productData));
        }
    }

    private List<ProductNode> buildProductNodes(final List<VariantMatrixElementData> variantMatrix)
    {
        final List<ProductNode> productNodes = variantMatrix.stream()
                .map(this::buildProductNode)
                .collect(Collectors.toList());

        // we don't need show selector for variant option if only one option exists
        if (isOnlyOneOptionToChoose(productNodes))
        {
            final ProductNode productNode = productNodes.get(0);
            if (!isLeafProductNode(productNode))
            {
                productNode.setViewType(NODE_VIEW_TYPE_HIDDEN);
            }
        }

        return productNodes;
    }

    private boolean isLeafProductNode(final ProductNode productNode)
    {
        return CollectionUtils.isEmpty(productNode.getNodes());
    }

    private boolean isOnlyOneOptionToChoose(final List<ProductNode> variantMatrix)
    {
        return variantMatrix.size() == 1;
    }

    private void populateAddonsNode(final ProductData productData, final ProductNode node) {
        final List<ProductReferenceData> productReferences = getProductReferencesFor(productData);
        for (ProductReferenceData productReference : productReferences) {
            final ProductData addonData = productReference.getTarget();
            final ProductNode addonNode = buildProductNode(addonData);
            node.getNodes().add(addonNode);
            populateProductNode(addonData, addonNode);
        }
    }

    private ProductNode buildProductNode() {
        final ProductNode productNode = new ProductNode();
        productNode.setNodes(new ArrayList<>());
        return productNode;
    }

    private ProductNode buildProductNode(final ProductData productData) {
        final ProductNode productNode = new ProductNode();
        productNode.setNodes(new ArrayList<>());
        productNode.setCode(productData.getCode());
        productNode.setName(productData.getName());
        return productNode;
    }

    private ProductNode buildProductNode(final VariantMatrixElementData variantMatrixElementData) {
        final ProductNode productNode = new ProductNode();
        productNode.setCode(variantMatrixElementData.getVariantValueCategory().getCode());
        productNode.setSetProduct(variantMatrixElementData.getIsSetCategory());
        productNode.setName(variantMatrixElementData.getVariantValueCategory().getName());
        populateProductNodeView(variantMatrixElementData, productNode);
        if (variantMatrixElementData.getIsLeaf()) {
            productNode.setValue(buildProductNodeValue(variantMatrixElementData));
        } else {
            productNode.setNodes(buildProductNodes(variantMatrixElementData.getElements()));
        }
        return productNode;
    }

    private void populateProductNodeView(final VariantMatrixElementData variantMatrixElementData,
            final ProductNode productNode) {
        productNode.setViewType(getNodeViewType(variantMatrixElementData));
        productNode.setViewName(getNodeViewName(variantMatrixElementData));
        productNode.setViewVisible(getNodeViewVisible(variantMatrixElementData));
    }

    private String getNodeViewType(final VariantMatrixElementData variantMatrixElementData) {
        String nodeViewType;
        final boolean variantCategoryType = variantMatrixElementData.getParentVariantCategory().isTypeVariantCategory();
        if (variantCategoryType) {
            nodeViewType = NODE_VIEW_TYPE_RADIO;
        } else {
            nodeViewType = NODE_VIEW_TYPE_CHECKBOX;
        }
        return nodeViewType;
    }

    private boolean getNodeUrlVisible(final VariantMatrixElementData variantMatrixElementData) {
        final String variantCategoryCode = variantMatrixElementData.getParentVariantCategory().getCode();
        return WEL_CFA_TYPE_CODE.equals(variantCategoryCode);
    }

	private String getNodeViewName(final VariantMatrixElementData variantMatrixElementData)
	{
		String nodeViewName = NODE_VIEW_NAME_PRODUCT;
		if (variantMatrixElementData.getIsLeaf())
		{
			if (variantMatrixElementData.getIsSetCategory())
			{
				nodeViewName = NODE_VIEW_NAME_SET;
			}
			else
			{
				nodeViewName = NODE_VIEW_NAME_SKU;
			}
		}
		return nodeViewName;
	}

    private boolean getNodeViewVisible(final VariantMatrixElementData variantMatrixElementData) {
        final String variantCategoryCode = variantMatrixElementData.getParentVariantCategory().getCode();
        return !(WileyCoreConstants.WEL_CFA_LEVEL_CATEGORY_CODE.equals(variantCategoryCode)
                || variantMatrixElementData.getIsSetCategory());
    }

    private ProductNodeValue buildProductNodeValue(final VariantMatrixElementData variantMatrixElementData)
    {
        final ProductNodeValue productNodeValue = new ProductNodeValue();
        final VariantOptionData variantOption = variantMatrixElementData.getVariantOption();
        productNodeValue.setSku(variantOption.getCode());
        productNodeValue.setUrl(variantOption.getUrl());
        productNodeValue.setUrlVisible(getNodeUrlVisible(variantMatrixElementData));
        productNodeValue.setPrice(getPriceFrom(variantOption.getPriceData()));
        if (nonNull(variantOption.getStudentPrice()))
        {
            productNodeValue.setStudentPrice(getPriceFrom(variantOption.getStudentPrice()));
        }
        if (nonNull(variantOption.getPartnerPrice()))
        {
            productNodeValue.setPartnerPrice(getPriceFrom(variantOption.getPartnerPrice()));
        }
        return productNodeValue;
    }

    private ProductNodeValue buildProductNodeValue(final ProductData productData) {
        final ProductNodeValue productNodeValue = new ProductNodeValue();
        productNodeValue.setSku(productData.getCode());
        productNodeValue.setPrice(getPriceFrom(productData.getPrice()));
        return productNodeValue;
    }

    private List<ProductReferenceData> getProductReferencesFor(final ProductData productData) {
        return productFacade.getProductReferencesForCode(productData.getCode(), Arrays
                .asList(ProductReferenceTypeEnum.ADDON), Arrays.asList(ProductOption.BASIC, ProductOption.PRICE,
                ProductOption.VARIANT_FULL, ProductOption.VARIANT_MATRIX, ProductOption.VARIANT_MATRIX_BASE,
                ProductOption.VARIANT_MATRIX_PRICE), null);
    }

    private double getPriceFrom(final PriceData priceData) {
        if (priceData != null && priceData.getValue() != null) {
            return priceData.getValue().doubleValue();
        }
        return 0;
    }

    @Required
    public void setProductFacade(final ProductFacade productFacade) {
        this.productFacade = productFacade;
    }

    public ProductFacade getProductFacade() {
        return productFacade;
    }
}
