package com.wiley.facades.order;

/**
 * Created by Uladzimir_Barouski on 2/25/2016.
 */
public interface WileyMagicLinkFacade
{
	String getMagicLink();
}
