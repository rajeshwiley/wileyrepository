package com.wiley.facades.wileyws.customer.update.facade;

import de.hybris.platform.commerceservices.customer.DuplicateUidException;

import javax.annotation.Nonnull;

import com.wiley.facades.wileyws.customer.update.dto.CustomerUpdateRequest;


/**
 * This interface is about inbound messages for updates in customer information.
 */
public interface WileyCustomerUpdateFacade
{
	/**
	 * Updates customer based on the given {@link CustomerUpdateRequest} DTO.
	 *
	 * @param customerId
	 * 		User's unique identifier that cannot be changed. This is an internal identifier assigned by hybris.
	 * @param customerUpdateRequest
	 * 		the customer update request
	 * @throws DuplicateUidException
	 * 		in case of ambiguous customer identifier
	 */
	void updateCustomer(@Nonnull String customerId, @Nonnull CustomerUpdateRequest customerUpdateRequest)
			throws DuplicateUidException;
}
