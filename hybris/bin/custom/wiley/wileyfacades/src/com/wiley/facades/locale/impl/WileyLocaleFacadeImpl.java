package com.wiley.facades.locale.impl;

import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.locale.WileyLocaleService;
import com.wiley.facades.locale.WileyLocaleFacade;


public class WileyLocaleFacadeImpl implements WileyLocaleFacade
{

	private final WileyLocaleService wileyLocaleService;
	private final Converter<CountryModel, CountryData> countryConverter;

	@Autowired
	public WileyLocaleFacadeImpl(final WileyLocaleService wileyLocaleService,
			final Converter<CountryModel, CountryData> countryConverter)
	{
		this.wileyLocaleService = wileyLocaleService;
		this.countryConverter = countryConverter;
	}

	@Override
	public String getDefaultEncodedLocale()
	{
		return wileyLocaleService.getDefaultEncodedLocale();
	}

	@Override
	public String getCurrentEncodedLocale()
	{
		return wileyLocaleService.getCurrentEncodedLocale();
	}

	@Override
	public CountryData getCountryForEncodedLocale(final String encodedLocale)
	{
		return countryConverter.convert(wileyLocaleService.getCountryForEncodedLocale(encodedLocale));
	}

	@Override
	public void setCurrentEncodedLocale(final String encodedLocale)
	{
		wileyLocaleService.setEncodedLocale(encodedLocale);
	}
}
