package com.wiley.facades.wileyb2c.cms;

import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;
import java.util.Map;

import com.wiley.facades.cms.data.MarketingContentMenuItemData;


public interface Wileyb2cCMSFacade
{
	/**
	 * Retrieves marketing content menu items for each marketing slot on PDP page to display menu items
	 * per each slot in content menu.
	 *
	 * @param pageModel
	 * 		the page model
	 * @param productModel
	 * 		product model for retrieving its references
	 * @return map of menu items per each marketing slot
	 */
	Map<String, List<MarketingContentMenuItemData>> getMarketingContentMenu(AbstractPageModel pageModel,
			ProductModel productModel);
}
