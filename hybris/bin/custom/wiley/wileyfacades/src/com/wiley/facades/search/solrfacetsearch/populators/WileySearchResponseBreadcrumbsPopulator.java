package com.wiley.facades.search.solrfacetsearch.populators;

import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.populators.SearchResponseBreadcrumbsPopulator;


public class WileySearchResponseBreadcrumbsPopulator extends SearchResponseBreadcrumbsPopulator
{
	@Override
	protected SolrSearchQueryData cloneSearchQueryData(final SolrSearchQueryData source)
	{
		final SolrSearchQueryData target = super.cloneSearchQueryData(source);
		target.setPageType(source.getPageType());

		return target;
	}
}
