package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.enums.WileyWebLinkTypeEnum;
import com.wiley.core.model.WileyWebLinkModel;
import com.wiley.facades.wileyb2c.product.converters.populators.helper.Wileyb2cProductCollectionAttributesHelper;
import com.wiley.facades.wileyb2c.product.data.WebLinkData;


public class Wileyb2cProductCompanionSitesLinksPopulator extends AbstractWileyb2cProductLinksPopulator
{
	private Wileyb2cProductCollectionAttributesHelper wileyb2cProductCollectionAttributesHelper;

	@Override
	public void populate(final ProductModel productModel, final ProductData productData) throws ConversionException
	{
		final Collection<WileyWebLinkModel> webLinks =
				(Collection<WileyWebLinkModel>) wileyb2cProductCollectionAttributesHelper.getProductAttribute(productModel,
						ProductModel.WEBLINKS);

		if (CollectionUtils.isNotEmpty(webLinks))
		{
			productData.setStudentLinks(collectLinksOfTypes(webLinks,
					Collections.singletonList(WileyWebLinkTypeEnum.STUDENTS_COMPANION_SITE)));
			productData.setInstructorLinks(collectLinksOfTypes(webLinks,
					Arrays.asList(WileyWebLinkTypeEnum.INSTRUCTORS_COMPANION_SITE)));
			productData.setEvaluationCopyLinks(collectLinksOfTypes(webLinks,
					Arrays.asList(WileyWebLinkTypeEnum.REQUEST_EVALUATION_COPY)));
			if (!productData.getInstructorLinks().isEmpty())
			{
				productData.getInstructorLinks().addAll(collectLinksOfTypes(webLinks,
						Arrays.asList(WileyWebLinkTypeEnum.CONTACT_REPRESENTATIVE)));
			}
			productData.setRequestPermissionsLink(collectLinksOfTypes(webLinks,
					Collections.singletonList(WileyWebLinkTypeEnum.REQUEST_PERMISSION)));

			final List<WebLinkData> wileyPlusLinks =
					collectLinksOfTypes(webLinks, Collections.singletonList(WileyWebLinkTypeEnum.WILEY_PLUS));
			if (!wileyPlusLinks.isEmpty()) {
				productData.setWileyPlusLink(wileyPlusLinks.get(0));
			}
			productData.setWOLLink(collectLinksOfTypes(webLinks, Collections.singletonList(WileyWebLinkTypeEnum.WOL)));
		}
	}

	@Required
	public void setWileyb2cProductCollectionAttributesHelper(
			final Wileyb2cProductCollectionAttributesHelper wileyb2cProductCollectionAttributesHelper)
	{
		this.wileyb2cProductCollectionAttributesHelper = wileyb2cProductCollectionAttributesHelper;
	}
}
