package com.wiley.facades.populators;

import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.subscriptionfacades.data.BillingTimeData;
import de.hybris.platform.subscriptionservices.enums.TermOfServiceFrequency;
import de.hybris.platform.subscriptionservices.model.BillingTimeModel;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Required;

import com.paypal.hybris.model.PaypalPaymentInfoModel;
import com.wiley.core.model.WileyProductSummaryModel;
import com.wiley.core.model.WileySubscriptionModel;
import com.wiley.facades.product.data.WileySubscriptionData;


public class Wileyb2cSubscriptionPopulator extends WileySubscriptionPopulator
{
	private Converter<CreditCardPaymentInfoModel, CCPaymentInfoData> creditCardPaymentInfoConverter;
	private Converter<PaypalPaymentInfoModel, CCPaymentInfoData> paypalPaymentInfoConverter;

	@Resource(name = "billingTimeConverter")
	private Converter<BillingTimeModel, BillingTimeData> billingTimeDataConverter;

	@Resource(name = "addressConverter")
	private Converter<AddressModel, AddressData> addressDataConverter;

	@Resource(name = "customerConverter")
	private Converter<CustomerModel, CustomerData> customerDataConverter;

	@Override
	public void populate(final WileySubscriptionModel source, final WileySubscriptionData target) throws ConversionException
	{
		super.populate(source, target);

		target.setStartDate(source.getStartDate());
		target.setStatus(source.getStatus());
		target.setContractDurationValue(source.getContractDurationValue());
		target.setBillingPriceValue(source.getBillingPriceValue());
		target.setNextBillingDate(source.getNextBillingDate());

		final WileyProductSummaryModel productSummary =
				source.getOrderEntry() != null ? source.getOrderEntry().getProductSummary() : null;
		if (productSummary != null)
		{
			target.setName(productSummary.getName());
			target.setImage(productSummary.getPictureUrl());
		}

		final PaymentInfoModel paymentInfo = source.getPaymentInfo();
		if (paymentInfo instanceof CreditCardPaymentInfoModel)
		{
			target.setPaymentInfo(creditCardPaymentInfoConverter.convert((CreditCardPaymentInfoModel) paymentInfo));
		}
		else if (paymentInfo instanceof PaypalPaymentInfoModel)
		{
			target.setPaymentInfo(paypalPaymentInfoConverter.convert((PaypalPaymentInfoModel) paymentInfo));
		}

		final BillingTimeModel billingTime = source.getBillingFrequency();
		if (billingTime != null)
		{
			target.setBillingFrequency(billingTimeDataConverter.convert(billingTime));
		}

		final AddressModel deliveryAddress = source.getDeliveryAddress();
		if (deliveryAddress != null)
		{
			target.setDeliveryAddress(addressDataConverter.convert(source.getDeliveryAddress()));
		}

		final CustomerModel customerModel = source.getCustomer();
		if (customerModel != null)
		{
			target.setCustomer(customerDataConverter.convert(customerModel));
		}

		final CurrencyModel currencyModel = source.getBillingPriceCurrency();
		if (currencyModel != null)
		{
			target.setBillingPriceCurrency(currencyModel.getSymbol());
		}

		final TermOfServiceFrequency tosFrequency = source.getContractDurationFrequency();
		if (tosFrequency != null)
		{
			target.setContractDurationFrequency(tosFrequency.name());
		}
	}

	@Required
	public void setPaypalPaymentInfoConverter(
			final Converter<PaypalPaymentInfoModel, CCPaymentInfoData> paypalPaymentInfoConverter)
	{
		this.paypalPaymentInfoConverter = paypalPaymentInfoConverter;
	}

	@Required
	public void setCreditCardPaymentInfoConverter(
			final Converter<CreditCardPaymentInfoModel, CCPaymentInfoData> creditCardPaymentInfoConverter)
	{
		this.creditCardPaymentInfoConverter = creditCardPaymentInfoConverter;
	}
}
