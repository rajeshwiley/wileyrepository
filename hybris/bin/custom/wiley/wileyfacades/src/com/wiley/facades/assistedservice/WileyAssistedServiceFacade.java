package com.wiley.facades.assistedservice;

import de.hybris.platform.assistedservicefacades.AssistedServiceFacade;
import de.hybris.platform.assistedserviceservices.exception.AssistedServiceException;
import de.hybris.platform.core.model.user.UserModel;


/**
 * Overridden OOTB Assisted Service facade interface.
 *
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public interface WileyAssistedServiceFacade extends AssistedServiceFacade
{
	void loginAssistedServiceAgent(UserModel agent) throws AssistedServiceException;
}
