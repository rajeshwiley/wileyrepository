package com.wiley.facades.wileyplus.cart.activation.dto;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;





@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2016-08-09T10:25:39.625+02:00")

public class ErrorDto
{

	private String type = null;
	private String message = null;


	public enum ReasonEnum
	{
		INVALID("invalid"),
		MISSING("missing");

		private String value;

		ReasonEnum(final String value)
		{
			this.value = value;
		}

		@Override
		@JsonValue
		public String toString()
		{
			return value;
		}
	}

	private ReasonEnum reason = null;
	private String subject = null;
	private String subjectType = null;

	/**
	 * Type of an error occurred during processing of request
	 **/
	public ErrorDto type(final String type)
	{
		this.type = type;
		return this;
	}


	@JsonProperty("type")
	public String getType()
	{
		return type;
	}

	public void setType(final String type)
	{
		this.type = type;
	}

	/**
	 * Detailed error-related message
	 **/
	public ErrorDto message(final String message)
	{
		this.message = message;
		return this;
	}


	@JsonProperty("message")
	public String getMessage()
	{
		return message;
	}

	public void setMessage(final String message)
	{
		this.message = message;
	}

	/**
	 * Provided in case of validation errors only. Presents the kind of issue with error subject
	 **/
	public ErrorDto reason(final ReasonEnum reason)
	{
		this.reason = reason;
		return this;
	}


	@JsonProperty("reason")
	public ReasonEnum getReason()
	{
		return reason;
	}

	public void setReason(final ReasonEnum reason)
	{
		this.reason = reason;
	}

	/**
	 * Provided in case of validation errors only. Depending on whether validation issue belongs to object 
	 * or localized at some object field it could be object name or field name accordingly.
	 **/
	public ErrorDto subject(final String subject)
	{
		this.subject = subject;
		return this;
	}


	@JsonProperty("subject")
	public String getSubject()
	{
		return subject;
	}

	public void setSubject(final String subject)
	{
		this.subject = subject;
	}

	/**
	 * Provided in case of validation errors only. Values other from \"parameter\" have been reserved for internal needs.
	 **/
	public ErrorDto subjectType(final String subjectType)
	{
		this.subjectType = subjectType;
		return this;
	}


	@JsonProperty("subjectType")
	public String getSubjectType()
	{
		return subjectType;
	}

	public void setSubjectType(final String subjectType)
	{
		this.subjectType = subjectType;
	}


	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		ErrorDto errorDto = (ErrorDto) o;
		return Objects.equals(type, errorDto.type)
				&& Objects.equals(message, errorDto.message)
				&& Objects.equals(reason, errorDto.reason)
				&& Objects.equals(subject, errorDto.subject)
				&& Objects.equals(subjectType, errorDto.subjectType);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(type, message, reason, subject, subjectType);
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("class ErrorWs {\n");

		sb.append("    type: ").append(toIndentedString(type)).append("\n");
		sb.append("    message: ").append(toIndentedString(message)).append("\n");
		sb.append("    reason: ").append(toIndentedString(reason)).append("\n");
		sb.append("    subject: ").append(toIndentedString(subject)).append("\n");
		sb.append("    subjectType: ").append(toIndentedString(subjectType)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(final Object o)
	{
		if (o == null)
		{
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}

