package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;

import com.wiley.core.model.WileyPurchaseOptionProductModel;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


/**
 * Created by Sergiy_Mishkovets on 1/24/2018.
 */
public class Wileyb2cProductImprintsPopulator implements Populator<ProductModel, ProductData>
{
	private static final String IMPRINTS_SEPARATOR = ", ";

	@Value("${wiley.imprints.category.code}")
	private String imprintsCategoryCode;

	@Override
	public void populate(final ProductModel productModel, final ProductData productData) throws ConversionException
	{
		validateParameterNotNullStandardMessage("productModel", productModel);
		validateParameterNotNullStandardMessage("productData", productData);

		final List<String> imprintCategoryNames = getSuperCategoriesForProduct(productModel).stream()
				.filter(this::isImprintCategory)
				.map(category -> category.getName())
				.collect(Collectors.toList());

		productData.setImprints(StringUtils.join(imprintCategoryNames, IMPRINTS_SEPARATOR));
	}

	private Collection<CategoryModel> getSuperCategoriesForProduct(final ProductModel productModel)
	{
		if (productModel instanceof WileyPurchaseOptionProductModel)
		{
			return ((WileyPurchaseOptionProductModel) productModel).getBaseProduct().getSupercategories();
		}
		return productModel.getSupercategories();
	}

	private boolean isImprintCategory(final CategoryModel category)
	{
		return category.getAllSupercategories().stream()
				.filter(superCategory -> imprintsCategoryCode.equals(superCategory.getCode()))
				.findAny().isPresent();
	}

	public void setImprintsCategoryCode(final String imprintsCategoryCode)
	{
		this.imprintsCategoryCode = imprintsCategoryCode;
	}
}
