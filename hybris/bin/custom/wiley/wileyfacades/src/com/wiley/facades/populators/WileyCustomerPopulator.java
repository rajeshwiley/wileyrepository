package com.wiley.facades.populators;

import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.springframework.beans.factory.annotation.Required;


/**
 * Converter implementation for {@link CustomerModel} as source and
 * {@link CustomerData} as target type.
 * Populates Customer fields specific for Wiley. Delegates all other fields population.
 */
public class WileyCustomerPopulator implements Populator<CustomerModel, CustomerData>
{
	private Populator<CustomerModel, CustomerData> customerPopulator;

	private Converter<AddressModel, AddressData> addressConverter;

	/**
	 * Populates 'name', 'first name' and 'last name' fields directly from CustomerData.
	 */
	@Override
	public void populate(final CustomerModel source, final CustomerData target)
	{
		customerPopulator.populate(source, target);
		target.setName(source.getName());
		target.setFirstName(source.getFirstName());
		target.setLastName(source.getLastName());
		target.setCurrentEmployerSchool(source.getCurrentEmployerSchool());

		if (source.getDefaultPaymentAddress() != null)
		{
			target.setDefaultBillingAddress(getAddressConverter().convert(source.getDefaultPaymentAddress()));
		}

		if (source.getDefaultShipmentAddress() != null)
		{
			target.setDefaultShippingAddress(getAddressConverter().convert(source.getDefaultShipmentAddress()));
		}
	}

	public Populator<CustomerModel, CustomerData> getCustomerPopulator()
	{
		return customerPopulator;
	}

	@Required
	public void setCustomerPopulator(
			final Populator<CustomerModel, CustomerData> customerPopulator)
	{
		this.customerPopulator = customerPopulator;
	}

	public Converter<AddressModel, AddressData> getAddressConverter()
	{
		return addressConverter;
	}

	@Required
	public void setAddressConverter(
			final Converter<AddressModel, AddressData> addressConverter)
	{
		this.addressConverter = addressConverter;
	}
}
