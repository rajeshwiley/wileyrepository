package com.wiley.facades.order.converters.populator;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.impl.AbstractPopulatingConverter;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.annotation.Resource;


public class WileyProductReverseConverter extends
		AbstractPopulatingConverter<ProductData, ProductModel>
{
	@Resource
	private ProductService productService;

	@Override
	public ProductModel convert(final ProductData source)
			throws ConversionException
	{
		ProductModel target = productService.getProductForCode(source.getCode());
		superPopulate(source, target);
		return target;
	}

	protected void superPopulate(final ProductData source,
			final ProductModel target)
	{
		super.populate(source, target);
	}
}
