package com.wiley.facades.wileyb2c.product.impl;

import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;

import com.wiley.facades.wileyb2c.product.WileyCourseProductFacade;
import com.wiley.facades.wileyb2c.product.data.WileyPlusCourseInfoData;
import com.wiley.facades.wileyplus.cart.activation.dto.CartActivationRequestDto;


/**
 * Created by Anton_Lukyanau on 8/23/2016.
 */
public class WileyCourseProductFacadeImpl implements WileyCourseProductFacade
{
	@Resource
	private Converter<CartActivationRequestDto, WileyPlusCourseInfoData> wileyb2cPlusCourseInfoDataConverter;

	@Override
	public WileyPlusCourseInfoData convertToWileyInfoData(final CartActivationRequestDto cartActivationRequestDto)
	{
		return wileyb2cPlusCourseInfoDataConverter.convert(cartActivationRequestDto);
	}

}
