package com.wiley.facades.assistedservice.impl;

import de.hybris.platform.assistedservicefacades.impl.DefaultAssistedServiceFacade;
import de.hybris.platform.assistedserviceservices.exception.AssistedServiceAgentBadCredentialsException;
import de.hybris.platform.assistedserviceservices.exception.AssistedServiceException;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.exceptions.ClassMismatchException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wiley.facades.assistedservice.WileyAssistedServiceFacade;


/**
 * Overridden OOTB Assisted Service facade interface.
 *
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public class WileyAssistedServiceFacadeImpl extends DefaultAssistedServiceFacade
		implements WileyAssistedServiceFacade
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyAssistedServiceFacadeImpl.class);

	/**
	 * Overridden OOTB loginAssistedServiceAgent method to allow login without password
	 *
	 * @param agent
	 * @throws AssistedServiceException
	 */
	@Override
	public void loginAssistedServiceAgent(final UserModel agent) throws AssistedServiceException
	{
		verifyAssistedServiceMode();
		try
		{
			super.loginAssistedServiceAgent(agent);
		}
		catch (final UnknownIdentifierException | ClassMismatchException e)
		{
			LOG.debug(e.getMessage(), e);
			throw new AssistedServiceAgentBadCredentialsException("Unknown user id.");
		}
	}
}
