package com.wiley.facades.voucher;

import de.hybris.platform.commercefacades.voucher.VoucherFacade;
import de.hybris.platform.commercefacades.voucher.data.VoucherData;
import de.hybris.platform.commercefacades.voucher.exceptions.VoucherOperationException;

import java.util.Collection;


public interface WileyCouponFacade extends VoucherFacade
{
	VoucherData getVoucherForCart();

	void removeVouchersFromCart();
	
	Collection<String> removeNotValidVouchersFromCart();

	boolean canApplyNewVoucher(String newDiscountCode) throws VoucherOperationException;
}
