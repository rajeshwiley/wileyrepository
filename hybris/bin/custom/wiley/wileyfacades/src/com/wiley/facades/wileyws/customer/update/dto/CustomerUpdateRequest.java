package com.wiley.facades.wileyws.customer.update.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;


/**
 * Request for customer information update.
 */
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringMVCServerCodegen",
		date = "2016-07-26T10:03:58.259+02:00")
public class CustomerUpdateRequest
{
	@Size(max = 255)
	private String userId = null;

	@Size(max = 255)
	private String individualEcidNumber = null;

	@Size(max = 255)
	private String individualSapAccountNumber = null;

	@Size(max = 150)
	private String firstName = null;

	@Size(max = 255)
	private String lastName = null;

	@Size(max = 255)
	@Email
	private String email = null;

	/**
	 * User's unique identifier. Generally, user emails are to be used as identifiers. Nevertheless,
	 * string literals are permitted to be used as identifiers too.
	 */
	@JsonProperty("userId")
	public String getUserId()
	{
		return userId;
	}

	public void setUserId(final String userId)
	{
		this.userId = userId;
	}

	/**
	 * Actual only for B2C: customer's ECID
	 */
	@JsonProperty("individualEcidNumber")
	public String getIndividualEcidNumber()
	{
		return individualEcidNumber;
	}

	public void setIndividualEcidNumber(final String individualEcidNumber)
	{
		this.individualEcidNumber = individualEcidNumber;
	}

	/**
	 * Actual only for B2C: SAP account number.
	 */
	@JsonProperty("individualSapAccountNumber")
	public String getIndividualSapAccountNumber()
	{
		return individualSapAccountNumber;
	}

	public void setIndividualSapAccountNumber(final String individualSapAccountNumber)
	{
		this.individualSapAccountNumber = individualSapAccountNumber;
	}

	/**
	 * User's first name.
	 */
	@JsonProperty("firstName")
	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(final String firstName)
	{
		this.firstName = firstName;
	}

	/**
	 * User's last name.
	 */
	@JsonProperty("lastName")
	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(final String lastName)
	{
		this.lastName = lastName;
	}

	/**
	 * User's email.
	 */
	@JsonProperty("email")
	public String getEmail()
	{
		return email;
	}

	public void setEmail(final String email)
	{
		this.email = email;
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		CustomerUpdateRequest customerUpdateRequest = (CustomerUpdateRequest) o;
		return Objects.equals(userId, customerUpdateRequest.userId)
				&& Objects.equals(individualEcidNumber, customerUpdateRequest.individualEcidNumber)
				&& Objects.equals(individualSapAccountNumber, customerUpdateRequest.individualSapAccountNumber)
				&& Objects.equals(firstName, customerUpdateRequest.firstName)
				&& Objects.equals(lastName, customerUpdateRequest.lastName)
				&& Objects.equals(email, customerUpdateRequest.email);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(userId, individualEcidNumber, individualSapAccountNumber, firstName, lastName, email);
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("class CustomerUpdateRequest {\n");
		sb.append("  userId: ").append(userId).append("\n");
		sb.append("  individualEcidNumber: ").append(individualEcidNumber).append("\n");
		sb.append("  individualSapAccountNumber: ").append(individualSapAccountNumber).append("\n");
		sb.append("  firstName: ").append(firstName).append("\n");
		sb.append("  lastName: ").append(lastName).append("\n");
		sb.append("  email: ").append(email).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
