package com.wiley.facades.process.email.context;

/**
 * Contains more specific stuff for generation b2c order confirmation emails
 */
public class Wileyb2cOrderNotificationEmailContext extends OrderNotificationEmailContext
{
}
