package com.wiley.facades.wiley.order;

import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.CartRestorationData;
import de.hybris.platform.commerceservices.order.CommerceCartRestorationException;


public interface WileyCartFacade extends CartFacade
{
	/**
	 * Restores the user's saved cart to the session. For non-anonymous users, the cart restored will be latest modified cart
	 * of general type for the current user and site.
	 *
	 * @return the cart restoration data that includes details of any items that could not be restored in part or in
	 * full.
	 */
	CartRestorationData restoreSavedCart() throws CommerceCartRestorationException;

	/**
	 * Switches session cart to latest modified cart of general type for the current user and site.
	 */
	void switchSessionCartForUser();

	/**
	 * Switches session cart to cart with specified GUID for current user and site.
	 *
	 * @return true if cart exists.
	 */
	boolean switchSessionCart(String guid);

	/**
	 * @return true if session session cart for current user and site is with specified GUID .
	 */
	boolean hasSessionCartWithGuid(String guid);

	/**
	 * @return true if payPal button should be visible on the cart page.
	 */
	boolean isPayPalAvailableForCurrentCart();

	/**
	 * @return true if discounts block should be visible on the cart page.
	 */
	boolean isDiscountsAvailableForCurrentCart();

	/**
	 * @return true if remove button should be visible on the cart page.
	 */
	boolean isRemoveButtonAvailableForCurrentCart();

	boolean isSessionCartWithProductsFromCategory(String categoryCode);

	String getLatestGeneralCartGuid();
}
