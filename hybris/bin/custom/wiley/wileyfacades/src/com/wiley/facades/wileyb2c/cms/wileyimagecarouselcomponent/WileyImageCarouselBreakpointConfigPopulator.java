package com.wiley.facades.wileyb2c.cms.wileyimagecarouselcomponent;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import com.wiley.core.model.WileyImageCarouselBreakpointConfigurationModel;
import com.wiley.facades.wileyb2c.cms.WileyImageCarouselBreakpointConfigData;


public class WileyImageCarouselBreakpointConfigPopulator
		implements Populator<WileyImageCarouselBreakpointConfigurationModel, WileyImageCarouselBreakpointConfigData>
{
	@Override
	public void populate(final WileyImageCarouselBreakpointConfigurationModel source,
			final WileyImageCarouselBreakpointConfigData target) throws ConversionException
	{
		target.setItems(source.getItems());
		target.setMargin(source.getMargin());
		target.setLoop(source.getLoop());
		target.setCenter(source.getCenter());
		target.setMouseDrag(source.getMouseDrag());
		target.setTouchDrag(source.getTouchDrag());
		target.setPullDrag(source.getPullDrag());
		target.setFreeDrag(source.getFreeDrag());
		target.setStagePadding(source.getStagePadding());
		target.setNav(source.getNav());
		target.setRewind(source.getRewind());
		target.setDots(source.getDots());
		target.setAutoplay(source.getAutoplay());
		target.setAutoplayTimeout(source.getAutoplayTimeout());
	}
}
