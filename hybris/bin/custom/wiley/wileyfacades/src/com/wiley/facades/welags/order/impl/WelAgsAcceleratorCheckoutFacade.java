package com.wiley.facades.welags.order.impl;

import de.hybris.platform.acceleratorfacades.order.impl.DefaultAcceleratorCheckoutFacade;
import de.hybris.platform.assistedservicefacades.AssistedServiceFacade;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.voucher.VoucherService;

import org.springframework.beans.factory.annotation.Autowired;


public class WelAgsAcceleratorCheckoutFacade extends DefaultAcceleratorCheckoutFacade
{
	@Autowired
	private AssistedServiceFacade assistedServiceFacade;

	@Autowired
	private VoucherService voucherService;

	@Override
	/** Overridden to call voucherService, that removes vouchers from cart, but redeem the same vouchers to order */
	protected void afterPlaceOrder(final CartModel cartModel, final OrderModel orderModel)
	{
		if (assistedServiceFacade.isAssistedServiceAgentLoggedIn())
		{
			orderModel.setPlacedBy(getCurrentUserForCheckout());
		}
		voucherService.afterOrderCreation(orderModel, cartModel);
		super.afterPlaceOrder(cartModel, orderModel);
	}

	@Override
	public boolean hasValidCart()
	{
		if (getCartService().hasSessionCart())
		{
			CartModel cart = getCartService().getSessionCart();
			return cart.getEntries() != null && !cart.getEntries().isEmpty();
		}

		return false;
	}
}
