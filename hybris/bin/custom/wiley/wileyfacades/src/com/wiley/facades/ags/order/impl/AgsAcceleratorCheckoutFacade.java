package com.wiley.facades.ags.order.impl;

import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.converters.Converters;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.wiley.facades.welags.order.impl.WelAgsAcceleratorCheckoutFacade;


/**
 * Created on 1/20/2016.
 */
public class AgsAcceleratorCheckoutFacade extends WelAgsAcceleratorCheckoutFacade
{
	private Comparator<CountryData> comparator;

	@Override
	public List<CountryData> getBillingCountries()
	{
		final List<CountryData> countries = Converters.convertAll(getCommonI18NService().getAllCountries(),
				getCountryConverter());
		Collections.sort(countries, comparator);
		return countries;
	}

	public void setComparator(final Comparator<CountryData> comparator)
	{
		this.comparator = comparator;
	}

	public Comparator<CountryData> getComparator()
	{
		return comparator;
	}
}
