package com.wiley.facades.as.product.converters.populator;

import de.hybris.platform.commercefacades.product.converters.populator.AbstractProductImagePopulator;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ImageDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Sergiy_Mishkovets on 2/20/2018.
 */
public class WileyasProductPrimaryImagePopulator extends AbstractProductImagePopulator<ProductModel, ProductData>
{
	@Override
	public void populate(final ProductModel source, final ProductData target) throws ConversionException
	{
		ServicesUtil.validateParameterNotNullStandardMessage("productModel", source);
		ServicesUtil.validateParameterNotNullStandardMessage("productData", target);

		final List<ImageData> images = new ArrayList<>();
		final MediaModel picture = getPicture(source);
		if (picture != null)
		{
			images.add(convertMedia(picture, ImageDataType.PRIMARY));
		}
		target.setImages(images);

	}

	private ImageData convertMedia(final MediaModel picture, final ImageDataType imageDataType)
	{
		ImageData imageData = getImageConverter().convert(picture);
		imageData.setImageType(imageDataType);
		return imageData;
	}

	private MediaModel getPicture(final ProductModel product)
	{
		if (product instanceof VariantProductModel)
		{
			return ((VariantProductModel) product).getBaseProduct().getPicture();
		}
		return product.getPicture();
	}
}
