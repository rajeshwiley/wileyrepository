package com.wiley.facades.wileyb2c.populator;

import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.subscriptionfacades.converters.populator.SubscriptionTermPopulator;
import de.hybris.platform.subscriptionfacades.data.SubscriptionTermData;
import de.hybris.platform.subscriptionservices.model.SubscriptionTermModel;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.model.WileyProductModel;
import com.wiley.core.model.WileySubscriptionModel;
import com.wiley.core.wileyb2c.product.Wileyb2cCommercePriceService;
import com.wiley.facades.product.data.WileySubscriptionData;
import com.wiley.facades.wileyb2c.populator.comparator.SubscriptionTermDataComparator;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


/**
 * Created by Oleg_Dolgopolov on 10/5/2016.
 */
public class Wileyb2cAvailableUpgradeSubscriptionTermsPopulator implements Populator<WileySubscriptionModel,
		WileySubscriptionData>
{

	private ProductService productService;
	private SubscriptionTermPopulator subscriptionTermPopulator;
	private Wileyb2cCommercePriceService priceService;
	private PriceDataFactory priceDataFactory;

	@Override
	public void populate(@NotNull final WileySubscriptionModel source, @NotNull final WileySubscriptionData target)
			throws ConversionException
	{
		validateParameterNotNullStandardMessage("source", source);
		validateParameterNotNullStandardMessage("target", target);

		final SubscriptionTermModel currentTerm =
				source.getOrderEntry() != null ? source.getOrderEntry().getSubscriptionTerm() : null;

		final ProductModel productModel = productService.getProductForCode(target.getProductCode());
		if (!(productModel instanceof WileyProductModel))
		{
			return;
		}

		final WileyProductModel product = (WileyProductModel) productModel;
		final Collection<SubscriptionTermModel> subscriptionTerms = product.getSubscriptionTerms();
		final List<SubscriptionTermData> availableSubscriptionTerms = new LinkedList<>();

		for (SubscriptionTermModel subscriptionTermModel : subscriptionTerms)
		{
			if (subscriptionTermModel.equals(currentTerm))
			{
				continue;
			}

			final List<PriceInformation> prices = priceService.findPricesByProductAndSubscriptionTerm(product,
					subscriptionTermModel);
			if (CollectionUtils.isNotEmpty(prices))
			{
				for (final PriceInformation priceInformation : prices)
				{
					final PriceData priceData = priceDataFactory.create(PriceDataType.FROM,
							new BigDecimal(priceInformation.getPriceValue().getValue()), priceInformation.getPriceValue()
									.getCurrencyIso());

					final SubscriptionTermData subscriptionTermData = new SubscriptionTermData();
					subscriptionTermPopulator.populate(subscriptionTermModel, subscriptionTermData);
					subscriptionTermData.setId(subscriptionTermModel.getId());
					subscriptionTermData.setPrice(priceData);
					availableSubscriptionTerms.add(subscriptionTermData);
				}
			}
		}

		Collections.sort(availableSubscriptionTerms, new SubscriptionTermDataComparator());
		target.setAvailableUpgradeSubscriptionTerms(availableSubscriptionTerms);
	}

	@Required
	public void setProductService(final ProductService productService)
	{
		this.productService = productService;
	}

	@Required
	public void setSubscriptionTermPopulator(final SubscriptionTermPopulator subscriptionTermPopulator)
	{
		this.subscriptionTermPopulator = subscriptionTermPopulator;
	}

	@Required
	public void setPriceService(final Wileyb2cCommercePriceService priceService)
	{
		this.priceService = priceService;
	}

	@Required
	public void setPriceDataFactory(final PriceDataFactory priceDataFactory)
	{
		this.priceDataFactory = priceDataFactory;
	}

}
