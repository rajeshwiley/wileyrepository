package com.wiley.facades.payment.paypal.hybris.converters.populators.impl;

import com.paypal.hybris.converters.populators.impl.SetExprCheckoutReqPaymentDetailsPopulator;
import de.hybris.platform.commercefacades.order.data.CartData;

/**
 * Created by georgii gavrysh on 1/6/2017.
 */
public class WileySetExprCheckoutReqPaymentDetailsPopulator extends SetExprCheckoutReqPaymentDetailsPopulator {
    @Override
    protected boolean isDigitalCart(final CartData cart) {
        return cart.isDigitalCart();
    }
}
