package com.wiley.facades.welags.pin.impl;

import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.price.DiscountModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Arrays;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;

import com.wiley.core.model.PinModel;
import com.wiley.core.pin.service.PinService;
import com.wiley.facades.welags.pin.exception.PinOperationException;
import com.wiley.facades.welags.pin.exception.PinOperationExceptionReason;
import com.wiley.facades.welags.pin.PinFacade;

import static com.wiley.facades.welags.pin.exception.PinOperationExceptionReason.ALREADY_ACTIVATED_PIN;
import static com.wiley.facades.welags.pin.exception.PinOperationExceptionReason.INVALID_CART;
import static com.wiley.facades.welags.pin.exception.PinOperationExceptionReason.INVALID_ORDER;
import static com.wiley.facades.welags.pin.exception.PinOperationExceptionReason.INVALID_PIN;
import static com.wiley.facades.welags.pin.exception.PinOperationExceptionReason.INVALID_PRODUCT;


/**
 * Default implementation of {@link PinFacade}
 *
 * @author Gabor_Bata
 */
public class DefaultPinFacade implements PinFacade
{
	@Resource
	private PinService pinService;

	@Resource
	private ModelService modelService;

	@Resource
	private ProductService productService;

	private CommerceCartService commerceCartService;

	@Resource
	private AddToCartStrategyProvider addToCartStrategyProvider;

	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Resource
	private UserService userService;

	@Override
	public void applyPin(final String cartCode, final String pinCode) throws PinOperationException
	{
		CartModel cartModel = getOrderModelForCode(CartModel.class, cartCode);
		PinModel pinModel = getPinModel(pinCode);
		validatePin(pinModel);

		ProductModel productModel = getProduct(pinModel.getProductCode());
		validateProduct(productModel);

		try
		{
			addToCartStrategyProvider.getStrategyForPinWorkFlow().addProductToCart(cartModel, pinModel);
		}
		catch (UnsupportedOperationException e)
		{
			throw new PinOperationException(PinOperationExceptionReason.INVALID_CART, e);
		}

		applyDiscountForCart(cartModel, pinModel.getCode());

		final CommerceCartParameter parameter = new CommerceCartParameter();
		parameter.setEnableHooks(true);
		parameter.setCart(cartModel);
		commerceCartService.calculateCart(parameter);
	}

	@Override
	public void activatePin(final String orderCode, final String pinCode) throws PinOperationException
	{
		OrderModel orderModel = getOrderModelForCode(OrderModel.class, orderCode);
		PinModel pinModel = getPinModel(pinCode);
		validatePin(pinModel);
		validateOrder(orderModel);
		pinModel.setOrder(orderModel);
		modelService.save(pinModel);
	}

	private PinModel getPinModel(final String pinCode) throws PinOperationException
	{
		try
		{
			return pinService.getPinForCode(pinCode);
		}
		catch (UnknownIdentifierException | IllegalArgumentException e)
		{
			throw new PinOperationException(INVALID_PIN, e);
		}
	}

	private void validatePin(final PinModel pinModel) throws PinOperationException
	{
		if (Boolean.TRUE == pinModel.getActivated())
		{
			throw new PinOperationException(ALREADY_ACTIVATED_PIN);
		}
	}

	private ProductModel getProduct(final String productCode) throws PinOperationException
	{
		try
		{
			return productService.getProductForCode(productCode);
		}
		catch (IllegalArgumentException | UnknownIdentifierException | AmbiguousIdentifierException e)
		{
			throw new PinOperationException(INVALID_PRODUCT, e);
		}
	}

	private void validateProduct(final ProductModel productModel) throws PinOperationException
	{
		if (productModel.getApprovalStatus() != ArticleApprovalStatus.APPROVED)
		{
			throw new PinOperationException(INVALID_PRODUCT);
		}
	}

	private void validateOrder(final OrderModel orderModel) throws PinOperationException
	{
		if (CollectionUtils.isEmpty(orderModel.getEntries()))
		{
			throw new PinOperationException(INVALID_ORDER);
		}
	}

	private void applyDiscountForCart(final CartModel cartModel, final String pinCode)
	{
	  final DiscountModel discount = modelService.create(DiscountModel.class);
	  discount.setCode(pinCode);
	  discount.setValue(100.0);
	  cartModel.setDiscounts(Arrays.asList(discount));
	  modelService.saveAll();
	}

	private <T extends AbstractOrderModel> T getOrderModelForCode(final Class<T> orderModelClass, final String orderModelCode)
			throws PinOperationException
	{
		T orderModelExample = null;
		try
		{
			orderModelExample = orderModelClass.newInstance();
		}
		catch (InstantiationException | IllegalAccessException e)
		{
			throw new PinOperationException(orderModelClass.isAssignableFrom(CartModel.class) ? INVALID_CART : INVALID_ORDER, e);
		}
		orderModelExample.setCode(orderModelCode);
		orderModelExample.setUser(userService.getCurrentUser());
		return flexibleSearchService.getModelByExample(orderModelExample);
	}

	public CommerceCartService getCommerceCartService()
	{
		return commerceCartService;
	}

	public void setCommerceCartService(final CommerceCartService commerceCartService)
	{
		this.commerceCartService = commerceCartService;
	}
}
