package com.wiley.facades.suggestion.impl;

import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.wiley.restriction.WileyProductRestrictionService;


public class WileyasSimpleSuggestionFacadeImpl extends DefaultSimpleSuggestionFacade
{
	@Autowired
	private WileyProductRestrictionService wileyProductRestrictionService;

	@Autowired
	private SessionService sessionService;

	@Autowired
	private CommonI18NService commonI18NService;

	@Override
	public List<ProductData> getSuggestionsForProductsInCart(final List<ProductReferenceTypeEnum> referenceTypes,
			final boolean excludePurchased, final Integer limit)
	{
		if (getCartService().hasSessionCart())
		{
			final Set<ProductModel> products = new HashSet<>();
			final CartModel sessionCart = getCartService().getSessionCart();
			for (final AbstractOrderEntryModel entry : sessionCart.getEntries())
			{
				products.addAll(getAllBaseProducts(entry.getProduct()));
			}
			final List<ProductModel> suggestions = getVisibleProducts(getSimpleSuggestionService().getReferencesForProducts(
					new LinkedList<>(products), referenceTypes, getUserService().getCurrentUser(), excludePurchased, limit));

			if (CollectionUtils.isEmpty(suggestions))
			{
				return Collections.emptyList();
			}

			return sessionService.executeInLocalView(new SessionExecutionBody()
			{
				@Override
				public List<ProductData> execute()
				{
					commonI18NService.setCurrentCurrency(sessionCart.getCurrency());
					return Converters.convertAll(suggestions, getProductConverter());
				}
			}, getUserService().getCurrentUser());
		}
		return Collections.emptyList();
	}

	private List<ProductModel> getVisibleProducts(final List<ProductModel> suggestions)
	{
		return suggestions.stream().filter(wileyProductRestrictionService::isVisible).collect(Collectors.toList());
	}
}
