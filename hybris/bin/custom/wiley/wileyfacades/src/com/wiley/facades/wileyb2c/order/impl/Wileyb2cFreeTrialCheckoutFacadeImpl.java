package com.wiley.facades.wileyb2c.order.impl;

import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.order.InvalidCartException;

import javax.annotation.Resource;

import com.wiley.core.wileyb2c.order.Wileyb2cFreeTrialCheckoutService;
import com.wiley.facades.wileyb2c.order.Wileyb2cFreeTrialCheckoutFacade;


/**
 * Created by Uladzimir_Barouski on 9/20/2016.
 */
public class Wileyb2cFreeTrialCheckoutFacadeImpl implements Wileyb2cFreeTrialCheckoutFacade
{
	@Resource
	private Wileyb2cFreeTrialCheckoutService wileyb2cFreeTrialCheckoutService;

	@Override
	public String placeFreeTrialOrder(final String code)
			throws CommerceCartModificationException, InvalidCartException
	{
		return wileyb2cFreeTrialCheckoutService.placeFreeTrialOrder(code);
	}
}
