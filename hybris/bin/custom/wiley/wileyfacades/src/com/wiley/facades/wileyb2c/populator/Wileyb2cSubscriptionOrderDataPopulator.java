package com.wiley.facades.wileyb2c.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import javax.annotation.Nonnull;

import com.wiley.core.model.WileySubscriptionModel;
import com.wiley.facades.product.data.WileySubscriptionData;


public class Wileyb2cSubscriptionOrderDataPopulator implements Populator<WileySubscriptionModel,
		WileySubscriptionData>
{

	@Override
	public void populate(@Nonnull final WileySubscriptionModel wileySubscriptionModel,
			@Nonnull final WileySubscriptionData wileySubscriptionData) throws ConversionException
	{
		ServicesUtil.validateParameterNotNullStandardMessage("wileySubscriptionModel", wileySubscriptionModel);
		ServicesUtil.validateParameterNotNullStandardMessage("wileySubscriptionData", wileySubscriptionData);

		final AbstractOrderModel order = getOrder(wileySubscriptionModel);
		if (order != null)
		{
			wileySubscriptionData.setOrderCode(order.getCode());
			wileySubscriptionData.setOrderDate(order.getCreationtime());
		}
	}

	private AbstractOrderModel getOrder(final WileySubscriptionModel subscription)
	{
		AbstractOrderModel order = null;
		final AbstractOrderEntryModel orderEntry = subscription.getOrderEntry();
		if (orderEntry != null)
		{
			order = orderEntry.getOrder();
		}
		return order;
	}
}