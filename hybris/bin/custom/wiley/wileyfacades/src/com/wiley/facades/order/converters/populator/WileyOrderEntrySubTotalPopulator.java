package com.wiley.facades.order.converters.populator;

import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.springframework.beans.factory.annotation.Required;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class WileyOrderEntrySubTotalPopulator implements Populator<AbstractOrderEntryModel, OrderEntryData>
{
    @Resource
    private PriceDataFactory wileyPriceDataFactory;

    @Override
    public void populate(final AbstractOrderEntryModel source, final OrderEntryData target)
            throws ConversionException
    {
        if (source.getSubtotalPrice() != null)
        {
            target.setSubtotalPrice(
                    getWileyPriceDataFactory().create(
                            PriceDataType.BUY,
                            new BigDecimal(source.getSubtotalPrice()).setScale(2, RoundingMode.HALF_UP),
                            source.getOrder().getCurrency()));
        }
    }

    public PriceDataFactory getWileyPriceDataFactory()
    {
        return wileyPriceDataFactory;
    }

    @Required
    public void setWileyPriceDataFactory(final PriceDataFactory wileyPriceDataFactory)
    {
        this.wileyPriceDataFactory = wileyPriceDataFactory;
    }
}
