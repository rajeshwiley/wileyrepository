package com.wiley.facades.as.address;

import de.hybris.platform.commercefacades.address.AddressVerificationFacade;


public interface WileyasAddressVerificationFacade extends AddressVerificationFacade
{
	boolean shouldCallSuggestionDoctor(String paymentMode);
	boolean shouldCallCheckoutCartAddressValidation();
}
