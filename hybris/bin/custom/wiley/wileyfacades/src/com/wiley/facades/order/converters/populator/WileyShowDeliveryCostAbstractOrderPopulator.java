package com.wiley.facades.order.converters.populator;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.wiley.core.product.WileyProductEditionFormatService;


/**
 * Populates a flag which means should we display or not delivery cost.
 */
public class WileyShowDeliveryCostAbstractOrderPopulator implements Populator<AbstractOrderModel, AbstractOrderData>
{

	@Resource
	private WileyProductEditionFormatService wileyProductEditionFormatService;

	@Override
	public void populate(@Nonnull final AbstractOrderModel source, @Nonnull final AbstractOrderData target)
			throws ConversionException
	{
		Assert.notNull(source);
		Assert.notNull(target);

		target.setShowDeliveryCost(calculateShowDeliveryCost(source, target));
	}

	/**
	 * Calculates the 'showDeliveryCost' flag, which defines if we need to show delivery cost section on storefront
	 * @param source
	 * @param target
	 * @return should/should not
	 */
	protected boolean calculateShowDeliveryCost(final AbstractOrderModel source, final AbstractOrderData target)
	{
		return !wileyProductEditionFormatService.isDigitalCart(source);
	}
}
