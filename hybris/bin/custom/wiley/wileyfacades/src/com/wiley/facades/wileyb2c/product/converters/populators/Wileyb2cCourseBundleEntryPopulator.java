package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.catalog.references.ProductReferenceService;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commerceservices.price.CommercePriceService;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.CoreAlgorithms;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.math.BigDecimal;
import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.wiley.core.enums.WileyProductSubtypeEnum;
import com.wiley.facades.wileyb2c.product.data.CourseBundleData;


/**
 * Created by Uladzimir_Barouski on 8/24/2016.
 */
public class Wileyb2cCourseBundleEntryPopulator implements Populator<ProductModel, CourseBundleData>
{
	private static final String MAIN_COMPONENT_TITLE_KEY =
			"wileypluscourse.purchase.option.instant-access.discounted-list.item-description";
	private static final String EBOOK_COMPONENT_TITLE_KEY = "ebook";
	private static final String LOOSELEAF_COMPONENT_TITLE_KEY = "looseleaf";
	private static final String EBOOK_ALT_TEXT_KEY = "wileypluscourse.purchase.option.instant-access.product.ebook.alt.text";
	private static final String LOOSELEAF_ALT_TEXT_KEY =
			"wileypluscourse.purchase.option.instant-access.product.looseleaf.alt.text";

	private static final int MAX_AMOUNT_OF_SET_COMPONENTS = 2; //by business requirements for product references


	@Resource
	private ProductReferenceService productReferenceService;

	@Resource
	private CommercePriceService wileyb2cCommercePriceService;

	@Resource
	private PriceDataFactory priceDataFactory;

	@Override
	public void populate(final ProductModel source, final CourseBundleData target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		Collection<ProductReferenceModel> productReferenceList = productReferenceService.getProductReferencesForSourceProduct(
				source,
				ProductReferenceTypeEnum.PRODUCT_SET_COMPONENT, true);
		if (productReferenceList.size() > MAX_AMOUNT_OF_SET_COMPONENTS)
		{
			throw new ConversionException(String.format("Product set %s contains more than 2 bundles", source.getCode()));
		}
		for (ProductReferenceModel productRef : productReferenceList)
		{
			ProductModel productModel = productRef.getTarget();
			PriceData componentPriceData = getPriceData(productModel);
			if (WileyProductSubtypeEnum.COURSE.equals(productModel.getSubtype()))
			{
				target.setMainComponentName(MAIN_COMPONENT_TITLE_KEY);
				target.setMainComponentPrice(componentPriceData);
			}
			else
			{
				populateExtraComponentData(source, target, productModel, componentPriceData);
			}
		}
		target.setBundleCode(source.getCode());

		PriceData fullSetPriceData = getPriceData(source);
		target.setDiscountedPriceValue(fullSetPriceData);

		PriceData savingPriceData = calculateDiscountedPriceSavings(fullSetPriceData, target.getMainComponentPrice(),
				target.getExtraComponentPrice());
		target.setDiscountedPriceSaving(savingPriceData);

		PriceData extraSavingPriceData = calculateExtraComponentSavings(target.getExtraComponentPrice(),
				target.getDiscountedPriceSaving());
		target.setExtraComponentSaving(extraSavingPriceData);
	}

	private void populateExtraComponentData(final ProductModel productSetModel, final CourseBundleData target,
			final ProductModel productModel, final PriceData componentPriceData)
	{
		target.setExtraComponentPrice(componentPriceData);
		if (WileyProductSubtypeEnum.EBOOK.equals(productModel.getSubtype()))
		{
			target.setExtraComponentNameKey(EBOOK_COMPONENT_TITLE_KEY);
			target.setExtraComponentAlttextKey(EBOOK_ALT_TEXT_KEY);
			target.setMostPopular(true);
		}
		else if (WileyProductSubtypeEnum.LOOSE_LEAF.equals(productModel.getSubtype()))
		{
			target.setExtraComponentNameKey(LOOSELEAF_COMPONENT_TITLE_KEY);
			target.setExtraComponentAlttextKey(LOOSELEAF_ALT_TEXT_KEY);
			target.setMostPopular(false);
		}
		else
		{
			throw new ConversionException(
					String.format("Product set %s contains unsupportable component %s with type %s", productSetModel.getCode(),
							productModel.getCode(), productModel.getSubtype().getCode()));
		}
	}

	private PriceData getPriceData(final ProductModel productModel)
	{
		final PriceInformation info = wileyb2cCommercePriceService.getWebPriceForProduct(productModel);
		final PriceData priceData = priceDataFactory.create(PriceDataType.BUY,
				BigDecimal.valueOf(info.getPriceValue().getValue()),
				info.getPriceValue().getCurrencyIso());
		return priceData;
	}

	private PriceData calculateDiscountedPriceSavings(final PriceData productSetPrice, final PriceData mainComponentPrice,
			final PriceData extraComponentPrice)
	{
		double savingPriceValue =
				mainComponentPrice.getValue().doubleValue() + extraComponentPrice.getValue().doubleValue() - productSetPrice
						.getValue().doubleValue();
		savingPriceValue = CoreAlgorithms.round(savingPriceValue, 2);
		PriceData savingPrice = priceDataFactory.create(mainComponentPrice.getPriceType(), BigDecimal.valueOf(savingPriceValue),
				mainComponentPrice.getCurrencyIso());
		return savingPrice;
	}


	private PriceData calculateExtraComponentSavings(final PriceData extraComponentPrice, final PriceData discountedPriceSaving)
	{
		double savingPriceValue =
				extraComponentPrice.getValue().doubleValue() - discountedPriceSaving.getValue().doubleValue();
		savingPriceValue = CoreAlgorithms.round(savingPriceValue, 2);
		PriceData savingPrice = priceDataFactory.create(extraComponentPrice.getPriceType(), BigDecimal.valueOf(savingPriceValue),
				extraComponentPrice.getCurrencyIso());
		return savingPrice;
	}
}
