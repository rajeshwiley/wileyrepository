package com.wiley.facades.payment;

import de.hybris.platform.acceleratorfacades.payment.PaymentFacade;
import de.hybris.platform.acceleratorfacades.payment.data.PaymentSubscriptionResultData;
import de.hybris.platform.acceleratorservices.payment.data.PaymentData;
import de.hybris.platform.commercefacades.user.data.AddressData;

import javax.annotation.Nonnull;
import java.util.Map;


public interface WileyPaymentFacade extends PaymentFacade
{
	PaymentData beginHopValidatePayment(String responseUrl, String merchantCallbackUrl);
	PaymentData beginHopValidatePayment(String responseUrl, String merchantCallbackUrl,
										AddressData billingAddress);


	/**
	 * Begins HOP validate payment with the address associated with the given address id.
	 *
	 * @param responseUrl the response URL
	 * @param merchantCallbackUrl the merchant callback URL
	 * @param addressId the address id
	 * @return the payment data
	 */
	PaymentData beginHopValidatePayment(String responseUrl, String merchantCallbackUrl,
			@Nonnull String addressId);

	PaymentSubscriptionResultData completeHopValidatePayment(Map<String, String> parameters,
			boolean saveInAccount);

}
