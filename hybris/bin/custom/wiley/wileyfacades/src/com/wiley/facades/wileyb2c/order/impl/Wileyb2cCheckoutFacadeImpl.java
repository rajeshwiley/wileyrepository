package com.wiley.facades.wileyb2c.order.impl;

import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.MapUtils;

import com.paypal.hybris.model.PaypalPaymentInfoModel;
import com.wiley.core.product.data.ExternalInventoryStatus;
import com.wiley.core.wileyb2c.order.Wileyb2cExternalInventoryService;
import com.wiley.facades.wileycom.order.WileycomCheckoutFacadeImpl;
import com.wiley.facades.wileyb2c.order.Wileyb2cCheckoutFacade;
import com.wiley.facades.wileyb2c.order.converters.populator.Wileyb2cOrderEntryExternalInventoryStatusPopulator;


/**
 * Checkout facade for b2cstorefront.
 */
public class Wileyb2cCheckoutFacadeImpl extends WileycomCheckoutFacadeImpl implements Wileyb2cCheckoutFacade
{
	@Resource
	private Wileyb2cExternalInventoryService wileyb2cExternalInventoryService;

	@Resource(name = "wileyb2cOrderEntryExternalInventoryStatusPopulator")
	private Wileyb2cOrderEntryExternalInventoryStatusPopulator externalInventoryStatusPopulator;

	@Override
	public void doRealTimeInventoryCheck(final CartData cartData)
	{
		final Map<String, ExternalInventoryStatus> productsInventoryStatuses =
				wileyb2cExternalInventoryService.doRealTimeInventoryCheck();

		if (MapUtils.isNotEmpty(productsInventoryStatuses))
		{
			final List<OrderEntryData> entries = cartData.getEntries();
			for (final OrderEntryData entry : entries)
			{
				final ProductData product = entry.getProduct();
				/**
				 * Inventory service doesn't process digital product, hence we should stay availability status as it was
				 * prior to inventory call
				 */
				if (!product.isIsDigital())
				{
					final ExternalInventoryStatus externalInventoryStatus = productsInventoryStatuses.
							get(product.getSapProductCode());
					externalInventoryStatusPopulator.populate(externalInventoryStatus, entry);
				}
			}
		}
	}

	@Override
	public void removePaypalPaymentInformationFromSessionCart()
	{
		CartModel cartModel = getCart();
		PaymentInfoModel paymentInfoModel = cartModel.getPaymentInfo();
		if (paymentInfoModel != null && paymentInfoModel instanceof PaypalPaymentInfoModel)
		{
			setPaymentAddress((AddressModel) null);
			getModelService().remove(paymentInfoModel);
			getModelService().refresh(cartModel);
		}
	}
}
