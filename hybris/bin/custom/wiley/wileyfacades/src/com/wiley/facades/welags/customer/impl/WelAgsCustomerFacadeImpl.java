package com.wiley.facades.welags.customer.impl;

import com.wiley.facades.welags.order.WelAgsCheckoutFacade;
import de.hybris.platform.commercefacades.customer.impl.DefaultCustomerFacade;
import de.hybris.platform.commercefacades.storesession.data.CurrencyData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.exceptions.CalculationException;

import java.util.Collection;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import com.wiley.core.customer.ResetPasswordRedirectType;
import com.wiley.core.customer.service.WileyCustomerAccountService;
import com.wiley.core.storesession.WileyStoreSessionService;
import com.wiley.facades.storesession.WileyStoreSessionFacade;
import com.wiley.facades.welags.customer.WelAgsCustomerFacade;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


/**
 * The implementation disables unnecessary validation of the customer's title.
 *
 * Created by Mikhail_Kondratyev on 05-Dec-15.
 */
public class WelAgsCustomerFacadeImpl extends DefaultCustomerFacade implements WelAgsCustomerFacade
{

	private static final Logger LOG = Logger.getLogger(WelAgsCustomerFacadeImpl.class);

	@Autowired
	private WileyStoreSessionFacade wileyStoreSessionFacade;

	@Autowired
	private WileyCustomerAccountService customerAccountService;

	private WelAgsCheckoutFacade checkoutFacade;

	
	@Override
	public void register(final RegisterData registerData) throws DuplicateUidException
	{
		validateParameterNotNullStandardMessage("registerData", registerData);
		Assert.hasText(registerData.getFirstName(), "The field [FirstName] cannot be empty");
		Assert.hasText(registerData.getLastName(), "The field [LastName] cannot be empty");
		Assert.hasText(registerData.getLogin(), "The field [Login] cannot be empty");

		final CustomerModel newCustomer = getModelService().create(CustomerModel.class);
		newCustomer.setFirstName(registerData.getFirstName());
		newCustomer.setLastName(registerData.getLastName());

		setUidForRegister(registerData, newCustomer);
		newCustomer.setSessionLanguage(getCommonI18NService().getCurrentLanguage());
		newCustomer.setSessionCurrency(getCommonI18NService().getCurrentCurrency());
		getCustomerAccountService().register(newCustomer, registerData.getPassword());
	}

	@Override
	protected void validateDataBeforeUpdate(final CustomerData customerData)
	{
		validateParameterNotNullStandardMessage("customerData", customerData);
		Assert.hasText(customerData.getFirstName(), "The field [FirstName] cannot be empty");
		Assert.hasText(customerData.getLastName(), "The field [LastName] cannot be empty");
		Assert.hasText(customerData.getUid(), "The field [Uid] cannot be empty");
	}

	@Override
	public void forgottenPassword(final String uid, final ResetPasswordRedirectType type)
	{
		Assert.hasText(uid, "The field [uid] cannot be empty");
		Assert.notNull(type, "The field [type] cannot be null");
		final CustomerModel customerModel = getUserService().getUserForUID(uid.toLowerCase(),
				CustomerModel.class);
		customerAccountService.forgottenPassword(customerModel, type);
	}

	@Override
	public void updateCustomerProfile(final CustomerData customerData) throws DuplicateUidException
	{
		validateDataBeforeUpdate(customerData);

		final String name = getCustomerNameStrategy().getName(customerData.getFirstName(),
				customerData.getLastName());
		final CustomerModel customer = getCurrentSessionCustomer();
		customer.setName(name);
		customer.setOriginalUid(customerData.getDisplayUid());
		customer.setUid(customerData.getDisplayUid());
		customer.setFirstName(customerData.getFirstName());
		customer.setLastName(customerData.getLastName());
		customerAccountService.updateCustomerProfile(customer);
	}

	/**
	 * Emulated OOTB behaviour from {@lunk DefaultCustomerFacade}.
	 * Logic is copied to {@link #loginSuccess()}.
	 */
	@Override
	public void loginSuccess()
	{
		loginSuccess(true);
	}

	/**
	 * Adjusted OOTB {@link DefaultCustomerFacade#loginSuccess()}.<br/>
	 * Modifications: <ul><li>execution of extended {@link #updateSessionCurrency(CurrencyData, CurrencyData, boolean)}
	 * instead of default {@link DefaultCustomerFacade#updateSessionCurrency(CurrencyData, CurrencyData)}</li>
	 * <li>eventual cart calculation depends on <code>recalculateCart</code> parameter</li></ul>
	 *
	 * @param recalculateCart
	 * 		Passed to {@link #updateSessionCurrency(CurrencyData, CurrencyData, boolean)}, see it javadoc for details.
	 * 		If true, cart is being recalcualted after cleanup, otherwise {@link CartModel#CALCULATED} set to false
	 */
	@Override
	public void loginSuccess(final boolean recalculateCart)
	{
		final CustomerData userData = getCurrentCustomer();

		// First thing to do is to try to change the user on the session cart
		if (getCartService().hasSessionCart())
		{
			getCartService().changeCurrentCartUser(getCurrentUser());
		}

		// Update the session currency (which might change the cart currency)
		if (!updateSessionCurrency(userData.getCurrency(), wileyStoreSessionFacade.getDefaultCurrency(), recalculateCart))
		{
			// Update the user
			getUserFacade().syncSessionCurrency();
		}

		// Update the user
		getUserFacade().syncSessionLanguage();

		// Calculate the cart after setting everything up
		if (getCartService().hasSessionCart())
		{
			final CartModel sessionCart = getCartService().getSessionCart();

			if (!checkoutFacade.isPaypalCheckout())
			{
				// Clean the existing info on the cart if it does not belong to the current user
				getCartCleanStrategy().cleanCart(sessionCart);
				if (recalculateCart)
				{
					try
					{
						final CommerceCartParameter parameter = new CommerceCartParameter();
						parameter.setEnableHooks(true);
						parameter.setCart(sessionCart);
						getCommerceCartService().recalculateCart(parameter);
					}
					catch (final CalculationException ex)
					{
						LOG.error("Failed to recalculate order [" + sessionCart.getCode() + "]", ex);
					}
				}
				else
				{
					sessionCart.setCalculated(false);
					getModelService().save(sessionCart);
				}
			}
		}
	}


	/**
	 * Emulated OOTB behaviour from {@lunk DefaultCustomerFacade}.
	 * Logic is copied to {@link #updateSessionCurrency(CurrencyData, CurrencyData, boolean)}.
	 */
	@Override
	protected boolean updateSessionCurrency(final CurrencyData preferredCurrency, final CurrencyData defaultCurrency)
	{
		return updateSessionCurrency(preferredCurrency, defaultCurrency, true);
	}

	/**
	 * Adjusted OOTB method {@link DefaultCustomerFacade#updateSessionCurrency(CurrencyData, CurrencyData)}. .
	 * Modification: execution of {@link WileyStoreSessionService} instead of default bean,
	 * added <code>recalculateCart</code> parameter to signature.
	 *
	 * @param preferredCurrency
	 * 		Currency isocode
	 * @param defaultCurrency
	 * 		Currency isocode
	 * @param recalculateCart
	 * 		Parameter will be passed to {@link WileyStoreSessionService#setCurrentCurrency(String, boolean)} method
	 * @return If session was updated with country
	 */
	protected boolean updateSessionCurrency(final CurrencyData preferredCurrency, final CurrencyData defaultCurrency,
			final boolean recalculateCart)
	{
		if (preferredCurrency != null)
		{
			final String currencyIsoCode = preferredCurrency.getIsocode();

			// Get the available currencies and check if the currency iso code is supported
			final Collection<CurrencyData> currencies = wileyStoreSessionFacade.getAllCurrencies();
			for (final CurrencyData currency : currencies)
			{
				if (StringUtils.equals(currency.getIsocode(), currencyIsoCode))
				{
					// Set the current currency
					wileyStoreSessionFacade.setCurrentCurrency(currencyIsoCode, recalculateCart);
					return true;
				}
			}
		}

		// Fallback to the default
		wileyStoreSessionFacade.setCurrentCurrency(defaultCurrency.getIsocode());
		return false;
	}

	/**
	 * Update customer billing address country and current Employer / School fields
	 *
	 * @param countryIso
	 * 		- country to update current customer
	 * @param currentEmployerSchool
	 */
	@Override
	public void updateCustomerCountryAndEmployerSchoolFields(final String countryIso,
			final String currentEmployerSchool) throws DuplicateUidException
	{
		final CustomerModel customer = getCurrentSessionCustomer();
		customer.setCurrentEmployerSchool(currentEmployerSchool);
		if (countryIso != null)
		{
			final AddressModel addressModel = getModelService().create(AddressModel.class);
			final CountryModel countryModel = getCommonI18NService().getCountry(countryIso);
			addressModel.setCountry(countryModel);
			addressModel.setOwner(customer);
			customer.setDefaultPaymentAddress(addressModel);
		}
		customerAccountService.updateCustomerProfile(customer);
	}

	@Override
	public boolean checkPassword(final String parameterPassword)
	{
		Assert.notNull(parameterPassword, "The parameter [parameterPassword] cannot be null");

		final UserModel userModel = getUserService().getCurrentUser();
		return getPasswordEncoderService().isValid(userModel, parameterPassword);
	}

	@Override
	public CustomerData checkPasswordGetCutomerName(final String parameterPassword)
	{
		Assert.notNull(parameterPassword, "The parameter [parameterPassword] cannot be null");

		CustomerData customerData = null;
		final UserModel userModel = getUserService().getCurrentUser();
		if (getPasswordEncoderService().isValid(userModel, parameterPassword))
		{
			if (userModel instanceof CustomerModel)
			{
				final CustomerModel customerModel = (CustomerModel) userModel;
				customerData = new CustomerData();
				customerData.setFirstName(customerModel.getFirstName());
				customerData.setLastName(customerModel.getLastName());
				return customerData;
			}
		}

		return null;
	}

	public void setCheckoutFacade(final WelAgsCheckoutFacade checkoutFacade) {
		this.checkoutFacade = checkoutFacade;
	}
}
