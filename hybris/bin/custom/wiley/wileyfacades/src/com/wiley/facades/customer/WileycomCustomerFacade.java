package com.wiley.facades.customer;

import de.hybris.platform.commercefacades.customer.CustomerFacade;

import java.util.List;

import com.wiley.core.customer.ResetPasswordRedirectType;
import com.wiley.facades.customer.data.NameSuffixData;
import com.wiley.facades.customer.data.SchoolData;


public interface WileycomCustomerFacade extends CustomerFacade
{

	void loginSuccess(boolean recalculateCart);

	void forgottenPassword(String uid, ResetPasswordRedirectType type);

	List<NameSuffixData> getNameSuffixes();

	List<SchoolData> getSchoolNames();

	List<SchoolData> getSchoolNamesWithLimitation();
}
