package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.commerceservices.stock.CommerceStockService;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.variants.model.VariantProductModel;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.wiley.restriction.WileyProductRestrictionService;
import com.wiley.facades.product.data.InventoryStatusRecord;
import com.wiley.facades.wileyb2c.util.Wileyb2cProductInventoryStatusCalculationUtils;


/**
 * Created by Uladzimir_Barouski on 6/2/2017.
 */
public class Wileyb2cPurchaseOptionInventoryStatusPopulator implements Populator<VariantProductModel, VariantOptionData>
{
	@Resource(name = "wileycomCommerceStockService")
	private CommerceStockService stockService;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	private WileyProductRestrictionService wileyProductRestrictionService;

	@Override
	public void populate(final VariantProductModel source,
			final VariantOptionData target)
			throws ConversionException
	{
		ServicesUtil.validateParameterNotNullStandardMessage("source", source);
		ServicesUtil.validateParameterNotNullStandardMessage("target", target);

		target.setInventoryStatus(populateInventoryStatus(source));
	}

	public InventoryStatusRecord populateInventoryStatus(final ProductModel productModel)
	{
		//define parameters required for Inventory Status calculation
		final Long availableQuantity = stockService.getStockLevelForProductAndBaseStore(productModel,
				baseStoreService.getCurrentBaseStore());

		final boolean isAvailable = wileyProductRestrictionService.isAvailable(productModel);

		final InventoryStatusRecord inventoryStatus = Wileyb2cProductInventoryStatusCalculationUtils
				.calculateInventoryStatusForProduct(productModel, availableQuantity, isAvailable);

		return inventoryStatus;
	}

	@Required
	public void setWileyProductRestrictionService(
			final WileyProductRestrictionService wileyProductRestrictionService)
	{
		this.wileyProductRestrictionService = wileyProductRestrictionService;
	}
}
