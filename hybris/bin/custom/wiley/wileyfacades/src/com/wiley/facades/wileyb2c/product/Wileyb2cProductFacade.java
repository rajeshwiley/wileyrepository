package com.wiley.facades.wileyb2c.product;

import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.commercefacades.product.data.ProductReferenceData;

import java.util.List;

import com.wiley.facades.product.WileyProductFacade;


public interface Wileyb2cProductFacade extends WileyProductFacade
{


	/**
	 * Retrieves product references of each product for related products carousel on "continue shopping" pop-up given its code.
	 * No duplicates.
	 *
	 * @param codes
	 * 		the product codes
	 * @param productReferenceTypes
	 * 		product reference types to use during search
	 * @param isDisplayRelatedProductsDespiteRestriction
	 * 		filter displaying products by restrictions
	 * @param limit
	 * 		the limit to return product references
	 * @return the product references
	 */
	List<ProductReferenceData> getProductReferencesForCodes(List<String> codes,
			List<ProductReferenceTypeEnum> productReferenceTypes, boolean isDisplayRelatedProductsDespiteRestriction, int limit);
}
