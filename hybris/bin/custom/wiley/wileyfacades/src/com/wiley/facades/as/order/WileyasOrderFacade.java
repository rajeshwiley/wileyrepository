package com.wiley.facades.as.order;

import com.wiley.facades.order.WileyOrderFacade;

import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.promotions.model.PromotionGroupModel;

import java.io.InputStream;
import java.util.Collection;


public interface WileyasOrderFacade extends WileyOrderFacade
{
	@Deprecated //This method is not in its place
	Collection<PromotionGroupModel> getPromotionGroups();

	OrderModel getOrderModel(String code);

	boolean hasActiveFulfillmentProcess(OrderModel orderModel);

	boolean hasActiveFulfillmentProcess(String orderCode);

	InputStream getInvoice(String orderEntryGuid);

	String getOrderGuidByOrderEntryGuid(String orderEntryGuid);

	void recalculateOrder(OrderModel orderModel, boolean forceRecalculate) throws CalculationException;

	void calculateExternalTaxes(OrderModel orderModel);

	void changeCountryForOrderData(OrderData orderData, String isoCode);
}
