/**
 *
 */
package com.wiley.facades.populators;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import com.wiley.facades.product.data.MediaData;


/**
 *
 */
public class ProductDetailsPopulator implements Populator<ProductModel, ProductData>
{

	private Converter<MediaModel, MediaData> mediaConverter;

	@Override
	public void populate(final ProductModel source, final ProductData target)
			throws ConversionException
	{
		final List<MediaData> mediaDataList = new ArrayList<MediaData>();
		for (final MediaModel mediaModel : source.getDetail())
		{
			final MediaData mediaData = mediaConverter.convert(mediaModel);
			mediaDataList.add(mediaData);
		}
		target.setDetails(mediaDataList);
	}

	@Required
	public void setMediaConverter(final Converter<MediaModel, MediaData> mediaConverter)
	{
		this.mediaConverter = mediaConverter;
	}
}
