package com.wiley.facades.order.impl;

import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.services.BaseStoreService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.integration.WileyIntegrationService;
import com.wiley.facades.order.WileyMagicLinkFacade;


/**
 * Created by Uladzimir_Barouski on 2/25/2016.
 */
public class WileyMagicLinkFacadeImpl implements WileyMagicLinkFacade
{
	protected static final Logger LOG = LoggerFactory.getLogger(WileyMagicLinkFacadeImpl.class);

	private WileyIntegrationService wileyIntegrationService;
	@Autowired
	private BaseStoreService baseStoreService;
	@Autowired
	private CustomerAccountService customerAccountService;
	@Autowired
	private UserService userService;

	@Override
	public String getMagicLink()
	{
		UserModel userModel = userService.getCurrentUser();
		if (userModel instanceof CustomerModel) {
			CustomerModel customerData = (CustomerModel) userModel;
			return wileyIntegrationService.getMagicLink(customerData);
		}
		LOG.error(String.format("User [%s] is not customer", userModel.getUid()));
		throw new IllegalStateException(String.format("User [%s] is not customer", userModel.getUid()));
	}

	protected WileyIntegrationService getWileyIntegrationService()
	{
		return wileyIntegrationService;
	}

	public void setWileyIntegrationService(final WileyIntegrationService wileyIntegrationService)
	{
		this.wileyIntegrationService = wileyIntegrationService;
	}
}
