package com.wiley.facades.order.converters.populator;

import de.hybris.platform.commercefacades.order.converters.populator.AbstractOrderPopulator;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.annotation.Nonnull;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


/**
 * Author Herman_Chukhrai (EPAM)
 */
public class WileySubtotalWithoutDiscountPopulator extends AbstractOrderPopulator<CartModel, CartData>
{
	@Override
	public void populate(@Nonnull final CartModel source, @Nonnull final CartData target) throws ConversionException
	{
		validateParameterNotNullStandardMessage("source", source);
		validateParameterNotNullStandardMessage("target", target);

		target.setSubTotalWithoutDiscount(createPrice(source, source.getSubTotalWithoutDiscount()));
	}

}
