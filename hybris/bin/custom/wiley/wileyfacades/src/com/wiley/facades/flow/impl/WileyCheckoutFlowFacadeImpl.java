package com.wiley.facades.flow.impl;

import de.hybris.platform.acceleratorfacades.flow.impl.DefaultCheckoutFlowFacade;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import com.wiley.facades.flow.WileyCheckoutFlowFacade;
import com.wiley.facades.i18n.WileyI18NFacade;


/**
 * Wiley checkout flow facade
 */
public class WileyCheckoutFlowFacadeImpl extends DefaultCheckoutFlowFacade implements WileyCheckoutFlowFacade
{
	@Resource
	private WileyI18NFacade wileyI18NFacade;

	@Override
	public boolean hasValidCart()
	{
		if (getCartService().hasSessionCart())
		{
			CartModel cart = getCartService().getSessionCart();
			return CollectionUtils.isNotEmpty(cart.getEntries());
		}

		return false;
	}

	@Override
	public boolean hasValidPaymentAddress()
	{
		if (getCartService().hasSessionCart() && getCartService().getSessionCart().getPaymentAddress() != null)
		{
			return isPaymentAddressValid(getCartService().getSessionCart().getPaymentAddress());
		}
		return false;
	}

	private boolean isPaymentAddressValid(final AddressModel paymentAddress)
	{
		final CountryModel country = paymentAddress.getCountry();
		final RegionModel region = paymentAddress.getRegion();
		return isCountryValid(country) && isRegionValid(country, region);
	}

	private boolean isCountryValid(final CountryModel country)
	{
		return country != null && StringUtils.isNotBlank(country.getIsocode());
	}

	private boolean isRegionValid(final CountryModel country, final RegionModel region)
	{
		if (country.getIsocode() != null && wileyI18NFacade.isDisplayRegions(country.getIsocode()))
		{
			return region != null && StringUtils.isNotBlank(region.getIsocode());
		}
		return true;
	}

	@Override
	public boolean hasNoPaymentInfo()
	{
		final CartModel cart = getCartService().getSessionCart();
		return cart == null || cart.getPaymentInfo() == null;
	}
}
