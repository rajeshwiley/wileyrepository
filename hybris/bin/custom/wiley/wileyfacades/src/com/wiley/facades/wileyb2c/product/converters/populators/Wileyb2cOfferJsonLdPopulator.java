package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.Assert;

import com.wiley.facades.wileyb2c.product.data.OfferJsonLdDto;


/**
 * Offer JSON LD populator
 *
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public class Wileyb2cOfferJsonLdPopulator implements Populator<PriceData, OfferJsonLdDto>
{
	private static final String TYPE_VALUE = "Offer";

	@Override
	public void populate(@NotNull final PriceData source,
			@NotNull final OfferJsonLdDto target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setType(TYPE_VALUE);
		final BigDecimal value = source.getValue();
		if (value != null)
		{
			target.setPrice(StringUtils.defaultIfEmpty(source.getValue().toString(), null));
		}
		target.setPriceCurrency(StringUtils.defaultIfEmpty(source.getCurrencyIso(), null));
	}
}
