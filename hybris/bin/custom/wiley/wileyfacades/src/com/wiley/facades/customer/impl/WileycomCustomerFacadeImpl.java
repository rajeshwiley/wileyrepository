package com.wiley.facades.customer.impl;

import de.hybris.platform.commercefacades.customer.impl.DefaultCustomerFacade;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import com.wiley.core.customer.ResetPasswordRedirectType;
import com.wiley.core.model.NameSuffixModel;
import com.wiley.core.model.SchoolModel;
import com.wiley.core.servicelayer.i18n.WileycomI18NService;
import com.wiley.core.wileycom.customer.service.WileycomCustomerAccountService;
import com.wiley.facades.customer.WileycomCustomerFacade;
import com.wiley.facades.customer.data.NameSuffixData;
import com.wiley.facades.customer.data.SchoolData;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


public class WileycomCustomerFacadeImpl extends DefaultCustomerFacade implements WileycomCustomerFacade
{
	public static final int SCHOOLS_MAX_QUANTITY = 30;

	protected static final int SCHOOLS_RANGE = 1000;

	private static final Logger LOG = LoggerFactory.getLogger(WileycomCustomerFacadeImpl.class);

	@Resource
	private WileycomCustomerAccountService wileycomCustomerAccountService;

	@Resource
	private Populator<CustomerData, CustomerModel> wileycomCustomerReversePopulator;

	@Resource
	private Converter<NameSuffixModel, NameSuffixData> wileycomNameSuffixConverter;

	@Resource
	private Converter<SchoolModel, SchoolData> wileycomSchoolConverter;

	@Resource
	private WileycomI18NService wileycomI18NService;



	@Override
	public void forgottenPassword(final String uid, final ResetPasswordRedirectType type)
	{
		Assert.hasText(uid, "The field [uid] cannot be empty");
		Assert.notNull(type, "The field [type] cannot be null");
		final CustomerModel customerModel = getUserService().getUserForUID(uid.toLowerCase(),
				CustomerModel.class);
		wileycomCustomerAccountService.forgottenPassword(customerModel, type);
	}

	@Override
	public List<NameSuffixData> getNameSuffixes()
	{
		return wileycomCustomerAccountService.getAllNameSuffixes()
				.stream()
				.map(nameSuffixModel -> wileycomNameSuffixConverter.convert(nameSuffixModel))
				.collect(Collectors.toList());
	}

	@Override
	public List<SchoolData> getSchoolNames()
	{
		return wileycomCustomerAccountService.getAllSchools()
				.stream()
				.map(schoolModel -> wileycomSchoolConverter.convert(schoolModel))
				.collect(Collectors.toList());
	}

	@Override
	public List<SchoolData> getSchoolNamesWithLimitation()
	{
		return wileycomCustomerAccountService.getSchoolsWithLimitation(SCHOOLS_RANGE, SCHOOLS_MAX_QUANTITY)
				.stream()
				.map(schoolModel -> wileycomSchoolConverter.convert(schoolModel))
				.collect(Collectors.toList());
	}

	@Override
	public void updateProfile(final CustomerData customerData) throws DuplicateUidException
	{
		validateDataBeforeUpdate(customerData);

		final CustomerModel customerModel = getCurrentSessionCustomer();
		wileycomCustomerReversePopulator.populate(customerData, customerModel);
		wileycomCustomerAccountService.updateCustomerProfile(customerModel);
	}

	@Override
	protected void validateDataBeforeUpdate(final CustomerData customerData)
	{
		validateParameterNotNullStandardMessage("customerData", customerData);
		Assert.hasText(customerData.getFirstName(), "The field [FirstName] cannot be empty");
		Assert.hasText(customerData.getLastName(), "The field [LastName] cannot be empty");
		Assert.hasText(customerData.getUid(), "The field [Uid] cannot be empty");
	}

	@Override
	public void register(final RegisterData registerData) throws DuplicateUidException
	{
		validateParameterNotNullStandardMessage("registerData", registerData);
		Assert.hasText(registerData.getFirstName(), "The field [FirstName] cannot be empty");
		Assert.hasText(registerData.getLastName(), "The field [LastName] cannot be empty");
		Assert.hasText(registerData.getLogin(), "The field [Login] cannot be empty");

		final CustomerModel newCustomer = getModelService().create(CustomerModel.class);

		if (StringUtils.isNotBlank(registerData.getFirstName()) && StringUtils.isNotBlank(registerData.getLastName()))
		{
			newCustomer.setFirstName(registerData.getFirstName());
			newCustomer.setLastName(registerData.getLastName());
			newCustomer.setName(getCustomerNameStrategy().getName(registerData.getFirstName(), registerData.getLastName()));
		}
		final TitleModel title = getUserService().getTitleForCode(registerData.getTitleCode());
		newCustomer.setTitle(title);
		setUidForRegister(registerData, newCustomer);

		newCustomer.setSessionLanguage(getCommonI18NService().getCurrentLanguage());
		newCustomer.setSessionCurrency(getCommonI18NService().getCurrentCurrency());
		Optional<CountryModel> currentCountry = wileycomI18NService.getCurrentCountry();
		if (currentCountry.isPresent())
		{
			newCustomer.setSessionCountry(currentCountry.get());
		}
		getCustomerAccountService().register(newCustomer, registerData.getPassword());
	}

	@Override
	public void loginSuccess()
	{
		loginSuccess(true);
	}


	@Override
	public void loginSuccess(final boolean recalculateCart)
	{
		final CustomerData userData = getCurrentCustomer();

		// First thing to do is to try to change the user on the session cart
		if (getCartService().hasSessionCart())
		{
			getCartService().changeCurrentCartUser(getUserService().getCurrentUser());
		}

		//NOTE: OOTB implentation has currency restoration here, but now currency is being set by country filters/handlers

		// Calculate the cart after setting everything up
		if (getCartService().hasSessionCart())
		{
			final CartModel sessionCart = getCartService().getSessionCart();

			// Clean the existing info on the cart if it does not beling to the current user
			getCartCleanStrategy().cleanCart(sessionCart);
			if (recalculateCart)
			{
				try
				{
					final CommerceCartParameter parameter = new CommerceCartParameter();
					parameter.setEnableHooks(true);
					parameter.setCart(sessionCart);
					getCommerceCartService().recalculateCart(parameter);
				}
				catch (final CalculationException ex)
				{
					LOG.error("Failed to recalculate order [" + sessionCart.getCode() + "]", ex);
				}
			}
			else
			{
				sessionCart.setCalculated(false);
				getModelService().save(sessionCart);
			}
		}
	}

	@Override
	public void updateCartWithGuestForAnonymousCheckout(final CustomerData guestCustomerData)
	{
		// First thing to do is to try to change the user on the session cart
		if (getCartService().hasSessionCart())
		{
			getCartService().changeCurrentCartUser(getUserService().getUserForUID(guestCustomerData.getUid()));
		}

		//NOTE: OOTB implentation has currency restoration here, but now currency is being set by country filters/hanlders

		if (!updateSessionLanguage(guestCustomerData.getLanguage(), getStoreSessionFacade().getDefaultLanguage()))
		{
			// Update the user
			getUserFacade().syncSessionLanguage();
		}

		// Calculate the cart after setting everything up
		if (getCartService().hasSessionCart())
		{
			final CartModel sessionCart = getCartService().getSessionCart();

			// Clear the delivery address, delivery mode, payment info before starting the guest checkout.
			sessionCart.setDeliveryAddress(null);
			sessionCart.setDeliveryMode(null);
			sessionCart.setPaymentInfo(null);
			getCartService().saveOrder(sessionCart);

			try
			{
				final CommerceCartParameter parameter = new CommerceCartParameter();
				parameter.setEnableHooks(true);
				parameter.setCart(sessionCart);
				getCommerceCartService().recalculateCart(parameter);
			}
			catch (final CalculationException ex)
			{
				LOG.error("Failed to recalculate order [" + sessionCart.getCode() + "]", ex);
			}
		}
	}

	@Override
	public void rememberMeLoginSuccessWithUrlEncoding(final boolean languageEncoding, final boolean currencyEncoding)
	{
		final CustomerData userData = getCurrentCustomer();

		// First thing to do is to try to change the user on the session cart
		if (getCartService().hasSessionCart())
		{
			getCartService().changeCurrentCartUser(getUserService().getCurrentUser());
		}

		//NOTE: OOTB implentation has currency restoration here, but now currency is being set by country filters/hanlders

		if (!languageEncoding && !updateSessionLanguage(userData.getLanguage(), getStoreSessionFacade().getDefaultLanguage()))
		{
			// Update the user
			getUserFacade().syncSessionLanguage();
		}

		// Calculate the cart after setting everything up
		if (getCartService().hasSessionCart())
		{
			final CartModel sessionCart = getCartService().getSessionCart();
			try
			{
				final CommerceCartParameter parameter = new CommerceCartParameter();
				parameter.setEnableHooks(true);
				parameter.setCart(sessionCart);
				getCommerceCartService().recalculateCart(parameter);
			}
			catch (final CalculationException ex)
			{
				LOG.error("Failed to recalculate order [" + sessionCart.getCode() + "]", ex);
			}
		}
	}

	public void setWileycomCustomerAccountService(
			final WileycomCustomerAccountService wileycomCustomerAccountService)
	{
		this.wileycomCustomerAccountService = wileycomCustomerAccountService;
	}

	public void setWileycomSchoolConverter(
			final Converter<SchoolModel, SchoolData> wileycomSchoolConverter)
	{
		this.wileycomSchoolConverter = wileycomSchoolConverter;
	}
}
