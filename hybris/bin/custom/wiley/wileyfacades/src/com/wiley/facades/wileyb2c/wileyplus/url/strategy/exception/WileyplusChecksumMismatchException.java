package com.wiley.facades.wileyb2c.wileyplus.url.strategy.exception;


public class WileyplusChecksumMismatchException extends Exception
{
	public WileyplusChecksumMismatchException(final String message)
	{
		super(message);
	}
}
