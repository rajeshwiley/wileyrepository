package com.wiley.facades.as.order.impl;

import com.wiley.core.exceptions.WileyOrderTaxNotAvailableException;
import com.wiley.core.integration.invoice.dto.InvoiceDto;
import com.wiley.core.integration.invoice.service.InvoiceService;
import com.wiley.core.model.WileyTnsMerchantModel;
import com.wiley.core.mpgs.services.strategies.WileyMPGSMerchantIdStrategy;
import com.wiley.core.order.WileyCheckoutService;
import com.wiley.core.order.WileyOrderFulfilmentProcessService;
import com.wiley.core.order.impl.WileyCalculationService;
import com.wiley.core.order.service.WileyOrderEntryService;
import com.wiley.core.payment.tnsmerchant.service.WileyTnsMerchantService;
import com.wiley.facades.as.order.WileyasOrderFacade;
import com.wiley.facades.order.impl.WileyOrderFacadeImpl;

import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commerceservices.externaltax.ExternalTaxesService;
import de.hybris.platform.commerceservices.order.impl.DefaultCommerceCartCalculationStrategy;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.promotions.PromotionsService;
import de.hybris.platform.promotions.model.PromotionGroupModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.BaseStoreModel;

import org.springframework.beans.factory.annotation.Required;

import javax.annotation.Resource;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;

import static com.wiley.facades.constants.WileyFacadesConstants.ORDER_NOT_FOUND_FOR_USER_AND_BASE_STORE;


/**
 * Default implementation for {@link WileyasOrderFacade}
 */
public class WileyasOrderFacadeImpl extends WileyOrderFacadeImpl implements WileyasOrderFacade
{
	private WileyCalculationService wileyasOrderEditCalculationService;
	private PromotionsService wileyasOrderEditPromotionEngineService;
	private ExternalTaxesService externalTaxesService;
	private BaseSiteService baseSiteService;
	private ModelService modelService;
	private WileyOrderFulfilmentProcessService wileyOrderFulfilmentProcessService;
	private WileyCheckoutService wileyCheckoutService;

	@Resource(name = "wileyasMPGSMerchantIdStrategy")
	private WileyMPGSMerchantIdStrategy wileyMPGSMerchantIdStrategy;

	@Resource
	private WileyTnsMerchantService wileyTnsMerchantService;

	@Resource(name = "wileyasOrderEntryService")
	private WileyOrderEntryService wileyOrderEntryService;

	@Resource
	private InvoiceService invoiceService;

	/**
	 * OOTB_CODE from {{@link DefaultCommerceCartCalculationStrategy#getPromotionGroups()} ()}}
	 */
	@Override
	public Collection<PromotionGroupModel> getPromotionGroups()
	{
		final Collection<PromotionGroupModel> promotionGroupModels = new ArrayList<>();
		if (getBaseSiteService().getCurrentBaseSite() != null
				&& getBaseSiteService().getCurrentBaseSite().getDefaultPromotionGroup() != null)
		{
			promotionGroupModels.add(getBaseSiteService().getCurrentBaseSite().getDefaultPromotionGroup());
		}
		return promotionGroupModels;
	}

	@Override
	public OrderModel getOrderModel(final String code)
	{
		CustomerModel currentUser = (CustomerModel) getUserService().getCurrentUser();
		BaseStoreModel currentBaseStore = getBaseStoreService().getCurrentBaseStore();
		return getCustomerAccountService().getOrderForCode(currentUser, code, currentBaseStore);
	}

	@Override
	public boolean hasActiveFulfillmentProcess(final OrderModel orderModel)
	{
		return wileyOrderFulfilmentProcessService.existsProcessActiveFor(orderModel);
	}

	@Override
	public boolean hasActiveFulfillmentProcess(final String orderCode)
	{
		OrderModel orderModel = getOrderModel(orderCode);
		return hasActiveFulfillmentProcess(orderModel);
	}


	@Override
	public InputStream getInvoice(final String orderEntryGuid)
	{
		InvoiceDto invoiceDto = invoiceService.getInvoice(orderEntryGuid);
		return new ByteArrayInputStream(invoiceDto.getBinaryFile());
	}

	@Override
	public String getOrderGuidByOrderEntryGuid(final String orderEntryGuid)
	{
		final OrderEntryModel orderEntry = wileyOrderEntryService.getOrderEntryForGUID(orderEntryGuid);
		final OrderModel order = orderEntry.getOrder();
		return order.getGuid();
	}

	@Override
	public void recalculateOrder(final OrderModel orderModel, final boolean forceRecalculate) throws CalculationException
	{
		if (forceRecalculate)
		{
			getWileyasOrderEditCalculationService().recalculate(orderModel);
		}
		else
		{
			getWileyasOrderEditCalculationService().calculate(orderModel);
		}
		getWileyasOrderEditPromotionEngineService().updatePromotions(getPromotionGroups(), orderModel);
		calculateExternalTaxes(orderModel);
		modelService.save(orderModel);
	}

	@Override
	public void calculateExternalTaxes(final OrderModel orderModel)
	{
		getExternalTaxesService().calculateExternalTaxes(orderModel);
		if (!wileyCheckoutService.isTaxAvailable(orderModel))
		{
			throw new WileyOrderTaxNotAvailableException();
		}
	}

	@Override
	public void changeCountryForOrderData(final OrderData orderData, final String countryIsoCode)
	{
		CountryData countryData = new CountryData();
		AddressData addressData = new AddressData();
		countryData.setIsocode(countryIsoCode);
		addressData.setCountry(countryData);
		orderData.setPaymentAddress(addressData);
		orderData.setHostedSessionScriptUrl(
				getHostedSessionScriptUrl(countryIsoCode, orderData.getTotalPrice().getCurrencyIso()));
	}

	private String getHostedSessionScriptUrl(final String countryIsoCode, final String currencyIsoCode)
	{
		final WileyTnsMerchantModel wileyTnsMerchantModel = wileyTnsMerchantService.getTnsMerchant(countryIsoCode,
				currencyIsoCode);
		return wileyMPGSMerchantIdStrategy.getHostedSessionScriptUrl(wileyTnsMerchantModel.getId());
	}

	public WileyCalculationService getWileyasOrderEditCalculationService()
	{
		return wileyasOrderEditCalculationService;
	}

	@Required
	public void setWileyasOrderEditCalculationService(final WileyCalculationService wileyasOrderEditCalculationService)
	{
		this.wileyasOrderEditCalculationService = wileyasOrderEditCalculationService;
	}

	public PromotionsService getWileyasOrderEditPromotionEngineService()
	{
		return wileyasOrderEditPromotionEngineService;
	}

	@Required
	public void setWileyasOrderEditPromotionEngineService(
			final PromotionsService wileyasOrderEditPromotionEngineService)
	{
		this.wileyasOrderEditPromotionEngineService = wileyasOrderEditPromotionEngineService;
	}

	public ExternalTaxesService getExternalTaxesService()
	{
		return externalTaxesService;
	}

	@Required
	public void setExternalTaxesService(final ExternalTaxesService externalTaxesService)
	{
		this.externalTaxesService = externalTaxesService;
	}

	public BaseSiteService getBaseSiteService()
	{
		return baseSiteService;
	}

	@Required
	public void setBaseSiteService(final BaseSiteService baseSiteService)
	{
		this.baseSiteService = baseSiteService;
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	public WileyOrderFulfilmentProcessService getWileyOrderFulfilmentProcessService()
	{
		return wileyOrderFulfilmentProcessService;
	}

	@Required
	public void setWileyOrderFulfilmentProcessService(
			final WileyOrderFulfilmentProcessService wileyOrderFulfilmentProcessService)
	{
		this.wileyOrderFulfilmentProcessService = wileyOrderFulfilmentProcessService;
	}

	@Required
	public void setWileyCheckoutService(final WileyCheckoutService wileyCheckoutService)
	{
		this.wileyCheckoutService = wileyCheckoutService;
	}

	@Override
	public OrderData getOrderDetailsForGUID(final String guid)
	{
		final OrderModel orderModel = getCustomerAccountService().getOrderDetailsForGUID(guid,
				getBaseStoreService().getCurrentBaseStore());
		if (orderModel == null)
		{
			throw new UnknownIdentifierException(String.format(ORDER_NOT_FOUND_FOR_USER_AND_BASE_STORE, guid));
		}
		return getOrderConverter().convert(orderModel);
	}
}

