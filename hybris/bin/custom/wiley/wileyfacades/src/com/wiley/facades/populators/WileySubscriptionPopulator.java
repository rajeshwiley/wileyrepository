package com.wiley.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.springframework.util.Assert;

import com.wiley.core.model.WileySubscriptionModel;
import com.wiley.facades.product.data.WileySubscriptionData;


public class WileySubscriptionPopulator implements Populator<WileySubscriptionModel, WileySubscriptionData>
{
	@Override
	public void populate(final WileySubscriptionModel source, final WileySubscriptionData target) throws ConversionException
	{
		Assert.notNull(source, "WileySubscriptionModel cannot be null.");
		Assert.notNull(target, "WileySubscriptionData cannot be null.");
		Assert.notNull(source.getProduct(), "Product cannot be null.");

		target.setCode(source.getCode());
		target.setEndDate(source.getExpirationDate());
		target.setRenewalEnabled(source.getRenewalEnabled().booleanValue());
		target.setAutoRenew(source.getRequireRenewal().booleanValue());

		final ProductModel product = source.getProduct();
		if (product != null)
		{
			target.setName(product.getName());
			target.setProductCode(product.getCode());
		}
	}
}
