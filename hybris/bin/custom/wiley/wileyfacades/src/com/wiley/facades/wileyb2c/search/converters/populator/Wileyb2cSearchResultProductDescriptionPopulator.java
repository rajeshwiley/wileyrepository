package com.wiley.facades.wileyb2c.search.converters.populator;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Required;


/**
 * Created by Uladzimir_Barouski on 5/25/2017.
 */
public class Wileyb2cSearchResultProductDescriptionPopulator implements Populator<SearchResultValueData, ProductData>
{
	private String descriptionProperty;

	@Override
	public void populate(@Nonnull final SearchResultValueData searchResultValueData, @Nonnull final ProductData productData)
			throws ConversionException
	{
		ServicesUtil.validateParameterNotNullStandardMessage("searchResultValueData", searchResultValueData);
		ServicesUtil.validateParameterNotNullStandardMessage("productData", productData);

		String description = (String) searchResultValueData.getValues().get(descriptionProperty);
		productData.setDescription(description);
	}

	@Required
	public void setDescriptionProperty(final String descriptionProperty)
	{
		this.descriptionProperty = descriptionProperty;
	}
}
