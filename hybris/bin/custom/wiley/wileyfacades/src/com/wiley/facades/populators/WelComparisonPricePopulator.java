package com.wiley.facades.populators;

import com.wiley.core.product.WileyProductVariantSetService;
import com.wiley.facades.converters.ProductNodesConverter;
import com.wiley.facades.product.node.ProductRootNode;
import com.wiley.facades.wel.product.WelProductFacade;

import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;

import javax.annotation.Resource;


public class WelComparisonPricePopulator extends WileyProductPricePopulator
{

	@Autowired
	private WileyProductVariantSetService wileyProductVariantSetService;

	@Resource(name = "productNodesConverter")
	private ProductNodesConverter productNodesConverter;

	@Resource(name = "welProductFacade")
	private WelProductFacade welProductFacade;


	@Override
	public void populate(final ProductModel productModel, final ProductData productData) throws ConversionException
	{
		PriceData productPriceData = productData.getPrice();
		if (productPriceData != null)
		{
			final ProductRootNode rootNode = productNodesConverter.convert(productData);
			addDefaultPartPrice(productData, rootNode);
			addDefaultCompleteSetPrice(productData, rootNode);
		} else
		{
			productData.setPurchasable(Boolean.FALSE);
		}
	}

	/**
	 * Method takes productdata with filled variant martix.
	 * Creates ProductRootNode(sorted by VARIANT_VALUE_CATEGORY.sequence list of nodes, that are used on PDP) from variant martix.
	 * Returns first set in the list. The first set will be default.
	 * @param productData
	 * @param rootNode	
	 */

	private void addDefaultCompleteSetPrice(final ProductData productData, final ProductRootNode rootNode)
	{
		final PriceData priceData = productData.getPrice();
		// do not compute default set price for Products w\out price (e.g. from impex)
		if (null != priceData)
		{
			final Double totalPrice = welProductFacade.getPriceForDefaultSetProduct(rootNode);
			if (null != totalPrice)
			{
				final PriceData newPriceData = getPriceDataFactory().create(
						priceData.getPriceType(), BigDecimal.valueOf(totalPrice), priceData.getCurrencyIso());
				productData.setPriceCompleteSet(newPriceData);
			}
		}
	}

	/**
	 * Method takes productdata with filled variant martix.
	 * Creates ProductRootNode(sorted by VARIANT_VALUE_CATEGORY.sequence list of nodes, that are used on PDP) from variant martix.
	 * Returns first part in the list. The first part in the list will be default.
	 * @param productData
	 * @param rootNode
	 */

	private void addDefaultPartPrice(final ProductData productData, final ProductRootNode rootNode)
	{
		final PriceData priceData = productData.getPrice();
		// do not compute  default part price  for Products w\out price (e.g. from impex)
		if (null != priceData)
		{
			final Double partPrice = welProductFacade.getPriceForDefaultPart(rootNode);
			PriceData newPriceData = priceData;
			if (null != partPrice)
			{
				newPriceData = getPriceDataFactory().create(
						priceData.getPriceType(), BigDecimal.valueOf(partPrice), priceData.getCurrencyIso());
			}
			productData.setPricePerPart(newPriceData);
		}
	}
}
