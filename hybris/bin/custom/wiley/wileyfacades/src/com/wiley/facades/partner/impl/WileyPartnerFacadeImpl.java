package com.wiley.facades.partner.impl;


import de.hybris.platform.commercefacades.user.data.UserGroupData;
import de.hybris.platform.europe1.enums.UserDiscountGroup;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.model.WileyPartnerCompanyModel;
import com.wiley.core.partner.WileyPartnerService;
import com.wiley.facades.partner.WileyPartnerCompanyData;
import com.wiley.facades.partner.WileyPartnerFacade;


public class WileyPartnerFacadeImpl implements WileyPartnerFacade
{
	@Resource
	private WileyPartnerService wileyPartnerService;
	@Resource
	private Converter<WileyPartnerCompanyModel, WileyPartnerCompanyData> wileyPartnerConverter;

	@Override
	public WileyPartnerCompanyData getWileyPartnerCompany(final String partnerId)
	{
		WileyPartnerCompanyData partnerCompanyData = null;
		WileyPartnerCompanyModel partnerCompany = wileyPartnerService.getPartnerById(partnerId);
		if (partnerCompany != null)
		{
			partnerCompanyData = wileyPartnerConverter.convert(partnerCompany);
		}
		return partnerCompanyData;
	}

	@Override
	public boolean partnerHasCategory(final WileyPartnerCompanyData partnerCompany, final String categoryCode)
	{
		boolean partnerCompanyHasCategory = false;
		if (CollectionUtils.isNotEmpty(partnerCompany.getPartnerCategories()))
		{
			partnerCompanyHasCategory = partnerCompany.getPartnerCategories().stream()
					.anyMatch(categoryData -> categoryCode != null && categoryCode.equals(categoryData.getCode()));
		}
		return partnerCompanyHasCategory;
	}

	@Override
	public String getPartnerDiscountGroupCode(final String partnerId)
	{
		String discountCode = null;
		WileyPartnerCompanyModel partnerCompany = wileyPartnerService.getPartnerById(partnerId);
		if (partnerCompany != null && partnerCompany.getUserDiscountGroup() != null)
		{
			discountCode = partnerCompany.getUserDiscountGroup().getCode();
		}
		return discountCode;
	}

	@Override
	public Map<String, String> getPartnersNamesByCategoryCode(final String categoryCode)
	{
		return wileyPartnerService.getPartnersByCategory(categoryCode).stream()
				.collect(Collectors.toMap(WileyPartnerCompanyModel::getUid, WileyPartnerCompanyModel::getLocName));
	}

	@Override
	public UserDiscountGroup getPartnerDiscountGroupForId(final String partnerId)
	{
		UserDiscountGroup discountGroup = null;
		WileyPartnerCompanyModel partnerCompany = wileyPartnerService.getPartnerById(partnerId);
		if (partnerCompany != null && partnerCompany.getUserDiscountGroup() != null)
		{
			discountGroup = partnerCompany.getUserDiscountGroup();
		}
		return discountGroup;
	}

	@Override
	public String getPartnerNameForId(final String partnerId)
	{
		String partnerName = null;
		WileyPartnerCompanyModel partnerCompany = wileyPartnerService.getPartnerById(partnerId);
		if (partnerCompany != null)
		{
			partnerName = partnerCompany.getDisplayName();
		}
		return partnerName;
	}

	@Override
	public boolean partnerBelongsToUserGroup(final UserGroupData partnerCompany, final String userGroupUid)
	{
		return wileyPartnerService.partnerBelongsToUserGroup(partnerCompany.getUid(), userGroupUid);
	}

	@Override
	public boolean isKPMGPartner(final UserGroupData partnerCompany)
	{
		return partnerBelongsToUserGroup(partnerCompany, WileyCoreConstants.KPMG_GROUP_UID);
	}

	@Override
	public boolean isSpecialPartner(final UserGroupData partnerCompany)
	{
		return partnerBelongsToUserGroup(partnerCompany, WileyCoreConstants.SPECIAL_PARTNER_GROUP_UID);
	}
}
