package com.wiley.facades.wileyb2c.product.converters.populators.helper;

import de.hybris.platform.commercefacades.product.converters.populator.AbstractProductPopulator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.Collection;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;


/**
 * @author Dzmitryi_Halahayeu
 */
public class Wileyb2cProductCollectionAttributesHelper
{
	private ModelService modelService;

	/**
	 * OOTB_CODE based {@link AbstractProductPopulator#getProductAttribute(ProductModel, String)}
	 * Get an collection attribute value from a product.
	 * If the attribute value is null and the product is a variant then the same attribute will be
	 * requested from the base product.
	 *
	 * @param productModel
	 * 		the product
	 * @param attribute
	 * 		the name of the attribute to lookup
	 * @return the value of the attribute
	 */
	public Object getProductAttribute(final ProductModel productModel, final String attribute)
	{
		final Object value = modelService.getAttributeValue(productModel, attribute);
		if (value != null)
		{
			Assert.isInstanceOf(Collection.class, value);
		}

		if (CollectionUtils.isEmpty((Collection<?>) value) && productModel instanceof VariantProductModel)
		{
			final ProductModel baseProduct = ((VariantProductModel) productModel).getBaseProduct();
			if (baseProduct != null)
			{
				return getProductAttribute(baseProduct, attribute);
			}
		}
		return value;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}
}
