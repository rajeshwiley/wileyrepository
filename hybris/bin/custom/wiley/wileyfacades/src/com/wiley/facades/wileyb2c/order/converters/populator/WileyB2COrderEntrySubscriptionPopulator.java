package com.wiley.facades.wileyb2c.order.converters.populator;

import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.subscriptionfacades.data.SubscriptionTermData;

import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.facades.populators.WileyB2CSubscriptionTermPopulator;


public class WileyB2COrderEntrySubscriptionPopulator implements Populator<AbstractOrderEntryModel, OrderEntryData>
{

	@Autowired
	private WileyB2CSubscriptionTermPopulator wileyB2CSubscriptionTermPopulator;

	@Override
	public void populate(final AbstractOrderEntryModel abstractOrderEntryModel, final OrderEntryData orderEntryData)
			throws ConversionException
	{
		if (abstractOrderEntryModel.getSubscriptionTerm() != null) {
			SubscriptionTermData subscriptionTermData = new SubscriptionTermData();
			wileyB2CSubscriptionTermPopulator.populate(abstractOrderEntryModel.getSubscriptionTerm(), subscriptionTermData);
			orderEntryData.setSubscriptionTerm(subscriptionTermData);
		}
	}
}
