package com.wiley.facades.welags.pin.impl;

import de.hybris.platform.site.BaseSiteService;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;

import com.wiley.facades.welags.pin.AddToCartStrategy;


/**
 * Providing the right implementation of {@link AddToCartStrategy}.
 *
 * @author Robert_Farkas
 * @author Gergo_Fodi
 */
public class AddToCartStrategyProvider
{
	private static final String BASE_SITE_UID_AGS = "ags";
	private static final String BASE_SITE_UID_WEL = "wel";

	@Resource
	private BaseSiteService baseSiteService;

	@Resource(name = "agsAddToCartStrategy")
	private AddToCartStrategy agsAddToCartStrategy;

	@Resource(name = "welAddToCartStrategy")
	private AddToCartStrategy welAddToCartStrategy;


	/**
	 * Providing the right implementation of {@link AddToCartStrategy} based on the site.
	 *
	 * @return {@link AddToCartStrategy}
	 */
	public AddToCartStrategy getStrategyForPinWorkFlow()
	{
		final String baseSiteUid = baseSiteService.getCurrentBaseSite().getUid();

		if (StringUtils.equals(BASE_SITE_UID_AGS, baseSiteUid))
		{
			return agsAddToCartStrategy;
		}
		else if (StringUtils.equals(BASE_SITE_UID_WEL, baseSiteUid))
		{
			return welAddToCartStrategy;
		}
		else
		{
			throw new UnsupportedOperationException("Base site is not supported: " + baseSiteUid);
		}
	}
}
