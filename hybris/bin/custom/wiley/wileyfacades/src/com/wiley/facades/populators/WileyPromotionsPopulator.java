package com.wiley.facades.populators;

import de.hybris.platform.acceleratorfacades.device.ResponsiveMediaFacade;
import de.hybris.platform.commercefacades.product.converters.populator.PromotionsPopulator;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.PromotionData;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.promotionengineservices.model.RuleBasedPromotionModel;
import de.hybris.platform.promotions.model.AbstractPromotionModel;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;
import java.util.List;


/**
 * Class for population image for product promotion.
 * To display image on PDP.
 */
public class WileyPromotionsPopulator extends PromotionsPopulator
{
	@Resource(name = "responsiveMediaFacade")
	private ResponsiveMediaFacade responsiveMediaFacade;

	@Override
	public void populate(@NotNull final AbstractPromotionModel source, @NotNull final PromotionData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setCode(source.getCode());
		target.setEndDate(source.getEndDate());
		target.setPromotionType(source.getPromotionType());
		processPromotionMessages(source, target);
		if (source instanceof RuleBasedPromotionModel)
		{
			final RuleBasedPromotionModel promotionModel = (RuleBasedPromotionModel) source;
			target.setDescription(promotionModel.getPromotionDescription());
			final MediaContainerModel mediaContainerModel = promotionModel.getProductBanner();
			if (mediaContainerModel != null)
			{
				final List<ImageData> mediaDataList = responsiveMediaFacade.getImagesFromMediaContainer(mediaContainerModel);
				target.setBanners(mediaDataList);
			}
		}
	}
}
