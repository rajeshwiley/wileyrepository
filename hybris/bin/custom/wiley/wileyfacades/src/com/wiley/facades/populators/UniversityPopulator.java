package com.wiley.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import com.wiley.core.model.UniversityModel;
import com.wiley.facades.university.data.UniversityData;


/**
 * Populates university data
 */
public class UniversityPopulator implements Populator<UniversityModel, UniversityData>
{
	/**
	 * Populate the {@link UniversityData} instance with values from the {@link UniversityModel} instance.
	 *
	 * @param universityModel
	 * 		the source object
	 * @param universityData
	 * 		the target to fill
	 * @throws ConversionException
	 * 		if an error occurs
	 */
	@Override
	public void populate(final UniversityModel universityModel, final UniversityData universityData) throws ConversionException
	{
		universityData.setCode(universityModel.getCode());
		universityData.setName(universityModel.getName());
	}
}
