package com.wiley.facades.event.listener.orderinfo;

import com.wiley.core.event.facade.orderinfo.Wileyb2cPopulateOrderInfoDataEvent;
import org.springframework.context.ApplicationListener;

/**
 * Created by Georgii_Gavrysh on 12/9/2016.
 */
public class Wileyb2cFacadeCartDataEventListener extends AbstractWileyFacadeCartDataEventListener
        implements ApplicationListener<Wileyb2cPopulateOrderInfoDataEvent> {
    @Override
    public void onApplicationEvent(final Wileyb2cPopulateOrderInfoDataEvent event) {
        populateOrderInfoData(event);
    }
}
