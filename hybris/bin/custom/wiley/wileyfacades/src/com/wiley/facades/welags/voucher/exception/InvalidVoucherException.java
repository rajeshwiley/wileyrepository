package com.wiley.facades.welags.voucher.exception;

import de.hybris.platform.commercefacades.voucher.exceptions.VoucherOperationException;

/**
 * May be thrown if voucher identified by a code does not exist or is invalid
 */
public class InvalidVoucherException extends VoucherOperationException {
    
    public InvalidVoucherException(final String message) {
        super(message);
    }

    public InvalidVoucherException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
