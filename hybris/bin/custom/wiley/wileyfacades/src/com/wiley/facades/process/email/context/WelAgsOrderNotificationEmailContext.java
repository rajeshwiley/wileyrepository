package com.wiley.facades.process.email.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;

import javax.annotation.Nullable;

import com.paypal.hybris.model.PaypalPaymentInfoModel;


/**
 * Contains more specific stuff for generation wel, ags order confirmation emails
 */
public class WelAgsOrderNotificationEmailContext extends OrderNotificationEmailContext
{
	private static final String PAYMENT_TYPE_PAYPAL = "PAYPAL";
	private static final String PAYMENT_TYPE_CARD = "CARD";

	private String paymentType = PAYMENT_TYPE_CARD;

	@Override
	public void init(final OrderProcessModel orderProcessModel, final EmailPageModel emailPageModel)
	{
		super.init(orderProcessModel, emailPageModel);
		setPaymentType(orderProcessModel.getOrder());
	}

	public String getPaymentType()
	{
		return paymentType;
	}

	private void setPaymentType(@Nullable final OrderModel orderModel) {
		if (orderModel == null) {
			return;
		}
		final PaymentInfoModel paymentInfo = orderModel.getPaymentInfo();
		if (paymentInfo instanceof PaypalPaymentInfoModel) {
			paymentType = PAYMENT_TYPE_PAYPAL;
		}
		else if (paymentInfo instanceof CreditCardPaymentInfoModel) {
			paymentType = PAYMENT_TYPE_CARD;
		}
	}
}
