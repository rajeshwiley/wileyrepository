package com.wiley.facades.order;

import de.hybris.platform.b2bacceleratorfacades.api.cart.CheckoutFacade;
import de.hybris.platform.b2bacceleratorfacades.order.B2BCheckoutFacade;
import de.hybris.platform.commercefacades.order.data.CartData;

import java.util.List;

import javax.annotation.Nullable;


public interface Wileyb2bCheckoutFacade extends WileycomCheckoutFacade, B2BCheckoutFacade, CheckoutFacade
{
	/**
	 * This Method saves po number in cart model
	 *
	 * @param cartData
	 */
	void savePoNumberInCart(CartData cartData);

	/**
	 * This method invokes integration with ESB system to validate PO numbers provided by customer
	 *
	 * @param cartData which contains PO numbers to be verified
	 * @return list of invalid PO numbers
	 */
	List<String> getInvalidPoNumbers(CartData cartData);

	/**
	 * This method saves OrderMessage in Cart Model
	 *
	 * @param orderMessage
	 */
	void saveOrderMessage(String orderMessage);

	void setDesiredShippingDate(@Nullable String desiredShippingDate);

	void setPaymentAddressFromUnit();
}
