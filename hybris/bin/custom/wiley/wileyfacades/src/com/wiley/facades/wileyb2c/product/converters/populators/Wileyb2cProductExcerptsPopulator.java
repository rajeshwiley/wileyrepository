package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.platform.commercefacades.product.converters.populator.AbstractProductPopulator;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.facades.product.data.MediaData;
import com.wiley.facades.wileyb2c.product.converters.populators.helper.Wileyb2cProductCollectionAttributesHelper;


/**
 * Created by Uladzimir_Barouski on 2/20/2017.
 */
public class Wileyb2cProductExcerptsPopulator extends AbstractProductPopulator<ProductModel, ProductData>
{
	private Wileyb2cProductCollectionAttributesHelper wileyb2cProductCollectionAttributesHelper;

	@Override
	public void populate(final ProductModel productModel, final ProductData productData) throws ConversionException
	{
		ServicesUtil.validateParameterNotNullStandardMessage("productModel", productModel);
		ServicesUtil.validateParameterNotNullStandardMessage("productData", productData);


		List<MediaContainerModel> excerptModelList =
				(List<MediaContainerModel>) wileyb2cProductCollectionAttributesHelper.getProductAttribute(productModel,
						ProductModel.EXCERPTS);
		if (CollectionUtils.isNotEmpty(excerptModelList))
		{
			List<MediaData> excerptDataList = excerptModelList.stream()
					.filter(excerpt -> StringUtils.isNotEmpty(excerpt.getName()) && CollectionUtils
							.isNotEmpty(excerpt.getMedias()))
					.map(excerpt -> getExcerptData(excerpt))
					.collect(Collectors.toList());
			productData.setExcerpts(excerptDataList);
		}
	}

	private MediaData getExcerptData(final MediaContainerModel excerptModel)
	{
		MediaData excerpt = new MediaData();
		excerpt.setName(excerptModel.getName());
		excerpt.setUrl(excerptModel.getMedias().iterator().next().getURL());
		return excerpt;
	}

	@Required
	public void setWileyb2cProductCollectionAttributesHelper(
			final Wileyb2cProductCollectionAttributesHelper wileyb2cProductCollectionAttributesHelper)
	{
		this.wileyb2cProductCollectionAttributesHelper = wileyb2cProductCollectionAttributesHelper;
	}
}
