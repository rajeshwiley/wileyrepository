/**
 *
 */
package com.wiley.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaModel;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import com.wiley.core.media.WileyMediaService;
import com.wiley.facades.product.data.MediaData;


/**
 * The type Media populator.
 */
public class MediaPopulator implements Populator<MediaModel, MediaData>
{
	private WileyMediaService mediaService;

	@Override
	public void populate(final MediaModel source, final MediaData target)
	{

		Assert.notNull(source, "Source should not be null.");
		Assert.notNull(target, "Target should not be null.");
		target.setName(mediaService.getFileNameWithoutExtension(source));
		target.setUrl(source.getURL());
		target.setDescription(source.getDescription());
	}

	/**
	 * Sets media service.
	 *
	 * @param mediaService
	 * 		the media service
	 */
	@Required
	public void setMediaService(final WileyMediaService mediaService)
	{
		this.mediaService = mediaService;
	}
}
