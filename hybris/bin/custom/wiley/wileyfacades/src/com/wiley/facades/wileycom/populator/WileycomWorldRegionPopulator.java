package com.wiley.facades.wileycom.populator;

import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import com.wiley.core.model.WorldRegionModel;
import com.wiley.facade.user.data.WorldRegionData;

import junit.framework.Assert;

public class WileycomWorldRegionPopulator implements Populator<WorldRegionModel, WorldRegionData>
{
	private Converter<CountryModel, CountryData> wileycomCountryConverter;

	@Override
	public void populate(final WorldRegionModel worldRegionModel, final WorldRegionData worldRegionData)
			throws ConversionException
	{
		Assert.assertNotNull(worldRegionModel);
		Assert.assertNotNull(worldRegionData);

		worldRegionData.setName(worldRegionModel.getName());
		worldRegionData.setCode(worldRegionModel.getCode());
		worldRegionData.setCountries(getWileycomCountryConverter().convertAll(worldRegionModel.getCountries()));
	}

	public Converter<CountryModel, CountryData> getWileycomCountryConverter()
	{
		return wileycomCountryConverter;
	}

	public void setWileycomCountryConverter(final Converter<CountryModel, CountryData> wileycomCountryConverter)
	{
		this.wileycomCountryConverter = wileycomCountryConverter;
	}
}
