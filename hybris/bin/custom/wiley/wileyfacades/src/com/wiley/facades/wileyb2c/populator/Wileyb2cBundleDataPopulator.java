package com.wiley.facades.wileyb2c.populator;

import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.CoreAlgorithms;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;

import com.google.common.base.Preconditions;
import com.wiley.core.model.WileyBundleModel;
import com.wiley.facades.populators.WileyPriceDataFactory;
import com.wiley.facades.product.WileyProductFacade;
import com.wiley.facades.wileybundle.data.WileyBundleData;


public class Wileyb2cBundleDataPopulator implements Populator<WileyBundleModel, WileyBundleData>
{
	private static final int DIGITS_AFTER_COMMA = 2;

	private static final double ONE_CENT = 0.01;
	private static final BigDecimal MINIMUM_DISCOUNT_VALUE = BigDecimal.valueOf(ONE_CENT);

	private List<ProductOption> productOptions;

	private final WileyProductFacade wileyProductFacade;
	private final WileyPriceDataFactory wileyPriceDataFactory;
	private final CommonI18NService commonI18NService;

	@Autowired
	public Wileyb2cBundleDataPopulator(
			@Qualifier("wileyb2cProductFacade")
			final WileyProductFacade wileyProductFacade,
			final WileyPriceDataFactory wileyPriceDataFactory,
			final CommonI18NService commonI18NService)
	{
		this.wileyProductFacade = wileyProductFacade;
		this.wileyPriceDataFactory = wileyPriceDataFactory;
		this.commonI18NService = commonI18NService;
	}


	@Override
	public void populate(final WileyBundleModel wileyBundleModel, final WileyBundleData wileyBundleData)
			throws ConversionException
	{
		final ProductData sourceProductData = wileyProductFacade.getProductForCodeAndOptions(
				wileyBundleModel.getSourceProductCode(), productOptions);
		Preconditions.checkState(sourceProductData.getPrice() != null, "Source product price can't be null");
		final ProductData upsellProductData =
				wileyProductFacade.getProductForCodeAndOptions(wileyBundleModel.getUpsellProductCode(), productOptions);
		Preconditions.checkState(upsellProductData.getPrice() != null, "Upsell product price can't be null");
		wileyBundleData.setSourceProduct(sourceProductData);
		wileyBundleData.setUpsellProduct(upsellProductData);
		wileyBundleData.setDiscountValue(wileyBundleModel.getDiscountValue());
		wileyBundleData.setDiscountCode(wileyBundleModel.getDiscountCode());
		setBundlePriceInformation(wileyBundleData, wileyBundleModel.getDiscountValue());
	}

	private void setBundlePriceInformation(final WileyBundleData bundle, final double discountVal) {
		final String currencyCode = bundle.getSourceProduct().getPrice().getCurrencyIso();
		final CurrencyModel currency = commonI18NService.getCurrency(currencyCode);

		double sourcePrice = bundle.getSourceProduct().getPrice().getValue().doubleValue();
		double upsellPrice = bundle.getUpsellProduct().getPrice().getValue().doubleValue();
		double originalPrice = sourcePrice + upsellPrice;

		final String originalPriceFormatted = wileyPriceDataFactory.formatPrice(BigDecimal.valueOf(originalPrice), currency);
		bundle.setOriginalPriceFormatted(originalPriceFormatted);

		final double purchasedTogetherPrice = calculatePrice(sourcePrice, upsellPrice, discountVal);
		final String purchasedTogetherPriceFormatted =
				wileyPriceDataFactory.formatPrice(BigDecimal.valueOf(purchasedTogetherPrice), currency);
		bundle.setPurchasedTogetherPriceFormatted(purchasedTogetherPriceFormatted);

		final BigDecimal discountedPriceValue = BigDecimal.valueOf(originalPrice - purchasedTogetherPrice);
		if (discountedPriceValue.compareTo(MINIMUM_DISCOUNT_VALUE) > 0) {
			final String discountedValuePriceFormatted =
					wileyPriceDataFactory.formatPrice(discountedPriceValue, currency);
			bundle.setDiscountedValueFormatted(discountedValuePriceFormatted);
		}
	}

	private double calculatePrice(final double sourcePrice, final double upsellPrice, final double discountVal) {
		final double total = sourcePrice + upsellPrice;
		final double percentageWithoutDiscount = 1 - discountVal / 100;
		return CoreAlgorithms.round(total * percentageWithoutDiscount, DIGITS_AFTER_COMMA);
	}

	@Required
	public void setProductOptions(final List<ProductOption> productOptions)
	{
		this.productOptions = productOptions;
	}
}
