package com.wiley.facades.wileyb2b.customer.converters.populators;

import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.BiFunction;

import javax.validation.constraints.NotNull;

import com.wiley.core.model.ExternalInventoryModel;
import com.wiley.facades.product.data.InventoryStatusRecord;

import static de.hybris.platform.basecommerce.enums.StockLevelStatus.BACK_ORDER;
import static de.hybris.platform.basecommerce.enums.StockLevelStatus.INSTOCK;
import static de.hybris.platform.basecommerce.enums.StockLevelStatus.LOWSTOCK;
import static de.hybris.platform.basecommerce.enums.StockLevelStatus.PRE_ORDER;
import static de.hybris.platform.basecommerce.enums.StockLevelStatus.PRINT_ON_DEMAND;


/**
 * Wileyb2b  InventoryStatus populator for cart page
 */
public class Wileyb2bInventoryStatusPopulator implements Populator<AbstractOrderEntryModel, OrderEntryData>
{

	@Override
	public void populate(@NotNull final AbstractOrderEntryModel source, @NotNull final OrderEntryData target)
	{
		if (source.getExternalInventories() != null && target.getProduct() != null)
		{

			List<InventoryStatusRecord> statuses = source.getExternalInventories().stream().
					reduce(new ArrayList<>(), populateStatusRecord(),
							(list1, list2) ->
							{
								list1.addAll(list2);
								return list1;
							});
			statuses.sort(Comparator.comparing(InventoryStatusRecord::getSortOrder)
					.thenComparing(InventoryStatusRecord::getAvailableDate));
			target.getProduct().setInventoryStatus(statuses);
		}
	}

	static final List<StockLevelStatus> STATUS_ORDERED = Arrays.asList(INSTOCK, LOWSTOCK, BACK_ORDER, PRE_ORDER, PRINT_ON_DEMAND);

	private BiFunction<List<InventoryStatusRecord>, ExternalInventoryModel, List<InventoryStatusRecord>> populateStatusRecord()
	{
		return (inventoryStatusRecords, externalInventoryModel) ->
		{
			InventoryStatusRecord inventoryStatusRecord = new InventoryStatusRecord();
			inventoryStatusRecord.setAvailableDate(externalInventoryModel.getEstimatedDeliveryDate());
			inventoryStatusRecord.setQuantity(externalInventoryModel.getQuantity());
			StockLevelStatus status = externalInventoryModel.getStatus();
			inventoryStatusRecord.setStatusCode(status);
			if (STATUS_ORDERED.contains(status))
			{
				inventoryStatusRecord.setSortOrder(STATUS_ORDERED.indexOf(status));
			}
			else
			{
				inventoryStatusRecord.setSortOrder(STATUS_ORDERED.size() + 1);
			}
			inventoryStatusRecords.add(inventoryStatusRecord);
			return inventoryStatusRecords;
		};
	}
}

