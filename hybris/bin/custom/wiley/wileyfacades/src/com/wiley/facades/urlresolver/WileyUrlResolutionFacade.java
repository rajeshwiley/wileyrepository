package com.wiley.facades.urlresolver;

import java.util.List;

import com.wiley.facades.common.LocalizedUrlData;


/**
 * Created by Uladzimir_Barouski on 6/14/2017.
 */
public interface WileyUrlResolutionFacade
{
	List<LocalizedUrlData> getAllLocalizedUrls(String servletPath, boolean secure);
}
