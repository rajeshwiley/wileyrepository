package com.wiley.facades.order.impl;

import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.wiley.core.address.WileyAddressService;
import com.wiley.core.i18n.WileyCommonI18NService;
import com.wiley.core.i18n.WileyCountryService;
import com.wiley.core.integration.vies.ViesCheckVatGateway;
import com.wiley.core.order.WileyCheckoutService;
import com.wiley.core.order.WileyCommerceCheckoutService;
import com.wiley.core.payment.WileyPaymentModeService;
import com.wiley.core.product.WileyProductEditionFormatService;
import com.wiley.core.validation.service.WileyTaxValidationService;
import com.wiley.facades.order.WileyCheckoutFacade;
import com.wiley.facades.payment.data.PaymentModeData;


/**
 * Created by Aliaksei_Zhvaleuski on 01.03.2018.
 */
public class WileyCheckoutFacadeImpl extends AbstractWileyCheckoutFacadeImpl implements WileyCheckoutFacade
{
	protected static final Logger LOG = LoggerFactory.getLogger(WileyCheckoutFacadeImpl.class);
	private static final String HOP_PAYMENT_RETURN_URL_ATTR_KEY = "hop.payment.return.url";
	private static final String HOP_PAYMENT_URL_ATTR_KEY = "hop.payment.url";

	@Autowired
	private WileyProductEditionFormatService wileyProductEditionFormatService;

	@Autowired
	@Qualifier("wileyCommerceCheckoutService")
	private WileyCommerceCheckoutService wileyCommerceCheckoutService;

	@Autowired
	private WileyAddressService wileyAddressService;

	@Autowired
	private WileyPaymentModeService wileyPaymentModeService;

	@Autowired
	private WileyCountryService wileyCountryService;

	@Resource
	private WileyTaxValidationService wileyTaxValidationService;

	@Autowired
	private WileyCommonI18NService wileyCommonI18NService;

	@Autowired
	@Qualifier(value = "wileyPaymentModeConverter")
	private Converter<PaymentModeModel, PaymentModeData> paymentModeConverter;

	@Resource
	private SessionService sessionService;

	@Autowired
	private WileyCheckoutService wileyCheckoutService;

	@Resource
	private ViesCheckVatGateway viesCheckVatGateway;

	@Resource(name = "wileyI18NFacade")
	private I18NFacade wileyI18NFacade;

	@Override
	public boolean isDigitalSessionCart()
	{
		final CartModel cartModel = getCart();
		return cartModel != null && wileyProductEditionFormatService.isDigitalCart(cartModel);
	}

	@Override
	public void calculateCart()
	{
		final CartModel cartModel = getCart();
		if (cartModel != null)
		{
			final CommerceCheckoutParameter parameter = new CommerceCheckoutParameter();
			parameter.setEnableHooks(true);
			cartModel.setCalculated(Boolean.FALSE);
			parameter.setCart(cartModel);
			getCommerceCheckoutService().calculateCart(parameter);
		}
	}

	@Override
	public void setPaymentAddress(final AddressModel addressModel)
	{
		final CommerceCheckoutParameter commerceCartParameter = new CommerceCheckoutParameter();
		commerceCartParameter.setCart(getCart());
		commerceCartParameter.setAddress(addressModel);
		wileyCommerceCheckoutService.setPaymentAddress(commerceCartParameter);
	}

	@Override
	public void setPaymentAddressData(final AddressData addressData)
	{
		final CartModel cartModel = getCart();
		if (cartModel != null)
		{
			AddressModel addressModel = getExistingAddressModel(addressData);
			if (addressModel == null)
			{
				addressModel = createAddressModel(cartModel);
			}
			getAddressReversePopulator().populate(addressData, addressModel);
			getModelService().save(addressModel);
			setPaymentAddress(addressModel);
		}
	}

	@Override
	public void setTaxExemptionDetails(final String taxNumber, final Date taxNumberExpirationDate)
	{
		CartModel cartModel = getCartService().getSessionCart();
		cartModel.setTaxNumber(taxNumber);
		cartModel.setTaxNumberExpirationDate(taxNumberExpirationDate);
		getModelService().save(cartModel);
	}

	@Override
	public void setVatRegistrationDetails(final String taxNumber)
	{
		CartModel cartModel = getCartService().getSessionCart();
		cartModel.setTaxNumber(taxNumber);
		cartModel.setTaxNumberExpirationDate(null);
		getModelService().save(cartModel);
	}



	@Override
	public void validateTaxInformation(final String countryIso, final String taxNumber)
	{
		CountryModel countryModel = wileyCountryService.findCountryByCode(countryIso);
		boolean validateTaxResult = wileyTaxValidationService.validateTaxInformation(countryModel, taxNumber);
		setTaxNumberValidated(validateTaxResult);
	}

	private void setTaxNumberValidated(final Boolean taxNumberValidated)
	{
		CartModel cartModel = getCartService().getSessionCart();
		cartModel.setTaxNumberValidated(taxNumberValidated);
		getModelService().save(cartModel);
	}

	@Override
	public boolean setDefaultCurrencyInCartForCurrentPaymentAddress()
	{
		final CartModel sessionCart = getCart();
		final AddressModel paymentAddress = sessionCart.getPaymentAddress();
		if (paymentAddress != null)
		{
			final CurrencyModel currentCurrency = sessionCart.getCurrency();
			final CurrencyModel defaultCurrency =
					wileyCommonI18NService.getDefaultCurrency(paymentAddress.getCountry());

			if (!Objects.equals(defaultCurrency, currentCurrency))
			{
				sessionCart.setCurrency(defaultCurrency);
				getModelService().save(sessionCart);
				return true;
			}
		}
		return false;
	}

	private AddressModel getExistingAddressModel(final AddressData addressData)
	{
		Optional<AddressModel> addressModel = Optional.empty();
		if (addressData != null && addressData.getId() != null)
		{
			addressModel = wileyAddressService.getAddressById(addressData.getId());
		}

		return addressModel.orElse(null);
	}

	private AddressModel createAddressModel(final CartModel cartModel)
	{
		final AddressModel addressModel = wileyAddressService.createAddressForOwner(cartModel);
		return addressModel;
	}

	@Override
	public AddressData getAddressDatabyId(final String addressId)
	{
		final Optional<AddressModel> addressModel = wileyAddressService.getAddressById(addressId);
		if (addressModel.isPresent()) {
			final AddressData addressData = new AddressData();
			getAddressConverter().convert(addressModel.get(), addressData);
			return addressData;
		} else {
			return null;
		}
	}

	@Override
	public void setCartCountry(final String countryIso)
	{
		if (StringUtils.isNotEmpty(countryIso))
		{
			final CountryModel country = wileyCountryService.findCountryByCode(countryIso);
			final CartModel cartModel = getCart();
			cartModel.setCountry(country);
			getModelService().save(cartModel);
		}

	}

	@Override
	public List<PaymentModeData> getSupportedPaymentModes(final AbstractOrderModel orderModel)
	{
		final List<PaymentModeModel> supportedPaymentModes =
				wileyPaymentModeService.getSupportedPaymentModes(orderModel);

		return Converters.convertAll(supportedPaymentModes, paymentModeConverter);
	}

	@Override
	public void saveHopPaymentUrl(final String hopPaymentUrl)
	{
		sessionService.setAttribute(HOP_PAYMENT_URL_ATTR_KEY, hopPaymentUrl);
	}

	@Override
	public String getSavedHopPaymentUrlAndRemove()
	{
		return getAttributeAndRemove(HOP_PAYMENT_URL_ATTR_KEY);
	}

	@Override
	public void saveHopPaymentReturnUrl(final String hopPaymentReturnUrl)
	{
		sessionService.setAttribute(HOP_PAYMENT_RETURN_URL_ATTR_KEY, hopPaymentReturnUrl);
	}

	@Override
	public String getSavedHopPaymentReturnUrlAndRemove()
	{
		return getAttributeAndRemove(HOP_PAYMENT_RETURN_URL_ATTR_KEY);
	}

	public String getAttributeAndRemove(final String attributeName)
	{
		final String value = sessionService.getAttribute(attributeName);
		sessionService.removeAttribute(attributeName);
		return value;
	}

	public void setWileyCommerceCheckoutService(final WileyCommerceCheckoutService wileyCommerceCheckoutService)
	{
		this.wileyCommerceCheckoutService = wileyCommerceCheckoutService;
	}

	@Override
	public boolean isTaxAvailable()
	{
		final CartModel sessionCart = getCart();
		return wileyCheckoutService.isTaxAvailable(sessionCart);
	}
}