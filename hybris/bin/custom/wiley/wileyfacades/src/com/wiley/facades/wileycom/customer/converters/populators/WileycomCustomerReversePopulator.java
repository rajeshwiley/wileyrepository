package com.wiley.facades.wileycom.customer.converters.populators;

import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.annotation.Resource;

import com.wiley.core.wileycom.customer.service.WileycomCustomerAccountService;


public class WileycomCustomerReversePopulator implements Populator<CustomerData, CustomerModel>
{
	@Resource
	private Populator<CustomerData, CustomerModel> customerReversePopulator;
	@Resource
	private WileycomCustomerAccountService wileycomCustomerAccountService;

	@Override
	public void populate(final CustomerData source, final CustomerModel target) throws ConversionException
	{
		customerReversePopulator.populate(source, target);
		target.setMiddleName(source.getMiddleName());
		target.setStudentId(source.getStudentId());
		target.setMajor(source.getMajor());
		target.setGraduationMonth(source.getGraduationMonth());
		target.setGraduationYear(source.getGraduationYear());
		if (source.getSchool() != null)
		{
		  target.setSchool(wileycomCustomerAccountService.getSchoolByCode(source.getSchool().getCode()).orElse(null));
		}
		else
		{
		  target.setSchool(null);
		}
		if (source.getSuffixCode() != null)
		{
			target.setSuffix(wileycomCustomerAccountService.getNameSuffixByCode(source.getSuffixCode()).orElse(null));
		}
	}
}
