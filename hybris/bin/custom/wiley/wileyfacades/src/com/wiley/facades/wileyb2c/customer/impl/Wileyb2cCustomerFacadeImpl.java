package com.wiley.facades.wileyb2c.customer.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.wiley.core.customer.ResetPasswordRedirectType;
import com.wiley.core.model.SchoolModel;
import com.wiley.core.order.WileycomCheckoutService;
import com.wiley.core.wileyb2c.customer.WileyB2CCustomerAccountService;
import com.wiley.facades.customer.data.SchoolData;
import com.wiley.facades.customer.impl.WileycomCustomerFacadeImpl;
import com.wiley.facades.wileyb2c.customer.Wileyb2cCustomerFacade;
import com.wiley.facades.wileycom.customer.converters.WileycomSchoolConverter;


public class Wileyb2cCustomerFacadeImpl extends WileycomCustomerFacadeImpl implements Wileyb2cCustomerFacade
{
	@Resource
	private WileyB2CCustomerAccountService wileyB2CCustomerAccountService;

	@Resource
	private WileycomSchoolConverter wileycomSchoolConverter;

	@Resource
	private WileycomCheckoutService wileycomCheckoutService;

	@Resource
	private BaseStoreService baseStoreService;

	@Resource
	private Converter<AbstractOrderEntryModel, OrderEntryData> wileyb2cOrderEntryConverter;

	@Override
	public void loginFailure()
	{
		getUserService().setCurrentUser(getUserService().getAnonymousUser());
	}

	@Override
	public SearchPageData<OrderEntryData> getDigitalOrderEntriesData(final PageableData pageableData)
	{
		final CustomerModel currentCustomer = (CustomerModel) getUserService().getCurrentUser();
		final BaseStoreModel currentBaseStore = baseStoreService.getCurrentBaseStore();
		final SearchPageData<OrderEntryModel> result = getCustomerAccountService()
				.getDigitalOrderEntries(currentCustomer, currentBaseStore, pageableData);

		final SearchPageData<OrderEntryData> resultData = new SearchPageData<>();
		resultData.setPagination(result.getPagination());
		resultData.setSorts(result.getSorts());
		resultData.setResults(Converters.convertAll(result.getResults(), wileyb2cOrderEntryConverter));
		return resultData;
	}


	@Override
	public void changeGuestToCustomer(final String pwd, final String orderGUID,
			final boolean acceptPromotions) throws DuplicateUidException
	{
		getCustomerAccountService().convertGuestToCustomer(pwd, orderGUID, acceptPromotions);
	}

	@Override
	public void forgottenPassword(final String uid, final ResetPasswordRedirectType type)
	{
		Assert.hasText(uid, "The field [uid] cannot be empty");
		final CustomerModel customerModel = getUserService().getUserForUID(uid.toLowerCase(),
				CustomerModel.class);
		Assert.state(!(customerModel instanceof B2BCustomerModel));
		super.forgottenPassword(uid, type);
	}

	@Override
	public List<SchoolData> getDisplayedSchoolsForCustomer(@Nonnull final String customerUid)
	{
		Assert.notNull(customerUid);

		List<SchoolData> resultSchoolList = getSchoolNamesWithLimitation();

		SchoolData customerSchool = getSchoolForCustomerFromProfile(customerUid);
		if (customerSchool == null)
		{
			customerSchool = getSchoolForCustomerFromLastOrder(customerUid);
		}

		if (customerSchool != null && !isSchoolPresentInSchoolList(resultSchoolList, customerSchool))
		{
			if (resultSchoolList.size() == SCHOOLS_MAX_QUANTITY)
			{
				resultSchoolList.remove(resultSchoolList.size() - 1);
			}

			resultSchoolList.add(customerSchool);
		}

		resultSchoolList.sort(Comparator.comparing(SchoolData::getName));

		return resultSchoolList;
	}

	@Override
	public SchoolData getSchoolForCustomerFromLastOrder(@Nonnull final String customerUid)
	{
		Assert.notNull(customerUid);

		SchoolData resultSchoolData = null;

		if (!getUserFacade().isAnonymousUser())
		{
			Optional<OrderModel> lastOrder = wileycomCheckoutService.getLastOrderForCustomer(customerUid);

			if (lastOrder.isPresent() && lastOrder.get().getSchool() != null)
			{
				resultSchoolData = wileycomSchoolConverter.convert(lastOrder.get().getSchool());
			}
		}

		return resultSchoolData;
	}

	@Override
	public SchoolData getSchoolForCustomerFromProfile(@Nonnull final String customerUid)
	{
		Assert.notNull(customerUid);

		SchoolData resultSchoolData = null;
		SchoolModel customerSchool = ((CustomerModel) getUserService().getUserForUID(customerUid)).getSchool();

		if (customerSchool != null)
		{
			resultSchoolData = wileycomSchoolConverter.convert(customerSchool);
		}

		return resultSchoolData;
	}


	@Override
	public void updateProfileIfChanged(final CustomerData customerData) throws DuplicateUidException
	{
		final CustomerModel customerModel = getCurrentSessionCustomer();
		if (!customerData.getFirstName().equals(customerModel.getFirstName())
		|| !customerData.getLastName().equals(customerModel.getLastName()))
		{
			customerModel.setFirstName(customerData.getFirstName());
			customerModel.setLastName(customerData.getLastName());
			wileyB2CCustomerAccountService.updateCustomerProfile(customerModel);
		}
	}

	@Override
	protected WileyB2CCustomerAccountService getCustomerAccountService()
	{
		return wileyB2CCustomerAccountService;
	}

	private boolean isSchoolPresentInSchoolList(final List<SchoolData> schools, final SchoolData customerSchoolData)
	{
		return schools.stream().map(SchoolData::getCode).anyMatch(customerSchoolData.getCode()::equals);
	}

	@Override
	public void createGuestUserForAnonymousCheckout(final String email, final String name) throws DuplicateUidException
	{
		validateParameterNotNullStandardMessage("email", email);
		final CustomerModel guestCustomer = getModelService().create(CustomerModel.class);
		final String guid = generateGUID();

		//takes care of localizing the name based on the site language
		guestCustomer.setUid(guid + "|" + email);
		guestCustomer.setFirstName(name);
		guestCustomer.setType(CustomerType.valueOf(CustomerType.GUEST.getCode()));
		guestCustomer.setSessionLanguage(getCommonI18NService().getCurrentLanguage());
		guestCustomer.setSessionCurrency(getCommonI18NService().getCurrentCurrency());

		getCustomerAccountService().registerGuestForAnonymousCheckout(guestCustomer, guid);
		updateCartWithGuestForAnonymousCheckout(getCustomerConverter().convert(guestCustomer));
	}

}