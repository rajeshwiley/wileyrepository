package com.wiley.facades.order.converters.populator;

import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;


/**
 * CartModificationData populator.
 */
public class WileyCartModificationReversePopulator implements Populator<CartModificationData, CommerceCartModification>
{
	private Converter<OrderEntryData, AbstractOrderEntryModel> orderEntryReverseConverter;

	@Override
	public void populate(
			@Nonnull final CartModificationData source, @Nonnull final CommerceCartModification target)
			throws ConversionException
	{
		Assert.notNull(source);
		Assert.notNull(target);

		if (source.getEntry() != null)
		{
			target.setEntry(getOrderEntryReverseConverter().convert(source.getEntry()));
		}

		target.setStatusCode(source.getStatusCode());
		target.setQuantity(source.getQuantity());
		target.setQuantityAdded(source.getQuantityAdded());
		target.setDeliveryModeChanged(source.getDeliveryModeChanged());

		target.setMessage(source.getStatusMessage());
		target.setMessageType(source.getStatusMessageType());
		target.setMessageParameters(source.getMessageParameters());
	}

	public Converter<OrderEntryData, AbstractOrderEntryModel> getOrderEntryReverseConverter()
	{
		return orderEntryReverseConverter;
	}

	@Required
	public void setOrderEntryReverseConverter(
			final Converter<OrderEntryData, AbstractOrderEntryModel> orderEntryReverseConverter)
	{
		this.orderEntryReverseConverter = orderEntryReverseConverter;
	}
}
