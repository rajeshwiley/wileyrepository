package com.wiley.facades.product.converters.populator;

import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.commercefacades.product.data.FeatureData;
import de.hybris.platform.converters.Populator;


public class WileyFeaturePositionPopulator implements Populator<ClassAttributeAssignmentModel, FeatureData>
{

	@Override
	public void populate(final ClassAttributeAssignmentModel source, final FeatureData target)
	{
		target.setPosition(source.getPosition() == null ? Integer.valueOf(0) : source.getPosition());
	}
}
