package com.wiley.facades.wileyb2c.product.converters.populators.helper;

import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import com.wiley.core.model.AuthorInfoModel;


/**
 * This helper class aggregates comon logic for author populating.
 *
 * Created by Uladzimir_Barouski on 5/31/2017.
 */
public class Wileyb2cAuthorPopulatorHelper
{
	@Resource
	private Wileyb2cProductCollectionAttributesHelper wileyb2cProductCollectionAttributesHelper;

	public String getAuthorsWithoutRoles(final ProductModel productModel)
	{
			final List<AuthorInfoModel> authorInformation =
					(List<AuthorInfoModel>) wileyb2cProductCollectionAttributesHelper.getProductAttribute(
							productModel, ProductModel.AUTHORINFOS);
			if (CollectionUtils.isNotEmpty(authorInformation))
			{
				final String authorInfos = authorInformation
						.stream()
						.map(author -> author.getName())
						.collect(Collectors.joining(", "));
				return authorInfos;
			}
		return StringUtils.EMPTY;
	}

}
