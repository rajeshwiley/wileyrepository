package com.wiley.facades.wileyb2c.wileyplus.url.strategy;

public interface WileyplusConstants
{
	String PURCHASE_OPTIONS_PAGE_URL = "/ucart/wileyPlus/purchase/options";
	String B2C_SITE_ID = "wileyb2c";

	String CART_ACTIVATION_PARAM = "cartActivationInfo";
	String CHECKSUM_PARAM = "checksum";
}
