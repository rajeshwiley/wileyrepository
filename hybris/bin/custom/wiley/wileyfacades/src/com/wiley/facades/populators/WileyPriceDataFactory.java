package com.wiley.facades.populators;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Locale;

import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.impl.DefaultPriceDataFactory;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.c2l.LanguageModel;

public class WileyPriceDataFactory extends DefaultPriceDataFactory {

    @Override
    public PriceData create(final PriceDataType priceType, final BigDecimal value, final CurrencyModel currency) {
        PriceData priceData = super.create(priceType, value, currency);
        priceData.setShortFormattedValue(createShortFormattedPrice(value, currency, priceData.getFormattedValue()));
        priceData.setCurrencySymbol(currency.getSymbol());
        return priceData;
    }

    @Override
    public String formatPrice(final BigDecimal value, final CurrencyModel currency)
    {
        return super.formatPrice(value, currency);
    }

    private String createShortFormattedPrice(final BigDecimal value, final CurrencyModel currency,
            final String formattedValue) {
        String shortFormatted = formattedValue;
        // check for fractional part. Consider it to be zero if it is less than 1 cents
        if (hasZeroCents(value)) {
            final LanguageModel currentLanguage = getCommonI18NService().getCurrentLanguage();
            Locale locale = getCommerceCommonI18NService().getLocaleForLanguage(currentLanguage);
            if (locale == null) {
                // Fallback to session locale
                locale = getI18NService().getCurrentLocale();
            }
            NumberFormat format = createNumberFormat(locale, currency);
            format.setMaximumFractionDigits(0);
            shortFormatted = format.format(value);
        }
        return shortFormatted;
    }

    private boolean hasZeroCents(final BigDecimal value) {
        // consider it to be zero if it is less than 1 cents
        return value.setScale(2, BigDecimal.ROUND_HALF_UP).remainder(BigDecimal.ONE).compareTo(BigDecimal.ZERO) == 0;
    }
}
