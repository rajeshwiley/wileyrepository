package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import com.wiley.core.wiley.restriction.WileyProductRestrictionService;


public class Wileyb2cVariantOptionAvailabilityPopulator implements Populator<ProductModel, VariantOptionData>
{
	private WileyProductRestrictionService wileyProductRestrictionService;

	@Override
	public void populate(final ProductModel productModel, final VariantOptionData variantOptionData) throws ConversionException
	{

		Assert.notNull(productModel);
		Assert.notNull(variantOptionData);

		variantOptionData.setAvailable(wileyProductRestrictionService.isAvailable(productModel));
	}

	@Required
	public void setWileyProductRestrictionService(final WileyProductRestrictionService wileyProductRestrictionService)
	{
		this.wileyProductRestrictionService = wileyProductRestrictionService;
	}
}
