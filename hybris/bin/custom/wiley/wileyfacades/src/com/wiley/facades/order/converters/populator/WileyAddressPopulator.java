package com.wiley.facades.order.converters.populator;

import de.hybris.platform.commercefacades.user.converters.populator.AddressPopulator;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;


/**
 * OOTB AddressPopulator does not use countryConverter that populates extra country fields. We need to have common
 * country population logic
 */
public class WileyAddressPopulator extends AddressPopulator
{
	@Resource
	private Converter<CountryModel, CountryData> wileyCountryConverter;

	@Override
	public void populate(final AddressModel source, final AddressData target)
	{
		superPopulate(source, target);
		populateCountry(source, target);
		populateAddressSummaryInformation(source, target);
	}

	protected void superPopulate(final AddressModel source, final AddressData target)
	{
		super.populate(source, target);
	}

	private void populateCountry(final AddressModel source, final AddressData target)
	{
		if (source.getCountry() != null)
		{
			target.setCountry(wileyCountryConverter.convert(source.getCountry()));
		}
	}

	private void populateAddressSummaryInformation(final AddressModel source, final AddressData target)
	{
		final String addressSummary = generateAddressSummary(source);
		target.setAddressSummary(addressSummary);
	}

	private String generateAddressSummary(final AddressModel source)
	{
		final List<String> addressAttributes = Arrays.asList(
				source.getLine1(), source.getLine2(), source.getTown(),
				getSpecificPostalCode(source), getCountryName(source)
		);

		return addressAttributes.stream()
				.filter(StringUtils::isNotEmpty)
				.collect(Collectors.joining(", "));
	}

	private String getSpecificPostalCode(final AddressModel source)
	{
		final RegionModel region = source.getRegion();
		final String postalCode = source.getPostalcode();

		if (postalCode != null && region != null && StringUtils.isNotEmpty(region.getName()))
		{
			return region.getName() + " " + postalCode;
		}

		return postalCode;
	}

	private String getCountryName(final AddressModel source)
	{
		final CountryModel country = source.getCountry();

		if (country != null && StringUtils.isNotEmpty(country.getName()))
		{
			return country.getName();
		}

		return StringUtils.EMPTY;
	}
}
