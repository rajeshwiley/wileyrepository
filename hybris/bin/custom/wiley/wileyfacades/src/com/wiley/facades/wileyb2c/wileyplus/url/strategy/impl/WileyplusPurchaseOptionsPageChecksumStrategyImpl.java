package com.wiley.facades.wileyb2c.wileyplus.url.strategy.impl;


import de.hybris.platform.servicelayer.config.ConfigurationService;

import javax.annotation.Resource;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;

import com.wiley.facades.wileyb2c.wileyplus.url.strategy.WileyplusPurchaseOptionsPageChecksumStrategy;
import com.wiley.facades.wileyb2c.wileyplus.url.strategy.exception.WileyplusChecksumMismatchException;


public class WileyplusPurchaseOptionsPageChecksumStrategyImpl implements WileyplusPurchaseOptionsPageChecksumStrategy
{
	private static final String CART_ACTIVATION_SECRET_KEY = "cart.activation.secret.key";

	@Resource
	private ConfigurationService configurationService;

	@Override
	public String calculateChecksum(final String value)
	{
		return DigestUtils.md5Hex(value + getConfigProperty(CART_ACTIVATION_SECRET_KEY));
	}

	@Override
	public void validateChecksum(final String value, final String checksum) throws WileyplusChecksumMismatchException
	{
		final String calculatedChecksum = calculateChecksum(value);
		if (!StringUtils.equals(calculatedChecksum, checksum))
		{
			final String message = String.format(
					"Calculated checksum for %s doesn't match checksum from request %s", value, checksum);
			throw new WileyplusChecksumMismatchException(message);
		}
	}

	private String getConfigProperty(final String propertyKey)
	{
		return configurationService.getConfiguration().getString(propertyKey);
	}
}
