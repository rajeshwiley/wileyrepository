package com.wiley.facades.order.converters.populator;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


public class WileyAbstractOrderCountryPopulator implements Populator<AbstractOrderModel, AbstractOrderData>
{

	@Resource
	private Converter<CountryModel, CountryData> wileyCountryConverter;

	@Override
	public void populate(final AbstractOrderModel abstractOrderModel, final AbstractOrderData abstractOrderData)
			throws ConversionException
	{
		validateParameterNotNullStandardMessage("abstractOrderModel", abstractOrderModel);
		validateParameterNotNullStandardMessage("abstractOrderData", abstractOrderData);

		final CountryModel cartCountry = abstractOrderModel.getCountry();
		if (cartCountry != null)
		{
			abstractOrderData.setCountry(wileyCountryConverter.convert(cartCountry));
		}
	}
}
