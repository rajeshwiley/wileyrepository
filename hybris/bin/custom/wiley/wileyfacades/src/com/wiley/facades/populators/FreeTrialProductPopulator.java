package com.wiley.facades.populators;

import de.hybris.platform.commercefacades.product.converters.populator.ProductDescriptionPopulator;
import de.hybris.platform.commercefacades.product.converters.populator.ProductGalleryImagesPopulator;
import de.hybris.platform.commercefacades.product.converters.populator.ProductUrlPopulator;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.wiley.core.model.WileyFreeTrialProductModel;
import com.wiley.facades.product.data.FreeTrialData;


/**
 * Created by Uladzimir_Barouski on 3/31/2016.
 */
public class FreeTrialProductPopulator
    implements Populator<WileyFreeTrialProductModel, FreeTrialData> {


  @Resource
  private ProductUrlPopulator productUrlPopulator;

  @Resource
  private ProductGalleryImagesPopulator productGalleryImagesPopulator;

  @Resource
  private ProductDescriptionPopulator productDescriptionPopulator;

  @Override
  public void populate(@Nonnull final WileyFreeTrialProductModel wileyFreeTrialProductModel,
      @Nonnull final FreeTrialData freeTrialData) throws ConversionException {
    Assert.notNull(wileyFreeTrialProductModel);
    Assert.notNull(freeTrialData);

    productUrlPopulator.populate(wileyFreeTrialProductModel, freeTrialData);
    productGalleryImagesPopulator.populate(wileyFreeTrialProductModel, freeTrialData);
    productDescriptionPopulator.populate(wileyFreeTrialProductModel, freeTrialData);
    freeTrialData.setVideoLink(wileyFreeTrialProductModel.getVideoLink());
    freeTrialData.setSlogan(wileyFreeTrialProductModel.getSlogan());

    List<ProductData> freeTrialList = new ArrayList<ProductData>();
    for (ProductModel freeTrial : wileyFreeTrialProductModel.getVariants()) {
      ProductData freeTrialVariantData = new ProductData();
      productUrlPopulator.populate(freeTrial, freeTrialVariantData);
      freeTrialList.add(freeTrialVariantData);
    }

    freeTrialList = freeTrialList.stream()
            .filter(freeTrialProduct -> freeTrialProduct.getName() != null)
            .collect(Collectors.toList());

    freeTrialList.sort(Comparator.comparing(ProductData::getName));

    freeTrialData.setFreeTrialList(freeTrialList);
  }

  public void setProductUrlPopulator(final ProductUrlPopulator productUrlPopulator)
  {
    this.productUrlPopulator = productUrlPopulator;
  }
}
