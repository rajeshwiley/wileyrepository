/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 *
 *
 */
package com.wiley.facades.order.converters.populator;

import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;


/**
 * Converter implementation for {@link PointOfServiceModel} as source and
 * {@link PointOfServiceData} as target type.
 */
public class WileyPointOfServiceReversePopulator implements Populator<PointOfServiceData, PointOfServiceModel>
{
	private Converter<AddressData, AddressModel> addressReverseConverter;

	@Override
	public void populate(final PointOfServiceData source, final PointOfServiceModel target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setName(source.getName());
		target.setDisplayName(source.getDisplayName());
		if (source.getAddress() != null)
		{
			target.setAddress(getAddressReverseConverter().convert(source.getAddress()));
		}
		target.setDescription(source.getDescription());
		target.setStoreContent(source.getStoreContent());
	}


	protected Converter<AddressData, AddressModel> getAddressReverseConverter()
	{
		return addressReverseConverter;
	}

	@Required
	public void setAddressReverseConverter(final Converter<AddressData, AddressModel> addressReverseConverter)
	{
		this.addressReverseConverter = addressReverseConverter;
	}
}
