/**
 *
 */
package com.wiley.facades.order.converters.populator;

import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.subscriptionfacades.data.OrderPriceData;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;


public class WileySubscriptionOrderPopulatorUtil
{
	@Autowired
	private PriceDataFactory priceDataFactory;

	public Double calcTotalWithTax(final OrderPriceData priceData)
	{
		if (priceData == null)
		{
			throw new IllegalArgumentException("source order must not be null");
		}

		BigDecimal totalPrice = priceData.getTotalPrice() == null ? BigDecimal.ZERO :
				priceData.getTotalPrice().getValue();

		if (totalPrice.compareTo(BigDecimal.ZERO) > 0 && priceData.getTotalTax() != null)
		{
			totalPrice = totalPrice.add(priceData.getTotalTax().getValue());
		}

		return Double.valueOf(totalPrice.doubleValue());
	}

	public List<OrderPriceData> fillOrderPriceDataByCustomFields(final AbstractOrderModel source,
			final List<OrderPriceData> orderPrices)
	{
		final Map<String, AbstractOrderModel> abstractOrders = getAbstractOrders(source);

		orderPrices.stream().forEach(price -> {
			convertFields(source, abstractOrders, price);
		});

		return orderPrices;
	}

	protected PriceData createPrice(final AbstractOrderModel source, final Double val)
	{
		if (source == null)
		{
			throw new IllegalArgumentException("source order must not be null");
		}

		final CurrencyModel currency = source.getCurrency();
		if (currency == null)
		{
			throw new IllegalArgumentException("source order currency must not be null");
		}

		// Get double value, handle null as zero
		final double priceValue = val != null ? val.doubleValue() : 0d;

		return priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(priceValue), currency);
	}

	private void convertFields(final AbstractOrderModel source, final Map<String, AbstractOrderModel> abstractOrders,
			final OrderPriceData price)
	{
		price.setTotalPriceWithTax(createPrice(source, calcTotalWithTax(price)));

		Double subtotalWithoutDiscount = abstractOrders.get(price.getBillingTime().getCode()).getSubTotalWithoutDiscount();
		price.setSubTotalWithoutDiscount(createPrice(source, subtotalWithoutDiscount));
	}

	private Map<String, AbstractOrderModel> getAbstractOrders(final AbstractOrderModel source)
	{
		final Map<String, AbstractOrderModel> abstractOrders = new HashMap<>();
		abstractOrders.put(source.getBillingTime().getCode(), source);

		Optional.ofNullable(source.getChildren()).ifPresent(list -> list.forEach(model -> {
			abstractOrders.put(model.getBillingTime().getCode(), source);
		}));

		return abstractOrders;
	}
}