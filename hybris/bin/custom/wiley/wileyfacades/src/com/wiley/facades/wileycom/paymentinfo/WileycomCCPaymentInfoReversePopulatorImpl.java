package com.wiley.facades.wileycom.paymentinfo;

import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import com.wiley.core.paymentinfo.CardPaymentInfoCcOwnerGenerator;


/**
 * Extends default CardPaymentInfoReversePopulator.s
 */
public class WileycomCCPaymentInfoReversePopulatorImpl implements Populator<CCPaymentInfoData, CreditCardPaymentInfoModel>
{

	@Resource
	private Populator<CCPaymentInfoData, CreditCardPaymentInfoModel> defaultCardPaymentInfoReversePopulator;

	@Resource
	private CardPaymentInfoCcOwnerGenerator wileyCardPaymentInfoCcOwnerGenerator;

	@Override
	public void populate(@Nonnull final CCPaymentInfoData ccPaymentInfoData,
			@Nonnull final CreditCardPaymentInfoModel creditCardPaymentInfoModel) throws ConversionException
	{
		ServicesUtil.validateParameterNotNullStandardMessage("ccPaymentInfoData", ccPaymentInfoData);
		ServicesUtil.validateParameterNotNullStandardMessage("creditCardPaymentInfoModel", creditCardPaymentInfoModel);

		defaultCardPaymentInfoReversePopulator.populate(ccPaymentInfoData, creditCardPaymentInfoModel);

		final AddressModel billingAddress = creditCardPaymentInfoModel.getBillingAddress();
		if (billingAddress != null)
		{
			creditCardPaymentInfoModel.setCcOwner(
					wileyCardPaymentInfoCcOwnerGenerator.getCCOwnerFromBillingAddress(billingAddress).orElse(null));
		}
	}
}
