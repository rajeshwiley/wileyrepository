package com.wiley.facades.wel.voucher.impl;

import de.hybris.platform.commercefacades.voucher.exceptions.VoucherOperationException;

import com.wiley.facades.voucher.impl.WileyCouponFacadeImpl;
import com.wiley.facades.welags.voucher.exception.InvalidVoucherException;
import com.wiley.facades.welags.voucher.exception.VoucherCannotBeAppliedException;


public class WelCouponFacadeImpl extends WileyCouponFacadeImpl
{

	@Override
	public void applyVoucher(final String voucherCode) throws VoucherOperationException
	{
		String voucherUpperCase = voucherCode.toUpperCase();
		if (!checkVoucherCode(voucherUpperCase))
		{
			throw new InvalidVoucherException("Checking voucher code returned false for " + voucherUpperCase);
		}
		if (!canApplyNewVoucher(voucherUpperCase))
		{
			throw new VoucherCannotBeAppliedException(voucherUpperCase + " voucher cannot be applied");
		}
		// redeem voucher
		super.applyVoucher(voucherUpperCase);
	}
}
