package com.wiley.facades.payment.converters.populators.impl;

import com.wiley.facades.payment.data.PaymentModeData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.util.ServicesUtil;

public class WileyPaymentModePopulator implements Populator<PaymentModeModel, PaymentModeData> {

    @Override
    public void populate(final PaymentModeModel paymentModeModel, final PaymentModeData paymentModeData)
            throws ConversionException
    {
        ServicesUtil.validateParameterNotNull(paymentModeModel, "Payment Mode cannot be null!");
        ServicesUtil.validateParameterNotNull(paymentModeData, "Payment Mode Data cannot be null!");

        paymentModeData.setCode(paymentModeModel.getCode());
        paymentModeData.setName(paymentModeModel.getName());
    }
}
