package com.wiley.facades.wileyb2c.wileyplus.url.strategy;


import com.wiley.facades.wileyb2c.wileyplus.url.strategy.exception.WileyplusChecksumMismatchException;


public interface WileyplusPurchaseOptionsPageChecksumStrategy
{
	/**
	 * Calculates MD5 checksum based on value and secret key from configuration
	 *
	 * @param value
	 * @return MD5 checksum
	 */
	String calculateChecksum(String value);

	/**
	 * Calculates MD5 checksum based on value and secret key from configuration
	 * Compares calculated checksum with checksum passed as argument
	 *
	 * @param value
	 * @param checksum
	 * @throws WileyplusChecksumMismatchException if checksum doesn't match
	 */
	void validateChecksum(String value, String checksum) throws WileyplusChecksumMismatchException;
}
