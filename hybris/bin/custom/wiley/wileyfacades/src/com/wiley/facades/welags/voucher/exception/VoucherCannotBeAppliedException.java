package com.wiley.facades.welags.voucher.exception;

import de.hybris.platform.commercefacades.voucher.exceptions.VoucherOperationException;

/**
 * May be thrown if a voucher cannot be applied to the order
 */
public class VoucherCannotBeAppliedException extends VoucherOperationException {
    
    public VoucherCannotBeAppliedException(final String message) {
        super(message);
    }

    public VoucherCannotBeAppliedException(final String message, final Throwable cause) {
        super(message, cause);
    }
}