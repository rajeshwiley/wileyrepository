package com.wiley.facades.event.listener.orderinfo;

import com.wiley.core.event.facade.orderinfo.WileyPopulateOrderInfoDataEvent;
import org.springframework.context.ApplicationListener;

/**
 * Created by Georgii_Gavrysh on 12/9/2016.
 */
public class WileyFacadeCartDataEventListener extends AbstractWileyFacadeCartDataEventListener
        implements ApplicationListener<WileyPopulateOrderInfoDataEvent> {

    @Override
    public void onApplicationEvent(final WileyPopulateOrderInfoDataEvent event) {
        populateOrderInfoData(event);
    }
}
