package com.wiley.facades.welags.pin.exception;

/**
 * Possible reasons enum for {@link PinOperationException}.
 */
public enum PinOperationExceptionReason {
	INVALID_PIN("The PIN code is invalid."),
	ALREADY_ACTIVATED_PIN("The PIN has been already activated."),
	INVALID_CART("The PIN could not be applied to cart."),
	INVALID_ORDER("The PIN could not be activated on the order."),
	INVALID_PRODUCT("The PIN could not be activated for the given product.");

	private final String message;

	PinOperationExceptionReason(final String message) {
		this.message = message;
	}

	public String getMessage() {
		return this.message;
	}
}
