/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 *
 *
 */
package com.wiley.facades.wileyb2c.search.solrfacetsearch;

import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;

import com.wiley.core.search.facetdata.Wileyb2cContentSearchPageData;
import com.wiley.facades.content.ContentData;


/**
 * Content search facade interface.
 * Used to retrieve content pages
 */
public interface Wileyb2cContentSearchFacade
{
	/**
	 * Refine an exiting search. The query object allows more complex queries using facet selection.
	 *
	 * @param searchState
	 * 		the search query object
	 * @param pageableData
	 * 		the page to return
	 * @return the search results
	 */
	Wileyb2cContentSearchPageData<SearchStateData, ContentData> textSearch(SearchStateData searchState,
			PageableData pageableData);

}
