/**
 * 
 */
package com.wiley.facades.order.converters.populator;

import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;

import javax.annotation.Nonnull;

import org.springframework.util.Assert;

/**
 * @author Yash_Chandarana
 *
 */
public class Wileyb2bOrderEntryPopulator implements Populator<AbstractOrderEntryModel, OrderEntryData>
{
	@Override
	public void populate(@Nonnull final AbstractOrderEntryModel source, @Nonnull final OrderEntryData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setPoNumber(source.getPoNumber());
	}

}
