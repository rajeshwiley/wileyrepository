package com.wiley.facades.wileyws.customer.update.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;


/**
 * Description of a server error.
 */
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringMVCServerCodegen",
		date = "2016-07-26T10:03:58.259+02:00")
public class Error
{
	private String code = null;
	private String message = null;

	/**
	 * Error code.
	 */
	@JsonProperty("code")
	public String getCode()
	{
		return code;
	}

	public void setCode(final String code)
	{
		this.code = code;
	}

	/**
	 * Error description.
	 */
	@JsonProperty("message")
	public String getMessage()
	{
		return message;
	}

	public void setMessage(final String message)
	{
		this.message = message;
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		Error error = (Error) o;
		return Objects.equals(code, error.code) && Objects.equals(message, error.message);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(code, message);
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("class Error {\n");
		sb.append("  code: ").append(code).append("\n");
		sb.append("  message: ").append(message).append("\n");
		sb.append("}\n");
		return sb.toString();
	}
}
