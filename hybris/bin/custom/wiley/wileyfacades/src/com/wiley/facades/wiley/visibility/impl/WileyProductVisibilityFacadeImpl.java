package com.wiley.facades.wiley.visibility.impl;

import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.wiley.restriction.WileyProductRestrictionService;
import com.wiley.core.wiley.restriction.dto.WileyRestrictionCheckResultDto;
import com.wiley.facades.wiley.visibility.WileyProductVisibilityFacade;


/**
 * @author Dzmitryi_Halahayeu
 */
public class WileyProductVisibilityFacadeImpl implements WileyProductVisibilityFacade
{
	private static final Logger LOG = Logger.getLogger(WileyProductVisibilityFacadeImpl.class);

	@Resource
	private ProductService productService;
	private WileyProductRestrictionService wileyProductRestrictionService;

	@Override
	public boolean isProductVisible(final String productCode)
	{
		final ProductModel product = productService.getProductForCode(productCode);
		final CommerceCartParameter commerceCartParameter = new CommerceCartParameter();
		commerceCartParameter.setProduct(product);
		final WileyRestrictionCheckResultDto checkResult = wileyProductRestrictionService.isVisible(commerceCartParameter);
		if (!checkResult.isSuccess())
		{
			LOG.warn("Visibility for product " + productCode + " is restricted. Details: " + checkResult.getErrorMessage());
		}
		return checkResult.isSuccess();
	}

	@Override
	public ProductModel filterRestrictedProductVariants(final ProductModel product)
	{
		return wileyProductRestrictionService.filterRestrictedProductVariants(product);
	}

	@Required
	public void setWileyProductRestrictionService(final WileyProductRestrictionService wileyProductRestrictionService)
	{
		this.wileyProductRestrictionService = wileyProductRestrictionService;
	}
}
