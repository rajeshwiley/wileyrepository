/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.facades.process.email.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.commerceservices.model.process.ForgottenPasswordProcessModel;
import de.hybris.platform.commerceservices.model.process.StoreFrontCustomerProcessModel;
import de.hybris.platform.util.Config;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.commons.lang3.StringUtils;


/**
 * Velocity context for a forgotten password email.
 */
public class ForgottenPasswordEmailContext extends CustomerEmailContext
{
	public static final String REDIRECT_TYPE_URL_PARAMETER_NAME = "t=";
	private static final String WEL_EFFICIENTLEARNING_URL_HTTP = "wel.efficientlearning.url.http";

	private int expiresInMinutes = 30;
	private String token;
	private String redirectType;
	private String efficientLearningLink;

	/**
	 * Gets expires in minutes.
	 *
	 * @return the expires in minutes
	 */
	public int getExpiresInMinutes()
	{
		return expiresInMinutes;
	}

	/**
	 * Sets expires in minutes.
	 *
	 * @param expiresInMinutes
	 * 		the expires in minutes
	 */
	public void setExpiresInMinutes(final int expiresInMinutes)
	{
		this.expiresInMinutes = expiresInMinutes;
	}

	/**
	 * Gets token.
	 *
	 * @return the token
	 */
	public String getToken()
	{
		return token;
	}

	/**
	 * Sets token.
	 *
	 * @param token
	 * 		the token
	 */
	public void setToken(final String token)
	{
		this.token = token;
	}


	/**
	 * Gets redirect type for current email
	 * @return redirect type
	 */
	public String getRedirectType()
	{
		return redirectType;
	}

	/**
	 * Sets redirect type for current email
	 * @param redirectType - type parameter that specifies redirect logic
	 */
	public void setRedirectType(final String redirectType)
	{
		this.redirectType = redirectType;
	}

	/**
	 * Gets url encoded token.
	 *
	 * @return the url encoded token
	 * @throws UnsupportedEncodingException
	 * 		the unsupported encoding exception
	 */
	public String getURLEncodedToken() throws UnsupportedEncodingException
	{
		return URLEncoder.encode(token, "UTF-8");
	}

	/**
	 * Gets request reset password url.
	 *
	 * @return the request reset password url
	 * @throws UnsupportedEncodingException
	 * 		the unsupported encoding exception
	 */
	public String getRequestResetPasswordUrl() throws UnsupportedEncodingException
	{
		return getSiteBaseUrlResolutionService().getWebsiteUrlForSite(getBaseSite(), getUrlEncodingAttributes(), false,
				"/login/pw/request/external");
	}

	/**
	 * Gets secure request reset password url.
	 *
	 * @return the secure request reset password url
	 * @throws UnsupportedEncodingException
	 * 		the unsupported encoding exception
	 */
	public String getSecureRequestResetPasswordUrl() throws UnsupportedEncodingException
	{
		return getSiteBaseUrlResolutionService().getWebsiteUrlForSite(getBaseSite(), getUrlEncodingAttributes(), true,
				"/login/pw/request/external");
	}

	/**
	 * Gets reset password url.
	 *
	 * @return the reset password url
	 * @throws UnsupportedEncodingException
	 * 		the unsupported encoding exception
	 */
	public String getResetPasswordUrl() throws UnsupportedEncodingException
	{
		return getResetPasswordUrl(false);
	}

	/**
	 * Gets secure reset password url.
	 *
	 * @return the secure reset password url
	 * @throws UnsupportedEncodingException
	 * 		the unsupported encoding exception
	 */
	public String getSecureResetPasswordUrl() throws UnsupportedEncodingException
	{
		return getResetPasswordUrl(true);
	}

	public String getResetPasswordUrl(final boolean isSecure) throws UnsupportedEncodingException
	{
		String parameters = "token=" + getURLEncodedToken();
		if (StringUtils.isNoneBlank(redirectType)) {
			parameters += "&" + REDIRECT_TYPE_URL_PARAMETER_NAME + redirectType;
		}
		return getSiteBaseUrlResolutionService().getWebsiteUrlForSite(getBaseSite(), getUrlEncodingAttributes(), isSecure,
				"/login/pw/change", parameters);
	}

	/**
	 * Gets display reset password url.
	 *
	 * @return the display reset password url
	 * @throws UnsupportedEncodingException
	 * 		the unsupported encoding exception
	 */
	public String getDisplayResetPasswordUrl() throws UnsupportedEncodingException
	{
		return getSiteBaseUrlResolutionService().getWebsiteUrlForSite(getBaseSite(), getUrlEncodingAttributes(), false,
				"/my-account/update-password");
	}

	/**
	 * Gets display secure reset password url.
	 *
	 * @return the display secure reset password url
	 * @throws UnsupportedEncodingException
	 * 		the unsupported encoding exception
	 */
	public String getDisplaySecureResetPasswordUrl() throws UnsupportedEncodingException
	{
		return getSiteBaseUrlResolutionService().getWebsiteUrlForSite(getBaseSite(), getUrlEncodingAttributes(), true,
				"/my-account/update-password");
	}

	/**
	 * Gets efficient learning link.
	 *
	 * @return the efficient learning link
	 */
	public String getEfficientLearningLink()
	{
		return efficientLearningLink;
	}

	/**
	 * Sets efficient learning link.
	 *
	 * @param efficientLearningLink
	 * 		the efficient learning link
	 */
	public void setEfficientLearningLink(final String efficientLearningLink)
	{
		this.efficientLearningLink = efficientLearningLink;
	}

	/**
	 * Init.
	 *
	 * @param storeFrontCustomerProcessModel
	 * 		the store front customer process model
	 * @param emailPageModel
	 * 		the email page model
	 */
	@Override
	public void init(final StoreFrontCustomerProcessModel storeFrontCustomerProcessModel, final EmailPageModel emailPageModel)
	{
		super.init(storeFrontCustomerProcessModel, emailPageModel);
		if (storeFrontCustomerProcessModel instanceof ForgottenPasswordProcessModel)
		{
			ForgottenPasswordProcessModel forgottenPasswordProcessModel =
					(ForgottenPasswordProcessModel) storeFrontCustomerProcessModel;
			setRedirectType(forgottenPasswordProcessModel.getRedirectType());
			setToken(forgottenPasswordProcessModel.getToken());
			setEfficientLearningLink(Config.getParameter(WEL_EFFICIENTLEARNING_URL_HTTP));
		}
	}
}
