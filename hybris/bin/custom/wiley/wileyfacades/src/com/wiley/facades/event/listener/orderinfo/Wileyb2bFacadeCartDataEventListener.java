package com.wiley.facades.event.listener.orderinfo;

import com.wiley.core.event.facade.orderinfo.Wileyb2bPopulateOrderInfoDataEvent;
import org.springframework.context.ApplicationListener;

/**
 * Created by Georgii_Gavrysh on 12/12/2016.
 */
public class Wileyb2bFacadeCartDataEventListener extends AbstractWileyFacadeCartDataEventListener
    implements ApplicationListener<Wileyb2bPopulateOrderInfoDataEvent>
{
    @Override
    public void onApplicationEvent(final Wileyb2bPopulateOrderInfoDataEvent event) {
        populateOrderInfoData(event);
    }
}
