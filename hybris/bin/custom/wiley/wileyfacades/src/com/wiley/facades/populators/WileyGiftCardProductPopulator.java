package com.wiley.facades.populators;

import com.wiley.core.model.WileyGiftCardProductModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;


/**
 * Wiley Gift Card Product Populator.
 */
public class WileyGiftCardProductPopulator implements Populator<ProductModel, ProductData>
{

	@Override
	public void populate(final ProductModel source, final ProductData target) throws ConversionException
	{
		if (source instanceof WileyGiftCardProductModel)
		{
			target.setItemType(WileyGiftCardProductModel._TYPECODE);
			target.setIsGiftCard(true);
		}
	}
}
