package com.wiley.facades.product;

import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.ProductReferenceData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;

import java.util.List;

import javax.annotation.Nonnull;

import com.wiley.facades.as.product.data.ProductPricesData;


/**
 * Interface contains methods for getting product data-objects
 *
 * Created by Raman_Hancharou on 1/19/2016.
 */
public interface WileyProductFacade extends ProductFacade
{
	/**
	 * Return products list for category
	 *
	 * @param productOptions
	 * @return
	 */
	List<ProductData> getProductsForCategory(String categoryCode, List<ProductOption> productOptions);

	/**
	 * Searches product by isbn in the system.
	 *
	 * @param isbn
	 * @return true if product exists otherwise false
	 */
	boolean doesProductExistForIsbn(@Nonnull String isbn);

	/**
	 * Finds product url for given ISBN in active session catalog version. If ISBN is wrong format search by code.
	 *
	 * @param isbn
	 * 		the product isbn
	 * @return product url if found
	 * @throws AmbiguousIdentifierException
	 * 		when more that one product is found for ISBN
	 * @throws de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException
	 * 		when no product is found
	 */
	String findProductUrlForIsbnOrCode(String isbn);

	/**
	 * Filters only visible productReferences if displayProductsDespiteRestrictions set to false.
	 * Returns all productReferences but marks visible products
	 * with flag showPdpUrl if displayProductsDespiteRestrictions set to true
	 *
	 * @param productReferences
	 * 		- collection of productReferences
	 * @param displayProductsDespiteRestrictions
	 * 		- the flag is used to mark visible products in returned collection
	 * @return filtered list of product references
	 */
	List<ProductReferenceData> filterVisibleReferenceProducts(List<ProductReferenceData> productReferences,
			boolean displayProductsDespiteRestrictions);


	/**
	 * Checks if product visible
	 *
	 * @param productCode
	 * 		- code of product
	 * @return product visibility
	 */
	boolean isVisible(@Nonnull String productCode);

	/**
	 * Return product prices list
	 *
	 * @param productCode product code
	 * @param currencyIsoCode currency ISO code
	 * @return product prices information
	 */
	ProductPricesData getProductPrices(String productCode, String currencyIsoCode);

	/**
	 * Return populated cached or not cached Product Data
	 *
	 * @param productModel - Product Model
	 * @param options      - Product Options for populator
	 * @return Product Data
	 */
	ProductData getProductForCodeAndOptionsCached(ProductModel productModel, List<ProductOption> options);

	/**
	 * Return extended Product Data
	 *
	 * @param productModel - Product Model
	 * @param productData  - Product Data for populate
	 * @param options      - Product Options for populator
	 * @return extended Product Data
	 */
	ProductData getProductDataForCodeAndOptions(ProductModel productModel, ProductData productData, List<ProductOption> options);
}

