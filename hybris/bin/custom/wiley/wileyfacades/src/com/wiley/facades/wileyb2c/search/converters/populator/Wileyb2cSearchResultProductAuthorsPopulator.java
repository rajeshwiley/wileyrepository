package com.wiley.facades.wileyb2c.search.converters.populator;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Required;


/**
 * Created by Uladzimir_Barouski on 3/9/2017.
 */
public class Wileyb2cSearchResultProductAuthorsPopulator implements Populator<SearchResultValueData, ProductData>
{
	private String authorProperty;

	@Override
	public void populate(final SearchResultValueData searchResultValueData, final ProductData productData)
			throws ConversionException
	{
		ServicesUtil.validateParameterNotNullStandardMessage("searchResultValueData", searchResultValueData);
		ServicesUtil.validateParameterNotNullStandardMessage("productData", productData);

		if (MapUtils.isNotEmpty(searchResultValueData.getValues()) && searchResultValueData.getValues().get(authorProperty)
				!= null)
		{
			String authors = ((List<String>) searchResultValueData.getValues().get(authorProperty)).stream().collect(
					Collectors.joining(", "));
			productData.setAuthorsWithoutRoles(authors);
		}
	}

	@Required
	public void setAuthorProperty(final String authorProperty)
	{
		this.authorProperty = authorProperty;
	}
}
