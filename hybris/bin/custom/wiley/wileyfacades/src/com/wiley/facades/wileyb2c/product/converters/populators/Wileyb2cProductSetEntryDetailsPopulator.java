package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.ConfigurablePopulator;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Required;

import com.wiley.facades.wileyb2c.product.data.ProductSetData;


/**
 * Created by Uladzimir_Barouski on 5/3/2017.
 */
public class Wileyb2cProductSetEntryDetailsPopulator implements Populator<ProductModel, ProductSetData>
{
	private ConfigurablePopulator<ProductModel, ProductData, ProductOption> configurablePopulator;

	protected static final List<ProductOption> PRODUCT_SET_OPTIONS = Arrays.asList(ProductOption.URL, ProductOption.GALLERY,
			ProductOption.VARIANT_PRODUCT_NAME, ProductOption.PRICE, ProductOption.SHOW_PDP_URL,
			ProductOption.PRODUCT_SET_COMPONENTS,  ProductOption.PURCHASE_OPTION,
			ProductOption.COUNTABLE, ProductOption.AUTHOR_INFOS, ProductOption.AUTHOR_INFOS_WITHOUT_ROLE, ProductOption.DETAILS);

	@Override
	public void populate(@Nonnull final ProductModel source, @Nonnull final ProductSetData target) throws ConversionException
	{
		ServicesUtil.validateParameterNotNullStandardMessage("productModel", source);
		ServicesUtil.validateParameterNotNullStandardMessage("productSetData", target);
		ProductData productSetData = new ProductData();
		configurablePopulator.populate(source, productSetData, PRODUCT_SET_OPTIONS);
		target.setProductSet(productSetData);
	}

	@Required
	public void setConfigurablePopulator(
			final ConfigurablePopulator<ProductModel, ProductData, ProductOption> configurablePopulator)
	{
		this.configurablePopulator = configurablePopulator;
	}
}
