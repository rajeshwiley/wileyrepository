package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import javax.annotation.Resource;

import com.wiley.core.model.WileyProductModel;
import com.wiley.core.model.WileyPurchaseOptionProductModel;
import com.wiley.core.wileyb2c.classification.Wileyb2cClassificationAttributes;
import com.wiley.core.wileyb2c.classification.Wileyb2cClassificationService;


/**
 * Created by Uladzimir_Barouski on 2/28/2017.
 */
public class Wileyb2cProductDetailsPopulator implements Populator<ProductModel, ProductData> {
  @Resource
  private Wileyb2cClassificationService wileyb2cClassificationService;

  @Override
  public void populate(final ProductModel productModel, final ProductData productData)
      throws ConversionException {
    ServicesUtil.validateParameterNotNullStandardMessage("productModel", productModel);
    ServicesUtil.validateParameterNotNullStandardMessage("productData", productData);

    productData.setPublicationDate(wileyb2cClassificationService.resolveAttributeValue(productModel,
        Wileyb2cClassificationAttributes.PUBLICATION_DATE));
    productData.setIsbn13(wileyb2cClassificationService.resolveAttribute(productModel,
        Wileyb2cClassificationAttributes.ISBN13));
    productData.setPagesNumber(wileyb2cClassificationService.resolveAttribute(productModel,
        Wileyb2cClassificationAttributes.NUMBER_OF_PAGES));
    productData.setPrintIssn(wileyb2cClassificationService.resolveAttribute(productModel,
        Wileyb2cClassificationAttributes.PRINT_ISSN));
    productData.setOnlineIssn(wileyb2cClassificationService.resolveAttribute(productModel,
        Wileyb2cClassificationAttributes.ONLINE_ISSN));
    productData.setVolumeAndIssues(wileyb2cClassificationService.resolveAttribute(productModel,
        Wileyb2cClassificationAttributes.VOLUME_AND_ISSUE));
    productData.setImpactFactor(wileyb2cClassificationService.resolveAttribute(productModel,
        Wileyb2cClassificationAttributes.IMPACT_FACTOR));
    productData.setSocietyLink(wileyb2cClassificationService.resolveAttributeValue(productModel,
        Wileyb2cClassificationAttributes.SOCIETY_LINK));

    if (productModel instanceof WileyPurchaseOptionProductModel) {
      final WileyPurchaseOptionProductModel variantProduct = (WileyPurchaseOptionProductModel) productModel;
      final WileyProductModel wileyProduct = (WileyProductModel) variantProduct.getBaseProduct();
      productData.setShowImpactFactor(wileyProduct.getShowImpactFactor());
    }

  }

}
