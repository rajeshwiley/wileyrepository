package com.wiley.facades.voucher.impl;

import de.hybris.platform.commercefacades.voucher.data.VoucherData;
import de.hybris.platform.commercefacades.voucher.exceptions.VoucherOperationException;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.couponfacades.facades.impl.DefaultCouponFacade;
import de.hybris.platform.couponservices.service.data.CouponResponse;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.wiley.facades.voucher.WileyCouponFacade;


public class WileyCouponFacadeImpl extends DefaultCouponFacade implements WileyCouponFacade
{
	protected static final Logger LOG = Logger.getLogger(WileyCouponFacadeImpl.class);

	/**
	 * Get cart voucher - as only one voucher could be applied we return only one
	 *
	 * @return voucher or null
	 */
	@Override
	public VoucherData getVoucherForCart()
	{
		final List<VoucherData> vouchers = getVouchersForCart();
		if (!vouchers.isEmpty() && vouchers.iterator().hasNext())
		{
			return vouchers.iterator().next();
		}
		return null;
	}

	@Override
	public void removeVouchersFromCart()
	{
		final List<VoucherData> vouchers = getVouchersForCart();
		for (VoucherData voucher : vouchers)
		{
			try
			{
				releaseVoucher(voucher.getCode());
			}
			catch (VoucherOperationException e)
			{
				LOG.error("Cannot release voucher with code '" + voucher.getCode() + "': " + e.getMessage());
			}
		}
	}

	@Override
	public Collection<String> removeNotValidVouchersFromCart()
	{
		final CartModel cart = getCartService().getSessionCart();
		final Collection<String> appliedCouponCodes = cart.getAppliedCouponCodes();
		Set<String> releasedCoupons = new HashSet<>();
		if (appliedCouponCodes != null)
		{
			for (String couponCode : appliedCouponCodes)
			{
				if (!getCouponService().getValidatedCouponForCode(couponCode).isPresent())
				{
					getCouponService().releaseCouponCode(couponCode, cart);
					releasedCoupons.add(couponCode);
				}
			}
		}
		return releasedCoupons;
	}

	@Override
	public boolean canApplyNewVoucher(final String newDiscountCode) throws VoucherOperationException
	{
		try
		{
			final CouponResponse couponResponse = getCouponService().verifyCouponCode(newDiscountCode,
					getCartService().getSessionCart());
			return couponResponse.getSuccess();
		}
		catch (IllegalArgumentException e)
		{
			throw new VoucherOperationException("Voucher with code '" + newDiscountCode + "' not found", e);
		}
	}
}
