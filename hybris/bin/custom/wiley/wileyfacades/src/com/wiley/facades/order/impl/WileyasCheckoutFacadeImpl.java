package com.wiley.facades.order.impl;

import com.wiley.facades.order.WileyCheckoutFacade;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.order.InvalidCartException;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public class WileyasCheckoutFacadeImpl extends WileyCheckoutFacadeImpl implements WileyCheckoutFacade
{
    @Override
    @Transactional
    public OrderData placeOrder() throws InvalidCartException
    {
        return super.placeOrder();
    }
}
