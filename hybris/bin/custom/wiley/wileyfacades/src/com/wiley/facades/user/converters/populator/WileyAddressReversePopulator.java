package com.wiley.facades.user.converters.populator;


import de.hybris.platform.commercefacades.user.converters.populator.AddressReversePopulator;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;


public class WileyAddressReversePopulator extends AddressReversePopulator
{
	@Override
	public void populate(final AddressData addressData, final AddressModel addressModel) throws ConversionException
	{
		super.populate(addressData, addressModel);
		if (addressData.getRegion() == null)
		{
			addressModel.setRegion(null);
		}
		addressModel.setDepartment(addressData.getDepartment());
		addressModel.setEmail(addressData.getEmail());
	}
}
