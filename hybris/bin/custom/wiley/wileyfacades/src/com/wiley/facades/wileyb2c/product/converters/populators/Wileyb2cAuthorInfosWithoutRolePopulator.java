package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.platform.commercefacades.product.converters.populator.AbstractProductPopulator;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import javax.annotation.Resource;

import com.wiley.facades.wileyb2c.product.converters.populators.helper.Wileyb2cAuthorPopulatorHelper;


public class Wileyb2cAuthorInfosWithoutRolePopulator extends AbstractProductPopulator<ProductModel, ProductData>
{
	@Resource
	private Wileyb2cAuthorPopulatorHelper wileyb2cAuthorPopulatorHelper;

	@Override
	public void populate(final ProductModel productModel, final ProductData productData) throws ConversionException
	{
		ServicesUtil.validateParameterNotNullStandardMessage("productModel", productModel);
		ServicesUtil.validateParameterNotNullStandardMessage("productData", productData);

		productData.setAuthorsWithoutRoles(wileyb2cAuthorPopulatorHelper.getAuthorsWithoutRoles(productModel));
	}
}
