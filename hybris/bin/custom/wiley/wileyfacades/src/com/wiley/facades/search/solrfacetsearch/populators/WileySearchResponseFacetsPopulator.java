package com.wiley.facades.search.solrfacetsearch.populators;

import de.hybris.platform.commerceservices.search.facetdata.FacetData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.populators.SearchResponseFacetsPopulator;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.search.Breadcrumb;
import de.hybris.platform.solrfacetsearch.search.SearchResult;
import de.hybris.platform.solrfacetsearch.search.impl.SolrSearchResult;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;

import com.wiley.core.search.solrfacetsearch.WileySearchQuery;


public class WileySearchResponseFacetsPopulator extends SearchResponseFacetsPopulator
{

	@Override
	protected List<FacetData<SolrSearchQueryData>> buildFacets(final SearchResult solrSearchResult,
			final SolrSearchQueryData searchQueryData, final IndexedType indexedType)
	{
		List<FacetData<SolrSearchQueryData>> result = superBuildFacets(solrSearchResult, searchQueryData, indexedType);
		filterFacetByQueryFacetCode(result, (SolrSearchResult) solrSearchResult);
		buildFacetHasMoreFacetValuesDataAndRemoveExtraValues(result, (SolrSearchResult) solrSearchResult);
		return result;
	}

	protected List<FacetData<SolrSearchQueryData>> superBuildFacets(final SearchResult solrSearchResult,
			final SolrSearchQueryData searchQueryData,
			final IndexedType indexedType)
	{
		return super.buildFacets(solrSearchResult, searchQueryData, indexedType);
	}

	protected void filterFacetByQueryFacetCode(final List<FacetData<SolrSearchQueryData>> result,
			final SolrSearchResult solrSearchResult)
	{
		WileySearchQuery wileySearchQuery = (WileySearchQuery) solrSearchResult.getSearchQuery();
		String searchQueryFacetCode = wileySearchQuery.getFacetCode();
		if (searchQueryFacetCode != null)
		{
			List<FacetData<SolrSearchQueryData>> extraFacets = result.stream()
					.filter(facetData -> !searchQueryFacetCode.equals(facetData.getCode()))
					.collect(Collectors.toList());
			result.removeAll(extraFacets);
		}
	}

	protected void buildFacetHasMoreFacetValuesDataAndRemoveExtraValues(final List<FacetData<SolrSearchQueryData>> result,
			final SolrSearchResult solrSearchResult)
	{
		WileySearchQuery wileySearchQuery = (WileySearchQuery) solrSearchResult.getSearchQuery();
		List<Breadcrumb> breadcrumbs = solrSearchResult.getBreadcrumbs();
		Integer searchQueryFacetLimit = wileySearchQuery.getFacetLimit();
		boolean limitProvided = searchQueryFacetLimit != null && searchQueryFacetLimit >= 0;
		if (limitProvided)
		{
			result.forEach(facetData -> {
				if (CollectionUtils.isNotEmpty(facetData.getValues()))
				{
					boolean hasMoreFacetValues = hasMoreFacetValues(facetData, breadcrumbs, searchQueryFacetLimit);
					facetData.setHasMoreFacetValues(hasMoreFacetValues);
					if (hasMoreFacetValues)
					{
						facetData.setValues(
								facetData.getValues().stream().limit(searchQueryFacetLimit).collect(Collectors.toList()));
					}
				}
			});
		}
	}

	private boolean hasMoreFacetValues(final FacetData<SolrSearchQueryData> facetData, final List<Breadcrumb> breadcrumbs,
			final Integer searchQueryFacetLimit)
	{
		String facetCode = facetData.getCode();
		int breadcrumbsSize = breadcrumbs.stream()
				.filter(breadcrumb -> breadcrumb.getFieldName().equals(facetCode))
				.collect(Collectors.toList())
				.size();
		return facetData.getValues().size() + breadcrumbsSize > searchQueryFacetLimit;
	}

	@Override
	protected SolrSearchQueryData cloneSearchQueryData(final SolrSearchQueryData source)
	{
		final SolrSearchQueryData target = super.cloneSearchQueryData(source);
		target.setPageType(source.getPageType());
		target.setFacetCode(source.getFacetCode());
		target.setUseFacetViewMoreLimit(source.getUseFacetViewMoreLimit());
		target.setFacetLimit(source.getFacetLimit());

		return target;
	}
}
