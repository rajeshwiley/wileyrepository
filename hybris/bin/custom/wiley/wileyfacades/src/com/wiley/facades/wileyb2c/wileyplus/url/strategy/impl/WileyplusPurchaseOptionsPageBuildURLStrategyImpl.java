package com.wiley.facades.wileyb2c.wileyplus.url.strategy.impl;


import de.hybris.platform.acceleratorservices.urlresolver.SiteBaseUrlResolutionService;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.site.BaseSiteService;

import java.net.MalformedURLException;
import java.net.URISyntaxException;

import javax.annotation.Resource;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.client.utils.URIBuilder;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wiley.facades.wileyb2c.wileyplus.url.strategy.WileyplusPurchaseOptionsPageBuildURLStrategy;
import com.wiley.facades.wileyb2c.wileyplus.url.strategy.WileyplusPurchaseOptionsPageChecksumStrategy;
import com.wiley.facades.wileyplus.cart.activation.dto.CartActivationRequestDto;

import static com.wiley.facades.wileyb2c.wileyplus.url.strategy.WileyplusConstants.B2C_SITE_ID;
import static com.wiley.facades.wileyb2c.wileyplus.url.strategy.WileyplusConstants.CART_ACTIVATION_PARAM;
import static com.wiley.facades.wileyb2c.wileyplus.url.strategy.WileyplusConstants.CHECKSUM_PARAM;
import static com.wiley.facades.wileyb2c.wileyplus.url.strategy.WileyplusConstants.PURCHASE_OPTIONS_PAGE_URL;


public class WileyplusPurchaseOptionsPageBuildURLStrategyImpl implements WileyplusPurchaseOptionsPageBuildURLStrategy
{
	private static final Logger LOG = Logger.getLogger(WileyplusPurchaseOptionsPageBuildURLStrategyImpl.class);

	@Resource
	private ConfigurationService configurationService;
	@Resource
	private BaseSiteService baseSiteService;
	@Resource
	private SiteBaseUrlResolutionService siteBaseUrlResolutionService;
	@Resource
	private WileyplusPurchaseOptionsPageChecksumStrategy wileyplusPurchaseOptionsPageChecksumStrategy;

	@Override
	public String buildURL(final CartActivationRequestDto cartActivationRequestDto)
			throws URISyntaxException, MalformedURLException
	{
		final String cartActivationQueryParams = buildCartActivationQueryParams(cartActivationRequestDto);

		if (cartActivationQueryParams == null)
		{
			throw new MalformedURLException("Unable to build a URI: cartActivationQueryParams is NULL");
		}

		final byte[] encodedQuery = Base64.encodeBase64(cartActivationQueryParams.getBytes());
		final URIBuilder uriBuilder = new URIBuilder(getBaseUrl());

		uriBuilder.addParameter(CART_ACTIVATION_PARAM, new String(encodedQuery));
		uriBuilder.addParameter(CHECKSUM_PARAM,
				wileyplusPurchaseOptionsPageChecksumStrategy.calculateChecksum(cartActivationQueryParams));

		return uriBuilder.toString();
	}

	private String buildCartActivationQueryParams(final CartActivationRequestDto cartActivationRequestDto)
	{
		try
		{
			return new ObjectMapper().writeValueAsString(cartActivationRequestDto);
		}
		catch (JsonProcessingException e)
		{
			LOG.error(e);
		}
		return null;
	}

	private String getBaseUrl()
	{
		final BaseSiteModel site = baseSiteService.getBaseSiteForUID(B2C_SITE_ID);
		return siteBaseUrlResolutionService.getWebsiteUrlForSite(site, true, PURCHASE_OPTIONS_PAGE_URL);
	}
}
