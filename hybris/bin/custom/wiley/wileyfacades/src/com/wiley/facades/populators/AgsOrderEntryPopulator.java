package com.wiley.facades.populators;

import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.subscriptionservices.model.SubscriptionProductModel;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Required;


/** Overridden to restore ProductDescription in ProductData */
public class AgsOrderEntryPopulator implements Populator<AbstractOrderEntryModel, OrderEntryData>
{
	private Populator<ProductModel, ProductData> productDescriptionPopulator;
	private Populator<ProductModel, ProductData> productExternalUrlPopulator;

	@Override
	public void populate(@Nonnull final AbstractOrderEntryModel source, @Nonnull final OrderEntryData target)
	{
		if (target.getProduct() != null && source.getProduct() instanceof SubscriptionProductModel)
		{
			productDescriptionPopulator.populate(source.getProduct(), target.getProduct());
			productExternalUrlPopulator.populate(source.getProduct(), target.getProduct());
		}
	}

	@Required
	public void setProductExternalUrlPopulator(final Populator<ProductModel, ProductData> productExternalUrlPopulator)
	{
		this.productExternalUrlPopulator = productExternalUrlPopulator;
	}

	@Required
	public void setProductDescriptionPopulator(final Populator<ProductModel, ProductData> productDescriptionPopulator)
	{
		this.productDescriptionPopulator = productDescriptionPopulator;
	}
}
