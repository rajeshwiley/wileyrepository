package com.wiley.facades.welags.customer;

import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;

import com.wiley.core.customer.ResetPasswordRedirectType;


public interface WelAgsCustomerFacade extends CustomerFacade
{
	/**
	 * Sends a forgotten password request for the customer specified.
	 *
	 * @param uid
	 * 		the uid of the customer to send the forgotten password mail for
	 * @param type
	 * 		type of forgotten password action
	 * @throws de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException
	 */
	void forgottenPassword(String uid, ResetPasswordRedirectType type);


	boolean checkPassword(String password);

	/**
	 * Update customer firtsName, lastName, fullName, email
	 *
	 * @param customerData
	 * 		- data to update current customer
	 * @throws DuplicateUidException
	 * 		- in case there is existent email
	 */
	void updateCustomerProfile(CustomerData customerData) throws DuplicateUidException;

	/**
	 * Extended OOTB method for login.
	 *
	 * @param recalculateCart
	 * 		Where cart recalculation should happen during cart modification
	 */
	void loginSuccess(boolean recalculateCart);

	/**
	 * Update customer billing address country and current Employer / School fields
	 *
	 * @param countryIso
	 * 		- country to update current customer
	 * @param currentEmployerSchool
	 * 		- currentEmployerSchool to update current customer
	 * @throws DuplicateUidException
	 */
	void updateCustomerCountryAndEmployerSchoolFields(String countryIso, String currentEmployerSchool)
			throws DuplicateUidException;


	/**
	 * @param parameterPassword
	 * @return CustomerData with customer's first name and last name if given password is correct
	 */
	CustomerData checkPasswordGetCutomerName(String parameterPassword);




}
