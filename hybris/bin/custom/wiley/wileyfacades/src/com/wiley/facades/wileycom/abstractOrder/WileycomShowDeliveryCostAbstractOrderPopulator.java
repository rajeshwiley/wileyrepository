package com.wiley.facades.wileycom.abstractOrder;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.core.model.order.AbstractOrderModel;

import com.wiley.facades.order.converters.populator.WileyShowDeliveryCostAbstractOrderPopulator;


/**
 * Created by Mikhail_Asadchy on 10/11/2016.
 */
public class WileycomShowDeliveryCostAbstractOrderPopulator extends WileyShowDeliveryCostAbstractOrderPopulator
{
	@Override
	protected boolean calculateShowDeliveryCost(final AbstractOrderModel source, final AbstractOrderData target)
	{
		return super.calculateShowDeliveryCost(source, target)
				&& source.getDeliveryAddress() != null
				&& source.getDeliveryMode() != null;
	}
}
