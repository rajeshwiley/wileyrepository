package com.wiley.facades.wileyb2c.order;

import de.hybris.platform.commercefacades.order.OrderFacade;
import de.hybris.platform.core.model.order.OrderEntryModel;

import javax.annotation.Nonnull;


/**
 * Wiley B2C specific Order Facade
 */
public interface Wileyb2cOrderFacade extends OrderFacade
{
	/**
	 * To receive an order code for a GUID
	 * @param guid order guid
	 * @return order code
	 */
	String getOrderCodeForGUID(String guid);

	/**
	 * To receive an order entry by order code and entry number
	 * @param orderCode order code
	 * @param entryNumber entry number
	 * @return order entry
	 */
	OrderEntryModel getOrderEntryByOrderCodeAndEntryNumber(@Nonnull String orderCode, @Nonnull Integer entryNumber);
}
