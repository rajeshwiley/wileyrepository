package com.wiley.facades.order;

import de.hybris.platform.commercefacades.order.CheckoutFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;

import java.util.Date;
import java.util.List;

import com.wiley.core.enums.PaymentModeEnum;
import com.wiley.facades.payment.PaymentAuthorizationResultData;
import com.wiley.facades.payment.data.PaymentModeData;


public interface WileyCheckoutFacade extends CheckoutFacade
{
	/**
	 * Check if cart in current session keeps only and only digital products
	 *
	 * @return true, if only digital products in the cart
	 */
	boolean isDigitalSessionCart();

	/**
	 * The method to check if the total cost of the order is zero.
	 *
	 * @return true if orderTotal is zero.
	 */
	boolean isZeroTotalOrder();

	/**
	 * Method performs the following actions: - sets this address into a cart
	 *
	 * @param addressModel
	 */
	void setPaymentAddress(AddressModel addressModel);

	/**
	 * Method converts and sets payment data into cart
	 *
	 * @param addressData
	 */
	void setPaymentAddressData(AddressData addressData);

	/**
	 * Authorizes the payment. The order must have payment details set on it
	 * before the payment can be authorized.
	 *
	 * @param securityCode
	 * 		the 3 or 4 number CV2 or CVV security code
	 * @return code - the result of authorization operation
	 */
	PaymentAuthorizationResultData authorizePaymentAndProvideResult(String securityCode);

	/**
	 * Check if student's checkout flow
	 *
	 * @return boolean
	 */
	boolean isStudentFlow();

	/**
	 * Check if student's checkout flow
	 *
	 * @param cartCode
	 * @return boolean
	 */
	boolean isStudentFlow(String cartCode);

	/**
	 * Check if student's cart has not empty university name
	 *
	 * @return boolean
	 */
	boolean isCartHasStudentVerification();

	/**
	 * Detects whether current checkout flow is operated by PayPal
	 *
	 * @return true for PayPal flow
	 */
	boolean isPaypalCheckout();

	/**
	 * Method adding invoice payment details to the cart
	 *
	 * @param userNotes
	 * @param purchaseOrderNumber
	 */
	void setInvoicePaymentDetails(String userNotes, String purchaseOrderNumber, String paymentMode);

	/**
	 * Gets address data by Address ID
	 *
	 * @param addressId
	 * @return AddressData
	 */
	AddressData getAddressDatabyId(String addressId);

	/**
	 * Method adding payment mode to the cart
	 *
	 * @param paymentMode
	 */
	void setPaymentMode(PaymentModeEnum paymentMode);

	/**
	 * Method sets tax exemption details into cart
	 *
	 * @param taxNumber
	 * @param taxNumberExpirationDate
	 */
	void setTaxExemptionDetails(String taxNumber, Date taxNumberExpirationDate);

	/**
	 * Method sets tax registration details into cart
	 *
	 * @param taxNumber
	 */
	void setVatRegistrationDetails(String taxNumber);

	/**
	 * Method sets tax number validated flag into cart
	 *
	 * @param countryIso
	 * @param taxNumber
	 */
	void validateTaxInformation(String countryIso, String taxNumber);

	/**
	 * Sets cart country from billing address form during checkout
	 *
	 * @param countryIso
	 */
	void setCartCountry(String countryIso);

	/**
	 * Set default currency in cart for current payment address
	 *
	 * @return boolean cart currency was changed
	 */
	boolean setDefaultCurrencyInCartForCurrentPaymentAddress();


	/**
	 * Method retrieving supported payment modes
	 *
	 * @param orderModel
	 */
	List<PaymentModeData> getSupportedPaymentModes(AbstractOrderModel orderModel);

	/**
	 * Saves into session hop payment url. Is used for redirecting on the payment method step
	 *
	 * @param hopPaymentUrl
	 */
	void saveHopPaymentUrl(String hopPaymentUrl);

	/**
	 * Returns hop payment url saved in session. Is used for redirecting on the payment method step
	 *
	 * @return
	 */
	String getSavedHopPaymentUrlAndRemove();

	/**
	 * Saves url could be used for redirect in case payment provider returns error response
	 *
	 * @param hopPaymentReturnUrl
	 */
	void saveHopPaymentReturnUrl(String hopPaymentReturnUrl);

	/**
	 * Returns url could be used for redirect in case payment provider returns error response
	 * Value is also cleared from the session
	 *
	 * @return
	 */
	String getSavedHopPaymentReturnUrlAndRemove();

	/**
	 * Calculate session cart
	 */
	void calculateCart();

	void resetCartPaymentInfo();

	boolean isNonZeroOrder();

	/**
	 * Clear invoice/proforma payment data if different payment method is chosen.
	 */
	void clearInvoicePaymentData();

	boolean isTaxAvailable();

	boolean checkSelectedPaymentMethodIsAvailable(CartData cartData);

}
