package com.wiley.facades.wileyb2c.search.solrfacetsearch.converters.populator;

import de.hybris.platform.commercefacades.search.data.SearchQueryData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryTermData;
import de.hybris.platform.converters.Populator;

import java.util.List;

import org.apache.commons.lang.StringUtils;


public class Wileyb2cSolrSearchQueryEncoderPopulator implements Populator<SolrSearchQueryData, SearchQueryData>
{
	@Override
	public void populate(final SolrSearchQueryData source, final SearchQueryData target)
	{
		final StringBuilder builder = new StringBuilder();

		if (source != null)
		{
			if (StringUtils.isNotBlank(source.getFreeTextSearch()))
			{
				builder.append(source.getFreeTextSearch());
			}

			builder.append('|');

			if (StringUtils.isNotBlank(source.getSort()))
			{
				builder.append(source.getSort());
			}

			final List<SolrSearchQueryTermData> terms = source.getFilterTerms();

			if (terms != null)
			{
				terms.stream()
						.filter(term -> StringUtils.isNotBlank(term.getKey()) && StringUtils.isNotBlank(term.getValue()))
						.forEach(term -> builder.append('|').append(term.getKey()).append(':').append(term.getValue()));
			}
		}

		final String result = builder.toString();

		// Special case for empty query
		if ("|".equals(result))
		{
			target.setValue("");
		}
		else
		{
			target.setValue(result);
		}
	}
}
