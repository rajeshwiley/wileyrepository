package com.wiley.facades.order.converters.populator;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import com.wiley.core.order.WileyCheckoutService;


/**
 * Created by Uladzimir_Barouski on 3/3/2016.
 */
public class WileyStudentAbstractOrderPopulator implements Populator<AbstractOrderModel, AbstractOrderData>
{
	@Autowired
	private WileyCheckoutService wileyCheckoutService;

	@Override
	public void populate(@Nonnull final AbstractOrderModel source, @Nonnull final AbstractOrderData target)
			throws ConversionException
	{
		Assert.notNull(source);
		Assert.notNull(target);

		target.setStudentOrder(wileyCheckoutService.isStudentOrder(source));
	}
}
