package com.wiley.facades.wileyb2c.order.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Resource;

import com.wiley.core.model.WileyProductSummaryModel;
import com.wiley.facades.wileyplus.cart.activation.dto.CartActivationRequestDto;
import com.wiley.facades.wileyplus.cart.activation.dto.PurchaseOptionsDto;
import com.wiley.facades.wileyplus.cart.activation.dto.WileyPlusCourseDto;

public class WileyOrderEntryCoursePopulator implements Populator<CartActivationRequestDto, AbstractOrderEntryModel>
{

	@Resource
	private ModelService modelService;

	@Override
	public void populate(final CartActivationRequestDto cartActivationRequestDto, final AbstractOrderEntryModel entry)
			throws ConversionException
	{
		final WileyPlusCourseDto course = cartActivationRequestDto.getCourse();
		final WileyProductSummaryModel productSummaryModel = modelService.create(WileyProductSummaryModel.class);
		entry.setProductSummary(productSummaryModel);

		productSummaryModel.setAuthors(course.getAuthors());
		productSummaryModel.setName(course.getTitle());
		productSummaryModel.setPictureUrl(course.getImageUrl());
		productSummaryModel.setEdition(course.getProductEdition());
		final PurchaseOptionsDto purchaseOptionsDto = cartActivationRequestDto.getPurchaseOptionsDto();
		if (purchaseOptionsDto != null)
		{
			productSummaryModel.setFreeTrialPeriod(purchaseOptionsDto.getGracePeriodDuration());
		}
		entry.setAdditionalInfo(cartActivationRequestDto.getAdditionalInfo());
		//populateExtraInfo(cartActivationRequestDto, entry);
	}

/* private void populateExtraInfo(final CartActivationRequestDto cartActivationRequestDto, final AbstractOrderEntryModel entry)
 {
    Map<String, String> extraParameters = new HashMap<>();
    extraParameters.put("extraInfoUserId", cartActivationRequestDto.getUserId());
    extraParameters.put("extraClassSectionId", cartActivationRequestDto.getClassSectionId());

    for (KeyValueDto extraParameter : cartActivationRequestDto.getExtraInfo()) {
      extraParameters.put(extraParameter.getKey(), extraParameter.getValue());
    }
    entry.setExtraInfo(extraParameters);
  }*/
}
