package com.wiley.facades.urlresolver.impl;

import de.hybris.platform.acceleratorservices.urlresolver.SiteBaseUrlResolutionService;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.locale.WileyLocaleService;
import com.wiley.facades.common.LocalizedUrlData;
import com.wiley.facades.urlresolver.WileyUrlResolutionFacade;


public class WileyUrlResolutionFacadeImpl implements WileyUrlResolutionFacade
{

	private static final Logger LOG = LoggerFactory.getLogger(WileyUrlResolutionFacadeImpl.class);

	public static final String X_DEFAULT = "x-default";

	@Autowired
	private WileyLocaleService wileyLocaleService;
	@Autowired
	private BaseSiteService baseSiteService;
	@Autowired
	private SiteBaseUrlResolutionService siteBaseUrlResolutionService;
	@Autowired
	private BaseStoreService baseStoreService;

	@Override
	public List<LocalizedUrlData> getAllLocalizedUrls(final String servletPath, final boolean secure)
	{
		List<String> locales = wileyLocaleService.getAllEncodedLocales();
		locales = removeLocalesWithSiteRedirects(locales);
		final List<LocalizedUrlData> localizedUrls = locales.stream()
				.map(locale -> populateLocalizedUrlData(servletPath, secure, locale))
				.collect(Collectors.toList());
		addDefaultLocalizedUrlData(servletPath, secure, localizedUrls);
		return localizedUrls;
	}

	private void addDefaultLocalizedUrlData(final String servletPath, final boolean secure,
			final List<LocalizedUrlData> localizedUrls)
	{
		final LocalizedUrlData defaultLocalizedUrlData = populateLocalizedUrlData(servletPath, secure,
				wileyLocaleService.getDefaultEncodedLocale());
		defaultLocalizedUrlData.setLocale(X_DEFAULT);
		localizedUrls.add(defaultLocalizedUrlData);
	}

	private List<String> removeLocalesWithSiteRedirects(final List<String> locales)
	{
		List<String> filteredLocales = locales;
		final BaseStoreModel currentBaseStore = baseStoreService.getCurrentBaseStore();
		if (CollectionUtils.isNotEmpty(currentBaseStore.getExternalLinks()))
		{
			filteredLocales = locales.stream().filter(locale ->
			{
				CountryModel countryFromLocale = wileyLocaleService.getCountryForEncodedLocale(locale);

				return currentBaseStore.getExternalLinks()
						.stream()
						.noneMatch(link ->
								StringUtils.equals(countryFromLocale.getIsocode(), link.getCountry().getIsocode()));
			}).collect(Collectors.toList());
		}
		return filteredLocales;
	}

	private LocalizedUrlData populateLocalizedUrlData(final String path, final boolean secure, final String locale)
	{
		final LocalizedUrlData urlData = new LocalizedUrlData();
		final BaseSiteModel site = baseSiteService.getCurrentBaseSite();
		final String encodingAttributes = "/" + locale;
		final String url = siteBaseUrlResolutionService.getWebsiteUrlForSite(site, encodingAttributes, secure, path);
		urlData.setLocale(locale);
		urlData.setUrl(url);
		return urlData;
	}
}
