package com.wiley.facades.process.email.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.util.Config;

import com.wiley.core.model.cartprocessing.CartProcessModel;


/**
 * Velocity context for a order notification email.
 */
public class AbandonCartEmailContext extends AbstractEmailContext<CartProcessModel>
{
	private static final String WEL_EFFICIENTLEARNING_URL_HTTP = "wel.efficientlearning.url.http";
	private static final String WEL_EFFICIENTLEARNING_URL_HTTPS = "wel.efficientlearning.url.https";
	private Converter<CartModel, CartData> cartConverter;
	private CartData cartData;
	private String efficientLearningLink;
	private String efficientLearningLinkHttps;
	private String customerFirstName;

	/**
	 * Init.
	 *
	 * @param cartProcessModel
	 * 		the cart process model
	 * @param emailPageModel
	 * 		the email page model
	 */
	@Override
	public void init(final CartProcessModel cartProcessModel, final EmailPageModel emailPageModel)
	{
		super.init(cartProcessModel, emailPageModel);
		cartData = getCartConverter().convert(cartProcessModel.getCart());
		setEfficientLearningLink(Config.getParameter(WEL_EFFICIENTLEARNING_URL_HTTP));
		setEfficientLearningLinkHttps(Config.getParameter(WEL_EFFICIENTLEARNING_URL_HTTPS));
		this.customerFirstName = extractCustomerFirstName(cartProcessModel);
	}

	/**
	 * Gets site.
	 *
	 * @param cartProcessModel
	 * 		the cart process model
	 * @return the site
	 */
	@Override
	protected BaseSiteModel getSite(final CartProcessModel cartProcessModel)
	{
		return cartProcessModel.getCart().getSite();
	}

	@Override
	protected CustomerModel getCustomer(final CartProcessModel businessProcessModel)
	{

		return (CustomerModel) businessProcessModel.getCart().getUser();
	}

	@Override
	protected LanguageModel getEmailLanguage(final CartProcessModel businessProcessModel)
	{
		return businessProcessModel.getCart().getStore().getDefaultLanguage();
	}

	/**
	 * Gets customer first name.
	 *
	 * @param cartProcessModel
	 * 		the order process model
	 * @return the customer
	 */
	protected String extractCustomerFirstName(final CartProcessModel cartProcessModel)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("cartProcessModel.cart", cartProcessModel.getCart());
		CustomerModel customer = (CustomerModel) cartProcessModel.getCart().getUser();
		ServicesUtil.validateParameterNotNullStandardMessage("cartProcessModel.cart.customer", customer);
		return customer.getFirstName();
	}


	public String getCustomerFirstName()
	{
		return customerFirstName;
	}

	public void setCustomerFirstName(final String customerFirstName)
	{
		this.customerFirstName = customerFirstName;
	}

	public CartData getCartData()
	{
		return cartData;
	}

	public void setCartData(final CartData cartData)
	{
		this.cartData = cartData;
	}


	/**
	 * Gets efficient learning link.
	 *
	 * @return the efficient learning link
	 */
	public String getEfficientLearningLink()
	{
		return efficientLearningLink;
	}

	/**
	 * Sets efficient learning link.
	 *
	 * @param efficientLearningLink
	 * 		the efficient learning link
	 */
	public void setEfficientLearningLink(final String efficientLearningLink)
	{
		this.efficientLearningLink = efficientLearningLink;
	}


	public Converter<CartModel, CartData> getCartConverter()
	{
		return cartConverter;
	}

	public void setCartConverter(final Converter<CartModel, CartData> cartConverter)
	{
		this.cartConverter = cartConverter;
	}

	public String getEfficientLearningLinkHttps()
	{
		return efficientLearningLinkHttps;
	}

	public void setEfficientLearningLinkHttps(final String efficientLearningLinkHttps)
	{
		this.efficientLearningLinkHttps = efficientLearningLinkHttps;
	}
}
