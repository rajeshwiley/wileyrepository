/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package com.wiley.facades.populators;

import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.commercefacades.product.converters.populator.AbstractProductPopulator;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;



/**
 * Populates the product data with the products classification data
 *
 * @param <SOURCE>
 * 		product model
 * @param <TARGET>
 * 		product data
 */
public class ProductClassificationPopulator<SOURCE extends ProductModel, TARGET extends ProductData>
		extends AbstractProductPopulator<SOURCE, TARGET>
{
	private ClassificationService classificationService;
	private Populator<FeatureList, ProductData> productFeatureListPopulator;
	private Populator<FeatureList, ProductData> productIncludeFeaturesPopulator;

	protected ClassificationService getClassificationService()
	{
		return classificationService;
	}

	/**
	 * Sets classification service.
	 *
	 * @param classificationService
	 * 		the classification service
	 */
	@Required
	public void setClassificationService(final ClassificationService classificationService)
	{
		this.classificationService = classificationService;
	}

	protected Populator<FeatureList, ProductData> getProductFeatureListPopulator()
	{
		return productFeatureListPopulator;
	}

	/**
	 * Sets product feature list populator.
	 *
	 * @param productFeatureListPopulator
	 * 		the product feature list populator
	 */
	@Required
	public void setProductFeatureListPopulator(final Populator<FeatureList, ProductData> productFeatureListPopulator)
	{
		this.productFeatureListPopulator = productFeatureListPopulator;
	}

	protected Populator<FeatureList, ProductData> getProductIncludeFeaturesPopulator()
	{
		return productIncludeFeaturesPopulator;
	}

	/**
	 * Sets product include features populator.
	 *
	 * @param productIncludeFeaturesPopulator
	 * 		the product include features populator
	 */
	@Required
	public void setProductIncludeFeaturesPopulator(
			final Populator<FeatureList, ProductData> productIncludeFeaturesPopulator)
	{
		this.productIncludeFeaturesPopulator = productIncludeFeaturesPopulator;
	}

	@Override
	public void populate(final SOURCE productModel, final TARGET productData) throws ConversionException
	{
		final FeatureList featureList = getClassificationService().getFeatures(productModel);
		if (featureList != null && CollectionUtils.isNotEmpty(featureList.getFeatures()))
		{
			getProductFeatureListPopulator().populate(featureList, productData);
			getProductIncludeFeaturesPopulator().populate(featureList, productData);
		}
	}
}
