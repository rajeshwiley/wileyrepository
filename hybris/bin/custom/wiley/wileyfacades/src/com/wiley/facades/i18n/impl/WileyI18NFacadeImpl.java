package com.wiley.facades.i18n.impl;

import de.hybris.platform.commercefacades.i18n.impl.DefaultI18NFacade;
import de.hybris.platform.commercefacades.user.data.RegionData;
import de.hybris.platform.commerceservices.util.AbstractComparator;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;

import com.wiley.core.i18n.WileyCommonI18NService;
import com.wiley.facades.i18n.WileyI18NFacade;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


public class WileyI18NFacadeImpl extends DefaultI18NFacade implements WileyI18NFacade
{
	@Resource
	private BaseStoreService baseStoreService;

	@Resource
	private WileyCommonI18NService wileyCommonI18NService;

	@Override
	public List<RegionData> getRegionsForCountryIso(final String countryIso)
	{
		final List<RegionModel> regions = new ArrayList<>(getConfigurableRegionsForCurrentBaseStore(countryIso));
		regions.sort(getRegionComparator());
		return Converters.convertAll(regions, getRegionConverter());
	}

	protected AbstractComparator<? super RegionModel> getRegionComparator()
	{
		return RegionNameComparator.INSTANCE;
	}

	@Override
	public boolean isDisplayRegions(final String countryIso)
	{
		validateParameterNotNullStandardMessage("countryIso", countryIso);
		return CollectionUtils.isNotEmpty(getConfigurableRegionsForCurrentBaseStore(countryIso));
	}

	private List<RegionModel> getConfigurableRegionsForCurrentBaseStore(final String countryIso)
	{
		final BaseStoreModel currentBaseStore = getBaseStoreService().getCurrentBaseStore();
		final CountryModel country = getCommonI18NService().getCountry(countryIso);
		return getWileyCommonI18NService()
				.getConfigurableRegionsForCurrentBaseStore(country, currentBaseStore);
	}

	public BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

	public WileyCommonI18NService getWileyCommonI18NService()
	{
		return wileyCommonI18NService;
	}
}
