package com.wiley.facades.process.email.context;

/**
 * Contains more specific stuff for generation b2b order confirmation emails
 */
public class Wileyb2bOrderNotificationEmailContext extends OrderNotificationEmailContext
{
}
