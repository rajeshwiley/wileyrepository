package com.wiley.facades.wileyb2b.order;

/**
 * Wiley B2B Facade to trigger email sending with order details in PDF form
 */
public interface Wileyb2bSendEmailWithOrderDetailsFacade
{
	boolean sendEmailWithOrderDetails(String orderCode, String email);
}
