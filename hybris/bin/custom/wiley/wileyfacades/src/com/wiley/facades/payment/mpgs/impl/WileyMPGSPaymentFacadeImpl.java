package com.wiley.facades.payment.mpgs.impl;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.customer.service.WileyCustomerAccountService;
import com.wiley.core.mpgs.services.WileyMPGSMessageService;
import com.wiley.core.mpgs.services.WileyMPGSPaymentService;
import com.wiley.facades.payment.data.WileyMPGSPaymentResult;
import com.wiley.facades.payment.mpgs.WileyMPGSPaymentFacade;



public class WileyMPGSPaymentFacadeImpl implements WileyMPGSPaymentFacade
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyMPGSPaymentFacadeImpl.class);

	private WileyMPGSPaymentService wileyMPGSPaymentService;

	@Autowired
	private CartService cartService;

	@Autowired
	private WileyCustomerAccountService customerAccountService;

	@Autowired
	private WileyMPGSMessageService mpgsMessageService;

	@Autowired
	private UserService userService;

	@Autowired
	private BaseStoreService baseStoreService;

	@Override
	public WileyMPGSPaymentResult retrieveSessionAndTokenize(@Nonnull final String sessionId,
			final Boolean saveInAccount)
	{
		final CartModel cartModel = cartService.getSessionCart();
		PaymentTransactionEntryModel retrieveSessionEntry = wileyMPGSPaymentService.retrieveSession(cartModel, sessionId);
		if (!TransactionStatus.ACCEPTED.name().equals(retrieveSessionEntry.getTransactionStatus()))
		{
			return createPaymentResult(false, mpgsMessageService.getStorefrontMessageKey(retrieveSessionEntry));

		}

		PaymentTransactionEntryModel verifyEntry = wileyMPGSPaymentService.verify(cartModel, sessionId);
		if (!TransactionStatus.ACCEPTED.name().equals(verifyEntry.getTransactionStatus()))
		{
			return createPaymentResult(false, mpgsMessageService.getStorefrontMessageKey(verifyEntry));
		}

		PaymentTransactionEntryModel tokenizeEntry = wileyMPGSPaymentService.tokenizeCart(cartModel,
				retrieveSessionEntry, saveInAccount);
		final boolean isTokenizeEntrySuccessful = TransactionStatus.ACCEPTED.name()
				.equals(tokenizeEntry.getTransactionStatus());
		return createPaymentResult(isTokenizeEntrySuccessful, mpgsMessageService.getStorefrontMessageKey(tokenizeEntry));
	}

	@Override
	public WileyMPGSPaymentResult retrieveSessionAndTokenize(@Nonnull final String sessionId, final Boolean saveInAccount,
			final String orderCode)
	{
		final OrderModel cartModel = getOrderModel(orderCode);

		PaymentTransactionEntryModel retrieveSessionEntry = wileyMPGSPaymentService.retrieveSession(cartModel, sessionId);
		if (!TransactionStatus.ACCEPTED.name().equals(retrieveSessionEntry.getTransactionStatus()))
		{
			return createPaymentResult(false, mpgsMessageService.getStorefrontMessageKey(retrieveSessionEntry));
		}
		PaymentTransactionEntryModel verifyEntry = wileyMPGSPaymentService.verify(cartModel, sessionId);
		if (!TransactionStatus.ACCEPTED.name().equals(verifyEntry.getTransactionStatus()))
		{
			return createPaymentResult(false, mpgsMessageService.getStorefrontMessageKey(verifyEntry));
		}
		OrderModel orderModel = getOrderModel(orderCode);
		PaymentTransactionEntryModel tokenizeEntry = wileyMPGSPaymentService.tokenizeOrder(orderModel, retrieveSessionEntry,
				saveInAccount);
		final boolean isTokenizeEntrySuccessful = TransactionStatus.ACCEPTED.name()
				.equals(tokenizeEntry.getTransactionStatus());
		return createPaymentResult(isTokenizeEntrySuccessful, mpgsMessageService.getStorefrontMessageKey(tokenizeEntry));
	}

	private WileyMPGSPaymentResult createPaymentResult(final boolean isOperationSuccessful, final String storefrontMessageKey)
	{
		WileyMPGSPaymentResult result = new WileyMPGSPaymentResult();
		result.setIsOperationSuccessful(isOperationSuccessful);
		result.setMessageKey(storefrontMessageKey);
		return result;
	}

	protected OrderModel getOrderModel(final String orderCode)
	{
		CustomerModel currentUser = (CustomerModel) getUserService().getCurrentUser();
		BaseStoreModel currentBaseStore = getBaseStoreService().getCurrentBaseStore();
		return getCustomerAccountService().getOrderForCode(currentUser, orderCode, currentBaseStore);
	}

	protected WileyCustomerAccountService getCustomerAccountService()
	{
		return customerAccountService;
	}

	protected UserService getUserService()
	{
		return userService;
	}

	protected BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

	public WileyMPGSPaymentService getWileyMPGSPaymentService()
	{
		return wileyMPGSPaymentService;
	}

	@Required
	public void setWileyMPGSPaymentService(final WileyMPGSPaymentService wileyMPGSPaymentService)
	{
		this.wileyMPGSPaymentService = wileyMPGSPaymentService;
	}
}
