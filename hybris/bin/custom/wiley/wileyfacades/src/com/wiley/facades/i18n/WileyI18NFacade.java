package com.wiley.facades.i18n;

import de.hybris.platform.commercefacades.i18n.I18NFacade;


public interface WileyI18NFacade extends I18NFacade
{
	boolean isDisplayRegions(String countryIso);
}
