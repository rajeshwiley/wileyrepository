package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.platform.commercefacades.product.converters.populator.ProductGalleryImagesPopulator;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ImageDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;


/**
 * Created by Uladzimir_Barouski on 2/21/2017.
 */
public class Wileyb2cProductGalleryImagesPopulator extends ProductGalleryImagesPopulator
{
	@Override
	public void populate(final ProductModel productModel, final ProductData productData) throws ConversionException
	{
		super.populate(productModel, productData);
		final MediaContainerModel externalImage =
				(MediaContainerModel) getProductAttribute(productModel, ProductModel.EXTERNALIMAGE);
		if (CollectionUtils.isEmpty(productData.getImages()) && externalImage != null)
		{
			List<ImageData> externalImageList = new ArrayList<>();
			addImagesInFormats(externalImage, ImageDataType.GALLERY, 0, externalImageList);
			productData.setImages(externalImageList);
		}
	}
}
