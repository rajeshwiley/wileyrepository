package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.platform.commercefacades.product.converters.populator.AbstractProductPopulator;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.util.ServicesUtil;


/**
 * Created by Raman_Hancharou on 6/28/2017.
 */
public class Wileyb2cProductSeoMetadataPopulator<SOURCE extends ProductModel, TARGET extends ProductData>
		extends AbstractProductPopulator<SOURCE, TARGET>
{
	@Override
	public void populate(final SOURCE source, final TARGET target) throws ConversionException
	{
		ServicesUtil.validateParameterNotNullStandardMessage("productModel", source);
		ServicesUtil.validateParameterNotNullStandardMessage("productData", target);

		Object seoTitleTag = getProductAttribute(source, ProductModel.SEOTITLETAG);
		Object seoDescriptionTag = getProductAttribute(source, ProductModel.SEODESCRIPTIONTAG);
		if (seoTitleTag != null)
		{
			target.setSeoTitleTag(seoTitleTag.toString());
		}
		if (seoDescriptionTag != null)
		{
			target.setSeoDescriptionTag(seoDescriptionTag.toString());
		}
	}
}
