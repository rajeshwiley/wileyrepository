package com.wiley.facades.populators;

import de.hybris.platform.commercefacades.user.converters.populator.CustomerReversePopulator;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.Assert;


/**
 * Converter implementation for {@link CustomerData} as source and
 * {@link CustomerModel} as target type.
 * Populates Customer fields specific for Wiley. Delegates all other fields population.
 */
public class WileyCustomerReversePopulator extends CustomerReversePopulator implements Populator<CustomerData, CustomerModel>
{
	/**
	 * Populates 'name', 'first name' and 'last name' fields directly from CustomerData.
	 */
	@Override
	public void populate(final CustomerData source, final CustomerModel target) throws ConversionException
	{
		defaultPopulate(source, target);
		target.setName(source.getName());
		target.setFirstName(source.getFirstName());
		target.setLastName(source.getLastName());
	}

	/**
	 * This method performs hybris ootb population logic with additional changes
	 *
	 * @param source
	 * @param target
	 */
	protected void defaultPopulate(final CustomerData source, final CustomerModel target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		if (source.getCurrency() != null)
		{
			final String isocode = source.getCurrency().getIsocode();
			try
			{
				target.setSessionCurrency(getCommonI18NService().getCurrency(isocode));
			}
			catch (final UnknownIdentifierException e)
			{
				throw new ConversionException("No currency with the code " + isocode + " found.", e);
			}

		}
		if (source.getLanguage() != null)
		{
			final String isocode = source.getLanguage().getIsocode();
			try
			{
				target.setSessionLanguage(getCommonI18NService().getLanguage(isocode));
			}
			catch (final UnknownIdentifierException e)
			{
				throw new ConversionException("No language with the code " + isocode + " found.", e);
			}

		}

		if (target.getDefaultPaymentAddress() == null)
		{
			if (source.getDefaultBillingAddress() != null)
			{
				final AddressModel paymentAddress = getModelService().create(AddressModel.class);
				getAddressReversePopulator().populate(source.getDefaultBillingAddress(), paymentAddress);
				paymentAddress.setOwner(target);
				target.setDefaultPaymentAddress(paymentAddress);
			}
		}
		else
		{
			// adittional null check
			if (source.getDefaultBillingAddress() != null)
			{
				getAddressReversePopulator().populate(source.getDefaultBillingAddress(), target.getDefaultPaymentAddress());
			}
		}

		if (target.getDefaultShipmentAddress() == null)
		{
			if (source.getDefaultShippingAddress() != null)
			{
				final AddressModel shipmentAddress = getModelService().create(AddressModel.class);
				getAddressReversePopulator().populate(source.getDefaultShippingAddress(), shipmentAddress);
				shipmentAddress.setOwner(target);
				target.setDefaultShipmentAddress(shipmentAddress);
			}
		}
		else
		{
			// adittional null check
			if (source.getDefaultShippingAddress() != null)
			{
				getAddressReversePopulator().populate(source.getDefaultShippingAddress(), target.getDefaultShipmentAddress());
			}
		}

		target.setName(getCustomerNameStrategy().getName(source.getFirstName(), source.getLastName()));

		if (StringUtils.isNotBlank(source.getTitleCode()))
		{
			target.setTitle(getUserService().getTitleForCode(source.getTitleCode()));
		}
		else
		{
			target.setTitle(null);
		}

		setUid(source, target);
	}
}
