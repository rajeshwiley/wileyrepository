package com.wiley.facades.as.product.converters.populator;

import de.hybris.platform.commercefacades.product.converters.populator.AbstractProductPopulator;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;


public class WileyasVariantProductNamePopulator<SOURCE extends ProductModel, TARGET extends ProductData>
		extends AbstractProductPopulator<ProductModel, ProductData>
{

	@Override
	public void populate(final ProductModel source, final ProductData target) throws ConversionException
	{
		target.setName((String) getProductAttribute(source, ProductModel.NAME));
	}

}
