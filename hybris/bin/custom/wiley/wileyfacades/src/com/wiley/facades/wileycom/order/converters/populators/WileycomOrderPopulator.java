package com.wiley.facades.wileycom.order.converters.populators;

import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.core.model.order.OrderModel;

import javax.annotation.Resource;

import com.wiley.facades.order.converters.populator.WileyOrderPopulator;
import com.wiley.facades.wileycom.abstractOrder.WileycomShowDeliveryCostAbstractOrderPopulator;


/**
 * Created by Mikhail_Asadchy on 10/11/2016.
 */
public class WileycomOrderPopulator extends WileyOrderPopulator
{
	@Resource
	private WileycomShowDeliveryCostAbstractOrderPopulator wileycomShowDeliveryCostAbstractOrderPopulator;

	@Override
	public void populate(final OrderModel source, final OrderData target)
	{
		super.populate(source, target);
		wileycomShowDeliveryCostAbstractOrderPopulator.populate(source, target);
	}
}
