package com.wiley.facades.event.listener.payment;

import com.wiley.core.event.facade.payment.BasketProductAvailabilityb2bWPGHttpRequestPopulatorEvent;
import com.wiley.facades.product.data.InventoryStatusRecord;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionRequest;
import de.hybris.platform.acceleratorservices.payment.data.OrderEntryInfoData;
import de.hybris.platform.acceleratorservices.payment.data.PaymentData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationListener;

import java.util.List;

/**
 * Created by Georgii_Gavrysh on 12/20/2016.
 */
public class BasketProductAvailabilityb2bWPGHttpRequestPopulatorEventListener
        extends AbstractBasketProductAvailabilityWPGHttpRequestPopulator
        implements ApplicationListener<BasketProductAvailabilityb2bWPGHttpRequestPopulatorEvent>
{
    private Converter<CartModel, CartData> cartConverter;

    @Autowired
    @Qualifier("defaultB2BCartService")
    private CartService cartService;

    @Override
    public void onApplicationEvent(final BasketProductAvailabilityb2bWPGHttpRequestPopulatorEvent event) {
        populate(event.getFirstParameter(), event.getSecondParameter());
    }

    @Override
    public void populate(final CreateSubscriptionRequest source, final PaymentData target) throws ConversionException {
        final List<OrderEntryInfoData> orderEntries = source.getOrderInfoData().getOrderEntries();
        if (CollectionUtils.isNotEmpty(orderEntries))
        {
            addBasketEntries(orderEntries, target);
        }
    }


    private void addBasketEntries(final List<OrderEntryInfoData> orderEntries, final PaymentData target)
    {
        final CartModel cart = cartService.getSessionCart();
        final CartData cartData = cartConverter.convert(cart);

        for (int orderEntryIndex = 1; orderEntryIndex <= orderEntries.size(); orderEntryIndex++)
        {
            for (final OrderEntryData entry : cartData.getEntries())
            {
                if (entry.getProduct().getName().equals(orderEntries.get(orderEntryIndex - 1).getName()))
                {
                    final List<InventoryStatusRecord> inventoryStatusRecords = entry.getProduct().getInventoryStatus();

                    this.populatePaymentData(target, orderEntryIndex, inventoryStatusRecords);
                }
            }
        }
    }

    public void setCartConverter(
            final Converter<CartModel, CartData> cartConverter)
    {
        this.cartConverter = cartConverter;
    }

}
