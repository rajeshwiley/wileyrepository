package com.wiley.facades.as.order.converters.populator;

import com.wiley.core.integration.article.dto.ArticleDto;
import com.wiley.facades.order.data.WileyArticleData;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.util.ServicesUtil;


public class WileyasArticlePopulator implements Populator<ArticleDto, WileyArticleData>
{

	@Override
	public void populate(final ArticleDto source, final WileyArticleData target) throws ConversionException
	{
		ServicesUtil.validateParameterNotNullStandardMessage("articleDto", source);
		ServicesUtil.validateParameterNotNullStandardMessage("wileyArticleData", target);
		target.setSummary(source.getArticleName());
		target.setDoi(source.getDoi());
		target.setJournalId(source.getJournalId());
		target.setJournalName(source.getJournalName());
		target.setPictureUrl(source.getPictureUrl());
	}
}
