package com.wiley.facades.order.converters.populator;

import de.hybris.platform.commercefacades.order.converters.populator.CartModificationPopulator;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commerceservices.order.CommerceCartModification;

import javax.annotation.Resource;

import com.wiley.core.product.WileyProductService;


/**
 * Custom CartModificationPopulator for WEL site.
 *
 * @author Aliaksei_Zlobich
 */
public class WELCartModificationPopulator extends CartModificationPopulator
{

	@Resource(name = "productService")
	private WileyProductService wileyProductService;

	@Override
	public void populate(final CommerceCartModification source, final CartModificationData target)
	{
		super.populate(source, target);

		if (source.getEntry() != null && source.getEntry().getProduct() != null)
		{
			target.setCanProductBePartOfSet(getWileyProductService().canBePartOfProductSet(source.getEntry().getProduct()));
		}
		else
		{
			target.setCanProductBePartOfSet(false);
		}

	}

	public WileyProductService getWileyProductService()
	{
		return wileyProductService;
	}

	public void setWileyProductService(final WileyProductService wileyProductService)
	{
		this.wileyProductService = wileyProductService;
	}
}
