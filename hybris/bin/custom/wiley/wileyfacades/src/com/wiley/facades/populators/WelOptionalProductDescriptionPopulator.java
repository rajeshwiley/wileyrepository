package com.wiley.facades.populators;

import com.wiley.core.model.WileyVariantProductModel;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import org.springframework.util.Assert;

import javax.annotation.Nonnull;


public class WelOptionalProductDescriptionPopulator implements Populator<AbstractOrderEntryModel, OrderEntryData>
{
	@Override
	public void populate(@Nonnull final AbstractOrderEntryModel source, @Nonnull final OrderEntryData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		final ProductModel product = source.getProduct();
		if (product instanceof WileyVariantProductModel)
		{
			target.getProduct().setOptionalDescription(((WileyVariantProductModel) product).getOptionalDescription());
		}
	}
}
