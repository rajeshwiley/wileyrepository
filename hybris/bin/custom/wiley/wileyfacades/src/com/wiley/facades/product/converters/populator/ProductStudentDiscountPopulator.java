package com.wiley.facades.product.converters.populator;

import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.VariantMatrixElementData;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.enums.UserDiscountGroup;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.util.DiscountValue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import com.wiley.core.product.WileyProductDiscountService;
import com.wiley.facades.product.data.DiscountData;

import static java.util.Objects.nonNull;


/**
 * Populates price with applied student discount for base product and product variants. It uses the first student discount<br/>
 * to calculate price for students.
 */
public class ProductStudentDiscountPopulator implements Populator<ProductModel, ProductData>
{

	private static final Logger LOG = LoggerFactory.getLogger(ProductStudentDiscountPopulator.class);

	protected WileyProductDiscountService wileyProductDiscountService;

	@Resource(name = "discountValueToDiscountDataConverter")
	protected Converter<DiscountValue, DiscountData> discountValueConverter;

	@Resource
	protected CommerceCommonI18NService commerceCommonI18NService;

	@Resource
	protected PriceDataFactory priceDataFactory;

	@Override
	public void populate(@Nonnull final ProductModel productModel, @Nonnull final ProductData productData)
			throws ConversionException
	{
		Assert.notNull(productModel);
		Assert.notNull(productData);

		final CurrencyModel currentCurrency = commerceCommonI18NService.getCurrentCurrency();

		LOG.debug("Method parameters: productModel [{}], productData [{}]. Current currency [{}].", productModel.getCode(),
				productData, currentCurrency.getIsocode());

		UserDiscountGroup studentDiscountGroup = UserDiscountGroup.STUDENT;
		final List<DiscountValue> discounts = wileyProductDiscountService.getPotentialDiscountForCurrentCustomer(
				productModel, studentDiscountGroup);

		if (CollectionUtils.isNotEmpty(discounts))
		{
			LOG.debug("Populating student prices.");
			DiscountValue studentDiscount = discounts.get(0);
			// populating information about student discount
			productData.setStudentDiscount(discountValueConverter.convert(studentDiscount));

			//<editor-fold desc="Populating price for variants">
			final List<VariantOptionData> variantProducts = getVariantProducts(productData);
			populateStudentDiscountPriceForVariants(variantProducts, currentCurrency, studentDiscount);
			//</editor-fold>

			//<editor-fold desc="Populating price for single product (product without variants)">
			if (nonNull(productData.getPrice()))
			{
				populateStudentDiscountPriceForBaseProduct(productData, currentCurrency, studentDiscount);
			}
			//</editor-fold>
		}
	}

	protected void populateStudentDiscountPriceForBaseProduct(final ProductData productData, final CurrencyModel currentCurrency,
			final DiscountValue studentDiscount)
	{
		final PriceData studentPrice = createPriceDataWithAppliedDiscount(productData.getPrice(), currentCurrency,
				studentDiscount);
		productData.setStudentPrice(studentPrice);
	}

	private void populateStudentDiscountPriceForVariants(final List<VariantOptionData> variantProducts,
			final CurrencyModel currentCurrency, final DiscountValue studentDiscount)
	{
		variantProducts.stream()
				.filter(variantOption -> variantOption.getPriceData() != null)
				.forEach(variantOption ->
				{
					final PriceData studentPrice = createPriceDataWithAppliedDiscount(variantOption.getPriceData(),
							currentCurrency, studentDiscount);
					variantOption.setStudentPrice(studentPrice);
				});
	}

	protected PriceData createPriceDataWithAppliedDiscount(final PriceData priceData, final CurrencyModel currentCurrency,
			final DiscountValue discountValue)
	{
		final double appliedValue = applyDiscount(discountValue, priceData.getValue().doubleValue(),
				currentCurrency).getAppliedValue();
		return priceDataFactory.create(priceData.getPriceType(), priceData.getValue().add(BigDecimal.valueOf(-appliedValue)),
				currentCurrency);
	}

	private DiscountValue applyDiscount(final DiscountValue discount, final Double price, final CurrencyModel currency)
	{
		final int productQuantity = 1;
		return discount.apply(productQuantity, price, currency.getDigits(), currency.getIsocode());
	}

	protected List<VariantOptionData> getVariantProducts(final ProductData productData)
	{
		final List<VariantMatrixElementData> variantMatrix = productData.getVariantMatrix();
		return getLeafElements(variantMatrix).stream()
				.map(VariantMatrixElementData::getVariantOption)
				.collect(Collectors.toList());
	}

	private List<VariantMatrixElementData> getLeafElements(final List<VariantMatrixElementData> matrixElements)
	{
		if (CollectionUtils.isNotEmpty(matrixElements))
		{
			List<VariantMatrixElementData> leafElements = new ArrayList<>();
			for (VariantMatrixElementData element : matrixElements)
			{
				if (Boolean.TRUE.equals(element.getIsLeaf()))
				{
					leafElements.add(element);
				}
				else
				{
					leafElements.addAll(getLeafElements(element.getElements()));
				}
			}
			return leafElements;
		}
		return Collections.emptyList();
	}

	@Required
	public void setWileyProductDiscountService(final WileyProductDiscountService wileyProductDiscountService)
	{
		this.wileyProductDiscountService = wileyProductDiscountService;
	}
}
