package com.wiley.facades.populators;

import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.springframework.util.Assert;


/**
 * Created by Uladzimir_Barouski on 5/6/2016.
 */
public class WileycomOrderEntryDatePopulator implements Populator<AbstractOrderEntryModel, OrderEntryData>
{
	@Override
	public void populate(final AbstractOrderEntryModel abstractOrderEntryModel, final OrderEntryData orderEntryData)
			throws ConversionException
	{
		Assert.notNull(abstractOrderEntryModel, "Parameter OrderEntryModel cannot be null.");
		Assert.notNull(orderEntryData, "Parameter OrderEntryData cannot be null.");
		final AbstractOrderModel order = abstractOrderEntryModel.getOrder();
		// check for case when user remove last orderEntry from cart
		if (order != null)
		{
			orderEntryData.setPurchaseDate(order.getDate());
		}
	}
}
