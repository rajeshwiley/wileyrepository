package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.model.WileyPurchaseOptionProductModel;
import com.wiley.core.model.WileyWebLinkModel;
import com.wiley.core.wileyb2c.weblinks.WileyWebLinkService;
import com.wiley.facades.product.data.ExternalStoreData;


/**
 * Created by Raman_Hancharou on 6/15/2017.
 */
public class Wileyb2cPdpVariantPurchaseOptionExternalStoresPopulator implements Populator<VariantProductModel, VariantOptionData>
{
	@Autowired
	private WileyWebLinkService wileyWebLinkService;

	@Override
	public void populate(final VariantProductModel variantProductModel, final VariantOptionData variantOptionData)
			throws ConversionException
	{
		if (variantProductModel instanceof WileyPurchaseOptionProductModel)
		{
			final Collection<WileyWebLinkModel> externalStores = variantProductModel.getExternalStores();
			variantOptionData.setHasExternalStores(CollectionUtils.isNotEmpty(externalStores));

			List<ExternalStoreData> externalStoreDataList = new ArrayList<>();
			if (externalStores != null)
			{
				externalStores.forEach(wileyWebLink ->
				{
					final ExternalStoreData externalStoreData = new ExternalStoreData();
					externalStoreData.setType(wileyWebLink.getType().getCode());
					externalStoreData.setUrl(wileyWebLinkService.getWebLinkUrl(wileyWebLink));
					externalStoreDataList.add(externalStoreData);
				});
				variantOptionData.setExternalStores(externalStoreDataList);
			}
		}
	}
}
