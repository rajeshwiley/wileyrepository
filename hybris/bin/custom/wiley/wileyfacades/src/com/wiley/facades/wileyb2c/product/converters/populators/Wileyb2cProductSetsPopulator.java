package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.catalog.references.ProductReferenceService;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.wiley.restriction.WileyProductRestrictionService;
import com.wiley.facades.wileyb2c.product.data.ProductSetData;


/**
 * Created by Uladzimir_Barouski on 5/2/2017.
 */
public class Wileyb2cProductSetsPopulator implements Populator<ProductModel, ProductData>
{
	public static final boolean ACTIVE_ONLY_REFERENCES = true;
	@Resource
	private ProductReferenceService productReferenceService;

	private Converter<ProductModel, ProductSetData> productSetConverter;

	private WileyProductRestrictionService wileyProductRestrictionService;

	@Override
	public void populate(@Nonnull final ProductModel source, @Nonnull final ProductData target) throws ConversionException
	{
		ServicesUtil.validateParameterNotNullStandardMessage("productModel", source);
		ServicesUtil.validateParameterNotNullStandardMessage("productData", target);

		List<ProductSetData> productSetDataList = new ArrayList<>();
		Collection<ProductReferenceModel> productSets = productReferenceService.getProductReferencesForSourceProduct(source,
				ProductReferenceTypeEnum.PRODUCT_SET, ACTIVE_ONLY_REFERENCES);
		for (ProductReferenceModel productSetReference : productSets)
		{
			ProductModel productModel = productSetReference.getTarget();
			if (wileyProductRestrictionService.isAvailable(productModel)
					&& wileyProductRestrictionService.isVisible(productModel))
			{
				ProductSetData productSetData = productSetConverter.convert(productModel);
				productSetDataList.add(productSetData);
			}
		}
		target.setProductSets(productSetDataList);
	}

	@Required
	public void setProductSetConverter(
			final Converter<ProductModel, ProductSetData> productSetConverter)
	{
		this.productSetConverter = productSetConverter;
	}

	@Required
	public void setWileyProductRestrictionService(
			final WileyProductRestrictionService wileyProductRestrictionService)
	{
		this.wileyProductRestrictionService = wileyProductRestrictionService;
	}
}
