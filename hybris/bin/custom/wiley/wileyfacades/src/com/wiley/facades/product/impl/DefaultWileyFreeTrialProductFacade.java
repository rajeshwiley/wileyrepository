package com.wiley.facades.product.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;

import javax.annotation.Nullable;
import javax.annotation.Resource;

import com.wiley.core.model.WileyFreeTrialProductModel;
import com.wiley.core.product.WileyFreeTrialProductService;
import com.wiley.facades.populators.FreeTrialProductPopulator;
import com.wiley.facades.product.WileyFreeTrialProductFacade;
import com.wiley.facades.product.data.FreeTrialData;


/**
 * Created by Uladzimir_Barouski on 3/17/2016.
 */
public class DefaultWileyFreeTrialProductFacade implements WileyFreeTrialProductFacade
{
	@Resource
	private WileyFreeTrialProductService freeTrialProductService;

	@Resource
	private FreeTrialProductPopulator freeTrialProductPopulator;

	@Resource
	private ProductService productService;

	@Override
	public FreeTrialData getFreeTrialProductByCode(final String code)
	{
		WileyFreeTrialProductModel freeTrialBaseModel = freeTrialProductService.getFreeTrialProductByCode(code);
		FreeTrialData freeTrialProduct = new FreeTrialData();
		freeTrialProductPopulator.populate(freeTrialBaseModel, freeTrialProduct);
		return freeTrialProduct;
	}

	@Nullable
	@Override
	public FreeTrialData getFreeTrialForProduct(final String productCode)
	{
		ProductModel productModel = productService.getProductForCode(productCode);
		WileyFreeTrialProductModel freeTrialProductModel = null;
		if (productModel instanceof WileyFreeTrialProductModel)
		{
			freeTrialProductModel = (WileyFreeTrialProductModel) productModel;
		}
		else
		{
			freeTrialProductModel = freeTrialProductService.getRelatedFreeTrialForProduct(productModel);
		}
		FreeTrialData freeTrialProductDTO = null;
		if (freeTrialProductModel != null)
		{
			freeTrialProductDTO = new FreeTrialData();
			freeTrialProductPopulator.populate(freeTrialProductModel, freeTrialProductDTO);
		}
		return freeTrialProductDTO;
	}

}
