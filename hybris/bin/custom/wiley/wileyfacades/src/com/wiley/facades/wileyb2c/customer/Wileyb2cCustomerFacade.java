package com.wiley.facades.wileyb2c.customer;

import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;

import java.util.List;

import javax.annotation.Nonnull;

import com.wiley.facades.customer.WileycomCustomerFacade;
import com.wiley.facades.customer.data.SchoolData;


public interface Wileyb2cCustomerFacade extends WileycomCustomerFacade
{
	/**
	 * Switch session user back to anonymous user when user is converted to customer but authentication failed.
	 */
	void loginFailure();

	/**
	 * Return digital order entries for current customer
	 */
	SearchPageData<OrderEntryData> getDigitalOrderEntriesData(PageableData pageableData);

	/**
	 * Proceeds registration guest user
	 * @param pwd - password value
	 * @param orderGUID - Order id
	 * @param acceptPromotions - specifies use aggreement to receive promotions notifications
	 * @throws DuplicateUidException - Exception is thrown when an attempt to store an UID that is already assigned
	 */
	void changeGuestToCustomer(String pwd, String orderGUID,
			boolean acceptPromotions) throws DuplicateUidException;

	/**
	 * Get at maxmimum SCHOOLS_MAX_QUANTITY schools while picking one school out of every SCHOOLS_RANGE from the school list.
	 * School associated with customer (refer to {@link #getSchoolForCustomerFromLastOrder(String)}) is added to the list.
	 *
	 * @param customerUid - Customer for which the list is created
	 */
	List<SchoolData> getDisplayedSchoolsForCustomer(@Nonnull String customerUid);

	/**
	 * Extract school data from the given customer's last order, if present.
	 *
	 * @param customerUid - Customer for which the associated school is returned
	 */
	SchoolData getSchoolForCustomerFromLastOrder(@Nonnull String customerUid);

	/**
	 * Extract school data from the given customer
	 */
	SchoolData getSchoolForCustomerFromProfile(@Nonnull String customerUid);

	/**
	 * Update profile if changed.
	 *
	 * @param customerData
	 * 		the customer data
	 * @throws DuplicateUidException
	 * 		the duplicate uid exception
	 */
	void updateProfileIfChanged(CustomerData customerData) throws DuplicateUidException;
}
