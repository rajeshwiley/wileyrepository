package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.ProductReferenceData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wiley.core.wiley.restriction.WileyProductRestrictionService;


/**
 * Class populater product references which has visible targets.
 * For visibility details see <code>wileyProductRestrictionService</code> bean.
 */
public class Wileyb2cRestrictedProductReferencesPopulator implements Populator<ProductModel, ProductData>
{
	private static final Logger LOG = LoggerFactory.getLogger(Wileyb2cRestrictedProductReferencesPopulator.class);

	private Converter<ProductReferenceModel, ProductReferenceData> productReferenceConverter;
	private WileyProductRestrictionService wileyProductRestrictionService;

	@Override
	public void populate(final ProductModel source, final ProductData target) throws ConversionException
	{
		final List<ProductReferenceData> productReferences = new ArrayList<ProductReferenceData>();
		for (final ProductReferenceModel ref : source.getProductReferences())
		{
			if (ref != null)
			{
				if (wileyProductRestrictionService.isVisible(ref.getTarget()))
				{
					productReferences.add(productReferenceConverter.convert(ref));
					LOG.debug("Product with code=[{}] was converted and added to ProductData references. Reference type is [{}], "
							+ "reference pk=[{}]", ref.getTarget().getCode(), ref.getReferenceType(), ref.getPk());
				}
				else
				{
					LOG.debug("Product with code=[{}] was skipped for conversion. Reference type=[{}], reference pk=[{}]",
							ref.getTarget().getCode(), ref.getReferenceType(), ref.getPk());
				}
			}
		}

		target.setProductReferences(productReferences);

	}


	public void setProductReferenceConverter(
			final Converter<ProductReferenceModel, ProductReferenceData> productReferenceConverter)
	{
		this.productReferenceConverter = productReferenceConverter;
	}

	protected Converter<ProductReferenceModel, ProductReferenceData> getProductReferenceConverter()
	{
		return productReferenceConverter;
	}

	public void setWileyProductRestrictionService(
			final WileyProductRestrictionService wileyProductRestrictionService)
	{
		this.wileyProductRestrictionService = wileyProductRestrictionService;
	}

	public WileyProductRestrictionService getWileyProductRestrictionService()
	{
		return wileyProductRestrictionService;
	}
}
