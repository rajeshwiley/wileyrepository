package com.wiley.facades.welags.pin.exception;

import de.hybris.platform.servicelayer.exceptions.BusinessException;


/**
 * PIN operation exception.
 *
 * @author Gabor_Bata
 */
public class PinOperationException extends BusinessException
{
	private final PinOperationExceptionReason reason;

	/**
	 * Constructs a new {@link PinOperationException} instance
	 * based on the given reason.
	 *
	 * @param reason
	 * 		the exception reason
	 */
	public PinOperationException(final PinOperationExceptionReason reason)
	{
		super(reason.getMessage());
		this.reason = reason;
	}

	/**
	 * Constructs a new {@link PinOperationException} instance
	 * based on the given reason and throwable.
	 *
	 * @param reason
	 * 		the exception reason
	 * @param throwable
	 * 		the throwable
	 */
	public PinOperationException(final PinOperationExceptionReason reason, final Throwable throwable)
	{
		super(reason.getMessage(), throwable);
		this.reason = reason;
	}

	public PinOperationExceptionReason getReason()
	{
		return reason;
	}
}
