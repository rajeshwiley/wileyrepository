package com.wiley.facades.partner.strategies;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.europe1.enums.UserDiscountGroup;

import java.util.List;

public interface WileyPartnerStrategy
{

	List<ProductData> getDiscountedPartnerProducts(List<ProductData> productList, UserDiscountGroup userDiscountGroup);

}
