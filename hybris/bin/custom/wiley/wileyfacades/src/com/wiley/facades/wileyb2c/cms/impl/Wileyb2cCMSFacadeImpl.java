package com.wiley.facades.wileyb2c.cms.impl;

import de.hybris.platform.acceleratorcms.model.components.ProductReferencesComponentModel;
import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;
import de.hybris.platform.cms2.model.contents.contentslot.ContentSlotModel;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.servicelayer.data.ContentSlotData;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

import com.wiley.facades.cms.data.MarketingContentMenuItemData;
import com.wiley.facades.wileyb2c.cms.Wileyb2cCMSFacade;


public class Wileyb2cCMSFacadeImpl implements Wileyb2cCMSFacade
{
	private static final String MARKETING_INFORMATION_SLOT_POSITION_PREFIX = "MarketingInformation";

	@Resource
	private CMSPageService cmsPageService;

	@Resource
	private Populator<AbstractCMSComponentModel, MarketingContentMenuItemData> wileyb2cMarketingContentMenuItemPopulator;

	@Override
	public Map<String, List<MarketingContentMenuItemData>> getMarketingContentMenu(final AbstractPageModel pageModel,
			final ProductModel productModel)
	{
		final Map<String, List<MarketingContentMenuItemData>> marketingContentMenu = new HashMap<>();
		final List<ProductReferenceModel> productReferences = new ArrayList<>(productModel.getProductReferences());
		final Collection<ContentSlotData> contentSlots = cmsPageService.getContentSlotsForPage(pageModel)
				.stream()
				.filter(Objects::nonNull)
				.filter(s -> StringUtils.startsWith(s.getPosition(), MARKETING_INFORMATION_SLOT_POSITION_PREFIX))
				.collect(Collectors.toList());

		for (final ContentSlotData contentSlotData : contentSlots)
		{
			final ContentSlotModel contentSlot = contentSlotData.getContentSlot();
			final String contentSlotPosition = contentSlotData.getPosition();

			final List<MarketingContentMenuItemData> contentSlotMenuItems = new ArrayList<>();
			final List<ProductReferencesComponentModel> contentSlotComponents = contentSlot.getCmsComponents()
					.stream()
					.filter(Objects::nonNull)
					.filter(c -> c instanceof ProductReferencesComponentModel)
					.filter(AbstractCMSComponentModel::getVisible)
					.map(c -> (ProductReferencesComponentModel) c)
					.filter(c -> hasProductReferences(productReferences, c.getProductReferenceTypes()))
					.collect(Collectors.toList());

			for (final ProductReferencesComponentModel component : contentSlotComponents)
			{
				MarketingContentMenuItemData menuItemData = new MarketingContentMenuItemData();

				wileyb2cMarketingContentMenuItemPopulator.populate(component, menuItemData);
				menuItemData.setSlotPosition(contentSlotPosition);

				contentSlotMenuItems.add(menuItemData);
			}

			marketingContentMenu.put(contentSlotPosition, contentSlotMenuItems);
		}

		return marketingContentMenu;
	}

	protected boolean hasProductReferences(final Collection<ProductReferenceModel> productReferences,
			final List<ProductReferenceTypeEnum> productReferenceTypes)
	{
		return productReferences
				.stream()
				.filter(ProductReferenceModel::getActive)
				.map(ProductReferenceModel::getReferenceType)
				.anyMatch(r -> productReferenceTypes.stream().anyMatch(r::equals));
	}
}
