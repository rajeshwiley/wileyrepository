package com.wiley.facades.populators;

import de.hybris.platform.assistedservicefacades.customer.converters.populator.CustomersListCustomerPopulator;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.user.UserService;

import javax.annotation.Resource;


/**
 * Temporary fix for https://jira.wiley.ru/browse/ECSC-16606
 * TODO: this should be deleted after migration to Hybris version >= 6.2.0.1
 */
public class WileyCustomersListCustomerPopulator extends CustomersListCustomerPopulator
{
	@Resource
	private UserService userService;

	@Override
	public void populate(final CustomerModel source, final CustomerData target)
	{
		//anonymous user is not used in ASM  and may have a thousands of carts that may cause very bad performance result
		if (!userService.isAnonymousUser(source)) {
			super.populate(source, target);
		}
	}
}
