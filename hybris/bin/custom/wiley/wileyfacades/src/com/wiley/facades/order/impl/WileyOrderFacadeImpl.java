package com.wiley.facades.order.impl;

import de.hybris.platform.commercefacades.order.impl.DefaultOrderFacade;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;

import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.order.WileyCheckoutService;
import com.wiley.facades.order.WileyOrderFacade;


public class WileyOrderFacadeImpl extends DefaultOrderFacade implements WileyOrderFacade
{
	@Autowired
	private WileyCheckoutService wileyCheckoutService;

	@Override
	public boolean isNonZeroOrder(final String orderCode)
	{
		final OrderModel orderModel = getCustomerAccountService().getOrderForCode(
				(CustomerModel) getUserService().getCurrentUser(), orderCode, getBaseStoreService().getCurrentBaseStore());
		return wileyCheckoutService.isNonZeroOrder(orderModel);
	}

	protected WileyCheckoutService getWileyCheckoutService()
	{
		return wileyCheckoutService;
	}

}
