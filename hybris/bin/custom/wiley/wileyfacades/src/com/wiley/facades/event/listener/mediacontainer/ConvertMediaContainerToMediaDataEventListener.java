package com.wiley.facades.event.listener.mediacontainer;

import com.wiley.core.event.facade.mediacontainer.ConvertMediaContainerToMediaDataEvent;
import de.hybris.platform.acceleratorservices.urlresolver.SiteBaseUrlResolutionService;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.springframework.context.ApplicationListener;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Georgii_Gavrysh on 12/20/2016.
 */
public class ConvertMediaContainerToMediaDataEventListener
        implements ApplicationListener<ConvertMediaContainerToMediaDataEvent> {

    private static final String PAIR_DELIMITER = ":";
    private static final String PAIRS_SEPARATOR = ",";
    private static final String QUOTE_MARK = "&quot;";
    private static final String JSON_BRACKET_START = "{";
    private static final String JSON_BRACKET_FINISH = "}";

    @Resource(name = "responsiveMediaContainerConverter")
    private Converter<MediaContainerModel, List<ImageData>> mediaContainerConverter;

    @Resource
    private SiteBaseUrlResolutionService siteBaseUrlResolutionService;

    @Override
    public void onApplicationEvent(final ConvertMediaContainerToMediaDataEvent event) {
        event.setResult(convertMediaContainerToMediaData(event.getFirstParameter(), event.getSecondParameter()));
    }

    private String convertMediaContainerToMediaData(final CMSSiteModel currentSite,
                                                    final MediaContainerModel mediaContainer)
    {
        StringBuilder responsiveLogoData = new StringBuilder();
        final List<ImageData> mediaDataList = getMediaContainerConverter().convert(mediaContainer);
        for (ImageData imageData : mediaDataList)
        {
            final Integer width = imageData.getWidth();
            if (width != null)
            {
                if (responsiveLogoData.length() > 0)
                {
                    responsiveLogoData.append(PAIRS_SEPARATOR);
                }
                responsiveLogoData.append(QUOTE_MARK);
                responsiveLogoData.append(width);
                responsiveLogoData.append(QUOTE_MARK);
                responsiveLogoData.append(PAIR_DELIMITER);
                responsiveLogoData.append(QUOTE_MARK);
                responsiveLogoData.append(
                        getSiteBaseUrlResolutionService().getMediaUrlForSite(currentSite, true) + imageData.getUrl());
                responsiveLogoData.append(QUOTE_MARK);
            }
        }
        responsiveLogoData.insert(0, JSON_BRACKET_START);
        responsiveLogoData.append(JSON_BRACKET_FINISH);
        return responsiveLogoData.toString();
    }

    public Converter<MediaContainerModel, List<ImageData>> getMediaContainerConverter() {
        return mediaContainerConverter;
    }

    public void setMediaContainerConverter(final Converter<MediaContainerModel, List<ImageData>> mediaContainerConverter) {
        this.mediaContainerConverter = mediaContainerConverter;
    }

    public SiteBaseUrlResolutionService getSiteBaseUrlResolutionService() {
        return siteBaseUrlResolutionService;
    }

    public void setSiteBaseUrlResolutionService(final SiteBaseUrlResolutionService siteBaseUrlResolutionService) {
        this.siteBaseUrlResolutionService = siteBaseUrlResolutionService;
    }
}
