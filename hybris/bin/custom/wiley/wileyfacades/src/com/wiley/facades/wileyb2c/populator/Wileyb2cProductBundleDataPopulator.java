package com.wiley.facades.wileyb2c.populator;

import de.hybris.platform.commercefacades.product.converters.populator.AbstractProductPopulator;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.wiley.core.model.WileyBundleModel;
import com.wiley.core.wileybundle.service.WileyBundleService;
import com.wiley.facades.wileybundle.data.WileyBundleData;


/**
 * Populates WileyProductBundles info in ProductData
 */

public class Wileyb2cProductBundleDataPopulator<SOURCE extends ProductModel, TARGET extends ProductData>
		extends AbstractProductPopulator<SOURCE, TARGET>
{

	public static final Logger LOG = LoggerFactory.getLogger(Wileyb2cProductBundleDataPopulator.class);

	private final Converter<WileyBundleModel, WileyBundleData> wileyb2cBundleDataConverter;
	private final WileyBundleService wileyBundleService;

	@Autowired
	public Wileyb2cProductBundleDataPopulator(
			@Qualifier("wileyb2cBundleDataConverter")
			final Converter<WileyBundleModel, WileyBundleData> wileyb2cBundleDataConverter,
			final WileyBundleService wileyBundleService)
	{
		this.wileyb2cBundleDataConverter = wileyb2cBundleDataConverter;
		this.wileyBundleService = wileyBundleService;
	}

	@Override
	public void populate(final SOURCE source, final TARGET target) throws ConversionException
	{
		List<WileyBundleModel> bundlesForProduct = wileyBundleService.getBundlesForProduct(source);
		List<WileyBundleData> bundleDtos = new ArrayList<>();
		for (final WileyBundleModel bundle : bundlesForProduct) {
			try
			{
				bundleDtos.add(wileyb2cBundleDataConverter.convert(bundle));
			}
			catch (IllegalStateException e) {
				LOG.error("Can't convert bundle for source product: " + bundle.getSourceProductCode()
						+ " and upsell product code: " + bundle.getUpsellProductCode(), e);
			}
		}

		target.setWileyBundles(bundleDtos);
	}
}
