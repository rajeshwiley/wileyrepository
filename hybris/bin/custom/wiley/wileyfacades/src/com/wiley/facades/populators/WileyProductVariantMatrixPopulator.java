/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.facades.populators;

import de.hybris.platform.commercefacades.product.converters.populator.ProductVariantMatrixPopulator;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.VariantMatrixElementData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.variants.model.GenericVariantProductModel;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.Collection;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.wiley.restriction.WileyProductRestrictionService;


/**
 * Populate the multi-dimensional variant tree for a given product.
 */
public class WileyProductVariantMatrixPopulator<SOURCE extends ProductModel, TARGET extends ProductData>
		extends ProductVariantMatrixPopulator
{
	private WileyProductRestrictionService wileyProductRestrictionService;

	/**
	 * OOTB_CODE
	 *
	 * Populates the productData with the categories information modeled as a tree.
	 *
	 * The resulting tree, modeled as a group of nested lists, where the outer list will have some
	 * {@link VariantMatrixElementData} representing the categories with priority_1, and each of those
	 * contains a list of {@link VariantMatrixElementData} representing the categories with
	 * priority_2, and so on.<br>
	 * e.g.: A product with dimensions color/fit/size would return a list organized like this:
	 * <BROWN:<normal:<7;8;9>; wide:<8;9;10>>; BLACK<normal:<7;8>>>
	 *
	 * @param productModel
	 * 		the product to take the data from.
	 * @param productData
	 * 		the data to put the data in.
	 * @throws ConversionException
	 */
	@Override
	public void populate(final ProductModel productModel, final ProductData productData)
			throws ConversionException
	{

		final Collection<VariantProductModel> variants = (Collection<VariantProductModel>) getVariants(productModel).stream()
				.filter(variant -> wileyProductRestrictionService.isVisible((ProductModel) variant)).collect(Collectors.toList());
		productData.setMultidimensional(CollectionUtils.isNotEmpty(variants));

		if (productData.getMultidimensional())
		{
			final VariantMatrixElementData nodeZero = createNode(null, null);
			final VariantProductModel starterVariant = getStarterVariant(productModel, variants);

			createNodesForVariant(starterVariant, nodeZero);

			for (final VariantProductModel variant : variants)
			{
				if (variant instanceof GenericVariantProductModel)
				{
					//Don't process the first variant again
					//Dontr process invisible variants
					if (!variant.getCode().equals(productModel.getCode()))
					{
						createNodesForVariant(variant, nodeZero);
					}
				}
			}

			orderTree(nodeZero.getElements());
			productData.setVariantMatrix(nodeZero.getElements());
		}
	}

	@Required
	public void setWileyProductRestrictionService(
			final WileyProductRestrictionService wileyProductRestrictionService)
	{
		this.wileyProductRestrictionService = wileyProductRestrictionService;
	}
}
