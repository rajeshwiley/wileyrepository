package com.wiley.facades.bootstrapgrid.impl;

import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Required;

import com.google.common.base.Preconditions;
import com.wiley.bootstragrid.data.WileyBootstrapColumnData;
import com.wiley.core.model.components.WileyBootstrapRowComponentModel;
import com.wiley.core.model.components.bootstrapgrid.WileyBootstrapColumnModel;
import com.wiley.facades.bootstrapgrid.WileyBootstrapGridFacade;


public class WileyBootstrapGridFacadeImpl implements WileyBootstrapGridFacade
{
	private Converter<WileyBootstrapColumnModel, WileyBootstrapColumnData> columnConverter;

	protected Converter<WileyBootstrapColumnModel, WileyBootstrapColumnData> getColumnConverter()
	{
		return columnConverter;
	}

	@Required
	public void setColumnConverter(final Converter<WileyBootstrapColumnModel, WileyBootstrapColumnData> columnConverter)
	{
		this.columnConverter = columnConverter;
	}

	@Override
	public List<WileyBootstrapColumnData> getColumnsFromRowComponent(final WileyBootstrapRowComponentModel component)
	{
		Preconditions.checkNotNull(component);

		return Optional.ofNullable(component.getColumns())
				.orElseGet(Collections::emptyList)
				.stream()
				.map(c -> getColumnConverter().convert(c))
				.collect(Collectors.toList());
	}
}
