package com.wiley.facades.wileyplus.cart.activation.dto;

import java.util.Objects;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;



/**
 * an object to combine URL and message to display
 **/

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2016-08-09T10:25:39.625+02:00")

public class WebLinkDto
{

	private String url = null;
	@JsonIgnore
	private String message = null;

	/**
	 **/
	public WebLinkDto url(final String url)
	{
		this.url = url;
		return this;
	}

	@NotNull
	@Size(max = 255)
	@JsonProperty("url")
	public String getUrl()
	{
		return url;
	}

	public void setUrl(final String url)
	{
		this.url = url;
	}

	/**
	 **/
	public WebLinkDto message(final String message)
	{
		this.message = message;
		return this;
	}

	@Size(max = 255)
	@JsonGetter("m")
	public String getMessage()
	{
		return message;
	}

	@JsonSetter("message")
	public void setMessage(final String message)
	{
		this.message = message;
	}

	@JsonSetter("m")
	public void setM(final String m)
	{
		this.message = m;
	}


	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		WebLinkDto webLinkDto = (WebLinkDto) o;
		return Objects.equals(url, webLinkDto.url) && Objects.equals(message, webLinkDto.message);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(url, message);
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("class WebLink {\n");

		sb.append("    url: ").append(toIndentedString(url)).append("\n");
		sb.append("    message: ").append(toIndentedString(message)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(final Object o)
	{
		if (o == null)
		{
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}

