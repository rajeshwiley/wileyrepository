package com.wiley.facades.partner;

import java.util.List;

import com.wiley.facades.partner.kpmg.KPMGEnrollmentData;
import com.wiley.facades.partner.kpmg.KPMGFunctionData;
import com.wiley.facades.partner.kpmg.KPMGOfficeData;


/**
 * KPMG Enrollment Facade
 */
public interface WileyKPMGEnrollmentFacade
{
	/**
	 *
	 * @return the list of KPMG Function Data objects
	 */
	List<KPMGFunctionData> getFunctions();

	/**
	 *
	 * @param partnerId the identifier of Wiley Partner Company
	 * @return the list of KPMG Offices associate with given partner company
	 */
	List<KPMGOfficeData> getOffices(String partnerId);

	/**
	 *
	 * @param code
	 * @return a new KPMG Office Data with given code and name
	 */
	KPMGOfficeData createOfficeData(String code);

	/**
	 *
	 * @param code
	 * @param name
	 * @return a new KPMG Function Data object with given code and name
	 */
	KPMGFunctionData createKPMGFunctionData(String code, String name);

	/**
	 * Creates new KPMG Enrollment Model and saves it to current Cart
	 * @param enrollmentData
	 */
	void saveEnrollmentDataToCurrentCart(KPMGEnrollmentData enrollmentData);
}
