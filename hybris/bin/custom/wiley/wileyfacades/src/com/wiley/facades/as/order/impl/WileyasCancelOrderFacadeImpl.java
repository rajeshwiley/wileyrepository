package com.wiley.facades.as.order.impl;

import de.hybris.platform.basecommerce.enums.CancelReason;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.strategies.CheckoutCustomerStrategy;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.ordercancel.OrderCancelEntry;
import de.hybris.platform.ordercancel.OrderCancelException;
import de.hybris.platform.ordercancel.OrderCancelRequest;
import de.hybris.platform.ordercancel.OrderCancelService;
import de.hybris.platform.promotions.PromotionsService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.support.TransactionTemplate;

import com.wiley.core.order.WileyOrderFulfilmentProcessService;
import com.wiley.core.order.impl.WileyCalculationService;
import com.wiley.core.order.service.WileyOrderStateSerializationService;
import com.wiley.core.order.strategies.WileyOrderCancelabilityStrategy;
import com.wiley.facades.as.order.CancelResult;
import com.wiley.facades.as.order.WileyasCancelOrderFacade;
import com.wiley.facades.as.order.WileyasOrderFacade;


public class WileyasCancelOrderFacadeImpl implements WileyasCancelOrderFacade
{

	private static final Logger LOG = LoggerFactory.getLogger(WileyasCancelOrderFacadeImpl.class);
	private static final Long QUANTITY_FOR_CANCEL_ORDER_ENTRY = 0L;
	private static final String ORDER_NOT_FOUND_FOR_USER_AND_BASE_STORE =
			"Order with guid %s not found for current user in current BaseStore";
	private static final String ORDER_NOT_CALCULATED = "Cloned order for order with guid %s not calculated: %s";

	private final WileyasOrderFacade wileyasOrderFacade;
	private final WileyOrderCancelabilityStrategy wileyAsmAwareOrderCancelabilityStrategy;
	private final OrderCancelService orderCancelService;
	private final ModelService modelService;
	private final WileyOrderStateSerializationService wileyOrderStateSerializationService;
	private final WileyCalculationService wileyasOrderEditCalculationService;
	private final WileyOrderFulfilmentProcessService wileyOrderFulfilmentProcessService;
	private final UserService userService;
	private final BaseStoreService baseStoreService;
	private final TransactionTemplate transactionTemplate;
	private final CheckoutCustomerStrategy checkoutCustomerStrategy;
	private final CustomerAccountService customerAccountService;
	private final PromotionsService wileyasOrderEditPromotionEngineService;
	private final Converter<OrderModel, OrderData> orderConverter;

	//CHECKSTYLE:OFF
	@Autowired
	public WileyasCancelOrderFacadeImpl(
			final @Qualifier("wileyasOrderFacade") WileyasOrderFacade wileyasOrderFacade,
			final @Qualifier("wileyAsmAwareOrderCancelabilityStrategy") WileyOrderCancelabilityStrategy wileyAsmAwareOrderCancelabilityStrategy,
			final @Qualifier("wileyasOrderCancelService") OrderCancelService orderCancelService,
			final ModelService modelService,
			final WileyOrderStateSerializationService wileyOrderStateSerializationService,
			final @Qualifier("wileyasOrderEditCalculationService") WileyCalculationService wileyasOrderEditCalculationService,
			final @Qualifier("wileyOrderFulfilmentProcessService") WileyOrderFulfilmentProcessService wileyOrderFulfilmentProcessService,
			final @Qualifier("wileyasUserService") UserService userService,
			final BaseStoreService baseStoreService,
			final TransactionTemplate transactionTemplate,
			final CheckoutCustomerStrategy checkoutCustomerStrategy,
			final @Qualifier("customerAccountService") CustomerAccountService customerAccountService,
			final @Qualifier("wileyasOrderEditPromotionEngineService") PromotionsService wileyasOrderEditPromotionEngineService,
			final @Qualifier("wileyasOrderConverter") Converter<OrderModel, OrderData> orderConverter)
	{
		this.wileyasOrderFacade = wileyasOrderFacade;
		this.wileyAsmAwareOrderCancelabilityStrategy = wileyAsmAwareOrderCancelabilityStrategy;
		this.orderCancelService = orderCancelService;
		this.modelService = modelService;
		this.wileyOrderStateSerializationService = wileyOrderStateSerializationService;
		this.wileyasOrderEditCalculationService = wileyasOrderEditCalculationService;
		this.wileyOrderFulfilmentProcessService = wileyOrderFulfilmentProcessService;
		this.userService = userService;
		this.baseStoreService = baseStoreService;
		this.transactionTemplate = transactionTemplate;
		this.checkoutCustomerStrategy = checkoutCustomerStrategy;
		this.customerAccountService = customerAccountService;
		this.wileyasOrderEditPromotionEngineService = wileyasOrderEditPromotionEngineService;
		this.orderConverter = orderConverter;
	}
	//CHECKSTYLE:ON

	@Override
	public CancelResult cancelOrder(final String orderCode, @Nullable final CancelReason cancelReason)
	{
		final OrderModel orderModel = wileyasOrderFacade.getOrderModel(orderCode);

		if (cancelReason != null)
		{
			final List<AbstractOrderEntryModel> entries = orderModel.getEntries().stream().filter(
					entry -> !OrderStatus.CANCELLED.equals(entry.getStatus())).collect(Collectors.toList());
			entries.forEach(r -> r.setCancelReason(cancelReason));
			modelService.saveAll(entries);
		}
		final boolean hasActiveFulfillmentProcess = wileyasOrderFacade.hasActiveFulfillmentProcess(orderModel);
		if (hasActiveFulfillmentProcess)
		{
			return CancelResult.BP_ACTIVE_ERROR;
		}

		final boolean hasCancellableEntries = getOrderCancelabilityStrategy().hasCancelableEntries(orderModel);
		if (!hasCancellableEntries)
		{
			return CancelResult.STATUS_CHANGED_ERROR;
		}

		try
		{
			String orderState = wileyOrderStateSerializationService.serializeToOrderState(orderModel);
			cancelOrder(orderModel);
			orderModel.getEntries().forEach(entry -> entry.setStatus(OrderStatus.CANCELLED));
			modelService.save(orderModel);

			markEntriesCancelled(orderModel);
			wileyasOrderEditCalculationService.recalculate(orderModel);
			wileyOrderFulfilmentProcessService.startFulfilmentProcessForOrder(orderModel, true, orderState, true);
		}
		catch (CalculationException | OrderCancelException e)
		{
			LOG.error(e.getMessage(), e);
			return CancelResult.UNKNOWN_ERROR;
		}

		return CancelResult.CANCELLED;
	}

	@Override
	public List<CancelReason> getOrderCancelReasons()
	{
		final BaseStoreModel currentBaseStore = baseStoreService.getCurrentBaseStore();
		return currentBaseStore.getOrderCancelReasons();
	}

	@Override
	public boolean isCancellable(final String orderCode)
	{
		final OrderModel orderModel = wileyasOrderFacade.getOrderModel(orderCode);
		return getOrderCancelabilityStrategy().isCancelable(orderModel);
	}

	@Override
	public void cancelOrderEntry(
			@Nonnull final String orderCode,
			@Nonnull final Integer entryNumber,
			@Nullable final CancelReason cancelReason
	) throws CalculationException, OrderCancelException

	{
		CustomerModel currentUser = (CustomerModel) userService.getCurrentUser();
		OrderModel orderModel = wileyasOrderFacade.getOrderModel(orderCode);
		Optional<AbstractOrderEntryModel> first = orderModel.getEntries().stream()
				.filter(entry -> entry.getEntryNumber().equals(entryNumber)).findFirst();
		if (first.isPresent()) {
			AbstractOrderEntryModel orderEntryModel = first.get();
			String orderState = wileyOrderStateSerializationService.serializeToOrderState(orderModel);
			cancelEntry(orderModel, currentUser, orderEntryModel);
			setNecessaryAttributesForCancelEntry(orderEntryModel);
			orderEntryModel.setCancelReason(cancelReason);
			wileyasOrderFacade.recalculateOrder(orderModel, true);
			wileyOrderFulfilmentProcessService.startFulfilmentProcessForOrder(orderModel, true, orderState, true);
		}
	}

	@Override
	public OrderData getOrderDetailsAfterEntryCancelled(final String code, final Integer entryNumber)
	{
		return transactionTemplate.execute(transactionStatus -> {
			transactionStatus.setRollbackOnly();

			OrderModel orderModel;
			if (checkoutCustomerStrategy.isAnonymousCheckout())
			{
				orderModel = customerAccountService.getOrderDetailsForGUID(code,
						baseStoreService.getCurrentBaseStore());
			}
			else
			{
				orderModel = wileyasOrderFacade.getOrderModel(code);
			}

			if (orderModel == null)
			{
				throw new UnknownIdentifierException(String.format(ORDER_NOT_FOUND_FOR_USER_AND_BASE_STORE, code));
			}
			AbstractOrderEntryModel orderEntryModel = orderModel.getEntries().stream()
					.filter(entry -> entry.getEntryNumber().equals(entryNumber)).findFirst().get();
			try
			{
				setNecessaryAttributesForCancelEntry(orderEntryModel);
				wileyasOrderFacade.calculateExternalTaxes(orderModel);
				wileyasOrderEditCalculationService.recalculate(orderModel);
				wileyasOrderEditPromotionEngineService.updatePromotions(wileyasOrderFacade.getPromotionGroups(), orderModel);
			}
			catch (CalculationException e)
			{
				throw new de.hybris.order.calculation.exception.CalculationException(
						String.format(ORDER_NOT_CALCULATED, code, e.getMessage()));
			}

			return orderConverter.convert(orderModel);
		});
	}

	@Override
	public boolean hasCancellableEntry(final OrderEntryData orderEntryData)
	{
		return getOrderCancelabilityStrategy().isCancelableEntryState(orderEntryData.getStatus());
	}

	private void cancelOrder(final OrderModel orderModel) throws OrderCancelException
	{
		OrderCancelRequest request = new OrderCancelRequest(orderModel, CancelReason.CUSTOMERREQUEST);
		CustomerModel currentUser = (CustomerModel) userService.getCurrentUser();
		orderCancelService.requestOrderCancel(request, currentUser);
	}

	private WileyOrderCancelabilityStrategy getOrderCancelabilityStrategy()
	{
		return wileyAsmAwareOrderCancelabilityStrategy;
	}

	private void markEntriesCancelled(final OrderModel orderModel)
	{
		for (AbstractOrderEntryModel entryModel : orderModel.getEntries())
		{
			entryModel.setStatus(OrderStatus.CANCELLED);
		}
		modelService.save(orderModel);
	}

	private void cancelEntry(final OrderModel orderModel, final PrincipalModel currentUser,
			final AbstractOrderEntryModel orderEntryModel) throws OrderCancelException
	{
		OrderCancelEntry cancelEntry = new OrderCancelEntry(orderEntryModel, QUANTITY_FOR_CANCEL_ORDER_ENTRY);
		List<OrderCancelEntry> orderCancelEntries = new ArrayList<>();
		orderCancelEntries.add(cancelEntry);
		OrderCancelRequest request = new OrderCancelRequest(orderModel, orderCancelEntries);
		orderCancelService.requestOrderCancel(request, currentUser);
	}

	private void setNecessaryAttributesForCancelEntry(final AbstractOrderEntryModel orderEntryModel)
	{
		orderEntryModel.setStatus(OrderStatus.CANCELLED);
		orderEntryModel.setQuantity(QUANTITY_FOR_CANCEL_ORDER_ENTRY);
	}
}
