package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.platform.commercefacades.product.converters.populator.AbstractProductImagePopulator;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ImageDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;


/**
 * Created by Maksim_Kozich on 2/22/2017.
 */
public class Wileyb2cProductBackgroundImagePopulator<SOURCE extends ProductModel, TARGET extends ProductData> extends
		AbstractProductImagePopulator<SOURCE, TARGET>
{
	@Resource(name = "responsiveMediaContainerConverter")
	private Converter<MediaContainerModel, List<ImageData>> mediaContainerConverter;

	@Override
	public void populate(final SOURCE productModel, final TARGET productData) throws ConversionException
	{
		ServicesUtil.validateParameterNotNullStandardMessage("productModel", productModel);
		ServicesUtil.validateParameterNotNullStandardMessage("productData", productData);

		final MediaContainerModel backgroundImageMediaContainer = (MediaContainerModel) getProductAttribute(productModel,
				ProductModel.BACKGROUNDIMAGE);
		if (backgroundImageMediaContainer != null)
		{
			final List<ImageData> imageList = new ArrayList<ImageData>();

			addImagesInFormats(backgroundImageMediaContainer, ImageDataType.BACKGROUND, 0, imageList);

			productData.setBackgroundImages(imageList);
		}
	}
}
