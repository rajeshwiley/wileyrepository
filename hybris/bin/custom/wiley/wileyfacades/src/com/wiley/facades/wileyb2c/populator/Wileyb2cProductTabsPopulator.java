package com.wiley.facades.wileyb2c.populator;

import de.hybris.platform.commercefacades.product.converters.populator.AbstractProductPopulator;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.Assert;


/**
 * Created by Raman_Hancharou on 2/22/2017.
 */
public class Wileyb2cProductTabsPopulator<SOURCE extends ProductModel, TARGET extends ProductData>
		extends AbstractProductPopulator<SOURCE, TARGET>
{

	@Override
	public void populate(final SOURCE source, final TARGET target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setPressRelease(safeToString(getProductAttribute(source, ProductModel.PRESSRELEASE)));
		target.setNewToEdition(safeToString(getProductAttribute(source, ProductModel.NEWTOEDITION)));
		target.setWileyReviews(safeToString(getProductAttribute(source, ProductModel.REVIEWS)));
		target.setErrata(safeToString(getProductAttribute(source, ProductModel.ERRATA)));
		target.setRelatedWebsites(safeToString(getProductAttribute(source, ProductModel.RELATEDWEBSITES)));
		target.setNotes(safeToString(getProductAttribute(source, ProductModel.NOTES)));
		target.setWhatsNew(safeToString(getProductAttribute(source, ProductModel.WHATSNEW)));
		target.setTableOfContents(safeToString(getProductAttribute(source, ProductModel.TABLEOFCONTENTS)));
		target.setAboutAuthors(safeToString(getProductAttribute(source, ProductModel.ABOUTAUTHORS)));
		target.setDownloadsTab(safeToString(getProductAttribute(source, ProductModel.DOWNLOADSTAB)));
		final String whatsNewEnrichedString = safeToString(getProductAttribute(source, ProductModel.WHATSNEWENRICHED));
		if (StringUtils.isNotEmpty(whatsNewEnrichedString))
		{
			target.setWhatsNewToDisplay(whatsNewEnrichedString);
		}
		else
		{
			target.setWhatsNewToDisplay(safeToString(getProductAttribute(source, ProductModel.WHATSNEW)));
		}
	}

}
