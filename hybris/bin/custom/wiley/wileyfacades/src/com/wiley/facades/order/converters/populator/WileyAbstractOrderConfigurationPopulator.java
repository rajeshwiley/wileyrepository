package com.wiley.facades.order.converters.populator;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import com.wiley.core.checkout.configuration.WileyOrderConfigurationService;
import com.wiley.core.model.WileyOrderConfigurationModel;
import com.wiley.core.payment.WileyPaymentModeService;
import com.wiley.facades.order.data.WileyOrderConfigurationData;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


public class WileyAbstractOrderConfigurationPopulator implements Populator<AbstractOrderModel, AbstractOrderData>
{
	@Resource
	private WileyPaymentModeService wileyPaymentModeService;

	@Resource
	private WileyOrderConfigurationService wileyOrderConfigurationService;

	@Override
	public void populate(final AbstractOrderModel abstractOrderModel, final AbstractOrderData abstractOrderData)
			throws ConversionException
	{
		validateParameterNotNullStandardMessage("AbstractOrderModel", abstractOrderModel);
		validateParameterNotNullStandardMessage("AbstractOrderData", abstractOrderData);

		WileyOrderConfigurationData orderConfigurationData = new WileyOrderConfigurationData();
		orderConfigurationData.setPermittedPaymentModes(convertPaymentModes(abstractOrderModel));
		Optional<WileyOrderConfigurationModel> orderConfigurationModel =
				wileyOrderConfigurationService.getOrderConfiguration(abstractOrderModel);
		orderConfigurationModel.ifPresent(orderConfiguration ->
				orderConfigurationData.setPermitChangePaymentCountry(
						orderConfiguration.getPermitChangePaymentCountry().booleanValue()));
		orderConfigurationModel.ifPresent(orderConfiguration ->
				orderConfigurationData.setHidePaymentDetails(orderConfiguration.
						getHidePaymentDetails().booleanValue()));
		orderConfigurationModel.ifPresent(orderConfiguration ->
				orderConfigurationData.setHidePricing(orderConfiguration.getHidePricing().booleanValue()));

		abstractOrderData.setOrderConfiguration(orderConfigurationData);
	}

	private Set<String> convertPaymentModes(final AbstractOrderModel abstractOrderModel)
	{
		return wileyPaymentModeService.getSupportedPaymentModes(abstractOrderModel).stream()
				.map(PaymentModeModel::getCode)
				.collect(Collectors.toSet());
	}
}