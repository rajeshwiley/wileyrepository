package com.wiley.facades.partner.strategies.impl;

import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.VariantMatrixElementData;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.enums.UserDiscountGroup;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.util.DiscountValue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;

import com.wiley.core.product.WileyProductDiscountService;
import com.wiley.core.product.WileyProductService;
import com.wiley.facades.partner.strategies.WileyPartnerStrategy;


public class WileyPartnerStrategyImpl implements WileyPartnerStrategy
{
	@Resource
	private CommonI18NService commonI18NService;

	@Resource
	private WileyProductService productService;

	@Resource
	private PriceDataFactory priceDataFactory;

	@Resource
	private WileyProductDiscountService wileyProductDiscountService;

	@Override
	public List<ProductData> getDiscountedPartnerProducts(final List<ProductData> productList,
			final UserDiscountGroup userDiscountGroup)
	{
		final CurrencyModel currentCurrency = commonI18NService.getCurrentCurrency();
		final List<ProductData> finalProductList = new ArrayList<ProductData>();
		for (final ProductData product : productList)
		{
			final PriceData partnerPrice = getPartnerPrice(currentCurrency, userDiscountGroup,
					product.getCode(), product.getPrice());

			product.setPartnerPrice(partnerPrice);

			List<VariantOptionData> variantProducts = getVariantProducts(product);
			if (CollectionUtils.isNotEmpty(variantProducts))
			{
				variantProducts = getDiscountedVariantProducts(variantProducts, currentCurrency, userDiscountGroup);
				product.setVariantOptions(variantProducts);
			}
			product.setHasPartnerDiscount(hasPartnerDiscount(product));
			finalProductList.add(product);
		}
		return finalProductList;
	}

	private boolean hasPartnerDiscount(final ProductData product)
	{
		boolean hasPartnerDiscount = false;

		if (product.getPartnerPrice() != null)
		{
			hasPartnerDiscount = true;
		}

		if (anyVariantHasPartnerPrice(product.getVariantOptions()))
		{
			hasPartnerDiscount = true;
		}
		return hasPartnerDiscount;
	}

	private boolean anyVariantHasPartnerPrice(final List<VariantOptionData> variantOptions)
	{
		boolean hasPartnerPrice = false;
		if (CollectionUtils.isNotEmpty(variantOptions))
		{
			hasPartnerPrice = variantOptions.stream().anyMatch(variantOption -> variantOption.getPartnerPrice() != null);
		}
		return hasPartnerPrice;
	}

	private List<VariantOptionData> getDiscountedVariantProducts(final List<VariantOptionData> variantProducts,
			final CurrencyModel currentCurrency, final UserDiscountGroup userDiscountGroup)
	{
		final List<VariantOptionData> finalVariantProducts = new ArrayList<>();
		variantProducts.stream().filter(variantOption -> variantOption.getPriceData() != null).forEach(variantOption ->
		{
			final PriceData partnerPrice = getPartnerPrice(currentCurrency, userDiscountGroup,
					variantOption.getCode(), variantOption.getPriceData());
			variantOption.setPartnerPrice(partnerPrice);
			finalVariantProducts.add(variantOption);
		});
		return finalVariantProducts;
	}

	private PriceData getPartnerPrice(final CurrencyModel currentCurrency, final UserDiscountGroup userDiscountGroup,
			final String productCode, final PriceData basePrice)
	{
		PriceData partnerPrice = null;
		final ProductModel productModel = productService.getProductForCode(productCode);
		final List<DiscountValue> potentialDiscounts = wileyProductDiscountService
				.getPotentialDiscountForCurrentCustomer(productModel, userDiscountGroup);

		if (CollectionUtils.isNotEmpty(potentialDiscounts))
		{
			partnerPrice = createPriceDataWithAppliedDiscount(basePrice, currentCurrency, potentialDiscounts.get(0));
		}
		return partnerPrice;
	}

	private PriceData createPriceDataWithAppliedDiscount(final PriceData priceData, final CurrencyModel currentCurrency,
			final DiscountValue discountValue)
	{
		final double appliedValue = applyDiscount(discountValue, priceData.getValue().doubleValue(), currentCurrency).
				getAppliedValue();
		return priceDataFactory.create(priceData.getPriceType(), priceData.getValue().subtract(BigDecimal.valueOf(appliedValue)),
				currentCurrency);
	}

	private DiscountValue applyDiscount(final DiscountValue discount, final Double price, final CurrencyModel currency)
	{
		final int productQuantity = 1;
		return discount.apply(productQuantity, price, currency.getDigits(), currency.getIsocode());
	}

	private List<VariantOptionData> getVariantProducts(final ProductData productData)
	{
		final List<VariantMatrixElementData> variantMatrix = productData.getVariantMatrix();
		return getLeafElements(variantMatrix).stream().map(VariantMatrixElementData::getVariantOption)
				.collect(Collectors.toList());
	}

	private List<VariantMatrixElementData> getLeafElements(final List<VariantMatrixElementData> matrixElements)
	{
		if (CollectionUtils.isNotEmpty(matrixElements))
		{
			final List<VariantMatrixElementData> leafElements = new ArrayList<>();
			for (final VariantMatrixElementData element : matrixElements)
			{
				if (Boolean.TRUE.equals(element.getIsLeaf()))
				{
					leafElements.add(element);
				}
				else
				{
					leafElements.addAll(getLeafElements(element.getElements()));
				}
			}
			return leafElements;
		}
		return Collections.emptyList();
	}
}
