package com.wiley.facades.search.solrfacetsearch.populators;

import de.hybris.platform.commerceservices.search.facetdata.FacetSearchPageData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchResponse;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.IndexedTypeSort;

import java.util.Objects;


public class WileySearchResponseQueryPageTypePopulator<FACET_SEARCH_CONFIG_TYPE, INDEXED_TYPE_TYPE, SEARCH_QUERY_TYPE,
		SEARCH_RESULT_TYPE, ITEM> implements Populator<SolrSearchResponse<FACET_SEARCH_CONFIG_TYPE, INDEXED_TYPE_TYPE,
		IndexedProperty, SEARCH_QUERY_TYPE, IndexedTypeSort, SEARCH_RESULT_TYPE>, FacetSearchPageData<SolrSearchQueryData, ITEM>>
{
	@Override
	public void populate(
			final SolrSearchResponse<FACET_SEARCH_CONFIG_TYPE, INDEXED_TYPE_TYPE, IndexedProperty, SEARCH_QUERY_TYPE,
					IndexedTypeSort, SEARCH_RESULT_TYPE> source,
			final FacetSearchPageData<SolrSearchQueryData, ITEM> target)
			throws ConversionException
	{
		if (Objects.nonNull(target.getCurrentQuery()))
		{
			target.getCurrentQuery().setPageType(source.getRequest().getSearchQueryData().getPageType());
		}
	}
}