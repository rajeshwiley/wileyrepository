package com.wiley.facades.wileyb2c.order;


import de.hybris.platform.commercefacades.order.data.CartData;

import com.wiley.facades.order.WileycomCheckoutFacade;


/**
 * Checkout facade for b2c storefront.
 */
public interface Wileyb2cCheckoutFacade extends WileycomCheckoutFacade
{

	/**
	 * Performs call to external system to real-time inventory check
	 *
	 * @param cartData
	 * 		- cart to check
	 */
	void doRealTimeInventoryCheck(CartData cartData);

	/**
	 * Removes paypal payment information and payment addresses from Cart object.
	 *
	 */
	void removePaypalPaymentInformationFromSessionCart();
}
