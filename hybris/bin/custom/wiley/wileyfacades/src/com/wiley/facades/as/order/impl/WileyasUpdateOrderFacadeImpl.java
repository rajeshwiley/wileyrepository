package com.wiley.facades.as.order.impl;

import com.wiley.core.address.WileyAddressService;
import com.wiley.core.enums.PaymentModeEnum;
import com.wiley.core.exceptions.WileyOrderCreditCardDetailsNotValidException;
import com.wiley.core.exceptions.WileyOrderHasActiveOrFailedFulfilmentProcessException;
import com.wiley.core.exceptions.WileyOrderNonEditableException;
import com.wiley.core.exceptions.WileyOrderPaymentMethodNotSupportedException;
import com.wiley.core.i18n.WileyCommonI18NService;
import com.wiley.core.i18n.WileyCountryService;
import com.wiley.core.mpgs.services.WileyMPGSMerchantService;
import com.wiley.core.mpgs.services.WileyMPGSMessageService;
import com.wiley.core.mpgs.services.WileyMPGSPaymentService;
import com.wiley.core.order.PaymentActonType;
import com.wiley.core.order.PendingPaymentActon;
import com.wiley.core.order.WileyCheckoutService;
import com.wiley.core.order.WileyOrderFulfilmentProcessService;
import com.wiley.core.order.service.WileyOrderStateSerializationService;
import com.wiley.core.order.strategies.WileyOrderEditabilityStrategy;
import com.wiley.core.validation.service.WileyTaxValidationService;
import com.wiley.core.wiley.order.WileyOrderPaymentService;
import com.wiley.facades.as.order.WileyasOrderFacade;
import com.wiley.facades.as.order.WileyasUpdateOrderFacade;
import com.wiley.facades.payment.data.WileyMPGSPaymentResult;
import com.wiley.facades.payment.mpgs.WileyMPGSPaymentFacade;
import com.wiley.facades.wiley.order.dto.OrderUpdateRequestDTO;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.InvoicePaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.order.PaymentModeService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.orderhistory.OrderHistoryService;
import de.hybris.platform.orderhistory.model.OrderHistoryEntryModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.processengine.enums.ProcessState;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.support.TransactionTemplate;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.Optional;

public class WileyasUpdateOrderFacadeImpl implements WileyasUpdateOrderFacade {

    private final WileyMPGSMerchantService wileyMPGSMerchantService;
    private final ModelService modelService;
    private final WileyasOrderFacade wileyasOrderFacade;
    private final TransactionTemplate transactionTemplate;
    private final WileyOrderStateSerializationService wileyOrderStateSerializationService;
    private final OrderHistoryService orderHistoryService;
    private final WileyCheckoutService wileyCheckoutService;
    private final WileyOrderEditabilityStrategy wileyAsmAwareOrderEditabilityStrategy;
    private final WileyOrderFulfilmentProcessService wileyOrderFulfilmentProcessService;
    private final WileyCountryService wileyCountryService;
    private final WileyCommonI18NService wileyCommonI18NService;
    private final Populator<AddressData, AddressModel> addressReversePopulator;
    private final Comparator<AddressData> addressDataComparator;
    private final Converter<AddressModel, AddressData> addressConverter;
    private final WileyAddressService wileyAddressService;
    private final WileyTaxValidationService wileyTaxValidationService;
    private final PaymentModeService paymentModeService;
    private final KeyGenerator guidKeyGenerator;
    private final Converter<OrderModel, OrderData> orderConverter;
    private final WileyOrderPaymentService wileyOrderPaymentService;
    private final WileyMPGSPaymentService wileyMPGSPaymentService;
    private final WileyMPGSMessageService wileyMPGSErrorService;
    private final WileyMPGSPaymentFacade wileyMPGSPaymentFacade;
    //CHECKSTYLE:OFF
    @Autowired
    public WileyasUpdateOrderFacadeImpl(final WileyMPGSMerchantService wileyMPGSMerchantService,
            final ModelService modelService,
            @Qualifier("wileyasOrderFacade") final WileyasOrderFacade wileyasOrderFacade,
            final TransactionTemplate transactionTemplate,
            final WileyOrderStateSerializationService wileyOrderStateSerializationService,
            @Qualifier("wileyOrderHistoryService") final OrderHistoryService orderHistoryService,
            final WileyCheckoutService wileyCheckoutService,
            final WileyOrderEditabilityStrategy wileyAsmAwareOrderEditabilityStrategy,
            final WileyOrderFulfilmentProcessService wileyOrderFulfilmentProcessService,
            final WileyCountryService wileyCountryService,
            final WileyCommonI18NService wileyCommonI18NService,
            @Qualifier("addressReversePopulator") final Populator<AddressData, AddressModel> addressReversePopulator,
            @Qualifier("wileyasAddressComparator") final Comparator<AddressData> addressDataComparator,
            @Qualifier("addressConverter") final Converter<AddressModel, AddressData> addressConverter,
            final WileyAddressService wileyAddressService,
            final WileyTaxValidationService wileyTaxValidationService,
            final PaymentModeService paymentModeService,
            @Qualifier("guidKeyGenerator") final KeyGenerator guidKeyGenerator,
            @Qualifier("wileyasOrderConverter") final Converter<OrderModel, OrderData> orderConverter,
            final WileyOrderPaymentService wileyOrderPaymentService,
            @Qualifier("wileyasOrderEditMPGSPaymentService") final WileyMPGSPaymentService wileyMPGSPaymentService,
            final WileyMPGSMessageService wileyMPGSErrorService,
            @Qualifier("wileyasOrderEditMpgsPaymentFacade") final WileyMPGSPaymentFacade wileyMPGSPaymentFacade) {
        this.wileyMPGSMerchantService = wileyMPGSMerchantService;
        this.modelService = modelService;
        this.wileyasOrderFacade = wileyasOrderFacade;
        this.transactionTemplate = transactionTemplate;
        this.wileyOrderStateSerializationService = wileyOrderStateSerializationService;
        this.orderHistoryService = orderHistoryService;
        this.wileyCheckoutService = wileyCheckoutService;
        this.wileyAsmAwareOrderEditabilityStrategy = wileyAsmAwareOrderEditabilityStrategy;
        this.wileyOrderFulfilmentProcessService = wileyOrderFulfilmentProcessService;
        this.wileyCountryService = wileyCountryService;
        this.wileyCommonI18NService = wileyCommonI18NService;
        this.addressReversePopulator = addressReversePopulator;
        this.addressDataComparator = addressDataComparator;
        this.addressConverter = addressConverter;
        this.wileyAddressService = wileyAddressService;
        this.wileyTaxValidationService = wileyTaxValidationService;
        this.paymentModeService = paymentModeService;
        this.guidKeyGenerator = guidKeyGenerator;
        this.orderConverter = orderConverter;
        this.wileyOrderPaymentService = wileyOrderPaymentService;
        this.wileyMPGSPaymentService = wileyMPGSPaymentService;
        this.wileyMPGSErrorService = wileyMPGSErrorService;
        this.wileyMPGSPaymentFacade = wileyMPGSPaymentFacade;
    }
    //CHECKSTYLE:ON
    void updatePaymentInfo(final OrderModel order) {
        updateMerchantId(order);
    }

    @Override
    public OrderData updateOrder(final OrderUpdateRequestDTO orderUpdateRequestDTO, final boolean saveChanges) {
        OrderModel orderModel = wileyasOrderFacade.getOrderModel(orderUpdateRequestDTO.getOrderCode());
        return transactionTemplate.execute(transactionStatus -> {
            if (!saveChanges)
            {
                transactionStatus.setRollbackOnly();
            }
            return updateOrderInternal(orderModel, orderUpdateRequestDTO, saveChanges);
        });
    }

    @Override
    public boolean isEditable(final String orderCode) {
        final OrderModel orderModel = wileyasOrderFacade.getOrderModel(orderCode);
        return wileyAsmAwareOrderEditabilityStrategy.isEditable(orderModel);
    }



    private OrderData updateOrderInternal(final OrderModel orderModel,
                                          final OrderUpdateRequestDTO orderUpdateRequestDTO,
                                          final boolean saveChanges)
    {
        String serializedOrderState = wileyOrderStateSerializationService.serializeToOrderState(orderModel);

        // history entry should be created before payment authorization
        createHistoryEntry(orderModel);

        if (saveChanges)
        {
            validateOrderBeforeSaveChanges(orderModel);
        }
        AddressData newPaymentAddress = orderUpdateRequestDTO.getPaymentAddress();
        if (newPaymentAddress != null)
        {
            populatePaymentAddress(orderModel, newPaymentAddress);
        }
        populateTaxData(orderModel, orderUpdateRequestDTO);
        populateAdditionalBillingInformation(orderModel, orderUpdateRequestDTO);
        if (orderUpdateRequestDTO.getPaymentMode() != null)
        {
            populatePaymentInfo(orderModel, orderUpdateRequestDTO);
        }
        try
        {
            wileyasOrderFacade.recalculateOrder(orderModel, false);
        }
        catch (CalculationException e)
        {
            throw new RuntimeException(e);
        }
        validatePaymentMode(orderModel, orderUpdateRequestDTO);
        modelService.save(orderModel);

        OrderData result;
        if (saveChanges)
        {
            if (isCreditCardPaymentMode(orderModel))
            {

                if (isKeepCurrent(orderUpdateRequestDTO))
                {
                    updatePaymentInfo(orderModel);
                    processCardAuthorization(orderModel);
                }
                else
                {
                    processCardTokenizationAndAuthorization(orderModel, orderUpdateRequestDTO);
                }
            }
            markEntriesUpdated(orderModel);
            result = orderConverter.convert(orderModel);
            wileyOrderFulfilmentProcessService.startFulfilmentProcessForOrder(orderModel, true, serializedOrderState, true);
        }
        else
        {
            result = orderConverter.convert(orderModel);
        }
        return result;
    }

    private void populateTaxData(final OrderModel orderModel, final OrderUpdateRequestDTO newData)
    {
        orderModel.setTaxNumber(newData.getTaxNumber());
        orderModel.setTaxNumberExpirationDate(newData.getTaxNumberExpirationDate());
        validateOrderTaxInformation(orderModel);
        modelService.save(orderModel);
    }

    private void populateAdditionalBillingInformation(final OrderModel orderModel, final OrderUpdateRequestDTO newData)
    {
        final AddressModel addressModel = orderModel.getPaymentAddress();
        if (newData.getPaymentAddress() != null)
        {
            addressModel.setCompany(newData.getPaymentAddress().getCompanyName());
            addressModel.setDepartment(newData.getPaymentAddress().getDepartment());
        }
        modelService.save(addressModel);
    }

    private void updateMerchantId(final OrderModel orderModel) {
        CreditCardPaymentInfoModel paymentInfo = (CreditCardPaymentInfoModel) orderModel.getPaymentInfo();
        paymentInfo.setMerchantId(wileyMPGSMerchantService.getMerchantID(orderModel));
        modelService.save(paymentInfo);
    }

    private void createHistoryEntry(final OrderModel orderModel)
    {
        OrderModel snapshot = orderHistoryService.createHistorySnapshot(orderModel);
        orderHistoryService.saveHistorySnapshot(snapshot);

        final OrderHistoryEntryModel historyModel = modelService.create(OrderHistoryEntryModel.class);
        historyModel.setTimestamp(new Date());
        historyModel.setOrder(orderModel);
        historyModel.setPreviousOrderVersion(snapshot);
        modelService.save(historyModel);
    }

    private void validateOrderBeforeSaveChanges(final OrderModel orderModel)
    {
        boolean hasNonEditableEntries = !wileyAsmAwareOrderEditabilityStrategy.hasAllEditableEntries(orderModel);
        if (wileyCheckoutService.isZeroOrder(orderModel) || hasNonEditableEntries)
        {
            throw new WileyOrderNonEditableException();
        }

        boolean hasActiveFulfillmentProcess = wileyOrderFulfilmentProcessService.existsProcessActiveFor(orderModel);
        boolean hasFailedFulfillmentProcess =
                wileyOrderFulfilmentProcessService.getOrderProcessInStateForOrder(orderModel, ProcessState.FAILED).isPresent();
        if (hasActiveFulfillmentProcess || hasFailedFulfillmentProcess)
        {
            throw new WileyOrderHasActiveOrFailedFulfilmentProcessException();
        }
    }

    private void populatePaymentAddress(final OrderModel orderModel, final AddressData newPaymentAddressData)
    {
        AddressModel addressModel = getExistingAddressModelOrCreateNewOne(newPaymentAddressData, orderModel);
        boolean addressDataChanged = modelService.isNew(addressModel);
        if (!addressDataChanged)
        {
            final AddressData existingPaymentAddressData = addressConverter.convert(addressModel);
            addressDataChanged = addressDataComparator.compare(existingPaymentAddressData, newPaymentAddressData) != 0;
        }
        // populate fields only if there were some actual changes to avoid order full recalculation
        // and time limited prices being lost
        if (addressDataChanged)
        {
            addressReversePopulator.populate(newPaymentAddressData, addressModel);
            modelService.save(addressModel);
            orderModel.setPaymentAddress(addressModel);

            CountryModel formCountry = wileyCountryService.findCountryByCode(newPaymentAddressData.getCountry().getIsocode());
            orderModel.setCountry(formCountry);

            CurrencyModel formCountryDefaultCurrency = wileyCommonI18NService.getDefaultCurrency(formCountry);
            orderModel.setCurrency(formCountryDefaultCurrency);
        }
        modelService.save(orderModel);
    }

    private AddressModel getExistingAddressModelOrCreateNewOne(final AddressData addressData, final OrderModel orderModel)
    {
        Optional<AddressModel> addressModel = Optional.empty();
        if (addressData != null && addressData.getId() != null)
        {
            addressModel = wileyAddressService.getAddressById(addressData.getId());
        }
        return addressModel.orElseGet(() -> wileyAddressService.createAddressForOwner(orderModel));
    }

    private void validateOrderTaxInformation(final OrderModel orderModel)
    {
        CountryModel countryModel = wileyCountryService.findCountryByCode(orderModel.getCountry().getIsocode());
        boolean validateTaxResult = wileyTaxValidationService.validateTaxInformation(countryModel, orderModel.getTaxNumber());
        orderModel.setTaxNumberValidated(validateTaxResult);
    }

    private void populatePaymentInfo(final OrderModel orderModel, final OrderUpdateRequestDTO orderUpdateRequestDTO)
    {
        final PaymentModeEnum paymentModeEnum = orderUpdateRequestDTO.getPaymentMode();
        PaymentModeModel paymentMode = paymentModeService.getPaymentModeForCode(paymentModeEnum.getCode());
        orderModel.setPaymentMode(paymentMode);
        switch (paymentModeEnum)
        {
            case CARD:
                orderModel.setPaymentInfo(null);
                orderModel.setUserNotes(null);
                orderModel.setPurchaseOrderNumber(null);
                break;
            case INVOICE:
            case PROFORMA:
                orderModel.setPaymentInfo(createInvoicePaymentInfo(orderModel));
                orderModel.setUserNotes(orderUpdateRequestDTO.getUserNotes());
                orderModel.setPurchaseOrderNumber(orderUpdateRequestDTO.getPurchaseOrderNumber());
                break;
            default:
                throw new WileyOrderPaymentMethodNotSupportedException();
        }
        modelService.save(orderModel);
    }

    private InvoicePaymentInfoModel createInvoicePaymentInfo(final OrderModel orderModel)
    {
        final InvoicePaymentInfoModel invoicePaymentInfoModel = new InvoicePaymentInfoModel();
        invoicePaymentInfoModel.setUser(orderModel.getUser());
        invoicePaymentInfoModel.setCode(guidKeyGenerator.generate().toString());
        return invoicePaymentInfoModel;
    }

    private void validatePaymentMode(final OrderModel orderModel, final OrderUpdateRequestDTO orderUpdateRequestDTO)
    {
        if (isPaypalPayment(orderModel)
                && (isAmountIncreased(orderModel) || wileyOrderPaymentService.isCurrencyChanged(orderModel)))
        {
            throw new WileyOrderPaymentMethodNotSupportedException();
        }
    }

    private boolean isPaypalPayment(final OrderModel orderModel)
    {
        final PaymentModeEnum paymentMode = PaymentModeEnum.valueOf(orderModel.getPaymentMode().getCode().toUpperCase());
        return PaymentModeEnum.PAYPAL.equals(paymentMode);
    }

    private boolean isAmountIncreased(final OrderModel orderModel)
    {
        BigDecimal amountChanged = wileyOrderPaymentService.calculateAmountChanged(orderModel);
        return BigDecimal.ZERO.compareTo(amountChanged) < 0;
    }

    private boolean isCreditCardPaymentMode(final OrderModel orderModel)
    {
        return PaymentModeEnum.CARD.getCode().equals(orderModel.getPaymentMode().getCode());
    }

    private boolean isKeepCurrent(final OrderUpdateRequestDTO orderUpdateRequestDTO)
    {
        return orderUpdateRequestDTO.getPaymentMode() == null;
    }

    private void processCardTokenizationAndAuthorization(final OrderModel orderModel,
                                                         final OrderUpdateRequestDTO orderUpdateRequestDTO)
    {
        final WileyMPGSPaymentResult mpgsPaymentTokenizeResult = wileyMPGSPaymentFacade.retrieveSessionAndTokenize(
                orderUpdateRequestDTO.getSessionId(), true, orderModel.getCode());
        Boolean isTokenizedSuccessfully = mpgsPaymentTokenizeResult.getIsOperationSuccessful();
        if (!isTokenizedSuccessfully)
        {
            throw new WileyOrderCreditCardDetailsNotValidException();
        }

        processCardAuthorization(orderModel);
    }

    private void processCardAuthorization(final OrderModel orderModel)
    {
        if (isAuthorizationRequired(orderModel))
        {
            PaymentTransactionEntryModel authorizeEntry = wileyMPGSPaymentService.authorize(orderModel);
            final boolean isAuthorizedSuccessfully = TransactionStatus.ACCEPTED.name()
                    .equals(authorizeEntry.getTransactionStatus());
            if (!isAuthorizedSuccessfully)
            {
                String errorMessageKey = wileyMPGSErrorService.getAuthorizationStorefrontMessageKey(authorizeEntry);
                throw new WileyOrderCreditCardDetailsNotValidException(errorMessageKey);
            }
        }
    }

    private boolean isAuthorizationRequired(final OrderModel orderModel)
    {
        Collection<PendingPaymentActon> pendingPaymentActions = wileyOrderPaymentService.getPendingPaymentActions(orderModel,
                PaymentActonType.AUTHORIZE);
        return CollectionUtils.isNotEmpty(pendingPaymentActions);
    }


    private void markEntriesUpdated(final OrderModel orderModel)
    {
        for (AbstractOrderEntryModel entryModel : orderModel.getEntries())
        {
            if (!OrderStatus.CANCELLED.equals(entryModel.getStatus()))
            {
                entryModel.setStatus(OrderStatus.CREATED);
                modelService.save(entryModel);
            }
        }
        modelService.save(orderModel);
    }

}
