package com.wiley.facades.payment.mpgs;

import javax.annotation.Nonnull;

import com.wiley.facades.payment.data.WileyMPGSPaymentResult;


public interface WileyMPGSPaymentFacade
{
	WileyMPGSPaymentResult retrieveSessionAndTokenize(@Nonnull String sessionId, Boolean saveInAccount);
	WileyMPGSPaymentResult retrieveSessionAndTokenize(@Nonnull String sessionId, Boolean saveInAccount, String orderCode);
}
