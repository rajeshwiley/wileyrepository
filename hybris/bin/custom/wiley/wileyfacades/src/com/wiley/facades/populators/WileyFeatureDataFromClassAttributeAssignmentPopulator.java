package com.wiley.facades.populators;

import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.commercefacades.product.data.FeatureData;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ImageDataType;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.util.Assert;

import com.wiley.facades.product.data.MediaData;


/**
 * Created by Uladzimir_Barouski on 1/4/2017.
 */
public class WileyFeatureDataFromClassAttributeAssignmentPopulator
		implements Populator<ClassAttributeAssignmentModel, FeatureData>
{
	private Populator<MediaModel, ImageData> imagePopulator;
	private Converter<MediaModel, MediaData> mediaConverter;

	/**
	 * @return the imagePopulator
	 */
	public Populator<MediaModel, ImageData> getImagePopulator()
	{
		return imagePopulator;
	}

	/**
	 * @param imagePopulator
	 * 		the imagePopulator to set
	 */
	public void setImagePopulator(final Populator<MediaModel, ImageData> imagePopulator)
	{
		this.imagePopulator = imagePopulator;
	}



	/**
	 * @return the mediaConverter
	 */
	public Converter<MediaModel, MediaData> getMediaConverter()
	{
		return mediaConverter;
	}

	/**
	 * @param mediaConverter
	 * 		the mediaConverter to set
	 */
	public void setMediaConverter(final Converter<MediaModel, MediaData> mediaConverter)
	{
		this.mediaConverter = mediaConverter;
	}

	@Override
	public void populate(final ClassAttributeAssignmentModel source, final FeatureData target)
			throws ConversionException
	{
		Assert.notNull(source, "ClassAttributeAssignment cannot be null.");
		Assert.notNull(target, "FeatureData cannot be null.");

		target.setCode(source.getClassificationAttribute().getCode());
		target.setName(source.getClassificationAttribute().getName());
		target.setDetails(source.getDetails());

		target.setDisplayType(source.getType());
		target.setTooltip(source.getTooltip());
		target.setDisplayName(source.getDisplayName());
		if (source.getDocument() != null)
		{
			target.setDocument(mediaConverter.convert(source.getDocument()));
		}
		if (CollectionUtils.isNotEmpty(source.getMedia()))
		{
			final List<ImageData> imageList = new ArrayList<>();

			for (final MediaModel mediaModel : source.getMedia())
			{
				final ImageData imageData = new ImageData();
				imageData.setImageType(ImageDataType.PRIMARY);
				getImagePopulator().populate(mediaModel, imageData);

				imageList.add(imageData);
			}
			target.setMedia(imageList);
		}
	}
}
