package com.wiley.facades.wileyb2b.order;

import de.hybris.platform.commercefacades.order.CartFacade;

import java.util.List;

import javax.annotation.Nonnull;

import com.wiley.facades.order.data.CartModificationMessage;


/**
 * Cart facade for wileyb2b
 */
public interface Wileyb2bCartFacade extends CartFacade
{

	/**
	 * Returns and removes Cart modifications messages for current cart which are stored in the system.
	 *
	 * @return list of cart modification messages.
	 */
	@Nonnull
	List<CartModificationMessage> popCartModificationMessages();

	/**
	 * Sets voucher code in the cart which will be calculated by external service.
	 * @param voucherCode the voucher code
	 */
	void setVoucherCodeInCart(@Nonnull String voucherCode);

	/**
	 * Removes voucher code in the cart which will be calculated by external service.
	 * @param voucherCode the voucher code
	 */
	void removeVoucherCodeFromCart(@Nonnull String voucherCode);
}
