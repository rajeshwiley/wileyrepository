package com.wiley.facades.populators;

import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.OrderModel;

import org.springframework.util.Assert;

import com.wiley.core.enums.OrderType;

public class Wileyb2cOrderTypePopulator implements Populator<OrderModel, OrderData>
{

	@Override
	public void populate(final OrderModel source, final OrderData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setOrderType(source.getOrderType());
		target.setPurchaseOptionContinueURL(source.getConfirmationPageContinueUrl());
		if (source.getOrderType() == OrderType.GRACE_PERIOD) {
			Integer freeTrialPeriod = source.getEntries().get(0).getProductSummary().getFreeTrialPeriod();
			target.setPurchaseOptionGracePeriodDuration(freeTrialPeriod);
		}

	}
}
