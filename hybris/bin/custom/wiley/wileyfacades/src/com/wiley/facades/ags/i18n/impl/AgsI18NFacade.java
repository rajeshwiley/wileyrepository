package com.wiley.facades.ags.i18n.impl;

import de.hybris.platform.commerceservices.util.AbstractComparator;
import de.hybris.platform.core.model.c2l.RegionModel;

import com.wiley.facades.i18n.impl.WileyI18NFacadeImpl;


/**
 * Created on 1/20/2016.
 */
public class AgsI18NFacade extends WileyI18NFacadeImpl
{
	@Override
	protected AbstractComparator<? super RegionModel> getRegionComparator()
	{
		return AgsRegionNameComparator.INSTANCE;
	}

	protected static class AgsRegionNameComparator extends AbstractComparator<RegionModel>
	{
		public static final AgsRegionNameComparator INSTANCE = new AgsRegionNameComparator();
		private static final String ARMED_FORCES_NAME = "Armed Forces";

		@Override
		protected int compareInstances(final RegionModel regionModel1, final RegionModel regionModel2)
		{
			if (regionModel1.getName() != null && regionModel1.getName().contains(ARMED_FORCES_NAME))
			{
				if (regionModel2.getName() != null && regionModel2.getName().contains(ARMED_FORCES_NAME))
				{
					return compareValues(regionModel1.getName(), regionModel2.getName(), Boolean.TRUE.booleanValue());
				}
				else
				{
					return AFTER;
				}
			}
			else
			{
				if (regionModel2.getName() != null && regionModel2.getName().contains(ARMED_FORCES_NAME))
				{
					return BEFORE;
				}
				else
				{
					return compareValues(regionModel1.getName(), regionModel2.getName(), Boolean.TRUE.booleanValue());
				}
			}
		}
	}
}
