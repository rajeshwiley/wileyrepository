package com.wiley.facades.populators;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantProductModel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import com.wiley.core.category.WileyCategoryService;


public class VariantProductUrlPopulator implements Populator<ProductModel, ProductData>
{
	@Autowired
	private WileyCategoryService wileyCategoryService;

	@Autowired
	private UrlResolver productModelUrlResolver;

	@Override
	public void populate(final ProductModel source, final ProductData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		
		target.setUrl(productModelUrlResolver.resolve(getProductForUrl(source)));
	}

	private ProductModel getProductForUrl(final ProductModel productModel)
	{
		ProductModel productForUrl = productModel;
		if (productModel instanceof VariantProductModel)
		{
			final CategoryModel primaryCategoryForProduct = wileyCategoryService.getPrimaryWileyCategoryForProduct(productModel);
			if (!wileyCategoryService.isCFACategory(primaryCategoryForProduct))
			{
				productForUrl = ((VariantProductModel) productModel).getBaseProduct();
			}
		}
		return productForUrl;
	}
}
