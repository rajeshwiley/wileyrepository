package com.wiley.facades.order.converters.populator;

import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.springframework.util.Assert;

import com.wiley.core.model.ExternalDeliveryModeModel;
import com.wiley.facades.order.data.ExternalDeliveryModeData;


/**
 * Created by Raman_Hancharou on 6/14/2016.
 */
public class ExternalDeliveryModeReversePopulator implements Populator<ExternalDeliveryModeData, ExternalDeliveryModeModel>
{
	/**
	 * Populate the target instance with values from the source instance.
	 *
	 * @param source
	 * 		the source object
	 * @param target
	 * 		the target to fill
	 * @throws ConversionException
	 * 		if an error occurs
	 */
	@Override
	public void populate(final ExternalDeliveryModeData source,
			final ExternalDeliveryModeModel target)
			throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		final String code = source.getCode();
		if (code == null)
		{
			throw new ConversionException("Field code is required for external delivery mode");
		}
		target.setCode(code);
		target.setExternalCode(source.getExternalCode());
		target.setName(source.getName());
		target.setDescription(source.getDescription());
		PriceData priceData = source.getDeliveryCost();
		if (priceData != null && priceData.getValue() != null)
		{
			target.setCostValue(priceData.getValue().doubleValue());
			target.setCurrency(priceData.getCurrencyIso());
		}
	}
}
