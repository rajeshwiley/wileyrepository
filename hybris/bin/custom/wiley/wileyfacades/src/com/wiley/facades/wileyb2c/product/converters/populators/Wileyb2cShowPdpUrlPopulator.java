package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.variants.model.VariantProductModel;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.model.WileyPurchaseOptionProductModel;
import com.wiley.core.wiley.restriction.WileyProductRestrictionService;


/**
 * Created by Uladzimir_Barouski on 5/5/2017.
 */
public class Wileyb2cShowPdpUrlPopulator implements Populator<ProductModel, ProductData>
{
	private WileyProductRestrictionService wileyProductRestrictionService;

	@Override
	public void populate(@Nonnull final ProductModel productModel, @Nonnull final ProductData productData)
			throws ConversionException
	{
		ServicesUtil.validateParameterNotNullStandardMessage("productModel", productModel);
		ServicesUtil.validateParameterNotNullStandardMessage("productData", productData);

		boolean showPdpUrl = false;

		if (productModel instanceof WileyPurchaseOptionProductModel)
		{
			showPdpUrl = wileyProductRestrictionService.isVisible(productModel);
		}
		else
		{

			for (VariantProductModel purchaseOptionProductModel: productModel.getVariants())
			{
				if (wileyProductRestrictionService.isVisible(purchaseOptionProductModel))
				{
					showPdpUrl = true;
					break;
				}
			}
		}
		productData.setShowPdpUrl(showPdpUrl);
	}

	@Required
	public void setWileyProductRestrictionService(
			final WileyProductRestrictionService wileyProductRestrictionService)
	{
		this.wileyProductRestrictionService = wileyProductRestrictionService;
	}
}
