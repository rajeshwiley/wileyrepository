package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.platform.commercefacades.product.converters.populator.AbstractProductPopulator;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.util.ServicesUtil;


import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.model.AuthorInfoModel;
import com.wiley.facades.wileyb2c.product.converters.populators.helper.Wileyb2cProductCollectionAttributesHelper;



/**
 * Created by Denis Kanapin on 10/03/2018.
 */
public class Wileyb2cEncodedAuthorsListPopulator extends AbstractProductPopulator<ProductModel, ProductData>
{
	private static final Logger LOG = Logger.getLogger(Wileyb2cEncodedAuthorsListPopulator.class);
	private Wileyb2cProductCollectionAttributesHelper wileyb2cProductCollectionAttributesHelper;

	@Override
	public void populate(final ProductModel productModel, final ProductData productData) throws ConversionException
	{
		ServicesUtil.validateParameterNotNullStandardMessage("productModel", productModel);
		ServicesUtil.validateParameterNotNullStandardMessage("productData", productData);

		final List<AuthorInfoModel> authorsInformation =
				(List<AuthorInfoModel>) wileyb2cProductCollectionAttributesHelper.getProductAttribute(
						productModel, ProductModel.AUTHORINFOS);

		List<String> encodedAuthors = CollectionUtils.emptyIfNull(authorsInformation).stream().map((author) -> {
			try
			{
				return URLEncoder.encode(author.getName(), "UTF-8");
			}
			catch (UnsupportedEncodingException e)
			{
				LOG.error(e);
			}
			return null;

		}).filter(Objects::nonNull).collect(Collectors.toList());
		productData.setEncodedAuthorsList(encodedAuthors);
	}

	@Required
	public void setWileyb2cProductCollectionAttributesHelper(
			final Wileyb2cProductCollectionAttributesHelper wileyb2cProductCollectionAttributesHelper)
	{
		this.wileyb2cProductCollectionAttributesHelper = wileyb2cProductCollectionAttributesHelper;
	}
}
