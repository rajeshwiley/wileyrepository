/**
 *
 */
package com.wiley.facades.order.converters.populator;

import de.hybris.platform.commercefacades.order.converters.populator.OrderPopulator;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.WileyPaymentInfoData;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.voucher.VoucherService;

import java.util.Collection;
import java.util.LinkedHashSet;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.pin.service.PinService;

public class WileyOrderPopulator extends OrderPopulator
{
	@Autowired
	private VoucherService voucherService;

	@Autowired
	private PinService pinService;

	@Autowired
	private Converter<PaymentInfoModel, WileyPaymentInfoData> wileyPaymentInfoConverter;

	@Override
	public void populate(final OrderModel source, final OrderData target) throws ConversionException
	{
		superPopulate(source, target);
		addVoucherCodes(source, target);
		addPinInfo(source, target);
		addSubtotalWithoutDiscount(source, target);
		populatePaymentAddressData(source, target);
		populateWileyPaymentInfoData(source, target);
		populatePaymentModeData(source, target);
		populateTaxDetails(source, target);
		populateExternalText(source, target);
	}

	protected void superPopulate(final OrderModel orderModel, final OrderData orderData)
	{
		super.populate(orderModel, orderData);
	}

	private void addSubtotalWithoutDiscount(final OrderModel source, final OrderData target)
	{
		target.setSubTotalWithoutDiscount(createPrice(source, source.getSubTotalWithoutDiscount()));
	}

	private void addVoucherCodes(final OrderModel source, final OrderData target)
	{
		final Collection<String> appliedVoucherCodes = voucherService.getAppliedVoucherCodes(source);
		target.setAppliedVoucherCodes(new LinkedHashSet<>(appliedVoucherCodes));
	}

	private void addPinInfo(final OrderModel source, final OrderData target)
	{
		target.setPinApplied(pinService.isPinUsedForOrder(source));
	}

	private void populatePaymentAddressData(final OrderModel orderModel, final OrderData orderData)
	{
		if (orderModel.getPaymentAddress() != null)
		{
			orderData.setPaymentAddress(getAddressConverter().convert(orderModel.getPaymentAddress()));
		}
	}

	private void populateWileyPaymentInfoData(final OrderModel orderModel, final OrderData orderData)
	{
		if (orderModel.getPaymentInfo() != null)
		{
			orderData.setWileyPaymentInfo(wileyPaymentInfoConverter.convert(orderModel.getPaymentInfo()));
		}
	}

	private void populatePaymentModeData(final OrderModel orderModel, final OrderData orderData)
	{
		orderData.setPaymentMode(orderModel.getPaymentMode() != null ? orderModel.getPaymentMode().getCode() : null);

		orderData.setPurchaseOrderNumber(orderModel.getPurchaseOrderNumber());
		orderData.setUserNotes(orderModel.getUserNotes());
	}

	private void populateTaxDetails(final OrderModel orderModel, final OrderData orderData)
	{
		orderData.setTaxCalculated(orderModel.getTaxCalculated());
		if (StringUtils.isNotEmpty(orderModel.getTaxNumber()))
		{
			orderData.setTaxNumber(orderModel.getTaxNumber());
			orderData.setTaxNumberExpirationDate(orderModel.getTaxNumberExpirationDate());
		}
	}
	
	private void populateExternalText(final OrderModel orderModel, final OrderData orderData)
	{
		orderData.setExternalText(orderModel.getExternalText());
	}
}
