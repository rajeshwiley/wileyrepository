package com.wiley.facades.as.order.converters.populator;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.integration.article.dto.ArticleDto;
import com.wiley.core.integration.article.service.ArticleService;
import com.wiley.facades.order.data.WileyArticleData;

import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;


public class WileyasOrderEntryArticlePopulator implements Populator<AbstractOrderEntryModel, OrderEntryData>
{
	@Autowired
	private ArticleService articleService;

	@Autowired
	private Converter<ArticleDto, WileyArticleData> wileyasArticleConverter;

	@Override
	public void populate(final AbstractOrderEntryModel source, final OrderEntryData target) throws ConversionException
	{
		if (source.getBusinessItemId() != null)
		{
			final String articleId = source.getBusinessItemId();
			final Optional<ArticleDto> article = articleService.getArticleData(articleId);
			article.ifPresent(articleDto -> target.setArticle(wileyasArticleConverter.convert(articleDto)));
		}
	}
}
