package com.wiley.facades.wel.pin.impl;

import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.order.CartModel;

import java.util.Arrays;

import javax.annotation.Resource;

import com.wiley.core.model.PinModel;
import com.wiley.facades.wel.order.WelMultiDimensionalCartFacade;
import com.wiley.facades.welags.pin.AddToCartStrategy;
import com.wiley.facades.welags.pin.exception.PinOperationException;

import static com.wiley.facades.welags.pin.exception.PinOperationExceptionReason.INVALID_CART;

/**
 * Implementation of {@link AddToCartStrategy} for AGS.
 *
 * @author Robert_Farkas
 * @author Gergo_Fodi
 */
public class WelAddToCartStrategy implements AddToCartStrategy
{
	@Resource(name = "welMultiDimensionalCartFacadePinWorkflow")
	private WelMultiDimensionalCartFacade welCartFacade;

	@Override
	public void addProductToCart(final CartModel cartModel, final PinModel pinModel) throws PinOperationException
	{
		try
		{
			final OrderEntryData orderEntry = new OrderEntryData();
			orderEntry.setQuantity(1L);
			orderEntry.setProduct(new ProductData());
			orderEntry.getProduct().setCode(pinModel.getProductCode());
			orderEntry.setEntryNumber(null);
			welCartFacade.addOrderEntryList(cartModel.getCode(), Arrays.asList(orderEntry));
		}
		catch (CommerceCartModificationException e)
		{
			throw new PinOperationException(INVALID_CART, e);
		}
	}
}
