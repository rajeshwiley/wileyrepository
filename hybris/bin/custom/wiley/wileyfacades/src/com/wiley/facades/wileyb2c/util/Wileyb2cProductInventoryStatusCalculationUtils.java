package com.wiley.facades.wileyb2c.util;

import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.annotation.Nonnull;

import org.apache.commons.lang.BooleanUtils;
import org.junit.Assert;

import com.wiley.core.enums.ProductEditionFormat;
import com.wiley.core.enums.WileyProductLifecycleEnum;
import com.wiley.facades.product.data.InventoryStatusRecord;

import static de.hybris.platform.basecommerce.enums.StockLevelStatus.BACK_ORDER;
import static de.hybris.platform.basecommerce.enums.StockLevelStatus.INSTOCK;
import static de.hybris.platform.basecommerce.enums.StockLevelStatus.LOWSTOCK;
import static de.hybris.platform.basecommerce.enums.StockLevelStatus.OUTOFSTOCK;
import static de.hybris.platform.basecommerce.enums.StockLevelStatus.PRE_ORDER;
import static de.hybris.platform.basecommerce.enums.StockLevelStatus.PRINT_ON_DEMAND;


/**
 * Strategy for calculating Inventory Status for products.
 */
public final class Wileyb2cProductInventoryStatusCalculationUtils
{
	private Wileyb2cProductInventoryStatusCalculationUtils()
	{

	}

	public static InventoryStatusRecord calculateInventoryStatusForProduct(@Nonnull final ProductModel product,
			@Nonnull final Long availableQuantity, @Nonnull final boolean isAvailable)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("product", product);
		ServicesUtil.validateParameterNotNullStandardMessage("availableQuantity", availableQuantity);

		final InventoryStatusRecord status = new InventoryStatusRecord();


			/*                  Input data                  */

		//Not pre-ordered digital products always have "In stock" availability status,
		//but this status should not be displayed on PDP.
		if (ProductEditionFormat.DIGITAL.equals(product.getEditionFormat()) && (product.getLifecycleStatus() == null
				|| !WileyProductLifecycleEnum.PRE_ORDER.equals(product.getLifecycleStatus())))
		{
			return createInventoryStatusRecord(INSTOCK, null, null, false);
		}

		if (product.getLifecycleStatus() != null && WileyProductLifecycleEnum.PRE_ORDER.equals(product.getLifecycleStatus()))
		{
			return createInventoryStatusRecord(PRE_ORDER, null, null, true);
		}

		if (isAvailable && (availableQuantity > 0 || BooleanUtils.isTrue(product.getPrintOnDemand())))
		{
			return createInventoryStatusRecord(INSTOCK, null, null, false);
		}

		if (isAvailable)
		{
			return createInventoryStatusRecord(OUTOFSTOCK, null, null, true);
		}

		return null;
	}

	public static List<InventoryStatusRecord> calculateInventoryStatusForCartProduct(@Nonnull final ProductModel product,
			@Nonnull final Long requestedQuantity, @Nonnull final Long availableQuantity)
	{
		Assert.assertNotNull(product);
		Assert.assertNotNull(availableQuantity);

		final AbstractList<InventoryStatusRecord> statusesList = new ArrayList<InventoryStatusRecord>();

		// TODO : [ECSC-20075] commented out code which uses obsolete attribute. Should be deleted
		//the quantity could be null, e.g. the product removed from cart - no need to calculate Inventory Status
/*		if (requestedQuantity != null)
		{
			//If a product is purchased before its releaseDate, it is treated as a pre-order.
			//If releaseDate is not specified, the product is treated as already released.
			boolean preOrder = product.getReleaseDate() != null && new Date().before(product.getReleaseDate());

			//If a non-digital product with empty stocks is purchased after its releaseDate, it is treated as a back-order.
			//If releaseDate is not specified, the product is treated as already released.
			boolean backOrder = product.getReleaseDate() == null || new Date().after(product.getReleaseDate());

			boolean printOnDemand = BooleanUtils.isTrue(product.getPrintOnDemand());

			boolean isDigital = product.getEditionFormat() == ProductEditionFormat.DIGITAL;

			if (preOrder)
			{
				//case 1: Pre-order
				statusesList.add(createInventoryStatusRecord(PRE_ORDER, null, null, true));
			}
			else
			{
				//case 7: In Stock
				//nothing displayed for digital products - empty statusesList
				if (!isDigital)
				{
					if (availableQuantity <= 0)
					{
						calculateZeroInventory(statusesList, printOnDemand);
					}
					else
					{
						calculateNonzeroInventory(statusesList, requestedQuantity, availableQuantity, backOrder, printOnDemand);
					}
				}
			}
		}*/
		statusesList.sort(Comparator.comparing(InventoryStatusRecord::getSortOrder));
		return statusesList;
	}

	protected static void calculateZeroInventory(final AbstractList<InventoryStatusRecord> statusesList, boolean printOnDemand)
	{
		if (printOnDemand)
		{
			//case 2: Print on demand
			statusesList.add(createInventoryStatusRecord(PRINT_ON_DEMAND, null, null, true));
		}
		else
		{
			//case 4: Back-order
			statusesList.add(createInventoryStatusRecord(BACK_ORDER, null, null, true));
		}
	}

	protected static void calculateNonzeroInventory(final AbstractList<InventoryStatusRecord> statusesList,
			final Long requestedQuantity, final Long availableQuantity, boolean backOrder, boolean printOnDemand)
	{
		if (requestedQuantity > availableQuantity)
		{
			final long deltaAboveAvailable = requestedQuantity - availableQuantity;
			if (printOnDemand)
			{
				//case 3: In Stock/Print on demand
				statusesList.add(createInventoryStatusRecord(INSTOCK, availableQuantity, null, true));
				statusesList.add(createInventoryStatusRecord(PRINT_ON_DEMAND,
						deltaAboveAvailable, null, true));
			}
			else
			{
				if (backOrder)
				{
					//case 5: In Stock/Back-order
					statusesList.add(createInventoryStatusRecord(INSTOCK, availableQuantity,
							null, true));
					statusesList.add(createInventoryStatusRecord(BACK_ORDER, deltaAboveAvailable,
							null, true));
				}
			}
		}
		else
		{
			//case 6: In Stock
			statusesList.add(createInventoryStatusRecord(INSTOCK, null, null, true));
		}
	}

	static final List<StockLevelStatus> STATUS_ORDERED = Arrays.asList(INSTOCK, LOWSTOCK, BACK_ORDER, PRE_ORDER, PRINT_ON_DEMAND);

	static InventoryStatusRecord createInventoryStatusRecord(final StockLevelStatus status, final Long quantity,
			final Date date, final boolean visible)
	{
		final InventoryStatusRecord statusRecord = new InventoryStatusRecord();
		statusRecord.setStatusCode(status);
		statusRecord.setQuantity(quantity);
		statusRecord.setAvailableDate(date);
		statusRecord.setVisible(visible);

		if (STATUS_ORDERED.contains(status))
		{
			statusRecord.setSortOrder(STATUS_ORDERED.indexOf(status));
		}
		else
		{
			statusRecord.setSortOrder(STATUS_ORDERED.size() + 1);
		}
		return statusRecord;
	}
}
