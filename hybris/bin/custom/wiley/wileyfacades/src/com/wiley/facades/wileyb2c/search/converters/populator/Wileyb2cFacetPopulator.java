package com.wiley.facades.wileyb2c.search.converters.populator;

import de.hybris.platform.commerceservices.search.facetdata.FacetData;
import de.hybris.platform.converters.Populator;


public class Wileyb2cFacetPopulator<QUERY, STATE> implements Populator<FacetData<QUERY>, FacetData<STATE>>
{

	@Override
	public void populate(final FacetData<QUERY> source, final FacetData<STATE> target)
	{
		target.setHasMoreFacetValues(source.isHasMoreFacetValues());
	}
}
