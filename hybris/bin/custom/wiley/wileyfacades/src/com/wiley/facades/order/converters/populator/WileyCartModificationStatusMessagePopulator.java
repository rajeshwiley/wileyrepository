package com.wiley.facades.order.converters.populator;

import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.annotation.Nonnull;

import org.springframework.util.Assert;


/**
 * CartModificationData populator for status message.
 */
public class WileyCartModificationStatusMessagePopulator implements Populator<CommerceCartModification, CartModificationData>
{
	@Override
	public void populate(@Nonnull final CommerceCartModification commerceCartModification,
			@Nonnull final CartModificationData cartModificationData)
			throws ConversionException
	{
		Assert.notNull(commerceCartModification);
		Assert.notNull(cartModificationData);

		cartModificationData.setStatusMessage(commerceCartModification.getMessage());
		cartModificationData.setStatusMessageType(commerceCartModification.getMessageType());
		cartModificationData.setMessageParameters(commerceCartModification.getMessageParameters());
	}
}
