package com.wiley.facades.flow;

import de.hybris.platform.acceleratorfacades.flow.CheckoutFlowFacade;

public interface WileyCheckoutFlowFacade extends CheckoutFlowFacade
{
    boolean hasValidPaymentAddress();
}
