package com.wiley.facades.wileycom.customer.converters.populators;

import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.wiley.core.model.NameSuffixModel;
import com.wiley.core.model.SchoolModel;
import com.wiley.facades.customer.data.SchoolData;


public class WileycomCustomerPopulator implements Populator<CustomerModel, CustomerData>
{
	@Resource
	private Converter<SchoolModel, SchoolData> wileycomSchoolConverter;

	@Override
	public void populate(final CustomerModel source, final CustomerData target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		if (source.getSuffix() != null)
		{
			NameSuffixModel suffixModel = source.getSuffix();
			target.setSuffix(suffixModel.getName());
			target.setSuffixCode(suffixModel.getCode());
		}
		target.setMiddleName(source.getMiddleName());
		target.setStudentId(source.getStudentId());
		target.setMajor(source.getMajor());
		target.setGraduationMonth(source.getGraduationMonth());
		target.setGraduationYear(source.getGraduationYear());
		if (source.getSchool() != null)
		{
		  target.setSchool(wileycomSchoolConverter.convert(source.getSchool()));
		}
	}
}
