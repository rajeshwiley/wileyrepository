/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 *
 *
 */
package com.wiley.facades.wileyb2c.search.solrfacetsearch.impl;

import de.hybris.platform.commercefacades.search.data.SearchQueryData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commercefacades.search.solrfacetsearch.impl.DefaultSolrProductSearchFacade;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.commerceservices.threadcontext.ThreadContextService;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import com.wiley.core.search.Wileyb2cContentSearchService;
import com.wiley.core.search.facetdata.Wileyb2cContentSearchPageData;
import com.wiley.facades.content.ContentData;
import com.wiley.facades.wileyb2c.search.solrfacetsearch.Wileyb2cContentSearchFacade;


/**
 * Implementation of this class is based on OOTB {@link DefaultSolrProductSearchFacade}
 */
public class Wileyb2cContentSearchFacadeImpl implements Wileyb2cContentSearchFacade
{
	private Wileyb2cContentSearchService wileyb2cContentSearchService;
	private ThreadContextService threadContextService;
	private Converter<Wileyb2cContentSearchPageData<SolrSearchQueryData, ContentData>,
			Wileyb2cContentSearchPageData<SearchStateData, ContentData>> contentSearchConverter;
	private Converter<SearchQueryData, SolrSearchQueryData> searchQueryDecoder;


	@Override
	public Wileyb2cContentSearchPageData<SearchStateData, ContentData> textSearch(final SearchStateData searchState,
			final PageableData pageableData)
	{
		Assert.notNull(searchState, "SearchStateData must not be null.");

		return getThreadContextService().executeInContext(
				() ->
				{
					final SolrSearchQueryData searchQueryData = searchQueryDecoder.convert(searchState.getQuery());
					return contentSearchConverter.convert(
							wileyb2cContentSearchService.searchAgain(searchQueryData, pageableData));
				});
	}

	public Wileyb2cContentSearchService getWileyb2cContentSearchService()
	{
		return wileyb2cContentSearchService;
	}

	@Required
	public void setWileyb2cContentSearchService(final Wileyb2cContentSearchService wileyb2cContentSearchService)
	{
		this.wileyb2cContentSearchService = wileyb2cContentSearchService;
	}

	public ThreadContextService getThreadContextService()
	{
		return threadContextService;
	}

	@Required
	public void setThreadContextService(final ThreadContextService threadContextService)
	{
		this.threadContextService = threadContextService;
	}

	@Required
	public void setContentSearchConverter(
			final Converter<Wileyb2cContentSearchPageData<SolrSearchQueryData, ContentData>,
					Wileyb2cContentSearchPageData<SearchStateData, ContentData>> contentSearchConverter)
	{
		this.contentSearchConverter = contentSearchConverter;
	}

	@Required
	public void setSearchQueryDecoder(
			final Converter<SearchQueryData, SolrSearchQueryData> searchQueryDecoder)
	{
		this.searchQueryDecoder = searchQueryDecoder;
	}
}
