package com.wiley.facades.populators;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.List;

import javax.annotation.Resource;

import com.wiley.core.model.WileyPartnerCompanyModel;
import com.wiley.facades.partner.WileyPartnerCompanyData;


public class WileyPartnerCompanyPopulator implements Populator<WileyPartnerCompanyModel, WileyPartnerCompanyData>
{
	@Resource
	private Converter<CategoryModel, CategoryData> categoryConverter;

	@Override
	public void populate(final WileyPartnerCompanyModel wileyPartnerCompanyModel,
			final WileyPartnerCompanyData wileyPartnerCompanyData)
			throws ConversionException
	{
		List<CategoryData> partnerCategories = Converters.convertAll(
				wileyPartnerCompanyModel.getPartnerCategories(), categoryConverter);
		wileyPartnerCompanyData.setPartnerCategories(partnerCategories);

	}
}
