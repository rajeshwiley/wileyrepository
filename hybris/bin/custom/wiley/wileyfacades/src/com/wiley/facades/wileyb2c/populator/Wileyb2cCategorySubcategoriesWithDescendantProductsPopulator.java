package com.wiley.facades.wileyb2c.populator;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.List;

import org.springframework.util.Assert;

import com.wiley.core.category.WileyCategoryService;


/**
 * Created by Maksim_Kozich on 12/20/2017.
 */
public class Wileyb2cCategorySubcategoriesWithDescendantProductsPopulator implements Populator<CategoryModel, CategoryData>
{
	private WileyCategoryService wileyCategoryService;
	private Converter<CategoryModel, CategoryData> categoryConverter;

	@Override
	public void populate(final CategoryModel source, final CategoryData target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		List<CategoryModel> subcategoriesWithDescendantProducts =
				getWileyCategoryService().getSubcategoriesWithDescendantProducts(source);
		target.setSubCategories(Converters.convertAll(subcategoriesWithDescendantProducts, getCategoryConverter()));
	}

	public WileyCategoryService getWileyCategoryService()
	{
		return wileyCategoryService;
	}

	public void setWileyCategoryService(final WileyCategoryService wileyCategoryService)
	{
		this.wileyCategoryService = wileyCategoryService;
	}

	public Converter<CategoryModel, CategoryData> getCategoryConverter()
	{
		return categoryConverter;
	}

	public void setCategoryConverter(
			final Converter<CategoryModel, CategoryData> categoryConverter)
	{
		this.categoryConverter = categoryConverter;
	}
}
