package com.wiley.facades.wileyb2c.access.impl;

import de.hybris.platform.core.model.order.OrderEntryModel;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import com.wiley.core.wileyb2c.product.access.Wileyb2cDownloadDigitalProductService;
import com.wiley.facades.wileyb2c.access.Wileyb2cDownloadDigitalProductFacade;



/**
 * @author Dzmitryi_Halahayeu
 */
public class Wileyb2cDownloadDigitalProductFacadeImpl implements Wileyb2cDownloadDigitalProductFacade
{
	@Resource
	private Wileyb2cDownloadDigitalProductService wileyb2cDownloadDigitalProductService;

	@Override
	public String generateRedirectUrl(@Nonnull final OrderEntryModel orderEntryModel) throws Exception
	{
		return wileyb2cDownloadDigitalProductService.generateRedirectUrl(orderEntryModel);
	}
}
