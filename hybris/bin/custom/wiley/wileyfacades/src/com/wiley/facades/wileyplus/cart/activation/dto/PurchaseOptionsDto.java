package com.wiley.facades.wileyplus.cart.activation.dto;

import java.util.Objects;

import javax.validation.constraints.Min;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;



/**
 * an object to include all available purchase options in one place
 **/

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2016-08-09T10:25:39.625+02:00")

public class PurchaseOptionsDto
{

	@JsonIgnore
	private Boolean purchaseOptionAvailable = true;
	@JsonIgnore
	private Boolean regCodeActivationAvailable = false;
	@JsonIgnore
	private Integer gracePeriodDuration = 0;
	@JsonIgnore
	private Boolean gracePeriodUsed = false;

	/**
	 * a flag that purchase option (purchase of instant access) should be displayed as one of available options
	 **/
	public PurchaseOptionsDto purchaseOptionAvailable(final Boolean purchaseOptionAvailable)
	{
		this.purchaseOptionAvailable = purchaseOptionAvailable;
		return this;
	}


	@JsonGetter("poa")
	public Boolean getPurchaseOptionAvailable()
	{
		return purchaseOptionAvailable;
	}

	@JsonSetter("purchaseOptionAvailable")
	public void setPurchaseOptionAvailable(final Boolean purchaseOptionAvailable)
	{
		this.purchaseOptionAvailable = purchaseOptionAvailable;
	}

	@JsonSetter("poa")
	public void setPoa(final Boolean poa)
	{
		this.purchaseOptionAvailable = poa;
	}

	/**
	 * a flag that RegCode activation should be displayed as one of available options
	 **/
	public PurchaseOptionsDto regCodeActivationAvailable(final Boolean regCodeActivationAvailable)
	{
		this.regCodeActivationAvailable = regCodeActivationAvailable;
		return this;
	}


	@JsonGetter("rca")
	public Boolean getRegCodeActivationAvailable()
	{
		return regCodeActivationAvailable;
	}

	@JsonSetter("regCodeActivationAvailable")
	public void setRegCodeActivationAvailable(final Boolean regCodeActivationAvailable)
	{
		this.regCodeActivationAvailable = regCodeActivationAvailable;
	}

	@JsonSetter("rca")
	public void setRca(final Boolean rca)
	{
		this.regCodeActivationAvailable = rca;
	}


	/**
	 * Number of days availble for grace period. if the value is zero then grace perios is disabled
	 * minimum: 0.0
	 **/
	public PurchaseOptionsDto gracePeriodDuration(final Integer gracePeriodDuration)
	{
		this.gracePeriodDuration = gracePeriodDuration;
		return this;
	}

	@Min(value = 0)
	@JsonGetter("gpd")
	public Integer getGracePeriodDuration()
	{
		return gracePeriodDuration;
	}

	@JsonSetter("gracePeriodDuration")
	public void setGracePeriodDuration(final Integer gracePeriodDuration)
	{
		this.gracePeriodDuration = gracePeriodDuration;
	}

	@JsonSetter("gpd")
	public void setGpd(final Integer gpd)
	{
		this.gracePeriodDuration = gpd;
	}

	/**
	 * a flag that customer has already used one grace period for the class
	 **/
	public PurchaseOptionsDto gracePeriodUsed(final Boolean gracePeriodUsed)
	{
		this.gracePeriodUsed = gracePeriodUsed;
		return this;
	}

	@JsonGetter("gpu")
	public Boolean getGracePeriodUsed()
	{
		return gracePeriodUsed;
	}

	@JsonSetter("gracePeriodUsed")
	public void setGracePeriodUsed(final Boolean gracePeriodUsed)
	{
		this.gracePeriodUsed = gracePeriodUsed;
	}

	@JsonSetter("gpu")
	public void setGpu(final Boolean gpu)
	{
		this.gracePeriodUsed = gpu;
	}

	/**
	 * a flag that ALA access should be displayed as one of available options
	 **/

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		PurchaseOptionsDto purchaseOptionsDto = (PurchaseOptionsDto) o;
		return Objects.equals(purchaseOptionAvailable, purchaseOptionsDto.purchaseOptionAvailable)
				&& Objects.equals(regCodeActivationAvailable, purchaseOptionsDto.regCodeActivationAvailable)
				&& Objects.equals(gracePeriodDuration, purchaseOptionsDto.gracePeriodDuration)
				&& Objects.equals(gracePeriodUsed, purchaseOptionsDto.gracePeriodUsed);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(purchaseOptionAvailable, regCodeActivationAvailable, gracePeriodDuration,
				gracePeriodUsed);
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("class PurchaseOptions {\n");

		sb.append("    purchaseOptionAvailable: ").append(toIndentedString(purchaseOptionAvailable)).append("\n");
		sb.append("    regCodeActivationAvailable: ").append(toIndentedString(regCodeActivationAvailable)).append("\n");
		sb.append("    gracePeriodDuration: ").append(toIndentedString(gracePeriodDuration)).append("\n");
		sb.append("    gracePeriodUsed: ").append(toIndentedString(gracePeriodUsed)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(final Object o)
	{
		if (o == null)
		{
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}

