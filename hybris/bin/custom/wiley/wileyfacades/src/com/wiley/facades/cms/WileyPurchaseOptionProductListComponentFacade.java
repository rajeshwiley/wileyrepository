package com.wiley.facades.cms;

import de.hybris.platform.commercefacades.product.data.ProductData;

import java.util.List;

import com.wiley.core.model.components.WileyPurchaseOptionProductListComponentModel;


/**
 * Created by Maksim_Kozich on 5/30/2017.
 */
public interface WileyPurchaseOptionProductListComponentFacade
{
	List<ProductData> getComponentProducts(WileyPurchaseOptionProductListComponentModel component);
}
