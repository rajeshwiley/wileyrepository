package com.wiley.facades.wiley.session;

import de.hybris.platform.commercefacades.order.data.CartModificationData;

import java.util.List;


/**
 * @author Dzmitryi_Halahayeu
 */
public interface WileyFailedModificationsFacade
{
	List<CartModificationData> popAll();
}
