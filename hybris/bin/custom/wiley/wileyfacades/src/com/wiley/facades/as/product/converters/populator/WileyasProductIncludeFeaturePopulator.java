package com.wiley.facades.as.product.converters.populator;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.wiley.facades.populators.ProductIncludeFeaturesPopulator;

import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.commercefacades.product.data.FeatureData;


public class WileyasProductIncludeFeaturePopulator extends ProductIncludeFeaturesPopulator
{
	@Override
	protected List<FeatureData> buildIncludeFeaturesList(final FeatureList source)
	{
		final List<FeatureData> result = new ArrayList();
		final List<Feature> features = source.getFeatures();

		if (CollectionUtils.isNotEmpty(features))
		{
			for (final Feature feature : features)
			{
				if (isVisible(feature))
				{
					final FeatureData newFeature = (FeatureData) getFeatureConverter().convert(feature);
					result.add(newFeature);
				}
			}
		}

		return result.isEmpty() ? null : getSortedList(result);
	}

	private List<FeatureData> getSortedList(final List<FeatureData> result)
	{
		result.sort((f1, f2) -> f1.getPosition().compareTo(f2.getPosition()));
		return result;
	}

	protected boolean isVisible(final Feature feature)
	{
		return feature.getValues() != null && !feature.getValues().isEmpty()
				&& feature.getClassAttributeAssignment().getListable();
	}

}
