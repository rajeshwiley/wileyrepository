package com.wiley.facades.as.order.converters.populator;

import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.util.ServicesUtil;

public class WileyasOrderEntryDeliveryDatePopulator implements Populator<AbstractOrderEntryModel, OrderEntryData>
{

	@Override
	public void populate(final AbstractOrderEntryModel source, final OrderEntryData target) throws ConversionException
	{
		ServicesUtil.validateParameterNotNullStandardMessage("orderEntryModel", source);
		ServicesUtil.validateParameterNotNullStandardMessage("orderEntryData", target);
		target.setNamedDeliveryDate(source.getNamedDeliveryDate());	
	}

}
