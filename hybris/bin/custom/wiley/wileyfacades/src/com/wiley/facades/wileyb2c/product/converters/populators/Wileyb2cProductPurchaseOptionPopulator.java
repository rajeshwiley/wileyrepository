package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.wileyb2c.classification.Wileyb2cClassificationService;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


public class Wileyb2cProductPurchaseOptionPopulator implements Populator<ProductModel, ProductData>
{
	private final Wileyb2cClassificationService wileyb2cClassificationService;

	@Autowired
	public Wileyb2cProductPurchaseOptionPopulator(final Wileyb2cClassificationService wileyb2cClassificationService)
	{
		this.wileyb2cClassificationService = wileyb2cClassificationService;
	}

	@Override
	public void populate(final ProductModel source, final ProductData target) throws ConversionException
	{
		validateParameterNotNull(source, "product model mustn't be null");
		validateParameterNotNull(target, "product data mustn't be null");

		final ClassificationClassModel classificationClass = wileyb2cClassificationService.resolveClassificationClass(source);
		target.setPurchaseOptionName(classificationClass.getName());
	}
}
