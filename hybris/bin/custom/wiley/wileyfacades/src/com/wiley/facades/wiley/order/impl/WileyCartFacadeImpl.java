package com.wiley.facades.wiley.order.impl;

import de.hybris.platform.commercefacades.order.data.CartRestorationData;
import de.hybris.platform.commercefacades.order.impl.DefaultCartFacade;
import de.hybris.platform.commerceservices.order.CommerceCartRestorationException;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;

import java.util.List;
import java.util.Objects;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.wiley.core.enums.OrderType;
import com.wiley.core.order.WileyCartService;
import com.wiley.core.payment.WileyPaymentModeService;
import com.wiley.core.wiley.order.WileyCommerceCartService;
import com.wiley.facades.wiley.order.WileyCartFacade;

import static com.wiley.core.enums.PaymentModeEnum.PAYPAL;


public class WileyCartFacadeImpl extends DefaultCartFacade implements WileyCartFacade
{
	private static final Logger LOG = Logger.getLogger(WileyCartFacadeImpl.class);
	private WileyCommerceCartService wileyCommerceCartService;

	@Resource
	private WileyPaymentModeService wileyPaymentModeService;

	@Resource(name = "cartService")
	private WileyCartService wileyCartService;

	@Override
	public CartRestorationData restoreSavedCart() throws CommerceCartRestorationException
	{
		final CommerceCartParameter parameter = new CommerceCartParameter();
		parameter.setEnableHooks(true);
		final CartModel latestGeneralCartForSiteAndUser = wileyCommerceCartService.getLatestGeneralCartForSiteUser(
				getBaseSiteService().getCurrentBaseSite(), getUserService().getCurrentUser());
		parameter.setCart(latestGeneralCartForSiteAndUser);

		return getCartRestorationConverter().convert(getCommerceCartService().restoreCart(parameter));
	}

	/**
	 * Override OOTB method to handle empty session carts.
	 *
	 * @param guid
	 * 		the cart guid to restore for Anonymous user.
	 * @return {@link CartRestorationData}
	 * @throws {@link CommerceCartRestorationException}
	 */
	@Override
	public CartRestorationData restoreSavedCart(final String guid) throws CommerceCartRestorationException
	{
		final CommerceCartParameter parameter = new CommerceCartParameter();
		parameter.setEnableHooks(true);
		final CartModel cartForGuidAndSiteAndUser = getCommerceCartService().getCartForGuidAndSiteAndUser(guid,
				getBaseSiteService().getCurrentBaseSite(), getUserService().getCurrentUser());
		parameter.setCart(cartForGuidAndSiteAndUser);

		return getCartRestorationConverter().convert(getCommerceCartService().restoreCart(parameter));
	}

	@Override
	public void switchSessionCartForUser()
	{
		final CartModel latestGeneralCartForSiteAndUser = wileyCommerceCartService.getLatestGeneralCartForSiteUser(
				getBaseSiteService().getCurrentBaseSite(), getUserService().getCurrentUser());

		getCartService().setSessionCart(latestGeneralCartForSiteAndUser);
	}

	@Override
	public boolean switchSessionCart(final String guid)
	{
		final CartModel cartForGuidAndSiteAndUser = wileyCommerceCartService.getCartForGuidAndSiteAndUser(
				guid, getBaseSiteService().getCurrentBaseSite(), getUserService().getCurrentUser());

		if (cartForGuidAndSiteAndUser != null)
		{
			getCartService().setSessionCart(cartForGuidAndSiteAndUser);

			return true;
		}
		return false;
	}

	@Override
	public boolean hasSessionCartWithGuid(final String guid)
	{
		return StringUtils.isNotEmpty(guid) ? guid.equals(getSessionCartGuid()) : false;
	}

	@Override
	public boolean isPayPalAvailableForCurrentCart()
	{
		final CartModel cart = getCartService().getSessionCart();
		final List<PaymentModeModel> supportedPaymentModes = wileyPaymentModeService.getSupportedPaymentModes(cart);
		final PaymentModeModel payPalPaymentMode = wileyPaymentModeService.getPaymentModeForCode(PAYPAL.getCode());
		return supportedPaymentModes.contains(payPalPaymentMode)
				&& (CollectionUtils.isNotEmpty(cart.getEntries()) && cart.getTotalPrice() > 0);
	}

	@Override
	public boolean isDiscountsAvailableForCurrentCart()
	{
		return isSessionGeneralCart();
	}

	@Override
	public boolean isRemoveButtonAvailableForCurrentCart()
	{
		return isSessionGeneralCart();
	}

	@Override
	public boolean isSessionCartWithProductsFromCategory(final String categoryCode)
	{
		return wileyCartService.isCartWithProductsFromCategory(categoryCode);
	}

	@Override
	public String getLatestGeneralCartGuid()
	{
		final CartModel latestGeneralCartForSiteAndUser = wileyCommerceCartService.getLatestGeneralCartForSiteUser(
				getBaseSiteService().getCurrentBaseSite(), getUserService().getCurrentUser());
		return Objects.nonNull(latestGeneralCartForSiteAndUser) ? latestGeneralCartForSiteAndUser.getGuid() : null;
	}

	private boolean isSessionGeneralCart()
	{
		final CartModel cart = getCartService().getSessionCart();
		return OrderType.GENERAL.equals(cart.getOrderType());
	}

	@Override
	public void setCommerceCartService(final CommerceCartService commerceCartService)
	{
		super.setCommerceCartService(commerceCartService);
		wileyCommerceCartService = (WileyCommerceCartService) commerceCartService;
	}
}
