package com.wiley.facades.order.converters.populator;


import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import javax.annotation.Resource;


public class WileyCartPaymentAddressPopulator implements Populator<CartModel, CartData>
{

	@Resource
	private Converter<AddressModel, AddressData> addressConverter;

	@Override
	public void populate(final CartModel source, final CartData target) throws ConversionException
	{
		validateParameterNotNullStandardMessage("CartModel", source);
		validateParameterNotNullStandardMessage("CartData", target);
		if (source.getPaymentAddress() != null)
		{
			target.setPaymentAddress(addressConverter.convert(source.getPaymentAddress()));
		}

	}

}
