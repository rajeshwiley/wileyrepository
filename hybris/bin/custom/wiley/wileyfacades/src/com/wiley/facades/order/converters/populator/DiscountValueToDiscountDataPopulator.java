package com.wiley.facades.order.converters.populator;

import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.util.DiscountValue;

import java.math.BigDecimal;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import com.wiley.facades.product.data.DiscountData;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;


/**
 * This populator prepares data for displaying information about amount of discount.
 * Populates {@link DiscountData} based on {@link DiscountValue}.
 */
public class DiscountValueToDiscountDataPopulator implements Populator<DiscountValue, DiscountData>
{

	private static final Logger LOG = LoggerFactory.getLogger(DiscountValueToDiscountDataPopulator.class);

	@Resource
	private CommerceCommonI18NService commerceCommonI18NService;

	@Resource
	private PriceDataFactory priceDataFactory;

	@Override
	public void populate(@Nonnull final DiscountValue  discountValue, @Nonnull final DiscountData discountData)
			throws ConversionException
	{
		Assert.notNull(discountValue);
		Assert.notNull(discountData);

		discountData.setCode(discountValue.getCode());

		final boolean absolute = discountValue.isAbsolute();
		discountData.setAbsolute(absolute);

		final double value = discountValue.getValue();


		if (absolute)
		{
			CurrencyModel currency = getCurrentOrDefaultCurrencyModel();
			if (nonNull(currency))
			{
				discountData.setPrice(priceDataFactory.create(PriceDataType.FROM, BigDecimal.valueOf(value), currency));
			}
			else
			{
				LOG.warn("Could not populate price for discount [{}] because current and default currency are null.",
						discountValue);
			}
		}
		else
		{
			discountData.setRelativeValue(value);
		}
	}

	private CurrencyModel getCurrentOrDefaultCurrencyModel()
	{
		CurrencyModel currentCurrency = commerceCommonI18NService.getCurrentCurrency();
		if (isNull(currentCurrency))
		{
			currentCurrency = commerceCommonI18NService.getDefaultCurrency();
		}
		return currentCurrency;
	}
}
