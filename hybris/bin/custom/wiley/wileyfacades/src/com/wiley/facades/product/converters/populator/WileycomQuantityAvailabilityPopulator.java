package com.wiley.facades.product.converters.populator;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import com.wiley.core.product.WileyCountableProductService;


/**
 * @author Dzmitryi_Halahayeu
 */
public class WileycomQuantityAvailabilityPopulator implements Populator<ProductModel, ProductData>
{

	private WileyCountableProductService wileyCountableProductService;

	@Required
	public void setWileyCountableProductService(final WileyCountableProductService wileyCountableProductService)
	{
		this.wileyCountableProductService = wileyCountableProductService;
	}

	@Override
	public void populate(final ProductModel productModel, final ProductData productData) throws ConversionException
	{
		Assert.notNull(productModel, "Parameter productModel cannot be null.");
		Assert.notNull(productData, "Parameter productData cannot be null.");

		productData.setDoesProductHaveQuantity(wileyCountableProductService.canProductHaveQuantity(productModel));
	}
}
