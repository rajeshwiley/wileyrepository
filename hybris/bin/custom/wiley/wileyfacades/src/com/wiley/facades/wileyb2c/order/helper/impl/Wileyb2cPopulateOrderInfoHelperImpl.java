package com.wiley.facades.wileyb2c.order.helper.impl;

import com.google.common.base.Preconditions;
import com.wiley.facades.order.helper.impl.WileyPopulateOrderInfoHelperImpl;
import de.hybris.platform.acceleratorservices.urlresolver.SiteBaseUrlResolutionService;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.site.BaseSiteService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Resource;

/**
 * Created by Georgii_Gavrysh on 12/12/2016.
 */
public class Wileyb2cPopulateOrderInfoHelperImpl extends WileyPopulateOrderInfoHelperImpl {

    private static final String CART_IMAGE_FORMAT = "cartIcon";
    private static final String EMPTY_PATH = "";
    private static final String EMPTY_ENCODING_ATTRIBUTES = "";

    @Resource
    private BaseSiteService baseSiteService;
    @Resource
    private SiteBaseUrlResolutionService siteBaseUrlResolutionService;

    @Override
    protected String getImageUrl(final ProductData productData) {
        if (StringUtils.isNotBlank(productData.getPurchaseOptionImageUrl())) {
            return productData.getPurchaseOptionImageUrl();
        }
        if (CollectionUtils.isNotEmpty(productData.getImages())) {
            BaseSiteModel currentBaseSite = baseSiteService.getCurrentBaseSite();
            String siteId = String.valueOf(currentBaseSite.getUid());
            final String siteUrl = siteBaseUrlResolutionService.getWebsiteUrlForSite(currentBaseSite, EMPTY_ENCODING_ATTRIBUTES,
                    true, EMPTY_PATH);
            Preconditions.checkArgument(siteUrl != null, "Site URL is not configured for site '%s'", siteId);
            return productData.getImages().stream()
                    .filter(image -> CART_IMAGE_FORMAT.equals(image.getFormat()))
                    .findFirst()
                    .map(iData -> String.format("%s%s", siteUrl, iData.getUrl()))
                    .orElse(StringUtils.EMPTY);
        }
        return StringUtils.EMPTY;
    }
}
