package com.wiley.facades.university;

import java.util.List;

import com.wiley.facades.university.data.UniversityData;


/**
 * University facade interface. Its main purpose is to retrieve universities using existing services.
 */
public interface UniversityFacade
{
	/**
	 * Retrieves all universities for persisted country and region isocodes.
	 * If country code and region code are incorrect, returns emply list
	 * @return - list of populated universities
	 */
	List<UniversityData> getUniversitiesByCountryAndRegionIso(String countryIso, String regionIso);
}
