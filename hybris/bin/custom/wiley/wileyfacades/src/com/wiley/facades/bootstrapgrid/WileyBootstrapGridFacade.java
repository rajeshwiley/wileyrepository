package com.wiley.facades.bootstrapgrid;

import java.util.List;

import com.wiley.bootstragrid.data.WileyBootstrapColumnData;
import com.wiley.core.model.components.WileyBootstrapRowComponentModel;


public interface WileyBootstrapGridFacade
{
	List<WileyBootstrapColumnData> getColumnsFromRowComponent(WileyBootstrapRowComponentModel component);
}
