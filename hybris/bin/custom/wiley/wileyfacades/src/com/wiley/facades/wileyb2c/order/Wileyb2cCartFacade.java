package com.wiley.facades.wileyb2c.order;

import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.wiley.facades.product.util.ProductInfo;
import com.wiley.facades.wileyplus.cart.activation.dto.CartActivationRequestDto;


public interface Wileyb2cCartFacade extends CartFacade
{
	/**
	 * Add wiley course to cart
	 *
	 * @param productCode
	 * 		the product code
	 * @param qty
	 * 		the quantity
	 * @param purchaseType
	 * 		the type of purchase
	 * @param activationRequestDto
	 * 		contains needed information regarding course and other extra information
	 * @return the cart modification data
	 * @throws CommerceCartModificationException
	 * 		the commerce cart modification exception
	 */
	CartModificationData addCourseToCart(@Nonnull String productCode, long qty, @Nonnull String purchaseType,
			@Nonnull CartActivationRequestDto activationRequestDto) throws CommerceCartModificationException;

	/**
	 * Checks whether the cart contains at least one textbook.
	 */
	boolean isEducationalCart();

	/**
	 * Persists school for current order (on model level).
	 */
	void persistSchoolInformationInCurrentOrder(@Nonnull String schoolUid);

	/**
	 * Method for adding a product to cart.
	 *
	 * @param isbn
	 * 		isbn of product to add
	 * @param productInfo
	 * 		product specific data, which contains quantity and optional subscription term code at the moment
	 * @param additionalInfo
	 * 		the extra info about product which will be saved in cart entry as well.
	 * @return the cart modification data that includes a statusCode and the actual quantity added to the cart
	 * @throws CommerceCartModificationException
	 * 		if the cart cannot be modified
	 */
	@Nonnull
	CartModificationData addToCartByIsbn(@Nonnull String isbn, @Nonnull ProductInfo productInfo,
			@Nonnull String additionalInfo)
			throws CommerceCartModificationException;


	/**
	 * Method for adding a subscription product + subscription term to cart.
	 *
	 * @param wileySubscriptionCode
	 * 		Code of WileySubscriptionModel
	 * @param subscriptionTermId
	 * 		Id of SubscriptionTermModel
	 * @return the cart modification data that includes a statusCode and the actual quantity added to the cart
	 * @throws CommerceCartModificationException
	 * 		if the cart cannot be modified
	 */
	@Nonnull
	CartModificationData addToCartBySubscriptionCodeAndTermId(@Nonnull String wileySubscriptionCode,
			@Nonnull String subscriptionTermId)
			throws CommerceCartModificationException;

	/**
	 * Saves continueUrl for session cart.
	 *
	 * @param continueUrl
	 * 		continueUrl is used on cart page in 'Continue shopping' button
	 */
	void saveContinueUrl(@Nullable String continueUrl);

	/**
	 * Returns continueUrl for session cart.
	 *
	 * @return continueUrl
	 */
	String getContinueUrl();

}
