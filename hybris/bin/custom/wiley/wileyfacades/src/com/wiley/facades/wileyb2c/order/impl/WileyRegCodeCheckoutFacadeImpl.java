package com.wiley.facades.wileyb2c.order.impl;

import com.wiley.core.product.WileyProductService;
import com.wiley.core.regCode.exception.RegCodeValidationException;
import com.wiley.core.regCode.service.Wileyb2cRegCodeService;
import com.wiley.facades.wileyb2c.order.WileyRegCodeCheckoutFacade;
import com.wiley.facades.wileyplus.cart.activation.dto.CartActivationRequestDto;
import com.wiley.facades.wileyplus.cart.activation.dto.WebLinkDto;

import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.commerceservices.order.CommerceCheckoutService;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Resource;


public class WileyRegCodeCheckoutFacadeImpl implements WileyRegCodeCheckoutFacade
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyRegCodeCheckoutFacadeImpl.class);

	@Autowired
	private ModelService modelService;

	@Autowired
	private CommerceCheckoutService commerceCheckoutService;

	@Autowired
	private Wileyb2cRegCodeService regCodeService;

	@Resource
	private WileyProductService productService;

	@Resource
	private Populator<CartActivationRequestDto, AbstractOrderEntryModel> wileyb2cOrderEntryCoursePopulator;

	@Override
	public String placeRegCodeOrder(final String regCode)
	{
		CartModel cart = null;
		try
		{
			cart = regCodeService.validateRegCode(regCode);

			return performPlaceOrder(cart);

		}
		catch (RegCodeValidationException ex)
		{
			// do some actions or extra logging depending on Spec
			LOG.warn("RegCode validation failed.", ex);
			throw ex;
		}
		finally
		{
			try
			{
				modelService.remove(cart);
			}
			catch (Exception ex)
			{
				LOG.warn("Error during removing RegCode-Cart. Anyway order has been placed successfully", ex);
			}
		}
	}

	@Override
	public String processRegistrationCodeForCourse(final String regCode, final CartActivationRequestDto activationRequestDto,
			final String purchaseOptionType)
	{
		CartModel cart = null;
		try
		{
			ProductModel courseProduct = productService.getProductForIsbn(activationRequestDto.getCourse().getIsbn());
			cart = regCodeService.validateRegCodeForProduct(regCode, courseProduct.getCode());

			cart.getEntries().stream().filter(entry -> Objects.equals(entry.getProduct().getCode(), courseProduct.getCode()))
					.forEach(entry -> {
						wileyb2cOrderEntryCoursePopulator.populate(activationRequestDto, entry);
						entry.getProductSummary().setSummary(purchaseOptionType);
						modelService.save(entry);
					});
			final WebLinkDto continueURL = activationRequestDto.getContinueURL();
			if (continueURL != null) {
				cart.setConfirmationPageContinueUrl(continueURL.getUrl());
			}
			modelService.save(cart);
			return performPlaceOrder(cart);

		}
		catch (RegCodeValidationException ex)
		{
			// do some actions or extra logging depending on Spec
			LOG.warn("RegCode validation failed.", ex);
			throw ex;
		}
		finally
		{
			if (cart != null)
			{
				try
				{
					modelService.remove(cart);
				}
				catch (Exception ex)
				{
					LOG.warn("Error during removing RegCode-Cart. Anyway order has been placed successfully", ex);
				}
			}
		}
	}

	private String performPlaceOrder(final CartModel cart)
	{
		try
		{
			CommerceCheckoutParameter placeOrderParam = new CommerceCheckoutParameter();
			placeOrderParam.setEnableHooks(true);
			placeOrderParam.setCart(cart);
			placeOrderParam.setSalesApplication(SalesApplication.WEB);
			OrderModel order = commerceCheckoutService.placeOrder(placeOrderParam).getOrder();
			if (order == null)
			{
				throw new RegCodeValidationException("Registration code validated, but order cannot be placed");
			}
			return order.getCode();
		}
		catch (InvalidCartException ex)
		{
			final String message = "RegCode validation passed, but Place order failed";
			LOG.error(message, ex);
			throw new RegCodeValidationException(message, ex);
		}
	}

}


