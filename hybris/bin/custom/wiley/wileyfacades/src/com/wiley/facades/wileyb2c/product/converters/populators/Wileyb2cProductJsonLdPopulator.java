package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import com.wiley.facades.wileyb2c.product.data.OfferJsonLdDto;
import com.wiley.facades.wileyb2c.product.data.ProductJsonLdDto;


/**
 * Product JSON LD populator
 *
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public class Wileyb2cProductJsonLdPopulator implements Populator<ProductData, ProductJsonLdDto>
{
	private static final String CONTEXT_VALUE = "http://schema.org";
	private static final String TYPE_VALUE = "Book";
	private static final String DATE_FORMAT = "yyyy-MM-dd";

	@Resource(name = "wileyb2cOfferJsonLdConverter")
	private Converter<PriceData, OfferJsonLdDto> wileyb2cOfferJsonLdConverter;

	private ThreadLocal<Format> formatter;

	public Wileyb2cProductJsonLdPopulator()
	{
		formatter = ThreadLocal.withInitial(() -> new SimpleDateFormat(DATE_FORMAT));
	}

	@Override
	public void populate(@NotNull final ProductData source,
			@NotNull final ProductJsonLdDto target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setContext(CONTEXT_VALUE);
		target.setType(TYPE_VALUE);
		target.setName(StringUtils.defaultIfEmpty(source.getName(), null));
		target.setIsbn(StringUtils.defaultIfEmpty(source.getIsbn13(), null));

		final Date publicationDate = source.getPublicationDate();
		if (publicationDate != null)
		{
			target.setDatePublished(formatter.get().format(source.getPublicationDate()));
		}

		final PriceData priceData = source.getPrice();
		if (priceData != null && (priceData.getValue() != null
				|| StringUtils.isNotEmpty(priceData.getCurrencyIso())))
		{
			target.setOffers(wileyb2cOfferJsonLdConverter.convert(priceData));
		}
		final Collection<ImageData> images = source.getImages();
		if (!CollectionUtils.isEmpty(images))
		{
			final ImageData image = images.iterator().next();
			target.setImage(image.getUrl());
		}
	}
}
