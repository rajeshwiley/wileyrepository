package com.wiley.facades.cms.impl;

import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.model.WileyPurchaseOptionProductModel;
import com.wiley.core.model.components.WileyPurchaseOptionProductListComponentModel;
import com.wiley.facades.cms.WileyPurchaseOptionProductListComponentFacade;
import com.wiley.facades.constants.WileyFacadesConstants;


/**
 * Created by Maksim_Kozich on 5/30/2017.
 */
public class WileyPurchaseOptionProductListComponentFacadeImpl implements WileyPurchaseOptionProductListComponentFacade
{
	private static final List<ProductOption> PRODUCT_OPTIONS = Arrays.asList(ProductOption.BASIC,
			ProductOption.URL, ProductOption.DESCRIPTION, ProductOption.GALLERY, ProductOption.AUTHOR_INFOS_WITHOUT_ROLE,
			ProductOption.VARIANT_FULL, ProductOption.VARIANT_MATRIX_PRICE, ProductOption.PRICE, ProductOption.STUDENT_DISCOUNTS,
			ProductOption.SHOW_PDP_URL, ProductOption.SHOW_RELATED_PRODUCTS_DISCLAIMER);

	private ProductFacade productFacade;

	private SessionService sessionService;

	@Override
	public List<ProductData> getComponentProducts(final WileyPurchaseOptionProductListComponentModel component)
	{

		final List<ProductData> products = new ArrayList<>();

		final List<ProductData> manualProducts = collectLinkedProducts(component);
		products.addAll(manualProducts);

		return products;

	}


	private List<ProductData> collectLinkedProducts(final WileyPurchaseOptionProductListComponentModel component)
	{
		final List<ProductData> products = new ArrayList<>();
		final List<WileyPurchaseOptionProductModel> manualProducts = component.getItems();
		if (CollectionUtils.isNotEmpty(manualProducts))
		{
			final Map<String, Object> sessionParams = new HashMap<>();
			sessionParams.put(WileyFacadesConstants.USER_GROUP_LOCAL_VIEW_PARAM_NAME, component.getUg());
			sessionParams.put(WileyFacadesConstants.IGNORE_VISIBILITY_LOCAL_VIEW_PARAM_NAME, component.isIgnoreVisibility());

			Map<ProductModel, List<String>> groupedPurchaseOptions = groupPurchaseOptionsByBaseProduct(
					manualProducts);

			products.addAll(groupedPurchaseOptions.entrySet()
					.stream()
					.map(entry -> populateProduct(entry.getKey().getCode(), entry.getValue(), sessionParams))
					.filter(productData -> CollectionUtils.isNotEmpty(productData.getVariantOptions()))
					.collect(Collectors.toList()));
		}
		return products;
	}

	private Map<ProductModel, List<String>> groupPurchaseOptionsByBaseProduct(
			final List<WileyPurchaseOptionProductModel> purchaseOptionProductList)
	{
		Map<ProductModel, List<String>> groupedPurchaseOptionMap = new LinkedHashMap<>();
		if (CollectionUtils.isNotEmpty(purchaseOptionProductList))
		{
			groupedPurchaseOptionMap = purchaseOptionProductList
					.stream()
					.collect(Collectors.groupingBy(WileyPurchaseOptionProductModel::getBaseProduct, LinkedHashMap::new,
							Collectors.mapping(WileyPurchaseOptionProductModel::getCode, Collectors.toList())));
		}
		return groupedPurchaseOptionMap;
	}

	private ProductData populateProduct(final String productCode, final List<String> purchaseOptions,
			final Map<String, Object> sessionParams)
	{
		sessionParams.put(WileyFacadesConstants.PURHASE_OPRIONS_LOCAL_VIEW_PARAM_NAME, purchaseOptions);

		ProductData productData = sessionService.executeInLocalViewWithParams(
				sessionParams,
				new SessionExecutionBody()
				{
					@Override
					public Object execute()
					{
						return productFacade.getProductForCodeAndOptions(productCode, PRODUCT_OPTIONS);
					}
				});
		return productData;
	}

	@Required
	public void setProductFacade(final ProductFacade productFacade)
	{
		this.productFacade = productFacade;
	}

	public SessionService getSessionService()
	{
		return sessionService;
	}

	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}
}
