package com.wiley.facades.wileyb2c.product.impl;

import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wiley.core.model.AuthorInfoModel;
import com.wiley.core.model.WileyPurchaseOptionProductModel;
import com.wiley.core.wileyb2c.classification.Wileyb2cClassificationService;
import com.wiley.facades.wileyb2c.product.WileyJsonLdProductFacade;
import com.wiley.facades.wileyb2c.product.data.PersonJsonLdDto;
import com.wiley.facades.wileyb2c.product.data.ProductJsonLdDto;


/**
 * Facade for JSON LD
 *
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public class WileyJsonLdProductFacadeImpl implements WileyJsonLdProductFacade
{
	private static final String TYPE = "type";
	private static final String CONTEXT = "context";

	@Resource(name = "wileyb2cProductJsonLdConverter")
	private Converter<ProductData, ProductJsonLdDto> wileyb2cProductJsonLdConverter;

	@Resource(name = "wileyb2cPersonJsonLdConverter")
	private Converter<AuthorInfoModel, PersonJsonLdDto> wileyb2cPersonJsonLdConverter;

	@Resource(name = "wileyb2cClassificationService")
	private Wileyb2cClassificationService wileyb2cClassificationService;

	private final Map<String, String> bookFormats;

	private final Gson gson;

	public WileyJsonLdProductFacadeImpl()
	{
		gson = new GsonBuilder().setFieldNamingStrategy(field -> {
			String name = field.getName();

			if (TYPE.equals(name) || CONTEXT.equals(name))
			{
				name = "@" + name;
			}
			return name;
		}).create();

		final Map<String, String> types = new HashMap<>();
		types.put("eBook", "http://schema.org/EBook");
		types.put("cloth", "http://schema.org/Hardcover");
		types.put("paper", "http://schema.org/Paperback");
		types.put("audio", "http://schema.org/AudioBook");
		bookFormats = Collections.unmodifiableMap(types);
	}

	@Override
	public String createJsonLdFromProduct(@NotNull final ProductData productData,
			@NotNull final WileyPurchaseOptionProductModel productModel, @NotNull final String productPageUrl,
			final String defaultImage)
	{
		final ProductModel baseProduct = productModel.getBaseProduct();
		final ProductJsonLdDto dto = wileyb2cProductJsonLdConverter.convert(productData);

		final List<AuthorInfoModel> authors = baseProduct.getAuthorInfos().stream().filter(
				r -> StringUtils.isNotEmpty(r.getName()))
				.collect(Collectors.toList());
		if (CollectionUtils.isNotEmpty(authors))
		{
			dto.setAuthor(Converters.convertAll(authors, wileyb2cPersonJsonLdConverter));
		}
		addBookFormatToJsonLdDto(dto, productModel);
		dto.setUrl(productPageUrl);
		dto.setDescription(baseProduct.getSeoDescriptionTag());
		if (StringUtils.isEmpty(dto.getImage()))
		{
			dto.setImage(defaultImage);
		}
		return gson.toJson(dto);
	}


	private void addBookFormatToJsonLdDto(final ProductJsonLdDto dto,
			final WileyPurchaseOptionProductModel productModel)
	{
		final ClassificationClassModel classificationClassModel = wileyb2cClassificationService.resolveClassificationClass(
				productModel);

		if (classificationClassModel != null)
		{
			dto.setBookFormat(bookFormats.get(classificationClassModel.getCode()));
		}
	}
}
