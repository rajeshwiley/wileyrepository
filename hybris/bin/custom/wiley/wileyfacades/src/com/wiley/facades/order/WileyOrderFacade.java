package com.wiley.facades.order;

import de.hybris.platform.commercefacades.order.OrderFacade;


public interface WileyOrderFacade extends OrderFacade
{
	boolean isNonZeroOrder(String orderCode);
}
