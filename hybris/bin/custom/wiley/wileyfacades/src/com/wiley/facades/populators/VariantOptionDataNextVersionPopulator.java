package com.wiley.facades.populators;

import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Required;


public class VariantOptionDataNextVersionPopulator<SOURCE extends VariantProductModel, TARGET extends VariantOptionData>
		implements Populator<SOURCE, TARGET>
{

	private ProductFacade wileyProductVariantFacade;

	@Override
	public void populate(final VariantProductModel source, final VariantOptionData target) throws ConversionException
	{
		ProductReferenceModel preOrderProductReference = getPreOrderProductReference(source);
		if (preOrderProductReference == null)
		{
			return;
		}

		final ProductData preOrderProductData = createPreOrderProductData(preOrderProductReference);

		target.setPreOrderNodes(new ArrayList<>(Arrays.asList(preOrderProductData)));
	}

	private ProductData createPreOrderProductData(final ProductReferenceModel preOrderProductReference)
	{
		ProductModel preOrderProduct = preOrderProductReference.getTarget();
		return wileyProductVariantFacade.getProductForOptions(preOrderProduct,
				Arrays.asList(ProductOption.BASIC,
						ProductOption.PRICE, ProductOption.CATEGORIES, ProductOption.CLASSIFICATION,
						ProductOption.VARIANT_FULL,
						ProductOption.PRICE_RANGE, ProductOption.VARIANT_MATRIX_BASE, ProductOption.VARIANT_MATRIX_URL,
						ProductOption.VARIANT_MATRIX_PRICE));
	}

	private ProductReferenceModel getPreOrderProductReference(final VariantProductModel source)
	{
		final Optional<ProductReferenceModel> firstReference = source.getProductReferences().stream().filter(
				productReference -> productReference.getReferenceType() == ProductReferenceTypeEnum.NEXT_VERSION).findFirst();

		return firstReference.isPresent() ? firstReference.get() : null;
	}

	@Required
	public void setWileyProductVariantFacade(final ProductFacade wileyProductVariantFacade)
	{
		this.wileyProductVariantFacade = wileyProductVariantFacade;
	}
}
