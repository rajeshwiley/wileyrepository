package com.wiley.facades.order.helper;

import de.hybris.platform.acceleratorservices.payment.data.OrderInfoData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.core.model.order.CartModel;

/**
 * Created by Georgii_Gavrysh on 12/9/2016.
 */
public interface WileyPopulateOrderInfoHelper
{
    void populateOrderInfoData(CartModel cartModel, CartData cartData, OrderInfoData orderInfoData);
}
