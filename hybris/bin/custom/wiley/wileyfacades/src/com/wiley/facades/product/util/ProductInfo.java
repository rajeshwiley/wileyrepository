package com.wiley.facades.product.util;

import java.io.Serializable;


/**
 * Author Herman_Chukhrai (EPAM)
 */
public class ProductInfo implements Serializable
{

	private Long quantity;
	private String subscriptionTermCode;

	public ProductInfo()
	{
	}

	public ProductInfo(final Long quantity)
	{
		this.quantity = quantity;
	}

	public ProductInfo(final Long quantity, final String subscriptionTermCode)
	{
		this.quantity = quantity;
		this.subscriptionTermCode = subscriptionTermCode;
	}

	public Long getQuantity()
	{
		return quantity;
	}

	public void setQuantity(final Long quantity)
	{
		this.quantity = quantity;
	}

	public String getSubscriptionTermCode()
	{
		return subscriptionTermCode;
	}

	public void setSubscriptionTermCode(final String subscriptionTermCode)
	{
		this.subscriptionTermCode = subscriptionTermCode;
	}
}
