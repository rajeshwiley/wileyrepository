/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 *
 *
 */
package com.wiley.facades.wileyb2c.populator;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.search.facetdata.ProductCategorySearchPageData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.validation.constraints.NotNull;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


/**
 */
public class Wileyb2cClearAllSelectedFiltersQueryPopulator<QUERY, STATE, RESULT, ITEM extends ProductData, SCAT, CATEGORY>
		implements
		Populator<ProductCategorySearchPageData<QUERY, RESULT, SCAT>, ProductCategorySearchPageData<STATE, ITEM, CATEGORY>>
{
	private Converter<QUERY, STATE> searchStateConverter;

	protected Converter<QUERY, STATE> getSearchStateConverter()
	{
		return searchStateConverter;
	}

	@Required
	public void setSearchStateConverter(final Converter<QUERY, STATE> searchStateConverter)
	{
		this.searchStateConverter = searchStateConverter;
	}

	@Override
	public void populate(@NotNull final ProductCategorySearchPageData<QUERY, RESULT, SCAT> source,
			@NotNull final ProductCategorySearchPageData<STATE, ITEM, CATEGORY> target)
	{
		validateParameterNotNullStandardMessage("source", source);
		validateParameterNotNullStandardMessage("target", target);

		if (CollectionUtils.isNotEmpty(source.getBreadcrumbs()))
		{
			target.setClearAllSelectedFiltersQuery(getSearchStateConverter().convert(source.getCurrentQuery()));
		}
	}
}
