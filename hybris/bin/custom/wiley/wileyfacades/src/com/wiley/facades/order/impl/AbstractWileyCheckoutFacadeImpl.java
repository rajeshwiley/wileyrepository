package com.wiley.facades.order.impl;

import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.impl.DefaultCheckoutFacade;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.InvoicePaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.order.PaymentModeService;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import com.paypal.hybris.data.ResultErrorData;
import com.paypal.hybris.facade.PayPalCheckoutFacade;
import com.paypal.hybris.facade.data.PayPalPaymentResultData;
import com.paypal.hybris.model.PaypalPaymentInfoModel;
import com.wiley.core.enums.PaymentModeEnum;
import com.wiley.core.mpgs.services.WileyMPGSMessageService;
import com.wiley.core.mpgs.services.WileyMPGSPaymentService;
import com.wiley.core.order.WileyCheckoutService;
import com.wiley.facades.order.WileyCheckoutFacade;
import com.wiley.facades.payment.PaymentAuthorizationResultData;

import static com.wiley.facades.payment.PaymentAuthorizationResultData.failure;
import static com.wiley.facades.payment.PaymentAuthorizationResultData.success;


public abstract class AbstractWileyCheckoutFacadeImpl extends DefaultCheckoutFacade implements WileyCheckoutFacade
{
	@Autowired
	private PaymentModeService paymentModeService;

	@Autowired
	private KeyGenerator guidKeyGenerator;

	@Autowired
	private WileyCheckoutService wileyCheckoutService;

	@Autowired
	private TransactionTemplate transactionTemplate;

	@Autowired
	private PayPalCheckoutFacade payPalCheckoutFacade;

	@Autowired
	private WileyMPGSPaymentService wileyMPGSPaymentService;

	@Autowired
	private WileyMPGSMessageService wileyMPGSErrorService;

	@Override
	public boolean isZeroTotalOrder()
	{
		final CartModel cartModel = getCart();
		return cartModel != null && Double.compare(cartModel.getTotalPrice(), 0.0D) == 0;
	}

	@Override
	public boolean isStudentFlow()
	{
		return false;
	}

	@Override
	public boolean isStudentFlow(final String cartCode)
	{
		return false;
	}

	@Override
	public boolean isCartHasStudentVerification()
	{
		return false;
	}

	@Override
	public boolean isPaypalCheckout()
	{
		return wileyCheckoutService.isPaypalCheckout();
	}

	@Override
	public void setInvoicePaymentDetails(final String userNotes, final String purchaseOrderNumber, final String paymentMode)
	{
		CartModel cartModel = setPaymentDetailsInCart(userNotes, purchaseOrderNumber, paymentMode);
		cartModel.setPaymentInfo(createInvoicePaymentInfo(cartModel));

		getModelService().save(cartModel);
	}

	@Override
	public void setPaymentMode(final PaymentModeEnum paymentMode)
	{
		final CartModel cart = getCart();
		cart.setPaymentMode(paymentModeService.getPaymentModeForCode(paymentMode.getCode()));
		getModelService().save(cart);
	}

	private CartModel setPaymentDetailsInCart(final String userNotes, final String purchaseOrderNumber, final String paymentMode)
	{
		CartModel cartModel = getCartService().getSessionCart();
		cartModel.setUserNotes(userNotes);
		cartModel.setPurchaseOrderNumber(purchaseOrderNumber);
		cartModel.setPaymentMode(paymentModeService.getPaymentModeForCode(paymentMode));
		return cartModel;
	}

	@Override
	public void calculateCart()
	{
		final CartModel cartModel = getCart();
		if (cartModel != null)
		{
			final CommerceCheckoutParameter parameter = new CommerceCheckoutParameter();
			parameter.setEnableHooks(true);
			cartModel.setCalculated(Boolean.FALSE);
			parameter.setCart(cartModel);
			getCommerceCheckoutService().calculateCart(parameter);
		}
	}

	private InvoicePaymentInfoModel createInvoicePaymentInfo(final CartModel cartModel)
	{
		final InvoicePaymentInfoModel invoicePaymentInfoModel = new InvoicePaymentInfoModel();
		invoicePaymentInfoModel.setUser(cartModel.getUser());
		invoicePaymentInfoModel.setCode(guidKeyGenerator.generate().toString());

		return invoicePaymentInfoModel;
	}

	protected PaymentAuthorizationResultData buildPaymentAuthorizationResultData(final PayPalPaymentResultData payPalResultData,
			final PaymentAuthorizationResultData.PaymentMethod payment)
	{
		final String status = payPalResultData.getStatus().toString();
		final List<String> errors = payPalResultData.getErrors().stream()
				.map(ResultErrorData::getLongMessage)
				.collect(Collectors.toList());
		return failure(status, errors, payment);
	}

	@Override
	public void resetCartPaymentInfo()
	{
		final CartModel currentCart = getCart();
		if (currentCart.getPaymentMode() == null && currentCart.getPaymentInfo() == null)
		{
			return;
		}

		transactionTemplate.execute(new TransactionCallbackWithoutResult()
		{
			@Override
			protected void doInTransactionWithoutResult(final TransactionStatus transactionStatus)
			{
				PaymentInfoModel paymentInfo = currentCart.getPaymentInfo();
				currentCart.setPaymentInfo(null);
				currentCart.setPaymentMode(null);

				getModelService().save(currentCart);

				if (paymentInfo != null && !paymentInfo.isSaved()) {
					getModelService().remove(paymentInfo);
				}
			}
		});

	}

	@Override
	public boolean isNonZeroOrder()
	{
		return wileyCheckoutService.isNonZeroOrder(getCart());
	}

	@Override
	public void clearInvoicePaymentData()
	{
		CartModel cartModel = getCartService().getSessionCart();
		cartModel.setUserNotes(null);
		cartModel.setPurchaseOrderNumber(null);
		getModelService().save(cartModel);
	}

	@Override
	public PaymentAuthorizationResultData authorizePaymentAndProvideResult(final String securityCode)
	{
		final CartModel cartModel = getCart();
		if (checkIfCurrentUserIsTheCartUser())
		{
			if (isZeroTotalOrder())
			{
				return PaymentAuthorizationResultData.success();
			}

			final PaymentInfoModel infoModel = cartModel.getPaymentInfo();
			if (infoModel instanceof PaypalPaymentInfoModel)
			{
				final PayPalPaymentResultData payPalPaymentResultData = payPalCheckoutFacade
						.authorizePaymentAndProvideResult(securityCode);

				return payPalPaymentResultData.isSuccess() ? success() :
						buildPaymentAuthorizationResultData(payPalPaymentResultData,
								PaymentAuthorizationResultData.PaymentMethod.PAYPAL);
			}
			else if (infoModel instanceof InvoicePaymentInfoModel)
			{
				return PaymentAuthorizationResultData.success();
			}
			else
			{
				final PaymentTransactionEntryModel authorizeEntry = wileyMPGSPaymentService.authorize(cartModel);
				final boolean isAuthorizedSuccessfully = de.hybris.platform.payment.dto.TransactionStatus.ACCEPTED.name()
						.equals(authorizeEntry.getTransactionStatus());

				return isAuthorizedSuccessfully ? PaymentAuthorizationResultData.success() :
						PaymentAuthorizationResultData.failure(authorizeEntry.getTransactionStatus(),
								wileyMPGSErrorService.getAuthorizationStorefrontMessageKey(authorizeEntry),
								PaymentAuthorizationResultData.PaymentMethod.MPGS);
			}

		}

		return PaymentAuthorizationResultData.failure(de.hybris.platform.payment.dto.TransactionStatus.ERROR.name());
	}

	@Override
	public boolean checkSelectedPaymentMethodIsAvailable(final CartData cartData)
	{
		return cartData.isTaxAvailable() || PaymentModeEnum.INVOICE.getCode().equals(cartData.getPaymentMode())
				|| PaymentModeEnum.PROFORMA.getCode().equals(cartData.getPaymentMode());
	}
}
