package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.math.BigDecimal;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.wileyb2c.product.Wileyb2cCommercePriceService;
import com.wiley.facades.wileyb2c.product.data.ProductSetData;


/**
 * Created by Uladzimir_Barouski on 5/2/2017.
 */
public class Wileyb2cProductSetPricePopulator implements Populator<ProductModel, ProductSetData>
{
	private Wileyb2cCommercePriceService priceService;

	private PriceDataFactory priceDataFactory;

	@Override
	public void populate(@Nonnull final ProductModel source, @Nonnull final ProductSetData target) throws ConversionException
	{
		ServicesUtil.validateParameterNotNullStandardMessage("productModel", source);
		ServicesUtil.validateParameterNotNullStandardMessage("productSetData", target);

		PriceInformation originalPrice = priceService.getComponentsOriginalPrice(source);
		final Double originalPriceValue = getPriceValue(originalPrice);
		PriceInformation productSetPrice = priceService.getWebPriceForProduct(source);
		final Double productSetPriceValue = getPriceValue(productSetPrice);
		if (originalPrice != null)
		{
			Double savingPrice = originalPriceValue - productSetPriceValue;
			if (savingPrice > 0)
			{
				PriceData originalPriceData = createPriceData(originalPrice.getPriceValue().getCurrencyIso(), originalPriceValue);
				PriceData savingPriceData = createPriceData(originalPrice.getPriceValue().getCurrencyIso(), savingPrice);
				target.setOriginalPrice(originalPriceData);
				target.setSave(savingPriceData);
			}
		}
	}

	private Double getPriceValue(final PriceInformation price)
	{
		Double priceValue = 0.0;
		if (price != null)
		{
			priceValue = price.getPriceValue().getValue();
		}
		return priceValue;
	}

	private PriceData createPriceData(final String currencyIso, final Double savingPrice)
	{
		return priceDataFactory.create(PriceDataType.BUY,
				BigDecimal.valueOf(savingPrice), currencyIso);
	}

	@Required
	public void setPriceService(final Wileyb2cCommercePriceService priceService)
	{
		this.priceService = priceService;
	}

	@Required
	public void setPriceDataFactory(final PriceDataFactory priceDataFactory)
	{
		this.priceDataFactory = priceDataFactory;
	}
}
