package com.wiley.facades.wileyb2c.search.converters.populator;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.Collection;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Required;


/**
 * Created by Raman_Hancharou on 6/13/2017.
 */
public class Wileyb2cSearchResultRelatedProductsDisclaimerPopulator implements Populator<SearchResultValueData, ProductData>
{

	private String hasRelatedProductsProperty;

	@Override
	public void populate(final SearchResultValueData searchResultValueData, final ProductData productData)
			throws ConversionException
	{
		Collection<SearchResultValueData> variants = searchResultValueData.getVariants();
		if (variants != null)
		{
			productData.setShowRelatedProductsDisclaimer(CollectionUtils.isNotEmpty(variants
					.stream()
					.filter(variant -> BooleanUtils.isTrue((Boolean) variant.getValues().get(hasRelatedProductsProperty)))
					.collect(Collectors.toList())));
		}
	}

	@Required
	public void setHasRelatedProductsProperty(final String hasRelatedProductsProperty)
	{
		this.hasRelatedProductsProperty = hasRelatedProductsProperty;
	}

}
