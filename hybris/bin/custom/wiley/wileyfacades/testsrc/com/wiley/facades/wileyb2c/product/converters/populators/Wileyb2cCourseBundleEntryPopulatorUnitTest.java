package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.catalog.references.ProductReferenceService;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.commerceservices.price.CommercePriceService;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.util.PriceValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.enums.WileyProductSubtypeEnum;
import com.wiley.facades.populators.WileyPriceDataFactory;
import com.wiley.facades.wileyb2c.product.data.CourseBundleData;


/**
 * Created by Uladzimir_Barouski on 8/25/2016.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cCourseBundleEntryPopulatorUnitTest
{

	private static final String PRODUCT_SET_CODE = "product_set_code";
	private static final double COURSE_PRICE = 250.01;
	private static final double EXTRA_PRODUCT_PRICE = 100.49;
	private static final double PRODUC_SET_PRICE = 299.51;
	private static final String CURRENCY = "USD";
	private static final String EBOOK_ALT_TEXT_KEY = "wileypluscourse.purchase.option.instant-access.product.ebook.alt.text";
	private static final String LOOSELEAF_ALT_TEXT_KEY =
			"wileypluscourse.purchase.option.instant-access.product.looseleaf.alt.text";
	private static final String EBOOK_COMPONENT_TITLE_KEY = "ebook";
	private static final String LOOSELEAF_COMPONENT_TITLE_KEY = "looseleaf";

	@Mock
	private ProductReferenceService productReferenceServiceMock;

	@Mock
	private CommercePriceService wileyb2cCommercePriceServiceMock;

	@InjectMocks
	private Wileyb2cCourseBundleEntryPopulator wileyb2cCourseBundleEntryPopulator;
	@InjectMocks
	@Spy
	private PriceDataFactory priceDataFactoryMock = new WileyPriceDataFactory();

	@Mock
	private CommonI18NService commonI18NServiceMock;
	@Mock
	private CommerceCommonI18NService commerceCommonI18NServiceMock;


	private ProductModel productSet;

	private ProductReferenceModel productSetReference1;
	private ProductReferenceModel productSetReference2;

	@Before
	public void setUp() throws Exception
	{
		productSet = new ProductModel();
		productSet.setCode(PRODUCT_SET_CODE);
		productSetReference1 = new ProductReferenceModel();
		ProductModel courseProduct = new ProductModel();
		courseProduct.setSubtype(WileyProductSubtypeEnum.COURSE);
		productSetReference1.setTarget(courseProduct);
		productSetReference2 = new ProductReferenceModel();
		ProductModel extraProduct = new ProductModel();
		productSetReference2.setTarget(extraProduct);

		PriceInformation productSetPriceInfo = new PriceInformation(getPriceValue(CURRENCY, PRODUC_SET_PRICE));
		PriceInformation coursePriceInfo = new PriceInformation(getPriceValue(CURRENCY, COURSE_PRICE));
		PriceInformation extraProductPriceInfo = new PriceInformation(getPriceValue(CURRENCY, EXTRA_PRODUCT_PRICE));

		when(wileyb2cCommercePriceServiceMock.getWebPriceForProduct(productSet)).thenReturn(productSetPriceInfo);
		when(wileyb2cCommercePriceServiceMock.getWebPriceForProduct(productSetReference1.getTarget())).thenReturn(
				coursePriceInfo);
		when(wileyb2cCommercePriceServiceMock.getWebPriceForProduct(productSetReference2.getTarget())).thenReturn(
				extraProductPriceInfo);
		//need for priceFactory service
		CurrencyModel currency = new CurrencyModel();
		currency.setIsocode(CURRENCY);
		when(commonI18NServiceMock.getCurrency(CURRENCY)).thenReturn(currency);
		when(commerceCommonI18NServiceMock.getLocaleForLanguage(any(LanguageModel.class))).thenReturn(new Locale("EN"));
	}

	@Test
	public void testPopulateSetWithEbook() throws Exception
	{
		productSetReference2.getTarget().setSubtype(WileyProductSubtypeEnum.EBOOK);
		List<ProductReferenceModel> productReferenceList = Arrays.asList(productSetReference1, productSetReference2);
		when(productReferenceServiceMock
				.getProductReferencesForSourceProduct(productSet, ProductReferenceTypeEnum.PRODUCT_SET_COMPONENT, true))
				.thenReturn(
						productReferenceList);
		CourseBundleData courseBundleData = new CourseBundleData();
		wileyb2cCourseBundleEntryPopulator.populate(productSet, courseBundleData);
		assertTrue(courseBundleData.getMostPopular());
		assertNotNull(courseBundleData.getDiscountedPriceValue());
		assertEquals(299.51, courseBundleData.getDiscountedPriceValue().getValue().doubleValue(), 0);
		assertNotNull(courseBundleData.getDiscountedPriceSaving());
		assertEquals(50.99, courseBundleData.getDiscountedPriceSaving().getValue().doubleValue(), 0);
		assertNotNull(courseBundleData.getExtraComponentPrice());
		assertEquals(100.49, courseBundleData.getExtraComponentPrice().getValue().doubleValue(), 0);
		assertNotNull(courseBundleData.getExtraComponentSaving());
		assertEquals(49.50, courseBundleData.getExtraComponentSaving().getValue().doubleValue(), 0);
		assertEquals(EBOOK_ALT_TEXT_KEY, courseBundleData.getExtraComponentAlttextKey());
		assertEquals(EBOOK_COMPONENT_TITLE_KEY, courseBundleData.getExtraComponentNameKey());
	}

	@Test
	public void testPopulateSetWithLooseleaf() throws Exception
	{
		productSetReference2.getTarget().setSubtype(WileyProductSubtypeEnum.LOOSE_LEAF);
		List<ProductReferenceModel> productReferenceList = Arrays.asList(productSetReference1, productSetReference2);
		when(productReferenceServiceMock
				.getProductReferencesForSourceProduct(productSet, ProductReferenceTypeEnum.PRODUCT_SET_COMPONENT, true))
				.thenReturn(
						productReferenceList);
		CourseBundleData courseBundleData = new CourseBundleData();
		wileyb2cCourseBundleEntryPopulator.populate(productSet, courseBundleData);
		assertFalse(courseBundleData.getMostPopular());
		assertEquals(LOOSELEAF_ALT_TEXT_KEY, courseBundleData.getExtraComponentAlttextKey());
		assertEquals(LOOSELEAF_COMPONENT_TITLE_KEY, courseBundleData.getExtraComponentNameKey());
	}

	@Test(expected = ConversionException.class)
	public void testPopulateWhenComponentsMoreThanThree()
	{
		List<ProductReferenceModel> productReferenceList = Arrays.asList(productSetReference1, productSetReference2,
				productSetReference1);
		when(productReferenceServiceMock
				.getProductReferencesForSourceProduct(productSet, ProductReferenceTypeEnum.PRODUCT_SET_COMPONENT, true))
				.thenReturn(
						productReferenceList);
		CourseBundleData courseBundleData = new CourseBundleData();
		wileyb2cCourseBundleEntryPopulator.populate(productSet, courseBundleData);
	}

	@Test(expected = ConversionException.class)
	public void testPopulateWhenComponentHasNotAcceptableType()
	{
		productSetReference2.getTarget().setSubtype(WileyProductSubtypeEnum.HARDCOVER);
		List<ProductReferenceModel> productReferenceList = Arrays.asList(productSetReference1, productSetReference2);
		when(productReferenceServiceMock
				.getProductReferencesForSourceProduct(productSet, ProductReferenceTypeEnum.PRODUCT_SET_COMPONENT, true))
				.thenReturn(
						productReferenceList);
		CourseBundleData courseBundleData = new CourseBundleData();
		wileyb2cCourseBundleEntryPopulator.populate(productSet, courseBundleData);
	}

	private PriceValue getPriceValue(final String currency, final double value)
	{
		return new PriceValue(currency, value, true);
	}
}