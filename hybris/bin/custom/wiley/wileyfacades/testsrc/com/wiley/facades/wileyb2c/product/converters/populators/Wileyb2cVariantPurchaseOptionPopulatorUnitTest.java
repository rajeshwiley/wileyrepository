package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ImageDataType;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaFormatModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.variants.model.VariantProductModel;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.enums.WileyWebLinkTypeEnum;
import com.wiley.core.model.WileyPurchaseOptionProductModel;
import com.wiley.core.model.WileyWebLinkModel;
import com.wiley.core.wileyb2c.classification.Wileyb2cClassificationAttributes;
import com.wiley.core.wileyb2c.classification.Wileyb2cClassificationService;
import com.wiley.facades.wileyb2c.product.converters.populators.helper.Wileyb2cAuthorPopulatorHelper;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cVariantPurchaseOptionPopulatorUnitTest
{
	private static final String CODE = "CODE";
	private static final Locale CURRENT_LOCALE = Locale.CANADA;
	private static final String NAME = "NAME";
	private static final String DESCRIPTION = "description";
	private static final Integer SEQUENCE_NUMBER = 55;
	private static final Date PUBLICATION_DATE = new Date();
	private static final Boolean COUNTABLE = Boolean.TRUE;
	private static final String AUTHORS = "author 1, author 2";
	private static final String MEDIA_FORMAT = "normal";
	private static final Integer GALLERY_INDEX = 0;
	@InjectMocks
	private Wileyb2cVariantPurchaseOptionPopulator wileyb2cVariantPurchaseOptionPopulator;
	@Mock
	private WileyPurchaseOptionProductModel variantProductModel;
	@Mock
	private VariantProductModel productModel;
	private VariantOptionData variantOptionData = new VariantOptionData();
	@Mock
	private CommerceCommonI18NService commerceCommonI18NService;
	@Mock
	private Wileyb2cClassificationService wileyb2cClassificationService;
	@Mock
	private Wileyb2cClassificationService wileyb2cSolrIndexClassificationService;
	@Mock
	private ClassificationClassModel classificationClass;
	@Mock
	private Wileyb2cAuthorPopulatorHelper wileyb2cAuthorPopulatorHelper;
	@Mock
	private ProductModel baseProduct;
	@Mock
	private MediaContainerModel mediaContainerModel1;
	@Mock
	private MediaModel mediaModel1;
	@Mock
	private MediaFormatModel mediaFormatModel;
	@Mock
	private Converter<MediaModel, ImageData> imageConverter;

	private List<WileyWebLinkModel> externalStores = new ArrayList<>();
	private List<MediaContainerModel> galleryImages = new ArrayList<>();
	private List<MediaModel> medias = new ArrayList<>();

	@Before
	public void setUp()
	{
		WileyWebLinkModel wileyPlus = new WileyWebLinkModel();
		wileyPlus.setType(WileyWebLinkTypeEnum.WILEY_PLUS_STORE);
		WileyWebLinkModel requestQuote = new WileyWebLinkModel();
		requestQuote.setType(WileyWebLinkTypeEnum.REQUEST_QUOTE);
		externalStores.add(wileyPlus);
		externalStores.add(requestQuote);
		when(variantProductModel.getExternalStores()).thenReturn(externalStores);
		when(variantProductModel.getBaseProduct()).thenReturn(baseProduct);
		galleryImages.add(mediaContainerModel1);
		medias.add(mediaModel1);
	}

	@Test
	public void populateWhenWileyPurchaseOptionProductShouldAddPurchaseOptionInfo()
	{
		prepareVariantMocks();

		wileyb2cVariantPurchaseOptionPopulator.populate(variantProductModel, variantOptionData);

		assertEquals(variantOptionData.getCode(), CODE);
		assertEquals(variantOptionData.getPurchaseOptionName(), NAME);
		assertEquals(variantOptionData.getPurchaseOptionDescription(), DESCRIPTION);
		assertEquals(variantOptionData.getPurchaseOptionSequenceNumber(), SEQUENCE_NUMBER);
		assertEquals(variantOptionData.getPublicationDate(), PUBLICATION_DATE);
		assertEquals(variantOptionData.getCountable(), COUNTABLE);
		assertEquals(variantOptionData.getAuthorsWithoutRoles(), AUTHORS);
		assertTrue(variantOptionData.isHidePrice());
		assertTrue(CollectionUtils.isEmpty(variantOptionData.getImages()));
	}

	@Test
	public void populateWhenNonWileyPurchaseOptionProductModelShouldDoNothing()
	{
		wileyb2cVariantPurchaseOptionPopulator.populate(productModel, variantOptionData);

		assertNull(variantOptionData.getCode());
		assertNull(variantOptionData.getPurchaseOptionName());
		assertNull(variantOptionData.getPurchaseOptionDescription());
		assertNull(variantOptionData.getPurchaseOptionSequenceNumber());
		assertTrue(CollectionUtils.isEmpty(variantOptionData.getImages()));
	}

	@Test
	public void testGalleryImages()
	{
		when(baseProduct.getGalleryImages()).thenReturn(galleryImages);
		testImages();
	}

	@Test
	public void testExternalImage()
	{
		when(baseProduct.getExternalImage()).thenReturn(mediaContainerModel1);
		testImages();
	}

	private void testImages()
	{
		prepareVariantMocks();
		final ImageData imageData = new ImageData();
		when(mediaContainerModel1.getMedias()).thenReturn(medias);
		when(mediaModel1.getMediaFormat()).thenReturn(mediaFormatModel);
		when(mediaFormatModel.getQualifier()).thenReturn(MEDIA_FORMAT);
		when(imageConverter.convert(mediaModel1)).thenReturn(imageData);

		wileyb2cVariantPurchaseOptionPopulator.populate(variantProductModel, variantOptionData);

		assertNotNull(variantOptionData.getImages());
		assertFalse(variantOptionData.getImages().isEmpty());
		final ImageData image = variantOptionData.getImages().iterator().next();
		assertEquals(GALLERY_INDEX, image.getGalleryIndex());
		assertEquals("product", image.getFormat());
		assertEquals(ImageDataType.GALLERY, image.getImageType());
	}

	void prepareVariantMocks()
	{
		when(variantProductModel.getCode()).thenReturn(CODE);
		when(variantProductModel.getCountable()).thenReturn(COUNTABLE);
		when(commerceCommonI18NService.getCurrentLocale()).thenReturn(Locale.CANADA);
		when(wileyb2cClassificationService.resolveClassificationClass(variantProductModel)).thenReturn(classificationClass);
		when(classificationClass.getName(CURRENT_LOCALE)).thenReturn(NAME);
		when(classificationClass.getDescription(CURRENT_LOCALE)).thenReturn(DESCRIPTION);
		when(classificationClass.getSequenceNumber()).thenReturn(SEQUENCE_NUMBER);
		when(wileyb2cSolrIndexClassificationService
				.resolveAttributeValue(variantProductModel, Wileyb2cClassificationAttributes.PUBLICATION_DATE)).thenReturn(
				PUBLICATION_DATE);
		when(wileyb2cAuthorPopulatorHelper.getAuthorsWithoutRoles(variantProductModel)).thenReturn(AUTHORS);
	}
}
