package com.wiley.facades.ags.order.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;


/**
 * Created on 1/21/2016.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AgsAcceleratorCheckoutFacadeTest
{
	private static final String TEST_USA = "US";
	private static final String TEST_DEU = "DE";
	private static final String TEST_FRA = "FR";
	private static final String TEST_UNITED_STATES = "United States";
	private static final String TEST_GERMANY = "Germany";
	private static final String TEST_FRANCE = "France";

	private final CountryData usa = new CountryData();
	private final CountryData deu = new CountryData();
	private final CountryData fra = new CountryData();
	private final CountryData empty = new CountryData();
	protected static final int BEFORE = -1;
	protected static final int EQUAL = 0;
	protected static final int AFTER = 1;
	@Mock
	private CountryModel usaModel;
	@Mock
	private CountryModel deuModel;
	@Mock
	private CountryModel fraModel;
	@Mock
	private CountryModel emptyModel;

	@Mock
	private CommonI18NService commonI18NService;

	@Mock
	private Converter<CountryModel, CountryData> countryConverter;

	@Mock
	private Comparator<CountryData> comparator;

	@Spy
	@InjectMocks
	private final AgsAcceleratorCheckoutFacade testAgsAcceleratorCheckoutFacade = new AgsAcceleratorCheckoutFacade();

	@Before
	public void setUp() throws Exception
	{
		testAgsAcceleratorCheckoutFacade.setCountryConverter(countryConverter);

		usa.setIsocode(TEST_USA);
		deu.setIsocode(TEST_DEU);
		fra.setIsocode(TEST_FRA);
		usa.setName(TEST_UNITED_STATES);
		deu.setName(TEST_GERMANY);
		fra.setName(TEST_FRANCE);

		given(usaModel.getIsocode()).willReturn(TEST_USA);
		given(deuModel.getIsocode()).willReturn(TEST_DEU);
		given(fraModel.getIsocode()).willReturn(TEST_FRA);
		given(usaModel.getName()).willReturn(TEST_UNITED_STATES);
		given(deuModel.getName()).willReturn(TEST_GERMANY);
		given(fraModel.getName()).willReturn(TEST_FRANCE);

		List<CountryModel> countries = new ArrayList<>();
		countries.add(deuModel);
		countries.add(fraModel);
		countries.add(usaModel);
		countries.add(emptyModel);

		given(commonI18NService.getAllCountries()).willReturn(countries);
		given(countryConverter.convert(usaModel)).willReturn(usa);
		given(countryConverter.convert(deuModel)).willReturn(deu);
		given(countryConverter.convert(fraModel)).willReturn(fra);
		given(countryConverter.convert(emptyModel)).willReturn(empty);

		given(comparator.compare(usa, usa)).willReturn(EQUAL);
		given(comparator.compare(usa, deu)).willReturn(BEFORE);
		given(comparator.compare(usa, fra)).willReturn(BEFORE);
		given(comparator.compare(deu, usa)).willReturn(AFTER);
		given(comparator.compare(fra, usa)).willReturn(AFTER);
		given(comparator.compare(usa, deu)).willReturn(BEFORE);
		given(comparator.compare(usa, empty)).willReturn(BEFORE);
		given(comparator.compare(empty, deu)).willReturn(BEFORE);
		given(comparator.compare(deu, empty)).willReturn(AFTER);

		given(comparator.compare(empty, fra)).willReturn(BEFORE);
		given(comparator.compare(fra, empty)).willReturn(AFTER);

		given(comparator.compare(fra, deu)).willReturn(BEFORE);
		given(comparator.compare(deu, fra)).willReturn(AFTER);
	}

	@Test
	public void testSortOrderInList()
	{
		List<CountryData> countries = testAgsAcceleratorCheckoutFacade.getBillingCountries();

		assertEquals("Expected USA on first position in list", TEST_USA, countries.get(0).getIsocode());
		assertEquals("Expected NULL on second position in list", null, countries.get(1).getIsocode());
		assertEquals("Expected FRA on third position in list", TEST_FRA, countries.get(2).getIsocode());
		assertEquals("Expected DEU on last position in list", TEST_DEU, countries.get(3).getIsocode());
	}

}
