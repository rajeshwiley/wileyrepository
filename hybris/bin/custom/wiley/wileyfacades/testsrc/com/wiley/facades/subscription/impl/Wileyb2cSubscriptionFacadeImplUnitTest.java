package com.wiley.facades.subscription.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.converter.ConfigurablePopulator;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileySubscriptionModel;
import com.wiley.core.subscription.exception.SubscriptionUpdateException;
import com.wiley.core.util.TestUtils;
import com.wiley.core.wileyb2c.subscription.services.Wileyb2cSubscriptionService;
import com.wiley.facades.product.data.WileySubscriptionData;
import com.wiley.facades.subscription.WileySubscriptionOption;


/**
 * Unit test for {@link Wileyb2cSubscriptionFacadeImpl}
 *
 * @author Robert_Farkas
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cSubscriptionFacadeImplUnitTest
{
	private static final String ANOTHER_CUSTOMER_UID = "another-customer-uid";
	private static final String DELIVERY_ADDRESS_CODE = "delivery-address-code";
	private static final String SUBSCRIPTION_CODE = "subscription-code";
	private static final String CUSTOMER_UID = "customer-uid";

	@InjectMocks
	private Wileyb2cSubscriptionFacadeImpl wileyb2cSubscriptionFacade;

	@Mock
	private Wileyb2cSubscriptionService wileyb2cSubscriptionServiceMock;

	@Mock
	private WileySubscriptionModel wileySubscriptionModelMock;

	@Mock
	private Converter<WileySubscriptionModel, WileySubscriptionData> wileyb2cSubscriptionDataConverterMock;

	@Mock
	private ConfigurablePopulator<WileySubscriptionModel, WileySubscriptionData, WileySubscriptionOption>
			wileyb2cSubscriptionConfiguredPopulatorMock;

	@Mock
	private WileySubscriptionData wileySubscriptionDataMock;

	@Mock
	private CustomerAccountService customerAccountServiceMock;

	@Mock
	private UserService userServiceMock;

	@Mock
	private CustomerModel customerModelMock;

	@Mock
	private CustomerModel anotherCustomerModelMock;

	@Mock
	private AddressModel addressModelMock;

	@Mock
	private AddressModel clonedAddressModelMock;

	@Mock
	private ModelService modelServiceMock;

	@Before
	public void setup()
	{
		wileyb2cSubscriptionFacade.setWileyb2cSubscriptionDataConverter(wileyb2cSubscriptionDataConverterMock);

		when(wileyb2cSubscriptionServiceMock.getSubscriptionByCode(SUBSCRIPTION_CODE)).thenReturn(wileySubscriptionModelMock);
		when(userServiceMock.getCurrentUser()).thenReturn(customerModelMock);
		when(customerAccountServiceMock.getAddressForCode(customerModelMock, DELIVERY_ADDRESS_CODE)).thenReturn(addressModelMock);
		when(wileyb2cSubscriptionDataConverterMock.convert(wileySubscriptionModelMock)).thenReturn(wileySubscriptionDataMock);
		when(customerModelMock.getUid()).thenReturn(CUSTOMER_UID);
		when(anotherCustomerModelMock.getUid()).thenReturn(ANOTHER_CUSTOMER_UID);
		when(wileySubscriptionModelMock.getCustomer()).thenReturn(customerModelMock);
		when(modelServiceMock.clone(addressModelMock)).thenReturn(clonedAddressModelMock);
	}

	@Test
	public void updateDeliveryAddressShouldFailIfSubscriptionServiceThrowsSubscriptionException()
	{
		// Given
		doThrow(new SubscriptionUpdateException("Useless exception message"))
				.when(wileyb2cSubscriptionServiceMock).updateDeliveryAddress(wileySubscriptionModelMock, addressModelMock);

		// When
		boolean result = wileyb2cSubscriptionFacade.updateDeliveryAddress(SUBSCRIPTION_CODE, DELIVERY_ADDRESS_CODE);

		// Then
		verify(wileyb2cSubscriptionServiceMock).updateDeliveryAddress(wileySubscriptionModelMock, addressModelMock);
		assertFalse(result);
	}

	@Test
	public void updateDeliveryAddressShouldFollowHappyPath()
	{
		// When
		boolean result = wileyb2cSubscriptionFacade.updateDeliveryAddress(SUBSCRIPTION_CODE, DELIVERY_ADDRESS_CODE);

		// Then
		verify(wileyb2cSubscriptionServiceMock).updateDeliveryAddress(wileySubscriptionModelMock, addressModelMock);
		assertTrue(result);
	}

	@Test(expected = IllegalArgumentException.class)
	public void getSubscriptionDataForCurrentUserShouldFailIfSubscriptionNotFound()
	{
		// Given
		when(wileyb2cSubscriptionServiceMock.getSubscriptionByCode(SUBSCRIPTION_CODE))
				.thenThrow(UnknownIdentifierException.class);

		// When
		wileyb2cSubscriptionFacade.getSubscriptionDataForCurrentUser(SUBSCRIPTION_CODE);

		// Then
		fail("Exception should have been thrown for missing subscription!");
	}

	@Test(expected = IllegalArgumentException.class)
	public void getSubscriptionDataForCurrentUserShouldFailIfSubscriptionCustomerNameDoesntMatch()
	{
		// Given
		when(wileySubscriptionModelMock.getCustomer()).thenReturn(anotherCustomerModelMock);

		// When
		wileyb2cSubscriptionFacade.getSubscriptionDataForCurrentUser(SUBSCRIPTION_CODE);

		// Then
		fail("Exception should have been thrown for non-matching customer name!");
	}

	@Test
	public void getSubscriptionDataForCurrentUserShouldFollowHappyPath()
	{
		// When
		WileySubscriptionData subscriptionData = wileyb2cSubscriptionFacade.getSubscriptionDataForCurrentUser(SUBSCRIPTION_CODE);

		// Then
		assertSame("The method should have returned with the converted subscription!",
				subscriptionData, wileySubscriptionDataMock);
	}

	@Test
	public void testGetSubscriptionDataForCurrentUserWithOptionsSuccessCase()
	{
		// Given
		final List<WileySubscriptionOption> options = Arrays.asList(WileySubscriptionOption.ORDER_DATA);

		// When
		final WileySubscriptionData subscriptionData =
				wileyb2cSubscriptionFacade.getSubscriptionDataForCurrentUserWithOptions(SUBSCRIPTION_CODE, options);

		// Then
		assertNotNull(subscriptionData);
		assertSame(wileySubscriptionDataMock, subscriptionData);
		verify(wileyb2cSubscriptionDataConverterMock).convert(same(wileySubscriptionModelMock));
		verify(wileyb2cSubscriptionConfiguredPopulatorMock).populate(same(wileySubscriptionModelMock),
				same(wileySubscriptionDataMock), eq(options));
	}

	@Test
	public void testGetSubscriptionDataForCurrentUserWithIllegalParameters()
	{
		// Given
		// Setup the system under test

		// When
		TestUtils.expectException(IllegalArgumentException.class,
				() -> wileyb2cSubscriptionFacade.getSubscriptionDataForCurrentUserWithOptions(null, null));
		// Then
		verifyZeroInteractions(wileyb2cSubscriptionDataConverterMock, wileyb2cSubscriptionConfiguredPopulatorMock);

		// -----------------------------------------------------------

		// When
		TestUtils.expectException(IllegalArgumentException.class,
				() -> wileyb2cSubscriptionFacade.getSubscriptionDataForCurrentUserWithOptions(SUBSCRIPTION_CODE, null));
		// Then
		verifyZeroInteractions(wileyb2cSubscriptionDataConverterMock, wileyb2cSubscriptionConfiguredPopulatorMock);

		// -----------------------------------------------------------

		// When
		TestUtils.expectException(IllegalArgumentException.class,
				() -> wileyb2cSubscriptionFacade.getSubscriptionDataForCurrentUserWithOptions(null, Collections.emptyList()));
		// Then
		verifyZeroInteractions(wileyb2cSubscriptionDataConverterMock, wileyb2cSubscriptionConfiguredPopulatorMock);
	}
}
