package com.wiley.facades.wileyb2b.order.converters.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.BDDMockito.given;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

import com.wiley.core.model.ExternalInventoryModel;
import com.wiley.facades.product.data.InventoryStatusRecord;
import com.wiley.facades.wileyb2b.customer.converters.populators.Wileyb2bInventoryStatusPopulator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * @author Anton_lukyanau
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2bInventoryStatusPopulatorTest
{
	private static final long ONE_DAY_MILLIS = 1000 * 60 * 60 * 24;
	private static final Date THE_FUTURE = new Date(new Date().getTime() + ONE_DAY_MILLIS);
	private static final Date THE_PAST = new Date(new Date().getTime() - ONE_DAY_MILLIS);
	private static final String PRODUCT_CODE = "007";
	private static final long QUANTITY = 3;
	private static final Date DATE = new Date();
	private static final List<StockLevelStatus> STATUSES = Arrays.asList(StockLevelStatus.INSTOCK,
			StockLevelStatus.LOWSTOCK,
			StockLevelStatus.BACK_ORDER);

	private OrderEntryData orderEntryData;

	@Mock
	CartModel cartModelMock;
	@Mock
	AbstractOrderEntryModel abstractOrderEntryModelMock;
	@Mock
	ProductModel productModelMock;
	@Mock
	ExternalInventoryModel externalInventoryModelMock;
	@Mock
	ExternalInventoryModel externalInventoryModelMock2;
	@Mock
	ExternalInventoryModel externalInventoryModelMock3;

	@InjectMocks
	Wileyb2bInventoryStatusPopulator wileyb2bInventoryStatusPopulator;

	@Before
	public void setUp()
	{
		ProductData productData = new ProductData();
		productData.setCode(PRODUCT_CODE);

		orderEntryData = new OrderEntryData();
		orderEntryData.setProduct(productData);
	}

	@Test
	public void populateTest()
	{
		given(abstractOrderEntryModelMock.getProduct()).willReturn(productModelMock);
		given(abstractOrderEntryModelMock.getExternalInventories()).
				willReturn(new HashSet<>(
						Arrays.asList(externalInventoryModelMock, externalInventoryModelMock2, externalInventoryModelMock3)));
		given(productModelMock.getCode()).willReturn(PRODUCT_CODE);


		Queue<StockLevelStatus> statuses = new LinkedList<>();
		statuses.addAll(STATUSES);

		abstractOrderEntryModelMock.getExternalInventories().forEach(extInvMock ->
				{
					given(extInvMock.getQuantity()).willReturn(QUANTITY);
					given(extInvMock.getEstimatedDeliveryDate()).willReturn(DATE);
					given(extInvMock.getStatus()).willReturn(statuses.poll());
				}
		);

		wileyb2bInventoryStatusPopulator.populate(abstractOrderEntryModelMock, orderEntryData);

		statuses.addAll(STATUSES);
		orderEntryData.getProduct().getInventoryStatus().forEach(inventoryStatusRecord ->
		{
			assertNotNull(inventoryStatusRecord);
			assertEquals(DATE, inventoryStatusRecord.getAvailableDate());
			assertEquals(QUANTITY, (long) inventoryStatusRecord.getQuantity());
			assertEquals(statuses.poll(), inventoryStatusRecord.getStatusCode());
		});
	}

	@Test
	public void sortingTest()
	{
		//Given
		ExternalInventoryModel expectedFirst = createExternalInventoryModel(DATE, StockLevelStatus.INSTOCK, QUANTITY);
		ExternalInventoryModel expectedSecond = createExternalInventoryModel(THE_PAST, StockLevelStatus.PRE_ORDER, QUANTITY);
		ExternalInventoryModel expectedThird = createExternalInventoryModel(DATE, StockLevelStatus.PRE_ORDER, QUANTITY);
		ExternalInventoryModel expectedForth = createExternalInventoryModel(THE_FUTURE, StockLevelStatus.PRE_ORDER, QUANTITY);
		ExternalInventoryModel expectedFifth = createExternalInventoryModel(DATE, StockLevelStatus.PRINT_ON_DEMAND, QUANTITY);
		Set<ExternalInventoryModel> actualExternalInventories = new HashSet<>(
				Arrays.asList(expectedThird, expectedFifth, expectedFirst, expectedSecond, expectedForth));
		given(abstractOrderEntryModelMock.getExternalInventories()).
				willReturn(actualExternalInventories);

		List<InventoryStatusRecord> expectedRecords = createInventoryStatusRecords(expectedFirst, expectedSecond, expectedThird,
				expectedForth, expectedFifth);

		//When
		wileyb2bInventoryStatusPopulator.populate(abstractOrderEntryModelMock, orderEntryData);

		//Than
		assertInventoryStatusOrder(expectedRecords, orderEntryData.getProduct().getInventoryStatus());
	}

	private void assertInventoryStatusOrder(final List<InventoryStatusRecord> expected, final List<InventoryStatusRecord> actual)
	{
		assertNotNull(actual);
		assertEquals(expected.size(), actual.size());
		for (int i = 0; i < expected.size(); i++)
		{
			assertEquals(expected.get(i).getStatusCode(), actual.get(i).getStatusCode());
			assertEquals(expected.get(i).getAvailableDate(), actual.get(i).getAvailableDate());
			assertEquals(expected.get(i).getQuantity(), actual.get(i).getQuantity());
		}
	}

	private ExternalInventoryModel createExternalInventoryModel(final Date estimatedDeliveryDate,
			final StockLevelStatus stockLevelStatus, final Long quantity)
	{
		ExternalInventoryModel externalInventory = new ExternalInventoryModel();
		externalInventory.setEstimatedDeliveryDate(estimatedDeliveryDate);
		externalInventory.setStatus(stockLevelStatus);
		externalInventory.setQuantity(quantity);
		return externalInventory;
	}

	private List<InventoryStatusRecord> createInventoryStatusRecords(final ExternalInventoryModel... args)
	{
		List<InventoryStatusRecord> result = new ArrayList<>(args.length);
		for (ExternalInventoryModel inventoryModel : args)
		{
			InventoryStatusRecord record = new InventoryStatusRecord();
			record.setQuantity(inventoryModel.getQuantity());
			record.setAvailableDate(inventoryModel.getEstimatedDeliveryDate());
			record.setStatusCode(inventoryModel.getStatus());
			result.add(record);
		}
		return result;
	}

}





















