/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.facades.populators;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.subscriptionservices.model.SubscriptionProductModel;

import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.wiley.core.model.WileySubscriptionModel;
import com.wiley.facades.product.data.WileySubscriptionData;


/**
 * Test suite for {@link WileySubscriptionPopulator}
 */
@UnitTest
public class WileySubscriptionPopulatorTest
{


	private static final String TEST_NAME = "testName";

	private static final String TEST_CODE = "testCode";
	
	private static final String TEST_PRODUCT_CODE = "testProductCode";

	@Mock
	private WileySubscriptionModel wileySubscriptionModel;

	private WileySubscriptionPopulator wileySubscriptionPopulator;

	@Mock
	private SubscriptionProductModel subscriptionProductModel;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);

		wileySubscriptionPopulator = new WileySubscriptionPopulator();
	}


	@Test
	public void testPopulate()
	{

		given(wileySubscriptionModel.getCode()).willReturn(TEST_CODE);
		given(wileySubscriptionModel.getProduct()).willReturn(subscriptionProductModel);
		given(subscriptionProductModel.getName()).willReturn(TEST_NAME);
		given(subscriptionProductModel.getCode()).willReturn(TEST_PRODUCT_CODE);
		final DateTime dt = new DateTime(2004, 12, 25, 12, 0, 0, 0);
		given(wileySubscriptionModel.getExpirationDate()).willReturn(dt.toDate());
		given(wileySubscriptionModel.getRenewalEnabled()).willReturn(Boolean.TRUE);
		given(wileySubscriptionModel.getRequireRenewal()).willReturn(Boolean.TRUE);



		final WileySubscriptionData wileySubscriptionData = new WileySubscriptionData();
		wileySubscriptionPopulator.populate(wileySubscriptionModel, wileySubscriptionData);


		Assert.assertEquals(TEST_CODE, wileySubscriptionData.getCode());
		Assert.assertEquals(TEST_NAME, wileySubscriptionData.getName());
		Assert.assertEquals(TEST_PRODUCT_CODE, wileySubscriptionData.getProductCode());
		Assert.assertEquals(dt.toDate(), wileySubscriptionData.getEndDate());
		Assert.assertTrue("Auto Renew field in not true", wileySubscriptionData.isAutoRenew());
		Assert.assertTrue("Renewal Enable field in not true", wileySubscriptionData.isRenewalEnabled());
	}
}
