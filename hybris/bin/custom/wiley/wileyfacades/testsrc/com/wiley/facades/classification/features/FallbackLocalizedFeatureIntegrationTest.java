package com.wiley.facades.classification.features;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ClassificationData;
import de.hybris.platform.commercefacades.product.data.FeatureData;
import de.hybris.platform.commercefacades.product.data.FeatureValueData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.i18n.I18NService;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.classification.features.FallbackLocalizedFeature;
import com.wiley.core.classification.strategy.impl.WileyLoadStoreFeaturesStrategy;

import static org.fest.assertions.Assertions.assertThat;


/**
 * Integration test for {@link WileyLoadStoreFeaturesStrategy} and {@link FallbackLocalizedFeature}
 */
@IntegrationTest
public class FallbackLocalizedFeatureIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{

	public static final Logger LOG = LoggerFactory.getLogger(FallbackLocalizedFeatureIntegrationTest.class);

	public static final List<ProductOption> PRODUCT_OPTIONS = Arrays.asList(ProductOption.CLASSIFICATION);
	public static final String LANG_DE_ISOCODE = "de";
	public static final String LANG_EN_ISOCODE = "en";
	public static final String LANG_EN_US_ISOCODE = "en_US";
	public static final String LANG_EN_GB_ISOCODE = "en_GB";
	public static final String TEST_PRODUCT_CODE = "WCOM_PRODUCT";
	public static final String TEST_CLASS_CODE = "testClass";
	public static final String TEST_VALUE_TRUE = "true";
	public static final String TEST_ATTRIBUTE_LOCALIZED_EN_CODE = "testAttributeLocalizedEn";
	public static final String TEST_ATTRIBUTE_LOCALIZED_EN_US_CODE = "testAttributeLocalizedEnUs";
	public static final String TEST_ATTRIBUTE_LOCALIZED_EN_GB_CODE = "testAttributeLocalizedEnGb";

	public static final String TEST_RESOURCES_FOLDER = "/wileyfacades/test/FallbackLocalizedFeatureIntegrationTest";


	@Resource
	private I18NService i18NService;

	@Resource
	private CommonI18NService commonI18NService;

	@Resource
	private ProductFacade productFacade;


	@Before
	public void setUp() throws Exception
	{
		i18NService.setLocalizationFallbackEnabled(true);

		importCsv(TEST_RESOURCES_FOLDER + "/languages.impex", DEFAULT_ENCODING);
		importCsv(TEST_RESOURCES_FOLDER + "/classificationSystem.impex", DEFAULT_ENCODING);
	}

	@Test
	public void testLocalizedFeatureEn() throws Exception
	{
		//Given
		commonI18NService.setCurrentLanguage(commonI18NService.getLanguage(LANG_EN_ISOCODE));

		//When
		final ProductData product = productFacade.getProductForCodeAndOptions(TEST_PRODUCT_CODE, PRODUCT_OPTIONS);

		//Then
		assertTrueFeatureValue(product, TEST_ATTRIBUTE_LOCALIZED_EN_CODE);
		assertNoClassificationAttribute(product, TEST_ATTRIBUTE_LOCALIZED_EN_US_CODE);
		assertNoClassificationAttribute(product, TEST_ATTRIBUTE_LOCALIZED_EN_GB_CODE);
	}

	@Test
	public void testLocalizedFeatureEnUs() throws Exception
	{
		//Given
		commonI18NService.setCurrentLanguage(commonI18NService.getLanguage(LANG_EN_US_ISOCODE));

		//When
		final ProductData product = productFacade.getProductForCodeAndOptions(TEST_PRODUCT_CODE, PRODUCT_OPTIONS);

		//Then
		assertTrueFeatureValue(product, TEST_ATTRIBUTE_LOCALIZED_EN_CODE);
		assertTrueFeatureValue(product, TEST_ATTRIBUTE_LOCALIZED_EN_US_CODE);
		assertNoClassificationAttribute(product, TEST_ATTRIBUTE_LOCALIZED_EN_GB_CODE);
	}


	@Test
	public void testLocalizedFeatureEnGB() throws Exception
	{
		//Given
		commonI18NService.setCurrentLanguage(commonI18NService.getLanguage(LANG_EN_GB_ISOCODE));

		//When
		final ProductData product = productFacade.getProductForCodeAndOptions(TEST_PRODUCT_CODE, PRODUCT_OPTIONS);

		//Then
		assertTrueFeatureValue(product, TEST_ATTRIBUTE_LOCALIZED_EN_CODE);
		assertTrueFeatureValue(product, TEST_ATTRIBUTE_LOCALIZED_EN_US_CODE);
		assertTrueFeatureValue(product, TEST_ATTRIBUTE_LOCALIZED_EN_GB_CODE);
	}

	@Test
	public void testLocalizedFeatureDe() throws Exception
	{
		//Given
		commonI18NService.setCurrentLanguage(commonI18NService.getLanguage(LANG_DE_ISOCODE));

		//When
		final ProductData product = productFacade.getProductForCodeAndOptions(TEST_PRODUCT_CODE, PRODUCT_OPTIONS);

		//Then
		assertThat(product.getClassifications()).isNull();
	}

	private void assertNoClassificationAttribute(final ProductData product, final String classifcationAttributeCode)
	{
		Optional<FeatureData> featureOptional = getFeatureDataFromTestClass(product, classifcationAttributeCode);
		assertThat(featureOptional.isPresent()).isFalse();

	}

	private void assertTrueFeatureValue(final ProductData product, final String classifcationAttributeCode)
	{
		Optional<FeatureData> featureOptional = getFeatureDataFromTestClass(product, classifcationAttributeCode);
		assertThat(featureOptional.isPresent()).isTrue();

		FeatureValueData valueData = featureOptional.get().getFeatureValues().iterator().next();
		assertThat(valueData.getValue()).isEqualTo(TEST_VALUE_TRUE);
	}

	private Optional<FeatureData> getFeatureDataFromTestClass(final ProductData product, final String classifcationAttributeCode)
	{
		assertThat(product.getClassifications()).isNotEmpty();

		ClassificationData classData = product.getClassifications().iterator().next();
		assertThat(classData.getCode()).isEqualTo(TEST_CLASS_CODE);

		return classData.getFeatures().stream()
				.filter(feature -> classifcationAttributeCode.equals(feature.getCode()))
				.findFirst();

	}
}
