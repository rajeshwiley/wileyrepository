package com.wiley.facades.ags.order.impl;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.subscriptionfacades.order.SubscriptionCartFacade;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Resource;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;


/**
 * Integration test for checking single subscription behavior.
 *
 * @author Aliaksei_Zlobich
 */
@IntegrationTest
public class AgsSingleSubscriptionCartFacadeIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{

	public static final String TEST_USER_CART = "test-user-cart";
	public static final String PRODUCT_AGS_LEGACY_24 = "ags_legacy_24";
	public static final String PRODUCT_AGS_LEGACY_1 = "ags_legacy_1";
	public static final String SIMPLE_PRODUCT = "simple_product";

	@Resource
	private UserService userService;

	@Resource
	private CommerceCartService commerceCartService;

	@Resource
	private CartService cartService;

	@Resource
	private BaseSiteService baseSiteService;

	@Resource(name = "agsSingleSubscriptionCartFacade")
	private SubscriptionCartFacade subscriptionCartFacade;

	@Before
	public void setUp() throws Exception
	{
		importCsv("/wileycore/test/subscription/testSubscriptions.impex", DEFAULT_ENCODING);
		importCsv("/wileyfacades/test/AgsSingleSubscriptionCartFacadeIntegrationTest/common.impex", DEFAULT_ENCODING);

		// Set up site and user
		baseSiteService.setCurrentBaseSite("ags", true);
		userService.setCurrentUser(userService.getUserForUID("test-user"));
	}

	@Test
	public void shouldReplaceExistingSubscription() throws CommerceCartModificationException
	{
		// Given
		final int quantityToAdd = 1;

		final CartModel cart = initSessionCartForCurrentUser(TEST_USER_CART);

		// When
		subscriptionCartFacade.addToCart(PRODUCT_AGS_LEGACY_24, quantityToAdd);
		final CartModificationData cartModificationData = subscriptionCartFacade.addToCart(PRODUCT_AGS_LEGACY_1, quantityToAdd);

		// Then
		assertNotNull(cartModificationData);
		assertEquals("Expected that product was added.", quantityToAdd, cartModificationData.getQuantityAdded());

		assertEquals("Expected only one subscription product in cart.", 1, cart.getEntries().size());
		doesProductExistInCart(PRODUCT_AGS_LEGACY_1, cart);
	}

	@Test
	public void shouldAddUsualProductWithSubscriptionProduct() throws CommerceCartModificationException, ImpExException
	{
		// Given
		importCsv(
				"/wileyfacades/test/AgsSingleSubscriptionCartFacadeIntegrationTest"
						+ "/shouldAddUsualProductWithSubscriptionProduct.impex", DEFAULT_ENCODING);

		final int quantityToAdd = 1;

		final CartModel cart = initSessionCartForCurrentUser(TEST_USER_CART);

		// When
		subscriptionCartFacade.addToCart(SIMPLE_PRODUCT, quantityToAdd);
		final CartModificationData cartModificationData = subscriptionCartFacade.addToCart(PRODUCT_AGS_LEGACY_1, quantityToAdd);

		// Then
		assertNotNull(cartModificationData);
		assertEquals("Expected that product was added.", quantityToAdd, cartModificationData.getQuantityAdded());
		assertEquals("Expected that subscription product with usual product exist in cart", 2, cart.getEntries().size());
		doesProductExistInCart(SIMPLE_PRODUCT, cart);
		doesProductExistInCart(PRODUCT_AGS_LEGACY_1, cart);
	}

	@Test
	public void shouldReplaceOnlySubscriptionProducts() throws ImpExException, CommerceCartModificationException
	{
		// Given
		importCsv(
				"/wileyfacades/test/AgsSingleSubscriptionCartFacadeIntegrationTest"
						+ "/shouldAddUsualProductWithSubscriptionProduct.impex", DEFAULT_ENCODING);

		final CartModel cart = initSessionCartForCurrentUser(TEST_USER_CART);

		// When
		subscriptionCartFacade.addToCart(SIMPLE_PRODUCT, 1);
		subscriptionCartFacade.addToCart(PRODUCT_AGS_LEGACY_1, 1);
		final CartModificationData cartModificationData = subscriptionCartFacade.addToCart(PRODUCT_AGS_LEGACY_24, 1);

		// Then
		assertNotNull(cartModificationData);
		assertEquals("Expected that product was added.", 1, cartModificationData.getQuantityAdded());
		assertEquals("Expected that subscription product with usual product exist in cart", 2, cart.getEntries().size());
		doesProductExistInCart(SIMPLE_PRODUCT, cart);
		doesProductExistInCart(PRODUCT_AGS_LEGACY_24, cart);
	}

	private void doesProductExistInCart(final String productCode, final CartModel cart)
	{
		assertTrue(String.format("Expected that product [%s] exists in cart.", productCode), cart.getEntries().stream().anyMatch(
				entry -> productCode.equals(entry.getProduct().getCode())));
	}

	private CartModel initSessionCartForCurrentUser(final String testUserCart)
	{
		final CartModel cart = commerceCartService.getCartForCodeAndUser(testUserCart, userService.getCurrentUser());
		cartService.setSessionCart(cart);
		return cart;
	}

}
