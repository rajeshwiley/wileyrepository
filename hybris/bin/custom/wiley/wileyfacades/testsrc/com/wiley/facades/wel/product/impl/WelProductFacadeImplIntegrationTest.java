package com.wiley.facades.wel.product.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.site.BaseSiteService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Locale;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.servicelayer.i18n.WileycomI18NService;
import com.wiley.facades.product.data.CompareTableLevelRowData;
import com.wiley.facades.product.data.WileyProductListCFAData;


/**
 * Integration test suite for {@link com.wiley.facades.wel.product.impl.WelProductFacadeImpl}
 *
 * Created by Raman_Hancharou on 1/21/2016.
 */
@IntegrationTest
public class WelProductFacadeImplIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{

	/**
	 * The constant CATALOG_ID.
	 */
	public static final String CATALOG_ID = "welProductCatalog";
	/**
	 * The constant CATALOG_VERSION.
	 */
	public static final String CATALOG_VERSION = "Online";

	private static final String CFA_CATEGORY = "WEL_CFA_CATEGORY";
	private static final String WEL_CFA_LEVEL_I = "WEL_CFA_LEVEL_I";
	private static final String WEL_CFA_LEVEL_II = "WEL_CFA_LEVEL_II";
	private static final String WEL_CFA_LEVEL_III = "WEL_CFA_LEVEL_III";
	private static final String WEL_CFA_VIRTUAL_CLASSROOM = "WEL_CFA_VIRTUAL_CLASSROOM";
	private static final String WEL_CFA_06_2016_SILVER_COURSE = "WEL_CFA_06_2016_SILVER_COURSE";
	private static final String WEL_CFA_06_2016_GOLD_COURSE = "WEL_CFA_06_2016_GOLD_COURSE";
	private static final String WEL_CFA_06_2016_PLATINUM_COURSE = "WEL_CFA_06_2016_PLATINUM_COURSE";
	private static final String LEVEL_PRODUCTS_LEVEL_I = "/cfa/products/level-i/";
	private static final String LEVEL_I = "Level I";
	private static final String CURRENT_RELEASE = "Current Release";
	private static final String PRE_ORDER_NEXT_RELEASE = "Pre-Order Next Release";

	/**
	 * The wel product facade.
	 */
	@Resource(name = "welProductFacade")
	WelProductFacadeImpl productFacade;
	@Resource
	private BaseSiteService baseSiteService;

	@Resource
	private I18NService i18nService;

	@Resource
	private WileycomI18NService wileycomI18NService;

	/**
	 * Sets up.
	 *
	 * @throws Exception
	 * 		the exception
	 */
	@Before
	public void setUp() throws Exception
	{
		//importCsv("/wileyfacades/test/sampledata/wel/catalog.impex", DEFAULT_ENCODING);
		//importCsv("/wileyfacades/test/sampledata/wel/categories.impex", DEFAULT_ENCODING);
		importCsv("/wileyfacades/test/sampledata/wel/categories-classifications.impex", DEFAULT_ENCODING);
		importCsv("/wileyfacades/test/sampledata/wel/products_en.impex", DEFAULT_ENCODING);
		importCsv("/wileyfacades/test/sampledata/wel/WileyProductFacadeIntegrationTestSet1/product-prices.impex",
				DEFAULT_ENCODING);
		i18nService.setCurrentLocale(Locale.ENGLISH);
		baseSiteService.setCurrentBaseSite("wel", false);
		wileycomI18NService.setDefaultCurrentCountry();
	}

	/**
	 * Test get product list cfa data with consistent data set.
	 * TEST IGNORED. It is unreliable can become red in any time without any changes at all
	 *
	 * @throws Exception
	 * 		the exception
	 */
	@Test
	@Ignore
	public void testGetProductListCFADataWithConsistentData() throws Exception
	{
		importCsv("/wileyfacades/test/sampledata/wel/WileyProductFacadeIntegrationTestSet1/products-classifications.impex",
				DEFAULT_ENCODING);
		importCsv("/wileyfacades/test/sampledata/wel/WileyProductFacadeIntegrationTestSet1/products-relations.impex",
				DEFAULT_ENCODING);
		WileyProductListCFAData wileyProductListCFAData = productFacade.getProductListCFAData(CFA_CATEGORY);

		//Grid Data
		assertEquals(3, wileyProductListCFAData.getGridData().size());
		assertEquals(1, wileyProductListCFAData.getGridData().get(WEL_CFA_LEVEL_I).getAddons().size());
		assertEquals(1, wileyProductListCFAData.getGridData().get(WEL_CFA_LEVEL_II).getAddons().size());
		assertEquals(1, wileyProductListCFAData.getGridData().get(WEL_CFA_LEVEL_III).getAddons().size());
		assertEquals(2, wileyProductListCFAData.getGridData().get(WEL_CFA_LEVEL_I).getSupplements().size());
		assertEquals(2, wileyProductListCFAData.getGridData().get(WEL_CFA_LEVEL_II).getSupplements().size());
		assertEquals(2, wileyProductListCFAData.getGridData().get(WEL_CFA_LEVEL_III).getSupplements().size());
		assertEquals("June 2016", wileyProductListCFAData.getGridData().get(WEL_CFA_LEVEL_I).getExamDate());

		//Compare Table
		assertEquals(3, wileyProductListCFAData.getCompareTableData().getHeader().size());
		assertEquals(3, wileyProductListCFAData.getCompareTableData().getLevels().size());
		assertEquals(11, wileyProductListCFAData.getCompareTableData().getCompareOptions().size());
		assertNull(wileyProductListCFAData.getCompareTableData().getCompareOptions().get(WEL_CFA_VIRTUAL_CLASSROOM)
				.getFeatureAvailability().get(WEL_CFA_06_2016_SILVER_COURSE));
		assertNotNull(wileyProductListCFAData.getCompareTableData().getCompareOptions().get(WEL_CFA_VIRTUAL_CLASSROOM)
				.getFeatureAvailability().get(WEL_CFA_06_2016_GOLD_COURSE));
		assertEquals(3, wileyProductListCFAData.getCompareTableData().getFooter().size());
		assertEquals(3, wileyProductListCFAData.getCompareTableData().getFooter().get(WEL_CFA_06_2016_GOLD_COURSE).size());
		assertEquals(LEVEL_PRODUCTS_LEVEL_I,
				wileyProductListCFAData.getCompareTableData().getFooter().get(WEL_CFA_06_2016_GOLD_COURSE).get(LEVEL_I));

		//Price Checking - base products
		CompareTableLevelRowData data = wileyProductListCFAData.getCompareTableData().getLevels().get(0);
		assertEquals(945, data.getPricesData()
				.get(WEL_CFA_06_2016_PLATINUM_COURSE).getValue().doubleValue(), 0);
		assertEquals(845, wileyProductListCFAData.getCompareTableData().getLevels().get(1).getPricesData()
				.get(WEL_CFA_06_2016_PLATINUM_COURSE).getValue().doubleValue(), 0);
		assertNull(wileyProductListCFAData.getCompareTableData().getLevels().get(2).getPricesData()
				.get(WEL_CFA_06_2016_PLATINUM_COURSE));

		assertEquals(945, wileyProductListCFAData.getCompareTableData().getLevels().get(0).getPricesData()
				.get(WEL_CFA_06_2016_GOLD_COURSE).getValue().doubleValue(), 0);
		assertEquals(900, wileyProductListCFAData.getCompareTableData().getLevels().get(1).getPricesData()
				.get(WEL_CFA_06_2016_GOLD_COURSE).getValue().doubleValue(), 0);
		assertEquals(700, wileyProductListCFAData.getCompareTableData().getLevels().get(2).getPricesData()
				.get(WEL_CFA_06_2016_GOLD_COURSE).getValue().doubleValue(), 0);

		//Price Checking - supplements and addons
		// TODO: looks like supplements order is not deterministic
		assertEquals(275, wileyProductListCFAData.getGridData().get(WEL_CFA_LEVEL_I).getSupplements().get(0).getPrice().getValue()
				.doubleValue(), 0);
		assertEquals(43, wileyProductListCFAData.getGridData().get(WEL_CFA_LEVEL_I).getSupplements().get(1).getPrice().getValue()
				.doubleValue(), 0);
		assertEquals(259, wileyProductListCFAData.getGridData().get(WEL_CFA_LEVEL_I).getAddons().get(0).getPrice().getValue()
				.doubleValue(), 0);
	}


	/**
	 * Test get product list cfa data when there are no addons.
	 *
	 * @throws Exception
	 * 		the exception
	 */
	@Test
	public void testGetProductListCFADataWithoutEmptyProductReferences() throws Exception
	{
		importCsv("/wileyfacades/test/sampledata/wel/WileyProductFacadeIntegrationTestSet2/products-classifications.impex",
				DEFAULT_ENCODING);
		WileyProductListCFAData wileyProductListCFAData = productFacade.getProductListCFAData(CFA_CATEGORY);

		//Grid Data
		assertEquals(3, wileyProductListCFAData.getGridData().size());
		assertEquals(0, wileyProductListCFAData.getGridData().get(WEL_CFA_LEVEL_I).getAddons().size());
		assertEquals(0, wileyProductListCFAData.getGridData().get(WEL_CFA_LEVEL_II).getAddons().size());
		assertEquals(0, wileyProductListCFAData.getGridData().get(WEL_CFA_LEVEL_III).getAddons().size());
		assertEquals(3, wileyProductListCFAData.getGridData().get(WEL_CFA_LEVEL_I).getSupplements().size());
		assertEquals(3, wileyProductListCFAData.getGridData().get(WEL_CFA_LEVEL_II).getSupplements().size());
		assertEquals(3, wileyProductListCFAData.getGridData().get(WEL_CFA_LEVEL_III).getSupplements().size());
		assertEquals("June 2016", wileyProductListCFAData.getGridData().get(WEL_CFA_LEVEL_I).getExamDate());
	}

	/**
	 * Test get product list cfa data with empty exam date for some products.
	 *
	 * @throws Exception
	 * 		the exception
	 */
	@Test
	public void testGetProductListCFADataWithEmptyExamDate() throws Exception
	{
		importCsv("/wileyfacades/test/sampledata/wel/WileyProductFacadeIntegrationTestSet3/products-classifications.impex",
				DEFAULT_ENCODING);
		importCsv("/wileyfacades/test/sampledata/wel/WileyProductFacadeIntegrationTestSet1/products-relations.impex",
				DEFAULT_ENCODING);
		WileyProductListCFAData wileyProductListCFAData = productFacade.getProductListCFAData(CFA_CATEGORY);

		//Grid Data
		assertEquals(3, wileyProductListCFAData.getGridData().size());
		assertEquals(1, wileyProductListCFAData.getGridData().get(WEL_CFA_LEVEL_I).getAddons().size());
		assertEquals(1, wileyProductListCFAData.getGridData().get(WEL_CFA_LEVEL_II).getAddons().size());
		assertEquals(1, wileyProductListCFAData.getGridData().get(WEL_CFA_LEVEL_III).getAddons().size());
		assertEquals(1, wileyProductListCFAData.getGridData().get(WEL_CFA_LEVEL_I).getSupplements().size());
		assertEquals(1, wileyProductListCFAData.getGridData().get(WEL_CFA_LEVEL_II).getSupplements().size());
		assertEquals(1, wileyProductListCFAData.getGridData().get(WEL_CFA_LEVEL_III).getSupplements().size());
		assertEquals("June 2016", wileyProductListCFAData.getGridData().get(WEL_CFA_LEVEL_I).getExamDate());

		//Compare Table
		assertEquals(2, wileyProductListCFAData.getCompareTableData().getHeader().size());
		assertNull(wileyProductListCFAData.getCompareTableData().getHeader().get(WEL_CFA_06_2016_GOLD_COURSE));
		assertEquals(6, wileyProductListCFAData.getCompareTableData().getLevels().size());
		assertEquals(11, wileyProductListCFAData.getCompareTableData().getCompareOptions().size());
		assertNull(wileyProductListCFAData.getCompareTableData().getCompareOptions().get(WEL_CFA_VIRTUAL_CLASSROOM)
				.getFeatureAvailability().get(WEL_CFA_06_2016_SILVER_COURSE));
		assertNotNull(wileyProductListCFAData.getCompareTableData().getCompareOptions().get(WEL_CFA_VIRTUAL_CLASSROOM)
				.getFeatureAvailability().get(WEL_CFA_06_2016_PLATINUM_COURSE));
		assertEquals(1, wileyProductListCFAData.getCompareTableData().getFooterProductData().size());
		assertEquals(2, wileyProductListCFAData.getCompareTableData().getFooterProductData().get(CURRENT_RELEASE).size());
	}

	@Test
	public void testGetProductListCFADataWithNextVersionProducts() throws Exception
	{
		importCsv("/wileyfacades/test/sampledata/wel/WileyProductFacadeIntegrationTestSet1/next-version-products.impex",
				DEFAULT_ENCODING);
		importCsv("/wileyfacades/test/sampledata/wel/WileyProductFacadeIntegrationTestSet3/products-classifications.impex",
				DEFAULT_ENCODING);
		WileyProductListCFAData wileyProductListCFAData = productFacade.getProductListCFAData(CFA_CATEGORY);

		//Compare Table
		assertEquals(2, wileyProductListCFAData.getCompareTableData().getHeader().size());
		assertNull(wileyProductListCFAData.getCompareTableData().getHeader().get(WEL_CFA_06_2016_GOLD_COURSE));
		assertTrue(wileyProductListCFAData.getCompareTableData().getHeader().containsKey(WEL_CFA_06_2016_SILVER_COURSE));
		assertTrue(wileyProductListCFAData.getCompareTableData().getHeader().containsKey(WEL_CFA_06_2016_PLATINUM_COURSE));

		assertEquals(6, wileyProductListCFAData.getCompareTableData().getLevels().size());
		assertTrue(wileyProductListCFAData.getCompareTableData().getLevels().stream().filter(
				level -> level.getRowTitle().contains(PRE_ORDER_NEXT_RELEASE)).findFirst().isPresent());
		assertTrue(wileyProductListCFAData.getCompareTableData().getLevels().stream().filter(
				level -> level.getRowTitle().contains(CURRENT_RELEASE)).findFirst().isPresent());

		assertEquals(11, wileyProductListCFAData.getCompareTableData().getCompareOptions().size());
		assertNull(wileyProductListCFAData.getCompareTableData().getCompareOptions().get(WEL_CFA_VIRTUAL_CLASSROOM)
				.getFeatureAvailability().get(WEL_CFA_06_2016_SILVER_COURSE));
		assertNotNull(wileyProductListCFAData.getCompareTableData().getCompareOptions().get(WEL_CFA_VIRTUAL_CLASSROOM)
				.getFeatureAvailability().get(WEL_CFA_06_2016_PLATINUM_COURSE));

		assertEquals(2, wileyProductListCFAData.getCompareTableData().getFooterProductData().size());
		assertEquals(2, wileyProductListCFAData.getCompareTableData().getFooterProductData().get(CURRENT_RELEASE).size());
		assertEquals(2, wileyProductListCFAData.getCompareTableData().getFooterProductData().get(PRE_ORDER_NEXT_RELEASE).size());
		assertEquals(2, wileyProductListCFAData.getCompareTableData().getFooterProductData().get(CURRENT_RELEASE).size());
	}
}
