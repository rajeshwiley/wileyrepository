package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.ImageFormatMapping;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ImageDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaFormatModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.media.MediaContainerService;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyPurchaseOptionProductModel;



/**
 * Created by Uladzimir_Barouski on 2/22/2017.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cProductGalleryImagesPopulatorUnitTest
{
	private static final String ZOOM_MEDIA_FORMAT = "zoom";
	private static final String ZOOM_MEDIA_FORMAT_QUALIFIER = "zoom";
	private static final String PRODUCT_MEDIA_FORMAT = "product";
	private static final String PRODUCT_MEDIA_FORMAT_QUALIFIER = "normal";
	private static final String THUMBNAIL_MEDIA_FORMAT = "thumbnail";
	private static final String THUMBNAIL_MEDIA_FORMAT_QUALIFIER = "thumbnail";
	private static final List<String> IMAGE_FORMATS = Arrays.asList(ZOOM_MEDIA_FORMAT, PRODUCT_MEDIA_FORMAT,
			THUMBNAIL_MEDIA_FORMAT);

	@InjectMocks
	private Wileyb2cProductGalleryImagesPopulator wileyb2cProductGalleryImagesPopulator;

	@Mock
	private MediaService mediaService;

	@Mock
	private MediaContainerService mediaContainerService;

	@Mock
	private MediaFormatModel zoomMediaFormat, productMediaFormat, thumbnailMediaFormat;

	@Mock
	private ImageFormatMapping imageFormatMapping;

	@Mock
	private MediaContainerModel externalImageMediaContainer, galleryImageMediaContainer;

	@Mock
	private Converter<MediaModel, ImageData> imageConverterMock;

	@Mock
	private MediaModel zoomMedia, productMedia, thumbnailMedia;
	@Mock
	private MediaModel externalZoomMedia, externalProductMedia, externalThumbnailMedia;

	@Mock
	private ImageData zoomDataMock, productDataMock, thumbnailDataMock;
	@Mock
	private ImageData externalZoomDataMock, externalProductDataMock, externalThumbnailDataMock;
	@Mock
	private WileyPurchaseOptionProductModel variantProduct;
	@Mock
	private ProductModel baseProduct;
	@Mock
	private ModelService modelService;

	@Before
	public void setUp() throws Exception
	{
		wileyb2cProductGalleryImagesPopulator.setImageFormats(IMAGE_FORMATS);

		when(imageFormatMapping.getMediaFormatQualifierForImageFormat(ZOOM_MEDIA_FORMAT)).thenReturn(
				ZOOM_MEDIA_FORMAT_QUALIFIER);
		when(imageFormatMapping.getMediaFormatQualifierForImageFormat(PRODUCT_MEDIA_FORMAT)).thenReturn(
				PRODUCT_MEDIA_FORMAT_QUALIFIER);
		when(imageFormatMapping.getMediaFormatQualifierForImageFormat(THUMBNAIL_MEDIA_FORMAT)).thenReturn(
				THUMBNAIL_MEDIA_FORMAT_QUALIFIER);
		wileyb2cProductGalleryImagesPopulator.setImageFormatMapping(imageFormatMapping);

		when(mediaService.getFormat(ZOOM_MEDIA_FORMAT_QUALIFIER)).thenReturn(zoomMediaFormat);
		when(mediaService.getFormat(PRODUCT_MEDIA_FORMAT_QUALIFIER)).thenReturn(productMediaFormat);
		when(mediaService.getFormat(THUMBNAIL_MEDIA_FORMAT_QUALIFIER)).thenReturn(thumbnailMediaFormat);

		when(mediaContainerService.getMediaForFormat(externalImageMediaContainer, zoomMediaFormat)).thenReturn(externalZoomMedia);
		when(mediaContainerService.getMediaForFormat(externalImageMediaContainer, productMediaFormat)).thenReturn(
				externalProductMedia);
		when(mediaContainerService.getMediaForFormat(externalImageMediaContainer, thumbnailMediaFormat)).thenReturn(
				externalThumbnailMedia);


		when(imageConverterMock.convert(externalZoomMedia)).thenReturn(externalZoomDataMock);
		when(imageConverterMock.convert(externalProductMedia)).thenReturn(externalProductDataMock);
		when(imageConverterMock.convert(externalThumbnailMedia)).thenReturn(externalThumbnailDataMock);

		when(externalImageMediaContainer.getMedias()).thenReturn(
				Arrays.asList(externalZoomMedia, externalProductMedia, externalThumbnailMedia));
		when(variantProduct.getBaseProduct()).thenReturn(baseProduct);
		when(modelService.getAttributeValue(baseProduct, ProductModel.EXTERNALIMAGE)).thenReturn(externalImageMediaContainer);
	}

	@Test
	public void populateGalleryImages() throws Exception
	{
		//Given
		when(modelService.getAttributeValue(variantProduct, ProductModel.GALLERYIMAGES)).thenReturn(
				Arrays.asList(galleryImageMediaContainer));

		when(mediaContainerService.getMediaForFormat(galleryImageMediaContainer, zoomMediaFormat)).thenReturn(zoomMedia);
		when(mediaContainerService.getMediaForFormat(galleryImageMediaContainer, productMediaFormat)).thenReturn(productMedia);
		when(mediaContainerService.getMediaForFormat(galleryImageMediaContainer, thumbnailMediaFormat)).thenReturn(
				thumbnailMedia);


		when(imageConverterMock.convert(zoomMedia)).thenReturn(zoomDataMock);
		when(imageConverterMock.convert(productMedia)).thenReturn(productDataMock);
		when(imageConverterMock.convert(thumbnailMedia)).thenReturn(thumbnailDataMock);

		when(galleryImageMediaContainer.getMedias()).thenReturn(Arrays.asList(zoomMedia, productMedia, thumbnailMedia));

		when(variantProduct.getGalleryImages()).thenReturn(Arrays.asList(galleryImageMediaContainer));
		ProductData productData = new ProductData();
		//When
		wileyb2cProductGalleryImagesPopulator.populate(variantProduct, productData);
		//Then
		List<ImageData> galleryImages = productData.getImages().stream().collect(Collectors.toList());
		assertNotNull(galleryImages);
		assertEquals(3, galleryImages.size());

		assertEquals(zoomDataMock, galleryImages.get(0));
		verify(zoomDataMock).setFormat(ZOOM_MEDIA_FORMAT);
		verify(zoomDataMock).setImageType(ImageDataType.GALLERY);
		verify(externalZoomDataMock, never()).setFormat(ZOOM_MEDIA_FORMAT);
		verify(externalZoomDataMock, never()).setImageType(ImageDataType.GALLERY);

		assertEquals(productDataMock, galleryImages.get(1));
		verify(productDataMock).setFormat(PRODUCT_MEDIA_FORMAT);
		verify(productDataMock).setImageType(ImageDataType.GALLERY);
		verify(externalProductDataMock, never()).setFormat(PRODUCT_MEDIA_FORMAT);
		verify(externalProductDataMock, never()).setImageType(ImageDataType.GALLERY);

		assertEquals(thumbnailDataMock, galleryImages.get(2));
		verify(thumbnailDataMock).setFormat(THUMBNAIL_MEDIA_FORMAT);
		verify(thumbnailDataMock).setImageType(ImageDataType.GALLERY);
		verify(externalThumbnailDataMock, never()).setFormat(THUMBNAIL_MEDIA_FORMAT);
		verify(externalThumbnailDataMock, never()).setImageType(ImageDataType.GALLERY);
	}

	@Test
	public void populateExternalImage() throws Exception
	{
		//Given
		when(variantProduct.getGalleryImages()).thenReturn(Collections.emptyList());
		ProductData productData = new ProductData();
		//When
		wileyb2cProductGalleryImagesPopulator.populate(variantProduct, productData);
		//Then
		List<ImageData> externalImages = productData.getImages().stream().collect(Collectors.toList());
		assertNotNull(externalImages);
		assertEquals(3, externalImages.size());

		assertEquals(externalZoomDataMock, externalImages.get(0));
		verify(zoomDataMock, never()).setFormat(ZOOM_MEDIA_FORMAT);
		verify(zoomDataMock, never()).setImageType(ImageDataType.GALLERY);
		verify(externalZoomDataMock).setFormat(ZOOM_MEDIA_FORMAT);
		verify(externalZoomDataMock).setImageType(ImageDataType.GALLERY);

		assertEquals(externalProductDataMock, externalImages.get(1));
		verify(productDataMock, never()).setFormat(PRODUCT_MEDIA_FORMAT);
		verify(productDataMock, never()).setImageType(ImageDataType.GALLERY);
		verify(externalProductDataMock).setFormat(PRODUCT_MEDIA_FORMAT);
		verify(externalProductDataMock).setImageType(ImageDataType.GALLERY);

		assertEquals(externalThumbnailDataMock, externalImages.get(2));
		verify(thumbnailDataMock, never()).setFormat(THUMBNAIL_MEDIA_FORMAT);
		verify(thumbnailDataMock, never()).setImageType(ImageDataType.GALLERY);
		verify(externalThumbnailDataMock).setFormat(THUMBNAIL_MEDIA_FORMAT);
		verify(externalThumbnailDataMock).setImageType(ImageDataType.GALLERY);
	}
}