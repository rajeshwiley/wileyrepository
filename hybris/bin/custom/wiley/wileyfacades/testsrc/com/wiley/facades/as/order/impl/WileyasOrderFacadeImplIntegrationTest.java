package com.wiley.facades.as.order.impl;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.facades.as.order.WileyasOrderFacade;
import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.site.BaseSiteService;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Resource;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;

@IntegrationTest
public class WileyasOrderFacadeImplIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
    private static final String BASE_SITE = "asSite";
    private static final String GUID = "asOldTestOrder";

    @Resource
    private BaseSiteService baseSiteService;

    @Resource
    private WileyasOrderFacade wileyasOrderFacade;

    @Before
    public void setUp() throws Exception
    {
        importCsv("/wileyfacades/test/WileyasOrderFacadeImplIntegrationTest/order.impex", DEFAULT_ENCODING);
        baseSiteService.setCurrentBaseSite(BASE_SITE, true);
    }

    @Test
    public void shouldGetOldOrder()
    {
        // when
        OrderData order = wileyasOrderFacade.getOrderDetailsForGUID(GUID);

        // then
        assertNotNull(order);
        assertEquals(GUID, order.getGuid());
    }
}