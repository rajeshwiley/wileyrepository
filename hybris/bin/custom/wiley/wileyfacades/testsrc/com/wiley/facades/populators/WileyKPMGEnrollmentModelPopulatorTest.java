package com.wiley.facades.populators;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.enums.KpmgFunction;
import com.wiley.core.model.KpmgEnrollmentModel;
import com.wiley.core.model.KpmgOfficeModel;
import com.wiley.core.partner.WileyKPMGEnrollmentService;
import com.wiley.facades.partner.kpmg.KPMGEnrollmentData;
import com.wiley.facades.partner.kpmg.KPMGFunctionData;
import com.wiley.facades.partner.kpmg.KPMGOfficeData;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyKPMGEnrollmentModelPopulatorTest
{
	private static final String REGISTRATION_ID = "redId";
	private static final String EMPLOYEE_ID = "34343";
	private static final String FIRST_NAME = "Firstname";
	private static final String LAST_NAME = "Lastname";
	private static final String EMAIL = "test@test.com";
	private static final String OFFICE_CODE = "495457";

	@InjectMocks
	private WileyKPMGEnrollmentModelPopulator testedInstance = new WileyKPMGEnrollmentModelPopulator();

	@Mock
	private WileyKPMGEnrollmentService wileyKpmgEnrollmentService;
	@Mock
	private KpmgEnrollmentModel enrollmentModel;
	@Mock
	private KpmgOfficeModel officeModel;

	private KPMGEnrollmentData enrollmentData = new KPMGEnrollmentData();

	@Test
	public void shouldPopulateRegistrationId()
	{
		//Given
		enrollmentData.setRegistrationId(REGISTRATION_ID);
		//When
		testedInstance.populate(enrollmentData, enrollmentModel);
		//Then
		verify(enrollmentModel).setRegistrationId(REGISTRATION_ID);
	}

	@Test
	public void shouldPopulateEmployeeId()
	{
		//Given
		enrollmentData.setEmployeeId(EMPLOYEE_ID);
		//When
		testedInstance.populate(enrollmentData, enrollmentModel);
		//Then
		verify(enrollmentModel).setEmployeeId(EMPLOYEE_ID);
	}

	@Test
	public void shouldPopulateFirstName()
	{
		//Given
		enrollmentData.setFirstName(FIRST_NAME);
		//When
		testedInstance.populate(enrollmentData, enrollmentModel);
		//Then
		verify(enrollmentModel).setFirstName(FIRST_NAME);
	}

	@Test
	public void shouldPopulateLastName()
	{
		//Given
		enrollmentData.setLastName(LAST_NAME);
		//When
		testedInstance.populate(enrollmentData, enrollmentModel);
		//Then
		verify(enrollmentModel).setLastName(LAST_NAME);
	}

	@Test
	public void shouldPopulateEmail()
	{
		//Given
		enrollmentData.setEmail(EMAIL);
		//When
		testedInstance.populate(enrollmentData, enrollmentModel);
		//Then
		verify(enrollmentModel).setEmail(EMAIL);
	}

	@Test
	public void shouldPopulateFunction()
	{
		//Given
		KPMGFunctionData functionData = new KPMGFunctionData();
		functionData.setCode(KpmgFunction.TAX.toString());
		enrollmentData.setFunction(functionData);
		//When
		testedInstance.populate(enrollmentData, enrollmentModel);
		//Then
		verify(enrollmentModel).setFunction(KpmgFunction.TAX);
	}

	@Test
	public void shouldNotPopulateFunctionWhenFunctionDataIsNull()
	{
		//Given
		enrollmentData.setFunction(null);
		//When
		testedInstance.populate(enrollmentData, enrollmentModel);
		//Then
		verify(enrollmentModel, never()).setFunction(anyObject());
	}

	@Test
	public void shouldPopulateOffice()
	{
		//Given
		KPMGOfficeData officeData = new KPMGOfficeData();
		officeData.setCode(OFFICE_CODE);
		enrollmentData.setOffice(officeData);
		when(wileyKpmgEnrollmentService.getOfficeByCode(OFFICE_CODE)).thenReturn(officeModel);

		//When
		testedInstance.populate(enrollmentData, enrollmentModel);
		//Then
		verify(enrollmentModel).setOffice(officeModel);
	}

	@Test
	public void shouldNotPopulateOfficeWhenOfficeDataIsNull()
	{
		//Given
		enrollmentData.setOffice(null);
		when(wileyKpmgEnrollmentService.getOfficeByCode(OFFICE_CODE)).thenReturn(officeModel);
		//When
		testedInstance.populate(enrollmentData, enrollmentModel);
		//Then
		verify(wileyKpmgEnrollmentService, never()).getOfficeByCode(anyString());
		verify(enrollmentModel, never()).setOffice(anyObject());
	}

	@Test
	public void shouldNotPopulateOfficeWhenOfficeCodeIsNull()
	{
		//Given
		KPMGOfficeData officeData = new KPMGOfficeData();
		officeData.setCode(null);
		enrollmentData.setOffice(officeData);
		//When
		testedInstance.populate(enrollmentData, enrollmentModel);
		//Then
		verify(wileyKpmgEnrollmentService, never()).getOfficeByCode(anyString());
		verify(enrollmentModel, never()).setOffice(anyObject());
	}

}