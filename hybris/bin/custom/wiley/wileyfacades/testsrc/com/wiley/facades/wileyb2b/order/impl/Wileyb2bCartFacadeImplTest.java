package com.wiley.facades.wileyb2b.order.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.DiscountValue;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyExternalDiscountModel;
import com.wiley.core.wileyb2b.order.impl.Wileyb2bCommerceCartCalculationStrategy;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2bCartFacadeImplTest
{
	private static final String EXTERNAL_DISCOUNT_GUID = "8512fd52-71ed-4755-94a3-94f2ef20772b";
	@InjectMocks
	private Wileyb2bCartFacadeImpl wileyb2bCartFacade;

	@Mock
	private CartService cartServiceMock;

	@Mock
	private CartModel cartModelMock;

	@Mock
	private Wileyb2bCommerceCartCalculationStrategy wileyb2bCommerceCartCalculationStrategyMock;

	@Mock
	private ModelService modelServiceMock;

	@Captor
	private ArgumentCaptor<Set<String>> voucherCodesCaptor;

	@Captor
	private ArgumentCaptor<CommerceCartParameter> commerceCartParameterCaptor;

	@Mock
	private ProductService productServiceMock;
	@Mock
	private ProductModel productMock;
	@Mock
	private UserService userServiceMock;
	@Mock
	private B2BCustomerModel userMock;
	@Mock
	private B2BUnitModel unitMock;
	@Mock
	private CommerceCartService commerceCartServiceMock;
	@Mock
	private CommerceCartModification commerceCartModificationMock;
	@Mock
	private Converter<CommerceCartModification, CartModificationData> cartModificationConverterMock;
	@Mock
	private WileyExternalDiscountModel externalDiscount;

	@Before
	public void setup()
	{
		final DiscountValue discountValue = mock(DiscountValue.class);
		when(cartServiceMock.getSessionCart()).thenReturn(cartModelMock);
		when(cartModelMock.getExternalDiscounts()).thenReturn(Arrays.asList(externalDiscount));
		when(externalDiscount.getGuid()).thenReturn(EXTERNAL_DISCOUNT_GUID);
	}
  // TODO rewrite test when implement voucher codes for b2b site
	/*
	 * @Test public void shouldSetVoucherCodeInCart() {
	 * when(cartServiceMock.hasSessionCart()).thenReturn(true);
	 * 
	 * wileyb2bCartFacade.setVoucherCodeInCart("third");
	 * 
	 * InOrder inOrder = inOrder(modelServiceMock, cartModelMock,
	 * wileyb2bCommerceCartCalculationStrategyMock);
	 * inOrder.verify(modelServiceMock).refresh(cartModelMock);
	 * //inOrder.verify(cartModelMock).setExternalDiscountCodes(voucherCodesCaptor
	 * .capture()); inOrder.verify(modelServiceMock).save(cartModelMock);
	 * inOrder.verify(wileyb2bCommerceCartCalculationStrategyMock).recalculateCart
	 * (commerceCartParameterCaptor.capture());
	 * 
	 * assertThat(commerceCartParameterCaptor.getValue(), allOf(
	 * hasProperty("cart", is(sameInstance(cartModelMock))),
	 * hasProperty("enableHooks", is(equalTo(true))) ));
	 * assertEquals(voucherCodesCaptor.getValue(), ImmutableSet.of("third")); }
	 */

	@Test
	public void shouldNotCalculateCartWithoutSessionCartInSetVoucherCodeInCart()
	{
		when(cartServiceMock.hasSessionCart()).thenReturn(false);

		wileyb2bCartFacade.setVoucherCodeInCart("example");

		verify(wileyb2bCommerceCartCalculationStrategyMock, never()).recalculateCart(isA(CommerceCartParameter.class));
	}

  //TODO rewrite test when implement voucher codes for b2b site
	/*@Test
	public void shouldRemoveVoucherCodeFromCart()
	{
		when(cartServiceMock.hasSessionCart()).thenReturn(true);

		wileyb2bCartFacade.removeVoucherCodeFromCart("first");

		final InOrder inOrder = inOrder(modelServiceMock, cartModelMock, wileyb2bCommerceCartCalculationStrategyMock);
		inOrder.verify(modelServiceMock).refresh(cartModelMock);
		// inOrder.verify(cartModelMock).setExternalDiscountCodes(voucherCodesCaptor.capture());
		inOrder.verify(modelServiceMock).save(cartModelMock);
		inOrder.verify(wileyb2bCommerceCartCalculationStrategyMock).recalculateCart(commerceCartParameterCaptor.capture());

		assertThat(commerceCartParameterCaptor.getValue(),
				allOf(hasProperty("cart", is(sameInstance(cartModelMock))), hasProperty("enableHooks", is(equalTo(true)))));
		assertEquals(voucherCodesCaptor.getValue(), ImmutableSet.of("second"));
	}*/

	@Test
	public void shouldNotCalculateCartWithoutSessionCartInRemoveVoucherCodeFromCart()
	{
		when(cartServiceMock.hasSessionCart()).thenReturn(false);

		wileyb2bCartFacade.removeVoucherCodeFromCart("example");

		verify(wileyb2bCommerceCartCalculationStrategyMock, never()).recalculateCart(isA(CommerceCartParameter.class));
	}

  //TODO rewrite test when implement voucher codes for b2b site
	/*@Test
	public void shouldNotRemoveNonExistentElementFromTheSet()
	{
		when(cartServiceMock.hasSessionCart()).thenReturn(true);
		wileyb2bCartFacade.removeVoucherCodeFromCart("example");

		final InOrder inOrder = inOrder(modelServiceMock, cartModelMock, wileyb2bCommerceCartCalculationStrategyMock);
		inOrder.verify(modelServiceMock).refresh(cartModelMock);
		// inOrder.verify(cartModelMock).setExternalDiscountCodes(voucherCodesCaptor.capture());
		inOrder.verify(modelServiceMock).save(cartModelMock);
		inOrder.verify(wileyb2bCommerceCartCalculationStrategyMock).recalculateCart(commerceCartParameterCaptor.capture());

		assertThat(commerceCartParameterCaptor.getValue(),
				allOf(hasProperty("cart", is(sameInstance(cartModelMock))), hasProperty("enableHooks", is(equalTo(true)))));
		assertEquals(voucherCodesCaptor.getValue(), ImmutableSet.of("first", "second"));
	}*/

	@Test
	public void testShouldSetb2bUnit() throws Exception
	{
		// given
		wileyb2bCartFacade.setCartModificationConverter(cartModificationConverterMock);
		when(productServiceMock.getProductForCode("code")).thenReturn(productMock);
		when(cartServiceMock.getSessionCart()).thenReturn(cartModelMock);
		when(userServiceMock.getCurrentUser()).thenReturn(userMock);
		when(userMock.getDefaultB2BUnit()).thenReturn(unitMock);
		when(commerceCartServiceMock.addToCart(Mockito.any())).thenReturn(commerceCartModificationMock);
		when(cartModificationConverterMock.convert(commerceCartModificationMock)).thenReturn(new CartModificationData());

		// when
		wileyb2bCartFacade.addToCart("code", 1);

		// then
		verify(cartModelMock).setUnit(unitMock);
	}
}
