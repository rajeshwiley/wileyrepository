package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.PriceData;
import static org.junit.Assert.assertNull;

import java.math.BigDecimal;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.facades.wileyb2c.product.data.OfferJsonLdDto;

import static junit.framework.Assert.assertEquals;


/**
 * Unit tests for Wileyb2cOfferJsonLdPopulator
 *
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cOfferJsonLdPopulatorUnitTest
{
	private static final String TYPE_VALUE = "Offer";
	private static final String CURRENCY_ISO = "USD";
	private static final BigDecimal PRICE_VALUE = BigDecimal.TEN;

	@InjectMocks
	private Wileyb2cOfferJsonLdPopulator populator;

	@Test
	public void populateFullTest() throws Exception
	{
		final PriceData source = new PriceData();
		final OfferJsonLdDto target = new OfferJsonLdDto();
		final String value = PRICE_VALUE.toString();

		source.setCurrencyIso(CURRENCY_ISO);
		source.setValue(PRICE_VALUE);

		populator.populate(source, target);

		assertEquals(value, target.getPrice());
		assertEquals(CURRENCY_ISO, target.getPriceCurrency());
		assertEquals(TYPE_VALUE, target.getType());
	}

	@Test
	public void populateEmptyTest() throws Exception
	{
		final PriceData source = new PriceData();

		source.setCurrencyIso(StringUtils.EMPTY);

		populateEmptyAndNullTest(source);
	}

	@Test
	public void populateNullTest() throws Exception
	{
		final PriceData source = new PriceData();

		source.setCurrencyIso(null);
		source.setValue(null);
	}

	private void populateEmptyAndNullTest(final PriceData source) throws Exception
	{
		final OfferJsonLdDto target = new OfferJsonLdDto();
		source.setValue(null);

		populator.populate(source, target);

		assertNull(target.getPrice());
		assertNull(target.getPriceCurrency());
		assertEquals(TYPE_VALUE, target.getType());
	}
}
