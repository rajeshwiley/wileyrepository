package com.wiley.facades.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.i18n.I18NService;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;

import com.wiley.core.enums.WileyProductLifecycleEnum;
import com.wiley.core.wel.preorder.service.WelPreOrderService;
import com.wiley.core.wel.preorder.strategy.ProductLifecycleStatusStrategy;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WelPreOrderProductNodePopulatorUnitTest
{
	private static final String TEST_ACTIVE_PRODUCT = "TEST_ACTIVE_PRODUCT";
	private static final String TEST_PRE_ORDER_PRODUCT = "TEST_PRE_ORDER_PRODUCT";
	private static final String SAMPLE_DESCRIPTION = "SAMPLE_DESCRIPTION";
	private static final String REFERENCE_DESCRIPTION = "referenceDescription";

	@InjectMocks
	private WelPreOrderProductNodePopulator welPreOrderProductNodePopulator = new WelPreOrderProductNodePopulator();

	@Mock
	private I18NService i18NService;

	@Mock
	private MessageSource messageSource;

	@Mock
	private ProductModel currentProductModelMock;

	@Mock
	private ProductData currentProductDataMock;

	@Mock
	private ProductModel targetProductModelMock;

	@Mock
	private ProductReferenceModel productReferenceModelMock;

	@Mock
	private VariantProductUrlPopulator variantProductUrlPopulator;

	@Mock
	private WelPreOrderService welPreOrderService;

	@Mock
	private ProductLifecycleStatusStrategy welProductLifecycleStatusStrategy;

	@Mock
	private WelProductLifeCycleStatusPopulator productLifeCycleStatusPopulator;

	private List<ProductData> testNodes = new ArrayList<>();

	@Before
	public void setUp()
	{
		when(productReferenceModelMock.getReferenceType()).thenReturn(ProductReferenceTypeEnum.NEXT_VERSION);
		when(productReferenceModelMock.getTarget()).thenReturn(targetProductModelMock);
		when(currentProductDataMock.getPreOrderNodes()).thenReturn(new ArrayList<>());
		doAnswer((ans) -> {
			final Object[] arguments = ans.getArguments();
			final WileyProductLifecycleEnum lifecycleStatus = ((ProductModel) arguments[0]).getLifecycleStatus();
			((ProductData) arguments[1]).setLifecycleStatus(lifecycleStatus);
			return null;
		}).when(
				productLifeCycleStatusPopulator).populate(any(ProductModel.class), any(ProductData.class));
	}


	@Test
	public void shouldNotCreatePreOrderNodesIfProductIsNotActiveAndNotPreOrder()
	{
		when(currentProductDataMock.getPreOrderNodes()).thenReturn(null);
		when(currentProductModelMock.getLifecycleStatus()).thenReturn(WileyProductLifecycleEnum.DISCONTINUED);
		when(currentProductDataMock.getPreOrderNodes()).thenReturn(null);
		when(welProductLifecycleStatusStrategy.getLifecycleStatusForProduct(currentProductModelMock)).thenReturn(null);
		// When
		welPreOrderProductNodePopulator.populate(currentProductModelMock, currentProductDataMock);
		// Then
		assertNull(currentProductDataMock.getPreOrderNodes());
	}

	@Test
	public void shouldCreatePreOrderNodesIfComeProductIsActive()
	{
		setUpIfComeProductIsActive();
		// When
		welPreOrderProductNodePopulator.populate(currentProductModelMock, currentProductDataMock);
		// Then
		final List<ProductData> nodes = currentProductDataMock.getPreOrderNodes();
		assertTrue(CollectionUtils.isNotEmpty(nodes));
		final Set<String> nodeCodes = nodes.stream().map(ProductData::getCode).collect(Collectors.toSet());
		assertTrue(nodeCodes.contains(TEST_PRE_ORDER_PRODUCT));
		assertTrue(nodeCodes.contains(TEST_ACTIVE_PRODUCT));
	}

	@Test
	public void shouldCreatePreOrderNodesIfComeProductIsPreOrder()
	{
		setUpIfComeProductIsPreOrder();
		// When
		welPreOrderProductNodePopulator.populate(currentProductModelMock, currentProductDataMock);
		// Then
		final List<ProductData> nodes = currentProductDataMock.getPreOrderNodes();
		assertTrue(CollectionUtils.isNotEmpty(nodes));
		final Set<String> nodeCodes = nodes.stream().map(ProductData::getCode).collect(Collectors.toSet());
		assertTrue(nodeCodes.contains(TEST_PRE_ORDER_PRODUCT));
		assertTrue(nodeCodes.contains(TEST_ACTIVE_PRODUCT));
	}

	@Test
	public void shouldHavePreOrderNodeWithReferenceDescriptionOnCurrentActiveProduct()
	{
		setUpIfComeProductIsActive();

		when(productReferenceModelMock.getDescription()).thenReturn(REFERENCE_DESCRIPTION);

		welPreOrderProductNodePopulator.populate(currentProductModelMock, currentProductDataMock);

		assertTrue(currentProductDataMock.getPreOrderNodes().stream()
				.anyMatch(productData -> productData.getLifecycleStatus() == WileyProductLifecycleEnum.PRE_ORDER));
		assertTrue(currentProductDataMock.getPreOrderNodes().stream().anyMatch(
				productData -> productData.getLifecycleStatus() == WileyProductLifecycleEnum.PRE_ORDER && REFERENCE_DESCRIPTION
						.equals(productData.getPreOrderText())));
		assertTrue(currentProductDataMock.getPreOrderNodes().stream()
				.anyMatch(productData -> productData.getLifecycleStatus() == WileyProductLifecycleEnum.ACTIVE));
		assertTrue(currentProductDataMock.getPreOrderNodes().stream().anyMatch(
				productData -> productData.getLifecycleStatus() == WileyProductLifecycleEnum.ACTIVE && StringUtils
						.isEmpty(productData.getPreOrderText())));
	}

	@Test
	public void shouldHavePreOrderNodeWithReferenceDescriptionOnCurrentPreOrderProduct()
	{
		setUpIfComeProductIsPreOrder();

		when(productReferenceModelMock.getDescription()).thenReturn(REFERENCE_DESCRIPTION);

		welPreOrderProductNodePopulator.populate(currentProductModelMock, currentProductDataMock);

		assertTrue(currentProductDataMock.getPreOrderNodes().stream()
				.anyMatch(productData -> productData.getLifecycleStatus() == WileyProductLifecycleEnum.PRE_ORDER));
		assertTrue(currentProductDataMock.getPreOrderNodes().stream().anyMatch(
				productData -> productData.getLifecycleStatus() == WileyProductLifecycleEnum.PRE_ORDER && REFERENCE_DESCRIPTION
						.equals(productData.getPreOrderText())));
		assertTrue(currentProductDataMock.getPreOrderNodes().stream()
				.anyMatch(productData -> productData.getLifecycleStatus() == WileyProductLifecycleEnum.ACTIVE));
		assertTrue(currentProductDataMock.getPreOrderNodes().stream().anyMatch(
				productData -> productData.getLifecycleStatus() == WileyProductLifecycleEnum.ACTIVE && StringUtils
						.isEmpty(productData.getPreOrderText())));
	}

	private void setUpIfComeProductIsActive()
	{
		doReturn(WileyProductLifecycleEnum.ACTIVE).when(welProductLifecycleStatusStrategy).getLifecycleStatusForProduct(
				currentProductModelMock);
		doReturn(Optional.of(currentProductModelMock)).when(welPreOrderService).getActiveProductForPreOrderProduct(
				targetProductModelMock);

		when(currentProductModelMock.getLifecycleStatus()).thenReturn(WileyProductLifecycleEnum.ACTIVE);
		when(currentProductModelMock.getCode()).thenReturn(TEST_ACTIVE_PRODUCT);
		when(currentProductModelMock.getDescription()).thenReturn(SAMPLE_DESCRIPTION);
		when(currentProductModelMock.getProductReferences()).thenReturn(Collections.singletonList(productReferenceModelMock));

		when(targetProductModelMock.getLifecycleStatus()).thenReturn(WileyProductLifecycleEnum.PRE_ORDER);
		when(targetProductModelMock.getCode()).thenReturn(TEST_PRE_ORDER_PRODUCT);
		when(targetProductModelMock.getDescription()).thenReturn(SAMPLE_DESCRIPTION);
	}

	private void setUpIfComeProductIsPreOrder()
	{
		doReturn(WileyProductLifecycleEnum.PRE_ORDER).when(welProductLifecycleStatusStrategy).getLifecycleStatusForProduct(
				currentProductModelMock);
		doReturn(Optional.of(targetProductModelMock)).when(welPreOrderService).getActiveProductForPreOrderProduct(
				currentProductModelMock);

		when(currentProductModelMock.getLifecycleStatus()).thenReturn(WileyProductLifecycleEnum.PRE_ORDER);
		when(currentProductModelMock.getCode()).thenReturn(TEST_PRE_ORDER_PRODUCT);
		when(currentProductModelMock.getDescription()).thenReturn(SAMPLE_DESCRIPTION);
		when(currentProductDataMock.getPreOrderNodes()).thenReturn(new ArrayList<>());

		when(targetProductModelMock.getCode()).thenReturn(TEST_ACTIVE_PRODUCT);
		when(targetProductModelMock.getDescription()).thenReturn(SAMPLE_DESCRIPTION);
		when(targetProductModelMock.getLifecycleStatus()).thenReturn(WileyProductLifecycleEnum.ACTIVE);
		when(targetProductModelMock.getProductReferences()).thenReturn(Collections.singletonList(productReferenceModelMock));
	}
}
