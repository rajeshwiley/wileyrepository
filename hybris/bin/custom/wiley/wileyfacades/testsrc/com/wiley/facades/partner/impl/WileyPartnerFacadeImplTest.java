package com.wiley.facades.partner.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.user.data.UserGroupData;
import de.hybris.platform.europe1.enums.UserDiscountGroup;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.model.WileyPartnerCompanyModel;
import com.wiley.core.partner.WileyPartnerService;
import com.wiley.facades.partner.WileyPartnerCompanyData;

import static junit.framework.Assert.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyPartnerFacadeImplTest
{
	private static final String PARTNER_ID = "partnerId";
	private static final String PARTNER_ID_2 = "partnerId2";
	private static final String PARTNER_NAME_1 = "partner1";
	private static final String PARTNER_NAME_2 = "partner2";
	private static final String PARTNER_CATEGORY_CODE = "partnerCategoryCode";
	private static final String CATEGORY_CODE = "testCategoryCode";

	@InjectMocks
	private final WileyPartnerFacadeImpl testedInstance = new WileyPartnerFacadeImpl();

	@Mock
	private WileyPartnerService wileyPartnerService;
	@Mock
	private WileyPartnerCompanyModel partnerCompany;
	@Mock
	private WileyPartnerCompanyModel partnerCompany2;
	@Mock
	private Converter<WileyPartnerCompanyModel, WileyPartnerCompanyData> wileyPartnerConverter;
	@Mock
	private UserGroupData userGroupData;

	private WileyPartnerCompanyData partnerCompanyData = new WileyPartnerCompanyData();
	private CategoryData partnerCategoryData = new CategoryData();

	@Test
	public void shouldReturnWileyPartnerCompany()
	{
		when(wileyPartnerService.getPartnerById(PARTNER_ID)).thenReturn(partnerCompany);
		when(wileyPartnerConverter.convert(partnerCompany)).thenReturn(partnerCompanyData);

		assertEquals(partnerCompanyData, testedInstance.getWileyPartnerCompany(PARTNER_ID));
	}

	@Test
	public void shouldReturnTrueIfCategoryBelongsToPartner()
	{
		partnerCompanyData.setPartnerCategories(Arrays.asList(partnerCategoryData));
		partnerCategoryData.setCode(PARTNER_CATEGORY_CODE);

		assertTrue(testedInstance.partnerHasCategory(partnerCompanyData, PARTNER_CATEGORY_CODE));
	}

	@Test
	public void shouldReturnFalseIfCategoryDoesNotBelongToPartner()
	{
		partnerCompanyData.setPartnerCategories(Arrays.asList(partnerCategoryData));

		assertFalse(testedInstance.partnerHasCategory(partnerCompanyData, PARTNER_CATEGORY_CODE));
	}

	@Test
	public void shouldReturnFalseIfCategoryCodeIsNull()
	{
		partnerCompanyData.setPartnerCategories(Arrays.asList(partnerCategoryData));

		assertFalse(testedInstance.partnerHasCategory(partnerCompanyData, null));
	}

	@Test
	public void shouldReturnDiscountGroupCode()
	{
		when(wileyPartnerService.getPartnerById(PARTNER_ID)).thenReturn(partnerCompany);
		when(partnerCompany.getUserDiscountGroup()).thenReturn(UserDiscountGroup.STUDENT);

		final String discountGroupCode = testedInstance.getPartnerDiscountGroupCode(PARTNER_ID);

		assertEquals(UserDiscountGroup.STUDENT.getCode(), discountGroupCode);
	}

	@Test
	public void shouldReturnNullIfPartnerCompanyNotFound()
	{
		when(wileyPartnerService.getPartnerById(PARTNER_ID)).thenReturn(null);

		final String discountGroupCode = testedInstance.getPartnerDiscountGroupCode(PARTNER_ID);

		assertNull(discountGroupCode);
	}

	@Test
	public void shouldReturnNullIfPartnerCompanyDiscountGroupIsNull()
	{
		when(wileyPartnerService.getPartnerById(PARTNER_ID)).thenReturn(partnerCompany);
		when(partnerCompany.getUserDiscountGroup()).thenReturn(null);

		final String discountGroupCode = testedInstance.getPartnerDiscountGroupCode(PARTNER_ID);

		assertNull(discountGroupCode);
	}

	@Test
	public void shouldReturnListOfPartnersByCategory()
	{
		when(partnerCompany.getLocName()).thenReturn(PARTNER_NAME_1);
		when(partnerCompany2.getLocName()).thenReturn(PARTNER_NAME_2);
		when(partnerCompany.getUid()).thenReturn(PARTNER_ID);
		when(partnerCompany2.getUid()).thenReturn(PARTNER_ID_2);
		when(wileyPartnerService.getPartnersByCategory(CATEGORY_CODE)).thenReturn(Arrays.asList(partnerCompany, partnerCompany2));

		final Map<String, String> partners = testedInstance.getPartnersNamesByCategoryCode(CATEGORY_CODE);
		final Map<String, String> partnerMap = new HashMap<String, String>();
		partnerMap.put(PARTNER_ID, PARTNER_NAME_1);
		partnerMap.put(PARTNER_ID_2, PARTNER_NAME_2);

		assertNotNull(partners);
		assertEquals("Should return the Map of partner Id and names.", partnerMap, partners);
	}

	@Test
	public void isKPMGPartner()
	{
		when(wileyPartnerService.partnerBelongsToUserGroup(PARTNER_ID, WileyCoreConstants.KPMG_GROUP_UID))
				.thenReturn(true);
		when(userGroupData.getUid()).thenReturn(PARTNER_ID);

		final Boolean isKPMGPartner = testedInstance.isKPMGPartner(userGroupData);
		assertTrue(isKPMGPartner);
	}

	@Test
	public void isSpecialPartner()
	{
		when(wileyPartnerService.partnerBelongsToUserGroup(PARTNER_ID, WileyCoreConstants.SPECIAL_PARTNER_GROUP_UID))
				.thenReturn(true);
		when(userGroupData.getUid()).thenReturn(PARTNER_ID);

		final Boolean isSpecialPartner = testedInstance.isSpecialPartner(userGroupData);
		assertTrue(isSpecialPartner);
	}
}