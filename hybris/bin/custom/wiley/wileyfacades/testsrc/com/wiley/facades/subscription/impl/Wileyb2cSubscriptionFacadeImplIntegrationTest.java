package com.wiley.facades.subscription.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.subscriptionfacades.data.SubscriptionTermData;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.enums.PaymentModeEnum;
import com.wiley.core.i18n.WileyCountryService;
import com.wiley.facades.product.data.WileySubscriptionData;
import com.wiley.facades.product.data.WileySubscriptionPaymentTransactionData;

import static java.lang.String.format;
import static org.apache.commons.collections.CollectionUtils.isNotEmpty;


@IntegrationTest
public class Wileyb2cSubscriptionFacadeImplIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	protected static final String CUSTOMER_ID = "customer@test.com";
	protected static final String BASE_SITE = "wileyb2c";

	protected static final String SUBSCRIPTION_CODE_1 = "intTestSubscriptionCode1";
	protected static final String SUBSCRIPTION_CODE_2 = "intTestSubscriptionCode2";
	protected static final String IMPEX_DIR = "/wileyfacades/test/Wileyb2cSubscriptionFacadeImplIntegrationTest/";
	protected static final int PAGE_SIZE = 3;

	@Resource
	private Wileyb2cSubscriptionFacadeImpl wileyb2cSubscriptionFacade;

	@Resource
	private BaseSiteService baseSiteService;
	@Resource
	private UserService userService;
	@Resource
	private WileyCountryService wileyCountryService;

	@Before
	public void setUp() throws Exception
	{
		userService.setCurrentUser(userService.getUserForUID(CUSTOMER_ID));
		baseSiteService.setCurrentBaseSite(BASE_SITE, true);
		wileyCountryService.useDefaultCountryAsCurrent();

		importCsv("/wileyfacades/test/Wileyb2cCartFacadeImplIntegrationTest/product-subscription.impex", DEFAULT_ENCODING);
		importCsv(IMPEX_DIR + "test_subscriptions.impex", DEFAULT_ENCODING);
		importCsv(IMPEX_DIR + "test_payment_transactions.impex", DEFAULT_ENCODING);
	}

	@Test
	public void verifyAllSubscriptionBillingActivitiesAreFetchedForProduct1()
	{
		// Given
		PageableData pageableData = new PageableData();
		pageableData.setPageSize(PAGE_SIZE);

		// Testing
		//Impex has 8 transactions for SUBSCRIPTION_CODE_1 - third page should display last 2 records
		pageableData.setCurrentPage(2);
		SearchPageData<WileySubscriptionPaymentTransactionData> searchPageData1
				= wileyb2cSubscriptionFacade.getSubscriptionBillingActivities(SUBSCRIPTION_CODE_1, pageableData);

		// Verify
		assertEquals(2, searchPageData1.getResults().size());
	}

	@Test
	public void verifyAllSubscriptionBillingActivitiesAreFetchedForProduct2()
	{
		// Given
		PageableData pageableData = new PageableData();
		pageableData.setPageSize(PAGE_SIZE);


		// Testing
		//There are 2 transactions for SUBSCRIPTION_CODE_2
		pageableData.setCurrentPage(0);
		SearchPageData<WileySubscriptionPaymentTransactionData> searchPageData2
				= wileyb2cSubscriptionFacade.getSubscriptionBillingActivities(SUBSCRIPTION_CODE_2, pageableData);

		// Verify
		assertEquals(2, searchPageData2.getResults().size());
	}

	@Test
	public void verifyAllSubscriptionBillingActivitiesTransactionFieldsMappedCorrectly()
	{
		// Given
		PageableData pageableData = new PageableData();
		pageableData.setPageSize(PAGE_SIZE);
		pageableData.setCurrentPage(0);

		// Testing
		final List<WileySubscriptionPaymentTransactionData> results
				= wileyb2cSubscriptionFacade.getSubscriptionBillingActivities(SUBSCRIPTION_CODE_1, pageableData).getResults();

		// Verify
		final WileySubscriptionPaymentTransactionData transaction1 = results.get(0);
		assertEquals("test_subscription_payment_transaction38", transaction1.getCode());
		assertEquals("$12.95", transaction1.getPaymentAmount());
		assertEquals(PaymentModeEnum.CARD, transaction1.getPaymentMode());
		assertEquals("**** 8888", transaction1.getCreditCardMask());
		assertEquals(PaymentTransactionType.CAPTURE, transaction1.getPaymentStatus());

		final WileySubscriptionPaymentTransactionData transaction3 = results.get(1);
		assertEquals("test_subscription_payment_transaction60", transaction3.getCode());
		assertEquals("AUD $3.00", transaction3.getPaymentAmount());
		assertEquals(PaymentModeEnum.INVOICE, transaction3.getPaymentMode());
		assertEquals(null, transaction3.getCreditCardMask());
		assertEquals(PaymentTransactionType.REFUND, transaction3.getPaymentStatus());

		final WileySubscriptionPaymentTransactionData transaction2 = results.get(2);
		assertEquals("test_subscription_payment_transaction59", transaction2.getCode());
		assertEquals("AUD $4.00", transaction2.getPaymentAmount());
		assertEquals(PaymentModeEnum.PAYPAL, transaction2.getPaymentMode());
		assertEquals(null, transaction2.getCreditCardMask());
		assertEquals(PaymentTransactionType.REFUND, transaction2.getPaymentStatus());
	}

	@Test
	public void verifyAllSubscriptionBillingActivitiesTransactionSortedByBillingDateDesc()
	{
		// Given
		PageableData pageableData = new PageableData();
		pageableData.setPageSize(100);
		pageableData.setCurrentPage(0);

		// Testing
		final List<WileySubscriptionPaymentTransactionData> resultList
				= wileyb2cSubscriptionFacade.getSubscriptionBillingActivities(SUBSCRIPTION_CODE_1, pageableData).getResults();

		// Verify
		//Checking the order is not as in the impex, but according to billing date
		for (int i = 0; i < resultList.size() - 1; ++i)
		{
			assertEquals(1, resultList.get(i).getBillingDate().compareTo(resultList.get(i + 1).getBillingDate()));
		}
	}

	@Test
	public void testConversationOfSubscriptionTerms() throws Exception
	{
		//Given
		final PageableData pageableData = createPageableData(0, 25, "byDate");

		//When
		final SearchPageData<WileySubscriptionData> subscriptionPage = wileyb2cSubscriptionFacade.getSubscriptionPage(
				pageableData);

		//Then
		final List<WileySubscriptionData> results = subscriptionPage.getResults();
		assertNotNull(results);
		assertEquals(3, results.size());
		final Optional<WileySubscriptionData> subscriptionProductData = results.stream().filter(
				data -> "intTestSubscriptionCode3".equals(data.getCode())).findAny();
		assertTrue(subscriptionProductData.isPresent());
		final List<SubscriptionTermData> availableUpgradeSubscriptionTerms =
				subscriptionProductData.get().getAvailableUpgradeSubscriptionTerms();
		assertTrue(isNotEmpty(availableUpgradeSubscriptionTerms));
		assertEquals(2, availableUpgradeSubscriptionTerms.size());

		checkSubscriptionTerm(findSubscriptionTerm(availableUpgradeSubscriptionTerms, "subscription_term_1_month"),
				"1 Month contract - billing every month");
		checkSubscriptionTerm(findSubscriptionTerm(availableUpgradeSubscriptionTerms, "subscription_term_1_year"),
				"1 Year contract - billing every year");
	}

	private static SubscriptionTermData findSubscriptionTerm(final List<SubscriptionTermData> terms,
			final String termId)
	{
		final Optional<SubscriptionTermData> termData = terms.stream().filter(term -> term.getId().equals(termId)).findAny();
		if (termData.isPresent())
		{
			return termData.get();
		}
		else
		{
			final String message = format("subscription term with id %s wasn't found in list", termId);
			fail(message);
			throw new IllegalStateException(message);

		}
	}

	private static void checkSubscriptionTerm(final SubscriptionTermData subscriptionTermData, final String name)
	{
		assertEquals(name, subscriptionTermData.getName());
		final PriceData price = subscriptionTermData.getPrice();
		assertNotNull(price);
	}

	private static PageableData createPageableData(final int pageNumber, final int pageSize, final String sortCode)
	{
		final PageableData pageableData = new PageableData();
		pageableData.setCurrentPage(pageNumber);
		pageableData.setSort(sortCode);
		pageableData.setPageSize(pageSize);
		return pageableData;
	}

}
