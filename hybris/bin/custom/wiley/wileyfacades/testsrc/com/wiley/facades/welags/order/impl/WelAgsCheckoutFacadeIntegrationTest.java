package com.wiley.facades.welags.order.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.order.impl.DefaultCartService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.util.DiscountValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Collection;
import java.util.List;
import java.util.Locale;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.enums.OrderType;
import com.wiley.core.model.WileyFreeTrialVariantProductModel;
import com.wiley.core.servicelayer.i18n.WileycomI18NService;
import com.wiley.facades.welags.pin.exception.PinOperationException;
import com.wiley.facades.product.exception.RepeatedFreeTrialOrderException;
import com.wiley.facades.welags.order.WelAgsCheckoutFacade;

import static com.wiley.facades.welags.pin.exception.PinOperationExceptionReason.ALREADY_ACTIVATED_PIN;
import static com.wiley.facades.welags.pin.exception.PinOperationExceptionReason.INVALID_PIN;
import static java.lang.String.format;


@IntegrationTest
public class WelAgsCheckoutFacadeIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	private static final String BILLING_VALUES = "bill";
	private static final String SHIPPING_VALUES = "shipp";

	private static final String TEST_USER = "test@hybris.com";

	private static final String VALID_PIN_CODE1 = "TEST-PIN-123";
	private static final String VALID_PIN_CODE2 = "TEST-PIN-456";
	private static final String INVALID_PIN_CODE = "INVALID-PIN";
	private static final String INVALID_FREE_TRIAL_CODE = "INVALID-FREE-TRIAL";
	private static final String VALID_FREE_TRIAL_CODE = "WEL_CFA_L1_2016_FREE_TRIAL_VARIANT";
	private static final String PRODUCT_CODE1 = "WEL_CPA_PLATINUM_AUD_EBOOK";
	private static final Double ZERO_PRICE = 0.0;
	private static final String BASE_SITE_UID = "wel";

	public static final String TEST_RESOURCES_FOLDER = "/wileyfacades/test/WileyCheckoutFacadeTest";
	private static final boolean SUBSCRIBE_TO_UPDATES = true;

	@Resource(name = "welAgsCheckoutFacade")
	private WelAgsCheckoutFacade wileyCheckoutFacade;

	@Resource(name = "i18NFacade")
	private I18NFacade i18NFacade;

	@Resource(name = "userFacade")
	private UserFacade userFacade;

	@Resource(name = "customerFacade")
	private CustomerFacade customerFacade;

	@Resource
	private UserService userService;

	@Resource
	private I18NService i18nService;

	@Resource
	private ModelService modelService;

	@Resource
	private SessionService sessionService;

	@Resource
	private BaseSiteService baseSiteService;

	@Resource
	private CustomerAccountService customerAccountService;

	@Resource
	private WileycomI18NService wileycomI18NService;
	
	@Before
	public void setUp() throws Exception
	{
		importCsv(TEST_RESOURCES_FOLDER + "/testWelCustomerAndCart.impex", DEFAULT_ENCODING);
		baseSiteService.setCurrentBaseSite(baseSiteService.getBaseSiteForUID("wel"), true);
		wileycomI18NService.setDefaultCurrentCountry();
	}

	@Test
	public void shouldInsertBothBillingAndShippingAddresses()
	{
		userService.setCurrentUser(userService.getUserForUID(TEST_USER));
		i18nService.setCurrentLocale(Locale.ENGLISH);
		UserModel currentUser = userService.getCurrentUser();
		assertNotNull(currentUser);
		assertNotNull(currentUser.getCarts().iterator().next());
		sessionService.setAttribute(DefaultCartService.SESSION_CART_PARAMETER_NAME, currentUser.getCarts().iterator().next());

		AddressData billingAddress = createAddressData(BILLING_VALUES, true);
		AddressData shippingAddress = createAddressData(SHIPPING_VALUES, false);

		wileyCheckoutFacade.updateBillingAndShippingAddresses(billingAddress, shippingAddress, false);

		// check existing both addresses in Customer
		CustomerData customer = customerFacade.getCurrentCustomer();
		assertNotNull(customer);
		assertNotNull(customer.getDefaultShippingAddress());
		assertNotNull(customer.getDefaultBillingAddress());
		assertEquals(SHIPPING_VALUES, customer.getDefaultShippingAddress().getFirstName());
		assertEquals(BILLING_VALUES, customer.getDefaultBillingAddress().getFirstName());

		// check cart addresses updated
		CartData updatedCart = wileyCheckoutFacade.getCheckoutCart();
		assertNotNull(updatedCart);
		assertNotNull(updatedCart.getDeliveryAddress());
		assertEquals(SHIPPING_VALUES, updatedCart.getDeliveryAddress().getFirstName());
	}

	@Test
	public void shouldInsertBillingAsShippingAddresses()
	{
		userService.setCurrentUser(userService.getUserForUID(TEST_USER));
		i18nService.setCurrentLocale(Locale.ENGLISH);
		UserModel currentUser = userService.getCurrentUser();
		assertNotNull(currentUser);
		CartModel currentCart = currentUser.getCarts().iterator().next();
		assertNotNull(currentCart);
		sessionService.setAttribute(DefaultCartService.SESSION_CART_PARAMETER_NAME, currentCart);

		AddressData billingAddress = createAddressData(BILLING_VALUES, true);
		AddressData shippingAddress = createAddressData(SHIPPING_VALUES, false);

		wileyCheckoutFacade.updateBillingAndShippingAddresses(billingAddress, shippingAddress, true);

		// check existing both addresses in Customer
		CustomerData customer = customerFacade.getCurrentCustomer();
		assertNotNull(customer);
		assertNotNull(customer.getDefaultShippingAddress());
		assertNotNull(customer.getDefaultBillingAddress());
		assertEquals(BILLING_VALUES, customer.getDefaultShippingAddress().getFirstName());
		assertEquals(BILLING_VALUES, customer.getDefaultBillingAddress().getFirstName());

		// check cart addresses updated
		CartData updatedCart = wileyCheckoutFacade.getCheckoutCart();
		assertNotNull(updatedCart);
		assertNotNull(updatedCart.getDeliveryAddress());
		assertEquals(BILLING_VALUES, updatedCart.getDeliveryAddress().getFirstName());
	}

	@Test
	public void shouldUpdateBothBillingAndShippingAddresses()
	{
		userService.setCurrentUser(userService.getUserForUID(TEST_USER));
		i18nService.setCurrentLocale(Locale.ENGLISH);

		AddressData billingAddressDataOld = createAddressData(BILLING_VALUES + "Old", true);
		AddressData shippingAddressDataOld = createAddressData(SHIPPING_VALUES + "Old", false);

		UserModel currentUser = userService.getCurrentUser();
		userFacade.addAddress(billingAddressDataOld);
		AddressModel billingAddress = currentUser.getDefaultPaymentAddress();
		userFacade.addAddress(shippingAddressDataOld);
		currentUser.setDefaultPaymentAddress(billingAddress);
		modelService.save(currentUser);

		modelService.refresh(currentUser);
		PK billingPk = currentUser.getDefaultPaymentAddress().getPk();
		System.out.println("billingPk = " + billingPk);
		PK shippingPk = currentUser.getDefaultShipmentAddress().getPk();
		System.out.println("shippingPk = " + shippingPk);

		AddressData billingAddressData = createAddressData(BILLING_VALUES, true);
		billingAddressData.setId(billingPk.toString());
		AddressData shippingAddressData = createAddressData(SHIPPING_VALUES, false);
		shippingAddressData.setId(shippingPk.toString());

		wileyCheckoutFacade.updateBillingAndShippingAddresses(billingAddressData, shippingAddressData, false);

		// check the same PK existing both addresses in Customer
		CustomerData customer = customerFacade.getCurrentCustomer();
		assertNotNull(customer);
		assertNotNull(customer.getDefaultShippingAddress());
		assertNotNull(customer.getDefaultBillingAddress());
		assertEquals(SHIPPING_VALUES, customer.getDefaultShippingAddress().getFirstName());
		assertEquals(BILLING_VALUES, customer.getDefaultBillingAddress().getFirstName());
		assertEquals(billingPk.toString(), customer.getDefaultBillingAddress().getId());
		assertEquals(shippingPk.toString(), customer.getDefaultShippingAddress().getId());

	}

	@Test
	public void shouldIgnoreShippingAddressIfBillingOnlyCase()
	{
		userService.setCurrentUser(userService.getUserForUID(TEST_USER));
		i18nService.setCurrentLocale(Locale.ENGLISH);
		UserModel currentUser = userService.getCurrentUser();
		assertNotNull(currentUser);
		CartModel currentCart = currentUser.getCarts().iterator().next();
		assertNotNull(currentCart);
		sessionService.setAttribute(DefaultCartService.SESSION_CART_PARAMETER_NAME, currentCart);

		AddressData billingAddress = createAddressData(BILLING_VALUES, true);

		wileyCheckoutFacade.updateBillingAndShippingAddresses(billingAddress, null, true);

		// check existing both addresses in Customer
		CustomerData customer = customerFacade.getCurrentCustomer();
		assertNotNull(customer);
		assertNotNull(customer.getDefaultShippingAddress());
		assertNotNull(customer.getDefaultBillingAddress());
		assertEquals(BILLING_VALUES, customer.getDefaultShippingAddress().getFirstName());
		assertEquals(BILLING_VALUES, customer.getDefaultBillingAddress().getFirstName());

		// check cart addresses updated
		CartData updatedCart = wileyCheckoutFacade.getCheckoutCart();
		assertNotNull(updatedCart);
		assertNull(updatedCart.getDeliveryAddress());
	}

	@Test
	public void shouldThrowExceptionForAnonymousUserWhenActivatePin()
			throws PinOperationException, InvalidCartException, ImpExException
	{
		importCsv(TEST_RESOURCES_FOLDER + "/testWelPinOrder.impex", DEFAULT_ENCODING);
		UserModel userModel = userService.getAnonymousUser();
		userService.setCurrentUser(userModel);

		try
		{
			wileyCheckoutFacade.placePinOrder(VALID_PIN_CODE1, OrderType.GENERAL);
			fail();
		}
		catch (IllegalAccessException e)
		{
			assertEquals("Anonymous user is not allowed to activate pin.", e.getMessage());
		}
		modelService.refresh(userModel);
		Collection<OrderModel> orders = userModel.getOrders();
		assertEquals("Anonymous user should not have an order", 0, orders.size());
	}

	@Test
	public void shouldActivateValidPin()
			throws PinOperationException, InvalidCartException, ImpExException, IllegalAccessException
	{
		importCsv(TEST_RESOURCES_FOLDER + "/testWelPinOrder.impex", DEFAULT_ENCODING);
		UserModel userModel = userService.getUserForUID(TEST_USER);
		userService.setCurrentUser(userModel);
		baseSiteService.setCurrentBaseSite(BASE_SITE_UID, false);

		String orderCode = wileyCheckoutFacade.placePinOrder(VALID_PIN_CODE1, OrderType.GENERAL);

		modelService.refresh(userModel);
		Collection<OrderModel> orders = userModel.getOrders();
		assertEquals("User should have exactly one order", 1, orders.size());

		OrderModel orderModel = orders.iterator().next();
		List<AbstractOrderEntryModel> orderEntries = orderModel.getEntries();
		assertEquals("Order codes should match", orderCode, orderModel.getCode());
		assertEquals("Order must contain one entry", 1, orderEntries.size());
		assertEquals("Proper product should be ordered", PRODUCT_CODE1, orderEntries.get(0).getProduct().getCode());
		assertEquals("Total price should be zero", ZERO_PRICE, orderModel.getTotalPrice());

		List<DiscountValue> globalDiscounts = orderModel.getGlobalDiscountValues();
		assertEquals("Order should have global discounts", 1, globalDiscounts.size());
		assertEquals("Discount code should be the PIN code", VALID_PIN_CODE1, globalDiscounts.get(0).getCode());
	}

	@Test
	public void shouldThrowExceptionWhenPinIsBeingActivatedTwice()
			throws PinOperationException, InvalidCartException, ImpExException, IllegalAccessException
	{
		importCsv(TEST_RESOURCES_FOLDER + "/testWelPinOrder.impex", DEFAULT_ENCODING);
		UserModel userModel = userService.getUserForUID(TEST_USER);
		userService.setCurrentUser(userModel);
		baseSiteService.setCurrentBaseSite(BASE_SITE_UID, false);

		wileyCheckoutFacade.placePinOrder(VALID_PIN_CODE2, OrderType.GENERAL);
		try
		{
			wileyCheckoutFacade.placePinOrder(VALID_PIN_CODE2, OrderType.GENERAL);
			fail("PIN operation exception must be thrown.");
		}
		catch (PinOperationException e)
		{
			assertEquals("Already activated exception reason must be set.", ALREADY_ACTIVATED_PIN, e.getReason());
		}
		modelService.refresh(userModel);
		assertEquals("User should have exactly one order", 1, userModel.getOrders().size());
	}

	@Test
	public void shouldTrowExceptionWhenPinIsInvalid() throws InvalidCartException, ImpExException, IllegalAccessException
	{
		importCsv(TEST_RESOURCES_FOLDER + "/testWelPinOrder.impex", DEFAULT_ENCODING);
		UserModel userModel = userService.getUserForUID(TEST_USER);
		userService.setCurrentUser(userModel);

		try
		{
			wileyCheckoutFacade.placePinOrder(INVALID_PIN_CODE, OrderType.GENERAL);
			fail("PIN operation exception must be thrown.");
		}
		catch (PinOperationException e)
		{
			assertEquals("Invalid PIN exception reason must be set.", INVALID_PIN, e.getReason());
		}
		modelService.refresh(userModel);
		assertTrue("User should not have orders", userModel.getOrders().isEmpty());
	}


	/**
	 * Test placeFreeTrialOrder when freeTrialCode is invalid
	 */
	@Test
	public void shouldThrowExceptionWhenFreeTrialCodeIsInvalid() throws Exception
	{
		UserModel userModel = userService.getUserForUID(TEST_USER);
		userService.setCurrentUser(userModel);

		try
		{
			wileyCheckoutFacade.placeFreeTrialOrder(INVALID_FREE_TRIAL_CODE, SUBSCRIBE_TO_UPDATES, OrderType.FREE_TRIAL);
			fail("UnknownIdentifierException must be thrown.");
		}
		catch (UnknownIdentifierException e)
		{
			assertEquals(
					format("%s with code '%s' not found!", WileyFreeTrialVariantProductModel._TYPECODE, INVALID_FREE_TRIAL_CODE),
					e.getMessage());
		}
		modelService.refresh(userModel);
		assertTrue("User should not have orders", userModel.getOrders().isEmpty());
	}

	/**
	 * Test placeFreeTrialOrder when freeTrialCode is valid and user hasn't orders with the same free trial product
	 */
	@Test
	public void shouldPlaceValidFreeTrialOrder() throws Exception
	{
		UserModel userModel = userService.getUserForUID(TEST_USER);
		userService.setCurrentUser(userModel);
		baseSiteService.setCurrentBaseSite(BASE_SITE_UID, false);

		String orderCode = wileyCheckoutFacade.placeFreeTrialOrder(VALID_FREE_TRIAL_CODE, SUBSCRIBE_TO_UPDATES,
				OrderType.FREE_TRIAL).getCode();

		modelService.refresh(userModel);
		Collection<OrderModel> orders = userModel.getOrders();
		assertEquals("User should have exactly one order", 1, orders.size());

		OrderModel orderModel = orders.iterator().next();
		List<AbstractOrderEntryModel> orderEntries = orderModel.getEntries();
		assertEquals("Order codes should match", orderCode, orderModel.getCode());
		assertEquals("Order must contain one entry", 1, orderEntries.size());
		assertEquals("Proper product should be ordered", VALID_FREE_TRIAL_CODE, orderEntries.get(0).getProduct().getCode());
		assertEquals("Total price should be zero", ZERO_PRICE, orderModel.getTotalPrice());
		assertEquals("Subscribe to updates value should be saved to order", SUBSCRIBE_TO_UPDATES,
				orderModel.getSubscribeToUpdates());
		assertEquals("Order must be free trial type", OrderType.FREE_TRIAL, orderModel.getOrderType());
	}

	/**
	 * Test placeFreeTrialOrder when user has already have order with the same freeTrialCode
	 */
	@Test
	public void shouldThrowExceptionForRepeatedFreeTrialOrder() throws Exception
	{
		UserModel userModel = userService.getUserForUID(TEST_USER);
		userService.setCurrentUser(userModel);
		baseSiteService.setCurrentBaseSite(BASE_SITE_UID, false);

		String orderCode = wileyCheckoutFacade.placeFreeTrialOrder(VALID_FREE_TRIAL_CODE, SUBSCRIBE_TO_UPDATES,
				OrderType.FREE_TRIAL).getCode();
		simulateSuccessfulFulfilment(orderCode);
		try
		{
			wileyCheckoutFacade.placeFreeTrialOrder(VALID_FREE_TRIAL_CODE, SUBSCRIBE_TO_UPDATES, OrderType.FREE_TRIAL);
			fail("RepeatedFreeTrialOrderException must be thrown.");
		}
		catch (RepeatedFreeTrialOrderException e)
		{
			assertEquals(String.format("User has already have order with %s code ", VALID_FREE_TRIAL_CODE), e.getMessage());
		}
		modelService.refresh(userModel);
		assertEquals("User should have exactly one order", 1, userModel.getOrders().size());
	}

	private void simulateSuccessfulFulfilment(final String orderCode)
	{
		BaseStoreModel currentStore = baseSiteService.getCurrentBaseSite().getStores().get(0);
		OrderModel order = customerAccountService.getOrderForCode(orderCode, currentStore);
		order.setStatus(OrderStatus.COMPLETED);
		modelService.save(order);
	}

	private AddressData createAddressData(final String value, boolean isBilling)
	{
		AddressData newAddress = new AddressData();
		newAddress.setId("");
		newAddress.setFirstName(value);
		newAddress.setLastName(value);
		newAddress.setLine1(value);
		newAddress.setLine2(value);
		newAddress.setTown(value);
		newAddress.setPostalCode(value);
		newAddress.setPhone(value);
		newAddress.setBillingAddress(isBilling);
		newAddress.setShippingAddress(!isBilling);
		newAddress.setDefaultAddress(true);
		newAddress.setVisibleInAddressBook(true);
		newAddress.setCountry(i18NFacade.getCountryForIsocode("US"));
		return newAddress;
	}
}
