package com.wiley.facades.product.converters.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyPurchaseOptionProductModel;
import com.wiley.facades.wileyb2c.product.converters.populators.Wileyb2cProductImprintsPopulator;

import static java.util.Arrays.asList;
import static junit.framework.Assert.assertEquals;


/**
 * Created by Sergiy_Mishkovets on 1/25/2018.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cProductImprintsPopulatorUnitTest
{
	private static final String BRANDS_CATEGORY_CODE = "brands";
	private static final String IMPRINTS_CATEGORY_CODE = "imprints";

	private static final String IMPRINT_CATEGORY_NAME_1 = "imprintCategory1";
	private static final String IMPRINT_CATEGORY_NAME_2 = "imprintCategory2";
	private static final String BRAND_CATEGORY_NAME = "brandCategory";

	private Wileyb2cProductImprintsPopulator testInstance = new Wileyb2cProductImprintsPopulator();

	@Mock
	private ProductModel mockProductModel;
	@Mock
	private WileyPurchaseOptionProductModel mockWileyPurchaseOptionProductModel;
	@Mock
	private CategoryModel mockImprintsTopCategory;
	@Mock
	private CategoryModel mockBrandsTopCategory;
	@Mock
	private CategoryModel mockBrandCategory;
	@Mock
	private CategoryModel mockImprintCategory1;
	@Mock
	private CategoryModel mockImprintCategory2;

	private ProductData productData;

	@Before
	public void setUp()
	{
		testInstance.setImprintsCategoryCode(IMPRINTS_CATEGORY_CODE);
		productData = new ProductData();

		when(mockImprintsTopCategory.getCode()).thenReturn(IMPRINTS_CATEGORY_CODE);
		when(mockBrandsTopCategory.getCode()).thenReturn(BRANDS_CATEGORY_CODE);

		mockBrandCategory = givenCategory(BRAND_CATEGORY_NAME, mockBrandsTopCategory);
		mockImprintCategory1 = givenCategory(IMPRINT_CATEGORY_NAME_1, mockBrandsTopCategory, mockImprintsTopCategory);
		mockImprintCategory2 = givenCategory(IMPRINT_CATEGORY_NAME_2, mockBrandsTopCategory, mockImprintsTopCategory);
	}

	@Test
	public void shouldPopulateAllImprintsIfProductBelongsToSeveralImprintCategories()
	{
		when(mockProductModel.getSupercategories()).thenReturn(
				asList(mockImprintCategory1, mockImprintCategory2, mockBrandCategory));

		testInstance.populate(mockProductModel, productData);

		assertEquals(StringUtils.join(asList(IMPRINT_CATEGORY_NAME_1, IMPRINT_CATEGORY_NAME_2), ", "), productData.getImprints());
	}

	@Test
	public void shouldPopulateImprintIfProductBelongsToJustOneImprintCategory()
	{
		when(mockProductModel.getSupercategories()).thenReturn(asList(mockImprintCategory1, mockBrandCategory));

		testInstance.populate(mockProductModel, productData);

		assertEquals(IMPRINT_CATEGORY_NAME_1, productData.getImprints());
	}

	@Test
	public void shouldNotPopulateImprintsIfProductNotBelongToImprintsCategory()
	{
		when(mockProductModel.getSupercategories()).thenReturn(asList(mockBrandCategory));

		testInstance.populate(mockProductModel, productData);

		assertEquals("", productData.getImprints());
	}


	@Test
	public void shouldPopulateImprintsForWileyPurchaseOptionProduct()
	{
		when(mockProductModel.getSupercategories()).thenReturn(asList(mockImprintCategory1, mockBrandCategory));
		when(mockWileyPurchaseOptionProductModel.getBaseProduct()).thenReturn(mockProductModel);

		testInstance.populate(mockWileyPurchaseOptionProductModel, productData);

		assertEquals(IMPRINT_CATEGORY_NAME_1, productData.getImprints());

	}

	private CategoryModel givenCategory(final String name, final CategoryModel... superCategories)
	{
		final CategoryModel category = mock(CategoryModel.class);
		when(category.getName()).thenReturn(name);
		if (superCategories != null)
		{
			when(category.getAllSupercategories()).thenReturn(asList(superCategories));
		}
		return category;
	}

}
