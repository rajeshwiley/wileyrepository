package com.wiley.facades.student.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.i18n.WileyCountryService;
import com.wiley.core.model.UniversityModel;
import com.wiley.core.university.services.UniversityService;
import com.wiley.facades.student.data.StudentVerificationData;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyStudentVerificationFacadeImplUnitTest
{

	public static final String TEST_UNIVERSITY_TEXT = "test university text";
	public static final String TEST_UNIVERSITY_NAME = "test university name";
	public static final String TEST_UNIVERSITY_STATE = "test university state";
	public static final String TEST_UNIVERSITY_COUNTRY = "test university country";
	public static final String TEST_COUNTRY_NON_USA_NUMERIC = "testCountryNonUSA";
	public static final String TEST_UNIVERSITY_CODE = "universityCode";
	@Mock
	private CartService cartService;

	@Mock
	private ModelService modelService;

	@Mock
	private CartModel cartModel;

	@Mock
	private UniversityService universityService;

	@Mock
	private StudentVerificationData studentVerificationData;

	@Mock
	private  UniversityModel universityModel;

	@Mock
	private RegionModel regionModelMock;
	
	@Mock
	private CountryModel countryModelMock;

	@Mock
	private WileyCountryService countryService;

	@InjectMocks
	private WileyStudentVerificationFacadeImpl testInstance = new WileyStudentVerificationFacadeImpl();


	@Test
	public void testSaveUniversityTextInCartForNonUSA()
	{
		when(cartService.hasSessionCart()).thenReturn(true);
		when(cartService.getSessionCart()).thenReturn(cartModel);
		when(countryService.findCountryByCode(any())).thenReturn(countryModelMock);
		when(countryModelMock.getNumeric()).thenReturn(TEST_COUNTRY_NON_USA_NUMERIC);
		
		when(studentVerificationData.getUniversityTextField()).thenReturn(TEST_UNIVERSITY_TEXT);

		testInstance.saveStudentVerification(studentVerificationData);
		verify(cartModel).setUniversity(TEST_UNIVERSITY_TEXT);
		verify(cartModel).setUniversityCountry(TEST_COUNTRY_NON_USA_NUMERIC);
		verify(modelService).save(cartModel);
	}
	
	@Test
	public void testSaveUniversityInCartForUSA()
	{
		when(cartService.hasSessionCart()).thenReturn(true);
		when(cartService.getSessionCart()).thenReturn(cartModel);
		when(studentVerificationData.getCountryIso()).thenReturn(WileyCoreConstants.USA_COUNTRY_ISO_CODE);
		
		mockUniversityInfo();

		testInstance.saveStudentVerification(studentVerificationData);
		verify(cartModel).setUniversity(TEST_UNIVERSITY_NAME);
		verify(cartModel).setUniversityCountry(TEST_UNIVERSITY_COUNTRY);
		verify(cartModel).setUniversityState(TEST_UNIVERSITY_STATE);
		verify(modelService).save(cartModel);
	}

	private void mockUniversityInfo()
	{
		when(studentVerificationData.getUniversityCode()).thenReturn(TEST_UNIVERSITY_CODE);
		when(universityService.getUniversityByCode(TEST_UNIVERSITY_CODE)).thenReturn(Optional.of(universityModel));
		when(universityModel.getName()).thenReturn(TEST_UNIVERSITY_NAME);
		when(universityModel.getRegion()).thenReturn(regionModelMock);
		when(regionModelMock.getName()).thenReturn(TEST_UNIVERSITY_STATE);
		when(universityModel.getCountry()).thenReturn(countryModelMock);
		when(countryModelMock.getNumeric()).thenReturn(TEST_UNIVERSITY_COUNTRY);
	}

	@Test
	public void testCouldntFindUniversityByCodeForUSA()
	{
		when(cartService.hasSessionCart()).thenReturn(true);
		when(cartService.getSessionCart()).thenReturn(cartModel);
		when(studentVerificationData.getCountryIso()).thenReturn(WileyCoreConstants.USA_COUNTRY_ISO_CODE);
		when(studentVerificationData.getUniversityCode()).thenReturn(TEST_UNIVERSITY_CODE);
		when(universityService.getUniversityByCode(TEST_UNIVERSITY_CODE)).thenReturn(Optional.empty());
		when(universityModel.getName()).thenReturn(TEST_UNIVERSITY_NAME);
		try
		{
			testInstance.saveStudentVerification(studentVerificationData);

		} catch (IllegalArgumentException ex) {
			Assert.assertEquals("University name should not be blank", ex.getMessage());
		}
		verify(cartModel, Mockito.never()).setUniversity(Mockito.anyString());
		verify(modelService, Mockito.never()).save(cartModel);
	}


}
