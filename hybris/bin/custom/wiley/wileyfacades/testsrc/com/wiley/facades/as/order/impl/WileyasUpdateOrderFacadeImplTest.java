package com.wiley.facades.as.order.impl;

import com.wiley.core.mpgs.services.WileyMPGSMerchantService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.servicelayer.model.ModelService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class WileyasUpdateOrderFacadeImplTest {

    private static final String DEFAULT_MERCHANT_ID = "DEFAULT_MERCHANT_ID";
    private static final String NEW_MERCHANT_ID = "NEW_MERCHANT_ID";
    @Mock
    private ModelService modelService;
    @Mock
    private WileyMPGSMerchantService wileyMPGSMerchantService;

    @InjectMocks
    private WileyasUpdateOrderFacadeImpl testInstance;

    @Mock
    private OrderModel orderMock;

    private CreditCardPaymentInfoModel paymentInfo = new CreditCardPaymentInfoModel();

    @Before
    public void setUp() throws Exception {
        paymentInfo.setMerchantId(DEFAULT_MERCHANT_ID);
        when(orderMock.getPaymentInfo()).thenReturn(paymentInfo);
    }

    @Test
    public void shouldUpdateMerchantId() {
        //Given
        when(wileyMPGSMerchantService.getMerchantID(orderMock)).thenReturn(NEW_MERCHANT_ID);
        //When
        testInstance.updatePaymentInfo(orderMock);
        //Then
        CreditCardPaymentInfoModel paymentInfo = (CreditCardPaymentInfoModel) orderMock.getPaymentInfo();
        assertEquals(NEW_MERCHANT_ID, paymentInfo.getMerchantId());

    }

}