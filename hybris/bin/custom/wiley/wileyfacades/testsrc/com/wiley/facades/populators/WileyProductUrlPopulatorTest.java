/**
 *
 */
package com.wiley.facades.populators;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.product.CommerceProductService;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


@UnitTest
public class WileyProductUrlPopulatorTest 
{
    @InjectMocks
    private final WileyProductUrlPopulator populator = new WileyProductUrlPopulator();

    private static final String TEST_INTERNAL_CATEGORY_CODE = "WEL_TEST_CATEGORY";
    private static final String TEST_EXTERNAL_CATEGORY_CODE = "WEL_NO_PDP";

    private ArrayList<CategoryModel> mockCategories;

    @Mock
    private AbstractOrderEntryModel mockOrderEntryModel;

    @Mock
    private CommerceProductService mockCommerceProductService;
    
    @Mock
    private OrderEntryData mockOrderEntryData;
  
    @Mock
    private ProductModel mockProduct;
  
    @Mock
    private VariantProductModel mockVariantProduct;
    
    @Mock
    private ProductData mockProductData;
  
    @Mock
    private CategoryModel mockInternalCategory;
    
    @Mock
    private CategoryModel mockExternalCategory;
    
    @Before
    public void setUp() throws Exception
    {
        MockitoAnnotations.initMocks(this);
        
        populator.setCommerceProductService(mockCommerceProductService);
        populator.setExternalCategoryCode(TEST_EXTERNAL_CATEGORY_CODE);
        
        mockCategories = new ArrayList<CategoryModel>();
        mockCategories.add(mockInternalCategory);
    
        when(mockInternalCategory.getCode()).thenReturn(TEST_INTERNAL_CATEGORY_CODE);
        when(mockExternalCategory.getCode()).thenReturn(TEST_EXTERNAL_CATEGORY_CODE);
        
        
        when(mockOrderEntryData.getProduct()).thenReturn(mockProductData);
    }
  
    @Test
    public void shouldSetTargetUrlIfNotExternalBaseProduct() throws Exception
    {
        //given
        when(mockOrderEntryModel.getProduct()).thenReturn(mockProduct);
        when(populator.getCommerceProductService()
            .getSuperCategoriesExceptClassificationClassesForProduct(mockProduct))
                .thenReturn(mockCategories);
    
        //when
        populator.populate(mockOrderEntryModel, mockOrderEntryData);
    
        //then
        verify(mockProductData, never()).setUrl(StringUtils.EMPTY);
    }
    
    @Test
    public void shouldNotSetTargetUrlIfExternalBaseProduct() throws Exception
    {
        mockCategories.add(mockExternalCategory);
        
        //given
        when(mockOrderEntryModel.getProduct()).thenReturn(mockProduct);  
        when(populator.getCommerceProductService()
            .getSuperCategoriesExceptClassificationClassesForProduct(mockProduct))
                .thenReturn(mockCategories);
    
        //when
        populator.populate(mockOrderEntryModel, mockOrderEntryData);
    
        //then
        verify(mockProductData).setUrl(StringUtils.EMPTY);
    }
    
    @Test
    public void shouldSetTargetUrlIfNotExternalVariantProduct() throws Exception
    {
        //given
        when(mockOrderEntryModel.getProduct()).thenReturn(mockVariantProduct);
        when(mockVariantProduct.getBaseProduct()).thenReturn(mockProduct);
        when(populator.getCommerceProductService()
            .getSuperCategoriesExceptClassificationClassesForProduct(mockProduct))
                .thenReturn(mockCategories);
    
        //when
        populator.populate(mockOrderEntryModel, mockOrderEntryData);
    
        //then
        verify(mockProductData, never()).setUrl(StringUtils.EMPTY);
    }
    
    @Test
    public void shouldNotSetTargetUrlIfExternalVariantProduct() throws Exception
    {
        mockCategories.add(mockExternalCategory);
        
        //given
        when(mockOrderEntryModel.getProduct()).thenReturn(mockVariantProduct);
        when(mockVariantProduct.getBaseProduct()).thenReturn(mockProduct);
        when(populator.getCommerceProductService()
            .getSuperCategoriesExceptClassificationClassesForProduct(mockProduct))
                .thenReturn(mockCategories);
    
        //when
        populator.populate(mockOrderEntryModel, mockOrderEntryData);
    
        //then
        verify(mockProductData).setUrl(StringUtils.EMPTY);
    }
}
