package com.wiley.facades.welags.order.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.assistedservicefacades.AssistedServiceFacade;
import de.hybris.platform.commerceservices.strategies.CheckoutCustomerStrategy;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.voucher.VoucherService;

import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WelAgsAcceleratorCheckoutFacadeUnitTest
{

	private static final Boolean ASM_LOGGED_IN = new Boolean(true);
	@Mock
	private CartService cartServiceMock;
	@Mock
	private ModelService modelServiceMock;
	@Mock
	private AssistedServiceFacade assistedServiceFacadeMock;
	@Mock
	private OrderModel orderModelMock;
	@Mock
	private CartModel cartModelMock;
	@Mock
	private CustomerModel customerModelMock;
	@Mock
	private VoucherService voucherServiceMock;
	@Mock
	private Collection<String> vouchersListMock;
	@Mock
	private CheckoutCustomerStrategy checkoutCustomerStrategyMock;

	@InjectMocks
	private WelAgsAcceleratorCheckoutFacade testInstance;

	@Test
	public void testAfterPlaceOrderShouldFillPlacedByPropertyIfOrderWasPlacedByASMAgent()
	{
		//Given
		when(assistedServiceFacadeMock.isAssistedServiceAgentLoggedIn()).thenReturn(ASM_LOGGED_IN);
		when(checkoutCustomerStrategyMock.getCurrentUserForCheckout()).thenReturn(customerModelMock);
		//When
		testInstance.afterPlaceOrder(cartModelMock, orderModelMock);
		//Then
		verify(orderModelMock).setPlacedBy(customerModelMock);
	}

	@Test
	public void testAfterPlaceOrderShouldCallVoucherServiceToCopyAppliedVoucherCodes()
	{
		//Given
		//When
		testInstance.afterPlaceOrder(cartModelMock, orderModelMock);
		//Then
		verify(voucherServiceMock).afterOrderCreation(same(orderModelMock), same(cartModelMock));
	}

}