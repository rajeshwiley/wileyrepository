package com.wiley.facades.product.converters.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.VariantMatrixElementData;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.enums.UserDiscountGroup;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.util.DiscountValue;

import java.math.BigDecimal;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.product.WileyProductDiscountService;
import com.wiley.facades.product.data.DiscountData;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyDouble;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


/**
 * Default unit test for {@link ProductStudentDiscountPopulator}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ProductStudentDiscountPopulatorUnitTest
{

	public static final String CURRENCY_ISOCODE = "USD";
	public static final int CURRENCY_DIGITS = 2;

	@Mock
	private WileyProductDiscountService wileyProductDiscountServiceMock;

	@Mock
	private Converter<DiscountValue, DiscountData> discountValue2DiscountDataConverterMock;

	@Mock
	private CommerceCommonI18NService commerceCommonI18NServiceMock;

	@Mock
	private PriceDataFactory priceDataFactoryMock;

	@InjectMocks
	private ProductStudentDiscountPopulator productStudentDiscountPopulator;

	// Test data
	@Mock
	private CurrencyModel currencyModelMock;

	@Mock
	private DiscountValue discountValueMock;

	@Mock
	private ProductModel productModelMock;

	@Mock
	private VariantMatrixElementData categoryMatrixElementMock1;
	@Mock
	private VariantMatrixElementData categoryMatrixElementMock2;

	@Mock
	private VariantMatrixElementData productMatrixElementMock1;
	@Mock
	private VariantMatrixElementData productMatrixElementMock2;

	@Mock
	private VariantOptionData productVariantOptionMock1;
	@Mock
	private VariantOptionData productVariantOptionMock2;

	@Mock
	private PriceData productPrice;
	@Mock
	private PriceData productOptionPriceMock1;
	@Mock
	private PriceData productOptionPriceMock2;

	@Mock
	private PriceData productOptionStudentPriceMock;
	
	private Double price = 200.00;

	@Before
	public void setUp() throws Exception
	{
		when(commerceCommonI18NServiceMock.getCurrentCurrency()).thenReturn(currencyModelMock);

		when(priceDataFactoryMock.create(any(PriceDataType.class), any(BigDecimal.class), eq(currencyModelMock))).thenReturn(
				productOptionStudentPriceMock);

		when(currencyModelMock.getIsocode()).thenReturn(CURRENCY_ISOCODE);
		when(currencyModelMock.getDigits()).thenReturn(CURRENCY_DIGITS);

		when(discountValueMock.apply(anyDouble(), eq(price), eq(CURRENCY_DIGITS), eq(CURRENCY_ISOCODE))).thenReturn(
				discountValueMock);
		when(discountValueMock.getAppliedValue()).thenReturn((double) 100);

		when(categoryMatrixElementMock1.getIsLeaf()).thenReturn(false);
		when(categoryMatrixElementMock2.getIsLeaf()).thenReturn(false);

		when(productMatrixElementMock1.getIsLeaf()).thenReturn(true);
		when(productMatrixElementMock2.getIsLeaf()).thenReturn(true);
		when(productMatrixElementMock1.getVariantOption()).thenReturn(productVariantOptionMock1);
		when(productMatrixElementMock2.getVariantOption()).thenReturn(productVariantOptionMock2);

		when(productVariantOptionMock1.getPriceData()).thenReturn(productOptionPriceMock1);
		when(productVariantOptionMock2.getPriceData()).thenReturn(productOptionPriceMock2);

		when(productOptionPriceMock1.getValue()).thenReturn(BigDecimal.valueOf(price));
		when(productOptionPriceMock2.getValue()).thenReturn(BigDecimal.valueOf(price));
		when(productOptionPriceMock1.getPriceType()).thenReturn(PriceDataType.FROM);
		when(productOptionPriceMock2.getPriceType()).thenReturn(PriceDataType.FROM);
		when(productPrice.getValue()).thenReturn(BigDecimal.valueOf(price));

		when(categoryMatrixElementMock1.getElements()).thenReturn(Arrays.asList(productMatrixElementMock1));
		when(categoryMatrixElementMock2.getElements()).thenReturn(Arrays.asList(productMatrixElementMock2));
	}

	@Test
	public void shouldPopulateStudentPriceForVariants()
	{
		// Given
		ProductData productData = new ProductData();
		productData.setVariantMatrix(Arrays.asList(categoryMatrixElementMock1, categoryMatrixElementMock2));

		// we return student discount
		when(wileyProductDiscountServiceMock.getPotentialDiscountForCurrentCustomer(productModelMock, UserDiscountGroup.STUDENT))
		.thenReturn(
				Arrays.asList(discountValueMock));

		// When
		productStudentDiscountPopulator.populate(productModelMock, productData);

		// Then
		verify(productVariantOptionMock1).setStudentPrice(eq(productOptionStudentPriceMock));
		verify(productVariantOptionMock2).setStudentPrice(eq(productOptionStudentPriceMock));
	}

	@Test
	public void shouldPopulateStudentPriceForSingleProduct()
	{
		// Given
		ProductData productData = new ProductData();
		productData.setPrice(productPrice);

		// we return student discount
		when(wileyProductDiscountServiceMock.getPotentialDiscountForCurrentCustomer(productModelMock, UserDiscountGroup.STUDENT))
		.thenReturn(
				Arrays.asList(discountValueMock));

		// When
		productStudentDiscountPopulator.populate(productModelMock, productData);

		// Then
		assertSame("Student price should be populated by expected object.", productOptionStudentPriceMock,
				productData.getStudentPrice());
	}

	@Test
	public void shouldNotPopulateStudentPriceIfDiscountIsAbsent()
	{
		// Given
		ProductData productData = new ProductData();
		productData.setVariantMatrix(Arrays.asList(categoryMatrixElementMock1, categoryMatrixElementMock2));
		productData.setPrice(productPrice);

		// we don't return student discount
		when(wileyProductDiscountServiceMock.getPotentialDiscountForCurrentCustomer(productModelMock, UserDiscountGroup.STUDENT))
		.thenReturn(
				null);

		// When
		productStudentDiscountPopulator.populate(productModelMock, productData);

		// Then
		verify(productVariantOptionMock1, never()).setStudentPrice(any());
		verify(productVariantOptionMock2, never()).setStudentPrice(any());
		assertNull("Should not populate student price if discount is absent.", productData.getStudentPrice());
	}
}