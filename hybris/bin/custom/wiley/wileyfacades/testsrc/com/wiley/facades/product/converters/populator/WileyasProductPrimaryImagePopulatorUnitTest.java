package com.wiley.facades.product.converters.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ImageDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.variants.model.VariantProductModel;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.facades.as.product.converters.populator.WileyasProductPrimaryImagePopulator;

import static junit.framework.Assert.assertEquals;


/**
 * Created by Sergiy_Mishkovets on 2/21/2018.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyasProductPrimaryImagePopulatorUnitTest
{
	private static final String IMAGE_FORMAT = "thumbnail";
	private static final ImageDataType IMAGE_DATA_TYPE = ImageDataType.PRIMARY;

	@InjectMocks
	private WileyasProductPrimaryImagePopulator testInstance;

	@Mock(name = "imageConverter")
	private Converter<MediaModel, ImageData> imageConverterMock;

	@Mock
	private ProductModel productModelMock;
	@Mock
	private VariantProductModel variantProductModelMock;
	@Mock
	private MediaModel mediaModelMock;

	private ProductData productData;
	private ImageData imageData;

	@Before
	public void setUp()
	{
		productData = new ProductData();
		imageData = new ImageData();

		when(productModelMock.getPicture()).thenReturn(mediaModelMock);
		when(variantProductModelMock.getBaseProduct()).thenReturn(productModelMock);
		when(imageConverterMock.convert(mediaModelMock)).thenReturn(imageData);
	}

	@Test
	public void shouldPopulateImage()
	{
		testInstance.populate(productModelMock, productData);

		verifyProductData(productData);
	}

	@Test
	public void shouldNotPopulateImageIfPictureIsNull()
	{
		when(productModelMock.getPicture()).thenReturn(null);

		testInstance.populate(productModelMock, productData);

		assertEquals(0, productData.getImages().size());
	}

	@Test
	public void shouldUsePictureFromBaseProduct()
	{
		testInstance.populate(variantProductModelMock, productData);

		verifyProductData(productData);
	}

	private void verifyProductData(final ProductData productData)
	{
		assertEquals(1, productData.getImages().size());
		final ImageData imageData = productData.getImages().iterator().next();
		assertEquals(IMAGE_DATA_TYPE, imageData.getImageType());
	}
}
