package com.wiley.facades.wileyb2c.order.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.SchoolModel;
import com.wiley.core.wileycom.customer.service.WileycomCustomerAccountService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cCartFacadeImplTest
{
	private static final String SCHOOL_UID = "school-uid";

	@InjectMocks
	private Wileyb2cCartFacadeImpl wileyb2cCartFacade;

	@Mock
	private WileycomCustomerAccountService wileycomCustomerAccountServiceMock;

	@Mock
	private CartService cartServiceMock;

	@Mock
	private CartModel cartModelMock;

	@Mock
	private ModelService modelServiceMock;

	@Mock
	private AbstractOrderEntryModel firstAbstractOrderEntryModelMock;

	@Mock
	private AbstractOrderEntryModel secondAbstractOrderEntryModelMock;

	@Mock
	private ProductModel textbookProductModelMock;

	@Mock
	private ProductModel nonTextbookProductModelMock;

	@Mock
	private SchoolModel schoolModelMock;

	@Before
	public void setup()
	{
		when(cartServiceMock.getSessionCart()).thenReturn(cartModelMock);
		when(cartServiceMock.hasSessionCart()).thenReturn(true);
		when(cartModelMock.getEntries()).thenReturn(
				Arrays.asList(firstAbstractOrderEntryModelMock, secondAbstractOrderEntryModelMock));
		when(firstAbstractOrderEntryModelMock.getProduct()).thenReturn(nonTextbookProductModelMock);
		when(nonTextbookProductModelMock.getTextbook()).thenReturn(false);
		when(textbookProductModelMock.getTextbook()).thenReturn(true);
	}

	@Test
	public void shouldIsEducationalCartReturnFalseIfNoTextbookFound()
	{
		// Given
		when(secondAbstractOrderEntryModelMock.getProduct()).thenReturn(nonTextbookProductModelMock);

		// When
		boolean isEducationalCart = wileyb2cCartFacade.isEducationalCart();

		// Then
		assertFalse("This cart does not hold any text book products, therefore it is not educational!", isEducationalCart);
	}

	@Test
	public void shouldIsEducationalCartReturnTrueIfAtLeastOneTextbookFound()
	{
		// Given
		when(secondAbstractOrderEntryModelMock.getProduct()).thenReturn(textbookProductModelMock);

		// When
		boolean isEducationalCart = wileyb2cCartFacade.isEducationalCart();

		// Then
		assertTrue("This cart has one text book product, therefore it is educational!", isEducationalCart);
	}

	@Test
	public void shouldNotPersistSchoolInformationInCurrentOrderIfSchoolNotFound()
	{
		// Given
		when(wileycomCustomerAccountServiceMock.getSchoolByCode(SCHOOL_UID)).thenReturn(Optional.empty());

		// When
		wileyb2cCartFacade.persistSchoolInformationInCurrentOrder(SCHOOL_UID);

		// Then
		verify(cartModelMock, never()).setSchool(isA(SchoolModel.class));
		verify(modelServiceMock, never()).save(isA(AbstractOrderModel.class));
	}

	@Test
	public void shouldPersistSchoolInformationInCurrentOrderIfSchoolIsFound()
	{
		// Given
		when(wileycomCustomerAccountServiceMock.getSchoolByCode(SCHOOL_UID)).thenReturn(Optional.of(schoolModelMock));

		// When
		wileyb2cCartFacade.persistSchoolInformationInCurrentOrder(SCHOOL_UID);

		// Then
		verify(cartModelMock).setSchool(schoolModelMock);
		verify(modelServiceMock).save(cartModelMock);
	}

	@Test
	public void testIsEducationalCartWhenNoSessionCart()
	{
		// Given
		when(cartServiceMock.hasSessionCart()).thenReturn(false);

		// When
		final boolean educationalCart = wileyb2cCartFacade.isEducationalCart();

		// Then
		assertFalse(educationalCart);
		verify(cartServiceMock, never()).getSessionCart();
	}
}
