package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyPurchaseOptionProductModel;
import com.wiley.facades.product.data.MediaData;
import com.wiley.facades.wileyb2c.product.converters.populators.helper.Wileyb2cProductCollectionAttributesHelper;


/**
 * Created by Uladzimir_Barouski on 2/20/2017.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cProductExcerptsPopulatorUnitTest
{
	public static final String EXCERPT_1_TITLE = "excerpt 1";
	public static final String EXCERPT_1_LINK = "excerpt 1 link";
	public static final String EXCERPT_2_TITLE = "excerpt 2";
	public static final String EXCERPT_2_LINK = "excerpt 2 link";
	@InjectMocks
	private Wileyb2cProductExcerptsPopulator wileyb2cProductExcerptsPopulator;

	@Mock
	private Wileyb2cProductCollectionAttributesHelper wileyb2cProductCollectionAttributesHelper;

	@Mock
	private WileyPurchaseOptionProductModel variantProduct;

	@Mock
	private MediaContainerModel excerptModel1, excerptModel2;

	@Mock
	private MediaModel media1, media2;

	@Before
	public void setUp() throws Exception
	{
		when(excerptModel1.getMedias()).thenReturn(Arrays.asList(media1));
		when(media1.getURL()).thenReturn(EXCERPT_1_LINK);
		when(excerptModel2.getName()).thenReturn(EXCERPT_2_TITLE);
	}

	@Test
	public void populateWhenExcerptsContainsMedia() throws Exception
	{
		//Given
		when(excerptModel1.getName()).thenReturn(EXCERPT_1_TITLE);
		when(excerptModel2.getMedias()).thenReturn(Arrays.asList(media2));
		when(media2.getURL()).thenReturn(EXCERPT_2_LINK);
		when(wileyb2cProductCollectionAttributesHelper.getProductAttribute(variantProduct, ProductModel.EXCERPTS)).thenReturn(
				Arrays.asList(excerptModel1, excerptModel2));
		ProductData productData = new ProductData();
		//When
		wileyb2cProductExcerptsPopulator.populate(variantProduct, productData);
		//Then
		List<MediaData> excerpts = productData.getExcerpts();
		assertNotNull(excerpts);
		assertEquals(2, excerpts.size());
		assertEquals(EXCERPT_1_TITLE, excerpts.get(0).getName());
		assertEquals(EXCERPT_1_LINK, excerpts.get(0).getUrl());
		assertEquals(EXCERPT_2_TITLE, excerpts.get(1).getName());
		assertEquals(EXCERPT_2_LINK, excerpts.get(1).getUrl());
	}

	@Test
	public void populateWhenExcerptMediaMissed() throws Exception
	{
		//Given
		ProductData productData = new ProductData();
		//When
		wileyb2cProductExcerptsPopulator.populate(variantProduct, productData);
		//Then
		List<MediaData> excerpts = productData.getExcerpts();
		assertNull(excerpts);
	}
}