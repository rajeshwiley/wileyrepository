package com.wiley.facades.wileyb2b.user;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2bacceleratorservices.company.B2BCommerceUserService;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.customer.strategy.WileyCustomerRegistrationStrategy;
import com.wiley.core.users.strategy.WileycomChangeUserUidStrategy;
import com.wiley.core.wileyb2b.service.WileyB2BCommerceUserService;
import com.wiley.core.wileycom.customer.service.WileycomCustomerAccountService;
import com.wiley.core.wileycom.customer.service.WileycomCustomerService;
import com.wiley.facades.wileyb2b.customer.populators.Wileyb2bCustomerEditReversePopulator;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2BUserFacadeUnitTest
{
	private static final String FIRST_NAME = "first-name";
	private static final String LAST_NAME = "last-name";
	private static final String CUSTOMER_UID = "customer-uid";
	private static final String PASSWORD = "password";
	private static final String EMAIL = "email@test.com";
	private static final String CUSTOMER_ID = "customer-id";

	@InjectMocks
	private Wileyb2bUserFacade wileyb2BUserFacade;

	@Mock
	private ModelService modelService;

	@Mock
	private WileycomCustomerService wileycomCustomerService;

	@Mock
	private UserService userService;

	@Mock
	private WileycomChangeUserUidStrategy changeCustomerUidStrategy;

	@Mock
	private WileyCustomerRegistrationStrategy wileyCustomerRegistrationStrategy;

	@Mock
	private Wileyb2bCustomerEditReversePopulator b2bCustomerEditReversePopulator;

	@Mock
	private B2BCommerceUserService b2BCommerceUserService;

	@Mock
	private CustomerData customerDataMock;

	@Mock
	private B2BCustomerModel customerModelMock;

	@Mock
	private B2BUnitModel b2BUnitModelMock;

	@Mock
	private WileycomCustomerAccountService wileycomCustomerAccountServiceMock;

	@Mock
	private WileyB2BCommerceUserService wileyB2BCommerceUserService;

	@Before
	public void setUp()
	{
		when(userService.getUserForUID(CUSTOMER_UID, B2BCustomerModel.class)).thenReturn(customerModelMock);
		when(modelService.create(B2BCustomerModel.class)).thenReturn(customerModelMock);
	}

	@Test
	public void shouldEnableCustomerDoNothingIfAlreadyActive() throws DuplicateUidException
	{
		setupCustomerModel(false);
		setupCustomerData(false);

		wileyb2BUserFacade.enableCustomer(CUSTOMER_UID);

		verify(b2BCommerceUserService, never()).enableCustomer(CUSTOMER_UID);
		verify(b2BCommerceUserService, never()).disableCustomer(CUSTOMER_UID);
	}

	@Test
	public void shouldEnableCustomerWhenNotActive() throws DuplicateUidException
	{
		setupCustomerModel(false);
		setupCustomerData(false);
		when(customerModelMock.getActive()).thenReturn(false);

		wileyb2BUserFacade.enableCustomer(CUSTOMER_UID);

		verify(b2BCommerceUserService).enableCustomer(CUSTOMER_UID);
		verify(b2BCommerceUserService, never()).disableCustomer(CUSTOMER_UID);
	}

	@Test
	public void shouldDisableCustomerWhenActive() throws DuplicateUidException
	{
		setupCustomerModel(false);
		setupCustomerData(false);

		wileyb2BUserFacade.disableCustomer(CUSTOMER_UID);

		verify(b2BCommerceUserService).disableCustomer(CUSTOMER_UID);
		verify(b2BCommerceUserService, never()).enableCustomer(CUSTOMER_UID);
	}

	@Test
	public void shouldDisableCustomerDoNothingWhenNotActive() throws DuplicateUidException
	{
		setupCustomerModel(false);
		setupCustomerData(false);
		when(customerModelMock.getActive()).thenReturn(false);

		wileyb2BUserFacade.disableCustomer(CUSTOMER_UID);

		verify(b2BCommerceUserService, never()).disableCustomer(CUSTOMER_UID);
		verify(b2BCommerceUserService, never()).enableCustomer(CUSTOMER_UID);
	}

	@Test
	public void resetCustomerPasswordShouldFollowHappyPath()
	{
		// When
		wileyb2BUserFacade.resetCustomerPassword(CUSTOMER_UID, PASSWORD);

		// Then
		verify(wileycomCustomerAccountServiceMock).externalResetCustomerPassword(CUSTOMER_UID, PASSWORD);
	}

	private CustomerData setupCustomerData(final boolean isNew)
	{
		when(customerDataMock.getFirstName()).thenReturn(FIRST_NAME);
		when(customerDataMock.getLastName()).thenReturn(LAST_NAME);
		when(customerDataMock.getEmail()).thenReturn(EMAIL);
		if (!isNew)
		{
			when(customerDataMock.getUid()).thenReturn(CUSTOMER_UID);
		}
		return customerDataMock;
	}

	private B2BCustomerModel setupCustomerModel(final boolean isNew)
	{
		when(customerModelMock.getFirstName()).thenReturn(FIRST_NAME);
		when(customerModelMock.getActive()).thenReturn(true);
		when(customerModelMock.getLastName()).thenReturn(LAST_NAME);
		when(customerModelMock.getName()).thenReturn(FIRST_NAME + ' ' + LAST_NAME);
		when(customerModelMock.getEmail()).thenReturn(EMAIL);
		when(customerModelMock.getDefaultB2BUnit()).thenReturn(b2BUnitModelMock);
		if (!isNew)
		{
			when(customerModelMock.getUid()).thenReturn(CUSTOMER_UID);
			when(customerModelMock.getCustomerID()).thenReturn(CUSTOMER_ID);
		}
		return customerModelMock;
	}
}
