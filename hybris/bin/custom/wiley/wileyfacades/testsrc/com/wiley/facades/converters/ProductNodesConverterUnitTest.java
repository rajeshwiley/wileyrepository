package com.wiley.facades.converters;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.VariantCategoryData;
import de.hybris.platform.commercefacades.product.data.VariantMatrixElementData;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.commercefacades.product.data.VariantValueCategoryData;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.facades.product.node.ProductNode;
import com.wiley.facades.product.node.ProductRootNode;

import static java.util.Objects.nonNull;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


/**
 * Default unit test for {@link ProductNodesConverter}.
 *
 * @author Aliaksei_Zlobich
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ProductNodesConverterUnitTest
{

	@Mock
	private ProductFacade productFacadeMock;

	@InjectMocks
	private ProductNodesConverter productNodesConverter;

	// Test data

	@Mock
	private ProductData baseProductDataMock;

	@Mock
	private VariantMatrixElementData categoryMatrixOptionMock1;

	@Mock
	private VariantValueCategoryData variantValueCategoryDataMock1;

	@Mock
	private VariantMatrixElementData categoryMatrixOptionMock2;

	@Mock
	private VariantValueCategoryData variantValueCategoryDataMock2;

	@Mock
	private VariantCategoryData variantCategoryDataMock;

	@Mock
	private VariantMatrixElementData productMatrixOptionMock1;

	@Mock
	private VariantOptionData variantOptionDataMock1;

	@Mock
	private VariantOptionData variantOptionDataMock2;

	@Mock
	private VariantMatrixElementData productMatrixOptionMock2;



	@Before
	public void setUp() throws Exception
	{
		when(categoryMatrixOptionMock1.getVariantValueCategory()).thenReturn(variantValueCategoryDataMock1);
		when(categoryMatrixOptionMock2.getVariantValueCategory()).thenReturn(variantValueCategoryDataMock2);

		when(categoryMatrixOptionMock1.getParentVariantCategory()).thenReturn(variantCategoryDataMock);
		when(categoryMatrixOptionMock2.getParentVariantCategory()).thenReturn(variantCategoryDataMock);

		when(productMatrixOptionMock1.getVariantValueCategory()).thenReturn(variantValueCategoryDataMock1);
		when(productMatrixOptionMock2.getVariantValueCategory()).thenReturn(variantValueCategoryDataMock2);

		when(productMatrixOptionMock1.getParentVariantCategory()).thenReturn(variantCategoryDataMock);
		when(productMatrixOptionMock2.getParentVariantCategory()).thenReturn(variantCategoryDataMock);

		when(productMatrixOptionMock1.getIsLeaf()).thenReturn(true);
		when(productMatrixOptionMock2.getIsLeaf()).thenReturn(true);

		when(productMatrixOptionMock1.getVariantOption()).thenReturn(variantOptionDataMock1);
		when(productMatrixOptionMock2.getVariantOption()).thenReturn(variantOptionDataMock2);
	}

	@Test
	public void shouldNotShowVariantValueCategoryOptionIfOnlyOneOptionIsAvailable()
	{
		// Given
		// product has variants only from one variantValue category

		// only one category
		when(baseProductDataMock.getVariantMatrix()).thenReturn(Arrays.asList(categoryMatrixOptionMock1));

		// many variant from one category
		when(categoryMatrixOptionMock1.getElements()).thenReturn(Arrays.asList(productMatrixOptionMock1,
				productMatrixOptionMock2));

		// When
		final ProductRootNode productRootNode = productNodesConverter.convert(baseProductDataMock);

		// Then
		assertNotNull(productRootNode);
		final ProductNode product = productRootNode.getProduct();
		final List<ProductNode> firstLevelOfVariantMatrix = product.getNodes();

		assertFalse("Expected that the first level of variant matrix is invisible.", areProductNodesVisible(
				firstLevelOfVariantMatrix));
	}

	@Test
	public void shouldShowVariantValueCategoryOptionIfMoreThenOneOptionAreAvailable()
	{
		// Given
		// product has two variants in separate variantValue categories

		// two variantValue categories
		when(baseProductDataMock.getVariantMatrix()).thenReturn(Arrays.asList(categoryMatrixOptionMock1,
				categoryMatrixOptionMock2));

		// two product variants for each variantValue category
		when(categoryMatrixOptionMock1.getElements()).thenReturn(Arrays.asList(productMatrixOptionMock1));
		when(categoryMatrixOptionMock2.getElements()).thenReturn(Arrays.asList(productMatrixOptionMock2));

		// When
		final ProductRootNode productRootNode = productNodesConverter.convert(baseProductDataMock);

		// Then
		assertNotNull(productRootNode);
		final ProductNode product = productRootNode.getProduct();
		final List<ProductNode> firstLevelOfVariantMatrix = product.getNodes();

		assertTrue("Expected that the first level of variant matrix is visible.", areProductNodesVisible(
				firstLevelOfVariantMatrix));
	}

	@Test
	public void shouldShowVariantProductsIfOnlyOneOptionIsAvailable()
	{
		// Given
		// product has only one variant product

		// only one variantValue category
		when(baseProductDataMock.getVariantMatrix()).thenReturn(Arrays.asList(categoryMatrixOptionMock1));

		// and only one product variant
		when(categoryMatrixOptionMock1.getElements()).thenReturn(Arrays.asList(productMatrixOptionMock1));

		// When
		final ProductRootNode productRootNode = productNodesConverter.convert(baseProductDataMock);

		// Then
		assertNotNull(productRootNode);
		final ProductNode product = productRootNode.getProduct();
		final List<ProductNode> firstLevelOfVariantMatrix = product.getNodes();
		final List<ProductNode> productVariants = extractNextLevelNodes(firstLevelOfVariantMatrix);

		assertTrue("Expected that product variants are visible.", areProductNodesVisible(productVariants));
	}

	@Test
	public void shouldShowVariantProductsIfMoreThanOneOptionAreAvailable()
	{
		// Given
		// product has few product variants which are in one variantValue category

		// one variantValue category
		when(baseProductDataMock.getVariantMatrix()).thenReturn(Arrays.asList(categoryMatrixOptionMock1));

		// and two product variants
		when(categoryMatrixOptionMock1.getElements()).thenReturn(Arrays.asList(productMatrixOptionMock1,
				productMatrixOptionMock2));

		// When
		final ProductRootNode productRootNode = productNodesConverter.convert(baseProductDataMock);

		// Then
		assertNotNull(productRootNode);
		final ProductNode product = productRootNode.getProduct();
		final List<ProductNode> firstLevelOfVariantMatrix = product.getNodes();
		final List<ProductNode> productVariants = extractNextLevelNodes(firstLevelOfVariantMatrix);

		assertTrue("Expected that product variants are visible.", areProductNodesVisible(productVariants));
	}

	@Test
	public void shouldPopulateStudentPrices()
	{
		// Given
		when(baseProductDataMock.getVariantMatrix()).thenReturn(Arrays.asList(categoryMatrixOptionMock1));

		// set up student price object
		PriceData studentPriceDataMock = mock(PriceData.class);
		final Double studentPrice = 150.00;
		when(studentPriceDataMock.getValue()).thenReturn(BigDecimal.valueOf(studentPrice));

		// set up product option 1
		VariantOptionData productVariantOptionMock1 = mock(VariantOptionData.class);
		when(productVariantOptionMock1.getStudentPrice()).thenReturn(studentPriceDataMock);
		when(productMatrixOptionMock1.getVariantOption()).thenReturn(productVariantOptionMock1);

		// set up product option 2
		VariantOptionData productVariantOptionMock2 = mock(VariantOptionData.class);
		when(productVariantOptionMock2.getStudentPrice()).thenReturn(studentPriceDataMock);
		when(productMatrixOptionMock2.getVariantOption()).thenReturn(productVariantOptionMock2);

		when(categoryMatrixOptionMock1.getElements()).thenReturn(Arrays.asList(productMatrixOptionMock1,
				productMatrixOptionMock2));

		// When
		final ProductRootNode productRootNode = productNodesConverter.convert(baseProductDataMock);

		// Then
		assertNotNull(productRootNode);
		final ProductNode product = productRootNode.getProduct();
		final List<ProductNode> firstLevelOfVariantMatrix = product.getNodes();
		final List<ProductNode> productVariants = extractNextLevelNodes(firstLevelOfVariantMatrix);

		assertTrue("Expected that all product variants have expected student price.",
				doProductVariantsContaintDiscountedPrice(productVariants, studentPrice));
	}
	
	@Test
	public void shouldPopulatePartnerPrices()
	{
		// Given
		when(baseProductDataMock.getVariantMatrix()).thenReturn(Arrays.asList(categoryMatrixOptionMock1));

		// set up student price object
		PriceData partnerPriceDataMock = mock(PriceData.class);
		final Double partnerPrice = 150.00;
		when(partnerPriceDataMock.getValue()).thenReturn(BigDecimal.valueOf(partnerPrice));

		// set up product option 1
		VariantOptionData productVariantOptionMock1 = mock(VariantOptionData.class);
		when(productVariantOptionMock1.getStudentPrice()).thenReturn(partnerPriceDataMock);
		when(productMatrixOptionMock1.getVariantOption()).thenReturn(productVariantOptionMock1);

		// set up product option 2
		VariantOptionData productVariantOptionMock2 = mock(VariantOptionData.class);
		when(productVariantOptionMock2.getStudentPrice()).thenReturn(partnerPriceDataMock);
		when(productMatrixOptionMock2.getVariantOption()).thenReturn(productVariantOptionMock2);

		when(categoryMatrixOptionMock1.getElements()).thenReturn(Arrays.asList(productMatrixOptionMock1,
				productMatrixOptionMock2));

		// When
		final ProductRootNode productRootNode = productNodesConverter.convert(baseProductDataMock);

		// Then
		assertNotNull(productRootNode);
		final ProductNode product = productRootNode.getProduct();
		final List<ProductNode> firstLevelOfVariantMatrix = product.getNodes();
		final List<ProductNode> productVariants = extractNextLevelNodes(firstLevelOfVariantMatrix);

		assertTrue("Expected that all product variants have expected partner price.",
				doProductVariantsContaintDiscountedPrice(productVariants, partnerPrice));
	}

	private boolean doProductVariantsContaintDiscountedPrice(final List<ProductNode> productVariants, final Double priceValue)
	{
		return productVariants.stream().allMatch(
				productNode -> nonNull(productNode.getValue()) && priceValue.equals(productNode.getValue().getStudentPrice()));
	}

	private List<ProductNode> extractNextLevelNodes(final List<ProductNode> productNodes)
	{
		return productNodes.stream()
				.map(ProductNode::getNodes)
				.flatMap(Collection::stream)
				.collect(Collectors.toList());
	}

	private boolean areProductNodesVisible(final List<ProductNode> productNodes)
	{
		return productNodes.stream()
				.allMatch(node -> !ProductNodesConverter.NODE_VIEW_TYPE_HIDDEN.equals(node.getViewType()));
	}

}