package com.wiley.facades.wileyb2c.search.converters.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Created by Raman_Hancharou on 6/14/2017.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cSearchResultRelatedProductsDisclaimerPopulatorUnitTest
{
	public static final String HAS_RELATED_PRODUCTS = "hasRelatedProducts";

	@InjectMocks
	private Wileyb2cSearchResultRelatedProductsDisclaimerPopulator wileyb2cSearchResultRelatedProductsDisclaimerPopulator;

	@Mock
	private SearchResultValueData searchResultValueData;
	@Mock
	private SearchResultValueData variantSearchResultValueData;
	@Mock
	private Map<String, Object> values;


	@Test
	public void populateWhenSearchResultHasNotVariants() throws Exception
	{
		//Given
		when(searchResultValueData.getVariants()).thenReturn(null);
		ProductData productData = new ProductData();

		//When
		wileyb2cSearchResultRelatedProductsDisclaimerPopulator.populate(searchResultValueData, productData);

		//Then
		assertNull(productData.getShowRelatedProductsDisclaimer());
	}

	@Test
	public void populateWhenSearchResultHasVariantsWithoutRelatedProducts() throws Exception
	{
		//Given
		when(searchResultValueData.getVariants()).thenReturn(Collections.singletonList(variantSearchResultValueData));
		ProductData productData = new ProductData();
		when(variantSearchResultValueData.getValues()).thenReturn(values);
		when(MapUtils.isNotEmpty(values)).thenReturn(false);
		wileyb2cSearchResultRelatedProductsDisclaimerPopulator.setHasRelatedProductsProperty(HAS_RELATED_PRODUCTS);
		when(values.get(HAS_RELATED_PRODUCTS)).thenReturn(Boolean.FALSE);

		//When
		wileyb2cSearchResultRelatedProductsDisclaimerPopulator.populate(searchResultValueData, productData);

		//Then
		assertFalse(productData.getShowRelatedProductsDisclaimer());
	}

	@Test
	public void populateWhenSearchResultHasVariantWithRelatedProduct() throws Exception
	{
		//Given
		when(searchResultValueData.getVariants()).thenReturn(Collections.singletonList(variantSearchResultValueData));
		ProductData productData = new ProductData();
		when(variantSearchResultValueData.getValues()).thenReturn(values);
		when(MapUtils.isNotEmpty(values)).thenReturn(false);
		wileyb2cSearchResultRelatedProductsDisclaimerPopulator.setHasRelatedProductsProperty(HAS_RELATED_PRODUCTS);
		when(values.get(HAS_RELATED_PRODUCTS)).thenReturn(Boolean.TRUE);

		//When
		wileyb2cSearchResultRelatedProductsDisclaimerPopulator.populate(searchResultValueData, productData);

		//Then
		assertTrue(productData.getShowRelatedProductsDisclaimer());
	}
}
