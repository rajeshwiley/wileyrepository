package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.catalog.references.ProductReferenceService;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.Collections;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyPurchaseOptionProductModel;


/**
 * Created by Raman_Hancharou on 6/14/2017.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cShowRelatedProductsDisclaimerPopulatorUnitTest
{
	@Mock
	private ProductReferenceService productReferenceService;

	@Mock
	private ProductModel productMock;
	@Mock
	private ProductReferenceModel productReferenceMock;
	@Mock
	private WileyPurchaseOptionProductModel purchaseOptionMock;

	@InjectMocks
	private Wileyb2cShowRelatedProductsDisclaimerPopulator wileyb2cShowRelatedProductsDisclaimerPopulator;

	@Test
	public void populateWhenProductHasNotVariants() throws Exception
	{
		//Given
		when(productMock.getVariants()).thenReturn(Collections.EMPTY_LIST);
		ProductData productData = new ProductData();
		//When
		wileyb2cShowRelatedProductsDisclaimerPopulator.populate(productMock, productData);
		//Then
		assertFalse(productData.getShowRelatedProductsDisclaimer());
	}

	@Test
	public void populateWhenVariantHasRelatedProducts() throws Exception
	{
		//Given
		when(productMock.getVariants()).thenReturn(Collections.singletonList(purchaseOptionMock));
		when(productReferenceService
				.getProductReferencesForSourceProduct(purchaseOptionMock, ProductReferenceTypeEnum.RELATED, Boolean.TRUE))
				.thenReturn(Collections.singletonList(productReferenceMock));
		ProductData productData = new ProductData();
		//When
		wileyb2cShowRelatedProductsDisclaimerPopulator.populate(productMock, productData);
		//Then
		assertTrue(productData.getShowRelatedProductsDisclaimer());
	}

	@Test
	public void populateWhenVariantHasNotRelatedProducts() throws Exception
	{
		//Given
		when(productMock.getVariants()).thenReturn(Collections.singletonList(purchaseOptionMock));
		when(productReferenceService
				.getProductReferencesForSourceProduct(purchaseOptionMock, ProductReferenceTypeEnum.RELATED, Boolean.TRUE))
				.thenReturn(Collections.EMPTY_LIST);
		ProductData productData = new ProductData();
		//When
		wileyb2cShowRelatedProductsDisclaimerPopulator.populate(productMock, productData);
		//Then
		assertFalse(productData.getShowRelatedProductsDisclaimer());
	}
}
