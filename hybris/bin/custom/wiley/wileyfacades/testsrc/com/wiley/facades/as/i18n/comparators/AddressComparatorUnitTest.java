package com.wiley.facades.as.i18n.comparators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.RegionData;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.util.Comparator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AddressComparatorUnitTest
{
	public static final String FIRST_NAME = "first name";
	public static final String LAST_NAME = "last name";
	public static final String OTHER = "other";
	public static final String LINE_1 = "line 1";
	public static final String LINE_2 = "line 2";
	public static final String TOWN = "town";
	public static final String POSTAL_CODE = "postal code";
	public static final String PHONE = "phone";
	public static final String EMAIL = "email";

	@Mock
	CountryData countryDataMock;

	@Mock
	CountryData countryDataMockOther;

	@Mock
	RegionData regionDataMock;

	@Mock
	RegionData regionDataMockOther;

	@Mock
	AddressData addressDataMock1;

	@Mock
	AddressData addressDataMock2;

	@Mock
	private Comparator<CountryData> countryDataComparatorMock;

	@Mock
	private Comparator<RegionData> regionDataComparatorMock;

	@InjectMocks
	private AddressComparator testInstance;

	@Before
	public void setUp()
	{
		when(countryDataComparatorMock.compare(any(), any())).thenReturn(0);
		when(regionDataComparatorMock.compare(any(), any())).thenReturn(0);
		setUpAddressMock(addressDataMock1);
		setUpAddressMock(addressDataMock2);
		testInstance.setCountryDataComparator(countryDataComparatorMock);
		testInstance.setRegionDataComparator(regionDataComparatorMock);
		testInstance.afterPropertiesSet();
	}

	private void setUpAddressMock(final AddressData addressDataMock)
	{
		when(addressDataMock.getCountry()).thenReturn(countryDataMock);
		when(addressDataMock.getRegion()).thenReturn(regionDataMock);
		when(addressDataMock.getFirstName()).thenReturn(FIRST_NAME);
		when(addressDataMock.getLastName()).thenReturn(LAST_NAME);
		when(addressDataMock.getLine1()).thenReturn(LINE_1);
		when(addressDataMock.getLine2()).thenReturn(LINE_2);
		when(addressDataMock.getTown()).thenReturn(TOWN);
		when(addressDataMock.getPostalCode()).thenReturn(POSTAL_CODE);
		when(addressDataMock.getPhone()).thenReturn(PHONE);
		when(addressDataMock.getEmail()).thenReturn(EMAIL);
	}

	@Test
	public void testEqual()
	{
		int result = testInstance.compare(addressDataMock1, addressDataMock2);

		assertEquals(result, 0);
	}

	@Test
	public void testNotEqualIfCountryIsDifferent()
	{
		when(addressDataMock1.getCountry()).thenReturn(countryDataMockOther);
		when(countryDataComparatorMock.compare(any(), any())).thenReturn(1);

		int result = testInstance.compare(addressDataMock1, addressDataMock2);

		assertNotEquals(result, 0);
	}

	@Test
	public void testNotEqualIfOneCountryIsNull()
	{
		when(addressDataMock1.getCountry()).thenReturn(null);

		int result = testInstance.compare(addressDataMock1, addressDataMock2);

		assertNotEquals(result, 0);
	}

	@Test
	public void testNotEqualIfRegionIsDifferent()
	{
		when(addressDataMock1.getRegion()).thenReturn(regionDataMockOther);
		when(regionDataComparatorMock.compare(any(), any())).thenReturn(1);

		int result = testInstance.compare(addressDataMock1, addressDataMock2);

		assertNotEquals(result, 0);
	}

	@Test
	public void testNotEqualIfOneRegionIsNull()
	{
		when(addressDataMock1.getRegion()).thenReturn(null);

		int result = testInstance.compare(addressDataMock1, addressDataMock2);

		assertNotEquals(result, 0);
	}

	@Test
	public void testEqualIfBothFirstNameNull()
	{
		when(addressDataMock1.getFirstName()).thenReturn(null);
		when(addressDataMock2.getFirstName()).thenReturn(null);

		int result = testInstance.compare(addressDataMock1, addressDataMock2);

		assertEquals(result, 0);
	}

	@Test
	public void testNotEqualIfFirstNameDiffers()
	{
		when(addressDataMock1.getFirstName()).thenReturn(OTHER);

		int result = testInstance.compare(addressDataMock1, addressDataMock2);

		assertNotEquals(result, 0);
	}

	@Test
	public void testNotEqualIfOneFirstNameNull()
	{
		when(addressDataMock1.getFirstName()).thenReturn(null);

		int result = testInstance.compare(addressDataMock1, addressDataMock2);

		assertNotEquals(result, 0);
	}

	@Test
	public void testEqualIfBothLastNameNull()
	{
		when(addressDataMock1.getLastName()).thenReturn(null);
		when(addressDataMock2.getLastName()).thenReturn(null);

		int result = testInstance.compare(addressDataMock1, addressDataMock2);

		assertEquals(result, 0);
	}

	@Test
	public void testNotEqualIfLastNameDiffers()
	{
		when(addressDataMock1.getLastName()).thenReturn(OTHER);

		int result = testInstance.compare(addressDataMock1, addressDataMock2);

		assertNotEquals(result, 0);
	}

	@Test
	public void testNotEqualIfOneLastNameNull()
	{
		when(addressDataMock1.getLastName()).thenReturn(null);

		int result = testInstance.compare(addressDataMock1, addressDataMock2);

		assertNotEquals(result, 0);
	}

	@Test
	public void testEqualIfBothLine1Null()
	{
		when(addressDataMock1.getLine1()).thenReturn(null);
		when(addressDataMock2.getLine1()).thenReturn(null);

		int result = testInstance.compare(addressDataMock1, addressDataMock2);

		assertEquals(result, 0);
	}

	@Test
	public void testNotEqualIfLine1Differs()
	{
		when(addressDataMock1.getLine1()).thenReturn(OTHER);

		int result = testInstance.compare(addressDataMock1, addressDataMock2);

		assertNotEquals(result, 0);
	}

	@Test
	public void testNotEqualIfOneLine1Null()
	{
		when(addressDataMock1.getLine1()).thenReturn(null);

		int result = testInstance.compare(addressDataMock1, addressDataMock2);

		assertNotEquals(result, 0);
	}

	@Test
	public void testEqualIfBothLine2Null()
	{
		when(addressDataMock1.getLine2()).thenReturn(null);
		when(addressDataMock2.getLine2()).thenReturn(null);

		int result = testInstance.compare(addressDataMock1, addressDataMock2);

		assertEquals(result, 0);
	}

	@Test
	public void testNotEqualIfLine2Differs()
	{
		when(addressDataMock1.getLine2()).thenReturn(OTHER);

		int result = testInstance.compare(addressDataMock1, addressDataMock2);

		assertNotEquals(result, 0);
	}

	@Test
	public void testNotEqualIfOneLine2Null()
	{
		when(addressDataMock1.getLine2()).thenReturn(null);

		int result = testInstance.compare(addressDataMock1, addressDataMock2);

		assertNotEquals(result, 0);
	}

	@Test
	public void testEqualIfBothTownNull()
	{
		when(addressDataMock1.getTown()).thenReturn(null);
		when(addressDataMock2.getTown()).thenReturn(null);

		int result = testInstance.compare(addressDataMock1, addressDataMock2);

		assertEquals(result, 0);
	}

	@Test
	public void testNotEqualIfTownDiffers()
	{
		when(addressDataMock1.getTown()).thenReturn(OTHER);

		int result = testInstance.compare(addressDataMock1, addressDataMock2);

		assertNotEquals(result, 0);
	}

	@Test
	public void testNotEqualIfOneTownNull()
	{
		when(addressDataMock1.getTown()).thenReturn(null);

		int result = testInstance.compare(addressDataMock1, addressDataMock2);

		assertNotEquals(result, 0);
	}

	@Test
	public void testEqualIfBothPostalCodeNull()
	{
		when(addressDataMock1.getPostalCode()).thenReturn(null);
		when(addressDataMock2.getPostalCode()).thenReturn(null);

		int result = testInstance.compare(addressDataMock1, addressDataMock2);

		assertEquals(result, 0);
	}

	@Test
	public void testNotEqualIfPostalCodeDiffers()
	{
		when(addressDataMock1.getPostalCode()).thenReturn(OTHER);

		int result = testInstance.compare(addressDataMock1, addressDataMock2);

		assertNotEquals(result, 0);
	}

	@Test
	public void testNotEqualIfOnePostalCodeNull()
	{
		when(addressDataMock1.getPostalCode()).thenReturn(null);

		int result = testInstance.compare(addressDataMock1, addressDataMock2);

		assertNotEquals(result, 0);
	}

	@Test
	public void testEqualIfBothPhoneNull()
	{
		when(addressDataMock1.getPhone()).thenReturn(null);
		when(addressDataMock2.getPhone()).thenReturn(null);

		int result = testInstance.compare(addressDataMock1, addressDataMock2);

		assertEquals(result, 0);
	}

	@Test
	public void testNotEqualIfPhoneDiffers()
	{
		when(addressDataMock1.getPhone()).thenReturn(OTHER);

		int result = testInstance.compare(addressDataMock1, addressDataMock2);

		assertNotEquals(result, 0);
	}

	@Test
	public void testNotEqualIfOnePhoneNull()
	{
		when(addressDataMock1.getPhone()).thenReturn(null);

		int result = testInstance.compare(addressDataMock1, addressDataMock2);

		assertNotEquals(result, 0);
	}

	@Test
	public void testEqualIfBothEmailNull()
	{
		when(addressDataMock1.getEmail()).thenReturn(null);
		when(addressDataMock2.getEmail()).thenReturn(null);

		int result = testInstance.compare(addressDataMock1, addressDataMock2);

		assertEquals(result, 0);
	}

	@Test
	public void testNotEqualIfEmailDiffers()
	{
		when(addressDataMock1.getEmail()).thenReturn(OTHER);

		int result = testInstance.compare(addressDataMock1, addressDataMock2);

		assertNotEquals(result, 0);
	}

	@Test
	public void testNotEqualIfOneEmailNull()
	{
		when(addressDataMock1.getEmail()).thenReturn(null);

		int result = testInstance.compare(addressDataMock1, addressDataMock2);

		assertNotEquals(result, 0);
	}
}
