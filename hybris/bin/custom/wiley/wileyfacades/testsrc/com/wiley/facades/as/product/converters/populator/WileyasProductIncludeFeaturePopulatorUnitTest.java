package com.wiley.facades.as.product.converters.populator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.classification.features.FeatureValue;
import de.hybris.platform.commercefacades.product.data.FeatureData;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import static org.mockito.Mockito.when;
import static org.mockito.Mockito.any;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyasProductIncludeFeaturePopulatorUnitTest
{

	@Mock
	private Converter<Feature, FeatureData> featureConverter;
	@InjectMocks
	private WileyasProductIncludeFeaturePopulator prodcutIncludeFeaturePopulator;
	@Mock
	private Feature featureNullValues, featureEmptyValues, featureNotListable, featureValid;
	@Mock
	private ClassAttributeAssignmentModel classAttributeAssignmentListable, classAttributeAssignmentNotListable;
	@Mock
	private FeatureValue featureValue;
	private FeatureList features;

	@Before
	public void setUp()
	{
		List<FeatureValue> featureValues = new ArrayList<>(Arrays.asList(featureValue));
		when(classAttributeAssignmentNotListable.getListable()).thenReturn(false);
		when(classAttributeAssignmentListable.getListable()).thenReturn(true);
		when(featureConverter.convert(any(Feature.class))).thenReturn(new FeatureData());
		
		when(featureNullValues.getValues()).thenReturn(null);
		when(featureEmptyValues.getValues()).thenReturn(Collections.EMPTY_LIST);
		when(featureNotListable.getClassAttributeAssignment()).thenReturn(classAttributeAssignmentNotListable);
		when(featureValid.getValues()).thenReturn(featureValues);
		when(featureValid.getClassAttributeAssignment()).thenReturn(classAttributeAssignmentListable);
		features = new FeatureList(Arrays.asList(featureEmptyValues, featureNotListable, featureNullValues, featureValid));
		
	}

	@Test
	public void shouldReturnVisibleFeatures()
	{
		List<FeatureData> result = prodcutIncludeFeaturePopulator.buildIncludeFeaturesList(features);
		Assert.assertTrue(result.size() == 1);
	}

}
