package com.wiley.facades.wileyb2b.order.converters.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2bacceleratorfacades.order.data.B2BPaymentTypeData;
import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.comments.model.CommentModel;
import de.hybris.platform.commercefacades.comment.data.CommentData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.promotions.PromotionsService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyExternalDiscountModel;
import com.wiley.facades.product.data.DiscountData;
import com.wiley.facades.wileycom.abstractOrder.WileycomShowDeliveryCostAbstractOrderPopulator;
import com.wiley.facades.wileycom.order.converters.populators.WileycomHasOutOfStockCartPopulator;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2bCartPopulatorUnitTest
{
	private static final String USER_NOTES = "userNotes";
	private static final Date DESIRED_SHIPPING_DATE = new Date();
	private static final CheckoutPaymentType CHECKOUT_PAYMENT_TYPE = CheckoutPaymentType.ACCOUNT;
	private static final String PAYMENT_TYPE_CODE = "code";
	private static final String PAYMENT_TYPE_DISPLAY_NAME = "displayName";

	@Mock
	private WileycomShowDeliveryCostAbstractOrderPopulator wileycomShowDeliveryCostAbstractOrderPopulator;
	@Mock
	private Converter<CheckoutPaymentType, B2BPaymentTypeData> paymentTypeDataConverter;
	@Mock
	private PriceDataFactory priceDataFactory;
	@Mock
	private CurrencyModel currencyModel;
	@Mock
	private PromotionsService promotionsService;
	@Mock
	private Converter<AbstractOrderEntryModel, OrderEntryData> orderEntryConverter;
	@Mock
	private Converter<CommentModel, CommentData> orderCommentConverter;
	@Mock
	private Converter<WileyExternalDiscountModel, DiscountData> wileyb2bExternalDiscountToDiscountDataConverter;
	@Mock
	private WileycomHasOutOfStockCartPopulator wileycomHasOutOfStockCartPopulator;
	@Mock
	private CartModel source;
	@InjectMocks
	private Wileyb2bCartPopulator wileyb2bCartPopulator;

	@Before
	public void setup()
	{
		when(source.getUserNotes()).thenReturn(USER_NOTES);
		when(source.getDesiredShippingDate()).thenReturn(DESIRED_SHIPPING_DATE);
		when(source.getEntries()).thenReturn(Collections.emptyList());
		when(source.getGlobalDiscountValues()).thenReturn(Collections.emptyList());
		when(source.getCurrency()).thenReturn(currencyModel);
		when(source.getTaxCalculated()).thenReturn(Boolean.FALSE);
		when(source.getSubtotal()).thenReturn(0.0);
	}

	@Test
	public void shouldSetPaymentTypeIfSourceContainsIt()
	{
		when(source.getPaymentType()).thenReturn(CHECKOUT_PAYMENT_TYPE);

		B2BPaymentTypeData b2BPaymentTypeData = new B2BPaymentTypeData();
		b2BPaymentTypeData.setCode(PAYMENT_TYPE_CODE);
		b2BPaymentTypeData.setDisplayName(PAYMENT_TYPE_DISPLAY_NAME);
		when(paymentTypeDataConverter.convert(CHECKOUT_PAYMENT_TYPE)).thenReturn(b2BPaymentTypeData);
		CartData target = new CartData();

		wileyb2bCartPopulator.populate(source, target);

		assertEquals(target.getUserNotes(), USER_NOTES);
		assertEquals(target.getDesiredShippingDate(), DESIRED_SHIPPING_DATE);
		assertEquals(target.getPaymentType(), b2BPaymentTypeData);
	}

	@Test
	public void shouldNotSetPaymentTypeIfSourceDoesNotContainIt()
	{
		CartData target = new CartData();

		wileyb2bCartPopulator.populate(source, target);

		assertEquals(target.getUserNotes(), USER_NOTES);
		assertEquals(target.getDesiredShippingDate(), DESIRED_SHIPPING_DATE);
		assertNull(target.getPaymentType());
	}

}
