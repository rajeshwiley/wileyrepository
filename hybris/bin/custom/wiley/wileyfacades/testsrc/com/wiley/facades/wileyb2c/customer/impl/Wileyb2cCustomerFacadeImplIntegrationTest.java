package com.wiley.facades.wileyb2c.customer.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.facades.wileyb2c.customer.Wileyb2cCustomerFacade;


/**
 * Created by Uladzimir_Barouski on 5/23/2016.
 */
@IntegrationTest
public class Wileyb2cCustomerFacadeImplIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	public static final String CUSTOMER_ID = "customer@test.com";
	public static final String FIRST_DIGITAL = "WCOM_B2C_DIGITAL";
	public static final String SECOND_DIGITAL = "WDIGITAL_PRODUCT";
	private static final String BASE_SITE = "wileyb2c";
	@Resource
	private Wileyb2cCustomerFacade wileyb2cCustomerFacade;

	@Resource
	private BaseSiteService baseSiteService;
	@Resource
	private UserService userService;

	@Before
	public void setUp() throws Exception
	{
		userService.setCurrentUser(userService.getUserForUID(CUSTOMER_ID));
		baseSiteService.setCurrentBaseSite(BASE_SITE, true);
	}

	@Test
	public void testMyDigitalProducts()
	{
		PageableData pageableData = new PageableData();
		pageableData.setPageSize(100);

		SearchPageData<OrderEntryData> searchPageData = wileyb2cCustomerFacade.getDigitalOrderEntriesData(pageableData);

		List<OrderEntryData> orderEntries = searchPageData.getResults();
		Assert.assertEquals(2, orderEntries.size());
		Assert.assertEquals(FIRST_DIGITAL, orderEntries.get(0).getProduct().getCode());
		Assert.assertEquals(SECOND_DIGITAL, orderEntries.get(1).getProduct().getCode());
	}
}