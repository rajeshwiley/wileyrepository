package com.wiley.facades.welags.customer.impl;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.facades.welags.customer.WelAgsCustomerFacade;
import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.PasswordEncoderService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Resource;


/**
 * Integration test suite for {@link WelAgsCustomerFacadeImpl}
 */
@IntegrationTest
public class WelAgsCustomerFacadeImplIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	private static final String TEST_FIRST_NAME = "First Name";
	private static final String TEST_LAST_NAME = "Last Name";
	private static final String TEST_CONCATENATED_NAME = TEST_FIRST_NAME + " " + TEST_LAST_NAME;
	private static final String TEST_LOGIN = "Login";
	private static final String TEST_PASSWORD = "Password";
	private static final String TEST_LANG_EN = "en";
	private static final String TEST_CURRENCY_EUR = "EUR";
	private static final String TEST_COUNTRY_ISO_US = "US";
	private static final String TEST_EMPLOYER_SCHOOL = "Test employer school";
	private static final String TEST_USER = "test@hybris.com";

	@Resource(name = "welAgsCustomerFacade", type = WelAgsCustomerFacadeImpl.class)
	private WelAgsCustomerFacade welAgsCustomerFacade;

	@Resource
	private UserService userService;

	@Resource
	private CommonI18NService commonI18NService;

	@Resource(name = "defaultPasswordEncoderService")
	private PasswordEncoderService passwordEncoderService;

	@Resource
	private ModelService modelService;
	@Resource
	private BaseSiteService baseSiteService;

	@Before
	public void setUp() throws Exception
	{
		commonI18NService.setCurrentLanguage(commonI18NService.getLanguage(TEST_LANG_EN));
		commonI18NService.setCurrentCurrency(commonI18NService.getCurrency(TEST_CURRENCY_EUR));
		baseSiteService.setCurrentBaseSite(baseSiteService.getBaseSiteForUID("wel"), true);
	}

	/**
	 * Tests that data provided during registration is stored properly into Customer.
	 *
	 * @throws DuplicateUidException
	 * 		the duplicate uid exception
	 */
	@Test
	public void testRegister() throws DuplicateUidException
	{

		final RegisterData registerData = new RegisterData();
		registerData.setFirstName(TEST_FIRST_NAME);
		registerData.setLastName(TEST_LAST_NAME);
		registerData.setLogin(TEST_LOGIN);
		registerData.setPassword(TEST_PASSWORD);

		welAgsCustomerFacade.register(registerData);

		UserModel registeredUser = userService.getUserForUID(TEST_LOGIN.toLowerCase());
		Assert.assertTrue(registeredUser instanceof CustomerModel);
		CustomerModel registeredCustomer = (CustomerModel) registeredUser;

		Assert.assertEquals(TEST_LOGIN.toLowerCase(), registeredCustomer.getUid());
		Assert.assertTrue(StringUtils.isNotBlank(registeredCustomer.getCustomerID()));
		Assert.assertTrue(passwordEncoderService.isValid(registeredCustomer, TEST_PASSWORD));

		Assert.assertEquals(TEST_CONCATENATED_NAME, registeredCustomer.getName());
		Assert.assertEquals(TEST_FIRST_NAME, registeredCustomer.getFirstName());
		Assert.assertEquals(TEST_LAST_NAME, registeredCustomer.getLastName());

		Assert.assertEquals(TEST_LANG_EN, registeredCustomer.getSessionLanguage().getIsocode());
		Assert.assertEquals(TEST_CURRENCY_EUR, registeredCustomer.getSessionCurrency().getIsocode());
	}

	/**
	 * Tests that updated fields are stored properly into Customer.
	 *
	 * @throws Exception
	 */
	@Test
	public void testUpdateCustomerCountryAndEmployerSchool() throws Exception
	{
		importCsv("/wileyfacades/test/WileyCustomerFacadeImplIntegrationTest/testUpdateCustomer.impex", DEFAULT_ENCODING);

		UserModel userModel = userService.getUserForUID(TEST_USER);
		userService.setCurrentUser(userModel);

		welAgsCustomerFacade.updateCustomerCountryAndEmployerSchoolFields(TEST_COUNTRY_ISO_US, TEST_EMPLOYER_SCHOOL);

		modelService.refresh(userModel);

		Assert.assertNotNull(userModel.getDefaultPaymentAddress());
		Assert.assertEquals(TEST_COUNTRY_ISO_US, userModel.getDefaultPaymentAddress().getCountry().getIsocode());
		Assert.assertEquals(TEST_EMPLOYER_SCHOOL, ((CustomerModel) userModel).getCurrentEmployerSchool());
	}

}
