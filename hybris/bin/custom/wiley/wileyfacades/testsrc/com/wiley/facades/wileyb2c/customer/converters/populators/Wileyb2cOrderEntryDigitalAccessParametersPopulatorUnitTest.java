package com.wiley.facades.wileyb2c.customer.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.enums.WileyDigitalContentTypeEnum;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cOrderEntryDigitalAccessParametersPopulatorUnitTest
{
	private static final String ORDER_CODE = "code";
	private static final String DIGITAL_ACCESS_TOKEN = "token";
	@InjectMocks
	private Wileyb2cOrderEntryDigitalAccessParametersPopulator populator;
	@Mock
	private AbstractOrderEntryModel source;
	@Mock
	private AbstractOrderModel order;
	private ProductModel productModel;


	@Before
	public void setUp() throws Exception
	{
		productModel = new ProductModel();
		when(source.getProduct()).thenReturn(productModel);
		when(source.getOrder()).thenReturn(order);
		when(order.getCode()).thenReturn(ORDER_CODE);
	}

	@Test
	public void populateWhenVitalSourceThenNeedAccessTokenTrue()
	{
		OrderEntryData target = new OrderEntryData();
		productModel.setDigitalContentType(WileyDigitalContentTypeEnum.VITAL_SOURCE);
		when(source.getDigitalProductAccessToken()).thenReturn(DIGITAL_ACCESS_TOKEN);

		populator.populate(source, target);

		assertTrue(target.isNeedAccessToken());
		assertTokenAndOrderCode(target, DIGITAL_ACCESS_TOKEN);
	}

	@Test
	public void populateWhenNonVitalSourceThenNeedAccessTokenFalse()
	{
		OrderEntryData target = new OrderEntryData();
		productModel.setDigitalContentType(WileyDigitalContentTypeEnum.LIGHTNING_SOURCE);
		when(source.getDigitalProductAccessToken()).thenReturn(DIGITAL_ACCESS_TOKEN);

		populator.populate(source, target);

		assertFalse(target.isNeedAccessToken());
		assertTokenAndOrderCode(target, DIGITAL_ACCESS_TOKEN);
	}

	@Test
	public void populateWhenTokenExistThenNeedAccessButtonTrue()
	{
		OrderEntryData target = new OrderEntryData();
		productModel.setDigitalContentType(WileyDigitalContentTypeEnum.VITAL_SOURCE);
		when(source.getDigitalProductAccessToken()).thenReturn(DIGITAL_ACCESS_TOKEN);

		populator.populate(source, target);

		assertTrue(target.isNeedAccessButton());
		assertTokenAndOrderCode(target, DIGITAL_ACCESS_TOKEN);
	}

	@Test
	public void populateWhenNoTokenThenNeedAccessButtonFalse()
	{
		OrderEntryData target = new OrderEntryData();

		populator.populate(source, target);

		assertFalse(target.isNeedAccessButton());
		assertTokenAndOrderCode(target, null);
	}

	private void assertTokenAndOrderCode(final OrderEntryData target, final Object actual)
	{
		assertEquals(target.getOrderCode(), ORDER_CODE);
		assertEquals(target.getDigitalAccessToken(), actual);
	}
}
