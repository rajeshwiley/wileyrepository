package com.wiley.facades.wileyb2c.product.converters.populators.helper;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.variants.model.VariantProductModel;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cProductCollectionAttributesHelperUnitTest
{
	private static final String ATTRIBUTE_NAME = "attributeValue";
	private static final List<String> ATTRIBUTE_VALUE = Collections.singletonList("testValue");
	@InjectMocks
	private Wileyb2cProductCollectionAttributesHelper wileyb2cProductCollectionAttributesHelper;
	@Mock
	private VariantProductModel variantProduct;
	@Mock
	private ProductModel baseProduct;
	@Mock
	private ModelService modelService;

	@Test
	public void getProductAttributeWhenInVariantShouldGetFromVariant()
	{
		when(modelService.getAttributeValue(variantProduct, ATTRIBUTE_NAME)).thenReturn(
				ATTRIBUTE_VALUE);

		final Object productAttribute = wileyb2cProductCollectionAttributesHelper.getProductAttribute(variantProduct,
				ATTRIBUTE_NAME);

		Assert.assertEquals(ATTRIBUTE_VALUE, productAttribute);
	}

	@Test
	public void getProductAttributeWhenInBaseShouldGetFromBase()
	{
		when(variantProduct.getBaseProduct()).thenReturn(baseProduct);
		when(modelService.getAttributeValue(baseProduct, ATTRIBUTE_NAME)).thenReturn(
				ATTRIBUTE_VALUE);

		final Object productAttribute = wileyb2cProductCollectionAttributesHelper.getProductAttribute(variantProduct,
				ATTRIBUTE_NAME);

		Assert.assertEquals(ATTRIBUTE_VALUE, productAttribute);
	}
}
