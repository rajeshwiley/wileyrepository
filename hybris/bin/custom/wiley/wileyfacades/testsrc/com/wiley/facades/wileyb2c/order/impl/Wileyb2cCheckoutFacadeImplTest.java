package com.wiley.facades.wileyb2c.order.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.isA;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.paypal.hybris.model.PaypalPaymentInfoModel;
import com.wiley.core.order.WileyCommerceCheckoutService;
import com.wiley.core.product.data.ExternalInventoryStatus;
import com.wiley.core.product.data.ExternalInventoryStatusRecord;
import com.wiley.core.wileyb2c.order.Wileyb2cExternalInventoryService;
import com.wiley.facades.wileyb2c.order.converters.populator.Wileyb2cOrderEntryExternalInventoryStatusPopulator;


/**
 * Author Herman_Chukhrai (EPAM)
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cCheckoutFacadeImplTest
{
	private static final String CART_CODE = "CART_CODE";
	private static final String PRODUCT_1_SAP_CODE = "PRODUCT_1_SAP_CODE";
	private static final String PRODUCT_2_SAP_CODE = "PRODUCT_2_SAP_CODE";

	@InjectMocks
	private Wileyb2cCheckoutFacadeImpl wileyb2cCheckoutFacade;

	@Mock
	private CartData cartDataMock;

	@Mock
	private Wileyb2cOrderEntryExternalInventoryStatusPopulator externalInventoryStatusPopulatorMock;

	@Mock
	private OrderEntryData cartEntry1Mock;

	@Mock
	private OrderEntryData cartEntry2Mock;

	@Mock
	private ProductData productModel1Mock;

	@Mock
	private ProductData productModel2Mock;

	@Mock
	private Wileyb2cExternalInventoryService wileyb2cExternalInventoryServiceMock;

	@Mock
	private CartFacade cartFacade;

	@Mock
	private CartService cartService;

	@Mock
	private CartModel cartModelMock;

	@Mock
	private PaypalPaymentInfoModel paypalPaymentInfoModelMock;

	@Mock
	private CreditCardPaymentInfoModel creditCardPaymentInfoModelMock;

	@Mock
	private WileyCommerceCheckoutService wileyCommerceCheckoutService;

	@Mock
	private ModelService modelService;

	@Mock
	private ExternalInventoryStatus inventoryStatusRecords;

	@Mock
	private ExternalInventoryStatusRecord externalInventoryStatusRecord;

	@Before
	public void setUp() throws Exception
	{
		when(cartDataMock.getEntries()).thenReturn(Arrays.asList(cartEntry1Mock, cartEntry2Mock));
		when(cartDataMock.getCode()).thenReturn(CART_CODE);

		when(cartEntry1Mock.getProduct()).thenReturn(productModel1Mock);
		when(cartEntry2Mock.getProduct()).thenReturn(productModel2Mock);


		when(productModel1Mock.getSapProductCode()).thenReturn(PRODUCT_1_SAP_CODE);
		when(productModel2Mock.getSapProductCode()).thenReturn(PRODUCT_2_SAP_CODE);

		when(externalInventoryStatusRecord.getAvailableDate()).thenReturn(new Date());
		when(externalInventoryStatusRecord.getQuantity()).thenReturn(5L);
		when(externalInventoryStatusRecord.getSortOrder()).thenReturn(1);
		when(externalInventoryStatusRecord.getStatusCode()).thenReturn(StockLevelStatus.INSTOCK);

		final ArrayList<ExternalInventoryStatusRecord> externalInventoryStatusRecordsList = new ArrayList<>();
		externalInventoryStatusRecordsList.add(externalInventoryStatusRecord);

		when(inventoryStatusRecords.getRecordsList()).thenReturn(externalInventoryStatusRecordsList);

		final HashMap<String, ExternalInventoryStatus> map = new HashMap<>();
		map.put(PRODUCT_2_SAP_CODE, inventoryStatusRecords);
		when(wileyb2cExternalInventoryServiceMock.doRealTimeInventoryCheck()).thenReturn(map);
	}

	@Test
	public void testDigitalProductsNotChanged() throws Exception
	{
		//given
		when(productModel1Mock.isIsDigital()).thenReturn(true);
		when(productModel2Mock.isIsDigital()).thenReturn(false);

		//when
		wileyb2cCheckoutFacade.doRealTimeInventoryCheck(cartDataMock);

		//then
		verify(externalInventoryStatusPopulatorMock, never()).populate(any(), eq(cartEntry1Mock));
		verify(externalInventoryStatusPopulatorMock).populate(any(), eq(cartEntry2Mock));
	}

	@Test
	public void shouldRemovePaypalPaymentInformationFromSessionCart()
	{
		when(cartFacade.hasSessionCart()).thenReturn(true);
		when(cartService.getSessionCart()).thenReturn(cartModelMock);
		when(cartModelMock.getPaymentInfo()).thenReturn(paypalPaymentInfoModelMock);

		wileyb2cCheckoutFacade.removePaypalPaymentInformationFromSessionCart();

		InOrder inOrder = Mockito.inOrder(wileyCommerceCheckoutService, modelService, cartModelMock);
		inOrder.verify(wileyCommerceCheckoutService).setPaymentAddress(isA(CommerceCheckoutParameter.class));
		inOrder.verify(modelService).remove(paypalPaymentInfoModelMock);
		inOrder.verify(modelService).refresh(cartModelMock);
	}

	@Test
	public void shouldNotRemoveANonPaypalPaymentInformationFromSessionCart()
	{
		when(cartFacade.hasSessionCart()).thenReturn(true);
		when(cartService.getSessionCart()).thenReturn(cartModelMock);
		when(cartModelMock.getPaymentInfo()).thenReturn(creditCardPaymentInfoModelMock);

		wileyb2cCheckoutFacade.removePaypalPaymentInformationFromSessionCart();

		verify(wileyCommerceCheckoutService, never()).setPaymentAddress(isA(CommerceCheckoutParameter.class));
		verify(modelService, never()).remove(any());
		verify(modelService, never()).refresh(cartModelMock);
		verify(cartModelMock, never()).setPaymentInfo(null);
		verify(modelService, never()).save(cartModelMock);
	}

	@Test
	public void shouldNotRemoveAnythingFromSessionCartIfThereIsNoPaymentInformationStoredInCart()
	{
		when(cartFacade.hasSessionCart()).thenReturn(true);
		when(cartService.getSessionCart()).thenReturn(cartModelMock);
		when(cartModelMock.getPaymentInfo()).thenReturn(null);

		wileyb2cCheckoutFacade.removePaypalPaymentInformationFromSessionCart();

		verify(wileyCommerceCheckoutService, never()).setPaymentAddress(isA(CommerceCheckoutParameter.class));
		verify(modelService, never()).remove(any());
		verify(modelService, never()).refresh(cartModelMock);
		verify(cartModelMock, never()).setPaymentInfo(null);
		verify(modelService, never()).save(cartModelMock);
	}
}
