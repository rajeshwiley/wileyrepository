package com.wiley.facades.wileyb2c.order.converters.populator;

import com.wiley.core.model.WileyProductSummaryModel;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;



/**
 * The type Wileyb 2 c course info populator unit test.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cCourseInfoPopulatorUnitTest
{

	private static final String AUTHORS = "PUSHKIN";
	private static final String IMAGE_URL = "https://my.com/image";
	private static final String TITLE = "Onegin";
	private static final String TYPE = "WileyPLUS Instant Access ONLY";

	private static final String PRODUCT_AUTHORS = "PUSHKIN";
	private static final String PRODUCT_NAME = "TOLSTOY";

	private Wileyb2cCourseInfoPopulator wileyb2cCourseInfoPopulator;

	/**
	 * Sets up.
	 */
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		wileyb2cCourseInfoPopulator = new Wileyb2cCourseInfoPopulator();
	}

	/**
	 * Test populate.
	 */
	@Test
	public void testPopulate()
	{
		AbstractOrderEntryModel orderEntryModel = mock(AbstractOrderEntryModel.class);
		WileyProductSummaryModel productSummaryModel = mock(WileyProductSummaryModel.class);

		given(productSummaryModel.getName()).willReturn(TITLE);
		given(productSummaryModel.getAuthors()).willReturn(AUTHORS);
		given(productSummaryModel.getPictureUrl()).willReturn(IMAGE_URL);
		given(productSummaryModel.getSummary()).willReturn(TYPE);
		given(orderEntryModel.getProductSummary()).willReturn(productSummaryModel);

		OrderEntryData orderEntryData = new OrderEntryData();
		orderEntryData.setProduct(new ProductData());


		wileyb2cCourseInfoPopulator.populate(orderEntryModel, orderEntryData);


		ProductData productData = orderEntryData.getProduct();

		Assert.assertEquals(AUTHORS, productData.getAuthors());
		Assert.assertEquals(IMAGE_URL, productData.getPurchaseOptionImageUrl());
		Assert.assertEquals(TITLE, productData.getName());
		Assert.assertEquals(TYPE, productData.getPurchaseOptionType());

	}


	@Test
	public void testEmptyWileyPlusCoursePopulate()
	{
		AbstractOrderEntryModel orderEntryModel = mock(AbstractOrderEntryModel.class);
		WileyProductSummaryModel productSummaryModel = mock(WileyProductSummaryModel.class);

		given(productSummaryModel.getName()).willReturn(StringUtils.EMPTY);
		given(productSummaryModel.getAuthors()).willReturn(StringUtils.EMPTY);
		given(productSummaryModel.getPictureUrl()).willReturn(StringUtils.EMPTY);
		given(productSummaryModel.getSummary()).willReturn(StringUtils.EMPTY);
		given(orderEntryModel.getProductSummary()).willReturn(productSummaryModel);

		OrderEntryData orderEntryData = new OrderEntryData();
		ProductData productData = new ProductData();
		productData.setAuthors(PRODUCT_AUTHORS);
		productData.setName(PRODUCT_NAME);
		orderEntryData.setProduct(productData);


		wileyb2cCourseInfoPopulator.populate(orderEntryModel, orderEntryData);


		ProductData resultProductData = orderEntryData.getProduct();

		Assert.assertEquals(PRODUCT_AUTHORS, resultProductData.getAuthors());
		Assert.assertEquals(null, resultProductData.getPurchaseOptionImageUrl());
		Assert.assertEquals(PRODUCT_NAME, resultProductData.getName());
		Assert.assertEquals(null, resultProductData.getPurchaseOptionType());

	}

}
