package com.wiley.facades.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.strategies.CustomerNameStrategy;
import de.hybris.platform.core.model.user.CustomerModel;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;


@UnitTest
public class WileyCustomerReversePopulatorTest
{
	public static final String TEST_NAME = "Name";
	public static final String TEST_FIRST_NAME = "First Name";
	public static final String TEST_LAST_NAME = "Last Name";

	@Mock
	private CustomerNameStrategy customerNameStrategy;

	@InjectMocks
	private WileyCustomerReversePopulator wileyCustomerReversePopulator;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Checks that 'name', 'first name' and 'last name' fields are populated directly from CustomerData.
	 */
	@Test
	public void testPopulateNames()
	{
		final CustomerData customerData = mock(CustomerData.class);
		final CustomerModel customerModel = new CustomerModel();

		given(customerData.getName()).willReturn(TEST_NAME);
		given(customerData.getFirstName()).willReturn(TEST_FIRST_NAME);
		given(customerData.getLastName()).willReturn(TEST_LAST_NAME);

		wileyCustomerReversePopulator.populate(customerData, customerModel);
		Assert.assertEquals(TEST_NAME, customerModel.getName());
		Assert.assertEquals(TEST_FIRST_NAME, customerModel.getFirstName());
		Assert.assertEquals(TEST_LAST_NAME, customerModel.getLastName());
	}
}
