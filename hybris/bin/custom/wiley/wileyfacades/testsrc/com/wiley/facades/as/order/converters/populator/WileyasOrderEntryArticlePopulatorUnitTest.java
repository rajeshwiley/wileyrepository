package com.wiley.facades.as.order.converters.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.integration.article.dto.ArticleDto;
import com.wiley.core.integration.article.service.ArticleService;
import com.wiley.facades.order.data.WileyArticleData;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyasOrderEntryArticlePopulatorUnitTest
{
	private static final String ARTICLE_ID = "articleId";

	@Mock
	private ArticleService articleService;

	@Mock
	private Converter<ArticleDto, WileyArticleData> wileyasArticleConverter;
	@Mock
	private ArticleDto articleDto;
	@Mock
	private AbstractOrderEntryModel orderEntryModel;
	@InjectMocks
	private WileyasOrderEntryArticlePopulator orderEntryArticlePopulator;

	private OrderEntryData orderEntry = new OrderEntryData();
	private WileyArticleData summaryData = new WileyArticleData();


	@Before
	public void setUp()
	{
		when(wileyasArticleConverter.convert(articleDto)).thenReturn(summaryData);
	}

	@Test
	public void shouldAddArticleWhenBusinessIdIsNotEmpty()
	{
		summaryData.setSummary(ARTICLE_ID);

		when(orderEntryModel.getBusinessItemId()).thenReturn(ARTICLE_ID);
		when(articleService.getArticleData(ARTICLE_ID)).thenReturn(Optional.of(articleDto));

		orderEntryArticlePopulator.populate(orderEntryModel, orderEntry);
		assertEquals(ARTICLE_ID, orderEntry.getArticle().getSummary());

	}

	@Test
	public void shouldNotAddArticleWhenBusinessIdIsEmpty()
	{
		when(orderEntryModel.getBusinessItemId()).thenReturn(null);
		when(articleService.getArticleData(ARTICLE_ID)).thenReturn(Optional.of(articleDto));

		orderEntryArticlePopulator.populate(orderEntryModel, orderEntry);
		assertNull(orderEntry.getArticle());
	}

	@Test
	public void shouldNotAddArticleWhenArticleIsEmpty()
	{
		when(orderEntryModel.getBusinessItemId()).thenReturn(ARTICLE_ID);
		when(articleService.getArticleData(ARTICLE_ID)).thenReturn(Optional.empty());

		orderEntryArticlePopulator.populate(orderEntryModel, orderEntry);
		assertNull(orderEntry.getArticle());
	}
}
