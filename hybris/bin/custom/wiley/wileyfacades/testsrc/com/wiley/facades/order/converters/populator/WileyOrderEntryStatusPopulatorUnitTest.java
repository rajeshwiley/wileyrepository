package com.wiley.facades.order.converters.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyOrderEntryStatusPopulatorUnitTest
{
    private static final OrderStatus DEFAULT_STATUS = OrderStatus.OPEN;

    @InjectMocks
    private WileyOrderEntryStatusPopulator targetMock;

    @Mock
    private AbstractOrderEntryModel modelMock;

    @Mock
    private OrderStatus statusMock;

    private OrderEntryData data;

    @Before
    private void prepare()
    {
        when(modelMock.getStatus()).thenReturn(statusMock);
        when(statusMock.getCode()).thenReturn(DEFAULT_STATUS.getCode());
        data = new OrderEntryData();
    }

    @Test
    private void populateNonNullStatusTest()
    {
        // given

        // when
        targetMock.populate(modelMock, data);

        // then
        assertNotNull(data);
        assertNotNull(data.getStatus());
        assertEquals(DEFAULT_STATUS, data.getStatus());
    }

    @Test
    private void populateNullStatusTest()
    {
        // given
        when(modelMock.getStatus()).thenReturn(null);

        // when
        targetMock.populate(modelMock, data);

        // then
        assertNotNull(data);
        assertNull(data.getStatus());
    }
}