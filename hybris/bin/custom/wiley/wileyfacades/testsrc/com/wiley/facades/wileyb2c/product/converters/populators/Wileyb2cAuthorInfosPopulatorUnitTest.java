package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.AuthorInfoModel;
import com.wiley.core.model.WileyPurchaseOptionProductModel;
import com.wiley.facades.wileyb2c.product.converters.populators.helper.Wileyb2cProductCollectionAttributesHelper;


/**
 * Created by Uladzimir_Barouski on 2/27/2017.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cAuthorInfosPopulatorUnitTest
{
	public static final String AUTHOR_1_NAME = "test author 1";
	public static final String AUTHOR_1_ROLE = "author's 1 role";
	public static final String AUTHOR_2_NAME = "test author 2";
	public static final String RESULT = "test author 1 (author's 1 role), test author 2";
	@InjectMocks
	private Wileyb2cAuthorInfosPopulator wileyb2cAuthorInfosPopulator;

	@Mock
	private WileyPurchaseOptionProductModel variantProduct;

	@Mock
	private AuthorInfoModel author1, author2;
	@Mock
	private Wileyb2cProductCollectionAttributesHelper wileyb2cProductCollectionAttributesHelper;

	@Test
	public void populate() throws Exception
	{
		//Given
		when(author1.getName()).thenReturn(AUTHOR_1_NAME);
		when(author1.getRole()).thenReturn(AUTHOR_1_ROLE);
		when(author2.getName()).thenReturn(AUTHOR_2_NAME);

		when(variantProduct.getBaseProduct()).thenReturn(variantProduct);
		when(wileyb2cProductCollectionAttributesHelper.getProductAttribute(variantProduct, ProductModel.AUTHORINFOS)).thenReturn(
				Arrays.asList(author1, author2));
		ProductData productData = new ProductData();

		//When
		wileyb2cAuthorInfosPopulator.populate(variantProduct, productData);

		//Then
		assertNotNull(productData.getAuthors());
		assertEquals(RESULT, productData.getAuthors());
	}

}