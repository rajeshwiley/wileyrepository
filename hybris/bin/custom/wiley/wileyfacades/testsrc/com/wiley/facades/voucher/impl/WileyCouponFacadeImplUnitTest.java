package com.wiley.facades.voucher.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.voucher.exceptions.VoucherOperationException;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.couponservices.service.data.CouponResponse;
import de.hybris.platform.couponservices.services.CouponService;
import de.hybris.platform.order.CartService;
import static org.mockito.BDDMockito.given;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Class testing only one method WileyVoucherFacadeImpl.canApplyNewVoucher
 * For all other methods from WileyVoucherFacadeImpl
 * unit tests were not implemented.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyCouponFacadeImplUnitTest
{
	private static final String COUPON_CODE_VALID = "couponCodeValid";
	private static final String COUPON_CODE_INVALID = "couponCodeInvalid";
	private List<String> expectedResultList = Arrays.asList("violation-1", "violation-2");


	@InjectMocks
	private WileyCouponFacadeImpl wileyCouponFacade = new WileyCouponFacadeImpl();

	@Mock
	private CartService cartService;

	@Mock
	private CouponService couponService;

	@Mock
	CartModel cartModel;

	@Before
	public void setUp() throws Exception
	{
		given(cartService.getSessionCart()).willReturn(cartModel);
		given(couponService.verifyCouponCode(COUPON_CODE_VALID, cartModel)).willReturn(givenCouponResponse(null, null, true));
		given(couponService.verifyCouponCode(COUPON_CODE_INVALID, cartModel)).willReturn(givenCouponResponse(null, null, false));
		given(couponService.verifyCouponCode(null, cartModel)).willThrow(IllegalArgumentException.class);
	}

	@Test
	public void shouldNotApplyNewCouponCodeIfItIsInvalid() throws VoucherOperationException
	{
		Assert.assertFalse(wileyCouponFacade.canApplyNewVoucher(COUPON_CODE_INVALID));
	}

	@Test(expected = VoucherOperationException.class)
	public void shouldNotApplyNewCouponCodeIfItIsNull() throws VoucherOperationException
	{
		Assert.assertFalse(wileyCouponFacade.canApplyNewVoucher(null));
	}

	@Test
	public void shouldApplyNewCouponCodeIfItIsValid() throws VoucherOperationException
	{
		Assert.assertTrue(wileyCouponFacade.canApplyNewVoucher(COUPON_CODE_VALID));

	}

	private CouponResponse givenCouponResponse(final String couponId, final String message, final Boolean success)
	{
		final CouponResponse couponResponse = new CouponResponse();
		couponResponse.setCouponId(couponId);
		couponResponse.setMessage(message);
		couponResponse.setSuccess(success);
		return couponResponse;
	}
}
