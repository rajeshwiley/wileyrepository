package com.wiley.facades.wileyws.subscriptions.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileySubscriptionModel;
import com.wiley.facades.wileyws.subscriptions.dto.SubscriptionSearchResult;


/**
 * Created by Georgii_Gavrysh on 10/6/2016.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SubscriptionSearchResultPopulatorUnitTest
{
	@InjectMocks
	private SubscriptionSearchResultPopulator testedPopulator;

	@Mock
	private WileySubscriptionModel wileySubscriptionModelMock;

	@Mock
	private SubscriptionSearchResult subscriptionSearchResultMock;

	@Test
	public void testSuccess()
	{
		when(wileySubscriptionModelMock.getCode()).thenReturn("The code");

		testedPopulator.populate(wileySubscriptionModelMock, subscriptionSearchResultMock);

		verify(subscriptionSearchResultMock).setInternalSubscriptionId("The code");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testNullParameter()
	{
		testedPopulator.populate(null, subscriptionSearchResultMock);

		verify(subscriptionSearchResultMock, never()).setInternalSubscriptionId(any());
	}

}
