package com.wiley.facades.wileyb2c.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.core.model.c2l.CountryModel;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.LegacyCartConfigurationModel;

import static junit.framework.Assert.assertEquals;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2CCountryLegacyCartUrlPopulatorUnitTest
{
	private static final String LEGACY_CART_BASE_URL = "https://customerq.wiley.com";
	private static final String LEGACY_ADD_TO_CART_LINK = "/CGI-BIN/LANSAWEB?PROCFUN+shopcart5+sh5fn90";

	@InjectMocks
	private Wileyb2cCountryLegacyCartUrlPopulator testInstance = new Wileyb2cCountryLegacyCartUrlPopulator();

	@Mock
	private CountryModel mockCountryModel;
	@Mock
	private LegacyCartConfigurationModel mockLegacyCartConfigurationModel;

	private CountryData countryData = new CountryData();

	@Before
	public void setUp()
	{
		testInstance.setLegacyAddToCartLink(LEGACY_ADD_TO_CART_LINK);
		when(mockCountryModel.getLegacyCart()).thenReturn(mockLegacyCartConfigurationModel);
		when(mockLegacyCartConfigurationModel.getBaseUrl()).thenReturn(LEGACY_CART_BASE_URL);
	}

	@Test
	public void shouldPopulateLegacyCartUrl()
	{
		testInstance.populate(mockCountryModel, countryData);
		assertEquals(LEGACY_CART_BASE_URL + LEGACY_ADD_TO_CART_LINK, countryData.getB2cLegacyCartUrl());
	}
}
