package com.wiley.facades.order.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.CartService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import com.wiley.core.b2b.unit.service.WileyB2BUnitExternalService;

import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2bCheckoutFacadeImplTest
{
	private static final String INVALID_PO_1 = "invalid_PO_one";
	private static final String INVALID_PO_2 = "invalid_PO_two";
	private static final String SAP_ACCOUNT_NUMBER = "23048234832";

	@InjectMocks
	private Wileyb2bCheckoutFacadeImpl testedInstance = new Wileyb2bCheckoutFacadeImpl();

	@Mock
	private CartService cartServiceMock;
	@Mock
	private CartFacade cartFacade;
	@Mock
	private WileyB2BUnitExternalService wileyB2BUnitExternalService;

	private CartData cartData = new CartData();
	@Mock
	private CartModel cartMock;
	@Mock
	private UserModel userMock;
	@Mock
	private B2BCustomerModel b2bCustomerMock;
	@Mock
	private B2BUnitModel b2bUnitMock;

	@Before
	public void setUp()
	{
		//Given session has Cart
		when(cartServiceMock.getSessionCart()).thenReturn(cartMock);
		when(cartFacade.hasSessionCart()).thenReturn(true);
		//Given cart belongs to B2B customer
		when(cartMock.getUser()).thenReturn(b2bCustomerMock);
		when(b2bCustomerMock.getDefaultB2BUnit()).thenReturn(b2bUnitMock);
		when(b2bUnitMock.getSapAccountNumber()).thenReturn(SAP_ACCOUNT_NUMBER);
		//Mock response from remote endpoint
		final List<String> invalidPONumbers = Arrays.asList(INVALID_PO_1, INVALID_PO_2);
		populateCartDataWithPONumbers();
		when(wileyB2BUnitExternalService.validatePONumbers(SAP_ACCOUNT_NUMBER, invalidPONumbers)).thenReturn(invalidPONumbers);
	}

	@Test
	public void shouldReturnListOfInvalidPoNumbers()
	{
		//When
		final List<String> invalidPoNumbers = testedInstance.getInvalidPoNumbers(cartData);
		//Then
		assertEquals(Arrays.asList(INVALID_PO_1, INVALID_PO_2), invalidPoNumbers);
	}

	@Test(expected = NullPointerException.class)
	public void shouldThrowExceptionIfCartModelIsNull()
	{
		//Given
		when(cartServiceMock.getSessionCart()).thenReturn(null);
		//When
		testedInstance.getInvalidPoNumbers(cartData);
		//Then NullPointerException
	}

	@Test
	public void shouldReturnEmptyListIfCurrentCustomerIsNotB2BCustomer()
	{
		//Given
		when(cartMock.getUser()).thenReturn(userMock);
		//When
		final List<String> invalidPoNumbers = testedInstance.getInvalidPoNumbers(cartData);
		//Then
		assertTrue(invalidPoNumbers.isEmpty());
	}

	@Test(expected = NullPointerException.class)
	public void shouldThrowExceptionIfB2BDefaultUnitIsNull()
	{
		//Given
		when(b2bCustomerMock.getDefaultB2BUnit()).thenReturn(null);
		//When
		testedInstance.getInvalidPoNumbers(cartData);
		//Then NullPointerException
	}

	@Test
	public void shouldReturnEmptyListIfSapAccountNumberIsBlank()
	{
		//Given
		when(b2bUnitMock.getSapAccountNumber()).thenReturn(StringUtils.SPACE);
		//When
		final List<String> invalidPoNumbers = testedInstance.getInvalidPoNumbers(cartData);
		//Then
		assertTrue(invalidPoNumbers.isEmpty());
	}

	private void populateCartDataWithPONumbers()
	{
		final OrderEntryData entry1 = new OrderEntryData();
		final OrderEntryData entry2 = new OrderEntryData();

		entry1.setPoNumber(INVALID_PO_1);
		entry2.setPoNumber(INVALID_PO_2);

		cartData.setEntries(Arrays.asList(entry1, entry2));
	}

}