package com.wiley.facades.product.converters.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ImageDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Test for {@link WileycomProductPicturePopulator}
 */

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileycomProductPicturePopulatorUnitTest
{
	private static final String EXPECTED_PRODUCT_DATA_FORMAT = "product";
	private static final String EXPECTED_THUMBNAIL_DATA_FORMAT = "thumbnail";
	private static final Integer DEFAULT_DATA_GALLERY_INDEX = 0;
	private static final Integer EXPECTED_IMAGE_DATA_GALLERY_INDEX = 0;
	private static final ImageDataType EXPECTED_GALLERY_IMAGE_DATA_TYPE = ImageDataType.GALLERY;
	private static final ImageDataType EXPECTED_PRIMARY_IMAGE_DATA_TYPE = ImageDataType.PRIMARY;

	@InjectMocks
	private WileycomProductPicturePopulator wileycomProductPicturePopulator;

	@Mock(name = "imageConverter")
	private Converter<MediaModel, ImageData> imageConverterMock;

	@Mock
	private ProductModel productModelMock;

	@Mock
	private ProductData productDataMock;

	@Mock
	private MediaModel mediaModelMock;

	@Mock
	private ImageData imageDataMock;

	@Before
	public void setUp() throws Exception
	{
		when(productModelMock.getPicture()).thenReturn(mediaModelMock);
		when(imageConverterMock.convert(mediaModelMock)).thenReturn(imageDataMock);
	}

	@Test
	public void shouldNotPopulateNullProductPicture() throws Exception
	{
		//Given
		when(productModelMock.getPicture()).thenReturn(null);

		//When
		wileycomProductPicturePopulator.populate(productModelMock, productDataMock);

		//Then
		verify(productDataMock, never()).setImages(any());

	}

	@Test
	public void shouldPopulateWhileDataHasNullImageList() throws Exception
	{
		//Given
		when(productDataMock.getImages()).thenReturn(null);

		//When
		wileycomProductPicturePopulator.populate(productModelMock, productDataMock);

		//Then
		verify(productDataMock).setImages(Arrays.asList(imageDataMock, imageDataMock));
	}

	@Test
	public void shouldPopulateWhileDataHasEmptyImageList() throws Exception
	{
		//Given
		when(productDataMock.getImages()).thenReturn(Collections.emptyList());

		//When
		wileycomProductPicturePopulator.populate(productModelMock, productDataMock);

		//Then
		verify(productDataMock).setImages(Arrays.asList(imageDataMock, imageDataMock));
	}

	@Test
	public void shouldPopulateWhileDataHasFilledImageList() throws Exception
	{
		//Given
		ImageData prepopulatedImageDataMock = mock(ImageData.class);
		when(productDataMock.getImages()).thenReturn(Collections.singletonList(prepopulatedImageDataMock));
		List<ImageData> expectedImageDataList = Arrays.asList(prepopulatedImageDataMock, imageDataMock, imageDataMock);

		//When
		wileycomProductPicturePopulator.populate(productModelMock, productDataMock);

		//Then
		verify(productDataMock).setImages(expectedImageDataList);
	}


	@Test
	public void shouldPopulate() throws Exception
	{
		//Given
		//no addition condition

		//When
		wileycomProductPicturePopulator.populate(productModelMock, productDataMock);

		//Then
		verify(imageDataMock).setFormat(EXPECTED_PRODUCT_DATA_FORMAT);
		verify(imageDataMock).setFormat(EXPECTED_THUMBNAIL_DATA_FORMAT);
		verify(imageDataMock).setImageType(EXPECTED_GALLERY_IMAGE_DATA_TYPE);
		verify(imageDataMock).setImageType(EXPECTED_PRIMARY_IMAGE_DATA_TYPE);
		verify(imageDataMock, times(2)).setGalleryIndex(EXPECTED_IMAGE_DATA_GALLERY_INDEX);
		verify(productDataMock).setImages(Arrays.asList(imageDataMock, imageDataMock));
	}
}

