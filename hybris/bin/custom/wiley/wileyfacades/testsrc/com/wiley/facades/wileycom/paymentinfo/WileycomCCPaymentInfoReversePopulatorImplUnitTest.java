package com.wiley.facades.wileycom.paymentinfo;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.paymentinfo.CardPaymentInfoCcOwnerGenerator;


/**
 * Default unit test for {@link WileycomCCPaymentInfoReversePopulatorImpl}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileycomCCPaymentInfoReversePopulatorImplUnitTest
{

	private static final String TEST_CC_OWNER = "someVeryTestCCOwner";
	@Mock
	private Populator<CCPaymentInfoData, CreditCardPaymentInfoModel> defaultCardPaymentInfoReversePopulator;

	@Mock
	private CardPaymentInfoCcOwnerGenerator cardPaymentInfoCcOwnerGeneratorMock;

	@InjectMocks
	private WileycomCCPaymentInfoReversePopulatorImpl wileycomCCPaymentInfoReversePopulator;

	// Test data

	@Mock
	private CCPaymentInfoData ccPaymentInfoDataMock;

	@Mock
	private CreditCardPaymentInfoModel creditCardPaymentInfoModelMock;

	@Mock
	private AddressModel billingAddressModelMock;

	@Before
	public void setUp() throws Exception
	{
		when(cardPaymentInfoCcOwnerGeneratorMock.getCCOwnerFromBillingAddress(same(billingAddressModelMock))).thenReturn(
				Optional.of(TEST_CC_OWNER));
	}

	@Test
	public void testPopulateWithValidData()
	{
		// Given
		when(creditCardPaymentInfoModelMock.getBillingAddress()).thenReturn(billingAddressModelMock);

		// When
		wileycomCCPaymentInfoReversePopulator.populate(ccPaymentInfoDataMock, creditCardPaymentInfoModelMock);

		// Then
		verify(defaultCardPaymentInfoReversePopulator).populate(same(ccPaymentInfoDataMock),
				same(creditCardPaymentInfoModelMock));

		verify(creditCardPaymentInfoModelMock).setCcOwner(eq(TEST_CC_OWNER));
	}

	@Test
	public void testPopulateWhenBillingAddressIsAbsent()
	{
		// Given
		when(creditCardPaymentInfoModelMock.getBillingAddress()).thenReturn(null);

		// When
		wileycomCCPaymentInfoReversePopulator.populate(ccPaymentInfoDataMock, creditCardPaymentInfoModelMock);

		// Then
		verify(defaultCardPaymentInfoReversePopulator).populate(same(ccPaymentInfoDataMock),
				same(creditCardPaymentInfoModelMock));

		verify(creditCardPaymentInfoModelMock, never()).setCcOwner(any());
	}

	@Test
	public void testPopulateWhenOptionalCCOwnerIsNotPresent()
	{
		// Given
		when(creditCardPaymentInfoModelMock.getBillingAddress()).thenReturn(billingAddressModelMock);
		when(cardPaymentInfoCcOwnerGeneratorMock.getCCOwnerFromBillingAddress(same(billingAddressModelMock))).thenReturn(
				Optional.empty());

		// When
		wileycomCCPaymentInfoReversePopulator.populate(ccPaymentInfoDataMock, creditCardPaymentInfoModelMock);

		// Then
		verify(defaultCardPaymentInfoReversePopulator).populate(same(ccPaymentInfoDataMock),
				same(creditCardPaymentInfoModelMock));

		verify(creditCardPaymentInfoModelMock).setCcOwner(null);
	}

	@Test
	public void testPopulateWithNullParameters()
	{
		testNullArguments(null, creditCardPaymentInfoModelMock);
		testNullArguments(ccPaymentInfoDataMock, null);
		testNullArguments(null, null);
	}

	private void testNullArguments(final CCPaymentInfoData ccPaymentInfoData,
			final CreditCardPaymentInfoModel creditCardPaymentInfoModel)
	{
		try
		{
			// When
			wileycomCCPaymentInfoReversePopulator.populate(ccPaymentInfoData, creditCardPaymentInfoModel);
			fail("Expected IllegalArgumentException.");
		}
		catch (IllegalArgumentException e)
		{
			// Then
			// Success
		}
	}

}