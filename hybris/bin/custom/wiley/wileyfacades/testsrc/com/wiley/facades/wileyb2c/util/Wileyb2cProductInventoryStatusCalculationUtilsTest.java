package com.wiley.facades.wileyb2c.util;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.core.model.product.ProductModel;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.doReturn;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.enums.ProductEditionFormat;
import com.wiley.facades.product.data.InventoryStatusRecord;

import static java.util.Arrays.asList;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
@Ignore("This tests depends on obsolete attribute")
public class Wileyb2cProductInventoryStatusCalculationUtilsTest
{
	private static final ProductEditionFormat DIGITAL = ProductEditionFormat.DIGITAL;
	private static final ProductEditionFormat PHYSICAL = ProductEditionFormat.PHYSICAL;

	private static final StockLevelStatus IN_STOCK = StockLevelStatus.INSTOCK;
	private static final StockLevelStatus BACK_ORDER = StockLevelStatus.BACK_ORDER;
	private static final StockLevelStatus PRE_ORDER = StockLevelStatus.PRE_ORDER;
	private static final StockLevelStatus PRINT_ON_DEMAND = StockLevelStatus.PRINT_ON_DEMAND;

	private static final long ONE_DAY_MILLIS = 1000 * 60 * 60 * 24;
	private static final Date THE_FUTURE = new Date(new Date().getTime() + ONE_DAY_MILLIS);
	private static final Date THE_PAST = new Date(new Date().getTime() - ONE_DAY_MILLIS);


	@InjectMocks
	private Wileyb2cProductInventoryStatusCalculationUtils inventoryStatusStrategy;

	@Mock
	private ProductModel productMock;



	/*  PARAMETERS ORDER
		INPUT: requestedQuantity, availableQuantity, releaseDate, printOnDemand, editionFormat
		EXPECTED: List<status, quantity, date>
	*/

	@Test
	public void testAbsentPhysicalProductPrintOnDemand()
	{
		// Given
		InventoryStatusTestInputData input = new InventoryStatusTestInputData(3L, 0L, null, true, PHYSICAL);
		List<InventoryStatusRecord> expected = asList(createRecord(PRINT_ON_DEMAND, null, null));

		// Test
		verifyCorrectCalculation(input, expected);
	}



	@Test
	public void testAbsentPhysicalProductBackOrder()
	{
		// Given
		InventoryStatusTestInputData input = new InventoryStatusTestInputData(3L, 0L, null, false, PHYSICAL);
		List<InventoryStatusRecord> expected = asList(createRecord(BACK_ORDER, null, null));

		// Test
		verifyCorrectCalculation(input, expected);
	}

	@Test
	public void testInsufficientPhysicalProductPrintOnDemand()
	{
		// Given
		InventoryStatusTestInputData input = new InventoryStatusTestInputData(3L, 1L, null, true, PHYSICAL);
		List<InventoryStatusRecord> expected = asList(createRecord(IN_STOCK, 1L, null),
				createRecord(PRINT_ON_DEMAND, 2L, null));

		// Test
		verifyCorrectCalculation(input, expected);
	}

	@Test
	public void testInsufficientPhysicalProductBackOrder()
	{
		// Given
		InventoryStatusTestInputData input = new InventoryStatusTestInputData(3L, 1L, null, false, PHYSICAL);
		List<InventoryStatusRecord> expected = asList(createRecord(IN_STOCK, 1L, null),
				createRecord(BACK_ORDER, 2L, null));

		// Test
		verifyCorrectCalculation(input, expected);
	}

	@Test
	public void testInsufficientPhysicalProductBackOrder2()
	{
		// Given
		InventoryStatusTestInputData input = new InventoryStatusTestInputData(3L, 1L, THE_PAST, false, PHYSICAL);
		List<InventoryStatusRecord> expected = asList(createRecord(IN_STOCK, 1L, null),
				createRecord(BACK_ORDER, 2L, null));

		// Test
		verifyCorrectCalculation(input, expected);
	}

	@Test
	public void testInStockPhysicalProduct()
	{
		// Given
		InventoryStatusTestInputData input = new InventoryStatusTestInputData(3L, 100L, null, false, PHYSICAL);
		List<InventoryStatusRecord> expected = asList(createRecord(IN_STOCK, null, null));

		// Test
		verifyCorrectCalculation(input, expected);
	}

	@Test
	public void testInStockPhysicalProduct2()
	{
		// Given
		InventoryStatusTestInputData input = new InventoryStatusTestInputData(3L, 100L, THE_PAST, false, PHYSICAL);
		List<InventoryStatusRecord> expected = asList(createRecord(IN_STOCK, null, null));

		// Test
		verifyCorrectCalculation(input, expected);
	}

	@Test
	public void testSimpleDigitalProduct()
	{
		// Given
		InventoryStatusTestInputData input = new InventoryStatusTestInputData(3L, 0L, null, false, DIGITAL);
		List<InventoryStatusRecord> expected = asList();

		// Test
		verifyCorrectCalculation(input, expected);
	}

	@Test
	public void testSimpleDigitalProduct2()
	{
		// Given
		InventoryStatusTestInputData input = new InventoryStatusTestInputData(3L, 100L, null, false, DIGITAL);
		List<InventoryStatusRecord> expected = asList();

		// Test
		verifyCorrectCalculation(input, expected);
	}

	@Test
	public void testSimpleDigitalProduct3()
	{
		// Given
		InventoryStatusTestInputData input = new InventoryStatusTestInputData(3L, 0L, THE_PAST, false, DIGITAL);
		List<InventoryStatusRecord> expected = asList();

		// Test
		verifyCorrectCalculation(input, expected);
	}

	@Test
	public void testDigitalProductPreOrder()
	{
		// Given
		InventoryStatusTestInputData input = new InventoryStatusTestInputData(3L, 0L, THE_FUTURE, false, DIGITAL);
		List<InventoryStatusRecord> expected = asList(createRecord(PRE_ORDER, null, null));

		// Test
		verifyCorrectCalculation(input, expected);
	}

	@Test
	public void testPhysicalProductPreOrder()
	{
		// Given
		InventoryStatusTestInputData input = new InventoryStatusTestInputData(3L, 0L, THE_FUTURE, false, PHYSICAL);
		List<InventoryStatusRecord> expected = asList(createRecord(PRE_ORDER, null, null));

		// Test
		verifyCorrectCalculation(input, expected);
	}

	@Test
	public void testPhysicalProductPreOrder2()
	{
		// Given
		InventoryStatusTestInputData input = new InventoryStatusTestInputData(3L, 100L, THE_FUTURE, false, PHYSICAL);
		List<InventoryStatusRecord> expected = asList(createRecord(PRE_ORDER, null, null));

		// Test
		verifyCorrectCalculation(input, expected);
	}

	@Test
	public void testRequestedQuantityIsNotDefined()
	{
		// Given
		InventoryStatusTestInputData input = new InventoryStatusTestInputData(null, 100L, null, false, PHYSICAL);
		List<InventoryStatusRecord> expected = asList();

		// Test
		verifyCorrectCalculation(input, expected);
	}


	protected void verifyCorrectCalculation(final  InventoryStatusTestInputData input, final List<InventoryStatusRecord> expected)
	{
		// Given
		// TODO : [ECSC-20075] commented out code which uses obsolete attribute. Should be deleted
		// doReturn(input.getReleaseDate()).when(productMock).getReleaseDate();
		doReturn(input.getPrintOnDemand()).when(productMock).getPrintOnDemand();
		doReturn(input.getEditionFormat()).when(productMock).getEditionFormat();

		// When
		final List<InventoryStatusRecord> result =
				Wileyb2cProductInventoryStatusCalculationUtils.
						calculateInventoryStatusForCartProduct(productMock, input.getRequestedQuantity(),
						input.getAvailableQuantity());

		// Then
		assertEquals(result.size(), expected.size());
		for (int i = 0; i < expected.size(); i++)
		{
			if (!isInventoryStatusRecordsEqual(result.get(i), expected.get(i)))
			{
				throw new IllegalStateException("Actual: " + statusesListToString(result)
						+ "; Expected: " + statusesListToString(expected));
			}
		}
	}

	protected String statusesListToString(final List<InventoryStatusRecord> list)
	{
		StringBuilder result = new StringBuilder();
		list.stream().forEach(record -> result.append(inventoryStatusRecordToString(record)));
		return result.toString();
	}

	protected String inventoryStatusRecordToString(final InventoryStatusRecord record)
	{
		assertNotNull(record);
		return Arrays.toString(new Object[] {record.getStatusCode(), record.getQuantity(), record.getAvailableDate()});
	}

	protected boolean isInventoryStatusRecordsEqual(final InventoryStatusRecord a, final InventoryStatusRecord b)
	{
		final boolean equalStatus = a.getStatusCode() == null && b.getStatusCode() == null
				|| a.getStatusCode() != null && a.getStatusCode().equals(b.getStatusCode());
		final boolean equalQuantity = a.getQuantity() == null && b.getQuantity() == null
				|| a.getQuantity() != null && a.getQuantity().equals(b.getQuantity());
		final boolean equalDate = a.getAvailableDate() == null && b.getAvailableDate() == null
				|| a.getAvailableDate() != null && a.getAvailableDate().equals(b.getAvailableDate());

		return equalStatus && equalQuantity && equalDate;
	}

	protected InventoryStatusRecord createRecord(final StockLevelStatus status, final Long quantity, final Date date)
	{
		return  Wileyb2cProductInventoryStatusCalculationUtils.createInventoryStatusRecord(status, quantity, date, true);
	}

	private static class InventoryStatusTestInputData
	{
		private Long requestedQuantity;
		private Long availableQuantity;
		private Date releaseDate;
		private Boolean printOnDemand;
		private ProductEditionFormat editionFormat;

		InventoryStatusTestInputData(final Long requestedQuantity, final Long availableQuantity, final Date releaseDate,
				final Boolean printOnDemand,
				final ProductEditionFormat editionFormat)
		{
			this.requestedQuantity = requestedQuantity;
			this.availableQuantity = availableQuantity;
			this.releaseDate = releaseDate;
			this.printOnDemand = printOnDemand;
			this.editionFormat = editionFormat;
		}

		public Long getRequestedQuantity()
		{
			return requestedQuantity;
		}

		public void setRequestedQuantity(final Long requestedQuantity)
		{
			this.requestedQuantity = requestedQuantity;
		}

		public Long getAvailableQuantity()
		{
			return availableQuantity;
		}

		public void setAvailableQuantity(final Long availableQuantity)
		{
			this.availableQuantity = availableQuantity;
		}

		public Date getReleaseDate()
		{
			return releaseDate;
		}

		public void setReleaseDate(final Date releaseDate)
		{
			this.releaseDate = releaseDate;
		}

		public Boolean getPrintOnDemand()
		{
			return printOnDemand;
		}

		public void setPrintOnDemand(final Boolean printOnDemand)
		{
			this.printOnDemand = printOnDemand;
		}

		public ProductEditionFormat getEditionFormat()
		{
			return editionFormat;
		}

		public void setEditionFormat(final ProductEditionFormat editionFormat)
		{
			this.editionFormat = editionFormat;
		}

	}
}
