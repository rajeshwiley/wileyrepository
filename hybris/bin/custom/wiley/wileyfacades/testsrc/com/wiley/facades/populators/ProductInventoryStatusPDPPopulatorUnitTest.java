package com.wiley.facades.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.stock.CommerceStockService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.enums.ProductEditionFormat;

import static junit.framework.Assert.assertEquals;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ProductInventoryStatusPDPPopulatorUnitTest
{
	@InjectMocks
	private final ProductInventoryStatusPDPPopulator testPopulator = new ProductInventoryStatusPDPPopulator();

	@Mock
	private CommerceStockService commerceStockService;

	@Mock
	private BaseStoreService baseStoreService;

	@Mock
	private BaseStoreModel baseStoreModel;

	private final ProductData target = new ProductData();

	@Mock
	private ProductModel productModel;

	@Before
	public void setUp()
	{
		when(baseStoreService.getCurrentBaseStore()).thenReturn(baseStoreModel);
	}

	/**
	 * Pre-order status test
	 */
	@Test
	@Ignore("This test depends on obsolete attribute")
	public void preOrderStatusTest()
	{
		// TODO : [ECSC-20075] commented out code which uses obsolete attribute. Should be deleted
/*		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.add(Calendar.DATE, 10);

		when(productModel.getReleaseDate()).thenReturn(c.getTime());*/

		testPopulator.populate(productModel, target);

		assertEquals("ProductData should contain PRE_ORDER inventory status", StockLevelStatus.PRE_ORDER,
				target.getInventoryStatus().get(0).getStatusCode());
	}

	/**
	 * Print on demand status test
	 */
	@Test
	public void printOnDemandStatusTest()
	{
		setupStockLevelStatusAndCount(StockLevelStatus.OUTOFSTOCK, 0);

		when(productModel.getPrintOnDemand()).thenReturn(true);
		testPopulator.populate(productModel, target);

		assertEquals("ProductData should contain PRINT_ON_DEMAND inventory status", StockLevelStatus.PRINT_ON_DEMAND,
				target.getInventoryStatus().get(0).getStatusCode());
	}

	/**
	 * Back order status test
	 */
	@Test
	public void backOrderStatusTest()
	{
		setupStockLevelStatusAndCount(StockLevelStatus.OUTOFSTOCK, 0);

		testPopulator.populate(productModel, target);

		assertEquals("ProductData should contain BACK_ORDER inventory status", StockLevelStatus.BACK_ORDER,
				target.getInventoryStatus().get(0).getStatusCode());
	}

	/**
	 * InStock status test
	 */
	@Test
	public void inStockStatusTest()
	{
		setupStockLevelStatusAndCount(StockLevelStatus.INSTOCK, 10);
		when(productModel.getEditionFormat()).thenReturn(ProductEditionFormat.PHYSICAL);

		testPopulator.populate(productModel, target);

		assertEquals("ProductData should contain INSTOCK inventory status", StockLevelStatus.INSTOCK,
				target.getInventoryStatus().get(0).getStatusCode());
	}

	/**
	 * Low Stock status test
	 */
	@Test
	public void lowStockStatusTest()
	{
		setupStockLevelStatusAndCount(StockLevelStatus.LOWSTOCK, 10);

		testPopulator.populate(productModel, target);

		assertEquals("ProductData should contain LOWSTOCK inventory status", StockLevelStatus.LOWSTOCK,
				target.getInventoryStatus().get(0).getStatusCode());
	}

	private void setupStockLevelStatusAndCount(final StockLevelStatus stockLevelStatus, final long count)
	{
		when(commerceStockService.getStockLevelStatusForProductAndBaseStore(productModel, baseStoreModel)).thenReturn(
				stockLevelStatus);
		when(commerceStockService.getStockLevelForProductAndBaseStore(productModel, baseStoreModel)).thenReturn(count);
	}

}
