package com.wiley.facades.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.wiley.facades.product.data.MediaData;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static org.mockito.Mockito.when;


@UnitTest
public class ProductDetailsPopulatorUnitTest
{

	private final ProductDetailsPopulator testingInstance = new ProductDetailsPopulator();

	@Mock
	Converter<MediaModel, MediaData> mediaConverter;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		BDDMockito.when(mediaConverter.convert(Matchers.any(MediaModel.class))).thenReturn(
				new MediaData());
		testingInstance.setMediaConverter(mediaConverter);
	}

	@Test
	public void shouldPopulateProductData()
	{

		final MediaModel mediaModel = new MediaModel();
		final List<MediaModel> detail = new ArrayList<MediaModel>();
		detail.add(mediaModel);
		final ProductModel source = Mockito.mock(ProductModel.class);
		when(source.getDetail()).thenReturn(detail);

		final ProductData target = new ProductData();
		testingInstance.populate(source, target);

		assertNotNull(target.getDetails());
		assertFalse(target.getDetails().isEmpty());

	}
}
