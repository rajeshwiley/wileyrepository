package com.wiley.facades.order.converters.populator;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.WileyPaymentInfoData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.voucher.VoucherService;

import java.util.Collections;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.pin.service.PinService;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyOrderPopulatorUnitTest
{

	private static final String PAYMENT_MODE_CODE = "paymentMode";
	private static final String PURCHASE_ORDER_NUMBER = "purchaseOrderNumber";
	private static final String USER_NOTES = "userNotes";
	private static final String TAX_NUMBER = "taxNumber";
	private static final String ISO_CODE = "isocode";
	private static final String EXTERNAL_TEXT = "externalText";

	@Spy
	@InjectMocks
	private WileyOrderPopulator wileyOrderPopulator;
	@Mock
	private OrderModel orderModelMock;
	@Mock
	private VoucherService voucherServiceMock;
	@Mock
	private PinService pinServiceMock;
	@Mock
	private PriceDataFactory priceDataFactoryMock;
	@Mock(name = "addressConverter")
	private Converter<AddressModel, AddressData> addressConverterMock;
	@Mock(name = "wileyPaymentInfoConverter")
	private Converter<PaymentInfoModel, WileyPaymentInfoData> wileyPaymentInfoConverterMock;

	private OrderData orderData;

	@Before
	public void setUp()
	{
		orderData = new OrderData();
		doNothing().when(wileyOrderPopulator).superPopulate(orderModelMock, orderData);
	}

	@Test
	public void shouldPopulateExpectedFields()
	{
		// Given
		final AddressModel addressModelMock = mock(AddressModel.class);
		final PaymentInfoModel wileyPaymentInfoModelMock = mock(PaymentInfoModel.class);
		final PaymentModeModel paymentModeModelMock = mock(PaymentModeModel.class);
		final AddressData addressDataMock = mock(AddressData.class);
		final WileyPaymentInfoData wileyPaymentInfoDataMock = mock(WileyPaymentInfoData.class);
		final CurrencyModel currencyModelMock = mock(CurrencyModel.class);
		final Date dateMock = mock(Date.class);

		given(orderModelMock.getPaymentAddress()).willReturn(addressModelMock);
		given(orderModelMock.getPaymentInfo()).willReturn(wileyPaymentInfoModelMock);
		given(orderModelMock.getPaymentMode()).willReturn(paymentModeModelMock);
		given(orderModelMock.getTaxNumberExpirationDate()).willReturn(dateMock);
		given(orderModelMock.getCurrency()).willReturn(currencyModelMock);
		given(orderModelMock.getPurchaseOrderNumber()).willReturn(PURCHASE_ORDER_NUMBER);
		given(orderModelMock.getUserNotes()).willReturn(USER_NOTES);
		given(orderModelMock.getTaxNumber()).willReturn(TAX_NUMBER);
		given(orderModelMock.getExternalText()).willReturn(EXTERNAL_TEXT);
		given(paymentModeModelMock.getCode()).willReturn(PAYMENT_MODE_CODE);
		given(currencyModelMock.getIsocode()).willReturn(ISO_CODE);
		given(addressConverterMock.convert(addressModelMock)).willReturn(addressDataMock);
		given(wileyPaymentInfoConverterMock.convert(wileyPaymentInfoModelMock)).willReturn(wileyPaymentInfoDataMock);
		given(pinServiceMock.isPinUsedForOrder(orderModelMock)).willReturn(true);
		given(voucherServiceMock.getAppliedVoucherCodes(orderModelMock)).willReturn(Collections.EMPTY_SET);

		// When
		wileyOrderPopulator.populate(orderModelMock, orderData);

		// Then
		assertEquals(orderData.getWileyPaymentInfo(), wileyPaymentInfoDataMock);
		assertEquals(orderData.getPaymentAddress(), addressDataMock);
		assertEquals(orderData.getPaymentMode(), PAYMENT_MODE_CODE);
		assertEquals(orderData.getPurchaseOrderNumber(), PURCHASE_ORDER_NUMBER);
		assertEquals(orderData.getUserNotes(), USER_NOTES);
		assertEquals(orderData.getTaxNumber(), TAX_NUMBER);
		assertEquals(orderData.getTaxNumberExpirationDate(), dateMock);
		assertEquals(orderData.getExternalText(), EXTERNAL_TEXT);
	}
}
