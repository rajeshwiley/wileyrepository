package com.wiley.facades.product.converters.populator;


import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;


import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyPurchaseOptionProductModel;
import com.wiley.facades.wileyb2c.product.converters.populators.Wileyb2cProductSeriesPopulator;

import static org.codehaus.groovy.runtime.InvokerHelper.asList;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cProductSeriesPopulatorUnitTest
{
	private ProductData productData;
	private CategoryData categoryData;

	@Mock
	private Converter<CategoryModel, CategoryData> mockCategoryUrlConverter;
	@Mock
	private CategoryModel mockSeriesCategoryModel;
	@Mock
	private WileyPurchaseOptionProductModel mockOptionProductModel;
	@Mock
	private ProductModel mockBaseProductModel;

	@InjectMocks
	private Wileyb2cProductSeriesPopulator testInstance = new Wileyb2cProductSeriesPopulator();

	@Before
	public void setUp()
	{
		productData = new ProductData();
		when(mockOptionProductModel.getBaseProduct()).thenReturn(mockBaseProductModel);

	}

	@Test
	public void shouldPopulateIfBaseProduct()
	{
		//when
		when(mockBaseProductModel.getSeries()).thenReturn(asList(mockSeriesCategoryModel));
		when(mockCategoryUrlConverter.convert(mockSeriesCategoryModel)).thenReturn(categoryData);

		//then
		testInstance.populate(mockBaseProductModel, productData);
		assertNotNull(productData.getSeries());

	}

	@Test
	public void shouldPopulateIfOptionProduct()
	{
		//when
		when(mockBaseProductModel.getSeries()).thenReturn(asList(mockSeriesCategoryModel));
		when(mockCategoryUrlConverter.convert(mockSeriesCategoryModel)).thenReturn(categoryData);

		//then
		testInstance.populate(mockOptionProductModel, productData);
		assertNotNull(productData.getSeries());
	}

	@Test
	public void shouldNotPopulateIfBaseProduct()
	{
		//when
		when(mockBaseProductModel.getSeries()).thenReturn(Collections.emptyList());

		//then
		testInstance.populate(mockBaseProductModel, productData);
		assertTrue(productData.getSeries().isEmpty());
	}

	@Test
	public void shouldNotPopulateIfOptionProduct()
	{
		//when
		when(mockBaseProductModel.getSeries()).thenReturn(Collections.emptyList());

		//then
		testInstance.populate(mockOptionProductModel, productData);
		assertTrue(productData.getSeries().isEmpty());
	}

}