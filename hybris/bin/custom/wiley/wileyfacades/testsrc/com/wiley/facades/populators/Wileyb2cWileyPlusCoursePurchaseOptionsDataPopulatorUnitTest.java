package com.wiley.facades.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.wiley.facades.wileyb2c.product.data.CoursePurchaseOptionsData;
import com.wiley.facades.wileyb2c.product.data.InstantAccessPurchaseOptionData;
import com.wiley.facades.wileyb2c.product.data.WileyPlusCourseInfoData;
import com.wiley.facades.wileyplus.cart.activation.dto.CartActivationRequestDto;
import com.wiley.facades.wileyplus.cart.activation.dto.PurchaseOptionsDto;


/**
 * Unit test for {@link Wileyb2cWileyPlusCoursePurchaseOptionsDataPopulator}
 */
@UnitTest
public class Wileyb2cWileyPlusCoursePurchaseOptionsDataPopulatorUnitTest
{

	private Wileyb2cWileyPlusCoursePurchaseOptionsDataPopulator populator;

	private CartActivationRequestDto cartActivationRequestDto;

	private PurchaseOptionsDto purchaseOptionsDto;

	private WileyPlusCourseInfoData wileyPlusCourseInfoData;

	@Before
	public void setUp() throws Exception
	{
		populator = new Wileyb2cWileyPlusCoursePurchaseOptionsDataPopulator();

		cartActivationRequestDto = new CartActivationRequestDto();
		purchaseOptionsDto = new PurchaseOptionsDto();
		wileyPlusCourseInfoData = new WileyPlusCourseInfoData();

		cartActivationRequestDto.setPurchaseOptionsDto(purchaseOptionsDto);
	}

	@Test
	public void testPopulate()
	{
		// Given
		purchaseOptionsDto.setGracePeriodDuration(24);
		purchaseOptionsDto.setGracePeriodUsed(true);
		purchaseOptionsDto.setRegCodeActivationAvailable(true);

		// When
		populator.populate(cartActivationRequestDto, wileyPlusCourseInfoData);

		// Then
		final CoursePurchaseOptionsData actualPurchaseOptions = wileyPlusCourseInfoData.getPurchaseOptions();

		assertNotNull(actualPurchaseOptions);

		// check free trial
		assertEquals(Integer.valueOf(24), actualPurchaseOptions.getGracePeriodDuration());
		assertTrue(actualPurchaseOptions.getGracePeriodUsed());

		// check regcode
		assertTrue(actualPurchaseOptions.getRegCodeActivationAvailable());

		// check instant access
		final InstantAccessPurchaseOptionData actualInstantAccessPurchaseOption =
				actualPurchaseOptions.getInstantAccessPurchaseOption();
		assertNotNull(actualInstantAccessPurchaseOption);
		assertTrue(actualInstantAccessPurchaseOption.getPurchaseOptionAvailable());
	}

	@Test
	public void testPopulateForNegativeCase()
	{
		// Given
		purchaseOptionsDto.setGracePeriodDuration(0);
		purchaseOptionsDto.setGracePeriodUsed(false);
		purchaseOptionsDto.setRegCodeActivationAvailable(false);

		// When
		populator.populate(cartActivationRequestDto, wileyPlusCourseInfoData);

		// Then
		final CoursePurchaseOptionsData actualPurchaseOptions = wileyPlusCourseInfoData.getPurchaseOptions();

		assertNotNull(actualPurchaseOptions);

		// check free trial
		assertEquals(Integer.valueOf(0), actualPurchaseOptions.getGracePeriodDuration());
		assertFalse(actualPurchaseOptions.getGracePeriodUsed());

		// check regcode
		assertFalse(actualPurchaseOptions.getRegCodeActivationAvailable());

		// check instant access
		final InstantAccessPurchaseOptionData actualInstantAccessPurchaseOption =
				actualPurchaseOptions.getInstantAccessPurchaseOption();
		assertNotNull(actualInstantAccessPurchaseOption);
		assertTrue(actualInstantAccessPurchaseOption.getPurchaseOptionAvailable());
	}

}