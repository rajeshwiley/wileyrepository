package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyPurchaseOptionProductModel;
import com.wiley.core.wiley.restriction.WileyProductRestrictionService;


/**
 * Created by Uladzimir_Barouski on 5/5/2017.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cShowPdpUrlPopulatorUnitTest
{
	@Mock
	private WileyProductRestrictionService wileyProductRestrictionServiceMock;

	@Mock
	private WileyPurchaseOptionProductModel wileyPurchaseOptionProductModelMock, wileyPurchaseOptionProductModelMock1,
			wileyPurchaseOptionProductModelMock2;

	@Mock
	private ProductModel productModelMock;

	@InjectMocks
	private Wileyb2cShowPdpUrlPopulator wileyb2cShowPdpUrlPopulator;


	@Test
	public void populatePurchaseOptionWhenUrlVisible() throws Exception
	{
		//Given
		when(wileyProductRestrictionServiceMock.isVisible(wileyPurchaseOptionProductModelMock)).thenReturn(true);
		ProductData productData = new ProductData();
		//When
		wileyb2cShowPdpUrlPopulator.populate(wileyPurchaseOptionProductModelMock, productData);
		//Then
		assertTrue(productData.getShowPdpUrl());
	}

	@Test
	public void populatePurchaseOptionWhenUrlNotVisible() throws Exception
	{
		//Given
		when(wileyProductRestrictionServiceMock.isVisible(wileyPurchaseOptionProductModelMock)).thenReturn(false);
		ProductData productData = new ProductData();
		//When
		wileyb2cShowPdpUrlPopulator.populate(wileyPurchaseOptionProductModelMock, productData);
		//Then
		assertFalse(productData.getShowPdpUrl());
	}

	@Test
	public void populateBaseProductWhenUrlVisible() throws Exception
	{
		//Given
		when(productModelMock.getVariants()).thenReturn(
				Arrays.asList(wileyPurchaseOptionProductModelMock1, wileyPurchaseOptionProductModelMock2));
		when(wileyProductRestrictionServiceMock.isVisible(wileyPurchaseOptionProductModelMock1)).thenReturn(true);
		when(wileyProductRestrictionServiceMock.isVisible(wileyPurchaseOptionProductModelMock2)).thenReturn(false);
		ProductData productData = new ProductData();
		//When
		wileyb2cShowPdpUrlPopulator.populate(productModelMock, productData);
		//Then
		assertTrue(productData.getShowPdpUrl());
	}

	@Test
	public void populateBaseProductWhenUrlNotVisible() throws Exception
	{
		//Given
		when(productModelMock.getVariants()).thenReturn(
				Arrays.asList(wileyPurchaseOptionProductModelMock1, wileyPurchaseOptionProductModelMock2));
		when(wileyProductRestrictionServiceMock.isVisible(wileyPurchaseOptionProductModelMock1)).thenReturn(false);
		when(wileyProductRestrictionServiceMock.isVisible(wileyPurchaseOptionProductModelMock2)).thenReturn(false);
		ProductData productData = new ProductData();
		//When
		wileyb2cShowPdpUrlPopulator.populate(productModelMock, productData);
		//Then
		assertFalse(productData.getShowPdpUrl());
	}

	@Test
	public void populateWhenBaseProductHasNoVariants() throws Exception
	{
		//Given
		when(productModelMock.getVariants()).thenReturn(Collections.EMPTY_LIST);
		ProductData productData = new ProductData();
		//When
		wileyb2cShowPdpUrlPopulator.populate(productModelMock, productData);
		//Then
		assertFalse(productData.getShowPdpUrl());
	}
}