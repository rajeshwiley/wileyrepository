package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.ImageFormatMapping;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ImageDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaFormatModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.media.MediaContainerService;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyPurchaseOptionProductModel;


/**
 * Created by Uladzimir_Barouski on 2/20/2017.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cProductBackgroundImagePopulatorUnitTest
{
	private static final String DESKTOP_MEDIA_FORMAT = "desktop";
	private static final String DESKTOP_MEDIA_FORMAT_QUALIFIER = "1920Wx600H";
	private static final String MOBILE_MEDIA_FORMAT = "mobile";
	private static final String MOBILE_MEDIA_FORMAT_QUALIFIER = "740Wx2748H";
	private static final List<String> IMAGE_FORMATS = Arrays.asList(DESKTOP_MEDIA_FORMAT, MOBILE_MEDIA_FORMAT);

	@InjectMocks
	private Wileyb2cProductBackgroundImagePopulator wileyb2cProductBackgroundImagePopulator;


	@Mock
	private WileyPurchaseOptionProductModel variantProduct;
	@Mock
	private ProductModel baseProduct;

	@Mock
	private MediaService mediaService;

	@Mock
	private MediaContainerService mediaContainerService;

	@Mock
	private MediaFormatModel desktopMediaFormat;

	@Mock
	private MediaFormatModel mobileMediaFormat;

	@Mock
	private ImageFormatMapping imageFormatMapping;


	@Mock
	private MediaContainerModel backgroundImageMediaContainer;

	@Mock
	private Converter<MediaModel, ImageData> imageConverterMock;

	@Mock
	private MediaModel desktopMedia, mobileMedia, otherMedia;

	@Mock
	private ImageData desktopDataMock, mobileDataMock;
	@Mock
	private ModelService modelService;

	@Before
	public void setUp() throws Exception
	{
		wileyb2cProductBackgroundImagePopulator.setImageFormats(IMAGE_FORMATS);

		when(imageFormatMapping.getMediaFormatQualifierForImageFormat(DESKTOP_MEDIA_FORMAT)).thenReturn(
				DESKTOP_MEDIA_FORMAT_QUALIFIER);
		when(imageFormatMapping.getMediaFormatQualifierForImageFormat(MOBILE_MEDIA_FORMAT)).thenReturn(
				MOBILE_MEDIA_FORMAT_QUALIFIER);
		wileyb2cProductBackgroundImagePopulator.setImageFormatMapping(imageFormatMapping);

		when(mediaService.getFormat(DESKTOP_MEDIA_FORMAT_QUALIFIER)).thenReturn(desktopMediaFormat);
		when(mediaService.getFormat(MOBILE_MEDIA_FORMAT_QUALIFIER)).thenReturn(mobileMediaFormat);

		when(mediaContainerService.getMediaForFormat(backgroundImageMediaContainer, desktopMediaFormat)).thenReturn(desktopMedia);
		when(mediaContainerService.getMediaForFormat(backgroundImageMediaContainer, mobileMediaFormat)).thenReturn(mobileMedia);


		when(imageConverterMock.convert(desktopMedia)).thenReturn(desktopDataMock);
		when(imageConverterMock.convert(mobileMedia)).thenReturn(mobileDataMock);

		when(backgroundImageMediaContainer.getMedias()).thenReturn(Arrays.asList(desktopMedia, mobileMedia));
		when(variantProduct.getBaseProduct()).thenReturn(baseProduct);
	}

	@Test
	public void populateBackgroundImageWithFormats() throws Exception
	{
		//Given
		when(modelService.getAttributeValue(baseProduct, ProductModel.BACKGROUNDIMAGE)).thenReturn(
				backgroundImageMediaContainer);
		ProductData productData = new ProductData();
		//When
		wileyb2cProductBackgroundImagePopulator.populate(variantProduct, productData);
		//Then
		List<ImageData> backgroundImages = productData.getBackgroundImages();
		assertNotNull(backgroundImages);
		assertEquals(2, backgroundImages.size());

		assertEquals(desktopDataMock, backgroundImages.get(0));
		verify(desktopDataMock).setFormat(DESKTOP_MEDIA_FORMAT);
		verify(desktopDataMock).setImageType(ImageDataType.BACKGROUND);

		assertEquals(mobileDataMock, backgroundImages.get(1));
		verify(mobileDataMock).setFormat(MOBILE_MEDIA_FORMAT);
		verify(mobileDataMock).setImageType(ImageDataType.BACKGROUND);
	}
}