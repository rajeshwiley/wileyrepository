package com.wiley.facades.wileycom.user.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.wileycom.customer.WileycomExternalAddressService;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


/**
 * Created by Aliaksei_Zlobich on 7/15/2016.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileycomUserFacadeImplUnitTest
{

	@Mock
	private WileycomExternalAddressService wileycomExternalAddressServiceMock;

	@Mock
	private UserService userServiceMock;

	@Mock
	private CustomerAccountService customerAccountServiceMock;

	@Mock
	private Populator<CCPaymentInfoData, CreditCardPaymentInfoModel> cardPaymentInfoReversePopulatorMock;

	@Mock
	private ModelService modelServiceMock;

	@InjectMocks
	private WileycomUserFacadeImpl wileycomUserFacade;

	// Test Data

	@Mock
	private CustomerModel customerModelMock;

	@Mock
	private CreditCardPaymentInfoModel creditCardPaymentInfoModelMock;

	@Mock
	private CCPaymentInfoData ccPaymentInfoDataMock;

	@Mock
	private AddressModel billingAddressModelMock;

	@Before
	public void setUp() throws Exception
	{
		wileycomUserFacade.setCardPaymentInfoReversePopulator(
				cardPaymentInfoReversePopulatorMock); // the populator is not injected by annotation

		when(userServiceMock.getCurrentUser()).thenReturn(customerModelMock);
	}

	@Test
	public void verifyUpdateCCPaymentInfo()
	{
		// Given
		final String paymentInfoId = "paymentInfoId";

		when(ccPaymentInfoDataMock.getId()).thenReturn(paymentInfoId);

		when(customerAccountServiceMock.getCreditCardPaymentInfoForCode(eq(customerModelMock), eq(paymentInfoId)))
				.thenReturn(creditCardPaymentInfoModelMock);

		when(creditCardPaymentInfoModelMock.getBillingAddress()).thenReturn(billingAddressModelMock);

		// When
		wileycomUserFacade.updateCCPaymentInfo(ccPaymentInfoDataMock);

		// Then
		verify(customerAccountServiceMock).getCreditCardPaymentInfoForCode(eq(customerModelMock), eq(paymentInfoId));
		verify(cardPaymentInfoReversePopulatorMock).populate(eq(ccPaymentInfoDataMock), eq(creditCardPaymentInfoModelMock));
		verify(modelServiceMock).save(eq(creditCardPaymentInfoModelMock));
		verify(wileycomExternalAddressServiceMock).updateBillingAddress(eq(customerModelMock), eq(billingAddressModelMock),
				eq(false));
	}

}