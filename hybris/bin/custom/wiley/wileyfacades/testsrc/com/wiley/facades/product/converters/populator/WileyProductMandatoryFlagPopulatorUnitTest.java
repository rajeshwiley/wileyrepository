package com.wiley.facades.product.converters.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Test for {@link WileyProductMandatoryFlagPopulator}
 */

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyProductMandatoryFlagPopulatorUnitTest
{
	@InjectMocks
	private WileyProductMandatoryFlagPopulator wileyProductMandatoryFlagPopulator;

	@Mock
	private ProductModel productModelMock;

	@Mock
	private ProductData productDataMock;

	@Test
	public void shouldPopulateMandatoryFieldIsTrue()
	{
		//Given
		when(productModelMock.getMandatory()).thenReturn(true);

		//When
		wileyProductMandatoryFlagPopulator.populate(productModelMock, productDataMock);

		//Then
		verify(productDataMock).setMandatory(true);
	}

	@Test
	public void shouldPopulateMandatoryFieldIsFalse()
	{
		//Given
		when(productModelMock.getMandatory()).thenReturn(false);

		//When
		wileyProductMandatoryFlagPopulator.populate(productModelMock, productDataMock);

		//Then
		verify(productDataMock).setMandatory(false);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowExceptionWhenSourceIdIsNull()
	{
		wileyProductMandatoryFlagPopulator.populate(null, productDataMock);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowExceptionWhenTargetRequestIsNull()
	{
		wileyProductMandatoryFlagPopulator.populate(productModelMock, null);
	}
}

