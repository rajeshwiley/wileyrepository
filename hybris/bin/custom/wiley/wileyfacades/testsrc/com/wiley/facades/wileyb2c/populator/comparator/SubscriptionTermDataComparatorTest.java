package com.wiley.facades.wileyb2c.populator.comparator;

import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.subscriptionfacades.data.BillingPlanData;
import de.hybris.platform.subscriptionfacades.data.BillingTimeData;
import de.hybris.platform.subscriptionfacades.data.SubscriptionTermData;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Test;


/**
 * Author Herman_Chukhrai (EPAM)
 */
public class SubscriptionTermDataComparatorTest
{
	final SubscriptionTermDataComparator subscriptionTermDataComparator = new SubscriptionTermDataComparator();

	private enum BillingFrequency
	{
		DAILY("daily", 1),
		MONTHLY("monthly", 2),
		YEARLY("yearly", 3);

		private String code;
		private int order;

		BillingFrequency(final String code, final int order)
		{
			this.code = code;
			this.order = order;
		}

		public String getCode()
		{
			return code;
		}

		public int getOrder()
		{
			return order;
		}
	}

	@Test
	public void testSameFrequencyComparator() throws Exception
	{
		final SubscriptionTermData term1 = createTerm(BillingFrequency.DAILY, 1.01d);
		final SubscriptionTermData term2 = createTerm(BillingFrequency.DAILY, 1.02d);
		final int compare = subscriptionTermDataComparator.compare(term1, term2);
		assertTrue(compare > 0);
	}

	@Test
	public void testSameFrequencyAndPriceComparator() throws Exception
	{
		final SubscriptionTermData term1 = createTerm(BillingFrequency.DAILY, 123d);
		final SubscriptionTermData term2 = createTerm(BillingFrequency.DAILY, 123d);
		final int compare = subscriptionTermDataComparator.compare(term1, term2);
		assertTrue(compare == 0);
	}

	@Test
	public void testDifferentFrequencyComparator() throws Exception
	{
		final SubscriptionTermData term1 = createTerm(BillingFrequency.MONTHLY, 545d);
		final SubscriptionTermData term2 = createTerm(BillingFrequency.DAILY, 123d);
		final int compare = subscriptionTermDataComparator.compare(term1, term2);
		assertTrue(compare > 0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testNullTerm1Provided()
	{
		//Given
		final SubscriptionTermData term2 = createTerm(BillingFrequency.MONTHLY, 1d);

		//When
		subscriptionTermDataComparator.compare(null, term2);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testNullTerm2Provided()
	{
		//Given
		final SubscriptionTermData term1 = createTerm(BillingFrequency.DAILY, 1d);

		//When
		subscriptionTermDataComparator.compare(term1, null);
	}

	@Test
	public void testCollectionSort() throws Exception
	{

		final List<SubscriptionTermData> terms = Arrays.asList(
				createTerm(BillingFrequency.MONTHLY, 2d),
				createTerm(BillingFrequency.DAILY, 2d),
				createTerm(BillingFrequency.DAILY, 3d),
				createTerm(BillingFrequency.YEARLY, 1d),
				createTerm(BillingFrequency.MONTHLY, 1d),
				createTerm(BillingFrequency.YEARLY, 2d),
				createTerm(BillingFrequency.DAILY, 1d),
				createTerm(BillingFrequency.MONTHLY, 3d),
				createTerm(BillingFrequency.YEARLY, 3d));

		Collections.sort(terms, subscriptionTermDataComparator);

		assertEquals(9, terms.size());
		assertEquals("DAILY-3.0", terms.get(0).getId());
		assertEquals("DAILY-2.0", terms.get(1).getId());
		assertEquals("DAILY-1.0", terms.get(2).getId());
		assertEquals("MONTHLY-3.0", terms.get(3).getId());
		assertEquals("MONTHLY-2.0", terms.get(4).getId());
		assertEquals("MONTHLY-1.0", terms.get(5).getId());
		assertEquals("YEARLY-3.0", terms.get(6).getId());
		assertEquals("YEARLY-2.0", terms.get(7).getId());
		assertEquals("YEARLY-1.0", terms.get(8).getId());
	}

	private static SubscriptionTermData createTerm(final BillingFrequency frequency, final double priceValue)
	{
		final BillingTimeData billingTime = new BillingTimeData();
		billingTime.setCode(frequency.getCode());
		billingTime.setOrderNumber(frequency.getOrder());

		final PriceData price = new PriceData();
		price.setValue(new BigDecimal(priceValue));

		final BillingPlanData billingPlan = new BillingPlanData();
		billingPlan.setBillingTime(billingTime);

		final SubscriptionTermData termData = new SubscriptionTermData();
		termData.setBillingPlan(billingPlan);
		termData.setPrice(price);
		termData.setId(frequency.name() + "-" + priceValue);

		return termData;
	}
}