package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.variants.model.VariantProductModel;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.enums.WileyWebLinkTypeEnum;
import com.wiley.core.model.WileyPurchaseOptionProductModel;
import com.wiley.core.model.WileyWebLinkModel;
import com.wiley.core.wileyb2c.weblinks.WileyWebLinkService;
import com.wiley.facades.product.data.ExternalStoreData;


/**
 * Created by Raman_Hancharou on 6/19/2017.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cPdpVariantPurchaseOptionExternalStoresPopulatorUnitTest
{
	private static final String TEST_URL_1 = "testUrl1";
	private static final String TEST_URL_2 = "testUrl2";

	@Mock
	private WileyWebLinkService wileyWebLinkService;

	@Mock
	private WileyPurchaseOptionProductModel purchaseOptionMock;
	@Mock
	private VariantProductModel variantProductMock;
	@Mock
	private WileyWebLinkModel wileyWebLinkMock1;
	@Mock
	private WileyWebLinkModel wileyWebLinkMock2;


	@InjectMocks
	private Wileyb2cPdpVariantPurchaseOptionExternalStoresPopulator wileyb2cPdpVariantPurchaseOptionExternalStoresPopulator;

	@Test
	public void populateWhenExternalStoresExist() throws Exception
	{
		//Given
		when(purchaseOptionMock.getExternalStores()).thenReturn(Arrays.asList(wileyWebLinkMock1, wileyWebLinkMock2));
		when(wileyWebLinkService.getWebLinkUrl(wileyWebLinkMock1)).thenReturn(TEST_URL_1);
		when(wileyWebLinkService.getWebLinkUrl(wileyWebLinkMock2)).thenReturn(TEST_URL_2);
		when(wileyWebLinkMock1.getType()).thenReturn(WileyWebLinkTypeEnum.WOL_STORE);
		when(wileyWebLinkMock2.getType()).thenReturn(WileyWebLinkTypeEnum.GOOGLE_PLAY_STORE);
		VariantOptionData variantOptionData = new VariantOptionData();
		//When
		wileyb2cPdpVariantPurchaseOptionExternalStoresPopulator.populate(purchaseOptionMock, variantOptionData);
		//Then
		assertTrue(variantOptionData.getHasExternalStores());
		List<ExternalStoreData> externalStores = variantOptionData.getExternalStores();
		assertEquals(2, externalStores.size());
		assertEquals(WileyWebLinkTypeEnum.WOL_STORE.getCode(), externalStores.get(0).getType());
		assertEquals(WileyWebLinkTypeEnum.GOOGLE_PLAY_STORE.getCode(), externalStores.get(1).getType());
		assertEquals(TEST_URL_1, externalStores.get(0).getUrl());
		assertEquals(TEST_URL_2, externalStores.get(1).getUrl());
	}

	@Test
	public void populateWhenExternalStoresNotFound() throws Exception
	{
		//Given
		when(purchaseOptionMock.getExternalStores()).thenReturn(null);
		VariantOptionData variantOptionData = new VariantOptionData();
		//When
		wileyb2cPdpVariantPurchaseOptionExternalStoresPopulator.populate(purchaseOptionMock, variantOptionData);
		//Then
		assertFalse(variantOptionData.getHasExternalStores());
		assertNull(variantOptionData.getExternalStores());
	}

	@Test
	public void populateWhenNotWileyPurchaseOption() throws Exception
	{
		//Given
		VariantOptionData variantOptionData = new VariantOptionData();
		//When
		wileyb2cPdpVariantPurchaseOptionExternalStoresPopulator.populate(variantProductMock, variantOptionData);
		//Then
		assertNull(variantOptionData.getHasExternalStores());
		assertNull(variantOptionData.getExternalStores());
	}
}
