package com.wiley.facades.cms.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.cms2.servicelayer.services.CMSComponentService;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.europe1.model.DiscountRowModel;
import de.hybris.platform.jalo.order.price.PriceFactory;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Before;
import org.junit.Test;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.model.components.WileyPurchaseOptionProductListComponentModel;
import com.wiley.core.servicelayer.i18n.WileycomI18NService;
import com.wiley.core.wileyb2c.jalo.Wileyb2cEurope1PriceFactory;


/**
 * Default integration test for {@link WileyPurchaseOptionProductListComponentFacadeImpl}.
 *
 * Created by Maksim_Kozich on 31.05.17.
 */
@IntegrationTest
public class WileyPurchaseOptionProductListComponentFacadeImplIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	public static final double WCOM_PRODUCT_EBOOK_10_USD = 10.0;
	public static final double WCOM_PRODUCT_PAPERBACK_20_USD = 20.0;
	public static final double WCOM_PRODUCT_HARDCOVER_30_USD = 30.0;
	public static final double WCOM_PRODUCT_DOWNLOAD_40_USD = 40.0;
	protected static final String BASE_SITE = "wileyb2c";

	private static final String COMPONENT_UID_10_PERCENT = "testWileyPOPLComponent_10_percent";
	private static final String COMPONENT_UID_15_USD = "testWileyPOPLComponent_15_USD";
	private static final String COMPONENT_UID_20_EUR = "testWileyPOPLComponent_20_EUR";

	private static final String USD_ISO = "USD";
	private static final String EUR_ISO = "EUR";
	public static final String EBOOK_EXPECTED_PRICE_USD = "40.11";
	public static final String EBOOK_EXPECTED_PRICE_EUR = "41.11";
	public static final String PAPERBACK_EXPECTED_PRICE_USD = "50.11";
	public static final String PAPERBACK_EXPECTED_PRICE_EUR = "51.11";
	public static final String HARDCOVER_EXPECTED_PRICE_USD = "60.11";
	public static final String HARDCOVER_EXPECTED_PRICE_EUR = "61.11";
	public static final String EXPECTED_RELATIVE_DISCOUNT_VALUE = "10";
	public static final String EXPECTED_ABSOLUTE_DISCOUNT_USD_VALUE = "15.0";
	public static final String EXPECTED_ABSOLUTE_DISCOUNT_EUR_VALUE = "20.0";

	public static final String EBOOK_EXPECTED_STUDENT_PRICE_RELATIVE_DISCOUNT_USD = "36.10";
	public static final String EBOOK_EXPECTED_STUDENT_PRICE_RELATIVE_DISCOUNT_EUR = "37.00";
	public static final String PAPERBACK_EXPECTED_STUDENT_PRICE_RELATIVE_DISCOUNT_USD = "45.10";
	public static final String PAPERBACK_EXPECTED_STUDENT_PRICE_RELATIVE_DISCOUNT_EUR = "46.00";
	public static final String HARDCOVER_EXPECTED_STUDENT_PRICE_RELATIVE_DISCOUNT_USD = "54.10";
	public static final String HARDCOVER_EXPECTED_STUDENT_PRICE_RELATIVE_DISCOUNT_EUR = "55.00";

	public static final String EBOOK_EXPECTED_STUDENT_PRICE_ABSOLUTE_DISCOUNT_15_USD = "25.11";
	public static final String EBOOK_EXPECTED_STUDENT_PRICE_ABSOLUTE_DISCOUNT_20_EUR = "21.11";
	public static final String PAPERBACK_EXPECTED_STUDENT_PRICE_ABSOLUTE_DISCOUNT_15_USD = "35.11";
	public static final String PAPERBACK_EXPECTED_STUDENT_PRICE_ABSOLUTE_DISCOUNT_20_EUR = "31.11";
	public static final String HARDCOVER_EXPECTED_STUDENT_PRICE_ABSOLUTE_DISCOUNT_15_USD = "45.11";
	public static final String HARDCOVER_EXPECTED_STUDENT_PRICE_ABSOLUTE_DISCOUNT_20_EUR = "41.11";

	public static final String EBOOK_EXPECTED_STUDENT_PRICE_ABSOLUTE_DISCOUNT_10_USD = "30.11";
	public static final String PAPERBACK_EXPECTED_STUDENT_PRICE_ABSOLUTE_DISCOUNT_20_USD = "30.11";
	public static final String HARDCOVER_EXPECTED_STUDENT_PRICE_ABSOLUTE_DISCOUNT_30_USD = "30.11";

	public static final boolean EXPECTED_STUDENT_DISCOUNT_ABSOLUTE = true;
	public static final boolean EXPECTED_STUDENT_DISCOUNT_RELATIVE = false;

	public static final int EXPECTED_RESULT_SIZE = 1;
	public static final int EXPECTED_VARIANT_OPTIONS_SIZE = 4;
	public static final String WCOM_PRODUCT_EBOOK_CODE = "WCOM_PRODUCT_EBOOK";
	public static final String WCOM_PRODUCT_PAPERBACK_CODE = "WCOM_PRODUCT_PAPERBACK";
	public static final String WCOM_PRODUCT_HARDCOVER_CODE = "WCOM_PRODUCT_HARDCOVER";
	public static final String WCOM_PRODUCT_DOWNLOAD_CODE = "WCOM_PRODUCT_download";

	@Resource
	private WileyPurchaseOptionProductListComponentFacadeImpl wileyb2cPurchaseOptionProductListComponentFacade;

	@Resource(name = "cmsComponentService")
	private CMSComponentService cmsComponentService;

	@Resource
	private CommonI18NService commonI18NService;

	@Resource
	private BaseSiteService baseSiteService;

	@Resource
	private UserService userService;

	@Resource
	private ModelService modelService;

	@Resource
	private Wileyb2cEurope1PriceFactory wileyb2cEurope1PriceFactory;

	@Resource
	private WileycomI18NService wileycomI18NService;

	private PriceFactory sessionPriceFactoryBefore;

	@Before
	public void setUp() throws Exception
	{
		userService.setCurrentUser(userService.getAnonymousUser());
		baseSiteService.setCurrentBaseSite(BASE_SITE, true);
		wileycomI18NService.setDefaultCurrentCountry();
		importCsv("/wileyfacades/test/WileyPurchaseOptionProductListComponentFacadeImplIntegrationTest/component.impex",
				"UTF-8");
	}

	@Test
	public void testDataPopulatedProperlyRelativeDiscount() throws Exception
	{
		//given
		final WileyPurchaseOptionProductListComponentModel component10Percent = cmsComponentService.getSimpleCMSComponent(
				COMPONENT_UID_10_PERCENT);

		//when
		commonI18NService.setCurrentCurrency(commonI18NService.getCurrency(USD_ISO));

		List<ProductData> productDataList10Percent = wileyb2cPurchaseOptionProductListComponentFacade.getComponentProducts(
				component10Percent);

		//then
		assertTrue(CollectionUtils.isNotEmpty(productDataList10Percent));
		assertEquals(EXPECTED_RESULT_SIZE, productDataList10Percent.size());

		List<VariantOptionData> variantOptions = productDataList10Percent.get(0).getVariantOptions();
		assertTrue(CollectionUtils.isNotEmpty(variantOptions));
		assertEquals(EXPECTED_VARIANT_OPTIONS_SIZE, variantOptions.size());
		assertProductDataPopulated(variantOptions,
				WCOM_PRODUCT_HARDCOVER_CODE,
				USD_ISO,
				HARDCOVER_EXPECTED_PRICE_USD,
				EXPECTED_STUDENT_DISCOUNT_RELATIVE,
				EXPECTED_RELATIVE_DISCOUNT_VALUE,
				HARDCOVER_EXPECTED_STUDENT_PRICE_RELATIVE_DISCOUNT_USD);
		assertProductDataPopulated(variantOptions,
				WCOM_PRODUCT_PAPERBACK_CODE,
				USD_ISO,
				PAPERBACK_EXPECTED_PRICE_USD,
				EXPECTED_STUDENT_DISCOUNT_RELATIVE,
				EXPECTED_RELATIVE_DISCOUNT_VALUE,
				PAPERBACK_EXPECTED_STUDENT_PRICE_RELATIVE_DISCOUNT_USD);
		assertProductDataPopulated(variantOptions,
				WCOM_PRODUCT_EBOOK_CODE,
				USD_ISO,
				EBOOK_EXPECTED_PRICE_USD,
				EXPECTED_STUDENT_DISCOUNT_RELATIVE,
				EXPECTED_RELATIVE_DISCOUNT_VALUE,
				EBOOK_EXPECTED_STUDENT_PRICE_RELATIVE_DISCOUNT_USD);

		//when
		commonI18NService.setCurrentCurrency(commonI18NService.getCurrency(EUR_ISO));

		productDataList10Percent = wileyb2cPurchaseOptionProductListComponentFacade.getComponentProducts(
				component10Percent);

		//then
		assertTrue(CollectionUtils.isNotEmpty(productDataList10Percent));
		assertEquals(EXPECTED_RESULT_SIZE, productDataList10Percent.size());

		variantOptions = productDataList10Percent.get(0).getVariantOptions();
		assertTrue(CollectionUtils.isNotEmpty(variantOptions));
		assertEquals(EXPECTED_VARIANT_OPTIONS_SIZE, variantOptions.size());
		assertProductDataPopulated(variantOptions,
				WCOM_PRODUCT_HARDCOVER_CODE,
				EUR_ISO,
				HARDCOVER_EXPECTED_PRICE_EUR,
				EXPECTED_STUDENT_DISCOUNT_RELATIVE,
				EXPECTED_RELATIVE_DISCOUNT_VALUE,
				HARDCOVER_EXPECTED_STUDENT_PRICE_RELATIVE_DISCOUNT_EUR);
		assertProductDataPopulated(variantOptions,
				WCOM_PRODUCT_PAPERBACK_CODE,
				EUR_ISO,
				PAPERBACK_EXPECTED_PRICE_EUR,
				EXPECTED_STUDENT_DISCOUNT_RELATIVE,
				EXPECTED_RELATIVE_DISCOUNT_VALUE,
				PAPERBACK_EXPECTED_STUDENT_PRICE_RELATIVE_DISCOUNT_EUR);
		assertProductDataPopulated(variantOptions,
				WCOM_PRODUCT_EBOOK_CODE,
				EUR_ISO,
				EBOOK_EXPECTED_PRICE_EUR,
				EXPECTED_STUDENT_DISCOUNT_RELATIVE,
				EXPECTED_RELATIVE_DISCOUNT_VALUE,
				EBOOK_EXPECTED_STUDENT_PRICE_RELATIVE_DISCOUNT_EUR);
	}

	@Test
	public void testDataPopulatedProperlyAbsoluteDiscountUSD() throws Exception
	{
		//given
		final WileyPurchaseOptionProductListComponentModel component15USD = cmsComponentService.getSimpleCMSComponent(
				COMPONENT_UID_15_USD);

		//when
		commonI18NService.setCurrentCurrency(commonI18NService.getCurrency(USD_ISO));

		List<ProductData> productDataList15USD = wileyb2cPurchaseOptionProductListComponentFacade.getComponentProducts(
				component15USD);

		//then
		assertTrue(CollectionUtils.isNotEmpty(productDataList15USD));
		assertEquals(EXPECTED_RESULT_SIZE, productDataList15USD.size());

		List<VariantOptionData> variantOptions = productDataList15USD.get(0).getVariantOptions();
		assertTrue(CollectionUtils.isNotEmpty(variantOptions));
		assertEquals(EXPECTED_VARIANT_OPTIONS_SIZE, variantOptions.size());
		assertProductDataPopulated(variantOptions,
				WCOM_PRODUCT_HARDCOVER_CODE,
				USD_ISO,
				HARDCOVER_EXPECTED_PRICE_USD,
				EXPECTED_STUDENT_DISCOUNT_ABSOLUTE,
				EXPECTED_ABSOLUTE_DISCOUNT_USD_VALUE,
				HARDCOVER_EXPECTED_STUDENT_PRICE_ABSOLUTE_DISCOUNT_15_USD);
		assertProductDataPopulated(variantOptions,
				WCOM_PRODUCT_PAPERBACK_CODE,
				USD_ISO,
				PAPERBACK_EXPECTED_PRICE_USD,
				EXPECTED_STUDENT_DISCOUNT_ABSOLUTE,
				EXPECTED_ABSOLUTE_DISCOUNT_USD_VALUE,
				PAPERBACK_EXPECTED_STUDENT_PRICE_ABSOLUTE_DISCOUNT_15_USD);
		assertProductDataPopulated(variantOptions,
				WCOM_PRODUCT_EBOOK_CODE,
				USD_ISO,
				EBOOK_EXPECTED_PRICE_USD,
				EXPECTED_STUDENT_DISCOUNT_ABSOLUTE,
				EXPECTED_ABSOLUTE_DISCOUNT_USD_VALUE,
				EBOOK_EXPECTED_STUDENT_PRICE_ABSOLUTE_DISCOUNT_15_USD);

		//when
		commonI18NService.setCurrentCurrency(commonI18NService.getCurrency(EUR_ISO));

		productDataList15USD = wileyb2cPurchaseOptionProductListComponentFacade.getComponentProducts(
				component15USD);

		//then
		assertTrue(CollectionUtils.isNotEmpty(productDataList15USD));
		assertEquals(EXPECTED_RESULT_SIZE, productDataList15USD.size());

		variantOptions = productDataList15USD.get(0).getVariantOptions();
		assertTrue(CollectionUtils.isNotEmpty(variantOptions));
		assertEquals(EXPECTED_VARIANT_OPTIONS_SIZE, variantOptions.size());
		assertProductDataPopulatedWithoutDiscount(variantOptions, WCOM_PRODUCT_HARDCOVER_CODE, EUR_ISO,
				HARDCOVER_EXPECTED_PRICE_EUR);
		assertProductDataPopulatedWithoutDiscount(variantOptions, WCOM_PRODUCT_PAPERBACK_CODE, EUR_ISO,
				PAPERBACK_EXPECTED_PRICE_EUR);
		assertProductDataPopulatedWithoutDiscount(variantOptions, WCOM_PRODUCT_EBOOK_CODE, EUR_ISO, EBOOK_EXPECTED_PRICE_EUR);
	}

	@Test
	public void testDataPopulatedProperlyAbsoluteDiscountEUR() throws Exception
	{
		//given
		final WileyPurchaseOptionProductListComponentModel component20EUR = cmsComponentService.getSimpleCMSComponent(
				COMPONENT_UID_20_EUR);

		//when
		commonI18NService.setCurrentCurrency(commonI18NService.getCurrency(EUR_ISO));

		List<ProductData> productDataList20EUR = wileyb2cPurchaseOptionProductListComponentFacade.getComponentProducts(
				component20EUR);

		//then
		assertTrue(CollectionUtils.isNotEmpty(productDataList20EUR));
		assertEquals(EXPECTED_RESULT_SIZE, productDataList20EUR.size());

		List<VariantOptionData> variantOptions = productDataList20EUR.get(0).getVariantOptions();
		assertTrue(CollectionUtils.isNotEmpty(variantOptions));
		assertEquals(EXPECTED_VARIANT_OPTIONS_SIZE, variantOptions.size());
		assertProductDataPopulated(variantOptions,
				WCOM_PRODUCT_HARDCOVER_CODE,
				EUR_ISO,
				HARDCOVER_EXPECTED_PRICE_EUR,
				EXPECTED_STUDENT_DISCOUNT_ABSOLUTE,
				EXPECTED_ABSOLUTE_DISCOUNT_EUR_VALUE,
				HARDCOVER_EXPECTED_STUDENT_PRICE_ABSOLUTE_DISCOUNT_20_EUR);
		assertProductDataPopulated(variantOptions,
				WCOM_PRODUCT_PAPERBACK_CODE,
				EUR_ISO,
				PAPERBACK_EXPECTED_PRICE_EUR,
				EXPECTED_STUDENT_DISCOUNT_ABSOLUTE,
				EXPECTED_ABSOLUTE_DISCOUNT_EUR_VALUE,
				PAPERBACK_EXPECTED_STUDENT_PRICE_ABSOLUTE_DISCOUNT_20_EUR);
		assertProductDataPopulated(variantOptions,
				WCOM_PRODUCT_EBOOK_CODE,
				EUR_ISO,
				EBOOK_EXPECTED_PRICE_EUR,
				EXPECTED_STUDENT_DISCOUNT_ABSOLUTE,
				EXPECTED_ABSOLUTE_DISCOUNT_EUR_VALUE,
				EBOOK_EXPECTED_STUDENT_PRICE_ABSOLUTE_DISCOUNT_20_EUR);

		//when
		commonI18NService.setCurrentCurrency(commonI18NService.getCurrency(USD_ISO));

		productDataList20EUR = wileyb2cPurchaseOptionProductListComponentFacade.getComponentProducts(
				component20EUR);

		//then
		assertTrue(CollectionUtils.isNotEmpty(productDataList20EUR));
		assertEquals(EXPECTED_RESULT_SIZE, productDataList20EUR.size());

		variantOptions = productDataList20EUR.get(0).getVariantOptions();
		assertTrue(CollectionUtils.isNotEmpty(variantOptions));
		assertEquals(EXPECTED_VARIANT_OPTIONS_SIZE, variantOptions.size());
		assertProductDataPopulatedWithoutDiscount(variantOptions, WCOM_PRODUCT_HARDCOVER_CODE, USD_ISO,
				HARDCOVER_EXPECTED_PRICE_USD);
		assertProductDataPopulatedWithoutDiscount(variantOptions, WCOM_PRODUCT_PAPERBACK_CODE, USD_ISO,
				PAPERBACK_EXPECTED_PRICE_USD);
		assertProductDataPopulatedWithoutDiscount(variantOptions, WCOM_PRODUCT_EBOOK_CODE, USD_ISO, EBOOK_EXPECTED_PRICE_USD);
	}

	@Test
	public void testDataPopulatedWhenDiscountRowsDiffers() throws Exception
	{
		//given
		final WileyPurchaseOptionProductListComponentModel component10Percent = cmsComponentService.getSimpleCMSComponent(
				COMPONENT_UID_10_PERCENT);

		final List<DiscountRowModel> discountRows = component10Percent.getDiscountRows();
		discountRows.stream().forEach(row ->
				{
					row.setCurrency(commonI18NService.getCurrency(USD_ISO));
					switch (row.getProduct().getCode())
					{
						case WCOM_PRODUCT_EBOOK_CODE:
							row.setValue(WCOM_PRODUCT_EBOOK_10_USD);
							break;
						case WCOM_PRODUCT_PAPERBACK_CODE:
							row.setValue(WCOM_PRODUCT_PAPERBACK_20_USD);
							break;
						case WCOM_PRODUCT_HARDCOVER_CODE:
							row.setValue(WCOM_PRODUCT_HARDCOVER_30_USD);
							break;
						case WCOM_PRODUCT_DOWNLOAD_CODE:
							row.setValue(WCOM_PRODUCT_DOWNLOAD_40_USD);
							break;
						default:
							break;
					}
					modelService.save(row);
				}
		);

		//when
		commonI18NService.setCurrentCurrency(commonI18NService.getCurrency(USD_ISO));

		final List<ProductData> productDataList10Percent = wileyb2cPurchaseOptionProductListComponentFacade.getComponentProducts(
				component10Percent);

		//then
		assertTrue(CollectionUtils.isNotEmpty(productDataList10Percent));
		assertEquals(EXPECTED_RESULT_SIZE, productDataList10Percent.size());

		final List<VariantOptionData> variantOptions = productDataList10Percent.get(0).getVariantOptions();
		assertTrue(CollectionUtils.isNotEmpty(variantOptions));
		assertEquals(EXPECTED_VARIANT_OPTIONS_SIZE, variantOptions.size());
		assertProductDataPopulated(variantOptions,
				WCOM_PRODUCT_HARDCOVER_CODE,
				USD_ISO,
				HARDCOVER_EXPECTED_PRICE_USD,
				EXPECTED_STUDENT_DISCOUNT_RELATIVE,
				EXPECTED_RELATIVE_DISCOUNT_VALUE,
				HARDCOVER_EXPECTED_STUDENT_PRICE_ABSOLUTE_DISCOUNT_30_USD);
		assertProductDataPopulated(variantOptions,
				WCOM_PRODUCT_PAPERBACK_CODE,
				USD_ISO,
				PAPERBACK_EXPECTED_PRICE_USD,
				EXPECTED_STUDENT_DISCOUNT_RELATIVE,
				EXPECTED_RELATIVE_DISCOUNT_VALUE,
				PAPERBACK_EXPECTED_STUDENT_PRICE_ABSOLUTE_DISCOUNT_20_USD);
		assertProductDataPopulated(variantOptions,
				WCOM_PRODUCT_EBOOK_CODE,
				USD_ISO,
				EBOOK_EXPECTED_PRICE_USD,
				EXPECTED_STUDENT_DISCOUNT_RELATIVE,
				EXPECTED_RELATIVE_DISCOUNT_VALUE,
				EBOOK_EXPECTED_STUDENT_PRICE_ABSOLUTE_DISCOUNT_10_USD);
	}

	private void assertProductDataPopulated(final List<VariantOptionData> variantOptions, final String expectedProductCode,
			final String expectedCurrencyIso, final String expectedPrice, final boolean expectedStudentDiscountAbsolute,
			final String expectedStudentDiscountValue, final String expectedStudentPriceValue)
	{

		final VariantOptionData productData = assertVariantOptionsContainOptionWithExpectedProductCode(variantOptions,
				expectedProductCode);

		final PriceData priceData = productData.getPriceData();
		assertNotNull(priceData);
		assertEquals(expectedCurrencyIso, priceData.getCurrencyIso());
		assertEquals(new BigDecimal(expectedPrice), priceData.getValue());

		assertNotNull(productData.getStudentPrice());
		assertEquals(new BigDecimal(expectedStudentPriceValue), productData.getStudentPrice().getValue());
		assertEquals(expectedCurrencyIso, productData.getStudentPrice().getCurrencyIso());
	}

	private void assertProductDataPopulatedWithoutDiscount(final List<VariantOptionData> variantOptions,
			final String expectedProductCode, final String expectedCurrencyIso, final String expectedPrice)
	{
		final VariantOptionData productData = assertVariantOptionsContainOptionWithExpectedProductCode(variantOptions,
				expectedProductCode);

		final PriceData priceData = productData.getPriceData();
		assertNotNull(priceData);
		assertEquals(expectedCurrencyIso, priceData.getCurrencyIso());
		assertEquals(new BigDecimal(expectedPrice), priceData.getValue());

		//		assertNull(productData.getStudentDiscount());
		assertNull(productData.getStudentPrice());
	}

	private VariantOptionData assertVariantOptionsContainOptionWithExpectedProductCode(
			final List<VariantOptionData> variantOptions, final String expectedProductCode)
	{
		final List<VariantOptionData> variantOptionsWithExpectedProductCode = variantOptions.stream()
				.filter(variantOptionData -> expectedProductCode.equals(variantOptionData.getCode()))
				.collect(Collectors.toList());
		assertNotNull(variantOptionsWithExpectedProductCode);
		assertEquals(1, variantOptionsWithExpectedProductCode.size());

		final VariantOptionData productData = variantOptionsWithExpectedProductCode.get(0);
		assertNotNull(productData);
		assertEquals(expectedProductCode, productData.getCode());
		return productData;
	}
}
