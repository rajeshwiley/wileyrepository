package com.wiley.facades.populators;

import com.wiley.facades.converters.ProductNodesConverter;
import com.wiley.facades.product.node.ProductRootNode;
import com.wiley.facades.wel.product.WelProductFacade;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.util.PriceValue;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;

import static org.junit.Assert.assertSame;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Test suite for {@link WelComparisonPricePopulator}
 */
@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class WelComparisonPricePopulatorTest
{
	private static final String CURRENCY_ISO = "usd";
	private static final double PRICE_SET_ONE = 10.0d;
	private static final double PRICE_SET_TWO = 10.1d;
	
	@Mock
	private PriceDataFactory mockPriceDataFactory;
	@Mock
	private ProductNodesConverter productNodesConverterMock;
	@Mock
	private WelProductFacade welProductFacade;
	
	@InjectMocks
	private WelComparisonPricePopulator underTest = new WelComparisonPricePopulator();


	@Mock
	private ProductModel mockProduct;
	@Mock
	private PriceInformation mockPriceSetOne;
	@Mock
	private PriceInformation mockPriceSetTwo;
	@Mock
	private PriceData mockPriceData;
	@Mock
	private PriceData mockPriceDataSetOne;
	@Mock
	private PriceData mockPriceDataSetTwo;
	@Mock
	private ProductRootNode rootNodeMock;

	private ProductData productData = new ProductData();


	@Before
	public void setUp()
	{
		underTest.setPriceDataFactory(mockPriceDataFactory);
		
		setupPrice(mockPriceSetOne, mockPriceDataSetOne, PRICE_SET_ONE, PriceDataType.BUY);
		setupPrice(mockPriceSetTwo, mockPriceDataSetTwo, PRICE_SET_TWO, PriceDataType.BUY);

		doReturn(rootNodeMock).when(productNodesConverterMock).convert(productData);
		productData.setPrice(mockPriceData);
		doReturn(PriceDataType.BUY).when(mockPriceData).getPriceType();
		doReturn(CURRENCY_ISO).when(mockPriceData).getCurrencyIso();
	}

	@Test
	public void shouldPopulateDefaultPrices()
	{
		doReturn(PRICE_SET_ONE).when(welProductFacade).getPriceForDefaultSetProduct(any());
		doReturn(PRICE_SET_TWO).when(welProductFacade).getPriceForDefaultPart(any());
		mockPriceFactoryInvocation(mockPriceDataSetOne, PRICE_SET_ONE, productData.getPrice().getPriceType());

		underTest.populate(mockProduct, productData);

		assertSame(mockPriceDataSetOne, productData.getPriceCompleteSet());
		assertSame(mockPriceDataSetTwo, productData.getPricePerPart());
	}

	private void setupPrice(final PriceInformation priceInfo, final PriceData priceData,
							final double price, final PriceDataType type)
	{
		PriceValue pv = mock(PriceValue.class);
		when(pv.getCurrencyIso()).thenReturn(CURRENCY_ISO);
		when(pv.getValue()).thenReturn(price);
		when(priceInfo.getPriceValue()).thenReturn(pv);
		mockPriceFactoryInvocation(priceData, price, type);
	}

	private void mockPriceFactoryInvocation(final PriceData priceData, double price, final PriceDataType type)
	{
		when(mockPriceDataFactory.create(
				eq(type),
				eq(BigDecimal.valueOf(price)),
				eq(CURRENCY_ISO))).thenReturn(priceData);
	}
}
