package com.wiley.facades.urlresolver.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.services.BaseStoreService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.junit.Test;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.locale.WileyLocaleService;
import com.wiley.core.model.WileyExternalSiteLinkModel;
import com.wiley.facades.common.LocalizedUrlData;
import com.wiley.facades.urlresolver.WileyUrlResolutionFacade;


/**
 * Created by Uladzimir_Barouski on 6/16/2017.
 */
@IntegrationTest
public class WileyUrlResolutionFacadeImpIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	private static final String SERVLET_PATH_WITH_PARAMS = "/anypage?anyparam=value";
	private static final String WILEYB2C = "wileyb2c";
	private static final String WEBSITE_URL_KEY = "website.wileyb2c.https";
	public static final String X_DEFAULT = "x-default";
	@Resource
	private WileyUrlResolutionFacade wileyUrlResolutionFacade;
	@Resource
	private BaseSiteService baseSiteService;
	@Resource
	private BaseStoreService baseStoreService;
	@Resource
	private WileyLocaleService wileyLocaleService;
	@Resource
	private ConfigurationService configurationService;

	@Test
	public void getAllLocalizedUrls() throws Exception
	{
		//Given
		baseSiteService.setCurrentBaseSite(WILEYB2C, true);
		List<String> locales = wileyLocaleService.getAllEncodedLocales();
		locales.add(X_DEFAULT);
		//When
		List<LocalizedUrlData> localizedUrlDatas = wileyUrlResolutionFacade.getAllLocalizedUrls(SERVLET_PATH_WITH_PARAMS, true);
		//Then
		List<String> localesFromUrl = localizedUrlDatas.stream().map(LocalizedUrlData::getLocale).collect(Collectors.toList());
		String siteURL = configurationService.getConfiguration().getString(WEBSITE_URL_KEY);
		Set<WileyExternalSiteLinkModel> externalLinks = baseStoreService.getCurrentBaseStore().getExternalLinks();
		Set<CountryModel> externalLinksCountries = externalLinks.stream().map(link -> link.getCountry()).collect(
				Collectors.toSet());
		assertEquals(locales.size(), localesFromUrl.size() + externalLinks.size());
		assertTrue("List equality without order", locales.containsAll(localesFromUrl));
		localizedUrlDatas.stream().forEach(localized ->
		{
			if (X_DEFAULT.equals(localized.getLocale()))
			{
				assertEquals(siteURL + "/" + wileyLocaleService.getDefaultEncodedLocale() + SERVLET_PATH_WITH_PARAMS,
						localized.getUrl());
			}
			else
			{
				assertEquals(siteURL + "/" + localized.getLocale() + SERVLET_PATH_WITH_PARAMS, localized.getUrl());
				CountryModel localizedCountry = wileyLocaleService.getCountryForEncodedLocale(localized.getLocale());
				assertFalse(externalLinksCountries.contains(localizedCountry));
			}
		});
	}
}