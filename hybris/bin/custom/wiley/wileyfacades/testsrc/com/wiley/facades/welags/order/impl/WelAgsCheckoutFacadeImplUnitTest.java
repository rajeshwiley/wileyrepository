package com.wiley.facades.welags.order.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.DeliveryModeData;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.order.CommerceCheckoutService;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.commerceservices.strategies.CheckoutCustomerStrategy;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.paypal.hybris.data.ResultErrorData;
import com.paypal.hybris.facade.PayPalCheckoutFacade;
import com.paypal.hybris.model.PaypalPaymentInfoModel;
import com.wiley.core.customer.service.WileyCustomerAccountService;
import com.wiley.core.delivery.WileyDeliveryService;
import com.wiley.core.enums.WileyProductLifecycleEnum;
import com.wiley.core.i18n.WileyCountryService;
import com.wiley.core.model.WileyVariantProductModel;
import com.wiley.core.order.WileyCheckoutService;
import com.wiley.core.product.WileyProductEditionFormatService;
import com.wiley.core.wel.preorder.strategy.ProductLifecycleStatusStrategy;
import com.wiley.facades.checkout.enums.BreadcrumbItem;
import com.wiley.facades.payment.PaymentAuthorizationResultData;
import com.wiley.facades.wel.order.WelMultiDimensionalCartFacade;

import static com.paypal.hybris.facade.data.PayPalPaymentResultData.failure;
import static com.paypal.hybris.facade.data.PayPalPaymentResultData.success;


/**
 * Default unit test for {@link WelAgsCheckoutFacadeImpl}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WelAgsCheckoutFacadeImplUnitTest
{

	public static final String TEST_PRODUCT_CODE = "TEST_PRODUCT_CODE";
	public static final double TEST_TOTAL_PRICE = 1000.0;
	@Mock
	private CartService cartServiceMock;

	@Mock
	private ModelService modelServiceMock;

	@Mock
	private UserService userServiceMock;

	@Mock
	private WileyCountryService wileyCountryServiceMock;

	@Mock
	private WileyDeliveryService wileyDeliveryService;

	@Mock
	private CommerceCheckoutService commerceCheckoutServiceMock;

	@Mock
	private WileyCheckoutService wileyCheckoutServiceMock;

	@Mock
	private ProductService productServiceMock;

	@Mock
	private CartFacade cartFacade;

	@Mock
	private Converter<DeliveryModeModel, DeliveryModeData> deliveryModeConverter;

	@Mock
	private CommerceCartService commerceCartServiceMock;

	@Mock
	private BaseStoreService baseStoreServiceMock;

	@Mock
	private CheckoutCustomerStrategy checkoutCustomerStrategyMock;

	@Mock
	private WileyCustomerAccountService customerAccountServiceMock;

	@Mock
	private PayPalCheckoutFacade payPalCheckoutFacadeMock;

	@Mock
	private WileyProductEditionFormatService wileyProductEditionFormatService;

	@Mock
	private ProductLifecycleStatusStrategy welProductLifecycleStatusStrategyMock;

	@Mock
	private WelMultiDimensionalCartFacade welMultiDimensionalcartFacadeMock;

	@Spy
	@InjectMocks
	private WelAgsCheckoutFacadeImpl wileyCheckoutFacade;

	// Test data

	@Mock
	private CartModel cartModelMock;

	@Mock
	private OrderModel orderModelMock;

	@Mock
	private CustomerModel customerModelMock;

	@Mock
	private PaypalPaymentInfoModel payPalPaymentInfoMock;

	@Mock
	private BaseStoreModel baseStoreModelMock;

	@Mock
	private AbstractOrderEntryModel orderEntryModelMock;

	@Mock
	private WileyVariantProductModel wileyVariantProductModel;

	@Mock
	private ProductModel productModelMock;

	private static final String SECURITY_TOKEN = "";
	private static final String PAY_PAL_ERROR_1 = "payPal error 1";
	private static final String PAY_PAL_ERROR_2 = "payPal error 2";

	public static final String CART_CODE = "cartCode";
	public static final String ORDER_CODE = "orderCode";

	@Before
	public void setUp() throws Exception
	{
		when(deliveryModeConverter.convert(any(DeliveryModeModel.class))).thenAnswer(invocation -> {
			final DeliveryModeModel deliveryMode = (DeliveryModeModel) invocation.getArguments()[0];
			DeliveryModeData deliveryModeData = new DeliveryModeData();
			deliveryModeData.setCode(deliveryMode.getCode());
			return deliveryModeData;
		});

		wileyCheckoutFacade.setDeliveryModeConverter(deliveryModeConverter);
		wileyCheckoutFacade.setWileyProductEditionFormatService(wileyProductEditionFormatService);
		when(cartModelMock.getCode()).thenReturn(CART_CODE);
		when(cartModelMock.getUser()).thenReturn(customerModelMock);
		when(cartServiceMock.getSessionCart()).thenReturn(cartModelMock);
		when(userServiceMock.getCurrentUser()).thenReturn(customerModelMock);
		when(commerceCartServiceMock.getCartForCodeAndUser(CART_CODE, customerModelMock)).thenReturn(cartModelMock);
		when(baseStoreServiceMock.getCurrentBaseStore()).thenReturn(baseStoreModelMock);
		when(customerAccountServiceMock.getOrderForCode(customerModelMock, ORDER_CODE, baseStoreModelMock))
				.thenReturn(orderModelMock);
		when(checkoutCustomerStrategyMock.getCurrentUserForCheckout()).thenReturn(customerModelMock);
		when(cartFacade.hasSessionCart()).thenReturn(true);
	}

	@Test
	public void shouldSubscribeCurrentCustomerToUpdates()
	{
		// Given
		when(cartServiceMock.hasSessionCart()).thenReturn(true);

		// When
		wileyCheckoutFacade.subscribeCurrentCustomerToUpdates();

		// Then
		verify(cartModelMock).setSubscribeToUpdates(eq(true));
		verify(modelServiceMock).save(cartModelMock);
	}

	@Test
	public void shouldUnsubscribeCurrentCustomerFromUpdates()
	{
		// Given
		when(cartServiceMock.hasSessionCart()).thenReturn(true);

		// When
		wileyCheckoutFacade.unsubscribeCurrentCustomerFromUpdates();

		// Then
		verify(cartModelMock).setSubscribeToUpdates(eq(false));
		verify(modelServiceMock).save(cartModelMock);
	}

	@Test
	public void shouldSubscribeToGMACInfo()
	{
		// Given
		when(cartServiceMock.hasSessionCart()).thenReturn(true);

		// When
		wileyCheckoutFacade.subscribeToGMACInfo(true);

		// Then
		verify(cartModelMock).setSubscribeGmacInfo(eq(true));
		verify(modelServiceMock).save(cartModelMock);
	}

	@Test
	public void shouldUnsubscribeToGMACInfo()
	{
		// Given
		when(cartServiceMock.hasSessionCart()).thenReturn(true);

		// When
		wileyCheckoutFacade.subscribeToGMACInfo(false);

		// Then
		verify(cartModelMock).setSubscribeGmacInfo(eq(false));
		verify(modelServiceMock).save(cartModelMock);
	}

	@Test
	public void shouldNotSaveSubscriptionStatusIsSessionCartNotExists()
	{
		// Given
		when(cartServiceMock.hasSessionCart()).thenReturn(false);

		// When
		wileyCheckoutFacade.subscribeCurrentCustomerToUpdates();

		// Then
		verify(cartModelMock, never()).setSubscribeToUpdates(anyBoolean());
	}

	@Test
	public void shouldSetDeliveryModeIfNotAvailable()
	{
		// Given
		// prepare cart data
		when(cartServiceMock.hasSessionCart()).thenReturn(true);
		when(userServiceMock.getCurrentUser()).thenReturn(customerModelMock);
		when(cartFacade.hasSessionCart()).thenReturn(true);

		// prepare default shipment address
		AddressModel addressModelMock = mock(AddressModel.class);
		when(customerModelMock.getDefaultShipmentAddress()).thenReturn(addressModelMock);

		// prepare country for shipment address
		CountryModel countryModelMock = mock(CountryModel.class);
		final String countryIsocode = "US-RL";
		when(countryModelMock.getIsocode()).thenReturn(countryIsocode);
		when(addressModelMock.getCountry()).thenReturn(countryModelMock);
		when(wileyCountryServiceMock.findCountryByCode(eq(countryIsocode))).thenReturn(countryModelMock);

		// prepare delivery mode
		DeliveryModeModel deliveryModeModelMock = mock(DeliveryModeModel.class);
		final String deliveryModelCode = "deliveryModelCode";
		when(deliveryModeModelMock.getCode()).thenReturn(deliveryModelCode);
		when(wileyDeliveryService
				.getSupportedDeliveryModes(any(BaseStoreModel.class), any(CurrencyModel.class), eq(countryModelMock))).thenReturn(
				Arrays.asList(deliveryModeModelMock));
		when(wileyDeliveryService.getDeliveryModeForCode(eq(deliveryModelCode))).thenReturn(deliveryModeModelMock);

		// When
		wileyCheckoutFacade.setDeliveryModeIfNotAvailable();

		// Then
		verify(commerceCheckoutServiceMock).setDeliveryMode(argThat(new ArgumentMatcher<CommerceCheckoutParameter>()
		{
			@Override
			public boolean matches(final Object argument)
			{
				CommerceCheckoutParameter parameter = (CommerceCheckoutParameter) argument;
				final DeliveryModeModel deliveryMode = parameter.getDeliveryMode();
				return deliveryModeModelMock.equals(deliveryMode);
			}
		}));
	}

	@Test
	public void testIsDeliveryAddressSetToCart()
	{
		// Given
		when(cartServiceMock.hasSessionCart()).thenReturn(true);

		AddressModel addressModelMock = mock(AddressModel.class);
		when(cartModelMock.getDeliveryAddress()).thenReturn(addressModelMock);

		// When
		final boolean result = wileyCheckoutFacade.isDeliveryAddressSetToCart();

		// Then
		assertTrue("Expected that cart has delivery address.", result);
		verify(cartModelMock).getDeliveryAddress();
	}

	@Test
	public void testIsDeliveryAddressSetToCartIfCartNotHaveDeliveryAddress()
	{
		// Given
		when(cartServiceMock.hasSessionCart()).thenReturn(true);

		AddressModel addressModelMock = mock(AddressModel.class);
		when(cartModelMock.getDeliveryAddress()).thenReturn(null);

		// When
		final boolean result = wileyCheckoutFacade.isDeliveryAddressSetToCart();

		// Then
		assertFalse("Expected that cart doesn't have delivery address.", result);
		verify(cartModelMock).getDeliveryAddress();
	}

	@Test
	public void testGetBreadcrumbLabelsWithDigitalCartAndStudentFlow()
	{
		// Given
		when(wileyCheckoutFacade.isDigitalCart(CART_CODE)).thenReturn(true);
		when(wileyCheckoutFacade.isStudentFlow(CART_CODE)).thenReturn(true);
		when(wileyCheckoutServiceMock.isNonZeroOrder(cartModelMock)).thenReturn(true);

		// When
		final List<String> breadcrumbLabels = wileyCheckoutFacade.getBreadcrumbLabels();

		// Then
		assertEquals(5, breadcrumbLabels.size());
		assertEquals(BreadcrumbItem.BILLING.getLabelKey(), breadcrumbLabels.get(1));
		assertEquals(BreadcrumbItem.STUDENT_VERIFICATION.getLabelKey(), breadcrumbLabels.get(2));
	}

	@Test
	public void testGetBreadcrumbLabelsWithDigitalCartAndNotStudentFlow()
	{
		// Given
		when(wileyCheckoutFacade.isDigitalCart(CART_CODE)).thenReturn(true);
		when(wileyCheckoutFacade.isStudentFlow(CART_CODE)).thenReturn(false);
		when(wileyCheckoutServiceMock.isNonZeroOrder(cartModelMock)).thenReturn(true);

		// When
		final List<String> breadcrumbLabels = wileyCheckoutFacade.getBreadcrumbLabels();

		// Then
		assertEquals(4, breadcrumbLabels.size());
		assertEquals(BreadcrumbItem.BILLING.getLabelKey(), breadcrumbLabels.get(1));
		assertEquals(BreadcrumbItem.PAYMENT.getLabelKey(), breadcrumbLabels.get(2));
	}

	@Test
	public void testGetBreadcrumbLabelsWithNotDigitalCartAndStudentFlow()
	{
		// Given
		when(wileyCheckoutFacade.isDigitalCart(CART_CODE)).thenReturn(false);
		when(wileyCheckoutFacade.isStudentFlow(CART_CODE)).thenReturn(true);
		when(wileyCheckoutServiceMock.isNonZeroOrder(cartModelMock)).thenReturn(true);

		// When
		final List<String> breadcrumbLabels = wileyCheckoutFacade.getBreadcrumbLabels();

		// Then
		assertEquals(5, breadcrumbLabels.size());
		assertEquals(BreadcrumbItem.BILLING_AND_SHIPPING.getLabelKey(), breadcrumbLabels.get(1));
		assertEquals(BreadcrumbItem.STUDENT_VERIFICATION.getLabelKey(), breadcrumbLabels.get(2));
	}

	@Test
	public void testGetBreadcrumbLabelsWithNotDigitalCartAndNotStudentFlow()
	{
		// Given
		when(wileyCheckoutFacade.isDigitalCart(CART_CODE)).thenReturn(false);
		when(wileyCheckoutFacade.isStudentFlow(CART_CODE)).thenReturn(false);
		when(wileyCheckoutServiceMock.isNonZeroOrder(cartModelMock)).thenReturn(true);

		// When
		final List<String> breadcrumbLabels = wileyCheckoutFacade.getBreadcrumbLabels();

		// Then
		assertEquals(4, breadcrumbLabels.size());
		assertEquals(BreadcrumbItem.BILLING_AND_SHIPPING.getLabelKey(), breadcrumbLabels.get(1));
		assertEquals(BreadcrumbItem.PAYMENT.getLabelKey(), breadcrumbLabels.get(2));
	}

	@Test
	public void testGetBreadcrumbLabelsWithZeroOrderFlowShouldNotContainPaymentStep()
	{
		// Given
		when(wileyCheckoutServiceMock.isNonZeroOrder(orderModelMock)).thenReturn(false);
		// When
		final List<String> breadcrumbLabels = wileyCheckoutFacade.getBreadcrumbLabels(ORDER_CODE);
		// Then
		assertFalse(breadcrumbLabels.contains(BreadcrumbItem.PAYMENT.getLabelKey()));
	}

	@Test
	public void testAuthorizePaymentAndProvideResultShouldProceedForPayPalWithSuccessResponse()
	{
		// Given
		givenCartWith(payPalPaymentInfoMock);
		when(payPalCheckoutFacadeMock.authorizePaymentAndProvideResult(SECURITY_TOKEN)).thenReturn(success());

		// When
		PaymentAuthorizationResultData authorizationResult = wileyCheckoutFacade.authorizePaymentAndProvideResult(SECURITY_TOKEN);

		// Then
		assertTrue(authorizationResult.isSuccess());
	}

	@Test
	public void testAuthorizePaymentAndProvideResultShouldProceedForPayPalWithFailureResponse()
	{
		// Given
		givenCartWith(payPalPaymentInfoMock);
		when(payPalCheckoutFacadeMock.authorizePaymentAndProvideResult(SECURITY_TOKEN)).thenReturn(
				failure(Arrays.asList(givenPayPalResultErrorData(PAY_PAL_ERROR_1), givenPayPalResultErrorData(PAY_PAL_ERROR_2))));

		// When
		PaymentAuthorizationResultData authorizationResult = wileyCheckoutFacade.authorizePaymentAndProvideResult(SECURITY_TOKEN);

		// Then
		assertEquals(PAY_PAL_ERROR_1, authorizationResult.getErrors().get(0));
		assertEquals(PAY_PAL_ERROR_2, authorizationResult.getErrors().get(1));
	}

	private void givenCartWith(final PaymentInfoModel paymentInfoModel)
	{
		when(cartModelMock.getPaymentInfo()).thenReturn(paymentInfoModel);
	}

	private ResultErrorData givenPayPalResultErrorData(final String longErrorMessage)
	{
		final ResultErrorData resultErrorData = new ResultErrorData();
		resultErrorData.setLongMessage(longErrorMessage);
		return resultErrorData;
	}

	@Test
	public void testClearCartForPreOrderProduct()
	{
		//given
		doReturn(WileyProductLifecycleEnum.PRE_ORDER).when(welProductLifecycleStatusStrategyMock).getLifecycleStatusForProduct(
				TEST_PRODUCT_CODE);
		doNothing().when(welMultiDimensionalcartFacadeMock).clearSessionCart();
		doReturn(true).when(cartFacade).hasEntries();

		//when
		boolean isCartCleared = wileyCheckoutFacade.clearCartForPreOrderProduct(TEST_PRODUCT_CODE);

		//then
		assertTrue(isCartCleared);
	}

	@Test
	public void testClearCartWithPreOrderProduct()
	{
		//given
		doReturn(productModelMock).when(productServiceMock).getProductForCode(TEST_PRODUCT_CODE);
		doReturn(WileyProductLifecycleEnum.ACTIVE).when(welProductLifecycleStatusStrategyMock).getLifecycleStatusForProduct(
				productModelMock);
		doReturn(true).when(wileyCheckoutFacade).isPreOrderCart();
		when(cartModelMock.getEntries()).thenReturn(Arrays.asList(orderEntryModelMock));
		when(orderEntryModelMock.getProduct()).thenReturn(wileyVariantProductModel);
		when(cartModelMock.getTotalPrice()).thenReturn(TEST_TOTAL_PRICE);
		doAnswer((Answer) invocation -> {
			cartModelMock.setEntries(new ArrayList<>());
			cartModelMock.setTotalPrice(0.0);
			return null;
		}).when(welMultiDimensionalcartFacadeMock).clearSessionCart();

		//when
		boolean isCartCleared = wileyCheckoutFacade.clearCartForPreOrderProduct(TEST_PRODUCT_CODE);

		//then
		assertTrue(isCartCleared);
	}
}
