package com.wiley.facades.as.i18n.comparators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.data.RegionData;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class RegionIsocodeComparatorUnitTest
{
	public static final String ISO_2 = "iso 2";
	public static final String ISO_1 = "iso 1";
	public static final String ISO = "iso";

	@Mock
	private RegionData regionDataMock1;

	@Mock
	private RegionData regionDataMock2;

	@InjectMocks
	private RegionIsocodeComparator testInstance;

	@Test
	public void testEqual()
	{
		when(regionDataMock1.getIsocode()).thenReturn(ISO);
		when(regionDataMock2.getIsocode()).thenReturn(ISO);

		int result = testInstance.compare(regionDataMock1, regionDataMock2);

		assertEquals(result, 0);
	}

	@Test
	public void testNotEqual()
	{
		when(regionDataMock1.getIsocode()).thenReturn(ISO_1);
		when(regionDataMock2.getIsocode()).thenReturn(ISO_2);

		int result = testInstance.compare(regionDataMock1, regionDataMock2);

		assertNotEquals(result, 0);
	}

	@Test
	public void testBothNullAreEqual()
	{
		int result = testInstance.compare(regionDataMock1, regionDataMock2);

		assertEquals(result, 0);
	}

	@Test
	public void testFistNullNotEqual()
	{
		when(regionDataMock1.getIsocode()).thenReturn(null);
		when(regionDataMock2.getIsocode()).thenReturn(ISO);

		int result = testInstance.compare(regionDataMock1, regionDataMock2);

		assertNotEquals(result, 0);
	}

	@Test
	public void testSecondNullNotEqual()
	{
		when(regionDataMock1.getIsocode()).thenReturn(ISO);
		when(regionDataMock2.getIsocode()).thenReturn(null);

		int result = testInstance.compare(regionDataMock1, regionDataMock2);

		assertNotEquals(result, 0);
	}
}
