package com.wiley.facades.partner.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.enums.KpmgFunction;
import com.wiley.core.model.KpmgEnrollmentModel;
import com.wiley.core.model.KpmgOfficeModel;
import com.wiley.core.partner.WileyKPMGEnrollmentService;
import com.wiley.facades.partner.kpmg.KPMGEnrollmentData;
import com.wiley.facades.partner.kpmg.KPMGFunctionData;
import com.wiley.facades.partner.kpmg.KPMGOfficeData;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyKPMGEnrollmentFacadeImplTest
{
	private static final String PARTNER_ID = "FGCU";
	private static final java.lang.String OFFICE_CODE = "officeCode";
	private static final java.lang.String OFFICE_NAME = "officeName";

	@InjectMocks
	private WileyKPMGEnrollmentFacadeImpl testedInstance = new WileyKPMGEnrollmentFacadeImpl();

	@Mock
	private WileyKPMGEnrollmentService wileyKpmgEnrollmentService;
	@Mock
	private KpmgOfficeModel officeModel;
	@Mock
	private ModelService modelService;
	@Mock
	private CartService cartService;

	private KPMGEnrollmentData enrollmentData = new KPMGEnrollmentData();
	@Mock
	private CartModel cart;
	@Mock
	private KpmgEnrollmentModel enrollmentModel;
	@Mock
	private Converter<KPMGEnrollmentData, KpmgEnrollmentModel> kpmgEnrollmentDataConverter;

	@Test
	public void getFunctionsShouldReturnListOfFunctions()
	{
		//When
		final List<KPMGFunctionData> functions = testedInstance.getFunctions();
		//Then
		final Iterator<KPMGFunctionData> iterator = functions.iterator();
		assertEquals(iterator.next().getCode(), KpmgFunction.AUDIT.toString());
		assertEquals(iterator.next().getCode(), KpmgFunction.ADVISORY.toString());
		assertEquals(iterator.next().getCode(), KpmgFunction.TAX.toString());
		assertEquals(iterator.next().getCode(), KpmgFunction.BTG.toString());
	}

	@Test
	public void getOfficesShouldReturnListOfOffices()
	{
		//Given
		when(officeModel.getCode()).thenReturn(OFFICE_CODE);
		when(officeModel.getName()).thenReturn(OFFICE_NAME);
		when(wileyKpmgEnrollmentService.getOffices(PARTNER_ID)).thenReturn(Arrays.asList(officeModel));
		//When
		final List<KPMGOfficeData> offices = testedInstance.getOffices(PARTNER_ID);
		//Then
		assertEquals(offices.get(0).getCode(), OFFICE_CODE);
		assertEquals(offices.get(0).getName(), OFFICE_NAME);
	}

	@Test
	public void getOfficesShouldReturnEmptyList()
	{
		//Given
		when(wileyKpmgEnrollmentService.getOffices(PARTNER_ID)).thenReturn(Arrays.asList());
		//When
		final List<KPMGOfficeData> offices = testedInstance.getOffices(PARTNER_ID);
		//Then
		assertTrue(offices.isEmpty());
	}

	@Test(expected = IllegalArgumentException.class)
	public void getOfficesShouldThrowExceptionIfPartnerIdIsNull()
	{
		//When
		testedInstance.getOffices(null);
	}

	@Test
	public void shouldSaveEnrollmentDataToCurrentCart()
	{
		//Given
		when(cartService.getSessionCart()).thenReturn(cart);
		when(kpmgEnrollmentDataConverter.convert(enrollmentData)).thenReturn(enrollmentModel);
		//When
		testedInstance.saveEnrollmentDataToCurrentCart(enrollmentData);
		//Then
		verify(cart).setKpmgEnrollment(enrollmentModel);
		verify(modelService).saveAll(enrollmentModel, cart);
	}


}