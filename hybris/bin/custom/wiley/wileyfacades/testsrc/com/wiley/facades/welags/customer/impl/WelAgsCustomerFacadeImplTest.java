/**
 *
 */
package com.wiley.facades.welags.customer.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.strategies.CustomerNameStrategy;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.customer.ResetPasswordRedirectType;
import com.wiley.facades.welags.customer.WelAgsCustomerFacade;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;



@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WelAgsCustomerFacadeImplTest
{
	private static final String TEST_DUMMY = "dummy";
	private static final String TEST_FIRST_NAME = "First Name";
	private static final String TEST_LAST_NAME = "Last Name";
	private static final String TEST_LOGIN = "Login";
	private static final String TEST_PASSWORD = "Password";
	private static final LanguageModel LANGUAGE_MODEL = new LanguageModel();
	private static final CurrencyModel CURRENCY_MODEL = new CurrencyModel();


	@Mock
	private ModelService mockModelService;
	@Mock
	private CustomerAccountService customerAccountService;
	@Mock
	private CommonI18NService commonI18NService;
	@Mock
	private CustomerNameStrategy customerNameStrategy;

	@InjectMocks
	private WelAgsCustomerFacade testInstance = new WelAgsCustomerFacadeImpl();

	@Test(expected = IllegalArgumentException.class)
	public void testForgottenPasswordShouldThrowUnknownIdentifierExceptionIfUidIsNullOrEmptyString() throws Exception
	{
		testInstance.forgottenPassword(null, ResetPasswordRedirectType.lp);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testForgottenPasswordShouldThrowUnknownIdentifierExceptionIfTypeIsNullOrEmptyString() throws Exception
	{
		testInstance.forgottenPassword(TEST_DUMMY, null);
	}

	@Test
	public void testRegister() throws DuplicateUidException
	{
		final RegisterData data = new RegisterData();
		data.setFirstName(TEST_FIRST_NAME);
		data.setLastName(TEST_LAST_NAME);
		data.setLogin(TEST_LOGIN);
		data.setPassword(TEST_PASSWORD);

		final CustomerModel model = new CustomerModel();
		given(mockModelService.create(CustomerModel.class)).willReturn(model);
		given(commonI18NService.getCurrentLanguage()).willReturn(LANGUAGE_MODEL);
		given(commonI18NService.getCurrentCurrency()).willReturn(CURRENCY_MODEL);

		testInstance.register(data);

		ArgumentCaptor<CustomerModel> argument = ArgumentCaptor.forClass(CustomerModel.class);

		verify(customerAccountService).register(argument.capture(), eq(TEST_PASSWORD));
		Assert.assertEquals(TEST_LOGIN.toLowerCase(), argument.getValue().getUid());
		Assert.assertEquals(TEST_LOGIN, argument.getValue().getOriginalUid());
		Assert.assertEquals(TEST_FIRST_NAME, argument.getValue().getFirstName());
		Assert.assertEquals(TEST_LAST_NAME, argument.getValue().getLastName());
		Assert.assertNull(argument.getValue().getName());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testRegisterErr() throws DuplicateUidException
	{
		final RegisterData data = new RegisterData();
		testInstance.register(data);
	}
}
