package com.wiley.facades.user.converters.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyAddressReversePopulatorTest
{
	@InjectMocks
	private WileyAddressReversePopulator testedInstance;

	@Mock
	private CommonI18NService commonI18NService;
	@Mock
	private FlexibleSearchService flexibleSearchService;

	private AddressData addressData = new AddressData();

	@Mock
	private AddressModel addressModel;

	@Test
	public void shouldResetRegion()
	{
		//Given addressData.getRegion is null
		//When
		testedInstance.populate(addressData, addressModel);
		//Then
		verify(addressModel).setRegion(null);
	}


}