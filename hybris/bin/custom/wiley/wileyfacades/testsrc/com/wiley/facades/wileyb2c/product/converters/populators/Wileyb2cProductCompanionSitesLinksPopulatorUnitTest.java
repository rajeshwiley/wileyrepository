package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.enums.WileyWebLinkTypeEnum;
import com.wiley.core.model.WileyPurchaseOptionProductModel;
import com.wiley.core.model.WileyWebLinkModel;
import com.wiley.core.wileyb2c.weblinks.WileyWebLinkService;
import com.wiley.facades.wileyb2c.product.converters.populators.helper.Wileyb2cProductCollectionAttributesHelper;
import com.wiley.facades.wileyb2c.product.data.WebLinkData;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cProductCompanionSitesLinksPopulatorUnitTest
{
	private static final String EMPTY_URL = "#";
	private static final String EVAL_LINK = "eval_link";
	private static final String CONT_REP_LINK = "cont_rep_link";
	private static final String INST_LINK = "inst_link";
	private static final String STUDENT_LINK = "student_link";
	@Mock
	private WileyWebLinkService mockWebLinkService;
	@Mock
	private WileyWebLinkModel mockEvalLinkModel;
	@Mock
	private WileyWebLinkModel mockInstructorLinkModel;
	@Mock
	private WileyWebLinkModel mockStudentLinkModel;
	@Mock
	private WileyWebLinkModel mockContactRepLinkModel;
	@Mock
	private WileyPurchaseOptionProductModel variantProduct;
	@Mock
	private Wileyb2cProductCollectionAttributesHelper wileyb2cProductCollectionAttributesHelper;

	private ProductData productData = new ProductData();

	@InjectMocks
	private Wileyb2cProductCompanionSitesLinksPopulator underTest = new Wileyb2cProductCompanionSitesLinksPopulator();


	@Before
	public void setup()
	{
		when(mockEvalLinkModel.getType()).thenReturn(WileyWebLinkTypeEnum.REQUEST_EVALUATION_COPY);
		when(mockContactRepLinkModel.getType()).thenReturn(WileyWebLinkTypeEnum.CONTACT_REPRESENTATIVE);
		when(mockInstructorLinkModel.getType()).thenReturn(WileyWebLinkTypeEnum.INSTRUCTORS_COMPANION_SITE);
		when(mockStudentLinkModel.getType()).thenReturn(WileyWebLinkTypeEnum.STUDENTS_COMPANION_SITE);

		when(mockWebLinkService.getWebLinkUrl(mockEvalLinkModel)).thenReturn(EVAL_LINK);
		when(mockWebLinkService.getWebLinkUrl(mockContactRepLinkModel)).thenReturn(CONT_REP_LINK);
		when(mockWebLinkService.getWebLinkUrl(mockInstructorLinkModel)).thenReturn(INST_LINK);
		when(mockWebLinkService.getWebLinkUrl(mockStudentLinkModel)).thenReturn(STUDENT_LINK);

		setUpWebLinks(Arrays.asList(mockEvalLinkModel, mockContactRepLinkModel, mockInstructorLinkModel, mockStudentLinkModel));
	}

	@Test
	public void shouldPopulateStudentLinks()
	{
		underTest.populate(variantProduct, productData);
		assertEquals(1, productData.getStudentLinks().size());
		checkLink(WileyWebLinkTypeEnum.STUDENTS_COMPANION_SITE, STUDENT_LINK, productData.getStudentLinks().get(0));

	}

	@Test
	public void shouldPopulateInstructorLinks()
	{
		underTest.populate(variantProduct, productData);
		final List<WebLinkData> instructorLinks = productData.getInstructorLinks();
		assertEquals(2, instructorLinks.size());

		checkLink(WileyWebLinkTypeEnum.INSTRUCTORS_COMPANION_SITE, INST_LINK, instructorLinks.get(0));
		checkLink(WileyWebLinkTypeEnum.CONTACT_REPRESENTATIVE, CONT_REP_LINK, instructorLinks.get(1));
	}

	@Test
	public void shouldPopulateEvaluationCopyLinks()
	{
		underTest.populate(variantProduct, productData);
		final List<WebLinkData> evaluationCopyLinks = productData.getEvaluationCopyLinks();
		assertEquals(1, evaluationCopyLinks.size());

		checkLink(WileyWebLinkTypeEnum.REQUEST_EVALUATION_COPY, EVAL_LINK, evaluationCopyLinks.get(0));

	}

	@Test
	public void shouldReplaceNullUrl()
	{
		when(mockWebLinkService.getWebLinkUrl(mockStudentLinkModel)).thenReturn(null);
		underTest.populate(variantProduct, productData);
		assertEquals(1, productData.getStudentLinks().size());
		checkLink(WileyWebLinkTypeEnum.STUDENTS_COMPANION_SITE, EMPTY_URL, productData.getStudentLinks().get(0));
	}

	@Test
	public void shouldNotPopulateInstructorLinks()
	{
		setUpWebLinks(Arrays.asList(mockContactRepLinkModel));
		underTest.populate(variantProduct, productData);
		assertTrue(productData.getInstructorLinks().isEmpty());
		setUpWebLinks(Arrays.asList(mockContactRepLinkModel, mockInstructorLinkModel, mockStudentLinkModel));
	}

	private void checkLink(final WileyWebLinkTypeEnum type, final String url, final WebLinkData link)
	{
		assertEquals(type, link.getType());
		assertEquals(url, link.getUrl());
	}

	private void setUpWebLinks(final List<WileyWebLinkModel> webLinks)
	{
		when(wileyb2cProductCollectionAttributesHelper.getProductAttribute(variantProduct, ProductModel.WEBLINKS))
				.thenReturn(webLinks);
	}
}
