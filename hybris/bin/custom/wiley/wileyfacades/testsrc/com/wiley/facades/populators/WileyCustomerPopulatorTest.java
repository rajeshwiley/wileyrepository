package com.wiley.facades.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.CustomerModel;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;


@UnitTest
public class WileyCustomerPopulatorTest
{
	@Mock
	private Populator<CustomerModel, CustomerData> customerPopulator;

	private WileyCustomerPopulator wileyCustomerPopulator;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		wileyCustomerPopulator = new WileyCustomerPopulator();
		wileyCustomerPopulator.setCustomerPopulator(customerPopulator);
	}

	@Test
	public void testPopulateNames()
	{
		final CustomerModel customerModel = mock(CustomerModel.class);
		final CustomerData customerData = new CustomerData();

		given(customerModel.getName()).willReturn("name");
		given(customerModel.getFirstName()).willReturn("first name");
		given(customerModel.getLastName()).willReturn("last name");
		given(customerModel.getCurrentEmployerSchool()).willReturn("employer/school");

		wileyCustomerPopulator.populate(customerModel, customerData);
		Assert.assertEquals("name", customerData.getName());
		Assert.assertEquals("first name", customerData.getFirstName());
		Assert.assertEquals("last name", customerData.getLastName());
		Assert.assertEquals("employer/school", customerData.getCurrentEmployerSchool());
	}
}
