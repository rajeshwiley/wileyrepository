package com.wiley.facades.order.converters.populator;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import org.junit.Before;

import javax.annotation.Resource;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


/**
 * Test for {@link WileySubscriptionOrderPopulator}
 */
@IntegrationTest
public class WileySubscriptionOrderPopulatorIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	@Resource
	private WileySubscriptionOrderPopulator wileySubscriptionOrderPopulator;

	@Resource
	private FlexibleSearchService flexibleSearchService;

	private OrderModel source = new OrderModel();

	//test data
	private static final Double SUBTOTAL_WITHOUT_DISCOUNT_TEST = 12.0;
	private static final String TEST_ORDER = "test_hybris_order";

	@Before
	public void setUp() throws Exception
	{
		importCsv("/wileyfacades/test/WileySubscriptionOrderPopulatorIntegrationTest/testOrder.impex", DEFAULT_ENCODING);

		final OrderModel orderTemplate = new OrderModel();
		orderTemplate.setCode(TEST_ORDER);
		source = flexibleSearchService.getModelByExample(orderTemplate);

		source.setSubTotalWithoutDiscount(SUBTOTAL_WITHOUT_DISCOUNT_TEST);
	}

	//Disabled due to fix in other branch
	//@Test
	// TODO: check with release/v2.1 version
	public void testPopulate()
	{
		OrderData target = new OrderData();

		wileySubscriptionOrderPopulator.populate(source, target);

		assertTrue(SUBTOTAL_WITHOUT_DISCOUNT_TEST.equals(target.getSubTotalWithoutDiscount().getValue().doubleValue()));
		assertEquals(1, target.getOrderPrices().size());
		assertTrue(SUBTOTAL_WITHOUT_DISCOUNT_TEST.equals(
				target.getOrderPrices().get(0).getSubTotalWithoutDiscount().getValue().doubleValue()));
	}
}