package com.wiley.facades.wileyb2c.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyPurchaseOptionProductModel;


/**
 * Unit test for {@link Wileyb2cProductTabsPopulator}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cProductTabsPopulatorUnitTest
{

	private static final String TEST_PRESS_RELEASE = "testPressRelease";
	private static final String TEST_NEW_TO_EDITION = "testNewToEdition";
	private static final String TEST_REVIEWS = "testReviews";
	private static final String TEST_ERRATA = "testErrata";
	private static final String TEST_RELATED_WEBSITES = "testRelatedWebsites";
	private static final String TEST_NOTES = "testNotes";
	private static final String TEST_WHATS_NEW = "testWhatsNew";
	private static final String TEST_TABLE_OF_CONTENT = "testTableOfContent";
	public static final String EMPTY = "";
	private static final String TEST_DOWNLOADS_HTML =
			"<table>"
					+ "<tr>"
					+ "<td>Sample Download Title</td>"
					+ "<td><a href=\"http://media.wiley.com/product_ancillary/47/11193087/DOWNLOAD/sample.pdf\" "
					+ "target=\"_blank\">Download</a></td>"
					+ "</tr>"
					+ "</table>";

	@InjectMocks
	private Wileyb2cProductTabsPopulator wileyb2cProductTabsPopulator;
	@Mock
	private WileyPurchaseOptionProductModel variantProduct;
	@Mock
	private ProductModel baseProduct;
	@Mock
	private ProductData productDataMock;
	@Mock
	private ModelService modelService;

	@Before
	public void setUp()
	{
		when(variantProduct.getBaseProduct()).thenReturn(baseProduct);

		when(modelService.getAttributeValue(baseProduct, ProductModel.TABLEOFCONTENTS)).thenReturn(TEST_TABLE_OF_CONTENT);
		when(modelService.getAttributeValue(baseProduct, ProductModel.NEWTOEDITION)).thenReturn(TEST_NEW_TO_EDITION);
		when(modelService.getAttributeValue(baseProduct, ProductModel.REVIEWS)).thenReturn(TEST_REVIEWS);
		when(modelService.getAttributeValue(baseProduct, ProductModel.ERRATA)).thenReturn(TEST_ERRATA);
		when(modelService.getAttributeValue(baseProduct, ProductModel.RELATEDWEBSITES)).thenReturn(TEST_RELATED_WEBSITES);
		when(modelService.getAttributeValue(baseProduct, ProductModel.NOTES)).thenReturn(TEST_NOTES);
		when(modelService.getAttributeValue(baseProduct, ProductModel.WHATSNEW)).thenReturn(TEST_WHATS_NEW);
		when(modelService.getAttributeValue(baseProduct, ProductModel.PRESSRELEASE)).thenReturn(TEST_PRESS_RELEASE);
		when(modelService.getAttributeValue(baseProduct, ProductModel.DOWNLOADSTAB)).thenReturn(TEST_DOWNLOADS_HTML);
	}

	@Test
	public void setTestSuccess()
	{
		wileyb2cProductTabsPopulator.populate(variantProduct, productDataMock);
		verify(productDataMock).setPressRelease(TEST_PRESS_RELEASE);
		verify(productDataMock).setNewToEdition(TEST_NEW_TO_EDITION);
		verify(productDataMock).setWileyReviews(TEST_REVIEWS);
		verify(productDataMock).setErrata(TEST_ERRATA);
		verify(productDataMock).setRelatedWebsites(TEST_RELATED_WEBSITES);
		verify(productDataMock).setNotes(TEST_NOTES);
		verify(productDataMock).setWhatsNew(TEST_WHATS_NEW);
		verify(productDataMock).setTableOfContents(TEST_TABLE_OF_CONTENT);
		verify(productDataMock).setDownloadsTab(TEST_DOWNLOADS_HTML);
	}

	@Test
	public void testWhenSourceIsNull()
	{
		try
		{
			wileyb2cProductTabsPopulator.populate(null, productDataMock);
		}
		catch (IllegalArgumentException e)
		{
			// success
		}
		verify(productDataMock, never()).setPressRelease(anyString());
		verify(productDataMock, never()).setNewToEdition(anyString());
		verify(productDataMock, never()).setWileyReviews(anyString());
		verify(productDataMock, never()).setErrata(anyString());
		verify(productDataMock, never()).setRelatedWebsites(anyString());
		verify(productDataMock, never()).setNotes(anyString());
		verify(productDataMock, never()).setWhatsNew(anyString());
		verify(productDataMock, never()).setTableOfContents(anyString());
		verify(productDataMock, never()).setDownloadsTab(anyString());
	}

	@Test
	public void testWhenTargetIsNull()
	{
		try
		{
			wileyb2cProductTabsPopulator.populate(variantProduct, null);
		}
		catch (IllegalArgumentException e)
		{
			// success
		}
		verify(productDataMock, never()).setPressRelease(anyString());
		verify(productDataMock, never()).setNewToEdition(anyString());
		verify(productDataMock, never()).setWileyReviews(anyString());
		verify(productDataMock, never()).setErrata(anyString());
		verify(productDataMock, never()).setRelatedWebsites(anyString());
		verify(productDataMock, never()).setNotes(anyString());
		verify(productDataMock, never()).setWhatsNew(anyString());
		verify(productDataMock, never()).setTableOfContents(anyString());
		verify(productDataMock, never()).setDownloadsTab(anyString());
	}

	@Test
	public void populateWhenDownloadsHTMLMissed() throws Exception
	{
		//Given
		when(modelService.getAttributeValue(baseProduct, ProductModel.DOWNLOADSTAB)).thenReturn(null);
		ProductData productData = new ProductData();
		//When
		wileyb2cProductTabsPopulator.populate(variantProduct, productData);
		//Then
		assertEquals(productData.getDownloadsTab(), EMPTY);
	}
}
