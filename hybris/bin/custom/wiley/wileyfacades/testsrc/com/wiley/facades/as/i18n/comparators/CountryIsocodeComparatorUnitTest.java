package com.wiley.facades.as.i18n.comparators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.data.CountryData;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CountryIsocodeComparatorUnitTest
{
	public static final String ISO_2 = "iso 2";
	public static final String ISO_1 = "iso 1";
	public static final String ISO = "iso";
	@Mock
	private CountryData countryDataMock1;

	@Mock
	private CountryData countryDataMock2;

	@InjectMocks
	private CountryIsocodeComparator testInstance;

	@Test
	public void testEqual()
	{
		when(countryDataMock1.getIsocode()).thenReturn(ISO);
		when(countryDataMock2.getIsocode()).thenReturn(ISO);

		int result = testInstance.compare(countryDataMock1, countryDataMock2);

		assertEquals(result, 0);
	}

	@Test
	public void testNotEqual()
	{
		when(countryDataMock1.getIsocode()).thenReturn(ISO_1);
		when(countryDataMock2.getIsocode()).thenReturn(ISO_2);

		int result = testInstance.compare(countryDataMock1, countryDataMock2);

		assertNotEquals(result, 0);
	}

	@Test
	public void testBothNullAreEqual()
	{
		int result = testInstance.compare(countryDataMock1, countryDataMock2);

		assertEquals(result, 0);
	}

	@Test
	public void testFistNullNotEqual()
	{
		when(countryDataMock1.getIsocode()).thenReturn(null);
		when(countryDataMock2.getIsocode()).thenReturn(ISO);

		int result = testInstance.compare(countryDataMock1, countryDataMock2);

		assertNotEquals(result, 0);
	}

	@Test
	public void testSecondNullNotEqual()
	{
		when(countryDataMock1.getIsocode()).thenReturn(ISO);
		when(countryDataMock2.getIsocode()).thenReturn(null);

		int result = testInstance.compare(countryDataMock1, countryDataMock2);

		assertNotEquals(result, 0);
	}
}
