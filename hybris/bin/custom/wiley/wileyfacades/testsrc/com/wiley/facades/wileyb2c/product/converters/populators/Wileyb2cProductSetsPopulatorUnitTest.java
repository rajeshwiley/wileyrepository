package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.catalog.references.ProductReferenceService;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.wiley.restriction.WileyProductRestrictionService;
import com.wiley.facades.wileyb2c.product.data.ProductSetData;


/**
 * Created by Uladzimir_Barouski on 5/5/2017.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cProductSetsPopulatorUnitTest
{
	@Mock
	private ProductReferenceService productReferenceServiceMock;
	@Mock
	private Converter<ProductModel, ProductSetData> wileyb2cProductSetEntryConverterMock;
	@Mock
	private ProductModel productModelMock;
	@Mock
	private WileyProductRestrictionService wileyProductRestrictionService;

	@InjectMocks
	private Wileyb2cProductSetsPopulator wileyb2cProductSetsPopulator;


	@Test
	public void populateWhenSetsReferenceExist() throws Exception
	{
		//Given
		ProductReferenceModel referenceModel1 = mock(ProductReferenceModel.class);
		ProductSetData productSetData1 = new ProductSetData();
		initSetData(referenceModel1, productSetData1, true, true);

		ProductReferenceModel referenceModel2 = mock(ProductReferenceModel.class);
		ProductSetData productSetData2 = new ProductSetData();
		initSetData(referenceModel2, productSetData2, true, true);

		when(productReferenceServiceMock.getProductReferencesForSourceProduct(productModelMock,
				ProductReferenceTypeEnum.PRODUCT_SET, true))
				.thenReturn(Arrays.asList(referenceModel1, referenceModel2));
		//When
		ProductData productData = new ProductData();
		wileyb2cProductSetsPopulator.populate(productModelMock, productData);
		//Then
		assertNotNull(productData.getProductSets());
		assertEquals(2, productData.getProductSets().size());
		assertEquals(productSetData1, productData.getProductSets().get(0));
		assertEquals(productSetData2, productData.getProductSets().get(1));
	}

	@Test
	public void populateWhenSetsReferenceNotExist() throws Exception
	{
		//Given
		when(productReferenceServiceMock.getProductReferencesForSourceProduct(productModelMock,
				ProductReferenceTypeEnum.PRODUCT_SET, true))
				.thenReturn(Collections.emptyList());
		//When
		ProductData productData = new ProductData();
		wileyb2cProductSetsPopulator.populate(productModelMock, productData);
		//Then
		assertNotNull(productData.getProductSets());
		assertEquals(0, productData.getProductSets().size());
		verify(wileyb2cProductSetEntryConverterMock, never()).convert(eq(productModelMock));
	}

	@Test
	public void populateWhenSetNotVisibleAndNotAvalable() throws Exception
	{
		//Given
		ProductReferenceModel referenceModel1 = mock(ProductReferenceModel.class);
		ProductSetData productSetData1 = new ProductSetData();
		initSetData(referenceModel1, productSetData1, false, true);

		ProductReferenceModel referenceModel2 = mock(ProductReferenceModel.class);
		ProductSetData productSetData2 = new ProductSetData();
		initSetData(referenceModel2, productSetData2, true, false);

		when(productReferenceServiceMock.getProductReferencesForSourceProduct(productModelMock,
				ProductReferenceTypeEnum.PRODUCT_SET, true))
				.thenReturn(Arrays.asList(referenceModel1, referenceModel2));
		//When
		ProductData productData = new ProductData();
		wileyb2cProductSetsPopulator.populate(productModelMock, productData);
		//Then
		assertNotNull(productData.getProductSets());
		assertEquals(0, productData.getProductSets().size());
	}

	private void initSetData(final ProductReferenceModel referenceModel, final ProductSetData productSetData,
			final boolean isAvalable, final boolean isVisible)
	{
		ProductModel productSetModel = mock(ProductModel.class);
		when(referenceModel.getTarget()).thenReturn(productSetModel);
		when(wileyb2cProductSetEntryConverterMock.convert(productSetModel)).thenReturn(productSetData);
		when(wileyProductRestrictionService.isAvailable(productSetModel)).thenReturn(isAvalable);
		when(wileyProductRestrictionService.isVisible(productSetModel)).thenReturn(isVisible);
	}
}