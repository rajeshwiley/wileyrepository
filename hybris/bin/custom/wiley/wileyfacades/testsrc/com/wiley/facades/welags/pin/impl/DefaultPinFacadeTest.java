package com.wiley.facades.welags.pin.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.price.DiscountModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.user.UserService;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.isA;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.PinModel;
import com.wiley.core.pin.service.PinService;
import com.wiley.facades.ags.pin.impl.AgsAddToCartStrategy;
import com.wiley.facades.welags.pin.exception.PinOperationException;


/**
 * Unit test for {@link DefaultPinFacade}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultPinFacadeTest
{
	private static final String PIN_CODE = "test-pin-1";
	private static final String PRODUCT_CODE = "product-code-0001";
	private static final String CART_CODE = "cart-code";
	private static final String ORDER_CODE = "order-code";

	@Mock
	private PinService pinServiceMock;

	@Mock
	private ModelService modelServiceMock;

	@Mock
	private ProductService productServiceMock;

	@Mock
	private CommerceCartService commerceCartServiceMock;

	@Mock
	private AddToCartStrategyProvider addToCartStrategyProviderMock;

	@Mock
	private AgsAddToCartStrategy agsAddToCartStrategyMock;

	@Mock
	private PinModel pinModelMock;

	@Mock
	private DiscountModel discountModelMock;

	@Mock
	private CartModel cartModelMock;

	@Mock
	private OrderModel orderModelMock;

	@Mock
	private UserModel userModelMock;

	@Mock
	private ProductModel productModelMock;

	@Mock
	private CurrencyModel currencyMock;

	@Mock
	private FlexibleSearchService flexibleSearchService;

	@Mock
	private UserService userService;

	@InjectMocks
	private DefaultPinFacade pinFacade;

	@Before
	public void setUp()
	{
		when(pinServiceMock.getPinForCode(PIN_CODE)).thenReturn(pinModelMock);
		when(pinModelMock.getActivated()).thenReturn(Boolean.FALSE);
		when(pinModelMock.getProductCode()).thenReturn(PRODUCT_CODE);
		when(pinModelMock.getCode()).thenReturn(PIN_CODE);
		when(productServiceMock.getProductForCode(anyString())).thenReturn(productModelMock);
		when(productModelMock.getApprovalStatus()).thenReturn(ArticleApprovalStatus.APPROVED);
		when(addToCartStrategyProviderMock.getStrategyForPinWorkFlow()).thenReturn(agsAddToCartStrategyMock);
		when(cartModelMock.getCurrency()).thenReturn(currencyMock);
		when(orderModelMock.getEntries()).thenReturn(Arrays.asList(new OrderEntryModel()));
		when(modelServiceMock.create(DiscountModel.class)).thenReturn(discountModelMock);
		when(commerceCartServiceMock.calculateCart((CommerceCartParameter) anyObject())).thenReturn(Boolean.TRUE);
		when(userService.getCurrentUser()).thenReturn(userModelMock);
		when(flexibleSearchService.getModelByExample(isA(CartModel.class))).thenAnswer(invocation -> {
			CartModel cartModel = (CartModel) invocation.getArguments()[0];
			return CART_CODE.equals(cartModel.getCode()) && userModelMock.equals(cartModel.getUser()) ? cartModelMock : null;
		});
		when(flexibleSearchService.getModelByExample(isA(OrderModel.class))).thenAnswer(invocation -> {
			OrderModel orderModel = (OrderModel) invocation.getArguments()[0];
			return ORDER_CODE.equals(orderModel.getCode()) && userModelMock.equals(orderModel.getUser()) ? orderModelMock : null;
		});
	}

	@Test
	public void shouldAddProductAssignedToGivenPinToCart() throws PinOperationException
	{
		pinFacade.applyPin(CART_CODE, PIN_CODE);
		verify(agsAddToCartStrategyMock).addProductToCart(cartModelMock, pinModelMock);
	}

	@Test
	public void shouldAssignOrderToPin() throws PinOperationException
	{
		pinFacade.activatePin(ORDER_CODE, PIN_CODE);
		verify(pinModelMock).setOrder(orderModelMock);
		verify(modelServiceMock).save(pinModelMock);
	}

	@Test(expected = PinOperationException.class)
	public void shouldThrowExceptionIfPinIsInvalid() throws PinOperationException
	{
		when(pinServiceMock.getPinForCode(PIN_CODE)).thenThrow(UnknownIdentifierException.class);
		pinFacade.applyPin(CART_CODE, PIN_CODE);
	}

	@Test(expected = PinOperationException.class)
	public void shouldThrowExceptionIfPinIsAlreadyActivated() throws PinOperationException
	{
		when(pinModelMock.getActivated()).thenReturn(Boolean.TRUE);
		pinFacade.applyPin(CART_CODE, PIN_CODE);
	}

	@Test(expected = PinOperationException.class)
	public void shouldThrowExceptionIfProductIsNotValid() throws PinOperationException
	{
		when(productServiceMock.getProductForCode(anyString())).thenThrow(UnknownIdentifierException.class);
		pinFacade.applyPin(CART_CODE, PIN_CODE);
	}

	@Test(expected = PinOperationException.class)
	public void shouldThrowExceptionIfProductIsNotApproved() throws PinOperationException
	{
		when(productModelMock.getApprovalStatus()).thenReturn(ArticleApprovalStatus.UNAPPROVED);
		pinFacade.applyPin(CART_CODE, PIN_CODE);
	}

	@Test(expected = PinOperationException.class)
	public void shouldThrowExceptionIfOrderIsNotValid() throws PinOperationException
	{
		when(orderModelMock.getEntries()).thenReturn(Collections.emptyList());
		pinFacade.activatePin(ORDER_CODE, PIN_CODE);
	}
}
