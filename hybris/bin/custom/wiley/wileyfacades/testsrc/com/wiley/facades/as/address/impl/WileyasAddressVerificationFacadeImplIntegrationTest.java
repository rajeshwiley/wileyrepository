package com.wiley.facades.as.address.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.enums.PaymentModeEnum;
import com.wiley.facades.order.WileyCheckoutFacade;


@IntegrationTest
public class WileyasAddressVerificationFacadeImplIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	private static final String CART_GUID = "test-cart-guid";
	private static final String BASE_SITE = "asSite";
	private static final String CUSTOMER_ID = "test@test.com";

	@Resource
	private WileyasAddressVerificationFacadeImpl wileyasAddressVerificationFacade;

	@Resource
	private WileyCheckoutFacade wileyasCheckoutFacade;

	@Resource
	private BaseSiteService baseSiteService;

	@Resource
	private UserService userService;

	@Resource
	private CommerceCartService commerceCartService;

	@Resource
	private CartService cartService;

	@Before
	public void setUp() throws Exception
	{
		importCsv("/wileyfacades/test/WileyasAddressVerificationFacadeImplIntegrationTest/cart.impex",
				DEFAULT_ENCODING);

		final BaseSiteModel baseSite = baseSiteService.getBaseSiteForUID(BASE_SITE);
		final UserModel userForUID = userService.getUserForUID(CUSTOMER_ID);
		final CartModel cart = commerceCartService.getCartForGuidAndSiteAndUser(CART_GUID, baseSite, userForUID);
		baseSiteService.setCurrentBaseSite(baseSite, true);
		userService.setCurrentUser(userForUID);
		cartService.setSessionCart(cart);
	}

	@Test
	public void shouldNotCallAddressValidationForPayPal()
	{
		wileyasCheckoutFacade.setPaymentMode(PaymentModeEnum.PAYPAL);

		boolean result = wileyasAddressVerificationFacade.shouldCallCheckoutCartAddressValidation();

		Assert.assertFalse(result);
	}

	@Test
	public void shouldCallAddressValidationForInvoice()
	{
		shouldCallAddressValidationForPaymentMethod(PaymentModeEnum.INVOICE);
	}

	@Test
	public void shouldCallAddressValidationForProforma()
	{
		shouldCallAddressValidationForPaymentMethod(PaymentModeEnum.PROFORMA);
	}

	@Test
	public void shouldCallAddressValidationForCard()
	{
		shouldCallAddressValidationForPaymentMethod(PaymentModeEnum.CARD);
	}

	@Test
	public void shouldCallAddressValidationForNullPaymentMode()
	{
		boolean result = wileyasAddressVerificationFacade.shouldCallCheckoutCartAddressValidation();

		Assert.assertTrue(result);
	}

	protected void shouldCallAddressValidationForPaymentMethod(final PaymentModeEnum invoice)
	{
		wileyasCheckoutFacade.setPaymentMode(invoice);

		boolean result = wileyasAddressVerificationFacade.shouldCallCheckoutCartAddressValidation();

		Assert.assertTrue(result);
	}
}