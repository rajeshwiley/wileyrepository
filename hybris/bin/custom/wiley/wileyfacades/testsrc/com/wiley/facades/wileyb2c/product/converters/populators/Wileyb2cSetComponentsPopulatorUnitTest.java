package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.catalog.references.ProductReferenceService;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.ConfigurablePopulator;
import de.hybris.platform.core.model.product.ProductModel;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyPurchaseOptionProductModel;
import com.wiley.core.wileyb2c.classification.Wileyb2cClassificationService;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;


/**
 * Created by Uladzimir_Barouski on 5/5/2017.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cSetComponentsPopulatorUnitTest
{
	private static final String CLASSIFICATION_CLASS_NAME = "classification class name";
	@Mock
	private ProductReferenceService productReferenceServiceMock;
	@Mock
	private ConfigurablePopulator<ProductModel, ProductData, ProductOption> wileyb2cProductConfiguredPopulatorMock;
	@Mock(answer = Answers.RETURNS_DEEP_STUBS)
	private Wileyb2cClassificationService wileyb2cClassificationServiceMock;
	@Mock
	private ProductModel productModelMock;

	@InjectMocks
	private Wileyb2cSetComponentsPopulator wileyb2cSetComponentsPopulator;

	@Test
	public void populateWhenComponentsReferenceExist() throws Exception
	{
		//Given
		ProductReferenceModel referenceModel1 = mock(ProductReferenceModel.class);
		WileyPurchaseOptionProductModel productSetPOModel1 = mock(WileyPurchaseOptionProductModel.class);
		when(referenceModel1.getTarget()).thenReturn(productSetPOModel1);
		when(wileyb2cClassificationServiceMock.resolveClassificationClass(productSetPOModel1).getName()).thenReturn(
				CLASSIFICATION_CLASS_NAME);

		ProductReferenceModel referenceModel2 = mock(ProductReferenceModel.class);
		ProductModel productSetModel2 = mock(ProductModel.class);
		when(referenceModel2.getTarget()).thenReturn(productSetModel2);

		when(productReferenceServiceMock.getProductReferencesForSourceProduct(productModelMock,
				ProductReferenceTypeEnum.PRODUCT_SET_COMPONENT, true))
				.thenReturn(Arrays.asList(referenceModel1, referenceModel2));

		List<ProductData> productDataList = new ArrayList<>();
		//When
		wileyb2cSetComponentsPopulator.populate(productModelMock, productDataList);
		//Then
		assertNotNull(productDataList);
		assertEquals(2, productDataList.size());
		assertEquals(CLASSIFICATION_CLASS_NAME, productDataList.get(0).getVariantType());
		assertNull(productDataList.get(1).getVariantType());
		verify(wileyb2cProductConfiguredPopulatorMock, times(1)).populate(eq(productSetPOModel1), any(ProductData.class),
				eq(Wileyb2cSetComponentsPopulator.PRODUCT_SET_COMPONENT_OPTIONS));
		verify(wileyb2cProductConfiguredPopulatorMock, times(1)).populate(eq(productSetModel2), any(ProductData.class),
				eq(Wileyb2cSetComponentsPopulator.PRODUCT_SET_COMPONENT_OPTIONS));
	}

	@Test
	public void populateWhenComponentsReferenceNotExist() throws Exception
	{
		//Given
		when(productReferenceServiceMock.getProductReferencesForSourceProduct(productModelMock,
				ProductReferenceTypeEnum.PRODUCT_SET_COMPONENT, true))
				.thenReturn(Collections.emptyList());

		List<ProductData> productDataList = new ArrayList<>();
		//When
		wileyb2cSetComponentsPopulator.populate(productModelMock, productDataList);
		//Then
		assertNotNull(productDataList);
		assertEquals(0, productDataList.size());
		verify(wileyb2cProductConfiguredPopulatorMock, never()).populate(eq(productModelMock), any(ProductData.class),
				eq(Wileyb2cSetComponentsPopulator.PRODUCT_SET_COMPONENT_OPTIONS));
	}

}