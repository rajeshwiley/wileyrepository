package com.wiley.facades.populators.bright.cover;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.product.data.ImageDataType;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.site.BaseSiteService;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.facades.product.data.BrightCoveVideoData;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyBrightCoverHelperUnitTest
{
	private static final String VIDEO_ID = "12345";
	private static final String VIDEO_FORMAT = "brightCoveVideo";
	private static final String PLAYER_ID = "integration.brightcove.wel.playerid";
	private static final String PLAYER_KEY = "integration.brightcove.wel.playerkey";
	private static final String ACCOUNT_ID = "integration.brightcove.wel.accountid";
	private static final String PLAYER_ID_VALUE = "222222";
	private static final String PLAYER_KEY_VALUE = "test_player_key";
	private static final String ACCOUNT_ID_VALUE = "33333";
	private static final String CURRENT_SITE_UID = "wel";
	private static final Integer GALLERY_INDEX = 1;

	@InjectMocks
	private WileyBrightCoverHelper wileyBrightCoverHelper;

	@Mock
	private BaseSiteService mockBaseSiteService;
	@Mock
	private ConfigurationService mockConfigurationService;
	@Mock
	private BaseSiteModel mockCurrentBaseSite;
	@Mock
	private Configuration mockConfiguration;


	@Before
	public void setUp()
	{
		initCurrentBaseSite();
		initConfiguration();
	}

	@Test
	public void createBrightCoveVideoDataForGallery()
	{

		final BrightCoveVideoData dataForGallery = wileyBrightCoverHelper.createBrightCoveVideoDataForGallery(VIDEO_ID,
				GALLERY_INDEX);

		assertEquals(GALLERY_INDEX, dataForGallery.getGalleryIndex());
		assertEquals(ImageDataType.GALLERY, dataForGallery.getImageType());
		assertEquals(VIDEO_FORMAT, dataForGallery.getFormat());
		assertVideoSettings(dataForGallery);
	}

	private void assertVideoSettings(final BrightCoveVideoData dataForGallery)
	{
		assertEquals(VIDEO_ID, dataForGallery.getVideoId());
		assertEquals(PLAYER_ID_VALUE, dataForGallery.getPlayerId());
		assertEquals(PLAYER_KEY_VALUE, dataForGallery.getPlayerKey());
		assertEquals(ACCOUNT_ID_VALUE, dataForGallery.getAccountId());
	}

	@Test
	public void createBrightCoveVideo()
	{

		final BrightCoveVideoData dataForGallery = wileyBrightCoverHelper.createBrightCoveVideo(VIDEO_ID);

		assertVideoSettings(dataForGallery);
	}

	private void initCurrentBaseSite()
	{
		when(mockBaseSiteService.getCurrentBaseSite()).thenReturn(mockCurrentBaseSite);
		when(mockCurrentBaseSite.getUid()).thenReturn(CURRENT_SITE_UID);
	}

	private void initConfiguration()
	{
		when(mockConfigurationService.getConfiguration()).thenReturn(mockConfiguration);
		when(mockConfiguration.getString(PLAYER_ID)).thenReturn(PLAYER_ID_VALUE);
		when(mockConfiguration.getString(PLAYER_KEY)).thenReturn(PLAYER_KEY_VALUE);
		when(mockConfiguration.getString(ACCOUNT_ID)).thenReturn(ACCOUNT_ID_VALUE);
	}


}
