package com.wiley.facades.order.converters.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.wiley.core.model.WileyVariantProductModel;
import com.wiley.core.product.WileyProductService;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;


/**
 * Unit test for {@link WELCartModificationPopulator}.
 *
 * @author Aliaksei_Zlobich
 */
@UnitTest
public class WELCartModificationPopulatorUnitTest
{
	private WELCartModificationPopulator welCartModificationPopulator;

	private CartModificationData cartModificationData;

	@Mock
	private WileyProductService wileyProductServiceMock;

	@Mock
	private CommerceCartModification commerceCartModificationMock;

	@Mock
	private Converter<AbstractOrderEntryModel, OrderEntryData> orderEntryConverter;

	@Mock
	private AbstractOrderEntryModel orderEntryMock;

	@Mock
	private WileyVariantProductModel productMock;

	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);

		cartModificationData = new CartModificationData();

		when(commerceCartModificationMock.getEntry()).thenReturn(orderEntryMock);
		when(orderEntryMock.getProduct()).thenReturn(productMock);

		when(wileyProductServiceMock.canBePartOfProductSet(eq(productMock))).thenReturn(true);

		welCartModificationPopulator = new WELCartModificationPopulator();
		welCartModificationPopulator.setWileyProductService(wileyProductServiceMock);
		welCartModificationPopulator.setOrderEntryConverter(orderEntryConverter);
	}

	@Test
	public void testPopulateSuccess() throws Exception
	{
		// Given
		// no changes in test data

		// When
		welCartModificationPopulator.populate(commerceCartModificationMock, cartModificationData);

		// Then
		assertTrue(cartModificationData.getCanProductBePartOfSet());
		verify(wileyProductServiceMock).canBePartOfProductSet(eq(productMock));
	}

	@Test
	public void testPopulateIfProductNull() throws Exception
	{
		// Given
		when(orderEntryMock.getProduct()).thenReturn(null);

		// When
		welCartModificationPopulator.populate(commerceCartModificationMock, cartModificationData);

		// Then
		assertFalse(cartModificationData.getCanProductBePartOfSet());
		verifyZeroInteractions(wileyProductServiceMock);
	}


}