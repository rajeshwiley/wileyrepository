/**
 * 
 */
package com.wiley.facades.wileycom.order;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.product.WileyProductEditionFormatService;


/**
 * Default unit test for {@link WileycomCheckoutFacadeImpl}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileycomCheckoutFacadeImplUnitTest
{

  private static final String DELIVERY_MODE_CODE = "test-delivery-mode-code";
  private static final String DIFFERENT_DELIVERY_MODE_CODE = "different-delivery-mode-code";
  private static final String EXAMPLE_ORDER_CODE = "example order code";

  @InjectMocks
  private WileycomCheckoutFacadeImpl wileycomCheckoutFacadeImpl = new WileycomCheckoutFacadeImpl();

  @Mock
  private CartService cartServiceMock;

  @Mock
  private CartFacade cartFacadeMock;

  @Mock
  private CartModel cartModelMock;

  @Mock
  private OrderModel orderModelMock;

  @Mock
  private DeliveryModeModel deliveryModeModelMock;

  @Mock
  BaseStoreModel baseStoreModelMock;

  @Mock
  BaseStoreService baseStoreServiceMock;

  @Mock
  CustomerAccountService customerAccountServiceMock;

  @Mock
  WileyProductEditionFormatService wileyProductEditionFormatService;

  @Before
  public void setUp() throws Exception
  {
    when(cartServiceMock.getSessionCart()).thenReturn(cartModelMock);
  }

  @Test
  public void shouldReturnTrueIfCartTotalIsZeroDotZero()
  {
    when(cartFacadeMock.hasSessionCart()).thenReturn(true);
    when(cartModelMock.getTotalPrice()).thenReturn(new Double(0.0));

    assertTrue("The cart total is 0 and should return true", wileycomCheckoutFacadeImpl.isZeroTotalOrder());
  }

  @Test
  public void shouldReturnTrueIfCartTotalIsZeroDotZeroZero()
  {
    when(cartFacadeMock.hasSessionCart()).thenReturn(true);
    when(cartModelMock.getTotalPrice()).thenReturn(new Double(0.00));

    assertTrue("The cart total is 0.00 and should return true", wileycomCheckoutFacadeImpl.isZeroTotalOrder());
  }

  @Test
  public void shouldReturnFalseIfCartIsNotPresent()
  {
    when(cartFacadeMock.hasSessionCart()).thenReturn(false);
    when(cartModelMock.getTotalPrice()).thenReturn(new Double(0.00));

    assertFalse("The cart is not present and should return false", wileycomCheckoutFacadeImpl.isZeroTotalOrder());
  }

  @Test
  public void shouldReturnFalseIfCartTotalIsNotZero()
  {
    when(cartFacadeMock.hasSessionCart()).thenReturn(true);
    when(cartModelMock.getTotalPrice()).thenReturn(new Double(0.0000005));

    assertFalse("The cart total is not zero and should return false", wileycomCheckoutFacadeImpl.isZeroTotalOrder());
  }

  @Test
  public void shouldIsDeliveryModeAlreadySetInCartReturnFalseIfNoSessionCart()
  {
    when(cartFacadeMock.hasSessionCart()).thenReturn(false);
    assertFalse("There is no session cart so isDeliveryModeAlreadySetInCart should return false",
            wileycomCheckoutFacadeImpl.isDeliveryModeAlreadySetInCart());
  }

  @Test
  public void shouldIsDeliveryModeAlreadySetInCartReturnFalseIfSessionCartDoesNotContainDeliveryMode()
  {
    when(cartFacadeMock.hasSessionCart()).thenReturn(true);
    assertFalse("There is a session cart but there is no deliveryMode in it so isDeliveryModeAlreadySetInCart"
                    + " should return false",
            wileycomCheckoutFacadeImpl.isDeliveryModeAlreadySetInCart());
  }

  @Test
  public void shouldIsDeliveryModeAlreadySetInCartReturnTrueIfSessionCartContainsDeliveryMode()
  {
    when(cartFacadeMock.hasSessionCart()).thenReturn(true);
    when(cartModelMock.getDeliveryMode()).thenReturn(deliveryModeModelMock);
    assertTrue("There is a session cart and contains deliveryMode so isDeliveryModeAlreadySetInCart should return true",
            wileycomCheckoutFacadeImpl.isDeliveryModeAlreadySetInCart());
  }

  @Test
  public void shouldIsDeliveryModeSetInCartTheSameThanReturnFalseIfNoSessionCart()
  {
    when(cartFacadeMock.hasSessionCart()).thenReturn(false);
    assertFalse("There is no session cart so isDeliveryModeSetInCartTheSameThan should return false",
            wileycomCheckoutFacadeImpl.isDeliveryModeSetInCartTheSameThan(DELIVERY_MODE_CODE));
  }

  @Test
  public void shouldIsDeliveryModeSetInCartTheSameThanReturnFalseIfSessionCartDoesNotContainDeliveryMode()
  {
    when(cartFacadeMock.hasSessionCart()).thenReturn(true);
    assertFalse("There is a session cart without delivery mode so isDeliveryModeSetInCartTheSameThan should return false",
            wileycomCheckoutFacadeImpl.isDeliveryModeSetInCartTheSameThan(DELIVERY_MODE_CODE));
  }

  @Test
  public void shouldIsDeliveryModeSetInCartTheSameThanReturnFalseIfSessionCartContainsADeliveryModeDifferent()
  {
    when(cartFacadeMock.hasSessionCart()).thenReturn(true);
    when(cartModelMock.getDeliveryMode()).thenReturn(deliveryModeModelMock);
    when(deliveryModeModelMock.getCode()).thenReturn(DIFFERENT_DELIVERY_MODE_CODE);
    assertFalse("There is a session cart with a delivery mode but its different so isDeliveryModeSetInCartTheSameThan"
                    + " should return false",
            wileycomCheckoutFacadeImpl.isDeliveryModeSetInCartTheSameThan(DELIVERY_MODE_CODE));
  }

  @Test
  public void shouldIsDeliveryModeSetInCartTheSameThanReturnTrueIfSessionCartContainsADeliveryModeTheSame()
  {
    when(cartFacadeMock.hasSessionCart()).thenReturn(true);
    when(cartModelMock.getDeliveryMode()).thenReturn(deliveryModeModelMock);
    when(deliveryModeModelMock.getCode()).thenReturn(DELIVERY_MODE_CODE);
    assertTrue("There is a session cart with a delivery mode and its the same as provided so isDeliveryModeSetInCartTheSameThan"
                    + " should return true",
            wileycomCheckoutFacadeImpl.isDeliveryModeSetInCartTheSameThan(DELIVERY_MODE_CODE));
  }

  @Test
  public void shouldReturnTrueBecauseOrderIsDigital()
  {
    when(baseStoreServiceMock.getCurrentBaseStore()).thenReturn(baseStoreModelMock);
    when(customerAccountServiceMock.getOrderForCode(EXAMPLE_ORDER_CODE, baseStoreModelMock)).thenReturn(orderModelMock);
    when(wileyProductEditionFormatService.isDigitalCart(orderModelMock)).thenReturn(true);
    assertTrue(wileycomCheckoutFacadeImpl.isDigitalOrder(EXAMPLE_ORDER_CODE));
  }

  @Test
  public void shouldReturnFalseBecauseOrderIsNotDigital()
  {
    when(baseStoreServiceMock.getCurrentBaseStore()).thenReturn(baseStoreModelMock);
    when(customerAccountServiceMock.getOrderForCode(EXAMPLE_ORDER_CODE, baseStoreModelMock)).thenReturn(orderModelMock);
    when(wileyProductEditionFormatService.isDigitalCart(orderModelMock)).thenReturn(false);
    assertFalse(wileycomCheckoutFacadeImpl.isDigitalOrder(EXAMPLE_ORDER_CODE));
  }
}
