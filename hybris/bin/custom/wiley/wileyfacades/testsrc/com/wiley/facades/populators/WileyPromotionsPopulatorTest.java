package com.wiley.facades.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorfacades.device.ResponsiveMediaFacade;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.PromotionData;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.promotionengineservices.model.RuleBasedPromotionModel;
import de.hybris.platform.promotions.PromotionsService;
import de.hybris.platform.promotions.model.OrderPromotionModel;
import static org.mockito.BDDMockito.given;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Unit test for {@link WileyPromotionsPopulator}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyPromotionsPopulatorTest
{
	@InjectMocks
	private WileyPromotionsPopulator wileyPromotionsPopulator;

	@Mock
	private CartService cartServiceMock;

	@Mock
	private RuleBasedPromotionModel productPromotionModelMock;

	@Mock
	private OrderPromotionModel orderPromotionModelMock;

	@Mock
	private PromotionsService promotionsServiceMock;

	@Mock
	private MediaContainerModel mediaModelMock;

	@Mock
	private ResponsiveMediaFacade responsiveMediaFacadeMock;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
	}


	@Test
	public void testPopulate()
	{
		final List<ImageData> imageDataList = new ArrayList<>();
		final PromotionData target = new PromotionData();

		given(productPromotionModelMock.getProductBanner()).willReturn(mediaModelMock);
		given(responsiveMediaFacadeMock.getImagesFromMediaContainer(mediaModelMock)).willReturn(imageDataList);

		wileyPromotionsPopulator.populate(productPromotionModelMock, target);

		Assert.assertNotNull("Banners should not be null", target.getBanners());
		Assert.assertEquals(imageDataList, target.getBanners());
	}


	@Test
	public void testPopulateNotProductPromotion()
	{
		final PromotionData target = new PromotionData();

		wileyPromotionsPopulator.populate(orderPromotionModelMock, target);

		Assert.assertNull("Banner should be null", target.getBanners());
	}


	@Test
	public void testPopulateEmptyBanner()
	{
		final PromotionData target = new PromotionData();

		given(productPromotionModelMock.getProductBanner()).willReturn(null);

		wileyPromotionsPopulator.populate(productPromotionModelMock, target);

		Assert.assertNull("Banner should be null", target.getBanners());
	}
}
