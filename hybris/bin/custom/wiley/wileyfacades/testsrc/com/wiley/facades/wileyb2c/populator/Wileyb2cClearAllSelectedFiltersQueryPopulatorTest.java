/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 *
 *
 */
package com.wiley.facades.wileyb2c.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.search.facetdata.BreadcrumbData;
import de.hybris.platform.commerceservices.search.facetdata.ProductCategorySearchPageData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;

import java.util.Arrays;
import java.util.Collections;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Author Maksim_Kozich
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cClearAllSelectedFiltersQueryPopulatorTest
{
	@InjectMocks
	private Wileyb2cClearAllSelectedFiltersQueryPopulator wileyb2cClearAllSelectedFiltersQueryPopulator;

	@Mock
	private Converter searchStateConvertedMock;

	@Mock
	private BreadcrumbData breadcrumbMock;

	@Mock
	private SolrSearchQueryData currentQueryMock;

	@Mock
	private SearchStateData searchStateDataMock;

	@Mock
	private ProductCategorySearchPageData sourceMock;

	@Mock
	private ProductCategorySearchPageData targetMock;

	@Test
	public void testBreadcrumbsNotEmpty() throws Exception
	{
		//Given
		when(sourceMock.getBreadcrumbs()).thenReturn(Arrays.asList(breadcrumbMock));
		when(sourceMock.getCurrentQuery()).thenReturn(currentQueryMock);
		when(searchStateConvertedMock.convert(currentQueryMock)).thenReturn(searchStateDataMock);

		//When
		wileyb2cClearAllSelectedFiltersQueryPopulator.populate(sourceMock, targetMock);

		//Then
		verify(targetMock).setClearAllSelectedFiltersQuery(searchStateDataMock);
	}

	@Test
	public void testBreadcrumbsEmpty() throws Exception
	{
		//Given
		when(sourceMock.getBreadcrumbs()).thenReturn(Collections.emptyList());

		//When
		wileyb2cClearAllSelectedFiltersQueryPopulator.populate(sourceMock, targetMock);

		//Then
		verify(searchStateConvertedMock, never()).convert(any());
		verify(targetMock, never()).setClearAllSelectedFiltersQuery(any());
	}

	@Test
	public void testNullSourceProvided() throws Exception
	{
		//When
		try
		{
			wileyb2cClearAllSelectedFiltersQueryPopulator.populate(null, targetMock);
			fail("Expected " + IllegalArgumentException.class);
		} catch (IllegalArgumentException e)
		{
			// Success
			// Then
			verify(searchStateConvertedMock, never()).convert(any());
		}
	}

	@Test
	public void testNullTargetProvided() throws Exception
	{
		//When
		try
		{
			wileyb2cClearAllSelectedFiltersQueryPopulator.populate(sourceMock, null);
			fail("Expected " + IllegalArgumentException.class);
		} catch (IllegalArgumentException e)
		{
			// Success
			// Then
			verify(searchStateConvertedMock, never()).convert(any());
		}
	}
}
