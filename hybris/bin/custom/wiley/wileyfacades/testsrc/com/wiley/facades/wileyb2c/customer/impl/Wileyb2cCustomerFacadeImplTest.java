package com.wiley.facades.wileyb2c.customer.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.user.UserService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.SchoolModel;
import com.wiley.core.order.WileycomCheckoutService;
import com.wiley.core.wileyb2c.customer.WileyB2CCustomerAccountService;
import com.wiley.core.wileycom.customer.service.WileycomCustomerAccountService;
import com.wiley.facades.customer.data.SchoolData;
import com.wiley.facades.customer.impl.WileycomCustomerFacadeImpl;
import com.wiley.facades.wileycom.customer.converters.WileycomSchoolConverter;



@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cCustomerFacadeImplTest
{
	private static final String CUSTOMER_UID = "customer-uid";

	@InjectMocks
	private Wileyb2cCustomerFacadeImpl wileyb2cCustomerFacadeImpl;

	@Mock
	private WileyB2CCustomerAccountService wileyB2CCustomerAccountServiceMock;

	@Mock
	private WileycomCheckoutService wileycomCheckoutServiceMock;

	@Mock
	private WileycomCustomerAccountService wileycomCustomerAccountServiceMock;

	@Mock
	private WileycomSchoolConverter wileycomSchoolConverterMock;

	@Mock
	private UserService userServiceMock;

	@Mock
	private UserFacade userFacadeMock;

	@Mock
	private OrderModel orderModelMock;

	@Mock
	private CustomerModel customerModelMock;

	@Mock
	private CustomerData customerDataMock;

	@Mock
	private SchoolModel firstSchoolModelMock;

	@Mock
	private SchoolModel secondSchoolModelMock;

	@Mock
	private SchoolModel thirdSchoolModelMock;

	@Mock
	private SchoolData firstSchoolDataMock;

	@Mock
	private SchoolData secondSchoolDataMock;

	@Mock
	private SchoolData thirdSchoolDataMock;

	@Mock
	private SchoolModel profileSchoolModelMock;

	@Mock
	private SchoolData profileSchoolDataMock;

	@Mock
	private SchoolModel orderSchoolModelMock;

	@Mock
	private SchoolData orderSchoolDataMock;

	private List<SchoolModel> schoolList;

	@Before
	public void setup()
	{
		wileyb2cCustomerFacadeImpl.setWileycomCustomerAccountService(wileycomCustomerAccountServiceMock);
		wileyb2cCustomerFacadeImpl.setWileycomSchoolConverter(wileycomSchoolConverterMock);

		schoolList = Arrays.asList(firstSchoolModelMock, secondSchoolModelMock, thirdSchoolModelMock);

		when(wileycomCustomerAccountServiceMock.getSchoolsWithLimitation(anyInt(), anyInt())).thenReturn(schoolList);

		setupSchoolMocks(firstSchoolModelMock, firstSchoolDataMock, "1", "1");
		setupSchoolMocks(secondSchoolModelMock, secondSchoolDataMock, "2", "2");
		setupSchoolMocks(thirdSchoolModelMock, thirdSchoolDataMock, "3", "3");
		setupSchoolMocks(profileSchoolModelMock, profileSchoolDataMock, "4", "4");
		setupSchoolMocks(orderSchoolModelMock, orderSchoolDataMock, "5", "5");

		when(userFacadeMock.isAnonymousUser()).thenReturn(false);
	}

	@Test
	public void shouldGetDisplayedSchoolsForCustomerWithoutAssociatedSchool()
	{
		// Given
		when(userServiceMock.getUserForUID(CUSTOMER_UID)).thenReturn(customerModelMock);
		when(wileycomCheckoutServiceMock.getLastOrderForCustomer(CUSTOMER_UID)).thenReturn(Optional.empty());

		// When
		List<SchoolData> resultSchoolList = wileyb2cCustomerFacadeImpl.getDisplayedSchoolsForCustomer(CUSTOMER_UID);

		// Then
		assertEquals("The resulting list should have been equal to the one provided by getSchoolsWithLimitation()!",
				Arrays.asList(firstSchoolDataMock, secondSchoolDataMock, thirdSchoolDataMock), resultSchoolList);
	}

	@Test
	public void shouldGetDisplayedSchoolsForCustomerInAlphabeticalOrder()
	{
		// Given
		schoolList = Arrays.asList(secondSchoolModelMock, thirdSchoolModelMock, firstSchoolModelMock);
		when(wileycomCustomerAccountServiceMock.getSchoolsWithLimitation(anyInt(), anyInt())).thenReturn(schoolList);

		when(userServiceMock.getUserForUID(CUSTOMER_UID)).thenReturn(customerModelMock);
		when(wileycomCheckoutServiceMock.getLastOrderForCustomer(CUSTOMER_UID)).thenReturn(Optional.empty());

		// When
		List<SchoolData> resultSchoolList = wileyb2cCustomerFacadeImpl.getDisplayedSchoolsForCustomer(CUSTOMER_UID);

		// Then
		assertEquals("The resulting list should have been ordered in alphabetical order!",
				Arrays.asList(firstSchoolDataMock, secondSchoolDataMock, thirdSchoolDataMock), resultSchoolList);
	}

	@Test
	public void shouldGetDisplayedSchoolsForCustomerWithDifferentProfileSchool()
	{
		// Given
		when(userServiceMock.getUserForUID(CUSTOMER_UID)).thenReturn(customerModelMock);
		when(customerModelMock.getSchool()).thenReturn(profileSchoolModelMock);
		when(wileycomCheckoutServiceMock.getLastOrderForCustomer(CUSTOMER_UID)).thenReturn(Optional.empty());

		// When
		List<SchoolData> resultSchoolList = wileyb2cCustomerFacadeImpl.getDisplayedSchoolsForCustomer(CUSTOMER_UID);

		// Then
		assertEquals("The resulting list should have contained the school set for the customer!",
				Arrays.asList(firstSchoolDataMock, secondSchoolDataMock, thirdSchoolDataMock, profileSchoolDataMock),
				resultSchoolList);
	}

	@Test
	public void shouldGetDisplayedSchoolsForCustomerWithNonDifferentProfileSchool()
	{
		// Given
		schoolList = Arrays.asList(firstSchoolModelMock, profileSchoolModelMock, thirdSchoolModelMock);
		when(wileycomCustomerAccountServiceMock.getSchoolsWithLimitation(anyInt(), anyInt())).thenReturn(schoolList);

		when(userServiceMock.getUserForUID(CUSTOMER_UID)).thenReturn(customerModelMock);
		when(customerModelMock.getSchool()).thenReturn(profileSchoolModelMock);
		when(wileycomCheckoutServiceMock.getLastOrderForCustomer(CUSTOMER_UID)).thenReturn(Optional.empty());

		// When
		List<SchoolData> resultSchoolList = wileyb2cCustomerFacadeImpl.getDisplayedSchoolsForCustomer(CUSTOMER_UID);

		// Then
		assertEquals("The resulting list should have contained the school set for the customer!",
				Arrays.asList(firstSchoolDataMock, thirdSchoolDataMock, profileSchoolDataMock), resultSchoolList);
	}

	@Test
	public void shouldGetDisplayedSchoolsForCustomerWithProfileSchoolInsteadOfOrderSchool()
	{
		// Given
		when(userServiceMock.getUserForUID(CUSTOMER_UID)).thenReturn(customerModelMock);
		when(customerModelMock.getSchool()).thenReturn(profileSchoolModelMock);
		when(wileycomCheckoutServiceMock.getLastOrderForCustomer(CUSTOMER_UID)).thenReturn(Optional.of(orderModelMock));
		when(orderModelMock.getSchool()).thenReturn(orderSchoolModelMock);

		// When
		List<SchoolData> resultSchoolList = wileyb2cCustomerFacadeImpl.getDisplayedSchoolsForCustomer(CUSTOMER_UID);

		// Then
		assertEquals("The resulting list should have contained the school set for the customer!",
				Arrays.asList(firstSchoolDataMock, secondSchoolDataMock, thirdSchoolDataMock, profileSchoolDataMock),
				resultSchoolList);
	}

	@Test
	public void shouldGetDisplayedSchoolsForCustomerWithDifferentOrderSchool()
	{
		// Given
		when(userServiceMock.getUserForUID(CUSTOMER_UID)).thenReturn(customerModelMock);
		when(customerModelMock.getSchool()).thenReturn(null);
		when(wileycomCheckoutServiceMock.getLastOrderForCustomer(CUSTOMER_UID)).thenReturn(Optional.of(orderModelMock));
		when(orderModelMock.getSchool()).thenReturn(orderSchoolModelMock);

		// When
		List<SchoolData> resultSchoolList = wileyb2cCustomerFacadeImpl.getDisplayedSchoolsForCustomer(CUSTOMER_UID);

		// Then
		assertEquals("The resulting list should have contained the school set in the customer's last order!",
				Arrays.asList(firstSchoolDataMock, secondSchoolDataMock, thirdSchoolDataMock, orderSchoolDataMock),
				resultSchoolList);
	}

	@Test
	public void shouldGetDisplayedSchoolsForCustomerWithNonDifferentOrderSchool()
	{
		schoolList = Arrays.asList(firstSchoolModelMock, orderSchoolModelMock, thirdSchoolModelMock);
		when(wileycomCustomerAccountServiceMock.getSchoolsWithLimitation(anyInt(), anyInt())).thenReturn(schoolList);

		// Given
		when(userServiceMock.getUserForUID(CUSTOMER_UID)).thenReturn(customerModelMock);
		when(customerModelMock.getSchool()).thenReturn(null);
		when(wileycomCheckoutServiceMock.getLastOrderForCustomer(CUSTOMER_UID)).thenReturn(Optional.of(orderModelMock));
		when(orderModelMock.getSchool()).thenReturn(orderSchoolModelMock);

		// When
		List<SchoolData> resultSchoolList = wileyb2cCustomerFacadeImpl.getDisplayedSchoolsForCustomer(CUSTOMER_UID);

		// Then
		assertEquals("The resulting list should have contained the school set in the customer's last order!",
				Arrays.asList(firstSchoolDataMock, thirdSchoolDataMock, orderSchoolDataMock), resultSchoolList);
	}

	@Test
	public void shouldGetDisplayedSchoolsForCustomerWithDifferentProfileSchoolAndMaximumDisplayedListSizeConfigured()
	{
		// Given
		schoolList = new ArrayList<>(schoolList);

		for (int i = 0; i < (WileycomCustomerFacadeImpl.SCHOOLS_MAX_QUANTITY - 3); ++i)
		{
			schoolList.add(firstSchoolModelMock);
		}

		when(wileycomCustomerAccountServiceMock.getSchoolsWithLimitation(anyInt(), anyInt())).thenReturn(schoolList);
		when(userServiceMock.getUserForUID(CUSTOMER_UID)).thenReturn(customerModelMock);
		when(customerModelMock.getSchool()).thenReturn(profileSchoolModelMock);
		when(wileycomCheckoutServiceMock.getLastOrderForCustomer(CUSTOMER_UID)).thenReturn(Optional.empty());

		// When
		List<SchoolData> resultSchoolList = wileyb2cCustomerFacadeImpl.getDisplayedSchoolsForCustomer(CUSTOMER_UID);

		// Then
		List<SchoolData> expectedSchoolList = new ArrayList<>();

		for (int i = 0; i < (WileycomCustomerFacadeImpl.SCHOOLS_MAX_QUANTITY - 3); ++i)
		{
			expectedSchoolList.add(firstSchoolDataMock);
		}

		expectedSchoolList.add(secondSchoolDataMock);
		expectedSchoolList.add(thirdSchoolDataMock);
		expectedSchoolList.add(profileSchoolDataMock);

		assertEquals("The resulting list should have contained 'SCHOOLS_MAX_QUANTITY' elements including the customer's school!",
				expectedSchoolList, resultSchoolList);
	}

	@Test (expected = UnknownIdentifierException.class)
	public void shouldNotGetDisplayedSchoolsForUnknownCustomer()
	{
		// Given
		when(userServiceMock.getUserForUID(CUSTOMER_UID)).thenThrow(UnknownIdentifierException.class);

		// When
		wileyb2cCustomerFacadeImpl.getDisplayedSchoolsForCustomer(CUSTOMER_UID);

		// Then
		fail("Test execution should have been interrupted with exception!");
	}

	@Test (expected = IllegalArgumentException.class)
	public void shouldNotGetDisplayedSchoolsForNullCustomer()
	{
		// When
		wileyb2cCustomerFacadeImpl.getDisplayedSchoolsForCustomer(null);

		// Then
		fail("Test execution should have been interrupted with exception!");
	}

	@Test
	public void shouldNotGetSchoolForCustomerFromLastOrderIfUserIsAnonymous()
	{
		// Given
		when(userFacadeMock.isAnonymousUser()).thenReturn(true);

		// When
		SchoolData resultSchool = wileyb2cCustomerFacadeImpl.getSchoolForCustomerFromLastOrder(CUSTOMER_UID);

		// Then
		assertNull("No orders should be found for anonymous user!", resultSchool);
	}

	@Test
	public void shouldNotGetSchoolForCustomerFromLastOrderIfNoOrdersFound()
	{
		// Given
		when(wileycomCheckoutServiceMock.getLastOrderForCustomer(CUSTOMER_UID)).thenReturn(Optional.empty());

		// When
		SchoolData resultSchool = wileyb2cCustomerFacadeImpl.getSchoolForCustomerFromLastOrder(CUSTOMER_UID);

		// Then
		assertNull("No schools should be returned if there wasn't any orders found!", resultSchool);
	}

	@Test
	public void shouldNotGetSchoolForCustomerFromLastOrderIfNoSchoolIsFoundInLastOrder()
	{
		// Given
		when(wileycomCheckoutServiceMock.getLastOrderForCustomer(CUSTOMER_UID)).thenReturn(Optional.of(orderModelMock));

		// When
		SchoolData resultSchool = wileyb2cCustomerFacadeImpl.getSchoolForCustomerFromLastOrder(CUSTOMER_UID);

		// Then
		assertNull("No schools should be returned if school was not set for the last order!", resultSchool);
	}

	@Test
	public void shouldGetSchoolForCustomerFromLastOrder()
	{
		// Given
		when(wileycomCheckoutServiceMock.getLastOrderForCustomer(CUSTOMER_UID)).thenReturn(Optional.of(orderModelMock));
		when(orderModelMock.getSchool()).thenReturn(orderSchoolModelMock);

		// When
		SchoolData resultSchool = wileyb2cCustomerFacadeImpl.getSchoolForCustomerFromLastOrder(CUSTOMER_UID);

		// Then
		assertNotNull("A school should be returned from the last order!", resultSchool);
		assertSame("A school should be returned from the last order!", orderSchoolDataMock, resultSchool);
	}

	@Test (expected = UnknownIdentifierException.class)
	public void shouldNotGetSchoolForUnknownCustomerFromLastOrder()
	{
		// Given
		when(wileycomCheckoutServiceMock.getLastOrderForCustomer(CUSTOMER_UID)).thenThrow(UnknownIdentifierException.class);

		// When
		wileyb2cCustomerFacadeImpl.getSchoolForCustomerFromLastOrder(CUSTOMER_UID);

		// Then
		fail("Test execution should have been interrupted with exception!");
	}

	@Test (expected = IllegalArgumentException.class)
	public void shouldNotGetSchoolForNullCustomerFromLastOrder()
	{
		// When
		wileyb2cCustomerFacadeImpl.getDisplayedSchoolsForCustomer(null);

		// Then
		fail("Test execution should have been interrupted with exception!");
	}

	@Test (expected = IllegalArgumentException.class)
	public void shouldNotGetSchoolForNullCustomerFromProfile()
	{
		// When
		wileyb2cCustomerFacadeImpl.getSchoolForCustomerFromProfile(null);

		// Then
		fail("Test execution should have been interrupted with exception!");
	}

	@Test (expected = UnknownIdentifierException.class)
	public void shouldNotGetSchoolForUnknownCustomerFromProfile()
	{
		// Given
		when(userServiceMock.getUserForUID(CUSTOMER_UID)).thenThrow(UnknownIdentifierException.class);

		// When
		wileyb2cCustomerFacadeImpl.getSchoolForCustomerFromProfile(CUSTOMER_UID);

		// Then
		fail("Test execution should have been interrupted with exception!");
	}

	@Test
	public void shouldNotGetSchoolForCustomerFromProfileIfNoSchoolSetForCustomer()
	{
		// Given
		when(userServiceMock.getUserForUID(CUSTOMER_UID)).thenReturn(customerModelMock);

		// When
		SchoolData resultSchool = wileyb2cCustomerFacadeImpl.getSchoolForCustomerFromProfile(CUSTOMER_UID);

		// Then
		assertNull("Result school should have been null!", resultSchool);
	}

	@Test
	public void shouldGetSchoolForCustomerFromProfile()
	{
		// Given
		when(userServiceMock.getUserForUID(CUSTOMER_UID)).thenReturn(customerModelMock);
		when(customerModelMock.getSchool()).thenReturn(profileSchoolModelMock);

		// When
		SchoolData resultSchool = wileyb2cCustomerFacadeImpl.getSchoolForCustomerFromProfile(CUSTOMER_UID);

		// Then
		assertSame("Result school should have been the one set for the customer!", profileSchoolDataMock, resultSchool);
	}

	private void setupSchoolMocks(final SchoolModel schoolModel, final SchoolData schoolData, final String schoolCode,
			final String schoolName)
	{
		when(wileycomSchoolConverterMock.convert(schoolModel)).thenReturn(schoolData);
		when(schoolData.getCode()).thenReturn(schoolCode);
		when(schoolData.getName()).thenReturn(schoolName);
	}
}
