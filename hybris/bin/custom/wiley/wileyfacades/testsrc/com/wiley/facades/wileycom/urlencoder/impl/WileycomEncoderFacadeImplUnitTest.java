package com.wiley.facades.wileycom.urlencoder.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorfacades.urlencoder.data.UrlEncoderData;
import de.hybris.platform.servicelayer.session.SessionService;
import static org.mockito.Mockito.when;

import java.net.URISyntaxException;
import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;



/**
 * Unit test for {@link WileycomEncoderFacadeImpl}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileycomEncoderFacadeImplUnitTest
{
	private static final String URL_ENCODING_DATA_ATTR = "urlEncodingData";
	private static final String TEST_WRONG_ATTRIBUTE_NAME = "wrongName";
	private static final String TEST_LANGUAGE_ATTRIBUTE_NAME = "language";
	private static final String TEST_VALUE = "value";
	private static final String TEST_CONTEXT_PATH = "/contextPath";

	@Mock
	private SessionService sessionService;

	@InjectMocks
	private WileycomEncoderFacadeImpl wileycomEncoderFacade;


	private UrlEncoderData languageUrlEncoderData;

	@Before
	public void setUp() throws Exception
	{
		languageUrlEncoderData = new UrlEncoderData();
		languageUrlEncoderData.setAttributeName(TEST_LANGUAGE_ATTRIBUTE_NAME);

	}

	@Test
	public void shouldBeSameUrlDueToEmptyEncodingData() throws URISyntaxException
	{
		//Given
		final String testUri = "/contextPath/uri/with/slashes";
		when(sessionService.getAttribute(URL_ENCODING_DATA_ATTR)).thenReturn(Collections.EMPTY_LIST);

		//When
		final String result = wileycomEncoderFacade.substituteValue(testUri, TEST_CONTEXT_PATH, TEST_LANGUAGE_ATTRIBUTE_NAME,
				TEST_VALUE);

		//Then
		assertThat(result).isEqualTo(testUri);
	}

	@Test
	public void shouldSubstituteValueForAbsoluteUrl() throws URISyntaxException
	{
		//Given
		final String testUri = "https://wiley.com/notvalue/with/slashes";
		when(sessionService.getAttribute(URL_ENCODING_DATA_ATTR)).thenReturn(Collections.singletonList(languageUrlEncoderData));

		//When
		final String result = wileycomEncoderFacade.substituteValue(testUri, "/", TEST_LANGUAGE_ATTRIBUTE_NAME,
				TEST_VALUE);

		//Then
		assertThat(result).isEqualTo("https://wiley.com/value/with/slashes");
	}

	@Test
	public void shouldReturnSameUrlDueToWrongEncodingData() throws URISyntaxException
	{
		// Given
		final String testUri = "/contextPath/uri/with/slashes";
		when(sessionService.getAttribute(URL_ENCODING_DATA_ATTR)).thenReturn(Collections.singletonList(languageUrlEncoderData));

		//When
		final String result = wileycomEncoderFacade.substituteValue(testUri, TEST_CONTEXT_PATH, TEST_WRONG_ATTRIBUTE_NAME,
				TEST_VALUE);

		//Then
		assertThat(result).isEqualTo(testUri);
	}

	@Test
	public void shouldSubstituteValueHavingLongUrl() throws URISyntaxException
	{
		// Given
		final String testUri = "/contextPath/notvalue/with/slashes/?id=1";
		when(sessionService.getAttribute(URL_ENCODING_DATA_ATTR)).thenReturn(Collections.singletonList(languageUrlEncoderData));

		//When
		final String result = wileycomEncoderFacade.substituteValue(testUri, TEST_CONTEXT_PATH, TEST_LANGUAGE_ATTRIBUTE_NAME,
				TEST_VALUE);

		//Then
		final String expectedUri = "/contextPath/value/with/slashes/?id=1";
		assertThat(result).isEqualTo(expectedUri);
	}

	@Test
	public void shouldSubstituteValueHavingOnlyAttributeInUrl() throws URISyntaxException
	{
		// Given
		final String testUri = "/contextPath/notvalue";
		when(sessionService.getAttribute(URL_ENCODING_DATA_ATTR)).thenReturn(Collections.singletonList(languageUrlEncoderData));

		//When
		final String result = wileycomEncoderFacade.substituteValue(testUri, TEST_CONTEXT_PATH, TEST_LANGUAGE_ATTRIBUTE_NAME,
				TEST_VALUE);

		//Then
		assertThat(result).isEqualTo("/contextPath/value");
	}

	@Test
	public void shouldSubstituteValueHavingOnlyAttributeInUrlAndSlash() throws URISyntaxException
	{
		// Given
		final String testUri = "/contextPath/notvalue/";
		when(sessionService.getAttribute(URL_ENCODING_DATA_ATTR)).thenReturn(Collections.singletonList(languageUrlEncoderData));

		//When
		final String result = wileycomEncoderFacade.substituteValue(testUri, TEST_CONTEXT_PATH, TEST_LANGUAGE_ATTRIBUTE_NAME,
				TEST_VALUE);

		//Then
		assertThat(result).isEqualTo("/contextPath/value/");
	}

	@Test
	public void shouldLeaveContextPathWithEndingSlash() throws URISyntaxException
	{
		// Given
		final String testUri = "/contextPath/";
		when(sessionService.getAttribute(URL_ENCODING_DATA_ATTR)).thenReturn(Collections.singletonList(languageUrlEncoderData));

		//When
		final String result = wileycomEncoderFacade.substituteValue(testUri, TEST_CONTEXT_PATH, TEST_LANGUAGE_ATTRIBUTE_NAME,
				TEST_VALUE);

		//Then
		assertThat(result).isEqualTo(testUri);
	}

	@Test
	public void shouldLeaveContextPath() throws URISyntaxException
	{
		// Given
		final String testUri = "/contextPath";
		when(sessionService.getAttribute(URL_ENCODING_DATA_ATTR)).thenReturn(Collections.singletonList(languageUrlEncoderData));

		//When
		final String result = wileycomEncoderFacade.substituteValue(testUri, TEST_CONTEXT_PATH, TEST_LANGUAGE_ATTRIBUTE_NAME,
				TEST_VALUE);

		//Then
		assertThat(result).isEqualTo(testUri);
	}

}
