package com.wiley.facades.user.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.wiley.facades.user.WileyUserFacade;
import com.wiley.facades.wiley.order.WileyCartFacade;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
@IntegrationTest
public class WileyUserFacadeImplIntegrationTest extends ServicelayerTransactionalTest
{
	private static final String DEFAULT_ENCODING = "UTF-8";
	private static final String BASE_SITE = "asSite";
	private static final String CUSTOMER_ID = "test@test.com";
	private static final String CART_GUID = "test-usd-cart-guid";
	private static final String AS_QUOTED_CART_GUID = "test-usd-cart-as-quoted-guid";

	@Resource
	private UserService userService;

	@Resource
	private BaseSiteService baseSiteService;

	@Resource(name = "wileyUserFacade")
	private WileyUserFacade wileyUserFacade;

	@Resource(name = "wileyasCartFacade")
	private WileyCartFacade wileyasCartFacade;

	@Before
	public void setUp() throws Exception
	{
		importCsv("/wileyfacades/test/WileyUserFacadeImplIntegrationTest/common.impex", DEFAULT_ENCODING);
		baseSiteService.setCurrentBaseSite(baseSiteService.getBaseSiteForUID(BASE_SITE), true);
		userService.setCurrentUser(userService.getUserForUID(CUSTOMER_ID));
	}

	@Test
	public void getPaymentAddressesForCurrentUserGeneral()
	{
		assertFalse(wileyasCartFacade.hasSessionCart());

		// when
		wileyasCartFacade.switchSessionCart(CART_GUID);

		// then
		final List<AddressData> addresses = wileyUserFacade.getPaymentAddressesForCurrentUser();
		assertEquals(3, addresses.size());
	}

	@Test
	public void getPaymentAddressesForCurrentUserQuoted()
	{
		assertFalse(wileyasCartFacade.hasSessionCart());

		// when
		wileyasCartFacade.switchSessionCart(AS_QUOTED_CART_GUID);

		// then
		final List<AddressData> addresses = wileyUserFacade.getPaymentAddressesForCurrentUser();
		assertEquals(2, addresses.size());
	}
}