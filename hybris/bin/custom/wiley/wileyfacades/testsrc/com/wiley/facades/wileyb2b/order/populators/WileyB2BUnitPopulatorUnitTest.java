package com.wiley.facades.wileyb2b.order.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2bcommercefacades.company.converters.populators.B2BUnitPopulator;
import de.hybris.platform.b2bcommercefacades.company.data.B2BUnitData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Test for {@link WileyB2BUnitPopulator}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyB2BUnitPopulatorUnitTest
{
	private static final String B2B_UNIT_SAP_SALES_REP_CONTACT_NAME = "TestName";
	private static final String B2B_UNIT_SAP_SALES_REP_CONTACT_EMAIL = "testemail@example.com";
	private static final String B2B_UNIT_SAP_SALES_REP_CONTACT_PHONE = "111222111222";

	@InjectMocks
	private WileyB2BUnitPopulator wileyB2BUnitPopulator;

	@Mock
	private B2BUnitPopulator defaultB2BUnitPopulatorMock;

	@Mock(name = "addressConverter")
	private Converter<AddressModel, AddressData> addressConverterMock;

	@Mock
	private B2BUnitModel sourceMock;

	@Mock
	private AddressModel addressModelMock;

	@Mock
	private B2BUnitData targetMock;

	@Mock
	private AddressData addressDataMock;

	@Test
	public void shouldExecutDefaultB2BunitPopulator()
	{
		//Given
		//no additional conditions

		//When
		wileyB2BUnitPopulator.populate(sourceMock, targetMock);

		//Then
		verify(defaultB2BUnitPopulatorMock).populate(sourceMock, targetMock);
	}

	@Test
	public void shouldNotFillDataForNullBillingAddress()
	{
		//Given
		//no additional conditions, sourceMock.getBillingAddress returns null for mock by default

		//When
		wileyB2BUnitPopulator.populate(sourceMock, targetMock);

		//Then
		verify(targetMock, never()).setBillingAddress(any());
	}


	@Test
	public void shouldFillBillingAddress()
	{
		//Given
		when(sourceMock.getBillingAddress()).thenReturn(addressModelMock);
		when(addressConverterMock.convert(addressModelMock)).thenReturn(addressDataMock);

		//When
		wileyB2BUnitPopulator.populate(sourceMock, targetMock);

		//Then
		verify(targetMock).setBillingAddress(addressDataMock);
	}

	@Test
	public void shouldNotFillShippingAddress()
	{
		//Given
		when(targetMock.getAddresses()).thenReturn(null);

		//When
		wileyB2BUnitPopulator.populate(sourceMock, targetMock);

		//Then
		verify(targetMock, never()).setShippingAddresses(any());
	}

	@Test
	public void shouldFillShippingAddresses()
	{
		//Given
		AddressData notShippingAddress = mock(AddressData.class);
		when(notShippingAddress.isShippingAddress()).thenReturn(false);
		AddressData shippingAddress = mock(AddressData.class);
		when(shippingAddress.isShippingAddress()).thenReturn(true);

		List<AddressData> addresses = Arrays.asList(notShippingAddress, shippingAddress);
		when(targetMock.getAddresses()).thenReturn(addresses);
		List<AddressData> expectedShippingAddress = Collections.singletonList(shippingAddress);

		//When
		wileyB2BUnitPopulator.populate(sourceMock, targetMock);


		//Then
		verify(targetMock).setShippingAddresses(expectedShippingAddress);
	}

	@Test
	public void shouldFillSapSalesRepContact()
	{
		//Given
		when(sourceMock.getSapSalesRepContactName()).thenReturn(B2B_UNIT_SAP_SALES_REP_CONTACT_NAME);
		when(sourceMock.getSapSalesRepContactPhone()).thenReturn(B2B_UNIT_SAP_SALES_REP_CONTACT_PHONE);
		when(sourceMock.getSapSalesRepContactEmail()).thenReturn(B2B_UNIT_SAP_SALES_REP_CONTACT_EMAIL);

		//When
		wileyB2BUnitPopulator.populate(sourceMock, targetMock);

		//Then
		verify(targetMock).setSapSalesRepContactName(B2B_UNIT_SAP_SALES_REP_CONTACT_NAME);
		verify(targetMock).setSapSalesRepContactPhone(B2B_UNIT_SAP_SALES_REP_CONTACT_PHONE);
		verify(targetMock).setSapSalesRepContactEmail(B2B_UNIT_SAP_SALES_REP_CONTACT_EMAIL);
	}

}
