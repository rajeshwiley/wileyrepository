package com.wiley.facades.ags.i18n.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.data.RegionData;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.i18n.WileyCommonI18NService;


/**
 * Created on 1/22/2016.
 */

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AgsI18NFacadeTest
{
	private static final String USA_ISOCODE = "US";
	private static final String ARMED_FORCES_AFRICA_NAME = "Armed Forces Africa";
	private static final String ARMED_FORCES_PACIFIC_NAME = "Armed Forces Pacific";
	private static final String ALABAMA_NAME = "Alabama";
	private static final String WYOMING_NAME = "Wyoming";

	private final RegionData armedForceAfrica = new RegionData();
	private final RegionData armedForcePacific = new RegionData();
	private final RegionData alabama = new RegionData();
	private final RegionData wyoming = new RegionData();
	private final RegionData empty = new RegionData();

	@Mock
	private RegionModel alabamaModel;

	@Mock
	private RegionModel armedForceAfricaModel;

	@Mock
	private RegionModel armedForcePacificModel;

	@Mock
	private RegionModel wyomingModel;

	@Mock
	private RegionModel emptyModel;

	@Mock
	private BaseStoreModel baseStoreModel;

	@Mock
	private CountryModel countryModel;

	@Mock
	private CommonI18NService commonI18NService;

	@Mock
	private Converter<RegionModel, RegionData> regionConverter;

	@Mock
	private BaseStoreService baseStoreService;

	@Mock
	private WileyCommonI18NService wileyCommonI18NService;

	@InjectMocks
	private final AgsI18NFacade testAgsI18NFacade = new AgsI18NFacade();

	@Before
	public void setUp() throws Exception
	{
		testAgsI18NFacade.setRegionConverter(regionConverter);

		armedForceAfrica.setName(ARMED_FORCES_AFRICA_NAME);
		armedForcePacific.setName(ARMED_FORCES_PACIFIC_NAME);
		alabama.setName(ALABAMA_NAME);
		wyoming.setName(WYOMING_NAME);

		given(alabamaModel.getName()).willReturn(ALABAMA_NAME);
		given(armedForceAfricaModel.getName()).willReturn(ARMED_FORCES_AFRICA_NAME);
		given(armedForcePacificModel.getName()).willReturn(ARMED_FORCES_PACIFIC_NAME);
		given(wyomingModel.getName()).willReturn(WYOMING_NAME);

		List<RegionModel> regions = new ArrayList<>();
		regions.add(alabamaModel);
		regions.add(armedForceAfricaModel);
		regions.add(armedForcePacificModel);
		regions.add(wyomingModel);
		regions.add(emptyModel);

		given(commonI18NService.getCountry(USA_ISOCODE)).willReturn(countryModel);
		given(countryModel.getRegions()).willReturn(regions);
		given(regionConverter.convert(alabamaModel)).willReturn(alabama);
		given(regionConverter.convert(armedForceAfricaModel)).willReturn(armedForceAfrica);
		given(regionConverter.convert(armedForcePacificModel)).willReturn(armedForcePacific);
		given(regionConverter.convert(wyomingModel)).willReturn(wyoming);
		given(regionConverter.convert(emptyModel)).willReturn(empty);

		given(baseStoreService.getCurrentBaseStore()).willReturn(baseStoreModel);
		given(wileyCommonI18NService.getConfigurableRegionsForCurrentBaseStore(countryModel, baseStoreModel))
				.willReturn(regions);

	}

	/**
	 * Verified the correct order in the list of States
	 */
	@Test
	public void testSortOrderInList()
	{
		List<RegionData> regions = testAgsI18NFacade.getRegionsForCountryIso(USA_ISOCODE);

		assertEquals("Expected NULL on first position in list", regions.get(0).getName(), null);
		assertEquals("Expected position number 1 in the list for Alabama", regions.get(1).getName(), ALABAMA_NAME);
		assertEquals("Expected position number 2 in the list for Wyoming", regions.get(2).getName(), WYOMING_NAME);
		assertEquals("Expected position number 3 in the list for Armed Forces Africa", regions.get(3).getName(),
				ARMED_FORCES_AFRICA_NAME);
		assertEquals("Expected position number 4 in the list for Armed Forces Pacific", regions.get(4).getName(),
				ARMED_FORCES_PACIFIC_NAME);
	}

	@Test
	public void testAgsRegionNameComparator()
	{
		int compareAlabamaVsArmedForceAfrica = AgsI18NFacade.AgsRegionNameComparator.INSTANCE.compareInstances(alabamaModel,
				armedForceAfricaModel);
		int compareArmedForcePacificVsWyoming = AgsI18NFacade.AgsRegionNameComparator.INSTANCE.compareInstances(
				armedForcePacificModel, wyomingModel);
		int compareWyomingVsAlabama = AgsI18NFacade.AgsRegionNameComparator.INSTANCE.compareInstances(wyomingModel, alabamaModel);
		int compareArmedForcePacificVsArmedForceAfrica = AgsI18NFacade.AgsRegionNameComparator.INSTANCE.compareInstances(
				armedForcePacificModel, armedForceAfricaModel);
		int compareEmptyVsWyoming = AgsI18NFacade.AgsRegionNameComparator.INSTANCE.compareInstances(emptyModel, wyomingModel);
		int compareArmedForceAfricaVsEmpty = AgsI18NFacade.AgsRegionNameComparator.INSTANCE.compareInstances(
				armedForceAfricaModel, emptyModel);
		int compareArmedForcePacificVsArmedForcePacific = AgsI18NFacade.AgsRegionNameComparator.INSTANCE.compareInstances(
				armedForcePacificModel, armedForcePacificModel);

		assertTrue("Expected Alabama will be above ArmedForceAfrica in list of countries", compareAlabamaVsArmedForceAfrica < 0);
		assertTrue("Expected ArmedForcePacific will be under Wyoming in list of countries",
				compareArmedForcePacificVsWyoming > 0);
		assertTrue("Expected Wyoming will be under Alabama in list of countries", compareWyomingVsAlabama > 0);
		assertTrue("Expected ArmedForcePacific will be under ArmedForceAfrica in list of countries",
				compareArmedForcePacificVsArmedForceAfrica > 0);
		assertTrue("Expected Empty will be above Wyoming in list of countries", compareEmptyVsWyoming < 0);
		assertTrue("Expected ArmedForceAfrica will be under EMPTY in list of countries", compareArmedForceAfricaVsEmpty > 0);
		assertTrue("Expected equality for ArmedForcePacific", compareArmedForcePacificVsArmedForcePacific == 0);
	}
}
