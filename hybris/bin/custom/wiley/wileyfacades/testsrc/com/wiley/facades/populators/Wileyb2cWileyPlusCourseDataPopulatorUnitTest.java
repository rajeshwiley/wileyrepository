package com.wiley.facades.populators;

import de.hybris.bootstrap.annotations.UnitTest;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import com.wiley.facades.wileyb2c.product.data.WileyPlusCourseData;
import com.wiley.facades.wileyb2c.product.data.WileyPlusCourseInfoData;
import com.wiley.facades.wileyplus.cart.activation.dto.CartActivationRequestDto;
import com.wiley.facades.wileyplus.cart.activation.dto.WileyPlusCourseDto;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Created by Anton_Lukyanau on 8/23/2016.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cWileyPlusCourseDataPopulatorUnitTest
{

	private static final String AUTHORS = "PUSHKIN";
	private static final String IMAGE_URL = "https://my.com/image";
	private static final String ISBN = "1234567";
	private static final String TITLE = "Onegin";

	private Wileyb2cWileyPlusCourseDataPopulator populator;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		populator = new Wileyb2cWileyPlusCourseDataPopulator();
	}

	@Test
	public void testPopulateCourseData()
	{
		CartActivationRequestDto cartActivationRequestDto = mock(CartActivationRequestDto.class);
		WileyPlusCourseDto course = mock(WileyPlusCourseDto.class);
		given(cartActivationRequestDto.getCourse()).willReturn(course);
		given(course.getAuthors()).willReturn(AUTHORS);
		given(course.getImageUrl()).willReturn(IMAGE_URL);
		given(course.getIsbn()).willReturn(ISBN);
		given(course.getTitle()).willReturn(TITLE);
		WileyPlusCourseInfoData wileyPlusCourseInfoData = new WileyPlusCourseInfoData();

		populator.populate(cartActivationRequestDto, wileyPlusCourseInfoData);

		final WileyPlusCourseData wileyPlusCourseData = wileyPlusCourseInfoData.getCourse();
		Assert.assertEquals(AUTHORS, wileyPlusCourseData.getAuthors());
		Assert.assertEquals(IMAGE_URL, wileyPlusCourseData.getImageUrl());
		Assert.assertEquals(ISBN, wileyPlusCourseData.getIsbn());
		Assert.assertEquals(TITLE, wileyPlusCourseData.getTitle());
	}

	@Test
	public void testPopulateEmptyCourseData()
	{
		CartActivationRequestDto cartActivationRequestDto = mock(CartActivationRequestDto.class);
		given(cartActivationRequestDto.getCourse()).willReturn(null);
		WileyPlusCourseInfoData wileyPlusCourseInfoData = new WileyPlusCourseInfoData();

		populator.populate(cartActivationRequestDto, wileyPlusCourseInfoData);

		final WileyPlusCourseData wileyPlusCourseData = wileyPlusCourseInfoData.getCourse();
		Assert.assertEquals(StringUtils.EMPTY, wileyPlusCourseData.getAuthors());
		Assert.assertEquals(StringUtils.EMPTY, wileyPlusCourseData.getImageUrl());
		Assert.assertEquals(StringUtils.EMPTY, wileyPlusCourseData.getIsbn());
		Assert.assertEquals(StringUtils.EMPTY, wileyPlusCourseData.getTitle());
	}

}
