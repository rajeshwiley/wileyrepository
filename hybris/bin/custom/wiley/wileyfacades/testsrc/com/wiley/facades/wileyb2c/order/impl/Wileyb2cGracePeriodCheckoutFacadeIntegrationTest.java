package com.wiley.facades.wileyb2c.order.impl;


import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.model.WileyProductSummaryModel;
import com.wiley.core.servicelayer.i18n.WileycomI18NService;
import com.wiley.facades.wileyb2c.order.Wileyb2cGracePeriodCheckoutFacade;
import com.wiley.facades.wileyplus.cart.activation.dto.CartActivationRequestDto;
import com.wiley.facades.wileyplus.cart.activation.dto.PurchaseOptionsDto;
import com.wiley.facades.wileyplus.cart.activation.dto.WebLinkDto;
import com.wiley.facades.wileyplus.cart.activation.dto.WileyPlusCourseDto;

@IntegrationTest
public class Wileyb2cGracePeriodCheckoutFacadeIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	private static final String BASE_SITE = "wileyb2c";
	private static final String CATALOG_ID = "wileyProductCatalog";
	private static final String CATALOG_VERSION = "Online";
	private static final String AUTHORS = "authors";
	private static final String IMAGE_URL = "imageUrl";
	private static final String ISBN = "isbn";
	private static final String PROD_EDITION = "prodEdition";
	private static final String TITLE = "title";
	private static final String CLASS_SECTION_ID = "classSectionId";
	private static final String USER_ID = "userId";
	private static final String ADDITIONAL_INFO_VALUE = "additionalInfoValue";
	private static final String CONTINUE_URL = "continueUrl";
	private static final int GRACE_PERIOD_DURATION = 14;
	@Resource
	private Wileyb2cGracePeriodCheckoutFacade wileyb2cGracePeriodCheckoutFacade;
	@Resource
	private BaseSiteService baseSiteService;
	@Resource
	private UserService userService;
	@Resource
	private CatalogVersionService catalogVersionService;
	@Resource
	private CustomerAccountService customerAccountService;
	@Resource
	private BaseStoreService baseStoreService;
	@Resource
	private CommerceCartService commerceCartService;
	@Resource
	private WileycomI18NService wileycomI18NService;

	private PurchaseOptionsDto purchaseOptionsDto;
	private CartActivationRequestDto activationRequestDto;

	@Before
	public void setup() throws ImpExException
	{
		PurchaseOptionsDto purchaseOptionsDto = new PurchaseOptionsDto()
				.gracePeriodDuration(GRACE_PERIOD_DURATION);

		WileyPlusCourseDto courseDto = new WileyPlusCourseDto()
				.authors(AUTHORS)
				.imageUrl(IMAGE_URL)
				.isbn(ISBN)
				.productEdition(PROD_EDITION)
				.title(TITLE);

		activationRequestDto = new CartActivationRequestDto()
				.classSectionId(CLASS_SECTION_ID)
				.userId(USER_ID)
				.additionalInfo(ADDITIONAL_INFO_VALUE)
				.continueURL(new WebLinkDto().url(CONTINUE_URL))
				.course(courseDto)
				.purchaseOptions(purchaseOptionsDto);

		baseSiteService.setCurrentBaseSite(baseSiteService.getBaseSiteForUID(BASE_SITE), true);
		userService.setCurrentUser(userService.getAdminUser());
		catalogVersionService.setSessionCatalogVersion(CATALOG_ID, CATALOG_VERSION);
		wileycomI18NService.setDefaultCurrentCountry();
		importCsv("/wileyfacades/test/Wileyb2cGracePeriodCheckoutFacade/testdata.impex", DEFAULT_ENCODING);

	}

	@Test
	public void testSubmitOrder() throws CommerceCartModificationException, InvalidCartException, CalculationException
	{
		userService.setCurrentUser(userService.getAnonymousUser());
		String orderCode = wileyb2cGracePeriodCheckoutFacade.placeGracePeriodOrder(activationRequestDto);
		OrderModel order = customerAccountService.getOrderForCode(orderCode, baseStoreService.getCurrentBaseStore());

		verifyOrder(order);
		verifyOrderEntry(order);
	}

	@Test
	public void testOrderShouldNotBeSubmittedWhenISBNIsWrong() throws CommerceCartModificationException,
			InvalidCartException, CalculationException
	{
		userService.setCurrentUser(userService.getAnonymousUser());
		activationRequestDto.getCourse().setIsbn("wrongIsbn");
		try {
			wileyb2cGracePeriodCheckoutFacade.placeGracePeriodOrder(activationRequestDto);
			fail("Exception was expected");
		} catch (UnknownIdentifierException e) {
			//got it, thanks
		}
		List<CartModel> carts = commerceCartService.getCartsForSiteAndUser(baseSiteService.getCurrentBaseSite(),
				userService.getAnonymousUser());
		assertEquals(0, carts.size());
	}

	private void verifyOrder(final OrderModel order)
	{
		assertNotNull(order);
		assertEquals(CONTINUE_URL, order.getConfirmationPageContinueUrl());
	}

	private void verifyOrderEntry(final OrderModel order)
	{
		List<AbstractOrderEntryModel> entries = order.getEntries();
		assertNotNull(entries);
		assertEquals(1, entries.size());

		AbstractOrderEntryModel entry = entries.get(0);
		assertEquals(ISBN, entry.getProduct().getIsbn());

		WileyProductSummaryModel productSummary = entry.getProductSummary();
		assertEquals(AUTHORS, productSummary.getAuthors());
		assertEquals(PROD_EDITION, productSummary.getEdition());
		assertEquals(IMAGE_URL, productSummary.getPictureUrl());
		assertEquals(TITLE, productSummary.getName());
		assertEquals(Integer.valueOf(GRACE_PERIOD_DURATION), productSummary.getFreeTrialPeriod());

		String purchaseOptionParameter = entry.getAdditionalInfo();

		assertEquals(ADDITIONAL_INFO_VALUE, purchaseOptionParameter);
	}


}
