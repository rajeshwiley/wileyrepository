package com.wiley.facades.order.converters.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.subscriptionfacades.data.SubscriptionTermData;
import de.hybris.platform.subscriptionservices.model.SubscriptionTermModel;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.product.WileySubscriptionTermService;


/**
 * Author Herman_Chukhrai (EPAM)
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cCartModificationPopulatorTest
{

	@InjectMocks
	private Wileyb2cCartModificationPopulator wileyb2cCartModificationPopulator;

	@Mock
	private CommerceCartModification commerceCartModification;
	@Mock
	private AbstractOrderEntryModel abstractOrderEntryModel;
	@Mock
	private SubscriptionTermModel subscriptionTermModel;
	@Mock
	private SubscriptionTermModel originalTermModel;
	@Mock
	private SubscriptionTermData subscriptionTermData;
	@Mock
	private SubscriptionTermData originalTermData;
	@Mock
	private Converter<AbstractOrderEntryModel, OrderEntryData> orderEntryConverter;
	@Mock
	private Converter<SubscriptionTermModel, SubscriptionTermData> subscriptionTermConverter;
	@Mock
	private WileySubscriptionTermService wileySubscriptionTermService;


	private static final String ORIGINAL_SUBSCRIPTION_ID = "ORIGINAL_SUBSCRIPTION_ID";
	private static final String NEW_SUBSCRIPTION_ID = "NEW_SUBSCRIPTION_ID";

	@Test
	public void testPopulate() throws Exception
	{
		//Given
		final CartModificationData target = new CartModificationData();
		when(commerceCartModification.getEntry()).thenReturn(abstractOrderEntryModel);
		when(abstractOrderEntryModel.getOriginalSubscriptionId()).thenReturn(ORIGINAL_SUBSCRIPTION_ID);
		when(abstractOrderEntryModel.getSubscriptionTerm()).thenReturn(subscriptionTermModel);
		when(subscriptionTermModel.getId()).thenReturn(NEW_SUBSCRIPTION_ID);
		when(originalTermModel.getId()).thenReturn(ORIGINAL_SUBSCRIPTION_ID);
		when(orderEntryConverter.convert(abstractOrderEntryModel)).thenReturn(new OrderEntryData());
		when(wileySubscriptionTermService.getSubscriptionTerm(ORIGINAL_SUBSCRIPTION_ID)).thenReturn(originalTermModel);
		when(subscriptionTermConverter.convert(subscriptionTermModel)).thenReturn(subscriptionTermData);
		when(subscriptionTermConverter.convert(originalTermModel)).thenReturn(originalTermData);

		//When
		wileyb2cCartModificationPopulator.populate(commerceCartModification, target);

		//Then
		assertEquals(originalTermData, target.getEntry().getOriginalSubscriptionTerm());
		assertEquals(subscriptionTermData, target.getEntry().getSubscriptionTerm());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testNullModelProvided() throws Exception
	{
		//When
		wileyb2cCartModificationPopulator.populate(null, new CartModificationData());
		//Then expect exception
	}

	@Test(expected = IllegalArgumentException.class)
	public void testNullBeanProvided() throws Exception
	{
		//When
		wileyb2cCartModificationPopulator.populate(commerceCartModification, null);
		//Then expect exception
	}
}