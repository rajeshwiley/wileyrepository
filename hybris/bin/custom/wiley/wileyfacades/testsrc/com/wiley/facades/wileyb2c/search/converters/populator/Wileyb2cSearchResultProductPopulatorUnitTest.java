package com.wiley.facades.wileyb2c.search.converters.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.wileyb2c.i18n.Wileyb2cI18NService;
import com.wiley.facades.product.data.ExternalStoreData;

import static junit.framework.TestCase.assertSame;


/**
 * Created by Maksim_Kozich on 4/30/2017.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cSearchResultProductPopulatorUnitTest
{
	private static final String SHORT_MESSAGE = "short message";
	private static final String TOOLTIP = "tooltip";
	private static final String EXTERNAL_STORE_URL_PROPERTY = "externalStoreUrl";
	private static final String EXTERNAL_STORE_TYPE_PROPERTY = "externalStoreType";
	private static final String HAS_EXTERNAL_STORES_PROPERTY = "hasExternalStores";
	public static final String TEST_TYPE = "testType";
	public static final String TEST_URL = "testUrl";
	public static final String REQUEST_QUOTE_TYPE = "REQUEST_QUOTE";
	public static final String REQUEST_QUOTE_URL = "REQUEST_QUOTE_URL";
	private static final String PUBLICATION_DATE_PROPERTY = "publicationDateProperty";

	@Spy
	@InjectMocks
	private Wileyb2cSearchResultProductPopulator wileyb2cSearchResultProductPopulator;

	@Mock
	private Wileyb2cI18NService wileyb2cI18NServiceMock;

	@Mock
	private SearchResultValueData searchResultValueDataMock;

	@Mock
	private Map<String, Object> values;

	private ProductData productData;
	private VariantOptionData variantOptionData;
	private PriceData priceData;

	@Before
	public void setUp() throws Exception
	{
		doNothing().when(wileyb2cSearchResultProductPopulator).superPopulatePrices(any(), any());
		productData = new ProductData();
		priceData = new PriceData();
	}

	@Test
	public void populateTaxDisclaimerWhenPriceIsPresented() throws Exception
	{
		//Given
		when(wileyb2cI18NServiceMock.getCurrentCountryTaxShortMsg()).thenReturn(Optional.of(SHORT_MESSAGE));
		when(wileyb2cI18NServiceMock.getCurrentCountryTaxTooltip()).thenReturn(Optional.of(TOOLTIP));
		productData.setPrice(priceData);
		doNothing().when(wileyb2cSearchResultProductPopulator).populateExternalStores(any(), any());

		//When
		wileyb2cSearchResultProductPopulator.populatePrices(searchResultValueDataMock, productData);

		//Then
		assertSame(priceData, productData.getPrice());
		assertEquals(SHORT_MESSAGE, priceData.getOptionalTaxShortMessage());
		assertEquals(TOOLTIP, priceData.getOptionalTaxTooltip());
	}

	@Test
	public void populateTaxDisclaimerWhenPriceIsNotPresented() throws Exception
	{
		//Given
		when(wileyb2cI18NServiceMock.getCurrentCountryTaxShortMsg()).thenReturn(Optional.of(SHORT_MESSAGE));
		when(wileyb2cI18NServiceMock.getCurrentCountryTaxTooltip()).thenReturn(Optional.of(TOOLTIP));
		doNothing().when(wileyb2cSearchResultProductPopulator).populateExternalStores(any(), any());

		//When
		wileyb2cSearchResultProductPopulator.populatePrices(searchResultValueDataMock, productData);

		//Then
		assertNotNull(productData.getPrice());
		assertEquals(SHORT_MESSAGE, productData.getPrice().getOptionalTaxShortMessage());
		assertEquals(TOOLTIP, productData.getPrice().getOptionalTaxTooltip());
	}

	@Test
	public void populatePublicationDateWhenNoDatesShouldNotPopulate() throws Exception
	{
		when(searchResultValueDataMock.getValues()).thenReturn(Collections.emptyMap());
		variantOptionData = new VariantOptionData();

		wileyb2cSearchResultProductPopulator.populatePublicationDate(variantOptionData, searchResultValueDataMock);

		assertNull(variantOptionData.getPublicationDate());
	}

	@Test
	public void populatePublicationDateExistsShouldPopulateFirst() throws Exception
	{
		variantOptionData = new VariantOptionData();
		wileyb2cSearchResultProductPopulator.setPublicationDateProperty(PUBLICATION_DATE_PROPERTY);
		final Calendar testedCalendar = Calendar.getInstance();
		testedCalendar.set(2016, 5, 22, 00, 59, 59);
		final Date date = testedCalendar.getTime();
		when(searchResultValueDataMock.getValues()).thenReturn(
				Collections.singletonMap(PUBLICATION_DATE_PROPERTY, new ArrayList<>(Collections.singletonList(date))));

		wileyb2cSearchResultProductPopulator.populatePublicationDate(variantOptionData, searchResultValueDataMock);

		assertEquals(variantOptionData.getPublicationDate(), date);
	}


	@Test
	public void populateNullTaxDisclaimerWhenOptionalIsEmpty() throws Exception
	{
		//Given
		when(wileyb2cI18NServiceMock.getCurrentCountryTaxShortMsg()).thenReturn(Optional.empty());
		when(wileyb2cI18NServiceMock.getCurrentCountryTaxTooltip()).thenReturn(Optional.empty());
		doNothing().when(wileyb2cSearchResultProductPopulator).populateExternalStores(any(), any());

		//When
		wileyb2cSearchResultProductPopulator.populatePrices(searchResultValueDataMock, productData);

		//Then
		assertNotNull(productData.getPrice());
		assertNull(productData.getPrice().getOptionalTaxShortMessage());
		assertNull(productData.getPrice().getOptionalTaxTooltip());
	}

	@Test
	public void populateExternalStoresWhenStoreTypeIsEmpty() throws Exception
	{
		//Given
		variantOptionData = new VariantOptionData();
		wileyb2cSearchResultProductPopulator.setExternalStoreUrlProperty(EXTERNAL_STORE_URL_PROPERTY);
		wileyb2cSearchResultProductPopulator.setExternalStoreTypeProperty(EXTERNAL_STORE_TYPE_PROPERTY);
		wileyb2cSearchResultProductPopulator.setHasExternalStoresProperty(HAS_EXTERNAL_STORES_PROPERTY);
		when(searchResultValueDataMock.getValues()).thenReturn(values);
		when(values.get(HAS_EXTERNAL_STORES_PROPERTY)).thenReturn(Boolean.TRUE);
		when(values.get(EXTERNAL_STORE_URL_PROPERTY)).thenReturn(TEST_URL);
		when(values.get(EXTERNAL_STORE_TYPE_PROPERTY)).thenReturn(StringUtils.EMPTY);

		//When
		wileyb2cSearchResultProductPopulator.populateExternalStores(searchResultValueDataMock, variantOptionData);

		//Then
		assertTrue(variantOptionData.getHasExternalStores());
		assertNull(variantOptionData.getExternalStores());
	}

	@Test
	public void populateExternalStoresWhenStoreUrlIsEmpty() throws Exception
	{
		//Given
		variantOptionData = new VariantOptionData();
		wileyb2cSearchResultProductPopulator.setExternalStoreUrlProperty(EXTERNAL_STORE_URL_PROPERTY);
		wileyb2cSearchResultProductPopulator.setExternalStoreTypeProperty(EXTERNAL_STORE_TYPE_PROPERTY);
		wileyb2cSearchResultProductPopulator.setHasExternalStoresProperty(HAS_EXTERNAL_STORES_PROPERTY);
		when(searchResultValueDataMock.getValues()).thenReturn(values);
		when(values.get(HAS_EXTERNAL_STORES_PROPERTY)).thenReturn(Boolean.TRUE);
		when(values.get(EXTERNAL_STORE_URL_PROPERTY)).thenReturn(StringUtils.EMPTY);
		when(values.get(EXTERNAL_STORE_TYPE_PROPERTY)).thenReturn(TEST_TYPE);

		//When
		wileyb2cSearchResultProductPopulator.populateExternalStores(searchResultValueDataMock, variantOptionData);

		//Then
		assertTrue(variantOptionData.getHasExternalStores());
		List<ExternalStoreData> externalStores = variantOptionData.getExternalStores();
		assertNotNull(externalStores);
		assertEquals(1, externalStores.size());
		assertEquals(TEST_TYPE, externalStores.get(0).getType());
		assertEquals(StringUtils.EMPTY, externalStores.get(0).getUrl());
	}

	@Test
	public void populateExternalStoresWhenStoreUrlAndTypeFound() throws Exception
	{
		//Given
		variantOptionData = new VariantOptionData();
		wileyb2cSearchResultProductPopulator.setExternalStoreUrlProperty(EXTERNAL_STORE_URL_PROPERTY);
		wileyb2cSearchResultProductPopulator.setExternalStoreTypeProperty(EXTERNAL_STORE_TYPE_PROPERTY);
		wileyb2cSearchResultProductPopulator.setHasExternalStoresProperty(HAS_EXTERNAL_STORES_PROPERTY);
		when(searchResultValueDataMock.getValues()).thenReturn(values);
		when(values.get(HAS_EXTERNAL_STORES_PROPERTY)).thenReturn(Boolean.TRUE);
		when(values.get(EXTERNAL_STORE_URL_PROPERTY)).thenReturn(TEST_URL);
		when(values.get(EXTERNAL_STORE_TYPE_PROPERTY)).thenReturn(TEST_TYPE);

		//When
		wileyb2cSearchResultProductPopulator.populateExternalStores(searchResultValueDataMock, variantOptionData);

		//Then
		assertTrue(variantOptionData.getHasExternalStores());
		List<ExternalStoreData> externalStores = variantOptionData.getExternalStores();
		assertNotNull(externalStores);
		assertEquals(1, externalStores.size());
		assertEquals(TEST_TYPE, externalStores.get(0).getType());
		assertEquals(TEST_URL, externalStores.get(0).getUrl());
	}

	@Test
	public void populateExternalStoresWhenStoreIsRequestQuote() throws Exception
	{
		//Given
		variantOptionData = new VariantOptionData();
		wileyb2cSearchResultProductPopulator.setExternalStoreUrlProperty(EXTERNAL_STORE_URL_PROPERTY);
		wileyb2cSearchResultProductPopulator.setExternalStoreTypeProperty(EXTERNAL_STORE_TYPE_PROPERTY);
		wileyb2cSearchResultProductPopulator.setHasExternalStoresProperty(HAS_EXTERNAL_STORES_PROPERTY);
		when(searchResultValueDataMock.getValues()).thenReturn(values);
		when(values.get(HAS_EXTERNAL_STORES_PROPERTY)).thenReturn(Boolean.TRUE);
		when(values.get(EXTERNAL_STORE_URL_PROPERTY)).thenReturn(REQUEST_QUOTE_URL);
		when(values.get(EXTERNAL_STORE_TYPE_PROPERTY)).thenReturn(REQUEST_QUOTE_TYPE);

		//When
		wileyb2cSearchResultProductPopulator.populateExternalStores(searchResultValueDataMock, variantOptionData);

		//Then
		assertTrue(variantOptionData.getHasExternalStores());
		List<ExternalStoreData> externalStores = variantOptionData.getExternalStores();
		assertNotNull(externalStores);
		assertEquals(1, externalStores.size());
		assertEquals(REQUEST_QUOTE_TYPE, externalStores.get(0).getType());
		assertEquals(REQUEST_QUOTE_URL, externalStores.get(0).getUrl());
		assertTrue(variantOptionData.isHidePrice());
	}

}