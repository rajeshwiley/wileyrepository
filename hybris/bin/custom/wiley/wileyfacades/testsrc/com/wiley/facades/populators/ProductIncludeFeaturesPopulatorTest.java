/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package com.wiley.facades.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.enums.ClassificationAttributeTypeEnum;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.classification.features.FeatureValue;
import de.hybris.platform.commercefacades.product.data.FeatureData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatcher;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.wiley.core.enums.ClassAttributeAssignmentTypeEnum;

import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


/**
 * Test suite for {@link com.wiley.facades.populators.ProductClassificationPopulator}
 */
@UnitTest
public class ProductIncludeFeaturesPopulatorTest
{
	@Mock
	private Converter<Feature, FeatureData> featureConverter;

	private ProductIncludeFeaturesPopulator<FeatureList, ProductData> productIncludeFeaturesPopulator;

	/**
	 * Only features with assignment type INCLUDES and value set to TRUE must be included into result product data.
	 */
	private static final Feature VALID_FEATURE_1 = getMockFeature(Boolean.TRUE, ClassAttributeAssignmentTypeEnum.INCLUDES,
			ClassificationAttributeTypeEnum.BOOLEAN);
	private static final Feature VALID_FEATURE_2 = getMockFeature(Boolean.TRUE, ClassAttributeAssignmentTypeEnum.INCLUDES,
			ClassificationAttributeTypeEnum.BOOLEAN);
	/**
	 * Some invalid features which must not be included into result product data.
	 */
	private static final Feature INVALID_FEATURE_FALSE = getMockFeature(Boolean.FALSE, ClassAttributeAssignmentTypeEnum.INCLUDES,
			ClassificationAttributeTypeEnum.BOOLEAN);
	private static final Feature INVALID_FEATURE_FALSE_AND_NULL_TYPE = getMockFeature(Boolean.FALSE, null,
			ClassificationAttributeTypeEnum.BOOLEAN);
	private static final Feature INVALID_FEATURE_STRING = getMockFeature("String feature INCLUDES",
			ClassAttributeAssignmentTypeEnum.INCLUDES,
			ClassificationAttributeTypeEnum.STRING);
	private static final Feature INVALID_FEATURE_STRING_AND_NULL_TYPE = getMockFeature("String feature null", null,
			ClassificationAttributeTypeEnum.STRING);

	private static final FeatureData VALID_FEATURE_DATA_1 = getMockFeatureData();
	private static final FeatureData VALID_FEATURE_DATA_2 = getMockFeatureData();
	private static final FeatureData INVALID_FEATURE_DATA_FALSE = getMockFeatureData();
	private static final FeatureData INVALID_FEATURE_DATA_FALSE_AND_NULL_TYPE = getMockFeatureData();
	private static final FeatureData INVALID_FEATURE_DATA_STRING = getMockFeatureData();
	private static final FeatureData INVALID_FEATURE_DATA_STRING_AND_NULL_TYPE = getMockFeatureData();


	/**
	 * Sets up.
	 */
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);

		productIncludeFeaturesPopulator = new ProductIncludeFeaturesPopulator<>();
		productIncludeFeaturesPopulator.setFeatureConverter(featureConverter);

		when(featureConverter.convert(VALID_FEATURE_1)).thenReturn(VALID_FEATURE_DATA_1);
		when(featureConverter.convert(VALID_FEATURE_2)).thenReturn(VALID_FEATURE_DATA_2);
		when(featureConverter.convert(INVALID_FEATURE_FALSE)).thenReturn(INVALID_FEATURE_DATA_FALSE);
		when(featureConverter.convert(INVALID_FEATURE_FALSE_AND_NULL_TYPE)).thenReturn(INVALID_FEATURE_DATA_FALSE_AND_NULL_TYPE);
		when(featureConverter.convert(INVALID_FEATURE_STRING)).thenReturn(INVALID_FEATURE_DATA_STRING);
		when(featureConverter.convert(INVALID_FEATURE_STRING_AND_NULL_TYPE))
				.thenReturn(INVALID_FEATURE_DATA_STRING_AND_NULL_TYPE);
	}


	/**
	 * Test populate.
	 */
	@Test
	public void testPopulate()
	{
		final FeatureList source = getMockFeatureList();
		final ProductData result = mock(ProductData.class);
		productIncludeFeaturesPopulator.populate(source, result);
		verify(result).setIncludeFeatures(argThat(new IsListContainsOnlySpecifiedElements(VALID_FEATURE_DATA_1,
				VALID_FEATURE_DATA_2)));
	}

	private static FeatureList getMockFeatureList()
	{
		FeatureList result = mock(FeatureList.class);

		final List<Feature> features = new ArrayList<>();

		features.add(VALID_FEATURE_1);
		features.add(INVALID_FEATURE_FALSE);
		features.add(INVALID_FEATURE_FALSE_AND_NULL_TYPE);
		features.add(INVALID_FEATURE_STRING);
		features.add(INVALID_FEATURE_STRING_AND_NULL_TYPE);
		features.add(VALID_FEATURE_2);

		when(result.getFeatures()).thenReturn(features);

		return result;
	}

	private static Feature getMockFeature(final Object value, final ClassAttributeAssignmentTypeEnum assignmentType,
			final ClassificationAttributeTypeEnum attributeType)
	{

		FeatureValue featureValueStub = mock(FeatureValue.class);
		when(featureValueStub.getValue()).thenReturn(value);

		ClassAttributeAssignmentModel classAttributeAssignmentModelStub = mock(ClassAttributeAssignmentModel.class);
		when(classAttributeAssignmentModelStub.getType()).thenReturn(assignmentType);
		when(classAttributeAssignmentModelStub.getAttributeType()).thenReturn(attributeType);

		final Feature featureStub = mock(Feature.class);
		when(featureStub.getValue()).thenReturn(featureValueStub);
		when(featureStub.getClassAttributeAssignment()).thenReturn(classAttributeAssignmentModelStub);

		return featureStub;
	}


	private static FeatureData getMockFeatureData()
	{
		return mock(FeatureData.class);
	}


	class IsListContainsOnlySpecifiedElements extends ArgumentMatcher<List>
	{

		private List expectedElements;

		/**
		 * Instantiates a new instance with specified expected elements
		 *
		 * @param expectedElements
		 * 		the expected elements
		 */
		IsListContainsOnlySpecifiedElements(final Object... expectedElements)
		{
			this.expectedElements = Arrays.asList(expectedElements);
		}

		@Override
		public boolean matches(final Object list)
		{
			return (expectedElements).equals(list);
		}
	}
}
