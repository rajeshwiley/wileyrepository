package com.wiley.facades.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.InvoicePaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.paypal.hybris.model.PaypalPaymentInfoModel;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyPaymentInfoOrderPopulatorTest
{
	@Mock
	private AbstractOrderModel orderModel;

	private WileyPaymentInfoOrderPopulator testInstance = new WileyPaymentInfoOrderPopulator();

	@Test
	public void shouldSetZeroOrderPaymentTypeIfPaymentInfoIsNull() throws Exception
	{
		//Given
		when(orderModel.getPaymentInfo()).thenReturn(null);
		AbstractOrderData abstractOrderData = new AbstractOrderData();
		//When
		testInstance.populate(orderModel, abstractOrderData);
		//Then
		assertEquals(WileyPaymentInfoOrderPopulator.PAYMENT_TYPE_ZERO_ORDER,
				abstractOrderData.getWileyPaymentInfo().getPaymentType());
	}

	@Test
	public void shouldPaypalPaymentTypeIfPaymentInfoIsOfPaypalPaymentInfoType() throws Exception
	{
		//Given
		when(orderModel.getPaymentInfo()).thenReturn(givenPaymentInfoModel(PaypalPaymentInfoModel.class));
		AbstractOrderData abstractOrderData = new AbstractOrderData();
		//When
		testInstance.populate(orderModel, abstractOrderData);
		//Then
		assertEquals(WileyPaymentInfoOrderPopulator.PAYMENT_TYPE_PAYPAL,
				abstractOrderData.getWileyPaymentInfo().getPaymentType());
	}

	@Test
	public void shouldSetInvoicePaymentTypeIfPaymentInfoIsOfInvoicePaymentInfoType() throws Exception
	{
		//Given
		when(orderModel.getPaymentInfo()).thenReturn(givenPaymentInfoModel(InvoicePaymentInfoModel.class));
		AbstractOrderData abstractOrderData = new AbstractOrderData();
		//When
		testInstance.populate(orderModel, abstractOrderData);
		//Then
		assertEquals(WileyPaymentInfoOrderPopulator.PAYMENT_TYPE_INVOICE,
				abstractOrderData.getWileyPaymentInfo().getPaymentType());
	}

	@Test
	public void shouldSetCardPaymentTypeIfPaymentInfoIsOfCreditCardPaymentInfoType() throws Exception
	{
		//Given
		when(orderModel.getPaymentInfo()).thenReturn(givenPaymentInfoModel(CreditCardPaymentInfoModel.class));
		AbstractOrderData abstractOrderData = new AbstractOrderData();
		//When
		testInstance.populate(orderModel, abstractOrderData);
		//Then
		assertEquals(WileyPaymentInfoOrderPopulator.PAYMENT_TYPE_CARD,
				abstractOrderData.getWileyPaymentInfo().getPaymentType());
	}


	private PaymentInfoModel givenPaymentInfoModel(final Class<? extends PaymentInfoModel> aClass) {
		return mock(aClass);
	}

}