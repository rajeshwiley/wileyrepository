package com.wiley.facades.wel.order.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.CartRestorationData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.order.CartService;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.cart.WileyCartFactory;
import com.wiley.core.product.WileyCountableProductService;
import com.wiley.core.product.WileyProductEditionFormatService;
import com.wiley.core.servicelayer.i18n.WileycomI18NService;


/**
 * Integrational test for {@link WelMultiDimensionalCartFacadeImpl}.
 *
 * @author Aliaksei_Zlobich
 */
@IntegrationTest
public class WelMultiDimensionalCartFacadeImplIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{

	/**
	 * The constant CATALOG_ID.
	 */
	public static final String CATALOG_ID = "welProductCatalog";
	/**
	 * The constant CATALOG_VERSION.
	 */
	public static final String CATALOG_VERSION = "Online";
	public static final String TEST_RESOURCES_FOLDER = "/wileyfacades/test/WelMultiDimensionalCartFacadeIntegrationTest";

	private static final String PARTNER_ID = "TEST_PARTNER";
	private static final String STUDENT_COUPON_CODE = "STUDENT";

	@Resource
	private CatalogVersionService catalogVersionService;

	@Resource
	private UserService userService;

	@Resource(name = "welMultiDimensionalCartFacade")
	private WelMultiDimensionalCartFacadeImpl wileyMultiDimensionalCartFacade;

	@Resource
	private CartService cartService;

	@Resource
	private CommerceCartService commerceCartService;

	@Resource
	private BaseSiteService baseSiteService;

	@Resource
	private WileyProductEditionFormatService wileyProductEditionFormatService;

	@Resource(name = "welAgsCountableProductService")
	private WileyCountableProductService wileyCountableProductService;

	@Resource
	private ProductService productService;

	@Resource
	private WileyCartFactory wileyCartFactory;

	@Resource
	private WileycomI18NService wileycomI18NService;

	// Test data
	private OrderEntryData orderEntryAUD;
	private final String welCPAPlatinumAUDPrint = "WEL_CPA_PLATINUM_AUD_PRINT";
	private final String welCPAPlatinumAUDEbook = "WEL_CPA_PLATINUM_AUD_EBOOK";

	private OrderEntryData orderEntryBEC;
	private final String welCPAPlatinumBECPrint = "WEL_CPA_PLATINUM_BEC_PRINT";

	private OrderEntryData orderEntryFAR;
	private final String welCPAPlatinumFARPrint = "WEL_CPA_PLATINUM_FAR_PRINT";

	private OrderEntryData orderEntryREG;
	private final String welCPAPlatinumREGPrint = "WEL_CPA_PLATINUM_REG_PRINT";

	@Before
	public void setUp() throws Exception
	{

		importCsv(TEST_RESOURCES_FOLDER + "/testDefaultWelMultiDimensionalCartFacade.impex", DEFAULT_ENCODING);
		importCsv(TEST_RESOURCES_FOLDER + "/discount.impex", DEFAULT_ENCODING);

		baseSiteService.setCurrentBaseSite(baseSiteService.getBaseSiteForUID("wel"), true);
		wileycomI18NService.setDefaultCurrentCountry();

		// AUD
		orderEntryAUD = new OrderEntryData();
		final ProductData product1 = new ProductData();
		product1.setCode(welCPAPlatinumAUDPrint);
		orderEntryAUD.setProduct(product1);
		orderEntryAUD.setQuantity((long) 1);

		// BEC
		orderEntryBEC = new OrderEntryData();
		final ProductData product2 = new ProductData();
		product2.setCode(welCPAPlatinumBECPrint);
		orderEntryBEC.setProduct(product2);
		orderEntryBEC.setQuantity((long) 1);

		// FAR
		orderEntryFAR = new OrderEntryData();
		final ProductData product3 = new ProductData();
		product3.setCode(welCPAPlatinumFARPrint);
		orderEntryFAR.setProduct(product3);
		orderEntryFAR.setQuantity((long) 1);

		// REG
		orderEntryREG = new OrderEntryData();
		final ProductData product4 = new ProductData();
		product4.setCode(welCPAPlatinumREGPrint);
		orderEntryREG.setProduct(product4);
		orderEntryREG.setQuantity((long) 1);
	}

	@Test
	public void checkAbilityForEndUserToChangeProductQuantityTest() throws CommerceCartModificationException
	{
		// Given
		catalogVersionService.setSessionCatalogVersion(CATALOG_ID, CATALOG_VERSION);

		// When
		wileyMultiDimensionalCartFacade.addOrderEntryList(Arrays.asList(orderEntryAUD, orderEntryBEC, orderEntryFAR));

		// Then
		final CartData cartData = wileyMultiDimensionalCartFacade.getSessionCartWithEntryOrdering(true);
		final List<OrderEntryData> entries = cartData.getEntries();
		entries.stream().forEach(entry ->
		{
			checkChangeQuantityStatus(entry);
		});
	}

	private void checkChangeQuantityStatus(final OrderEntryData entry)
	{
		switch (entry.getProduct().getCode())
		{
			case (welCPAPlatinumAUDPrint):
				assertEquals(true, entry.isDoesProductHaveQuantity());
				break;
			case (welCPAPlatinumBECPrint):
				assertEquals(false, entry.isDoesProductHaveQuantity());
				break;
			case (welCPAPlatinumFARPrint):
				assertEquals(false, entry.isDoesProductHaveQuantity());
				break;
			default:
				throw new IllegalArgumentException("Cart contains undefined Entry: " + entry);
		}
	}

	@Test
	public void addOrderEntryListSuccess() throws CommerceCartModificationException
	{
		// Given
		userService.setCurrentUser(userService.getAnonymousUser()); // setting anonymous customer
		catalogVersionService.setSessionCatalogVersion(CATALOG_ID, CATALOG_VERSION);
		final String welCPAPlatinumReviewCourseSetPrint = "WEL_CPA_PLATINUM_REVIEW_COURSE_SET_PRINT";
		final List<OrderEntryData> productToAdd = Arrays.asList(orderEntryAUD, orderEntryBEC, orderEntryFAR, orderEntryREG);

		// When
		final List<CartModificationData> cartModificationDataList = wileyMultiDimensionalCartFacade.addOrderEntryList(
				productToAdd);

		// Then
		// updating cart
		final CartModel sessionCart = cartService.getSessionCart();

		assertNotNull(cartModificationDataList);
		assertEquals(productToAdd.size(), cartModificationDataList.size());
		assertTrue(cartModificationDataList.stream()
				.anyMatch(modification -> welCPAPlatinumAUDPrint.equals(modification.getEntry().getProduct().getCode())));
		assertTrue(cartModificationDataList.stream()
				.anyMatch(modification -> welCPAPlatinumBECPrint.equals(modification.getEntry().getProduct().getCode())));
		assertTrue(cartModificationDataList.stream()
				.anyMatch(modification -> welCPAPlatinumFARPrint.equals(modification.getEntry().getProduct().getCode())));
		assertTrue(cartModificationDataList.stream()
				.anyMatch(modification -> welCPAPlatinumREGPrint.equals(modification.getEntry().getProduct().getCode())));

		// we expect only ProductSet in our cart.
		final List<AbstractOrderEntryModel> sessionCartEntries = sessionCart.getEntries();
		assertEquals("Expected only product set in cart.", 1, sessionCartEntries.size());

		final AbstractOrderEntryModel orderEntryModel = sessionCartEntries.get(0);
		final ProductModel product = orderEntryModel.getProduct();
		assertEquals("Expected product set in cart.", welCPAPlatinumReviewCourseSetPrint, product.getCode());
	}

	@Test
	public void addOrderEntryListSecondCaseSuccess() throws CommerceCartModificationException
	{
		// Given
		userService.setCurrentUser(userService.getAnonymousUser()); // setting anonymous customer
		catalogVersionService.setSessionCatalogVersion(CATALOG_ID, CATALOG_VERSION);
		final String welCPAPlatinumReviewCourseSetPrint = "WEL_CPA_PLATINUM_REVIEW_COURSE_SET_PRINT";

		// When
		wileyMultiDimensionalCartFacade.addOrderEntryList(Arrays.asList(orderEntryAUD, orderEntryBEC, orderEntryFAR));
		wileyMultiDimensionalCartFacade.addOrderEntryList(Arrays.asList(orderEntryAUD, orderEntryBEC, orderEntryREG));

		// Then
		// updating cart
		final CartModel sessionCart = cartService.getSessionCart();

		final List<AbstractOrderEntryModel> sessionCartEntries = sessionCart.getEntries();
		assertEquals("Expected three products in cart.", 3, sessionCartEntries.size());

		// Expected that our cart contains:
		// AUD part
		final AbstractOrderEntryModel orderEntryModelAUD = sessionCartEntries.stream()
				.filter(entry -> welCPAPlatinumAUDPrint.equals(entry.getProduct().getCode()))
				.findFirst()
				.orElse(null);
		assertNotNull("Expected AUD part in cart.", orderEntryModelAUD);

		// the second one is BEC part
		final AbstractOrderEntryModel orderEntryModelBEC = sessionCartEntries.stream()
				.filter(entry -> welCPAPlatinumBECPrint.equals(entry.getProduct().getCode()))
				.findFirst()
				.orElse(null);
		assertNotNull("Expected BEC part in cart.", orderEntryModelBEC);

		// and the third one is ProductSet
		final AbstractOrderEntryModel orderEntryModelSet = sessionCartEntries.stream()
				.filter(entry -> welCPAPlatinumReviewCourseSetPrint.equals(entry.getProduct().getCode()))
				.findFirst()
				.orElse(null);
		assertNotNull("Expected product set in cart.", orderEntryModelSet);
	}

	@Test
	public void addOrderEntryListThirdCaseSuccess() throws CommerceCartModificationException
	{
		// Given
		userService.setCurrentUser(userService.getAnonymousUser()); // setting anonymous customer
		catalogVersionService.setSessionCatalogVersion(CATALOG_ID, CATALOG_VERSION);
		final String welCPAPlatinumReviewCourseSetPrint = "WEL_CPA_PLATINUM_REVIEW_COURSE_SET_PRINT";

		// When
		wileyMultiDimensionalCartFacade.addOrderEntryList(Arrays.asList(orderEntryAUD, orderEntryBEC, orderEntryFAR));
		wileyMultiDimensionalCartFacade.addOrderEntryList(Arrays.asList(orderEntryAUD, orderEntryBEC, orderEntryREG));
		wileyMultiDimensionalCartFacade.addOrderEntryList(Arrays.asList(orderEntryFAR, orderEntryREG));

		// Then
		// updating cart
		final CartModel sessionCart = cartService.getSessionCart();

		final List<AbstractOrderEntryModel> sessionCartEntries = sessionCart.getEntries();
		assertEquals("Expected only one product in cart.", 1, sessionCartEntries.size());

		final AbstractOrderEntryModel orderEntryModelSet = sessionCartEntries.stream()
				.filter(entry -> welCPAPlatinumReviewCourseSetPrint.equals(entry.getProduct().getCode()))
				.findFirst()
				.orElse(null);
		assertNotNull("Expected product set in cart.", orderEntryModelSet);
	}

	@Test
	public void testMergeCartsSuccess() throws Exception
	{
		// Given
		userService.setCurrentUser(userService.getUserForUID("test@hybris.com"));
		catalogVersionService.setSessionCatalogVersion(CATALOG_ID, CATALOG_VERSION);

		// When
		final CartRestorationData cartRestoration = wileyMultiDimensionalCartFacade.restoreCartAndMerge("123456", "123456789");

		// Then
		final List<CartModificationData> commerceCartModificationList = cartRestoration.getModifications();
		final CartModel mergedCart = commerceCartService.getCartForGuidAndSiteAndUser("123456789",
				baseSiteService.getBaseSiteForUID("wel"), userService.getCurrentUser());
		final List<AbstractOrderEntryModel> entries = mergedCart.getEntries();
		assertEquals("Expected 3 product in cart.", 3, entries.size());
		assertTrue("Expected product part WEL_CPA_PLATINUM_BEC_PRINT in cart.", entries.stream()
				.anyMatch(entry -> "WEL_CPA_PLATINUM_BEC_PRINT".equals(entry.getProduct().getCode())));
		assertTrue("Expected product part WEL_CPA_PLATINUM_FAR_PRINT in cart.", entries.stream()
				.anyMatch(entry -> "WEL_CPA_PLATINUM_FAR_PRINT".equals(entry.getProduct().getCode())));
		assertTrue("Expected product part WEL_CPA_PLATINUM_REVIEW_COURSE_SET_PRINT in cart.", entries.stream()
				.anyMatch(entry -> "WEL_CPA_PLATINUM_REVIEW_COURSE_SET_PRINT".equals(entry.getProduct().getCode())));

		// Expected cart modifications with products from first cart.
		assertEquals("Expected 3 products ware merged in cart.", 3, entries.size());
		assertTrue(commerceCartModificationList.stream()
				.anyMatch(modification -> "WEL_CPA_PLATINUM_AUD_PRINT".equals(modification.getEntry().getProduct().getCode())));
		assertTrue(commerceCartModificationList.stream()
				.anyMatch(modification -> "WEL_CPA_PLATINUM_BEC_PRINT".equals(modification.getEntry().getProduct().getCode())));
		assertTrue(commerceCartModificationList.stream()
				.anyMatch(modification -> "WEL_CPA_PLATINUM_FAR_PRINT".equals(modification.getEntry().getProduct().getCode())));
	}

	@Test
	public void testAddProductWithQuantityGreaterThenOneIfTheProductDidNotExistBefore() throws CommerceCartModificationException
	{
		// Given
		catalogVersionService.setSessionCatalogVersion(CATALOG_ID, CATALOG_VERSION);
		final long quantity = 5;
		orderEntryAUD.setQuantity(quantity);

		assertTrue("Expected that product can have quantity.", wileyCountableProductService.canProductHaveQuantity(
				productService.getProductForCode(orderEntryAUD.getProduct().getCode())));

		// When
		// trying to add product which can have quantity
		wileyMultiDimensionalCartFacade.addOrderEntryList(Arrays.asList(orderEntryAUD));

		// Then
		final CartModel sessionCart = cartService.getSessionCart();

		final List<AbstractOrderEntryModel> sessionCartEntries = sessionCart.getEntries();
		assertEquals("Expected only one product in cart.", 1, sessionCartEntries.size());
		final AbstractOrderEntryModel orderEntryModel = sessionCartEntries.get(0);
		assertEquals("WEL_CPA_PLATINUM_AUD_PRINT", orderEntryModel.getProduct().getCode());
		assertEquals(quantity, orderEntryModel.getQuantity().longValue());
	}

	@Test
	public void testAddProductWithQuantityGreaterThenOneIfTheProductExistedBefore() throws CommerceCartModificationException
	{
		// Given
		catalogVersionService.setSessionCatalogVersion(CATALOG_ID, CATALOG_VERSION);

		assertTrue("Expected that product can have quantity.", wileyCountableProductService.canProductHaveQuantity(
				productService.getProductForCode(orderEntryAUD.getProduct().getCode())));

		// add product to make sure that product will be in cart
		wileyMultiDimensionalCartFacade.addOrderEntryList(Arrays.asList(orderEntryAUD));

		// When
		// trying to add product which can have quantity
		orderEntryAUD.setQuantity((long) 5);
		wileyMultiDimensionalCartFacade.addOrderEntryList(Arrays.asList(orderEntryAUD));

		// Then
		final CartModel sessionCart = cartService.getSessionCart();

		final List<AbstractOrderEntryModel> sessionCartEntries = sessionCart.getEntries();
		assertEquals("Expected only one product in cart.", 1, sessionCartEntries.size());
		final AbstractOrderEntryModel orderEntryModel = sessionCartEntries.get(0);
		assertEquals("WEL_CPA_PLATINUM_AUD_PRINT", orderEntryModel.getProduct().getCode());
		assertEquals(6, orderEntryModel.getQuantity().longValue());
	}

	@Test
	public void shouldFindAndAddRightProductSetIfAnotherSetWithoutVariantsIsConfigured()
			throws ImpExException, CommerceCartModificationException
	{
		// Given
		importCsv(TEST_RESOURCES_FOLDER + "/wrongSet.impex", DEFAULT_ENCODING);

		catalogVersionService.setSessionCatalogVersion(CATALOG_ID, CATALOG_VERSION);


		// When
		wileyMultiDimensionalCartFacade.addOrderEntryList(Arrays.asList(orderEntryAUD, orderEntryBEC, orderEntryFAR,
				orderEntryREG));

		// Then
		final CartModel sessionCart = cartService.getSessionCart();

		final List<AbstractOrderEntryModel> sessionCartEntries = sessionCart.getEntries();
		assertEquals("Expected only one product set in cart.", 1, sessionCartEntries.size());
		final AbstractOrderEntryModel orderEntryModel = sessionCartEntries.get(0);
		assertEquals("WEL_CPA_PLATINUM_REVIEW_COURSE_SET_PRINT", orderEntryModel.getProduct().getCode());
	}

	@Test
	public void shouldNotModifySessionCartIfThereIsAGivenCart() throws CommerceCartModificationException
	{
		// Given
		userService.setCurrentUser(userService.getAnonymousUser());
		catalogVersionService.setSessionCatalogVersion(CATALOG_ID, CATALOG_VERSION);
		final List<OrderEntryData> productToAdd = Arrays.asList(orderEntryAUD);
		final CartModel cartModel = wileyCartFactory.createCart();
		final CartModel sessionCart = cartService.getSessionCart();

		// When
		wileyMultiDimensionalCartFacade.addOrderEntryList(cartModel.getCode(), productToAdd);

		// Then
		assertEquals("Session cart should not be modified.", 0, sessionCart.getEntries().size());

		final List<AbstractOrderEntryModel> cartEntries = cartModel.getEntries();
		assertEquals("Expected only product set in cart.", 1, cartEntries.size());

		final AbstractOrderEntryModel orderEntryModel = cartEntries.get(0);
		final ProductModel product = orderEntryModel.getProduct();
		assertEquals("Expected product set in cart.", welCPAPlatinumAUDPrint, product.getCode());
	}

	@Test
	public void clearSessionCart()
	{
		//given
		final Collection<CartModel> carts = userService.getUserForUID("test@hybris.com").getCarts();
		final CartModel cartModel = carts.stream().findAny().get();
		final AbstractOrderEntryModel testEntry1 = cartModel.getEntries().get(0);
		final AbstractOrderEntryModel testEntry2 = cartModel.getEntries().get(1);
		final AbstractOrderEntryModel testEntry3 = cartModel.getEntries().get(2);
		//session cart with entries
		cartService.setSessionCart(cartModel);

		//when
		wileyMultiDimensionalCartFacade.clearSessionCart();

		//then
		assertTrue("Session cart must be empty", cartService.getSessionCart().getEntries().isEmpty());

	}

	@Test
	public void changeProductTypeInCart() throws CommerceCartModificationException
	{
		// Given
		orderEntryAUD.setApplyStudentDiscount(true);
		orderEntryAUD.setPartnerId(PARTNER_ID);
		userService.setCurrentUser(userService.getAnonymousUser()); // setting anonymous customer
		catalogVersionService.setSessionCatalogVersion(CATALOG_ID, CATALOG_VERSION);
		wileyMultiDimensionalCartFacade.addOrderEntryList(Arrays.asList(orderEntryAUD, orderEntryBEC));
		// When
		boolean succeed = wileyMultiDimensionalCartFacade.changeInCartProductType(
				welCPAPlatinumAUDEbook, 0, 1);

		// Then
		assertTrue(succeed);

		// updating cart
		final CartModel sessionCart = cartService.getSessionCart();
		// we expect only ProductSet in our cart.
		final List<AbstractOrderEntryModel> sessionCartEntries = sessionCart.getEntries();
		assertEquals("Expected two products in cart.", 2, sessionCartEntries.size());

		final AbstractOrderEntryModel orderEntryModel = sessionCartEntries.get(0);
		final ProductModel product = orderEntryModel.getProduct();
		assertEquals("Expected product in cart.", welCPAPlatinumAUDEbook, product.getCode());
		assertTrue(sessionCart.getAppliedCouponCodes().contains(STUDENT_COUPON_CODE));
		assertEquals(PARTNER_ID, sessionCart.getWileyPartner().getUid());
	}
}
