package com.wiley.facades.as.address.impl;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.address.data.AddressVerificationResult;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.address.AddressVerificationDecision;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.exceptions.ExternalSystemAccessException;
import com.wiley.core.integration.address.dto.AddressDto;
import com.wiley.core.integration.address.service.WileyasAddressVerificationService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyasAddressVerificationFacadeImplUnitTest {

	private WileyasAddressVerificationFacadeImpl wileyasAddressVerificationFacade;
	private WileyasAddressVerificationService wileyasAddressVerificationService;
	private Converter<AddressData, AddressDto> addressDtoReverseConverter;

	@Before
	public void setUp()
	{
		wileyasAddressVerificationService = mock(WileyasAddressVerificationService.class);
		addressDtoReverseConverter = mock(Converter.class);

		wileyasAddressVerificationFacade = new WileyasAddressVerificationFacadeImpl();
		wileyasAddressVerificationFacade.setWileyasAddressVerificationService(wileyasAddressVerificationService);
		wileyasAddressVerificationFacade.setAddressDtoReverseConverter(addressDtoReverseConverter);
	}

	@Test
	public void testVerifyAddressExternalSystemAccessException() {
		//given
		when(wileyasAddressVerificationService.verifyAddress(Mockito.any(AddressDto.class)))
			.thenThrow(new ExternalSystemAccessException("External system is not available"));

		//when
		AddressVerificationResult result = wileyasAddressVerificationFacade.verifyAddressData(new AddressData());

		//then
		Assert.assertTrue(result.isSkipAddressDoctor());
		Assert.assertEquals(AddressVerificationDecision.UNKNOWN, result.getDecision());
		Assert.assertTrue(result.getSuggestedAddresses().isEmpty());
	}
}
