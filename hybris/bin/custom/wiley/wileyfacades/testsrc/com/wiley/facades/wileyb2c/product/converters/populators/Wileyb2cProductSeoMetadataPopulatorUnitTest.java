package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.wiley.restriction.WileyProductRestrictionService;


/**
 * Created by Raman_Hancharou on 6/28/2017.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cProductSeoMetadataPopulatorUnitTest
{

	public static final String TEST_SEO_TITLE_TAG = "testSeoTitleTag";
	public static final String TEST_SEO_DESCRIPTION_TAG = "testSeoDescriptionTag";
	@Mock
	private WileyProductRestrictionService wileyProductRestrictionServiceMock;

	@Mock
	private ProductModel productModelMock;
	@Mock
	private ModelService modelService;

	@InjectMocks
	private Wileyb2cProductSeoMetadataPopulator wileyb2cProductSeoMetadataPopulator;


	@Test
	public void populateWhenFieldsNotDefined() throws Exception
	{
		//Given
		when(modelService.getAttributeValue(productModelMock, ProductModel.SEOTITLETAG)).thenReturn(null);
		when(modelService.getAttributeValue(productModelMock, ProductModel.SEODESCRIPTIONTAG)).thenReturn(null);
		ProductData productData = new ProductData();
		//When
		wileyb2cProductSeoMetadataPopulator.populate(productModelMock, productData);
		//Then
		assertNull(productData.getSeoTitleTag());
		assertNull(productData.getSeoDescriptionTag());
	}

	@Test
	public void populateWhenFieldsDefined() throws Exception
	{
		//Given
		when(modelService.getAttributeValue(productModelMock, ProductModel.SEOTITLETAG)).thenReturn(TEST_SEO_TITLE_TAG);
		when(modelService.getAttributeValue(productModelMock, ProductModel.SEODESCRIPTIONTAG)).thenReturn(
				TEST_SEO_DESCRIPTION_TAG);
		ProductData productData = new ProductData();
		//When
		wileyb2cProductSeoMetadataPopulator.populate(productModelMock, productData);
		//Then
		assertEquals(TEST_SEO_TITLE_TAG, productData.getSeoTitleTag());
		assertEquals(TEST_SEO_DESCRIPTION_TAG, productData.getSeoDescriptionTag());
	}
}
