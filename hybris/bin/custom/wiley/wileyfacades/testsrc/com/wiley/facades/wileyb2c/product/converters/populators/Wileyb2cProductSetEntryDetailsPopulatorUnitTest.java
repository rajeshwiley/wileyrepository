package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.ConfigurablePopulator;
import de.hybris.platform.core.model.product.ProductModel;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.wiley.restriction.WileyProductRestrictionService;
import com.wiley.facades.wileyb2c.product.data.ProductSetData;


/**
 * Created by Uladzimir_Barouski on 5/5/2017.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cProductSetEntryDetailsPopulatorUnitTest
{
	@Mock
	private WileyProductRestrictionService wileyb2cProductRestrictionServiceMock;
	@Mock
	private ConfigurablePopulator<ProductModel, ProductData, ProductOption> wileyb2cProductConfiguredPopulatorMock;
	@Mock
	private ProductModel productModelMock;

	@InjectMocks
	private Wileyb2cProductSetEntryDetailsPopulator wileyb2cProductSetEntryDetailsPopulator;

	@Test
	public void populate() throws Exception
	{
		//Given
		ProductSetData productSetData = mock(ProductSetData.class);
		//When
		wileyb2cProductSetEntryDetailsPopulator.populate(productModelMock, productSetData);
		//Then
		verify(wileyb2cProductConfiguredPopulatorMock, times(1)).populate(eq(productModelMock), any(ProductData.class),
				eq(Wileyb2cProductSetEntryDetailsPopulator.PRODUCT_SET_OPTIONS));
		verify(productSetData, times(1)).setProductSet(any(ProductData.class));
	}

}