package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.references.ProductReferenceService;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.product.WileyProductService;
import com.wiley.core.wileyb2c.product.Wileyb2cCourseProductService;
import com.wiley.facades.populators.WileyProductPricePopulator;
import com.wiley.facades.wileyb2c.product.data.CourseBundleData;
import com.wiley.facades.wileyb2c.product.data.CourseBundlesData;
import com.wiley.facades.wileyb2c.product.data.InstantAccessPurchaseOptionData;
import com.wiley.facades.wileyb2c.product.data.WileyPlusCourseInfoData;
import com.wiley.facades.wileyplus.cart.activation.dto.CartActivationRequestDto;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;


/**
 * Created by Uladzimir_Barouski on 8/25/2016.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cCourseBundlePopulatorUnitTest
{
	public static final String COURSE_ISBN = "11111";
	public static final String COURSE_CODE = "course_code";

	@Mock
	private WileyProductService productService;

	@Mock
	private ProductReferenceService productReferenceServiceMock;

	@Mock
	private Converter<ProductModel, CourseBundleData> wileyb2cCourseBundleEntryConverterMock;

	@Mock
	private WileyProductPricePopulator wileyb2cProductPricePopulatorMock;

	@Mock
	private Wileyb2cCourseProductService wileyb2cCourseProductServiceMock;

	@InjectMocks
	private Wileyb2cCourseBundlePopulator wileyb2cCourseBundlePopulator;

	ProductModel productSet1;
	ProductModel productSet2;

	private CourseBundleData courseBundleData1;
	private CourseBundleData courseBundleData2;


	@Mock(answer = Answers.RETURNS_DEEP_STUBS)
	private CartActivationRequestDto cartActivationRequestDtoMock;

	@Mock(answer = Answers.RETURNS_DEEP_STUBS)
	private WileyPlusCourseInfoData wileyPlusCourseInfoDataMock;

	@Before
	public void setUp() throws Exception
	{
		productSet1 = new ProductModel();
		productSet2 = new ProductModel();

		courseBundleData1 = new CourseBundleData();
		courseBundleData1.setMostPopular(false);
		courseBundleData2 = new CourseBundleData();
		courseBundleData2.setMostPopular(true);
		ProductModel courseProduct = new ProductModel();
		courseProduct.setCode(COURSE_CODE);
		ProductModel eprofProduct = new ProductModel();


		when(cartActivationRequestDtoMock.getCourse().getIsbn()).thenReturn(COURSE_ISBN);
		when(productService.getProductForIsbn(COURSE_ISBN)).thenReturn(courseProduct);
		when(wileyb2cCourseProductServiceMock.getReferencedEprofProduct(courseProduct)).thenReturn(eprofProduct);
		when(wileyb2cCourseProductServiceMock.getReferencedBundleProducts(eprofProduct, courseProduct))
				.thenReturn(Arrays.asList(productSet1, productSet2));
		doNothing().when(wileyb2cProductPricePopulatorMock).populate(any(ProductModel.class), any(ProductData.class));
	}

	@Test
	public void testPopulate() throws Exception
	{

		when(wileyb2cCourseBundleEntryConverterMock.convert(productSet1)).thenReturn(courseBundleData1);
		when(wileyb2cCourseBundleEntryConverterMock.convert(productSet2)).thenReturn(courseBundleData2);
		when(wileyb2cCourseProductServiceMock.getReferencedBundleProducts(any(), any()))
				.thenReturn(Arrays.asList(productSet1, productSet2));
		when(wileyPlusCourseInfoDataMock.getPurchaseOptions().getInstantAccessPurchaseOption()).thenReturn(
				new InstantAccessPurchaseOptionData());

		wileyb2cCourseBundlePopulator.populate(cartActivationRequestDtoMock, wileyPlusCourseInfoDataMock);
		CourseBundlesData bundlesData =
				wileyPlusCourseInfoDataMock.getPurchaseOptions().getInstantAccessPurchaseOption().getCourseBundles();
		List<CourseBundleData> bundlesList = bundlesData.getBundles();
		assertEquals(COURSE_CODE, bundlesData.getCourseCode());
		assertNotNull(bundlesList);
		assertEquals(2, bundlesList.size());
		assertTrue(bundlesList.get(0).getMostPopular());
		//check that budles were sorted correctly
		assertEquals(courseBundleData2, bundlesList.get(0));
	}

	@Test
	public void testPopulateForOneBundleWrong() throws Exception
	{
		when(wileyb2cCourseBundleEntryConverterMock.convert(productSet1)).thenReturn(courseBundleData1);
		when(wileyb2cCourseBundleEntryConverterMock.convert(productSet2)).thenThrow(new ConversionException(""));
		when(wileyPlusCourseInfoDataMock.getPurchaseOptions().getInstantAccessPurchaseOption()).thenReturn(
				new InstantAccessPurchaseOptionData());

		wileyb2cCourseBundlePopulator.populate(cartActivationRequestDtoMock, wileyPlusCourseInfoDataMock);
		List<CourseBundleData> bundlesList =
				wileyPlusCourseInfoDataMock.getPurchaseOptions().getInstantAccessPurchaseOption().getCourseBundles().getBundles();
		assertNotNull(bundlesList);
		assertEquals(1, bundlesList.size());
		assertTrue(bundlesList.get(0).getMostPopular());
		assertEquals(courseBundleData1, bundlesList.get(0));
	}
}