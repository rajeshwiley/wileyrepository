package com.wiley.facades.wileyb2c.order.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.wiley.core.wileyb2b.order.Wileyb2bSendEmailWithOrderDetailsService;
import com.wiley.core.wileycom.customer.service.WileycomCustomerAccountService;
import com.wiley.facades.wileyb2b.order.impl.Wileyb2bSendEmailWithOrderDetailsFacadeImpl;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Unit test for {@link Wileyb2bSendEmailWithOrderDetailsFacadeImpl}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2bSendEmailWithOrderDetailsFacadeImplUnitTest
{

	private static final String TEST_EMAIL = "testemail@example.com";
	private static final String TEST_ORDER_CODE = "1000000";
	private static final boolean TEST_SERVICE_EMAIL_SENT_RESULT = true;


	@InjectMocks
	private Wileyb2bSendEmailWithOrderDetailsFacadeImpl wileyb2bSendEmailWithOrderDetailsFacade;

	@Mock
	private Wileyb2bSendEmailWithOrderDetailsService wileyb2bSendEmailWithOrderDetailsServiceMock;

	@Mock
	private WileycomCustomerAccountService wileyb2bCustomerAccountServiceMock;

	@Mock
	private BaseStoreService baseStoreServiceMock;

	@Mock
	private UserService userServiceMock;

	@Mock
	private BaseStoreModel baseStoreMock;

	@Mock
	private B2BCustomerModel b2BCustomerMock;

	@Mock
	private OrderModel orderMock;

	@Rule
	public final ExpectedException exception = ExpectedException.none();

	@Before
	public void setup()
	{
		when(baseStoreServiceMock.getCurrentBaseStore()).thenReturn(baseStoreMock);
		when(userServiceMock.getCurrentUser()).thenReturn(b2BCustomerMock);
	}


	@Test
	public void shouldThrowIllegalArgumentExceptionOnNullOrderCode()
	{
		//Given
		exception.expect(IllegalArgumentException.class);

		//When
		wileyb2bSendEmailWithOrderDetailsFacade.sendEmailWithOrderDetails(null, null);
	}

	@Test
	public void shouldThrowIllegalArgumentExceptionOnEmptyOrderCode()
	{
		//Given
		exception.expect(IllegalArgumentException.class);

		//When
		wileyb2bSendEmailWithOrderDetailsFacade.sendEmailWithOrderDetails(StringUtils.EMPTY, null);
	}


	@Test
	public void shouldThrowIllegalArgumentExceptionOnNullEmail()
	{
		//Given
		exception.expect(IllegalArgumentException.class);

		//When
		wileyb2bSendEmailWithOrderDetailsFacade.sendEmailWithOrderDetails(TEST_ORDER_CODE, null);
	}

	@Test
	public void shouldThrowIllegalArgumentExceptionOnEmptyEmail()
	{
		//Given
		exception.expect(IllegalArgumentException.class);

		//When
		wileyb2bSendEmailWithOrderDetailsFacade.sendEmailWithOrderDetails(TEST_ORDER_CODE, StringUtils.EMPTY);
	}


	@Test
	public void shouldThrowUnknownIdentifierExceptionDueOrderNotFound()
	{
		//Given
		when(wileyb2bCustomerAccountServiceMock.getOrderForCode(TEST_ORDER_CODE, baseStoreMock))
				.thenThrow(ModelNotFoundException.class);
		exception.expect(UnknownIdentifierException.class);

		//When
		wileyb2bSendEmailWithOrderDetailsFacade.sendEmailWithOrderDetails(TEST_ORDER_CODE, TEST_EMAIL);
	}

	@Test
	public void shouldReturnServiceResult()
	{
		//Given
		when(wileyb2bCustomerAccountServiceMock.getOrderForCode(TEST_ORDER_CODE, baseStoreMock)).thenReturn(
				orderMock);
		when(wileyb2bSendEmailWithOrderDetailsServiceMock.sendEmailWithOrderDetails(b2BCustomerMock, orderMock, TEST_EMAIL))
				.thenReturn(TEST_SERVICE_EMAIL_SENT_RESULT);

		//When
		boolean isEmailSent = wileyb2bSendEmailWithOrderDetailsFacade.sendEmailWithOrderDetails(TEST_ORDER_CODE, TEST_EMAIL);

		//Then
		verify(wileyb2bSendEmailWithOrderDetailsServiceMock).sendEmailWithOrderDetails(b2BCustomerMock, orderMock, TEST_EMAIL);
		Assert.assertTrue("Facade should return boolean result from underlying service",
				TEST_SERVICE_EMAIL_SENT_RESULT == isEmailSent);
	}

}
