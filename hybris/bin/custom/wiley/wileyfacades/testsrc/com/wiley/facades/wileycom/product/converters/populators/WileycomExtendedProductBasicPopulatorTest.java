package com.wiley.facades.wileycom.product.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import java.util.Date;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.enums.WileyProductSubtypeEnum;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;


/**
 * Default unit test for {@link WileycomExtendedProductBasicPopulator}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
@Ignore("This tests depends on obsolete attribute")
public class WileycomExtendedProductBasicPopulatorTest
{

	@InjectMocks
	private WileycomExtendedProductBasicPopulator wileycomExtendedProductBasicPopulator;

	// Test data
	@Mock
	private ProductModel productModelMock;

	@Test
	public void shouldPopulateExpectedFields()
	{
		// Given
		final ProductData productData = new ProductData();
		final String expectedAuthors = "Author, Author, Author";
		final Date expectedDateImprint = new Date();
		final String expectedIsbn = "978-1452102276";
		final WileyProductSubtypeEnum expectedSubtype = WileyProductSubtypeEnum.COURSE;

		// TODO : [ECSC-20075] commented out code which uses obsolete attribute. Should be deleted
//		when(productModelMock.getAuthors()).thenReturn(expectedAuthors);
		when(productModelMock.getIsbn()).thenReturn(expectedIsbn);
		when(productModelMock.getSubtype()).thenReturn(expectedSubtype);

		// When
		wileycomExtendedProductBasicPopulator.populate(productModelMock, productData);

		// Then
		assertThat(productData, allOf(
				hasProperty("authors", equalTo(expectedAuthors)),
				hasProperty("subtype", equalTo(expectedSubtype.getCode())),
				hasProperty("isbn", equalTo(expectedIsbn))));
	}
}
