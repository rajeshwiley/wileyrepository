package com.wiley.facades.wileyb2c.product.converters.populators.helper;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.AuthorInfoModel;


/**
 * Created by Uladzimir_Barouski on 5/31/2017.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cAuthorPopulatorHelperUnitTest
{
	private static final String AUTHORS = "author 1 name, author 2 name";
	private static final String AUTHOR_1_NAME = "author 1 name";
	private static final String AUTHOR_2_NAME = "author 2 name";

	@Mock
	private ProductModel productModel;
	@Mock
	private AuthorInfoModel author1, author2;
	@Mock
	private Wileyb2cProductCollectionAttributesHelper wileyb2cProductCollectionAttributesHelper;
	@InjectMocks
	private Wileyb2cAuthorPopulatorHelper wileyb2cAuthorPopulatorHelper;

	@Test
	public void getAuthorsWithoutRoles() throws Exception
	{
		//Given
		when(author1.getName()).thenReturn(AUTHOR_1_NAME);
		when(author2.getName()).thenReturn(AUTHOR_2_NAME);
		when(wileyb2cProductCollectionAttributesHelper.getProductAttribute(productModel, ProductModel.AUTHORINFOS)).thenReturn(
				Arrays.asList(author1, author2));
		//When
		String authors = wileyb2cAuthorPopulatorHelper.getAuthorsWithoutRoles(productModel);
		//Then
		assertNotNull(authors);
		assertEquals(AUTHORS, authors);
	}

}