package com.wiley.facades.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Arrays;
import java.util.Collections;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyPartnerCompanyModel;
import com.wiley.facades.partner.WileyPartnerCompanyData;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyPartnerCompanyPopulatorTest
{
	@InjectMocks
	private WileyPartnerCompanyPopulator testedInstance = new WileyPartnerCompanyPopulator();

	@Mock
	private Converter<CategoryModel, CategoryData> categoryConverter;
	@Mock
	private WileyPartnerCompanyModel partnerModel;
	@Mock
	private CategoryModel partnerCategoryModel;

	private WileyPartnerCompanyData partnerData = new WileyPartnerCompanyData();
	private CategoryData partnerCategoryData = new CategoryData();

	@Test
	public void shouldSetPartnerCategories()
	{
		//Given
		when(partnerModel.getPartnerCategories()).thenReturn(Collections.singleton(partnerCategoryModel));
		when(categoryConverter.convert(partnerCategoryModel)).thenReturn(partnerCategoryData);
		//When
		testedInstance.populate(partnerModel, partnerData);
		//Then
		assertEquals(Arrays.asList(partnerCategoryData), partnerData.getPartnerCategories());
	}

}