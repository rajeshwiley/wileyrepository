package com.wiley.facades.wileyb2c.product.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.ProductReferenceData;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.jalo.order.price.PriceFactory;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.i18n.L10NService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.site.BaseSiteService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;
import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.i18n.WileyCommonI18NService;
import com.wiley.core.i18n.WileyCountryService;
import com.wiley.core.servicelayer.i18n.WileycomI18NService;
import com.wiley.core.wileyb2c.jalo.Wileyb2cEurope1PriceFactory;
import com.wiley.facades.product.impl.DefaultWileyProductFacade;
import com.wiley.facades.wileyb2c.product.Wileyb2cProductFacade;

import static com.wiley.facades.constants.WileyFacadesConstants.CAROUSEL_MAX_NUMBER_OF_PRODUCTS_DEFAULT_VALUE;
import static com.wiley.facades.constants.WileyFacadesConstants.CAROUSEL_MAX_NUMBER_OF_PRODUCTS_MESSAGE_CODE;


/**
 * Integration test suite for {@link DefaultWileyProductFacade}. Covers county fallback logic from
 * {@link Wileyb2cEurope1PriceFactory}
 *
 * Created by Maksim Kozich on 4/21/2017.
 */
@IntegrationTest
public class Wileyb2cProductFacadeIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	public static final Logger LOG = LoggerFactory.getLogger(Wileyb2cProductFacadeIntegrationTest.class);

	private static final String WCOM_PRODUCT = "WCOM_PRODUCT";
	private static final String WCOM_PRODUCT2 = "WCOM_PRODUCT2";
	private static final String BASE_SITE = "wileyb2c";
	private static final String CATALOG_ID = "wileyProductCatalog";
	private static final String CATALOG_VERSION = "Online";

	private static final List<ProductOption> PRODUCT_OPTIONS = Arrays.asList(ProductOption.BASIC, ProductOption.URL);

	private static final List<ProductReferenceTypeEnum> PRODUCT_REFERENCE_TYPES = Collections.singletonList(
			ProductReferenceTypeEnum.RELATED);

	public static final double USD_U1_PRICE = 40.11;
	public static final double USD_U2_PRICE = 40.22;
	public static final double USD_U3_PRICE = 40.33;
	public static final double USD_GLOBAL_PRICE = 40.99;

	public static final double EUR_E1_PRICE = 41.11;
	public static final double EUR_E2_PRICE = 41.22;
	public static final double EUR_E3_PRICE = 41.33;
	public static final double EUR_GLOBAL_PRICE = 41.99;

	public static final String U1_COUNTRY_CODE = "AD";
	public static final String U2_COUNTRY_CODE = "AE";
	public static final String U3_COUNTRY_CODE = "AF";
	public static final String U4_COUNTRY_CODE = "AG";
	public static final String U5_COUNTRY_CODE = "AI";
	public static final String U6_COUNTRY_CODE = "AL";
	public static final String U7_COUNTRY_CODE = "AM";
	public static final String E1_COUNTRY_CODE = "AN";
	public static final String E2_COUNTRY_CODE = "AO";
	public static final String E3_COUNTRY_CODE = "AQ";
	public static final String E4_COUNTRY_CODE = "AR";
	public static final String E5_COUNTRY_CODE = "AS";
	public static final String E6_COUNTRY_CODE = "AT";
	public static final String E7_COUNTRY_CODE = "AU";

	public static final String AU_COUNTRY_CODE = "AU";
	public static final String DE_COUNTRY_CODE = "DE";
	public static final String US_COUNTRY_CODE = "US";

	public static final String WCOM_BASE_PRODUCT_SET = "WCOM_BASE_PRODUCT_SET";
	public static final String WCOM_BASE_PRODUCT_SET_NAME = "baseProductSetName";
	public static final String WCOM_VARIANT_SET = "WCOM_VARIANT_SET";
	public static final String WCOM_VARIANT_SET_NAME = "variantSetName1";
	public static final String WCOM_VARIANT_SET_PO_NAME = "Hardcover";
	public static final String WCOM_VARIANT_SET_AUTHORS = "a1_en_US (a1_role_en_US), a2_en_US (a2_role_en_US)";
	public static final Date WCOM_VARIANT_SET_PUB_DATE = (new GregorianCalendar(2017, 5, 1)).getTime();

	// countable field from impex is overwritten by WileyPurchaseOptionProductCountablePrepareInterceptor
	public static final Boolean WCOM_VARIANT_SET_COUNTABLE = true;
	public static final String WCOM_VARIANT_SET_COMPONENT_3 = "WCOM_VARIANT_SET_COMPONENT3";
	public static final String WCOM_VARIANT_SET_COMPONENT = "WCOM_VARIANT_SET_COMPONENT";
	public static final String WCOM_VARIANT_SET_COMPONENT_NAME = "variantSetComponentName1";
	public static final String WCOM_VARIANT_SET_COMPONENT_PO_NAME = "Journal";
	public static final String WCOM_VARIANT_SET_COMPONENT_AUTHORS = "a3_en_US (a3_role_en_US), a4_en_US (a4_role_en_US)";
	public static final Date WCOM_VARIANT_SET_COMPONENT_PUB_DATE = (new GregorianCalendar(2017, 6, 1)).getTime();

	// countable field from impex is overwritten by WileyPurchaseOptionProductCountablePrepareInterceptor
	public static final Boolean WCOM_VARIANT_SET_COMPONENT_COUNTABLE = true;
	public static final String REGION_CODE = "<REGION_CODE>";

	public static final String WCOM_PRODUCT_EBOOK = "WCOM_PRODUCT_EBOOK";
	public static final String RELATED_WCOM_PRODUCT2_CODE = "WCOM_PRODUCT2_HARDCOVER";
	public static final String RELATED_WCOM_PRODUCT2_NAME = "WCOM_PRODUCT2 name";
	public static final String RELATED_WCOM_PRODUCT2_AUTHORS = "ra1_en_AU (ra1_role_en_AU), ra2_en_AU (ra2_role_en_AU)";
	public static final double RELATED_WCOM_PRODUCT2_PRICE = 22.22;

	public static final String RELATED_WCOM_PRODUCT3_CODE = "WCOM_PRODUCT3_PAPERBACK";
	public static final String RELATED_WCOM_PRODUCT3_NAME = "WCOM_PRODUCT3 name";
	public static final double RELATED_WCOM_PRODUCT3_PRICE = 33.33;
	public static final String RELATED_WCOM_PRODUCT3_AUTHORS = "ra3_en_AU (ra3_role_en_AU), ra4_en_AU (ra4_role_en_AU)";

	public static final String WCOM_PRODUCT4_EBOOK = "WCOM_PRODUCT4_EBOOK";
	public static final String RELATED_WCOM_PRODUCT5_CODE = "WCOM_PRODUCT5_EBOOK";
	public static final String RELATED_WCOM_PRODUCT5_NAME = "WCOM_PRODUCT5 name";
	public static final String RELATED_WCOM_PRODUCT5_AUTHORS = "ra5_en_AU (ra5_role_en_AU), ra6_en_AU (ra6_role_en_AU)";
	public static final double RELATED_WCOM_PRODUCT5_PRICE = 44.44;

	public static final String RELATED_WCOM_PRODUCT6_CODE = "WCOM_PRODUCT6_EBOOK";
	public static final String RELATED_WCOM_PRODUCT6_NAME = "WCOM_PRODUCT6 name";
	public static final String RELATED_WCOM_PRODUCT6_AUTHORS = "ra7_en_AU (ra7_role_en_AU), ra8_en_AU (ra8_role_en_AU)";
	public static final double RELATED_WCOM_PRODUCT6_PRICE = 55.55;

	public static final String RELATED_WCOM_PRODUCT5_HARDCOVER_CODE = "WCOM_PRODUCT5_HARDCOVER";
	public static final double RELATED_WCOM_PRODUCT5_HARDCOVER_PRICE = 66.66;

	public static final String RELATED_WCOM_PRODUCT6_HARDCOVER_CODE = "WCOM_PRODUCT6_HARDCOVER";
	public static final double RELATED_WCOM_PRODUCT6_HARDCOVER_PRICE = 77.77;

	public static final int EXPECTED_RELATED_PRODUCTS_NUMBER = 4;
	private static final int PRODUCT_REFERENCES_LIMIT = 5;

	@Resource
	private WileycomI18NService wileycomI18NService;

	@Resource
	private WileyCountryService wileyCountryService;

	@Resource
	private Wileyb2cProductFacade wileyb2cProductFacade;

	@Resource
	private Wileyb2cEurope1PriceFactory wileyb2cEurope1PriceFactory;

	@Resource
	private BaseSiteService baseSiteService;

	@Resource
	private CatalogVersionService catalogVersionService;

	@Resource
	private L10NService l10nService;

	@Resource
	private SessionService sessionService;

	@Resource
	private ConfigurationService configurationService;

	@Resource
	private WileyCommonI18NService wileyCommonI18NService;

	private PriceFactory sessionPriceFactoryBefore;

	/**
	 * Sets up.
	 *
	 * @throws Exception
	 * 		the exception
	 */
	@Before
	public void setUp() throws Exception
	{
		replacePriceFactory();
		baseSiteService.setCurrentBaseSite(baseSiteService.getBaseSiteForUID(BASE_SITE), true);
		catalogVersionService.setSessionCatalogVersion(CATALOG_ID, CATALOG_VERSION);
	}

	protected void replacePriceFactory()
	{
		sessionPriceFactoryBefore = jaloSession.getPriceFactory();
		jaloSession.setPriceFactory(wileyb2cEurope1PriceFactory);
	}

	@After
	public void tearDown()
	{
		restorePriceFactory();
	}

	protected void restorePriceFactory()
	{
		jaloSession.setPriceFactory(sessionPriceFactoryBefore);
	}

	@Test
	public void testFallbackCountrySpecificPrice() throws Exception
	{
		importCsv("/wileyfacades/test/Wileyb2cProductFacadeIntegrationTest/products-prices.impex", DEFAULT_ENCODING);

		testPriceForTheCountry(U1_COUNTRY_CODE, WCOM_PRODUCT, USD_U1_PRICE);
		testPriceForTheCountry(U2_COUNTRY_CODE, WCOM_PRODUCT, USD_U2_PRICE);
		testPriceForTheCountry(U3_COUNTRY_CODE, WCOM_PRODUCT, USD_U3_PRICE);
		testPriceForTheCountry(U4_COUNTRY_CODE, WCOM_PRODUCT, USD_U2_PRICE);
		testPriceForTheCountry(U5_COUNTRY_CODE, WCOM_PRODUCT, USD_U3_PRICE);
		testPriceForTheCountry(U6_COUNTRY_CODE, WCOM_PRODUCT, USD_GLOBAL_PRICE);
		testPriceForTheCountry(U7_COUNTRY_CODE, WCOM_PRODUCT, USD_GLOBAL_PRICE);


		testPriceForTheCountry(E1_COUNTRY_CODE, WCOM_PRODUCT, EUR_E1_PRICE);
		testPriceForTheCountry(E2_COUNTRY_CODE, WCOM_PRODUCT, EUR_E2_PRICE);
		testPriceForTheCountry(E3_COUNTRY_CODE, WCOM_PRODUCT, EUR_E3_PRICE);
		testPriceForTheCountry(E4_COUNTRY_CODE, WCOM_PRODUCT, EUR_E2_PRICE);
		testPriceForTheCountry(E5_COUNTRY_CODE, WCOM_PRODUCT, EUR_E3_PRICE);
		testPriceForTheCountry(E6_COUNTRY_CODE, WCOM_PRODUCT, EUR_GLOBAL_PRICE);
		testPriceForTheCountry(E7_COUNTRY_CODE, WCOM_PRODUCT, EUR_GLOBAL_PRICE);
	}

	private void testPriceForTheCountry(final String countryCode, final String productCode, final double expectedPrice)
	{
		LOG.info("Test price for country: " + countryCode + ". Expected price: " + expectedPrice);
		//given
		final CountryModel country = wileyCountryService.findCountryByCode(countryCode);
		wileycomI18NService.setCurrentCountry(country);

		//when
		final List<ProductOption> options = Arrays.asList(ProductOption.PRICE);
		final ProductData productData = wileyb2cProductFacade.getProductForCodeAndOptions(productCode, options);

		//then
		assertEquals(wileyCommonI18NService.getDefaultCurrency(country).getIsocode(), productData.getPrice().getCurrencyIso());
		assertEquals(BigDecimal.valueOf(expectedPrice), productData.getPrice().getValue());
	}

	@Test
	public void testTaxDisclaimer() throws Exception
	{
		importCsv("/wileyfacades/test/Wileyb2cProductFacadeIntegrationTest/disclaimer.impex",
				DEFAULT_ENCODING);
		// if price is specified, disclaimer should be populated only for AU and EU cart region.
		testTaxDisclaimerForAU(WCOM_PRODUCT);
		testTaxDisclaimerForDE(WCOM_PRODUCT);
		testTaxDisclaimerForTheCountryIsNull(US_COUNTRY_CODE, WCOM_PRODUCT);

		// if price is not specified, disclaimer should not be populated.
		testTaxDisclaimerForTheCountryIsNull(AU_COUNTRY_CODE, WCOM_PRODUCT2);
		testTaxDisclaimerForTheCountryIsNull(DE_COUNTRY_CODE, WCOM_PRODUCT2);
		testTaxDisclaimerForTheCountryIsNull(US_COUNTRY_CODE, WCOM_PRODUCT2);
	}

	@Test
	public void testFilterVisibleReferenceProducts() throws Exception
	{
		//given
		final List<ProductReferenceData> productReferences = prepareTestDataForProductSetsReferences(false);

		//when
		final List<ProductReferenceData> filteredProductReferences = wileyb2cProductFacade
				.filterVisibleReferenceProducts(productReferences, false);

		//than
		assertNotNull(filteredProductReferences);
		assertEquals(1, filteredProductReferences.size());
		assertEquals(WCOM_VARIANT_SET_COMPONENT_3, filteredProductReferences.get(0).getTarget().getCode());
		assertEquals(Boolean.TRUE, filteredProductReferences.get(0).getTarget().getShowPdpUrl());
	}

	@Test
	public void testFilterVisibleReferenceProductsDespiteRestrictions() throws Exception
	{
		//given
		final List<ProductReferenceData> productReferences = prepareTestDataForProductSetsReferences(true);

		//when
		final List<ProductReferenceData> filteredProductReferences = wileyb2cProductFacade
				.filterVisibleReferenceProducts(productReferences, true);

		//than
		assertNotNull(filteredProductReferences);
		assertEquals(3, filteredProductReferences.size());
		assertEquals(WCOM_VARIANT_SET_COMPONENT, filteredProductReferences.get(0).getTarget().getCode());
		assertEquals(Boolean.FALSE, filteredProductReferences.get(0).getTarget().getShowPdpUrl());
	}

	@Test
	public void testGetProductReferencesForCode() throws Exception
	{
		//given
		importCsv("/wileyfacades/test/Wileyb2cProductFacadeIntegrationTest/related-product.impex", DEFAULT_ENCODING);

		final CountryModel country = wileyCountryService.findCountryByCode(AU_COUNTRY_CODE);
		wileycomI18NService.setCurrentCountry(country);

		//when
		final List<ProductReferenceData> productReferences = wileyb2cProductFacade.getProductReferencesForCodes(
				Lists.newArrayList(WCOM_PRODUCT_EBOOK, WCOM_PRODUCT4_EBOOK), PRODUCT_REFERENCE_TYPES, false,
				PRODUCT_REFERENCES_LIMIT);

		//then
		assertNotNull(productReferences);
		assertEquals(EXPECTED_RELATED_PRODUCTS_NUMBER, productReferences.size());

		final String currencyIso = wileyCommonI18NService.getDefaultCurrency(country).getIsocode();
		testRelatedProductData(productReferences, RELATED_WCOM_PRODUCT2_CODE,
				RELATED_WCOM_PRODUCT2_NAME, RELATED_WCOM_PRODUCT2_AUTHORS, RELATED_WCOM_PRODUCT2_PRICE, currencyIso);
		testRelatedProductData(productReferences, RELATED_WCOM_PRODUCT3_CODE,
				RELATED_WCOM_PRODUCT3_NAME, RELATED_WCOM_PRODUCT3_AUTHORS, RELATED_WCOM_PRODUCT3_PRICE, currencyIso);
		testRelatedProductData(productReferences, RELATED_WCOM_PRODUCT5_CODE, RELATED_WCOM_PRODUCT5_NAME,
				RELATED_WCOM_PRODUCT5_AUTHORS, RELATED_WCOM_PRODUCT5_PRICE, currencyIso);
		testRelatedProductData(productReferences, RELATED_WCOM_PRODUCT6_CODE, RELATED_WCOM_PRODUCT6_NAME,
				RELATED_WCOM_PRODUCT6_AUTHORS, RELATED_WCOM_PRODUCT6_PRICE, currencyIso);
	}

	@Test
	public void testProductReferencesOverLimitForCode() throws Exception
	{
		//given
		importCsv("/wileyfacades/test/Wileyb2cProductFacadeIntegrationTest/related-product.impex",
				DEFAULT_ENCODING);
		importCsv("/wileyfacades/test/Wileyb2cProductFacadeIntegrationTest/related-product-references-over-limit.impex",
				DEFAULT_ENCODING);
		final CountryModel country = wileyCountryService.findCountryByCode(AU_COUNTRY_CODE);
		wileycomI18NService.setCurrentCountry(country);

		//when
		final List<ProductReferenceData> productReferences = wileyb2cProductFacade.getProductReferencesForCodes(
				Lists.newArrayList(WCOM_PRODUCT_EBOOK, WCOM_PRODUCT4_EBOOK), PRODUCT_REFERENCE_TYPES, false,
				PRODUCT_REFERENCES_LIMIT);

		//then
		assertNotNull(productReferences);
		final Integer maximumNumberOfProducts = configurationService.getConfiguration().getInteger(
				CAROUSEL_MAX_NUMBER_OF_PRODUCTS_MESSAGE_CODE, CAROUSEL_MAX_NUMBER_OF_PRODUCTS_DEFAULT_VALUE);
		assertEquals(maximumNumberOfProducts, Integer.valueOf(productReferences.size()));

		final String currencyIso = wileyCommonI18NService.getDefaultCurrency(country).getIsocode();
		testRelatedProductData(productReferences, RELATED_WCOM_PRODUCT2_CODE,
				RELATED_WCOM_PRODUCT2_NAME, RELATED_WCOM_PRODUCT2_AUTHORS, RELATED_WCOM_PRODUCT2_PRICE, currencyIso);
		testRelatedProductData(productReferences, RELATED_WCOM_PRODUCT3_CODE,
				RELATED_WCOM_PRODUCT3_NAME, RELATED_WCOM_PRODUCT3_AUTHORS, RELATED_WCOM_PRODUCT3_PRICE, currencyIso);
		testRelatedProductData(productReferences, RELATED_WCOM_PRODUCT5_CODE, RELATED_WCOM_PRODUCT5_NAME,
				RELATED_WCOM_PRODUCT5_AUTHORS, RELATED_WCOM_PRODUCT5_PRICE, currencyIso);
		testRelatedProductData(productReferences, RELATED_WCOM_PRODUCT6_CODE, RELATED_WCOM_PRODUCT6_NAME,
				RELATED_WCOM_PRODUCT6_AUTHORS, RELATED_WCOM_PRODUCT6_PRICE, currencyIso);
		testRelatedProductData(productReferences, RELATED_WCOM_PRODUCT5_HARDCOVER_CODE, RELATED_WCOM_PRODUCT5_NAME,
				RELATED_WCOM_PRODUCT5_AUTHORS, RELATED_WCOM_PRODUCT5_HARDCOVER_PRICE, currencyIso);
	}

	protected void testRelatedProductData(final List<ProductReferenceData> productReferences, final String code,
			final String name, final String authors, final Double price, final String currencyIso)
	{
		final Optional<ProductReferenceData> productReferenceDataOptional = productReferences.stream()
				.filter(r -> code.equals(r.getTarget().getCode())).findFirst();
		assertTrue(productReferenceDataOptional.isPresent());
		final ProductReferenceData productReferenceData = productReferenceDataOptional.get();

		assertEquals(ProductReferenceTypeEnum.RELATED, productReferenceData.getReferenceType());
		final ProductData productData = productReferenceData.getTarget();
		assertNotNull(productData);
		assertEquals(code, productData.getCode());
		assertNotNull(productData.getUrl());
		assertTrue(CollectionUtils.isNotEmpty(productData.getImages()));
		assertEquals(name, productData.getName());
		assertEquals(authors, productData.getAuthors());
		assertEquals(BigDecimal.valueOf(price), productData.getPrice().getValue());
		assertEquals(currencyIso, productData.getPrice().getCurrencyIso());
		// make sure disclaimer is also populated
		assertNotNull(productData.getPrice().getOptionalTaxShortMessage());
		assertNotNull(productData.getPrice().getOptionalTaxTooltip());
	}

	private List<ProductReferenceData> prepareTestDataForProductSetsReferences(final boolean despiteRestriction)
			throws ImpExException
	{
		importCsv("/wileyfacades/test/Wileyb2cProductFacadeIntegrationTest/product-sets.impex", DEFAULT_ENCODING);

		final CountryModel country = wileyCountryService.findCountryByCode(US_COUNTRY_CODE);
		wileycomI18NService.setCurrentCountry(country);

		return wileyb2cProductFacade.getProductReferencesForCodes(Lists.newArrayList(WCOM_VARIANT_SET),
				Lists.newArrayList(ProductReferenceTypeEnum.PRODUCT_SET_COMPONENT), despiteRestriction, 10);
	}

	private void testTaxDisclaimerForAU(final String productCode)
	{
		LOG.info("Test price disclaimer for country: " + AU_COUNTRY_CODE + ".");

		//given
		final CountryModel country = wileyCountryService.findCountryByCode(AU_COUNTRY_CODE);
		wileycomI18NService.setCurrentCountry(country);

		//when
		final List<ProductOption> options = Collections.singletonList(ProductOption.PRICE);
		final ProductData productData = wileyb2cProductFacade.getProductForCodeAndOptions(productCode,
				options);

		//then
		assertNotNull(productData.getPrice());
		final String expectedShortMessage = l10nService.getLocalizedString(
				getMsgForRegion(WileyCoreConstants.TAX_SHORT_MESSAGE_CODE, country.getLegacyCart().getCode()));
		final String expectedTooltip = l10nService.getLocalizedString(
				getMsgForRegion(WileyCoreConstants.TAX_TOOLTIP_CODE, country.getLegacyCart().getCode()));
		assertEquals(expectedShortMessage, productData.getPrice().getOptionalTaxShortMessage());
		assertEquals(expectedTooltip, productData.getPrice().getOptionalTaxTooltip());
	}

	private void testTaxDisclaimerForDE(final String productCode)
	{
		LOG.info("Test price disclaimer for country: " + DE_COUNTRY_CODE + ".");

		//given
		final CountryModel country = wileyCountryService.findCountryByCode(DE_COUNTRY_CODE);
		wileycomI18NService.setCurrentCountry(country);

		//when
		final List<ProductOption> options = Collections.singletonList(ProductOption.PRICE);
		final ProductData productData = wileyb2cProductFacade.getProductForCodeAndOptions(productCode, options);

		//then
		assertNotNull(productData.getPrice());

		final String expectedShortMessage = l10nService.getLocalizedString(
				getMsgForRegion(WileyCoreConstants.TAX_SHORT_MESSAGE_CODE, country.getLegacyCart().getCode()));
		final String expectedTooltip = l10nService.getLocalizedString(
				getMsgForRegion(WileyCoreConstants.TAX_TOOLTIP_CODE, country.getLegacyCart().getCode()));

		assertEquals(expectedShortMessage, productData.getPrice().getOptionalTaxShortMessage());
		assertEquals(expectedTooltip, productData.getPrice().getOptionalTaxTooltip());
	}

	private void testTaxDisclaimerForTheCountryIsNull(final String countryCode, final String productCode)
	{
		LOG.info("Test price disclaimer is not provided for country: " + countryCode + ".");

		//given
		final CountryModel country = wileyCountryService.findCountryByCode(countryCode);
		wileycomI18NService.setCurrentCountry(country);

		//when
		final List<ProductOption> options = Collections.singletonList(ProductOption.PRICE);
		final ProductData productData = wileyb2cProductFacade.getProductForCodeAndOptions(productCode, options);

		//then
		if (productData.getPrice() != null)
		{
			assertNull(productData.getPrice().getOptionalTaxShortMessage());
			assertNull(productData.getPrice().getOptionalTaxTooltip());
		}
	}

	@Test
	public void testAddToLegacyCartActionFieldsPopulatedForSetComponentAndSet() throws Exception
	{
		//given
		importCsv("/wileyfacades/test/Wileyb2cProductFacadeIntegrationTest/product-sets.impex", DEFAULT_ENCODING);

		final CountryModel country = wileyCountryService.findCountryByCode(US_COUNTRY_CODE);
		wileycomI18NService.setCurrentCountry(country);

		//when
		final List<ProductOption> options = Arrays.asList(ProductOption.DETAILS, ProductOption.PURCHASE_OPTION,
				ProductOption.COUNTABLE, ProductOption.AUTHOR_INFOS, ProductOption.PRODUCT_SETS);
		final ProductData productData = wileyb2cProductFacade.getProductForCodeAndOptions(WCOM_VARIANT_SET_COMPONENT, options);

		//then
		assertNotNull(productData);
		assertTrue(CollectionUtils.isNotEmpty(productData.getProductSets()));
		assertEquals(1, productData.getProductSets().size());

		testProductDataFields(productData, WCOM_VARIANT_SET_COMPONENT, WCOM_VARIANT_SET_COMPONENT_NAME,
				WCOM_VARIANT_SET_COMPONENT_PO_NAME, WCOM_VARIANT_SET_COMPONENT_AUTHORS,
				WCOM_VARIANT_SET_COMPONENT_PUB_DATE, WCOM_VARIANT_SET_COMPONENT_COUNTABLE);

		final ProductData productSetData = productData.getProductSets().get(0).getProductSet();
		testProductDataFields(productSetData, WCOM_VARIANT_SET, WCOM_BASE_PRODUCT_SET_NAME,
				WCOM_VARIANT_SET_PO_NAME, WCOM_VARIANT_SET_AUTHORS,
				WCOM_VARIANT_SET_PUB_DATE, WCOM_VARIANT_SET_COUNTABLE);
	}

	private void testProductDataFields(final ProductData productData, final String code, final String name,
			final String purchaseOptionName, final String authors, final Date publicationDate, final Boolean countable)
	{
		assertNotNull(productData);
		assertEquals(code, productData.getCode());
		assertEquals(name, productData.getName());
		assertEquals(purchaseOptionName, productData.getPurchaseOptionName());
		assertEquals(authors, productData.getAuthors());
		assertEquals(publicationDate, productData.getPublicationDate());
		assertEquals(countable, productData.getCountable());
	}

	private String getMsgForRegion(final String taxMsg, final String legacyCartCode)
	{
		return l10nService.getLocalizedString(taxMsg.replace(REGION_CODE, legacyCartCode));
	}
}
