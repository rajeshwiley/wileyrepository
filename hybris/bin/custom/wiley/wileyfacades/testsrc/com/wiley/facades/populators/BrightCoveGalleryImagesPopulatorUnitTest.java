package com.wiley.facades.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ImageDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.BrightCoveMediaContainerModel;
import com.wiley.facades.populators.bright.cover.WileyBrightCoverHelper;
import com.wiley.facades.product.data.BrightCoveVideoData;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class BrightCoveGalleryImagesPopulatorUnitTest
{
	private static final String VIDEO_ID = "12345";
	private static final String VIDEO_FORMAT = "brightCoveVideo";
	private static final String PLAYER_ID_VALUE = "222222";
	private static final String PLAYER_KEY_VALUE = "test_player_key";
	private static final String ACCOUNT_ID_VALUE = "33333";
	public static final int IMAGES_COUNT = 1;

	@InjectMocks
	private BrightCoveGalleryImagesPopulator<ProductModel, ProductData> testingInstance =
			new BrightCoveGalleryImagesPopulator<ProductModel, ProductData>();

	@Mock
	private WileyBrightCoverHelper wileyBrightCoverHelper;
	@Mock
	private ProductModel mockProductModel;
	@Mock
	private MediaContainerModel mockMediaContainerModel;
	@Mock
	private BrightCoveMediaContainerModel mockBrightCoveMediaContainerModel;
	@Mock
	private ModelService mockModelService;
	@Mock
	private BaseSiteModel mockCurrentBaseSite;
	@Mock
	private Configuration mockConfiguration;

	private List<MediaContainerModel> galleryImages = new ArrayList<>();
	private ProductData productData = new ProductData();

	@Before
	public void setUp()
	{
		initBrightCoveMediaContainerModel();
		initGalleryImages();
		when(wileyBrightCoverHelper.createBrightCoveVideoDataForGallery(VIDEO_ID, IMAGES_COUNT)).thenReturn(
				createBrightCoveVideoData());
		when(mockModelService.getAttributeValue(mockProductModel, ProductModel.GALLERYIMAGES))
				.thenReturn(galleryImages);
	}

	private BrightCoveVideoData createBrightCoveVideoData()
	{
		final BrightCoveVideoData brightCoveVideoData = new BrightCoveVideoData();
		brightCoveVideoData.setVideoId(VIDEO_ID);
		brightCoveVideoData.setFormat(VIDEO_FORMAT);
		brightCoveVideoData.setPlayerId(PLAYER_ID_VALUE);
		brightCoveVideoData.setPlayerKey(PLAYER_KEY_VALUE);
		brightCoveVideoData.setImageType(ImageDataType.GALLERY);
		brightCoveVideoData.setGalleryIndex(IMAGES_COUNT);
		brightCoveVideoData.setAccountId(ACCOUNT_ID_VALUE);
		return brightCoveVideoData;
	}

	@Test
	public void shouldAddVideoDataForBrightCoveContainer()
	{
		testingInstance.populate(mockProductModel, productData);

		assertEquals(IMAGES_COUNT, productData.getImages().size());
		ImageData imageData = productData.getImages().iterator().next();
		assertTrue(imageData instanceof BrightCoveVideoData);
		assertEquals(IMAGES_COUNT, imageData.getGalleryIndex().intValue());
	}

	@Test
	public void shouldPopulateBrightCoveVideoData()
	{
		testingInstance.populate(mockProductModel, productData);
		BrightCoveVideoData videoData = (BrightCoveVideoData) productData.getImages().iterator().next();

		assertEquals(VIDEO_FORMAT, videoData.getFormat());
		assertEquals(ImageDataType.GALLERY, videoData.getImageType());
		assertEquals(IMAGES_COUNT, videoData.getGalleryIndex().intValue());
		assertEquals(VIDEO_ID, videoData.getVideoId());
		assertEquals(PLAYER_ID_VALUE, videoData.getPlayerId());
		assertEquals(PLAYER_KEY_VALUE, videoData.getPlayerKey());
		assertEquals(ACCOUNT_ID_VALUE, videoData.getAccountId());
	}

	private void initBrightCoveMediaContainerModel()
	{
		when(mockBrightCoveMediaContainerModel.getVideoId()).thenReturn(VIDEO_ID);
	}

	private void initGalleryImages()
	{
		galleryImages.add(mockMediaContainerModel);
		galleryImages.add(mockBrightCoveMediaContainerModel);
	}

}
