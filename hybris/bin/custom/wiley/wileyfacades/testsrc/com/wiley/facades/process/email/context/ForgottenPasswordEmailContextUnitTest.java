package com.wiley.facades.process.email.context;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.urlresolver.SiteBaseUrlResolutionService;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;

import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.AdditionalMatchers.not;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.contains;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ForgottenPasswordEmailContextUnitTest
{
	private static final String REDIRECT_TYPE = "redirectType";

	@Mock
	private SiteBaseUrlResolutionService siteBaseUrlResolutionServiceMock;
	@Mock
	private BaseSiteModel baseSiteModelMock;
	@Spy
	@InjectMocks
	private ForgottenPasswordEmailContext testInstance = new ForgottenPasswordEmailContext();

	@Before
	public void setUp() throws Exception
	{
		doReturn(baseSiteModelMock).when(testInstance).getBaseSite();
	}

	@Test
	public void testGetResetPasswordUrlVerifyRedirectTypeAddedToUrlIfItsNotBlank() throws Exception
	{
		//Given
		testInstance.setRedirectType(REDIRECT_TYPE);
		doReturn(UUID.randomUUID().toString()).when(testInstance).getURLEncodedToken();
		//Testing
		testInstance.getResetPasswordUrl(false);
		//Verifying
		siteBaseUrlResolutionServiceMock.getWebsiteUrlForSite(
				eq(baseSiteModelMock), anyString(), anyBoolean(), anyString(), contains(REDIRECT_TYPE));
	}

	@Test
	public void testGetResetPasswordUrlVerifyRedirectTypeNotAddedToUrlIfItsBlank() throws Exception
	{
		//Given
		testInstance.setRedirectType(null);
		doReturn(UUID.randomUUID().toString()).when(testInstance).getURLEncodedToken();
		//Testing
		testInstance.getResetPasswordUrl(false);
		//Verifying
		siteBaseUrlResolutionServiceMock.getWebsiteUrlForSite(
				eq(baseSiteModelMock), anyString(), anyBoolean(), anyString(),
				not(contains(ForgottenPasswordEmailContext.REDIRECT_TYPE_URL_PARAMETER_NAME)));

	}
}