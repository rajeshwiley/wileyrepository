package com.wiley.facades.wileyb2c.search.converters.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Created by Uladzimir_Barouski on 3/13/2017.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cSearchResultProductAuthorsPopulatorUnitTest
{
	private static final String AUTHORS = "author 1 name, author 2 name";
	private static final String AUTHORS_KEY = "authors";
	private static final String AUTHOR_1_NAME = "author 1 name";
	private static final String AUTHOR_2_NAME = "author 2 name";

	@InjectMocks
	private Wileyb2cSearchResultProductAuthorsPopulator wileyb2cSearchResultProductAuthorsPopulator;

	@Mock(answer = Answers.RETURNS_DEEP_STUBS)
	private SearchResultValueData searchResultValueData;
	@Mock
	private Map<String, Object> values;

	@Before
	public void setUp() throws Exception
	{

	}

	@Test
	public void populate() throws Exception
	{
		//Given
		when(searchResultValueData.getValues()).thenReturn(values);
		when(MapUtils.isNotEmpty(searchResultValueData.getValues())).thenReturn(false);
		wileyb2cSearchResultProductAuthorsPopulator.setAuthorProperty(AUTHORS_KEY);
		when(searchResultValueData.getValues().get(AUTHORS_KEY)).thenReturn(Arrays.asList(AUTHOR_1_NAME, AUTHOR_2_NAME));
		ProductData productData = new ProductData();
		//When
		wileyb2cSearchResultProductAuthorsPopulator.populate(searchResultValueData, productData);

		//Then
		assertNotNull(productData.getAuthorsWithoutRoles());
		assertEquals(AUTHORS, productData.getAuthorsWithoutRoles());
	}

}