package com.wiley.facades.wel.voucher.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.user.UserService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.facades.welags.voucher.exception.InvalidVoucherException;


/**
 * Integration test for {@link WelCouponFacadeImpl}.
 */
@IntegrationTest
public class WelVoucherFacadeImplIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{

	@Resource
	private WelCouponFacadeImpl welCouponFacade;

	@Resource
	private CommerceCartService commerceCartService;

	@Resource
	private UserService userService;

	@Resource
	private CartService cartService;

	private static final String APPLIED_COUPON_CODE = "MYCOUPON";
	private static final String INPUT_COUPON_CODE = "myCoupon";
	private static final String UNEXISTING_COUPON_CODE = "coupon153135115";
	private static final String TEST_CART_UID = "testCart1";
	private static final String TEST_USER_UID = "test@hybris.com";

	@Before
	public void setUp() throws Exception
	{
		importCsv("/wileyfacades/test/WelVoucherFacadeImplIntegrationTest/cart.impex", DEFAULT_ENCODING);
		importCsv("/wileyfacades/test/WelVoucherFacadeImplIntegrationTest/voucher.impex", DEFAULT_ENCODING);
	}
	
	@Test
	public void shouldApplyCoupon() throws Exception
	{
		// Given
		final UserModel user = userService.getUserForUID(TEST_USER_UID);
		final CartModel cart = commerceCartService.getCartForCodeAndUser(TEST_CART_UID, user);

		userService.setCurrentUser(user);
		cartService.setSessionCart(cart);	
	
		// Testing
		welCouponFacade.applyVoucher(INPUT_COUPON_CODE);
		
		// Verify
		final Collection<String> appliedCouponCodes = cart.getAppliedCouponCodes();
		assertNotNull(appliedCouponCodes);
		assertEquals(1, appliedCouponCodes.size());
		assertEquals(APPLIED_COUPON_CODE, appliedCouponCodes.iterator().next());
	}


	@Test
	public void testInvalidCouponCode() throws Exception
	{
		// Given
		final UserModel user = userService.getUserForUID(TEST_USER_UID);
		final CartModel cart = commerceCartService.getCartForCodeAndUser(TEST_CART_UID, user);

		userService.setCurrentUser(user);
		cartService.setSessionCart(cart);

		// Testing
		try {
			welCouponFacade.applyVoucher(UNEXISTING_COUPON_CODE);
		} catch (InvalidVoucherException ex) {
			assertTrue(ex.getMessage().contains(UNEXISTING_COUPON_CODE.toUpperCase()));
		}

		// Verify
		final Collection<String> appliedCouponCodes = cart.getAppliedCouponCodes();
		assertNull(appliedCouponCodes);
	}
}