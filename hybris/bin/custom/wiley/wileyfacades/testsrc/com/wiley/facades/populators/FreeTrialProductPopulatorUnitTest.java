package com.wiley.facades.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.converters.populator.ProductDescriptionPopulator;
import de.hybris.platform.commercefacades.product.converters.populator.ProductGalleryImagesPopulator;
import de.hybris.platform.commercefacades.product.converters.populator.ProductUrlPopulator;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyFreeTrialProductModel;
import com.wiley.core.model.WileyFreeTrialVariantProductModel;
import com.wiley.facades.product.data.FreeTrialData;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;


/**
 * Created by Uladzimir_Barouski on 4/1/2016.
 */

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class FreeTrialProductPopulatorUnitTest
{
	private static final String SOME_VIDEO_LINK = "some video link";
	private static final String SOME_SLOGAN = "some slogan";

	private static final String PRODUCT_CODE1 = "WEL_CFA_FREETRIAL_2017_LEVEL1";
	private static final String PRODUCT_NAME1 = "Level I";
	private static final String PRODUCT_URL1 = "proURL/WEL_CFA_FREETRIAL_2017_LEVEL1";

	private static final String PRODUCT_CODE2 = "WEL_CFA_FREETRIAL_2017_LEVEL2";
	private static final String PRODUCT_NAME2 = "Level II";
	private static final String PRODUCT_URL2 = "proURL/WEL_CFA_FREETRIAL_2017_LEVEL2";

	private static final String PRODUCT_CODE3 = "WEL_CFA_FREETRIAL_2017_LEVEL3";
	private static final String PRODUCT_NAME3 = "Level III";
	private static final String PRODUCT_URL3 = "proURL/WEL_CFA_FREETRIAL_2017_LEVEL3";


	@Mock
	private ProductGalleryImagesPopulator productGalleryImagesPopulatorMock;

	@Mock
	private ProductDescriptionPopulator productDescriptionPopulatorMock;

	@InjectMocks
	private FreeTrialProductPopulator freeTrialProductPopulator;

	@Mock
	private WileyFreeTrialProductModel wileyFreeTrialProductModel;

	@Mock
	private WileyFreeTrialVariantProductModel wileyFreeTrialVariantModelOne;

	@Mock
	private WileyFreeTrialVariantProductModel wileyFreeTrialVariantModelTwo;

	@Mock
	private WileyFreeTrialVariantProductModel wileyFreeTrialVariantModelThree;

	@Mock
	private UrlResolver<ProductModel> productModelUrlResolverMock;

	@Before
	public void setUp()
	{
		when(wileyFreeTrialVariantModelOne.getCode()).thenReturn(PRODUCT_CODE1);
		when(wileyFreeTrialVariantModelOne.getName()).thenReturn(PRODUCT_NAME1);
		when(productModelUrlResolverMock.resolve(wileyFreeTrialVariantModelOne)).thenReturn(PRODUCT_URL1);

		when(wileyFreeTrialVariantModelTwo.getCode()).thenReturn(PRODUCT_CODE2);
		when(wileyFreeTrialVariantModelTwo.getName()).thenReturn(PRODUCT_NAME2);
		when(productModelUrlResolverMock.resolve(wileyFreeTrialVariantModelTwo)).thenReturn(PRODUCT_URL2);

		when(wileyFreeTrialVariantModelThree.getCode()).thenReturn(PRODUCT_CODE3);
		when(wileyFreeTrialVariantModelThree.getName()).thenReturn(PRODUCT_NAME3);
		when(productModelUrlResolverMock.resolve(wileyFreeTrialVariantModelThree)).thenReturn(PRODUCT_URL3);
	}


	@Test
	public void testPopulatedSortedList() throws Exception
	{
		ProductUrlPopulator productUrlPopulator = new ProductUrlPopulator();
		freeTrialProductPopulator.setProductUrlPopulator(productUrlPopulator);
		productUrlPopulator.setProductModelUrlResolver(productModelUrlResolverMock);


		when(wileyFreeTrialProductModel.getVideoLink()).thenReturn(SOME_VIDEO_LINK);
		when(wileyFreeTrialProductModel.getSlogan()).thenReturn(SOME_SLOGAN);
		when(wileyFreeTrialProductModel.getVariants()).thenReturn(Arrays.asList(wileyFreeTrialVariantModelTwo,
				wileyFreeTrialVariantModelThree, wileyFreeTrialVariantModelOne));

		doNothing().when(productGalleryImagesPopulatorMock).populate(eq(wileyFreeTrialProductModel), any(ProductData.class));
		doNothing().when(productDescriptionPopulatorMock).populate(eq(wileyFreeTrialProductModel), any(ProductData.class));

		FreeTrialData freeTrialData = new FreeTrialData();
		freeTrialProductPopulator.populate(wileyFreeTrialProductModel, freeTrialData);

		// When
		final List<ProductData> resultFreeTrialList = freeTrialData.getFreeTrialList();

		// Then
		assertEquals(SOME_VIDEO_LINK, freeTrialData.getVideoLink());
		assertEquals(SOME_SLOGAN, freeTrialData.getSlogan());
		assertEquals(3, resultFreeTrialList.size());

		assertEquals("The resulting list should have been ordered in alphabetical order!",
				PRODUCT_NAME1, resultFreeTrialList.get(0).getName());

		assertEquals("The resulting list should have been ordered in alphabetical order!",
				PRODUCT_NAME2, resultFreeTrialList.get(1).getName());

		assertEquals("The resulting list should have been ordered in alphabetical order!",
				PRODUCT_NAME3, resultFreeTrialList.get(2).getName());
	}

}