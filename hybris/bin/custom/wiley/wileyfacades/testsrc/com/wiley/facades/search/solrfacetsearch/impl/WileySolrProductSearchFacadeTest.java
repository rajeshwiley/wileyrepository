package com.wiley.facades.search.solrfacetsearch.impl;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileySolrProductSearchFacadeTest
{
	private WileySolrProductSearchFacade wileyb2cProductSearchFacade;

	@Before
	public void setUp()
	{
		this.wileyb2cProductSearchFacade = new WileySolrProductSearchFacade();
		wileyb2cProductSearchFacade.setIsbnStrategyPattern("^([0-9\\u2013-]{17}){1}$");
	}

	@Test
	public void testCleanupDashesFromSearchIsbn()
	{
		assertEquals("1211111111141", wileyb2cProductSearchFacade.cleanupDashesFromSearchIsbn("121-1-111-11114-1"));
		assertEquals("1211111111141|relevance",
				wileyb2cProductSearchFacade.cleanupDashesFromSearchIsbn("121-1-111-11114-1|relevance"));
		assertEquals("1211111111141|relevance",
				wileyb2cProductSearchFacade.cleanupDashesFromSearchIsbn("-12111-1-1111141-|relevance"));
		assertEquals("121-1-111-11114-188|relevance",
				wileyb2cProductSearchFacade.cleanupDashesFromSearchIsbn("121-1-111-11114-188|relevance"));
		assertEquals("121-1-111-11114-1 OR 121-1-111-11114-2 AND 121-1-111-11114-3", wileyb2cProductSearchFacade
				.cleanupDashesFromSearchIsbn("121-1-111-11114-1 OR 121-1-111-11114-2 AND 121-1-111-11114-3"));
		assertEquals("Halliday-Kiley|relevance",
				wileyb2cProductSearchFacade.cleanupDashesFromSearchIsbn("Halliday-Kiley|relevance"));
	}
}
