package com.wiley.facades.i18n;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.data.RegionData;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;

import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.i18n.WileyCommonI18NService;
import com.wiley.facades.i18n.impl.WileyI18NFacadeImpl;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyI18NFacadeImplTest
{
	private static final String USA_ISOCODE = "US";
	private static final String ALABAMA_NAME = "Alabama";

	private final RegionData alabama = new RegionData();

	@Mock
	private RegionModel alabamaModel;

	@Mock
	private BaseStoreModel baseStoreModel;

	@Mock
	private CountryModel usCountryModel;

	@Mock
	private CommonI18NService commonI18NService;

	@Mock
	private Converter<RegionModel, RegionData> regionConverter;

	@Mock
	private BaseStoreService baseStoreService;

	@Mock
	private WileyCommonI18NService wileyCommonI18NService;

	private List<RegionModel> regionModels = Collections.singletonList(alabamaModel);

	@InjectMocks
	private final WileyI18NFacadeImpl testWileyI18NFacade = new WileyI18NFacadeImpl();

	@Before
	public void setUp() throws Exception
	{
		testWileyI18NFacade.setRegionConverter(regionConverter);

		given(commonI18NService.getCountry(USA_ISOCODE)).willReturn(usCountryModel);
		given(usCountryModel.getRegions()).willReturn(Collections.singletonList(alabamaModel));

		given(alabamaModel.getName()).willReturn(ALABAMA_NAME);
		given(regionConverter.convert(alabamaModel)).willReturn(alabama);

		given(baseStoreService.getCurrentBaseStore()).willReturn(baseStoreModel);
	}

	@Test
	public void testGetRegionsForCountryIsoIfCountryIsAllowed()
	{
		given(wileyCommonI18NService.getConfigurableRegionsForCurrentBaseStore(usCountryModel, baseStoreModel))
				.willReturn(regionModels);

		List<RegionData> regions = testWileyI18NFacade.getRegionsForCountryIso(USA_ISOCODE);

		assertEquals("Expected list with one region", 1, regions.size());
	}

	@Test
	public void testGetRegionsForCountryIsoIfCountryIsNotAllowed()
	{
		given(wileyCommonI18NService.getConfigurableRegionsForCurrentBaseStore(usCountryModel, baseStoreModel))
				.willReturn(Collections.emptyList());

		List<RegionData> regions = testWileyI18NFacade.getRegionsForCountryIso(USA_ISOCODE);

		assertEquals("Expected list with zero regions", 0, regions.size());
	}
}