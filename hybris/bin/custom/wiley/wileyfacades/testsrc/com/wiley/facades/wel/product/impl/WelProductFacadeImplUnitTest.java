package com.wiley.facades.wel.product.impl;

import com.wiley.facades.product.node.ProductNode;
import com.wiley.facades.product.node.ProductNodeValue;
import com.wiley.facades.product.node.ProductRootNode;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.ProductFeatureModel;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.converter.ConfigurablePopulator;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.FeatureData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.cms.CMSProductAttributeModel;
import com.wiley.core.product.WileyProductComparisonService;
import com.wiley.core.product.WileyProductService;
import com.wiley.core.wiley.classification.WileyClassificationService;
import com.wiley.core.wiley.restriction.WileyProductRestrictionService;
import com.wiley.facades.product.data.ProductComparisonData;
import com.wiley.facades.product.impl.DefaultWileyProductFacade;


/**
 * Unit test for {@link DefaultWileyProductFacade}
 *
 * Created by Raman_Hancharou on 1/26/2016.
 */

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WelProductFacadeImplUnitTest
{
	private static final String BASE_PRODUCT = "baseProduct";
	private static final String ANOTHER_BASE_PRODUCT = "anotherBaseProduct";
	private static final double MIN_PRICE_VALUE = 5;
	private static final double PRICE_VALUE = 10;
	private static final double PRICE_PART_1A = 11;
	private static final double PRICE_PART_1B = 12;
	private static final double PRICE_PART_1SET = 13;
	private static final double PRICE_PART_2A = 21;
	private static final double PRICE_PART_2B = 22;
	private static final double PRICE_PART_2SET = 23;
	private static final double DEFAULT_PART_PRICE = 2.2;
	private static final String PRODUCT_1 = "product1";
	private static final String PRODUCT_2 = "product2";
	private static final List<ProductOption> PRODUCT_OPTIONS = Arrays.asList(ProductOption.BASIC, ProductOption.CLASSIFICATION);
	private static final int MAX_NUMBER_OF_PRODUCTS = 6;
	private static final String FEATURE_NAME_1 = "feature1";
	private static final String FEATURE_NAME_2 = "feature2";
	private static final String FEATURE_NAME_3 = "feature3";
	private static final String FEATURE_NAME_4 = "feature4";
	private static final String FEATURE_CODE_1 = "featureCode1";
	private static final String FEATURE_CODE_2 = "featureCode2";
	private static final String FEATURE_CODE_3 = "featureCode3";
	private static final String FEATURE_CODE_4 = "featureCode4";
	private static final String BASE_PRODUCT_CATEGORY_CODE = "baseProductCategoryCode";
	private static final String VARIANT_VALUE_CATEGORY_CODE = "variantValueCategoryCode";
	private static final String SUPPLEMENT_CATEGORY_CODE = "CATEGORY_SUPPLEMENTS";
	private static final String PRODUCT_CODE_1 = "productCode1";
	private static final String PRODUCT_CODE_2 = "productCode2";
	private static final String PRODUCT_CODE_3 = "productCode3";
	private static final String PRODUCT_CODE_4 = "productCode4";
	private static final String CATEGORY_CODE = "categoryCode";
	private static final String CATEGORY_NAME = "categoryName";
	private static final String PRODUCT_1_NAME = "product1Name";
	private static final String PRODUCT_2_NAME = "product2Name";
	private static final String PRODUCT_3_NAME = "product3Name";

	@InjectMocks
	private WelProductFacadeImpl welProductFacade;

	@Mock
	private WileyProductComparisonService mockProductComparisonService;
	@Mock
	private ProductService mockProductService;
	@Mock
	private WileyProductService mockWileyProductService;
	@Mock
	private CategoryService mockCategoryService;
	@Mock
	private Converter<ProductModel, ProductData> mockProductConverter;
	@Mock
	private ConfigurablePopulator<ProductModel, ProductData, ProductOption> mockConfigurablePopulator;
	@Mock
	private Converter<ClassAttributeAssignmentModel, FeatureData> wileyFeatureFromAssignmentConverter;
	@Mock
	private WileyClassificationService wileyClassificationService;
	@Mock
	private WileyProductRestrictionService mockWileyProductRestrictionService;

	@Mock
	private CategoryModel mockCategoryModel;
	@Mock
	private CategoryModel mockSupplementsCategoryModel;
	@Mock(answer = Answers.RETURNS_DEEP_STUBS)
	private CMSProductAttributeModel productAttribute1, productAttribute2, productAttribute3, productAttribute4;
	@Mock
	private ProductModel mockProductModel1, mockProductModel2, mockProductModel3, mockProductModel4;
	@Mock
	private ProductFeatureModel featureModel1, featureModel2, featureModel3;
	@Mock
	private ProductRootNode rootNodeMock;
	@Mock
	private ProductNode rootProductNodeMock;
	@Mock
	private List<ProductNode> productNodeListMock;
	@Mock
	private ProductNode productNodeMock;
	@Mock
	private List<ProductNode> productNodeChildListMock;
	@Mock
	private ProductNode productChildNodeMock;
	@Mock
	private ProductNodeValue productNodeValueModel;
	@Mock
	private List emptyListMock;
	
	//mocks for getPriceForDefaultSetProduct()
	@Mock
	private ProductNode nodeMockPart1A;
	@Mock
	private ProductNode nodeMockPart1B;
	@Mock
	private ProductNode nodeMockPart1Set;
	@Mock
	private ProductNode nodeMockPart2A;
	@Mock
	private ProductNode nodeMockPart2B;
	@Mock
	private ProductNode nodeMockPart2Set;
	@Mock
	private ProductNodeValue nodeValueMockPart1A;
	@Mock
	private ProductNodeValue nodeValueMockPart1B;
	@Mock
	private ProductNodeValue nodeValueMockPart1Set;
	@Mock
	private ProductNodeValue nodeValueMockPart2A;
	@Mock
	private ProductNodeValue nodeValueMockPart2B;
	@Mock
	private ProductNodeValue nodeValueMockPart2Set;
	@Mock
	private ProductNode nodeMockType1;
	@Mock
	private ProductNode nodeMockType2;

	private ProductData productData1, productData2, productData3;
	private FeatureData feature1, feature2, feature3, feature4;
	private List<FeatureData> featureList1, featureList2, featureList3;

	@Before
	public void setUp()
	{
		welProductFacade.setProductConverter(mockProductConverter);
		initFeatures();
		initProducts();
		initCategories();
		initProductConverter();
		initServices();
		initFeatureAssignmentConverter();
		initNodeTreeForTestGetPriceForDefaultSetProduct();
		doReturn(true).when(emptyListMock).isEmpty();
	}

	private void initCategories()
	{
		when(mockCategoryModel.getAllSubcategories()).thenReturn(Arrays.asList(mockSupplementsCategoryModel));
		when(mockCategoryModel.getCode()).thenReturn(CATEGORY_CODE);
		when(mockCategoryModel.getName()).thenReturn(CATEGORY_NAME);
		when(mockSupplementsCategoryModel.getProducts()).thenReturn(Arrays.asList(mockProductModel3));
		when(mockSupplementsCategoryModel.getCode()).thenReturn(SUPPLEMENT_CATEGORY_CODE);
	}

	private void initFeatures()
	{
		feature1 = new FeatureData();
		feature2 = new FeatureData();
		feature3 = new FeatureData();
		feature4 = new FeatureData();

		feature1.setCode(FEATURE_CODE_1);
		feature2.setCode(FEATURE_CODE_2);
		feature3.setCode(FEATURE_CODE_3);
		feature4.setCode(FEATURE_CODE_4);

		feature1.setName(FEATURE_NAME_1);
		feature2.setName(FEATURE_NAME_2);
		feature3.setName(FEATURE_NAME_3);
		feature4.setName(FEATURE_NAME_4);

		featureList1 = new ArrayList<>();
		featureList2 = new ArrayList<>();
		featureList3 = new ArrayList<>();

		featureList1.add(feature1);
		featureList2.add(feature2);
		featureList2.add(feature3);
		featureList3.add(feature3); //add to check feature list does not contain duplicates
		featureList3.add(feature4);
	}

	private void initProducts()
	{
		when(mockProductModel1.getCode()).thenReturn(PRODUCT_CODE_1);
		when(mockProductModel2.getCode()).thenReturn(PRODUCT_CODE_2);
		when(mockProductModel3.getCode()).thenReturn(PRODUCT_CODE_3);
		when(mockProductModel4.getCode()).thenReturn(PRODUCT_CODE_4);

		productData1 = new ProductData();
		productData2 = new ProductData();
		productData3 = new ProductData();

		productData1.setName(PRODUCT_1_NAME);
		productData2.setName(PRODUCT_2_NAME);
		productData3.setName(PRODUCT_3_NAME);

		productData1.setIncludeFeatures(featureList1);
		productData2.setIncludeFeatures(featureList2);
		productData3.setIncludeFeatures(featureList3);

		productData1.setLevelVariantCategoryCode(CATEGORY_CODE);
		productData2.setLevelVariantCategoryCode(CATEGORY_CODE);
		productData3.setLevelVariantCategoryCode(CATEGORY_CODE);
	}

	private void initProductConverter()
	{
		when(mockProductConverter.convert(mockProductModel1)).thenReturn(productData1);
		when(mockProductConverter.convert(mockProductModel2)).thenReturn(productData2);
		when(mockProductConverter.convert(mockProductModel3)).thenReturn(productData3);
	}

	private void initServices()
	{
		welProductFacade.setProductService(mockProductService);
		when(mockProductComparisonService.getProductsForComparison(mockCategoryModel, MAX_NUMBER_OF_PRODUCTS)).thenReturn(
				Arrays.asList(mockProductModel1, mockProductModel2, mockProductModel3));
		when(mockProductComparisonService.getProductsForComparison(BASE_PRODUCT_CATEGORY_CODE, VARIANT_VALUE_CATEGORY_CODE,
				MAX_NUMBER_OF_PRODUCTS)).thenReturn(Arrays.asList(mockProductModel1, mockProductModel2));

		when(mockProductService.getProductForCode(PRODUCT_CODE_1)).thenReturn(mockProductModel1);
		when(mockProductService.getProductForCode(PRODUCT_CODE_2)).thenReturn(mockProductModel2);
		when(mockProductService.getProductForCode(PRODUCT_CODE_3)).thenReturn(mockProductModel3);

		when(mockWileyProductService.getWileyProductsFromCategories(SUPPLEMENT_CATEGORY_CODE, VARIANT_VALUE_CATEGORY_CODE))
				.thenReturn(Arrays.asList(mockProductModel2, mockProductModel3, mockProductModel4));
		when(mockWileyProductService.filterProducts(eq(Arrays.asList(mockProductModel2, mockProductModel3, mockProductModel4))))
				.thenReturn(
						Arrays.asList(mockProductModel3, mockProductModel4));

		when(mockCategoryService.getCategoryForCode(CATEGORY_CODE)).thenReturn(mockCategoryModel);

		when(mockWileyProductRestrictionService.isVisible(mockProductModel1)).thenReturn(true);
		when(mockWileyProductRestrictionService.isVisible(mockProductModel2)).thenReturn(true);
		when(mockWileyProductRestrictionService.isVisible(mockProductModel3)).thenReturn(true);
		when(mockWileyProductRestrictionService.isVisible(mockProductModel4)).thenReturn(false);
	}

	private void initFeatureAssignmentConverter()
	{
		when(wileyFeatureFromAssignmentConverter.convert(productAttribute1.getAssignment())).thenReturn(feature1);
		when(wileyFeatureFromAssignmentConverter.convert(productAttribute2.getAssignment())).thenReturn(feature2);
		when(wileyFeatureFromAssignmentConverter.convert(productAttribute3.getAssignment())).thenReturn(feature3);
		when(wileyFeatureFromAssignmentConverter.convert(productAttribute4.getAssignment())).thenReturn(feature4);
	}
	
	private void initNodeTreeForTestGetPriceForDefaultSetProduct()
	{
		doReturn(rootProductNodeMock).when(rootNodeMock).getProduct();
		doReturn(Arrays.asList(nodeMockType1, nodeMockType2)).when(rootProductNodeMock).getNodes();
		doReturn(Arrays.asList(nodeMockPart1A, nodeMockPart1B, nodeMockPart1Set)).when(nodeMockType1).getNodes();
		doReturn(Arrays.asList(nodeMockPart2A, nodeMockPart2B, nodeMockPart2Set)).when(nodeMockType2).getNodes();
		setUpNodePrice(nodeMockPart1A, nodeValueMockPart1A, PRICE_PART_1A, false);
		setUpNodePrice(nodeMockPart1B, nodeValueMockPart1B, PRICE_PART_1B, false);
		setUpNodePrice(nodeMockPart1Set, nodeValueMockPart1Set, PRICE_PART_1SET, true);
		setUpNodePrice(nodeMockPart2A, nodeValueMockPart2A, PRICE_PART_2A, false);
		setUpNodePrice(nodeMockPart2B, nodeValueMockPart2B, PRICE_PART_2B, false);
		setUpNodePrice(nodeMockPart2Set, nodeValueMockPart2Set, PRICE_PART_2SET, true);
	}
	
	@Test
	public void testGetPriceForDefaultSetProductWithNullProduct()
	{
		// Given
		doReturn(null).when(rootNodeMock).getProduct();
		
		// Testing
		final Double result = welProductFacade.getPriceForDefaultSetProduct(rootNodeMock);

		// Verify
		assertNull(result);
	}
	
	@Test
	public void testGetPriceForDefaultSetProductForNullNodesList()
	{
		// Given
		doReturn(null).when(rootProductNodeMock).getNodes();
		
		// Testing
		final Double result = welProductFacade.getPriceForDefaultSetProduct(rootNodeMock);

		// Verify
		assertNull(result);
	}
	
	@Test
	public void testGetPriceForDefaultSetProductForEmptyNodesList()
	{
		// Given
		doReturn(emptyListMock).when(rootProductNodeMock).getNodes();
		
		// Testing
		final Double result = welProductFacade.getPriceForDefaultSetProduct(rootNodeMock);

		// Verify
		assertNull(result);
	}

	@Test
	public void testGetPriceForDefaultSetProductWhenChildListIsNull()
	{
		// Given
		doReturn(emptyListMock).when(nodeMockType1).getNodes();

		// Testing
		final Double result = welProductFacade.getPriceForDefaultSetProduct(rootNodeMock);

		// Verify
		assertNull(result);
	}
	
	@Test
	public void testGetPriceForDefaultSetProductWhenDefaultSetPresent()
	{
		// Given
		
		// Testing
		final Double result = welProductFacade.getPriceForDefaultSetProduct(rootNodeMock);
	
		// Verify
		assertEquals(Double.valueOf(PRICE_PART_1SET), result);
	}
	
	@Test
	public void testGetPriceForDefaultSetProductWhenDefaultSetPresentInOneLevelDeepProduct()
	{
		// Given
		doReturn(Arrays.asList(nodeMockPart1A, nodeMockPart1B, nodeMockPart1Set)).when(rootProductNodeMock).getNodes();
		
		// Testing
		final Double result = welProductFacade.getPriceForDefaultSetProduct(rootNodeMock);
	
		// Verify
		assertEquals(Double.valueOf(PRICE_PART_1SET), result);
	}
	
	@Test
	public void testGetPriceForDefaultSetProductWhenNoSetPresentInOneLevelDeepProduct()
	{
		// Given
		doReturn(Arrays.asList(nodeMockPart1B, nodeMockPart2B)).when(rootProductNodeMock).getNodes();
		
		// Testing
		final Double result = welProductFacade.getPriceForDefaultSetProduct(rootNodeMock);
	
		// Verify
		assertNull(result);
	}
	
	@Test
	public void testGetPriceForDefaultSetProductWhenNoSetPresent()
	{
		// Given
		doReturn(Arrays.asList(nodeMockPart1A, nodeMockPart2B)).when(nodeMockType1).getNodes();
		
		// Testing
		final Double result = welProductFacade.getPriceForDefaultSetProduct(rootNodeMock);
	
		// Verify
		assertNull(result);
	}
	
	@Test
	public void checkPriceAndAddProductWhenListIsEmpty()
	{
		// Given
		ProductData productData = createProductData(PRODUCT_1, BASE_PRODUCT, PRICE_VALUE);
		List<ProductData> products = new ArrayList<>();

		// When
		welProductFacade.checkPriceAndAddProductToList(products, productData);

		// Then
		assertEquals(1, products.size());
		assertEquals(PRICE_VALUE, products.get(0).getPrice().getValue().doubleValue(), 0);
	}

	@Test
	public void checkPriceAndAddProductWhenElementWithTheSameBaseProductInList()
	{
		// Given
		ProductData productData = createProductData(PRODUCT_1, BASE_PRODUCT, PRICE_VALUE);
		List<ProductData> products = new ArrayList<>();
		ProductData listElement = createProductData(PRODUCT_2, BASE_PRODUCT, MIN_PRICE_VALUE);
		products.add(listElement);

		// When
		welProductFacade.checkPriceAndAddProductToList(products, productData);

		// Then
		assertEquals(1, products.size());
		assertEquals(MIN_PRICE_VALUE, products.get(0).getPrice().getValue().doubleValue(), 0);
		assertEquals(PRODUCT_2, products.get(0).getCode());
	}

	@Test
	public void checkPriceAndAddProductWhenNoElementWithTheSameBaseProductInList()
	{
		// Given
		ProductData productData = createProductData(PRODUCT_1, BASE_PRODUCT, PRICE_VALUE);
		List<ProductData> products = new ArrayList<>();
		ProductData listElement = createProductData(PRODUCT_2, ANOTHER_BASE_PRODUCT, MIN_PRICE_VALUE);
		products.add(listElement);

		// When
		welProductFacade.checkPriceAndAddProductToList(products, productData);

		// Then
		assertEquals(2, products.size());
		assertEquals(MIN_PRICE_VALUE, products.get(0).getPrice().getValue().doubleValue(), 0);
		assertEquals(PRICE_VALUE, products.get(1).getPrice().getValue().doubleValue(), 0);
	}

	@Test
	public void checkPriceAndAddProductWhenProductHasMinPrice()
	{
		// Given
		ProductData productData = createProductData(PRODUCT_1, BASE_PRODUCT, MIN_PRICE_VALUE);
		List<ProductData> products = new ArrayList<>();
		ProductData listElement = createProductData(PRODUCT_2, BASE_PRODUCT, PRICE_VALUE);

		// When
		welProductFacade.checkPriceAndAddProductToList(products, productData);

		// Then
		assertEquals(1, products.size());
		assertEquals(MIN_PRICE_VALUE, products.get(0).getPrice().getValue().doubleValue(), 0);
		assertEquals(PRODUCT_1, products.get(0).getCode());
	}

	@Test
	public void shouldGetComparisonProductsForCategory()
	{
		ProductComparisonData productComparisonData = welProductFacade.getComparisonProductsForCategory(
				CATEGORY_CODE, PRODUCT_OPTIONS, MAX_NUMBER_OF_PRODUCTS);
		final List<ProductData> expectedProductsDataList = Arrays.asList(productData1, productData2, productData3);
		final List<FeatureData> expectedFeaturesDataList = Arrays.asList(feature1, feature2, feature3, feature4);
		assertEquals(expectedProductsDataList, productComparisonData.getProducts());
		assertEquals(expectedFeaturesDataList, productComparisonData.getFeatures());
	}

	@Test
	public void shouldGetComparisonProductsData()
	{
		//Given
		when(productAttribute1.isVisible()).thenReturn(true);
		when(productAttribute2.isVisible()).thenReturn(true);
		when(productAttribute3.isVisible()).thenReturn(false);
		when(productAttribute4.isVisible()).thenReturn(true);
		when(wileyClassificationService.isFeatureVisible(featureModel1)).thenReturn(true);
		when(wileyClassificationService.isFeatureVisible(featureModel2)).thenReturn(true);
		when(wileyClassificationService.isFeatureVisible(featureModel3)).thenReturn(false);
		when(mockProductModel1.getFeatures()).thenReturn(Arrays.asList(featureModel1));
		when(mockProductModel2.getFeatures()).thenReturn(Arrays.asList(featureModel2));
		when(mockProductModel3.getFeatures()).thenReturn(Arrays.asList(featureModel3));
		//When
		ProductComparisonData productComparisonData = welProductFacade.getComparisonProductData(
				Arrays.asList(mockProductModel1, mockProductModel2, mockProductModel3), PRODUCT_OPTIONS,
				Arrays.asList(productAttribute1, productAttribute2, productAttribute3, productAttribute4));
		//Then
		final List<ProductData> expectedProductsDataList = Arrays.asList(productData1, productData2);
		final List<FeatureData> expectedFeaturesDataList = Arrays.asList(feature1, feature2);
		assertEquals(expectedProductsDataList, productComparisonData.getProducts());
		assertEquals(expectedFeaturesDataList, productComparisonData.getFeatures());
		assertEquals(CATEGORY_NAME + " " + PRODUCT_1_NAME, productComparisonData.getProducts().get(0).getName());
		assertEquals(CATEGORY_NAME + " " + PRODUCT_2_NAME, productComparisonData.getProducts().get(1).getName());
	}

	@Test
	public void shouldGetSupplementsProductData()
	{
		//When
		List<ProductData> productDataList = welProductFacade.getSupplementsProductData(
				Arrays.asList(mockProductModel1, mockProductModel2, mockProductModel3, mockProductModel4), PRODUCT_OPTIONS);
		//Then
		final List<ProductData> expectedProductsDataList = Arrays.asList(productData1, productData2, productData3);
		assertEquals(expectedProductsDataList, productDataList);
		assertEquals(CATEGORY_NAME + " " + PRODUCT_1_NAME, productDataList.get(0).getName());
		assertEquals(CATEGORY_NAME + " " + PRODUCT_2_NAME, productDataList.get(1).getName());
		assertEquals(CATEGORY_NAME + " " + PRODUCT_3_NAME, productDataList.get(2).getName());
	}

	@Test
	public void shouldGetComparisonProductsForCategories()
	{
		ProductComparisonData productComparisonData = welProductFacade.getComparisonProductsForCategories(
				BASE_PRODUCT_CATEGORY_CODE, VARIANT_VALUE_CATEGORY_CODE, PRODUCT_OPTIONS, MAX_NUMBER_OF_PRODUCTS);
		final List<ProductData> expectedProductsDataList = Arrays.asList(productData1, productData2);
		final List<FeatureData> expectedFeaturesDataList = Arrays.asList(feature1, feature2, feature3);
		assertEquals(expectedProductsDataList, productComparisonData.getProducts());
		assertEquals(expectedFeaturesDataList, productComparisonData.getFeatures());
	}

	@Test
	public void shouldGetSupplementProductsForCategory()
	{
		List<ProductData> actualSupplementsList = welProductFacade.getSupplementProductsForCategory(
				CATEGORY_CODE, PRODUCT_OPTIONS);
		final List<ProductData> expectedSupplementsList = Arrays.asList(productData3);
		assertEquals(expectedSupplementsList, actualSupplementsList);
	}

	@Test
	public void shouldGetSupplementProductsForCategories()
	{
		List<ProductData> actualSupplementsList = welProductFacade.getSupplementProductsForCategories(CATEGORY_CODE,
				VARIANT_VALUE_CATEGORY_CODE, PRODUCT_OPTIONS);
		final List<ProductData> expectedSupplementsList = Arrays.asList(productData3);
		assertEquals(expectedSupplementsList, actualSupplementsList);
	}

	@Test
	public void shouldFilterSupplementProductsForCategories()
	{
		welProductFacade.getSupplementProductsForCategories(CATEGORY_CODE,
				VARIANT_VALUE_CATEGORY_CODE, PRODUCT_OPTIONS);
		verify(mockWileyProductService).getWileyProductsFromCategories(SUPPLEMENT_CATEGORY_CODE, VARIANT_VALUE_CATEGORY_CODE);
		verify(mockWileyProductService).filterProducts(
				eq(Arrays.asList(mockProductModel2, mockProductModel3, mockProductModel4)));
	}

	@Test
	public void testDoesProductExistForIsbnWhenProductIsFound()
	{
		// Given
		final String testIsbn = "123412345";
		when(mockWileyProductService.getProductForIsbn(eq(testIsbn))).thenReturn(mockProductModel1);

		// When
		final boolean result = welProductFacade.doesProductExistForIsbn(testIsbn);

		// Then
		assertTrue(result);
	}

	@Test
	public void testDoesProductExistForIsbnWhenProductIsNotFoundCase1()
	{
		// Given
		when(mockWileyProductService.getProductForIsbn(anyString())).thenThrow(
				new UnknownIdentifierException("Product not found."));

		// When
		final boolean result = welProductFacade.doesProductExistForIsbn("124554624545");

		// Then
		assertFalse(result);
	}

	@Test
	public void testDoesProductExistForIsbnWhenProductIsNotFoundCase2()
	{
		// Given
		when(mockWileyProductService.getProductForIsbn(anyString())).thenReturn(null);

		// When
		final boolean result = welProductFacade.doesProductExistForIsbn("124554624545");

		// Then
		assertFalse(result);
	}


	@Test
	public void testGetPriceForDefaultPartInOneLevelDeepProduct()
	{
		// Given
		doReturn(productNodeListMock).when(rootProductNodeMock).getNodes();
		doReturn(productNodeMock).when(productNodeListMock).get(0);
		doReturn(null).when(productNodeMock).getNodes();
		doReturn(productNodeValueModel).when(productNodeMock).getValue();
		doReturn(DEFAULT_PART_PRICE).when(productNodeValueModel).getPrice();

		// Testing
		final Double priceForDefaultPart = welProductFacade.getPriceForDefaultPart(rootNodeMock);

		// Verify
		assertEquals(Double.valueOf(DEFAULT_PART_PRICE), priceForDefaultPart);
	}
		
	@Test
	public void testGetPriceForDefaultPart()
	{
		// Given
		doReturn(productNodeListMock).when(rootProductNodeMock).getNodes();
		doReturn(productNodeMock).when(productNodeListMock).get(0);
		doReturn(productNodeChildListMock).when(productNodeMock).getNodes();
		doReturn(productChildNodeMock).when(productNodeChildListMock).get(0);
		doReturn(productNodeValueModel).when(productChildNodeMock).getValue();
		doReturn(DEFAULT_PART_PRICE).when(productNodeValueModel).getPrice();
				
		// Testing
		final Double priceForDefaultPart = welProductFacade.getPriceForDefaultPart(rootNodeMock);

		// Verify
		assertEquals(Double.valueOf(DEFAULT_PART_PRICE), priceForDefaultPart);
	}

	private ProductData createProductData(final String productCode, final String baseProduct, final double price)
	{
		ProductData productData = new ProductData();
		productData.setCode(productCode);
		productData.setBaseProduct(baseProduct);
		PriceData priceData = new PriceData();
		priceData.setValue(new BigDecimal(price));
		productData.setPrice(priceData);
		return productData;
	}

	private void setUpNodePrice(final ProductNode productNode, final ProductNodeValue productNodeValue,
								double price, boolean isSet)
	{
		doReturn(productNodeValue).when(productNode).getValue();
		doReturn(isSet).when(productNode).isSetProduct();
		doReturn(null).when(productNode).getNodes();
		doReturn(price).when(productNodeValue).getPrice();
	}
}
