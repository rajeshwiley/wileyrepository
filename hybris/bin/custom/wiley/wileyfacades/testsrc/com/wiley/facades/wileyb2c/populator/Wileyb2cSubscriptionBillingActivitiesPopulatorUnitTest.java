package com.wiley.facades.wileyb2c.populator;

import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.wiley.core.enums.PaymentModeEnum;
import com.wiley.core.model.WileySubscriptionPaymentTransactionModel;
import com.wiley.facades.product.data.WileySubscriptionPaymentTransactionData;


public class Wileyb2cSubscriptionBillingActivitiesPopulatorUnitTest
{
	private static final String CODE = "codeValue";
	private static final double AMOUNT = 123.0;
	private static final Date DATE = new Date();
	private static final PaymentTransactionType PAYMENT_TRANSACTION_TYPE = PaymentTransactionType.CAPTURE;
	private static final PaymentModeEnum PAYMENT_MODE_CARD = PaymentModeEnum.CARD;
	private static final String FORMATTED_PRICE = "FORMATTED_PRICE";
	private static final String MASK = "**** ";
	private static final String LONG_STRING = "abcdefghijklmnopqrstuvwxyz";
	private static final String FOUR_CHARS = "abcd";
	private static final String STRING_OF_DIGITS = "123412341234";
	private static final String FOUR_DIGITS = "6789";
	private static final String SHORT_STRING = "123";
	private static final String EMPTY_STRING = "";

	@InjectMocks
	private Wileyb2cSubscriptionBillingActivitiesPopulator subscriptionBillingActivitiesPopulator;

	@Mock
	private WileySubscriptionPaymentTransactionModel paymentTransactionModelMock;

	@Mock
	private PriceDataFactory priceDataFactory;

	@Mock
	private CurrencyModel currencyModelMock;

	@Mock
	private PriceData priceDataMock;


	private WileySubscriptionPaymentTransactionData paymentTransactionData;


	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);

		paymentTransactionData = new WileySubscriptionPaymentTransactionData();

		doReturn(currencyModelMock).when(paymentTransactionModelMock).getCurrency();
		doReturn(priceDataMock).when(priceDataFactory).create(any(), eq(new BigDecimal(AMOUNT)), eq(currencyModelMock));
		doReturn(FORMATTED_PRICE).when(priceDataMock).getFormattedValue();

		doReturn(CODE).when(paymentTransactionModelMock).getCode();
		doReturn(DATE).when(paymentTransactionModelMock).getTime();
		doReturn(PAYMENT_TRANSACTION_TYPE).when(paymentTransactionModelMock).getType();
		doReturn(AMOUNT).when(paymentTransactionModelMock).getAmount();
	}

	@Test
	public void testMainFields()
	{
		//given

		//testing
		subscriptionBillingActivitiesPopulator.populate(paymentTransactionModelMock, paymentTransactionData);

		//verify
		assertEquals(CODE, paymentTransactionData.getCode());
		assertEquals(DATE, paymentTransactionData.getBillingDate());
		assertEquals(PAYMENT_TRANSACTION_TYPE, paymentTransactionData.getPaymentStatus());
		assertEquals(FORMATTED_PRICE, paymentTransactionData.getPaymentAmount());
	}

	@Test
	public void verifyThatOnlyCreditCardPaymentMethodGeneratesCardMask()
	{
		Arrays.stream(PaymentModeEnum.values()).forEach(paymentModeEnumValue -> {
			if (!PaymentModeEnum.CARD.equals(paymentModeEnumValue))
			{
				//given
				doReturn(paymentModeEnumValue).when(paymentTransactionModelMock).getPaymentMode();

				//testing
				subscriptionBillingActivitiesPopulator.populate(paymentTransactionModelMock, paymentTransactionData);

				//verify
				assertEquals(paymentModeEnumValue, paymentTransactionData.getPaymentMode());
				assertNull(paymentTransactionData.getCreditCardMask());
			}
		});
	}
	
	@Test
	public void verifyCardMaskWhenPaymentTokenIsNumber()
	{

		//given
		doReturn(PAYMENT_MODE_CARD).when(paymentTransactionModelMock).getPaymentMode();
		doReturn(STRING_OF_DIGITS + FOUR_DIGITS).when(paymentTransactionModelMock).getPaymentToken();

		//testing
		subscriptionBillingActivitiesPopulator.populate(paymentTransactionModelMock, paymentTransactionData);

		//verify
		verifyThatPaymentModeIsCreditCardAndMaskIsCorrect(MASK + FOUR_DIGITS);
	}

	@Test
	public void verifyCardMaskWhenPaymentTokenIsLongString()
	{
		//given
		doReturn(PAYMENT_MODE_CARD).when(paymentTransactionModelMock).getPaymentMode();
		doReturn(LONG_STRING + FOUR_CHARS).when(paymentTransactionModelMock).getPaymentToken();

		//testing
		subscriptionBillingActivitiesPopulator.populate(paymentTransactionModelMock, paymentTransactionData);

		//verify
		verifyThatPaymentModeIsCreditCardAndMaskIsCorrect(MASK + FOUR_CHARS);
	}

	@Test
	public void verifyCardMaskWhenPaymentTokenIsTooShort()
	{
		//given
		doReturn(PAYMENT_MODE_CARD).when(paymentTransactionModelMock).getPaymentMode();
		doReturn(SHORT_STRING).when(paymentTransactionModelMock).getPaymentToken();

		//testing
		subscriptionBillingActivitiesPopulator.populate(paymentTransactionModelMock, paymentTransactionData);

		//verify
		verifyThatPaymentModeIsCreditCardAndMaskIsCorrect(EMPTY_STRING);
	}

	@Test
	public void verifyCardMaskWhenPaymentTokenIsEmptyString()
	{
		//given
		doReturn(PAYMENT_MODE_CARD).when(paymentTransactionModelMock).getPaymentMode();
		doReturn(EMPTY_STRING).when(paymentTransactionModelMock).getPaymentToken();

		//testing
		subscriptionBillingActivitiesPopulator.populate(paymentTransactionModelMock, paymentTransactionData);

		//verify
		verifyThatPaymentModeIsCreditCardAndMaskIsCorrect(EMPTY_STRING);
	}

	@Test
	public void verifyCardMaskWhenPaymentTokenIsNull()
	{

		//given
		doReturn(PAYMENT_MODE_CARD).when(paymentTransactionModelMock).getPaymentMode();
		doReturn(null).when(paymentTransactionModelMock).getPaymentToken();

		//testing
		subscriptionBillingActivitiesPopulator.populate(paymentTransactionModelMock, paymentTransactionData);

		//verify
		verifyThatPaymentModeIsCreditCardAndMaskIsCorrect(EMPTY_STRING);
	}

	protected void verifyThatPaymentModeIsCreditCardAndMaskIsCorrect(final String expected)
	{
		assertEquals(PAYMENT_MODE_CARD, paymentTransactionData.getPaymentMode());
		assertEquals(expected, paymentTransactionData.getCreditCardMask());
	}

}