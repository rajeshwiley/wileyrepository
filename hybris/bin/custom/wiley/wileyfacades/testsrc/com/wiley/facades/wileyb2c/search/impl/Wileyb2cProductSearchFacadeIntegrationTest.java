package com.wiley.facades.wileyb2c.search.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.ProductSearchFacade;
import de.hybris.platform.commercefacades.search.data.SearchQueryData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.search.facetdata.ProductSearchPageData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.jalo.order.price.PriceFactory;
import de.hybris.platform.site.BaseSiteService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Sets;
import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.servicelayer.i18n.WileycomI18NService;
import com.wiley.core.wileyb2c.jalo.Wileyb2cEurope1PriceFactory;


/**
 * Created by Maksim_Kozich on 23.05.17.
 */
@IntegrationTest
public class Wileyb2cProductSearchFacadeIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	public static final Logger LOG = LoggerFactory.getLogger(Wileyb2cProductSearchFacadeIntegrationTest.class);

	public static final int MAX_PAGE_LIMIT = 10;

	private static final String WCOM_BASE_PRODUCT = "WCOM_BASE_PRODUCT";
	private static final String BASE_SITE = "wileyb2c";
	private static final String CATALOG_ID = "wileyProductCatalog";
	private static final String CATALOG_VERSION = "Online";
	public static final Set<String> EXPECTED_CODES = Sets.newHashSet(
			"WCOM_PRODUCT_EBOOK",
			"WCOM_VARIANT_EBOOK_SET", "WCOM_VARIANT_EBOOK_SET_COMPONENT",
			"WCOM_VARIANT_HARDCOVER_SET2", "WCOM_VARIANT_HARDCOVER_SET2_COMPONENT",
			"WCOM_VARIANT_EBOOK_SET3", "WCOM_VARIANT_EBOOK_SET3_COMPONENT");

	/**
	 * The Wileyb2c product search facade.
	 */
	@Resource
	private ProductSearchFacade<ProductData> wileyb2cProductSearchFacade;

	@Resource
	private Wileyb2cEurope1PriceFactory wileyb2cEurope1PriceFactory;

	@Resource
	private BaseSiteService baseSiteService;

	@Resource
	private CatalogVersionService catalogVersionService;

	@Resource
	private WileycomI18NService wileycomI18NService;

	private PriceFactory sessionPriceFactoryBefore;


	/**
	 * Sets up.
	 *
	 * @throws Exception
	 * 		the exception
	 */
	@Before
	public void setUp() throws Exception
	{
		replacePriceFactory();
		baseSiteService.setCurrentBaseSite(baseSiteService.getBaseSiteForUID(BASE_SITE), true);
		catalogVersionService.setSessionCatalogVersion(CATALOG_ID, CATALOG_VERSION);
		importCsv("/wileyfacades/test/Wileyb2cProductSearchFacadeIntegrationTest/legacy-cart-configurations.impex",
				DEFAULT_ENCODING);
		importCsv("/wileyfacades/test/Wileyb2cProductSearchFacadeIntegrationTest/solr-indexed-type.impex",
				DEFAULT_ENCODING);
	}

	protected void replacePriceFactory()
	{
		sessionPriceFactoryBefore = jaloSession.getPriceFactory();
		jaloSession.setPriceFactory(wileyb2cEurope1PriceFactory);
	}

	@After
	public void tearDown()
	{
		restorePriceFactory();
	}

	protected void restorePriceFactory()
	{
		jaloSession.setPriceFactory(sessionPriceFactoryBefore);
	}

	@Test
	public void testSearchByCodeProductCode() throws Exception
	{
		//given
		wileycomI18NService.setDefaultCurrentCountry();


		//when
		final PageableData pageableData = createPageableData(0, getMaxSearchPageSize(), null);
		final SearchStateData searchState = getSearchStateData(WCOM_BASE_PRODUCT);

		//when
		ProductSearchPageData<SearchStateData, ProductData> searchPageData = wileyb2cProductSearchFacade.textSearch(searchState,
				pageableData);

		//then
		List<ProductData> results = searchPageData.getResults();
		assertTrue(CollectionUtils.isNotEmpty(results));
		Set<String> codes = results.stream().map(ProductData::getCode).collect(Collectors.toSet());
		assertEquals(EXPECTED_CODES, codes);
	}

	private SearchStateData getSearchStateData(final String searchQuery)
	{
		final SearchStateData searchState = new SearchStateData();
		final SearchQueryData searchQueryData = new SearchQueryData();
		searchQueryData.setValue(searchQuery);
		searchState.setQuery(searchQueryData);
		return searchState;
	}

	private PageableData createPageableData(final int pageNumber, final int pageSize, final String sortCode)
	{
		final PageableData pageableData = new PageableData();
		pageableData.setCurrentPage(pageNumber);
		pageableData.setSort(sortCode);
		pageableData.setPageSize(pageSize);

		return pageableData;
	}

	protected int getMaxSearchPageSize()
	{
		return MAX_PAGE_LIMIT;
	}
}
