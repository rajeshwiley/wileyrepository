package com.wiley.facades.order.converters.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyAddressPopulatorTest
{
	@Mock
	private AddressModel addressModelMock;

	@Mock
	private CountryModel countryModelMock;

	@Mock
	private RegionModel regionModelMock;

	@Mock
	private CountryData countryDataMock;

	@Mock
	private Converter<CountryModel, CountryData> wileyCountryConverter;

	@Mock
	private AddressData addressData;

	@Spy
	@InjectMocks
	private WileyAddressPopulator wileyAddressPopulator;

	@Before
	public void setUp()
	{
		doNothing().when(wileyAddressPopulator).superPopulate(addressModelMock, addressData);

		when(addressModelMock.getCountry()).thenReturn(countryModelMock);
		when(wileyCountryConverter.convert(countryModelMock)).thenReturn(countryDataMock);
		when(addressModelMock.getLine1()).thenReturn("Line 1");
		when(addressModelMock.getLine2()).thenReturn("Line 2");
		when(addressModelMock.getTown()).thenReturn("Town");
		when(countryModelMock.getName()).thenReturn("United States");
		when(addressModelMock.getRegion()).thenReturn(regionModelMock);
		when(addressModelMock.getPostalcode()).thenReturn("453213");
		when(regionModelMock.getName()).thenReturn("Region name");
	}

	@Test
	public void testShouldPopulateExpectedFields()
	{
		wileyAddressPopulator.populate(addressModelMock, addressData);

		verify(wileyAddressPopulator).superPopulate(addressModelMock, addressData);
		verify(addressData).setCountry(countryDataMock);
		verify(addressData).setAddressSummary("Line 1, Line 2, Town, Region name 453213, United States");
	}

	@Test
	public void testShouldNotPopulateCountry()
	{
		when(addressModelMock.getCountry()).thenReturn(null);

		wileyAddressPopulator.populate(addressModelMock, addressData);

		verify(wileyAddressPopulator).superPopulate(addressModelMock, addressData);
		verify(addressData, never()).setCountry(countryDataMock);
	}

	@Test
	public void testShouldNotPopulateLine1FieldInSummary()
	{
		// Given
		when(addressModelMock.getLine1()).thenReturn(null);

		// When
		wileyAddressPopulator.populate(addressModelMock, addressData);

		// Then
		verify(wileyAddressPopulator).superPopulate(addressModelMock, addressData);
		verify(addressData).setCountry(countryDataMock);
		verify(addressData).setAddressSummary("Line 2, Town, Region name 453213, United States");
	}

	@Test
	public void testShouldNotPopulateLine2FieldInSummary()
	{
		// Given
		when(addressModelMock.getLine2()).thenReturn(null);

		// When
		wileyAddressPopulator.populate(addressModelMock, addressData);

		// Then
		verify(wileyAddressPopulator).superPopulate(addressModelMock, addressData);
		verify(addressData).setCountry(countryDataMock);
		verify(addressData).setAddressSummary("Line 1, Town, Region name 453213, United States");
	}

	@Test
	public void testShouldNotPopulateTownFieldInSummary()
	{
		// Given
		when(addressModelMock.getTown()).thenReturn(null);

		// When
		wileyAddressPopulator.populate(addressModelMock, addressData);

		// Then
		verify(wileyAddressPopulator).superPopulate(addressModelMock, addressData);
		verify(addressData).setCountry(countryDataMock);
		verify(addressData).setAddressSummary("Line 1, Line 2, Region name 453213, United States");
	}

	@Test
	public void testShouldNotPopulateRegionNameAndPostalCodeFieldsInSummaryWhenPostalCodeIsNull()
	{
		// Given
		when(addressModelMock.getPostalcode()).thenReturn(null);

		// When
		wileyAddressPopulator.populate(addressModelMock, addressData);

		// Then
		verify(wileyAddressPopulator).superPopulate(addressModelMock, addressData);
		verify(addressData).setCountry(countryDataMock);
		verify(addressData).setAddressSummary("Line 1, Line 2, Town, United States");
	}

	@Test
	public void testShouldNotPopulateRegionNameFieldInSummaryWhenRegionIsNull()
	{
		// Given
		when(addressModelMock.getRegion()).thenReturn(null);

		// When
		wileyAddressPopulator.populate(addressModelMock, addressData);

		// Then
		verify(wileyAddressPopulator).superPopulate(addressModelMock, addressData);
		verify(addressData).setCountry(countryDataMock);
		verify(addressData).setAddressSummary("Line 1, Line 2, Town, 453213, United States");
	}

	@Test
	public void testShouldNotPopulateRegionNameFieldInSummaryWhenRegionNameIsNull()
	{
		// Given
		when(regionModelMock.getName()).thenReturn(null);

		// When
		wileyAddressPopulator.populate(addressModelMock, addressData);

		// Then
		verify(wileyAddressPopulator).superPopulate(addressModelMock, addressData);
		verify(addressData).setCountry(countryDataMock);
		verify(addressData).setAddressSummary("Line 1, Line 2, Town, 453213, United States");
	}

	@Test
	public void testShouldNotPopulateCountryNameFieldInSummaryWhenCountryModelIsNotNull()
	{
		// Given
		when(countryModelMock.getName()).thenReturn(null);

		// When
		wileyAddressPopulator.populate(addressModelMock, addressData);

		// Then
		verify(wileyAddressPopulator).superPopulate(addressModelMock, addressData);
		verify(addressData).setCountry(countryDataMock);
		verify(addressData).setAddressSummary("Line 1, Line 2, Town, Region name 453213");
	}

	@Test
	public void testShouldNotPopulateCountryNameFieldInSummaryWhenCountryModelIsNull()
	{
		// Given
		when(addressModelMock.getCountry()).thenReturn(null);

		// When
		wileyAddressPopulator.populate(addressModelMock, addressData);

		// Then
		verify(wileyAddressPopulator).superPopulate(addressModelMock, addressData);
		verify(addressData).setAddressSummary("Line 1, Line 2, Town, Region name 453213");
	}

	@Test
	public void testAddressSummaryFieldShouldBeEmptyWhenAddressDataIsNotFullySpecified()
	{
		// Given
		when(addressModelMock.getLine1()).thenReturn(null);
		when(addressModelMock.getLine2()).thenReturn(null);
		when(addressModelMock.getTown()).thenReturn(null);
		when(addressModelMock.getPostalcode()).thenReturn(null);
		when(countryModelMock.getName()).thenReturn(null);
		when(regionModelMock.getName()).thenReturn(null);

		// When
		wileyAddressPopulator.populate(addressModelMock, addressData);

		// Then
		verify(addressData).setAddressSummary("");
	}
}