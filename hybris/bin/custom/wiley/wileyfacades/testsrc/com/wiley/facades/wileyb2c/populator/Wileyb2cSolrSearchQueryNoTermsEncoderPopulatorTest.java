/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 *
 *
 */
package com.wiley.facades.wileyb2c.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.search.data.SearchQueryData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryTermData;
import de.hybris.platform.converters.Populator;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Author Maksim_Kozich
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cSolrSearchQueryNoTermsEncoderPopulatorTest
{
	@InjectMocks
	private Wileyb2cSolrSearchQueryNoTermsEncoderPopulator wileyb2cSolrSearchQueryNoTermsEncoderPopulator;

	@Mock
	private Populator<SolrSearchQueryData, SearchQueryData> solrSearchQueryDataSearchQueryDataPopulatorMock;

	@Mock
	private SolrSearchQueryData sourceMock;

	@Mock
	private SearchQueryData targetMock;

	@Mock
	private SolrSearchQueryTermData termMock1;

	@Mock
	private SolrSearchQueryTermData termMock2;


	@Test
	public void testNotEmptyTermsAreFiltered() throws Exception
	{
		//Given
		when(sourceMock.getFilterTerms()).thenReturn(new ArrayList<>(Arrays.asList(termMock1, termMock2)));

		//When
		wileyb2cSolrSearchQueryNoTermsEncoderPopulator.populate(sourceMock, targetMock);

		//Then
		verify(solrSearchQueryDataSearchQueryDataPopulatorMock).populate(argThat(new EmptyFilterTermsMatcher()),
				same(targetMock));
	}


	@Test
	public void testNullTermsAreOk() throws Exception
	{
		//Given
		when(sourceMock.getFilterTerms()).thenReturn(null);

		//When
		wileyb2cSolrSearchQueryNoTermsEncoderPopulator.populate(sourceMock, targetMock);

		//Then
		verify(solrSearchQueryDataSearchQueryDataPopulatorMock).populate(argThat(new EmptyFilterTermsMatcher()),
				same(targetMock));
	}

	class EmptyFilterTermsMatcher extends ArgumentMatcher<SolrSearchQueryData>
	{
		@Override
		public boolean matches(final Object o)
		{
			if (o instanceof SolrSearchQueryData)
			{
				final SolrSearchQueryData solrSearchQueryData = (SolrSearchQueryData) o;
				return CollectionUtils.isEmpty(solrSearchQueryData.getFilterTerms());
			}
			return false;
		}
	}

	@Test
	public void testNullSourceProvided() throws Exception
	{
		//When
		try
		{
			wileyb2cSolrSearchQueryNoTermsEncoderPopulator.populate(null, targetMock);
			fail("Expected " + IllegalArgumentException.class);
		} catch (IllegalArgumentException e)
		{
			// Success
			// Then
			verify(solrSearchQueryDataSearchQueryDataPopulatorMock, never()).populate(any(), any());
		}
	}

	@Test
	public void testNullTargetProvided() throws Exception
	{
		//When
		try
		{
			wileyb2cSolrSearchQueryNoTermsEncoderPopulator.populate(sourceMock, null);
			fail("Expected " + IllegalArgumentException.class);
		} catch (IllegalArgumentException e)
		{
			// Success
			// Then
			verify(solrSearchQueryDataSearchQueryDataPopulatorMock, never()).populate(any(), any());
		}
	}
}
