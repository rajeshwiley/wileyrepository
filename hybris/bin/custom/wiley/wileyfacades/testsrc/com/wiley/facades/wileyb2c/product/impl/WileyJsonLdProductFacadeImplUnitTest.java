package com.wiley.facades.wileyb2c.product.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.AuthorInfoModel;
import com.wiley.core.model.WileyPurchaseOptionProductModel;
import com.wiley.core.wileyb2c.classification.Wileyb2cClassificationService;
import com.wiley.facades.wileyb2c.product.data.PersonJsonLdDto;
import com.wiley.facades.wileyb2c.product.data.ProductJsonLdDto;


/**
 * Unit tests for WileyJsonLdProductFacadeImpl
 *
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyJsonLdProductFacadeImplUnitTest
{
	private static final String DEFAULT_IMAGE = "defaultImage";
	private static final String IMAGE = "image";
	private static final String URL = "url";
	private static final String SEO_DESCRIPTION_TAG = "seo description";

	private static final String DEFAULT_IMAGE_RESULT =
			"{\"image\":\"defaultImage\",\"url\":\"url\",\"description\":\"seo description\",\"author\":[{},"
					+ "{}],\"bookFormat\":\"http://schema.org/Paperback\"}";
	private static final String WITHOUT_IMAGE_RESULT =
			"{\"image\":\"image\",\"url\":\"url\",\"description\":\"seo description\",\"author\":[{},"
					+ "{}],\"bookFormat\":\"http://schema.org/Paperback\"}";

	@Mock
	private Converter<ProductData, ProductJsonLdDto> wileyb2cProductJsonLdConverter;
	@Mock
	private Converter<AuthorInfoModel, PersonJsonLdDto> wileyb2cPersonJsonLdConverter;
	@Mock
	private Wileyb2cClassificationService wileyb2cClassificationServiceMock;
	@Mock
	private WileyPurchaseOptionProductModel productModelMock;
	@Mock
	private ProductModel baseProductMock;
	@Mock
	private AuthorInfoModel firstAuthorMock;
	@Mock
	private AuthorInfoModel secondAuthorMock;
	@Mock
	private AuthorInfoModel thirdAuthorMock;
	@Mock
	private ClassificationClassModel classificationClassModelMock;

	private ProductData productData;
	private List<AuthorInfoModel> authorInfos;
	private String firstAuthorName = "firstName";
	private String secondAuthorName = "secondName";
	private PersonJsonLdDto firstPersonDto;
	private PersonJsonLdDto secondPersonDto;
	private PersonJsonLdDto thirdPersonDto;
	private String classificationCode = "paper";
	private ProductJsonLdDto productJsonLdDto;

	@InjectMocks
	private WileyJsonLdProductFacadeImpl wileyJsonLdProductFacade;

	@Before
	public void setUp() throws Exception
	{
		productData = new ProductData();
		authorInfos = new ArrayList<>();
		firstPersonDto = new PersonJsonLdDto();
		secondPersonDto = new PersonJsonLdDto();
		thirdPersonDto = new PersonJsonLdDto();
		productJsonLdDto = new ProductJsonLdDto();

		authorInfos.add(firstAuthorMock);
		authorInfos.add(secondAuthorMock);
		authorInfos.add(thirdAuthorMock);

		when(firstAuthorMock.getName()).thenReturn(firstAuthorName);
		when(secondAuthorMock.getName()).thenReturn(secondAuthorName);
		when(productModelMock.getBaseProduct()).thenReturn(baseProductMock);
		when(baseProductMock.getSeoDescriptionTag()).thenReturn(SEO_DESCRIPTION_TAG);
		when(baseProductMock.getAuthorInfos()).thenReturn(authorInfos);
		when(wileyb2cProductJsonLdConverter.convert(productData)).thenReturn(productJsonLdDto);
		when(wileyb2cPersonJsonLdConverter.convert(firstAuthorMock)).thenReturn(firstPersonDto);
		when(wileyb2cPersonJsonLdConverter.convert(secondAuthorMock)).thenReturn(secondPersonDto);
		when(wileyb2cPersonJsonLdConverter.convert(thirdAuthorMock)).thenReturn(thirdPersonDto);
		when(wileyb2cClassificationServiceMock.resolveClassificationClass(productModelMock))
				.thenReturn(classificationClassModelMock);
		when(classificationClassModelMock.getCode()).thenReturn(classificationCode);
	}

	@Test
	public void createJsonLdFromProductWithDefaultImageTest() throws Exception
	{
		final String jsonLdFromProduct = wileyJsonLdProductFacade
				.createJsonLdFromProduct(productData, productModelMock, URL, DEFAULT_IMAGE);
		assertNotNull(jsonLdFromProduct);
		assertEquals(DEFAULT_IMAGE_RESULT, jsonLdFromProduct);
	}

	@Test
	public void createJsonLdFromProductWithoutDefaultImageTest() throws Exception
	{
		productJsonLdDto.setImage(IMAGE);

		final String jsonLdFromProduct = wileyJsonLdProductFacade
				.createJsonLdFromProduct(productData, productModelMock, URL, DEFAULT_IMAGE);
		assertNotNull(jsonLdFromProduct);
		assertEquals(WITHOUT_IMAGE_RESULT, jsonLdFromProduct);
	}

}
