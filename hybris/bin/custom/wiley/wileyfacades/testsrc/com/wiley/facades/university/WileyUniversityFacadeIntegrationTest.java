package com.wiley.facades.university;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.facades.university.data.UniversityData;
import com.wiley.facades.university.impl.WileyUniversityFacade;
import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Integration test suite for {@link com.wiley.facades.university.impl.WileyUniversityFacade}.
 */
@IntegrationTest
public class WileyUniversityFacadeIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{

	private static final String INVALID_ISOCODE = "INVALID_ISOCODE";
	private static final String US_ISOCODE = "US";

	@Resource(name = "universityFacade")
	private WileyUniversityFacade wileyUniversityFacade;

	/**
	 * Sets up.
	 *
	 * @throws Exception
	 * 		the exception
	 */
	@Before
	public void setUp() throws Exception
	{
		importCsv("/wileycore/test/coredata/common/universities.impex", "utf-8");
	}

	/**
	 * Test getUniversitiesByCountryAndRegionIso with valid country and region isocodes.
	 */
	@Test
	public void getUniversitiesByCountryAndRegionIsoWithValidParameters()
	{
		List<UniversityData> universityDataList = wileyUniversityFacade.getUniversitiesByCountryAndRegionIso(US_ISOCODE, "US-AL");

		assertNotNull(universityDataList);
		assertEquals(3, universityDataList.size());

		//check order
		assertEquals("Alabama A & M University", universityDataList.get(0).getName());
		assertEquals("Community College of the Air Force", universityDataList.get(1).getName());
		assertEquals("University of Alabama at Birmingham", universityDataList.get(2).getName());
	}

	/**
	 * Test getUniversitiesByCountryAndRegionIso method with invalid country isocode.
	 */
	@Test(expected = UnknownIdentifierException.class)
	public void getUniversitiesByCountryAndRegionIsoWithInvalidCountryIso()
	{
		List<UniversityData> universityDataList = wileyUniversityFacade.getUniversitiesByCountryAndRegionIso(INVALID_ISOCODE,
				"US-AK");
	}

	/**
	 * Test getUniversitiesByCountryAndRegionIso method with invalid region isocode.
	 */
	@Test(expected = UnknownIdentifierException.class)
	public void getUniversitiesByCountryAndRegionIsoWithInvalidRegionIso()
	{
		List<UniversityData> universityDataList = wileyUniversityFacade.getUniversitiesByCountryAndRegionIso(US_ISOCODE,
				INVALID_ISOCODE);
	}
}
