package com.wiley.facades.wileyb2c.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.wiley.restriction.impl.WileyProductRestrictionServiceImpl;
import com.wiley.facades.wileyb2c.product.converters.populators.Wileyb2cProductAvailabilityPopulator;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cProductAvailabilityPopulatorTest
{

	@InjectMocks
	private Wileyb2cProductAvailabilityPopulator populator;

	@Mock
	private WileyProductRestrictionServiceImpl wileyProductRestrictionService;

	@Mock
	private ProductModel productModelMock;


	@Test
	public void testWhenProductIsUnavailalbe()
	{
		final ProductData data = new ProductData();

		when(wileyProductRestrictionService.isAvailable(productModelMock)).thenReturn(false);

		populator.populate(productModelMock, data);

		assertFalse(data.getAvailable());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testNullArguments()
	{
		populator.populate(null, null);
	}

}