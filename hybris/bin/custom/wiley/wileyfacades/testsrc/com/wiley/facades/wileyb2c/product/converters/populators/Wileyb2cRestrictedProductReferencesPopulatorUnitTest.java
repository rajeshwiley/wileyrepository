package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.ProductReferenceData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.wiley.restriction.WileyProductRestrictionService;


/**
 * Test for {@link Wileyb2cRestrictedProductReferencesPopulator}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cRestrictedProductReferencesPopulatorUnitTest
{
	@InjectMocks
	private Wileyb2cRestrictedProductReferencesPopulator wileyb2cRestrictedProductReferencesPopulator;

	@Mock(name = "productReferenceConverter")
	private Converter<ProductReferenceModel, ProductReferenceData> mockProductReferenceConverter;

	@Mock
	private WileyProductRestrictionService mockWileyProductRestrictionService;

	private ProductModel source;

	private ProductData target;

	private ProductReferenceModel productReferenceModel;

	private ProductModel referenceTargetProduct;

	@Before
	public void setUp() throws Exception
	{
		referenceTargetProduct = new ProductModel();
		productReferenceModel = new ProductReferenceModel();
		productReferenceModel.setTarget(referenceTargetProduct);

		target = new ProductData();
		source = new ProductModel();

	}




	@Test
	public void shouldPopulateEmptyListDueEmptyModel() throws Exception
	{
		//Given
		source.setProductReferences(Collections.EMPTY_LIST);

		//When
		wileyb2cRestrictedProductReferencesPopulator.populate(source, target);

		//Then
		assertEquals(Collections.EMPTY_LIST, target.getProductReferences());
	}

	@Test
	public void shouldPopulateEmptyListDueNull() throws Exception
	{
		//Given
		source.setProductReferences(Collections.singletonList(null));

		//When
		wileyb2cRestrictedProductReferencesPopulator.populate(source, target);

		//Then
		assertEquals(Collections.EMPTY_LIST, target.getProductReferences());
	}

	@Test
	public void shouldPopulateEmptyListDueNotVisibleTargetProduct() throws Exception
	{
		//Given
		source.setProductReferences(Collections.singletonList(productReferenceModel));
		when(mockWileyProductRestrictionService.isVisible(referenceTargetProduct)).thenReturn(Boolean.FALSE);

		//When
		wileyb2cRestrictedProductReferencesPopulator.populate(source, target);

		//Then
		assertEquals(Collections.EMPTY_LIST, target.getProductReferences());
	}


	@Test
	public void shouldPopulate() throws Exception
	{
		//Given
		source.setProductReferences(Collections.singletonList(productReferenceModel));
		when(mockWileyProductRestrictionService.isVisible(referenceTargetProduct)).thenReturn(Boolean.TRUE);
		ProductReferenceData convertedReferenceData = new ProductReferenceData();
		when(mockProductReferenceConverter.convert(productReferenceModel)).thenReturn(convertedReferenceData);
		//When
		wileyb2cRestrictedProductReferencesPopulator.populate(source, target);

		//Then
		assertEquals(Collections.singletonList(convertedReferenceData), target.getProductReferences());
	}


}
