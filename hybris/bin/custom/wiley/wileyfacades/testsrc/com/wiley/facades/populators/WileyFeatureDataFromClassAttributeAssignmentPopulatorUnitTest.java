package com.wiley.facades.populators;

import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureValue;
import de.hybris.platform.commercefacades.product.data.FeatureData;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import java.util.Collections;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.wiley.core.enums.ClassAttributeAssignmentTypeEnum;
import com.wiley.facades.product.data.MediaData;


/**
 * Created by Uladzimir_Barouski on 1/4/2017.
 */
public class WileyFeatureDataFromClassAttributeAssignmentPopulatorUnitTest
{
	private static final String CODE = "code";
	private static final String DETAILS = "Details details details";
	private static final String TOOLTIP = "Tooltip Tooltip Tooltip";
	private static final String DISPLAY_NAME = "Display Name";
	private static final String ATTRIBUTE_NAME = "Display Name";

	@InjectMocks
	WileyFeatureDataFromClassAttributeAssignmentPopulator wileyFeatureDataFromClassAttributeAssignmentPopulator;

	@Mock
	Populator<MediaModel, ImageData> imagePopulator;

	@Mock
	Converter<MediaModel, MediaData> mediaConverter;

	@Mock
	private Feature source;
	@Mock(answer = Answers.RETURNS_DEEP_STUBS)
	private ClassAttributeAssignmentModel classAttributeAssignmentModel;

	@Mock
	private MediaModel media;

	@Mock
	private MediaData document;


	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);

		given(mediaConverter.convert(media)).willReturn(document);
		given(source.getClassAttributeAssignment()).willReturn(classAttributeAssignmentModel);
		given(classAttributeAssignmentModel.getClassificationAttribute().getCode()).willReturn(CODE);
		given(classAttributeAssignmentModel.getClassificationAttribute().getName()).willReturn(ATTRIBUTE_NAME);
		given(classAttributeAssignmentModel.getType()).willReturn(
				ClassAttributeAssignmentTypeEnum.INCLUDES);
		given(classAttributeAssignmentModel.getDetails()).willReturn(DETAILS);
		given(classAttributeAssignmentModel.getMedia()).willReturn(Collections.singletonList(media));
		given(classAttributeAssignmentModel.getTooltip()).willReturn(TOOLTIP);
		given(classAttributeAssignmentModel.getDisplayName()).willReturn(DISPLAY_NAME);
		given(classAttributeAssignmentModel.getDocument()).willReturn(media);

	}

	@Test
	public void testConvert()
	{
		final FeatureValue featureValue = mock(FeatureValue.class);
		given(source.getValues()).willReturn(Collections.singletonList(featureValue));
		final FeatureData result = new FeatureData();
		wileyFeatureDataFromClassAttributeAssignmentPopulator.populate(source.getClassAttributeAssignment(), result);
		Assert.assertEquals(CODE, result.getCode());
		Assert.assertEquals(ClassAttributeAssignmentTypeEnum.INCLUDES, result.getDisplayType());
		Assert.assertEquals(DETAILS, result.getDetails());
		Assert.assertEquals(TOOLTIP, result.getTooltip());
		Assert.assertEquals(DISPLAY_NAME, result.getDisplayName());
		Assert.assertEquals(1, result.getMedia().size());
		Assert.assertEquals(document, result.getDocument());
	}
}