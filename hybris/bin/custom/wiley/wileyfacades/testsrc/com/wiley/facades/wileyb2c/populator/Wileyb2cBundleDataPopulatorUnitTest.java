package com.wiley.facades.wileyb2c.populator;

import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyBundleModel;
import com.wiley.facades.populators.WileyPriceDataFactory;
import com.wiley.facades.product.WileyProductFacade;
import com.wiley.facades.wileybundle.data.WileyBundleData;


@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cBundleDataPopulatorUnitTest
{

	private static final String ANY_FORMATTED_PRICE = "$10";
	private static final Double VERY_SMALL_DISCOUNT_VALUE = 0.00000001d;
	private static final Double NORMAL_DISCOUNT_VALUE = 25d;

	private static final String SOURCE_PRODUCT_CODE = "sourceProductCode";
	private static final BigDecimal SOURCE_PRODUCT_PRICE = BigDecimal.valueOf(11);
	private PriceData sourceProductPriceData;
	private ProductData sourceProductData;

	private static final String UPSELL_PRODUCT_CODE = "upsellProductCode";
	private static final BigDecimal UPSELL_PRODUCT_PRICE = BigDecimal.valueOf(22);
	private PriceData upsellProductPriceData;
	private ProductData upsellProductData;

	private WileyBundleModel testBundle;

	@Mock
	private WileyProductFacade wileyProductFacade;
	@Mock
	private WileyPriceDataFactory wileyPriceDataFactory;
	@Mock
	private CommonI18NService commonI18NService;

	private Wileyb2cBundleDataPopulator testInstance;

	@Before
	public void setUp() throws Exception
	{
		testInstance = new Wileyb2cBundleDataPopulator(wileyProductFacade, wileyPriceDataFactory, commonI18NService);
		sourceProductPriceData = givenPriceData(SOURCE_PRODUCT_PRICE);
		sourceProductData = givenProductData(sourceProductPriceData);
		when(wileyProductFacade.getProductForCodeAndOptions(eq(SOURCE_PRODUCT_CODE), any())).thenReturn(sourceProductData);

		upsellProductPriceData = givenPriceData(UPSELL_PRODUCT_PRICE);
		upsellProductData = givenProductData(upsellProductPriceData);
		when(wileyProductFacade.getProductForCodeAndOptions(eq(UPSELL_PRODUCT_CODE), any())).thenReturn(upsellProductData);

		when(wileyPriceDataFactory.formatPrice(any(), any())).thenReturn(ANY_FORMATTED_PRICE);

		testBundle = givenWileyBundleModel();
	}

	@Test
	public void shouldNotPopulateDiscountedValueIfItLessThanOneCent()
	{
		//Given
		testBundle.setDiscountValue(VERY_SMALL_DISCOUNT_VALUE);
		//When
		WileyBundleData bundleData = new WileyBundleData();
		testInstance.populate(testBundle, bundleData);
		//Then
		assertNull(bundleData.getDiscountedValueFormatted());
	}

	@Test
	public void shouldNotPopulatePricesWithDiscountIfDiscountValueIsMoreThatOneCent()
	{
		//Given
		testBundle.setDiscountValue(NORMAL_DISCOUNT_VALUE);
		//When
		WileyBundleData bundleData = new WileyBundleData();
		testInstance.populate(testBundle, bundleData);
		//Then
		assertNotNull(bundleData.getOriginalPriceFormatted());
		assertNotNull(bundleData.getPurchasedTogetherPriceFormatted());
		assertNotNull(bundleData.getDiscountedValueFormatted());
	}

	private WileyBundleModel givenWileyBundleModel()
	{
		WileyBundleModel wileyBundleModel = new WileyBundleModel();
		wileyBundleModel.setSourceProductCode(SOURCE_PRODUCT_CODE);
		wileyBundleModel.setUpsellProductCode(UPSELL_PRODUCT_CODE);
		return wileyBundleModel;
	}

	private ProductData givenProductData(final PriceData priceData)
	{
		ProductData productData = new ProductData();
		productData.setPrice(priceData);
		return productData;
	}

	private PriceData givenPriceData(final BigDecimal price)
	{
		PriceData priceData = new PriceData();
		priceData.setValue(price);
		return priceData;
	}

}