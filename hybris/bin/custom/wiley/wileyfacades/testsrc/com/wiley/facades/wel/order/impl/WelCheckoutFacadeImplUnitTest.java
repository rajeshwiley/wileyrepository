package com.wiley.facades.wel.order.impl;


import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commerceservices.strategies.CheckoutCustomerStrategy;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.servicelayer.user.UserService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.paypal.hybris.data.ResultErrorData;
import com.paypal.hybris.facade.PayPalCheckoutFacade;
import com.paypal.hybris.model.PaypalPaymentInfoModel;
import com.wiley.core.mpgs.services.WileyMPGSMessageService;
import com.wiley.core.mpgs.services.WileyMPGSPaymentService;
import com.wiley.facades.payment.PaymentAuthorizationResultData;

import static com.paypal.hybris.facade.data.PayPalPaymentResultData.failure;
import static com.paypal.hybris.facade.data.PayPalPaymentResultData.success;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WelCheckoutFacadeImplUnitTest
{
	@Mock
	private CartService cartServiceMock;

	@Mock
	private UserService userServiceMock;

	@Mock
	private PayPalCheckoutFacade payPalCheckoutFacadeMock;

	@Mock
	private CheckoutCustomerStrategy checkoutCustomerStrategyMock;

	@Mock
	private WileyMPGSPaymentService wileyMPGSPaymentService;

	@Mock
	private WileyMPGSMessageService wileyMPGSErrorService;

	@Mock
	private CartFacade cartFacade;

	@Spy
	@InjectMocks
	private WelCheckoutFacadeImpl wileyCheckoutFacade;

	// Test Data
	@Mock
	private CartModel cartModelMock;

	@Mock
	private CustomerModel customerModelMock;

	@Mock
	private PaypalPaymentInfoModel payPalPaymentInfoMock;

	@Mock
	private CreditCardPaymentInfoModel creditCardPaymentInfoMock;

	@Mock
	private PaymentTransactionEntryModel successfulPaymentTransactionEntryMock;

	@Mock
	private PaymentTransactionEntryModel failedPaymentTransactionEntryMock;

	private static final String TEST_ERROR_MESSAGE = "mpgs.checkout.result.authorize.timed_out";
	private static final String SECURITY_TOKEN = "";
	private static final String PAY_PAL_ERROR_1 = "payPal error 1";
	private static final String PAY_PAL_ERROR_2 = "payPal error 2";
	private static final String ACCEPTED_STATUS = "ACCEPTED";
	private static final String FAILURE_STATUS = "FAILURE";
	private static final Double PRICE = 725d;
	private static final Double ZERO_PRICE = 0d;


	@Before
	public void setUp() throws Exception
	{
		when(userServiceMock.getCurrentUser()).thenReturn(customerModelMock);
		when(cartServiceMock.getSessionCart()).thenReturn(cartModelMock);
		when(cartModelMock.getUser()).thenReturn(customerModelMock);
		when(cartModelMock.getTotalPrice()).thenReturn(PRICE);
		when(checkoutCustomerStrategyMock.getCurrentUserForCheckout()).thenReturn(customerModelMock);
		when(cartFacade.hasSessionCart()).thenReturn(true);

		when(successfulPaymentTransactionEntryMock.getTransactionStatus()).thenReturn(ACCEPTED_STATUS);
		when(failedPaymentTransactionEntryMock.getTransactionStatus()).thenReturn(FAILURE_STATUS);
	}

	private void givenCartWith(final PaymentInfoModel paymentInfoModel)
	{
		when(cartModelMock.getPaymentInfo()).thenReturn(paymentInfoModel);
	}

	private ResultErrorData givenPayPalResultErrorData(final String longErrorMessage)
	{
		final ResultErrorData resultErrorData = new ResultErrorData();
		resultErrorData.setLongMessage(longErrorMessage);
		return resultErrorData;
	}

	@Test
	public void testAuthorizePaymentAndProvideResultShouldProceedForPayPalWithSuccessResponse()
	{
		// Given
		givenCartWith(payPalPaymentInfoMock);
		when(payPalCheckoutFacadeMock.authorizePaymentAndProvideResult(SECURITY_TOKEN)).thenReturn(success());

		// When
		PaymentAuthorizationResultData authorizationResult = wileyCheckoutFacade.authorizePaymentAndProvideResult(SECURITY_TOKEN);

		// Then
		assertTrue(authorizationResult.isSuccess());

	}

	@Test
	public void testAuthorizePaymentAndProvideResultShouldProceedForPayPalWithFailureResponse()
	{
		// Given
		givenCartWith(payPalPaymentInfoMock);
		when(payPalCheckoutFacadeMock.authorizePaymentAndProvideResult(SECURITY_TOKEN)).thenReturn(
				failure(Arrays.asList(givenPayPalResultErrorData(PAY_PAL_ERROR_1), givenPayPalResultErrorData(PAY_PAL_ERROR_2))));

		// When
		PaymentAuthorizationResultData authorizationResult = wileyCheckoutFacade.authorizePaymentAndProvideResult(SECURITY_TOKEN);

		// Then
		assertEquals(PAY_PAL_ERROR_1, authorizationResult.getErrors().get(0));
		assertEquals(PAY_PAL_ERROR_2, authorizationResult.getErrors().get(1));
	}



	@Test
	public void testAuthorizePaymentAndProvideResultShouldProceedForCreditCardWithSuccessResponse()
	{
		// Given
		givenCartWith(creditCardPaymentInfoMock);
		when(wileyMPGSPaymentService.authorize(cartModelMock)).thenReturn(successfulPaymentTransactionEntryMock);

		// When
		PaymentAuthorizationResultData authorizationResult = wileyCheckoutFacade.authorizePaymentAndProvideResult(SECURITY_TOKEN);

		// Then
		assertTrue(authorizationResult.isSuccess());

	}

	@Test
	public void testAuthorizePaymentAndProvideResultShouldProceedForCreditCardWithFailureResponse()
	{
		// Given
		givenCartWith(creditCardPaymentInfoMock);
		when(wileyMPGSPaymentService.authorize(cartModelMock)).thenReturn(failedPaymentTransactionEntryMock);
		when(wileyMPGSErrorService.getAuthorizationStorefrontMessageKey(failedPaymentTransactionEntryMock)).thenReturn(
				TEST_ERROR_MESSAGE);

		// When
		PaymentAuthorizationResultData authorizationResult = wileyCheckoutFacade.authorizePaymentAndProvideResult(SECURITY_TOKEN);

		// Then
		assertFalse(authorizationResult.isSuccess());
		assertEquals(TEST_ERROR_MESSAGE, authorizationResult.getErrorCode());

	}


	@Test
	public void testAuthorizePaymentAndProvideResultForZeroTotalOrder()
	{
		// Given
		givenCartWith(creditCardPaymentInfoMock);
		when(wileyMPGSPaymentService.authorize(cartModelMock)).thenReturn(successfulPaymentTransactionEntryMock);
		when(cartModelMock.getTotalPrice()).thenReturn(ZERO_PRICE);

		// When
		PaymentAuthorizationResultData authorizationResult = wileyCheckoutFacade.authorizePaymentAndProvideResult(SECURITY_TOKEN);

		// Then
		assertTrue(authorizationResult.isSuccess());
	}
}
