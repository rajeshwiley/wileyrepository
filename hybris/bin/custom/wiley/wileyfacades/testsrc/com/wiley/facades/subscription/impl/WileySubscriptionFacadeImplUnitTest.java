package com.wiley.facades.subscription.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.subscription.services.WileySubscriptionService;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


/**
 * Unit test for {@link WileySubscriptionFacadeImpl}
 *
 * @author Aliaksei_Zlobich
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileySubscriptionFacadeImplUnitTest
{

	@Mock
	private CartService cartServiceMock;

	@Mock
	private WileySubscriptionService wileySubscriptionServiceMock;

	@InjectMocks
	private WileySubscriptionFacadeImpl wileySubscriptionFacade;

	@Test
	public void testHasCurrentUserAnySubscriptionInCartPositiveCase()
	{
		// Given
		CartModel cartModelStub = initSessionCart();

		when(wileySubscriptionServiceMock.doesAnySubscriptionProductExistInCart(cartModelStub)).thenReturn(true);

		// When
		final boolean result = wileySubscriptionFacade.hasCurrentUserAnySubscriptionInCart();

		// Then
		assertTrue("Expected that cart has subscription.", result);
	}

	@Test
	public void testHasCurrentUserAnySubscriptionInCartNegativeCase()
	{
		// Given
		CartModel cartModelStub = initSessionCart();

		when(wileySubscriptionServiceMock.doesAnySubscriptionProductExistInCart(cartModelStub)).thenReturn(false);

		// When
		final boolean result = wileySubscriptionFacade.hasCurrentUserAnySubscriptionInCart();

		// Then
		assertFalse("Expected that cart doesn't have subscription.", result);
	}

	@Test
	public void testHasCurrentUserAnySubscriptionInCartWhenCustomerHasNoSessionCart()
	{
		// Given
		when(cartServiceMock.hasSessionCart()).thenReturn(false);

		// When
		final boolean result = wileySubscriptionFacade.hasCurrentUserAnySubscriptionInCart();

		// Then
		assertFalse("Expected that cart doesn't have subscription.", result);
		verify(wileySubscriptionServiceMock, never()).doesAnySubscriptionProductExistInCart(any(CartModel.class));
	}

	private CartModel initSessionCart()
	{
		CartModel cartModelStub = new CartModel();
		when(cartServiceMock.hasSessionCart()).thenReturn(true);
		when(cartServiceMock.getSessionCart()).thenReturn(cartModelStub);
		return cartModelStub;
	}

}