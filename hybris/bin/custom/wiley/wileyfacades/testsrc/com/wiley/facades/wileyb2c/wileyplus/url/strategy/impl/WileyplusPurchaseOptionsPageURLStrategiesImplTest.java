package com.wiley.facades.wileyb2c.wileyplus.url.strategy.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.urlresolver.SiteBaseUrlResolutionService;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.site.BaseSiteService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.facades.wileyb2c.wileyplus.url.strategy.WileyplusPurchaseOptionsPageChecksumStrategy;
import com.wiley.facades.wileyb2c.wileyplus.url.strategy.exception.WileyplusChecksumMismatchException;
import com.wiley.facades.wileyplus.cart.activation.dto.CartActivationRequestDto;
import com.wiley.facades.wileyplus.cart.activation.dto.PurchaseOptionsDto;
import com.wiley.facades.wileyplus.cart.activation.dto.WebLinkDto;
import com.wiley.facades.wileyplus.cart.activation.dto.WileyPlusCourseDto;

import static com.wiley.facades.wileyb2c.wileyplus.url.strategy.WileyplusConstants.B2C_SITE_ID;
import static com.wiley.facades.wileyb2c.wileyplus.url.strategy.WileyplusConstants.PURCHASE_OPTIONS_PAGE_URL;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyplusPurchaseOptionsPageURLStrategiesImplTest
{
	private static final String FULL_PURCHASE_OPTIONS_PAGE_URL = "http://wiley.com/ucart/wileyPlus/purchase/options";
	private static final String FICE_CODE = "B03302001";
	private static final Integer GRACE_PERIOD_DURATION = 14;
	private static final String RETURN_URL = "https://wiley.instructure.com/callback";
	private static final String RETURN_URL_MESSAGE = "Return to Canvas";
	private static final String AUTHORS = "Halliday";
	private static final String IMAGE_URL = "https://wileyplus.com/abc.png";
	private static final String ISBN = "EPROF0001232123";
	private static final String PRODUCT_EDITION = "7th";
	private static final String TITLE = "Fundamentals of accounting";
	private static final String COUNTINUE_URL = "https://inegration.wiley.com/service=lti";
	private static final String COUNTINUE_URL_MESSAGE = "Continue to WileyPlus";
	private static final String CLASS_SECTION_ID = "cls12345";
	private static final String USER_ID = "usr12345";
	private static final String CURRENCY = "USD";
	private static final String LANGUAGE = "en";
	private static final String COUNTRY = "US";
	private static final String SSO_TOKEN = "abcd";
	private static final String ADDITIONAL_INFO = "AdditionalInfo";

	@Mock
	private ConfigurationService configurationService;
	@Mock
	private BaseSiteService baseSiteService;
	@Mock
	private SiteBaseUrlResolutionService siteBaseUrlResolutionService;
	@Mock
	private WileyplusPurchaseOptionsPageChecksumStrategy wileyplusPurchaseOptionsPageChecksumStrategy;
	@Mock
	private BaseSiteModel baseSiteModel;

	@InjectMocks
	private WileyplusPurchaseOptionsPageBuildURLStrategyImpl buildURLStrategy =
			new WileyplusPurchaseOptionsPageBuildURLStrategyImpl();

	@InjectMocks
	private WileyplusPurchaseOptionsPageParseURLStrategyImpl parseURLStrategy =
			new WileyplusPurchaseOptionsPageParseURLStrategyImpl();

	private final CartActivationRequestDto cartActivationRequstDto = new CartActivationRequestDto();
	private final PurchaseOptionsDto purchaseOptionsDto = new PurchaseOptionsDto();

	@Before
	public void setUp() throws WileyplusChecksumMismatchException
	{
		when(baseSiteService.getBaseSiteForUID(B2C_SITE_ID)).thenReturn(baseSiteModel);
		when(siteBaseUrlResolutionService.getWebsiteUrlForSite(baseSiteModel, true, PURCHASE_OPTIONS_PAGE_URL))
				.thenReturn(FULL_PURCHASE_OPTIONS_PAGE_URL);
		doNothing().when(wileyplusPurchaseOptionsPageChecksumStrategy).validateChecksum(anyString(), anyString());

		purchaseOptionsDto.setGracePeriodUsed(false);
		purchaseOptionsDto.setPurchaseOptionAvailable(false);
		purchaseOptionsDto.setGracePeriodDuration(null);
	}

	@Test
	public void shouldSetCartActivationParams()
			throws URISyntaxException, WileyplusChecksumMismatchException, MalformedURLException
	{
		//Given
		cartActivationRequstDto.setSsoToken(SSO_TOKEN);
		cartActivationRequstDto.setCountry(COUNTRY);
		cartActivationRequstDto.setLanguage(LANGUAGE);
		cartActivationRequstDto.setCurrency(CURRENCY);
		cartActivationRequstDto.setRegistrationRequired(true);
		cartActivationRequstDto.setUserId(USER_ID);
		cartActivationRequstDto.setFiceCode(FICE_CODE);
		cartActivationRequstDto.setClassSectionId(CLASS_SECTION_ID);

		//When
		final CartActivationRequestDto parsedDto = buildAndParseQueryParameters(cartActivationRequstDto);
		//Then
		assertEquals(SSO_TOKEN, parsedDto.getSsoToken());
		assertEquals(COUNTRY, parsedDto.getCountry());
		assertEquals(LANGUAGE, parsedDto.getLanguage());
		assertEquals(CURRENCY, parsedDto.getCurrency());
		assertTrue(CURRENCY, parsedDto.getRegistrationRequired());
		assertEquals(USER_ID, parsedDto.getUserId());
		assertEquals(FICE_CODE, parsedDto.getFiceCode());
		assertEquals(CLASS_SECTION_ID, parsedDto.getClassSectionId());
	}

	@Test
	public void shouldSetAllPurchaseOptionsToFalse()
			throws URISyntaxException, WileyplusChecksumMismatchException, MalformedURLException
	{
		//Given
		cartActivationRequstDto.setPurchaseOptionsDto(purchaseOptionsDto);
		//When
		final PurchaseOptionsDto parsedPurchaseOptionsDto =
				buildAndParseQueryParameters(cartActivationRequstDto).getPurchaseOptionsDto();
		//Then
		assertFalse(parsedPurchaseOptionsDto.getGracePeriodUsed());
		assertFalse(parsedPurchaseOptionsDto.getRegCodeActivationAvailable());
		assertFalse(parsedPurchaseOptionsDto.getPurchaseOptionAvailable());
		assertNull(parsedPurchaseOptionsDto.getGracePeriodDuration());
	}

	@Test
	public void shouldSetPurchaseOptionsToTrue()
			throws URISyntaxException, WileyplusChecksumMismatchException, MalformedURLException
	{
		//Given
		cartActivationRequstDto.setPurchaseOptionsDto(purchaseOptionsDto);
		purchaseOptionsDto.setPurchaseOptionAvailable(true);
		//When
		final CartActivationRequestDto parsedDto = buildAndParseQueryParameters(cartActivationRequstDto);
		//Then
		assertTrue(parsedDto.getPurchaseOptionsDto().getPurchaseOptionAvailable());
	}

	@Test
	public void shouldSetRegCodeActivationToTrue()
			throws URISyntaxException, WileyplusChecksumMismatchException, MalformedURLException
	{
		//Given
		cartActivationRequstDto.setPurchaseOptionsDto(purchaseOptionsDto);
		purchaseOptionsDto.setRegCodeActivationAvailable(true);
		//When
		final CartActivationRequestDto parsedDto = buildAndParseQueryParameters(cartActivationRequstDto);
		//Then
		assertTrue(parsedDto.getPurchaseOptionsDto().getRegCodeActivationAvailable());
	}

	@Test
	public void shouldSetGracePeriodDuration()
			throws URISyntaxException, WileyplusChecksumMismatchException, MalformedURLException
	{
		//Given
		cartActivationRequstDto.setPurchaseOptionsDto(purchaseOptionsDto);
		purchaseOptionsDto.setGracePeriodDuration(GRACE_PERIOD_DURATION);
		//When
		final CartActivationRequestDto parsedDto = buildAndParseQueryParameters(cartActivationRequstDto);
		//Then
		assertEquals(GRACE_PERIOD_DURATION, parsedDto.getPurchaseOptionsDto().getGracePeriodDuration());
	}

	@Test
	public void shouldSetGracePeriodUsedToTrue()
			throws URISyntaxException, WileyplusChecksumMismatchException, MalformedURLException
	{
		//Given
		cartActivationRequstDto.setPurchaseOptionsDto(purchaseOptionsDto);
		purchaseOptionsDto.setGracePeriodUsed(true);
		//When
		final CartActivationRequestDto parsedDto = buildAndParseQueryParameters(cartActivationRequstDto);
		//Then
		assertTrue(parsedDto.getPurchaseOptionsDto().getGracePeriodUsed());
	}

	@Test
	public void shouldSetReturnUrl() throws URISyntaxException, WileyplusChecksumMismatchException, MalformedURLException
	{
		//Given
		final WebLinkDto returnUrl = new WebLinkDto();
		cartActivationRequstDto.setReturnURL(returnUrl);
		returnUrl.setUrl(RETURN_URL);
		returnUrl.setMessage(RETURN_URL_MESSAGE);
		//When
		final CartActivationRequestDto parsedDto = buildAndParseQueryParameters(cartActivationRequstDto);
		//Then
		assertEquals(RETURN_URL, parsedDto.getReturnURL().getUrl());
		assertEquals(RETURN_URL_MESSAGE, parsedDto.getReturnURL().getMessage());
	}

	@Test
	public void shouldSetContinueUrl() throws URISyntaxException, WileyplusChecksumMismatchException, MalformedURLException
	{
		//Given
		final WebLinkDto continueUrl = new WebLinkDto();
		cartActivationRequstDto.setContinueURL(continueUrl);
		continueUrl.setUrl(COUNTINUE_URL);
		continueUrl.setMessage(COUNTINUE_URL_MESSAGE);
		//When
		final CartActivationRequestDto parsedDto = buildAndParseQueryParameters(cartActivationRequstDto);
		//Then
		assertEquals(COUNTINUE_URL, parsedDto.getContinueURL().getUrl());
		assertEquals(COUNTINUE_URL_MESSAGE, parsedDto.getContinueURL().getMessage());
	}

	@Test
	public void shouldSetCourseParams() throws URISyntaxException, WileyplusChecksumMismatchException, MalformedURLException
	{
		//Given
		final WileyPlusCourseDto course = new WileyPlusCourseDto();
		cartActivationRequstDto.setCourse(course);
		course.setAuthors(AUTHORS);
		course.setImageUrl(IMAGE_URL);
		course.setIsbn(ISBN);
		course.setProductEdition(PRODUCT_EDITION);
		course.setTitle(TITLE);
		//When
		final CartActivationRequestDto parsedDto = buildAndParseQueryParameters(cartActivationRequstDto);
		//Then
		assertEquals(AUTHORS, parsedDto.getCourse().getAuthors());
		assertEquals(IMAGE_URL, parsedDto.getCourse().getImageUrl());
		assertEquals(ISBN, parsedDto.getCourse().getIsbn());
		assertEquals(PRODUCT_EDITION, parsedDto.getCourse().getProductEdition());
		assertEquals(TITLE, parsedDto.getCourse().getTitle());
	}

	@Test
	public void shouldSetExtraInfo() throws URISyntaxException, WileyplusChecksumMismatchException, MalformedURLException
	{
		//Given
		cartActivationRequstDto.setAdditionalInfo(ADDITIONAL_INFO);
		//When
		final CartActivationRequestDto parsedDto = buildAndParseQueryParameters(cartActivationRequstDto);
		//Then
		assertEquals(ADDITIONAL_INFO, parsedDto.getAdditionalInfo());
	}


	private CartActivationRequestDto buildAndParseQueryParameters(final CartActivationRequestDto cartActivationRequstDto)
			throws MalformedURLException, URISyntaxException, WileyplusChecksumMismatchException

	{
		final URI uri = new URI(buildURLStrategy.buildURL(cartActivationRequstDto));
		final List<NameValuePair> params = URLEncodedUtils.parse(uri, "UTF-8");
		final String encodedCartActivationParams = params.get(0).getValue();

		return parseURLStrategy.parseCartActivationParams(encodedCartActivationParams, StringUtils.EMPTY);
	}
}