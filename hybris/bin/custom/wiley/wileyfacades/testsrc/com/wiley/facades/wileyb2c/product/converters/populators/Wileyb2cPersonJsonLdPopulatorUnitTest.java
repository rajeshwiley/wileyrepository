package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.AuthorInfoModel;
import com.wiley.facades.wileyb2c.product.data.PersonJsonLdDto;


/**
 * Unit test for Wileyb2cPersonJsonLdPopulator
 *
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cPersonJsonLdPopulatorUnitTest
{
	private static final String TYPE_VALUE = "Person";

	@Mock
	private AuthorInfoModel sourceMock;

	@InjectMocks
	private Wileyb2cPersonJsonLdPopulator populator;

	@Test
	public void populate() throws Exception
	{
		final PersonJsonLdDto target = new PersonJsonLdDto();
		final String testName = "test name";

		Mockito.when(sourceMock.getName()).thenReturn(testName);
		populator.populate(sourceMock, target);

		assertEquals(TYPE_VALUE, target.getType());
		assertEquals(testName, target.getName());
	}
}
