package com.wiley.facades.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.Arrays;
import java.util.Collections;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.category.WileyCategoryService;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ProductPartnerPopulatorUnitTest
{
	@InjectMocks
	private ProductPartnerPopulator testInstance = new ProductPartnerPopulator();
	@Mock
	private WileyCategoryService categoryService;

	private CategoryModel parentPartnersCategory;

	@Before
	public void setUp() throws Exception
	{
		parentPartnersCategory = givenParentPartnersCategory();
	}

	@Test
	public void shouldSetProductAsPartnerOnlyIfProductHasOnlyPartnerCategories() throws Exception
	{
		//Given
		final CategoryModel partnerCategory = givenPartnerCategory();
		final ProductModel product = givenProduct(
			partnerCategory
		);
		when(categoryService.getTopLevelCategoriesForCategory(partnerCategory))
				.thenReturn(Collections.singletonList(parentPartnersCategory));

		//When
		final ProductData productData = new ProductData();
		testInstance.populate(product, productData);
		//Then
		assertTrue(productData.getPartnerOnly());
	}

	@Test
	public void shouldNotSetProductAsPartnerOnlyIfProductHasAtLeastOneNonPartnerCategory() throws Exception
	{
		//Given
		final CategoryModel nonPartnerCategory = givenNonPartnerCategory();
		final CategoryModel partnerCategory = givenPartnerCategory();

		when(categoryService.getTopLevelCategoriesForCategory(partnerCategory))
				.thenReturn(Collections.singletonList(parentPartnersCategory));
		when(categoryService.getTopLevelCategoriesForCategory(nonPartnerCategory))
				.thenReturn(Collections.singletonList(nonPartnerCategory)); //nonPartnerCategory is top level

		final ProductModel product = givenProduct(
				partnerCategory,
				nonPartnerCategory
		);

		//When
		final ProductData productData = new ProductData();
		testInstance.populate(product, productData);
		//Then
		assertFalse(productData.getPartnerOnly());
	}

	private ProductModel givenProduct(final CategoryModel... categories) {
		final ProductModel product = mock(ProductModel.class);
		when(product.getSupercategories()).thenReturn(Arrays.asList(categories));
		return product;
	}

	private CategoryModel givenPartnerCategory() {
		final CategoryModel categoryModel = mock(CategoryModel.class);
		when(categoryModel.getSupercategories()).thenReturn(Collections.singletonList(parentPartnersCategory));
		when(categoryModel.getCode()).thenReturn(UUID.randomUUID().toString());
		return categoryModel;
	}

	private CategoryModel givenNonPartnerCategory() {
		final CategoryModel categoryModel = mock(CategoryModel.class);
		when(categoryModel.getCode()).thenReturn(UUID.randomUUID().toString());
		return categoryModel;
	}

	private CategoryModel givenParentPartnersCategory() {
		CategoryModel categoryModel = mock(CategoryModel.class);
		when(categoryModel.getCode()).thenReturn(ProductPartnerPopulator.WEL_PARTNERS_CATEGORY);
		return categoryModel;
	}
}