package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.commerceservices.stock.CommerceStockService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.variants.model.VariantProductModel;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.enums.ProductEditionFormat;
import com.wiley.core.enums.WileyProductLifecycleEnum;
import com.wiley.core.wiley.restriction.WileyProductRestrictionService;
import com.wiley.facades.product.data.InventoryStatusRecord;

import static de.hybris.platform.basecommerce.enums.StockLevelStatus.INSTOCK;
import static de.hybris.platform.basecommerce.enums.StockLevelStatus.OUTOFSTOCK;
import static de.hybris.platform.basecommerce.enums.StockLevelStatus.PRE_ORDER;



/**
 * Created by Uladzimir_Barouski on 6/22/2017.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cPurchaseOptionInventoryStatusPopulatorUnitTest
{
	@Mock
	private CommerceStockService stockServiceMock;
	@Mock
	private BaseStoreService baseStoreServiceMock;
	@Mock
	private WileyProductRestrictionService wileyProductRestrictionService;
	@InjectMocks
	private Wileyb2cPurchaseOptionInventoryStatusPopulator wileyb2cPurchaseOptionInventoryStatusPopulator;

	@Mock
	private VariantProductModel purchaseOptionModelMock;

	@Before
	public void setUp() throws Exception
	{
		when(baseStoreServiceMock.getCurrentBaseStore()).thenReturn(any(BaseStoreModel.class));
	}

	@Test
	public void populateInventoryStatusWhenDigitalAndNotPreorder() throws Exception
	{
		//Given
		when(stockServiceMock.getStockLevelForProductAndBaseStore(purchaseOptionModelMock, any())).thenReturn(0L);
		when(wileyProductRestrictionService.isAvailable(purchaseOptionModelMock)).thenReturn(true);
		when(purchaseOptionModelMock.getEditionFormat()).thenReturn(ProductEditionFormat.DIGITAL);
		when(purchaseOptionModelMock.getLifecycleStatus()).thenReturn(WileyProductLifecycleEnum.ACTIVE);
		VariantOptionData variantOptionData = new VariantOptionData();
		//When
		wileyb2cPurchaseOptionInventoryStatusPopulator.populate(purchaseOptionModelMock, variantOptionData);
		//Then
		assertInventoryRecord(variantOptionData.getInventoryStatus(), INSTOCK, false);
	}

	@Test
	public void populateInventoryStatusWhenAnyEditionAndPreorder() throws Exception
	{
		//Given
		when(stockServiceMock.getStockLevelForProductAndBaseStore(purchaseOptionModelMock, any())).thenReturn(0L);
		when(wileyProductRestrictionService.isAvailable(purchaseOptionModelMock)).thenReturn(true);
		when(purchaseOptionModelMock.getLifecycleStatus()).thenReturn(WileyProductLifecycleEnum.PRE_ORDER);
		VariantOptionData variantOptionData = new VariantOptionData();
		//When
		wileyb2cPurchaseOptionInventoryStatusPopulator.populate(purchaseOptionModelMock, variantOptionData);
		//Then
		assertInventoryRecord(variantOptionData.getInventoryStatus(), PRE_ORDER, true);
	}

	@Test
	public void populateInventoryStatusWhenAvailableAndQtyGreatZero() throws Exception
	{
		//Given
		when(stockServiceMock.getStockLevelForProductAndBaseStore(purchaseOptionModelMock, any())).thenReturn(1L);
		when(wileyProductRestrictionService.isAvailable(purchaseOptionModelMock)).thenReturn(true);
		VariantOptionData variantOptionData = new VariantOptionData();
		//When
		wileyb2cPurchaseOptionInventoryStatusPopulator.populate(purchaseOptionModelMock, variantOptionData);
		//Then
		assertInventoryRecord(variantOptionData.getInventoryStatus(), INSTOCK, false);
	}

	@Test
	public void populateInventoryStatusWhenAvailableAndPrintOnDemand() throws Exception
	{
		//Given
		when(stockServiceMock.getStockLevelForProductAndBaseStore(purchaseOptionModelMock, any())).thenReturn(0L);
		when(wileyProductRestrictionService.isAvailable(purchaseOptionModelMock)).thenReturn(true);
		when(purchaseOptionModelMock.getPrintOnDemand()).thenReturn(true);
		VariantOptionData variantOptionData = new VariantOptionData();
		//When
		wileyb2cPurchaseOptionInventoryStatusPopulator.populate(purchaseOptionModelMock, variantOptionData);
		//Then
		assertInventoryRecord(variantOptionData.getInventoryStatus(), INSTOCK, false);
	}

	@Test
	public void populateInventoryStatusWhenAvailable() throws Exception
	{
		//Given
		when(stockServiceMock.getStockLevelForProductAndBaseStore(purchaseOptionModelMock, any())).thenReturn(0L);
		when(wileyProductRestrictionService.isAvailable(purchaseOptionModelMock)).thenReturn(true);
		VariantOptionData variantOptionData = new VariantOptionData();
		//When
		wileyb2cPurchaseOptionInventoryStatusPopulator.populate(purchaseOptionModelMock, variantOptionData);
		//Then
		assertInventoryRecord(variantOptionData.getInventoryStatus(), OUTOFSTOCK, true);
	}

	@Test
	public void populateInventoryStatusWhenNotAvailable() throws Exception
	{
		when(stockServiceMock.getStockLevelForProductAndBaseStore(purchaseOptionModelMock, any())).thenReturn(0L);
		when(wileyProductRestrictionService.isAvailable(purchaseOptionModelMock)).thenReturn(false);
		//Given
		VariantOptionData variantOptionData = new VariantOptionData();
		//When
		wileyb2cPurchaseOptionInventoryStatusPopulator.populate(purchaseOptionModelMock, variantOptionData);
		//Then
		assertNull(variantOptionData.getInventoryStatus());
	}

	private void assertInventoryRecord(final InventoryStatusRecord inventoryStatus, final StockLevelStatus stockLevelStatus,
			final boolean isVisible)
	{
		assertNotNull(inventoryStatus);
		assertEquals(stockLevelStatus, inventoryStatus.getStatusCode());
		assertEquals(isVisible, inventoryStatus.getVisible());
		assertNull(inventoryStatus.getQuantity());
		assertNull(inventoryStatus.getAvailableDate());
	}
}