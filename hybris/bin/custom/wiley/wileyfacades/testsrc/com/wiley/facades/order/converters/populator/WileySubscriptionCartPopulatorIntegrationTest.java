package com.wiley.facades.order.converters.populator;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import org.junit.Before;

import javax.annotation.Resource;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


/**
 * Test for {@link WileySubscriptionCartPopulator}
 */
@IntegrationTest
public class WileySubscriptionCartPopulatorIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	@Resource
	private WileySubscriptionCartPopulator wileySubscriptionCartPopulator;

	@Resource
	private UserService userService;

	@Resource
	private BaseSiteService baseSiteService;

	@Resource
	private CommerceCartService commerceCartService;

	@Resource
	private CartService cartService;

	private CartModel source;

	//test data
	private static final Double SUBTOTAL_WITHOUT_DISCOUNT_TEST = 12.0;
	private static final String TEST_USER = "test-user";
	private static final String TEST_CART = "test-master";

	@Before
	public void setUp() throws Exception
	{
		// Set up test data
		importCsv("/wileyfacades/test/WileySubscriptionCartPopulatorIntegrationTest/common.impex", DEFAULT_ENCODING);

		// Set up site and user
		baseSiteService.setCurrentBaseSite("ags", true);
		userService.setCurrentUser(userService.getUserForUID(TEST_USER));

		source = initSessionCartForCurrentUser(TEST_CART);

		source.setSubTotalWithoutDiscount(SUBTOTAL_WITHOUT_DISCOUNT_TEST);
	}

	//Disabled due to fix in other branch
	//@Test
	// TODO: compare with release/v2.1
	public void testPopulate()
	{
		CartData target = new CartData();

		wileySubscriptionCartPopulator.populate(source, target);

		assertTrue(SUBTOTAL_WITHOUT_DISCOUNT_TEST.equals(target.getSubTotalWithoutDiscount().getValue().doubleValue()));
		assertEquals(2, target.getOrderPrices().size());
		assertTrue(SUBTOTAL_WITHOUT_DISCOUNT_TEST.equals(
				target.getOrderPrices().get(0).getSubTotalWithoutDiscount().getValue().doubleValue()));
		assertTrue(SUBTOTAL_WITHOUT_DISCOUNT_TEST.equals(
				target.getOrderPrices().get(1).getSubTotalWithoutDiscount().getValue().doubleValue()));
	}

	private CartModel initSessionCartForCurrentUser(final String testCart)
	{
		final CartModel cart = commerceCartService.getCartForCodeAndUser(testCart, userService.getCurrentUser());
		cartService.setSessionCart(cart);
		return cart;
	}
}