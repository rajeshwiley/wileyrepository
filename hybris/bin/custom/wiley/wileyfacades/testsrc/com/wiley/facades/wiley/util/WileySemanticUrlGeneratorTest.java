package com.wiley.facades.wiley.util;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Assert;
import org.junit.Test;


/**
 * Unit test for {@link WileySemanticUrlGenerator}
 */
@UnitTest
public class WileySemanticUrlGeneratorTest
{
	private static WileySemanticUrlGenerator wileySemanticUrlGenerator = new WileySemanticUrlGenerator();

	@Test
	public void shouldReturnEmptyStringIfInputIsNull()
	{
		final String actual = wileySemanticUrlGenerator.generateFor(null);
		Assert.assertEquals("", actual);
	}

	@Test
	public void shouldReturnEmptyStringIfInputIsEmpty()
	{
		final String actual = wileySemanticUrlGenerator.generateFor("");
		Assert.assertEquals("", actual);
	}

	@Test
	public void shouldReturnSemanticUrlInLowercase()
	{
		final String actual = wileySemanticUrlGenerator.generateFor("TestLowercase Title");
		Assert.assertEquals("testlowercase-title", actual);
	}

	@Test
	public void shouldReturnSemanticUrlWithDashesInsteadOfWhitespaces()
	{
		final String actual = wileySemanticUrlGenerator.generateFor("Test Dashes Title");
		Assert.assertEquals("test-dashes-title", actual);
	}

	@Test
	public void shouldReturnSemanticUrlWithoutEdgeDashes()
	{
		final String actual = wileySemanticUrlGenerator.generateFor(" Test Without Edge Dashes Title-");
		Assert.assertEquals("test-without-edge-dashes-title", actual);
	}

	@Test
	public void shouldReturnSemanticUrlWithoutProhibitedCharacters()
	{
		final String actual = wileySemanticUrlGenerator.generateFor("Test Without *,Prohibited Characters Title!");
		Assert.assertEquals("test-without-prohibited-characters-title", actual);
	}
}
