package com.wiley.facades.wileycom.customer.converters;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.comments.model.CommentModel;
import de.hybris.platform.commercefacades.comment.data.CommentData;
import de.hybris.platform.commercefacades.order.data.ConfigurationInfoData;
import de.hybris.platform.commercefacades.order.data.ConsignmentEntryData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.strategies.ModifiableChecker;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.order.model.AbstractOrderEntryProductInfoModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.wiley.facades.wileycom.customer.converters.populators.WileycomOrderEntryPopulator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Created by Mikhail_Asadchy on 8/10/2016.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileycomOrderEntryPopulatorTest
{

	@InjectMocks
	private WileycomOrderEntryPopulator populator;

	@Mock
	private Converter<ConsignmentEntryModel, ConsignmentEntryData> consignmentEntryConverterMock;

	@Mock
	private Converter<CommentModel, CommentData> orderCommentConverter;

	@Mock
	private AbstractOrderEntryModel sourceMock;

	@Mock
	private OrderEntryData targetMock;

	@Mock
	private ModifiableChecker<AbstractOrderEntryModel> entryOrderChecker;

	@Mock
	private Converter<ProductModel, ProductData> productConverter;

	@Mock
	private PriceDataFactory priceDataFactory;

	@Mock
	private Converter<ConsignmentEntryModel, ConsignmentEntryData> consignmentEntryConverter;

	@Mock
	private Converter<AbstractOrderEntryProductInfoModel, List<ConfigurationInfoData>> productConfigurationConverter;

	@Before
	public void setUp() throws Exception
	{
		Mockito.when(sourceMock.getOrder()).thenReturn(new AbstractOrderModel());
	}

	@Test
	public void quantityIsEqualsToZero() throws Exception
	{
		// given
		final Long quantity = 0L;
		final Long[] shippedQuantities = { 5L, 7L };


		// preparations
		doTestCase(quantity, shippedQuantities, ConsignmentStatus.CANCELLED);
	}

	@Test
	public void totalShippedQuantityIsEqualsToZero() throws Exception
	{
		// given
		final Long quantity = 5L;
		final Long[] shippedQuantities = { 0L };
		doTestCase(quantity, shippedQuantities, ConsignmentStatus.WAITING);
	}

	@Test
	public void bothOfQuantitiesAreEqualsToZero() throws Exception
	{
		// given
		final Long quantity = 0L;
		final Long[] shippedQuantities = { 0L };
		doTestCase(quantity, shippedQuantities, ConsignmentStatus.CANCELLED);
	}

	@Test
	public void totalShippedQuantityMoreThenQuantity() throws Exception
	{
		// given
		final Long quantity = 1L;
		final Long[] shippedQuantities = { 2L };
		doTestCase(quantity, shippedQuantities, ConsignmentStatus.SHIPPED);
	}

	@Test
	public void totalShippedQuantityEqualsQuantity() throws Exception
	{
		// given
		final Long quantity = 1L;
		final Long[] shippedQuantities = { 1L };
		doTestCase(quantity, shippedQuantities, ConsignmentStatus.SHIPPED);
	}

	@Test
	public void totalShippedQuantityLessThenQuantity() throws Exception
	{
		// given
		final Long quantity = 3L;
		final Long[] shippedQuantities = { 1L };
		doTestCase(quantity, shippedQuantities, ConsignmentStatus.PARTIALLY_SHIPPED);
	}

	private void doTestCase(final Long quantity, final Long[] shippedQuantities, final ConsignmentStatus status)
	{
		// preparations
		mockQuantityAndConsignmentEntries(quantity, shippedQuantities);

		// when
		populator.populate(sourceMock, targetMock);

		// then
		Mockito.verify(targetMock).setConsignmentStatus(status);
	}

	private void mockQuantityAndConsignmentEntries(final Long quantity, final Long[] shippedQuantities)
	{
		Set<ConsignmentEntryModel> consignmentEntryModels = getConsignmentEntryModels(shippedQuantities);
		Mockito.when(sourceMock.getQuantity()).thenReturn(quantity);
		Mockito.when(sourceMock.getConsignmentEntries()).thenReturn(consignmentEntryModels);
	}

	private Set<ConsignmentEntryModel> getConsignmentEntryModels(final Long[] shippedQuantities)
	{
		Set<ConsignmentEntryModel> consignmentEntryModels = new HashSet<>();
		for (Long shippedQuantity : shippedQuantities)
		{
			final ConsignmentEntryModel consignmentEntryModel = Mockito.mock(ConsignmentEntryModel.class);
			Mockito.when(consignmentEntryModel.getShippedQuantity()).thenReturn(shippedQuantity);
			consignmentEntryModels.add(consignmentEntryModel);
		}
		return consignmentEntryModels;
	}
}
