package com.wiley.facades.wel.pin.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.order.CartModel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.PinModel;
import com.wiley.facades.welags.pin.exception.PinOperationException;
import com.wiley.facades.wel.order.WelMultiDimensionalCartFacade;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Unit test for {@link WelAddToCartStrategy}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WelAddToCartStrategyTest
{
	private static final String PRODUCT_CODE = "product-code";
	private static final String CART_CODE = "cart-code";

	@Mock
	private CartModel cartModelMock;

	@Mock
	private PinModel pinModelMock;

	@Mock
	private WelMultiDimensionalCartFacade welCartFacade;

	@InjectMocks
	private WelAddToCartStrategy welAddToCartStrategy;

	@Before
	public void setup()
	{
		when(pinModelMock.getProductCode()).thenReturn(PRODUCT_CODE);
		when(cartModelMock.getCode()).thenReturn(CART_CODE);
	}

	@Test
	public void shouldAddProductToCartBeSuccessful() throws PinOperationException, CommerceCartModificationException
	{
		welAddToCartStrategy.addProductToCart(cartModelMock, pinModelMock);
		verify(welCartFacade).addOrderEntryList(eq(CART_CODE), anyObject());
	}

	@Test(expected = PinOperationException.class)
	public void shouldThrowExceptionIfAddProductToCartWasNotSuccessful() throws PinOperationException,
			CommerceCartModificationException
	{
		when(welCartFacade.addOrderEntryList(eq(CART_CODE), anyObject())).thenThrow(CommerceCartModificationException.class);

		welAddToCartStrategy.addProductToCart(cartModelMock, pinModelMock);
		verify(welCartFacade).addOrderEntryList(eq(CART_CODE), anyObject());
	}
}
