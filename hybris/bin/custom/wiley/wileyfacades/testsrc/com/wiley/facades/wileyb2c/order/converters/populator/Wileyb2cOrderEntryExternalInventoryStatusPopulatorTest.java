package com.wiley.facades.wileyb2c.order.converters.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.product.data.ExternalInventoryStatus;
import com.wiley.core.product.data.ExternalInventoryStatusRecord;
import com.wiley.facades.product.data.InventoryStatusRecord;


@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class Wileyb2cOrderEntryExternalInventoryStatusPopulatorTest
{

	@InjectMocks
	private Wileyb2cOrderEntryExternalInventoryStatusPopulator inventoryStatusPopulator;

	@Mock
	private OrderEntryData orderEntryData;
	@Mock
	private ProductData productData;

	@Test
	public void testPopulateStatuses()
	{
		// given
		final List<ExternalInventoryStatusRecord> inventoryStatusList = new ArrayList<>();

		final ExternalInventoryStatusRecord status = new ExternalInventoryStatusRecord();
		final ExternalInventoryStatusRecord status2 = new ExternalInventoryStatusRecord();

		status.setStatusCode(StockLevelStatus.INSTOCK);
		status.setAvailableDate(new Date(2017, 11, 17));
		status2.setStatusCode(StockLevelStatus.BACK_ORDER);
		status2.setAvailableDate(new Date(2017, 11, 18));

		inventoryStatusList.add(status);
		inventoryStatusList.add(status2);



		// when
		final List<InventoryStatusRecord> inventoryStatusRecords = inventoryStatusPopulator.populateStatuses(inventoryStatusList);



		// then
		assertEquals(status.getAvailableDate(), inventoryStatusRecords.get(0).getAvailableDate());
		assertEquals(status.getStatusCode(), inventoryStatusRecords.get(0).getStatusCode());

		assertEquals(status2.getAvailableDate(), inventoryStatusRecords.get(1).getAvailableDate());
		assertEquals(status2.getStatusCode(), inventoryStatusRecords.get(1).getStatusCode());

	}


	@Test
	public void testPopulate()
	{
		// given
		final List<ExternalInventoryStatusRecord> inventoryStatusList = new ArrayList<>();

		final ExternalInventoryStatusRecord status = new ExternalInventoryStatusRecord();
		final ExternalInventoryStatusRecord status2 = new ExternalInventoryStatusRecord();

		status.setStatusCode(StockLevelStatus.INSTOCK);
		status.setAvailableDate(new Date(2017, 11, 17));
		status2.setStatusCode(StockLevelStatus.BACK_ORDER);
		status2.setAvailableDate(new Date(2017, 11, 18));

		inventoryStatusList.add(status);
		inventoryStatusList.add(status2);

		final ExternalInventoryStatus externalInventoryStatus = new ExternalInventoryStatus();
		externalInventoryStatus.setEstimatedDeliveryDays(5L);
		externalInventoryStatus.setRecordsList(inventoryStatusList);

		when(orderEntryData.getProduct()).thenReturn(productData);

		// when
		inventoryStatusPopulator.populate(externalInventoryStatus, orderEntryData);



		// then
		verify(productData).setInventoryStatus(anyListOf(InventoryStatusRecord.class));
		verify(orderEntryData).setEstimatedDeliveryDays(externalInventoryStatus.getEstimatedDeliveryDays());

	}


}
