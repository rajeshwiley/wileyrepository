package com.wiley.facades.order.converters.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.subscriptionfacades.data.BillingTimeData;
import de.hybris.platform.subscriptionfacades.data.OrderPriceData;
import de.hybris.platform.subscriptionservices.model.BillingTimeModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;


/**
 * Test for {@link WileySubscriptionOrderPopulatorUtil}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileySubscriptionOrderPopulatorUtilTest
{
	private static final String BILLING_TIME_CODE = "2016/05/06";
	private static final Double SUBTOTAL_WITHOUT_DISCOUNT_TEST = 12.0;
	private static final Double TOTAL_TAX_TEST = 2.0;
	private static final Double TOTAL_PRICE_TAX = 14.0;

	private AbstractOrderModel source;
	private List<OrderPriceData> orderPrices;

	@Mock
	private PriceDataFactory priceDataFactoryMock;
	@Mock
	private CurrencyModel currencyModelMock;
	@Mock
	private PriceData priceDataTotalPriceMock;
	@Mock
	private PriceData priceDataTotalTaxMock;
	@Mock
	private PriceData priceDataTotalPriceTaxMock;
	@Mock
	private PriceData priceDataSubtotalWithoutDiscountMock;

	@InjectMocks
	private WileySubscriptionOrderPopulatorUtil populatorUtil;


	@Before
	public void setUp()
	{
		when(priceDataFactoryMock.create(PriceDataType.BUY,
				BigDecimal.valueOf(SUBTOTAL_WITHOUT_DISCOUNT_TEST),
				currencyModelMock)
		).thenReturn(priceDataSubtotalWithoutDiscountMock);

		when(priceDataFactoryMock.create(PriceDataType.BUY,
				BigDecimal.valueOf(TOTAL_PRICE_TAX),
				currencyModelMock)
		).thenReturn(priceDataTotalPriceTaxMock);

		source = new OrderModel();

		BillingTimeModel billingTimeModel = new BillingTimeModel();
		billingTimeModel.setCode(BILLING_TIME_CODE);

		source.setBillingTime(billingTimeModel);
		source.setCurrency(currencyModelMock);
		source.setSubTotalWithoutDiscount(SUBTOTAL_WITHOUT_DISCOUNT_TEST);
		source.setTotalPrice(SUBTOTAL_WITHOUT_DISCOUNT_TEST);
		source.setTotalTax(TOTAL_TAX_TEST);

		initOrderPices();

	}

	private void initOrderPices()
	{
		orderPrices = new ArrayList<>();
		OrderPriceData orderPriceData = new OrderPriceData();

		BillingTimeData billingTimeData = new BillingTimeData();
		billingTimeData.setCode(BILLING_TIME_CODE);

		orderPriceData.setBillingTime(billingTimeData);

		when(priceDataTotalPriceMock.getValue()).thenReturn(BigDecimal.valueOf(SUBTOTAL_WITHOUT_DISCOUNT_TEST));
		orderPriceData.setTotalPrice(priceDataTotalPriceMock);

		when(priceDataTotalTaxMock.getValue()).thenReturn(BigDecimal.valueOf(TOTAL_TAX_TEST));
		orderPriceData.setTotalTax(priceDataTotalTaxMock);

		orderPrices.add(orderPriceData);
	}

	@Test
	public void testFillOrderPriceDataByCustomFields()
	{
		List<OrderPriceData> result;

		result = populatorUtil.fillOrderPriceDataByCustomFields(source, orderPrices);

		assertNotNull(result);
		assertFalse(result.isEmpty());
		assertEquals(priceDataSubtotalWithoutDiscountMock, result.get(0).getSubTotalWithoutDiscount());
		assertEquals(priceDataTotalPriceTaxMock, result.get(0).getTotalPriceWithTax());
	}

}