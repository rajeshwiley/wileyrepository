package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.facades.wileyb2c.product.data.OfferJsonLdDto;
import com.wiley.facades.wileyb2c.product.data.ProductJsonLdDto;


/**
 * Unit tests for Wileyb2cProductJsonLdPopulator
 *
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cProductJsonLdPopulatorUnitTest
{
	private static final String CONTEXT_VALUE = "http://schema.org";
	private static final String TYPE_VALUE = "Book";
	private static final String DATE_FORMAT = "yyyy-MM-dd";

	@Mock
	private Converter<PriceData, OfferJsonLdDto> wileyb2cOfferJsonLdConverterMock;

	@Mock
	private ThreadLocal<Format> formatterMock;


	@InjectMocks
	private Wileyb2cProductJsonLdPopulator populator;

	@Test
	public void populateTest() throws Exception
	{
		final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);
		final ProductData source = new ProductData();
		final ProductJsonLdDto target = new ProductJsonLdDto();
		final String name = "name";
		final String isbn = "isbn";
		final Date publicationDate = new Date();
		final PriceData priceData = new PriceData();
		final List<ImageData> images = new ArrayList<>();
		final ImageData image = new ImageData();
		final String imageUrl = "imageUrl";
		final OfferJsonLdDto testOffer = new OfferJsonLdDto();

		priceData.setValue(BigDecimal.ONE);
		priceData.setCurrencyIso("USD");

		image.setUrl(imageUrl);
		images.add(image);
		images.add(new ImageData());

		source.setName(name);
		source.setIsbn13(isbn);
		source.setPublicationDate(publicationDate);
		source.setPrice(priceData);
		source.setImages(images);

		when(formatterMock.get()).thenReturn(simpleDateFormat);
		when(wileyb2cOfferJsonLdConverterMock.convert(priceData)).thenReturn(testOffer);

		populator.populate(source, target);

		assertEquals(CONTEXT_VALUE, target.getContext());
		assertEquals(TYPE_VALUE, target.getType());
		assertEquals(name, target.getName());
		assertEquals(isbn, target.getIsbn());
		assertEquals(simpleDateFormat.format(publicationDate), target.getDatePublished());
		assertEquals(testOffer, target.getOffers());
		assertEquals(imageUrl, target.getImage());
	}
}
