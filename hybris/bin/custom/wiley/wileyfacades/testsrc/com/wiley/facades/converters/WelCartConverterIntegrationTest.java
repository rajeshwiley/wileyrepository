package com.wiley.facades.converters;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;


/**
 * Test for welCartConverter
 *
 * Author Herman_Chukhrai (EPAM)
 */
@IntegrationTest
public class WelCartConverterIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	@Resource
	private CartService cartService;

	@Resource(name = "welCartConverter")
	private Converter<CartModel, CartData> cartConverter;

	@Resource
	private CommerceCartService commerceCartService;

	@Resource
	private UserService userService;

	@Resource
	private BaseSiteService baseSiteService;

	private CartModel source;

	//test data
	private static final BigDecimal SUBTOTAL_WITHOUT_DISCOUNT_TEST = new BigDecimal("12.0");
	private static final BigDecimal TOTAL_PRICE_TEST = new BigDecimal("10.5");
	private static final BigDecimal TOTAL_TAX_TEST = new BigDecimal("100.0");
	private static final boolean TAX_CALCULATED_TEST = true;
	private static final String TEST_USER = "test-user";
	private static final String TEST_CART = "test-master";
	private CartData target = new CartData();

	@Before
	public void setUp() throws Exception
	{
		assertNotNull(cartConverter);

		// Set up test data
		importCsv("/wileyfacades/test/WelCartConverterIntegrationTest/common.impex", DEFAULT_ENCODING);

		// Set up site and user
		baseSiteService.setCurrentBaseSite("wel", true);
		userService.setCurrentUser(userService.getUserForUID(TEST_USER));

		source = initSessionCartForCurrentUser(TEST_CART);
		source.setSubTotalWithoutDiscount(SUBTOTAL_WITHOUT_DISCOUNT_TEST.doubleValue());
		source.setTotalPrice(TOTAL_PRICE_TEST.doubleValue());
		source.setTotalTax(TOTAL_TAX_TEST.doubleValue());
		source.setTaxCalculated(TAX_CALCULATED_TEST);
	}

	@Test
	public void testConversion()
	{
		//when
		cartConverter.convert(source, target);

		//then
		assertNotNull(target.getSubTotalWithoutDiscount());
		assertTrue(SUBTOTAL_WITHOUT_DISCOUNT_TEST.equals(target.getSubTotalWithoutDiscount().getValue()));
		assertTrue(TOTAL_PRICE_TEST.equals(target.getTotalPrice().getValue()));
		assertNotNull(target.getTotalTax());
		assertTrue(TOTAL_TAX_TEST.equals(target.getTotalTax().getValue()));
		assertEquals(TAX_CALCULATED_TEST, target.isTaxAvailable());
	}

	private CartModel initSessionCartForCurrentUser(final String testCart)
	{
		final CartModel cart = commerceCartService.getCartForCodeAndUser(testCart, userService.getCurrentUser());
		cartService.setSessionCart(cart);
		return cart;
	}
}
