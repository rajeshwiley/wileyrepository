package com.wiley.facades.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.core.model.product.ProductModel;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyFreeTrialProductModel;
import com.wiley.core.model.WileyProductModel;
import com.wiley.core.product.impl.WileyDefaultFreeTrialProductService;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Unit test for {@link WileyFreeTrialReferencePopulator} internal logic
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyFreeTrialReferencePopulatorTest
{
	private static final String TEST_PROMPT = "test_prompt";
	private static final String TEST_URL = "test_url";
	
	@Mock
	private WileyDefaultFreeTrialProductService freeTrialProductServiceMock;

	@Mock
	private UrlResolver<ProductModel> productModelUrlResolverMock;
	
	@Mock
	private WileyFreeTrialProductModel freeTrialProductMock;

	@Mock
	private WileyProductModel sourceMock;

	@Mock
	private ProductData targetMock;

	@InjectMocks
	private WileyFreeTrialReferencePopulator freeTrialReferencePopulator;
	

	@Test
	public void testPopulateWhenFreeTrialReferencePresents()
	{
		//Given
		when(freeTrialProductServiceMock.getRelatedFreeTrialForProduct(sourceMock)).thenReturn(freeTrialProductMock);
		when(freeTrialProductMock.getPromt()).thenReturn(TEST_PROMPT);
		when(productModelUrlResolverMock.resolve(freeTrialProductMock)).thenReturn(TEST_URL);
	
		// When
		freeTrialReferencePopulator.populate(sourceMock, targetMock);

		// Then
		verify(targetMock, times(1)).setFreeTrialUrl(TEST_URL);
		verify(targetMock, times(1)).setFreeTrialButtonLabel(TEST_PROMPT);
	}
	
	@Test
	public void testPopulateWhenFreeTrialReferenceAbsent()
	{
		//Given
		when(freeTrialProductServiceMock.getRelatedFreeTrialForProduct(sourceMock)).thenReturn(null);

		// When
		freeTrialReferencePopulator.populate(sourceMock, targetMock);

		// Then
		verify(targetMock, never()).setFreeTrialUrl(any());
		verify(targetMock, never()).setFreeTrialButtonLabel(any());
	}
}