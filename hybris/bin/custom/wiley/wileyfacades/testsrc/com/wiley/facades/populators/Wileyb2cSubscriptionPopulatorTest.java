package com.wiley.facades.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.subscriptionservices.enums.SubscriptionStatus;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyProductSummaryModel;
import com.wiley.core.model.WileySubscriptionModel;
import com.wiley.facades.product.data.WileySubscriptionData;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.any;


/**
 * Created by Mikhail_Asadchy on 9/23/2016.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cSubscriptionPopulatorTest
{
	private static final String PRODUCT_NAME = "Product Name";
	private final Date startDate = new Date(0);
	private final Date nextBillingDate = new Date(1);
	private final SubscriptionStatus status = SubscriptionStatus.ACTIVE;
	private final String url = "somePictureUrl";
	@Mock
	private Converter<CreditCardPaymentInfoModel, CCPaymentInfoData> wileyPaymentInfoDataConverter;
	@InjectMocks
	private Wileyb2cSubscriptionPopulator populator;
	@Mock
	private CreditCardPaymentInfoModel paymentInfoModel;
	@Mock
	private ProductModel productModelMock;
	@Mock
	private WileySubscriptionModel wileySubscriptionModelMock;
	@Mock
	private WileyProductSummaryModel wileyProductSummaryModelMock;
	@Mock
	private AbstractOrderEntryModel abstractOrderEntryModelMock;
	@Mock
	private WileySubscriptionData dto;

	@Before
	public void setUp() throws Exception
	{
		when(wileySubscriptionModelMock.getStartDate()).thenReturn(startDate);
		when(wileySubscriptionModelMock.getStatus()).thenReturn(status);
		when(wileySubscriptionModelMock.getOrderEntry()).thenReturn(abstractOrderEntryModelMock);
		when(wileySubscriptionModelMock.getNextBillingDate()).thenReturn(nextBillingDate);
		when(productModelMock.getName()).thenReturn(PRODUCT_NAME);

		populator.setCreditCardPaymentInfoConverter(wileyPaymentInfoDataConverter);
	}

	@Test
	public void anywayFields() throws Exception
	{
		// given
		setUpProduct();
		setUpProductSummary();

		// preparations

		// when
		populator.populate(wileySubscriptionModelMock, dto);

		// then
		checkAnywayFields();
	}

	@Test
	public void productSummaryIsNull() throws Exception
	{
		// given
		setUpProduct();

		// preparations

		// when
		populator.populate(wileySubscriptionModelMock, dto);

		// then
		verify(dto).setName(PRODUCT_NAME);
		verify(dto, never()).setImage(any());
	}

	@Test
	public void checkImagePopulated() throws Exception
	{
		// given
		setUpProduct();
		setUpProductSummary();

		// preparations

		// when
		populator.populate(wileySubscriptionModelMock, dto);

		// then
		checkAnywayFields();
		verify(dto).setImage(url);
		verify(dto, never()).setPaymentInfo(any());
	}

	@Test
	public void checkPaymentInfoPopulated() throws Exception
	{
		// given
		setUpProduct();
		setUpPaymentInfo();
		setUpProductSummary();

		// preparations

		// when
		populator.populate(wileySubscriptionModelMock, dto);

		// then
		checkAnywayFields();
		verify(dto).setImage(url);
		verify(dto).setPaymentInfo(any());
	}

	@Test(expected = IllegalArgumentException.class)
	public void productIsNull() throws Exception
	{
		// given

		// preparations

		// when
		populator.populate(wileySubscriptionModelMock, dto);

		// then
		// expect exception
		verify(dto, never()).setImage(any());
		verify(dto, never()).setPaymentInfo(any());
		verify(dto, never()).setStartDate(any());
		verify(dto, never()).setStatus(any());
	}

	private void setUpProductSummary()
	{
		when(abstractOrderEntryModelMock.getProductSummary()).thenReturn(wileyProductSummaryModelMock);
		when(wileyProductSummaryModelMock.getPictureUrl()).thenReturn(url);
	}

	private void setUpProduct()
	{
		when(wileySubscriptionModelMock.getProduct()).thenReturn(productModelMock);
	}

	private void checkAnywayFields()
	{
		verify(dto).setStartDate(startDate);
		verify(dto).setNextBillingDate(nextBillingDate);
		verify(dto).setStatus(status);
	}

	private void setUpPaymentInfo()
	{
		when(wileySubscriptionModelMock.getPaymentInfo()).thenReturn(paymentInfoModel);
	}
}
