package com.wiley.facades.order.converters.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.wiley.core.product.WileyCountableProductService;

import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


/**
 * Unit test for {@link WileyQuantityAvailabilityOrderEntryPopulator}.
 *
 * @author Aliaksei_Zlobich
 */
@UnitTest
public class WileyQuantityAvailabilityOrderEntryPopulatorUnitTest
{

	@Mock
	private WileyCountableProductService wileyCountableProductService;

	@InjectMocks
	private WileyQuantityAvailabilityOrderEntryPopulator wileyQuantityAvailabilityOrderEntryPopulator;

	// test data
	@Mock
	private ProductModel productMock;

	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testPopulate()
	{
		// Given
		AbstractOrderEntryModel orderEntryMock = mock(AbstractOrderEntryModel.class);
		OrderEntryData orderEntryDataMock = mock(OrderEntryData.class);

		when(orderEntryMock.getProduct()).thenReturn(productMock);

		// When
		this.wileyQuantityAvailabilityOrderEntryPopulator.populate(orderEntryMock, orderEntryDataMock);

		// Then
		verify(wileyCountableProductService).canProductHaveQuantity(productMock);
		verify(orderEntryDataMock).setDoesProductHaveQuantity(anyBoolean());
	}
}