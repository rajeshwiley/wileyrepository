package com.wiley.facades.wileycom.customer.converters;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.ConsignmentData;
import de.hybris.platform.commercefacades.order.data.ConsignmentEntryData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import com.wiley.facades.wileycom.customer.converters.populators.WileycomConsignmentEntryPopulator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Created by Mikhail_Asadchy on 8/12/2016.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileycomConsignmentEntryPopulatorTest
{
	@InjectMocks
	private WileycomConsignmentEntryPopulator populator;

	@Mock
	private ConsignmentEntryModel sourceMock;

	@Mock
	private ConsignmentEntryData targetMock;

	@Mock
	private Converter<ConsignmentModel, ConsignmentData> consignmentConverter;

	@Mock
	private Converter<AbstractOrderEntryModel, OrderEntryData> orderEntryConverter;

	@Test
	public void shouldPopulateConsignment() throws Exception
	{
		// given

		// preparations

		// when
		populator.populate(sourceMock, targetMock);

		// then
		Mockito.verify((targetMock)).setConsignment(Mockito.any(ConsignmentData.class));
	}
}
