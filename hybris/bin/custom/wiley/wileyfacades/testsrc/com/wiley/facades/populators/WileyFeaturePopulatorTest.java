package com.wiley.facades.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.commercefacades.product.data.FeatureData;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;



@UnitTest
public class WileyFeaturePopulatorTest
{

	private static final String DETAILS = "Details details details";
	private static final String TOOLTIP = "Tooltip Tooltip Tooltip";
	private static final String DISPLAY_NAME = "Display Name";

	@InjectMocks
	WileyFeaturePopulator wileyFeaturePopulator;

	@Mock
	Converter<ClassAttributeAssignmentModel, FeatureData> wileyFeatureFromAssignmentConverter;

	@Mock
	private Feature source;
	@Mock
	private ClassAttributeAssignmentModel classAttributeAssignmentModel;


	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		given(source.getClassAttributeAssignment()).willReturn(classAttributeAssignmentModel);
		doReturn(wileyFeatureFromAssignmentConverter.getClass()).when(wileyFeatureFromAssignmentConverter).convert(
				any(ClassAttributeAssignmentModel.class),
				any(FeatureData.class));
	}

	@Test
	public void testConvert()
	{
		final FeatureData feature = new FeatureData();
		wileyFeaturePopulator.populate(source, feature);
		verify(wileyFeatureFromAssignmentConverter, times(1)).convert(eq(classAttributeAssignmentModel), eq(feature));
	}

}
