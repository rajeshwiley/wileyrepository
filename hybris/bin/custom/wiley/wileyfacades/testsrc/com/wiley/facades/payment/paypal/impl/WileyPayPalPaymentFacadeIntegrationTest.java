package com.wiley.facades.payment.paypal.impl;

import com.ebay.api.AbstractRequestType;
import com.paypal.hybris.data.AbstractRequestData;
import com.paypal.hybris.facade.impl.PayPalPaymentFacade;
import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.converters.impl.AbstractPopulatingConverter;
import de.hybris.platform.core.Registry;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import org.junit.Test;

import javax.annotation.Resource;

import static org.junit.Assert.assertTrue;

/**
 * Created by gw on 1/12/2017.
 */
@IntegrationTest
public class WileyPayPalPaymentFacadeIntegrationTest
    extends ServicelayerTransactionalTest
{

    @Resource
    private PayPalPaymentFacade payPalPaymentFacade;

    @Test
    public void testCheckWileySetExprCheckoutReqPaymentDetailsPopulator()
    {
        AbstractPopulatingConverter<AbstractRequestData, AbstractRequestType> setExprCheckoutReqDataConverter =
                (AbstractPopulatingConverter) payPalPaymentFacade.getSetExprCheckoutReqDataConverter();
        assertTrue(setExprCheckoutReqDataConverter.getPopulators()
                .contains(Registry.getApplicationContext().
                        getBean("wileySetExprCheckoutReqPaymentDetailsPopulator")));
    }
}
