/**
 *
 */
package com.wiley.facades.order.converters.populator;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.WileyPaymentInfoData;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.enumeration.EnumerationService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * @author nitingakhar
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyPaymentInfoPopulatorTest
{

	private static final String CREDIT_CARD_NUM = "1234";
	private static final String CREDIT_CARD_TYPE_NAME = CreditCardType.VISA.name();

	@InjectMocks
	private WileyPaymentInfoPopulator testInstance;

	@Mock
	private CreditCardPaymentInfoModel creditCardPaymentInfoModelMock;

	@Mock
	private WileyPaymentInfoData wileyPaymentInfoDataMock;

	@Mock
	private EnumerationService enumerationServiceMock;

	@Test
	public void testPopulateCreditCardPaymentInfo()
	{
		// given
		when(creditCardPaymentInfoModelMock.getNumber()).thenReturn(CREDIT_CARD_NUM);
		when(creditCardPaymentInfoModelMock.getType()).thenReturn(CreditCardType.VISA);
		when(enumerationServiceMock.getEnumerationValue(CreditCardType.class, CREDIT_CARD_TYPE_NAME))
				.thenReturn(CreditCardType.VISA);

		// when
		testInstance.populate(creditCardPaymentInfoModelMock, wileyPaymentInfoDataMock);

		// then
		verify(wileyPaymentInfoDataMock).setCreditCardNumber(CREDIT_CARD_NUM);
		verify(wileyPaymentInfoDataMock).setCreditCardType(CREDIT_CARD_TYPE_NAME);
	}
}
