package com.wiley.facades.wileycom.customer.converters;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.commercefacades.order.data.ConsignmentData;
import de.hybris.platform.commercefacades.order.data.ConsignmentEntryData;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.Date;

import com.wiley.facades.wileycom.customer.converters.populators.WileycomConsignmentPopulator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Created by Mikhail_Asadchy on 8/12/2016.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileycomConsignmentPopulatorTest
{
	@InjectMocks
	private WileycomConsignmentPopulator populator;

	@Mock
	private ConsignmentModel sourceMock;

	@Mock
	private ConsignmentData targetMock;

	@Mock
	private Converter<ConsignmentEntryModel, ConsignmentEntryData> consignmentEntryConverter;

	@Mock
	private Converter<PointOfServiceModel, PointOfServiceData> pointOfServiceConverter;

	@Mock
	private Converter<AddressModel, AddressData> addressConverter;

	@Test
	public void shouldPopulateFieldsInAnyCase() throws Exception
	{
		// given

		// preparations

		// when
		populator.populate(sourceMock, targetMock);

		// then
		Mockito.verify((targetMock)).setCode(Mockito.anyString());
		Mockito.verify((targetMock)).setTrackingID(Mockito.anyString());
		Mockito.verify((targetMock)).setStatus(Mockito.anyObject());
		Mockito.verify((targetMock), Mockito.times(0)).setEntries(Matchers.anyList());
	}

	@Test
	public void shouldPopulateStatusDate() throws Exception
	{
		// given
		final ConsignmentStatus consignmentStatus = ConsignmentStatus.SHIPPED;

		// preparations
		Mockito.when(sourceMock.getStatus()).thenReturn(consignmentStatus);

		// when
		populator.populate(sourceMock, targetMock);

		// then
		Mockito.verify((targetMock)).setStatusDate(Mockito.any(Date.class));
	}

	@Test
	public void shouldPopulatePointOfServiceData() throws Exception
	{
		// given
		final PointOfServiceModel model = Mockito.mock(PointOfServiceModel.class); // model not null

		// preparations
		Mockito.when(sourceMock.getDeliveryPointOfService()).thenReturn(model);

		// when
		populator.populate(sourceMock, targetMock);

		// then
		Mockito.verify((targetMock)).setDeliveryPointOfService(Mockito.any(PointOfServiceData.class));
	}

	@Test
	public void shouldPopulateShippingAddressField() throws Exception
	{
		// given
		final AddressModel model = Mockito.mock(AddressModel.class); // model not null

		// preparations
		Mockito.when(sourceMock.getShippingAddress()).thenReturn(model);

		// when
		populator.populate(sourceMock, targetMock);

		// then
		Mockito.verify((targetMock)).setShippingAddress(Mockito.any(AddressData.class));
	}
}
