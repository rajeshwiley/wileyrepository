package com.wiley.facades.wileyb2c.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.subscriptionfacades.converters.populator.SubscriptionTermPopulator;
import de.hybris.platform.subscriptionfacades.data.SubscriptionTermData;
import de.hybris.platform.subscriptionservices.model.SubscriptionTermModel;
import de.hybris.platform.util.PriceValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyProductModel;
import com.wiley.core.model.WileySubscriptionModel;
import com.wiley.core.wileyb2c.product.Wileyb2cCommercePriceService;
import com.wiley.facades.product.data.WileySubscriptionData;

import static java.util.Collections.singletonList;


/**
 * Author Herman_Chukhrai (EPAM)
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cAvailableUpgradeSubscriptionTermsPopulatorTest
{

	@InjectMocks
	private Wileyb2cAvailableUpgradeSubscriptionTermsPopulator subscriptionTermsPopulatorMock;
	@Mock
	private WileySubscriptionModel wileySubscriptionModelMock;
	@Mock
	private AbstractOrderEntryModel orderEntryModelMock;
	@Mock
	private SubscriptionTermModel subscriptionTermModelMock;
	@Mock
	private SubscriptionTermModel availableSubscriptionTermModelMock;
	@Mock
	private ProductService productServiceMock;
	@Mock
	private WileyProductModel wileyProductModelMock;
	@Mock
	private SubscriptionTermPopulator subscriptionTermPopulatorMock;
	@Mock
	private Wileyb2cCommercePriceService priceServiceMock;
	@Mock
	private PriceInformation priceInformationMock;
	@Mock
	private PriceValue priceValueMock;
	@Mock
	private PriceDataFactory priceDataFactoryMock;

	private static final String PRODUCT_CODE = "PRODUCT_CODE";
	private static final String SUBSCRIPTION_TERM_ID = "SUBSCRIPTION_TERM_ID";
	private static final String AVAILABLE_SUBSCRIPTION_TERM_ID = "AVAILABLE_SUBSCRIPTION_TERM_ID";
	private static final double PRICE_VALUE = 100.0d;
	private static final String CURRENCY_ISO = "USD";

	@Before
	public void setUp() throws Exception
	{
	}

	@Test
	public void testPopulateSingleAvailable() throws Exception
	{
		//Given
		WileySubscriptionData wileySubscriptionData = new WileySubscriptionData();
		Set<SubscriptionTermModel> set = new HashSet<>();
		set.add(subscriptionTermModelMock);
		set.add(availableSubscriptionTermModelMock);
		wileySubscriptionData.setProductCode(PRODUCT_CODE);

		when(wileySubscriptionModelMock.getOrderEntry()).thenReturn(orderEntryModelMock);
		when(orderEntryModelMock.getSubscriptionTerm()).thenReturn(subscriptionTermModelMock);
		when(productServiceMock.getProductForCode(PRODUCT_CODE)).thenReturn(wileyProductModelMock);
		when(wileyProductModelMock.getSubscriptionTerms()).thenReturn(set);
		when(subscriptionTermModelMock.getId()).thenReturn(SUBSCRIPTION_TERM_ID);
		when(availableSubscriptionTermModelMock.getId()).thenReturn(AVAILABLE_SUBSCRIPTION_TERM_ID);
		when(priceServiceMock.findPricesByProductAndSubscriptionTerm(wileyProductModelMock, availableSubscriptionTermModelMock))
				.thenReturn(singletonList(priceInformationMock));
		when(priceInformationMock.getPriceValue()).thenReturn(priceValueMock);
		when(priceValueMock.getValue()).thenReturn(PRICE_VALUE);
		when(priceValueMock.getCurrencyIso()).thenReturn(CURRENCY_ISO);
		final PriceData priceData = new PriceData();
		priceData.setValue(new BigDecimal(priceValueMock.getValue()));
		priceData.setCurrencyIso(priceValueMock.getCurrencyIso());
		when(priceDataFactoryMock
				.create(PriceDataType.FROM, new BigDecimal(priceValueMock.getValue()), priceValueMock.getCurrencyIso()))
				.thenReturn(priceData);

		//When
		subscriptionTermsPopulatorMock.populate(wileySubscriptionModelMock, wileySubscriptionData);

		//Then
		final List<SubscriptionTermData> availableTerms =
				wileySubscriptionData.getAvailableUpgradeSubscriptionTerms();
		assertTrue(CollectionUtils.isNotEmpty(availableTerms));
		final SubscriptionTermData subscriptionTermData = availableTerms.get(0);
		assertEquals(AVAILABLE_SUBSCRIPTION_TERM_ID, subscriptionTermData.getId());
		final PriceData price = subscriptionTermData.getPrice();
		assertNotNull(price);
		assertEquals(PRICE_VALUE, price.getValue().doubleValue(), Double.MIN_VALUE);
		assertEquals(CURRENCY_ISO, price.getCurrencyIso());
	}

	@Test
	public void testPopulateNoAvailable() throws Exception
	{
		//Given
		WileySubscriptionData wileySubscriptionData = new WileySubscriptionData();
		Set<SubscriptionTermModel> set = new HashSet<>();
		set.add(subscriptionTermModelMock);
		set.add(availableSubscriptionTermModelMock);
		wileySubscriptionData.setProductCode(PRODUCT_CODE);

		when(wileySubscriptionModelMock.getOrderEntry()).thenReturn(orderEntryModelMock);
		when(orderEntryModelMock.getSubscriptionTerm()).thenReturn(subscriptionTermModelMock);
		when(productServiceMock.getProductForCode(PRODUCT_CODE)).thenReturn(wileyProductModelMock);
		when(wileyProductModelMock.getSubscriptionTerms()).thenReturn(set);
		when(subscriptionTermModelMock.getId()).thenReturn(SUBSCRIPTION_TERM_ID);

		//When
		subscriptionTermsPopulatorMock.populate(wileySubscriptionModelMock, wileySubscriptionData);

		//Then
		final List<SubscriptionTermData> availableTerms =
				wileySubscriptionData.getAvailableUpgradeSubscriptionTerms();
		assertTrue(CollectionUtils.isEmpty(availableTerms));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testNullModelProvided() throws Exception
	{
		//When
		subscriptionTermsPopulatorMock.populate(null, new WileySubscriptionData());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testNullDataProvided() throws Exception
	{
		//When
		subscriptionTermsPopulatorMock.populate(wileySubscriptionModelMock, null);
	}
}