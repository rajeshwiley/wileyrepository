package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;

import static org.mockito.Mockito.when;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.AuthorInfoModel;
import com.wiley.facades.wileyb2c.product.converters.populators.helper.Wileyb2cProductCollectionAttributesHelper;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cEncodedAuthorsListPopulatorUnitTest
{
	private static final String AUTHOR_1 = "Name* Test-Author._Lastname";
	private static final String AUTHOR_2 = "Nȁmȇ Test_Aȕthoȓ";

	private static final String ENCODED_AUTHOR_1 = "Name*+Test-Author._Lastname";
	private static final String ENCODED_AUTHOR_2 = "N%C8%81m%C8%87+Test_A%C8%95tho%C8%93";

	@InjectMocks
	private Wileyb2cEncodedAuthorsListPopulator wileyb2cEncodedAuthorsListPopulator;

	@Mock
	private Wileyb2cProductCollectionAttributesHelper wileyb2cProductCollectionAttributesHelper;

	@Mock
	private ProductModel productModel;

	private ProductData productData;

	@Mock
	private AuthorInfoModel author1;

	@Mock
	private AuthorInfoModel author2;

	@Before
	public void setUp()
	{
		when(author1.getName()).thenReturn(AUTHOR_1);
		when(author2.getName()).thenReturn(AUTHOR_2);

		when(wileyb2cProductCollectionAttributesHelper.getProductAttribute(
				productModel, ProductModel.AUTHORINFOS)).thenReturn(Arrays.asList(author1, author2));
		wileyb2cEncodedAuthorsListPopulator
				.setWileyb2cProductCollectionAttributesHelper(wileyb2cProductCollectionAttributesHelper);
	}

	@Test
	public void testPopulate()
	{
		productData = new ProductData();

		wileyb2cEncodedAuthorsListPopulator.populate(productModel, productData);

		assertNotNull(productData.getEncodedAuthorsList());
		assertEquals(2, productData.getEncodedAuthorsList().size());
		assertEquals(ENCODED_AUTHOR_1, productData.getEncodedAuthorsList().get(0));
		assertEquals(ENCODED_AUTHOR_2, productData.getEncodedAuthorsList().get(1));
	}

	@Test
	public void testPopulateWithNullAuthors()
	{
		productData = new ProductData();
		when(wileyb2cProductCollectionAttributesHelper.getProductAttribute(
				productModel, ProductModel.AUTHORINFOS)).thenReturn(null);

		wileyb2cEncodedAuthorsListPopulator.populate(productModel, productData);

		assertTrue(productData.getEncodedAuthorsList().isEmpty());
	}
}
