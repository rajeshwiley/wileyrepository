package com.wiley.facades.wileyb2c.product.converters.populators;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.wileyb2c.classification.Wileyb2cClassificationAttributes;
import com.wiley.core.wileyb2c.classification.Wileyb2cClassificationService;


/**
 * Created by Uladzimir_Barouski on 2/28/2017.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cProductDetailsPopulatorUnitTest
{

	private static final String TEST_ISBN_FORMATTED = "111-1-111-11111-1";
	private static final Date TEST_DATE = new Date();
	private static final String TEST_NUMBER_OF_PAGES = "99";
	private static final String TEST_P_ISSN_FORMATTED = "1111-1111";
	private static final String TEST_O_ISSN_FORMATTED = "2222-2222";
	private static final String TEST_VOLUME = "test volume and issues";
	private static final String TEST_FACTOR_FORMATTED = "12345.670";
	private static final List<String> TEST_SOCIETY_LIST = Arrays.asList("societyLink");

	@InjectMocks
	private Wileyb2cProductDetailsPopulator wileyb2cProductDetailsPopulator;

	@Mock
	private Wileyb2cClassificationService wileyb2cClassificationService;

	@Mock
	private ProductModel productModelMock;


	@Test
	public void populate() throws Exception
	{
		//Given
		when(wileyb2cClassificationService.resolveAttribute(productModelMock, Wileyb2cClassificationAttributes.ISBN13))
				.thenReturn(
						TEST_ISBN_FORMATTED);
		when(wileyb2cClassificationService
				.resolveAttributeValue(productModelMock, Wileyb2cClassificationAttributes.PUBLICATION_DATE))
				.thenReturn(
						TEST_DATE);
		when(wileyb2cClassificationService.resolveAttribute(productModelMock, Wileyb2cClassificationAttributes.NUMBER_OF_PAGES))
				.thenReturn(
						TEST_NUMBER_OF_PAGES);
		when(wileyb2cClassificationService.resolveAttribute(productModelMock, Wileyb2cClassificationAttributes.PRINT_ISSN))
				.thenReturn(
						TEST_P_ISSN_FORMATTED);
		when(wileyb2cClassificationService.resolveAttribute(productModelMock, Wileyb2cClassificationAttributes.ONLINE_ISSN))
				.thenReturn(
						TEST_O_ISSN_FORMATTED);
		when(wileyb2cClassificationService.resolveAttribute(productModelMock, Wileyb2cClassificationAttributes.VOLUME_AND_ISSUE))
				.thenReturn(
						TEST_VOLUME);
		when(wileyb2cClassificationService.resolveAttribute(productModelMock, Wileyb2cClassificationAttributes.IMPACT_FACTOR))
				.thenReturn(
						TEST_FACTOR_FORMATTED);
		when(wileyb2cClassificationService.resolveAttributeValue(productModelMock, Wileyb2cClassificationAttributes.SOCIETY_LINK))
				.thenReturn(
						TEST_SOCIETY_LIST);
		final ProductData productData = new ProductData();

		//When
		wileyb2cProductDetailsPopulator.populate(productModelMock, productData);

		//Then
		assertEquals(TEST_ISBN_FORMATTED, productData.getIsbn13());
		assertEquals(TEST_DATE, productData.getPublicationDate());
		assertEquals(TEST_NUMBER_OF_PAGES, productData.getPagesNumber());
		assertEquals(TEST_VOLUME, productData.getVolumeAndIssues());
		assertEquals(TEST_O_ISSN_FORMATTED, productData.getOnlineIssn());
		assertEquals(TEST_P_ISSN_FORMATTED, productData.getPrintIssn());
		assertEquals(TEST_FACTOR_FORMATTED, productData.getImpactFactor());
		assertEquals(TEST_SOCIETY_LIST, productData.getSocietyLink());
	}

}