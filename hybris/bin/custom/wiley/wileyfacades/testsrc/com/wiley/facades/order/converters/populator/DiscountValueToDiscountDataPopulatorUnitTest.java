package com.wiley.facades.order.converters.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.util.DiscountValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.facades.product.data.DiscountData;


/**
 * Default unit test for {@link DiscountValueToDiscountDataPopulator}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DiscountValueToDiscountDataPopulatorUnitTest
{

	@Mock
	private CommerceCommonI18NService commerceCommonI18NServiceMock;

	@Mock
	private PriceDataFactory priceDataFactoryMock;

	@InjectMocks
	private DiscountValueToDiscountDataPopulator discountValueToDiscountDataPopulator;

	// Test data
	@Mock
	private DiscountValue discountValueMock;

	@Mock
	private CurrencyModel currencyModelMock;

	@Mock
	private PriceData priceDataMock;

	@Before
	public void setUp() throws Exception
	{
		when(priceDataFactoryMock.create(any(PriceDataType.class), any(BigDecimal.class), eq(currencyModelMock))).thenReturn(
				priceDataMock);
	}

	@Test
	public void testPopulateRelativeDiscount()
	{
		// Given
		when(commerceCommonI18NServiceMock.getCurrentCurrency()).thenReturn(currencyModelMock);

		final Double discountValue = 25.0;
		final Boolean isAbsolute = false;
		mockDiscountValue(discountValue, isAbsolute);
		DiscountData discountData = new DiscountData();

		// When
		discountValueToDiscountDataPopulator.populate(discountValueMock, discountData);

		// Then
		assertEquals("[Absolute] attribute should equal expected value.", isAbsolute, discountData.getAbsolute());
		assertEquals("Discount value should equal expected one.", discountValue, discountData.getRelativeValue());
	}

	@Test
	public void testPopulateAbsoluteDiscount()
	{
		// Given
		when(commerceCommonI18NServiceMock.getCurrentCurrency()).thenReturn(currencyModelMock);

		final Double discountValue = 235.5;
		final Boolean isAbsolute = true;
		mockDiscountValue(discountValue, isAbsolute);
		DiscountData discountData = new DiscountData();

		// When
		discountValueToDiscountDataPopulator.populate(discountValueMock, discountData);

		// Then
		assertEquals("[Absolute] attribute should equal expected value.", isAbsolute, discountData.getAbsolute());
		assertSame("Discount.price should be expected object.", priceDataMock, discountData.getPrice());
	}

	@Test
	public void testPopulateWithDefaultCurrency()
	{
		// Given
		when(commerceCommonI18NServiceMock.getCurrentCurrency()).thenReturn(null);
		when(commerceCommonI18NServiceMock.getDefaultCurrency()).thenReturn(currencyModelMock);

		final Double discountValue = 235.5;
		final Boolean isAbsolute = true;
		mockDiscountValue(discountValue, isAbsolute);
		DiscountData discountData = new DiscountData();

		// When
		discountValueToDiscountDataPopulator.populate(discountValueMock, discountData);

		// Then
		assertEquals("[Absolute] attribute should equal expected value.", isAbsolute, discountData.getAbsolute());
		assertSame("Discount.price should be expected object.", priceDataMock, discountData.getPrice());
		verify(commerceCommonI18NServiceMock).getCurrentCurrency();
		verify(commerceCommonI18NServiceMock).getDefaultCurrency();
	}

	private void mockDiscountValue(final Double discountValue, final Boolean isAbsolute)
	{
		when(discountValueMock.isAbsolute()).thenReturn(isAbsolute);
		when(discountValueMock.getValue()).thenReturn(discountValue);
	}


}