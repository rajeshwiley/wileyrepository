package com.wiley.facades.payment.paypal.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.facades.payment.impl.WileyPayPalPaymentFacadeImpl;
import com.wiley.facades.wileyb2c.order.Wileyb2cCheckoutFacade;


/**
 * Unit test for {@link WileyPayPalPaymentFacadeImpl}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyPayPalPaymentFacadeImplTest
{

	@InjectMocks
	private WileyPayPalPaymentFacadeImpl wileyb2cPayPalPaymentFacade;

	@Mock
	private Wileyb2cCheckoutFacade wileyb2cCheckoutFacade;

	@Mock
	private UserFacade userFacade;

	@Mock
	private CartData cartDataMock;

	@Mock
	private AddressData addressDataMock;

	@Mock
	private AbstractOrderEntryModel mockOrderEntryModel;

	@Before
	public void setUp()
	{
		when(wileyb2cCheckoutFacade.getCheckoutCart()).thenReturn(cartDataMock);
	}

	@Test
	public void testRemoveDeliveryAddressWhenAddressIsNull()
	{
		//Given
		when(cartDataMock.getDeliveryAddress()).thenReturn(null);

		//When
		wileyb2cPayPalPaymentFacade.removeDeliveryAddressFromCart();

		//Then
		verify(userFacade, never()).removeAddress(any(AddressData.class));
		verify(wileyb2cCheckoutFacade, times(1)).removeDeliveryAddress();
	}

	@Test
	public void testRemoveDeliveryAddressWhenAddressIsVisibleInAddressBook()
	{
		//Given
		when(cartDataMock.getDeliveryAddress()).thenReturn(addressDataMock);
		when(addressDataMock.isVisibleInAddressBook()).thenReturn(Boolean.TRUE);

		//When
		wileyb2cPayPalPaymentFacade.removeDeliveryAddressFromCart();

		//Then
		verify(userFacade, never()).removeAddress(any(AddressData.class));
		verify(wileyb2cCheckoutFacade, times(1)).removeDeliveryAddress();
	}

	@Test
	public void testRemoveDeliveryAddressWhenAddressIsNotVisibleInAddressBook()
	{
		//Given
		when(cartDataMock.getDeliveryAddress()).thenReturn(addressDataMock);
		when(addressDataMock.isVisibleInAddressBook()).thenReturn(Boolean.FALSE);

		//When
		wileyb2cPayPalPaymentFacade.removeDeliveryAddressFromCart();

		//Then
		verify(userFacade, times(1)).removeAddress(addressDataMock);
		verify(wileyb2cCheckoutFacade, times(1)).removeDeliveryAddress();
	}

}
