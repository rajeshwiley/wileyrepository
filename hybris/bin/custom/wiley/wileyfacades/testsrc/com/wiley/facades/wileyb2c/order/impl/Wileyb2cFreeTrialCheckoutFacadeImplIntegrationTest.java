package com.wiley.facades.wileyb2c.order.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.subscriptionservices.model.SubscriptionTermModel;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.model.WileyProductSummaryModel;
import com.wiley.core.servicelayer.i18n.WileycomI18NService;


/**
 * Created by Uladzimir_Barouski on 10/6/2016.
 */
@IntegrationTest
public class Wileyb2cFreeTrialCheckoutFacadeImplIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{

	private static final String BASE_SITE = "wileyb2c";
	private static final String CATALOG_ID = "wileyProductCatalog";
	private static final String CATALOG_VERSION = "Online";
	private static final String USER_ID = "customer@test.com";
	public static final String FREE_TRIAL_TEST1 = "FreeTrialTest1";
	public static final String FREE_TRIAL_TEST2 = "FreeTrialTest2";
	private static final String TITLE = "Free Trial 1 name";
	public static final String FREE_TRIAL_TERM_ID = "free_15D";


	@Resource
	private Wileyb2cFreeTrialCheckoutFacadeImpl wileyb2cFreeTrialCheckoutFacade;
	@Resource
	private BaseSiteService baseSiteService;
	@Resource
	private UserService userService;
	@Resource
	private CatalogVersionService catalogVersionService;
	@Resource
	private CustomerAccountService customerAccountService;
	@Resource
	private BaseStoreService baseStoreService;
	@Resource
	private WileycomI18NService wileycomI18NService;

	@Before
	public void setUp() throws Exception
	{
		baseSiteService.setCurrentBaseSite(baseSiteService.getBaseSiteForUID(BASE_SITE), true);
		userService.setCurrentUser(userService.getUserForUID(USER_ID));
		catalogVersionService.setSessionCatalogVersion(CATALOG_ID, CATALOG_VERSION);
		wileycomI18NService.setDefaultCurrentCountry();
		importCsv("/wileyfacades/test/Wileyb2cFreeTrialCheckoutFacadeImplIntegrationTest/testdata.impex", DEFAULT_ENCODING);
	}

	@Test
	public void testSubmitOrder() throws CommerceCartModificationException, InvalidCartException
	{
		String orderCode = wileyb2cFreeTrialCheckoutFacade.placeFreeTrialOrder(FREE_TRIAL_TEST1);
		OrderModel order = customerAccountService.getOrderForCode(orderCode, baseStoreService.getCurrentBaseStore());

		assertNotNull(order);
		verifyOrderEntry(order);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSubmitOrderWithoutFreeTrialTerm() throws CommerceCartModificationException, InvalidCartException
	{
		String orderCode = wileyb2cFreeTrialCheckoutFacade.placeFreeTrialOrder(FREE_TRIAL_TEST2);
		OrderModel order = customerAccountService.getOrderForCode(orderCode, baseStoreService.getCurrentBaseStore());
	}

	private void verifyOrderEntry(final OrderModel order)
	{
		List<AbstractOrderEntryModel> entries = order.getEntries();
		assertNotNull(entries);
		assertEquals(1, entries.size());

		AbstractOrderEntryModel entry = entries.get(0);
		assertEquals(FREE_TRIAL_TEST1, entry.getProduct().getCode());
		SubscriptionTermModel subscriptionTermModel = entry.getSubscriptionTerm();
		assertNotNull(subscriptionTermModel);
		assertEquals(FREE_TRIAL_TERM_ID, subscriptionTermModel.getId());

		WileyProductSummaryModel productSummary = entry.getProductSummary();
		assertEquals(TITLE, productSummary.getName());
	}
}