package com.wiley.facades.wileyb2c.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileySubscriptionModel;
import com.wiley.core.util.TestUtils;
import com.wiley.facades.product.data.WileySubscriptionData;


/**
 * Default unit test for {@link Wileyb2cSubscriptionOrderDataPopulator}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cSubscriptionOrderDataPopulatorUnitTest
{
	@InjectMocks
	private Wileyb2cSubscriptionOrderDataPopulator wileyb2cSubscriptionOrderDataPopulator;

	// Test data

	@Mock
	private WileySubscriptionModel wileySubscriptionModelMock;

	@Mock
	private WileySubscriptionData wileySubscriptionDataMock;

	@Mock
	private AbstractOrderEntryModel orderEntryModelMock;

	@Mock
	private AbstractOrderModel orderModelMock;
	private static final String ORDER_CODE = "some order code";

	@Mock
	private Date orderCreationDateMock;

	@Before
	public void setUp() throws Exception
	{
		when(wileySubscriptionModelMock.getOrderEntry()).thenReturn(orderEntryModelMock);
		when(orderEntryModelMock.getOrder()).thenReturn(orderModelMock);

		when(orderModelMock.getCode()).thenReturn(ORDER_CODE);
		when(orderModelMock.getCreationtime()).thenReturn(orderCreationDateMock);
	}

	@Test
	public void testPopulateSuccessCase()
	{
		// Given
		// no changes in test data

		// When
		wileyb2cSubscriptionOrderDataPopulator.populate(wileySubscriptionModelMock, wileySubscriptionDataMock);

		// Then
		verify(wileySubscriptionDataMock).setOrderCode(eq(ORDER_CODE));
		verify(wileySubscriptionDataMock).setOrderDate(same(orderCreationDateMock));
	}

	@Test
	public void testPopulateWhenOrderEntryIsNull()
	{
		// Given
		when(wileySubscriptionModelMock.getOrderEntry()).thenReturn(null);

		// When
		wileyb2cSubscriptionOrderDataPopulator.populate(wileySubscriptionModelMock, wileySubscriptionDataMock);

		// Then
		verifyZeroInteractions(wileySubscriptionDataMock);
	}

	@Test
	public void testPopulateWhenOrderIsNull()
	{
		// Given
		when(orderEntryModelMock.getOrder()).thenReturn(null);

		// When
		wileyb2cSubscriptionOrderDataPopulator.populate(wileySubscriptionModelMock, wileySubscriptionDataMock);

		// Then
		verifyZeroInteractions(wileySubscriptionDataMock);
	}

	@Test
	public void testPopulateWithIllegalParameters()
	{
		// When
		TestUtils.expectException(IllegalArgumentException.class,
				() -> wileyb2cSubscriptionOrderDataPopulator.populate(null, null));

		// ----------------------------------------------------------

		// When
		TestUtils.expectException(IllegalArgumentException.class,
				() -> wileyb2cSubscriptionOrderDataPopulator.populate(wileySubscriptionModelMock, null));
		// Then
		verifyZeroInteractions(wileySubscriptionModelMock);

		// ----------------------------------------------------------

		// When
		TestUtils.expectException(IllegalArgumentException.class,
				() -> wileyb2cSubscriptionOrderDataPopulator.populate(null, wileySubscriptionDataMock));
		// Then
		verifyZeroInteractions(wileySubscriptionDataMock);
	}
}