package com.wiley.facades.populators;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Locale;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyPriceDataFactoryTest {

    private static final String USD = "usd";
    @Mock
    private CommerceCommonI18NService mockCommerceCommonI18NService;
    @Mock
    private CommonI18NService mockCommonI18NService;
    @Mock
    private CurrencyModel mockCurrencyModel;
    @Mock
    private LanguageModel mockLanguageModel;
    @InjectMocks
    private WileyPriceDataFactory underTest = new WileyPriceDataFactory();

    @Before
    public void setup() {
        when(mockCurrencyModel.getIsocode()).thenReturn(USD);
        when(mockCurrencyModel.getDigits()).thenReturn(2);
        when(mockCommonI18NService.getCurrentLanguage()).thenReturn(mockLanguageModel);
        when(mockCommerceCommonI18NService.getLocaleForLanguage(mockLanguageModel)).thenReturn(Locale.US);
    }

    @Test
    public void shouldTruncateZeroCents() {
        PriceData priceData = underTest.create(PriceDataType.FROM, BigDecimal.valueOf(10.00d), mockCurrencyModel);
        assertEquals("$10.00", priceData.getFormattedValue());
        assertEquals("$10", priceData.getShortFormattedValue());
    }

    @Test
    public void shouldTruncateLessThanOneRoundedCent() {
        PriceData priceData = underTest.create(PriceDataType.FROM, BigDecimal.valueOf(10.001d), mockCurrencyModel);
        assertEquals("$10.00", priceData.getFormattedValue());
        assertEquals("$10", priceData.getShortFormattedValue());
    }

    @Test
    public void shouldNotTruncateOneRoundedCent() {
        PriceData priceData = underTest.create(PriceDataType.FROM, BigDecimal.valueOf(10.009d), mockCurrencyModel);
        assertEquals("$10.01", priceData.getFormattedValue());
        assertEquals("$10.01", priceData.getShortFormattedValue());
    }

    @Test
    public void shouldNotTruncateNotZeroCents() {
        PriceData priceData = underTest.create(PriceDataType.FROM, BigDecimal.valueOf(10.01d), mockCurrencyModel);
        assertEquals("$10.01", priceData.getFormattedValue());
        assertEquals("$10.01", priceData.getShortFormattedValue());

        priceData = underTest.create(PriceDataType.FROM, BigDecimal.valueOf(0.01d), mockCurrencyModel);
        assertEquals("$0.01", priceData.getFormattedValue());
        assertEquals("$0.01", priceData.getShortFormattedValue());
    }

}
