package com.wiley.facades.order.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.i18n.WileyCommonI18NService;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyCheckoutFacadeImplTestUnitTest
{
	@Mock
	private WileyCommonI18NService wileyCommonI18NServiceMock;

	@Mock
	private CartFacade cartFacadeMock;

	@Mock
	private CartService cartServiceMock;

	@Mock
	private ModelService modelServiceMock;

	@InjectMocks
	private WileyCheckoutFacadeImpl wileyCheckoutFacade;

	@Test
	public void setDefaultCurrencyInCartForCurrentPaymentAddressNullTest()
	{
		final CartModel cartMock = mock(CartModel.class);
		when(cartFacadeMock.hasSessionCart()).thenReturn(true);
		when(cartServiceMock.getSessionCart()).thenReturn(cartMock);
		when(cartMock.getPaymentAddress()).thenReturn(null);

		final boolean result = wileyCheckoutFacade.setDefaultCurrencyInCartForCurrentPaymentAddress();

		assertFalse(result);
		verify(cartFacadeMock, only()).hasSessionCart();
		verify(cartServiceMock, only()).getSessionCart();
		verify(cartMock, only()).getPaymentAddress();
		verify(wileyCommonI18NServiceMock, never()).getDefaultCurrency(any());
		verify(modelServiceMock, never()).save(cartMock);
	}

	@Test
	public void setDefaultCurrencyInCartForCurrentPaymentAddressSameCurrencyTest()
	{
		final CartModel cartMock = mock(CartModel.class);
		final AddressModel paymentAddressMock = mock(AddressModel.class);
		final CurrencyModel currencyMock = mock(CurrencyModel.class);
		final CountryModel countryMock = mock(CountryModel.class);
		when(cartFacadeMock.hasSessionCart()).thenReturn(true);
		when(cartServiceMock.getSessionCart()).thenReturn(cartMock);
		when(cartMock.getPaymentAddress()).thenReturn(paymentAddressMock);
		when(cartMock.getCurrency()).thenReturn(currencyMock);
		when(paymentAddressMock.getCountry()).thenReturn(countryMock);
		when(wileyCommonI18NServiceMock.getDefaultCurrency(countryMock)).thenReturn(currencyMock);

		final boolean result = wileyCheckoutFacade.setDefaultCurrencyInCartForCurrentPaymentAddress();

		assertFalse(result);
		verify(cartFacadeMock, only()).hasSessionCart();
		verify(cartServiceMock, only()).getSessionCart();
		verify(cartMock, atLeastOnce()).getPaymentAddress();
		verify(cartMock, atLeastOnce()).getCurrency();
		verify(paymentAddressMock, only()).getCountry();
		verify(wileyCommonI18NServiceMock, only()).getDefaultCurrency(any());
		verify(cartMock, never()).setCurrency(any());
		verify(modelServiceMock, never()).save(cartMock);
	}

	@Test
	public void setDefaultCurrencyInCartForCurrentPaymentAddressDifferentCurrencyTest()
	{
		final CartModel cartMock = mock(CartModel.class);
		final AddressModel paymentAddressMock = mock(AddressModel.class);
		final CurrencyModel currencyMock = mock(CurrencyModel.class);
		final CurrencyModel currencyNewMock = mock(CurrencyModel.class);
		final CountryModel countryMock = mock(CountryModel.class);
		when(cartFacadeMock.hasSessionCart()).thenReturn(true);
		when(cartServiceMock.getSessionCart()).thenReturn(cartMock);
		when(cartMock.getPaymentAddress()).thenReturn(paymentAddressMock);
		when(cartMock.getCurrency()).thenReturn(currencyMock);
		when(paymentAddressMock.getCountry()).thenReturn(countryMock);
		when(wileyCommonI18NServiceMock.getDefaultCurrency(countryMock)).thenReturn(currencyNewMock);

		final boolean result = wileyCheckoutFacade.setDefaultCurrencyInCartForCurrentPaymentAddress();

		assertTrue(result);
		verify(cartFacadeMock, only()).hasSessionCart();
		verify(cartServiceMock, only()).getSessionCart();
		verify(paymentAddressMock, only()).getCountry();
		verify(cartMock, atLeastOnce()).getPaymentAddress();
		verify(cartMock, atLeastOnce()).getCurrency();
		verify(wileyCommonI18NServiceMock, only()).getDefaultCurrency(any());
		verify(cartMock, atLeastOnce()).setCurrency(currencyNewMock);
		verify(modelServiceMock, only()).save(cartMock);
	}
}