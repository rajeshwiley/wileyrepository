package com.wiley.facades.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.order.CartModel;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.wiley.facades.product.data.InventoryStatusRecord;
import com.wiley.facades.wileycom.order.converters.populators.WileycomHasOutOfStockCartPopulator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileycomHasOutOfStockCartPopulatorUnitTest
{
	@Mock
	CartModel sourceMock;

	@InjectMocks
	private WileycomHasOutOfStockCartPopulator wileycomCartPopulator;

	private CartData target;
	private OrderEntryData instockEntryData;
	private OrderEntryData preorderEntryData;
	private OrderEntryData instockPreorderEntryData;

	@Before
	public void setup()
	{
		target = mock(CartData.class);
		instockEntryData = createOrderEntryData(StockLevelStatus.INSTOCK);
		preorderEntryData = createOrderEntryData(StockLevelStatus.PRE_ORDER);

		instockPreorderEntryData = createOrderEntryData(StockLevelStatus.INSTOCK, StockLevelStatus.PRE_ORDER);
	}

	@Test(expected = IllegalArgumentException.class)
	public void populateFirstParamAsNullTest()
	{
		wileycomCartPopulator.populate(null, target);
	}


	@Test(expected = IllegalArgumentException.class)
	public void populateSecondParamAsNullTest()
	{
		wileycomCartPopulator.populate(sourceMock, null);
	}

	@Test
	public void productDoesNotHaveOutOfStockItemsTest()
	{
		// Create List of OrderEntryData
		final List<OrderEntryData> entryDatas = Arrays.asList(instockEntryData, instockEntryData, instockEntryData);
		target.setEntries(entryDatas);
		wileycomCartPopulator.populate(sourceMock, target);

		verify(target).setHasOutOfStockItems(false);
	}

	@Test
	public void productHasOutOfStockItemsWithOnePreorderItemTest()
	{
		// One of three products has PRE_ORDER status
		when(target.getEntries()).thenReturn(Arrays.asList(instockEntryData, preorderEntryData, instockEntryData));
		wileycomCartPopulator.populate(sourceMock, target);

		verify(target).setHasOutOfStockItems(true);
	}

	@Test
	public void productHasOutOfStockItemsWithOneMixedItemTest()
	{
		// One of three products has INSTOCK + PRE_ORDER status
		when(target.getEntries()).thenReturn(Arrays.asList(instockEntryData, instockPreorderEntryData, instockEntryData));
		wileycomCartPopulator.populate(sourceMock, target);

		verify(target).setHasOutOfStockItems(true);
	}

	private OrderEntryData createOrderEntryData(final StockLevelStatus... stockLevelStatuses)
	{
		final OrderEntryData entryData = mock(OrderEntryData.class);
		ProductData productData = mock(ProductData.class);
		List<InventoryStatusRecord> statuses = new ArrayList<>();
		for (StockLevelStatus stockLevelStatus : stockLevelStatuses)
		{
			InventoryStatusRecord inventoryStatusRecord = mock(InventoryStatusRecord.class);
			when(inventoryStatusRecord.getStatusCode()).thenReturn(stockLevelStatus);
			statuses.add(inventoryStatusRecord);
		}
		when(productData.getInventoryStatus()).thenReturn(statuses);
		when(entryData.getProduct()).thenReturn(productData);
		return entryData;
	}
}