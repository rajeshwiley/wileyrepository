package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyPurchaseOptionProductModel;
import com.wiley.facades.populators.bright.cover.WileyBrightCoverHelper;
import com.wiley.facades.product.data.BrightCoveVideoData;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cProductBrightCoveVideoPopulatorUnitTest
{
	private static final String VIDEO_ID = "videoId";
	@InjectMocks
	private Wileyb2cProductBrightCoveVideoPopulator wileyb2cProductBrightCoveVideoPopulator;
	@Mock
	private WileyBrightCoverHelper wileyBrightCoverHelper;
	@Mock
	private WileyPurchaseOptionProductModel variantProduct;
	@Mock
	private ProductModel baseProduct;
	@Mock
	private ModelService modelService;

	@Before
	public void setUp()
	{
		when(wileyBrightCoverHelper.createBrightCoveVideo(VIDEO_ID)).thenReturn(createBrightCoveVideoData());
	}

	private BrightCoveVideoData createBrightCoveVideoData()
	{
		final BrightCoveVideoData brightCoveVideoData = new BrightCoveVideoData();
		brightCoveVideoData.setVideoId(VIDEO_ID);
		return brightCoveVideoData;
	}

	@Test
	public void shouldAddBrightcoveImageWhenBrightcoveVideoIdIsNonEmpty()
	{
		when(variantProduct.getBaseProduct()).thenReturn(baseProduct);
		when(modelService.getAttributeValue(baseProduct, ProductModel.BRIGHTCOVEVIDEOID)).thenReturn(VIDEO_ID);
		final ProductData productData = new ProductData();

		wileyb2cProductBrightCoveVideoPopulator.populate(variantProduct, productData);

		assertNotNull(productData.getBrightcoveVideo());
		assertEquals(productData.getBrightcoveVideo().getVideoId(), VIDEO_ID);
	}

	@Test
	public void shouldNotAddBrightcoveImageWhenBrightcoveVideoIdIsEmpty()
	{
		final ProductData productData = new ProductData();

		wileyb2cProductBrightCoveVideoPopulator.populate(variantProduct, productData);

		assertNull(productData.getBrightcoveVideo());
	}
}
