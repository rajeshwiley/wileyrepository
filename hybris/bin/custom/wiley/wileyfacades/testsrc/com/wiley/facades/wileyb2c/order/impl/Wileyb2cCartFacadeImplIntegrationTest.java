package com.wiley.facades.wileyb2c.order.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.CartRestorationData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartModificationStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.model.WileyProductSummaryModel;
import com.wiley.core.servicelayer.i18n.WileycomI18NService;
import com.wiley.facades.product.util.ProductInfo;
import com.wiley.facades.wileyplus.cart.activation.dto.CartActivationRequestDto;
import com.wiley.facades.wileyplus.cart.activation.dto.WileyPlusCourseDto;


/**
 * Integration test for {@link Wileyb2cCartFacadeImpl}
 */
@IntegrationTest
public class Wileyb2cCartFacadeImplIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	private static final String BASE_SITE = "wileyb2c";
	public static final String CATALOG_ID = "wileyProductCatalog";
	public static final String CATALOG_VERSION = "Online";

	private static final String TEST_IMAGE_URL = "testImageUrl";
	private static final String TEST_TITLE = "testTitle";
	private static final String TEST_AUTHORS = "testAuthors";
	private static final String TEST_PRODUCT_CODE = "WCOM_COURSE_EEGRP";
	private static final String TEST_PRODUCT_ISBN = "954EEGRP11123";
	private static final String TEST_SUBSCRIPTION_PRODUCT_CODE = "WCOM_PRODUCT_SUBSCRIPTION";
	private static final String TEST_SUBSCRIPTION_PRODUCT_ISBN = "222222222235";
	private static final String TEST_SUBSCRIPTION_MONTHLY_TERM = "subscription_term_1_month";
	private static final String TEST_SUBSCRIPTION_YEARLY_TERM = "subscription_term_1_year";
	private static final String TEST_WILEY_SUBSCRIPTION_CODE = "testSubscriptionCode1";
	private static final int QTY = 1;
	private static final String TEST_PURCHASE_TYPE = "testPurchaseType";
	private static final String INVALID_PRODUCT_CODE = "invalidProductCode";
	private static final String CLASS_SECTION_ID = "classSectionId";
	private static final String USER_ID = "userId";
	private static final String ADDITIONAL_INFO_VALUE = "additionalInfoValue";
	private static final String TEST_SUBSCRIPTION_PRODUCT_NAME = "WCOM PRODUCT SUBSCRIPTION";

	@Resource
	private Wileyb2cCartFacadeImpl wileyb2cCartFacade;

	@Resource
	private BaseSiteService baseSiteService;

	@Resource
	private UserService userService;

	@Resource
	private CatalogVersionService catalogVersionService;

	@Resource
	private CartService cartService;

	@Resource
	private SessionService sessionService;

	@Resource
	private ModelService modelService;

	@Resource
	private WileycomI18NService wileycomI18NService;
	
	@Before
	public void setUp() throws Exception
	{
		baseSiteService.setCurrentBaseSite(baseSiteService.getBaseSiteForUID(BASE_SITE), true);
		userService.setCurrentUser(userService.getAnonymousUser());
		catalogVersionService.setSessionCatalogVersion(CATALOG_ID, CATALOG_VERSION);
		wileycomI18NService.setDefaultCurrentCountry();
		importCsv("/wileyfacades/test/Wileyb2cCartFacadeImplIntegrationTest/product-for-set.impex", DEFAULT_ENCODING);
		importCsv("/wileyfacades/test/Wileyb2cCartFacadeImplIntegrationTest/product-prices.impex", DEFAULT_ENCODING);
		importCsv("/wileyfacades/test/Wileyb2cCartFacadeImplIntegrationTest/product-relations.impex", DEFAULT_ENCODING);
		importCsv("/wileyfacades/test/Wileyb2cCartFacadeImplIntegrationTest/product-subscription.impex", DEFAULT_ENCODING);

	}

	@Test
	public void testAddCourseToCartSuccess() throws CommerceCartModificationException
	{
		//Given
		CartActivationRequestDto activationDto = createActivationDto(TEST_IMAGE_URL, TEST_TITLE, TEST_AUTHORS);

		//When
		CartModificationData modification = wileyb2cCartFacade.addCourseToCart(TEST_PRODUCT_CODE, QTY, TEST_PURCHASE_TYPE,
				activationDto);

		//Than
		assertNotNull(modification);
		assertEquals(CommerceCartModificationStatus.SUCCESS, modification.getStatusCode());

		CartModel cartModel = cartService.getSessionCart();
		List<AbstractOrderEntryModel> entries = cartModel.getEntries();
		assertNotNull(entries);
		assertEquals(1, entries.size());

		final AbstractOrderEntryModel expected = createEntry(TEST_PRODUCT_CODE, TEST_TITLE, TEST_AUTHORS, TEST_IMAGE_URL,
				TEST_PURCHASE_TYPE);
		final AbstractOrderEntryModel actual = entries.get(0);
		assertEntry(expected, actual);
		assertEquals(Long.valueOf(1), actual.getQuantity());
	}

	@Test
	public void testAddCourseToCartWhenAlreadyExist() throws CommerceCartModificationException
	{
		//Given
		CartActivationRequestDto activationDto = createActivationDto(TEST_IMAGE_URL, TEST_TITLE, TEST_AUTHORS);

		//When
		// 1
		wileyb2cCartFacade.addCourseToCart(TEST_PRODUCT_CODE, QTY, TEST_PURCHASE_TYPE, activationDto);
		// 2
		CartModificationData modification = wileyb2cCartFacade.addCourseToCart(TEST_PRODUCT_CODE, QTY, TEST_PURCHASE_TYPE,
				activationDto);

		//Than
		assertNotNull(modification);
		assertEquals(CommerceCartModificationStatus.SUCCESS, modification.getStatusCode());

		CartModel cartModel = cartService.getSessionCart();
		List<AbstractOrderEntryModel> entries = cartModel.getEntries();
		assertNotNull(entries);
		assertEquals(1, entries.size());

		AbstractOrderEntryModel expected = createEntry(TEST_PRODUCT_CODE, TEST_TITLE, TEST_AUTHORS, TEST_IMAGE_URL,
				TEST_PURCHASE_TYPE);
		final AbstractOrderEntryModel actual = entries.get(0);
		assertEntry(expected, actual);
		assertEquals(Long.valueOf(2), actual.getQuantity());
	}

	@Test(expected = UnknownIdentifierException.class)
	public void testAddCourseToCartWithInvalidProductCode() throws CommerceCartModificationException
	{
		//Given
		CartActivationRequestDto activationDto = createActivationDto(TEST_IMAGE_URL, TEST_TITLE, TEST_AUTHORS);

		//When
		CartModificationData modification = wileyb2cCartFacade.addCourseToCart(INVALID_PRODUCT_CODE, QTY, TEST_PURCHASE_TYPE,
				activationDto);
	}

	@Test
	public void testAddToCartByIsbn() throws Exception
	{
		// Given
		final ProductInfo productInfo = new ProductInfo(3L);
		final long quantity = productInfo.getQuantity();

		// When
		final CartModificationData cartModificationData = wileyb2cCartFacade.addToCartByIsbn(TEST_PRODUCT_ISBN, productInfo,
				ADDITIONAL_INFO_VALUE);

		// Then
		assertNotNull(cartModificationData);
		assertEquals(quantity, cartModificationData.getQuantity());
		assertEquals(quantity, cartModificationData.getQuantityAdded());
		assertEquals(TEST_PRODUCT_CODE, cartModificationData.getEntry().getProduct().getCode());

		final CartModel sessionCart = cartService.getSessionCart();
		final List<AbstractOrderEntryModel> entries = sessionCart.getEntries();
		assertEquals(1, entries.size());
		final AbstractOrderEntryModel firstEntry = entries.get(0);
		assertEquals(TEST_PRODUCT_CODE, firstEntry.getProduct().getCode());
		assertEquals(quantity, firstEntry.getQuantity().intValue());

		// check extra info
		final String actualAdditionalInfo = firstEntry.getAdditionalInfo();
		assertEquals(ADDITIONAL_INFO_VALUE, actualAdditionalInfo);
	}

	@Test
	public void testAddToCartSubscriptionProductByIsbn() throws Exception
	{
		// Given
		final ProductInfo productInfo = new ProductInfo(3L, TEST_SUBSCRIPTION_MONTHLY_TERM);
		final long quantity = productInfo.getQuantity();

		// When
		CartModificationData cartModificationData = wileyb2cCartFacade.addToCartByIsbn(TEST_SUBSCRIPTION_PRODUCT_ISBN,
				productInfo, ADDITIONAL_INFO_VALUE);

		// Then
		assertNotNull(cartModificationData);
		assertEquals(quantity, cartModificationData.getQuantity());
		assertEquals(quantity, cartModificationData.getQuantityAdded());
		assertEquals(TEST_SUBSCRIPTION_PRODUCT_CODE, cartModificationData.getEntry().getProduct().getCode());

		final CartModel sessionCart = cartService.getSessionCart();
		final List<AbstractOrderEntryModel> entries = sessionCart.getEntries();
		assertEquals(1, entries.size());
		final AbstractOrderEntryModel firstEntry = entries.get(0);
		assertEquals(TEST_SUBSCRIPTION_PRODUCT_CODE, firstEntry.getProduct().getCode());
		assertEquals(quantity, firstEntry.getQuantity().intValue());
		assertEquals(TEST_SUBSCRIPTION_MONTHLY_TERM, firstEntry.getSubscriptionTerm().getId());
		assertNotNull(firstEntry.getProductSummary());
		assertEquals("WCOM PRODUCT SUBSCRIPTION", firstEntry.getProductSummary().getName());

		// check extra info
		final String actualAdditionalInfo = firstEntry.getAdditionalInfo();
		assertEquals(ADDITIONAL_INFO_VALUE, actualAdditionalInfo);

		//Given
		final ProductInfo yearlyTerm = new ProductInfo(1L, TEST_SUBSCRIPTION_YEARLY_TERM);

		//When
		cartModificationData = wileyb2cCartFacade.addToCartByIsbn(TEST_SUBSCRIPTION_PRODUCT_ISBN,
				yearlyTerm, ADDITIONAL_INFO_VALUE);

		//Then
		assertNotNull(cartModificationData);
		assertEquals(1L, cartModificationData.getQuantity());
		assertEquals(1L, cartModificationData.getQuantityAdded());
		assertEquals(TEST_SUBSCRIPTION_PRODUCT_CODE, cartModificationData.getEntry().getProduct().getCode());

		final CartModel sessionCartModified = cartService.getSessionCart();
		final List<AbstractOrderEntryModel> entriesModified = sessionCartModified.getEntries();
		assertEquals(1, entriesModified.size());
		final AbstractOrderEntryModel newEntry = entriesModified.get(0);
		assertEquals(TEST_SUBSCRIPTION_PRODUCT_CODE, newEntry.getProduct().getCode());
		assertEquals(1, newEntry.getQuantity().intValue());
		assertEquals(TEST_SUBSCRIPTION_YEARLY_TERM, newEntry.getSubscriptionTerm().getId());
		assertEquals(TEST_SUBSCRIPTION_MONTHLY_TERM, newEntry.getOriginalSubscriptionId());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAddToCartSubscriptionProductWithoutTerm() throws Exception
	{
		// Given
		final ProductInfo productInfo = new ProductInfo(3L);

		// When
		wileyb2cCartFacade.addToCartByIsbn(TEST_SUBSCRIPTION_PRODUCT_ISBN, productInfo, ADDITIONAL_INFO_VALUE);
	}

	@Test
	public void testAddToCartSubscriptionUpgrade() throws Exception
	{
		// When
		final CartModificationData cartModificationData = wileyb2cCartFacade.addToCartBySubscriptionCodeAndTermId(
				TEST_WILEY_SUBSCRIPTION_CODE, TEST_SUBSCRIPTION_MONTHLY_TERM);

		// Then
		assertNotNull(cartModificationData);
		assertEquals(QTY, cartModificationData.getQuantity());
		assertEquals(QTY, cartModificationData.getQuantityAdded());
		assertEquals(TEST_SUBSCRIPTION_PRODUCT_CODE, cartModificationData.getEntry().getProduct().getCode());

		final CartModel sessionCart = cartService.getSessionCart();
		final List<AbstractOrderEntryModel> entries = sessionCart.getEntries();
		assertEquals(1, entries.size());
		final AbstractOrderEntryModel firstEntry = entries.get(0);
		assertEquals(TEST_SUBSCRIPTION_PRODUCT_CODE, firstEntry.getProduct().getCode());
		assertEquals(QTY, firstEntry.getQuantity().intValue());
		assertEquals(TEST_SUBSCRIPTION_MONTHLY_TERM, firstEntry.getSubscriptionTerm().getId());
		assertNotNull(firstEntry.getProductSummary());
		assertEquals(TEST_SUBSCRIPTION_PRODUCT_NAME, firstEntry.getProductSummary().getName());
		final String additionalInfo = firstEntry.getAdditionalInfo();
		assertNotNull(additionalInfo);
		assertTrue(additionalInfo.isEmpty());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAddToCartSubscriptionUpgradeWithoutTerm() throws Exception
	{
		// When
		wileyb2cCartFacade.addToCartBySubscriptionCodeAndTermId(TEST_WILEY_SUBSCRIPTION_CODE, null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAddToCartSubscriptionUpgradeWithoutSubscription() throws Exception
	{
		// When
		wileyb2cCartFacade.addToCartBySubscriptionCodeAndTermId(null, TEST_SUBSCRIPTION_MONTHLY_TERM);
	}

	@Test
	public void testSaveContinueUrl()
	{
		// Given
		final String someContinueUrl = "someContinueUrl";
		final CartModel sessionCart = cartService.getSessionCart();

		// When
		wileyb2cCartFacade.saveContinueUrl(someContinueUrl);

		// Then
		modelService.refresh(sessionCart);
		assertEquals(someContinueUrl, sessionCart.getCartPageContinueUrl());
	}

	@Test
	public void testGetContinueUrl()
	{
		// Given
		final String someContinueUrl = "someContinueUrl";
		final CartModel sessionCart = cartService.getSessionCart();
		sessionCart.setCartPageContinueUrl(someContinueUrl);
		modelService.save(sessionCart);

		// When
		final String continueUrl = wileyb2cCartFacade.getContinueUrl();

		// Then
		assertNotNull(continueUrl);
		assertEquals(someContinueUrl, continueUrl);
	}

	@Test
	public void testRestoreCartAndMerge() throws Exception
	{
		//Given
		userService.setCurrentUser(userService.getAdminUser());
		importCsv("/wileyfacades/test/Wileyb2cCartFacadeImplIntegrationTest/product-cart.impex", DEFAULT_ENCODING);

		final ProductInfo productInfo = new ProductInfo(1L);
		wileyb2cCartFacade.addToCartByIsbn(TEST_PRODUCT_ISBN, productInfo, StringUtils.EMPTY);

		CartModel sessionCart = cartService.getSessionCart();
		assertNotNull(sessionCart);
		final String sessionCartGuid = sessionCart.getGuid();

		//When
		final CartRestorationData cartRestorationData = wileyb2cCartFacade.restoreCartAndMerge("cart-guid", sessionCartGuid);

		//Then
		modelService.detachAll();
		assertNotNull(cartRestorationData);
		sessionCart = cartService.getSessionCart();
		assertNotNull(sessionCart);
		final List<AbstractOrderEntryModel> entries = sessionCart.getEntries();
		assertTrue(CollectionUtils.isNotEmpty(entries));
		final AbstractOrderEntryModel subscriptionEntry = findEntry(entries, TEST_SUBSCRIPTION_PRODUCT_CODE);
		assertNotNull(subscriptionEntry.getSubscriptionTerm());
		assertEquals(1, subscriptionEntry.getQuantity().intValue());
		final AbstractOrderEntryModel normalProductEntry = findEntry(entries, TEST_PRODUCT_CODE);
		assertNotNull(normalProductEntry);
		assertEquals(1, normalProductEntry.getQuantity().intValue());
	}

	private static AbstractOrderEntryModel findEntry(final List<AbstractOrderEntryModel> entries, final String productCode)
	{
		final Optional<AbstractOrderEntryModel> orderEntryModel = entries.stream().filter(
				e -> e.getProduct().getCode().equals(productCode))
				.findAny();
		if (orderEntryModel.isPresent())
		{
			return orderEntryModel.get();
		}
		else
		{
			final String message = String.format("Product with code %s wasn't found in entries collection", productCode);
			fail(message);
			throw new IllegalStateException(message);
		}
	}

	private static void assertEntry(final AbstractOrderEntryModel expected, final AbstractOrderEntryModel actual)
	{
		assertEquals(expected.getProduct().getCode(), actual.getProduct().getCode());
		WileyProductSummaryModel expectedSummary = expected.getProductSummary();
		WileyProductSummaryModel actualSummary = actual.getProductSummary();

		assertEquals(expectedSummary.getSummary(), actualSummary.getSummary());
		assertEquals(expectedSummary.getAuthors(), actualSummary.getAuthors());
		assertEquals(expectedSummary.getPictureUrl(), actualSummary.getPictureUrl());
		assertEquals(expectedSummary.getName(), actualSummary.getName());

		assertEquals(expected.getAdditionalInfo(), actual.getAdditionalInfo());
	}

	private CartActivationRequestDto createActivationDto(final String imageUrl, final String title, final String authors)
	{
		WileyPlusCourseDto course = new WileyPlusCourseDto()
				.authors(authors)
				.imageUrl(imageUrl)
				.title(title);

		return new CartActivationRequestDto()
				.course(course)
				.classSectionId(CLASS_SECTION_ID)
				.userId(USER_ID)
				.additionalInfo(ADDITIONAL_INFO_VALUE);
	}

	private AbstractOrderEntryModel createEntry(final String productCode, final String title, final String authors,
			final String imageUrl, final String purchaseType)
	{
		AbstractOrderEntryModel entry = new AbstractOrderEntryModel();
		ProductModel product = new ProductModel();
		product.setCode(productCode);
		entry.setProduct(product);
		WileyProductSummaryModel summaryModel = new WileyProductSummaryModel();
		entry.setProductSummary(summaryModel);

		summaryModel.setAuthors(authors);
		summaryModel.setPictureUrl(imageUrl);
		summaryModel.setSummary(purchaseType);
		summaryModel.setName(title);

		entry.setAdditionalInfo(ADDITIONAL_INFO_VALUE);

		return entry;

	}
}
