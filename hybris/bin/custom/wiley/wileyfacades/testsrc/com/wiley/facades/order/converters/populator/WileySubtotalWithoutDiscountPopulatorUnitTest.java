package com.wiley.facades.order.converters.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.CartModel;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Unit test for {@link WileySubtotalWithoutDiscountPopulator}.
 *
 * Author Herman_Chukhrai (EPAM)
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileySubtotalWithoutDiscountPopulatorUnitTest
{

	@InjectMocks
	private WileySubtotalWithoutDiscountPopulator testInstance;

	@Mock
	private PriceDataFactory priceDataFactory;

	@Mock
	private PriceData priceDataMock;

	private static final BigDecimal TEST_SUBTOTAL_VALUE = new BigDecimal("12.0");

	@Mock
	private CartModel sourceMock;
	@Mock
	private CurrencyModel currencyModelMock;
	@Mock
	private CartData targetMock;

	@Test
	public void testSubtotalPopulation() throws Exception
	{
		when(priceDataFactory
				.create(eq(PriceDataType.BUY), eq(TEST_SUBTOTAL_VALUE), eq(currencyModelMock)))
				.thenReturn(priceDataMock);

		when(sourceMock.getCurrency()).thenReturn(currencyModelMock);
		when(sourceMock.getSubTotalWithoutDiscount()).thenReturn(TEST_SUBTOTAL_VALUE.doubleValue());
		when(priceDataMock.getValue()).thenReturn(TEST_SUBTOTAL_VALUE);

		testInstance.populate(sourceMock, targetMock);

		verify(targetMock).setSubTotalWithoutDiscount(priceDataMock);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testPopulateSourceIsNull() throws Exception
	{
		testInstance.populate(null, targetMock);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testPopulateTargetIsNull() throws Exception
	{
		testInstance.populate(sourceMock, null);
	}
}