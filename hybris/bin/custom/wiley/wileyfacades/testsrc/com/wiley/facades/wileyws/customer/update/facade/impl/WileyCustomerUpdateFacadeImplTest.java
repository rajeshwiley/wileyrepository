package com.wiley.facades.wileyws.customer.update.facade.impl;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.never;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;

import com.wiley.core.customer.service.WileyCustomerAccountService;
import com.wiley.facades.wileyws.customer.update.dto.CustomerUpdateRequest;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


/**
 * Unit test for {@link WileyCustomerUpdateFacadeImpl}.
 * Created by Csanad_Toth-Benedek on 7/29/2016.
 */
@UnitTest
public class WileyCustomerUpdateFacadeImplTest
{
	private static final String VALID_B2C_CUSTOMER_ID = "b2ccustomer@wiley.com";
	private static final String VALID_B2B_CUSTOMER_ID = "b2bcustomer@wiley.com";
	private static final String INVALID_CUSTOMER_ID = "invalid";
	private static final String FIRST_NAME = "FirstName";
	private static final String LAST_NAME = "LastName";
	private static final String EMAIL = "Email";
	private static final String ECID_NUMBER = "ECIDNumber";
	private static final String SAP_ACCOUNT_NUMBER = "SAP_ACC_NUMBER";

	private static final CustomerUpdateRequest CUSTOMER_UPDATE_REQUEST;

	static
	{
		CUSTOMER_UPDATE_REQUEST = new CustomerUpdateRequest();
		CUSTOMER_UPDATE_REQUEST.setFirstName(FIRST_NAME);
		CUSTOMER_UPDATE_REQUEST.setLastName(LAST_NAME);
		CUSTOMER_UPDATE_REQUEST.setEmail(EMAIL);
		CUSTOMER_UPDATE_REQUEST.setIndividualEcidNumber(ECID_NUMBER);
		CUSTOMER_UPDATE_REQUEST.setIndividualSapAccountNumber(SAP_ACCOUNT_NUMBER);
	}

	@Mock
	private UserModel userModel;

	@Mock
	private CustomerModel b2cCustomerModel;

	@Mock
	private B2BCustomerModel b2bCustomerModel;

	@Mock
	private UserService userService;

	@Mock
	private WileyCustomerAccountService customerAccountService;

	@InjectMocks
	private WileyCustomerUpdateFacadeImpl wileyCustomerUpdateFacade;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		when(userService.getUserForUID(VALID_B2C_CUSTOMER_ID)).thenReturn(b2cCustomerModel);
		when(userService.getUserForUID(VALID_B2B_CUSTOMER_ID)).thenReturn(b2bCustomerModel);
		when(userService.getUserForUID(INVALID_CUSTOMER_ID)).thenReturn(userModel);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowExceptionWhenCustomerIdIsNull() throws DuplicateUidException
	{
		wileyCustomerUpdateFacade.updateCustomer(null, CUSTOMER_UPDATE_REQUEST);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowExceptionWhenUpdateRequestIsNull() throws DuplicateUidException
	{
		wileyCustomerUpdateFacade.updateCustomer(INVALID_CUSTOMER_ID, null);
	}

	@Test(expected = IllegalStateException.class)
	public void shouldThrowExceptionForInvalidCustomer() throws DuplicateUidException
	{
		wileyCustomerUpdateFacade.updateCustomer(INVALID_CUSTOMER_ID, CUSTOMER_UPDATE_REQUEST);
	}

	@Test
	public void shouldUpdateValidB2CCustomer() throws DuplicateUidException
	{
		wileyCustomerUpdateFacade.updateCustomer(VALID_B2C_CUSTOMER_ID, CUSTOMER_UPDATE_REQUEST);

		verify(b2cCustomerModel).setFirstName(FIRST_NAME);
		verify(b2cCustomerModel).setLastName(LAST_NAME);
		verify(b2cCustomerModel).setSapAccountNumber(SAP_ACCOUNT_NUMBER);
		verify(b2cCustomerModel).setEcidNumber(ECID_NUMBER);
		verify(customerAccountService).updateCustomerProfile(b2cCustomerModel);
	}

	@Test
	public void shouldUpdateValidB2BCustomer() throws DuplicateUidException
	{
		wileyCustomerUpdateFacade.updateCustomer(VALID_B2B_CUSTOMER_ID, CUSTOMER_UPDATE_REQUEST);

		verify(b2bCustomerModel).setFirstName(FIRST_NAME);
		verify(b2bCustomerModel).setLastName(LAST_NAME);
		verify(b2bCustomerModel).setEmail(EMAIL);
		verify(b2bCustomerModel, never()).setSapAccountNumber(SAP_ACCOUNT_NUMBER);
		verify(b2bCustomerModel, never()).setEcidNumber(ECID_NUMBER);
		verify(customerAccountService).updateCustomerProfile(b2bCustomerModel);
	}
}
