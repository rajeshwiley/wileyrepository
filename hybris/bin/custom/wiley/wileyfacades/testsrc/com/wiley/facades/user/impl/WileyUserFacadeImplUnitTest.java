package com.wiley.facades.user.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyUserFacadeImplUnitTest
{
	@InjectMocks
	private WileyUserFacadeImpl wileyUserFacade;

	@Mock
	private UserService userService;

	@Mock
	private ModelService modelService;

	@Mock
	private CommonI18NService commonI18NService;

	@Mock
	private CustomerModel customerModelMock;

	@Before
	public void setUp()
	{
		when(userService.getCurrentUser())
				.thenReturn(customerModelMock);
	}

	@Test
	public void shouldNotSaveUserWhenAnonymousAndSyncLanguage()
	{
		when(userService.isAnonymousUser(customerModelMock))
				.thenReturn(true);

		wileyUserFacade.syncSessionLanguage();

		verify(modelService, never())
				.save(customerModelMock);
	}

	@Test
	public void shouldSaveUserWhenNotAnonymousAndSyncLanguage()
	{
		when(userService.isAnonymousUser(customerModelMock))
				.thenReturn(false);

		wileyUserFacade.syncSessionLanguage();

		verify(modelService)
				.save(customerModelMock);
	}

	@Test
	public void shouldNotSaveUserWhenAnonymousAndSyncCurrency()
	{
		when(userService.isAnonymousUser(customerModelMock))
				.thenReturn(true);

		wileyUserFacade.syncSessionCurrency();

		verify(modelService, never())
				.save(customerModelMock);
	}

	@Test
	public void shouldSaveUserWhenNotAnonymousAndSyncCurrency()
	{
		when(userService.isAnonymousUser(customerModelMock))
				.thenReturn(false);

		wileyUserFacade.syncSessionCurrency();

		verify(modelService)
				.save(customerModelMock);
	}
}
