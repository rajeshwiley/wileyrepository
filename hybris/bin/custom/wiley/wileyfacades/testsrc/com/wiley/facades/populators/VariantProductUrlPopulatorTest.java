package com.wiley.facades.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantProductModel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.category.WileyCategoryService;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VariantProductUrlPopulatorTest
{
	@InjectMocks
	private VariantProductUrlPopulator testInstance = new VariantProductUrlPopulator();

	@Mock
	private ProductModel mockBaseProductModel;
	@Mock
	private VariantProductModel mockVariantProductModel;
	@Mock
	private VariantProductModel mockCfaVariantProductModel;
	@Mock
	private WileyCategoryService mockWileyCategoryService;
	@Mock
	private UrlResolver mockProductModelUrlResolver;
	@Mock
	private CategoryModel mockCfaCategoryModel;

	private ProductData productData;

	@Before
	public void setUp()
	{
		productData = new ProductData();
		when(mockVariantProductModel.getBaseProduct()).thenReturn(mockBaseProductModel);
		when(mockWileyCategoryService.getPrimaryWileyCategoryForProduct(mockCfaVariantProductModel)).thenReturn(
				mockCfaCategoryModel);
		when(mockWileyCategoryService.isCFACategory(mockCfaCategoryModel)).thenReturn(true);
	}

	@Test
	public void shouldBuildUrlFromBaseProductIfItsBase()
	{
		testInstance.populate(mockBaseProductModel, productData);
		verify(mockProductModelUrlResolver).resolve(mockBaseProductModel);
	}

	@Test
	public void shouldBuildUrlFromBaseProductIfItsVariantAndNonCfa()
	{
		testInstance.populate(mockVariantProductModel, productData);
		verify(mockProductModelUrlResolver).resolve(mockBaseProductModel);
	}

	@Test
	public void shouldBuildUrlFromVariantProductIfItsVariantAndCfa()
	{
		testInstance.populate(mockCfaVariantProductModel, productData);
		verify(mockProductModelUrlResolver).resolve(mockCfaVariantProductModel);
	}
}
