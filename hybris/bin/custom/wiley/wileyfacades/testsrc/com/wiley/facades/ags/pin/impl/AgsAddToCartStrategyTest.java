package com.wiley.facades.ags.pin.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.order.CartModel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.PinModel;
import com.wiley.facades.ags.order.AgsSubscriptionCartFacade;
import com.wiley.facades.welags.pin.exception.PinOperationException;

import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Unit test for {@link AgsAddToCartStrategy}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AgsAddToCartStrategyTest
{
	private static final String PRODUCT_CODE = "product-code";
	private static final String CART_CODE = "cart-code";

	@Mock
	private CartModel cartModelMock;

	@Mock
	private PinModel pinModelMock;

	@Mock
	private AgsSubscriptionCartFacade agsCartFacade;

	@InjectMocks
	private AgsAddToCartStrategy agsAddToCartStrategy;

	@Before
	public void setup()
	{
		when(pinModelMock.getProductCode()).thenReturn(PRODUCT_CODE);
		when(cartModelMock.getCode()).thenReturn(CART_CODE);
	}

	@Test
	public void shouldAddProductToCartBeSuccessful() throws PinOperationException, CommerceCartModificationException
	{
		agsAddToCartStrategy.addProductToCart(cartModelMock, pinModelMock);
		verify(agsCartFacade).addToCart(CART_CODE, PRODUCT_CODE, 1);
	}

	@Test(expected = PinOperationException.class)
	public void shouldThrowExceptionIfAddProductToCartWasNotSuccessful() throws PinOperationException,
			CommerceCartModificationException
	{
		when(agsCartFacade.addToCart(anyString(), anyString(), eq(1L))).thenThrow(CommerceCartModificationException.class);

		agsAddToCartStrategy.addProductToCart(cartModelMock, pinModelMock);
		verify(agsCartFacade).addToCart(CART_CODE, PRODUCT_CODE, 1);
	}
}
