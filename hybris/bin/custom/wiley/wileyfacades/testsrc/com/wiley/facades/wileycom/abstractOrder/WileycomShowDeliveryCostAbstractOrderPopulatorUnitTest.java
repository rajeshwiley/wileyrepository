package com.wiley.facades.wileycom.abstractOrder;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.user.AddressModel;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.wiley.core.product.WileyProductEditionFormatService;


/**
 * Created by Mikhail_Asadchy on 11/21/2016.
 */
@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class WileycomShowDeliveryCostAbstractOrderPopulatorUnitTest
{
	@InjectMocks
	private WileycomShowDeliveryCostAbstractOrderPopulator populator;

	@Mock
	private AbstractOrderModel source;

	@Mock
	private AbstractOrderData target;

	@Mock
	private AddressModel addressMock;

	@Mock
	private DeliveryModeModel deliveryModeModelMock;

	@Mock
	private WileyProductEditionFormatService wileyProductEditionFormatService;

	@Test
	public void doNeedToShow() throws Exception
	{
		doTestCase(addressMock, deliveryModeModelMock, false);

		// then
		verify(target, times(1)).setShowDeliveryCost(true);
	}

	@Test
	public void doNotNeedToShow() throws Exception
	{
		doTestCase(null, null, false);
		doTestCase(null, null, true);
		doTestCase(null, deliveryModeModelMock, false);
		doTestCase(null, deliveryModeModelMock, true);
		doTestCase(addressMock, null, false);
		doTestCase(addressMock, null, true);
		doTestCase(addressMock, deliveryModeModelMock, true);

		// then
		verify(target, times(7)).setShowDeliveryCost(false);
	}

	private void doTestCase(final AddressModel addressMock, final DeliveryModeModel deliveryModeModelMock,
			final boolean isDigitalCart)
	{
		// given

		// preparations
		when(source.getDeliveryAddress()).thenReturn(addressMock);
		when(source.getDeliveryMode()).thenReturn(deliveryModeModelMock);
		when(wileyProductEditionFormatService.isDigitalCart(source)).thenReturn(isDigitalCart);

		// when
		populator.populate(source, target);
	}
}
