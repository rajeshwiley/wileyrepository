package com.wiley.facades.voucher.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.voucher.VoucherService;

import javax.annotation.Resource;

import org.junit.Before;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.facades.voucher.WileyCouponFacade;


/**
 * Default integration test for {@link WileyCouponFacadeImpl}.
 */
@IntegrationTest
public class WileyCouponFacadeImplIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{

	@Resource
	private WileyCouponFacade wileyCouponFacade;

	@Resource
	private CommerceCartService commerceCartService;

	@Resource
	private UserService userService;

	@Resource
	private CartService cartService;

	@Resource
	private VoucherService voucherService;

	private static final String TEST_VOUCHER_CODE = "222-B5K6-EYCB-6GBP";
	private static final String TEST_CART_UID = "test-cart1";
	private static final String TEST_USER_UID = "test@hybris.com";

	@Before
	public void setUp() throws Exception
	{
		importCsv("/wileyfacades/test/WileyVoucherFacadeImplIntegrationTest/cart.impex", DEFAULT_ENCODING);
		importCsv("/wileyfacades/test/WileyVoucherFacadeImplIntegrationTest/voucher.impex", DEFAULT_ENCODING);
	}
	
	//TODO: Cover WileyVoucherFacadeImpl with tests
}