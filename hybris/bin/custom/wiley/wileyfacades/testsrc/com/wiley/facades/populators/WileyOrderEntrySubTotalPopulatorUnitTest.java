package com.wiley.facades.populators;

import com.wiley.facades.order.converters.populator.WileyOrderEntrySubTotalPopulator;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyOrderEntrySubTotalPopulatorUnitTest
{
    private static final Double SUB_TOTAL_PRICE = 123.45;
    private static final Integer CURRENCY_DIGITS = 2;
    private static final String CURRENCY_SYMBOL = "$";
    private static final String CURRENCY_ISO = "USD";

    @InjectMocks
    private WileyOrderEntrySubTotalPopulator target;

    private WileyPriceDataFactory priceDataFactoryMock = new WileyPriceDataFactory();

    @Mock
    private AbstractOrderEntryModel entryMock;

    @Mock
    private AbstractOrderModel cartMock;

    @Mock
    private CommonI18NService commonI18NServiceMock;

    @Mock
    private CommerceCommonI18NService commerceCommonI18NServiceMock;

    @Mock
    private LanguageModel languageModelMock;

    @Mock
    private CurrencyModel currencyMock;

    private OrderEntryData data;


    @Before
    public void prepare()
    {
        priceDataFactoryMock.setCommonI18NService(commonI18NServiceMock);
        priceDataFactoryMock.setCommerceCommonI18NService(commerceCommonI18NServiceMock);
        target.setWileyPriceDataFactory(priceDataFactoryMock);
        when(currencyMock.getDigits()).thenReturn(CURRENCY_DIGITS);
        when(currencyMock.getSymbol()).thenReturn(CURRENCY_SYMBOL);
        when(currencyMock.getIsocode()).thenReturn(CURRENCY_ISO);
        when(entryMock.getSubtotalPrice()).thenReturn(SUB_TOTAL_PRICE);
        when(entryMock.getOrder()).thenReturn(cartMock);
        when(cartMock.getCurrency()).thenReturn(currencyMock);
        when(commonI18NServiceMock.getCurrentLanguage()).thenReturn(languageModelMock);
        when(commerceCommonI18NServiceMock.getLocaleForLanguage(languageModelMock)).thenReturn(Locale.US);
    }

    @Test
    public void testPopulationSubTotalPriceWithValue()
    {
        // given
        data = new OrderEntryData();

        // when
        target.populate(entryMock, data);

        // then
        assertNotNull(data);
        assertEquals(SUB_TOTAL_PRICE, data.getSubtotalPrice().getValue().doubleValue(), 2);
    }


    @Test
    public void testPopulationSubTotalPriceWithZero()
    {
        // given
        data = new OrderEntryData();
        when(entryMock.getSubtotalPrice()).thenReturn(0.0);

        // when
        target.populate(entryMock, data);

        // then
        assertNotNull(data);
        assertNotEquals(SUB_TOTAL_PRICE, data.getSubtotalPrice().getValue().doubleValue());
    }


    @Test
    public void testPopulationSubTotalPriceWithNull()
    {
        // given
        data = new OrderEntryData();
        when(entryMock.getSubtotalPrice()).thenReturn(null);

        // when
        target.populate(entryMock, data);

        // then
        assertNotNull(data);
        assertNull(data.getSubtotalPrice());
    }
}
