package com.wiley.facades.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.media.MediaModel;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.wiley.core.media.WileyMediaService;
import com.wiley.facades.product.data.MediaData;

import static junit.framework.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;


@UnitTest
public class MediaPopulatorUnitTest
{
	private static final String TEST_URL = "url";
	private static final String TEST_FILE_NAME = "name.pdf";
	private static final String TEST_NAME = "name";
	private static final String TEST_DESCRIPTION = "descr";
	private final MediaPopulator testingInstance = new MediaPopulator();

	@Mock
	private WileyMediaService wileyMediaService;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		testingInstance.setMediaService(wileyMediaService);
	}

	@Test
	public void shouldPopulateMediaData()
	{
		final MediaModel source = Mockito.mock(MediaModel.class);
		given(wileyMediaService.getFileNameWithoutExtension(source)).willReturn(TEST_NAME);
		when(source.getURL()).thenReturn(TEST_URL);
		when(source.getCode()).thenReturn(TEST_NAME);
		when(source.getDescription()).thenReturn(TEST_DESCRIPTION);
		final MediaData target = new MediaData();

		testingInstance.populate(source, target);

		assertEquals(TEST_URL, target.getUrl());
		assertEquals(TEST_NAME, target.getName());
		assertEquals(TEST_DESCRIPTION, target.getDescription());
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldFailedWhenSourceIsNull()
	{
		final MediaData target = new MediaData();
		testingInstance.populate(null, target);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldFailedWhenTargetIsNull()
	{
		final MediaModel source = Mockito.mock(MediaModel.class);
		testingInstance.populate(source, null);
	}
}
