package com.wiley.facades.wileyb2c.search.solrfacetsearch.converters.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.facades.content.ContentData;



/**
 * Created by Uladzimir_Barouski on 7/19/2017.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cSearchResultContentPopulatorTest
{
	public static final int CONTENT_MAX_SIZE = 20;
	public static final Float BOOST_CONTENT = 1f;
	public static final String ID_CONTENT = "id content";
	public static final String URL_CONTENT = "url content";
	public static final String TITLE_CONTENT = "title content";
	public static final String METADESCRIPTION_CONTENT = "metadescription content";
	public static final String STRIPPED_CONTENT_21 = "stripped content long";
	public static final String STRIPPED_CONTENT_16 = "stripped content";
	public static final String STRIPPED_CONTENT_21_RESULT = "stripped content...";
	public static final String STRIPPED_CONTENT_16_RESULT = "stripped content...";
	@InjectMocks
	Wileyb2cSearchResultContentPopulator wileyb2cSearchResultContentPopulator;
	@Mock(answer = Answers.RETURNS_DEEP_STUBS)
	SearchResultValueData searchResultValueDataMock;

	@Before
	public void setUp() throws Exception
	{
		when(searchResultValueDataMock.getValues().get(Wileyb2cSearchResultContentPopulator.BOOST)).thenReturn(BOOST_CONTENT);
		when(searchResultValueDataMock.getValues().get(Wileyb2cSearchResultContentPopulator.TITLE)).thenReturn(TITLE_CONTENT);
		when(searchResultValueDataMock.getValues().get(Wileyb2cSearchResultContentPopulator.ID)).thenReturn(ID_CONTENT);
		when(searchResultValueDataMock.getValues().get(Wileyb2cSearchResultContentPopulator.URL)).thenReturn(URL_CONTENT);

		wileyb2cSearchResultContentPopulator.setContentMaxSize(CONTENT_MAX_SIZE);
	}

	@Test
	public void populateWhenMetaDescriptionNotEmpty() throws Exception
	{
		//Given
		final ContentData contentData = new ContentData();
		when(searchResultValueDataMock.getValues().get(Wileyb2cSearchResultContentPopulator.STRIPPED_CONTENT))
				.thenReturn(null);
		when(searchResultValueDataMock.getValues().get(Wileyb2cSearchResultContentPopulator.META_DESCRIPTION))
				.thenReturn(METADESCRIPTION_CONTENT);
		//When
		wileyb2cSearchResultContentPopulator.populate(searchResultValueDataMock, contentData);
		//Then
		assertFields(contentData);
		assertNotNull(contentData.getContent());
		assertEquals(METADESCRIPTION_CONTENT, contentData.getContent());
	}

	@Test
	public void populateWhenMetaDescriptionEmptyAndContentLong() throws Exception
	{
		//Given
		final ContentData contentData = new ContentData();
		when(searchResultValueDataMock.getValues().get(Wileyb2cSearchResultContentPopulator.STRIPPED_CONTENT))
				.thenReturn(STRIPPED_CONTENT_21);
		when(searchResultValueDataMock.getValues().get(Wileyb2cSearchResultContentPopulator.META_DESCRIPTION))
				.thenReturn(null);
		//When
		wileyb2cSearchResultContentPopulator.populate(searchResultValueDataMock, contentData);
		//Then
		assertFields(contentData);
		assertNotNull(contentData.getContent());
		assertEquals(STRIPPED_CONTENT_21_RESULT, contentData.getContent());
	}

	@Test
	public void populateWhenMetaDescriptionAndContentShort() throws Exception
	{
		//Given
		final ContentData contentData = new ContentData();
		when(searchResultValueDataMock.getValues().get(Wileyb2cSearchResultContentPopulator.STRIPPED_CONTENT))
				.thenReturn(STRIPPED_CONTENT_16);
		when(searchResultValueDataMock.getValues().get(Wileyb2cSearchResultContentPopulator.META_DESCRIPTION))
				.thenReturn(null);
		//When
		wileyb2cSearchResultContentPopulator.populate(searchResultValueDataMock, contentData);
		//Then
		assertFields(contentData);
		assertNotNull(contentData.getContent());
		assertEquals(STRIPPED_CONTENT_16_RESULT, contentData.getContent());
	}

	private void assertFields(final ContentData contentData)
	{
		assertNotNull(contentData.getBoost());
		assertEquals(BOOST_CONTENT, contentData.getBoost());
		assertNotNull(contentData.getId());
		assertEquals(ID_CONTENT, contentData.getId());
		assertNotNull(contentData.getUrl());
		assertEquals(URL_CONTENT, contentData.getUrl());
		assertNotNull(contentData.getTitle());
		assertEquals(TITLE_CONTENT, contentData.getTitle());
	}
}