package com.wiley.facades.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyVariantProductModel;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WelOptionalProductDescriptionPopulatorTest
{
	private static final String TEST_DESC = "test_desc";

	@InjectMocks
	private WelOptionalProductDescriptionPopulator descriptionPopulator;

	@Mock
	private ProductModel productModelMock;
	
	@Mock
	private AbstractOrderEntryModel orderEntryModelMock;
	
	@Mock
	private WileyVariantProductModel wileyVariantProductMock;

	@Mock
	private ProductData productDataMock;
	
	@Mock
	private OrderEntryData orderEntryDataMock;


	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		
		doReturn(wileyVariantProductMock).when(orderEntryModelMock).getProduct();
		doReturn(productDataMock).when(orderEntryDataMock).getProduct();
	}

	@Test
	public void shouldIgnoreNotWileyVariantProducts()
	{
		// Given
		doReturn(productModelMock).when(orderEntryModelMock).getProduct();
				
		// Testing
		descriptionPopulator.populate(orderEntryModelMock, orderEntryDataMock);

		// Verify
		verify(productDataMock, never()).setOptionalDescription(any());

	}

	@Test
	public void shouldPopulateOptionalDescriptionForWileyVariantProduct()
	{
		// Given
		doReturn(TEST_DESC).when(wileyVariantProductMock).getOptionalDescription();

		// Testing
		descriptionPopulator.populate(orderEntryModelMock, orderEntryDataMock);

		// Verify

		verify(productDataMock).setOptionalDescription(TEST_DESC);
	}

	@Test
	public void optionalDescriptionIsNullIfNotDefined()
	{
		// Given
		doReturn(null).when(wileyVariantProductMock).getOptionalDescription();

		// Testing
		descriptionPopulator.populate(orderEntryModelMock, orderEntryDataMock);

		// Verify
		verify(productDataMock).setOptionalDescription(null);
	}
}