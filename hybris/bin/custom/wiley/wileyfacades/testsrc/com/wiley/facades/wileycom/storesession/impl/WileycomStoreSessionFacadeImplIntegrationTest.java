package com.wiley.facades.wileycom.storesession.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.strategies.calculation.OrderRequiresCalculationStrategy;
import de.hybris.platform.site.BaseSiteService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Optional;

import javax.annotation.Resource;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.i18n.WileyCountryService;
import com.wiley.core.wileyb2c.i18n.Wileyb2cI18NService;


/**
 * Default integration test for {@link WileycomStoreSessionFacadeImpl}.
 */
@IntegrationTest
public class WileycomStoreSessionFacadeImplIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	public static final Logger LOG = LoggerFactory.getLogger(WileycomStoreSessionFacadeImplIntegrationTest.class);

	public static final String AU_LEGACY_CART_URL = "https://testhost1:testport1/CGI-BIN/lansajson?procfun+shopcart5+sh5fn90";
	public static final String DE_LEGACY_CART_URL = "https://testhost2:testport2/CGI-BIN/lansajson?procfun+shopcart5+sh5fn90";
	public static final String US_LEGACY_CART_URL = "https://testhost3:testport3/CGI-BIN/lansajson?procfun+shopcart5+sh5fn90";

	public static final String AU_COUNTRY_CODE = "AU";
	public static final String DE_COUNTRY_CODE = "DE";
	public static final String US_COUNTRY_CODE = "US";

	@Resource
	private WileycomStoreSessionFacadeImpl wileyb2cStoreSessionFacade;

	@Resource
	private CartService cartService;

	@Resource
	private OrderRequiresCalculationStrategy orderRequiresCalculationStrategy;

	@Resource
	private WileyCountryService wileyCountryService;

	@Resource
	private Wileyb2cI18NService wileyb2cI18NService;

	@Resource
	private BaseSiteService baseSiteService;

	@Test
	public void testSetCurrentCountry()
	{
		// Given
		final String countryCA = "CA";
		baseSiteService.setCurrentBaseSite("wileyb2c", true);
		final CountryData sessionCountry = wileyb2cStoreSessionFacade.getSessionCountry();
		assertNull(sessionCountry);

		final CartModel sessionCart = cartService.getSessionCart();
		assertNotNull(sessionCart);
		assertTrue(orderRequiresCalculationStrategy.requiresCalculation(sessionCart));

		// When
		final Optional<String> optionalCountryIsoCode = wileyb2cStoreSessionFacade.setCurrentCountry(countryCA);

		// Then
		final CountryData actualSessionCountry = wileyb2cStoreSessionFacade.getSessionCountry();

		assertNotNull(actualSessionCountry);
		assertNotNull(optionalCountryIsoCode);

		assertTrue(optionalCountryIsoCode.isPresent());

		assertEquals(countryCA, actualSessionCountry.getIsocode());
		assertEquals(countryCA, optionalCountryIsoCode.get());

		assertFalse(orderRequiresCalculationStrategy.requiresCalculation(sessionCart));
	}

	@Test
	public void testSetCurrentCountryWhenCountryIsNotFound()
	{
		// Given
		final String countryBlabla = "blabla";
		final CountryData sessionCountry = wileyb2cStoreSessionFacade.getSessionCountry();
		assertNull(sessionCountry);

		// When
		try
		{
			wileyb2cStoreSessionFacade.setCurrentCountry(countryBlabla);
			fail("Expected " + IllegalArgumentException.class);
		}
		catch (final IllegalArgumentException e)
		{
			// Then
			// Success
			assertNull(wileyb2cStoreSessionFacade.getSessionCountry());
		}
	}

	@Test
	public void testLegacyCartUrlPopulated() throws Exception
	{
		//given
		baseSiteService.setCurrentBaseSite("wileyb2c", true);
		importCsv("/wileyfacades/test/WileycomStoreSessionFacadeImplIntegrationTest/legacy-cart-urls.impex", DEFAULT_ENCODING);

		//when
		testLegacyCartUrlPopulatedForTheCountry(AU_COUNTRY_CODE, AU_LEGACY_CART_URL);
		testLegacyCartUrlPopulatedForTheCountry(DE_COUNTRY_CODE, DE_LEGACY_CART_URL);
		// US is supposed to be default session country
		testLegacyCartUrlPopulatedForTheDefaultCountryIfSessionCountryNotSpecified(US_LEGACY_CART_URL);
	}

	private void testLegacyCartUrlPopulatedForTheCountry(final String countryCode, final String expectedUrl)
	{
		LOG.info("Test legacy cart URL for country: " + countryCode + ". Expected URL: " + expectedUrl);

		//given
		final CountryModel country = wileyCountryService.findCountryByCode(countryCode);
		wileyb2cI18NService.setCurrentCountry(country);

		//when
		final CountryData countryData = wileyb2cStoreSessionFacade.getSessionCountry();

		//then
		assertNotNull(countryData);
		assertNotNull(countryData.getB2cLegacyCartUrl());
		assertEquals(expectedUrl, countryData.getB2cLegacyCartUrl());
		assertEquals(countryCode, countryData.getIsocode());
	}

	private void testLegacyCartUrlPopulatedForTheDefaultCountryIfSessionCountryNotSpecified(final String expectedUrl)
	{
		LOG.info("Test legacy cart URL for default. Expected URL: " + expectedUrl);

		//given
		// can not remove related session attribute completely as NPE occurs in Wileyb2cEurope1PriceFactory
		wileyCountryService.useDefaultCountryAsCurrent();

		//when
		final CountryData countryData = wileyb2cStoreSessionFacade.getSessionCountry();

		//then
		assertNotNull(countryData);
		assertNotNull(countryData.getB2cLegacyCartUrl());
		assertEquals(expectedUrl, countryData.getB2cLegacyCartUrl());
		assertEquals(US_COUNTRY_CODE, countryData.getIsocode());
	}

}