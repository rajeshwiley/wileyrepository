package com.wiley.facades.order.converters.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyExternalDiscountModel;
import com.wiley.facades.product.data.DiscountData;


/**
 * Default unit test for {@link WileyExternalDiscountToDiscountDataPopulator}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyExternalDiscountToDiscountDataPopulatorUnitTest
{

	@Mock
	private CommerceCommonI18NService commerceCommonI18NServiceMock;

	@Mock
	private PriceDataFactory priceDataFactoryMock;

	@InjectMocks
	private WileyExternalDiscountToDiscountDataPopulator wileyExternalDiscountToDiscountDataPopulator;

	// Test data
	@Mock
	private WileyExternalDiscountModel externalDiscountMock;

	@Mock
	private CurrencyModel currencyModelMock;

	@Mock
	private PriceData priceDataMock;

	@Before
	public void setUp() throws Exception
	{
		when(priceDataFactoryMock.create(any(PriceDataType.class), any(BigDecimal.class), eq(currencyModelMock))).thenReturn(
				priceDataMock);
	}

	@Test
	public void testPopulateRelativeDiscount()
	{
		// Given
		when(commerceCommonI18NServiceMock.getCurrentCurrency()).thenReturn(currencyModelMock);

		final Double discountValue = 25.0;
		final Boolean isAbsolute = false;
		mockExternalDiscount(discountValue, isAbsolute);
		DiscountData discountData = new DiscountData();

		// When
		wileyExternalDiscountToDiscountDataPopulator.populate(externalDiscountMock, discountData);

		// Then
		assertEquals("[Absolute] attribute should equal expected value.", isAbsolute, discountData.getAbsolute());
		assertEquals("Discount value should equal expected one.", discountValue, discountData.getRelativeValue());
	}

	@Test
	public void testPopulateAbsoluteDiscount()
	{
		// Given
		when(commerceCommonI18NServiceMock.getCurrentCurrency()).thenReturn(currencyModelMock);

		final Double discountValue = 235.5;
		final Boolean isAbsolute = true;
		mockExternalDiscount(discountValue, isAbsolute);
		DiscountData discountData = new DiscountData();

		// When
		wileyExternalDiscountToDiscountDataPopulator.populate(externalDiscountMock, discountData);

		// Then
		assertEquals("[Absolute] attribute should equal expected value.", isAbsolute, discountData.getAbsolute());
		assertSame("Discount.price should be expected object.", priceDataMock, discountData.getPrice());
	}

	@Test
	public void testPopulateWithDefaultCurrency()
	{
		// Given
		when(commerceCommonI18NServiceMock.getCurrentCurrency()).thenReturn(null);
		when(commerceCommonI18NServiceMock.getDefaultCurrency()).thenReturn(currencyModelMock);

		final Double discountValue = 235.5;
		final Boolean isAbsolute = true;
		mockExternalDiscount(discountValue, isAbsolute);
		DiscountData discountData = new DiscountData();

		// When
		wileyExternalDiscountToDiscountDataPopulator.populate(externalDiscountMock, discountData);

		// Then
		assertEquals("[Absolute] attribute should equal expected value.", isAbsolute, discountData.getAbsolute());
		assertSame("Discount.price should be expected object.", priceDataMock, discountData.getPrice());
		verify(commerceCommonI18NServiceMock).getCurrentCurrency();
		verify(commerceCommonI18NServiceMock).getDefaultCurrency();
	}

	private void mockExternalDiscount(final Double discountValue, final Boolean isAbsolute)
	{
		when(externalDiscountMock.getAbsolute()).thenReturn(isAbsolute);
		when(externalDiscountMock.getValue()).thenReturn(discountValue);
	}


}