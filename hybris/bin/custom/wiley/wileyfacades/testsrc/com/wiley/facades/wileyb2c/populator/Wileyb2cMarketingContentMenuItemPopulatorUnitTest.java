package com.wiley.facades.wileyb2c.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorcms.model.components.ProductReferencesComponentModel;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.facades.cms.data.MarketingContentMenuItemData;
import com.wiley.facades.wiley.util.WileySemanticUrlGenerator;


/**
 * Default unit test for {@link Wileyb2cMarketingContentMenuItemPopulator}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cMarketingContentMenuItemPopulatorUnitTest
{
	@Mock
	private ProductReferencesComponentModel productReferencesComponentModelMock;

	@Mock
	private WileySemanticUrlGenerator wileySemanticUrlGeneratorMock;

	@InjectMocks
	private Wileyb2cMarketingContentMenuItemPopulator wileyb2cMarketingContentMenuItemPopulatorMock;

	@Before
	public void setUp() throws Exception
	{
		when(productReferencesComponentModelMock.getTitle()).thenReturn("Product References Component Title");
	}

	@Test
	public void shouldPopulateTitleAndAnchorLinkWhenParamsAreValid()
	{
		final MarketingContentMenuItemData contentMenuItemData = new MarketingContentMenuItemData();

		when(productReferencesComponentModelMock.getUid()).thenReturn("Product References Component Uid");
		when(wileySemanticUrlGeneratorMock.generateFor("Product References Component Uid"))
				.thenReturn("product-references-component-uid");

		wileyb2cMarketingContentMenuItemPopulatorMock.populate(productReferencesComponentModelMock, contentMenuItemData);

		assertEquals(contentMenuItemData.getTitle(), "Product References Component Title");
		assertEquals(contentMenuItemData.getAnchorLink(), "#product-references-component-uid-section");
	}

	@Test
	public void shouldPopulateAnchorLinkAsEmptyStringWhenComponentModelUidIsNull()
	{
		final MarketingContentMenuItemData contentMenuItemData = new MarketingContentMenuItemData();

		when(productReferencesComponentModelMock.getUid()).thenReturn(null);
		when(wileySemanticUrlGeneratorMock.generateFor(null)).thenReturn("");

		wileyb2cMarketingContentMenuItemPopulatorMock.populate(productReferencesComponentModelMock, contentMenuItemData);

		assertEquals(contentMenuItemData.getAnchorLink(), "#-section");
	}

	@Test
	public void shouldPopulateTitleWhenComponentModelTitleIsNull()
	{
		final MarketingContentMenuItemData contentMenuItemData = new MarketingContentMenuItemData();

		when(productReferencesComponentModelMock.getTitle()).thenReturn(null);

		wileyb2cMarketingContentMenuItemPopulatorMock.populate(productReferencesComponentModelMock, contentMenuItemData);

		assertNull(contentMenuItemData.getTitle());
	}
}