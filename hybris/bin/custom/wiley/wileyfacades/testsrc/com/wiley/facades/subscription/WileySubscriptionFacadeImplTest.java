package com.wiley.facades.subscription;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.facades.product.data.WileySubscriptionData;
import com.wiley.facades.subscription.impl.WileySubscriptionFacadeImpl;
import com.wiley.facades.subscription.impl.WileySubscriptionFacadeImplUnitTest;
import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.user.UserService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Resource;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


/**
 * Integration test for {@link WileySubscriptionFacadeImpl}.
 */
@IntegrationTest
public class WileySubscriptionFacadeImplTest extends AbstractWileyServicelayerTransactionalTest
{


	private static final String TEST_USER2 = "user2";

	private static final String TEST_USER1 = "user1";

	private static final String TEST_NAME = "AGS-legacy 1 months subscription";

	private static final String TEST_CODE = "testSubscriptionCode1";

	@Resource
	private UserService userService;

	@Resource
	private CartService cartService;

	@Resource
	private CommerceCartService commerceCartService;

	@Resource
	private WileySubscriptionFacadeImpl wileySubscriptionFacade;

	@Resource
	private I18NService i18nService;


	@Before
	public void setUp() throws Exception
	{
		importCsv("/wileycore/test/subscription/testSubscriptions.impex", DEFAULT_ENCODING);

	}

	@Test
	public void getActiveSubscription()
	{
		// Given
		userService.setCurrentUser(userService.getUserForUID(TEST_USER1)); // setting anonymous customer

		i18nService.setCurrentLocale(Locale.ENGLISH);
		// When
		WileySubscriptionData wileySubscriptionData = wileySubscriptionFacade.getActiveSubscription();

		final ZonedDateTime dt = ZonedDateTime.parse("2017-04-03T04:14:00.235-07:00");

		//Then
		assertEquals(TEST_CODE, wileySubscriptionData.getCode());
		assertEquals(TEST_NAME, wileySubscriptionData.getName());
		assertEquals(Date.from(dt.toInstant()), wileySubscriptionData.getEndDate());
		assertTrue("Auto Renew field in not true", wileySubscriptionData.isAutoRenew());
		assertTrue("Renewal Enable field in not true", wileySubscriptionData.isRenewalEnabled());
	}

	@Test
	public void getUserHasNoActiveSubscription()
	{
		// Given

		userService.setCurrentUser(userService.getUserForUID(TEST_USER2));

		// When
		WileySubscriptionData wileySubscriptionData = wileySubscriptionFacade.getActiveSubscription();

		//Then
		Assert.assertEquals(null, wileySubscriptionData);
	}


	@Test
	public void testCurrentUserHasActiveSubscription()
	{
		// Given
		userService.setCurrentUser(userService.getUserForUID(TEST_USER1)); // setting anonymous customer

		// When
		final boolean result = wileySubscriptionFacade.hasCurrentUserActiveSubscription();

		// Then
		assertTrue(String.format("Current user [%s] should have active subscription.", TEST_USER1), result);
	}

	@Test
	public void testCurrentUserHasNoActiveSubscription()
	{
		// Given
		userService.setCurrentUser(userService.getUserForUID(TEST_USER2));

		// When
		final boolean result = wileySubscriptionFacade.hasCurrentUserActiveSubscription();

		// Then
		assertFalse(String.format("Current user [%s] shouldn't have active subscription.", TEST_USER2), result);
	}

	/**
	 * The main purpose of this test method to check application configuration.<br/>
	 * Other test cases are checked at {@link WileySubscriptionFacadeImplUnitTest}.
	 */
	@Test
	public void testHasCurrentUserAnySubscriptionInCartWhenUserHasSubscriptionProduct() throws ImpExException
	{
		// Given
		importCsv("/wileyfacades/test/WileySubscriptionFacadeImplTest"
				+ "/testHasCurrentUserAnySubscriptionInCartWhenUserHasSubscriptionProduct.impex", DEFAULT_ENCODING);

		final CartModel cart = commerceCartService.getCartForCodeAndUser("test-user-cart",
				userService.getUserForUID(TEST_USER1));
		cartService.setSessionCart(cart);

		// When
		final boolean result = wileySubscriptionFacade.hasCurrentUserAnySubscriptionInCart();

		// Then
		assertTrue("Expected that session cart has subscription product.", result);
	}

}
