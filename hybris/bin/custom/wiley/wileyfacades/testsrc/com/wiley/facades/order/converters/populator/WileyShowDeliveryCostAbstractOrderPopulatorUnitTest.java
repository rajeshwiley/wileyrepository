package com.wiley.facades.order.converters.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.core.model.order.AbstractOrderModel;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.wiley.core.product.WileyProductEditionFormatService;


/**
 * Default unit test for {@link WileyShowDeliveryCostAbstractOrderPopulator}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyShowDeliveryCostAbstractOrderPopulatorUnitTest
{
	@InjectMocks
	private WileyShowDeliveryCostAbstractOrderPopulator wileyShowDeliveryCostAbstractOrderPopulator;

	@Mock
	private AbstractOrderModel abstractOrderModelMock;

	@Mock
	private AbstractOrderData abstractOrderDataMock;

	@Mock
	private WileyProductEditionFormatService wileyProductEditionFormatService;

	@Test
	public void testPopulateIfDeliveryCostIsCalculated()
	{
		// Given
		when(wileyProductEditionFormatService.isDigitalCart(abstractOrderModelMock)).thenReturn(false);

		// When
		wileyShowDeliveryCostAbstractOrderPopulator.populate(abstractOrderModelMock, abstractOrderDataMock);

		// Then
		verify(abstractOrderDataMock).setShowDeliveryCost(eq(true));
	}

	@Test
	public void testPopulateIfDeliveryCostIsNotCalculated()
	{
		// Given
		when(wileyProductEditionFormatService.isDigitalCart(abstractOrderModelMock)).thenReturn(true);

		// When
		wileyShowDeliveryCostAbstractOrderPopulator.populate(abstractOrderModelMock, abstractOrderDataMock);

		// Then
		verify(abstractOrderDataMock).setShowDeliveryCost(eq(false));
	}

}