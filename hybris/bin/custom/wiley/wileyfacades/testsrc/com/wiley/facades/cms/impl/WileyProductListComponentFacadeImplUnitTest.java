package com.wiley.facades.cms.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commercefacades.search.solrfacetsearch.impl.DefaultSolrProductSearchFacade;
import de.hybris.platform.commerceservices.search.facetdata.ProductCategorySearchPageData;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyProductModel;
import com.wiley.core.model.WileyPurchaseOptionProductModel;
import com.wiley.core.model.components.WileyProductListComponentModel;
import com.wiley.facades.wiley.visibility.WileyProductVisibilityFacade;

import static com.wiley.facades.cms.impl.WileyProductListComponentFacadeImpl.PRODUCT_OPTIONS;




/**
 * Created by Uladzimir_Barouski on 5/22/2017.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyProductListComponentFacadeImplUnitTest
{
	private static final String PRODUCT_1_CODE = "product 1 code";
	private static final String PRODUCT_2_CODE = "product 2 code";
	private static final String PRODUCT_3_CODE = "product 3 code";
	private static final String PRODUCT_4_CODE = "product 4 code";
	private static final String NOT_EMPTY_QUERY = "not empty query";
	private static final String CAYRGORY_CODE = "cayrgory_code";
	@Mock
	private ProductFacade productFacadeMock;
	@Mock
	private DefaultSolrProductSearchFacade<ProductData> productSearchFacadeMock;
	@Mock
	private WileyProductVisibilityFacade wileyProductVisibilityFacadeMock;
	@InjectMocks
	private WileyProductListComponentFacadeImpl wileyProductListComponentFacade;
	@Mock
	private CategoryModel categoryMock;
	@Mock
	private WileyProductListComponentModel componentMock;
	@Mock
	private WileyProductModel productMock1, productMock2;

	@Mock
	private WileyPurchaseOptionProductModel purchaseOptionMock1, purchaseOptionMock2;

	@Before
	public void setUp() throws Exception
	{
		when(productMock1.getCode()).thenReturn(PRODUCT_1_CODE);
		when(productMock2.getCode()).thenReturn(PRODUCT_2_CODE);
	}

	@Test
	public void getComponentProductsWhenManualEmptyAndSearchQueryEmpty() throws Exception
	{
		//Given
		int productsInResult = 10;
		when(componentMock.getProducts()).thenReturn(Collections.emptyList());
		when(componentMock.getSearchQuery()).thenReturn(StringUtils.EMPTY);
		when(componentMock.getTotalProducts()).thenReturn(productsInResult);
		//When
		final List<ProductData> products = wileyProductListComponentFacade.getComponentProducts(categoryMock, componentMock);
		//Then
		assertTrue(CollectionUtils.isEmpty(products));
		verify(productSearchFacadeMock, never()).categorySearch(any(), any(), any());
	}

	@Test
	public void getComponentProductsWhenManualEmptyAndSearchQueryNotEmpty() throws Exception
	{
		//Given
		int productsInResult = 2;
		when(componentMock.getSearchQuery()).thenReturn(NOT_EMPTY_QUERY);
		when(componentMock.getTotalProducts()).thenReturn(productsInResult);
		initManualProducts();
		//When
		final List<ProductData> products = wileyProductListComponentFacade.getComponentProducts(categoryMock, componentMock);
		//Then
		assertTrue(CollectionUtils.isNotEmpty(products));
		assertEquals(productsInResult, products.size());
		verify(productSearchFacadeMock, never()).categorySearch(any(), any(), any());
	}

	@Test
	public void getComponentProductsWhenManualAndSearchProductsInResult() throws Exception
	{
		//Given
		int productsInResult = 4;
		when(componentMock.getSearchQuery()).thenReturn(NOT_EMPTY_QUERY);
		when(componentMock.getTotalProducts()).thenReturn(productsInResult);
		initManualProducts();
		ProductCategorySearchPageData pageData = initSerarchProducts();
		when(productSearchFacadeMock.categorySearch(eq(CAYRGORY_CODE), anyObject(), anyObject())).thenReturn(pageData);
		when(categoryMock.getCode()).thenReturn(CAYRGORY_CODE);
		//When
		final List<ProductData> products = wileyProductListComponentFacade.getComponentProducts(categoryMock, componentMock);
		//Then
		assertTrue(CollectionUtils.isNotEmpty(products));
		assertEquals(productsInResult, products.size());
		assertEquals(PRODUCT_1_CODE, products.get(0).getCode());
		assertEquals(PRODUCT_2_CODE, products.get(1).getCode());
		assertEquals(PRODUCT_3_CODE, products.get(2).getBaseProduct());
		assertEquals(PRODUCT_4_CODE, products.get(3).getBaseProduct());
		verify(productSearchFacadeMock, times(1)).categorySearch(eq(CAYRGORY_CODE), any(), any());
		verify(productSearchFacadeMock, never()).textSearch(any(SearchStateData.class), any());
	}

	@Test
	public void getComponentProductsWhenCategoryEmpty() throws Exception
	{
		//Given
		int productsInResult = 4;
		when(componentMock.getSearchQuery()).thenReturn(NOT_EMPTY_QUERY);
		when(componentMock.getTotalProducts()).thenReturn(productsInResult);
		initManualProducts();
		ProductCategorySearchPageData pageData = initSerarchProducts();
		when(productSearchFacadeMock.textSearch(any(SearchStateData.class), anyObject())).thenReturn(pageData);
		//When
		final List<ProductData> products = wileyProductListComponentFacade.getComponentProducts(null, componentMock);
		//Then
		assertTrue(CollectionUtils.isNotEmpty(products));
		assertEquals(productsInResult, products.size());
		assertEquals(PRODUCT_1_CODE, products.get(0).getCode());
		assertEquals(PRODUCT_2_CODE, products.get(1).getCode());
		assertEquals(PRODUCT_3_CODE, products.get(2).getBaseProduct());
		assertEquals(PRODUCT_4_CODE, products.get(3).getBaseProduct());
		verify(productSearchFacadeMock, never()).categorySearch(eq(CAYRGORY_CODE), any(), any());
		verify(productSearchFacadeMock, times(1)).textSearch(any(SearchStateData.class), any());
	}

	private ProductCategorySearchPageData initSerarchProducts()
	{
		ProductData productData1 = new ProductData();
		productData1.setBaseProduct(PRODUCT_1_CODE);
		ProductData productData2 = new ProductData();
		productData2.setBaseProduct(PRODUCT_3_CODE);
		ProductData productData3 = new ProductData();
		productData3.setBaseProduct(PRODUCT_4_CODE);
		ProductCategorySearchPageData<SearchStateData, ProductData, CategoryData> pageData = new ProductCategorySearchPageData();
		pageData.setResults(Arrays.asList(productData1, productData2, productData3));
		return pageData;
	}

	private void initManualProducts()
	{
		when(componentMock.getProducts()).thenReturn(Arrays.asList(productMock1, productMock2));
		ProductData productData1 = new ProductData();
		productData1.setCode(PRODUCT_1_CODE);
		ProductData productData2 = new ProductData();
		productData2.setCode(PRODUCT_2_CODE);
		when(productFacadeMock.getProductForCodeAndOptions(PRODUCT_1_CODE, PRODUCT_OPTIONS)).thenReturn(productData1);
		when(productFacadeMock.getProductForCodeAndOptions(PRODUCT_2_CODE, PRODUCT_OPTIONS)).thenReturn(productData2);
		when(wileyProductVisibilityFacadeMock.filterRestrictedProductVariants(productMock1)).thenReturn(productMock1);
		when(wileyProductVisibilityFacadeMock.filterRestrictedProductVariants(productMock2)).thenReturn(productMock2);
		when(productMock1.getVariants()).thenReturn(Arrays.asList(purchaseOptionMock1));
		when(productMock2.getVariants()).thenReturn(Arrays.asList(purchaseOptionMock2));
	}
}