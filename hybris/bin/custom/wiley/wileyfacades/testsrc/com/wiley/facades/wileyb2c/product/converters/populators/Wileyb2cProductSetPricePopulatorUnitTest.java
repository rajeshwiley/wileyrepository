package com.wiley.facades.wileyb2c.product.converters.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.wileyb2c.product.Wileyb2cCommercePriceService;
import com.wiley.facades.wileyb2c.product.data.ProductSetData;



/**
 * Created by Uladzimir_Barouski on 5/5/2017.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cProductSetPricePopulatorUnitTest
{
	public static final double ORIGINAL_PRICE_100 = 100.0;
	public static final double SET_PRICE_60 = 60.0;
	public static final double SET_PRICE_110 = 110.0;
	public static final String COUNTRY = "US";
	public static final double SAVE_PRICE_40 = 40.0;
	@Mock
	private Wileyb2cCommercePriceService priceService;
	@Mock
	private PriceDataFactory priceDataFactory;
	@InjectMocks
	private Wileyb2cProductSetPricePopulator wileyb2cProductSetPricePopulator;

	@Mock
	private ProductModel productModelMock;
	@Mock(answer = Answers.RETURNS_DEEP_STUBS)
	private PriceInformation originalPriceInformation, productSetPriceInformation;

	@Before
	public void setUp() throws Exception
	{
		when(originalPriceInformation.getPriceValue().getValue()).thenReturn(ORIGINAL_PRICE_100);
		when(originalPriceInformation.getPriceValue().getCurrencyIso()).thenReturn(COUNTRY);
		when(productSetPriceInformation.getPriceValue().getCurrencyIso()).thenReturn(COUNTRY);
	}

	@Test
	public void populateWhenSetPriceLessThenOriginal() throws Exception
	{
		//Given
		PriceData originalPriceData = new PriceData();
		originalPriceData.setValue(BigDecimal.valueOf(ORIGINAL_PRICE_100));
		PriceData productSavePriceData = new PriceData();
		productSavePriceData.setValue(BigDecimal.valueOf(SAVE_PRICE_40));
		initPriceInformations(SET_PRICE_60);
		when(priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(ORIGINAL_PRICE_100), COUNTRY)).thenReturn(
				originalPriceData);
		when(priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(SAVE_PRICE_40), COUNTRY)).thenReturn(
				productSavePriceData);
		ProductSetData productSetData = new ProductSetData();
		//When
		wileyb2cProductSetPricePopulator.populate(productModelMock, productSetData);
		//Then
		assertNotNull(productSetData.getOriginalPrice());
		assertNotNull(productSetData.getSave());
		assertEquals(originalPriceData, productSetData.getOriginalPrice());
		assertEquals(productSavePriceData, productSetData.getSave());
	}

	@Test
	public void populateWhenSetPriceGreatThenOriginal() throws Exception
	{
		//Given
		PriceData originalPriceData = new PriceData();
		originalPriceData.setValue(BigDecimal.valueOf(ORIGINAL_PRICE_100));
		initPriceInformations(SET_PRICE_110);
		ProductSetData productSetData = new ProductSetData();
		//When
		wileyb2cProductSetPricePopulator.populate(productModelMock, productSetData);
		//Then
		assertNull(productSetData.getOriginalPrice());
		assertNull(productSetData.getSave());
		verify(priceDataFactory, never()).create(eq(PriceDataType.BUY), any(BigDecimal.class), eq(COUNTRY));
	}

	@Test
	public void populateWhenPricesNull() throws Exception
	{
		//Given
		when(priceService.getComponentsOriginalPrice(productModelMock)).thenReturn(null);
		when(priceService.getWebPriceForProduct(productModelMock)).thenReturn(null);
		ProductSetData productSetData = new ProductSetData();
		//When
		wileyb2cProductSetPricePopulator.populate(productModelMock, productSetData);
		//Then
		assertNull(productSetData.getOriginalPrice());
		assertNull(productSetData.getSave());
		verify(priceDataFactory, never()).create(eq(PriceDataType.BUY), any(BigDecimal.class), eq(COUNTRY));
	}

	private void initPriceInformations(final double productSetPrice)
	{
		when(productSetPriceInformation.getPriceValue().getValue()).thenReturn(productSetPrice);
		when(priceService.getComponentsOriginalPrice(productModelMock)).thenReturn(originalPriceInformation);
		when(priceService.getWebPriceForProduct(productModelMock)).thenReturn(productSetPriceInformation);
	}
}