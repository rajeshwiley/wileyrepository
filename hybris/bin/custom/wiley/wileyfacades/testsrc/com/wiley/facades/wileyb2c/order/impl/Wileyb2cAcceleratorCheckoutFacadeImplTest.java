package com.wiley.facades.wileyb2c.order.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.product.WileyProductEditionFormatService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cAcceleratorCheckoutFacadeImplTest
{
	@InjectMocks
	private Wileyb2cAcceleratorCheckoutFacadeImpl testInstance;

	@Mock
	private WileyProductEditionFormatService mockWileyProductEditionFormatService;

	@Mock
	private CartService mockCartService;

	@Mock
	private CartFacade mockCartFacade;

	@Mock
	private CartModel mockCart;
	
	@Mock
	private AbstractOrderEntryModel mockOrderEntryModel;

	@Before
	public void setUp()
	{
	  
		when(mockCartFacade.hasSessionCart()).thenReturn(true);
		when(mockCartService.getSessionCart()).thenReturn(mockCart);
		
		List<AbstractOrderEntryModel> entries = new ArrayList<AbstractOrderEntryModel>();
		entries.add(mockOrderEntryModel);
		
		when(mockCart.getEntries()).thenReturn(entries);
	}

	@Test
	public void shouldReturnTrueIfCartHasShippingItems()
	{
		when(mockWileyProductEditionFormatService.isDigitalCart(mockCart)).thenReturn(false);
		assertTrue(testInstance.hasShippingItems());
	}

	@Test
	public void shouldReturnFalseIfCartNotHaveShippingItems()
	{
		when(mockWileyProductEditionFormatService.isDigitalCart(mockCart)).thenReturn(true);
		assertFalse(testInstance.hasShippingItems());
	}
}
