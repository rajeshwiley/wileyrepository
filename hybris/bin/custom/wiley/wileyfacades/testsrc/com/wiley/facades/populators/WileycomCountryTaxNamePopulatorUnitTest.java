package com.wiley.facades.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.core.model.c2l.CountryModel;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Created by Georgii_Gavrysh on 8/25/2016.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileycomCountryTaxNamePopulatorUnitTest
{
	@InjectMocks
	private WileycomCountryTaxNamePopulator wileycomCountryTaxNamePopulator;

	@Mock
	private CountryModel countryModelMock;

	@Mock
	private CountryData countryDataMock;

	@Test
	public void test()
	{
		when(countryModelMock.getTaxName()).thenReturn("taxLabel");
		wileycomCountryTaxNamePopulator.populate(countryModelMock, countryDataMock);
		verify(countryDataMock).setTaxName("taxLabel");
	}

}
