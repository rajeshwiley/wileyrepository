package com.wiley.facades.search.solrfacetsearch.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.search.facetdata.FacetData;
import de.hybris.platform.commerceservices.search.facetdata.FacetValueData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.search.Breadcrumb;
import de.hybris.platform.solrfacetsearch.search.impl.SolrSearchResult;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.search.solrfacetsearch.WileySearchQuery;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileySearchResponseFacetsPopulatorUnitTest
{
	public static final Integer TEST_FACET_LIMIT = 3;
	public static final String TEST_FACET_CODE = "test facet code";
	public static final String TEST_FACET_CODE_2 = "test facet code 2";
	public static final String TEST_FACET_CODE_3 = "test facet code 3";

	@Mock
	private WileySearchQuery wileySearchQueryMock;

	@Mock
	private FacetData<SolrSearchQueryData> facetData1Mock;

	@Mock
	private FacetValueData<SolrSearchQueryData> facetData1MockValue1, facetData1MockValue2;

	private List<FacetValueData<SolrSearchQueryData>> facetData1MockValues = new ArrayList<>();

	@Mock
	private FacetData<SolrSearchQueryData> facetData2Mock;

	@Mock
	private FacetValueData<SolrSearchQueryData> facetData2MockValue1, facetData2MockValue2, facetData2MockValue3;

	private List<FacetValueData<SolrSearchQueryData>> facetData2MockValues = new ArrayList<>();

	@Mock
	private FacetData<SolrSearchQueryData> facetData3Mock;

	@Mock
	private FacetValueData<SolrSearchQueryData> facetData3MockValue1, facetData3MockValue2, facetData3MockValue3,
			facetData3MockValue4;

	private List<FacetValueData<SolrSearchQueryData>> facetData3MockValues = new ArrayList<>();

	private List<FacetData<SolrSearchQueryData>> facetData = new ArrayList<>();

	@Mock
	private SolrSearchResult solrSearchResultMock;

	@Mock
	private SolrSearchQueryData searchQueryDataMock;

	@Mock
	private IndexedType indexedTypeMock;

	@Mock
	private Breadcrumb breadcrumbMock1, breadcrumbMock2;


	private List<Breadcrumb> breadcrumbMocks = new ArrayList<>();

	@Spy
	private WileySearchResponseFacetsPopulator testInstance;

	@Before
	public void init()
	{
		when(facetData1Mock.getCode()).thenReturn(TEST_FACET_CODE);
		when(facetData1Mock.getValues()).thenReturn(facetData1MockValues);
		facetData1MockValues.addAll(Arrays.asList(facetData1MockValue1, facetData1MockValue2));

		when(facetData2Mock.getCode()).thenReturn(TEST_FACET_CODE_2);
		when(facetData2Mock.getValues()).thenReturn(facetData2MockValues);
		facetData2MockValues.addAll(Arrays.asList(facetData2MockValue1, facetData2MockValue2, facetData2MockValue3));

		when(facetData3Mock.getCode()).thenReturn(TEST_FACET_CODE_3);
		when(facetData3Mock.getValues()).thenReturn(facetData3MockValues);
		facetData3MockValues.addAll(Arrays.asList(facetData3MockValue1, facetData3MockValue2, facetData3MockValue3,
				facetData3MockValue4));

		facetData.addAll(Arrays.asList(facetData1Mock, facetData2Mock, facetData3Mock));
	}

	@Test
	public void testFilterFacetByQueryFacetCodeShouldFilterOutFacetsWhenFacetCodeIsProvidedInQuery()
	{
		// given
		when(solrSearchResultMock.getSearchQuery()).thenReturn(wileySearchQueryMock);
		when(wileySearchQueryMock.getFacetCode()).thenReturn(TEST_FACET_CODE);

		// when
		testInstance.filterFacetByQueryFacetCode(facetData, solrSearchResultMock);

		// then
		Assert.assertTrue(facetData.size() == 1);
		Assert.assertTrue(facetData.get(0).equals(facetData1Mock));
	}

	@Test
	public void testFilterFacetByQueryFacetCodeShouldNotFilterOutAnyFacetsWhenFacetCodeIsNotProvidedInQuery()
	{
		// given
		when(solrSearchResultMock.getSearchQuery()).thenReturn(wileySearchQueryMock);
		when(wileySearchQueryMock.getFacetCode()).thenReturn(null);

		// when
		testInstance.filterFacetByQueryFacetCode(facetData, solrSearchResultMock);

		// then
		Assert.assertTrue(facetData.size() == 3);
		Assert.assertTrue(facetData.containsAll(Arrays.asList(facetData1Mock, facetData2Mock, facetData3Mock)));
	}

	@Test
	public void testBuildFacetHasMoreFacetValuesDataAndRemoveExtraValuesShouldPopulateFacetDataIfLimitProvided()
	{
		// given
		when(solrSearchResultMock.getSearchQuery()).thenReturn(wileySearchQueryMock);
		when(wileySearchQueryMock.getFacetLimit()).thenReturn(TEST_FACET_LIMIT);

		// when
		testInstance.buildFacetHasMoreFacetValuesDataAndRemoveExtraValues(facetData, solrSearchResultMock);

		// then
		Assert.assertTrue(facetData1MockValues.equals(Arrays.asList(facetData1MockValue1, facetData1MockValue2)));
		verify(facetData1Mock).setHasMoreFacetValues(false);

		Assert.assertTrue(
				facetData2MockValues.equals(Arrays.asList(facetData2MockValue1, facetData2MockValue2, facetData2MockValue3)));
		verify(facetData2Mock).setHasMoreFacetValues(false);

		verify(facetData3Mock).setValues(eq(Arrays.asList(facetData3MockValue1, facetData3MockValue2, facetData3MockValue3)));
		verify(facetData3Mock).setHasMoreFacetValues(true);
	}

	@Test
	public void testBuildFacetHasMoreFacetValuesDataAndRemoveExtraValuesShouldNotPopulateFacetDataIfLimitNotProvided()
	{
		// given
		when(solrSearchResultMock.getSearchQuery()).thenReturn(wileySearchQueryMock);
		when(wileySearchQueryMock.getFacetLimit()).thenReturn(null);

		// when
		testInstance.buildFacetHasMoreFacetValuesDataAndRemoveExtraValues(facetData, solrSearchResultMock);

		// then
		Assert.assertTrue(facetData1MockValues.equals(Arrays.asList(facetData1MockValue1, facetData1MockValue2)));
		verify(facetData1Mock, never()).setHasMoreFacetValues(anyBoolean());
		Assert.assertTrue(
				facetData2MockValues.equals(Arrays.asList(facetData2MockValue1, facetData2MockValue2, facetData2MockValue3)));
		verify(facetData2Mock, never()).setHasMoreFacetValues(anyBoolean());
		Assert.assertTrue(facetData3MockValues
				.equals(Arrays.asList(facetData3MockValue1, facetData3MockValue2, facetData3MockValue3, facetData3MockValue4)));
		verify(facetData3Mock, never()).setHasMoreFacetValues(anyBoolean());
	}

	@Test
	public void testBuildFacetHasMoreFacetValuesDataAndRemoveExtraValuesShouldNotPopulateFacetDataIfLimitIsNegative()
	{
		// given
		when(solrSearchResultMock.getSearchQuery()).thenReturn(wileySearchQueryMock);
		when(wileySearchQueryMock.getFacetLimit()).thenReturn(-1);

		// when
		testInstance.buildFacetHasMoreFacetValuesDataAndRemoveExtraValues(facetData, solrSearchResultMock);

		// then
		Assert.assertTrue(facetData1MockValues.equals(Arrays.asList(facetData1MockValue1, facetData1MockValue2)));
		verify(facetData1Mock, never()).setHasMoreFacetValues(anyBoolean());

		Assert.assertTrue(
				facetData2MockValues.equals(Arrays.asList(facetData2MockValue1, facetData2MockValue2, facetData2MockValue3)));
		verify(facetData2Mock, never()).setHasMoreFacetValues(anyBoolean());

		Assert.assertTrue(facetData3MockValues
				.equals(Arrays.asList(facetData3MockValue1, facetData3MockValue2, facetData3MockValue3, facetData3MockValue4)));

		verify(facetData3Mock, never()).setHasMoreFacetValues(anyBoolean());
	}

	@Test
	public void testShouldSetHasMoreFacetValuesIfSingleFacetSelected()
	{
		// given
		when(breadcrumbMock1.getFieldName()).thenReturn(TEST_FACET_CODE);
		when(breadcrumbMock2.getFieldName()).thenReturn(TEST_FACET_CODE_2);
		breadcrumbMocks.addAll(Arrays.asList(breadcrumbMock1, breadcrumbMock2));
		when(solrSearchResultMock.getBreadcrumbs()).thenReturn(breadcrumbMocks);

		when(solrSearchResultMock.getSearchQuery()).thenReturn(wileySearchQueryMock);
		when(wileySearchQueryMock.getFacetLimit()).thenReturn(TEST_FACET_LIMIT);

		// when
		testInstance.buildFacetHasMoreFacetValuesDataAndRemoveExtraValues(facetData, solrSearchResultMock);

		// then
		Assert.assertTrue(facetData1MockValues.equals(Arrays.asList(facetData1MockValue1, facetData1MockValue2)));
		// facetData1Mock values and breadcrumbs number is equal to limit => view more should not be displayed
		verify(facetData1Mock).setHasMoreFacetValues(false);

		Assert.assertTrue(
				facetData2MockValues.equals(Arrays.asList(facetData2MockValue1, facetData2MockValue2, facetData2MockValue3)));
		// facetData2Mock values and breadcrumbs number is higher then limit => view more should not be displayed
		verify(facetData2Mock).setHasMoreFacetValues(true);

		verify(facetData3Mock).setValues(eq(Arrays.asList(facetData3MockValue1, facetData3MockValue2, facetData3MockValue3)));
		verify(facetData3Mock).setHasMoreFacetValues(true);
	}

	@Test
	public void testBuildFacets()
	{
		// given
		doReturn(facetData).when(testInstance).superBuildFacets(solrSearchResultMock, searchQueryDataMock,
				indexedTypeMock);
		when(solrSearchResultMock.getSearchQuery()).thenReturn(wileySearchQueryMock);

		// when
		final List<FacetData<SolrSearchQueryData>> result = testInstance.buildFacets(solrSearchResultMock, searchQueryDataMock,
				indexedTypeMock);

		// then
		verify(testInstance).superBuildFacets(solrSearchResultMock, searchQueryDataMock, indexedTypeMock);
		verify(testInstance).filterFacetByQueryFacetCode(facetData, solrSearchResultMock);
		verify(testInstance).buildFacetHasMoreFacetValuesDataAndRemoveExtraValues(facetData, solrSearchResultMock);
		Assert.assertEquals(facetData, result);
	}
}
