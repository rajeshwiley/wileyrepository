package com.wiley.facades.order.converters.populator;

import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.order.strategies.WileyOrderCancelabilityStrategy;


@RunWith(MockitoJUnitRunner.class)
public class WileyOrderEntryCancellablePopulatorTest
{

	public static final boolean CANCELLABLE = true;
	@Mock
	private WileyOrderCancelabilityStrategy wileyOrderCancelabilityStrategy;

	@InjectMocks
	private WileyOrderEntryCancellablePopulator testInstance;

	@Before
	public void setUp() throws Exception
	{
	}

	@Test
	public void shouldDoNothingIfEntryBelongsToNonOrderType()
	{
		//Given
		AbstractOrderEntryModel entry = new AbstractOrderEntryModel();
		entry.setOrder(new CartModel());
		OrderEntryData entryData = new OrderEntryData();
		entryData.setStatus(OrderStatus.CREATED);
		//When
		testInstance.populate(entry, entryData);
		//Then
		verify(wileyOrderCancelabilityStrategy, never()).isCancelableOrderEntry(any(), any());
	}

	@Test
	public void shouldDoNothingIfEntryHasNoStatus()
	{
		//Given
		OrderEntryData entryData = new OrderEntryData();
		//When
		testInstance.populate(null, entryData);
		//Then
		verify(wileyOrderCancelabilityStrategy, never()).isCancelableOrderEntry(any(), any());
	}

	@Test
	public void shouldSetProperCancellableStatus()
	{
		//Given
		AbstractOrderEntryModel entry = new AbstractOrderEntryModel();
		entry.setOrder(new OrderModel());
		when(wileyOrderCancelabilityStrategy.isCancelableOrderEntry(any(), any())).thenReturn(CANCELLABLE);
		OrderEntryData entryData = new OrderEntryData();
		entryData.setStatus(OrderStatus.CREATED);
		//When
		testInstance.populate(entry, entryData);
		//Then
		assertTrue(entryData.isCancelable());

	}
}