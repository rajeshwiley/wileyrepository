package com.wiley.facades.populators;



import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.enums.ClassificationAttributeTypeEnum;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.classification.features.FeatureValue;
import de.hybris.platform.commercefacades.product.data.ClassificationData;
import de.hybris.platform.commercefacades.product.data.FeatureData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.wiley.core.enums.ClassAttributeAssignmentTypeEnum;


/**
 * Test suite for {@link WileyProductFeatureListPopulator}
 */
@UnitTest
public class WileyProductFeatureListPopulatorTest {

  private static final String SOME_DESCRIPTION = "some description";

  private static final String CLASSIFICATION_CLASS_CODE = "classClassCode";
  private static final String CLASSIFICATION_CLASS_CODE_2 = "classClassCode2";

  @Mock
  private Converter<ClassificationClassModel, ClassificationData> classificationConverter;
  @Mock
  private Converter<Feature, FeatureData> featureConverter;

  private WileyProductFeatureListPopulator productFeatureListPopulator;

  @Mock
  private FeatureValue featureValue;

  @Mock
  private ClassAttributeAssignmentModel assignmentModel1;

  @Mock
  private ClassAttributeAssignmentModel assignmentModel2;

  @Mock
  private ClassificationClassModel classificationClassModel1;

  @Mock
  private ClassificationClassModel classificationClassModel2;

  @Mock
  private ClassificationData classificationData;

  @Mock
  private ClassificationData classificationData2;

  @Mock
  private FeatureData featureData;

  @Mock
  private FeatureList source;

  @Mock
  private Feature feature;

  @Mock
  private Feature feature2;


  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);

    productFeatureListPopulator = new WileyProductFeatureListPopulator();
    productFeatureListPopulator.setFeatureConverter(featureConverter);
    productFeatureListPopulator.setClassificationConverter(classificationConverter);

  }


  @Test
  public void testPopulate() {

    final List<Feature> features = new ArrayList<Feature>();
    features.add(feature);
    features.add(feature2);
    given(featureConverter.convert(feature)).willReturn(featureData);
    given(classificationConverter.convert(classificationClassModel1))
        .willReturn(classificationData);
    given(classificationConverter.convert(classificationClassModel2)).willReturn(
        classificationData2);
    given(classificationClassModel1.getCode()).willReturn(CLASSIFICATION_CLASS_CODE);
    given(classificationClassModel2.getCode()).willReturn(CLASSIFICATION_CLASS_CODE_2);
    given(feature.getClassAttributeAssignment()).willReturn(assignmentModel1);
    given(feature2.getClassAttributeAssignment()).willReturn(assignmentModel2);
    given(assignmentModel1.getClassificationClass()).willReturn(classificationClassModel1);
    given(assignmentModel1.getAttributeType()).willReturn(ClassificationAttributeTypeEnum.STRING);

    given(assignmentModel2.getClassificationClass()).willReturn(classificationClassModel2);
    given(assignmentModel2.getType()).willReturn(ClassAttributeAssignmentTypeEnum.INCLUDES);
    given(assignmentModel2.getAttributeType()).willReturn(ClassificationAttributeTypeEnum.BOOLEAN);
    given(assignmentModel2.getDetails()).willReturn(SOME_DESCRIPTION);
    given(feature.getValues()).willReturn(Collections.singletonList(featureValue));
    given(feature2.getValues()).willReturn(Collections.singletonList(featureValue));
    given(source.getFeatures()).willReturn(features);

    final ProductData result = new ProductData();
    productFeatureListPopulator.populate(source, result);

    Assert.assertEquals(1, result.getClassifications().size());
    Assert.assertEquals(classificationData2, result.getClassifications().iterator().next());
  }
}
