package com.wiley.facades.flow.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.order.CartService;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.facades.i18n.WileyI18NFacade;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyCheckoutFlowFacadeImplTest
{
	private static final String COUNTRY_WITH_REGIONS_ISO = "US";
	private static final String COUNTRY_WITHOUT_REGIONS_ISO = "UA";
	private static final String REGION_ISO = "AL";

	@InjectMocks
	private WileyCheckoutFlowFacadeImpl testInstance;

	@Mock
	private WileyI18NFacade mockWileyI18NFacade;

	@Mock
	private CartService mockCartService;

	@Mock
	private CartModel mockCartModel;

	@Mock
	private AddressModel mockPaymentAddress;
	
	@Mock
	private CountryModel mockCountryModel;
	
	@Mock
	private RegionModel mockRegionModel;


	@Before
	public void setUp()
	{
		when(mockWileyI18NFacade.isDisplayRegions(COUNTRY_WITH_REGIONS_ISO)).thenReturn(true);
		when(mockWileyI18NFacade.isDisplayRegions(COUNTRY_WITHOUT_REGIONS_ISO)).thenReturn(false);

		when(mockCartService.getSessionCart()).thenReturn(mockCartModel);
		when(mockCartService.hasSessionCart()).thenReturn(true);
		
		when(mockCartModel.getPaymentAddress()).thenReturn(mockPaymentAddress);
		
		when(mockPaymentAddress.getCountry()).thenReturn(mockCountryModel);
		when(mockPaymentAddress.getRegion()).thenReturn(mockRegionModel);
		
		when(mockCountryModel.getIsocode()).thenReturn(COUNTRY_WITH_REGIONS_ISO);
		when(mockRegionModel.getIsocode()).thenReturn(REGION_ISO);
	}

	@Test
	public void shouldReturnPaymentAddressValidTrueIfAddressIsValid()
	{
		boolean hasValidPaymentAddress = testInstance.hasValidPaymentAddress();

		assertTrue(hasValidPaymentAddress);
	}

	@Test
	public void shouldReturnPaymentAddressValidFalseIfNoCart()
	{
		when(mockCartService.hasSessionCart()).thenReturn(false);

		boolean hasValidPaymentAddress = testInstance.hasValidPaymentAddress();

		assertFalse(hasValidPaymentAddress);
	}


	@Test
	public void shouldReturnPaymentAddressValidFalseIfNoAddress()
	{
		when(mockCartModel.getPaymentAddress()).thenReturn(null);

		boolean hasValidPaymentAddress = testInstance.hasValidPaymentAddress();

		assertFalse(hasValidPaymentAddress);
	}

	@Test
	public void shouldReturnPaymentAddressValidFalseIfNoCountry()
	{
		when(mockPaymentAddress.getCountry()).thenReturn(null);

		boolean hasValidPaymentAddress = testInstance.hasValidPaymentAddress();

		assertFalse(hasValidPaymentAddress);
	}

	@Test
	public void shouldReturnPaymentAddressValidFalseIfNoCountryIso()
	{
		when(mockCountryModel.getIsocode()).thenReturn(null);

		boolean hasValidPaymentAddress = testInstance.hasValidPaymentAddress();

		assertFalse(hasValidPaymentAddress);
	}

	@Test
	public void shouldReturnPaymentAddressValidFalseIfNoRegionButItIsRequired()
	{
		when(mockPaymentAddress.getRegion()).thenReturn(null);

		boolean hasValidPaymentAddress = testInstance.hasValidPaymentAddress();

		assertFalse(hasValidPaymentAddress);
	}

	@Test
	public void shouldReturnPaymentAddressValidFalseIfNoRegionIsoButItIsRequired()
	{
		when(mockRegionModel.getIsocode()).thenReturn(null);

		boolean hasValidPaymentAddress = testInstance.hasValidPaymentAddress();

		assertFalse(hasValidPaymentAddress);
	}

	@Test
	public void shouldReturnPaymentAddressValidTrueIfNoRegionIsoButItIsOptional()
	{
		when(mockCountryModel.getIsocode()).thenReturn(COUNTRY_WITHOUT_REGIONS_ISO);

		boolean hasValidPaymentAddress = testInstance.hasValidPaymentAddress();

		assertTrue(hasValidPaymentAddress);
	}
}
