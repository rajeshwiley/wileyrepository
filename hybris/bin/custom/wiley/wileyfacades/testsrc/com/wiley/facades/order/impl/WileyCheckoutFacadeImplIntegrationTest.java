package com.wiley.facades.order.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;

import java.util.Date;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;


/**
 * Integration test for {@link WileyCheckoutFacadeImpl}
 */
@IntegrationTest
public class WileyCheckoutFacadeImplIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	public static final String CART_GUID = "test-cart-guid";
	public static final String BASE_SITE = "asSite";
	public static final String CUSTOMER_ID = "test@test.com";
	public static final String TEST_TAX_NUMBER = "TEST_TAX_NUMBER";
	public static final Date TAX_NUMBER_EXPIRATION_DATE = new Date();

	@Resource
	private WileyCheckoutFacadeImpl wileyCheckoutFacade;

	@Resource
	private BaseSiteService baseSiteService;

	@Resource
	private UserService userService;

	@Resource
	private CommerceCartService commerceCartService;

	@Resource
	private CartService cartService;



	@Before
	public void setUp() throws Exception
	{
		importCsv("/wileyfacades/test/WileyCheckoutFacadeImplIntegrationTest/cart.impex", DEFAULT_ENCODING);

		final BaseSiteModel baseSite = baseSiteService.getBaseSiteForUID(BASE_SITE);
		final UserModel userForUID = userService.getUserForUID(CUSTOMER_ID);
		final CartModel cart = commerceCartService.getCartForGuidAndSiteAndUser(CART_GUID, baseSite, userForUID);
		baseSiteService.setCurrentBaseSite(baseSite, true);
		userService.setCurrentUser(userForUID);
		cartService.setSessionCart(cart);
	}

	@Test
	public void testSetTaxExemptionDetails()
	{
		// given

		// when
		wileyCheckoutFacade.setTaxExemptionDetails(TEST_TAX_NUMBER, TAX_NUMBER_EXPIRATION_DATE);

		// then
		CartModel sessionCart = cartService.getSessionCart();
		Assert.assertEquals(CART_GUID, sessionCart.getGuid());
		Assert.assertEquals(TEST_TAX_NUMBER, sessionCart.getTaxNumber());
		Assert.assertEquals(TAX_NUMBER_EXPIRATION_DATE, sessionCart.getTaxNumberExpirationDate());
	}

	@Test
	public void testSetVatRegistrationDetails()
	{
		// given

		// when
		wileyCheckoutFacade.setVatRegistrationDetails(TEST_TAX_NUMBER);

		// then
		CartModel sessionCart = cartService.getSessionCart();
		Assert.assertEquals(CART_GUID, sessionCart.getGuid());
		Assert.assertEquals(TEST_TAX_NUMBER, sessionCart.getTaxNumber());
		Assert.assertNull(sessionCart.getTaxNumberExpirationDate());
	}
}
