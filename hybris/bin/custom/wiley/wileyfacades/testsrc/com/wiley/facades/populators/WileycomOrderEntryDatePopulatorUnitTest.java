package com.wiley.facades.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Test for {@link WileycomOrderEntryDatePopulator}
 */
@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class WileycomOrderEntryDatePopulatorUnitTest
{
	private OrderEntryData targetEntry;

	@Mock
	private AbstractOrderEntryModel sourceEntryMock;

	@Mock
	private AbstractOrderModel orderMock;

	@InjectMocks
	private WileycomOrderEntryDatePopulator wileycomOrderEntryDatePopulator;

	@Before
	public void setup()
	{
		targetEntry = new OrderEntryData();
	}

	@Test
	public void testWhenOrderIsPassed()
	{
		//Given
		final Date date = new Date();
		when(sourceEntryMock.getOrder()).thenReturn(orderMock);
		when(orderMock.getDate()).thenReturn(date);

		//When
		wileycomOrderEntryDatePopulator.populate(sourceEntryMock, targetEntry);

		//Than
		assertNotNull(targetEntry.getPurchaseDate());
		assertEquals(date, targetEntry.getPurchaseDate());
	}

	@Test
	public void testWhenOrderIsNull()
	{
		//Given

		when(sourceEntryMock.getOrder()).thenReturn(null);

		//When
		wileycomOrderEntryDatePopulator.populate(sourceEntryMock, targetEntry);

		//Than
		assertNull(targetEntry.getPurchaseDate());
		verify(orderMock, times(0)).getDate();
	}
}
