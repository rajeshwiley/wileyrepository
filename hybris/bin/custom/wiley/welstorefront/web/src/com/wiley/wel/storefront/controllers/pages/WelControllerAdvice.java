package com.wiley.wel.storefront.controllers.pages;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.InitBinder;


@ControllerAdvice
public class WelControllerAdvice
{
	@Autowired
	private StringTrimmerEditor stringTrimmer;

	@InitBinder
	public void initBinder(final WebDataBinder binder)
	{
		binder.registerCustomEditor(String.class, stringTrimmer);
	}
}
