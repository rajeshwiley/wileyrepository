/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wel.storefront.controllers.cms;

import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;

import org.apache.commons.lang.StringUtils;

import com.wiley.storefrontcommons.controllers.cms.AbstractWileyCMSComponentController;
import com.wiley.welags.storefrontcommons.constants.WelagsstorefrontcommonsConstants;


/**
 * Abstract Controller for CMS Components
 *
 * @param <T>
 * 		the type parameter
 */
public abstract class AbstractCMSComponentController<T extends AbstractCMSComponentModel>
		extends AbstractWileyCMSComponentController<T>
{
	/**
	 * Gets view.
	 *
	 * @param component
	 * 		the component
	 * @return the view
	 */
	protected String getView(final T component)
	{
		// build a jsp response based on the component type
		return WelagsstorefrontcommonsConstants.Views.Cms.COMPONENT_PREFIX + StringUtils.lowerCase(getTypeCode(component));
	}
}
