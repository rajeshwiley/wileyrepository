package com.wiley.wel.storefront.controllers.cms;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.wiley.core.model.components.VariantProductComparisonTableComponentModel;
import com.wiley.facades.product.data.ProductComparisonData;
import com.wiley.facades.wel.product.WelProductFacade;
import com.wiley.wel.storefront.controllers.ControllerConstants;

import static com.wiley.wel.storefront.controllers.ControllerConstants.ProductOptions.getProductLevelOptionsForComparison;


/**
 * Component for VariantProductComparisonTableComponent
 */
@Controller("VariantProductComparisonTableComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.VARIANT_PRODUCT_COMPARISON_TABLE_COMPONENT)
public class VariantProductComparisonTableComponentController
		extends AbstractCMSComponentController<VariantProductComparisonTableComponentModel>
{
	private static final Logger LOG = Logger.getLogger(VariantProductComparisonTableComponentController.class);

	@Autowired
	private WelProductFacade welProductFacade;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model,
			final VariantProductComparisonTableComponentModel component)
	{

		if (CollectionUtils.isNotEmpty(component.getProducts()))
		{
			final ProductComparisonData productComparisonData = welProductFacade.getComparisonProductData(
					component.getProducts(), getProductLevelOptionsForComparison(), component.getCmsProductAttributes());

			model.addAttribute("productComparisonData", productComparisonData);
		}
	}
}
