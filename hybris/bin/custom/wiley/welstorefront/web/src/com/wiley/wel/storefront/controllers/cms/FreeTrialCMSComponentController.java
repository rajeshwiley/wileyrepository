package com.wiley.wel.storefront.controllers.cms;

import de.hybris.platform.acceleratorservices.data.RequestContextData;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.order.CheckoutFacade;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.model.product.ProductModel;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.model.components.FreeTrialCMSComponentModel;
import com.wiley.facades.product.WileyFreeTrialProductFacade;
import com.wiley.facades.product.data.FreeTrialData;
import com.wiley.wel.storefront.controllers.ControllerConstants;
import com.wiley.wel.storefront.forms.WileyFreeTrialForm;


/**
 * Created by Uladzimir_Barouski on 3/17/2016.
 */
@Controller("FreeTrialCMSComponentController")
@Scope("tenant")
@RequestMapping(value = ControllerConstants.Actions.Cms.FREE_TRIAL_COMPONENT)
public class FreeTrialCMSComponentController extends AbstractCMSComponentController<FreeTrialCMSComponentModel>
{

	private static final Logger LOG = Logger.getLogger(FreeTrialCMSComponentController.class);
	private static final String WILEY_FREE_TRIAL_FORM = "wileyFreeTrialForm";

	@Autowired
	private WileyFreeTrialProductFacade freeTrialProductFacade;

	@Autowired
	private CustomerFacade customerFacade;
	@Autowired
	private UserFacade userFacade;
	@Autowired
	private CheckoutFacade checkoutFacade;

	@Resource
	private I18NFacade i18NFacade;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final FreeTrialCMSComponentModel component)
	{

		RequestContextData requestContextData = getBean(request, "requestContextData", RequestContextData.class);
		ProductModel productModel = requestContextData.getProduct();

		FreeTrialData freeTrialProduct = freeTrialProductFacade.getFreeTrialForProduct(productModel.getCode());
		if (freeTrialProduct != null)
		{
			model.addAttribute("freeTrial", freeTrialProduct);
			WileyFreeTrialForm freeTrialForm;
			if (!model.containsAttribute(WILEY_FREE_TRIAL_FORM))
			{
				freeTrialForm = new WileyFreeTrialForm();
			}
			else
			{
				freeTrialForm = (WileyFreeTrialForm) model.asMap().get(WILEY_FREE_TRIAL_FORM);
			}
			populateFreeTrialForm(model, freeTrialForm);
			populateFreeTrialFormCountry(model, freeTrialForm);
			model.addAttribute(WILEY_FREE_TRIAL_FORM, freeTrialForm);
			model.addAttribute("countries", checkoutFacade.getBillingCountries());
		}
	}

	private void populateFreeTrialForm(final Model model, final WileyFreeTrialForm freeTrialForm)
	{
		if (!userFacade.isAnonymousUser())
		{
			CustomerData currentCustomer = customerFacade.getCurrentCustomer();
			freeTrialForm.setFirstName(currentCustomer.getFirstName());
			freeTrialForm.setLastName(currentCustomer.getLastName());
			freeTrialForm.setEmail(currentCustomer.getUid());
			freeTrialForm.setEmployerOrSchool(currentCustomer.getCurrentEmployerSchool());
			if (currentCustomer.getDefaultBillingAddress() != null)
			{
				freeTrialForm.setCountry(currentCustomer.getDefaultBillingAddress().getCountry().getIsocode());
				model.addAttribute("selectedCountryName", currentCustomer.getDefaultBillingAddress().getCountry().getName());
				model.addAttribute("isCountryDisabled", true);
			}
		}
	}

	private void populateFreeTrialFormCountry(final Model model, final WileyFreeTrialForm freeTrialForm)
	{
		if (StringUtils.isEmpty(freeTrialForm.getCountry()))
		{
			freeTrialForm.setCountry(WileyCoreConstants.DEFAULT_COUNTRY_ISO_CODE);
			model.addAttribute("selectedCountryName",
					i18NFacade.getCountryForIsocode(WileyCoreConstants.DEFAULT_COUNTRY_ISO_CODE).getName());
		}
	}
}
