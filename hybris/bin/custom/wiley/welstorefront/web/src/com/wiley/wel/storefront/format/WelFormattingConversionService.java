package com.wiley.wel.storefront.format;

import java.io.Serializable;

import org.springframework.format.support.DefaultFormattingConversionService;


/**
 * Class is used to fix an issue of putting non Serializable  "DefaultFormattingConversionService" into Redis.
 */
public class WelFormattingConversionService extends DefaultFormattingConversionService implements Serializable
{
}
