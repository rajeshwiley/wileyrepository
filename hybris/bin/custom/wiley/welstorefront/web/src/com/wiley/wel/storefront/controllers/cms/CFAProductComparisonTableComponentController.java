package com.wiley.wel.storefront.controllers.cms;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.wiley.core.model.components.CFAProductComparisonTableComponentModel;
import com.wiley.facades.product.data.WileyProductListCFAData;
import com.wiley.facades.wel.product.WelProductFacade;
import com.wiley.wel.storefront.controllers.ControllerConstants;




@Controller("CFAProductComparisonTableComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.CFA_PRODUCT_COMPARISON_TABLE_COMPONENT)
public class CFAProductComparisonTableComponentController
		extends AbstractCMSComponentController<CFAProductComparisonTableComponentModel>
{

	@Resource(name = "welProductFacade")
	private WelProductFacade productFacade;

	private static final String CFA_CATEGORY = "WEL_CFA_CATEGORY";

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model,
			final CFAProductComparisonTableComponentModel component)
	{
		WileyProductListCFAData cfaProducts = productFacade.getProductListCFAData(CFA_CATEGORY);
		model.addAttribute(cfaProducts);
	}
}