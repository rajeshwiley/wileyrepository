package com.wiley.wel.storefront.aspects;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.PredicateUtils;

import com.wiley.recaptcha.aspects.WileyAspectRequestStrategy;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public class WileyReCaptchaRegisterRequestStrategy implements WileyAspectRequestStrategy
{
	@Override
	public HttpServletRequest getRequest(@NotNull final List<Object> args)
	{
		return (HttpServletRequest) CollectionUtils.find(args,
				PredicateUtils.instanceofPredicate(HttpServletRequest.class));
	}
}
