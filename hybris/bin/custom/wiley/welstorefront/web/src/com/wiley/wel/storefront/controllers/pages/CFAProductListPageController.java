package com.wiley.wel.storefront.controllers.pages;

import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.acceleratorservices.data.RequestContextData;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.CategoryPageModel;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.wiley.facades.wel.product.WelProductFacade;


/**
 * Created by uladzimir_barouski on 1/16/2016.
 */
@Controller
@Scope("tenant")
@RequestMapping(value = CFAProductListPageController.CFA_SHOP_ALL_URL)
public class CFAProductListPageController extends ProductListPageController
{
	public static final String CFA_SHOP_ALL_URL = "/wiley-cfa/products/shop-all";

	protected static final Logger LOG = Logger.getLogger(CFAProductListPageController.class);
	private static final String CFA_CATEGORY = "WEL_CFA_CATEGORY";


	@Resource(name = "welProductFacade")
	private WelProductFacade productFacade;

	@RequestMapping(method = RequestMethod.GET)
	public String cfaProducts(final Model model, final HttpServletRequest request,
			final HttpServletResponse response) throws CMSItemNotFoundException
	{
		final CategoryModel category = getCommerceCategoryService().getCategoryForCode(CFA_CATEGORY);
		final CategoryPageModel categoryPage = getCategoryPage(category);

		storeCmsPageInModel(model, categoryPage);
		updatePageTitle(category, model);
		setUpMetaData(category, model);
		model.addAttribute("categoryName", category.getName());
		model.addAttribute("pageType", PageType.CATEGORY.name());
		model.addAttribute("userLocation", getCustomerLocationService().getUserLocation());

		final RequestContextData requestContextData = getRequestContextData(request);
		requestContextData.setCategory(category);

		return getViewPage(categoryPage);
	}
}
