
/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wel.storefront.controllers.pages;

import com.wiley.facades.voucher.WileyCouponFacade;
import com.wiley.facades.wel.order.WelMultiDimensionalCartFacade;
import com.wiley.facades.welags.order.WelAgsCheckoutFacade;
import com.wiley.wel.storefront.controllers.ControllerConstants;
import com.wiley.wel.storefront.controllers.constantsts.WelWebConstants;
import com.wiley.wel.storefront.forms.WileyUpdateQuantityForm;
import com.wiley.welags.storefrontcommons.controllers.pages.AbstractWelAgsCartPageController;
import com.wiley.welags.storefrontcommons.voucher.VoucherForm;
import de.hybris.platform.acceleratorfacades.flow.impl.SessionOverrideCheckoutFlowFacade;
import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.voucher.data.VoucherData;
import de.hybris.platform.commercefacades.voucher.exceptions.VoucherOperationException;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;


/**
 * Controller for cart page
 */
@Controller
@Scope("tenant")
@RequestMapping(value = "/cart")
public class CartPageController extends AbstractWelAgsCartPageController
{

	private static final String CART_CMS_PAGE_LABEL = "cart";

	@Autowired
	private WelMultiDimensionalCartFacade cartFacade;

	@Resource(name = "welCouponFacade")
	private WileyCouponFacade couponFacade;

	@Resource(name = "checkoutFacade")
	private WelAgsCheckoutFacade wileyCheckoutFacade;

	@Resource
	private UserFacade userFacade;

	/**
	 * The constant LOG.
	 */
	protected static final Logger LOG = Logger.getLogger(CartPageController.class);

	/**
	 * The constant SHOW_CHECKOUT_STRATEGY_OPTIONS.
	 */
	public static final String SHOW_CHECKOUT_STRATEGY_OPTIONS = "storefront.show.checkout.flows";

	@Resource(name = "simpleBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder resourceBreadcrumbBuilder;

	/**
	 * Is checkout strategy visible boolean.
	 *
	 * @return the boolean
	 */
	@ModelAttribute("showCheckoutStrategies")
	public boolean isCheckoutStrategyVisible()
	{
		return getSiteConfigService().getBoolean(SHOW_CHECKOUT_STRATEGY_OPTIONS, false);
	}

	/**
	 * Display the cart page
	 *
	 * @param model
	 * 		the model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 * 		the cms item not found exception
	 * @throws CommerceCartModificationException
	 * 		the commerce cart modification exception
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String showCart(final Model model) throws CMSItemNotFoundException, CommerceCartModificationException
	{
		model.addAttribute("voucherForm", new VoucherForm());
		return prepareCart(model);
	}


	@Override
	protected String prepareCart(final Model model) throws CMSItemNotFoundException
	{
		if (!userFacade.isAnonymousUser())
		{
			if (!wileyCheckoutFacade.isDeliveryAddressSetToCart())
			{
				wileyCheckoutFacade.setDefaultDeliveryAddressForCheckout();
			}
			wileyCheckoutFacade.setDeliveryModeIfNotAvailable();
		}

		prepareDataForPage(model);
		model.addAttribute(WebConstants.BREADCRUMBS_KEY, resourceBreadcrumbBuilder.getBreadcrumbs("breadcrumb.cart"));

		model.addAttribute("pageType", PageType.CART.name());

		final List<VoucherData> vouchers = couponFacade.getVouchersForCart();
		if (null != vouchers && !vouchers.isEmpty())
		{
			model.addAttribute("vouchers", vouchers);
		}
		model.addAttribute("isPreOrder", wileyCheckoutFacade.isPreOrderCart());
		return ControllerConstants.Views.Pages.Cart.CART_PAGE;
	}

	/**
	 * Handle the '/cart/checkout' request url. This method checks to see if the cart is valid before allowing the
	 * checkout to begin. Note that this method does not require the user to be authenticated and therefore allows us to
	 * validate that the cart is valid without first forcing the user to login. The cart will be checked again once the
	 * user has logged in.
	 *
	 * @param model
	 * 		the model
	 * @param redirectModel
	 * 		the redirect model
	 * @return The page to redirect to
	 * @throws CommerceCartModificationException
	 * 		the commerce cart modification exception
	 */
	@RequestMapping(value = "/checkout", method = RequestMethod.GET)
	@RequireHardLogIn
	public String cartCheck(final Model model, final RedirectAttributes redirectModel) throws CommerceCartModificationException
	{
		SessionOverrideCheckoutFlowFacade.resetSessionOverrides();

		if (!getCartFacade().hasEntries())
		{
			LOG.info("Missing or empty cart");

			// No session cart or empty session cart. Bounce back to the cart page.
			return REDIRECT_PREFIX + WelWebConstants.URL_CART_PAGE;
		}


		if (validateCart(redirectModel))
		{
			return REDIRECT_PREFIX + WelWebConstants.URL_CART_PAGE;
		}

		// Redirect to the start of the checkout flow to begin the checkout process
		// We just redirect to the generic '/checkout' page which will actually select the checkout flow
		// to use. The customer is not necessarily logged in on this request, but will be forced to login
		// when they arrive on the '/checkout' page.
		return REDIRECT_PREFIX + WelWebConstants.URL_CHECKOUT_PAGE;
	}

	/**
	 * Update cart quantities string.
	 *
	 * @param entryNumber
	 * 		the entry number
	 * @param model
	 * 		the model
	 * @param form
	 * 		the form
	 * @param bindingResult
	 * 		the binding result
	 * @param request
	 * 		the request
	 * @param redirectModel
	 * 		the redirect model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 * 		the cms item not found exception
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String updateCartQuantities(@RequestParam("entryNumber") final long entryNumber, final Model model,
			@Valid final WileyUpdateQuantityForm form, final BindingResult bindingResult, final HttpServletRequest request,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		if (bindingResult.hasErrors())
		{
			for (final ObjectError error : bindingResult.getAllErrors())
			{
				if ("typeMismatch".equals(error.getCode()))
				{
					GlobalMessages.addErrorMessage(model, "basket.error.quantity.invalid");
				}
				else
				{
					String messageKey = form.getQuantity() == null ? "basket.error.quantity.notNull" : error.getDefaultMessage();
					GlobalMessages.addErrorMessage(model, messageKey);
				}
			}
		}
		else if (getCartFacade().hasEntries())
		{
			try
			{

				final CartModificationData cartModification = getCartFacade().updateCartEntry(entryNumber,
						form.getQuantity().longValue());
				if (cartModification.getQuantity() == form.getQuantity().longValue())
				{
					// Success

					if (cartModification.getQuantity() == 0)
					{
						// Success in removing entry
						GlobalMessages
								.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
										"basket.page.message.remove");
					}
					else
					{
						// Success in update quantity
						GlobalMessages
								.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
										"basket.page.message.update");
					}
				}
				else if (cartModification.getQuantity() > 0)
				{
					// Less than successful
					GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
							"basket.page.message.update.reducedNumberOfItemsAdded.lowStock", new Object[]
									{ cartModification.getEntry().getProduct().getName(), cartModification.getQuantity(),
											form.getQuantity() });
				}
				else
				{
					// No more stock available
					GlobalMessages.addFlashMessage(
							redirectModel,
							GlobalMessages.ERROR_MESSAGES_HOLDER,
							"basket.page.message.update.reducedNumberOfItemsAdded.noStock",
							new Object[] { cartModification.getEntry().getProduct().getName() });
				}

				if (!getCartFacade().hasEntries())
				{
					// remove previous vouchers
					List<VoucherData> vouchers = couponFacade.getVouchersForCart();
					for (VoucherData voucher : vouchers)
					{
						try
						{
							couponFacade.releaseVoucher(voucher.getCode());
						}
						catch (VoucherOperationException e)
						{
							LOG.error(e.getMessage(), e);
						}
					}
				}

				// Redirect to the cart page on update success so that the browser doesn't re-post again
				return REDIRECT_PREFIX + WelWebConstants.URL_CART_PAGE;
			}
			catch (final CommerceCartModificationException ex)
			{
				GlobalMessages.addMessage(model, GlobalMessages.ERROR_MESSAGES_HOLDER,
						"basket.page.message.update.cartModificationException", new Object[] { ex.getMessage() });
				LOG.error("Couldn't update product with the entry number: " + entryNumber + ".", ex);
			}
		}

		model.addAttribute("voucherForm", new VoucherForm());
		return prepareCart(model);
	}


	@RequestMapping(value = "/change/producttype", method = RequestMethod.POST)
	public String changeProductType(@RequestParam(value = "sku", required = true) final String productSku,
			@RequestParam("entryNumber") final int entryNumber,
			@RequestParam(value = "quantity", required = false, defaultValue = "1") final long quantity, final Model model,
			final HttpServletRequest request,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		if (getCartFacade().hasEntries())
		{
			try
			{
				boolean succeed = cartFacade.changeInCartProductType(productSku, entryNumber, quantity);

				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
						succeed ? "basket.page.message.product.type.update" : "basket.page.message.product.change.error");

				// Redirect to the cart page on update success so that the browser doesn't re-post again
				return REDIRECT_PREFIX + WelWebConstants.URL_CART_PAGE;
			}
			catch (final CommerceCartModificationException ex)
			{
				GlobalMessages.addMessage(model, GlobalMessages.ERROR_MESSAGES_HOLDER,
						"basket.page.message.update.cartModificationException", new Object[] { ex.getMessage() });
				LOG.error(String.format("Couldn't update product with the entry number: %s with product %s",
						entryNumber, productSku), ex);
			}
		}

		model.addAttribute("voucherForm", new VoucherForm());
		return prepareCart(model);
	}

	@Override
	// This method is overridden because we need use WelMultiDimensionalCartFacade to get session cart which will be populated
	// by custom populators.
	protected void createProductList(final Model model) throws CMSItemNotFoundException
	{
		final CartData cartData = getCartFacade().getSessionCartWithEntryOrdering(true);
		boolean hasPickUpCartEntries = false;
		if (cartData.getEntries() != null && !cartData.getEntries().isEmpty())
		{
			for (final OrderEntryData entry : cartData.getEntries())
			{
				if (!hasPickUpCartEntries && entry.getDeliveryPointOfService() != null)
				{
					hasPickUpCartEntries = true;
				}
				final WileyUpdateQuantityForm uqf = new WileyUpdateQuantityForm();
				uqf.setQuantity(entry.getQuantity());
				model.addAttribute("updateQuantityForm" + entry.getEntryNumber(), uqf);
			}
		}

		model.addAttribute("cartData", cartData);
		model.addAttribute("hasPickUpCartEntries", Boolean.valueOf(hasPickUpCartEntries));

		storeCmsPageInModel(model, getContentPageForLabelOrId(CART_CMS_PAGE_LABEL));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(CART_CMS_PAGE_LABEL));
	}

	@Override
	protected CartFacade getCartFacade()
	{
		return this.cartFacade;
	}

	@Override
	protected Logger getLogger()
	{
		return LOG;
	}

	@Override
	protected WileyCouponFacade getCouponFacade()
	{
		return couponFacade;
	}
}
