package com.wiley.wel.storefront.validators;

import org.apache.commons.lang.StringUtils;
import org.jsoup.helper.StringUtil;
import org.springframework.validation.Errors;

import com.wiley.wel.storefront.forms.WileyFreeTrialForm;
import com.wiley.welags.storefrontcommons.forms.validation.WileyRegistrationFormValidator;


/**
 * Validator for {@link WileyFreeTrialForm}
 */
public class WileyFreeTrialFormValidator extends WileyRegistrationFormValidator
{

	private static final int MAX_FIELD_LENGTH = 255;

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return WileyFreeTrialForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object o, final Errors errors)
	{
		final WileyFreeTrialForm wileyFreeTrialForm = (WileyFreeTrialForm) o;
		final boolean isAnonymous = wileyFreeTrialForm.getAnonymous();
		if (isAnonymous)
		{
			validateBasicFields(wileyFreeTrialForm, errors);
		}
		validateFreeTrialSpecificFields(wileyFreeTrialForm, errors, isAnonymous);
	}

	/**
	 * Validate free trial specific fields.
	 *
	 * @param wileyFreeTrialForm
	 * 		the wiley free trial form
	 * @param errors
	 * 		the errors
	 * @param isAnonymous
	 * 		the is anonymous
	 */
	protected void validateFreeTrialSpecificFields(final WileyFreeTrialForm wileyFreeTrialForm, final Errors errors,
			final boolean isAnonymous)
	{
		final String country = wileyFreeTrialForm.getCountry();
		final String employerOrSchool = wileyFreeTrialForm.getEmployerOrSchool();
		final String course = wileyFreeTrialForm.getFreeTrial();

		if (isAnonymous && StringUtils.isBlank(country))
		{
			errors.rejectValue("country", "freeTrial.error.country.empty");
		}
		if (StringUtils.isBlank(employerOrSchool))
		{
			errors.rejectValue("employerOrSchool", "freeTrial.error.employerOrSchool.empty");
		}
		else if (StringUtils.length(employerOrSchool) > MAX_FIELD_LENGTH)
		{
			errors.rejectValue("employerOrSchool", "freeTrial.error.employerOrSchool.length");
		}
		if (StringUtil.isBlank(course))
		{
			errors.rejectValue("freeTrial", "freeTrial.error.course.empty");
		}
	}
}
