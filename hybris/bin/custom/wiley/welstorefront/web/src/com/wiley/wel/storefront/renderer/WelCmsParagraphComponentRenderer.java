package com.wiley.wel.storefront.renderer;


import de.hybris.platform.acceleratorcms.component.renderer.impl.CMSParagraphComponentRenderer;
import de.hybris.platform.cms2.model.contents.components.CMSParagraphComponentModel;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;


public class WelCmsParagraphComponentRenderer extends CMSParagraphComponentRenderer
{
	@Override
	public void renderComponent(final PageContext pageContext, final CMSParagraphComponentModel component) throws
			ServletException, IOException
	{
		// <div class="content">${content}</div>
		final JspWriter out = pageContext.getOut();
		out.write("<div class=\"content\">");
		// for NULL content exception is thrown and closing tag is not rendered
		if (null != component.getContent())
		{
			out.write(component.getContent());
		}
		out.write("</div>");
	}
}