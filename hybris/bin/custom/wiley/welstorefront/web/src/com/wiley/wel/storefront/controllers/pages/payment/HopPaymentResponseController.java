package com.wiley.wel.storefront.controllers.pages.payment;

import de.hybris.platform.acceleratorfacades.payment.data.PaymentSubscriptionResultData;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.user.UserFacade;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.facades.payment.WileyPaymentFacade;
import com.wiley.facades.welags.order.WelAgsCheckoutFacade;
import com.wiley.storefrontcommons.util.HttpServletRequestUtil;
import com.wiley.welags.storefrontcommons.controllers.exceptions.OrderAlreadyPlacedException;
import com.wiley.welags.storefrontcommons.controllers.pages.checkout.steps.PaymentMethodCheckoutStepController;


@RequestMapping("/checkout/multi/hop")
public class HopPaymentResponseController extends PaymentMethodCheckoutStepController
{
	private static final Logger LOG = LoggerFactory.getLogger(HopPaymentResponseController.class);

	@Resource(name = "welAgsPaymentFacade")
	private WileyPaymentFacade wileyPaymentFacade;

	@Resource(name = "welCheckoutFacade")
	private WelAgsCheckoutFacade welCheckoutFacade;

	@Autowired
	private UserFacade userFacade; 

	private HttpServletRequestUtil httpServletRequestUtil = new HttpServletRequestUtil();

	@RequestMapping(value = "/response", method = RequestMethod.POST)
	@RequireHardLogIn
	public String doHandleResponse(final RedirectAttributes redirectAttributes, final HttpServletRequest request)
	{

		if (!welCheckoutFacade.hasCheckoutCart())
		{
			LOG.error("Cart is empty. Order has already been placed");
			throw new OrderAlreadyPlacedException();
		}

		final Map<String, String> resultMap = httpServletRequestUtil.getRequestParameterMap(request);
		try
		{
			final PaymentSubscriptionResultData paymentSubscriptionResultData =
					wileyPaymentFacade.completeHopValidatePayment(resultMap, true);

			if (paymentSubscriptionResultData.isSuccess() && paymentSubscriptionResultData.getStoredCard() != null
					&& StringUtils.isNotBlank(paymentSubscriptionResultData.getStoredCard().getSubscriptionId()))
			{
				final CCPaymentInfoData paymentInfoData = paymentSubscriptionResultData.getStoredCard();
				userFacade.setDefaultPaymentInfo(paymentInfoData);

				final CCPaymentInfoData newPaymentSubscription = paymentSubscriptionResultData.getStoredCard();
				welCheckoutFacade.setPaymentDetails(newPaymentSubscription.getId());
			}
			else
			{
				LOG.error("Failed to perform validation.  Please check the log files for more information");
				String errorDescription = getMessageSource().getMessage("checkout.placing.order.paymentFailed.error.description."
								+ paymentSubscriptionResultData.getResultCode(), null,
						getI18nService().getCurrentLocale());

				GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
						"checkout.placing.order.paymentFailed.validation",
						new Object[]
								{ getContactMail(), getContactPhone(),
										errorDescription,
										paymentSubscriptionResultData.getResultCode() });


				final WelAgsCheckoutFacade checkoutFacade = getWelAgsCheckoutFacade();
				if (checkoutFacade.isDigitalSessionCart() && checkoutFacade.isNonZeroPriceCart())
				{
						return getCheckoutStep().getCheckoutGroup().getValidationResultsMap().get(
								ValidationResults.REDIRECT_TO_DELIVERY_ADDRESS.name());
				}
				return back(redirectAttributes);
			}
			return getCheckoutStep().nextStep();
		}
		catch (final Exception ex)
		{
			LOG.error(String.format("Failed to perform validation reason [%1$s]. ", ex.getMessage()), ex);
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"checkout.error.validation.exception", new Object[]
							{ getContactMail(), getContactPhone() });
			return REDIRECT_URL_CART;
		}
	}

	public void setHttpServletRequestUtil(final HttpServletRequestUtil httpServletRequestUtil)
	{
		this.httpServletRequestUtil = httpServletRequestUtil;
	}
}
