/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wel.storefront.controllers.misc;

import com.wiley.core.enums.WileyProductLifecycleEnum;
import com.wiley.core.wel.preorder.strategy.ProductLifecycleStatusStrategy;
import com.wiley.facades.partner.WileyPartnerFacade;
import com.wiley.facades.wel.order.WelMultiDimensionalCartFacade;
import com.wiley.facades.wel.product.WelProductFacade;
import com.wiley.facades.welags.order.WelAgsCheckoutFacade;
import com.wiley.facades.welags.voucher.exception.InvalidVoucherException;
import com.wiley.facades.welags.voucher.exception.VoucherCannotBeAppliedException;
import com.wiley.wel.storefront.controllers.constantsts.WelWebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.AbstractController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.voucher.exceptions.VoucherOperationException;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartModificationStatus;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import com.wiley.facades.voucher.WileyCouponFacade;


/**
 * Controller for Add to Cart functionality which is not specific to a certain page.
 */
@Controller
public class AddToCartController extends AbstractController
{
	private static final String ERROR_MSG_TYPE = "errorMsg";

	// Messages
	private static final String BASKET_ERROR_OCCURRED_CAUSE = "basket.error.occurred.cause";
	private static final String SELECT_AT_LEAST_ONE_ERROR = "basket.error.selectAtLeastOne";
	private static final String BASKET_INFORMATION_QUANTITY_NO_ITEMS_ADDED = "basket.information.quantity.noItemsAdded.";
	private static final String BASKET_INFORMATION_QUANTITY_REDUCED_NUMBER_OF_ITEMS_ADDED = 
	"basket.information.quantity.reducedNumberOfItemsAdded.";
	private static final String CLEAR_CART_MESSAGE = "cart.page.preorder.added";
	private static final String UTM_PARAMETER_PREFIX = "utm";
	private static final String ELQ_PARAMETER_PREFIX = "elq";
	private static final String EQUALS = "=";
	private static final String AMPERSAND = "&";
	private static final String QUESTION_MARK = "?";
	/**
	 * The constant LOG.
	 */
	protected static final Logger LOG = Logger.getLogger(AddToCartController.class);

	@Resource(name = "cartFacade")
	private WelMultiDimensionalCartFacade cartFacade;
	@Resource(name = "welCouponFacade")
	private WileyCouponFacade couponFacade;
	@Resource
	private WileyPartnerFacade wileyPartnerFacade;
	@Resource(name = "welProductFacade")
	private WelProductFacade welProductFacade;
	@Resource(name = "welAgsCheckoutFacade")
	private WelAgsCheckoutFacade wileyCheckoutFacade;
	@Resource(name = "welProductLifecycleStatusStrategy")
	private ProductLifecycleStatusStrategy welProductLifecycleStatusStrategy;

	/**
	 * Add to cart.
	 *
	 * @param codes
	 *           the codes
	 * @param redirectAttributes
	 *           the redirectAttributes
	 * @return the string
	 */
	@RequestMapping(value = "/cart/add", method = RequestMethod.POST)
	public String addToCart(@RequestParam("productCodePost") final Set<String> codes,
			@RequestParam(value = "student-discount", required = false, defaultValue = "false") 
			final boolean applyStudentDiscount,
			@RequestParam(value = "partnerId", required = false, defaultValue = "") final String partnerId,
			final RedirectAttributes redirectAttributes)
	{
		List<CartModificationData> cartModifications = Collections.emptyList();

		try
		{
			addClearCartMessage(null, new ArrayList<>(codes), redirectAttributes);

			final List<OrderEntryData> orderEntryDataList = new ArrayList<>();
			for (final String code : codes)
			{
				orderEntryDataList.add(getOrderEntryData(1L, code, null, 
				applyStudentDiscount, partnerId));
			}
			cartModifications = cartFacade.addOrderEntryList(orderEntryDataList);
		}
		catch (final CommerceCartModificationException e)
		{
			LOG.error("While adding products an error has occurred.", e);
			GlobalMessages.addFlashMessage(redirectAttributes, 
			GlobalMessages.ERROR_MESSAGES_HOLDER, BASKET_ERROR_OCCURRED_CAUSE,
					new String[]
					{ e.getMessage() });
		}

		// display status information on cart page
		for (final CartModificationData modification : cartModifications)
		{
			addRedirectStatusMessages(redirectAttributes, modification);
		}

		return REDIRECT_PREFIX + WelWebConstants.URL_CART_PAGE;
	}

	@RequestMapping(value = "/cart/addProduct", method = RequestMethod.GET)
	public final String addProductToCart(@RequestParam("sku") final String sku,
			@RequestParam(value = "quantity", required = false, defaultValue = "1") final long quantity,
			@RequestParam(value = "student-discount", required = false, defaultValue = "false") 
			final boolean applyStudentDiscount,
			@RequestParam(value = "discount-code", required = false, defaultValue = "") final String discountCode,
			@RequestParam(value = "partnerId", required = false, defaultValue = "") final String partnerId,
			final RedirectAttributes redirectAttributes, 
			final HttpServletRequest request)
	{
		List<CartModificationData> cartModifications = Collections.emptyList();
		final StringBuffer paramNameStringBuffer = new StringBuffer();
		final List<String> paramNames;
		final List<String> result;
		String requestQueryString = null;
		try
		{
			addClearCartMessage(null, new ArrayList<>(Arrays.asList(sku)), redirectAttributes);

			final List<OrderEntryData> orderEntryDataList = Collections
					.singletonList(getOrderEntryData(quantity, sku, null, applyStudentDiscount, partnerId));

			cartModifications = cartFacade.addOrderEntryList(orderEntryDataList);

			applyDiscount(discountCode, redirectAttributes);
		}
		catch (final CommerceCartModificationException e)
		{
			LOG.error("While adding products an error has occurred.", e);
			GlobalMessages.addFlashMessage(redirectAttributes, 
			GlobalMessages.ERROR_MESSAGES_HOLDER, BASKET_ERROR_OCCURRED_CAUSE,
					new String[]
					{ e.getMessage() });
		}

		// display status information on cart page
		for (final CartModificationData modification : cartModifications)
		{
			addRedirectStatusMessages(redirectAttributes, modification);
		}
		// Adding utm and elq parameters to link

		paramNames = Collections.list(request.getParameterNames());

		paramNames.stream()
				.filter(paramName -> (paramName.startsWith(UTM_PARAMETER_PREFIX) || paramName.startsWith(ELQ_PARAMETER_PREFIX))
						&& !request.getParameterValues(paramName)[0].isEmpty())
				.collect(Collectors.toList()).forEach((paramName) -> {
					paramNameStringBuffer.append(paramName);
					paramNameStringBuffer.append(EQUALS);
					paramNameStringBuffer.append(request.getParameterValues(paramName)[0]);
					paramNameStringBuffer.append(AMPERSAND);
				});

		requestQueryString = QUESTION_MARK + paramNameStringBuffer.toString();
		if (requestQueryString.endsWith(AMPERSAND))
		{
			requestQueryString = requestQueryString.substring(0, requestQueryString.length() - 1);
		}

		return REDIRECT_PREFIX + WelWebConstants.URL_CART_PAGE + requestQueryString;
	}

	protected void applyDiscount(final String discountCode, final RedirectAttributes redirectAttributes)
	{
		try
		{
			if (!discountCode.isEmpty())
			{
				couponFacade.applyVoucher(discountCode);
			}
		}
		catch (final InvalidVoucherException ex)
		{
			LOG.error(ex.getMessage(), ex);
			GlobalMessages.addFlashMessage(redirectAttributes, 
			GlobalMessages.ERROR_MESSAGES_HOLDER, "text.voucher.error");
		}
		catch (final VoucherCannotBeAppliedException ex)
		{
			LOG.error(ex.getMessage(), ex);
			GlobalMessages.addFlashMessage(redirectAttributes, 
			GlobalMessages.ERROR_MESSAGES_HOLDER, "text.voucher.priority.error");
		}
		catch (final VoucherOperationException ex)
		{
			LOG.error("Failed to apply voucher '" + discountCode + "' because of exception: ", ex);
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"text.voucher.error.unexpected");
		}
	}

	@RequestMapping(value = "/cart/addGrid", method = RequestMethod.POST)
	public final String addGridToCart(@RequestParam(value = "sku", required = false) final List<String> cartEntries,
			@RequestParam(value = "set", required = false) final List<String> productSets,
			@RequestParam(value = "student-discount", required = false, defaultValue = "false") 
			final boolean applyStudentDiscount,
			@RequestParam(value = "partnerId", required = false, defaultValue = "") final String partnerId,
			final RedirectAttributes redirectAttributes, 
			final HttpServletRequest request)
	{
		List<CartModificationData> modifications = Collections.emptyList();
		try
		{
			addClearCartMessage(cartEntries, productSets, redirectAttributes);

			if (CollectionUtils.isNotEmpty(productSets))
			{
				modifications = internalAddGridToCart(productSets, applyStudentDiscount, partnerId);
			}
			else if (CollectionUtils.isNotEmpty(cartEntries))
			{
				modifications = internalAddGridToCart(cartEntries, applyStudentDiscount, partnerId);
			}
			else
			{
				GlobalMessages.addFlashMessage(redirectAttributes, 
				GlobalMessages.ERROR_MESSAGES_HOLDER, SELECT_AT_LEAST_ONE_ERROR);
				final String referer = request.getHeader("Referer");
				return REDIRECT_PREFIX + referer;
			}
		}
		catch (final CommerceCartModificationException e)
		{
			LOG.error("While adding products an error has occurred.", e);
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER, BASKET_ERROR_OCCURRED_CAUSE,
					new String[]
					{ e.getMessage() });
		}

		// display status information on cart page
		for (final CartModificationData modification : modifications)
		{
			addRedirectStatusMessages(redirectAttributes, modification);
		}

		return REDIRECT_PREFIX + WelWebConstants.URL_CART_PAGE;
	}

	private void addClearCartMessage(final List<String> cartEntries, final List<String> products,
			final RedirectAttributes redirectAttributes)
	{
		final boolean isCartCleared = wileyCheckoutFacade
				.clearCartForPreOrderProduct(products != null ? products.get(0) : cartEntries.get(0));
		if (isCartCleared)
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER, CLEAR_CART_MESSAGE);
		}
	}

	@RequestMapping(value = "/cart/put", method = RequestMethod.POST)
	public String putToCart(@RequestParam("productCode") final String code, final RedirectAttributes redirectAttributes)
	{
		return addToCart(Collections.singleton(code), false, StringUtils.EMPTY, redirectAttributes);
	}


	private List<CartModificationData> internalAddGridToCart(final List<String> products, final boolean applyStudentDiscount,
			final String partnerId) throws CommerceCartModificationException
	{
		final List<OrderEntryData> orderEntries = getOrderEntryData(products, applyStudentDiscount, partnerId);
		return cartFacade.addOrderEntryList(orderEntries);
	}

	protected List<OrderEntryData> getOrderEntryData(final List<String> cartEntries, final boolean discountGroupCode,
			final String partnerId)
	{
		return cartEntries.stream().map(entry -> getOrderEntryData(1L, entry, null, discountGroupCode, partnerId))
				.collect(Collectors.toList());
	}

	protected OrderEntryData getOrderEntryData(final long quantity, final String productCode, final Integer entryNumber,
			final boolean applyDiscountCode, final String partnerId)
	{
		final OrderEntryData orderEntry = new OrderEntryData();
		final WileyProductLifecycleEnum productLifecycleStatus = welProductLifecycleStatusStrategy
				.getLifecycleStatusForProduct(productCode);
		if (productLifecycleStatus == WileyProductLifecycleEnum.PRE_ORDER)
		{
			orderEntry.setPartnerId(StringUtils.EMPTY);
		}
		else
		{
			orderEntry.setPartnerId(partnerId);
		}
		orderEntry.setQuantity(quantity);
		orderEntry.setProduct(new ProductData());
		orderEntry.getProduct().setCode(productCode);
		orderEntry.setEntryNumber(entryNumber);
		orderEntry.setApplyStudentDiscount(applyDiscountCode);
		return orderEntry;
	}

	protected void addRedirectStatusMessages(final RedirectAttributes redirectAttributes, final CartModificationData modification)
	{
		final boolean hasMessage = StringUtils.isNotEmpty(modification.getStatusMessage());
		if (hasMessage)
		{
			if (CommerceCartModificationStatus.SUCCESS.equals(modification.getStatusCode()))
			{
				GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER,
						modification.getStatusMessage(), null);
			}
			else if (!redirectAttributes.containsAttribute(ERROR_MSG_TYPE))
			{
				GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
						modification.getStatusMessage(), null);
			}
		}
		else if (!CommerceCartModificationStatus.SUCCESS.equals(modification.getStatusCode()))
		{
			if (modification.getQuantityAdded() == 0L)
			{
				GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
						BASKET_INFORMATION_QUANTITY_NO_ITEMS_ADDED + modification.getStatusCode(), new String[]
						{ getProductNameFromCartModificationData(modification) });
			}
			else if (modification.getQuantityAdded() < modification.getQuantity())
			{
				GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
						BASKET_INFORMATION_QUANTITY_REDUCED_NUMBER_OF_ITEMS_ADDED + modification.getStatusCode(), new String[]
						{ getProductNameFromCartModificationData(modification) });
			}
		}
	}

	private String getProductNameFromCartModificationData(final CartModificationData modification)
	{
		if (modification != null && modification.getEntry() != null)
		{
			final OrderEntryData entry = modification.getEntry();
			final ProductData product = entry.getProduct();
			if (product != null)
			{
				if (StringUtils.isNotEmpty(product.getName()))
				{
					return product.getName();
				}
				if (StringUtils.isNotEmpty(product.getCode()))
				{
					return product.getCode();
				}
			}
		}
		return "";
	}
}
