package com.wiley.wel.storefront.controllers.pages;

import com.wiley.wel.storefront.controllers.ControllerConstants;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Paypal specific login logic.
 */
@Controller
@Scope("tenant")
@RequestMapping(value = "/paypal/login")
public class PayPalLoginPageController extends LoginPageController
{
	protected static final String PAYPAL_RESPONSE_HANDLER_URL = "/paypal/checkout/hop/response";
	
	@Override
	protected String getSuccessRedirect(final HttpServletRequest request, final HttpServletResponse response)
	{
		return PAYPAL_RESPONSE_HANDLER_URL;
	}

	@Override
	protected String getView()
	{
		return ControllerConstants.Views.Pages.PayPal.PAYPAL_LOGIN_PAGE;
	}
}
