package com.wiley.wel.storefront.controllers.cms;

import de.hybris.platform.acceleratorservices.data.RequestContextData;
import de.hybris.platform.core.model.product.ProductModel;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.wiley.core.model.components.SystemRequirementsCMSTabComponentModel;
import com.wiley.core.product.WileyProductService;
import com.wiley.wel.storefront.controllers.ControllerConstants;


/**
 * Created by Aliaksei_Zlobich on 10/22/2015.
 */
@Controller("SystemRequirementsCMSTabComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.SYSTEM_REQUIREMENTS_TAB_COMPONENT)
public class SystemRequirementsCMSTabComponentController
		extends AbstractCMSComponentController<SystemRequirementsCMSTabComponentModel>
{
	private static final Logger LOG = Logger.getLogger(SystemRequirementsCMSTabComponentController.class);

	private static final String MODEL_CONTENT = "content";
	private static final String MODEL_TITLE = "title";

	@Resource
	private WileyProductService wileyProductService;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model,
			final SystemRequirementsCMSTabComponentModel component)
	{
		final RequestContextData requestContextData = getRequestContextData(request);
		if (requestContextData != null)
		{
			final ProductModel product = requestContextData.getProduct();
			model.addAttribute(MODEL_CONTENT, getWileyProductService().getSystemRequirements(product));
			model.addAttribute(MODEL_TITLE, getTitle(component));
		}
		else
		{
			LOG.warn("RequestContextData is null.");
		}
	}

	private String getTitle(final SystemRequirementsCMSTabComponentModel component)
	{
		String title = component.getTitle();
		if (StringUtils.isEmpty(title))
		{
			title = component.getName();
		}
		if (StringUtils.isEmpty(title))
		{
			title = component.getUid();
		}
		return title;
	}

	/**
	 * Gets wiley product service.
	 *
	 * @return the wiley product service
	 */
	public WileyProductService getWileyProductService()
	{
		return wileyProductService;
	}

	/**
	 * Sets wiley product service.
	 *
	 * @param wileyProductService
	 * 		the wiley product service
	 */
	public void setWileyProductService(final WileyProductService wileyProductService)
	{
		this.wileyProductService = wileyProductService;
	}
}
