package com.wiley.wel.storefront.controllers.pages;

import com.wiley.facades.partner.kpmg.KPMGEnrollmentData;
import com.wiley.facades.partner.kpmg.KPMGFunctionData;
import com.wiley.facades.partner.kpmg.KPMGOfficeData;
import com.wiley.facades.wel.order.WelMultiDimensionalCartFacade;
import com.wiley.wel.storefront.controllers.constantsts.WelWebConstants;
import com.wiley.wel.storefront.forms.WileyKPMGEnrollmentForm;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;

import static com.wiley.welags.storefrontcommons.constants.WelagsstorefrontcommonsConstants.Keys.FORM_GLOBAL_ERROR_MESSAGE;



@Controller
@RequestMapping(value = "/partner")
public class KPMGEnrollmentController extends AbstractPartnerPageController
{
	private static final Logger LOG = LoggerFactory.getLogger(KPMGEnrollmentController.class);

	private static final String BASKET_ERROR_OCCURRED_CAUSE = "basket.error.occurred.cause";
	private static final Long PRODUCT_QUANTITY_TO_ADD = 1L;

	@Resource(name = "cartFacade")
	private WelMultiDimensionalCartFacade cartFacade;

	@RequestMapping(value = PARTNER_ID_CATEGORY_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.POST)
	public String kpmgEnroll(@Valid @ModelAttribute("kpmgEnrollmentForm") final WileyKPMGEnrollmentForm kpmgEnrollmentForm,
			final BindingResult bindingResult, final Model model, final HttpServletRequest request,
			@PathVariable("partnerId") final String partnerId,
			@PathVariable("categoryCode") final String categoryCode)
	{
		if (bindingResult.hasErrors())
		{
			return handleKpmgEnrollmentError(kpmgEnrollmentForm, partnerId, categoryCode, model, request);
		}
		else
		{
			return processValidKpmgEnrollmentForm(kpmgEnrollmentForm, partnerId, categoryCode, model, request);
		}
	}

	private String processValidKpmgEnrollmentForm(final WileyKPMGEnrollmentForm kpmgEnrollmentForm,
			final String partnerId, final String categoryCode, final Model model, final HttpServletRequest request)
	{
		final String kpmgProductCode = kpmgEnrollmentForm.getProductVariantCode();

		if (kpmgProductCode != null)
		{
			try
			{
				cartFacade.addOrderEntryList(createOrderEntries(partnerId, kpmgProductCode));
				KPMGEnrollmentData enrollmentData = createKPMGEnrollmentData(kpmgEnrollmentForm);
				kpmgEnrollmentFacade.saveEnrollmentDataToCurrentCart(enrollmentData);
			}
			catch (CommerceCartModificationException e)
			{
				LOG.error("While adding KPMG product an error has occurred.", e);
				GlobalMessages.addMessage(model, GlobalMessages.ERROR_MESSAGES_HOLDER,
						BASKET_ERROR_OCCURRED_CAUSE, new String[] { e.getMessage() });
				return handleKpmgEnrollmentError(kpmgEnrollmentForm, partnerId, categoryCode, model, request);

			}
		}
		return REDIRECT_PREFIX + WelWebConstants.URL_CART_PAGE;
	}

	private String handleKpmgEnrollmentError(final WileyKPMGEnrollmentForm kpmgEnrollmentForm, final String partnerId,
			final String categoryCode, final Model model, final HttpServletRequest request)
	{
		GlobalMessages.addErrorMessage(model, FORM_GLOBAL_ERROR_MESSAGE);

		model.addAttribute("partnerId", partnerId);
		addPartnerProductsToModel(partnerId, categoryCode, model, request);
		addKpmgEnrollmentFormAttributes(kpmgEnrollmentForm, partnerId, model);

		return getPartnerPageByCategoryCode(categoryCode, model);
	}

	private List<OrderEntryData> createOrderEntries(final String partnerId, final String productCode)
	{
		final OrderEntryData orderEntry = new OrderEntryData();
		orderEntry.setQuantity(PRODUCT_QUANTITY_TO_ADD);
		orderEntry.setProduct(new ProductData());
		orderEntry.getProduct().setCode(productCode);
		orderEntry.setPartnerId(partnerId);
		return Arrays.asList(orderEntry);
	}

	private KPMGEnrollmentData createKPMGEnrollmentData(final WileyKPMGEnrollmentForm kpmgEnrollmentForm)
	{
		KPMGEnrollmentData enrollmentData = new KPMGEnrollmentData();
		enrollmentData.setEmployeeId(kpmgEnrollmentForm.getEmployeeId());
		enrollmentData.setRegistrationId(kpmgEnrollmentForm.getRegistrationId());
		enrollmentData.setFirstName(kpmgEnrollmentForm.getFirstName());
		enrollmentData.setLastName(kpmgEnrollmentForm.getLastName());
		enrollmentData.setEmail(kpmgEnrollmentForm.getEmail());

		if (kpmgEnrollmentForm.getFunction() != null)
		{
			final KPMGFunctionData kpmgFunctionData = kpmgEnrollmentFacade.createKPMGFunctionData(
					kpmgEnrollmentForm.getFunction(), kpmgEnrollmentForm.getFunction());
			enrollmentData.setFunction(kpmgFunctionData);
		}

		if (kpmgEnrollmentForm.getOffice() != null)
		{
			KPMGOfficeData kpmgOfficeData = kpmgEnrollmentFacade.createOfficeData(kpmgEnrollmentForm.getOffice());
			enrollmentData.setOffice(kpmgOfficeData);
		}
		return enrollmentData;
	}
}
