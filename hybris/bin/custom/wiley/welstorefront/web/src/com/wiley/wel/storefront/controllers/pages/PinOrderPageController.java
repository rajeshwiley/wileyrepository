package com.wiley.wel.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.order.InvalidCartException;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.core.enums.OrderType;
import com.wiley.facades.welags.order.WelAgsCheckoutFacade;
import com.wiley.facades.welags.pin.exception.PinOperationException;
import com.wiley.welags.storefrontcommons.form.PinActivationForm;

import static com.wiley.welags.storefrontcommons.constants.WelagsstorefrontcommonsConstants.Keys.FORM_GLOBAL_ERROR_MESSAGE;
import static com.wiley.welags.storefrontcommons.constants.WelagsstorefrontcommonsConstants.Keys.PIN_FIELD_ERROR;


/**
 * Controller for the PIN Activation placement as an Order.
 */
@Controller
@SessionAttributes("pinActivationForm")
public class PinOrderPageController extends AbstractPageController
{
    private static final Logger LOG = Logger.getLogger(PinOrderPageController.class);

    private static final String ACTIVATE_PAGE = "/pin/activate";
    private static final String CONFIRMATION_PAGE = "/pin/confirmation?orderCode={orderCode}";
    private static final String PIN_ERROR_MSG_KEY  = "pinActivation.checkPin.invalid";
    private static final String ORDER_CODE = "orderCode";

    @Resource(name = "welCheckoutFacade")
    private WelAgsCheckoutFacade wileyCheckoutFacade;

    @ModelAttribute("pinActivationForm")
    public PinActivationForm initializeForm() {
        return new PinActivationForm();
    }

    @RequestMapping(value = "/pin/order")
    @RequireHardLogIn
    public String order(@Valid @ModelAttribute("pinActivationForm") final PinActivationForm form,
            final BindingResult bindingResult, final SessionStatus sessionStatus, final RedirectAttributes redirectAttributes)
    {
        sessionStatus.setComplete();
        if (bindingResult.hasErrors())
        {
            return handleInvalidForm(redirectAttributes, form.getPin());
        }
        try
        {
            String orderCode = wileyCheckoutFacade.placePinOrder(form.getPin(), OrderType.GENERAL);
            redirectAttributes.addAttribute(ORDER_CODE, orderCode);
            return REDIRECT_PREFIX + CONFIRMATION_PAGE;
        }
        catch (IllegalAccessException | InvalidCartException | PinOperationException e)
        {
            LOG.error("Couldn't place pin activation: " + form.getPin() + ".", e);
            return handleInvalidForm(redirectAttributes, form.getPin());
        }
    }

    private String handleInvalidForm(final RedirectAttributes redirectAttributes, final String pin) {
        String errorMessage = getMessageSource().getMessage(FORM_GLOBAL_ERROR_MESSAGE, null,
                getI18nService().getCurrentLocale());
        String pinErrorMessage = getMessageSource().getMessage(PIN_ERROR_MSG_KEY, null,
                getI18nService().getCurrentLocale());
        GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
                errorMessage);
        FieldError fieldError = new FieldError("pinActivationForm", "pin", pinErrorMessage);
        redirectAttributes.addFlashAttribute(PIN_FIELD_ERROR, fieldError);
        redirectAttributes.addFlashAttribute("invalidPin", pin);
        return REDIRECT_PREFIX + ACTIVATE_PAGE;
    }
}
