package com.wiley.wel.storefront.filters.seo;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.UriUtils;

import com.wiley.core.util.seo.SeoUrlMappingManager;
import com.wiley.wel.storefront.util.ContentPageForRequestUtil;


/**
 * Filter that resolves SEO friendly url to Hybris specific url.
 */
public class SeoUrlForwardingFilter extends OncePerRequestFilter
{
	private static final Logger LOG = LoggerFactory.getLogger(SeoUrlForwardingFilter.class);

	private SeoUrlMappingManager seoUrlMappingManager;
	private ContentPageForRequestUtil contentPageForRequestUtil;

	@Override
	protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response,
			final FilterChain filterChain) throws ServletException, IOException
	{
		final String requestURI = UriUtils.decode(request.getRequestURI(), StandardCharsets.UTF_8.toString());
		final String internalURL = seoUrlMappingManager.getMappingForURI(requestURI);

		//if URL belongs to Content Page - do not do SEO forwarding
		if (internalURL != null && contentPageForRequestUtil.getContentPageForRequest(request) == null)
		{
			request.getRequestDispatcher(internalURL).forward(request, response);
		}
		else
		{
			filterChain.doFilter(request, response);
		}
	}

	@Required
	public void setSeoUrlMappingManager(final SeoUrlMappingManager seoUrlMappingManager)
	{
		this.seoUrlMappingManager = seoUrlMappingManager;
	}

	@Required
	public void setContentPageForRequestUtil(final ContentPageForRequestUtil contentPageForRequestUtil)
	{
		this.contentPageForRequestUtil = contentPageForRequestUtil;
	}
}
