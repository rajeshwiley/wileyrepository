package com.wiley.wel.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.security.AutoLoginStrategy;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.util.Config;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.core.enums.OrderType;
import com.wiley.facades.product.WileyFreeTrialProductFacade;
import com.wiley.facades.product.exception.RepeatedFreeTrialOrderException;
import com.wiley.facades.welags.customer.WelAgsCustomerFacade;
import com.wiley.facades.welags.order.WelAgsCheckoutFacade;
import com.wiley.facades.wiley.visibility.WileyProductVisibilityFacade;
import com.wiley.wel.storefront.format.WelFormattingConversionService;
import com.wiley.wel.storefront.forms.WileyFreeTrialForm;
import com.wiley.wel.storefront.validators.WileyFreeTrialFormValidator;

import static com.wiley.storefrontcommons.constants.WileystorefrontcommonsConstants.FORWARD_TO_404;


/**
 * Created by Aliaksei_Zlobich on 3/16/2016.
 */
@Controller
@RequestMapping("/**/free-trial")
public class FreeTrialPageController extends AbstractPageController
{
	private static final Logger LOG = LoggerFactory.getLogger(FreeTrialPageController.class);

	private static final String FREE_TRIAL_CODE_PATH_VARIABLE_PATTERN = "/{freeTrialCode:.*}";
	private static final String FREE_TRIAL_CMS_PAGE = "freeTrialPage";
	private static final String FREE_TRIAL_CONFIRMATION_PAGE = "freeTrialConfirmationPage/";
	private static final String WEL_EFFICIENTLEARNING_URL_HTTP = "wel.efficientlearning.url.http";

	@Resource
	private WileyFreeTrialProductFacade freeTrialProductFacade;
	@Resource
	private WileyFreeTrialFormValidator freeTrialValidator;
	@Resource
	private AutoLoginStrategy autoLoginStrategy;
	@Resource(name = "checkoutFacade")
	private WelAgsCheckoutFacade wileyCheckoutFacade;
	@Resource
	private UserFacade userFacade;
	@Resource(name = "welAgsCustomerFacade")
	private WelAgsCustomerFacade welAgsCustomerFacade;
	@Resource
	private WileyProductVisibilityFacade wileyProductVisibilityFacade;
	@Resource
	private WelFormattingConversionService welFormattingConversionService;


	@RequestMapping(method = RequestMethod.POST)
	public String submitFreeTrial(@Valid @ModelAttribute("wileyFreeTrialForm") final WileyFreeTrialForm wileyFreeTrialForm,
			final BindingResult bindingResult, final HttpServletRequest request, final HttpServletResponse response,
			final RedirectAttributes redirectModel)
	{
		getFreeTrialValidator().validate(wileyFreeTrialForm, bindingResult);
		if (bindingResult.hasErrors()
				|| wileyProductVisibilityFacade.isProductVisible(wileyFreeTrialForm.getFreeTrial()))
		{
			return processSubmitFreeTrialRequest(request.getHeader("Referer"), wileyFreeTrialForm, bindingResult, request,
					response,
					redirectModel);
		}
		else
		{
			return FORWARD_TO_404;
		}
	}


	/**
	 * This method takes data from the free trail form and submits free trial order
	 * Method updates user profile (billing address country and employer/school fields)
	 * In case of anonymous user, new user account is created using the form fields and user is automatically logged in
	 *
	 * @return redirect to freeTrial order confirmation page if there are no binding errors, account does not already exists or
	 * user haven't order with the same free trial product
	 */
	protected String processSubmitFreeTrialRequest(final String referer, final WileyFreeTrialForm form,
			final BindingResult bindingResult, final HttpServletRequest request, final HttpServletResponse response,
			final RedirectAttributes redirectModel)
	{
		if (bindingResult.hasErrors())
		{
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "form.global.error");
			return handleSubmitError(redirectModel, referer, form, bindingResult);
		}
		String orderCode;
		try
		{
			if (userFacade.isAnonymousUser())
			{
				final RegisterData data = new RegisterData();
				data.setFirstName(form.getFirstName());
				data.setLastName(form.getLastName());
				data.setLogin(form.getEmail());
				data.setPassword(form.getPwd());
				welAgsCustomerFacade.register(data);
				getAutoLoginStrategy().login(form.getEmail().toLowerCase(), form.getPwd(), request, response);
			}
			welAgsCustomerFacade.updateCustomerCountryAndEmployerSchoolFields(form.getCountry(), form.getEmployerOrSchool());
			orderCode = getWileyCheckoutFacade().placeFreeTrialOrder(form.getFreeTrial(), form.getSignUp(), OrderType.FREE_TRIAL)
					.getCode();
		}
		catch (final DuplicateUidException e)
		{
			LOG.warn("Free Trial order submit failed: " + e.getMessage(), e);
			String loginUrl = Config.getParameter(WEL_EFFICIENTLEARNING_URL_HTTP) + Config.getParameter("welLoginLink");
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"freeTrial.error.account.exists", new Object[] { loginUrl });
			return handleSubmitError(redirectModel, referer, form, bindingResult);
		}
		catch (final RepeatedFreeTrialOrderException e)
		{
			LOG.warn("Free Trial order submit failed: " + e.getMessage(), e);
			String contactUsUrl = Config.getParameter(WEL_EFFICIENTLEARNING_URL_HTTP) + Config.getParameter("contactUsLink");
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"freeTrial.error.order.exists", new Object[] { contactUsUrl });
			return handleSubmitError(redirectModel, referer, form, bindingResult);
		}
		catch (final InvalidCartException | IllegalAccessException | CommerceCartModificationException
				| UnknownIdentifierException e)
		{
			LOG.warn("Free Trial order submit failed: " + e.getMessage(), e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, e.getMessage());
			return handleSubmitError(redirectModel, referer, form, bindingResult);
		}
		return REDIRECT_PREFIX + FREE_TRIAL_CONFIRMATION_PAGE + orderCode;
	}

	protected String handleSubmitError(final RedirectAttributes redirectAttributes, final String referer,
			final WileyFreeTrialForm wileyFreeTrialForm, final BindingResult binding)
	{

		((BeanPropertyBindingResult) binding).initConversion(welFormattingConversionService);
		redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.wileyFreeTrialForm", binding);
		redirectAttributes.addFlashAttribute(wileyFreeTrialForm);
		return REDIRECT_PREFIX + referer;
	}

	/**
	 * @return the autoLoginStrategy
	 */
	protected AutoLoginStrategy getAutoLoginStrategy()
	{
		return autoLoginStrategy;
	}

	/**
	 * @return the freeTrialValidator
	 */
	protected WileyFreeTrialFormValidator getFreeTrialValidator()
	{
		return freeTrialValidator;
	}

	/**
	 * @return the wileyCheckoutFacade
	 */
	protected WelAgsCheckoutFacade getWileyCheckoutFacade()
	{
		return wileyCheckoutFacade;
	}

}
