package com.wiley.wel.storefront.util;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.util.UrlPathHelper;


/**
 * Util for content page lookup
 */
public class ContentPageForRequestUtil
{
	@Resource(name = "urlPathHelper")
	private UrlPathHelper urlPathHelper;

	@Resource(name = "cmsPageService")
	private CMSPageService cmsPageService;

	/**
	 * OOTB method transferred from DefaultPageController for re-usage.
	 * Lookup the CMS Content Page for request.
	 *
	 * @param request
	 * 		The request
	 * @return the CMS content page
	 */
	public ContentPageModel getContentPageForRequest(final HttpServletRequest request)
	{
		// Get the path for this request.
		// Note that the path begins with a '/'
		final String lookupPathForRequest = urlPathHelper.getLookupPathForRequest(request);

		try
		{
			// Lookup the CMS Content Page by label. Note that the label value must begin with a '/'.
			ContentPageModel page = cmsPageService.getPageForLabel(lookupPathForRequest);
			return page != null && Boolean.TRUE.equals(page.getDefaultPage()) ? page : null;
		}
		catch (final CMSItemNotFoundException ignore)
		{
			// Ignore exception
		}
		return null;
	}
}
