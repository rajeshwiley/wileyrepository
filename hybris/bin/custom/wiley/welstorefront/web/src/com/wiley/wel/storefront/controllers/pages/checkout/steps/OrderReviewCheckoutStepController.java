package com.wiley.wel.storefront.controllers.pages.checkout.steps;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;

import java.util.Collection;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.facades.welags.order.WelAgsCheckoutFacade;
import com.wiley.welags.storefrontcommons.controllers.pages.checkout.steps.AbstractWelAgsCheckoutStepController;
import com.wiley.welags.storefrontcommons.forms.StudentVerificationForm;

import static com.wiley.wel.storefront.controllers.ControllerConstants.Views.Pages.MultiStepCheckout.ORDER_REVIEW_PAGE;


@RequestMapping(value = "/checkout/multi/order-review")
public class OrderReviewCheckoutStepController extends AbstractWelAgsCheckoutStepController
{
	private static final String ORDER_REVIEW = "order-review";
	private static final String ORDER_REVIEW_CMS_PAGE_LABEL = "orderReviewPage";
	public static final String REVIEW_STEP_NUMBER = "4";
	@Resource(name = "checkoutFacade")
	private WelAgsCheckoutFacade wileyCheckoutFacade;

	@Override
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	@RequireHardLogIn
	@PreValidateCheckoutStep(checkoutStep = ORDER_REVIEW)
	public String enterStep(final Model model, final RedirectAttributes redirectAttributes)
			throws CMSItemNotFoundException, CommerceCartModificationException
	{
		getWileyCheckoutFacade().calculateCart();
		storeCmsPageInModel(model, getContentPageForLabelOrId(ORDER_REVIEW_CMS_PAGE_LABEL));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ORDER_REVIEW_CMS_PAGE_LABEL));
		final CartData cartData = getWileyCheckoutFacade().getCheckoutCart();
		Collection<String> breadcrumbLabels = getBreadcrumbLabels();
		model.addAttribute("step", CollectionUtils.isNotEmpty(breadcrumbLabels) ?
				breadcrumbLabels.size() - 1 : REVIEW_STEP_NUMBER);
		model.addAttribute("cartData", cartData);
		model.addAttribute("displayDeliveryCost", true);
		model.addAttribute("isPreOrder", wileyCheckoutFacade.isPreOrderCart());
		return ORDER_REVIEW_PAGE;
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@RequireHardLogIn
	public String add(final StudentVerificationForm form, final BindingResult bindingResult, final Model model)
			throws CMSItemNotFoundException
	{
		return getCheckoutStep(ORDER_REVIEW).nextStep();
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	@RequireHardLogIn
	public String edit(final StudentVerificationForm form, final BindingResult bindingResult, final Model model)
			throws CMSItemNotFoundException
	{
		return getCheckoutStep(ORDER_REVIEW).previousStep();
	}

	@Override
	public String back(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep(ORDER_REVIEW).previousStep();
	}

	@Override
	public String next(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep(ORDER_REVIEW).nextStep();
	}
}