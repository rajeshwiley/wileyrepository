package com.wiley.wel.storefront.forms;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;


/**
 * Form object for KPMG Enrollment.
 */
public class WileyKPMGEnrollmentForm
{
	@Size(max = 255, message = "{kpmg.enroll.length.invalid}")
	private String registrationId;

	@Size(max = 255, message = "{kpmg.enroll.length.invalid}")
	private String employeeId;

	@Size.List({
			@Size(min = 1, message = "{kpmg.enroll.firstName.empty}"),
			@Size(max = 255, message = "{kpmg.enroll.length.invalid}")})
	private String firstName;

	@Size.List({
			@Size(min = 1, message = "{kpmg.enroll.lastName.empty}"),
			@Size(max = 255, message = "{kpmg.enroll.length.invalid}")})
	private String lastName;

	@Size.List({
			@Size(min = 1, message = "{kpmg.enroll.email.invalid}"),
			@Size(max = 255, message = "{kpmg.enroll.length.invalid}")})
	@Pattern(regexp = "\\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}\\b", message = "{kpmg.enroll.email.invalid}")
	private String email;

	private String function;

	private String office;
	
	private String productVariantCode; 

	public String getRegistrationId()
	{
		return registrationId;
	}

	public void setRegistrationId(final String registrationId)
	{
		this.registrationId = registrationId;
	}

	public String getEmployeeId()
	{
		return employeeId;
	}

	public void setEmployeeId(final String employeeId)
	{
		this.employeeId = employeeId;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(final String firstName)
	{
		this.firstName = firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(final String lastName)
	{
		this.lastName = lastName;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(final String email)
	{
		this.email = email;
	}

	public String getFunction()
	{
		return function;
	}

	public void setFunction(final String function)
	{
		this.function = function;
	}

	public String getOffice()
	{
		return office;
	}

	public void setOffice(final String office)
	{
		this.office = office;
	}

	public String getProductVariantCode()
	{
		return productVariantCode;
	}

	public void setProductVariantCode(final String productVariantCode)
	{
		this.productVariantCode = productVariantCode;
	}
}
