/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wel.storefront.controllers;

import com.wiley.core.model.components.CFAProductComparisonTableComponentModel;
import com.wiley.core.model.components.FreeTrialCMSComponentModel;
import com.wiley.core.model.components.SocialMediaFollowComponentModel;
import com.wiley.core.model.components.SystemRequirementsCMSTabComponentModel;
import com.wiley.core.model.components.VariantProductComparisonTableComponentModel;
import com.wiley.core.model.components.VariantProductListComponentModel;
import com.wiley.welags.storefrontcommons.constants.WelagsstorefrontcommonsConstants;

import de.hybris.platform.acceleratorcms.model.components.CMSTabParagraphContainerModel;
import de.hybris.platform.commercefacades.product.ProductOption;

import java.util.Arrays;
import java.util.List;


/**
 * The type Controller constants.
 */
public final class ControllerConstants
{
    private ControllerConstants()
    {
        //empty to avoid instantiating this constant class
    }

    /**
     * The type Actions.
     */
    public static final class Actions
    {
        private Actions()
        {
        }

        /**
         * The type Cms.
         */
        public static final class Cms
        {
            private static final String PREFIX = WelagsstorefrontcommonsConstants.Actions.Cms.PREFIX;
            private static final String SUFFIX = WelagsstorefrontcommonsConstants.Actions.Cms.SUFFIX;

            /**
             * Free Trial CMS component controller
             */
            public static final String FREE_TRIAL_COMPONENT = PREFIX + FreeTrialCMSComponentModel._TYPECODE + SUFFIX;

            /**
             * The constant SYSTEM_REQUIREMENTS_TAB_COMPONENT.
             */
            public static final String SYSTEM_REQUIREMENTS_TAB_COMPONENT = PREFIX + SystemRequirementsCMSTabComponentModel
                    ._TYPECODE + SUFFIX;

            public static final String SOCIAL_MEDIA_FOLLOW_COMPONENT = PREFIX + SocialMediaFollowComponentModel
            		._TYPECODE + SUFFIX;

			public static final String VARIANT_PRODUCT_COMPARISON_TABLE_COMPONENT =
					PREFIX + VariantProductComparisonTableComponentModel._TYPECODE + SUFFIX;

			public static final String VARIANT_PRODUCT_LIST_COMPONENT =
					PREFIX + VariantProductListComponentModel._TYPECODE + SUFFIX;

            /**
             * The constant CFA_PRODUCT_COMPARISON_TABLE_COMPONENT.
             */
            public static final String CFA_PRODUCT_COMPARISON_TABLE_COMPONENT =
                    PREFIX + CFAProductComparisonTableComponentModel._TYPECODE + SUFFIX;

            public static final String CMS_TAB_PARAGRAPH_CONTAINER = PREFIX + CMSTabParagraphContainerModel._TYPECODE + SUFFIX;

            private Cms()
            {
            }
        }
    }

    /**
     * The type Views.
     */
    public static final class Views
    {
        private Views()
        {
        }

        /**
         * The type Pages.
         */
        public static final class Pages
        {
            private Pages()
            {
            }

            /**
             * The type MultiStepCheckout.
             */
            public static final class MultiStepCheckout
            {
                public static final String ORDER_REVIEW_PAGE = "pages/checkout/multi/orderReviewPage";
            }

            /**
             * The type Cart.
             */
            public static final class Cart
            {
                /**
                 * The constant CART_PAGE.
                 */
                public static final String CART_PAGE = "pages/cart/cartPage";

                private Cart()
                {
                }
            }

            /**
             * The type PayPal.
             */
            public static final class PayPal
            {
                public static final String PAYPAL_LOGIN_PAGE = "pages/paypal/paypalLoginPage";
                
                private PayPal()
                {
                }
            }
        }

        /**
         * The type Fragments.
         */
        public static final class Fragments
        {
            private Fragments()
            {
            }

            /**
             * The type Checkout.
             */
            public static final class Checkout
            {
                public static final String TERMS_AND_CONDITIONS_POPUP = "fragments/checkout/termsAndConditionsPopup";
                public static final String BILLING_ADDRESS_FORM = "fragments/checkout/billingAddressForm";
                public static final String READ_ONLY_EXPANDED_ORDER_FORM = "fragments/checkout/readOnlyExpandedOrderForm";

                private Checkout()
                {
                }
            }

            /**
             * The type Password.
             */
            public static final class Password
            {

                /**
                 * The constant PASSWORD_RESET_REQUEST_POPUP.
                 */
                public static final String PASSWORD_RESET_REQUEST_POPUP = "fragments/password/passwordResetRequestPopup";
                /**
                 * The constant FORGOT_PASSWORD_VALIDATION_MESSAGE.
                 */
                public static final String FORGOT_PASSWORD_VALIDATION_MESSAGE =
                        "fragments/password/forgotPasswordValidationMessage";

                private Password()
                {
                }
            }
        }
    }

    public static final class ProductOptions
    {
        private ProductOptions()
        {
        }


        public static List<ProductOption> getProductLevelOptionsForComparison() {
            return Arrays.asList(ProductOption.BASIC,
                    ProductOption.COMPARISONPRICE, ProductOption.VARIANT_PRODUCT_NAME,
                    ProductOption.CLASSIFICATION);
        }

        public static List<ProductOption> getProductLevelOptionsForSupplements() {
            return Arrays.asList(ProductOption.BASIC,
                    ProductOption.PRICE, ProductOption.SUMMARY, ProductOption.DESCRIPTION, ProductOption.GALLERY,
                    ProductOption.VARIANT_FULL, ProductOption.FREE_TRIAL_REFERENCE,
                    ProductOption.VARIANT_PRODUCT_NAME, ProductOption.VARIANT_PRODUCT_CLASSIFICATION);
        }

    }
}
