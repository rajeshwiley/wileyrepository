/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wel.storefront.controllers.pages;

import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.ProductBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.forms.ReviewForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.ReviewValidator;
import de.hybris.platform.acceleratorstorefrontcommons.util.MetaSanitizerUtil;
import de.hybris.platform.acceleratorstorefrontcommons.variants.VariantSortStrategy;
import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.BaseOptionData;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ImageDataType;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.ProductReferenceData;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.util.Config;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.gson.Gson;
import com.wiley.core.model.WileyFreeTrialProductModel;
import com.wiley.core.model.WileyVariantProductModel;
import com.wiley.core.product.WileyProductService;
import com.wiley.core.util.url.UrlVisibilityStrategy;
import com.wiley.facades.converters.ProductNodesConverter;
import com.wiley.facades.populators.WelPreOrderProductNodePopulator;
import com.wiley.facades.product.node.ProductRootNode;
import com.wiley.facades.wel.product.WelProductFacade;
import com.wiley.facades.wiley.visibility.WileyProductVisibilityFacade;
import com.wiley.storefrontcommons.util.WileyMetaSanitizerUtil;
import com.wiley.wel.storefront.controllers.constantsts.WelWebConstants;

import static com.wiley.storefrontcommons.constants.WileystorefrontcommonsConstants.FORWARD_TO_404;


/**
 * Controller for product details page
 */
@Controller
@Scope("tenant")
@RequestMapping(value = "/**/p")
public class ProductPageController extends AbstractPageController
{
	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(ProductPageController.class);

	/**
	 * We use this suffix pattern because of an issue with Spring 3.1 where a Uri value is incorrectly extracted if it
	 * contains on or more '.' characters. Please see https://jira.springsource.org/browse/SPR-6164 for a discussion on
	 * the issue and future resolution.
	 */
	private static final String PRODUCT_CODE_PATH_VARIABLE_PATTERN = "/{productCode:.*}";
	private static final String REVIEWS_PATH_VARIABLE_PATTERN = "{numberOfReviews:.*}";
	private static final String FREE_TRIAL_CMS_PAGE = "freeTrialPage";

	@Resource(name = "productModelUrlResolver")
	private UrlResolver<ProductModel> productModelUrlResolver;

	@Resource(name = "wileyProductVariantFacade")
	private ProductFacade productFacade;

	@Resource(name = "welProductFacade")
	private WelProductFacade welProductFacade;

	@Resource(name = "productService")
	private WileyProductService productService;

	@Resource(name = "productBreadcrumbBuilder")
	private ProductBreadcrumbBuilder productBreadcrumbBuilder;

	@Resource(name = "cmsPageService")
	private CMSPageService cmsPageService;

	@Resource(name = "variantSortStrategy")
	private VariantSortStrategy variantSortStrategy;

	@Resource(name = "reviewValidator")
	private ReviewValidator reviewValidator;

	@Resource(name = "productNodesConverter")
	private ProductNodesConverter productNodesConverter;

	@Resource(name = "priceDataFactory")
	private PriceDataFactory priceDataFactory;

	@Resource(name = "wileyProductVisibilityFacade")
	private WileyProductVisibilityFacade wileyProductVisibilityFacade;

	@Resource(name = "welPreOrderProductNodePopulator")
	private WelPreOrderProductNodePopulator welPreOrderProductNodePopulator;

	@Resource
	private UrlVisibilityStrategy<ProductModel> welProductUrlVisibilityStrategy;

	/**
	 * Product detail string.
	 *
	 * @param productCode
	 * 		the product code
	 * @param model
	 * 		the model
	 * @param request
	 * 		the request
	 * @param response
	 * 		the response
	 * @return the string
	 * @throws CMSItemNotFoundException
	 * 		the cms item not found exception
	 * @throws UnsupportedEncodingException
	 * 		the unsupported encoding exception
	 */
	@RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	public String productDetail(@PathVariable("productCode") final String productCode, final Model model,
			final HttpServletRequest request, final HttpServletResponse response) throws CMSItemNotFoundException,
			UnsupportedEncodingException
	{
		final ProductModel productModel = productService.getProductForCode(productCode);
		if (welProductUrlVisibilityStrategy.isUrlForItemVisible(productModel)
				&& wileyProductVisibilityFacade.isProductVisible(productCode))
		{
			ProductModel productWithFilteredVariants = wileyProductVisibilityFacade.filterRestrictedProductVariants(productModel);
			updatePageTitle(productWithFilteredVariants, model);
			populateProductDetailForDisplay(productWithFilteredVariants, model, request);
			model.addAttribute(new ReviewForm());
			final List<ProductReferenceData> productReferences = productFacade.getProductReferencesForCode(productCode,
					Arrays.asList(ProductReferenceTypeEnum.ADDON), Arrays.asList(ProductOption.BASIC, ProductOption.PRICE,
							ProductOption.SUMMARY), null);
			model.addAttribute("productReferences", productReferences);
			model.addAttribute("pageType", PageType.PRODUCT.name());
			model.addAttribute("mockExamUrl", Config.getString("cfa.mockexam.iframe.url", StringUtils.EMPTY));
			model.addAttribute("liveChatUrl", Config.getString("live.chat.url", StringUtils.EMPTY));

			final String metaKeywords = WileyMetaSanitizerUtil.sanitizeKeywords(productWithFilteredVariants.getKeywords());
			final String metaDescription = MetaSanitizerUtil.sanitizeDescription(resolveDescription(productWithFilteredVariants));
			setUpMetaData(model, metaKeywords, metaDescription);
			return getViewForPage(model);
		}
		else
		{
			return FORWARD_TO_404;
		}
	}

	/**
	 * Sets up review page.
	 *
	 * @param model
	 * 		the model
	 * @param productModel
	 * 		the product model
	 * @throws CMSItemNotFoundException
	 * 		the cms item not found exception
	 */
	protected void setUpReviewPage(final Model model, final ProductModel productModel) throws CMSItemNotFoundException
	{
		final String metaKeywords = WileyMetaSanitizerUtil.sanitizeKeywords(productModel.getKeywords());
		final String metaDescription = MetaSanitizerUtil.sanitizeDescription(resolveDescription(productModel));
		setUpMetaData(model, metaKeywords, metaDescription);
		storeCmsPageInModel(model, getPageForProduct(productModel));
		model.addAttribute("product", productFacade.getProductForOptions(productModel, Arrays.asList(ProductOption.BASIC)));
		updatePageTitle(productModel, model);
	}

	/**
	 * Handle unknown identifier exception string.
	 *
	 * @param exception
	 * 		the exception
	 * @param request
	 * 		the request
	 * @return the string
	 */
	@ExceptionHandler(UnknownIdentifierException.class)
	public String handleUnknownIdentifierException(final UnknownIdentifierException exception, final HttpServletRequest request)
	{
		request.setAttribute("message", exception.getMessage());
		return FORWARD_TO_404;
	}

	/**
	 * Update page title.
	 *
	 * @param productModel
	 * 		the product model
	 * @param model
	 * 		the model
	 */
	protected void updatePageTitle(final ProductModel productModel, final Model model)
	{
		storeContentPageTitleInModel(model, getPageTitleResolver().resolveProductPageTitle(productModel));
	}

	/**
	 * Populate product detail for display.
	 *
	 * @param productModel
	 * 		the product model
	 * @param model
	 * 		the model
	 * @param request
	 * 		the request
	 * @throws CMSItemNotFoundException
	 * 		the cms item not found exception
	 */
	protected void populateProductDetailForDisplay(final ProductModel productModel, final Model model,
			final HttpServletRequest request) throws CMSItemNotFoundException
	{
		getRequestContextData(request).setProduct(productModel);

		final ProductData productData = productFacade.getProductForOptions(productModel, Arrays.asList(ProductOption.BASIC,
				ProductOption.PRICE, ProductOption.SUMMARY, ProductOption.DESCRIPTION, ProductOption.GALLERY,
				ProductOption.CATEGORIES, ProductOption.REVIEW, ProductOption.PROMOTIONS,
				ProductOption.CLASSIFICATION, ProductOption.VARIANT_FULL, ProductOption.VOLUME_PRICES,
				ProductOption.DELIVERY_MODE_AVAILABILITY, ProductOption.VARIANT_MATRIX_BASE,
				ProductOption.VARIANT_MATRIX_URL, ProductOption.VARIANT_MATRIX_PRICE, ProductOption.VARIANT_MATRIX_MEDIA,
				ProductOption.STUDENT_DISCOUNTS, ProductOption.FREE_TRIAL_REFERENCE, ProductOption.LIFECYCLE_STATUS));

		sortVariantOptionData(productData);
		storeCmsPageInModel(model, getPageForProduct(productModel));
		populateProductData(productData, model);
		model.addAttribute(WebConstants.BREADCRUMBS_KEY, productBreadcrumbBuilder.getBreadcrumbs(productModel.getCode()));

		model.addAttribute(WelWebConstants.MULTI_DIMENSIONAL_PRODUCT, CollectionUtils.isNotEmpty(
				productData.getVariantMatrix()));

		final ProductRootNode rootNode = productNodesConverter.convert(productData);
		final PriceData priceData = productData.getPrice();
		// do not compute totalPrice for Products w\out price (e.x. from impex)
		if (null != priceData)
		{
			final Double totalPrice = welProductFacade.getPriceForDefaultSetProduct(rootNode);
			PriceData newPriceData = priceData;
			if (null != totalPrice)
			{
				newPriceData = priceDataFactory.create(
						priceData.getPriceType(), BigDecimal.valueOf(totalPrice), priceData.getCurrencyIso());
			}
			model.addAttribute("totalPrice", newPriceData);
		}
		model.addAttribute("productRootNodeJson", new Gson().toJson(rootNode));
		model.addAttribute("productRootNode", rootNode);

		populatePreOrderNodes(productModel, productData);
	}

	private void populatePreOrderNodes(final ProductModel productModel, final ProductData productData)
	{
		welPreOrderProductNodePopulator.populate(productModel, productData);
	}

	/**
	 * Populate product data.
	 *
	 * @param productData
	 * 		the product data
	 * @param model
	 * 		the model
	 */
	protected void populateProductData(final ProductData productData, final Model model)
	{
		model.addAttribute("galleryImages", getGalleryImages(productData));
		model.addAttribute("product", productData);
	}

	/**
	 * Sort variant option data.
	 *
	 * @param productData
	 * 		the product data
	 */
	protected void sortVariantOptionData(final ProductData productData)
	{
		if (CollectionUtils.isNotEmpty(productData.getBaseOptions()))
		{
			for (final BaseOptionData baseOptionData : productData.getBaseOptions())
			{
				if (CollectionUtils.isNotEmpty(baseOptionData.getOptions()))
				{
					Collections.sort(baseOptionData.getOptions(), variantSortStrategy);
				}
			}
		}

		if (CollectionUtils.isNotEmpty(productData.getVariantOptions()))
		{
			Collections.sort(productData.getVariantOptions(), variantSortStrategy);
		}
	}

	/**
	 * Gets gallery images.
	 *
	 * @param productData
	 * 		the product data
	 * @return the gallery images
	 */
	protected List<Map<String, ImageData>> getGalleryImages(final ProductData productData)
	{
		final List<Map<String, ImageData>> galleryImages = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(productData.getImages()))
		{
			final List<ImageData> images = new ArrayList<>();
			for (final ImageData image : productData.getImages())
			{
				if (ImageDataType.GALLERY.equals(image.getImageType()))
				{
					images.add(image);
				}
			}
			Collections.sort(images, new Comparator<ImageData>()
			{
				@Override
				public int compare(final ImageData image1, final ImageData image2)
				{
					return image1.getGalleryIndex().compareTo(image2.getGalleryIndex());
				}
			});

			if (CollectionUtils.isNotEmpty(images))
			{
				int currentIndex = images.get(0).getGalleryIndex().intValue();
				Map<String, ImageData> formats = new HashMap<String, ImageData>();
				for (final ImageData image : images)
				{
					if (currentIndex != image.getGalleryIndex().intValue())
					{
						galleryImages.add(formats);
						formats = new HashMap<>();
						currentIndex = image.getGalleryIndex().intValue();
					}
					formats.put(image.getFormat(), image);
				}
				if (!formats.isEmpty())
				{
					galleryImages.add(formats);
				}
			}
		}
		return galleryImages;
	}

	/**
	 * Gets review validator.
	 *
	 * @return the review validator
	 */
	protected ReviewValidator getReviewValidator()
	{
		return reviewValidator;
	}

	/**
	 * Gets page for product.
	 *
	 * @param product
	 * 		the product
	 * @return the page for product
	 * @throws CMSItemNotFoundException
	 * 		the cms item not found exception
	 */
	protected AbstractPageModel getPageForProduct(final ProductModel product) throws CMSItemNotFoundException
	{
		AbstractPageModel pageModel;
		//this check is required because free trial product has view different from one for root category it belongs to   
		if (product instanceof WileyFreeTrialProductModel)
		{
			pageModel = getCmsPageService().getPageForId(FREE_TRIAL_CMS_PAGE);
		}
		else
		{
			pageModel = cmsPageService.getPageForProduct(product instanceof WileyVariantProductModel ?
					((WileyVariantProductModel) product).getBaseProduct() : product);
		}
		return pageModel;
	}

	private String resolveDescription(final ProductModel productModel)
	{
		String seoDescription = productModel.getSeoDescriptionTag();
		if (seoDescription != null)
		{
			return seoDescription;
		}
		return productModel.getSummary();
	}

}
