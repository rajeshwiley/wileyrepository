package com.wiley.wel.storefront.controllers.pages;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.welags.storefrontcommons.constants.WelagsstorefrontcommonsConstants;
import com.wiley.welags.storefrontcommons.controllers.pages.AbstractWileyLoginPageController;
import com.wiley.welags.storefrontcommons.forms.WileyRegisterForm;


/**
 * PIN Login Controller. Handles login and register for the PIN activation flow.
 *
 * @author Gabor_Bata
 */
@Controller
@RequestMapping(value = "/pin/login")
public class PinLoginPageController extends AbstractWileyLoginPageController
{
	public static final String DEFAULT_TARGET_URL = "/pin/order";

	@Override
	protected AbstractPageModel getCmsPage() throws CMSItemNotFoundException
	{
		return getContentPageForLabelOrId("pinLogin");
	}

	@Override
	protected String getView()
	{
		return WelagsstorefrontcommonsConstants.Views.Pages.PinActivation.PIN_LOGIN_PAGE;
	}

	@Override
	protected String getSuccessRedirect(final HttpServletRequest request, final HttpServletResponse response)
	{
		return DEFAULT_TARGET_URL;
	}

	@RequestMapping(method = RequestMethod.GET)
	public String doPinLogin(@RequestParam(value = "error", defaultValue = "false") final boolean loginError,
			@RequestParam(value = "disabled", defaultValue = "false") final boolean isDisabledUser, final Model model,
			final HttpSession session)
			throws CMSItemNotFoundException
	{
		return getDefaultLoginPage(loginError, isDisabledUser, session, model);
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String doPinRegister(@RequestHeader(value = "referer", required = false) final String referer,
			final WileyRegisterForm form,
			final BindingResult bindingResult, final Model model, final HttpServletRequest request,
			final HttpServletResponse response, final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		getRegistrationValidator().validate(form, bindingResult);
		return processRegisterUserRequest(referer, form, bindingResult, model, request, response, redirectModel);
	}
}
