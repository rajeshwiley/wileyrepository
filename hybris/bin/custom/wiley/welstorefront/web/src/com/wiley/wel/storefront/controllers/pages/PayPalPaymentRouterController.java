package com.wiley.wel.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.AbstractController;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.wiley.facades.welags.order.WelAgsCheckoutFacade;


@Controller
@Scope("tenant")
@RequestMapping("/checkout/paypal")
public class PayPalPaymentRouterController extends AbstractController
{
	private static final Logger LOG = LoggerFactory.getLogger(PayPalPaymentRouterController.class);

	private static final String PAY_PAL_FROM_CART_URL = "/paypal/checkout/hop/expressCheckoutCartShortcut";
	private static final String PAY_PAL_FROM_CHECKOUT_URL = "/paypal/checkout/hop/expressCheckoutShortcut";
	private static final String STUDENT_VERIFICATION_URL = "/checkout/multi/student-verification/add";
	
	@Resource
	private WelAgsCheckoutFacade welAgsCheckoutFacade;

	@RequestMapping(value = "/from-cart", method = RequestMethod.GET)
	public String routeFromCart()
	{
		//student verification should be done after PayPal flow, not here
		return REDIRECT_PREFIX + PAY_PAL_FROM_CART_URL;
	}

	@RequestMapping(value = "/from-billing", method = RequestMethod.GET)
	public String routeFromBilling()
	{
		return performRoute(PAY_PAL_FROM_CHECKOUT_URL);
	}

	private String performRoute(final String hopPaymentUrl)
	{
		if (!welAgsCheckoutFacade.isStudentFlow() || welAgsCheckoutFacade.isCartHasStudentVerification())
		{
			LOG.debug("Student check isn't required. Redirecting to [{}]", hopPaymentUrl);
			return REDIRECT_PREFIX + hopPaymentUrl;
		}
		else
		{
			LOG.debug("Saving hopPaymentUrl [{}] into session", hopPaymentUrl);
			welAgsCheckoutFacade.saveHopPaymentUrl(hopPaymentUrl);
			LOG.debug("Student check is required. Redirecting to student verification page: [{}]", STUDENT_VERIFICATION_URL);
			return REDIRECT_PREFIX + STUDENT_VERIFICATION_URL;
		}
	}
}
