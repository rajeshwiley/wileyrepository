package com.wiley.wel.storefront.controllers.pages.checkout.steps;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.payment.AdapterException;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.core.mpgs.constants.WileyMPGSConstants;
import com.wiley.facades.payment.PaymentAuthorizationResultData;
import com.wiley.facades.welags.order.WelAgsCheckoutFacade;
import com.wiley.welags.storefrontcommons.controllers.pages.checkout.steps.SummaryCheckoutStepController;

import static com.wiley.storefrontcommons.controllers.util.WileyGlobalMessages.STATIC_ERROR_MESSAGES_HOLDER;


public class WelSummaryCheckoutStepController extends SummaryCheckoutStepController
{
	private static final Logger LOGGER = Logger.getLogger(WelSummaryCheckoutStepController.class);

	@Resource(name = "welCheckoutFacade")
	private WelAgsCheckoutFacade welCheckoutFacade;

	@RequestMapping(value = "/view", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	@PreValidateCheckoutStep(checkoutStep = SUMMARY)
	public String enterStep(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException,
			CommerceCartModificationException
	{

		//Validate the cart
		if (!validateCart(redirectAttributes))
		{
			if (welCheckoutFacade.isPreOrderCart())
			{
				return placeOrderAndRedirect(model);
			}
			PaymentAuthorizationResultData authorizationResult = PaymentAuthorizationResultData.failure("Unexpected error");
			try
			{
				authorizationResult = getWileyCheckoutFacade().authorizePaymentAndProvideResult("");
			}
			catch (final AdapterException ae)
			{
				// handle a case where a wrong paymentProvider configurations on the store
				// see getCommerceCheckoutService().getPaymentProvider()
				LOGGER.error(ae.getMessage(), ae);
			}
			if (!authorizationResult.isSuccess()
					&& authorizationResult.getPaymentMethod() == PaymentAuthorizationResultData.PaymentMethod.MPGS)
			{
				String errorDescription = getMessageSource().getMessage(authorizationResult.getErrorCode(), null,
						getI18nService().getCurrentLocale());
				LOGGER.error(String.format("Failed to create authorization in MPGS: %s. %s", authorizationResult.getStatus(),
						errorDescription));

				GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
						WileyMPGSConstants.MESSAGE_KEY_ISSUE_COMMON_MESSAGE,
						new Object[] { getContactMail(), getContactPhone(), errorDescription });
				return getCheckoutStep().previousStep();
			}

			if (!authorizationResult.isSuccess()
					&& authorizationResult.getPaymentMethod() == PaymentAuthorizationResultData.PaymentMethod.PAYPAL)
			{
				authorizationResult.getErrors()
						.forEach(e -> GlobalMessages.addFlashMessage(redirectAttributes, STATIC_ERROR_MESSAGES_HOLDER, e));
				String redirectUrl = getHopPaymentRepeatRedirectUrl();
				if (redirectUrl == null)
				{
					return getCheckoutStep().previousStep();
				}
				return REDIRECT_PREFIX + redirectUrl;
			}

			return placeOrderAndRedirect(model);
		}

		GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
				"checkout.placing.order.exception", new Object[]
						{ getContactMail(), getContactPhone() });

		return REDIRECT_PREFIX + "/cart";
	}

	private String placeOrderAndRedirect(final Model model)
	{
		final OrderData orderData;
		try
		{
			orderData = getCheckoutFacade().placeOrder();
			return redirectToOrderConfirmationPage(orderData);
		}
		catch (final Exception e)
		{
			LOGGER.error("Failed to place Order", e);
			GlobalMessages.addErrorMessage(model, "checkout.placeOrder.failed");

			return getCheckoutStep().previousStep();
		}
	}

	@Override
	public WelAgsCheckoutFacade getWileyCheckoutFacade()
	{
		return welCheckoutFacade;
	}
}
