/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wel.storefront.security;

import de.hybris.platform.acceleratorservices.uiexperience.UiExperienceService;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.security.BruteForceAttackCounter;
import de.hybris.platform.commerceservices.enums.UiExperienceLevel;
import de.hybris.platform.commerceservices.order.CommerceCartMergingException;
import de.hybris.platform.commerceservices.order.CommerceCartRestorationException;
import de.hybris.platform.servicelayer.session.SessionService;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;

import com.wiley.facades.wel.order.WelMultiDimensionalCartFacade;
import com.wiley.facades.welags.customer.WelAgsCustomerFacade;


/**
 * Success handler initializing user settings, restoring or merging the cart and ensuring the cart is handled correctly.
 * Cart restoration is stored in the session since the request coming in is that to j_spring_security_check and will be
 * redirected
 */
public class StorefrontAuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler
{
	private static final Logger LOG = LoggerFactory.getLogger(StorefrontAuthenticationSuccessHandler.class);
	private static final String CHECKOUT_URL = "/checkout";

	private WelAgsCustomerFacade customerFacade;
	private UiExperienceService uiExperienceService;
	private WelMultiDimensionalCartFacade cartFacade;
	private SessionService sessionService;
	private BruteForceAttackCounter bruteForceAttackCounter;
	private Map<UiExperienceLevel, Boolean> forceDefaultTargetForUiExperienceLevel;
	private List<String> restrictedPages;
	private List<String> listRedirectUrlsForceDefaultTarget;

	private boolean recalculateCart = true;
	private boolean restoreCart = true;

	@Override
	public void onAuthenticationSuccess(final HttpServletRequest request, final HttpServletResponse response,
			final Authentication authentication) throws IOException, ServletException
	{

		//if redirected from some specific url, need to remove the cachedRequest to force use defaultTargetUrl
		final RequestCache requestCache = new HttpSessionRequestCache();
		final SavedRequest savedRequest = requestCache.getRequest(request, response);
		for (final String redirectUrlForceDefaultTarget : getListRedirectUrlsForceDefaultTarget())
		{
			if (savedRequest != null && savedRequest.getRedirectUrl().contains(redirectUrlForceDefaultTarget))
			{
				requestCache.removeRequest(request, response);
				break;
			}
		}

		getCartFacade().searchAndRemoveBrokenCarts();
		getCustomerFacade().loginSuccess(recalculateCart);

		if (!getCartFacade().hasEntries())
		{
			getSessionService().setAttribute(WebConstants.CART_RESTORATION_SHOW_MESSAGE, Boolean.TRUE);
			try
			{
				getSessionService().setAttribute(WebConstants.CART_RESTORATION, getCartFacade().restoreSavedCart(null));
			}
			catch (final CommerceCartRestorationException e)
			{
				LOG.info(e.getMessage(), e);
				getSessionService().setAttribute(WebConstants.CART_RESTORATION_ERROR_STATUS,
						WebConstants.CART_RESTORATION_ERROR_STATUS);
			}
		}
		else
		{
			if (restoreCart)
			{
				final String sessionCartGuid = getCartFacade().getSessionCartGuid();
				final String mostRecentSavedCartGuid = getMostRecentSavedCartGuid(sessionCartGuid);
	
				//check that cart for user have already existed and login is not from checkout step
				if (StringUtils.isNotEmpty(mostRecentSavedCartGuid) && !StringUtils.equals(determineTargetUrl(request, response),
						CHECKOUT_URL))
				{
					try
					{
						getSessionService().setAttribute(WebConstants.CART_RESTORATION,
								getCartFacade().restoreCartAndMerge(mostRecentSavedCartGuid, sessionCartGuid));
					}
					catch (final CommerceCartRestorationException e)
					{
						LOG.info(e.getMessage(), e);
						getSessionService().setAttribute(WebConstants.CART_RESTORATION_ERROR_STATUS,
								WebConstants.CART_RESTORATION_ERROR_STATUS);
					}
					catch (final CommerceCartMergingException e)
					{
						LOG.error("User saved cart could not be merged", e);
					}
				}
			}	
		}

		getBruteForceAttackCounter().resetUserCounter(getCustomerFacade().getCurrentCustomerUid());
		super.onAuthenticationSuccess(request, response, authentication);
	}

	/**
	 * Gets restricted pages.
	 *
	 * @return the restricted pages
	 */
	protected List<String> getRestrictedPages()
	{
		return restrictedPages;
	}

	/**
	 * Sets restricted pages.
	 *
	 * @param restrictedPages
	 * 		the restricted pages
	 */
	public void setRestrictedPages(final List<String> restrictedPages)
	{
		this.restrictedPages = restrictedPages;
	}

	/**
	 * Gets cart facade.
	 *
	 * @return the cart facade
	 */
	protected WelMultiDimensionalCartFacade getCartFacade()
	{
		return cartFacade;
	}

	/**
	 * Sets cart facade.
	 *
	 * @param cartFacade
	 * 		the cart facade
	 */
	@Required
	public void setCartFacade(final WelMultiDimensionalCartFacade cartFacade)
	{
		this.cartFacade = cartFacade;
	}

	/**
	 * Gets session service.
	 *
	 * @return the session service
	 */
	protected SessionService getSessionService()
	{
		return sessionService;
	}

	/**
	 * Sets session service.
	 *
	 * @param sessionService
	 * 		the session service
	 */
	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	/**
	 * Gets customer facade.
	 *
	 * @return the customer facade
	 */
	protected WelAgsCustomerFacade getCustomerFacade()
	{
		return customerFacade;
	}

	/**
	 * Sets customer facade.
	 *
	 * @param customerFacade
	 * 		the customer facade
	 */
	@Required
	public void setCustomerFacade(final WelAgsCustomerFacade customerFacade)
	{
		this.customerFacade = customerFacade;
	}


	/*
	 * @see org.springframework.security.web.authentication.AbstractAuthenticationTargetUrlRequestHandler#
	 * isAlwaysUseDefaultTargetUrl()
	 */
	@Override
	protected boolean isAlwaysUseDefaultTargetUrl()
	{
		final UiExperienceLevel uiExperienceLevel = getUiExperienceService().getUiExperienceLevel();
		if (getForceDefaultTargetForUiExperienceLevel().containsKey(uiExperienceLevel))
		{
			return Boolean.TRUE.equals(getForceDefaultTargetForUiExperienceLevel().get(uiExperienceLevel));
		}
		else
		{
			return false;
		}
	}

	@Override
	protected String determineTargetUrl(final HttpServletRequest request, final HttpServletResponse response)
	{
		String targetUrl = super.determineTargetUrl(request, response);
		if (CollectionUtils.isNotEmpty(getRestrictedPages()))
		{
			for (final String restrictedPage : getRestrictedPages())
			{
				// When logging in from a restricted page, return user to homepage.
				if (targetUrl.contains(restrictedPage))
				{
					targetUrl = super.getDefaultTargetUrl();
				}
			}
		}
		return targetUrl;
	}

	/**
	 * Determine the most recent saved cart of a user for the site that is not the current session cart. The current
	 * session cart is already owned by the user and for the merging functionality to work correctly the most recently
	 * saved cart must be determined. getMostRecentCartGuidForUser(excludedCartsGuid) returns the cart guid which is ordered by
	 * modified time and is not the session cart.
	 *
	 * @param currentCartGuid
	 * 		the current cart guid
	 * @return most recently saved cart guid
	 */
	protected String getMostRecentSavedCartGuid(final String currentCartGuid)
	{
		return getCartFacade().getMostRecentCartGuidForUser(Arrays.asList(currentCartGuid));
	}

	/**
	 * Gets force default target for ui experience level.
	 *
	 * @return the force default target for ui experience level
	 */
	protected Map<UiExperienceLevel, Boolean> getForceDefaultTargetForUiExperienceLevel()
	{
		return forceDefaultTargetForUiExperienceLevel;
	}


	public void setRecalculateCart(final boolean recalculateCart)
	{
		this.recalculateCart = recalculateCart;
	}

	public void setRestoreCart(final boolean restoreCart) {
		this.restoreCart = restoreCart;
	}

	/**
	 * Sets force default target for ui experience level.
	 *
	 * @param forceDefaultTargetForUiExperienceLevel
	 * 		the force default target for ui experience level
	 */
	@Required
	public void setForceDefaultTargetForUiExperienceLevel(
			final Map<UiExperienceLevel, Boolean> forceDefaultTargetForUiExperienceLevel)
	{
		this.forceDefaultTargetForUiExperienceLevel = forceDefaultTargetForUiExperienceLevel;
	}

	/**
	 * Gets brute force attack counter.
	 *
	 * @return the brute force attack counter
	 */
	protected BruteForceAttackCounter getBruteForceAttackCounter()
	{
		return bruteForceAttackCounter;
	}

	/**
	 * Sets brute force attack counter.
	 *
	 * @param bruteForceAttackCounter
	 * 		the brute force attack counter
	 */
	@Required
	public void setBruteForceAttackCounter(final BruteForceAttackCounter bruteForceAttackCounter)
	{
		this.bruteForceAttackCounter = bruteForceAttackCounter;
	}

	/**
	 * Gets ui experience service.
	 *
	 * @return the ui experience service
	 */
	protected UiExperienceService getUiExperienceService()
	{
		return uiExperienceService;
	}

	/**
	 * Sets ui experience service.
	 *
	 * @param uiExperienceService
	 * 		the ui experience service
	 */
	@Required
	public void setUiExperienceService(final UiExperienceService uiExperienceService)
	{
		this.uiExperienceService = uiExperienceService;
	}

	protected List<String> getListRedirectUrlsForceDefaultTarget()
	{
		return listRedirectUrlsForceDefaultTarget;
	}

	@Required
	public void setListRedirectUrlsForceDefaultTarget(final List<String> listRedirectUrlsForceDefaultTarget)
	{
		this.listRedirectUrlsForceDefaultTarget = listRedirectUrlsForceDefaultTarget;
	}


}
