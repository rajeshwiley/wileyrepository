package com.wiley.wel.storefront.controllers.pages;

import com.wiley.wel.storefront.controllers.ControllerConstants;
import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.acceleratorservices.data.RequestContextData;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractCategoryPageController;
import de.hybris.platform.acceleratorstorefrontcommons.util.MetaSanitizerUtil;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.CategoryPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.util.url.UrlVisibilityStrategy;
import com.wiley.facades.product.data.ProductComparisonData;
import com.wiley.facades.wel.product.WelProductFacade;
import com.wiley.wel.storefront.util.ContentPageForRequestUtil;

import static com.wiley.storefrontcommons.constants.WileystorefrontcommonsConstants.FORWARD_TO_404;


/**
 * Controller for productsListPage
 */
@Controller
@Scope("tenant")
public class ProductListPageController extends AbstractCategoryPageController
{
	private static final Logger LOG = LoggerFactory.getLogger(ProductListPageController.class);
	private static final String PRODUCT_LIST_PAGE_ID = "ProductListPage";
	private static final String LEVEL_PRODUCT_LIST_PAGE_ID = "levelProductList";
	private static final String SIMPLE_PRODUCT_LIST_PAGE_ID = "simpleProductList";
	private static final String CATEGORY_RESTRICTION_FOR_SIMPLE_PLP = "WEL-SimpleProductsListPageRestriction";

	private static final List<ProductOption> PRODUCT_OPTIONS_FOR_COMPARISON = Arrays.asList(ProductOption.BASIC,
			ProductOption.CLASSIFICATION, ProductOption.VARIANT_MATRIX_BASE,
			ProductOption.VARIANT_MATRIX_PRICE, ProductOption.COMPARISONPRICE);
	private static final List<ProductOption> PRODUCT_OPTIONS_FOR_SUPPLEMENTS = Arrays.asList(ProductOption.BASIC,
			ProductOption.PRICE, ProductOption.SUMMARY, ProductOption.DESCRIPTION, ProductOption.GALLERY,
			ProductOption.CLASSIFICATION, ProductOption.VARIANT_FULL, ProductOption.FREE_TRIAL_REFERENCE);


	@Value(value = "${product.list.table.max.products:6}")
	private Integer maxNumberOfProducts;

	@Resource(name = "welProductFacade")
	private WelProductFacade productFacade;

	@Resource(name = "contentPageForRequestUtil")
	private ContentPageForRequestUtil contentPageForRequestUtil;

	@Resource
	private UrlVisibilityStrategy<CategoryModel> welCategoryUrlVisibilityStrategy;

	/**
	 * Returns view for all WEL categories except CFA
	 *
	 * @param categoryCode
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/{categoryCode:.*}/products", method = RequestMethod.GET)
	public String productList(@PathVariable("categoryCode") final String categoryCode, final Model model,
			final HttpServletRequest request)
	{
		//we don't handle requests for cfa category. it's used another controller with another mapping for it
		if (StringUtils.containsIgnoreCase(categoryCode, "cfa"))
		{
			return FORWARD_PREFIX + CFAProductListPageController.CFA_SHOP_ALL_URL;
		}
		final CategoryModel category = getCommerceCategoryService().getCategoryForCode(categoryCode);


		if (welCategoryUrlVisibilityStrategy.isUrlForItemVisible(category))
		{
			if (isNeedSimplePLP(category))
			{
				return showSimplePLP(model, request, category);
			}
			return showPLPwithComparison(model, request, category);

		}
		else
		{
			return FORWARD_TO_404;
		}
	}

	@RequestMapping(value = "/{categoryCode:.*}/products/{levelCategoryCode:.*}", method = RequestMethod.GET)
	public String levelProductList(@PathVariable("categoryCode") final String categoryCode,
			@PathVariable("levelCategoryCode") final String levelCategoryCode, final Model model,
			final HttpServletRequest request)
	{

		final ContentPageModel pageForRequest = contentPageForRequestUtil.getContentPageForRequest(request);
		if (pageForRequest != null)
		{
			// Sets WEL_CFA_CATEGORY to requestContextData to show cfa navigation bar (temporary solution)
			getRequestContextData(request).setCategory(
					getCommerceCategoryService().getCategoryForCode(WileyCoreConstants.WEL_CFA_CATEGORY_CODE));

			storeCmsPageInModel(model, pageForRequest);
			setUpMetaDataForContentPage(model, pageForRequest);
			return getViewForPage(pageForRequest);
		}

		final CategoryModel category = getCommerceCategoryService().getCategoryForCode(categoryCode);
		final CategoryModel levelCategory = getCommerceCategoryService().getCategoryForCode(levelCategoryCode);
		if (welCategoryUrlVisibilityStrategy.isUrlForItemVisible(levelCategory))
		{
			final CategoryPageModel categoryPage = getCategoryPageForId(LEVEL_PRODUCT_LIST_PAGE_ID);

			final ProductComparisonData productComparisonData = productFacade.getComparisonProductsForCategories(
					categoryCode, levelCategoryCode,
					ControllerConstants.ProductOptions.getProductLevelOptionsForComparison(), maxNumberOfProducts);
			final List<ProductData> supplementProductsDataList = productFacade.getSupplementProductsForCategories(
					category.getCode(), levelCategoryCode,
					ControllerConstants.ProductOptions.getProductLevelOptionsForSupplements());

			amendNameForProducts(levelCategory, productComparisonData.getProducts());
			amendNameForProducts(levelCategory, supplementProductsDataList);

			storeCommonAttributesInModel(category, categoryPage, model, request);
			storeProductComparisonDataInModel(productComparisonData, model);
			storeSupplementsDataInModel(supplementProductsDataList, model);
			storeTitlesInfoInModel(levelCategory, category, model);
			return getViewPage(categoryPage);
		}
		else
		{
			return FORWARD_TO_404;
		}
	}

	private void amendNameForProducts(final CategoryModel levelCategory, final List<ProductData> productsList)
	{
		for (ProductData productData : productsList)
		{
			final String productLevelName = levelCategory.getName() + " " + productData.getName();
			productData.setName(productLevelName);
		}
	}

	private void storeCommonAttributesInModel(final CategoryModel category, final CategoryPageModel categoryPage,
			final Model model, final HttpServletRequest request)
	{
		storeCmsPageInModel(model, categoryPage);
		model.addAttribute("categoryName", category.getName());
		model.addAttribute("pageType", PageType.CATEGORY.name());
		model.addAttribute("userLocation", getCustomerLocationService().getUserLocation());
		final RequestContextData requestContextData = getRequestContextData(request);
		requestContextData.setCategory(category);
	}

	private void storeProductComparisonDataInModel(final ProductComparisonData productComparisonData, final Model model)
	{
		model.addAttribute("productComparisonData", productComparisonData);
		model.addAttribute("displayCompleteSet", isDisplayCompleteSet(productComparisonData));
	}

	private void storeSupplementsDataInModel(final List<ProductData> productDataList, final Model model)
	{
		model.addAttribute("categorySupplementsProducts", productDataList);
	}

	private void storeTitlesInfoInModel(final CategoryModel levelCategory, final CategoryModel category, final Model model)
	{
		String productComparisonTableTitle = levelCategory.getName() + " " + category.getName();
		model.addAttribute("productComparisonTableTitle", productComparisonTableTitle);
		model.addAttribute("showSupplementsTitle", true);
	}

	private boolean isDisplayCompleteSet(final ProductComparisonData productComparisonData)
	{
		return productComparisonData.getProducts().stream().filter(productData -> (productData.getPriceCompleteSet() != null))
				.findAny().isPresent();
	}

	private CategoryPageModel getCategoryPageForId(final String categoryPageId)
	{
		try
		{
			final AbstractPageModel page = getCmsPageService().getPageForId(categoryPageId);
			if (page instanceof CategoryPageModel)
			{
				return (CategoryPageModel) page;
			}
			else
			{
				LOG.error("Page with id {} isn't CategoryPage", categoryPageId);
			}
		}
		catch (final CMSItemNotFoundException e)
		{
			LOG.error("CMSPage with id {} not found", categoryPageId);
		}
		return null;
	}

	private String showSimplePLP(final Model model, final HttpServletRequest request, final CategoryModel category)
	{
		final CategoryPageModel categoryPage = getCategoryPageForId(SIMPLE_PRODUCT_LIST_PAGE_ID);
		List<ProductData> productsDataList = productFacade.getProductsForCategory(category.getCode(),
				PRODUCT_OPTIONS_FOR_SUPPLEMENTS);

		updatePageTitle(category, model);
		setUpMetaData(category, model);

		storeCommonAttributesInModel(category, categoryPage, model, request);
		storeProductsDataInModel(productsDataList, model);
		return getViewPage(categoryPage);
	}

	private String showPLPwithComparison(final Model model, final HttpServletRequest request, final CategoryModel category)
	{
		final CategoryPageModel categoryPage = getCategoryPageForId(PRODUCT_LIST_PAGE_ID);
		final ProductComparisonData productComparisonData = productFacade.getComparisonProductsForCategory(category.getCode(),
				PRODUCT_OPTIONS_FOR_COMPARISON, maxNumberOfProducts);
		final List<ProductData> supplementsProductData = productFacade.getSupplementProductsForCategory(category.getCode(),
				PRODUCT_OPTIONS_FOR_SUPPLEMENTS);

		updatePageTitle(category, model);
		setUpMetaData(category, model);

		storeCommonAttributesInModel(category, categoryPage, model, request);
		storeProductComparisonDataInModel(productComparisonData, model);
		storeSupplementsDataInModel(supplementsProductData, model);

		return getViewPage(categoryPage);
	}

	private boolean isNeedSimplePLP(final CategoryModel category)
	{
		return category.getRestrictions().stream()
				.filter(restrict -> (restrict.getUid().equals(CATEGORY_RESTRICTION_FOR_SIMPLE_PLP)))
				.findAny()
				.isPresent();
	}


	protected void setUpMetaData(final CategoryModel categoryModel, final Model model)
	{
		Optional<String> description = Optional.ofNullable(categoryModel.getSeoDescriptionTag());
		if (!description.isPresent())
		{
			description = Optional.ofNullable(categoryModel.getDescription());
		}
		String metaDescription = MetaSanitizerUtil.sanitizeDescription(description.orElse(null));
		super.setUpMetaData(model, StringUtils.EMPTY, metaDescription);
	}

	private void storeProductsDataInModel(final List<ProductData> productDataList, final Model model)
	{
		model.addAttribute("categoryProducts", productDataList);
	}

}
