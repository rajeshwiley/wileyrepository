package com.wiley.wel.storefront.controllers.pages.checkout.steps;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;



import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.welags.storefrontcommons.controllers.pages.checkout.steps.PaymentMethodCheckoutStepController;


public class WelPaymentMethodCheckoutStepController extends PaymentMethodCheckoutStepController
{

	private static final String ORDER_REVIEW = "order-review";
	private static final String PAYMENT_METHOD = "payment-method";

	@Override
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	@RequireHardLogIn
	@PreValidateCheckoutStep(checkoutStep = PAYMENT_METHOD)
	public String enterStep(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		final String savedHopPaymentUrl = checkoutFacade.getSavedHopPaymentUrlAndRemove();
		if (savedHopPaymentUrl != null)
		{
			return REDIRECT_PREFIX + savedHopPaymentUrl;
		}

		getCheckoutFacade().setDeliveryModeIfAvailable();
		setupAddPaymentPage(model);
		setCheckoutStepLinksForModel(model, getCheckoutStep());

		return getCheckoutStep(ORDER_REVIEW).currentStep();
	}
}