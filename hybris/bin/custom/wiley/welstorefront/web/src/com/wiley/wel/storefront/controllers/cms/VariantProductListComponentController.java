package com.wiley.wel.storefront.controllers.cms;

import de.hybris.platform.commercefacades.product.data.ProductData;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.wiley.core.model.components.VariantProductListComponentModel;
import com.wiley.facades.wel.product.WelProductFacade;
import com.wiley.wel.storefront.controllers.ControllerConstants;

import static com.wiley.wel.storefront.controllers.ControllerConstants.ProductOptions.getProductLevelOptionsForSupplements;


/**
 * Controller for VariantProductListComponent
 */
@Controller("VariantProductListComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.VARIANT_PRODUCT_LIST_COMPONENT)
public class VariantProductListComponentController
		extends AbstractCMSComponentController<VariantProductListComponentModel>
{
	private static final Logger LOG = Logger.getLogger(VariantProductListComponentController.class);

	@Autowired
	private WelProductFacade welProductFacade;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model,
			final VariantProductListComponentModel component)
	{
		if (CollectionUtils.isNotEmpty(component.getProducts()))
		{
			List<ProductData> products = welProductFacade.getSupplementsProductData(component.getProducts(),
                    getProductLevelOptionsForSupplements());
			model.addAttribute("products", products);
		}
	}

}
