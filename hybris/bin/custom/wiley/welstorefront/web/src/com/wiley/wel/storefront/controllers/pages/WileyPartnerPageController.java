/**
 *
 */
package com.wiley.wel.storefront.controllers.pages;

import com.wiley.facades.partner.WileyPartnerFacade;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.gson.Gson;
import com.wiley.core.model.WileyPartnerCompanyModel;
import com.wiley.core.model.WileyPartnerPageModel;
import com.wiley.core.partner.WileyPartnerService;
import com.wiley.facades.autocomplete.data.WileyAutocompleteData;
import com.wiley.facades.constants.WileyFacadesConstants;
import com.wiley.facades.partner.WileyPartnerCompanyData;
import com.wiley.wel.storefront.forms.WileyKPMGEnrollmentForm;

import static com.wiley.storefrontcommons.constants.WileystorefrontcommonsConstants.FORWARD_TO_404;


@Controller
@RequestMapping(value = "/partner")
public class WileyPartnerPageController extends AbstractPartnerPageController
{
	private static final String CATEGORIES_LINKS_PATTERN = "/partner/%s/%s";


	@Resource
	private CommerceCategoryService commerceCategoryService;

	@Resource
	private WileyPartnerFacade wileyPartnerFacade;

	//TODO remove the service. Use a facade instead
	@Resource
	private WileyPartnerService wileyPartnerService;

	@Resource
	private CategoryService categoryService;

	@ModelAttribute("kpmgEnrollmentForm")
	public WileyKPMGEnrollmentForm initializeForm()
	{
		return new WileyKPMGEnrollmentForm();
	}

	@RequestMapping(value = PARTNER_ID_CATEGORY_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	public String getPartnerPage(@PathVariable("partnerId") final String partnerId,
			@PathVariable("categoryCode") final String categoryCode, final Model model, final HttpServletRequest request)
	{
		// for general page and  for certain partner companies
		WileyPartnerPageModel partnerPageModel = wileyCMSPageService.getPartnerPageByCategoryId(categoryCode);
		if (null == partnerPageModel)
		{
			return FORWARD_TO_404;
		}

		if (!StringUtils.equalsIgnoreCase(partnerId, WileyFacadesConstants.GENERIC_PARTNER_PAGE_URL))
		{
			final WileyPartnerCompanyData partnerCompany = wileyPartnerFacade.getWileyPartnerCompany(partnerId);

			model.addAttribute("partnerId", partnerId);


			if (null == partnerCompany
					|| !wileyPartnerFacade.partnerHasCategory(partnerCompany, categoryCode))
			{
				return FORWARD_TO_404;
			}

			if (!wileyPartnerFacade.isSpecialPartner(partnerCompany)) {
				model.addAttribute("showPriceBeforeDiscountLine", Boolean.TRUE);
			}
			model.addAttribute("partnerName", partnerCompany.getName());

			if (wileyPartnerFacade.isKPMGPartner(partnerCompany))
			{
				addKpmgEnrollmentFormAttributes(initializeForm(), partnerId, model);
			}

			model.addAttribute("categoriesLinks", createCategoriesLinks(partnerId));
		}
		storeCmsPageInModel(model, partnerPageModel);
		model.addAttribute("partners", new Gson().toJson(
				getAutocompleteData(wileyPartnerFacade.getPartnersNamesByCategoryCode(categoryCode))));
		addPartnerProductsToModel(partnerId, categoryCode, model, request);
		model.addAttribute("activeCategory", getActiveCategoryDescription(categoryCode));
		setCategoryInContext(categoryCode, request);

		return getPartnerPageByCategoryCode(categoryCode, model);
	}



	private List<WileyAutocompleteData> getAutocompleteData(final Map<String, String> preferredPartnersNames)
	{
		List<WileyAutocompleteData> wileyDataList = new ArrayList<>();
		for (Map.Entry<String, String> entry : preferredPartnersNames.entrySet())
		{
			WileyAutocompleteData data = new WileyAutocompleteData();
			data.setLabel(entry.getValue());
			data.setValue(entry.getKey());
			wileyDataList.add(data);
		}
		return wileyDataList;
	}

	private void setCategoryInContext(final String categoryCode, final HttpServletRequest request)
	{
		final CategoryModel category = commerceCategoryService.getCategoryForCode(categoryCode);
		getRequestContextData(request).setCategory(category);
	}

	/**
	 * Create relation of tabs labels and category links
	 *
	 * @param partnerId
	 * 		ID of the Partner
	 * @return Map<CategoryModel::getDescription, CategoryURL>
	 */
	private Map<String, Map<String, String>> createCategoriesLinks(final String partnerId)
	{
		final Map<String, Map<String, String>> categoriesLinks = new LinkedHashMap<>();
		for (final CategoryModel category : getPartnerCategories(partnerId))
		{
			final Map<String, String> categoryProperties = new HashMap<>();
			categoryProperties.put("description", category.getDescription() == null ?
					category.getCode() : category.getDescription());
			categoryProperties.put("link", String.format(CATEGORIES_LINKS_PATTERN, partnerId, category.getCode()));
			categoriesLinks.put(category.getCode(), categoryProperties);
		}
		return categoriesLinks;
	}

	private List<CategoryModel> getPartnerCategories(final String partnerId)
	{
		final WileyPartnerCompanyModel companyModel = wileyPartnerService.getPartnerById(partnerId);
		if (null != companyModel)
		{
			final Set<CategoryModel> categoryModels = companyModel.getPartnerCategories();
			// show more than one category only
			if (CollectionUtils.isNotEmpty(categoryModels) && categoryModels.size() > 1)
			{
				List<CategoryModel> sortedCategorise = new ArrayList<>(categoryModels);
				Collections.sort(sortedCategorise, this::compare);
				return sortedCategorise;
			}
		}
		return Collections.emptyList();
	}

	private int compare(final CategoryModel category1, final CategoryModel category2)
	{
		Integer categoryOrder1 = category1.getOrder();
		Integer categoryOrder2 = category2.getOrder();
		if (categoryOrder1 != null && categoryOrder2 != null)
		{
			return categoryOrder1.compareTo(categoryOrder2);
		}

		if (categoryOrder1 == null && categoryOrder2 != null)
		{
			return 1;
		}

		if (categoryOrder1 != null && categoryOrder2 == null)
		{
			return -1;
		}
		return 0;
	}

	private String getActiveCategoryDescription(final String categoryCode)
	{
		final CategoryModel category = categoryService.getCategoryForCode(categoryCode);
		if (null != category)
		{
			return category.getDescription();
		}
		return StringUtils.EMPTY;
	}
}
