/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wel.storefront.controllers.pages;

import de.hybris.platform.acceleratorfacades.flow.CheckoutFlowFacade;
import de.hybris.platform.acceleratorstorefrontcommons.forms.GuestForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.GuestValidator;
import de.hybris.platform.acceleratorstorefrontcommons.security.GUIDCookieStrategy;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.order.data.CartData;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.context.annotation.Scope;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.facades.welags.order.WelAgsCheckoutFacade;
import com.wiley.wel.storefront.controllers.constantsts.WelWebConstants;
import com.wiley.welags.storefrontcommons.constants.WelagsstorefrontcommonsConstants;
import com.wiley.welags.storefrontcommons.controllers.pages.AbstractWileyLoginPageController;
import com.wiley.welags.storefrontcommons.forms.WileyRegisterForm;


/**
 * Checkout Login Controller. Handles login and register for the checkout flow.
 */
@Controller
@Scope("tenant")
@RequestMapping(value = "/login/checkout")
public class CheckoutLoginController extends AbstractWileyLoginPageController
{
	private static final String FORM_NAME_KEY = "formName";
	private static final String FORM_NAME_VALUE = "wileyRegisterForm";

	@Resource(name = "checkoutFlowFacade")
	private CheckoutFlowFacade checkoutFlowFacade;

	@Resource(name = "guidCookieStrategy")
	private GUIDCookieStrategy guidCookieStrategy;

	@Resource(name = "authenticationManager")
	private AuthenticationManager authenticationManager;

	@Resource(name = "guestValidator")
	private GuestValidator guestValidator;

	@Resource(name = "checkoutFacade")
	private WelAgsCheckoutFacade wileyCheckoutFacade;

	@Resource(name = "welCustomerFacade")
	private CustomerFacade welCustomerFacade;

	@Resource(name = "httpSessionRequestCache")
	private HttpSessionRequestCache httpSessionRequestCache;

	/**
	 * Do checkout login string.
	 *
	 * @param loginError
	 * 		the login error
	 * @param session
	 * 		the session
	 * @param model
	 * 		the model
	 * @param request
	 * 		the request
	 * @return the string
	 * @throws CMSItemNotFoundException
	 * 		the cms item not found exception
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String doCheckoutLogin(@RequestParam(value = "error", defaultValue = "false") final boolean loginError,
			@RequestParam(value = "disabled", defaultValue = "false") final boolean isDisabledUser,
			final HttpSession session, final Model model, final HttpServletRequest request) throws CMSItemNotFoundException
	{
		model.addAttribute("expressCheckoutAllowed", Boolean.valueOf(checkoutFlowFacade.isExpressCheckoutEnabledForStore()));
		model.addAttribute(FORM_NAME_KEY, FORM_NAME_VALUE);
		return getDefaultLoginPage(loginError, isDisabledUser, session, model);
	}

	/**
	 * Do checkout register string.
	 *
	 * @param form
	 * 		the form
	 * @param bindingResult
	 * 		the binding result
	 * @param model
	 * 		the model
	 * @param request
	 * 		the request
	 * @param response
	 * 		the response
	 * @param redirectModel
	 * 		the redirect model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 * 		the cms item not found exception
	 */
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String doCheckoutRegister(final WileyRegisterForm form, final BindingResult bindingResult, final Model model,
			final HttpServletRequest request, final HttpServletResponse response, final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException
	{
		getRegistrationValidator().validate(form, bindingResult);
		model.addAttribute(FORM_NAME_KEY, FORM_NAME_VALUE);
		return processRegisterUserRequest(null, form, bindingResult, model, request, response, redirectModel);
	}

	/**
	 * Do anonymous checkout string.
	 *
	 * @param form
	 * 		the form
	 * @param bindingResult
	 * 		the binding result
	 * @param model
	 * 		the model
	 * @param request
	 * 		the request
	 * @param response
	 * 		the response
	 * @return the string
	 * @throws CMSItemNotFoundException
	 * 		the cms item not found exception
	 */
	@RequestMapping(value = "/guest", method = RequestMethod.POST)
	public String doAnonymousCheckout(final GuestForm form, final BindingResult bindingResult, final Model model,
			final HttpServletRequest request, final HttpServletResponse response) throws CMSItemNotFoundException
	{
		getGuestValidator().validate(form, bindingResult);
		return processAnonymousCheckoutUserRequest(form, bindingResult, model, request, response);
	}


	/**
	 * Checkout register string.
	 *
	 * @param loginError
	 * 		the login error
	 * @param session
	 * 		the session
	 * @param model
	 * 		the model
	 * @param request
	 * 		the request
	 * @return the string
	 * @throws CMSItemNotFoundException
	 * 		the cms item not found exception
	 */
	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String checkoutRegister(@RequestParam(value = "error", defaultValue = "false") final boolean loginError,
			final HttpSession session, final Model model, final HttpServletRequest request)
			throws CMSItemNotFoundException
	{
		return doCheckoutLogin(loginError, false, session, model, request);
	}

	/**
	 * Do anonymous checkout string.
	 *
	 * @param loginError
	 * 		the login error
	 * @param session
	 * 		the session
	 * @param model
	 * 		the model
	 * @param request
	 * 		the request
	 * @return the string
	 * @throws CMSItemNotFoundException
	 * 		the cms item not found exception
	 */
	@RequestMapping(value = "/guest", method = RequestMethod.GET)
	public String doAnonymousCheckout(@RequestParam(value = "error", defaultValue = "false") final boolean loginError,
			final HttpSession session, final Model model, final HttpServletRequest request) throws CMSItemNotFoundException
	{
		return doCheckoutLogin(loginError, false, session, model, request);
	}

	@Override
	protected String getView()
	{
		return WelagsstorefrontcommonsConstants.Views.Pages.Checkout.CHECKOUT_LOGIN_PAGE;
	}

	@Override
	protected String getSuccessRedirect(final HttpServletRequest request, final HttpServletResponse response)
	{
		final SavedRequest savedRequest = getSavedRequest(request, response);
		if (savedRequest != null)
		{
			return savedRequest.getRedirectUrl();
		}
		else
		{
			if (hasItemsInCart())
			{
				return getCheckoutUrl();
			}
			// Redirect to the main checkout controller to handle checkout.
			return WelWebConstants.URL_CHECKOUT_PAGE;
		}
	}

	private SavedRequest getSavedRequest(final HttpServletRequest request, final HttpServletResponse response)
	{
		return httpSessionRequestCache.getRequest(request, response);
	}


	/**
	 * Checks if there are any items in the cart.
	 *
	 * @return returns true if items found in cart.
	 */
	protected boolean hasItemsInCart()
	{
		final CartData cartData = getCheckoutFlowFacade().getCheckoutCart();

		return (cartData.getEntries() != null && !cartData.getEntries().isEmpty());
	}

	/**
	 * Gets checkout url.
	 *
	 * @return the checkout url
	 */
	protected String getCheckoutUrl()
	{
		// Default to the multi-step checkout
		return "/checkout/multi";
	}

	/**
	 * Gets guest validator.
	 *
	 * @return the guest validator
	 */
	protected GuestValidator getGuestValidator()
	{
		return guestValidator;
	}

	/**
	 * Gets checkout flow facade.
	 *
	 * @return the checkout flow facade
	 */
	protected CheckoutFlowFacade getCheckoutFlowFacade()
	{
		return checkoutFlowFacade;
	}

	@Override
	protected GUIDCookieStrategy getGuidCookieStrategy()
	{
		return guidCookieStrategy;
	}

	/**
	 * Gets authentication manager.
	 *
	 * @return the authentication manager
	 */
	protected AuthenticationManager getAuthenticationManager()
	{
		return authenticationManager;
	}

	public WelAgsCheckoutFacade getWileyCheckoutFacade()
	{
		return wileyCheckoutFacade;
	}


	@Override
	protected CustomerFacade getCustomerFacade()
	{
		return welCustomerFacade;
	}

	@Override
	protected AbstractPageModel getCmsPage() throws CMSItemNotFoundException
	{
		return getContentPageForLabelOrId("checkout-login");
	}

	@ModelAttribute("breadcrumbLabels")
	public List<String> getBreadcrumbLabels()
	{
		return getWileyCheckoutFacade().getBreadcrumbLabels();
	}
}
