package com.wiley.wel.storefront.controllers.constantsts;

/**
 * Constants used in the Web tier
 */
public final class WelWebConstants
{
	private WelWebConstants()
	{
	}

	public static final String MULTI_DIMENSIONAL_PRODUCT = "multiDimensionalProduct";

	public static final String URL_CART_PAGE = "/cart/";
	public static final String URL_CHECKOUT_PAGE = "/checkout/";
}
