package com.wiley.wel.storefront.validators;

import java.util.HashMap;
import java.util.Map;

import com.wiley.welags.storefrontcommons.forms.validation.AbstractWelAgsAddressValidator;

import static com.wiley.welags.storefrontcommons.forms.validation.AbstractWelAgsAddressValidator.WileyAddressField.COUNTRY;
import static com.wiley.welags.storefrontcommons.forms.validation.AbstractWelAgsAddressValidator.WileyAddressField.FIRSTNAME;
import static com.wiley.welags.storefrontcommons.forms.validation.AbstractWelAgsAddressValidator.WileyAddressField.LASTNAME;
import static com.wiley.welags.storefrontcommons.forms.validation.AbstractWelAgsAddressValidator.WileyAddressField.LINE1;
import static com.wiley.welags.storefrontcommons.forms.validation.AbstractWelAgsAddressValidator.WileyAddressField.LINE2;
import static com.wiley.welags.storefrontcommons.forms.validation.AbstractWelAgsAddressValidator.WileyAddressField.PHONE;
import static com.wiley.welags.storefrontcommons.forms.validation.AbstractWelAgsAddressValidator.WileyAddressField.POSTCODE;
import static com.wiley.welags.storefrontcommons.forms.validation.AbstractWelAgsAddressValidator.WileyAddressField.PROVINCE;
import static com.wiley.welags.storefrontcommons.forms.validation.AbstractWelAgsAddressValidator.WileyAddressField.TOWN;
import static com.wiley.welags.storefrontcommons.forms.validation.AbstractWelAgsAddressValidator.WileyAddressField.ZIPCODE;


public class WelAddressValidator extends AbstractWelAgsAddressValidator
{
	private static final Map<WileyAddressField, Integer> MAX_LENGTHS = new HashMap<>();

	static
	{
		MAX_LENGTHS.put(FIRSTNAME, Integer.valueOf(35));
		MAX_LENGTHS.put(LASTNAME, Integer.valueOf(35));
		MAX_LENGTHS.put(LINE1, Integer.valueOf(35));
		MAX_LENGTHS.put(LINE2, Integer.valueOf(35));
		MAX_LENGTHS.put(PROVINCE, Integer.valueOf(35));
		MAX_LENGTHS.put(TOWN, Integer.valueOf(35));
		MAX_LENGTHS.put(POSTCODE, Integer.valueOf(15));
		MAX_LENGTHS.put(ZIPCODE, Integer.valueOf(9));
		MAX_LENGTHS.put(PHONE, Integer.valueOf(15));
		MAX_LENGTHS.put(COUNTRY, Integer.valueOf(MAX_FIELD_LENGTH));
	}

	@Override
	protected Map<WileyAddressField, Integer> getMaxLengths()
	{
		return MAX_LENGTHS;
	}
}
