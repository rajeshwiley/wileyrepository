package com.wiley.wel.storefront.controllers.pages;


import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.ui.Model;

import com.google.gson.Gson;
import com.wiley.core.model.WileyPartnerPageModel;
import com.wiley.core.pages.WileyCMSPageService;
import com.wiley.facades.converters.ProductNodesConverter;
import com.wiley.facades.partner.WileyKPMGEnrollmentFacade;
import com.wiley.facades.product.node.ProductRootNode;
import com.wiley.facades.wel.product.WelProductFacade;
import com.wiley.wel.storefront.forms.WileyKPMGEnrollmentForm;


public class AbstractPartnerPageController extends AbstractPageController
{
	protected static final String PARTNER_ID_CATEGORY_CODE_PATH_VARIABLE_PATTERN = "/{partnerId:.*}/{categoryCode:.*}";
	@Resource
	protected WelProductFacade welProductFacade;
	@Resource
	protected WileyKPMGEnrollmentFacade kpmgEnrollmentFacade;
	@Resource
	protected WileyCMSPageService wileyCMSPageService;
	@Resource(name = "productService")
	private ProductService productService;
	@Resource
	private ProductNodesConverter productNodesConverter;

	protected void addPartnerProductsToModel(final String partnerId, final String categoryCode,
			final Model model, final HttpServletRequest request)
	{
		final List<ProductData> partnerProducts = welProductFacade.getPartnerProductsByCategoryAndPartner(categoryCode,
				partnerId);
		model.addAttribute("partnerProducts", partnerProducts);
		storeProductNodesInModel(model, partnerProducts);

		final Optional<ProductData> mostExpensiveProduct = partnerProducts.stream().findFirst();
		if (mostExpensiveProduct.isPresent())
		{
			model.addAttribute("product", mostExpensiveProduct.get());
			final ProductModel productModel = productService.getProductForCode(mostExpensiveProduct.get().getCode());
			getRequestContextData(request).setProduct(productModel);
		}
	}

	protected void addKpmgEnrollmentFormAttributes(final WileyKPMGEnrollmentForm kpmgEnrollmentForm, final String partnerId,
			final Model model)
	{
		model.addAttribute("kpmgEnrollmentForm", kpmgEnrollmentForm);
		model.addAttribute("functions", kpmgEnrollmentFacade.getFunctions());
		model.addAttribute("offices", kpmgEnrollmentFacade.getOffices(partnerId));
	}

	protected String getPartnerPageByCategoryCode(final String categoryCode, final Model model)
	{
		final WileyPartnerPageModel partnerPageModel = wileyCMSPageService.getPartnerPageByCategoryId(categoryCode);
		storeCmsPageInModel(model, partnerPageModel);
		return getViewForPage(partnerPageModel);
	}

	private void storeProductNodesInModel(final Model model, final List<ProductData> products)
	{
		final int partnerProductsSize = products.size();
		final Map<String, ProductRootNode> productRootNodesMap = new HashMap<>(partnerProductsSize);
		final Map<String, String> productRootNodesJsonMap = new HashMap<>(partnerProductsSize);
		final Gson gson = new Gson();

		for (final ProductData productData : products)
		{
			final String productCode = productData.getCode();
			final ProductRootNode productRootNode = productNodesConverter.convert(productData);
			productRootNodesMap.put(productCode, productRootNode);
			productRootNodesJsonMap.put(productCode, gson.toJson(productRootNode));
		}

		model.addAttribute("productRootNodesMap", productRootNodesMap);
		model.addAttribute("productRootNodesJsonMap", productRootNodesJsonMap);
	}

}
