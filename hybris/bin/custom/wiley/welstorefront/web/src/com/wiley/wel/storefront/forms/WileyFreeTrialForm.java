package com.wiley.wel.storefront.forms;


import de.hybris.platform.acceleratorstorefrontcommons.forms.RegisterForm;

import java.io.Serializable;


/**
 * Created by Uladzimir_Barouski on 3/30/2016.
 */
public class WileyFreeTrialForm extends RegisterForm implements Serializable
{
	private String country;
	private String freeTrial;
	private String employerOrSchool;
	private Boolean signUp;
	private Boolean anonymous;
	// Need to duplicate fields from
	// parent due to serialization functionality
	// and saving type of WileyFreeTrialForm to be RegisterForm
	private String titleCode;
	private String firstName;
	private String lastName;
	private String email;
	private String pwd;
	private String checkPwd;
	private String captcha;
	private String mobileNumber;

	public String getTitleCode()
	{
		return titleCode;
	}

	public void setTitleCode(final String titleCode)
	{
		this.titleCode = titleCode;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(final String firstName)
	{
		this.firstName = firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(final String lastName)
	{
		this.lastName = lastName;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(final String email)
	{
		this.email = email;
	}

	public String getPwd()
	{
		return pwd;
	}

	public void setPwd(final String pwd)
	{
		this.pwd = pwd;
	}

	public String getCheckPwd()
	{
		return checkPwd;
	}

	public void setCheckPwd(final String checkPwd)
	{
		this.checkPwd = checkPwd;
	}

	public String getCaptcha()
	{
		return captcha;
	}

	public void setCaptcha(final String captcha)
	{
		this.captcha = captcha;
	}

	public String getMobileNumber()
	{
		return mobileNumber;
	}

	public void setMobileNumber(final String mobileNumber)
	{
		this.mobileNumber = mobileNumber;
	}

	public String getCountry()
	{
		return country;
	}

	public void setCountry(final String country)
	{
		this.country = country;
	}

	public String getFreeTrial()
	{
		return freeTrial;
	}

	public void setFreeTrial(final String freeTrial)
	{
		this.freeTrial = freeTrial;
	}

	public String getEmployerOrSchool()
	{
		return employerOrSchool;
	}

	public void setEmployerOrSchool(final String employerOrSchool)
	{
		this.employerOrSchool = employerOrSchool;
	}

	public Boolean getSignUp()
	{
		return signUp;
	}

	public void setSignUp(final Boolean signUp)
	{
		this.signUp = signUp;
	}

	public Boolean getAnonymous()
	{
		return anonymous;
	}

	public void setAnonymous(final Boolean anonymous)
	{
		this.anonymous = anonymous;
	}
}
