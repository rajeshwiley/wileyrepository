package com.wiley.wel.storefront.controllers.pages;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.acceleratorservices.customer.CustomerLocationService;
import de.hybris.platform.acceleratorservices.data.RequestContextData;
import de.hybris.platform.acceleratorservices.store.data.UserLocationData;
import de.hybris.platform.acceleratorservices.storefront.data.MetaElementData;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.CategoryPageModel;
import de.hybris.platform.cms2.model.pages.PageTemplateModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.core.model.product.ProductModel;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.Model;

import com.wiley.core.util.url.UrlVisibilityStrategy;
import com.wiley.facades.product.data.ProductComparisonData;
import com.wiley.facades.wel.product.WelProductFacade;
import com.wiley.storefrontcommons.util.WileyPageTitleResolver;

import static java.util.Collections.singletonList;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ProductListPageControllerTest
{
	private static final List<ProductOption> PRODUCT_OPTIONS_FOR_COMPARISON = Arrays.asList(
			ProductOption.BASIC,
			ProductOption.CLASSIFICATION,
			ProductOption.VARIANT_MATRIX_BASE,
			ProductOption.VARIANT_MATRIX_PRICE,
			ProductOption.COMPARISONPRICE);
	
	private static final List<ProductOption> PRODUCT_OPTIONS_FOR_SUPPLEMENTS = Arrays.asList(ProductOption.BASIC,
			ProductOption.PRICE, ProductOption.SUMMARY, ProductOption.DESCRIPTION, ProductOption.GALLERY,
			ProductOption.CLASSIFICATION, ProductOption.VARIANT_FULL, ProductOption.FREE_TRIAL_REFERENCE);

	private static final String PRODUCT_LIST_PAGE_ID = "ProductListPage";

	private static final String CATEGORY_CODE = "WEL_CMA_CATEGORY";
	private static final String CFA_CATEGORY_CODE = "WEL_CfA_CATEGORY";
	private static final String CATEGORY_NAME = "WEL_CMA_CATEGORY";
	private static final String SUPPLEMENTS_CATEGORY_CODE = "WEL_CPA_SUPPLEMENTS";
	private static final String FRONTEND_TEMPLATE_NAME = "category/productListPage";
	public static final String SEO_TITLE_ON_CATEGORY = "seo title on category";
	public static final String CATEGORY_SEO_DESCRIPTION = "seo description on category";
	public static final String DESCRIPTION_ON_CATEGORY = "description on category";
	@InjectMocks
	private ProductListPageController testInstance = new ProductListPageController()
	{
		@Override
		protected RequestContextData getRequestContextData(final HttpServletRequest request)
		{
			return mockRequestContextData;
		}
	};

	@Mock
	private Model mockModel;
	@Mock
	private CategoryModel mockCategoryModel;
	@Mock
	private CategoryPageModel mockCategoryPageModel;
	@Mock
	private CategoryModel mockSupplementsCategoryModel;
	@Mock
	private ProductModel mockSupplementProductModel;
	@Mock
	private HttpServletRequest mockRequest;
	@Mock
	private RequestContextData mockRequestContextData;
	@Mock
	private PageTemplateModel mockPageTemplate;

	@Mock
	private CommerceCategoryService mockCommerceCategoryService;
	@Mock
	private CMSPageService mockCmsPageService;
	@Mock
	private CustomerLocationService mockCustomerLocationService;
	@Mock
	private WelProductFacade mockProductFacade;
	@Mock
	private UrlVisibilityStrategy<CategoryModel> welCategoryUrlVisibilityStrategy;

	@Mock
	private WileyPageTitleResolver wileyPageTitleResolver;

	private ProductComparisonData productComparisonData;
	private ProductData productData;
	private ProductData supplementData;
	private UserLocationData userLocation;



	@Before
	public void setUp() throws CMSItemNotFoundException
	{
		initCategories();
		initData();
		when(mockCommerceCategoryService.getCategoryForCode(CATEGORY_CODE)).thenReturn(mockCategoryModel);
		when(mockCategoryModel.getCode()).thenReturn(CATEGORY_CODE);
		when(welCategoryUrlVisibilityStrategy.isUrlForItemVisible(mockCategoryModel)).thenReturn(true);
		when(mockCmsPageService.getPageForId(PRODUCT_LIST_PAGE_ID)).thenReturn(mockCategoryPageModel);
		when(mockCmsPageService.getFrontendTemplateName(mockPageTemplate)).thenReturn(FRONTEND_TEMPLATE_NAME);


		when(mockProductFacade.getComparisonProductsForCategory(eq(CATEGORY_CODE), eq(PRODUCT_OPTIONS_FOR_COMPARISON),
				anyInt())).thenReturn(productComparisonData);
		when(mockProductFacade.getSupplementProductsForCategory(CATEGORY_CODE, PRODUCT_OPTIONS_FOR_SUPPLEMENTS)).thenReturn(
				singletonList(supplementData));
		when(mockCustomerLocationService.getUserLocation()).thenReturn(userLocation);

		productComparisonData.setProducts(singletonList(productData));
	}

	private void initCategories()
	{
		when(mockCategoryModel.getName()).thenReturn(CATEGORY_NAME);
		when(mockCategoryModel.getAllSubcategories()).thenReturn(singletonList(mockSupplementsCategoryModel));
		when(mockCategoryPageModel.getMasterTemplate()).thenReturn(mockPageTemplate);

		when(mockSupplementsCategoryModel.getCode()).thenReturn(SUPPLEMENTS_CATEGORY_CODE);
		when(mockSupplementsCategoryModel.getProducts()).thenReturn(singletonList(mockSupplementProductModel));
	}

	private void initData()
	{
		productComparisonData = new ProductComparisonData();
		productData = new ProductData();
		supplementData = new ProductData();
		userLocation = new UserLocationData();
	}

	@Test
	public void shouldStoreCommonAttributesInModel()
	{
		testInstance.productList(CATEGORY_CODE, mockModel, mockRequest);
		verify(mockModel).addAttribute("cmsPage", mockCategoryPageModel);
		verify(mockModel).addAttribute("categoryName", CATEGORY_NAME);
		verify(mockModel).addAttribute("pageType", PageType.CATEGORY.name());
		verify(mockModel).addAttribute("userLocation", userLocation);
		verify(mockRequestContextData).setCategory(mockCategoryModel);
	}

	@Test
	public void shouldStoreSupplementsDataInModel()
	{
		testInstance.productList(CATEGORY_CODE, mockModel, mockRequest);
		verify(mockModel).addAttribute(eq("categorySupplementsProducts"), eq(singletonList(supplementData)));
	}

	@Test
	public void shouldReturnCorrectTemplateName()
	{
		String actual = testInstance.productList(CATEGORY_CODE, mockModel, mockRequest);
		assertEquals("pages/" + FRONTEND_TEMPLATE_NAME, actual);
	}

	@Test
	public void shouldReturnPlpForCfaCategory()
	{
		String actual = testInstance.productList(CFA_CATEGORY_CODE, mockModel, mockRequest);
		assertEquals("forward:/wiley-cfa/products/shop-all", actual);
	}

	@Test
	public void shouldSetSeoTitleFromCategory()
	{
		//given
		when(wileyPageTitleResolver.resolveCategoryPageTitle(mockCategoryModel)).thenReturn(SEO_TITLE_ON_CATEGORY);
		//when
		testInstance.productList(CATEGORY_CODE, mockModel, mockRequest);
		//then
		verify(mockModel).addAttribute(AbstractPageController.CMS_PAGE_TITLE, SEO_TITLE_ON_CATEGORY);
	}

	@Test
	public void shouldSetSeoDescriptionFromCategoryDescription()
	{
		//given
		when(mockCategoryModel.getDescription()).thenReturn(DESCRIPTION_ON_CATEGORY);
		//when
		testInstance.productList(CATEGORY_CODE, mockModel, mockRequest);
		//then
		final ArgumentMatcher<Collection<MetaElementData>> matcher = getDescriptionArgumentMatcher(DESCRIPTION_ON_CATEGORY);
		verify(mockModel).addAttribute(eq("metatags"), Mockito.argThat(matcher));
	}


	@Test
	public void shouldSetSeoDescriptionFromCategorySeoDescription()
	{
		//given
		when(mockCategoryModel.getSeoDescriptionTag()).thenReturn(CATEGORY_SEO_DESCRIPTION);
		//when
		testInstance.productList(CATEGORY_CODE, mockModel, mockRequest);
		//then
		final ArgumentMatcher<Collection<MetaElementData>> matcher = getDescriptionArgumentMatcher(CATEGORY_SEO_DESCRIPTION);
		verify(mockModel).addAttribute(eq("metatags"), Mockito.argThat(matcher));
	}


	@Test
	public void emptySeoDescriptionCategory()
	{
		//given
		when(mockCategoryModel.getSeoDescriptionTag()).thenReturn(null);
		when(mockCategoryModel.getDescription()).thenReturn(null);
		//when
		testInstance.productList(CATEGORY_CODE, mockModel, mockRequest);
		//then
		final ArgumentMatcher<Collection<MetaElementData>> matcher = getDescriptionArgumentMatcher(StringUtils.EMPTY);
		verify(mockModel).addAttribute(eq("metatags"), Mockito.argThat(matcher));
	}

	private ArgumentMatcher<Collection<MetaElementData>> getDescriptionArgumentMatcher(final String description)
	{
		return new ArgumentMatcher<Collection<MetaElementData>>()
		{
			@Override
			public boolean matches(final Object argument)
			{
				if (!(argument instanceof Collection<?>))
				{
					return false;
				}
				final Collection<MetaElementData> metaElementDataList = (Collection<MetaElementData>) argument;

				if (metaElementDataList.size() == 2)
				{

					MetaElementData metaElementData = metaElementDataList.stream()
							.filter(e -> e.getName().equals("description"))
							.findFirst()
							.get();
					return metaElementData.getContent().equals(description);
				}

				return false;
			}
		};
	}
}


