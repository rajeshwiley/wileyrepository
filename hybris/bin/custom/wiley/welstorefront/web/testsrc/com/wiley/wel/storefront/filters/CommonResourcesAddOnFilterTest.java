/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wel.storefront.filters;

import de.hybris.bootstrap.annotations.UnitTest;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletException;

import org.apache.commons.io.FilenameUtils;
import org.junit.Test;
import org.mockito.Mockito;


/**
 * The type Common resources add on filter test.
 */
@UnitTest
public class CommonResourcesAddOnFilterTest extends AbstractAddOnFilterTest
{
	/**
	 * Test resource for not existing target.
	 *
	 * @throws ServletException
	 * 		the servlet exception
	 * @throws IOException
	 * 		the io exception
	 */
	@Test
	public void testResourceForNotExistingTarget() throws ServletException, IOException
	{
		//create specific resource
		createResource(addOnSourceResource, "/", "c.txt");
		prepareRequest("c.txt");
		prepareLocalContextPathRequest(STOREFRONT_NAME + "/_ui/addons/" + ADDONTWO_NAME + "/c.txt");
		filter.doFilter(request, response, filterChain);
		verifyFileCreated(webTargetResource, "/", "c.txt");
	}

	/**
	 * Test resource for not existing target in sub folder.
	 *
	 * @throws ServletException
	 * 		the servlet exception
	 * @throws IOException
	 * 		the io exception
	 */
	@Test
	public void testResourceForNotExistingTargetInSubFolder() throws ServletException, IOException
	{
		//create specific resource
		createResource(addOnSourceResource, "/a/b/c", "c.txt");
		prepareRequest("/a/b/c/c.txt");
		prepareLocalContextPathRequest(STOREFRONT_NAME + "/_ui/addons/" + ADDONTWO_NAME + "/a/b/c/c.txt");
		filter.doFilter(request, response, filterChain);
		verifyFileCreated(webTargetResource, "/a/b/c", "c.txt");
	}


	/**
	 * Test resource for update existing target.
	 *
	 * @throws ServletException
	 * 		the servlet exception
	 * @throws IOException
	 * 		the io exception
	 * @throws InterruptedException
	 * 		the interrupted exception
	 */
	@Test
	public void testResourceForUpdateExistingTarget() throws ServletException, IOException, InterruptedException
	{
		//assume resource exists 
		createResource(webTargetResource, "/", "c.txt");
		waitASecond();
		//updating locally  
		createResourceWithContent(addOnSourceResource, "/", "c.txt", "changed here");
		prepareRequest("c.txt");
		prepareLocalContextPathRequest(STOREFRONT_NAME + "/_ui/addons/" + ADDONTWO_NAME + "/c.txt");
		filter.doFilter(request, response, filterChain);
		verifyFileCreatedWithContent(webTargetResource, "/", "c.txt", "changed here");
	}


	/**
	 * Test resource for update existing target in sub folder.
	 *
	 * @throws ServletException
	 * 		the servlet exception
	 * @throws IOException
	 * 		the io exception
	 * @throws InterruptedException
	 * 		the interrupted exception
	 */
	@Test
	public void testResourceForUpdateExistingTargetInSubFolder() throws ServletException, IOException, InterruptedException
	{
		//assume resource exists 
		createResource(webTargetResource, "/a/b/c", "c.txt");
		waitASecond();
		//updating locally  
		createResourceWithContent(addOnSourceResource, "/a/b/c", "c.txt", "changed here");
		prepareRequest("/a/b/c/c.txt");
		prepareLocalContextPathRequest(STOREFRONT_NAME + "/_ui/addons/" + ADDONTWO_NAME + "/a/b/c/c.txt");
		filter.doFilter(request, response, filterChain);
		verifyFileCreatedWithContent(webTargetResource, "/a/b/c", "c.txt", "changed here");
	}


	@Override
	protected String getFolder()
	{
		return UI_FOLDER;
	}


	@Override
	protected void prepareRequest(final String remotePath)
	{
		final String normalized = FilenameUtils.normalize(new File(webTargetResource, remotePath).getPath(), true);
		Mockito.doReturn(normalized).when(filter).getFullPathNameFromRequest(request);
	}

}
