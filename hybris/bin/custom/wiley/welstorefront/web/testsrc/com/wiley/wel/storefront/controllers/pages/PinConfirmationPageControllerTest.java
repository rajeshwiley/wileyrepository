package com.wiley.wel.storefront.controllers.pages;

import de.hybris.platform.acceleratorservices.storefront.util.PageTitleResolver;
import de.hybris.platform.cms2.data.PagePreviewCriteriaData;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.model.pages.PageTemplateModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.cms2.servicelayer.services.CMSPreviewService;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.order.OrderFacade;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.CustomerData;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;


/**
 * Test for Pin Confirmation Page Controller.
 */
public class PinConfirmationPageControllerTest
{
	private static final String FRONTEND_TEMPLATE_NAME = "pinActivation/pinConfirmationPage";
	private static final String PAGE_LABEL = "pinConfirmation";
	private static final String PAGE_ROOT = "pages/";
	private static final String TEST_USER_UID = "testUser";
	private static final String TEST_ORDER_CODE = "orderCode-123";

	@Mock
	private CMSPageService cmsPageService;
	@Mock
	private CMSPreviewService cmsPreviewService;
	@Mock
	private PageTitleResolver pageTitleResolver;
	@Mock
	private PageTemplateModel masterTemplate;
	@Mock
	private OrderFacade orderFacade;
	@Mock
	private UserFacade userFacade;
	@Mock
	private CustomerFacade customerFacade;
	@Mock
	private ContentPageModel contentPageModel;
	@Mock
	private HttpServletRequest request;
	@Mock
	private HttpServletResponse response;

	@InjectMocks
	private final PinConfirmationPageController controller = new PinConfirmationPageController();

	private final Model model = new BindingAwareModelMap();
	private final OrderData orderData = new OrderData();
	private final CustomerData customerData = new CustomerData();

	@Before
	public void prepare() throws CMSItemNotFoundException
	{
		MockitoAnnotations.initMocks(this);

		customerData.setUid(TEST_USER_UID);
		orderData.setCode(TEST_ORDER_CODE);
		orderData.setUser(customerData);

		given(cmsPageService.getFrontendTemplateName(masterTemplate)).willReturn(FRONTEND_TEMPLATE_NAME);
		given(cmsPageService.getPageForLabelOrId(eq(PAGE_LABEL), any(PagePreviewCriteriaData.class))).willReturn(
				contentPageModel);
		given(contentPageModel.getMasterTemplate()).willReturn(masterTemplate);
		given(contentPageModel.getDescription()).willReturn("");
		given(contentPageModel.getKeywords()).willReturn("");
		given(contentPageModel.getTitle()).willReturn("");
		given(orderFacade.getOrderDetailsForCode(TEST_ORDER_CODE)).willReturn(orderData);
		given(userFacade.isAnonymousUser()).willReturn(false);
		given(customerFacade.getCurrentCustomer()).willReturn(customerData);
	}

	@Test
	public void shouldReturnCorrectView() throws CMSItemNotFoundException
	{
		final String view = controller.showPinConfirmationPage(TEST_ORDER_CODE, model, request, response);
		Assert.assertEquals("The correct view is returned.", PAGE_ROOT + FRONTEND_TEMPLATE_NAME, view);
	}
}
