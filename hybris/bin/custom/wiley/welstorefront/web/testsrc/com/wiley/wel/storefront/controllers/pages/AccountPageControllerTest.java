package com.wiley.wel.storefront.controllers.pages;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commerceservices.util.ResponsiveUtils;
import de.hybris.platform.servicelayer.i18n.I18NService;

import java.util.Locale;

import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.facades.order.WileyMagicLinkFacade;

import static org.junit.Assert.assertEquals;


@UnitTest
public class AccountPageControllerTest
{
	private final Model page = Mockito.spy(new BindingAwareModelMap());

	private static final String VIEW_FOR_PAGE = "accountTest.jsp";
	private static final String VIEW_PREFIX = "pages/";
	private static final String FULL_VIEW_PATH = VIEW_PREFIX + VIEW_FOR_PAGE;
	private static final String TITLE_FOR_PAGE = "Account Test Title";

	public static final String CMS_PAGE_MODEL = "cmsPage";
	public static final String REDIRECT_URL = "redirectUrl";

	@InjectMocks
	private final AccountPageController accountController = Mockito.spy(new AccountPageController());
	@Mock
	private I18NFacade i18NFacade;
	@Mock
	private I18NService i18NService;
	@Mock
	private SiteConfigService siteConfigService;
	@Mock
	private RedirectAttributes redirectModel;
	@Mock
	private WileyMagicLinkFacade magicLinkFacadeMock;

	@Before
	public void prepare() throws CMSItemNotFoundException
	{
		MockitoAnnotations.initMocks(this);
		final Locale locale = new Locale("en");
		BDDMockito.given(Boolean.valueOf(page.containsAttribute(CMS_PAGE_MODEL))).willReturn(Boolean.TRUE);
		BDDMockito.given(i18NService.getCurrentLocale()).willReturn(locale);
	}

	@Test
	public void shouldAccounPageBeRedirectedIfUiExperienceIsResponsive() throws CMSItemNotFoundException
	{
		BDDMockito.given(magicLinkFacadeMock.getMagicLink()).willReturn(REDIRECT_URL);
		BDDMockito.given(siteConfigService.getString(Mockito.eq(ResponsiveUtils.DEFAULT_UI_EXPERIENCE), Mockito.anyString()))
				.willReturn(ResponsiveUtils.RESPONSIVE);

		final String accountPage = accountController.account(page, redirectModel);

		assertEquals(AccountPageController.REDIRECT_PREFIX + REDIRECT_URL, accountPage);
	}
}
