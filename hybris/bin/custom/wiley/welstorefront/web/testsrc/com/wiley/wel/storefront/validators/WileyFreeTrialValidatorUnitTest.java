package com.wiley.wel.storefront.validators;

import de.hybris.bootstrap.annotations.UnitTest;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.validation.Errors;

import com.wiley.wel.storefront.forms.WileyFreeTrialForm;


/**
 * Tests logic of {@link WileyFreeTrialFormValidator}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyFreeTrialValidatorUnitTest
{

	private static final int EMPTY_SIZE = 0;

	@Mock
	private Errors errorsMock;

	private final WileyFreeTrialFormValidator testInstance = new WileyFreeTrialFormValidator();

	@Test
	public void testValidateEmptyCountry() throws Exception
	{
		//Given
		final WileyFreeTrialForm wileyFreeTrialForm = new WileyFreeTrialForm();
		wileyFreeTrialForm.setAnonymous(true);
		wileyFreeTrialForm.setCountry(StringUtils.EMPTY);

		//When
		testInstance.validate(wileyFreeTrialForm, errorsMock);

		//Then
		Mockito.verify(errorsMock).rejectValue("country", "freeTrial.error.country.empty");
	}

	@Test
	public void testValidateEmptyCourse() throws Exception
	{
		//Given
		final WileyFreeTrialForm wileyFreeTrialForm = new WileyFreeTrialForm();
		wileyFreeTrialForm.setAnonymous(true);
		wileyFreeTrialForm.setFreeTrial(StringUtils.EMPTY);

		//When
		testInstance.validate(wileyFreeTrialForm, errorsMock);

		//Then
		Mockito.verify(errorsMock).rejectValue("freeTrial", "freeTrial.error.course.empty");
	}

	@Test
	public void testValidateEmptyEmployerOrSchool() throws Exception
	{
		//Given
		final WileyFreeTrialForm wileyFreeTrialForm = new WileyFreeTrialForm();
		wileyFreeTrialForm.setAnonymous(true);
		wileyFreeTrialForm.setEmployerOrSchool(StringUtils.EMPTY);

		//When
		testInstance.validate(wileyFreeTrialForm, errorsMock);

		//Then
		Mockito.verify(errorsMock).rejectValue("employerOrSchool", "freeTrial.error.employerOrSchool.empty");
	}

	@Test
	public void testCountryIsNotValidatedWhenUserLoggedIn() throws Exception
	{
		//Given
		final WileyFreeTrialForm wileyFreeTrialForm = new WileyFreeTrialForm();
		wileyFreeTrialForm.setAnonymous(false);
		wileyFreeTrialForm.setFreeTrial(StringUtils.EMPTY);

		Assert.assertEquals(EMPTY_SIZE, errorsMock.getFieldErrorCount());

		//When
		testInstance.validate(wileyFreeTrialForm, errorsMock);

		//Then
		Assert.assertEquals(EMPTY_SIZE, errorsMock.getFieldErrorCount());
	}
}
