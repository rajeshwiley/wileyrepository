/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.wel.storefront.security;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorstorefrontcommons.security.BruteForceAttackCounter;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.Authentication;

import com.wiley.facades.wel.order.WelMultiDimensionalCartFacade;
import com.wiley.facades.welags.customer.WelAgsCustomerFacade;


/**
 * The type Storefront authentication success handler test.
 */
@UnitTest
public class StorefrontAuthenticationSuccessHandlerTest
{

	private final StorefrontAuthenticationSuccessHandler authenticationSuccessHandler = BDDMockito
			.spy(new StorefrontAuthenticationSuccessHandler());

	@Mock
	private SessionService sessionService;

	@Mock
	private HttpServletRequest request;

	@Mock
	private HttpServletResponse response;

	@Mock
	private Authentication authentication;

	@Mock
	private WelMultiDimensionalCartFacade cartFacade;
	@Mock
	private WelAgsCustomerFacade customerFacade;
	@Mock
	private BruteForceAttackCounter bruteForceAttackCounter;
	@Mock
	private CartData savedCart1;
	@Mock
	private CartData savedCart2;
	@Mock
	private CartData sessionCart;

	private List<String> excludedCartsGuid;

	private static final String SAVED_CART_1 = "savedCart1";
	private static final String SESSION_CART = "sessionCart";

	/**
	 * Sets up.
	 */
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		BDDMockito.given(authenticationSuccessHandler.getCartFacade()).willReturn(cartFacade);
		BDDMockito.given(authenticationSuccessHandler.getCustomerFacade()).willReturn(customerFacade);
		BDDMockito.given(authenticationSuccessHandler.getBruteForceAttackCounter()).willReturn(bruteForceAttackCounter);
		BDDMockito.given(authenticationSuccessHandler.getSessionService()).willReturn(sessionService);
		BDDMockito.given(authenticationSuccessHandler.getListRedirectUrlsForceDefaultTarget())
				.willReturn(new ArrayList<String>());
		createUserCarts();
	}

	/**
	 * Should continue to default url.
	 */
	@Test
	public void shouldContinueToDefaultUrl()
	{
		BDDMockito.doReturn(Boolean.FALSE).when(authenticationSuccessHandler).isAlwaysUseDefaultTargetUrl();
		authenticationSuccessHandler.setDefaultTargetUrl("/im/a/default/");

		Assert.assertTrue(StringUtils.equals("/im/a/default/",
				authenticationSuccessHandler.determineTargetUrl(request, response)));
	}

	/**
	 * Should continue to checkout no merge.
	 */
	@Test
	public void shouldContinueToCheckoutNoMerge()
	{
		BDDMockito.doReturn(Boolean.FALSE).when(authenticationSuccessHandler).isAlwaysUseDefaultTargetUrl();
		authenticationSuccessHandler.setDefaultTargetUrl("/checkout");

		Assert.assertTrue(StringUtils.equals("/checkout", authenticationSuccessHandler.determineTargetUrl(request, response)));
	}

	/**
	 * Should not return session cart.
	 */
	@Test
	public void shouldNotReturnSessionCart()
	{
		excludedCartsGuid.add(SESSION_CART);
		BDDMockito.given(authenticationSuccessHandler.getCartFacade().getMostRecentCartGuidForUser(excludedCartsGuid)).willReturn(
				null);

		Assert.assertNull(authenticationSuccessHandler.getMostRecentSavedCartGuid(SESSION_CART));
	}

	/**
	 * Should return saved cart.
	 */
	@Test
	public void shouldReturnSavedCart()
	{
		excludedCartsGuid.add(SESSION_CART);
		BDDMockito.given(authenticationSuccessHandler.getCartFacade().getMostRecentCartGuidForUser(excludedCartsGuid)).willReturn(
				SAVED_CART_1);

		Assert.assertEquals(authenticationSuccessHandler.getMostRecentSavedCartGuid(SESSION_CART), SAVED_CART_1);
	}

	/**
	 * Should not merge carts none saved.
	 *
	 * @throws Exception
	 * 		the exception
	 */
	@Test
	public void shouldNotMergeCartsNoneSaved() throws Exception
	{
		setupAuthenticationHandler();
		BDDMockito.given(authenticationSuccessHandler.getMostRecentSavedCartGuid(SESSION_CART)).willReturn(null);

		authenticationSuccessHandler.onAuthenticationSuccess(request, response, authentication);
		BDDMockito.verify(authenticationSuccessHandler.getCartFacade(), BDDMockito.times(0)).restoreCartAndMerge(
				BDDMockito.anyString(), BDDMockito.anyString());
	}

	/**
	 * Should not merge carts on checkout login.
	 *
	 * @throws Exception
	 * 		the exception
	 */
	@Test
	public void shouldNotMergeCartsOnCheckoutLogin() throws Exception
	{
		setupAuthenticationHandler();
		BDDMockito.given(authenticationSuccessHandler.getMostRecentSavedCartGuid(SESSION_CART)).willReturn(SESSION_CART);
		BDDMockito.given(authenticationSuccessHandler.determineTargetUrl(request, response)).willReturn("/checkout");
		authenticationSuccessHandler.onAuthenticationSuccess(request, response, authentication);
		BDDMockito.verify(authenticationSuccessHandler.getCartFacade(), BDDMockito.times(0)).restoreCartAndMerge(
				BDDMockito.anyString(), BDDMockito.anyString());
	}

	/**
	 * Should not merge carts current cart empty.
	 *
	 * @throws Exception
	 * 		the exception
	 */
	@Test
	public void shouldNotMergeCartsCurrentCartEmpty() throws Exception
	{
		BDDMockito.given(Boolean.valueOf(cartFacade.hasEntries())).willReturn(Boolean.FALSE);

		setupAuthenticationHandler();
		authenticationSuccessHandler.onAuthenticationSuccess(request, response, authentication);
		BDDMockito.verify(authenticationSuccessHandler.getCartFacade(), BDDMockito.times(0)).restoreCartAndMerge(SAVED_CART_1,
				SESSION_CART);
	}

	/**
	 * Should merge carts.
	 *
	 * @throws Exception
	 * 		the exception
	 */
	@Test
	public void shouldMergeCarts() throws Exception
	{
		BDDMockito.given(Boolean.valueOf(cartFacade.hasEntries())).willReturn(Boolean.TRUE);

		setupAuthenticationHandler();
		authenticationSuccessHandler.onAuthenticationSuccess(request, response, authentication);
		BDDMockito.verify(authenticationSuccessHandler.getCartFacade()).restoreCartAndMerge(SAVED_CART_1, SESSION_CART);
	}

	/**
	 * Sets authentication handler.
	 */
	protected void setupAuthenticationHandler()
	{
		BDDMockito.doReturn(Boolean.FALSE).when(authenticationSuccessHandler).isAlwaysUseDefaultTargetUrl();

		BDDMockito.doNothing().when(customerFacade).loginSuccess();
		BDDMockito.given(customerFacade.getCurrentCustomerUid()).willReturn("customer");

		excludedCartsGuid.add(SESSION_CART);
		BDDMockito.given(authenticationSuccessHandler.getCartFacade().getMostRecentCartGuidForUser(excludedCartsGuid)).willReturn(
				SAVED_CART_1);
		BDDMockito.given(cartFacade.hasSessionCart()).willReturn(Boolean.TRUE);
		BDDMockito.given(cartFacade.getSessionCartGuid()).willReturn(SESSION_CART);

		BDDMockito.doNothing().when(bruteForceAttackCounter).resetUserCounter("customer");
	}

	/**
	 * Create user carts.
	 */
	protected void createUserCarts()
	{
		BDDMockito.given(savedCart1.getGuid()).willReturn(SAVED_CART_1);
		BDDMockito.given(savedCart2.getGuid()).willReturn("savedCart2");
		BDDMockito.given(sessionCart.getGuid()).willReturn(SESSION_CART);
		final OrderEntryData entry = BDDMockito.mock(OrderEntryData.class);
		final List<OrderEntryData> orderEntries = new ArrayList();
		orderEntries.add(entry);

		BDDMockito.given(sessionCart.getEntries()).willReturn(orderEntries);

		final List<CartData> savedCarts = new ArrayList();

		savedCarts.add(sessionCart);

		excludedCartsGuid = new ArrayList<String>();
	}
}
