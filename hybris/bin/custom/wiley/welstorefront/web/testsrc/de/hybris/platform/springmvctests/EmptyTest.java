/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package de.hybris.platform.springmvctests;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Test;

import junit.framework.Assert;


/**
 * The type Empty test.
 */
@UnitTest
public class EmptyTest
{
	/**
	 * Empty test.
	 */
	@Test
	public void emptyTest()
	{
		Assert.assertTrue(true);
	}
}
