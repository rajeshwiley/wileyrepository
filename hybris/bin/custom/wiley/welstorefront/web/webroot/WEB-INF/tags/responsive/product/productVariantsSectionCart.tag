<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="rootNode" required="true" type="com.wiley.facades.product.node.ProductNode" %>
<%@ attribute name="productInCart" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ attribute name="formIndex" required="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>

<c:if test="${rootNode.nodes != null && fn:length(rootNode.nodes) > 0}">
    <div>
        <c:forEach items="${rootNode.nodes}" var="node" varStatus="loopFirstLevel">
            <c:choose>
            <c:when test="${not empty node.nodes}">
              <c:forEach items="${node.nodes}" var="nodeSecondLevel" varStatus="loopSecondLevel">
              <c:set var="checked" value="${(productInCart.code == nodeSecondLevel.value.sku)? 'checked' : ''}"/>
              <c:choose>

              <c:when test="${productInCart.CFACategory}">
				   <c:if test="${productInCart.levelVariantCategoryCode ==  node.code}">
                       <product:productType loopIndex="${loopSecondLevel.index}" node="${nodeSecondLevel}" name="${nodeSecondLevel.name}" checkedItem="${checked}" formIndex="${formIndex}"/>
 				   </c:if>
              </c:when>
              <c:otherwise>
				  <c:if test="${productInCart.partVariantCategoryCode ==  nodeSecondLevel.code }">
                      <product:productType loopIndex="${loopFirstLevel.index}" node="${nodeSecondLevel}" name="${node.name}" checkedItem="${checked}" formIndex="${formIndex}"/>
				  </c:if>
              </c:otherwise>
              </c:choose>
             </c:forEach>
            </c:when>
             <c:otherwise>
                    <c:set var="checked" value="${(productInCart.code == node.value.sku)? 'checked' : ''}"/>
                    <c:if test="${not empty productInCart.typeVariantCategoryCode}">
                        <product:productType loopIndex="${loopFirstLevel.index}" node="${node}" name="${node.name}" checkedItem="${checked}" formIndex="${formIndex}"/>
                    </c:if>
             </c:otherwise>
            </c:choose>
        </c:forEach>
    </div>
</c:if>
