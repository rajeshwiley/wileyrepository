if (!wiley) {var wiley = {};}

wiley.initPopover = function() {
    wiley.initPopover.options = {
        html: true,
        trigger: 'hover',
        placement: 'auto',
        container: 'body'
    };

    wiley.initPopover.popovers = $('[data-toggle="popover"]');

    wiley.initPopover.popovers
        .popover(wiley.initPopover.options)
        .on('touchstart', function (e) {
            if(wiley.isMobileView) {
                return false;
            }

            var $this = $(this);
            e.stopPropagation();
            e.preventDefault();

            if($this.hasClass('opened')) {
                wiley.initPopover.hide($this);
                return false;
            }

            if(wiley.initPopover.shown !== undefined) {
                wiley.initPopover.hide(wiley.initPopover.shown);
            }

            wiley.initPopover.shown = $this.popover('show').addClass('opened');
        })
        .on('mouseover', function() {
            if(wiley.isMobileView) {
                return false;
            }

            wiley.initPopover.shown = $(this).addClass('opened');
        })
        .on('mouseout', function() {
            if(wiley.isMobileView) {
                return false;
            }

            $(this).removeClass('opened');
            delete wiley.initPopover.shown;
        });

    wiley.initPopover.hide = function(element) {
        element.popover('hide').removeClass('opened');
        delete wiley.initPopover.shown;
    };

    $('body').on('touchstart', function() {
        if(!wiley.isMobileView && wiley.initPopover.shown !== undefined) {
            wiley.initPopover.hide(wiley.initPopover.shown);
        }
    });
};

wiley.mediaGallery = function() {
    var sync1 = $("#sync1");
    var sync2 = $("#sync2");
    var sync3 = $("#owl-text");

    if(sync1.length) {
        sync1.owlCarousel({
            singleItem: true,
            slideSpeed: 1000,
            navigation: true,
            pagination: false,
            afterAction: syncPosition,
            responsiveRefreshRate: 200
        });
    }

    if(sync2.length) {
        sync2.owlCarousel({
            items: 4,
            pagination: false,
            itemsDesktop: false,
            itemsDesktopSmall: false,
            itemsTablet: false,
            itemsMobile: false,
            responsiveRefreshRate : 100,
            afterInit : function(el) {
                el.find(".owl-item").eq(0).addClass("synced");
            }
        });
    }

    if(sync3.length) {
        sync3.owlCarousel({
            navigation : false,
            stopOnHover : true,
            paginationSpeed : 1000,
            goToFirstSpeed : 2000,
            singleItem: true,
            autoHeight : true
        });
    }

    function syncPosition(el) {
        var current = this.currentItem;
        sync2
            .find(".owl-item")
            .removeClass("synced")
            .eq(current)
            .addClass("synced");

        if($("#sync2").data("owlCarousel") !== undefined) {
            center(current)
        }
    }

    sync2.on("click", ".owl-item", function(e) {
        e.preventDefault();
        var number = $(this).data("owlItem");
        sync1.trigger("owl.goTo",number);
    });

    function center(number) {
        var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
        var num = number;
        var found = false;

        for(var i in sync2visible) {
            if(num === sync2visible[i]) {
                found = true;
            }
        }

        if(found === false) {
            if(num > sync2visible[sync2visible.length-1]) {
                sync2.trigger("owl.goTo", num - sync2visible.length+2)
            } else {
                if(num - 1 === -1) {
                    num = 0;
                }
                sync2.trigger("owl.goTo", num);
            }
        } else if(num === sync2visible[sync2visible.length-1]) {
            sync2.trigger("owl.goTo", sync2visible[1])
        } else if(num === sync2visible[0]) {
            sync2.trigger("owl.goTo", num-1)
        }
    }
};

wiley.showMoreLess = function () {
	var showHide = $(".show-hide");

	if (showHide) {
		showHide.on("click", function(e) {
			e.preventDefault();
			var currentLink = $(this);

			if(currentLink.hasClass("show-less")) {
				currentLink.removeClass("show-less");
				currentLink.prev().children("li:nth-child(n+4)").fadeOut("0", function() {
					currentLink.text("Show more ...");
				});
			} else {
				currentLink.addClass("show-less");
				currentLink.prev().children("li:nth-child(n+4)").fadeIn();
				currentLink.text("Show less ...");
			}
		});
	}
};

wiley.dropDown = function() {

    $.each($('.drop-down'), function() {
        var status = $(this).find('.dropdown-status');
        var menu = $(this).find('.dropdown-menu');
        var li = menu.find('li');

        menu.on('click', 'li', function(e) {
            var $this = $(this);

            $.each(li, function() {
                if($(this).hasClass('selected')) {
                    $(this).removeClass('selected');
                }
            });

            $this.addClass('selected');

            var html = $this.html();
            status.text(html);
        });
    });
};

wiley.validationForm = function() {
    var validationForm = $('#cfa-form');
    var generalMessage = "This field is required.";

    if (validationForm.length) {
        validationForm.validate({
            rules : {
                firstName : "required",
                lastName : "required",
                email : {
                    required : true,
                    email : true,
                },
                password : "required",
                password_confirm : {
                    required : true,
                    equalTo : "#password"
                },
            },

            messages : {
                firstName : generalMessage,
                lastName : generalMessage,
                email : {
                    required : generalMessage,
                    email : "Please enter a valid email address.",
                },
                password : generalMessage,
                password_confirm : {
                    required : generalMessage,
                    equalTo : "Confirm a password",
                },
            },

            highlight : function(element) {
                var id_attr = "#" + $(element).attr("id") + "1";
                $(element).closest('.form-group').removeClass('has-success')
                    .addClass('has-error');
                $(id_attr).removeClass('glyphicon-ok').addClass(
                    'glyphicon-remove');
            },

            unhighlight : function(element) {
                var id_attr = "#" + $(element).attr("id") + "1";
                $(element).closest('.form-group').removeClass('has-error')
                    .addClass('has-success');
                $(id_attr).removeClass('glyphicon-remove').addClass(
                    'glyphicon-ok');
            },

            submitHandler : function(form) {
                form.submit();
            },
        })
    }
};

wiley.initQuantityFields = function() {
    $('.quantity-field').each(function() {
        var $this = $(this);
        var buttonPlus = $this.find('.btn-quantity-plus');
        var buttonMinus = $this.find('.btn-quantity-minus');
        var formControlQuantity = $this.find('.form-control-quantity');

        formControlQuantity.on('keypress', function (evt){
            var charCode = evt.which;
            // accept only enter and numbers
            return (charCode <= 31 || (charCode >= 48 && charCode <= 57));
        });

        buttonPlus.click(function(e) {
            e.preventDefault();

            var currentVal = parseInt(formControlQuantity.val(), 10);

            if (!isNaN(currentVal) && currentVal < 99) {
                formControlQuantity.val(currentVal + 1);
            }

            formControlQuantity.blur();
        });

        buttonMinus.click(function(e) {
            e.preventDefault();

            var currentVal = parseInt(formControlQuantity.val(), 10);

            if (!isNaN(currentVal) && currentVal > 1) {
                formControlQuantity.val(currentVal - 1);
            } else {
                formControlQuantity.val(1);
            }
            formControlQuantity.blur();
        });
    });
};

wiley.initAccordionForLoginPage = function() {
    var accordion = $('#accordion');

    if(!accordion.length) {
        return false;
    }

    var panelGroup = $('.panel-group');
    var panelCollapse = panelGroup.find('.panel-collapse');
    var errorsCount = 0;
    var _thisCollapsed;
    var isBillingAddressForm = panelGroup.hasClass('billing-address');

    panelCollapse.each(function() {
        var _this = $(this);

        errorsCount = _this.find('.has-error').length;

        if(errorsCount > 0 && !isBillingAddressForm) {
            _thisCollapsed = _this.closest('.panel').find('.collapsed');
            _thisCollapsed.removeClass('collapsed');
            _this.addClass('in');
        }
    });
};

wiley.initBillingForm = function() {
    var sameAsBillingCheckbox = $('#sameAsBilling');
    var billingFields = sameAsBillingCheckbox.closest('.same-as-billing').siblings('.form-fields');

    sameAsBillingCheckbox.on('change', function() {
        billingFields.stop().slideToggle();
    });
};

wiley.initMainHeader = function() {
    wiley.mainHeader = $('#main-header');

	if(wiley.isMobileView) {
		var menuItem = $('.product-menu > li ul');

		menuItem.each(function () {
			$(this).parent('li').addClass('inner-menu');
		});
	}

    wiley.mainHeader.on("touchstart", 'li', function(e) {
        var menuItem = $(this);
        var hasLink = !menuItem.hasClass('not-link');
        var isHovered = menuItem.hasClass('hover');
        var subMenu = menuItem.children('ul');

        if(!wiley.isMobileView) {
            menuItem.closest('ul').find('li').removeClass('animated hover');
        } else if (isHovered) {
            menuItem.find('li').add(menuItem).removeClass('animated hover');
        }

        if(!isHovered && subMenu.length) {
            menuItem.addClass('animated hover');

            return false;
        }

        return hasLink;
    });

    $('body').on('touchstart', function(e) {
        if(!wiley.isMobileView) {
            wiley.mainHeader.closeMenu();
        }

    });

    wiley.mainHeader.closeMenu = function() {
        wiley.mainHeader.find('li').removeClass('animated hover');
    };
};

wiley.enableLiveChatLink = function() {
    $('#main-header').find('.livechat').on('click', function(e) {
        e.preventDefault();

        /* We use data-* attribute instead of simple href attribute to store link address
           in order to make the link unclickable while the page is loading and this handler is not yet attached,
           because otherwise clicking on the link before this event has been attached opens LiveChat in the same window */
        window.open( $('.livechat').data("targetlink"), 'livepersonwindow', 'toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,resizable=yes,width=475,height=400,left=0,top=0');
    });
};

wiley.initAutocompleteField = function() {
	var $partnerChooser = $('#partner-chooser');

	$partnerChooser.autocomplete({
		source: $partnerChooser.data('partners'),
		appendTo: $partnerChooser.parent(),
		focus: function( event, ui ) {
			$partnerChooser.val( ui.item.label );
			return false;
		},
		select: function( event, ui ) {
            $partnerChooser.data('selected-partner', ui.item.label);
            var pathName = window.location.pathname;
            var regExp = /\/$/;
            var pathNameTrimmed = pathName.replace(regExp, "");
            var paths = pathNameTrimmed.split("/");

            var category = paths[paths.length - 1];
            window.location.href = ACC.config.encodedContextPath + "/partner/" + ui.item.value + "/" + category;
            return false;
		}
	});

	$partnerChooser.on('blur', function () {
		var selectedPartner = $partnerChooser.data('selected-partner');
		$partnerChooser.val(selectedPartner);
	});
};

wiley.setMaxHeightSections = function(sections) {
	var maxHeight = 0;
	sections.height('auto');

	sections.each(function() {
		maxHeight = Math.max($(this).height(), maxHeight);
	});

	$(sections).height(maxHeight);
};

wiley.initProductDetailedInfo = function() {
    var $productItems = $('.product-detailed-info .row');
    var $productInfoSections, $productCheckoutSections;

    $productItems.each(function() {

        $productInfoSections = $(this).find('.product-info-section');
        $productCheckoutSections = $(this).find('.product-checkout-section');

        wiley.setMaxHeightSections($productInfoSections);
        wiley.setMaxHeightSections($productCheckoutSections);
    });
};

wiley.destroyProductDetailedInfo = function() {
    var $productItems = $('.product-detailed-info .row');
    var $productInfoSections = $productItems.find('.product-info-section');
    var $productCheckoutSections = $productItems.find('.product-checkout-section');

    $productItems.each(function() {

        $productInfoSections.removeAttr('style');
        $productCheckoutSections.removeAttr('style');

    });
};

wiley.collapseMobileSocialLinks = function() {
    var navbar = $('#navbar');
    var socialLinks = navbar.find('.social-links');

    navbar.on('hide.bs.collapse', function() {
        socialLinks.css('display', 'none');
    });

    navbar.on('show.bs.collapse in', function() {
        socialLinks.fadeIn('slow');
    });
};

wiley.showActiveTabBody = function() {
    if (!ACC.tabs.jsTabs) {
        return false;
    }

    ACC.tabs.jsTabs.each(function() {
        var tabs = $(this);
        var activeTabId = tabs.find('.tabs-list .active a').attr('href');

        $(activeTabId).addClass('active');
    });
};

wiley.showOrHidePaymentStep = function() {
    var totalPrice = $("div.order-details-info").find('div.total span:last-child').html();
    var paymentBreadcrumbsStep = 3;
    var orderConfirmationBreadcrumbsStep = 5;
    // check, if it is not a student checkout flow.
    // usual flow has 5 steps, student flow has 6 steps.
    if ($("ul.progress-line").children("li.step").length === 5){
       paymentBreadcrumbsStep = 2;
       orderConfirmationBreadcrumbsStep = 4;
    }
    // need to change numbers of steps, if the Payment step was hided or showed.
    var stepAfterPayment = paymentBreadcrumbsStep;
    var paymentStep = $("ul.progress-line").find("li.step").get(paymentBreadcrumbsStep);
    // the first symbol is a symbol of currency.
    if (totalPrice != null && 0 < totalPrice.length && totalPrice[1] === "0"){
        $(paymentStep).css("display", "none");
    }
    else {
        $(paymentStep).css("display", "table-cell");
        stepAfterPayment = stepAfterPayment + 1;
    }
    // below, change number of checkout steps.
    var orderConfirmationStep = $("ul.progress-line").find("li.step").get(orderConfirmationBreadcrumbsStep);
    ++paymentBreadcrumbsStep;
    ++stepAfterPayment;
    if ($(orderConfirmationStep).hasClass('step-completed') !== true){
        $($("ul.progress-line").find("li.step")[paymentBreadcrumbsStep]).find(".step-number").html(stepAfterPayment);
    }
    $($("ul.progress-line").find("li.step")[++paymentBreadcrumbsStep]).find(".step-number").html(++stepAfterPayment);
};

$(document).ready(function() {
    wiley.isMobileView = ($(window).width() <= 640);

    if(!wiley.isMobileView) {
        wiley.initProductDetailedInfo();
    } else {
        wiley.destroyProductDetailedInfo();
    }

    wiley.initMainHeader();
    wiley.initPopover();
    wiley.mediaGallery();
    wiley.showMoreLess();
    wiley.dropDown();
    wiley.validationForm();
    wiley.initQuantityFields();
    wiley.initAccordionForLoginPage();
    wiley.initBillingForm();
    wiley.enableLiveChatLink();
    wiley.initAutocompleteField();
    wiley.collapseMobileSocialLinks();
    wiley.showOrHidePaymentStep();
});

$(window).resize(function() {
    wiley.isMobileView = ($(window).width() <= 640);


    if(wiley.initPopover.shown !== undefined) {
        wiley.initPopover.hide(wiley.initPopover.shown);

        wiley.initPopover.popovers.popover('destroy');
    }

    if(!wiley.isMobileView) {
        wiley.initPopover.popovers.popover(wiley.initPopover.options);
        wiley.initProductDetailedInfo();
    } else {
        wiley.destroyProductDetailedInfo();
    }

    wiley.showActiveTabBody();
});

$(window).on('orientationchange', function() {
    wiley.isMobileView = ($(window).width() <= 640);

    wiley.mainHeader.closeMenu();
});