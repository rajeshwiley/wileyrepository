<%@ page trimDirectiveWhitespaces="true" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>

<c:if test="${categoriesLinks ne null && !empty categoriesLinks}">
	<ul class="tabs-list links-tabs-list">
		<c:forEach items="${categoriesLinks}" var="categoryLink">
			<c:set var="activeClass" value=""/>
			<c:if test="${activeCategory eq categoryLink.value['description']}">
				<c:set var="activeClass" value="active"/>
			</c:if>
			<li class="${activeClass}">
				<c:url value="${categoryLink.value['link']}" var="link"/>
				<a href="${link}"><c:out value="${categoryLink.value['description']}"/></a>
			</li>
		</c:forEach>
	</ul>
</c:if>