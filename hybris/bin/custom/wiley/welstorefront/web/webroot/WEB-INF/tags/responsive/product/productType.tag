<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="node" required="true" type="com.wiley.facades.product.node.ProductNode" %>
<%@ attribute name="formIndex" required="false" %>
<%@ attribute name="loopIndex" required="true" %>
<%@ attribute name="name" required="true" %>
<%@ attribute name="checkedItem" required="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:if test="${loopIndex == 0}" >
 <div><b><spring:theme code="basket.page.message.product.type"/></b></div>
</c:if>
<input class="form-control-print-ebook" type="radio" name="sku" id="product-variant-${node.value.sku}-${formIndex}"  value="${node.value.sku}" ${checkedItem}/>
<label for="product-variant-${node.value.sku}-${formIndex}" >${name}</label>

