<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<template:page pageTitle="${pageTitle}"	containerCSSClass="container">
	<cms:pageSlot position="ProductsTitle" var="feature">
		<cms:component component="${feature}" element="div" />
	</cms:pageSlot>
	<c:if test="${fn:length(categoryProducts) > 0}">
		<cms:pageSlot position="ProductsList" var="feature">
			<cms:component component="${feature}" element="div" />
		</cms:pageSlot>
	</c:if>
</template:page>
