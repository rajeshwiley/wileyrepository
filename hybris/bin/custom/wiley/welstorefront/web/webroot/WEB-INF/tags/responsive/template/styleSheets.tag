<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="g" uri="http://granule.com/tags/accelerator"%>
<%@ taglib prefix="compressible" tagdir="/WEB-INF/tags/responsive/template/compressible" %>
<%@ taglib prefix="cms" tagdir="/WEB-INF/tags/responsive/template/cms" %>
<%@ taglib prefix="wt" uri="http://hybris.com/tld/wro4j_tag" %>


<c:choose>
	<c:when test="${wro4jEnabled}">
	    <%--  Common CSS files --%>
	    <link rel="stylesheet" type="text/css" href="<wt:WroResourceTag path='${commonResourcePath}/min' resource='responsive_common.css' />" />
		<%--  AddOn Common CSS files --%>
		<link rel="stylesheet" type="text/css" media="all" href="<wt:WroResourceTag path='${commonResourcePath}/min' resource='addons_responsive_common.css' />" />
		<%-- Theme CSS files --%>
		<!--[if IE 9]>
			<link rel="stylesheet" type="text/css" media="all" href="<wt:WroResourceTag path='${themeResourcePath}/min' resource='responsive_theme_${themeName}_ie9.css' />" />
		<![endif]-->
		<![if !IE]>
			<link rel="stylesheet" type="text/css" media="all" href="<wt:WroResourceTag path='${themeResourcePath}/min' resource='responsive_theme_${themeName}.css' />" />
		<![endif]>
		<c:if test="${not empty addOnThemeCssPaths}">
			<%--  AddOn Theme CSS files --%>
			<link rel="stylesheet" type="text/css" media="all" href="<wt:WroResourceTag path='${themeResourcePath}/min' resource='addons_responsive_theme_${themeName}.css' />" />
		</c:if>
	</c:when>
	<c:otherwise>
		<compressible:css/>
	</c:otherwise>
</c:choose>


<%-- <link rel="stylesheet" href="${commonResourcePath}/blueprint/print.css" type="text/css" media="print" /> 
<style type="text/css" media="print">
	@IMPORT url("${commonResourcePath}/blueprint/print.css");
</style>
--%>

<cms:previewCSS cmsPageRequestContextData="${cmsPageRequestContextData}" />
