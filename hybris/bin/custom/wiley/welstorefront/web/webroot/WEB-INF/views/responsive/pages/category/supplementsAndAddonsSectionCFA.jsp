<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="comparison-products">
	<c:forEach items="${wileyProductListCFAData.gridData}" var="entry">
		<c:if test="${not empty entry.value.supplements}">
			<div class="comparison-product">
				<div class="comparison-product-title">
					<a name="${entry.key}"></a>
					${entry.value.levelName}&nbsp;<spring:theme code="plp.cfa.examSupplements"/> - ${entry.value.examDate}
				</div>
				<div class="product-brief">
				    <div class="container-fluid">
				    	<div class="row">
							<c:forEach items="${entry.value.supplements}" var="supplement" varStatus="status">
								<product:supplementsListerItem product="${supplement}"/>
							</c:forEach>
						</div>
					</div>
				</div>
			</div>
		</c:if>
		<c:if test="${not empty entry.value.addons}">
			<div class="comparison-product">
				<div class="comparison-product-title">
					<a name="${entry.key}"></a>
					${entry.value.levelName}&nbsp;<spring:theme code="plp.cfa.courseAddons"/> - ${entry.value.examDate}
				</div>
				<div class="product-brief">
				    <div class="container-fluid">
				    	<div class="row">
							<c:forEach items="${entry.value.addons}" var="addon">
								<product:supplementsListerItem product="${addon}"/>
							</c:forEach>
						</div>
					</div>
				</div>
			</div>
		</c:if>
	</c:forEach>
</div>