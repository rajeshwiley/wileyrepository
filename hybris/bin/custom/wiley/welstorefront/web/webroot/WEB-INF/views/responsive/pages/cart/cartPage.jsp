<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>
	
<template:page pageTitle="${pageTitle}" hideCartLink="true" showLoginLinkOnMobile="false" containerCSSClass="container my-cart">

	<cart:cartValidation/>
	
    
	<div class="subscriptions">
		<cms:pageSlot position="TopContent" var="feature">
			<cms:component component="${feature}"/>
		</cms:pageSlot>
	 <div class="subscriptions-footer"> 
	   <c:if test="${not empty cartData.entries}">
			   <cms:pageSlot position="CenterLeftContentSlot" var="feature">
				   <cms:component component="${feature}"/>
			   </cms:pageSlot>
		</c:if>
		
		 <c:if test="${not empty cartData.entries}">
			<cms:pageSlot position="CenterRightContentSlot" var="feature">
				<cms:component component="${feature}"/>
			</cms:pageSlot>
		</c:if>
      </div>
	</div>
   
    
    <c:if test="${empty cartData.entries}">
            <cms:pageSlot position="EmptyCartMiddleContent" var="feature" element="div">
                <cms:component component="${feature}"/>
            </cms:pageSlot>
    </c:if>
</template:page>