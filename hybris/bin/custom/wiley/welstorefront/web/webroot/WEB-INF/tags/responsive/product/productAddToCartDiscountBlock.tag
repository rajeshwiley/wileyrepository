<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData"%>
<%@ attribute name="productTemplate" required="false"%>
<%@ attribute name="formIndex" required="false"%>

<!-- Rendering of student discount -->
<c:if test="${not empty product.studentDiscount}">

	<c:set var="studentDiscount" value="${product.studentDiscount}" />
	<c:if test="${not studentDiscount.absolute}">
		<fmt:formatNumber var="studentDiscountPercent" type="percent" value="${studentDiscount.relativeValue/100}" />
	</c:if>
	<spring:theme var="studentDiscountText" code="product.student-discount.text"
		arguments="${studentDiscount.absolute?studentDiscount.price.shortFormattedValue:studentDiscountPercent}" argumentSeparator=";" />

	<c:choose>
		<c:when test="${productTemplate == 'cfa'}">
			<input type="checkbox" id="student-discount-${formIndex}" name="student-discount" />
			<label for="student-discount-${formIndex}">
				<spring:theme var="studentDiscountCFATooltip" code="product.student-discount.tooltip" arguments="${studentDiscount.absolute?studentDiscount.price.shortFormattedValue:studentDiscountPercent}" argumentSeparator=";" />
				<a class="tooltip-text">
					<c:out value="${studentDiscountText}" />
					<span class="tooltip-text-element" data-toggle="popover" data-content='${studentDiscountCFATooltip}'></span>
				</a>
			</label>
		</c:when>
		<c:otherwise>
			<div class="product-discount">
				<spring:theme var="studentDiscountTooltip" code="product.student-discount.tooltip"
					arguments="${studentDiscount.absolute?studentDiscount.price.shortFormattedValue:studentDiscountPercent}" argumentSeparator=";" />
				<input type="checkbox" id="student-discount-${formIndex}" name="student-discount" /> <label for="student-discount-${formIndex}"><c:out value="${studentDiscountText}" /> &nbsp;<!--
                --> <span class="tooltipHelp" data-toggle="popover" data-content='${studentDiscountTooltip}'>?</span> </label>
			</div>
		</c:otherwise>
	</c:choose>

</c:if>
