<%@ attribute name="countryIso" required="true" type="java.lang.String"%>
<%@ attribute name="countries" required="true" type="java.util.List"%>
<%@ attribute name="regions" required="false" type="java.util.List"%>
<%@ attribute name="universities" required="false" type="java.util.List"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>



<div id="countrySelector" class="form-cell">
    <formElement:formSelectBox idKey="country" labelCSS="form-label" isRequred="false" adjustToTwoColLayout="true"
        labelKey="" path="countryIso" mandatory="true" skipBlank="false"
        skipBlankMessageKey="checkout.multi.studentVerification.select.country" items="${countries}" itemValue="isocode" />
</div>
<c:choose>
    <c:when test="${not empty countryIso and countryIso == isocodeUSA}">
        <div class="form-cell">
            <formElement:formSelectBox idKey="region" labelCSS="form-label" isRequred="false"
                adjustToTwoColLayout="true" labelKey="" path="regionIso" mandatory="true" skipBlank="false"
                skipBlankMessageKey="checkout.multi.studentVerification.select.state" items="${regions}" itemValue="isocode" />
        </div>
        <div class="form-cell">
            <formElement:formSelectBox idKey="university" labelCSS="form-label" isRequred="false"
                adjustToTwoColLayout="true" labelKey="" path="universityCode" mandatory="true" skipBlank="false"
                skipBlankMessageKey="checkout.multi.studentVerification.select.college" items="${universities}" itemValue="code" />
        </div>
    </c:when>
    <c:otherwise>
        <c:if test="${not empty countryIso}">
            <div class="form-cell">
                <formElement:formInputBox idKey="universityText" labelCSS="form-label" isRequred="false"
                    adjustToTwoColLayout="true" labelKey="" path="universityTextField" mandatory="true" />
            </div>
        </c:if>
    </c:otherwise>
</c:choose>
