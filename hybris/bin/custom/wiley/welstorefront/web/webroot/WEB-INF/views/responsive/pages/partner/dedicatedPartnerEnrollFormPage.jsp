<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/mobile/common"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>

<div class="col-md-12">
    <h3><spring:theme code="kpmg.enroll.priceToAuthorizedEmployees"/></h3>
    <div class="kpmg-enroll">
		<form:form method="post" commandName="kpmgEnrollmentForm">
            <input type="hidden" name="productVariantCode" id="productVariantCodeInput" value=""/>
			<div class="site-form">

				<div class="form-cell">
					<div class="form-label label-bold"><spring:theme code="kpmg.enroll.registrationId.label1"/></div>
					<formElement:formInputBox idKey="registrationId" labelKey="kpmg.enroll.registrationId.label2" path="registrationId" type="text" 
						labelCSS="form-label" tabindex="1"/>
				</div>
				<div class="form-cell">
					<div class="form-label label-bold"><spring:theme code="kpmg.enroll.employeeId.label1"/></div>
					<formElement:formInputBox idKey="employeeId" labelKey="kpmg.enroll.employeeId.label2" path="employeeId" type="text" 
						labelCSS="form-label" tabindex="2"/>
				</div>
				<div class="form-cell">
					<formElement:formInputBox idKey="firstName" labelKey="kpmg.enroll.firstName" path="firstName" type="text" 
						labelCSS="form-label label-bold" isRequred="true" tabindex="3"/>
				</div>
				<div class="form-cell">
					<formElement:formInputBox idKey="lastName" labelKey="kpmg.enroll.lastName" path="lastName" type="text" 
						labelCSS="form-label label-bold" isRequred="true" tabindex="4"/>
				</div>
				<div class="form-cell">
					<div class="form-label label-bold required"><spring:theme code="kpmg.enroll.email.label1"/></div>
					<formElement:formInputBox idKey="email" labelKey="kpmg.enroll.email.label2" path="email" type="text" 
						labelCSS="form-label" tabindex="5"/>
				</div>
				<div class="form-cell">
					<formElement:formSelectBox idKey="function" labelKey="kpmg.enroll.function" path="function" skipBlank="true" items="${functions}"/>
				</div>
				<div class="form-cell">
					<formElement:formSelectBox idKey="office" labelKey="kpmg.enroll.office" path="office" skipBlank="true" items="${offices}"/>
				</div>
				<div class="form-buttons">
					<span class="btn-control">
						<button type="submit" class="button form-button"><spring:theme code="kpmg.enroll.submitbutton" /></button>
					</span>
				</div>
			</div>
		</form:form>
    </div>
</div>

