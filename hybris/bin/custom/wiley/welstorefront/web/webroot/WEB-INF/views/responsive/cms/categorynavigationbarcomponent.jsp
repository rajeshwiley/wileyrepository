<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/responsive/common/header"  %>

<c:set value="${component.styleClass} ${dropDownLayout}" var="bannerClasses"/>

<li class="${bannerClasses}">

	<cms:component component="${component.link}" evaluateRestriction="true"/>
	
	<header:categoryMenuNode navigationNode="${component.navigationNode}"/>

</li>
