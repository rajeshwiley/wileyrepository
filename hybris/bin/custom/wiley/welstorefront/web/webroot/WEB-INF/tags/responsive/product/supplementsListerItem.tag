<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="product" required="true"
	type="de.hybris.platform.commercefacades.product.data.ProductData"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:url value="${product.url}" var="productUrl" />
<spring:theme code="product.list.wileys" var="wileys" />

<c:set var="product" value="${product}" scope="request" />
	<div class="product-brief-item col-md-4 col-sm-4 col-xs-12 ">
		<a href="${productUrl}" title="${product.name}"> 
		<product:productPrimaryImage product="${product}" format="product" />
		</a>
		<h3 class="product-brief-item-name">
			<ycommerce:testId code="searchPage_productName_link_${product.code}">
				<a href="${productUrl}">${wileys}${product.name}</a>
			</ycommerce:testId>
		</h3>
		<div class="product-brief-item-description">
			<c:if test="${not empty product.summary}">
			        ${product.summary}
		    </c:if>
		</div>
    
		<div class="product-brief-item-buttons">
            <c:if test="${not empty product.freeTrialUrl}">
                <div class="row two-btn">
                    <div class="col-xs-12 col-md-6 col-sm-6">
                        <span class="product-brief-item-price">
                            <c:if test="${product.price.priceType eq 'FROM'}">
                                <spring:theme code="product.list.price.startingAt"/>
                            </c:if>
                            &nbsp;<format:price priceData="${product.price}" /></span>
                    </div>
                    <div class="col-xs-12 col-md-6 col-sm-6">
                        <a href="<c:url value='${product.freeTrialUrl}'/>" class="button button-default form-button"><spring:theme code="product.list.freetrial.button.label"/></a>
                        <a href="${productUrl}" class="button button-secondary form-button">Learn More</a>
                    </div>
                </div>
            </c:if>
            <c:if test="${ empty product.freeTrialUrl}">
                    <span class="product-brief-item-price">
                        <c:if test="${product.price.priceType eq 'FROM'}">
                            <spring:theme code="product.list.price.startingAt"/>
                        </c:if>
                        &nbsp;<format:price priceData="${product.price}" />
                    </span>
                    <a href="${productUrl}" class="button button-secondary form-button">Learn More</a>
            </c:if>
		</div>
	</div>