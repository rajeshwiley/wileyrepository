<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>


<c:if test="${not empty component.link}">
	<li class="${(component.link.url eq '#')?'not-link':''}">
		
		<cms:component component="${component.link}" evaluateRestriction="true"/>

		<c:if test="${not empty component.navigationNode.links}">
			<ul>
				<c:forEach items="${component.navigationNode.links}" var="link">
					<li ${(link.url eq '#')?'class="not-link"':''}>
						<cms:component component="${link}" evaluateRestriction="true"/>
					</li>
				</c:forEach>
			</ul>
		</c:if>
	</li>
</c:if>