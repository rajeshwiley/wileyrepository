<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="hideHeaderLinks" required="false"%>
<%@ attribute name="hideCustomLinks" required="false"%>
<%@ attribute name="hideActivatePinLink" required="false"%>
<%@ attribute name="hideCartLink" required="false"%>
<%@ attribute name="hideLoginLink" required="false"%>
<%@ attribute name="showLoginLinkOnMobile" required="false"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>

<cms:pageSlot position="TopHeaderSlot" var="component" element="div" class="container">
	<cms:component component="${component}" />
</cms:pageSlot>

<header id="main-header">
    <div class="site-header">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <cms:pageSlot position="SiteLogo" var="component" element="div" class="logo">
                        <cms:component component="${component}" />
                    </cms:pageSlot>
                </div>
                <c:url value="/cart" var="cartUrl"/>
                <div class="col-md-9 col-sm-9 col-xs-6">
                    <ul class="site-menu nav navbar-nav navbar-right">

                        <c:if test="${!hideCustomLinks}" >
                            <nav:topNavigation />
                        </c:if>

                        <%-- This slot is not visible in CMS Cockpit --%>
                        <cms:pageSlot position="LiveChat" var="component">
                            <cms:component component="${component}"/>
                        </cms:pageSlot>

						<c:if test="${!hideActivatePinLink}">
                            <li><a href="<spring:url value="/pin/activate"/>"><spring:theme code="text.header.activatePin"/></a></li>
                        </c:if>
                        <c:if test="${!hideCartLink}" >
                            <li><a class="cart" href="${cartUrl}" title="Cart"><spring:theme code="text.header.cart"/></a></li>
                        </c:if>
                        <li>
                            <cms:pageSlot position="CurrencySelector" var="component" >
                                <cms:component component="${component}" />
                            </cms:pageSlot>
                        </li>
                        <c:if test="${!hideLoginLink}">
                            <li class="${showLoginLinkOnMobile ? 'menu-item-mobile' : ''} menu-item-bordered">
                                <sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
                                    <a href="<spring:url value="/login"/>"><spring:theme code="text.header.login"/></a>
                                </sec:authorize>
                                <sec:authorize access="!hasRole('ROLE_ANONYMOUS')">
                                    <a href="<spring:url value="/my-account"/>"><spring:theme code="text.header.myAccount"/></a>
                                    <ul>
                                        <li><a href="<spring:url value="/logout"/>" title="Sign Out"><spring:theme code="text.header.signOut"/></a></li>
                                    </ul>
                                </sec:authorize>
                            </li>
                        </c:if>
                    </ul>
                </div>
            </div>
        </div>
    </div>

	<cms:pageSlot position="CategoryNavigation" var="component">
		<cms:component component="${component}"/>
	</cms:pageSlot>
</header>

<cms:pageSlot position="BottomHeaderSlot" var="component" element="div"	class="container">
	<cms:component component="${component}" />
</cms:pageSlot>

