<%@ page trimDirectiveWhitespaces="true" pageEncoding="utf-8" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>

<div class="whats-included">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-sm-8 col-xs-12">
          <product:productImagePanel galleryType="slider" galleryImages="${galleryImages}" product="${product}" />
        </div>
        <div class="col-md-6 col-sm-4 col-xs-12">
          <h4>See what you’ll get from our course</h4>
          <product:productIncludesSection product="${product}" useOneColumnOnly="true"/>
        </div>
      </div>
    </div>
  </div>