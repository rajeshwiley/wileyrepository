<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>

<div class="plan"><img src="${themeResourcePath}/images/bgimage_211.jpg" alt="" class="img-block">
        <div class="container">
          <div class="row">
            <div class="col-sm-6 col-md-6">
              <h3><spring:theme code="pdp.cfa.examPlanner.title"/></h3>
              <p>
                <spring:theme code="pdp.cfa.examPlanner.description"/>
              </p>
            </div>
          </div>
        </div>
      </div>
      
   <div class="product-tour">
        <div class="container">
          <div class="row">
            <div class="col-sm-4 col-md-4">
              <h4><spring:theme code="pdp.cfa.examPlanner.prepare.study.process"/></h4>
              <ul>
                <spring:theme code="pdp.cfa.examPlanner.prepare.study.process.list"/>
              </ul>
            </div>
            
            <div class="col-sm-4 col-md-4">
              <h4><spring:theme code="pdp.cfa.examPlanner.study.material"/></h4>
              <ul>
                <spring:theme code="pdp.cfa.examPlanner.study.material.list"/>
             </ul>
              <a href="#" class="show-hide"><spring:theme code="pdp.cfa.examPlanner.show.more"/></a>
            </div>
            
            <div class="col-sm-4 col-md-4">
              <h4><spring:theme code="pdp.cfa.examPlanner.review.for.test"/></h4>
              <ul>
                <spring:theme code="pdp.cfa.examPlanner.review.for.test.list"/>
              </ul><a href="#" class="show-hide"><spring:theme code="pdp.cfa.examPlanner.show.more"/></a>
            </div>
          </div>
        </div>
      </div> 
      
      <product:productAddToCartForm product="${product}" productTemplate="cfa" formIndex="1"/>
      
