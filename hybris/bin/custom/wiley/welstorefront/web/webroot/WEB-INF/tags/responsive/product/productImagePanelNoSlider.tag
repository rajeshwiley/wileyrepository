<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ attribute name="galleryImages" required="true" type="java.util.List" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="commonsProduct" tagdir="/WEB-INF/tags/addons/wileystorefrontcommons/responsive/product" %>

<div class="carousel gallery-image js-gallery-image">
	<c:forEach items="${galleryImages}" var="container" varStatus="varStatus">
		<div class="item">
			<div class="thumb">
				<c:choose>
					<c:when test="${not empty container.brightCoveVideo}">
						<commonsProduct:brightCoveVideo video="${container.brightCoveVideo}"/>
					</c:when>
					<c:otherwise>
						<img class="lazyOwl" data-src="${container.product.url}"
							 data-zoom-image="${container.superZoom.url}"
							 alt="${container.thumbnail.altText}" >
					</c:otherwise>
				</c:choose>
			</div>
		</div>
	</c:forEach>
</div>
<c:if test="${galleryImages != null && galleryImages.size() > 1}">
	<product:productGalleryThumbnail galleryImages="${galleryImages}" />
</c:if>