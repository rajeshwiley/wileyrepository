<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<c:set var="tableData" value="${wileyProductListCFAData.compareTableData}" scope="request" />
<div class="comparison-table comparison-table-mobile">
   <c:forEach items="${tableData.header}" var="product" varStatus="status">
      <div class="course-component">
         <div class="course-component-header">
            <div class="course-component-title">
               <c:if test="${status.index == 0}">
                  <span class="most-popular-label">
                     <spring:theme code="plp.cfa.mostPopular"/>
                  </span>
               </c:if>
               ${fn:replace(product.value,' ', '<br>')}
            </div>
            <div class="learn-more-links">
               <p class="title">
                  <spring:theme code="plp.cfa.learnMore"/>
               </p>
               <c:forEach items="${tableData.footerProductData}" var="entry">
                  <div class="links">
                     <c:if test="${entry.value[product.key] != null}">
                        <div>${entry.key}</div>
                        <div class="footer-level-links">
                           <c:forEach items="${entry.value[product.key]}" var="levelUrlEntry">
                              <c:url value="${levelUrlEntry.url}" var="url"></c:url>
                              <div class="link-wrapper level-url"><a href="${url}" class="link">${fn:toUpperCase(levelUrlEntry.level)}</a></div>
                           </c:forEach>
                        </div>
                     </c:if>
                  </div>
               </c:forEach>
            </div>
         </div>
         <div class="course-component-content">
            <c:forEach items="${tableData.levels}" var="level">
               <div class="course-component-price">
                  <c:choose>
                     <c:when test="${not empty level.pricesData[product.key]}">
                        <div class="price-value">
                           <c:set var="price" value="${level.pricesData[product.key]}"/>
                           ${price.shortFormattedValue}
                        </div>
                     </c:when>
                     <c:otherwise>
                        <span class="not-applicable">
                           <spring:theme code="plp.cfa.table.na"/>
                        </span>
                     </c:otherwise>
                  </c:choose>
                  <div class="price-name">${fn:toUpperCase(level.rowTitle)}</div>
               </div>
            </c:forEach>
            <ul class="course-component-products">
               <c:forEach items="${tableData.compareOptions}" var="feature">
                  <c:if test="${not empty feature.value.featureAvailability[product.key]}">
                     <li class="course-component-product">
                        <span class="check-mark"></span>
                        ${feature.value.featureName}
                     </li>
                  </c:if>
               </c:forEach>
            </ul>
         </div>
         <div class="course-component-footer">
            <div class="learn-more-links">
               <p class="title">
                  <spring:theme code="plp.cfa.learnMore"/>
               </p>
               <c:forEach items="${tableData.footerProductData}" var="entry">
                  <div class="links">
                     <c:if test="${entry.value[product.key] != null}">
                        <div>${entry.key}</div>
                        <div class="footer-level-links">
                           <c:forEach items="${entry.value[product.key]}" var="levelUrlEntry">
                              <c:url value="${levelUrlEntry.url}" var="url"></c:url>
                              <div class="link-wrapper level-url"><a href="${url}" class="link">${fn:toUpperCase(levelUrlEntry.level)}</a></div>
                           </c:forEach>
                        </div>
                     </c:if>
                  </div>
               </c:forEach>
            </div>
         </div>
      </div>
   </c:forEach>
</div>
<div class="comparison-table-cfa comparison-table-cfa-desktop">
   <table>
      <thead>
         <tr class="cfa-thead-image">
            <th style="background: #ffffff;"/>
               <c:forEach items="${tableData.header}" var="product" varStatus="status">
            <th>
            <c:if test="${status.index eq 0}">
            <span class="most-popular-label">
            <spring:theme code="product.list.table.most.popular" />
            </span>
            </c:if>
            <product:productPrimaryImage product="${tableData.headerProductData[product.key]}" format="thumbnail" />
            </th>
            </c:forEach>
         </tr>
         <tr>
            <th/>
               <c:forEach items="${tableData.header}" var="product">
            <th>${product.value}</th>
            </c:forEach>
         </tr>
      </thead>
      <tbody>
         <!-- Levels price section -->
         <c:forEach items="${tableData.levels}" var="level">
            <tr class="level-row">
               <td class="cell-right-text" ><span class="price-name">${level.rowTitle}</span></td>
               <c:forEach items="${tableData.header}" var="product">
                  <c:choose>
                     <c:when test="${not empty level.pricesData[product.key]}">
                        <td>
                           <span class="price-value">
                              <c:set var="price" value="${level.pricesData[product.key]}"/>
                              ${price.shortFormattedValue}
                           </span>
                        </td>
                     </c:when>
                     <c:otherwise>
                        <td>
                           <span class="not-applicable">
                              <spring:theme code="plp.cfa.table.na"/>
                           </span>
                        </td>
                     </c:otherwise>
                  </c:choose>
               </c:forEach>
            </tr>
         </c:forEach>
         <!-- Compare oprions section -->
         <c:forEach items="${tableData.compareOptions}" var="feature">
            <tr>
               <td>${feature.value.featureName}</td>
               <c:forEach items="${tableData.header}" var="product">
                  <td>
                     <c:if test="${not empty feature.value.featureAvailability[product.key]}">
                        <span class="check-mark"></span>
                     </c:if>
                  </td>
               </c:forEach>
            </tr>
         </c:forEach>
      </tbody>
      <tfoot>
         <c:forEach items="${tableData.footerProductData}" var="entry">
            <tr class="level-link">
               <td class="cell-level cell-right-text">${entry.key}</td>
               <c:forEach items="${tableData.header}" var="product" varStatus="status">
                  <td>
                     <c:if test="${entry.value[product.key] ne null}">
                        <div>
                           <c:forEach items="${entry.value[product.key]}" var="levelUrlEntry">
                              <c:url value="${levelUrlEntry.url}" var="url"></c:url>
                              <a class="cell-level learn-more" href="${url}">${fn:toUpperCase(levelUrlEntry.level)}</a>
                           </c:forEach>
                        </div>
                     </c:if>
                  </td>
               </c:forEach>
            </tr>
         </c:forEach>
      </tfoot>
   </table>
</div>