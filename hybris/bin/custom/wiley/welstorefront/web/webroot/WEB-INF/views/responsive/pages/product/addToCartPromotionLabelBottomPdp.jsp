<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>

<div class="addtocart-bottom-component">
    <product:productAddToCartBottomPdp product="${product}" formIndex="1" />
</div>
