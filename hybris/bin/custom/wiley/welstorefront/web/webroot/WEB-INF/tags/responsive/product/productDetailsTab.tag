<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="product" required="true"
	type="de.hybris.platform.commercefacades.product.data.ProductData"%>

<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>

<div class="tab-details">
	<ycommerce:testId code="productDetails_content_label">

		<%-- TODO: Tab content with left block image
		Anton Sharapa --%>
		<div class="tab-item">
			<img class="img" src="/welstorefront/_ui/responsive/theme-blue/images/missing_product_300x300.jpg">
			<div class="tab-item-content">
				${product.description}
			</div>
		</div>

		<%-- TODO: Tab content with right block image
		Anton Sharapa --%>
		<div class="tab-item">
			<img class="img rightSide" src="/welstorefront/_ui/responsive/theme-blue/images/missing_product_300x300.jpg">

			<div class="tab-item-content">
				${product.description}
			</div>
		</div>
	</ycommerce:testId>
</div>