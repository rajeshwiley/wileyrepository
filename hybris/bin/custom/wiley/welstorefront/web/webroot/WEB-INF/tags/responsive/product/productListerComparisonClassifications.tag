<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ attribute name="feature" required="true" type="de.hybris.platform.commercefacades.product.data.FeatureData" %>
<%@ attribute name="featureName" required="false" type="java.lang.String" %>
<%@ attribute name="mobile" required="false" type="java.lang.Boolean" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${not empty product.includeFeatures}">
	<c:forEach items="${product.includeFeatures}" var="value">
		<c:if test="${feature.name eq value.name}">
			<c:choose>
				<c:when test="${mobile}">
					<li class="course-component-product">
						<span class="check-mark"></span>
						${featureName}
					</li>
				</c:when>
				<c:otherwise>
					<span class="check-mark"></span>
				</c:otherwise>
			</c:choose>
		</c:if>
	</c:forEach> 
</c:if>