<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="orderData" required="true" type="de.hybris.platform.commercefacades.order.data.OrderData" %>
<%@ attribute name="showTax" required="false" type="java.lang.Boolean" %>
<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>
<%@ taglib prefix="multi" tagdir="/WEB-INF/tags/addons/b2ccheckoutaddon/responsive/checkout/multi" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="format-common" tagdir="/WEB-INF/tags/addons/welagsstorefrontcommons/shared/format"%>
<%@ taglib prefix="wileycommerce" uri="/WEB-INF/tld/addons/welagsstorefrontcommons/wileycommercetags.tld" %>

<template:page pageTitle="${pageTitle}" hideHeaderLinks="false" containerCSSClass="container confirmation-page" showLoginLinkOnMobile="false">

		<cms:pageSlot position="CheckoutBreadcrumbs" var="feature">
         <cms:component component="${feature}" />
        </cms:pageSlot>

		<div class="row-mobile">
			<cms:pageSlot position="TopContent" var="feature" element="div">
				<cms:component component="${feature}"/>
			</cms:pageSlot>
		</div>
		<div class="row-mobile">
			<cms:pageSlot position="SideContent" var="feature" element="div">
				    <cms:component component="${feature}"/>
			</cms:pageSlot>
		</div>
		<div class="table-order">
			<div class="table-order-row">
			  <div class="table-order-head"><spring:theme code="text.account.order.orderDetails.orderNumber" arguments="${orderData.code}"/>
				&nbsp;<format-common:formattedDate date="${orderData.created}"/>
			  </div>
			  <div class="table-order-head"><spring:theme code="checkout.orderConfirmation.price"/></div>
			  <div class="table-order-head"><spring:theme code="checkout.orderConfirmation.quantity"/></div>
			  <div class="table-order-head"><spring:theme code="checkout.orderConfirmation.total"/></div>
			</div>


			<c:forEach items="${orderData.entries}" var="entry">
				<c:url value="${entry.product.url}" var="productUrl" />
				<div class="table-order-row subscription-row">
				  <div class="table-order-cell">
					  <div class="subscription-image">
						  <c:forEach items="${entry.product.images}" var="image">
							  <c:if test="${image.format eq 'cartIcon'}">
								  <img src="${image.url}" alt="${image.altText}"/>
							  </c:if>
						  </c:forEach>
						   <c:if test="${isPreOrder}">
                              <spring:theme code="checkout.PreOrder.text.message"/>
                           </c:if>
					  </div>
					  <h3 class="subscription-title">
						  ${entry.product.name}
					  </h3>
					  <div class="subscription-description">
						  <ul>
							  <li>${entry.product.description}</li>
						  </ul>
					  </div>
				  </div>
				  <div class="table-order-cell"><format:price priceData="${entry.basePrice}" displayFreeForZero="true"/></div>
				  <div class="table-order-cell">${entry.quantity}</div>
				  <div class="table-order-cell"><format:price priceData="${entry.totalPrice}" displayFreeForZero="true"/></div>
				</div>
				<div class="table-order-row-mobile">
				  <div class="table-order-row">
						<div class="table-order-cell"><spring:theme code="checkout.orderConfirmation.price.mobile"/>&nbsp;<format:price priceData="${entry.basePrice}" displayFreeForZero="true"/></div>
				  </div>
				  <div class="table-order-row">
					<div class="table-order-cell"><spring:theme code="checkout.orderConfirmation.quantity.mobile" arguments="${entry.quantity}"/></div>
				  </div>
				  <div class="table-order-row">
					<div class="table-order-cell"><spring:theme code="checkout.orderConfirmation.total.mobile"/>&nbsp;<format:price priceData="${entry.totalPrice}" displayFreeForZero="true"/></div>
				  </div>
				</div>
			</c:forEach>

			<div class="table-order-row-group">
			  <div class="table-order-row">
				<div class="table-order-cell"></div>
				<div class="table-order-cell"></div>
				<div class="table-order-cell"><spring:theme code="checkout.orderConfirmation.subtotal"/></div>
				<div class="table-order-cell"><format:price priceData="${orderData.subTotalWithoutDiscount}" displayFreeForZero="true" /></div>
			  </div>
			  <c:if test="${orderData.totalDiscounts.value > 0}">
				  <div class="table-order-row">
					<div class="table-order-cell"></div>
					<div class="table-order-cell"></div>
					<div class="table-order-cell"><spring:theme code="checkout.orderConfirmation.discount"/></div>
					<div class="table-order-cell">&#45;<format:price priceData="${orderData.totalDiscounts}"/></div>
				  </div>
			  </c:if>
			  <c:if test="${orderData.taxAvailable}">
                  <div class="table-order-row">
                    <div class="table-order-cell"></div>
                    <div class="table-order-cell"></div>
                    <div class="table-order-cell"><spring:theme code="checkout.orderConfirmation.tax"/></div>
                    <div class="table-order-cell"><format:price priceData="${orderData.totalTax}"/></div>
                  </div>
              </c:if>
			  <c:if test="${not empty orderData.deliveryCost and orderData.showDeliveryCost}">
			  	<div class="table-order-row">
					<div class="table-order-cell"></div>
					<div class="table-order-cell"></div>
					<div class="table-order-cell"><spring:theme code="checkout.orderConfirmation.delivery"/></div>
					<div class="table-order-cell"><format:price priceData="${orderData.deliveryCost}" displayFreeForZero="true"/></div>
				</div>
			  </c:if>
			  <div class="table-order-row total-row">
				<div class="table-order-cell"></div>
				<div class="table-order-cell"></div>
				<div class="table-order-cell"><spring:theme code="checkout.orderConfirmation.total.mobile"/></div>
				<div class="table-order-cell"><format:price priceData="${orderData.totalPriceWithTax}"/></div>
			  </div>
			</div>
		</div>
		<div class="form-buttons">
		  <button type="submit" class="button form-button continueShoppingButton" data-continue-shopping-url="<c:url value="/my-account"/>"><spring:theme code="checkout.orderConfirmation.goToMyAccount"/></button>
		</div>
		<cms:pageSlot position="Disclaimer" var="feature">
			<cms:component component="${feature}" />
		</cms:pageSlot>
	</div>
</template:page>

