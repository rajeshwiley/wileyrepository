<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:if test="${not empty adobeAnalyticsScriptURL}">
    <script src="${adobeAnalyticsScriptURL}"></script>
</c:if>
