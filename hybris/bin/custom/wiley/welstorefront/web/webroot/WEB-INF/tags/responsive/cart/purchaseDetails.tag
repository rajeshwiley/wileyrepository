<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart"%>

<div class="subscriptions-container">
    <div class="subscriptions-header">
        <div class="subscriptions-header-item item-item">
            <spring:theme code="basket.page.item" />
        </div>
        <div class="subscriptions-header-item item-price">
            <spring:theme code="basket.page.price" />
        </div>
        <div class="subscriptions-header-item item-qty">
            <spring:theme code="product.grid.quantityText" />
        </div>
        <div class="subscriptions-header-item item-total">
            <spring:theme code="basket.page.total" />
        </div>
    </div>
    <div class="subscriptions-content">
        <c:forEach items="${cartData.entries}" var="entry">
            <c:remove var="productUrl"/>
            <c:if test="${! entry.product.partnerOnly}">
                <c:url value="${entry.product.url}" var="productUrl" />
            </c:if>

            <c:set var="isGiftCard" value="${entry.product.isGiftCard}"/>
            <div class="subscription">
                <div class="subscription-info">
					<cart:entryProductImage entry="${entry}" productUrl="${productUrl}"/>
                    <cart:entryProductTitle entry="${entry}" isGiftCard="${isGiftCard}" productUrl="${productUrl}"/>

                </div>

                <div class="subscription-price-item item-price">
                    <div class="subscription-price-name">
                        <spring:theme code="basket.page.price" />
                        :
                    </div>
                    <div class="subscription-price-value">
                        <format:price priceData="${entry.basePrice}" displayFreeForZero="true" />
                    </div>
                </div>
                <div class="subscription-price-item item-qty">
                    <div class="subscription-price-name">
                        <spring:theme code="basket.page.qty" />
                        :
                    </div>
                    <div class="subscription-price-value">
						<div class="quantity-field">
							${entry.quantity}
						</div>
                    </div>
                </div>
                <div class="subscription-price-item item-total">
                    <div class="subscription-price-name">
                        <spring:theme code="basket.page.total" />
                        :
                    </div>
                    <div class="subscription-price-value">
                        <format:price priceData="${entry.totalPrice}" displayFreeForZero="true" />
                    </div>
                </div>
            </div>
        </c:forEach>
    </div>
</div>

