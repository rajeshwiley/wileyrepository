<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ attribute name="formIndex" required="false" %>

<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:set var="currentProductCode" value="${product.code}"/>

<c:if test="${product.preOrderNodes != null && fn:length(product.preOrderNodes) > 0}">
    <div class="preorder-nodes">
        <c:forEach items="${product.preOrderNodes}" var="node" varStatus="loop">
            <c:set var="checked" value="${node.code eq currentProductCode ? 'checked' : ''}"/>

            <input type="radio" id="product-preorder-${node.code}-${formIndex}" name="${currentProductCode}" ${checked} class="js-current-product-type"
                data-url="${node.url}#product-preorder-${node.code}-${formIndex}" />
            <label for="product-preorder-${node.code}-${formIndex}">
                <c:choose>
                    <c:when test="${node.lifecycleStatus eq 'PRE_ORDER'}">
                        <spring:theme code="${node.preOrderText}" />
                        <span class="tooltipHelp" data-toggle="popover" data-content="${fn:escapeXml(node.description)}">?</span>
                    </c:when>
                    <c:otherwise>
                        <spring:theme code="pdp.radio.active.product" />
                    </c:otherwise>
                </c:choose>
            </label>
        </c:forEach>
    </div>
</c:if>