<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<div class="competitors bottom-shadow">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <h3>See why Wiley is better than the competitors</h3>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <table class="competitors-table">
                <thead>
                  <tr>
                    <th></th>
                    <th>Wiley</th>
                    <th>Others</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Price</td>             
	                <td>${product.price.formattedValue}</td>
                    <td>$1,399+</td>
                  </tr>
                  <tr>
                    <td>Course format</td>
                    <td>Self Study and/or Virtual Classes</td>
                    <td>Self-Study and/or Live Classes</td>
                  </tr>
                  <tr>
                    <td>Cost to repeat the course</td>
                    <td>Free until you pass, no conditions</td>
                    <td>50% extra per part retaken</td>
                  </tr>
                  <tr>
                    <td>Length of access time</td>
                    <td>Until you pass</td>
                    <td>Limited</td>
                  </tr>
                  <tr>
                    <td>Video lecture hours</td>
                    <td>110+</td>
                    <td>100</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <p class="notice">Our data is taken from the Kaplan<sup>TM &nbsp;</sup>website and other sources we believe to be reliable. We strive to be accurate.</p>
            </div>
          </div>
        </div>
      </div>
