<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/address" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:url var="editUrl" value="/checkout/multi/order-review/edit" />


<c:set var="billingAddress" value="${empty cartData.wileyPaymentInfo ? '' : cartData.wileyPaymentInfo.billingAddress}"/>
<c:set var="shippingAddress" value="${cartData.deliveryAddress}"/>

	<div class="review-container">
		<div class="review-header">
			<span><spring:theme code="checkout.orderReview.reviewYourOrderTitle"/></span>
			<span><a href="${editUrl}" class="go-to-card"><spring:theme code="checkout.multi.paymentMethod.edit"/></a></span>
		</div>
		<div class="review-details">
			<div class="row-item address-container">
				<c:if test="${not empty billingAddress}">
					<div class="cell-item">
						<strong>
							<spring:theme code="checkout.multi.paymentMethod.addPaymentDetails.billingAddress"/>
						</strong>
						<br>
						<address:addressItem address="${billingAddress}"/>
					</div>
				</c:if>
				<c:if test="${not empty shippingAddress}">
					<div class="cell-item">
						<strong>
							<spring:theme code="checkout.summary.shippingAddress"/>
						</strong>
						<br>
						<address:addressItem address="${shippingAddress}"/>
					</div>
				</c:if>
			</div>
			<div class="methodSection row-item">
				<div class="cell-item">
					<c:if test="${not empty cartData.wileyPaymentInfo && cartData.wileyPaymentInfo.paymentType ne 'ZERO_ORDER'}">
						<strong>
							<spring:theme code="checkout.orderReview.paymentMethodTitle"/>
						</strong>
						<br/>
						<c:choose>
							<c:when test="${cartData.wileyPaymentInfo.paymentType eq 'CARD'}">
								<spring:theme code="checkout.orderReview.cardPaymentMethodInfo" arguments="${fn:escapeXml(cartData.paymentInfo.cardTypeData.name)},${fn:escapeXml(cartData.paymentInfo.cardNumber)}"/>
							</c:when>
							<c:when test="${cartData.wileyPaymentInfo.paymentType eq 'PAYPAL'}">
								<spring:theme code="checkout.orderReview.paymentType.paypal"/>
							</c:when>
						</c:choose>
					</c:if>
				</div>
				<c:if test="${not empty cartData.deliveryMode}">
					<div class="cell-item">
						<strong>
							<spring:theme code="checkout.orderReview.shippingMethodTitle"/>
						</strong>
						<br/>
						${fn:escapeXml(cartData.deliveryMode.name)}
					</div>
				</c:if>
			</div>
		</div>
	</div>

