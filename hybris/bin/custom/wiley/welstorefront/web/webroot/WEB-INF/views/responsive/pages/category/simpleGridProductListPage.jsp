<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:if test="${showSupplementsTitle eq 'true' && categorySupplementsProducts.size() > 0}">
    <div class="content">
        <h2 id="supplements" class="comparison-product-title">
            <spring:theme code="level.product.list.supplements.title"/>
        </h2>
    </div>
</c:if>
<div class="container product-brief">
    <div class="container-fluid">
        <div class="row">
            <ul class="product-listing product-list">
                <c:forEach items="${categoryProducts}" var="product" varStatus="status">
                    <product:supplementsListerItem product="${product}" />
                </c:forEach>
            </ul>
        </div>
    </div>
</div>


