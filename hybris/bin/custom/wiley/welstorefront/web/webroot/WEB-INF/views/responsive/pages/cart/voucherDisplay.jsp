<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<div class="discount-code">
    <div class="discount-code-wrapper">
        <div class="discount-code-title">
            <spring:theme code="basket.discount.code" />
        </div>
        <spring:url value='/cart' var="voucherFormAction"/>
        <form:form class="discount-code-form" commandName="voucherForm" action="${voucherFormAction}" method="post">
            <div class="input-group">
                <div class="form-field">
                    <form:input path="discountCodeValue" cssClass="discount-code-input form-control" cssErrorClass="has-error"/>
                </div>
                <span class="input-group-btn">
                    <button type="submit" class="button form-button button-apply discount-code-apply">
                        <spring:theme code="basket.discount.apply" />
                    </button>
                </span>
            </div>
            <div class="discount-code-message">
                <c:set var="showMessage" value="true"/>
                <form:errors path="discountCodeValue" cssClass="discount-error has-error" element="div">
                    <c:set var="showMessage" value="false"/>
                </form:errors>
            </div>
            <div class="discount-code-message">
                <c:choose>
                    <c:when test="${showMessage eq true}">
                        <div>${voucherAppliedMessage}</div>
                    </c:when>
                </c:choose>
            </div>
            <cart:cartPromotions cartData="${cartData}"/>

            <cart:cartPotentialPromotions cartData="${cartData}"/>
           
        </form:form>
    </div>
</div>