
<div class="product-classifications">
    <div class="tab-item">
        <div class="headline">Core Study Materials</div>
        <img
            src="http://www.efficientlearning.com/wileycpaexcel/wp-content/uploads/sites/2/2014/10/product-features-core-study-materials.png"
            class="img">
        <div class="tab-item-content">
            <p>Pass the CPA Exam with confidence. With CPAexcel’s Core Study Materials you can study online or offline at
                your own pace. Your scores are synchronized after each study session so you can pick up where you left off each
                time. The Core Study Materials include:</p>
            <p>
                <strong>Preparing your Study Process:</strong>
            </p>
            <ul class="checkmarkList">
                <li>Exam Planner &amp; Personalized Study Schedule</li>
                <li>Diagnostic Exams &amp; Strategy Support</li>
                <li>Performance Metrics</li>
            </ul>
            <p>
                <strong>Studying the Material:</strong>
            </p>
            <ul class="checkmarkList">
                <li>Efficient Learning System with Bite-Sized Lessons</li>
                <li>2,600+ Pages of Electronic Study Text</li>
                <li>7,500 Past CPA Exam Questions with Answer Rationales</li>
                <li>5,500+ Proficiency Questions</li>
                <li>4,200+ Digital Flash Cards</li>
                <li>Student Discussions</li>
                <li>Free Software &amp; Content Updates</li>
            </ul>
            <p>
                <strong>Reviewing for the Test:</strong>
            </p>
            <ul class="checkmarkList"></ul>
            <li>450+ Task-Based Simulations</li>
            <li>Unlimited Simulated CPA Exams</li>
            <li>Final Review</li>

        </div>
    </div>
    <div class="tab-item">
        <div class="headline">Video Lectures + Slides</div>
        <img src="http://www.efficientlearning.com/wileycpaexcel/wp-content/uploads/sites/2/2015/01/deep-dive-videos-sm2.jpg"
            class="img right-side">
        <div class="tab-item-content">
            <p>
                Bring the classroom to your home with Video Lectures and Slides. We’ve broken down each lecture into Bite-Sized
                Lessons, and created accompanying slides so you can take notes while you watch. Over 110 hours of Video Lectures
                and 6,800+ slides are included in the 4-part course. View a<a href="https://www.youtube.com/watch?v=flsuXN1TA2A">Video
                    Lecture sample</a>
            </p>
            <p>Features &amp; Benefits:</p>
            <ul>
                <li>Ideal for Audio Visual Learners</li>
                <li>Learn from the Best – Wiley CPAexcel’s professors hail from the nation’s leading accounting
                    universities, including the University of Texas at Austin, California State University Sacramento, and
                    Northern Illinois University.</li>
                <li>Keeps Your Attention – Learn from a variety of ten different subject-matter experts.</li>
                <li>Stay Focused – The key concepts and principles are covered in every subject area, plus heavily weighted
                    exam topics and challenging subjects that often give students trouble during the CPA Exam.</li>
                <li>Enjoy the Convenience – No wasted time commuting to a classroom. Never worry about missed lectures.
                    Listen to any speaker or topic as many times as you need.</li>
                <li><strong>New for 2015!</strong>Over 60 individual deep dive lectures. Our leading accounting professors
                    dig in to the toughest problems on the CPA Exam and break down the solutions step by step.<a
                    href="https://www.youtube.com/watch?v=517XghnAFso">View a Deep Dive Lecture sample.</a></li>
            </ul>
        </div>
    </div>
    <div class="tab-item">
        <div class="headline">Professor Mentoring</div>
        <img src="http://www.efficientlearning.com/wileycpaexcel/wp-content/uploads/sites/2/2014/10/professor-mentoring.png"
            class="img">
        <div class="tab-item-content">
            <div class="ipass-right">Get the answers you need from experienced subject-expert professors. With our Professor
                Mentoring, you can post your Wiley CPAexcel related questions and get the information you need within 24 hours or
                less, get clarified lecture points and have them demonstrate how to solve problems. Need an immediate response?
                Just access the archived discussions to find what you’re looking for.</div>
            <div class="ipass-right"></div>
            <div class="ipass-right">
                Features &amp; Benefits:
                <ul>
                    <li>Answers in 24 hours or less – Post your Wiley CPAexcel related question, and get the correct answer
                        every time from experienced subject-expert Professors.</li>
                    <li>Archived discussions – Access prior archived discussions, so you can often find the answer that you
                        need immediately and see what types of questions your peers are asking.</li>
                    <li>Expert team of professor mentors- Every Wiley CPAexcel mentor is a distinguished university professor
                        and an expert in their field. With access to prompt answers from these professors, there is no question
                        you can’t answer.</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="tab-item">
        <div class="headline">Course Outlines</div>
        <img
            src="http://www.efficientlearning.com/wileycpaexcel/wp-content/uploads/sites/2/2014/10/product-features-course-outlines.png"
            class="img right-side">
        <div class="tab-item-content">
            <p>With Wiley CPAexcel, you choose how you want to learn. Get your Course Outlines in printed textbook format or
                eBook format on your computer or mobile device. Both are identical, map perfectly to our courseware outline and
                structure, and feature Bite-Sized Lessons.</p>
            <p>Features &amp; Benefits:</p>
            <ul>
                <li>Separate course outlines(s) for each exam section covering the complete scope of the AICPA content
                    specification.</li>
                <li>Course outlines map perfectly to our courseware outline and structure. No cross-referencing is required.</li>
                <li>The content of each course outline is identical to our courseware at the date of printing. Later content
                    updates are included in our courseware. The courseware logs all updates and identifies them to the affected
                    lessons. Thus each course outline user may see which lessons have been updated and, if desired, print a copy
                    of the updated version to use in place of the version in the textbook.</li>
                <li>At the front of each course outline is advice from the author concerning the emphasis of the questions on
                    the CPA Exam, as a guide to students completing their studies.</li>
                <li>Each course outlines is a series of Bite-Sized Lessons. Each lesson usually consists of 3 to 10 pages of
                    study material.</li>
            </ul>
        </div>
    </div>
    <div class="tab-item">
        <div class="headline">Virtual Classroom Series</div>
        <img src="http://www.efficientlearning.com/wileycpaexcel/wp-content/uploads/sites/2/2014/10/virtual-classroom-series.png"
            class="img">
        <div class="tab-item-content">
            <p>
                A blend of pre-recorded lectures and live online classes led by instructors who discuss the most frequently missed
                questions in CPAexcel. Students have the opportunity to ask questions of live instructors and access archived
                lectures.<a
                    href="/wileycpaexcel/wp-content/uploads/sites/2/2015/09/Fall2015-Wiley-CPAexcel-Virtual-Classroom-Schedule.pdf"
                    target="_blank" rel="shadowbox"> See upcoming class dates!</a>
            </p>
            <p>Features &amp; Benefits:</p>
            <ul>
                <li>Live Instruction &amp; Support</li>
                <li>Connect with classmates</li>
                <li>Integrate with Your “CPAexcel Approved” Exam Plan</li>
                <li>Mobile-ready</li>
                <li>Access lecture archives</li>
            </ul>
        </div>
    </div>
    <div class="tab-item">
        <div class="headline">CPA Online Test Bank</div>
        <img src="http://www.efficientlearning.com/wileycpaexcel/wp-content/uploads/sites/2/2014/10/cpa-online-test-bank.png"
            class="img right-side">
        <div class="tab-item-content">
            <p>Boost your review with the ultimate online practice tool. Get access to the most popular supplement in
                CPA-land: 4,400+ practice questions and 148 simulations. We’ve improved our Test Bank with added customizations,
                reports and features. Everything you love and more</p>
            <p>Features &amp; Benefits:</p>
            <ul>
                <li>4,400+ multiple choice questions with detailed answers, Study Guide references and hints</li>
                <li>148 Task-based Simulations</li>
                <li>Interface replicates the Prometric experience</li>
                <li>Tracks your strengths &amp; weaknesses</li>
                <li>Forms &amp; formats for the most current exam</li>
                <li><em>Partner Until You Pass Guarantee</em>– free updates and access until you pass the exam</li>
            </ul>
            <p>With the CPA Online Test Bank included with your CPA Platinum Review Course, you now have access to more than
                12,000 practice questions and 600+ task-based simulations–the most of any CPA Review Course on the market!</p>
        </div>
    </div>
    <div class="tab-item">
        <div class="headline">Flash Cards</div>
        <img
            src="http://www.efficientlearning.com/wileycpaexcel/wp-content/uploads/sites/2/2014/10/product-features-flashcards.png"
            class="img">
        <div class="tab-item-content">
            <p>Use Flash Cards to reinforce and improve knowledge retention in all four sections. Order the complete 4-parts
                (1,000 cards) or individual sections (250 cards/section).</p>
            <p>Features &amp; Benefits:</p>
            <ul>
                <li>250 flash cards per section</li>
                <li>Covers the 250 most critical points to know on the CPA Exam for each section</li>
            </ul>
        </div>
    </div>
    <div class="tab-item">
        <div class="headline">Focus Notes</div>
        <img
            src="http://www.efficientlearning.com/wileycpaexcel/wp-content/uploads/sites/2/2014/10/product-features-focus-notes.png"
            class="img right-side">
        <div class="tab-item-content">
            <p>Reinforce key concepts for every section of the CPA Exam with these easy-to-read and carry spiral-bound
                supplements. Learn acronyms and mnemonic devices to help you remember accounting rules, checklists and more.</p>
            <p>Features &amp; Benefits:</p>
            <ul>
                <li>Mnemonics</li>
                <li>Formulas and calculations</li>
                <li>Key concepts</li>
                <li>Exam tips</li>
            </ul>
        </div>
    </div>
</div>