<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="idKey" required="true" type="java.lang.String"%>
<%@ attribute name="labelKey" required="true" type="java.lang.String"%>
<%@ attribute name="path" required="true" type="java.lang.String"%>
<%@ attribute name="mandatory" required="false" type="java.lang.Boolean"%>
<%@ attribute name="labelCSS" required="false" type="java.lang.String"%>
<%@ attribute name="inputCSS" required="false" type="java.lang.String"%>
<%@ attribute name="type" required="false" type="java.lang.String"%>
<%@ attribute name="isRequred" required="false" type="java.lang.Boolean"%>
<%@ attribute name="readonly" required="false" type="java.lang.Boolean"%>
<%@ attribute name="adjustToTwoColLayout" required="false" type="java.lang.Boolean"%>
<%@ attribute name="placeholder" required="false" type="java.lang.String"%>
<%@ attribute name="tabindex" required="false" rtexprvalue="true"%>
<%@ attribute name="autocomplete" required="false" type="java.lang.String"%>
<%@ attribute name="value" required="false" type="java.lang.String"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>

<c:if test="${empty type}" >
    <c:set var="type" value="text" />
</c:if>

<template:errorSpanField path="${path}" adjustToTwoColLayout="${adjustToTwoColLayout}">
	<ycommerce:testId code="LoginPage_Item_${idKey}">
		<label class="control-label ${labelCSS}${isRequred?' required':''}" for="${idKey}">
			<spring:theme code="${labelKey}" />
		</label>

		<spring:theme code="${placeholder}" var="placeHolderMessage" />
		<div class="form-field">
			<form:input cssClass="${inputCSS}" id="${idKey}" path="${path}" type="${type}"
					tabindex="${tabindex}" autocomplete="${autocomplete}" placeholder="${placeHolderMessage}" value="${value}" readonly="${readonly}"/>
		</div>
	</ycommerce:testId>
</template:errorSpanField>
