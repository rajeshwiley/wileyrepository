<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="checkout" tagdir="/WEB-INF/tags/responsive/checkout" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/address"%>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>



                <form:hidden path="billingOnly" />
                <div id="billingShippingAddress">
                    <div class="col-xs-12 col-sm-6 col-md-4 panel">
                    <div role="tab" id="headingTwo" class="panel-heading">
                        <div class="form-title"><spring:theme code="checkout.multi.paymentMethod.addPaymentDetails.billingAddress"/>
                           <a role="button" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo" class="form-title-toggle" data-toggle="collapse"></a>
                        </div>
                    </div>
                    <div id="collapseTwo" role="tabpanel" aria-labelledby="headingTwo" class="panel-collapse collapse in">
                          <div class="site-form">
                                <address:addressFormElements subpath="${billingFormPrefix}" formKey="${wileyBillShippAddressForm.billingAddress}" 
                                    countries="${shippingCountries}" regions="${billingAddressRegions}" isFormVisible="true" tabIndexStartFrom="10"/>
                                <div class="row">
                                    <div class="col-sm-12 form-cell">
                                        <form:input type="hidden" path="${billingFormPrefix}hasGmacSelect" id="${billingFormPrefix}hasGmacSelect" value="${isShowGmacSelect}"/>
                                        <c:if test="${isShowGmacSelect}">
                                            <address:subscribeGmacInfo subpath="${billingFormPrefix}" tabIndexStartFrom="20" />
                                        </c:if>
                                        <div class="form-buttons">
                                            <div class="checkbox-component">
                                                <form:checkbox path="${billingFormPrefix}subscribeToUpdates" id="${billingFormPrefix}subscribeToUpdates" tabindex="23"/>
                                                <label class="left-side" for="${billingFormPrefix}subscribeToUpdates"><spring:theme code="checkout.updates.subscribe" /></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                          </div>
                          <div class="form-required"><spring:theme code="form.field.required"/></div>
                    </div>
                </div>
                <c:if test="${not wileyBillShippAddressForm.billingOnly}">
                <div class="col-xs-12 col-sm-6 col-md-4 panel">
                    <div role="tab" id="headingOne" class="panel-heading">
                        <div class="form-title"><spring:theme code="checkout.summary.shippingAddress"/>
                           <a role="button" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="form-title-toggle" data-toggle="collapse"></a>
                        </div>
                    </div>
                    <div id="collapseOne" role="tabpanel" aria-labelledby="headingOne" class="panel-collapse collapse in">
                          <div class="site-form">
                             <div class="panel-body">
                                <div class="same-as-billing">
                                    <div class="form-cell">
                                        <div class="form-field">
                                            <div class="checkbox-component">
                                              <form:checkbox path="billingAsShipping" id="sameAsBilling" disabled="false" tabindex="24"/>
                                              <label class="shipping-label" for="sameAsBilling"><spring:theme code="checkout.summary.billingAsShipping" /></label>
                                            </div>
                                        </div>
                                    </div>
                                    <multi-checkout:deliveryMethodSelector deliveryMethods="${deliveryMethods}" selectedDeliveryMethodId="${cartData.deliveryMode.code}" tabIndexStartFrom="25"/>
                                    <div class="site-form">
                                        <address:addressFormElements  subpath="${shippingFormPrefix}" formKey="${wileyBillShippAddressForm.shippingAddress}" countries="${shippingCountries}" 
                                            regions="${shippingAddressRegions}" isFormVisible="${isShippingFormVisible}" tabIndexStartFrom="35"/>
                                    </div>                                </div>
                             </div>
                          </div>
                          <div class="form-required"><spring:theme code="form.field.required"/></div>
                    </div>
                </div>
                </c:if>
                </div>
                <div class="order-details col-xs-12 col-sm-6 col-md-4">
					<div class="form-title"><spring:theme code="text.account.order.title.details"/></div>
					<checkout:orderTotals cartData="${cartData}" showCardEntries="true"/>

					<div class="payment-method panel">
						<div role="tab" id="headingThree" class="panel-heading">
							<div class="form-title">Payment<a role="button" data-toggle="collapse" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree" class="form-title-toggle"></a></div>
							</div>
							<div id="collapseThree" role="tabpanel" aria-labelledby="headingThree" class="panel-collapse collapse in">
								<div class="panel-body">
									<div class="payment-method-variants">
										<div class="form-cell">
											<div class="form-label required">Payment Method</div>
											<div class="form-field">
												<form:radiobutton  cssClass="${isPreOrder ? 'no-radio-button' : ''}" path="paymentMethod" id="creditCard" value="creditCard" checked="checked" tabindex="28"/>
												<label for="creditCard" class="payment-icons">
													<img src="${themeResourcePath}/images/icon-visa.png" alt="Visa">
													<img src="${themeResourcePath}/images/icon-ms-cd.png" alt="MasterCard">
													<img src="${themeResourcePath}/images/icon-disc.png" alt="Discover">
													<img src="${themeResourcePath}/images/icon-amex.png" alt="American Express">
												</label>
											</div>
										<c:if test="${not isPreOrder}">
											<div class="form-field">
												<form:radiobutton path="paymentMethod" id="paypal" value="payPal" tabindex="29"/>
												<label for="paypal" class="payment-icons">
													<img src="${themeResourcePath}/images/icon-pp.png" alt="PayPal">
													<a href="https://www.paypal.com/cgi-bin/webscr?cmd=xpt/Marketing/popup/OLCWhatIsPayPal-outside" target="_blank" title="What is PayPal?" id="what-is-paypal-link" class="paypal-link">What is PayPal?</a>
												</label>
											</div>
										</c:if>
										<multi-checkout:hostedSessionFields paymentCardMonths="${paymentCardMonths}" cardExpiryYears="${cardExpiryYears}"/>
									</div>
								</div>
							</div>
						</div>
					</div>
					<c:url var="returnToCart" value="/cart" />
					<div class="form-buttons"><span class="link-control"><a href="${returnToCart}" title="" class="go-to-card" tabindex="50">Cancel and return to cart</a></span><span class="btn-control">
						<button id="saveBilling" type="button" class="button form-button" tabindex="51"><spring:theme code="payment.button.sendBillingForm" /></button></span>
					</div>
                </div>