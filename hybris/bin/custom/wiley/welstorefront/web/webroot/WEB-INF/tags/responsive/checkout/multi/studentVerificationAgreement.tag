<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<div class="agreement">
    <p class="agreement-title">
        <spring:theme code="checkout.multi.studentVerification.agreement.header" />
    </p>

   <formElement:formCheckbox idKey="agreement"
   		labelKey="checkout.multi.studentVerification.agreement.text"
        path="agreement" mandatory="true" />

</div>