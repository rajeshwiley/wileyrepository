<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<div class="mock-questions">
    <div class="container">
        <div class="row">
            <h3>Sample Our Mock Exam Questions<br><a href="" title="" target="_blank"><strong>Read the Case Scenario &nbsp;</strong>before answering the questions below.</a></h3>
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <iframe height="300" width="100%" src="${mockExamUrl}" frameborder="0" allowfullscreen=""></iframe>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <p><strong>Are you ready for exam day?</strong></p>
                <p>We understand it can be difficult to go through the entire CFA® Institute curriculum in the last few weeks, so we have condensed the material to help you focus on what you need to know to pass.</p>
                <p><strong>Test your knowledge. &nbsp;</strong>Our 11th Hour Review Course lets you:</p>
                <ul class="list-unstyled">
                    <li>Take a full-length mock CFA<sup>®</sup>Exam in timed conditions</li>
                    <li>Retake the mock exam as many times as you need</li>
                    <li>Your last 10 results will be recorded so you can stay on track and measure your progress</li>
                </ul>
                <p>Want to test your knowledge further? &nbsp;<strong>Check out our &nbsp;<a href="" title="">CFA<sup>®</sup> Test Bank</a> where you can take an additional full-length Mock Exam and build your own online quiz from a new set of 500 questions &nbsp;</strong>(Wiley Test Bank is included as standard in Platinum, Gold, Silver and Self-Study courses).</p>
            </div>
        </div>
    </div>
</div>