<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="entry" required="true" type="de.hybris.platform.commercefacades.order.data.OrderEntryData"%>
<%@ attribute name="isGiftCard" required="true" type="java.lang.Boolean"%>
<%@ attribute name="productUrl" required="true" type="java.lang.String"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<h3 class="subscription-title">
	<c:choose>
		<c:when test="${isGiftCard}">
			<span class="subscription-title-link">${entry.product.name}</a>
		</c:when>
		<c:otherwise>
			<c:choose>
				<c:when test="${not empty productUrl}">
					<a href="${productUrl}" class="subscription-title-link">${entry.product.name}</a>
				</c:when>
				<c:otherwise>
					<span class="subscription-title-link">${entry.product.name}</span>
				</c:otherwise>
			</c:choose>
		</c:otherwise>
	</c:choose>
</h3>