<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="storepickup" tagdir="/WEB-INF/tags/responsive/storepickup"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart"%>

<div class="subscriptions-container">
    <div class="subscriptions-header">
        <div class="subscriptions-header-item item-item">
            <spring:theme code="basket.page.item" />
        </div>
        <div class="subscriptions-header-item item-price">
            <spring:theme code="basket.page.price" />
        </div>
        <div class="subscriptions-header-item item-qty">
            <spring:theme code="basket.page.qty" />
        </div>
        <div class="subscriptions-header-item item-total">
            <spring:theme code="basket.page.total" />
        </div>
        <div class="subscriptions-header-item item-action"></div>
    </div>
    <div class="subscriptions-content">
        <c:forEach items="${cartData.entries}" var="entry">
            <c:remove var="productUrl"/>
            <c:if test="${! entry.product.partnerOnly}">
                <c:url value="${entry.product.url}" var="productUrl" />
            </c:if>

            <c:set var="isGiftCard" value="${entry.product.isGiftCard}"/>
            <div class="subscription">
                <div class="subscription-info">
                    <cart:entryProductImage entry="${entry}" productUrl="${productUrl}"/>
                    <cart:entryProductTitle entry="${entry}" isGiftCard="${isGiftCard}" productUrl="${productUrl}"/>
                    <div class="subscription-description">
                        <ul>
                            <c:choose>
								<c:when test="${not empty entry.product.optionalDescription}">
									<li>${entry.product.optionalDescription}</li>
								</c:when>
								<c:otherwise>
									<li>${entry.product.description}</li>
								</c:otherwise>
                            </c:choose>
                        </ul>
                    </div>
                     <div class="subscription-description product-type" >
						 <c:url value="/cart/change/producttype" var="wileyUpdateProductTypeAction" />
						 <form:form id="wileyUpdateProductTypeForm${entry.entryNumber}" action="${wileyUpdateProductTypeAction}" method="post"
							   commandName="wileyUpdateProductTypeForm${entry.entryNumber}">
							   <input type="hidden" name="entryNumber" value="${entry.entryNumber}" />
							   <input type="hidden" name="productCode" value="${entry.product.code}" />
							   <input type="hidden" name="quantity" value="${entry.quantity}" />
							   <product:productVariantsSectionCart rootNode = "${entry.productRootNode.product}" formIndex="${entry.entryNumber}" productInCart="${entry.product}" />
						  </form:form>
                      </div>



                    <c:if test="${ycommerce:doesPotentialPromotionExistForOrderEntry(cartData, entry.entryNumber)}">
                        <c:forEach items="${cartData.potentialProductPromotions}" var="promotion">
                            <c:set var="displayed" value="false"/>	                           
                             <c:forEach items="${promotion.consumedEntries}" var="consumedEntry">
                                <c:if test="${not displayed && consumedEntry.orderEntryNumber == entry.entryNumber && not empty promotion.description}">
                                  <c:set var="displayed" value="true"/>
                                    <div class="discount-code-message">
                                      <ycommerce:testId code="cart_potentialPromotion_label">
                                         ${promotion.description}
                                      </ycommerce:testId>
                                    </div>
                               	</c:if>
                           </c:forEach>
                        </c:forEach>
                    </c:if>

					<c:if test="${not isGiftCard &&  ycommerce:doesAppliedPromotionExistForOrderEntry(cartData, entry.entryNumber)}">
                        <c:forEach items="${cartData.appliedProductPromotions}" var="promotion">
                            <c:set var="displayed" value="false"/>	                            
                            <c:forEach items="${promotion.consumedEntries}" var="consumedEntry">
                               <c:if test="${not displayed && consumedEntry.orderEntryNumber == entry.entryNumber}">
                                  <c:set var="displayed" value="true"/>
                                  <div class="discount-code-message">
                                      <ycommerce:testId code="cart_appliedPromotion_label">
                                          ${promotion.description}
                                      </ycommerce:testId>
                                  </div>
                              	</c:if>
                            </c:forEach>
                        </c:forEach>
                     </c:if>

                </div>
                <div class="subscription-price-item item-action item-action-mobile">
                    <div class="subscription-price-name"></div>
                    <div class="subscription-price-value">
                        <c:if test="${not isGiftCard}">
                            <button type="button" class="remove-entry-button link remove-link" id="removeEntry_${entry.entryNumber}">
                                <spring:theme code="basket.page.remove" />
                            </button>
                        </c:if>
                    </div>
                </div>

                <div class="subscription-price-item item-price">
                    <div class="subscription-price-name">
                        <spring:theme code="basket.page.price" />
                        :
                    </div>
                    <div class="subscription-price-value">
                        <format:price priceData="${entry.basePrice}" displayFreeForZero="true" />
                    </div>
                </div>
                <div class="subscription-price-item item-qty">
                    <div class="subscription-price-name">
                        <spring:theme code="basket.page.qty" />
                        :
                    </div>
                    <div class="subscription-price-value">
                        <c:url value="/cart/update" var="cartUpdateFormAction" />
                        <form:form id="updateCartForm${entry.entryNumber}" action="${cartUpdateFormAction}" method="post"
                            commandName="updateQuantityForm${entry.entryNumber}">
                            <input type="hidden" name="entryNumber" value="${entry.entryNumber}" />
                            <input type="hidden" name="productCode" value="${entry.product.code}" />
                            <input type="hidden" name="initialQuantity" value="${entry.quantity}" />
                            <ycommerce:testId code="cart_product_quantity">
                                <div class="quantity-field">
                                    <c:set value="${not entry.updateable or not entry.doesProductHaveQuantity}"
                                        var="quantityDisplay" />
                                    <c:choose>
                                        <c:when test="${quantityDisplay}">
                                        ${entry.quantity}
                                         <form:input cssClass="form-control form-control-quantity" type="hidden" size="1"
                                                id="quantity_${entry.entryNumber}" path="quantity" />
                                        </c:when>
                                        <c:otherwise>
                                            <button type="button" class="btn btn-quantity-minus"></button>
                                            <form:input cssClass="form-control form-control-quantity" type="text" size="1"
                                                id="quantity_${entry.entryNumber}" path="quantity" maxlength="2"/>
                                            <button type="button" class="btn btn-quantity-plus"></button>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </ycommerce:testId>
                        </form:form>
                    </div>
                </div>
                <div class="subscription-price-item item-total">
                    <div class="subscription-price-name">
                        <spring:theme code="basket.page.total" />
                        :
                    </div>
                    <div class="subscription-price-value">
                        <format:price priceData="${entry.totalPrice}" displayFreeForZero="true" />
                    </div>
                </div>
                <div class="subscription-price-item item-action">
                    <div class="subscription-price-name"></div>
                    <div class="subscription-price-value">
                        <c:if test="${not isGiftCard}">
                            <button type="button" class="remove-entry-button link remove-link" id="removeEntry_${entry.entryNumber}">
                                <spring:theme code="basket.page.remove" />
                            </button>
                        </c:if>
                    </div>
                </div>
            </div>
        </c:forEach>
    </div>
</div>

