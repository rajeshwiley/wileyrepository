<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>

<template:page pageTitle="${pageTitle}" containerCSSClass="container compare-cfa">
	<cms:pageSlot position="ProductComparisonSlot" var="feature">
		<cms:component component="${feature}" />
	</cms:pageSlot>

	<div class="comparison-products">
		<div class="comparison-product">
			<cms:pageSlot position="ProductListSlot" var="feature">
				<cms:component component="${feature}" />
			</cms:pageSlot>
		</div>
	</div>
</template:page>