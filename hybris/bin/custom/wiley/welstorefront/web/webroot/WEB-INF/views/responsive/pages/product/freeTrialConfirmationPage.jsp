<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<template:page pageTitle="${pageTitle}" containerCSSClass="container free-trial-confirmation">

    <cms:pageSlot position="TopContent" var="contactInfoComponent">
        <cms:component component="${contactInfoComponent}"/>
    </cms:pageSlot>

    <cms:pageSlot position="FreeTrialConfirmation" var="pinConfirmationComponent">
        <cms:component component="${pinConfirmationComponent}"/>
    </cms:pageSlot>

    <form method="get" action="<spring:url value="/my-account"/>">
        <div class="form-buttons">
            <button class="button form-button myaccount-button"><spring:theme code="pinActivation.gotomyaccountbutton"/></button>
        </div>
    </form>

    <cms:pageSlot position="Disclaimer" var="disclaimer">
        <cms:component component="${disclaimer}"/>
    </cms:pageSlot>

</template:page>