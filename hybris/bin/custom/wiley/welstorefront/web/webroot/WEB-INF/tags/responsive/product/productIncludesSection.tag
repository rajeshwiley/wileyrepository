<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ attribute name="useOneColumnOnly" required="false" type="java.lang.Boolean" %>

<c:if test="${useOneColumnOnly == null}"><c:set var="useOneColumnOnly" value="${false}" /></c:if>

<c:set var="features" value="${product.includeFeatures}" scope="request"/>

<spring:eval var="twoColumnMinSize"
             expression="T(com.wiley.facades.constants.WileyFacadesConstants).PDP_INCLUDE_LIST_TWO_COLUMN_MIN_SIZE"/>             
<c:set var="oneColumn" value="${features.size() >= twoColumnMinSize and not useOneColumnOnly ?  false : true}" scope="request"/>
             
<c:if test="${not empty features}">
	<div class="product-include clearfix">
		<div class="container-fluid">
			<div class="row">
				<p class="product-include-title"><spring:message code="product.includes" text="Includes:"/></p>

                <c:choose>
                    <c:when test="${oneColumn}">
                        <ul class="features-listing col-md-12 clearfix" >
                    </c:when>
                    <c:otherwise>
                        <ul class="features-listing col-md-6 clearfix" >
                    </c:otherwise>
                </c:choose>
					<c:forEach items="${features}" var="feature" varStatus="columnLoop1">
						<c:if test="${oneColumn or !oneColumn and columnLoop1.index < features.size() div 2}">
							<product:productIncludeItem feature="${feature}"/>
						</c:if>

					</c:forEach>
				</ul>

				<ul class="features-listing col-md-6 clearfix">
					<c:forEach items="${features}" var="feature" varStatus="columnLoop2">
						<c:if test="${!oneColumn and columnLoop2.index >= features.size() div 2}">
							<product:productIncludeItem feature="${feature}"/>
						</c:if>

					</c:forEach>
				</ul>
			</div>
		</div>
	</div>
</c:if>