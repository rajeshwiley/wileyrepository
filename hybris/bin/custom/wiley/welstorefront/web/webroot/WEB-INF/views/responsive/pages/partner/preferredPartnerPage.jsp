<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>

<template:page pageTitle="${pageTitle}" containerCSSClass="container unified-partner">
    <div class="content">
        <div class="welcome-partners">
            <h2 class="title">
                <cms:pageSlot position="PartnerTitle" var="feature">
                    <cms:component component="${feature}" />
                </cms:pageSlot>
            </h2>
            <div class="row">
                <div class="welcome-partners-image col-sm-5 col-xs-12">
                    <cms:pageSlot position="PartnerImage" var="feature">
                        <cms:component component="${feature}" />
                    </cms:pageSlot>
                </div>
                <div class="welcome-partners-content col-sm-7 col-xs-12">
                    <div class="partner-chooser">
                        <cms:pageSlot position="PartnerSelectionBar" var="feature">
                            <cms:component component="${feature}" />
                        </cms:pageSlot>
                    </div>
                    <div class="welcome-partners-description">
                        <cms:pageSlot position="PartnerCategoryDescription" var="feature">
                            <cms:component component="${feature}" />
                        </cms:pageSlot>
                    </div>
                </div>
            </div>
        </div>
        <div class="product-detailed-info">
            <cms:pageSlot position="PartnerProductList" var="feature">
                <cms:component component="${feature}" />
            </cms:pageSlot>
        </div>
        <product:productPageTabs />
    </div>
</template:page>