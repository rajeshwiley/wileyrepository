<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>

<div class="bundle">
    <ycommerce:testId code="productDetails_promotion_label">
        <c:if test="${not empty product.potentialPromotions}">
            <c:forEach items="${product.potentialPromotions}" var="potentialPromotion">
                <c:forEach items="${potentialPromotion.banners}" var="media">
                    <c:choose>
                        <c:when test="${empty imagerData}">
                            <c:set var="imagerData">"${media.width}":"${media.url}"</c:set>
                        </c:when>
                        <c:otherwise>
                            <c:set var="imagerData">${imagerData},"${media.width}":"${media.url}"</c:set>
                        </c:otherwise>
                    </c:choose>
                    <c:if test="${empty altText}">
                        <c:set var="altText" value="${media.altText}"/>
                    </c:if>
                </c:forEach>

                <c:if test="${not empty imagerData}">
                    <c:url value="${urlLink}" var="encodedUrl"/>
                    <div class="simple-responsive-banner-component">
                        <p>
                            <c:choose>
                                <c:when test="${empty encodedUrl || encodedUrl eq '#'}">
                                    <img class="js-responsive-image" data-media='{${imagerData}}' alt='${altText}'
                                         title='${altText}'
                                         style="">
                                </c:when>
                                <c:otherwise>
                                    <a href="${encodedUrl}">
                                        <img class="js-responsive-image" data-media='{${imagerData}}' title='${altText}'
                                             alt='${altText}'
                                             style="">
                                    </a>
                                </c:otherwise>
                            </c:choose>
                        </p>
                    </div>
                </c:if>
                <p>${potentialPromotion.description}</p>
                <c:remove var="imagerData"/>
                <c:remove var="altText"/>
            </c:forEach>
        </c:if>
    </ycommerce:testId>
</div>
