<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<spring:theme code="partner.selection.dropdown.label" var="label" />
<spring:theme code="partner.selection.dropdown.placeholder"
	var="placeholder" />
<div class="partner-chooser">
	<label for="partner-chooser" class="partner-chooser-label">${label}</label>
	<div class="partner-chooser-input">
		<c:choose>
			<c:when test="${partnerName ne null}">
				<input type="text" autocomplete="off" placeholder="${partnerName}"
					name="partner-chooser" id="partner-chooser"
					class="autocomplete ui-autocomplete-input"
					data-partners="${fn:escapeXml(partners)}">
			</c:when>
			<c:otherwise>
				<input type="text" autocomplete="off" placeholder="${placeholder}"
					name="partner-chooser" id="partner-chooser"
					class="autocomplete ui-autocomplete-input"
					data-partners="${fn:escapeXml(partners)}">
			</c:otherwise>
		</c:choose>
	</div>
</div>