<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<template:page pageTitle="${pageTitle}" containerCSSClass="container compare-cfa">
        <cms:pageSlot position="ProductsComparisonTitle" var="feature">
            <cms:component component="${feature}" element="div" />
        </cms:pageSlot>
    <cms:pageSlot position="ProductsComparison" var="feature">
        <cms:component component="${feature}" element="div" />
    </cms:pageSlot>
	<c:if test="${fn:length(categorySupplementsProducts) > 0}">
        <cms:pageSlot position="SupplementsListTitle" var="feature">
            <cms:component component="${feature}" element="div" />
        </cms:pageSlot>
        <cms:pageSlot position="SupplementsList" var="feature">
            <cms:component component="${feature}" element="div" />
        </cms:pageSlot>
	</c:if> 		
</template:page>
