<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ attribute name="productTemplate" required="false" %>
<%@ attribute name="formIndex" required="false" %>


<c:set var="isAddonsBlockVisible" value="true"/>
<c:set var="isContainer" value="false"/>

<c:if test="${productTemplate == 'cfa' && formIndex ne '0'}">
    <c:set var="isAddonsBlockVisible" value="false"/>
</c:if>
<c:if test="${productTemplate == 'cfa' }">
    <c:set var="isContainer" value="true"/>
</c:if>

<c:set var="displayStyle" value="${isAddonsBlockVisible ? '' : 'display:none'}"/>

<c:if test="${not empty productReferences}">
    <c:choose>
        <c:when test="${product.multidimensional}">
            <c:set var="addonInputName" value="product-addon"/>
        </c:when>
        <c:otherwise>
            <c:set var="addonInputName" value="productCodePost"/>
        </c:otherwise>
    </c:choose>
    <c:if test="${isContainer}">
        <div class="container">
    </c:if>
        <div class="product-addons" style = ${displayStyle}>
            <div class="product-addons-title"><spring:theme code="text.addons.title"/></div>
            <div class="product-addons-content">
                <c:forEach items="${productReferences}" var="productReference">
                    <input type="checkbox" name="${addonInputName}" id="product-addon-${productReference.target.code}-${formIndex}" value="${productReference.target.code}" data-price="${productReference.target.price.value}"/>
                    <label for="product-addon-${productReference.target.code}-${formIndex}">${productReference.target.name}<%--
                        --%><c:if test="${productReference.target.summary ne ''}"><%--
                            --%>&nbsp;<span class="tooltipHelp" data-toggle="popover" data-content='<b>${productReference.target.name}</b><br/>${productReference.target.summary}'>?</span>
                        </c:if>
                    </label>
                </c:forEach>
            </div>
        </div>
    <c:if test="${isContainer}">
        </div>
    </c:if>
</c:if>