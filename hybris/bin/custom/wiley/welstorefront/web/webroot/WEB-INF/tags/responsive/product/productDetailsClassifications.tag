<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="product-classifications">
	<c:if test="${not empty product.classifications}">
		<c:forEach items="${product.classifications}" var="classification">
						<c:forEach items="${classification.features}" var="feature" varStatus="loopStatus">
						 <div class="tab-item">
							
							 
									<c:forEach items="${feature.featureValues}" var="value" varStatus="status">
										<c:if test="${value.value and not empty feature.details}">
											  <c:forEach items="${feature.media}" var="image">
										      <c:set var="details_img" value="${image}"/>
										      
											  <img  class="${loopStatus.index % 2 == 0 ? 'img' : 'img right-side'}" src="${details_img.url}"/>
											 
										      </c:forEach> 
											 <div  class="tab-item-content">
												${feature.details}
											 </div>
										</c:if>
									</c:forEach>
							
							</div>
						</c:forEach>
				

		</c:forEach>
	</c:if>
</div>


