<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="user" tagdir="/WEB-INF/tags/responsive/user" %>

<c:url value="/pin/j_spring_security_check" var="pinLoginActionUrl" />
<c:set var="forgotPasswordRedirectType" value="pl" scope="request" />
<user:login actionNameKey="login.login" action="${pinLoginActionUrl}"/>
