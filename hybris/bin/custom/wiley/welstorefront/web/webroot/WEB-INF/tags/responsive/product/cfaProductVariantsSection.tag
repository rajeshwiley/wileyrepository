<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ attribute name="formIndex" required="false" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ attribute name="rootNode" required="true" type="com.wiley.facades.product.node.ProductNode" %>

<c:set var="code" value="${rootNode.code}"/>
<c:if test="${code eq ''}">
    <c:set var="code" value="root"/>
</c:if>
<c:if test="${not empty rootNode.nodes}">
    <div id="variants-section-${code}-${formIndex}">
    	<c:set var="selectedNodeIndex" value="0"/>
        <c:forEach items="${rootNode.nodes}" var="node" varStatus="loop">
            <c:set var="visibleStyle" value="${(not node.viewVisible || fn:length(rootNode.nodes) eq 1) ? 'display: none' : '' }"/>
            <c:set var="checked" value=""/>
			<c:forEach items="${product.categories}" var="category">
				<c:if test="${node.code == category.code}">
					<c:set var="selectedNodeIndex" value="${loop.index}"/>
					<c:set var="checked" value="checked"/>
				</c:if>
			</c:forEach>

            <input type="${node.viewType}" name="${node.viewName}" id="product-variant-${node.code}-${formIndex}" data-code="${node.code}" value="${node.value.sku}" data-url="<c:url value="${node.value.url}"/>" onclick="window.location.assign($(this).data('url'))" ${checked}/>
            <label for="product-variant-${node.code}-${formIndex}" style="${visibleStyle}">${node.name}</label>
        </c:forEach>
        <product:cfaProductVariantsSection product="${product}" rootNode="${rootNode.nodes[selectedNodeIndex]}" formIndex="${formIndex}"/>
    </div>
</c:if>
