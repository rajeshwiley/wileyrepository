<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>

<div class="cart-box">
<div class="container">
  <div class="box">
  <form:form method="post" id="addToCartForm">

    <c:choose>
        <c:when test="${product.multidimensional}">
            <c:url var="addToCartUrl" value="/cart/addGrid" />
            <c:set var="inputName" value="sku"/>
            <c:set var="inputType" value="checkbox"/>
            <%-- TODO: should be changed when logic with variants for CFA is more clear --%>
            <c:set var="inputValue" value="${product.variantMatrix[0].variantOption.code}"/>
        </c:when>
        <c:otherwise>
            <c:url var="addToCartUrl" value="/cart/add" />
            <c:set var="inputName" value="productCodePost"/>
            <c:set var="inputType" value="hidden"/>
            <c:set var="inputValue" value="${product.code}"/>
        </c:otherwise>
    </c:choose>

    <div>
      <div class="price">${product.price.formattedValue}</div>
      <div class="text">
        <input type="${inputType}" name="${inputName}" id="cardBox1" value="${inputValue}" checked/>
        <label for="cardBox1" style="display:none" >
          <a class="tooltip-text">
            Apply 20% student discount
            <span class="tooltip-text-element" data-toggle="popover" data-content="<p>And here's some amazing content.</p> <p>It's very engaging. Right?</p>" data-placement="top" data-original-title="Popover title"></span>
          </a>
        </label>
      </div>
    </div>
    <div>
        <button type="submit" class="btn btn-primary js-add-to-cart" formaction="${addToCartUrl}">
            <spring:theme code="basket.add.to.basket"/>
        </button>
    </div>
  </form:form>
  </div>
</div>
</div>