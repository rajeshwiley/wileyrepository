<%@ page trimDirectiveWhitespaces="true" isErrorPage="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
    <title>WEL Site | Client error page</title>
				<c:url var="uiUrl" value="/_ui" />
				<link href="${uiUrl}/addons/welagsstorefrontcommons/responsive/common/css/welagsstorefrontcommons.css" media="all" type="text/css"
								rel="stylesheet">
				<link href="${uiUrl}/responsive/theme-blue/css/style.css" media="all" type="text/css" rel="stylesheet">
</head>
<body>
		<c:url var="homePageUrl" value="/" />
		<c:set var="homePageUrlTitle" value="Wiley AGS" />
		<main>
     <header id="main-header">
       <div class="site-header">
         <div class="container">
           <div class="row">
             <div class="col-md-4 col-sm-4 col-xs-8">
               <div class="logo">
                 <a href="${homePageUrl}" title="Wiley EL" class="hidden-xs">
                  <img src="${uiUrl}/responsive/theme-blue/images/logo.png" alt="Wiley EL" class="img-responsive"></a>
                  <a href="${homePageUrl}" title="Wiley EL" class="visible-xs">&lt;&nbsp;&nbsp;
                  <img src="${uiUrl}/responsive/theme-blue/images/logo-320.png" alt="Wiley EL" class="img-responsive"></a>
               </div>
             </div>
           </div>
         </div>
       </div>
     </header>

     <div class="container">
       <div class="content">
         <h2 style="font-family: Arial, Helvetica, sans-serif;">&nbsp;</h2>
         <h1 class="page-title"
             style="font-style: inherit; border: 0px; font-family: 'Liberation Sans', sans-serif, Helvetica; font-size: 16pt; font-weight: 300; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; clear: both; line-height: 1.09091; color: rgb(187, 189, 192); text-transform: uppercase; text-align: center;">
             CLIENT ERROR (405) <strong><br /></strong>
         </h1>
         <h1 class="page-title"
             style="border: 0px; font-family: 'Liberation Sans', sans-serif, Helvetica; font-size: 16pt; font-style: inherit; font-weight: 300; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; clear: both; line-height: 1.09091; color: rgb(187, 189, 192); text-transform: uppercase; text-align: center;">
             <p style="color: rgb(0, 0, 0); font-family: Arial, Verdana, sans-serif; font-size: 12px; line-height: normal; text-align: start; text-transform: none;">&nbsp;</p>
             <div class="page-content"
                 style="color: rgb(0, 0, 0); font-family: Arial, Verdana, sans-serif; font-size: 12px; line-height: normal; text-align: start; text-transform: none; border: 0px; margin: 0px auto 48px; outline: 0px; padding: 0px 30px; vertical-align: baseline; max-width: 100%; word-wrap: break-word;">
                 <p style="color: rgb(51, 51, 51); font-family: inherit; font-size: 16px; line-height: 24px; border: 0px; border-image-source: initial; border-image-slice: initial; border-image-width: initial; border-image-outset: initial; border-image-repeat: initial; font-style: inherit; font-weight: inherit; margin: 0px 0px 24px; outline: 0px; padding: 0px; vertical-align: baseline; text-align: center;">
                     Please try again later or contact&nbsp;<a href="mailto:info@efficientlearning.com">info@efficientlearning.com</a>&nbsp;
                 </p>
             </div>
         </h1>
         <p>&nbsp;</p>
       </div>
     </div>

					<footer class="main-footer">
							<div class="container">
									<div class="row">
										 <div class="col-md-10 col-sm-10 col-xs-12"></div>
											<div class="col-md-2 col-sm-2 col-xs-12">
														<a href="" title="" class="logo"><img src="${uiUrl}/responsive/theme-blue/images/logo-wiley-footer.png"
																	alt="Wiley logo" class="img-responsive"></a>
										 </div>
									</div>
							</div>
					</footer>
		</main>
</body>
</html>