<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="step" required="false"%>
<%@ attribute name="breadcrumbLabels" required="true" type="java.util.List"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:set var="doneSymbol" value="&#10004" />
<ul class="progress-line">
    <c:forEach items="${breadcrumbLabels}" var="item" varStatus="loop">
		<c:set var="isPreviousStep" value="${loop.count < step}" />
        <li class="step  ${loop.count <= step ? 'step-completed':'none'}">
        <span class="step-number">
        	<c:choose>
        		<c:when test="${isPreviousStep}">${doneSymbol}</c:when>
        		<c:otherwise>
        			<spring:theme code="checkout.multi.breadcrumb.step.${loop.count}" />
        		</c:otherwise>
        	</c:choose>
        </span>
        <span class="step-name step-name-mobile"><spring:theme code="${item}.mobile" /></span>
        <span class="step-name step-name-desktop"><spring:theme code="${item}.desktop" /></span>
    </c:forEach>
</ul>