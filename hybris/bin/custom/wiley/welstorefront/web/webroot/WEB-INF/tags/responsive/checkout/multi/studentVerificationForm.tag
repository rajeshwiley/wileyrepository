<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi"%>

<form:form method="post" commandName="studentVerificationForm"
    action="${request.contextPath}/checkout/multi/student-verification/add" id="studentVerificationForm">
    <div class="site-form">

        <multi-checkout:studentVerificationSelectors countries="${countries}"
            countryIso="${studentVerificationForm.countryIso}" />

        <div id="studentVerificationParagraphComponent" class="panel-group expanding-sections">
            <c:if test="${not empty cmsPage}">
                <cms:pageSlot position="StudentVerification" var="feature">
                    <cms:component component="${feature}" />
                </cms:pageSlot>
            </c:if>
        </div>

        <multi-checkout:studentVerificationAgreement />

        <div class="form-required">
            <spring:theme code="checkout.multi.studentVerification.required" />
        </div>
        <div class="form-buttons">
            <button type="submit" class="button form-button">
                <spring:theme code="checkout.multi.studentVerification.next" />
            </button>
        </div>
    </div>
</form:form>