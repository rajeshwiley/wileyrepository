<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<a name="learnmore"></a>
<div class="tabs js-tabs tabs-responsive">

	
	<div class="tabhead">
		<span class="glyphicon"></span>
		<a href=""><spring:theme code="product.product.spec" /></a>
	</div>
	<div class="tabbody">
		<product:productDetailsClassifications product="${product}" />
	</div>

	<cms:pageSlot position="ProductTabsContainer" var="tabs">
		<cms:component component="${tabs}" />
	</cms:pageSlot>

</div>
