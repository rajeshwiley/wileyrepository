<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<template:page containerCSSClass=" " pageTitle="${pageTitle}" mainCSSClass="full-width-layout">

    <div class="description-product">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 tablet">
                    <h1>${product.name}</h1>
                </div>
                <div class="col-md-6 col-md-push-6 col-sm-6 col-sm-push-6 col-xs-12">
                    <div class="image">
                        <cms:pageSlot position="ProductImage" var="component">
                            <cms:component element="div class='${component.uid}'" component="${component}"/>
                        </cms:pageSlot>
                    </div>
               		<product:productPromotionSection product="${product}"/>
                </div>
                <div class="col-md-6 col-md-pull-6 col-sm-6 col-sm-pull-6 col-xs-12">
                    <div class="description">
                        <h2 class="mobile">${product.name}</h2>
                        <div class="text">
                            <cms:pageSlot element="div class='offset-bottom-20'" position="NavigationLinks" var="component">
                                <cms:component element="div class=top-link" component="${component}"/>
                            </cms:pageSlot>
                            <c:if test="${not empty product.details}">
                                <p>
                                    <c:forEach items="${product.details}" var="medialink">
                                        <a href="${medialink.url}">${medialink.name}</a><br>
                                    </c:forEach>
                                </p>
                            </c:if>
                            <p>${product.description}</p>
                            <cms:pageSlot position="BottomNavigationLinks" var="component">
                                <cms:component component="${component}"/>
                            </cms:pageSlot>
                            <cms:pageSlot position="CustomProductInformation" var="component">
                                <cms:component element="div class='note'" component="${component}"/>
                            </cms:pageSlot>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <product:productAddToCartForm product="${product}" productTemplate="cfa" formIndex="0"/>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <cms:pageSlot position="ContentModules" var="component">
        <cms:component component="${component}"/>
    </cms:pageSlot>


    <div class="chat-with-expert">
        <div class="container">
          <div class="row">
            <div class="col-md-12"><a href="${liveChatUrl}" onclick="window.open('${liveChatUrl}', 'newwindow', 'width=475, height=400'); return false;" title=""><span><spring:theme code="liveChat.haveAQuestion"/>&nbsp;<strong><spring:theme code="liveChat.chatNow"/></strong></span></a></div>
          </div>
        </div>
      </div>

    <product:productAddToCartForm product="${product}" productTemplate="cfa" formIndex="2"/>

</template:page>

