<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ attribute name="paymentCardMonths" required="true" type="java.util.List" %>
<%@ attribute name="cardExpiryYears" required="true" type="java.util.List" %>

<script src="${cartData.hostedSessionScriptUrl}"></script>


<div id="hostedSessionFields" class="paymentCardData">
	<div id="card-number" class="form-field">
		<label class="control-label form-label required" for="card-number-input">
			<b><spring:theme code="payment.paymentCardNumber" /></b>
		</label>
		<input type="text" id="card-number-input" class="input-field card-number-input" value="" readonly>
	</div>
	<div id="expiry-month" class="form-field inline">
		<label class="control-label form-label required">
			<b><spring:theme code="payment.expirationDate" /></b>
		</label>
		<select class="expiry-select" id="expiry-month-select">
			<c:forEach items="${paymentCardMonths}" var="month">
				<option value="${month.code}">${month.name}</option>
			</c:forEach>
		</select>
	 </div>
	<div id="expiry-year" class="form-field inline">
		<select id="expiry-year-select" class="expiry-select">
			<c:forEach items="${cardExpiryYears}" var="year">
				<option value="${year.code}">${year.name}</option>
			</c:forEach>
		</select>
	</div>
	 <div id="security-code" class="form-field">
		<label class="control-label form-label required" for="security-code-input">
			<b><spring:theme code="checkout.security.code" /></b>
		</label>
		<input type="text" id="security-code-input" class="input-field security-code-input" value="" readonly>
		<div class="tooltip-block">
			<span data-toggle="popover" data-content='<spring:theme code="payment.securityCode.tooltip" />' data-original-title="" class="tooltipHelp">?</span>
		</div>
		<input type="hidden" name="sessionId" id="sessionId">
		<input type="hidden" value="${supportedCardTypes}" id="supportedCardTypes">
		<input type="hidden" id="hostedSessionErrorMessages">
	 </div>
</div>

<script type="text/javascript" src="${commonResourcePath}/js/hostedSession.js"></script>
<script type="text/javascript" src="${commonResourcePath}/js/paymentSession.js"></script>
<script>
	HostedSession.errorMessages = ${hostedSessionErrorMessages};
</script>
