<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi"%>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="checkout" tagdir="/WEB-INF/tags/responsive/checkout" %>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/address"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart" %>

<c:url var="returnToCartUrl" value="/cart" />

<template:page pageTitle="${pageTitle}" hideHeaderLinks="true"
	hideActivatePinLink="true" hideCartLink="true" hideLoginLink="true"
	hideCustomLinks="true"
	containerCSSClass="container order-review-container">

    <cms:pageSlot position="CheckoutBreadcrumbs" var="feature">
        <cms:component component="${feature}" />
    </cms:pageSlot>

	<center>
		<b><spring:theme code="checkout.orderReview.reviewYourOrderMessage" /></b>
	</center>

	<div class="cancelLinkSlot">
		<div class="row-item">
			<a href="${returnToCartUrl}" class="cancelAndReturnToCart"><spring:theme code="checkout.orderReview.cancelAndReturnToCart"/></a>
		</div>
	</div>
	<div class="row-item">
		<div class="cancelLinkSlot">
			<a href="${returnToCartUrl}" class="cancelAndReturnToCart"><spring:theme code="checkout.orderReview.cancelAndReturnToCart"/></a>
		</div>
		<div class="reviewYourOrderSlot">
			<multi-checkout:reviewYourOrder cartData="${cartData}"/>
		</div>

		<div class="orderSummarySlot">
			<multi-checkout:orderSummary cartData="${cartData}"/>
		</div>
	</div>

	<div class="row-item">
		<div class="purchaseDetailsSlot">
			<cart:purchaseDetails cartData="${cartData}"/>
		</div>
	</div>

	<cms:pageSlot position="CustomerSupportContact" var="feature">
    	<cms:component component="${feature}" />
    </cms:pageSlot>

</template:page>

