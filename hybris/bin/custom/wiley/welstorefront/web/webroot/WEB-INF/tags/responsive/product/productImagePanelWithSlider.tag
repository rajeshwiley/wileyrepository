<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ attribute name="galleryImages" required="true" type="java.util.List" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>

<div class="slider">
	<div id="sync1" class="owl-carousel">
        <c:forEach items="${galleryImages}" var="container" varStatus="varStatus">
        	<div class="item">
                <img class="lazyOwl" src="${container.product.url}"
                     alt="${container.thumbnail.altText}" >
            </div>
        </c:forEach>
	</div>
	<c:if test="${galleryImages != null && galleryImages.size() > 1}">
		<div id="sync2" class="owl-carousel">
        	<c:forEach items="${galleryImages}" var="container" varStatus="varStatus">
        		<div class="item">
                	<img class="lazyOwl" src="${container.product.url}"
                    	 alt="${container.thumbnail.altText}" >
            	</div>
        	</c:forEach>
		</div>
	</c:if>
</div>