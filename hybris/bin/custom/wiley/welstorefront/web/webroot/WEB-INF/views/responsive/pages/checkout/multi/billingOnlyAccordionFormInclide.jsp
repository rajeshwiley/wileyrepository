<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi"%>

	<div id="accordion" role="tablist" aria-multiselectable="false" class="row billing-address panel-group">
       <form:form method="post" commandName="wileyBillShippAddressForm" action="${request.contextPath}/checkout/multi/billing-shipping-address/billing-only-add" id="wileyBillShippAddressForm">
  			<div id="i18nAddressForm" class="i18nAddressForm">
                 <multi-checkout:billingShippingAccordion/>
             </div>
        </form:form>
    </div>