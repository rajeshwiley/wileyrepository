<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="container product-brief">
    <div class="container-fluid">
        <div class="row">
            <ul class="product-listing product-list">
                <c:forEach items="${products}" var="product" varStatus="status">
                    <product:supplementsListerItem product="${product}" />
                </c:forEach>
            </ul>
        </div>
    </div>
</div>


