<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<c:set var="features" value="${product.includeFeatures}" scope="request"/>

<div class="front-row bottom-line">
    <div class="front-row-image">
	    <img width="1280" height="443" src="${themeResourcePath}/images/bgimage_3.jpg" alt="">
    </div>
    <div class="front-row-text">
        <div class="container">
            <div class="row">
                <div class="col-sm-7 col-md-7">
                    <spring:message code="product.virtual.class.description1"/>
                    <p>
                        <spring:message code="product.virtual.class.description2"/>&nbsp;
                        <c:forEach items="${features}" var="feature">
                            <c:if test="${not empty feature.document}">
                                <a href="${feature.document.url}" target="_blank"><spring:message
                                        code="product.virtual.class.schedule" /></a>
                            </c:if>
                        </c:forEach>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>