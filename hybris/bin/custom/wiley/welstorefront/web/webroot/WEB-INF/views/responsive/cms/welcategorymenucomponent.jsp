<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<style>
	<c:if test="${component.menuBackgroundColor ne null}">
		.${component.headerCssClass}.product-header {
			background-color: ${component.menuBackgroundColor} !important;
		}
	</c:if>

	<c:if test="${component.subMenuBackgroundColor ne null}">
		.${component.headerCssClass} .product-menu.navbar-nav > li:hover > a, 
		.${component.headerCssClass} .product-menu.navbar-nav > li.hover > a, 
		.${component.headerCssClass} .product-menu.navbar-nav li ul {
			background: ${component.subMenuBackgroundColor};
		}
	</c:if>
</style>

<div class="product-header ${component.headerCssClass}">
	<div class="container">
		<div class="row">
			<div class="navbar-header col-sm-3">
				<c:url value="/" var="homepageUrl"/>
				<button data-toggle="collapse" data-target="#search-bar" class="search-button visible-xs" type="button"></button>
				<button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="collapsed menu-button visible-xs" type="button"></button>
				<cms:pageSlot position="BrandLogo" var="brandLogoComponent" element="div" class="logo">
					<cms:component component="${brandLogoComponent}" />
				</cms:pageSlot>
			</div>
			<div class="navbar-collapse collapse col-sm-9" id="navbar">
				<div class="social-links nav navbar-nav navbar-right">
				   <cms:pageSlot position="SocialLinksSlot" var="components">
                       <cms:component component="${components}"/>
                   </cms:pageSlot> 
				</div>
				<ul class="product-menu nav navbar-nav navbar-right">
					<cms:component component="${component.productsMenuNavigationBarCollection}"/>
				</ul>
    		</div>
		</div>
	</div>
</div>
