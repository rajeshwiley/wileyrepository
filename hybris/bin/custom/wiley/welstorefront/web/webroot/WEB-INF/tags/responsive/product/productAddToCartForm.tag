<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>

<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ attribute name="productTemplate" required="false" type="java.lang.String" %>
<%@ attribute name="formIndex" required="false" type="java.lang.String" %>

<c:set var="formIndex" value="${empty formIndex ? '0' : formIndex}"/>
<c:set var="isPreOrder" value="${product.lifecycleStatus eq 'PRE_ORDER'}"/>

<c:choose>
    <c:when test="${product.multidimensional}">
        <c:url var="addToCartUrl" value="/cart/addGrid" />
    </c:when>
    <c:otherwise>
        <%-- If product doesn't have variants we add this product to cart. --%>
        <c:url var="addToCartUrl" value="/cart/add" />
    </c:otherwise>
</c:choose>


<form:form method="post" id="addToCartForm-${formIndex}" action="${addToCartUrl}" data-index="${formIndex}" data-base-product-code="${product.code}">
    <c:choose>
        <c:when test="${productTemplate == 'cfa'}">
            <product:productAddToCartAddonsBlock product="${product}" productTemplate="${productTemplate}" formIndex="${formIndex}"/>
            <div class="cart-box">
                <div class="container">
                    <div class="box">
                        <div>
                            <div class="price" id="product-price-${formIndex}">
                                <format:price priceData="${product.price}" displayShortFormat="true"/>
                            </div>
                            <div class="text">
                                <product:productPreOrderSelectorBlock product="${product}" formIndex="${formIndex}"/>
								<product:productAddToCartMainBlock product="${product}" productTemplate="${productTemplate}" formIndex="${formIndex}"/>
								<product:productAddToCartDiscountBlock product="${product}" productTemplate="${productTemplate}" formIndex="${formIndex}"/>
                            </div>
                        </div>
                        <div class="cart-box-buttons">
                            <button type="submit" class="js-add-to-cart" disabled="disabled">
                               <spring:theme code="${product.lifecycleStatus eq 'PRE_ORDER' ? 'basket.preorder.add.to.basket' : 'basket.add.to.basket' }" />
                            </button>
                            <c:if test="${not empty product.freeTrialUrl}">
                            	<c:url value="${product.freeTrialUrl}" var="freeTrialUrl"/>
								<a href="${freeTrialUrl}"><spring:theme code="pdp.cfa.freetrial.button.label"/></a>
							</c:if>
                        </div>
                    </div>
                </div>
            </div>
        </c:when>
        <c:otherwise>
            <div class="col-md-8 col-sm-7">
                <product:productPreOrderSelectorBlock product="${product}" formIndex="${formIndex}" />
                <product:productAddToCartMainBlock product="${product}" productTemplate="${productTemplate}" formIndex="${formIndex}"/>
                <product:productAddToCartDiscountBlock product="${product}" productTemplate="${productTemplate}" formIndex="${formIndex}"/>
                <product:productAddToCartAddonsBlock product="${product}" productTemplate="${productTemplate}" formIndex="${formIndex}"/>
            </div>
            <div class="col-md-4 col-sm-5 text-right pdp-addtocart-container">
                <p class="price" id="product-price-${formIndex}"><format:price priceData="${totalPrice}" displayShortFormat="true"/></p>
                <button type="submit" class="btn btn-primary js-add-to-cart ${isPreOrder ? 'pdp-addtocart-button' : ''}" disabled="disabled">
                    <div class="pdp-addtocart-div">
                        <spring:theme code="${isPreOrder ? 'basket.preorder.add.to.basket' : 'basket.add.to.basket' }" />
                    </div>
                </button>
				<%-- Free Trial button for non-CFA pages is rendered in productDetailsPanel.tag --%>
            </div>
        </c:otherwise>
    </c:choose>
</form:form>


