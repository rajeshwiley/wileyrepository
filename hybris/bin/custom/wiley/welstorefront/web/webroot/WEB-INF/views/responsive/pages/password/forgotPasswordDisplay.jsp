<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="captcha" tagdir="/WEB-INF/tags/addons/wileyrecaptchaaddon/responsive/captcha"%>

<h1 class="page-title">
    <spring:theme code="forgottenPwd.caption" />
</h1>

<div class="site-form forgot-password-form">
    <div class="form-title">
        <spring:theme code="forgottenPwd.subcaption" />
    </div>
    <div class="form-description">
        <p><spring:theme code="forgottenPwd.description" /></p>
        <p><spring:theme code="forgottenPwd.description2" /></p>
    </div>
    <form:form method="post" commandName="forgottenPwdForm">
        <div class="form-cell">
            <formElement:formInputBox idKey="forgottenPwd.email" labelKey="forgottenPwd.email" path="email" type="text"
                                      mandatory="true" labelCSS="form-label" />
        </div>
        <div class="form-controls">
            <captcha:captcha publicKey="${requestScope.recaptchaPublicKey}"/>
            <div class="form-cell">
                <button class="button button-submit form-button" type="submit">
                    <spring:theme code="forgottenPwd.submit" />
                </button>
            </div>
        </div>
    </form:form>
</div>