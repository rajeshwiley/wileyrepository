<%@ page trimDirectiveWhitespaces="true" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
	<h2 class="title">Your choice</h2>
	<c:forEach items="${partnerProducts}" var="product" varStatus="loop">
		<c:if test="${loop.index % 2 == 0}"><div class="row"> </c:if>
              <div class="col-sm-6 col-xs-12">
					<div class="product-info-section">
						<h3>${product.name}</h3>
						<ul>
							<c:forEach items="${product.includeFeatures}" var="feature">
								<product:productIncludeItem feature="${feature}"/>
							</c:forEach>
						</ul>
					</div>
						<div class="product-checkout-section">
                                <div class="product-checkout-header">Checkout</div>
                                <div class="product-checkout-content">
                                  <product:partnerProductAddToCartForm product="${product}" formIndex="${loop.index}"/>
                                </div>
                           </div>
                      </div>
              <c:if test="${loop.index % 2 != 0 or loop.last}"></div> </c:if>
     </c:forEach>
