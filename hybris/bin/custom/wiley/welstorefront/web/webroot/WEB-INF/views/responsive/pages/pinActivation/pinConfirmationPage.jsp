<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<template:page pageTitle="${pageTitle}" containerCSSClass="container confirmation-pin">

    <ul class="progress-line">
        <li class="step step-completed"><span class="step-number">1</span><span class="step-name"><spring:theme code="pinActivation.breadcrumb.step.one"/></span></li>
        <li class="step step-completed"><span class="step-number">2</span>
            <span class="step-name step-name--desktop"><spring:theme code="pinActivation.breadcrumb.step.two"/></span>
            <span class="step-name step-name--mobile"><spring:theme code="pinActivation.breadcrumb.step.two.mobile"/></span>
        </li>
        <li class="step step-completed"><span class="step-number">3</span><span class="step-name"><spring:theme code="pinActivation.breadcrumb.step.three"/></span></li>
    </ul>

    <cms:pageSlot position="TopContent" var="contactInfoComponent">
        <cms:component component="${contactInfoComponent}"/>
    </cms:pageSlot>

    <cms:pageSlot position="PinConfirmation" var="pinConfirmationComponent">
        <cms:component component="${pinConfirmationComponent}"/>
    </cms:pageSlot>

    <form method="get" action="<spring:url value="/my-account"/>">
        <div class="form-buttons">
            <button class="button form-button myaccount-button"><spring:theme code="pinActivation.gotomyaccountbutton"/></button>
        </div>
    </form>

</template:page>