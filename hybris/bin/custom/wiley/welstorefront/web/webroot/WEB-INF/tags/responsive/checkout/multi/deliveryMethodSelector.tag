<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="deliveryMethods" required="true" type="java.util.List" %>
<%@ attribute name="selectedDeliveryMethodId" required="false" type="java.lang.String" %>
<%@ attribute name="tabindex" required="false" type="java.lang.Integer"%>
<%@ attribute name="tabIndexStartFrom" required="false" type="java.lang.Integer"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<c:if test="${deliveryMethods != null}">
	<div class="form-cell">
		<div class="form-label required">Shipping Method</div>
		<div id="delivery-method-selector" class="form-field">
			<c:forEach items="${deliveryMethods}" var="deliveryMethod" varStatus="loop">
				<multi-checkout:deliveryMethodDetails deliveryMethod="${deliveryMethod}" isSelected="${deliveryMethod.code eq selectedDeliveryMethodId}" tabindex="${tabIndexStartFrom + loop.index}"/>
			</c:forEach>
		</div>
		<script id="deliveryMethodTemplate" type="text/html">
            {{#deliveryModels}}
				<input type="radio" name="delivery_method" id="shipping-{{code}}" value="{{code}}" tabindex="{{#getTabIndex}}${tabIndexStartFrom}{{/getTabIndex}}" {{#checked}}checked{{/checked}}/>
				<label for="shipping-{{code}}">{{name}}</label>
            {{/deliveryModels}}
		</script>
	</div>
</c:if>