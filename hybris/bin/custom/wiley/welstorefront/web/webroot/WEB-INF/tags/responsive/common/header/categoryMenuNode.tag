<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/responsive/common/header"  %>
<%@ attribute name="navigationNode" type="de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel" required="true"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<c:if test="${not empty navigationNode.links}">
	<c:forEach items="${navigationNode.links}" var="link">
		<c:if test="${ycommerce:evaluateRestrictions(link)}">	
			<li class="${(not empty navigationNode.children)? 'sub-menu' : ''}">
				<cms:component component="${link}" evaluateRestriction="false" />
	
			<c:if test="${empty navigationNode.children}">
				</li>
			</c:if>
		</c:if>
	</c:forEach>
</c:if>

<c:if test="${not empty navigationNode.children}">
	<ul>
	<c:forEach items="${navigationNode.children}" var="childNavigationNode">
		<c:if test="${childNavigationNode.visible}">
			<header:categoryMenuNode navigationNode="${childNavigationNode}"/>
		</c:if>
	</c:forEach>
	</ul>

    <c:if test="${not empty navigationNode.links}">
        </li>
    </c:if>
</c:if>
