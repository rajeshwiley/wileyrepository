<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="orderData" required="true" type="de.hybris.platform.commercefacades.order.data.OrderData" %>
<%@ attribute name="showTax" required="false" type="java.lang.Boolean" %>

<%-- 
	there is no guest confirmation page for wel. Tag has been added to avoid error on the paypaladdon jsp:
	web/webroot/WEB-INF/views/addons/paypaladdon/responsive/pages/checkout/checkoutConfirmationPage.jsp
--%>