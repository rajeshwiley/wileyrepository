<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="rootNode" required="true" type="com.wiley.facades.product.node.ProductNode" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ attribute name="formIndex" required="false" %>

<c:set var="code" value="${rootNode.code}"/>
<c:if test="${code eq ''}">
    <c:set var="code" value="root"/>
</c:if>
<c:if test="${rootNode.nodes != null && fn:length(rootNode.nodes) > 0}">
    <div id="variants-section-${code}-${formIndex}" class="variants-section">
        <c:forEach items="${rootNode.nodes}" var="node" varStatus="loop">
            <c:set var="visibleStyle" value="${node.viewVisible ? '' : 'display: none' }"/>
            <c:set var="checked" value="${(loop.index == 0 || node.viewType == 'checkbox') && node.viewVisible ? 'checked' : ''}"/>

            <input type="${node.viewType}" name="${node.viewName}" id="product-variant-${node.code}-${formIndex}" data-code="${node.code}" value="${node.value.sku}" ${checked}/>
            <label for="product-variant-${node.code}-${formIndex}" style="${visibleStyle}">${node.name}</label>
        </c:forEach>
        <product:productVariantsSection rootNode="${rootNode.nodes[0]}" formIndex="${formIndex}"/>
    </div>
</c:if>
