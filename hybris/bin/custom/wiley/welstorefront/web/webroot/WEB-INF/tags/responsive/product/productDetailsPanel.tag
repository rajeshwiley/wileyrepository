<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%-- TODO: Rating block need to implement & if not Product code need to delete it --%>
<div class="product-details">
	<ycommerce:testId code="productDetails_productNamePrice_label_${product.code}">
		<h1 class="name">${product.name} <%-- <span class="sku">ID ${product.code}</span> --%> </h1>
	</ycommerce:testId>
	<%--
	<product:productReviewSummary product="${product}" showLinks="true"/>
	--%>
	<div class="row">
		<div class="col-sm-6">
			<product:productImagePanel galleryImages="${galleryImages}" product="${product}"/>

			<product:productPromotionSection product="${product}"/>

			<%-- TODO: Gift card promotion block need to implement --%>
			<%--<div class="gift-card">
				<div class="gift-card-image">
					<img src="/welstorefront/_ui/responsive/theme-blue/images/apply-gift-card-purple.jpg">
				</div>
				<div class="gift-card-text">
					With purchase of our Platinum course get a free $299 gift card!
				</div>
			</div>--%>

		</div>
		<div class="col-sm-6">
			<div class="row">
				<div class="col-md-12">
					<div>
						<div class="description">
							<c:forEach items="${product.details}" var="medialink">
								<a href="${medialink.url}">${medialink.name}</a><br>
							</c:forEach> 
							${product.description}
							<a href="#learnmore">
								<spring:theme code="text.learn.more"/>
							</a>
						</div>

						<product:productIncludesSection product="${product}"/>

					</div>
				</div>

				<div class="col-md-12">
					<div class="row">
						<product:productAddToCartForm product="${product}"/>
					</div>
				</div>

                <c:if test="${not empty product.freeTrialUrl}">
                 	<c:url value="${product.freeTrialUrl}" var="freeTrialUrl"/>
					<div class="col-md-12">
						<div class="free-trial">
							<form action="${freeTrialUrl}" method="get">
								<c:choose>
									<c:when test="${not empty product.freeTrialButtonLabel}"> 
										<c:set var="freeTrialButtonLabel" value="${product.freeTrialButtonLabel}"/>
									</c:when>
									<c:otherwise>
										<%-- When product configured incorrectly - show constatnt label --%>
										<c:set var="freeTrialButtonLabel">
											<spring:theme code="pdp.freetrial.button.label.default"/>
										</c:set>
									</c:otherwise>
								</c:choose>
								<button class="free-trial-button">${freeTrialButtonLabel}</button>
							</form>
						</div>
					</div>
				</c:if>
				
			</div>
		</div>

	</div>
</div>

