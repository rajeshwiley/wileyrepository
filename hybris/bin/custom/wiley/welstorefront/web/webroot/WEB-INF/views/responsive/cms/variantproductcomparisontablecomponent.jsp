<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="component" tagdir="/WEB-INF/tags/shared/component"%>


<c:if test="${productComparisonTableTitle ne null}">
	<h2 class="comparison-table-title">${productComparisonTableTitle}</h2>
</c:if>

<c:set var="products" value="${productComparisonData.products}" />
<spring:theme code="product.list.table.learn.more" var="learnMore" />
<spring:theme code="product.list.wileys" var="wileys" />

<div class="comparison-table-cfa comparison-table-cfa-mobile">
	<c:forEach items="${products}" var="product" varStatus="status">
		<c:url value="${product.url}" var="productUrl"></c:url>
		<div class="course-component-cfa">
			<div class="course-component-header">
				<div class="course-component-image">
					<c:if test="${status.index eq 0}">
						<span class="most-popular-label">
							<spring:theme code="product.list.table.most.popular" />
						</span>
					</c:if>
                    <a href="${productUrl}"> <product:productPrimaryImage product="${product}" format="thumbnail" /></a>
                </div>
                <div class="course-component-title">
					<p>${product.name}</p>
					<div class="learn-more"><a href="${productUrl}" title="${learnMore}">${learnMore}</a></div>
                    <div class="course-price">${product.price.shortFormattedValue}</div>
				</div>
			</div>
			<div class="course-component-content">
				<ul class="course-component-products">
					<c:forEach items="${productComparisonData.features}" var="feature">
						<product:productListerComparisonClassifications product="${product}" feature="${feature}" featureName="${feature.name}" mobile="true"/>
					</c:forEach>
				</ul>
			</div>
			<div class="course-component-footer">
				<div class="learn-more"><a href="${productUrl}" title="${learnMore}">${learnMore}</a></div>
			</div>
		</div>
	</c:forEach>
</div>

<div class="comparison-table-cfa comparison-table-cfa-desktop">
	<table>
		<thead>
			<tr class="cfa-thead-title">
				<th rowspan="4"></th>
			</tr>
			<tr class="cfa-thead-image">
				<c:forEach items="${products}" var="product" varStatus="status">
					<th>
						<c:if test="${status.index eq 0}">
							<span class="most-popular-label">
								<spring:theme code="product.list.table.most.popular" />
							</span>
						</c:if>
						<c:url value="${product.url}" var="productImageUrl"></c:url>
						<a href="${productImageUrl}"> <product:productPrimaryImage product="${product}" format="thumbnail" /></a>
					</th>
				</c:forEach>
			</tr>
			<tr class="cfa-thead-courses">
				<c:forEach items="${products}" var="product">
					<th>
						<c:url value="${product.url}" var="productNameUrl"></c:url>
						<p><a href="${productNameUrl}">${wileys}${product.name}</a></p>
						<a href="${productNameUrl}" title="${learnMore}" class="learn-more">${learnMore}</a>
					</th>
				</c:forEach>
			</tr>
			<tr class="cfa-thead-price">
				<c:forEach items="${products}" var="product">
					<th>${product.price.shortFormattedValue}</th>
				</c:forEach>
			</tr>
		</thead>

		<tbody>
			<c:forEach items="${productComparisonData.features}" var="feature">
				<tr>
					<td>${feature.displayName}
						<c:if test="${not empty feature.tooltip}">
							<span class="tooltipHelp" data-toggle="popover" data-content='${feature.tooltip}'>?</span>
						</c:if>
					</td>
					<c:forEach items="${products}" var="product">
						<td>
							<product:productListerComparisonClassifications product="${product}" feature="${feature}" />
						</td>
					</c:forEach>
				</tr>
			</c:forEach>
		</tbody>

		<tfoot>
			<tr>
				<td/>
				<c:forEach items="${products}" var="product">
					<td>
						<c:url value="${product.url}" var="productLearnMoreUrl" />
						<a href="${productLearnMoreUrl}" title="${learnMore}" class="learn-more">${learnMore}</a>
					</td>
				</c:forEach>
			</tr>
		</tfoot>
	</table>
</div>