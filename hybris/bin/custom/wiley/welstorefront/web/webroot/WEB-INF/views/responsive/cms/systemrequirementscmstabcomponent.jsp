<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:if test="${not empty content}">
	<div class="tabhead">
        <span class="glyphicon"></span><a href="">${title}</a>
	</div>
	<div class="tabbody">${content}</div>
</c:if>