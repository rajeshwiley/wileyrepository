<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="partner" tagdir="/WEB-INF/tags/responsive/partner"%>
<template:page pageTitle="${pageTitle}" hideHeader="true" containerCSSClass="container kpmg-page">
	<cms:pageSlot position="TopHeaderSlot" var="feature">
        <cms:component component="${feature}" />
    </cms:pageSlot>
    <div class="row">
        <div class="col-md-12">
            <cms:pageSlot position="PartnerTitle" var="feature">
                <cms:component component="${feature}" />
            </cms:pageSlot>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-md-push-8 col-sm-4 col-sm-push-8 col-xs-12 ">
            <div class="kpmg-img">
                <cms:pageSlot position="PartnerImage" var="feature">
                    <cms:component component="${feature}" />
                </cms:pageSlot>
            </div>
        </div>
        <div class="col-md-8 col-md-pull-4 col-sm-8 col-sm-pull-4 col-xs-12">
            <div class="kpmg-description">
                <cms:pageSlot position="PartnerCategoryDescription" var="feature">
                    <cms:component component="${feature}" />
                </cms:pageSlot>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 top-option">
            <cms:pageSlot position="TopOptionsDescription" var="feature">
                <cms:component component="${feature}" />
            </cms:pageSlot>
        </div>
    </div>
    <div class="row">
        <cms:pageSlot position="PartnerEnroll" var="feature">
            <cms:component component="${feature}" />
        </cms:pageSlot>
    </div>
    <div class="row">
        <div class="col-md-12 top-option">
            <cms:pageSlot position="BottomOptionsDescription" var="feature">
                <cms:component component="${feature}" />
            </cms:pageSlot>
        </div>
    </div>
    <cms:pageSlot position="PartnerProduct" var="feature">
        <cms:component component="${feature}" />
    </cms:pageSlot>
    <div class="content">
        <div class="tabs js-tabs tabs-responsive tabamount5">
            <div class="tabhead first active">
                <span class="glyphicon"></span> <a href=""><spring:theme code="product.product.spec" /></a>
            </div>
            <div class="tabbody">
                <partner:dedicatedPartnerClassifications />
            </div>
            <cms:pageSlot position="ProductTabsContainer" var="feature">
                <cms:component component="${feature}" />
            </cms:pageSlot>
        </div>
    </div>

</template:page>