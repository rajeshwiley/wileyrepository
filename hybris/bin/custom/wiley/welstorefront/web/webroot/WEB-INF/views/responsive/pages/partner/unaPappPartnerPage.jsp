<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<template:page pageTitle="${pageTitle}" containerCSSClass="container una-page">
    <div class="row">
        <div class="col-sm-3">
            <div class="una-left-side">
                <cms:pageSlot position="PartnerSideContent" var="feature">
                    <cms:component component="${feature}" />
                </cms:pageSlot>
            </div>
        </div>
        <div class="col-sm-9">
            <cms:pageSlot position="PartnerTitle" var="feature">
                <cms:component component="${feature}" />
            </cms:pageSlot>
            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <div class="una-description-content">
                        <cms:pageSlot position="PartnerTopDescription" var="feature">
                            <cms:component component="${feature}" />
                        </cms:pageSlot>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <cms:pageSlot position="PartnerProductList" var="feature">
                        <cms:component component="${feature}" />
                    </cms:pageSlot>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="una-middle-content">
                        <cms:pageSlot position="PartnerBottomDescription" var="feature">
                            <cms:component component="${feature}" />
                        </cms:pageSlot>
                    </div>
                </div>
            </div>
        </div>
    </div>

</template:page>

