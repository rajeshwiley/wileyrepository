<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ attribute name="feature" required="true" type="de.hybris.platform.commercefacades.product.data.FeatureData" %>

            
<li>${feature.name}
<c:if test="${not empty feature.tooltip}">
            	&nbsp<span class="tooltipHelp" data-toggle="popover" data-content='${feature.tooltip}'>?</span>
</c:if>
 <c:if test="${not empty feature.document}">
  	&nbsp-&nbsp<a href="${feature.document.url}"  target="_blank" ><spring:message code="product.see.attachment" text="See attachement"/></a>
 </c:if>
 </li>
