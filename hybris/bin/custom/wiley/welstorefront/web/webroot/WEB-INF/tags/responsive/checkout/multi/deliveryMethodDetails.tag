<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="deliveryMethod" required="true" type="de.hybris.platform.commercefacades.order.data.DeliveryModeData" %>
<%@ attribute name="isSelected" required="false" type="java.lang.Boolean" %>
<%@ attribute name="tabindex" required="false" type="java.lang.Integer"%>

<input type="radio" name="delivery_method" id="shipping-${deliveryMethod.code}" tabindex="${tabindex}"  value="${deliveryMethod.code}" ${isSelected ? 'checked' : ''}/>
<label for="shipping-${deliveryMethod.code}">${deliveryMethod.name}</label>