<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>

<%@ attribute name="product" required="true"
	type="de.hybris.platform.commercefacades.product.data.ProductData"%>
<%@ attribute name="formIndex" required="false"%>

<c:choose>
	<c:when test="${product.multidimensional}">
		<c:url var="addToCartUrl" value="/cart/addGrid" />
	</c:when>
	<c:otherwise>
		<%-- If product doesn't have variants we add this product to cart. --%>
		<c:url var="addToCartUrl" value="/cart/add" />
	</c:otherwise>
</c:choose>

<form:form method="post" id="addToCartForm-${formIndex}"
	action="${addToCartUrl}" data-index="${formIndex}"
	data-base-product-code="${product.code}">
	<c:choose>
		<c:when test="${product.multidimensional}">
			<product:productVariantSelector product="${product}" isVisible="true"
				productRootNode="${productRootNodesMap[product.code]}"
				productRootNodeJson="${productRootNodesJsonMap[product.code]}"
				formIndex="${formIndex}" />
		</c:when>
		<c:otherwise>
			<%-- If product doesn't have variants we add this product to cart. --%>
			<input type="hidden" name="productCodePost"
				id="product-single-${product.code}-${formIndex}"
				value="${product.code}" data-price="${product.price.value}"
				data-partner-price="${product.partnerPrice.value}" />
		</c:otherwise>
	</c:choose>

	<div class="product-price-section">
		<c:choose>
			<c:when
				test="${partnerName ne null && product.hasPartnerDiscount}">
				<p class="price stating-price">
					<spring:theme code="product.price.afterDiscount"
						arguments="${partnerName}" />
					<span id="partner-price-${formIndex}"> <format:price
							priceData="${product.partnerPrice}" displayShortFormat="true"/>
					</span>
				</p>
                <c:if test="${showPriceBeforeDiscountLine}">
				<p class="price price-before-coupon">
					<spring:theme code="product.price.beforeDiscount" />
					<span id="product-price-${formIndex}"> <format:price
							priceData="${product.price}" displayShortFormat="true" displayFreeForZero="true"/>
					</span>
				</p>
               </c:if>
			</c:when>
			<c:otherwise>
				<p class="price price-before-coupon">
					<span id="product-price-${formIndex}"> <format:price
							priceData="${product.price}" displayShortFormat="true" displayFreeForZero="true"/>
					</span>
				</p>
			</c:otherwise>
		</c:choose>
		<button type="submit" class="btn btn-primary js-add-to-cart">
			<spring:theme code="basket.add.to.basket" />
		</button>
	</div>

	<input type="hidden" id="partnerId" name="partnerId"
		value="${partnerId}" />

</form:form>
