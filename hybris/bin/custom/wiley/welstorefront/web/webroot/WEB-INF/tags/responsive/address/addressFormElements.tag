<%@ attribute name="tabindex" required="false" type="java.lang.Integer"%>
<%@ attribute name="subpath" required="false" type="java.lang.String"%>
<%@ attribute name="countries" required="true" type="java.util.List"%>
<%@ attribute name="formKey" required="true" type="de.hybris.platform.acceleratorstorefrontcommons.forms.AddressForm"%>
<%@ attribute name="regions" required="false" type="java.util.List"%>
<%@ attribute name="isFormVisible" required="false" type="java.lang.Boolean"%>
<%@ attribute name="tabIndexStartFrom" required="false" type="java.lang.Integer"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<c:set var="countryHasRegionsVar" value="${not empty regions}"/>
<div id="${subpath}form"  style="${isFormVisible eq 'true' ? '':'display:none'}">
<form:hidden path="${subpath}addressId" />
<input type="hidden" id="${subpath}customerHasSavedAddress" value="${not empty formKey.addressId}"/>
<input type="hidden" id="countryHasRegions" value="${countryHasRegionsVar}"/>

<c:choose>
    <c:when test="${countryHasRegionsVar}">
        <div class="form-cell">
            <formElement:formInputBox idKey="${subpath}addressFirstName" labelKey="address.firstName"
                                      labelCSS="form-label" isRequred="true" adjustToTwoColLayout="true"
                                      path="${subpath}firstName" mandatory="true" tabindex="${tabIndexStartFrom +1}"/>
        </div>
        <div class="form-cell">
            <formElement:formInputBox idKey="${subpath}addressSurname" labelKey="address.surname" path="${subpath}lastName"
                                      labelCSS="form-label" isRequred="true" adjustToTwoColLayout="true" mandatory="true"
                                      tabindex="${tabIndexStartFrom +2}"/>
        </div>

			  <div id="${subpath}countrySelector" class="form-cell">
				<formElement:formSelectBox idKey="${subpath}address.country" labelCSS="form-label" isRequred="true"
					adjustToTwoColLayout="true" labelKey="address.country" path="${subpath}countryIso" mandatory="true"
					skipBlank="true" skipBlankMessageKey="address.selectCountry"
					items="${countries}" itemValue="isocode" tabindex="${tabIndexStartFrom +3}" />
			  </div>
			  <div class="form-cell">
				<formElement:formInputBox idKey="${subpath}addressLine1" labelKey="address.address1" path="${subpath}line1"
					labelCSS="form-label" isRequred="true" adjustToTwoColLayout="true" mandatory="true" tabindex="${tabIndexStartFrom +4}" />
			  </div>

        <div class="form-cell">
            <formElement:formInputBox idKey="${subpath}addressLine2" labelKey="address.address2" path="${subpath}line2"
                                      labelCSS="form-label" adjustToTwoColLayout="true" mandatory="false" tabindex="${tabIndexStartFrom +5}"/>
        </div>

        <div class="form-cell">
            <formElement:formInputBox idKey="${subpath}addressTownCity" labelKey="address.townCity" path="${subpath}townCity"
                                      labelCSS="form-label" isRequred="true" adjustToTwoColLayout="true" mandatory="true"  tabindex="${tabIndexStartFrom +6}"/>
        </div>

        <div class="form-cell">
            <formElement:formSelectBox idKey="${subpath}addressRegion" labelKey="address.state" path="${subpath}regionIso" mandatory="true"
                                       skipBlank="false"
                                       labelCSS="form-label" isRequred="true" adjustToTwoColLayout="true"
                                       items="${regions}" itemValue="${useShortRegionIso ? 'isocodeShort' : 'isocode'}"
                                       selectedValue="${wileyAddressForm.regionIso}"  tabindex="${tabIndexStartFrom +7}"/>
        </div>

        <div class="form-cell">
            <formElement:formInputBox idKey="${subpath}addressPostcode" labelKey="address.zipcode" path="${subpath}postcode"
                                      labelCSS="form-label" isRequred="true" adjustToTwoColLayout="true" mandatory="true"  tabindex="${tabIndexStartFrom +8}"/>
        </div>

        <div class="form-cell">
            <formElement:formInputBox idKey="${subpath}addressPhone" labelKey="address.phone" path="${subpath}phone"
                                      labelCSS="form-label" isRequred="true" adjustToTwoColLayout="true" mandatory="true"  tabindex="${tabIndexStartFrom +9}"/>
        </div>

    </c:when>
    <c:otherwise>
        <div class="form-cell">
            <formElement:formInputBox idKey="${subpath}addressFirstName" labelKey="address.firstName"
                                      labelCSS="form-label" isRequred="true" adjustToTwoColLayout="true"
                                      path="${subpath}firstName" mandatory="true" tabindex="${tabIndexStartFrom +1}"/>
        </div>
        <div class="form-cell">
            <formElement:formInputBox idKey="${subpath}addressSurname" labelKey="address.surname" path="${subpath}lastName"
                                      labelCSS="form-label" isRequred="true" adjustToTwoColLayout="true" mandatory="true"
                                      tabindex="${tabIndexStartFrom +2}"/>
        </div>

        <div id="countrySelector" class="form-cell">
            <formElement:formSelectBox idKey="${subpath}address.country" labelCSS="form-label" isRequred="true"
                                       adjustToTwoColLayout="true" labelKey="address.country" path="${subpath}countryIso"
                                       mandatory="true"
                                       skipBlank="true" skipBlankMessageKey="address.selectCountry"
                                       items="${countries}" itemValue="isocode" tabindex="${tabIndexStartFrom +3}"/>
        </div>
        <div class="form-cell">
            <formElement:formInputBox idKey="${subpath}addressLine1" labelKey="address.address1" path="${subpath}line1"
                                      labelCSS="form-label" isRequred="true" adjustToTwoColLayout="true" mandatory="true"
                                      tabindex="${tabIndexStartFrom +4}"/>
        </div>

        <div class="form-cell">
            <formElement:formInputBox idKey="${subpath}addressLine2" labelKey="address.address2" path="${subpath}line2"
                                      labelCSS="form-label" adjustToTwoColLayout="true" mandatory="false" tabindex="${tabIndexStartFrom +5}"/>
        </div>

        <div class="form-cell">
            <formElement:formInputBox idKey="${subpath}addressCityProvince" labelKey="address.cityProvince" path="${subpath}townCity"
                                      labelCSS="form-label" isRequred="true" adjustToTwoColLayout="true" mandatory="true"  tabindex="${tabIndexStartFrom +6}"/>
        </div>

        <div class="form-cell">
            <formElement:formInputBox idKey="${subpath}addressPostcode" labelKey="address.postalcode" path="${subpath}postcode"
                                      labelCSS="form-label" isRequred="true" adjustToTwoColLayout="true" mandatory="true" tabindex="${tabIndexStartFrom +7}"/>
        </div>

        <div class="form-cell">
            <formElement:formInputBox idKey="${subpath}addressPhone" labelKey="address.phone" path="${subpath}phone"
                                      labelCSS="form-label" isRequred="true" adjustToTwoColLayout="true" mandatory="true"  tabindex="${tabIndexStartFrom +8}"/>
        </div>
    </c:otherwise>
</c:choose>
</div>	
	
