<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<c:if test="${not empty cartData.appliedOrderPromotions}">
    <ycommerce:testId code="cart_recievedPromotions_labels">
        <c:forEach items="${cartData.appliedOrderPromotions}" var="promotion">
            <div class="discount-code-message"><p>${promotion.description}</p></div>
        </c:forEach>
    </ycommerce:testId>
</c:if>
