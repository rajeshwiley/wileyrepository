<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:set value="false" var="displayPartnerTitle"/>
<c:set value="false" var="displayPartnerContent"/>
<c:set value="false" var="displayPartnerImage"/>

<cms:pageSlot position="PartnerTitle" var="feature">
	<c:set value="true" var="displayPartnerTitle"/>
</cms:pageSlot>
<cms:pageSlot position="PartnerSelectionBar" var="feature">
	<c:set value="true" var="displayPartnerContent"/>
</cms:pageSlot>
<cms:pageSlot position="PartnerCategoryDescription" var="feature">
	<c:set value="true" var="displayPartnerContent"/>
</cms:pageSlot>
<cms:pageSlot position="PartnerImage" var="feature">
	<c:set value="true" var="displayPartnerImage"/>
</cms:pageSlot>

<template:page pageTitle="${pageTitle}" containerCSSClass="container unified-partner">
    <div class="content">
        <div class="tabs links-tabs">
            <cms:pageSlot position="PartnerCategorySelectionBar" var="feature">
                <cms:component component="${feature}" />
            </cms:pageSlot>
            <c:if test="${displayPartnerTitle || displayPartnerContent || displayPartnerImage}">
				<div class="tabbody links-tabs-content">
					<div class="welcome-partners">
						<c:if test="${displayPartnerTitle}">
							<cms:pageSlot position="PartnerTitle" var="feature" element="h2" class="title welcome-partners-title">
								<cms:component component="${feature}" />
							</cms:pageSlot>
						</c:if>
						<c:if test="${displayPartnerContent || displayPartnerImage}">
							<div class="row welcome-partners-row">
								<cms:pageSlot position="PartnerImage" var="feature" element="div" class="welcome-partners-image col-sm-5 col-xs-12">
									<cms:component component="${feature}" />
								</cms:pageSlot>
								<c:if test="${displayPartnerContent}">
									<div class="welcome-partners-content col-sm-7 col-xs-12">
										<cms:pageSlot position="PartnerSelectionBar" var="feature" element="div" class="partner-chooser">
											<cms:component component="${feature}" />
										</cms:pageSlot>
										<cms:pageSlot position="PartnerCategoryDescription" var="feature" element="div" class="welcome-partners-description">
											<cms:component component="${feature}" />
										</cms:pageSlot>
									</div>
								</c:if>
							</div>
						</c:if>
					</div>
				</div>
			</c:if>
			<cms:pageSlot position="PartnerProductList" var="feature" element="div" class="product-detailed-info">
				<cms:component component="${feature}" />
			</cms:pageSlot>
        </div>
        <div class="product-detailed-info">
                    <cms:pageSlot position="SpecialPartnerProductList" var="feature">
                        <cms:component component="${feature}" />
                    </cms:pageSlot>
        </div>
        <product:productPageTabs />
        <cms:pageSlot position="PartnerFooter" var="feature">
            <cms:component component="${feature}" />
        </cms:pageSlot>
    </div>

</template:page>