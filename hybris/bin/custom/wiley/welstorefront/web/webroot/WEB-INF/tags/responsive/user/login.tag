<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="actionNameKey" required="true" type="java.lang.String"%>
<%@ attribute name="action" required="true" type="java.lang.String"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>

<c:set var="hideDescription" value="checkout.login.loginAndCheckout" />


    <div role="tab" id="headingTwo" class="panel-heading">
        <div class="form-title">
            <spring:theme code="login.title" />
            <a role="button" data-parent="#accordion" data-toggle="collapse" href="#collapseTwo" aria-expanded="false"
                aria-controls="collapseTwo" class="collapsed form-title-toggle"></a>
        </div>
    </div>
    <div id="collapseTwo" role="tabpanel" aria-labelledby="headingTwo" class="panel-collapse collapse">
        <div class="panel-body create-account">
            <form:form action="${action}" method="post" commandName="loginForm" modelAttribute="loginForm">
                <div class="row">
                    <div class="col-sm-6 col-md-6 form-cell">
                        <div class="form-field">
                            <formElement:formInputBox idKey="j_username" labelKey="login.email"
                                labelCSS="form-label required" path="j_username" inputCSS="text" mandatory="true" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-md-6 form-cell">
                        <div class="form-field">
                            <formElement:formPasswordBox idKey="j_password" labelKey="login.password"
                                labelCSS="form-label required" path="j_password" inputCSS="text password"
                                mandatory="true" />
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-required mobile">Required</div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-12 form-buttons">
                        <c:if test="${not empty forgotPasswordRedirectType}">
                            <c:set var="forgotPwdRedirectPath" value="?t=${forgotPasswordRedirectType}" />
                        </c:if>
                        <a href="<c:url value='/login/pw/request${forgotPwdRedirectPath}'/>" class="recover-password pull-left">
                            <spring:theme code="login.link.forgottenPwd" />
                        </a>
                        <button type="submit" class="button form-button pull-right">
                            <spring:theme code="${actionNameKey}" />
                        </button>
                    </div>
                </div>
            </form:form>
        </div>
    </div>