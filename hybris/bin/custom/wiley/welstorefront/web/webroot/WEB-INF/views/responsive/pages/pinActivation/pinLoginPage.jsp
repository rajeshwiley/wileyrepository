<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<template:page pageTitle="${pageTitle}" hideHeaderLinks="true" containerCSSClass="container login-page" hideActivatePinLink="true" hideLoginLink="true">

    <ul class="progress-line">
        <li class="step step-completed"><span class="step-number">1</span><span class="step-name"><spring:theme code="pinActivation.breadcrumb.step.one"/></span></li>
        <li class="step step-completed"><span class="step-number">2</span>
            <span class="step-name step-name--desktop"><spring:theme code="pinActivation.breadcrumb.step.two"/></span>
            <span class="step-name step-name--mobile"><spring:theme code="pinActivation.breadcrumb.step.two.mobile"/></span>
        </li>
        <li class="step"><span class="step-number">3</span><span class="step-name"><spring:theme code="pinActivation.breadcrumb.step.three"/></span></li>
    </ul>

    <cms:pageSlot position="TopContentSlot" var="contactInfoComponent">
        <cms:component component="${contactInfoComponent}"/>
    </cms:pageSlot>

    <h1 class="page-title">
        <spring:theme code="register.login.new.customer" />
        <span class="tooltip-question"><spring:theme code="register.login.already.free.trial" />&nbsp;<span
            data-toggle="popover" data-content='<spring:theme code="register.login.already.free.trial.tooltip" />'
            data-original-title="" class="tooltipHelp">?</span></span>
    </h1>

    <div id="accordion" role="tablist" aria-multiselectable="false" class="row panel-group">
    <div class="col-sm-6 col-md-6 panel">
        <cms:pageSlot position="LeftContentSlot" var="feature">
            <cms:component component="${feature}" />
        </cms:pageSlot>
    </div>

    <div class="col-sm-6 col-md-6 panel">
        <cms:pageSlot position="RightContentSlot" var="feature">
            <cms:component component="${feature}" />
        </cms:pageSlot>
    </div>
</div>

<div class="form-required laptop">Required</div>
</template:page>
