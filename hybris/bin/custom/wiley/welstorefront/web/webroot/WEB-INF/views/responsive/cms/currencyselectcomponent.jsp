<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>

    <c:if test="${fn:length(currencies) > 1}">
    <c:url value="/_s/currency" var="setCurrencyActionUrl" />
        <form:form action="${setCurrencyActionUrl}" method="post" id="currency-form">

            <ycommerce:testId code="header_currency_select">
                <div class="dropdown currency-selector" id="currency-selector">
                    <button class="dropdown-toggle" type="button"
                    id="currency-selector-menu" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="true">
                        <span class="dropdown-text">${currentCurrency.isocode}&nbsp;${currentCurrency.symbol}</span>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="currency-selector-menu">
                        <c:forEach items="${currencies}" var="curr">
                            <li class="${curr.isocode == currentCurrency.isocode ? 'current' : ''}"
                                data-value="${curr.isocode}"><c:out
                                value="${curr.isocode} ${curr.symbol}"/></li>
                        </c:forEach>
                    </ul>
                </div>
                <input type="hidden" name="code" value="${currentCurrency.isocode}"/>
            </ycommerce:testId>


        </form:form>
    </c:if>
