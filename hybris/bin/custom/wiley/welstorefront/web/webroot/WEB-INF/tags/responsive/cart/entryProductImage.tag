<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="entry" required="true" type="de.hybris.platform.commercefacades.order.data.OrderEntryData"%>
<%@ attribute name="productUrl" required="true" type="java.lang.String"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="subscription-image">
	<c:forEach items="${entry.product.images}" var="image">
		<c:if test="${image.format eq 'cartIcon'}">
		   <c:choose>
			   <c:when test="${isGiftCard}">
					<img src="${image.url}" alt="${image.altText}"/>
			   </c:when>
			   <c:otherwise>
					<c:choose>
						<c:when test="${not empty productUrl}">
							<a href="${productUrl}" class="subscription-image-link">
								<img src="${image.url}" alt="${image.altText}"/>
							</a>
						</c:when>
						<c:otherwise>
							<a class="subscription-image-link">
								<img src="${image.url}" alt="${image.altText}"/>
							</a>
						</c:otherwise>
					</c:choose>
			   </c:otherwise>
		   </c:choose>
		</c:if>
	</c:forEach>
		  <c:if test="${isPreOrder}">
             <spring:theme code="checkout.PreOrder.text.message"/>
          </c:if>
</div>