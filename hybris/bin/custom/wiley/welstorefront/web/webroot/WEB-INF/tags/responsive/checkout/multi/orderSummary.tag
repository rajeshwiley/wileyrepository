<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="checkout" tagdir="/WEB-INF/tags/responsive/checkout/" %>


<spring:url value="/termsAndConditions" var="termsAndConditionsUrl"/>

<div class="order-summary-container">
	<div class="title">
		<spring:theme code="checkout.multi.order.summary"/>
	</div>
	<checkout:orderTotals cartData="${cartData}" orderDetailsInfoStyle="order-details-info order-summary-info"/>

	<div class="placeOrderButton">
		<form:form method="POST" onsubmit="ACC.common.disableButton('placeOrderButtonId')"  action="${request.contextPath}/checkout/multi/order-review/add" >
			<button id="placeOrderButtonId" type="submit" class="button form-confirmation-button"><spring:theme code="checkout.orderReview.button.placeYourOrder"/></button></span>
		</form:form>
	</div>
	<div class="termsAndConditions">
		<spring:theme code="checkout.orderReview.termsAndConditionsTemplate"/><a class="go-to-card" href="${termsAndConditionsUrl}" target="_blank"><spring:theme code="checkout.orderReview.termsAndConditions"/></a>
	</div>

</div>

