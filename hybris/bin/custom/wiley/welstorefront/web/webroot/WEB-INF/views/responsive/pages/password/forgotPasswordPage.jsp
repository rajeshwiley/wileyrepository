<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<template:page pageTitle="${pageTitle}" hideHeaderLinks="true" containerCSSClass="container login-page">
    <cms:pageSlot position="TopContent" var="feature">
        <cms:component component="${feature}" />
    </cms:pageSlot>
</template:page>