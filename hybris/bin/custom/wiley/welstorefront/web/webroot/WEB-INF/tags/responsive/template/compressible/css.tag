<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%--  AddOn Common CSS files --%>
<c:forEach items="${addOnCommonCssPaths}" var="addOnCommonCss">
	<link rel="stylesheet" type="text/css" media="all" href="${addOnCommonCss}"/>
	<link rel="stylesheet" type="text/css" href="${commonResourcePath}/css/productLayout2Page.css">
</c:forEach>

<%-- Theme CSS files --%>
<!--[if IE 9]>
    <link rel="stylesheet" type="text/css" media="all" href="${themeResourcePath}/css/ybase-ie9.css"/>
    <link rel="stylesheet" type="text/css" media="all" href="${themeResourcePath}/css/style-ie9.css"/>
<![endif]-->

<![if !IE]>
    <link rel="stylesheet" type="text/css" media="all" href="${themeResourcePath}/css/style.css"/>
<![endif]>

<%--  AddOn Theme CSS files --%>
<c:forEach items="${addOnThemeCssPaths}" var="addOnThemeCss">
	<link rel="stylesheet" type="text/css" media="all" href="${addOnThemeCss}"/>
</c:forEach>