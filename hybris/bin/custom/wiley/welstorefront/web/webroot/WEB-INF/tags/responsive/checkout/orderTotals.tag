<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>
<%@ attribute name="orderDetailsInfoStyle" required="false" type="java.lang.String"%>
<%@ attribute name="showCardEntries" required="false" type="java.lang.Boolean"%>

<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart" %>

<c:set var="orderDetailsInfoStyle" value="${(empty orderDetailsInfoStyle) ? 'order-details-info' : orderDetailsInfoStyle}" />

<div class="${orderDetailsInfoStyle}">
	<c:if test="${showCardEntries}">
		<c:forEach items="${cartData.entries}" var="entry">
			<c:remove var="productUrl"/>
			<c:if test="${! entry.product.partnerOnly}">
				<c:url value="${entry.product.url}" var="productUrl" />
			</c:if>

			<c:set var="isGiftCard" value="${entry.product.isGiftCard}"/>
			<div class="subscription">
				<div>
					<cart:entryProductImage entry="${entry}" productUrl="${productUrl}"/>
					<cart:entryProductTitle entry="${entry}" isGiftCard="${isGiftCard}" productUrl="${productUrl}"/>
				</div>
				<div>
					<div class="item-title">
						<spring:theme code="basket.page.price" />:
					</div>
					<div class="item-value">
						<format:price priceData="${entry.basePrice}" displayFreeForZero="true" />
					</div>
				</div>
				<div>
					<div class="item-title">
						<spring:theme code="basket.page.qty" />:
					</div>
					<div class="item-value">
						<div class="quantity-field">
							${entry.quantity}
						</div>
					</div>
				</div>
			</div>
		</c:forEach>
	</c:if>
    <div class="subtotal"><span><spring:theme code="basket.page.totals.subtotal"/></span><span><format:price priceData="${cartData.subTotalWithoutDiscount}"/></span></div>
    <c:if test="${cartData.totalDiscounts.value > 0}">
        <div class="subtotal"><span><spring:theme code="basket.page.totals.discount"/></span><span>-&nbsp;<format:price priceData="${cartData.totalDiscounts}"/></span></div>
    </c:if>
    <c:if test="${cartData.taxAvailable}">
        <div class="tax"><span><spring:theme code="basket.page.totals.netTax"/></span><span><format:price priceData="${cartData.totalTax}"/></span></div>
    </c:if>

    <c:if test="${displayDeliveryCost and cartData.showDeliveryCost}">
	    <div class="subtotal"><span><spring:theme code="basket.page.totals.delivery"/></span>
	        <c:set var="deliveryPriceClass" value="${cartData.deliveryCost.value > 0 ? '' : 'strong'}"/>
	        <span class="${deliveryPriceClass}" id="order-totals-delivery-cost">
	            <format:price displayFreeForZero="true" priceData='${cartData.deliveryCost}'/>
	        </span>
	    </div>
	</c:if>

    <div class="total"><span><spring:theme code="basket.page.totals.total"/>
        </span><span><format:price priceData="${cartData.totalPriceWithTax}"/></span>
    </div>
	<span id="totalPriceValue" class="hidden">${cartData.totalPriceWithTax.value}</span>
</div>