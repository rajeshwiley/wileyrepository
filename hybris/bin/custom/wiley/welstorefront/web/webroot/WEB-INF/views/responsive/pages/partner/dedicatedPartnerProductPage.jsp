<%@ page trimDirectiveWhitespaces="true" pageEncoding="utf-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<spring:url value="/_ui/responsive/theme-blue/images/cpa_gold_large.jpg" var="imgCpaGold"/>

<div class="row course-summary">
    <div class="col-md-12 course-description">
        <h2>Course Description:</h2>
        <p>The Gold CPA Review Course includes Wiley CPAexcel’s most powerful combination of features, including the exam
            planner, online/offline synchronization, full set of textbooks, flash drive (for offline use), the Core Study
            Materials, Video Lectures and Professor Mentoring. See below for a complete list of course features.</p>
        <p>You now have free access to the Wiley CPA Online Test Bank until you pass the exam. We trust you will find it a
            valuable supplement in your exam preparation process. The Wiley Test Bank has over 4,400 multiple choice questions and
            164 simulations – ideal for drilling on your weakest areas!</p>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12 course-description-image">
        <img src="${imgCpaGold}" alt="" class="course-description-image">
    </div>
    <div class="col-md-6 col-sm-6  col-xs-12 course-description-includes">
        <div class="container-fluid">
            <div class="row">
                <p class="product-include-title">Includes:</p>
                <ul class="features-listing blueTheme col-md-6 clearfix">
                    <li><a href="" title="">Core Study Materials</a></li>
                    <li><a href="" title="">Video Lectures + Slides</a></li>
                    <li><a href="" title="">Professor Mentoring</a></li>
                </ul>
                <ul class="features-listing blueTheme col-md-6 clearfix">
                    <li><a href="" title="">Course Outlines</a></li>
                    <li><a href="" title="">Flash Drive for Offline Access</a></li>
                    <li>Partner Until You Pass Guarantee – Free Content and Software Updates &nbsp;<span
                            data-toggle="popover" data-original-title="Free Continuing Access to the Course - Guaranteed"
                            data-content="<p>Your Wiley CPAexcel course has no expiration date. Use it until one year after you pass the CPA Exam.<br>Course repeats, online content updates, and software upgrades are free.</p><p>This is our promise and guarantee. There is no fine print.</p>"
                            class="tooltipHelp">?</span></li>
                </ul>
            </div>
        </div>
    </div>
    <%-- for now suppose its possible to have just one base product for this page --%>
    <c:if test="${partnerProducts.size()>0}">
        <div class="col-md-6 col-sm-6  col-xs-12 course-product-variant">
            <c:set var="product" value="${partnerProducts[0]}"/>
            <input type="hidden" name="baseProductCode" id="product-${product.code}" value="${product.code}" />
            <c:forEach items="${product.variantMatrix}" var="variantMatrixElement" varStatus="loop">
                <c:set var="checkedValue" value="${loop.first ? 'checked' : ''}"/>
                <input type="radio" name="productVariantCode" id="product-variant-${variantMatrixElement.variantOption.code}" 
                    value="${variantMatrixElement.variantOption.code}" ${checkedValue} />
                <label for="product-variant-${variantMatrixElement.variantOption.code}">${variantMatrixElement.variantValueCategory.name}</label>
            </c:forEach>
        </div>
    </c:if>
</div>
