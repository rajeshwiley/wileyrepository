<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ attribute name="productTemplate" required="false" %>
<%@ attribute name="formIndex" required="false" %>

<c:choose>
    <c:when test="${product.multidimensional}">
        <product:productVariantSelector product="${product}" productTemplate="${productTemplate}" isVisible="true" formIndex="${formIndex}"/>
    </c:when>
    <c:otherwise>
        <%-- If product doesn't have variants we add this product to cart. --%>
        <input type="hidden" name="productCodePost" id="product-single-${product.code}-${formIndex}" value="${product.code}" data-price="${product.price.value}" data-student-price="${product.studentPrice.value}"/>
    </c:otherwise>
</c:choose>
