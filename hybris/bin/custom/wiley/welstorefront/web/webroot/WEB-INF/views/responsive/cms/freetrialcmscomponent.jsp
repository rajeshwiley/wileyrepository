<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>

<c:if test="${not empty wileyFreeTrialForm && not empty freeTrial}">

<sec:authorize access="isAuthenticated()">
    <c:set var="readonly" value="true" />
</sec:authorize>

<div class="cfa-free-trial">
    <div class="container">
		<h1 class="page-title">${freeTrial.name}</h1>
		<div class="row">
			<div class="col-sm-6 col-xs-12 descriptionBlock">
				<c:choose>
					<c:when test="${not empty freeTrial.videoLink}">
						<div class="video">
							<iframe width="330" height="250" src="${freeTrial.videoLink}" frameborder="0" allowfullscreen></iframe>
						</div>
					</c:when>
					<c:otherwise>
						<c:if test="${not empty freeTrial.images}">
							<div class="thumb">
								<c:set var="image" value="${freeTrial.images.iterator().next()}" />
								<img src="${image.url}" alt="${fn:escapeXml(image.altText)}"
									title="${fn:escapeXml(image.altText)}" />
							</div>
						</c:if>
					</c:otherwise>
				</c:choose>
				${freeTrial.description}
			</div>
			<div class="col-sm-6 col-xs-12 freeTrialFormBlock">
				<h2 class="sub-title">${freeTrial.slogan}</h2>
		
				<form:form action="${request.contextPath}/free-trial" method="post" modelAttribute="wileyFreeTrialForm"
					class="site-form">
					<form:hidden path="anonymous" value="${!readonly}" />
		
					<div class="form-cell">
						<formElement:formInputBox idKey="firstName" labelKey="freeTrial.placeHolder.firstName"
							labelCSS="form-label required" path="firstName" mandatory="true" readonly="${readonly}" />
					</div>
					<div class="form-cell">
						<formElement:formInputBox idKey="lastName" labelKey="freeTrial.placeHolder.lastName"
							labelCSS="form-label required" path="lastName" mandatory="true" readonly="${readonly}" />
					</div>
					<div class="form-cell">
						<formElement:formInputBox idKey="email" labelKey="freeTrial.placeHolder.email"
							labelCSS="form-label required" path="email" mandatory="true" autocomplete="off"
							readonly="${readonly}" />
					</div>
					<div class="form-cell">
						<formElement:formInputBox idKey="employerOrSchool" labelKey="freeTrial.placeHolder.employerOrSchool"
							labelCSS="form-label required" path="employerOrSchool" mandatory="true" />
					</div>
					<div class="form-cell">
						<formElement:formSelectBox idKey="country" labelKey="freeTrial.selectCountry" path="country"
							mandatory="true" skipBlank="true" items="${countries}" itemValue="isocode"
							disabled="${isCountryDisabled ? 'true' : ''}" labelCSS="form-label required" />
					</div>
		
							<c:choose>
								<c:when test="${not empty freeTrial.freeTrialList}">
									<div class="form-cell">
										<formElement:formSelectBox idKey="freeTrial" labelKey="freeTrial.selectCourse"
											path="freeTrial" mandatory="true" items="${freeTrial.freeTrialList}" itemValue="code"
											labelCSS="form-label required" />
									</div>
								</c:when>
								<c:otherwise>
									<form:hidden path="freeTrial" value="${freeTrial.code}" />
								</c:otherwise>
							</c:choose>
		
					<sec:authorize access="!isAuthenticated()">
						<div class="form-cell">
							<formElement:formPasswordBox idKey="pwd" labelKey="freeTrial.placeHolder.enterPassword" path="pwd"
								labelCSS="form-label required" mandatory="true" />
						</div>
						<div class="form-cell">
							<formElement:formPasswordBox idKey="checkPwd" labelKey="freeTrial.placeHolder.confirmPassword"
								path="checkPwd" labelCSS="form-label required" mandatory="true" />
						</div>
					</sec:authorize>
					<div class="form-cell">
						<formElement:formCheckbox path="signUp" idKey="signUp" labelKey="freeTrial.signUp.text" />
					</div>
					<div class="form-buttons">
						<span class="btn-control">
							<button type="submit" class="button form-button" onclick="this.form.submit(); this.disabled=true;">
								<spring:theme code="freeTrial.launchFreeTrial" />
							</button>
						</span>
					</div>
					<div class="note">
						<p>
							<spring:theme code="freeTrial.note.text" />
							&nbsp<a href="${component.privacyPolicyLink.url}"><spring:theme
									code="freeTrial.privacyPolicyUrl.text" /></a>.
						</p>
					</div>
				</form:form>
			</div>
		</div>
	</div>
</div>

</c:if>
