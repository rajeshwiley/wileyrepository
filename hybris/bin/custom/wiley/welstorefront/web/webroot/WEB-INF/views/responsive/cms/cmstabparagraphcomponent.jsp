<%@ page trimDirectiveWhitespaces="true" %>

<div class="tabhead">
    <span class="glyphicon"></span>
    <a href="">${component.title}</a>
</div>
<div class="tabbody">${component.content}</div>