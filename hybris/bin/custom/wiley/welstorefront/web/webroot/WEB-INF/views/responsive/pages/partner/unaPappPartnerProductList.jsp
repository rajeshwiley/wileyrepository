<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>

<form:form method="post" action="${request.contextPath}/cart/addGrid">
    <div class="uma-programm-details">
        <div class="uma-programm-table">
            <div class="title">
                <spring:theme code="partner.una.section" />
            </div>
            <c:forEach items="${partnerProducts}" var="product" varStatus="loop">
                <div class="table-row">
                    <div class="table-row-left">
                        <h3>${product.name}</h3>
                        <p>${product.summary}&ensp;<format:price priceData="${product.price}" /></p>
                    </div>
                    <div class="table-row-right">
                        <div class="checkbox-component">
                            <input type="checkbox" id="courseName${loop.index}" value="${product.code}" name="sku">
                            <label for="courseName${loop.index}"></label>
                        </div>
                    </div>
                </div>
            </c:forEach>
        </div>
        <input type="hidden" name="partnerId" value="${partnerId}"/>
        <button type="submit" class="btn btn-primary">
            <spring:theme code="partner.una.continue" />
        </button>
    </div>
</form:form>