<%@ attribute name="messageKey" required="true" type="java.lang.String" %>
<%@ attribute name="message" required="false" type="java.lang.String" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div id="global-alert" class="global-alerts hidden">
	<div class="alert alert-danger alert-dismissable">
		<button class="close" type="button">x</button>
		<div id="${messageKey}">${message}</div>
	</div>
</div>