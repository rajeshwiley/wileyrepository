<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="component" tagdir="/WEB-INF/tags/shared/component"%>


  <c:if test="${productComparisonTableTitle ne null}">
  	<div class="content">
  		<h2 class="comparison-table-title">${productComparisonTableTitle}</h2>
  	</div>
  </c:if>
  <div class="comparison-table comparison-table-mobile">
	<c:forEach items="${productComparisonData.products}" var="product" varStatus="status">
		<div class="course-component">
	      <div class="course-component-header">
	        <div class="course-component-title">
	        	<c:if test="${status.index eq 0}">
               		<span class="most-popular-label"><spring:theme code="product.list.table.most.popular"/></span>
               	</c:if>
               	${product.name}
	        </div>
	        <div class="learn-more-links">
	          <c:url value="${product.url}" var="productUrl"></c:url>
	          <div class="links">
	            <div class="link-wrapper"><a href="${productUrl}" class="link"><spring:theme code="product.list.table.learn.more"/></a></div>
	          </div>
	        </div>
	      </div>
	      <div class="course-component-content">
	        <div class="course-component-price">
	           <c:if test="${product.pricePerPart.priceType eq 'FROM'}">
                 <span class="starting-at"><spring:theme code="product.list.price.startingAt"/>&nbsp;</span>
               </c:if>
	          <div class="price-value">${product.pricePerPart.shortFormattedValue}</div>
	          <c:if test="${displayCompleteSet}">
	          	<div class="price-name"><spring:theme code="product.list.table.per.part"/></div>
	          </c:if>
	        </div>
	        <c:if test="${displayCompleteSet}">
		        <div class="course-component-price">
		          <div class="price-value">${product.priceCompleteSet.shortFormattedValue}</div>
		          <div class="price-name"><spring:theme code="product.list.mobile.complete.set"/></div>
		        </div>
	        </c:if>
	        <ul class="course-component-products">
	          <c:forEach items="${productComparisonData.features}" var="feature">
					<product:productListerComparisonClassifications product="${product}" feature="${feature}" featureName="${feature.name}" mobile="true"/>
	          </c:forEach>
	        </ul>
	      </div>
	      <div class="course-component-footer">
	        <div class="learn-more-links">
	          <c:url value="${product.url}" var="productUrl"></c:url>
	          <div class="links">
	            <div class="link-wrapper"><a href="${productUrl}" class="link"><spring:theme code="product.list.table.learn.more"/></a></div>
	          </div>
	        </div>
	      </div>
	    </div>
	</c:forEach>
  </div>
  
  <div class="comparison-table comparison-table-desktop">
          <table>
            <thead>
              <tr>
                <th><spring:theme code="product.list.table.components"/></th> 
                <c:forEach items="${productComparisonData.products}" var="product" varStatus="status">
                	<th>
	                	<c:if test="${status.index eq 0}">
	                		<span class="most-popular-label"><spring:theme code="product.list.table.most.popular"/></span>
	                	</c:if>
	                	<c:url value="${product.url}" var="productUrl"></c:url>
	                	<a href="${productUrl}">${product.name}</a>	
                	</th>
                </c:forEach>
              </tr>
            </thead>
            
            <tbody>
           	  <tr>
           	  	<td>
           	  		<c:choose>
	           	  		<c:when test="${displayCompleteSet}">
	           	  			<spring:theme code="product.list.table.price.per.part"/>
	           	  		</c:when>
	           	  		<c:otherwise>
	           	  			<spring:theme code="product.list.table.price"/>
	           	  		</c:otherwise>	           	  		
           	  		</c:choose>
           	  	</td>
           	  		<c:forEach items="${productComparisonData.products}" var="product" varStatus="status">
            		 	<td>
                            <span class="price-value">
                                ${product.pricePerPart.shortFormattedValue}
	            		 	</span>
            		 	</td>
           		    </c:forEach>	
              </tr>
              
              <c:if test="${displayCompleteSet}">
              	 <tr>
              	 	<td><spring:theme code="product.list.table.complete.set"/></td>
	              	<c:forEach items="${productComparisonData.products}" var="product" varStatus="status">
            		 	<td>
            		 		<c:choose>
								<c:when test="${not empty product.priceCompleteSet}">
		            		 		<span class="price-value">
		            		 			${product.priceCompleteSet.shortFormattedValue}
		            		 		</span>
								</c:when>
								<c:otherwise>
									<span class="not-applicable"><spring:theme code="product.list.price.notAvailable"/></span>
								</c:otherwise>
							</c:choose>
            		 	</td>
	              	</c:forEach>
               	</tr>
              </c:if>
             
	           	<c:forEach items="${productComparisonData.features}" var="feature">
	           		<tr>
	           			<td>${feature.name}</td>
	         				<c:forEach items="${productComparisonData.products}" var="product" varStatus="status">
	         					<td><product:productListerComparisonClassifications product="${product}" feature="${feature}"/></td>
	         				</c:forEach>
	           		</tr>
	           	</c:forEach> 
            </tbody>
            
            <tfoot>
              <tr class="learn-more">
                <td></td>
                <c:forEach items="${productComparisonData.products}" var="product" varStatus="status">
                	<td>
	                  	<c:url value="${product.url}" var="productUrl"></c:url>
	                    <a href="${productUrl}" class="link"><spring:theme code="product.list.table.learn.more"/></a>
              		</td>
                </c:forEach>
              </tr>
            </tfoot>
          </table>
        </div>