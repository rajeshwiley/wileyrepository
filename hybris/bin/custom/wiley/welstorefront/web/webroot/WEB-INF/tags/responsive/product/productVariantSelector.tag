<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ attribute name="isVisible" required="false" type="java.lang.Boolean" %>
<%@ attribute name="formIndex" required="false" %>
<%@ attribute name="productRootNode" required="false" type="com.wiley.facades.product.node.ProductRootNode" %>
<%@ attribute name="productRootNodeJson" required="false" type="java.lang.String" %>
<%@ attribute name="productTemplate" required="false" %>

<%-- variables required for JS --%>
<script type='text/javascript'>
	if (typeof multidimensionalProducts === 'undefined'){
		multidimensionalProducts = [];
	}
	multidimensionalProducts.push('${product.code}');
	
	if (typeof productRootNodes === 'undefined'){
		productRootNodes = {};
	}
	productRootNodes['${product.code}'] = JSON.parse('${productRootNodeJson}');
	productVariantsTemplates ="\
		<div id='view-template-product'>\
			<div id='variants-section-{{code}}{{^code}}root{{/code}}' class='variants-section'>\
				{{#nodes}}\
					{{#viewVisible}}\
						<input type='{{viewType}}' name='{{viewName}}' id='product-variant-{{code}}-formIndexHolder' data-code='{{code}}' value='{{value.sku}}' checked/>\
						<label for='product-variant-{{code}}-formIndexHolder'>{{name}}</label>\
					{{/viewVisible}}\
					{{^viewVisible}}\
						<input type='{{viewType}}' name='{{viewName}}' id='product-variant-{{code}}-formIndexHolder' data-code='{{code}}' value='{{value.sku}}'/>\
						<label for='product-variant-{{code}}-formIndexHolder' style='display: none'>{{name}}</label>\
					{{/viewVisible}}\
				{{/nodes}}\
			</div>\
		</div>\
		\
		<div id='view-template-addons'>\
			<div id='variants-section-addons'>\
				{{#nodes}}\
					<input type='checkbox' name='{{viewName}}' id='addon-variant-{{code}}-formIndexHolder' value='{{value.sku}}' checked/>\
					<label for='addon-variant-{{code}}-formIndexHolder' style='display: none'>{{name}}</label>\
				{{/nodes}}\
			</div>\
		</div>";
</script>

<c:set var="displayStyle" value="${isVisible ? '' : 'display:none'}"/>
<%-- Render initial variants sections --%>
<div class="clearfix product-variants" style="${displayStyle}">
	<c:choose>
		<c:when test="${productTemplate == 'cfa' || cmsPage.uid == 'productDetailsCFASupplements'}">
    		<product:cfaProductVariantsSection rootNode = "${productRootNode.product}" product="${product}" formIndex="${formIndex}"/>
    	</c:when>
        <c:otherwise>
            <product:productVariantsSection rootNode = "${productRootNode.product}" formIndex="${formIndex}"/>
        </c:otherwise>
    </c:choose>
</div>

