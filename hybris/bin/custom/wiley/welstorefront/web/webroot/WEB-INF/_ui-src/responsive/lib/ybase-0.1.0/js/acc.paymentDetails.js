ACC.paymentDetails = {
	_autoload: [
		"showRemovePaymentDetailsConfirmation"
	],
	
	showRemovePaymentDetailsConfirmation: function ()
	{
		$(document).on("click", ".removePaymentDetailsButton", function ()
		{
			var paymentId = $(this).data("paymentId");
			var popupTitle = $(this).data("popupTitle");

			ACC.colorbox.open(popupTitle,{
				inline: true,
				transition: "elastic",
				href: "#popup_confirm_payment_removal_" + paymentId,
				onComplete: function ()
				{
					$(this).colorbox.resize();
				}
			});

		});
        
        function sendBillShippAddressForm() {
            $('#wileyBillShippAddressForm').submit();
        }

        $('#saveBilling').on('click', function(event) {
            event.preventDefault();
            sendBillShippAddressForm();
        });

        $('#wileyBillShippAddressForm').on('keydown', 'input, select', function(event) {
            if(event.which == 13)  sendBillShippAddressForm();
        });
	}
}