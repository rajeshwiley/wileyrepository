<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="address" required="true" type="de.hybris.platform.commercefacades.user.data.AddressData" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

${fn:escapeXml(address.firstName)}&nbsp;${fn:escapeXml(address.lastName)}
<br>
${fn:escapeXml(address.line1)}
<c:if test="${not empty fn:escapeXml(address.line2)}">
	<br>
	${fn:escapeXml(address.line2)}
</c:if>
<br>
${fn:escapeXml(address.town)}
<c:if test="${not empty address.town && (not empty address.region || not empty  address.postalCode)}">,</c:if>
<c:if test="${not empty address.region && not empty address.region.isocodeShort}">
	&nbsp;${address.region.isocodeShort}
</c:if>
&nbsp;${fn:escapeXml(address.postalCode)}
<br>
${fn:escapeXml(address.country.name)}&nbsp;
<br/>
${fn:escapeXml(address.phone)}