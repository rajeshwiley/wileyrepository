<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>

<template:page pageTitle="${pageTitle}">
	<h1 class="comparison-table-title"><spring:theme code="plp.cfa.title"/></h1>
	<div class="comparison-product-navigation">
	  <h3 class="comparison-table-subtitle"><spring:theme code="plp.cfa.subtitle"/></h3>
	  <div class="comparison-product-anchors">
		<p class="anchors-text"><spring:theme code="plp.cfa.anchors.title"/></p>
		<div class="anchors-links">
			<c:forEach items="${wileyProductListCFAData.gridData}" var="entry">
            	<a href="#${entry.key}"  class="anchor-link">${entry.value.levelName}</a>
            </c:forEach>
		</div>
	  </div>
	</div>
	<cms:pageSlot position="MainContent" var="feature">
		<cms:component component="${feature}"/>
	</cms:pageSlot>
</template:page>