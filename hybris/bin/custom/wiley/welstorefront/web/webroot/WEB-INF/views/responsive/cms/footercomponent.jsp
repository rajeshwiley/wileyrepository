<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="footer" tagdir="/WEB-INF/tags/responsive/common/footer"  %>

<footer class="main-footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-10 col-sm-10 col-xs-12">
                <ul class="nav navbar-nav navbar-left">
                
                <c:forEach items="${navigationNodes}" var="node">
                	<c:if test="${node.visible}">
                		<c:forEach items="${node.links}" var="childlink">
								<cms:component component="${childlink}" evaluateRestriction="true" element="li" />
						</c:forEach>
                	</c:if>
                </c:forEach>
                                    
                </ul>
            </div>
            <div class="col-sm-12 col-md-2 col-sm-2 col-xs-12">
                 <cms:pageSlot position="CompanyLogo" var="component" element="div" class="logo">
                     <cms:component component="${component}" />
                 </cms:pageSlot>
            </div>
        </div>
    </div>
</footer>