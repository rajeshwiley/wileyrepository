<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>

<%-- Don't remove containerCSSClass attribute, otherwise 'container' class will be added to div and it may broken layout. Look page.tag for more info --%>
<template:page pageTitle="${pageTitle}" containerCSSClass="dummy-class-instead-of-absence-of-any-classes">
	<cms:pageSlot position="MainContent" var="feature">
		<cms:component component="${feature}"/>
	</cms:pageSlot>
</template:page>