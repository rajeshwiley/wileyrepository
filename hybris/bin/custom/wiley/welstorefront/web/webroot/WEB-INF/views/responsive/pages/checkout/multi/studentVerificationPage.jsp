<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi"%>

<template:page pageTitle="${pageTitle}" hideHeaderLinks="true"
	hideActivatePinLink="true" hideCartLink="true" hideLoginLink="true"
	hideCustomLinks="true"
	containerCSSClass="container student-verification">

    <cms:pageSlot position="CheckoutBreadcrumbs" var="feature">
        <c:set var="step" value="3" scope="request" />
        <cms:component component="${feature}" />
    </cms:pageSlot>

    <div class="content">
        <h1 class="page-title">
            <spring:theme code="checkout.multi.studentVerification.header" />
        </h1>
        <p class="student-verification-question">
            <spring:theme code="checkout.multi.studentVerification.question" />
        </p>
        <multi-checkout:studentVerificationForm />
    </div>
</template:page>

