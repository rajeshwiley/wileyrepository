<%@ attribute name="subpath" required="false" type="java.lang.String"%>
<%@ attribute name="tabIndexStartFrom" required="false" type="java.lang.Integer"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>

<c:set var="path" value="${subpath}subscribeGmacInfo" />

<div class="gmac-select form-buttons">
  <template:errorSpanField path="${path}" adjustToTwoColLayout="true">
    <div class="form-field">
        <input type="radio" name="${path}" id="gmac_yes" value="true" tabindex="${tabIndexStartFrom + 1}" />
        <label for="gmac_yes"><spring:theme code="form.subscribeGmacInfo.yes"/> <a target="_blank" href="https://www.mba.com/service/contact-us.aspx"><spring:theme code="form.subscribeGmacInfo.contactGmac"/></a>.</label>
    </div>
    <div class="form-field">
        <input type="radio" name="${path}" id="gmac_no" value="false"  tabindex="${tabIndexStartFrom + 2}" />
        <label for="gmac_no"><spring:theme code="form.subscribeGmacInfo.no"/></label>
    </div>
    <div id="gmacNoSelected" style="color:red; display:none">
        <spring:theme code="form.subscribeGmacInfo.message"/>
    </div>
  </template:errorSpanField>
</div>