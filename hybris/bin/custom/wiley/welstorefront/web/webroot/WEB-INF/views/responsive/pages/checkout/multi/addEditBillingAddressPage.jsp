<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/address"%>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="checkout" tagdir="/WEB-INF/tags/responsive/checkout" %>
<%@ taglib prefix="error" tagdir="/WEB-INF/tags/responsive/error" %>

<%-- This page support combined billing and shipping addresses form --%>
<template:page renderAntiClickJacking="true" pageTitle="${pageTitle}" hideHeaderLinks="true" hideCustomLinks="true" hideCartLink="true" hideActivatePinLink="true" hideLoginLink="true" showLoginLinkOnMobile="false" containerCSSClass="container billing">
		<script type="text/javascript">
		if (self === top) {
			var antiClickjack = document.getElementById("antiClickjack");
			antiClickjack.parentNode.removeChild(antiClickjack);
		} else {
			top.location = self.location;
		}
		</script>

		<error:paymentGlobalAlert messageKey="global-error-message-list"/>

		<cms:pageSlot position="CheckoutBreadcrumbs" var="feature">
			<c:set var="step" value="2" scope="request"/>
			<cms:component component="${feature}" />
		</cms:pageSlot>

        <div class="content">   
          <div class="text-left">
	          <cms:pageSlot position="TopContent" var="feature">
				<cms:component component="${feature}"/>
			</cms:pageSlot>
          </div>

          <cms:pageSlot position="MiddleContent" var="feature">
				<cms:component component="${feature}"/>
		  </cms:pageSlot>

        </div>
      
</template:page>
