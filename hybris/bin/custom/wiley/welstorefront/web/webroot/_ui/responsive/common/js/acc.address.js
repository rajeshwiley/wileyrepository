ACC.address = {

	_autoload: [
		"bindToChangeAddressButton",
		"bindCreateUpdateAddressForm",
		"bindSuggestedDeliveryAddresses",
		"bindCountrySpecificAddressForms",
		"showAddressFormButtonPanel",
		"bindViewAddressBook",
		"bindToColorboxClose",
		"showRemoveAddressFromBookConfirmation",
		"backToListAddresses",
		"bindDeliveryMethodSelector",
		"bindToChangeGmacButton"
	],

	spinner: $("<img src='" + ACC.config.commonResourcePath + "/images/spinner.gif' />"),
	addressID: '',

	handleChangeAddressButtonClick: function ()
	{
		ACC.address.addressID = ($(this).data("address")) ? $(this).data("address") : '';
		$('#summaryDeliveryAddressFormContainer').show();
		$('#summaryOverlayViewAddressBook').show();
		$('#summaryDeliveryAddressBook').hide();


		$.getJSON(getDeliveryAddressesUrl, ACC.address.handleAddressDataLoad);
		return false;
	},

	handleAddressDataLoad: function (data)
	{
		ACC.address.setupDeliveryAddressPopupForm(data);

		// Show the delivery address popup
		ACC.colorbox.open("",{
		 	inline: true,
			href: "#summaryDeliveryAddressOverlay",
			overlayClose: false,
			onOpen: function (){
				// empty address form fields
				ACC.address.emptyAddressForm();
				$(document).on('change', '#saveAddress', function ()
				{
					var saveAddressChecked = $(this).prop('checked');
					$('#defaultAddress').prop('disabled', !saveAddressChecked);
					if (!saveAddressChecked)
					{
						$('#defaultAddress').prop('checked', false);
					}
				});
			}
		});

	},

	setupDeliveryAddressPopupForm: function (data)
	{
		// Fill the available delivery addresses
		$('#summaryDeliveryAddressBook').html($('#deliveryAddressesTemplate').tmpl({addresses: data}));
		// Handle selection of address
		$('#summaryDeliveryAddressBook button.use_address').click(ACC.address.handleSelectExistingAddressClick);
		// Handle edit address
		$('#summaryDeliveryAddressBook button.edit').click(ACC.address.handleEditAddressClick);
		// Handle set default address
		$('#summaryDeliveryAddressBook button.default').click(ACC.address.handleDefaultAddressClick);
	},

	emptyAddressForm: function ()
	{
		var options = {
			url: getDeliveryAddressFormUrl,
			data: {addressId: ACC.address.addressID, createUpdateStatus: ''},
			type: 'GET',
			success: function (data)
			{
				$('#summaryDeliveryAddressFormContainer').html(data);
				ACC.address.bindCreateUpdateAddressForm();
			}
		};

		$.ajax(options);
	},

	handleSelectExistingAddressClick: function ()
	{
		var addressId = $(this).attr('data-address');
		$.postJSON(setDeliveryAddressUrl, {addressId: addressId}, ACC.address.handleSelectExitingAddressSuccess);
		return false;
	},

	handleEditAddressClick: function ()
	{

		$('#summaryDeliveryAddressFormContainer').show();
		$('#summaryOverlayViewAddressBook').show();
		$('#summaryDeliveryAddressBook').hide();

		var addressId = $(this).attr('data-address');
		var options = {
			url: getDeliveryAddressFormUrl,
			data: {addressId: addressId, createUpdateStatus: ''},
			target: '#summaryDeliveryAddressFormContainer',
			type: 'GET',
			success: function (data)
			{
				ACC.address.bindCreateUpdateAddressForm();
				ACC.colorbox.resize();
			},
			error: function (xht, textStatus, ex)
			{
				alert("Failed to update cart. Error details [" + xht + ", " + textStatus + ", " + ex + "]");
			}
		};

		$(this).ajaxSubmit(options);
		return false;
	},

	handleDefaultAddressClick: function ()
	{
		var addressId = $(this).attr('data-address');
		var options = {
			url: setDefaultAddressUrl,
			data: {addressId: addressId},
			type: 'GET',
			success: function (data)
			{
				ACC.address.setupDeliveryAddressPopupForm(data);
			},
			error: function (xht, textStatus, ex)
			{
				alert("Failed to update address book. Error details [" + xht + ", " + textStatus + ", " + ex + "]");
			}
		};

		$(this).ajaxSubmit(options);
		return false;
	},

	handleSelectExitingAddressSuccess: function (data)
	{
		if (data != null)
		{
			ACC.refresh.refreshPage(data);
			ACC.colorbox.close();
		}
		else
		{
			alert("Failed to set delivery address");
		}
	},

	bindCreateUpdateAddressForm: function ()
	{
		$('.create_update_address_form').each(function ()
		{
			var options = {
				type: 'POST',
				beforeSubmit: function ()
				{
					$('#checkout_delivery_address').block({ message: ACC.address.spinner });
				},
				success: function (data)
				{
					$('#summaryDeliveryAddressFormContainer').html(data);
					var status = $('.create_update_address_id').attr('status');
					if (status != null && "success" === status.toLowerCase())
					{
						ACC.refresh.getCheckoutCartDataAndRefreshPage();
						ACC.colorbox.close();
					}
					else
					{
						ACC.address.bindCreateUpdateAddressForm();
						ACC.colorbox.resize();
					}
				},
				error: function (xht, textStatus, ex)
				{
					alert("Failed to update cart. Error details [" + xht + ", " + textStatus + ", " + ex + "]");
				},
				complete: function ()
				{
					$('#checkout_delivery_address').unblock();
				}
			};

			$(this).ajaxForm(options);
		});
	},

	refreshDeliveryAddressSection: function (data)
	{
		$('.summaryDeliveryAddress').replaceWith($('#deliveryAddressSummaryTemplate').tmpl(data));

	},

	bindSuggestedDeliveryAddresses: function ()
	{
		var status = $('.add_edit_delivery_address_id').attr('status');
		if (status != null && "hasSuggestedAddresses" == status)
		{
			ACC.address.showSuggestedAddressesPopup();
		}
	},

	showSuggestedAddressesPopup: function ()
	{

		ACC.colorbox.open("",{
			href: "#popup_suggested_delivery_addresses",
			inline: true,
			overlayClose: false,
			width: 525,
		});
	},
	bindCountrySpecificAddressForms: function (){
    	var billingAddressForm = $("div[id='billingAddress.form']");
		ACC.address.bindCountrySpecificAddressForm(billingAddressForm);
		var shippingAddressForm = $("div[id='shippingAddress.form']");
		ACC.address.bindCountrySpecificAddressForm(shippingAddressForm);
		$("#sameAsBilling").on("change",ACC.address.handleSameAsBillingOptionChanged);
	},
	
	bindDeliveryMethodSelector: function (){
		$(document).on("change","input[name='delivery_method']",ACC.address.handleDeliveryMethodChanged)
	},

	bindToChangeGmacButton: function(){
	  $("div[id='gmacNoSelected']").hide();
	  $(document).on("change","input[name='billingAddress.subscribeGmacInfo']",ACC.address.gmacButtonChanged);
 	},

 gmacButtonChanged: function(){
  var gmacNoSelectedDiv = $("div[id='gmacNoSelected']");
  var errorMessage = $("span[id='billingAddress.subscribeGmacInfo.errors']");
   if ($("#gmac_no").is(":checked")){
  		gmacNoSelectedDiv.show();
  		errorMessage.hide();
  	}else{
  		gmacNoSelectedDiv.hide();
  		errorMessage.hide();
  	}
 },
	
	bindCountrySpecificAddressForm: function(addressForm){
		$(addressForm).find("[id$='countrySelector'] select").on("change", ACC.address.handleCountryChanged);
		$(addressForm).find("select[id$=addressRegion]").on("change", ACC.address.handleRegionChanged)		
		$(addressForm).find("input:text").not("[id$=addressLine2]").on("focusout", ACC.address.handleInputFocusOut);
	},

	showAddressFormButtonPanel: function ()
	{
		if ($('#countrySelector :input').val() !== '')
		{
			$('#addressform_button_panel').show();
		}
	},

	bindToColorboxClose: function ()
	{
		$(document).on("click", ".closeColorBox", function ()
		{
			ACC.colorbox.close();
		})
	},
	
	handleCountryChanged: function (){
		var addressForm = ACC.address.getAddressForm(this);
		ACC.address.displayCountrySpecificAddressForm(addressForm, false);
	},

	needChangeDeliveryOptions: function(addressForm) {
		return !($(addressForm).attr("id") == 'billingAddress.form' && !ACC.address.isShippingSameAsBilling());
	},

	updateDeliveryOptions: function (countryIso) {
		$.ajax({
			url: ACC.config.encodedContextPath + '/checkout/multi/billing-shipping-address/deliveryoptions',
			async: true,
			data: {
				"countryIsoCode": countryIso
			},
			dataType: "json"
			}).done(function (data) {
                var idx = 0;
                var deliveryModesData = {
                    deliveryModels : data,
                    getTabIndex : function () {
                        return function(val, render) {
                            var tabIndex = parseFloat(render(val)) + idx;
                            idx ++;
                            return tabIndex;
                        }
                    }
                }
				var delivery_method_template = $("#deliveryMethodTemplate").html();
				$('#delivery-method-selector').html(Mustache.render(delivery_method_template, deliveryModesData));
				ACC.address.handleDeliveryMethodChanged();
			});
	},

	handleRegionChanged: function(){
		var addressForm = ACC.address.getAddressForm(this);
		ACC.address.updateTaxInCart(addressForm, true);
	},
    		
   	handleInputFocusOut: function(){
		var addressForm = ACC.address.getAddressForm(this);
		ACC.address.saveNewAddressAndCalculateTax(addressForm);
	},
	
	handleSameAsBillingOptionChanged: function(){
		var billingAddressForm = $("div[id='billingAddress.form']");
		var shippingAddressForm = $("div[id='shippingAddress.form']");
		if ($(this).is(":checked")) {
			shippingAddressForm.hide();
			ACC.address.displayCountrySpecificAddressForm(billingAddressForm, false);
		} else {
			var customerHasSavedShippingAddress = $("[id='shippingAddress.customerHasSavedAddress']").val();
			if (customerHasSavedShippingAddress != 'true'){
				//if customer doesn't have saved shipping address, make form the same as billing one
				ACC.address.displayCountrySpecificAddressForm(shippingAddressForm, true);
			} else {
				ACC.address.displayCountrySpecificAddressForm(shippingAddressForm, false);
			}
			shippingAddressForm.show();
		}
	},
	
	handleDeliveryMethodChanged: function () {
		var selectedDeliveryMethod = $("input[name='delivery_method']:checked").val();
		var options = {
				'deliveryMethod': selectedDeliveryMethod
			};
			$.ajax({
				url: ACC.config.encodedContextPath + '/checkout/multi/billing-shipping-address/updateDeliveryMethod',
				async: true,
				data: options,
				dataType: "html",
			}).done(function (data)
				{
					ACC.address.updateOrderTotals($(data));
					ACC.address.updatePaymentForm();
				})
	},
	
	displayCountrySpecificAddressForm: function (addressForm, needCopyBillingAddressToShipping)
	{
		if( ACC.address.isBillingOnly() ) {
			ACC.address.displayMemorizableCountrySpecificAddressForm(addressForm);
		} else {
			var urlGetAddressForm = ACC.config.encodedContextPath;
			urlGetAddressForm += 
				(needCopyBillingAddressToShipping === true)
				? '/checkout/multi/billing-shipping-address/initShippingAddress' 
				: '/checkout/multi/billing-shipping-address/addressform';
			$.ajax({
				url: urlGetAddressForm,
				async: true,
				data: $('#wileyBillShippAddressForm').serialize(),
				dataType: "html"
			}).done(function (data)
					{
						ACC.address.updateAddressForm(addressForm, data);
						ACC.address.updateTaxInCart(addressForm, false);
						if (ACC.address.needChangeDeliveryOptions(addressForm)) {
							ACC.address.updateDeliveryOptions(ACC.address.getCountryIsoForAddressForm(addressForm));
						}
						ACC.address.setFocusOnCountry(addressForm);
					});
		}
	},

	// this function keeps filled form after Ajax-call
	displayMemorizableCountrySpecificAddressForm: function(addressForm)
	{
		$.ajax({
			url: ACC.config.encodedContextPath + '/checkout/multi/billing-shipping-address/billing-only-addressform',
			async: true,
			data: $('#wileyBillShippAddressForm').serialize(),
			dataType: "html"
		}).done(function (data)
				{
					ACC.address.updateAddressForm(addressForm, data);
					ACC.address.updateTaxInCart(addressForm, true);
					ACC.address.setFocusOnCountry(addressForm);
				});

	},

	setFocusOnCountry: function (addressForm)
	{
		$("[id='" + addressForm.context.id + "']").focus();
	},

	updateTaxInCart: function (addressForm, isAsync)
	{
		var formId = $(addressForm).attr("id");
		if (ACC.address.isTaxCalulationRequired(formId))
		{
			var countryIsocode = $(addressForm).find("[id$='countrySelector'] select option:selected").val();
			var regionIsocode = '';
			var countryHasRegions = $("#countryHasRegions").val();
			var isShippingSameAsBilling = ACC.address.isShippingSameAsBilling() || ACC.address.isBillingOnly();
			if(countryHasRegions == 'true'){
				regionIsocode = $(addressForm).find("[id$='addressRegion'] option:selected").val();
			}
			var customerHasSavedAddress = $(addressForm).find("[id$='customerHasSavedAddress']").val();
			if(customerHasSavedAddress != 'true'){
				ACC.address.saveNewAddressAndCalculateTax(addressForm);
			}
			else if(ACC.address.orderDetailsElementExists())
			{
				var options = {
					'countryIsocode': countryIsocode,
					'regionIsocode': regionIsocode,
					'isShippingSameAsBilling': isShippingSameAsBilling
				};
				$.ajax({
					url: ACC.config.encodedContextPath + '/checkout/multi/billing-shipping-address/updateTax',
					async: isAsync,
					data: options,
					dataType: "html",
				}).done(function (data)
					{
						ACC.address.updateOrderTotals($(data));
						ACC.address.updatePaymentForm();
					})
			}
		}
	},

	saveNewAddressAndCalculateTax: function(addressForm)
	{
		var customerHasSavedAddress = $(addressForm).find("[id$='customerHasSavedAddress']");
		var formId = $(addressForm).attr("id");
		if (ACC.address.isTaxCalulationRequired(formId)) {
			if (customerHasSavedAddress.val() != 'true') {
				if(ACC.address.addressIsComplete(addressForm)){
					$.ajax({
						url: ACC.config.encodedContextPath + '/checkout/multi/billing-shipping-address/saveAddressAndCalculateTax',
						async: true,
						type: "POST",
						data: $('#wileyBillShippAddressForm').serialize() + "&changedFormId=" + formId,
						dataType: "html",
					}).done(function (data)
						{
							ACC.address.updateAddressForm(addressForm, data);
							ACC.address.updateOrderTotals($(data).find("div.order-details-info"));
							ACC.address.updatePaymentForm();
						})
				}
			}
		}
	},
	
	isTaxCalulationRequired: function(formId)
	{
		return (ACC.address.isShippingSameAsBilling() && (formId === 'billingAddress.form')) 
		|| (!ACC.address.isShippingSameAsBilling() && (formId === 'shippingAddress.form'))
		|| (ACC.address.isBillingOnly() && (formId === 'billingAddress.form'));
	},
	
	isShippingSameAsBilling: function()
	{
		return $('#sameAsBilling').is(':checked');
	},

	isBillingOnly: function()
	{
		var billingOnly =  $('#billingOnly').val();
        return ('true' === billingOnly );
	},

	getAddressForm: function(el){
		return $(el).closest("div[id$='Address.form']")
	},
	
	getAddressFormPrefix: function(addressForm){
		var formId = $(addressForm).attr("id");
		return formId.split(".")[0];
	},
	
	updateAddressForm: function(addressForm, data){
		if (ACC.address.isResponseLoginPage(data)){
			ACC.address.makeRedirectToLoginPage();
		} else {
			var formId = $(addressForm).attr("id");
			var $newAddressForm = $(data).find("[id='" + formId + "']");
			$(addressForm).html($newAddressForm.html());

			ACC.address.bindCountrySpecificAddressForm(addressForm);
		}
	},

	updateOrderTotals: function(data){
		if (ACC.address.isResponseLoginPage(data)){
			ACC.address.makeRedirectToLoginPage();	
		} else {
			$("div.order-details-info").replaceWith(data);
		}
	},

	updatePaymentForm: function(){
		var totalPriceValue = parseFloat($('#totalPriceValue').text());
		if(totalPriceValue == 0){
			$('.payment-method.panel').hide();
		}else{
			$('.payment-method.panel').show();
		}
	},

	isResponseLoginPage: function(data){
		return $(data).find("#wileyRegisterForm").length || $(data).find("#loginForm").length
	},
	
	makeRedirectToLoginPage: function(){
		window.location.href =  ACC.config.encodedContextPath + '/login/checkout';
	},
	
	copyElementBetweenForms: function(srcForm, destForm, elementId){
		var elementSelector = "[name$='" + elementId + "']";
		$(destForm).find(elementSelector).val($(srcForm).find(elementSelector).val());
	},
	
	addressIsComplete: function(addressForm)
	{
		var isCompete = true;
		$(addressForm).find('input:text, select').each(function(index, element){
			if(this.value === '' && !/addressLine2$/.test(this.id)){
				isCompete = false;
			}
		});
		return isCompete;
	},

	orderDetailsElementExists: function()
	{
		return $("div.order-details-info").length > 0;
	},

	bindToChangeAddressButton: function ()
	{
		$(document).on("click", '.summaryDeliveryAddress .editButton', ACC.address.handleChangeAddressButtonClick);
	},

	bindViewAddressBook: function ()
	{

		$(document).on("click",".js-address-book",function(e){
			e.preventDefault();

			ACC.colorbox.open("Saved Addresses",{
				href: "#addressbook",
				inline: true,
				width:"320px",
			});
			
		})

		
		$(document).on("click", '#summaryOverlayViewAddressBook', function ()
		{
			$('#summaryDeliveryAddressFormContainer').hide();
			$('#summaryOverlayViewAddressBook').hide();
			$('#summaryDeliveryAddressBook').show();
			ACC.colorbox.resize();
		});
	},
	
	showRemoveAddressFromBookConfirmation: function ()
	{
		$(document).on("click", ".removeAddressFromBookButton", function ()
		{
			var addressId = $(this).data("addressId");
			var popupTitle = $(this).data("popupTitle");

			ACC.colorbox.open(popupTitle,{
				inline: true,
				height: false,
				href: "#popup_confirm_address_removal_" + addressId,
				onComplete: function ()
				{

					$(this).colorbox.resize();
				}
			});

		})
	},

	backToListAddresses: function(){
		$(".addressBackBtn").on("click", function(){
			var sUrl = $(this).data("backToAddresses");
			window.location = sUrl;
		});
	},

	getCountryIsoForAddressForm: function(addressForm) {
		return addressForm.find($("[id$='.address.country']")).val();
	}
};
