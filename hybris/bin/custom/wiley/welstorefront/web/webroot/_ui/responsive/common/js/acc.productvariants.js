ACC.productcommon = {
	$forms:					$("form[id^=addToCartForm-]"),
	$currentForm:			$("form[id=addToCartForm-0]"),

	$price: 							function(){return ACC.productcommon.$currentForm.find("[id^='product-price']")},
	$addonsCheckboxes: 					function(){return ACC.productcommon.$currentForm.find("input[id^='product-addon-']")},
	$studentDiscountCheckboxes: 		function(){return ACC.productcommon.$currentForm.find("input[id^='student-discount-']")},
	$selectedVariantCode:   			function(){return ACC.productcommon.getSelectedVariantCode()},
	$partnerPrice: 						function(){return ACC.productcommon.$currentForm.find("[id^='partner-price']")},
	
	getSelectedVariantCode: function(){
		var selectedVariantCode =  ACC.productcommon.$currentForm.find("input[name='product'][id^='product-variant-']:checked").data('code');
		if (typeof selectedVariantCode === 'undefined'){
		 	selectedVariantCode = ACC.productcommon.$currentForm.find("input[name='product'][id^='product-variant-']").first().data('code');
		}
		return selectedVariantCode;
	},

    formatTotalsCurrency: function(amount)  {
		//current currency should be added to ACC.common (present in 'desktop' but is absent from 'responsive'
		var format = Currency.money_format[$("main").data("currencyIsoCode")];
		if (amount % 1 == 0) {
			format = format.replace('amount', 'amount_no_decimals');
		}
		return Currency.formatMoney(Number(amount).toFixed(2), format);
    },

    setCurrentForm: function(){
    	ACC.productcommon.$currentForm = $(this).closest("form");
    },

    synchronizeAction: function(){
		var elementId = $(this).prop('id');
		var elementIdCommon = elementId.substring(0,elementId.lastIndexOf("-"));
		var $form = $(this).closest("form");
		var baseProductCode = $form.data("base-product-code");
		ACC.productcommon.$forms.each(function(){
			var formIndex = $(this).data("index");
			var elementIdToTrigger = elementIdCommon + "-" + formIndex;
			var baseProductCodeToTrigger = $(this).data("base-product-code");
			if (elementIdToTrigger != elementId && baseProductCode == baseProductCodeToTrigger){
				$("#" + elementIdToTrigger).off("click", ACC.productcommon.synchronizeAction);
				$("#" + elementIdToTrigger).trigger("click");
				$("#" + elementIdToTrigger).on("click", ACC.productcommon.synchronizeAction);
			}
		});
		ACC.productcommon.$currentForm = $form;
    },
    
    isProductInFormMultidimensional: function($form){
    	if (typeof multidimensionalProducts === 'undefined'){
    		return false;
    	} else {
    		return multidimensionalProducts.indexOf($form.data("base-product-code")) > -1;
    	}
    }
}

ACC.productvariants = {
	$productsInputs: 		function(){return ACC.productcommon.$currentForm.find("input[id^='product-variant-']")},
	
	getProductRootNode: function($form){
		return productRootNodes[$form.data("base-product-code")];
	},

	bindAll: function() {
		ACC.productvariants.bindInput(ACC.productcommon.$addonsCheckboxes());
		ACC.productvariants.bindInput(ACC.productcommon.$studentDiscountCheckboxes());

		ACC.productcommon.$currentForm.on('submit', ACC.productcommon.setCurrentForm);
		ACC.productcommon.$currentForm.on('submit', ACC.productvariants.beforeAddToCart);

		ACC.productvariants.bindVariantInputs(ACC.productvariants.$productsInputs());
	},

	bindVariantInputsForSection: function(variantSection){
		var $variantInputs = $(variantSection).find("input[id^='product-variant-']");
		ACC.productvariants.bindVariantInputs($variantInputs);
	},

	bindInput: function($input){
		$input.on('click', ACC.productcommon.setCurrentForm);
		$input.on('click', ACC.productvariants.calculate);
		$input.on('click', ACC.productcommon.synchronizeAction);
	},

	bindVariantInputs: function(variantInputs){
		var $variantInputs = $(variantInputs);
		$variantInputs.off();
		$variantInputs.on('click', ACC.productcommon.setCurrentForm);
		$variantInputs.on('click', ACC.productvariants.renderChildVariantsSection);
		$variantInputs.on('click', ACC.productvariants.makeLastInputChecked);
		$variantInputs.on('click', ACC.productvariants.calculate);
		$variantInputs.on('click', ACC.productcommon.synchronizeAction);
	},

	makeFirstInputChecked: function(variantSection){
		$(variantSection).find("input[id^='product-variant-']:visible").first().prop( "checked", true );
	},

	makeLastInputChecked: function(){
		var checkedInputs = ACC.productcommon.$currentForm.find("input:checkbox[name='sku'][id^='product-variant-']:checked");
		if (checkedInputs.length < 1) {
			$(this).prop('checked', true);
		}
	},

	getChildVariantSection: function(variantsSection){
		return $(variantsSection).find("div[id^='variants-section-']").first();
	},

	getTemplate: function(node, viewName){
		return $(productVariantsTemplates).filter('#view-template-' + viewName).html();
	},

	renderTemplate: function(node, viewName){
		var template = ACC.productvariants.getTemplate(node,viewName);
		Mustache.parse(template);
		return ACC.productvariants.addFormIndex(Mustache.render(template, node));
	},

	addFormIndex: function(str){
		return str.replace(/formIndexHolder/g, ACC.productcommon.$currentForm.data("index"));
	},


	renderVariantsSection: function(currentVariantsSection, node){
  		if (node.nodes != null && node.nodes.length != 0){
			ACC.productvariants.getChildVariantSection(currentVariantsSection).remove();
			$(currentVariantsSection).append(ACC.productvariants.renderTemplate(node, "product"));

			ACC.productvariants.bindVariantInputsForSection(currentVariantsSection);
			ACC.productvariants.cacheNodes(node.nodes);
  			ACC.productvariants.renderVariantsSection(ACC.productvariants.getChildVariantSection(currentVariantsSection),node.nodes[0]);
  		}
	},

	renderChildVariantsSection: function(){
		var currentVariantsSection = $(this).closest("div[id^='variants-section-']");
		var currentNode = ACC.productvariants.getVariantNode($(this).data("code"));
		if (currentNode != null && currentNode.nodes != null){
			var uncheckedVariantIds = ACC.productcommon.$currentForm.find("input[id^='product-variant-'][name='sku'][type='checkbox']:not(:checked)").map(function(){
				return $(this).attr("id");
			});
			ACC.productvariants.renderVariantsSection(currentVariantsSection, currentNode);
			uncheckedVariantIds.each(function(){
				$('#'+this).prop('checked', false);
			});
		}
	},

	cacheNodes: function(nodes){
		for (var i = 0; i < nodes.length; ++i){
			var node = nodes[i];
			cachedProductNodes[node.code + "-" + ACC.productcommon.$currentForm.data("index")] = node;
		}
	},

	initiallyCacheNodes: function(node){
		if (node.nodes != null && node.nodes.length > 0) {
			ACC.productvariants.cacheNodes(node.nodes);
			var selectedVariantCode = ACC.productcommon.$selectedVariantCode();
			node.nodes.forEach(function(node){
				if(node.code == selectedVariantCode){
					ACC.productvariants.initiallyCacheNodes(node);
				}
			});
		}
	},

	createBackReferences: function(node){
		if (node.nodes != null){
			for (var i =0; i < node.nodes.length; ++i){
				node.nodes[i].parent = node;
				ACC.productvariants.createBackReferences(node.nodes[i]);
			}
		}
	},

	isOnlyOneVariantExist: function(){
		return ACC.productcommon.$currentForm.find("input:checkbox[name='sku'][id^='product-variant-']").length == 1;
	},
	
	isAllVariantInputsSelected: function(){
		return ACC.productcommon.$currentForm.find("input:checkbox[name='sku'][id^='product-variant-']:checked").length ==
			ACC.productcommon.$currentForm.find("input:checkbox[name='sku'][id^='product-variant-']").length;
	},

	isSetVariantExisted: function(){
		return ACC.productcommon.$currentForm.find("input:checkbox[name='set'][id^='product-variant-']").length > 0;
	},
	
	makeSetVariantCheckedIfRequired: function(){
		var shouldSetBeChecked = ACC.productvariants.isAllVariantInputsSelected() && ACC.productvariants.isSetVariantExisted();
		ACC.productcommon.$currentForm.find("input:checkbox[name='set'][id^='product-variant-']").prop('checked', shouldSetBeChecked);
	},

	getSelectedVariantCodes: function(){
		var variantCodes = [];
		if (ACC.productvariants.isAllVariantInputsSelected() && ACC.productvariants.isSetVariantExisted()){
			variantCodes.push(ACC.productcommon.$currentForm.find("input:checkbox[name='set'][id^='product-variant-']").data("code"));
		}else {
			if (ACC.productvariants.isOnlyOneVariantExist()) {
				ACC.productcommon.$currentForm.find("input:checkbox[name='sku'][id^='product-variant-']").prop('checked', true);
			}
			ACC.productcommon.$currentForm.find("input[name='sku'][id^='product-variant-']:checked").each(function(){
				variantCodes.push($(this).data("code"));
			});
		}
			
		return variantCodes;
	},

	getSelectedAddonCodes: function(){
		var addonCodes = [];
		ACC.productcommon.$currentForm.find("input:checkbox[id^='product-addon-']:checked").each(function(){
			addonCodes.push($(this).val());
		});
		return addonCodes;
	},

	getNodePath: function(node){
		if (node.parent != null){
			var path = ACC.productvariants.getNodePath(node.parent);
			var delimiter = "";
			if (path != ""){
				delimiter = "/";
			}
			return path + delimiter + node.code;
		}
		return "";
	},

	findNodeByPath: function(rootNode, path){
		var codes = path.split("/");
		var node = rootNode;
		for (var i = 0; i < codes.length; ++i){
			var foundNode = ACC.productvariants.findNodeByCode(node.nodes, codes[i], ACC.productvariants.nodeNotFoundStrategy_returnFirstElement);
			if (foundNode == null){
				break;
			}
			node = foundNode;
		}
		return node;
	},

	findNodeByCode: function(nodes, code, nodeNotFoundStrategy){
		if (nodes == null || nodes.length == 0){
			return null;
		}
		for (var i = 0; i < nodes.length; ++i){
			if (nodes[i].code == code){
				return nodes[i];
			}
		}

		if (nodeNotFoundStrategy == null){
			nodeNotFoundStrategy = ACC.productvariants.nodeNotFoundStrategy_default;
		}
		return nodeNotFoundStrategy(nodes,code);
	},

	nodeNotFoundStrategy_returnFirstElement: function(nodes){
		/* in case e.g. addon doesn't have eBook type, use first value from array */
		return nodes[0];
	},

	nodeNotFoundStrategy_default: function(){
		return null;
	},

	getVariantNode: function(variantCode){
		return cachedProductNodes[variantCode  + "-" + ACC.productcommon.$currentForm.data("index")];
	},

	getAddonNode: function(variantCode, addonCode){
		var variantNode = ACC.productvariants.getVariantNode(variantCode);
		var path  = ACC.productvariants.getNodePath(variantNode);
		var addon = ACC.productvariants.findNodeByCode(ACC.productvariants.getProductRootNode(ACC.productvariants.$currentForm).addons.nodes, addonCode);
		return ACC.productvariants.findNodeByPath(addon, path);
	},

	getVariantNodes: function(variantCodes){
		var variantNodes = []
		variantCodes.forEach(function(variantCode){
			variantNodes.push(ACC.productvariants.getVariantNode(variantCode));
		});
		return variantNodes;
	},

	getAddonNodes: function(variantCodes, addonCodes){
		var addonNodes = []
		addonCodes.forEach(function(addonCode){
			variantCodes.forEach(function(variantCode){
				var addonNode = ACC.productvariants.getAddonNode(variantCode, addonCode);
				if (addonNodes.indexOf(addonNode) == -1){
					addonNodes.push(addonNode);
				}
			});
		});
		return addonNodes;
	},

	calculateVariants: function(isPartner){
		var totalPrice = 0;
		var variantCodes = ACC.productvariants.getSelectedVariantCodes();
		var variantNodes = ACC.productvariants.getVariantNodes(variantCodes);
		variantNodes.forEach(function(variantNode){
			var variantPrice = ACC.productvariants.getPrice(variantNode, isPartner);
			if($.isNumeric(variantPrice))
			{
				totalPrice += variantPrice;
			}
			else
			{
				totalPrice += ACC.productvariants.getPrice(variantNode, false);
			}
		});
		return totalPrice;
	},

	calculateAddons: function(isPartner){
		var totalPrice = 0;
		var variantCodes = ACC.productvariants.getSelectedVariantCodes();
		var addonCodes = ACC.productvariants.getSelectedAddonCodes();
		var addonNodes = ACC.productvariants.getAddonNodes(variantCodes, addonCodes);
		addonNodes.forEach(function(addonNode){
			if (addonNode != null){
				totalPrice += ACC.productvariants.getPrice(addonNode, isPartner);
			}
		});
		return totalPrice;
	},


    calculate: function() {
		var totalPrice = 0;
		var partnerTotalPrice = 0;
		totalPrice += ACC.productvariants.calculateVariants(false);
		totalPrice += ACC.productvariants.calculateAddons(false);
		partnerTotalPrice += ACC.productvariants.calculateVariants(true);
		partnerTotalPrice += ACC.productvariants.calculateAddons(true);
        ACC.productcommon.$price().html(ACC.productcommon.formatTotalsCurrency(totalPrice));
		
		if(!$.isNumeric(partnerTotalPrice))
		{
			partnerTotalPrice = totalPrice;
		}
        ACC.productcommon.$partnerPrice().html(ACC.productcommon.formatTotalsCurrency(partnerTotalPrice));
    },


    beforeAddToCart: function(){
   		var addonsVariantsSection = ACC.productcommon.$currentForm.find(".product-addons");
		var variantCodes = ACC.productvariants.getSelectedVariantCodes();
		var addonCodes = ACC.productvariants.getSelectedAddonCodes();
		var addonNodes = ACC.productvariants.getAddonNodes(variantCodes, addonCodes);
		var addonRootNode=[];
		addonRootNode.nodes = addonNodes;
		ACC.productcommon.$currentForm.find("#variants-section-addons").remove();
		ACC.productcommon.$currentForm.find(".product-addons-content").append(ACC.productvariants.renderTemplate(addonRootNode, "addons"));
		ACC.productvariants.makeSetVariantCheckedIfRequired();
    },

    getPrice : function(element, isPartner) {
		var price;
		var elementValue = element.value;
		if (ACC.productcommon.$studentDiscountCheckboxes().is(':checked')) {
			price = elementValue.studentPrice;
		} else if(isPartner) {
			price = elementValue.partnerPrice;
		} else {
			price = elementValue.price;
		}
		return price;
	}
};

ACC.productsingle = {

	bindAll: function() {
		ACC.productsingle.bindInput(ACC.productcommon.$addonsCheckboxes());
		ACC.productsingle.bindInput(ACC.productcommon.$studentDiscountCheckboxes());
		ACC.productcommon.$currentForm.on('submit', ACC.productcommon.setCurrentForm);
	},

	bindInput: function($input){
		$input.on('click', ACC.productcommon.setCurrentForm);
		$input.on('click', ACC.productsingle.calculate);
		$input.on('click', ACC.productcommon.synchronizeAction);
	},

    calculate: function() {
		var totalPrice = 0;
		var partnerTotalPrice = 0;
		totalPrice += ACC.productsingle.calculateProduct(false);
		totalPrice += ACC.productsingle.calculateAddons(false);
		partnerTotalPrice += ACC.productsingle.calculateProduct(true);
		partnerTotalPrice += ACC.productsingle.calculateAddons(true);
		ACC.productcommon.$price().html(ACC.productcommon.formatTotalsCurrency(totalPrice));
		ACC.productcommon.$partnerPrice().html(ACC.productcommon.formatTotalsCurrency(partnerTotalPrice));
	},

	calculateProduct: function(isPartner){
		return parseFloat(ACC.productsingle.getPrice(ACC.productcommon.$currentForm.find("input:hidden[id^='product-single-']"), isPartner));
	},

	calculateAddons: function(isPartner){
		var totalPrice = 0;
		ACC.productcommon.$currentForm.find("input:checkbox[id^='product-addon-']:checked").each(function(){
			var addonPrice = parseFloat(ACC.productsingle.getPrice($(this), isPartner));
			if(addonPrice)
			{
				totalPrice += addonPrice;
			}
			else
			{
				totalPrice += parseFloat(ACC.productsingle.getPrice($(this), false));;
			}
		});
		return totalPrice;
	},

	getPrice : function(element, isPartner) {
		var price;
		if (ACC.productcommon.$studentDiscountCheckboxes().is(':checked')) {
			price = $(element).data("student-price");
		} else if(isPartner) {
			price = $(element).data("partner-price");
		} else {
			price = $(element).data("price");
		}

		if (price == "") {
			price = 0;
		}

		return price;
	}
}




$(document).ready(function() {
	ACC.productcommon.$forms.each(function(){
		ACC.productcommon.$currentForm = $(this);
		if (ACC.productcommon.isProductInFormMultidimensional($(this))){
			if (typeof cachedProductNodes === 'undefined') 
			{
				cachedProductNodes = {};
			}
			ACC.productvariants.bindAll();
			ACC.productvariants.createBackReferences(ACC.productvariants.getProductRootNode($(this)).product);
			ACC.productvariants.createBackReferences(ACC.productvariants.getProductRootNode($(this)).addons);
			ACC.productvariants.initiallyCacheNodes(ACC.productvariants.getProductRootNode($(this)).product);
			ACC.productvariants.calculate();
		} else{
			ACC.productsingle.bindAll();
			ACC.productsingle.calculate();
		}
	});
});
