ACC.imagegallery = {

	_autoload: [
		"bindImageGallery"
	],

	bindImageGallery: function() {
		$(".js-gallery").each(function() {
			var $image = $(this).find(".js-gallery-image");
			var $carousel = $(this).find(".js-gallery-carousel");

			$image.owlCarousel({
				singleItem : true,
				pagination: true,
				navigation: true,
				lazyLoad: true,
				navigationText: ["<span class='glyphicon glyphicon-chevron-left'></span>", "<span class='glyphicon glyphicon-chevron-right'></span>"],

				afterMove: function() {
					var player = $(this.$owlItems[this.prevItem]).find('.video-js');

					if(player.length) {
						videojs(player[0].id).pause();
					}
				},

				afterAction: function() {
					ACC.imagegallery.syncPosition($image,$carousel,this.currentItem);
					$image.data("zoomEnable", true);
				},

				startDragging: function() {
					$image.data("zoomEnable",false);
				},

				afterLazyLoad: function(e) {
					var b = $image.data("owlCarousel") || {};

					if(!b.currentItem) {
						b.currentItem = 0
					}

					var $e = $($image.find("img.lazyOwl")[b.currentItem]);
					startZoom($e.parent())
				}
			});

			$carousel.owlCarousel({
				navigation: true,
				navigationText : ["<span class='glyphicon glyphicon-chevron-left'></span>", "<span class='glyphicon glyphicon-chevron-right'></span>"],
				pagination: false,
				items: 2,
				itemsDesktop: [5000, 5],
				itemsDesktopSmall: [1200, 5],
				itemsTablet: [1024, 4],
				itemsTabletSmall: [768, 3],
				itemsMobile: [640, 3],
				lazyLoad: true
			});

			$carousel.on("click","a.item", function(e) {
				e.preventDefault();
				var index = $(this).parent(".owl-item").data("owlItem");
				$image.trigger("owl.goTo",index);
				createVideoPlayerIfRequired(index);
			});

			$(this).find("div.owl-controls").on("click", function(e) {
				var $owlPagination = $(this).find("div.owl-pagination");
				var index = $owlPagination.find("div.active").index();
				createVideoPlayerIfRequired(index);
			});

			function createVideoPlayerIfRequired(index) {
				var $videoContainer = $($image.find(".owl-item")[index]).find(".brightcove-video-container");
				if($videoContainer.length !== 0) {
					$videoContainer.find("object").addClass("BrightcoveExperience");
					brightcove.createExperiences();
 				}
			}

			/* this call is required if video is the first item in gallery.*/
			createVideoPlayerIfRequired(0);

			function startZoom(e) {
				$(e).zoom({
					url: $(e).find("img.lazyOwl").data("zoomImage"),
					touch: true,
					on: "grab",
					touchduration:300,

					onZoomIn:function() {
					},

					onZoomOut:function() {
						var owl = $image.data('owlCarousel');
						owl.dragging(true);
						$image.data("zoomEnable",true);
					},

					zoomEnableCallBack:function() {
						var bool = $image.data("zoomEnable");

						var owl = $image.data('owlCarousel');
						if(bool==false) {
							owl.dragging(true);
						} else {
							owl.dragging(false);
						}
						return bool;
					}
				});
			}
		})
	},

	syncPosition: function($image,$carousel,currentItem) {
		$carousel.trigger("owl.goTo",currentItem);
	}
};