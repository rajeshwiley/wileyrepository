ACC.langcurrency = {

	_autoload: [
		"bindLangCurrencySelector"
	],

	bindLangCurrencySelector: function (){

		$('#lang-selector').change(function(){
			$('#lang-form').submit();
		});


		$('#currency-selector').on('click', 'li', function () {
			var currency = $(this).data().value;
			var $currencyForm = $("#currency-form");

			$currencyForm.find("input[name='code']").val(currency);

			$.blockUI("body");

			$('#currency-form').submit();
			return false;
		});

	}
};