PaymentSession.configure({
    fields: {
    // ATTACH HOSTED FIELDS TO PAYMENT PAGE FOR A CREDIT CARD
        card: {
        	number: "#card-number-input",
        	securityCode: "#security-code-input",
        	expiryMonth: "#expiry-month-select",
        	expiryYear: "#expiry-year-select"
        }
    },
	// MITIGATION OPTIONS
    frameEmbeddingMitigation: ["javascript","x-frame-options"],
    callbacks: {
		// HANDLE INITIALIZATION RESPONSE
        initialized: function(response) {
        	if (response.status != "ok") {
				HostedSession.showGlobalAlert(HostedSession.errorMessages.processingError);
			}
        },
        formSessionUpdate: function(response) {
        	// HANDLE RESPONSE FOR UPDATE SESSION
            if (response.status) {
				var supportedCardTypes = $("input[id='supportedCardTypes']").val();

                if ("ok" == response.status) {
					if (supportedCardTypes.indexOf(response.sourceOfFunds.provided.card.brand) < 0) {
						HostedSession.validateField(true, '#card-number', HostedSession.errorMessages.paymentCardNotSupported);
					} else{
						if (response.sourceOfFunds.provided.card.securityCode) {

                            if(ACC.paymentDetails.addressValidationSuccess) {
                            	ACC.paymentDetails.cardValidationSuccess = true;
								HostedSession.submitForm(response);
							}
						}else{
							HostedSession.validateField(true, '#security-code', HostedSession.errorMessages.securityCodeNotProvided);
						}
					}
                } else if ("fields_in_error" == response.status)  {
                	HostedSession.validation(response);
                } else {
					HostedSession.showGlobalAlert(HostedSession.errorMessages.processingError);
                }
            } else {
				HostedSession.showGlobalAlert(HostedSession.errorMessages.processingError);
            }

			if (!ACC.paymentDetails.addressValidationSuccess || !ACC.paymentDetails.cardValidationSuccess) {
				ACC.common.enableButton('saveBilling');
			}
        }
      }
  });


