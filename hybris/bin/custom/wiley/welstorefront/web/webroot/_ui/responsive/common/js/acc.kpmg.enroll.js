ACC.kpmgEnroll = {
    _autoload: [
        "bindForms"
    ],

    bindForms: function() {
        $('#kpmgEnrollmentForm').submit(function(e) {
            var variantProductValue = $('input[id^="product-variant-"]:checked').val() || $('input[id^="product-"]').val() ||'';
            $('#productVariantCodeInput').val(variantProductValue);
        });
    }
};