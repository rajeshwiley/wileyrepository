ACC.cart = {

	_autoload: [
		"bindHelp",
		"cartRestoration",
		"bindVoucherRemove"
	],

	bindHelp: function(){
		$(document).on("click",".js-cart-help",function(e){
			e.preventDefault();
			var title = $(this).data("help");
			ACC.colorbox.open(title,{
				html:$(".js-help-popup-content").html(),
				width:"300px"
			});
		})
	},

	cartRestoration: function(){
		$('.cartRestoration').click(function(){
			var sCartUrl = $(this).data("cartUrl");
			window.location = sCartUrl;
		});
	},
	
 bindVoucherRemove: function(){
  var countOfVouchers = $('.subscriptions-subtotal-value').children().length;
  for (var i = 0; i < countOfVouchers; i++)
  {
   $('#voucher-remove-button_'+i).on('click', function(){
    $(this).closest("#voucherForm").submit();
   });
  }
 }

};