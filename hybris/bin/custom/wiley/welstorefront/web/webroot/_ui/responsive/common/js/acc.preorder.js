ACC.preorder = {

	_autoload: [
		"bindPreOrderAndActiveProducts"
	],

	bindPreOrderAndActiveProducts: function() {
	    $(".js-current-product-type").on("click", function(e) {
	         $(".js-current-product-type").each(function() {
                 this.disabled = true;
             });
             window.location = ACC.config.encodedContextPath + this.dataset.url;
        });
	}
}