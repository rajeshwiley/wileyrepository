ACC.studentFlow = {

	_autoload: [
		'onloadBindForm'
	],

	$studentVerificationForm: $('#studentVerificationForm'),

	onloadBindForm: function () {
		ACC.studentFlow.bindForm(ACC.studentFlow.$studentVerificationForm);
	},

	bindForm: function(studentVerificationForm) {
		$(studentVerificationForm).find('#country').on('change', ACC.studentFlow.handleCountryChanged);
		$(studentVerificationForm).find('#region').on('change', ACC.studentFlow.handleRegionChanged);
	},

	handleCountryChanged: function () {
		ACC.studentFlow.$studentVerificationForm.find('#region').val('');
		ACC.studentFlow.handleRegionChanged();
	},

	handleRegionChanged: function () {
        ACC.studentFlow.$studentVerificationForm.find('#university').val('');
        ACC.studentFlow.$studentVerificationForm.find('#universityText').val('');
        ACC.studentFlow.updateFormAjax();
    },

	updateFormAjax: function() {
		var urlUpdateForm = ACC.config.encodedContextPath + '/checkout/multi/student-verification/update-form';
		$.ajax({
			url: urlUpdateForm,
			async: true,
			data: $('#studentVerificationForm').serialize(),
			dataType: 'html'
		}).done(function (data) {
			ACC.studentFlow.updateStudentVerificationForm(ACC.studentFlow.$studentVerificationForm, data);
		});
	},

	updateStudentVerificationForm: function(studentVerificationForm, data){
		if (ACC.studentFlow.isResponseLoginPage(data)){
			ACC.studentFlow.makeRedirectToLoginPage();
		} else {
			$(studentVerificationForm).html(data);
			ACC.studentFlow.bindForm(studentVerificationForm);
		}
	},

	isResponseLoginPage: function(data) {
		return $(data).find('#wileyRegisterForm').length || $(data).find('#loginForm').length
	},

	makeRedirectToLoginPage: function() {
		window.location.href =  ACC.config.encodedContextPath + '/login/checkout';
	}
};

