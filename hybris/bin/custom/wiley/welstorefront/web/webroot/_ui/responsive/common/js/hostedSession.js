var HostedSession = {
	errorMessages : '',
	validation : function(response){
		HostedSession.validateField(response.errors.cardNumber,'#card-number', HostedSession.errorMessages.paymentCardNumberInvalid);
		HostedSession.validateField(response.errors.expiryYear, '#expiry-year', HostedSession.errorMessages.expirationDateInvalid);
		HostedSession.validateField(response.errors.expiryMonth,'#expiry-month', HostedSession.errorMessages.expirationDateInvalid);
		HostedSession.validateField(response.errors.securityCode,'#security-code', HostedSession.errorMessages.securityCodeInvalid);
	},
	showGlobalAlert : function(message){
		$('#global-error-message-list').append( message + "<br/>");
		$("#global-alert").removeClass('hidden');
	},
	validateField : function(fieldName, fieldId, message){
       if (fieldName) {
       		$(fieldId).addClass("has-error");
       		HostedSession.showGlobalAlert(message);
    	}
    },
	submitForm : function(response){
		$("input[name='sessionId']").val(response.session.id);
		$('#wileyBillShippAddressForm').submit();
	},
	clearErrors : function(){
		$('#global-error-message-list').text("");
		$('[id*="\\.errors"]').each(function() {
			$(this).text('');
		});
		$('.has-error').each(function() {
			$(this).removeClass('has-error');
		});
		$("#global-alert").addClass("hidden");
	}
};
