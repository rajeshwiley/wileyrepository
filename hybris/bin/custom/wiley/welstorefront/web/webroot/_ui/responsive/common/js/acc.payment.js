ACC.payment = {
		
		activateSavedPaymentButton: function(){

			$(document).on("click",".js-saved-payments",function(e){
				e.preventDefault();
				
				var title = $("#savedpaymentstitle").html();
				
				$.colorbox({
					href: "#savedpaymentsbody",
					inline:true,
					maxWidth:"100%",
					opacity:0.7,
					width:"320px",
					title: title,
					close:'<span class="glyphicon glyphicon-remove"></span>',
					onComplete: function(){
					}
				});
			})
		},
		bindPaymentCardTypeSelect: function ()
		{
			ACC.payment.filterCardInformationDisplayed();
			$("#card_cardType").change(function ()
			{
				var cardType = $(this).val();
				if (cardType == '024')
				{
					$('#startDate, #issueNum').show();
				}
				else
				{
					$('#startDate, #issueNum').hide();
				}
			});
		},
		filterCardInformationDisplayed: function ()
		{
			var cardType = $('#card_cardType').val();
			if (cardType == '024')
			{
				$('#startDate, #issueNum').show();
			}
			else
			{
				$('#startDate, #issueNum').hide();
			}
		},
		
		bindPaymentMethodSelect: function()
		{
			$("#creditCard").click(function(){
				$("#saveBilling").text("Continue to Order Review")
				$('#hostedSessionFields').show()
			});
			$("#paypal").click(function(){
				$("#saveBilling").text("Continue to PayPal")
				$('#hostedSessionFields').hide();
			});
		},
		
		bindWhatIsPayPalLink: function()
		{
			$("#what-is-paypal-link").click(function(){
				window.open(
					'https://www.paypal.com/cgi-bin/webscr?cmd=xpt/Marketing/popup/OLCWhatIsPayPal-outside',
					'WIPaypal',
					'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060, height=700'
				);
				return false;
			});
		}
}

$(document).ready(function () {
	with (ACC.payment) {
		activateSavedPaymentButton();
		bindPaymentCardTypeSelect();
		bindPaymentMethodSelect();
		bindWhatIsPayPalLink();
	}
});
	
	
	
