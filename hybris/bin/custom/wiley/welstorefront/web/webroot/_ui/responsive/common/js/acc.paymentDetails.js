ACC.paymentDetails = {
	_autoload: [
		"showRemovePaymentDetailsConfirmation"
	],

	addressValidationSuccess : false,
	cardValidationSuccess : false,

	showBillingShippingAddressFormErrors : function(page) {
		var newDivElement = $( '<div></div>' );
		newDivElement.html(page);
		var newBillingShippingAddress = $("#billingShippingAddress", newDivElement);
		$("#billingShippingAddress").replaceWith(newBillingShippingAddress);
		ACC.address.bindCountrySpecificAddressForms();
	},
	billingShippingAddresseFormValidation : function() {
	    $.ajax({
	        url: ACC.config.encodedContextPath + '/checkout/multi/billing-shipping-address/billingShippingAddressValidation',
            async : false,
            type: 'POST',
	        data: $('#wileyBillShippAddressForm').serialize(),
            dataType: "html",
	        success: function(data) {
                ACC.paymentDetails.addressValidationSuccess = true;
			},
            error: function(data, thrownError) {
                if (data.status  == 400) {
					HostedSession.showGlobalAlert(HostedSession.errorMessages.globalAlertCorrectMessages);
					if(data.responseText.indexOf("billingShippingAddress") > -1) {
                        ACC.paymentDetails.showBillingShippingAddressFormErrors(data.responseText);
                    }
                } else {
                    console.log(thrownError);
                }
            }
	    });
	},

    submitBillingShippingForm : function() {
		$('#wileyBillShippAddressForm').submit();
	},
	billingShippingFormContinueEventHandling : function(event) {
		event.preventDefault();
		HostedSession.clearErrors();
		ACC.common.disableButton('saveBilling');
		ACC.paymentDetails.billingShippingAddresseFormValidation();

		var totalPriceValue = parseFloat($('#totalPriceValue').text());
		if(totalPriceValue > 0){
			ACC.paymentDetails.processNonZeroOrder();
		}else{
			ACC.paymentDetails.processAddressValidationResult();
		}
	},

	processNonZeroOrder : function(){
		var isCreditCardPayment = $('#creditCard').is(':checked');
		if (isCreditCardPayment){
			PaymentSession.updateSessionFromForm('card');
		} else {
			ACC.paymentDetails.processAddressValidationResult();
		}
	},
	processAddressValidationResult : function(){
		if (ACC.paymentDetails.addressValidationSuccess){
			ACC.paymentDetails.submitBillingShippingForm();
		}else{
			ACC.common.enableButton('saveBilling');
		}
	},

	showRemovePaymentDetailsConfirmation: function ()
	{
		ACC.address.updatePaymentForm();

		$(document).on("click", ".removePaymentDetailsButton", function ()
		{
			var paymentId = $(this).data("paymentId");
			var popupTitle = $(this).data("popupTitle");

			ACC.colorbox.open(popupTitle,{
				inline: true,
				transition: "elastic",
				href: "#popup_confirm_payment_removal_" + paymentId,
				onComplete: function ()
				{
					$(this).colorbox.resize();
				}
			});

		});

		$(".close").on("click", function(){
		    $('.global-alerts').addClass('hidden');
		});

        $('#saveBilling').click(function(event) {
        	ACC.paymentDetails.billingShippingFormContinueEventHandling(event);
        });

        $('#wileyBillShippAddressForm').submit(function(event) {
        	ACC.common.disableButton('saveBilling');
        });

        $('#wileyBillShippAddressForm').on('keydown', 'input, select', function(event) {
            if(event.which == 13)  {
            	ACC.paymentDetails.billingShippingFormContinueEventHandling(event);	
            }
        });
	}
}

