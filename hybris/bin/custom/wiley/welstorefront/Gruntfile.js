module.exports = function(grunt) {
    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        buildkit_path: '../../../../../buildkit/<%= pkg.name %>',
        watch: {
            less: {
                files: [
                    'web/webroot/WEB-INF/_ui-src/shared/less/variableMapping.less',
                    'web/webroot/WEB-INF/_ui-src/shared/less/generatedVariables.less',
                    'web/webroot/WEB-INF/_ui-src/**/themes/**/less/*',
                    'web/webroot/WEB-INF/_ui-src/responsive/lib/ybase-*/less/*'
                ],
                tasks: ['less']
            },
            fonts: {
                files: ['web/webroot/WEB-INF/_ui-src/**/themes/**/fonts/*'],
                tasks: ['sync:syncfonts']
            },
            ybasejs: {
                files: ['web/webroot/WEB-INF/_ui-src/responsive/lib/ybase-0.1.0/js/**/*.js'],
                tasks: ['sync:syncybase']
            },
            jquery: {
                files: ['web/webroot/WEB-INF/_ui-src/responsive/lib/jquery*.js'],
                tasks: ['sync:syncjquery']
            },
			customjs: {
				files: ['web/webroot/WEB-INF/_ui-src/responsive/lib/js/*.js'],
				tasks: ['sync:syncustomjs']
			},
            images: {
                files: ['web/webroot/WEB-INF/_ui-src/**/themes/**/images/*'],
                tasks: ['sync:syncimages']
            },
            jade: {
                files: ['<%= buildkit_path %>/jade/**/*.jade'],
                tasks: ['jade'],
                options: {
                    nospawn: true
                }
            }
        },

        less: {
            default: {
                files: [
                    {
                        expand: true,
                        cwd: 'web/webroot/WEB-INF/_ui-src/',
                        src: ['**/themes/**/less/style.less', '**/themes/**/less/ybase-ie9.less', '**/themes/**/less/style-ie9.less'],
                        dest: 'web/webroot/_ui/',
                        ext: '.css',
                        rename:function(dest,src){
                            var nsrc = src.replace(new RegExp("/themes/(.*)/less"),"/theme-$1/css");
                            return dest+nsrc;
                        }
                    }
                ]
            }
        },

        jade: {
            compile: {
                options: {
                    client: false,
                    pretty: true
                },
                files: [ {
                    cwd: "<%= buildkit_path %>/jade/",
                    src: "*.jade",
                    dest: "<%= buildkit_path %>/pages/",
                    expand: true,
                    ext: ".html"
                } ]
            }
        },

        sync : {
            syncfonts: {
                files: [{
                    expand: true,
                    cwd: 'web/webroot/WEB-INF/_ui-src/',
                    src: '**/themes/**/fonts/*',
                    dest: 'web/webroot/_ui/',
                    rename:function(dest,src){
                        var nsrc = src.replace(new RegExp("/themes/(.*)"),"/theme-$1");
                        return dest+nsrc;
                    }
                }]
            },
            syncybase: {
                files: [{
                    cwd: 'web/webroot/WEB-INF/_ui-src/responsive/lib/ybase-0.1.0/js/',
                    src: '**/*.js',
                    dest: 'web/webroot/_ui/responsive/common/js'
                }]
            },
            syncjquery: {
                files: [{
                    cwd: 'web/webroot/WEB-INF/_ui-src/responsive/lib',
                    src: 'jquery*.js',
                    dest: 'web/webroot/_ui/responsive/common/js'
                }]
            },
			syncustomjs: {
				files: [{
					cwd: 'web/webroot/WEB-INF/_ui-src/responsive/lib/js/',
					src: '*.js',
					dest: 'web/webroot/_ui/responsive/common/js'
				}]
			},
            syncimages: {
                files: [{
                    expand: true,
                    cwd: 'web/webroot/WEB-INF/_ui-src/',
                    src: '**/themes/**/images/*',
                    dest: 'web/webroot/_ui/',
                    rename:function(dest,src){
                        var nsrc = src.replace(new RegExp("/themes/(.*)"),"/theme-$1");
                        return dest+nsrc;
                    }
                }]
            }
        }

    });

    // Plugins
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks("grunt-contrib-jade");
    grunt.loadNpmTasks('grunt-sync');


    // Default task(s).
    grunt.registerTask('default', ['jade', 'less', 'sync', 'watch']);
};