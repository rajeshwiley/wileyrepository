package com.wiley.welags.storefrontcommons.controllers.pages.checkout.steps;

import com.wiley.facades.welags.order.WelAgsCheckoutFacade;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.CheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.facades.student.WileyStudentVerificationFacade;
import com.wiley.facades.student.data.StudentVerificationData;
import com.wiley.facades.university.data.UniversityData;
import com.wiley.welags.storefrontcommons.controllers.ControllerConstants;
import com.wiley.welags.storefrontcommons.forms.StudentVerificationForm;


/**
 * Created by Uladzimir_Barouski on 3/1/2016.
 */
@RequestMapping(value = "/checkout/multi/student-verification")
public class StudentVerificationCheckoutStepController extends AbstractWelAgsCheckoutStepController
{
	private static final String STUDENT_VERIFICATION = "student-verification";
	private static final String STUDENT_VERIFICATION_CMS_PAGE_LABEL = "studentVerificationPage";
	private static final String STUDENT_VERIFICATION_FORM_ID = "studentVerificationForm";
	protected static final String PAYPAL_REDIRECT = REDIRECT_PREFIX + "/paypal/checkout/hop/response";

	@Resource
	private WileyStudentVerificationFacade wileyStudentVerificationFacade;

	@Resource
	private Validator studentVerificationFormValidator;

	@Autowired
	private WileyStudentVerificationFacade studentFacade;

	@Resource
	private WelAgsCheckoutFacade checkoutFacade;


	@Value("${default.country.isocode}")
	private String defaultCountryIsocode;

	@Override
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	@RequireHardLogIn
	@PreValidateCheckoutStep(checkoutStep = STUDENT_VERIFICATION)
	public String enterStep(final Model model, final RedirectAttributes redirectAttributes)
			throws CMSItemNotFoundException, CommerceCartModificationException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(STUDENT_VERIFICATION_CMS_PAGE_LABEL));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(STUDENT_VERIFICATION_CMS_PAGE_LABEL));
		model.addAttribute(WebConstants.BREADCRUMBS_KEY,
				getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.studentVerification.breadcrumb"));

		setCheckoutStepLinksForModel(model, getCheckoutStep());

		model.addAttribute(STUDENT_VERIFICATION_FORM_ID, new StudentVerificationForm());

		return ControllerConstants.Views.Pages.MultiStepCheckout.STUDENT_VERIFICATION_PAGE;
	}

	@RequestMapping(value = "/update-form", method = RequestMethod.GET)
	@RequireHardLogIn
	public String getCountryAddressForm(final StudentVerificationForm studentVerificationForm, final Model model)
			throws CMSItemNotFoundException
	{
		prepareCustomDataForPage(model, studentVerificationForm);
		storeCmsPageInModel(model, getContentPageForLabelOrId(STUDENT_VERIFICATION_CMS_PAGE_LABEL));
		model.addAttribute(STUDENT_VERIFICATION_FORM_ID, studentVerificationForm);

		return ControllerConstants.Views.Fragments.Checkout.STUDENT_VERIFICATION_FORM;
	}

	private void prepareCustomDataForPage(final Model model, final StudentVerificationForm studentVerificationForm)
	{

		model.addAttribute("isocodeUSA", defaultCountryIsocode);
		final String countryIso = studentVerificationForm.getCountryIso();
		model.addAttribute("countryIso", countryIso);
		if (countryIso != null && StringUtils.equalsIgnoreCase(defaultCountryIsocode, countryIso))
		{
			model.addAttribute("regions", getI18NFacade().getRegionsForCountryIso(countryIso));
			final String regionIso = studentVerificationForm.getRegionIso();
			if (regionIso != null)
			{
				List<UniversityData> universities = wileyStudentVerificationFacade.getUniversitiesByCountryAndRegionIso(
						countryIso, regionIso);
				model.addAttribute("universities", universities);
			}
		}
	}

	@Override
	public String back(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().previousStep();
	}

	@Override
	public String next(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().nextStep();
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@RequireHardLogIn
	public String add(final StudentVerificationForm form, final BindingResult bindingResult, final Model model)
			throws CMSItemNotFoundException
	{
		studentVerificationFormValidator.validate(form, bindingResult);
		if (bindingResult.hasErrors())
		{
			GlobalMessages.addErrorMessage(model, "form.global.error");
			model.addAttribute(STUDENT_VERIFICATION_FORM_ID, form);
			prepareCustomDataForPage(model, form);
			storeCmsPageInModel(model, getContentPageForLabelOrId(STUDENT_VERIFICATION_CMS_PAGE_LABEL));
			model.addAttribute(WebConstants.BREADCRUMBS_KEY,
					getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.studentVerification.breadcrumb"));

			setCheckoutStepLinksForModel(model, getCheckoutStep());
			return ControllerConstants.Views.Pages.MultiStepCheckout.STUDENT_VERIFICATION_PAGE;
		}

		StudentVerificationData studentVerificationData = createStudentVerificationData(form);
		studentFacade.saveStudentVerification(studentVerificationData);

		if (checkoutFacade.isPaypalCheckout())
		{
			return PAYPAL_REDIRECT;
		}
		return getCheckoutStep().nextStep();
	}


	/**
	 * Method to fill StudentVerificationForm from StudentVerificationForm
	 */
	private StudentVerificationData createStudentVerificationData(final StudentVerificationForm form)
	{
		StudentVerificationData studentVerificationData = new StudentVerificationData();
		studentVerificationData.setCountryIso(form.getCountryIso());
		studentVerificationData.setUniversityCode(form.getUniversityCode());
		studentVerificationData.setUniversityTextField(form.getUniversityTextField());
		return studentVerificationData;
	}

	protected CheckoutStep getCheckoutStep()
	{
		return getCheckoutStep(STUDENT_VERIFICATION);
	}

}
