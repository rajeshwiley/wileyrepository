package com.wiley.welags.storefrontcommons.security;

import de.hybris.platform.acceleratorstorefrontcommons.security.BruteForceAttackCounter;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.user.UserService;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;


/**
 * The type Login authentication failure handler.
 */
public class LoginAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler
{
	private static final Logger LOG = LoggerFactory.getLogger(LoginAuthenticationFailureHandler.class);

	private BruteForceAttackCounter bruteForceAttackCounter;
	private boolean invalidateSessionOnLoginFail;
	private String disabledUserUrl;
	private String forgetPasswordUrl;
	private Integer failedLoginsToForgetPassword;
	private UserService userService;


	@Override
	public void onAuthenticationFailure(final HttpServletRequest request, final HttpServletResponse response,
			final AuthenticationException exception) throws IOException, ServletException
	{

		// Register brute attacks
		bruteForceAttackCounter.registerLoginFailure(request.getParameter("j_username"));


		if (invalidateSessionOnLoginFail)
		{
			/*
			This difference triggers functionality in SecurityUserCheckBeforeControllerHandler to invalidate current request HTTP
			session in the middle of filter processing chain. Due to invalidated session Hybris session also becomes invalid. It
			makes further filter in chain  to fail with exception as there is no more Base Site attached to session that is
			required for functionality to work. It is worth to mention that clearing cookie is not working in any storefront that
			is not mapped to /storefrontname (e.g no /en or other path elements after). Because cookie is set to /storefrontname
			path but is cleared for /storefrontname/en (checked on WEL and OOTB storefronts) - this simply means cookie is not
			cleared at all. And that's why it is only reproducible to WEL storefront that does not use any /en in its path.

			The fix is simple - clear http session on unsuccessful login attempt (before redirecting to /login?error=true url).
			This makes Hybris session to be lost and hybris session user will become equal to spring security user (anonymous).
			So SecurityUserCheckBeforeControllerHandler will not invalidate session in the middle of request filter processing
			chain(as users will not be different). For checkout we should not clear session for unsuccessful login attempt as
			this will clear a cart. That's why invalidateSessionOnLoginFail was added to be set to 'true' for normal login
			handler and 'false' for checkout login handler.
			 */

			LOG.info("Login attempt failed. Invalidating session");
			request.getSession().invalidate();
		}
		else
		{
			// Store the j_username in the session
			request.getSession().setAttribute("SPRING_SECURITY_LAST_USERNAME", request.getParameter("j_username"));
		}
		try
		{
			final UserModel userModel = getUserService()
					.getUserForUID(StringUtils.lowerCase(request.getParameter("j_username")));

			if (userModel.isLoginDisabled() || getBruteForceAttackCounter()
					.getUserFailedLogins(request.getParameter("j_username")) >= getFailedLoginsToForgetPassword())
			{
				getRedirectStrategy().sendRedirect(request, response, getForgetPasswordUrl());
			}
			else
			{
				super.onAuthenticationFailure(request, response, exception);
			}
		}
		catch (UnknownIdentifierException e)
		{
			super.onAuthenticationFailure(request, response, exception);
		}

	}



	/**
	 * Gets brute force attack counter.
	 *
	 * @return the brute force attack counter
	 */
	protected BruteForceAttackCounter getBruteForceAttackCounter()
	{
		return bruteForceAttackCounter;
	}

	/**
	 * Sets brute force attack counter.
	 *
	 * @param bruteForceAttackCounter
	 * 		the brute force attack counter
	 */
	@Required
	public void setBruteForceAttackCounter(final BruteForceAttackCounter bruteForceAttackCounter)
	{
		this.bruteForceAttackCounter = bruteForceAttackCounter;
	}

	public void setInvalidateSessionOnLoginFail(final boolean invalidateSessionOnLoginFail)
	{
		this.invalidateSessionOnLoginFail = invalidateSessionOnLoginFail;
	}

	public String getDisabledUserUrl()
	{
		return disabledUserUrl;
	}

	public void setDisabledUserUrl(final String disabledUserUrl)
	{
		this.disabledUserUrl = disabledUserUrl;
	}

	public String getForgetPasswordUrl()
	{
		return forgetPasswordUrl;
	}

	public void setForgetPasswordUrl(final String forgetPasswordUrl)
	{
		this.forgetPasswordUrl = forgetPasswordUrl;
	}

	public Integer getFailedLoginsToForgetPassword()
	{
		return failedLoginsToForgetPassword;
	}

	public void setFailedLoginsToForgetPassword(final Integer failedLoginsToForgetPassword)
	{
		this.failedLoginsToForgetPassword = failedLoginsToForgetPassword;
	}

	public UserService getUserService() { return userService; }

	public void setUserService(final UserService userService) { this.userService = userService; }
}
