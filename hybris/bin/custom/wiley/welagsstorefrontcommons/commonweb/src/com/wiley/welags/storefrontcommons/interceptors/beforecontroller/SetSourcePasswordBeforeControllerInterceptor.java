package com.wiley.welags.storefrontcommons.interceptors.beforecontroller;

import java.util.Map;

import javax.servlet.ServletRequest;
import javax.servlet.ServletRequestWrapper;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.catalina.connector.RequestFacade;
import org.apache.catalina.util.ParameterMap;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;


public class SetSourcePasswordBeforeControllerInterceptor extends HandlerInterceptorAdapter
{

	public static final String PWD = "pwd";
	public static final String CHECK_PWD = "checkPwd";

	@Override
	public boolean preHandle(final HttpServletRequest request, final HttpServletResponse response, final Object handler)
			throws Exception
	{
		if (RequestMethod.POST.name().equals(request.getMethod()))
		{
			ServletRequest requestFacade = getServletRequestFacade((ServletRequestWrapper) request);
			replaceStrippedPasswordParameters(request, requestFacade);
		}

		return true;
	}

	private void replaceStrippedPasswordParameters(final HttpServletRequest request, final ServletRequest requestFacade)
	{
		final Map<String, String[]> strippedParameterMap = request.getParameterMap();
		if (strippedParameterMap != null && requestFacade != null)
		{
			Map<String, String[]> originalParameters = requestFacade.getParameterMap();
			lockParameterMap(strippedParameterMap, false);
			strippedParameterMap.computeIfPresent(PWD, (key, oldValue) -> originalParameters.get(key));
			strippedParameterMap.computeIfPresent(CHECK_PWD, (key, oldValue) -> originalParameters.get(key));
			lockParameterMap(strippedParameterMap, true);
		}
	}

	private void lockParameterMap(final Map<String, String[]> strippedParameterMap, final boolean isLocked)
	{
		if (strippedParameterMap instanceof ParameterMap)
		{
			((ParameterMap<String, String[]>) strippedParameterMap).setLocked(isLocked);
		}
	}

	private ServletRequest getServletRequestFacade(final ServletRequestWrapper request)
	{
		ServletRequest requestFacade = request.getRequest();
		while (requestFacade != null && !(requestFacade instanceof RequestFacade))
		{
			requestFacade = ((ServletRequestWrapper) requestFacade).getRequest();
		}
		return requestFacade;
	}
}
