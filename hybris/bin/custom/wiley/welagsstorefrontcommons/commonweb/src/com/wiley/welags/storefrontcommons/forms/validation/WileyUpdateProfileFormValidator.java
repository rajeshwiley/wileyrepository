package com.wiley.welags.storefrontcommons.forms.validation;

import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.RegistrationValidator;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.wiley.welags.storefrontcommons.forms.WileyUpdateProfileForm;


/**
 * Validates WileyUpdateProfileForm.
 *
 * Created by Mikhail_Kondratyev on 05-Dec-15.
 */
public class WileyUpdateProfileFormValidator implements Validator
{
	private static final int MAX_FIELD_LENGTH = 255;

	@Autowired
	private RegistrationValidator registrationValidator;

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return WileyUpdateProfileForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final WileyUpdateProfileForm profileForm = (WileyUpdateProfileForm) object;
		final String firstName = profileForm.getFirstName();
		final String lastName = profileForm.getLastName();
		final String email = profileForm.getEmailUid();

		if (StringUtils.isBlank(firstName))
		{
			errors.rejectValue("firstName", "profile.firstName.invalid");
		}

		if (StringUtils.isBlank(lastName))
		{
			errors.rejectValue("lastName", "profile.lastName.invalid");
		}

		// First and last names are concatenated with a single space
		if (StringUtils.length(firstName) + StringUtils.length(lastName) > MAX_FIELD_LENGTH - 1)
		{
			errors.rejectValue("firstName", "profile.name.invalid");
			errors.rejectValue("lastName", "profile.name.invalid");
		}

		if (StringUtils.isEmpty(email) || StringUtils.length(email) > MAX_FIELD_LENGTH
				|| !registrationValidator.validateEmailAddress(email))
		{
			errors.rejectValue("emailUid", "profile.email.invalid");
		}
	}

}
