/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.welags.storefrontcommons.controllers;

public interface ControllerConstants
{

	/**
	 * Class with view name constants
	 */
	interface Views
	{

		interface Pages
		{
			interface MultiStepCheckout
			{
				String ADD_EDIT_DELIVERY_ADDRESS_PAGE = "pages/checkout/multi/addEditDeliveryAddressPage";
				String ADD_EDIT_BILLING_ADDRESS_PAGE = "pages/checkout/multi/addEditBillingAddressPage";
				String CHOOSE_DELIVERY_METHOD_PAGE = "pages/checkout/multi/chooseDeliveryMethodPage";
				String CHOOSE_PICKUP_LOCATION_PAGE = "pages/checkout/multi/choosePickupLocationPage";
				String ADD_PAYMENT_METHOD_PAGE = "pages/checkout/multi/addPaymentMethodPage";
				String CHECKOUT_SUMMARY_PAGE = "pages/checkout/multi/checkoutSummaryPage";
				String HOSTED_ORDER_PAGE_ERROR_PAGE = "pages/checkout/multi/hostedOrderPageErrorPage";
				String HOSTED_ORDER_POST_PAGE = "pages/checkout/multi/hostedOrderPostPage";
				String SILENT_ORDER_POST_PAGE = "pages/checkout/multi/silentOrderPostPage";
				String GIFT_WRAP_PAGE = "pages/checkout/multi/giftWrapPage";
				String STUDENT_VERIFICATION_PAGE = "pages/checkout/multi/studentVerificationPage";

				// for checkstyle
				void dummy();
			}

			// for checkstyle
			void dummy();

			interface Payment
			{
				String PAYMENT_INFO_PAGE = "/my-account/payment-details";

				// for checkstyle
				void dummy();
			}
		}

		interface Fragments
		{
			interface Checkout
			{
				String TERMS_AND_CONDITIONS_POPUP = "fragments/checkout/termsAndConditionsPopup";
				String BILLING_ADDRESS_FORM = "fragments/checkout/billingAddressForm";
				String STUDENT_VERIFICATION_FORM = "fragments/checkout/studentVerificationForm";

				// for checkstyle
				void dummy();

				String UPDATE_TAX_FORM = "fragments/checkout/updateTaxForm";
			}

			interface Address
			{
				String COUNTRY_SPECIFIC_ADDRESS_FORM = "fragments/address/countryAddressForm";

				// for checkstyle
				void dummy();
			}

		}

		// for checkstyle
		void dummy();
	}
}
