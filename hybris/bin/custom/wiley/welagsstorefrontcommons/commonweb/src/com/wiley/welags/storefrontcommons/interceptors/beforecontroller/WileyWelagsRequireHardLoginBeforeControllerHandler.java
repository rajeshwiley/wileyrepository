/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.welags.storefrontcommons.interceptors.beforecontroller;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.util.CookieGenerator;

import com.wiley.storefrontcommons.interceptors.beforecontroller.AbstractWileyRequireHardLoginBeforeControllerHandler;


/**
 * The type Require hard login before controller handler.
 */
public class WileyWelagsRequireHardLoginBeforeControllerHandler extends AbstractWileyRequireHardLoginBeforeControllerHandler
{
	private String loginPinUrl;
	private CookieGenerator cookieGenerator;
	private UserService userService;
	private SessionService sessionService;
	private CartService cartService;

	/**
	 * Gets cookie generator.
	 *
	 * @return the cookie generator
	 */
	protected CookieGenerator getCookieGenerator()
	{
		return cookieGenerator;
	}

	/**
	 * Sets cookie generator.
	 *
	 * @param cookieGenerator
	 * 		the cookie generator
	 */
	@Required
	public void setCookieGenerator(final CookieGenerator cookieGenerator)
	{
		this.cookieGenerator = cookieGenerator;
	}

	/**
	 * Gets user service.
	 *
	 * @return the user service
	 */
	protected UserService getUserService()
	{
		return userService;
	}

	/**
	 * Sets user service.
	 *
	 * @param userService
	 * 		the user service
	 */
	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	/**
	 * Gets the login URL for PIN activation.
	 *
	 * @return the login URL for PIN activation
	 */
	public String getLoginPinUrl()
	{
		return loginPinUrl;
	}

	/**
	 * Sets the login URL for PIN activation.
	 *
	 * @param loginPinUrl the login URL for PIN activation
	 */
	@Required
	public void setLoginPinUrl(final String loginPinUrl)
	{
		this.loginPinUrl = loginPinUrl;
	}

	/**
	 * Gets session service.
	 *
	 * @return the session service
	 */
	protected SessionService getSessionService()
	{
		return sessionService;
	}

	/**
	 * Sets session service.
	 *
	 * @param sessionService
	 * 		the session service
	 */
	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	/**
	 * Gets cart service.
	 *
	 * @return the cart service
	 */
	public CartService getCartService()
	{
		return cartService;
	}

	/**
	 * Sets cart service.
	 *
	 * @param cartService
	 * 		the cart service
	 */
	@Required
	public void setCartService(final CartService cartService)
	{
		this.cartService = cartService;
	}

	@Override
	public boolean beforeController(final HttpServletRequest request, final HttpServletResponse response,
			final HandlerMethod handler) throws Exception
	{
		// We only care if the request is secure
		if (request.isSecure())
		{
			// Check if the handler has our annotation
			if (LOG.isDebugEnabled())
			{
				LOG.debug("Before controller for " + request.getRequestURL() + " Handler is " + handler.toString());
			}
			final RequireHardLogIn annotation = findAnnotation(handler, RequireHardLogIn.class);
			if (annotation != null)
			{
				final String guid = (String) request.getSession().getAttribute(SECURE_GUID_SESSION_KEY);
				boolean redirect = true;

				if (((!getUserService().isAnonymousUser(getUserService().getCurrentUser()) || checkForAnonymousCheckout())
						&& checkForGUIDCookie(request, response, guid)))
				{
					redirect = false;
				}

				if (redirect)
				{
					LOG.warn((guid == null ? "missing secure token in session" : "no matching guid cookie") + ", redirecting");
					getRedirectStrategy().sendRedirect(request, response, getRedirectUrl(request));
					return false;
				}
			}
		}

		return true;
	}


	/**
	 * Check for guid cookie boolean.
	 *
	 * @param request
	 * 		the request
	 * @param response
	 * 		the response
	 * @param guid
	 * 		the guid
	 * @return the boolean
	 */
	protected boolean checkForGUIDCookie(final HttpServletRequest request, final HttpServletResponse response, final String guid)
	{
		if (guid != null && request.getCookies() != null)
		{
			final String guidCookieName = getCookieGenerator().getCookieName();
			if (guidCookieName != null)
			{
				for (final Cookie cookie : request.getCookies())
				{
					if (guidCookieName.equals(cookie.getName()))
					{
						if (guid.equals(cookie.getValue()))
						{
							return true;
						}
						else
						{
							LOG.info(
									"Found secure cookie with invalid value. expected [" + guid + "] actual [" + cookie.getValue()
											+ "]. removing.");
							getCookieGenerator().removeCookie(response);
						}
					}
				}
			}
		}

		return false;

	}

	/**
	 * Check for anonymous checkout boolean.
	 *
	 * @return the boolean
	 */
	protected boolean checkForAnonymousCheckout()
	{
		if (Boolean.TRUE.equals(getSessionService().getAttribute(WebConstants.ANONYMOUS_CHECKOUT)))
		{
			if (getSessionService().getAttribute(WebConstants.ANONYMOUS_CHECKOUT_GUID) == null)
			{
				getSessionService().setAttribute(WebConstants.ANONYMOUS_CHECKOUT_GUID,
						StringUtils.substringBefore(getCartService().getSessionCart().getUser().getUid(), "|"));
			}
			return true;
		}
		return false;
	}

	/**
	 * Gets redirect url.
	 *
	 * @param request
	 * 		the request
	 * @return the redirect url
	 */
	protected String getRedirectUrl(final HttpServletRequest request)
	{
		String servletPath = request != null ? request.getServletPath() : StringUtils.EMPTY;
		if (servletPath.contains("checkout"))
		{
			return getLoginAndCheckoutUrl();
		}
		else if (servletPath.contains("/pin/"))
		{
			return getLoginPinUrl();
		}
		else
		{
			return getLoginUrl();
		}
	}
}
