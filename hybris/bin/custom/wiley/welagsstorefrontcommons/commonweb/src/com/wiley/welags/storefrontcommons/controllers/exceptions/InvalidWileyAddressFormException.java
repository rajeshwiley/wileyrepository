package com.wiley.welags.storefrontcommons.controllers.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(value = HttpStatus.EXPECTATION_FAILED, reason = "WelAgsAddressForm Is Not Valid")
public class InvalidWileyAddressFormException extends Exception
{
	public InvalidWileyAddressFormException(final String message)
	{
		super(message);
	}
}
