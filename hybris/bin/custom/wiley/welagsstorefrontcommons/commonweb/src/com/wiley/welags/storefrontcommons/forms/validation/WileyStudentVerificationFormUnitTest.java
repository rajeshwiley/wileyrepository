package com.wiley.welags.storefrontcommons.forms.validation;

import de.hybris.bootstrap.annotations.UnitTest;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.validation.Errors;

import com.wiley.welags.storefrontcommons.forms.StudentVerificationForm;

import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyStudentVerificationFormUnitTest
{

	private static final String USA_COUNTRY_ISO_CODE = "US";
	private static final String TEST_UNIVERSITY_CODE = "test_university_code";
	private static final String TEST_REGION_ISO = "test_region_iso";
	private static final String TEST_UNIVERSITY_TEXT = "university text field test";
	private static final String TEST_NON_USA_COUNTRY_ISO_CODE = "CA";

	private static final String STUDENT_VERIFICATION_COUNTRY_ISO_EMPTY = "student.verification.countryIso.empty";
	private static final String STUDENT_VERIFICATION_REGION_ISO_EMPTY = "student.verification.regionIso.empty";
	private static final String STUDENT_VERIFICATION_UNIVERSITY_CODE_EMPTY = "student.verification.universityCode.empty";
	private static final String STUDENT_VERIFICATION_UNIVERSITY_TEXT_EMPTY = "student.verification.universityTextField.empty";
	private static final String STUDENT_VERIFICATION_UNIVERSITY_TEXT_INVALID = "student.verification.universityTextField.invalid";
	private static final String STUDENT_VERIFICATION_AGREEMENT_INVALID = "student.verification.agreement.empty";

	private static final String COUNTRY_ISO = "countryIso";
	private static final String REGION_ISO = "regionIso";
	private static final String UNIVERSITY_CODE = "universityCode";
	private static final String UNIVERSITY_TEXT_FIELD = "universityTextField";
	private static final String AGREEMENT = "agreement";
	public static final String UNIVERSITY_TEXT_FIELD_BLANK = "   ";

	@Mock
	private Errors errorsMock;

	private final WileyStudentVerificationFormValidator testInstance = new WileyStudentVerificationFormValidator();

	/**
	 * Test validate should accept correct data for non-USA country.
	 *
	 * @throws Exception
	 * 		the exception
	 */
	@Test
	public void testValidateShouldAcceptCorrectDataForNonUSA() throws Exception
	{

		//Given
		final StudentVerificationForm studentVerificationForm = new StudentVerificationForm();
		studentVerificationForm.setCountryIso(TEST_NON_USA_COUNTRY_ISO_CODE);
		studentVerificationForm.setUniversityTextField(TEST_UNIVERSITY_TEXT);
		studentVerificationForm.setAgreement(true);
		//When
		testInstance.validate(studentVerificationForm, errorsMock);

		//Then
		verify(errorsMock, never()).rejectValue(anyString(), anyString());


	}



	/**
	 * Test validate should reject if country isocode is empty.
	 *
	 * @throws Exception
	 * 		the exception
	 */
	@Test
	public void testValidateShouldRejectIfCountryEmpty() throws Exception
	{
		//Given
		final StudentVerificationForm studentVerificationForm = new StudentVerificationForm();
		studentVerificationForm.setCountryIso(StringUtils.EMPTY);


		//When
		testInstance.validate(studentVerificationForm, errorsMock);

		//Then
		verify(errorsMock).rejectValue(COUNTRY_ISO, STUDENT_VERIFICATION_COUNTRY_ISO_EMPTY);

	}





	/**
	 * Test validate should reject - not accepted agreement.
	 *
	 * @throws Exception
	 * 		the exception
	 */
	@Test
	public void testValidateShouldRejectNotAcceptedAgreement() throws Exception
	{

		//Given
		final StudentVerificationForm studentVerificationForm = new StudentVerificationForm();
		studentVerificationForm.setCountryIso(TEST_NON_USA_COUNTRY_ISO_CODE);
		studentVerificationForm.setAgreement(false);
		//When
		testInstance.validate(studentVerificationForm, errorsMock);

		//Then
		verify(errorsMock).rejectValue(AGREEMENT, STUDENT_VERIFICATION_AGREEMENT_INVALID);
	}

	/**
	 * Test validate should reject if university text field empty for Non-USA.
	 *
	 * @throws Exception
	 * 		the exception
	 */
	@Test
	public void testValidateShouldRejectIfUniversityEmpty() throws Exception
	{

		//Given
		final StudentVerificationForm studentVerificationForm = new StudentVerificationForm();
		studentVerificationForm.setCountryIso(TEST_NON_USA_COUNTRY_ISO_CODE);
		studentVerificationForm.setUniversityTextField(StringUtils.EMPTY);


		//When
		testInstance.validate(studentVerificationForm, errorsMock);

		//Then
		verify(errorsMock).rejectValue(UNIVERSITY_TEXT_FIELD, STUDENT_VERIFICATION_UNIVERSITY_TEXT_EMPTY);
		verify(errorsMock, never()).rejectValue(eq(COUNTRY_ISO), anyString());

	}


	/**
	 * Test validate should reject if university text field blank for Non-USA.
	 *
	 * @throws Exception
	 * 		the exception
	 */
	@Test
	public void testValidateShouldRejectIfUniversityBlank() throws Exception
	{

		//Given
		final StudentVerificationForm studentVerificationForm = new StudentVerificationForm();
		studentVerificationForm.setCountryIso(TEST_NON_USA_COUNTRY_ISO_CODE);
		studentVerificationForm.setUniversityTextField(UNIVERSITY_TEXT_FIELD_BLANK);


		//When
		testInstance.validate(studentVerificationForm, errorsMock);

		//Then
		verify(errorsMock).rejectValue(UNIVERSITY_TEXT_FIELD, STUDENT_VERIFICATION_UNIVERSITY_TEXT_EMPTY);
		verify(errorsMock, never()).rejectValue(eq(COUNTRY_ISO), anyString());

	}

	/**
	 * Test validate should reject if university text field > MAX_FIELD_LENGTH for Non-USA.
	 *
	 * @throws Exception
	 * 		the exception
	 */
	@Test
	public void testValidateShouldRejectIfUniversityExceedsMaxLength() throws Exception
	{
		//Given
		final StudentVerificationForm studentVerificationForm = new StudentVerificationForm();
		studentVerificationForm.setCountryIso(TEST_NON_USA_COUNTRY_ISO_CODE);
		studentVerificationForm
				.setUniversityTextField(
						"0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789"
								+ "123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890"
								+ "123456789012345678901234567890123456789012345678901234567890123456");


		//When
		testInstance.validate(studentVerificationForm, errorsMock);

		//Then
		verify(errorsMock).rejectValue(UNIVERSITY_TEXT_FIELD, STUDENT_VERIFICATION_UNIVERSITY_TEXT_INVALID);
		verify(errorsMock, never()).rejectValue(eq(COUNTRY_ISO), anyString());

	}


	/**
	 * Test validate should accept correct data for USA country.
	 *
	 * @throws Exception
	 * 		the exception
	 */
	@Test
	public void testValidateShouldAcceptCorrectDataForUSA() throws Exception
	{

		//Given
		final StudentVerificationForm studentVerificationForm = new StudentVerificationForm();
		studentVerificationForm.setCountryIso(USA_COUNTRY_ISO_CODE);
		studentVerificationForm.setRegionIso(TEST_REGION_ISO);
		studentVerificationForm.setUniversityCode(TEST_UNIVERSITY_CODE);
		studentVerificationForm.setAgreement(true);
		testInstance.setDefaultCountryIsocode(USA_COUNTRY_ISO_CODE);
		//When
		testInstance.validate(studentVerificationForm, errorsMock);

		//Then
		verify(errorsMock, never()).rejectValue(anyString(), anyString());

	}


	/**
	 * Test validate should reject empty region iso  data for USA country.
	 *
	 * @throws Exception
	 * 		the exception
	 */
	@Test
	public void testValidateShouldRejectRegionIsoCodeEmpty() throws Exception
	{

		//Given
		final StudentVerificationForm studentVerificationForm = new StudentVerificationForm();
		studentVerificationForm.setCountryIso(USA_COUNTRY_ISO_CODE);
		studentVerificationForm.setRegionIso(StringUtils.EMPTY);

		studentVerificationForm.setAgreement(true);
		testInstance.setDefaultCountryIsocode(USA_COUNTRY_ISO_CODE);
		//When
		testInstance.validate(studentVerificationForm, errorsMock);

		//Then
		verify(errorsMock).rejectValue(REGION_ISO, STUDENT_VERIFICATION_REGION_ISO_EMPTY);
	}

	/**
	 * Test validate should reject empty university code  for USA country.
	 *
	 * @throws Exception
	 * 		the exception
	 */
	@Test
	public void testValidateShouldRejectUniversityCodeEmpty() throws Exception
	{

		//Given
		final StudentVerificationForm studentVerificationForm = new StudentVerificationForm();
		studentVerificationForm.setCountryIso(USA_COUNTRY_ISO_CODE);
		studentVerificationForm.setRegionIso(TEST_REGION_ISO);
		studentVerificationForm.setUniversityCode(StringUtils.EMPTY);
		studentVerificationForm.setAgreement(true);
		testInstance.setDefaultCountryIsocode(USA_COUNTRY_ISO_CODE);
		//When
		testInstance.validate(studentVerificationForm, errorsMock);

		//Then
		verify(errorsMock).rejectValue(UNIVERSITY_CODE, STUDENT_VERIFICATION_UNIVERSITY_CODE_EMPTY);
	}

}
