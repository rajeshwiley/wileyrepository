package com.wiley.welags.storefrontcommons.voucher;

import java.io.Serializable;


public class VoucherForm implements Serializable
{
	
	private static final long serialVersionUID = 6240549265172509876L;
	
	private String discountCodeValue;

	public VoucherForm()
	{
	}

	public VoucherForm(final String discountCodeValue)
	{
		this.discountCodeValue = discountCodeValue;
	}

	public String getDiscountCodeValue()
	{
		return discountCodeValue;
	}

	public void setDiscountCodeValue(final String discountCodeValue)
	{
		this.discountCodeValue = discountCodeValue;
	}
}
