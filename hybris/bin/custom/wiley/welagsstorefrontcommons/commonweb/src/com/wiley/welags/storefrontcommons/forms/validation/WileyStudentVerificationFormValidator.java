package com.wiley.welags.storefrontcommons.forms.validation;


import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.wiley.welags.storefrontcommons.forms.StudentVerificationForm;




public class WileyStudentVerificationFormValidator implements Validator
{
	private static final int MAX_FIELD_LENGTH = 255;

	@Value("${default.country.isocode}")
	private String defaultCountryIsocode;

	@Override
	public boolean supports(final Class<?> paramClass)
	{
		return StudentVerificationForm.class.equals(paramClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final StudentVerificationForm studentVerificationForm = (StudentVerificationForm) object;
		final String countryIso = studentVerificationForm.getCountryIso();

		if (StringUtils.isEmpty(countryIso))
		{
			errors.rejectValue("countryIso", "student.verification.countryIso.empty");
		}
		else
		{
			if (StringUtils.endsWithIgnoreCase(defaultCountryIsocode, countryIso))
			{
				validateUSAFields(errors, studentVerificationForm);

			}
			else
			{
				validateNonUSAFields(errors, studentVerificationForm);
			}

			final boolean agreement = studentVerificationForm.isAgreement();

			if (!agreement)
			{
				errors.rejectValue("agreement", "student.verification.agreement.empty");
			}
		}
	}



	private void validateUSAFields(final Errors errors, final StudentVerificationForm studentVerificationForm)
	{
		final String regionIso = studentVerificationForm.getRegionIso();
		if (StringUtils.isEmpty(regionIso))
		{
			errors.rejectValue("regionIso", "student.verification.regionIso.empty");
		}

		final String universityCode = studentVerificationForm.getUniversityCode();
		if (StringUtils.isEmpty(universityCode))
		{
			errors.rejectValue("universityCode", "student.verification.universityCode.empty");
		}
	}

	private void validateNonUSAFields(final Errors errors, final StudentVerificationForm studentVerificationForm)
	{
		final String universityTextField = studentVerificationForm.getUniversityTextField();

		if (StringUtils.isBlank(universityTextField))
		{
			errors.rejectValue("universityTextField", "student.verification.universityTextField.empty");
		}
		else if (StringUtils.length(universityTextField) > MAX_FIELD_LENGTH)
		{
			errors.rejectValue("universityTextField", "student.verification.universityTextField.invalid");
		}
	}

	public String getDefaultCountryIsocode()
	{
		return defaultCountryIsocode;
	}

	public void setDefaultCountryIsocode(final String defaultCountryIsocode)
	{
		this.defaultCountryIsocode = defaultCountryIsocode;
	}
}
