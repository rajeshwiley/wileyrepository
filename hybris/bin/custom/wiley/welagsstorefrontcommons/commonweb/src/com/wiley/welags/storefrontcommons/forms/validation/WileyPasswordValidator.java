package com.wiley.welags.storefrontcommons.forms.validation;

import de.hybris.platform.acceleratorstorefrontcommons.forms.UpdatePasswordForm;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


/**
 * Created by uladzimir_barouski on 12/1/2015.
 */
public class WileyPasswordValidator implements Validator
{
	@Override
	public boolean supports(final Class<?> aClass)
	{
		return UpdatePasswordForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final UpdatePasswordForm passwordForm = (UpdatePasswordForm) object;
		final String currPasswd = passwordForm.getCurrentPassword();
		final String newPasswd = passwordForm.getNewPassword();
		final String checkPasswd = passwordForm.getCheckNewPassword();

		if (StringUtils.isEmpty(currPasswd))
		{
			errors.rejectValue("currentPassword", "profile.currentPassword.invalid");
		}

		if (StringUtils.isEmpty(newPasswd))
		{
			errors.rejectValue("newPassword", "updatePwd.pwd.empty");
		}
		else if (StringUtils.length(newPasswd) < 6)
		{
			errors.rejectValue("newPassword", "updatePwd.pwd.invalid");
		}
		else if (StringUtils.length(newPasswd) > 255)
		{
			errors.rejectValue("newPassword", "updatePwd.pwd.invalid.length");
		}

		if (StringUtils.isEmpty(checkPasswd))
		{
			errors.rejectValue("checkNewPassword", "updatePwd.pwd.empty");
		}
		else if (StringUtils.length(checkPasswd) < 6)
		{
			errors.rejectValue("checkNewPassword", "updatePwd.pwd.invalid");
		}
		else if (StringUtils.length(checkPasswd) > 255)
		{
			errors.rejectValue("checkNewPassword", "updatePwd.pwd.invalid.length");
		}
	}
}
