package com.wiley.welags.storefrontcommons.data.populators;

import de.hybris.platform.commercefacades.order.data.DeliveryModeData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.List;
import java.util.stream.Collectors;

import com.wiley.storefront.checkout.DeliveryOptionData;


public class DeliveryOptionDataPopulator implements Populator<List<DeliveryModeData>, List<DeliveryOptionData>>
{
	@Override
	public void populate(final List<DeliveryModeData> deliveryModeData, final List<DeliveryOptionData> deliveryOptionData)
			throws ConversionException
	{
		deliveryOptionData.addAll(deliveryModeData.stream().map(this::convert).collect(Collectors.toList()));
		deliveryOptionData.stream().findFirst().ifPresent(deliveryMode -> deliveryMode.setChecked(true));
	}

	private DeliveryOptionData convert(final DeliveryModeData deliveryModeData) {
		DeliveryOptionData deliveryOptionData = new DeliveryOptionData();
		deliveryOptionData.setCode(deliveryModeData.getCode());
		deliveryOptionData.setName(deliveryModeData.getName());
		return deliveryOptionData;
	}
}
