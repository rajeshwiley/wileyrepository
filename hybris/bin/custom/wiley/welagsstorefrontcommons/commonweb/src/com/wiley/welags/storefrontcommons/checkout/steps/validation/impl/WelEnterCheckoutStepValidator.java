/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.welags.storefrontcommons.checkout.steps.validation.impl;


import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.AbstractCheckoutStepValidator;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.facades.welags.order.WelAgsCheckoutFacade;


public class WelEnterCheckoutStepValidator extends AbstractCheckoutStepValidator
{
	private static final Logger LOG = LoggerFactory.getLogger(WelEnterCheckoutStepValidator.class);

	@Resource(name = "checkoutFacade")
	private WelAgsCheckoutFacade wileyCheckoutFacade;

	@Override
	public ValidationResults validateOnEnter(final RedirectAttributes redirectAttributes)
	{
		if (getCheckoutFlowFacade().hasValidCart())
		{
			return ValidationResults.SUCCESS;
		}
		LOG.info("Missing, empty or unsupported cart");
		return ValidationResults.FAILED;
	}

	@Override
	public ValidationResults validateOnExit()
	{
		final boolean isZeroPriceCart = !wileyCheckoutFacade.isNonZeroPriceCart();
		if (isZeroPriceCart && wileyCheckoutFacade.isDigitalSessionCart())
		{
			return ValidationResults.REDIRECT_TO_SUMMARY;
		}
		return super.validateOnExit();
	}
}
