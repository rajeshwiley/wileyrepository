/**
 *
 */
package com.wiley.welags.storefrontcommons.forms.validation;

import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.AddressValidator;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.Errors;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.welags.storefrontcommons.forms.WelAgsAddressForm;


@Deprecated
// Please extend AbstractWileyAddressValidator
public class WileyAddressValidator extends AddressValidator
{
	private static final int MAX_FIELD_LENGTH = 255;
	private static final int NAME_FIELD_LENGTH = 20;
	private static final int ADDRESS_LINE_MAX_FIELD_LENGTH = 30;
	private static final int POSTCODE_MAX_FIELD_LENGTH = 8;
	private static final int TOWN_CITY_FIELD_LENGTH = 24;
	private static final String FIELD_LIMIT_EXCEED = "address.field.limit.exceed";
	private static final String POSTCODE_FIELD_LIMIT_EXCEED = "address.postcode.size.invalid";

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return WelAgsAddressForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final WelAgsAddressForm addressForm = (WelAgsAddressForm) object;
		validateStandardFields(addressForm, errors);
		validateCountrySpecificFields(addressForm, errors);
	}

	protected void validateStandardFields(final WelAgsAddressForm addressForm, final Errors errors)
	{
		validateStringField(addressForm.getCountryIso(), WileyAddressField.COUNTRY, MAX_FIELD_LENGTH, errors);
		validateStringField(addressForm.getFirstName(), WileyAddressField.FIRSTNAME, NAME_FIELD_LENGTH, errors);
		validateStringField(addressForm.getLastName(), WileyAddressField.LASTNAME, NAME_FIELD_LENGTH, errors);
		validateStringField(addressForm.getLine1(), WileyAddressField.LINE1, ADDRESS_LINE_MAX_FIELD_LENGTH, errors);
		validateFieldMaxLength(addressForm.getLine2(), WileyAddressField.LINE2, ADDRESS_LINE_MAX_FIELD_LENGTH, errors);
		validateStringField(addressForm.getPhone(), WileyAddressField.PHONE, MAX_FIELD_LENGTH, errors);
	}

	protected void validateCountrySpecificFields(final WelAgsAddressForm addressForm, final Errors errors)
	{
		final String isoCode = addressForm.getCountryIso();
		if (isoCode != null && WileyCoreConstants.DEFAULT_COUNTRY_ISO_CODE.equals(isoCode))
		{
			validateFieldNotNull(addressForm.getRegionIso(), WileyAddressField.REGION, errors);
			validateStringField(addressForm.getTownCity(), WileyAddressField.TOWN, TOWN_CITY_FIELD_LENGTH, errors);
			validateStringField(addressForm.getPostcode(), WileyAddressField.ZIPCODE, POSTCODE_MAX_FIELD_LENGTH, errors,
					POSTCODE_FIELD_LIMIT_EXCEED);
		}
		else
		{
			validateStringField(addressForm.getPostcode(), WileyAddressField.POSTCODE, POSTCODE_MAX_FIELD_LENGTH, errors,
					POSTCODE_FIELD_LIMIT_EXCEED);
			validateStringField(addressForm.getTownCity(), WileyAddressField.PROVINCE, TOWN_CITY_FIELD_LENGTH, errors);
		}
	}

	protected static void validateStringField(final String addressField, final WileyAddressField fieldType,
			final int maxFieldLength, final Errors errors)
	{
		validateStringField(addressField, fieldType, maxFieldLength, errors, FIELD_LIMIT_EXCEED);
	}

	/**
	 * Validates String field on Empty or length restrictions
	 *
	 * @param addressField
	 * 		- field value
	 * @param fieldType
	 * 		- metadata about field
	 * @param maxFieldLength
	 * 		- Max fields length
	 * @param errors
	 * 		- errors bundle
	 * @param sizeExceededErrorMessage
	 * 		- error message if does not match
	 */
	protected static void validateStringField(final String addressField, final WileyAddressField fieldType,
			final int maxFieldLength, final Errors errors, final String sizeExceededErrorMessage)
	{
		validateStringField(addressField, fieldType, maxFieldLength, errors, sizeExceededErrorMessage,
				String.valueOf(maxFieldLength));
	}

	/**
	 * Validates String field on Empty or length restrictions
	 *
	 * @param addressField
	 * 		- field value
	 * @param fieldType
	 * 		- metadata about field
	 * @param maxFieldLength
	 * 		- Max fields length
	 * @param errors
	 * 		- errors bundle
	 * @param sizeExceededErrorMessage
	 * 		- error message if does not match
	 * @param messageArguments
	 * 		- custom message arguments
	 */
	protected static void validateStringField(final String addressField, final WileyAddressField fieldType,
			final int maxFieldLength, final Errors errors, final String sizeExceededErrorMessage,
			final String... messageArguments)
	{
		if (StringUtils.isEmpty(addressField))
		{
			errors.rejectValue(fieldType.getFieldKey(), fieldType.getErrorKey());
		}
		else if (StringUtils.length(addressField) > maxFieldLength)
		{
			errors.rejectValue(fieldType.getFieldKey(), sizeExceededErrorMessage, messageArguments, sizeExceededErrorMessage);
		}
	}

	protected static void validateFieldNotNull(final String addressField, final WileyAddressField fieldType, final Errors errors)
	{
		if (addressField == null)
		{
			errors.rejectValue(fieldType.getFieldKey(), fieldType.getErrorKey());
		}
	}

	/**
	 * Validates whether field max lengths is not exceeded, do no makes checks on null, empty string
	 *
	 * @param value
	 * 		- field value
	 * @param addressField
	 * 		- field metadata
	 * @param maxFieldLength
	 * 		- max field length allowed
	 * @param errors
	 * 		- errors bundle
	 */
	protected static void validateFieldMaxLength(final String value, final WileyAddressField addressField,
			final int maxFieldLength,
			final Errors errors)
	{
		if (StringUtils.isNotEmpty(value) && value.length() > maxFieldLength)
		{
			String[] messageArguments = { String.valueOf(maxFieldLength) };
			errors.rejectValue(addressField.getFieldKey(), FIELD_LIMIT_EXCEED, messageArguments, FIELD_LIMIT_EXCEED);
		}
	}

	public enum WileyAddressField
	{
		FIRSTNAME("firstName", "address.firstName.invalid"), LASTNAME("lastName", "address.lastName.invalid"), LINE1("line1",
			"address.line1.invalid"), LINE2("line2", "address.line2.invalid"), TOWN("townCity",
			"address.townCity.invalid"), PROVINCE("townCity", "address.cityProvince.invalid"), ZIPCODE("postcode",
			"address.zipcode.invalid"), POSTCODE("postcode", "address.postcode.invalid"), REGION("regionIso",
			"address.state.invalid"), COUNTRY("countryIso", "address.country.invalid"), PHONE("phone",
			"address.phone.invalid");

		private final String fieldKey;
		private final String errorKey;

		WileyAddressField(final String fieldKey, final String errorKey)
		{
			this.fieldKey = fieldKey;
			this.errorKey = errorKey;
		}

		public String getFieldKey()
		{
			return fieldKey;
		}

		public String getErrorKey()
		{
			return errorKey;
		}
	}
}