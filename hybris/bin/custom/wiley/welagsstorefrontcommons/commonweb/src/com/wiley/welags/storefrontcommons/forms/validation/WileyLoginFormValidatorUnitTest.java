package com.wiley.welags.storefrontcommons.forms.validation;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorstorefrontcommons.forms.LoginForm;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.validation.Errors;


/**
 * Created by Raman_Hancharou on 12/25/2015.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyLoginFormValidatorUnitTest
{
	private static final String PROFILE_CURRENT_PASSWORD_INVALID = "profile.currentPassword.invalid";
	private static final String PROFILE_EMAIL_INVALID = "profile.email.invalid";
	private static final String J_USERNAME = "j_username";
	private static final String J_PASSWORD = "j_password";

	@Mock
	private Errors errorsMock;

	private final WileyLoginFormValidator testInstance = new WileyLoginFormValidator();

	/**
	 * Test validate should reject if username and password incorrect.
	 *
	 * @throws Exception
	 * 		the exception
	 */
	@Test
	public void testValidateShouldRejectIfUsernameAndPasswordIncorrect() throws Exception
	{
		//Given
		final LoginForm loginForm = new LoginForm();
		loginForm.setJ_username("email@q.q");
		loginForm.setJ_password("123");

		//When
		testInstance.validate(loginForm, errorsMock);

		//Then
		Mockito.verify(errorsMock).rejectValue(J_USERNAME, PROFILE_EMAIL_INVALID);
		Mockito.verify(errorsMock).rejectValue(J_PASSWORD, PROFILE_CURRENT_PASSWORD_INVALID);
	}

	/**
	 * Test validate should reject if username incorrect.
	 *
	 * @throws Exception
	 * 		the exception
	 */
	@Test
	public void testValidateShouldRejectIfUsernameIncorrect() throws Exception
	{
		//Given
		final LoginForm loginForm = new LoginForm();
		loginForm.setJ_username("email@q.q");
		loginForm.setJ_password("123123");

		//When
		testInstance.validate(loginForm, errorsMock);

		//Then
		Mockito.verify(errorsMock).rejectValue(J_USERNAME, PROFILE_EMAIL_INVALID);
		Assert.assertEquals(0, errorsMock.getFieldErrorCount(J_PASSWORD));
	}

	/**
	 * Test validate should reject if password incorrect.
	 *
	 * @throws Exception
	 * 		the exception
	 */
	@Test
	public void testValidateShouldRejectIfPasswordIncorrect() throws Exception
	{
		//Given
		final LoginForm loginForm = new LoginForm();
		loginForm.setJ_username("email@mail.ru");
		loginForm.setJ_password("123");

		//When
		testInstance.validate(loginForm, errorsMock);

		//Then
		Mockito.verify(errorsMock).rejectValue(J_PASSWORD, PROFILE_CURRENT_PASSWORD_INVALID);
		Assert.assertEquals(0, errorsMock.getFieldErrorCount(J_USERNAME));
	}
}
