package com.wiley.welags.storefrontcommons.forms;

import de.hybris.platform.acceleratorstorefrontcommons.forms.RegisterForm;




/**
 * Extended OOTB register form, with additional fields
 *
 * Note: Rename class if needed on other sites
 */
public class WileyRegisterForm extends RegisterForm
{
	private String confirmEmail;

	public String getConfirmEmail()
	{
		return confirmEmail;
	}

	public void setConfirmEmail(final String confirmEmail)
	{
		this.confirmEmail = confirmEmail;
	}
}
