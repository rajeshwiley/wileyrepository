package com.wiley.welags.storefrontcommons.forms.validation;

import static org.mockito.Mockito.never;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.validation.Errors;

import com.wiley.welags.storefrontcommons.forms.WileyRegisterForm;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyRegistrationFormValidatorUnitTest
{
	@Mock
	private Errors errorsMock;

	private final WileyRegistrationFormValidator testInstance = new WileyRegistrationFormValidator();

	@Test
	public void testValidateShouldRejectIfConfirmEmailIsNotTheSame() throws Exception
	{
		//Given
		final WileyRegisterForm wileyRegisterForm = new WileyRegisterForm();
		wileyRegisterForm.setEmail("email@email.com");
		wileyRegisterForm.setConfirmEmail("another@email.com");

		//When
		testInstance.validate(wileyRegisterForm, errorsMock);

		//Then
		Mockito.verify(errorsMock).rejectValue("confirmEmail", "register.confirmEmail.match");
	}

	@Test
	public void testValidateShouldAcceptIfConfirmEmailIsNotTheSame() throws Exception
	{
		// Given
		final String testEmailValue = "email@email.com";
		final WileyRegisterForm wileyRegisterForm = new WileyRegisterForm();
		wileyRegisterForm.setEmail(testEmailValue);
		wileyRegisterForm.setConfirmEmail(testEmailValue);

		//When
		testInstance.validate(wileyRegisterForm, errorsMock);

		//Then
		Mockito.verify(errorsMock, never()).rejectValue("confirmEmail", "register.confirmEmail.match");
	}



	@Test
	public void testValidateEmptyConfirmEmail() throws Exception
	{
		// Given
		final String testEmailValue = "email@email.com";
		final String testConfirmEmail = "";
		final WileyRegisterForm wileyRegisterForm = new WileyRegisterForm();
		wileyRegisterForm.setEmail(testEmailValue);

		wileyRegisterForm.setConfirmEmail(testConfirmEmail);

		//When
		testInstance.validate(wileyRegisterForm, errorsMock);

		//Then
		Mockito.verify(errorsMock).rejectValue("confirmEmail", "register.confirmEmail.empty");
	}
}
