package com.wiley.welags.storefrontcommons.checkout.steps.validation.impl;

import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.AbstractCheckoutStepValidator;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.facades.welags.order.WelAgsCheckoutFacade;


public class WelAddressCheckoutStepValidator extends AbstractCheckoutStepValidator
{
	private static final Logger LOG = LoggerFactory.getLogger(WelAddressCheckoutStepValidator.class);

	@Autowired
	private WelAgsCheckoutFacade welAgsCheckoutFacade;

	@Override
	/**
	 * In case of full digital cart we have options:
	 *  - zero order: go to order summary, no shipping, no payment
	 *  - non-zero order: go to billing only page, no shipping
	 */
	public ValidationResults validateOnEnter(final RedirectAttributes redirectAttributes)
	{
		if (!getCheckoutFlowFacade().hasValidCart())
		{
			LOG.info("Missing, empty or unsupported cart");
			return ValidationResults.REDIRECT_TO_CART;
		}


		if (welAgsCheckoutFacade.isDigitalSessionCart())
		{
			if (welAgsCheckoutFacade.isNonZeroPriceCart())
			{
				return ValidationResults.REDIRECT_TO_DELIVERY_ADDRESS;
			}
			else
			{
				return ValidationResults.REDIRECT_TO_SUMMARY;
			}
		}

		return ValidationResults.SUCCESS;
	}


}
