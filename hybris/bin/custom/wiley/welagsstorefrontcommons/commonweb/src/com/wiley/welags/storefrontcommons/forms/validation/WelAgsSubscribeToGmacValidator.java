package com.wiley.welags.storefrontcommons.forms.validation;

import java.util.Objects;

import org.springframework.validation.Errors;
import org.springframework.validation.SmartValidator;


public class WelAgsSubscribeToGmacValidator implements SmartValidator
{

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return WelAgsSubscribeToGmacValidator.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors, final Object... objects)
	{
		Boolean subscription = (Boolean) object;
		String formPrefix = (String) objects[0];

		validateFieldNotNull(subscription, formPrefix, errors);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		validate(object, errors, "");
	}

	private void validateFieldNotNull(final Boolean subscription, final String formPrefix, final Errors errors)
	{
		if (Objects.isNull(subscription))
		{
			errors.rejectValue(formPrefix + "subscribeGmacInfo", "address.gmac.invalid");
		}
	}
}
