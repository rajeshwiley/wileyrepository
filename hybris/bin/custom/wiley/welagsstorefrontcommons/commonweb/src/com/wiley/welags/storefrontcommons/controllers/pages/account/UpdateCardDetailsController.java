package com.wiley.welags.storefrontcommons.controllers.pages.account;

import com.wiley.facades.payment.WileyPaymentFacade;
import com.wiley.welags.storefrontcommons.controllers.ControllerConstants;
import de.hybris.platform.acceleratorservices.payment.data.PaymentData;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;

import static de.hybris.platform.acceleratorservices.payment.constants.PaymentConstants.PaymentProperties.HOP_DEBUG_MODE;


@RequestMapping(value = "/my-account")
public class UpdateCardDetailsController extends AbstractPageController
{
	private static final Logger LOG = LoggerFactory.getLogger(UpdateCardDetailsController.class);

	private static final String REDIRECT_TO_PAYMENT_INFO_PAGE =
			REDIRECT_PREFIX + ControllerConstants.Views.Pages.Payment.PAYMENT_INFO_PAGE;
	private static final String RESPONSE_URL = "my-account/update-card-details/hop/response";
	private static final String MERCHANT_CALLBACK_URL = "/integration/merchant_callback";
	private static final String PAYMENT_DETAILS_CMS_PAGE = "payment-details";


	@Resource(name = "welAgsPaymentFacade")
	private WileyPaymentFacade wileyPaymentFacade;

	@RequestMapping(value = "/update-card-details", method = RequestMethod.GET)
	@RequireHardLogIn
	public String updateCardDetails(final Model model, final RedirectAttributes redirectAttributes)
	{
		try
		{
			final PaymentData paymentData = wileyPaymentFacade.beginHopValidatePayment(RESPONSE_URL, MERCHANT_CALLBACK_URL);
			final boolean hopDebugMode = getSiteConfigService().getBoolean(HOP_DEBUG_MODE, false);
			model.addAttribute("hostedOrderPageData", paymentData);
			model.addAttribute("hopDebugMode", Boolean.valueOf(hopDebugMode));
			storeCmsPageInModel(model, getContentPageForLabelOrId(PAYMENT_DETAILS_CMS_PAGE));

			return ControllerConstants.Views.Pages.MultiStepCheckout.HOSTED_ORDER_POST_PAGE;
		}
		catch (final Exception e)
		{
			LOG.error("Failed to build updateCardDetails request", e);
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"account.card.update.error.general", null);

		}
		return REDIRECT_TO_PAYMENT_INFO_PAGE;
	}
}
