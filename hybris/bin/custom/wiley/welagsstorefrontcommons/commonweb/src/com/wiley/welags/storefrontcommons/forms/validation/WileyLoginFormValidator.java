package com.wiley.welags.storefrontcommons.forms.validation;

import de.hybris.platform.acceleratorstorefrontcommons.forms.LoginForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.RegistrationValidator;

import java.util.regex.Matcher;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.Errors;

import com.wiley.core.constants.WileyCoreConstants;


/**
 * Created by Raman_Hancharou on 12/24/2015.
 */
public class WileyLoginFormValidator extends RegistrationValidator
{

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return LoginForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object o, final Errors errors)
	{
		final LoginForm loginForm = (LoginForm) o;
		final String username = loginForm.getJ_username();
		final String pwd = loginForm.getJ_password();

		if (StringUtils.isEmpty(username)
				|| StringUtils.length(username) > WileyCoreConstants.MAX_FIELD_LENGTH
				|| !validateEmailAddress(username))
		{
			errors.rejectValue("j_username", "profile.email.invalid");
		}

		if (StringUtils.isEmpty(pwd)
				|| StringUtils.length(pwd) < WileyCoreConstants.MIN_PASSWORD_LENGTH
				|| StringUtils.length(pwd) > WileyCoreConstants.MAX_FIELD_LENGTH)
		{
			errors.rejectValue("j_password", "profile.currentPassword.invalid");
		}
	}

	@Override
	public boolean validateEmailAddress(final String email)
	{
		final Matcher matcher = WileyCoreConstants.EMAIL_REGEX.matcher(email);
		return matcher.matches();
	}

}
