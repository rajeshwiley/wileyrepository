package com.wiley.welags.storefrontcommons.forms.validation;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.user.data.RegionData;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.validation.Errors;
import org.springframework.validation.SmartValidator;

import com.wiley.welags.storefrontcommons.forms.WelAgsAddressForm;

import static com.wiley.welags.storefrontcommons.forms.validation.AbstractWelAgsAddressValidator.FIELD_LIMIT_EXCEED;
import static com.wiley.welags.storefrontcommons.forms.validation.AbstractWelAgsAddressValidator.WileyAddressField.POSTCODE;
import static com.wiley.welags.storefrontcommons.forms.validation.AbstractWelAgsAddressValidator.WileyAddressField.REGION;
import static com.wiley.welags.storefrontcommons.forms.validation.AbstractWelAgsAddressValidator.WileyAddressField.ZIPCODE;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WelAgsAddressFormValidationTest
{
	private static final String USA_COUNTRY_ISO_CODE = "US";
	private static final String CANADA_COUNTRY_ISO_CODE = "CA";
	private static final String ZERO = "";
	private static final String TEST_NAME_LENGTH_1 = "1";
	private static final String TEST_NAME_LENGTH_2 = "11";
	private static final String TEST_POST_CODE_2 = "11";
	private static final String NON_USA = "AL";
	private static final int NAME_LENGTH_1 = 1;
	private static final int NAME_LENGTH_2 = 2;

	private static final int ZIPCODE_LENGTH_1 = 1;
	private static final int ZIPCODE_LENGTH_2 = 2;

	private static final int POSTCODE_LENGTH_2 = 2;
	private static final int POSTCODE_LENGTH_1 = 1;
	private static final String TEST_USA_STATE = "DC";
	private static final String TEST_CANADA_REGION = "BC";
	@Mock
	protected I18NFacade i18NFacade;


	@Before
	public void setup()
	{
		final RegionData usaRedionData1 = new RegionData();
		final RegionData usaRedionData2 = new RegionData();

		final RegionData canadaRegionData1 = new RegionData();
		final RegionData canadaRegionData2 = new RegionData();

		List<RegionData> usaRegions = new ArrayList<RegionData>();
		usaRegions.add(usaRedionData1);
		usaRegions.add(usaRedionData2);
		List<RegionData> canadaRegions = new ArrayList<RegionData>();
		canadaRegions.add(canadaRegionData1);
		canadaRegions.add(canadaRegionData1);
		when(i18NFacade.getRegionsForCountryIso(USA_COUNTRY_ISO_CODE)).thenReturn(usaRegions);
		when(i18NFacade.getRegionsForCountryIso(CANADA_COUNTRY_ISO_CODE)).thenReturn(canadaRegions);
		when(i18NFacade.getRegion(USA_COUNTRY_ISO_CODE, TEST_USA_STATE)).thenReturn(usaRedionData1);
		when(i18NFacade.getRegion(CANADA_COUNTRY_ISO_CODE, TEST_CANADA_REGION)).thenReturn(canadaRegionData1);
	}

	@Test
	public void shouldPassForUsa()
	{
		Errors errorsMock = Mockito.mock(Errors.class);

		SmartValidator validator = new TestInstance(NAME_LENGTH_1, ZIPCODE_LENGTH_2, POSTCODE_LENGTH_1, i18NFacade);
		validator.validate(createForm(TEST_NAME_LENGTH_1, TEST_POST_CODE_2,
				USA_COUNTRY_ISO_CODE, TEST_USA_STATE), errorsMock, "");

		verify(errorsMock, times(0)).rejectValue(anyString(), anyString());
		verify(errorsMock, times(0)).rejectValue(anyString(), anyString(), Matchers.any(), anyString());

	}


	@Test
	public void shouldPassForCanada()
	{
		Errors errorsMock = Mockito.mock(Errors.class);

		SmartValidator validator = new TestInstance(NAME_LENGTH_1, ZIPCODE_LENGTH_2,
				POSTCODE_LENGTH_1, i18NFacade);
		validator.validate(createForm(TEST_NAME_LENGTH_1, TEST_POST_CODE_2,
				CANADA_COUNTRY_ISO_CODE, TEST_CANADA_REGION), errorsMock, "");
		verify(errorsMock, times(0)).rejectValue(anyString(), anyString());
		verify(errorsMock, times(0)).rejectValue(anyString(), anyString(), Matchers.any(), anyString());

	}

	@Test
	public void shouldFailForCountryNull()
	{
		Errors errorsMock = Mockito.mock(Errors.class);

		SmartValidator validator = new TestInstance(NAME_LENGTH_1, ZIPCODE_LENGTH_2,
				POSTCODE_LENGTH_2, i18NFacade);

		validator.validate(createForm(TEST_NAME_LENGTH_1, TEST_POST_CODE_2,
				null, TEST_USA_STATE), errorsMock, "");

		verify(errorsMock, times(1)).rejectValue(anyString(), anyString());
		verify(errorsMock, times(0)).rejectValue(anyString(), eq(FIELD_LIMIT_EXCEED), Matchers.any(), anyString());

	}

	@Test
	public void shouldFailForUsaZipcodeLen()
	{
		Errors errorsMock = Mockito.mock(Errors.class);

		SmartValidator validator = new TestInstance(NAME_LENGTH_1, ZIPCODE_LENGTH_1,
				POSTCODE_LENGTH_2, i18NFacade);

		validator.validate(createForm(TEST_NAME_LENGTH_1, TEST_POST_CODE_2,
				USA_COUNTRY_ISO_CODE, TEST_USA_STATE), errorsMock, "");

		verify(errorsMock, times(0)).rejectValue(anyString(), anyString());
		verify(errorsMock, times(1)).rejectValue(anyString(), eq(FIELD_LIMIT_EXCEED), Matchers.any(), anyString());

	}



	@Test
	public void shouldFailForCanadaZipcodeLen()
	{
		Errors errorsMock = Mockito.mock(Errors.class);

		SmartValidator validator = new TestInstance(NAME_LENGTH_1, ZIPCODE_LENGTH_1,
				POSTCODE_LENGTH_2, i18NFacade);

		validator.validate(createForm(TEST_NAME_LENGTH_1, TEST_POST_CODE_2,
				CANADA_COUNTRY_ISO_CODE, TEST_CANADA_REGION), errorsMock, "");

		verify(errorsMock, times(0)).rejectValue(anyString(), anyString());
		verify(errorsMock, times(1)).rejectValue(anyString(), eq(FIELD_LIMIT_EXCEED), Matchers.any(), anyString());

	}



	@Test
	public void shouldFailForUsaZipcodeEmpty()
	{
		Errors errorsMock = Mockito.mock(Errors.class);

		SmartValidator validator = new TestInstance(NAME_LENGTH_1, ZIPCODE_LENGTH_1,
				POSTCODE_LENGTH_2, i18NFacade);

		validator.validate(createForm(TEST_NAME_LENGTH_1, null,
				USA_COUNTRY_ISO_CODE, TEST_USA_STATE), errorsMock, "");

		verify(errorsMock, times(1)).rejectValue(anyString(), eq(ZIPCODE.getErrorKey()));
		verify(errorsMock, times(0)).rejectValue(anyString(), anyString(), Matchers.any(), anyString());
	}


	@Test
	public void shouldFailForCanadaZipcodeEmpty()
	{
		Errors errorsMock = Mockito.mock(Errors.class);

		SmartValidator validator = new TestInstance(NAME_LENGTH_1, ZIPCODE_LENGTH_1,
				POSTCODE_LENGTH_2, i18NFacade);

		validator.validate(createForm(TEST_NAME_LENGTH_1, null,
				CANADA_COUNTRY_ISO_CODE, TEST_CANADA_REGION), errorsMock, "");

		verify(errorsMock, times(1)).rejectValue(anyString(), eq(ZIPCODE.getErrorKey()));
		verify(errorsMock, times(0)).rejectValue(anyString(), anyString(), Matchers.any(), anyString());
	}

	@Test
	public void shouldFailForUsaNamesLen()
	{
		Errors errorsMock = Mockito.mock(Errors.class);

		SmartValidator validator = new TestInstance(NAME_LENGTH_1, ZIPCODE_LENGTH_2,
				POSTCODE_LENGTH_2, i18NFacade);

		validator.validate(createForm(TEST_NAME_LENGTH_2, TEST_POST_CODE_2,
				USA_COUNTRY_ISO_CODE, TEST_USA_STATE), errorsMock, "");

		verify(errorsMock, times(0)).rejectValue(anyString(), anyString());
		verify(errorsMock, times(6)).rejectValue(anyString(), anyString(), Matchers.any(), anyString());

	}

	@Test
	public void shouldFailForCanadaNamesLen()
	{
		Errors errorsMock = Mockito.mock(Errors.class);

		SmartValidator validator = new TestInstance(NAME_LENGTH_1, ZIPCODE_LENGTH_2,
				POSTCODE_LENGTH_2, i18NFacade);

		validator.validate(createForm(TEST_NAME_LENGTH_2, TEST_POST_CODE_2,
				CANADA_COUNTRY_ISO_CODE, TEST_CANADA_REGION), errorsMock, "");

		verify(errorsMock, times(0)).rejectValue(anyString(), anyString());
		verify(errorsMock, times(6)).rejectValue(anyString(), anyString(), Matchers.any(), anyString());

	}

	@Test
	public void shouldFailForUsaNamesEmpty()
	{
		Errors errorsMock = Mockito.mock(Errors.class);

		SmartValidator validator = new TestInstance(NAME_LENGTH_1, ZIPCODE_LENGTH_2,
				POSTCODE_LENGTH_2, i18NFacade);

		validator.validate(createForm(null, TEST_POST_CODE_2,
				USA_COUNTRY_ISO_CODE, TEST_USA_STATE), errorsMock, "");

		verify(errorsMock, times(5)).rejectValue(anyString(), anyString()); // Line2 is not mandatory
		verify(errorsMock, times(0)).rejectValue(anyString(), anyString(), Matchers.any(), anyString());

	}

	@Test
	public void shouldFailForCanadaNamesEmpty()
	{
		Errors errorsMock = Mockito.mock(Errors.class);

		SmartValidator validator = new TestInstance(NAME_LENGTH_1, ZIPCODE_LENGTH_2,
				POSTCODE_LENGTH_2, i18NFacade);

		validator.validate(createForm(null, TEST_POST_CODE_2, CANADA_COUNTRY_ISO_CODE,
				TEST_CANADA_REGION), errorsMock, "");

		verify(errorsMock, times(5)).rejectValue(anyString(), anyString()); // Line2 is not mandatory
		verify(errorsMock, times(0)).rejectValue(anyString(), anyString(), Matchers.any(), anyString());

	}


	@Test
	public void shouldFailForCanadaNoRegionForIsocode()
	{
		Errors errorsMock = Mockito.mock(Errors.class);

		SmartValidator validator = new TestInstance(NAME_LENGTH_1, ZIPCODE_LENGTH_2,
				POSTCODE_LENGTH_2, i18NFacade);

		validator.validate(createForm(TEST_NAME_LENGTH_1, TEST_POST_CODE_2, CANADA_COUNTRY_ISO_CODE,
				TEST_USA_STATE), errorsMock, "");

		verify(errorsMock, times(1)).rejectValue(anyString(), anyString()); // Line2 is not mandatory
		verify(errorsMock, times(0)).rejectValue(anyString(), anyString(), Matchers.any(), anyString());

	}

	@Test
	public void shouldFailForNonUsaPostcodeLen()
	{
		Errors errorsMock = Mockito.mock(Errors.class);

		SmartValidator validator = new TestInstance(NAME_LENGTH_1, ZIPCODE_LENGTH_1,
				POSTCODE_LENGTH_1, i18NFacade);

		validator.validate(createForm(TEST_NAME_LENGTH_1, TEST_POST_CODE_2,
				NON_USA, TEST_USA_STATE), errorsMock, "");

		verify(errorsMock, times(0)).rejectValue(anyString(), anyString());
		verify(errorsMock, times(1)).rejectValue(anyString(), eq(FIELD_LIMIT_EXCEED), Matchers.any(), anyString());

	}

	@Test
	public void shouldFailForUsaRegion()
	{
		Errors errorsMock = Mockito.mock(Errors.class);
		final String formPrefix = "prefix.";

		SmartValidator validator = new TestInstance(NAME_LENGTH_2, ZIPCODE_LENGTH_2,
				POSTCODE_LENGTH_2, i18NFacade);

		validator.validate(createForm(TEST_NAME_LENGTH_2, TEST_POST_CODE_2,
				USA_COUNTRY_ISO_CODE, null), errorsMock,
				formPrefix);

		verify(errorsMock).rejectValue(eq(formPrefix + REGION.getFieldKey()), eq(REGION.getErrorKey()));

	}


	@Test
	public void shouldFailForCanadaRegion()
	{
		Errors errorsMock = Mockito.mock(Errors.class);
		final String formPrefix = "prefix.";

		SmartValidator validator = new TestInstance(NAME_LENGTH_2, ZIPCODE_LENGTH_2,
				POSTCODE_LENGTH_2, i18NFacade);

		validator.validate(createForm(TEST_NAME_LENGTH_2, TEST_POST_CODE_2,
				CANADA_COUNTRY_ISO_CODE, null), errorsMock,
				formPrefix);

		verify(errorsMock).rejectValue(eq(formPrefix + REGION.getFieldKey()), eq(REGION.getErrorKey()));

	}

	@Test
	public void shouldFailForNonUsaPostcodeEmpty()
	{
		Errors errorsMock = Mockito.mock(Errors.class);

		SmartValidator validator = new TestInstance(NAME_LENGTH_1, ZIPCODE_LENGTH_1, 2, i18NFacade);

		validator.validate(createForm(TEST_NAME_LENGTH_1, null, NON_USA, "DC"), errorsMock, "");
		verify(errorsMock, times(1)).rejectValue(anyString(), eq(POSTCODE.getErrorKey()));
		verify(errorsMock, times(0)).rejectValue(anyString(), anyString(), Matchers.any(), anyString());
	}

	private WelAgsAddressForm createForm(final String fName, final String postcode, final String country, final String region)
	{
		WelAgsAddressForm form = new WelAgsAddressForm();
		form.setFirstName(fName);
		form.setLastName(fName);
		form.setLine1(fName);
		form.setLine2(fName);
		form.setPhone(fName);
		form.setTownCity(fName);

		form.setRegionIso(region);
		form.setCountryIso(country);
		form.setPostcode(postcode);
		return form;
	}

	private static class TestInstance extends AbstractWelAgsAddressValidator
	{

		int nameLen;
		int zipcodeLen;
		int postcodeLen;

		TestInstance(final int name, final int zipcode, final int postcode, final I18NFacade i18NFacade)
		{
			nameLen = name;
			zipcodeLen = zipcode;
			postcodeLen = postcode;
			setI18NFacade(i18NFacade);
		}

		@Override
		protected Map<WileyAddressField, Integer> getMaxLengths()
		{
			HashMap<WileyAddressField, Integer> maxLens = new HashMap<>();
			maxLens.put(WileyAddressField.FIRSTNAME, nameLen);
			maxLens.put(WileyAddressField.LASTNAME, nameLen);
			maxLens.put(WileyAddressField.LINE1, nameLen);
			maxLens.put(WileyAddressField.LINE2, nameLen);
			maxLens.put(REGION, MAX_FIELD_LENGTH);
			maxLens.put(WileyAddressField.ZIPCODE, zipcodeLen);
			maxLens.put(WileyAddressField.COUNTRY, MAX_FIELD_LENGTH);
			maxLens.put(WileyAddressField.PHONE, nameLen);
			maxLens.put(WileyAddressField.POSTCODE, postcodeLen);
			maxLens.put(WileyAddressField.PROVINCE, nameLen);
			maxLens.put(WileyAddressField.TOWN, nameLen);
			return maxLens;
		}
	}
}
