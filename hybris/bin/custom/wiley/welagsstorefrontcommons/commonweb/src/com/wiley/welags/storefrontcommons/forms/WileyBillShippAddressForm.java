package com.wiley.welags.storefrontcommons.forms;

public class WileyBillShippAddressForm
{
	private WelAgsAddressForm billingAddress;
	private WelAgsAddressForm shippingAddress;
	private boolean billingAsShipping;
	private boolean billingOnly;
	private String paymentMethod;
	private String sessionId;

	public boolean isBillingOnly()
	{
		return billingOnly;
	}

	public void setBillingOnly(final boolean billingOnly)
	{
		this.billingOnly = billingOnly;
	}

	public boolean isBillingAsShipping()
	{
		return billingAsShipping;
	}

	public void setBillingAsShipping(final boolean billingAsShipping)
	{
		this.billingAsShipping = billingAsShipping;
	}

	public String getPaymentMethod()
	{
		return paymentMethod;
	}

	public void setPaymentMethod(final String paymentMethod)
	{
		this.paymentMethod = paymentMethod;
	}

	public WelAgsAddressForm getBillingAddress()
	{
		return billingAddress;
	}

	public void setBillingAddress(final WelAgsAddressForm billingAddress)
	{
		this.billingAddress = billingAddress;
	}

	public WelAgsAddressForm getShippingAddress()
	{
		return shippingAddress;
	}

	public void setShippingAddress(final WelAgsAddressForm shippingAddress)
	{
		this.shippingAddress = shippingAddress;
	}

	public String getSessionId()
	{
		return sessionId;
	}

	public void setSessionId(final String sessionId)
	{
		this.sessionId = sessionId;
	}
}
