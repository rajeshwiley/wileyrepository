package com.wiley.welags.storefrontcommons.forms.validation;

import de.hybris.platform.acceleratorstorefrontcommons.forms.RegisterForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.RegistrationValidator;

import java.util.regex.Matcher;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.Errors;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.welags.storefrontcommons.forms.WileyRegisterForm;


public class WileyRegistrationFormValidator extends RegistrationValidator
{
	private static final int MAX_FIELD_LENGTH = 255;
	private static final int MIN_PASSWORD_LENGTH = 6;

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final WileyRegisterForm registerForm = (WileyRegisterForm) object;
		final String email = registerForm.getEmail();
		final String confirmationEmail = registerForm.getConfirmEmail();

		validateBasicFields(registerForm, errors); //OOTB fields

		if (StringUtils.isEmpty(confirmationEmail))
		{
			errors.rejectValue("confirmEmail", "register.confirmEmail.empty");
		}
		else if (!StringUtils.equalsIgnoreCase(email, confirmationEmail))
		{
			errors.rejectValue("confirmEmail", "register.confirmEmail.match");
		}


	}

	/**
	 * OOTB Fields validation
	 */
	public void validateBasicFields(final RegisterForm registerForm, final Errors errors)
	{
		final String firstName = registerForm.getFirstName();
		final String lastName = registerForm.getLastName();
		final String email = registerForm.getEmail();
		final String pwd = registerForm.getPwd();
		final String checkPwd = registerForm.getCheckPwd();

		if (StringUtils.isBlank(firstName))
		{
			errors.rejectValue("firstName", "register.firstName.empty");
		}
		if (StringUtils.isBlank(lastName))
		{
			errors.rejectValue("lastName", "register.lastName.empty");
		}

		// First and last names are concatenated with a single space
		if (StringUtils.length(firstName) + StringUtils.length(lastName) > MAX_FIELD_LENGTH - 1)
		{
			if (!errors.hasFieldErrors("firstName"))
			{
				errors.rejectValue("firstName", "register.name.invalid");
			}
			if (!errors.hasFieldErrors("lastName"))
			{
			errors.rejectValue("lastName", "register.name.invalid");
			}
		}

		if (StringUtils.isEmpty(email))
		{
			errors.rejectValue("email", "register.email.empty");
		}
		else if (StringUtils.length(email) > MAX_FIELD_LENGTH || !validateEmailAddress(email))
		{
			errors.rejectValue("email", "register.email.invalid");
		}

		if (StringUtils.isEmpty(pwd))
		{
			errors.rejectValue("pwd", "register.pwd.empty");
		}
		else if (StringUtils.length(pwd) < MIN_PASSWORD_LENGTH)
		{
			errors.rejectValue("pwd", "register.pwd.invalid");
		}
		else if (StringUtils.length(pwd) > MAX_FIELD_LENGTH)
		{
			errors.rejectValue("pwd", "register.pwd.invalid.length");
		}


		if (StringUtils.isNotEmpty(pwd) && StringUtils.isNotEmpty(checkPwd) && !StringUtils.equals(pwd, checkPwd))
		{
			errors.rejectValue("checkPwd", "register.checkPwd.equals");
		}
		else
		{
			if (StringUtils.isEmpty(checkPwd))
			{
				errors.rejectValue("checkPwd", "register.checkPwd.invalid");
			}
		}
	}

	@Override
	public boolean validateEmailAddress(final String email)
	{
		final Matcher matcher = WileyCoreConstants.EMAIL_REGEX.matcher(email);
		return matcher.matches();
	}

}
