package com.wiley.welags.storefrontcommons.controllers.pages.checkout.steps;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.AddressForm;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.DeliveryModeData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.RegionData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;

import java.time.Year;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Nonnull;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.SmartValidator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.gson.Gson;
import com.wiley.core.integration.mpgs.WileyMPGSPaymentGateway;
import com.wiley.core.mpgs.constants.WileyMPGSConstants;
import com.wiley.core.mpgs.enums.MPGSSupportedCardType;
import com.wiley.facades.payment.data.WileyMPGSPaymentResult;
import com.wiley.facades.payment.mpgs.WileyMPGSPaymentFacade;
import com.wiley.facades.voucher.WileyCouponFacade;
import com.wiley.facades.welags.order.WelAgsCheckoutFacade;
import com.wiley.facades.wiley.order.WileyCartFacade;
import com.wiley.storefront.checkout.DeliveryOptionData;
import com.wiley.welags.storefrontcommons.controllers.ControllerConstants;
import com.wiley.welags.storefrontcommons.controllers.exceptions.InvalidWileyAddressFormException;
import com.wiley.welags.storefrontcommons.data.populators.DeliveryOptionDataPopulator;
import com.wiley.welags.storefrontcommons.forms.WelAgsAddressForm;
import com.wiley.welags.storefrontcommons.forms.WileyBillShippAddressForm;

import static com.wiley.core.constants.WileyCoreConstants.WEL_GMAT_CATEGORY_CODE;
import static com.wiley.core.enums.PaymentModeEnum.CARD;


/**
 * This class is intended to control BillingAddress step on checkout
 */
@RequestMapping(value = "/checkout/multi/billing-shipping-address")
public class BillingShippingAddressCheckoutStepController extends AbstractAddressCheckoutStepController
{
	private static final Logger LOG = LoggerFactory.getLogger(BillingShippingAddressCheckoutStepController.class);

	private static final String MULTI_CHECKOUT_BILLING_ONLY_CMS_PAGE_LABEL = "multiStepCheckoutBilling";

	private static final String BILLING_FORM_PREFIX = "billingAddress.";
	private static final String SHIPPING_FORM_PREFIX = "shippingAddress.";

	private static final String BILLING_SHIPPING_FORM_ID = "wileyBillShippAddressForm";

	private static final String PAY_PAL_PAYMENT_METHOD = "payPal";
	private static final String PAY_PAL_PAYMENT_URL = "/checkout/paypal/from-billing";

	private static final String BILLING_SHIPPING_URL = "/checkout/multi/billing-shipping-address/add";
	private static final String BILLING_ONLY_URL = "/checkout/multi/billing-shipping-address/billing-only-add";
	private static final Integer MONTH_AMOUNT = 12;


	@Resource(name = "i18NFacade")
	private I18NFacade i18NFacade;
	@Resource(name = "deliveryOptionDataPopulator")
	private DeliveryOptionDataPopulator deliveryOptionDataPopulator;
	@Resource
	private WelAgsCheckoutFacade checkoutFacade;
	@Resource
	private WileyCouponFacade wileyCouponFacade;

	@Resource(name = "wileyMpgsPaymentFacade")
	private WileyMPGSPaymentFacade wileyMPGSPaymentFacade;

	@Resource(name = "wileyCartFacade")
	private WileyCartFacade wileyCartFacade;

	@Resource(name = "welAgsSubscribeToGmacValidator")
	private SmartValidator welAgsSubscribeToGmacValidator;

	@Value("${default.country.isocode}")
	private String defaultCountryIsocode;

	@Autowired
	private WileyMPGSPaymentGateway wileyMpgsPaymentGateway;

	@ModelAttribute("billingCountries")
	public Collection<CountryData> getBillingCountries()
	{
		return getCheckoutFacade().getBillingCountries();
	}

	@ModelAttribute("shippingCountries")
	public Collection<CountryData> getShippingCountries(final Model model)
	{
		return (Collection) model.asMap().get("countries");
	}

	@ModelAttribute("paymentCardMonths")
	public List<SelectOption> getPaymentCardMonths()
	{
		final List<SelectOption> months = new ArrayList<>();
		for (Integer monthNumber = 1; monthNumber <= MONTH_AMOUNT; monthNumber++)
		{
			months.add(new SelectOption(monthNumber.toString(),
					getMessageSource().getMessage("payment.month." + monthNumber, null, getI18nService().getCurrentLocale())));
		}
		return months;
	}

	@ModelAttribute("cardExpiryYears")
	public List<SelectOption> getCardExpiryYears()
	{
		final List<SelectOption> expiryYears = new ArrayList<SelectOption>();
		int currentYear = Year.now().getValue();
		final Integer yearAmount = Integer.valueOf(getSiteConfigService().getProperty("payment.mpgs.calendar.yearAmount"));

		for (int year = currentYear; year <= (currentYear + yearAmount); year++)
		{
			expiryYears.add(new SelectOption(String.valueOf(year), String.valueOf(year)));
		}
		return expiryYears;
	}

	@ModelAttribute("supportedCardTypes")
	public Set<String> getSupportedCardTypes()
	{
		Set<String> supportedCardTypes = new HashSet<String>();
		for (final MPGSSupportedCardType cardType : MPGSSupportedCardType.values())
		{
			supportedCardTypes.add(cardType.getStringValue());
		}
		return supportedCardTypes;
	}

	@ModelAttribute("hostedSessionErrorMessages")
	public String getHostedSessionErrorMessages()
	{
		Map<String, String> messageMap = new HashMap<>();
		messageMap.put("globalAlertCorrectMessages",
				getMessageSource().getMessage("form.global.error", null, getI18nService().getCurrentLocale()));
		messageMap.put("processingError", getMessageSource()
				.getMessage("payment.hostedSesstion.processing.error", null, getI18nService().getCurrentLocale()));
		messageMap.put("securityCodeInvalid",
				getMessageSource().getMessage("payment.securityCode.error", null, getI18nService().getCurrentLocale()));
		messageMap.put("securityCodeNotProvided",
				getMessageSource().getMessage("payment.securityCode.notProvided", null, getI18nService().getCurrentLocale()));
		messageMap.put("expirationDateInvalid",
				getMessageSource().getMessage("payment.expirationDate.error", null, getI18nService().getCurrentLocale()));
		messageMap.put("paymentCardNumberInvalid",
				getMessageSource().getMessage("payment.paymentCardNumber.error", null, getI18nService().getCurrentLocale()));
		messageMap.put("paymentCardNotSupported", getMessageSource().getMessage("payment.paymentCard.notSupported", null,
				getI18nService().getCurrentLocale()));
		Gson gson = new Gson();
		return gson.toJson(messageMap);
	}

	@Override
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	@RequireHardLogIn
	@PreValidateCheckoutStep(checkoutStep = BILLING_ADDRESS_STEP)
	public String enterStep(final Model model, final RedirectAttributes redirectAttributes)
			throws CMSItemNotFoundException, CommerceCartModificationException
	{
		prepareDataForPage(model);
		storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		model.addAttribute(WebConstants.BREADCRUMBS_KEY,
				getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.deliveryAddress.breadcrumb"));
		model.addAttribute("metaRobots", "noindex,nofollow");


		setCheckoutStepLinksForModel(model, getCheckoutStep());

		final WileyBillShippAddressForm addressForm = createAddressForm(false);
		if (StringUtils.isNotEmpty(addressForm.getBillingAddress().getAddressId())
				&& StringUtils.isNotEmpty(addressForm.getShippingAddress().getAddressId()))
		{
			// compare billing and shipping addresses by fields
			Comparator<WelAgsAddressForm> addressComparator = Comparator.comparing(WelAgsAddressForm::getFirstName)
					.thenComparing(WelAgsAddressForm::getLastName)
					.thenComparing(WelAgsAddressForm::getCountryIso)
					.thenComparing(WelAgsAddressForm::getLine1)
					.thenComparing(WelAgsAddressForm::getLine2, Comparator.nullsFirst(Comparator.naturalOrder()))
					.thenComparing(WelAgsAddressForm::getPhone, Comparator.nullsFirst(Comparator.naturalOrder()))
					.thenComparing(WelAgsAddressForm::getPostcode)
					.thenComparing(WelAgsAddressForm::getRegionIso, Comparator.nullsFirst(Comparator.naturalOrder()))
					.thenComparing(WelAgsAddressForm::getTownCity);

			addressForm.setBillingAsShipping(
					addressComparator.compare(addressForm.getBillingAddress(), addressForm.getShippingAddress()) == 0);
		}
		model.addAttribute(BILLING_SHIPPING_FORM_ID, addressForm);
		prepareCustomDataForPage(model, addressForm);

		model.addAttribute("cartData", getCheckoutFacade().getCheckoutCart());
		model.addAttribute("displayDeliveryCost", customerHasDefaultShippingAddress());
		return ControllerConstants.Views.Pages.MultiStepCheckout.ADD_EDIT_BILLING_ADDRESS_PAGE;
	}

	@RequestMapping(value = "/billingShippingAddressValidation", method = RequestMethod.POST)
	public String wileyBillingShippingAddressFormValidation(final WileyBillShippAddressForm wileyBillShippAddressForm,
			final BindingResult bindingResult, final Model model, final HttpServletResponse httpResponse)
			throws CMSItemNotFoundException
	{
		validateCombinedForm(wileyBillShippAddressForm, bindingResult);

		if (bindingResult.hasErrors())
		{
			httpResponse.setStatus(HttpStatus.BAD_REQUEST.value());

		}
		if (checkoutFacade.isDigitalSessionCart())
		{
			return returnWithErrors(wileyBillShippAddressForm, MULTI_CHECKOUT_BILLING_ONLY_CMS_PAGE_LABEL, model);
		}
		else
		{
			return returnWithErrors(wileyBillShippAddressForm, MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL, model);
		}

	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@RequireHardLogIn
	public String add(final WileyBillShippAddressForm wileyBillShippAddressForm, final BindingResult bindingResult,
			final Model model, final RedirectAttributes ra) throws CMSItemNotFoundException
	{
		validateCombinedForm(wileyBillShippAddressForm, bindingResult);

		if (bindingResult.hasErrors())
		{
			return returnWithErrors(wileyBillShippAddressForm, MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL, model);
		}

		final AddressData billingAddress = convertToAddressData(wileyBillShippAddressForm.getBillingAddress(), true, false);
		final AddressData shippingAddress = convertToAddressData(wileyBillShippAddressForm.getShippingAddress(), false, true);

		LOG.debug("Insert/update addresses for checkout");

		getWileyCheckoutFacade().updateBillingAndShippingAddresses(billingAddress, shippingAddress,
				wileyBillShippAddressForm.isBillingAsShipping());
		subscribeToGMACInfo(wileyBillShippAddressForm.getBillingAddress());
		subscribeToUpdates(wileyBillShippAddressForm.getBillingAddress());
		wileyCouponFacade.removeNotValidVouchersFromCart();

		// it's required because for student flow hoPaymentUrl is saved in session
		checkoutFacade.getSavedHopPaymentUrlAndRemove();
		if (isPayPalPaymentMethod(wileyBillShippAddressForm))
		{
			checkoutFacade.saveHopPaymentReturnUrl(BILLING_SHIPPING_URL);
			return REDIRECT_PREFIX + PAY_PAL_PAYMENT_URL;
		}
		else if (!isZeroOrder())
		{
			String sessionId = wileyBillShippAddressForm.getSessionId();
			WileyMPGSPaymentResult mpgsPaymentResult = wileyMPGSPaymentFacade.retrieveSessionAndTokenize(sessionId, true);
			checkoutFacade.setPaymentMode(CARD);

			if (!mpgsPaymentResult.getIsOperationSuccessful())
			{
				String messageKey = mpgsPaymentResult.getMessageKey();
				return handleMPGSErrors(ra, messageKey, wileyBillShippAddressForm, MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL, model);
			}
		}
		else
		{
			checkoutFacade.resetCartPaymentInfo();
		}

		return getCheckoutStep().nextStep();
	}

	private String handleMPGSErrors(final RedirectAttributes ra, final String messageKey,
			final WileyBillShippAddressForm wileyBillShippAddressForm, final String pageLabel,
			final Model model) throws CMSItemNotFoundException
	{

		GlobalMessages.addFlashMessage(ra, GlobalMessages.ERROR_MESSAGES_HOLDER,
				WileyMPGSConstants.MESSAGE_KEY_ISSUE_COMMON_MESSAGE,
				new Object[] { getContactMail(), getContactPhone(), messageKey });
		storeCmsPageInModel(model, getContentPageForLabelOrId(pageLabel));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(pageLabel));
		if (StringUtils.isNotBlank(wileyBillShippAddressForm.getBillingAddress().getCountryIso()))
		{
			model.addAttribute("country", wileyBillShippAddressForm.getBillingAddress().getCountryIso());
		}
		model.addAttribute("cartData", getCheckoutFacade().getCheckoutCart());
		prepareCustomDataForPage(model, wileyBillShippAddressForm);
		return getCurrentCheckoutStep();
	}

	private String getCurrentCheckoutStep()
	{
		if (checkoutFacade.isDigitalSessionCart())
		{
			return REDIRECT_PREFIX + BILLING_ONLY_URL;
		}
		else
		{
			return getCheckoutStep().currentStep();
		}
	}

	private boolean isPayPalPaymentMethod(final WileyBillShippAddressForm wileyBillShippAddressForm)
	{
		return PAY_PAL_PAYMENT_METHOD.equals(wileyBillShippAddressForm.getPaymentMethod());
	}

	@RequestMapping(value = "/billing-only-add", method = RequestMethod.GET)
	@RequireHardLogIn
	public String billingOnlyAdd(final Model model, final RedirectAttributes redirectAttributes)
			throws CMSItemNotFoundException, CommerceCartModificationException
	{
		prepareDataForPage(model);
		storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_BILLING_ONLY_CMS_PAGE_LABEL));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_BILLING_ONLY_CMS_PAGE_LABEL));
		model.addAttribute(WebConstants.BREADCRUMBS_KEY,
				getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.deliveryAddress.breadcrumb"));
		model.addAttribute("metaRobots", "noindex,nofollow");

		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		model.addAttribute("cartData", cartData);

		setCheckoutStepLinksForModel(model, getCheckoutStep());

		final WileyBillShippAddressForm addressForm = createAddressForm(true);
		model.addAttribute(BILLING_SHIPPING_FORM_ID, addressForm);
		prepareCustomDataForPage(model, addressForm);

		return ControllerConstants.Views.Pages.MultiStepCheckout.ADD_EDIT_BILLING_ADDRESS_PAGE;

	}

	@RequestMapping(value = "/billing-only-add", method = RequestMethod.POST)
	@RequireHardLogIn
	public String billingOnlyAdd(final WileyBillShippAddressForm wileyBillShippAddressForm, final BindingResult bindingResult,
			final Model model, final RedirectAttributes ra) throws CMSItemNotFoundException
	{
		validateCombinedForm(wileyBillShippAddressForm, bindingResult);

		if (bindingResult.hasErrors())
		{
			return returnWithErrors(wileyBillShippAddressForm, MULTI_CHECKOUT_BILLING_ONLY_CMS_PAGE_LABEL, model);
		}

		final AddressData billingAddress = convertToAddressData(wileyBillShippAddressForm.getBillingAddress(), true, false);

		LOG.debug("Insert/update addresses for checkout");

		getWileyCheckoutFacade().updateBillingAndShippingAddresses(billingAddress, null, true);

		// it's required because for student flow hoPaymentUrl is saved in session
		checkoutFacade.getSavedHopPaymentUrlAndRemove();
		if (isPayPalPaymentMethod(wileyBillShippAddressForm))
		{
			checkoutFacade.saveHopPaymentReturnUrl(BILLING_ONLY_URL);
			return REDIRECT_PREFIX + PAY_PAL_PAYMENT_URL;
		}

		else if (!isZeroOrder())
		{
			String sessionId = wileyBillShippAddressForm.getSessionId();
			WileyMPGSPaymentResult mpgsPaymentResult = wileyMPGSPaymentFacade.retrieveSessionAndTokenize(sessionId, true);

			if (!mpgsPaymentResult.getIsOperationSuccessful())
			{
				String messageKey = mpgsPaymentResult.getMessageKey();
				return handleMPGSErrors(ra, messageKey, wileyBillShippAddressForm, MULTI_CHECKOUT_BILLING_ONLY_CMS_PAGE_LABEL,
						model);
			}
		}

		return getCheckoutStep().nextStep();
	}

	private String returnWithErrors(final WileyBillShippAddressForm wileyBillShippAddressForm, final String pageLabel,
			final Model model) throws CMSItemNotFoundException
	{
		GlobalMessages.addErrorMessage(model, FORM_GLOBAL_ERROR);
		storeCmsPageInModel(model, getContentPageForLabelOrId(pageLabel));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(pageLabel));
		if (StringUtils.isNotBlank(wileyBillShippAddressForm.getBillingAddress().getCountryIso()))
		{
			model.addAttribute("country", wileyBillShippAddressForm.getBillingAddress().getCountryIso());
		}
		model.addAttribute("cartData", getCheckoutFacade().getCheckoutCart());
		model.addAttribute("displayDeliveryCost", customerHasDefaultShippingAddress());
		prepareCustomDataForPage(model, wileyBillShippAddressForm);
		return ControllerConstants.Views.Pages.MultiStepCheckout.ADD_EDIT_BILLING_ADDRESS_PAGE;
	}

	@RequestMapping(value = "/billing-only-addressform", method = RequestMethod.GET)
	public String getBillingOnlyCountryAddressForm(final WileyBillShippAddressForm wileyBillShippAddressForm, final Model model)
	{
		// as billingAsShipping is missed on page we assign it explicit to support logic of billShipp page
		wileyBillShippAddressForm.setBillingAsShipping(wileyBillShippAddressForm.isBillingOnly());
		prepareCustomDataForPage(model, wileyBillShippAddressForm);
		model.addAttribute(BILLING_SHIPPING_FORM_ID, wileyBillShippAddressForm);

		return ControllerConstants.Views.Fragments.Address.COUNTRY_SPECIFIC_ADDRESS_FORM;
	}

	@RequestMapping(value = "/addressform", method = RequestMethod.GET)
	@RequireHardLogIn
	public String getCountryAddressForm(final WileyBillShippAddressForm wileyBillShippAddressForm, final Model model)
	{
		String countryIso = wileyBillShippAddressForm.getBillingAddress().getCountryIso();
		if (!wileyBillShippAddressForm.isBillingAsShipping())
		{
			countryIso = wileyBillShippAddressForm.getShippingAddress().getCountryIso();
		}
		getWileyCheckoutFacade().updateCountryInAddress(countryIso, wileyBillShippAddressForm.isBillingAsShipping());

		final AddressData billingAddress = convertToAddressData(wileyBillShippAddressForm.getBillingAddress(), true, false);
		final AddressData shippingAddress = convertToAddressData(wileyBillShippAddressForm.getShippingAddress(), false, true);

		LOG.debug("Insert/update addresses for checkout");

		getWileyCheckoutFacade().updateBillingAndShippingAddresses(billingAddress, shippingAddress,
				wileyBillShippAddressForm.isBillingAsShipping());

		prepareCustomDataForPage(model, wileyBillShippAddressForm);

		model.addAttribute(BILLING_SHIPPING_FORM_ID, wileyBillShippAddressForm);

		return ControllerConstants.Views.Fragments.Address.COUNTRY_SPECIFIC_ADDRESS_FORM;
	}

	@RequestMapping(value = "/initShippingAddress", method = RequestMethod.GET)
	@RequireHardLogIn
	public String initShippingAddress(final WileyBillShippAddressForm wileyBillShippAddressForm, final Model model)
	{
		//if we just uncheked "shipping as billing", copy billing to shipping
		wileyBillShippAddressForm.setShippingAddress(wileyBillShippAddressForm.getBillingAddress());
		return getCountryAddressForm(wileyBillShippAddressForm, model);
	}

	@RequestMapping(value = "/deliveryoptions", method = RequestMethod.GET)
	@ResponseBody
	@RequireHardLogIn
	public List<DeliveryOptionData> getDeliveryOptionsForCountry(@RequestParam("countryIsoCode") final String countryIso)
	{
		final List<DeliveryModeData> deliveryModes = getWileyCheckoutFacade().getPotentialDeliveryModesForCountry(countryIso);
		List<DeliveryOptionData> deliveryOptions = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(deliveryModes))
		{
			deliveryOptionDataPopulator.populate(deliveryModes, deliveryOptions);
			getWileyCheckoutFacade().setDeliveryMode(deliveryModes.get(0).getCode());
		}
		return deliveryOptions;
	}

	@RequestMapping(value = "/saveAddressAndCalculateTax", method = RequestMethod.POST)
	@RequireHardLogIn
	public String saveCustomerAddressAndCalculateTax(@RequestParam("changedFormId") final String changedFormId,
			final WileyBillShippAddressForm wileyBillShippAddressForm,
			final BindingResult bindingResult,
			final Model model) throws InvalidWileyAddressFormException
	{
		validateChangedFormPart(changedFormId, wileyBillShippAddressForm, bindingResult);
		if (bindingResult.hasErrors())
		{
			throw new InvalidWileyAddressFormException(bindingResult.getAllErrors().toString());
		}
		final boolean originBillingOnly = wileyBillShippAddressForm.isBillingOnly();
		/*
			Below flags guarantee that we save address as default for
			shipping, billing or both, depending on flags.
			For shipping there is an additional logic to check sameAsBilling attribute
		 */
		final boolean isBilling = isBillingForm(changedFormId);
		final boolean isShipping = isShippingForm(changedFormId);
		final AddressForm changedForm = getFormPartById(changedFormId, wileyBillShippAddressForm);
		final AddressData newAddress = convertToAddressData(changedForm, isBilling, isShipping);
		LOG.debug("Create address for tax calculation");
		getWileyCheckoutFacade().updateDefaultAddress(newAddress);
		if (!isShipping && wileyBillShippAddressForm.isBillingAsShipping() && isBilling)
		{
			final AddressData clonedAddress = convertToAddressData(changedForm, false, true);
			getWileyCheckoutFacade().updateDefaultAddress(clonedAddress);
		}
		if (isBilling)
		{
			subscribeToUpdates(wileyBillShippAddressForm.getBillingAddress());
		}
		getWileyCheckoutFacade().calculateCart();

		model.addAttribute("cartData", getCheckoutFacade().getCheckoutCart());
		model.addAttribute("displayDeliveryCost", customerHasDefaultShippingAddress());

		final WileyBillShippAddressForm addressForm = createAddressForm(originBillingOnly);
		prepareCustomDataForPage(model, addressForm);
		model.addAttribute(BILLING_SHIPPING_FORM_ID, addressForm);

		return ControllerConstants.Views.Fragments.Address.COUNTRY_SPECIFIC_ADDRESS_FORM;
	}

	@RequestMapping(value = "/updateDeliveryMethod", method = RequestMethod.GET)
	@RequireHardLogIn
	public String updateDeliveryMethod(@RequestParam("deliveryMethod") final String deliveryMethod, final Model model)
	{
		getWileyCheckoutFacade().setDeliveryMode(deliveryMethod);
		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		model.addAttribute("cartData", cartData);
		model.addAttribute("displayDeliveryCost", customerHasDefaultShippingAddress());
		return ControllerConstants.Views.Fragments.Checkout.UPDATE_TAX_FORM;
	}

	private void prepareCustomDataForPage(final Model model, final WileyBillShippAddressForm wileyBillShippAddressForm)
	{
		final String billingAddressCountryIso = wileyBillShippAddressForm.getBillingAddress().getCountryIso();
		final String shippingAddressCountryIso = wileyBillShippAddressForm.getShippingAddress() != null ?
				wileyBillShippAddressForm.getShippingAddress().getCountryIso() :
				null;
		final boolean isBillingAsShipping = wileyBillShippAddressForm.isBillingAsShipping();
		String countryIsoForDeliveryModes = billingAddressCountryIso;
		if (!isBillingAsShipping && !wileyBillShippAddressForm.isBillingOnly())
		{
			countryIsoForDeliveryModes = shippingAddressCountryIso;
		}
		prepareCustomDataForPage(model, billingAddressCountryIso, shippingAddressCountryIso);
		model.addAttribute("isShippingFormVisible", !isBillingAsShipping);
		model.addAttribute("isPreOrder", checkoutFacade.isPreOrderCart());
		model.addAttribute("isShowGmacSelect", wileyCartFacade.isSessionCartWithProductsFromCategory(WEL_GMAT_CATEGORY_CODE));

		// we do not show delivery methods if the cart has only non-shippable products.
		if (!getWileyCheckoutFacade().isDigitalSessionCart())
		{
			final List<DeliveryModeData> deliveryModes = getWileyCheckoutFacade().getPotentialDeliveryModesForCountry(
					countryIsoForDeliveryModes);
			model.addAttribute("deliveryMethods", deliveryModes);
			getWileyCheckoutFacade().setDeliveryModeIfNotAvailable(countryIsoForDeliveryModes);
		}
	}

	private void prepareCustomDataForPage(final Model model, final String billingAddressCountryIso,
			final String shippingAddressCountryIso)
	{
		if (billingAddressCountryIso != null && billingAddressCountryIso.equals(shippingAddressCountryIso))
		{
			List<RegionData> regions = i18NFacade.getRegionsForCountryIso(billingAddressCountryIso);
			model.addAttribute("billingAddressRegions", regions);
			model.addAttribute("shippingAddressRegions", regions);
		}
		else
		{
			if (billingAddressCountryIso != null)
			{
				model.addAttribute("billingAddressRegions",
						i18NFacade.getRegionsForCountryIso(billingAddressCountryIso));
			}
			if (shippingAddressCountryIso != null)
			{
				model.addAttribute("shippingAddressRegions",
						i18NFacade.getRegionsForCountryIso(shippingAddressCountryIso));
			}
		}
		model.addAttribute("billingFormPrefix", BILLING_FORM_PREFIX);
		model.addAttribute("shippingFormPrefix", SHIPPING_FORM_PREFIX);
	}

	private boolean isShippingForm(final String formId)
	{
		return formId.startsWith(SHIPPING_FORM_PREFIX);
	}

	private boolean isBillingForm(final String formId)
	{
		return formId.startsWith(BILLING_FORM_PREFIX);
	}

	private AddressForm getFormPartById(final String formId, final WileyBillShippAddressForm wileyBillShippAddressForm)
	{
		AddressForm addressForm = null;
		if (isBillingForm(formId))
		{
			addressForm = wileyBillShippAddressForm.getBillingAddress();
		}
		else if (isShippingForm(formId))
		{
			addressForm = wileyBillShippAddressForm.getShippingAddress();
		}
		return addressForm;
	}

	private void validateChangedFormPart(final String changedFormId, final WileyBillShippAddressForm wileyBillShippAddressForm,
			final BindingResult bindingResult)
	{
		if (changedFormId == null)
		{
			return;
		}
		if (isBillingForm(changedFormId))
		{
			validateBillingAddress(wileyBillShippAddressForm, bindingResult);
		}
		else if (isShippingForm(changedFormId))
		{
			validateShippingAddress(wileyBillShippAddressForm, bindingResult);
		}
	}

	private void validateCombinedForm(final WileyBillShippAddressForm wileyBillShippAddressForm,
			final BindingResult bindingResult)
	{

		validateBillingAddress(wileyBillShippAddressForm, bindingResult);
		if (!wileyBillShippAddressForm.isBillingAsShipping() && wileyBillShippAddressForm.getShippingAddress() != null)
		{
			validateShippingAddress(wileyBillShippAddressForm, bindingResult);
		}
	}

	private void validateBillingAddress(final WileyBillShippAddressForm wileyBillShippAddressForm,
			final BindingResult bindingResult)
	{
		getWileyAddressValidator().validate(wileyBillShippAddressForm.getBillingAddress(), bindingResult, BILLING_FORM_PREFIX);
		if (wileyBillShippAddressForm.getBillingAddress().getHasGmacSelect())
		{
			welAgsSubscribeToGmacValidator.validate(wileyBillShippAddressForm.getBillingAddress().getSubscribeGmacInfo(),
					bindingResult, BILLING_FORM_PREFIX);
		}
	}

	private void validateShippingAddress(final WileyBillShippAddressForm wileyBillShippAddressForm,
			final BindingResult bindingResult)
	{
		getWileyAddressValidator().validate(wileyBillShippAddressForm.getShippingAddress(), bindingResult,
				SHIPPING_FORM_PREFIX);
	}

	private WileyBillShippAddressForm createAddressForm(boolean billingOnly)
	{
		final WileyBillShippAddressForm addressForm = new WileyBillShippAddressForm();
		addressForm.setBillingAsShipping(true);
		addressForm.setBillingOnly(billingOnly);

		addressForm.setBillingAddress(new WelAgsAddressForm());
		addressForm.getBillingAddress().setCountryIso(defaultCountryIsocode);

		if (customerHasDefaultBillingAddress())
		{
			final AddressData billingAddress = getCustomerFacade().getCurrentCustomer().getDefaultBillingAddress();
			final AddressForm billingForm = addressForm.getBillingAddress();
			copyAddressDataToAddressForm(billingAddress, billingForm);
		}

		addressForm.setShippingAddress(new WelAgsAddressForm());

		if (!billingOnly)
		{
			addressForm.getShippingAddress().setCountryIso(defaultCountryIsocode);

			if (customerHasDefaultShippingAddress())
			{
				final AddressData shippingAddress = getCustomerFacade().getCurrentCustomer().getDefaultShippingAddress();
				copyAddressDataToAddressForm(shippingAddress, addressForm.getShippingAddress());
			}
		}
		return addressForm;
	}

	private void subscribeToGMACInfo(@Nonnull final WelAgsAddressForm addressForm)
	{
		Assert.notNull(addressForm, "Parameter addressForm cannot be null.");

		final Boolean subscribeToGmac = addressForm.getSubscribeGmacInfo();
		if (Boolean.TRUE.equals(subscribeToGmac))
		{
			getWileyCheckoutFacade().subscribeToGMACInfo(true);
		}
		else if (Boolean.FALSE.equals(subscribeToGmac))
		{
			getWileyCheckoutFacade().subscribeToGMACInfo(false);
		}
	}

}
