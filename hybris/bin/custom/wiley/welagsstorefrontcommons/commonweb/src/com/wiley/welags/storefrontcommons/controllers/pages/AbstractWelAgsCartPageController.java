package com.wiley.welags.storefrontcommons.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.voucher.exceptions.VoucherOperationException;

import javax.validation.Valid;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.facades.voucher.WileyCouponFacade;
import com.wiley.facades.welags.voucher.exception.InvalidVoucherException;
import com.wiley.facades.welags.voucher.exception.VoucherCannotBeAppliedException;
import com.wiley.storefrontcommons.controllers.pages.AbstractWileyCartPageController;
import com.wiley.welags.storefrontcommons.voucher.VoucherForm;


public abstract class AbstractWelAgsCartPageController extends AbstractWileyCartPageController
{


	private static final String REDIRECT_CART_URL = REDIRECT_PREFIX + "/cart";

	protected abstract CartFacade getCartFacade();

	protected abstract WileyCouponFacade getCouponFacade();

	protected abstract String prepareCart(Model model) throws CMSItemNotFoundException;

	protected abstract Logger getLogger();

	@RequestMapping(method = RequestMethod.POST)
	public String applyVoucherForCart(final VoucherForm form, final Model model, final BindingResult bindingResult)
			throws CMSItemNotFoundException
	{
		final String discountCode = form.getDiscountCodeValue();

		if (getLogger().isDebugEnabled())
		{
			getLogger().debug("apply voucher : voucherCode = " + sanitize(discountCode));
		}

		try
		{
			if (getCartFacade().hasEntries())
			{
				getCouponFacade().applyVoucher(discountCode);
				model.addAttribute("voucherAppliedMessage", getVoucherAppliedMessage(form.getDiscountCodeValue()));
			}
			// empty field for redeemed voucher
			form.setDiscountCodeValue(StringUtils.EMPTY);
		}
		catch (InvalidVoucherException ex)
		{
			rejectDiscountCode(bindingResult, "text.voucher.error");
		}
		catch (VoucherCannotBeAppliedException ex)
		{
			rejectDiscountCode(bindingResult, "text.voucher.priority.error");
		}
		catch (VoucherOperationException ex)
		{
			getLogger().info("Failed to apply voucher because of exception: ", ex);
			rejectDiscountCode(bindingResult, "text.voucher.error.unexpected");
		}
		finally
		{
			return prepareCart(model);
		}
	}

	protected void rejectDiscountCode(final BindingResult bindingResult, final String messageKey)
	{
		bindingResult.rejectValue("discountCodeValue", messageKey, new Object[] {}, StringUtils.EMPTY);
	}


	@RequestMapping(value = "/voucher/remove", method = RequestMethod.POST)
	public String removeVoucher(@Valid final VoucherForm form, final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException
	{
		try
		{
			getCouponFacade().releaseVoucher(form.getDiscountCodeValue());
		}
		catch (final VoucherOperationException e)
		{
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "text.voucher.release.error",
					new Object[] { form.getDiscountCodeValue() });
			getLogger().debug(e.getMessage(), e);
		}
		finally
		{
			return REDIRECT_CART_URL;
		}
	}

	protected String sanitize(final String input)
	{
		// clean input
		String output = StringUtils.defaultString(input).trim();
		// remove CRLF injection
		output = output.replaceAll("(\\r\\n|\\r|\\n)+", " ");
		// escape html
		output = StringEscapeUtils.escapeHtml(output);
		return output;
	}

}
