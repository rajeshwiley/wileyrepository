package com.wiley.welags.storefrontcommons.controllers.pages.account;

import de.hybris.platform.acceleratorfacades.payment.data.PaymentSubscriptionResultData;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.user.UserFacade;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.facades.payment.WileyPaymentFacade;
import com.wiley.storefrontcommons.util.HttpServletRequestUtil;
import com.wiley.welags.storefrontcommons.controllers.ControllerConstants;


@RequestMapping("/my-account/hop")
public class UpdateCardDetailsResponseController extends AbstractPageController
{
	private static final Logger LOG = LoggerFactory.getLogger(UpdateCardDetailsResponseController.class);

	private static final String REDIRECT_TO_PAYMENT_INFO_PAGE =
			REDIRECT_PREFIX + ControllerConstants.Views.Pages.Payment.PAYMENT_INFO_PAGE;

	@Resource(name = "welAgsPaymentFacade")
	private WileyPaymentFacade wileyPaymentFacade;
	@Autowired
	private UserFacade userFacade;

	private HttpServletRequestUtil httpServletRequestUtil = new HttpServletRequestUtil();

	@RequestMapping(value = "/response", method = RequestMethod.POST)
	@RequireHardLogIn
	public String doHandleResponse(final RedirectAttributes redirectAttributes, final HttpServletRequest request)
	{
		final Map<String, String> resultMap = httpServletRequestUtil.getRequestParameterMap(request);
		final PaymentSubscriptionResultData paymentSubscriptionResultData = 
						wileyPaymentFacade.completeHopValidatePayment(resultMap, true);

		if (paymentSubscriptionResultData.isSuccess() && paymentSubscriptionResultData.getStoredCard() != null
				&& StringUtils.isNotBlank(paymentSubscriptionResultData.getStoredCard().getSubscriptionId()))
		{
			final CCPaymentInfoData paymentInfoData = paymentSubscriptionResultData.getStoredCard();
			userFacade.setDefaultPaymentInfo(paymentInfoData);
		}
		else
		{
			final Object[] parameters =
			{ paymentSubscriptionResultData.getResultCode() };
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"account.card.update.error.general", parameters);
			LOG.error("Failed to update card.  Please check the log files for more information");
		}
		return REDIRECT_TO_PAYMENT_INFO_PAGE;
	}

	public void setHttpServletRequestUtil(final HttpServletRequestUtil httpServletRequestUtil)
	{
		this.httpServletRequestUtil = httpServletRequestUtil;
	}
}
