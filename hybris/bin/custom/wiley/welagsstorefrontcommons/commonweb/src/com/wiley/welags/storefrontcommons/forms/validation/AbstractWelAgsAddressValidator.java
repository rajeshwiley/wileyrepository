/**
 *
 */
package com.wiley.welags.storefrontcommons.forms.validation;

import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.AddressValidator;
import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.user.data.RegionData;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.SmartValidator;

import com.wiley.welags.storefrontcommons.forms.WelAgsAddressForm;

import static com.wiley.welags.storefrontcommons.forms.validation.AbstractWelAgsAddressValidator.WileyAddressField.COUNTRY;
import static com.wiley.welags.storefrontcommons.forms.validation.AbstractWelAgsAddressValidator.WileyAddressField.FIRSTNAME;
import static com.wiley.welags.storefrontcommons.forms.validation.AbstractWelAgsAddressValidator.WileyAddressField.LASTNAME;
import static com.wiley.welags.storefrontcommons.forms.validation.AbstractWelAgsAddressValidator.WileyAddressField.LINE1;
import static com.wiley.welags.storefrontcommons.forms.validation.AbstractWelAgsAddressValidator.WileyAddressField.LINE2;
import static com.wiley.welags.storefrontcommons.forms.validation.AbstractWelAgsAddressValidator.WileyAddressField.PHONE;
import static com.wiley.welags.storefrontcommons.forms.validation.AbstractWelAgsAddressValidator.WileyAddressField.POSTCODE;
import static com.wiley.welags.storefrontcommons.forms.validation.AbstractWelAgsAddressValidator.WileyAddressField.PROVINCE;
import static com.wiley.welags.storefrontcommons.forms.validation.AbstractWelAgsAddressValidator.WileyAddressField.REGION;
import static com.wiley.welags.storefrontcommons.forms.validation.AbstractWelAgsAddressValidator.WileyAddressField.TOWN;
import static com.wiley.welags.storefrontcommons.forms.validation.AbstractWelAgsAddressValidator.WileyAddressField.ZIPCODE;


public abstract class AbstractWelAgsAddressValidator extends AddressValidator implements SmartValidator
{
	protected static final int MAX_FIELD_LENGTH = 255;
	protected static final String FIELD_LIMIT_EXCEED = "address.field.limit.exceed";

	private I18NFacade i18NFacade;

	protected abstract Map<WileyAddressField, Integer> getMaxLengths();

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return WelAgsAddressForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		validate(object, errors, "");
	}

	@Override
	public void validate(final Object object, final Errors errors, final Object... objects)
	{
		final WelAgsAddressForm addressForm = (WelAgsAddressForm) object;
		String formPrefix = (String) objects[0];
		validateStandardFields(addressForm, formPrefix, errors);
		validateCountrySpecificFields(addressForm, formPrefix, errors);
	}

	protected void validateStandardFields(final WelAgsAddressForm addressForm, final String formPrefix, final Errors errors)
	{
		Map<WileyAddressField, Integer> maxLengths = getMaxLengths();
		validateStringField(addressForm.getCountryIso(), COUNTRY, formPrefix, maxLengths.get(COUNTRY), errors);
		validateStringField(addressForm.getFirstName(), FIRSTNAME, formPrefix, maxLengths.get(FIRSTNAME), errors);
		validateStringField(addressForm.getLastName(), LASTNAME, formPrefix, maxLengths.get(LASTNAME), errors);
		validateStringField(addressForm.getLine1(), LINE1, formPrefix, maxLengths.get(LINE1), errors);
		validateFieldMaxLength(addressForm.getLine2(), LINE2, formPrefix, maxLengths.get(LINE2), errors);
		validateStringField(addressForm.getPhone(), PHONE, formPrefix, maxLengths.get(PHONE), errors);
	}

	protected void validateCountrySpecificFields(final WelAgsAddressForm addressForm, final String formPrefix,
			final Errors errors)
	{
		Map<WileyAddressField, Integer> maxLengths = getMaxLengths();
		final String countryIsoCode = addressForm.getCountryIso();
		List<RegionData> regionsForCountryIso = i18NFacade.getRegionsForCountryIso(countryIsoCode);
		if (CollectionUtils.isNotEmpty(regionsForCountryIso))
		{
			final String regionIso = addressForm.getRegionIso();
			validateFieldNotNull(regionIso, REGION, formPrefix, errors);
			if (regionIso != null) {
				RegionData regionData = i18NFacade.getRegion(countryIsoCode, regionIso);
				validateRegionExists(regionData, REGION, formPrefix, errors);
			}
			validateStringField(addressForm.getTownCity(), TOWN, formPrefix, maxLengths.get(TOWN), errors);
			validateStringField(addressForm.getPostcode(), ZIPCODE, formPrefix, maxLengths.get(ZIPCODE), errors);
		}
		else
		{
			validateStringField(addressForm.getPostcode(), POSTCODE, formPrefix, maxLengths.get(POSTCODE), errors);
			validateStringField(addressForm.getTownCity(), PROVINCE, formPrefix, maxLengths.get(PROVINCE), errors);
		}
	}

	/**
	 * Validates String field on Empty or length restrictions
	 *  @param addressField
	 * 		- field value
	 * @param fieldType
	 * 		- metadata about field
	 * @param maxFieldLength
 * 		- Max fields length
	 * @param errors
* 		- errors bundle
	 */
	protected static void validateStringField(final String addressField, final WileyAddressField fieldType,
			final String formPrefix, final int maxFieldLength, final Errors errors)
	{
		if (StringUtils.isEmpty(addressField))
		{
			errors.rejectValue(formPrefix + fieldType.getFieldKey(), fieldType.getErrorKey());
		}
		else if (StringUtils.length(addressField) > maxFieldLength)
		{
			String errorMessageKey = fieldType.getErrorMessageKey();
			errors.rejectValue(formPrefix + fieldType.getFieldKey(), errorMessageKey,
					new String[] { String.valueOf(maxFieldLength) }, errorMessageKey);
		}
	}

	protected static void validateFieldNotNull(final String addressField, final WileyAddressField fieldType,
			final String formPrefix, final Errors errors)
	{
		if (addressField == null)
		{
			errors.rejectValue(formPrefix + fieldType.getFieldKey(), fieldType.getErrorKey());
		}
	}

	protected static void validateRegionExists(final RegionData region, final WileyAddressField fieldType,
			final String formPrefix, final Errors errors)
	{
		if (region == null)
		{
			errors.rejectValue(formPrefix + fieldType.getFieldKey(), fieldType.getErrorKey());
		}
	}

	/**
	 * Validates whether field max lengths is not exceeded, do no makes checks on null, empty string
	 *
	 * @param value
	 * 		- field value
	 * @param addressField
	 * 		- field metadata
	 * @param maxFieldLength
	 * 		- max field length allowed
	 * @param errors
	 * 		- errors bundle
	 */
	protected static void validateFieldMaxLength(final String value, final WileyAddressField addressField,
			final String formPrefix, final int maxFieldLength, final Errors errors)
	{
		if (StringUtils.isNotEmpty(value) && value.length() > maxFieldLength)
		{
			String[] messageArguments = { String.valueOf(maxFieldLength) };
			errors.rejectValue(formPrefix + addressField.getFieldKey(), FIELD_LIMIT_EXCEED, messageArguments, FIELD_LIMIT_EXCEED);
		}
	}

	public enum WileyAddressField
	{
		FIRSTNAME("firstName", "address.firstName.invalid", FIELD_LIMIT_EXCEED),
		LASTNAME("lastName", "address.lastName.invalid", FIELD_LIMIT_EXCEED),
		LINE1("line1", "address.line1.invalid", FIELD_LIMIT_EXCEED),
		LINE2("line2", "address.line2.invalid", FIELD_LIMIT_EXCEED),
		TOWN("townCity", "address.townCity.invalid", FIELD_LIMIT_EXCEED),
		PROVINCE("townCity", "address.cityProvince.invalid", FIELD_LIMIT_EXCEED),
		ZIPCODE("postcode", "address.zipcode.invalid", FIELD_LIMIT_EXCEED),
		POSTCODE("postcode", "address.postcode.invalid", FIELD_LIMIT_EXCEED),
		REGION("regionIso", "address.state.invalid", FIELD_LIMIT_EXCEED),
		COUNTRY("countryIso", "address.country.invalid", FIELD_LIMIT_EXCEED),
		PHONE("phone", "address.phone.invalid", FIELD_LIMIT_EXCEED);

		private final String fieldKey;
		private final String errorKey;
		private final String errorMessageKey;

		WileyAddressField(final String fieldKey, final String errorKey, final String errorMessageKey)
		{
			this.fieldKey = fieldKey;
			this.errorKey = errorKey;
			this.errorMessageKey = errorMessageKey;
		}

		public String getFieldKey()
		{
			return fieldKey;
		}

		public String getErrorKey()
		{
			return errorKey;
		}

		public String getErrorMessageKey()
		{
			return errorMessageKey;
		}

	}

	public I18NFacade getI18NFacade()
	{
		return i18NFacade;
	}

	public void setI18NFacade(final I18NFacade i18NFacade)
	{
		this.i18NFacade = i18NFacade;
	}
}