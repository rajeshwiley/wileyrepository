package com.wiley.welags.storefrontcommons.checkout.steps.validation.impl;

import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.AbstractCheckoutStepValidator;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.facades.welags.order.WelAgsCheckoutFacade;


/**
 * Created by Uladzimir_Barouski on 2/29/2016.
 */
public class WelStudentVerificationStepValidator extends AbstractCheckoutStepValidator
{
	private static final Logger LOG = LoggerFactory.getLogger(WelStudentVerificationStepValidator.class);

	@Autowired
	private WelAgsCheckoutFacade welAgsCheckoutFacade;

	@Override
	public ValidationResults validateOnEnter(final RedirectAttributes redirectAttributes)
	{
		if (!getCheckoutFlowFacade().hasValidCart())
		{
			LOG.info("Missing, empty or unsupported cart");
			return ValidationResults.REDIRECT_TO_CART;
		}

		if (!welAgsCheckoutFacade.isStudentFlow() || welAgsCheckoutFacade.isCartHasStudentVerification())
		{
			return ValidationResults.REDIRECT_TO_PAYMENT_METHOD;
		}
		return ValidationResults.SUCCESS;
	}
}
