package com.wiley.welags.storefrontcommons.forms.validation.groups;

import javax.validation.GroupSequence;

@GroupSequence(value = { NotEmptyGroup.class, LengthCheckGroup.class })
public interface WileyUpdatePwdFormValidationOrder {
}
