package com.wiley.welags.storefrontcommons.forms;

/**
 * Form object for updating profile.
 *
 * Created by Mikhail_Kondratyev on 05-Dec-15.
 */
public class WileyUpdateProfileForm
{
	private String emailUid;
	private String firstName;
	private String lastName;

	public String getEmailUid()
	{
		return emailUid;
	}

	public void setEmailUid(final String emailUid)
	{
		this.emailUid = emailUid;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(final String firstName)
	{
		this.firstName = firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(final String lastName)
	{
		this.lastName = lastName;
	}
}
