package com.wiley.welags.storefrontcommons.forms;

/**
 * Form for Student Verification Page
 */
public class StudentVerificationForm
{
	private String countryIso;
	private String regionIso;
	private String universityCode;
	private String universityTextField;
	private boolean agreement;

	public String getCountryIso()
	{
		return countryIso;
	}

	public void setCountryIso(final String countryIso)
	{
		this.countryIso = countryIso;
	}

	public String getRegionIso()
	{
		return regionIso;
	}

	public void setRegionIso(final String regionIso)
	{
		this.regionIso = regionIso;
	}

	public String getUniversityCode()
	{
		return universityCode;
	}

	public void setUniversityCode(final String universityCode)
	{
		this.universityCode = universityCode;
	}

	public String getUniversityTextField()
	{
		return universityTextField;
	}

	public void setUniversityTextField(final String universityTextField)
	{
		this.universityTextField = universityTextField;
	}

	public boolean isAgreement()
	{
		return agreement;
	}

	public void setAgreement(final boolean agreement)
	{
		this.agreement = agreement;
	}
}
