/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.welags.storefrontcommons.security;

import de.hybris.platform.acceleratorstorefrontcommons.security.BruteForceAttackCounter;
import de.hybris.platform.core.Constants;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.spring.security.CoreAuthenticationProvider;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;


/**
 * Derived authentication provider supporting additional authentication checks. See
 * {@link de.hybris.platform.spring.security.RejectUserPreAuthenticationChecks}.
 *
 * <ul>
 * <li>prevent login without password for users created via CSCockpit</li>
 * <li>prevent login as user in group admingroup</li>
 * </ul>
 *
 * any login as admin disables SearchRestrictions and therefore no page can be viewed correctly
 */
public class WileyAuthenticationProvider extends CoreAuthenticationProvider
{
	private static final Logger LOG = Logger.getLogger(WileyAuthenticationProvider.class);
	private static final String ROLE_ADMIN_GROUP = "ROLE_" + Constants.USER.ADMIN_USERGROUP.toUpperCase();
	public static final String J_PASSWORD = "j_password";

	private BruteForceAttackCounter bruteForceAttackCounter;
	private UserService userService;
	private ModelService modelService;
	private GrantedAuthority adminAuthority = new SimpleGrantedAuthority(ROLE_ADMIN_GROUP);


	@Override
	public Authentication authenticate(final Authentication authentication) throws AuthenticationException
	{
		final String username = (authentication.getPrincipal() == null) ? "NONE_PROVIDED" : authentication.getName();

		if (getBruteForceAttackCounter().isAttack(username))
		{
			try
			{
				final UserModel userModel = getUserService().getUserForUID(StringUtils.lowerCase(username));
				userModel.setLoginDisabled(true);
				getModelService().save(userModel);
				bruteForceAttackCounter.resetUserCounter(userModel.getUid());
			}
			catch (final UnknownIdentifierException e)
			{
				LOG.warn("Brute force attack attempt for non existing user name " + username);
			}

			throw new BadCredentialsException(messages.getMessage("CoreAuthenticationProvider.badCredentials",
					"Bad credentials"));

		}

		return doAuthentication(authentication, username);
	}

	private Authentication doAuthentication(final Authentication authentication, final String username)
	{
		final String sourcePassword = getSourcePassword();
		Authentication authenticationResult = null;
		try
		{
			UsernamePasswordAuthenticationToken sourcePasswordAuthenticationToken = getAuthenticationToken(authentication,
					username, sourcePassword);
			//Attempt to authenticate with source password
			authenticationResult = super.authenticate(sourcePasswordAuthenticationToken);
		}
		catch (BadCredentialsException ex)
		{
			//Attempt to authenticate with stripped password
			authenticationResult = super.authenticate(authentication);
			markUserWithIncorrectPassword(username);
		}
		return authenticationResult;
	}

	private CustomerModel getCustomerModel(final String username)
	{
		return (CustomerModel) getUserService().getUserForUID(StringUtils.lowerCase(username));
	}

	private UsernamePasswordAuthenticationToken getAuthenticationToken(final Authentication authentication, final String username,
			final String sourcePassword)
	{
		UsernamePasswordAuthenticationToken authRequestWithTypedPassword = new UsernamePasswordAuthenticationToken(
				username, sourcePassword);
		authRequestWithTypedPassword.setDetails(authentication.getDetails());
		return authRequestWithTypedPassword;
	}

	private void markUserWithIncorrectPassword(final String username)
	{
		final CustomerModel userModel = getCustomerModel(username);
		if (!userModel.getHasStrippedPassword())
		{
			userModel.setHasStrippedPassword(true);
			getModelService().save(userModel);
		}
	}

	private String getSourcePassword()
	{
		HttpServletRequest servletRequest =
				((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		final String[] password = servletRequest.getParameterMap().get(J_PASSWORD);
		return password != null ? password[0] : StringUtils.EMPTY;
	}

	/**
	 * @see de.hybris.platform.spring.security.CoreAuthenticationProvider#additionalAuthenticationChecks(org.springframework
	 * .security.core.userdetails.UserDetails,
	 * org.springframework.security.authentication.AbstractAuthenticationToken)
	 */
	@Override
	protected void additionalAuthenticationChecks(final UserDetails details, final AbstractAuthenticationToken authentication)
			throws AuthenticationException
	{
		super.additionalAuthenticationChecks(details, authentication);

		// Check if user has supplied no password
		if (StringUtils.isEmpty((String) authentication.getCredentials()))
		{
			throw new BadCredentialsException("Login without password");
		}

		// Check if the user is in role admingroup
		if (getAdminAuthority() != null && details.getAuthorities().contains(getAdminAuthority()))
		{
			throw new LockedException("Login attempt as " + Constants.USER.ADMIN_USERGROUP + " is rejected");
		}
	}

	/**
	 * Sets admin group.
	 *
	 * @param adminGroup
	 * 		the admin group
	 */
	public void setAdminGroup(final String adminGroup)
	{
		if (StringUtils.isBlank(adminGroup))
		{
			adminAuthority = null;
		}
		else
		{
			adminAuthority = new SimpleGrantedAuthority(adminGroup);
		}
	}

	/**
	 * Gets admin authority.
	 *
	 * @return the admin authority
	 */
	protected GrantedAuthority getAdminAuthority()
	{
		return adminAuthority;
	}

	/**
	 * Gets brute force attack counter.
	 *
	 * @return the brute force attack counter
	 */
	protected BruteForceAttackCounter getBruteForceAttackCounter()
	{
		return bruteForceAttackCounter;
	}

	/**
	 * Sets brute force attack counter.
	 *
	 * @param bruteForceAttackCounter
	 * 		the brute force attack counter
	 */
	@Required
	public void setBruteForceAttackCounter(final BruteForceAttackCounter bruteForceAttackCounter)
	{
		this.bruteForceAttackCounter = bruteForceAttackCounter;
	}

	/**
	 * Gets user service.
	 *
	 * @return the user service
	 */
	protected UserService getUserService()
	{
		return userService;
	}

	/**
	 * Sets user service.
	 *
	 * @param userService
	 * 		the user service
	 */
	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	/**
	 * Gets model service.
	 *
	 * @return the model service
	 */
	protected ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * Sets model service.
	 *
	 * @param modelService
	 * 		the model service
	 */
	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}
}
