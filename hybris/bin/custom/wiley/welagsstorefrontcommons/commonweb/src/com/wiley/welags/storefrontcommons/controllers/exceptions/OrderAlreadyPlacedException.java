package com.wiley.welags.storefrontcommons.controllers.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Your order has already been placed")
public class OrderAlreadyPlacedException extends RuntimeException
{
}
