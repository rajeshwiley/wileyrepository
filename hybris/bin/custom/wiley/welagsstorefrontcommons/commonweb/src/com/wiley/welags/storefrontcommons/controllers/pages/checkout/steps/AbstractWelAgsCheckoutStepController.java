/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.welags.storefrontcommons.controllers.pages.checkout.steps;

import de.hybris.platform.acceleratorfacades.payment.PaymentFacade;
import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.acceleratorservices.customer.CustomerLocationService;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.ContentPageBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.CheckoutGroup;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.CheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.AbstractController;
import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.PaymentDetailsValidator;
import de.hybris.platform.acceleratorstorefrontcommons.forms.verification.AddressVerificationResultHandler;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.TitleData;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.wiley.facades.welags.order.WelAgsCheckoutFacade;
import com.wiley.storefrontcommons.config.SiteContactConfigUtil;
import com.wiley.storefrontcommons.controllers.pages.checkout.steps.AbstractWileyCheckoutStepController;
import com.wiley.welags.storefrontcommons.forms.validation.AbstractWelAgsAddressValidator;


public abstract class AbstractWelAgsCheckoutStepController extends AbstractWileyCheckoutStepController
		implements CheckoutStepController
{
	protected static final String MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL = "multiStepCheckoutSummary";
	protected static final String REDIRECT_URL_ADD_DELIVERY_ADDRESS =
			AbstractController.REDIRECT_PREFIX + "/checkout/multi/delivery-address/add/";
	protected static final String REDIRECT_URL_CHOOSE_DELIVERY_METHOD =
			AbstractController.REDIRECT_PREFIX + "/checkout/multi/delivery-method/choose/";
	protected static final String REDIRECT_URL_ADD_PAYMENT_METHOD =
			AbstractController.REDIRECT_PREFIX + "/checkout/multi/payment-method/add/";
	protected static final String REDIRECT_URL_SUMMARY = AbstractController.REDIRECT_PREFIX + "/checkout/multi/summary/view/";
	protected static final String REDIRECT_URL_CART = AbstractController.REDIRECT_PREFIX + "/cart/";
	protected static final String REDIRECT_URL_ERROR = AbstractController.REDIRECT_PREFIX + "/checkout/multi/hop/error";

	@Resource(name = "paymentDetailsValidator")
	private PaymentDetailsValidator paymentDetailsValidator;

	@Resource
	private CMSSiteService cmsSiteService;

	@Resource
	private SiteConfigService siteConfigService;

	@Resource
	private SiteContactConfigUtil siteContactConfigUtil;

	@Resource(name = "accProductFacade")
	private ProductFacade productFacade;

	@Resource(name = "multiStepCheckoutBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder resourceBreadcrumbBuilder;

	@Resource(name = "paymentFacade")
	private PaymentFacade paymentFacade;

	@Resource(name = "checkoutFacade")
	private WelAgsCheckoutFacade wileyCheckoutFacade;

	@Resource(name = "wileyAddressValidator")
	private AbstractWelAgsAddressValidator addressValidator;

	@Resource(name = "customerLocationService")
	private CustomerLocationService customerLocationService;

	@Resource(name = "cartFacade")
	private CartFacade cartFacade;

	@Resource(name = "addressVerificationResultHandler")
	private AddressVerificationResultHandler addressVerificationResultHandler;

	@Resource(name = "contentPageBreadcrumbBuilder")
	private ContentPageBreadcrumbBuilder contentPageBreadcrumbBuilder;

	@Resource(name = "checkoutFlowGroupMap")
	private Map<String, CheckoutGroup> checkoutFlowGroupMap;

	@ModelAttribute("breadcrumbLabels")
	public Collection<String> getBreadcrumbLabels()
	{
		return getWileyCheckoutFacade().getBreadcrumbLabels();
	}

	@ModelAttribute("titles")
	public Collection<TitleData> getTitles()
	{
		return getUserFacade().getTitles();
	}

	@ModelAttribute
	public void setCountries(final Model model)
	{
		Collection<CountryData> countries = getCheckoutFacade().getDeliveryCountries();
		model.addAttribute("countries", countries);

		final Map<String, CountryData> countryDataMap = new HashMap<String, CountryData>();
		for (final CountryData countryData : countries)
		{
			countryDataMap.put(countryData.getIsocode(), countryData);
		}
		model.addAttribute("countryDataMap", countryDataMap);
	}

	@ModelAttribute("checkoutSteps")
	public List<CheckoutSteps> addCheckoutStepsToModel()
	{
		final CheckoutGroup checkoutGroup = getCheckoutFlowGroupMap().get(getCheckoutFacade().getCheckoutFlowGroupForCheckout());
		final Map<String, CheckoutStep> progressBarMap = checkoutGroup.getCheckoutProgressBar();
		final List<CheckoutSteps> checkoutSteps = new ArrayList<CheckoutSteps>(progressBarMap.size());

		for (final Map.Entry<String, CheckoutStep> entry : progressBarMap.entrySet())
		{
			final CheckoutStep checkoutStep = entry.getValue();
			checkoutSteps.add(new CheckoutSteps(checkoutStep.getProgressBarId(), StringUtils.remove(checkoutStep.currentStep(),
					"redirect:"), Integer.valueOf(entry.getKey())));
		}
		return checkoutSteps;
	}

	protected String getContactPhone()
	{
		return siteContactConfigUtil.getContactPhone();
	}

	protected String getContactMail()
	{
		return siteContactConfigUtil.getContactMail();
	}

	protected void prepareDataForPage(final Model model) throws CMSItemNotFoundException
	{
		Collection<CountryData> countries;
		if (model.containsAttribute("countries"))
		{
			countries = (Collection<CountryData>) model.asMap().get("countries");
		}
		else
		{
			countries = getCartFacade().getDeliveryCountries();
		}
		model.addAttribute("supportedCountries", countries);

		model.addAttribute("isOmsEnabled", Boolean.valueOf(getSiteConfigService().getBoolean("oms.enabled", false)));
		model.addAttribute("expressCheckoutAllowed", Boolean.valueOf(getCheckoutFacade().isExpressCheckoutAllowedForCart()));
		model.addAttribute("taxEstimationEnabled", Boolean.valueOf(getCheckoutFacade().isTaxEstimationEnabledForCart()));
		model.addAttribute("deliveryMethods", getCheckoutFacade().getSupportedDeliveryModes());
	}

	protected CheckoutStep getCheckoutStep(final String currentController)
	{
		final CheckoutGroup checkoutGroup = getCheckoutFlowGroupMap().get(getCheckoutFacade().getCheckoutFlowGroupForCheckout());
		return checkoutGroup.getCheckoutStepMap().get(currentController);
	}

	protected void setCheckoutStepLinksForModel(final Model model, final CheckoutStep checkoutStep)
	{
		model.addAttribute("previousStepUrl", StringUtils.remove(checkoutStep.previousStep(), "redirect:"));
		model.addAttribute("nextStepUrl", StringUtils.remove(checkoutStep.nextStep(), "redirect:"));
		model.addAttribute("currentStepUrl", StringUtils.remove(checkoutStep.currentStep(), "redirect:"));
		model.addAttribute("progressBarId", checkoutStep.getProgressBarId());
	}

	protected Map<String, String> getRequestParameterMap(final HttpServletRequest request)
	{
		final Map<String, String> map = new HashMap<String, String>();

		final Enumeration myEnum = request.getParameterNames();
		while (myEnum.hasMoreElements())
		{
			final String paramName = (String) myEnum.nextElement();
			final String paramValue = request.getParameter(paramName);
			map.put(paramName, paramValue);
		}

		return map;
	}

	@Override
	protected CartFacade getCartFacade()
	{
		return cartFacade;
	}

	protected ProductFacade getProductFacade()
	{
		return productFacade;
	}

	protected PaymentDetailsValidator getPaymentDetailsValidator()
	{
		return paymentDetailsValidator;
	}

	protected ResourceBreadcrumbBuilder getResourceBreadcrumbBuilder()
	{
		return resourceBreadcrumbBuilder;
	}

	protected PaymentFacade getPaymentFacade()
	{
		return paymentFacade;
	}

	protected AbstractWelAgsAddressValidator getAddressValidator()
	{
		return addressValidator;
	}

	protected CustomerLocationService getCustomerLocationService()
	{
		return customerLocationService;
	}

	protected AddressVerificationResultHandler getAddressVerificationResultHandler()
	{
		return addressVerificationResultHandler;
	}

	public ContentPageBreadcrumbBuilder getContentPageBreadcrumbBuilder()
	{
		return contentPageBreadcrumbBuilder;
	}

	public Map<String, CheckoutGroup> getCheckoutFlowGroupMap()
	{
		return checkoutFlowGroupMap;
	}

	public WelAgsCheckoutFacade getWileyCheckoutFacade()
	{
		return wileyCheckoutFacade;
	}
}
