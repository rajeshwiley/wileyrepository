/**
 *
 */
package com.wiley.welags.storefrontcommons.controllers.pages.checkout.steps;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;

import java.util.Collection;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.facades.voucher.WileyCouponFacade;
import com.wiley.welags.storefrontcommons.controllers.ControllerConstants;
import com.wiley.welags.storefrontcommons.controllers.exceptions.InvalidWileyAddressFormException;
import com.wiley.welags.storefrontcommons.forms.WelAgsAddressForm;

// TODO: make use of AbstractWileyAddressValidator

@RequestMapping(value = "/checkout/multi/billing-address")
public class AgsBillingAddressCheckoutStepController extends AbstractAddressCheckoutStepController
{
	private static final Logger LOG = LoggerFactory.getLogger(AgsBillingAddressCheckoutStepController.class);

	@Resource
	private WileyCouponFacade wileyCouponFacade;

	@Value("${default.country.isocode}")
	private String defaultCountryIsocode;

	@ModelAttribute("billingCountries")
	public Collection<CountryData> getBillingCountries()
	{
		return getCheckoutFacade().getBillingCountries();
	}

	@Override
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	@RequireHardLogIn
	//@PreValidateCheckoutStep(checkoutStep = BILLING_ADDRESS)
	public String enterStep(final Model model, final RedirectAttributes redirectAttributes)
			throws CMSItemNotFoundException, CommerceCartModificationException
	{
		prepareDataForPage(model);
		storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		model.addAttribute(WebConstants.BREADCRUMBS_KEY,
				getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.deliveryAddress.breadcrumb"));
		model.addAttribute("metaRobots", "noindex,nofollow");

		final CartData cartData = getCheckoutFacade().getCheckoutCart();

		// For ZERO amount orders we do not need any billing information
		if (null != cartData && isZeroOrder())
		{
			return REDIRECT_URL_SUMMARY;
		}

		model.addAttribute("cartData", cartData);

		setCheckoutStepLinksForModel(model, getCheckoutStep());

		final WelAgsAddressForm addressForm = createAddressForm();
		model.addAttribute("wileyAddressForm", addressForm);
		model.addAttribute("regions", getI18NFacade().getRegionsForCountryIso(addressForm.getCountryIso()));

		return ControllerConstants.Views.Pages.MultiStepCheckout.ADD_EDIT_BILLING_ADDRESS_PAGE;
	}

	@RequestMapping(value = "/saveAddressAndCalculateTax", method = RequestMethod.POST)
	@RequireHardLogIn
	public String saveCustomerAddressAndCalculateTax(final WelAgsAddressForm wileyAddressForm, final BindingResult bindingResult,
			final Model model) throws InvalidWileyAddressFormException
	{
		getAddressValidator().validate(wileyAddressForm, bindingResult);
		if (bindingResult.hasErrors())
		{
			throw new InvalidWileyAddressFormException(bindingResult.getAllErrors().toString());
		}
		final AddressData newAddress = convertToAddressData(wileyAddressForm, true, false);
		if (StringUtils.isBlank(newAddress.getId()))
		{
			getUserFacade().addAddress(newAddress);
			subscribeToUpdates(wileyAddressForm);
			getWileyCheckoutFacade().calculateCart();
		}
		else
		{
			LOG.error("This method should be invoked only when customer doesn't have a billing address");
		}

		model.addAttribute("cartData", getCheckoutFacade().getCheckoutCart());
		return ControllerConstants.Views.Fragments.Checkout.UPDATE_TAX_FORM;
	}

	private WelAgsAddressForm createAddressForm()
	{
		final WelAgsAddressForm addressForm = new WelAgsAddressForm();
		addressForm.setCountryIso(defaultCountryIsocode);

		if (customerHasDefaultBillingAddress())
		{
			final AddressData billingAddress = getCustomerFacade().getCurrentCustomer().getDefaultBillingAddress();

			addressForm.setAddressId(billingAddress.getId());
			addressForm.setFirstName(billingAddress.getFirstName());
			addressForm.setLastName(billingAddress.getLastName());

			if (billingAddress.getCountry() != null)
			{
				addressForm.setCountryIso(billingAddress.getCountry().getIsocode());
			}
			addressForm.setLine1(billingAddress.getLine1());
			addressForm.setLine2(billingAddress.getLine2());
			addressForm.setTownCity(billingAddress.getTown());

			if (billingAddress.getRegion() != null)
			{
				addressForm.setRegionIso(billingAddress.getRegion().getIsocode());
			}

			addressForm.setPostcode(billingAddress.getPostalCode());
			addressForm.setPhone(billingAddress.getPhone());
		}
		return addressForm;
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@RequireHardLogIn
	public String add(@ModelAttribute("wileyAddressForm") final WelAgsAddressForm wileyAddressForm,
			final BindingResult bindingResult, final Model model)
			throws CMSItemNotFoundException
	{
		getAddressValidator().validate(wileyAddressForm, bindingResult);

		if (StringUtils.isNotBlank(wileyAddressForm.getCountryIso()))
		{
			model.addAttribute("regions", getI18NFacade().getRegionsForCountryIso(wileyAddressForm.getCountryIso()));
			model.addAttribute("country", wileyAddressForm.getCountryIso());
		}

		if (bindingResult.hasErrors())
		{
			GlobalMessages.addErrorMessage(model, FORM_GLOBAL_ERROR);
			storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
			model.addAttribute("cartData", getCheckoutFacade().getCheckoutCart());
			return ControllerConstants.Views.Pages.MultiStepCheckout.ADD_EDIT_BILLING_ADDRESS_PAGE;
		}

		final AddressData newAddress = convertToAddressData(wileyAddressForm, true, false);
		getWileyCheckoutFacade().updateBillingAndShippingAddresses(newAddress, null, true);
		subscribeToUpdates(wileyAddressForm);
		wileyCouponFacade.removeNotValidVouchersFromCart();

		return getCheckoutStep().nextStep();
	}

}
