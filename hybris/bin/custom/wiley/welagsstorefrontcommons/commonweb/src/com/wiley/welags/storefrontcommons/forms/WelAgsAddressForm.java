package com.wiley.welags.storefrontcommons.forms;

import de.hybris.platform.acceleratorstorefrontcommons.forms.AddressForm;


public class WelAgsAddressForm extends AddressForm
{
	private String phone;

	private Boolean subscribeGmacInfo;

	private Boolean subscribeToUpdates;

	private Boolean hasGmacSelect;

	public String getPhone()
	{
		return phone;
	}

	public void setPhone(final String phone)
	{
		this.phone = phone;
	}

	public Boolean getSubscribeGmacInfo()
	{
		return subscribeGmacInfo;
	}

	public void setSubscribeGmacInfo(final Boolean subscribeGmacInfo)
	{
		this.subscribeGmacInfo = subscribeGmacInfo;
	}

	public Boolean getSubscribeToUpdates()
	{
		return subscribeToUpdates;
	}

	public void setSubscribeToUpdates(final Boolean subscribeToUpdates)
	{
		this.subscribeToUpdates = subscribeToUpdates;
	}

	public Boolean getHasGmacSelect()
	{
		return hasGmacSelect;
	}

	public void setHasGmacSelect(final Boolean hasGmacSelect)
	{
		this.hasGmacSelect = hasGmacSelect;
	}
}
