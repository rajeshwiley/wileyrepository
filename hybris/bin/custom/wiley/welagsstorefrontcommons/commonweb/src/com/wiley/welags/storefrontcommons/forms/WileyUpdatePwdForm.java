package com.wiley.welags.storefrontcommons.forms;

import de.hybris.platform.acceleratorstorefrontcommons.forms.UpdatePwdForm;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.wiley.core.customer.ResetPasswordRedirectType;
import com.wiley.welags.storefrontcommons.forms.validation.groups.LengthCheckGroup;
import com.wiley.welags.storefrontcommons.forms.validation.groups.NotEmptyGroup;

public class WileyUpdatePwdForm extends UpdatePwdForm {

    private ResetPasswordRedirectType redirectType;

    @Pattern(regexp = ".+", message = "{updatePwd.pwd.empty}", groups = NotEmptyGroup.class)
    @Size(min = 6, max = 255, message = "{updatePwd.pwd.invalid}", groups = LengthCheckGroup.class)
    @Override
    public String getPwd() {
        return super.getPwd();
    }

    @Pattern(regexp = ".+", message = "{updatePwd.pwd.empty}", groups = NotEmptyGroup.class)
    @Size(min = 6, max = 255, message = "{updatePwd.checkPwd.invalid}", groups = LengthCheckGroup.class)
    @Override
    public String getCheckPwd() {
        return super.getCheckPwd();
    }

    public ResetPasswordRedirectType getRedirectType()
    {
        return redirectType;
    }

    public void setRedirectType(final ResetPasswordRedirectType redirectType)
    {
        this.redirectType = redirectType;
    }
}
