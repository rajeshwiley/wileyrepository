/**
 *
 */
package com.wiley.welags.storefrontcommons.controllers.pages;



import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractLoginPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.LoginForm;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.validation.Validator;

import com.wiley.welags.storefrontcommons.forms.WileyRegisterForm;


public abstract class AbstractWileyLoginPageController extends AbstractLoginPageController
{

	protected static final String LOGIN_FORM = "loginForm";

	@Resource(name = "loginValidator")
	protected Validator loginValidator;

	protected String getDefaultLoginPage(final boolean loginError, final boolean isDisabledUser, final HttpSession session,
			final Model model)
			throws CMSItemNotFoundException
	{
		final String view = super.getDefaultLoginPage(loginError, session, model);
		if (isDisabledUser)
		{
			model.addAttribute("loginError", true);
			GlobalMessages.addErrorMessage(model, "login.disabled");
		}
		else if (loginError)
		{
			final Map<String, Object> modelMap = model.asMap();
			final LoginForm loginForm = (LoginForm) modelMap.get(LOGIN_FORM);
			final DataBinder binder = new DataBinder(loginForm);
			binder.setValidator(loginValidator);
			binder.validate();
			final BindingResult errors = binder.getBindingResult();
			model.addAttribute(BindingResult.MODEL_KEY_PREFIX + LOGIN_FORM, errors);

		}
		model.addAttribute("wileyRegisterForm", new WileyRegisterForm());
		return view;
	}
}