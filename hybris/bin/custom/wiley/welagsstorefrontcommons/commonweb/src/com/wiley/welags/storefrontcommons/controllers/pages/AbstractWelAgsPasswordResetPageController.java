package com.wiley.welags.storefrontcommons.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.ForgottenPwdForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.UpdatePwdForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.RegistrationValidator;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commerceservices.security.SecureToken;
import de.hybris.platform.commerceservices.security.SecureTokenService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.core.customer.ResetPasswordRedirectType;
import com.wiley.facades.welags.customer.WelAgsCustomerFacade;
import com.wiley.welags.storefrontcommons.constants.WelagsstorefrontcommonsConstants;
import com.wiley.welags.storefrontcommons.forms.WileyUpdatePwdForm;
import com.wiley.welags.storefrontcommons.forms.validation.groups.WileyUpdatePwdFormValidationOrder;


public abstract class AbstractWelAgsPasswordResetPageController extends AbstractPageController
{
	protected static final String REDIRECT_PWD_REQ_CONF = "redirect:/login/pw/request/external/conf";
	protected static final String REDIRECT_LOGIN = "redirect:/login";
	protected static final String REDIRECT_CHECKOUT_LOGIN = "redirect:/login/checkout";
	protected static final String REDIRECT_PIN_LOGIN = "redirect:/pin/login";
	protected static final String REDIRECT_HOME = "redirect:/";
	protected static final String UPDATE_PWD_CMS_PAGE = "updatePassword";
	protected static final String FORGOT_PASSWORD_CMS_PAGE_LABEL = "forgot-password";
	protected static final String RESET_PASSWORD_EMAIL = "email";
	protected static final String RESET_PASSWORD_CMS_PAGE_LABEL = "reset-password";

	/**
	 * Should be the same as default value of ResetPasswordRedirectType
	 */
	protected static final String DEFAULT_REDIRECT_TYPE = "lp";
	protected static final String FORGOTTEN_PWD_SUCCESS = "forgottenPwd.success";
	protected static final String FORM_GLOBAL_ERROR = "form.global.error";
	protected static final String FORGOTTEN_PWD_NOUSER = "forgottenPwd.nouser";
	protected static final String EMAIL = "email";
	protected static final String FORGOTTEN_PWD_EMAIL_INVALID = "forgottenPwd.email.invalid";
	private static final String FORM_NAME_KEY = "formName";
	private static final String FORM_NAME_VALUE = "forgottenPwdForm";


	@Resource
	private WelAgsCustomerFacade customerFacade;

	@Resource(name = "simpleBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder resourceBreadcrumbBuilder;

	@Resource(name = "secureTokenService")
	private SecureTokenService secureTokenService;

	@Resource(name = "registrationValidator")
	private RegistrationValidator registrationValidator;

	/**
	 * Gets password request.
	 *
	 * @param model
	 * 		the model
	 * @return the password request
	 * @throws CMSItemNotFoundException
	 * 		the cms item not found exception
	 */
	@RequestMapping(value = "/request", method = RequestMethod.GET)
	public String getPasswordRequest(final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute(new ForgottenPwdForm());
		model.addAttribute(FORM_NAME_KEY, FORM_NAME_VALUE);
		storeCmsPageInModel(model, getContentPageForLabelOrId(FORGOT_PASSWORD_CMS_PAGE_LABEL));
		return WelagsstorefrontcommonsConstants.Views.Pages.Password.FORGOT_PASSWORD_PAGE;
	}

	/**
	 * Password request string.
	 *
	 * @param form
	 * 		the form
	 * @param bindingResult
	 * 		the binding result
	 * @param model
	 * 		the model
	 * @param redirectType
	 * 		- redirect type of password reset request
	 * @return the string
	 * @throws CMSItemNotFoundException
	 * 		the cms item not found exception
	 */
	@RequestMapping(value = "/request", method = RequestMethod.POST)
	public String passwordRequest(final ForgottenPwdForm form, final BindingResult bindingResult, final Model model,
			@RequestParam(defaultValue = DEFAULT_REDIRECT_TYPE, value = "t") final ResetPasswordRedirectType redirectType)
			throws CMSItemNotFoundException
	{
		validateEmail(form, bindingResult);
		if (bindingResult.hasErrors())
		{
			prepareErrorMessage(model, FORGOT_PASSWORD_CMS_PAGE_LABEL);
			model.addAttribute(FORM_NAME_KEY, FORM_NAME_VALUE);
			return WelagsstorefrontcommonsConstants.Views.Pages.Password.FORGOT_PASSWORD_PAGE;
		}
		else
		{
			storeCmsPageInModel(model, getContentPageForLabelOrId(FORGOT_PASSWORD_CMS_PAGE_LABEL));
			try
			{
				customerFacade.forgottenPassword(form.getEmail(), redirectType);
				GlobalMessages.addConfMessage(model, FORGOTTEN_PWD_SUCCESS);
			}
			catch (final UnknownIdentifierException unknownIdentifierException)
			{
				getLogger().warn("Email: " + form.getEmail() + " does not exist in the database.");
				GlobalMessages.addErrorMessage(model, FORM_GLOBAL_ERROR);
				bindingResult.rejectValue(EMAIL, FORGOTTEN_PWD_NOUSER);

			}
			return WelagsstorefrontcommonsConstants.Views.Pages.Password.FORGOT_PASSWORD_PAGE;
		}
	}

	/**
	 * Gets change password.
	 *
	 * @param token
	 * 		the token
	 * @param model
	 * 		the model
	 * @return the change password
	 * @throws CMSItemNotFoundException
	 * 		the cms item not found exception
	 */
	@RequestMapping(value = "/change", method = RequestMethod.GET)
	public String getChangePassword(@RequestParam(required = false) final String token,
			@RequestParam(required = false, defaultValue = DEFAULT_REDIRECT_TYPE, value = "t")
			final ResetPasswordRedirectType redirectType,
			final Model model)
			throws CMSItemNotFoundException
	{
		if (StringUtils.isBlank(token))
		{
			return REDIRECT_HOME;
		}
		final UpdatePwdForm form = new WileyUpdatePwdForm();
		form.setToken(token);
		model.addAttribute(form);
		storeCmsPageInModel(model, getContentPageForLabelOrId(RESET_PASSWORD_CMS_PAGE_LABEL));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(UPDATE_PWD_CMS_PAGE));
		model.addAttribute(WebConstants.BREADCRUMBS_KEY, resourceBreadcrumbBuilder.getBreadcrumbs("updatePwd.title"));
		storeEmailInModel(token, model);
		model.addAttribute("redirectType", redirectType);
		return WelagsstorefrontcommonsConstants.Views.Pages.Password.PASSWORD_RESET_CHANGE_PAGE;
	}

	/**
	 * Change password string.
	 *
	 * @param form
	 * 		the form
	 * @param bindingResult
	 * 		the binding result
	 * @param model
	 * 		the model
	 * @param redirectModel
	 * 		the redirect model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 * 		the cms item not found exception
	 */
	public abstract String changePassword(@Validated(value = { WileyUpdatePwdFormValidationOrder.class }) WileyUpdatePwdForm form,
			BindingResult bindingResult, Model model, RedirectAttributes redirectModel) throws CMSItemNotFoundException;

	/**
	 * Prepares the view to display an error message
	 *
	 * @param model
	 * 		the model
	 * @param page
	 * 		the page
	 * @throws CMSItemNotFoundException
	 * 		the cms item not found exception
	 */
	protected void prepareErrorMessage(final Model model, final String page) throws CMSItemNotFoundException
	{
		GlobalMessages.addErrorMessage(model, "form.global.error");
		storeCmsPageInModel(model, getContentPageForLabelOrId(page));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(page));
	}

	/**
	 * @return the registrationValidator
	 */
	protected RegistrationValidator getRegistrationValidator()
	{
		return registrationValidator;
	}


	protected void storeEmailInModel(final String token, final Model model)
	{
		model.addAttribute(RESET_PASSWORD_EMAIL, getEmailForToken(token));
	}

	protected String getEmailForToken(final String token)
	{
		final SecureToken data = secureTokenService.decryptData(token);
		return data.getData();
	}

	protected void validatePwdAndCheckPwdEqual(final UpdatePwdForm form, final BindingResult bindingResult)
	{
		if (!bindingResult.hasErrors() && !form.getPwd().equals(form.getCheckPwd()))
		{
			bindingResult.rejectValue("checkPwd", "validation.checkPwd.equals", new Object[] {},
					"validation.checkPwd.equals");
		}
	}

	protected void validateEmail(final ForgottenPwdForm form, final BindingResult bindingResult)
	{
		if (!getRegistrationValidator().validateEmailAddress(form.getEmail()))
		{
			bindingResult.rejectValue(EMAIL, FORGOTTEN_PWD_EMAIL_INVALID);
		}
	}

	protected abstract Logger getLogger();
}
