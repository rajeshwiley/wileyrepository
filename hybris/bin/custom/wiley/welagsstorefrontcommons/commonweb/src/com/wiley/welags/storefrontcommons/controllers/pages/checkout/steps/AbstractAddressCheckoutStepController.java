package com.wiley.welags.storefrontcommons.controllers.pages.checkout.steps;

import com.wiley.facades.welags.order.WelAgsCheckoutFacade;
import com.wiley.welags.storefrontcommons.controllers.ControllerConstants;
import com.wiley.welags.storefrontcommons.forms.WelAgsAddressForm;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.CheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.forms.AddressForm;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.user.data.AddressData;

import javax.annotation.Nonnull;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.validation.SmartValidator;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


public abstract class AbstractAddressCheckoutStepController extends AbstractWelAgsCheckoutStepController
{
	protected static final String BILLING_ADDRESS_STEP = "billing-address";
	protected static final String FORM_GLOBAL_ERROR = "form.global.error";

	@Resource(name = "wileyAddressValidator")
	private SmartValidator wileyAddressValidator;

	@Resource(name = "checkoutFacade")
	private WelAgsCheckoutFacade wileyCheckoutFacade;

	@RequestMapping(value = "/updateTax", method = RequestMethod.GET)
	@RequireHardLogIn
	public String updateTaxInCart(@RequestParam("countryIsocode") final String countryIsocode,
			@RequestParam(value = "regionIsocode", required = false) final String regionIsocode,
			@RequestParam(value = "isShippingSameAsBilling", required = false) final boolean isShippingSameAsBilling,
			final Model model, final HttpServletResponse response)
	{
		wileyCheckoutFacade.updateTaxInCart(countryIsocode, regionIsocode, isShippingSameAsBilling);
		getCheckoutFacade().prepareCartForCheckout();
		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		model.addAttribute("cartData", cartData);
		model.addAttribute("displayDeliveryCost", customerHasDefaultShippingAddress());

		response.setHeader("regionIsocode", regionIsocode);
		return ControllerConstants.Views.Fragments.Checkout.UPDATE_TAX_FORM;
	}


	protected AddressData convertToAddressData(final AddressForm addressForm, boolean isBilling, boolean isShipping)
	{
		final AddressData newAddress = new AddressData();
		if (addressForm != null)
		{
			newAddress.setId(addressForm.getAddressId());
			newAddress.setFirstName(addressForm.getFirstName());
			newAddress.setLastName(addressForm.getLastName());
			newAddress.setLine1(addressForm.getLine1());
			newAddress.setLine2(addressForm.getLine2());
			newAddress.setTown(addressForm.getTownCity());
			newAddress.setPostalCode(addressForm.getPostcode());
			newAddress.setPhone(addressForm.getPhone());
			newAddress.setBillingAddress(isBilling);
			newAddress.setShippingAddress(isShipping);
			newAddress.setDefaultAddress(true);
			newAddress.setVisibleInAddressBook(true);
			if (StringUtils.isNotEmpty(addressForm.getCountryIso()))
			{
				newAddress.setCountry(getI18NFacade().getCountryForIsocode(addressForm.getCountryIso()));
			}
			if (StringUtils.isNotEmpty(addressForm.getRegionIso()))
			{
				newAddress.setRegion(getI18NFacade().getRegion(addressForm.getCountryIso(), addressForm.getRegionIso()));
			}
		}
		return newAddress;
	}

	protected AddressForm copyAddressDataToAddressForm(final AddressData addressData, final AddressForm addressForm)
	{
		AddressForm resultAddressForm = addressForm != null ? addressForm : new AddressForm();
		if (addressData != null)
		{
			resultAddressForm.setAddressId(addressData.getId());
			resultAddressForm.setFirstName(addressData.getFirstName());
			resultAddressForm.setLastName(addressData.getLastName());

			if (addressData.getCountry() != null)
			{
				resultAddressForm.setCountryIso(addressData.getCountry().getIsocode());
			}
			resultAddressForm.setLine1(addressData.getLine1());
			resultAddressForm.setLine2(addressData.getLine2());
			resultAddressForm.setTownCity(addressData.getTown());

			if (addressData.getRegion() != null)
			{
				resultAddressForm.setRegionIso(addressData.getRegion().getIsocode());
			}

			resultAddressForm.setPostcode(addressData.getPostalCode());
			resultAddressForm.setPhone(addressData.getPhone());
		}
		return resultAddressForm;
	}

	protected boolean customerHasDefaultBillingAddress()
	{
		return getCustomerFacade().getCurrentCustomer() != null
				&& getCustomerFacade().getCurrentCustomer().getDefaultBillingAddress() != null;
	}

	protected boolean customerHasDefaultShippingAddress()
	{
		return getCustomerFacade().getCurrentCustomer() != null
				&& getCustomerFacade().getCurrentCustomer().getDefaultShippingAddress() != null;
	}

	protected boolean isZeroOrder()
	{
		return !wileyCheckoutFacade.isNonZeroPriceCart();
	}

	protected void subscribeToUpdates(@Nonnull final WelAgsAddressForm addressForm)
	{
		Assert.notNull(addressForm, "Parameter addressForm cannot be null.");

		final Boolean subscribeToUpdates = addressForm.getSubscribeToUpdates();
		if (Boolean.TRUE.equals(subscribeToUpdates))
		{
			getWileyCheckoutFacade().subscribeCurrentCustomerToUpdates();
		}
		else if (Boolean.FALSE.equals(subscribeToUpdates))
		{
			getWileyCheckoutFacade().unsubscribeCurrentCustomerFromUpdates();
		}
	}

	protected final SmartValidator getWileyAddressValidator()
	{
		return wileyAddressValidator;
	}

	public WelAgsCheckoutFacade getWileyCheckoutFacade()
	{
		return wileyCheckoutFacade;
	}

	@Override
	public String back(final RedirectAttributes redirectAttributes)
	{
		return null;
	}

	@RequestMapping(value = "/next", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String next(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().nextStep();
	}

	protected CheckoutStep getCheckoutStep()
	{
		return getCheckoutStep(BILLING_ADDRESS_STEP);
	}

}
