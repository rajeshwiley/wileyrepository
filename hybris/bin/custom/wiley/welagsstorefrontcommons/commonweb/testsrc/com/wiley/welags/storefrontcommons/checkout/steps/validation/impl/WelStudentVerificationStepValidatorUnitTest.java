package com.wiley.welags.storefrontcommons.checkout.steps.validation.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorfacades.flow.CheckoutFlowFacade;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.facades.welags.order.WelAgsCheckoutFacade;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;


/**
 * Created by Uladzimir_Barouski on 3/2/2016.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WelStudentVerificationStepValidatorUnitTest
{

	@Mock
	private WelAgsCheckoutFacade checkoutFacadeMock;

	@Mock
	private CheckoutFlowFacade checkoutFlowFacadeMock;

	@Mock
	RedirectAttributes redirectAttributesMock;

	@InjectMocks
	private WelStudentVerificationStepValidator validator;

	@Test
	public void shouldDetectInvalidCartOnEnter()
	{
		when(checkoutFlowFacadeMock.hasValidCart()).thenReturn(false);

		assertEquals(ValidationResults.REDIRECT_TO_CART, validator.validateOnEnter(redirectAttributesMock));
	}

	@Test
	public void shouldRedirectToPaymentIfHasUniversity()
	{
		when(checkoutFlowFacadeMock.hasValidCart()).thenReturn(true);
		when(checkoutFacadeMock.isCartHasStudentVerification()).thenReturn(true);

		assertEquals(ValidationResults.REDIRECT_TO_PAYMENT_METHOD, validator.validateOnEnter(redirectAttributesMock));
	}

	@Test
	public void shouldRedirectOnDefaultUrlOnEnter()
	{
		when(checkoutFlowFacadeMock.hasValidCart()).thenReturn(true);
		when(checkoutFacadeMock.isCartHasStudentVerification()).thenReturn(false);
		when(checkoutFacadeMock.isStudentFlow()).thenReturn(true);

		assertEquals(ValidationResults.SUCCESS, validator.validateOnEnter(redirectAttributesMock));
	}

	@Test
	public void shouldRedirectOnPaymentUrlIfNotStudentFlowOnEnter()
	{
		when(checkoutFlowFacadeMock.hasValidCart()).thenReturn(true);
		when(checkoutFacadeMock.isStudentFlow()).thenReturn(false);

		assertEquals(ValidationResults.REDIRECT_TO_PAYMENT_METHOD, validator.validateOnEnter(redirectAttributesMock));
	}

	@Test
	public void shouldRedirectOnPaymentUrlIfHasStudentVerificationOnEnter()
	{
		when(checkoutFlowFacadeMock.hasValidCart()).thenReturn(true);
		when(checkoutFacadeMock.isCartHasStudentVerification()).thenReturn(true);

		assertEquals(ValidationResults.REDIRECT_TO_PAYMENT_METHOD, validator.validateOnEnter(redirectAttributesMock));
	}
}