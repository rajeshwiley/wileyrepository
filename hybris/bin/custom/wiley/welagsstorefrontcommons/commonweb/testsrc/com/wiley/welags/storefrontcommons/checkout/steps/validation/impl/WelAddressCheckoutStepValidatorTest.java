package com.wiley.welags.storefrontcommons.checkout.steps.validation.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorfacades.flow.CheckoutFlowFacade;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.facades.welags.order.WelAgsCheckoutFacade;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WelAddressCheckoutStepValidatorTest
{
	@Mock
	private WelAgsCheckoutFacade checkoutFacadeMock;

	@Mock
	private CheckoutFlowFacade checkoutFlowFacadeMock;

	@Mock
	RedirectAttributes redirectAttributesMock;

	@InjectMocks
	private WelAddressCheckoutStepValidator validator;

	@Test
	public void shouldDetectInvalidCartOnEnter()
	{
		when(checkoutFlowFacadeMock.hasValidCart()).thenReturn(false);

		assertEquals(ValidationResults.REDIRECT_TO_CART, validator.validateOnEnter(redirectAttributesMock));
	}

	@Test
	public void shouldRedirectOnDefaultUrlOnEnter()
	{
		when(checkoutFlowFacadeMock.hasValidCart()).thenReturn(true);
		when(checkoutFacadeMock.isNonZeroPriceCart()).thenReturn(true);
		when(checkoutFacadeMock.isDigitalSessionCart()).thenReturn(false);

		assertEquals(ValidationResults.SUCCESS, validator.validateOnEnter(redirectAttributesMock));
	}

	@Test
	public void shouldRedirectToSummaryOnEnter()
	{
		when(checkoutFlowFacadeMock.hasValidCart()).thenReturn(true);
		when(checkoutFacadeMock.isNonZeroPriceCart()).thenReturn(false);
		when(checkoutFacadeMock.isDigitalSessionCart()).thenReturn(true);

		assertEquals(ValidationResults.REDIRECT_TO_SUMMARY, validator.validateOnEnter(redirectAttributesMock));
	}

	@Test
	public void shouldRedirectToBullingOnlyOnEnter()
	{
		when(checkoutFlowFacadeMock.hasValidCart()).thenReturn(true);
		when(checkoutFacadeMock.isNonZeroPriceCart()).thenReturn(true);
		when(checkoutFacadeMock.isDigitalSessionCart()).thenReturn(true);

		// for WEL delivery address is mapped to BILLING_ONLY_page
		assertEquals(ValidationResults.REDIRECT_TO_DELIVERY_ADDRESS, validator.validateOnEnter(redirectAttributesMock));
	}

	@Test
	public void shouldRedirectToStudentVerification()
	{
		when(checkoutFlowFacadeMock.hasValidCart()).thenReturn(true);
		when(checkoutFacadeMock.isStudentFlow()).thenReturn(true);

		assertEquals(ValidationResults.SUCCESS, validator.validateOnExit());
	}


}
