package com.wiley.welags.storefrontcommons.checkout.steps.validation.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.facades.welags.order.WelAgsCheckoutFacade;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WelEnterCheckoutStepValidatorTest
{
	@Mock
	private WelAgsCheckoutFacade welAgsCheckoutFacadeMock;
	@InjectMocks
	private WelEnterCheckoutStepValidator testInstance = new WelEnterCheckoutStepValidator();

	@Test
	public void testValidateOnExitShouldReturnRedirectToSummaryIfDigitalCartAndZeroOrder() throws Exception
	{
		//Given
		givenSessionCartIsGidital(true);
		givenIsZeroDollarCart(true);
		//Testing
		final ValidationResults result = testInstance.validateOnExit();
		//Verify
		assertTrue(result == ValidationResults.REDIRECT_TO_SUMMARY);
	}

	private void givenSessionCartIsGidital(boolean isDigital) {
		when(welAgsCheckoutFacadeMock.isDigitalSessionCart()).thenReturn(isDigital);
	}

	private void givenIsZeroDollarCart(boolean isZeroDollarCart) {
		when(welAgsCheckoutFacadeMock.isNonZeroPriceCart()).thenReturn(!isZeroDollarCart);
	}
}