package com.wiley.welags.storefrontcommons.controllers.pages.account;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorfacades.payment.data.PaymentSubscriptionResultData;
import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.user.UserFacade;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.facades.payment.WileyPaymentFacade;
import com.wiley.storefrontcommons.util.HttpServletRequestUtil;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class UpdateCardDetailsResponseControllerTest
{
	private static final String OPERATION_KEY = "operation";
	private static final String OPERATION_VALUE = "operationValue";
	private static final String REDIRECT_TO_PAYMENT_INFO_PAGE = "redirect:/my-account/payment-details";
	private static final String SUBSCRIPTION_ID = "subscriptionId";

	@InjectMocks
	private UpdateCardDetailsResponseController testInstance = new UpdateCardDetailsResponseController();

	@Mock
	private Model mockModel;
	@Mock
	private RedirectAttributes mockRedirectAttributes;
	@Mock
	private WileyPaymentFacade mockWileyPaymentFacade;
	@Mock
	private SiteConfigService mockSiteConfigService;
	@Mock
	private CMSSiteService mockCmsSiteService;
	@Mock
	private CMSPageService mockCmsPageService;
	@Mock
	private HttpServletRequest mockRequest;
	@Mock
	private HttpServletRequestUtil mockHttpServletRequestUtil;
	@Mock
	private UserFacade mockUserFacade;

	private Map<String, String> requestParamMap = new HashMap<>();
	private PaymentSubscriptionResultData paymentSubscriptionResultData = new PaymentSubscriptionResultData();
	private CCPaymentInfoData storedCard = new CCPaymentInfoData();

	@Before
	public void setUp()
	{
		requestParamMap.put(OPERATION_KEY, OPERATION_VALUE);
		when(mockHttpServletRequestUtil.getRequestParameterMap(mockRequest)).thenReturn(requestParamMap);
		testInstance.setHttpServletRequestUtil(mockHttpServletRequestUtil);

		storedCard.setSubscriptionId(SUBSCRIPTION_ID);
		paymentSubscriptionResultData.setStoredCard(storedCard);
		paymentSubscriptionResultData.setSuccess(true);
		when(mockWileyPaymentFacade.completeHopValidatePayment(requestParamMap, true)).thenReturn(paymentSubscriptionResultData);
	}

	@Test
	public void shouldReturnPaymentInfoPageIfOperationIsSuccessful()
	{
		final String actualResult = testInstance.doHandleResponse(mockRedirectAttributes, mockRequest);
		assertEquals(REDIRECT_TO_PAYMENT_INFO_PAGE, actualResult);
	}

	@Test
	public void shouldReturnPaymentInfoPageIfOperationIsNotSuccessful()
	{
		paymentSubscriptionResultData.setSuccess(false);
		final String actualResult = testInstance.doHandleResponse(mockRedirectAttributes, mockRequest);
		assertEquals(REDIRECT_TO_PAYMENT_INFO_PAGE, actualResult);
	}

	@Test
	public void shouldUpdatePaymentInfoIfOperationIsSuccessful()
	{
		testInstance.doHandleResponse(mockRedirectAttributes, mockRequest);
		verify(mockUserFacade).setDefaultPaymentInfo(storedCard);
	}

	@Test
	public void shouldNotUpdatePaymentInfoIfOperationNotSuccessful()
	{
		paymentSubscriptionResultData.setSuccess(false);
		testInstance.doHandleResponse(mockRedirectAttributes, mockRequest);
		verify(mockUserFacade, never()).setDefaultPaymentInfo(storedCard);
	}

	@Test
	public void shouldNotUpdatePaymentInfoIfNoStoredCard()
	{
		paymentSubscriptionResultData.setStoredCard(null);
		testInstance.doHandleResponse(mockRedirectAttributes, mockRequest);
		verify(mockUserFacade, never()).setDefaultPaymentInfo(storedCard);
	}

	@Test
	public void shouldNotUpdatePaymentInfoIfCardSubscriptionIdIsNull()
	{
		storedCard.setSubscriptionId(null);
		testInstance.doHandleResponse(mockRedirectAttributes, mockRequest);
		verify(mockUserFacade, never()).setDefaultPaymentInfo(storedCard);
	}


}
