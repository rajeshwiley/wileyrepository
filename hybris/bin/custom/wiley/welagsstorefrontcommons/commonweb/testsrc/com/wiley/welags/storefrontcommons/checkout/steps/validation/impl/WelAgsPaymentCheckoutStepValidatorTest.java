package com.wiley.welags.storefrontcommons.checkout.steps.validation.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorfacades.flow.CheckoutFlowFacade;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.facades.welags.order.WelAgsCheckoutFacade;


/**
 * Created by Georgii_Gavrysh on 5/23/2016.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WelAgsPaymentCheckoutStepValidatorTest
{
	@Mock
	private WelAgsCheckoutFacade welAgsCheckoutFacadeMock;

	@Mock
	private CheckoutFlowFacade checkoutFlowFacadeMock;

	@InjectMocks
	private WelAgsPaymentCheckoutStepValidator validator;

	@Mock
	RedirectAttributes redirectAttributesMock;

	@Test
	public void shouldDetectInvalidCartOnEnter()
	{
		when(checkoutFlowFacadeMock.hasValidCart()).thenReturn(false);

		assertEquals(ValidationResults.REDIRECT_TO_CART, validator.validateOnEnter(redirectAttributesMock));
	}

	@Test
	public void shouldRedirectToSummaryIfZeroPriceCartOnEnter()
	{
		when(checkoutFlowFacadeMock.hasValidCart()).thenReturn(true);
		when(welAgsCheckoutFacadeMock.isNonZeroPriceCart()).thenReturn(false);

		assertEquals(ValidationResults.REDIRECT_TO_SUMMARY, validator.validateOnEnter(redirectAttributesMock));
	}

	@Test
	public void shouldReturnSuccessIfNonZeroPriceCartOnEnter()
	{
		when(checkoutFlowFacadeMock.hasValidCart()).thenReturn(true);
		when(welAgsCheckoutFacadeMock.isNonZeroPriceCart()).thenReturn(true);

		assertEquals(ValidationResults.SUCCESS, validator.validateOnEnter(redirectAttributesMock));
	}
}
