package com.wiley.welags.storefrontcommons.controllers.pages.account;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.acceleratorservices.payment.constants.PaymentConstants.PaymentProperties;
import de.hybris.platform.acceleratorservices.payment.data.PaymentData;
import de.hybris.platform.acceleratorservices.storefront.util.PageTitleResolver;
import de.hybris.platform.cms2.data.PagePreviewCriteriaData;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.cms2.servicelayer.services.CMSPreviewService;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.wiley.facades.payment.WileyPaymentFacade;
import com.wiley.welags.storefrontcommons.controllers.ControllerConstants;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class UpdateCardDetailsControllerTest
{
	private static final boolean HOP_DEBUG_MODE = true;
	private static final String RESPONSE_URL = "my-account/update-card-details/hop/response";
	private static final String MERCHANT_CALLBACK_URL = "/integration/merchant_callback";
	private static final String PAYMENT_INFO_PAGE = "redirect:/my-account/payment-details";


	@InjectMocks
	private UpdateCardDetailsController testInstance = new UpdateCardDetailsController();

	@Mock
	private Model mockModel;
	@Mock
	private RedirectAttributes mockRedirectAttributes;
	@Mock
	private WileyPaymentFacade mockWileyPaymentFacade;
	@Mock
	private SiteConfigService mockSiteConfigService;
	@Mock
	private CMSSiteService mockCmsSiteService;
	@Mock
	private CMSPageService mockCmsPageService;
	@Mock
	private CMSPreviewService cmsPreviewService;
	@Mock
	private PageTitleResolver pageTitleResolver;
	@Mock
	private ContentPageModel contentPageModel;

	private PaymentData paymentData;

	@Before
	public void setUp()
	{
		paymentData = new PaymentData();
		when(mockSiteConfigService.getBoolean(PaymentProperties.HOP_DEBUG_MODE, false)).thenReturn(HOP_DEBUG_MODE);
	}

	@Test
	public void shouldPopulateModelAndReturnHopIfThereAreNoErrors() throws CMSItemNotFoundException
	{
		given(mockCmsPageService.getPageForLabelOrId(anyString(), any(PagePreviewCriteriaData.class))).willReturn(
				contentPageModel);
		when(mockWileyPaymentFacade.beginHopValidatePayment(RESPONSE_URL, MERCHANT_CALLBACK_URL)).thenReturn(paymentData);
		String actualResult = testInstance.updateCardDetails(mockModel, mockRedirectAttributes);
		verify(mockModel).addAttribute("hostedOrderPageData", paymentData);
		verify(mockModel).addAttribute("hopDebugMode", HOP_DEBUG_MODE);
		assertEquals(ControllerConstants.Views.Pages.MultiStepCheckout.HOSTED_ORDER_POST_PAGE, actualResult);
	}

	@Test
	public void shouldReturnPaymentInfoPageIfThereIsAnError()
	{
		when(mockWileyPaymentFacade.beginHopValidatePayment(RESPONSE_URL, MERCHANT_CALLBACK_URL)).thenThrow(Exception.class);
		String actualResult = testInstance.updateCardDetails(mockModel, mockRedirectAttributes);
		assertEquals(PAYMENT_INFO_PAGE, actualResult);
	}
}
