
package com.wiley.welags.storefrontcommons.validators;

/**
 * @author Nitin_Gakhar
 * Common Pin activation validator for WEL/AGS sites
 *
 */

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.wiley.welags.storefrontcommons.form.PinActivationForm;


public class WelAgsPinActivationValidator implements Validator
{

	private static final int MIN_PIN_FIELD_LENGTH = 1;
	private static final int MAX_PIN_FIELD_LENGTH = 25;

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return WelAgsPinActivationValidator.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final PinActivationForm pinActivationForm = (PinActivationForm) object;
		validatePin(pinActivationForm, errors);

	}

	private void validatePin(final PinActivationForm pinActivationForm, final Errors errors)
	{
		final String pin = pinActivationForm.getPin();

		if (StringUtils.isBlank(pin))
		{
			errors.rejectValue("pin", "pinActivation.checkPin.invalid");
		}

		else if (StringUtils.length(pin) < MIN_PIN_FIELD_LENGTH || StringUtils.length(pin) > MAX_PIN_FIELD_LENGTH)
		{
			errors.rejectValue("pin", "pinActivation.checkPin.invalid");
		}

	}
}
