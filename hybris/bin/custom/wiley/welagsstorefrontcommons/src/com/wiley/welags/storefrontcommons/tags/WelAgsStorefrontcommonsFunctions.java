package com.wiley.welags.storefrontcommons.tags;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.subscriptionfacades.data.OrderEntryPriceData;
import de.hybris.platform.subscriptionfacades.data.OrderPriceData;


/**
 * Created by maksim on 10/27/15.
 *
 * Functions used in TLD.
 */
public final class WelAgsStorefrontcommonsFunctions
{
	private WelAgsStorefrontcommonsFunctions()
	{
		// private to avoid instantiating this constant class
	}

	public static OrderEntryPriceData getEntryForBillingEvent(final OrderEntryData entryData, final String billingCode)
	{
		OrderEntryPriceData result = null;
		if (entryData != null && billingCode != null && entryData.getOrderEntryPrices() != null)
		{
			for (OrderEntryPriceData entryPrice : entryData.getOrderEntryPrices())
			{
				if (entryPrice.getBillingTime() != null && billingCode.equals(entryPrice.getBillingTime().getCode()))
				{
					result = entryPrice;
					break;
				}
			}
		}
		return result;
	}

	public static OrderPriceData getOrderForBillingEvent(final AbstractOrderData orderData, final String billingCode)
	{
		OrderPriceData result = null;
		if (orderData != null && billingCode != null && orderData.getOrderPrices() != null)
		{
			for (OrderPriceData orderPrice : orderData.getOrderPrices())
			{
				if (orderPrice.getBillingTime() != null && billingCode.equals(orderPrice.getBillingTime().getCode()))
				{
					result = orderPrice;
					break;
				}
			}
		}
		return result;
	}
}
