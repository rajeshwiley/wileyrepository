package com.wiley.welags.storefrontcommons.form;

import java.io.Serializable;

/**
 * Form object for PIN activation.
 */
public class PinActivationForm implements Serializable
{
	private String pin;

	/**
	 * @return the pin
	 */
	public String getPin()
	{
		return pin;
	}

	/**
	 * @param pin
	 *           the pin to set
	 */
	public void setPin(final String pin)
	{
		this.pin = pin;
	}
}
