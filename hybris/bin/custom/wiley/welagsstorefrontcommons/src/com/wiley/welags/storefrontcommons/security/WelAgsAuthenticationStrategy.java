package com.wiley.welags.storefrontcommons.security;

import org.springframework.security.core.Authentication;


public interface WelAgsAuthenticationStrategy
{
	/**
	 * Update authentication spring 
	 */
	void refreshSpringSecurityToken();

	/**
	 * Add authorities to asm agent if this mode is active,
	 * or set defined authentication to spring security context.
	 *
	 * @param authentication
	 */
	void setSpringSecurityToken(Authentication authentication);
}
