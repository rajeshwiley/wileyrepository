/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.welags.storefrontcommons.constants;

import de.hybris.platform.acceleratorcms.model.components.CartSuggestionComponentModel;
import de.hybris.platform.acceleratorcms.model.components.CategoryFeatureComponentModel;
import de.hybris.platform.acceleratorcms.model.components.DynamicBannerComponentModel;
import de.hybris.platform.acceleratorcms.model.components.MiniCartComponentModel;
import de.hybris.platform.acceleratorcms.model.components.NavigationBarComponentModel;
import de.hybris.platform.acceleratorcms.model.components.ProductFeatureComponentModel;
import de.hybris.platform.acceleratorcms.model.components.ProductReferencesComponentModel;
import de.hybris.platform.acceleratorcms.model.components.PurchasedCategorySuggestionComponentModel;
import de.hybris.platform.acceleratorcms.model.components.SimpleResponsiveBannerComponentModel;
import de.hybris.platform.acceleratorcms.model.components.SubCategoryListComponentModel;
import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;
import de.hybris.platform.cms2lib.model.components.ProductCarouselComponentModel;

import com.wiley.core.model.components.WileyFooterComponentModel;


/**
 * Global class for all Welagsstorefrontcommons constants. You can add global constants for your extension into this class.
 */
public final class WelagsstorefrontcommonsConstants extends GeneratedWelagsstorefrontcommonsConstants
{
	public static final String EXTENSIONNAME = "welagsstorefrontcommons";

	private WelagsstorefrontcommonsConstants()
	{
		//empty to avoid instantiating this constant class
	}

	/**
	 * The type Actions.
	 */
	public static final class Actions
	{
		private Actions()
		{
		}

		/**
		 * The type Cms.
		 */
		public static final class Cms
		{
			/**
			 * The constant PREFIX.
			 */
			public static final String PREFIX = "/view/";
			/**
			 * The constant SUFFIX.
			 */
			public static final String SUFFIX = "Controller";

			/**
			 * Default CMS component controller
			 */
			public static final String DEFAULT_CMS_COMPONENT = PREFIX + "DefaultCMSComponentController";

			/**
			 * CMS components that have specific handlers
			 */
			public static final String PURCHASED_CATEGORY_SUGGESTION_COMPONENT =
					PREFIX + PurchasedCategorySuggestionComponentModel._TYPECODE + SUFFIX;
			/**
			 * The constant CART_SUGGESTION_COMPONENT.
			 */
			public static final String CART_SUGGESTION_COMPONENT = PREFIX + CartSuggestionComponentModel._TYPECODE + SUFFIX;
			/**
			 * The constant PRODUCT_REFERENCES_COMPONENT.
			 */
			public static final String PRODUCT_REFERENCES_COMPONENT = PREFIX + ProductReferencesComponentModel._TYPECODE + SUFFIX;
			/**
			 * The constant PRODUCT_CAROUSEL_COMPONENT.
			 */
			public static final String PRODUCT_CAROUSEL_COMPONENT = PREFIX + ProductCarouselComponentModel._TYPECODE + SUFFIX;
			/**
			 * The constant MINI_CART_COMPONENT.
			 */
			public static final String MINI_CART_COMPONENT = PREFIX + MiniCartComponentModel._TYPECODE + SUFFIX;
			/**
			 * The constant PRODUCT_FEATURE_COMPONENT.
			 */
			public static final String PRODUCT_FEATURE_COMPONENT = PREFIX + ProductFeatureComponentModel._TYPECODE + SUFFIX;
			/**
			 * The constant CATEGORY_FEATURE_COMPONENT.
			 */
			public static final String CATEGORY_FEATURE_COMPONENT = PREFIX + CategoryFeatureComponentModel._TYPECODE + SUFFIX;
			/**
			 * The constant NAVIGATION_BAR_COMPONENT.
			 */
			public static final String NAVIGATION_BAR_COMPONENT = PREFIX + NavigationBarComponentModel._TYPECODE + SUFFIX;
			/**
			 * The constant CMS_LINK_COMPONENT.
			 */
			public static final String CMS_LINK_COMPONENT = PREFIX + CMSLinkComponentModel._TYPECODE + SUFFIX;
			/**
			 * The constant DYNAMIC_BANNER_COMPONENT.
			 */
			public static final String DYNAMIC_BANNER_COMPONENT = PREFIX + DynamicBannerComponentModel._TYPECODE + SUFFIX;
			/**
			 * The constant SUB_CATEGORY_LIST_COMPONENT.
			 */
			public static final String SUB_CATEGORY_LIST_COMPONENT = PREFIX + SubCategoryListComponentModel._TYPECODE + SUFFIX;

			/**
			 * CMS components that have specific handlers
			 */
			public static final String WILEY_FOOTER_COMPONENT = PREFIX + WileyFooterComponentModel._TYPECODE + SUFFIX;
			/**
			 * The constant SIMPLE_RESPONSIVE_BANNER_COMPONENT.
			 */
			public static final String SIMPLE_RESPONSIVE_BANNER_COMPONENT =
					PREFIX + SimpleResponsiveBannerComponentModel._TYPECODE + SUFFIX;

			private Cms()
			{
			}
		}
	}

	/**
	 * The type Views.
	 */
	public static final class Views
	{
		private Views()
		{
		}

		/**
		 * The type Cms.
		 */
		public static final class Cms
		{
			/**
			 * The constant COMPONENT_PREFIX.
			 */
			public static final String COMPONENT_PREFIX = "cms/";

			private Cms()
			{
			}
		}

		/**
		 * The type Pages.
		 */
		public static final class Pages
		{
			private Pages()
			{
			}

			/**
			 * The type Account.
			 */
			public static final class Account
			{
				/**
				 * The constant ACCOUNT_LOGIN_PAGE.
				 */
				public static final String ACCOUNT_LOGIN_PAGE = "pages/account/accountLoginPage";
				/**
				 * The constant ACCOUNT_HOME_PAGE.
				 */
				public static final String ACCOUNT_HOME_PAGE = "pages/account/accountHomePage";
				/**
				 * The constant ACCOUNT_ORDER_HISTORY_PAGE.
				 */
				public static final String ACCOUNT_ORDER_HISTORY_PAGE = "pages/account/accountOrderHistoryPage";
				/**
				 * The constant ACCOUNT_ORDER_PAGE.
				 */
				public static final String ACCOUNT_ORDER_PAGE = "pages/account/accountOrderPage";
				/**
				 * The constant ACCOUNT_PROFILE_PAGE.
				 */
				public static final String ACCOUNT_PROFILE_PAGE = "pages/account/accountProfilePage";
				/**
				 * The constant ACCOUNT_PROFILE_EDIT_PAGE.
				 */
				public static final String ACCOUNT_PROFILE_EDIT_PAGE = "pages/account/accountProfileEditPage";
				/**
				 * The constant ACCOUNT_PROFILE_EMAIL_EDIT_PAGE.
				 */
				public static final String ACCOUNT_PROFILE_EMAIL_EDIT_PAGE = "pages/account/accountProfileEmailEditPage";
				/**
				 * The constant ACCOUNT_CHANGE_PASSWORD_PAGE.
				 */
				public static final String ACCOUNT_CHANGE_PASSWORD_PAGE = "pages/account/accountChangePasswordPage";
				/**
				 * The constant ACCOUNT_ADDRESS_BOOK_PAGE.
				 */
				public static final String ACCOUNT_ADDRESS_BOOK_PAGE = "pages/account/accountAddressBookPage";
				/**
				 * The constant ACCOUNT_EDIT_ADDRESS_PAGE.
				 */
				public static final String ACCOUNT_EDIT_ADDRESS_PAGE = "pages/account/accountEditAddressPage";
				/**
				 * The constant ACCOUNT_PAYMENT_INFO_PAGE.
				 */
				public static final String ACCOUNT_PAYMENT_INFO_PAGE = "pages/account/accountPaymentInfoPage";
				/**
				 * The constant ACCOUNT_REGISTER_PAGE.
				 */
				public static final String ACCOUNT_REGISTER_PAGE = "pages/account/accountRegisterPage";

				private Account()
				{
				}
			}

			/**
			 * The type Checkout.
			 */
			public static final class Checkout
			{
				/**
				 * The constant CHECKOUT_REGISTER_PAGE.
				 */
				public static final String CHECKOUT_REGISTER_PAGE = "pages/checkout/checkoutRegisterPage";
				/**
				 * The constant CHECKOUT_CONFIRMATION_PAGE.
				 */
				public static final String CHECKOUT_CONFIRMATION_PAGE = "pages/checkout/checkoutConfirmationPage";
				/**
				 * The constant CHECKOUT_LOGIN_PAGE.
				 */
				public static final String CHECKOUT_LOGIN_PAGE = "pages/checkout/checkoutLoginPage";

				private Checkout()
				{
				}
			}

			/**
			 * The type MultiStepCheckout.
			 */
			public static final class MultiStepCheckout
			{
				/**
				 * The constant ADD_EDIT_DELIVERY_ADDRESS_PAGE.
				 */
				public static final String ADD_EDIT_DELIVERY_ADDRESS_PAGE = "pages/checkout/multi/addEditDeliveryAddressPage";
				/**
				 * The constant CHOOSE_DELIVERY_METHOD_PAGE.
				 */
				public static final String CHOOSE_DELIVERY_METHOD_PAGE = "pages/checkout/multi/chooseDeliveryMethodPage";
				/**
				 * The constant CHOOSE_PICKUP_LOCATION_PAGE.
				 */
				public static final String CHOOSE_PICKUP_LOCATION_PAGE = "pages/checkout/multi/choosePickupLocationPage";
				/**
				 * The constant ADD_PAYMENT_METHOD_PAGE.
				 */
				public static final String ADD_PAYMENT_METHOD_PAGE = "pages/checkout/multi/addPaymentMethodPage";
				/**
				 * The constant CHECKOUT_SUMMARY_PAGE.
				 */
				public static final String CHECKOUT_SUMMARY_PAGE = "pages/checkout/multi/checkoutSummaryPage";
				/**
				 * The constant HOSTED_ORDER_PAGE_ERROR_PAGE.
				 */
				public static final String HOSTED_ORDER_PAGE_ERROR_PAGE = "pages/checkout/multi/hostedOrderPageErrorPage";
				/**
				 * The constant HOSTED_ORDER_POST_PAGE.
				 */
				public static final String HOSTED_ORDER_POST_PAGE = "pages/checkout/multi/hostedOrderPostPage";
				/**
				 * The constant SILENT_ORDER_POST_PAGE.
				 */
				public static final String SILENT_ORDER_POST_PAGE = "pages/checkout/multi/silentOrderPostPage";
				/**
				 * The constant GIFT_WRAP_PAGE.
				 */
				public static final String GIFT_WRAP_PAGE = "pages/checkout/multi/giftWrapPage";

				private MultiStepCheckout()
				{
				}
			}

			/**
			 * The type Password.
			 */
			public static final class Password
			{
				/**
				 * The constant FORGOT_PASSWORD_PAGE.
				 */
				public static final String FORGOT_PASSWORD_PAGE = "pages/password/forgotPasswordPage";
				/**
				 * The constant PASSWORD_RESET_CHANGE_PAGE.
				 */
				public static final String PASSWORD_RESET_CHANGE_PAGE = "pages/password/passwordResetChangePage";
				/**
				 * The constant PASSWORD_RESET_REQUEST.
				 */
				public static final String PASSWORD_RESET_REQUEST = "pages/password/passwordResetRequestPage";
				/**
				 * The constant PASSWORD_RESET_REQUEST_CONFIRMATION.
				 */
				public static final String PASSWORD_RESET_REQUEST_CONFIRMATION =
						"pages/password/passwordResetRequestConfirmationPage";

				private Password()
				{
				}
			}

			/**
			 * The type Error.
			 */
			public static final class Error
			{
				/**
				 * The constant ERROR_NOT_FOUND_PAGE.
				 */
				public static final String ERROR_NOT_FOUND_PAGE = "pages/error/errorNotFoundPage";

				private Error()
				{
				}
			}

			/**
			 * The type Cart.
			 */
			public static final class Cart
			{
				/**
				 * The constant CART_PAGE.  deprecated within this site
				 */

				public static final String SUBSCRIPTIONCART_PAGE = "pages/subscriptioncart/cartPage";

				private Cart()
				{
				}
			}

			/**
			 * The type Store finder.
			 */
			public static final class StoreFinder
			{
				/**
				 * The constant STORE_FINDER_SEARCH_PAGE.
				 */
				public static final String STORE_FINDER_SEARCH_PAGE = "pages/storeFinder/storeFinderSearchPage";
				/**
				 * The constant STORE_FINDER_DETAILS_PAGE.
				 */
				public static final String STORE_FINDER_DETAILS_PAGE = "pages/storeFinder/storeFinderDetailsPage";
				/**
				 * The constant STORE_FINDER_VIEW_MAP_PAGE.
				 */
				public static final String STORE_FINDER_VIEW_MAP_PAGE = "pages/storeFinder/storeFinderViewMapPage";

				private StoreFinder()
				{
				}
			}

			/**
			 * The type Misc.
			 */
			public static final class Misc
			{
				/**
				 * The constant MISC_ROBOTS_PAGE.
				 */
				public static final String MISC_ROBOTS_PAGE = "pages/misc/miscRobotsPage";
				/**
				 * The constant MISC_SITE_MAP_PAGE.
				 */
				public static final String MISC_SITE_MAP_PAGE = "pages/misc/miscSiteMapPage";

				private Misc()
				{
				}
			}

			/**
			 * The type Guest.
			 */
			public static final class Guest
			{
				/**
				 * The constant GUEST_ORDER_PAGE.
				 */
				public static final String GUEST_ORDER_PAGE = "pages/guest/guestOrderPage";
				/**
				 * The constant GUEST_ORDER_ERROR_PAGE.
				 */
				public static final String GUEST_ORDER_ERROR_PAGE = "pages/guest/guestOrderErrorPage";

				private Guest()
				{
				}
			}

			/**
			 * The type Product.
			 */
			public static final class Product
			{
				/**
				 * The constant WRITE_REVIEW.
				 */
				public static final String WRITE_REVIEW = "pages/product/writeReview";
				/**
				 * The constant WRITE_REVIEW.
				 */
				public static final String ORDER_FORM = "pages/product/productOrderFormPage";

				private Product()
				{
				}
			}

			/*
			 * The type Pin.
			 */
			public static final class PinActivation
			{
				public static final String PIN_LOGIN_PAGE = "pages/pinActivation/pinLoginPage";

				private PinActivation()
				{
				}
			}
		}

		/**
		 * The type Fragments.
		 */
		public static final class Fragments
		{
			private Fragments()
			{
			}

			/**
			 * The type Cart.
			 */
			public static final class Cart
			{
				/**
				 * The constant ADD_TO_CART_POPUP.
				 */
				public static final String ADD_TO_CART_POPUP = "fragments/cart/addToCartPopup";
				/**
				 * The constant MINI_CART_PANEL.
				 */
				public static final String MINI_CART_PANEL = "fragments/cart/miniCartPanel";
				/**
				 * The constant MINI_CART_ERROR_PANEL.
				 */
				public static final String MINI_CART_ERROR_PANEL = "fragments/cart/miniCartErrorPanel";
				/**
				 * The constant CART_POPUP.
				 */
				public static final String CART_POPUP = "fragments/cart/cartPopup";
				/**
				 * The constant EXPAND_GRID_IN_CART.
				 */
				public static final String EXPAND_GRID_IN_CART = "fragments/cart/expandGridInCart";

				private Cart()
				{
				}
			}

			/**
			 * The type Account.
			 */
			public static final class Account
			{
				/**
				 * The constant COUNTRY_ADDRESS_FORM.
				 */
				public static final String COUNTRY_ADDRESS_FORM = "fragments/address/countryAddressForm";

				public static final String ACCOUNT_SUBSCRIPTION = "fragments/account/accountSubscription";

				private Account()
				{
				}
			}

			/**
			 * The type Checkout.
			 */
			public static final class Checkout
			{
				/**
				 * The constant TERMS_AND_CONDITIONS_POPUP.
				 */
				public static final String TERMS_AND_CONDITIONS_POPUP =
						"acceleratoraddon/web/webroot/WEB-INF/views/mobile/fragments/checkout/termsAndConditionsPopup";
				/**
				 * The constant BILLING_ADDRESS_FORM.
				 */
				public static final String BILLING_ADDRESS_FORM =
						"acceleratoraddon/web/webroot/WEB-INF/views/mobile/fragments/checkout/billingAddressForm";
				/**
				 * The constant READ_ONLY_EXPANDED_ORDER_FORM.
				 */
				public static final String READ_ONLY_EXPANDED_ORDER_FORM = "fragments/checkout/readOnlyExpandedOrderForm";

				private Checkout()
				{
				}
			}

			/**
			 * The type Product.
			 */
			public static final class Product
			{
				/**
				 * The constant FUTURE_STOCK_POPUP.
				 */
				public static final String FUTURE_STOCK_POPUP = "fragments/product/futureStockPopup";
				/**
				 * The constant QUICK_VIEW_POPUP.
				 */
				public static final String QUICK_VIEW_POPUP = "fragments/product/quickViewPopup";
				/**
				 * The constant ZOOM_IMAGES_POPUP.
				 */
				public static final String ZOOM_IMAGES_POPUP = "fragments/product/zoomImagesPopup";
				/**
				 * The constant REVIEWS_TAB.
				 */
				public static final String REVIEWS_TAB = "fragments/product/reviewsTab";
				/**
				 * The constant STORE_PICKUP_SEARCH_RESULTS.
				 */
				public static final String STORE_PICKUP_SEARCH_RESULTS = "fragments/product/storePickupSearchResults";

				private Product()
				{
				}
			}
		}
	}

	/**
	 * The type Keys.
	 */
	public static final class Keys
	{
		private Keys()
		{
		}

		public static final String PIN_FIELD_ERROR = "pinFieldError";
		public static final String FORM_GLOBAL_ERROR_MESSAGE = "form.global.error";
	}

}
