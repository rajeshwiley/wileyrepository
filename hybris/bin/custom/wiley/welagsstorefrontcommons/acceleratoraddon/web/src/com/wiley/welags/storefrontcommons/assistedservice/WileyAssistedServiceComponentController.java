package com.wiley.welags.storefrontcommons.assistedservice;

import de.hybris.platform.assistedservicestorefront.controllers.cms.AssistedServiceComponentController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.wiley.welags.storefrontcommons.security.WelAgsAuthenticationStrategy;


/**
 * WileyAssistedServiceComponentController extends AssistedServiceComponentController
 * to avoid redirection to home page.
 */
@Controller
@RequestMapping(value = "/assisted-service")
public class WileyAssistedServiceComponentController extends AssistedServiceComponentController
{
	private static final String ASM_REDIRECT_URL_ATTRIBUTE = "redirect_url";
	private static final String ASM_HOME_URL = "/";

	@Resource
	private WelAgsAuthenticationStrategy welAgsAuthenticationStrategy;

	@Override
	protected void refreshSpringSecurityToken()
	{
		welAgsAuthenticationStrategy.refreshSpringSecurityToken();
	}

	@Override
	public String loginAssistedServiceAgent(final Model model, final HttpServletRequest request,
			final HttpServletResponse response, final String username, final String password)
	{
		final String viewPage = super.loginAssistedServiceAgent(model, request, response, username, password);
		updateRedirectToHome(model);
		return viewPage;
	}

	@Override
	public String bindCart(final String customerId, final String cartId, final Model model)
	{
		final String viewPage = super.bindCart(customerId, cartId, model);
		updateRedirectToHome(model);
		return viewPage;
	}

	@Override
	public String emulateCustomer(final Model model, final String customerId, final String customerName, final String cartId)
	{
		final String viewPage = super.emulateCustomer(model, customerId, customerName, cartId);
		updateRedirectToHome(model);
		return viewPage;
	}

	@Override
	public String logoutAssistedServiceAgent(final Model model, final HttpServletRequest request)
	{
		endEmulateCustomer(model);
		final String viewPage = super.logoutAssistedServiceAgent(model, request);
		updateRedirectToHome(model);
		return viewPage;
	}

	@Override
	public String endEmulateCustomer(final Model model)
	{
		final String viewPage = super.endEmulateCustomer(model);
		updateRedirectToHome(model);
		return viewPage;
	}

	private void updateRedirectToHome(final Model model)
	{
		if (ASM_HOME_URL.equals(model.asMap().get(ASM_REDIRECT_URL_ATTRIBUTE)))
		{
			model.addAttribute(ASM_REDIRECT_URL_ATTRIBUTE, StringUtils.EMPTY);
			model.addAttribute("customerReload", "reload");
		}
	}
}