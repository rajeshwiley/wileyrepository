package com.wiley.welags.storefrontcommons.security.impl;

import de.hybris.platform.assistedservicestorefront.constants.AssistedservicestorefrontConstants;
import de.hybris.platform.assistedservicestorefront.security.AssistedServiceAgentAuthoritiesManager;
import de.hybris.platform.assistedservicestorefront.security.impl.AssistedServiceAuthenticationToken;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Set;

import javax.annotation.Resource;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.wiley.welags.storefrontcommons.security.WelAgsAuthenticationStrategy;


public class WelAgsAsmAuthenticationStrategyImpl implements WelAgsAuthenticationStrategy
{
	@Resource(name = "assistedServiceAgentAuthoritiesManager")
	private AssistedServiceAgentAuthoritiesManager authoritiesManager;

	@Resource(name = "userService")
	private UserService userService;


	@Override
	public void refreshSpringSecurityToken()
	{
		refreshSpringSecurityTokenInternal();
	}

	@Override
	public void setSpringSecurityToken(final Authentication authentication)
	{
		refreshSpringSecurityTokenInternal();
	}

	private void refreshSpringSecurityTokenInternal()
	{
		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication instanceof AssistedServiceAuthenticationToken)
		{
			final UserModel currentUser = userService.getCurrentUser();
			if (currentUser == null || userService.isAnonymousUser(currentUser) || isASAgent(currentUser))
			{
				((AssistedServiceAuthenticationToken) authentication).setEmulating(false);
			}
			else
			{
				((AssistedServiceAuthenticationToken) authentication).setEmulating(true);
				authoritiesManager.addCustomerAuthoritiesToAgent(currentUser.getUid());
			}
		}
	}


	private boolean isASAgent(final UserModel currentUser)
	{
		final Set<UserGroupModel> userGroups = userService.getAllUserGroupsForUser(currentUser);
		for (final UserGroupModel userGroup : userGroups)
		{
			if (AssistedservicestorefrontConstants.AS_AGENT_GROUP_UID.equals(userGroup.getUid()))
			{
				return true;
			}
		}
		return false;
	}

}
