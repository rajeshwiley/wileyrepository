package com.wiley.welags.storefrontcommons.security.impl;

import de.hybris.platform.assistedservicestorefront.security.impl.AssistedServiceAuthenticationToken;

import javax.annotation.Resource;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.wiley.welags.storefrontcommons.security.WelAgsAuthenticationStrategy;



public class WelAgsAuthenticationStrategyImpl implements WelAgsAuthenticationStrategy
{
	@Resource(name = "welAgsAsmAuthenticationStrategy")
	private WelAgsAuthenticationStrategy asmAuthenticationStrategy;

	@Resource(name = "welAgsCustomerAuthenticationStrategy")
	private WelAgsAuthenticationStrategy customerAuthenticationStrategy;

	@Override
	public void refreshSpringSecurityToken()
	{
		getAuthenticationStrategy().refreshSpringSecurityToken();
	}

	@Override
	public void setSpringSecurityToken(final Authentication authentication)
	{
		getAuthenticationStrategy().setSpringSecurityToken(authentication);
	}

	private WelAgsAuthenticationStrategy getAuthenticationStrategy()
	{
		if (isAsmAuthenticationSavedInContext())
		{
			return asmAuthenticationStrategy;
		}
		else
		{
			return customerAuthenticationStrategy;
		}
	}

	private boolean isAsmAuthenticationSavedInContext()
	{
		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		return authentication instanceof AssistedServiceAuthenticationToken;
	}
}
