package com.wiley.welags.storefrontcommons.security.impl;

import de.hybris.platform.commercefacades.customer.CustomerFacade;

import javax.annotation.Resource;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.wiley.welags.storefrontcommons.security.WelAgsAuthenticationStrategy;


public class WelAgsCustomerAuthenticationStrategyImpl implements WelAgsAuthenticationStrategy
{
	@Resource
	private CustomerFacade customerFacade;

	@Override
	public void refreshSpringSecurityToken()
	{
		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		final String customerUid = customerFacade.getCurrentCustomer().getUid().toLowerCase();
		final UsernamePasswordAuthenticationToken newAuthentication = new UsernamePasswordAuthenticationToken(customerUid,
				null, authentication.getAuthorities());
		newAuthentication.setDetails(authentication.getDetails());
		SecurityContextHolder.getContext().setAuthentication(newAuthentication);
	}

	@Override
	public void setSpringSecurityToken(final Authentication authentication)
	{
		SecurityContextHolder.getContext().setAuthentication(authentication);
	}
}
