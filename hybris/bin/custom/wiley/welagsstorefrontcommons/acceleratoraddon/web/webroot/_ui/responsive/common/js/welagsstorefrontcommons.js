// put common js here. Also you can create another js file if needed, and register it at project.properties. e.g. wileystorefrontcommons.css.paths.responsive=/responsive/common/css/wileystorefrontcommons.css

ACC.wileystorefrontcommons = {

	_autoload: [
		"bindDate"
	],

	bindDate: function ()
	{
		$("[data-date-millis]").each(function() {
    		var orderDate = new Date($(this).data("date-millis"));
        	$(this).text(formatAMPM(orderDate));
     	});

    	function formatAMPM(date) {
			var year = date.getFullYear().toString().substring(2);
			var day = date.getDate();
			var month = date.getMonth() + 1;
			var hours = date.getHours();
			var minutes = date.getMinutes();
			var ampm = hours >= 12 ? 'pm' : 'am';
			hours = hours % 12;
			hours = hours ? hours : 12; // the hour '0' should be '12'
			minutes = minutes < 10 ? '0'+minutes : minutes;
			var strTime = month + '/' + day + '/' + year + ' ' + hours + ':' + minutes + ampm;
			return strTime;
    	}
	}
};
