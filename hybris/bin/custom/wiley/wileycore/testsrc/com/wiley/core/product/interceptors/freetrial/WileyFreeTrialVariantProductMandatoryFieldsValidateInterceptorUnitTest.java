package com.wiley.core.product.interceptors.freetrial;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.servicelayer.i18n.L10NService;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyFreeTrialProductModel;
import com.wiley.core.model.WileyFreeTrialVariantProductModel;
import com.wiley.core.product.WileyProductService;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;



/**
 * Unit test for {@link WileyFreeTrialVariantProductMandatoryFieldsValidateInterceptor}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyFreeTrialVariantProductMandatoryFieldsValidateInterceptorUnitTest
{
	@InjectMocks
	private WileyFreeTrialVariantProductMandatoryFieldsValidateInterceptor interceptor;

	@Mock
	private L10NService l10nService;

	@Mock
	private InterceptorContext interceptorContext;

	@Mock
	private CatalogVersionModel catalogVersion;

	@Mock
	private WileyFreeTrialVariantProductModel model;

	@Mock
	private WileyFreeTrialProductModel baseTrialProduct;

	@Mock
	private WileyProductService wileyProductService;

	/**
	 * Sets up.
	 */
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);

		when(l10nService.getLocalizedString("error.wileytrialproduct.Required.fields.missing.message"))
				.thenReturn("Required fields are not filled");

		interceptor.setL10nService(l10nService);
	}

	@Test
	public void testOnValidateSuccess() throws InterceptorException
	{
		WileyFreeTrialVariantProductMandatoryFieldsValidateInterceptor interceptor = spy(
				this.interceptor);
		doReturn(Boolean.TRUE).when(interceptor).validateCommonFields(model);
		when(model.getBaseProduct()).thenReturn(baseTrialProduct);

		interceptor.onValidate(model, interceptorContext);
	}

	@Test(expected = InterceptorException.class)
	public void testOnValidateFail() throws InterceptorException
	{
		WileyFreeTrialVariantProductMandatoryFieldsValidateInterceptor interceptor = spy(
				this.interceptor);
		doReturn(Boolean.TRUE).when(interceptor).validateCommonFields(model);
		when(model.getBaseProduct()).thenReturn(null);

		interceptor.onValidate(model, interceptorContext);
	}
}
