package com.wiley.core.jalo;

import static org.junit.Assert.assertEquals;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.jalo.CatalogManager;
import de.hybris.platform.catalog.model.SyncItemJobModel;
import de.hybris.platform.commerceservices.setup.SetupSyncJobService;
import de.hybris.platform.servicelayer.ServicelayerTest;
import de.hybris.platform.servicelayer.event.EventService;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationListener;

import com.wiley.core.event.AfterCronJobFinishedEvent;


@IntegrationTest
public class WileyCatalogVersionSyncJobTest extends ServicelayerTest
{
	private static final String TEST_PRODUCT_CATALOG = "productCatalog";

	@Resource
	private SetupSyncJobService setupSyncJobService;

	@Resource
	private CatalogVersionService catalogVersionService;

	@Resource
	private EventService eventService;

	private TestFinishListener testFinishListener;

	@Before
	public void setUp() throws Exception
	{
		importCsv("/wileycore/test/testSystemSetup.impex", "utf8");
		testFinishListener = new TestFinishListener();
		eventService.registerEventListener(testFinishListener);
	}

	@Test
	public void shouldSendAfterCronJobFinishedEvent()
	{

		setupSyncJobService.createProductCatalogSyncJob(TEST_PRODUCT_CATALOG);
		final SyncItemJobModel job = getCatalogSyncJob(TEST_PRODUCT_CATALOG);
		setupSyncJobService.executeCatalogSyncJob(TEST_PRODUCT_CATALOG);
		assertEquals(1, testFinishListener.events.stream().filter(evt -> evt.getJobCode().equals(job.getCode())).count());

	}

	protected SyncItemJobModel getCatalogSyncJob(final String catalogId)
	{
		final List<SyncItemJobModel> synchronizations = catalogVersionService
				.getCatalogVersion(catalogId, CatalogManager.OFFLINE_VERSION).getSynchronizations();
		if (CollectionUtils.isNotEmpty(synchronizations))
		{
			return synchronizations.get(0);
		}
		return null;
	}

	static class TestFinishListener implements ApplicationListener<AfterCronJobFinishedEvent>
	{
		final List<AfterCronJobFinishedEvent> events = new ArrayList<>();

		@Override
		public void onApplicationEvent(final AfterCronJobFinishedEvent event)
		{
			events.add(event);
		}
	}
}
