package com.wiley.core.wileyb2c.product.access.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.enums.WileyDigitalContentTypeEnum;
import com.wiley.core.lightningsource.LightningSourceGateway;
import com.wiley.core.servicelayer.i18n.WileycomI18NService;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;


/**
 * Created by Georgii_Gavrysh on 9/8/2016.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cLightningSourceDownloadDigitalStrategyUnitTest
{
	@InjectMocks
	private Wileyb2cLightningSourceDownloadDigitalStrategy testedStrategy;

	@Mock
	private OrderEntryModel orderEntryModelMock;

	@Mock
	private ProductModel productModelMock;

	@Mock
	private LightningSourceGateway lightningSourceGatewayMock;

	@Mock
	private WileycomI18NService wileycomI18NServiceMock;

	@Mock
	private CountryModel countryModelMock;

	@Mock
	private ConfigurationService configurationServiceMock;

	@Mock
	private Configuration configurationMock;

	@Before
	public void setUp()
	{
		when(orderEntryModelMock.getProduct()).thenReturn(productModelMock);
		when(wileycomI18NServiceMock.getCurrentCountry()).thenReturn(Optional.of(countryModelMock));
		when(countryModelMock.getIsocode()).thenReturn("US");
		when(configurationServiceMock.getConfiguration()).thenReturn(configurationMock);
		when(configurationMock.getString("default.country.isocode", "US")).thenReturn("US");
	}

	@Test
	public void testApplyLightningSource()
	{
		when(productModelMock.getDigitalContentType()).thenReturn(WileyDigitalContentTypeEnum.LIGHTNING_SOURCE);

		final boolean apply = testedStrategy.apply(orderEntryModelMock);

		assertThat(apply, is(true));
	}

	@Test
	public void testApplyVitalSource()
	{
		when(productModelMock.getDigitalContentType()).thenReturn(WileyDigitalContentTypeEnum.VITAL_SOURCE);

		final boolean apply = testedStrategy.apply(orderEntryModelMock);

		assertThat(apply, is(false));
	}

	@Test
	public void testGenerateLink() throws Exception
	{

		when(lightningSourceGatewayMock.getLink("US", orderEntryModelMock)).thenReturn("a_link");

		final String redirectUrl = testedStrategy.generateRedirectUrl(orderEntryModelMock);

		assertThat(redirectUrl, is("a_link"));
	}
}
