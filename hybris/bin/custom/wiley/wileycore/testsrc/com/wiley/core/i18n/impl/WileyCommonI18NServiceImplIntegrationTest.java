package com.wiley.core.i18n.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.order.PaymentModeService;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import javax.annotation.Resource;

import org.junit.Test;

import com.wiley.core.i18n.WileyCommonI18NService;
import com.wiley.core.i18n.WileyCountryService;


@IntegrationTest
public class WileyCommonI18NServiceImplIntegrationTest extends ServicelayerTransactionalTest
{
	private static final String WILEYB2C_BASE_SITE = "wileyb2c";
	private static final String WILEYB2B_BASE_SITE = "wileyb2b";
	private static final String AGS_BASE_SITE = "ags";
	private static final String CANADA_ISO_CODE = "CA";
	private static final String US_ISO_CODE = "US";
	private static final String CAD_ISO_CODE = "CAD";
	private static final String USD_ISO_CODE = "USD";
	private static final String EUR_ISO_CODE = "EUR";
	public static final String TEST_PAYMENTMODE = "testPaymentMode";

	@Resource
	private BaseSiteService baseSiteService;
	@Resource
	private BaseStoreService baseStoreService;
	@Resource
	private WileyCommonI18NService wileyCommonI18NService;
	@Resource
	private WileyCountryService wileyCountryService;
	@Resource
	private PaymentModeService paymentModeService;

	@Test
	public void shouldReturnProperCurrencyAgainstCurrentBaseStore()
	{
		//given
		baseSiteService.setCurrentBaseSite(baseSiteService.getBaseSiteForUID(WILEYB2C_BASE_SITE), true);
		final CountryModel country = wileyCountryService.findCountryByCode(US_ISO_CODE);

		//when
		final CurrencyModel defaultCurrency = wileyCommonI18NService.getDefaultCurrency(country);

		//then
		assertEquals(defaultCurrency.getIsocode(), USD_ISO_CODE);
	}

	@Test
	public void shouldReturnProperCurrencyAgainstSpecifiedBaseStore()
	{
		//given
		final BaseStoreModel baseStore = baseStoreService.getBaseStoreForUid(WILEYB2B_BASE_SITE);
		final CountryModel country = wileyCountryService.findCountryByCode(CANADA_ISO_CODE);

		//when
		final CurrencyModel defaultCurrency = wileyCommonI18NService.getDefaultCurrency(country, baseStore);

		//then
		assertEquals(defaultCurrency.getIsocode(), CAD_ISO_CODE);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowExceptionWhenCurrentBaseStoreNotSpecified()
	{
		//given
		final CountryModel country = wileyCountryService.findCountryByCode(US_ISO_CODE);

		//when
		wileyCommonI18NService.getDefaultCurrency(country);

		//then
		fail("currentBasestore not specified");
	}

	@Test(expected = IllegalStateException.class)
	public void shouldThrowExceptionWhenDefaultCurrencyNotConfigured()
	{
		//given
		final CountryModel country = wileyCountryService.findCountryByCode(CANADA_ISO_CODE);
		baseSiteService.setCurrentBaseSite(baseSiteService.getBaseSiteForUID(AGS_BASE_SITE), true);

		//when
		wileyCommonI18NService.getDefaultCurrency(country);

		//then
		fail("configuration not found");
	}
}
