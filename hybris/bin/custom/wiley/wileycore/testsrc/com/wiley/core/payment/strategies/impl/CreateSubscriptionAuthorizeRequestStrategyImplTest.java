package com.wiley.core.payment.strategies.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.DebitPaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.payment.dto.BillingInfo;
import de.hybris.platform.store.BaseStoreModel;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Currency;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.customer.service.WileyCustomerAccountService;
import com.wiley.core.payment.request.WileySubscriptionAuthorizeRequest;
import com.wiley.core.payment.strategies.AmountToPayCalculationStrategy;


@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class CreateSubscriptionAuthorizeRequestStrategyImplTest
{
	private static final String CARD_TOKEN = "cardToken";
	private static final BigDecimal AMOUNT = new BigDecimal("10.22");
	private static final String PAYMENT_PROVIDER = "paymentProvider";
	private static final String POSTCODE = "postcode";
	private static final String STREET = "street";
	private static final String USD = "USD";
	private static final String SITE_ID = "siteId";
	@InjectMocks
	private CreateSubscriptionAuthorizeRequestStrategyImpl underTest =
			new CreateSubscriptionAuthorizeRequestStrategyImpl();
	@Mock
	private OrderModel mockOrder;
	@Mock
	private BaseStoreModel mockStore;
	@Mock
	private CurrencyModel mockCurrency;
	@Mock
	private CustomerModel mockCustomer;
	@Mock
	private CreditCardPaymentInfoModel mockPaymentInfo;
	@Mock
	private AddressModel mockPaymentAddress;
	@Mock
	private BaseSiteModel mockSite;
	@Mock
	private AmountToPayCalculationStrategy amountToPayCalculationStrategy;
	@Mock
	private WileyCustomerAccountService mockCustomerAccountService;

	@Before
	public void setup()
	{
		when(amountToPayCalculationStrategy.getOrderAmountToPay(mockOrder)).thenReturn(AMOUNT);
		when(mockOrder.getCurrency()).thenReturn(mockCurrency);
		when(mockOrder.getUser()).thenReturn(mockCustomer);
		when(mockOrder.getStore()).thenReturn(mockStore);
		when(mockOrder.getSite()).thenReturn(mockSite);
		when(mockStore.getPaymentProvider()).thenReturn(PAYMENT_PROVIDER);
		when(mockSite.getUid()).thenReturn(SITE_ID);
		when(mockCustomer.getDefaultPaymentInfo()).thenReturn(mockPaymentInfo);
		when(mockCustomerAccountService.getPaymentAddress(mockCustomer)).thenReturn(mockPaymentAddress);
		when(mockPaymentInfo.getSubscriptionId()).thenReturn(CARD_TOKEN);
		when(mockPaymentAddress.getLine1()).thenReturn(STREET);
		when(mockPaymentAddress.getPostalcode()).thenReturn(POSTCODE);
		when(mockCurrency.getIsocode()).thenReturn(USD);
	}

	@Test
	public void shouldReturnPopulatedRequest()
	{
		WileySubscriptionAuthorizeRequest request = underTest.createRequest(mockOrder);

		assertEquals(SITE_ID, request.getSite());
		assertEquals(CARD_TOKEN, request.getSubscriptionID());
		assertEquals(Currency.getInstance(USD), request.getCurrency());
		assertEquals(AMOUNT, request.getTotalAmount());
		assertEquals(PAYMENT_PROVIDER, request.getPaymentProvider());

		verifyAddress(request.getShippingInfo());

	}

	@Test(expected = IllegalStateException.class)
	public void shouldThrowWhenNoCreditCardPaymentInfo()
	{
		when(mockCustomer.getDefaultPaymentInfo()).thenReturn(mock(DebitPaymentInfoModel.class));
		underTest.createRequest(mockOrder);
	}

	@Test(expected = IllegalStateException.class)
	public void shouldThrowWhenOrderUserIsNotCustomer()
	{
		when(mockOrder.getUser()).thenReturn(mock(EmployeeModel.class));
		underTest.createRequest(mockOrder);
	}

	@Test(expected = IllegalStateException.class)
	public void shouldThrowWheNoDefaultPaymentAddress()
	{
		when(mockCustomerAccountService.getPaymentAddress(mockCustomer)).thenReturn(null);
		underTest.createRequest(mockOrder);
	}

	private void verifyAddress(final BillingInfo address)
	{
		assertEquals(STREET, address.getStreet1());
		assertEquals(POSTCODE, address.getPostalCode());
	}
}

