package com.wiley.core.wileyb2c.sitemap.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.sitemap.data.SiteMapUrlData;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Created by Maksim_Kozich on 29.08.17.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cCustomPageModelToSiteMapUrlDataPopulatorUnitTest
{
	private static final String SOURCE_URL_WITH_RESERVED_CHARACTERS = "SOURCE URL";
	private static final String LOCATION = "some location";

	@Mock
	private SiteMapUrlData siteMapUrlDataMock;

	@Mock
	private Logger loggerMock;

	@Spy
	@InjectMocks
	private Wileyb2cCustomPageModelToSiteMapUrlDataPopulator testInstance;

	@Before
	public void setUp()
	{
		when(testInstance.getLogger()).thenReturn(loggerMock);
		doNothing().when(testInstance).superPopulate(SOURCE_URL_WITH_RESERVED_CHARACTERS, siteMapUrlDataMock);
	}

	@Test
	public void testNoErrorIsLoggedIfLocIsNotNull()
	{
		// Given
		when(siteMapUrlDataMock.getLoc()).thenReturn(LOCATION);

		// When
		testInstance.populate(SOURCE_URL_WITH_RESERVED_CHARACTERS, siteMapUrlDataMock);

		// Then
		verify(loggerMock, never()).error(any());
	}

	@Test
	public void testErrorIsLoggedIfLocIsNull()
	{
		// Given
		when(siteMapUrlDataMock.getLoc()).thenReturn(null);

		// When
		testInstance.populate(SOURCE_URL_WITH_RESERVED_CHARACTERS, siteMapUrlDataMock);

		// Then
		verify(loggerMock).error(Mockito.contains(SOURCE_URL_WITH_RESERVED_CHARACTERS));
	}
}
