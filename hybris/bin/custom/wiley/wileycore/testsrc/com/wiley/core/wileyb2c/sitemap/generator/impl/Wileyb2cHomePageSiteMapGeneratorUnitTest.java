package com.wiley.core.wileyb2c.sitemap.generator.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Created by Maksim_Kozich on 29.08.17.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cHomePageSiteMapGeneratorUnitTest
{
	@Mock
	private CMSPageService cmsPageServiceMock;

	@Mock
	private ContentPageModel homePageModelMock;

	@InjectMocks
	private Wileyb2cHomePageSiteMapGenerator testInstance;

	@Test
	public void testGetDataInternalShouldReturnSingletonListWhenHomePageIsConfigured()
	{
		// Given
		when(cmsPageServiceMock.getHomepage()).thenReturn(homePageModelMock);

		// When
		List<ContentPageModel> result = testInstance.getDataInternal(null);

		// Then
		assertNotNull(result);
		assertTrue(result.size() == 1);
		assertSame(result.get(0), homePageModelMock);
	}

	@Test
	public void testGetDataInternalShouldReturnEmptyListWhenHomePageIsNotConfigured()
	{
		// Given
		when(cmsPageServiceMock.getHomepage()).thenReturn(null);

		// When
		List<ContentPageModel> result = testInstance.getDataInternal(null);

		// Then
		assertNotNull(result);
		assertTrue(result.size() == 0);
	}
}
