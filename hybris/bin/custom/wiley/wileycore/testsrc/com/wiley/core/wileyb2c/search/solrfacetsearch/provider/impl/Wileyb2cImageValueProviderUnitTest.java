package com.wiley.core.wileyb2c.search.solrfacetsearch.provider.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaFormatModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.media.MediaContainerService;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyPurchaseOptionProductModel;


/**
 * Created by Uladzimir_Barouski on 3/13/2017.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cImageValueProviderUnitTest
{
	private static final String MEDIA_FORMAT = "thumbnail";
	private static final String GALLERY_IMAGE_URL = "gallery_image_url";
	private static final String EXTERNAL_IMAGE_URL = "external_image_url";
	private static final String FIELD_NAME = "thumbnail_field_name";
	@InjectMocks
	private Wileyb2cImageValueProvider wileyb2cImageValueProvider;
	@Mock
	private MediaService mediaService;
	@Mock
	private MediaContainerService mediaContainerService;
	@Mock
	private IndexConfig indexConfig;
	@Mock
	private IndexedProperty indexedProperty;
	@Mock
	private ProductModel productModel;
	@Mock
	private WileyPurchaseOptionProductModel variantProductModel;
	@Mock
	private FieldNameProvider fieldNameProvider;
	@Mock
	private MediaFormatModel thumbnailMediaFormat;
	@Mock
	private MediaContainerModel externalImageMediaContainer, galleryImageMediaContainer;
	@Mock
	private MediaModel thumbnailMedia;

	@Before
	public void setUp() throws Exception
	{
		wileyb2cImageValueProvider.setMediaFormat(MEDIA_FORMAT);
		when(mediaService.getFormat(MEDIA_FORMAT)).thenReturn(thumbnailMediaFormat);
		when(fieldNameProvider.getFieldNames(indexedProperty, null)).thenReturn(Arrays.asList(FIELD_NAME));
		when(variantProductModel.getBaseProduct()).thenReturn(productModel);
	}

	@Test
	public void getFieldValuesWhenGalleryNotEmpty() throws Exception
	{
		//Given
		when(productModel.getGalleryImages()).thenReturn(Arrays.asList(galleryImageMediaContainer));
		when(mediaContainerService.getMediaForFormat(galleryImageMediaContainer, thumbnailMediaFormat)).thenReturn(
				thumbnailMedia);
		when(thumbnailMedia.getURL()).thenReturn(GALLERY_IMAGE_URL);
		//When
		Collection<FieldValue> images = wileyb2cImageValueProvider.getFieldValues(indexConfig, indexedProperty,
				variantProductModel);

		//Then
		assertTrue(CollectionUtils.isNotEmpty(images));
		assertEquals(1, images.size());
		assertEquals(GALLERY_IMAGE_URL, images.iterator().next().getValue());
	}

	@Test
	public void getFieldValuesWhenGalleryEmpty() throws Exception
	{
		//Given
		when(productModel.getGalleryImages()).thenReturn(Collections.emptyList());
		when(productModel.getExternalImage()).thenReturn(externalImageMediaContainer);
		when(mediaContainerService.getMediaForFormat(externalImageMediaContainer, thumbnailMediaFormat)).thenReturn(
				thumbnailMedia);
		when(thumbnailMedia.getURL()).thenReturn(EXTERNAL_IMAGE_URL);
		//When
		Collection<FieldValue> images = wileyb2cImageValueProvider.getFieldValues(indexConfig, indexedProperty,
				variantProductModel);

		//Then
		assertTrue(CollectionUtils.isNotEmpty(images));
		assertEquals(1, images.size());
		assertEquals(EXTERNAL_IMAGE_URL, images.iterator().next().getValue());
	}

	@Test
	public void getFieldValuesWhenNoGalleryAndExternalImages() throws Exception
	{
		//Given
		when(productModel.getGalleryImages()).thenReturn(Collections.emptyList());
		//When
		Collection<FieldValue> images = wileyb2cImageValueProvider.getFieldValues(indexConfig, indexedProperty,
				variantProductModel);

		//Then
		assertTrue(CollectionUtils.isEmpty(images));
	}

}