package com.wiley.core.payment.impl;

import com.wiley.core.payment.enums.WileyTransactionStatusEnum;
import com.wiley.core.payment.request.WileyCaptureRequest;
import com.wiley.core.payment.response.WileyCaptureResult;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.payment.PaymentService;
import de.hybris.platform.payment.commands.request.CaptureRequest;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.methods.CardPaymentService;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.model.ModelService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Currency;
import java.util.Date;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class WileyWPGPaymentServiceImplCaptureTest
{
	private static final String TRANSACTION_ID = "transactionId";
	private static final String PAYMENT_PROVIDER = "paymentProvider";
	private static final String REQUEST_TOKEN = "requestToken";
	private static final BigDecimal VALUE = new BigDecimal("10.01");
	private static final String WPG_AUTH_CODE = "wpgAuthCode";
	private static final String SITE_ID = "siteId";
	private static final String USD = "USD";
	private static final String NEW_ENTRY_CODE = "newEntryCode";
	private static final String MERCHANT_RESPONSE = "merchantResponse";
	private static final TransactionStatus TRANSACTION_STATUS = TransactionStatus.ACCEPTED;
	private static final Date REQUEST_TIME = new Date();
	private static final WileyTransactionStatusEnum WPG_STATUS = WileyTransactionStatusEnum.SUCCESS;

	@Mock
	private ModelService mockModelService;
	@Mock
	private CardPaymentService mockCardPaymentService;
	@Mock
	private PaymentService mockPaymentService;
	@InjectMocks
	private WileyWPGPaymentServiceImpl underTest = new WileyWPGPaymentServiceImpl();

	@Mock
	private PaymentTransactionModel mockTransaction;
	@Mock
	private PaymentTransactionEntryModel mockCaptureEntry;
	@Mock
	private PaymentTransactionEntryModel mockAuthEntry;
	@Mock
	private OrderModel mockOrder;
	@Mock
	private CurrencyModel mockCurrency;
	@Mock
	private BaseSiteModel mockSite;
	@Captor
	private ArgumentCaptor<WileyCaptureRequest> captureRequestArgumentCaptor;

	private WileyCaptureResult captureResult = new WileyCaptureResult();

	@Before
	public void setup()
	{
		when(mockTransaction.getOrder()).thenReturn(mockOrder);
		when(mockTransaction.getRequestId()).thenReturn(TRANSACTION_ID);
		when(mockTransaction.getPaymentProvider()).thenReturn(PAYMENT_PROVIDER);
		when(mockTransaction.getEntries()).thenReturn(Arrays.asList(mockAuthEntry));

		when(mockAuthEntry.getRequestToken()).thenReturn(REQUEST_TOKEN);
		when(mockAuthEntry.getCurrency()).thenReturn(mockCurrency);
		when(mockAuthEntry.getAmount()).thenReturn(VALUE);
		when(mockAuthEntry.getWpgAuthCode()).thenReturn(WPG_AUTH_CODE);
		when(mockAuthEntry.getTransactionStatus()).thenReturn(TransactionStatus.ACCEPTED.name());
		when(mockAuthEntry.getType()).thenReturn(PaymentTransactionType.AUTHORIZATION);
		when(mockAuthEntry.getPaymentTransaction()).thenReturn(mockTransaction);

		when(mockOrder.getSite()).thenReturn(mockSite);
		when(mockSite.getUid()).thenReturn(SITE_ID);
		when(mockCurrency.getIsocode()).thenReturn(USD);

		when(mockPaymentService.getNewPaymentTransactionEntryCode(mockTransaction, PaymentTransactionType.CAPTURE))
				.thenReturn(NEW_ENTRY_CODE);
		when(mockModelService.create(PaymentTransactionEntryModel.class)).thenReturn(mockCaptureEntry);
		when(mockCardPaymentService.capture(any(WileyCaptureRequest.class))).thenReturn(captureResult);

		captureResult.setStatus(WPG_STATUS);
		captureResult.setTransactionStatus(TRANSACTION_STATUS);
		captureResult.setTotalAmount(VALUE);
		captureResult.setRequestId(TRANSACTION_ID);
		captureResult.setRequestToken(REQUEST_TOKEN);
		captureResult.setMerchantResponse(MERCHANT_RESPONSE);
		captureResult.setTransactionStatus(TRANSACTION_STATUS);
		captureResult.setRequestTime(REQUEST_TIME);
	}

	@Test
	public void shouldPassCorrectParamsToCardService()
	{
		underTest.capture(mockTransaction);

		verify(mockCardPaymentService).capture(captureRequestArgumentCaptor.capture());
		WileyCaptureRequest actualRequest = captureRequestArgumentCaptor.getValue();
		assertEquals(NEW_ENTRY_CODE, actualRequest.getMerchantTransactionCode());
		assertEquals(TRANSACTION_ID, actualRequest.getRequestId());
		assertEquals(REQUEST_TOKEN, actualRequest.getRequestToken());
		assertEquals(Currency.getInstance(USD), actualRequest.getCurrency());
		assertEquals(VALUE, actualRequest.getTotalAmount());
		assertEquals(PAYMENT_PROVIDER, actualRequest.getPaymentProvider());
		assertEquals(WPG_AUTH_CODE, actualRequest.getAuthCode());
		assertEquals(SITE_ID, actualRequest.getSite());
	}

	@Test
	public void shouldSaveTransactionEntry()
	{
		Optional<PaymentTransactionEntryModel> captureEntry = underTest.capture(mockTransaction);

		assertTrue(captureEntry.isPresent());
		assertSame(mockCaptureEntry, captureEntry.get());

		verify(mockCaptureEntry).setRequestId(TRANSACTION_ID);
		verify(mockCaptureEntry).setRequestToken(REQUEST_TOKEN);
		verify(mockCaptureEntry).setTransactionStatus(TRANSACTION_STATUS.toString());
		verify(mockCaptureEntry).setCurrency(mockCurrency);
		verify(mockCaptureEntry).setCode(NEW_ENTRY_CODE);
		verify(mockCaptureEntry).setAmount(VALUE);
		verify(mockCaptureEntry).setTime(REQUEST_TIME);
		verify(mockCaptureEntry).setTransactionStatusDetails(WPG_STATUS.getDescription());
		verify(mockCaptureEntry).setWpgResponseCode(WPG_STATUS.getCode());
		verify(mockCaptureEntry).setWpgMerchantResponse(MERCHANT_RESPONSE);

		verify(mockModelService).save(mockCaptureEntry);
	}

	@Test
	public void shouldNotCaptureWhenAuthorizeEntryIsMissing()
	{
		when(mockAuthEntry.getType()).thenReturn(PaymentTransactionType.CREATE_SUBSCRIPTION);

		Optional<PaymentTransactionEntryModel> captureEntry = underTest.capture(mockTransaction);

		assertFalse(captureEntry.isPresent());
		verify(mockCardPaymentService, never()).capture(any(CaptureRequest.class));
	}

	@Test
	public void shouldNotCaptureWhenAuthorizeEntryIsNotAccepted()
	{
		when(mockAuthEntry.getTransactionStatus()).thenReturn(TransactionStatus.ERROR.toString());

		Optional<PaymentTransactionEntryModel> captureEntry = underTest.capture(mockTransaction);

		assertFalse(captureEntry.isPresent());
		verify(mockCardPaymentService, never()).capture(any(CaptureRequest.class));
	}

}
