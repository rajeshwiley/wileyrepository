package com.wiley.core.integration.esb.service.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Default unit test for {@link Wileyb2bCartEntryServiceImpl}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2bCartEntryServiceImplUnitTest
{

	@InjectMocks
	private Wileyb2bCartEntryServiceImpl wileyb2bCartEntryService;

	// Test Data
	@Mock
	private CartModel cartModelMock;

	@Mock
	private CartEntryModel cartEntryModelMock1;

	@Mock
	private ProductModel productModelMock1;
	private final String productIsbn1 = "productCode_1";

	@Mock
	private CartEntryModel cartEntryModelMock2;

	@Mock
	private ProductModel productModelMock2;
	private final String productIsbn2 = "productCode_2";

	@Before
	public void setUp() throws Exception
	{
		when(productModelMock1.getIsbn()).thenReturn(productIsbn1);
		when(productModelMock2.getIsbn()).thenReturn(productIsbn2);

		when(cartEntryModelMock1.getProduct()).thenReturn(productModelMock1);
		when(cartEntryModelMock2.getProduct()).thenReturn(productModelMock2);
	}

	@Test
	public void shouldFindCartEntryByIsbn()
	{
		// Given
		when(cartModelMock.getEntries()).thenReturn(Arrays.asList(cartEntryModelMock1, cartEntryModelMock2));

		// When
		final Optional<CartEntryModel> optionalCartEntry = wileyb2bCartEntryService.findCartEntryByIsbn(cartModelMock,
				productIsbn2);

		// Then
		assertTrue("CartEntry should be found.", optionalCartEntry.isPresent());
		assertSame(cartEntryModelMock2, optionalCartEntry.get());
	}

	@Test
	public void shouldNotFindCartEntryByIsbn()
	{
		// Given
		when(cartModelMock.getEntries()).thenReturn(Arrays.asList(cartEntryModelMock1, cartEntryModelMock2));

		// When
		final Optional<CartEntryModel> optionalCartEntry = wileyb2bCartEntryService.findCartEntryByIsbn(cartModelMock,
				"UnknownProduct");

		// Then
		assertFalse("CartEntry should not be found.", optionalCartEntry.isPresent());
	}
}