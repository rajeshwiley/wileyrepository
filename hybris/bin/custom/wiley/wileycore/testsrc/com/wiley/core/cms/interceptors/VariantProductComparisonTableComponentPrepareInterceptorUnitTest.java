package com.wiley.core.cms.interceptors;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyVariantProductModel;
import com.wiley.core.model.cms.CMSProductAttributeModel;
import com.wiley.core.model.components.VariantProductComparisonTableComponentModel;
import com.wiley.core.wiley.classification.WileyClassificationService;


/**
 * Created by Uladzimir_Barouski on 12/30/2016.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VariantProductComparisonTableComponentPrepareInterceptorUnitTest
{

	@Mock
	private VariantProductComparisonTableComponentModel comparisonTableComponentMock;
	@Mock
	private ModelService modelServiceMock;
	@Mock
	private WileyClassificationService wileyClassificationServiceMock;
	@Mock
	private InterceptorContext interceptorContextMock;
	@InjectMocks
	private VariantProductComparisonTableComponentPrepareInterceptor variantProductComparisonTableComponentPrepareInterceptor;

	@Before
	public void setUp() throws Exception
	{

	}

	@Test
	public void verifyWhenAllProductsRemovedComponent() throws Exception
	{
		//Given
		given(interceptorContextMock
				.isModified(comparisonTableComponentMock, VariantProductComparisonTableComponentModel.PRODUCTS)).willReturn(true);
		given(comparisonTableComponentMock.getProducts()).willReturn(Collections.emptyList());
		List<CMSProductAttributeModel> attributes = Arrays.asList(mock(CMSProductAttributeModel.class));
		given(comparisonTableComponentMock.getCmsProductAttributes()).willReturn(attributes);
		doNothing().when(modelServiceMock).removeAll(eq(attributes));

		//When
		variantProductComparisonTableComponentPrepareInterceptor.onPrepare(comparisonTableComponentMock, interceptorContextMock);

		//Then
		verify(modelServiceMock, times(1)).removeAll(eq(attributes));
		verify(wileyClassificationServiceMock, never()).getFilteredAssignmentForProducts(any());
		verify(wileyClassificationServiceMock, never()).mergeAssigmentsWithCMSProductAttributes(any(), any());
	}

	@Test
	public void verifyWhenComponentHasProducts() throws Exception
	{
		//Given
		given(interceptorContextMock
				.isModified(comparisonTableComponentMock, VariantProductComparisonTableComponentModel.PRODUCTS)).willReturn(true);
		List<WileyVariantProductModel> products = Arrays.asList(mock(WileyVariantProductModel.class));
		given(comparisonTableComponentMock.getProducts()).willReturn(products);
		List<CMSProductAttributeModel> attributes = Arrays.asList(mock(CMSProductAttributeModel.class));
		given(comparisonTableComponentMock.getCmsProductAttributes()).willReturn(attributes);
		Set<ClassAttributeAssignmentModel> assignments = new HashSet<>(Arrays.asList(mock(ClassAttributeAssignmentModel.class)));
		given(wileyClassificationServiceMock.getFilteredAssignmentForProducts(products)).willReturn(assignments);
		doNothing().when(wileyClassificationServiceMock).mergeAssigmentsWithCMSProductAttributes(assignments,
				comparisonTableComponentMock);
		//When
		variantProductComparisonTableComponentPrepareInterceptor.onPrepare(comparisonTableComponentMock, interceptorContextMock);
		//Then
		verify(modelServiceMock, never()).removeAll(eq(attributes));
		verify(wileyClassificationServiceMock, times(1)).getFilteredAssignmentForProducts(eq(products));
		verify(wileyClassificationServiceMock, times(1))
				.mergeAssigmentsWithCMSProductAttributes(eq(assignments), eq(comparisonTableComponentMock));
	}

	@Test
	public void verifyDoNothingIfComponentNotModified() throws Exception
	{
		//Given
		given(interceptorContextMock
				.isModified(comparisonTableComponentMock, VariantProductComparisonTableComponentModel.PRODUCTS)).willReturn(
				false);
		//When
		variantProductComparisonTableComponentPrepareInterceptor.onPrepare(comparisonTableComponentMock, interceptorContextMock);
		//Then
		verify(modelServiceMock, never()).removeAll(any(List.class));
		verify(wileyClassificationServiceMock, never()).getFilteredAssignmentForProducts(any());
		verify(wileyClassificationServiceMock, never()).mergeAssigmentsWithCMSProductAttributes(any(), any());
	}
}