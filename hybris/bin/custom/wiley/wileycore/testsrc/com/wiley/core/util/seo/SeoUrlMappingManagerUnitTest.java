package com.wiley.core.util.seo;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.variants.model.VariantValueCategoryModel;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.io.UnsupportedEncodingException;
import java.util.Collections;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.category.WileyCategoryService;
import com.wiley.core.resolver.impl.WelCategoryModelUrlResolver;
import com.wiley.core.wileycom.seo.impl.WileyCatalogAwareSeoUrlResolver;


/**
 * Unit test for {@link SeoUrlMappingManager}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SeoUrlMappingManagerUnitTest
{
	private static final String BASE_SITE_UID = "testUid";
	private static final String PRODUCT_CATALOG = BASE_SITE_UID + "ProductCatalog";
	private static final String CATALOG_VERSION = "Online";
	private static final String STOREFRONT_WEBROOT = BASE_SITE_UID + "storefront.webroot";

	private static final String CONTEXT = "/teststorefront";
	private static final String SEO_FRIENDLY_ROOT_CATEGORY_URL = "/rootcategory/";
	private static final String SEO_FRIENDLY_SUB_CATEGORY_URL = "/subcategory/";
	private static final String SEO_FRIENDLY_ROOT_CATEGORY_PRODUCT_URL = "/rootcategory/rootproduct/";
	private static final String SEO_FRIENDLY_SUB_CATEGORY_PRODUCT_URL = "/subcategory/subproduct/";
	
	private static final String SEO_FRIENDLY_ROOT_CATEGORY_URL_WITH_SESSION_ID = "/rootcategory/;WJSESSIONID=5363423324";
	private static final String SEO_FRIENDLY_SUB_CATEGORY_URL_WITH_SESSION_ID = "/subcategory/;WJSESSIONID=5363423324";
	private static final String SEO_FRIENDLY_ROOT_CATEGORY_PRODUCT_URL_WITH_SESSION_ID = "/rootcategory/rootproduct/"
      + ";WJSESSIONID=5363423324";
	private static final String SEO_FRIENDLY_SUB_CATEGORY_PRODUCT_URL_WITH_SESSION_ID = "/subcategory/subproduct/"
      + ";WJSESSIONID=5363423324";

	private static final String ROOT_CATEGORY_CODE = "rootcategorycode";
	private static final String SUB_CATEGORY_CODE = "subcategorycode";
	private static final String ROOT_PRODUCT_CODE = "rootproductcode";
	private static final String SUB_PRODUCT_CODE = "subproductcode";

	private static final String ROOT_CATEGORY_EXPECTED_FORMAT = "/%s/products";
	private static final String L2_CATEGORY_EXPECTED_FORMAT = "/%s/products/%s";


	@Mock
	private ProductModel mockRootCategoryProductModel;

	@Mock
	private ProductModel mockSubcategoryProductModel;

	@Mock
	private CategoryModel mockRootCategoryModel;

	@Mock
	private VariantValueCategoryModel mockSubcategoryModel;

	@Mock
	private WelCategoryModelUrlResolver mockCategoryModelUrlResolver;

	@Mock
	private WileyCatalogAwareSeoUrlResolver mockProductModelUrlResolver;

	@Mock
	private CatalogVersionModel mockCatalogVersionModel;

	@Mock
	private BaseSiteModel mockBaseSiteModel;

	@Mock
	private CategoryService mockCategoryService;

	@Mock
	private ProductService mockProductService;

	@Mock
	private CatalogVersionService mockCatalogVersionService;

	@Mock
	private BaseSiteService mockBaseSiteService;

	@Mock
	private SiteConfigService mockSiteConfigService;

	@Mock
	private WileyCategoryService mockWileyCategoryService;

	@InjectMocks
	private SeoUrlMappingManager testedInstance = new SeoUrlMappingManager();

	@Before
	public void setup()
	{
		when(mockBaseSiteModel.getUid()).thenReturn(BASE_SITE_UID);
		when(mockRootCategoryModel.getCode()).thenReturn(ROOT_CATEGORY_CODE);
		when(mockSubcategoryModel.getCode()).thenReturn(SUB_CATEGORY_CODE);
		when(mockRootCategoryProductModel.getCode()).thenReturn(ROOT_PRODUCT_CODE);
		when(mockSubcategoryProductModel.getCode()).thenReturn(SUB_PRODUCT_CODE);

		when(mockBaseSiteService.getCurrentBaseSite()).thenReturn(mockBaseSiteModel);
		when(mockCatalogVersionService.getCatalogVersion(PRODUCT_CATALOG, CATALOG_VERSION)).thenReturn(mockCatalogVersionModel);
		when(mockSiteConfigService.getProperty(STOREFRONT_WEBROOT)).thenReturn(CONTEXT);

		when(mockCategoryService.getRootCategoriesForCatalogVersion(mockCatalogVersionModel)).
				thenReturn(Collections.singletonList(mockRootCategoryModel));
		when(mockCategoryService.getAllSubcategoriesForCategory(mockRootCategoryModel)).
				thenReturn(Collections.singletonList(mockSubcategoryModel));
		when(mockProductService.getOnlineProductsForCategory(mockRootCategoryModel)).
				thenReturn(Collections.singletonList(mockRootCategoryProductModel));
		when(mockProductService.getOnlineProductsForCategory(mockSubcategoryModel)).
				thenReturn(Collections.singletonList(mockSubcategoryProductModel));

		when(mockWileyCategoryService.getPrimaryWileyCategoryForVariantCategory(mockSubcategoryModel))
				.thenReturn(mockRootCategoryModel);
		
		when(mockCategoryModelUrlResolver.resolve(mockRootCategoryModel)).thenReturn(SEO_FRIENDLY_ROOT_CATEGORY_URL);
		when(mockCategoryModelUrlResolver.resolve(mockSubcategoryModel)).thenReturn(SEO_FRIENDLY_SUB_CATEGORY_URL);
		when(mockProductModelUrlResolver.resolve(mockRootCategoryProductModel))
               .thenReturn(SEO_FRIENDLY_ROOT_CATEGORY_PRODUCT_URL);
		when(mockProductModelUrlResolver.resolve(mockSubcategoryProductModel))
                .thenReturn(SEO_FRIENDLY_SUB_CATEGORY_PRODUCT_URL);
	}

	@Test
	public void shouldReturnInternalRootCategoryUrlForSeoFriendlyUrl() throws UnsupportedEncodingException
	{
		String expectedUrl = String.format(ROOT_CATEGORY_EXPECTED_FORMAT, ROOT_CATEGORY_CODE);

		verifyURIMapping("Should return internal category URL for SEO friendly category URL", expectedUrl,
				CONTEXT + SEO_FRIENDLY_ROOT_CATEGORY_URL);
	}

	@Test
	public void shouldReturnInternalSubcategoryUrlForSeoFriendlyUrl() throws UnsupportedEncodingException
	{
		String expectedUrl = String.format(L2_CATEGORY_EXPECTED_FORMAT, ROOT_CATEGORY_CODE, SUB_CATEGORY_CODE);

		verifyURIMapping("Should return internal subcategory URL for SEO friendly subcategory URL", expectedUrl,
				CONTEXT + SEO_FRIENDLY_SUB_CATEGORY_URL);
	}

	@Test
	public void shouldReturnInternalRootCategoryProductPDPUrlForSeoFriendlyUrl() throws UnsupportedEncodingException
	{
		String expectedUrl = "/p/" + ROOT_PRODUCT_CODE;

		verifyURIMapping("Should return internal root category product PDP URL for SEO friendly PDP URL", expectedUrl,
				CONTEXT + SEO_FRIENDLY_ROOT_CATEGORY_PRODUCT_URL);
	}

	@Test
	public void shouldReturnInternalSubcategoryProductPDPUrlForSeoFriendlyUrl() throws UnsupportedEncodingException
	{
		String expectedUrl = "/p/" + SUB_PRODUCT_CODE;
		verifyURIMapping("Should return internal subcategory product PDP URL for SEO friendly PDP URL", expectedUrl,
				CONTEXT + SEO_FRIENDLY_SUB_CATEGORY_PRODUCT_URL);
	}

	 @Test
	 public void shouldReturnInternalRootCategoryUrlForSeoFriendlyUrlWithoutContext() throws UnsupportedEncodingException
	 {
           String expectedUrl = String.format(ROOT_CATEGORY_EXPECTED_FORMAT, ROOT_CATEGORY_CODE);
           verifyURIMapping("Should return internal category URL for SEO friendly category URL", expectedUrl,
               SEO_FRIENDLY_ROOT_CATEGORY_URL);
	  }
    

	 @Test
	 public void shouldReturnInternalSubcategoryUrlForSeoFriendlyUrlWithoutContext() throws UnsupportedEncodingException
	 {
	     String expectedUrl = String.format(L2_CATEGORY_EXPECTED_FORMAT, ROOT_CATEGORY_CODE, SUB_CATEGORY_CODE);
	     verifyURIMapping("Should return internal subcategory URL for SEO friendly subcategory URL", expectedUrl,
	              SEO_FRIENDLY_SUB_CATEGORY_URL);
	 }

	 @Test
	 public void shouldReturnInternalRootCategoryProductPDPUrlForSeoFriendlyUrlWithoutContext() 
	     throws UnsupportedEncodingException
	 {
	    String expectedUrl = "/p/" + ROOT_PRODUCT_CODE;
	    verifyURIMapping("Should return internal root category product PDP URL for SEO friendly PDP URL", expectedUrl,
	              SEO_FRIENDLY_ROOT_CATEGORY_PRODUCT_URL);
	 }

	 @Test
	 public void shouldReturnInternalSubcategoryProductPDPUrlForSeoFriendlyUrlWithoutContext() throws UnsupportedEncodingException
	 {
	      String expectedUrl = "/p/" + SUB_PRODUCT_CODE;  
	      verifyURIMapping("Should return internal subcategory product PDP URL for SEO friendly PDP URL", expectedUrl,
	            SEO_FRIENDLY_SUB_CATEGORY_PRODUCT_URL);
	 }
	  
	@Test
	public void shouldReturnInternalRootCategoryUrlForSeoFriendlyUrlWithSessionId() throws UnsupportedEncodingException
	{
		String expectedUrl = String.format(ROOT_CATEGORY_EXPECTED_FORMAT, ROOT_CATEGORY_CODE);

		verifyURIMapping("Should return internal category URL for SEO friendly category URL", expectedUrl,
		CONTEXT + SEO_FRIENDLY_ROOT_CATEGORY_URL_WITH_SESSION_ID);
	}
	
	@Test
	public void shouldReturnInternalSubcategoryUrlForSeoFriendlyUrlWithSessionId() throws UnsupportedEncodingException
	{
		String expectedUrl = String.format(L2_CATEGORY_EXPECTED_FORMAT, ROOT_CATEGORY_CODE, SUB_CATEGORY_CODE);

		verifyURIMapping("Should return internal subcategory URL for SEO friendly subcategory URL", expectedUrl,
		CONTEXT + SEO_FRIENDLY_SUB_CATEGORY_URL_WITH_SESSION_ID);
	}

	@Test
	public void shouldReturnInternalRootCategoryProductPDPUrlForSeoFriendlyUrlWithSessionId() throws UnsupportedEncodingException
	{
		String expectedUrl = "/p/" + ROOT_PRODUCT_CODE;

		verifyURIMapping("Should return internal root category product PDP URL for SEO friendly PDP URL", expectedUrl,
		CONTEXT + SEO_FRIENDLY_ROOT_CATEGORY_PRODUCT_URL_WITH_SESSION_ID);
	}

	@Test
	public void shouldReturnInternalSubcategoryProductPDPUrlForSeoFriendlyUrlWithSessionId() throws UnsupportedEncodingException
	{
		String expectedUrl = "/p/" + SUB_PRODUCT_CODE;
		verifyURIMapping("Should return internal subcategory product PDP URL for SEO friendly PDP URL", expectedUrl,
		CONTEXT + SEO_FRIENDLY_SUB_CATEGORY_PRODUCT_URL_WITH_SESSION_ID);
	}
  
	private void verifyURIMapping(final String assertMessage, final String expectedInternal, final String seoUrl)
			throws UnsupportedEncodingException
	{
		assertEquals(assertMessage, expectedInternal, testedInstance.getMappingForURI(seoUrl));
		assertEquals(assertMessage + ". Absence of ending '/' is not tolerated ",
				expectedInternal, testedInstance.getMappingForURI(StringUtils.removeEnd(seoUrl, "/")));
	}

}
