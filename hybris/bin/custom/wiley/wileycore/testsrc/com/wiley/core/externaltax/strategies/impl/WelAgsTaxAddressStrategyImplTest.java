/**
 *
 */
package com.wiley.core.externaltax.strategies.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WelAgsTaxAddressStrategyImplTest
{
	@Mock
	private AbstractOrderModel abstractOrder;
	@Mock
	private UserModel userModel;
	@Mock
	private AddressModel defaultPaymentAddress;
	@Mock
	private AddressModel defaultShipmentAddress;
	@Mock
	private AddressModel deliveryAddress;

	@InjectMocks
	private final WelAgsTaxAddressStrategyImpl testedInstance = new WelAgsTaxAddressStrategyImpl();

	@Before
	public void setUp()
	{
		when(abstractOrder.getUser()).thenReturn(userModel);
		when(userModel.getDefaultPaymentAddress()).thenReturn(defaultPaymentAddress);
		when(userModel.getDefaultShipmentAddress()).thenReturn(defaultShipmentAddress);
	}

	@Test
	public void testShouldReturnDefaultPaymentAddress()
	{
		// when
		final AddressModel returnedAddress = testedInstance.resolvePaymentAddress(abstractOrder);
		// then
		Assert.assertEquals(returnedAddress, defaultPaymentAddress);
	}

	@Test
	public void testShouldReturnDefaultShipmentAddress()
	{
		// given 
		when(abstractOrder.getDeliveryAddress()).thenReturn(null);
		// when
		final AddressModel returnedAddress = testedInstance.resolveDeliveryAddress(abstractOrder);
		// then
		Assert.assertEquals(returnedAddress, defaultShipmentAddress);
	}

	@Test
	public void testShouldReturnDeliveryAddress()
	{
		// given
		when(abstractOrder.getDeliveryAddress()).thenReturn(deliveryAddress);
		// when
		final AddressModel returnedAddress = testedInstance.resolveDeliveryAddress(abstractOrder);
		// then
		Assert.assertEquals(returnedAddress, deliveryAddress);
	}

}
