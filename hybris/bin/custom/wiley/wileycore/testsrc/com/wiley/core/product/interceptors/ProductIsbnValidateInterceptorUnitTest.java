package com.wiley.core.product.interceptors;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.product.WileyProductService;


/**
 * Unit test for {@link ProductIsbnValidateInterceptor}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ProductIsbnValidateInterceptorUnitTest
{
	private static final String WEL_CATALOG_ID = "welProductCatalog";
	private static final String WILEY_CATALOG_ID = "wileyProductCatalog";

	@InjectMocks
	private ProductIsbnValidateInterceptor productIsbnValidateInterceptor;

	@Mock
	private WileyProductService wileyProductServiceMock;

	@Mock
	private InterceptorContext contextMock;

	@Mock
	private ProductModel productModelMock;
	@Mock
	private CatalogVersionModel welCatalogVersionModelMock;
	@Mock
	private CatalogModel welCatalogModelMock;
	@Mock
	private CatalogVersionModel wileyCatalogVersionModelMock;
	@Mock
	private CatalogModel wileyCatalogModelMock;

	@Before
	public void setUp()
	{
		Set<String> catalogIds = new HashSet<>();
		catalogIds.add(WEL_CATALOG_ID);
		productIsbnValidateInterceptor.setCatalogIds(catalogIds);
		productIsbnValidateInterceptor.setWileyProductService(wileyProductServiceMock);

		when(welCatalogVersionModelMock.getCatalog()).thenReturn(welCatalogModelMock);
		when(welCatalogModelMock.getId()).thenReturn(WEL_CATALOG_ID);
		when(wileyCatalogVersionModelMock.getCatalog()).thenReturn(wileyCatalogModelMock);
		when(wileyCatalogModelMock.getId()).thenReturn(WILEY_CATALOG_ID);
	}

	@Test
	public void testProductWithISBNisNull() throws InterceptorException
	{
		when(productModelMock.getCatalogVersion()).thenReturn(welCatalogVersionModelMock);
		productIsbnValidateInterceptor.onValidate(productModelMock, contextMock);
	}

	@Test
	public void testProductIsbnValid() throws InterceptorException
	{
		when(productModelMock.getIsbn()).thenReturn("testIsbn");
		when(productModelMock.getCatalogVersion()).thenReturn(welCatalogVersionModelMock);

		when(wileyProductServiceMock.getProductForIsbn("testIsbn", welCatalogVersionModelMock)).thenReturn(null);

		productIsbnValidateInterceptor.onValidate(productModelMock, contextMock);
	}

	@Test
	public void testUpdatedProduct() throws InterceptorException
	{
		ProductModel model = new ProductModel();

		model.setIsbn("testIsbn");
		model.setCatalogVersion(welCatalogVersionModelMock);

		when(wileyProductServiceMock.getProductForIsbn("testIsbn", welCatalogVersionModelMock)).thenReturn(model);

		productIsbnValidateInterceptor.onValidate(model, contextMock);
	}

	@Test(expected = InterceptorException.class)
	public void testFailProductIsbn() throws InterceptorException
	{
		ProductModel model = new ProductModel();

		model.setIsbn("testIsbn");
		model.setCatalogVersion(welCatalogVersionModelMock);

		when(productModelMock.getIsbn()).thenReturn("testIsbn");

		when(wileyProductServiceMock.getProductForIsbn("testIsbn", welCatalogVersionModelMock)).thenReturn(productModelMock);

		productIsbnValidateInterceptor.onValidate(model, contextMock);
	}

	@Test
	public void testSkipValidationIfProductIsNotInRestrictedCatalog() throws InterceptorException
	{
		ProductModel model = new ProductModel();

		model.setIsbn("testIsbn");
		model.setCatalogVersion(wileyCatalogVersionModelMock);

		productIsbnValidateInterceptor.onValidate(model, contextMock);

		verifyZeroInteractions(wileyProductServiceMock);
	}
}
