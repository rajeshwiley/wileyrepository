package com.wiley.core.order.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.order.CommerceAddToCartStrategy;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartModificationStatus;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.wiley.core.model.WileyProductVariantSetModel;
import com.wiley.core.model.WileyVariantProductModel;
import com.wiley.core.product.WileyProductService;
import com.wiley.core.product.WileyProductVariantSetService;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


/**
 * BugProof test for {@link DefaultWileyMultiDimensionalCommerceCartService}
 *
 * @author Aliaksei_Zlobich
 */
@UnitTest
public class DefaultWileyMultiDimensionalCommerceCartServiceBugProofUnitTest
{

	private DefaultWileyMultiDimensionalCommerceCartService defaultWELCommerceCartService;

	@Mock
	private ProductModel productMock;

	@Mock
	private CartModel cartMock;

	@Mock
	private CommerceAddToCartStrategy commerceAddToCartStrategyMock;

	@Mock
	private WileyProductVariantSetService wileyProductVariantSetServiceMock;

	@Mock
	private WileyProductService wileyProductServiceMock;


	// Cart Entries
	// Entry 1
	@Mock
	private AbstractOrderEntryModel orderEntryMock1;
	private final Integer orderEntryNumber1 = 1;
	@Mock
	private WileyVariantProductModel productMock1;

	// Entry 2
	@Mock
	private AbstractOrderEntryModel orderEntryMock2;
	private final Integer orderEntryNumber2 = 2;
	@Mock
	private WileyVariantProductModel productMock2;

	// Entry 3
	@Mock
	private AbstractOrderEntryModel orderEntryMock3;
	private final Integer orderEntryNumber3 = 3;
	@Mock
	private WileyVariantProductModel productMock3;

	// Entry 4
	@Mock
	private AbstractOrderEntryModel orderEntryMock4;
	private final Integer orderEntryNumber4 = 4;
	@Mock
	private WileyVariantProductModel productMock4;

	// Product Sets
	@Mock
	private WileyProductVariantSetModel productVariantSetMock1;
	@Mock
	private WileyProductVariantSetModel productVariantSetMock2;

	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);

		// Set up Cart
		when(orderEntryMock1.getEntryNumber()).thenReturn(orderEntryNumber1);
		when(orderEntryMock1.getProduct()).thenReturn(productMock1);

		when(orderEntryMock2.getEntryNumber()).thenReturn(orderEntryNumber2);
		when(orderEntryMock2.getProduct()).thenReturn(productMock2);

		when(orderEntryMock3.getEntryNumber()).thenReturn(orderEntryNumber3);
		when(orderEntryMock3.getProduct()).thenReturn(productMock3);

		when(orderEntryMock4.getEntryNumber()).thenReturn(orderEntryNumber4);
		when(orderEntryMock4.getProduct()).thenReturn(productMock4);

		final List<AbstractOrderEntryModel> cartEntries = new ArrayList<>();
		cartEntries.add(orderEntryMock1);
		cartEntries.add(orderEntryMock2);
		cartEntries.add(orderEntryMock3);
		cartEntries.add(orderEntryMock4);

		when(cartMock.getEntries()).thenReturn(cartEntries);

		// Set up CommerceAddToCartStrategy
		CommerceCartModification addCartModification = new CommerceCartModification();
		addCartModification.setStatusCode(CommerceCartModificationStatus.SUCCESS);
		when(commerceAddToCartStrategyMock.addToCart(any(CommerceCartParameter.class))).thenReturn(addCartModification);


		// Set up DefaultWileyMultiDimensionalCommerceCartService
		defaultWELCommerceCartService = new DefaultWileyMultiDimensionalCommerceCartService();
		defaultWELCommerceCartService.setCommerceAddToCartStrategy(commerceAddToCartStrategyMock);
	}

	/**
	 * Steps to reproduce:
	 *
	 * 1. Add 3 parts (AUD,BEC,FAR)
	 * 2. Add again 3 parts (AUD,BEC,REG)
	 * 3. Go to cart page.
	 *
	 * Expected: one product set (AUD+BEC+FAR+REG) and two products (AUD,BEC)
	 * Actually: one product set
	 */
	@Test
	public void wrongBehaviorWhenAddingMultipleParts() throws CommerceCartModificationException
	{
		// Given
		// Setup the system under test
		CommerceCartParameter paramAUD = new CommerceCartParameter();
		paramAUD.setProduct(productMock1);
		paramAUD.setQuantity(1);
		paramAUD.setCart(cartMock);
		CommerceCartParameter paramBEC = new CommerceCartParameter();
		paramBEC.setProduct(productMock2);
		paramBEC.setQuantity(1);
		paramBEC.setCart(cartMock);
		CommerceCartParameter paramFAR = new CommerceCartParameter();
		paramFAR.setProduct(productMock3);
		paramFAR.setQuantity(1);
		paramFAR.setCart(cartMock);
		CommerceCartParameter paramREG = new CommerceCartParameter();
		paramREG.setProduct(productMock4);
		paramREG.setQuantity(1);
		paramREG.setCart(cartMock);

		// When
		final CommerceCartModification modificationAUD1 = defaultWELCommerceCartService.addToCart(paramAUD);
		final CommerceCartModification modificationBEC1 = defaultWELCommerceCartService.addToCart(paramBEC);
		final CommerceCartModification modificationFAR1 = defaultWELCommerceCartService.addToCart(paramFAR);

		final CommerceCartModification modificationAUD2 = defaultWELCommerceCartService.addToCart(paramAUD);
		final CommerceCartModification modificationBEC2 = defaultWELCommerceCartService.addToCart(paramBEC);
		final CommerceCartModification modificationREG2 = defaultWELCommerceCartService.addToCart(paramREG);

		// Then
		// checking that we got results
		assertNotNull(modificationAUD1);
		assertNotNull(modificationBEC1);
		assertNotNull(modificationFAR1);
		assertNotNull(modificationAUD2);
		assertNotNull(modificationBEC2);
		assertNotNull(modificationREG2);
		// checking that products have been added
		verify(commerceAddToCartStrategyMock, times(2)).addToCart(paramAUD);
		verify(commerceAddToCartStrategyMock, times(2)).addToCart(paramBEC);
		verify(commerceAddToCartStrategyMock).addToCart(paramFAR);
		verify(commerceAddToCartStrategyMock).addToCart(paramREG);
	}
}