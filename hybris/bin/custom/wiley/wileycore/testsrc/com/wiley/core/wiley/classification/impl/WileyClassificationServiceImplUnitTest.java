package com.wiley.core.wiley.classification.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.enums.ClassificationAttributeTypeEnum;
import de.hybris.platform.catalog.model.ProductFeatureModel;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeModel;
import de.hybris.platform.servicelayer.keygenerator.impl.PersistentKeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.enums.ClassAttributeAssignmentTypeEnum;
import com.wiley.core.model.WileyVariantProductModel;
import com.wiley.core.model.cms.CMSProductAttributeModel;
import com.wiley.core.model.components.VariantProductComparisonTableComponentModel;


/**
 * Created by Uladzimir_Barouski on 12/31/2016.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyClassificationServiceImplUnitTest
{
	private static final String CLASS_ATTRIBUTE_1 = "classAttribute1";
	private static final String CLASS_ATTRIBUTE_2 = "classAttribute2";
	private static final String CLASS_ATTRIBUTE_3 = "classAttribute3";
	private static final String CLASS_ATTRIBUTE_4 = "classAttribute4";
	private static final String CLASS_ATTRIBUTE_STRING = "classAttribute1_notBoolean";
	private static final String ASSIGNMENT_DISPLAY_NAME = "displayName";

	@Mock
	private ModelService modelServiceMock;
	@Mock
	private PersistentKeyGenerator cmsPoductAttributeUidGenerator;

	@InjectMocks
	private WileyClassificationServiceImpl wileyClassificationService;

	@Before
	public void setUp() throws Exception
	{
	}

	@Test
	public void getFilteredAssignmentForProducts() throws Exception
	{
		//Given
		ProductFeatureModel featureBooleanAssignment1True = getFeature(true, CLASS_ATTRIBUTE_1,
				ClassificationAttributeTypeEnum.BOOLEAN, StringUtils.EMPTY);
		ProductFeatureModel featureBooleanAssignment2False = getFeature(false, CLASS_ATTRIBUTE_2,
				ClassificationAttributeTypeEnum.BOOLEAN, ASSIGNMENT_DISPLAY_NAME);
		ProductFeatureModel featureBooleanAssignment2True = getFeature(true, CLASS_ATTRIBUTE_2,
				ClassificationAttributeTypeEnum.BOOLEAN, StringUtils.EMPTY);
		ProductFeatureModel featureBooleanAssignment4True = getFeature(true, CLASS_ATTRIBUTE_4,
				ClassificationAttributeTypeEnum.BOOLEAN, StringUtils.EMPTY);
		ProductFeatureModel featureAssignmentString = getFeature("anyString", CLASS_ATTRIBUTE_STRING,
				ClassificationAttributeTypeEnum.STRING, StringUtils.EMPTY);
		WileyVariantProductModel product1 = new WileyVariantProductModel();
		product1.setFeatures(
				Arrays.asList(featureBooleanAssignment1True, featureBooleanAssignment2False, featureAssignmentString));
		WileyVariantProductModel product2 = new WileyVariantProductModel();
		product2.setFeatures(Arrays
				.asList(featureBooleanAssignment1True, featureBooleanAssignment2True, featureBooleanAssignment4True));

		//When
		Set<ClassAttributeAssignmentModel> filteredAssignments = wileyClassificationService.getFilteredAssignmentForProducts(
				Arrays.asList(product1, product2));

		//Then
		assertEquals(3, filteredAssignments.size());
		verify(featureBooleanAssignment2False.getClassificationAttributeAssignment(), never()).setDisplayName(
				eq(CLASS_ATTRIBUTE_2));
		verify(featureBooleanAssignment1True.getClassificationAttributeAssignment(), atLeastOnce()).setDisplayName(
				eq(CLASS_ATTRIBUTE_1));
		verify(featureBooleanAssignment4True.getClassificationAttributeAssignment(), atLeastOnce()).setDisplayName(
				eq(CLASS_ATTRIBUTE_4));
		assertThat(filteredAssignments, CoreMatchers
				.hasItems(featureBooleanAssignment2True.getClassificationAttributeAssignment(),
						featureBooleanAssignment1True.getClassificationAttributeAssignment(),
						featureBooleanAssignment4True.getClassificationAttributeAssignment()));
	}

	@Test
	public void getFilteredAssignmentForProductWhenProductFeatureEmpty() throws Exception
	{
		WileyVariantProductModel product1 = new WileyVariantProductModel();
		WileyVariantProductModel product2 = new WileyVariantProductModel();

		Set<ClassAttributeAssignmentModel> filteredAssignments = wileyClassificationService.getFilteredAssignmentForProducts(
				Arrays.asList(product1, product2));
		assertEquals(0, filteredAssignments.size());
	}

	@Test
	public void getFilteredAssignmentForProductWhenProductListEmpty() throws Exception
	{
		Set<ClassAttributeAssignmentModel> filteredAssignments = wileyClassificationService.getFilteredAssignmentForProducts(
				Collections.emptyList());
		assertEquals(0, filteredAssignments.size());
	}

	@Test
	public void mergeAssigmentsWithCMSProductAttributes() throws Exception
	{
		//Given
		ClassAttributeAssignmentModel assignment1 = getClassAttributeAssignment(getClassAttribute(CLASS_ATTRIBUTE_1),
				StringUtils.EMPTY,
				ClassAttributeAssignmentTypeEnum.INCLUDES,
				ClassificationAttributeTypeEnum.BOOLEAN);
		ClassAttributeAssignmentModel assignment2 = getClassAttributeAssignment(getClassAttribute(CLASS_ATTRIBUTE_2),
				"displayName",
				ClassAttributeAssignmentTypeEnum.INCLUDES,
				ClassificationAttributeTypeEnum.BOOLEAN);
		ClassAttributeAssignmentModel assignment3 = getClassAttributeAssignment(getClassAttribute(CLASS_ATTRIBUTE_3),
				StringUtils.EMPTY,
				ClassAttributeAssignmentTypeEnum.INCLUDES,
				ClassificationAttributeTypeEnum.BOOLEAN);

		Set<ClassAttributeAssignmentModel> filteredAssignments = new HashSet<>();
		filteredAssignments.add(assignment1);
		filteredAssignments.add(assignment2);

		CMSProductAttributeModel cmsAttributeInSet = getCmsAttributeMock(assignment1);
		CMSProductAttributeModel cmsAttributeNotInSet = getCmsAttributeMock(assignment3);

		Set<CMSProductAttributeModel> attributesForRemoval = new HashSet<CMSProductAttributeModel>();
		attributesForRemoval.add(cmsAttributeNotInSet);

		VariantProductComparisonTableComponentModel variantProductComparisonTableComponentModel = mock(
				VariantProductComparisonTableComponentModel.class);
		when(variantProductComparisonTableComponentModel.getCmsProductAttributes()).thenReturn(
				Arrays.asList(cmsAttributeInSet, cmsAttributeNotInSet));

		doNothing().when(modelServiceMock).removeAll(any(HashSet.class));
		when(modelServiceMock.create(CMSProductAttributeModel.class)).thenReturn(new CMSProductAttributeModel());
		when(cmsPoductAttributeUidGenerator.generate()).thenReturn("uid");
		doNothing().when(modelServiceMock).remove(any(CMSProductAttributeModel.class));
		doNothing().when(modelServiceMock).remove(any(ClassAttributeAssignmentModel.class));

		//When
		wileyClassificationService.mergeAssigmentsWithCMSProductAttributes(filteredAssignments,
				variantProductComparisonTableComponentModel);

		//Then
		verify(modelServiceMock, times(1)).removeAll(eq(attributesForRemoval));
		final ArgumentMatcher<CMSProductAttributeModel> matcher = new ArgumentMatcher<CMSProductAttributeModel>()
		{

			@Override
			public boolean matches(final Object argument)
			{
				if (argument instanceof CMSProductAttributeModel)
				{
					CMSProductAttributeModel cmsAttribute = (CMSProductAttributeModel) argument;
					if (cmsAttribute.getAssignment() != assignment2)
					{
						return false;
					}
				}
				return true;
			}
		};
		verify(modelServiceMock, times(2)).save(any());
		verify(modelServiceMock, atLeastOnce()).save(argThat(matcher));
		verify(modelServiceMock, atLeastOnce()).save(eq(assignment2));
	}

	private ProductFeatureModel getFeature(final Object fetureValue, final String attributeName,
			final ClassificationAttributeTypeEnum attributeType, final String attributeAssignmentDisplayName)
	{
		ProductFeatureModel feature = mock(ProductFeatureModel.class);
		when(feature.getValue()).thenReturn(fetureValue);
		ClassAttributeAssignmentModel classAttributeAssignment = getClassAttributeAssignment(getClassAttribute(attributeName),
				attributeAssignmentDisplayName,
				ClassAttributeAssignmentTypeEnum.INCLUDES,
				attributeType);
		when(feature.getClassificationAttributeAssignment()).thenReturn(classAttributeAssignment);
		return feature;
	}

	private ClassAttributeAssignmentModel getClassAttributeAssignment(final ClassificationAttributeModel classAttribute,
			final String attributeAssignmentDisplayName, final ClassAttributeAssignmentTypeEnum type,
			final ClassificationAttributeTypeEnum attributeType)
	{
		ClassAttributeAssignmentModel classAttributeAssignment = mock(ClassAttributeAssignmentModel.class);
		when(classAttributeAssignment.getClassificationAttribute()).thenReturn(classAttribute);
		when(classAttributeAssignment.getDisplayName()).thenReturn(attributeAssignmentDisplayName);
		when(classAttributeAssignment.getType()).thenReturn(type);
		when(classAttributeAssignment.getAttributeType()).thenReturn(attributeType);
		doNothing().when(classAttributeAssignment).setDisplayName(any());
		return classAttributeAssignment;
	}

	private ClassificationAttributeModel getClassAttribute(final String attributeName)
	{
		ClassificationAttributeModel classAttribute = mock(ClassificationAttributeModel.class);
		when(classAttribute.getName()).thenReturn(attributeName);
		return classAttribute;
	}

	private CMSProductAttributeModel getCmsAttributeMock(final ClassAttributeAssignmentModel attribute)
	{
		CMSProductAttributeModel cmsAttribute = mock(CMSProductAttributeModel.class);
		when(cmsAttribute.getAssignment()).thenReturn(attribute);
		return cmsAttribute;
	}
}