package com.wiley.core.partner.impl;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.KpmgOfficeModel;
import com.wiley.core.partner.WileyKPMGOfficeDao;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyKPMGEnrollmentServiceImplTest
{
	private static final String PARTNER_ID = "FGCU";
	private static final String OFFICE_CODE = "485948";

	@InjectMocks
	private WileyKPMGEnrollmentServiceImpl testedInstance = new WileyKPMGEnrollmentServiceImpl();

	@Mock
	private WileyKPMGOfficeDao wileyKpmgOfficeDao;
	@Mock
	private KpmgOfficeModel officeModel;

	@Test
	public void shouldReturnOfficesByPartnerId()
	{
		//Given
		when(wileyKpmgOfficeDao.findOffices(PARTNER_ID)).thenReturn(Arrays.asList(officeModel));
		//When
		final List<KpmgOfficeModel> offices = testedInstance.getOffices(PARTNER_ID);
		//Then
		assertEquals(Arrays.asList(officeModel), offices);
	}

	@Test
	public void shouldReturnOfficesByCode()
	{
		//Given
		when(wileyKpmgOfficeDao.findOfficeByCode(OFFICE_CODE)).thenReturn(officeModel);
		//When
		final KpmgOfficeModel office = testedInstance.getOfficeByCode(OFFICE_CODE);
		//Then
		assertEquals(officeModel, office);
	}

}