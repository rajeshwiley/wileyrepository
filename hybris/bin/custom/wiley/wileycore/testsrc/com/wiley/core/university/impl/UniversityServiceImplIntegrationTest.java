package com.wiley.core.university.impl;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.model.UniversityModel;
import com.wiley.core.university.services.UniversityService;
import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.not;
import static org.hamcrest.beans.HasPropertyWithValue.hasProperty;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.hamcrest.core.IsCollectionContaining.hasItem;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;


/**
 * Unit test suite for {@link com.wiley.core.university.services.impl.UniversityServiceImpl}
 */
@IntegrationTest
public class UniversityServiceImplIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{

	public static final String TEST_UNIVERSITY_CODE = "3";
	public static final String TEST_UNIVERSITY_NAME = "University of Alabama at Birmingham";
	public static final String TEST_INACTIVE_UNIVERSITY_NAME = "University of Alabama at TEST";
	public static final String TEST_UNIVERSITY_CODE_NOT_EXISTING = "99999";

	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Resource
	private UniversityService wileyUniversityService;


	/**
	 * Sets up.
	 *
	 * @throws Exception
	 * 		the exception
	 */
	@Before
	public void setUp() throws Exception
	{
		importCsv("/wileycore/test/coredata/common/universities.impex", "utf-8");
	}

	@Test
	public void testGetUniversityByCode()
	{
		Optional<UniversityModel> universityModelReturned = wileyUniversityService.getUniversityByCode(TEST_UNIVERSITY_CODE);
		assertEquals(TEST_UNIVERSITY_NAME, universityModelReturned.get().getName());
	}

	@Test
	public void testGetUniversityByCodeNotExisting()
	{
		Optional<UniversityModel> universityModelReturned = wileyUniversityService
				.getUniversityByCode(TEST_UNIVERSITY_CODE_NOT_EXISTING);
		assertEquals(Optional.empty(), universityModelReturned);
	}

	@Test
	public void testGetUniversitiesByRegion()
	{
		RegionModel regionModel = new RegionModel();
		regionModel.setIsocodeShort("AL");

		List<UniversityModel> result = wileyUniversityService.getUniversitiesByRegion(regionModel);

		assertThat(result, containsInAnyOrder(
				hasProperty("code", equalTo("1")),
				hasProperty("code", equalTo("2")),
				hasProperty("code", equalTo("3"))));
		// redundant but still descriptive
		assertThat(result, not(hasItem(hasProperty("code", equalTo("6")))));
	}
}
