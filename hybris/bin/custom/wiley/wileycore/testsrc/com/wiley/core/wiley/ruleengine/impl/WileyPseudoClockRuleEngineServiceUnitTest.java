package com.wiley.core.wiley.ruleengine.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.ruleengine.RuleEvaluationContext;
import de.hybris.platform.ruleengine.model.DroolsRuleEngineContextModel;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.drools.core.command.runtime.AdvanceSessionTimeCommand;
import org.drools.core.command.runtime.BatchExecutionCommandImpl;
import org.drools.core.command.runtime.rule.FireAllRulesCommand;
import org.drools.core.command.runtime.rule.InsertElementsCommand;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kie.api.command.BatchExecutionCommand;
import org.kie.api.command.Command;
import org.kie.api.runtime.rule.AgendaFilter;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.Sets;
import com.wiley.ruleengine.model.WileyDroolsRuleEngineContextModel;


/**
 * @author Maksim_Kozich
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyPseudoClockRuleEngineServiceUnitTest
{
	private static final long DATE_TIME = 12345L;
	private static final String RULE_EVALUATION_CONTEXT_FACT = "some context fact";

	private final Set<Object> ruleEvaluationContextFacts = Sets.newHashSet(RULE_EVALUATION_CONTEXT_FACT);

	@Mock
	private Date dateMock;
	@Mock
	private AgendaFilter agendaFilterMock;
	@Mock
	private RuleEvaluationContext ruleEvaluationContextMock;
	@Mock
	private DroolsRuleEngineContextModel droolsRuleEngineContextModelMock;
	@Mock
	private WileyDroolsRuleEngineContextModel wileyDroolsRuleEngineContextModelMock;
	@InjectMocks
	private WileyPseudoClockRuleEngineService testInstance;

	@Test
	public void testGetCommandObjectReturnsBatchCommandWithTimeCommandForWileyDroolsContext()
	{
		// given
		when(dateMock.getTime()).thenReturn(DATE_TIME);
		when(ruleEvaluationContextMock.getFacts()).thenReturn(ruleEvaluationContextFacts);
		when(wileyDroolsRuleEngineContextModelMock.getDate()).thenReturn(dateMock);

		// when
		final Object result = testInstance.getCommandObject(ruleEvaluationContextMock, wileyDroolsRuleEngineContextModelMock,
				null);

		// then
		assertTrue(result instanceof BatchExecutionCommand);

		final List<Command> capturedCommands = ((BatchExecutionCommandImpl) result).getCommands();

		final Command firstCommand = capturedCommands.get(0);
		assertTrue(firstCommand instanceof AdvanceSessionTimeCommand);
		final AdvanceSessionTimeCommand advanceSessionTimeCommand = (AdvanceSessionTimeCommand) firstCommand;
		assertEquals(DATE_TIME, advanceSessionTimeCommand.getAmount());
		assertEquals(TimeUnit.MILLISECONDS, advanceSessionTimeCommand.getUnit());

		final Command secondCommand = capturedCommands.get(1);
		assertTrue(secondCommand instanceof InsertElementsCommand);
		final InsertElementsCommand insertElementsCommand = (InsertElementsCommand) secondCommand;
		assertSame(ruleEvaluationContextFacts, insertElementsCommand.getObjects());
	}

	@Test
	public void testGetCommandObjectReturnsBatchCommandWithTimeCommandForWileyDroolsContextAndAgendaFilter()
	{
		// given
		when(dateMock.getTime()).thenReturn(DATE_TIME);
		when(wileyDroolsRuleEngineContextModelMock.getDate()).thenReturn(dateMock);
		when(ruleEvaluationContextMock.getFacts()).thenReturn(ruleEvaluationContextFacts);

		// when
		final Object result = testInstance.getCommandObject(ruleEvaluationContextMock, wileyDroolsRuleEngineContextModelMock,
				agendaFilterMock);

		// then
		assertTrue(result instanceof BatchExecutionCommandImpl);

		final List<Command> capturedCommands = ((BatchExecutionCommandImpl) result).getCommands();
		assertEquals(3, capturedCommands.size());

		final Command firstCommand = capturedCommands.get(0);
		assertTrue(firstCommand instanceof AdvanceSessionTimeCommand);
		final AdvanceSessionTimeCommand advanceSessionTimeCommand = (AdvanceSessionTimeCommand) firstCommand;
		assertEquals(DATE_TIME, advanceSessionTimeCommand.getAmount());
		assertEquals(TimeUnit.MILLISECONDS, advanceSessionTimeCommand.getUnit());

		final Command secondCommand = capturedCommands.get(1);
		assertTrue(secondCommand instanceof InsertElementsCommand);
		final InsertElementsCommand insertElementsCommand = (InsertElementsCommand) secondCommand;
		assertSame(ruleEvaluationContextFacts, insertElementsCommand.getObjects());

		final Command thirdCommand = capturedCommands.get(2);
		assertTrue(thirdCommand instanceof FireAllRulesCommand);
		final FireAllRulesCommand fireAllRulesCommand = (FireAllRulesCommand) thirdCommand;
		assertEquals(agendaFilterMock, fireAllRulesCommand.getAgendaFilter());
	}

	@Test
	public void testGetCommandObjectReturnsBatchCommandWithTimeCommandForDroolsContext()
	{
		// given
		when(ruleEvaluationContextMock.getFacts()).thenReturn(ruleEvaluationContextFacts);

		// when
		final BatchExecutionCommand result = testInstance.getCommandObject(ruleEvaluationContextMock,
				droolsRuleEngineContextModelMock, null);

		// then
		assertTrue(result instanceof BatchExecutionCommandImpl);

		final BatchExecutionCommandImpl batchExecutionCommand = (BatchExecutionCommandImpl) result;
		final InsertElementsCommand insertElementsCommand = findInsertElementsCommand(batchExecutionCommand.getCommands());
		assertNotNull(insertElementsCommand);
		assertSame(ruleEvaluationContextFacts, insertElementsCommand.getObjects());
	}

	@Test
	public void testGetCommandObjectReturnsBatchCommandWithTimeCommandForDroolsContextAndAgendaFilter()
	{
		// given
		when(ruleEvaluationContextMock.getFacts()).thenReturn(ruleEvaluationContextFacts);

		// when
		final Object result = testInstance.getCommandObject(ruleEvaluationContextMock, droolsRuleEngineContextModelMock,
				agendaFilterMock);

		// then
		assertTrue(result instanceof BatchExecutionCommand);

		final List<Command> capturedCommands = ((BatchExecutionCommandImpl) result).getCommands();
		assertEquals(2, capturedCommands.size());

		final Command firstCommand = capturedCommands.get(0);
		assertTrue(firstCommand instanceof InsertElementsCommand);
		final InsertElementsCommand insertElementsCommand = (InsertElementsCommand) firstCommand;
		assertSame(ruleEvaluationContextFacts, insertElementsCommand.getObjects());

		final Command secondCommand = capturedCommands.get(1);
		assertTrue(secondCommand instanceof FireAllRulesCommand);
		FireAllRulesCommand fireAllRulesCommand = (FireAllRulesCommand) secondCommand;
		assertEquals(agendaFilterMock, fireAllRulesCommand.getAgendaFilter());
	}

	private InsertElementsCommand findInsertElementsCommand(final Collection<Command> commands)
	{
		for (Command command : commands)
		{
			if (command instanceof InsertElementsCommand)
			{
				return (InsertElementsCommand) command;
			}
		}

		return null;
	}
}