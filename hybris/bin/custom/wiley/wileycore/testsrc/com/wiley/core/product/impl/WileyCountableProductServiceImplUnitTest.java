package com.wiley.core.product.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantProductModel;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyCountableProductServiceImplUnitTest
{
	@InjectMocks
	private WileyCountableProductServiceImpl wileyCountableProductServiceImpl;
	@Mock
	private ProductModel baseProductMock;
	@Mock
	private VariantProductModel variantProductMock;


	@Before
	public void setUp()
	{
		when(variantProductMock.getBaseProduct()).thenReturn(baseProductMock);
	}

	@Test
	public void shouldReturnTrueIfVariantIsCountable()
	{
		when(baseProductMock.getCountable()).thenReturn(false);
		when(variantProductMock.getCountable()).thenReturn(true);

		boolean actual = wileyCountableProductServiceImpl.doCanProductHaveQuantity(variantProductMock);

		assertTrue(actual);
	}

	@Test
	public void shouldReturnTrueIfVariantCountabilityIsNotSpecifiedBaseIsCountable()
	{
		when(baseProductMock.getCountable()).thenReturn(true);
		when(variantProductMock.getCountable()).thenReturn(null);

		boolean actual = wileyCountableProductServiceImpl.doCanProductHaveQuantity(variantProductMock);

		assertTrue(actual);
	}

	@Test
	public void shouldReturnTrueIfVariantAndBaseCountabilityIsNotSpecified()
	{
		when(baseProductMock.getCountable()).thenReturn(null);
		when(variantProductMock.getCountable()).thenReturn(null);

		boolean actual = wileyCountableProductServiceImpl.doCanProductHaveQuantity(variantProductMock);

		assertTrue(actual);
	}

	@Test
	public void shouldReturnFalseIfVariantIsNotCountable()
	{
		when(variantProductMock.getCountable()).thenReturn(false);

		boolean actual = wileyCountableProductServiceImpl.doCanProductHaveQuantity(variantProductMock);

		assertFalse(actual);
	}

	@Test
	public void shouldReturnFalseIfVariantCountabilityIsNotSpecifiedBaseIsNotCountable()
	{
		when(baseProductMock.getCountable()).thenReturn(false);
		when(variantProductMock.getCountable()).thenReturn(null);

		boolean actual = wileyCountableProductServiceImpl.doCanProductHaveQuantity(variantProductMock);

		assertFalse(actual);
	}
}