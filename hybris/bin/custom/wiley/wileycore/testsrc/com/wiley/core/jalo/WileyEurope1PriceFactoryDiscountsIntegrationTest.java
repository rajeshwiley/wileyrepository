package com.wiley.core.jalo;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.order.data.ProductDiscountParameter;
import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.europe1.enums.UserDiscountGroup;
import de.hybris.platform.europe1.model.AbstractDiscountRowModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.jalo.order.AbstractOrderEntry;
import de.hybris.platform.jalo.order.price.JaloPriceFactoryException;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.util.DiscountValue;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;


/**
 * Contains test to check methods for getting discounts.
 */
@IntegrationTest
public class WileyEurope1PriceFactoryDiscountsIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{

	public static final String USER_UID = "test@hybris.com";
	public static final String CART_CODE = "test-cart1";
	public static final String BASE_SITE_WEL = "wel";

	@Resource(name = "wileyPriceFactory")
	private WileyEurope1PriceFactory wileyEurope1PriceFactory;

	@Resource
	private CommerceCartService commerceCartService;

	@Resource
	private UserService userService;

	@Resource
	private ModelService modelService;

	@Resource
	private ProductService productService;

	@Resource
	private CommerceCommonI18NService commerceCommonI18NService;

	@Resource
	private BaseSiteService baseSiteService;


	@Before
	public void setUp() throws Exception
	{
		baseSiteService.setCurrentBaseSite(BASE_SITE_WEL, true);
	}

	@Test
	public void shouldFindDiscountsAccordingToOrderDiscountGroup() throws JaloPriceFactoryException, ImpExException
	{
		// Given
		importCsv("/wileycore/test/product/WileyEurope1PriceFactoryDiscountsIntegrationTest/defaultStudentDiscount.impex",
				DEFAULT_ENCODING);
		importCsv("/wileycore/test/product/WileyEurope1PriceFactoryDiscountsIntegrationTest/testUser.impex", DEFAULT_ENCODING);
		importCsv("/wileycore/test/product/WileyEurope1PriceFactoryDiscountsIntegrationTest/cartWithStudentDiscount.impex",
				DEFAULT_ENCODING);

		final UserModel user = userService.getUserForUID(USER_UID);
		final CartModel cart = commerceCartService.getCartForCodeAndUser(CART_CODE, user);

		// When
		final List<DiscountValue> discountValues = wileyEurope1PriceFactory.getDiscountValues(
				(AbstractOrderEntry) modelService.getSource(cart.getEntries().get(0)));

		// Then
		assertNotNull("Should return empty collection.", discountValues);
		assertEquals("List should be expected size.", 1, discountValues.size());
		assertTrue("List of discount values should contain expected discount.",
				containsDiscount(discountValues, Double.valueOf(20), false));
	}

	@Test
	public void shouldReturnDiscountValuesForProductAndDiscountGroup() throws ImpExException, JaloPriceFactoryException
	{
		// Given
		importCsv("/wileycore/test/product/WileyEurope1PriceFactoryDiscountsIntegrationTest/testUser.impex", DEFAULT_ENCODING);
		importCsv("/wileycore/test/product/WileyEurope1PriceFactoryDiscountsIntegrationTest/defaultStudentDiscount.impex",
				DEFAULT_ENCODING);
		importCsv("/wileycore/test/product/WileyEurope1PriceFactoryDiscountsIntegrationTest/defaultSampleDiscount.impex",
				DEFAULT_ENCODING);

		ProductModel product = productService.getProductForCode("WEL_CPA_PLATINUM_AUD_EBOOK");
		UserModel user = userService.getUserForUID(USER_UID);
		CurrencyModel currency = commerceCommonI18NService.getDefaultCurrency();
		final Date now = new Date();
		Boolean isNet = false;

		ProductDiscountParameter parameter = new ProductDiscountParameter();
		parameter.setProduct(product);
		parameter.setUserDiscountGroup(UserDiscountGroup.STUDENT);
		parameter.setUser(user);
		parameter.setCurrency(currency);
		parameter.setDate(now);
		parameter.setNet(isNet);
		parameter.setAdditionalFilter(Optional.empty());

		// When
		final List<DiscountValue> discountValues = wileyEurope1PriceFactory.getProductDiscountValues(parameter);

		// Then
		assertNotNull("Should return empty collection.", discountValues);
		assertEquals("List should be expected size.", 2, discountValues.size());
		// check student discount
		assertTrue("List of discount values should contain expected discount.",
				containsDiscount(discountValues, Double.valueOf(20), false));
		// check common discount
		assertTrue("List of discount values should contain expected discount.",
				containsDiscount(discountValues, Double.valueOf(50), false));
	}

	@Test
	public void shouldFilterReturnedDiscountValuesByPredicate() throws ImpExException, JaloPriceFactoryException
	{
		// Given
		importCsv("/wileycore/test/product/WileyEurope1PriceFactoryDiscountsIntegrationTest/testUser.impex", DEFAULT_ENCODING);
		importCsv("/wileycore/test/product/WileyEurope1PriceFactoryDiscountsIntegrationTest/defaultStudentDiscount.impex",
				DEFAULT_ENCODING);
		importCsv("/wileycore/test/product/WileyEurope1PriceFactoryDiscountsIntegrationTest/defaultSampleDiscount.impex",
				DEFAULT_ENCODING);

		ProductModel product = productService.getProductForCode("WEL_CPA_PLATINUM_AUD_EBOOK");
		UserModel user = userService.getUserForUID(USER_UID);
		CurrencyModel currency = commerceCommonI18NService.getDefaultCurrency();
		final Date now = new Date();
		Boolean isNet = false;

		Predicate<AbstractDiscountRowModel> filter = discountRowModel -> UserDiscountGroup.STUDENT.equals(
				discountRowModel.getUg());

		ProductDiscountParameter parameter = new ProductDiscountParameter();
		parameter.setProduct(product);
		parameter.setUserDiscountGroup(UserDiscountGroup.STUDENT);
		parameter.setUser(user);
		parameter.setCurrency(currency);
		parameter.setDate(now);
		parameter.setNet(isNet);
		parameter.setAdditionalFilter(Optional.of(filter));

		// When
		final List<DiscountValue> discountValues = wileyEurope1PriceFactory.getProductDiscountValues(parameter);

		// Then
		assertNotNull("Should return empty collection.", discountValues);
		assertEquals("List should be expected size.", 1, discountValues.size());
		// check student discount
		assertTrue("List of discount values should contain expected discount.",
				containsDiscount(discountValues, Double.valueOf(20), false));
	}

	private boolean containsDiscount(final List<DiscountValue> discountValues, final Double value, boolean isAbsolute)
	{
		return discountValues.stream().anyMatch(discountValue ->
				value.equals(discountValue.getValue()) && isAbsolute == discountValue.isAbsolute()
		);
	}
}