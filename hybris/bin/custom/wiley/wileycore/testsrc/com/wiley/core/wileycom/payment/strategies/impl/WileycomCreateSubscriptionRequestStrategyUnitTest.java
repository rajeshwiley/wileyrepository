package com.wiley.core.wileycom.payment.strategies.impl;

import com.wiley.core.order.WileyCartService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionRequest;
import de.hybris.platform.acceleratorservices.payment.data.CustomerBillToData;
import de.hybris.platform.acceleratorservices.payment.data.CustomerShipToData;
import de.hybris.platform.acceleratorservices.payment.data.OrderInfoData;
import de.hybris.platform.acceleratorservices.payment.data.PaymentInfoData;
import de.hybris.platform.commerceservices.customer.CustomerEmailResolutionService;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Test for {@link WileycomCreateSubscriptionRequestStrategy}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileycomCreateSubscriptionRequestStrategyUnitTest
{

	private static final Date DESIRED_SHIPPING_DATE = new Date();

	@InjectMocks
	private WileycomCreateSubscriptionRequestStrategy wileycomCreateSubscriptionRequestStrategy;

	@Mock
	private Converter<AddressModel, CustomerBillToData> customerBillToDataConverter;

	@Mock
	private Converter<CartModel, CustomerShipToData> customerShipToDataConverter;

	@Mock
	private Converter<CreditCardPaymentInfoModel, PaymentInfoData> paymentInfoDataConverter;

	@Mock
	private CustomerEmailResolutionService customerEmailResolutionServiceMock;

	@Mock
	private SiteConfigService siteConfigService;

	@Mock
	private CartModel mockCartModel;

	@Mock
	private CurrencyModel mockCurrencyModel;

	@Mock
	private CreditCardPaymentInfoModel mockCreditCardPaymentInfoModel;

	@Mock
	private Converter<AddressModel, CustomerBillToData> mockCustomerBillToDataConverter;

	@Mock
	private Converter<CreditCardPaymentInfoModel, PaymentInfoData> mockPaymentInfoDataConverter;

	@Mock
	private PaymentInfoData mockPaymentInfoData;

	@Mock
	private CustomerBillToData mockCustomerBillToData;

	@Mock
	private CreateSubscriptionRequest mockRequest;

	@Mock
	private DeliveryModeModel mockDeliveryModeModel;

	@Mock
	private OrderInfoData mockOrderInfoData;

	@Mock
	private WileyCartService mockWileyCartService;

	@Before
	public void setup()
	{
		//service setup
		when(mockWileyCartService.getSessionCart()).thenReturn(mockCartModel);

		final CustomerBillToData customerBillToDataMock = mock(CustomerBillToData.class);
		when(customerBillToDataMock.getBillToEmail()).thenReturn("SomeTestEmail@gmail.com");
		when(customerBillToDataConverter.convert(any(AddressModel.class))).thenReturn(customerBillToDataMock);

		//dto
		when(mockRequest.getOrderInfoData()).thenReturn(mockOrderInfoData);

		when(mockCartModel.getCurrency()).thenReturn(mockCurrencyModel);
		when(mockCurrencyModel.getIsocode()).thenReturn("USD");

		when(customerEmailResolutionServiceMock.getEmailForCustomer(any(CustomerModel.class))).thenReturn(
				"SomeTestEmail@gmail.com");
	}


	@Test
	public void shouldSetupRequestCreditCardInfo()
	{
		//Given
		when(mockCustomerBillToDataConverter.convert(any())).thenReturn(mockCustomerBillToData);
		when(mockPaymentInfoDataConverter.convert(any())).thenReturn(mockPaymentInfoData);
		wileycomCreateSubscriptionRequestStrategy.setCustomerBillToDataConverter(mockCustomerBillToDataConverter);
		wileycomCreateSubscriptionRequestStrategy.setPaymentInfoDataConverter(mockPaymentInfoDataConverter);
		when(mockCartModel.getPaymentInfo()).thenReturn(mockCreditCardPaymentInfoModel);


		//When
		CreateSubscriptionRequest formedRequest = wileycomCreateSubscriptionRequestStrategy.createSubscriptionRequest("",
				"", "", "", mock(CustomerModel.class), mock(CreditCardPaymentInfoModel.class), mock(AddressModel.class));

		//Then
		assertEquals(mockPaymentInfoData, formedRequest.getPaymentInfoData());
		assertEquals(mockCustomerBillToData, formedRequest.getCustomerBillToData());

	}

	@Test
	public void shouldSetupDesiredShippingDate()
	{
		//Given
		when(mockCartModel.getDesiredShippingDate()).thenReturn(DESIRED_SHIPPING_DATE);


		//When
		CreateSubscriptionRequest formedRequest = wileycomCreateSubscriptionRequestStrategy.createSubscriptionRequest("",
				"", "", "", mock(CustomerModel.class), mock(CreditCardPaymentInfoModel.class), mock(AddressModel.class));

		//Then
		assertEquals(DESIRED_SHIPPING_DATE, formedRequest.getOrderInfoData().getDesiredShippingDate());
	}

}
