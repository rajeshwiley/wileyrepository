package com.wiley.core.partner.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.UserGroupModel;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyPartnerCompanyModel;
import com.wiley.core.order.WileyFindProductsInCartService;
import com.wiley.core.partner.WileyPartnerCompanyDao;

import static junit.framework.Assert.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyPartnerServiceImplTest
{
	private static final String PARTNER_ID = "partnerId";
	private static final String EMPTY_PARTNER_ID = "";
	private static final String CATEGORY_CODE = "testCategoryCode";

	@InjectMocks
	private WileyPartnerServiceImpl testedInstance = new WileyPartnerServiceImpl();
	@Mock
	private WileyPartnerCompanyDao wileyPartnerCompanyDao;
	@Mock
	private WileyFindProductsInCartService wileyFindProductsInCartService;
	@Mock
	private WileyPartnerCompanyModel partnerCompany;
	@Mock
	private UserGroupModel userGroup;
	@Mock
	private AbstractOrderModel orderMock;
	@Mock
	private CategoryModel partnerCategory;

	@Test
	public void shouldReturnTrueIfCartContainsPartnerProduct()
	{
		when(wileyFindProductsInCartService.containsAtLeastOneProduct(orderMock, partnerCategory)).thenReturn(true);
		when(partnerCompany.getPartnerCategories()).thenReturn(Collections.singleton(partnerCategory));

		final boolean containsProduct = testedInstance.orderContainsPartnerProduct(partnerCompany, orderMock);

		assertTrue(containsProduct);
	}

	@Test
	public void shouldReturnFalseIfCartContainsNoPartnerProducts()
	{
		when(wileyFindProductsInCartService.containsAtLeastOneProduct(orderMock, partnerCategory)).thenReturn(false);
		when(partnerCompany.getPartnerCategories()).thenReturn(Collections.singleton(partnerCategory));

		final boolean containsProduct = testedInstance.orderContainsPartnerProduct(partnerCompany, orderMock);

		assertFalse(containsProduct);
	}

	@Test
	public void shouldReturnFalseIfPartnerCompanyIsNull()
	{
		WileyPartnerCompanyModel wileyPartnerCompanyModel = null;

		final boolean containsProduct = testedInstance.orderContainsPartnerProduct(wileyPartnerCompanyModel, orderMock);

		assertFalse(containsProduct);
	}

	@Test
	public void shouldReturnFalseIfPartnerCategoriesIsEmpty()
	{
		when(partnerCompany.getPartnerCategories()).thenReturn(Collections.emptySet());

		final boolean containsProduct = testedInstance.orderContainsPartnerProduct(partnerCompany, orderMock);

		assertFalse(containsProduct);
	}

	@Test
	public void shouldReturnFalseIfPartnerCategoriesIsNull()
	{
		when(partnerCompany.getPartnerCategories()).thenReturn(null);

		final boolean containsProduct = testedInstance.orderContainsPartnerProduct(partnerCompany, orderMock);

		assertFalse(containsProduct);
	}

	@Test
	public void shouldReturnWileyPartnerCompany()
	{
		when(wileyPartnerCompanyDao.findUserGroupByUid(PARTNER_ID)).thenReturn(partnerCompany);

		final WileyPartnerCompanyModel partnerCompany = testedInstance.getPartnerById(PARTNER_ID);

		assertEquals(this.partnerCompany, partnerCompany);
	}

	@Test
	public void shouldReturnNullIfPartnerCompanyDoesNotExist()
	{
		when(wileyPartnerCompanyDao.findUserGroupByUid(PARTNER_ID)).thenReturn(null);

		final WileyPartnerCompanyModel partnerCompany = testedInstance.getPartnerById(PARTNER_ID);

		assertNull(partnerCompany);
	}

	@Test
	public void shouldReturnNullIfPartnerCompanyHasWrongType()
	{
		when(wileyPartnerCompanyDao.findUserGroupByUid(PARTNER_ID)).thenReturn(userGroup);

		final WileyPartnerCompanyModel partnerCompany = testedInstance.getPartnerById(PARTNER_ID);

		assertNull(partnerCompany);
	}

	@Test
	public void shouldReturnListOfPartnersByCategoryCode()
	{
		when(wileyPartnerCompanyDao.getPartnersByCategory(CATEGORY_CODE))
				.thenReturn(Arrays.asList(partnerCompany));

		final List<WileyPartnerCompanyModel> partners = testedInstance.getPartnersByCategory(CATEGORY_CODE);

		assertNotNull(partners);
		assertEquals("Should return list of partner company models", Arrays.asList(partnerCompany), partners);
	}
	
	@Test
	public void shoudReturnPartnerIdAsCodeWhenPartnerExists()
	{
		// Given
		WileyPartnerCompanyModel testPartner = new WileyPartnerCompanyModel();
		testPartner.setUid(PARTNER_ID);
		when(orderMock.getWileyPartner()).thenReturn(testPartner);
	
		// When
		final Optional<String> partnerCode = testedInstance.getPartnerCode(orderMock);

		// Then
		assertTrue(partnerCode.isPresent());
		assertEquals(partnerCode.get(), PARTNER_ID);
	}
	
	@Test
	public void shoudReturnEmptyAsPartnerCodeWhenPartnerNotExists()
	{
		// Given
		// the order has no partner
		
		// When
	 	final Optional<String> partnerCode = testedInstance.getPartnerCode(orderMock);
	
		// Then
		assertFalse(partnerCode.isPresent());
	}

	@Test
	public void shouldReturnNullIfPartnerIdIsEmptyString()
	{
		// When
		WileyPartnerCompanyModel partnerById = testedInstance.getPartnerById(EMPTY_PARTNER_ID);

		// Then
		assertNull("Partner should be null in case of empty string passed", partnerById);
	}
}