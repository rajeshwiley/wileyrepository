package com.wiley.core.order.impl;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.coupon.WileyCouponService;
import com.wiley.core.model.WileyPartnerCompanyModel;
import com.wiley.core.partner.WileyPartnerService;
import com.wiley.core.product.WileyProductDiscountService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.enums.UserDiscountGroup;
import de.hybris.platform.jalo.order.price.JaloPriceFactoryException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.util.DiscountValue;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyOrderDiscountGroupServiceImplUnitTest
{
	private static final String PARTNER_ID = "partnerId";

	@Spy
	@InjectMocks
	private WileyOrderDiscountGroupServiceImpl wileyOrderDiscountGroupService;

	@Mock
	private ModelService modelServiceMock;
	@Mock
	private WileyCouponService mockCouponService;
	@Mock
	private WileyPartnerService wileyPartnerService;
	@Mock
	private CartModel sessionCartMock;
	@Mock
	private WileyProductDiscountService wileyProductDiscountServiceMock;
	@Mock
	private ProductModel productModelMock1, productModelMock2, productModelMock3;
	@Mock
	private DiscountValue discountValue1, discountValue2, discountValue3;
	@Mock
	private CartModel cartModelMock;
	@Mock
	private WileyPartnerCompanyModel partnerCompanyMock;

	private final CommerceCartParameter commerceCartParameter = new CommerceCartParameter();

	@Before
	public void setUp()
	{
		when(wileyPartnerService.getPartnerById(PARTNER_ID)).thenReturn(partnerCompanyMock);
		commerceCartParameter.setCart(cartModelMock);
	}

	@Test
	public void hasStudentDiscountProducts1()
	{
		// Given
		List<AbstractOrderEntryModel> abstractOrderEntryModelsMocks = mockCartEntries(
				Arrays.asList(productModelMock1, productModelMock2, productModelMock3));
		when(sessionCartMock.getEntries()).thenReturn(abstractOrderEntryModelsMocks);
		when(wileyProductDiscountServiceMock.getPotentialDiscountForCurrentCustomer(productModelMock1, UserDiscountGroup.STUDENT))
				.thenReturn(Arrays.asList(discountValue1));
		when(wileyProductDiscountServiceMock.getPotentialDiscountForCurrentCustomer(productModelMock2, UserDiscountGroup.STUDENT))
				.thenReturn(new ArrayList<>());
		when(wileyProductDiscountServiceMock.getPotentialDiscountForCurrentCustomer(productModelMock3, UserDiscountGroup.STUDENT))
				.thenReturn(new ArrayList<>());

		// When
		final boolean result = wileyOrderDiscountGroupService.hasStudentDiscountProducts(sessionCartMock);

		// Then
		assertTrue(result);
	}

	@Test
	public void hasStudentDiscountProducts2()
	{
		// Given
		List<AbstractOrderEntryModel> abstractOrderEntryModelsMocks = mockCartEntries(
				Arrays.asList(productModelMock1, productModelMock2, productModelMock3));
		when(sessionCartMock.getEntries()).thenReturn(abstractOrderEntryModelsMocks);
		when(wileyProductDiscountServiceMock.getPotentialDiscountForCurrentCustomer(productModelMock1, UserDiscountGroup.STUDENT))
				.thenReturn(new ArrayList<>());
		when(wileyProductDiscountServiceMock.getPotentialDiscountForCurrentCustomer(productModelMock2, UserDiscountGroup.STUDENT))
				.thenReturn(Arrays.asList(discountValue2));
		when(wileyProductDiscountServiceMock.getPotentialDiscountForCurrentCustomer(productModelMock3, UserDiscountGroup.STUDENT))
				.thenReturn(new ArrayList<>());

		// When
		final boolean result = wileyOrderDiscountGroupService.hasStudentDiscountProducts(sessionCartMock);

		// Then
		assertTrue(result);
	}

	@Test
	public void hasStudentDiscountProducts3()
	{
		// Given
		List<AbstractOrderEntryModel> abstractOrderEntryModelsMocks = mockCartEntries(
				Arrays.asList(productModelMock1, productModelMock2, productModelMock3));
		when(sessionCartMock.getEntries()).thenReturn(abstractOrderEntryModelsMocks);
		when(wileyProductDiscountServiceMock.getPotentialDiscountForCurrentCustomer(productModelMock1, UserDiscountGroup.STUDENT))
				.thenReturn(new ArrayList<>());
		when(wileyProductDiscountServiceMock.getPotentialDiscountForCurrentCustomer(productModelMock2, UserDiscountGroup.STUDENT))
				.thenReturn(new ArrayList<>());
		when(wileyProductDiscountServiceMock.getPotentialDiscountForCurrentCustomer(productModelMock3, UserDiscountGroup.STUDENT))
				.thenReturn(Arrays.asList(discountValue3));

		// When
		final boolean result = wileyOrderDiscountGroupService.hasStudentDiscountProducts(sessionCartMock);

		// Then
		assertTrue(result);
	}

	@Test
	public void hasNoStudentDiscountProducts()
	{
		// Given
		List<AbstractOrderEntryModel> abstractOrderEntryModelsMocks = mockCartEntries(
				Arrays.asList(productModelMock1, productModelMock2, productModelMock3));
		when(sessionCartMock.getEntries()).thenReturn(abstractOrderEntryModelsMocks);
		when(wileyProductDiscountServiceMock.getPotentialDiscountForCurrentCustomer(productModelMock1, UserDiscountGroup.STUDENT))
				.thenReturn(new ArrayList<>());
		when(wileyProductDiscountServiceMock.getPotentialDiscountForCurrentCustomer(productModelMock2, UserDiscountGroup.STUDENT))
				.thenReturn(new ArrayList<>());
		when(wileyProductDiscountServiceMock.getPotentialDiscountForCurrentCustomer(productModelMock3, UserDiscountGroup.STUDENT))
				.thenReturn(new ArrayList<>());

		// When
		final boolean result = wileyOrderDiscountGroupService.hasStudentDiscountProducts(sessionCartMock);

		// Then
		assertFalse(result);
	}

	@Test
	public void shouldApplyStudentDiscountToCartWithStudentDiscount() throws JaloPriceFactoryException
	{
		// Given
		commerceCartParameter.setApplyStudentDiscount(true);
		when(cartModelMock.getDiscountGroup()).thenReturn(UserDiscountGroup.STUDENT);
		cartContainsStudentProducts(true);

		// When
		wileyOrderDiscountGroupService.decideIfDiscountGroupOrVoucherShouldBeApplied(commerceCartParameter);

		// Then
		verifyStudentCouponApplied();
	}

	@Test
	public void shouldApplyStudentDiscountToCartWithoutStudentDiscount() throws JaloPriceFactoryException
	{
		// Given
		commerceCartParameter.setApplyStudentDiscount(true);

		cartContainsStudentProducts(true);

		// When
		wileyOrderDiscountGroupService.decideIfDiscountGroupOrVoucherShouldBeApplied(commerceCartParameter);

		// Then
		verifyStudentCouponApplied();
	}

	@Test
	public void shouldResetStudentDiscountWhenCartHasNoStudentProducts() throws JaloPriceFactoryException
	{
		// Given
		doReturn(UserDiscountGroup.STUDENT).when(cartModelMock).getDiscountGroup();
		cartContainsStudentProducts(false);

		// When
		wileyOrderDiscountGroupService.decideIfDiscountGroupOrVoucherShouldBeApplied(commerceCartParameter);

		// Then
		verifyResetStudentDiscount();
	}

	@Test
	public void shouldNotApplyStudentDiscountIfCartHasNoStudentProducts() throws JaloPriceFactoryException
	{
		// Given
		commerceCartParameter.setApplyStudentDiscount(true);
		cartContainsStudentProducts(false);

		// When
		wileyOrderDiscountGroupService.decideIfDiscountGroupOrVoucherShouldBeApplied(commerceCartParameter);

		// Then
		verifyStudentInfoNotUpdated();
	}

	@Test
	public void shouldNotResetStudentDiscount() throws JaloPriceFactoryException
	{
		// Given
		when(cartModelMock.getDiscountGroup()).thenReturn(UserDiscountGroup.STUDENT);
		cartContainsStudentProducts(true);
		// When
		wileyOrderDiscountGroupService.decideIfDiscountGroupOrVoucherShouldBeApplied(commerceCartParameter);
		// Then
		verifyStudentInfoNotUpdated();
	}

	@Test
	public void shouldApplyPartnerDiscount() throws JaloPriceFactoryException
	{
		// Given
		commerceCartParameter.setPartnerId(PARTNER_ID);
		cartContainsPartnerProducts(true);

		// When
		wileyOrderDiscountGroupService.decideIfDiscountGroupOrVoucherShouldBeApplied(commerceCartParameter);

		// Then
		verifyPartnerCompanySetOnCart();
	}

	@Test
	public void shouldResetPartnerDiscountAfterVoucherIsApplied() throws JaloPriceFactoryException
	{
		// Given
		when(cartModelMock.getDiscountGroup()).thenReturn(UserDiscountGroup.PREFERRED_PARTNER);
		when(cartModelMock.getWileyPartner()).thenReturn(partnerCompanyMock);
		commerceCartParameter.setVoucherApplied(true);
		// When
		wileyOrderDiscountGroupService.decideIfDiscountGroupOrVoucherShouldBeApplied(commerceCartParameter);
		// Then
		verifyResetPartnerDiscount();
	}

	@Test
	public void shouldResetPartnerDiscountIfCartHasNoPartnerProducts() throws JaloPriceFactoryException
	{
		// Given
		when(cartModelMock.getDiscountGroup()).thenReturn(UserDiscountGroup.PREFERRED_PARTNER);
		when(cartModelMock.getWileyPartner()).thenReturn(partnerCompanyMock);
		cartContainsPartnerProducts(false);
		// When
		wileyOrderDiscountGroupService.decideIfDiscountGroupOrVoucherShouldBeApplied(commerceCartParameter);
		// Then
		verifyResetPartnerDiscount();
	}

	@Test
	public void shouldNotApplyPartnerDiscountIfCartHasNoPartnerProducts() throws JaloPriceFactoryException
	{
		// Given
		commerceCartParameter.setPartnerId(PARTNER_ID);
		cartContainsPartnerProducts(false);
		// When
		wileyOrderDiscountGroupService.decideIfDiscountGroupOrVoucherShouldBeApplied(commerceCartParameter);
		// Then
		verifyPartnerInfoNotUpdated();
	}

	@Test
	public void shouldNotResetPartnerDiscount() throws JaloPriceFactoryException
	{
		// Given
		commerceCartParameter.setPartnerId(null);
		when(cartModelMock.getWileyPartner()).thenReturn(partnerCompanyMock);
		when(cartModelMock.getDiscountGroup()).thenReturn(UserDiscountGroup.PREFERRED_PARTNER);
		cartContainsPartnerProducts(true);
		// When
		wileyOrderDiscountGroupService.decideIfDiscountGroupOrVoucherShouldBeApplied(commerceCartParameter);
		// Then
		verifyPartnerInfoNotUpdated();
	}

	@Test
	public void shouldUpdatePartnerRegardlessOfStudentFlag() throws JaloPriceFactoryException
	{
		// Given
		commerceCartParameter.setPartnerId(PARTNER_ID);
		commerceCartParameter.setApplyStudentDiscount(true);
		cartContainsPartnerProducts(true);
		// When
		wileyOrderDiscountGroupService.decideIfDiscountGroupOrVoucherShouldBeApplied(commerceCartParameter);
		// Then
		verifyPartnerCompanySetOnCart();
		verifyStudentInfoNotUpdated();
	}

	@Test
	public void shouldNotApplyStudentDiscountForPartnerDiscountGroup() throws JaloPriceFactoryException
	{
		// Given
		cartContainsStudentProducts(true);
		// When
		wileyOrderDiscountGroupService.decideIfDiscountGroupOrVoucherShouldBeApplied(commerceCartParameter);
		// Then
		verifyPartnerInfoNotUpdated();
		verifyStudentInfoNotUpdated();
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowExceptionWhenCommerceCart()
	{
		wileyOrderDiscountGroupService.decideIfDiscountGroupOrVoucherShouldBeApplied(null);
	}

	private static List<AbstractOrderEntryModel> mockCartEntries(final Collection<ProductModel> products)
	{
		return products.stream().map(product -> mockCartEntry(product)).collect(Collectors.toList());
	}

	private static AbstractOrderEntryModel mockCartEntry(final ProductModel productModel)
	{
		final AbstractOrderEntryModel result = mock(AbstractOrderEntryModel.class);
		when(result.getProduct()).thenReturn(productModel);
		return result;
	}

	private void verifyStudentCouponApplied() throws JaloPriceFactoryException
	{
		verify(cartModelMock, never()).setUniversity(null);
		verify(mockCouponService).redeemCoupon(WileyCoreConstants.STUDENT_COUPON_CODE, cartModelMock);
	}

	private void verifyPartnerCompanySetOnCart() throws JaloPriceFactoryException
	{
		verify(cartModelMock).setWileyPartner(partnerCompanyMock);
		verify(cartModelMock, never()).setWileyPartner(null);
		verify(modelServiceMock, atLeastOnce()).save(cartModelMock);
	}

	private void verifyResetStudentDiscount() throws JaloPriceFactoryException
	{
		verify(cartModelMock).setUniversity(null);
		verify(mockCouponService).releaseCouponCode(WileyCoreConstants.STUDENT_COUPON_CODE, cartModelMock);
		verify(modelServiceMock).save(cartModelMock);
	}

	private void verifyResetPartnerDiscount() throws JaloPriceFactoryException
	{
		verify(cartModelMock).setWileyPartner(null);
		verify(modelServiceMock).save(cartModelMock);
	}

	private void verifyStudentInfoNotUpdated() throws JaloPriceFactoryException
	{
		verify(cartModelMock, never()).setUniversity(any());
		verify(mockCouponService, never()).releaseCouponCode(WileyCoreConstants.STUDENT_COUPON_CODE, cartModelMock);
		verify(mockCouponService, never()).redeemCoupon(WileyCoreConstants.STUDENT_COUPON_CODE, cartModelMock);
	}

	private void verifyPartnerInfoNotUpdated() throws JaloPriceFactoryException
	{
		verify(cartModelMock, never()).setWileyPartner(any());
	}

	private void cartContainsStudentProducts(final boolean hasStudentProducts)
	{
		doReturn(hasStudentProducts).when(wileyOrderDiscountGroupService).hasStudentDiscountProducts(cartModelMock);
	}

	private void cartContainsPartnerProducts(final boolean hasPartnerProducts)
	{
		when(wileyPartnerService.orderContainsPartnerProduct(partnerCompanyMock, cartModelMock)).thenReturn(hasPartnerProducts);
	}
}
