package com.wiley.core.payment.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.payment.cybersource.enums.CardTypeEnum;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.acceleratorservices.payment.data.WPGHttpValidateResultData;
import de.hybris.platform.acceleratorservices.payment.enums.DecisionsEnum;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.payment.enums.WPGCreditCardType;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNull;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PaymentInfoWPGHttpAuthoriseResultPopulatorTest
{
	private static final String SUCCESSFUL_RETURN_CODE = "0";
	private static final String SUCCESSFUL_RETURN_MESSAGE = "SUCCESS";
	private static final String CARD_EXPIRY = "0517";
	private static final Integer EXPIRY_MONTH = 5;
	private static final Integer EXPIRY_YEAR = 2017;
	private static final String MASKED_CARD_NUMBER = "344058xxxxx6266";
	private static final String TOKEN = "T344058642196266";
	private static final String UNKNOWN_TYPE = "UNKNOWN";

	@InjectMocks
	private PaymentInfoWPGHttpAuthoriseResultPopulator testedInstance = new PaymentInfoWPGHttpAuthoriseResultPopulator();

	private Map<String, String> parametersMap = new HashMap<>();
	private CreateSubscriptionResult createSubscriptionResult = new CreateSubscriptionResult();
	private WPGHttpValidateResultData wpgResultInfoData = new WPGHttpValidateResultData();

	@Mock
	private CartService cartServiceMock;
	@Mock
	private CartModel cartModelMock;

	@Before
	public void setUp()
	{
		createSubscriptionResult.setWpgResultInfoData(wpgResultInfoData);
		createSubscriptionResult.setDecision(DecisionsEnum.ACCEPT.name());
		wpgResultInfoData.setReturnCode(SUCCESSFUL_RETURN_CODE);
		wpgResultInfoData.setReturnMessage(SUCCESSFUL_RETURN_MESSAGE);
		wpgResultInfoData.setAcquirerID(WPGCreditCardType.VISA.getStringValue());
		wpgResultInfoData.setCardExpiry(CARD_EXPIRY);
		wpgResultInfoData.setMaskedCardNumber(MASKED_CARD_NUMBER);
		wpgResultInfoData.setToken(TOKEN);

		when(cartServiceMock.getSessionCart()).thenReturn(cartModelMock);

	}

	@Test
	public void shouldSkipUnknownCreditCardType()
	{
		wpgResultInfoData.setAcquirerID(UNKNOWN_TYPE);

		testedInstance.populate(parametersMap, createSubscriptionResult);

		assertNull(createSubscriptionResult.getPaymentInfoData().getCardCardType());
	}

	@Test
	public void shouldSetAmexCreditCardType()
	{
		wpgResultInfoData.setAcquirerID(WPGCreditCardType.AMEX.getStringValue());

		testedInstance.populate(parametersMap, createSubscriptionResult);

		final String resultCardType = createSubscriptionResult.getPaymentInfoData().getCardCardType();
		final String mappedCardType = CardTypeEnum.get(resultCardType).name().toUpperCase();
		assertEquals(CreditCardType.AMEX, CreditCardType.valueOf(mappedCardType));
	}

	@Test
	public void shouldSetVisaCreditCardType()
	{
		wpgResultInfoData.setAcquirerID(WPGCreditCardType.VISA.getStringValue());

		testedInstance.populate(parametersMap, createSubscriptionResult);

		final String resultCardType = createSubscriptionResult.getPaymentInfoData().getCardCardType();
		final String mappedCardType = CardTypeEnum.get(resultCardType).name().toUpperCase();
		assertEquals(CreditCardType.VISA, CreditCardType.valueOf(mappedCardType));
	}

	@Test
	public void shouldSetMasterCreditCardType()
	{
		wpgResultInfoData.setAcquirerID(WPGCreditCardType.MASTER.getStringValue());

		testedInstance.populate(parametersMap, createSubscriptionResult);

		final String resultCardType = createSubscriptionResult.getPaymentInfoData().getCardCardType();
		final String mappedCardType = CardTypeEnum.get(resultCardType).name().toUpperCase();
		assertEquals(CreditCardType.MASTER, CreditCardType.valueOf(mappedCardType));
	}

	@Test
	public void shouldSetDinersCreditCardType()
	{
		wpgResultInfoData.setAcquirerID(WPGCreditCardType.DINERS.getStringValue());

		testedInstance.populate(parametersMap, createSubscriptionResult);

		final String resultCardType = createSubscriptionResult.getPaymentInfoData().getCardCardType();
		final String mappedCardType = CardTypeEnum.get(resultCardType).name().toUpperCase();
		assertEquals(CreditCardType.DINERS, CreditCardType.valueOf(mappedCardType));
	}

	@Test
	public void shouldSetDiscoverCreditCardType()
	{
		wpgResultInfoData.setAcquirerID(WPGCreditCardType.DISCOVER.getStringValue());

		testedInstance.populate(parametersMap, createSubscriptionResult);

		final String resultCardType = createSubscriptionResult.getPaymentInfoData().getCardCardType();
		final String mappedCardType = CardTypeEnum.get(resultCardType).name().toUpperCase();
		assertEquals(CreditCardType.DISCOVER, CreditCardType.valueOf(mappedCardType));
	}

	@Test
	public void shouldSetExpiryMonth()
	{
		testedInstance.populate(parametersMap, createSubscriptionResult);

		assertEquals(EXPIRY_MONTH, createSubscriptionResult.getPaymentInfoData().getCardExpirationMonth());
	}

	@Test
	public void shouldSetExpiryYear()
	{
		testedInstance.populate(parametersMap, createSubscriptionResult);

		assertEquals(EXPIRY_YEAR, createSubscriptionResult.getPaymentInfoData().getCardExpirationYear());
	}
}
