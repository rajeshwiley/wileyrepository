package com.wiley.core.delivery.interceptors;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.model.ExternalDeliveryModeModel;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.daos.DeliveryModeDao;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import javax.annotation.Resource;


/**
 * Created by Uladzimir_Barouski on 6/15/2016.
 */
@Ignore // todo related functionality was commented
public class WileycomExternalDeliveryModePrepareInterceptorIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{

	private static final String ORDER_CODE = "test-cart1";
	private static final String CUSTOMER_ID = "customer@test.com";

	private static final String EXTERNAL_DELIVERY_MODE_CODE_1 = "externalDeliveryMode1";
	private static final String EXTERNAL_DELIVERY_MODE_CODE_2 = "externalDeliveryMode2";

	private static final String DELIVERY_MODE_CODE_1 = "deliveryMode1";
	private static final String DELIVERY_MODE_CODE_2 = "deliveryMode2";

	@Resource
	private UserService userService;

	@Resource
	private CommerceCartService commerceCartService;

	@Resource
	private ModelService modelService;

	@Resource
	private DeliveryModeDao deliveryModeDao;

	private UserModel user;

	@Before
	public void setUp() throws Exception
	{
		importCsv("/wileycore/test/sampledata/wileycom/order/WileycomExternalDeliveryModePrepareInterceptorIntegrationTest.impex",
				DEFAULT_ENCODING);
		user = userService.getUserForUID(CUSTOMER_ID);
	}


	@Test
	public void testExternalDeliveryModeChangedToNotNullInOrder()
	{
		final AbstractOrderModel abstractOrder = commerceCartService.getCartForCodeAndUser(ORDER_CODE, user);
		final ExternalDeliveryModeModel deliveryMode = new ExternalDeliveryModeModel();
		deliveryMode.setCode(EXTERNAL_DELIVERY_MODE_CODE_1);
		deliveryMode.setExternalCode(EXTERNAL_DELIVERY_MODE_CODE_1);
		createAndChangeDeliveryModeForOrder(deliveryMode, abstractOrder);

		//Check externalDeliveryMode1 stored to DB
		Assert.assertEquals(1, deliveryModeDao.findDeliveryModesByCode(EXTERNAL_DELIVERY_MODE_CODE_1).size());

		final ExternalDeliveryModeModel newDeliveryMode = new ExternalDeliveryModeModel();
		newDeliveryMode.setCode(EXTERNAL_DELIVERY_MODE_CODE_2);
		newDeliveryMode.setExternalCode(EXTERNAL_DELIVERY_MODE_CODE_2);
		createAndChangeDeliveryModeForOrder(newDeliveryMode, abstractOrder);
		//Check externalDeliveryMode1 removed from DB
		Assert.assertEquals(0, deliveryModeDao.findDeliveryModesByCode(EXTERNAL_DELIVERY_MODE_CODE_1).size());
		Assert.assertEquals(1, deliveryModeDao.findDeliveryModesByCode(EXTERNAL_DELIVERY_MODE_CODE_2).size());
	}

	@Test
	public void testExternalDeliveryModeChangedToNullInOrder()
	{
		final AbstractOrderModel abstractOrder = commerceCartService.getCartForCodeAndUser(ORDER_CODE, user);
		final ExternalDeliveryModeModel deliveryMode = new ExternalDeliveryModeModel();
		deliveryMode.setCode(EXTERNAL_DELIVERY_MODE_CODE_1);
		deliveryMode.setExternalCode(EXTERNAL_DELIVERY_MODE_CODE_1);
		createAndChangeDeliveryModeForOrder(deliveryMode, abstractOrder);

		//Check externalDeliveryMode1 stored to DB
		Assert.assertEquals(1, deliveryModeDao.findDeliveryModesByCode(EXTERNAL_DELIVERY_MODE_CODE_1).size());

		createAndChangeDeliveryModeForOrder(null, abstractOrder);
		//Check externalDeliveryMode1 removed from DB
		Assert.assertEquals(0, deliveryModeDao.findDeliveryModesByCode(EXTERNAL_DELIVERY_MODE_CODE_1).size());
	}

	@Test
	public void testDeliveryModeStayedUntouchedAsOpposedToExternalDeliveryMode()
	{
		final AbstractOrderModel abstractOrder = commerceCartService.getCartForCodeAndUser(ORDER_CODE, user);
		final DeliveryModeModel deliveryMode = new DeliveryModeModel();
		deliveryMode.setCode(DELIVERY_MODE_CODE_1);
		createAndChangeDeliveryModeForOrder(deliveryMode, abstractOrder);

		//Check deliveryMode1 stored to DB
		Assert.assertEquals(1, deliveryModeDao.findDeliveryModesByCode(DELIVERY_MODE_CODE_1).size());

		final DeliveryModeModel newDeliveryMode = new DeliveryModeModel();
		newDeliveryMode.setCode(DELIVERY_MODE_CODE_2);
		createAndChangeDeliveryModeForOrder(newDeliveryMode, abstractOrder);
		//Check deliveryMode1 wasn't removed from DB
		Assert.assertEquals(1, deliveryModeDao.findDeliveryModesByCode(DELIVERY_MODE_CODE_1).size());
	}

	private void createAndChangeDeliveryModeForOrder(final DeliveryModeModel deliveryMode, final AbstractOrderModel abstractOrder)
	{
		abstractOrder.setDeliveryMode(deliveryMode);
		modelService.save(abstractOrder);
	}
}
