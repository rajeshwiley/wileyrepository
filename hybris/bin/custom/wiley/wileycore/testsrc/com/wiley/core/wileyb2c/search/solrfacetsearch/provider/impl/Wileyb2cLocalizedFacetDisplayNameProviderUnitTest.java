package com.wiley.core.wileyb2c.search.solrfacetsearch.provider.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cLocalizedFacetDisplayNameProviderUnitTest 
{

  private static final String FACET_VALUE = "PRE_ORDER";
  private static final String EMPTY_FACET_VALUE = "";

  @InjectMocks
  Wileyb2cLocalizedFacetDisplayNameProvider wileyb2cLocalizedFacetDisplayNameProvider;

  @Mock
  SearchQuery paramSearchQuery;
  @Mock
  IndexedProperty paramIndexedProperty;

  @Test
  public void testGetDisplayNameWhenFacetValueExistsShouldNotReturnEmpty() 
  {

    when(paramIndexedProperty.getName()).thenReturn("lifecycleStatus");
    final String expectedValue = "facet.property.lifecycleStatus.PRE_ORDER";
    final String actualValue = wileyb2cLocalizedFacetDisplayNameProvider
        .getDisplayName(paramSearchQuery, paramIndexedProperty, FACET_VALUE);

    assertEquals(actualValue, expectedValue);
  }

  @Test
  public void testGetDisplayNameWhenFacetValueEmptyShouldReturnEmpty() 
  {

    when(paramIndexedProperty.getName()).thenReturn("lifecycleStatus");
    final String expectedValue = "";
    final String actualValue = wileyb2cLocalizedFacetDisplayNameProvider
        .getDisplayName(paramSearchQuery, paramIndexedProperty, EMPTY_FACET_VALUE);

    assertEquals(actualValue, expectedValue);
  }


}
