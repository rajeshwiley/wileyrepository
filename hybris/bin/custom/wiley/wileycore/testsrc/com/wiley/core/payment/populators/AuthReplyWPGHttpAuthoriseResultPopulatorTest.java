package com.wiley.core.payment.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.payment.data.AuthReplyData;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.acceleratorservices.payment.data.OrderInfoData;
import de.hybris.platform.acceleratorservices.payment.data.SignatureData;
import de.hybris.platform.acceleratorservices.payment.data.SubscriptionSignatureData;
import de.hybris.platform.acceleratorservices.payment.data.WPGHttpValidateResultData;
import de.hybris.platform.acceleratorservices.payment.enums.DecisionsEnum;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AuthReplyWPGHttpAuthoriseResultPopulatorTest
{
	private static final String SUCCESSFUL_CODE = "0";
	private static final String NOT_SUCCESSFUL_CODE = "1111";
	private static final String NOT_NUMERIC_CODE = "notNumericCode";


	@InjectMocks
	private AuthReplyWPGHttpAuthoriseResultPopulator testedInstance = new AuthReplyWPGHttpAuthoriseResultPopulator();

	private Map<String, String> parametersMap = new HashMap<>();
	private CreateSubscriptionResult createSubscriptionResult = new CreateSubscriptionResult();
	private WPGHttpValidateResultData wpgResultInfoData = new WPGHttpValidateResultData();

	@Before
	public void setUp()
	{
		createSubscriptionResult.setWpgResultInfoData(wpgResultInfoData);
		wpgResultInfoData.setReturnCode(SUCCESSFUL_CODE);

		final AuthReplyData authReplyData = new AuthReplyData();
		authReplyData.setCvnDecision(Boolean.TRUE);
		createSubscriptionResult.setAuthReplyData(authReplyData);

		final OrderInfoData orderInfoData = new OrderInfoData();
		createSubscriptionResult.setOrderInfoData(orderInfoData);

		final SignatureData signatureData = new SignatureData();
		createSubscriptionResult.setSignatureData(signatureData);

		final SubscriptionSignatureData subscriptionSignatureData = new SubscriptionSignatureData();
		createSubscriptionResult.setSubscriptionSignatureData(subscriptionSignatureData);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldHaveTargetNotNull()
	{
		testedInstance.populate(parametersMap, null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldHaveWpgResultInfoDataNotNull()
	{
		createSubscriptionResult.setWpgResultInfoData(null);
		testedInstance.populate(parametersMap, createSubscriptionResult);
	}

	@Test
	public void shouldHaveAcceptedDecision()
	{
		testedInstance.populate(parametersMap, createSubscriptionResult);
		assertEquals(DecisionsEnum.ACCEPT.name(), createSubscriptionResult.getDecision());
	}

	@Test
	public void shouldHaveReturnCode()
	{
		testedInstance.populate(parametersMap, createSubscriptionResult);
	}

	@Test
	public void shouldHaveCvnDecision()
	{
		testedInstance.populate(parametersMap, createSubscriptionResult);
		assertEquals(Boolean.TRUE, createSubscriptionResult.getAuthReplyData().getCvnDecision());
	}

	@Test
	public void shouldHaveOrderInfoData()
	{
		testedInstance.populate(parametersMap, createSubscriptionResult);
		assertNotNull(createSubscriptionResult.getOrderInfoData());
	}

	@Test
	public void shouldHaveSignatureData()
	{
		testedInstance.populate(parametersMap, createSubscriptionResult);
		assertNotNull(createSubscriptionResult.getSignatureData());
	}

	@Test
	public void shouldHaveSubscriptionSignatureData()
	{
		testedInstance.populate(parametersMap, createSubscriptionResult);
		assertNotNull(createSubscriptionResult.getSubscriptionSignatureData());
	}

	@Test
	public void shouldSetDecisionToRejectOnFailedPayment()
	{
		wpgResultInfoData.setReturnCode(NOT_SUCCESSFUL_CODE);
		testedInstance.populate(parametersMap, createSubscriptionResult);
		assertEquals(DecisionsEnum.REJECT.name(), createSubscriptionResult.getDecision());
		assertEquals(Integer.valueOf(NOT_SUCCESSFUL_CODE), createSubscriptionResult.getReasonCode());
	}

	@Test
	public void shouldTolerateNonNumericReasonCode()
	{
		wpgResultInfoData.setReturnCode(NOT_NUMERIC_CODE);
		testedInstance.populate(parametersMap, createSubscriptionResult);
		assertNull(createSubscriptionResult.getReasonCode());
	}

}
