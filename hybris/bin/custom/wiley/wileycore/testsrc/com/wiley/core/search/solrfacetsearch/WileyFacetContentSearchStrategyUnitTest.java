package com.wiley.core.search.solrfacetsearch;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.solrfacetsearch.search.FacetSearchException;
import de.hybris.platform.solrfacetsearch.search.SearchResult;
import de.hybris.platform.solrfacetsearch.search.context.FacetSearchContext;
import de.hybris.platform.solrfacetsearch.solr.Index;
import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrServerException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyFacetContentSearchStrategyUnitTest
{
	@InjectMocks
	private WileyFacetContentSearchStrategy wileyFacetContentSearchStrategy;
	@Mock
	private SolrClient solrClient;
	@Mock
	private FacetSearchContext facetSearchContext;
	@Mock
	private Index index;
	@Mock
	private SearchResult searchResult;

	@Test
	public void postProcessSearchResult() throws SolrServerException, IOException, FacetSearchException
	{
		final SearchResult result = wileyFacetContentSearchStrategy.postProcessSearchResult(solrClient, facetSearchContext,
				index, searchResult);

		assertEquals(result, searchResult);
	}
}
