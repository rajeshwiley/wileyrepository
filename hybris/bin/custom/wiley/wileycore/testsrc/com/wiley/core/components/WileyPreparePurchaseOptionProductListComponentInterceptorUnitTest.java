/**
 *
 */
package com.wiley.core.components;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.HybrisEnumValue;
import de.hybris.platform.core.model.order.price.DiscountModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.model.DiscountRowModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.components.interceptors.WileyPreparePurchaseOptionProductListComponentInterceptor;
import com.wiley.core.model.components.WileyPurchaseOptionProductListComponentModel;

import static java.util.Collections.singletonList;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import static org.mockito.Mockito.times;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyPreparePurchaseOptionProductListComponentInterceptorUnitTest
{

	@InjectMocks
	private WileyPreparePurchaseOptionProductListComponentInterceptor interceptor;

	@Mock
	private InterceptorContext interceptorContextMock;

	@Mock
	private WileyPurchaseOptionProductListComponentModel wileyPurchaseOptionProductListComponentModel;

	@Mock
	private HybrisEnumValue ug;

	@Mock
	private ProductModel productModel;

	@Mock
	private DiscountRowModel discountRowModel;

	@Mock
	private DiscountRowModel newDiscountRowModel;

	@Mock
	private DiscountModel discountModel;

	@Mock
	private ModelService modelService;


	@Test
	public void testPrepareWhenWileyPurchaseOptionProductModelDiscountModified() throws InterceptorException
	{
		//when
		when(interceptorContextMock.isModified(wileyPurchaseOptionProductListComponentModel,
				WileyPurchaseOptionProductListComponentModel.DISCOUNT)).thenReturn(true);
		when(modelService.create(DiscountRowModel.class)).thenReturn(newDiscountRowModel);
		when(discountRowModel.getUg()).thenReturn(ug);
		when(discountRowModel.getProduct()).thenReturn(productModel);
		final List<DiscountRowModel> discountRowModels = singletonList(discountRowModel);
		final List<DiscountRowModel> newDiscountRowModels = singletonList(newDiscountRowModel);
		when(wileyPurchaseOptionProductListComponentModel.getDiscountRows()).thenReturn(discountRowModels);
		when(wileyPurchaseOptionProductListComponentModel.getDiscount()).thenReturn(discountModel);

		//given
		interceptor.onPrepare(wileyPurchaseOptionProductListComponentModel, interceptorContextMock);

		//then
		verify(newDiscountRowModel).setDiscount(discountModel);
		verify(newDiscountRowModel).setUg(ug);
		verify(newDiscountRowModel).setProduct(productModel);
		verify(modelService, times(discountRowModels.size())).removeAll(discountRowModels);
		verify(modelService, times(discountRowModels.size())).saveAll(newDiscountRowModels);
	}

}
