package com.wiley.core.wiley.process.email.context.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.model.contents.components.SimpleCMSComponentModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.store.BaseStoreModel;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.wiley.process.strategies.WileyProcessContextStoreResolutionStrategy;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyEmailContextFactoryImplUnitTest
{
	private static final Class<SimpleCMSComponentModel> COMPONENT_MODEL_CLASS = SimpleCMSComponentModel.class;
	private static final String COMPONENT_CODE = "componentCode";
	private static final String STORE_UID = "testUid";
	@InjectMocks
	private WileyEmailContextFactoryImpl wileyEmailContextFactory;
	@Mock
	private WileyProcessContextStoreResolutionStrategy wileyProcessContextStoreResolutionStrategy;
	@Mock
	private TypeService typeService;
	@Mock
	private BusinessProcessModel businessProcessModel;
	private ComposedTypeModel composedTypeModel;
	private SimpleCMSComponentModel component;

	@Before
	public void setUp() throws Exception
	{
		composedTypeModel = new ComposedTypeModel();
		composedTypeModel.setCode(COMPONENT_CODE);
		component = COMPONENT_MODEL_CLASS.newInstance();
		when(typeService.getComposedTypeForClass(COMPONENT_MODEL_CLASS)).thenReturn(composedTypeModel);
	}

	@Test(expected = IllegalArgumentException.class)
	public void resolveRendererTemplateForComponentWhenBaseStoreIsNullThenTemplateShouldBeWithoutIt()
			throws IllegalAccessException, InstantiationException
	{
		// given
		when(wileyProcessContextStoreResolutionStrategy.getBaseStore(businessProcessModel)).thenReturn(null);

		// when
		String template = wileyEmailContextFactory.resolveRendererTemplateForComponent(component, businessProcessModel);

		// then
		// Expected exception...
	}

	@Test
	public void resolveRendererTemplateForComponentWhenBaseStorePresentsThenTemplateShouldBeWithIt()
			throws IllegalAccessException, InstantiationException
	{
		// given
		BaseStoreModel baseStore = new BaseStoreModel();
		baseStore.setUid(STORE_UID);
		when(wileyProcessContextStoreResolutionStrategy.getBaseStore(businessProcessModel)).thenReturn(baseStore);

		// when
		String template = wileyEmailContextFactory.resolveRendererTemplateForComponent(component, businessProcessModel);

		// then
		assertEquals(template, STORE_UID + WileyEmailContextFactoryImpl.SEPARATOR + COMPONENT_CODE
				+ WileyEmailContextFactoryImpl.TEMPLATE_POSTFIX);
	}

}
