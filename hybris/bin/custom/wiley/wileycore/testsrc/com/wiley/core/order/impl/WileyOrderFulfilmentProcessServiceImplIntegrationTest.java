package com.wiley.core.order.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.exception.FullfilmentProcessStaringException;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.processengine.enums.ProcessState;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.model.WileyOrderProcessModel;
import com.wiley.core.order.service.WileyOrderService;

import static org.fest.assertions.Assertions.assertThat;


/**
 * Integration test for {@link WileyOrderFulfilmentProcessServiceImpl}
 */

@IntegrationTest
public class WileyOrderFulfilmentProcessServiceImplIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{

	public static final String TEST_EMPTY_WILEY_ORDER_PROCESS = "test-empty-wiley-order-process";

	@Resource
	private WileyOrderFulfilmentProcessServiceImpl wileyOrderFulfilmentProcessService;

	@Resource
	private WileyOrderService wileyOrderService;

	@Resource
	private BaseSiteService baseSiteService;

	@Resource
	private ModelService modelService;

	@Resource
	private BusinessProcessService businessProcessService;

	private BaseSiteModel asSite;
	private BaseSiteModel agsSite;
	OrderModel testOrderForFulfilment;

	@Rule
	public ExpectedException thrown = ExpectedException.none();


	@Before
	public void setUp() throws Exception
	{
		importCsv("/wileycore/test/order/WileyOrderFulfilmentProcessServiceImplIntegrationTest/"
				+ "testOrdersForFulfillmentProcess.impex", DEFAULT_ENCODING);
		asSite = baseSiteService.getBaseSiteForUID("asSite");
		agsSite = baseSiteService.getBaseSiteForUID("ags");
		testOrderForFulfilment = wileyOrderService.getOrderForGUID("test_order_for_fulfilment ", asSite);
	}

	@Test
	public void shouldThrowIAEForNullOrder()
	{
		//Given
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("order parameter can not be null");

		//When
		wileyOrderFulfilmentProcessService.startFulfilmentProcessForOrder(null, false);
	}

	@Test
	public void shouldThrowIAEForNullOrderStatus()
	{
		//Given
		testOrderForFulfilment.setStatus(null);
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("order is in incorrect status: [null]");


		//When
		wileyOrderFulfilmentProcessService.startFulfilmentProcessForOrder(testOrderForFulfilment, false);
	}

	@Test
	public void shouldThrowIAEForNonCreatedOrderStatus()
	{
		//Given
		OrderModel order = wileyOrderService.getOrderForGUID("test_order_for_fulfilment_with_cancelled_status ", asSite);
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("order is in incorrect status: [CANCELLED]");

		//When
		wileyOrderFulfilmentProcessService.startFulfilmentProcessForOrder(order, false);
	}

	@Test
	public void shouldThrowFPSForNullStoreInOrder()
	{
		//Given
		setStoreSubmitOrderProcessCode(testOrderForFulfilment, null);
		thrown.expect(FullfilmentProcessStaringException.class);
		thrown.expectMessage("Could not start process [] for order [order1] due to : "
				+ "SubmitOrderProcessCode for [asStore] store is null or empty");

		//When
		wileyOrderFulfilmentProcessService.startFulfilmentProcessForOrder(testOrderForFulfilment, false);
	}

	@Test
	public void shouldThrowFPSForNullSubmitOrderProcessCodeInStore()
	{
		//Given
		setStoreSubmitOrderProcessCode(testOrderForFulfilment, null);
		thrown.expect(FullfilmentProcessStaringException.class);
		thrown.expectMessage("Could not start process [] for order [order1] due to : SubmitOrderProcessCode "
				+ "for [asStore] store is null or empty");

		//When
		wileyOrderFulfilmentProcessService.startFulfilmentProcessForOrder(testOrderForFulfilment, false);
	}

	@Test
	public void shouldThrowFPSForEmptySubmitOrderProcessCodeInStore()
	{
		//Given
		setStoreSubmitOrderProcessCode(testOrderForFulfilment, StringUtils.EMPTY);
		thrown.expect(FullfilmentProcessStaringException.class);
		thrown.expectMessage("Could not start process [] for order [order1] due to : SubmitOrderProcessCode "
				+ "for [asStore] store is null or empty");

		//When
		wileyOrderFulfilmentProcessService.startFulfilmentProcessForOrder(testOrderForFulfilment, false);
	}

	@Test
	public void shouldCreateBusinessProcessButNotStart()
	{
		//Given
		boolean shouldStartBusinessProcess = false;

		//When
		OrderProcessModel orderProcess = wileyOrderFulfilmentProcessService.startFulfilmentProcessForOrder(testOrderForFulfilment,
				shouldStartBusinessProcess);

		assertSavedOrderProcess(orderProcess, OrderProcessModel.class, ProcessState.CREATED);
	}

	@Test
	public void shouldCreateAndStartBusinessProcess()
	{
		//Given
		boolean shouldStartBusinessProcess = true;

		//When
		OrderProcessModel orderProcess = wileyOrderFulfilmentProcessService.startFulfilmentProcessForOrder(testOrderForFulfilment,
				shouldStartBusinessProcess);

		assertSavedOrderProcess(orderProcess, OrderProcessModel.class, ProcessState.RUNNING);
	}

	@Test
	public void shouldThrowIAEForNullOrderAndPreviousState()
	{
		//Given
		OrderModel order = null;
		String previousOrderState = null;
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("order parameter can not be null");

		//When
		wileyOrderFulfilmentProcessService.startFulfilmentProcessForOrder(order, false, previousOrderState, true);
	}

	@Test
	public void shouldThrowIAEForNullPreviousState()
	{
		//Given
		String previousOrderState = null;
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("previousOrderState parameter can not be null or empty");

		//When
		wileyOrderFulfilmentProcessService.startFulfilmentProcessForOrder(testOrderForFulfilment, false, previousOrderState,
				true);
	}

	@Test
	public void shouldThrowIAEForEmptyPreviousState()
	{
		//Given
		String previousOrderState = StringUtils.EMPTY;
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("previousOrderState parameter can not be null or empty");

		//When
		wileyOrderFulfilmentProcessService.startFulfilmentProcessForOrder(testOrderForFulfilment, false, previousOrderState,
				true);
	}

	@Test
	public void shouldCreateBusinessProcessWithActiveForAndPreviousState()
	{
		//Given
		String previousOrderState = "{}";
		setStoreSubmitOrderProcessCode(testOrderForFulfilment, TEST_EMPTY_WILEY_ORDER_PROCESS);

		//When
		OrderProcessModel orderProcess = wileyOrderFulfilmentProcessService.startFulfilmentProcessForOrder(testOrderForFulfilment,
				false, previousOrderState, true);

		//Then
		assertSavedOrderProcess(orderProcess, WileyOrderProcessModel.class, ProcessState.CREATED);
		assertThat(((WileyOrderProcessModel) orderProcess).getPreviousOrderState()).isEqualTo(previousOrderState);
		assertThat(((WileyOrderProcessModel) orderProcess).getActiveFor()).isEqualTo(testOrderForFulfilment);
	}

	@Test
	public void shouldCreateBusinessProcessForOrderWithStatusPaymentAuthorized()
	{
		//Given
		OrderModel order = wileyOrderService.getOrderForGUID("test_order_for_fulfilment_with_payment_authorized_status ",
				agsSite);

		//When
		OrderProcessModel orderProcess = wileyOrderFulfilmentProcessService.startFulfilmentProcessForOrder(order, false);

		//Then
		assertSavedOrderProcess(orderProcess, OrderProcessModel.class, ProcessState.CREATED);
	}

	@Test
	public void shouldCreateBusinessProcessWithPaymentProcessFalse()
	{
		//Given
		String previousOrderState = "{}";
		setStoreSubmitOrderProcessCode(testOrderForFulfilment, TEST_EMPTY_WILEY_ORDER_PROCESS);

		//When
		OrderProcessModel orderProcess = wileyOrderFulfilmentProcessService.startFulfilmentProcessForOrder(testOrderForFulfilment,
				false, previousOrderState, false);

		//Then
		assertSavedOrderProcess(orderProcess, WileyOrderProcessModel.class, ProcessState.CREATED);
		assertThat(((WileyOrderProcessModel) orderProcess).getProcessPayment()).isEqualTo(false);
	}

	@Test
	public void shouldCreateTwoBusinessOrderProcesses()
	{
		//Given
		String previousOrderState = "{}";
		setStoreSubmitOrderProcessCode(testOrderForFulfilment, "test-empty-order-process");

		//When
		OrderProcessModel orderProcess1 = wileyOrderFulfilmentProcessService.startFulfilmentProcessForOrder(
				testOrderForFulfilment, false, previousOrderState, true);
		OrderProcessModel orderProcess2 = wileyOrderFulfilmentProcessService.startFulfilmentProcessForOrder(
				testOrderForFulfilment, false, previousOrderState, true);

		//Then
		assertSavedOrderProcess(orderProcess1, OrderProcessModel.class, ProcessState.CREATED);
		assertSavedOrderProcess(orderProcess2, OrderProcessModel.class, ProcessState.CREATED);
	}

	@Test
	public void shouldFailToCreateTwoOrderProcesses()
	{
		//Given
		String previousOrderState = "{}";
		setStoreSubmitOrderProcessCode(testOrderForFulfilment, TEST_EMPTY_WILEY_ORDER_PROCESS);
		thrown.expect(ModelSavingException.class);
		thrown.expectMessage("DuplicateKeyException");
		thrown.expectMessage("junit_activeForIdx");

		//When
		wileyOrderFulfilmentProcessService.startFulfilmentProcessForOrder(testOrderForFulfilment, false, previousOrderState,
				true);
		wileyOrderFulfilmentProcessService.startFulfilmentProcessForOrder(testOrderForFulfilment, false, previousOrderState,
				true);
	}

	@Test
	public void shouldExistsBusinessProcess()
	{
		//Given
		setStoreSubmitOrderProcessCode(testOrderForFulfilment, TEST_EMPTY_WILEY_ORDER_PROCESS);
		wileyOrderFulfilmentProcessService.startFulfilmentProcessForOrder(testOrderForFulfilment, false);

		//When
		boolean result = wileyOrderFulfilmentProcessService.existsProcessActiveFor(testOrderForFulfilment);

		//Then
		assertThat(result).isTrue();
	}

	@Test
	public void shouldNotExistsBusinessProcess()
	{
		//Given
		//no process created

		//When
		boolean result = wileyOrderFulfilmentProcessService.existsProcessActiveFor(testOrderForFulfilment);

		//Then
		assertThat(result).isFalse();
	}

	@Test
	public void shouldReturnBusinessProcessActiveForOrder()
	{
		//Given
		setStoreSubmitOrderProcessCode(testOrderForFulfilment, TEST_EMPTY_WILEY_ORDER_PROCESS);
		BusinessProcessModel createdBusinessProcess = wileyOrderFulfilmentProcessService.startFulfilmentProcessForOrder(
				testOrderForFulfilment, false);

		//When
		Optional<WileyOrderProcessModel> activeBusinessProcess = wileyOrderFulfilmentProcessService.getOrderProcessActiveFor(
				testOrderForFulfilment);

		//Then
		assertThat(activeBusinessProcess.isPresent()).isTrue();
		assertThat(activeBusinessProcess.get()).isEqualTo(createdBusinessProcess);
	}

	@Test
	public void shouldNotReturnBusinessProcessActiveForOrder()
	{
		//Given
		//no process created

		//When
		Optional<WileyOrderProcessModel> activeBusinessProcess = wileyOrderFulfilmentProcessService.getOrderProcessActiveFor(
				testOrderForFulfilment);

		//Then
		assertThat(activeBusinessProcess.isPresent()).isFalse();
	}

	private void assertSavedOrderProcess(final OrderProcessModel orderProcess, final Class<?> type, final ProcessState state)
	{
		assertThat(orderProcess).isNotNull();
		assertThat(orderProcess.getClass()).isEqualTo(type);
		assertThat(orderProcess.getPk()).isNotNull(); //saved business process
		assertThat(orderProcess.getState()).isEqualTo(state);
	}

	private void setStoreSubmitOrderProcessCode(final OrderModel order, final String submitOrderProcesscode)
	{
		BaseStoreModel store = order.getStore();
		store.setSubmitOrderProcessCode(submitOrderProcesscode);
		modelService.save(store);
	}
}
