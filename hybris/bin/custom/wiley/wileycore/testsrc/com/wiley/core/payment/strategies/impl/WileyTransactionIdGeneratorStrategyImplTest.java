package com.wiley.core.payment.strategies.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyTransactionIdGeneratorStrategyImplTest
{
	private static final long TRANSACTION_ID = 11233;

	private WileyTransactionIdGeneratorStrategyImpl testedInstance = new WileyTransactionIdGeneratorStrategyImpl();
	@Mock
	private KeyGenerator keyGenerator;

	@Test
	public void shouldReturnGeneratedId()
	{
		when(keyGenerator.generate()).thenReturn(TRANSACTION_ID);
		testedInstance.setKeyGenerator(keyGenerator);

		final String transactionId = testedInstance.generateTransactionId();

		assertEquals(String.valueOf(TRANSACTION_ID), transactionId);
	}


}