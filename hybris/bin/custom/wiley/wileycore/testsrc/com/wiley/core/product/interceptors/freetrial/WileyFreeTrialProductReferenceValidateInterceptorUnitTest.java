package com.wiley.core.product.interceptors.freetrial;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.catalog.references.ProductReferenceService;
import de.hybris.platform.servicelayer.i18n.L10NService;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import java.util.Arrays;
import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyFreeTrialProductModel;
import com.wiley.core.model.WileyFreeTrialVariantProductModel;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


/**
 * Unit test for {@link WileyFreeTrialProductReferenceValidateInterceptor}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyFreeTrialProductReferenceValidateInterceptorUnitTest
{
	@InjectMocks
	private WileyFreeTrialProductReferenceValidateInterceptor interceptorMock;

	@Mock
	private ProductReferenceService productReferenceServiceMock;

	@Mock
	private L10NService l10nServiceMock;

	@Mock
	private WileyFreeTrialProductModel freeTrialProductModelMock;

	@Mock
	private WileyFreeTrialVariantProductModel freeTrialVariantProductModelMock;

	@Mock
	private ProductReferenceModel referenceModelMock;
	@Mock
	private ProductReferenceModel referenceModelOtherMock;

	/**
	 * Sets up.
	 */
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);

		when(l10nServiceMock.getLocalizedString("error.wileytrialproduct.reference.exist.message"))
				.thenReturn("Product has relation on Free trial product");

		interceptorMock.setL10nService(l10nServiceMock);
		interceptorMock.setProductReferenceService(productReferenceServiceMock);
	}

	/**
	 * Test on validate success.
	 *
	 * @throws Exception
	 * 		the exception
	 */
	@Test
	public void testOnValidateSuccess() throws InterceptorException
	{
		// Given
		when(productReferenceServiceMock
				.getProductReferencesForSourceProduct(referenceModelMock.getSource(), ProductReferenceTypeEnum.FREE_TRIAL,
						Boolean.TRUE))
				.thenReturn(Collections.emptyList());

		// When
		interceptorMock.onValidate(referenceModelMock, mock(InterceptorContext.class));

		// Then
		verify(productReferenceServiceMock)
				.getProductReferencesForSourceProduct(referenceModelMock.getSource(), ProductReferenceTypeEnum.FREE_TRIAL,
						Boolean.TRUE);
	}

	/**
	 * exclude free trial product from validation
	 *
	 * @throws Exception
	 * 		the exception
	 */
	@Test
	public void testOnSuccessForFreeTrialProduct() throws InterceptorException
	{
		when(referenceModelMock.getSource()).thenReturn(freeTrialProductModelMock);

		when(productReferenceServiceMock
				.getProductReferencesForSourceProduct(referenceModelMock.getSource(), ProductReferenceTypeEnum.FREE_TRIAL,
						Boolean.TRUE))
				.thenReturn(Arrays.asList(referenceModelOtherMock));

		// When
		interceptorMock.onValidate(referenceModelMock, mock(InterceptorContext.class));
	}


	/**
	 * exclude free trial variant product from validation
	 *
	 * @throws Exception
	 * 		the exception
	 */
	@Test
	public void testOnSuccessForFreeTrialVariantProduct() throws InterceptorException
	{
		when(referenceModelMock.getSource()).thenReturn(freeTrialVariantProductModelMock);

		when(productReferenceServiceMock
				.getProductReferencesForSourceProduct(referenceModelMock.getSource(), ProductReferenceTypeEnum.FREE_TRIAL,
						Boolean.TRUE))
				.thenReturn(Arrays.asList(referenceModelOtherMock));

		// When
		interceptorMock.onValidate(referenceModelMock, mock(InterceptorContext.class));
	}

	/**
	 * Test on validate fail.
	 *
	 * @throws InterceptorException
	 * 		the exception
	 */
	@Test(expected = InterceptorException.class)
	public void shouldFailWhenAddingSecondFreeTrialToProduct() throws InterceptorException
	{
		when(referenceModelMock.getReferenceType()).thenReturn(ProductReferenceTypeEnum.FREE_TRIAL);
		when(productReferenceServiceMock
				.getProductReferencesForSourceProduct(referenceModelMock.getSource(), ProductReferenceTypeEnum.FREE_TRIAL,
						Boolean.TRUE))
				.thenReturn(Arrays.asList(referenceModelOtherMock));

		// When
		interceptorMock.onValidate(referenceModelMock, mock(InterceptorContext.class));
	}

	@Test
	public void shouldSucceedWhenAddingAddonToProductWithFreeTrial() throws InterceptorException
	{
		when(referenceModelMock.getReferenceType()).thenReturn(ProductReferenceTypeEnum.ADDON);
		when(productReferenceServiceMock
				.getProductReferencesForSourceProduct(referenceModelMock.getSource(), ProductReferenceTypeEnum.FREE_TRIAL,
						Boolean.TRUE))
				.thenReturn(Arrays.asList(referenceModelOtherMock));

		// When
		interceptorMock.onValidate(referenceModelMock, mock(InterceptorContext.class));
	}
}
