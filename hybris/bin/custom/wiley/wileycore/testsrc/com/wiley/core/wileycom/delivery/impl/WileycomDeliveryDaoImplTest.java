package com.wiley.core.wileycom.delivery.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.site.BaseSiteService;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.model.ExternalDeliveryModeModel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


/**
 * Created by Mikhail_Asadchy on 9/1/2016.
 */
@IntegrationTest
public class WileycomDeliveryDaoImplTest extends AbstractWileyServicelayerTransactionalTest
{

	@Resource
	private WileycomDeliveryDaoImpl wileycomDeliveryDaoImpl;

	@Resource
	private BaseSiteService baseSiteService;

	@Before
	public void setUp() throws Exception
	{
		importCsv("/wileycore/test/delivery/wileycomDeliveryDaoImplTest.impex",
				DEFAULT_ENCODING);

		baseSiteService.setCurrentBaseSite("wileyb2c", false);
	}

	@Test
	public void successCase() throws Exception
	{
		// given
		final List<String> codes = Arrays.asList("aa", "bb", "cc", "dd", "ee");

		// preparations

		// when
		final List<ExternalDeliveryModeModel> supportedExternalDeliveryModes = wileycomDeliveryDaoImpl
				.getSupportedExternalDeliveryModes(codes);

		// then
		checkResults(codes, supportedExternalDeliveryModes);
	}

	@Test
	public void shouldNotFindEntriesFromOtherSite() throws Exception
	{
		// given
		final List<String> codes = Arrays.asList("aa", "bb", "cc", "dd", "ee", "ff", "gg", "hh");
		final List<String> shouldFindCodes = Arrays.asList("aa", "bb", "cc", "dd", "ee");

		// preparations
		importCsv("/wileycore/test/delivery/unmappedDeliveryModes.impex",
				DEFAULT_ENCODING);

		// when
		final List<ExternalDeliveryModeModel> supportedExternalDeliveryModes = wileycomDeliveryDaoImpl
				.getSupportedExternalDeliveryModes(codes);

		// then
		checkResults(shouldFindCodes, supportedExternalDeliveryModes);
	}

	private void checkResults(final List<String> shouldFindCodes,
			final List<ExternalDeliveryModeModel> supportedExternalDeliveryModes)
	{
		assertEquals(shouldFindCodes.size(), supportedExternalDeliveryModes.size());
		for (final ExternalDeliveryModeModel supportedExternalDeliveryMode : supportedExternalDeliveryModes)
		{
			final String externalCode = supportedExternalDeliveryMode.getExternalCode();
			boolean found = false;
			for (final String code : shouldFindCodes)
			{
				if (code.equals(externalCode)) {
					found = true;
					break;
				}
			}
			assertTrue(found);
		}
	}
}
