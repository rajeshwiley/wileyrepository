package com.wiley.core.wileycom.seo.product.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantProductModel;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Created by Uladzimir_Barouski on 6/27/2017.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileycomProductSeoUrlResolverUnitTest
{
	private static final String PRODUCT_URL_PATTERN = "/{localized-product-name}-p-{product-code}";
	private static final String PRODUCT_SEO_NAME = "product Seo Name";
	private static final String PRODUCT_NAME = "product Name";
	private static final String PRODUCT_CODE = "productCode";
	private static final String EXPECTED_SEO_URL_WITH_SEO_NAME = "/product+Seo+Name-p-productCode";
	private static final String EXPECTED_SEO_URL_WITHOUT_SEO_NAME = "/product+Name-p-productCode";

	private static final String SEO_NAME_WITH_RESERVED_CHARACTERS = "seo name :/#?&@%+";
	private static final String EXPECTED_SEO_NAME_WITH_RESERVED_CHARACTERS = "seo+name+%3A%2F%23%3F%26%40%25%2B";

	@InjectMocks
	private WileycomProductSeoUrlResolver wileycomProductSeoUrlResolver;

	@Mock
	private VariantProductModel variantProductMock;
	@Mock
	private ProductModel baseProductMock;

	@Before
	public void setUp() throws Exception
	{
		when(variantProductMock.getCode()).thenReturn(PRODUCT_CODE);
		wileycomProductSeoUrlResolver.setUrlPattern(PRODUCT_URL_PATTERN);
		when(variantProductMock.getBaseProduct()).thenReturn(baseProductMock);
	}

	@Test
	public void resolveInternalWhenProductContainsSeoName() throws Exception
	{
		//Given
		when(baseProductMock.getSeoName()).thenReturn(PRODUCT_SEO_NAME);
		//When
		String url = wileycomProductSeoUrlResolver.resolve(variantProductMock);
		//Then
		assertNotNull(url);
		assertEquals(EXPECTED_SEO_URL_WITH_SEO_NAME, url);
	}

	@Test
	public void resolveInternalWhenProductNotContainSeoName() throws Exception
	{
		//Given
		when(baseProductMock.getName()).thenReturn(PRODUCT_NAME);
		//When
		String url = wileycomProductSeoUrlResolver.resolve(variantProductMock);
		//Then
		assertNotNull(url);
		assertEquals(EXPECTED_SEO_URL_WITHOUT_SEO_NAME, url);
	}

	@Test
	public void shouldUrlSafeSeoNameWithReservedCharacters()
	{
		// Given
		when(baseProductMock.getSeoName()).thenReturn(SEO_NAME_WITH_RESERVED_CHARACTERS);

		// When
		final String actualSeoName = wileycomProductSeoUrlResolver.getSeoName(baseProductMock);

		// Then
		assertEquals(EXPECTED_SEO_NAME_WITH_RESERVED_CHARACTERS, actualSeoName);
	}
}
