package com.wiley.core.pages.interceptor;

import com.wiley.core.model.WileyDiscountRowModel;
import com.wiley.core.model.WileyPartnerCompanyModel;
import com.wiley.core.model.WileyPartnerConfigurationModel;
import com.wiley.core.model.WileyPartnerPageModel;
import com.wiley.core.partner.WileyPartnerService;
import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.jalo.CatalogManager;
import de.hybris.platform.catalog.jalo.RemoveCatalogVersionCronJob;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.commerceservices.setup.SetupSyncJobService;
import de.hybris.platform.core.model.order.price.DiscountModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cronjob.jalo.CronJob;
import de.hybris.platform.europe1.enums.UserDiscountGroup;
import de.hybris.platform.europe1.model.DiscountRowModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.jalo.type.JaloAbstractTypeException;
import de.hybris.platform.jalo.type.JaloGenericCreationException;
import de.hybris.platform.order.DiscountService;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.ServicelayerTest;
import de.hybris.platform.servicelayer.model.ModelService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/*
	Due to nature of this test (it relies on catalog synchronization) it cannot be transactional.
	Data clean up is made in WileyPartnerPagePrepareInterceptorIntegrationTest#removeTestData
 */
@IntegrationTest
public class WileyPartnerPagePrepareInterceptorIntegrationTest extends ServicelayerTest
{
	public static final String CATEGORY_CODE = "CATEGORY_CODE";
	private static final String DEFAULT_ENCODING = "UTF-8";
	public static final String SAMPLE_CATALOG = "sampleCatalog";
	public static final String PRODUCT_ONE_CODE = "PRODUCT_ONE";
	public static final String PRODUCT_TWO_CODE = "PRODUCT_TWO";
	public static final double DISCOUNT_VALUE = 20.0d;
	public static final String PARTNER_CODE = "SAMPLE_PARTNER";

	@Resource
	private ModelService modelService;
	@Resource
	private SetupSyncJobService setupSyncJobService;
	@Resource
	private CMSPageService cmsPageService;
	@Resource
	private CatalogVersionService catalogVersionService;
	@Resource
	private ProductService productService;
	@Resource
	private CategoryService categoryService;
	@Resource
	private WileyPartnerService wileyPartnerService;
	@Resource
	private DiscountService discountService;

	private CatalogVersionModel catalogVersion;
	private List<ProductModel> products;
	private CategoryModel category;
	private WileyPartnerCompanyModel partner;
	private WileyPartnerPageModel partnerPage;
	private DiscountModel discount;

	@Before
	public void setup() throws ImpExException, CMSItemNotFoundException
	{
		importCsv("/wileycore/test/pages/interceptor/sampledata.impex",
				DEFAULT_ENCODING);
		setupSyncJobService.createProductCatalogSyncJob(SAMPLE_CATALOG);

		catalogVersion = catalogVersionService.getCatalogVersion(SAMPLE_CATALOG, CatalogManager.OFFLINE_VERSION);

		products = Arrays.asList(productService.getProductForCode(catalogVersion, PRODUCT_ONE_CODE),
				productService.getProductForCode(catalogVersion, PRODUCT_TWO_CODE));
		partner = wileyPartnerService.getPartnerById(PARTNER_CODE);

		category = categoryService.getCategoryForCode(catalogVersion, CATEGORY_CODE);

		WileyPartnerPageModel templatePage =
				(WileyPartnerPageModel) cmsPageService.getDefaultPageForLabel("SamplePartnerPageTemplate",
						catalogVersion);
		partnerPage = modelService.clone(templatePage);
		partnerPage.setUid("SamplePartnerPage");

		discount = discountService.getDiscountForCode("SampleDiscount");
	}

	@Test
	public void verifyDiscountsAndProductsWereSynchronized()
	{


		WileyPartnerConfigurationModel partnerConfiguration = modelService.create(WileyPartnerConfigurationModel.class);
		partnerConfiguration.setCategory(category);
		partnerConfiguration.setWileyDiscountRows(createDiscountsForProducts(products, discount));
		modelService.save(partnerConfiguration);

		partnerPage.setPartnerConfiguration(partnerConfiguration);
		modelService.save(partnerPage);

		modelService.refresh(partnerPage);
		modelService.refresh(partner);

		assertEquals(category.getCode(), partnerPage.getPartnerCategoryCode());
		assertEquals(PARTNER_CODE, partner.getUserDiscountGroup().getCode());

		CatalogVersionModel onlineCatalog = catalogVersionService.getCatalogVersion(SAMPLE_CATALOG,
				CatalogManager.ONLINE_VERSION);

		verifyDiscount(productService.getProductForCode(onlineCatalog, PRODUCT_ONE_CODE),
				partner.getUserDiscountGroup());
		verifyDiscount(productService.getProductForCode(onlineCatalog, PRODUCT_TWO_CODE),
				partner.getUserDiscountGroup());
		verifyProducts(categoryService.getCategoryForCode(onlineCatalog, CATEGORY_CODE));

	}

	private void verifyProducts(final CategoryModel categoryForCode)
	{
		List<ProductModel> products = categoryForCode.getProducts();
		assertEquals(2, products.size());
		List<String> validProductCodes = Arrays.asList(PRODUCT_ONE_CODE, PRODUCT_TWO_CODE);
		assertTrue(validProductCodes.contains(products.get(0).getCode()));
		assertTrue(validProductCodes.contains(products.get(1).getCode()));

	}

	private void verifyDiscount(final ProductModel product, final UserDiscountGroup userDiscountGroup)
	{
		Collection<DiscountRowModel> discounts = product.getOwnEurope1Discounts();
		assertEquals(1, discounts.size());
		DiscountRowModel discount = discounts.iterator().next();
		assertEquals(userDiscountGroup, discount.getUg());
		assertEquals(DISCOUNT_VALUE, discount.getValue(), 0.01d);
	}

	private List<WileyDiscountRowModel> createDiscountsForProducts(final List<ProductModel> products,
																   final DiscountModel discount)
	{
		List<WileyDiscountRowModel> rows = new ArrayList<>();
		for (ProductModel product : products)
		{
			WileyDiscountRowModel wileyDiscountRow = modelService.create(WileyDiscountRowModel.class);
			wileyDiscountRow.setProduct(product);
			wileyDiscountRow.setDiscount(discount);
			wileyDiscountRow.setValue(DISCOUNT_VALUE);
			wileyDiscountRow.setCatalogVersion(catalogVersion);
			rows.add(wileyDiscountRow);
		}
		modelService.saveAll(rows);
		return rows;
	}

	@After
	public void removeTestData() throws JaloGenericCreationException, JaloAbstractTypeException
	{
		//Catalog removal is taken from com.epam.hybris.migration.service.impl.MigrationServiceImpl.removeCatalog()
		final ComposedType comptype = JaloSession.getCurrentSession().getTypeManager()
				.getComposedType(RemoveCatalogVersionCronJob.class);
		final HashMap params = new HashMap();
		params.put(CronJob.JOB, CatalogManager.getInstance().getOrCreateDefaultRemoveCatalogVersionJob());
		RemoveCatalogVersionCronJob cronjob = (RemoveCatalogVersionCronJob) comptype.newInstance(params);
		cronjob.setCatalog(CatalogManager.getInstance().getCatalog(SAMPLE_CATALOG));
		cronjob.getJob().perform(cronjob, true);

		partner = wileyPartnerService.getPartnerById(PARTNER_CODE);
		modelService.remove(partner);
	}
}
