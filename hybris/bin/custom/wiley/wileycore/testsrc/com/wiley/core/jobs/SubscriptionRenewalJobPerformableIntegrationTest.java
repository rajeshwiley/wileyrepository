package com.wiley.core.jobs;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.model.WileySubscriptionModel;
import com.wiley.core.subscription.events.SubscriptionRenewalEvent;
import com.wiley.core.subscription.services.WileySubscriptionService;
import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.CronJobService;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.time.TimeService;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import static org.junit.Assert.assertEquals;


/**
 * Default integration test for {@link SubscriptionRenewalJobPerformable}.
 *
 * @author Aliaksei_Zlobich
 */
@IntegrationTest
public class SubscriptionRenewalJobPerformableIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{

	@Resource
	private CronJobService cronJobService;

	@Resource
	private WileySubscriptionService wileySubscriptionService;

	@Resource
	private ModelService modelService;

	@Resource
	private TimeService timeService;

	@Resource
	private SubscriptionRenewalJobPerformable subscriptionRenewalJobPerformable;

	@Resource
	private EventService eventService;

	@Before
	public void setUp() throws Exception
	{
		importCsv("/wileycore/test/subscription/testSubscriptions.impex", DEFAULT_ENCODING);
		importCsv("/wileycore/test/jobs/testSubscriptionRenewalJobPerformable.impex", DEFAULT_ENCODING);
	}

	@Test
	public void testPerform() throws Exception
	{
		// Given
		final LocalDateTime localDateTime = LocalDateTime.parse("2017-04-03T04:14:00");
		final LocalDateTime nextExpirationDate = LocalDateTime.parse("2018-04-03T04:14:00");
		final Date expectedExpirationDate = Date.from(nextExpirationDate.atZone(ZoneId.systemDefault()).toInstant());

		// setting system time.
		timeService.setCurrentTime(Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant()));

		// we expect that the event listener modifies expiration date.
		eventService.registerEventListener(new AbstractEventListener<SubscriptionRenewalEvent>() // stub implementation
		{

			@Override
			protected void onEvent(final SubscriptionRenewalEvent event)
			{
				final WileySubscriptionModel subscription = event.getSubscription();
				subscription.setExpirationDate(expectedExpirationDate);
				modelService.save(subscription);
			}
		});

		// getting cron job for executing subscriptionRenewalJobPerformable.perform(cronJob).
		final CronJobModel cronJob = cronJobService.getCronJob("subscriptionRenewalCronJob");

		// When
		final PerformResult performResult = subscriptionRenewalJobPerformable.perform(cronJob);

		// Then
		final WileySubscriptionModel subscription = wileySubscriptionService.getSubscriptionByCode("testSubscriptionCode4");

		assertEquals(CronJobStatus.FINISHED, performResult.getStatus());
		assertEquals(CronJobResult.SUCCESS, performResult.getResult());
		assertEquals("Subscription's expiration date should equals expected.", expectedExpirationDate,
				subscription.getExpirationDate());
	}
}