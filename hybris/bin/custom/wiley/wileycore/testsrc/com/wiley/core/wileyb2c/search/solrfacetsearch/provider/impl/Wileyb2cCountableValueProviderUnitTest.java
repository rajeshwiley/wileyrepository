package com.wiley.core.wileyb2c.search.solrfacetsearch.provider.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.product.WileyCountableProductService;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cCountableValueProviderUnitTest
{
	@InjectMocks
	private Wileyb2cCountableValueProvider wileyb2cCountableValueProvider;
	@Mock
	private WileyCountableProductService wileyCountableProductService;
	@Mock
	private ProductModel productModel;

	@Test
	public void collectValuesWhenProductShouldReturnCountable() throws FieldValueProviderException
	{
		Mockito.when(wileyCountableProductService.canProductHaveQuantity(productModel)).thenReturn(true);

		final List<String> values = wileyb2cCountableValueProvider.collectValues(null, null, productModel);

		Assert.assertEquals(values.size(), 1);
		Assert.assertEquals(values.get(0), "true");
	}

	@Test
	public void collectValuesWhenNonProductShouldReturnEmptyList() throws FieldValueProviderException
	{
		final List<String> values = wileyb2cCountableValueProvider.collectValues(null, null, 1);

		Assert.assertEquals(values.size(), 0);
	}
}
