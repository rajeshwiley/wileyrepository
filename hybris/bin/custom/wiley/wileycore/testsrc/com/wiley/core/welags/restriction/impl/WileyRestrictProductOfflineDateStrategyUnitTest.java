package com.wiley.core.welags.restriction.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantProductModel;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.when;


import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.wiley.restriction.impl.WileyRestrictProductOfflineDateStrategy;


/**
 * @author Veranika_Zhvaleuskaya
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyRestrictProductOfflineDateStrategyUnitTest
{
	@InjectMocks
	private WileyRestrictProductOfflineDateStrategy wileyRestrictProductOfflineDateStrategy;
	@Mock
	private ProductModel productModelMock;
	@Mock
	private VariantProductModel variantMock1, variantMock2, variantMock3;

	private CommerceCartParameter parameter;


	@Before
	public void before()
	{
		parameter = new CommerceCartParameter();
		parameter.setProduct(productModelMock);

	}

	@Test
	public void shouldNotBeRestricted() throws CommerceCartModificationException
	{
		// Given
		setUpProductData();
		// When
		final boolean restricted = wileyRestrictProductOfflineDateStrategy.isRestricted(productModelMock);
		// Then
		assertFalse(restricted);
	}

	private void setUpProductData()
	{
		LocalDate today = LocalDate.now();
		when(productModelMock.getOnlineDate()).thenReturn(today.minusDays(1).toDate());
		when(productModelMock.getOfflineDate()).thenReturn(today.plusDays(1).toDate());
	}


	@Test
	public void shouldNotBeRestrictedOnlineDateNullOfflineDateNull() throws CommerceCartModificationException
	{
		// Given
		LocalDate today = LocalDate.now();
		when(productModelMock.getOnlineDate()).thenReturn(null);
		when(productModelMock.getOfflineDate()).thenReturn(null);
		// When
		final boolean restricted = wileyRestrictProductOfflineDateStrategy.isRestricted(productModelMock);
		// Then
		assertFalse(restricted);
	}

	@Test
	public void shouldBeRestrictedOnlineDateNull() throws CommerceCartModificationException
	{
		// Given
		LocalDate today = LocalDate.now();
		when(productModelMock.getOnlineDate()).thenReturn(null);
		when(productModelMock.getOfflineDate()).thenReturn(today.minusDays(1).toDate());
		// When
		final boolean restricted = wileyRestrictProductOfflineDateStrategy.isRestricted(productModelMock);
		// Then
		assertTrue(restricted);
	}


	@Test
	public void shouldBeRestrictedOfflineDateNull() throws CommerceCartModificationException
	{
		// Given
		LocalDate today = LocalDate.now();
		when(productModelMock.getOnlineDate()).thenReturn(today.plusDays(1).toDate());
		when(productModelMock.getOfflineDate()).thenReturn(null);
		// When
		final boolean restricted = wileyRestrictProductOfflineDateStrategy.isRestricted(productModelMock);
		// Then
		assertTrue(restricted);
	}


	@Test
	public void shouldNotBeRestrictedOfflineDateToday() throws CommerceCartModificationException
	{
		// Given
		LocalDate today = LocalDate.now();
		when(productModelMock.getOnlineDate()).thenReturn(null);
		when(productModelMock.getOfflineDate()).thenReturn(today.toDate());
		// When
		final boolean restricted = wileyRestrictProductOfflineDateStrategy.isRestricted(productModelMock);
		// Then
		assertFalse(restricted);
	}


	@Test
	public void shouldNotBeRestrictedOnlineDateToday() throws CommerceCartModificationException
	{
		// Given
		LocalDate today = LocalDate.now();
		when(productModelMock.getOnlineDate()).thenReturn(today.toDate());
		when(productModelMock.getOfflineDate()).thenReturn(null);
		// When
		final boolean restricted = wileyRestrictProductOfflineDateStrategy.isRestricted(productModelMock);
		// Then
		assertFalse(restricted);
	}

	@Test
	public void shouldNotBeRestrictedOnlineDateTodayOfflineDateToday() throws CommerceCartModificationException
	{
		// Given
		LocalDate today = LocalDate.now();
		when(productModelMock.getOnlineDate()).thenReturn(today.toDate());
		when(productModelMock.getOfflineDate()).thenReturn(today.toDate());
		// When
		final boolean restricted = wileyRestrictProductOfflineDateStrategy.isRestricted(productModelMock);
		// Then
		assertFalse(restricted);
	}


	@Test
	public void shouldNotBeRestrictedCommerce() throws CommerceCartModificationException
	{
		// Given
		setUpProductData();
		// When
		final boolean restricted = wileyRestrictProductOfflineDateStrategy.isRestricted(parameter);
		// Then
		assertFalse(restricted);
	}

}
