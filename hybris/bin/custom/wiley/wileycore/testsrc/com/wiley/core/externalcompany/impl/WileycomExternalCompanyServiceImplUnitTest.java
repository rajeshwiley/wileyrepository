package com.wiley.core.externalcompany.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.ExternalCompanyModel;

import static junit.framework.Assert.assertEquals;


/**
 * Test for {@link WileycomExternalCompanyServiceImpl}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileycomExternalCompanyServiceImplUnitTest
{

	private static final String TEST_EXTERNAL_ID = "testid";

	@InjectMocks
	private WileycomExternalCompanyServiceImpl wileycomExternalCompanyService;

	@Mock(name = "externalCompanyGenericDao")
	private DefaultGenericDao<ExternalCompanyModel> externalCompanyGenericDaoMock;

	@Mock
	private ExternalCompanyModel externalCompanyModelMock;

	@Test(expected = IllegalStateException.class)
	public void shouldThrowIllegalStateExceptionDueToNullParameter()
	{
		//Given
		//no additional setup

		//When
		wileycomExternalCompanyService.getExternalCompany(null);
	}

	@Test(expected = IllegalStateException.class)
	public void shouldThrowIllegalStateExceptionDueToEmptyParameter()
	{
		//Given
		//no additional setup

		//When
		wileycomExternalCompanyService.getExternalCompany(StringUtils.EMPTY);
	}

	@Test
	public void shouldReturnItem()
	{
		//Given
		ExternalCompanyModel expectedCompany = mock(ExternalCompanyModel.class);
		when(externalCompanyGenericDaoMock.find(Collections.singletonMap(ExternalCompanyModel.EXTERNALID, TEST_EXTERNAL_ID)))
				.thenReturn(Collections.singletonList(expectedCompany));

		//When
		ExternalCompanyModel actualCompany = wileycomExternalCompanyService.getExternalCompany(TEST_EXTERNAL_ID);

		//Then
		assertEquals(expectedCompany, actualCompany);
	}


	@Test(expected = UnknownIdentifierException.class)
	public void shouldThrowUnknownIdentifierException()
	{
		//Given
		when(externalCompanyGenericDaoMock.find(Collections.singletonMap(ExternalCompanyModel.EXTERNALID, TEST_EXTERNAL_ID)))
				.thenReturn(Collections.EMPTY_LIST);

		//When
		wileycomExternalCompanyService.getExternalCompany(TEST_EXTERNAL_ID);
	}

	@Test(expected = AmbiguousIdentifierException.class)
	public void shouldThrowAmbiguousIdentifierException()
	{
		//Given
		List<ExternalCompanyModel> daoFoundCompanies = Arrays.asList(mock(ExternalCompanyModel.class),
				mock(ExternalCompanyModel.class));
		when(externalCompanyGenericDaoMock.find(Collections.singletonMap(ExternalCompanyModel.EXTERNALID, TEST_EXTERNAL_ID)))
				.thenReturn(daoFoundCompanies);

		//When
		wileycomExternalCompanyService.getExternalCompany(TEST_EXTERNAL_ID);
	}

}
