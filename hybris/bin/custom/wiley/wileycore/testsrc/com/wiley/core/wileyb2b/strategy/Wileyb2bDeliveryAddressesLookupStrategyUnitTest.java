package com.wiley.core.wileyb2b.strategy;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BUnitService;
import de.hybris.platform.b2bacceleratorservices.company.CompanyB2BCommerceService;
import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.commerceservices.strategies.DeliveryAddressesLookupStrategy;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.verification.VerificationMode;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


/**
 * Unit test for {@link Wileyb2bDeliveryAddressesLookupStrategy }
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2bDeliveryAddressesLookupStrategyUnitTest
{

	private static final Long TEST_ADDRESS_LONG_PK_1 = 88888888888888881L;
	private static final Long TEST_ADDRESS_LONG_PK_2 = 88888888888888882L;
	private static final boolean TEST_VISIBLE_ADDRESSES_ONLY = false; //no matter true or false here

	@InjectMocks
	private Wileyb2bDeliveryAddressesLookupStrategy wileyb2bDeliveryAddressesLookupStrategy;

	@Mock
	private DeliveryAddressesLookupStrategy fallbackDeliveryAddressesLookupStrategyMock;

	@Mock
	private CompanyB2BCommerceService companyB2BCommerceServiceMock;

	@Mock
	private B2BUnitService b2bUnitServiceMock;

	@Mock
	private B2BUnitModel b2BUnitMock;
	List<AddressModel> b2bUnitAddresses = new ArrayList<>();

	@Mock
	private B2BCustomerModel b2BCustomerModelMock;

	@Mock
	private AbstractOrderModel abstractOrderMock;

	@Before
	public void b2bModelSetup()
	{
		when(companyB2BCommerceServiceMock.getCurrentUser()).thenReturn(b2BCustomerModelMock);
		when(b2bUnitServiceMock.getParent(b2BCustomerModelMock)).thenReturn(b2BUnitMock);
		when(b2bUnitServiceMock.getBranch(b2BUnitMock)).thenReturn(Collections.singleton(b2BUnitMock));

		b2bUnitAddresses.add(setupAddress(TEST_ADDRESS_LONG_PK_1));
		b2bUnitAddresses.add(setupAddress(TEST_ADDRESS_LONG_PK_2));
		when(b2BUnitMock.getAddresses()).thenReturn(b2bUnitAddresses);
	}

	private AddressModel setupAddress(final Long longPk)
	{
		AddressModel addressMock = mock(AddressModel.class);
		when(addressMock.getPk()).thenReturn(PK.fromLong(longPk));
		when(addressMock.getShippingAddress()).thenReturn(Boolean.TRUE);
		return addressMock;
	}

	@Test
	public void shouldReturnB2bUnitAddresses()
	{
		//Given
		when(abstractOrderMock.getPaymentType()).thenReturn(CheckoutPaymentType.ACCOUNT);

		//When
		List<AddressModel> result = wileyb2bDeliveryAddressesLookupStrategy.getDeliveryAddressesForOrder(abstractOrderMock,
				TEST_VISIBLE_ADDRESSES_ONLY);


		//Then
		assertFallbackStrategyWasExecuted(never(), abstractOrderMock, TEST_VISIBLE_ADDRESSES_ONLY);
		assertB2bBranchLookupWasExecuted(times(1), b2BCustomerModelMock);
		for (AddressModel b2bAddress : b2bUnitAddresses)
		{
			assertTrue("Strategy should return address from b2bUnitAddresses", result.contains(b2bAddress));
		}
	}

	@Test
	public void shouldReturnFallbackAddressesForCardPayment()
	{

		//Given
		List<AddressModel> addressesMock = mock(List.class);
		when(abstractOrderMock.getPaymentType()).thenReturn(CheckoutPaymentType.CARD);
		when(fallbackDeliveryAddressesLookupStrategyMock
				.getDeliveryAddressesForOrder(eq(abstractOrderMock), eq(TEST_VISIBLE_ADDRESSES_ONLY))).thenReturn(addressesMock);

		//When
		List<AddressModel> result = wileyb2bDeliveryAddressesLookupStrategy.getDeliveryAddressesForOrder(abstractOrderMock,
				TEST_VISIBLE_ADDRESSES_ONLY);

		//Then
		assertFallbackStrategyWasExecuted(times(1), abstractOrderMock, TEST_VISIBLE_ADDRESSES_ONLY);
		assertB2bBranchLookupWasExecuted(never(), null);
		assertEquals(addressesMock, result);
	}

	private void assertFallbackStrategyWasExecuted(final VerificationMode mode, final AbstractOrderModel abstractOrderMock,
			final Boolean visibleAddressesOnly)
	{
		verify(fallbackDeliveryAddressesLookupStrategyMock, mode).getDeliveryAddressesForOrder(eq(abstractOrderMock),
				eq(visibleAddressesOnly));
	}

	private void assertB2bBranchLookupWasExecuted(final VerificationMode mode, final B2BCustomerModel b2BCustomerModelMock)
	{
		verify(b2bUnitServiceMock, mode).getParent(eq(b2BCustomerModelMock));
	}


}
