/**
 *
 */
package com.wiley.core.subscription.impl;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.model.WileySubscriptionModel;
import com.wiley.core.subscription.services.WileySubscriptionService;
import com.wiley.fulfilmentprocess.model.WileySubscriptionProcessModel;
import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.processengine.enums.ProcessState;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.subscriptionservices.enums.SubscriptionStatus;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.Date;


/**
 *
 */
@IntegrationTest
public class WileySubscriptionServiceImplIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{


	private static final String TEST_USER_1 = "user1";

	private static final String TEST_USER_2 = "user2";

	private static final String TEST_USER_3 = "user3";

	private static final String TEST_USER_4 = "user4";

	private static final String DEFATULT_ENCODING = "utf-8";

	private static final String TEST_SUBSCRIPTION_CODE = "testSubscriptionCode1";

	private static final String TEST_SUBSCRIPTION_CODE_5 = "testSubscriptionCode5";


	@Resource
	private WileySubscriptionService subscriptionService;


	@Resource
	private UserService userService;

	@Resource
	private ModelService modelService;

	@Resource
	private ProductService productService;

	@Resource
	private CommonI18NService commonI18NService;

	/**
	 * Sets up.
	 *
	 * @throws Exception
	 * 		the exception
	 */
	@Before
	public void setUp() throws Exception
	{
		importCsv("/wileycore/test/subscription/testSubscriptions.impex", DEFATULT_ENCODING);
	}


	/**
	 * Test recieve ACTIVE WileySubscription for TEST_USER_1
	 */
	@Test
	public void testGetSubscription()
	{
		final UserModel user = userService.getUserForUID(TEST_USER_1);
		WileySubscriptionModel subscriptionModel = subscriptionService.getSubscription(user, SubscriptionStatus.ACTIVE);
		Assert.assertEquals(TEST_SUBSCRIPTION_CODE, subscriptionModel.getCode());
	}


	/**
	 * Test recieve ACTIVE subscription for TEST_USER_2 with no WileySubscription
	 */
	@Test
	public void testGetSubscriptionNoSubscrioptions()
	{
		final UserModel user = userService.getUserForUID(TEST_USER_2);
		WileySubscriptionModel subscriptionModel = subscriptionService.getSubscription(user, SubscriptionStatus.ACTIVE);
		Assert.assertEquals(null, subscriptionModel);

	}


	/**
	 * Test get ACTIVE subscription for TEST_USER_3 with 2 WileySubscription
	 */
	@Test(expected = AmbiguousIdentifierException.class)
	public void testGetSubscription2Subscrioptions()
	{
		final UserModel user = userService.getUserForUID(TEST_USER_3);
		subscriptionService.getSubscription(user, SubscriptionStatus.ACTIVE);

	}


	@Test
	public void testGetSubscriptionByCode()
	{
		WileySubscriptionModel subscriptionModel = subscriptionService.getSubscriptionByCode(TEST_SUBSCRIPTION_CODE);
		Assert.assertEquals(TEST_SUBSCRIPTION_CODE, subscriptionModel.getCode());
	}


	@Test(expected = UnknownIdentifierException.class)
	public void testGetSubscriptionNoExistingCode()
	{
		subscriptionService.getSubscriptionByCode(TEST_SUBSCRIPTION_CODE_5);

	}

	/**
	 * Test CreateSubscriptionFromOrder method when orderEntry contains all required fields
	 */
	@Test
	public void testCreateSubscriptionFromOrderWhenValid()
	{
		UserModel user = userService.getUserForUID(TEST_USER_1);
		OrderModel order = createOrder(user);
		ProductModel subscriptionProduct = productService.getProductForCode("ags_legacy_1");
		AbstractOrderEntryModel abstractOrderEntry = createOrderEntry(order, subscriptionProduct);
		WileySubscriptionModel wileySubscription = subscriptionService.createSubscriptionFromOrder(abstractOrderEntry);
		Assert.assertNotNull(wileySubscription);
	}

	/**
	 * Test CreateSubscriptionFromOrder method when orderEntry product is not instance of SubscriptionProduct
	 */
	@Test
	public void testCreateSubscriptionFromOrderWhenEntryIsNotSubscription()
	{
		UserModel user = userService.getUserForUID(TEST_USER_2);
		OrderModel order = createOrder(user);
		ProductModel product = productService.getProductForCode("non_subscription_test");
		AbstractOrderEntryModel abstractOrderEntry = createOrderEntry(order, product);
		WileySubscriptionModel wileySubscription = subscriptionService.createSubscriptionFromOrder(abstractOrderEntry);
		Assert.assertNull(wileySubscription);
	}

	/**
	 * Test CreateSubscriptionFromOrder method when order owner is not a customer
	 */
	@Test(expected = ModelSavingException.class)
	public void testCreateSubscriptionFromOrderWhenOrderOwnerIsNotCustomer()
	{
		UserModel user = userService.getUserForUID(TEST_USER_4);
		OrderModel order = createOrder(user);
		ProductModel product = productService.getProductForCode("ags_legacy_1");
		AbstractOrderEntryModel abstractOrderEntry = createOrderEntry(order, product);
		WileySubscriptionModel wileySubscription = subscriptionService.createSubscriptionFromOrder(abstractOrderEntry);
	}

	/**
	 * Test CreateSubscriptionFromOrder method when customer has active subscription
	 */
	@Test
	public void testCreateSubscriptionFromOrderWhenCustomerHasActiveSubscription()
	{
		UserModel user = userService.getUserForUID(TEST_USER_1);
		OrderModel order = createOrder(user);
		ProductModel product = productService.getProductForCode("ags_legacy_12");
		AbstractOrderEntryModel abstractOrderEntry = createOrderEntry(order, product);
		WileySubscriptionModel wileySubscription = subscriptionService.createSubscriptionFromOrder(abstractOrderEntry);
		WileySubscriptionModel previousSubscription = subscriptionService.getSubscriptionByCode(TEST_SUBSCRIPTION_CODE);
		Assert.assertNotNull(wileySubscription);
		Assert.assertEquals(SubscriptionStatus.EXPIRED, previousSubscription.getStatus());
		Assert.assertEquals(SubscriptionStatus.ACTIVE, wileySubscription.getStatus());

	}

	@Test
	public void testCheckingSubscriptionInProgress() throws ImpExException
	{
		createRenewalProcess("ags-order-process-001", "testSubscriptionCode1", ProcessState.CREATED);
		createRenewalProcess("ags-order-process-002", "testSubscriptionCode1", ProcessState.RUNNING);
		createRenewalProcess("ags-order-process-003", "testSubscriptionCode1", ProcessState.WAITING);
		createRenewalProcess("ags-order-process-004", "testSubscriptionCode1", ProcessState.ERROR);
		createRenewalProcess("ags-order-process-005", "testSubscriptionCode2", ProcessState.CREATED);
		createRenewalProcess("ags-order-process-006", "testSubscriptionCode2", ProcessState.RUNNING);
		createRenewalProcess("ags-order-process-007", "testSubscriptionCode3", ProcessState.WAITING);
		createRenewalProcess("ags-order-process-008", "testSubscriptionCode4", ProcessState.SUCCEEDED);

		Assert.assertTrue(subscriptionService.isSubscriptionInProgress("testSubscriptionCode1"));
		Assert.assertTrue(subscriptionService.isSubscriptionInProgress("testSubscriptionCode2"));
		Assert.assertTrue(subscriptionService.isSubscriptionInProgress("testSubscriptionCode3"));
		Assert.assertFalse(subscriptionService.isSubscriptionInProgress("testSubscriptionCode4"));
	}

	private void createRenewalProcess(final String code, final String subscrCode, final ProcessState state)
	{
		WileySubscriptionProcessModel process = modelService.create(WileySubscriptionProcessModel.class);
		process.setCode(code);
		process.setProcessDefinitionName("ags-order-process");
		process.setWileySubscription(subscriptionService.getSubscriptionByCode(subscrCode));
		process.setState(state);
		modelService.save(process);
	}


	private OrderEntryModel createOrderEntry(final OrderModel order, final ProductModel product)
	{
		final OrderEntryModel orderEntry = modelService.create(OrderEntryModel.class);
		final UnitModel unit = createUnit("testUnit", "testType");
		orderEntry.setOrder(order);
		orderEntry.setProduct(product);
		orderEntry.setQuantity(1L);
		orderEntry.setUnit(unit);
		modelService.save(orderEntry);
		return orderEntry;
	}

	private OrderModel createOrder(final UserModel user)
	{
		final OrderModel order = modelService.create(OrderModel.class);
		final CurrencyModel currency = commonI18NService.getCurrency("USD");
		order.setUser(user);
		order.setDate(new Date());
		order.setCurrency(currency);
		modelService.save(order);
		return order;
	}

	private UnitModel createUnit(final String code, final String type)
	{
		final UnitModel unit = modelService.create(UnitModel.class);
		unit.setCode(code);
		unit.setUnitType(type);
		modelService.save(unit);
		return unit;
	}
}
