package com.wiley.core.wileyb2c.search.solrfacetsearch.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SearchQueryPageableData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchRequest;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.search.QueryField;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.locale.WileyLocaleService;

import static com.wiley.core.constants.WileyCoreConstants.SLASH;
import static com.wiley.core.wileyb2c.search.solrfacetsearch.populators.Wileyb2cLocaleFilterPopulator.URL_INDEXED_PROPERTY_NAME;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cLocaleFilterPopulatorUnitTest
{
	private static final String FIELD_NAME = "fieldName";
	private static final String CURRENT_LOCALE = "xx-yy";
	@InjectMocks
	private Wileyb2cLocaleFilterPopulator<Object, Object> wileyb2cLocaleFilterPopulator;
	@Mock
	private FieldNameProvider fieldNameProvider;
	@Mock
	private WileyLocaleService wileyLocaleService;
	@Mock
	private SearchQueryPageableData<SolrSearchQueryData> source;
	@Mock
	private SolrSearchRequest<Object, IndexedType, IndexedProperty, SearchQuery, Object> target;
	@Mock
	private SearchQuery searchQuery;
	@Mock
	private IndexedType indexedType;
	@Mock
	private IndexedProperty indexedProperty;

	@Test
	public void populate()
	{
		when(target.getSearchQuery()).thenReturn(searchQuery);
		when(searchQuery.getIndexedType()).thenReturn(indexedType);
		when(indexedType.getIndexedProperties()).thenReturn(Collections.singletonMap(URL_INDEXED_PROPERTY_NAME, indexedProperty));
		when(fieldNameProvider.getFieldName(indexedProperty, null, FieldNameProvider.FieldType.INDEX)).thenReturn(FIELD_NAME);
		when(wileyLocaleService.getCurrentEncodedLocale()).thenReturn(CURRENT_LOCALE);

		wileyb2cLocaleFilterPopulator.populate(source, target);

		final ArgumentCaptor<QueryField> argumentCaptor =
				ArgumentCaptor.forClass(QueryField.class);
		verify(searchQuery).addFilterQuery(argumentCaptor.capture());
		final QueryField queryField = argumentCaptor.getValue();
		assertEquals(queryField.getField(), FIELD_NAME);
		assertEquals(queryField.getValues(), Collections.singleton(SLASH + CURRENT_LOCALE));
		assertEquals(queryField.getQueryOperator(), SearchQuery.QueryOperator.CONTAINS);
	}
}
