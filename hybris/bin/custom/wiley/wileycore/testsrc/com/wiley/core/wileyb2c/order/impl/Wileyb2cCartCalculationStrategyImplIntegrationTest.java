package com.wiley.core.wileyb2c.order.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.commerceservices.order.CommerceCartCalculationStrategy;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.site.BaseSiteService;
import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.wiley.session.storage.WileyFailedCartModificationsStorageService;
import com.wiley.core.wileycom.order.impl.WileycomCartCalculationStrategyImplIntegrationTest;


/**
 * @author Dzmitryi_Halahayeu
 */
@IntegrationTest
public class Wileyb2cCartCalculationStrategyImplIntegrationTest extends WileycomCartCalculationStrategyImplIntegrationTest
{
	private static final String NO_PRICES_PRODUCT = "B2C_NO_PRICES_PRODUCT";
	@Resource
	private ModelService modelService;
	@Resource
	private BaseSiteService baseSiteService;
	@Resource
	private ProductService productService;
	@Resource
	private Wileyb2cCartCalculationStrategyImpl wileyb2cCommerceCartCalculationStrategy;
	@Resource
	private WileyFailedCartModificationsStorageService wileyFailedCartModificationsStorageService;

	@Override
	protected String getSiteId()
	{
		return "wileyb2c";
	}

	@Override
	protected String getRestrictedAssortmentProductCode()
	{
		return "B2B_ONLY_PRODUCT";
	}

	@Test
	public void testFailWhenNonPricesRestrictionApply() throws CommerceCartModificationException
	{
		final ProductModel nonPurchasableProduct = productService.getProductForCode(NO_PRICES_PRODUCT);
		final CommerceCartParameter commerceCartParameter = createCartParameter(nonPurchasableProduct);

		getCartCalculationStrategy().calculateCart(commerceCartParameter);

		assertEquals(commerceCartParameter.getCart().getEntries().size(), 0);
		final List<CommerceCartModification> modifications = wileyFailedCartModificationsStorageService.popAll();
		assertEquals(modifications.size(), 1);
		final CommerceCartModification commerceCartModification = modifications.get(0);
		assertEquals(commerceCartModification.getMessage(), WileyCoreConstants.BASKET_VALIDATION_UNVAILABLE);
		assertEquals(commerceCartModification.getMessageParameters().length, 1);
		assertEquals(commerceCartModification.getMessageParameters()[0], nonPurchasableProduct.getName());
	}

	@Override
	protected CommerceCartCalculationStrategy getCartCalculationStrategy()
	{
		return wileyb2cCommerceCartCalculationStrategy;
	}
}
