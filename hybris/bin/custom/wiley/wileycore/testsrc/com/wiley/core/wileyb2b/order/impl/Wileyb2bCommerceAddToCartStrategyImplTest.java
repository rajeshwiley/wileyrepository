package com.wiley.core.wileyb2b.order.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commerceservices.order.CommerceCartCalculationStrategy;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartModificationStatus;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.enums.WileyProductSubtypeEnum;
import com.wiley.core.integration.b2baccounts.WileyB2BAccountsGateway;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2bCommerceAddToCartStrategyImplTest
{
	private static final String SAP_ACCOUNT_NUMBER = "testSapAccount";
	private static final long QUANTITY = 1;

	@Mock
	private WileyB2BAccountsGateway wileyB2BAccountsGatewayMock;

	@Mock
	private CartModel fromCartModelMock;

	@Mock
	private CartModel toCartModelMock;

	@Mock
	private List<CommerceCartModification> modificationsMock;

	@Mock
	private CommerceCartCalculationStrategy commerceCartCalculationStrategyMock;

	@Mock
	private AbstractOrderEntryModel entryMock1;

	@Mock
	private AbstractOrderEntryModel entryMock2;

	@Mock
	private CommerceCartParameter commerceCartParameter1;

	@Mock
	private CommerceCartParameter commerceCartParameter2;

	@Mock
	private Wileyb2bCommerceAddToCartStrategyImpl.AddToCartData addToCartDataMock1;

	@Mock
	private Wileyb2bCommerceAddToCartStrategyImpl.AddToCartData addToCartDataMock2;

	@Mock
	private CommerceCartModification commerceCartModificationMock1;

	@Mock
	private CommerceCartModification commerceCartModificationMock2;

	@Mock
	private ProductModel productModelMock;

	@Mock
	private B2BCustomerModel b2BCustomerModelMock;

	@Mock
	private B2BUnitModel b2BUnitModelMock;

	@Spy
	@InjectMocks
	private Wileyb2bCommerceAddToCartStrategyImpl wileyb2bCommerceAddToCartStrategy;

	@Before
	public void setUp() throws Exception
	{
		when(entryMock1.getProduct()).thenReturn(productModelMock);
		when(entryMock2.getProduct()).thenReturn(productModelMock);

		doReturn(addToCartDataMock1).doReturn(addToCartDataMock2)
				.when(wileyb2bCommerceAddToCartStrategy).addToCartInternal(any());

		doReturn(Arrays.asList(entryMock1, entryMock2)).when(fromCartModelMock).getEntries();

		doReturn(commerceCartParameter1).when(addToCartDataMock1).getCommerceCartParameter();
		doReturn(commerceCartModificationMock1).when(addToCartDataMock1).getModification();
		doNothing().when(wileyb2bCommerceAddToCartStrategy).doExternalModification(any());

		doReturn(commerceCartParameter2).when(addToCartDataMock2).getCommerceCartParameter();
		doReturn(commerceCartModificationMock2).when(addToCartDataMock2).getModification();
		when(wileyb2bCommerceAddToCartStrategy.getCartParameterValidators()).thenReturn(Collections.emptyList());
	}

	@Test
	public void verifyCartIsCalculatedOnlyOnce() throws Exception
	{
		// Given

		// When
		wileyb2bCommerceAddToCartStrategy.addAllEntriesToCart(fromCartModelMock, toCartModelMock, modificationsMock);

		// Then
		verify(commerceCartCalculationStrategyMock).calculateCart((CommerceCartParameter) any());
	}

	@Test
	public void verifyModificationsHasBeenAddedToList() throws Exception
	{
		// Given

		// When
		wileyb2bCommerceAddToCartStrategy.addAllEntriesToCart(fromCartModelMock, toCartModelMock, modificationsMock);

		// Then
		verify(modificationsMock).add(same(commerceCartModificationMock1));
		verify(modificationsMock).add(same(commerceCartModificationMock2));
	}

	@Test
	public void verifyExternalCalculationCalledOnlyIfProductCanBeModified() throws Exception
	{
		// Given
		when(addToCartDataMock1.getModification()).thenReturn(commerceCartModificationMock1);
		when(addToCartDataMock2.getModification()).thenReturn(commerceCartModificationMock2);
		when(commerceCartModificationMock1.getStatusCode()).thenReturn(CommerceCartModificationStatus.SUCCESS);
		when(commerceCartModificationMock2.getStatusCode()).thenReturn(CommerceCartModificationStatus.NO_STOCK);

		// When
		wileyb2bCommerceAddToCartStrategy.addAllEntriesToCart(fromCartModelMock, toCartModelMock, modificationsMock);

		// Then
		verify(wileyb2bCommerceAddToCartStrategy).doExternalModification(same(addToCartDataMock1));
		verify(wileyb2bCommerceAddToCartStrategy, never()).doExternalModification(same(addToCartDataMock2));
	}

	@Test
	public void verifyCartIsNotCalculatedIfNoEntries() throws Exception
	{
		// Given
		doReturn(Arrays.asList()).when(fromCartModelMock).getEntries();

		// When
		wileyb2bCommerceAddToCartStrategy.addAllEntriesToCart(fromCartModelMock, toCartModelMock, modificationsMock);

		// Then
		verify(commerceCartCalculationStrategyMock, never()).calculateCart((CommerceCartParameter) any());
	}

	@Test
	public void verifyHasProductBeenAddedMethodIfProductWasUnavailable()
	{
		// Given
		when(commerceCartModificationMock1.getStatusCode()).thenReturn(CommerceCartModificationStatus.UNAVAILABLE);

		// When
		final boolean result = wileyb2bCommerceAddToCartStrategy.hasProductBeenAdded(addToCartDataMock1);

		// Then
		assertFalse(result);
	}

	@Test
	public void verifyHasProductBeenAddedMethodIfProductWasNoStock()
	{
		// Given
		when(commerceCartModificationMock1.getStatusCode()).thenReturn(CommerceCartModificationStatus.NO_STOCK);

		// When
		final boolean result = wileyb2bCommerceAddToCartStrategy.hasProductBeenAdded(addToCartDataMock1);

		// Then
		assertFalse(result);
	}

	@Test
	public void verifyHasProductBeenAddedMethodIfProductWasAddedToCart()
	{
		// Given
		when(commerceCartModificationMock1.getStatusCode()).thenReturn(CommerceCartModificationStatus.SUCCESS);

		// When
		final boolean result = wileyb2bCommerceAddToCartStrategy.hasProductBeenAdded(addToCartDataMock1);

		// Then
		assertTrue(result);
	}

	@Test
	public void verifyAddProductToCartWhenThrowException() throws Exception
	{
		// Given
		doThrow(CommerceCartModificationException.class).when(wileyb2bCommerceAddToCartStrategy).
				addToCartInternal(same(commerceCartParameter1));

		// When
		try
		{
			wileyb2bCommerceAddToCartStrategy.addToCart(commerceCartParameter1);
			fail("Expected CommerceCartModificationException.");
		}
		catch (CommerceCartModificationException e)
		{
			// success
		}

		// Then
		verify(wileyb2bCommerceAddToCartStrategy, never()).hasProductBeenAdded(addToCartDataMock1);
		verify(wileyb2bCommerceAddToCartStrategy, never()).doExternalModification(addToCartDataMock1);
	}

	@Test
	public void verifyValidateAddToCartWhenCheckLicenseNotRequired() throws Exception
	{
		List<WileyProductSubtypeEnum> subtypes = createCheckLicenseNotRequiredSubtypes();

		for (WileyProductSubtypeEnum subtype : subtypes)
		{
			// Given
			createMocksForCheckLicense();
			when(productModelMock.getSubtype()).thenReturn(subtype);

			// When
			wileyb2bCommerceAddToCartStrategy.validateAddToCart(commerceCartParameter1);

			// Then
			verify(wileyB2BAccountsGatewayMock, never()).checkLicense(any(ProductModel.class), any(B2BCustomerModel.class));
		}
	}


	private void createMocksForCheckLicense()
	{
		when(commerceCartParameter1.getProduct()).thenReturn(productModelMock);
		when(commerceCartParameter1.getUser()).thenReturn(b2BCustomerModelMock);
		when(commerceCartParameter1.getCart()).thenReturn(fromCartModelMock);
		when(commerceCartParameter1.getQuantity()).thenReturn(QUANTITY);
		when(b2BCustomerModelMock.getDefaultB2BUnit()).thenReturn(b2BUnitModelMock);
		when(b2BUnitModelMock.getSapAccountNumber()).thenReturn(SAP_ACCOUNT_NUMBER);
		when(productModelMock.getCode()).thenReturn(StringUtils.EMPTY);
		// TODO : [ECSC-20075] commented out code which uses obsolete attribute. Should be deleted
//		when(productModelMock.getPurchasable()).thenReturn(Boolean.TRUE);
	}

	//In case of extension of WileyProductSubtypeEnum result list must be extended with new subtypes
	private static List<WileyProductSubtypeEnum> createCheckLicenseNotRequiredSubtypes()
	{
		List<WileyProductSubtypeEnum> result = new ArrayList<>();
		result.add(WileyProductSubtypeEnum.COURSE);
		result.add(WileyProductSubtypeEnum.EBOOK);
		result.add(WileyProductSubtypeEnum.EMRW);
		result.add(WileyProductSubtypeEnum.ETEXT);
		result.add(WileyProductSubtypeEnum.HARDCOVER);
		result.add(WileyProductSubtypeEnum.JOURNAL_BACKFILE);
		result.add(WileyProductSubtypeEnum.LOOSE_LEAF);
		result.add(WileyProductSubtypeEnum.OLR_SUBSCIPTION);
		result.add(WileyProductSubtypeEnum.PAPERBACK);
		result.add(WileyProductSubtypeEnum.PRODUCT_SET);
		result.add(WileyProductSubtypeEnum.REGISTRATION_CODE);
		result.add(WileyProductSubtypeEnum.SUBSCRIPTION);
		return result;
	}
}