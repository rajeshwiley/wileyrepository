package com.wiley.core.util;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import javax.annotation.Nonnull;


/**
 * Contains util methods for tests.
 */
public final class TestUtils
{

	private TestUtils()
	{
	}

	@Nonnull
	public static <T extends Exception> T expectException(@Nonnull final Class<T> expectedExceptionClass,
			@Nonnull final Runnable function)
	{
		org.springframework.util.Assert.notNull(expectedExceptionClass);
		org.springframework.util.Assert.notNull(function);

		try
		{
			// When
			function.run();
			fail("Expected " + expectedExceptionClass);
			throw new IllegalStateException("Incorrect behavior of the method.");
		}
		catch (Exception e)
		{
			// Then
			assertTrue(String.format("Expected instance of [%s] but was [%s]", expectedExceptionClass, e.getClass()),
					expectedExceptionClass.isInstance(e));
			return (T) e;
		}
	}
}
