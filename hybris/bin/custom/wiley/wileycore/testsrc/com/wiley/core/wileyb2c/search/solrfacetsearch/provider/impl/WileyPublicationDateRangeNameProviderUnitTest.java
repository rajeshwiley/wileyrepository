package com.wiley.core.wileyb2c.search.solrfacetsearch.provider.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.ValueRange;
import de.hybris.platform.solrfacetsearch.config.ValueRangeSet;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyPublicationDateRangeNameProviderUnitTest
{
	public static final String TEST_NAME = "testName";
	@Spy
	private WileyPublicationDateRangeNameProvider wileyPublicationDateRangeNameProvider =
			new WileyPublicationDateRangeNameProvider();
	@Mock
	private IndexedProperty indexedProperty;
	@Mock
	private ValueRangeSet valueRange;

	@Before
	public void setUp() throws Exception
	{
		when(indexedProperty.getValueRangeSets()).thenReturn(Collections.singletonMap("default", valueRange));
	}

	@Test
	public void getValueRangesWhenValueCorrectShouldConvert()
	{
		when(valueRange.getValueRanges()).thenReturn(Collections.singletonList(createValueRange("1 30")));
		doReturn(LocalDate.parse("2007-12-03")).when(wileyPublicationDateRangeNameProvider).getNow();

		final List<ValueRange> valueRanges = wileyPublicationDateRangeNameProvider.getValueRanges(indexedProperty, null);

		assertEquals(valueRanges.size(), 1);
		final ValueRange valueRange = valueRanges.get(0);
		assertEquals(valueRange.getName(), TEST_NAME);
		assertNull(valueRange.getFrom());
		assertEquals(valueRange.getTo(), "396");
	}

	@Test(expected = IllegalStateException.class)
	public void getValueRangesWhenValueHasIncorrectFormatShouldThrowException()
	{
		when(valueRange.getValueRanges()).thenReturn(Collections.singletonList(createValueRange("1 30 45")));
		doReturn(LocalDate.parse("2007-12-03")).when(wileyPublicationDateRangeNameProvider).getNow();

		wileyPublicationDateRangeNameProvider.getValueRanges(indexedProperty, null);
	}

	private ValueRange createValueRange(final String to)
	{
		final ValueRange valueRange = new ValueRange();
		valueRange.setName(TEST_NAME);
		valueRange.setTo(to);
		valueRange.setFrom(null);
		return valueRange;
	}
}
