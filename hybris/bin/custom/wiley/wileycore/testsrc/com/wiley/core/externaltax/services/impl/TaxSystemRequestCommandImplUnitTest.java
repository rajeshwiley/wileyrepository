package com.wiley.core.externaltax.services.impl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import com.wiley.core.externaltax.xml.parsing.dto.TaxServiceRequest;
import com.wiley.core.externaltax.xml.parsing.dto.TaxServiceResponse;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;


/**
 * Focusing on checking of error processing
 */
@RunWith(MockitoJUnitRunner.class)
public class TaxSystemRequestCommandImplUnitTest
{
	public static final String TAX_SERVICE_URL = "https/stubtax";

	@InjectMocks
	private TaxSystemRequestCommandImpl taxSystemRequestCommand;

	@Mock
	private RestTemplate restTemplateMock;


	@Before
	public void setUp() throws Exception
	{
		ReflectionTestUtils.setField(taxSystemRequestCommand, "taxServiceUri", TAX_SERVICE_URL);
		ReflectionTestUtils.setField(taxSystemRequestCommand, "taxServiceLocaleProperty", "en_US");

		taxSystemRequestCommand.postConstruct();
	}

	@Test
	public void testExecuteSuccessfully() throws Exception
	{
		// Given
		when(restTemplateMock
				.exchange(eq(TAX_SERVICE_URL), eq(HttpMethod.POST), any(HttpEntity.class), eq(TaxServiceResponse.class)))
				.thenReturn(stubResponseEntity("10.15"));

		// When
		Double actual = taxSystemRequestCommand.execute(new TaxServiceRequest(), "orderCode", 0);

		// Then
		Assert.assertEquals(Double.valueOf(10.15), actual);

	}

	@Test(expected = TaxSystemException.class)
	public void testShouldThrowTaxSystemExceptionInCaseExceptionOccursDuringHttpCall()
	{
		// Given
		when(restTemplateMock
				.exchange(eq(TAX_SERVICE_URL), eq(HttpMethod.POST), any(HttpEntity.class), eq(TaxServiceResponse.class)))
				.thenThrow(new ResourceAccessException(""));

		// When
		taxSystemRequestCommand.execute(new TaxServiceRequest(), "orderCode", 0);

		// Then
		// throw TaxSystemException
	}

	@Test(expected = TaxSystemException.class)
	public void testShouldThrowTaxSystemExceptionInCaseTaxSystemResponsesWithNotSuccessHttpStatus()
	{
		// Given
		when(restTemplateMock
				.exchange(eq(TAX_SERVICE_URL), eq(HttpMethod.POST), any(HttpEntity.class), eq(TaxServiceResponse.class)))
				.thenReturn(new ResponseEntity<>(new TaxServiceResponse(), HttpStatus.BAD_REQUEST));

		// When
		taxSystemRequestCommand.execute(new TaxServiceRequest(), "orderCode", 0);

		// Then
		// throw TaxSystemException
	}

	@Test(expected = TaxSystemResponseError.class)
	public void testShouldThrowTaxSystemResponseErrorInCaseOfErrorResponse()
	{
		// Given
		when(restTemplateMock
				.exchange(eq(TAX_SERVICE_URL), eq(HttpMethod.POST), any(HttpEntity.class), eq(TaxServiceResponse.class)))
				.thenReturn(stubResponseEntity(
						(String) ReflectionTestUtils.getField(taxSystemRequestCommand, "RESPONSE_STATUS_ERROR")));

		// When
		taxSystemRequestCommand.execute(new TaxServiceRequest(), "orderCode", 0);

		// Then
		// throw TaxSystemResponseError
	}

	@Test(expected = TaxSystemException.class)
	public void testShouldThrowTaxSystemExceptionInCaseInvalidFormatOfTaxAmountReply()
	{
		// Given
		when(restTemplateMock
				.exchange(eq(TAX_SERVICE_URL), eq(HttpMethod.POST), any(HttpEntity.class), eq(TaxServiceResponse.class)))
				.thenReturn(stubResponseEntity("foo"));

		// When
		taxSystemRequestCommand.execute(new TaxServiceRequest(), "orderCode", 0);

		// Then
		// throw TaxSystemException
	}

	private ResponseEntity<TaxServiceResponse> stubResponseEntity(final String taxesReply)
	{
		TaxServiceResponse taxServiceResponse = new TaxServiceResponse();
		ReflectionTestUtils.setField(taxServiceResponse, "taxAmountOrStatus", taxesReply);
		return new ResponseEntity<>(taxServiceResponse, HttpStatus.OK);
	}
}
