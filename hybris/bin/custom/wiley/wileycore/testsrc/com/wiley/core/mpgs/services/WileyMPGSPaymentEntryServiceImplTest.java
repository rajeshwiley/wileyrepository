package com.wiley.core.mpgs.services;


import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.payment.PaymentService;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Currency;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.mpgs.response.WileyAuthorizationResponse;
import com.wiley.core.mpgs.response.WileyCaptureResponse;
import com.wiley.core.mpgs.response.WileyFollowOnRefundResponse;
import com.wiley.core.mpgs.response.WileyRetrieveSessionResponse;
import com.wiley.core.mpgs.response.WileyTokenizationResponse;
import com.wiley.core.mpgs.response.WileyVerifyResponse;
import com.wiley.core.mpgs.services.impl.WileyMPGSPaymentEntryServiceImpl;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyMPGSPaymentEntryServiceImplTest
{
	private static final String TOKEN = "TOKEN";
	private static final String USD_CURRENCY = "USD";
	private static final BigDecimal TOTAL_AMOUNT = BigDecimal.ONE;
	private static final Date TIME = Date.from(Instant.now());
	private static final String SESSION_ID = "SESSION_ID";
	private static final String TRANSACTION_ID = "TRANSACTION_ID";
	private static final String CODE = "CODE";
	private static final String STATUS = "ACCEPTED";
	private static final String STATUS_DETAILS = "STATUS_DETAILS";
	private static final String MERCHANT_ID = "MERCHANT_ID";
	private static final String TEST_DATE = "2018-01-01 00:00:00";
	private static final String ORDER_STATUS = "ORDER_STATUS";

	@Mock
	private CommonI18NService commonI18NService;

	@Mock
	private WileyAuthorizationResponse mockAuthorizationResponse;

	@Mock
	private PaymentTransactionModel mockPaymentTransaction;

	@Mock
	private CurrencyModel mockCurrencyModel;

	@Mock
	private AbstractOrderModel orderMock;

	@Mock
	private PaymentInfoModel paymentInfoMock;

	@Mock
	private WileyRetrieveSessionResponse mockRetrieveSessionResponse;

	@Mock
	private WileyVerifyResponse mockVerifyResponse;

	@Mock
	private WileyTokenizationResponse mockTokenizationResponse;

	@Mock
	private WileyCaptureResponse mockCaptureResponse;

	@Mock
	private WileyFollowOnRefundResponse mockRefundResponse;

	@Mock
	private ModelService modelService;

	@Mock
	private PaymentService paymentService;

	@Mock
	private WileyMPGSPaymentInfoService wileyMPGSPaymentInfoService;

	@Mock
	private WileyMPGSMerchantService wileyMPGSMerchantService;

	@InjectMocks
	private WileyMPGSPaymentEntryServiceImpl paymentEntryServiceImpl = new WileyMPGSPaymentEntryServiceImpl();


	private void checkAssertsForPaymentTransactionEntry(final PaymentTransactionEntryModel entry,
			final PaymentTransactionType type)
	{
		assertEquals(type, entry.getType());
		assertEquals(mockPaymentTransaction, entry.getPaymentTransaction());
		assertEquals(CODE, entry.getCode());
		assertEquals(STATUS, entry.getTransactionStatus());
		assertEquals(STATUS_DETAILS, entry.getTransactionStatusDetails());
	}

	@Before
	public void setup()
	{
		PaymentTransactionModel paymentTransaction = new PaymentTransactionModel();
		when(modelService.create(PaymentTransactionModel.class)).thenReturn(paymentTransaction);
		PaymentTransactionEntryModel paymentTransactionEntry = new PaymentTransactionEntryModel();
		when(modelService.create(PaymentTransactionEntryModel.class)).thenReturn(paymentTransactionEntry);
		when(commonI18NService.getCurrency(USD_CURRENCY)).thenReturn(mockCurrencyModel);
		when(mockPaymentTransaction.getOrder()).thenReturn(orderMock);
		when(wileyMPGSMerchantService.getMerchantID(orderMock)).thenReturn(MERCHANT_ID);
	}

	@Test
	public void testCreateRetrieveSessionEntry()
	{
		when(mockRetrieveSessionResponse.getSessionId()).thenReturn(SESSION_ID);
		when(mockRetrieveSessionResponse.getStatus()).thenReturn(STATUS);
		when(mockRetrieveSessionResponse.getStatusDetails()).thenReturn(STATUS_DETAILS);
		when(paymentService.getNewPaymentTransactionEntryCode(mockPaymentTransaction, PaymentTransactionType.RETRIEVE_SESSION))
				.thenReturn(CODE);
		when(orderMock.getCurrency()).thenReturn(mockCurrencyModel);
		when(mockCurrencyModel.getIsocode()).thenReturn(USD_CURRENCY);

		PaymentTransactionEntryModel entry = paymentEntryServiceImpl.createRetrieveSessionEntry(
				mockRetrieveSessionResponse, mockPaymentTransaction);

		checkAssertsForPaymentTransactionEntry(entry, PaymentTransactionType.RETRIEVE_SESSION);
		assertEquals(SESSION_ID, entry.getRequestId());
		assertEquals(mockCurrencyModel, entry.getCurrency());
		assertNotNull(entry.getTime());
		verify(modelService).saveAll(mockPaymentTransaction, entry);
	}


	@Test(expected = IllegalArgumentException.class)
	public void testCreateRetrieveSessionEntryPassNull()
	{
		paymentEntryServiceImpl.createRetrieveSessionEntry(null, null);
	}

	@Test
	public void testCreateVerifyEntry() throws ParseException
	{
		SimpleDateFormat sdfr = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		final Date date = sdfr.parse(TEST_DATE);
		when(mockVerifyResponse.getTimeOfRecord()).thenReturn(date);
		when(mockVerifyResponse.getStatus()).thenReturn(STATUS);
		when(mockVerifyResponse.getStatusDetails()).thenReturn(STATUS_DETAILS);
		when(paymentService.getNewPaymentTransactionEntryCode(mockPaymentTransaction, PaymentTransactionType.VERIFY))
				.thenReturn(CODE);
		when(mockVerifyResponse.getTransactionId()).thenReturn(TRANSACTION_ID);
		when(mockVerifyResponse.getCurrency()).thenReturn(USD_CURRENCY);

		PaymentTransactionEntryModel entry = paymentEntryServiceImpl.createVerifyEntry(mockVerifyResponse,
				mockPaymentTransaction);

		checkAssertsForPaymentTransactionEntry(entry, PaymentTransactionType.VERIFY);
		assertEquals(TRANSACTION_ID, entry.getRequestId());
		assertNotNull(entry.getTime());
		assertEquals(TEST_DATE, sdfr.format(entry.getTime()));
		verify(modelService).saveAll(mockPaymentTransaction, entry);
	}


	@Test(expected = IllegalArgumentException.class)
	public void testCreateVerifyEntryPassNull()
	{
		paymentEntryServiceImpl.createVerifyEntry(null, null);
	}

	@Test
	public void testCreateTokenizationEntry()
	{
		when(mockTokenizationResponse.getStatus()).thenReturn(STATUS);
		when(mockTokenizationResponse.getStatusDetails()).thenReturn(STATUS_DETAILS);
		when(mockTokenizationResponse.getToken()).thenReturn(TOKEN);
		when(paymentService.getNewPaymentTransactionEntryCode(mockPaymentTransaction, PaymentTransactionType.TOKENIZATION))
				.thenReturn(CODE);
		when(orderMock.getPaymentInfo()).thenReturn(paymentInfoMock);

		PaymentTransactionEntryModel entry = paymentEntryServiceImpl.createTokenizationEntry(
				mockTokenizationResponse, mockPaymentTransaction, true);

		checkAssertsForPaymentTransactionEntry(entry, PaymentTransactionType.TOKENIZATION);
		assertEquals(TOKEN, entry.getRequestToken());
		assertNotNull(entry.getTime());
		verify(mockPaymentTransaction).setInfo(paymentInfoMock);
		verify(modelService).saveAll(mockPaymentTransaction, entry);
	}


	@Test(expected = IllegalArgumentException.class)
	public void testCreateTokenizationEntryPassNull()
	{
		paymentEntryServiceImpl.createTokenizationEntry(null, null, null, null);
	}

	@Test
	public void testCreateAuthorizationPaymentTransactionEntry()
	{
		when(mockAuthorizationResponse.getStatus()).thenReturn(STATUS);
		when(mockAuthorizationResponse.getStatusDetails()).thenReturn(STATUS_DETAILS);
		when(mockAuthorizationResponse.getToken()).thenReturn(TOKEN);
		when(mockAuthorizationResponse.getTransactionId()).thenReturn(TRANSACTION_ID);
		when(mockAuthorizationResponse.getTransactionCreatedTime()).thenReturn(TIME);
		when(mockAuthorizationResponse.getTotalAmount()).thenReturn(TOTAL_AMOUNT);
		when(mockAuthorizationResponse.getCurrency()).thenReturn(USD_CURRENCY);
		when(paymentService.getNewPaymentTransactionEntryCode(mockPaymentTransaction, PaymentTransactionType.AUTHORIZATION))
				.thenReturn(CODE);

		PaymentTransactionEntryModel entry = paymentEntryServiceImpl.createAuthorizationPaymentTransactionEntry(
				mockAuthorizationResponse, mockPaymentTransaction);

		checkAssertsForPaymentTransactionEntry(entry, PaymentTransactionType.AUTHORIZATION);
		assertEquals(TOKEN, entry.getRequestToken());
		assertEquals(TIME, entry.getTime());
		assertEquals(TRANSACTION_ID, entry.getRequestId());
		verify(modelService).saveAll(entry, mockPaymentTransaction);
	}


	@Test(expected = IllegalArgumentException.class)
	public void testCreateAuthorizationPaymentTransactionEntryPassNull()
	{
		paymentEntryServiceImpl.createAuthorizationPaymentTransactionEntry(null, null);
	}

	@Test
	public void testCreateCaptureEntry()
	{
		when(mockCaptureResponse.getStatus()).thenReturn(STATUS);
		when(mockCaptureResponse.getStatusDetails()).thenReturn(STATUS_DETAILS);
		when(mockCaptureResponse.getTransactionId()).thenReturn(TRANSACTION_ID);
		when(mockCaptureResponse.getTransactionCreatedTime()).thenReturn(TIME);
		when(mockCaptureResponse.getTransactionAmount()).thenReturn(TOTAL_AMOUNT);
		when(mockCaptureResponse.getTransactionCurrency()).thenReturn(USD_CURRENCY);
		when(paymentService.getNewPaymentTransactionEntryCode(mockPaymentTransaction, PaymentTransactionType.CAPTURE))
				.thenReturn(CODE);

		PaymentTransactionEntryModel entry = paymentEntryServiceImpl.createCaptureEntry(
				mockCaptureResponse, mockPaymentTransaction);

		checkAssertsForPaymentTransactionEntry(entry, PaymentTransactionType.CAPTURE);
		assertEquals(TIME, entry.getTime());
		assertEquals(TRANSACTION_ID, entry.getRequestId());
		verify(modelService).saveAll(mockPaymentTransaction, entry);
	}


	@Test(expected = IllegalArgumentException.class)
	public void testCreateCaptureEntryPassNull()
	{
		paymentEntryServiceImpl.createCaptureEntry(null, null);
	}

	@Test
	public void testCreateFollowOnRefundEntry()
	{
		when(mockRefundResponse.getStatus()).thenReturn(STATUS);
		when(mockRefundResponse.getStatusDetails()).thenReturn(STATUS_DETAILS);
		when(mockRefundResponse.getRequestId()).thenReturn(TRANSACTION_ID);
		when(mockRefundResponse.getTransactionCreatedTime()).thenReturn(TIME);
		when(mockRefundResponse.getTotalAmount()).thenReturn(TOTAL_AMOUNT);
		when(mockRefundResponse.getCurrency()).thenReturn(Currency.getInstance(USD_CURRENCY));
		when(paymentService.getNewPaymentTransactionEntryCode(mockPaymentTransaction, PaymentTransactionType.REFUND_FOLLOW_ON))
				.thenReturn(CODE);

		PaymentTransactionEntryModel entry = paymentEntryServiceImpl.createFollowOnRefundEntry(
				mockRefundResponse, mockPaymentTransaction);

		checkAssertsForPaymentTransactionEntry(entry, PaymentTransactionType.REFUND_FOLLOW_ON);
		assertEquals(TIME, entry.getTime());
		assertEquals(TRANSACTION_ID, entry.getRequestId());
		verify(modelService).saveAll(mockPaymentTransaction, entry);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCreateFollowOnRefundEntryPassNull()
	{
		paymentEntryServiceImpl.createFollowOnRefundEntry(null, null);
	}

}


