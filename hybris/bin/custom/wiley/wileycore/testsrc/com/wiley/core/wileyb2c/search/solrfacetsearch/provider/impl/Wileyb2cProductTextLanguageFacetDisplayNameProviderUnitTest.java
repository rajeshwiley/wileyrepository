package com.wiley.core.wileyb2c.search.solrfacetsearch.provider.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cProductTextLanguageFacetDisplayNameProviderUnitTest
{
	private static final String CODE = "code";
	private static final String NAME = "name";
	@InjectMocks
	private Wileyb2cProductTextLanguageFacetDisplayNameProvider wileyb2cProductTextLanguageFacetDisplayNameProvider;
	@Mock
	private CommonI18NService commonI18NService;
	@Mock
	private LanguageModel languageModel;

	@Test
	public void getDisplayName()
	{
		when(commonI18NService.getLanguage(CODE)).thenReturn(languageModel);
		when(languageModel.getName()).thenReturn(NAME);

		final String displayName = wileyb2cProductTextLanguageFacetDisplayNameProvider.getDisplayName(null, CODE);

		Assert.assertEquals(displayName, NAME);
	}
}
