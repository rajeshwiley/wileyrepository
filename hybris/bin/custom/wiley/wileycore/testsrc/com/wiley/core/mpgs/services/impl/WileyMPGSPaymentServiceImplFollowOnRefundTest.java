package com.wiley.core.mpgs.services.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.payment.PaymentService;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Currency;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyTnsMerchantModel;
import com.wiley.core.mpgs.constants.WileyMPGSConstants;
import com.wiley.core.mpgs.response.WileyFollowOnRefundResponse;
import com.wiley.core.mpgs.services.WileyCardPaymentService;
import com.wiley.core.mpgs.services.WileyMPGSMerchantService;
import com.wiley.core.mpgs.services.WileyMPGSPaymentEntryService;
import com.wiley.core.mpgs.services.WileyUrlGenerationService;
import com.wiley.core.mpgs.strategies.WileyMPGSTransactionIdGeneratorStrategy;
import com.wiley.core.payment.strategies.WileyTransactionIdGeneratorStrategy;
import com.wiley.core.payment.tnsmerchant.service.WileyTnsMerchantService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyMPGSPaymentServiceImplFollowOnRefundTest
{
	private static final String CURRENCY_CODE = "USD";
	private static final String GATEWAY_CODE = "GATEWAY_CODE";
	private static final String TRANSACTION_ID = "TRANSACTION_ID";
	private static final String ERROR_CAUSE = "ERROR_CAUSE";
	private static final String ENTRY_CODE = "ENTRY_CODE";
	private static final String ORDER_CODE = "ORDER_CODE";
	private static final String PREVIOUS_ORDER_CODE = "PREVIOUS_ORDER_CODE";
	private static final String REFUND_URL = "http://refund.test.com";
	private static final BigDecimal AMOUNT = BigDecimal.ONE;
	private static final Currency USD_CURRENCY = Currency.getInstance(CURRENCY_CODE);
	private static final String TEST_TRANSACTION_ID = "TEST_TRANSACTION_ID";
	private static final String AS_SITE_ID = "asSite";
	private static final String TEST_MERCHANT_ID = "TEST_MERHCANT_ID";
	private static final String TEST_ISO_CODE = "US";

	@Mock
	private ModelService modelService;
	@Mock
	private PaymentService paymentService;
	@Mock
	private OrderModel order;
	@Mock
	private ConfigurationService configurationService;
	@Mock
	private Configuration configuration;
	@Mock
	private WileyCardPaymentService wileyCardPaymentService;
	@Mock
	private CurrencyModel currency;
	@Mock
	private PaymentTransactionModel paymentTransaction;
	@Mock
	private WileyFollowOnRefundResponse followOnRefundResponse;
	@Mock
	private WileyTransactionIdGeneratorStrategy wileyTransactionIdGeneratorStrategy;
	@Mock
	private CommonI18NService commonI18NService;
	@Mock
	private WileyUrlGenerationService wileyUrlGenerationService;
	@Mock
	private WileyMPGSTransactionIdGeneratorStrategy wileyMPGSTransactionIdGenerateStrategy;
	@Mock
	private WileyMPGSMerchantService wileyMPGSMerchantService;
	@Mock
	private AddressModel paymentAddress;
	@Mock
	private CountryModel countryModel;
	@Mock
	private BaseSiteModel baseSiteModel;
	@Mock
	private WileyTnsMerchantModel wileyTnsMerchantMock;
	@Mock
	private WileyTnsMerchantService wileyTnsMerchantService;

	@Mock
	private PaymentTransactionEntryModel mockCapturePaymentTransactionEntry, mockCancelPaymentTransactionEntry,
			mockVerifyPaymentTransactionEntry;

	@Spy
	@InjectMocks
	private WileyMPGSPaymentEntryService wileyMPGSPaymentEntryService = new WileyMPGSPaymentEntryServiceImpl();
	@InjectMocks
	private WileyMPGSPaymentServiceImpl mpgsPaymentService = new WileyMPGSPaymentServiceImpl();

	@Before
	public void setUp()
	{
		when(paymentService.getNewPaymentTransactionEntryCode(paymentTransaction, PaymentTransactionType.REFUND_FOLLOW_ON))
				.thenReturn(ENTRY_CODE);
		PaymentTransactionEntryModel paymentTransactionEntry = new PaymentTransactionEntryModel();
		when(mockCapturePaymentTransactionEntry.getType()).thenReturn(PaymentTransactionType.CAPTURE);
		when(mockCapturePaymentTransactionEntry.getTnsMerchantId()).thenReturn(TEST_MERCHANT_ID);
		when(mockCapturePaymentTransactionEntry.getCurrency()).thenReturn(currency);
		when(mockCancelPaymentTransactionEntry.getType()).thenReturn(PaymentTransactionType.CANCEL);
		when(mockVerifyPaymentTransactionEntry.getType()).thenReturn(PaymentTransactionType.VERIFY);
		when(modelService.create(PaymentTransactionEntryModel.class)).thenReturn(paymentTransactionEntry);
		when(paymentTransaction.getOrder()).thenReturn(order);
		when(paymentTransaction.getEntries()).thenReturn(
				Arrays.asList(mockCapturePaymentTransactionEntry, mockCancelPaymentTransactionEntry,
						mockVerifyPaymentTransactionEntry));
		when(order.getCode()).thenReturn(ORDER_CODE);
		when(order.getCurrency()).thenReturn(currency);
		when(currency.getIsocode()).thenReturn(CURRENCY_CODE);
		when(commonI18NService.getCurrency(CURRENCY_CODE)).thenReturn(currency);
		when(wileyCardPaymentService.refundFollowOn(any())).thenReturn(followOnRefundResponse);
		when(wileyMPGSTransactionIdGenerateStrategy.generateTransactionId()).thenReturn(TEST_TRANSACTION_ID);
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getString(WileyMPGSConstants.PAYMENT_MPGS_VENDOR_ID)).thenReturn("257");

	}

	@Test
	public void successResult()
	{
		when(order.getSite()).thenReturn(baseSiteModel);
		when(baseSiteModel.getUid()).thenReturn(AS_SITE_ID);
		when(order.getPaymentAddress()).thenReturn(paymentAddress);
		when(paymentAddress.getCountry()).thenReturn(countryModel);
		when(wileyTnsMerchantService.getTnsMerchant(TEST_ISO_CODE, CURRENCY_CODE)).thenReturn(wileyTnsMerchantMock);
		when(wileyTnsMerchantMock.getId()).thenReturn(TEST_MERCHANT_ID);
		when(followOnRefundResponse.getStatus()).thenReturn(WileyMPGSConstants.RETURN_STATUS_SUCCESS);
		when(followOnRefundResponse.getStatusDetails()).thenReturn(GATEWAY_CODE);
		when(followOnRefundResponse.getRequestId()).thenReturn(TRANSACTION_ID);
		when(followOnRefundResponse.getTotalAmount()).thenReturn(AMOUNT);
		when(followOnRefundResponse.getCurrency()).thenReturn(USD_CURRENCY);

		PaymentTransactionEntryModel entry = mpgsPaymentService.refundFollowOn(paymentTransaction, AMOUNT);
		PaymentTransactionModel transaction = entry.getPaymentTransaction();

		assertEquals(PaymentTransactionType.REFUND_FOLLOW_ON, entry.getType());
		assertEquals(transaction, entry.getPaymentTransaction());
		assertEquals(ENTRY_CODE, entry.getCode());
		assertEquals(TRANSACTION_ID, entry.getRequestId());
		assertEquals(WileyMPGSConstants.RETURN_STATUS_SUCCESS, entry.getTransactionStatus());
		assertEquals(GATEWAY_CODE, entry.getTransactionStatusDetails());
		assertEquals(AMOUNT, entry.getAmount());
		assertEquals(currency, entry.getCurrency());

		verify(modelService).saveAll(transaction, entry);
	}

	@Test
	public void errorResult()
	{
		when(order.getSite()).thenReturn(baseSiteModel);
		when(baseSiteModel.getUid()).thenReturn(AS_SITE_ID);
		when(order.getPaymentAddress()).thenReturn(paymentAddress);
		when(paymentAddress.getCountry()).thenReturn(countryModel);
		when(wileyTnsMerchantService.getTnsMerchant(TEST_ISO_CODE, CURRENCY_CODE)).thenReturn(wileyTnsMerchantMock);
		when(wileyTnsMerchantMock.getId()).thenReturn(TEST_MERCHANT_ID);
		when(followOnRefundResponse.getStatus()).thenReturn(WileyMPGSConstants.RETURN_STATUS_ERROR);
		when(followOnRefundResponse.getStatusDetails()).thenReturn(ERROR_CAUSE);

		PaymentTransactionEntryModel entry = mpgsPaymentService.refundFollowOn(paymentTransaction, AMOUNT);
		PaymentTransactionModel transaction = entry.getPaymentTransaction();

		assertEquals(PaymentTransactionType.REFUND_FOLLOW_ON, entry.getType());
		assertEquals(transaction, entry.getPaymentTransaction());
		assertEquals(ENTRY_CODE, entry.getCode());
		assertNull(entry.getRequestId());
		assertEquals(WileyMPGSConstants.RETURN_STATUS_ERROR, entry.getTransactionStatus());
		assertEquals(ERROR_CAUSE, entry.getTransactionStatusDetails());

		verify(modelService).saveAll(transaction, entry);
	}

	@Test(expected = IllegalArgumentException.class)
	public void passNullParameter()
	{
		mpgsPaymentService.refundFollowOn(null, null);
	}

	@Test
	public void testGetCapturePaymentTransactionEntryModel()
	{
		PaymentTransactionEntryModel capturePaymentTransactionEntryModel =
				mpgsPaymentService.getCapturePaymentTransactionEntryModel(paymentTransaction);

		assertEquals(mockCapturePaymentTransactionEntry, capturePaymentTransactionEntryModel);
	}
}
