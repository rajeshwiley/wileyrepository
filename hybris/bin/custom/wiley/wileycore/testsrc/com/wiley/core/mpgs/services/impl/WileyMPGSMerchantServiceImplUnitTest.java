package com.wiley.core.mpgs.services.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.order.OrderModel;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.mpgs.services.strategies.impl.WileyMPGSMerchantIdStrategyImpl;
import com.wiley.core.mpgs.services.strategies.impl.WileyasMPGSMerchantIdStrategyImpl;

import static com.wiley.core.constants.WileyCoreConstants.AS_SITE_ID;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyMPGSMerchantServiceImplUnitTest
{

	private static final String AS_SITE_MERCHANT_ID = "AS_SITE_MERCHANT_ID";
	private static final String OTHER_SITE_MERCHANT_ID = "OTHER_SITE_MERCHANT_ID";

	@Mock
	private WileyasMPGSMerchantIdStrategyImpl wileyasMPGSMerchantIdStrategy;
	@Mock
	private WileyMPGSMerchantIdStrategyImpl wileyMPGSMerchantIdStrategy;
	@InjectMocks
	private WileyMPGSMerchantServiceImpl testInstance = new WileyMPGSMerchantServiceImpl();

	private OrderModel nonAsOrder = new OrderModel();
	private OrderModel asOrder = new OrderModel();

	@Before
	public void setUp()
	{
		BaseSiteModel asSite = new BaseSiteModel();
		asSite.setUid(AS_SITE_ID);
		asOrder.setSite(asSite);

		BaseSiteModel otherSite = new BaseSiteModel();
		otherSite.setUid("OTHER");
		nonAsOrder.setSite(otherSite);

		when(wileyasMPGSMerchantIdStrategy.getMerchantID(any(OrderModel.class))).thenReturn(AS_SITE_MERCHANT_ID);
		when(wileyMPGSMerchantIdStrategy.getMerchantID(any(OrderModel.class))).thenReturn(OTHER_SITE_MERCHANT_ID);
	}

	@Test
	public void shouldUseASSpecificStrategyForOrdersSiteAS()
	{
		//Given
		//When
		String result = testInstance.getMerchantID(asOrder);
		//Then
		assertEquals(AS_SITE_MERCHANT_ID, result);

	}

	@Test
	public void shouldUseDefaultStrategyForOrdersSiteAS()
	{
		//Given
		//When
		String result = testInstance.getMerchantID(nonAsOrder);
		//Then
		assertEquals(OTHER_SITE_MERCHANT_ID, result);

	}

}