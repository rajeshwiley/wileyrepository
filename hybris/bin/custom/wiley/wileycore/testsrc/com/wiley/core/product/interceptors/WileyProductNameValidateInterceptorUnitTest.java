package com.wiley.core.product.interceptors;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import com.wiley.core.model.WileyProductModel;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


/**
 * Unit test for {@link WileyProductNameValidateInterceptor}
 */
public class WileyProductNameValidateInterceptorUnitTest
{

	private WileyProductNameValidateInterceptor interceptor;

	/**
	 * Sets up.
	 */
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);

		interceptor = new WileyProductNameValidateInterceptor();

	}

	/**
	 * Test on validate success.
	 *
	 * @throws Exception
	 * 		the exception
	 */
	@Test
	public void testOnValidateSuccess() throws Exception
	{
		// Given
		final WileyProductModel productModel = mock(WileyProductModel.class);
		final String name = "name";
		when(productModel.getName()).thenReturn(name);

		// When
		interceptor.onValidate(productModel, mock(InterceptorContext.class));

		// Then
		verify(productModel).getName();


	}

	/**
	 * Test on validate name greater than MAX_PRODUCT_NAME_SIZE.
	 *
	 * @throws Exception
	 * 		the exception
	 */
	@Test(expected = InterceptorException.class)
	public void testOnValidateError() throws InterceptorException
	{
		// Given
		final WileyProductModel productModel = mock(WileyProductModel.class);
		final String name = "123456789012345657890123456789012345678901234567890123456789012345"
				+ "67890123456789012345678901234567890";
		when(productModel.getName()).thenReturn(name);

		// When
		interceptor.onValidate(productModel, mock(InterceptorContext.class));

		// Then
		verify(productModel).getName();

	}



}
