package com.wiley.core.university.validators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.servicelayer.i18n.L10NService;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.UniversityModel;
import com.wiley.core.university.interceptors.RegionUniversityValidateInterceptor;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


/**
 * Unit test for {@link RegionUniversityValidateInterceptor}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class RegionUniversityValidateInterceptorUnitTest
{
	@InjectMocks
	private RegionUniversityValidateInterceptor regionUniversityValidateInterceptor;

	@Mock
	private L10NService l10nService;

	@Mock
	private UniversityModel university;

	@Mock
	private RegionModel region;

	@Mock
	private RegionModel otherRegion;

	@Mock
	private CountryModel country;

	@Mock
	private CountryModel otherCountry;

	@Before
	public void setUp()
	{
		when(l10nService.getLocalizedString("error.university.region.required.message")).thenReturn("Invalid");
	}

	/**
	 * Case when region from hmc's form is null, and country hasn't regions
	 *
	 * @throws InterceptorException
	 */
	@Test
	public void testOnValidateSuccessCountryWithoutRegions() throws InterceptorException
	{
		//when
		when(university.getRegion()).thenReturn(null);
		when(university.getCountry()).thenReturn(country);
		when(country.getRegions()).thenReturn(Collections.emptyList());

		//than
		regionUniversityValidateInterceptor.onValidate(university, mock(InterceptorContext.class));
	}

	/**
	 * Case when region from hmc's form isn't null, and country has this region
	 *
	 * @throws InterceptorException
	 */
	@Test
	public void testOnValidateSuccessCountryWithRegions() throws InterceptorException
	{
		//when
		when(region.getCountry()).thenReturn(country);
		when(university.getRegion()).thenReturn(region);
		when(university.getCountry()).thenReturn(country);
		List<RegionModel> regions = new ArrayList<>();
		regions.add(region);

		when(country.getRegions()).thenReturn(regions);

		//than
		regionUniversityValidateInterceptor.onValidate(university, mock(InterceptorContext.class));
	}

	/**
	 * Case when region from hmc's form is null, but country has regions
	 *
	 * @throws InterceptorException
	 */
	@Test(expected = InterceptorException.class)
	public void testOnValidateFailCountryWithRegions() throws InterceptorException
	{
		//when
		when(university.getRegion()).thenReturn(otherRegion);
		when(university.getCountry()).thenReturn(country);
		List<RegionModel> regions = new ArrayList<>();
		regions.add(region);

		when(country.getRegions()).thenReturn(regions);

		//than
		regionUniversityValidateInterceptor.onValidate(university, mock(InterceptorContext.class));
	}

	/**
	 * Case when region from hmc's form isn't null, but country hasn't region
	 *
	 * @throws InterceptorException
	 */
	@Test(expected = InterceptorException.class)
	public void testOnValidateFailCountryWithRegionsAndOtherRegion() throws InterceptorException
	{
		//when
		when(otherRegion.getCountry()).thenReturn(otherCountry);
		when(university.getRegion()).thenReturn(otherRegion);
		when(university.getCountry()).thenReturn(country);
		List<RegionModel> regions = new ArrayList<>();
		regions.add(region);

		when(country.getRegions()).thenReturn(regions);


		//than
		regionUniversityValidateInterceptor.onValidate(university, mock(InterceptorContext.class));
	}

	/**
	 * Case when region from hmc's form isn't null, but country hasn't regions
	 *
	 * @throws InterceptorException
	 */
	@Test(expected = InterceptorException.class)
	public void testOnValidateFailCountryWithoutRegionsAndRegion() throws InterceptorException
	{
		//when
		when(otherRegion.getCountry()).thenReturn(otherCountry);
		when(university.getRegion()).thenReturn(otherRegion);
		when(university.getCountry()).thenReturn(country);
		when(country.getRegions()).thenReturn(Collections.emptyList());

		//than
		regionUniversityValidateInterceptor.onValidate(university, mock(InterceptorContext.class));
	}
}
