package com.wiley.core.product.attributehandler;

import de.hybris.platform.subscriptionservices.enums.TermOfServiceFrequency;
import de.hybris.platform.subscriptionservices.model.SubscriptionTermModel;

import org.junit.Test;

import com.wiley.core.enums.SubscriptionTermFrequencyValue;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SubscriptionTermFrequencyValueAttributeHandlerUnitTest
{

	private SubscriptionTermFrequencyValueAttributeHandler testInstance = new SubscriptionTermFrequencyValueAttributeHandler();

	@Test
	public void shouldReturnMonthlyIfFrequencyIsMonthlyAndNumberLessThan12() throws Exception
	{
		//Given
		SubscriptionTermModel givenTerm = givenSubscriptionTerm(TermOfServiceFrequency.MONTHLY, 11);
		//Testing
		SubscriptionTermFrequencyValue result = testInstance.get(givenTerm);
		//Verify
		assertEquals(SubscriptionTermFrequencyValue.MONTHLY, result);
	}

	@Test
	public void shouldReturnYearlyIfFrequencyIsMonthlyAndNumberOfMonthIs12() throws Exception
	{
		//Given
		SubscriptionTermModel givenTerm = givenSubscriptionTerm(TermOfServiceFrequency.MONTHLY, 12);
		//Testing
		SubscriptionTermFrequencyValue result = testInstance.get(givenTerm);
		//Verify
		assertEquals(SubscriptionTermFrequencyValue.YEARLY, result);
	}

	@Test
	public void shouldReturnUnknownIfFrequencyIsMonthlyAndNumberIs0() throws Exception
	{
		//Given
		SubscriptionTermModel givenTerm = givenSubscriptionTerm(TermOfServiceFrequency.MONTHLY, 0);
		//Testing
		SubscriptionTermFrequencyValue result = testInstance.get(givenTerm);
		//Verify
		assertEquals(SubscriptionTermFrequencyValue.UNKNOWN, result);
	}

	@Test
	public void shouldReturnUnknownIfFrequencyIsAnnuallyAndNumberIs0() throws Exception
	{
		//Given
		SubscriptionTermModel givenTerm = givenSubscriptionTerm(TermOfServiceFrequency.ANNUALLY, 0);
		//Testing
		SubscriptionTermFrequencyValue result = testInstance.get(givenTerm);
		//Verify
		assertEquals(SubscriptionTermFrequencyValue.UNKNOWN, result);
	}

	@Test
	public void shouldReturnYearlyIfFrequencyIsAnnuallyAndNumberIsMoreThan0() throws Exception
	{
		//Given
		SubscriptionTermModel givenTerm = givenSubscriptionTerm(TermOfServiceFrequency.ANNUALLY, 1);
		//Testing
		SubscriptionTermFrequencyValue result = testInstance.get(givenTerm);
		//Verify
		assertEquals(SubscriptionTermFrequencyValue.YEARLY, result);
	}

	@Test
	public void shouldReturnUnknownIfNumberIsNULL() throws Exception
	{
		//Given
		SubscriptionTermModel givenTerm = givenSubscriptionTerm(TermOfServiceFrequency.ANNUALLY, null);
		//Testing
		SubscriptionTermFrequencyValue result = testInstance.get(givenTerm);
		//Verify
		assertEquals(SubscriptionTermFrequencyValue.UNKNOWN, result);
	}

	@Test
	public void shouldReturnUnknownIfSubscriptionTermIsNULL() throws Exception
	{
		//Testing
		SubscriptionTermFrequencyValue result = testInstance.get(null);
		//Verify
		assertEquals(SubscriptionTermFrequencyValue.UNKNOWN, result);
	}

	private SubscriptionTermModel givenSubscriptionTerm(final TermOfServiceFrequency termOfServiceFrequency,
			final Integer value) {
		SubscriptionTermModel subscriptionTermModel = mock(SubscriptionTermModel.class);
		when(subscriptionTermModel.getTermOfServiceFrequency()).thenReturn(termOfServiceFrequency);
		when(subscriptionTermModel.getTermOfServiceNumber()).thenReturn(value);
		return subscriptionTermModel;
	}
}