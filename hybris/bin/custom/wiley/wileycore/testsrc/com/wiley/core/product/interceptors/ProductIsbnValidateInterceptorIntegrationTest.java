package com.wiley.core.product.interceptors;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.subscriptionservices.enums.TermOfServiceFrequency;
import de.hybris.platform.subscriptionservices.model.SubscriptionProductModel;
import de.hybris.platform.subscriptionservices.model.SubscriptionTermModel;
import de.hybris.platform.variants.model.VariantProductModel;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Arrays;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.enums.ProductEditionFormat;
import com.wiley.core.model.WileyFreeTrialProductModel;
import com.wiley.core.model.WileyFreeTrialVariantProductModel;
import com.wiley.core.model.WileyGiftCardProductModel;
import com.wiley.core.model.WileyProductModel;
import com.wiley.core.model.WileyProductVariantSetModel;
import com.wiley.core.model.WileyVariantProductModel;


/**
 * Integration test for {@link ProductIsbnValidateInterceptor} and tests children of Product
 */
@IntegrationTest
public class ProductIsbnValidateInterceptorIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	private static final String LANG_EN_ISOCODE = "en";

	public static final String WEL_CATALOG_ID = "welProductCatalog";
	public static final String AGS_CATALOG_ID = "agsProductCatalog";
	public static final String WILEY_CATALOG_ID = "wileyProductCatalog";
	public static final String CATALOG_VERSION = "Online";

	public static final String WEL_TYPE_EBOOK = "WEL_TYPE_EBOOK";
	public static final String WEL_PART_PART_1 = "WEL_PART_PART_1";
	public static final String WEL_CPA_CATEGORY = "cpa";

	private static final String TEST_ISBN = "12345679078789";
	private static final String TEST_SUBSCRIPTION_TERM_ID = "testSubscriptionTermID1";
	private static final String TEST_SUBSCRIPTION_TERM_NAME = "testSubscriptionTermName1";
	private static final String UNIT_TYPE = "unitType1";
	private static final String UNIT_CODE = "unitCode1";

	private static final String WEL_EXISTING_ISBN = "00998";
	private static final String AGS_EXISTING_ISBN = "9781119233879";
	private static final String WILEY_EXISTING_ISBN = "222222222220";
	private static final String TEST_PRODUCT_NAME_1 = "Test product Name 1";
	private static final String TEST_PRODUCT_DESCRIPTION_1 = "Test product Description 1";
	private static final String TEST_PRODUCT_NAME_2 = "Test product Name 2";

	@Resource
	private ModelService modelService;
	@Resource
	private ProductService productService;
	@Resource
	private CatalogVersionService catalogVersionService;
	@Resource
	private CommonI18NService commonI18NService;
	@Resource
	private CategoryService categoryService;

	private CategoryModel welTypeEbook;
	private CategoryModel welPartPart1;
	private CategoryModel welCpaCategory;
	private CatalogVersionModel agsCatalogOnline;
	private CatalogVersionModel welCatalogOnline;
	private CatalogVersionModel wileyCatalogOnline;
	private ProductModel baseProduct;

	@Before
	public void setUp() throws Exception
	{
		commonI18NService.setCurrentLanguage(commonI18NService.getLanguage(LANG_EN_ISOCODE));

		importCsv("/wileycore/test/product/dao/WileyVariantProductIntegrationTest/baseProducts.impex", DEFAULT_ENCODING);

		agsCatalogOnline = catalogVersionService.getCatalogVersion(AGS_CATALOG_ID, CATALOG_VERSION);
		welCatalogOnline = catalogVersionService.getCatalogVersion(WEL_CATALOG_ID, CATALOG_VERSION);
		wileyCatalogOnline = catalogVersionService.getCatalogVersion(WILEY_CATALOG_ID, CATALOG_VERSION);

		welCpaCategory = categoryService.getCategoryForCode(WEL_CPA_CATEGORY);

		welTypeEbook = categoryService.getCategoryForCode(WEL_TYPE_EBOOK);
		welPartPart1 = categoryService.getCategoryForCode(WEL_PART_PART_1);

		baseProduct = productService.getProductForCode("WEL_CPA_PLATINUM_REVIEW_COURSE");
	}

	//Skip ISBN Uniqueness validation for SubscriptionProductModel in AGS
	@Test
	public void shouldSkipIsbnValidationAndCreateSubscriptionProductInAGS()
	{
		verifyCreateProductSucceeded(AGS_EXISTING_ISBN, SubscriptionProductModel.class, agsCatalogOnline);
	}

	//Skip ISBN Uniqueness validation for ProductModel in WEL
	@Test
	public void shouldSkipIsbnValidationAndCreateProductInWEL()
	{
		verifyCreateProductSucceeded(WEL_EXISTING_ISBN, ProductModel.class, welCatalogOnline);
	}

	//Skip ISBN Uniqueness validation for SubscriptionProductModel in WEL
	@Test
	public void shouldSkipIsbnValidationAndCreateSubscriptionProductInWEL()
	{
		verifyCreateProductSucceeded(WEL_EXISTING_ISBN, SubscriptionProductModel.class, welCatalogOnline);
	}

	//Skip ISBN Uniqueness validation for ProductModel in WILEY.COM
	@Test
	public void shouldSkipIsbnValidationAndCreateProductInWiley()
	{
		verifyCreateProductSucceeded(WILEY_EXISTING_ISBN, ProductModel.class, wileyCatalogOnline);
	}

	//Skip ISBN Uniqueness validation for WileyFreeTrialVariantProductModel in WILEY.COM
	@Test
	public void shouldSkipIsbnValidationAndCreateWileyFreeTrialVariantProductModelInWiley()
	{
		verifyCreateProductSucceeded(WILEY_EXISTING_ISBN, WileyFreeTrialVariantProductModel.class, wileyCatalogOnline);
	}

	//Skip ISBN Uniqueness validation for WileyVariantProductModel in WILEY.COM
	@Test
	public void shouldSkipIsbnValidationAndCreateWileyVariantProductModelInWiley()
	{
		verifyCreateProductSucceeded(WILEY_EXISTING_ISBN, WileyVariantProductModel.class, wileyCatalogOnline);
	}

	//Skip ISBN Uniqueness validation for WileyProductVariantSetModel in WILEY.COM
	@Test
	public void shouldSkipIsbnValidationAndCreateWileyProductVariantSetModelInWiley()
	{
		verifyCreateProductSucceeded(WILEY_EXISTING_ISBN, WileyProductVariantSetModel.class, wileyCatalogOnline);
	}

	//Skip ISBN Uniqueness validation for SubscriptionProductModel in WILEY.COM
	@Test
	public void shouldSkipIsbnValidationAndCreateSubscriptionProductModelInWiley()
	{
		verifyCreateProductSucceeded(WILEY_EXISTING_ISBN, SubscriptionProductModel.class, wileyCatalogOnline);
	}

	//Skip ISBN Uniqueness validation for WileyProductModel in WILEY.COM
	@Test
	public void shouldSkipIsbnValidationAndCreateWileyProductModelInWiley()
	{
		verifyCreateProductSucceeded(WILEY_EXISTING_ISBN, WileyProductModel.class, wileyCatalogOnline);
	}

	//Skip ISBN Uniqueness validation for WileyGiftCardProductModel in WILEY.COM
	@Test
	public void shouldSkipIsbnValidationAndCreateWileyGiftCardProductModelInWiley()
	{
		verifyCreateProductSucceeded(WILEY_EXISTING_ISBN, WileyGiftCardProductModel.class, wileyCatalogOnline);
	}

	//Skip ISBN Uniqueness validation for WileyFreeTrialProductModel in WILEY.COM
	@Test
	public void shouldSkipIsbnValidationAndCreateWileyFreeTrialProductModelInWiley()
	{
		verifyCreateProductSucceeded(WILEY_EXISTING_ISBN, WileyFreeTrialProductModel.class, wileyCatalogOnline);
	}

	//Successfully create WileyFreeTrialVariantProductModel in WEL if ISBN is Not duplicated
	@Test
	public void shouldCreateWileyFreeTrialVariantProductInWEL()
	{
		verifyCreateProductSucceeded(TEST_ISBN, WileyFreeTrialVariantProductModel.class, welCatalogOnline);
	}

	//Successfully create WileyVariantProductModel in WEL if ISBN is Not duplicated
	@Test
	public void shouldCreateWileyVariantProductInWEL()
	{
		verifyCreateProductSucceeded(TEST_ISBN, WileyVariantProductModel.class, welCatalogOnline);
	}

	//Successfully create WileyProductVariantSetModel in WEL if ISBN is Not duplicated
	@Test
	public void shouldCreateWileyProductVariantSetInWEL()
	{
		verifyCreateProductSucceeded(TEST_ISBN, WileyProductVariantSetModel.class, welCatalogOnline);
	}

	//Successfully create WileyProductModel in WEL if ISBN is Not duplicated
	@Test
	public void shouldCreateWileyProductInWEL()
	{
		verifyCreateProductSucceeded(TEST_ISBN, WileyProductModel.class, welCatalogOnline);
	}

	//Successfully create WileyGiftCardProductModel in WEL if ISBN is Not duplicated
	@Test
	public void shouldCreateWileyGiftCardProductInWEL()
	{
		verifyCreateProductSucceeded(TEST_ISBN, WileyGiftCardProductModel.class, welCatalogOnline);
	}

	//Successfully create WileyFreeTrialProductModel in WEL if ISBN is Not duplicated
	@Test
	public void shouldCreateWileyFreeTrialProductModelInWEL()
	{
		verifyCreateProductSucceeded(TEST_ISBN, WileyFreeTrialProductModel.class, welCatalogOnline);
	}

	//Fail to create WileyFreeTrialVariantProductModel in WEL if duplicated ISBN found
	@Test
	public void shouldFailCreatingWileyFreeTrialVariantProductModelInWEL()
	{
		verifyCreateProductSubtypeFailed(WEL_EXISTING_ISBN, WileyFreeTrialVariantProductModel.class, welCatalogOnline);
	}

	//Fail to create WileyVariantProductModel in WEL if duplicated ISBN found
	@Test
	public void shouldFailCreatingWileyVariantProductModelInWEL()
	{
		verifyCreateProductSubtypeFailed(WEL_EXISTING_ISBN, WileyVariantProductModel.class, welCatalogOnline);
	}

	//Fail to create WileyProductVariantSetModel in WEL if duplicated ISBN found
	@Test
	public void shouldFailCreatingWileyProductVariantSetModelInWEL()
	{
		verifyCreateProductSubtypeFailed(WEL_EXISTING_ISBN, WileyProductVariantSetModel.class, welCatalogOnline);
	}

	//Fail to create WileyProductModel in WEL if duplicated ISBN found
	@Test
	public void shouldFailCreatingWileyProductModelInWEL()
	{
		verifyCreateProductSubtypeFailed(WEL_EXISTING_ISBN, WileyProductModel.class, welCatalogOnline);
	}

	//Fail to create WileyGiftCardProductModel in WEL if duplicated ISBN found
	@Test
	public void shouldFailCreatingWileyGiftCardProductModelInWEL()
	{
		verifyCreateProductSubtypeFailed(WEL_EXISTING_ISBN, WileyGiftCardProductModel.class, welCatalogOnline);
	}

	//Fail to create WileyGiftCardProductModel in WEL if duplicated ISBN found
	@Test
	public void shouldFailCreatingWileyFreeTrialProductModelInWEL()
	{
		verifyCreateProductSubtypeFailed(WEL_EXISTING_ISBN, WileyFreeTrialProductModel.class, welCatalogOnline);
	}

	private void verifyCreateProductSucceeded(final String isbn, final Class<? extends ProductModel> productModelClass,
			final CatalogVersionModel catalogVersion)
	{
		final ProductModel product = prepareProduct(isbn, productModelClass, catalogVersion);
		modelService.save(product);

		//verify product can be updated
		product.setName(TEST_PRODUCT_NAME_2);

		modelService.save(product);

		assertEquals(TEST_PRODUCT_NAME_2, product.getName());
		assertNotNull(product.getPk());
	}

	private void verifyCreateProductSubtypeFailed(final String isbn, final Class<? extends ProductModel> productModelClass,
			final CatalogVersionModel catalogVersion)
	{
		final ProductModel product = prepareProduct(isbn, productModelClass, catalogVersion);
		try
		{
			modelService.save(product);
			fail();
		}
		catch (ModelSavingException ex)
		{
			assertTrue(((InterceptorException) ex.getCause()).getInterceptor() instanceof ProductIsbnValidateInterceptor);
		}
	}

	private <T extends ProductModel> T prepareProduct(final String isbn, final Class<T> productModelClass,
			final CatalogVersionModel catalogVersion)
	{
		final T product = modelService.create(productModelClass);
		product.setCode("Test product code 123456");
		product.setName(TEST_PRODUCT_NAME_1);
		product.setDescription(TEST_PRODUCT_DESCRIPTION_1);
		product.setEditionFormat(ProductEditionFormat.DIGITAL_AND_PHYSICAL);
		product.setIsbn(isbn);
		product.setCatalogVersion(catalogVersion);
		product.setUnit(createUnit(UNIT_CODE, UNIT_TYPE));

		if (product instanceof VariantProductModel)
		{
			product.setSupercategories(Arrays.asList(welTypeEbook, welPartPart1));
			((VariantProductModel) product).setBaseProduct(baseProduct);
		}
		else if (product instanceof SubscriptionProductModel)
		{
			product.setSupercategories(Arrays.asList(welCpaCategory));
			((SubscriptionProductModel) product).setSubscriptionTerm(createSubscriptionTerm());
		}
		return product;
	}

	private UnitModel createUnit(final String code, final String type)
	{
		final UnitModel unit = modelService.create(UnitModel.class);
		unit.setCode(code);
		unit.setUnitType(type);
		modelService.save(unit);
		return unit;
	}

	private SubscriptionTermModel createSubscriptionTerm()
	{
		final SubscriptionTermModel subscriptionTerm = modelService.create(SubscriptionTermModel.class);
		subscriptionTerm.setId(TEST_SUBSCRIPTION_TERM_ID);
		subscriptionTerm.setName(TEST_SUBSCRIPTION_TERM_NAME);
		subscriptionTerm.setTermOfServiceNumber(1);
		subscriptionTerm.setTermOfServiceFrequency(TermOfServiceFrequency.MONTHLY);
		modelService.save(subscriptionTerm);
		return subscriptionTerm;
	}
}
