package com.wiley.core.product.dao;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.enums.ProductEditionFormat;
import com.wiley.core.model.WileyProductModel;
import com.wiley.core.model.WileyVariantProductModel;
import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.product.UnitService;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.variants.model.VariantTypeModel;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;


/**
 * The test checks if {@link com.wiley.core.model.WileyVariantProductModel} is saved to db
 * correctly. Created by Aliaksei_Zlobich on 10/16/2015.
 */
@IntegrationTest
public class WileyVariantProductIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{

	/**
	 * The constant CATALOG_ID.
	 */
	public static final String CATALOG_ID = "welProductCatalog";
	/**
	 * The constant CATALOG_VERSION.
	 */
	public static final String CATALOG_VERSION = "Online";
	/**
	 * The constant WEL_TYPE_PRINT.
	 */
	public static final String WEL_TYPE_PRINT = "WEL_TYPE_PRINT";
	/**
	 * The constant WEL_TYPE_EBOOK.
	 */
	public static final String WEL_TYPE_EBOOK = "WEL_TYPE_EBOOK";
	/**
	 * The constant WEL_PART_PART_1.
	 */
	public static final String WEL_PART_PART_1 = "WEL_PART_PART_1";
	/**
	 * The constant VARIANT_TYPE.
	 */
	public static final String VARIANT_TYPE = "WileyVariantProduct";
	/**
	 * The constant CPA_PLATINUM.
	 */
	public static final String CPA_PLATINUM = "CPA-PLATINUM";
	/**
	 * The constant WEL_CMA_REVIEW_COURSE.
	 */
	public static final String WEL_CMA_REVIEW_COURSE = "WEL_CMA_REVIEW_COURSE";

	@Resource
	private ModelService modelService;

	@Resource
	private UnitService unitService;

	@Resource
	private TypeService typeService;

	@Resource
	private CatalogVersionService catalogVersionService;

	@Resource
	private CategoryService categoryService;

	@Resource
	private ProductService productService;


	/**
	 * Before.
	 *
	 * @throws Exception
	 * 		the exception
	 */
	@Before
	public void before() throws Exception
	{
		importCsv("/wileycore/test/product/dao/WileyVariantProductIntegrationTest/baseProducts.impex",
				DEFAULT_ENCODING);
		catalogVersionService.setSessionCatalogVersion(CATALOG_ID, CATALOG_VERSION);
	}

	/**
	 * Test save two wiley variant products success.
	 */
	@Test
	public void testSaveTwoWileyVariantProductsSuccess()
	{
		CategoryModel welTypePrint = categoryService.getCategoryForCode(WEL_TYPE_PRINT);
		CategoryModel welTypeEbook = categoryService.getCategoryForCode(WEL_TYPE_EBOOK);
		CategoryModel welPartPart1 = categoryService.getCategoryForCode(WEL_PART_PART_1);
		// When
		final WileyVariantProductModel wProduct1 = createAndSaveWileyVariantProduct(
				"TEST_WEL_CMA_REVIEW_COURSE_PRINT_PART_1",
				"Wiley CMAexcel Exam Review Course 2016: Part 1, Financial Planning, Performance and Control",
				Arrays.asList(welTypePrint, welPartPart1), VARIANT_TYPE, "123456789", WEL_CMA_REVIEW_COURSE);

		final WileyVariantProductModel wProduct2 = createAndSaveWileyVariantProduct("TEST_WEL_CMA_REVIEW_COURSE_EBOOK_PART_1",
				"Wiley CMAexcel Exam Review Course 2016: Part 1, Financial Planning, Performance and Control (VitalSource "
						+ "eBook)", Arrays.asList(welTypeEbook, welPartPart1), VARIANT_TYPE, "123456790", WEL_CMA_REVIEW_COURSE);

		// Then
		// we have different ISBN so expect that two products have been created.
		assertNotNull(wProduct1);
		assertNotNull(wProduct1.getPk());
		assertNotNull(wProduct2);
		assertNotNull(wProduct2.getPk());
	}

	/**
	 * Test save two wiley variant products unique index exception.
	 */
	@Test
	public void testSaveTwoWileyVariantProductsUniqueIndexException()
	{
		// Given
		final String isbn = "123456789";
		CategoryModel welTypePrint = categoryService.getCategoryForCode(WEL_TYPE_PRINT);
		CategoryModel welTypeEbook = categoryService.getCategoryForCode(WEL_TYPE_EBOOK);
		CategoryModel welPartPart1 = categoryService.getCategoryForCode(WEL_PART_PART_1);

		// When
		final WileyVariantProductModel wProduct1 = createAndSaveWileyVariantProduct("TEST_WEL_CMA_REVIEW_COURSE_PRINT_PART_1",
				"Wiley CMAexcel Exam Review Course 2016: Part 1, Financial Planning, Performance and Control", Arrays.asList(
				welTypePrint, welPartPart1),
				VARIANT_TYPE, isbn, WEL_CMA_REVIEW_COURSE);

		try
		{
			final WileyVariantProductModel wProduct2 = createAndSaveWileyVariantProduct(
					"TEST_WEL_CMA_REVIEW_COURSE_EBOOK_PART_1", "Platinum CPA Review Course-BEC", Arrays.asList(welTypeEbook,
					welPartPart1), VARIANT_TYPE, isbn,
					WEL_CMA_REVIEW_COURSE);
			fail("Expected DuplicateKeyException.");
		}
		catch (final ModelSavingException e)
		{
			// Then
			// Success
		}
	}

	/**
	 * Test save wiley product and wiley variant product.
	 */
	@Test
	public void testSaveWileyProductAndWileyVariantProduct()
	{
		// Given
		final String isbn = "123456789";
		CategoryModel welTypePrint = categoryService.getCategoryForCode(WEL_TYPE_PRINT);
		CategoryModel welPartPart1 = categoryService.getCategoryForCode(WEL_PART_PART_1);

		// When
		final WileyProductModel product1 = createAndSaveProduct("CPA-PLATINUM-TEST", "Platinum Test",
				"cpa");
		final WileyVariantProductModel wProduct1 = createAndSaveWileyVariantProduct("TEST_WEL_CMA_REVIEW_COURSE_PRINT_PART_1",
				"Wiley CMAexcel Exam Review Course 2016: Part 1, Financial Planning, Performance and Control", Arrays.asList(
						welTypePrint, welPartPart1),
				VARIANT_TYPE, isbn, WEL_CMA_REVIEW_COURSE);

		// Then
		assertNotNull(product1);
		assertNotNull(product1.getPk());
		assertNotNull(wProduct1);
		assertNotNull(wProduct1.getPk());
	}

	private WileyVariantProductModel createAndSaveWileyVariantProduct(final String code,
			final String name, final List<CategoryModel> supercategories, final String variantType, final String isbn,
			final String baseProductCode)
	{
		final WileyVariantProductModel wProduct = modelService.create(WileyVariantProductModel.class);
		wProduct.setCode(code);
		wProduct.setName(name);
		wProduct.setSupercategories(supercategories);
		wProduct.setVariantType((VariantTypeModel) typeService.getComposedTypeForCode(variantType));
		wProduct
				.setCatalogVersion(catalogVersionService.getCatalogVersion(CATALOG_ID, CATALOG_VERSION));
		wProduct.setApprovalStatus(ArticleApprovalStatus.APPROVED);
		wProduct.setIsbn(isbn);
		wProduct.setBaseProduct(productService.getProductForCode(baseProductCode));
		wProduct.setEditionFormat(ProductEditionFormat.DIGITAL_AND_PHYSICAL);
		modelService.save(wProduct);
		return wProduct;
	}

	private WileyProductModel createAndSaveProduct(final String code, final String name,
			final String category)
	{
		final WileyProductModel wProduct = modelService.create(WileyProductModel.class);
		wProduct.setCode(code);
		wProduct.setName(name);
		wProduct.setSupercategories(Arrays.asList(categoryService.getCategoryForCode(category)));
		wProduct
				.setCatalogVersion(catalogVersionService.getCatalogVersion(CATALOG_ID, CATALOG_VERSION));
		wProduct.setEditionFormat(ProductEditionFormat.DIGITAL_AND_PHYSICAL);
		modelService.save(wProduct);
		return wProduct;
	}

}
