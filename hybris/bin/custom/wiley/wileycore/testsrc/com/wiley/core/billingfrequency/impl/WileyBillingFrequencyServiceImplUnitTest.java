package com.wiley.core.billingfrequency.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.subscriptionservices.model.BillingFrequencyModel;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.billingfrequency.dao.WileyBillingFrequencyDao;


/**
 * Default unit test for {@link WileyBillingFrequencyServiceImpl}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyBillingFrequencyServiceImplUnitTest
{

	@Mock
	private WileyBillingFrequencyDao wileyBillingFrequencyDaoMock;

	@InjectMocks
	private WileyBillingFrequencyServiceImpl wileyBillingFrequencyService;

	// Test data
	private static final String TEST_BILLING_FREQUENCY_CODE = "test code";

	@Mock
	private BillingFrequencyModel billingFrequencyModelMock;

	@Test
	public void testGetBillingFrequencyByCodeSuccessCase()
	{
		// Given
		when(wileyBillingFrequencyDaoMock.findBillingFrequenciesByCode(eq(TEST_BILLING_FREQUENCY_CODE)))
				.thenReturn(Arrays.asList(billingFrequencyModelMock));
		// When
		BillingFrequencyModel actualBillingFrequency = wileyBillingFrequencyService.getBillingFrequencyByCode(
				TEST_BILLING_FREQUENCY_CODE);

		// Then
		assertSame(billingFrequencyModelMock, actualBillingFrequency);
	}

	@Test
	public void testGetBillingFrequencyByCodeWhenModelIsNotFound()
	{
		// Given
		when(wileyBillingFrequencyDaoMock.findBillingFrequenciesByCode(eq(TEST_BILLING_FREQUENCY_CODE)))
				.thenReturn(Collections.emptyList());
		// When
		try
		{
			wileyBillingFrequencyService.getBillingFrequencyByCode(
					TEST_BILLING_FREQUENCY_CODE);
			fail("Expected " + UnknownIdentifierException.class);
		}
		catch (UnknownIdentifierException e)
		{
			// Success
			// Then
			verify(wileyBillingFrequencyDaoMock).findBillingFrequenciesByCode(eq(TEST_BILLING_FREQUENCY_CODE));
		}
	}

	@Test
	public void testGetBillingFrequencyByCodeWhenMoreThenOneItemFound()
	{
		// Given
		when(wileyBillingFrequencyDaoMock.findBillingFrequenciesByCode(eq(TEST_BILLING_FREQUENCY_CODE)))
				.thenReturn(Arrays.asList(billingFrequencyModelMock, mock(BillingFrequencyModel.class)));
		// When
		try
		{
			wileyBillingFrequencyService.getBillingFrequencyByCode(
					TEST_BILLING_FREQUENCY_CODE);
			fail("Expected " + AmbiguousIdentifierException.class);
		}
		catch (AmbiguousIdentifierException e)
		{
			// Success
			// Then
			verify(wileyBillingFrequencyDaoMock).findBillingFrequenciesByCode(eq(TEST_BILLING_FREQUENCY_CODE));
		}
	}

	@Test
	public void testGetBillingFrequencyByCodeWithNotValidParameters()
	{
		// Given
		// No changes in test data.

		// When
		try
		{
			wileyBillingFrequencyService.getBillingFrequencyByCode(null);
			fail("Expected " + IllegalArgumentException.class);
		}
		catch (IllegalArgumentException e)
		{
			// Success
			// Then
			verify(wileyBillingFrequencyDaoMock, never()).findBillingFrequenciesByCode(any());
		}
	}


}