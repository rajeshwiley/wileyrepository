package com.wiley.core.order.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.order.CommerceAddToCartStrategy;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationStatus;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.order.CartService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatcher;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.product.WileyCountableProductService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;


/**
 * Unit test for {@link WileyProductQuantityInCartManagerImpl}.<br/>
 *
 * Unit test checks CommerceAddToCart behavior which is implemented by WileyProductQuantityInCartManagerImpl.
 *
 * @author Aliaksei_Zlobich
 */
@UnitTest
public class WileyProductQuantityInCartManagerAddToCartUnitTestImpl
{
	@InjectMocks
	private WileyProductQuantityInCartManagerImpl wileyProductQuantityInCartManagerImpl;

	@Mock
	private CartService cartServiceMock;

	@Mock
	private CommerceAddToCartStrategy commerceAddToCartStrategyMock;

	// Test data
	@Mock
	private CartModel cartModelMock;
	@Mock
	private ProductModel productModelMock;
	@Mock
	private CommerceCartModification commerceCartModificationMock;
	@Mock
	private WileyCountableProductService wileyCountableProductServiceMock;

	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);

		when(commerceAddToCartStrategyMock.addToCart(any(CommerceCartParameter.class))).thenReturn(commerceCartModificationMock);

		when(wileyCountableProductServiceMock.canProductHaveQuantity(productModelMock)).thenReturn(false);
	}

	@Test
	public void testAddToCartWithEditionFormatChecks1() throws Exception
	{
		// Given
		CommerceCartParameter parameter = new CommerceCartParameter();
		parameter.setCart(cartModelMock);
		parameter.setProduct(productModelMock);
		final int quantity = 1234;
		parameter.setQuantity(quantity);

		List<CartEntryModel> cartEntryModelList = new ArrayList<>();
		final CartEntryModel cartEntryModelMock = mock(CartEntryModel.class);
		cartEntryModelList.add(cartEntryModelMock);

		when(cartServiceMock.getEntriesForProduct(cartModelMock, productModelMock)).thenReturn(cartEntryModelList);

		// When
		final CommerceCartModification modification = wileyProductQuantityInCartManagerImpl.addToCart(parameter, true);

		// Then
		verifyZeroInteractions(commerceAddToCartStrategyMock);
		assertNotNull(modification);
		assertSame("Expected existing cart entry after successful cart modification.", cartEntryModelMock,
				modification.getEntry());
		assertSame(productModelMock, modification.getProduct());
		assertEquals(quantity, modification.getQuantity());
		assertEquals(0, modification.getQuantityAdded());
		assertEquals(CommerceCartModificationStatus.SUCCESS, modification.getStatusCode());
	}

	@Test
	public void testAddToCartWithEditionFormatChecks2() throws Exception
	{
		// Given
		CommerceCartParameter parameter = new CommerceCartParameter();
		parameter.setCart(cartModelMock);
		parameter.setProduct(productModelMock);
		final int quantity = 1234;
		parameter.setQuantity(quantity);

		when(cartServiceMock.getEntriesForProduct(cartModelMock, productModelMock)).thenReturn(
				Collections.<CartEntryModel> emptyList());

		// When
		final CommerceCartModification modification = wileyProductQuantityInCartManagerImpl.addToCart(parameter, true);

		// Then
		verify(commerceAddToCartStrategyMock).addToCart(argThat(new ArgumentMatcher<CommerceCartParameter>()
		{
			@Override
			public boolean matches(final Object o)
			{
				CommerceCartParameter param = (CommerceCartParameter) o;
				return param.getQuantity() == WileyCoreConstants.DEFAULT_QUANTITY_FOR_PRODUCT_WHICH_CANNOT_HAVE_QUANTITY;
			}
		}));
	}

	@Test
	public void testAddToCartWithoutEditionFormatChecks() throws Exception
	{
		// Given
		CommerceCartParameter parameter = new CommerceCartParameter();
		parameter.setCart(cartModelMock);
		parameter.setProduct(productModelMock);

		// When
		final CommerceCartModification modification = wileyProductQuantityInCartManagerImpl.addToCart(parameter, false);

		// Then
		verifyZeroInteractions(wileyCountableProductServiceMock, cartServiceMock);
		verify(commerceAddToCartStrategyMock).addToCart(eq(parameter));
	}

	@Test
	public void testAddToCartOnlyCommerceParam() throws Exception
	{
		// Given
		CommerceCartParameter parameterMock = mock(CommerceCartParameter.class);
		CommerceCartModification modificationMock = mock(CommerceCartModification.class);

		wileyProductQuantityInCartManagerImpl = spy(wileyProductQuantityInCartManagerImpl);

		doReturn(modificationMock).when(wileyProductQuantityInCartManagerImpl).addToCart(eq(parameterMock), eq(true));

		// When
		final CommerceCartModification modification = wileyProductQuantityInCartManagerImpl.addToCart(parameterMock);

		// Then
		verify(wileyProductQuantityInCartManagerImpl).addToCart(eq(parameterMock), eq(true));
		assertEquals(modificationMock, modification);
	}
}
