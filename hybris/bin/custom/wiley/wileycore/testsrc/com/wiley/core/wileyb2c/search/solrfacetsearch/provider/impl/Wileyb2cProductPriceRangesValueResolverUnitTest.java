package com.wiley.core.wileyb2c.search.solrfacetsearch.provider.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.RangeNameProvider;
import de.hybris.platform.util.PriceValue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import java.util.Collections;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import static de.hybris.platform.commerceservices.search.solrfacetsearch.provider.impl.ProductPricesValueResolver.OPTIONAL_PARAM;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cProductPriceRangesValueResolverUnitTest
{
	private static final double PRICE_VALUE = 40.0;
	private static final String FIELD_QUALIFIER = "fieldQualifier";
	private static final String CURRENCY = "currency";
	private static final String RANGE = "range";
	private static final String FIELD_NAME = "fieldName";
	@Spy
	@InjectMocks
	private Wileyb2cProductPriceRangesValueResolver priceRangesValueResolver =
			new Wileyb2cProductPriceRangesValueResolver();
	@Mock
	private RangeNameProvider rangeNameProvider;
	@Mock
	private FieldNameProvider fieldNameProvider;
	@Mock
	private InputDocument document;
	@Mock
	private IndexerBatchContext batchContext;
	@Mock
	private IndexedProperty indexedProperty;
	@Mock
	private ProductModel productModel;
	@Mock
	private PriceInformation priceInformation;
	@Mock
	private PriceValue priceValue;
	@Mock
	private Wileyb2cCountryAndCurrencyQualifierProvider.Wileyb2cCountryAndCurrencyQualifier countryAndCurrencyQualifier;
	@Mock
	private CurrencyModel currrency;

	@Test
	public void addFieldValuesWhenNoPricesAndOptionalShouldDoNothing() throws FieldValueProviderException
	{
		doReturn(null).when(priceRangesValueResolver).getQualifierData(null);

		priceRangesValueResolver.addFieldValues(document, batchContext, indexedProperty, productModel, null);

		verifyZeroInteractions(document);
	}

	@Test(expected = FieldValueProviderException.class)
	public void addFieldValuesWhenNoPricesAndRequiredShouldThrowException() throws FieldValueProviderException
	{
		doReturn(null).when(priceRangesValueResolver).getQualifierData(null);
		when(indexedProperty.getValueProviderParameters()).thenReturn(Collections.singletonMap(OPTIONAL_PARAM, "false"));

		priceRangesValueResolver.addFieldValues(document, batchContext, indexedProperty, productModel, null);
	}


	@Test
	public void addFieldValuesWhenPricesShouldAddFields() throws FieldValueProviderException
	{
		doReturn(Collections.singletonList(priceInformation)).when(priceRangesValueResolver).getQualifierData(null);
		when(priceInformation.getPriceValue()).thenReturn(priceValue);
		when(priceValue.getValue()).thenReturn(PRICE_VALUE);
		doReturn(countryAndCurrencyQualifier).when(priceRangesValueResolver).getQualifier(null);
		when(countryAndCurrencyQualifier.getCurrency()).thenReturn(currrency);
		when(currrency.getIsocode()).thenReturn(CURRENCY);
		when(rangeNameProvider.getRangeNameList(indexedProperty, PRICE_VALUE, CURRENCY)).thenReturn(Collections.singletonList(
				RANGE));
		doReturn(FIELD_QUALIFIER).when(priceRangesValueResolver).getFieldQualifier(null);
		when(fieldNameProvider.getFieldNames(indexedProperty, FIELD_QUALIFIER)).thenReturn(Collections.singletonList(FIELD_NAME));

		priceRangesValueResolver.addFieldValues(document, batchContext, indexedProperty, productModel, null);

		verify(document).addField(FIELD_NAME, RANGE);
	}
}
