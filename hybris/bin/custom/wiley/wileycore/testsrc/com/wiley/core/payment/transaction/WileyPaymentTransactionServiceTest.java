package com.wiley.core.payment.transaction;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.paypal.hybris.data.ResultErrorData;
import com.wiley.core.payment.enums.WileyTransactionStatusEnum;
import com.wiley.core.payment.transaction.impl.WileyPaymentTransactionService;

import static junit.framework.TestCase.assertNull;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyPaymentTransactionServiceTest
{
	public static final String PAYMENT_TRANSACTION_CODE = "PAYMENT_TRANSACTION_CODE";
	public static final String CODE_ONE_CODE = "codeOneCode";
	public static final String ERROR_ONE_MESSAGE = "errorOneMessage";
	public static final String CODE_TWO_CODE = "codeTwoCode";
	public static final String ERROR_TWO_MESSAGE = "errorTwoMessage";
	public static final String PAYPAL_ERROR_FORMAT = "%s-%s. ";
	public static final int MAX_PAYPAL_ERROR_LENGTH = 255;
	final WileyPaymentTransactionService testInstance = new WileyPaymentTransactionService();

	@Mock
	protected OrderModel orderModel;
	@Mock
	private PaymentTransactionEntryModel paymentTransactionEntryModel;

	@Before
	public void setUp()
	{
		PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);
		List<PaymentTransactionModel> transactionModelList =
				Collections.singletonList(paymentTransactionModel);
		when(orderModel.getPaymentTransactions()).thenReturn(transactionModelList);
		when(paymentTransactionModel.getOrder()).thenReturn(orderModel);

		paymentTransactionEntryModel = mock(PaymentTransactionEntryModel.class);
		when(paymentTransactionEntryModel.getType()).thenReturn(PaymentTransactionType.CAPTURE);
		when(paymentTransactionEntryModel.getCode()).thenReturn(PAYMENT_TRANSACTION_CODE);
		when(paymentTransactionModel.getEntries()).thenReturn(Arrays.asList(paymentTransactionEntryModel));
	}

	@Test
	public void testGetSuccessfulCaptureTransactionEntry()
	{
		//Given
		when(paymentTransactionEntryModel.getTransactionStatus()).thenReturn(TransactionStatus.ACCEPTED.name());
		//Testing
		final PaymentTransactionEntryModel transactionEntry = testInstance.getAcceptedTransactionEntry(orderModel,
				PaymentTransactionType.CAPTURE);
		//Verify
		assertEquals("PaymentTransactionId is not correct", PAYMENT_TRANSACTION_CODE, transactionEntry.getCode());
	}

	@Test
	public void testReturnNullForDeclinedTransactionEntry()
	{
		//Given
		when(paymentTransactionEntryModel.getTransactionStatus()).thenReturn(WileyTransactionStatusEnum.DECLINED.toString());
		//Testing
		final PaymentTransactionEntryModel transactionEntry = testInstance.getAcceptedTransactionEntry(orderModel,
				PaymentTransactionType.CAPTURE);
		//Verify
		assertNull(transactionEntry);
	}

	/**
	 * test of getAcceptedTransactionEntries method
	 */
	@Test
	public void shouldReturnOnlyAcceptedTransactionEntries()
	{
		//Given
		PaymentTransactionEntryModel acceptedRefundEntry1 = givenTransactionEntry(PaymentTransactionType.REFUND_FOLLOW_ON,
				TransactionStatus.ACCEPTED);
		PaymentTransactionEntryModel acceptedRefundEntry2 = givenTransactionEntry(PaymentTransactionType.REFUND_FOLLOW_ON,
				TransactionStatus.ACCEPTED);
		PaymentTransactionEntryModel rejectedRefundEntry = givenTransactionEntry(PaymentTransactionType.REFUND_FOLLOW_ON,
				TransactionStatus.REJECTED);

		PaymentTransactionModel transaction = givenTransactionModel(
				acceptedRefundEntry1,
				acceptedRefundEntry2,
				rejectedRefundEntry
		);
		//Testing
		List<PaymentTransactionEntryModel> resultEntries =
				testInstance.getAcceptedTransactionEntries(transaction, PaymentTransactionType.REFUND_FOLLOW_ON);
		//Verify
		assertEquals(2, resultEntries.size());
		resultEntries.forEach(entry -> assertEquals(TransactionStatus.ACCEPTED.name(), entry.getTransactionStatus()));
	}

	/**
	 * test of getAcceptedTransactionEntries method
	 */
	@Test
	public void shouldReturnOnlyEntriesOfSpecifiedType()
	{
		//Given
		PaymentTransactionEntryModel refundEntry1 = givenTransactionEntry(PaymentTransactionType.REFUND_FOLLOW_ON,
				TransactionStatus.ACCEPTED);
		PaymentTransactionEntryModel refundEntry2 = givenTransactionEntry(PaymentTransactionType.REFUND_FOLLOW_ON,
				TransactionStatus.ACCEPTED);
		PaymentTransactionEntryModel captureEntry = givenTransactionEntry(PaymentTransactionType.CAPTURE,
				TransactionStatus.ACCEPTED);

		PaymentTransactionModel transaction = givenTransactionModel(
				refundEntry1,
				refundEntry2,
				captureEntry
		);
		//Testing
		List<PaymentTransactionEntryModel> resultEntries =
				testInstance.getAcceptedTransactionEntries(transaction, PaymentTransactionType.REFUND_FOLLOW_ON);
		//Verify
		assertEquals(2, resultEntries.size());
		resultEntries.forEach(entry -> assertEquals(PaymentTransactionType.REFUND_FOLLOW_ON, entry.getType()));
	}

	/**
	 * test of getAcceptedTransactionEntries method
	 */
	@Test
	public void shouldReturnEmptyListIfNoTransactionsOfSpecifiedType()
	{
		//Given
		PaymentTransactionEntryModel captureEntry = givenTransactionEntry(PaymentTransactionType.CAPTURE,
				TransactionStatus.ACCEPTED);

		PaymentTransactionModel transaction = givenTransactionModel(
				captureEntry
		);
		//Testing
		List<PaymentTransactionEntryModel> resultEntries =
				testInstance.getAcceptedTransactionEntries(transaction, PaymentTransactionType.REFUND_FOLLOW_ON);
		//Verify
		assertEquals(0, resultEntries.size());
	}

	/**
	 * test of getAcceptedTransactionEntries method
	 */
	@Test(expected = NullPointerException.class)
	public void shouldThrowExceptionIfTransactionIsNull()
	{
		testInstance.getAcceptedTransactionEntries((PaymentTransactionModel) null, PaymentTransactionType.REFUND_FOLLOW_ON);
	}

	/**
	 * test of getAcceptedTransactionEntries method
	 */
	@Test(expected = NullPointerException.class)
	public void shouldThrowExceptionIfTransactionTypeIsNull()
	{
		testInstance.getAcceptedTransactionEntries(new PaymentTransactionModel(), null);
	}


	@Test
	public void shouldCreatePaypalErrorStringFromAllErrors() {
		ResultErrorData errorOne = new ResultErrorData();
		errorOne.setErrorCode(CODE_ONE_CODE);
		errorOne.setLongMessage(ERROR_ONE_MESSAGE);
		ResultErrorData errorTwo = new ResultErrorData();
		errorTwo.setErrorCode(CODE_TWO_CODE);
		errorTwo.setLongMessage(ERROR_TWO_MESSAGE);

		final String payPalErrorDetails = testInstance.createPayPalErrorDetails(Arrays.asList(errorOne, errorTwo));

		assertEquals(String.format(PAYPAL_ERROR_FORMAT, CODE_ONE_CODE, ERROR_ONE_MESSAGE)
				+ String.format(PAYPAL_ERROR_FORMAT, CODE_TWO_CODE, ERROR_TWO_MESSAGE), payPalErrorDetails);
	}

	@Test
	public void shouldCreatePayPalErrorWithLimitedLength() {
		ResultErrorData errorOne = new ResultErrorData();
		errorOne.setErrorCode(CODE_ONE_CODE);
		errorOne.setLongMessage(StringUtils.repeat('s', 1024));

		final String payPalErrorDetails = testInstance.createPayPalErrorDetails(Arrays.asList(errorOne));

		assertEquals(MAX_PAYPAL_ERROR_LENGTH, payPalErrorDetails.length());
	}

	private PaymentTransactionModel givenTransactionModel(final PaymentTransactionEntryModel... entries)
	{
		PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);
		when(paymentTransactionModel.getEntries()).thenReturn(Arrays.asList(entries));
		return paymentTransactionModel;
	}

	private PaymentTransactionEntryModel givenTransactionEntry(final PaymentTransactionType transactionType,
			final TransactionStatus transactionStatus)
	{
		PaymentTransactionEntryModel entry = mock(PaymentTransactionEntryModel.class);
		when(entry.getType()).thenReturn(transactionType);
		when(entry.getTransactionStatus()).thenReturn(transactionStatus.name());
		return entry;
	}
}