package com.wiley.core.mpgs.services;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.ApplicationContext;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.model.WileyTnsMerchantModel;
import com.wiley.core.mpgs.services.impl.WileyMPGSPaymentServiceImpl;
import com.wiley.core.mpgs.services.strategies.CardVerificationStrategy;

import static com.wiley.core.constants.WileyCoreConstants.WILEY_COM_SITE_ID;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyMPGSPaymentServiceImplVerifyTest
{
	private static final String SESSION_ID = "TEST_SESSION_ID";
	private static final String CURRENCY_ISO = "USD";
	private static final String TNS_MERCHANT_ID = "TNS_MERCHANT_ID";
	private static final int GUID = 1234567;

	@Mock
	private WileyMPGSMerchantService wileyMPGSMerchantService;

	@InjectMocks
	private WileyMPGSPaymentServiceImpl wileyMPGSPaymentService;

	@Mock
	private ApplicationContext applicationContext;

	@Mock
	private CardVerificationStrategy cardVerificationStrategy;

	@Test
	public void shouldCreateVerifyEntryTest()
	{
		CartModel cartModel = new CartModel();
		cartModel.setGuid(String.valueOf(GUID));

		CurrencyModel currencyModel = new CurrencyModel();
		currencyModel.setIsocode(CURRENCY_ISO);
		cartModel.setCurrency(currencyModel);

		BaseSiteModel site = new BaseSiteModel();
		site.setUid(WILEY_COM_SITE_ID);
		cartModel.setSite(site);

		WileyTnsMerchantModel merchant = new WileyTnsMerchantModel();
		merchant.setId(TNS_MERCHANT_ID);
		merchant.setEnableVerification(true);
		when(wileyMPGSMerchantService.getMerchant(cartModel)).thenReturn(merchant);
		when(applicationContext.getBean(WileyCoreConstants.DEFAULT_MPGS_CARD_VERIFY_STRATEGY, CardVerificationStrategy.class))
				.thenReturn(cardVerificationStrategy);

		PaymentTransactionEntryModel transactionEntryModel = new PaymentTransactionEntryModel();
		when(cardVerificationStrategy.verify(cartModel, SESSION_ID)).thenReturn(transactionEntryModel);

		final PaymentTransactionEntryModel transactionEntry = wileyMPGSPaymentService.verify(cartModel, SESSION_ID);

		assertSame(transactionEntryModel, transactionEntry);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldNotCallVerifyIfNull()
	{
		wileyMPGSPaymentService.verify(null, null);
	}
}
