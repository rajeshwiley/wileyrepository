package com.wiley.core.servicelayer.dao.processdefinition.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.processengine.model.DynamicProcessDefinitionModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyProcessDefinitionDaoImplUnitTest
{
	private static final String ALL_DEFINITIONS_QUERY = "select {pk} from {DynamicProcessDefinition} order by {code},{version}";
	private static final String DEFINITIONS_BY_CODE_QUERY =
			"select {pk} from {DynamicProcessDefinition} where {code}=?code order by {code},{version}";

	@Mock
	private FlexibleSearchService flexibleSearchServiceMock;

	@Mock
	private SearchResult searchResultMock;

	@InjectMocks
	private WileyProcessDefinitionDaoImpl dao;

	@Test
	public void findAllProcessDefinitions()
	{
		final List<DynamicProcessDefinitionModel> resultList = new ArrayList<>();
		when(flexibleSearchServiceMock.search(ALL_DEFINITIONS_QUERY)).thenReturn(searchResultMock);
		when(searchResultMock.getResult()).thenReturn(resultList);
		final List<DynamicProcessDefinitionModel> result = dao.findAllProcessDefinitions();

		verify(flexibleSearchServiceMock, atLeastOnce()).search(ALL_DEFINITIONS_QUERY);
		verify(searchResultMock, atLeastOnce()).getResult();
		assertEquals(resultList, result);
	}

	@Test
	public void findProcessDefinitionsByCode()
	{
		final String testCode = "testCode";
		final Map<String, String> parameters = Collections.singletonMap("code", testCode);
		final List<DynamicProcessDefinitionModel> resultList = new ArrayList<>();
		when(flexibleSearchServiceMock.search(DEFINITIONS_BY_CODE_QUERY, parameters)).thenReturn(searchResultMock);
		when(searchResultMock.getResult()).thenReturn(resultList);
		final List<DynamicProcessDefinitionModel> result = dao.findProcessDefinitionsByCode(testCode);

		verify(flexibleSearchServiceMock, atLeastOnce()).search(DEFINITIONS_BY_CODE_QUERY, parameters);
		verify(searchResultMock, atLeastOnce()).getResult();
		assertEquals(resultList, result);
	}
}