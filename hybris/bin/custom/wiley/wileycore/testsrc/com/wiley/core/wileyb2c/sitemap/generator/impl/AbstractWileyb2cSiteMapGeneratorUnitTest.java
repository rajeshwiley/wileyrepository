package com.wiley.core.wileyb2c.sitemap.generator.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.sitemap.data.SiteMapUrlData;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.Lists;


/**
 * Created by Maksim_Kozich on 25.09.17.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AbstractWileyb2cSiteMapGeneratorUnitTest
{
	private static final String LOC1 = "loc1";

	private static final String LOC2 = "loc2";

	private static final String LOC3 = "loc3";

	@Mock
	private SiteMapUrlData siteMapUrlDataMock1;

	@Mock
	private SiteMapUrlData siteMapUrlDataMock2;

	@Mock
	private SiteMapUrlData siteMapUrlDataMock3;

	private ArrayList<SiteMapUrlData> siteMapUrlDataList;

	@InjectMocks
	private AbstractWileyb2cSiteMapGeneratorTestable instanceToTest;

	@Before
	public void setUp() throws Exception
	{
		when(siteMapUrlDataMock1.getLoc()).thenReturn(LOC1);
		when(siteMapUrlDataMock2.getLoc()).thenReturn(LOC2);
		when(siteMapUrlDataMock3.getLoc()).thenReturn(LOC3);
		siteMapUrlDataList = Lists.newArrayList(siteMapUrlDataMock1, siteMapUrlDataMock2,
				siteMapUrlDataMock3);
	}

	@Test(expected = IllegalArgumentException.class)
	public void throwsExceptionIfListIsNull()
	{
		// Given

		// When
		instanceToTest.validateSiteMapUrlData(null);

		// Then
		// expected IllegalArgumentException exception
	}

	@Test(expected = IllegalArgumentException.class)
	public void throwsExceptionIfOneOfListElementsIsNull()
	{
		// Given
		siteMapUrlDataList.add(2, null);

		// When
		instanceToTest.validateSiteMapUrlData(siteMapUrlDataList);

		// Then
		// expected IllegalArgumentException exception
	}

	@Test(expected = IllegalArgumentException.class)
	public void throwsExceptionIfOneOfListElementsHasNullLoc()
	{
		// Given
		when(siteMapUrlDataMock2.getLoc()).thenReturn(null);

		// When
		instanceToTest.validateSiteMapUrlData(siteMapUrlDataList);

		// Then
		// expected IllegalArgumentException exception
	}

	@Test
	public void doesNotThrowExceptionIfAllElementsOk()
	{
		// Given

		// When
		instanceToTest.validateSiteMapUrlData(siteMapUrlDataList);

		// Then
		// no IllegalArgumentException exception
	}
}

/** This is needed, because we cannot construct abstract class directly */
class AbstractWileyb2cSiteMapGeneratorTestable extends AbstractWileyb2cSiteMapGenerator<String>
{
	@Override
	public List<SiteMapUrlData> getSiteMapUrlData(final List<String> models)
	{
		return null;
	}

	@Override
	protected List<String> getDataInternal(final CMSSiteModel siteModel)
	{
		return null;
	}
}
