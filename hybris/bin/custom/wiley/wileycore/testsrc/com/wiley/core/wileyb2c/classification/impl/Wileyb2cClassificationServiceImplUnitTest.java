package com.wiley.core.wileyb2c.classification.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.wileyb2c.classification.Wileyb2cClassificationAttributes;
import com.wiley.core.wileyb2c.classification.Wileyb2cResolveClassificationAttributeStrategy;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cClassificationServiceImplUnitTest
{
	private static final Wileyb2cClassificationAttributes CLASSIFICATION_ATTRIBUTE =
			Wileyb2cClassificationAttributes.PUBLICATION_DATE;
	private static final String RESOLVED_VALUE = "resolvedValue";
	@InjectMocks
	private Wileyb2cClassificationServiceImpl wileyb2cClassificationService;
	@Mock
	private ProductModel productModel;
	@Mock
	private Wileyb2cResolveClassificationAttributeStrategy wileyb2cResolveClassificationAttributeStrategy;
	@Mock
	private CategoryModel categoryModel;
	@Mock
	private ClassificationClassModel classificationClassModel1;
	@Mock
	private ClassificationClassModel classificationClassModel2;

	private List<Wileyb2cResolveClassificationAttributeStrategy> strategies =
			new ArrayList<Wileyb2cResolveClassificationAttributeStrategy>();

	@Before
	public void init()
	{
		strategies.add(wileyb2cResolveClassificationAttributeStrategy);
		wileyb2cClassificationService.setStrategies(strategies);
	}

	@Test(expected = IllegalStateException.class)
	public void resolveAttributesWhenNoApplicableStrategyShouldThrowException()
	{
		when(wileyb2cResolveClassificationAttributeStrategy.apply(CLASSIFICATION_ATTRIBUTE))
				.thenReturn(Boolean.FALSE);

		wileyb2cClassificationService.resolveAttribute(productModel, CLASSIFICATION_ATTRIBUTE);
	}

	@Test
	public void resolveAttributesWhenApplicableStrategyShouldResolve()
	{
		when(wileyb2cResolveClassificationAttributeStrategy.apply(CLASSIFICATION_ATTRIBUTE))
				.thenReturn(true);
		when(wileyb2cResolveClassificationAttributeStrategy.resolveAttribute(productModel, CLASSIFICATION_ATTRIBUTE))
				.thenReturn(RESOLVED_VALUE);

		final String resolveAttribute = wileyb2cClassificationService.resolveAttribute(productModel, CLASSIFICATION_ATTRIBUTE);

		assertEquals(resolveAttribute, RESOLVED_VALUE);
	}

	public void resolveClassificationClass()
	{
		when(productModel.getSupercategories()).thenReturn(
				Arrays.asList(classificationClassModel1, categoryModel));

		ClassificationClassModel result = wileyb2cClassificationService.resolveClassificationClass(productModel);

		assertEquals(classificationClassModel1, result);
	}

	@Test(expected = IllegalStateException.class)
	public void resolveClassificationClassWhenMoreThenOneClassesFound()
	{
		when(productModel.getSupercategories()).thenReturn(
				Arrays.asList(classificationClassModel1, classificationClassModel2));

		wileyb2cClassificationService.resolveClassificationClass(productModel);
	}

	@Test(expected = IllegalArgumentException.class)
	public void resolveClassificationClassWhenNoClassesFound()
	{
		when(productModel.getSupercategories()).thenReturn(Collections.emptyList());

		wileyb2cClassificationService.resolveClassificationClass(productModel);
	}
}
