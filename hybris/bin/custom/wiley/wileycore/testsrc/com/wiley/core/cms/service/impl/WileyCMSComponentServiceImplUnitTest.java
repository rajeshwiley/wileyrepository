package com.wiley.core.cms.service.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;
import de.hybris.platform.cms2.servicelayer.daos.CMSComponentDao;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.cms.service.WileyCMSComponentService;

import static java.util.Arrays.asList;
import static org.fest.util.Collections.set;


/**
 * Created by Sergiy_Mishkovets on 1/21/2018.
 */

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyCMSComponentServiceImplUnitTest
{
	private static final String COMPONENT_ID = "componentId";
	private static final String ONLY_ONLINE_COMPONENT_ID = "onlyOnlineComponentId";
	private static final String NOT_EXISTED_COMPONENT_ID = "notExistedComponentId";

	private static final String STAGED_VERSION = "Staged";
	private static final String ONLINE_VERSION = "Online";

	@InjectMocks
	private WileyCMSComponentService testInstance = new WileyCMSComponentServiceImpl();

	@Mock
	private CMSComponentDao mockCmsComponentDao;
	@Mock
	private CatalogService mockCatalogService;
	@Mock
	private CatalogVersionModel mockStagedCatalogVersion;
	@Mock
	private CatalogVersionModel mockOnlineCatalogVersion;
	@Mock
	private AbstractCMSComponentModel mockStagedComponent;
	@Mock
	private AbstractCMSComponentModel mockOnlineComponent;

	private Set<CatalogVersionModel> sessionCatalogVersions;


	@Before
	public void setUp()
	{
		sessionCatalogVersions = set(mockStagedCatalogVersion, mockOnlineCatalogVersion);
		when(mockCatalogService.getSessionCatalogVersions()).thenReturn(sessionCatalogVersions);

		when(mockCmsComponentDao.findCMSComponentsByIdAndCatalogVersions(COMPONENT_ID, sessionCatalogVersions))
				.thenReturn(
						asList(mockStagedComponent, mockOnlineComponent));
		when(mockCmsComponentDao.findCMSComponentsByIdAndCatalogVersions(ONLY_ONLINE_COMPONENT_ID, sessionCatalogVersions))
				.thenReturn(
						asList(mockOnlineComponent));
		when(mockCmsComponentDao.findCMSComponentsByIdAndCatalogVersions(NOT_EXISTED_COMPONENT_ID, sessionCatalogVersions))
				.thenReturn(Collections.emptyList());

		when(mockStagedCatalogVersion.getVersion()).thenReturn(STAGED_VERSION);
		when(mockOnlineCatalogVersion.getVersion()).thenReturn(ONLINE_VERSION);

		when(mockStagedComponent.getCatalogVersion()).thenReturn(mockStagedCatalogVersion);
		when(mockOnlineComponent.getCatalogVersion()).thenReturn(mockOnlineCatalogVersion);
	}

	@Test
	public void shouldReturnAbstractCMSComponentForVersion() throws CMSItemNotFoundException
	{
		AbstractCMSComponentModel component = testInstance.getAbstractCMSComponentForCatalogVersion(COMPONENT_ID, ONLINE_VERSION);
		Assert.assertEquals(mockOnlineComponent, component);
	}

	@Test(expected = CMSItemNotFoundException.class)
	public void shouldThrowExceptionIfThereIsNoComponentForVersion() throws CMSItemNotFoundException
	{
		testInstance.getAbstractCMSComponentForCatalogVersion(ONLY_ONLINE_COMPONENT_ID, STAGED_VERSION);
	}

	@Test(expected = CMSItemNotFoundException.class)
	public void shouldThrowExceptionIfThereAreNoComponentsForId() throws CMSItemNotFoundException
	{
		testInstance.getAbstractCMSComponentForCatalogVersion(NOT_EXISTED_COMPONENT_ID, ONLINE_VERSION);
	}

}
