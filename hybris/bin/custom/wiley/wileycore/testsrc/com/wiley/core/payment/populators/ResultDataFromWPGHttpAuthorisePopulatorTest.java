package com.wiley.core.payment.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static junit.framework.Assert.assertEquals;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ResultDataFromWPGHttpAuthorisePopulatorTest
{
	private static final String OPERATION_PARAM = "operation";
	private static final String RETURN_CODE_PARAM = "returnCode";
	private static final String RETURN_MESSAGE_PARAM = "returnMessage";
	private static final String VENDOR_ID_PARAM = "vendorID";
	private static final String TRANSACTION_ID_PARAM = "transID";
	private static final String MERCHANT_RESPONSE_PARAM = "merchantResponse";
	private static final String MERCHANT_ID_PARAM = "merchantID";
	private static final String AVS_ADDRESS_RESULT_PARAM = "AVSAddrResult";
	private static final String AVS_POST_RESULT_PARAM = "AVSPostResult";
	private static final String TOKEN_PARAM = "token";
	private static final String ACQUIRER_ID_PARAM = "acquirerID";
	private static final String ACQUIRER_NAME_PARAM = "acquirerName";
	private static final String BANK_ID_PARAM = "bankID";
	private static final String BANK_NAME_PARAM = "bankName";
	private static final String MASKED_CARD_NUMBER_PARAM = "maskedCardNumber";
	private static final String CARD_EXPIRY_PARAM = "cardExpiry";
	private static final String TIMESTAMP_PARAM = "timestamp";
	private static final String CSC_RESULT_PARAM = "CSCResult";
	private static final String SECURITY_PARAM = "security";
	private static final String VALUE_PARAM = "value";
	private static final String CURRENCY_PARAM = "currency";


	private static final String OPERATION = "operationValue";
	private static final String RETURN_CODE = "returnCodeValue";
	private static final String RETURN_MESSAGE = "returnMessageValue";
	private static final String VENDOR_ID = "vendorIDValue";
	private static final String TRANSACTION_ID = "transIDValue";
	private static final String MERCHANT_RESPONSE = "merchantResponseValue";
	private static final String MERCHANT_ID = "merchantIDValue";
	private static final String AVS_ADDRESS_RESULT = "AVSAddrResultValue";
	private static final String AVS_POST_RESULT = "AVSPostResultValue";
	private static final String TOKEN = "tokenValue";
	private static final String ACQUIRER_ID = "acquirerIDValue";
	private static final String ACQUIRER_NAME = "acquirerNameValue";
	private static final String BANK_ID = "bankIDValue";
	private static final String BANK_NAME = "bankNameValue";
	private static final String MASKED_CARD_NUMBER = "maskedCardNumberValue";
	private static final String CARD_EXPIRY = "cardExpiryValue";
	private static final String TIMESTAMP = "timestampValue";
	private static final String CSC_RESULT = "CSCResultValue";
	private static final String SECURITY = "securityValue";
	private static final String VALUE = "135.67";
	private static final String CURRENCY = "USD";

	private ResultDataFromWPGHttpAuthorisePopulator testedInstance = new ResultDataFromWPGHttpAuthorisePopulator();
	private Map<String, String> parameters = new HashMap<>();
	private CreateSubscriptionResult createSubscriptionResult = new CreateSubscriptionResult();

	@Test
	public void shouldSetOperation()
	{

		parameters.put(OPERATION_PARAM, OPERATION);

		testedInstance.populate(parameters, createSubscriptionResult);

		assertEquals(OPERATION, createSubscriptionResult.getWpgResultInfoData().getOperation());
	}

	@Test
	public void shouldSetReturnCode()
	{

		parameters.put(RETURN_CODE_PARAM, RETURN_CODE);

		testedInstance.populate(parameters, createSubscriptionResult);

		assertEquals(RETURN_CODE, createSubscriptionResult.getWpgResultInfoData().getReturnCode());
	}

	@Test
	public void shouldSetReturnMessage()
	{

		parameters.put(RETURN_MESSAGE_PARAM, RETURN_MESSAGE);

		testedInstance.populate(parameters, createSubscriptionResult);

		assertEquals(RETURN_MESSAGE, createSubscriptionResult.getWpgResultInfoData().getReturnMessage());
	}

	@Test
	public void shouldSetVendorId()
	{

		parameters.put(VENDOR_ID_PARAM, VENDOR_ID);

		testedInstance.populate(parameters, createSubscriptionResult);

		assertEquals(VENDOR_ID, createSubscriptionResult.getWpgResultInfoData().getVendorID());
	}

	@Test
	public void shouldSetTransactionId()
	{

		parameters.put(TRANSACTION_ID_PARAM, TRANSACTION_ID);

		testedInstance.populate(parameters, createSubscriptionResult);

		assertEquals(TRANSACTION_ID, createSubscriptionResult.getWpgResultInfoData().getTransID());
	}

	@Test
	public void shouldSetMerchantResponse()
	{

		parameters.put(MERCHANT_RESPONSE_PARAM, MERCHANT_RESPONSE);

		testedInstance.populate(parameters, createSubscriptionResult);

		assertEquals(MERCHANT_RESPONSE, createSubscriptionResult.getWpgResultInfoData().getMerchantResponse());
	}

	@Test
	public void shouldSetMerchantId()
	{

		parameters.put(MERCHANT_ID_PARAM, MERCHANT_ID);

		testedInstance.populate(parameters, createSubscriptionResult);

		assertEquals(MERCHANT_ID, createSubscriptionResult.getWpgResultInfoData().getMerchantID());
	}

	@Test
	public void shouldSetAvsAddressResult()
	{

		parameters.put(AVS_ADDRESS_RESULT_PARAM, AVS_ADDRESS_RESULT);

		testedInstance.populate(parameters, createSubscriptionResult);

		assertEquals(AVS_ADDRESS_RESULT, createSubscriptionResult.getWpgResultInfoData().getAVSAddrResult());
	}

	@Test
	public void shouldSetAvsPostResult()
	{

		parameters.put(AVS_POST_RESULT_PARAM, AVS_POST_RESULT);

		testedInstance.populate(parameters, createSubscriptionResult);

		assertEquals(AVS_POST_RESULT, createSubscriptionResult.getWpgResultInfoData().getAVSPostResult());
	}

	@Test
	public void shouldSetToken()
	{

		parameters.put(TOKEN_PARAM, TOKEN);

		testedInstance.populate(parameters, createSubscriptionResult);

		assertEquals(TOKEN, createSubscriptionResult.getWpgResultInfoData().getToken());
	}

	@Test
	public void shouldSetAcquirerID()
	{

		parameters.put(ACQUIRER_ID_PARAM, ACQUIRER_ID);

		testedInstance.populate(parameters, createSubscriptionResult);

		assertEquals(ACQUIRER_ID, createSubscriptionResult.getWpgResultInfoData().getAcquirerID());
	}

	@Test
	public void shouldSetAcquirerName()
	{

		parameters.put(ACQUIRER_NAME_PARAM, ACQUIRER_NAME);

		testedInstance.populate(parameters, createSubscriptionResult);

		assertEquals(ACQUIRER_NAME, createSubscriptionResult.getWpgResultInfoData().getAcquirerName());
	}

	@Test
	public void shouldSetBankId()
	{

		parameters.put(BANK_ID_PARAM, BANK_ID);

		testedInstance.populate(parameters, createSubscriptionResult);

		assertEquals(BANK_ID, createSubscriptionResult.getWpgResultInfoData().getBankID());
	}

	@Test
	public void shouldSetBankName()
	{

		parameters.put(BANK_NAME_PARAM, BANK_NAME);

		testedInstance.populate(parameters, createSubscriptionResult);

		assertEquals(BANK_NAME, createSubscriptionResult.getWpgResultInfoData().getBankName());
	}

	@Test
	public void shouldSetCardNumber()
	{

		parameters.put(MASKED_CARD_NUMBER_PARAM, MASKED_CARD_NUMBER);

		testedInstance.populate(parameters, createSubscriptionResult);

		assertEquals(MASKED_CARD_NUMBER, createSubscriptionResult.getWpgResultInfoData().getMaskedCardNumber());
	}

	@Test
	public void shouldSetCardExpiry()
	{

		parameters.put(CARD_EXPIRY_PARAM, CARD_EXPIRY);

		testedInstance.populate(parameters, createSubscriptionResult);

		assertEquals(CARD_EXPIRY, createSubscriptionResult.getWpgResultInfoData().getCardExpiry());
	}

	@Test
	public void shouldSetTimestamp()
	{

		parameters.put(TIMESTAMP_PARAM, TIMESTAMP);

		testedInstance.populate(parameters, createSubscriptionResult);

		assertEquals(TIMESTAMP, createSubscriptionResult.getWpgResultInfoData().getTimestamp());
	}

	@Test
	public void shouldSetCSRREsult()
	{

		parameters.put(CSC_RESULT_PARAM, CSC_RESULT);

		testedInstance.populate(parameters, createSubscriptionResult);

		assertEquals(CSC_RESULT, createSubscriptionResult.getWpgResultInfoData().getCSCResult());
	}

	@Test
	public void shouldSetSecurity()
	{

		parameters.put(SECURITY_PARAM, SECURITY);

		testedInstance.populate(parameters, createSubscriptionResult);

		assertEquals(SECURITY, createSubscriptionResult.getWpgResultInfoData().getSecurity());
	}

	@Test
	public void shouldSetValue()
	{
		parameters.put(VALUE_PARAM, VALUE);

		testedInstance.populate(parameters, createSubscriptionResult);

		assertEquals(VALUE, createSubscriptionResult.getWpgResultInfoData().getValue());
	}

	@Test
	public void shouldSetCurrency()
	{
		parameters.put(CURRENCY_PARAM, CURRENCY);

		testedInstance.populate(parameters, createSubscriptionResult);

		assertEquals(CURRENCY, createSubscriptionResult.getWpgResultInfoData().getCurrency());
	}
}
