package com.wiley.core.wileyas.user.service.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.servicelayer.user.daos.UserDao;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyasUserServiceImplUnitTest {

    public static final String TEST_USER_ID = "USER_ID";

    @Mock
    private UserDao userDaoMock;

    @InjectMocks
    private WileyasUserServiceImpl testInstance = new WileyasUserServiceImpl();

    private CustomerModel customerMock = new CustomerModel();
    private EmployeeModel employeeMock = new EmployeeModel();

    @Before
    public void setUp() throws Exception {

    }

    @Test
    public void shouldReturnFalseIfUserExistsButOfAnotherType() {
        //Given
        when(userDaoMock.findUserByUID(TEST_USER_ID)).thenReturn(employeeMock);
        when(userDaoMock.findUserByUID(TEST_USER_ID.toLowerCase())).thenReturn(employeeMock);
        //When
        boolean result = testInstance.isUserExisting(TEST_USER_ID, CustomerModel.class);
        //Then
        assertFalse(result);
    }

    @Test
    public void shouldReturnTrueIfUserOfNeededTypeExists() {
        //Given
        when(userDaoMock.findUserByUID(TEST_USER_ID)).thenReturn(customerMock);
        when(userDaoMock.findUserByUID(TEST_USER_ID.toLowerCase())).thenReturn(customerMock);
        //When
        boolean result = testInstance.isUserExisting(TEST_USER_ID, CustomerModel.class);
        //Then
        assertTrue(result);
    }



}