package com.wiley.core.payment.populators;

import com.wiley.core.media.WileyMediaService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorcms.model.components.SimpleResponsiveBannerComponentModel;
import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionRequest;
import de.hybris.platform.acceleratorservices.payment.data.CustomerBillToData;
import de.hybris.platform.acceleratorservices.payment.data.CustomerShipToData;
import de.hybris.platform.acceleratorservices.payment.data.OrderInfoData;
import de.hybris.platform.acceleratorservices.payment.data.PaymentData;
import de.hybris.platform.acceleratorservices.urlresolver.SiteBaseUrlResolutionService;
import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;
import de.hybris.platform.cms2.model.contents.contentslot.ContentSlotModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSContentSlotService;
import de.hybris.platform.cms2.servicelayer.services.impl.DefaultCMSSiteService;
import de.hybris.platform.commerceservices.enums.SiteTheme;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.payment.strategies.SecurityHashGeneratorStrategy;
import com.wiley.core.payment.strategies.WPGRegionStrategy;
import com.wiley.core.payment.strategies.WPGVendorIdStrategy;
import com.wiley.core.payment.strategies.WileyTransactionIdGeneratorStrategy;
import com.wiley.core.wileyb2c.basesite.theme.Wileyb2cThemeNameResolutionStrategy;

import static com.wiley.core.payment.WileyHttpRequestParams.WPG_ADDRESS;
import static com.wiley.core.payment.WileyHttpRequestParams.WPG_ALLOW_AVS_FAIL;
import static com.wiley.core.payment.WileyHttpRequestParams.WPG_COUNTRY_CODE;
import static com.wiley.core.payment.WileyHttpRequestParams.WPG_CUSTOM_BACK_URL;
import static com.wiley.core.payment.WileyHttpRequestParams.WPG_CUSTOM_CUSTOMER_FIRST_NAME;
import static com.wiley.core.payment.WileyHttpRequestParams.WPG_CUSTOM_INITIATOR;
import static com.wiley.core.payment.WileyHttpRequestParams.WPG_CUSTOM_LOGO_IMAGE;
import static com.wiley.core.payment.WileyHttpRequestParams.WPG_CUSTOM_LOGO_URL;
import static com.wiley.core.payment.WileyHttpRequestParams.WPG_CUSTOM_OPERATION;
import static com.wiley.core.payment.WileyHttpRequestParams.WPG_CUSTOM_RESPONSIVE_LOGO_DATA;
import static com.wiley.core.payment.WileyHttpRequestParams.WPG_CUSTOM_SITE_URL;
import static com.wiley.core.payment.WileyHttpRequestParams.WPG_CUSTOM_THEME;
import static com.wiley.core.payment.WileyHttpRequestParams.WPG_DESCRIPTION;
import static com.wiley.core.payment.WileyHttpRequestParams.WPG_METHOD;
import static com.wiley.core.payment.WileyHttpRequestParams.WPG_OPERATION;
import static com.wiley.core.payment.WileyHttpRequestParams.WPG_POSTCODE;
import static com.wiley.core.payment.WileyHttpRequestParams.WPG_REGION;
import static com.wiley.core.payment.WileyHttpRequestParams.WPG_TIMESTAMP;
import static com.wiley.core.payment.WileyHttpRequestParams.WPG_TRANSACTION_ID;
import static com.wiley.core.payment.WileyHttpRequestParams.WPG_VENDOR_ID;
import static com.wiley.core.payment.populators.WPGHttpRequestPopulator.DEFAULT_IMAGE_HREF;
import static com.wiley.core.payment.populators.WPGHttpRequestPopulator.DEFAULT_IMAGE_PATH_TEMPLATE;
import static com.wiley.core.payment.populators.WPGHttpRequestPopulator.SITE_LOGO_SLOT;
import static junit.framework.Assert.assertEquals;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WPGHttpRequestPopulatorTest
{
	private static final String WPG_CUSTOM_CURRENT_YEAR = "WPG_CUSTOM_CURRENT_YEAR";
	private static final String WPG_CUSTOM_GRAPHICSTANDARDS_LINK = "WPG_CUSTOM_GRAPHICSTANDARDS_LINK";
	private static final String WPG_CUSTOM_WILEY_LINK = "WPG_CUSTOM_WILEY_LINK";
	private static final String ALLOW_AVS_FAIL = "Y";
	private static final String DESCRIPTION = "Test Auth Transaction";
	private static final String METHOD = "HTTP";
	private static final String OPERATION = "WPG_validate";
	private static final String REGION = "EMEA";
	private static final String ADDRESS_LINE1 = "14 Wallace Road";
	private static final String POSTCODE = "SO19 9GX";
	private static final String POSTCODE_LONG = "12345678901";
	private static final String POSTCODE_TRUNCATED = "12345678";

	private static final String COUNTRY_CODE = "826";
	private static final Integer VENDOR_ID = 204;
	private static final String REQUEST_URL = "http://wpgAuthRequestUrl";
	private static final String REDIRECT_URL = "http://redirectUrl";
	private static final String TIMESTAMP = "1448024849595";
	private static final String ORDER_NUMBER = "orderNumber";
	private static final String SECURITY = "13412575029d1d78920e580917088eea";
	private static final String VALUE = "12.99";
	private static final String CURRENCY = "USD";
	private static final String TEST_TRANSACTION_ID = "TestTransactionId";
	private static final String TEST_SITE_ID = "testSiteId";
	private static final String BACK_URL = "backUrl";
	private static final String INITIATOR = "initiator";
	private static final String CUSTOMER_FIRST_NAME = "firstName";
	private static final String GRAPHICSTANDARDS_LINK = "testLinkGraphicStandards";
	private static final String WILEY_LINK = "testWileyLink";

	private static final String SITE_URL = "https://siteUrl";
	private static final String INITIATOR_KEY = "wpg_validation_initiator";
	private static final String TEST_THEME_CODE = "testThemeCode";
	private static final String SITE_URL_CONFIG_GRAPHICSTANDARDS = "ags.graphicstandards.url.https";
	private static final String SITE_URL_CONFIG_WILEY = "wiley.com.url";
	private static final String LOGO_URL = "logoURL";
	private static final String MEDIA_LOGO_PATH = "mediaPath";
	private static final String SITE_LOGO_PATH = "siteLogoPath";
	private static final String EMPTY_PATH = "";
	private static final String EMPTY_ENCODING_ATTRIBUTES = "";
	private static final String IMAGE_URL = "url/test";
	private static final String IMAGE_URL_FOR_CONTAINER = "url2/test2";
	private static final Integer IMAGE_WIDTH = 123;
	private static final Integer IMAGE_WIDTH_FOR_CONTAINER = 567;

	@InjectMocks
	private final WPGHttpRequestPopulator testedInstance = new WPGHttpRequestPopulator()
	{
		@Override
		protected String createSecurityHashCode(final Map<String, String> parameters)
		{
			return SECURITY;
		}

		@Override
		public SecurityHashGeneratorStrategy getSecurityHashGeneratorStrategy()
		{
			return securityHashGeneratorStrategy;
		}

		@Override
		protected String createWpgTimestamp()
		{
			return TIMESTAMP;
		}
	};

	private final CreateSubscriptionRequest source = new CreateSubscriptionRequest();
	private final PaymentData target = new PaymentData();
	private final CustomerBillToData customerBillToData = new CustomerBillToData();
	private final CustomerShipToData customerShipToData = new CustomerShipToData();
	private final OrderInfoData orderInfoData = new OrderInfoData();
	@Mock
	private WileyTransactionIdGeneratorStrategy mockTransactionIdGeneratorStrategy;
	@Mock
	private SecurityHashGeneratorStrategy securityHashGeneratorStrategy;
	@Mock
	private WPGRegionStrategy wpgRegionStrategy;
	@Mock
	private ConfigurationService mockConfigurationService;
	@Mock
	private BaseSiteService mockBaseSiteService;
	@Mock
	private CMSSiteModel mockBaseSite;
	@Mock
	private DefaultCMSSiteService cmsSiteService;
	@Mock
	private WPGVendorIdStrategy httpVendorIdStrategy;
	@Mock
	private SessionService sessionService;
	@Mock
	private UserService userService;
	@Mock
	private Wileyb2cThemeNameResolutionStrategy mockwileyb2cThemeNameResolutionStrategy;
	@Mock
	private SiteBaseUrlResolutionService siteBaseUrlResolutionService;

	@Mock
	private Configuration mockConfiguration;
	@Mock
	private CustomerModel customer;
	@Mock
	private SiteTheme testTheme;
	@Mock
	private MediaModel logoImage;
	@Mock
	private CMSContentSlotService cmsContentSlotServiceMock;
	@Mock
	private ContentSlotModel contentSlotModelMock;
	@Mock
	private MediaContainerModel mediaContainerModelMock;
	@Mock
	private SimpleResponsiveBannerComponentModel simpleResponsiveBannerComponentMock;
	@Mock
	private SiteConfigService siteConfigService;
	@Mock
	private WileyMediaService wileyMediaService;


	@Before
	public void setUp()
	{
		when(mockTransactionIdGeneratorStrategy.generateTransactionId()).thenReturn(TEST_TRANSACTION_ID);
		when(mockConfigurationService.getConfiguration()).thenReturn(mockConfiguration);
		when(mockBaseSiteService.getCurrentBaseSite()).thenReturn(mockBaseSite);
		when(mockBaseSite.getUid()).thenReturn(TEST_SITE_ID);
		when(mockBaseSite.getTheme()).thenReturn(testTheme);
		when(testTheme.getCode()).thenReturn(TEST_THEME_CODE);
		when(siteBaseUrlResolutionService.getWebsiteUrlForSite(mockBaseSite, EMPTY_ENCODING_ATTRIBUTES, true, EMPTY_PATH))
				.thenReturn(SITE_URL);
		when(sessionService.getAttribute(INITIATOR_KEY)).thenReturn(INITIATOR);
		when((CustomerModel) userService.getCurrentUser()).thenReturn(customer);
		when(mockConfiguration.getString(SITE_URL_CONFIG_GRAPHICSTANDARDS, null)).thenReturn(GRAPHICSTANDARDS_LINK);
		when(mockConfiguration.getString(SITE_URL_CONFIG_WILEY, null)).thenReturn(WILEY_LINK);
		when(customer.getFirstName()).thenReturn(CUSTOMER_FIRST_NAME);
		when(cmsSiteService.getCurrentSite()).thenReturn(mockBaseSite);
		when(mockwileyb2cThemeNameResolutionStrategy.resolveThemeName(mockBaseSite)).thenReturn(TEST_THEME_CODE);
		when(siteConfigService.getProperty(WileyCoreConstants.ANALYTICS_SCRIPT_URL_CONFIG_KEY)).thenReturn(StringUtils.EMPTY);

		doReturn(contentSlotModelMock).when(cmsContentSlotServiceMock).getContentSlotForId(SITE_LOGO_SLOT);
		doReturn(new ArrayList<AbstractCMSComponentModel>()).when(contentSlotModelMock).getCmsComponents();
		doReturn(MEDIA_LOGO_PATH).when(siteBaseUrlResolutionService).getMediaUrlForSite(mockBaseSite, true, LOGO_URL);
		doReturn(MEDIA_LOGO_PATH).when(siteBaseUrlResolutionService).getMediaUrlForSite(mockBaseSite, true);

		testedInstance.setWpgAllowAvsFail(ALLOW_AVS_FAIL);
		testedInstance.setWpgDescription(DESCRIPTION);
		testedInstance.setWpgMethod(METHOD);
		testedInstance.setWpgOperation(OPERATION);
		testedInstance.setWpgRegionStrategy(wpgRegionStrategy);

		final Map<String, String> backUrlsBySite = new HashMap<>();
		backUrlsBySite.put(TEST_SITE_ID, BACK_URL);
		testedInstance.setWpgBackUrlBySite(backUrlsBySite);

		//TODO: remove ship to data and get parameters for bill to data only
		source.setCustomerShipToData(customerShipToData);
		source.setCustomerBillToData(customerBillToData);

		orderInfoData.setCurrency(CURRENCY);
		orderInfoData.setOrderTotal(VALUE);
		source.setOrderInfoData(orderInfoData);
	}

	@Test
	public void shouldSetPostUrl()
	{
		source.setRequestUrl(REQUEST_URL);
		testedInstance.populate(source, target);
		assertEquals(REQUEST_URL, target.getPostUrl());
	}

	@Test
	public void shouldSetTheme()
	{

		testedInstance.populate(source, target);

		assertEquals(TEST_THEME_CODE, target.getParameters().get(WPG_CUSTOM_THEME));
	}

	@Test
	public void shouldSetLogoUrlFromSiteWhenProvided()
	{
		when(mockBaseSite.getRedirectURL()).thenReturn(REDIRECT_URL);

		testedInstance.populate(source, target);

		assertEquals(REDIRECT_URL, target.getParameters().get(WPG_CUSTOM_LOGO_URL));
	}


	@Test
	public void shouldSetDefaultLogoUrlWhenNotProvided()
	{
		when(mockBaseSite.getRedirectURL()).thenReturn(null);

		testedInstance.populate(source, target);

		assertEquals(DEFAULT_IMAGE_HREF, target.getParameters().get(WPG_CUSTOM_LOGO_URL));
	}


	@Test
	public void shouldSetLogoImageFromSiteMediaWhenProvided()
	{
		when(mockBaseSite.getResponsiveLogoImages()).thenReturn(mediaContainerModelMock);
		when(logoImage.getDownloadURL()).thenReturn(LOGO_URL);
		when(wileyMediaService.convertMediaContainerToMediaData(mockBaseSite, mediaContainerModelMock))
				.thenReturn("{&quot;123&quot;:&quot;mediaPathurl/test&quot;}");

		testedInstance.populate(source, target);

		assertEquals("{&quot;" + IMAGE_WIDTH + "&quot;:&quot;" + MEDIA_LOGO_PATH + IMAGE_URL + "&quot;}",
				target.getParameters().get(WPG_CUSTOM_RESPONSIVE_LOGO_DATA));
	}

	@Test
	public void shouldSetLogoFromMediaContainerWhenPresent()
	{
		// Given
		when(mockBaseSite.getResponsiveLogoImages()).thenReturn(null);
		doReturn(Arrays.asList(simpleResponsiveBannerComponentMock)).when(contentSlotModelMock).getCmsComponents();
		doReturn(mediaContainerModelMock).when(simpleResponsiveBannerComponentMock).getMedia();
		when(wileyMediaService.convertMediaContainerToMediaData(mockBaseSite, mediaContainerModelMock))
				.thenReturn("{&quot;567&quot;:&quot;mediaPathurl2/test2&quot;}");
		// Testing
		testedInstance.populate(source, target);

		// Verify
		assertEquals("{&quot;" + IMAGE_WIDTH_FOR_CONTAINER + "&quot;:&quot;" + MEDIA_LOGO_PATH + IMAGE_URL_FOR_CONTAINER
				+ "&quot;}", target.getParameters().get(WPG_CUSTOM_RESPONSIVE_LOGO_DATA));
	}

	@Test
	public void shouldSetLogoImageFromWebsiteUrlWhenNotProvided()
	{
		when(mockBaseSite.getResponsiveLogoImages()).thenReturn(null);
		when(siteBaseUrlResolutionService.getWebsiteUrlForSite(mockBaseSite, EMPTY_ENCODING_ATTRIBUTES, true, String.format(
				DEFAULT_IMAGE_PATH_TEMPLATE, TEST_THEME_CODE))).thenReturn(SITE_LOGO_PATH);

		testedInstance.populate(source, target);

		assertEquals(SITE_LOGO_PATH, target.getParameters().get(WPG_CUSTOM_LOGO_IMAGE));
	}


	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowExceptionIfSourceIsNull()
	{
		testedInstance.populate(null, target);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowExceptionIfTargetIsNull()
	{
		testedInstance.populate(source, null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowExceptionIfCustomerBillToDataIsNull()
	{
		source.setCustomerBillToData(null);
		testedInstance.populate(source, target);
	}

	@Test
	public void shouldAddWPGOperationParameter()
	{
		testedInstance.populate(source, target);
		assertEquals(OPERATION, target.getParameters().get(WPG_OPERATION));
	}

	@Test
	public void shouldAddWPGTimestampParameter()
	{
		testedInstance.populate(source, target);
		assertEquals(TIMESTAMP, target.getParameters().get(WPG_TIMESTAMP));
	}

	@Test
	public void shouldAddWPGVendorIdParameter()
	{
		when(httpVendorIdStrategy.getVendorId(TEST_SITE_ID)).thenReturn(VENDOR_ID);
		testedInstance.populate(source, target);
		assertEquals(String.valueOf(VENDOR_ID), target.getParameters().get(WPG_VENDOR_ID));
	}

	@Test
	public void shouldAddWPGSiteUrlParameter()
	{
		when(siteBaseUrlResolutionService.getWebsiteUrlForSite(mockBaseSite, "", true, "")).thenReturn(SITE_URL);

		testedInstance.populate(source, target);

		assertEquals(SITE_URL, target.getParameters().get(WPG_CUSTOM_SITE_URL));
	}

	@Test
	public void shouldAddWPGTransactionIdParameter()
	{
		orderInfoData.setOrderNumber(ORDER_NUMBER);
		testedInstance.populate(source, target);
		assertEquals(TEST_TRANSACTION_ID, target.getParameters().get(WPG_TRANSACTION_ID));
	}

	@Test
	public void shouldAddWPGMethodParameter()
	{
		testedInstance.populate(source, target);
		assertEquals(METHOD, target.getParameters().get(WPG_METHOD));
	}

	@Test
	public void shouldAddWPGDescriptionParameter()
	{
		testedInstance.populate(source, target);
		assertEquals(DESCRIPTION, target.getParameters().get(WPG_DESCRIPTION));
	}


	@Test
	public void shouldAddWPGRegionParameter()
	{
		orderInfoData.setCurrency(CURRENCY);
		when(wpgRegionStrategy.getRegionByCurrency(CURRENCY)).thenReturn(REGION);

		testedInstance.populate(source, target);

		assertEquals(REGION, target.getParameters().get(WPG_REGION));
	}

	@Test
	public void shouldAddWPGAddressParameter()
	{
		customerBillToData.setBillToStreet1(ADDRESS_LINE1);
		testedInstance.populate(source, target);
		assertEquals(ADDRESS_LINE1, target.getParameters().get(WPG_ADDRESS));
	}

	@Test
	public void shouldAddWPGPostcodeParameter()
	{
		customerBillToData.setBillToPostalCode(POSTCODE);
		testedInstance.populate(source, target);
		assertEquals(POSTCODE, target.getParameters().get(WPG_POSTCODE));
	}

	@Test
	public void shouldAddAndTruncateWPGPostcodeParameterWhenItIsTooLong()
	{
		customerBillToData.setBillToPostalCode(POSTCODE_LONG);
		testedInstance.populate(source, target);
		assertEquals(POSTCODE_TRUNCATED, target.getParameters().get(WPG_POSTCODE));
	}

	@Test
	public void shouldTolerateNullWPGPostcodeParameter()
	{
		customerBillToData.setBillToPostalCode(null);
		testedInstance.populate(source, target);
		assertEquals(null, target.getParameters().get(WPG_POSTCODE));
	}

	@Test
	public void shouldAddWPGCountryCodeParameter()
	{
		customerBillToData.setCountryNumericIsocode(COUNTRY_CODE);
		testedInstance.populate(source, target);
		assertEquals(COUNTRY_CODE, target.getParameters().get(WPG_COUNTRY_CODE));
	}

	@Test
	public void shouldAddWPGAllowAvsFailParameter()
	{
		testedInstance.populate(source, target);
		assertEquals(ALLOW_AVS_FAIL, target.getParameters().get(WPG_ALLOW_AVS_FAIL));
	}

	@Test
	public void shouldAddWPGCustomOperationParameter()
	{
		testedInstance.populate(source, target);
		assertEquals(OPERATION, target.getParameters().get(WPG_CUSTOM_OPERATION));
	}

	@Test
	public void shouldAddWPGCCustomBackUrl()
	{
		testedInstance.populate(source, target);
		assertEquals(BACK_URL, target.getParameters().get(WPG_CUSTOM_BACK_URL));
	}

	@Test
	public void shouldAddCustomerFirstNameParameter()
	{
		testedInstance.populate(source, target);
		assertEquals(CUSTOMER_FIRST_NAME, target.getParameters().get(WPG_CUSTOM_CUSTOMER_FIRST_NAME));
	}

	@Test
	public void shouldAddInitiatorParameter()
	{
		testedInstance.populate(source, target);
		assertEquals(INITIATOR, target.getParameters().get(WPG_CUSTOM_INITIATOR));
	}

	@Test
	public void shouldAddCurrentYear()
	{
		testedInstance.populate(source, target);
		LocalDateTime timePoint = LocalDateTime.now();
		assertEquals(String.valueOf(timePoint.getYear()), target.getParameters().get(WPG_CUSTOM_CURRENT_YEAR));
	}

	@Test
	public void shouldAddGraphicstandardsLink()
	{
		testedInstance.populate(source, target);
		assertEquals(GRAPHICSTANDARDS_LINK, target.getParameters().get(WPG_CUSTOM_GRAPHICSTANDARDS_LINK));
	}

	@Test
	public void shouldAddWileyLink()
	{
		testedInstance.populate(source, target);
		assertEquals(WILEY_LINK, target.getParameters().get(WPG_CUSTOM_WILEY_LINK));
	}

}
