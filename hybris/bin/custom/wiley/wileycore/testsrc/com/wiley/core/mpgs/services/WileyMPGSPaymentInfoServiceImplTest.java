package com.wiley.core.mpgs.services;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.payment.data.PaymentInfoData;
import de.hybris.platform.acceleratorservices.payment.data.SubscriptionInfoData;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.impl.DefaultCheckoutFacade;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.mpgs.response.WileyTokenizationResponse;
import com.wiley.core.mpgs.services.impl.WileyMPGSPaymentInfoServiceImpl;
import com.wiley.core.payment.strategies.impl.WileyCreditCardPaymentInfoCreateStrategy;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyMPGSPaymentInfoServiceImplTest
{
	private static final String TOKEN = "4508753994831019";
	private static final String CARD_NUMBER = "450875xxxxxx1019";
	private static final String CARD_SCHEME = "VISA";
	private static final String CARD_EXPIRY_MONTH = "05";
	private static final String CARD_EXPIRY_YEAR = "21";
	private static final String MERCHANT_ID = "TESTMERCHANTID";

	private PK cardPaymentInfoPK = PK.fromLong(123456789);

	@Mock
	private CartModel mockCartModel;

	@Mock
	private CustomerModel mockCustomerModel;

	@Mock
	private AddressModel mockBillingAddress;

	@Mock
	private CreditCardPaymentInfoModel mockCreditCardPaymentInfoModel;

	@Mock
	private CartService mockCartService;

	@Mock
	private WileyTokenizationResponse mockTokenizationResponse;

	@Mock
	private WileyCreditCardPaymentInfoCreateStrategy mockWileyCreditCardPaymentInfoCreateStrategy;

	@Mock
	private ModelService mockModelService;

	@Mock
	private UserFacade mockUserFacade;

	@Mock
	private DefaultCheckoutFacade mockCheckoutFacade;

	@Mock
	private OrderModel orderModelMock;

	@Mock
	private CustomerModel orderCustomerModelMock;

	@InjectMocks
	private WileyMPGSPaymentInfoServiceImpl underTestMpgsPaymentInfoServiceImpl = new WileyMPGSPaymentInfoServiceImpl();

	@Before
	public void setup()
	{
		when(orderModelMock.getUser()).thenReturn(orderCustomerModelMock);

		when(mockCartService.getSessionCart()).thenReturn(mockCartModel);
		when(mockCartModel.getUser()).thenReturn(mockCustomerModel);
		when(mockCustomerModel.getDefaultPaymentAddress()).thenReturn(mockBillingAddress);
		when(mockCreditCardPaymentInfoModel.getPk()).thenReturn(cardPaymentInfoPK);

		when(mockWileyCreditCardPaymentInfoCreateStrategy
				.createCreditCardPaymentInfo(any(SubscriptionInfoData.class), any(PaymentInfoData.class), any(AddressModel.class),
						any(CustomerModel.class), eq(true))).thenReturn(mockCreditCardPaymentInfoModel);


		// mockTokenizationResponse
		when(mockTokenizationResponse.getToken()).thenReturn(TOKEN);
		when(mockTokenizationResponse.getCardNumber()).thenReturn(CARD_NUMBER);
		when(mockTokenizationResponse.getCardScheme()).thenReturn(CARD_SCHEME);
		when(mockTokenizationResponse.getCardExpiryMonth()).thenReturn(CARD_EXPIRY_MONTH);
		when(mockTokenizationResponse.getCardExpiryYear()).thenReturn(CARD_EXPIRY_YEAR);
	}

	@Test
	public void testCreditCardPaymentInfoIsSetOnCustomerAndCart()
	{
		underTestMpgsPaymentInfoServiceImpl.setCreditCardPaymentInfo(MERCHANT_ID, mockTokenizationResponse, true);

		verify(mockModelService).save(mockCreditCardPaymentInfoModel);
		verify(mockCheckoutFacade).setPaymentDetails(mockCreditCardPaymentInfoModel.getPk().toString());
		verify(mockUserFacade).setDefaultPaymentInfo(any(CCPaymentInfoData.class));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testNullValue()
	{
		underTestMpgsPaymentInfoServiceImpl.setCreditCardPaymentInfo(null, null, true);
	}

	@Test
	public void testCreditCardPaymentInfoIsSetToOrder()
	{
		// given

		// when
		underTestMpgsPaymentInfoServiceImpl.setCreditCardPaymentInfo(MERCHANT_ID, mockTokenizationResponse, true, orderModelMock);

		// then
		verify(orderModelMock).setPaymentInfo(mockCreditCardPaymentInfoModel);
		verify(mockModelService).saveAll(mockCreditCardPaymentInfoModel, orderModelMock);
		verify(mockModelService).refresh(orderModelMock);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetCreditCardPaymentInfoForOrderWithNullTnsMerchant()
	{
		underTestMpgsPaymentInfoServiceImpl.setCreditCardPaymentInfo(null, mockTokenizationResponse, true, orderModelMock);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetCreditCardPaymentInfoForOrderWithNullResponse()
	{
		underTestMpgsPaymentInfoServiceImpl.setCreditCardPaymentInfo(MERCHANT_ID, null, true, orderModelMock);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetCreditCardPaymentInfoForOrderWithNullOrder()
	{
		underTestMpgsPaymentInfoServiceImpl.setCreditCardPaymentInfo(MERCHANT_ID, mockTokenizationResponse, true, null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetCreditCardPaymentInfoForOrderWithNullOrderUser()
	{
		when(orderModelMock.getUser()).thenReturn(null);
		underTestMpgsPaymentInfoServiceImpl.setCreditCardPaymentInfo(MERCHANT_ID, mockTokenizationResponse, true, orderModelMock);
	}

}
