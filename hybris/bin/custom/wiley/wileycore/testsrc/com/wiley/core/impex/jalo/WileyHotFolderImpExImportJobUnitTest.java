package com.wiley.core.impex.jalo;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.impex.jalo.Importer;
import de.hybris.platform.impex.jalo.cronjob.ImpExImportCronJob;
import de.hybris.platform.impex.jalo.imp.ImpExImportReader;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Test;

import com.wiley.core.integration.handlers.WileyHotFolderCronJobErrorHandler;

import static org.hamcrest.CoreMatchers.instanceOf;


/**
 * Created by Uladzimir_Barouski on 11/1/2016.
 */
@UnitTest
public class WileyHotFolderImpExImportJobUnitTest
{

	@Test
	public void testAdjustImporter() throws Exception
	{
		ImpExImportReader reader = mock(ImpExImportReader.class);
		Importer importer = new Importer(reader);

		ImpExImportCronJob cronJob = mock(ImpExImportCronJob.class);
		when(cronJob.getValueCountAsPrimitive()).thenReturn(1);

		WileyHotFolderImpExImportJob job = new WileyHotFolderImpExImportJob();
		job.adjustImporter(importer, cronJob);

		assertThat(importer.getErrorHandler(), instanceOf(WileyHotFolderCronJobErrorHandler.class));
	}
}