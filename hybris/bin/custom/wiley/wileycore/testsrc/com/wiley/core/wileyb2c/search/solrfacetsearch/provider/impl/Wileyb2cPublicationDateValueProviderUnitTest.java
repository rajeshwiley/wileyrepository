package com.wiley.core.wileyb2c.search.solrfacetsearch.provider.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.wileyb2c.classification.Wileyb2cClassificationAttributes;
import com.wiley.core.wileyb2c.classification.Wileyb2cClassificationService;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cPublicationDateValueProviderUnitTest
{
	private static final Date PUBLICATION_DATE = new Date();
	@InjectMocks
	private Wileyb2cPublicationDateValueProvider wileyb2cPublicationDateValueProvider;
	@Mock
	private ProductModel productModel;
	@Mock
	private IndexConfig indexConfig;
	@Mock
	private IndexedProperty indexedProperty;
	@Mock
	private Wileyb2cClassificationService wileyb2cClassificationService;

	@Test
	public void collectValues() throws FieldValueProviderException
	{
		when(wileyb2cClassificationService.resolveAttributeValue(productModel, Wileyb2cClassificationAttributes.PUBLICATION_DATE))
				.thenReturn(
						PUBLICATION_DATE);

		final List<Date> values = wileyb2cPublicationDateValueProvider.collectValues(indexConfig, indexedProperty,
				productModel);

		assertEquals(values.size(), 1);
		assertEquals(values.get(0), PUBLICATION_DATE);
	}
}
