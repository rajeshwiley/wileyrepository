package com.wiley.core.wiley.ruleengine.drools.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.ruleengine.RuleEvaluationContext;
import de.hybris.platform.ruleengine.enums.DroolsSessionType;
import de.hybris.platform.ruleengine.model.DroolsKIESessionModel;
import de.hybris.platform.ruleengine.model.DroolsRuleEngineContextModel;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.drools.core.SessionConfiguration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.StatelessKieSession;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.ruleengine.model.WileyDroolsRuleEngineContextModel;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyKieSessionHelperUnitTest
{
	private static final String SESSION_NAME = "SESSION NAME";

	@Mock
	private StatelessKieSession statelessKieSessionMock;
	@Mock
	private RuleEvaluationContext ruleEvaluationContextMock;
	@Mock
	private KieContainer kieContainerMock;
	@Mock
	private DroolsRuleEngineContextModel droolsRuleEngineContextModelMock;
	@Mock
	private WileyDroolsRuleEngineContextModel wileyDroolsRuleEngineContextModelMock;
	@Mock
	private SessionConfiguration sessionConfigurationMock;
	@Mock
	private DroolsKIESessionModel droolsKIESessionModelMock;
	@InjectMocks
	private WileyKieSessionHelper testInstance;

	@Before
	public void setUp()
	{
		when(droolsKIESessionModelMock.getName()).thenReturn(SESSION_NAME);
		when(droolsKIESessionModelMock.getSessionType()).thenReturn(DroolsSessionType.STATELESS);
		when(kieContainerMock.getKieSessionConfiguration(SESSION_NAME)).thenReturn(sessionConfigurationMock);
	}

	@Test
	public void testGetStatelessKieSessionReturnsPseudoClockSessionForWileyDroolsContext()
	{
		// given
		when(ruleEvaluationContextMock.getRuleEngineContext()).thenReturn(wileyDroolsRuleEngineContextModelMock);
		when(wileyDroolsRuleEngineContextModelMock.getKieSession()).thenReturn(droolsKIESessionModelMock);
		when(kieContainerMock.newStatelessKieSession(SESSION_NAME, sessionConfigurationMock)).thenReturn(statelessKieSessionMock);

		// when
		testInstance.initializeSession(StatelessKieSession.class, ruleEvaluationContextMock, kieContainerMock);

		// then
		verify(sessionConfigurationMock).setClockType(org.drools.core.ClockType.PSEUDO_CLOCK);
	}

	@Test
	public void testGetStatelessKieSessionReturnsGeneralClockSessionForDroolsContext()
	{
		// given
		when(ruleEvaluationContextMock.getRuleEngineContext()).thenReturn(droolsRuleEngineContextModelMock);
		when(droolsRuleEngineContextModelMock.getKieSession()).thenReturn(droolsKIESessionModelMock);
		when(kieContainerMock.newStatelessKieSession(SESSION_NAME)).thenReturn(statelessKieSessionMock);

		// when
		testInstance.initializeSession(StatelessKieSession.class, ruleEvaluationContextMock, kieContainerMock);

		// then
		verify(sessionConfigurationMock, never()).setClockType(any());
	}
}