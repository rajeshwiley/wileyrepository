package com.wiley.core.refund.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.integration.edi.service.impl.WileyPurchaseOrderService;
import com.wiley.core.payment.transaction.PaymentTransactionService;
import com.wiley.fulfilmentprocess.model.WileyExportProcessModel;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SingleRefundTransactionRefundCheckStrategyTest
{
	private static final BigDecimal TOTAL_REFUND_AMOUNT = BigDecimal.valueOf(65.35);
	private static final BigDecimal FIRST_PARTIAL_REFUND_AMOUNT = BigDecimal.valueOf(25.15);
	private static final BigDecimal SECOND_PARTIAL_REFUND_AMOUNT = BigDecimal.valueOf(40.20);

	@Mock
	private PaymentTransactionService transactionService;
	@InjectMocks
	private SingleRefundTransactionRefundCheckStrategy testInstance = new SingleRefundTransactionRefundCheckStrategy();
	@Mock
	private OrderModel order;
	@Mock
	private PaymentTransactionModel transaction;
	@Mock
	private PaymentTransactionEntryModel captureTransactionEntry;
	@Mock
	private PaymentTransactionEntryModel refundTransactionEntry;
	@Mock
	private PaymentTransactionEntryModel firstPartialRefundTransactionEntry;
	@Mock
	private PaymentTransactionEntryModel secondPartialRefundTransactionEntry;
	@Mock
	private WileyExportProcessModel exportProcess;
	@Mock
	private WileyPurchaseOrderService wileyPurchaseOrderService;

	@Before
	public void setUp() throws Exception
	{
		when(order.getPaymentTransactions()).thenReturn(Collections.singletonList(transaction));
		when(captureTransactionEntry.getAmount()).thenReturn(TOTAL_REFUND_AMOUNT);
		givenTransactionEntryForOrder(order, captureTransactionEntry, PaymentTransactionType.CAPTURE);

	}

	/**
	 * test isFullyRefundedOrder
	 */
	@Test
	public void testIsFullyRefundedOrderShouldReturnTrueIfOrderCaptureTotalEqualsRefundedTotal()
	{
		//Given
		givenTransactionEntryForOrder(order, refundTransactionEntry, PaymentTransactionType.REFUND_FOLLOW_ON);
		when(refundTransactionEntry.getAmount()).thenReturn(TOTAL_REFUND_AMOUNT); //Fully refunded
		//When
		final boolean result = testInstance.isFullyRefundedOrder(order);
		//Then
		assertTrue(result);
	}

	/**
	 * test isFullyRefundedOrder
	 */
	@Test
	public void shouldReturnFalseIfOrderCaptureTotalIsBiggerThenRefundedTotal()
	{
		//Given
		final BigDecimal refundTotal = TOTAL_REFUND_AMOUNT.subtract(BigDecimal.ONE);
		givenTransactionEntryForOrder(order, refundTransactionEntry, PaymentTransactionType.REFUND_FOLLOW_ON);
		when(refundTransactionEntry.getAmount()).thenReturn(refundTotal);
		//When
		final boolean result = testInstance.isFullyRefundedOrder(order);
		//Then
		assertFalse(result);
	}

	@Test
	public void shouldReturnTrueIfOrderWasFullRefundedByPartialRefunds()
	{
		when(firstPartialRefundTransactionEntry.getAmount()).thenReturn(FIRST_PARTIAL_REFUND_AMOUNT);
		when(secondPartialRefundTransactionEntry.getAmount()).thenReturn(SECOND_PARTIAL_REFUND_AMOUNT);
		when(transactionService.getAcceptedTransactionEntries(order, PaymentTransactionType.REFUND_FOLLOW_ON))
				.thenReturn(Arrays.asList(firstPartialRefundTransactionEntry, secondPartialRefundTransactionEntry));
		final boolean result = testInstance.isFullyRefundedOrder(order);

		assertTrue(result);
	}

	/**
	 * test isFullyRefundedOrder
	 */
	@Test(expected = IllegalStateException.class)
	public void shouldThrowIllegalStateExceptionIfOrderCaptureTotalIsLessThenRefundedTotal()
	{
		//Given
		final BigDecimal refundTotal = TOTAL_REFUND_AMOUNT.add(BigDecimal.ONE);
		givenTransactionEntryForOrder(order, refundTransactionEntry, PaymentTransactionType.REFUND_FOLLOW_ON);
		when(refundTransactionEntry.getAmount()).thenReturn(refundTotal);
		//When
		testInstance.isFullyRefundedOrder(order);
	}

	/**
	 * test isFullyRefundedOrder
	 */
	@Test(expected = IllegalStateException.class)
	public void shouldThrowIllegalStateExceptionIfOrderDoesNotHaveCaptureTransaction()
	{
		//Given
		givenTransactionEntryForOrder(order, null, PaymentTransactionType.CAPTURE);
		//When
		testInstance.isFullyRefundedOrder(order);
	}

	/**
	 * test isFullyRefundedOrder
	 */
	@Test(expected = NullPointerException.class)
	public void shouldThrowNullPointerExceptionIfOrderIsNull()
	{
		//When
		testInstance.isFullyRefundedOrder(null);
	}

	@Test
	public void shouldReturnTrueIfOrderWasRefundedByOneTransaction()
	{
		when(exportProcess.getOrder()).thenReturn(order);
		when(wileyPurchaseOrderService.isOrderRefunded(exportProcess)).thenReturn(true);
		when(exportProcess.getTransactionEntry()).thenReturn(refundTransactionEntry);
		when(refundTransactionEntry.getAmount()).thenReturn(TOTAL_REFUND_AMOUNT);

		final boolean result = testInstance.isOrderRefundedByOneTransaction(exportProcess);

		assertTrue(result);
	}

	@Test
	public void shouldReturnFalseIfOrderWasNotFullRefundedByOneTransaction()
	{
		when(exportProcess.getOrder()).thenReturn(order);
		when(wileyPurchaseOrderService.isOrderRefunded(exportProcess)).thenReturn(true);
		when(exportProcess.getTransactionEntry()).thenReturn(refundTransactionEntry);
		final BigDecimal refundTotal = TOTAL_REFUND_AMOUNT.add(BigDecimal.ONE);
		when(refundTransactionEntry.getAmount()).thenReturn(refundTotal);

		final boolean result = testInstance.isOrderRefundedByOneTransaction(exportProcess);

		assertFalse(result);
	}

	private void givenTransactionEntryForOrder(
			final OrderModel order,
			final PaymentTransactionEntryModel transactionEntry,
			final PaymentTransactionType transactionType
	)
	{
		when(transactionService.getAcceptedTransactionEntry(order, transactionType))
				.thenReturn(transactionEntry);
		when(transactionService.getAcceptedTransactionEntries(order, transactionType))
				.thenReturn(Arrays.asList(transactionEntry));
	}

}