package com.wiley.core.wileyb2c.search.solrfacetsearch.listeners;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.solrfacetsearch.search.FacetSearchException;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.solrfacetsearch.search.context.FacetSearchContext;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.wiley.core.wileyb2c.search.solrfacetsearch.listeners.Wileyb2cPurchaseOptionSortListener.POSTFIX;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cPurchaseOptionSortListenerUnitTest
{
	private static final String SEQUENCE_PROPERTY = "sequence";
	@InjectMocks
	private Wileyb2cPurchaseOptionSortListener wileyb2cPurchaseOptionSortListener;
	@Mock
	private FacetSearchContext target;
	@Mock
	private SearchQuery searchQuery;

	@Test
	public void populate() throws FacetSearchException
	{
		wileyb2cPurchaseOptionSortListener.setPurchaseOptionSequenceProperty(SEQUENCE_PROPERTY);
		when(target.getSearchQuery()).thenReturn(searchQuery);

		wileyb2cPurchaseOptionSortListener.beforeSearch(target);

		verify(searchQuery).addRawParam("group.sort", SEQUENCE_PROPERTY + POSTFIX);
	}
}
