package com.wiley.core.wileyb2c.sitemap.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.sitemap.data.SiteMapUrlData;
import de.hybris.platform.category.model.CategoryModel;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Created by Maksim_Kozich on 29.08.17.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cCategoryModelToSiteMapUrlDataPopulatorUnitTest
{
	private static final String CATEGORY_CODE = "category code";
	private static final String LOCATION = "some location";

	@Mock
	private CategoryModel categoryModelMock;

	@Mock
	private SiteMapUrlData siteMapUrlDataMock;

	@Mock
	private Logger loggerMock;

	@Spy
	@InjectMocks
	private Wileyb2cCategoryModelToSiteMapUrlDataPopulator testInstance;

	@Before
	public void setUp()
	{
		when(testInstance.getLogger()).thenReturn(loggerMock);
		doNothing().when(testInstance).superPopulate(categoryModelMock, siteMapUrlDataMock);
	}

	@Test
	public void testNoErrorIsLoggedIfLocIsNotNull()
	{
		// Given
		when(siteMapUrlDataMock.getLoc()).thenReturn(LOCATION);

		// When
		testInstance.populate(categoryModelMock, siteMapUrlDataMock);

		// Then
		verify(loggerMock, never()).error(any());
	}

	@Test
	public void testErrorIsLoggedIfLocIsNull()
	{
		// Given
		when(siteMapUrlDataMock.getLoc()).thenReturn(null);
		when(categoryModelMock.getCode()).thenReturn(CATEGORY_CODE);

		// When
		testInstance.populate(categoryModelMock, siteMapUrlDataMock);

		// Then
		verify(loggerMock).error(Mockito.contains(CATEGORY_CODE));
	}
}
