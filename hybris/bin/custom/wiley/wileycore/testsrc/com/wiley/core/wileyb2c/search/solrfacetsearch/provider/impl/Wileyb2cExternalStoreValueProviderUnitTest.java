package com.wiley.core.wileyb2c.search.solrfacetsearch.provider.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.enums.WileyWebLinkTypeEnum;
import com.wiley.core.model.WileyWebLinkModel;
import com.wiley.core.wileyb2c.weblinks.WileyWebLinkService;


/**
 * Created by Raman_Hancharou on 6/19/2017.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cExternalStoreValueProviderUnitTest
{
	private static final String TEST_URL = "testUrl";
	private static final String EXTERNAL_STORE_URL_PROPERTY = "externalStoreUrl";
	private static final String EXTERNAL_STORE_TYPE_PROPERTY = "externalStoreType";

	@InjectMocks
	private Wileyb2cExternalStoreValueProvider wileyb2cExternalStoreValueProvider;

	@Mock
	private ProductModel productModel;
	@Mock
	private WileyWebLinkModel wileyWebLinkModelMock;
	@Mock
	private IndexedProperty indexedProperty;
	@Mock
	private WileyWebLinkService wileyWebLinkService;

	@Test
	public void collectValuesWhenProductHasNotExternalStores() throws FieldValueProviderException
	{
		when(productModel.getExternalStores()).thenReturn(null);

		final List<String> values = wileyb2cExternalStoreValueProvider.collectValues(null, indexedProperty, productModel);

		assertEquals(values.size(), 0);
	}

	@Test
	public void collectValuesWhenProductHasMoreThanOneExternalStore() throws FieldValueProviderException
	{
		when(productModel.getExternalStores()).thenReturn(Arrays.asList(wileyWebLinkModelMock, wileyWebLinkModelMock));

		final List<String> values = wileyb2cExternalStoreValueProvider.collectValues(null, indexedProperty, productModel);

		assertEquals(values.size(), 0);
	}

	@Test
	public void collectValuesWhenNonProductShouldReturnEmptyList() throws FieldValueProviderException
	{
		final List<String> values = wileyb2cExternalStoreValueProvider.collectValues(null, null, 1);

		assertEquals(values.size(), 0);
	}

	@Test
	public void collectValuesWhenWebLinkTypeIsWolAndIndexedPropertyIsUrl() throws FieldValueProviderException
	{
		when(productModel.getExternalStores()).thenReturn(Collections.singletonList(wileyWebLinkModelMock));
		when(wileyWebLinkModelMock.getType()).thenReturn(WileyWebLinkTypeEnum.WOL_STORE);
		when(wileyWebLinkService.getWebLinkUrl(wileyWebLinkModelMock)).thenReturn(TEST_URL);
		wileyb2cExternalStoreValueProvider.setExternalStoreUrlProperty(EXTERNAL_STORE_URL_PROPERTY);
		when(indexedProperty.getName()).thenReturn(EXTERNAL_STORE_URL_PROPERTY);

		final List<String> values = wileyb2cExternalStoreValueProvider.collectValues(null, indexedProperty, productModel);

		assertEquals(1, values.size());
		assertEquals(TEST_URL, values.get(0));
	}

	@Test
	public void collectValuesWhenWebLinkTypeIsWolAndIndexedPropertyIsType() throws FieldValueProviderException
	{
		when(productModel.getExternalStores()).thenReturn(Collections.singletonList(wileyWebLinkModelMock));
		when(wileyWebLinkModelMock.getType()).thenReturn(WileyWebLinkTypeEnum.WOL_STORE);
		wileyb2cExternalStoreValueProvider.setExternalStoreUrlProperty(EXTERNAL_STORE_URL_PROPERTY);
		wileyb2cExternalStoreValueProvider.setExternalStoreTypeProperty(EXTERNAL_STORE_TYPE_PROPERTY);
		when(indexedProperty.getName()).thenReturn(EXTERNAL_STORE_TYPE_PROPERTY);

		final List<String> values = wileyb2cExternalStoreValueProvider.collectValues(null, indexedProperty, productModel);

		assertEquals(1, values.size());
		assertEquals(WileyWebLinkTypeEnum.WOL_STORE.getCode(), values.get(0));
	}

	@Test
	public void collectValuesWhenWebLinkTypeIsRequestQuote() throws FieldValueProviderException
	{
		when(productModel.getExternalStores()).thenReturn(Collections.singletonList(wileyWebLinkModelMock));
		when(wileyWebLinkModelMock.getType()).thenReturn(WileyWebLinkTypeEnum.REQUEST_QUOTE);
		wileyb2cExternalStoreValueProvider.setExternalStoreUrlProperty(EXTERNAL_STORE_URL_PROPERTY);
		wileyb2cExternalStoreValueProvider.setExternalStoreTypeProperty(EXTERNAL_STORE_TYPE_PROPERTY);
		when(indexedProperty.getName()).thenReturn(EXTERNAL_STORE_TYPE_PROPERTY);

		final List<String> values = wileyb2cExternalStoreValueProvider.collectValues(null, indexedProperty, productModel);

		assertEquals(1, values.size());
		assertEquals(WileyWebLinkTypeEnum.REQUEST_QUOTE.getCode(), values.get(0));
	}

	@Test
	public void collectValuesWhenWebLinkTypeIsNotWolOrWpOrRequestQuote() throws FieldValueProviderException
	{
		when(productModel.getExternalStores()).thenReturn(Collections.singletonList(wileyWebLinkModelMock));
		when(wileyWebLinkModelMock.getType()).thenReturn(WileyWebLinkTypeEnum.GOOGLE_PLAY_STORE);

		final List<String> values = wileyb2cExternalStoreValueProvider.collectValues(null, indexedProperty, productModel);

		assertEquals(values.size(), 0);
	}
}
