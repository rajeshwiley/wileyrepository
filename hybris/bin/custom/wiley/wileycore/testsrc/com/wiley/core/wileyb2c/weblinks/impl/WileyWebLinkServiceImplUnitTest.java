package com.wiley.core.wileyb2c.weblinks.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.enums.WileyWebLinkTypeEnum;
import com.wiley.core.model.WileyWebLinkModel;
import com.wiley.core.wileyb2c.weblinks.providers.WileyWebLinkUrlProvider;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyWebLinkServiceImplUnitTest
{

	private static final String TEST_URL_ONE = "test_url_one";
	private static final String TEST_URL_TWO = "test_url_two";

	private WileyWebLinkServiceImpl underTest = new WileyWebLinkServiceImpl();
	@Mock
	private WileyWebLinkUrlProvider mockUrlProviderOne;
	@Mock
	private WileyWebLinkUrlProvider mockUrlProviderTwo;

	@Mock
	private WileyWebLinkModel mockLinkOne;
	@Mock
	private WileyWebLinkModel mockLinkTwo;



	@Before
	public void setup() {
		when(mockLinkOne.getType()).thenReturn(WileyWebLinkTypeEnum.INSTRUCTORS_COMPANION_SITE);
		when(mockLinkTwo.getType()).thenReturn(WileyWebLinkTypeEnum.REQUEST_EVALUATION_COPY);

		when(mockUrlProviderOne.getWebLinkUrl(mockLinkOne)).thenReturn(TEST_URL_ONE);
		when(mockUrlProviderTwo.getWebLinkUrl(mockLinkTwo)).thenReturn(TEST_URL_TWO);

		Map<WileyWebLinkTypeEnum, WileyWebLinkUrlProvider> providers = new HashMap<>();
		providers.put(WileyWebLinkTypeEnum.INSTRUCTORS_COMPANION_SITE, mockUrlProviderOne);
		underTest.setUrlProviders(providers);
		underTest.setDefaultWebLinkProvider(mockUrlProviderTwo);
	}

	@Test
	public void shouldPickUpProviderForType()
	{
		String actualUrl = underTest.getWebLinkUrl(mockLinkOne);
		assertEquals(TEST_URL_ONE, actualUrl);
		verify(mockUrlProviderOne).getWebLinkUrl(mockLinkOne);
	}

	@Test
	public void shouldUseDefaultWhenNoProviderForType()
	{
		String actualUrl = underTest.getWebLinkUrl(mockLinkTwo);
		assertEquals(TEST_URL_TWO, actualUrl);
		verify(mockUrlProviderTwo).getWebLinkUrl(mockLinkTwo);
	}

	@Test
	public void shouldReturnNullIfLinkCannotBeResolved()
	{
		doThrow(IllegalArgumentException.class).when(mockUrlProviderTwo).getWebLinkUrl(mockLinkTwo);
		String actualUrl = underTest.getWebLinkUrl(mockLinkTwo);
		assertNull(null, actualUrl);
	}



}
