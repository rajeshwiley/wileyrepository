package com.wiley.core.wileyb2c.search.solrfacetsearch.provider.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.variants.model.VariantProductModel;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cCategoryCodeResolverUnitTest
{
	private static final String CATEGORY1 = "category1";
	private static final String CATEGORY2 = "category2";
	private static final String SUBJECTS = "subjects";
	@InjectMocks
	private Wileyb2cCategoryCodeResolver wileyb2cCategoryCodeResolver;
	@Mock
	private IndexedProperty indexedProperty;
	@Mock
	private VariantProductModel variantProductModel;
	@Mock
	private ProductModel baseProductModel;
	@Mock
	private ModelService modelService;
	@Mock
	private TypeService typeService;
	@Mock
	private ComposedTypeModel productType;
	@Mock
	private CategoryModel categoryModel1;
	@Mock
	private CategoryModel categoryModel2;

	@Test
	public void getAttributeValue() throws FieldValueProviderException
	{
		when(variantProductModel.getBaseProduct()).thenReturn(baseProductModel);
		when(indexedProperty.getValueProviderParameters()).thenReturn(Collections.singletonMap("skipVariants", "TRUE"));
		when(typeService.getComposedTypeForClass(any())).thenReturn(productType);
		when(typeService.hasAttribute(productType, SUBJECTS)).thenReturn(true);
		when(modelService.getAttributeValue(baseProductModel, SUBJECTS)).thenReturn(
				Arrays.asList(categoryModel1, categoryModel2));
		when(categoryModel1.getCode()).thenReturn(CATEGORY1);
		when(categoryModel2.getCode()).thenReturn(CATEGORY2);

		final Object attributeValue = wileyb2cCategoryCodeResolver.getAttributeValue(indexedProperty, variantProductModel,
				SUBJECTS);

		final List<CategoryModel> categories = (List<CategoryModel>) attributeValue;
		Assert.assertEquals(categories.size(), 2);
		Assert.assertEquals(categories.get(0), CATEGORY1);
		Assert.assertEquals(categories.get(1), CATEGORY2);
	}
}
