package com.wiley.core.integration.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.exceptions.ExternalSystemException;
import com.wiley.core.integration.article.ArticleGateway;
import com.wiley.core.integration.article.dto.ArticleDto;
import com.wiley.core.integration.article.service.impl.ArticleServiceImpl;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyasArticleServiceUnitTest
{
	private static final String TEST_ID = "1";
	private static final ArticleDto SUCCESS_DTO = new ArticleDto();

	@Mock
	private ArticleGateway articleGatewayMock;

	@InjectMocks
	private ArticleServiceImpl articleService;

	@Test
	public void getArticleDataSuccessTest()
	{
		//Given
		when(articleGatewayMock.getArticleData(TEST_ID)).thenReturn(SUCCESS_DTO);

		//When
		Optional<ArticleDto> optional = articleService.getArticleData(TEST_ID);

		//Then
		assertNotNull(optional);
		assertTrue(optional.isPresent());
		ArticleDto articleDto = optional.get();
		assertNotNull(articleDto);
		assertEquals(SUCCESS_DTO, articleDto);
	}

	@Test
	public void getArticleDataFailTest()
	{
		//Given
		when(articleGatewayMock.getArticleData(TEST_ID)).thenThrow(ExternalSystemException.class);

		//When
		Optional<ArticleDto> optional = articleService.getArticleData(TEST_ID);

		//Then
		assertNotNull(optional);
		assertFalse(optional.isPresent());
	}
}
