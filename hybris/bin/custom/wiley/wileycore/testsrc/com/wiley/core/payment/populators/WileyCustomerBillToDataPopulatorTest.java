package com.wiley.core.payment.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.payment.data.CustomerBillToData;
import de.hybris.platform.core.model.user.AddressModel;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyCustomerBillToDataPopulatorTest
{
	private static final String COUNTRY_NUMERIC_ISOCODE = "826";
	private static final String BILL_TO_COUNTRY = "US";
	private static final String REGION_NAME = "Alabama";

	private final WileyCustomerBillToDataPopulator testedInstance = new WileyCustomerBillToDataPopulator();

	@Mock(answer = Answers.RETURNS_DEEP_STUBS)
	private AddressModel paymentAddress;

	private CustomerBillToData customerBillToData = new CustomerBillToData();

	@Test
	public void shouldPopulateFieldsFromCountryModel()
	{
		when(paymentAddress.getCountry().getNumeric()).thenReturn(COUNTRY_NUMERIC_ISOCODE);
		when(paymentAddress.getCountry().getName()).thenReturn(BILL_TO_COUNTRY);

		testedInstance.populate(paymentAddress, customerBillToData);

		assertEquals(COUNTRY_NUMERIC_ISOCODE, customerBillToData.getCountryNumericIsocode());
		assertEquals(BILL_TO_COUNTRY, customerBillToData.getBillToCountryName());
	}

	@Test
	public void shouldPopulateFieldsFromRegionModel() throws Exception
	{
		// given
		when(paymentAddress.getRegion().getName()).thenReturn(REGION_NAME);

		// preparations

		// when
		testedInstance.populate(paymentAddress, customerBillToData);

		// then
		assertEquals(REGION_NAME, customerBillToData.getBillToStateName());
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldValidateParams() throws Exception
	{
		// given
		when(paymentAddress.getCountry()).thenReturn(null);

		// preparations

		// when
		testedInstance.populate(paymentAddress, customerBillToData);

		// then
		// expect exception
	}
}
