package com.wiley.core.delivery.interceptors;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.daos.DeliveryModeDao;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import javax.annotation.Resource;


/**
 * Created by Uladzimir_Barouski on 6/15/2016.
 */
@Ignore // todo related functionality was commented
public class WileycomDropExternalDeliveryModeOrderRemoveInterceptorIntegrationTest extends
		AbstractWileyServicelayerTransactionalTest
{

	private static final String ORDER_CODE_1 = "test-cart1";
	private static final String ORDER_CODE_2 = "test-cart2";
	private static final String CUSTOMER_ID = "customer@test.com";

	private static final String EXTERNAL_DELIVERY_MODE_CODE_1 = "externalDeliveryMode1";

	private static final String DELIVERY_MODE_CODE_1 = "deliveryMode1";

	@Resource
	private UserService userService;

	@Resource
	private CommerceCartService commerceCartService;

	@Resource
	private ModelService modelService;

	@Resource
	private DeliveryModeDao deliveryModeDao;

	private UserModel user;

	@Before
	public void setUp() throws Exception
	{
		importCsv("/wileycore/test/sampledata/wileycom/order/WileycomExternalDeliveryModeRemoveInterceptorIntegrationTest.impex",
				DEFAULT_ENCODING);
		user = userService.getUserForUID(CUSTOMER_ID);
	}

	@Test
	public void testRemoveExternalDeliveryModeWhenCartRemoved()
	{
		final AbstractOrderModel abstractOrder = commerceCartService.getCartForCodeAndUser(ORDER_CODE_1, user);
		modelService.remove(abstractOrder);
		//Check externalDeliveryMode1 removed from DB
		Assert.assertEquals(0, deliveryModeDao.findDeliveryModesByCode(EXTERNAL_DELIVERY_MODE_CODE_1).size());
	}

	@Test
	public void testRemoveDeliveryModeWhenCartRemoved()
	{
		final AbstractOrderModel abstractOrder = commerceCartService.getCartForCodeAndUser(ORDER_CODE_2, user);
		modelService.remove(abstractOrder);
		//Check deliveryMode1 wasn't removed from DB
		Assert.assertEquals(1, deliveryModeDao.findDeliveryModesByCode(DELIVERY_MODE_CODE_1).size());

	}
}
