package com.wiley.core.payment.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.acceleratorservices.payment.data.CustomerInfoData;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.CartService;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.customer.service.WileyCustomerAccountService;
import com.wiley.core.model.PendingBillingAddressModel;

import static junit.framework.Assert.assertEquals;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CustomerInfoHttpAuthoriseResultPopulatorTest
{
	private static final String CITY = "New York";
	private static final String COMPANY = "HYBRIS";
	private static final String EMAIL = "test@test.com";
	private static final String FIRSTNAME = "Leo";
	private static final String LASTNAME = "Gavrilov";
	private static final String ADDRESS_LINE_1 = "14 Wallace Road";
	private static final String POSTALCODE = "SO19 9GX";
	private static final String REGION = "NA";
	private static final String COUNTRY_ISO_CODE = "826";
	private static final String TRANSACTION_ID_VALUE = "transactionId";
	private static final String TRANSACTION_ID_PARAM = "transID";


	@InjectMocks
	private CustomerInfoHttpAuthoriseResultPopulator testedInstance = new CustomerInfoHttpAuthoriseResultPopulator();

	@Mock
	private CartService cartServiceMock;

	@Mock
	private CartModel cartModelMock;
	@Mock
	private CustomerModel cartUser;
	@Mock
	private WileyCustomerAccountService wileyCustomerAccountServiceMock;

	private AddressModel cartBillingAddress = new AddressModel();
	private PendingBillingAddressModel userPendingBillingAddress = new PendingBillingAddressModel();
	private AddressModel userPaymentAddress = new AddressModel();
	private AddressModel cartDeliveryAddress = new AddressModel();



	private CreateSubscriptionResult createSubscriptionResult = new CreateSubscriptionResult();
	private Map<String, String> parametersMap = new HashMap<>();
	private CustomerInfoData customerInfoData = new CustomerInfoData();

	@Before
	public void setUp()
	{
		createSubscriptionResult.setCustomerInfoData(customerInfoData);
		parametersMap.put(TRANSACTION_ID_PARAM, TRANSACTION_ID_VALUE);

		populateAddress(cartBillingAddress);
		populateAddress(userPendingBillingAddress);
		populateAddress(userPaymentAddress);
		populateAddress(cartDeliveryAddress);

		when(cartServiceMock.getSessionCart()).thenReturn(cartModelMock);
		when(cartModelMock.getUser()).thenReturn(cartUser);
	}

	@Test
	public void shouldTakeCartBillingAddressWhenTransactionMatch()
	{
		when(cartModelMock.getPaymentAddress()).thenReturn(cartBillingAddress);

		testedInstance.populate(parametersMap, createSubscriptionResult);
		validateAddress(createSubscriptionResult.getCustomerInfoData());
	}

	@Test
	public void shouldTakePendingBillingAddressWhenTransactionMatch()
	{
		when(cartUser.getPendingBillingAddress()).thenReturn(userPendingBillingAddress);

		testedInstance.populate(parametersMap, createSubscriptionResult);
		validateAddress(createSubscriptionResult.getCustomerInfoData());
	}

	@Test
	public void shouldTakeCustomerPaymentAddressWhenTransactionDoesNotMatch()
	{
		userPaymentAddress.setTransactionId(null);
		when(wileyCustomerAccountServiceMock.getPaymentAddress(cartUser)).thenReturn(userPaymentAddress);

		testedInstance.populate(parametersMap, createSubscriptionResult);
		validateAddress(createSubscriptionResult.getCustomerInfoData());
	}

	@Test
	public void shouldDefaultToDeliveryAddressWhenNoMatch()
	{
		cartDeliveryAddress.setTransactionId(null);
		when(cartModelMock.getDeliveryAddress()).thenReturn(cartDeliveryAddress);

		testedInstance.populate(parametersMap, createSubscriptionResult);
		validateAddress(createSubscriptionResult.getCustomerInfoData());
	}


	private void validateAddress(final CustomerInfoData customerInfoData)
	{
		assertEquals(CITY, customerInfoData.getBillToCity());
		assertEquals(COMPANY, customerInfoData.getBillToCompany());
		assertEquals(EMAIL, customerInfoData.getBillToEmail());
		assertEquals(FIRSTNAME, customerInfoData.getBillToFirstName());
		assertEquals(LASTNAME, customerInfoData.getBillToLastName());
		assertEquals(POSTALCODE, customerInfoData.getBillToPostalCode());
		assertEquals(ADDRESS_LINE_1, customerInfoData.getBillToStreet1());
		assertEquals(REGION, customerInfoData.getBillToState());
		assertEquals(COUNTRY_ISO_CODE, customerInfoData.getBillToCountry());
	}

	private void populateAddress(final AddressModel address)
	{
		address.setTown(CITY);
		address.setCompany(COMPANY);
		address.setEmail(EMAIL);
		address.setFirstname(FIRSTNAME);
		address.setLastname(LASTNAME);
		address.setPostalcode(POSTALCODE);
		address.setStreetname(ADDRESS_LINE_1);
		address.setTransactionId(TRANSACTION_ID_VALUE);

		final RegionModel region = new RegionModel();
		region.setIsocodeShort(REGION);
		address.setRegion(region);

		final CountryModel country = new CountryModel();
		country.setIsocode(COUNTRY_ISO_CODE);
		address.setCountry(country);
	}
}