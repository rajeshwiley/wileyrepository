package com.wiley.core.resolver.impl;

import com.wiley.core.category.WileyCategoryService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.variants.model.VariantValueCategoryModel;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Unit tests for {@link WelCategoryModelUrlResolver}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WelCategoryModelUrlResolverUnitTest
{
	private static final String ROOT_CATEGORY_NAME = "root-category-name";
	private static final String ROOT_CATEGORY_SEO_LABEL = "root category seo label";

	private static final String SUBCATEGORY_CODE = "SUBCATEGORY_LEVEL_I";
	private static final String SUBCATEGORY_NAME = "subcategory-name";
	private static final String SUBCATEGORY_SEO_LABEL = "subcategory seo label";

	private static final String CATEGORY_CODE = "category-code";

	private static final String WEL_CATEGORY_URL_PATTERN = "/{category-path}/products/{subcategory-path}";
	private static final String DEFAULT_CATEGORY_URL_PATTERN = "/{category-path}/c/{category-code}";

	private static final String AGS_PRODUCT_CATALOG = "agsProductCatalog";
	private static final String WEL_PRODUCT_CATALOG = "welProductCatalog";
	private static final String SLASH = "/";

	@Mock
	private BaseSiteService mockBaseSiteService;

	@Mock
	private BaseSiteModel mockBaseSiteModel;

	@Mock
	private CommerceCategoryService mockCommerceCategoryService;

	@Mock
	private CategoryModel mockCategoryModel;

	@Mock
	private CategoryModel mockRootCategoryModel;

	@Mock
	private VariantValueCategoryModel mockSubcategoryModel;

	@Mock
	private CatalogVersionModel mockCatalogVersionModel;

	@Mock
	private CatalogModel mockCatalogModel;

	@Mock
	private WileyCategoryService mockWileyCategoryService;

	@InjectMocks
	private WelCategoryModelUrlResolver testedInstance = new WelCategoryModelUrlResolver();

	@Before
	public void setUp()
	{
		List<CategoryModel> categories = Arrays.asList(mockRootCategoryModel, mockSubcategoryModel);
		Collection<List<CategoryModel>> pathsForCategory = Collections.singletonList(categories);

		testedInstance.setPattern(DEFAULT_CATEGORY_URL_PATTERN);
		testedInstance.setCategoryPageUrlPatterns(
				Collections.singletonMap(WEL_PRODUCT_CATALOG, WEL_CATEGORY_URL_PATTERN));

		when(mockSubcategoryModel.getCode()).thenReturn(SUBCATEGORY_CODE);
		when(mockCommerceCategoryService.getPathsForCategory(mockCategoryModel)).thenReturn(pathsForCategory);
		when(mockRootCategoryModel.getName()).thenReturn(ROOT_CATEGORY_NAME);
		when(mockSubcategoryModel.getName()).thenReturn(SUBCATEGORY_NAME);
		when(mockCategoryModel.getCode()).thenReturn(CATEGORY_CODE);
		when(mockCategoryModel.getCatalogVersion()).thenReturn(mockCatalogVersionModel);
		when(mockCatalogVersionModel.getCatalog()).thenReturn(mockCatalogModel);
		when(mockWileyCategoryService.getPrimaryWileyCategoryForVariantCategory(mockSubcategoryModel))
				.thenReturn(mockRootCategoryModel);
	}

	@Test
	public void shouldUseSeoLabelIfAvailableForWelSite() throws UnsupportedEncodingException
	{
		when(mockCatalogModel.getId()).thenReturn(WEL_PRODUCT_CATALOG);
		when(mockRootCategoryModel.getSeoLabel()).thenReturn(ROOT_CATEGORY_SEO_LABEL);
		when(mockSubcategoryModel.getSeoLabel()).thenReturn(SUBCATEGORY_SEO_LABEL);

		String resolvedUrl = testedInstance.resolveInternal(mockCategoryModel);
		String expectedUrl = MessageFormat.format("/{0}/products/{1}/",
				convertToSafeAndLowerCaseUrl(ROOT_CATEGORY_SEO_LABEL),
				convertToSafeAndLowerCaseUrl(SUBCATEGORY_SEO_LABEL));

		assertEquals("URL should contain model.seoLabel if available.", expectedUrl, resolvedUrl);
	}

	@Test
	public void shouldUseNameIfSeoLabelIsNotAvailableForWelSite() throws UnsupportedEncodingException
	{
		when(mockCatalogModel.getId()).thenReturn(WEL_PRODUCT_CATALOG);
		when(mockRootCategoryModel.getSeoLabel()).thenReturn(null);
		when(mockSubcategoryModel.getSeoLabel()).thenReturn(null);

		String resolvedUrl = testedInstance.resolveInternal(mockCategoryModel);
		String expectedUrl = MessageFormat.format("/{0}/products/{1}/",
				convertToSafeAndLowerCaseUrl(ROOT_CATEGORY_NAME),
				convertToSafeAndLowerCaseUrl(SUBCATEGORY_NAME));

		assertEquals("URL should contain model.name if model.seoLabel is not available.", expectedUrl, resolvedUrl);
	}

	@Test
	public void shouldUseDefaultUrlResolutionForNonWelSites()
	{
		when(mockCatalogModel.getId()).thenReturn(AGS_PRODUCT_CATALOG);

		String resolvedUrl = testedInstance.resolveInternal(mockCategoryModel);
		String expectedUrl = SLASH + ROOT_CATEGORY_NAME + SLASH + SUBCATEGORY_NAME + SLASH + "c" + SLASH + CATEGORY_CODE;

		assertEquals("Default URL should be used for non-wel site categories.", expectedUrl, resolvedUrl);
	}

	private String convertToSafeAndLowerCaseUrl(final String text) throws UnsupportedEncodingException
	{
		return URLEncoder.encode(text, "utf-8").replaceAll("[^%A-Za-z0-9\\-]+", "-").toLowerCase();
	}
}
