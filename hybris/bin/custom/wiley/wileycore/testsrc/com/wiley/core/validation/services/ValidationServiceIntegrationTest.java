package com.wiley.core.validation.services;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.validation.exceptions.HybrisConstraintViolation;
import de.hybris.platform.validation.model.constraints.ConstraintGroupModel;
import de.hybris.platform.validation.services.ValidationService;

import java.util.Collections;
import java.util.Locale;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.validation.localized.WileyLocalizedAttributeConstraint;
import com.wiley.core.validation.service.ConstraintGroupService;
import com.wiley.core.validation.service.impl.WileyLocalizedHybrisConstraintViolation;

import static org.apache.commons.lang.LocaleUtils.toLocale;
import static org.fest.assertions.Assertions.assertThat;


/**
 * Test covers validation scenarios customized by {@link WileyLocalizedAttributeConstraint} and
 * {@link WileyLocalizedHybrisConstraintViolation}.
 */
@IntegrationTest
public class ValidationServiceIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	private static final String IMPEX_FOLDER = "/wileycore/test/validation/service/ValidationServiceIntegrationTest";
	private static final String TEST_CATEGORY_CODE = "testCategoryCode";
	private static final String TEST_SESSION_LANG = "en";
	private static final Locale EN_US_LOCALE = toLocale("en_US");
	private static final Locale EN_DE_LOCALE = toLocale("en_DE");
	private static final String TEST_CONSTRAINT_GROUP_NO_CONTRAINTS = "testConstraintGroupNoConstraints";
	private static final String TEST_CONSTRAINT_GROUP_EN_SIZE_CONSTRAINT = "testConstraintGroupEnSizeConstraint";
	private static final String TEST_CONSTRAINT_GROUP_EN_US_SIZE_CONSTRAINT = "testConstraintGroupEnUsSizeConstraint";
	private static final String TEST_CONSTRAINT_GROUP_EN_DE_SIZE_CONSTRAINT = "testConstraintGroupEnDeSizeConstraint";
	private static final String TEST_EN_MESSAGE = "The attribute \"name\" must not be null in language: en.";
	private static final String TEST_EN_US_MESSAGE = "The attribute \"name\" must not be null in language: en_US.";
	private static final String TEST_EN_DE_MESSAGE = "The attribute \"name\" must not be null in language: en_DE.";
	private static final String TEST_CATALOG = "testCatalog";
	private static final String TEST_CATALOG_VERSION = "Online";

	@Resource
	private ValidationService validationService;

	@Resource
	private ModelService modelService;

	@Resource
	private ConstraintGroupService constraintGroupService;

	@Resource
	private CommonI18NService commonI18NService;

	@Resource
	private CatalogVersionService catalogVersionService;

	private CatalogVersionModel catalogVersionModel;

	@Before
	public void setUp() throws Exception
	{
		commonI18NService.setCurrentLanguage(commonI18NService.getLanguage(TEST_SESSION_LANG));
		importCsv(IMPEX_FOLDER + "/catalog.impex", DEFAULT_ENCODING);
		catalogVersionModel = catalogVersionService.getCatalogVersion(TEST_CATALOG, TEST_CATALOG_VERSION);
	}

	@Test
	public void shouldPassValidation() throws ImpExException
	{
		//Given
		importCsv(IMPEX_FOLDER + "/group-no-constrains.impex", DEFAULT_ENCODING);
		validationService.reloadValidationEngine();
		CategoryModel category = createCategory(null, Locale.ENGLISH);
		ConstraintGroupModel group = constraintGroupService.getConstraintGroupForId(TEST_CONSTRAINT_GROUP_NO_CONTRAINTS);

		//When
		Set<HybrisConstraintViolation> violations = validationService.validate(category, Collections.singletonList(group));

		//Than
		assertThat(violations).isEmpty();
	}

	@Test
	public void shouldViolateEnNullName() throws ImpExException
	{
		//Given
		importCsv(IMPEX_FOLDER + "/group-with-en-null-constraint.impex", DEFAULT_ENCODING);
		validationService.reloadValidationEngine();
		CategoryModel category = createCategory(null, Locale.ENGLISH);
		ConstraintGroupModel group = constraintGroupService.getConstraintGroupForId(TEST_CONSTRAINT_GROUP_EN_SIZE_CONSTRAINT);

		//When
		Set<HybrisConstraintViolation> violations = validationService.validate(category, Collections.singletonList(group));

		//Than
		assertThat(violations).isNotEmpty();
		assertThat(violations).hasSize(1);
		assertThat(violations.iterator().next().getLocalizedMessage()).isEqualTo(TEST_EN_MESSAGE);
	}

	@Test
	public void shoulPassNonNullEnName() throws ImpExException
	{
		//Given
		importCsv(IMPEX_FOLDER + "/group-with-en-null-constraint.impex", DEFAULT_ENCODING);
		validationService.reloadValidationEngine();
		CategoryModel category = createCategory(StringUtils.EMPTY, Locale.ENGLISH);
		ConstraintGroupModel group = constraintGroupService.getConstraintGroupForId(TEST_CONSTRAINT_GROUP_EN_SIZE_CONSTRAINT);

		//When
		Set<HybrisConstraintViolation> violations = validationService.validate(category, Collections.singletonList(group));

		//Than
		assertThat(violations).isEmpty();
	}

	@Test
	public void shouldViolateEnUsNullName() throws ImpExException
	{
		//Given
		importCsv(IMPEX_FOLDER + "/group-with-en-us-null-constraint.impex", DEFAULT_ENCODING);
		validationService.reloadValidationEngine();
		CategoryModel category = createCategory(null, EN_US_LOCALE);
		ConstraintGroupModel group = constraintGroupService.getConstraintGroupForId(TEST_CONSTRAINT_GROUP_EN_US_SIZE_CONSTRAINT);

		//When
		Set<HybrisConstraintViolation> violations = validationService.validate(category, Collections.singletonList(group));

		//Than
		assertThat(violations).isNotEmpty();
		assertThat(violations).hasSize(1);
		assertThat(violations.iterator().next().getLocalizedMessage()).isEqualTo(TEST_EN_US_MESSAGE);
	}

	@Test
	public void shouldPassNonNullEnUsName() throws ImpExException
	{
		//Given
		importCsv(IMPEX_FOLDER + "/group-with-en-us-null-constraint.impex", DEFAULT_ENCODING);
		validationService.reloadValidationEngine();
		CategoryModel category = createCategory(StringUtils.EMPTY, EN_US_LOCALE);
		ConstraintGroupModel group = constraintGroupService.getConstraintGroupForId(TEST_CONSTRAINT_GROUP_EN_US_SIZE_CONSTRAINT);

		//When
		Set<HybrisConstraintViolation> violations = validationService.validate(category, Collections.singletonList(group));

		//Than
		assertThat(violations).isEmpty();
	}

	@Test
	public void shouldViolateEnDeNullName() throws ImpExException
	{
		//Given
		importCsv(IMPEX_FOLDER + "/group-with-en-de-null-constraint.impex", DEFAULT_ENCODING);
		validationService.reloadValidationEngine();
		CategoryModel category = createCategory(null, EN_DE_LOCALE);
		ConstraintGroupModel group = constraintGroupService.getConstraintGroupForId(TEST_CONSTRAINT_GROUP_EN_DE_SIZE_CONSTRAINT);

		//When
		Set<HybrisConstraintViolation> violations = validationService.validate(category, Collections.singletonList(group));

		//Than
		assertThat(violations).isNotEmpty();
		assertThat(violations).hasSize(1);
		assertThat(violations.iterator().next().getLocalizedMessage()).isEqualTo(TEST_EN_DE_MESSAGE);
	}


	private CategoryModel createCategory(final String name, final Locale locale)
	{
		CategoryModel category = modelService.create(CategoryModel.class);
		category.setCatalogVersion(catalogVersionModel);
		category.setCode(TEST_CATEGORY_CODE);
		category.setName(name, locale);
		modelService.save(category);
		return category;
	}
}
