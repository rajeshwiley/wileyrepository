package com.wiley.core.validation.service.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CountryModel;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.enums.TaxExemptionEnum;
import com.wiley.core.exceptions.WileyVatTaxNumberNotValidException;
import com.wiley.core.integration.vies.ViesCheckVatGateway;
import com.wiley.core.integration.vies.dto.CheckVatResponseDto;


/**
 * Created by Maksim_Kozich on 06.06.18
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyTaxValidationServiceImplUnitTest
{
	public static final String TAX_NUMBER = "TAX NUMBER";
	public static final String COUNTRY_ISO = "COUNTRY ISO";
	@Mock
	private CountryModel countryModelMock;

	@Mock
	private ViesCheckVatGateway viesCheckVatGatewayMock;

	@Mock
	private CheckVatResponseDto checkVatResponseDtoMock;

	@InjectMocks
	private WileyTaxValidationServiceImpl testInstance;

	@Before
	public void before()
	{
		when(countryModelMock.getIsocode()).thenReturn(COUNTRY_ISO);
		when(countryModelMock.getApplicableTaxExemption()).thenReturn(TaxExemptionEnum.VAT);
	}

	@Test(expected = WileyVatTaxNumberNotValidException.class)
	public void shouldThrowWileyVatTaxNumberNotValidExceptionIfViesResponseIsInvalid()
	{
		// given
		when(viesCheckVatGatewayMock.checkVat(COUNTRY_ISO, TAX_NUMBER)).thenReturn(checkVatResponseDtoMock);
		when(checkVatResponseDtoMock.getValid()).thenReturn(false);

		// when
		boolean result = testInstance.validateTaxInformation(countryModelMock, TAX_NUMBER);

		// then
		// exception is expected
	}

	@Test
	public void shouldReturnTrueIfViesResponseIsValid()
	{
		// given
		when(viesCheckVatGatewayMock.checkVat(COUNTRY_ISO, TAX_NUMBER)).thenReturn(checkVatResponseDtoMock);
		when(checkVatResponseDtoMock.getValid()).thenReturn(true);

		// when
		boolean result = testInstance.validateTaxInformation(countryModelMock, TAX_NUMBER);

		// then
		Assert.assertTrue(result);
	}

	@Test
	public void shouldReturnFalseIfViesServiceThrowsException()
	{
		// given
		when(viesCheckVatGatewayMock.checkVat(COUNTRY_ISO, TAX_NUMBER)).thenThrow(new RuntimeException());

		// when
		boolean result = testInstance.validateTaxInformation(countryModelMock, TAX_NUMBER);

		// then
		Assert.assertFalse(result);
	}

	@Test
	public void shouldNotCallViesGatewayIfNullApplicableTaxExemption()
	{
		// given
		when(countryModelMock.getApplicableTaxExemption()).thenReturn(null);

		// when
		testInstance.validateTaxInformation(countryModelMock, TAX_NUMBER);

		// then
		verify(viesCheckVatGatewayMock, never()).checkVat(any(), any());
	}

	@Test
	public void shouldNotCallViesGatewayIfTaxApplicableTaxExemption()
	{
		// given
		when(countryModelMock.getApplicableTaxExemption()).thenReturn(TaxExemptionEnum.TAX);

		// when
		testInstance.validateTaxInformation(countryModelMock, TAX_NUMBER);

		// then
		verify(viesCheckVatGatewayMock, never()).checkVat(any(), any());
	}

	@Test
	public void shouldNotCallViesGatewayIfNullTaxNumber()
	{
		// given

		// when
		testInstance.validateTaxInformation(countryModelMock, null);

		// then
		verify(viesCheckVatGatewayMock, never()).checkVat(any(), any());
	}

	@Test
	public void shouldNotCallViesGatewayIfEmptyTaxNumber()
	{
		// given

		// when
		testInstance.validateTaxInformation(countryModelMock, "");

		// then
		verify(viesCheckVatGatewayMock, never()).checkVat(any(), any());
	}

}