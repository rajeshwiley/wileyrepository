package com.wiley.core.subscription.services.impl;


import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.subscriptionservices.enums.SubscriptionStatus;
import de.hybris.platform.subscriptionservices.enums.TermOfServiceFrequency;
import de.hybris.platform.subscriptionservices.model.SubscriptionProductModel;
import de.hybris.platform.subscriptionservices.model.SubscriptionTermModel;

import java.util.Calendar;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileySubscriptionModel;

import static java.util.Calendar.DAY_OF_MONTH;
import static java.util.Calendar.DAY_OF_YEAR;
import static java.util.Calendar.HOUR_OF_DAY;
import static java.util.Calendar.MINUTE;
import static java.util.Calendar.MONTH;
import static java.util.Calendar.SECOND;
import static java.util.Calendar.YEAR;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;



@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SubscriptionMergingStrategyTest
{

	@Mock
	private WileySubscriptionModel previous;
	@Mock
	private WileySubscriptionModel next;
	@Mock
	private SubscriptionProductModel productMock;
	@Mock
	private SubscriptionTermModel termMock;

	private SubscriptionMergingStrategyImpl strategy = new SubscriptionMergingStrategyImpl();

	@Before
	public void setUp()
	{
		when(previous.getStatus()).thenReturn(SubscriptionStatus.ACTIVE);
		when(previous.getProduct()).thenReturn(productMock);
		when(next.getProduct()).thenReturn(productMock);
		when(productMock.getSubscriptionTerm()).thenReturn(termMock);
	}

	@Test
	public void shouldDetectMissedOrInactivePreviuos()
	{
		mockTerm(TermOfServiceFrequency.MONTHLY, 3);
		Calendar calendar = Calendar.getInstance();
		calendar.set(MONTH, Calendar.JANUARY);
		calendar.set(DAY_OF_MONTH, 3);

		Date result = strategy.mergeSubscriptionsFromDate(null, next, calendar.getTime());
		checkResult(result, calendar.get(YEAR), Calendar.APRIL, 3);

		when(previous.getStatus()).thenReturn(SubscriptionStatus.EXPIRED);
		result = strategy.mergeSubscriptionsFromDate(previous, next, calendar.getTime());
		checkResult(result, calendar.get(YEAR), Calendar.APRIL, 3);

	}

	@Test
	public void shouldMergeWithRealExpiration()
	{
		mockTerm(TermOfServiceFrequency.MONTHLY, 3);
		Calendar calendar = Calendar.getInstance();
		calendar.set(MONTH, Calendar.JANUARY);
		calendar.set(DAY_OF_MONTH, 3);
		mockExpirationIn(previous, calendar.getTime(), 0);

		Date result = strategy.mergeSubscriptionsFromDate(previous, next, calendar.getTime());

		checkResult(result, calendar.get(YEAR), Calendar.APRIL, 3);
	}

	@Test
	public void shouldMergeWithExtension()
	{
		mockTerm(TermOfServiceFrequency.MONTHLY, 3);
		Calendar calendar = Calendar.getInstance();
		calendar.set(MONTH, Calendar.JANUARY);
		calendar.set(DAY_OF_MONTH, 3);
		mockExpirationIn(previous, calendar.getTime(), 3);

		Date result = strategy.mergeSubscriptionsFromDate(previous, next, calendar.getTime());

		checkResult(result, calendar.get(YEAR), Calendar.APRIL, 3 + 3);
	}

	@Test
	public void shouldAnnuallyMergeWithExtension()
	{
		mockTerm(TermOfServiceFrequency.ANNUALLY, 2);
		Calendar calendar = Calendar.getInstance();
		calendar.set(YEAR, 2015);
		calendar.set(MONTH, Calendar.JANUARY);
		calendar.set(DAY_OF_MONTH, 3);
		mockExpirationIn(previous, calendar.getTime(), 300);

		Date result = strategy.mergeSubscriptionsFromDate(previous, next, calendar.getTime());

		calendar.add(DAY_OF_YEAR, 300);
		checkResult(result, 2017, calendar.get(MONTH), calendar.get(DAY_OF_MONTH));
	}

	@Test
	public void shouldAnnuallyLenientMergeWithExtension()
	{
		mockTerm(TermOfServiceFrequency.ANNUALLY, 1);
		Calendar calendar = Calendar.getInstance();
		calendar.set(YEAR, 2016);
		calendar.set(MONTH, Calendar.JANUARY);
		calendar.set(DAY_OF_MONTH, 3);
		mockExpirationIn(previous, calendar.getTime(), 40);

		Date result = strategy.mergeSubscriptionsFromDate(previous, next, calendar.getTime());

		checkResult(result, 2017, Calendar.FEBRUARY, 12);
	}

	@Test
	public void shouldHandleSpecialMonthCases()
	{
		mockTerm(TermOfServiceFrequency.MONTHLY, 1);
		Calendar calendar = Calendar.getInstance();

		// February lenient
		calendar.set(YEAR, 2016);
		calendar.set(MONTH, Calendar.JANUARY);
		calendar.set(DAY_OF_MONTH, 30);
		mockExpirationIn(previous, calendar.getTime(), 0);

		Date result = strategy.mergeSubscriptionsFromDate(previous, next, calendar.getTime());

		checkResult(result, 2016, Calendar.FEBRUARY, 29);

		// February not lenient

		calendar.set(YEAR, 2015);
		calendar.set(MONTH, Calendar.JANUARY);
		calendar.set(DAY_OF_MONTH, 30);
		mockExpirationIn(previous, calendar.getTime(), 0);

		result = strategy.mergeSubscriptionsFromDate(previous, next, calendar.getTime());

		checkResult(result, 2015, Calendar.FEBRUARY, 28);

		// April

		calendar.set(YEAR, 2015);
		calendar.set(MONTH, Calendar.MARCH);
		calendar.set(DAY_OF_MONTH, 31);
		mockExpirationIn(previous, calendar.getTime(), 0);

		result = strategy.mergeSubscriptionsFromDate(previous, next, calendar.getTime());

		checkResult(result, 2015, Calendar.APRIL, 30);
	}

	private void checkResult(final Date result, int year, int month, int day)
	{
		assertNotNull(result);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(result);
		assertEquals(year, calendar.get(YEAR));
		assertEquals(month, calendar.get(MONTH));
		assertEquals(day, calendar.get(DAY_OF_MONTH));
		assertEquals(23, calendar.get(HOUR_OF_DAY));
		assertEquals(59, calendar.get(MINUTE));
		assertEquals(59, calendar.get(SECOND));
	}

	private void mockExpirationIn(final WileySubscriptionModel subscription, final Date from, int days)
	{
		final Calendar calendar = Calendar.getInstance();
		calendar.setTime(from);
		calendar.add(DAY_OF_YEAR, days);
		calendar.set(HOUR_OF_DAY, 23);
		calendar.set(MINUTE, 59);
		calendar.set(SECOND, 59);
		when(subscription.getExpirationDate()).thenReturn(calendar.getTime());
	}

	private void mockTerm(final TermOfServiceFrequency frequency, int number)
	{
		when(termMock.getTermOfServiceFrequency()).thenReturn(frequency);
		when(termMock.getTermOfServiceNumber()).thenReturn(Integer.valueOf(number));
	}


}
