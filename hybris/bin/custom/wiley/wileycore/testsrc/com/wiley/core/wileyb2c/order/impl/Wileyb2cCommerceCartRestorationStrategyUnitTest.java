package com.wiley.core.wileyb2c.order.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.order.CommerceCartCalculationStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.time.TimeService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;


/**
 * Default unit test for {@link Wileyb2cCommerceCartRestorationStrategy}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cCommerceCartRestorationStrategyUnitTest
{
	@Mock
	private BaseStoreService baseStoreServiceMock;

	@Mock
	private TimeService timeServiceMock;

	@Mock
	private CommerceCartCalculationStrategy commerceCartCalculationStrategyMock;

	@Mock
	private CartService cartServiceMock;

	@Mock
	private ModelService modelServiceMock;

	@Mock
	private BaseSiteService baseSiteServiceMock;

	@InjectMocks
	private Wileyb2cCommerceCartRestorationStrategy wileyb2cCommerceCartRestorationStrategy;

	// Test Data

	@Mock
	private CartModel cartModelMock;

	@Mock
	private BaseStoreModel currentBaseStoreModelMock;

	@Mock
	private BaseStoreModel anotherBaseStoreModelMock;

	@Mock
	private BaseSiteModel currentBaseSiteModel;

	@Before
	public void setUp() throws Exception
	{
		ReflectionTestUtils.setField(wileyb2cCommerceCartRestorationStrategy, "baseStoreService", baseStoreServiceMock);

		when(baseStoreServiceMock.getCurrentBaseStore()).thenReturn(currentBaseStoreModelMock);
		when(baseSiteServiceMock.getCurrentBaseSite()).thenReturn(currentBaseSiteModel);

		when(cartModelMock.getPaymentTransactions()).thenReturn(Collections.emptyList());
	}

	@Test
	public void testRestoreCartWhenBaseStoreTheSame() throws Exception
	{
		// Given
		when(cartModelMock.getStore()).thenReturn(currentBaseStoreModelMock);

		CommerceCartParameter parameter = new CommerceCartParameter();
		parameter.setCart(cartModelMock);

		// When
		wileyb2cCommerceCartRestorationStrategy.restoreCart(parameter);

		// Then
		verify(cartModelMock).setSite(same(currentBaseSiteModel));
		verify(modelServiceMock).save(same(cartModelMock));
	}

	@Test
	public void testRestoreCartWhenBaseStoreTheSameAndSiteTheSame() throws Exception
	{
		// Given
		when(cartModelMock.getStore()).thenReturn(currentBaseStoreModelMock);
		when(cartModelMock.getSite()).thenReturn(currentBaseSiteModel);

		CommerceCartParameter parameter = new CommerceCartParameter();
		parameter.setCart(cartModelMock);

		// When
		wileyb2cCommerceCartRestorationStrategy.restoreCart(parameter);

		// Then
		verify(cartModelMock, never()).setSite(any());
		verify(modelServiceMock).save(same(cartModelMock));
	}

	@Test
	public void testRestoreCartWhenCartBaseStoreNotEqualCurrentBaseStore() throws Exception
	{
		// Given
		when(cartModelMock.getStore()).thenReturn(anotherBaseStoreModelMock);

		CommerceCartParameter parameter = new CommerceCartParameter();
		parameter.setCart(cartModelMock);

		// When
		wileyb2cCommerceCartRestorationStrategy.restoreCart(parameter);

		// Then
		verify(cartModelMock, never()).setSite(any());
		verify(cartServiceMock, never()).setSessionCart(same(cartModelMock));
	}

}