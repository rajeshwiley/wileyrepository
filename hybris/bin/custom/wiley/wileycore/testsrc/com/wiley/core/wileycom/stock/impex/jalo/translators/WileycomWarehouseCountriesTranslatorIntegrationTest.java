package com.wiley.core.wileycom.stock.impex.jalo.translators;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.ordersplitting.WarehouseService;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Test;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;


/**
 * @author Dzmitryi_Halahayeu
 */
@IntegrationTest
public class WileycomWarehouseCountriesTranslatorIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	private static final String WAREHOUSE_CODE = "1086";
	private static final String WAREHOUSE_IGNORE_CODE = "1087";
	@Resource
	private WarehouseService warehouseService;

	@Test
	public void testTranslator() throws Exception
	{
		importCsv("/wileycore/test/stock/impex/jalo/translators/WileycomWarehouseCountriesTranslatorIntegrationTest/"
				+ "import-warehouse.impex", DEFAULT_ENCODING);


		WarehouseModel warehouse = warehouseService.getWarehouseForCode(WAREHOUSE_CODE);
		assertEquals(warehouse.getCountries().size(), 4);
		List<String> isoCodes = warehouse.getCountries().stream().map(CountryModel::getIsocode).collect(Collectors.toList());
		assertTrue(isoCodes.contains("IN"));
		assertTrue(isoCodes.contains("JP"));
		assertTrue(isoCodes.contains("AU"));
		assertTrue(isoCodes.contains("CN"));
	}

	public void testTranslatorEmptyCountries() throws Exception
	{
		importCsv("/wileycore/test/stock/impex/jalo/translators/WileycomWarehouseCountriesTranslatorIntegrationTest/"
				+ "import-warehouse.impex", DEFAULT_ENCODING);
		assertEquals(warehouseService.getWarehouseForCode(WAREHOUSE_CODE).getCountries().size(), 4);

		try
		{
			importCsv("/wileycore/test/stock/impex/jalo/translators/WileycomWarehouseCountriesTranslatorIntegrationTest/"
					+ "import-warehouse-empty-countries.impex", DEFAULT_ENCODING);
		}
		catch (AssertionError error)
		{
			Assert.assertTrue(error.getMessage().contains("this String argument must have text"));
		}
	}

	@Test
	public void testTranslatorWrongCountry() throws Exception
	{
		try
		{
			importCsv("/wileycore/test/stock/impex/jalo/translators/WileycomWarehouseCountriesTranslatorIntegrationTest/"
					+ "import-warehouse-incorrect-country.impex", DEFAULT_ENCODING);
			fail("Expect exception failure");
		}
		catch (Throwable e)
		{

			Assert.assertTrue(e instanceof AssertionError);
			Assert.assertTrue(e.getMessage().contains("Can not resolve country for ISO code WRONG"));
		}
	}

	@Test
	public void testTranslatorIgnoreCountries() throws Exception
	{
		importCsv("/wileycore/test/stock/impex/jalo/translators/WileycomWarehouseCountriesTranslatorIntegrationTest/"
				+ "ignore-import-warehouse.impex", DEFAULT_ENCODING);

		assertEquals(warehouseService.getWarehouseForCode(WAREHOUSE_IGNORE_CODE).getCountries().size(), 0);
	}


}




