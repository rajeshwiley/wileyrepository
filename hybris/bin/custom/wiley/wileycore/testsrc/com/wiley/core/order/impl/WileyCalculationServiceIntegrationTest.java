package com.wiley.core.order.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Before;
import org.junit.Test;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.product.WileyProductEditionFormatService;


/**
 * Default integration test for {@link WileyCalculationService}.
 */
@IntegrationTest
public class WileyCalculationServiceIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	private static final String RESOURCE_PREFIX = "/wileycore/test/order/WileyCalculationServiceIntegrationTest/";

	private static final Double ORDER_ENTRY_TOTAL = 30.3;
	private static final Double ORDER_ENTRY_TOTAL_EXTERNAL_PRICE = 44.4;
	private static final Double ORDER_ENTRY_TOTAL_STACKABLE_PRICE = 240.0;
	private static final Double ORDER_ENTRY_TOTAL_EXTERNAL_DISCOUNT = 21.78;

	@Resource(name = "wileyCalculationService")
	private WileyCalculationService wileyCalculationService;

	@Resource
	private ModelService modelService;

	@Resource
	private CommerceCartService commerceCartService;

	@Resource
	private UserService userService;

	@Resource
	private WileyProductEditionFormatService wileyProductEditionFormatService;

	@Before
	public void setUp() throws Exception
	{
		importCsv(RESOURCE_PREFIX + "testCustomer.impex", DEFAULT_ENCODING);
		importCsv(RESOURCE_PREFIX + "testProduct.impex", DEFAULT_ENCODING);
	}

	@Test
	public void shouldCalculateDeliveryCostForOrderWithShippableProduct() throws CalculationException, ImpExException
	{
		// Given
		importCsv(RESOURCE_PREFIX + "orderWithShippableProduct.impex", DEFAULT_ENCODING);
		final UserModel user = userService.getUserForUID("test@hybris.com");

		AbstractOrderModel order = commerceCartService.getCartForCodeAndUser("test-cart1", user);

		// When
		wileyCalculationService.calculate(order);

		// Then
		checkNonDigitalOrder(order);
	}

	@Test
	public void shouldCalculateDeliveryCostForOrderWithShippableProductAndNonShippableProduct()
			throws CalculationException, ImpExException
	{
		// Given
		importCsv(RESOURCE_PREFIX + "orderWithShippableProductAndNonShippableProduct.impex", DEFAULT_ENCODING);
		final UserModel user = userService.getUserForUID("test@hybris.com");

		AbstractOrderModel order = commerceCartService.getCartForCodeAndUser("test-cart1", user);

		// When
		wileyCalculationService.calculate(order);

		// Then
		checkNonDigitalOrder(order);
	}

	@Test
	public void shouldNotCalculateDeliveryCostForOrderWithNonShippableProduct() throws CalculationException, ImpExException
	{
		// Given
		importCsv(
				RESOURCE_PREFIX + "orderWithNonShippableProduct.impex", DEFAULT_ENCODING);
		final UserModel user = userService.getUserForUID("test@hybris.com");

		AbstractOrderModel order = commerceCartService.getCartForCodeAndUser("test-cart1", user);

		// When
		wileyCalculationService.calculate(order);

		// Then
		modelService.refresh(order);
		assertNull("Expected that DeliveryMode was not set to Order for non-shippable product.",
				order.getDeliveryMode());
		assertNotNull("Expected that Order has delivery cost.", order.getDeliveryCost());
		assertEquals("Expected that delivery cost is 0 for order with non-shippable product.", Double.valueOf(0),
				order.getDeliveryCost());
		assertTrue("Expected that cart is digital.", wileyProductEditionFormatService.isDigitalCart(order));
	}

	@Test
	public void shouldCalculateOrderEntryTotalsWithoutDoublePrecisionIssue() throws CalculationException, ImpExException
	{
		// Given
		importCsv(RESOURCE_PREFIX + "orderWithDiscounts.impex", DEFAULT_ENCODING);
		final UserModel user = userService.getUserForUID("test@hybris.com");

		AbstractOrderModel order = commerceCartService.getCartForCodeAndUser("test-cart-with-discounts-code", user);

		// When
		wileyCalculationService.calculate(order);

		// Then
		assertEquals("Expected that total price has been calculated without double precision issue for cart entry.",
				ORDER_ENTRY_TOTAL, getOrderEntryTotal(order));
	}

	@Test
	public void shouldCalculateOrderEntryTotalsUsingExternalPrices() throws ImpExException, CalculationException
	{
		// Given
		importCsv(RESOURCE_PREFIX + "orderWithExternalPrices.impex", DEFAULT_ENCODING);
		final UserModel user = userService.getUserForUID("test@hybris.com");

		AbstractOrderModel order = commerceCartService.getCartForCodeAndUser("test-cart-with-external-prices-code", user);

		// When
		wileyCalculationService.calculate(order);

		// Then
		assertEquals("Expected that total price has been calculated based on external price",
				ORDER_ENTRY_TOTAL_EXTERNAL_PRICE, getOrderEntryTotal(order));
	}

	@Test
	public void shouldCalculateOrderEntryTotalsUsingStackablePrices() throws ImpExException, CalculationException
	{
		importCsv(RESOURCE_PREFIX + "orderWithStackablePrices.impex", DEFAULT_ENCODING);
		final UserModel user = userService.getUserForUID("test@hybris.com");

		AbstractOrderModel order = commerceCartService.getCartForCodeAndUser("test-cart-with-stackable-prices-code", user);

		// When
		wileyCalculationService.calculate(order);

		// Then
		assertEquals("Expected that total price has been calculated based on stackable price",
				ORDER_ENTRY_TOTAL_STACKABLE_PRICE, getOrderEntryTotal(order));
	}


	@Test
	public void shouldCalculateOrderEntryTotalsUsingExternalDiscounts() throws ImpExException, CalculationException
	{
		importCsv(RESOURCE_PREFIX + "orderWithExternalDiscounts.impex", DEFAULT_ENCODING);
		final UserModel user = userService.getUserForUID("test@hybris.com");

		AbstractOrderModel order = commerceCartService.getCartForCodeAndUser("test-cart-with-external-discounts-code", user);

		// When
		wileyCalculationService.calculate(order);

		// Then
		assertEquals("Expected that total price has been calculated using external discounts price",
				ORDER_ENTRY_TOTAL_EXTERNAL_DISCOUNT, getOrderEntryTotal(order));
	}

	private Double getOrderEntryTotal(final AbstractOrderModel abstractOrder)
	{
		List<AbstractOrderEntryModel> orderEntries = abstractOrder.getEntries();
		assertTrue(CollectionUtils.isNotEmpty(orderEntries));
		assertEquals(1, orderEntries.size());
		return orderEntries.get(0).getTotalPrice();
	}
	
	private void checkNonDigitalOrder(final AbstractOrderModel abstractOrder)
	{
		modelService.refresh(abstractOrder);
		assertNotNull("Order should have delivery mode.", abstractOrder.getDeliveryMode());
		assertNotNull("Order should have delivery cost.", abstractOrder.getDeliveryCost());
		assertFalse("Expected that cart is not digital.", wileyProductEditionFormatService.isDigitalCart(abstractOrder));
	}
}