package com.wiley.core.wileycom.storesession.impl;


import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.verification.VerificationMode;

import com.wiley.core.i18n.WileyCommonI18NService;
import com.wiley.core.i18n.WileyCountryService;
import com.wiley.core.servicelayer.i18n.WileycomI18NService;


/**
 * Unit test for {@link WileycomCountryStoreSessionServiceImpl}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileycomCountryStoreSessionServiceImplUnitTest
{
	private static final String US_COUNTRY_ISOCODE = "US";
	private static final String CA_COUNTRY_ISOCODE = "CA";
	private static final String UK_COUNTRY_ISOCODE = "UK";
	private static final boolean FORCE_CART_RECALCULATION = true;

	@Mock
	private CartService cartServiceMock;

	@Mock
	private WileycomI18NService wileycomI18NServiceMock;

	@Mock
	private WileyCommonI18NService wileyCommonI18NService;

	@Mock
	private CommerceCartService commerceCartServiceMock;

	@Mock
	private WileyCountryService wileyCountryServiceMock;

	@Mock
	private CommonI18NService commonI18NServiceMock;

	@Mock
	private CartModel cartMock;

	@Mock
	private CurrencyModel currencyMock;

	private Optional<CountryModel> caCountry;
	private Optional<String> caIsocode;
	private Optional<CountryModel> ukCountry;
	private Optional<String> ukIsocode;
	private Optional<CountryModel> defaultCountry;

	@InjectMocks
	private WileycomCountryStoreSessionServiceImpl wileycomCountryStoreSessionService;


	@Before
	public void setUp()
	{
		//setup cartMock
		when(cartServiceMock.hasSessionCart()).thenReturn(Boolean.TRUE);
		when(cartServiceMock.getSessionCart()).thenReturn(cartMock);

		//setup country optional containers
		caCountry = setupOptionalCountry(CA_COUNTRY_ISOCODE);
		caIsocode = Optional.of(CA_COUNTRY_ISOCODE);
		ukCountry = setupOptionalCountry(UK_COUNTRY_ISOCODE);
		ukIsocode = Optional.of(UK_COUNTRY_ISOCODE);
		defaultCountry = setupOptionalCountry(US_COUNTRY_ISOCODE);

		//setup country lookup
		when(wileyCountryServiceMock.findCountryByCode(ukIsocode.get())).thenReturn(ukCountry.get());

	}

	public Optional<CountryModel> setupOptionalCountry(final String isocode)
	{
		final CountryModel countryModel = mock(CountryModel.class);
		when(countryModel.getIsocode()).thenReturn(isocode);
		when(wileyCommonI18NService.getDefaultCurrency(countryModel)).thenReturn(currencyMock);
		return Optional.of(countryModel);
	}

	@Test
	public void shouldNotUpdateCountryDueToCodeMatch() throws CalculationException
	{
		//Given
		when(wileycomI18NServiceMock.getCurrentCountry()).thenReturn(caCountry);

		//When
		final Optional<CountryModel> sessionCountry = wileycomCountryStoreSessionService.updateCurrentCountryIfRequired(caIsocode,
				FORCE_CART_RECALCULATION);

		//Then
		assertFalse("Isodcode should match to session country and country should not be updated", sessionCountry.isPresent());
		verify(wileycomI18NServiceMock, never()).setCurrentCountry(any(CountryModel.class));
		assertCartRecalulated(never(), caCountry);
	}

	@Test
	public void shouldNotUpdateExistedCountryByEmptyIsocode() throws CalculationException
	{
		//Given
		when(wileycomI18NServiceMock.getCurrentCountry()).thenReturn(caCountry);

		//When
		final Optional<CountryModel> sessionCountry = wileycomCountryStoreSessionService.updateCurrentCountryIfRequired(
				Optional.empty(), FORCE_CART_RECALCULATION);

		//Then
		assertFalse("Session country should not be updated by empty isocode", sessionCountry.isPresent());
		verify(wileycomI18NServiceMock, never()).setCurrentCountry(any(CountryModel.class));
		assertCartRecalulated(never(), caCountry);
	}


	@Test
	public void shouldUpdateCountry() throws CalculationException
	{
		//Given
		when(wileycomI18NServiceMock.getCurrentCountry()).thenReturn(caCountry);

		//When
		final Optional<CountryModel> sessionCountry = wileycomCountryStoreSessionService.updateCurrentCountryIfRequired(ukIsocode,
				FORCE_CART_RECALCULATION);

		//Then
		assertTrue("Session should be updated with new country", sessionCountry.isPresent());
		verify(wileycomI18NServiceMock, times(1)).setCurrentCountry(ukCountry.get());
		assertCartRecalulated(times(1), ukCountry);
	}

	@Test
	public void shouldSetDefaultCountryForEmptyIsocodeAndEmptySessionCountry() throws CalculationException
	{
		//Given
		when(wileycomI18NServiceMock.getCurrentCountry()).thenReturn(Optional.empty());
		when(wileycomI18NServiceMock.setDefaultCurrentCountry()).thenReturn(defaultCountry.get());

		//When
		final Optional<CountryModel> sessionCountry = wileycomCountryStoreSessionService.updateCurrentCountryIfRequired(
				Optional.empty(), FORCE_CART_RECALCULATION);

		//Then
		assertFalse("No country should be return for default country set", sessionCountry.isPresent());
		verify(wileycomI18NServiceMock, times(1)).setDefaultCurrentCountry();
		assertCartRecalulated(times(1), defaultCountry);
	}


	private void assertCartRecalulated(final VerificationMode verificationMode, final Optional<CountryModel> cartCountry)
			throws CalculationException
	{
		verify(commerceCartServiceMock, verificationMode).recalculateCart(any(CommerceCartParameter.class));
	}
}
