package com.wiley.core.order.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.europe1.enums.UserDiscountGroup;
import de.hybris.platform.servicelayer.user.UserService;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.order.dao.WileyOrderEntryDao;

import static de.hybris.platform.core.enums.OrderStatus.CANCELLED;
import static de.hybris.platform.core.enums.OrderStatus.FAILED;


/**
 * Created by Uladzimir_Barouski on 3/2/2016.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyCheckoutServiceImplUnitTest
{

	private static final String UNIVERSITY = "University";
	private static final String FREE_TRIAL_CODE_ORDERED = "freeTrialCode1";
	private static final String FREE_TRIAL_CODE_NOT_ORDERED = "freeTrialCode2";
	private static final OrderStatus[] ORDER_STATUSES_TO_BE_EXCLUDED = { CANCELLED, FAILED };
	private static final String USER_ID = "userId";
	private static final Double TOTAL_PRICE = 5.0;

	@Mock
	private CartModel cartModelMock;

	@Mock
	private UserModel userModelMock;

	@Mock
	private OrderEntryModel orderEntryModelMock;

	@Mock
	private WileyOrderEntryDao wileyOrderEntryDaoMock;

	@Mock
	private UserService userServiceMock;

	@InjectMocks
	private WileyCheckoutServiceImpl wileyCheckoutService;
	
	@Mock
	private AbstractOrderModel orderModelMock;

	@Before
	public void setUp()
	{
		when(userServiceMock.getCurrentUser()).thenReturn(userModelMock);
		when(userModelMock.getUid()).thenReturn(USER_ID);
		when(wileyOrderEntryDaoMock.findOrderEntriesByProductAndOwnerExcludingStatuses(FREE_TRIAL_CODE_ORDERED, USER_ID,
				ORDER_STATUSES_TO_BE_EXCLUDED)).thenReturn(Arrays.asList(orderEntryModelMock));
		when(wileyOrderEntryDaoMock.findOrderEntriesByProductAndOwnerExcludingStatuses(FREE_TRIAL_CODE_NOT_ORDERED, USER_ID,
				ORDER_STATUSES_TO_BE_EXCLUDED)).thenReturn(Collections.emptyList());
		when(orderModelMock.getTaxCalculated()).thenReturn(false);
		when(orderModelMock.getTotalPrice()).thenReturn(TOTAL_PRICE);
	}

	@Test
	public void testIsCartHasStudentVerification() throws Exception
	{
		when(cartModelMock.getUniversity()).thenReturn(UNIVERSITY);

		final boolean result = wileyCheckoutService.isCartHasStudentVerification(cartModelMock);
		assertTrue("Expected that cart have university.", result);
	}

	@Test
	public void testIsCartNotHaveStudentVerification() throws Exception
	{
		when(cartModelMock.getUniversity()).thenReturn(null);

		final boolean result = wileyCheckoutService.isCartHasStudentVerification(cartModelMock);
		assertFalse("Expected that cart doesn't have university.", result);
	}

	@Test
	public void testIsStudentOrder() throws Exception
	{
		when(cartModelMock.getDiscountGroup()).thenReturn(UserDiscountGroup.STUDENT);

		final boolean result = wileyCheckoutService.isStudentOrder(cartModelMock);
		assertTrue("Expected that it is Student flow.", result);
	}

	@Test
	public void testIsNotStudentOrder() throws Exception
	{
		when(cartModelMock.getUniversity()).thenReturn(null);

		final boolean result = wileyCheckoutService.isStudentOrder(cartModelMock);
		assertFalse("Expected that it is not Student flow.", result);
	}


	@Test
	public void testIsFreeTrialWasOrderedByCurrentUser()
	{
		assertTrue(wileyCheckoutService.isFreeTrialWasOrderedByCurrentUser(FREE_TRIAL_CODE_ORDERED));
		assertFalse(wileyCheckoutService.isFreeTrialWasOrderedByCurrentUser(FREE_TRIAL_CODE_NOT_ORDERED));
	}
	
	@Test
	public void shouldReturnTaxAvailableForNotEmptyTaxNumber()
	{
		when(orderModelMock.getTaxNumber()).thenReturn("123");
		assertTrue(wileyCheckoutService.isTaxAvailable(orderModelMock));
	}
	
	@Test
	public void shouldReturnTaxNotAvailableForEmptyTaxNumber()
	{
		assertFalse(wileyCheckoutService.isTaxAvailable(orderModelMock));
	}

	@Test
	public void shouldReturnTaxAvailableForZeroOrder()
	{
		when(orderModelMock.getTotalPrice()).thenReturn(0.0);

		assertTrue(wileyCheckoutService.isTaxAvailable(orderModelMock));
	}
}