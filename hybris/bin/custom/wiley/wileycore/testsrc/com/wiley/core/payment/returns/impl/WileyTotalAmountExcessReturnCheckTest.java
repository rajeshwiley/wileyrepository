package com.wiley.core.payment.returns.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;



import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doReturn;

import com.wiley.core.payment.transaction.returns.impl.WileyTotalAmountExcessReturnCheck;
import com.wiley.core.refund.WileyRefundCalculationService;


/**
 * Unit test for {@link WileyTotalAmountExcessReturnCheck}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyTotalAmountExcessReturnCheckTest
{
	private static final BigDecimal ZERO = BigDecimal.ZERO;
	private static final BigDecimal POSITIVE_NUMBER = BigDecimal.valueOf(100);

	@InjectMocks
	private WileyTotalAmountExcessReturnCheck wileyTotalAmountExcessReturnCheck;

	@Mock
	private OrderModel orderModelMock;

	@Mock
	private WileyRefundCalculationService calculationServiceMock;


	@Test
	public void returnAmountExcessTotals()
	{
		// Given
		doReturn(ZERO).when(calculationServiceMock).calculateMaximumAvailableRefundAmountForOrder(orderModelMock);

		// Testing
		final boolean result = wileyTotalAmountExcessReturnCheck.perform(orderModelMock, null, 0L);

		// Verify
		assertFalse(result);
	}

	@Test
	public void returnAmountDoesNotExcedTotals()
	{
		// Given
		doReturn(POSITIVE_NUMBER).when(calculationServiceMock).calculateMaximumAvailableRefundAmountForOrder(orderModelMock);

		// Testing
		final boolean result = wileyTotalAmountExcessReturnCheck.perform(orderModelMock, null, 0L);

		// Verify
		assertTrue(result);
	}
}
