package com.wiley.core.wileyb2c.subscription.services.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.subscriptionservices.model.SubscriptionTermModel;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyVararg;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyProductModel;
import com.wiley.core.model.WileySubscriptionModel;
import com.wiley.core.order.dao.WileyOrderEntryDao;
import com.wiley.core.wileyb2c.subscription.dao.Wileyb2cSubscriptionDao;

import static com.wiley.core.enums.WileyProductSubtypeEnum.SUBSCRIPTION;
import static de.hybris.platform.core.enums.OrderStatus.CANCELLED;
import static de.hybris.platform.core.enums.OrderStatus.CHECKED_INVALID;
import static de.hybris.platform.core.enums.OrderStatus.FAILED;
import static java.util.Arrays.asList;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cSubscriptionServiceImplUnitTest
{
	private static final String PRODUCT_CODE = "productCode";
	private static final String SUBSCRIPTION_PRODUCT_CODE = "subscriptionProductCode";
	private static final String CUSTOMER_UID = "customerUid";
	private static final String SUBSCRIPTION_TERM_ID = "subscr_term_id";

	@Spy
	@InjectMocks
	private Wileyb2cSubscriptionServiceImpl testInstance = new Wileyb2cSubscriptionServiceImpl();

	@Mock
	private Wileyb2cSubscriptionDao mockSubscriptionDao;
	@Mock
	private WileyOrderEntryDao mockOrderEntryDao;

	@Mock
	private WileyProductModel mockSubscriptionProduct;
	@Mock
	private WileyProductModel mockProduct;
	@Mock
	private CustomerModel mockCustomer;
	@Mock
	private WileySubscriptionModel mockSubscription;
	@Mock
	private SubscriptionTermModel mockSubscriptionTerm;
	@Mock
	private OrderEntryModel mockOrderEntry;


	@Before
	public void setUp()
	{
		when(mockSubscriptionTerm.getId()).thenReturn(SUBSCRIPTION_TERM_ID);
		when(mockCustomer.getUid()).thenReturn(CUSTOMER_UID);
		when(mockProduct.getCode()).thenReturn(PRODUCT_CODE);
		when(mockSubscriptionProduct.getCode()).thenReturn(SUBSCRIPTION_PRODUCT_CODE);
		when(mockSubscriptionProduct.getSubtype()).thenReturn(SUBSCRIPTION);
		when(mockSubscriptionProduct.getSubscriptionTerms()).thenReturn(getSubscriptions());
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowAnExceptionIfProductIsNotSubscription()
	{
		assertTrue(testInstance.isCustomerHasSubscription(mockProduct, mockCustomer));
	}

	@Test
	public void shouldReturnTrueIfCustomerHasSubscriptionsForProduct()
	{
		givenCustomerHasSubscriptionsForProduct(SUBSCRIPTION_PRODUCT_CODE, CUSTOMER_UID, asList(mockSubscription));
		assertTrue(testInstance.isCustomerHasSubscription(mockSubscriptionProduct, mockCustomer));
	}

	@Test
	public void shouldReturnTrueIfCustomerHasOrderEntriesForSubscriptionProduct()
	{
		givenCustomerHasOrderEntriesForSubscriptionsProduct(SUBSCRIPTION_PRODUCT_CODE, CUSTOMER_UID, asList(mockOrderEntry));
		assertTrue(testInstance.isCustomerHasSubscription(mockSubscriptionProduct, mockCustomer));
	}

	@Test
	public void shouldExcludeSomeOrderStatusesForCustomerHasSubscriptionCheck()
	{
		testInstance.isCustomerHasSubscription(mockSubscriptionProduct, mockCustomer);
		final OrderStatus[] orderStatuesToBeExcluded = { CANCELLED, FAILED, CHECKED_INVALID };
		verify(mockOrderEntryDao).findOrderEntriesByProductAndOwnerExcludingStatuses(SUBSCRIPTION_PRODUCT_CODE, CUSTOMER_UID,
				orderStatuesToBeExcluded);
	}

	@Test
	public void shouldReturnFalseIfCustomerHasNoSubscriptionsAndOrderEntriesForProduct()
	{
		givenCustomerHasSubscriptionsForProduct(SUBSCRIPTION_PRODUCT_CODE, CUSTOMER_UID, Collections.emptyList());
		givenCustomerHasOrderEntriesForSubscriptionsProduct(SUBSCRIPTION_PRODUCT_CODE, CUSTOMER_UID, Collections.emptyList());

		assertFalse(testInstance.isCustomerHasSubscription(mockSubscriptionProduct, mockCustomer));
	}

	@Test
	public void shouldReturnTrueIfProductIsSubscription()
	{
		assertTrue(testInstance.isProductSubscription(mockSubscriptionProduct));
	}

	@Test
	public void shouldReturnFalseIfProductSubtypeIsNotSubscription()
	{
		assertFalse(testInstance.isProductSubscription(mockProduct));
	}

	@Test
	public void subscriptionTermShouldBeInvalidForNotValidSubscriptionProduct()
	{
		//Given
		doReturn(false).when(testInstance).isProductSubscription(eq(mockProduct));
		when(mockProduct.getSubscriptionTerms()).thenReturn(getSubscriptions());
		//When
		boolean result = testInstance.isValidSubscriptionTermForProduct(mockProduct, mockSubscriptionTerm);
		//Then
		assertFalse(result);
	}


	@Test
	public void subscriptionTermShouldBeValidIfProductHasThisTermAttached()
	{
		//Given
		doReturn(true).when(testInstance).isProductSubscription(eq(mockProduct));
		when(mockProduct.getSubscriptionTerms()).thenReturn(getSubscriptions());
		//When
		boolean result = testInstance.isValidSubscriptionTermForProduct(mockProduct, mockSubscriptionTerm);
		//Then
		assertTrue("Product should have the same subscription term in its subscriptionTerms", result);
	}

	@Test
	public void subscriptionTermShouldBeInvalidIfProductDontHaveThisTermAttached()
	{
		//Given
		doReturn(true).when(testInstance).isProductSubscription(eq(mockProduct));
		SubscriptionTermModel anotherSubscriptionTerm = givenSubscriptionTerm("another_term");
		Set<SubscriptionTermModel> set = new HashSet<>();
		set.add(anotherSubscriptionTerm);
		when(mockProduct.getSubscriptionTerms()).thenReturn(set);
		//When
		boolean result = testInstance.isValidSubscriptionTermForProduct(mockProduct, mockSubscriptionTerm);
		//Then
		assertFalse("Product subscriptionTerms should not contain requested term", result);
	}

	private void givenCustomerHasSubscriptionsForProduct(final String productCode, final String customerUid,
			final List<WileySubscriptionModel> subscriptions)
	{
		when(mockSubscriptionDao.findSubscriptionsByProductAndCustomer(productCode, customerUid)).thenReturn(subscriptions);
	}

	private void givenCustomerHasOrderEntriesForSubscriptionsProduct(final String productCode, final String customerUid,
			final List<OrderEntryModel> orderEntries)
	{
		when(mockOrderEntryDao.findOrderEntriesByProductAndOwnerExcludingStatuses(eq(productCode), eq(customerUid), anyVararg()))
				.thenReturn(orderEntries);
	}

	private SubscriptionTermModel givenSubscriptionTerm(final String id) {
		SubscriptionTermModel subscriptionTermModel = mock(SubscriptionTermModel.class);
		when(subscriptionTermModel.getId()).thenReturn(id);
		return subscriptionTermModel;
	}

	private Set<SubscriptionTermModel> getSubscriptions() {
		Set<SubscriptionTermModel> set = new HashSet<>();
		set.add(mockSubscriptionTerm);
		return set;
	}
}
