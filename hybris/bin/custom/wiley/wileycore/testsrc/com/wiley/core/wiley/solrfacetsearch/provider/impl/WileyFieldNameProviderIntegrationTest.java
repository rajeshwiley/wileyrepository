package com.wiley.core.wiley.solrfacetsearch.provider.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import static org.junit.Assert.assertEquals;

import java.util.Collections;

import javax.annotation.Resource;

import org.junit.Test;


/**
 * Unit test for {@link WileyFieldNameProvider}
 */
@IntegrationTest
public class WileyFieldNameProviderIntegrationTest extends ServicelayerTransactionalTest
{
	public static final String TESTSPECIFIER = "testspecifier";
	public static final String TESTTYPE = "testtype";
	public static final String TESTNAME = "testname";

	@Resource
	private WileyFieldNameProvider wileyFieldNameProvider;

	@Test
	public void getFieldNameForMultiValuedProperty() throws Exception
	{
		//Given
		IndexedProperty indexedProperty = createIndexedProperty();

		//When
		String actual = wileyFieldNameProvider.getFieldName(indexedProperty, TESTNAME, TESTTYPE, TESTSPECIFIER);

		//Then
		assertEquals("testname_testspecifier_testtype_mv_c", actual);
	}

	private IndexedProperty createIndexedProperty()
	{
		IndexedProperty indexedProperty = new IndexedProperty();
		indexedProperty.setMultiValue(Boolean.TRUE);
		indexedProperty.setValueRangeSets(Collections.emptyMap());
		return indexedProperty;
	}

}
