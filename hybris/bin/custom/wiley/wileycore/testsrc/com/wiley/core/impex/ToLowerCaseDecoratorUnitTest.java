package com.wiley.core.impex;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Spy;


/**
 * Test for {@link ToLowerCaseDecorator}
 */
@UnitTest
public class ToLowerCaseDecoratorUnitTest
{
	public static final String EMAIL = "CamelCaseEmailValue@.gmail.com";
	public static final String EXPECTED_NAME = EMAIL.toLowerCase();
	private static final Integer EMAIL_POSITION = Integer.valueOf(0);

	@Spy
	ToLowerCaseDecorator decorator = new ToLowerCaseDecorator();

	@Test
	public void decorateTest()
	{
		// given
		final Map<Integer, String> srcLine = new HashMap<Integer, String>();
		srcLine.put(EMAIL_POSITION, EMAIL);

		//when
		String decoratedName = decorator.decorate(EMAIL_POSITION, srcLine);

		//then
		Assert.assertEquals(EXPECTED_NAME, decoratedName);
	}
}
