package com.wiley.core.wileyb2c.search.solrfacetsearch.provider.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.RangeNameProvider;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyPurchaseOptionProductModel;
import com.wiley.core.wileyb2c.classification.Wileyb2cClassificationAttributes;
import com.wiley.core.wileyb2c.classification.Wileyb2cClassificationService;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cProductPublicationDateDurationValueProviderUnitTest
{
	public static final String TEST_RANGE = "testRange";
	@InjectMocks
	private Wileyb2cProductPublicationDateDurationValueProvider wileyb2cProductPublicationDateDurationValueProvider;
	@Mock
	private WileyPurchaseOptionProductModel wileyPurchaseOptionProductModel;
	@Mock
	private IndexConfig indexConfig;
	@Mock
	private IndexedProperty indexedProperty;
	@Mock
	private RangeNameProvider rangeNameProvider;
	@Mock
	private Wileyb2cClassificationService wileyb2cClassificationService;

	@Test
	public void collectValuesWhenNoPublicationDateShouldReturnEmptyString() throws FieldValueProviderException
	{
		when(wileyb2cClassificationService.resolveAttribute(wileyPurchaseOptionProductModel,
				Wileyb2cClassificationAttributes.PUBLICATION_DATE)).thenReturn(StringUtils.EMPTY);

		final List<String> values = wileyb2cProductPublicationDateDurationValueProvider.collectValues(indexConfig,
				indexedProperty,
				wileyPurchaseOptionProductModel);

		Assert.assertTrue(values.isEmpty());
	}

	@Test
	public void collectValuesWhenPublicationDateInPastShouldReturnEmptyString() throws FieldValueProviderException
	{
		when(wileyb2cClassificationService.resolveAttribute(wileyPurchaseOptionProductModel,
				Wileyb2cClassificationAttributes.PUBLICATION_DATE)).thenReturn("11/11/06");

		final List<String> values = wileyb2cProductPublicationDateDurationValueProvider.collectValues(indexConfig,
				indexedProperty,
				wileyPurchaseOptionProductModel);

		Assert.assertTrue(values.isEmpty());
	}

	@Test
	public void collectValuesWhenPublicationDateInFutureShouldReturnRanges() throws FieldValueProviderException
	{
		when(wileyb2cClassificationService.resolveAttribute(wileyPurchaseOptionProductModel,
				Wileyb2cClassificationAttributes.PUBLICATION_DATE)).thenReturn("11/11/06");
		when(rangeNameProvider.getRangeNameList(eq(indexedProperty), any()))
				.thenReturn(Collections.singletonList(TEST_RANGE));

		final List<String> values = wileyb2cProductPublicationDateDurationValueProvider.collectValues(indexConfig,
				indexedProperty,
				wileyPurchaseOptionProductModel);

		Assert.assertEquals(values.size(), 1);
		Assert.assertEquals(values.get(0), TEST_RANGE);
	}
}
