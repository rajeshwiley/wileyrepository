package com.wiley.core.externaltax.services.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.ServicelayerTest;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;

import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.wiley.core.externaltax.dto.TaxAddressDto;
import com.wiley.core.externaltax.dto.TaxCalculationRequestDto;
import com.wiley.core.externaltax.dto.TaxCalculationRequestEntryDto;


@IntegrationTest
public class WileyCalculateExternalTaxesStrategyImplIntegrationTest extends ServicelayerTest
{
	private static final String BASE_SITE = "wel";
	private static final String DEFAULT_ENCODING = "UTF-8";
	private static final String CART_GUID = "test-cart-guid";
	private static final String CUSTOMER_ID = "test@test.com";
	private static final String CURRENCY = "USD";
	private static final String DELIVERY_CITY = "DeliveryTown";
	private static final String DELIVERY_POSTCODE = "70000";
	private static final String DELIVERY_REGION = "AZ";
	private static final String COUNTRY = "US";
	private static final String PAYMENT_CITY = "PaymentTown";
	private static final String PAYMENT_POSTCODE = "71601";
	private static final String PAYMENT_REGION = "AL";

	private static final String ENTRY_ID_0 = "0";
	private static final String ENTRY_ID_1 = "1";
	private static final String ENTRY_ID_2 = "2";
	private static final String ENTRY_CODE_TYPE = "clusterCode";
	private static final Double AMOUNT_1 = 100.0;
	private static final Double AMOUNT_2 = 150.0;
	private static final Double AMOUNT_3 = 300.0;
	private static final String ISBN_1 = "ISBN-1";
	private static final String ISBN_2 = "ISBN-2";
	private static final String ISBN_3 = "ISBN-3";
	private static final String DIGITAL = "Digital";
	private static final String PHYSICAL = "Physical";

	@Resource
	private Converter<AbstractOrderModel, TaxCalculationRequestDto> welAgsTaxCalculationRequestDtoConverter;
	@Resource
	private BaseSiteService baseSiteService;
	@Resource
	private UserService userService;
	@Resource
	private CommerceCartService commerceCartService;
	@Resource
	private CartService cartService;

	@Before
	public void setUp() throws Exception
	{
		importCsv("/wileycore/test/externalTax/WileyCalculateExternalTaxesStrategyImplIntegrationTest/cart.impex",
				DEFAULT_ENCODING);
	}

	@Test
	public void testCalculation()
	{
		final BaseSiteModel baseSite = baseSiteService.getBaseSiteForUID(BASE_SITE);
		final UserModel userForUID = userService.getUserForUID(CUSTOMER_ID);
		final CartModel cart = commerceCartService.getCartForGuidAndSiteAndUser(CART_GUID, baseSite, userForUID);
		baseSiteService.setCurrentBaseSite(baseSite, true);
		userService.setCurrentUser(userForUID);
		cartService.setSessionCart(cart);

		final TaxCalculationRequestDto taxCalculationRequestDto = welAgsTaxCalculationRequestDtoConverter.convert(cart);
		testTaxCalculationRequestDto(taxCalculationRequestDto);
	}


	public void testTaxCalculationRequestDto(final TaxCalculationRequestDto taxCalculationRequestDto)
	{
		assertEquals(CURRENCY, taxCalculationRequestDto.getCurrency());
		assertEquals(BASE_SITE, taxCalculationRequestDto.getSiteId());
		testAddreses(taxCalculationRequestDto);
		testEntries(taxCalculationRequestDto.getEntries());

	}

	private void testAddreses(final TaxCalculationRequestDto taxCalculationRequestDto)
	{
		final TaxAddressDto deliveryAddress = taxCalculationRequestDto.getDeliveryAddress();
		final TaxAddressDto paymentAddress = taxCalculationRequestDto.getPaymentAddress();
		assertEquals(DELIVERY_CITY, deliveryAddress.getCity());
		assertEquals(DELIVERY_REGION, deliveryAddress.getState());
		assertEquals(DELIVERY_POSTCODE, deliveryAddress.getPostcode());
		assertEquals(COUNTRY, deliveryAddress.getCountry());
		assertEquals(PAYMENT_CITY, paymentAddress.getCity());
		assertEquals(PAYMENT_REGION, paymentAddress.getState());
		assertEquals(PAYMENT_POSTCODE, paymentAddress.getPostcode());
		assertEquals(COUNTRY, paymentAddress.getCountry());
	}

	private void testEntries(final List<TaxCalculationRequestEntryDto> entries)
	{
		TaxCalculationRequestEntryDto firstEntry = entries.get(0);
		assertEquals(ENTRY_ID_0, firstEntry.getId());
		assertEquals(ENTRY_CODE_TYPE, firstEntry.getCodeType());
		assertEquals(AMOUNT_1, firstEntry.getAmount());
		assertEquals(ISBN_1, firstEntry.getCode());
		assertEquals(PHYSICAL, firstEntry.getProductType());

		TaxCalculationRequestEntryDto secondEntry = entries.get(1);
		assertEquals(ENTRY_ID_1, secondEntry.getId());
		assertEquals(ENTRY_CODE_TYPE, secondEntry.getCodeType());
		assertEquals(AMOUNT_2, secondEntry.getAmount());
		assertEquals(ISBN_2, secondEntry.getCode());
		assertEquals(DIGITAL, secondEntry.getProductType());

		TaxCalculationRequestEntryDto thirdEntry = entries.get(2);
		assertEquals(ENTRY_ID_2, thirdEntry.getId());
		assertEquals(ENTRY_CODE_TYPE, thirdEntry.getCodeType());
		assertEquals(AMOUNT_3, thirdEntry.getAmount());
		assertEquals(ISBN_3, thirdEntry.getCode());
		assertEquals(PHYSICAL, thirdEntry.getProductType());
	}
}
