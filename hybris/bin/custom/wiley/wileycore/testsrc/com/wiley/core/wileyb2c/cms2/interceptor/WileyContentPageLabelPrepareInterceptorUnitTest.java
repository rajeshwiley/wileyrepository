package com.wiley.core.wileyb2c.cms2.interceptor;

import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PersistenceOperation;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class WileyContentPageLabelPrepareInterceptorUnitTest
{
	private static final String HOME_PAGE_LABEL = "home";
	private static final String CHILD_LEVEL_1 = "childLevel1";
	private static final String CHILD_LEVEL_2 = "childLevel2";
	private static final String PARENT = "parent";
	private static final String PARENT_FORMAT = "/%s";
	private static final String L1_FORMAT = "/%s/%s";
	private static final String L2_FORMAT = "/%s/%s/%s";

	@InjectMocks
	private WileyContentPageLabelPrepareInterceptor underTest;
	private Set<Object> registeredContentPages = new HashSet<>();
	@Mock
	private ContentPageModel mockHomePage;
	@Mock
	private ContentPageModel mockRootPage;
	@Mock
	private ContentPageModel mockChildLevel1;
	@Mock
	private ContentPageModel mockChildLevel2;
	@Mock
	private InterceptorContext mockInterceptorContext;

	@Before
	public void setup()
	{
		Set<ContentPageModel> childLevelOneSet = new HashSet<>();
		childLevelOneSet.add(mockChildLevel1);

		Set<ContentPageModel> childLevelTwoSet = new HashSet<>();
		childLevelOneSet.add(mockChildLevel2);

		when(mockRootPage.isHomepage()).thenReturn(false);
		when(mockChildLevel1.isHomepage()).thenReturn(false);
		when(mockChildLevel2.isHomepage()).thenReturn(false);

		when(mockRootPage.getPageLabel()).thenReturn(PARENT);
		when(mockChildLevel1.getPageLabel()).thenReturn(CHILD_LEVEL_1);
		when(mockChildLevel2.getPageLabel()).thenReturn(CHILD_LEVEL_2);

		when(mockRootPage.getChildPages()).thenReturn(childLevelOneSet);
		when(mockChildLevel1.getParentPage()).thenReturn(mockRootPage);
		when(mockChildLevel1.getChildPages()).thenReturn(childLevelTwoSet);
		when(mockChildLevel2.getParentPage()).thenReturn(mockChildLevel1);

		when(mockRootPage.getLabel()).thenReturn(String.format(PARENT_FORMAT, PARENT));
		when(mockChildLevel1.getLabel()).thenReturn(String.format(L1_FORMAT, PARENT, CHILD_LEVEL_1));
		when(mockChildLevel2.getLabel()).thenReturn(String.format(L2_FORMAT, PARENT, CHILD_LEVEL_1, CHILD_LEVEL_2));

		doAnswer(invocationOnMock -> registeredContentPages.add(invocationOnMock.getArguments()[0]))
				.when(mockInterceptorContext).registerElementFor(anyObject(), eq(PersistenceOperation.SAVE));

		doAnswer(invocationOnMock -> registeredContentPages.contains(invocationOnMock.getArguments()[0]))
				.when(mockInterceptorContext).contains(anyObject(), eq(PersistenceOperation.SAVE));

		when(mockInterceptorContext.isModified(anyObject(), anyString())).thenReturn(Boolean.TRUE);
	}

	@Test
	public void shouldUpdateLabelForAllChildren() throws InterceptorException
	{
		underTest.onPrepare(mockRootPage, mockInterceptorContext);
		verifyAllLabels();
	}

	@Test
	public void shouldHandleCyclicDependencyAndDoNotUpdateLabelAgain() throws InterceptorException
	{
		when(mockChildLevel2.getChildPages()).thenReturn(prepareRootPageSet());

		underTest.onPrepare(mockRootPage, mockInterceptorContext);
		verifyAllLabels();
	}

	@Test
	public void shouldHandleCyclicDependencyOnSamePage() throws InterceptorException
	{
		when(mockRootPage.getChildPages()).thenReturn(prepareRootPageSet());
		underTest.onPrepare(mockRootPage, mockInterceptorContext);
		verify(mockRootPage).setLabel(String.format(PARENT_FORMAT, PARENT));
	}

	@Test
	public void shouldNotCreateHierarchyLabelForHomePage() throws InterceptorException
	{
		when(mockHomePage.getPageLabel()).thenReturn(HOME_PAGE_LABEL);
		when(mockHomePage.isHomepage()).thenReturn(true);
		underTest.onPrepare(mockHomePage, mockInterceptorContext);
		verify(mockHomePage).setLabel(HOME_PAGE_LABEL);
	}

	private void verifyAllLabels()
	{
		verify(mockRootPage).setLabel(String.format(PARENT_FORMAT, PARENT));
		verify(mockChildLevel1).setLabel(String.format(L1_FORMAT, PARENT, CHILD_LEVEL_1));
		verify(mockChildLevel2).setLabel(String.format(L2_FORMAT, PARENT, CHILD_LEVEL_1, CHILD_LEVEL_2));
	}

	private Set<ContentPageModel> prepareRootPageSet()
	{
		Set<ContentPageModel> rootPageSet = new HashSet<>();
		rootPageSet.add(mockRootPage);
		return rootPageSet;
	}
}
