package com.wiley.core.wileyb2c.order.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.order.CommerceCartMergingException;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationStatus;
import de.hybris.platform.commerceservices.order.impl.DefaultCommerceAddToCartStrategy;
import de.hybris.platform.commerceservices.order.strategies.EntryMergeStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Default unit test for {@link Wileyb2cCommerceCartMergingStrategyImpl}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
@Ignore("Postponed, because Phase2 functionality is not critical path for 4.3 release")
// TODO-1808: com.wiley.core.wileyb2c.order.impl.Wileyb2cCommerceCartMergingStrategyImpl wasn't fully migrated.
// Hybris 1808 changes to Wileyb2cCommerceCartMergingStrategyImpl has to be included into test logic.
public class Wileyb2cCommerceCartMergingStrategyImplUnitTest
{

	@Mock
	private UserService userServiceMock;

	@Mock
	private BaseStoreService baseStoreServiceMock;

	@Mock
	private ModelService modelServiceMock;

	@Mock
	private BaseSiteService baseSiteServiceMock;

	@InjectMocks
	private Wileyb2cCommerceCartMergingStrategyImpl wileyb2cCommerceCartMergingStrategy;

	@Mock
	private DefaultCommerceAddToCartStrategy commerceAddToCartStrategy;

	@Mock
	private EntryMergeStrategy entryMergeStrategy;

	// Test Data
	@Mock
	private UserModel userModelMock;

	@Mock
	private CartModel fromCartMock;
	private final String fromCartGuid = "fromCartQuid";

	@Mock
	private CartModel toCartMock;
	private final String toCartGuid = "toCartQuid";

	@Mock
	private BaseSiteModel baseSiteModel1Mock;
	private final String baseSiteModel1Uid = "baseSiteUid1";

	@Mock
	private BaseSiteModel baseSiteModel2Mock;
	private final String baseSiteModel2Uid = "baseSiteUid2";

	@Mock
	private BaseStoreModel currentBaseStoreModelMock;

	@Mock
	private BaseStoreModel anotherBaseStoreModelMock;

	@Mock
	private ProductModel commonProductMock;

	@Mock
	private ProductModel productMock;

	private CommerceCartModification commonCartModificationSuccess;
	private CommerceCartModification commonCartModificationPartialSuccess;
	private CommerceCartModification commonCartModificationNoSuccess;
	private CommerceCartModification notCommonCartModification;
	private final PK pk = PK.BIG_PK;
	private CartEntryModel cartEntryModel1;
	private CartEntryModel cartEntryModel3;

	@Before
	public void setUp() throws Exception
	{
		when(Boolean.valueOf(userServiceMock.isAnonymousUser(same(userModelMock)))).thenReturn(Boolean.FALSE);
		when(baseStoreServiceMock.getCurrentBaseStore()).thenReturn(currentBaseStoreModelMock);

		when(fromCartMock.getGuid()).thenReturn(fromCartGuid);
		when(toCartMock.getGuid()).thenReturn(toCartGuid);

		when(baseSiteModel1Mock.getUid()).thenReturn(baseSiteModel1Uid);
		when(baseSiteModel2Mock.getUid()).thenReturn(baseSiteModel2Uid);

		when(userServiceMock.getCurrentUser()).thenReturn(userModelMock);

		cartEntryModel1 = spy(new CartEntryModel());
		cartEntryModel1.setEntryNumber(Integer.valueOf(0));
		cartEntryModel1.setQuantity(Long.valueOf(6));
		cartEntryModel1.setProduct(commonProductMock);

		final CartEntryModel cartEntryModel2 = new CartEntryModel();
		cartEntryModel2.setEntryNumber(Integer.valueOf(1));
		cartEntryModel2.setQuantity(Long.valueOf(9));
		cartEntryModel2.setProduct(productMock);

		final List<AbstractOrderEntryModel> entryList = new ArrayList<>();
		entryList.add(cartEntryModel1);
		entryList.add(cartEntryModel2);
		when(fromCartMock.getEntries()).thenReturn(entryList);

		cartEntryModel3 = spy(new CartEntryModel());
		cartEntryModel3.setEntryNumber(Integer.valueOf(0));
		cartEntryModel3.setQuantity(Long.valueOf(6));
		cartEntryModel3.setProduct(commonProductMock);

		final List<AbstractOrderEntryModel> entryList2 = new ArrayList<>();
		entryList2.add(cartEntryModel3);

		when(toCartMock.getEntries()).thenReturn(entryList2);
		when(baseSiteServiceMock.getCurrentBaseSite()).thenReturn(baseSiteModel1Mock);

		commonCartModificationSuccess = new CommerceCartModification();
		commonCartModificationSuccess.setStatusCode(CommerceCartModificationStatus.SUCCESS);
		commonCartModificationSuccess.setQuantityAdded(2);
		commonCartModificationSuccess.setQuantity(2);
		commonCartModificationSuccess.setEntry(fromCartMock.getEntries().get(0));

		commonCartModificationPartialSuccess = new CommerceCartModification();
		commonCartModificationPartialSuccess.setStatusCode(CommerceCartModificationStatus.NO_STOCK);
		commonCartModificationPartialSuccess.setQuantityAdded(1);
		commonCartModificationPartialSuccess.setQuantity(2);
		commonCartModificationPartialSuccess.setEntry(fromCartMock.getEntries().get(0));

		commonCartModificationNoSuccess = new CommerceCartModification();
		commonCartModificationNoSuccess.setStatusCode(CommerceCartModificationStatus.NO_STOCK);
		commonCartModificationNoSuccess.setQuantityAdded(0);
		commonCartModificationNoSuccess.setQuantity(2);
		commonCartModificationNoSuccess.setEntry(fromCartMock.getEntries().get(0));

		notCommonCartModification = new CommerceCartModification();
		notCommonCartModification.setStatusCode(CommerceCartModificationStatus.SUCCESS);
		notCommonCartModification.setQuantityAdded(3);
		notCommonCartModification.setQuantity(3);
		notCommonCartModification.setEntry(fromCartMock.getEntries().get(1));

	}

	@Test
	public void testMergeCartsWhenTwoCartsHaveDifferentSitesButSameStoreWhichEqualsCurrentStore() throws Exception
	{
		// Given
		// different sites
		when(fromCartMock.getSite()).thenReturn(baseSiteModel1Mock);
		when(toCartMock.getSite()).thenReturn(baseSiteModel2Mock);

		// same store
		when(fromCartMock.getStore()).thenReturn(currentBaseStoreModelMock);
		when(toCartMock.getStore()).thenReturn(currentBaseStoreModelMock);

		// When
		wileyb2cCommerceCartMergingStrategy.mergeCarts(fromCartMock, toCartMock, new ArrayList<>());

		// Then
		verify(modelServiceMock).save(same(toCartMock));
		verify(modelServiceMock).remove(same(fromCartMock));
	}

	@Test
	public void testMergeCartsWhenTwoCartsHaveSameSiteAndSameStoreWhichEqualsCurrentStore() throws Exception
	{
		// Given
		// same site
		when(fromCartMock.getSite()).thenReturn(baseSiteModel1Mock);
		when(toCartMock.getSite()).thenReturn(baseSiteModel1Mock);

		// same store
		when(fromCartMock.getStore()).thenReturn(currentBaseStoreModelMock);
		when(toCartMock.getStore()).thenReturn(currentBaseStoreModelMock);

		// When
		wileyb2cCommerceCartMergingStrategy.mergeCarts(fromCartMock, toCartMock, new ArrayList<>());

		// Then
		verify(modelServiceMock).save(same(toCartMock));
		verify(modelServiceMock).remove(same(fromCartMock));
	}

	@Test
	public void testMergeCartsWhenTwoCartsHaveDifferentSitesAndFirstCartHasStoreWhichNotEqualsCurrentStore()
			throws Exception
	{
		// Given
		// different sites
		when(fromCartMock.getSite()).thenReturn(baseSiteModel1Mock);
		when(toCartMock.getSite()).thenReturn(baseSiteModel2Mock);

		// same store
		when(fromCartMock.getStore()).thenReturn(anotherBaseStoreModelMock);
		when(toCartMock.getStore()).thenReturn(currentBaseStoreModelMock);

		// When
		try
		{
			wileyb2cCommerceCartMergingStrategy.mergeCarts(fromCartMock, toCartMock, new ArrayList<>());
			fail("Expected " + CommerceCartMergingException.class);
		}
		catch (final CommerceCartMergingException e)
		{
			// Then
			// Success
			verify(modelServiceMock, never()).save(same(toCartMock));
			verify(modelServiceMock, never()).remove(same(fromCartMock));
		}
	}

	@Test
	public void testMergeCartsWhenTwoCartsHaveDifferentSitesAndSecondCartHasStoreWhichNotEqualsCurrentStore()
			throws Exception
	{
		// Given
		// different sites
		when(fromCartMock.getSite()).thenReturn(baseSiteModel1Mock);
		when(toCartMock.getSite()).thenReturn(baseSiteModel2Mock);

		// same store
		when(fromCartMock.getStore()).thenReturn(currentBaseStoreModelMock);
		when(toCartMock.getStore()).thenReturn(anotherBaseStoreModelMock);

		// When
		try
		{
			wileyb2cCommerceCartMergingStrategy.mergeCarts(fromCartMock, toCartMock, new ArrayList<>());
			fail("Expected " + CommerceCartMergingException.class);
		}
		catch (final CommerceCartMergingException e)
		{
			// Then
			// Success
			verify(modelServiceMock, never()).save(same(toCartMock));
			verify(modelServiceMock, never()).remove(same(fromCartMock));
		}
	}

	@Test
	public void testMergeCartsWhenTwoCartsHaveDifferentSitesAndBothCartsHaveStoreWhichNotEqualsCurrentStore()
			throws Exception
	{
		// Given
		// different sites
		when(fromCartMock.getSite()).thenReturn(baseSiteModel1Mock);
		when(toCartMock.getSite()).thenReturn(baseSiteModel2Mock);

		// same store
		when(fromCartMock.getStore()).thenReturn(anotherBaseStoreModelMock);
		when(toCartMock.getStore()).thenReturn(anotherBaseStoreModelMock);

		// When
		try
		{
			wileyb2cCommerceCartMergingStrategy.mergeCarts(fromCartMock, toCartMock, new ArrayList<>());
			fail("Expected " + CommerceCartMergingException.class);
		}
		catch (final CommerceCartMergingException e)
		{
			// Then
			// Success
			verify(modelServiceMock, never()).save(same(toCartMock));
			verify(modelServiceMock, never()).remove(same(fromCartMock));
		}
	}

	@Test
	public void testMergeCartsForNullParameters() throws Exception
	{
		checkForParameters(null, toCartMock, new ArrayList<>());
		checkForParameters(fromCartMock, null, new ArrayList<>());
		checkForParameters(null, null, new ArrayList<>());
	}

	private void checkForParameters(final CartModel fromCart, final CartModel toCart,
			final List<CommerceCartModification> modifications) throws CommerceCartMergingException
	{
		// When
		try
		{
			wileyb2cCommerceCartMergingStrategy.mergeCarts(fromCart, toCart, modifications);
			fail("Expected " + IllegalStateException.class);
		}
		catch (final IllegalArgumentException e)
		{
			// Then
			// Success
			verify(modelServiceMock, never()).save(any());
			verify(modelServiceMock, never()).remove(any());
		}
	}

	@Test
	public void testMergeInStockModifications() throws CommerceCartMergingException
	{
		when(fromCartMock.getSite()).thenReturn(baseSiteModel1Mock);
		when(toCartMock.getSite()).thenReturn(baseSiteModel1Mock);
		when(fromCartMock.getStore()).thenReturn(currentBaseStoreModelMock);
		when(toCartMock.getStore()).thenReturn(currentBaseStoreModelMock);
		when(cartEntryModel1.getPk()).thenReturn(pk);
		when(cartEntryModel3.getPk()).thenReturn(pk);
		final CommerceCartModification cartModification = new CommerceCartModification();
		cartModification.setStatusCode(CommerceCartModificationStatus.SUCCESS);
		cartModification.setQuantityAdded(6);
		cartModification.setQuantity(6);
		cartModification.setEntry(toCartMock.getEntries().get(0));

		final List<CommerceCartModification> list = new ArrayList<>();
		list.add(cartModification);

		final List<CommerceCartParameter> parameterList = new ArrayList<>();
		parameterList.add(any(CommerceCartParameter.class));

		final List<CommerceCartModification> cartModificationList = new ArrayList<>();
		cartModificationList.add(commonCartModificationSuccess);
		cartModificationList.add(notCommonCartModification);

		when(commerceAddToCartStrategy.addToCart(parameterList)).thenReturn(cartModificationList);

		wileyb2cCommerceCartMergingStrategy.mergeCarts(fromCartMock, toCartMock, list);
		Assert.assertEquals(list.get(0).getQuantity(), 8);
		Assert.assertEquals(list.get(0).getQuantityAdded(), 8);
		Assert.assertEquals(list.get(0).getStatusCode(), CommerceCartModificationStatus.SUCCESS);
		Assert.assertEquals(list.get(1).getQuantity(), 3);
		Assert.assertEquals(list.get(1).getQuantityAdded(), 3);
		Assert.assertEquals(list.get(1).getStatusCode(), CommerceCartModificationStatus.SUCCESS);
	}

	@Test
	public void testMergePartiallyOutOfStockModifications() throws CommerceCartMergingException
	{
		when(fromCartMock.getSite()).thenReturn(baseSiteModel1Mock);
		when(toCartMock.getSite()).thenReturn(baseSiteModel1Mock);
		when(fromCartMock.getStore()).thenReturn(currentBaseStoreModelMock);
		when(toCartMock.getStore()).thenReturn(currentBaseStoreModelMock);
		when(cartEntryModel1.getPk()).thenReturn(pk);
		when(cartEntryModel3.getPk()).thenReturn(pk);
		final CommerceCartModification cartModification = new CommerceCartModification();
		cartModification.setStatusCode(CommerceCartModificationStatus.SUCCESS);
		cartModification.setQuantityAdded(6);
		cartModification.setQuantity(6);
		cartModification.setEntry(toCartMock.getEntries().get(0));

		final List<CommerceCartModification> list = new ArrayList<>();
		list.add(cartModification);

		final List<CommerceCartParameter> parameterList = new ArrayList<>();
		parameterList.add(any(CommerceCartParameter.class));

		final List<CommerceCartModification> cartModificationList = new ArrayList<>();
		cartModificationList.add(commonCartModificationPartialSuccess);
		cartModificationList.add(notCommonCartModification);

		when(commerceAddToCartStrategy.addToCart(parameterList)).thenReturn(cartModificationList);

		wileyb2cCommerceCartMergingStrategy.mergeCarts(fromCartMock, toCartMock, list);
		Assert.assertEquals(list.get(0).getQuantity(), 8);
		Assert.assertEquals(list.get(0).getQuantityAdded(), 7);
		Assert.assertEquals(list.get(0).getStatusCode(), CommerceCartModificationStatus.NO_STOCK);
		Assert.assertEquals(list.get(1).getQuantity(), 3);
		Assert.assertEquals(list.get(1).getQuantityAdded(), 3);
		Assert.assertEquals(list.get(1).getStatusCode(), CommerceCartModificationStatus.SUCCESS);
	}

	@Test
	public void testMergeOutOfStockModifications() throws CommerceCartMergingException
	{
		when(fromCartMock.getSite()).thenReturn(baseSiteModel1Mock);
		when(toCartMock.getSite()).thenReturn(baseSiteModel1Mock);
		when(fromCartMock.getStore()).thenReturn(currentBaseStoreModelMock);
		when(toCartMock.getStore()).thenReturn(currentBaseStoreModelMock);
		when(cartEntryModel1.getPk()).thenReturn(pk);
		when(cartEntryModel3.getPk()).thenReturn(pk);
		final CommerceCartModification cartModification = new CommerceCartModification();
		cartModification.setStatusCode(CommerceCartModificationStatus.NO_STOCK);
		cartModification.setQuantityAdded(0);
		cartModification.setQuantity(6);
		cartModification.setEntry(toCartMock.getEntries().get(0));

		final List<CommerceCartModification> list = new ArrayList<>();
		list.add(cartModification);

		final List<CommerceCartParameter> parameterList = new ArrayList<>();
		parameterList.add(any(CommerceCartParameter.class));

		final List<CommerceCartModification> cartModificationList = new ArrayList<>();
		cartModificationList.add(commonCartModificationNoSuccess);
		cartModificationList.add(notCommonCartModification);

		when(commerceAddToCartStrategy.addToCart(parameterList)).thenReturn(cartModificationList);

		wileyb2cCommerceCartMergingStrategy.mergeCarts(fromCartMock, toCartMock, list);
		Assert.assertEquals(list.get(0).getQuantity(), 8);
		Assert.assertEquals(list.get(0).getQuantityAdded(), 0);
		Assert.assertEquals(list.get(0).getStatusCode(), CommerceCartModificationStatus.NO_STOCK);
		Assert.assertEquals(list.get(1).getQuantity(), 3);
		Assert.assertEquals(list.get(1).getQuantityAdded(), 3);
		Assert.assertEquals(list.get(1).getStatusCode(), CommerceCartModificationStatus.SUCCESS);
	}

	@Test
	public void testMergeInStockModificationsSeparateEntries() throws CommerceCartMergingException
	{
		when(fromCartMock.getSite()).thenReturn(baseSiteModel1Mock);
		when(toCartMock.getSite()).thenReturn(baseSiteModel1Mock);
		when(fromCartMock.getStore()).thenReturn(currentBaseStoreModelMock);
		when(toCartMock.getStore()).thenReturn(currentBaseStoreModelMock);
		final CommerceCartModification cartModification = new CommerceCartModification();
		cartModification.setStatusCode(CommerceCartModificationStatus.SUCCESS);
		cartModification.setQuantityAdded(6);
		cartModification.setQuantity(6);
		cartModification.setEntry(toCartMock.getEntries().get(0));

		final List<CommerceCartModification> list = new ArrayList<>();
		list.add(cartModification);

		final List<CommerceCartParameter> parameterList = new ArrayList<>();
		parameterList.add(any(CommerceCartParameter.class));

		final List<CommerceCartModification> cartModificationList = new ArrayList<>();
		cartModificationList.add(commonCartModificationSuccess);
		cartModificationList.add(notCommonCartModification);

		when(commerceAddToCartStrategy.addToCart(parameterList)).thenReturn(cartModificationList);

		wileyb2cCommerceCartMergingStrategy.mergeCarts(fromCartMock, toCartMock, list);
		Assert.assertEquals(list.get(0).getQuantity(), 6);
		Assert.assertEquals(list.get(0).getQuantityAdded(), 6);
		Assert.assertEquals(list.get(0).getStatusCode(), CommerceCartModificationStatus.SUCCESS);
		Assert.assertEquals(list.get(1).getQuantity(), 2);
		Assert.assertEquals(list.get(1).getQuantityAdded(), 2);
		Assert.assertEquals(list.get(1).getStatusCode(), CommerceCartModificationStatus.SUCCESS);
		Assert.assertEquals(list.get(2).getQuantity(), 3);
		Assert.assertEquals(list.get(2).getQuantityAdded(), 3);
		Assert.assertEquals(list.get(2).getStatusCode(), CommerceCartModificationStatus.SUCCESS);
	}

	@Test
	public void testMergePartiallyOutOfStockModificationsSeparateEntries() throws CommerceCartMergingException
	{
		when(fromCartMock.getSite()).thenReturn(baseSiteModel1Mock);
		when(toCartMock.getSite()).thenReturn(baseSiteModel1Mock);
		when(fromCartMock.getStore()).thenReturn(currentBaseStoreModelMock);
		when(toCartMock.getStore()).thenReturn(currentBaseStoreModelMock);
		final CommerceCartModification cartModification = new CommerceCartModification();
		cartModification.setStatusCode(CommerceCartModificationStatus.SUCCESS);
		cartModification.setQuantityAdded(6);
		cartModification.setQuantity(6);
		cartModification.setEntry(toCartMock.getEntries().get(0));

		final List<CommerceCartModification> list = new ArrayList<>();
		list.add(cartModification);

		final List<CommerceCartParameter> parameterList = new ArrayList<>();
		parameterList.add(any(CommerceCartParameter.class));

		final List<CommerceCartModification> cartModificationList = new ArrayList<>();
		cartModificationList.add(commonCartModificationPartialSuccess);
		cartModificationList.add(notCommonCartModification);

		when(commerceAddToCartStrategy.addToCart(parameterList)).thenReturn(cartModificationList);

		wileyb2cCommerceCartMergingStrategy.mergeCarts(fromCartMock, toCartMock, list);
		Assert.assertEquals(list.get(0).getQuantity(), 6);
		Assert.assertEquals(list.get(0).getQuantityAdded(), 6);
		Assert.assertEquals(list.get(0).getStatusCode(), CommerceCartModificationStatus.SUCCESS);
		Assert.assertEquals(list.get(1).getQuantity(), 2);
		Assert.assertEquals(list.get(1).getQuantityAdded(), 1);
		Assert.assertEquals(list.get(1).getStatusCode(), CommerceCartModificationStatus.NO_STOCK);
		Assert.assertEquals(list.get(2).getQuantity(), 3);
		Assert.assertEquals(list.get(2).getQuantityAdded(), 3);
		Assert.assertEquals(list.get(2).getStatusCode(), CommerceCartModificationStatus.SUCCESS);
	}

	@Test
	public void testMergeOutOfStockModificationsSeparateEntries() throws CommerceCartMergingException
	{
		when(fromCartMock.getSite()).thenReturn(baseSiteModel1Mock);
		when(toCartMock.getSite()).thenReturn(baseSiteModel1Mock);
		when(fromCartMock.getStore()).thenReturn(currentBaseStoreModelMock);
		when(toCartMock.getStore()).thenReturn(currentBaseStoreModelMock);
		final CommerceCartModification cartModification = new CommerceCartModification();
		cartModification.setStatusCode(CommerceCartModificationStatus.NO_STOCK);
		cartModification.setQuantityAdded(0);
		cartModification.setQuantity(6);
		cartModification.setEntry(toCartMock.getEntries().get(0));

		final List<CommerceCartModification> list = new ArrayList<>();
		list.add(cartModification);

		final List<CommerceCartParameter> parameterList = new ArrayList<>();
		parameterList.add(any(CommerceCartParameter.class));

		final List<CommerceCartModification> cartModificationList = new ArrayList<>();
		cartModificationList.add(commonCartModificationNoSuccess);
		cartModificationList.add(notCommonCartModification);

		when(commerceAddToCartStrategy.addToCart(parameterList)).thenReturn(cartModificationList);

		wileyb2cCommerceCartMergingStrategy.mergeCarts(fromCartMock, toCartMock, list);
		Assert.assertEquals(list.get(0).getQuantity(), 6);
		Assert.assertEquals(list.get(0).getQuantityAdded(), 0);
		Assert.assertEquals(list.get(0).getStatusCode(), CommerceCartModificationStatus.NO_STOCK);
		Assert.assertEquals(list.get(1).getQuantity(), 2);
		Assert.assertEquals(list.get(1).getQuantityAdded(), 0);
		Assert.assertEquals(list.get(1).getStatusCode(), CommerceCartModificationStatus.NO_STOCK);
		Assert.assertEquals(list.get(2).getQuantity(), 3);
		Assert.assertEquals(list.get(2).getQuantityAdded(), 3);
		Assert.assertEquals(list.get(2).getStatusCode(), CommerceCartModificationStatus.SUCCESS);
	}

}