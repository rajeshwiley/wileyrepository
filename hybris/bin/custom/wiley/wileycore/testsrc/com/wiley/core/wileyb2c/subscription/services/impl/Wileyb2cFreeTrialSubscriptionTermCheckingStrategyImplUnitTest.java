package com.wiley.core.wileyb2c.subscription.services.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.subscriptionservices.model.BillingFrequencyModel;
import de.hybris.platform.subscriptionservices.model.BillingPlanModel;
import de.hybris.platform.subscriptionservices.model.SubscriptionTermModel;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cFreeTrialSubscriptionTermCheckingStrategyImplUnitTest
{
	private static final String FREE_TRIAL_BILLING_FREQUENCY_CODE = "FREE";
	private static final String SUBSCRIPTION_BILLING_FREQUENCY_CODE = "SUBSCRIPTION";

	private Wileyb2cFreeTrialSubscriptionTermCheckingStrategyImpl testInstance =
			new Wileyb2cFreeTrialSubscriptionTermCheckingStrategyImpl();

	@Mock
	private SubscriptionTermModel mockSubscriptionTerm;
	@Mock
	private BillingPlanModel mockBillingPlan;
	@Mock
	private BillingFrequencyModel mockBillingFrequency;

	@Before
	public void setUp()
	{
		when(mockSubscriptionTerm.getBillingPlan()).thenReturn(mockBillingPlan);
		when(mockBillingPlan.getBillingFrequency()).thenReturn(mockBillingFrequency);
	}

	@Test
	public void shouldReturnTrueIfBillingFrequencyCodeIsFree()
	{
		givenBillingFrequencyWithCode(FREE_TRIAL_BILLING_FREQUENCY_CODE);
		assertTrue(testInstance.isFreeTrial(mockSubscriptionTerm));
	}

	@Test
	public void shouldReturnFalseIfBillingFrequencyCodeIsNotFree()
	{
		givenBillingFrequencyWithCode(SUBSCRIPTION_BILLING_FREQUENCY_CODE);
		assertFalse(testInstance.isFreeTrial(mockSubscriptionTerm));
	}

	@Test
	public void shouldReturnFalseIfBillingPlanIsNull()
	{
		when(mockSubscriptionTerm.getBillingPlan()).thenReturn(null);
		assertFalse(testInstance.isFreeTrial(mockSubscriptionTerm));
	}

	@Test
	public void shouldReturnFalseIfBillingFrequencyIsNull()
	{
		when(mockBillingPlan.getBillingFrequency()).thenReturn(null);
		assertFalse(testInstance.isFreeTrial(mockSubscriptionTerm));
	}

	private void givenBillingFrequencyWithCode(final String billingFrequencyCode)
	{
		when(mockBillingFrequency.getCode()).thenReturn(billingFrequencyCode);
	}
}
