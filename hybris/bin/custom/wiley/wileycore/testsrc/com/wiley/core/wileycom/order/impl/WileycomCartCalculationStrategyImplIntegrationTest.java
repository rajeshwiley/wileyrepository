package com.wiley.core.wileycom.order.impl;

import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.i18n.WileyCountryService;
import com.wiley.core.order.impl.WileyCheckoutCartCalculationStrategyIntegrationTest;
import com.wiley.core.servicelayer.i18n.WileycomI18NService;
import com.wiley.core.wiley.session.storage.WileyFailedCartModificationsStorageService;


/**
 * @author Dzmitryi_Halahayeu
 */
@Ignore
public abstract class WileycomCartCalculationStrategyImplIntegrationTest
		extends WileyCheckoutCartCalculationStrategyIntegrationTest
{
	private static final String PRODUCT_CODE = "WCOM_PRODUCT";
	private static final String NON_PURCHASABLE_PRODUCT = "WCOM_NON_PURCHASABLE_PRODUCT";
	private static final String CANADA_ISO_CODE = "CA";
	private static final String US_ISO_CODE = "US";
	private static final String DEFAULT_ENCODING = "UTF-8";

	@Resource
	private ModelService modelService;
	@Resource
	private UserService userService;
	@Resource
	private CommonI18NService commonI18NService;
	@Resource
	private BaseSiteService baseSiteService;
	@Resource
	private ProductService productService;
	@Resource
	private WileyFailedCartModificationsStorageService wileyFailedCartModificationsStorageService;
	@Resource
	private WileyCountryService wileyCountryService;
	@Resource
	private WileycomI18NService wileycomI18NService;

	@Before
	public void setUp() throws Exception
	{
		importCsv("/wileycore/test/product/WileycomCartCalculationStrategyImplIntegrationTest/testProducts.impex",
				DEFAULT_ENCODING);
		baseSiteService.setCurrentBaseSite(getSiteId(), true);
		wileycomI18NService.setCurrentCountry(wileyCountryService.findCountryByCode(US_ISO_CODE));
	}


	@Test
	public void testWhenCountryRestrictionApplyShouldRemoveCartEntryAndAddFailures()
	{
		wileycomI18NService.setCurrentCountry(wileyCountryService.findCountryByCode(CANADA_ISO_CODE));
		final ProductModel productModel = productService.getProductForCode(PRODUCT_CODE);
		final CommerceCartParameter commerceCartParameter = createCartParameter(productModel);

		getCartCalculationStrategy().calculateCart(commerceCartParameter);

		assertEquals(commerceCartParameter.getCart().getEntries().size(), 0);
		final List<CommerceCartModification> modifications = wileyFailedCartModificationsStorageService.popAll();
		assertEquals(modifications.size(), 1);
		final CommerceCartModification commerceCartModification = modifications.get(0);
		assertEquals(commerceCartModification.getMessage(), WileyCoreConstants.BASKET_VALIDATION_UNVAILABLE);
		assertEquals(commerceCartModification.getMessageParameters().length, 1);
		assertEquals(commerceCartModification.getMessageParameters()[0], productModel.getName());
	}


	@Test
	public void testFailWhenAssortmentRestrictionApply() throws CommerceCartModificationException
	{
		final ProductModel b2cProduct = productService.getProductForCode(getRestrictedAssortmentProductCode());
		baseSiteService.setCurrentBaseSite(getSiteId(), true);
		final CommerceCartParameter commerceCartParameter = createCartParameter(b2cProduct);

		getCartCalculationStrategy().calculateCart(commerceCartParameter);

		assertEquals(commerceCartParameter.getCart().getEntries().size(), 0);
		final List<CommerceCartModification> modifications = wileyFailedCartModificationsStorageService.popAll();
		assertEquals(modifications.size(), 1);
		final CommerceCartModification commerceCartModification = modifications.get(0);
		assertEquals(commerceCartModification.getMessage(), WileyCoreConstants.BASKET_VALIDATION_UNVAILABLE);
		assertEquals(commerceCartModification.getMessageParameters().length, 1);
		assertEquals(commerceCartModification.getMessageParameters()[0], b2cProduct.getName());
	}

	protected abstract String getSiteId();

	protected abstract String getRestrictedAssortmentProductCode();

	@Test
	@Ignore("[ECSC-20075] ignored test which tests obsolete attribute. Should be deleted")
	public void testFailWhenNonPurchasableRestrictionApply() throws CommerceCartModificationException
	{
		final ProductModel nonPurchasableProduct = productService.getProductForCode(NON_PURCHASABLE_PRODUCT);
		final CommerceCartParameter commerceCartParameter = createCartParameter(nonPurchasableProduct);

		getCartCalculationStrategy().calculateCart(commerceCartParameter);

		assertEquals(commerceCartParameter.getCart().getEntries().size(), 0);
		final List<CommerceCartModification> modifications = wileyFailedCartModificationsStorageService.popAll();
		assertEquals(modifications.size(), 1);
		final CommerceCartModification commerceCartModification = modifications.get(0);
		assertEquals(commerceCartModification.getMessage(), WileyCoreConstants.BASKET_VALIDATION_UNVAILABLE);
		assertEquals(commerceCartModification.getMessageParameters().length, 1);
		assertEquals(commerceCartModification.getMessageParameters()[0], nonPurchasableProduct.getName());
	}


	@Test
	public void testWhenProductPurchasableHaveSameCountryAndAssortmentShouldNotRemoveCartEntryAndDoNotHaveFailures()
	{
		final ProductModel product = productService.getProductForCode(PRODUCT_CODE);
		final CommerceCartParameter commerceCartParameter = createCartParameter(product);

		getCartCalculationStrategy().calculateCart(commerceCartParameter);

		assertEquals(commerceCartParameter.getCart().getEntries().size(), 1);
		final List<CommerceCartModification> modifications = wileyFailedCartModificationsStorageService.popAll();
		assertEquals(modifications.size(), 0);
	}

	@Override
	protected String getRestrictedUserGroupProductCode()
	{
		return "WCOM_USER_GROUP_RESTRICTED";
	}
}
