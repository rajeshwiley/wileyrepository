
package com.wiley.core.wel.preorder.strategy.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.variants.model.VariantProductModel;

import org.junit.Assert;

import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.category.WileyCategoryService;
import com.wiley.core.enums.WileyProductLifecycleEnum;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WelProductLifecycleStatusStrategyImplTest
{
	@InjectMocks
	private WelProductLifecycleStatusStrategyImpl welProductLifecycleStatusStrategy;

	@Mock
	private VariantProductModel variantProduct;
	@Mock
	private ProductModel baseProduct;
	@Mock
	private WileyCategoryService wileyCategoryServiceMock;
	@Mock
	private ProductService productServiceMock;
	@Mock
	private CategoryModel primaryCFACategoryForProductMock;
	@Mock
	private CategoryModel primaryCPACategoryForProductMock;

	@Test
	public void getLifecycleStatusForBaseProduct()
	{
		//given
		when(wileyCategoryServiceMock.getPrimaryWileyCategoryForProduct(baseProduct)).thenReturn(
				primaryCPACategoryForProductMock);
		when(wileyCategoryServiceMock.isCFACategory(primaryCPACategoryForProductMock)).thenReturn(false);
		when(baseProduct.getLifecycleStatus()).thenReturn(WileyProductLifecycleEnum.ACTIVE);

		//when
		final WileyProductLifecycleEnum lifecycleStatusForProduct =
				welProductLifecycleStatusStrategy.getLifecycleStatusForProduct(baseProduct);

		//then
		Assert.assertEquals(lifecycleStatusForProduct, WileyProductLifecycleEnum.ACTIVE);
	}

	@Test
	public void getLifecycleStatusForCFAVariantProduct()
	{
		//given
		when(wileyCategoryServiceMock.getPrimaryWileyCategoryForProduct(variantProduct)).thenReturn(
				primaryCFACategoryForProductMock);
		when(wileyCategoryServiceMock.isCFACategory(primaryCFACategoryForProductMock)).thenReturn(true);
		when(variantProduct.getLifecycleStatus()).thenReturn(WileyProductLifecycleEnum.PRE_ORDER);

		//when
		final WileyProductLifecycleEnum lifecycleStatusForProduct =
				welProductLifecycleStatusStrategy.getLifecycleStatusForProduct(variantProduct);

		//then
		Assert.assertEquals(lifecycleStatusForProduct, WileyProductLifecycleEnum.PRE_ORDER);
	}

	@Test
	public void getLifecycleStatusForNotCFAVariantProduct()
	{
		//given
		when(wileyCategoryServiceMock.getPrimaryWileyCategoryForProduct(variantProduct)).thenReturn(
				primaryCPACategoryForProductMock);
		when(wileyCategoryServiceMock.isCFACategory(primaryCPACategoryForProductMock)).thenReturn(false);
		when(variantProduct.getBaseProduct()).thenReturn(baseProduct);
		when(baseProduct.getLifecycleStatus()).thenReturn(WileyProductLifecycleEnum.ACTIVE);

		//when
		final WileyProductLifecycleEnum lifecycleStatusForProduct =
				welProductLifecycleStatusStrategy.getLifecycleStatusForProduct(variantProduct);

		//then
		Assert.assertEquals(lifecycleStatusForProduct, WileyProductLifecycleEnum.ACTIVE);
	}
}
