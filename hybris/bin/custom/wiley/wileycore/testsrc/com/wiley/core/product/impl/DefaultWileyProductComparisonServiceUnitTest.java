package com.wiley.core.product.impl;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.price.CommercePriceService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.util.PriceValue;
import static org.mockito.BDDMockito.given;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.wiley.core.product.WileyProductService;
import com.wiley.core.wiley.restriction.WileyProductRestrictionService;

/**
 * Unit test for {@link DefaultWileyProductComparisonService}.
 */
public class DefaultWileyProductComparisonServiceUnitTest
{
	private static final String TEST_CURRENCY = "USD";
	private static final Integer MAX_NUMBER_OF_PRODUCTS = 2;

	private DefaultWileyProductComparisonService comparisonService;

	@Mock
	private WileyProductService productService;
	@Mock
	private CommercePriceService commercePriceService;
	@Mock
	private WileyProductRestrictionService wileyProductRestrictionService;
	@Mock
	private CategoryModel categoryModel;
	@Mock
	private ProductModel product1;
	@Mock
	private ProductModel product2;
	@Mock
	private ProductModel product3;
	@Mock
	private ProductModel product4;
	@Mock
	private PriceInformation priceInfo1;
	@Mock
	private PriceInformation priceInfo2;
	@Mock
	private PriceInformation priceInfo3;
	@Mock
	private PriceValue priceVal1;
	@Mock
	private PriceValue priceVal2;
	@Mock
	private PriceValue priceVal3;

	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);
		comparisonService = new DefaultWileyProductComparisonService();

		comparisonService.setProductService(productService);
		comparisonService.setCommercePriceService(commercePriceService);
		comparisonService.setWileyProductRestrictionService(wileyProductRestrictionService);

		given(wileyProductRestrictionService.isVisible(product1)).willReturn(true);
		given(wileyProductRestrictionService.isVisible(product2)).willReturn(true);
		given(wileyProductRestrictionService.isVisible(product3)).willReturn(true);
		given(wileyProductRestrictionService.isVisible(product4)).willReturn(false);

		given(wileyProductRestrictionService.filterRestrictedProductVariants(product1)).willReturn(product1);
		given(wileyProductRestrictionService.filterRestrictedProductVariants(product2)).willReturn(product2);
		given(wileyProductRestrictionService.filterRestrictedProductVariants(product3)).willReturn(product3);

		given(priceInfo1.getPriceValue()).willReturn(priceVal1);
		given(priceInfo2.getPriceValue()).willReturn(priceVal2);
		given(priceInfo3.getPriceValue()).willReturn(priceVal3);

		given(priceVal1.getCurrencyIso()).willReturn(TEST_CURRENCY);
		given(priceVal2.getCurrencyIso()).willReturn(TEST_CURRENCY);
		given(priceVal3.getCurrencyIso()).willReturn(TEST_CURRENCY);
	}

	@Test
	public void testProductsForComparison()
	{
		final List<ProductModel> products = new ArrayList<ProductModel>();
		products.add(product1);
		products.add(product2);
		products.add(product3);
		products.add(product4);

		given(productService.getProductsForCategoryExcludingSubcategories(categoryModel)).willReturn(products);
		given(commercePriceService.getFromPriceForProduct(product1)).willReturn(priceInfo1);
		given(commercePriceService.getFromPriceForProduct(product2)).willReturn(priceInfo2);
		given(commercePriceService.getFromPriceForProduct(product3)).willReturn(priceInfo3);

		given(Double.valueOf(priceVal1.getValue())).willReturn(new Double(5.00));
		given(Double.valueOf(priceVal2.getValue())).willReturn(new Double(10.00));
		given(Double.valueOf(priceVal3.getValue())).willReturn(new Double(8.00));

		final List<ProductModel> productsToCompare = comparisonService.getProductsForComparison(categoryModel,
				MAX_NUMBER_OF_PRODUCTS);

		Assert.assertEquals(MAX_NUMBER_OF_PRODUCTS, Integer.valueOf(productsToCompare.size()));
		Assert.assertEquals(product2, productsToCompare.get(0));
		Assert.assertEquals(product3, productsToCompare.get(1));
	}

}