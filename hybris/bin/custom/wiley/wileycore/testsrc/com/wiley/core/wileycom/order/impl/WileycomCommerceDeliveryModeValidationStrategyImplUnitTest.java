package com.wiley.core.wileycom.order.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.ExternalDeliveryModeModel;
import com.wiley.core.wileycom.order.WileycomDeliveryService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;


/**
 * Default Unit test for {@link WileycomCommerceDeliveryModeValidationStrategyImpl}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileycomCommerceDeliveryModeValidationStrategyImplUnitTest
{
	private static final String DELIVERY_MODE_1 = "deliveryMode1";
	private static final String DELIVERY_MODE_2 = "deliveryMode2";
	private static final double COST_VALUE_1 = 1.0;
	private static final double COST_VALUE_2 = 2.0;

	@Mock
	private WileycomDeliveryService wileycomDeliveryServiceMock;

	@Mock
	private ModelService modelServiceMock;


	// Test data
	@Mock
	private CartModel cartModelMock;

	@Mock
	private ExternalDeliveryModeModel externalDeliveryModeMock;

	@Mock
	private List<ExternalDeliveryModeModel> supportedDeliveryModesMock;

	@InjectMocks
	private WileycomCommerceDeliveryModeValidationStrategyImpl wileycomCommerceDeliveryModeValidationStrategy;

	@Before
	public void setUp() throws Exception
	{
		Mockito.doNothing().when(modelServiceMock).save(any());
		Mockito.doNothing().when(modelServiceMock).refresh(any());
	}

	@Test
	public void testInvalidateCartDeliveryModeWhenCurrentIsNull() throws Exception
	{
		// Given
		CartModel cartModel = new CartModel();
		when(wileycomDeliveryServiceMock.getDefaultDeliveryMode(eq(supportedDeliveryModesMock))).thenReturn(
				externalDeliveryModeMock);

		// When
		wileycomCommerceDeliveryModeValidationStrategy.invalidateCartDeliveryMode(cartModel, supportedDeliveryModesMock);

		// Than
		assertNotNull(cartModel.getDeliveryMode());
		assertEquals(externalDeliveryModeMock, cartModel.getDeliveryMode());
	}

	@Test
	public void testInvalidateCartDeliveryModeWhenCurrentIsSupported() throws Exception
	{
		// Given
		CartModel cartModel = new CartModel();

		ExternalDeliveryModeModel currentDeliveryMode = new ExternalDeliveryModeModel();
		currentDeliveryMode.setExternalCode(DELIVERY_MODE_1);
		currentDeliveryMode.setCostValue(COST_VALUE_1);

		cartModel.setDeliveryMode(currentDeliveryMode);

		ExternalDeliveryModeModel newDeliveryMode = new ExternalDeliveryModeModel();
		newDeliveryMode.setExternalCode(DELIVERY_MODE_1);
		newDeliveryMode.setCostValue(COST_VALUE_2);


		when(wileycomDeliveryServiceMock.getSupportedExternalDeliveryModeListForOrder(cartModel)).thenReturn(
				supportedDeliveryModesMock);
		when(wileycomDeliveryServiceMock.findDeliveryModeByExternalCode(DELIVERY_MODE_1, supportedDeliveryModesMock)).thenReturn(
				Optional.of(newDeliveryMode));

		// When
		wileycomCommerceDeliveryModeValidationStrategy.invalidateCartDeliveryMode(cartModel, supportedDeliveryModesMock);

		// Than
		assertNotNull(cartModel.getDeliveryMode());
		assertNotEquals(currentDeliveryMode, cartModel.getDeliveryMode());
		assertEquals(newDeliveryMode.getCostValue(), ((ExternalDeliveryModeModel) cartModel.getDeliveryMode()).getCostValue());
	}

	@Test
	public void testInvalidateCartDeliveryModeWhenCurrentIsNotSupported() throws Exception
	{
		// Given
		CartModel cartModel = new CartModel();

		ExternalDeliveryModeModel currentDeliveryMode = new ExternalDeliveryModeModel();
		currentDeliveryMode.setExternalCode(DELIVERY_MODE_2);

		cartModel.setDeliveryMode(currentDeliveryMode);

		ExternalDeliveryModeModel supportedDeliveryMode = new ExternalDeliveryModeModel();
		supportedDeliveryMode.setExternalCode(DELIVERY_MODE_1);

		when(wileycomDeliveryServiceMock.getSupportedExternalDeliveryModeListForOrder(cartModel)).thenReturn(
				supportedDeliveryModesMock);
		when(wileycomDeliveryServiceMock.findDeliveryModeByExternalCode(DELIVERY_MODE_2, supportedDeliveryModesMock)).thenReturn(
				Optional.empty());
		when(wileycomDeliveryServiceMock.getDefaultDeliveryMode(eq(supportedDeliveryModesMock))).thenReturn(
				supportedDeliveryMode);

		// When
		wileycomCommerceDeliveryModeValidationStrategy.invalidateCartDeliveryMode(cartModel, supportedDeliveryModesMock);

		// Than
		assertNotNull(cartModel.getDeliveryMode());
		assertNotEquals(currentDeliveryMode, cartModel.getDeliveryMode());
		assertEquals(supportedDeliveryMode.getCode(), cartModel.getDeliveryMode().getCode());
	}
}
