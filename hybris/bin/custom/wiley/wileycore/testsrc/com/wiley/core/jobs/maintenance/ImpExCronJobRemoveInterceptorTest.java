package com.wiley.core.jobs.maintenance;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.impex.model.ImpExMediaModel;
import de.hybris.platform.impex.model.cronjob.ImpExExportCronJobModel;
import de.hybris.platform.impex.model.cronjob.ImpExImportCronJobModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;

@IntegrationTest
public class ImpExCronJobRemoveInterceptorTest extends AbstractWileyServicelayerTransactionalTest
{
	private static final String JOB_MEDIA_QUERY_STRING =
			"SELECT {p:" + ImpExMediaModel.PK + "}" + "FROM {" + ImpExMediaModel._TYPECODE + " AS p} " + "WHERE " + "{p:"
					+ ImpExMediaModel.CODE + "}=?code ";

	private ImpExMediaModel jobMediaModel;
	private ImpExMediaModel externalJobMediaModel;
	private Collection<ImpExMediaModel> externalDataCollection;


	private ImpExImportCronJobModel importCronJobModel;
	private ImpExExportCronJobModel cronExportJobModel;

	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Resource
	private ModelService modelService;

	@Before
	public void setup()
	{
		importCronJobModel = modelService.create(ImpExImportCronJobModel.class);
		cronExportJobModel = modelService.create(ImpExExportCronJobModel.class);

		jobMediaModel = modelService.create(ImpExMediaModel.class);
		modelService.initDefaults(jobMediaModel);

		externalJobMediaModel = modelService.create(ImpExMediaModel.class);
		modelService.initDefaults(externalJobMediaModel);
		externalDataCollection = new ArrayList<ImpExMediaModel>();
		externalDataCollection.add(externalJobMediaModel);

	}

	@Test
	public void testImpExImportCronJobRemove()
	{
		// Given
		importCronJobModel.setJobMedia(jobMediaModel);
		importCronJobModel.setExternalDataCollection(externalDataCollection);
		modelService.save(importCronJobModel);

		String jobMediaCode = jobMediaModel.getCode();
		String externalJobMediaCode = externalJobMediaModel.getCode();

		final FlexibleSearchQuery query = new FlexibleSearchQuery(JOB_MEDIA_QUERY_STRING);
		query.addQueryParameter("code", jobMediaCode);

		List<ImpExMediaModel> searchResult = flexibleSearchService.<ImpExMediaModel> search(query).getResult();
		assertNotNull(searchResult.get(0));

		query.addQueryParameter("code", externalJobMediaCode);
		searchResult = flexibleSearchService.<ImpExMediaModel> search(query).getResult();
		assertNotNull(searchResult.get(0));

		// When
		modelService.remove(importCronJobModel);

		// Then
		query.addQueryParameter("code", jobMediaCode);
		searchResult = flexibleSearchService.<ImpExMediaModel> search(query).getResult();
		assertTrue(searchResult.size() == 0);

		query.addQueryParameter("code", externalJobMediaCode);
		searchResult = flexibleSearchService.<ImpExMediaModel> search(query).getResult();
		assertTrue(searchResult.size() == 0);
	}

	@Test
	public void testImpExExportCronJobRemove()
	{
		// Given
		cronExportJobModel.setJobMedia(jobMediaModel);
		modelService.save(cronExportJobModel);

		String jobMediaCode = jobMediaModel.getCode();

		final FlexibleSearchQuery query = new FlexibleSearchQuery(JOB_MEDIA_QUERY_STRING);
		query.addQueryParameter("code", jobMediaCode);

		List<ImpExMediaModel> searchResult = flexibleSearchService.<ImpExMediaModel> search(query).getResult();
		assertNotNull(searchResult.get(0));

		//When
		modelService.remove(cronExportJobModel);

		// Than
		searchResult = flexibleSearchService.<ImpExMediaModel> search(query).getResult();
		assertTrue(searchResult.size() == 0);
	}
}
