package com.wiley.core.externaltax.strategies.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.user.UserService;

import static org.junit.Assert.assertFalse;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;


@IntegrationTest
public class WileyDetermineExternalTaxStrategyIntegrationTest extends ServicelayerTransactionalTest
{
	private static final String DEFAULT_ENCODING = "UTF-8";
	private static final String ANONYMOUS = "anonymous";
	private static final String CART_CODE = "test-cart-code";

	@Resource
	private UserService userService;

	@Resource
	private WileyDetermineExternalTaxStrategy welAgsDetermineExternalTaxStrategy;

	@Resource
	private CommerceCartService commerceCartService;

	@Before
	public void setUp() throws Exception
	{
		importCsv("/wileycore/test/externalTax/WileyDetermineExternalTaxStrategyIntegrationTest/cart.impex",
				DEFAULT_ENCODING);
	}

	@Test
	public void shouldNotCalculateExternalTaxesForAnonymousUser()
	{
		UserModel user = userService.getUserForUID(ANONYMOUS);
		AbstractOrderModel cart = commerceCartService.getCartForCodeAndUser(CART_CODE, user);
		assertFalse(welAgsDetermineExternalTaxStrategy.shouldCalculateExternalTaxes(cart));
	}
}
