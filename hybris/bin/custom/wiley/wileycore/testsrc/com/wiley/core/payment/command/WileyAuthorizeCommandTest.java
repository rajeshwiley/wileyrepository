package com.wiley.core.payment.command;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.payment.data.PaymentData;
import de.hybris.platform.payment.dto.TransactionStatus;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.integration.wpg.WileyPaymentGateway;
import com.wiley.core.payment.enums.WileyTransactionStatusEnum;
import com.wiley.core.payment.request.WileySubscriptionAuthorizeRequest;
import com.wiley.core.payment.response.WileyAuthorizeResult;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyAuthorizeCommandTest
{
	private static final String AMOUNT = "10.00";
	private static final String AMOUNT_WITH_DIFFERENT_SCALE = "10.0000000";
	private static final String AMOUNT_WITH_DIFFERENT_VALUE = "10.01";
	@Mock
	private WileyPaymentGateway mockWileyPaymentGateway;

	@InjectMocks
	private WileyAuthorizeCommand underTest;

	@Mock
	private PaymentData mockPaymentData;
	@Mock
	private Map<String, String> mockTokenSettleResponse;
	@Mock
	private WileySubscriptionAuthorizeRequest mockAuthorizeCommandRequest;
	@Mock
	private WileyAuthorizeResult mockauthorizeCommandResponse;


	@Before
	public void setup()
	{
		when(mockWileyPaymentGateway.authorizePayment(mockAuthorizeCommandRequest)).thenReturn(mockauthorizeCommandResponse);

		when(mockauthorizeCommandResponse.getTotalAmount()).thenReturn(new BigDecimal(AMOUNT));
		when(mockauthorizeCommandResponse.getStatus()).thenReturn(WileyTransactionStatusEnum.SUCCESS);

		when(mockAuthorizeCommandRequest.getTotalAmount()).thenReturn(new BigDecimal(AMOUNT));


	}

	@Test
	public void shouldCallClientAndConvertResult()
	{
		WileyAuthorizeResult result = (WileyAuthorizeResult) underTest.perform(mockAuthorizeCommandRequest);
		assertSame(mockauthorizeCommandResponse, result);
	}

	@Test
	public void shouldHandleAmountMismatchForSuccessfulTransaction()
	{
		when(mockAuthorizeCommandRequest.getTotalAmount()).thenReturn(new BigDecimal(AMOUNT_WITH_DIFFERENT_VALUE));
		WileyAuthorizeResult result = (WileyAuthorizeResult) underTest.perform(mockAuthorizeCommandRequest);
		assertNotNull(result);
		assertEquals(TransactionStatus.ERROR, result.getTransactionStatus());
		assertEquals(WileyTransactionStatusEnum.AMOUNT_MISMATCH, result.getStatus());
	}

	@Test
	public void shouldNotHandleAmountMismatchForFailedTransaction()
	{
		when(mockauthorizeCommandResponse.getStatus()).thenReturn(WileyTransactionStatusEnum.DECLINED);

		when(mockAuthorizeCommandRequest.getTotalAmount()).thenReturn(new BigDecimal(AMOUNT_WITH_DIFFERENT_VALUE));
		WileyAuthorizeResult result = (WileyAuthorizeResult) underTest.perform(mockAuthorizeCommandRequest);
		assertSame(mockauthorizeCommandResponse, result);

	}

	@Test
	public void shouldTolerateAmountScaleMismatch()
	{
		when(mockAuthorizeCommandRequest.getTotalAmount()).thenReturn(new BigDecimal(AMOUNT_WITH_DIFFERENT_SCALE));
		WileyAuthorizeResult result = (WileyAuthorizeResult) underTest.perform(mockAuthorizeCommandRequest);
		assertSame(mockauthorizeCommandResponse, result);

	}

	@Test
	public void shouldHandleOtherExceptions()
	{
		doThrow(NullPointerException.class).when(mockWileyPaymentGateway).authorizePayment(mockAuthorizeCommandRequest);
		WileyAuthorizeResult result = (WileyAuthorizeResult) underTest.perform(mockAuthorizeCommandRequest);
		assertNotNull(result);
		assertEquals(TransactionStatus.ERROR, result.getTransactionStatus());
		assertEquals(WileyTransactionStatusEnum.GENERAL_SYSTEM_ERROR, result.getStatus());
	}
}
