package com.wiley.core.product.attributehandler;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyCategoryAttributeHandlerTest
{
	private WileyCategoryAttributeHandler handler;
	private static final String TEST_CATEGORY_CODE = "1234";
	private static final String TEST_CATALOG_ID = "wileyProductCatalog";

	@Rule
	public ExpectedException exception = ExpectedException.none();

	@Mock
	private CategoryService categoryService;
	@Mock
	private ProductModel productModel;
	@Mock
	private CategoryModel categoryModel;
	@Mock
	private CatalogVersionModel catalogVersionModel;
	@Mock
	private CatalogModel catalogModel;

	@Captor
	private ArgumentCaptor<Collection<CategoryModel>> superCategoriesCaptor;

	@Before
	public void setUp()
	{
		initMocks(this);
		handler = new WileyCategoryAttributeHandler();
		handler.setCategoryCode(TEST_CATEGORY_CODE);
		handler.setCategoryService(categoryService);
		handler.setSupportedProductCatalogsList(Arrays.asList(TEST_CATALOG_ID));

		when(categoryModel.getCode()).thenReturn(TEST_CATEGORY_CODE);

		//Mock catalog
		when(catalogVersionModel.getCatalog()).thenReturn(catalogModel);
		when(catalogModel.getId()).thenReturn(TEST_CATALOG_ID);

		when(productModel.getCatalogVersion()).thenReturn(catalogVersionModel);

		when(categoryService.getCategoryForCode(catalogVersionModel, TEST_CATEGORY_CODE))
				.thenReturn(categoryModel);

	}

	@Test
	public void getShouldReturnEmptyCollectionWhenCatalogVersionIsNull()
	{
		final Collection<CategoryModel> actualCategories = handler.get(productModel);
		assertThat(actualCategories).isEmpty();
	}

	@Test
	public void getShouldReturnCollectionWithSizeOneWhenCategoryExistInSuperCategoriesList()
	{
		when(productModel.getSupercategories()).thenReturn(Collections.singletonList(categoryModel));
		when(categoryService.getAllSupercategoriesForCategory(categoryModel))
				.thenReturn(Collections.singletonList(categoryModel));

		final Collection<CategoryModel> actualCategories = handler.get(productModel);
		assertThat(actualCategories).size().isEqualTo(1);
	}

	@Test
	public void getShouldReturnEmptyCollectionIfCategoryCatalogIsIncorrect()
	{
		//Given
		handler.setSupportedProductCatalogsList(Collections.emptyList());
		//When
		final Collection<CategoryModel> actualCategories = handler.get(productModel);
		//Then
		assertTrue(CollectionUtils.isEmpty(actualCategories));
	}

	@Test
	public void setCategoryIsNotSubcategoryOfInputtedOneThrowException()
	{
		final String expectedErrorMessage =
				String.format("Category %s is not a subcategory of %s", TEST_CATEGORY_CODE, TEST_CATEGORY_CODE);

		exception.expect(IllegalArgumentException.class);
		exception.expectMessage(expectedErrorMessage);

		handler.set(productModel, Collections.singleton(categoryModel));
	}

	@Test
	public void setShouldAcceptEmptyCategoriesWhenSuperCategoriesAreEmpty()
	{
		when(categoryService.getAllSupercategoriesForCategory(any(CategoryModel.class)))
				.thenReturn(Collections.singletonList(categoryModel));

		handler.set(productModel, Collections.emptyList());
		verify(productModel).setSupercategories(superCategoriesCaptor.capture());
		assertThat(superCategoriesCaptor.getValue()).isEmpty();
	}

}
