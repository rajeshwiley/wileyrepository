package com.wiley.core.wileyb2b.product;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BUnitService;
import de.hybris.platform.catalog.model.CompanyModel;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.PriceValue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.exceptions.ExternalSystemException;
import com.wiley.core.i18n.WileyCommonI18NService;
import com.wiley.core.integration.esb.EsbCartCalculationGateway;
import com.wiley.core.servicelayer.i18n.WileycomI18NService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2bPriceServiceTest
{
	@Mock
	private EsbCartCalculationGateway esbCartCalculationGateway;

	@Mock
	private UserService userService;

	@Mock
	private WileycomI18NService wileycomI18NService;

	@Mock
	private B2BUnitService b2bUnitService;

	@InjectMocks
	private Wileyb2bPriceService wileyb2bPriceService;

	@Mock
	private CustomerModel customer;

	@Mock
	private B2BCustomerModel b2bCustomer;

	@Mock
	private B2BUnitModel b2bUnit;

	@Mock
	private CompanyModel company;

	@Mock
	private CountryModel country;

	@Mock
	private CurrencyModel currency;

	@Mock
	private ProductModel product;

	@Mock
	private WileyCommonI18NService wileyCommonI18NService;

	@Before
	public void setUp()
	{
		when(esbCartCalculationGateway.getProductPriceInformations(any(ProductModel.class), anyString(), anyString(),
				anyString())).thenReturn(Collections.singletonList(new PriceInformation(new PriceValue("", 0.0, false))));

		// happy path moked here
		when(userService.getCurrentUser()).thenReturn(b2bCustomer);
		when(userService.isAnonymousUser(any(CustomerModel.class))).thenReturn(false);
		when(b2bUnitService.getParent(any(B2BCustomerModel.class))).thenReturn(b2bUnit);
		when(b2bUnit.getSapAccountNumber()).thenReturn("SAP");
		when(wileycomI18NService.getCurrentCountry()).thenReturn(Optional.of(country));
		when(country.getIsocode()).thenReturn("ISO");
		when(wileyCommonI18NService.getDefaultCurrency(country)).thenReturn(currency);
		when(currency.getIsocode()).thenReturn("ISO");
	}

	@Test
	public void checkHappyPath()
	{
		checkNotEmpty(wileyb2bPriceService.getPriceInformationsForProduct(product));
	}

	@Test
	public void testForException()
	{
		when(esbCartCalculationGateway.getProductPriceInformations(any(ProductModel.class), anyString(), anyString(),
				anyString())).thenThrow(ExternalSystemException.class);
		checkEmpty(wileyb2bPriceService.getPriceInformationsForProduct(product));
	}

	@Test
	public void testForAnonymous()
	{
		when(userService.isAnonymousUser(any(CustomerModel.class))).thenReturn(true);
		checkEmpty(wileyb2bPriceService.getPriceInformationsForProduct(product));
	}

	@Test
	public void testForSimpleCustomer()
	{
		when(userService.getCurrentUser()).thenReturn(customer);
		checkEmpty(wileyb2bPriceService.getPriceInformationsForProduct(product));
	}

	@Test
	public void testForSimpleCompany()
	{
		when(b2bUnitService.getParent(any(B2BCustomerModel.class))).thenReturn(company);
		checkEmpty(wileyb2bPriceService.getPriceInformationsForProduct(product));
	}

	@Test
	public void testForEmptySap()
	{
		when(b2bUnit.getSapAccountNumber()).thenReturn("");
		checkEmpty(wileyb2bPriceService.getPriceInformationsForProduct(product));
	}

	@Test
	public void testForEmptyCountry()
	{
		when(country.getIsocode()).thenReturn("");
		checkEmpty(wileyb2bPriceService.getPriceInformationsForProduct(product));
	}

	@Test
	public void testForNoDefaultCurrency()
	{
		when(wileyCommonI18NService.getDefaultCurrency(country)).thenReturn(null);
		checkEmpty(wileyb2bPriceService.getPriceInformationsForProduct(product));
	}

	@Test
	public void testForEmptyCurrency()
	{
		when(currency.getIsocode()).thenReturn("");
		checkEmpty(wileyb2bPriceService.getPriceInformationsForProduct(product));
	}

	private void checkEmpty(final List<PriceInformation> result)
	{
		Assert.assertTrue(result == null || result.isEmpty());
	}

	private void checkNotEmpty(final List<PriceInformation> result)
	{
		Assert.assertTrue(result != null && !result.isEmpty());
	}
}
