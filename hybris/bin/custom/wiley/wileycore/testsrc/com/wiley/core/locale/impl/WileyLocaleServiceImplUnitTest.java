package com.wiley.core.locale.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;


/**
 * Created by Uladzimir_Barouski on 6/16/2017.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyLocaleServiceImplUnitTest
{
	private static final String LOCALE_1 = "en-us";
	private static final String LOCALE_2 = "en-gb";
	private static final String COUNTRY_1 = "US";
	private static final String LANG_1 = "en";
	private static final String COUNTRY_2 = "GB";
	private static final String LANG_2 = "en";
	@Mock
	private CommonI18NService commonI18NService;
	@Mock
	private CommerceCommonI18NService commerceCommonI18NService;
	@InjectMocks
	private WileyLocaleServiceImpl wileyLocaleService;

	@Mock
	private CountryModel countryModelMock1, countryModelMock2;
	@Mock
	private LanguageModel languageModelMock1, languageModelMock2;

	@Test
	public void testAllEncodedLocales() throws Exception
	{
		//Given
		when(commerceCommonI18NService.getAllCountries()).thenReturn(Arrays.asList(countryModelMock1, countryModelMock2));
		when(countryModelMock1.getDefaultLanguage()).thenReturn(languageModelMock1);
		when(countryModelMock1.getIsocode()).thenReturn(COUNTRY_1);
		Locale locale1 = new Locale(LANG_1);
		when(commonI18NService.getLocaleForLanguage(languageModelMock1)).thenReturn(locale1);
		when(countryModelMock2.getDefaultLanguage()).thenReturn(languageModelMock2);
		when(countryModelMock2.getIsocode()).thenReturn(COUNTRY_2);
		Locale locale2 = new Locale(LANG_2);
		when(commonI18NService.getLocaleForLanguage(languageModelMock2)).thenReturn(locale2);
		//When
		List<String> locales = wileyLocaleService.getAllEncodedLocales();
		//Then
		assertNotNull(locales);
		assertThat("List equality without order", locales, containsInAnyOrder(LOCALE_1, LOCALE_2));
	}
}