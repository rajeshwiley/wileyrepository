package com.wiley.core.product.dao.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.model.WileyProductVariantSetModel;


/**
 * Integrational test for {@link DefaultWileyVariantProductDao}.
 *
 * @author Aliaksei_Zlobich
 */
@IntegrationTest
public class DefaultWileyProductVariantSetDaoIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{

	/**
	 * The constant CATALOG_ID.
	 */
	public static final String CATALOG_ID = "welProductCatalog";
	/**
	 * The constant CATALOG_VERSION.
	 */
	public static final String CATALOG_VERSION = "Online";

	@Resource
	private CatalogVersionService catalogVersionService;

	@Resource
	private UserService userService;

	@Resource
	private DefaultWileyProductVariantSetDao defaultWileyProductVariantSetDao;

	@Before
	public void setUp() throws Exception
	{
		importCsv("/wileycore/test/product/dao/DefaultWileyProductVariantSetDaoIntegrationTest/productSets.impex",
				DEFAULT_ENCODING);
		userService.setCurrentUser(userService.getAnonymousUser()); // default customer
		catalogVersionService.setSessionCatalogVersion(CATALOG_ID, CATALOG_VERSION);
	}

	@Test
	public void testFindProductSets() throws Exception
	{
		HashSet<String> codes = new HashSet<>(Arrays.asList("WEL_CPA_GOLD_REVIEW_COURSE_SET_EBOOK",
				"WEL_CPA_PLATINUM_REVIEW_COURSE_SET_EBOOK"));

		// When
		final List<WileyProductVariantSetModel> productSets = defaultWileyProductVariantSetDao.findProductSets();

		// Then
		Set<String> collect = productSets.stream().map(WileyProductVariantSetModel::getCode).collect(Collectors.toSet());
		Assert.assertTrue(collect.containsAll(codes));
	}
}