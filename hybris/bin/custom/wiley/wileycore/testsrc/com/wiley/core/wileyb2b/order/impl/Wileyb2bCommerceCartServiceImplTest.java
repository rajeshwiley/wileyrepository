package com.wiley.core.wileyb2b.order.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.impl.DefaultCalculationService;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Author Herman_Chukhrai (EPAM)
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2bCommerceCartServiceImplTest
{
	@InjectMocks
	private Wileyb2bCommerceCartServiceImpl commerceCartService;

	@Mock
	private Wileyb2bCommerceCartCalculationStrategy cartCalculationStrategy;

	@Mock
	private DefaultCalculationService calculationService;

	private CartModel cartModel;

	@Mock
	private CartEntryModel entryOne;

	@Mock
	private CartEntryModel entryTwo;

	@Before
	public void setUp() throws Exception
	{
		cartModel = new CartModel();

		cartModel.setEntries(Arrays.asList(entryOne, entryTwo));
	}

	@Test
	public void testCalculateCartForPaymentTypeChangeInvoice() throws Exception
	{
		cartModel.setPaymentType(CheckoutPaymentType.ACCOUNT);

		commerceCartService.calculateCartForPaymentTypeChange(cartModel);

		verify(entryOne, never()).setPoNumber(null);
		verify(entryTwo, never()).setPoNumber(null);
	}

	@Test
	public void testCalculateCartForPaymentTypeChangeCard() throws Exception
	{
		cartModel.setPaymentType(CheckoutPaymentType.CARD);

		commerceCartService.calculateCartForPaymentTypeChange(cartModel);

		verify(entryOne, times(1)).setPoNumber(null);
		verify(entryTwo, times(1)).setPoNumber(null);
	}
}