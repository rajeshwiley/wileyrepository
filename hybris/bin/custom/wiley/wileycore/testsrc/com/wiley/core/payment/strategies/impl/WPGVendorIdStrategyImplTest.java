package com.wiley.core.payment.strategies.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.store.WileyBaseStoreService;

import static junit.framework.Assert.assertEquals;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WPGVendorIdStrategyImplTest
{
	private static final String VENDOR_ID_KEY_TEMPLATE = "payment.wpg.soap.vendor.id.%s";
	private static final String VENDOR_ID_KEY = "payment.wpg.soap.vendor.id.agsStore";
	private static final String SITE_ID = "agsSite";
	private static final String STORE_ID = "agsStore";
	private static final Integer VENDOR_ID = 210;

	@InjectMocks
	private WPGVendorIdStrategyImpl testedInstance = new WPGVendorIdStrategyImpl();

	@Mock(answer = Answers.RETURNS_DEEP_STUBS)
	private ConfigurationService configurationService;

	@Mock
	private WileyBaseStoreService wileyBaseStoreService;

	@Before
	public void setUp()
	{
		testedInstance.setVendorIdKeyTemplate(VENDOR_ID_KEY_TEMPLATE);
		when(wileyBaseStoreService.getBaseStoreUidForSite(eq(SITE_ID))).thenReturn(STORE_ID);
	}

	@Test
	public void shouldReturnVendorId()
	{
		when(configurationService.getConfiguration().getInteger(VENDOR_ID_KEY, null)).thenReturn(VENDOR_ID);

		final Integer vendorId = testedInstance.getVendorId(SITE_ID);

		assertEquals(VENDOR_ID, vendorId);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowExceptionWhenVendorIdNotConfigured()
	{
		when(configurationService.getConfiguration().getInteger(VENDOR_ID_KEY, null)).thenReturn(null);

		testedInstance.getVendorId(SITE_ID);
	}
}