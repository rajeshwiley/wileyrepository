package com.wiley.core.refund.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderhistory.model.OrderHistoryEntryModel;
import de.hybris.platform.payment.PaymentService;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.methods.impl.DefaultCardPaymentServiceImpl;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Currency;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.mpgs.services.WileyMPGSPaymentProviderService;
import com.wiley.core.order.PaymentActonType;
import com.wiley.core.order.PendingPaymentActon;
import com.wiley.core.payment.enums.WileyTransactionStatusEnum;
import com.wiley.core.payment.request.WileyFollowOnRefundRequest;
import com.wiley.core.payment.response.WileyRefundResult;
import com.wiley.core.payment.transaction.PaymentTransactionService;
import com.wiley.core.refund.WileyRefundCheckStrategy;
import com.wiley.core.refund.WileyRefundCalculationService;
import com.wiley.core.wiley.order.WileyOrderHistoryService;
import com.wiley.core.wiley.order.WileyOrderPaymentService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyRefundServiceImplTest
{
	private static final String NEW_ENTRY_CODE = "newEntryCode";
	private static final String TRANSACTION_ENTRY_CODE = "transactionEntryCode";
	private static final String REQUEST_ID = "requestId";
	private static final String REQUEST_TOKEN = "requestToken";
	private static final String MERCHANT_RESPONSE = "merchantResponse";
	private static final BigDecimal TOTAL_REFUND_AMOUNT = BigDecimal.valueOf(65.35);
	private static final String USD = "USD";
	private static final String ORDER_HISTORY = "Refunded order";

	@Mock
	private PaymentService paymentService;
	@Mock
	private DefaultCardPaymentServiceImpl cardPaymentService;
	@Mock
	private CommonI18NService commonI18NService;
	@Mock
	private ModelService modelService;
	@Mock
	private PaymentTransactionService transactionService;
	@Mock
	private WileyRefundCalculationService refundCalculationService;
	@Mock
	private EventService eventService;
	@Mock
	private WileyRefundCheckStrategy fullRefundCheckStrategy;

	@InjectMocks
	private WileyRefundServiceImpl refundService = new WileyRefundServiceImpl();

	@Mock
	private OrderModel order;
	@Mock
	private PaymentTransactionModel transaction;
	@Mock
	private PaymentTransactionEntryModel authTransactionEntry;
	@Mock
	private PaymentTransactionEntryModel captureTransactionEntry;
	@Mock
	private CurrencyModel currency;
	@Mock
	private BaseSiteModel site;
	private Date requestTime = new Date();
	@Mock
	private PaymentTransactionEntryModel followOnRefundTransaction;
	@Mock
	private WileyOrderPaymentService wileyOrderPaymentService;
	@Mock
	private WileyMPGSPaymentProviderService wileyMPGSPaymentProviderService;
	@Mock
	private WileyOrderHistoryService wileyOrderHistoryServiceMock;

	private OrderHistoryEntryModel testHistoryEntry = new OrderHistoryEntryModel();

	@Before
	public void setUp()
	{
		setUpMockOrder();
		setUpMockTransactions();
		when(paymentService.getNewPaymentTransactionEntryCode(transaction, PaymentTransactionType.REFUND_FOLLOW_ON))
				.thenReturn(NEW_ENTRY_CODE);
		when(cardPaymentService.refundFollowOn(any(WileyFollowOnRefundRequest.class)))
				.thenReturn(createSuccessfulWileyRefundResult());
		when(commonI18NService.getCurrency(USD)).thenReturn(currency);

		when(captureTransactionEntry.getAmount()).thenReturn(TOTAL_REFUND_AMOUNT);
		givenTransactionEntryForOrder(order, captureTransactionEntry, PaymentTransactionType.CAPTURE);

		when(captureTransactionEntry.getPaymentTransaction()).thenReturn(transaction);
		when(captureTransactionEntry.getRequestId()).thenReturn(REQUEST_ID);

		when(followOnRefundTransaction.getCode()).thenReturn(TRANSACTION_ENTRY_CODE);
		when(refundCalculationService.calculateMaximumAvailableRefundAmountForOrder(order)).thenReturn(TOTAL_REFUND_AMOUNT);
		when(wileyOrderHistoryServiceMock.createHistorySnapshot(order)).thenReturn(order);
		when(modelService.create(OrderHistoryEntryModel.class)).thenReturn(testHistoryEntry);
	}

	@Test
	public void shouldInvokeFollowOnRefundCommand()
	{
		PaymentTransactionEntryModel result = refundService.processRefundPaymentTransaction(order, TOTAL_REFUND_AMOUNT);
		assertEquals(followOnRefundTransaction, result);
		verify(cardPaymentService).refundFollowOn(any(WileyFollowOnRefundRequest.class));
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldReturnNullWhenAuthorizationRejected()
	{
		when(transactionService.getAcceptedTransactionEntry(transaction, PaymentTransactionType.AUTHORIZATION))
				.thenReturn(null);

		refundService.processRefundPaymentTransaction(order, TOTAL_REFUND_AMOUNT);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowIllegalArgumentExceptionExceptionWhenRefundAmountIsNull()
	{
		refundService.processRefundPaymentTransaction(order, null);
	}

	@Test
	public void shouldSaveRefundTransaction()
	{
		refundService.processRefundPaymentTransaction(order, TOTAL_REFUND_AMOUNT);

		verify(followOnRefundTransaction).setAmount(TOTAL_REFUND_AMOUNT);
		verify(followOnRefundTransaction).setCurrency(currency);
		verify(followOnRefundTransaction).setType(PaymentTransactionType.REFUND_FOLLOW_ON);
		verify(followOnRefundTransaction).setRequestId(REQUEST_ID);
		verify(followOnRefundTransaction).setRequestToken(REQUEST_TOKEN);
		verify(followOnRefundTransaction).setTime(requestTime);
		verify(followOnRefundTransaction).setPaymentTransaction(transaction);
		verify(followOnRefundTransaction).setTransactionStatus(TransactionStatus.ACCEPTED.toString());
		verify(followOnRefundTransaction).setCode(NEW_ENTRY_CODE);
		verify(followOnRefundTransaction).setWpgMerchantResponse(MERCHANT_RESPONSE);
		verify(followOnRefundTransaction).setTransactionStatusDetails(WileyTransactionStatusEnum.SUCCESS.getDescription());
		verify(modelService).saveAll(followOnRefundTransaction, transaction);
	}


	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowIllegalArgumentExceptionIfRefundAmountIsZero()
	{
		//Testing
		refundService.processRefundPaymentTransaction(order, BigDecimal.ZERO);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowIllegalArgumentExceptionIfRefundAmountIsBiggerThanMaxAllowed()
	{
		final BigDecimal tooHighRefundAmount = TOTAL_REFUND_AMOUNT.add(BigDecimal.ONE);
		//Testing
		refundService.processRefundPaymentTransaction(order, tooHighRefundAmount);
	}
	
	@Test
	public void shouldExecuteRefundForRefundAction()
	{
		List<PendingPaymentActon> paymentActions = new ArrayList<>();
		paymentActions.add(createPendingPaymentAction(PaymentActonType.REFUND));
		paymentActions.add(createPendingPaymentAction(PaymentActonType.CAPTURE));
		when(wileyOrderPaymentService.getPendingPaymentActions(order)).thenReturn(paymentActions);

		refundService.processRefundForModifiedOrder(order);
		verify(cardPaymentService).refundFollowOn(any());
	}

	private PendingPaymentActon createPendingPaymentAction(final PaymentActonType actionType)
	{
		PendingPaymentActon paymentAction = new PendingPaymentActon();
		paymentAction.setAction(actionType);
		paymentAction.setAmount(10.0D);
		paymentAction.setTransaction(transaction);
		return paymentAction;
	}

	private void setUpMockOrder()
	{
		when(order.getCurrency()).thenReturn(currency);
		when(order.getSite()).thenReturn(site);
		when(currency.getIsocode()).thenReturn(USD);
		when(order.getPaymentTransactions()).thenReturn(Collections.singletonList(transaction));
		when(fullRefundCheckStrategy.isFullyRefundedOrder(order)).thenReturn(true);
	}

	private void setUpMockTransactions()
	{
		when(transactionService.getAcceptedTransactionEntry(transaction, PaymentTransactionType.AUTHORIZATION))
				.thenReturn(authTransactionEntry);
		when(transactionService.getAcceptedTransactionEntry(transaction, PaymentTransactionType.CAPTURE))
				.thenReturn(captureTransactionEntry);
		when(modelService.create(PaymentTransactionEntryModel.class)).thenReturn(followOnRefundTransaction);
	}



	private WileyRefundResult createSuccessfulWileyRefundResult()
	{
		WileyRefundResult wileyRefundResult = new WileyRefundResult();
		wileyRefundResult.setTotalAmount(TOTAL_REFUND_AMOUNT);
		wileyRefundResult.setCurrency(Currency.getInstance(USD));
		wileyRefundResult.setRequestId(REQUEST_ID);
		wileyRefundResult.setRequestToken(REQUEST_TOKEN);
		wileyRefundResult.setRequestTime(requestTime);
		wileyRefundResult.setMerchantResponse(MERCHANT_RESPONSE);
		wileyRefundResult.setTransactionStatus(TransactionStatus.ACCEPTED);
		wileyRefundResult.setWpgTransactionStatus(WileyTransactionStatusEnum.SUCCESS);
		return wileyRefundResult;
	}

	private void givenTransactionEntryForOrder(
			final OrderModel order,
			final PaymentTransactionEntryModel transactionEntry,
			final PaymentTransactionType transactionType
	)
	{
		when(transactionService.getAcceptedTransactionEntry(order, transactionType))
				.thenReturn(transactionEntry);
	}

}
