package com.wiley.core.pages.dao.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.servicelayer.services.impl.DefaultCMSComponentService;
import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.pages.dao.WileyCMSPageDao;


/**
 * Created by Uladzimir_Barouski on 4/27/2017.
 */
@IntegrationTest
public class WileyCMSPageDaoImplIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	private static final String COMPONENT_ON_PAGE = "testTopParagraphComponent";
	private static final String CATALOG_ID = "wileyb2cContentCatalog";
	private static final String CATALOG_VERSION = "Online";
	private static final String COMPONENT_WITHOUT_PAGE = "testlink";
	@Resource
	private DefaultCMSComponentService cmsComponentService;
	@Resource
	private WileyCMSPageDao wileyCMSPageDao;
	@Resource
	private CatalogVersionService catalogVersionService;

	@Before
	public void setUp() throws Exception
	{
		importCsv("/wileycore/test/pages/dao/WileyCMSPageDaoImplIntegrationTest.impex",
				DEFAULT_ENCODING);
		catalogVersionService.setSessionCatalogVersion(CATALOG_ID, CATALOG_VERSION);
	}

	@Test
	public void testFindPagesWhenComponentOnPage() throws Exception
	{
		//Given
		AbstractCMSComponentModel abstractCMSComponentModel = cmsComponentService.getAbstractCMSComponent(COMPONENT_ON_PAGE);

		//When
		List pages = wileyCMSPageDao.findPagesByComponent(abstractCMSComponentModel);
		//Then
		assertEquals(1, pages.size());
		assertEquals("searchEmpty", ((AbstractPageModel) pages.get(0)).getUid());
	}

	@Test
	public void testFindPagesWhenComponentNotOnPage() throws Exception
	{
		//Given
		AbstractCMSComponentModel abstractCMSComponentModel = cmsComponentService.getAbstractCMSComponent(COMPONENT_WITHOUT_PAGE);

		//When
		List pages = wileyCMSPageDao.findPagesByComponent(abstractCMSComponentModel);
		//Then
		assertEquals(0, pages.size());
	}
}