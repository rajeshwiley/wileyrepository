package com.wiley.core.product.interceptors;

import com.wiley.core.enums.ProductEditionFormat;
import com.wiley.core.model.WileyPurchaseOptionProductModel;
import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.MockitoAnnotations.initMocks;

public class WileyPurchaseOptionProductCountablePrepareInterceptorTest
{
    private static final String WILEY_PRODUCT_CATALOG_ID = "wileyProductCatalog";
    private static final String DUMMY_CATALOG_ID = "dummyCatalogId";

    @Mock
    private InterceptorContext interceptorContext;

    @Mock
    private CatalogVersionModel catalogVersionModel;

    @Mock
    private CatalogModel catalogModel;

    private WileyPurchaseOptionProductModel wileyPurchaseOptionProductModel;

    @InjectMocks
    private WileyPurchaseOptionProductCountablePrepareInterceptor interceptor =
            new WileyPurchaseOptionProductCountablePrepareInterceptor();

    @Before
    public void setUp()
    {
        initMocks(this);
        wileyPurchaseOptionProductModel = new WileyPurchaseOptionProductModel();
        wileyPurchaseOptionProductModel.setCatalogVersion(catalogVersionModel);
    }

    @Test
    public void shouldSetCountableIfProductCatalogIdIsCorrect()
    {
        //given
        wileyPurchaseOptionProductModel.setEditionFormat(ProductEditionFormat.PHYSICAL);
        given(catalogVersionModel.getCatalog()).willReturn(catalogModel);
        given(catalogModel.getId()).willReturn(WILEY_PRODUCT_CATALOG_ID);

        //when
        interceptor.onPrepare(wileyPurchaseOptionProductModel, interceptorContext);

        //then
        assertThat(wileyPurchaseOptionProductModel.getCountable()).isTrue();
    }

    @Test
    public void shouldSetCountableToFalseIfProductCatalogIdIsCorrectAndEditionFormatIsNotPhysical()
    {
        //given
        wileyPurchaseOptionProductModel.setEditionFormat(ProductEditionFormat.DIGITAL);
        given(catalogVersionModel.getCatalog()).willReturn(catalogModel);
        given(catalogModel.getId()).willReturn(WILEY_PRODUCT_CATALOG_ID);

        //when
        interceptor.onPrepare(wileyPurchaseOptionProductModel, interceptorContext);

        //then
        assertThat(wileyPurchaseOptionProductModel.getCountable()).isFalse();
    }

    @Test
    public void shouldNotSetCountableIfProductCatalogIdIsNotCorrect()
    {
        //given
        given(catalogVersionModel.getCatalog()).willReturn(catalogModel);
        given(catalogModel.getId()).willReturn(DUMMY_CATALOG_ID);

        //when
        interceptor.onPrepare(wileyPurchaseOptionProductModel, interceptorContext);

        //then
        assertThat(wileyPurchaseOptionProductModel.getCountable()).isNull();
    }
}