package com.wiley.core.media.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.media.MediaModel;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;


/**
 * Created by Raman_Hancharou on 11/9/2015.
 */
@UnitTest
public class DefaultWileyMediaServiceUnitTest
{

	private DefaultWileyMediaService mediaService;

	/**
	 * The Media model.
	 */
	@Mock
	MediaModel mediaModel;

	/**
	 * Sets up.
	 *
	 * @throws Exception
	 * 		the exception
	 */
	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);
		mediaService = new DefaultWileyMediaService();
	}

	/**
	 * Test get file name without extension.
	 */
	@Test
	public void testGetFileNameWithoutExtension()
	{
		// Given
		String expected = "test";
		when(mediaModel.getRealFileName()).thenReturn("test.pdf");

		// When
		String actual = mediaService.getFileNameWithoutExtension(mediaModel);

		// Then
		assertEquals(expected, actual);
	}

	/**
	 * Test get file name without extension when extension is empty.
	 */
	@Test
	public void testGetFileNameWithoutExtensionWhenExtensionIsEmpty()
	{
		// Given
		String expected = "test";
		when(mediaModel.getRealFileName()).thenReturn("test");

		// When
		String actual = mediaService.getFileNameWithoutExtension(mediaModel);

		// Then
		assertEquals(expected, actual);
	}

	/**
	 * Test get file name without extension when file name is null.
	 */
	@Test
	public void testGetFileNameWithoutExtensionWhenFileNameIsNull()
	{
		// Given
		when(mediaModel.getRealFileName()).thenReturn(null);

		// When
		String actual = mediaService.getFileNameWithoutExtension(mediaModel);

		// Then
		assertNull(actual);
	}
}
