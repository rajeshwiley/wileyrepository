package com.wiley.core.wileycom.customer.service.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.customer.PasswordMismatchException;
import de.hybris.platform.commerceservices.customer.TokenInvalidatedException;
import de.hybris.platform.commerceservices.security.SecureToken;
import de.hybris.platform.commerceservices.security.SecureTokenService;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.jalo.user.Customer;
import de.hybris.platform.jalo.user.PasswordCheckingStrategy;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.services.BaseStoreService;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.customer.strategy.WileyCustomerRegistrationStrategy;
import com.wiley.core.exceptions.ExternalSystemException;
import com.wiley.core.integration.customer.gateway.WileyCustomerGateway;
import com.wiley.core.wileycom.exceptions.PasswordUpdateException;
import com.wiley.core.wileycom.users.service.WileycomUsersService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileycomCustomerAccountServiceImplUnitTest
{
	private static final String PASSWORD = "1234";
	private static final String USERID = "userid";
	private static final String TOKEN = "user-token";
	private static final String TOKEN_DATA = "token-data";
	private static final String OLD_PASSWORD = "1O231O2I3";
	private static final String NEW_PASSWORD = "sfwefwklfle";

	@InjectMocks
	private WileycomCustomerAccountServiceImpl objectUnderTest;

	@Mock
	private CustomerModel customerModelMock;

	@Mock
	private WileyCustomerRegistrationStrategy wileyCustomerRegistrationStrategyMock;

	@Mock
	private UserService userServiceMock;

	@Mock
	private ModelService modelServiceMock;

	@Mock
	private BaseStoreService baseStoreServiceMock;

	@Mock
	private BaseSiteService baseSiteServiceMock;

	@Mock
	private CommonI18NService commonI18NServiceMock;

	@Mock
	private PasswordCheckingStrategy wileyPasswordCheckingStrategy;

	@Mock
	private EventService eventServiceMock;

	@Mock
	private WileyCustomerGateway wileyCustomerGatewayMock;

	@Mock
	private WileycomUsersService wileycomUsersService;

	@Mock
	private SecureTokenService secureTokenServiceMock;

	@Mock
	private Customer customerJaloMock;

	@Test
	public void shouldPrepareCustomerModelForRegistrationDuringRegistration()
	{
		// When
		try
		{
			objectUnderTest.register(customerModelMock, PASSWORD);
		}
		catch (DuplicateUidException exception)
		{
			fail("Should not have not catched DuplicateUidException.");
		}

		// Then
		verify(customerModelMock).setCustomerID(any(String.class));
	}

	@Test
	public void shouldRegisterCustomerInExternalWileycomSystemsDuringRegistration()
	{
		// When
		try
		{
			objectUnderTest.register(customerModelMock, PASSWORD);
		}
		catch (DuplicateUidException exception)
		{
			fail("Should not have not catched DuplicateUidException.");
		}

		// Then
		verify(wileyCustomerRegistrationStrategyMock).register(customerModelMock, PASSWORD);
	}

	@Test
	public void shouldRegisterCustomerInLocalHybrisSystemDuringRegistration()
	{
		// When
		try
		{
			objectUnderTest.register(customerModelMock, PASSWORD);
		}
		catch (DuplicateUidException exception)
		{
			fail("Should not have not catched DuplicateUidException.");
		}

		// Then
		verify(modelServiceMock).save(customerModelMock);
	}

	@Test
	public void externalResetCustomerPasswordShouldFollowHappyPath()
	{
		// When
		objectUnderTest.externalResetCustomerPassword(USERID, PASSWORD);

		// Then
		verify(wileyCustomerGatewayMock).forceResetPassword(USERID, PASSWORD);
	}

	@Test(expected = PasswordUpdateException.class)
	public void externalResetCustomerPasswordShouldThrowPasswordUpdateExceptionInCaseOfExternalSystemFailure()
	{
		// Given
		doThrow(ExternalSystemException.class).when(wileyCustomerGatewayMock).forceResetPassword(USERID, PASSWORD);

		// When
		objectUnderTest.externalResetCustomerPassword(USERID, PASSWORD);

		// Then
		fail("PasswordUpdateException exception should have been thrown in case of external system failure!");
	}

	@Test(expected = IllegalArgumentException.class)
	public void externalResetCustomerPasswordShouldThrowIllegalArgumentExceptionForEmptyUser()
	{
		// When
		objectUnderTest.externalResetCustomerPassword(null, PASSWORD);

		// Then
		fail("IllegalArgumentException exception should have been thrown for null user id!");
	}

	@Test(expected = IllegalArgumentException.class)
	public void externalResetCustomerPasswordShouldThrowIllegalArgumentExceptionForEmptyPassword()
	{
		// When
		objectUnderTest.externalResetCustomerPassword(USERID, null);

		// Then
		fail("IllegalArgumentException exception should have been thrown for null password!");
	}

	@Test
	public void updatePasswordShouldFollowHappyCase() throws TokenInvalidatedException
	{
		// Given
		objectUnderTest.setTokenValiditySeconds(1000);
		SecureToken secureToken = new SecureToken(TOKEN_DATA, new Date().getTime());

		when(secureTokenServiceMock.decryptData(TOKEN)).thenReturn(secureToken);
		when(userServiceMock.getUserForUID(TOKEN_DATA, CustomerModel.class)).thenReturn(customerModelMock);
		when(customerModelMock.getToken()).thenReturn(TOKEN);
		when(customerModelMock.getUid()).thenReturn(USERID);

		// When
		objectUnderTest.updatePassword(TOKEN, PASSWORD);

		// Then
		verify(wileyCustomerGatewayMock).forceResetPassword(USERID, PASSWORD);
	}

	@Test
	public void shouldChangePassword() throws PasswordMismatchException
	{
		//Given
		when(userServiceMock.isAnonymousUser(customerModelMock)).thenReturn(false);
		when(modelServiceMock.toPersistenceLayer(customerModelMock)).thenReturn(customerJaloMock);
		when(wileyPasswordCheckingStrategy.checkPassword(customerJaloMock, OLD_PASSWORD)).thenReturn(true);

		// When
		objectUnderTest.changePassword(customerModelMock, OLD_PASSWORD, NEW_PASSWORD);

		// Then
		verify(wileycomUsersService).updatePassword(customerModelMock, OLD_PASSWORD, NEW_PASSWORD);
	}

	@Test
	public void shouldNotChangePasswordForAnonymousUser() throws PasswordMismatchException
	{
		//Given
		when(userServiceMock.isAnonymousUser(customerModelMock)).thenReturn(true);

		// When
		objectUnderTest.changePassword(customerModelMock, OLD_PASSWORD, NEW_PASSWORD);

		// Then
		verifyZeroInteractions(wileycomUsersService, modelServiceMock, wileyPasswordCheckingStrategy);
	}

	@Test
	public void shouldNotUpdatePasswordInExternalSystemIfPasswordCheckFailed()
	{
		//Given
		when(userServiceMock.isAnonymousUser(customerModelMock)).thenReturn(false);
		when(modelServiceMock.toPersistenceLayer(customerModelMock)).thenReturn(customerJaloMock);
		when(wileyPasswordCheckingStrategy.checkPassword(customerJaloMock, OLD_PASSWORD)).thenReturn(false);

		//When
		try
		{
			objectUnderTest.changePassword(customerModelMock, OLD_PASSWORD, NEW_PASSWORD);
			fail("PasswordMismatchException should be thrown if password check has failed");
		}
		catch (PasswordMismatchException e)
		{
			// This exception is expected
		}

		//Then
		verifyZeroInteractions(wileycomUsersService);
	}
}
