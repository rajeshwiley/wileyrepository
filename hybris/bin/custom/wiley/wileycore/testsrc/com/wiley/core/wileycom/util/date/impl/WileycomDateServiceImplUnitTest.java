package com.wiley.core.wileycom.util.date.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.time.TimeService;

import java.util.Calendar;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import junit.framework.Assert;
import static org.mockito.Mockito.when;



/**
 * Created by Georgii_Gavrysh on 6/21/2016.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileycomDateServiceImplUnitTest
{
	@Mock
	TimeService timeService;

	@InjectMocks
	WileycomDateServiceImpl wileyDateServiceImpl;

	@Test
	public void shouldReturnOneForUpcomingMorning()
	{
		final Calendar todayCalendar = Calendar.getInstance();
		todayCalendar.set(2016, 5, 21, 23, 59, 59);
		final Date today = todayCalendar.getTime();
		when(timeService.getCurrentTime()).thenReturn(today);

		final Calendar testedCalendar = Calendar.getInstance();
		testedCalendar.set(2016, 5, 22, 00, 59, 59);
		final Date date = testedCalendar.getTime();

		Assert.assertEquals(wileyDateServiceImpl.daysNumberTillDate(date), 1);

	}

	@Test
	public void shouldReturnZeroForToday()
	{
		final Calendar todayCalendar = Calendar.getInstance();
		todayCalendar.set(2016, 5, 21, 22, 59, 59);
		final Date today = todayCalendar.getTime();
		when(timeService.getCurrentTime()).thenReturn(today);

		final Calendar testedCalendar = Calendar.getInstance();
		testedCalendar.set(2016, 5, 21, 23, 59, 59);
		final Date date = testedCalendar.getTime();

		Assert.assertEquals(wileyDateServiceImpl.daysNumberTillDate(date), 0);

	}

	@Test
	public void shouldReturnOneForNextEvening()
	{
		final Calendar todayCalendar = Calendar.getInstance();
		todayCalendar.set(2016, 5, 21, 00, 59, 59);
		final Date today = todayCalendar.getTime();
		when(timeService.getCurrentTime()).thenReturn(today);

		final Calendar testedCalendar = Calendar.getInstance();
		testedCalendar.set(2016, 5, 22, 23, 59, 59);
		final Date date = testedCalendar.getTime();

		Assert.assertEquals(wileyDateServiceImpl.daysNumberTillDate(date), 1);

	}

	@Test
	public void shouldReturnMinusOneForYesterday()
	{
		final Calendar todayCalendar = Calendar.getInstance();
		todayCalendar.set(2016, 5, 21, 00, 59, 59);
		final Date today = todayCalendar.getTime();
		when(timeService.getCurrentTime()).thenReturn(today);

		final Calendar testedCalendar = Calendar.getInstance();
		testedCalendar.set(2016, 5, 20, 23, 59, 59);
		final Date date = testedCalendar.getTime();

		Assert.assertEquals(wileyDateServiceImpl.daysNumberTillDate(date), -1);

	}

}
