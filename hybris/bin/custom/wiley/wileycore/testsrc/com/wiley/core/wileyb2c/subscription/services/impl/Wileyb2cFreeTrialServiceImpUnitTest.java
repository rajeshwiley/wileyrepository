package com.wiley.core.wileyb2c.subscription.services.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.subscriptionservices.model.SubscriptionTermModel;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyProductModel;
import com.wiley.core.wileyb2c.subscription.services.Wileyb2cFreeTrialSubscriptionTermCheckingStrategy;
import com.wiley.core.wileyb2c.subscription.services.Wileyb2cSubscriptionService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cFreeTrialServiceImpUnitTest
{
	@InjectMocks
	private Wileyb2cFreeTrialServiceImpl testInstance = new Wileyb2cFreeTrialServiceImpl();

	@Mock
	private Wileyb2cFreeTrialSubscriptionTermCheckingStrategy mockFreeTrialSubscriptionTermCheckingStrategy;
	@Mock
	private Wileyb2cSubscriptionService mockSubscriptionService;
	@Mock
	private WileyProductModel mockProduct;
	@Mock
	private SubscriptionTermModel mockSubscriptionTerm;

	@Test
	public void shouldReturnTrueIfProductIsSubscriptionAndHasFreeTrialTerm()
	{
		Set<SubscriptionTermModel> set = new HashSet<>();
		set.add(mockSubscriptionTerm);
		givenProductIsSubscription(true);
		givenProductHasSubscriptionTerms(set);
		givenSubscriptionTermIsFreeTrial(true);
		assertTrue(testInstance.isProductFreeTrial(mockProduct));
	}

	@Test
	public void shouldReturnFalseIfProductDoesNotHaveFreeTrialTerm()
	{
		Set<SubscriptionTermModel> set = new HashSet<>();
		set.add(mockSubscriptionTerm);
		givenProductIsSubscription(true);
		givenProductHasSubscriptionTerms(set);
		givenSubscriptionTermIsFreeTrial(false);
		assertFalse(testInstance.isProductFreeTrial(mockProduct));
	}

	@Test
	public void shouldReturnFalseIfProductDoesNotHaveSubscriptionTerms()
	{
		givenProductIsSubscription(true);
		givenProductHasSubscriptionTerms(Collections.EMPTY_SET);
		assertFalse(testInstance.isProductFreeTrial(mockProduct));
	}

	@Test
	public void shouldReturnFalseIfProductIsNotSubscription()
	{
		givenProductIsSubscription(false);
		assertFalse(testInstance.isProductFreeTrial(mockProduct));
	}

	private void givenProductIsSubscription(boolean isSubscription)
	{
		when(mockSubscriptionService.isProductSubscription(mockProduct)).thenReturn(isSubscription);
	}

	private void givenSubscriptionTermIsFreeTrial(boolean isFreeTrial)
	{
		when(mockFreeTrialSubscriptionTermCheckingStrategy.isFreeTrial(mockSubscriptionTerm)).thenReturn(isFreeTrial);
	}

	private void givenProductHasSubscriptionTerms(final Set<SubscriptionTermModel> subscriptionTerms)
	{
		when(mockProduct.getSubscriptionTerms()).thenReturn(subscriptionTerms);
	}

}
