package com.wiley.core.payment.command;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.payment.dto.TransactionStatus;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Currency;

import javax.xml.ws.WebServiceException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.integration.wpg.WileyPaymentGateway;
import com.wiley.core.payment.enums.WileyTransactionStatusEnum;
import com.wiley.core.payment.request.WileyFollowOnRefundRequest;
import com.wiley.core.payment.response.WileyRefundResult;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyFollowOnRefundCommandTest
{
	private static final String REQUEST_TOKEN = "requestToken";
	private static final String USD = "USD";
	private static final String PAYMENT_PROVIDER = "paymentProvider";
	private static final String SITE_ID = "siteId";
	private static final String ORDER_CODE = "orderCode";
	private static final String MERCHANT_TRANSACTION_CODE = "MerchantCode";
	private static final String REQUEST_ID = "requestId";
	private Currency currency = Currency.getInstance(USD);
	private BigDecimal totalAmount = BigDecimal.valueOf(34.45);

	@InjectMocks
	private WileyFollowOnRefundCommand testedInstance = new WileyFollowOnRefundCommand();

	@Mock
	private WileyPaymentGateway wileyPaymentGateway;

	private WileyFollowOnRefundRequest commandRequest;
	private WileyRefundResult convertedRefundResult = new WileyRefundResult();

	@Before
	public void setUp()
	{
		commandRequest = createCommandRequest();
		when(wileyPaymentGateway.tokenRefund(commandRequest)).thenReturn(convertedRefundResult);
	}

	@Test
	public void shouldInvokeTokenRefundOperation()
	{
		testedInstance.perform(commandRequest);

		verify(wileyPaymentGateway).tokenRefund(commandRequest);
	}

	@Test
	public void shouldReturnRefundResult()
	{
		final WileyRefundResult refundResult = testedInstance.perform(commandRequest);

		assertSame(convertedRefundResult, refundResult);
	}

	@Test
	public void shouldSetCurrencyToResult()
	{
		final WileyRefundResult refundResult = testedInstance.perform(commandRequest);

		assertEquals(USD, refundResult.getCurrency().getCurrencyCode());
	}

	@Test
	public void shouldReturnCommunicationFailureOnWebServiceException()
	{
		when(wileyPaymentGateway.tokenRefund(commandRequest)).thenThrow(new WebServiceException());

		final WileyRefundResult refundResult = testedInstance.perform(commandRequest);

		assertEquals(TransactionStatus.ERROR, refundResult.getTransactionStatus());
		assertEquals(WileyTransactionStatusEnum.WPG_COMMUNICATION_FAILURE, refundResult.getWpgTransactionStatus());
	}

	@Test
	public void shouldReturnGeneralFailure()
	{
		when(wileyPaymentGateway.tokenRefund(commandRequest)).thenThrow(new NullPointerException());

		final WileyRefundResult refundResult = testedInstance.perform(commandRequest);

		assertEquals(TransactionStatus.ERROR, refundResult.getTransactionStatus());
		assertEquals(WileyTransactionStatusEnum.GENERAL_SYSTEM_ERROR, refundResult.getWpgTransactionStatus());
	}

	private WileyFollowOnRefundRequest createCommandRequest()
	{
		WileyFollowOnRefundRequest refundRequest = new WileyFollowOnRefundRequest(
				MERCHANT_TRANSACTION_CODE,
				REQUEST_ID,
				REQUEST_TOKEN,
				currency,
				totalAmount,
				PAYMENT_PROVIDER,
				SITE_ID,
				ORDER_CODE
		);
		return refundRequest;
	}
}