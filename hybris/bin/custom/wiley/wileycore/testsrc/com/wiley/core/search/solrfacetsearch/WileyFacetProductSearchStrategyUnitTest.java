package com.wiley.core.search.solrfacetsearch;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.solrfacetsearch.search.FacetSearchException;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.solrfacetsearch.search.SearchResult;
import de.hybris.platform.solrfacetsearch.search.context.FacetSearchContext;
import de.hybris.platform.solrfacetsearch.solr.Index;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrServerException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyFacetProductSearchStrategyUnitTest
{
	@InjectMocks
	private WileyFacetProductSearchStrategy wileyFacetProductSearchStrategy;
	@Mock
	private SolrClient solrClient;
	@Mock
	private FacetSearchContext facetSearchContext;
	@Mock
	private Index index;
	@Mock
	private SearchResult searchResult;
	@Mock
	private WileySearchVariantsHelper wileySearchVariantsHelper;
	@Mock
	private SearchQuery searchQuery;

	@Test
	public void postProcessSearchResult() throws SolrServerException, IOException, FacetSearchException
	{
		when(facetSearchContext.getSearchQuery()).thenReturn(searchQuery);

		final SearchResult result = wileyFacetProductSearchStrategy.postProcessSearchResult(solrClient, facetSearchContext,
				index, searchResult);

		verify(wileySearchVariantsHelper).queryForVariants(solrClient, index, searchResult, searchQuery);
		assertEquals(result, searchResult);
	}
}
