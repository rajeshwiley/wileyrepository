package com.wiley.core.payment.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionRequest;
import de.hybris.platform.acceleratorservices.payment.data.CustomerBillToData;
import de.hybris.platform.acceleratorservices.payment.data.CustomerShipToData;
import de.hybris.platform.acceleratorservices.payment.data.OrderInfoData;
import de.hybris.platform.acceleratorservices.payment.data.PaymentData;
import de.hybris.platform.acceleratorservices.urlresolver.SiteBaseUrlResolutionService;
import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;
import de.hybris.platform.cms2.model.contents.contentslot.ContentSlotModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSContentSlotService;
import de.hybris.platform.cms2.servicelayer.services.impl.DefaultCMSSiteService;
import de.hybris.platform.commerceservices.enums.SiteTheme;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.payment.strategies.SecurityHashGeneratorStrategy;
import com.wiley.core.payment.strategies.WPGRegionStrategy;
import com.wiley.core.payment.strategies.WPGVendorIdStrategy;
import com.wiley.core.payment.strategies.WileyTransactionIdGeneratorStrategy;
import com.wiley.core.wileyb2c.basesite.theme.Wileyb2cThemeNameResolutionStrategy;

import static com.wiley.core.payment.populators.WPGHttpRequestPopulator.SITE_LOGO_SLOT;
import static junit.framework.Assert.assertEquals;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WPGHttpValidateRequestPopulatorTest
{
	private static final String SESSION_WPG_VALIDATION_INITIATOR_KEY = "wpg_validation_initiator";

	private static final String WPG_SECURITY = "WPG_security";

	private static final String DESCRIPTION = "Test Auth Transaction";
	private static final String METHOD = "HTTP";
	private static final Integer VENDOR_ID = 204;
	private static final String VENDOR_ID_STR = VENDOR_ID.toString();
	private static final String TIMESTAMP = "1448024849595";
	private static final String VALUE = "12.99";
	private static final String CURRENCY = "USD";
	private static final String TRANSACTION_ID = "TestTransactionId";
	private static final String SITE_ID = "testSiteId";
	private static final String SITE_URL = "testSiteUrl";
	private static final String REGION = "NA";
	private static final String ADDRESS = "address";
	private static final String COUNTRY_CODE = "countryCode";
	private static final String POST_CODE = "postCode";
	private static final String SECURITY = "13412575029d1d78920e580917088eea";
	private static final String BACK_URL = "backUrl";
	private static final String[] PARAMETERS_FOR_SECURITY =
			{ TIMESTAMP, VENDOR_ID_STR, TRANSACTION_ID, METHOD, DESCRIPTION, REGION, ADDRESS, POST_CODE, COUNTRY_CODE };
	private static final String CHECKOUT = "checkout";
	private static final String EMPTY_PATH = "";
	private static final String EMPTY_ENCODING_ATTRIBUTES = "";
	private static final Integer IMAGE_WIDTH = 123;
	private static final String IMAGE_URL = "url/test";

	@InjectMocks
	private WPGHttpRequestPopulator testedInstance = new WPGHttpValidateRequestPopulator()
	{

		@Override
		public SecurityHashGeneratorStrategy getSecurityHashGeneratorStrategy()
		{
			return mockSecurityHashGeneratorStrategy;
		}

		@Override
		protected String createWpgTimestamp()
		{
			return TIMESTAMP;
		}
	};

	private CreateSubscriptionRequest source = new CreateSubscriptionRequest();
	private PaymentData target = new PaymentData();
	private CustomerBillToData customerBillToData = new CustomerBillToData();
	private CustomerShipToData customerShipToData = new CustomerShipToData();
	private OrderInfoData orderInfoData = new OrderInfoData();


	@Mock
	private Wileyb2cThemeNameResolutionStrategy wileyb2cThemeNameResolutionStrategy;
	@Mock
	private WileyTransactionIdGeneratorStrategy mockTransactionIdGeneratorStrategy;
	@Mock
	private SecurityHashGeneratorStrategy mockSecurityHashGeneratorStrategy;
	@Mock
	private WPGRegionStrategy mockWpgRegionStrategy;
	@Mock
	private ConfigurationService mockConfigurationService;
	@Mock
	private BaseSiteService mockBaseSiteService;
	@Mock
	private CMSSiteModel mockBaseSite;
	@Mock
	private Configuration mockConfiguration;
	@Mock
	private WPGVendorIdStrategy httpVendorIdStrategy;
	@Mock
	private SessionService mockSessionService;
	@Mock
	private UserService mockUserService;
	@Mock
	private UserModel mockUserModel;
	@Mock
	private DefaultCMSSiteService cmsSiteService;
	@Mock
	private SiteTheme testTheme;
	@Mock
	private SiteBaseUrlResolutionService siteBaseUrlResolutionService;
	@Mock
	private CMSContentSlotService cmsContentSlotServiceMock;
	@Mock
	private ContentSlotModel contentSlotModelMock;
	@Mock
	private MediaContainerModel mediaContainerModelMock;
	@Mock
	private SiteConfigService siteConfigService;

	@Before
	public void setUp()
	{
		when(mockTransactionIdGeneratorStrategy.generateTransactionId()).thenReturn(TRANSACTION_ID);
		when(mockConfigurationService.getConfiguration()).thenReturn(mockConfiguration);
		when(mockBaseSiteService.getCurrentBaseSite()).thenReturn(mockBaseSite);
		when(siteBaseUrlResolutionService.getWebsiteUrlForSite(mockBaseSite, EMPTY_ENCODING_ATTRIBUTES, true, EMPTY_PATH))
				.thenReturn(SITE_URL);
		when(mockBaseSite.getUid()).thenReturn(SITE_ID);
		when(mockWpgRegionStrategy.getRegionByCurrency(anyString())).thenReturn(REGION);
		when(mockBaseSite.getTheme()).thenReturn(testTheme);
		when(testTheme.getCode()).thenReturn("testThemeCode");
		when(cmsSiteService.getCurrentSite()).thenReturn(mockBaseSite);

		when(mockSessionService.getAttribute(SESSION_WPG_VALIDATION_INITIATOR_KEY)).thenReturn(CHECKOUT);
		when(mockUserService.getCurrentUser()).thenReturn(mockUserModel);
		when(siteConfigService.getProperty(WileyCoreConstants.ANALYTICS_SCRIPT_URL_CONFIG_KEY)).thenReturn(StringUtils.EMPTY);

		doReturn(contentSlotModelMock).when(cmsContentSlotServiceMock).getContentSlotForId(SITE_LOGO_SLOT);
		doReturn(new ArrayList<AbstractCMSComponentModel>()).when(contentSlotModelMock).getCmsComponents();

		testedInstance.setWpgDescription(DESCRIPTION);
		testedInstance.setWpgMethod(METHOD);

		orderInfoData.setCurrency(CURRENCY);
		orderInfoData.setOrderTotal(VALUE);

		customerBillToData.setBillToStreet1(ADDRESS);
		customerBillToData.setBillToPostalCode(POST_CODE);
		customerBillToData.setCountryNumericIsocode(COUNTRY_CODE);

		source.setCustomerShipToData(customerShipToData);
		source.setCustomerBillToData(customerBillToData);
		source.setOrderInfoData(orderInfoData);

		Map<String, String> backUrlsBySite = new HashMap<>();
		backUrlsBySite.put(SITE_ID, BACK_URL);
		testedInstance.setWpgBackUrlBySite(backUrlsBySite);
	}

	@Test
	public void shouldCreateCorrectWPGSecurity()
	{
		when(httpVendorIdStrategy.getVendorId(SITE_ID)).thenReturn(VENDOR_ID);
		testedInstance.populate(source, target);
		final String expectedCounterMeasureBase = buildCounterMeasureBase(PARAMETERS_FOR_SECURITY);
		verify(mockSecurityHashGeneratorStrategy).generateSecurityHash(eq(SITE_ID), eq(expectedCounterMeasureBase));
	}

	@Test
	public void shouldAddWPGSecurityParameter()
	{
		when(mockSecurityHashGeneratorStrategy.generateSecurityHash(eq(SITE_ID), anyString())).thenReturn(SECURITY);
		testedInstance.populate(source, target);
		assertEquals(SECURITY, target.getParameters().get(WPG_SECURITY));
	}


	private String buildCounterMeasureBase(final String... parameters)
	{
		return StringUtils.join(parameters, "");
	}

}
