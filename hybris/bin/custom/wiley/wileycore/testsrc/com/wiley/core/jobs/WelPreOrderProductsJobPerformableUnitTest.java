package com.wiley.core.jobs;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.junit.Assert.assertEquals;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.BusinessProcessEvent;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.util.ArrayList;
import java.util.List;

import com.wiley.core.model.WelPreOrderProductsCronJobModel;
import com.wiley.core.order.dao.WileyOrderDao;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WelPreOrderProductsJobPerformableUnitTest
{
	private static final String WEL_ORDER_PROCESS_CODE = "welOrderProcess";
	private static final String NON_WEL_ORDER_PROCESS_CODE = "nonWelOrderProcess";
	private static final String WEL_ORDER_PROCESS = "wel-order-process";
	private static final String NON_WEL_ORDER_PROCESS = "non-wel-order-process";

	@InjectMocks
	private WelPreOrderProductsJobPerformable job;

	@Mock
	private BusinessProcessService businessProcessService;

	@Mock
	private WileyOrderDao wileyOrderDao;

	@Before
	public void setUp()
	{
		List<OrderModel> orders = createListOfOrders();
		when(wileyOrderDao.findPreOrdersWithActiveProducts()).thenReturn(orders);
	}

	@Test
	public void testPerform()
	{
		PerformResult result = job.perform(new WelPreOrderProductsCronJobModel());
		verify(businessProcessService, times(1)).triggerEvent(any(BusinessProcessEvent.class));
		assertEquals(CronJobStatus.FINISHED, result.getStatus());
		assertEquals(CronJobResult.SUCCESS, result.getResult());
	}

	private List<OrderModel> createListOfOrders()
	{
		List<OrderModel> listOfOrders = new ArrayList<>();
		OrderModel order = new OrderModel();
		List<OrderProcessModel> collectionOfProcess = new ArrayList<>();
		collectionOfProcess.add(createOrderProcess(WEL_ORDER_PROCESS_CODE, WEL_ORDER_PROCESS));
		collectionOfProcess.add(createOrderProcess(NON_WEL_ORDER_PROCESS_CODE, NON_WEL_ORDER_PROCESS));
		order.setOrderProcess(collectionOfProcess);
		listOfOrders.add(order);
		return listOfOrders;
	}

	private OrderProcessModel createOrderProcess(final String code, final String processDefinitionName)
	{
		OrderProcessModel welOrderProcess = new OrderProcessModel();
		welOrderProcess.setCode(code);
		welOrderProcess.setProcessDefinitionName(processDefinitionName);
		return welOrderProcess;
	}
}
