package com.wiley.core.wiley.caching.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.regioncache.key.CacheKey;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.externaltax.xml.caching.impl.WileyVarArgsCacheKey;

import static junit.framework.TestCase.assertFalse;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyVarArgsKeyMapperImplTest
{
	@Spy
	private WileyVarArgsKeyMapperImpl testedInstance;

	@Before
	public void setup()
	{
		Mockito.doAnswer((req) -> new WileyVarArgsCacheKey(req.getArguments())).when(testedInstance).wrap(Mockito.anyVararg());
	}

	@Test
	public void shouldReturnNullIfKeyObjectIsNull()
	{
		final CacheKey key = testedInstance.map(null);
		assertNull(key);
	}

	@Test
	public void shouldCreateCacheKeyWithParametersArray()
	{
		final String[] keyParams = { "a", "b", "c" };
		final CacheKey key = testedInstance.map(keyParams);
		assertEquals(new WileyVarArgsCacheKey(keyParams), key);
	}

	@Test
	public void shouldCreateCacheKeyWithSingleParameter()
	{
		final String keyValue = "a";
		final CacheKey key = testedInstance.map(keyValue);
		assertEquals(new WileyVarArgsCacheKey(keyValue), key);
	}

	@Test
	public void shouldCreateNotMatchingKeys()
	{
		//Given
		final Object[] varArgsParams = { "a", "b", "c" };
		final Object singleValueParam = varArgsParams;
		//When
		final CacheKey varArgsKey = testedInstance.map(varArgsParams);
		final CacheKey singleParamKey = new WileyVarArgsCacheKey(singleValueParam);
		//Then
		assertFalse(singleParamKey.equals(varArgsKey));
	}
}
