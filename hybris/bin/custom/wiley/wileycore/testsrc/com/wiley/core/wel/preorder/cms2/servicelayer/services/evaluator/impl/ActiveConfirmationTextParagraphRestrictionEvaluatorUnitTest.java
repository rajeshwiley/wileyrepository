package com.wiley.core.wel.preorder.cms2.servicelayer.services.evaluator.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;
import de.hybris.platform.cms2.servicelayer.data.RestrictionData;

import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.wiley.core.model.WelActiveConfirmationCMSRestrictionModel;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ActiveConfirmationTextParagraphRestrictionEvaluatorUnitTest
{
	public static final String CONFIRMATION_TEXT_PARAGRAPH = "ConfirmationTextParagraph";
	public static final String OTHER_COMPONENTS = "OTHER_COMPONENTS";
	public static final String IS_PRE_ORDER = "isPreOrder";
	public static final String COMPONENT = "component";
	@Mock
	private WelActiveConfirmationCMSRestrictionModel testRestriction;
	@Mock
	private RestrictionData testRestrictionData;
	@Mock
	private AbstractCMSComponentModel testComponent;
	@Mock
	private ServletRequestAttributes attrs;
	@InjectMocks
	private ActiveConfirmationTextParagraphRestrictionEvaluator testEvaluator;

	@Before
	public void before()
	{
		RequestContextHolder.setRequestAttributes(attrs);
	}

	@After
	public void cleanup()
	{
		RequestContextHolder.resetRequestAttributes();
	}

	@Test
	public void hideActiveConfirmationTextParagraphComponentWithPreOrderProductTest()
	{
		//given
		when(testRestrictionData.getValue(COMPONENT)).thenReturn(testComponent);
		when(testComponent.getUid()).thenReturn(CONFIRMATION_TEXT_PARAGRAPH);
		when(attrs.getAttribute(IS_PRE_ORDER, RequestAttributes.SCOPE_REQUEST)).thenReturn(Boolean.TRUE);

		//when
		final boolean show = testEvaluator.evaluate(testRestriction, testRestrictionData);

		//then
		Assert.assertFalse(show);
	}

	@Test
	public void showActiveConfirmationTextParagraphComponentWithoutPreOrderProductTest()
	{
		//given
		when(testRestrictionData.getValue(COMPONENT)).thenReturn(testComponent);
		when(testComponent.getUid()).thenReturn(CONFIRMATION_TEXT_PARAGRAPH);
		when(attrs.getAttribute(IS_PRE_ORDER, RequestAttributes.SCOPE_REQUEST)).thenReturn(Boolean.FALSE);

		//when
		final boolean show = testEvaluator.evaluate(testRestriction, testRestrictionData);

		//then
		Assert.assertTrue(show);
	}

	@Test
	public void showAnyComponentDifferentedFromActiveConfirmationTextParagraphTest()
	{
		//given
		when(testRestrictionData.getValue(COMPONENT)).thenReturn(testComponent);
		when(testComponent.getUid()).thenReturn(OTHER_COMPONENTS);

		//when
		final boolean show = testEvaluator.evaluate(testRestriction, testRestrictionData);

		//then
		Assert.assertTrue(show);
	}
}
