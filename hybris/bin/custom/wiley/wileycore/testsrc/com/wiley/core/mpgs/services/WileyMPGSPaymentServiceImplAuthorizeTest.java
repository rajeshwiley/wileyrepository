package com.wiley.core.mpgs.services;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import static org.junit.Assert.assertSame;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyTnsMerchantModel;
import com.wiley.core.mpgs.constants.WileyMPGSConstants;
import com.wiley.core.mpgs.request.WileyAuthorizationRequest;
import com.wiley.core.mpgs.response.WileyAuthorizationResponse;
import com.wiley.core.mpgs.services.impl.WileyMPGSPaymentServiceImpl;
import com.wiley.core.mpgs.strategies.WileyMPGSTransactionIdGeneratorStrategy;
import com.wiley.core.payment.strategies.AmountToPayCalculationStrategy;
import com.wiley.core.payment.tnsmerchant.service.WileyTnsMerchantService;
import com.wiley.core.payment.transaction.PaymentTransactionService;

import org.apache.commons.configuration.Configuration;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyMPGSPaymentServiceImplAuthorizeTest
{
	private static final String TEST_TOKEN = "TEST_TOKEN";
	private static final String TEST_TRANSACTION_ID = "TEST_TRANSACTION_ID";
	private static final String TEST_CURRENCY = "AUD";
	private static final BigDecimal TEST_TOTAL_AMOUNT = new BigDecimal(100.0);
	private static final String TEST_CITY = "New Jersey";
	private static final String TEST_COUNTRY = "USA";
	private static final String TEST_ISO_CODE = "US";
	private static final String TEST_CURRENCY_ISO_CODE = "USA";
	private static final String TEST_POSTAL_CODE = "07030";
	private static final String TEST_STATE = "US-NJ";
	private static final String TEST_STREET = "111 River St";
	private static final String TEST_ORDER_ID = "TEST_ORDER_ID";
	private static final String ORDER_URL_PART = "/order/";
	private static final String TRANSACTION_URL_PART = "/transaction/";
	private static final String TEST_MERCHANT_ID = "TEST_MERCHANT_ID";
	private static final String MERCHANT_ID_PART = "/merhcant/";
	private static final String TEST_AUTHORIZATION_END_POINT_URL =
			"https://pki.na.tnspayments.com/api/rest/version/44" + MERCHANT_ID_PART + TEST_MERCHANT_ID + ORDER_URL_PART 
			+ TEST_ORDER_ID + TRANSACTION_URL_PART + TEST_TRANSACTION_ID;
	private static final String AS_SITE_ID = "asSite";
	private static final String TEST_CODE = "a19b9a12-87c9-4c75-b35e-d943f569e59d";

	private List<PaymentTransactionModel> paymentTransactionList = new ArrayList<>();

	@InjectMocks
	private WileyMPGSPaymentServiceImpl wileyMPGSPaymentService;

	@Mock
	private WileyCardPaymentService wileyCardPaymentService;

	@Mock
	private WileyMPGSPaymentEntryService wileyMPGSPaymentEntryService;

	@Mock
	private PaymentTransactionService paymentTransactionService;

	@Mock
	private WileyMPGSTransactionIdGeneratorStrategy wileyMPGSTransactionIdGenerateStrategy;

	@Mock
	private AmountToPayCalculationStrategy amountToPayCalculationStrategy;

	@Mock
	private AbstractOrderModel cart;

	@Mock
	private CurrencyModel currency;

	@Mock
	private CountryModel country;

	@Mock
	private PaymentTransactionModel paymentTransaction;

	@Mock
	private PaymentTransactionEntryModel authorizeTransactionEntry;

	@Mock
	private WileyAuthorizationResponse response;

	@Mock
	private CreditCardPaymentInfoModel paymentInfo;

	@Mock
	private AddressModel billingAddress;

	@Mock
	private RegionModel region;

	@Mock
	private WileyUrlGenerationService wileyUrlGenerationService;

	@Mock
	private WileyMPGSMerchantService wileyMPGSMerchantService;

	@Mock
	private WileyTnsMerchantService wileyTnsMerchantService;

	@Mock
	private ConfigurationService configurationService;

	@Mock
	private Configuration configuration;
	
	@Mock
	private AddressModel paymentAddress;
	
	@Mock
	private BaseSiteModel baseSiteModel;

	@Mock
	private WileyTnsMerchantModel wileyTnsMerchantMock;

	@Test
	public void shouldCreateAuthorizationEntryTest()
	{
		paymentTransactionList.add(paymentTransaction);
		when(cart.getPaymentTransactions()).thenReturn(paymentTransactionList);
		when(cart.getCurrency()).thenReturn(currency);
		when(cart.getPaymentInfo()).thenReturn(paymentInfo);
		when(cart.getPaymentInfo().getBillingAddress()).thenReturn(billingAddress);
		when(billingAddress.getCountry()).thenReturn(country);
		when(paymentInfo.getSubscriptionId()).thenReturn(TEST_TOKEN);
		when(wileyMPGSTransactionIdGenerateStrategy.generateTransactionId()).thenReturn(TEST_TRANSACTION_ID);
		when(cart.getGuid()).thenReturn(TEST_ORDER_ID);
		when(cart.getSite()).thenReturn(baseSiteModel);
		when(baseSiteModel.getUid()).thenReturn(AS_SITE_ID);
		when(cart.getPaymentAddress()).thenReturn(paymentAddress);
		when(paymentAddress.getCountry()).thenReturn(country);
		when(country.getIsocode()).thenReturn(TEST_ISO_CODE);
		when(wileyTnsMerchantService.getTnsMerchant(TEST_ISO_CODE, TEST_CURRENCY_ISO_CODE)).thenReturn(wileyTnsMerchantMock);
		when(wileyTnsMerchantMock.getId()).thenReturn(TEST_MERCHANT_ID);
		when(wileyUrlGenerationService.getAuthorizationURL(TEST_MERCHANT_ID, TEST_ORDER_ID, TEST_TRANSACTION_ID)).thenReturn(
				TEST_AUTHORIZATION_END_POINT_URL);
		when(paymentTransactionService.getLastPaymentTransaction(cart)).thenReturn(paymentTransaction);
		when(currency.getIsocode()).thenReturn(TEST_CURRENCY);
		when(amountToPayCalculationStrategy.getOrderAmountToPay(cart)).thenReturn(TEST_TOTAL_AMOUNT);
		when(billingAddress.getLine1()).thenReturn(TEST_STREET);
		when(billingAddress.getLine2()).thenReturn(TEST_STREET);
		when(billingAddress.getTown()).thenReturn(TEST_CITY);
		when(billingAddress.getRegion()).thenReturn(region);
		when(region.getIsocode()).thenReturn(TEST_STATE);
		when(billingAddress.getPostalcode()).thenReturn(TEST_POSTAL_CODE);
		when(country.getIsocode3()).thenReturn(TEST_COUNTRY);
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getString(WileyMPGSConstants.PAYMENT_MPGS_VENDOR_ID)).thenReturn("257");
		when(wileyCardPaymentService.authorize(any(WileyAuthorizationRequest.class))).thenReturn(response);
		when(paymentTransaction.getOrder()).thenReturn(cart);
		when(wileyMPGSPaymentEntryService.createAuthorizationPaymentTransactionEntry(response, paymentTransaction)).thenReturn(
				authorizeTransactionEntry);
		when(wileyMPGSPaymentEntryService.createPaymentTransaction(cart)).thenReturn(paymentTransaction);
		when(paymentTransaction.getCode()).thenReturn(TEST_CODE);

		final PaymentTransactionEntryModel transactionEntry = wileyMPGSPaymentService.authorize(cart);

		assertSame(authorizeTransactionEntry, transactionEntry);
		verify(wileyCardPaymentService).authorize(any(WileyAuthorizationRequest.class));
		verify(wileyMPGSPaymentEntryService).createAuthorizationPaymentTransactionEntry(response, paymentTransaction);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldNotCallAuthorizationWhenNull()
	{
		PaymentTransactionEntryModel transactionEntry = wileyMPGSPaymentService.authorize(null);
	}
}

