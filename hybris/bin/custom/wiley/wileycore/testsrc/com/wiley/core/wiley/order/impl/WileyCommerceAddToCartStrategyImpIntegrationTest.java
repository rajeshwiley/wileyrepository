package com.wiley.core.wiley.order.impl;

import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.impl.DefaultCommerceAddToCartStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import javax.annotation.Resource;

import org.junit.Ignore;
import org.junit.Test;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.model.WileyProductModel;
import com.wiley.core.wiley.restriction.exception.ProductNotVisibleException;

import reactor.util.Assert;


/**
 * @author Dzmitryi_Halahayeu
 */
@Ignore
public abstract class WileyCommerceAddToCartStrategyImpIntegrationTest extends ServicelayerTransactionalTest
{
	private static final String USD_CURRENCY = "USD";
	private static final String TEST_GROUP = "testgroup";
	@Resource
	private ModelService modelService;
	@Resource
	private UserService userService;
	@Resource
	private CommonI18NService commonI18NService;
	@Resource
	private ProductService productService;

	@Test
	public void testFailWhenUserGroupRestrictionApply() throws CommerceCartModificationException
	{
		final WileyProductModel product = (WileyProductModel) productService.getProductForCode(
				getRestrictedUserGroupProductCode());
		final CommerceCartParameter commerceCartParameter = createCartParameter(
				product, userService.getCurrentUser());

		boolean isFailed = false;
		try
		{
			getAddToCartStrategy().addToCart(commerceCartParameter);
		}
		catch (final CommerceCartModificationException exception)
		{
			isFailed = true;
			checkException(exception, WileyCoreConstants.PRODUCT_IS_NOT_PURCHASABLE, "not available for user group");
		}

		assertTrue(isFailed);
	}

	@Test
	public void testDoNotFailWhenUserGroupIsCorrect() throws CommerceCartModificationException
	{
		final WileyProductModel product = (WileyProductModel) productService.getProductForCode(
				getRestrictedUserGroupProductCode());
		final UserModel currentUser = userService.getCurrentUser();
		currentUser.setGroups(Collections.singleton(userService.getUserGroupForUID(TEST_GROUP)));
		final CommerceCartParameter commerceCartParameter = createCartParameter(product);

		final CommerceCartModification commerceCartModification = getAddToCartStrategy().addToCart(
				commerceCartParameter);

		Assert.notNull(commerceCartModification);
	}

	private void checkException(final CommerceCartModificationException exception, final String messageCode, final String message)
	{
		final Throwable cause = exception.getCause();
		assertNotNull(cause);
		assertEquals(cause.getClass(), ProductNotVisibleException.class);
		assertTrue(exception.getMessage().contains(message));
		final ProductNotVisibleException productNotVisibleException = (ProductNotVisibleException) cause;
		assertEquals(productNotVisibleException.getErrorMessageCode(), messageCode);
	}

	private CommerceCartParameter createCartParameter(final ProductModel b2cProduct, final UserModel user)
	{
		final CommerceCartParameter commerceCartParameter = new CommerceCartParameter();
		commerceCartParameter.setProduct(b2cProduct);
		final CartModel cart = new CartModel();
		cart.setUser(user);
		cart.setCurrency(commonI18NService.getCurrency(USD_CURRENCY));
		cart.setDate(new Date());
		cart.setNet(Boolean.TRUE);
		cart.setEntries(new ArrayList<>());
		commerceCartParameter.setCart(cart);
		commerceCartParameter.setQuantity(1);
		commerceCartParameter.setUser(user);
		return commerceCartParameter;
	}


	protected CommerceCartParameter createCartParameter(final ProductModel product)
	{
		final UserModel user = userService.getCurrentUser();
		final CommerceCartParameter commerceCartParameter = new CommerceCartParameter();
		commerceCartParameter.setEnableHooks(true);
		commerceCartParameter.setProduct(product);
		final CartModel cart = new CartModel();
		cart.setUser(user);
		cart.setCurrency(commonI18NService.getCurrency(USD_CURRENCY));
		cart.setDate(new Date());
		cart.setNet(Boolean.TRUE);
		final CartEntryModel cartEntryModel = new CartEntryModel();
		cartEntryModel.setBasePrice(44.0);
		cartEntryModel.setQuantity(1L);
		cartEntryModel.setProduct(product);
		cartEntryModel.setOrder(cart);
		cartEntryModel.setUnit(product.getUnit());
		modelService.save(cartEntryModel);
		cart.setEntries(Collections.singletonList(cartEntryModel));
		modelService.save(cart);
		commerceCartParameter.setCart(cart);
		commerceCartParameter.setQuantity(1);
		commerceCartParameter.setUser(user);
		return commerceCartParameter;
	}

	protected abstract DefaultCommerceAddToCartStrategy getAddToCartStrategy();

	protected abstract String getRestrictedUserGroupProductCode();

}
