package com.wiley.core.customer;

import de.hybris.platform.core.Registry;
import de.hybris.platform.core.Tenant;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.concurrent.Callable;


/**
 * Task for WileyCustomerHotFolderIntegrationTest
 */
public class GetWileyCustomerTask implements Callable<CustomerModel>
{
	private String customerUID;
	private UserService userService;
	private Tenant tenant;

	public GetWileyCustomerTask(final String customerUID, final UserService userService, final Tenant tenant)
	{
		this.customerUID = customerUID;
		this.userService = userService;
		this.tenant = tenant;
	}

	/**
	 * Computes a result, or throws an exception if unable to do so.
	 *
	 * @return computed result
	 * @throws Exception
	 * 		if unable to compute a result
	 */
	@Override
	public CustomerModel call() throws Exception
	{
		CustomerModel customerModel = null;
		try
		{
			Registry.setCurrentTenant(tenant);
			JaloSession.getCurrentSession().activate();
			while (!userService.isUserExisting(customerUID))
			{
				Thread.sleep(100);
			}
			customerModel = userService.getUserForUID(customerUID, CustomerModel.class);
		}
		finally
		{
			JaloSession.getCurrentSession().close();
			JaloSession.deactivate();
			Registry.unsetCurrentTenant();
		}
		return customerModel;
	}
}
