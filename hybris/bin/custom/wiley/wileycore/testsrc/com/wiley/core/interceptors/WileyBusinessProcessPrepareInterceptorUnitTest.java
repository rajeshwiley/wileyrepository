package com.wiley.core.interceptors;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.verification.VerificationMode;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyBusinessProcessPrepareInterceptorUnitTest
{
	@Mock
	private InterceptorContext ctxMock;

	@Mock
	private BusinessProcessModel processMock;

	@InjectMocks
	private WileyBusinessProcessPrepareInterceptor interceptor;

	@Test
	public void onPrepareNewTest()
	{
		when(ctxMock.isNew(processMock)).thenReturn(true);
		interceptor.onPrepare(processMock, ctxMock);
		verifyTestResult(atLeastOnce(), atLeastOnce());
	}

	@Test
	public void onPrepareNotNewTest()
	{
		when(ctxMock.isNew(processMock)).thenReturn(false);
		interceptor.onPrepare(processMock, ctxMock);
		verifyTestResult(atLeastOnce(), never());
	}

	private void verifyTestResult(final VerificationMode... modes)
	{
		verify(ctxMock, modes[0]).isNew(processMock);
		verify(processMock, modes[1]).setTypeSystem(any());
	}
}