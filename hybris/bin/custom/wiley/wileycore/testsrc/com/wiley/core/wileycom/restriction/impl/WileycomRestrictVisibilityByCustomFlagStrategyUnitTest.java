/**
 *
 */
package com.wiley.core.wileycom.restriction.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.product.ProductModel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileycomRestrictVisibilityByCustomFlagStrategyUnitTest
{
	@InjectMocks
	private WileycomRestrictVisibilityByCustomFlagStrategy wileycomRestrictVisibilityByCustomFlagStrategy;
	@Mock
	private ProductModel productModel;
	private CommerceCartParameter parameter;

	@Before
	public void before()
	{
		parameter = new CommerceCartParameter();
		parameter.setProduct(productModel);
	}

	@Test
	public void shouldBeRestrictedWhenCustomFlagIsTrue() throws CommerceCartModificationException
	{
		when(productModel.getCustom()).thenReturn(Boolean.TRUE);

		final boolean restricted = wileycomRestrictVisibilityByCustomFlagStrategy.isRestricted(parameter);

		assertTrue(restricted);
	}

	@Test
	public void shouldNotBeRestrictedWhenCustomFlagIsFalse() throws CommerceCartModificationException
	{
		when(productModel.getCustom()).thenReturn(Boolean.FALSE);

		final boolean restricted = wileycomRestrictVisibilityByCustomFlagStrategy.isRestricted(parameter);

		assertFalse(restricted);
	}

	@Test
	public void shouldNotBeRestrictedWhenCustomFlagIsNull() throws CommerceCartModificationException
	{
		when(productModel.getCustom()).thenReturn(null);

		final boolean restricted = wileycomRestrictVisibilityByCustomFlagStrategy.isRestricted(parameter);

		assertFalse(restricted);
	}

}
