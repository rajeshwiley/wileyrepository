package com.wiley.core.mpgs.services;


import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;

import static org.junit.Assert.assertSame;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyTnsMerchantModel;
import com.wiley.core.mpgs.request.WileyTokenizationRequest;
import com.wiley.core.mpgs.response.WileyTokenizationResponse;
import com.wiley.core.mpgs.services.impl.WileyMPGSPaymentServiceImpl;
import com.wiley.core.payment.tnsmerchant.service.WileyTnsMerchantService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyMPGSPaymentServiceImplTokenizeTest
{
	private static final String TOKEN_URL_VALUE = "https://localhost/api/rest/version/44/merchant/TESTID/token/";
	private static final String SESSION_ID = "SESSION0002893994386G920688978J3";
	private static final String TEST_MERCHANT_ID = "TEST_MERCHANT_ID";
	private static final String TEST_ISO_CODE = "US";
	private static final String TEST_CURRENCY_ISO_CODE = "USA";
	private static final String AS_SITE_ID = "asSite";
	private static final String TEST_CODE = "a19b9a12-87c9-4c75-b35e-d943f569e59d";

	@Mock
	private PaymentTransactionEntryModel mockRetrieveSessionEntry;

	@Mock
	private PaymentTransactionEntryModel mockTokenizationEntry;

	@Mock
	private PaymentTransactionModel mockPaymentTransaction;

	@Mock
	private WileyTokenizationResponse mockTokenizationResult;

	@Mock
	private WileyCardPaymentService mockWileyCardPaymentService;

	@Mock
	private WileyMPGSPaymentEntryService mockWileyMPGSPaymentEntryService;

	@Mock
	private WileyUrlGenerationService wileyUrlGenerationService;

	@Mock
	private WileyMPGSMerchantService wileyMPGSMerchantService;

	@Mock
	private WileyTnsMerchantService wileyTnsMerchantService;

	@Mock
	private CartModel mockCartModel;

	@Mock
	private AddressModel paymentAddress;

	@Mock
	private CountryModel countryModel;

	@Mock
	private BaseSiteModel baseSiteModel;

	@Mock
	private WileyTnsMerchantModel wileyTnsMerchantMock;

	@InjectMocks
	private WileyMPGSPaymentServiceImpl underTestMpgsPaymentServiceImpl = new WileyMPGSPaymentServiceImpl();


	@Before
	public void setup()
	{
		when(wileyUrlGenerationService.getTokenizationUrl(TEST_MERCHANT_ID)).thenReturn(TOKEN_URL_VALUE);
		when(mockRetrieveSessionEntry.getRequestId()).thenReturn(SESSION_ID);
		when(mockRetrieveSessionEntry.getPaymentTransaction()).thenReturn(mockPaymentTransaction);
		when(mockWileyCardPaymentService.tokenize(any(WileyTokenizationRequest.class))).thenReturn(mockTokenizationResult);
		when(mockWileyMPGSPaymentEntryService
				.createTokenizationEntry(mockTokenizationResult, mockPaymentTransaction, true))
				.thenReturn(mockTokenizationEntry);

	}

	@Test
	public void shouldCreateTokenizationEntry()
	{
		when(mockCartModel.getSite()).thenReturn(baseSiteModel);
		when(baseSiteModel.getUid()).thenReturn(AS_SITE_ID);
		when(mockCartModel.getPaymentAddress()).thenReturn(paymentAddress);
		when(paymentAddress.getCountry()).thenReturn(countryModel);
		when(countryModel.getIsocode()).thenReturn(TEST_ISO_CODE);
		when(wileyTnsMerchantService.getTnsMerchant(TEST_ISO_CODE, TEST_CURRENCY_ISO_CODE)).thenReturn(wileyTnsMerchantMock);
		when(wileyTnsMerchantMock.getId()).thenReturn(TEST_MERCHANT_ID);
		when(mockPaymentTransaction.getCode()).thenReturn(TEST_CODE);

		PaymentTransactionEntryModel tokenizationEntry = underTestMpgsPaymentServiceImpl
				.tokenizeCart(mockCartModel, mockRetrieveSessionEntry, true);

		assertSame(mockTokenizationEntry, tokenizationEntry);

		verify(mockWileyCardPaymentService).tokenize(any(WileyTokenizationRequest.class));
		verify(mockWileyMPGSPaymentEntryService)
				.createTokenizationEntry(mockTokenizationResult, mockPaymentTransaction, true);

	}


	@Test(expected = IllegalArgumentException.class)
	public void shouldNotCallTokenizationWhenNull()
	{
		PaymentTransactionEntryModel tokenizationEntry = underTestMpgsPaymentServiceImpl
				.tokenizeCart(null, null, null);

		verify(mockWileyCardPaymentService, times(0)).tokenize(any(WileyTokenizationRequest.class));
		verify(mockWileyMPGSPaymentEntryService, times(0))
				.createTokenizationEntry(mockTokenizationResult, mockPaymentTransaction, true);

	}

}
