package com.wiley.core.mpgs.services.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.mpgs.strategies.WileyMPGSTransactionIdGeneratorStrategy;

import reactor.util.Assert;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyMPGSUrlGenerationServiceImplTest
{
	private static final String TRANSACTION_ID = "TRANSACTION_ID";
	private static final String ORDER_GUID = "ORDER_GUID";
	private static final String END_POINT = "END_POINT";
	private static final String SESSION_ID = "SESSION_ID";
	private static final String MERCHANT_ID = "MERCHANT_ID";

	@Mock
	private WileyMPGSTransactionIdGeneratorStrategy wileyMPGSTransactionIdGenerateStrategy;

	@Spy
	@InjectMocks
	private WileyMPGSUrlGenerationServiceImpl urlGenerationServiceImpl = new WileyMPGSUrlGenerationServiceImpl();

	@Before
	public void setup()
	{
		when(wileyMPGSTransactionIdGenerateStrategy.generateTransactionId()).thenReturn(TRANSACTION_ID);
		when(urlGenerationServiceImpl.getPaymentMpgsEndpoint()).thenReturn(END_POINT);
	}

	@Test
	public void testRetrieveSessionUrl()
	{
		String url = urlGenerationServiceImpl.getRetrieveSessionUrl(MERCHANT_ID, SESSION_ID);
		Assert.notNull(url);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testRetrieveSessionUrlNullParameter()
	{
		urlGenerationServiceImpl.getRetrieveSessionUrl(null, null);
	}

	@Test
	public void testTokenizationUrl()
	{
		String url = urlGenerationServiceImpl.getTokenizationUrl(MERCHANT_ID);
		Assert.notNull(url);
	}

	@Test
	public void testAuthorizationUrl()
	{
		String url = urlGenerationServiceImpl.getAuthorizationURL(MERCHANT_ID, ORDER_GUID, TRANSACTION_ID);
		Assert.notNull(url);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAuthorizationUrlNullParameter()
	{
		urlGenerationServiceImpl.getAuthorizationURL(null, null, null);
	}

	@Test
	public void testCaptureUrl()
	{
		String url = urlGenerationServiceImpl.getCaptureUrl(MERCHANT_ID, ORDER_GUID, TRANSACTION_ID);
		Assert.notNull(url);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testCaptureUrlNullParameter()
	{
		urlGenerationServiceImpl.getCaptureUrl(null, null, null);
	}

	@Test
	public void testRefundFollowOnUrl()
	{
		String url = urlGenerationServiceImpl.getRefundFollowOnUrl(MERCHANT_ID, ORDER_GUID, TRANSACTION_ID);
		Assert.notNull(url);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testRefundFollowOnNullParameter()
	{
		urlGenerationServiceImpl.getRefundFollowOnUrl(null, null, null);
	}
}
