package com.wiley.core.ags.order.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.order.CommerceAddToCartStrategy;
import de.hybris.platform.commerceservices.order.CommerceCartCalculationStrategy;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.subscriptionservices.model.SubscriptionProductModel;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertSame;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


/**
 * Default unit test for {@link AgsSingleSubscriptionAddToCartStrategy}.
 *
 * @author Aliaksei_Zlobich
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AgsSingleSubscriptionAddToCartStrategyUnitTest
{

	// Services

	@Mock
	private CommerceCartCalculationStrategy commerceCartCalculationStrategyMock;

	@Mock
	private CommerceAddToCartStrategy commerceAddToCartStrategyMock;

	@Mock
	private ModelService modelServiceMock;

	@InjectMocks
	private AgsSingleSubscriptionAddToCartStrategy agsSingleSubscriptionAddToCartStrategy =
			new AgsSingleSubscriptionAddToCartStrategy();

	// Test data

	@Mock
	private SubscriptionProductModel subscriptionProductModelMock;

	@Mock
	private AbstractOrderEntryModel orderEntryModelMock;

	@Mock
	private CommerceCartModification successCartModificationMock;

	@Mock
	private CommerceCartModification failCartModificationMock;

	@Mock
	private CartModel cartModelMock;

	@Before
	public void setUp() throws Exception
	{
		when(successCartModificationMock.getQuantityAdded()).thenReturn(1L);
		when(failCartModificationMock.getQuantityAdded()).thenReturn(0L);

		when(orderEntryModelMock.getProduct()).thenReturn(subscriptionProductModelMock);

		when(commerceAddToCartStrategyMock.addToCart(any(CommerceCartParameter.class))).thenReturn(successCartModificationMock);

		when(cartModelMock.getEntries()).thenReturn(Arrays.asList(orderEntryModelMock));
	}

	@Test
	public void shouldReplaceExistingSubscription() throws CommerceCartModificationException
	{
		// Given
		final CommerceCartParameter commerceCartParameter = createCommerceCartParameter(cartModelMock, mock(
				SubscriptionProductModel.class));

		// When
		final CommerceCartModification modification = agsSingleSubscriptionAddToCartStrategy.addToCart(
				commerceCartParameter);

		// Then
		assertSame("Expected success CartModification.", successCartModificationMock, modification);
		verify(commerceAddToCartStrategyMock).addToCart(eq(commerceCartParameter));
		verify(modelServiceMock).removeAll(Arrays.asList(orderEntryModelMock));
		verify(commerceCartCalculationStrategyMock).calculateCart(eq(commerceCartParameter));
	}

	@Test
	public void shouldAddProductWithSubscriptionProduct() throws CommerceCartModificationException
	{
		// Given
		final CommerceCartParameter commerceCartParameter = createCommerceCartParameter(cartModelMock, mock(ProductModel.class));

		// When
		final CommerceCartModification modification = agsSingleSubscriptionAddToCartStrategy.addToCart(
				commerceCartParameter);

		// Then
		assertSame("Expected success CartModification.", successCartModificationMock, modification);
		verify(commerceAddToCartStrategyMock).addToCart(eq(commerceCartParameter));
		verify(modelServiceMock, never()).removeAll(Arrays.asList(orderEntryModelMock));
	}

	@Test
	public void singleSubscriptionRuleShouldBeAppliedOnlyForMasterCart() throws CommerceCartModificationException
	{
		// Given
		final CommerceCartParameter commerceCartParameter = createCommerceCartParameter(cartModelMock, mock(
				SubscriptionProductModel.class));

		when(cartModelMock.getParent()).thenReturn(mock(CartModel.class));

		// When
		final CommerceCartModification modification = agsSingleSubscriptionAddToCartStrategy.addToCart(
				commerceCartParameter);

		// Then
		assertSame("Expected success CartModification.", successCartModificationMock, modification);
		verify(commerceAddToCartStrategyMock).addToCart(eq(commerceCartParameter));
		verify(modelServiceMock, never()).removeAll(Arrays.asList(orderEntryModelMock));
	}

	@Test
	public void shouldNotReplaceExistingSubscriptionIfCartModificationExceptionOccurred() throws CommerceCartModificationException
	{
		// Given
		final CommerceCartParameter commerceCartParameter = createCommerceCartParameter(cartModelMock, mock(
				SubscriptionProductModel.class));

		reset(commerceAddToCartStrategyMock);
		when(commerceAddToCartStrategyMock.addToCart(any(CommerceCartParameter.class))).thenThrow(
				CommerceCartModificationException.class);

		// When
		try
		{
			agsSingleSubscriptionAddToCartStrategy.addToCart(commerceCartParameter);
			fail("Expected CommerceCartModificationException.");
		}
		catch (CommerceCartModificationException e)
		{
			// success
		}

		// Then
		verify(commerceAddToCartStrategyMock).addToCart(eq(commerceCartParameter));
		verify(modelServiceMock, never()).removeAll(Arrays.asList(orderEntryModelMock));
	}

	@Test
	public void shouldNotReplaceExistingSubscriptionIfAnotherOneWasNotBeenAdded() throws CommerceCartModificationException
	{
		// Given
		final CommerceCartParameter commerceCartParameter = createCommerceCartParameter(cartModelMock, mock(
				SubscriptionProductModel.class));

		when(commerceAddToCartStrategyMock.addToCart(any(CommerceCartParameter.class))).thenReturn(failCartModificationMock);

		// When
		final CommerceCartModification modification = agsSingleSubscriptionAddToCartStrategy.addToCart(commerceCartParameter);

		// Then
		assertSame("Expected fail CartModification.", failCartModificationMock, modification);
		verify(commerceAddToCartStrategyMock).addToCart(eq(commerceCartParameter));
		verify(modelServiceMock, never()).removeAll(Arrays.asList(orderEntryModelMock));
	}

	private CommerceCartParameter createCommerceCartParameter(final CartModel cart, final ProductModel product)
	{
		CommerceCartParameter parameter = new CommerceCartParameter();
		parameter.setCart(cart);
		parameter.setProduct(product);
		return parameter;
	}
}