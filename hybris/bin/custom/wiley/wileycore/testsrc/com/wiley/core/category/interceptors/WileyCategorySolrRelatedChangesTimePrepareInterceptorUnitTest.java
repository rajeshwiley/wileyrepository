package com.wiley.core.category.interceptors;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PersistenceOperation;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.Lists;


/**
 * Unit test for {@link WileyCategorySolrRelatedChangesTimePrepareInterceptor}
 *
 * Created by Maksim_Kozich on 23.08.17.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyCategorySolrRelatedChangesTimePrepareInterceptorUnitTest
{
	public static final String CATEGORY_PROPERTY_1 = "categoryProperty1";
	public static final String CATEGORY_PROPERTY_2 = "categoryProperty2";
	public static final String CATEGORY_PROPERTY_3 = "categoryProperty3";
	public static final String CATEGORY_PROPERTY_OTHER = "categoryPropertyOther";
	private static final List<String> TEST_SOLR_CATEGORY_RELATED_PROPERTIES = Lists.newArrayList(CATEGORY_PROPERTY_1,
			CATEGORY_PROPERTY_2, CATEGORY_PROPERTY_3);
	public static final String CLASSIFICATION_CLASS_PROPERTY_1 = "classificationClassProperty1";
	public static final String CLASSIFICATION_CLASS_PROPERTY_2 = "classificationClassProperty2";
	public static final String CLASSIFICATION_CLASS_PROPERTY_3 = "classificationClassProperty3";
	public static final String CLASSIFICATION_CLASS_PROPERTY_OTHER = "classificationClassPropertyOther";
	private static final List<String> TEST_SOLR_CLASSIFICATION_CLASS_RELATED_PROPERTIES = Lists.newArrayList(
			CLASSIFICATION_CLASS_PROPERTY_1, CLASSIFICATION_CLASS_PROPERTY_2, CLASSIFICATION_CLASS_PROPERTY_3);

	@Mock
	private Date currentDateMock;

	@Mock
	private CategoryModel categoryModelMock,
			sub1Category1Mock, sub1Category2Mock,
			sub2Category1Mock,
			sub3Category1Mock,
			sub4Category1Mock, sub4Category2Mock;

	@Mock
	private ProductModel categoryProductMock1, categoryProductMock2,
			sub1Category1ProductMock1, sub1Category1ProductMock2, sub1Category2ProductMock1, sub1Category2ProductMock2,
			sub2Category1ProductMock1, sub2Category1ProductMock2,
			sub3Category1ProductMock1, sub3Category1ProductMock2,
			sub4Category1ProductMock1, sub4Category1ProductMock2, sub4Category2ProductMock1, sub4Category2ProductMock2;

	private List<ProductModel> categoryDescendantProductMocks;
	private List<ProductModel> categoryChildProductMocks;
	private List<ProductModel> categoryGrandChildProductMocks;

	@Mock
	private ClassificationClassModel classificationClassModelMock,
			sub1ClassificationClass1Mock, sub1ClassificationClass2Mock;

	@Mock
	private ProductModel classificationClassProductMock1, classificationClassProductMock2,
			sub1ClassificationClass1ProductMock1, sub1ClassificationClass1ProductMock2,
			sub1ClassificationClass2ProductMock1, sub1ClassificationClass2ProductMock2;

	private List<ProductModel> classificationClassDescendantProductMocks;
	private List<ProductModel> classificationClassChildProductMocks;
	private List<ProductModel> classificationClassGrandChildProductMocks;

	@Mock
	private InterceptorContext interceptorContextMock;

	@Spy
	@InjectMocks
	private WileyCategorySolrRelatedChangesTimePrepareInterceptor testInstance;


	@Before
	public void setUp() throws Exception
	{
		testInstance.setSolrCategoryRelatedProperties(TEST_SOLR_CATEGORY_RELATED_PROPERTIES);
		testInstance.setSolrClassificationClassRelatedProperties(TEST_SOLR_CLASSIFICATION_CLASS_RELATED_PROPERTIES);

		doReturn(currentDateMock).when(testInstance).getCurrentTime();

		when(interceptorContextMock.isModified(categoryModelMock, CATEGORY_PROPERTY_1)).thenReturn(Boolean.FALSE);
		when(interceptorContextMock.isModified(categoryModelMock, CATEGORY_PROPERTY_2)).thenReturn(Boolean.FALSE);
		when(interceptorContextMock.isModified(categoryModelMock, CATEGORY_PROPERTY_3)).thenReturn(Boolean.FALSE);
		when(interceptorContextMock.isModified(categoryModelMock, CATEGORY_PROPERTY_OTHER)).thenReturn(Boolean.FALSE);

		when(interceptorContextMock.isModified(classificationClassModelMock, CLASSIFICATION_CLASS_PROPERTY_1)).thenReturn(
				Boolean.FALSE);
		when(interceptorContextMock.isModified(classificationClassModelMock, CLASSIFICATION_CLASS_PROPERTY_2)).thenReturn(
				Boolean.FALSE);
		when(interceptorContextMock.isModified(classificationClassModelMock, CLASSIFICATION_CLASS_PROPERTY_3)).thenReturn(
				Boolean.FALSE);
		when(interceptorContextMock.isModified(classificationClassModelMock, CLASSIFICATION_CLASS_PROPERTY_OTHER)).thenReturn(
				Boolean.FALSE);
	}

	private void setUpClassificationClassStructure()
	{
		when(classificationClassModelMock.getProducts()).thenReturn(
				Arrays.asList(classificationClassProductMock1, classificationClassProductMock2));

		when(classificationClassModelMock.getAllSubcategories()).thenReturn(
				Arrays.asList(sub1ClassificationClass1Mock, sub1ClassificationClass2Mock));
		when(sub1ClassificationClass1Mock.getProducts()).thenReturn(
				Arrays.asList(sub1ClassificationClass1ProductMock1, sub1ClassificationClass1ProductMock2));
		when(sub1ClassificationClass2Mock.getProducts()).thenReturn(
				Arrays.asList(sub1ClassificationClass2ProductMock1, sub1ClassificationClass2ProductMock2));

		classificationClassDescendantProductMocks = Arrays.asList(classificationClassProductMock1,
				classificationClassProductMock2,
				sub1ClassificationClass1ProductMock1, sub1ClassificationClass1ProductMock2,
				sub1ClassificationClass2ProductMock1, sub1ClassificationClass2ProductMock2);
		classificationClassChildProductMocks = Arrays.asList(classificationClassProductMock1, classificationClassProductMock2);
		classificationClassGrandChildProductMocks = Arrays.asList(
				sub1ClassificationClass1ProductMock1,
				sub1ClassificationClass1ProductMock2,
				sub1ClassificationClass2ProductMock1, sub1ClassificationClass2ProductMock2);
	}

	private void setUpCategoryStructure()
	{
		when(categoryModelMock.getProducts()).thenReturn(Arrays.asList(categoryProductMock1, categoryProductMock2));

		when(categoryModelMock.getAllSubcategories()).thenReturn(Arrays.asList(sub1Category1Mock, sub1Category2Mock));
		when(sub1Category1Mock.getProducts()).thenReturn(Arrays.asList(sub1Category1ProductMock1, sub1Category1ProductMock2));
		when(sub1Category2Mock.getProducts()).thenReturn(Arrays.asList(sub1Category2ProductMock1, sub1Category2ProductMock2));

		when(sub1Category1Mock.getAllSubcategories()).thenReturn(Arrays.asList(sub2Category1Mock));
		when(sub2Category1Mock.getProducts()).thenReturn(Arrays.asList(sub2Category1ProductMock1, sub2Category1ProductMock2));

		when(sub2Category1Mock.getAllSubcategories()).thenReturn(Arrays.asList(sub3Category1Mock));
		when(sub3Category1Mock.getProducts()).thenReturn(Arrays.asList(sub3Category1ProductMock1, sub3Category1ProductMock2));

		when(sub3Category1Mock.getAllSubcategories()).thenReturn(Arrays.asList(sub4Category1Mock, sub4Category2Mock));
		when(sub4Category1Mock.getProducts()).thenReturn(Arrays.asList(sub4Category1ProductMock1, sub4Category1ProductMock2));
		when(sub4Category2Mock.getProducts()).thenReturn(Arrays.asList(sub4Category2ProductMock1, sub4Category2ProductMock2));

		categoryDescendantProductMocks = Arrays.asList(
				categoryProductMock1, categoryProductMock2,
				sub1Category1ProductMock1, sub1Category1ProductMock2, sub1Category2ProductMock1, sub1Category2ProductMock2,
				sub2Category1ProductMock1, sub2Category1ProductMock2,
				sub3Category1ProductMock1, sub3Category1ProductMock2,
				sub4Category1ProductMock1, sub4Category1ProductMock2, sub4Category2ProductMock1, sub4Category2ProductMock2);

		categoryChildProductMocks = Arrays.asList(categoryProductMock1, categoryProductMock2);
		categoryGrandChildProductMocks = Arrays.asList(
				sub1Category1ProductMock1, sub1Category1ProductMock2, sub1Category2ProductMock1, sub1Category2ProductMock2,
				sub2Category1ProductMock1, sub2Category1ProductMock2,
				sub3Category1ProductMock1, sub3Category1ProductMock2,
				sub4Category1ProductMock1, sub4Category1ProductMock2, sub4Category2ProductMock1, sub4Category2ProductMock2);
	}

	@Test
	public void shouldNotMarkDescendantProductsIfNoSolrRelatedPropertiesModifiedInCategory() throws Exception
	{
		// given
		setUpCategoryStructure();
		when(interceptorContextMock.isModified(categoryModelMock, CATEGORY_PROPERTY_OTHER)).thenReturn(Boolean.TRUE);

		// when
		testInstance.onPrepare(categoryModelMock, interceptorContextMock);

		// then
		verifyNoProductsMarked(categoryDescendantProductMocks);
	}

	@Test
	public void shouldMarkDescendantProductsIfAnySolrRelatedPropertiesModifiedInCategory() throws Exception
	{
		// given
		setUpCategoryStructure();
		when(interceptorContextMock.isModified(categoryModelMock, CATEGORY_PROPERTY_2)).thenReturn(Boolean.TRUE);

		// when
		testInstance.onPrepare(categoryModelMock, interceptorContextMock);

		// then
		verifyAllProductsMarked(categoryDescendantProductMocks);
	}

	@Test
	public void shouldNotMarkDescendantProductsIfOnlyNotCategorySolrRelatedPropertiesModifiedInClassificationClass()
			throws Exception
	{
		// given
		setUpClassificationClassStructure();
		when(interceptorContextMock.isModified(classificationClassModelMock, CATEGORY_PROPERTY_OTHER)).thenReturn(Boolean.TRUE);

		// when
		testInstance.onPrepare(classificationClassModelMock, interceptorContextMock);

		// then
		verifyNoProductsMarked(classificationClassDescendantProductMocks);
	}

	@Test
	public void shouldNotMarkDescendantProductsIfOnlyNotClassificationClassSolrRelatedPropertiesModifiedInClassificationClass()
			throws Exception
	{
		// given
		setUpClassificationClassStructure();
		when(interceptorContextMock.isModified(classificationClassModelMock, CLASSIFICATION_CLASS_PROPERTY_OTHER)).thenReturn(
				Boolean.TRUE);

		// when
		testInstance.onPrepare(classificationClassModelMock, interceptorContextMock);

		// then
		verifyNoProductsMarked(classificationClassDescendantProductMocks);
	}

	@Test
	public void shouldMarkChildProductsIfAnyCategorySolrRelatedPropertiesModifiedInClassificationClass() throws Exception
	{
		// given
		setUpClassificationClassStructure();
		when(interceptorContextMock.isModified(classificationClassModelMock, CATEGORY_PROPERTY_2)).thenReturn(Boolean.TRUE);

		// when
		testInstance.onPrepare(classificationClassModelMock, interceptorContextMock);

		// then
		verifyAllProductsMarked(classificationClassChildProductMocks);
		verifyNoProductsMarked(classificationClassGrandChildProductMocks);
	}

	@Test
	public void shouldMarkChildProductsIfAnyClassificationClassSolrRelatedPropertiesModifiedInClassificationClass()
			throws Exception
	{
		// given
		setUpClassificationClassStructure();
		when(interceptorContextMock.isModified(classificationClassModelMock, CLASSIFICATION_CLASS_PROPERTY_2)).thenReturn(
				Boolean.TRUE);

		// when
		testInstance.onPrepare(classificationClassModelMock, interceptorContextMock);

		// then
		verifyAllProductsMarked(classificationClassChildProductMocks);
		verifyNoProductsMarked(classificationClassGrandChildProductMocks);
	}

	@Test
	public void shouldNotFailIfCategorySubcategoriesAreNull() throws InterceptorException
	{
		// given
		setUpCategoryStructure();
		when(interceptorContextMock.isModified(categoryModelMock, CATEGORY_PROPERTY_1)).thenReturn(Boolean.TRUE);
		when(categoryModelMock.getAllSubcategories()).thenReturn(null);

		// when
		testInstance.onPrepare(categoryModelMock, interceptorContextMock);

		// then
		verifyAllProductsMarked(categoryChildProductMocks);
		verifyNoProductsMarked(categoryGrandChildProductMocks);
	}

	@Test
	public void shouldNotFailIfCategoryProductsAreNull() throws InterceptorException
	{
		// given
		setUpCategoryStructure();
		when(interceptorContextMock.isModified(categoryModelMock, CATEGORY_PROPERTY_1)).thenReturn(Boolean.TRUE);

		when(categoryModelMock.getProducts()).thenReturn(null);

		// when
		testInstance.onPrepare(categoryModelMock, interceptorContextMock);

		// then
		verifyAllProductsMarked(categoryGrandChildProductMocks);
		verifyNoProductsMarked(categoryChildProductMocks);
	}

	@Test
	public void shouldNotFailIfClassificationClassSubcategoriesAreNull() throws InterceptorException
	{
		// given
		setUpClassificationClassStructure();
		when(interceptorContextMock.isModified(classificationClassModelMock, CLASSIFICATION_CLASS_PROPERTY_1)).thenReturn(
				Boolean.TRUE);
		when(classificationClassModelMock.getAllSubcategories()).thenReturn(null);

		// when
		testInstance.onPrepare(classificationClassModelMock, interceptorContextMock);

		// then
		verifyAllProductsMarked(classificationClassChildProductMocks);
		verifyNoProductsMarked(classificationClassGrandChildProductMocks);
	}

	@Test
	public void shouldNotFailIfClassificationClassProductsAreNull() throws InterceptorException
	{
		// given
		setUpClassificationClassStructure();
		when(interceptorContextMock.isModified(classificationClassModelMock, CLASSIFICATION_CLASS_PROPERTY_1)).thenReturn(
				Boolean.TRUE);
		when(classificationClassModelMock.getProducts()).thenReturn(null);

		// when
		testInstance.onPrepare(classificationClassModelMock, interceptorContextMock);

		// then
		verifyNoProductsMarked(classificationClassGrandChildProductMocks);
		verifyNoProductsMarked(classificationClassChildProductMocks);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldFailFastIfCategoryIsNull() throws InterceptorException
	{
		testInstance.onPrepare(null, interceptorContextMock);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldFailFastIfContextIsNull() throws InterceptorException
	{
		testInstance.onPrepare(categoryModelMock, null);
	}

	private void verifyNoProductsMarked(final List<ProductModel> products)
	{
		products.forEach(productMock ->
				{
					verify(productMock, never()).setModifiedtime(any());
					verify(interceptorContextMock, never()).registerElementFor(same(productMock), any());
				}
		);
	}

	private void verifyAllProductsMarked(final List<ProductModel> products)
	{
		products.forEach(productMock ->
				{
					verify(productMock).setModifiedtime(currentDateMock);
					verify(interceptorContextMock).registerElementFor(productMock, PersistenceOperation.SAVE);
				}
		);
	}
}
