package com.wiley.core.price.impl;

import com.wiley.core.model.WileyProductVariantSetModel;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.product.PriceService;
import de.hybris.platform.util.PriceValue;
import de.hybris.platform.variants.model.VariantProductModel;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyCommercePriceServiceImplTest
{
	private static final String MINQTD_QUALIFIER = "minqtd";
	private static final String USD = "USD";
	private static final double PROD_ONE_PRICE = 10.1d;
	private static final double PROD_TWO_PRICE = 10.0d;
	private static final double PROD_SET_PRICE = 50.0d;

	@Mock
	private PriceService mockPriceService;

	@InjectMocks
	private WileyCommercePriceServiceImpl underTest;

	@Mock
	private ProductModel mockProduct;
	@Mock
	private VariantProductModel mockVariantOne;
	@Mock
	private VariantProductModel mockVariantTwo;
	@Mock
	private WileyProductVariantSetModel mockVariantSet;
	@Mock
	private PriceInformation mockPriceInformationOne;
	@Mock
	private PriceInformation mockPriceInformationTwo;
	@Mock
	private PriceInformation mockPriceInformationSet;


	@Before
	public void setup() {
		when(mockProduct.getVariants()).thenReturn(Arrays.asList(mockVariantOne, mockVariantSet, mockVariantTwo));
		setupPriceForProduct(mockVariantOne, mockPriceInformationOne, PROD_ONE_PRICE);
		setupPriceForProduct(mockVariantTwo, mockPriceInformationTwo, PROD_TWO_PRICE);
		setupPriceForProduct(mockVariantSet, mockPriceInformationSet, PROD_SET_PRICE);
	}

	private void setupPriceForProduct(final VariantProductModel product, final PriceInformation priceInformation,
									  final double price)
	{
		//this is needed for OOTB getWebPriceForProduct to work
		when(priceInformation.getQualifierValue(MINQTD_QUALIFIER)).thenReturn(Long.MAX_VALUE);
		PriceValue mockPriceValue = mock(PriceValue.class);
		when(mockPriceValue.getValue()).thenReturn(price);
		when(mockPriceValue.getCurrencyIso()).thenReturn(USD);
		when(priceInformation.getPriceValue()).thenReturn(mockPriceValue);
		when(mockPriceService.getPriceInformationsForProduct(product)).thenReturn(Arrays.asList(priceInformation));
	}

	@Test
	public void shouldReturnStartingAtPriceWhenSetPriceIgnored() {
		PriceInformation startingAtPrice = underTest.getStartingAtPrice(mockProduct, true);
		assertSame(mockPriceInformationTwo, startingAtPrice);
	}

	@Test
	public void shouldReturnStartingAtSetPriceWhenSetPriceIsNotIgnored() {
		setupPriceForProduct(mockVariantSet, mockPriceInformationSet, 1.0d);
		PriceInformation startingAtPrice = underTest.getStartingAtPrice(mockProduct, false);
		assertSame(mockPriceInformationSet, startingAtPrice);
	}

	@Test
	public void shouldNotReturnStartingAtPriceWhenAllPricesAreEqual() {
		setupPriceForProduct(mockVariantTwo, mockPriceInformationTwo, PROD_ONE_PRICE);
		PriceInformation startingAtPrice = underTest.getStartingAtPrice(mockProduct, true);
		assertNull(startingAtPrice);
	}

	@Test
	public void shouldConsiderNullPricesAsAbsentPrices() {
		when(mockPriceService.getPriceInformationsForProduct(mockVariantTwo)).thenReturn(null);

		PriceInformation startingAtPrice = underTest.getStartingAtPrice(mockProduct, false);
		assertSame(mockPriceInformationOne, startingAtPrice);
	}

	@Test
	public void shouldReturnNullWhenProductHasNoVariants() {
		when(mockProduct.getVariants()).thenReturn(Arrays.asList());
		PriceInformation startingAtPrice = underTest.getStartingAtPrice(mockProduct, false);
		assertNull(startingAtPrice);
	}
}
