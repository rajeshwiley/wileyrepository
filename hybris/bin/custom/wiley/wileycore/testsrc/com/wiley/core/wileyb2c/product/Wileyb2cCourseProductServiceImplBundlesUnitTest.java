package com.wiley.core.wileyb2c.product;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.catalog.references.ProductReferenceService;
import de.hybris.platform.core.model.product.ProductModel;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.enums.WileyProductSubtypeEnum;


/**
 * Test for {@link Wileyb2cCourseProductServiceImpl#getReferencedBundleProducts(ProductModel, ProductModel)}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cCourseProductServiceImplBundlesUnitTest
{
	private static final boolean ACTIVE_REFERENCES_ONLY = true;

	@InjectMocks
	private Wileyb2cCourseProductServiceImpl testInstance;

	@Mock
	private ProductReferenceService productReferenceServiceMock;

	@Mock
	private ProductModel courseProductMock;

	@Mock
	private ProductModel eprofProductMock;

	@Mock
	private ProductModel bundleProductMock;

	@Before
	public void setup()
	{

		when(courseProductMock.getSubtype()).thenReturn(WileyProductSubtypeEnum.COURSE);
		when(eprofProductMock.getSubtype()).thenReturn(WileyProductSubtypeEnum.COURSE);
		// TODO : [ECSC-20075] commented out code which uses obsolete attribute. Should be deleted
//		when(eprofProductMock.getPurchasable()).thenReturn(false);

		ProductReferenceModel eprofProductReference = new ProductReferenceModel();
		eprofProductReference.setTarget(bundleProductMock);

		when(productReferenceServiceMock
				.getProductReferencesForSourceProduct(eprofProductMock, ProductReferenceTypeEnum.WILEY_PLUS_PRODUCT_SET,
						ACTIVE_REFERENCES_ONLY)).thenReturn(Collections.singletonList(eprofProductReference));
	}

	@Test(expected = NullPointerException.class)
	public void shouldThrowNullPointerExceptionDueToNullCourseProductParameter()
	{
		//When
		testInstance.getReferencedBundleProducts(eprofProductMock, null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowIllegalArgumentExceptionDueToCourseProductSubtypeIsNotCourse()
	{
		//Given
		when(courseProductMock.getSubtype()).thenReturn(WileyProductSubtypeEnum.HARDCOVER);

		//When
		testInstance.getReferencedBundleProducts(eprofProductMock, courseProductMock);
	}

	@Test(expected = NullPointerException.class)
	public void shouldThrowNullPointerExceptionDueToNullEprofProductParameter()
	{
		//When
		testInstance.getReferencedBundleProducts(null, courseProductMock);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowIllegalArgumentExceptionDueToEprofProductSubtypeIsNotCourse()
	{
		//Given
		when(eprofProductMock.getSubtype()).thenReturn(WileyProductSubtypeEnum.HARDCOVER);

		//When
		testInstance.getReferencedBundleProducts(eprofProductMock, courseProductMock);
	}

	@Ignore("This test method should be deleted as it uses obsolete field")
	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowIllegalArgumentExceptionDueToEprofProductIsPurchasable()
	{
		//Given
		// TODO : [ECSC-20075] commented out code which uses obsolete attribute. Should be deleted
//		when(eprofProductMock.getPurchasable()).thenReturn(true);

		//When
		testInstance.getReferencedBundleProducts(eprofProductMock, courseProductMock);
	}

	@Test
	public void shouldReturnEmptyListDueToNullReferenceList()
	{
		//Given
		when(productReferenceServiceMock
				.getProductReferencesForSourceProduct(eprofProductMock, ProductReferenceTypeEnum.WILEY_PLUS_PRODUCT_SET,
						ACTIVE_REFERENCES_ONLY)).thenReturn(null);

		//When
		List<ProductModel> bundleProducts = testInstance.getReferencedBundleProducts(eprofProductMock,
				courseProductMock
		);

		//Then
		assertNotNull(bundleProducts);
		assertTrue(bundleProducts.isEmpty());
	}


	@Test
	public void shouldReturnEmptyListDueToEmptyReferenceList()
	{
		//Given
		when(productReferenceServiceMock
				.getProductReferencesForSourceProduct(eprofProductMock, ProductReferenceTypeEnum.WILEY_PLUS_PRODUCT_SET,
						ACTIVE_REFERENCES_ONLY)).thenReturn(Collections.emptyList());

		//When
		List<ProductModel> bundleProducts = testInstance.getReferencedBundleProducts(eprofProductMock,
				courseProductMock
		);

		//Then
		assertNotNull(bundleProducts);
		assertTrue(bundleProducts.isEmpty());
	}

	@Test
	public void shouldReturnNoBundleDueToNoValidReference()
	{
		//Given
		when(productReferenceServiceMock
				.getProductReferencesForSourceAndTarget(bundleProductMock, courseProductMock,
						ACTIVE_REFERENCES_ONLY)).thenReturn(Collections.singletonList(mock(ProductReferenceModel.class)));

		//When
		List<ProductModel> bundleProducts = testInstance.getReferencedBundleProducts(eprofProductMock,
				courseProductMock
		);

		//Then
		assertNotNull(bundleProducts);
		assertTrue(bundleProducts.isEmpty());
	}

	@Test
	public void shouldReturnValidatedBundleProducts()
	{
		//Given
		ProductReferenceModel bundleReference = new ProductReferenceModel();
		bundleReference.setReferenceType(ProductReferenceTypeEnum.PRODUCT_SET_COMPONENT);

		when(productReferenceServiceMock
				.getProductReferencesForSourceAndTarget(bundleProductMock, courseProductMock,
						ACTIVE_REFERENCES_ONLY)).thenReturn(Collections.singletonList(bundleReference));


		//When
		List<ProductModel> bundleProducts = testInstance.getReferencedBundleProducts(eprofProductMock,
				courseProductMock
		);

		//Then
		assertEquals(Collections.singletonList(bundleProductMock), bundleProducts);
	}

}
