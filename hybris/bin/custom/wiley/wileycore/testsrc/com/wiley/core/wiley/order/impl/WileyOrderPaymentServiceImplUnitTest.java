package com.wiley.core.wiley.order.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyDouble;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.enums.PaymentModeEnum;
import com.wiley.core.order.PaymentActonType;
import com.wiley.core.order.PendingPaymentActon;
import com.wiley.core.payment.transaction.impl.WileyPaymentTransactionService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyOrderPaymentServiceImplUnitTest
{
	public static final String USD = "USD";
	public static final String EUR = "EUR";
	private static final int CURRENCY_DIGITS = 2;
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");
	private static final double DELTA = 0.001;
	@Mock
	private CommonI18NService mockCommonI18NService;
	@Spy
	private WileyPaymentTransactionService spyWileyPaymentTransactionService;
	@InjectMocks
	private WileyOrderPaymentServiceImpl underTest = new WileyOrderPaymentServiceImpl();
	@Mock
	private WileyOrderHistoryServiceImpl mockWileyOrderHistoryService;

	@Before
	public void setup()
	{
		doAnswer(invocationOnMock -> invocationOnMock.getArguments()[0])
				.when(mockCommonI18NService).roundCurrency(anyDouble(), eq(CURRENCY_DIGITS));
	}


	@Test
	public void shouldReturnAuthForNewOrderAndNoAuth()
	{
		OrderModel order = createOrder(100.0d, 5.0d, USD, PaymentModeEnum.PAYPAL);

		Collection<PendingPaymentActon> actions = underTest.getPendingPaymentActions(order);
		assertEquals(1, actions.size());
		assertHasAction(PaymentActonType.AUTHORIZE, 105.0d, null, actions);
	}


	@Test
	public void shouldReturnCaptureForNewOrderAndAcceptedAuth()
	{
		OrderModel order = createOrder(100.0d, 5.0d, USD, PaymentModeEnum.CARD);

		PaymentTransactionModel transaction = attachTransaction(order,
				createTransactionEntry(new BigDecimal("105.00"),
						USD, PaymentTransactionType.AUTHORIZATION,
						TransactionStatus.ACCEPTED));

		Collection<PendingPaymentActon> actions = underTest.getPendingPaymentActions(order);
		assertEquals(1, actions.size());
		assertHasAction(PaymentActonType.CAPTURE, 105.0d, transaction, actions);
	}

	@Test
	public void shouldReturnNothingForNewOrderAndAcceptedCapture()
	{
		OrderModel order = createOrder(100.0d, 5.0d, USD, PaymentModeEnum.CARD);

		PaymentTransactionModel transaction = attachTransaction(order,
				createTransactionEntry(new BigDecimal("105.00"),
						USD, PaymentTransactionType.CAPTURE,
						TransactionStatus.ACCEPTED),
				createTransactionEntry(new BigDecimal("105.00"),
						USD, PaymentTransactionType.AUTHORIZATION,
						TransactionStatus.ACCEPTED));

		Collection<PendingPaymentActon> actions = underTest.getPendingPaymentActions(order);
		assertEquals(0, actions.size());
	}

	@Test
	public void shouldReturnCardAuthForOnlyChangedAmountAndNoAuth()
	{
		OrderModel order = createOrder(100.0d, 5.0d, USD, PaymentModeEnum.CARD);
		mockOrderSnapshot(order, createOrder(90.0d, 5.0d, USD, PaymentModeEnum.CARD));

		Collection<PendingPaymentActon> actions = underTest.getPendingPaymentActions(order);
		assertEquals(1, actions.size());
		assertHasAction(PaymentActonType.AUTHORIZE, 10.0d, null, actions);
	}

	@Test
	public void shouldReturnCardCaptureForOnlyChangedAmountAndAcceptedAuth()
	{
		OrderModel order = createOrder(100.0d, 5.0d, USD, PaymentModeEnum.CARD);
		mockOrderSnapshot(order, createOrder(90.0d, 5.0d, USD, PaymentModeEnum.CARD));
		PaymentTransactionModel transaction = attachTransaction(order,
				createTransactionEntry(new BigDecimal("10.00"),
						USD, PaymentTransactionType.AUTHORIZATION,
						TransactionStatus.ACCEPTED));
		Collection<PendingPaymentActon> actions = underTest.getPendingPaymentActions(order);
		assertEquals(1, actions.size());
		assertHasAction(PaymentActonType.CAPTURE, 10.0d, transaction, actions);
	}

	@Test
	public void shouldReturnNothingForOnlyChangedAmountAndAcceptedCapture()
	{
		OrderModel order = createOrder(100.0d, 5.0d, USD, PaymentModeEnum.CARD);
		mockOrderSnapshot(order, createOrder(90.0d, 5.0d, USD, PaymentModeEnum.CARD));
		PaymentTransactionModel transaction = attachTransaction(order,
				createTransactionEntry(new BigDecimal("10.00"),
						USD, PaymentTransactionType.AUTHORIZATION,
						TransactionStatus.ACCEPTED),
				createTransactionEntry(new BigDecimal("10.00"),
						USD, PaymentTransactionType.CAPTURE,
						TransactionStatus.ACCEPTED)
		);
		Collection<PendingPaymentActon> actions = underTest.getPendingPaymentActions(order);
		assertEquals(0, actions.size());
	}


	@Test
	public void shouldReturnCardRefundForOnlyChangedAmountAndAcceptedCapture()
	{
		OrderModel order = createOrder(90.0d, 5.0d, USD, PaymentModeEnum.CARD);
		mockOrderSnapshot(order, createOrder(120.0d, 5.0d, USD, PaymentModeEnum.CARD));
		PaymentTransactionModel transaction = attachTransaction(order,
				createTransactionEntry(new BigDecimal("90.00"),
						USD, PaymentTransactionType.AUTHORIZATION,
						TransactionStatus.ACCEPTED),
				createTransactionEntry(new BigDecimal("90.00"),
						USD, PaymentTransactionType.CAPTURE,
						TransactionStatus.ACCEPTED)
		);
		Collection<PendingPaymentActon> actions = underTest.getPendingPaymentActions(order);
		assertEquals(1, actions.size());
		assertHasAction(PaymentActonType.REFUND, 30.0d, transaction, actions);
	}

	@Test
	public void shouldReturnFullRefundAndFullChargeOnCurrencyChange()
	{
		OrderModel order = createOrder(90.0d, 5.0d, EUR, PaymentModeEnum.CARD);
		mockOrderSnapshot(order, createOrder(120.0d, 5.0d, USD, PaymentModeEnum.CARD));
		PaymentTransactionModel transaction = attachTransaction(order,
				createTransactionEntry(new BigDecimal("125.00"),
						USD, PaymentTransactionType.AUTHORIZATION,
						TransactionStatus.ACCEPTED),
				createTransactionEntry(new BigDecimal("125.00"),
						USD, PaymentTransactionType.CAPTURE,
						TransactionStatus.ACCEPTED)
		);
		Collection<PendingPaymentActon> actions = underTest.getPendingPaymentActions(order);
		assertEquals(2, actions.size());
		assertHasAction(PaymentActonType.REFUND, 125.0d, transaction, actions);
		assertHasAction(PaymentActonType.AUTHORIZE, 95.0d, null, actions);

	}

	@Test
	public void shouldReturnFullRefundAndFullChargeOnCardChange()
	{
		CreditCardPaymentInfoModel paymentInfo = createCreditCardPaymentInfo("newSubscriptionId");
		OrderModel order = createOrder(90.0d, 5.0d, USD, PaymentModeEnum.CARD, paymentInfo);

		CreditCardPaymentInfoModel paymentInfoSnapshot = createCreditCardPaymentInfo("oldSubscriptionId");
		mockOrderSnapshot(order, createOrder(90.0d, 5.0d, USD, PaymentModeEnum.CARD, paymentInfoSnapshot));

		PaymentTransactionModel transactionForOldCard = attachTransaction(order,
				createTransactionEntry(new BigDecimal("95.00"),
						USD, PaymentTransactionType.AUTHORIZATION,
						TransactionStatus.ACCEPTED),
				createTransactionEntry(new BigDecimal("95.00"),
						USD, PaymentTransactionType.CAPTURE,
						TransactionStatus.ACCEPTED)
		);


		PaymentTransactionModel transactionForNewCard = attachTransaction(order,
				createTransactionEntry(new BigDecimal("95.00"),
						USD, PaymentTransactionType.AUTHORIZATION,
						TransactionStatus.ACCEPTED)
		);

		Collection<PendingPaymentActon> actions = underTest.getPendingPaymentActions(order);
		assertEquals(2, actions.size());
		assertHasAction(PaymentActonType.REFUND, 95.0d, transactionForOldCard, actions);
		assertHasAction(PaymentActonType.CAPTURE, 95.0d, transactionForNewCard, actions);
	}

	@Test
	public void shouldFilterPendingPaymentActionsCorrectly()
	{
		OrderModel order = createOrder(90.0d, 5.0d, EUR, PaymentModeEnum.CARD);
		mockOrderSnapshot(order, createOrder(120.0d, 5.0d, USD, PaymentModeEnum.CARD));
		PaymentTransactionModel transaction = attachTransaction(order,
				createTransactionEntry(new BigDecimal("125.00"),
						USD, PaymentTransactionType.AUTHORIZATION,
						TransactionStatus.ACCEPTED),
				createTransactionEntry(new BigDecimal("125.00"),
						USD, PaymentTransactionType.CAPTURE,
						TransactionStatus.ACCEPTED)
		);
		Collection<PendingPaymentActon> actions = underTest.getPendingPaymentActions(order, PaymentActonType.REFUND);
		assertEquals(1, actions.size());
		assertHasAction(PaymentActonType.REFUND, 125.0d, transaction, actions);

		actions = underTest.getPendingPaymentActions(order, PaymentActonType.AUTHORIZE);
		assertEquals(1, actions.size());
		assertHasAction(PaymentActonType.AUTHORIZE, 95.0d, null, actions);
	}

	@Test
	public void shouldReturnFullRefundAndFullChargeOnPaymentModeCartToInvoiceChange()
	{
		testReturnFullRefundAndFullChargeOnPaymentModeChange(PaymentModeEnum.INVOICE, PaymentModeEnum.CARD);
	}

	@Test
	public void shouldReturnFullRefundOnPaymentModeCartToProformaChange()
	{
		testReturnFullRefundAndFullChargeOnPaymentModeChange(PaymentModeEnum.PROFORMA, PaymentModeEnum.CARD);
	}

	@Test
	public void shouldReturnFullRefundOnPaymentModePayPalToInvoiceChange()
	{
		testReturnFullRefundAndFullChargeOnPaymentModeChange(PaymentModeEnum.INVOICE, PaymentModeEnum.PAYPAL);
	}

	@Test
	public void shouldReturnFullRefundOnPaymentModePayPalToProformaChange()
	{
		testReturnFullRefundAndFullChargeOnPaymentModeChange(PaymentModeEnum.PROFORMA, PaymentModeEnum.PAYPAL);
	}

	@Test
	public void shouldReturnFullRefundAndFullChargeOnPaymentModePayPalToCartChange()
	{
		testReturnFullRefundAndFullChargeOnPaymentModeChange(PaymentModeEnum.CARD, PaymentModeEnum.PAYPAL);
	}

	private void testReturnFullRefundAndFullChargeOnPaymentModeChange(final PaymentModeEnum paymentModeTo,
			final PaymentModeEnum paymentModeFrom)
	{
		OrderModel order = createOrder(90.0d, 5.0d, USD, paymentModeTo);
		mockOrderSnapshot(order, createOrder(120.0d, 5.0d, USD, paymentModeFrom));
		PaymentTransactionModel transaction = attachTransaction(order,
				createTransactionEntry(new BigDecimal("125.00"),
						USD, PaymentTransactionType.AUTHORIZATION,
						TransactionStatus.ACCEPTED),
				createTransactionEntry(new BigDecimal("125.00"),
						USD, PaymentTransactionType.CAPTURE,
						TransactionStatus.ACCEPTED)
		);
		Collection<PendingPaymentActon> actions = underTest.getPendingPaymentActions(order);
		assertEquals(2, actions.size());
		assertHasAction(PaymentActonType.REFUND, 125.0d, transaction, actions);
		assertHasAction(PaymentActonType.AUTHORIZE, 95.0d, null, actions);
	}

	@Test
	public void shouldAddRefundActionInManyTransactions()
	{
		OrderModel order = createOrder(90.0d, 5.0d, USD, PaymentModeEnum.CARD);
		mockOrderSnapshot(order, createOrder(115.0d, 5.0d, USD, PaymentModeEnum.PAYPAL));
		PaymentTransactionModel transactionOne = attachTransaction(order,
				createTransactionEntry(new BigDecimal("10.00"),
						USD, PaymentTransactionType.AUTHORIZATION,
						TransactionStatus.ACCEPTED),
				createTransactionEntry(new BigDecimal("10.00"),
						USD, PaymentTransactionType.CAPTURE,
						TransactionStatus.ACCEPTED)
		);

		PaymentTransactionModel transactionTwo = attachTransaction(order,
				createTransactionEntry(new BigDecimal("110.00"),
						EUR, PaymentTransactionType.AUTHORIZATION,
						TransactionStatus.ACCEPTED),
				createTransactionEntry(new BigDecimal("110.00"),
						EUR, PaymentTransactionType.CAPTURE,
						TransactionStatus.ACCEPTED)
		);
		Collection<PendingPaymentActon> actions = underTest.getPendingPaymentActions(order);
		assertEquals(3, actions.size());
		assertHasAction(PaymentActonType.REFUND, 110.0d, transactionTwo, actions);
		assertHasAction(PaymentActonType.REFUND, 10.0d, transactionOne, actions);
		assertHasAction(PaymentActonType.AUTHORIZE, 95.0d, null, actions);

	}

	@Test(expected = IllegalStateException.class)
	public void shouldFailForRefundInManyTransactionsWhenNotEnoughCaptured()
	{
		OrderModel order = createOrder(90.0d, 5.0d, USD, PaymentModeEnum.CARD);
		mockOrderSnapshot(order, createOrder(115.0d, 5.0d, USD, PaymentModeEnum.PAYPAL));
		PaymentTransactionModel transactionOne = attachTransaction(order,
				createTransactionEntry(new BigDecimal("10.00"),
						USD, PaymentTransactionType.AUTHORIZATION,
						TransactionStatus.ACCEPTED),
				createTransactionEntry(new BigDecimal("10.00"),
						USD, PaymentTransactionType.CAPTURE,
						TransactionStatus.ACCEPTED)
		);

		PaymentTransactionModel transactionTwo = attachTransaction(order,
				createTransactionEntry(new BigDecimal("109.00"),
						USD, PaymentTransactionType.AUTHORIZATION,
						TransactionStatus.ACCEPTED),
				createTransactionEntry(new BigDecimal("109.00"),
						USD, PaymentTransactionType.CAPTURE,
						TransactionStatus.ACCEPTED)
		);
		Collection<PendingPaymentActon> actions = underTest.getPendingPaymentActions(order);
		assertEquals(3, actions.size());
		assertHasAction(PaymentActonType.REFUND, 110.0d, transactionTwo, actions);
		assertHasAction(PaymentActonType.REFUND, 10.0d, transactionOne, actions);
		assertHasAction(PaymentActonType.AUTHORIZE, 95.0d, null, actions);

	}

	@Test
	public void shouldNotRefundInvoiceOnPaymentChange()
	{
		OrderModel order = createOrder(90.0d, 5.0d, USD, PaymentModeEnum.CARD);
		mockOrderSnapshot(order, createOrder(90.0d, 5.0d, USD, PaymentModeEnum.INVOICE));
		Collection<PendingPaymentActon> actions = underTest.getPendingPaymentActions(order);
		assertEquals(1, actions.size());
		assertHasAction(PaymentActonType.AUTHORIZE, 95.0d, null, actions);
	}

	@Test
	public void shouldNotRefundInvoiceWhenPaymentModeDoesNotChange()
	{
		OrderModel order = createOrder(80.0d, 5.0d, USD, PaymentModeEnum.INVOICE);
		mockOrderSnapshot(order, createOrder(90.0d, 5.0d, USD, PaymentModeEnum.INVOICE));
		Collection<PendingPaymentActon> actions = underTest.getPendingPaymentActions(order);
		assertEquals(1, actions.size());
		assertHasAction(PaymentActonType.AUTHORIZE, 85.0d, null, actions);
	}


	private void assertHasAction(final PaymentActonType type, final double amount,
			final PaymentTransactionModel transaction,
			final Collection<PendingPaymentActon> actions)
	{
		StringBuilder builder = new StringBuilder();
		for (PendingPaymentActon action : actions)
		{
			if (isExpectedAction(action, type, amount, transaction))
			{
				return;
			}
			builder.append("\n").append(ToStringBuilder.reflectionToString(action, ToStringStyle.SIMPLE_STYLE));
		}

		fail(String.format("Expected action is not found. Visited actions: %s", builder.toString()));
	}

	private boolean isExpectedAction(final PendingPaymentActon action,
			final PaymentActonType type,
			final double amount,
			final PaymentTransactionModel transaction)
	{
		return action.getAction() == type && ((action.getAmount() - amount) <= DELTA)
				&& action.getTransaction() == transaction;
	}

	private PaymentTransactionEntryModel createTransactionEntry(final BigDecimal amount,
			final String currencyIso,
			final PaymentTransactionType type,
			final TransactionStatus status)
	{
		PaymentTransactionEntryModel entry = mock(PaymentTransactionEntryModel.class);
		when(entry.getAmount()).thenReturn(amount);
		when(entry.getTransactionStatus()).thenReturn(status.name());
		CurrencyModel currencyModel = mock(CurrencyModel.class);
		when(currencyModel.getIsocode()).thenReturn(currencyIso);
		when(entry.getCurrency()).thenReturn(currencyModel);
		when(entry.getType()).thenReturn(type);
		return entry;
	}

	private PaymentTransactionModel attachTransaction(final OrderModel order,
			final PaymentTransactionEntryModel... entryModels)
	{
		PaymentTransactionModel transactionModel = mock(PaymentTransactionModel.class);
		when(transactionModel.getEntries()).thenReturn(Arrays.asList(entryModels));
		when(transactionModel.getOrder()).thenReturn(order);
		when(transactionModel.getCreationtime()).thenReturn(new Date());
		order.getPaymentTransactions().add(transactionModel);
		return transactionModel;
	}

	private void mockOrderSnapshot(final OrderModel mainOrder, final OrderModel snapshot)
	{
		when(mockWileyOrderHistoryService.getPreviousOrderVersion(mainOrder)).thenReturn(snapshot);
	}

	private OrderModel createOrder(final double total, final double tax, final String currencyIso,
			final PaymentModeEnum paymentMode)
	{
		OrderModel order = mock(OrderModel.class);
		when(order.getTotalPrice()).thenReturn(total);
		when(order.getTotalTax()).thenReturn(tax);
		PaymentModeModel paymentModeModel = mock(PaymentModeModel.class);
		when(paymentModeModel.getCode()).thenReturn(paymentMode.getCode());
		when(order.getPaymentMode()).thenReturn(paymentModeModel);

		CurrencyModel currencyModel = mock(CurrencyModel.class);
		when(currencyModel.getIsocode()).thenReturn(currencyIso);
		when(currencyModel.getDigits()).thenReturn(CURRENCY_DIGITS);
		when(order.getCurrency()).thenReturn(currencyModel);
		when(order.getHistoryEntries()).thenReturn(new ArrayList<>());
		when(order.getPaymentTransactions()).thenReturn(new ArrayList<>());

		return order;
	}

	private CreditCardPaymentInfoModel createCreditCardPaymentInfo(final String subscriptionId)
	{
		final CreditCardPaymentInfoModel paymentInfo = mock(CreditCardPaymentInfoModel.class);
		when(paymentInfo.getSubscriptionId()).thenReturn(subscriptionId);
		return paymentInfo;
	}

	private OrderModel createOrder(final double total, final double tax, final String currencyIso,
			final PaymentModeEnum paymentMode, final PaymentInfoModel paymentInfo)
	{
		final OrderModel order = createOrder(total, tax, currencyIso, paymentMode);
		when(order.getPaymentInfo()).thenReturn(paymentInfo);
		return order;
	}

}
