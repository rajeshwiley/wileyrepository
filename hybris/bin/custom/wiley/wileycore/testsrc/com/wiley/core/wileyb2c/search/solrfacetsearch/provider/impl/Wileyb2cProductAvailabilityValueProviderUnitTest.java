package com.wiley.core.wileyb2c.search.solrfacetsearch.provider.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.wiley.restriction.WileyProductRestrictionService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cProductAvailabilityValueProviderUnitTest
{
	@InjectMocks
	private Wileyb2cProductAvailabilityValueProvider provider;

	@Mock
	private WileyProductRestrictionService wileyb2cProductRestrictionService;
	@Mock
	private ProductModel productModel;
	@Mock
	private IndexConfig indexConfig;
	@Mock
	private IndexedProperty indexedProperty;

	@Test
	public void testCollectValues() throws FieldValueProviderException
	{
		when(wileyb2cProductRestrictionService.isAvailable(productModel)).thenReturn(true);

		final List<String> values = provider.collectValues(indexConfig, indexedProperty, productModel);

		assertEquals(values.size(), 1);
		assertEquals(values.get(0), "true");
	}
}
