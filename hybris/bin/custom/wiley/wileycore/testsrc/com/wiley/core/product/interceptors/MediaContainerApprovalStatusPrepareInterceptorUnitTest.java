package com.wiley.core.product.interceptors;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyCollection;
import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.Lists;
import com.wiley.core.product.WileyProductService;


/**
 * Unit test for {@link ProductApprovalStatusPrepareInterceptor}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class MediaContainerApprovalStatusPrepareInterceptorUnitTest
{
	public static final String PRODUCTEDITORGROUP = "producteditorgroup";

	@Mock
	private MediaContainerModel mediaContainerModelMock;
	@Mock
	private UserService userService;
	@Mock
	private UserModel userMock;
	@Mock
	private UserGroupModel productEditorGroupMock;
	@Mock
	private InterceptorContext interceptorContextMock;
	@Mock
	private WileyProductService productServiceMock;
	@Mock
	private ProductModel backgroundImageProduct1;
	@Mock
	private ProductModel backgroundImageProduct2;
	@Mock
	private ProductModel galleryImageProduct1;
	@Mock
	private ProductModel galleryImageProduct2;
	@Mock
	private ModelService modelServiceMock;

	private List<ProductModel> backgroundImageProducts;
	private List<ProductModel> galleryImageProducts;
	private List<ProductModel> allImageProducts;

	@InjectMocks
	private MediaContainerApprovalStatusPrepareInterceptor testInstance;

	@Before
	public void setUp() throws Exception
	{
		when(userService.getCurrentUser()).thenReturn(userMock);
		when(userService.getUserGroupForUID(PRODUCTEDITORGROUP)).thenReturn(productEditorGroupMock);
		backgroundImageProducts = Lists.newArrayList(backgroundImageProduct1, backgroundImageProduct2);
		galleryImageProducts = Lists.newArrayList(galleryImageProduct1, galleryImageProduct2);
		allImageProducts = new ArrayList<>(backgroundImageProducts);
		allImageProducts.addAll(galleryImageProducts);
	}

	@Test
	public void shouldPrepareForProductEditors() throws Exception
	{
		//given
		when(productServiceMock.getProductsForMediaContainerInBackgroundImage(mediaContainerModelMock))
				.thenReturn(backgroundImageProducts);
		when(productServiceMock.getProductsForMediaContainerInGalleryImages(mediaContainerModelMock))
				.thenReturn(galleryImageProducts);
		when(userService.isMemberOfGroup(userMock, productEditorGroupMock)).thenReturn(Boolean.TRUE);
		//when
		testInstance.onPrepare(mediaContainerModelMock, null);
		//then
		allImageProducts.forEach(
				backgroundImageProduct -> verify(backgroundImageProduct).setApprovalStatus(ArticleApprovalStatus.CHECK));
		verify(modelServiceMock).saveAll(allImageProducts);
	}

	@Test
	public void shouldNotPrepareForProductEditorsIfNotModified() throws Exception
	{
		//given
		when(userService.isMemberOfGroup(userMock, productEditorGroupMock)).thenReturn(Boolean.TRUE);
		when(interceptorContextMock.isModified(mediaContainerModelMock)).thenReturn(Boolean.FALSE);
		when(mediaContainerModelMock.getMedias()).thenReturn(Collections.emptyList());
		//when
		testInstance.onPrepare(mediaContainerModelMock, interceptorContextMock);
		//then
		verifyProductsNotUpdated();
	}

	@Test
	public void shouldNotPrepareForProductEditorsIfModifiedButNew() throws Exception
	{
		//given
		when(userService.isMemberOfGroup(userMock, productEditorGroupMock)).thenReturn(Boolean.TRUE);
		when(interceptorContextMock.isModified(mediaContainerModelMock)).thenReturn(Boolean.TRUE);
		when(interceptorContextMock.isNew(mediaContainerModelMock)).thenReturn(Boolean.TRUE);
		//when
		testInstance.onPrepare(mediaContainerModelMock, interceptorContextMock);
		//then
		verifyProductsNotUpdated();
	}

	@Test
	public void shouldNotPrepareForEmptyMediaContainer() throws Exception
	{
		//given
		when(userService.isMemberOfGroup(userMock, productEditorGroupMock)).thenReturn(Boolean.TRUE);
		when(interceptorContextMock.isModified(mediaContainerModelMock)).thenReturn(Boolean.TRUE);
		when(interceptorContextMock.isNew(mediaContainerModelMock)).thenReturn(Boolean.FALSE);
		//when
		testInstance.onPrepare(mediaContainerModelMock, interceptorContextMock);
		//then
		verify(productServiceMock, times(1)).getProductsForMediaContainerInBackgroundImage(any());
		verify(productServiceMock, times(1)).getProductsForMediaContainerInGalleryImages(any());
		verify(modelServiceMock, never()).saveAll(anyCollection());
	}

	@Test
	public void testUpdateMedias() throws Exception
	{
		final List<MediaContainerModel> galeryImages = new ArrayList<>();
		galeryImages.add(mediaContainerModelMock);
		galeryImages.add(mediaContainerModelMock);
		//given
		when(productServiceMock.getProductsForMediaContainerInBackgroundImage(mediaContainerModelMock))
				.thenReturn(backgroundImageProducts);
		when(productServiceMock.getProductsForMediaContainerInGalleryImages(mediaContainerModelMock))
				.thenReturn(galleryImageProducts);
		when(userService.isMemberOfGroup(userMock, productEditorGroupMock)).thenReturn(Boolean.TRUE);
		when(interceptorContextMock.isModified(mediaContainerModelMock)).thenReturn(Boolean.TRUE);
		when(interceptorContextMock.isNew(mediaContainerModelMock)).thenReturn(Boolean.FALSE);
		when(backgroundImageProduct1.getBackgroundImage()).thenReturn(mediaContainerModelMock);
		when(backgroundImageProduct1.getGalleryImages()).thenReturn(galeryImages);

		//when
		testInstance.onPrepare(mediaContainerModelMock, interceptorContextMock);

		//then
		verify(backgroundImageProduct1, atLeastOnce()).setBackgroundImage(mediaContainerModelMock);
		verify(backgroundImageProduct1, atLeastOnce()).setGalleryImages(anyList());
	}

	protected void verifyProductsNotUpdated()
	{
		verify(productServiceMock, never()).getProductsForMediaContainerInBackgroundImage(any());
		verify(productServiceMock, never()).getProductsForMediaContainerInGalleryImages(any());
		verify(modelServiceMock, never()).saveAll(anyCollection());
	}
}
