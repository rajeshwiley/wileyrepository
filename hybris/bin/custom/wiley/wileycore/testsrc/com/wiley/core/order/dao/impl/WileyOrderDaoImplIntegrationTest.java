package com.wiley.core.order.dao.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.variants.model.VariantProductModel;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.enums.WileyProductLifecycleEnum;


@IntegrationTest
public class WileyOrderDaoImplIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	public static final String CATALOG_ID = "welProductCatalog";
	public static final String CATALOG_VERSION = "Online";

	@Resource
	private WileyOrderDaoImpl wileyOrderDao;

	@Resource
	private CatalogVersionService catalogVersionService;

	@Before
	public void setUp() throws Exception
	{
		catalogVersionService.setSessionCatalogVersion(CATALOG_ID, CATALOG_VERSION);
	}

	@Test
	public void testFindPreOrdersWithActiveProducts() throws ImpExException
	{
		importCsv("/wileycore/test/order/WileyOrderDaoImplIntegrationTest/orders.impex", DEFAULT_ENCODING);
		HashSet<String> codes = new HashSet<>(Arrays.asList("TEST_ORDER_1", "TEST_ORDER_2", "TEST_ORDER_3"));

		List<OrderModel> result = wileyOrderDao.findPreOrdersWithActiveProducts();

		assertEquals(3, result.size());
		Set<String> collect = result.stream().map(OrderModel::getCode).collect(Collectors.toSet());
		Assert.assertTrue(collect.containsAll(codes));
	}

	@Test
	public void testFindPreOrdersWithCFAActiveProducts() throws ImpExException
	{
		//given
		importCsv("/wileycore/test/order/WileyOrderDaoImplIntegrationTest/activeCFAOrders.impex", DEFAULT_ENCODING);
		catalogVersionService.setSessionCatalogVersion(CATALOG_ID, CATALOG_VERSION);
		HashSet<String> codes = new HashSet<>(Arrays.asList("TEST_ORDER_10", "TEST_ORDER_11", "TEST_ORDER_12"));
		//when
		List<OrderModel> result = wileyOrderDao.findPreOrdersWithActiveProducts();
		//then
		assertEquals(3, result.size());
		Set<String> collect = result.stream().map(OrderModel::getCode).collect(Collectors.toSet());
		Assert.assertTrue(collect.containsAll(codes));

		assertEquals(WileyProductLifecycleEnum.ACTIVE, result.get(0).getEntries().get(0).getProduct().getLifecycleStatus());
		assertEquals(WileyProductLifecycleEnum.ACTIVE, ((VariantProductModel) result.get(0).getEntries().get(0).getProduct())
				.getBaseProduct().getLifecycleStatus());
	}

	@Test
	public void testFindPreOrdersWithCFAPreOrderProducts() throws ImpExException
	{
		//given
		importCsv("/wileycore/test/order/WileyOrderDaoImplIntegrationTest/preOrderCFAOrders.impex", DEFAULT_ENCODING);
		catalogVersionService.setSessionCatalogVersion(CATALOG_ID, CATALOG_VERSION);
		//when
		List<OrderModel> result = wileyOrderDao.findPreOrdersWithActiveProducts();
		//then
		assertEquals(0, result.size());
	}

	@Test
	public void testFindPreOrdersWithNotCFAActiveProducts() throws ImpExException
	{
		//given
		importCsv("/wileycore/test/order/WileyOrderDaoImplIntegrationTest/activeCPAorders.impex", DEFAULT_ENCODING);
		HashSet<String> codes = new HashSet<>(Arrays.asList("TEST_ORDER_16", "TEST_ORDER_17", "TEST_ORDER_18"));
		//when
		List<OrderModel> result = wileyOrderDao.findPreOrdersWithActiveProducts();
		//then
		assertEquals(3, result.size());
		Set<String> collect = result.stream().map(OrderModel::getCode).collect(Collectors.toSet());
		Assert.assertTrue(collect.containsAll(codes));

		assertEquals(null, result.get(0).getEntries().get(0).getProduct().getLifecycleStatus());
		assertEquals(WileyProductLifecycleEnum.ACTIVE, ((VariantProductModel) result.get(0).getEntries().get(0).getProduct())
				.getBaseProduct().getLifecycleStatus());
	}

	@Test
	public void testFindPreOrdersWithNotCFAPreOrderProducts() throws ImpExException
	{
		//given
		importCsv("/wileycore/test/order/WileyOrderDaoImplIntegrationTest/preOrderCPAorders.impex", DEFAULT_ENCODING);
		catalogVersionService.setSessionCatalogVersion(CATALOG_ID, CATALOG_VERSION);
		//when
		List<OrderModel> result = wileyOrderDao.findPreOrdersWithActiveProducts();
		//then
		assertEquals(0, result.size());
	}
}
