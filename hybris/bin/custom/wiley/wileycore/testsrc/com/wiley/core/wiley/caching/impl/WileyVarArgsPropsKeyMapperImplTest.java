package com.wiley.core.wiley.caching.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.regioncache.key.CacheKey;
import static org.junit.Assert.assertEquals;

import java.util.Collections;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.externaltax.xml.caching.impl.WileyVarArgsPropsCacheKey;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyVarArgsPropsKeyMapperImplTest
{
	@Spy
	private WileyVarArgsKeyMapperImpl testedInstance;


	@Test
	public void checkSkipOfParam()
	{
		Mockito.doAnswer((req) -> wrapWithProps(req.getArguments()))
				.when(testedInstance).wrap(Mockito.anyVararg());

		final String sameField = "test1";
		final InnerObj toCheck = new InnerObj(sameField, "test2");
		final InnerObj forCheck = new InnerObj(sameField, "test8");
		final CacheKey key = testedInstance.map(toCheck);
		assertEquals(wrapWithProps(forCheck), key);
	}

	protected Object wrapWithProps(final Object... req)
	{
		final WileyVarArgsPropsCacheKey cacheKey = new WileyVarArgsPropsCacheKey(req);
		//skip prop2 from equals/hashCode
		cacheKey.setExcludedFields(Collections.singletonList(Collections.singletonList("prop2")));
		return cacheKey;
	}

	private final class InnerObj
	{
		private final String prop1;
		private final String prop2;

		private InnerObj(final String prop1, final String prop2)
		{
			this.prop1 = prop1;
			this.prop2 = prop2;
		}
	}
}
