package com.wiley.core.product.interceptors;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Set;

import javax.annotation.Resource;

import org.junit.Test;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.constants.WileyCoreConstants;

import static org.fest.assertions.Assertions.assertThat;


/**
 * Integration test for {@link ProductApprovalStatusPrepareInterceptor}
 * Validates that base product approval status and modifiedTime not changed when varian product updated
 */
@IntegrationTest
public class ProductApprovalStatusPrepareInterceptorIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	public static final String TEST_PRODUCT_EDITOR_USER = "testproducteditor";
	public static final String TEST_BASE_PRODUCT_CODE = "TEST_BASE_PRODUCT";
	public static final String TEST_VARIANT_PRODUCT_CODE = "TEST_VARIANT_PRODUCT";

	@Resource
	private ModelService modelService;
	@Resource
	private ProductService productService;
	@Resource
	private UserService userService;

	@Test
	public void shouldNotUpdateBaseProductApprovalStatusWhenVariantProductUpdatedByProductEditor() throws Exception
	{
		importCsv(
				"/wileycore/test/product/interceptors/"
						+ "ProductApprovalStatusPrepareInterceptorIntegrationTest/sample-user-and-products.impex",
				DEFAULT_ENCODING);

		final UserModel user = userService.getUserForUID(TEST_PRODUCT_EDITOR_USER);
		Set<UserGroupModel> userGroups = userService.getAllUserGroupsForUser(user);
		UserGroupModel userGroup = userService.getUserGroupForUID(WileyCoreConstants.PRODUCTEDITORGROUP_UID);
		assertThat(userGroups.contains(userGroup));
		userService.setCurrentUser(user);

		ProductModel baseProductModel = productService.getProductForCode(TEST_BASE_PRODUCT_CODE);
		long initialBaseProductModifiedTime = baseProductModel.getModifiedtime().getTime();
		assertThat(initialBaseProductModifiedTime).isNotNull();
		ArticleApprovalStatus initialBaseProductApprovalStatus = baseProductModel.getApprovalStatus();
		assertThat(initialBaseProductApprovalStatus).isNotEqualTo(ArticleApprovalStatus.CHECK);


		ProductModel variantProductModel = productService.getProductForCode(TEST_VARIANT_PRODUCT_CODE);
		long initialVariantProductModifiedTime = variantProductModel.getModifiedtime().getTime();
		assertThat(initialVariantProductModifiedTime).isNotNull();
		ArticleApprovalStatus initialVariantProductApprovalStatus = variantProductModel.getApprovalStatus();
		assertThat(initialVariantProductApprovalStatus).isNotEqualTo(ArticleApprovalStatus.CHECK);
		//given
		variantProductModel.setDescription("Updated Description");
		//when
		modelService.save(variantProductModel);
		//then
		assertThat(baseProductModel.getApprovalStatus()).isEqualTo(initialBaseProductApprovalStatus);
		assertThat(baseProductModel.getModifiedtime().getTime()).isEqualTo(initialBaseProductModifiedTime);

		assertThat(variantProductModel.getApprovalStatus()).isEqualTo(ArticleApprovalStatus.CHECK);
		assertThat(variantProductModel.getModifiedtime().getTime()).isGreaterThan(initialVariantProductModifiedTime);
	}
}
