package com.wiley.core.wileyb2c.classification.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureValue;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.wileyb2c.classification.Wileyb2cClassificationAttributes;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cResolveImpactFactorStrategyUnitTest
{
	@InjectMocks
	private Wileyb2cResolveImpactFactorStrategy wileyb2cResolveImpactFactorStrategy;
	@Mock
	private Feature feature;
	@Mock
	private FeatureValue featureValue;

	@Test
	public void processFeature()
	{
		when(feature.getValue()).thenReturn(featureValue);
		when(featureValue.getValue()).thenReturn(33.2);

		final String processFeature = wileyb2cResolveImpactFactorStrategy.processFeature(feature);

		assertEquals(processFeature, "33.200");
	}

	@Test
	public void getAttributes()
	{
		final Set<Wileyb2cClassificationAttributes> attributes = wileyb2cResolveImpactFactorStrategy.getAttributes();

		assertEquals(attributes, Wileyb2cResolveImpactFactorStrategy.CLASSIFICATION_ATTRIBUTES);
	}
}
