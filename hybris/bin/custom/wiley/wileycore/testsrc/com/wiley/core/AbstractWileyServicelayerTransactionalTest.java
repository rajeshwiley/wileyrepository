package com.wiley.core;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.user.User;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.user.UserService;

import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;


/**
 * Created by Aliaksei_Zlobich on 10/16/2015.
 */
public abstract class AbstractWileyServicelayerTransactionalTest extends ServicelayerTransactionalTest
{
	protected static final String DEFAULT_ENCODING = "UTF-8";

	@Resource
	protected UserService userService;

	@Resource
	protected FlexibleSearchService flexibleSearchService;

	@Resource
	protected ModelService modelService;

	protected UserModel setUpUser(final String userUid)
	{
		final UserModel user = userService.getUserForUID(userUid);
		userService.setCurrentUser(user);
		JaloSession.getCurrentSession().getSessionContext().setUser((User) modelService.getSource(user));
		return user;
	}

	protected ApplicationContext initializeTestApplicationContext(final ApplicationContext parentApplicationContext,
			final String context)
	{
		final GenericXmlApplicationContext applicationContext = new GenericXmlApplicationContext();
		applicationContext.setParent(parentApplicationContext);
		applicationContext.load(context);
		applicationContext.refresh();
		return applicationContext;
	}

	protected int getTableSize(final String itemTypeCode)
	{
		final String query = "select count(*) from {" + itemTypeCode + "}";
		final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query);
		final List<Class> resultClassList = new ArrayList<Class>();
		resultClassList.add(Integer.class);
		searchQuery.setResultClassList(resultClassList);
		final Integer totalSize = (Integer) flexibleSearchService.search(searchQuery).getResult().iterator().next();
		return totalSize.intValue();
	}

	protected UserService getUserService()
	{
		return userService;
	}

	protected String readFromFile(final String filePath) throws Exception
	{
		final URI fileUri = this.getClass().getResource(filePath).toURI();
		return Files.lines(Paths.get(fileUri))
				.collect(Collectors.joining());
	}

	protected byte[] readBytesFromFile(final String filePathBinary) throws Exception
	{
		final URI fileUri = this.getClass().getResource(filePathBinary).toURI();
		return Files.readAllBytes(Paths.get(fileUri));
	}
}
