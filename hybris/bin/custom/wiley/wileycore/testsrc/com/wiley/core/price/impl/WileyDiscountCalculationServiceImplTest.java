package com.wiley.core.price.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.util.DiscountValue;

import java.math.BigDecimal;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.price.WileyDiscountCalculationService;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class WileyDiscountCalculationServiceImplTest
{
	private static final Double ORDER_TAX = 1d;

	private static final long ORDER_ENTRY_ONE_QTY = 2L;
	private static final Double ORDER_ENTRY_ONE_BASE_PRICE = 10d;
	private static final Double DISCOUNT_ENTRY_ONE = 2d;

	private static final Double ORDER_ENTRY_ONE_TOTAL_PRICE =
			ORDER_ENTRY_ONE_BASE_PRICE * ORDER_ENTRY_ONE_QTY - DISCOUNT_ENTRY_ONE;

	private static final long ORDER_ENTRY_TWO_QTY = 1L;
	private static final Double ORDER_ENTRY_TWO_BASE_PRICE = 20.0d;

	private static final Double ORDER_ENTRY_TWO_TOTAL_PRICE = ORDER_ENTRY_TWO_BASE_PRICE * ORDER_ENTRY_TWO_QTY;

	private static final Double ORDER_TOTAL_DISCOUNT = 5d;

	private static final Double ORDER_SUBTOTAL_PRICE = ORDER_ENTRY_ONE_TOTAL_PRICE
			+ ORDER_ENTRY_TWO_TOTAL_PRICE;

	private static final Double ORDER_ENTRY_ONE_WEIGHT = ORDER_ENTRY_ONE_TOTAL_PRICE
			/ ORDER_SUBTOTAL_PRICE;

	private static final Double ORDER_ENTRY_TWO_WEIGHT = ORDER_ENTRY_TWO_TOTAL_PRICE
			/ ORDER_SUBTOTAL_PRICE;

	private static final String PRODUCT_CODE_ONE = "pc1";
	private static final String PRODUCT_CODE_TWO = "pc2";
	private static final double ALLOWED_PRICE_DELTA = 0.001d;
	@InjectMocks
	private WileyDiscountCalculationService testInstance = new WileyDiscountCalculationServiceImpl();
	@Mock
	private OrderEntryModel orderEntryOne;
	@Mock
	private OrderEntryModel orderEntryTwo;
	@Mock
	private DiscountValue orderEntryOneDiscount;
	@Mock
	private ProductModel productOne;
	@Mock
	private ProductModel productTwo;
	@Mock
	private OrderModel orderModel;


	@Before
	public void setupOrderEntries()
	{
		when(orderModel.getTotalTax()).thenReturn(ORDER_TAX);
		when(orderModel.getSubtotal()).thenReturn(ORDER_SUBTOTAL_PRICE);
		when(orderModel.getTotalDiscounts()).thenReturn(ORDER_TOTAL_DISCOUNT);

		when(orderEntryOneDiscount.getAppliedValue()).thenReturn(DISCOUNT_ENTRY_ONE);
		when(orderEntryOne.getDiscountValues()).thenReturn(Arrays.asList(orderEntryOneDiscount));
		when(orderEntryOne.getTotalPrice()).thenReturn(ORDER_ENTRY_ONE_TOTAL_PRICE);
		when(orderEntryOne.getBasePrice()).thenReturn(ORDER_ENTRY_ONE_BASE_PRICE);
		when(orderEntryOne.getQuantity()).thenReturn(ORDER_ENTRY_ONE_QTY);
		when(orderEntryOne.getProduct()).thenReturn(productOne);
		when(orderEntryOne.getOrder()).thenReturn(orderModel);
		when(productOne.getCode()).thenReturn(PRODUCT_CODE_ONE);

		when(orderEntryTwo.getQuantity()).thenReturn(ORDER_ENTRY_TWO_QTY);
		when(orderEntryTwo.getTotalPrice()).thenReturn(ORDER_ENTRY_TWO_TOTAL_PRICE);
		when(orderEntryTwo.getBasePrice()).thenReturn(ORDER_ENTRY_TWO_BASE_PRICE);
		when(orderEntryTwo.getProduct()).thenReturn(productTwo);
		when(orderEntryTwo.getOrder()).thenReturn(orderModel);
		when(productTwo.getCode()).thenReturn(PRODUCT_CODE_TWO);

		when(orderModel.getEntries()).thenReturn(Arrays.asList(orderEntryOne, orderEntryTwo));

	}

	@Test
	public void shouldCalculateProportionalDiscountWithoutEntryOwnDiscount()
	{

		BigDecimal discountTwo = testInstance.getEntryProportionalDiscount(orderEntryTwo);
		assertPricesEqual(ORDER_ENTRY_TWO_WEIGHT * ORDER_TOTAL_DISCOUNT, discountTwo.doubleValue());
	}

	@Test
	public void shouldCalculateProportionalDiscountWithEntryOwnDiscount()
	{

		BigDecimal discountOne = testInstance.getEntryProportionalDiscount(orderEntryOne);
		assertPricesEqual(DISCOUNT_ENTRY_ONE + ORDER_ENTRY_ONE_WEIGHT * ORDER_TOTAL_DISCOUNT,
				discountOne.doubleValue());
	}

	@Test
	public void shouldCalculateOrderLevelProportionalDiscount()
	{
		BigDecimal discountOne = testInstance.getEntryOrderLevelProportionalDiscount(orderEntryOne);
		assertPricesEqual(ORDER_ENTRY_ONE_WEIGHT * ORDER_TOTAL_DISCOUNT,
				discountOne.doubleValue());
	}

	private void assertPricesEqual(final double expected, final double actual)
	{
		assertEquals(expected, actual, ALLOWED_PRICE_DELTA);
	}

}
