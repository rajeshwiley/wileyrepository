package com.wiley.core.promotions.ruledefinitions.conditions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.ruledefinitions.conditions.RuleQualifyingProductsConditionTranslator;
import de.hybris.platform.ruleengineservices.compiler.RuleCompilerContext;
import de.hybris.platform.ruleengineservices.compiler.RuleCompilerException;
import de.hybris.platform.ruleengineservices.rule.data.RuleConditionData;
import de.hybris.platform.ruleengineservices.rule.data.RuleConditionDefinitionData;
import de.hybris.platform.ruleengineservices.rule.data.RuleParameterData;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.variants.model.VariantProductModel;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.map.HashedMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.wiley.core.model.WileyProductModel;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyRuleQualifyingProductsAndVariantsConditionTranslatorTest
{

	private static final String WILEYAS_CATALOG = "asProductCatalog";
	private static final String WEL_CATALOG = "welProductCatalog";
	private static final String AGS_CATALOG = "agsProductCatalog";

	private static final String TEST_BASE_PRODUCT_1 = "test_base_product_1";
	private static final String TEST_BASE_PRODUCT_2 = "test_base_product_2";

	private static final String TEST_VARIANT_FOR_BASE_PRODUCT_1 = "test_variant_for_base_product_1";
	private static final String TEST_VARIANT_FOR_BASE_PRODUCT_2 = "test_variant_for_base_product_2";

	@Mock
	private RuleQualifyingProductsConditionTranslator ruleQualifyingProductsConditionTranslator;
	@Mock
	private CatalogVersionService catalogVersionService;
	@Mock
	private ProductService productService;
	@Mock
	private SessionService sessionService;

	@InjectMocks
	private WileyRuleQualifyingProductsAndVariantsConditionTranslator testInstance =
			new WileyRuleQualifyingProductsAndVariantsConditionTranslator();

	@Mock
	private RuleCompilerContext context;
	@Mock
	private RuleConditionData condition;
	@Mock
	private RuleConditionDefinitionData conditionDefinition;

	private WileyProductModel testBaseProduct1 = new WileyProductModel();
	private WileyProductModel testBaseProduct2 = new WileyProductModel();

	private VariantProductModel testVariant1 = new VariantProductModel();
	private VariantProductModel testVariant2 = new VariantProductModel();



	@Before
	public void setUp() throws Exception
	{
		testVariant1.setCode(TEST_VARIANT_FOR_BASE_PRODUCT_1);
		testVariant2.setCode(TEST_VARIANT_FOR_BASE_PRODUCT_2);

		testBaseProduct1.setCode(TEST_BASE_PRODUCT_1);
		testBaseProduct1.setVariants(Collections.singletonList(testVariant1));

		testBaseProduct2.setCode(TEST_BASE_PRODUCT_2);
		testBaseProduct2.setVariants(Collections.singletonList(testVariant2));

		testInstance.setApplicableForCatalogs(Arrays.asList(WILEYAS_CATALOG, WEL_CATALOG, AGS_CATALOG));

		setupBaseProductAsACondition(TEST_BASE_PRODUCT_1, TEST_BASE_PRODUCT_2);

		when(productService.getProductForCode(any(), eq(TEST_BASE_PRODUCT_1))).thenReturn(testBaseProduct1);
		when(productService.getProductForCode(any(), eq(TEST_BASE_PRODUCT_2))).thenReturn(testBaseProduct2);
		when(sessionService.executeInLocalView(Mockito.any(SessionExecutionBody.class))).thenAnswer(new Answer() {
			@Override
			public Object answer(final InvocationOnMock invocation) throws Throwable
			{
				final SessionExecutionBody args = (SessionExecutionBody) invocation.getArguments()[0];
				return args.execute();
			}
		});
	}

	private void setupBaseProductAsACondition(final String... baseProductCodes) {

		RuleParameterData productsParameter = new RuleParameterData();
		productsParameter.setValue(Arrays.asList(baseProductCodes));

		Map<String, RuleParameterData> params = new HashedMap<>();
		params.put("products", productsParameter);

		when(condition.getParameters()).thenReturn(params);
	}

	@Test
	public void shouldAddVariantCodesToConditionToAllBaseProducts() throws RuleCompilerException
	{
		//Given
		setupBaseProductAsACondition(TEST_BASE_PRODUCT_1, TEST_BASE_PRODUCT_2);
		//When
		testInstance.translate(context, condition, conditionDefinition);
		//Then
		List products = (List) condition.getParameters().get("products").getValue();
		assertTrue(products.containsAll(Arrays.asList(TEST_BASE_PRODUCT_1, TEST_BASE_PRODUCT_2,
				TEST_VARIANT_FOR_BASE_PRODUCT_1, TEST_VARIANT_FOR_BASE_PRODUCT_2)));
	}


	@Test
	public void shouldAvoidDuplicatesIfConditionContainsBaseProductAndVariants() throws RuleCompilerException
	{
		//Given
		setupBaseProductAsACondition(TEST_BASE_PRODUCT_1, TEST_VARIANT_FOR_BASE_PRODUCT_1);
		//When
		testInstance.translate(context, condition, conditionDefinition);
		//Then
		List products = (List) condition.getParameters().get("products").getValue();
		assertEquals(2, products.size());
	}

}