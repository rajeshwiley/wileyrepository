package com.wiley.core.payment.response.validators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.RegionData;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.paypal.hybris.data.GetExpressCheckoutDetailsResultData;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PayPalResponseBillingUnicodeCharactersValidatorUnitTest
{
	private PayPalResponseBillingUnicodeCharactersValidator testInstance = new PayPalResponseBillingUnicodeCharactersValidator();

	private GetExpressCheckoutDetailsResultData resultData = new GetExpressCheckoutDetailsResultData();

	private AddressData billingAddress;
	private CountryData billingCountry;
	private RegionData billingRegion;

	@Before
	public void setUp()
	{
		billingAddress = new AddressData();
		billingCountry = new CountryData();
		billingRegion = new RegionData();

		billingAddress.setBillingAddress(true);
		billingAddress.setCountry(billingCountry);
		billingAddress.setRegion(billingRegion);

		resultData.setBillingAddress(billingAddress);
	}

	@Test
	public void shouldReturnTrueIfPayPalResponseDataDoesNotHaveInvalidCharacters()
	{
		billingAddress.setId("");
		billingAddress.setAddressSummary(null);
		billingAddress.setFirstName("First Name");
		billingAddress.setLastName("Last Name");
		billingAddress.setLine1("Line 1, - asd");
		billingAddress.setLine2("Line 2, - River St");
		billingAddress.setTown("Hawesville");
		billingAddress.setPostalCode("124-0125");
		billingAddress.setPhone(null);
		billingAddress.setEmail("test@test.mail");

		billingCountry.setIsocode("US");
		billingCountry.setName("United States");

		billingRegion.setName("HOKKAIDO");
		billingRegion.setIsocode("");

		Assert.assertTrue(testInstance.validateResponse(resultData));
	}

	@Test
	public void shouldReturnFalseIfPayPalResponseDataHasAtLeastOneAttributeWithInvalidCharacters()
	{
		billingRegion.setName("asd青森やお支払いはできますasd");

		Assert.assertFalse(testInstance.validateResponse(resultData));
		Assert.assertEquals(1, resultData.getErrors().size());
	}

	@Test
	public void validateCountryAndRegionShouldReturnTrueIfBothCountryAndRegionHaveValidCharacters()
	{
		billingRegion.setName("Region");
		billingRegion.setIsocode("Region iso code");

		billingCountry.setName("Country name");
		billingCountry.setIsocode("Country iso code");

		Assert.assertTrue(testInstance.validateCountryAndRegion(billingAddress));
	}

	@Test
	public void validateCountryAndRegionShouldReturnFalseIfCountryIsoCodeIsInvalid()
	{
		billingCountry.setIsocode("お支");
		Assert.assertFalse(testInstance.validateCountryAndRegion(billingAddress));
	}

	@Test
	public void validateCountryAndRegionShouldReturnFalseIfRegionNameIsInvalid()
	{
		billingRegion.setName("買い物");
		Assert.assertFalse(testInstance.validateCountryAndRegion(billingAddress));
	}
}
