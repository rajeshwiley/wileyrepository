package com.wiley.core.payment.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionRequest;
import de.hybris.platform.acceleratorservices.payment.data.OrderEntryInfoData;
import de.hybris.platform.acceleratorservices.payment.data.OrderInfoData;
import de.hybris.platform.acceleratorservices.payment.data.PaymentData;
import de.hybris.platform.core.model.c2l.CountryModel;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.servicelayer.i18n.WileycomI18NService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class BasketWPGHttpRequestPopulatorTest
{
	private static final String WPG_CUSTOM_BASKET_TAX = "WPG_CUSTOM_BASKET_tax";
	private static final String WPG_CUSTOM_BASKET_TOTAL = "WPG_CUSTOM_BASKET_total";
	private static final String WPG_CUSTOM_BASKET_SHIPPING = "WPG_CUSTOM_BASKET_shipping";
	private static final String WPG_CUSTOM_BASKET_DESIRED_SHIPPING_DATE = "WPG_CUSTOM_BASKET_desired_shipping_date";
	private static final String WPG_CUSTOM_BASKET_SUBTOTAL = "WPG_CUSTOM_BASKET_subtotal";
	private static final String WPG_CUSTOM_BASKET_DISCOUNT = "WPG_CUSTOM_BASKET_discount";
	private static final String WPG_CUSTOM_BASKET_COUNT = "WPG_CUSTOM_BASKET_count";
	private static final String WPG_CUSTOM_BASKET_ENTRY_NAME = "WPG_CUSTOM_BASKET_1_name";
	private static final String WPG_CUSTOM_BASKET_ENTRY_DESCRIPTION = "WPG_CUSTOM_BASKET_1_desc";
	private static final String WPG_CUSTOM_BASKET_ENTRY_QUANTITY = "WPG_CUSTOM_BASKET_1_qty";
	private static final String WPG_CUSTOM_BASKET_ENTRY_IMAGE_URL = "WPG_CUSTOM_BASKET_1_imgUrl";
	private static final String WPG_CUSTOM_BASKET_ENTRY_FIRST_FEATURE = "WPG_CUSTOM_BASKET_1_1_feature";
	private static final String WPG_CUSTOM_BASKET_ENTRY_SECOND_FEATURE = "WPG_CUSTOM_BASKET_1_2_feature";
	private static final String WPG_CUSTOM_BASKET_ENTRY_PRICE = "WPG_CUSTOM_BASKET_1_price";
	private static final String WPG_CUSTOM_BASKET_ENTRY_TOTAL = "WPG_CUSTOM_BASKET_1_total";
	private static final String WPG_CUSTOM_BASKET_ENTRY_TAX = "WPG_CUSTOM_BASKET_1_tax";
	private static final String WPG_CUSTOM_BASKET_CURRENCY_SYMBOL = "WPG_CUSTOM_BASKET_currency_symbol";
	private static final String WPG_CUSTOM_BASKET_STUDENT_FLOW = "WPG_CUSTOM_BASKET_student_flow";
	private static final String WPG_CUSTOM_BASKET_ORDER_WITHOUT_SHIPPING = "WPG_CUSTOM_BASKET_order_without_shipping";
	private static final String WPG_CUSTOM_TAX_LABEL = "WPG_CUSTOM_tax_label";

	private static final String BASKET_COUNT = "2";
	private static final String BASKET_TOTAL = "100.25";
	private static final String BASKET_TAX = "15.45";
	private static final String BASKET_SHIPPING = "8.78";
	private static final String BASKET_ENTRY_NAME = "entryName";
	private static final String BASKET_ENTRY_DESCRIPTION = "entryDescription";
	private static final String BASKET_ENTRY_TOTAL_PRICE = "10.55";
	private static final String BASKET_ENTRY_TAX = "4.45";
	private static final String BASKET_ENTRY_QUANTITY = "5";
	private static final String BASKET_ENTRY_BASE_PRICE = "3.45";
	private static final String BASKET_SUBTOTAL = "100.20";
	private static final String BASKET_DISCOUNT = "11.0";
	private static final String CURRENCY_SYMBOL = "currencySymbol";
	private static final String BASKET_ENTRY_IMAGE_URL = "imageUrl";
	private static final String FIRST_FEATURE = "firstFeature";
	private static final String SECOND_FEATURE = "secondFeature";
	private static final int SHIPPING_YEAR = 2030;
	private static final int SHIPPING_MONTH = 00;
	private static final int SHIPPING_DATE = 02;
	private static final String FORMATTED_SHIPPING_DATE = "01/02/2030";

	@InjectMocks
	private BasketWPGHttpRequestPopulator testedInstance;

	private CreateSubscriptionRequest createSubscriptionRequest = new CreateSubscriptionRequest();
	private PaymentData paymentData = new PaymentData();
	private OrderInfoData orderInfoData = new OrderInfoData();
	private OrderEntryInfoData firstOrderEntry = new OrderEntryInfoData();
	private OrderEntryInfoData secondOrderEntry = new OrderEntryInfoData();

	@Mock
	private WileycomI18NService wileycomI18NServiceMock;

	@Mock
	private CountryModel currentCountryMock;

	@Before
	public void setUp()
	{
		orderInfoData.setOrderWithoutShipping(true);
		createSubscriptionRequest.setOrderInfoData(orderInfoData);
		orderInfoData.setOrderEntries(Arrays.asList(firstOrderEntry, secondOrderEntry));
		when(wileycomI18NServiceMock.getCurrentCountry()).thenReturn(Optional.of(currentCountryMock));
	}

	@Test
	public void shouldPopulateBasketCount() throws Exception
	{
		testedInstance.populate(createSubscriptionRequest, paymentData);

		assertEquals(BASKET_COUNT, paymentData.getParameters().get(WPG_CUSTOM_BASKET_COUNT));
	}

	@Test
	public void shouldPopulateBasketTotal() throws Exception
	{
		orderInfoData.setOrderTotal(BASKET_TOTAL);

		testedInstance.populate(createSubscriptionRequest, paymentData);

		assertEquals(BASKET_TOTAL, paymentData.getParameters().get(WPG_CUSTOM_BASKET_TOTAL));
	}

	@Test
	public void shouldPopulateBasketSubtotal() throws Exception
	{
		orderInfoData.setSubtotalWithoutDiscount(BASKET_SUBTOTAL);

		testedInstance.populate(createSubscriptionRequest, paymentData);

		assertEquals(BASKET_SUBTOTAL, paymentData.getParameters().get(WPG_CUSTOM_BASKET_SUBTOTAL));
	}

	@Test
	public void shouldPopulateBasketDiscount() throws Exception
	{
		orderInfoData.setDiscount(BASKET_DISCOUNT);

		testedInstance.populate(createSubscriptionRequest, paymentData);

		assertEquals(BASKET_DISCOUNT, paymentData.getParameters().get(WPG_CUSTOM_BASKET_DISCOUNT));
	}

	@Test
	public void shouldPopulateBasketCurrencySymbol() throws Exception
	{
		orderInfoData.setCurrencySymbol(CURRENCY_SYMBOL);

		testedInstance.populate(createSubscriptionRequest, paymentData);

		assertEquals(CURRENCY_SYMBOL, paymentData.getParameters().get(WPG_CUSTOM_BASKET_CURRENCY_SYMBOL));
	}

	@Test
	public void shouldPopulateBasketTax() throws Exception
	{
		orderInfoData.setTaxAmount(BASKET_TAX);

		testedInstance.populate(createSubscriptionRequest, paymentData);

		assertEquals(BASKET_TAX, paymentData.getParameters().get(WPG_CUSTOM_BASKET_TAX));
	}

	@Test
	public void shouldPopulateBasketShipping() throws Exception
	{
		orderInfoData.setShippingCost(BASKET_SHIPPING);

		testedInstance.populate(createSubscriptionRequest, paymentData);

		assertEquals(BASKET_SHIPPING, paymentData.getParameters().get(WPG_CUSTOM_BASKET_SHIPPING));
	}

	@Test
	public void shouldPopulateBasketEntryName()
	{
		firstOrderEntry.setName(BASKET_ENTRY_NAME);

		testedInstance.populate(createSubscriptionRequest, paymentData);

		assertEquals(BASKET_ENTRY_NAME, paymentData.getParameters().get(WPG_CUSTOM_BASKET_ENTRY_NAME));
	}

	@Test
	public void shouldPopulateBasketEntryDescription()
	{
		firstOrderEntry.setDescription(BASKET_ENTRY_DESCRIPTION);

		testedInstance.populate(createSubscriptionRequest, paymentData);

		assertEquals(BASKET_ENTRY_DESCRIPTION, paymentData.getParameters().get(WPG_CUSTOM_BASKET_ENTRY_DESCRIPTION));
	}

	@Test
	public void shouldPopulateBasketEntryBasePrice()
	{
		firstOrderEntry.setPrice(BASKET_ENTRY_BASE_PRICE);

		testedInstance.populate(createSubscriptionRequest, paymentData);

		assertEquals(BASKET_ENTRY_BASE_PRICE, paymentData.getParameters().get(WPG_CUSTOM_BASKET_ENTRY_PRICE));
	}

	@Test
	public void shouldPopulateBasketEntryTotalPrice()
	{
		firstOrderEntry.setTotal(BASKET_ENTRY_TOTAL_PRICE);

		testedInstance.populate(createSubscriptionRequest, paymentData);

		assertEquals(BASKET_ENTRY_TOTAL_PRICE, paymentData.getParameters().get(WPG_CUSTOM_BASKET_ENTRY_TOTAL));
	}

	@Test
	public void shouldPopulateBasketEntryTax()
	{
		firstOrderEntry.setTax(BASKET_ENTRY_TAX);

		testedInstance.populate(createSubscriptionRequest, paymentData);

		assertEquals(BASKET_ENTRY_TAX, paymentData.getParameters().get(WPG_CUSTOM_BASKET_ENTRY_TAX));
	}

	@Test
	public void shouldPopulateBasketEntryQuantity()
	{
		firstOrderEntry.setQuantity(BASKET_ENTRY_QUANTITY);

		testedInstance.populate(createSubscriptionRequest, paymentData);

		assertEquals(BASKET_ENTRY_QUANTITY, paymentData.getParameters().get(WPG_CUSTOM_BASKET_ENTRY_QUANTITY));
	}

	@Test
	public void shouldPopulateBasketEntryImageUrl()
	{
		firstOrderEntry.setImageUrl(BASKET_ENTRY_IMAGE_URL);

		testedInstance.populate(createSubscriptionRequest, paymentData);

		assertEquals(BASKET_ENTRY_IMAGE_URL, paymentData.getParameters().get(WPG_CUSTOM_BASKET_ENTRY_IMAGE_URL));
	}

	@Test
	public void shouldPopulateBasketEntryFeatures()
	{
		firstOrderEntry.setFeatures(Arrays.asList(FIRST_FEATURE, SECOND_FEATURE));

		testedInstance.populate(createSubscriptionRequest, paymentData);

		assertEquals(FIRST_FEATURE, paymentData.getParameters().get(WPG_CUSTOM_BASKET_ENTRY_FIRST_FEATURE));
		assertEquals(SECOND_FEATURE, paymentData.getParameters().get(WPG_CUSTOM_BASKET_ENTRY_SECOND_FEATURE));
	}


	@Test
	public void shouldPopulateStudentFlow() throws Exception
	{
		orderInfoData.setStudentFlow(true);

		testedInstance.populate(createSubscriptionRequest, paymentData);

		assertEquals("true", paymentData.getParameters().get(WPG_CUSTOM_BASKET_STUDENT_FLOW));
	}

	@Test
	public void shouldPopulateOrderWithoutShippingParamPositive()
	{
		orderInfoData.setOrderWithoutShipping(true);

		testedInstance.populate(createSubscriptionRequest, paymentData);

		assertEquals("true", paymentData.getParameters().get(WPG_CUSTOM_BASKET_ORDER_WITHOUT_SHIPPING));
	}

	@Test
	public void shouldPopulateOrderWithoutShippingParamNegative()
	{
		orderInfoData.setOrderWithoutShipping(false);

		testedInstance.populate(createSubscriptionRequest, paymentData);

		assertEquals("false", paymentData.getParameters().get(WPG_CUSTOM_BASKET_ORDER_WITHOUT_SHIPPING));
	}

	@Test
	public void shouldPopulateFormattedDesiredShippingDate()
	{
		Calendar calendar = Calendar.getInstance();
		calendar.set(SHIPPING_YEAR, SHIPPING_MONTH, SHIPPING_DATE);
		orderInfoData.setDesiredShippingDate(calendar.getTime());

		testedInstance.populate(createSubscriptionRequest, paymentData);

		assertEquals(FORMATTED_SHIPPING_DATE, paymentData.getParameters().get(WPG_CUSTOM_BASKET_DESIRED_SHIPPING_DATE));
	}

	@Test
	public void testPopulateTaxLabel()
	{
		when(currentCountryMock.getTaxName()).thenReturn("Tax_label");
		testedInstance.populate(createSubscriptionRequest, paymentData);
		assertEquals("Tax_label", paymentData.getParameters().get(WPG_CUSTOM_TAX_LABEL));
	}
}