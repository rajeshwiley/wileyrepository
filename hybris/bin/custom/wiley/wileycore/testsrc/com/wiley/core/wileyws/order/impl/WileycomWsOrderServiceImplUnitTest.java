package com.wiley.core.wileyws.order.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;


/**
 * Created by Georgii_Gavrysh on 8/12/2016.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileycomWsOrderServiceImplUnitTest
{
	private static final String ISBN_PRODUCT_FIRST = "ISBN_PRODUCT_FIRST";
	private static final String ISBN_PRODUCT_SECOND = "ISBN_PRODUCT_SECOND";
	private static final String ISBN_NO_PRODUCT_IN_ORDER = "ISBN_NO_PRODUCT_IN_ORDER";

	@InjectMocks
	private WileycomWsOrderServiceImpl wileycomWsOrderService;

	@Mock
	private OrderModel orderMock;

	@Mock
	private OrderEntryModel orderEntryFirstMock;

	@Mock
	private OrderEntryModel orderEntrySecondMock;

	@Mock
	private ProductModel productFirstMock;

	@Mock
	private ProductModel productSecondMock;

	@Before
	public void setupOrder()
	{
		when(orderEntryFirstMock.getProduct()).thenReturn(productFirstMock);
		when(orderEntrySecondMock.getProduct()).thenReturn(productSecondMock);

		when(productFirstMock.getIsbn()).thenReturn(ISBN_PRODUCT_FIRST);
		when(productSecondMock.getIsbn()).thenReturn(ISBN_PRODUCT_SECOND);
	}

	private void setupOrderWithOneEntry()
	{
		when(orderMock.getEntries()).thenReturn(Arrays.asList(orderEntryFirstMock));
	}

	private void setupOrderWithTwoEntries()
	{
		when(orderMock.getEntries()).thenReturn(Arrays.asList(orderEntryFirstMock, orderEntrySecondMock));
	}

	@Test
	public void testOrderWithProductIsbn()
	{
		setupOrderWithOneEntry();
		assertThat(wileycomWsOrderService.getOrderEntryModelByISBN(orderMock, ISBN_PRODUCT_FIRST),
				is(Optional.of(orderEntryFirstMock)));
	}

	@Test
	public void testOrderOneEntryWithoutProductIsbn()
	{
		setupOrderWithOneEntry();
		assertThat(wileycomWsOrderService.getOrderEntryModelByISBN(orderMock, ISBN_PRODUCT_SECOND), is(Optional.empty()));
	}

	@Test
	public void testOrderTwoEntriesWithFirstProductIsbn()
	{
		setupOrderWithTwoEntries();
		assertThat(wileycomWsOrderService.getOrderEntryModelByISBN(orderMock, ISBN_PRODUCT_FIRST),
				is(Optional.of(orderEntryFirstMock)));
	}

	@Test
	public void testOrderTwoEntriesWithSecondProductIsbn()
	{
		setupOrderWithTwoEntries();
		assertThat(wileycomWsOrderService.getOrderEntryModelByISBN(orderMock, ISBN_PRODUCT_SECOND),
				is(Optional.of(orderEntrySecondMock)));
	}

	@Test
	public void testOrderTwoEntriesWithoutProductIsbn()
	{
		setupOrderWithTwoEntries();
		assertThat(wileycomWsOrderService.getOrderEntryModelByISBN(orderMock, ISBN_NO_PRODUCT_IN_ORDER), is(Optional.empty()));
	}

}
