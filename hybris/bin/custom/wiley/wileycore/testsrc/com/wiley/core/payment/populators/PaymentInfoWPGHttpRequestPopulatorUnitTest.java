package com.wiley.core.payment.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionRequest;
import de.hybris.platform.acceleratorservices.payment.data.PaymentData;
import de.hybris.platform.acceleratorservices.payment.data.PaymentInfoData;
import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;


/**
 * Test for {@link PaymentInfoWPGHttpRequestPopulator}
 */
@UnitTest
public class PaymentInfoWPGHttpRequestPopulatorUnitTest
{
	private static final String WPG_CUSTOM_PAYINFO_CARD_NUMBER = "WPG_CUSTOM_payinfo_card_number";
	private static final String WPG_CUSTOM_PAYINFO_EXPIRY_MONTH = "WPG_CUSTOM_payinfo_expiry_month";
	private static final String WPG_CUSTOM_PAYINFO_EXPIRY_YEAR = "WPG_CUSTOM_payinfo_expiry_year";

	private static final String PAYINFO_CARD_NUMBER = "9090";
	private static final String EXPECTED_MASKED_CARD_NUMBER = "************9090";
	private static final Integer PAYINFO_EXPIRY_MONTH = 1;
	private static final Integer PAYINFO_EXPIRY_YEAR = 2;

	private PaymentInfoWPGHttpRequestPopulator paymentInfoWPGHttpRequestPopulator = new PaymentInfoWPGHttpRequestPopulator();

	private PaymentInfoData paymentInfoData;
	private CreateSubscriptionRequest createSubscriptionRequest;
	private PaymentData paymentData;

	@Before
	public void setup()
	{
		paymentInfoData = new PaymentInfoData();
		createSubscriptionRequest = new CreateSubscriptionRequest();
		createSubscriptionRequest.setPaymentInfoData(paymentInfoData);
		paymentData = new PaymentData();
	}

	@Test
	public void shouldPopulateCardNumber()
	{
		paymentInfoData.setCardAccountNumber(PAYINFO_CARD_NUMBER);

		paymentInfoWPGHttpRequestPopulator.populate(createSubscriptionRequest, paymentData);

		assertEquals(EXPECTED_MASKED_CARD_NUMBER, paymentData.getParameters().get(WPG_CUSTOM_PAYINFO_CARD_NUMBER));
	}

	@Test
	public void shouldPopulateExpirationMonth()
	{
		paymentInfoData.setCardExpirationMonth(PAYINFO_EXPIRY_MONTH);

		paymentInfoWPGHttpRequestPopulator.populate(createSubscriptionRequest, paymentData);

		assertEquals(PAYINFO_EXPIRY_MONTH.toString(), paymentData.getParameters().get(WPG_CUSTOM_PAYINFO_EXPIRY_MONTH));
	}

	@Test
	public void shouldPopulateExpirationYear()
	{
		paymentInfoData.setCardExpirationYear(PAYINFO_EXPIRY_YEAR);

		paymentInfoWPGHttpRequestPopulator.populate(createSubscriptionRequest, paymentData);

		assertEquals(PAYINFO_EXPIRY_YEAR.toString(), paymentData.getParameters().get(WPG_CUSTOM_PAYINFO_EXPIRY_YEAR));
	}
}
