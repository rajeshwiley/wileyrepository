package com.wiley.core.integration.address.service.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.address.AddressVerificationDecision;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.validation.services.ValidationService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.i18n.WileyCommonI18NService;
import com.wiley.core.i18n.WileyCountryService;
import com.wiley.core.integration.address.WileyAddressesGateway;
import com.wiley.core.integration.address.dto.AddressDto;
import com.wiley.core.integration.address.dto.AddressVerificationResultDto;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyasAddressVerificationServiceImplUnitTest
{
	private static final String COUNTRY_BY = "BY";
	private static final String COUNTRY_PL = "PL";
	private static final String REGION_HR = "HR";
	private static final String CITY_HR = "Grodno";
	private static final String POSTAL_CODE = "230005";
	private static final String LINE1 = "line1";
	private static final String LINE2 = "line2";
	private static final int ADDRESSES_LIMIT = 3;
	private static final String EMPTY_LINE = StringUtils.EMPTY;
	private static final String NULL_LINE = null;

	@Mock
	private WileyAddressesGateway wileyAddressesGateway;

	@Mock
	private WileyCountryService wileyCountryServiceMock;

	@Mock
	private WileyCommonI18NService wileyCommonI18NServiceMock;

	@Mock
	private CountryModel countryModelMock;

	@Mock
	private RegionModel regionModelMock;

	@Mock
	private ValidationService validationServiceMock;

	@InjectMocks
	private WileyasAddressVerificationServiceImpl wileyasAddressVerificationService;

	@Before
	public void setUp() throws Exception
	{
		wileyasAddressVerificationService.setSuggestedAddressesLimit(ADDRESSES_LIMIT);
		Set<String> acceptCodes = new HashSet<>();
		acceptCodes.add("Q2");
		acceptCodes.add("Q3");
		wileyasAddressVerificationService.setAcceptCodes(acceptCodes);
		Set<String> rejectCodes = new HashSet<>();
		rejectCodes.add("Q0");
		rejectCodes.add("Q1");
		wileyasAddressVerificationService.setRejectCodes(rejectCodes);
		wileyasAddressVerificationService.createKnownCodesSet();
	}
	@Test
	public void verifyAddressFiltering()
	{
		//given
		AddressDto userAddress = createAddressDto(COUNTRY_BY, REGION_HR, CITY_HR, POSTAL_CODE, LINE1, LINE2, null);
		AddressDto userAddress1 = createAddressDto(COUNTRY_BY, REGION_HR, CITY_HR, POSTAL_CODE, LINE1, LINE2, 10d);
		AddressDto userAddress2 = createAddressDto(COUNTRY_PL, REGION_HR, CITY_HR, POSTAL_CODE, LINE1, LINE2, null);
		when(validationServiceMock.validate(any(AddressDto.class)))
				.thenReturn(Collections.emptySet());


		// when
		when(wileyAddressesGateway.getSuggestions(userAddress)).thenReturn(
				Arrays.asList(userAddress1, userAddress2));

		final AddressVerificationResultDto verificationResultDto = wileyasAddressVerificationService.verifyAddress(userAddress);
		List<AddressDto> addresses = verificationResultDto.getSuggestedAddresses();

		// then
		assertFalse(addresses.isEmpty());
		assertEquals("Wrong addresses count returned", 1, addresses.size());
		assertEquals(userAddress2, addresses.get(0));
	}

	@Test
	public void userAddressWithoutRequiredField()
	{
		// given
		AddressDto userAddress = createAddressDto(COUNTRY_BY, REGION_HR, CITY_HR, POSTAL_CODE, LINE1, EMPTY_LINE, null);
		AddressDto suggAddress1 = createAddressDto(COUNTRY_BY, REGION_HR, CITY_HR, POSTAL_CODE, LINE1, EMPTY_LINE, 10d);
		AddressDto suggAddress2 = createAddressDto(COUNTRY_BY, REGION_HR, CITY_HR, POSTAL_CODE, LINE1, NULL_LINE, null);
		when(validationServiceMock.validate(any(AddressDto.class)))
				.thenReturn(Collections.emptySet());


		// when
		when(wileyAddressesGateway.getSuggestions(userAddress)).thenReturn(
				Arrays.asList(suggAddress1, suggAddress2));

		final AddressVerificationResultDto verificationResultDto = wileyasAddressVerificationService.verifyAddress(userAddress);
		List<AddressDto> addresses = verificationResultDto.getSuggestedAddresses();

		// then
		assertTrue(addresses.isEmpty());
	}

	@Test
	public void verifyAddressSorting()
	{
		AddressDto userAddress = createAddressDto(COUNTRY_PL, REGION_HR, CITY_HR, POSTAL_CODE, LINE2, LINE1, 10d);
		AddressDto suggestedAddress = createAddressDto(COUNTRY_BY, REGION_HR, CITY_HR, POSTAL_CODE, LINE2, LINE1, 10d);
		AddressDto suggestedAddress1 = createAddressDto(COUNTRY_BY, REGION_HR, CITY_HR, POSTAL_CODE, LINE1, LINE2, 10d);
		AddressDto suggestedAddress2 = createAddressDto(COUNTRY_PL, REGION_HR, CITY_HR, POSTAL_CODE, LINE1, LINE2, 5d);
		AddressDto suggestedAddress3 = createAddressDto(COUNTRY_PL, REGION_HR, CITY_HR, POSTAL_CODE, LINE1, LINE2, 1d);
		AddressDto suggestedAddress4 = createAddressDto(COUNTRY_PL, REGION_HR, CITY_HR, POSTAL_CODE, null, LINE2);
		AddressDto suggestedAddress5 = createAddressDto(COUNTRY_PL, null, null, null, null, null);

		when(wileyAddressesGateway.getSuggestions(userAddress)).thenReturn(
				Arrays.asList(suggestedAddress5, suggestedAddress4, suggestedAddress3, suggestedAddress2, suggestedAddress1,
						suggestedAddress));

		final AddressVerificationResultDto verificationResultDto = wileyasAddressVerificationService.verifyAddress(userAddress);
		List<AddressDto> addresses = verificationResultDto.getSuggestedAddresses();
		assertFalse(addresses.isEmpty());
		assertEquals("Wrong addresses count returned", ADDRESSES_LIMIT, addresses.size());

		// verify returned items order
		// it should be max relevance and by line1
		assertEquals(suggestedAddress1, addresses.get(0));
		assertEquals(suggestedAddress, addresses.get(1));
		assertEquals(suggestedAddress2, addresses.get(2));

		assertEquals("Wrong decision calculated", AddressVerificationDecision.REVIEW, verificationResultDto.getDecision());
	}

	@Test
	public void verifyRejectOption()
	{
		AddressDto userAddress = createAddressDto(COUNTRY_PL, REGION_HR, CITY_HR, POSTAL_CODE, LINE2, LINE1, 10d);
		AddressDto suggestedAddress = createAddressDto(COUNTRY_BY, REGION_HR, CITY_HR, POSTAL_CODE, LINE2, LINE1, 10d);
		suggestedAddress.setDecision("Q0");

		when(wileyAddressesGateway.getSuggestions(userAddress)).thenReturn(Arrays.asList(suggestedAddress));

		final AddressVerificationResultDto verificationResultDto = wileyasAddressVerificationService.verifyAddress(userAddress);

		List<AddressDto> addresses = verificationResultDto.getSuggestedAddresses();
		assertFalse(addresses.isEmpty());
		assertEquals("Wrong addresses count returned", 1, addresses.size());
		assertEquals("Wrong decision calculated", AddressVerificationDecision.REJECT, verificationResultDto.getDecision());
		// result is rejected, so initially requested address expected
		assertEquals(userAddress, addresses.get(0));
	}

	@Test
	public void verifyErrorOption()
	{
		AddressDto userAddress = createAddressDto(COUNTRY_PL, REGION_HR, CITY_HR, POSTAL_CODE, LINE2, LINE1, 10d);
		AddressDto suggestedAddress = createAddressDto(COUNTRY_BY, REGION_HR, CITY_HR, POSTAL_CODE, LINE2, LINE1, 10d);
		suggestedAddress.setDecision("X");

		when(wileyAddressesGateway.getSuggestions(userAddress)).thenReturn(Arrays.asList(suggestedAddress));

		final AddressVerificationResultDto verificationResultDto = wileyasAddressVerificationService.verifyAddress(userAddress);

		List<AddressDto> addresses = verificationResultDto.getSuggestedAddresses();
		assertFalse(addresses.isEmpty());
		assertEquals("Wrong addresses count returned", 1, addresses.size());
		assertEquals("Wrong decision calculated", AddressVerificationDecision.UNKNOWN, verificationResultDto.getDecision());
		assertEquals(suggestedAddress, addresses.get(0));
	}

	@Test
	public void verifyInvalidRegionForCountryWithRegionTest()
	{
		AddressDto userAddress = createAddressDto(COUNTRY_PL, REGION_HR, CITY_HR, POSTAL_CODE, LINE2, LINE1, 10d);
		AddressDto suggestedAddress = createAddressDto(COUNTRY_BY, REGION_HR, CITY_HR, POSTAL_CODE, LINE2, LINE1, 10d);
		AddressDto suggestedAddress1 = createAddressDto(COUNTRY_BY, REGION_HR, CITY_HR, POSTAL_CODE, LINE1, LINE2, 10d);
		AddressDto suggestedAddress2 = createAddressDto(COUNTRY_PL, REGION_HR, CITY_HR, POSTAL_CODE, LINE1, LINE2, 5d);

		when(wileyAddressesGateway.getSuggestions(userAddress)).thenReturn(
				new ArrayList<>(Arrays.asList(suggestedAddress2, suggestedAddress1, suggestedAddress)));

		when(wileyCountryServiceMock.findCountryByCode(COUNTRY_PL)).thenReturn(countryModelMock);
		when(wileyCommonI18NServiceMock.getRegionForShortCode(countryModelMock, REGION_HR))
				.thenThrow(UnknownIdentifierException.class);
		when(countryModelMock.getRegions()).thenReturn(Collections.singletonList(regionModelMock));

		final AddressVerificationResultDto verificationResultDto = wileyasAddressVerificationService.verifyAddress(userAddress);
		List<AddressDto> addresses = verificationResultDto.getSuggestedAddresses();
		assertFalse(addresses.isEmpty());
		assertEquals(2, addresses.size());
	}

	@Test
	public void verifyInvalidRegionForCountryWithoutRegionTest()
	{
		AddressDto userAddress = createAddressDto(COUNTRY_PL, REGION_HR, CITY_HR, POSTAL_CODE, LINE2, LINE1, 10d);
		AddressDto suggestedAddress = createAddressDto(COUNTRY_BY, REGION_HR, CITY_HR, POSTAL_CODE, LINE2, LINE1, 10d);
		AddressDto suggestedAddress1 = createAddressDto(COUNTRY_BY, REGION_HR, CITY_HR, POSTAL_CODE, LINE1, LINE2, 10d);
		AddressDto suggestedAddress2 = createAddressDto(COUNTRY_PL, REGION_HR, CITY_HR, POSTAL_CODE, LINE1, LINE2, 5d);

		when(wileyAddressesGateway.getSuggestions(userAddress)).thenReturn(
				Arrays.asList(suggestedAddress2, suggestedAddress1, suggestedAddress));

		when(wileyCountryServiceMock.findCountryByCode(COUNTRY_PL)).thenReturn(countryModelMock);
		when(wileyCommonI18NServiceMock.getRegionForShortCode(countryModelMock, REGION_HR))
				.thenThrow(UnknownIdentifierException.class);
		when(countryModelMock.getRegions()).thenReturn(Collections.emptyList());

		final AddressVerificationResultDto verificationResultDto = wileyasAddressVerificationService.verifyAddress(userAddress);
		List<AddressDto> addresses = verificationResultDto.getSuggestedAddresses();
		assertFalse(addresses.isEmpty());
		assertEquals(3, addresses.size());
		assertNull(suggestedAddress2.getRegionIso2());
	}

	private AddressDto createAddressDto(final String country, final String region, final String city, final String postalCode,
			final String line1, final String line2)
	{
		return createAddressDto(country, region, city, postalCode, line1, line2, null);
	}

	private AddressDto createAddressDto(final String country, final String region, final String city, final String postalCode,
			final String line1, final String line2, final Double relevance)
	{
		AddressDto userAddress = new AddressDto();
		userAddress.setCountryIso2(country);
		userAddress.setRegionIso2(region);
		userAddress.setTown(city);
		userAddress.setPostalCode(postalCode);
		userAddress.setLine1(line1);
		userAddress.setLine2(line2);
		userAddress.setDecision("Q2");
		if (relevance != null)
		{
			userAddress.setRelevance(relevance);
		}
		return userAddress;
	}
}