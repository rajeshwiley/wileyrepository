package com.wiley.core.i18n.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.servicelayer.i18n.daos.CountryDao;
import de.hybris.platform.servicelayer.session.SessionService;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.Lists;

import static com.wiley.core.constants.WileyCoreConstants.CURRENT_COUNTRY_SESSION_ATTR;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyCountryServiceImplTest
{
	private static final String DEFAULT_COUNTRY_ISOCODE = "US";
	@Mock
	private SessionService sessionServiceMock;
	@Mock
	private CountryDao countryDaoMock;
	@Mock
	private CountryModel countryMock;
	@InjectMocks
	private WileyCountryServiceImpl wileyCountryService = new WileyCountryServiceImpl();


	@Before
	public void setUp()
	{
		wileyCountryService.setDefaultCountryIsocode(DEFAULT_COUNTRY_ISOCODE);
	}

	@Test
	public void shouldSetDefaultCountryAsCurrent()
	{
		when(countryDaoMock.findCountriesByCode(DEFAULT_COUNTRY_ISOCODE)).thenReturn(Lists.newArrayList(countryMock));

		wileyCountryService.useDefaultCountryAsCurrent();

		verify(sessionServiceMock, times(1)).setAttribute(CURRENT_COUNTRY_SESSION_ATTR, countryMock);
	}
}
