package com.wiley.core.wileyb2c.sitemap.job;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.enums.SiteMapPageEnum;
import de.hybris.platform.acceleratorservices.model.SiteMapConfigModel;
import de.hybris.platform.acceleratorservices.model.SiteMapMediaCronJobModel;
import de.hybris.platform.acceleratorservices.model.SiteMapPageModel;
import de.hybris.platform.acceleratorservices.sitemap.generator.SiteMapGenerator;
import de.hybris.platform.basecommerce.strategies.ActivateBaseSiteInSessionStrategy;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.internal.util.collections.Sets;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Created by Maksim_Kozich on 31.08.17.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cContentSearchConfigMediaJobUnitTest
{
	@Mock
	private SiteMapMediaCronJobModel siteMapMediaCronJobModelMock;

	@Mock
	private Logger loggerMock;

	@Mock
	private CMSSiteModel cmsSiteModelMock;

	@Mock
	private SiteMapConfigModel contentSearchConfigModelMock;

	@Mock
	private SiteMapPageModel productSiteMapPageModelMock;

	@Mock
	private SiteMapPageModel homepageSiteMapPageModelMock;

	@Mock
	private SiteMapPageModel categorylandingSiteMapPageModelMock;

	@Mock
	private SiteMapPageModel contentSiteMapPageModelMock;

	@Mock
	private SiteMapPageModel customSiteMapPageModelMock;

	@Mock
	private SiteMapGenerator productSiteMapGeneratorModelMock;

	@Mock
	private SiteMapGenerator homepageSiteMapGeneratorModelMock;

	@Mock
	private SiteMapGenerator categorylandingSiteMapGeneratorModelMock;

	@Mock
	private SiteMapGenerator contentSiteMapGeneratorModelMock;

	@Mock
	private SiteMapGenerator customSiteMapGeneratorModelMock;

	@Mock
	private CMSSiteService cmsSiteServiceMock;

	@Mock
	private ActivateBaseSiteInSessionStrategy<CMSSiteModel> activateBaseSiteInSessionMock;

	private Map<SiteMapPageModel, SiteMapPageEnum> siteMapPageModelSiteMapPageEnumMap;

	private Map<SiteMapPageEnum, SiteMapGenerator> siteMapPageEnumSiteMapGeneratorMap;

	@Spy
	@InjectMocks
	private Wileyb2cContentSearchConfigMediaJob testInstance;



	@Test
	public void testExceptionIsThrownInCaseAnyGeneratorFailed()
	{
		// Given
		initMaps();
		Set<SiteMapPageEnum> badSiteMapPages = Sets.newSet(SiteMapPageEnum.CATEGORYLANDING, SiteMapPageEnum.CONTENT);
		whenPerformMocks(badSiteMapPages);

		// When
		try
		{
			testInstance.perform(siteMapMediaCronJobModelMock);
			fail();
		}
		catch (IllegalArgumentException e)
		{
			// Then
			verifyPerformMocks(badSiteMapPages);

			assertEquals(Wileyb2cSiteMapMediaJob.PREPARE_MODEL_LIST_FOR_ONE_OF_SITE_MAP_PAGES_FAILED_MESSAGE, e.getMessage());
		}
	}

	@Test
	public void testExceptionIsNotThrownInCaseNoGeneratorFailed()
	{
		// Given
		initMaps();
		Set<SiteMapPageEnum> noBadSiteMapPages = Sets.newSet();
		whenPerformMocks(noBadSiteMapPages);

		// When
		testInstance.perform(siteMapMediaCronJobModelMock);

		// Then
		verifyPerformMocks(noBadSiteMapPages);
	}

	protected void initMaps()
	{
		siteMapPageModelSiteMapPageEnumMap = new HashMap<>();
		siteMapPageModelSiteMapPageEnumMap.put(productSiteMapPageModelMock, SiteMapPageEnum.PRODUCT);
		siteMapPageModelSiteMapPageEnumMap.put(homepageSiteMapPageModelMock, SiteMapPageEnum.HOMEPAGE);
		siteMapPageModelSiteMapPageEnumMap.put(categorylandingSiteMapPageModelMock, SiteMapPageEnum.CATEGORYLANDING);
		siteMapPageModelSiteMapPageEnumMap.put(contentSiteMapPageModelMock, SiteMapPageEnum.CONTENT);
		siteMapPageModelSiteMapPageEnumMap.put(customSiteMapPageModelMock, SiteMapPageEnum.CUSTOM);

		siteMapPageEnumSiteMapGeneratorMap = new HashMap<>();
		siteMapPageEnumSiteMapGeneratorMap.put(SiteMapPageEnum.PRODUCT, productSiteMapGeneratorModelMock);
		siteMapPageEnumSiteMapGeneratorMap.put(SiteMapPageEnum.HOMEPAGE, homepageSiteMapGeneratorModelMock);
		siteMapPageEnumSiteMapGeneratorMap.put(SiteMapPageEnum.CATEGORYLANDING, categorylandingSiteMapGeneratorModelMock);
		siteMapPageEnumSiteMapGeneratorMap.put(SiteMapPageEnum.CONTENT, contentSiteMapGeneratorModelMock);
		siteMapPageEnumSiteMapGeneratorMap.put(SiteMapPageEnum.CUSTOM, customSiteMapGeneratorModelMock);
	}

	protected void whenPerformMocks(final Set<SiteMapPageEnum> badSiteMapPages)
	{
		when(testInstance.getLogger()).thenReturn(loggerMock);
		when(siteMapMediaCronJobModelMock.getContentSite()).thenReturn(cmsSiteModelMock);
		when(cmsSiteModelMock.getContentSearchConfig()).thenReturn(contentSearchConfigModelMock);

		for (Map.Entry<SiteMapPageModel, SiteMapPageEnum> entry : siteMapPageModelSiteMapPageEnumMap.entrySet())
		{
			SiteMapPageModel entryKey = entry.getKey();
			when(entryKey.getCode()).thenReturn(entry.getValue());
			when(entryKey.getActive()).thenReturn(Boolean.TRUE);
		}

		final Collection<SiteMapPageModel> siteMapPages = siteMapPageModelSiteMapPageEnumMap.keySet();
		when(contentSearchConfigModelMock.getSiteMapPages()).thenReturn(siteMapPages);


		for (Map.Entry<SiteMapPageEnum, SiteMapGenerator> entry : siteMapPageEnumSiteMapGeneratorMap.entrySet())
		{
			SiteMapPageEnum entryKey = entry.getKey();
			SiteMapGenerator entryValue = entry.getValue();
			when(testInstance.getGeneratorForSiteMapPage(entryKey)).thenReturn(entryValue);
			if (badSiteMapPages.contains(entryKey))
			{
				doThrow(IllegalArgumentException.class).when(testInstance).prepareModelsList(
						eq(siteMapMediaCronJobModelMock), eq(cmsSiteModelMock), eq(contentSearchConfigModelMock), any(),
						eq(entryKey), eq(entryValue));
			}
			else
			{
				doNothing().when(testInstance).prepareModelsList(
						eq(siteMapMediaCronJobModelMock), eq(cmsSiteModelMock), eq(contentSearchConfigModelMock), any(),
						eq(entryKey), eq(entryValue));
			}
		}
	}

	protected void verifyPerformMocks(final Set<SiteMapPageEnum> badSiteMapPages)
	{
		for (Map.Entry<SiteMapPageEnum, SiteMapGenerator> entry : siteMapPageEnumSiteMapGeneratorMap.entrySet())
		{
			SiteMapPageEnum entryKey = entry.getKey();
			SiteMapGenerator entryValue = entry.getValue();
			verify(testInstance).prepareModelsList(
					eq(siteMapMediaCronJobModelMock), eq(cmsSiteModelMock), eq(contentSearchConfigModelMock), any(),
					eq(entryKey), eq(entryValue));
			if (badSiteMapPages.contains(entryKey))
			{
				verify(loggerMock).error(
						eq(String.format(Wileyb2cSiteMapMediaJob.PREPARE_MODEL_LIST_FOR_SITE_MAP_PAGE_FAILED_MESSAGE, entryKey)),
						isA(IllegalArgumentException.class));
			}
			else
			{
				verify(loggerMock, never()).error(
						eq(String.format(Wileyb2cSiteMapMediaJob.PREPARE_MODEL_LIST_FOR_SITE_MAP_PAGE_FAILED_MESSAGE, entryKey)),
						isA(IllegalArgumentException.class));
			}
		}
	}
}
