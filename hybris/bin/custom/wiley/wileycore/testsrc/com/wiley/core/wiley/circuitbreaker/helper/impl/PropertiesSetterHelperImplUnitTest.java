package com.wiley.core.wiley.circuitbreaker.helper.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.netflix.hystrix.HystrixCommandProperties;


/**
 * Created by Georgii_Gavrysh on 11/3/2016.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PropertiesSetterHelperImplUnitTest
{
	@InjectMocks
	PropertiesSetterHelperImpl testedPropertiesSetterHelperImpl;

	@Mock
	private ConfigurationService configurationServiceMock;

	@Mock
	private Configuration configurationMock;

	@Mock
	private HystrixCommandProperties.Setter setterMock;

	@Before
	public void setup()
	{
		when(configurationServiceMock.getConfiguration()).thenReturn(configurationMock);
	}

	@Test
	public void testSetIsolationStrategyForCommand()
	{
		final String commandKey = "commandKey";
		when(configurationMock.getString("hystrix.command.commandKey.execution.isolation.strategy")).thenReturn("SEMAPHORE");
		testedPropertiesSetterHelperImpl.configureCircuitBreakerCommand(commandKey, setterMock);
		verify(setterMock).withExecutionIsolationStrategy(HystrixCommandProperties.ExecutionIsolationStrategy.SEMAPHORE);
	}

	@Test
	public void testSetIsolationStrategyDefaultProperty()
	{
		final String commandKey = "commandKey";
		when(configurationMock.getString("hystrix.command.execution.isolation.strategy")).thenReturn("SEMAPHORE");
		testedPropertiesSetterHelperImpl.configureCircuitBreakerCommand(commandKey, setterMock);
		verify(setterMock).withExecutionIsolationStrategy(HystrixCommandProperties.ExecutionIsolationStrategy.SEMAPHORE);
	}

	@Test
	public void testSetIsolationStrategyNotInvokedPropertyNotFound()
	{
		final String commandKey = "commandKey";
		testedPropertiesSetterHelperImpl.configureCircuitBreakerCommand(commandKey, setterMock);
		verify(setterMock, never()).withExecutionIsolationStrategy(any());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetIsolationStrategyFailedToParse()
	{
		final String commandKey = "commandKey";
		when(configurationMock.getString("hystrix.command.commandKey.execution.isolation.strategy")).thenReturn("NO_SUCH_ENUM");
		testedPropertiesSetterHelperImpl.configureCircuitBreakerCommand(commandKey, setterMock);
	}

	@Test
	public void testSetTimeoutEnabledForCommand()
	{
		final String commandKey = "commandKey";
		when(configurationMock.getString("hystrix.command.commandKey.execution.timeout.enabled")).thenReturn("false");
		testedPropertiesSetterHelperImpl.configureCircuitBreakerCommand(commandKey, setterMock);
		verify(setterMock).withExecutionTimeoutEnabled(false);
	}

	@Test
	public void testSetTimeoutEnabledDefaultProperty()
	{
		final String commandKey = "commandKey";
		when(configurationMock.getString("hystrix.command.execution.timeout.enabled")).thenReturn("true");
		testedPropertiesSetterHelperImpl.configureCircuitBreakerCommand(commandKey, setterMock);
		verify(setterMock).withExecutionTimeoutEnabled(true);
	}

	@Test
	public void testSetTimeoutEnabledNotInvokedPropertyNotFound()
	{
		final String commandKey = "commandKey";
		testedPropertiesSetterHelperImpl.configureCircuitBreakerCommand(commandKey, setterMock);
		verify(setterMock, never()).withExecutionTimeoutEnabled(anyBoolean());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetTimeoutEnabledNotInvokedFailedToParse()
	{
		final String commandKey = "commandKey";
		when(configurationMock.getString("hystrix.command.execution.timeout.enabled")).thenReturn("NOT_TRUE_NO_FALSE");
		testedPropertiesSetterHelperImpl.configureCircuitBreakerCommand(commandKey, setterMock);
	}

	@Test
	public void testSetTimeoutInMillisecondsForCommand()
	{
		final String commandKey = "commandKey";
		when(configurationMock.getString("hystrix.command.commandKey.execution.isolation.thread.timeoutInMilliseconds"))
				.thenReturn("123");
		testedPropertiesSetterHelperImpl.configureCircuitBreakerCommand(commandKey, setterMock);
		verify(setterMock).withExecutionTimeoutInMilliseconds(123);
	}

	@Test
	public void testSetTimeoutInMillisecondsDefaultProperty()
	{
		final String commandKey = "commandKey";
		when(configurationMock.getString("hystrix.command.execution.isolation.thread.timeoutInMilliseconds")).thenReturn("123");
		testedPropertiesSetterHelperImpl.configureCircuitBreakerCommand(commandKey, setterMock);
		verify(setterMock).withExecutionTimeoutInMilliseconds(123);
	}

	@Test
	public void testSetTimeoutInMillisecondsNotInvokedPropertyNotFound()
	{
		final String commandKey = "commandKey";
		testedPropertiesSetterHelperImpl.configureCircuitBreakerCommand(commandKey, setterMock);
		verify(setterMock, never()).withExecutionTimeoutInMilliseconds(anyInt());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetTimeoutInMillisecondsdNotInvokedFailedToParse()
	{
		final String commandKey = "commandKey";
		when(configurationMock.getString("hystrix.command.execution.isolation.thread.timeoutInMilliseconds")).thenReturn(
				"NOT_A_NUMBER");
		testedPropertiesSetterHelperImpl.configureCircuitBreakerCommand(commandKey, setterMock);
	}

	@Test
	public void testAllProperties()
	{
		final String commandKey = "commandKey";
		when(configurationMock.getString("hystrix.command.commandKey.execution.isolation.strategy")).thenReturn("SEMAPHORE");
		when(configurationMock.getString("hystrix.command.commandKey.execution.isolation.thread.timeoutInMilliseconds"))
				.thenReturn("234");
		when(configurationMock.getString("hystrix.command.commandKey.circuitBreaker.requestVolumeThreshold")).thenReturn("123");
		when(configurationMock.getString("hystrix.command.commandKey.circuitBreaker.sleepWindowInMilliseconds")).thenReturn(
				"345");
		when(configurationMock.getString("hystrix.command.commandKey.circuitBreaker.errorThresholdPercentage")).thenReturn("456");
		when(configurationMock.getString("hystrix.command.commandKey.metrics.rollingPercentile.timeInMilliseconds")).thenReturn(
				"567");
		when(configurationMock.getString("hystrix.command.commandKey.metrics.rollingPercentile.numBuckets")).thenReturn("678");
		when(configurationMock.getString("hystrix.command.commandKey.execution.timeout.enabled")).thenReturn("false");
		when(configurationMock.getString("hystrix.command.commandKey.circuitBreaker.enabled")).thenReturn("true");
		when(configurationMock.getString("hystrix.command.commandKey.fallback.enabled")).thenReturn("false");
		when(configurationMock.getString("hystrix.command.commandKey.requestCache.enabled")).thenReturn("true");
		testedPropertiesSetterHelperImpl.configureCircuitBreakerCommand(commandKey, setterMock);
		verify(setterMock).withExecutionIsolationStrategy(HystrixCommandProperties.ExecutionIsolationStrategy.SEMAPHORE);
		verify(setterMock).withExecutionTimeoutInMilliseconds(234);
		verify(setterMock).withCircuitBreakerRequestVolumeThreshold(123);
		verify(setterMock).withCircuitBreakerSleepWindowInMilliseconds(345);
		verify(setterMock).withCircuitBreakerErrorThresholdPercentage(456);
		verify(setterMock).withMetricsRollingPercentileWindowInMilliseconds(567);
		verify(setterMock).withMetricsRollingPercentileWindowBuckets(678);
		verify(setterMock).withExecutionTimeoutEnabled(false);
		verify(setterMock).withCircuitBreakerEnabled(true);
		verify(setterMock).withFallbackEnabled(false);
		verify(setterMock).withRequestCacheEnabled(true);
	}

}
