package com.wiley.core.wileyb2c.search.solrfacetsearch.provider.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.model.config.SolrFacetSearchConfigModel;
import de.hybris.platform.solrfacetsearch.provider.Qualifier;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Optional;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.servicelayer.i18n.WileycomI18NService;

import static com.wiley.core.wileyb2c.search.solrfacetsearch.provider.impl.Wileyb2cCountryAndCurrencyQualifierProvider.COUNTRIES;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cCountryAndCurrencyQualifierProviderUnitTest
{
	private static final String US_CODE = "US";
	private static final String CURRENCY_CODE = "USD";
	private static final String CANADA_CODE = "CA";
	@InjectMocks
	private Wileyb2cCountryAndCurrencyQualifierProvider wileyb2cCountryAndCurrencyQualifierProvider;
	@Mock
	private WileycomI18NService wileycomI18NService;
	@Mock
	private CommonI18NService commonI18NService;
	@Mock
	private SessionService sessionService;
	@Mock
	private FacetSearchConfig facetSearchConfig;
	@Mock
	private BaseSiteService baseSiteService;
	@Mock
	private IndexedType indexedType;
	@Mock
	private IndexConfig indexConfig;
	@Mock
	private CountryModel usCountry;
	@Mock
	private CountryModel canadaCountry;
	@Mock
	private CurrencyModel currency;
	@Mock
	private IndexedProperty indexedProperty;
	@Mock
	private BaseSiteModel currentSite;
	@Mock
	private SolrFacetSearchConfigModel facetSearchConfigModel;

	@Before
	public void setUp() throws Exception
	{
		when(facetSearchConfig.getIndexConfig()).thenReturn(indexConfig);
		when(usCountry.getIsocode()).thenReturn(US_CODE);
		when(canadaCountry.getIsocode()).thenReturn(CANADA_CODE);
		when(currency.getIsocode()).thenReturn(CURRENCY_CODE);
		when(baseSiteService.getCurrentBaseSite()).thenReturn(currentSite);
		when(currentSite.getSolrFacetSearchConfiguration()).thenReturn(facetSearchConfigModel);
	}

	@Test
	public void getSupportedTypes()
	{
		final Set<Class<?>> supportedTypes = wileyb2cCountryAndCurrencyQualifierProvider.getSupportedTypes();

		assertEquals(supportedTypes, Wileyb2cCountryAndCurrencyQualifierProvider.SUPPORTED_TYPES);
	}


	@Test
	public void getAvailableQualifiersWhenNoCountriesThenReturnEmptyList()
	{
		when(indexConfig.getCountries()).thenReturn(Collections.emptyList());

		final Collection<Qualifier> qualifiers = wileyb2cCountryAndCurrencyQualifierProvider.getAvailableQualifiers(
				facetSearchConfig, indexedType);

		assertEquals(qualifiers.size(), 0);
	}

	@Test
	public void getAvailableQualifiersWhenNoCurrenciesThenReturnEmptyList()
	{
		when(indexConfig.getCountries()).thenReturn(Arrays.asList(usCountry, canadaCountry));
		when(indexConfig.getCurrencies()).thenReturn(Collections.emptyList());

		final Collection<Qualifier> qualifiers = wileyb2cCountryAndCurrencyQualifierProvider.getAvailableQualifiers(
				facetSearchConfig, indexedType);

		assertEquals(qualifiers.size(), 0);
	}


	@Test
	public void getAvailableQualifiersWhenNoCountriesForUpdateThenReturnForAllCountries()
	{
		when(indexConfig.getCountries()).thenReturn(Arrays.asList(usCountry, canadaCountry));
		when(indexConfig.getCurrencies()).thenReturn(Collections.singletonList(currency));

		final Collection<Qualifier> qualifiers = wileyb2cCountryAndCurrencyQualifierProvider.getAvailableQualifiers(
				facetSearchConfig, indexedType);

		assertEquals(qualifiers.size(), 2);
		final Iterator<Qualifier> iterator = qualifiers.iterator();
		assertQualifier(usCountry, currency, US_CODE + "_" + CURRENCY_CODE, iterator.next());
		assertQualifier(canadaCountry, currency, CANADA_CODE + "_" + CURRENCY_CODE, iterator.next());
	}

	@Test
	public void getAvailableQualifiersWhenCountriesForUpdateThenReturnForCountriesForUpdate()
	{
		when(indexConfig.getCountries()).thenReturn(Arrays.asList(usCountry, canadaCountry));
		when(indexConfig.getCurrencies()).thenReturn(Collections.singletonList(currency));
		when(sessionService.getAttribute(COUNTRIES)).thenReturn(Collections.singletonList(canadaCountry));


		final Collection<Qualifier> qualifiers = wileyb2cCountryAndCurrencyQualifierProvider.getAvailableQualifiers(
				facetSearchConfig, indexedType);

		assertEquals(qualifiers.size(), 1);
		assertQualifier(canadaCountry, currency, CANADA_CODE + "_" + CURRENCY_CODE, qualifiers.iterator().next());
	}

	@Test
	public void canApplyWhenNonLocalizedShouldNotApply()
	{
		when(indexedProperty.isLocalized()).thenReturn(false);

		final boolean canApply = wileyb2cCountryAndCurrencyQualifierProvider.canApply(indexedProperty);

		assertFalse(canApply);
	}

	@Test
	public void canApplyWhenNonCurrencyShouldNotApply()
	{
		when(indexedProperty.isLocalized()).thenReturn(true);
		when(indexedProperty.isCurrency()).thenReturn(false);

		final boolean canApply = wileyb2cCountryAndCurrencyQualifierProvider.canApply(indexedProperty);

		assertFalse(canApply);
	}

	@Test
	public void canApplyWhenCurrencyAndLocalizedShouldApply()
	{
		when(indexedProperty.isLocalized()).thenReturn(true);
		when(indexedProperty.isCurrency()).thenReturn(true);

		final boolean canApply = wileyb2cCountryAndCurrencyQualifierProvider.canApply(indexedProperty);

		assertTrue(canApply);
	}

	@Test(expected = IllegalArgumentException.class)
	public void applyQualifierWhenIncorrectTypeShouldThrowException()
	{
		wileyb2cCountryAndCurrencyQualifierProvider.applyQualifier(new Qualifier()
		{
			@Override
			public <U> U getValueForType(final Class<U> aClass)
			{
				return null;
			}

			@Override
			public String toFieldQualifier()
			{
				return null;
			}
		});
	}

	@Test
	public void applyQualifierWhenCorrectTypeShouldSetCountryAndCurrency()
	{
		Wileyb2cCountryAndCurrencyQualifierProvider.Wileyb2cCountryAndCurrencyQualifier qualifier =
				new Wileyb2cCountryAndCurrencyQualifierProvider.Wileyb2cCountryAndCurrencyQualifier(usCountry, currency);

		wileyb2cCountryAndCurrencyQualifierProvider.applyQualifier(qualifier);

		verify(wileycomI18NService).setCurrentCountry(usCountry);
		verify(commonI18NService).setCurrentCurrency(currency);
	}

	@Test
	public void getCurrentQualifierWhenNoCurrencyShouldReturnNull()
	{
		when(wileycomI18NService.getCurrentCountry()).thenReturn(Optional.of(canadaCountry));
		when(facetSearchConfigModel.getCountries()).thenReturn(Collections.singletonList(canadaCountry));

		final Qualifier currentQualifier = wileyb2cCountryAndCurrencyQualifierProvider.getCurrentQualifier();

		assertNull(currentQualifier);
	}

	@Test
	public void getCurrentQualifierWhenCurrencyExistsShouldReturnQualifier()
	{
		when(commonI18NService.getCurrentCurrency()).thenReturn(currency);
		when(wileycomI18NService.getCurrentCountry()).thenReturn(Optional.of(canadaCountry));
		when(facetSearchConfigModel.getCountries()).thenReturn(Collections.singletonList(canadaCountry));

		final Qualifier currentQualifier = wileyb2cCountryAndCurrencyQualifierProvider.getCurrentQualifier();

		assertQualifier(canadaCountry, currency, CANADA_CODE + "_" + CURRENCY_CODE, currentQualifier);
	}

	@Test
	public void getCurrentQualifierWhenCurrencyExistsButCountryNotInIndexShouldReturnQualifierFromFallback()
	{
		when(commonI18NService.getCurrentCurrency()).thenReturn(currency);
		when(wileycomI18NService.getCurrentCountry()).thenReturn(Optional.of(canadaCountry));
		when(facetSearchConfigModel.getCountries()).thenReturn(Collections.singletonList(usCountry));
		when(canadaCountry.getFallbackCountries()).thenReturn(Collections.singletonList(usCountry));

		final Qualifier currentQualifier = wileyb2cCountryAndCurrencyQualifierProvider.getCurrentQualifier();

		assertQualifier(usCountry, currency, US_CODE + "_" + CURRENCY_CODE, currentQualifier);
	}


	private void assertQualifier(final CountryModel country, final CurrencyModel currency,
			final String fieldQualifier, final Qualifier qualifier)
	{
		assertEquals(Wileyb2cCountryAndCurrencyQualifierProvider.Wileyb2cCountryAndCurrencyQualifier.class, qualifier.getClass());
		Wileyb2cCountryAndCurrencyQualifierProvider.Wileyb2cCountryAndCurrencyQualifier countryAndCurrencyQualifier =
				(Wileyb2cCountryAndCurrencyQualifierProvider.Wileyb2cCountryAndCurrencyQualifier) qualifier;
		assertEquals(countryAndCurrencyQualifier.getCountry(), country);
		assertEquals(countryAndCurrencyQualifier.getCurrency(), currency);
		assertEquals(countryAndCurrencyQualifier.toFieldQualifier(), fieldQualifier);
	}



}
