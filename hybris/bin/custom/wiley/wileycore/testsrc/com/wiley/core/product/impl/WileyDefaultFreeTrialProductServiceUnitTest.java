package com.wiley.core.product.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.catalog.references.ProductReferenceService;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyFreeTrialProductModel;
import com.wiley.core.model.WileyVariantProductModel;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;


/**
 * Created by Uladzimir_Barouski on 3/31/2016.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyDefaultFreeTrialProductServiceUnitTest
{

	@InjectMocks
	private WileyDefaultFreeTrialProductService freeTrialProductService;

	@Mock
	private ProductReferenceService productReferenceServiceMock;

	@Mock
	private ProductModel productMock;

	@Mock
	private WileyVariantProductModel variantProductMock;

	@Test
	public void testGetRelatedFreeTrialForProductSuccess() throws Exception
	{
		initRefferenceMock();

		final WileyFreeTrialProductModel freeTrialProductModel = freeTrialProductService
				.getRelatedFreeTrialForProduct(productMock);
		assertNotNull(freeTrialProductModel);
	}

	@Test
	public void testGetRelatedFreeTrialForVariantProductSuccess() throws Exception
	{
		when(productReferenceServiceMock
				.getProductReferencesForSourceProduct(variantProductMock, ProductReferenceTypeEnum.FREE_TRIAL, true))
				.thenReturn(Collections.EMPTY_LIST);
		when(variantProductMock.getBaseProduct()).thenReturn(productMock);
		initRefferenceMock();

		final WileyFreeTrialProductModel freeTrialProductModel = freeTrialProductService
				.getRelatedFreeTrialForProduct(variantProductMock);
		assertNotNull(freeTrialProductModel);
	}

	@Test
	public void testGetRelatedFreeTrialForProductReturnsNull() throws Exception
	{
		when(productReferenceServiceMock
				.getProductReferencesForSourceProduct(productMock, ProductReferenceTypeEnum.FREE_TRIAL, true))
				.thenReturn(Collections.EMPTY_LIST);

		assertNull(freeTrialProductService.getRelatedFreeTrialForProduct(productMock));
	}

	private void initRefferenceMock()
	{
		ProductReferenceModel refferrence = new ProductReferenceModel();
		refferrence.setTarget(new WileyFreeTrialProductModel());
		Collection<ProductReferenceModel> referenceList = Arrays.asList(refferrence);

		when(productReferenceServiceMock
				.getProductReferencesForSourceProduct(productMock, ProductReferenceTypeEnum.FREE_TRIAL, true))
				.thenReturn(referenceList);
	}
}