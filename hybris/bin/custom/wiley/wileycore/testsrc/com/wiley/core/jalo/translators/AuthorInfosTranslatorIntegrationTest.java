package com.wiley.core.jalo.translators;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Locale;

import javax.annotation.Resource;

import org.fest.assertions.Assertions;
import org.fest.assertions.GenericAssert;
import org.junit.Before;
import org.junit.Test;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.model.AuthorInfoModel;

import static org.fest.assertions.Assertions.assertThat;


/**
 * Test for {@link AuthorInfosTranslator}
 */
public class AuthorInfosTranslatorIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	private static final String TEST_PRODUCT_CODE = "WCOM_PRODUCT";
	private static final String TEST_AUTHOR_NAME_1 = "authorName1";
	private static final String TEST_AUTHOR_ROLE_1 = "authorRole1";
	private static final String TEST_AUTHOR_NAME_2 = "authorName2";
	private static final String TEST_AUTHOR_ROLE_2 = "authorRole2";
	private static final String LANG_EN_ISOCODE = "en";

	private static final String TEST_RESOURCES_FOLDER = "/wileycore/test/jalo/translators/AuthorInfosTranslatorIntegrationTest";

	@Resource
	private ProductService productService;

	@Resource
	private CommonI18NService commonI18NService;

	@Resource
	private ModelService modelService;

	@Before
	public void setUp() throws Exception
	{
		commonI18NService.setCurrentLanguage(commonI18NService.getLanguage(LANG_EN_ISOCODE));
	}

	@Test
	public void shouldFillEmptyFieldDueToEmptyImpexValue() throws Exception
	{
		//Given
		importCsv(TEST_RESOURCES_FOLDER + "/emptyAuthorInfos.impex", DEFAULT_ENCODING);

		//When
		final ProductModel product = productService.getProductForCode(TEST_PRODUCT_CODE);

		//Then
		assertThat(product.getAuthorInfos()).isEmpty();
	}

	@Test
	public void shouldFillEmptyFieldDueToListOfEmptyTokens() throws Exception
	{
		//Given
		importCsv(TEST_RESOURCES_FOLDER + "/emptyTokensAuthorInfo.impex", DEFAULT_ENCODING);

		//When
		final ProductModel product = productService.getProductForCode(TEST_PRODUCT_CODE);

		//Then
		assertThat(product.getAuthorInfos()).isEmpty();
	}

	@Test(expected = ImpExException.class)
	public void shouldThrowExceptionDueToAbsentPipe() throws Exception
	{
		final String impexFileName = TEST_RESOURCES_FOLDER + "/authorInfoNameOnlyWithoutPipe.impex";
		importStream(getClass().getResourceAsStream(impexFileName), DEFAULT_ENCODING, impexFileName, false);
	}

	@Test(expected = ImpExException.class)
	public void shouldThrowExceptionDueToWrongHeaderType() throws Exception
	{
		final String impexFileName = TEST_RESOURCES_FOLDER + "/wrongHeaderType.impex";
		importStream(getClass().getResourceAsStream(impexFileName), DEFAULT_ENCODING, impexFileName, false);
	}

	@Test
	public void shouldFillAuthorInfoWithNameOnlyWithPipe() throws Exception
	{
		//Given
		importCsv(TEST_RESOURCES_FOLDER + "/authorInfoNameOnlyWithPipe.impex", DEFAULT_ENCODING);

		//When
		final ProductModel product = productService.getProductForCode(TEST_PRODUCT_CODE);

		//Then
		assertThat(product.getAuthorInfos()).hasSize(1);
		final AuthorInfoModel authorInfo = product.getAuthorInfos().iterator().next();
		AuthorInfoAssert.assertThat(authorInfo).hasNameOnly(TEST_AUTHOR_NAME_1);
	}

	@Test
	public void shouldFillAuthorInfo() throws Exception
	{
		//Given
		importCsv(TEST_RESOURCES_FOLDER + "/authorInfo1.impex", DEFAULT_ENCODING);

		//When
		final ProductModel product = productService.getProductForCode(TEST_PRODUCT_CODE);

		//Then
		assertThat(product.getAuthorInfos()).hasSize(1);
		final AuthorInfoModel authorInfo = product.getAuthorInfos().iterator().next();
		AuthorInfoAssert.assertThat(authorInfo).hasNameAndRole(TEST_AUTHOR_NAME_1, TEST_AUTHOR_ROLE_1);
	}

	@Test
	public void shouldFillTwoAuthorInfos() throws Exception
	{
		//Given
		importCsv(TEST_RESOURCES_FOLDER + "/twoAuthorInfos.impex", DEFAULT_ENCODING);

		//When
		final ProductModel product = productService.getProductForCode(TEST_PRODUCT_CODE);

		//Then
		assertThat(product.getAuthorInfos()).hasSize(2);
		AuthorInfoAssert.assertThat(product.getAuthorInfos().get(0)).hasNameAndRole(TEST_AUTHOR_NAME_1, TEST_AUTHOR_ROLE_1);
		AuthorInfoAssert.assertThat(product.getAuthorInfos().get(1)).hasNameAndRole(TEST_AUTHOR_NAME_2, TEST_AUTHOR_ROLE_2);
	}

	/**
	 * Method checks import for partof relation between Product and AuthorInfo
	 * i.e. guarantee that old instances of {@link AuthorInfoModel} are substituted and removed.
	 * Such test ensures that import won't leave {@link AuthorInfoModel} instances which are not attached to product.
	 */
	@Test
	public void shouldSubstituteOldAuthorInfo() throws Exception
	{
		//Given
		importCsv(TEST_RESOURCES_FOLDER + "/authorInfo1.impex", DEFAULT_ENCODING);

		final ProductModel product = productService.getProductForCode(TEST_PRODUCT_CODE);

		assertThat(product.getAuthorInfos()).hasSize(1);
		final AuthorInfoModel authorInfo = product.getAuthorInfos().iterator().next();
		AuthorInfoAssert.assertThat(authorInfo).hasNameAndRole(TEST_AUTHOR_NAME_1, TEST_AUTHOR_ROLE_1);

		//When
		importCsv(TEST_RESOURCES_FOLDER + "/authorInfo2.impex", DEFAULT_ENCODING);
		modelService.refresh(product);

		//Then
		assertThat(modelService.isRemoved(authorInfo)).isTrue();
		assertThat(product.getAuthorInfos()).hasSize(1);
		final AuthorInfoModel createdAuthorInfo = product.getAuthorInfos().iterator().next();
		AuthorInfoAssert.assertThat(createdAuthorInfo).hasNameAndRole(TEST_AUTHOR_NAME_2, TEST_AUTHOR_ROLE_2);
	}


	@Test
	public void shouldIgnore() throws Exception
	{
		//Given
		importCsv(TEST_RESOURCES_FOLDER + "/authorInfo1.impex", DEFAULT_ENCODING);

		final ProductModel product = productService.getProductForCode(TEST_PRODUCT_CODE);

		assertThat(product.getAuthorInfos()).hasSize(1);
		final AuthorInfoModel authorInfo = product.getAuthorInfos().iterator().next();
		AuthorInfoAssert.assertThat(authorInfo).hasNameAndRole(TEST_AUTHOR_NAME_1, TEST_AUTHOR_ROLE_1);

		//When
		importCsv(TEST_RESOURCES_FOLDER + "/ignoredAuthorInfo1.impex", DEFAULT_ENCODING);
		modelService.refresh(product);

		//Then
		assertThat(modelService.isRemoved(authorInfo)).isFalse();
		assertThat(product.getAuthorInfos()).hasSize(1);
	}

	/**
	 * Assert to check {@link AuthorInfoModel} import result
	 */
	static class AuthorInfoAssert extends GenericAssert<AuthorInfoAssert, AuthorInfoModel>
	{
		private static final Locale EN_LOCALE = Locale.ENGLISH;

		public static AuthorInfoAssert assertThat(final AuthorInfoModel authorInfo)
		{
			return new AuthorInfoAssert(authorInfo);
		}

		AuthorInfoAssert(final AuthorInfoModel authorInfo)
		{
			super(AuthorInfoAssert.class, authorInfo);
		}

		AuthorInfoAssert hasNameOnly(final String name)
		{
			Assertions.assertThat(this.actual.getName(EN_LOCALE)).isEqualTo(name);
			Assertions.assertThat(this.actual.getRole(EN_LOCALE)).isEmpty();
			return this;
		}

		AuthorInfoAssert hasNameAndRole(final String name, final String role)
		{
			Assertions.assertThat(this.actual.getName(EN_LOCALE)).isEqualTo(name);
			Assertions.assertThat(this.actual.getRole(EN_LOCALE)).isEqualTo(role);
			return this;
		}
	}

}

