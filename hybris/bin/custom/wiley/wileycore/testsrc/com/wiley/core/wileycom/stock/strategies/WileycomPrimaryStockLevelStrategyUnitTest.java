package com.wiley.core.wileycom.stock.strategies;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.InStockStatus;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Unit test for {@link WileycomPrimaryStockLevelStrategy}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileycomPrimaryStockLevelStrategyUnitTest
{

	@InjectMocks
	private WileycomPrimaryStockLevelStrategy wileycomPrimaryStockLevelStrategy;

	@Test
	public void testWhenSingleStockLevel()
	{
		//Given
		Collection<StockLevelModel> stockLevels = new ArrayList<>();
		StockLevelModel expected = createStockLevel(InStockStatus.FORCEINSTOCK, Boolean.FALSE);
		stockLevels.add(expected);

		//When
		StockLevelModel actual = wileycomPrimaryStockLevelStrategy.getPrimaryStockLevel(stockLevels);

		//Then
		assertStockLevel(expected, actual);
	}

	@Test
	public void testWhenPrimaryStockLevelFound()
	{
		//Given
		Collection<StockLevelModel> stockLevels = new ArrayList<>();
		stockLevels.add(createStockLevel(InStockStatus.FORCEOUTOFSTOCK, Boolean.FALSE));
		StockLevelModel expected = createStockLevel(InStockStatus.FORCEINSTOCK, Boolean.TRUE);
		stockLevels.add(expected);


		//When
		StockLevelModel actual = wileycomPrimaryStockLevelStrategy.getPrimaryStockLevel(stockLevels);

		//Then
		assertStockLevel(expected, actual);
	}

	@Test
	public void testWhenPrimaryStockLevelNotFound()
	{
		//Given
		Collection<StockLevelModel> stockLevels = new ArrayList<>();
		StockLevelModel expected = createStockLevel(InStockStatus.FORCEINSTOCK, Boolean.FALSE);
		stockLevels.add(expected);
		stockLevels.add(createStockLevel(InStockStatus.FORCEOUTOFSTOCK, Boolean.FALSE));

		//When
		StockLevelModel actual = wileycomPrimaryStockLevelStrategy.getPrimaryStockLevel(stockLevels);

		//Then
		assertStockLevel(expected, actual);
	}

	@Test
	public void testWhenPassedStockLevelsEmpty()
	{
		//Given
		Collection<StockLevelModel> stockLevels = new ArrayList<>();

		//When
		try
		{
			StockLevelModel actual = wileycomPrimaryStockLevelStrategy.getPrimaryStockLevel(stockLevels);
			fail("Expected UnknownIdentifierException");
		}
		catch (UnknownIdentifierException e)
		{
			//Then
			//Success
		}
	}

	private void assertStockLevel(final StockLevelModel expected, final StockLevelModel actual)
	{
		assertNotNull(actual);
		assertEquals(expected.getInStockStatus(), actual.getInStockStatus());
		assertEquals(expected.getPrimary(), actual.getPrimary());
	}

	private StockLevelModel createStockLevel(final InStockStatus inStockStatus, final Boolean primary)
	{
		StockLevelModel stockLevel = new StockLevelModel();
		stockLevel.setInStockStatus(inStockStatus);
		stockLevel.setPrimary(primary);
		return stockLevel;
	}
}
