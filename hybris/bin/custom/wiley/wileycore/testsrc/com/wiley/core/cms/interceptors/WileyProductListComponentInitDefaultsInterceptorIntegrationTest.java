package com.wiley.core.cms.interceptors;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cms2lib.model.components.ProductListComponentModel;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Before;
import org.junit.Test;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.model.actions.AddToLegacyCartActionModel;
import com.wiley.core.model.components.WileyProductListComponentModel;
import com.wiley.core.model.components.WileyPurchaseOptionProductListComponentModel;

import static com.wiley.core.constants.WileyCoreConstants.WILEY_ADD_TO_LEGACY_CART_ACTION_UID;


/**
 * Created by Uladzimir_Barouski on 5/19/2017.
 */
@IntegrationTest
public class WileyProductListComponentInitDefaultsInterceptorIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	public static final String CONTENT_CATALOG = "wileyb2cContentCatalog";
	public static final String STAGED_CATALOG_VERSION = "Staged";
	public static final String ONLINE_CATALOG_VERSION = "Online";

	@Resource
	private ModelService modelService;

	@Resource
	private CatalogVersionService catalogVersionService;

	@Before
	public void setUp() throws Exception
	{
		final CatalogVersionModel stagedCatalogVersion = catalogVersionService.getCatalogVersion(CONTENT_CATALOG,
				STAGED_CATALOG_VERSION);
		final CatalogVersionModel onlineCatalogVersion = catalogVersionService.getCatalogVersion(CONTENT_CATALOG,
				ONLINE_CATALOG_VERSION);
		catalogVersionService.setSessionCatalogVersions(Arrays.asList(stagedCatalogVersion, onlineCatalogVersion));
	}

	@Test
	public void onInitWileyProductListComponentWhenActionExist() throws Exception
	{
		//Given
		initActions();

		//When
		WileyProductListComponentModel wileyProductListComponent = modelService.create(WileyProductListComponentModel.class);

		//Then
		assertTrue(CollectionUtils.isNotEmpty(wileyProductListComponent.getActions()));
		assertEquals(1, wileyProductListComponent.getActions().size());
		assertEquals(WILEY_ADD_TO_LEGACY_CART_ACTION_UID, wileyProductListComponent.getActions().get(0).getUid());
	}

	@Test
	public void onWileyProductListComponentWhenActionNotExist() throws Exception
	{
		//When
		WileyProductListComponentModel wileyProductListComponent = modelService.create(WileyProductListComponentModel.class);

		//Then
		assertTrue(CollectionUtils.isEmpty(wileyProductListComponent.getActions()));
	}

	@Test
	public void onInitwileyPurchaseOptionProductListComponentWhenActionExist() throws Exception
	{
		//Given
		initActions();

		//When
		final WileyPurchaseOptionProductListComponentModel wileyPurchaseOptionProductListComponentModel = modelService
				.create(WileyPurchaseOptionProductListComponentModel.class);

		//Then
		assertTrue(CollectionUtils.isNotEmpty(wileyPurchaseOptionProductListComponentModel.getActions()));
		assertEquals(1, wileyPurchaseOptionProductListComponentModel.getActions().size());
		assertEquals(WILEY_ADD_TO_LEGACY_CART_ACTION_UID,
				wileyPurchaseOptionProductListComponentModel.getActions().get(0).getUid());
	}

	@Test
	public void onwileyPurchaseOptionProductListComponentWhenActionNotExist() throws Exception
	{
		//When
		final WileyPurchaseOptionProductListComponentModel wileyPurchaseOptionProductListComponentModel = modelService
				.create(WileyPurchaseOptionProductListComponentModel.class);

		//Then
		assertTrue(CollectionUtils.isEmpty(wileyPurchaseOptionProductListComponentModel.getActions()));
	}

	@Test
	public void onInitProductListComponent() throws Exception
	{
		//Given
		initActions();

		//When
		ProductListComponentModel productListComponent = modelService.create(ProductListComponentModel.class);

		//Then
		assertTrue(CollectionUtils.isEmpty(productListComponent.getActions()));
	}

	private void initActions()
	{
		initActionForCatalogVersion(CONTENT_CATALOG, STAGED_CATALOG_VERSION);
		initActionForCatalogVersion(CONTENT_CATALOG, ONLINE_CATALOG_VERSION);
	}

	private void initActionForCatalogVersion(final String catalogId, final String catalogVersion)
	{
		AddToLegacyCartActionModel addToLegacyCartAction = modelService.create(AddToLegacyCartActionModel.class);
		addToLegacyCartAction.setUid(WILEY_ADD_TO_LEGACY_CART_ACTION_UID);
		final CatalogVersionModel activeCatalogVersion = catalogVersionService.getCatalogVersion(catalogId, catalogVersion);
		addToLegacyCartAction.setCatalogVersion(activeCatalogVersion);
		modelService.save(addToLegacyCartAction);
	}
}