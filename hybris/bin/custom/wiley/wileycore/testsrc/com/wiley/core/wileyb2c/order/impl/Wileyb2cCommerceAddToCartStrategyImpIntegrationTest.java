package com.wiley.core.wileyb2c.order.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.impl.DefaultCommerceAddToCartStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import static org.junit.Assert.assertTrue;

import javax.annotation.Resource;

import org.junit.Test;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.wileycom.order.impl.WileycomCommerceAddToCartStrategyImpIntegrationTest;


/**
 * @author Dzmitryi_Halahayeu
 */
@IntegrationTest
public class Wileyb2cCommerceAddToCartStrategyImpIntegrationTest extends WileycomCommerceAddToCartStrategyImpIntegrationTest
{
	private static final String NO_PRICES_PRODUCT = "B2C_NO_PRICES_PRODUCT";

	@Resource
	private Wileyb2cCommerceAddToCartStrategyImpl wileyb2cCommerceAddToCartStrategyImpl;
	@Resource
	private ProductService productService;


	@Test
	public void testFailWhenNonPricesRestrictionApply() throws CommerceCartModificationException
	{
		final ProductModel nonPurchasableProduct = productService.getProductForCode(NO_PRICES_PRODUCT);
		final CommerceCartParameter commerceCartParameter = createCartParameter(nonPurchasableProduct);

		boolean isFailed = false;
		try
		{
			wileyb2cCommerceAddToCartStrategyImpl.addToCart(commerceCartParameter);
		}
		catch (final CommerceCartModificationException exception)
		{
			isFailed = true;
			checkException(exception, WileyCoreConstants.PRODUCT_IS_NOT_PURCHASABLE_FOR_COUNTRY, "has no configured price");
		}

		assertTrue(isFailed);
	}


	@Override
	protected String getSiteId()
	{
		return "wileyb2c";
	}

	@Override
	protected String getRestrictedAssortmentProductCode()
	{
		return "B2B_ONLY_PRODUCT";
	}

	@Override
	protected DefaultCommerceAddToCartStrategy getAddToCartStrategy()
	{
		return wileyb2cCommerceAddToCartStrategyImpl;
	}


}
