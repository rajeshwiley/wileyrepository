package com.wiley.core.wiley.order.impl;

import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.commerceservices.strategies.CartValidationStrategy;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.ServicelayerTest;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import static org.junit.Assert.assertEquals;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.model.WileyProductModel;


/**
 * @author Dzmitryi_Halahayeu
 */
@Ignore
public abstract class WileyCommerceCartValidationStrategyIntegrationTest extends ServicelayerTest
{
	private static final String USD_CURRENCY = "USD";
	private static final String TEST_GROUP = "testgroup";
	private static final String SUCCESS_STATUS = "success";
	@Resource
	private UserService userService;
	@Resource
	private CommonI18NService commonI18NService;
	@Resource
	private ProductService productService;
	@Resource
	private ModelService modelService;
	@Resource
	private BaseSiteService baseSiteService;

	@Before
	public void setUp() throws Exception
	{
		baseSiteService.setCurrentBaseSite(getSiteId(), true);
	}

	protected abstract String getSiteId();

	@Test
	public void testWhenProductHasIncorrectUserGroupShouldRemoveCartEntryAndAddFailures() throws CommerceCartModificationException
	{
		final WileyProductModel product = (WileyProductModel) productService.getProductForCode(
				getRestrictedUserGroupProductCode());
		final CommerceCartParameter commerceCartParameter = createCartParameter(product);


		final List<CommerceCartModification> cartModifications = getCartValidationStrategy().validateCart(
				commerceCartParameter);

		assertEquals(commerceCartParameter.getCart().getEntries().size(), 0);
		assertEquals(cartModifications.size(), 1);
		final CommerceCartModification commerceCartModification = cartModifications.get(0);
		assertEquals(commerceCartModification.getMessage(), WileyCoreConstants.BASKET_VALIDATION_UNVAILABLE);
		assertEquals(commerceCartModification.getMessageParameters().length, 1);
		assertEquals(commerceCartModification.getMessageParameters()[0], product.getName());
	}

	@Test
	public void testWhenProductHasCorrectUserGroupShouldNotRemoveCartEntryAndDoNotHaveFailures()
	{
		final WileyProductModel product = (WileyProductModel) productService.getProductForCode(
				getRestrictedUserGroupProductCode());
		final UserModel currentUser = userService.getCurrentUser();
		currentUser.setGroups(Collections.singleton(userService.getUserGroupForUID(TEST_GROUP)));
		final CommerceCartParameter commerceCartParameter = createCartParameter(product);

		final List<CommerceCartModification> cartModifications = getCartValidationStrategy().validateCart(
				commerceCartParameter);

		assertEquals(commerceCartParameter.getCart().getEntries().size(), 1);
		assertEquals(cartModifications.stream().filter(commerceCartModification -> !SUCCESS_STATUS.equals(
				commerceCartModification.getStatusCode())).collect(Collectors.toList()).size(), 0);
	}

	protected CommerceCartParameter createCartParameter(final ProductModel product)
	{
		final UserModel user = userService.getCurrentUser();
		final CommerceCartParameter commerceCartParameter = new CommerceCartParameter();
		commerceCartParameter.setEnableHooks(true);
		commerceCartParameter.setProduct(product);
		final CartModel cart = new CartModel();
		cart.setUser(user);
		cart.setCurrency(commonI18NService.getCurrency(USD_CURRENCY));
		cart.setDate(new Date());
		cart.setNet(Boolean.TRUE);
		final CartEntryModel cartEntryModel = new CartEntryModel();
		cartEntryModel.setBasePrice(44.0);
		cartEntryModel.setQuantity(1L);
		cartEntryModel.setProduct(product);
		cartEntryModel.setOrder(cart);
		cartEntryModel.setUnit(product.getUnit());
		modelService.save(cartEntryModel);
		cart.setEntries(Collections.singletonList(cartEntryModel));
		modelService.save(cart);
		commerceCartParameter.setCart(cart);
		commerceCartParameter.setQuantity(1);
		commerceCartParameter.setUser(user);
		return commerceCartParameter;
	}

	protected abstract CartValidationStrategy getCartValidationStrategy();

	protected abstract String getRestrictedUserGroupProductCode();



}
