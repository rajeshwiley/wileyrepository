package com.wiley.core.wileyb2c.order.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.order.CommerceCartCalculationStrategy;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartModificationStatus;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.commerceservices.stock.CommerceStockService;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.subscriptionservices.model.SubscriptionTermModel;
import de.hybris.platform.variants.model.VariantProductModel;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.enums.WileyProductSubtypeEnum;
import com.wiley.core.model.WileyProductModel;
import com.wiley.core.wiley.strategies.cart.validation.WileyCartParameterValidator;
import com.wiley.core.wileyb2c.subscription.services.Wileyb2cFreeTrialSubscriptionTermCheckingStrategy;
import com.wiley.core.wileyb2c.subscription.services.Wileyb2cSubscriptionService;

import static java.util.Collections.singletonList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;


//TODO: Review this test before commit
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cCommerceAddToCartStrategyImplUnitTest
{

	//####
	@Mock
	private Wileyb2cSubscriptionService wileyb2cSubscriptionService;

	@Mock
	private Wileyb2cFreeTrialSubscriptionTermCheckingStrategy wileyb2cFreeTrialSubscriptionCheckingStrategy;

	@Mock
	private WileyCartParameterValidator wileyCartParameterValidatorMock;

	@InjectMocks
	private Wileyb2cCommerceAddToCartStrategyImpl wileyb2cCommerceAddToCartStrategy = new Wileyb2cCommerceAddToCartStrategyImpl();

	@Mock
	private ProductService productServiceMock;

	@Mock
	private ModelService modelServiceMock;

	@Mock
	private CartService cartServiceMock;

	@Mock
	private BaseStoreService baseStoreServiceMock;

	@Mock
	private CommerceStockService commerceStockServiceMock;

	@Mock
	private CommerceCartCalculationStrategy commerceCartCalculationStrategyMock;

	// Test data
	@Mock
	private CartModel cartModelMock;
	@Mock
	private WileyProductModel productModelMock;
	@Mock
	private BaseStoreModel baseStoreModelMock;
	@Mock
	private VariantProductModel variantProductModelMock;
	@Mock
	private CartEntryModel cartEntryModelMock;

	@Before
	public void setUp() throws Exception
	{
		when(cartServiceMock.getEntriesForProduct(eq(cartModelMock), eq(productModelMock))).thenReturn(Collections.emptyList());

		when(baseStoreServiceMock.getCurrentBaseStore()).thenReturn(baseStoreModelMock);
		when(commerceStockServiceMock.isStockSystemEnabled(eq(baseStoreModelMock))).thenReturn(false);
		wileyb2cCommerceAddToCartStrategy.setCartParameterValidators(Collections.singletonList(wileyCartParameterValidatorMock));
	}


	@Test
	public void shouldNotThrowExceptionIfProductAdded() throws CommerceCartModificationException
	{
		// Given
		CommerceCartParameter parameter = createCommerceCartParameter(productModelMock, cartModelMock);
		when(productModelMock.getVariants()).thenReturn(Collections.singleton(variantProductModelMock));

		// When
		final CommerceCartModification commerceCartModification = wileyb2cCommerceAddToCartStrategy.addToCart(parameter);

		// Then
		assertNotNull(commerceCartModification);
		assertEquals(CommerceCartModificationStatus.SUCCESS, commerceCartModification.getStatusCode());
	}

	@Test
	public void shouldAllowAddToCartIgnoringStockLevel()
	{
		final long allowedCartAdjustmentForProduct = wileyb2cCommerceAddToCartStrategy.getAllowedCartAdjustmentForProduct(null,
				null, 2, null);
		assertThat(allowedCartAdjustmentForProduct, is(2L));
	}

	@Test
	public void shouldSaveExtraInfoIntoOrderEntry() throws Exception
	{
		// Given
		CommerceCartParameter parameter = createCommerceCartParameter(productModelMock, cartModelMock);
		when(cartServiceMock
				.addNewEntry(eq(cartModelMock), eq(productModelMock), anyLong(), any(UnitModel.class), anyInt(), anyBoolean()))
				.thenReturn(cartEntryModelMock);
		final String additionalInfo = "param_1";
		parameter.setAdditionalInfo(additionalInfo);

		// When
		wileyb2cCommerceAddToCartStrategy.addToCart(parameter);

		// Then
		verify(cartEntryModelMock).setAdditionalInfo(same(additionalInfo));
	}

	@Test
	public void testShouldRemoveOldSubscriptionProductFromCart() throws Exception
	{
		// Given
		CommerceCartParameter parameter = createCommerceCartParameter(productModelMock, cartModelMock);
		when(productModelMock.getSubtype()).thenReturn(WileyProductSubtypeEnum.SUBSCRIPTION);
		final CartEntryModel cartEntryMock = mock(CartEntryModel.class);
		final SubscriptionTermModel originalTerm = mock(SubscriptionTermModel.class);
		final SubscriptionTermModel newTerm = mock(SubscriptionTermModel.class);
		parameter.setSubscriptionTerm(newTerm);
		when(cartEntryMock.getSubscriptionTerm()).thenReturn(originalTerm);
		when(cartServiceMock.getEntriesForProduct(cartModelMock, productModelMock)).thenReturn(singletonList(cartEntryMock));

		// When
		wileyb2cCommerceAddToCartStrategy.addToCart(parameter);

		// Then
		verify(modelServiceMock).remove(cartEntryMock);
		verify(modelServiceMock).refresh(cartModelMock);
	}


	private CommerceCartParameter createCommerceCartParameter(final ProductModel productModel, final CartModel cartModel)
	{
		CommerceCartParameter parameter = new CommerceCartParameter();
		parameter.setProduct(productModel);
		parameter.setCart(cartModel);
		parameter.setQuantity(1);
		return parameter;
	}

}