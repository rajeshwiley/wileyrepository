package com.wiley.core.mpgs.services;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;

import static org.junit.Assert.assertSame;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyTnsMerchantModel;
import com.wiley.core.mpgs.response.WileyRetrieveSessionResponse;
import com.wiley.core.mpgs.services.impl.WileyMPGSPaymentServiceImpl;
import com.wiley.core.payment.tnsmerchant.service.WileyTnsMerchantService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyMPGSPaymentServiceImplRetrieveSessionTest
{
	private static final String SESSION_ID = "SESSION_ID";
	private static final String RETRIEVE_SESSION_URL = "RETRIEVE_SESSION_URL";
	private static final String TEST_MERCHANT_ID = "TEST_MERCHANT_ID";
	private static final String TEST_ISO_CODE = "US";
	private static final String TEST_CURRENCY_ISO_CODE = "USA";
	private static final String AS_SITE_ID = "asSite";
	private static final String TEST_CODE = "a19b9a12-87c9-4c75-b35e-d943f569e59d";

	@Mock
	private AbstractOrderModel cart;
	@Mock
	private WileyCardPaymentService wileyCardPaymentService;
	@Mock
	private WileyUrlGenerationService wileyUrlGenerationService;

	@Mock
	private PaymentTransactionModel paymentTransaction;
	@Mock
	private WileyRetrieveSessionResponse retrieveSessionResponse;
	@Mock
	private PaymentTransactionEntryModel retriveSessionEntry;
	@Mock
	private WileyMPGSPaymentEntryService wileyMPGSPaymentEntryService;
	@Mock
	private WileyMPGSMerchantService wileyMPGSMerchantService;
	@Mock
	private WileyTnsMerchantService wileyTnsMerchantService;
	@Mock
	private AddressModel paymentAddress;
	@Mock
	private CountryModel countryModel;
	@Mock
	private BaseSiteModel baseSiteModel;
	@Mock
	private WileyTnsMerchantModel wileyTnsMerchantMock;

	@InjectMocks
	private WileyMPGSPaymentServiceImpl mpgsPaymentService = new WileyMPGSPaymentServiceImpl();

	@Test
	public void successResult()
	{
		when(wileyUrlGenerationService.getRetrieveSessionUrl(TEST_MERCHANT_ID, SESSION_ID)).thenReturn(RETRIEVE_SESSION_URL);
		when(wileyCardPaymentService.retrieveSession(any())).thenReturn(retrieveSessionResponse);
		when(wileyMPGSPaymentEntryService.createPaymentTransaction(cart)).thenReturn(paymentTransaction);
		when(wileyMPGSPaymentEntryService.createRetrieveSessionEntry(retrieveSessionResponse, paymentTransaction)).thenReturn(
				retriveSessionEntry);
		when(paymentTransaction.getCode()).thenReturn(TEST_CODE);
		when(cart.getSite()).thenReturn(baseSiteModel);
		when(baseSiteModel.getUid()).thenReturn(AS_SITE_ID);
		when(cart.getPaymentAddress()).thenReturn(paymentAddress);
		when(paymentAddress.getCountry()).thenReturn(countryModel);
		when(countryModel.getIsocode()).thenReturn(TEST_ISO_CODE);
		when(wileyTnsMerchantService.getTnsMerchant(TEST_ISO_CODE, TEST_CURRENCY_ISO_CODE)).thenReturn(wileyTnsMerchantMock);
		when(wileyTnsMerchantMock.getId()).thenReturn(TEST_MERCHANT_ID);
		when(paymentTransaction.getCode()).thenReturn(TEST_CODE);
		PaymentTransactionEntryModel entry = mpgsPaymentService.retrieveSession(cart, SESSION_ID);

		assertSame(retriveSessionEntry, entry);
		verify(wileyMPGSPaymentEntryService).createPaymentTransaction(cart);
		verify(wileyMPGSPaymentEntryService).createRetrieveSessionEntry(retrieveSessionResponse, paymentTransaction);
	}


	@Test(expected = IllegalArgumentException.class)
	public void passNullParameter()
	{
		mpgsPaymentService.retrieveSession(null, null);
	}
}
