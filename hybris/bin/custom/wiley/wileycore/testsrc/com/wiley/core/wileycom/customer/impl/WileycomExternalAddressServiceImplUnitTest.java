package com.wiley.core.wileycom.customer.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.wiley.core.integration.esb.EsbExternalAddressGateway;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Created by Uladzimir_Barouski on 7/12/2016.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileycomExternalAddressServiceImplUnitTest
{
	public static final String CUSTOMER_ID = "customerID";
	@InjectMocks
	private WileycomExternalAddressServiceImpl wileycomExternalAddressService;

	@Mock
	private EsbExternalAddressGateway esbExternalAddressGatewayMock;

	@Mock
	private CustomerAccountService customerAccountService;

	@Before
	public void setUp() throws Exception
	{
		doNothing().when(customerAccountService).setDefaultAddressEntry(any(CustomerModel.class), any(AddressModel.class));
		doNothing().when(customerAccountService).saveAddressEntry(any(CustomerModel.class), any(AddressModel.class));
		doNothing().when(customerAccountService).clearDefaultAddressEntry(any(CustomerModel.class));
	}

	@Test
	public void addShippingAddress()
	{
		final CustomerModel customerModel = new CustomerModel();
		customerModel.setCustomerID(CUSTOMER_ID);
		final AddressModel addressModel = new AddressModel();
		addressModel.setShippingAddress(true);
		addressModel.setBillingAddress(false);
		addressModel.setVisibleInAddressBook(true);

		wileycomExternalAddressService.addShippingAddress(customerModel, addressModel, false);

		assertNotNull(addressModel.getExternalId());
		verify(esbExternalAddressGatewayMock, times(1)).addShippingAddress(addressModel, customerModel);
		verify(esbExternalAddressGatewayMock, times(0)).addBillingAddress(addressModel, customerModel);
	}

	@Test
	public void addShippingAddressWhenNotVisibleInAddressBook()
	{
		final CustomerModel customerModel = new CustomerModel();
		customerModel.setCustomerID(CUSTOMER_ID);
		final AddressModel addressModel = new AddressModel();
		addressModel.setShippingAddress(true);
		addressModel.setBillingAddress(false);
		addressModel.setVisibleInAddressBook(false);

		wileycomExternalAddressService.addShippingAddress(customerModel, addressModel, false);

		assertNull(addressModel.getExternalId());
		verify(esbExternalAddressGatewayMock, times(0)).addShippingAddress(addressModel, customerModel);
		verify(esbExternalAddressGatewayMock, times(0)).addBillingAddress(addressModel, customerModel);
	}

	@Test
	public void addBillingAddress()
	{
		final CustomerModel customerModel = new CustomerModel();
		customerModel.setCustomerID(CUSTOMER_ID);
		final AddressModel addressModel = new AddressModel();
		addressModel.setShippingAddress(false);
		addressModel.setBillingAddress(true);
		addressModel.setVisibleInAddressBook(true);

		wileycomExternalAddressService.addShippingAddress(customerModel, addressModel, false);

		assertNotNull(addressModel.getExternalId());
		verify(esbExternalAddressGatewayMock, times(1)).addBillingAddress(addressModel, customerModel);
		verify(esbExternalAddressGatewayMock, times(0)).addShippingAddress(addressModel, customerModel);
	}

	@Test(expected = IllegalArgumentException.class)
	public void addAddressThrowExceptionIfCustomerIDEmpty()
	{
		final CustomerModel customerModel = new CustomerModel();
		final AddressModel addressModel = new AddressModel();

		wileycomExternalAddressService.addShippingAddress(customerModel, addressModel, false);
	}

	@Test
	public void updateShippingAddress()
	{
		final CustomerModel customerModel = new CustomerModel();
		customerModel.setCustomerID(CUSTOMER_ID);
		final AddressModel addressModel = new AddressModel();
		addressModel.setShippingAddress(true);
		addressModel.setBillingAddress(false);

		wileycomExternalAddressService.updateShippingAddress(customerModel, addressModel, true);

		verify(esbExternalAddressGatewayMock, times(1)).updateShippingAddress(addressModel, customerModel);
		verify(esbExternalAddressGatewayMock, times(0)).updateBillingAddress(addressModel, customerModel);
		verify(customerAccountService, times(1)).setDefaultAddressEntry(customerModel, addressModel);
		verify(customerAccountService, times(0)).clearDefaultAddressEntry(customerModel);
	}

	@Test
	public void updateShippingAddressAndClearDefault()
	{
		final CustomerModel customerModel = new CustomerModel();
		customerModel.setCustomerID(CUSTOMER_ID);
		final AddressModel addressModel = new AddressModel();
		addressModel.setShippingAddress(true);
		addressModel.setBillingAddress(false);
		customerModel.setDefaultShipmentAddress(addressModel);

		wileycomExternalAddressService.updateShippingAddress(customerModel, addressModel, false);

		verify(esbExternalAddressGatewayMock, times(1)).updateShippingAddress(addressModel, customerModel);
		verify(esbExternalAddressGatewayMock, times(0)).updateBillingAddress(addressModel, customerModel);
		verify(customerAccountService, times(1)).clearDefaultAddressEntry(customerModel);
	}

	@Test
	public void updateBillingAddress()
	{
		final CustomerModel customerModel = new CustomerModel();
		customerModel.setCustomerID(CUSTOMER_ID);
		final AddressModel addressModel = new AddressModel();
		addressModel.setShippingAddress(false);
		addressModel.setBillingAddress(true);

		wileycomExternalAddressService.updateShippingAddress(customerModel, addressModel, false);

		verify(esbExternalAddressGatewayMock, times(1)).updateBillingAddress(addressModel, customerModel);
		verify(esbExternalAddressGatewayMock, times(0)).updateShippingAddress(addressModel, customerModel);
	}

	@Test(expected = IllegalArgumentException.class)
	public void updateAddressThrowExceptionIfCustomerIDEmpty()
	{
		final CustomerModel customerModel = new CustomerModel();
		final AddressModel addressModel = new AddressModel();

		wileycomExternalAddressService.updateShippingAddress(customerModel, addressModel, false);
	}
}