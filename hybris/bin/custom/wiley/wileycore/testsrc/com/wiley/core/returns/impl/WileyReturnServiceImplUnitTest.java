package com.wiley.core.returns.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.any;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.ImmutableMap;
import com.wiley.core.event.StartExportProcessEvent;
import com.wiley.core.event.WileyOrderRefundEvent;
import com.wiley.core.model.WileyGiftCardProductModel;
import com.wiley.core.model.WileyProductModel;
import com.wiley.core.refund.impl.WileyRefundServiceImpl;



@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyReturnServiceImplUnitTest
{
	private static final long ENTRY_QUANTITY = 2L;
	private static final long GIFT_CARD_ENTRY_QUANTITY = 3L;
	private static final BigDecimal TOTAL_REFUND_AMOUNT = BigDecimal.valueOf(65.35);

	@Mock
	private OrderModel orderModel;

	@Mock
	private AbstractOrderEntryModel orderEntry;
	@Mock
	private AbstractOrderEntryModel giftCardOrderEntry;
	@Mock
	private WileyGiftCardProductModel giftCardProduct;
	@Mock
	private WileyProductModel product;
	@Mock
	private ReturnRequestModel request;
	@Mock
	private EventService eventService;
	@Mock
	private WileyRefundServiceImpl refundService;
	@Mock
	private PaymentTransactionEntryModel paymentTransactionEntryModel;
	@Mock
	private ModelService modelService;

	private BaseMatcher<WileyOrderRefundEvent> wileyOrderRefundEventMatcher = new BaseMatcher<WileyOrderRefundEvent>()
	{
		@Override
		public boolean matches(final Object item)
		{
			if (item instanceof WileyOrderRefundEvent)
			{
				final WileyOrderRefundEvent event = (WileyOrderRefundEvent) item;
				if (event.getReturnRequest().equals(request))
				{
					return true;
				}
			}
			return false;
		}

		@Override
		public void describeTo(final Description description)
		{
			description.appendText("Published events do not match");

		}
	};
	@InjectMocks
	private WileyReturnServiceImpl returnService = new WileyReturnServiceImpl();

	@Before
	public void setUp()
	{
		when(giftCardOrderEntry.getQuantity()).thenReturn(GIFT_CARD_ENTRY_QUANTITY);
		when(giftCardOrderEntry.getProduct()).thenReturn(giftCardProduct);
		when(orderEntry.getQuantity()).thenReturn(ENTRY_QUANTITY);
		when(orderEntry.getProduct()).thenReturn(product);

		when(refundService.processRefundPaymentTransaction(orderModel, TOTAL_REFUND_AMOUNT)).thenReturn(
				paymentTransactionEntryModel);
		when(paymentTransactionEntryModel.getTransactionStatus()).thenReturn(TransactionStatus.ACCEPTED.toString());
		when(request.getOrder()).thenReturn(orderModel);
	}

	@Test(expected = NullPointerException.class)
	public void setRefundAmountToReturnRequestShouldThrowNpeIfReturnEntryIsNull() throws Exception
	{
		//Testing
		returnService.setRefundAmountToReturnRequest(null, BigDecimal.TEN);
	}

	@Test
	public void shouldNotReturnGiftCardsEntries()
	{
		//Given
		when(orderModel.getEntries()).thenReturn(Arrays.asList(giftCardOrderEntry, orderEntry));
		//When
		final Map<AbstractOrderEntryModel, Long> allReturnableEntries = returnService.getAllReturnableEntries(orderModel);
		//Then
		assertEquals(ImmutableMap.of(orderEntry, ENTRY_QUANTITY), allReturnableEntries);
	}

	@Test
	public void shouldReturnEmptyMapIfOrderHasNoEntries()
	{
		//Given
		when(orderModel.getEntries()).thenReturn(Arrays.asList());
		//When
		final Map<AbstractOrderEntryModel, Long> allReturnableEntries = returnService.getAllReturnableEntries(orderModel);
		//Then
		assertEquals(new HashMap<>(), allReturnableEntries);
	}

	@Test
	public void successfulFullRefund()
	{
		when(refundService.isFullyRefundedOrder(orderModel)).thenReturn(true);
		//Testing
		returnService.postRefundProcess(paymentTransactionEntryModel, request);

		//Verify
		verify(eventService).publishEvent(Mockito.argThat(new BaseMatcher<StartExportProcessEvent>()
		{

			@Override
			public boolean matches(final Object item)
			{
				if (item instanceof StartExportProcessEvent)
				{
					final StartExportProcessEvent event = (StartExportProcessEvent) item;
					if (event.getOrder().equals(orderModel))
					{
						return true;
					}
				}
				return false;
			}

			@Override
			public void describeTo(final Description description)
			{
				description.appendText("Published events do not match");

			}
		}));

		verify(eventService).publishEvent(Mockito.argThat(wileyOrderRefundEventMatcher));
	}

	@Test
	public void failedFullRefund()
	{

		when(refundService.isFullyRefundedOrder(orderModel)).thenReturn(true);
		when(paymentTransactionEntryModel.getTransactionStatus()).thenReturn(TransactionStatus.ERROR.toString());
		//Testing
		returnService.postRefundProcess(paymentTransactionEntryModel, request);
		//Verify
		verify(eventService, never()).publishEvent(any(AbstractEvent.class));
	}

	@Test
	public void failedPartialRefund()
	{

		when(refundService.isFullyRefundedOrder(orderModel)).thenReturn(false);
		when(paymentTransactionEntryModel.getTransactionStatus()).thenReturn(TransactionStatus.ERROR.toString());
		//Testing
		returnService.postRefundProcess(paymentTransactionEntryModel, request);
		//Verify
		verify(eventService, never()).publishEvent(any(AbstractEvent.class));
	}

	@Test
	public void successfulPartialRefund()
	{
		when(refundService.isFullyRefundedOrder(orderModel)).thenReturn(false);
		//Testing
		returnService.postRefundProcess(paymentTransactionEntryModel, request);
		//Verify
		verify(eventService, times(1)).publishEvent(Mockito.argThat(wileyOrderRefundEventMatcher));
	}
}