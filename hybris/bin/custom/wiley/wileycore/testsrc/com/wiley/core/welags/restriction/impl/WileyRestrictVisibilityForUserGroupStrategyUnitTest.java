package com.wiley.core.welags.restriction.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.variants.model.VariantProductModel;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.wiley.restriction.impl.WileyRestrictVisibilityForUserGroupStrategy;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyRestrictVisibilityForUserGroupStrategyUnitTest
{
	@InjectMocks
	private WileyRestrictVisibilityForUserGroupStrategy wileyRestrictVisibilityForUserGroupStrategy;
	@Mock
	private UserService userService;
	@Mock
	private ProductModel productModel;
	@Mock
	private VariantProductModel variantMock1, variantMock2, variantMock3;
	@Mock
	private UserGroupModel oneUserGroup;
	@Mock
	private UserGroupModel secondUserGroup;
	@Mock
	private UserModel user;
	private CommerceCartParameter parameter;


	@Before
	public void before()
	{
		parameter = new CommerceCartParameter();
		parameter.setProduct(productModel);
		when(userService.getCurrentUser()).thenReturn(user);
	}

	@Test
	public void shouldBeRestrictedWhenProductDontUserGroup() throws CommerceCartModificationException
	{
		when(productModel.getUserGroups()).thenReturn(Collections.singletonList(oneUserGroup));
		when(userService.getAllUserGroupsForUser(user)).thenReturn(Collections.singleton(secondUserGroup));

		final boolean restricted = wileyRestrictVisibilityForUserGroupStrategy.isRestricted(parameter);

		assertTrue(restricted);
	}

	@Test
	public void shouldBeRestrictedWhenAllVariantsRestricted() throws CommerceCartModificationException
	{
		when(productModel.getVariants()).thenReturn(Arrays.asList(variantMock1, variantMock2));
		when(productModel.getUserGroups()).thenReturn(Collections.emptyList());
		when(variantMock1.getUserGroups()).thenReturn(Collections.singletonList(oneUserGroup));
		when(variantMock2.getUserGroups()).thenReturn(Collections.singletonList(oneUserGroup));
		when(variantMock3.getUserGroups()).thenReturn(Collections.singletonList(oneUserGroup));
		when(userService.getAllUserGroupsForUser(user)).thenReturn(Collections.singleton(secondUserGroup));

		final boolean restricted = wileyRestrictVisibilityForUserGroupStrategy.isRestricted(parameter);

		assertTrue(restricted);
	}

	@Test
	public void shouldBeNotRestrictedWhenAnyVariantNotRestricted() throws CommerceCartModificationException
	{
		when(productModel.getVariants()).thenReturn(Arrays.asList(variantMock1, variantMock2));
		when(productModel.getUserGroups()).thenReturn(Collections.emptyList());
		when(variantMock1.getUserGroups()).thenReturn(Collections.singletonList(oneUserGroup));
		when(variantMock2.getUserGroups()).thenReturn(Collections.emptyList());
		when(variantMock3.getUserGroups()).thenReturn(Collections.singletonList(oneUserGroup));
		when(userService.getAllUserGroupsForUser(user)).thenReturn(Collections.singleton(secondUserGroup));

		final boolean restricted = wileyRestrictVisibilityForUserGroupStrategy.isRestricted(parameter);

		assertFalse(restricted);
	}

	@Test
	public void shouldBeNotRestrictedWhenProductDontHaveUserGroups() throws CommerceCartModificationException
	{
		when(productModel.getUserGroups()).thenReturn(Collections.emptyList());

		final boolean restricted = wileyRestrictVisibilityForUserGroupStrategy.isRestricted(parameter);

		assertFalse(restricted);
	}

	@Test
	public void shouldBeNotRestrictedWhenProductHaveUserGroup() throws CommerceCartModificationException
	{
		when(productModel.getUserGroups()).thenReturn(Collections.singletonList(oneUserGroup));
		when(userService.getAllUserGroupsForUser(user)).thenReturn(Collections.singleton(oneUserGroup));

		final boolean restricted = wileyRestrictVisibilityForUserGroupStrategy.isRestricted(parameter);

		assertFalse(restricted);
	}

}
