package com.wiley.core.ags.order.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.commerceservices.order.CommerceCartCalculationStrategy;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.site.BaseSiteService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;


@IntegrationTest
public class AgsSingleSubscriptionAddToCartStrategyIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	private static final String CART_CODE = "agsTestCart";
	private static final String EMPTY_CART_CODE = "agsTestEmptyCart";
	private static final String PRODUCT_CODE = "agsTestSubscriptionProduct";
	private static final Double SUBTOTAL_WITHOUT_DISCOUNT = 1000.0;
	private static final Long QUANTITY = 1L;
	private static final String BASE_SITE = "ags";
	private static final String CURRENCY_USD = "USD";

	@Resource
	private AgsSingleSubscriptionAddToCartStrategy agsSingleSubscriptionAddToCartStrategy;

	@Resource
	private CommerceCartCalculationStrategy welAgsCommerceCartCalculationStrategy;

	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Resource
	private BaseSiteService baseSiteService;

	@Resource
	private CommonI18NService commonI18NService;

	@Before
	public void setUp() throws ImpExException
	{
		baseSiteService.setCurrentBaseSite(baseSiteService.getBaseSiteForUID(BASE_SITE), true);
		CurrencyModel currency = commonI18NService.getCurrency(CURRENCY_USD);
		commonI18NService.setCurrentCurrency(currency);
		importCsv("/test/testAgsSingleSubscriptionAddToCartStrategy.impex", "utf-8");
	}

	@Test
	public void shouldAddProductToEmptyCartAndCalculate() throws CommerceCartModificationException
	{
		final CartModel cart = getCartForCode(EMPTY_CART_CODE);
		final ProductModel product = getProductForCode(PRODUCT_CODE);
		final CommerceCartParameter parameter = createCommerceCartParameter(cart, product);

		agsSingleSubscriptionAddToCartStrategy.addToCart(parameter);

		verifyCart(cart);
	}

	@Test
	public void shouldAddProductToNonEmptyCartAndCalculate() throws CommerceCartModificationException
	{
		final CartModel cart = getCartForCode(CART_CODE);
		final ProductModel product = getProductForCode(PRODUCT_CODE);
		final CommerceCartParameter parameter = createCommerceCartParameter(cart, product);

		agsSingleSubscriptionAddToCartStrategy.addToCart(parameter);

		verifyCart(cart);
	}

	private void verifyCart(final CartModel cart)
	{
		assertTrue(cart.getCalculated());
		assertEquals(SUBTOTAL_WITHOUT_DISCOUNT, cart.getSubTotalWithoutDiscount());
		assertEquals(QUANTITY, cart.getEntries().get(0).getQuantity());
	}

	private CommerceCartParameter createCommerceCartParameter(final CartModel cart, final ProductModel product)
	{
		final CommerceCartParameter parameter = new CommerceCartParameter();
		parameter.setCart(cart);
		parameter.setProduct(product);
		parameter.setQuantity(1);
		parameter.setEnableHooks(true);
		parameter.setCreateNewEntry(true);
		return parameter;
	}

	private ProductModel getProductForCode(final String productCode)
	{
		final ProductModel productExample = new ProductModel();
		productExample.setCode(productCode);
		return flexibleSearchService.getModelByExample(productExample);
	}

	private CartModel getCartForCode(final String code)
	{
		final CartModel cartExample = new CartModel();
		cartExample.setCode(code);
		return flexibleSearchService.getModelByExample(cartExample);
	}
}
