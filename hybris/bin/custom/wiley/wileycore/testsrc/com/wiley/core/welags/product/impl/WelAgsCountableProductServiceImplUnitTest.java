package com.wiley.core.welags.product.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.enums.ProductEditionFormat;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WelAgsCountableProductServiceImplUnitTest
{
	@InjectMocks
	private WelAgsCountableProductServiceImpl welAgsCountableProductServiceImpl;
	@Mock
	private ProductModel productMock;

	@Test
	public void testCanProductHaveQuantitySuccessIfPhysical()
	{
		// Given
		when(productMock.getEditionFormat()).thenReturn(ProductEditionFormat.PHYSICAL);

		// When
		final boolean result = welAgsCountableProductServiceImpl.canProductHaveQuantity(productMock);

		// Then
		assertTrue("Expected that Physical product can have quantity.", result);
	}

	@Test
	public void testCanProductHaveQuantitySuccessIfDigital()
	{
		// Given
		when(productMock.getEditionFormat()).thenReturn(ProductEditionFormat.DIGITAL);

		// When
		final boolean result = welAgsCountableProductServiceImpl.canProductHaveQuantity(productMock);

		// Then
		assertFalse("Expected that Digital product cannot have quantity.", result);
	}

	@Test
	public void testCanProductHaveQuantitySuccessIfDigitalAndPhysical()
	{
		// Given
		when(productMock.getEditionFormat()).thenReturn(ProductEditionFormat.DIGITAL_AND_PHYSICAL);

		// When
		final boolean result = welAgsCountableProductServiceImpl.canProductHaveQuantity(productMock);

		// Then
		assertFalse("Expected that DigitalAndPhysical product cannot have quantity.", result);
	}

	@Test
	public void testCanProductHaveQuantitySuccessIfEditionFormatIsNull()
	{
		// Given
		when(productMock.getEditionFormat()).thenReturn(null);

		// When
		final boolean result = welAgsCountableProductServiceImpl.canProductHaveQuantity(productMock);

		// Then
		assertTrue("Expected that product without edition format have quantity.", result);
	}
}
