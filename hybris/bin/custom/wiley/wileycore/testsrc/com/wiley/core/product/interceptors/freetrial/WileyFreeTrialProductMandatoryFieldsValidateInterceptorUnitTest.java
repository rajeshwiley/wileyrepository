package com.wiley.core.product.interceptors.freetrial;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.i18n.L10NService;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.enums.ProductEditionFormat;
import com.wiley.core.model.WileyFreeTrialProductModel;

import static org.mockito.Mockito.when;


/**
 * Unit test for {@link WileyFreeTrialProductMandatoryFieldsValidateInterceptor}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyFreeTrialProductMandatoryFieldsValidateInterceptorUnitTest
{
	@InjectMocks
	private WileyFreeTrialProductMandatoryFieldsValidateInterceptor interceptorMock;

	@Mock
	private L10NService l10nServiceMock;

	@Mock
	private InterceptorContext interceptorContextMock;

	@Mock
	private CatalogVersionModel catalogVersionMock;

	@Mock
	private WileyFreeTrialProductModel modelMock;

	@Mock
	private ProductModel productModelMock;

	/**
	 * Sets up.
	 */
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);

		when(l10nServiceMock.getLocalizedString("error.wileytrialproduct.Required.fields.missing.message"))
				.thenReturn("Required fields are not filled");

		interceptorMock.setL10nService(l10nServiceMock);
	}

	@Test
	public void testOnValidateSuccess() throws InterceptorException
	{
		when(modelMock.getCode()).thenReturn("testCode");
		when(modelMock.getName()).thenReturn("testName");
		when(modelMock.getIsbn()).thenReturn("testIsbn");
		when(modelMock.getEditionFormat()).thenReturn(ProductEditionFormat.DIGITAL);
		when(modelMock.getCatalogVersion()).thenReturn(catalogVersionMock);

		interceptorMock.onValidate(modelMock, interceptorContextMock);
	}

	@Test(expected = InterceptorException.class)
	public void testFailCatalogVersion() throws InterceptorException
	{
		when(modelMock.getCode()).thenReturn("testCode");
		when(modelMock.getName()).thenReturn("testName");
		when(modelMock.getIsbn()).thenReturn("testIsbn");
		when(modelMock.getEditionFormat()).thenReturn(ProductEditionFormat.DIGITAL);
		when(modelMock.getCatalogVersion()).thenReturn(null);
		interceptorMock.onValidate(modelMock, interceptorContextMock);
	}

	@Test(expected = InterceptorException.class)
	public void testFailEditionFormat() throws InterceptorException
	{
		when(modelMock.getCode()).thenReturn("testCode");
		when(modelMock.getName()).thenReturn("testName");
		when(modelMock.getIsbn()).thenReturn("testIsbn");
		when(modelMock.getEditionFormat()).thenReturn(null);
		when(modelMock.getCatalogVersion()).thenReturn(catalogVersionMock);
		interceptorMock.onValidate(modelMock, interceptorContextMock);
	}

	@Test(expected = InterceptorException.class)
	public void testFailISBN() throws InterceptorException
	{
		when(modelMock.getCode()).thenReturn("testCode");
		when(modelMock.getName()).thenReturn("testName");
		when(modelMock.getIsbn()).thenReturn(null);
		when(modelMock.getEditionFormat()).thenReturn(ProductEditionFormat.DIGITAL);
		when(modelMock.getCatalogVersion()).thenReturn(catalogVersionMock);
		interceptorMock.onValidate(modelMock, interceptorContextMock);
	}

	@Test(expected = InterceptorException.class)
	public void testFailName() throws InterceptorException
	{
		when(modelMock.getCode()).thenReturn("testCode");
		when(modelMock.getName()).thenReturn(null);
		when(modelMock.getIsbn()).thenReturn("testIsbn");
		when(modelMock.getEditionFormat()).thenReturn(ProductEditionFormat.DIGITAL);
		when(modelMock.getCatalogVersion()).thenReturn(catalogVersionMock);
		interceptorMock.onValidate(modelMock, interceptorContextMock);
	}

	@Test(expected = InterceptorException.class)
	public void testFailCode() throws InterceptorException
	{
		when(modelMock.getCode()).thenReturn(null);
		when(modelMock.getName()).thenReturn("testName");
		when(modelMock.getIsbn()).thenReturn("testIsbn");
		when(modelMock.getEditionFormat()).thenReturn(ProductEditionFormat.DIGITAL);
		when(modelMock.getCatalogVersion()).thenReturn(catalogVersionMock);
		interceptorMock.onValidate(modelMock, interceptorContextMock);
	}

}


