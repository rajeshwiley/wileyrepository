package com.wiley.core.wileyb2c.search.solrfacetsearch.provider.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.catalog.references.ProductReferenceService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Created by Raman_Hancharou on 6/13/2017.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cHasRelatedProductsValueProviderTest
{
	@InjectMocks
	private Wileyb2cHasRelatedProductsValueProvider wileyb2cHasRelatedProductsValueProvider;

	@Mock
	private ProductModel productModel;
	@Mock
	private ProductReferenceModel productReferenceModelMock;
	@Mock
	private ProductReferenceService productReferenceService;

	@Test
	public void collectValuesWhenProductAndHasRelatedProductsShouldReturnTrue() throws FieldValueProviderException
	{
		when(productReferenceService
				.getProductReferencesForSourceProduct(productModel, ProductReferenceTypeEnum.RELATED, Boolean.TRUE))
				.thenReturn(Collections.singletonList(productReferenceModelMock));

		final List<Boolean> values = wileyb2cHasRelatedProductsValueProvider.collectValues(null, null, productModel);

		assertEquals(1, values.size());
		assertEquals(Boolean.TRUE, values.get(0));
	}

	@Test
	public void collectValuesWhenProductAndNoRelatedProductsShouldReturnFalse() throws FieldValueProviderException
	{
		when(productReferenceService
				.getProductReferencesForSourceProduct(productModel, ProductReferenceTypeEnum.RELATED, Boolean.TRUE))
				.thenReturn(Collections.EMPTY_LIST);

		final List<Boolean> values = wileyb2cHasRelatedProductsValueProvider.collectValues(null, null, productModel);

		assertEquals(1, values.size());
		assertEquals(Boolean.FALSE, values.get(0));
	}

	@Test
	public void collectValuesWhenNonProductShouldReturnEmptyList() throws FieldValueProviderException
	{
		final List<Boolean> values = wileyb2cHasRelatedProductsValueProvider.collectValues(null, null, 1);

		assertEquals(values.size(), 0);
	}
}
