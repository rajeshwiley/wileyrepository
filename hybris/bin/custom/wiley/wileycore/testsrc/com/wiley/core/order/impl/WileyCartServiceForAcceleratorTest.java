package com.wiley.core.order.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.couponservices.service.data.CouponResponse;
import de.hybris.platform.couponservices.services.CouponService;
import de.hybris.platform.jalo.order.price.JaloPriceFactoryException;
import de.hybris.platform.order.CartFactory;
import de.hybris.platform.order.impl.DefaultCartService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyCartServiceForAcceleratorTest
{
	private static final String VOUCHER_CODE = "voucherCodeOne";
	private static final String FIRST_USER_ID = "First User ID";

	@Mock
	private SessionService mockSessionService;
	@Mock
	private ModelService mockModelService;
	@Mock
	private CouponService mockCouponService;

	@Mock
	private CartModel mockCart;
	@Mock
	private UserModel mockNewUser;
	@Mock
	private UserModel mockCartOwnerUser;
	@Mock
	private CartFactory mockCartFactory;

	@InjectMocks
	private WileyCartServiceForAccelerator underTest;


	@Before
	public void setup()
	{
		when(mockCart.getAppliedCouponCodes())
				.thenReturn(Collections.singletonList(VOUCHER_CODE));
		when(mockSessionService.getOrLoadAttribute(eq(DefaultCartService.SESSION_CART_PARAMETER_NAME),
				any())).thenReturn(mockCart);
		when(mockSessionService.getAttribute(DefaultCartService.SESSION_CART_PARAMETER_NAME)).thenReturn(mockCart);
		when(mockNewUser.getUid()).thenReturn(FIRST_USER_ID);
		when(mockCouponService.redeemCoupon(VOUCHER_CODE, mockCart)).thenReturn(givenCouponResponse(true));
	}

	@Test
	public void testShouldNotChangeUserIfUserIsSame() throws JaloPriceFactoryException
	{
		when(mockCart.getUser()).thenReturn(mockNewUser);

		underTest.changeCurrentCartUser(mockNewUser);

		verify(mockCart, never()).setUser(mockNewUser);
		verify(mockModelService, never()).save(mockCart);
		verify(mockCouponService, never()).releaseCouponCode(VOUCHER_CODE, mockCart);
		verify(mockCouponService, never()).redeemCoupon(VOUCHER_CODE, mockCart);
	}

	@Test
	public void testShouldReapplyVouchersOnCartOwnerChange() throws JaloPriceFactoryException
	{
		when(mockCart.getUser()).thenReturn(mockCartOwnerUser);

		underTest.changeCurrentCartUser(mockNewUser);

		InOrder inOrder = inOrder(mockCart, mockModelService, mockCouponService);
		inOrder.verify(mockCart).setUser(mockNewUser);
		inOrder.verify(mockModelService).save(mockCart);
		inOrder.verify(mockCouponService).releaseCouponCode(VOUCHER_CODE, mockCart);
		inOrder.verify(mockCouponService).redeemCoupon(VOUCHER_CODE, mockCart);
	}

	@Test
	public void testShouldReapplyVouchersIfCartOwnerWasNull() throws JaloPriceFactoryException
	{
		when(mockCart.getUser()).thenReturn(null);

		underTest.changeCurrentCartUser(mockNewUser);

		InOrder inOrder = inOrder(mockCart, mockModelService, mockCouponService);
		inOrder.verify(mockCart).setUser(mockNewUser);
		inOrder.verify(mockModelService).save(mockCart);
		inOrder.verify(mockCouponService).releaseCouponCode(VOUCHER_CODE, mockCart);
		inOrder.verify(mockCouponService).redeemCoupon(VOUCHER_CODE, mockCart);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testShouldThrowExceptionIfUserIsNull()
	{
		underTest.changeCurrentCartUser(null);
	}

	@Test
	public void testNotShouldReapplyVouchersOnCartOwnerChangeWhenNoCartIsInSession() throws JaloPriceFactoryException
	{
		when(mockCart.getUser()).thenReturn(mockCartOwnerUser);
		when(mockSessionService.getAttribute(DefaultCartService.SESSION_CART_PARAMETER_NAME)).thenReturn(null);

		underTest.changeCurrentCartUser(mockNewUser);

		verify(mockCartFactory, never()).createCart();
		verify(mockCouponService, never()).releaseCouponCode(VOUCHER_CODE, mockCart);
		verify(mockCouponService, never()).redeemCoupon(VOUCHER_CODE, mockCart);
	}

	private CouponResponse givenCouponResponse(boolean success)
	{
		final CouponResponse couponResponse = new CouponResponse();
		couponResponse.setSuccess(success);
		return couponResponse;
	}

}
