package com.wiley.core.wileyb2c.search.solrfacetsearch.indexer.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexOperation;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.indexer.exceptions.IndexerException;
import de.hybris.platform.solrfacetsearch.indexer.strategies.IndexerStrategy;
import de.hybris.platform.solrfacetsearch.indexer.strategies.IndexerStrategyFactory;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.wileyb2c.search.solrfacetsearch.indexer.strategies.WileyIndexerStrategy;
import com.wiley.core.wileyb2c.search.solrfacetsearch.provider.impl.Wileyb2cCountryAndCurrencyQualifierProvider;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyIndexerServiceImplUnitTest
{
	public static final List<PK> PKS = Collections.singletonList(PK.BIG_PK);
	@InjectMocks
	private WileyIndexerServiceImpl wileyIndexerService;
	@Mock
	private FacetSearchConfig facetSearchConfig;
	@Mock
	private IndexerStrategyFactory indexerStrategyFactory;
	@Mock
	private IndexedType indexedType;
	@Mock
	private IndexConfig indexConfig;
	@Mock
	private IndexerStrategy updateStrategy;
	@Mock
	private IndexerStrategy deleteStrategy;
	@Mock
	private WileyIndexerStrategy partialUpdateStrategy;
	@Mock
	private IndexedProperty indexedProperty;
	@Mock
	private CountryModel country;
	@Mock
	private SessionService sessionService;

	@Test
	public void updateIndex() throws IndexerException
	{
		when(facetSearchConfig.getIndexConfig()).thenReturn(indexConfig);
		when(indexConfig.getIndexedTypes()).thenReturn(Collections.singletonMap("", indexedType));
		when(indexerStrategyFactory.createIndexerStrategy(facetSearchConfig)).thenReturn(deleteStrategy).thenReturn(
				updateStrategy);

		wileyIndexerService.updateIndex(facetSearchConfig, Collections.emptyMap());

		Mockito.inOrder(deleteStrategy, updateStrategy);
		verify(deleteStrategy).setIndexOperation(IndexOperation.DELETE);
		verify(deleteStrategy).execute();
		verify(updateStrategy).setIndexOperation(IndexOperation.UPDATE);
		verify(updateStrategy).execute();
	}

	@Test
	public void updatePartialTypeIndex() throws IndexerException
	{
		when(indexerStrategyFactory.createIndexerStrategy(facetSearchConfig)).thenReturn(partialUpdateStrategy);
		final List<IndexedProperty> indexedProperties = Collections.singletonList(indexedProperty);
		final Set<CountryModel> countries = Collections.singleton(country);

		wileyIndexerService.updatePartialTypeIndex(facetSearchConfig, indexedType, indexedProperties, PKS, countries);

		verify(partialUpdateStrategy).setIndexOperation(IndexOperation.PARTIAL_UPDATE);
		verify(partialUpdateStrategy).setFacetSearchConfig(facetSearchConfig);
		verify(partialUpdateStrategy).setIndexedType(indexedType);
		verify(partialUpdateStrategy).setIndexedProperties(indexedProperties);
		verify(partialUpdateStrategy).setPks(PKS);
		verify(partialUpdateStrategy).setIndexerHints(Collections.EMPTY_MAP);
		verify(sessionService).setAttribute(Wileyb2cCountryAndCurrencyQualifierProvider.COUNTRIES, countries);
		verify(partialUpdateStrategy).execute();

	}
}
