package com.wiley.core.billingfrequency.dao.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.subscriptionservices.model.BillingFrequencyModel;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.util.TestUtils;


/**
 * Default unit test for {@link WileyBillingFrequencyDaoImpl}.
 */
@IntegrationTest
@RunWith(MockitoJUnitRunner.class)
public class WileyBillingFrequencyDaoImplUnitTest
{

	@Mock
	private FlexibleSearchService flexibleSearchServiceMock;

	@InjectMocks
	private WileyBillingFrequencyDaoImpl wileyBillingFrequencyDao;

	// Test data

	@Mock
	private SearchResult<BillingFrequencyModel> searchResultMock;

	@Test
	public void testFindBillingFrequenciesByCodeSuccessCase()
	{
		// Given
		List<BillingFrequencyModel> billingFrequencyModels = Arrays.asList(mock(BillingFrequencyModel.class),
				mock(BillingFrequencyModel.class));
		when(searchResultMock.getResult()).thenReturn(
				billingFrequencyModels);
		when(flexibleSearchServiceMock.<BillingFrequencyModel> search(anyString(), anyMap())).thenReturn(searchResultMock);

		// When
		List<BillingFrequencyModel> actualBillingFrequencyModels = wileyBillingFrequencyDao.findBillingFrequenciesByCode(
				"SomeCode");

		// Then
		assertNotNull(actualBillingFrequencyModels);
		assertEquals(billingFrequencyModels, actualBillingFrequencyModels);
	}

	@Test
	public void testFindBillingFrequenciesByCodeWithIllegalParameters()
	{
		// When
		TestUtils.expectException(IllegalArgumentException.class,
				() -> wileyBillingFrequencyDao.findBillingFrequenciesByCode(null));

		// Then
		verifyZeroInteractions(flexibleSearchServiceMock);
	}

}