package com.wiley.core.wileyb2b.order.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.externaltax.ExternalTaxesService;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.CalculationService;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.order.OrderService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.services.BaseStoreService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


/**
 * Default unit test for {@link Wileyb2bCommercePlaceOrderStrategyImpl}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2bCommercePlaceOrderStrategyImplUnitTest
{

	@Mock
	private CalculationService calculationServiceMock;

	@Mock
	private OrderService orderServiceMock;

	@Mock
	private ModelService modelServiceMock;

	@Mock
	private ExternalTaxesService externalTaxesServiceMock;

	@Mock
	private BaseSiteService baseSiteServiceMock;

	@Mock
	private BaseStoreService baseStoreServiceMock;

	@Mock
	private CommonI18NService commonI18NServiceMock;

	@InjectMocks
	private Wileyb2bCommercePlaceOrderStrategyImpl wileyb2bCommercePlaceOrderStrategy;

	// Test data
	@Mock
	private CommerceCheckoutParameter commerceCheckoutParameterMock;

	@Mock
	private CartModel cartModelMock;

	@Mock
	private OrderModel orderModelMock;

	@Mock
	private CustomerModel customerModelMock;


	@Before
	public void setUp() throws Exception
	{
		when(commerceCheckoutParameterMock.getCart()).thenReturn(cartModelMock);
		when(cartModelMock.getUser()).thenReturn(customerModelMock);

		when(calculationServiceMock.requiresCalculation(eq(cartModelMock))).thenReturn(false);

		when(orderServiceMock.createOrderFromCart(eq(cartModelMock))).thenReturn(orderModelMock);
	}

	@Test
	public void verifyPlaceOrderIfOrderCalculated() throws InvalidCartException, CalculationException
	{
		// Given
		when(orderModelMock.getCalculated()).thenReturn(true);

		// When
		wileyb2bCommercePlaceOrderStrategy.placeOrder(commerceCheckoutParameterMock);

		// Then
		verify(orderModelMock, never()).setCalculated(anyBoolean());
		checkThatOrderWasNotCalculatedOrRecalculated();
	}

	@Test
	public void verifyPlaceOrderIfOrderNotCalculated() throws InvalidCartException, CalculationException
	{
		// Given
		when(orderModelMock.getCalculated()).thenReturn(false);

		// When
		wileyb2bCommercePlaceOrderStrategy.placeOrder(commerceCheckoutParameterMock);

		// Then
		verify(orderModelMock).setCalculated(eq(true));
		checkThatOrderWasNotCalculatedOrRecalculated();
	}

	private void checkThatOrderWasNotCalculatedOrRecalculated() throws CalculationException
	{
		verify(calculationServiceMock, never()).calculate(eq(orderModelMock));
		verify(calculationServiceMock, never()).recalculate(eq(orderModelMock));
		verify(calculationServiceMock, never()).recalculate(eq(orderModelMock));
		verify(calculationServiceMock, never()).calculateTotals(eq(orderModelMock), anyBoolean());
	}


}