package com.wiley.core.wileycom.search.solrfacetsearch.provider.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.internal.i18n.LocalizationService;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.servicelayer.i18n.WileycomI18NService;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AbstractWileycomValueProviderImplUnitTest
{
	private static final String LANG_CODE = "en";
	private static final String CANADA_CODE = "CA";
	private static final String US_CODE = "US";
	private static final String FIELD1 = "field1";
	private static final String FIELD2 = "field2";
	private static final String FIRST_VALUE = "test1";
	private static final String SECOND_VALUE = "test2";
	@Mock
	private IndexConfig indexConfig;
	@Mock
	private IndexedProperty indexedProperty;
	@Mock
	private ProductModel productModel;
	@Mock
	private FieldNameProvider fieldNameProvider;
	@Mock
	private I18NService i18nService;
	@Mock
	private LocalizationService localeService;
	@Mock
	private CountryModel usCountry;
	@Mock
	private CountryModel canadaCountry;
	@Mock
	private LanguageModel language;
	@Mock
	private WileycomI18NService wileycomI18NService;
	@Mock
	private CountryModel currentCountry;


	@Test
	public void shouldReturnFieldsWithValuesWhenValueExists() throws FieldValueProviderException
	{
		AbstractWileycomValueProvider valueProvider = createValueProvider(Arrays.asList(FIRST_VALUE, SECOND_VALUE));
		valueProvider.setFieldNameProvider(fieldNameProvider);
		when(fieldNameProvider.getFieldNames(indexedProperty, null)).thenReturn(Collections.singletonList(FIELD1));

		Collection<FieldValue> fieldValues = valueProvider.getFieldValues(indexConfig, indexedProperty, productModel);

		assertEquals(fieldValues.size(), 2);
		Iterator<FieldValue> iterator = fieldValues.iterator();
		FieldValue firstValue = iterator.next();
		FieldValue secondValue = iterator.next();
		assertEquals(firstValue.getFieldName(), FIELD1);
		assertEquals(firstValue.getValue(), FIRST_VALUE);
		assertEquals(secondValue.getFieldName(), FIELD1);
		assertEquals(secondValue.getValue(), SECOND_VALUE);
	}

	@Test
	public void shouldGetLocalizedValueWhenIndexPropertyIsLocalizedAndValueExists() throws FieldValueProviderException
	{
		AbstractWileycomValueProvider valueProvider = createValueProvider(Arrays.asList(FIRST_VALUE, SECOND_VALUE));
		valueProvider.setI18nService(i18nService);
		valueProvider.setLocaleService(localeService);
		valueProvider.setFieldNameProvider(fieldNameProvider);
		valueProvider.setWileycomI18NService(wileycomI18NService);
		when(indexedProperty.isLocalized()).thenReturn(true);
		when(i18nService.getCurrentLocale()).thenReturn(Locale.CANADA);
		when(language.getIsocode()).thenReturn(LANG_CODE);
		when(canadaCountry.getIsocode()).thenReturn(CANADA_CODE);
		when(usCountry.getIsocode()).thenReturn(US_CODE);
		when(indexConfig.getLanguages()).thenReturn(Collections.singletonList(language));
		when(indexConfig.getCountries()).thenReturn(Arrays.asList(usCountry, canadaCountry));
		when(fieldNameProvider.getFieldNames(indexedProperty, LANG_CODE + "_" + US_CODE))
				.thenReturn(Collections.singletonList(FIELD1));
		when(fieldNameProvider.getFieldNames(indexedProperty, LANG_CODE + "_" + CANADA_CODE))
				.thenReturn(Collections.singletonList(FIELD2));
		when(wileycomI18NService.getCurrentCountry()).thenReturn(Optional.of(currentCountry));

		Collection<FieldValue> fieldValues = valueProvider.getFieldValues(indexConfig, indexedProperty, productModel);

		assertEquals(fieldValues.size(), 4);
		Iterator<FieldValue> iterator = fieldValues.iterator();
		FieldValue firstValue = iterator.next();
		FieldValue secondValue = iterator.next();
		FieldValue thirdValue = iterator.next();
		FieldValue fourthValue = iterator.next();
		assertEquals(firstValue.getFieldName(), FIELD1);
		assertEquals(firstValue.getValue(), FIRST_VALUE);
		assertEquals(secondValue.getFieldName(), FIELD1);
		assertEquals(secondValue.getValue(), SECOND_VALUE);
		assertEquals(thirdValue.getFieldName(), FIELD2);
		assertEquals(thirdValue.getValue(), FIRST_VALUE);
		assertEquals(fourthValue.getFieldName(), FIELD2);
		assertEquals(fourthValue.getValue(), SECOND_VALUE);
	}

	@Test
	public void shouldReturnEmptyFieldsWhenEmptyValues() throws FieldValueProviderException
	{
		AbstractWileycomValueProvider valueProvider = createValueProvider(Collections.emptyList());

		Collection<FieldValue> fieldValues = valueProvider.getFieldValues(indexConfig, indexedProperty, productModel);

		assertEquals(fieldValues.size(), 0);
	}

	private AbstractWileycomValueProvider createValueProvider(final List<String> values)
	{
		return new AbstractWileycomValueProvider()
		{
			@Override
			protected List<String> collectValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
					final Object model)
			{
				assertEquals(model, productModel);
				return values;
			}
		};
	}


}
