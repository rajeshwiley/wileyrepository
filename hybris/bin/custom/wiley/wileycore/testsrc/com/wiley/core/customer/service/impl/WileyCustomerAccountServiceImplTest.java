package com.wiley.core.customer.service.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.strategies.CustomerNameStrategy;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * JUnit test suite for {@link WileyCustomerAccountServiceImpl}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyCustomerAccountServiceImplTest
{
	private static final String TEST_FIRST_NAME = "First Name";
	private static final String TEST_LAST_NAME = "Last Name";
	private static final String TEST_NAME = "Name";

	@InjectMocks
	private WileyCustomerAccountServiceImpl wileyCustomerAccountService;

	@Mock
	private CustomerNameStrategy customerNameStrategy;

	@Mock
	private CustomerModel customerModelMock;

	@Mock
	private AddressModel addressModelMock;

	@Mock
	private OrderModel orderModelMock;

	@Before
	public void setUp()
	{
		when(addressModelMock.getFirstname())
				.thenReturn(TEST_FIRST_NAME);
		when(addressModelMock.getLastname())
				.thenReturn(TEST_LAST_NAME);
		when(customerModelMock.getDefaultPaymentAddress())
				.thenReturn(addressModelMock);
		when(customerModelMock.getName())
				.thenReturn(TEST_NAME);
		when(orderModelMock.getPaymentAddress())
				.thenReturn(null);
	}


	/**
	 * Test fill missing customer info. First and Last name should be retrieved from corresponding default payment address.
	 * OOTB name field should be unchanged.
	 *
	 * @throws DuplicateUidException
	 * 		the duplicate uid exception
	 */
	@Test
	public void testFillMissingCustomerInfo() throws DuplicateUidException
	{
		wileyCustomerAccountService.fillValuesForCustomerInfo(customerModelMock, orderModelMock);
		verify(customerModelMock)
				.setFirstName(TEST_FIRST_NAME);
		verify(customerModelMock)
				.setLastName(TEST_LAST_NAME);
		verify(customerModelMock)
				.setName(TEST_NAME);

	}

	/**
	 * Test fill missing customer info. First and Last name should be retrieved from corresponding payment address.
	 * OOTB name field should be unchanged.
	 *
	 * @throws DuplicateUidException
	 * 		the duplicate uid exception
	 */
	@Test
	public void testFillMissingCustomerInfoFromOrder() throws DuplicateUidException
	{
		when(customerModelMock.getDefaultPaymentAddress())
				.thenReturn(null);
		when(orderModelMock.getPaymentAddress())
				.thenReturn(addressModelMock);

		wileyCustomerAccountService.fillValuesForCustomerInfo(customerModelMock, orderModelMock);
		verify(customerModelMock)
				.setFirstName(TEST_FIRST_NAME);
		verify(customerModelMock)
				.setLastName(TEST_LAST_NAME);
		verify(customerModelMock)
				.setName(TEST_NAME);
	}
}
