package com.wiley.core.product.interceptors.freetrial;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.enums.ProductEditionFormat;
import com.wiley.core.model.WileyFreeTrialVariantProductModel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;


/**
 * Unit test for {@link WileyFreeTrialVariantProductInitInterceptor}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyFreeTrialVariantProductInitInterceptorUnitTest
{
	@InjectMocks
	private WileyFreeTrialVariantProductInitInterceptor wileyFreeTrialVariantProductInitInterceptor;

	@Mock
	private InterceptorContext interceptorContext;

	@Test
	public void testInit() throws InterceptorException
	{
		WileyFreeTrialVariantProductModel freeTrialVariantProductModel = new WileyFreeTrialVariantProductModel();

		assertNull(freeTrialVariantProductModel.getEditionFormat());

		wileyFreeTrialVariantProductInitInterceptor.onInitDefaults(freeTrialVariantProductModel, interceptorContext);

		assertEquals(ProductEditionFormat.DIGITAL, freeTrialVariantProductModel.getEditionFormat());
	}
}
