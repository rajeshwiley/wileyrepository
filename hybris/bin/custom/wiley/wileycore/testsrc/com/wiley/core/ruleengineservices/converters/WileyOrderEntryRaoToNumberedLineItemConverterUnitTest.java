package com.wiley.core.ruleengineservices.converters;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.order.calculation.money.Currency;
import de.hybris.platform.ruleengineservices.calculation.NumberedLineItem;
import de.hybris.platform.ruleengineservices.rao.AbstractOrderRAO;
import de.hybris.platform.ruleengineservices.rao.OrderEntryRAO;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.ruleengineservices.calculation.WileyNumberedLineItem;

import static org.fest.assertions.Assertions.assertThat;


/**
 * Unit test for {@link WileyOrderEntryRaoToNumberedLineItemConverter}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyOrderEntryRaoToNumberedLineItemConverterUnitTest
{
	private static final String TEST_CURRENCY = "USD";
	private static final int TEST_DIGITS = 2;
	private static final BigDecimal TEST_BASE_PRICE = BigDecimal.valueOf(11);
	private static final BigDecimal TEST_SUBTOTAL_PRICE = BigDecimal.valueOf(22);
	private static final Integer TEST_ENTRY_NUMBER = 100;

	private AbstractOrderRAO abstractOrderRAO = new AbstractOrderRAO();
	private OrderEntryRAO entryRao = new OrderEntryRAO();
	private Currency currency = new Currency(TEST_CURRENCY, TEST_DIGITS);


	@Mock(name = "abstractOrderRaoToCurrencyConverter")
	private Converter<AbstractOrderRAO, Currency> abstractOrderRaoToCurrencyConverterMock;

	@InjectMocks
	private WileyOrderEntryRaoToNumberedLineItemConverter wileyOrderEntryRaoToNumberedLineItemConverter;

	@Before
	public void setup()
	{
		when(abstractOrderRaoToCurrencyConverterMock.convert(abstractOrderRAO)).thenReturn(currency);
		entryRao.setOrder(abstractOrderRAO);

		entryRao.setBasePrice(TEST_BASE_PRICE);
		entryRao.setSubtotalPrice(TEST_SUBTOTAL_PRICE);
	}


	@Test
	public void shouldPopulatePrices() throws Exception
	{
		// When
		final NumberedLineItem lineItem = wileyOrderEntryRaoToNumberedLineItemConverter.convert(entryRao);

		// Then
		assertThat(lineItem).isInstanceOf(WileyNumberedLineItem.class);
		assertThat(lineItem.getBasePrice().getAmount()).isEqualByComparingTo(TEST_BASE_PRICE);
		assertThat(lineItem.getSubTotal().getAmount()).isEqualByComparingTo(TEST_SUBTOTAL_PRICE);
	}

	@Test
	public void shouldPopulateEntryNumber() throws Exception
	{
		// Given
		entryRao.setEntryNumber(TEST_ENTRY_NUMBER);

		// When
		final NumberedLineItem lineItem = wileyOrderEntryRaoToNumberedLineItemConverter.convert(entryRao);

		// Then
		assertThat(lineItem.getEntryNumber()).isEqualTo(TEST_ENTRY_NUMBER);
	}
}
