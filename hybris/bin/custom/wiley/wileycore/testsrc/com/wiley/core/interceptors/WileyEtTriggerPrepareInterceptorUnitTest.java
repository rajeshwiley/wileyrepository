package com.wiley.core.interceptors;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.quartz.CronExpression;

import com.wiley.core.model.EtTriggerModel;


@UnitTest
public class WileyEtTriggerPrepareInterceptorUnitTest
{
	private static final String DATE_PATTERN = "MM/dd/yyyy hh:mm a";
	private static final String ET_ZONE = "America/New_York";
	private static final String CRON_EXPRESSION = "0 30 17 1/1 * ? *";
	private EtTriggerModel etTriggerModel;
	private WileyEtTriggerValidateInterceptor interceptor;
	private InterceptorContext interceptorContext;

	@Before
	public void setUp()
	{
		interceptorContext = Mockito.mock(InterceptorContext.class);
		interceptor = new WileyEtTriggerValidateInterceptor();
		etTriggerModel = new EtTriggerModel();
	}

	@Test
	public void isCorrectOffSetOnActivationTimeChangeTest() throws InterceptorException
	{
		Date testDate = new Date();
		etTriggerModel.setActivationTime(testDate);
		when(interceptorContext.isModified(etTriggerModel, EtTriggerModel.ACTIVATIONTIME)).thenReturn(true);
		when(interceptorContext.isModified(etTriggerModel, EtTriggerModel.CRONEXPRESSION)).thenReturn(false);

		checkOffSet(testDate);
	}

	@Test
	public void isCorrectOffSetOnCronExpressionChangeTest() throws InterceptorException, ParseException
	{
		CronExpression cronExpression = new CronExpression(CRON_EXPRESSION);
		Date testDate = cronExpression.getNextValidTimeAfter(new Date());
		etTriggerModel.setCronExpression(CRON_EXPRESSION);
		when(interceptorContext.isModified(etTriggerModel, EtTriggerModel.CRONEXPRESSION)).thenReturn(true);
		when(interceptorContext.isModified(etTriggerModel, EtTriggerModel.ACTIVATIONTIME)).thenReturn(false);

		checkOffSet(testDate);
	}

	private void checkOffSet(final Date testDate) throws InterceptorException
	{
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_PATTERN);
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(DATE_PATTERN);
		String formattedTestDate = simpleDateFormat.format(testDate);
		LocalDateTime localDateTime = LocalDateTime.parse(formattedTestDate, dateTimeFormatter);
		ZonedDateTime etZonedDateTime = localDateTime.atZone(ZoneId.of(ET_ZONE));
		ZonedDateTime testZonedDateTime = localDateTime.atZone(ZoneId.systemDefault());
		long etLocalTimeZoneDiff = Duration.between(etZonedDateTime, testZonedDateTime).toMillis();

		interceptor.onValidate(etTriggerModel, interceptorContext);

		String formattedResultDate = simpleDateFormat.format(etTriggerModel.getActivationTime());
		assertNotEquals(formattedTestDate, formattedResultDate);

		LocalDateTime activationDateTime = LocalDateTime.parse(simpleDateFormat.format(etTriggerModel.getActivationTime()),
				dateTimeFormatter);
		ZonedDateTime activationZonedDateTime = activationDateTime.atZone(ZoneId.systemDefault());
		long resultTimeZoneDiff = Duration.between(activationZonedDateTime, testZonedDateTime).toMillis();
		assertEquals(etLocalTimeZoneDiff, resultTimeZoneDiff);
	}
}
