package com.wiley.core.jobs.maintenance;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.processengine.enums.ProcessState;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.BusinessProcessUpdateMaintenanceCronJobModel;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyBusinessProcessUpdateJobPerformableUnitTest
{
	private static final String TYPE_SYSTEM = "test_type_system";
	private static final List<ProcessState> STATES =
			Arrays.asList(ProcessState.CREATED, ProcessState.RUNNING, ProcessState.WAITING);
	private static final String SQL_BASE_QUERY = "select {pk} from {BusinessProcess} where {state} in (?states)";
	private static final String SQL_TYPE_SYSTEM_CONDITION = " and {typeSystem} = ?typeSystem";
	private static final String TYPE_SYSTEM_PARAMETER = "typeSystem";
	private static final String STATES_PARAMETER = "states";

	@Mock
	private ModelService modelServiceMock;

	@Mock
	private BusinessProcessModel businessProcessModelMock;

	@InjectMocks
	private WileyBusinessProcessUpdateJobPerformable performable;

	@Test(expected = UnsupportedOperationException.class)
	public void getFetchQueryUnsupported()
	{
		performable.getFetchQuery(new CronJobModel());
	}

	@Test
	public void getFetchQueryFull()
	{
		final BusinessProcessUpdateMaintenanceCronJobModel cronJobModel = new BusinessProcessUpdateMaintenanceCronJobModel();
		cronJobModel.setTypeSystem(TYPE_SYSTEM);
		cronJobModel.setProcessStates(STATES);

		final FlexibleSearchQuery query = performable.getFetchQuery(cronJobModel);

		assertNotNull(query);

		final Map<String, Object> queryParameters = query.getQueryParameters();
		final String strQuery = query.getQuery();

		assertNotNull(queryParameters);
		assertEquals(2, queryParameters.size());
		assertEquals(TYPE_SYSTEM, queryParameters.get(TYPE_SYSTEM_PARAMETER));
		assertEquals(STATES, queryParameters.get(STATES_PARAMETER));
		assertEquals(strQuery, SQL_BASE_QUERY + SQL_TYPE_SYSTEM_CONDITION);
	}

	@Test
	public void getFetchQueryWithStates()
	{
		final BusinessProcessUpdateMaintenanceCronJobModel cronJobModel = new BusinessProcessUpdateMaintenanceCronJobModel();
		cronJobModel.setProcessStates(STATES);

		final FlexibleSearchQuery query = performable.getFetchQuery(cronJobModel);

		assertNotNull(query);

		final Map<String, Object> queryParameters = query.getQueryParameters();
		final String strQuery = query.getQuery();

		assertNotNull(queryParameters);
		assertEquals(1, queryParameters.size());
		assertTrue(queryParameters.containsKey(STATES_PARAMETER));
		final List stateList = (List) queryParameters.get(STATES_PARAMETER);
		assertEquals(3, stateList.size());
		assertEquals(strQuery, SQL_BASE_QUERY);
	}

	@Test
	public void getFetchQueryWithTypeSystem()
	{
		final BusinessProcessUpdateMaintenanceCronJobModel cronJobModel = new BusinessProcessUpdateMaintenanceCronJobModel();
		cronJobModel.setTypeSystem(TYPE_SYSTEM);

		final FlexibleSearchQuery query = performable.getFetchQuery(cronJobModel);

		assertNotNull(query);

		final Map<String, Object> queryParameters = query.getQueryParameters();
		final String strQuery = query.getQuery();

		assertNotNull(queryParameters);
		assertEquals(2, queryParameters.size());
		assertTrue(queryParameters.containsKey(STATES_PARAMETER));
		final List stateList = (List) queryParameters.get(STATES_PARAMETER);
		assertEquals(1, stateList.size());
		assertEquals(ProcessState.RUNNING, stateList.get(0));
		assertEquals(strQuery, SQL_BASE_QUERY + SQL_TYPE_SYSTEM_CONDITION);
	}

	@Test
	public void getFetchQueryEmpty()
	{
		final BusinessProcessUpdateMaintenanceCronJobModel cronJobModel = new BusinessProcessUpdateMaintenanceCronJobModel();

		final FlexibleSearchQuery query = performable.getFetchQuery(cronJobModel);

		assertNotNull(query);

		final Map<String, Object> queryParameters = query.getQueryParameters();
		final String strQuery = query.getQuery();

		assertNotNull(queryParameters);
		assertEquals(1, queryParameters.size());
		assertTrue(queryParameters.containsKey(STATES_PARAMETER));
		final List stateList = (List) queryParameters.get(STATES_PARAMETER);
		assertEquals(1, stateList.size());
		assertEquals(ProcessState.RUNNING, stateList.get(0));
		assertEquals(strQuery, SQL_BASE_QUERY);
	}

	@Test(expected = UnsupportedOperationException.class)
	public void processUnsupported()
	{
		performable.process(null, new CronJobModel());
	}

	@Test
	public void processWithTypeSystem()
	{
		final BusinessProcessUpdateMaintenanceCronJobModel cronJobModel = new BusinessProcessUpdateMaintenanceCronJobModel();
		cronJobModel.setNewTypeSystem(TYPE_SYSTEM);
		final List<ItemModel> elements = new ArrayList<>();
		final ItemModel itemModel = new ItemModel();
		elements.add(itemModel);
		elements.add(businessProcessModelMock);

		performable.process(elements, cronJobModel);

		verify(modelServiceMock, atLeastOnce()).saveAll(elements);
		verify(businessProcessModelMock, atLeastOnce()).setTypeSystem(TYPE_SYSTEM);
	}

	@Test
	public void processWithoutTypeSystem()
	{
		final BusinessProcessUpdateMaintenanceCronJobModel cronJobModel = new BusinessProcessUpdateMaintenanceCronJobModel();
		final List<ItemModel> elements = new ArrayList<>();
		final ItemModel itemModel = new ItemModel();
		elements.add(itemModel);
		elements.add(businessProcessModelMock);

		performable.process(elements, cronJobModel);

		verify(modelServiceMock, atLeastOnce()).saveAll(elements);
		verify(businessProcessModelMock, atLeastOnce()).setTypeSystem(null);
	}
}