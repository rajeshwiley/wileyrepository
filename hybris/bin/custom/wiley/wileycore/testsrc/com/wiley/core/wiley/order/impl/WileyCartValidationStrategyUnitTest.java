package com.wiley.core.wiley.order.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationStatus;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyCartValidationStrategyUnitTest
{
	private static final String PRODUCT_CODE = "code";
	private static final long QUANTITY = 1L;

	@Mock
	private ProductService productServiceMock;

	@Mock
	private ModelService modelServiceMock;

	@Mock
	private CartEntryModel cartEntryModelMock;

	@Mock
	private CartModel cartModelMock;

	@Mock
	private ProductModel productModelMock;

	@InjectMocks
	private WileyCartValidationStrategy strategy;

	@Test
	public void validateCartEntryTest()
	{
		when(cartEntryModelMock.getProduct()).thenReturn(productModelMock);
		when(cartEntryModelMock.getQuantity()).thenReturn(QUANTITY);
		when(productModelMock.getCode()).thenReturn(PRODUCT_CODE);
		when(productServiceMock.getProductForCode(PRODUCT_CODE)).thenReturn(productModelMock);

		final CommerceCartModification modification = strategy.validateCartEntry(cartModelMock, cartEntryModelMock);

		assertEquals(CommerceCartModificationStatus.SUCCESS, modification.getStatusCode());
		assertEquals(QUANTITY, modification.getQuantity());
		assertEquals(QUANTITY, modification.getQuantityAdded());
		assertNotNull(modification.getEntry());
		assertEquals(productModelMock, modification.getEntry().getProduct());
		assertEquals(cartEntryModelMock, modification.getEntry());

		verify(modelServiceMock, never()).remove(cartEntryModelMock);
		verify(modelServiceMock, never()).refresh(cartModelMock);
	}

	@Test
	public void validateCartEntryUnknownIdentifierTest()
	{
		when(cartEntryModelMock.getProduct()).thenReturn(productModelMock);
		when(productModelMock.getCode()).thenReturn(PRODUCT_CODE);
		when(productServiceMock.getProductForCode(PRODUCT_CODE)).thenThrow(UnknownIdentifierException.class);

		final CommerceCartModification modification = strategy.validateCartEntry(cartModelMock, cartEntryModelMock);

		assertEquals(CommerceCartModificationStatus.UNAVAILABLE, modification.getStatusCode());
		assertEquals(0L, modification.getQuantity());
		assertEquals(0L, modification.getQuantityAdded());
		assertNotNull(modification.getEntry());
		assertEquals(productModelMock, modification.getEntry().getProduct());
		assertNotEquals(cartEntryModelMock, modification.getEntry());

		verify(modelServiceMock, atLeastOnce()).remove(cartEntryModelMock);
		verify(modelServiceMock, atLeastOnce()).refresh(cartModelMock);
	}
}