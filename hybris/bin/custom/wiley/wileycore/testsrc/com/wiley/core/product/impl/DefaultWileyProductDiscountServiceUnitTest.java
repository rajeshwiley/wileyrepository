package com.wiley.core.product.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.europe1.enums.UserDiscountGroup;
import de.hybris.platform.europe1.model.AbstractDiscountRowModel;
import de.hybris.platform.europe1.model.DiscountRowModel;
import de.hybris.platform.jalo.order.price.JaloPriceFactoryException;
import de.hybris.platform.servicelayer.exceptions.SystemException;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.DiscountValue;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.order.WileyPriceFactory;
import com.wiley.core.order.data.ProductDiscountParameter;

import static java.util.Objects.nonNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


/**
 * Default unit test for {@link DefaultWileyProductDiscountService}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultWileyProductDiscountServiceUnitTest
{

	@Mock
	private WileyPriceFactory wileyPriceFactoryMock;

	@Mock
	private CommerceCommonI18NService commerceCommonI18NServiceMock;

	@Mock
	private UserService userServiceMock;

	@InjectMocks
	private DefaultWileyProductDiscountService defaultWileyProductDiscountService;

	// Test data
	@Mock
	private CurrencyModel currencyModelMock;

	@Mock
	private UserModel userModelMock;

	@Mock
	private ProductModel productModelMock;

	@Mock
	private DiscountValue studentDiscountValueMock;

	@Mock
	private DiscountValue commonDiscountValueMock;

	@Before
	public void setUp() throws Exception
	{
		when(commerceCommonI18NServiceMock.getCurrentCurrency()).thenReturn(currencyModelMock);

		when(userServiceMock.getCurrentUser()).thenReturn(userModelMock);

		final DiscountRowModel studentDiscountMock = mock(DiscountRowModel.class);
		when(studentDiscountMock.getUg()).thenReturn(UserDiscountGroup.STUDENT);

		// we need create dynamic answer to make sure that service can filter not student discounts
		when(wileyPriceFactoryMock
				.getProductDiscountValues(argThat(new ArgumentMatcher<ProductDiscountParameter>()
				{
					@Override
					public boolean matches(final Object argument)
					{
						ProductDiscountParameter parameter = (ProductDiscountParameter) argument;
						return productModelMock.equals(parameter.getProduct())
								&& UserDiscountGroup.STUDENT.equals(parameter.getUserDiscountGroup())
								&& userModelMock.equals(parameter.getUser())
								&& currencyModelMock.equals(parameter.getCurrency())
								&& nonNull(parameter.getDate())
								&& nonNull(parameter.getAdditionalFilter());
					}
				}))).thenAnswer(invocation -> {

			final ProductDiscountParameter parameter = (ProductDiscountParameter) invocation.getArguments()[0];
			final Optional<Predicate<AbstractDiscountRowModel>> additionalFilter = parameter.getAdditionalFilter();

			if (additionalFilter.isPresent() && additionalFilter.get().test(studentDiscountMock))
			{
				return Arrays.asList(studentDiscountValueMock);
			}

			return Arrays.asList(commonDiscountValueMock, studentDiscountValueMock);
		});
	}

	@Test
	public void shouldReturnUserDiscounts()
	{
		// Given
		// no changes in test data

		// When
		final List<DiscountValue> studentDiscounts =
				defaultWileyProductDiscountService.getPotentialDiscountForCurrentCustomer(productModelMock, 
						UserDiscountGroup.STUDENT);

		//Then
		assertEquals(1, studentDiscounts.size());
		assertSame(studentDiscountValueMock, studentDiscounts.get(0));
	}

	@Test
	public void shouldThrowSystemExceptionsIfJaloPriceExceptionOccurred() throws JaloPriceFactoryException
	{
		// Given
		reset(wileyPriceFactoryMock);

		when(wileyPriceFactoryMock.getProductDiscountValues(any(ProductDiscountParameter.class))).thenThrow(
				JaloPriceFactoryException.class);

		try
		{
			// When
			defaultWileyProductDiscountService.getPotentialDiscountForCurrentCustomer(productModelMock, 
					UserDiscountGroup.STUDENT);
			fail("Expected SystemException.");
		}
		catch (SystemException e)
		{
			// success
		}

		verify(wileyPriceFactoryMock).getProductDiscountValues(any(ProductDiscountParameter.class));
	}
}