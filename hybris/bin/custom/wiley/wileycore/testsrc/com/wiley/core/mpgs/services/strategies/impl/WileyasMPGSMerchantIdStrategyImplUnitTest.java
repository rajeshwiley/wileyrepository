package com.wiley.core.mpgs.services.strategies.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import static org.junit.Assert.assertEquals;

import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyTnsMerchantModel;
import com.wiley.core.payment.tnsmerchant.service.WileyTnsMerchantService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyasMPGSMerchantIdStrategyImplUnitTest
{
	private static final String TEST_MERCHANT_ID = "TEST_MERCHANT_ID";
	private static final String TEST_ISO_CODE = "US";
	private static final String TEST_CURRENCY_ISO_CODE = "USA";

	@Mock
	private WileyTnsMerchantService wileyTnsMerchantService;

	@Mock
	private CountryModel countryModelMock;

	@Mock
	private WileyTnsMerchantModel wileyTnsMerchantMock;

	@InjectMocks
	private WileyasMPGSMerchantIdStrategyImpl testInstance;

	private AbstractOrderModel order;

	@Before
	public void setUp()
	{
		when(countryModelMock.getIsocode()).thenReturn(TEST_ISO_CODE);
		when(wileyTnsMerchantService.getTnsMerchant(TEST_ISO_CODE, TEST_CURRENCY_ISO_CODE)).thenReturn(wileyTnsMerchantMock);
		prepareOrder();
	}

	@Test
	public void shouldReturnMertchantIdFromCountry()
	{
		//Given
		when(wileyTnsMerchantMock.getId()).thenReturn(TEST_MERCHANT_ID);
		//When
		String result = testInstance.getMerchantID(order);
		//Then
		assertEquals(TEST_MERCHANT_ID, result);
	}

	@Test(expected = IllegalStateException.class)
	public void shouldThrowAnExceptionIfMerchantIdIsEmptyForCountry()
	{
		//Given
		when(wileyTnsMerchantMock.getId()).thenReturn(null);
		//When
		testInstance.getMerchantID(order);
	}

	private void prepareOrder()
	{
		order = new OrderModel();
		AddressModel paymentAddress = new AddressModel();
		CurrencyModel currency = new CurrencyModel();
		currency.setIsocode(TEST_CURRENCY_ISO_CODE);
		order.setPaymentAddress(paymentAddress);
		order.setCurrency(currency);
		paymentAddress.setCountry(countryModelMock);
	}
}