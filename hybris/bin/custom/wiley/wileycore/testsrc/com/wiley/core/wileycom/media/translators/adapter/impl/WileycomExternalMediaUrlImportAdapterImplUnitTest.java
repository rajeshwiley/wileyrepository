package com.wiley.core.wileycom.media.translators.adapter.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.impex.jalo.header.UnresolvedValueException;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.exceptions.ExternalSystemNotFoundException;
import com.wiley.core.wileycom.media.WileycomGetMediaGateway;
import com.wiley.core.wileycom.media.WileycomVerifyMediaGateway;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileycomExternalMediaUrlImportAdapterImplUnitTest
{
	private static final String ISBN = "isbn";
	private static final String CORRECT_URL = "correctUrl";
	private static final String BROKEN_URL = "brokenUrl";
	@Mock
	private ModelService modelService;
	@Mock
	private WileycomVerifyMediaGateway wileycomVerifyMediaGateway;
	@Mock
	private WileycomGetMediaGateway wileycomGetMediaGateway;
	@Mock
	private Item item;
	@InjectMocks
	private WileycomExternalMediaUrlImportAdapterImpl wileycomExternalMediaUrlImportAdapter;

	@Test
	public void performImportWhenUrlIsOkShouldSetItToMedia() throws UnresolvedValueException
	{
		MediaModel mediaModel = new MediaModel();
		when(modelService.get(item)).thenReturn(mediaModel);
		when(wileycomGetMediaGateway.getURL(ISBN)).thenReturn(CORRECT_URL);

		wileycomExternalMediaUrlImportAdapter.performImport(ISBN, item);

		Assert.assertEquals(CORRECT_URL, mediaModel.getInternalURL());
	}

	@Test(expected = UnresolvedValueException.class)
	public void performImportWhenUrlIsBrokenShouldThrowUnresolvedValueException() throws UnresolvedValueException
	{
		MediaModel mediaModel = new MediaModel();
		when(modelService.get(item)).thenReturn(mediaModel);
		when(wileycomGetMediaGateway.getURL(ISBN)).thenReturn(BROKEN_URL);
		doThrow(new ExternalSystemNotFoundException("Not found")).when(wileycomVerifyMediaGateway).verifyURL(BROKEN_URL);

		wileycomExternalMediaUrlImportAdapter.performImport(ISBN, item);
	}
}
