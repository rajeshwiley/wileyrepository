package com.wiley.core.util.url.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantProductModel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.category.WileyCategoryService;
import com.wiley.core.model.WileyGiftCardProductModel;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WelProductUrlVisibilityStrategyImplUnitTest
{
	@InjectMocks
	private WelProductUrlVisibilityStrategyImpl testInstance;

	@Mock
	private WileyCategoryService mockWileyCategoryService;

	@Mock
	private ProductModel mockBaseProduct;
	@Mock
	private VariantProductModel mockVariantProduct;
	@Mock
	private VariantProductModel mockVariantPartnersProduct;
	@Mock
	private WileyGiftCardProductModel mockGiftCardProduct;

	@Mock
	private CategoryModel mockCfaCategory;
	@Mock
	private CategoryModel mockNonCfaCategory;
	@Mock
	private CategoryModel mockPartnersCategory;

	@Before
	public void setUp()
	{
		when(mockWileyCategoryService.isCFACategory(mockCfaCategory)).thenReturn(true);
		when(mockWileyCategoryService.isCFACategory(mockNonCfaCategory)).thenReturn(false);
		when(mockWileyCategoryService.isPartnersCategory(mockPartnersCategory)).thenReturn(true);
	}

	@Test
	public void shouldReturnFalseIfProductIsGiftCard()
	{
		assertFalse(testInstance.isUrlForItemVisible(mockGiftCardProduct));
	}

	@Test
	public void shouldReturnFalseIfProductIsPartners()
	{
		when(mockWileyCategoryService.getPrimaryWileyCategoryForProduct(mockBaseProduct)).thenReturn(mockPartnersCategory);
		assertFalse(testInstance.isUrlForItemVisible(mockBaseProduct));
	}

	@Test
	public void shouldReturnTrueIfProductIsVariantAndCfaLike()
	{
		when(mockWileyCategoryService.getPrimaryWileyCategoryForProduct(mockVariantProduct)).thenReturn(mockCfaCategory);
		assertTrue(testInstance.isUrlForItemVisible(mockVariantProduct));
	}

	@Test
	public void shouldReturnFalseIfProductIsVariantAndNonCfaLike()
	{
		when(mockWileyCategoryService.getPrimaryWileyCategoryForProduct(mockVariantProduct)).thenReturn(mockNonCfaCategory);
		assertFalse(testInstance.isUrlForItemVisible(mockVariantProduct));
	}

	@Test
	public void shouldReturnTrueIfProductIsBaseProductAndNonCfaLike()
	{
		when(mockWileyCategoryService.getPrimaryWileyCategoryForProduct(mockBaseProduct)).thenReturn(mockNonCfaCategory);
		assertTrue(testInstance.isUrlForItemVisible(mockBaseProduct));
	}

	@Test
	public void shouldReturnFalseIfProductIsBaseProductAndCfaLike()
	{
		when(mockWileyCategoryService.getPrimaryWileyCategoryForProduct(mockBaseProduct)).thenReturn(mockCfaCategory);
		assertFalse(testInstance.isUrlForItemVisible(mockBaseProduct));
	}

	@Test
	public void shouldReturnFalseIfProductIsVariantPartnersAndNonCfaLike()
	{
		when(mockWileyCategoryService.getPrimaryWileyCategoryForProduct(mockVariantPartnersProduct)).thenReturn(
				mockPartnersCategory);
		assertFalse(testInstance.isUrlForItemVisible(mockVariantPartnersProduct));
	}
}
