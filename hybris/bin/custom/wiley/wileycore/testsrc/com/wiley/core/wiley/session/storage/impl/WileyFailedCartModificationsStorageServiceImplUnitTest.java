package com.wiley.core.wiley.session.storage.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.session.Session;
import de.hybris.platform.servicelayer.session.SessionService;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.ListUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyFailedCartModificationsStorageServiceImplUnitTest
{
	@InjectMocks
	private WileyFailedCartModificationsStorageServiceImpl wileyFailedCartModificationsStorageService;
	@Mock
	private CommerceCartModification commerceCartModification;
	private List<CommerceCartModification> commerceCartModificationList;
	@Mock
	private CommerceCartModification previousCommerceCartModification;
	private List<CommerceCartModification> previousCommerceCartModificationList;
	@Mock
	private SessionService sessionService;
	@Mock
	private Session session;
	@Mock
	private CartModificationData cartModificationDataMock;
	private List<CartModificationData> cartModificationDataMockList;
	@Mock
	private CartModificationData previousCartModificationDataMock;
	private List<CartModificationData> previousCartModificationDataMockList;
	@Mock
	private Converter<CommerceCartModification, CartModificationData> cartModificationConverterMock;
	@Mock
	private Converter<CartModificationData, CommerceCartModification> wileyCartModificationReverseConverter;

	@Before
	public void setUp() throws Exception
	{
		when(sessionService.getCurrentSession()).thenReturn(session);

		commerceCartModificationList = Arrays.asList(commerceCartModification);
		previousCommerceCartModificationList = Arrays.asList(previousCommerceCartModification);
		cartModificationDataMockList = Arrays.asList(cartModificationDataMock);
		previousCartModificationDataMockList = Arrays.asList(previousCartModificationDataMock);

		when(cartModificationConverterMock.convertAll(commerceCartModificationList)).thenReturn(cartModificationDataMockList);
		when(wileyCartModificationReverseConverter.convertAll(cartModificationDataMockList)).thenReturn(
				commerceCartModificationList);

		when(cartModificationConverterMock.convertAll(previousCommerceCartModificationList)).thenReturn(
				previousCartModificationDataMockList);
		when(wileyCartModificationReverseConverter.convertAll(previousCartModificationDataMockList)).thenReturn(
				previousCommerceCartModificationList);
	}

	@Test
	public void pushAllWhenPushModificationsThenSetItToSession()
	{
		wileyFailedCartModificationsStorageService.pushAll(commerceCartModificationList);

		verify(session).setAttribute(WileyFailedCartModificationsStorageServiceImpl.CART_MODIFICATION_REMOVED_ENTRIES_KEY,
				cartModificationDataMockList);
	}

	@Test
	public void pushAllWhenPreviousModificationsExistsThenSetItToSessionToo()
	{
		when(sessionService.getAttribute(WileyFailedCartModificationsStorageServiceImpl.CART_MODIFICATION_REMOVED_ENTRIES_KEY))
				.thenReturn(previousCartModificationDataMockList);

		wileyFailedCartModificationsStorageService.pushAll(commerceCartModificationList);

		verify(session).setAttribute(WileyFailedCartModificationsStorageServiceImpl.CART_MODIFICATION_REMOVED_ENTRIES_KEY,
				ListUtils.union(cartModificationDataMockList, previousCartModificationDataMockList));
	}

	@Test
	public void popAllWhenFailedModificationsExistsThenRemoveItFromSessionAndReturn()
	{
		when(sessionService.getAttribute(WileyFailedCartModificationsStorageServiceImpl.CART_MODIFICATION_REMOVED_ENTRIES_KEY))
				.thenReturn(cartModificationDataMockList);

		final List<CommerceCartModification> modifications = wileyFailedCartModificationsStorageService.popAll();

		Assert.assertEquals(commerceCartModificationList, modifications);
		verify(sessionService).removeAttribute(
				WileyFailedCartModificationsStorageServiceImpl.CART_MODIFICATION_REMOVED_ENTRIES_KEY);
	}

	@Test
	public void popAllWhenFailedModificationsDoesNotExistThenReturnEmptyCollection()
	{
		final List<CommerceCartModification> modifications = wileyFailedCartModificationsStorageService.popAll();

		Assert.assertTrue(modifications.isEmpty());
	}
}
