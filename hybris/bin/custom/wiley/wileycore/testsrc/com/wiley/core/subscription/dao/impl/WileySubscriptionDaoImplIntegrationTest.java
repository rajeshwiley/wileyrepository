package com.wiley.core.subscription.dao.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Before;
import org.junit.Test;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.model.WileySubscriptionModel;


/**
 * Created by Mikhail_Asadchy on 9/23/2016.
 */
@IntegrationTest
public class WileySubscriptionDaoImplIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	@Resource(name = "subscriptionDao")
	private WileySubscriptionDaoImpl wileySubscriptionDao;

	@Resource
	private UserService userService;
	@Resource
	private CatalogVersionService catalogVersionService;

	private static final String RESOURCE_PREFIX = "/wileycore/test/subscription/WileySubscriptionDaoImplIntegrationTest/";

	final String userName = "customer@test.com";

	private static final int PAGE_SIZE = 2;
	private long subscriptionsCount;
	private long subscriptionsPagesCount;
	private Collection<CatalogVersionModel> sessionCatalogVersions;

	@Before
	public void setUp() throws Exception
	{
		sessionCatalogVersions = initAndGetCatalogVersions("wileyProductCatalog", "Online");
		subscriptionsCount = 7L;
		subscriptionsPagesCount = 4;
	}

	@Test
	public void successCase() throws Exception
	{
		// given
		importCsv(RESOURCE_PREFIX + "wileySubscriptions.impex", DEFAULT_ENCODING);

		// preparations

		// when
		doTestCase(userName, setUpPageableData(PAGE_SIZE, 0), Arrays.asList("testCode101", "testCode301"),
				new ArrayList<>(), 2);
		doTestCase(userName, setUpPageableData(PAGE_SIZE, 1), Arrays.asList("testCode201", "testCode401"),
				new ArrayList<>(), 2);
		doTestCase(userName, setUpPageableData(PAGE_SIZE, 2), Arrays.asList("testCode502", "testCode302"),
				new ArrayList<>(), 2);
		doTestCase(userName, setUpPageableData(PAGE_SIZE, 3), Arrays.asList("testCode102"),
				new ArrayList<>(), 1);
	}

	private void doTestCase(final String userName, final PageableData pageableData, final List<String> subscriptionCodes,
			final List<String> unexpectedSubscriptionCodes, final int expectedSubscriptionsCount)
	{
		final UserModel userModel = userService.getUserForUID(userName);
		final SearchPageData<WileySubscriptionModel> subscriptions =
				wileySubscriptionDao.getSubscriptions(userModel, sessionCatalogVersions, pageableData);
		final List<WileySubscriptionModel> results = subscriptions.getResults();

		// then
		// verify
		checkResults(results, subscriptionCodes, unexpectedSubscriptionCodes);
		assertEquals(expectedSubscriptionsCount, results.size());
		assertEquals(subscriptionsCount, subscriptions.getPagination().getTotalNumberOfResults());
		assertEquals(subscriptionsPagesCount, subscriptions.getPagination().getNumberOfPages());
	}

	@Test
	public void testSubscriptionsFromOtherCatalogsShouldNotBeFetched() throws Exception
	{
		// given
		importCsv(RESOURCE_PREFIX + "wileyAgsSubscription.impex", DEFAULT_ENCODING);
		final UserModel userModel = userService.getUserForUID(userName);

		// when
		final SearchPageData<WileySubscriptionModel> subscriptions =
				wileySubscriptionDao.getSubscriptions(userModel, sessionCatalogVersions, setUpPageableData(10, 0));

		// then
		final List<WileySubscriptionModel> results = subscriptions.getResults();
		assertTrue(CollectionUtils.isEmpty(results));
	}

	@Test
	public void testAgsOnlyCatalogVersionsAreFetched() throws Exception
	{
		// given
		importCsv(RESOURCE_PREFIX + "wileyAgsSubscription.impex", DEFAULT_ENCODING);
		sessionCatalogVersions = initAndGetCatalogVersions("agsProductCatalog", "Online");
		final UserModel userModel = userService.getUserForUID(userName);

		// when
		final SearchPageData<WileySubscriptionModel> subscriptions =
				wileySubscriptionDao.getSubscriptions(userModel, sessionCatalogVersions, setUpPageableData(10, 0));

		// then
		final List<WileySubscriptionModel> results = subscriptions.getResults();
		assertTrue(CollectionUtils.isNotEmpty(results));
		assertEquals(1, results.size());
		assertEquals("agsTestSubscriptionCode1", results.get(0).getCode());
	}

	private Collection<CatalogVersionModel> initAndGetCatalogVersions(final String catalog, final String versionName)
	{
		catalogVersionService.setSessionCatalogVersion(catalog, versionName);
		return catalogVersionService.getSessionCatalogVersions();
	}

	private void checkResults(final List<WileySubscriptionModel> results, final List<String> expectedSubscriptionCodes,
			final List<String> unexpectedSubscriptionCodes)
	{
		for (final WileySubscriptionModel result : results)
		{
			boolean found = false;
			for (final String subscriptionCode : expectedSubscriptionCodes)
			{
				if (subscriptionCode.equals(result.getCode()))
				{
					found = true;
					break;
				}
			}
			assertTrue(found);
		}

		for (final WileySubscriptionModel result : results)
		{
			boolean found = false;
			for (final String subscriptionCode : unexpectedSubscriptionCodes)
			{
				if (subscriptionCode.equals(result.getCode()))
				{
					found = true;
					break;
				}
			}
			assertFalse(found);
		}
	}

	private PageableData setUpPageableData(final int pageSize, final int currentPage)
	{
		final PageableData pageableData = new PageableData();
		pageableData.setPageSize(pageSize);
		pageableData.setCurrentPage(currentPage);
		return pageableData;
	}
}
