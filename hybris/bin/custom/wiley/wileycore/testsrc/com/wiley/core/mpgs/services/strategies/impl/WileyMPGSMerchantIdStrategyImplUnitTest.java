package com.wiley.core.mpgs.services.strategies.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

import com.wiley.core.model.WileyTnsMerchantModel;


@UnitTest
public class WileyMPGSMerchantIdStrategyImplUnitTest
{
	private static final String DEFAULT_MERCHANT_ID = "DEFAULT_MERCHANT_Id";

	private WileyMPGSMerchantIdStrategyImpl testInstance;

	@Before
	public void setUp() throws Exception
	{
		testInstance = new WileyMPGSMerchantIdStrategyImpl();
		testInstance.setDefaultMerchantId(DEFAULT_MERCHANT_ID);
	}

	@Test
	public void shouldReturnDefaultTNSValueFromTheConfiguration()
	{
		String tnsMId = testInstance.getMerchantID(new AbstractOrderModel());
		assertEquals(DEFAULT_MERCHANT_ID, tnsMId);
	}

	@Test
	public void getMerchant()
	{
		WileyTnsMerchantModel merchant = testInstance.getMerchant(new AbstractOrderModel());
		assertNotNull(merchant);
		assertEquals(DEFAULT_MERCHANT_ID, merchant.getId());
	}
}