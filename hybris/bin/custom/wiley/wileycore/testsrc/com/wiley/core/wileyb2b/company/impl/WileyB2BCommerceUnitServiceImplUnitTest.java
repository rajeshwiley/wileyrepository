package com.wiley.core.wileyb2b.company.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BUnitService;
import de.hybris.platform.b2bacceleratorservices.company.CompanyB2BCommerceService;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;

import com.wiley.core.b2b.unit.service.WileyB2BUnitExternalAddressService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyB2BCommerceUnitServiceImplUnitTest
{
	private static final String UNIT_ID = "TEST_UNIT_ID";
	private static final String ADDRESS_ID = PK.BIG_PK.getLongValueAsString();
	private static final String ADDRESS_EXT_ID = "TEST_ADDRESS_EXT_ID";

	@Mock
	private CompanyB2BCommerceService companyB2BCommerceService;
	@Mock
	private WileyB2BUnitExternalAddressService wileyB2BUnitExternalAddressService;
	@InjectMocks
	private WileyB2BCommerceUnitServiceImpl testInstance = new WileyB2BCommerceUnitServiceImpl();

	@Test
	public void removeAddressEntryShouldUseExternalAddressIdWhenCallExternalServiceToRemoveAddress() throws Exception
	{
		//Given
		B2BUnitModel unit = createUnit(UNIT_ID);
		addAddressToUnit(unit, ADDRESS_EXT_ID);
		mockSuperClassBehaviorForRemoveAddressEntry(unit);

		when(companyB2BCommerceService.getUnitForUid(UNIT_ID)).thenReturn(unit);
		//Test
		testInstance.removeAddressEntry(UNIT_ID, ADDRESS_ID);
		//Verify
		verify(wileyB2BUnitExternalAddressService).deleteShippingAddress(eq(unit), eq(ADDRESS_EXT_ID));
	}

	private B2BUnitModel createUnit(final String unitId) {
		B2BUnitModel unit = mock(B2BUnitModel.class);
		when(unit.getId()).thenReturn(unitId);
		return unit;
	}

	private void addAddressToUnit(final B2BUnitModel unit, final String addressExtId) {
		AddressModel addressModel = mock(AddressModel.class);
		PK addressPk = PK.BIG_PK;
		when(addressModel.getPk()).thenReturn(addressPk);

		when(addressModel.getExternalId()).thenReturn(addressExtId);
		when(unit.getAddresses()).thenReturn(Collections.singletonList(addressModel));
	}

	//Super class mocks
	@Mock
	private B2BUnitService<B2BUnitModel, B2BCustomerModel> b2BUnitService;
	@Mock
	private ModelService modelService;

	private void mockSuperClassBehaviorForRemoveAddressEntry(final B2BUnitModel unit) {
		when(b2BUnitService.getUnitForUid(UNIT_ID)).thenReturn(unit);
	}
}