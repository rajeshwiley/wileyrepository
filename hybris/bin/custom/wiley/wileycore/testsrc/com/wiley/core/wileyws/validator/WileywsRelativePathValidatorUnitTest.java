package com.wiley.core.wileyws.validator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.validation.Errors;


@Ignore
public abstract class WileywsRelativePathValidatorUnitTest
{
	private static final String TEST_PATH = "testPath";

	@Mock
	protected Object targetMock;

	@Mock
	protected Errors errorsMock;

	@Mock
	protected Object pathObjectMock;

	@Test
	public void testValidateNullObject()
	{
		// given

		try
		{
			// when
			getTestInstance().validate(targetMock, null);
			fail("Should throw IllegalArgumentException");
		}
		catch (final IllegalArgumentException e)
		{
			// then
			assertEquals("Errors object must not be null", e.getMessage());
		}
	}

	@Test
	public void testValidateShouldValidatePathObjectIfItExists()
	{
		// given
		doReturn(true).when(getTestInstance()).pathObjectExists(errorsMock);

		// when
		getTestInstance().validate(targetMock, errorsMock);

		// then
		verify(getTestInstance()).validatePathObject(targetMock, errorsMock);
	}

	@Test
	public void testValidateShouldNotValidatePathObjectIfItDoesNotExist()
	{
		// given
		doReturn(false).when(getTestInstance()).pathObjectExists(errorsMock);

		// when
		getTestInstance().validate(targetMock, errorsMock);

		// then
		verify(getTestInstance(), never()).validatePathObject(targetMock, errorsMock);
	}

	@Test
	public void testPathObjectExists()
	{
		// given
		doReturn(TEST_PATH).when(getTestInstance()).getPath();
		when(errorsMock.getFieldValue(TEST_PATH)).thenReturn(pathObjectMock);

		// when
		boolean result = getTestInstance().pathObjectExists(errorsMock);

		// then
		assertTrue(result);
	}

	@Test
	public void testPathObjectDoesNotExistIfPathIsNull()
	{
		// given
		doReturn(null).when(getTestInstance()).getPath();

		// when
		boolean result = getTestInstance().pathObjectExists(errorsMock);

		// then
		assertFalse(result);
		verify(errorsMock, never()).getFieldValue(any());
	}

	@Test
	public void testPathObjectDoesNotExistIfPathObjectIsNull()
	{
		// given
		doReturn(TEST_PATH).when(getTestInstance()).getPath();
		when(errorsMock.getFieldValue(TEST_PATH)).thenReturn(null);

		// when
		boolean result = getTestInstance().pathObjectExists(errorsMock);

		// then
		assertFalse(result);
	}

	abstract WileywsRelativePathValidator getTestInstance();

}