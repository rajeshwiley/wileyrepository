package com.wiley.core.wileycom.order.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.jalo.order.AbstractOrder;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.util.PriceValue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.jalo.ExternalDeliveryMode;
import com.wiley.core.wileycom.order.WileycomDeliveryService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileycomFindDeliveryCostStrategyTest
{
	private static final String USD_ISOCODE = "USD";
	private static final String POUNDS_ISOCODE = "GBP";
	private static final double PRECISION = 0.01;
	private static final double DELIVERY_COST_VALUE = 15.0;

	@InjectMocks
	private WileycomFindDeliveryCostStrategy testedInstance = new WileycomFindDeliveryCostStrategy();

	@Mock
	private ModelService modelServiceMock;
	@Mock
	private WileycomCommerceDeliveryModeValidationStrategy wileycomCommerceDeliveryModeValidationStrategy;
	@Mock
	private WileycomDeliveryService wileycomDeliveryService;
	@Mock
	private AbstractOrderModel orderMock;
	@Mock
	private AbstractOrder orderJaloMock;
	@Mock
	private ExternalDeliveryMode externalDeliveryModeMock;
	@Mock
	private PriceValue deliveryPriceMock;
	@Mock
	private CurrencyModel usdCurrencyMock;

	@Before
	public void setUp()
	{
		when(orderMock.getCurrency()).thenReturn(usdCurrencyMock);
		when(usdCurrencyMock.getIsocode()).thenReturn(USD_ISOCODE);
		when(orderMock.getNet()).thenReturn(Boolean.TRUE);

		when(modelServiceMock.getSource(orderMock)).thenReturn(orderJaloMock);
		when(orderJaloMock.getDeliveryMode()).thenReturn(externalDeliveryModeMock);
		when(externalDeliveryModeMock.getCost(orderJaloMock)).thenReturn(deliveryPriceMock);
		when(deliveryPriceMock.getValue()).thenReturn(DELIVERY_COST_VALUE);
		when(deliveryPriceMock.getCurrencyIso()).thenReturn(USD_ISOCODE);
	}

	@Test
	public void shouldReturnDeliveryCost()
	{
		//When
		final PriceValue deliveryCost = testedInstance.getDeliveryCost(orderMock);
		//Then
		assertEquals(deliveryPriceMock, deliveryCost);
	}

	@Test
	public void shouldReturnZeroPriceValueWhenDeliveryModeNotFound()
	{
		//Given
		when(orderJaloMock.getDeliveryMode()).thenReturn(null);
		//When
		final PriceValue deliveryCost = testedInstance.getDeliveryCost(orderMock);
		//Then
		verifyZeroPriceDeliveryCost(deliveryCost);
	}

	@Test
	public void shouldReturnZeroPriceValueWhenCurrencyDoesNotMatch()
	{
		//Given
		when(orderMock.getCurrency()).thenReturn(usdCurrencyMock);
		when(deliveryPriceMock.getCurrencyIso()).thenReturn(POUNDS_ISOCODE);
		//When
		final PriceValue deliveryCost = testedInstance.getDeliveryCost(orderMock);
		//Then
		verifyZeroPriceDeliveryCost(deliveryCost);
	}



	private void verifyZeroPriceDeliveryCost(final PriceValue deliveryCost)
	{
		assertEquals(0.0, deliveryCost.getValue(), PRECISION);
		assertEquals(USD_ISOCODE, deliveryCost.getCurrencyIso());
		assertTrue(deliveryCost.isNet());
	}
}