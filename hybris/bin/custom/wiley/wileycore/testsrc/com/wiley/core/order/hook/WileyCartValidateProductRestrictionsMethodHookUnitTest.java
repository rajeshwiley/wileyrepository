package com.wiley.core.order.hook;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.transaction.TransactionException;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import com.wiley.core.event.WileyOrderEntryEvent;
import com.wiley.core.subscription.services.WileySubscriptionService;
import com.wiley.core.wiley.restriction.WileyProductRestrictionService;
import com.wiley.core.wiley.restriction.dto.WileyRestrictionCheckResultDto;
import com.wiley.core.wiley.session.storage.WileyFailedCartModificationsStorageService;

import static com.wiley.core.wiley.restriction.dto.WileyRestrictionCheckResultDto.failureResult;
import static com.wiley.core.wiley.restriction.dto.WileyRestrictionCheckResultDto.successfulResult;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyCartValidateProductRestrictionsMethodHookUnitTest
{
	@InjectMocks
	private WileyCartValidateProductRestrictionsMethodHook wileyCartValidateProductRestrictionsMethodHook;
	@Mock
	private ModelService modelService;
	@Mock
	private WileyFailedCartModificationsStorageService wileyFailedCartModificationsStorageService;
	@Mock
	private WileySubscriptionService wileySubscriptionService;
	@Mock
	private WileyProductRestrictionService wileyProductVisibilityService;
	@Mock
	private EventService eventService;

	private TransactionTemplate transactionTemplate = new TransactionTemplate()
	{
		@Override
		public <T> T execute(final TransactionCallback<T> action) throws TransactionException
		{
			return action.doInTransaction(null);
		}
	};

	@Mock
	private CartEntryModel cartEntryModel;
	@Mock
	private ProductModel productModel;

	private CartModel cartModel;

	private WileyRestrictionCheckResultDto successfulResult = successfulResult();
	private WileyRestrictionCheckResultDto failureResult = failureResult("errorCode", "errorMessage", null);

	@Before
	public void setUp()
	{
		wileyCartValidateProductRestrictionsMethodHook.setTransactionTemplate(transactionTemplate);

		cartModel = new CartModel();
		cartModel.setEntries(Collections.singletonList(cartEntryModel));
		when(cartEntryModel.getProduct()).thenReturn(productModel);
	}

	@Test
	public void shouldDoNotAddFailuresWhenCartIsNull()
	{
		final CommerceCartParameter successfulCommerceCartParameter = givenCommerceCartParameterWithResult(successfulResult);
		successfulCommerceCartParameter.setCart(null);

		wileyCartValidateProductRestrictionsMethodHook.beforeCalculate(successfulCommerceCartParameter);

		verify(wileyFailedCartModificationsStorageService).pushAll(Collections.emptyList());
	}

	@Test
	public void shouldDoNotAddFailuresWhenCartEntriesAreNull()
	{
		final CommerceCartParameter successfulCommerceCartParameter = givenCommerceCartParameterWithResult(successfulResult);
		cartModel.setEntries(null);

		wileyCartValidateProductRestrictionsMethodHook.beforeCalculate(successfulCommerceCartParameter);

		verify(wileyFailedCartModificationsStorageService).pushAll(Collections.emptyList());
	}

	@Test
	public void shouldDoNotAddFailuresWhenCartEntryIsValid()
	{
		final CommerceCartParameter successfulCommerceCartParameter = givenCommerceCartParameterWithResult(successfulResult);

		wileyCartValidateProductRestrictionsMethodHook.beforeCalculate(successfulCommerceCartParameter);

		verify(wileyFailedCartModificationsStorageService).pushAll(Collections.emptyList());
	}

	@Test
	public void shouldAddFailuresAndRemoveCartEntryWhenCartEntryIsInvalid()
	{
		final CommerceCartParameter failureCommerceCartParameter = givenCommerceCartParameterWithResult(failureResult);

		wileyCartValidateProductRestrictionsMethodHook.beforeCalculate(failureCommerceCartParameter);

		verify(modelService).remove(cartEntryModel);
		verify(wileyFailedCartModificationsStorageService).pushAll(any(List.class));
	}

	@Test
	public void shouldSendOrderEntryRemovalEventIfItEnabled()
	{
		final CommerceCartParameter failureCommerceCartParameter = givenCommerceCartParameterWithResult(failureResult);
		wileyCartValidateProductRestrictionsMethodHook.setSendOrderEntryRemovalEvent(true);

		wileyCartValidateProductRestrictionsMethodHook.beforeCalculate(failureCommerceCartParameter);

		verify(eventService).publishEvent(any(WileyOrderEntryEvent.class));
		verify(modelService).remove(cartEntryModel);
		verify(wileyFailedCartModificationsStorageService).pushAll(any(List.class));

	}

	private CommerceCartParameter givenCommerceCartParameterWithResult(final WileyRestrictionCheckResultDto result)
	{
		final CommerceCartParameter commerceCartParameter = new CommerceCartParameter();
		commerceCartParameter.setCart(cartModel);
		when(wileyProductVisibilityService.isAvailable(any(CommerceCartParameter.class))).thenReturn(result);
		return commerceCartParameter;
	}

}
