package com.wiley.core.payment.response.validators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.paypal.hybris.constants.PaypalConstants;
import com.paypal.hybris.data.GetExpressCheckoutDetailsResultData;
import com.wiley.core.i18n.WileyCommonI18NService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PayPalExpressCheckoutResponseCurrencyValidatorUnitTest
{

	private static final String COUNTRY_CODE_US = "us";
	private static final String COUNTRY_CODE_GB = "gb";
	private static final String COUNTRY_CODE_AU = "au";

	@Mock
	private WileyCommonI18NService wileyCommonI18NService;
	@Mock
	private CommonI18NService commonI18NService;
	@Mock
	private CartService cartService;

	@InjectMocks
	private PayPalExpressCheckoutResponseCurrencyValidator testInstance = new PayPalExpressCheckoutResponseCurrencyValidator();

	private CountryModel us = new CountryModel();
	private CountryModel gb = new CountryModel();
	private CountryModel au = new CountryModel();

	CurrencyModel usd = new CurrencyModel();
	CurrencyModel aud = new CurrencyModel();

	CartModel sessionCart = new CartModel();

	GetExpressCheckoutDetailsResultData resultData = new GetExpressCheckoutDetailsResultData();
	AddressData billingAddress = new AddressData();
	CountryData billingCountry = new CountryData();

	@Before
	public void setUp() {
		billingAddress.setCountry(billingCountry);
		resultData.setBillingAddress(billingAddress);

		when(commonI18NService.getCountry(COUNTRY_CODE_US)).thenReturn(us);
		when(commonI18NService.getCountry(COUNTRY_CODE_GB)).thenReturn(gb);
		when(commonI18NService.getCountry(COUNTRY_CODE_AU)).thenReturn(au);

		//US and GB countries have the same currency, and AU has different - AUD
		when(wileyCommonI18NService.getDefaultCurrency(us)).thenReturn(usd);
		when(wileyCommonI18NService.getDefaultCurrency(gb)).thenReturn(usd);

		when(wileyCommonI18NService.getDefaultCurrency(au)).thenReturn(aud);

		when(cartService.getSessionCart()).thenReturn(sessionCart);
	}

	@Test
	public void shouldReturnTrueIfPayPalCountryCurrencyIsTheSameAsInTheCart() {
		//Given
		//GB and US have the same currency
		billingCountry.setIsocode(COUNTRY_CODE_GB);
		sessionCart.setCurrency(usd);
		//When
		//Then
		Assert.assertTrue(testInstance.validateResponse(resultData));
	}

	@Test
	public void shouldReturnFalseAndFillErrorDataIfPayPalCountryCurrencyDifferentThanInCart() {
		//Given
		//GB and US have the same currency
		billingCountry.setIsocode(COUNTRY_CODE_US);
		sessionCart.setCurrency(aud);
		//When
		//Then
		Assert.assertFalse(testInstance.validateResponse(resultData));
		Assert.assertEquals(PaypalConstants.STATUS_ERROR, resultData.getAck());
		Assert.assertEquals(1, resultData.getErrors().size());

	}
}