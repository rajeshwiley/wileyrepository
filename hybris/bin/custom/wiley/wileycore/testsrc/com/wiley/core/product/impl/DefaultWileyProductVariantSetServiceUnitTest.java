package com.wiley.core.product.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.GenericVariantProductModel;
import de.hybris.platform.variants.model.VariantValueCategoryModel;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.hamcrest.collection.IsIterableContainingInAnyOrder;
import org.hamcrest.core.IsSame;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatcher;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.wiley.core.model.WileyProductModel;
import com.wiley.core.model.WileyProductVariantSetModel;
import com.wiley.core.model.WileyVariantProductModel;
import com.wiley.core.product.WileyProductService;
import com.wiley.core.product.WileyProductVariantSetInformation;
import com.wiley.core.product.dao.WileyProductVariantSetDao;
import com.wiley.core.wiley.restriction.WileyProductRestrictionService;


/**
 * Unit test for {@link DefaultWileyProductVariantSetService}.
 *
 * @author Aliaksei_Zlobich
 */
@UnitTest
public class DefaultWileyProductVariantSetServiceUnitTest
{

	private DefaultWileyProductVariantSetService defaultWileyProductVariantSetService;

	@Mock
	private WileyProductVariantSetDao wileyProductVariantSetDaoMock;

	@Mock
	private WileyProductService wileyProductServiceMock;

	@Mock
	private WileyProductRestrictionService wileyProductRestrictionServiceMock;

	@Mock
	private WileyProductModel productMock;

	private WileyVariantProductModel variantProductStub1;
	private WileyVariantProductModel variantProductStub2;
	private WileyVariantProductModel variantProductStub3;


	private WileyVariantProductModel variantProductStub1ForSet2;
	private WileyVariantProductModel variantProductStub2ForSet2;

	// Product Sets
	@Mock
	private WileyProductVariantSetModel productVariantSetMock1;
	@Mock
	private WileyProductVariantSetModel productVariantSetMock2;


	@Mock
	private VariantValueCategoryModel variantValueSetCategoryMock1, variantValueSetCategoryMock2;
	@Mock
	private VariantValueCategoryModel variantValueCategoryMock1, variantValueCategoryMock2;


	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);

		variantProductStub1 = new WileyVariantProductModel();
		variantProductStub2 = new WileyVariantProductModel();
		variantProductStub3 = new WileyVariantProductModel();
		variantProductStub1ForSet2 = new WileyVariantProductModel();
		variantProductStub2ForSet2 = new WileyVariantProductModel();

		when(wileyProductServiceMock.canBePartOfProductSet(argThat(new ArgumentMatcher<ProductModel>()
		{
			@Override
			public boolean matches(final Object o)
			{
				ProductModel product = (ProductModel) o;
				return WileyVariantProductModel.class.equals(product.getClass());
			}
		}))).thenReturn(true);

		when(productMock.getVariants()).thenReturn(Arrays.asList(variantProductStub1, variantProductStub2, variantProductStub3,
				productVariantSetMock1, variantProductStub1ForSet2, variantProductStub2ForSet2, productVariantSetMock2));

		variantProductStub1.setSupercategories(Arrays.asList(variantValueCategoryMock1));
		variantProductStub2.setSupercategories(Arrays.asList(variantValueCategoryMock1));
		variantProductStub3.setSupercategories(Arrays.asList(variantValueCategoryMock1));

		variantProductStub1ForSet2.setSupercategories(Arrays.asList(variantValueCategoryMock2));
		variantProductStub2ForSet2.setSupercategories(Arrays.asList(variantValueCategoryMock2));

		when(wileyProductRestrictionServiceMock.isVisible(variantProductStub1)).thenReturn(true);
		when(wileyProductRestrictionServiceMock.isVisible(variantProductStub2)).thenReturn(true);
		when(wileyProductRestrictionServiceMock.isVisible(variantProductStub3)).thenReturn(false);
		when(wileyProductRestrictionServiceMock.isVisible(eq(productVariantSetMock1))).thenReturn(true);

		when(wileyProductRestrictionServiceMock.isVisible(variantProductStub1ForSet2)).thenReturn(true);
		when(wileyProductRestrictionServiceMock.isVisible(variantProductStub2ForSet2)).thenReturn(true);
		when(wileyProductRestrictionServiceMock.isVisible(eq(productVariantSetMock2))).thenReturn(false);

		when(variantValueSetCategoryMock1.getIsSetCategory()).thenReturn(true);

		when(productVariantSetMock1.getBaseProduct()).thenReturn(productMock);
		when(productVariantSetMock1.getSupercategories()).thenReturn(Arrays.asList(variantValueSetCategoryMock1,
				variantValueCategoryMock1));

		when(productVariantSetMock2.getBaseProduct()).thenReturn(productMock);
		when(productVariantSetMock2.getSupercategories()).thenReturn(Arrays.asList(variantValueSetCategoryMock2,
				variantValueCategoryMock2));

		when(wileyProductVariantSetDaoMock.findProductSets()).thenReturn(Arrays.<WileyProductVariantSetModel> asList(
				productVariantSetMock1));

		defaultWileyProductVariantSetService = new DefaultWileyProductVariantSetService();
		defaultWileyProductVariantSetService.setWileyProductVariantSetDao(wileyProductVariantSetDaoMock);
		defaultWileyProductVariantSetService.setWileyProductService(wileyProductServiceMock);
		defaultWileyProductVariantSetService.setWileyProductRestrictionService(wileyProductRestrictionServiceMock);
	}


	@Test
	public void getWileyProductSetsByProductsSuccess()
	{
		// Given
		Set<ProductModel> products = new HashSet<>();
		products.add(variantProductStub1);
		products.add(variantProductStub2);
		products.add(variantProductStub3);

		// When
		final List<WileyProductVariantSetInformation> setInformationList =
				defaultWileyProductVariantSetService.getWileyProductSetsByProducts(products);

		// Then
		assertTrue("Expected filled list of set information objects.", CollectionUtils.isNotEmpty(setInformationList));
		assertEquals(1, setInformationList.size());
		final WileyProductVariantSetInformation information = setInformationList.get(0);
		assertThat(information.getProductSet(), IsSame.sameInstance(productVariantSetMock1));
		assertThat(information.getIncludedProducts(), IsIterableContainingInAnyOrder.containsInAnyOrder(variantProductStub1,
				variantProductStub2));
		verify(wileyProductVariantSetDaoMock).findProductSets();
	}

	@Test
	public void getWileyProductSetsWhenSetInvisible()
	{
		// Given
		Set<ProductModel> products = new HashSet<>();
		products.add(variantProductStub1ForSet2);
		products.add(variantProductStub2ForSet2);

		// When
		final List<WileyProductVariantSetInformation> setInformationList =
				defaultWileyProductVariantSetService.getWileyProductSetsByProducts(products);

		// Then
		assertTrue("Expected filled list of set information objects.", CollectionUtils.isEmpty(setInformationList));
		verify(wileyProductVariantSetDaoMock).findProductSets();
	}

	@Test
	public void getWileyProductSetsByProductsIfProductIsNull()
	{
		// Given
		// No changes in test data

		try
		{
			// When
			defaultWileyProductVariantSetService.getWileyProductSetsByProducts(null);
			fail("Expected IllegalArgumentException.");
		}
		catch (IllegalArgumentException e)
		{
			// Then
			// Success
		}
	}

	@Test
	public void getSetEntriesSuccess()
	{
		// Given
		// No changes in test data

		// When
		final List<GenericVariantProductModel> setEntries = defaultWileyProductVariantSetService.getSetEntries(
				productVariantSetMock1);

		// Then
		assertTrue("Expected filled list of products.", CollectionUtils.isNotEmpty(setEntries));
		assertThat(setEntries, IsIterableContainingInAnyOrder.containsInAnyOrder(variantProductStub1, variantProductStub2));
	}

	@Test
	public void getSetEntriesIfNoRequiredVariantCategory()
	{
		// Given
		variantProductStub1.setSupercategories(Collections.emptyList());
		variantProductStub2.setSupercategories(Collections.emptyList());
		variantProductStub3.setSupercategories(Collections.emptyList());

		// When
		final List<GenericVariantProductModel> setEntries = defaultWileyProductVariantSetService.getSetEntries(
				productVariantSetMock1);

		// Then
		assertTrue("Expected empty list of products.", CollectionUtils.isEmpty(setEntries));
		assertNotNull(setEntries);
	}

	@Test
	public void getSetEntriesIfNoRequiredVariantCategory2()
	{
		// Given
		when(variantValueSetCategoryMock1.getIsSetCategory()).thenReturn(false);

		// When
		final List<GenericVariantProductModel> setEntries = defaultWileyProductVariantSetService.getSetEntries(
				productVariantSetMock1);

		// Then
		assertTrue("Expected empty list of products.", CollectionUtils.isEmpty(setEntries));
		assertNotNull(setEntries);
	}

	@Test
	public void getSetEntriesIfSetIsNull()
	{
		// Given
		// No changes in test data

		try
		{
			// When
			defaultWileyProductVariantSetService.getSetEntries(null);
			fail("Expected IllegalArgumentException.");
		}
		catch (IllegalArgumentException e)
		{
			// Then
			// Success
		}
	}

	@Test
	public void shouldReturnProductSetForBaseProduct() {
		List<WileyProductVariantSetInformation> sets
				= defaultWileyProductVariantSetService.getWileyProductSetsByBaseProduct(productMock);

		assertEquals(1, sets.size());
		WileyProductVariantSetInformation setInformation = sets.get(0);
		assertEquals(productVariantSetMock1, setInformation.getProductSet());
		assertThat(setInformation.getIncludedProducts(),
				IsIterableContainingInAnyOrder.containsInAnyOrder(variantProductStub1, variantProductStub2));
	}

	@Test
	public void shouldReturnEmptyListWhenNoSetsFound() {
		when(productMock.getVariants()).thenReturn(Arrays.asList(variantProductStub1, variantProductStub2));
		List<WileyProductVariantSetInformation> sets
				= defaultWileyProductVariantSetService.getWileyProductSetsByBaseProduct(productMock);
		assertNotNull(sets);
		assertEquals(0, sets.size());
	}
}