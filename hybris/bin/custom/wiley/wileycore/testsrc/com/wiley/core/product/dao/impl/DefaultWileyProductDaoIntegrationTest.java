/**
 *
 */
package com.wiley.core.product.dao.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.search.SearchResult;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.model.WileyProductModel;
import com.wiley.core.product.dao.WileyProductDao;

import static org.fest.assertions.Assertions.assertThat;


/**
 *
 */
@IntegrationTest
public class DefaultWileyProductDaoIntegrationTest extends
		AbstractWileyServicelayerTransactionalTest
{

	public static final String TEST_RESOURCES_FOLDER = "/wileycore/test/product/dao/DefaultWileyProductDaoIntegrationTest";

	public static final String CATALOG_ID = "welProductCatalog";
	public static final String CATALOG_VERSION = "Online";


	public static final String CATEGORY_CODE = "WEL_CFA_CATEGORY";
	public static final String CATEGORY_CODE_UNKNOWN = "WEL_CFA_CATEGORY_UNKNOWN";

	/**
	 * Validation constants
	 */
	public static final String VALIDATION_CATALOG_ID = "catalogToValidate";
	public static final String VALIDATION_CV_STAGED = "Staged";
	public static final String VALIDATION_CV_ONLINE = "Online";
	public static final String PRODUCT_STAGED_APPROVED = "PRODUCT_STAGED_APPROVED";
	public static final String PRODUCT_STAGED_CHECK = "PRODUCT_STAGED_CHECK";

	@Resource
	private WileyProductDao wileyProductDao;
	@Resource
	private CatalogVersionService catalogVersionService;
	@Resource
	private CategoryService categoryService;

	/**
	 * Before.
	 *
	 * @throws Exception
	 * 		the exception
	 */
	@Before
	public void before() throws Exception
	{
		catalogVersionService.setSessionCatalogVersion(CATALOG_ID, CATALOG_VERSION);
	}


	/**
	 * Test find WileyProduct by code.
	 *
	 * @throws Exception
	 * 		the exception
	 */
	@Test
	public void testFindWileyProductByCode() throws Exception
	{
		final List<WileyProductModel> products = wileyProductDao
				.findWileyProductsByCode("WEL_CMA_REVIEW_COURSE");
		assertThat(products).isNotNull();
		assertThat(products).isNotEmpty();
		assertEquals("WEL_CMA_REVIEW_COURSE", products.get(0).getCode());
	}

	/**
	 * Test find WileyProduct by code when code is null.
	 */
	@Test
	public void testFindWileyProductByCodeWhenCodeIsNull()
	{
		//when
		try
		{
			wileyProductDao.findWileyProductsByCode(null);
			Assert.fail("Should throw IllegalArgumentException when code is null");
		}
		catch (final IllegalArgumentException ex)
		{
			//OK
		}
	}

	/**
	 * Test find WileyProduct by code when code is unknown.
	 */
	@Test
	public void testFindWileyProductByCodeWhenCodeIsUnknown()
	{
		//when
		final List<WileyProductModel> products = wileyProductDao.findWileyProductsByCode("unknown");

		//then
		assertThat(products).isNotNull();
		assertThat(products).isEmpty();
	}

	/**
	 * Test find WileyProduct by code when code is for VariantProduct.
	 */
	@Test
	public void testFindWileyProductByCodeWhenCodeIsForVariantProduct()
	{
		//when
		final List<WileyProductModel> products = wileyProductDao
				.findWileyProductsByCode("WEL_CMA_REVIEW_COURSE_PRINT_PART_1");

		//then
		assertThat(products).isNotNull();
		assertThat(products).isEmpty();
	}

	/**
	 * Test find products by category excluding its subcategories.
	 */
	@Test
	public void testFindProductsByCategoryExcludingSubCategories()
	{
		final SearchResult<ProductModel> result = wileyProductDao.findProductsByCategoryExcludingSubCategories(
				categoryService.getCategoryForCode(catalogVersionService.getSessionCatalogVersionForCatalog(CATALOG_ID),
						"cpa"));
		assertThat(result).isNotNull();
		Assert.assertEquals("Result count", 3, result.getCount());
	}

	/**
	 * Test find WileyProducts from root of category.
	 *
	 * @throws Exception
	 * 		the exception
	 */
	@Test
	public void testFindWileyProductFromRootOfCategory() throws Exception
	{
		final List<WileyProductModel> products = wileyProductDao
				.findWileyProductsFromCategoryRoot(CATEGORY_CODE);
		assertThat(products).isNotNull();
		assertThat(products).isNotEmpty();
		assertEquals(9, products.size());
	}

	/**
	 * Test find WileyProducts from root of Category when Category code is null.
	 */
	@Test
	public void testFindWileyProductFromRootOfCategoryWhenCategoryCodeIsNull()
	{
		//when
		try
		{
			wileyProductDao.findWileyProductsFromCategoryRoot(null);
			Assert.fail("Should throw IllegalArgumentException when Category code is null");
		}
		catch (final IllegalArgumentException ex)
		{
			//OK
		}
	}

	/**
	 * Test find WileyProducts from root of Category when Category code is unknown.
	 */
	@Test
	public void testFindWileyProductFromRootOfCategoryWhenCategoryCodeIsUnknown()
	{
		//when
		final List<WileyProductModel> products = wileyProductDao.findWileyProductsFromCategoryRoot(CATEGORY_CODE_UNKNOWN);

		//then
		assertThat(products).isNotNull();
		assertThat(products).isEmpty();
	}

	@Test
	public void testFindProductsByIsbn()
	{
		//given
		final String isbn = "100010";
		final CatalogVersionModel catalogVersionModel = catalogVersionService.getCatalogVersion(CATALOG_ID, CATALOG_VERSION);

		//when
		final List<ProductModel> products = wileyProductDao.findProductsByIsbn(isbn, catalogVersionModel);

		//then
		assertTrue(products.size() == 1);
		assertEquals("WEL_CMA_REVIEW_COURSE_PRINT_PART_1", products.get(0).getCode());
	}

	@Test
	public void testFindCheckProductsToValidate() throws ImpExException
	{
		//given
		importCsv(TEST_RESOURCES_FOLDER + "/productsToValidation.impex", DEFAULT_ENCODING);
		final CatalogVersionModel stagedCatalogVersion = catalogVersionService.getCatalogVersion(VALIDATION_CATALOG_ID,
				VALIDATION_CV_STAGED);

		//when
		final List<ProductModel> products = wileyProductDao.findProductsToValidate(ArticleApprovalStatus.CHECK,
				stagedCatalogVersion);

		//then
		assertThat(products).hasSize(1);
		assertThat(products.iterator().next().getCode()).isEqualTo(PRODUCT_STAGED_CHECK);
	}

	/**
	 * Approved products are not supposed to be validated, here used only for test purposes.
	 */
	@Test
	public void testFindApprovedProductsToValidate() throws ImpExException
	{
		//given
		importCsv(TEST_RESOURCES_FOLDER + "/productsToValidation.impex", DEFAULT_ENCODING);
		final CatalogVersionModel stagedCatalogVersion = catalogVersionService.getCatalogVersion(VALIDATION_CATALOG_ID,
				VALIDATION_CV_STAGED);

		//when
		final List<ProductModel> products = wileyProductDao.findProductsToValidate(ArticleApprovalStatus.APPROVED,
				stagedCatalogVersion);

		//then
		assertThat(products).hasSize(1);
		assertThat(products.iterator().next().getCode()).isEqualTo(PRODUCT_STAGED_APPROVED);
	}

	@Test
	public void testFindNotExistedProductsToValidate() throws ImpExException
	{
		//given
		importCsv(TEST_RESOURCES_FOLDER + "/productsToValidation.impex", DEFAULT_ENCODING);
		final CatalogVersionModel onlineCatalogVersion = catalogVersionService.getCatalogVersion(VALIDATION_CATALOG_ID,
				VALIDATION_CV_ONLINE);

		//when
		final List<ProductModel> products = wileyProductDao.findProductsToValidate(ArticleApprovalStatus.CHECK,
				onlineCatalogVersion);

		//then
		assertThat(products).isEmpty();
	}
}