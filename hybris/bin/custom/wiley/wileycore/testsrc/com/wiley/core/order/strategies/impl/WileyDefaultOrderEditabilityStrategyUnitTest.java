package com.wiley.core.order.strategies.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.wiley.core.order.strategies.impl.OrderEntriesUtil.createOrderEntriesWithStatus;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyDefaultOrderEditabilityStrategyUnitTest
{
	@InjectMocks
	private WileyDefaultOrderEditabilityStrategyImpl defaultOrderEditabilityStrategy;
	@Mock
	private Set<OrderStatus> orderEntryEditableStatusesMock;
	@Mock
	private OrderModel orderMock;

	@Before
	public void setup()
	{
		when(orderEntryEditableStatusesMock.contains(OrderStatus.CREATED)).thenReturn(true);
		when(orderEntryEditableStatusesMock.contains(OrderStatus.INVOICE_PENDING)).thenReturn(true);
		when(orderEntryEditableStatusesMock.contains(OrderStatus.INVOICE_OVERDUE)).thenReturn(true);
		when(orderEntryEditableStatusesMock.contains(OrderStatus.CANCELLED)).thenReturn(true);
	}

	@Test
	public void shouldReturnEditableForCorrectStatuses()
	{
		Set<OrderStatus> statuses = new HashSet<>(
				Arrays.asList(OrderStatus.CREATED, OrderStatus.INVOICE_PENDING, OrderStatus.INVOICE_OVERDUE,
						OrderStatus.CANCELLED));
		createOrderEntriesWithStatus(orderMock, statuses);
		assertTrue(defaultOrderEditabilityStrategy.hasAllEditableEntries(orderMock));
	}

	@Test
	public void shouldReturnEditableForIncorrectStatuses()
	{
		Set<OrderStatus> statuses = new HashSet<>(
				Arrays.asList(OrderStatus.PAYMENT_CAPTURED, OrderStatus.FAILED, OrderStatus.PAYMENT_WAIVED));
		createOrderEntriesWithStatus(orderMock, statuses);
		assertFalse(defaultOrderEditabilityStrategy.hasAllEditableEntries(orderMock));
	}
}
