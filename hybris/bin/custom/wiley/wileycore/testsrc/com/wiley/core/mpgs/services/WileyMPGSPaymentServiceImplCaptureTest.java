package com.wiley.core.mpgs.services;


import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.math.BigDecimal;
import java.util.Optional;

import org.apache.commons.configuration.Configuration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyTnsMerchantModel;
import com.wiley.core.mpgs.constants.WileyMPGSConstants;
import com.wiley.core.mpgs.request.WileyCaptureRequest;
import com.wiley.core.mpgs.response.WileyCaptureResponse;
import com.wiley.core.mpgs.services.impl.WileyMPGSPaymentServiceImpl;
import com.wiley.core.mpgs.strategies.WileyMPGSTransactionIdGeneratorStrategy;
import com.wiley.core.payment.transaction.PaymentTransactionService;
import com.wiley.core.payment.tnsmerchant.service.WileyTnsMerchantService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyMPGSPaymentServiceImplCaptureTest
{
	private static final BigDecimal AMOUNT = new BigDecimal(100);
	private static final String CURRENCY_ISO_CODE = "USA";
	private static final String TEST_ISO_CODE = "US";
	private static final String TEST_TRANSACTION_ID = "TEST_TRANSACTION_ID";
	private static final String TEST_MERCHANT_ID = "TEST_MERCHANT_ID";
	public static final String TEST_CAPTURE_URL = "https://pki.na.tnspayments.com/api/rest/version/45/merchant/TESTWILEYABATEST/"
			+ "order/TEST_ORDER_ID/transaction/TEST_TRANSACTION_ID";
	private static final String AS_SITE_ID = "asSite";
	private static final String TEST_CODE = "a19b9a12-87c9-4c75-b35e-d943f569e59d";

	@Mock
	private PaymentTransactionModel transaction;

	@Mock
	private PaymentTransactionEntryModel authorizationEntry;

	@Mock
	private CurrencyModel currency;

	@Mock
	private OrderModel order;

	@Mock
	private PaymentTransactionService paymentTransactionService;

	@Mock
	private WileyCardPaymentService wileyCardPaymentService;

	@Mock
	private WileyMPGSPaymentEntryService wileyMPGSPaymentEntryService;

	@Mock
	private WileyUrlGenerationService wileyUrlGenerationService;

	@Mock
	private WileyMPGSTransactionIdGeneratorStrategy wileyMPGSTransactionIdGenerateStrategy;

	@Mock
	private WileyMPGSMerchantService wileyMPGSMerchantService;

	@Mock
	private WileyTnsMerchantService wileyTnsMerchantService;

	@Mock
	private ConfigurationService configurationService;

	@Mock
	private Configuration configuration;
	
	@Mock
	private AddressModel paymentAddress;
	
	@Mock
	private CountryModel countryModel;
	
	@Mock
	private BaseSiteModel baseSiteModel;

	@Mock
	private WileyTnsMerchantModel wileyTnsMerchantMock;

	@InjectMocks
	private WileyMPGSPaymentServiceImpl underTestMpgsPaymentServiceImpl = new WileyMPGSPaymentServiceImpl();


	@Test
	public void shouldCreateCaptureEntry()
	{
		// given

		when(paymentTransactionService.findAuthorizationEntry(transaction)).thenReturn(Optional.of(authorizationEntry));
		when(authorizationEntry.getAmount()).thenReturn(AMOUNT);
		when(transaction.getOrder()).thenReturn(order);
		when(authorizationEntry.getCurrency()).thenReturn(currency);
		when(currency.getIsocode()).thenReturn(CURRENCY_ISO_CODE);
		when(wileyMPGSTransactionIdGenerateStrategy.generateTransactionId()).thenReturn(TEST_TRANSACTION_ID);
		when(wileyUrlGenerationService.getCaptureUrl(any(String.class), any(String.class),
				any(String.class))).thenReturn(TEST_CAPTURE_URL);
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getString(WileyMPGSConstants.PAYMENT_MPGS_VENDOR_ID)).thenReturn("257");
		when(order.getSite()).thenReturn(baseSiteModel);
		when(baseSiteModel.getUid()).thenReturn(AS_SITE_ID);
		when(order.getPaymentAddress()).thenReturn(paymentAddress);
		when(paymentAddress.getCountry()).thenReturn(countryModel);
		when(countryModel.getIsocode()).thenReturn(TEST_ISO_CODE);
		when(wileyTnsMerchantService.getTnsMerchant(TEST_ISO_CODE, CURRENCY_ISO_CODE)).thenReturn(wileyTnsMerchantMock);
		when(wileyTnsMerchantMock.getId()).thenReturn(TEST_MERCHANT_ID);
		when(transaction.getCode()).thenReturn(TEST_CODE);

		// when
		Optional<PaymentTransactionEntryModel> captureEntry = underTestMpgsPaymentServiceImpl.capture(transaction);

		// then
		verify(wileyCardPaymentService, times(1)).capture(any(WileyCaptureRequest.class));
		verify(wileyMPGSPaymentEntryService, times(1)).createCaptureEntry(any(WileyCaptureResponse.class),
				any(PaymentTransactionModel.class));

	}

	@Test
	public void shouldNotCreateCaptureEntry()
	{
		// given
		when(paymentTransactionService.findAuthorizationEntry(transaction)).thenReturn(Optional.empty());
		when(transaction.getOrder()).thenReturn(order);
		// when
		Optional<PaymentTransactionEntryModel> captureEntry = underTestMpgsPaymentServiceImpl.capture(transaction);

		// then
		verify(wileyCardPaymentService, times(0)).capture(any(WileyCaptureRequest.class));
		verify(wileyMPGSPaymentEntryService, times(0)).createCaptureEntry(any(WileyCaptureResponse.class),
				any(PaymentTransactionModel.class));

	}


	@Test(expected = IllegalArgumentException.class)
	public void shouldNotCallCaptureWhenNull()
	{
		//when
		Optional<PaymentTransactionEntryModel> captureEntry = underTestMpgsPaymentServiceImpl.capture(null);
		//then
		verify(wileyCardPaymentService, times(0)).capture(any(WileyCaptureRequest.class));
	}

}
