package com.wiley.core.order.hook;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Unit test for {@link WileyCartCalculationMethodDiscountGroupHook}.<br/>
 *
 * Unit test checks CommerceCartCalculationMethodHook behavior which is implemented by
 * WileyCartCalculationMethodDiscountGroupHook.
 *
 * Created by Maksim_Kozich on 03.03.2016.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyCartCalculationMethodDiscountGroupHookUnitTest
{

	@InjectMocks
	private WileyCartCalculationMethodDiscountGroupHook wileyCartCalculationMethodDiscountGroupHook;

	@Test(expected = IllegalArgumentException.class)
	public void testNullParameter()
	{
		// Given

		// When
		wileyCartCalculationMethodDiscountGroupHook.beforeCalculate(null);

		// Then
	}
}
