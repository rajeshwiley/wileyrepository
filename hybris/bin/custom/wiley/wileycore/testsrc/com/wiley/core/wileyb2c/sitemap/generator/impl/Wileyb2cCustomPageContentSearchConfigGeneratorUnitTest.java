package com.wiley.core.wileyb2c.sitemap.generator.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.model.SiteMapConfigModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Created by Maksim_Kozich on 29.08.17.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cCustomPageContentSearchConfigGeneratorUnitTest
{
	private static final String CUSTOM_URL_1 = "Custom URL 1";
	private static final String CUSTOM_URL_2 = "Custom URL 2";
	private static final List<String> CUSTOM_URLS = Arrays.asList(CUSTOM_URL_1, CUSTOM_URL_2);

	@Mock
	private CMSSiteModel cmsSiteModelMock;

	@Mock
	private SiteMapConfigModel siteMapConfigModelMock;

	@InjectMocks
	private Wileyb2cCustomPageContentSearchConfigGenerator testInstance;

	@Test
	public void testGetDataInternalShouldReturnContentSearchConfigCustomUrls()
	{
		// Given
		when(cmsSiteModelMock.getContentSearchConfig()).thenReturn(siteMapConfigModelMock);
		when(siteMapConfigModelMock.getCustomUrls()).thenReturn(CUSTOM_URLS);

		// When
		List<String> result = testInstance.getDataInternal(cmsSiteModelMock);

		// Then
		assertSame(result, CUSTOM_URLS);
	}

	@Test
	public void testGetDataInternalShouldReturnEmptyListIfContentSearchConfigIsNull()
	{
		// Given
		when(cmsSiteModelMock.getContentSearchConfig()).thenReturn(null);

		// When
		List<String> result = testInstance.getDataInternal(cmsSiteModelMock);

		// Then
		assertNotNull(result);
		assertTrue(result.size() == 0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetDataInternalShouldThrowIllegalArgumentExceptionIfNullPassed() throws Exception
	{
		testInstance.getDataInternal(null);
	}
}
