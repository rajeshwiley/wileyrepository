package com.wiley.core.paymentinfo.impl;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Assert;
import org.junit.Test;

/**
 * Unit test for {@link WileyCreditCardNumberHelper}.<br/>
 *
 * Unit test checks WileyCreditCardNumberHelper behavior.
 *
 * Created by Raman_Hancharou on 1/15/2016.
 */
@UnitTest
public class WileyCreditCardNumberHelperTest
{
	private WileyCreditCardNumberHelper wileyCreditCardNumberHelper = new WileyCreditCardNumberHelper();

	/**
	 * Test mask credit card number when number is valid.
	 */
	@Test
	public void testMaskCreditCardNumberWithValidNumber()
	{
		String actual = wileyCreditCardNumberHelper.maskCreditCardNumber("1231 23xx xxxx 1234");

		Assert.assertNotNull(actual);
		Assert.assertEquals("************1234", actual);
	}

	/**
	 * Test mask credit card number when number is null.
	 */
	@Test
	public void testMaskCreditCardNumberWithNullNumber()
	{
		String actual = wileyCreditCardNumberHelper.maskCreditCardNumber(null);

		Assert.assertNull(actual);
	}
}
