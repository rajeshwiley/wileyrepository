package com.wiley.core.integration.alm.authentication.service.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;

import com.wiley.core.exceptions.ExternalSystemInternalErrorException;
import com.wiley.core.exceptions.ExternalSystemNotFoundException;
import com.wiley.core.integration.alm.authentication.AlmAuthenticationGateway;
import com.wiley.core.integration.alm.authentication.dto.AlmAuthenticationResponseDto;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AlmAuthenticationServiceImplUnitTest
{
	private static final String TEST_TOKEN = "1";

	@Mock
	private AlmAuthenticationGateway gatewayMock;

	@InjectMocks
	private AlmAuthenticationServiceImpl service;

	@Test
	public void validateSessionTokenSuccessTest()
	{
		//Given
		final AlmAuthenticationResponseDto dto = new AlmAuthenticationResponseDto();
		when(gatewayMock.validateSessionToken(TEST_TOKEN)).thenReturn(dto);

		//When
		final AlmAuthenticationResponseDto result = service.validateSessionToken(TEST_TOKEN);

		//Then
		assertNotNull(result);
		assertEquals(dto, result);
	}

	@Test
	public void validateSessionTokenInternalServerErrorTest()
	{
		when(gatewayMock.validateSessionToken(TEST_TOKEN)).thenThrow(ExternalSystemInternalErrorException.class);

		testError(INTERNAL_SERVER_ERROR);
	}

	@Test
	public void validateSessionTokenNotFoundErrorTest()
	{
		when(gatewayMock.validateSessionToken(TEST_TOKEN)).thenThrow(ExternalSystemNotFoundException.class);

		testError(NOT_FOUND);
	}

	private void testError(final HttpStatus httpStatus)
	{
		//When
		final AlmAuthenticationResponseDto result = service.validateSessionToken(TEST_TOKEN);

		//Then
		assertNotNull(result);
		assertNull(result.getUserId());
		assertNull(result.getImitateeId());
		assertEquals(httpStatus, result.getHttpStatus());
	}
}