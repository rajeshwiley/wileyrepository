package com.wiley.core.wileyb2c.search.solrfacetsearch.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.model.config.SolrFacetSearchConfigModel;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cFacetSearchConfigPopulatorUnitTest
{
	private static final Integer FACET_LIMIT = new Integer(99);
	private static final Integer FACET_VIEW_MORE_LIMIT = new Integer(88);
	@InjectMocks
	private Wileyb2cFacetSearchConfigPopulator wileyb2cFacetSearchConfigPopulator;
	@Mock
	private SolrFacetSearchConfigModel sourceMock;
	@Mock
	private FacetSearchConfig targetMock;

	@Test
	public void testLimitsArePopulated()
	{
		when(sourceMock.getFacetLimit()).thenReturn(FACET_LIMIT);
		when(sourceMock.getFacetViewMoreLimit()).thenReturn(FACET_VIEW_MORE_LIMIT);

		wileyb2cFacetSearchConfigPopulator.populate(sourceMock, targetMock);

		verify(targetMock).setFacetLimit(FACET_LIMIT);
		verify(targetMock).setFacetViewMoreLimit(FACET_VIEW_MORE_LIMIT);
	}
}
