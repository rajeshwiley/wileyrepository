package com.wiley.core.product.dao.impl;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.model.WileyVariantProductModel;
import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;


/**
 * Integration test for {@link DefaultWileyVariantProductDao}
 *
 * @author Aliaksei_Zlobich
 */
@IntegrationTest
public class DefaultWileyVariantProductDaoIntegrationalTest extends AbstractWileyServicelayerTransactionalTest
{

	/**
	 * The constant CATALOG_ID.
	 */
	public static final String CATALOG_ID = "welProductCatalog";

	/**
	 * The constant CATALOG_VERSION.
	 */
	public static final String CATALOG_VERSION = "Online";

	@Resource
	private CatalogVersionService catalogVersionService;

	@Resource
	private DefaultWileyVariantProductDao defaultWileyVariantProductDao;

	/**
	 * Before.
	 *
	 * @throws Exception
	 * 		the exception
	 */
	@Before
	public void before() throws Exception
	{
		catalogVersionService.setSessionCatalogVersion(CATALOG_ID, CATALOG_VERSION);
	}

	/**
	 * Test find product by isbn successful.
	 */
	@Test
	public void testFindProductByISBNSuccessful()
	{
		// Given
		final String isbn = "100010";
		final CatalogVersionModel catalogVersion = catalogVersionService.getCatalogVersion(CATALOG_ID, CATALOG_VERSION);

		// When
		final Optional<WileyVariantProductModel> productByISBN = defaultWileyVariantProductDao.findProductByISBNIfExists(isbn,
				catalogVersion);

		// Then
		WileyVariantProductModel wileyVariantProductModel = productByISBN.get();
		assertNotNull(wileyVariantProductModel);
		assertEquals(isbn, wileyVariantProductModel.getIsbn());
	}

	/**
	 * Test find product by isbn not found.
	 */
	@Test
	public void testFindProductByISBNNotFound()
	{
		// Given
		final String isbn = "0000000000000000";
		final CatalogVersionModel catalogVersion = catalogVersionService.getCatalogVersion(CATALOG_ID, CATALOG_VERSION);

		// When

		final Optional<WileyVariantProductModel> productByISBN = defaultWileyVariantProductDao.findProductByISBNIfExists(isbn,
				catalogVersion);

		// Then

		boolean present = productByISBN.isPresent();
		assertFalse(present);
	}

}