package com.wiley.core.wileyb2c.product;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.catalog.references.ProductReferenceService;
import de.hybris.platform.core.model.product.ProductModel;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.enums.WileyProductSubtypeEnum;

import static junit.framework.Assert.assertEquals;


/**
 * Test for {@link Wileyb2cCourseProductServiceImpl#getReferencedEprofProduct(ProductModel)} method
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cCourseProductServiceImplEprofProductUnitTest
{

	private static final boolean ACTIVE_REFERENCES_ONLY = true;

	@InjectMocks
	private Wileyb2cCourseProductServiceImpl testInstance;

	@Mock
	private ProductReferenceService productReferenceServiceMock;

	@Mock
	private ProductModel courseProductMock;

	@Before
	public void setup()
	{
		when(courseProductMock.getSubtype()).thenReturn(WileyProductSubtypeEnum.COURSE);
	}

	@Test(expected = NullPointerException.class)
	public void shouldThrowNullPointerExceptionDueToNullParameter()
	{
		//When
		testInstance.getReferencedEprofProduct(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowIllegalArgumentExceptionDueToProductSubtypeIsNotCourse()
	{
		//Given
		when(courseProductMock.getSubtype()).thenReturn(WileyProductSubtypeEnum.HARDCOVER);

		//When
		testInstance.getReferencedEprofProduct(courseProductMock);
	}

	@Test(expected = IllegalStateException.class)
	public void shouldThrowIllegalStateExceptionDueToEprofProductHasnoReferencesToCourseProduct()
	{
		//Given
		when(productReferenceServiceMock
				.getProductReferencesForTargetProduct(courseProductMock, ProductReferenceTypeEnum.WILEY_PLUS_COURSE,
						ACTIVE_REFERENCES_ONLY)).thenReturn(Collections.emptyList());

		//When
		testInstance.getReferencedEprofProduct(courseProductMock);
	}

	@Test(expected = IllegalStateException.class)
	public void shouldThrowIllegalStateExceptionDueToEprofProductHasNullReferencesToCourseProduct()
	{
		//Given
		when(productReferenceServiceMock
				.getProductReferencesForTargetProduct(courseProductMock, ProductReferenceTypeEnum.WILEY_PLUS_COURSE,
						ACTIVE_REFERENCES_ONLY)).thenReturn(Collections.emptyList());

		//When
		testInstance.getReferencedEprofProduct(courseProductMock);
	}

	@Test(expected = IllegalStateException.class)
	public void shouldThrowIllegalStateExceptionDueToCourseProductHasMoreThanOneEprofProduct()
	{
		//Given
		List<ProductReferenceModel> references = Arrays.asList(new ProductReferenceModel(), new ProductReferenceModel());
		when(productReferenceServiceMock
				.getProductReferencesForTargetProduct(courseProductMock, ProductReferenceTypeEnum.WILEY_PLUS_COURSE,
						ACTIVE_REFERENCES_ONLY)).thenReturn(references);

		//When
		testInstance.getReferencedEprofProduct(courseProductMock);
	}

	@Test
	public void shouldReturnReferencedEprofProduct()
	{
		//Given
		ProductModel expectedeprofProductMock = mock(ProductModel.class);
		ProductReferenceModel eprofToCourseReference = mock(ProductReferenceModel.class);
		when(eprofToCourseReference.getSource()).thenReturn(expectedeprofProductMock);
		when(productReferenceServiceMock
				.getProductReferencesForTargetProduct(courseProductMock, ProductReferenceTypeEnum.WILEY_PLUS_COURSE,
						ACTIVE_REFERENCES_ONLY)).thenReturn(Collections.singletonList(eprofToCourseReference));

		//When
		ProductModel actualEprofProductModel = testInstance.getReferencedEprofProduct(courseProductMock);

		//Then
		assertEquals(expectedeprofProductMock, actualEprofProductModel);
	}



}
