package com.wiley.core.security.impl;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


/**
 * @author Mikhail_Kondratyev
 */
@UnitTest
public class WileyPbkdf2MessageEncoderTest
{
	private static final String SECRET_KEY = "This is a very secret key.";
	private static final String MESSAGE = "customer@gmail.com2011-04-17T15:10:00+0400";
	private static final String FAKE_MESSAGE = "customer@gmail.com2011-04-17T06:11:59+0400";

	private WileyPbkdf2MessageEncoder encoder;

	@Before
	public void setUp() throws Exception
	{
		encoder = new WileyPbkdf2MessageEncoder();
		encoder.setSecretKey(SECRET_KEY);
		encoder.afterPropertiesSet();
	}

	@Test
	public void testEncoding()
	{
		final String hash1 = encoder.encode(MESSAGE);
		final String hash2 = encoder.encode(MESSAGE);
		Assert.assertNotEquals(hash1, hash2);

		Assert.assertTrue(encoder.check(MESSAGE, hash1));
		Assert.assertTrue(encoder.check(MESSAGE, hash2));

		final char c = hash1.charAt(0) == 'a' ? 'b' : 'a';
		Assert.assertFalse(encoder.check(MESSAGE, c + hash1.substring(1)));
		Assert.assertFalse(encoder.check(MESSAGE, hash2.substring(4)));
		Assert.assertFalse(encoder.check(FAKE_MESSAGE, hash1));
	}
}
