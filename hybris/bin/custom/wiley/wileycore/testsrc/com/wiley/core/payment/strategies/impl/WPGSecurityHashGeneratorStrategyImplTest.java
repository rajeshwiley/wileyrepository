package com.wiley.core.payment.strategies.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.store.WileyBaseStoreService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WPGSecurityHashGeneratorStrategyImplTest
{
	private static final String STORE_ONE_ACCOUNT_PASSWORD = "siteOneAccPwd";
	private static final String STORE_TWO_ACCOUNT_PASSWORD = "siteTwoAccPwd";

	private static final String COUNTER_MEASURE_BASE = "someRandomString";
	private static final String SITE_ONE_SECURITY_HASH = "03d6e7b8fc77feeea4f4fe83225f4e2f";
	private static final String SITE_TWO_SECURITY_HASH = "6c9d873d1f2af15ec8f830bcdaab5c77";
	private static final String SITE_ONE_ID = "siteOneId";
	private static final String SITE_TWO_ID = "siteTwoId";
	private static final String SITE_THREE_ID = "siteThreeId";

	private static final String BASE_STORE_1_UID = "storeOneId";
	private static final String BASE_STORE_2_UID = "storeTwoId";

	@Mock
	private WileyBaseStoreService wileyBaseStoreServiceMock;

	@InjectMocks
	private WPGSecurityHashGeneratorStrategyImpl testedInstance = new WPGSecurityHashGeneratorStrategyImpl();

	@Before
	public void setUp()
	{
		HashMap<String, char[]> passwords = new HashMap<>();
		passwords.put(BASE_STORE_1_UID, STORE_ONE_ACCOUNT_PASSWORD.toCharArray());
		passwords.put(BASE_STORE_2_UID, STORE_TWO_ACCOUNT_PASSWORD.toCharArray());

		testedInstance.setVendorAccountPasswords(passwords);

		when(wileyBaseStoreServiceMock.getBaseStoreUidForSite(eq(SITE_ONE_ID))).thenReturn(BASE_STORE_1_UID);
		when(wileyBaseStoreServiceMock.getBaseStoreUidForSite(eq(SITE_TWO_ID))).thenReturn(BASE_STORE_2_UID);
	}

	@Test
	public void shouldReturnFirstSiteHashForSiteOne()
	{
		String generatedSecurityHash = testedInstance.generateSecurityHash(SITE_ONE_ID, COUNTER_MEASURE_BASE);

		assertEquals(SITE_ONE_SECURITY_HASH, generatedSecurityHash);
	}

	@Test
	public void shouldReturnSecondSiteHashForSiteTwo()
	{
		String generatedSecurityHash = testedInstance.generateSecurityHash(SITE_TWO_ID, COUNTER_MEASURE_BASE);

		assertEquals(SITE_TWO_SECURITY_HASH, generatedSecurityHash);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowWhenSiteVendorPasswordIsNotSet()
	{
		testedInstance.generateSecurityHash(SITE_THREE_ID, COUNTER_MEASURE_BASE);
	}
}