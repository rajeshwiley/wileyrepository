package com.wiley.core.product.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.wiley.core.enums.ProductEditionFormat;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;



/**
 * Unit test for {@link DefaultWileyProductEditionFormatService}.
 *
 * @author Aliaksei_Zlobich
 */
@UnitTest
public class DefaultWileyProductEditionFormatServiceUnitTest
{

	private DefaultWileyProductEditionFormatService defaultWileyProductEditionFormatService;

	@Mock
	private ProductModel productMock;

	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);

		this.defaultWileyProductEditionFormatService = new DefaultWileyProductEditionFormatService();
	}


	@Test
	public void shouldReturnShippableProductIfProductEditionFormatIsNull()
	{
		// Given
		ProductModel productModelMock = mock(ProductModel.class);
		when(productModelMock.getEditionFormat()).thenReturn(null);

		AbstractOrderModel orderModelMock = createOrderMockWithProductInSpecifiedEditionFormat(productModelMock);

		// When
		final List<ProductModel> allShippableProducts = defaultWileyProductEditionFormatService.getAllShippableProducts(
				orderModelMock);

		// Then
		checkIfListContainsOnlyExpectedProduct(productModelMock, allShippableProducts);
	}

	@Test
	public void shouldReturnShippableProductIfProductEditionFormatIsPhysical()
	{
		// Given
		ProductModel productModelMock = mock(ProductModel.class);
		when(productModelMock.getEditionFormat()).thenReturn(ProductEditionFormat.PHYSICAL);

		AbstractOrderModel orderModelMock = createOrderMockWithProductInSpecifiedEditionFormat(productModelMock);

		// When
		final List<ProductModel> allShippableProducts = defaultWileyProductEditionFormatService.getAllShippableProducts(
				orderModelMock);

		// Then
		checkIfListContainsOnlyExpectedProduct(productModelMock, allShippableProducts);
	}

	@Test
	public void shouldReturnShippableProductIfProductEditionFormatIsPhysicalAndDigital()
	{
		// Given
		ProductModel productModelMock = mock(ProductModel.class);
		when(productModelMock.getEditionFormat()).thenReturn(ProductEditionFormat.DIGITAL_AND_PHYSICAL);

		AbstractOrderModel orderModelMock = createOrderMockWithProductInSpecifiedEditionFormat(productModelMock);

		// When
		final List<ProductModel> allShippableProducts = defaultWileyProductEditionFormatService.getAllShippableProducts(
				orderModelMock);

		// Then
		checkIfListContainsOnlyExpectedProduct(productModelMock, allShippableProducts);
	}

	@Test
	public void shouldNotReturnShippableProductIfProductEditionFormatIsDigital()
	{
		// Given
		ProductModel productModelMock = mock(ProductModel.class);
		when(productModelMock.getEditionFormat()).thenReturn(ProductEditionFormat.DIGITAL);

		AbstractOrderModel orderModelMock = createOrderMockWithProductInSpecifiedEditionFormat(productModelMock);

		// When
		final List<ProductModel> allShippableProducts = defaultWileyProductEditionFormatService.getAllShippableProducts(
				orderModelMock);

		// Then
		assertTrue("Expected that list with shippable products is empty.", allShippableProducts.isEmpty());
	}

	private void checkIfListContainsOnlyExpectedProduct(final ProductModel productModel, final List<ProductModel> productList)
	{
		assertEquals("Expected that list will have only one product.", 1, productList.size());
		assertSame(productModel, productList.get(0));
	}

	private AbstractOrderModel createOrderMockWithProductInSpecifiedEditionFormat(final ProductModel productModel)
	{
		List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();

		AbstractOrderEntryModel orderEntryModelMock = mock(AbstractOrderEntryModel.class);
		when(orderEntryModelMock.getProduct()).thenReturn(productModel);
		orderEntries.add(orderEntryModelMock);
		AbstractOrderModel orderModelMock = mock(AbstractOrderModel.class);
		when(orderModelMock.getEntries()).thenReturn(orderEntries);
		return orderModelMock;
	}

}