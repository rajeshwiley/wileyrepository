package com.wiley.core.payment.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionRequest;
import de.hybris.platform.acceleratorservices.payment.data.CustomerShipToData;
import de.hybris.platform.acceleratorservices.payment.data.PaymentData;
import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static com.wiley.core.payment.populators.ShipToWPGHttpRequestPopulator.WPG_CUSTOM_SHIPTO_CITY;
import static com.wiley.core.payment.populators.ShipToWPGHttpRequestPopulator.WPG_CUSTOM_SHIPTO_COUNTRY_NAME;
import static com.wiley.core.payment.populators.ShipToWPGHttpRequestPopulator.WPG_CUSTOM_SHIPTO_FIRST_NAME;
import static com.wiley.core.payment.populators.ShipToWPGHttpRequestPopulator.WPG_CUSTOM_SHIPTO_LAST_NAME;
import static com.wiley.core.payment.populators.ShipToWPGHttpRequestPopulator.WPG_CUSTOM_SHIPTO_LINE1;
import static com.wiley.core.payment.populators.ShipToWPGHttpRequestPopulator.WPG_CUSTOM_SHIPTO_LINE2;
import static com.wiley.core.payment.populators.ShipToWPGHttpRequestPopulator.WPG_CUSTOM_SHIPTO_POSTAL_CODE;
import static com.wiley.core.payment.populators.ShipToWPGHttpRequestPopulator.WPG_CUSTOM_SHIPTO_STATE_NAME;
import static com.wiley.core.payment.populators.ShipToWPGHttpRequestPopulator.WPG_CUSTOM_SHIPTO_TITLE_NAME;


/**
 * Test for {@link ShipToWPGHttpRequestPopulator}
 */

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ShipToWPGHttpRequestPopulatorUnitTest
{

	private static final String SHIPTO_TITLE_NAME = "Mr.";
	private static final String SHIPTO_FIRST_NAME = "Fname";
	private static final String SHIPTO_LAST_NAME = "Lname";
	private static final String SHIPTO_LINE1 = "150 Fifth Avenue";
	private static final String SHIPTO_LINE2 = "31st floor";
	private static final String SHIPTO_CITY = "New York";
	private static final String SHIPTO_STATE_NAME = "NY";
	private static final String SHIPTO_POSTAL_CODE = "10118";
	private static final String SHIPTO_COUNTRY_NAME = "USA";

	@InjectMocks
	private ShipToWPGHttpRequestPopulator shipToWPGHttpRequestPopulator;

	private CustomerShipToData customerShipToData;
	private CreateSubscriptionRequest createSubscriptionRequest;
	private PaymentData paymentData;


	@Before
	public void setup()
	{
		customerShipToData = new CustomerShipToData();
		createSubscriptionRequest = new CreateSubscriptionRequest();
		createSubscriptionRequest.setCustomerShipToData(customerShipToData);
		paymentData = new PaymentData();
	}

	@Test
	public void shouldPopulateTitleName()
	{
		customerShipToData.setShipToTitleName(SHIPTO_TITLE_NAME);

		shipToWPGHttpRequestPopulator.populate(createSubscriptionRequest, paymentData);

		assertEquals(SHIPTO_TITLE_NAME, paymentData.getParameters().get(WPG_CUSTOM_SHIPTO_TITLE_NAME));
	}

	@Test
	public void shouldPopulateFirstName()
	{
		customerShipToData.setShipToFirstName(SHIPTO_FIRST_NAME);

		shipToWPGHttpRequestPopulator.populate(createSubscriptionRequest, paymentData);

		assertEquals(SHIPTO_FIRST_NAME, paymentData.getParameters().get(WPG_CUSTOM_SHIPTO_FIRST_NAME));
	}

	@Test
	public void shouldPopulateLastName()
	{
		customerShipToData.setShipToLastName(SHIPTO_LAST_NAME);

		shipToWPGHttpRequestPopulator.populate(createSubscriptionRequest, paymentData);

		assertEquals(SHIPTO_LAST_NAME, paymentData.getParameters().get(WPG_CUSTOM_SHIPTO_LAST_NAME));
	}

	@Test
	public void shouldPopulateLine1()
	{
		customerShipToData.setShipToStreet1(SHIPTO_LINE1);

		shipToWPGHttpRequestPopulator.populate(createSubscriptionRequest, paymentData);

		assertEquals(SHIPTO_LINE1, paymentData.getParameters().get(WPG_CUSTOM_SHIPTO_LINE1));
	}

	@Test
	public void shouldPopulateLine2()
	{
		customerShipToData.setShipToStreet2(SHIPTO_LINE2);

		shipToWPGHttpRequestPopulator.populate(createSubscriptionRequest, paymentData);

		assertEquals(SHIPTO_LINE2, paymentData.getParameters().get(WPG_CUSTOM_SHIPTO_LINE2));
	}

	@Test
	public void shouldPopulateCity()
	{
		customerShipToData.setShipToCity(SHIPTO_CITY);

		shipToWPGHttpRequestPopulator.populate(createSubscriptionRequest, paymentData);

		assertEquals(SHIPTO_CITY, paymentData.getParameters().get(WPG_CUSTOM_SHIPTO_CITY));
	}

	@Test
	public void shouldPopulateStateName()
	{
		customerShipToData.setShipToStateName(SHIPTO_STATE_NAME);

		shipToWPGHttpRequestPopulator.populate(createSubscriptionRequest, paymentData);

		assertEquals(SHIPTO_STATE_NAME, paymentData.getParameters().get(WPG_CUSTOM_SHIPTO_STATE_NAME));
	}

	@Test
	public void shouldPopulatePostalCode()
	{
		customerShipToData.setShipToPostalCode(SHIPTO_POSTAL_CODE);

		shipToWPGHttpRequestPopulator.populate(createSubscriptionRequest, paymentData);

		assertEquals(SHIPTO_POSTAL_CODE, paymentData.getParameters().get(WPG_CUSTOM_SHIPTO_POSTAL_CODE));
	}

	@Test
	public void shouldPopulateCountryName()
	{
		customerShipToData.setShipToCountryName(SHIPTO_COUNTRY_NAME);

		shipToWPGHttpRequestPopulator.populate(createSubscriptionRequest, paymentData);

		assertEquals(SHIPTO_COUNTRY_NAME, paymentData.getParameters().get(WPG_CUSTOM_SHIPTO_COUNTRY_NAME));
	}


}
