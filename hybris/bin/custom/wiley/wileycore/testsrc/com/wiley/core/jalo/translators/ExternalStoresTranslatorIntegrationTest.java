package com.wiley.core.jalo.translators;

import static com.wiley.core.enums.WileyWebLinkTypeEnum.WOL;
import static org.fest.assertions.Assertions.assertThat;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.enums.WileyWebLinkTypeEnum;
import com.wiley.core.jalo.translators.WileyWebLinksIntegrationTest.WebLinkAssert;


public class ExternalStoresTranslatorIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	private static final String TEST_RESOURCES_FOLDER = "/wileycore/test/jalo/translators/"
			+ "ExternalStoresTranslatorIntegrationTest";
	private static final String TEST_STORE_URL = "http://bcs.wiley.com/?bcsId=10000";
	private static final String TEST_VARIANT_PRODUCT_CODE = "PURCHASE_OPTION_PRODUCT ";
	private static final String LANG_EN_ISOCODE = "en";
	private static final WileyWebLinkTypeEnum TEST_STORE_TYPE = WOL;


	@Resource
	private ProductService productService;
	@Resource
	private CommonI18NService commonI18NService;

	@Before
	public void setUp() throws Exception
	{
		commonI18NService.setCurrentLanguage(commonI18NService.getLanguage(LANG_EN_ISOCODE));
	}

	@Test(expected = ImpExException.class)
	public void shouldThrowExceptionDueToExternalStoresWrongHeaderType() throws Exception
	{
		final String impexFileName = TEST_RESOURCES_FOLDER + "/wrongHeaderTypeExternalStores.impex";
		importStream(getClass().getResourceAsStream(impexFileName), DEFAULT_ENCODING, impexFileName, false);
	}

	@Test
	public void shouldFillOneExternalStore() throws Exception
	{
		//Given
		importCsv(TEST_RESOURCES_FOLDER + "/externalStore.impex", DEFAULT_ENCODING);

		//When
		final ProductModel product = productService.getProductForCode(TEST_VARIANT_PRODUCT_CODE);

		//Then
		assertThat(product.getExternalStores()).hasSize(1);
		WebLinkAssert.assertThat(product.getExternalStores().iterator().next()).hasTypeAndUrl(TEST_STORE_TYPE, TEST_STORE_URL);
	}

	@Test
	public void shouldFillEmptyFieldDueToEmptyImpexValue() throws Exception
	{
		//Given
		importCsv(TEST_RESOURCES_FOLDER + "/emptyExternalStores.impex", DEFAULT_ENCODING);

		//When
		final ProductModel product = productService.getProductForCode(TEST_VARIANT_PRODUCT_CODE);

		//Then
		assertThat(product.getExternalStores()).isEmpty();
	}

	@Test
	public void shouldFillEmptyFieldDueToListOfEmptyTokens() throws Exception
	{
		//Given
		importCsv(TEST_RESOURCES_FOLDER + "/emptyTokensExternalStores.impex", DEFAULT_ENCODING);

		//When
		final ProductModel product = productService.getProductForCode(TEST_VARIANT_PRODUCT_CODE);

		//Then
		assertThat(product.getExternalStores()).isEmpty();
	}

	@Test(expected = ImpExException.class)
	public void shouldThrowExceptionDueToAbsentPipe() throws Exception
	{
		final String impexFileName = TEST_RESOURCES_FOLDER + "/externalStoreWithoutPipe.impex";
		importStream(getClass().getResourceAsStream(impexFileName), DEFAULT_ENCODING, impexFileName, false);
	}


	@Test(expected = ImpExException.class)
	public void shouldThrowExceptionDueToAbsentUrl() throws Exception
	{
		//Given
		final String impexFileName = TEST_RESOURCES_FOLDER + "/externalStoreWithPipe.impex";
		importStream(getClass().getResourceAsStream(impexFileName), DEFAULT_ENCODING, impexFileName, false);
	}
}
