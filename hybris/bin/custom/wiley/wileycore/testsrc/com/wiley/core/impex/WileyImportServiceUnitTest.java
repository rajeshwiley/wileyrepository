package com.wiley.core.impex;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.impex.model.cronjob.ImpExImportCronJobModel;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.servicelayer.cronjob.CronJobService;
import de.hybris.platform.servicelayer.impex.ImportConfig;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.impex.model.WileyHotFolderImpExImportJobModel;


/**
 * Created by Uladzimir_Barouski on 11/3/2016.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyImportServiceUnitTest
{
	@Spy
	@InjectMocks
	private WileyImportService wileyImportService;

	@Mock
	private CronJobService cronJobServiceMock;

	@Mock
	private ModelService modelServiceMock;


	@Test
	public void testImportData() throws Exception
	{
		ImportConfig config = new ImportConfig();
		ImpExImportCronJobModel cronJob = new ImpExImportCronJobModel();
		when(modelServiceMock.create("ImpExImportCronJob")).thenReturn(cronJob);
		WileyHotFolderImpExImportJobModel job = new WileyHotFolderImpExImportJobModel();
		when(cronJobServiceMock.getJob("WileyHotFolder-ImpEx-Import")).thenReturn(job);
		doNothing().when(wileyImportService).importData(cronJob, config.isSynchronous(), config.isRemoveOnSuccess());
		doNothing().when(wileyImportService).configureCronJob(cronJob, config);
		doNothing().when(modelServiceMock).saveAll(new Object[] { cronJob.getJob(), cronJob });
		Item item = mock(Item.class);
		when(modelServiceMock.getSource(cronJob)).thenReturn(item);
		when(item.isAlive()).thenReturn(true);

		final ArgumentMatcher<ImpExImportCronJobModel> matcher = new ArgumentMatcher<ImpExImportCronJobModel>()
		{

			@Override
			public boolean matches(final Object argument)
			{
				ImpExImportCronJobModel cronjob = (ImpExImportCronJobModel) argument;
				if (cronjob.getJob() == job)
				{
					return true;
				}
				return false;
			}
		};

		wileyImportService.importData(config);
		verify(wileyImportService).importData(eq(cronJob), eq(config.isSynchronous()), eq(config.isRemoveOnSuccess()));
		verify(wileyImportService).configureCronJob(eq(cronJob), eq(config));
		verify(modelServiceMock).saveAll(new Object[] { eq(job), argThat(matcher) });
	}
}