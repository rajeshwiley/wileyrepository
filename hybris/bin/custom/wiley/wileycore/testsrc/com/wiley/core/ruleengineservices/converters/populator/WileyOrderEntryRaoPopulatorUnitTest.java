package com.wiley.core.ruleengineservices.converters.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.ruleengineservices.converters.populator.OrderEntryRaoPopulator;
import de.hybris.platform.ruleengineservices.rao.AbstractRuleActionRAO;
import de.hybris.platform.ruleengineservices.rao.DiscountRAO;
import de.hybris.platform.ruleengineservices.rao.OrderEntryRAO;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.util.DiscountValue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.LinkedHashSet;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static java.util.Arrays.asList;


/**
 * Unit test for {@link WileyOrderEntryRaoPopulator}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyOrderEntryRaoPopulatorUnitTest
{
	private static final Double TEST_SUBTOTAL_PRICE = 12.2d;

	@Mock
	private AbstractOrderEntryModel abstractOrderEntryModelMock;

	@Mock
	private OrderEntryRAO entryRaoMock;

	@Mock
	private OrderEntryRaoPopulator orderEntryRaoPopulatorMock;

	@Mock
	private Converter<DiscountValue, DiscountRAO> wileyDiscountRaoConverterMock;

	@Mock
	private DiscountValue discountValueMock;

	@Mock
	private DiscountRAO discountRaoMock;

	@Mock
	private AbstractRuleActionRAO actionRaoMock;

	@InjectMocks
	private WileyOrderEntryRaoPopulator wileyOrderEntryRaoPopulator;

	@Before
	public void setUp()
	{
		when(entryRaoMock.getActions()).thenReturn(givenActions(actionRaoMock));

		when(abstractOrderEntryModelMock.getDiscountValues()).thenReturn(asList(discountValueMock));
		when(wileyDiscountRaoConverterMock.convert(discountValueMock)).thenReturn(discountRaoMock);

	}

	@Test
	public void shouldExecutePopulator() throws Exception
	{
		// When
		wileyOrderEntryRaoPopulator.populate(abstractOrderEntryModelMock, entryRaoMock);

		// Then
		verify(orderEntryRaoPopulatorMock).populate(eq(abstractOrderEntryModelMock), eq(entryRaoMock));
	}

	@Test
	public void shouldLeaveNullSubtotalPrice() throws Exception
	{
		// Given
		when(abstractOrderEntryModelMock.getSubtotalPrice()).thenReturn(null);

		// When
		wileyOrderEntryRaoPopulator.populate(abstractOrderEntryModelMock, entryRaoMock);

		// Then
		verify(entryRaoMock, never()).setSubtotalPrice(any());
	}

	@Test
	public void shouldPopulateSubtotalPrice() throws Exception
	{
		// Given
		when(abstractOrderEntryModelMock.getSubtotalPrice()).thenReturn(TEST_SUBTOTAL_PRICE);

		// When
		wileyOrderEntryRaoPopulator.populate(abstractOrderEntryModelMock, entryRaoMock);

		// Then
		verify(entryRaoMock).setSubtotalPrice(eq(BigDecimal.valueOf(TEST_SUBTOTAL_PRICE)));
	}

	@Test
	public void shouldAddActionsFromDiscountValuesAtTheBeginning()
	{
		// When
		wileyOrderEntryRaoPopulator.populate(abstractOrderEntryModelMock, entryRaoMock);

		// Then
		verify(entryRaoMock).setActions(givenActions(discountRaoMock, actionRaoMock));
	}

	@Test
	public void shouldAddActionsFromDiscountValuesWhenActionsListIsEmpty()
	{
		// Given
		when(entryRaoMock.getActions()).thenReturn(null);

		// When
		wileyOrderEntryRaoPopulator.populate(abstractOrderEntryModelMock, entryRaoMock);

		// Then
		verify(entryRaoMock).setActions(givenActions(discountRaoMock));
	}


	@Test
	public void shouldSetTheSameActionsIfDiscountValuesListIsEmpty()
	{
		// Given
		when(abstractOrderEntryModelMock.getDiscountValues()).thenReturn(null);

		// When
		wileyOrderEntryRaoPopulator.populate(abstractOrderEntryModelMock, entryRaoMock);

		// Then
		verify(entryRaoMock).setActions(givenActions(actionRaoMock));
	}



	private LinkedHashSet<AbstractRuleActionRAO> givenActions(final AbstractRuleActionRAO... actions)
	{
		LinkedHashSet<AbstractRuleActionRAO> actionsSet = new LinkedHashSet<>();
		for (AbstractRuleActionRAO action : actions)
		{
			actionsSet.add(action);
		}
		return actionsSet;
	}
}
