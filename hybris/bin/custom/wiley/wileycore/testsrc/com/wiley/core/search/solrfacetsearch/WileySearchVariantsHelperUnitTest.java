package com.wiley.core.search.solrfacetsearch;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.search.Document;
import de.hybris.platform.solrfacetsearch.search.FacetSearchException;
import de.hybris.platform.solrfacetsearch.search.GroupCommandField;
import de.hybris.platform.solrfacetsearch.search.QueryField;
import de.hybris.platform.solrfacetsearch.search.RawQuery;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.solrfacetsearch.search.SearchResult;
import de.hybris.platform.solrfacetsearch.search.SearchResultGroup;
import de.hybris.platform.solrfacetsearch.search.SearchResultGroupCommand;
import de.hybris.platform.solrfacetsearch.search.context.FacetSearchContext;
import de.hybris.platform.solrfacetsearch.search.context.FacetSearchContextFactory;
import de.hybris.platform.solrfacetsearch.search.impl.SearchQueryConverterData;
import de.hybris.platform.solrfacetsearch.search.impl.SearchResultConverterData;
import de.hybris.platform.solrfacetsearch.solr.Index;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrRequest;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileySearchVariantsHelperUnitTest
{
	private static final String LANGUAGE = "language";
	private static final String INDEX_NAME = "indexName";
	private static final String CURRENCY_ISO = "currencyIso";
	private static final int PAGE_SIZE = 10;
	private static final SearchQuery.Operator DEFAULT_OPERATOR = SearchQuery.Operator.AND;
	private static final boolean GROUP_FACETS = true;
	private static final String FIELD = "field";
	private static final Map<String, String[]> RAW_PARAMS = Collections.singletonMap("rawParam",
			new String[] { "rawParamValue" });
	private static final String GROUP_NAME = "groupName";
	private static final String GROUP_VALUE = "groupValue";
	@InjectMocks
	private WileySearchVariantsHelper wileySearchVariantsHelper;
	@Mock
	private Converter<SearchResultConverterData, SearchResult> facetSearchResultConverter;
	@Mock
	private Converter<SearchQueryConverterData, SolrQuery> facetSearchQueryConverter;
	@Mock
	private FacetSearchContextFactory<FacetSearchContext> facetSearchContextFactory;
	@Mock
	private WileySearchQuery searchQuery;
	@Mock
	private SearchResultGroupCommand baseSearchResultGroupCommand;
	@Mock
	private SolrClient solrClient;
	@Mock
	private Index index;
	@Mock
	private SearchResult baseProductSearchResult;
	@Mock
	private SearchResultGroup baseGroup;
	@Mock
	private FacetSearchConfig facetSearchConfig;
	@Mock
	private IndexedType indexedType;
	@Mock
	private CatalogVersionModel catalogVersion;
	@Mock
	private QueryField filterQuery;
	@Mock
	private RawQuery rawQuery;
	@Mock
	private GroupCommandField groupCommand;
	@Mock
	private FacetSearchContext facetSearchContext;
	@Mock
	private SolrQuery solrQuery;
	@Mock
	private QueryResponse queryResponse;
	@Mock
	private SearchResult variantSearchResult;
	@Mock
	private SearchResultGroupCommand variantSearchResultGroupCommand;
	@Mock
	private SearchResultGroup variantGroup;
	@Mock
	private Document variantDocument;
	@Mock
	private List<Document> baseDocuments;
	@Mock
	private WileyQueryFacet queryFacet;


	@Test
	public void queryForVariantsWhenGroupExistsShouldPopulateVariants()
			throws SolrServerException, IOException, FacetSearchException
	{
		when(index.getName()).thenReturn(INDEX_NAME);
		when(baseProductSearchResult.getGroupCommands()).thenReturn(Collections.singletonList(baseSearchResultGroupCommand));
		when(baseSearchResultGroupCommand.getGroups()).thenReturn(Collections.singletonList(baseGroup));
		when(baseSearchResultGroupCommand.getName()).thenReturn(GROUP_NAME);
		when(baseGroup.getGroupValue()).thenReturn(GROUP_VALUE);
		when(searchQuery.getFacetSearchConfig()).thenReturn(facetSearchConfig);
		when(searchQuery.getIndexedType()).thenReturn(indexedType);
		when(searchQuery.getCatalogVersions()).thenReturn(Collections.singletonList(catalogVersion));
		when(searchQuery.getLanguage()).thenReturn(LANGUAGE);
		when(searchQuery.getCurrency()).thenReturn(CURRENCY_ISO);
		when(searchQuery.getPageSize()).thenReturn(PAGE_SIZE);
		when(searchQuery.getDefaultOperator()).thenReturn(DEFAULT_OPERATOR);
		when(searchQuery.getFilterQueries()).thenReturn(Collections.singletonList(filterQuery));
		when(searchQuery.getFilterRawQueries()).thenReturn(Collections.singletonList(rawQuery));
		when(searchQuery.getGroupCommands()).thenReturn(Collections.singletonList(groupCommand));
		when(searchQuery.isGroupFacets()).thenReturn(GROUP_FACETS);
		when(searchQuery.getFields()).thenReturn(Collections.singletonList(FIELD));
		when(searchQuery.getRawParams()).thenReturn(RAW_PARAMS);
		when(searchQuery.getQueryFacets()).thenReturn(Collections.singletonList(queryFacet));
		when(facetSearchContextFactory
				.createContext(eq(facetSearchConfig), eq(indexedType), argThat(new ArgumentMatcher<SearchQuery>()
				{
					@Override
					public boolean matches(final Object argument)
					{
						return isCorrectQuery((WileySearchQuery) argument);
					}
				}))).thenReturn(facetSearchContext);
		when(facetSearchQueryConverter.convert(argThat(new ArgumentMatcher<SearchQueryConverterData>()
		{
			@Override
			public boolean matches(final Object argument)
			{
				final SearchQueryConverterData searchQueryConverterData = (SearchQueryConverterData) argument;
				return searchQueryConverterData.getFacetSearchContext() == facetSearchContext
						&& isCorrectQuery((WileySearchQuery) searchQueryConverterData.getSearchQuery());
			}
		}))).thenReturn(solrQuery);
		when(solrClient.query(INDEX_NAME, solrQuery, SolrRequest.METHOD.POST)).thenReturn(queryResponse);
		when(facetSearchResultConverter.convert(createSearchResultConverterData(facetSearchContext, queryResponse))).thenReturn(
				variantSearchResult);
		when(facetSearchContextFactory.getContext()).thenReturn(facetSearchContext);
		when(variantSearchResult.getGroupCommands()).thenReturn(Collections.singletonList(variantSearchResultGroupCommand));
		when(variantSearchResultGroupCommand.getGroups()).thenReturn(Collections.singletonList(variantGroup));
		when(variantGroup.getGroupValue()).thenReturn(GROUP_VALUE);
		when(variantGroup.getDocuments()).thenReturn(Collections.singletonList(variantDocument));
		when(baseGroup.getDocuments()).thenReturn(baseDocuments);

		wileySearchVariantsHelper.queryForVariants(solrClient, index, baseProductSearchResult, searchQuery);

		verify(facetSearchContext).setSearchResult(variantSearchResult);
		verify(facetSearchContextFactory).destroyContext();
		verify(baseDocuments).clear();
		verify(baseDocuments).add(variantDocument);
	}

	private SearchResultConverterData createSearchResultConverterData(final FacetSearchContext facetSearchContext,
			final QueryResponse queryResponse)
	{
		final SearchResultConverterData searchResultConverterData = new SearchResultConverterData();
		searchResultConverterData.setFacetSearchContext(facetSearchContext);
		searchResultConverterData.setQueryResponse(queryResponse);
		return searchResultConverterData;
	}

	private boolean isCorrectQuery(final WileySearchQuery searchQuery)
	{
		return Objects.equals(searchQuery.getFacetSearchConfig(), facetSearchConfig)
				&& Objects.equals(searchQuery.getIndexedType(), indexedType)
				&& Objects.equals(searchQuery.getCatalogVersions(), Collections.singletonList(catalogVersion))
				&& Objects.equals(searchQuery.getLanguage(), LANGUAGE)
				&& Objects.equals(searchQuery.getCurrency(), CURRENCY_ISO)
				&& Objects.equals(searchQuery.getPageSize(), PAGE_SIZE)
				&& Objects.equals(searchQuery.getDefaultOperator(), DEFAULT_OPERATOR)
				&& checkFilterQueries(searchQuery.getFilterQueries())
				&& Objects.equals(searchQuery.getFilterRawQueries(), Collections.singletonList(rawQuery))
				&& Objects.equals(searchQuery.getGroupCommands(), Collections.singletonList(groupCommand))
				&& Objects.equals(searchQuery.isGroupFacets(), GROUP_FACETS)
				&& Objects.equals(searchQuery.getFields(), Collections.singletonList(FIELD))
				&& Objects.equals(searchQuery.getRawParams(), RAW_PARAMS)
				&& Objects.equals(searchQuery.getQueryFacets(), Collections.singletonList(queryFacet));
	}

	private boolean checkFilterQueries(final List<QueryField> filterQueries)
	{

		if (!filterQuery.equals(filterQueries.get(0)))
		{
			return false;
		}
		if (filterQueries.size() != 2)
		{
			return false;
		}
		final QueryField queryField = filterQueries.get(1);
		return GROUP_NAME.equals(queryField.getField()) && SearchQuery.Operator.OR.equals(queryField.getOperator())
				&& queryField.getValues().contains(GROUP_VALUE);
	}

}
