package com.wiley.core.wileyas.order.strategies.validation.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.product.ProductModel;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.wiley.restriction.WileyProductRestrictionService;
import com.wiley.core.wiley.restriction.dto.WileyRestrictionCheckResultDto;
import com.wiley.core.wiley.strategies.cart.validation.impl.WileyProductAvailabilityCartParameterValidator;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyProductAvailabilityValidatorUnitTest
{
	private static final String FAIL_RESULT_MESSAGE = "failMessage";
	@Mock
	private WileyProductRestrictionService wileyProductRestrictionService;

	@InjectMocks
	private WileyProductAvailabilityCartParameterValidator testInstance = new WileyProductAvailabilityCartParameterValidator();

	ProductModel product = new ProductModel();
	CommerceCartParameter commerceCartParameter = new CommerceCartParameter();

	@Before
	public void setUp() throws Exception
	{
		commerceCartParameter.setProduct(product);
	}

	@Test(expected = NullPointerException.class)
	public void shouldThrowExceptionIfNoProductInCommerceCartParameterInput() throws CommerceCartModificationException
	{
		//Given
		commerceCartParameter.setProduct(null);
		//When
		testInstance.validateAddToCart(commerceCartParameter);
	}

	@Test(expected = CommerceCartModificationException.class)
	public void shouldThrowCommerceCartModificationExceptionIfProductIsNotValid() throws CommerceCartModificationException
	{
		//Given
		WileyRestrictionCheckResultDto failedResult = getFailedResult();
		when(wileyProductRestrictionService.isAvailable(commerceCartParameter)).thenReturn(failedResult);
		//When
		testInstance.validateAddToCart(commerceCartParameter);
	}

	@Test
	public void shouldNotThrowAnythingIfProductIsValid() throws CommerceCartModificationException
	{
		//Given
		when(wileyProductRestrictionService.isAvailable(commerceCartParameter))
				.thenReturn(WileyRestrictionCheckResultDto.successfulResult());
		//When
		testInstance.validateAddToCart(commerceCartParameter);
		//Then
		//Success
	}

	@Test
	public void shouldContainProperMessageWhenValidationFailed() {
		//Given
		WileyRestrictionCheckResultDto failedResult = getFailedResult();
		when(wileyProductRestrictionService.isAvailable(commerceCartParameter)).thenReturn(failedResult);
		//When
		try
		{
			testInstance.validateAddToCart(commerceCartParameter);
		}
		//Then
		catch (CommerceCartModificationException e)
		{
			Assert.assertEquals(FAIL_RESULT_MESSAGE, e.getMessage());
			return;
		}
		Assert.fail();
	}

	private WileyRestrictionCheckResultDto getFailedResult() {
		return WileyRestrictionCheckResultDto.failureResult(null, FAIL_RESULT_MESSAGE, new Object[]{});
	}

}