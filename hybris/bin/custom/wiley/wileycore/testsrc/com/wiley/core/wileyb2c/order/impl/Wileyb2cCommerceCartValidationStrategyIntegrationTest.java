package com.wiley.core.wileyb2c.order.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.commerceservices.strategies.CartValidationStrategy;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.wiley.order.impl.WileyCartValidationStrategy;
import com.wiley.core.wileycom.order.impl.WileycomCommerceCartValidationStrategyIntegrationTest;


/**
 * @author Dzmitryi_Halahayeu
 */
@IntegrationTest
public class Wileyb2cCommerceCartValidationStrategyIntegrationTest extends WileycomCommerceCartValidationStrategyIntegrationTest
{

	private static final String NO_PRICES_PRODUCT = "B2C_NO_PRICES_PRODUCT";
	@Resource
	private ProductService productService;
	@Resource
	private WileyCartValidationStrategy wileyCartValidationStrategy;

	@Test
	public void testFailWhenNonPricesRestrictionApply() throws CommerceCartModificationException
	{
		final ProductModel nonPurchasableProduct = productService.getProductForCode(NO_PRICES_PRODUCT);
		final CommerceCartParameter commerceCartParameter = createCartParameter(nonPurchasableProduct);

		final List<CommerceCartModification> modifications = wileyCartValidationStrategy.validateCart(commerceCartParameter);

		assertEquals(commerceCartParameter.getCart().getEntries().size(), 0);
		assertEquals(modifications.size(), 1);
		final CommerceCartModification commerceCartModification = modifications.get(0);
		assertEquals(commerceCartModification.getMessage(), WileyCoreConstants.BASKET_VALIDATION_UNVAILABLE);
		assertEquals(commerceCartModification.getMessageParameters().length, 1);
		assertEquals(commerceCartModification.getMessageParameters()[0], nonPurchasableProduct.getName());
	}


	@Override
	protected String getSiteId()
	{
		return "wileyb2c";
	}

	@Override
	protected String getRestrictedAssortmentProductCode()
	{
		return "B2B_ONLY_PRODUCT";
	}


	@Override
	protected CartValidationStrategy getCartValidationStrategy()
	{
		return wileyCartValidationStrategy;
	}


}
