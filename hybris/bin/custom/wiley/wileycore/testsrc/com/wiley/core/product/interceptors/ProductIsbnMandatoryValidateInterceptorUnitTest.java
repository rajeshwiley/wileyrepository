package com.wiley.core.product.interceptors;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


/**
 * Created by Uladzimir_Barouski on 5/26/2016.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ProductIsbnMandatoryValidateInterceptorUnitTest
{

	private ProductIsbnMandatoryValidateInterceptor isbnValidator = Mockito.spy(new ProductIsbnMandatoryValidateInterceptor());

	@Test(expected = InterceptorException.class)
	public void onValidateShouldThrowInterceptorExceptionIfIsbnIsEmpty() throws Exception
	{
		ProductModel testProduct = givenProduct("");
		givenRestriction(testProduct, true);

		isbnValidator.onValidate(testProduct, null);
	}



	@Test
	public void onValidateShouldNotThrowInterceptorExceptionForWelProductCatalogIfIsbnIsNotEmpty() throws Exception
	{
		ProductModel testProduct = givenProduct("111");
		givenRestriction(testProduct, true);

		isbnValidator.onValidate(testProduct, null);
	}

	@Test
	public void onValidateShouldNotThrowInterceptorExceptionForCatalogs() throws Exception
	{
		ProductModel testProduct = givenProduct("");
		givenRestriction(testProduct, false);

		isbnValidator.onValidate(testProduct, null);
	}

	private ProductModel givenProduct(/*final String catalogId,*/ final String isbn)
	{
		ProductModel productModel = mock(ProductModel.class);
		when(productModel.getIsbn()).thenReturn(isbn);
		return productModel;
	}

	private void givenRestriction(final ProductModel product, final boolean productHasRestriction)
	{
		doReturn(productHasRestriction).when(isbnValidator).isProductWithRestriction(product);
	}
}