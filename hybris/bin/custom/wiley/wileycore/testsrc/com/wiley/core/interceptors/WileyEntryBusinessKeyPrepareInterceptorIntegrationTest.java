package com.wiley.core.interceptors;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.product.UnitService;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.wileyb2c.order.dao.impl.Wileyb2cCommerceCartDaoImpl;

import static org.fest.assertions.Assertions.assertThat;


@IntegrationTest
public class WileyEntryBusinessKeyPrepareInterceptorIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	private static final String TEST_STORE_UID = "wileyb2c";
	private static final String TEST_USER_UID = "test@hybris.com";
	private static final String TEST_CART_GUID = "123456";
	private static final String CATALOG_ID = "wileyProductCatalog";
	private static final String CATALOG_VERSION = "Online";
	private static final String TEST_PRODUCT = "test-product";
	private static final String UNIT_CODE = "pieces";

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Resource
	private EnumerationService enumerationService;

	@Resource
	private CatalogVersionService catalogVersionService;

	@Resource
	private ProductService productService;

	@Resource
	private ModelService modelService;

	@Resource
	private BaseStoreService baseStoreService;

	@Resource
	private UserService userService;

	@Resource
	private Wileyb2cCommerceCartDaoImpl wileyb2cCommerceCartDao;

	@Resource
	private UnitService unitService;

	private CartModel cart;
	private ProductModel product;
	private UnitModel unit;

	@Before
	public void setUp() throws Exception
	{
		importCsv(
				"/wileycore/test/interceptors/WileyEntryBusinessKeyPrepareInterceptorIntegrationTest/test-customer.impex",
				DEFAULT_ENCODING);
		final BaseStoreModel baseStore = baseStoreService.getBaseStoreForUid(TEST_STORE_UID);
		final UserModel user = userService.getUserForUID(TEST_USER_UID);

		importCsv(
				"/wileycore/test/interceptors/WileyEntryBusinessKeyPrepareInterceptorIntegrationTest/test-cart.impex",
				DEFAULT_ENCODING);
		cart = wileyb2cCommerceCartDao.getCartForGuidAndStoreAndUser(TEST_CART_GUID, baseStore, user);

		importCsv(
				"/wileycore/test/interceptors/WileyEntryBusinessKeyPrepareInterceptorIntegrationTest/test-product.impex",
				DEFAULT_ENCODING);
		catalogVersionService.setSessionCatalogVersion(CATALOG_ID, CATALOG_VERSION);
		product = productService.getProductForCode(TEST_PRODUCT);
		unit = unitService.getUnitForCode(UNIT_CODE);
	}

	@Test
	public void shouldUpdateBusinessKeyInDB()
	{
		// Given
		CartEntryModel entry = createEntryWithBusinessKey();
		entry.setBusinessKey(null);
		entry.setStatus(OrderStatus.CREATED);
		modelService.save(entry);

		// When
		String newBusinessKey = "business_key_that_is_new_in_DB";
		entry.setBusinessKey(newBusinessKey);
		modelService.save(entry);

		// Then
		modelService.refresh(entry);
		assertThat(entry.getBusinessKey()).isEqualTo(newBusinessKey);
	}

	@Test
	public void shouldThrowExceptionOnSecondBusinessKeyUpdate()
	{
		// Given
		String businessKey = "business_key_for_two_rows";
		CartEntryModel entry1 = createEntryWithBusinessKey();
		entry1.setBusinessKey(businessKey);
		modelService.save(entry1);
		thrown.expect(ModelSavingException.class);
		thrown.expectMessage(
				"org.springframework.dao.DuplicateKeyException: query; SQL []; "
						+ "Duplicate entry 'business_key_for_two_rows' for key 'junit_businessKeyIdx_44'");

		// When
		CartEntryModel entry2 = createEntryWithBusinessKey();
		entry2.setBusinessKey(businessKey);
		modelService.save(entry2);
	}

	@Test
	public void shouldNotChangeBusinessKeyForNotCancelledStatuses()
	{
		// Given
		String businessKey = "business_key";
		CartEntryModel entry = createEntryWithBusinessKey();
		entry.setBusinessKey(businessKey);
		modelService.save(entry);

		// When and Then
		List<OrderStatus> orderStatuses = enumerationService.getEnumerationValues(OrderStatus.class);
		for (OrderStatus status : orderStatuses)
		{
			if (status != OrderStatus.CANCELLED)
			{
				entry.setStatus(status);
				modelService.save(entry);
				modelService.refresh(entry);
				assertThat(entry.getBusinessKey()).isEqualTo(businessKey);
			}
		}
	}

	@Test
	public void shouldNullifyBusinessKeyForCancelledStatus()
	{
		// Given
		CartEntryModel entry = createEntryWithBusinessKey();
		entry.setBusinessKey("business_key");
		entry.setStatus(OrderStatus.CREATED);
		modelService.save(entry);

		// When
		entry.setStatus(OrderStatus.CANCELLED);
		modelService.save(entry);

		// Then
		modelService.refresh(entry);
		assertThat(entry.getBusinessKey()).isNull();
	}

	private CartEntryModel createEntryWithBusinessKey()
	{
		CartEntryModel entry = modelService.create(CartEntryModel.class);
		entry.setOrder(cart);
		entry.setProduct(product);
		entry.setUnit(unit);
		entry.setQuantity(1L);
		return entry;
	}


}
