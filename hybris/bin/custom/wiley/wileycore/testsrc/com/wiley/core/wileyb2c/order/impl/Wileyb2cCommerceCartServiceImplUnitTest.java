package com.wiley.core.wileyb2c.order.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.store.BaseStoreModel;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.store.WileyBaseStoreService;
import com.wiley.core.wileyb2c.order.dao.Wileyb2cCommerceCartDao;


/**
 * Default unit test for {@link Wileyb2cCommerceCartServiceImpl}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cCommerceCartServiceImplUnitTest
{

	@Mock
	private ModelService modelServiceMock;

	@Mock
	private CMSSiteService cmsSiteServiceMock;

	@Mock
	private WileyBaseStoreService wileyBaseStoreServiceMock;

	@Mock
	private Wileyb2cCommerceCartDao wileyb2cCommerceCartDaoMock;

	@Mock
	private ConfigurationService configurationServiceMock;

	@InjectMocks
	private Wileyb2cCommerceCartServiceImpl wileyb2cCommerceCartService;

	// Test Data

	@Mock
	private CartModel cartModelMock;

	@Mock
	private CMSSiteModel currentCmsSiteModelMock;

	@Mock
	private BaseStoreModel baseStoreModelMock;

	@Mock
	private UserModel userModelMock;

	@Mock
	private Configuration configurationMock;

	private static final String WILEY_HOME_PAGE_URL = "someTestWileyHomePageUrl";
	private static final String TEST_GUID = "guid";
	private static final String TEST_REDIRECT_URL = "redirectUrl";

	@Before
	public void setUp() throws Exception
	{
		ReflectionTestUtils.setField(wileyb2cCommerceCartService, "modelService", modelServiceMock);
		when(cmsSiteServiceMock.getCurrentSite()).thenReturn(currentCmsSiteModelMock);

		when(wileyBaseStoreServiceMock.getBaseStoreForBaseSite(eq(currentCmsSiteModelMock))).thenReturn(
				Optional.of(baseStoreModelMock));

		when(configurationServiceMock.getConfiguration()).thenReturn(configurationMock);
		when(configurationMock.getString(eq(WileyCoreConstants.WILEY_HOMEPAGE_URL), any())).thenReturn(WILEY_HOME_PAGE_URL);
	}

	@Test
	public void testSaveContinueUrlForCart()
	{
		// Given
		// no changes in test data

		// When
		wileyb2cCommerceCartService.saveContinueUrlForCart(TEST_REDIRECT_URL, cartModelMock);

		// Then
		verify(cartModelMock).setCartPageContinueUrl(eq(TEST_REDIRECT_URL));
		verify(modelServiceMock).save(same(cartModelMock));
	}

	@Test
	public void testSaveContinueUrlForCartWithNullParameters()
	{
		// Given
		// no changes in test data

		// When
		try
		{
			wileyb2cCommerceCartService.saveContinueUrlForCart(TEST_REDIRECT_URL, null);
			fail("Expected " + IllegalArgumentException.class);
		}
		catch (IllegalArgumentException e)
		{
			// Then
			// Success
		}
	}


	@Test
	public void testGetCartForGuidAndSiteAndUser()
	{
		// Given
		when(wileyb2cCommerceCartDaoMock.getCartForGuidAndStoreAndUser(eq(TEST_GUID), eq(baseStoreModelMock), eq(userModelMock)))
				.thenReturn(cartModelMock);

		// When
		final CartModel cartModel = wileyb2cCommerceCartService.getCartForGuidAndSiteAndUser(TEST_GUID, currentCmsSiteModelMock,
				userModelMock);

		// Then
		assertSame(cartModelMock, cartModel);
	}

	@Test
	public void testGetCartForGuidAndSiteAndUserWhenSiteHasNoBaseStore()
	{
		// Given
		when(wileyb2cCommerceCartDaoMock.getCartForGuidAndStoreAndUser(eq(TEST_GUID), eq(baseStoreModelMock), eq(userModelMock)))
				.thenReturn(cartModelMock);
		when(wileyBaseStoreServiceMock.getBaseStoreForBaseSite(eq(currentCmsSiteModelMock))).thenReturn(Optional.empty());

		// When
		final CartModel cartModel = wileyb2cCommerceCartService.getCartForGuidAndSiteAndUser(TEST_GUID, currentCmsSiteModelMock,
				userModelMock);

		// Then
		assertNull(cartModel);
	}

	@Test
	public void testGetCartForGuidAndSiteAndUserWhenCartIsNotFound()
	{
		// Given
		when(wileyb2cCommerceCartDaoMock.getCartForGuidAndStoreAndUser(eq(TEST_GUID), eq(baseStoreModelMock), eq(userModelMock)))
				.thenReturn(null);

		// When
		final CartModel cartModel = wileyb2cCommerceCartService.getCartForGuidAndSiteAndUser(TEST_GUID, currentCmsSiteModelMock,
				userModelMock);

		// Then
		assertNull(cartModel);
	}

	@Test
	public void testGetCartForGuidAndSite()
	{
		// Given
		when(wileyb2cCommerceCartDaoMock.getCartForGuidAndStore(eq(TEST_GUID), eq(baseStoreModelMock))).thenReturn(cartModelMock);

		// When
		final CartModel cartModel = wileyb2cCommerceCartService.getCartForGuidAndSite(TEST_GUID, currentCmsSiteModelMock);

		// Then
		assertSame(cartModelMock, cartModel);
	}

	@Test
	public void testGetCartForGuidAndSiteWhenSiteHasNoBaseStore()
	{
		// Given
		when(wileyb2cCommerceCartDaoMock.getCartForGuidAndStore(eq(TEST_GUID), eq(baseStoreModelMock))).thenReturn(cartModelMock);
		when(wileyBaseStoreServiceMock.getBaseStoreForBaseSite(eq(currentCmsSiteModelMock))).thenReturn(Optional.empty());

		// When
		final CartModel cartModel = wileyb2cCommerceCartService.getCartForGuidAndSite(TEST_GUID, currentCmsSiteModelMock);

		// Then
		assertNull(cartModel);
	}

	@Test
	public void testGetCartForGuidAndSiteWhenCartIsNotFound()
	{
		// Given
		when(wileyb2cCommerceCartDaoMock.getCartForGuidAndStore(eq(TEST_GUID), eq(baseStoreModelMock))).thenReturn(null);

		// When
		final CartModel cartModel = wileyb2cCommerceCartService.getCartForGuidAndSite(TEST_GUID, currentCmsSiteModelMock);

		// Then
		assertNull(cartModel);
	}

	@Test
	public void testGetCartsForSiteAndUser()
	{
		// Given
		final CartModel anotherCart = mock(CartModel.class);
		when(wileyb2cCommerceCartDaoMock.getCartsForStoreAndUser(eq(baseStoreModelMock), eq(userModelMock))).thenReturn(
				Arrays.asList(cartModelMock, anotherCart));

		// When
		final List<CartModel> carts = wileyb2cCommerceCartService.getCartsForSiteAndUser(currentCmsSiteModelMock,
				userModelMock);

		// Then
		assertNotNull(carts);
		assertEquals(2, carts.size());
		assertSame(carts.get(0), cartModelMock);
		assertSame(carts.get(1), anotherCart);
	}

	@Test
	public void testGetCartsForSiteAndUserWhenCartsAreNotFound()
	{
		// Given
		when(wileyb2cCommerceCartDaoMock.getCartsForStoreAndUser(eq(baseStoreModelMock), eq(userModelMock))).thenReturn(
				Collections.emptyList());

		// When
		final List<CartModel> carts = wileyb2cCommerceCartService.getCartsForSiteAndUser(currentCmsSiteModelMock,
				userModelMock);

		// Then
		assertNotNull(carts);
		assertTrue(carts.isEmpty());
	}

}