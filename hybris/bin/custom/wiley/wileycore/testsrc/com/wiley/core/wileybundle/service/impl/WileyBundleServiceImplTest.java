package com.wiley.core.wileybundle.service.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.site.BaseSiteService;
import static org.junit.Assert.assertTrue;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.i18n.WileyCountryService;
import com.wiley.core.wileybundle.service.WileyBundleService;


@IntegrationTest
public class WileyBundleServiceImplTest extends AbstractWileyServicelayerTransactionalTest
{
	private static final String DEFAULT_ENCODING = "UTF-8";
	private static final String PRODUCT_CODE = "WCOM_PRODUCT";
	private static final String CURRENT_SITE = "wileyb2c";
	private static final String SESSION_COUNTRY = "US";

	@Resource
	private WileyBundleService wileyBundleService;
	@Resource
	private ProductService productService;
	@Resource
	private SessionService sessionService;
	@Resource
	private BaseSiteService baseSiteService;
	@Resource
	private WileyCountryService wileyCountryService;

	@Before
	public void setUp() throws ImpExException
	{
		importCsv("/wileycore/test/product/WileyBundleServiceImplTest/testProducts.impex", DEFAULT_ENCODING);
		sessionService.setAttribute("currentSite", baseSiteService.getBaseSiteForUID(CURRENT_SITE));
		sessionService.setAttribute("sessionCountry", wileyCountryService.findCountryByCode(SESSION_COUNTRY));
	}

	@Test
	public void testDiscontinuedProductBundle()
	{
		final ProductModel productModel = productService.getProductForCode(PRODUCT_CODE);
		assertTrue(wileyBundleService.getBundlesForProduct(productModel).isEmpty());
	}


}
