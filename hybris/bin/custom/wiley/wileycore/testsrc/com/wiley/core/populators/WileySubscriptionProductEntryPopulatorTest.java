package com.wiley.core.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.enums.WileyProductSubtypeEnum;
import com.wiley.core.model.WileyProductSummaryModel;


/**
 * Author Herman_Chukhrai (EPAM)
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileySubscriptionProductEntryPopulatorTest
{
	private static final String PRODUCT_NAME = "PRODUCT_NAME";
	private static final String PICTURE_URL = "PICTURE_URL";

	@InjectMocks
	private Wileyb2cSubscriptionProductEntryPopulator populatorMock = new Wileyb2cSubscriptionProductEntryPopulator();
	@Mock
	private ProductModel productModelMock;
	@Mock
	private MediaModel mediaModel;
	@Mock
	private ModelService modelService;
	private AbstractOrderEntryModel orderEntryModel = new AbstractOrderEntryModel();

	@Test
	public void testPopulate()
	{
		//Given
		when(productModelMock.getSubtype()).thenReturn(WileyProductSubtypeEnum.SUBSCRIPTION);
		when(productModelMock.getName()).thenReturn(PRODUCT_NAME);
		when(productModelMock.getPicture()).thenReturn(mediaModel);
		when(mediaModel.getURL()).thenReturn(PICTURE_URL);
		when(modelService.create(WileyProductSummaryModel.class)).thenReturn(new WileyProductSummaryModel());

		//When
		populatorMock.populate(productModelMock, orderEntryModel);

		//Then
		final WileyProductSummaryModel productSummary = orderEntryModel.getProductSummary();
		assertNotNull(productSummary);
		assertEquals(PRODUCT_NAME, productSummary.getName());
		assertEquals("PICTURE_URL", productSummary.getPictureUrl());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEmptyProductProvided() throws Exception
	{
		//When
		populatorMock.populate(null, orderEntryModel);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEmptyEntryProvided() throws Exception
	{
		//When
		populatorMock.populate(productModelMock, null);
	}
}