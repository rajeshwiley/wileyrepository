package com.wiley.core.wileyb2c.search.solrfacetsearch.indexer.strategies;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.tenant.TenantService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.FlexibleSearchQuerySpec;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexOperation;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.config.IndexedTypeFlexibleSearchQuery;
import de.hybris.platform.solrfacetsearch.config.factories.FlexibleSearchQuerySpecFactory;
import de.hybris.platform.solrfacetsearch.indexer.IndexerContext;
import de.hybris.platform.solrfacetsearch.indexer.IndexerContextFactory;
import de.hybris.platform.solrfacetsearch.indexer.IndexerQueriesExecutor;
import de.hybris.platform.solrfacetsearch.indexer.exceptions.IndexerException;
import de.hybris.platform.solrfacetsearch.indexer.strategies.IndexOperationIdGenerator;
import de.hybris.platform.solrfacetsearch.indexer.workers.IndexerWorkerFactory;
import de.hybris.platform.solrfacetsearch.model.SolrIndexModel;
import de.hybris.platform.solrfacetsearch.solr.Index;
import de.hybris.platform.solrfacetsearch.solr.SolrIndexOperationService;
import de.hybris.platform.solrfacetsearch.solr.SolrIndexService;
import de.hybris.platform.solrfacetsearch.solr.SolrSearchProvider;
import de.hybris.platform.solrfacetsearch.solr.SolrSearchProviderFactory;
import de.hybris.platform.solrfacetsearch.solr.exceptions.SolrServiceException;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyCollection;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.servicelayer.i18n.WileycomI18NService;
import com.wiley.core.wileyb2c.search.solrfacetsearch.indexer.listeners.WileyIndexerOperationListener;
import com.wiley.core.wileyb2c.storesession.impl.Wileyb2cStoreSessionServiceImpl;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyIndexerStrategyUnitTest
{
	private static final String BASE_SITE_UID = "baseSiteUid";
	private static final String LANGUAGE_CODE = "en";
	private static final String COUNTRY_CODE = "US";
	private static final String QUALIFIER = "qualifier";
	private static final long OPERATION_ID = 555L;
	@InjectMocks
	private WileyIndexerStrategy wileyIndexerStrategy;
	@Mock
	private WileycomI18NService wileycomI18NService;
	@Mock
	private Wileyb2cStoreSessionServiceImpl wileyb2cStoreSessionService;
	@Mock
	private BaseSiteService baseSiteService;
	@Mock
	private BaseStoreService baseStoreService;
	@Mock
	private TenantService tenantService;
	@Mock
	private WileyIndexerOperationListener wileyIndexerOperationListener;
	@Mock
	private SolrIndexOperationService solrIndexOperationService;
	@Mock
	private FacetSearchConfig facetSearchConfig;
	@Mock
	private IndexerWorkerFactory indexerWorkerFactory;
	@Mock
	private IndexConfig indexConfig;
	@Mock
	private BaseSiteModel baseSite;
	@Mock
	private CountryModel countryModel;
	@Mock
	private SessionService sessionService;
	@Mock
	private BaseStoreModel baseStore;
	@Mock
	private LanguageModel languageModel;
	@Mock
	private IndexedType indexedType;
	@Mock
	private JaloSession jaloSession;
	@Mock
	private SolrIndexService solrIndexService;
	@Mock
	private SolrIndexModel solrIndexModel;
	@Mock
	private SolrSearchProviderFactory solrSearchProviderFactory;
	@Mock
	private SolrSearchProvider solrSearchProvider;
	@Mock
	private Index index;
	@Mock
	private IndexOperationIdGenerator indexOperationIdGenerator;
	@Mock
	private IndexerContextFactory indexerContextFactory;
	@Mock
	private IndexerContext indexerContext;
	@Mock
	private IndexerQueriesExecutor indexerQueriesExecutor;
	@Mock
	private Map<IndexOperation, IndexedTypeFlexibleSearchQuery> queriesMap;
	@Mock
	private IndexedTypeFlexibleSearchQuery query;
	@Mock
	private FlexibleSearchQuerySpecFactory flexibleSearchQuerySpecFactory;
	@Mock
	private FlexibleSearchQuerySpec indexQuery;
	@Mock
	private UserService userService;

	@Before
	public void setUp() throws Exception
	{
		when(sessionService.getRawSession(any())).thenReturn(jaloSession);
		when(solrIndexService.getActiveIndex(anyString(), anyString())).thenReturn(solrIndexModel);
		when(solrIndexModel.getQualifier()).thenReturn(QUALIFIER);
		when(solrSearchProviderFactory.getSearchProvider(facetSearchConfig, indexedType)).thenReturn(solrSearchProvider);
		when(solrSearchProvider.resolveIndex(facetSearchConfig, indexedType, QUALIFIER)).thenReturn(index);
		when(indexOperationIdGenerator.generate(facetSearchConfig, indexedType, index)).thenReturn(OPERATION_ID);
		when(indexerContextFactory.createContext(anyLong(), any(), anyBoolean(), any(), any(), anyCollection())).thenReturn(
				indexerContext);
		when(indexerQueriesExecutor.getPks(any(), any(), anyString(), anyMap())).thenReturn(Collections.emptyList());
		when(indexedType.getFlexibleSearchQueries()).thenReturn(queriesMap);
		when(queriesMap.get(any())).thenReturn(query);
		when(flexibleSearchQuerySpecFactory.createIndexQuery(query, indexedType, facetSearchConfig)).thenReturn(indexQuery);
		when(indexerContext.getPks()).thenReturn(Collections.emptyList());
	}

	@Test
	public void executeWhenFullOperationShouldIndexForAllCountries() throws IndexerException, SolrServiceException
	{
		wileyIndexerStrategy.setIndexOperation(IndexOperation.FULL);
		wileyIndexerStrategy.setIndexedType(indexedType);
		when(facetSearchConfig.getIndexConfig()).thenReturn(indexConfig);
		when(indexConfig.getBaseSite()).thenReturn(baseSite);
		when(baseSite.getUid()).thenReturn(BASE_SITE_UID);
		when(indexConfig.getCountries()).thenReturn(Collections.singletonList(countryModel));
		when(baseStoreService.getCurrentBaseStore()).thenReturn(baseStore);
		when(indexConfig.getLanguages()).thenReturn(Collections.singletonList(languageModel));
		when(languageModel.getIsocode()).thenReturn(LANGUAGE_CODE);
		when(countryModel.getIsocode()).thenReturn(COUNTRY_CODE);

		wileyIndexerStrategy.execute();

		verify(baseSiteService).setCurrentBaseSite(BASE_SITE_UID, true);
		verify(sessionService).setAttribute("currentStore", baseStore);
		verify(indexerContextFactory).initializeContext();
	}


}
