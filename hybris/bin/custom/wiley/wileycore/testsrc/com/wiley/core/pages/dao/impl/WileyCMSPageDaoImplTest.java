
package com.wiley.core.pages.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyPartnerPageModel;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyCMSPageDaoImplTest {

  private static final String PARTNER_CATEGORY_CODE = "WEL_CPA_PREFERRED_CATEGORY";
  private static final String CATALOG_VERSION_STATEMENT = "catalog version condition";
  private static final String FIND_PAGES_BY_CATEGORY_QUERY = "SELECT {pk} FROM {WileyPartnerPage} WHERE "
      + CATALOG_VERSION_STATEMENT + " AND {partnerCategoryCode} = ?partnerCategoryCode";

  @Spy
  @InjectMocks
  private final WileyCMSPageDaoImpl testedInstance = new WileyCMSPageDaoImpl();

  @Mock
  private FlexibleSearchService flexibleSearchService;
  @Mock
  private SearchResult<AbstractPageModel> searchResult;
  @Mock
  private AbstractPageModel partnerPage;
  @Mock
  private CatalogVersionModel catalogVersion;

  private final Collection<CatalogVersionModel> catalogVersions = Arrays.asList(catalogVersion);

  @Test
  public void testFindAllPagesByCategoryCode() throws Exception {
    when(flexibleSearchService.search(eq(FIND_PAGES_BY_CATEGORY_QUERY), anyMap()))
        .thenReturn(searchResult);
    doReturn(CATALOG_VERSION_STATEMENT).when(testedInstance)
        .buildCatalogVersionsStatement(catalogVersions, new HashMap<>());
    when(searchResult.getResult()).thenReturn(Arrays.asList(partnerPage));

    final Collection<AbstractPageModel> results = testedInstance.findAllPagesByCategoryCode(
        WileyPartnerPageModel._TYPECODE, catalogVersions, PARTNER_CATEGORY_CODE);
    assertEquals(Arrays.asList(partnerPage), results);
  }

}
