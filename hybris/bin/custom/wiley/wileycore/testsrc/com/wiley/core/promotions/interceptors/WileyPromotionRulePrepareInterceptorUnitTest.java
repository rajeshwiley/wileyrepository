package com.wiley.core.promotions.interceptors;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.promotionengineservices.model.PromotionSourceRuleModel;
import de.hybris.platform.promotionengineservices.model.RuleBasedPromotionModel;
import de.hybris.platform.ruleengine.enums.RuleType;
import de.hybris.platform.ruleengine.model.AbstractRuleEngineRuleModel;
import de.hybris.platform.ruleengineservices.compiler.impl.DefaultRuleCompilerContext;
import de.hybris.platform.ruleengineservices.rule.dao.RuleDao;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.AdditionalMatchers.or;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyPromotionRulePrepareInterceptorUnitTest
{
	private static final String EN_NAME = "enName";
	private static final String CA_NAME = "caName";
	private static final String EN_DESCRIPTION = "enDescription";
	private static final String CA_DESCRIPTION = "caDescription";
	private static final String RULE_CODE = "aRuleCode";

	@InjectMocks
	private WileyPromotionRulePrepareInterceptor prepareInterceptor;
	@Mock
	private ModelService modelServiceMock;
	@Mock
	private CommonI18NService commonI18NServiceMock;
	@Mock
	private InterceptorContext contextMock;
	@Mock
	private DefaultRuleCompilerContext defaultRuleCompilerContextMock;
	@Mock
	private AbstractRuleEngineRuleModel ruleModel;
	@Mock
	private RuleBasedPromotionModel ruleBasedPromotionMock;
	@Mock
	private RuleDao ruleDaoMock;
	private PromotionSourceRuleModel sourceRuleModel;

	@Before
	public void setUp()
	{
		ruleModel = new AbstractRuleEngineRuleModel();
		ruleModel.setRuleType(RuleType.PROMOTION);
		ruleModel.setCode(RULE_CODE);
		Mockito.when(modelServiceMock.create(RuleBasedPromotionModel.class)).thenReturn(ruleBasedPromotionMock);
		Mockito.when(commonI18NServiceMock.getAllLanguages()).thenReturn(Collections.emptyList());

		sourceRuleModel = new PromotionSourceRuleModel();
		final LanguageModel language1 = new LanguageModel();
		final LanguageModel language2 = new LanguageModel();
		final List<LanguageModel> languageModelList = new ArrayList<>();
		languageModelList.add(language1);
		languageModelList.add(language2);

		Mockito.when(defaultRuleCompilerContextMock.getRule()).thenReturn(sourceRuleModel);
		Mockito.when(ruleDaoMock.findRuleByCode(RULE_CODE)).thenReturn(sourceRuleModel);
		Mockito.when(commonI18NServiceMock.getAllLanguages()).thenReturn(languageModelList);
		Mockito.when(commonI18NServiceMock.getLocaleForLanguage(language1)).thenReturn(Locale.ENGLISH);
		Mockito.when(commonI18NServiceMock.getLocaleForLanguage(language2)).thenReturn(Locale.CANADA);
	}

	/**
	 * OOTB test
	 */
	@Test
	public void testOnPrepareNotPromotionRuleType() throws InterceptorException
	{
		prepareInterceptor = Mockito.spy(prepareInterceptor);
		ruleModel.setRuleType(RuleType.DEFAULT);

		prepareInterceptor.onPrepare(ruleModel, contextMock);

		verify(prepareInterceptor, Mockito.never())
				.doOnPrepare(Mockito.eq(ruleModel), Mockito.eq(contextMock));
	}

	@Test
	public void testOnPreparePromotionNotSet() throws InterceptorException
	{
		ruleModel.setVersion(0L);

		prepareInterceptor.onPrepare(ruleModel, contextMock);

		Assertions.assertThat(ruleModel.getPromotion()).isNotNull();
		Assertions.assertThat(ruleModel.getPromotion().getRuleVersion()).isEqualTo(0L);
	}

	@Test
	public void testLocalizedProductBanner() throws InterceptorException
	{
		final MediaContainerModel enBanner = new MediaContainerModel();
		final MediaContainerModel caBanner = new MediaContainerModel();

		sourceRuleModel.setProductBanner(enBanner, Locale.ENGLISH);
		sourceRuleModel.setProductBanner(caBanner, Locale.CANADA);

		prepareInterceptor.onPrepare(ruleModel, contextMock);

		verify(ruleBasedPromotionMock).setProperty(RuleBasedPromotionModel.PRODUCTBANNER,
				Locale.ENGLISH, enBanner);
		verify(ruleBasedPromotionMock).setProperty(RuleBasedPromotionModel.PRODUCTBANNER,
				Locale.CANADA, caBanner);
		//verify other locales were not changed
		verify(ruleBasedPromotionMock, never()).setProperty(eq(RuleBasedPromotionModel.PRODUCTBANNER),
				not(or(eq(Locale.ENGLISH), eq(Locale.CANADA))), anyString());
	}

	@Test
	public void testLocalizedDescription() throws InterceptorException
	{
		sourceRuleModel.setLongDescription(EN_DESCRIPTION, Locale.ENGLISH);
		sourceRuleModel.setLongDescription(CA_DESCRIPTION, Locale.CANADA);

		prepareInterceptor.onPrepare(ruleModel, contextMock);

		verify(ruleBasedPromotionMock).setProperty(RuleBasedPromotionModel.PROMOTIONDESCRIPTION,
				Locale.ENGLISH, EN_DESCRIPTION);
		verify(ruleBasedPromotionMock).setProperty(RuleBasedPromotionModel.PROMOTIONDESCRIPTION,
				Locale.CANADA, CA_DESCRIPTION);
		//verify other locales were not changed
		verify(ruleBasedPromotionMock, never()).setProperty(eq(RuleBasedPromotionModel.DESCRIPTION),
				not(or(eq(Locale.ENGLISH), eq(Locale.CANADA))), anyString());

	}

	@Test
	public void testLocalizedName() throws InterceptorException
	{
		sourceRuleModel.setName(EN_NAME, Locale.ENGLISH);
		sourceRuleModel.setName(CA_NAME, Locale.CANADA);

		prepareInterceptor.onPrepare(ruleModel, contextMock);

		verify(ruleBasedPromotionMock).setProperty(RuleBasedPromotionModel.NAME, Locale.ENGLISH, EN_NAME);
		verify(ruleBasedPromotionMock).setProperty(RuleBasedPromotionModel.NAME, Locale.CANADA, CA_NAME);
		//verify other locales were not changed
		verify(ruleBasedPromotionMock, never()).setProperty(eq(RuleBasedPromotionModel.NAME),
				not(or(eq(Locale.ENGLISH), eq(Locale.CANADA))), anyString());
	}
}