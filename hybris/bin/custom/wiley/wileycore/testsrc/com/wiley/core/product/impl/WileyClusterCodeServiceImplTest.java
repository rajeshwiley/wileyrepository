package com.wiley.core.product.impl;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.model.ClusterCodeModel;
import com.wiley.core.product.WileyClusterCodeService;
import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Resource;

import java.util.Optional;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

/**
 *Integration test for {@link WileyClusterCodeServiceImpl}
 */
@IntegrationTest
public class WileyClusterCodeServiceImplTest extends AbstractWileyServicelayerTransactionalTest
{
    private static final String TEST_PRODUCT_1 = "TEST_PRODUCT_1";
    private static final String TEST_PRODUCT_2 = "TEST_PRODUCT_2";
    private static final String USD = "USD";
    private static final String EUR = "EUR";
    private static final String GBP = "GBP";

    @Resource
    private WileyClusterCodeService clusterCodeService;
    
    @Resource
    private ProductService productService;
    @Resource
    private CommonI18NService commonI18NService;
    
    @Before
    public void setUp() throws Exception
    {
        importCsv("/wileycore/test/product/clusterCodeCurrencyProductRelation.impex", DEFAULT_ENCODING);
    }
    
    @Test
    public void getClusterCodeForCurrencyPositiveCase() throws Exception
    {
        // Given
        final ProductModel product = productService.getProductForCode(TEST_PRODUCT_1);
        CurrencyModel existingCurrency1 = commonI18NService.getCurrency(EUR);
        CurrencyModel existingCurrency2 = commonI18NService.getCurrency(USD);

        // Testing
        final Optional<ClusterCodeModel> clusterCode1 = clusterCodeService.getClusterCodeForCurrency(product, existingCurrency1);
        final Optional<ClusterCodeModel> clusterCode2 = clusterCodeService.getClusterCodeForCurrency(product, existingCurrency2);

        // Verify
        assertTrue(clusterCode1.isPresent() && clusterCode1.get().getClusterCode().equals("clusterCode1"));
        assertTrue(clusterCode2.isPresent() && clusterCode2.get().getClusterCode().equals("clusterCode2"));
    }
    
    @Test
    public void getClusterCodeForCurrencyNegativeCase() throws Exception
    {
        // Given
        final ProductModel product = productService.getProductForCode(TEST_PRODUCT_1);
        CurrencyModel notExistingCurrency = commonI18NService.getCurrency(GBP);

        // Testing
        final Optional<ClusterCodeModel> clusterCode
                = clusterCodeService.getClusterCodeForCurrency(product, notExistingCurrency);

        // Verify
        assertFalse(clusterCode.isPresent());
    }
    
    @Test
    public void assertionForNullProduct() throws Exception
    {
        // Given
        Optional<ClusterCodeModel> clusterCode;
        Exception catchedException = null;

        // Testing
        try {
            clusterCode = clusterCodeService.getClusterCodeForCurrency(null, null);
        } catch (Exception e) {
            catchedException = e;
        }

        // Verify
        assertTrue(catchedException instanceof IllegalArgumentException);
    }
    
    @Test
    public void assertionForNullCurrency() throws Exception
    {
        // Given
        final ProductModel product = productService.getProductForCode(TEST_PRODUCT_1);
        Optional<ClusterCodeModel> clusterCode;
        Exception catchedException = null;

        // Testing
        try {
            clusterCode = clusterCodeService.getClusterCodeForCurrency(product, null);
        } catch (Exception e) {
            catchedException = e;
        }

        // Verify
        assertTrue(catchedException instanceof IllegalArgumentException);
    }
    
    @Test
    public void clusterCodeShouldNotBeDefinedForConfiguredCurrencies()
    {
        // Given
        //Assuming USD is configured not to require cluster code (see WileyClusterCodeService bean definition) 
        CurrencyModel usd = commonI18NService.getCurrency(USD);

        // Testing
        final boolean result = clusterCodeService.shouldClusterCodeBeDefinedForThisCurrency(usd);

        // Verify
        assertFalse(result);
    }
    
    @Test
    public void clusterCodeShouldBeDefinedForCurrency()
    {
        // Given
        //Assuming EUR is configured to require cluster code (see WileyClusterCodeService bean definition) 
        CurrencyModel eur = commonI18NService.getCurrency(EUR);

        // Testing
        final boolean result = clusterCodeService.shouldClusterCodeBeDefinedForThisCurrency(eur);

        // Verify
        assertTrue(result);
    }
    
    @Test
    public void clusterCodeIsMissingIfItIsNotConfigured()
    {
    	// Given
        final ProductModel product = productService.getProductForCode(TEST_PRODUCT_2);
        CurrencyModel eur = commonI18NService.getCurrency(EUR);
    
    	// Testing
        final boolean result = clusterCodeService.isClusterCodeMissingForCurrency(product, eur);
    
    	// Verify
        assertTrue(result);
    }
    
    @Test
    public void clusterCodeIsNotMissingIfTheCurrencyDoesNotRequireIt()
    {
    	// Given
        final ProductModel product = productService.getProductForCode(TEST_PRODUCT_2);
        //Assuming USD is configured not to require cluster code (see WileyClusterCodeService bean definition) 
        CurrencyModel usd = commonI18NService.getCurrency(USD);
    
    	// Testing
        final boolean result = clusterCodeService.isClusterCodeMissingForCurrency(product, usd);
    
    	// Verify
        assertFalse(result);
    }
}