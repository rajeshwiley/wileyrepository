package com.wiley.core.wileyb2c.search.solrfacetsearch.provider.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.provider.Qualifier;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collection;
import java.util.Collections;
import java.util.Locale;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cLanguageQualifierProviderUnitTest
{
	private static final String LANG_CODE = "en";
	private static final String COUNTRY_CODE = "US";
	public static final Locale LOCALE = Locale.CANADA;
	@InjectMocks
	private Wileyb2cLanguageQualifierProvider wileyb2cLanguageQualifierProvider;
	@Mock
	private CommonI18NService commonI18NService;
	@Mock
	private LanguageModel languageModel;
	@Mock
	private CountryModel countryModel;
	@Mock
	private FacetSearchConfig facetSearchConfig;
	@Mock
	private IndexedType indexedType;
	@Mock
	private IndexConfig indexConfig;
	@Mock
	private LanguageModel indexLanguage;

	@Test
	public void getAvailableQualifiersWhenNoLanguagesShouldReturnEmptyList()
	{
		when(facetSearchConfig.getIndexConfig()).thenReturn(indexConfig);
		when(indexConfig.getLanguages()).thenReturn(Collections.emptyList());

		final Collection<Qualifier> availableQualifiers =
				wileyb2cLanguageQualifierProvider.getAvailableQualifiers(facetSearchConfig, indexedType);

		Assert.assertEquals(availableQualifiers.size(), 0);
	}

	@Test
	public void getAvailableQualifiersWhenNoCountriesShouldReturnEmptyList()
	{
		when(facetSearchConfig.getIndexConfig()).thenReturn(indexConfig);
		when(indexConfig.getLanguages()).thenReturn(Collections.singletonList(languageModel));
		when(indexConfig.getCountries()).thenReturn(Collections.emptyList());

		final Collection<Qualifier> availableQualifiers =
				wileyb2cLanguageQualifierProvider.getAvailableQualifiers(facetSearchConfig, indexedType);

		Assert.assertEquals(availableQualifiers.size(), 0);
	}

	@Test
	public void getAvailableQualifiers()
	{
		when(facetSearchConfig.getIndexConfig()).thenReturn(indexConfig);
		when(indexConfig.getLanguages()).thenReturn(Collections.singletonList(languageModel));
		when(indexConfig.getCountries()).thenReturn(Collections.singletonList(countryModel));
		when(languageModel.getIsocode()).thenReturn(LANG_CODE);
		when(countryModel.getIsocode()).thenReturn(COUNTRY_CODE);
		when(commonI18NService.getLanguage(LANG_CODE + Wileyb2cLanguageQualifierProvider.DELIMITER + COUNTRY_CODE)).thenReturn(
				indexLanguage);
		when(commonI18NService.getLocaleForLanguage(indexLanguage)).thenReturn(LOCALE);

		final Collection<Qualifier> availableQualifiers =
				wileyb2cLanguageQualifierProvider.getAvailableQualifiers(facetSearchConfig, indexedType);

		Assert.assertEquals(availableQualifiers.size(), 1);
		final Qualifier qualifier = availableQualifiers.iterator().next();
		wileyb2cLanguageQualifierProvider.applyQualifier(qualifier);
		verify(commonI18NService).setCurrentLanguage(indexLanguage);
	}
}
