package com.wiley.core.pin.dao.impl;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.model.PinModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.site.BaseSiteService;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Resource;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;


/**
 * Test class for {@code DefaultPinDao}.
 */
public class DefaultPinDaoTest extends AbstractWileyServicelayerTransactionalTest
{
	private static final String TEST_PIN_1 = "testpin-1";
	private static final String TEST_PIN_2 = "testpin-2";
	private static final String TEST_UNKNOWN_PIN = "testpin-unknown";

	@Resource
	private DefaultPinDao defaultPinDao;
	@Resource
	private ModelService modelService;
	@Resource
	private BaseSiteService baseSiteService;

	@Before
	public void setUp() throws Exception
	{
		importCsv("/wileycore/test/sampledata/wel/pins.impex", "utf-8");
	}

	@Test
	public void testFindPinByCode()
	{
		final PinModel pin = defaultPinDao.findPinByCode(TEST_PIN_1);
		assertNotNull("Pin is not null", pin);
		assertTrue("Pin is activated", pin.getActivated());
	}

	@Test
	public void testFindPinByCodeForANotActivatedPin()
	{
		final PinModel pin = defaultPinDao.findPinByCode(TEST_PIN_2);
		assertNotNull("Pin is not null", pin);
		assertTrue("Pin is not activated", !pin.getActivated());
	}

	@Test
	public void testFindPinByCodeForANonExistentPin()
	{
		final PinModel pin = defaultPinDao.findPinByCode(TEST_UNKNOWN_PIN);
		assertNull("Pin is null", pin);
	}
}
