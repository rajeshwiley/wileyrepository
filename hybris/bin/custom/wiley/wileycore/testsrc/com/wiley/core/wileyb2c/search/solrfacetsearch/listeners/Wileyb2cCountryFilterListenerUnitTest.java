package com.wiley.core.wileyb2c.search.solrfacetsearch.listeners;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SearchQueryPageableData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.search.FacetSearchException;
import de.hybris.platform.solrfacetsearch.search.QueryField;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.solrfacetsearch.search.context.FacetSearchContext;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.servicelayer.i18n.WileycomI18NService;

import static com.wiley.core.wileyb2c.search.solrfacetsearch.listeners.Wileyb2cCountryFilterListener.NOT_QUALIFIER;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cCountryFilterListenerUnitTest
{
	private static final String COUNTRIES_PROPERTY = "COUNTRIES";
	private static final String FIELD_NAME = "fieldName";
	private static final String ISO_CODE = "isoCode";
	@InjectMocks
	private Wileyb2cCountryFilterListener wileyb2cCountryFilterPopulator;
	@Mock
	private FieldNameProvider fieldNameProvider;
	@Mock
	private WileycomI18NService wileycomCountryI18NService;
	@Mock
	private SearchQueryPageableData<SolrSearchQueryData> source;
	@Mock
	private FacetSearchContext target;
	@Mock
	private SearchQuery searchQuery;
	@Mock
	private IndexedType indexedType;
	@Mock
	private IndexedProperty indexedProperty;
	@Mock
	private CountryModel currentCountry;


	@Before
	public void setUp() throws Exception
	{
		wileyb2cCountryFilterPopulator.setCountriesProperty(COUNTRIES_PROPERTY);
	}

	@Test
	public void populate() throws FacetSearchException
	{
		when(target.getSearchQuery()).thenReturn(searchQuery);
		when(searchQuery.getIndexedType()).thenReturn(indexedType);
		when(indexedType.getIndexedProperties()).thenReturn(Collections.singletonMap(COUNTRIES_PROPERTY, indexedProperty));
		when(fieldNameProvider.getFieldName(indexedProperty, null, FieldNameProvider.FieldType.INDEX)).thenReturn(FIELD_NAME);
		when(wileycomCountryI18NService.getCurrentCountry()).thenReturn(Optional.of(currentCountry));
		when(currentCountry.getIsocode()).thenReturn(ISO_CODE);

		wileyb2cCountryFilterPopulator.beforeSearch(target);

		final ArgumentCaptor<QueryField> argumentCaptor =
				ArgumentCaptor.forClass(QueryField.class);
		verify(searchQuery).addFilterQuery(argumentCaptor.capture());
		final QueryField queryField = argumentCaptor.getValue();
		assertEquals(queryField.getField(), NOT_QUALIFIER + FIELD_NAME);
		assertEquals(queryField.getValues(), Collections.singleton(ISO_CODE));
		assertEquals(queryField.getQueryOperator(), SearchQuery.QueryOperator.CONTAINS);
	}
}
