package com.wiley.core.wileyb2c.restriction.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.enums.WileyProductLifecycleEnum;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cAvailabilityByStatusStrategyTest
{
	private Wileyb2cAvailabilityByStatusStrategy testInstance = new Wileyb2cAvailabilityByStatusStrategy();

	@Test
	public void shoudlRestrictIfProductLifecycleStatusIsDiscontinued() {
		//Given
		ProductModel productWithDiscontinuedStatus = givenProduct(WileyProductLifecycleEnum.DISCONTINUED);
		//When
		boolean result = testInstance.isRestricted(productWithDiscontinuedStatus);
		//Then
		assertTrue(result);
	}

	@Test
	public void shoudlRestrictIfProductLifecycleStatusIsPreview() {
		//Given
		ProductModel productWithDiscontinuedStatus = givenProduct(WileyProductLifecycleEnum.PREVIEW);
		//When
		boolean result = testInstance.isRestricted(productWithDiscontinuedStatus);
		//Then
		assertTrue(result);
	}

	@Test
	public void shouldNotRestrictOtherThanPreviewOrDiscontinuedStatuses() {

		List<WileyProductLifecycleEnum> possibleStatuses = new ArrayList<>();
		possibleStatuses.addAll(Arrays.asList(WileyProductLifecycleEnum.values()));
		possibleStatuses.remove(WileyProductLifecycleEnum.PREVIEW);
		possibleStatuses.remove(WileyProductLifecycleEnum.DISCONTINUED);

		for (WileyProductLifecycleEnum status : possibleStatuses) {
			//Given
			ProductModel productWithDiscontinuedStatus = givenProduct(status);
			//When
			boolean result = testInstance.isRestricted(productWithDiscontinuedStatus);
			//Then
			assertFalse(result);
		}
	}

	private ProductModel givenProduct(final WileyProductLifecycleEnum status) {
		ProductModel product = mock(ProductModel.class);
		when(product.getLifecycleStatus()).thenReturn(status);
		return product;
	}

}