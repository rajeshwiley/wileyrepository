package com.wiley.core.wileyb2c.search.solrfacetsearch.indexer.worker;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfigService;
import de.hybris.platform.solrfacetsearch.indexer.strategies.IndexerBatchStrategyFactory;
import de.hybris.platform.solrfacetsearch.indexer.workers.IndexerWorkerParameters;
import de.hybris.platform.solrfacetsearch.solr.SolrSearchProviderFactory;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.i18n.WileyCountryService;
import com.wiley.core.servicelayer.i18n.WileycomI18NService;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyIndexerWorkerUnitTest
{
	private static final String COUNTRY_CODE = "en_US";
	@Spy
	@InjectMocks
	private WileyIndexerWorker wileyIndexerWorker;
	@Mock
	private WileycomI18NService wileycomI18NService;
	@Mock
	private WileyCountryService wileyCountryService;
	@Mock
	private I18NService i18NService;
	@Mock
	private SessionService sessionService;
	@Mock
	private UserService userService;
	@Mock
	private CommonI18NService commonI18NService;
	@Mock
	private FacetSearchConfigService facetSearchConfigService;
	@Mock
	private IndexerBatchStrategyFactory indexerBatchStrategyFactory;
	@Mock
	private SolrSearchProviderFactory solrSearchProviderFactory;
	@Mock
	private IndexerWorkerParameters workerParameters;
	@Mock
	private CountryModel countryModel;
	@Mock
	private BaseSiteService baseSiteService;
	@Mock
	private BaseSiteModel baseSiteModel;

	@Before
	public void setUp() throws Exception
	{
		when(workerParameters.getTenant()).thenReturn("junit");
		doNothing().when(wileyIndexerWorker).superInitializeSession();
		doNothing().when(wileyIndexerWorker).superInitialize(any());
		when(baseSiteService.getCurrentBaseSite()).thenReturn(baseSiteModel);
	}

	@Test
	public void initializeSession()
	{
		when(wileycomI18NService.getCurrentCountry()).thenReturn(Optional.of(countryModel));
		when(countryModel.getIsocode()).thenReturn(COUNTRY_CODE);
		wileyIndexerWorker.initialize(workerParameters);
		when(wileyCountryService.findCountryByCode(COUNTRY_CODE)).thenReturn(countryModel);

		wileyIndexerWorker.initializeSession();

		verify(wileycomI18NService).setCurrentCountry(countryModel);
		verify(i18NService).setLocalizationFallbackEnabled(true);
		verify(baseSiteService).setCurrentBaseSite(baseSiteModel, true);
	}

	@Test
	public void initialize()
	{
		when(wileycomI18NService.getCurrentCountry()).thenReturn(Optional.of(countryModel));
		when(countryModel.getIsocode()).thenReturn(COUNTRY_CODE);

		wileyIndexerWorker.initialize(workerParameters);

		wileyIndexerWorker.initializeSession();
		verify(wileyCountryService).findCountryByCode(COUNTRY_CODE);
		verify(baseSiteService).setCurrentBaseSite(baseSiteModel, true);
	}

}
