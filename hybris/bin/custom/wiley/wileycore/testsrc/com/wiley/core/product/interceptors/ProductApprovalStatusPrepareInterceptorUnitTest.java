package com.wiley.core.product.interceptors;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.user.UserService;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Unit test for {@link ProductApprovalStatusPrepareInterceptor}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ProductApprovalStatusPrepareInterceptorUnitTest
{
	public static final String PRODUCTEDITORGROUP = "producteditorgroup";

	@Mock
	private ProductModel productMock;
	@Mock
	private UserService userService;
	@Mock
	private UserModel userMock;
	@Mock
	private UserGroupModel productEditorGroupMock;
	@Mock
	private InterceptorContext interceptorContextMock;

	@InjectMocks
	private ProductApprovalStatusPrepareInterceptor testInstance;

	@Before
	public void setUp() throws Exception
	{
		when(userService.getCurrentUser()).thenReturn(userMock);
		when(userService.getUserGroupForUID(PRODUCTEDITORGROUP)).thenReturn(productEditorGroupMock);
	}

	@Test
	public void shouldNotPrepareForOtherUsers() throws Exception
	{
		//given
		when(userService.isMemberOfGroup(userMock, productEditorGroupMock)).thenReturn(Boolean.FALSE);
		//when
		testInstance.onPrepare(productMock, null);
		//then
		verify(productMock, never()).setApprovalStatus(any());
	}

	@Test
	public void shouldNotPrepareWhenProductEditorGroupNotFound() throws Exception
	{
		//given
		when(userService.getUserGroupForUID(PRODUCTEDITORGROUP)).thenReturn(null);
		//when
		testInstance.onPrepare(productMock, null);
		//then
		verify(productMock, never()).setApprovalStatus(any());
	}

	@Test
	public void shouldPrepareForProductEditors() throws Exception
	{
		//given
		when(userService.isMemberOfGroup(userMock, productEditorGroupMock)).thenReturn(Boolean.TRUE);
		//when
		testInstance.onPrepare(productMock, null);
		//then
		verify(productMock, times(1)).setApprovalStatus(ArticleApprovalStatus.CHECK);
	}

	@Test
	public void shouldNotPrepareForProductEditorsIfNotModified() throws Exception
	{
		//given
		when(userService.isMemberOfGroup(userMock, productEditorGroupMock)).thenReturn(Boolean.TRUE);
		when(interceptorContextMock.isModified(productMock)).thenReturn(Boolean.FALSE);
		//when
		testInstance.onPrepare(productMock, interceptorContextMock);
		//then
		verify(productMock, never()).setApprovalStatus(any());
	}
}
