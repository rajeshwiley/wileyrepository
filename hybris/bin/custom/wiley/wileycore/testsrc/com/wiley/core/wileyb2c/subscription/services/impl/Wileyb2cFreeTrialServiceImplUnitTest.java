package com.wiley.core.wileyb2c.subscription.services.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.subscriptionservices.model.SubscriptionTermModel;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.enums.WileyProductSubtypeEnum;
import com.wiley.core.model.WileyProductModel;
import com.wiley.core.wileyb2c.subscription.services.Wileyb2cFreeTrialSubscriptionTermCheckingStrategy;
import com.wiley.core.wileyb2c.subscription.services.Wileyb2cSubscriptionService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cFreeTrialServiceImplUnitTest
{

	//Test instance mocks
	@Mock
	private Wileyb2cFreeTrialSubscriptionTermCheckingStrategy freeTrialSubscriptionTermCheckingStrategy;
	@Mock
	private Wileyb2cSubscriptionService subscriptionService;
	@InjectMocks
	private Wileyb2cFreeTrialServiceImpl testInstance = new Wileyb2cFreeTrialServiceImpl();
	//Mocks for testing
	@Mock
	private SubscriptionTermModel freeTrialSubscriptionTermMock;
	@Mock
	private AbstractOrderEntryModel freeTrialOrderEntryMock;
	@Mock
	private WileyProductModel freeTrialProductMock;

	@Mock
	private WileyProductModel regularSubscriptionProductMock;
	@Mock
	private SubscriptionTermModel regularSubscriptionTermMock;
	@Mock
	private AbstractOrderEntryModel regularSubscriptionOrderEntryMock;


	@Test
	public void isFreeTrialEntryShouldReturnFalseIfProductIsNotFreeTrial()
	{
		//When
		boolean result = testInstance.isFreeTrialEntry(regularSubscriptionOrderEntryMock);
		//Then
		assertFalse("If product is not Free Trial then entry is NOT free trial", result);
	}

	@Test
	public void isFreeTrialEntryShouldReturnTrueIfProductIsFreeTrialAndEntrysSubscriptionTermFreeTrial()
	{
		//When
		boolean result = testInstance.isFreeTrialEntry(freeTrialOrderEntryMock);
		//Then
		assertTrue("If Product is Free Trial and OrderEntry's SubscriptionTerm free trial then OrderEntry is Free Trial", result);
	}

	@Before
	public void setupFreeTrialEntry()
	{
		Set<SubscriptionTermModel> set = new HashSet<>();
		set.add(freeTrialSubscriptionTermMock);
		when(freeTrialSubscriptionTermCheckingStrategy.isFreeTrial(freeTrialSubscriptionTermMock)).thenReturn(true);

		when(freeTrialProductMock.getSubscriptionTerms()).thenReturn(set);
		when(freeTrialProductMock.getSubtype()).thenReturn(WileyProductSubtypeEnum.SUBSCRIPTION);
		when(subscriptionService.isProductSubscription(freeTrialProductMock)).thenReturn(true);

		when(freeTrialOrderEntryMock.getSubscriptionTerm()).thenReturn(freeTrialSubscriptionTermMock);
		when(freeTrialOrderEntryMock.getProduct()).thenReturn(freeTrialProductMock);
	}

	@Before
	public void setupRegularSubscriptionEntry()
	{
		Set<SubscriptionTermModel> set = new HashSet<>();
		set.add(freeTrialSubscriptionTermMock);
		when(freeTrialSubscriptionTermCheckingStrategy.isFreeTrial(regularSubscriptionTermMock)).thenReturn(false);

		when(regularSubscriptionProductMock.getSubscriptionTerms()).thenReturn(set);
		when(regularSubscriptionProductMock.getSubtype()).thenReturn(WileyProductSubtypeEnum.SUBSCRIPTION);
		when(subscriptionService.isProductSubscription(regularSubscriptionProductMock)).thenReturn(true);

		when(regularSubscriptionOrderEntryMock.getSubscriptionTerm()).thenReturn(regularSubscriptionTermMock);
		when(regularSubscriptionOrderEntryMock.getProduct()).thenReturn(regularSubscriptionProductMock);
	}

}