package com.wiley.core.wileyb2c.product;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.catalog.references.ProductReferenceService;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.subscriptionservices.model.SubscriptionTermModel;
import de.hybris.platform.util.PriceValue;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyProductModel;
import com.wiley.core.wileyb2c.subscription.services.Wileyb2cSubscriptionService;

import static java.util.Collections.singletonList;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;


/**
 * Author Herman_Chukhrai (EPAM)
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cCommercePriceServiceImplTest
{
	public static final String CORRENCY_ISO = "corrency iso";
	public static final boolean NET_PRICE = true;
	public static final double PRICE_10 = 10.0;
	public static final double PRICE_20 = 20.0;

	@Spy
	@InjectMocks
	private Wileyb2cCommercePriceServiceImpl wileyb2cCommercePriceService;

	// Test data
	@Mock
	private CommerceCartParameter commerceCartParameterMock;
	@Mock
	private WileyProductModel wileyProductMock;
	@Mock
	private SubscriptionTermModel subscriptionTermMock;
	@Mock
	private Wileyb2cPriceServiceImpl wileyb2cPriceService;
	@Mock
	private Wileyb2cSubscriptionService wileyb2cSubscriptionService;
	@Mock
	private ProductReferenceService productReferenceServiceMock;

	@Test
	public void testValidProductSubscriptionTermPrice() throws Exception
	{
		// Given product and subscriptionTerm
		final PriceInformation priceRow = Mockito.mock(PriceInformation.class);
		Set<SubscriptionTermModel> set = new HashSet<>();
		set.add(subscriptionTermMock);
		when(wileyProductMock.getSubscriptionTerms()).thenReturn(set);
		subscriptionTermIsValidForProduct(true);
		when(subscriptionTermMock.getId()).thenReturn("TERM_ID");
		when(wileyb2cPriceService.findPricesByProductAndSubscriptionTerm(wileyProductMock, subscriptionTermMock))
				.thenReturn(singletonList(priceRow));

		// When
		final boolean isValid = wileyb2cCommercePriceService
				.validateProductSubscriptionTermPrice(wileyProductMock, subscriptionTermMock);
		// Then
		assertTrue(isValid);
	}

	private void subscriptionTermIsValidForProduct(boolean isValid)
	{
		when(wileyb2cSubscriptionService.isValidSubscriptionTermForProduct(eq(wileyProductMock), any()))
				.thenReturn(isValid);
	}

	@Test
	public void testMissedSubscriptionTermsForProducts() throws Exception
	{
		// Given
		// When
		final boolean isValid = wileyb2cCommercePriceService
				.validateProductSubscriptionTermPrice(wileyProductMock, subscriptionTermMock);
		// Then
		assertFalse(isValid);
	}

	@Test
	public void testNoPriceForSubscriptionTermForProducts() throws Exception
	{
		// Given
		Set<SubscriptionTermModel> set = new HashSet<>();
		set.add(subscriptionTermMock);
		when(wileyProductMock.getSubscriptionTerms()).thenReturn(set);
		subscriptionTermIsValidForProduct(true);
		when(subscriptionTermMock.getId()).thenReturn("TERM_ID");
		when(wileyb2cPriceService.findPricesByProductAndSubscriptionTerm(wileyProductMock, subscriptionTermMock))
				.thenReturn(Collections.emptyList());

		// When
		final boolean isValid = wileyb2cCommercePriceService
				.validateProductSubscriptionTermPrice(wileyProductMock, subscriptionTermMock);
		// Then
		assertFalse(isValid);
	}

	@Test
	public void testComponentsOriginalPrice() throws Exception
	{
		//Given
		ProductReferenceModel referenceModel1 = mock(ProductReferenceModel.class);
		initPriceInformation(referenceModel1, PRICE_10);

		ProductReferenceModel referenceModel2 = mock(ProductReferenceModel.class);
		initPriceInformation(referenceModel2, PRICE_20);

		when(productReferenceServiceMock
				.getProductReferencesForSourceProduct(wileyProductMock, ProductReferenceTypeEnum.PRODUCT_SET_COMPONENT, true))
				.thenReturn(Arrays.asList(referenceModel1, referenceModel2));
		//When
		PriceInformation priceInformation = wileyb2cCommercePriceService.getComponentsOriginalPrice(wileyProductMock);
		//Then
		assertNotNull(priceInformation);
		assertEquals(CORRENCY_ISO, priceInformation.getPriceValue().getCurrencyIso());
		assertEquals(NET_PRICE, priceInformation.getPriceValue().isNet());
		assertEquals(PRICE_10 + PRICE_20, priceInformation.getPriceValue().getValue());
	}

	private void initPriceInformation(final ProductReferenceModel referenceModel, final double price)
	{
		ProductModel productComponentModel = mock(ProductModel.class);
		PriceInformation productComponentPriceInformation = mock(PriceInformation.class);
		when(referenceModel.getTarget()).thenReturn(productComponentModel);
		PriceValue priceValue = mock(PriceValue.class);
		when(productComponentPriceInformation.getPriceValue()).thenReturn(priceValue);
		when(priceValue.getCurrencyIso()).thenReturn(CORRENCY_ISO);
		when(priceValue.isNet()).thenReturn(NET_PRICE);
		when(priceValue.getValue()).thenReturn(price);
		doReturn(productComponentPriceInformation).when(wileyb2cCommercePriceService).getWebPriceForProduct(
				productComponentModel);
	}

	@Test
	public void testComponentsHasNoOriginalPrice() throws Exception
	{
		//Given
		ProductReferenceModel referenceModel1 = mock(ProductReferenceModel.class);
		ProductModel productComponentModel1 = mock(ProductModel.class);
		when(referenceModel1.getTarget()).thenReturn(productComponentModel1);
		doReturn(null).when(wileyb2cCommercePriceService).getWebPriceForProduct(productComponentModel1);

		ProductReferenceModel referenceModel2 = mock(ProductReferenceModel.class);
		ProductModel productComponentModel2 = mock(ProductModel.class);
		when(referenceModel2.getTarget()).thenReturn(productComponentModel1);
		doReturn(null).when(wileyb2cCommercePriceService).getWebPriceForProduct(productComponentModel2);

		when(productReferenceServiceMock
				.getProductReferencesForSourceProduct(wileyProductMock, ProductReferenceTypeEnum.PRODUCT_SET_COMPONENT, true))
				.thenReturn(Arrays.asList(referenceModel1, referenceModel2));
		//When
		PriceInformation priceInformation = wileyb2cCommercePriceService.getComponentsOriginalPrice(wileyProductMock);
		//Then
		assertNull(priceInformation);
	}

	@Test
	public void testSetHasNoComponents() throws Exception
	{
		//Given
		when(productReferenceServiceMock
				.getProductReferencesForSourceProduct(wileyProductMock, ProductReferenceTypeEnum.PRODUCT_SET_COMPONENT, true))
				.thenReturn(Collections.emptyList());
		//When
		PriceInformation priceInformation = wileyb2cCommercePriceService.getComponentsOriginalPrice(wileyProductMock);
		//Then
		assertNull(priceInformation);
	}
}