package com.wiley.core.wileycom.product.valuetranslator;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.voucher.VoucherService;
import de.hybris.platform.voucher.model.ProductRestrictionModel;
import de.hybris.platform.voucher.model.PromotionVoucherModel;

import java.util.Collection;
import java.util.Collections;

import javax.annotation.Resource;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;

import org.junit.Assert;
import org.junit.Test;


/**
 * @author Dzmitryi_Halahayeu
 */
@IntegrationTest
public class WileycomProductRestrictionTranslatorIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	private static final String VOUCHER_CODE = "222-B5K6-EYCB-6GBP";
	private static final String FIRST_ISBN = "222222222220";
	private static final String SECOND_ISBN = "222222222221";
	@Resource
	private VoucherService voucherService;
	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Test
	public void testTranslator() throws Exception
	{
		importCsv("/wileycore/test/product/valuetranslator/WileycomProductRestrictionTranslatorIntegrationTest/"
				+ "import-product-restriction.impex", DEFAULT_ENCODING);

		Collection<PromotionVoucherModel> vouchers = voucherService.getPromotionVouchers(VOUCHER_CODE);

		Assert.assertNotNull(vouchers);
		Assert.assertEquals(vouchers.size(), 1);
		PromotionVoucherModel voucherModel = vouchers.iterator().next();
		ProductRestrictionModel productRestrictionModel = (ProductRestrictionModel) flexibleSearchService.search(
				"SELECT {" + ProductRestrictionModel.PK + "} FROM {" + ProductRestrictionModel._TYPECODE
						+ "} WHERE {" + ProductRestrictionModel.VOUCHER + "} = ?voucher",
				Collections.singletonMap("voucher", voucherModel)).getResult().get(0);
		Assert.assertEquals(productRestrictionModel.getProducts().size(), 2);
		Assert.assertTrue(checkThatContainsProductWithIsbn(productRestrictionModel, FIRST_ISBN));
		Assert.assertTrue(checkThatContainsProductWithIsbn(productRestrictionModel, SECOND_ISBN));
	}

	private boolean checkThatContainsProductWithIsbn(final ProductRestrictionModel productRestrictionModel,
			final String productIsbn)
	{
		boolean found = false;
		for (ProductModel productModel : productRestrictionModel.getProducts())
		{
			if (productModel.getIsbn().equals(productIsbn))
			{
				found = true;
				break;
			}
		}
		return found;
	}
}




