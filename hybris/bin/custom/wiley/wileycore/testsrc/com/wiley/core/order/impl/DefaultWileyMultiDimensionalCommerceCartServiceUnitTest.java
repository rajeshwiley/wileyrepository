package com.wiley.core.order.impl;

import com.wiley.core.cart.WileyCommerceCartDao;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartModificationStatus;
import de.hybris.platform.commerceservices.order.CommerceUpdateCartEntryStrategy;
import de.hybris.platform.commerceservices.order.impl.CommerceCartMergingStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.site.BaseSiteService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.anySet;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.hamcrest.core.IsSame;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.wiley.core.model.WileyProductVariantSetModel;
import com.wiley.core.model.WileyVariantProductModel;
import com.wiley.core.order.WileyFindProductsInCartService;
import com.wiley.core.order.WileyProductQuantityInCartManager;
import com.wiley.core.product.WileyProductService;
import com.wiley.core.product.WileyProductVariantSetInformation;
import com.wiley.core.product.WileyProductVariantSetService;


/**
 * Unit test for {@link DefaultWileyMultiDimensionalCommerceCartService}
 *
 * @author Aliaksei_Zlobich
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultWileyMultiDimensionalCommerceCartServiceUnitTest {
	@InjectMocks
	private DefaultWileyMultiDimensionalCommerceCartService testedInstance =
			new DefaultWileyMultiDimensionalCommerceCartService();

	private CommerceCartParameter commerceCartParameter;
	@Mock
	private WileyFindProductsInCartService findProductsInCartService;
	@Mock
	private ProductModel productMock;

	@Mock
	private CartModel cartMock;

	@Mock
	UserModel userModelMock;
	@Mock
	BaseSiteModel siteModelMock;

	@Mock
	private WileyProductVariantSetService wileyProductVariantSetServiceMock;

	@Mock
	private WileyProductService wileyProductServiceMock;

	@Mock
	private CommerceUpdateCartEntryStrategy commerceUpdateCartEntryStrategyMock;

	@Mock
	private CommerceCartMergingStrategy commerceCartMergingStrategyMock;

	@Mock
	private ModelService modelServiceMock;

	@Mock
	BaseSiteService baseSiteServiceMock;

	@Mock
	private WileyProductQuantityInCartManager wileyProductQuantityInCartManagerMock;

	@Mock
	private WileyCommerceCartDao commerceCartDaoMock;

	// Cart Entries
	// Entry 1
	@Mock
	private AbstractOrderEntryModel orderEntryMock1;
	private Integer orderEntryNumber1 = 1;
	@Mock
	private WileyVariantProductModel productMock1;

	// Entry 2
	@Mock
	private AbstractOrderEntryModel orderEntryMock2;
	private Integer orderEntryNumber2 = 2;
	@Mock
	private WileyVariantProductModel productMock2;

	// Entry 3
	@Mock
	private AbstractOrderEntryModel orderEntryMock3;
	private Integer orderEntryNumber3 = 3;
	@Mock
	private WileyVariantProductModel productMock3;

	// Entry 4
	@Mock
	private AbstractOrderEntryModel orderEntryMock4;
	private Integer orderEntryNumber4 = 4;
	@Mock
	private WileyVariantProductModel productMock4;

	// Product Sets
	@Mock
	private WileyProductVariantSetModel productVariantSetMock1;
	@Mock
	private WileyProductVariantSetModel productVariantSetMock2;

	private class AddToCartStrategyAnswer implements Answer {
		@Override
		public Object answer(final InvocationOnMock invocation) throws Throwable {
			final CommerceCartParameter param = (CommerceCartParameter) invocation.getArguments()[0];
			CommerceCartModification addCartModification = new CommerceCartModification();
			addCartModification.setStatusCode(CommerceCartModificationStatus.SUCCESS);
			if (param != null) {
				addCartModification.setQuantity(param.getQuantity());
				addCartModification.setQuantityAdded(param.getQuantity());
				addCartModification.setEntry(orderEntryMock1);
			}
			return addCartModification;
		}
	}

	@Before
	public void setUp() throws Exception {
		// Set up Cart Parameter
		commerceCartParameter = new CommerceCartParameter();

		// Set up Cart
		when(orderEntryMock1.getEntryNumber()).thenReturn(orderEntryNumber1);
		when(orderEntryMock1.getProduct()).thenReturn(productMock1);

		when(orderEntryMock2.getEntryNumber()).thenReturn(orderEntryNumber2);
		when(orderEntryMock2.getProduct()).thenReturn(productMock2);

		when(orderEntryMock3.getEntryNumber()).thenReturn(orderEntryNumber3);
		when(orderEntryMock3.getProduct()).thenReturn(productMock3);

		when(orderEntryMock4.getEntryNumber()).thenReturn(orderEntryNumber4);
		when(orderEntryMock4.getProduct()).thenReturn(productMock4);

		final List<AbstractOrderEntryModel> cartEntries = new ArrayList<>();
		cartEntries.add(orderEntryMock1);
		cartEntries.add(orderEntryMock2);
		cartEntries.add(orderEntryMock3);
		cartEntries.add(orderEntryMock4);

		when(cartMock.getEntries()).thenReturn(cartEntries);

		// Set up WileyProductService
		when(wileyProductServiceMock.canBePartOfProductSet(eq(productMock1))).thenReturn(true);
		when(wileyProductServiceMock.canBePartOfProductSet(eq(productMock2))).thenReturn(true);
		when(wileyProductServiceMock.canBePartOfProductSet(eq(productMock3))).thenReturn(true);
		when(wileyProductServiceMock.canBePartOfProductSet(eq(productMock4))).thenReturn(true);

		// Set up CommerceAddToCartStrategy
		when(wileyProductQuantityInCartManagerMock.addToCart(any(CommerceCartParameter.class)))
				.thenAnswer(new AddToCartStrategyAnswer());
		when(wileyProductQuantityInCartManagerMock.addToCart(any(), anyBoolean())).
				thenAnswer(new AddToCartStrategyAnswer());

		// Set up CommerceRemoveCartEntriesStrategy
		CommerceCartModification removeEntriesCartModification = new CommerceCartModification();
		removeEntriesCartModification.setStatusCode(CommerceCartModificationStatus.SUCCESS);

		// Set up wileyProductVariantSetServiceMock
		WileyProductVariantSetInformation setInformation1 = new WileyProductVariantSetInformation();
		setInformation1.setProductSet(productVariantSetMock1);
		setInformation1.setIncludedProducts(Arrays.asList(productMock1, productMock2));

		WileyProductVariantSetInformation setInformation2 = new WileyProductVariantSetInformation();
		setInformation2.setProductSet(productVariantSetMock2);
		setInformation2.setIncludedProducts(Arrays.asList(productMock3, productMock4));

		when(wileyProductVariantSetServiceMock.getWileyProductSetsByProducts(any()))
				.thenReturn(Arrays.asList(setInformation1,
						setInformation2));
	}


	@Test
	public void collectCartEntriesToSetSuccess() throws CommerceCartModificationException {
		// Given
		// no changes on test data

		// When
		final List<CommerceCartModification> modifications = testedInstance.collectCartEntriesToSet(cartMock,
				Collections.emptyList());

		// Then
		assertTrue("Expected filled collection of cart modifications", CollectionUtils.isNotEmpty(modifications));
		// checking that product set parts have been removed for each product set

		// checking if two product sets have been added
		verify(wileyProductQuantityInCartManagerMock).addToCart(argThat(new ArgumentMatcher<CommerceCartParameter>() {
			@Override
			public boolean matches(final Object o) {
				CommerceCartParameter param = (CommerceCartParameter) o;
				return productVariantSetMock1.equals(param.getProduct()) && param.getQuantity() > 0;
			}
		}));
		verify(wileyProductQuantityInCartManagerMock).addToCart(argThat(new ArgumentMatcher<CommerceCartParameter>() {
			@Override
			public boolean matches(final Object o) {
				CommerceCartParameter param = (CommerceCartParameter) o;
				return productVariantSetMock2.equals(param.getProduct()) && param.getQuantity() > 0;
			}
		}));

		verify(wileyProductServiceMock, atLeast(1)).canBePartOfProductSet(same(productMock1));
		verify(wileyProductServiceMock, atLeast(1)).canBePartOfProductSet(same(productMock2));
		verify(wileyProductServiceMock, atLeast(2)).canBePartOfProductSet(same(productMock3));
		verify(wileyProductServiceMock, atLeast(2)).canBePartOfProductSet(same(productMock4));
	}

	@Test
	public void collectCartEntriesToSetIfThereAreNoProductSets() throws CommerceCartModificationException {
		// Given
		when(wileyProductVariantSetServiceMock.getWileyProductSetsByProducts(any())).
				thenReturn(Collections.emptyList());

		// When
		final List<CommerceCartModification> modifications = testedInstance.collectCartEntriesToSet(cartMock,
				Collections.emptyList());

		// Then
		assertTrue("Expected empty collection of cart modifications", CollectionUtils.isEmpty(modifications));
	}

	@Test(expected = CommerceCartModificationException.class)
	public void collectCartEntriesToSetCommerceCartModificationExceptionDuringAddingProductSet()
			throws CommerceCartModificationException {
		// Given
		CommerceCartModification modification = new CommerceCartModification();
		modification.setStatusCode(CommerceCartModificationStatus.UNAVAILABLE);
		when(wileyProductQuantityInCartManagerMock.addToCart(argThat(new ArgumentMatcher<CommerceCartParameter>() {
			@Override
			public boolean matches(final Object o) {
				CommerceCartParameter param = (CommerceCartParameter) o;
				return param.getProduct() instanceof WileyProductVariantSetModel;
			}
		}))).thenReturn(modification);

		// When
		testedInstance.collectCartEntriesToSet(cartMock, Collections.emptyList());
		fail("Expected CommerceCartModificationException");
	}

	@Test(expected = IllegalArgumentException.class)
	public void collectCartEntriesToSetCartIsNull() throws CommerceCartModificationException {
		// Given
		cartMock = null;
		// When
		testedInstance.collectCartEntriesToSet(cartMock, Collections.emptyList());
	}

	@Test
	public void addToCartSuccess() throws CommerceCartModificationException {
		// Given
		// set up cart parameter
		commerceCartParameter.setCart(cartMock);
		commerceCartParameter.setProduct(productMock);
		final int quantity = 4;
		commerceCartParameter.setQuantity(quantity);

		// set up cart modification
		CommerceCartModification modification = new CommerceCartModification();
		modification.setQuantityAdded(quantity);
		modification.setStatusCode(CommerceCartModificationStatus.SUCCESS);

		when(wileyProductQuantityInCartManagerMock.addToCart(eq(commerceCartParameter))).thenReturn(modification);

		// When
		final CommerceCartModification cartModification = testedInstance.addToCart(commerceCartParameter);

		// Then
		assertNotNull(cartModification);
		assertEquals(quantity, cartModification.getQuantityAdded());
		assertEquals(CommerceCartModificationStatus.SUCCESS, cartModification.getStatusCode());

		// checking that we delegate it to CommerceAddToCartStrategy
		verify(wileyProductQuantityInCartManagerMock).addToCart(eq(commerceCartParameter));
	}

	@Test
	public void addToCartSuccessIfProductExistsInCart() throws CommerceCartModificationException {
		// Given
		// set up cart parameter
		commerceCartParameter.setCart(cartMock);
		commerceCartParameter.setProduct(productMock1);
		final int quantity = 4;
		commerceCartParameter.setQuantity(quantity);

		// When
		final CommerceCartModification cartModification = testedInstance.addToCart(commerceCartParameter);

		// Then
		assertNotNull(cartModification);
		assertEquals(CommerceCartModificationStatus.SUCCESS, cartModification.getStatusCode());
		assertEquals("Expected that no products have been added to cart.", quantity,
				cartModification.getQuantityAdded());

		verify(wileyProductQuantityInCartManagerMock).addToCart(argThat(IsSame.sameInstance(commerceCartParameter)));
	}

	@Test
	public void addToCartMultipleParams() throws CommerceCartModificationException {
		// Given
		CommerceCartParameter param1 = new CommerceCartParameter();
		param1.setProduct(productMock1);
		param1.setQuantity(1);
		CommerceCartParameter param2 = new CommerceCartParameter();
		param2.setProduct(productMock2);
		param2.setQuantity(1);

		// When
		final List<CommerceCartModification> modifications = testedInstance.addToCart(cartMock, Arrays.asList(
				param1, param2));

		// Then
		assertNotNull(modifications);

		// Checking that expected product have been added to cart
		verify(wileyProductQuantityInCartManagerMock).addToCart(argThat(IsSame.sameInstance(param1)), eq(false));
		verify(wileyProductQuantityInCartManagerMock).addToCart(argThat(IsSame.sameInstance(param2)), eq(false));

		// Checking that products set have been searched to collect product parts.
		verify(wileyProductServiceMock, atLeast(1)).canBePartOfProductSet(any());
		verify(wileyProductVariantSetServiceMock, atLeast(1)).getWileyProductSetsByProducts(anySet());
	}

	@Test
	public void addToCartMultipleParamsWithNotSpecifiedStockLevel() throws CommerceCartModificationException {
		// Given
		when(orderEntryMock1.getQuantity()).thenReturn(null);
		// when trying to add product without stock level, order entry is
		// in [not saved] status and has null quantity inside
		// keepQuantityForEntriesInCartOrAllowToAddIfProductDidNotExist()
		// method. However in this case it will not go further to updateQuantityForCartEntry() method,
		// so other null fields can
		// be ignored for now.
		addToCartMultipleParams();
	}

	@Test
	public void testMergeCarts() throws Exception {
		// Given
		CartModel newCartModelMock = mock(CartModel.class);

		List<CommerceCartModification> commerceCartModificationList = new ArrayList<>();

		// When
		testedInstance.mergeCarts(cartMock, newCartModelMock, commerceCartModificationList);

		// Then
		verify(commerceCartMergingStrategyMock).mergeCarts(eq(cartMock), eq(newCartModelMock), same(
				commerceCartModificationList));
		// the case with collecting products to sets is covered in integration test.
		verify(wileyProductQuantityInCartManagerMock).checkAndUpdateQuantityIfRequired(eq(newCartModelMock), anyList());
	}

	@Test
	public void testSearchAndRemoveBrokenCarts()
	{
		// Given
		List<CartModel> cartModelListMock = new ArrayList<CartModel>(Arrays.asList(cartMock));
		when(cartMock.getEntries().get(orderEntryNumber1).getProduct()).thenReturn(null);

		when(baseSiteServiceMock.getCurrentBaseSite()).thenReturn(siteModelMock);
		when(commerceCartDaoMock.getCartsForSiteAndUser(siteModelMock, userModelMock)).thenReturn(cartModelListMock);

		// When
		testedInstance.searchAndRemoveBrokenCarts(userModelMock);

		// Then
		verify(modelServiceMock, times(1)).remove(cartMock);
	}

	@Test
	public void testDontRemoveBrokenCarts()
	{
		// Given
		List<CartModel> cartModelListMock = new ArrayList<CartModel>(Arrays.asList(cartMock));

		when(baseSiteServiceMock.getCurrentBaseSite()).thenReturn(siteModelMock);
		when(commerceCartDaoMock.getCartsForSiteAndUser(siteModelMock, userModelMock)).thenReturn(cartModelListMock);

		// When
		testedInstance.searchAndRemoveBrokenCarts(userModelMock);

		// Then
		verify(modelServiceMock, never()).remove(cartMock);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testNullUserForBrokenCart()
	{
		// Given
		List<CartModel> cartModelListMock = new ArrayList<CartModel>(Arrays.asList(cartMock));

		when(baseSiteServiceMock.getCurrentBaseSite()).thenReturn(siteModelMock);
		when(commerceCartDaoMock.getCartsForSiteAndUser(siteModelMock, userModelMock)).thenThrow(new NullPointerException());

		// When
		testedInstance.searchAndRemoveBrokenCarts(null);

		// Then
		verify(modelServiceMock, never()).remove(cartMock);

	}
}
