package com.wiley.core.metainformation;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.storefront.data.MetaElementData;
import de.hybris.platform.acceleratorservices.util.HtmlElementHelper;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.jsp.PageContext;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Created by Raman_Hancharou on 7/6/2017.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyHtmlMetaTagUnitTest
{


	public static final String TEST_VALUE = "testValue";
	public static final String CONTENT_ATTR = "content";
	public static final String NAME_ATTR = "name";

	private MetaElementData metaElementData;
	private Map<String, String> attributesMap;

	@Mock
	private HtmlElementHelper htmlElementHelper;

	@Spy
	@InjectMocks
	private WileyHtmlMetaTag wileyHtmlMetaTag;

	@Before
	public void setUp() throws Exception
	{
		metaElementData = new MetaElementData();
		attributesMap = new HashMap<>();
		attributesMap.put(NAME_ATTR, TEST_VALUE);
		doReturn(attributesMap).when(wileyHtmlMetaTag).getAttributesMap(metaElementData);
		doNothing().when(htmlElementHelper).writeOpenElement(any(PageContext.class), anyString(), anyMap());
	}

	@Test
	public void testWhenContentIsNotDefined() throws Exception
	{
		//when
		wileyHtmlMetaTag.writeMetaElement(metaElementData);
		//than
		verify(htmlElementHelper, never()).writeOpenElement(any(PageContext.class), anyString(), anyMap());
	}

	@Test
	public void testWhenContentIsDefined() throws Exception
	{
		//given
		attributesMap.put(CONTENT_ATTR, TEST_VALUE);
		//when
		wileyHtmlMetaTag.writeMetaElement(metaElementData);
		//than
		verify(htmlElementHelper, times(1)).writeOpenElement(any(PageContext.class), anyString(), anyMap());
	}

	@Test
	public void testWhenContentIsDefinedButEmpty() throws Exception
	{
		//given
		attributesMap.put(CONTENT_ATTR, StringUtils.EMPTY);
		//when
		wileyHtmlMetaTag.writeMetaElement(metaElementData);
		//than
		verify(htmlElementHelper, never()).writeOpenElement(any(PageContext.class), anyString(), anyMap());
	}

}
