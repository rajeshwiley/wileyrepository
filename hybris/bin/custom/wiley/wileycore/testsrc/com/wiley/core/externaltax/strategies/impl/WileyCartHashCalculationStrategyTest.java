package com.wiley.core.externaltax.strategies.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.service.data.CommerceOrderParameter;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.externaltax.strategies.WileyTaxAddressStrategy;




@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyCartHashCalculationStrategyTest
{
	private static final Double DELIVERY_COST = Double.valueOf(10.0);
	private static final Double TOTAL_DISCOUNT = Double.valueOf(5.0);
	private static final Long QUANTITY = Long.valueOf(1);
	private static final Double SUB_TOTAL_PRICE = Double.valueOf(25.0);
	private static final Double TOTAL_PRICE = Double.valueOf(50.0);
	private static final String CITY = "City";
	private static final String OTHER_CITY = "City1";
	private static final String POST_CODE = "1234";
	private static final String REGION_ISO_CODE = "NJ";
	private static final String COUNTRY_CODE = "US";
	private static final String CURRENCY_CODE = "USD";
	private static final String PRODUCT_CODE = "productCode1";
	private static final String HASH = "324614c95272b3437fd4d4ea88e0b9c8";

	@Mock
	private WileyTaxAddressStrategy taxAddressStrategyMock;

	@InjectMocks
	private final WileyCartHashCalculationStrategy testedInstance = new WileyCartHashCalculationStrategy();

	@Mock
	private CartModel cartModel;

	@Mock
	private AbstractOrderEntryModel cartEntry;

	@Mock
	private AddressModel address;

	@Mock
	private RegionModel region;

	@Mock
	private CountryModel country;

	@Mock
	private CurrencyModel currency;

	@Mock
	private ProductModel product;

	@Before
	public void initCart()
	{
		when(taxAddressStrategyMock.resolvePaymentAddress(cartModel)).thenReturn(address);
		when(address.getRegion()).thenReturn(region);
		when(cartModel.getCurrency()).thenReturn(currency);
		when(cartModel.getEntries()).thenReturn(Arrays.asList(cartEntry));
		when(cartEntry.getProduct()).thenReturn(product);

		when(address.getTown()).thenReturn(CITY);
		when(address.getPostalcode()).thenReturn(POST_CODE);
		when(address.getCountry()).thenReturn(country);
		when(region.getIsocodeShort()).thenReturn(REGION_ISO_CODE);
		when(country.getIsocode()).thenReturn(COUNTRY_CODE);
		when(cartModel.getDeliveryCost()).thenReturn(DELIVERY_COST);
		when(currency.getIsocode()).thenReturn(CURRENCY_CODE);
		when(cartModel.getTotalDiscounts()).thenReturn(TOTAL_DISCOUNT);
		when(cartEntry.getTotalPrice()).thenReturn(TOTAL_PRICE);
		when(cartEntry.getSubtotalPrice()).thenReturn(SUB_TOTAL_PRICE);
		when(cartEntry.getQuantity()).thenReturn(QUANTITY);
		when(cartEntry.getQuantity()).thenReturn(QUANTITY);
		when(product.getCode()).thenReturn(PRODUCT_CODE);
	}

	@Test
	public void shouldCalculateSameHash()
	{
		final CommerceOrderParameter parameter = new CommerceOrderParameter();
		parameter.setOrder(cartModel);

		final String hash = testedInstance.buildHashForAbstractOrder(parameter);
		Assert.assertThat(hash, CoreMatchers.equalTo(HASH));
	}

	@Test
	public void shouldCalculateDifferentHashesWithCartUpdate()
	{
		final CommerceOrderParameter parameter = new CommerceOrderParameter();
		parameter.setOrder(cartModel);
		when(address.getTown()).thenReturn(OTHER_CITY);

		final String hash = testedInstance.buildHashForAbstractOrder(parameter);
		Assert.assertThat(hash, CoreMatchers.not(HASH));
	}

}
