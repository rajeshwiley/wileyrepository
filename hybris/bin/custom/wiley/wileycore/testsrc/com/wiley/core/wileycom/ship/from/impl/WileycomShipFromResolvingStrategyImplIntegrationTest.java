package com.wiley.core.wileycom.ship.from.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.ordersplitting.WarehouseService;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.i18n.WileyCountryService;
import com.wiley.core.servicelayer.i18n.WileycomI18NService;


/**
 * Integration test for {@link WileycomShipFromResolvingStrategyImpl}
 */
@IntegrationTest
public class WileycomShipFromResolvingStrategyImplIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	private static final String B2C_UID = "wileyb2c";
	private static final String PRODUCT_CODE_PHYSICAL = "WCOM_B2C_PHYSICAL";
	private static final String PRODUCT_CODE_DIGITAL = "WDIGITAL_PRODUCT";
	private static final String PRODUCT_CODE_WITHOUT_DEFAULT_WAREHOUSE = "WCOM_PRODUCT";
	private static final String COUNTRY_ISOCODE_WITHOUT_WAREHOUSES = "CA";
	private static final String COUNTRY_ISOCODE_WITH_WAREHOUSES = "US";
	private static final String US_WAREHOUSE_CODE = "1002";

	private ProductModel product;

	@Resource
	private ProductService productService;

	@Resource
	private BaseSiteService baseSiteService;

	@Resource
	private WileycomShipFromResolvingStrategyImpl wileycomShipFromResolvingStrategy;

	@Resource
	private WileycomI18NService wileycomI18NService;

	@Resource
	private WileyCountryService wileyCountryService;

	@Resource
	private UserService userService;

	@Resource
	private WarehouseService warehouseService;

	@Before
	public void setUp() throws Exception
	{
		userService.setCurrentUser(userService.getAnonymousUser());
		baseSiteService.setCurrentBaseSite(baseSiteService.getBaseSiteForUID(B2C_UID), false);
		wileycomI18NService.setDefaultCurrentCountry();
	}

	@Test
	public void testForDigitalProduct()
	{
		//Given
		product = productService.getProductForCode(PRODUCT_CODE_DIGITAL);
		AddressModel expected = product.getExternalCompany().getAddress();

		//When
		AddressModel result = wileycomShipFromResolvingStrategy.resolveShipFromAddress(product);

		//Than
		assertAddress(expected, result);
	}

	@Test
	public void testWhenCountryInSession()
	{
		//Given
		AddressModel expected = warehouseService.getWarehouseForCode(US_WAREHOUSE_CODE).getAddress();
		product = productService.getProductForCode(PRODUCT_CODE_PHYSICAL);
		wileycomI18NService.setCurrentCountry(wileyCountryService.findCountryByCode(COUNTRY_ISOCODE_WITH_WAREHOUSES));

		//When
		AddressModel result = wileycomShipFromResolvingStrategy.resolveShipFromAddress(product);

		//Than
		assertAddress(expected, result);
	}

	@Test
	public void testWhenCountryNotInSession()
	{
		//Given
		AddressModel expected = warehouseService.getWarehouseForCode(US_WAREHOUSE_CODE).getAddress();
		product = productService.getProductForCode(PRODUCT_CODE_PHYSICAL);

		//When
		AddressModel result = wileycomShipFromResolvingStrategy.resolveShipFromAddress(product);

		//Than
		assertAddress(expected, result);
	}

	@Test
	public void testWhenWarehousesNotFound()
	{
		//Given
		product = productService.getProductForCode(PRODUCT_CODE_PHYSICAL);
		AddressModel expected = product.getDefaultWarehouse().getAddress();
		wileycomI18NService.setCurrentCountry(wileyCountryService.findCountryByCode(COUNTRY_ISOCODE_WITHOUT_WAREHOUSES));

		//When
		AddressModel result = wileycomShipFromResolvingStrategy.resolveShipFromAddress(product);

		//Than
		assertAddress(expected, result);
	}

	@Test
	public void testWhenProductHasNotDefaultWarehouse()
	{
		//Given
		product = productService.getProductForCode(PRODUCT_CODE_WITHOUT_DEFAULT_WAREHOUSE);
		wileycomI18NService.setCurrentCountry(wileyCountryService.findCountryByCode(COUNTRY_ISOCODE_WITHOUT_WAREHOUSES));

		//When
		try
		{
			wileycomShipFromResolvingStrategy.resolveShipFromAddress(product);
			fail("Expected IllegalStateException.");
		}
		catch (IllegalStateException e)
		{
			// Then
			//Success
		}
	}

	private void assertAddress(final AddressModel expected, final AddressModel actual)
	{
		assertNotNull(actual);
		assertEquals(expected.getStreetname(), actual.getStreetname());
		assertEquals(expected.getStreetnumber(), actual.getStreetnumber());
		assertEquals(expected.getTown(), actual.getTown());
		assertEquals(expected.getCountry().getIsocode(), actual.getCountry().getIsocode());
		if (expected.getRegion() != null)
		{
			assertEquals(expected.getRegion().getIsocode(), actual.getRegion().getIsocode());
		}
	}
}
