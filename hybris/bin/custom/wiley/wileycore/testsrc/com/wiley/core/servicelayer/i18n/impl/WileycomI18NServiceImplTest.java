package com.wiley.core.servicelayer.i18n.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.i18n.L10NService;
import de.hybris.platform.servicelayer.session.SessionService;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WorldRegionModel;

import static com.wiley.core.constants.WileyCoreConstants.CURRENT_COUNTRY_SESSION_ATTR;
import static junit.framework.TestCase.assertFalse;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileycomI18NServiceImplTest
{
	@Mock
	private SessionService sessionService;

	@Mock
	private CountryModel countryMock;

	@Mock
	private LanguageModel languageMock;

	@Mock
	private WorldRegionModel worldRegionMock;

	@Mock
	private CommonI18NService commonI18NService;

	@Mock
	private L10NService l10nService;


	@Spy
	@InjectMocks
	private WileycomI18NServiceImpl testedInstance = new WileycomI18NServiceImpl();

	@Test
	public void shouldReturnEmptyOptionalIfCurrentCountryIsNull()
	{
		when(sessionService.getAttribute(CURRENT_COUNTRY_SESSION_ATTR)).thenReturn(null);

		final Optional<CountryModel> currentCountry = testedInstance.getCurrentCountry();

		assertFalse(currentCountry.isPresent());
	}

	@Test
	public void shouldReturnCurrentCountry()
	{
		when(sessionService.getAttribute(CURRENT_COUNTRY_SESSION_ATTR)).thenReturn(countryMock);

		final Optional<CountryModel> currentCountry = testedInstance.getCurrentCountry();

		assertEquals(countryMock, currentCountry.get());
	}

}