package com.wiley.core.wileycom.restriction.impl;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.wiley.restriction.dto.WileyRestrictionCheckResultDto;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.BaseStoreModel;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.enums.WileyAssortment;
import com.wiley.core.store.WileyBaseStoreService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileycomRestrictVisibilityByAssortmentStrategyUnitTest
{
	private static final String DUMMY_CODE = "dummyCode";
	private static final String PRODUCT_NULL_IS_NOT_AVAILABLE_DUE_TO_ASSORTMENT_CHECK = "Product [" + DUMMY_CODE
			+ "] is not available due to assortment check.";

	@InjectMocks
	private WileycomRestrictVisibilityByAssortmentStrategy wileycomRestrictVisibilityByAssortmentStrategy;
	@Mock
	private ProductModel productModel;
	@Mock
	private BaseSiteService baseSiteService;
	@Mock
	private WileyBaseStoreService wileyBaseStoreService;
	@Mock
	private BaseSiteModel baseSiteModel;
	@Mock
	private BaseStoreModel baseStoreModel;
	private CommerceCartParameter parameter;
	private List<WileyAssortment> listWithIdenticalAssortment;
	private List<WileyAssortment> listWithDummyAssortment;

	@Before
	public void before()
	{
		parameter = new CommerceCartParameter();
		parameter.setProduct(productModel);
		listWithIdenticalAssortment =  new ArrayList<>();
		listWithIdenticalAssortment.add(WileyAssortment.B2BASSORTMENT);
		listWithDummyAssortment = new ArrayList<>();
		listWithDummyAssortment.add(WileyAssortment.B2CASSORTMENT);
		when(baseSiteService.getCurrentBaseSite()).thenReturn(baseSiteModel);
		when(wileyBaseStoreService.acquireBaseStoreByBaseSite(baseSiteModel)).thenReturn(baseStoreModel);
	}

	@Test
	public void shouldNotBeRestrictedWhenBaseStoreHasNoAssortment()
	{
		when(wileyBaseStoreService.acquireBaseStoreByBaseSite(baseSiteModel).getAssortment()).thenReturn(null);

		final boolean restricted = wileycomRestrictVisibilityByAssortmentStrategy.isRestricted(parameter);

		assertFalse(restricted);
	}

	@Test
	public void shouldNotBeRestrictedWhenBaseStoreHasAssortmentAndProductHasNoAssortments()
	{
		when(wileyBaseStoreService.acquireBaseStoreByBaseSite(baseSiteModel).getAssortment())
				.thenReturn(WileyAssortment.B2BASSORTMENT);
		when(productModel.getAssortments()).thenReturn(Collections.emptyList());

		final boolean restricted = wileycomRestrictVisibilityByAssortmentStrategy.isRestricted(parameter);

		assertTrue(restricted);
	}

	@Test
	public void shouldNotBeRestrictedWhenBaseStoreHasAssortmentAndProductHasSameAssortment()
	{
		when(wileyBaseStoreService.acquireBaseStoreByBaseSite(baseSiteModel).getAssortment())
				.thenReturn(WileyAssortment.B2BASSORTMENT);

        when(productModel.getAssortments()).thenReturn(listWithIdenticalAssortment);

		final boolean restricted = wileycomRestrictVisibilityByAssortmentStrategy.isRestricted(parameter);

		assertFalse(restricted);
	}

	@Test
	public void shouldBeRestrictedWhenBaseStoreHasAssortmentAndProductHasDifferentAssortment()
	{
		when(wileyBaseStoreService.acquireBaseStoreByBaseSite(baseSiteModel).getAssortment())
				.thenReturn(WileyAssortment.B2BASSORTMENT);

		when(productModel.getAssortments()).thenReturn(listWithDummyAssortment);

		final boolean restricted = wileycomRestrictVisibilityByAssortmentStrategy.isRestricted(parameter);

		assertTrue(restricted);
	}

	@Test
	public void shouldCreateErrorResult()
	{
		given(productModel.getCode()).willReturn(DUMMY_CODE);

		WileyRestrictionCheckResultDto result = wileycomRestrictVisibilityByAssortmentStrategy.createErrorResult(parameter);

		assertTrue(result != null);
		assertTrue(result.getErrorMessageCode().equals(WileyCoreConstants.PRODUCT_IS_NOT_PURCHASABLE));
		assertTrue(result.getErrorMessage().equals(PRODUCT_NULL_IS_NOT_AVAILABLE_DUE_TO_ASSORTMENT_CHECK));
		assertTrue(result.getErrorMessageParameters() == null);
	}
}
