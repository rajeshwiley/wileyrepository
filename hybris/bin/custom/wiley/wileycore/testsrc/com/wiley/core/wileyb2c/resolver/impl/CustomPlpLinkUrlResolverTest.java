/**
 *
 */
package com.wiley.core.wileyb2c.resolver.impl;

import static org.junit.Assert.assertNotNull;

import de.hybris.platform.category.model.CategoryModel;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class CustomPlpLinkUrlResolverTest
{
	protected static final String CATEGORY_SEO_NAME = "seoName";
	protected static final String CATEGORY_CODE = "catCode";
	protected static final String CATEGORY_NAME = "catName";
	protected static final String PATTERN = "pattern";

	@Mock
	private CategoryModel category;

	@InjectMocks
	private final CustomPlpLinkUrlResolver customPlpLinkUrlResolverTest = new CustomPlpLinkUrlResolver();

	@Test
	public void testResolveInternalWhenSeoNameExists()
	{
		BDDMockito.given(category.getSeoName()).willReturn(CATEGORY_SEO_NAME);
		BDDMockito.given(category.getCode()).willReturn(CATEGORY_CODE);
		customPlpLinkUrlResolverTest.setPattern(PATTERN);
		final String resolvedUrl = customPlpLinkUrlResolverTest.resolveInternal(category);
		assertNotNull(resolvedUrl);
		Assert.assertEquals(PATTERN, resolvedUrl);
	}

	@Test
	public void testResolveInternalWhenSeoNameDoesNotExists()
	{
		BDDMockito.given(category.getName()).willReturn(CATEGORY_NAME);
		BDDMockito.given(category.getCode()).willReturn(CATEGORY_CODE);
		customPlpLinkUrlResolverTest.setPattern(PATTERN);
		final String resolvedUrl = customPlpLinkUrlResolverTest.resolveInternal(category);
		assertNotNull(resolvedUrl);
		Assert.assertEquals(PATTERN, resolvedUrl);
	}
}
