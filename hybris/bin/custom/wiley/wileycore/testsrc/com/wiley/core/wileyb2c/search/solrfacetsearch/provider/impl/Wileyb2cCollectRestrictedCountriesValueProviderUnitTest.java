package com.wiley.core.wileyb2c.search.solrfacetsearch.provider.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.variants.model.VariantProductModel;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Created by Uladzimir_Barouski on 3/10/2017.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cCollectRestrictedCountriesValueProviderUnitTest
{
	private static final String US_CODE = "US";
	private static final String BY_CODE = "BY";
	private static final String PL_CODE = "PL";
	@InjectMocks
	private Wileyb2cRestrictedCountriesValueProvider wileyb2cCollectRestrictedCountriesValueProvider;

	@Mock
	private IndexConfig indexConfig;
	@Mock
	private IndexedProperty indexedProperty;
	@Mock
	private VariantProductModel variant;

	@Mock
	private CountryModel usCountry, byCountry, plCountry;

	@Before
	public void setUp() throws Exception
	{
		when(usCountry.getIsocode()).thenReturn(US_CODE);
		when(byCountry.getIsocode()).thenReturn(BY_CODE);
		when(plCountry.getIsocode()).thenReturn(PL_CODE);
	}

	@Test
	public void collectValuesWhenProductVariansEmpty() throws Exception
	{
		//Given
		when(variant.getVariants()).thenReturn(Collections.emptyList());

		//When
		List<String> restrictedCountries = wileyb2cCollectRestrictedCountriesValueProvider.collectValues(indexConfig,
				indexedProperty, variant);

		//Then
		assertTrue(CollectionUtils.isEmpty(restrictedCountries));
	}

	@Test
	public void collectValuesWhenProductHasRestrictedCountries() throws Exception
	{
		//Given
		when(variant.getCountryRestrictions()).thenReturn(Arrays.asList(usCountry, byCountry, plCountry));

		//When
		List<String> restrictedCountries = wileyb2cCollectRestrictedCountriesValueProvider.collectValues(indexConfig,
				indexedProperty, variant);

		//Then
		assertEquals(3, restrictedCountries.size());
		assertEquals(US_CODE, restrictedCountries.get(0));
		assertEquals(BY_CODE, restrictedCountries.get(1));
		assertEquals(PL_CODE, restrictedCountries.get(2));
	}

}