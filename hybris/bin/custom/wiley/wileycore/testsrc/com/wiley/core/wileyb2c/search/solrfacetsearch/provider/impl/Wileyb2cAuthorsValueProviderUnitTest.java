package com.wiley.core.wileyb2c.search.solrfacetsearch.provider.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.variants.model.VariantProductModel;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.AuthorInfoModel;
import com.wiley.core.wileyb2c.classification.impl.Wileyb2cClassificationServiceImpl;

import static org.hamcrest.Matchers.containsInAnyOrder;


/**
 * Created by Uladzimir_Barouski on 3/13/2017.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cAuthorsValueProviderUnitTest
{
	private static final String AUTHOR_1_NAME = "author 1 name";
	private static final String AUTHOR_2_NAME = "author 2 name";
	private static final String AUTHOR_3_NAME = "author 3 name";
	public static final String CLASSIFICATION_CODE = "ClassificationCode";
	@InjectMocks
	@Spy
	private Wileyb2cAuthorsValueProvider wileyb2cAuthorsValueProvider;
	@Mock
	private Wileyb2cClassificationServiceImpl wileyb2cClassificationService;
	@Mock
	private IndexConfig indexConfig;
	@Mock
	private IndexedProperty indexedProperty;
	@Mock
	private VariantProductModel variantProductModel;
	@Mock
	private ProductModel baseProductModel;
	@Mock
	private AuthorInfoModel author1, author2, author3;

	@Before
	public void setUp() throws Exception
	{
		when(author1.getName()).thenReturn(AUTHOR_1_NAME);
		when(author2.getName()).thenReturn(AUTHOR_2_NAME);
		when(author3.getName()).thenReturn(AUTHOR_3_NAME);
		when(variantProductModel.getBaseProduct()).thenReturn(baseProductModel);
	}

	@Test
	public void getFieldValuesWhenAuthorInfoEmpty() throws Exception
	{
		//Given
		when(baseProductModel.getAuthorInfos()).thenReturn(Collections.emptyList());

		//When
		Collection<FieldValue> authorsFieldValues = wileyb2cAuthorsValueProvider.getFieldValues(indexConfig,
				indexedProperty, variantProductModel);

		//Then
		assertTrue(CollectionUtils.isEmpty(authorsFieldValues));
	}

	@Test
	public void getFieldValuesWhenAuthorInfoNotEmpty() throws Exception
	{
		//Given
		when(baseProductModel.getAuthorInfos()).thenReturn(Collections.emptyList());
		when(indexedProperty.isLocalized()).thenReturn(false);
		when(wileyb2cAuthorsValueProvider.collectValues(indexConfig, indexedProperty, variantProductModel)).thenReturn(
				Collections.emptyList());

		//When
		Collection<FieldValue> authorsFieldValues = wileyb2cAuthorsValueProvider.getFieldValues(indexConfig,
				indexedProperty, variantProductModel);

		//Then
		verify(wileyb2cAuthorsValueProvider, atLeastOnce()).collectValues(eq(indexConfig), eq(indexedProperty), eq(
				variantProductModel));
	}

	@Test
	public void getFieldValuesWhenAuthorFacetRestricted() throws Exception
	{
		//Given
		Map<String, String> valueProviderParameters = Mockito.mock(HashMap.class);
		ClassificationClassModel classificationClassModel = Mockito.mock(ClassificationClassModel.class);
		when(baseProductModel.getAuthorInfos()).thenReturn(Arrays.asList(author1, author2, author3));
		when(indexedProperty.getValueProviderParameters()).thenReturn(valueProviderParameters);
		when(valueProviderParameters.get(Wileyb2cAuthorsValueProvider.SKIP_CATEGORY_PARAM)).thenReturn(CLASSIFICATION_CODE);
		when(wileyb2cClassificationService.resolveClassificationClass(variantProductModel)).thenReturn(classificationClassModel);
		when(classificationClassModel.getCode()).thenReturn(CLASSIFICATION_CODE);
		//When
		Collection<FieldValue> authorsFieldValues = wileyb2cAuthorsValueProvider.getFieldValues(indexConfig,
				indexedProperty, variantProductModel);

		//Then
		assertTrue(CollectionUtils.isEmpty(authorsFieldValues));
		verify(wileyb2cAuthorsValueProvider, never()).collectValues(eq(indexConfig), eq(indexedProperty), eq(
				variantProductModel));

	}

	@Test
	public void collectValuesAuthorInfoEmpty() throws Exception
	{
		//Given
		when(baseProductModel.getAuthorInfos()).thenReturn(Collections.emptyList());

		//When
		List<String> authors = wileyb2cAuthorsValueProvider.collectValues(indexConfig,
				indexedProperty, variantProductModel);

		//Then
		assertTrue(CollectionUtils.isEmpty(authors));
	}

	@Test
	public void collectValuesAuthorInfoNotEmpty() throws Exception
	{
		//Given
		when(baseProductModel.getAuthorInfos()).thenReturn(Arrays.asList(author1, author2, author3));

		//When
		List<String> authors = wileyb2cAuthorsValueProvider.collectValues(indexConfig,
				indexedProperty, variantProductModel);

		//Then
		assertTrue(CollectionUtils.isNotEmpty(authors));
		assertEquals(3, authors.size());
		assertThat(authors, containsInAnyOrder(AUTHOR_1_NAME, AUTHOR_2_NAME, AUTHOR_3_NAME));
	}

}