package com.wiley.core.wileycom.product.valuetranslator;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.voucher.VoucherService;
import de.hybris.platform.voucher.model.DateRestrictionModel;
import de.hybris.platform.voucher.model.ProductRestrictionModel;
import de.hybris.platform.voucher.model.PromotionVoucherModel;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Collections;

import javax.annotation.Resource;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;

import org.junit.Assert;
import org.junit.Test;


/**
 * @author Dzmitryi_Halahayeu
 */
@IntegrationTest
public class WileycomTemporaryRestrictionTranslatorIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	private static final String VOUCHER_CODE = "222-B5K6-EYCB-6GBP";
	private static final String START_DATE = "2016-12-22 11:44:00";
	private static final String END_DATE = "2016-12-22 15:44:00";
	private static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
	@Resource
	private VoucherService voucherService;
	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Test
	public void testTranslator() throws Exception
	{
		importCsv("/wileycore/test/product/valuetranslator/WileycomTemporaryRestrictionTranslatorIntegrationTest/"
				+ "import-temporary-restriction.impex", DEFAULT_ENCODING);
		Collection<PromotionVoucherModel> vouchers = voucherService.getPromotionVouchers(VOUCHER_CODE);


		Assert.assertNotNull(vouchers);
		Assert.assertEquals(vouchers.size(), 1);
		PromotionVoucherModel voucherModel = vouchers.iterator().next();
		DateRestrictionModel restrictionModel = (DateRestrictionModel) flexibleSearchService.search(
				"SELECT {" + DateRestrictionModel.PK + "} FROM {" + DateRestrictionModel._TYPECODE
						+ "} WHERE {" + ProductRestrictionModel.VOUCHER + "} = ?voucher",
				Collections.singletonMap("voucher", voucherModel)).getResult().get(0);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_TIME_FORMAT);
		Assert.assertEquals(restrictionModel.getStartDate(), simpleDateFormat.parse(START_DATE));
		Assert.assertEquals(restrictionModel.getEndDate(), simpleDateFormat.parse(END_DATE));
	}

}




