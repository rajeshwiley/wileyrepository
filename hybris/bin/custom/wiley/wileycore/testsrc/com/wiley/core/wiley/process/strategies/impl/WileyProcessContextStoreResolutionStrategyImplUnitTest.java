 package com.wiley.core.wiley.process.strategies.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.model.process.StoreFrontProcessModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.returns.model.ReturnProcessModel;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.store.BaseStoreModel;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.fulfilmentprocess.model.WileyExportProcessModel;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyProcessContextStoreResolutionStrategyImplUnitTest
{
	private static final String STORE_FRONT_STORE_UID = "storeFrontStoreUid";
	private static final String ORDER_STORE_UID = "orderStoreUid";
	private static final String CONSIGNMENT_STORE_UID = "consignmentStoreUid";
	private static final String RETURN_STORE_UID = "returnStoreUid";
	@InjectMocks
	private WileyProcessContextStoreResolutionStrategyImpl wileyProcessContextStoreResolutionStrategy;

	@Test
	public void whenStoreFrontProcessModelThenShouldReturnStoreFromIt()
	{
		StoreFrontProcessModel businessProcess = new StoreFrontProcessModel();
		businessProcess.setStore(createStore(STORE_FRONT_STORE_UID));

		BaseStoreModel store = wileyProcessContextStoreResolutionStrategy.getBaseStore(businessProcess);

		assertNotNull(store);
		assertEquals(store.getUid(), STORE_FRONT_STORE_UID);
	}

	@Test
	public void whenOrderProcessModelThenShouldReturnStoreFromOrder()
	{
		OrderProcessModel businessProcess = new OrderProcessModel();
		OrderModel orderModel = createOrder(createStore(ORDER_STORE_UID));
		businessProcess.setOrder(orderModel);

		BaseStoreModel store = wileyProcessContextStoreResolutionStrategy.getBaseStore(businessProcess);

		assertNotNull(store);
		assertEquals(store.getUid(), ORDER_STORE_UID);
	}

	@Test
	public void whenConsignmentProcessModelThenShouldReturnStoreFromConsignment()
	{
		ConsignmentProcessModel businessProcess = new ConsignmentProcessModel();
		ConsignmentModel consignmentModel = new ConsignmentModel();
		businessProcess.setConsignment(consignmentModel);
		OrderModel orderModel = createOrder(createStore(CONSIGNMENT_STORE_UID));
		consignmentModel.setOrder(orderModel);

		BaseStoreModel store = wileyProcessContextStoreResolutionStrategy.getBaseStore(businessProcess);

		assertNotNull(store);
		assertEquals(store.getUid(), CONSIGNMENT_STORE_UID);
	}

	@Test
	public void whenReturnProcessModelThenShouldReturnStoreFromReturnRequest()
	{
		ReturnProcessModel businessProcess = new ReturnProcessModel();
		ReturnRequestModel returnRequestModel = new ReturnRequestModel();
		businessProcess.setReturnRequest(returnRequestModel);
		OrderModel orderModel = createOrder(createStore(RETURN_STORE_UID));
		returnRequestModel.setOrder(orderModel);

		BaseStoreModel store = wileyProcessContextStoreResolutionStrategy.getBaseStore(businessProcess);

		assertNotNull(store);
		assertEquals(store.getUid(), RETURN_STORE_UID);
	}

	@Test
	public void whenOtherProcessModelThenShouldReturnNull()
	{
		WileyExportProcessModel wileyExportProcessModel = new WileyExportProcessModel();

		BaseStoreModel store = wileyProcessContextStoreResolutionStrategy.getBaseStore(wileyExportProcessModel);

		assertNull(store);
	}

	private OrderModel createOrder(final BaseStoreModel store)
	{
		OrderModel orderModel = new OrderModel();
		orderModel.setStore(store);
		return orderModel;
	}

	private BaseStoreModel createStore(final String storeUid)
	{
		BaseStoreModel baseStoreModel = new BaseStoreModel();
		baseStoreModel.setUid(storeUid);
		return baseStoreModel;
	}
}
