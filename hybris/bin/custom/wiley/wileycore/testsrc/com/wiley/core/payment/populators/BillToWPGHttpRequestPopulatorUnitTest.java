package com.wiley.core.payment.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionRequest;
import de.hybris.platform.acceleratorservices.payment.data.CustomerBillToData;
import de.hybris.platform.acceleratorservices.payment.data.PaymentData;
import de.hybris.platform.acceleratorservices.payment.data.PaymentInfoData;
import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Test for {@link BillToWPGHttpRequestPopulator}
 */

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class BillToWPGHttpRequestPopulatorUnitTest
{

	private static final String WPG_CUSTOM_BILLTO_TITLE_NAME = "WPG_CUSTOM_billto_title_name";
	private static final String WPG_CUSTOM_BILLTO_FIRST_NAME = "WPG_CUSTOM_billto_first_name";
	private static final String WPG_CUSTOM_BILLTO_LAST_NAME = "WPG_CUSTOM_billto_last_name";
	private static final String WPG_CUSTOM_BILLTO_LINE1 = "WPG_CUSTOM_billto_line1";
	private static final String WPG_CUSTOM_BILLTO_LINE2 = "WPG_CUSTOM_billto_line2";
	private static final String WPG_CUSTOM_BILLTO_CITY = "WPG_CUSTOM_billto_city";
	private static final String WPG_CUSTOM_BILLTO_STATE_NAME = "WPG_CUSTOM_billto_state_name";
	private static final String WPG_CUSTOM_BILLTO_POSTAL_CODE = "WPG_CUSTOM_billto_postalcode";
	private static final String WPG_CUSTOM_BILLTO_COUNTRY_NAME = "WPG_CUSTOM_billto_country_name";
	private static final String WPG_CUSTOM_PAYMENT_TYPE = "American Express";

	private static final String BILLTO_TITLE_NAME = "Mr.";
	private static final String BILLTO_FIRST_NAME = "Fname";
	private static final String BILLTO_LAST_NAME = "Lname";
	private static final String BILLTO_LINE1 = "150 Fifth Avenue";
	private static final String BILLTO_LINE2 = "31st floor";
	private static final String BILLTO_CITY = "New York";
	private static final String BILLTO_STATE_NAME = "NY";
	private static final String BILLTO_POSTAL_CODE = "10118";
	private static final String BILLTO_COUNTRY_NAME = "USA";

	@InjectMocks
	private BillToWPGHttpRequestPopulator billToWPGHttpRequestPopulator;

	private CustomerBillToData customerBillTooData;
	private CreateSubscriptionRequest createSubscriptionRequest;
	private PaymentData paymentData;
	private PaymentInfoData paymentInfoData;


	@Before
	public void setup()
	{
		customerBillTooData = new CustomerBillToData();
		createSubscriptionRequest = new CreateSubscriptionRequest();
		createSubscriptionRequest.setCustomerBillToData(customerBillTooData);
		paymentData = new PaymentData();
		paymentInfoData = new PaymentInfoData();
		paymentInfoData.setDisplayName(WPG_CUSTOM_PAYMENT_TYPE);
		createSubscriptionRequest.setPaymentInfoData(paymentInfoData);
	}

	@Test
	public void shouldPopulateTitleName()
	{
		customerBillTooData.setBillToTitleName(BILLTO_TITLE_NAME);

		billToWPGHttpRequestPopulator.populate(createSubscriptionRequest, paymentData);

		assertEquals(BILLTO_TITLE_NAME, paymentData.getParameters().get(WPG_CUSTOM_BILLTO_TITLE_NAME));
	}

	@Test
	public void shouldPopulateFirstName()
	{
		customerBillTooData.setBillToFirstName(BILLTO_FIRST_NAME);

		billToWPGHttpRequestPopulator.populate(createSubscriptionRequest, paymentData);

		assertEquals(BILLTO_FIRST_NAME, paymentData.getParameters().get(WPG_CUSTOM_BILLTO_FIRST_NAME));
	}

	@Test
	public void shouldPopulateLastName()
	{
		customerBillTooData.setBillToLastName(BILLTO_LAST_NAME);

		billToWPGHttpRequestPopulator.populate(createSubscriptionRequest, paymentData);

		assertEquals(BILLTO_LAST_NAME, paymentData.getParameters().get(WPG_CUSTOM_BILLTO_LAST_NAME));
	}

	@Test
	public void shouldPopulateLine1()
	{
		customerBillTooData.setBillToStreet1(BILLTO_LINE1);

		billToWPGHttpRequestPopulator.populate(createSubscriptionRequest, paymentData);

		assertEquals(BILLTO_LINE1, paymentData.getParameters().get(WPG_CUSTOM_BILLTO_LINE1));
	}

	@Test
	public void shouldPopulateLine2()
	{
		customerBillTooData.setBillToStreet2(BILLTO_LINE2);

		billToWPGHttpRequestPopulator.populate(createSubscriptionRequest, paymentData);

		assertEquals(BILLTO_LINE2, paymentData.getParameters().get(WPG_CUSTOM_BILLTO_LINE2));
	}

	@Test
	public void shouldPopulateCity()
	{
		customerBillTooData.setBillToCity(BILLTO_CITY);

		billToWPGHttpRequestPopulator.populate(createSubscriptionRequest, paymentData);

		assertEquals(BILLTO_CITY, paymentData.getParameters().get(WPG_CUSTOM_BILLTO_CITY));
	}

	@Test
	public void shouldPopulateStateName()
	{
		customerBillTooData.setBillToStateName(BILLTO_STATE_NAME);

		billToWPGHttpRequestPopulator.populate(createSubscriptionRequest, paymentData);

		assertEquals(BILLTO_STATE_NAME, paymentData.getParameters().get(WPG_CUSTOM_BILLTO_STATE_NAME));
	}

	@Test
	public void shouldPopulatePostalCode()
	{
		customerBillTooData.setBillToPostalCode(BILLTO_POSTAL_CODE);

		billToWPGHttpRequestPopulator.populate(createSubscriptionRequest, paymentData);

		assertEquals(BILLTO_POSTAL_CODE, paymentData.getParameters().get(WPG_CUSTOM_BILLTO_POSTAL_CODE));
	}

	@Test
	public void shouldPopulateCountryName()
	{
		customerBillTooData.setBillToCountryName(BILLTO_COUNTRY_NAME);

		billToWPGHttpRequestPopulator.populate(createSubscriptionRequest, paymentData);

		assertEquals(BILLTO_COUNTRY_NAME, paymentData.getParameters().get(WPG_CUSTOM_BILLTO_COUNTRY_NAME));
	}


}
