package com.wiley.core.validation.constraints;

import de.hybris.bootstrap.annotations.UnitTest;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.wiley.core.validation.constraintvalidators.DoubleNonnegativeValidator;


/**
 * Test for {@link DoubleNonnegativeValidator}
 */
@UnitTest
public class DoubleNonnegativeValidatorUnitTest
{
	private static final Double NULL_DOUBLE = null;
	private static final Double NEGATIVE_DOUBLE = -1D;
	private static final Double ZERO_DOUBLE = 0D;
	private static final Double POSITIVE_DOUBLE = 1D;
	private DoubleNonnegativeValidator doubleNonnegativeValidator;

	@Before
	public void setup()
	{
		doubleNonnegativeValidator = new DoubleNonnegativeValidator();
	}

	@Test
	public void shouldReturnTrueValidDueToNullValue()
	{
		//When
		boolean isValid = doubleNonnegativeValidator.isValid(NULL_DOUBLE, null);

		//Then
		assertTrue("Null value should be delegated to @NotNull validator", isValid);
	}

	@Test
	public void shouldReturnFalseValidDueToNegativeValue()
	{
		//When
		boolean isValid = doubleNonnegativeValidator.isValid(NEGATIVE_DOUBLE, null);

		//Then
		assertFalse(isValid);
	}

	@Test
	public void shouldReturnTrueValidDueToZeroValue()
	{
		//When
		boolean isValid = doubleNonnegativeValidator.isValid(ZERO_DOUBLE, null);

		//Then
		assertTrue(isValid);
	}

	@Test
	public void shouldReturnTrueValidDueToPositiveValue()
	{
		//When
		boolean isValid = doubleNonnegativeValidator.isValid(POSITIVE_DOUBLE, null);

		//Then
		assertTrue(isValid);
	}
}
