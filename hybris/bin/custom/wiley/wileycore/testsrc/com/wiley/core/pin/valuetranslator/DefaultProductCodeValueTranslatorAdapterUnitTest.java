package com.wiley.core.pin.valuetranslator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.impex.jalo.header.UnresolvedValueException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.jalo.Pin;
import com.wiley.core.product.WileyProductService;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultProductCodeValueTranslatorAdapterUnitTest
{

	public static final String ISBN = "test-isbn";
	public static final String TERM = "test-term";
	public static final String CODE = "test-code";
	public static final String CATALOG_ID = "test-catalogId";
	public static final String CATALOG_VERSION = "test-catalogVersion";
	public static final String SEPARATOR = ":";

	@Mock
	private WileyProductService productService;

	@Mock
	private Pin pin;

	@Mock
	private ProductModel productModel;

	@InjectMocks
	private DefaultProductCodeValueTranslatorAdapter defaultProductCodeValueTranslatorAdapter;

	@Before
	public void setUp() throws Exception
	{
		when(productModel.getCode()).thenReturn(CODE);
		when(productModel.getIsbn()).thenReturn(ISBN);
	}

	@Test
	public void shouldLookupProductByIsbnAndTermForSubscriptionProduct() throws UnresolvedValueException
	{
		when(productService.getProductForIsbnAndTerm(ISBN, TERM, CATALOG_ID, CATALOG_VERSION)).thenReturn(productModel);
		String productCode = (String) defaultProductCodeValueTranslatorAdapter
				.importValue(ISBN + SEPARATOR + TERM, pin, CATALOG_ID, CATALOG_VERSION);
		assertEquals(CODE, productCode);
	}

	@Test
	public void shouldLookupProductByIsbnForProduct() throws UnresolvedValueException
	{
		when(productService.getProductForIsbnAndTerm(ISBN, null, CATALOG_ID, CATALOG_VERSION)).thenReturn(productModel);
		String productCode = (String) defaultProductCodeValueTranslatorAdapter
				.importValue(ISBN, pin, CATALOG_ID, CATALOG_VERSION);
		assertEquals(CODE, productCode);
	}

	@Test(expected = UnresolvedValueException.class)
	public void shouldThrowExceptionWhenValueExpressionIsInvalid() throws UnresolvedValueException
	{
		when(productService.getProductForIsbnAndTerm(ISBN, null, CATALOG_ID, CATALOG_VERSION)).thenReturn(productModel);
		defaultProductCodeValueTranslatorAdapter
				.importValue(ISBN + SEPARATOR + TERM + SEPARATOR + CODE, pin, CATALOG_ID, CATALOG_VERSION);
	}

	@Test
	public void shouldExportValueAsIs() throws Exception
	{
		assertEquals(CODE, defaultProductCodeValueTranslatorAdapter.exportValue(CODE));
	}
}