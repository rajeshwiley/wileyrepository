package com.wiley.core.wileyb2c.search.solrfacetsearch.provider.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.variants.model.VariantProductModel;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.wiley.restriction.WileyProductRestrictionService;
import com.wiley.core.wileyb2c.classification.Wileyb2cClassificationAttributes;
import com.wiley.core.wileyb2c.classification.Wileyb2cClassificationService;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cNewestPublicationDateValueProviderUnitTest
{
	private static final String INCORRECT_DATE_FORMAT = "incorrectDateFormat";
	@InjectMocks
	private Wileyb2cNewestPublicationDateValueProvider wileyb2cNewestPublicationDateValueProvider;
	@Mock
	private WileyProductRestrictionService wileyProductRestrictionService;
	@Mock
	private Wileyb2cClassificationService wileyb2cClassificationService;
	@Mock
	private IndexConfig indexConfig;
	@Mock
	private IndexedProperty indexedProperty;
	@Mock
	private ProductModel baseProduct;
	@Mock
	private VariantProductModel variantProduct1;
	@Mock
	private VariantProductModel variantProduct2;

	@Before
	public void setUp() throws Exception
	{
		when(variantProduct1.getBaseProduct()).thenReturn(baseProduct);

	}

	@Test
	public void collectValuesWhenProductAreRestrictedThenNoValues() throws FieldValueProviderException
	{
		mockVariantProduct(false);

		final List<Date> dates = wileyb2cNewestPublicationDateValueProvider.collectValues(indexConfig, indexedProperty,
				variantProduct1);

		Assert.assertTrue(dates.isEmpty());
	}

	@Test
	public void collectValuesWhenProductHasNotPublicationDateThenNoValues() throws FieldValueProviderException
	{
		mockVariantProduct(true);
		when(wileyb2cClassificationService.resolveAttribute(variantProduct1,
				Wileyb2cClassificationAttributes.PUBLICATION_DATE)).thenReturn(StringUtils.EMPTY);

		final List<Date> dates = wileyb2cNewestPublicationDateValueProvider.collectValues(indexConfig, indexedProperty,
				variantProduct1);

		Assert.assertTrue(dates.isEmpty());
	}

	@Test(expected = RuntimeException.class)
	public void collectValuesWhenProductHasIncorrectDateFormatThenThrowException() throws FieldValueProviderException
	{
		mockVariantProduct(true);
		when(wileyb2cClassificationService.resolveAttribute(variantProduct1,
				Wileyb2cClassificationAttributes.PUBLICATION_DATE)).thenReturn(INCORRECT_DATE_FORMAT);

		wileyb2cNewestPublicationDateValueProvider.collectValues(indexConfig, indexedProperty,
				variantProduct1);
	}

	private void mockVariantProduct(final boolean searchable)
	{
		when(baseProduct.getVariants()).thenReturn(Collections.singletonList(variantProduct1));
		when(wileyProductRestrictionService.isSearchable(variantProduct1)).thenReturn(searchable);
	}

	@Test
	public void collectValuesWhenThereAreSeveralDatesThenShouldReturnNewest() throws FieldValueProviderException, ParseException
	{
		when(baseProduct.getVariants()).thenReturn(Arrays.asList(variantProduct1, variantProduct2));
		when(wileyProductRestrictionService.isSearchable(variantProduct1)).thenReturn(true);
		when(wileyProductRestrictionService.isSearchable(variantProduct2)).thenReturn(true);
		when(wileyb2cClassificationService.resolveAttribute(variantProduct1,
				Wileyb2cClassificationAttributes.PUBLICATION_DATE)).thenReturn("06/01/17");
		when(wileyb2cClassificationService.resolveAttribute(variantProduct2,
				Wileyb2cClassificationAttributes.PUBLICATION_DATE)).thenReturn("07/01/17");

		final List<Date> dates = wileyb2cNewestPublicationDateValueProvider.collectValues(indexConfig, indexedProperty,
				variantProduct1);

		Assert.assertEquals(dates.size(), 1);
		Assert.assertEquals(dates.get(0),
				new SimpleDateFormat(Wileyb2cNewestPublicationDateValueProvider.DATE_PATTERN).parse("07/01/17 +0000"));
	}


}
