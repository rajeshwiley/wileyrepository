package com.wiley.core.delivery.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.util.PriceValue;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyDeliveryServiceImplTest
{

	@Spy
	private WileyDeliveryServiceImpl testInstance = new WileyDeliveryServiceImpl();


	@Test
	public void testSortDeliveryModes() throws Exception
	{
		//Given
		DeliveryModeModel smallerPriceDeliveryMode = givenDeliveryModeWithPriceForOrder(cartModel, 10);
		DeliveryModeModel higherPriceDeliveryMode = givenDeliveryModeWithPriceForOrder(cartModel, 50);
		List<DeliveryModeModel> deliveryModes = Arrays.asList(higherPriceDeliveryMode, smallerPriceDeliveryMode);

		assert deliveryModes.get(0) == higherPriceDeliveryMode;
		//Testing
		testInstance.sortDeliveryModes(deliveryModes, cartModel);
		//Verify
		assertEquals(smallerPriceDeliveryMode, deliveryModes.get(0));

	}

	@Test
	public void testSortDeliveryModesShouldWorkIfNoDeliveryCostForPriceAndOrder() throws Exception
	{
		//Given
		DeliveryModeModel noPriceDeliveryMode = givenDeliveryModeWithoutPrice();
		DeliveryModeModel noPriceDeliveryMode2 = givenDeliveryModeWithoutPrice();
		// No price for Delivery mode and Order

		List<DeliveryModeModel> deliveryModes = Arrays.asList(noPriceDeliveryMode, noPriceDeliveryMode2);
		//Testing
		testInstance.sortDeliveryModes(deliveryModes, cartModel);
		//Verify
		assertEquals(2, deliveryModes.size());
	}

	@Mock
	private CartModel cartModel;

	private DeliveryModeModel givenDeliveryModeWithPriceForOrder(final AbstractOrderModel orderModel, double price) {
		DeliveryModeModel deliveryMode = mock(DeliveryModeModel.class);
		PriceValue priceValue = mock(PriceValue.class);
		when(priceValue.getValue()).thenReturn(price);
		doReturn(priceValue).when(testInstance).getDeliveryCostForDeliveryModeAndAbstractOrder(deliveryMode, orderModel);
		return deliveryMode;
	}

	private DeliveryModeModel givenDeliveryModeWithoutPrice() {
		DeliveryModeModel noPriceDeliveryMode = givenDeliveryModeWithPriceForOrder(cartModel, 0);
		doReturn(null).when(testInstance).getDeliveryCostForDeliveryModeAndAbstractOrder(noPriceDeliveryMode, cartModel);
		return noPriceDeliveryMode;
	}
}