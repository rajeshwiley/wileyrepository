package com.wiley.core.mpgs.services.impl;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.mpgs.constants.WileyMPGSConstants;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyMPGSPaymentProviderServiceImplTest
{
	private static final String TEST_PAYMENT_PROVIDER_VERSION = "V2";
	private static final String TEST_PAYMENT_PROVIDER = "MPGSV2";

	@InjectMocks
	private WileyMPGSPaymentProviderServiceImpl wileyMPGSPaymentProviderServiceMock;

	@Test
	public void testGeneratePaymentProvider()
	{
		//Given
		wileyMPGSPaymentProviderServiceMock.setPaymentProviderVersion(TEST_PAYMENT_PROVIDER_VERSION);
		//When
		String paymentProvider = wileyMPGSPaymentProviderServiceMock.generatePaymentProvider();
		//Then
		Assert.assertEquals(paymentProvider, WileyMPGSConstants.MPGS_PAYMENT_PROVIDER + TEST_PAYMENT_PROVIDER_VERSION);
	}

	@Test
	public void testIsMPGSProviderGroupTrue()
	{
		//Given
		//When
		boolean isMPGSProviderGroup = wileyMPGSPaymentProviderServiceMock.isMPGSProviderGroup(TEST_PAYMENT_PROVIDER);
		//Then
		Assert.assertTrue(isMPGSProviderGroup);

	}

	@Test
	public void testIsMPGSProviderGroupFalse()
	{
		//Given
		//When
		boolean isMPGSProviderGroup = wileyMPGSPaymentProviderServiceMock.isMPGSProviderGroup(null);
		//Then
		Assert.assertFalse(isMPGSProviderGroup);
	}
}