
package com.wiley.core.pages.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.restrictions.AbstractRestrictionModel;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyPartnerPageModel;
import com.wiley.core.pages.dao.WileyCMSPageDao;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyCMSPageServiceImplTest {

  private static final String PARTNER_CATEGORY_CODE = "WEL_CPA_PREFERRED_CATEGORY";

  @InjectMocks
  private WileyCMSPageServiceImpl testedInstance;

  @Mock
  private WileyCMSPageDao wileyCMSPageDao;
  @Mock
  private CatalogVersionService catalogVersionService;
  @Mock
  private WileyPartnerPageModel wileyPageModel;

  private Collection<CatalogVersionModel> catalogCollection = Collections.emptySet();

  @Test
  public void shouldReturnPartnerPage() {
    when(catalogVersionService.getSessionCatalogVersions()).thenReturn(catalogCollection);
    when(wileyCMSPageDao.findAllPagesByCategoryCode(WileyPartnerPageModel._TYPECODE,
        catalogCollection, PARTNER_CATEGORY_CODE))
            .thenReturn(createMockPartnerPages(wileyPageModel));
    when(wileyPageModel.getRestrictions()).thenReturn(new ArrayList<>());

    final WileyPartnerPageModel wileyPartnerPage = testedInstance
        .getPartnerPageByCategoryId(PARTNER_CATEGORY_CODE);

    assertEquals(wileyPageModel, wileyPartnerPage);

  }

  @Test
  public void shouldReturnNullIfNoPagesWereFound() {
    when(catalogVersionService.getSessionCatalogVersions()).thenReturn(catalogCollection);
    when(wileyCMSPageDao.findAllPagesByCategoryCode(WileyPartnerPageModel._TYPECODE,
        catalogCollection, PARTNER_CATEGORY_CODE)).thenReturn(createMockPartnerPages());
    final WileyPartnerPageModel wileyPartnerPage = testedInstance
        .getPartnerPageByCategoryId(PARTNER_CATEGORY_CODE);
    assertNull(wileyPartnerPage);
  }

  @Test
  public void shouldReturnNullIfFoundMoreThanOnePage() {
    when(catalogVersionService.getSessionCatalogVersions()).thenReturn(catalogCollection);
    when(wileyCMSPageDao.findAllPagesByCategoryCode(WileyPartnerPageModel._TYPECODE,
        catalogCollection, PARTNER_CATEGORY_CODE))
            .thenReturn(createMockPartnerPages(wileyPageModel, wileyPageModel));
    final WileyPartnerPageModel wileyPartnerPage = testedInstance
        .getPartnerPageByCategoryId(PARTNER_CATEGORY_CODE);
    assertNull(wileyPartnerPage);
  }


  @Test
  public void shouldReturnNullIfRestrictionFoundForPage() {
    when(catalogVersionService.getSessionCatalogVersions()).thenReturn(catalogCollection);
    when(wileyCMSPageDao.findAllPagesByCategoryCode(WileyPartnerPageModel._TYPECODE,
        catalogCollection, PARTNER_CATEGORY_CODE))
            .thenReturn(createMockPartnerPages(wileyPageModel));
    when(wileyPageModel.getRestrictions())
        .thenReturn(Collections.singletonList(new AbstractRestrictionModel()));
    final WileyPartnerPageModel wileyPartnerPage = testedInstance
        .getPartnerPageByCategoryId(PARTNER_CATEGORY_CODE);
    assertNull(wileyPartnerPage);
  }


  private Collection<AbstractPageModel> createMockPartnerPages(final AbstractPageModel... pages) {
    return Arrays.asList(pages);
  }
}
