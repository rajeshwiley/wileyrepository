package com.wiley.core.wileyb2b.order.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.commerceservices.strategies.CartCleanStrategy;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.product.ProductService;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.integration.ExternalCartModification;
import com.wiley.core.integration.ExternalCartModificationsStorageService;
import com.wiley.core.wiley.session.storage.WileyFailedCartModificationsStorageService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2bCartValidationStrategyTest
{

	private static final String FAILED_STATUS = "failedStatus";
	private static final String EXTERNAL_STATUS = "externalStatus";
	@InjectMocks
	private Wileyb2bCartValidationStrategy wileyb2bCartValidationStrategy;

	@Mock
	private WileyFailedCartModificationsStorageService wileyFailedCartModificationsStorageService;

	@Mock
	private CartCleanStrategy cartCleanStrategy;

	@Mock
	private CommerceCartService commerceCartService;

	@Mock
	private ProductService productService;

	@Mock
	private ExternalCartModificationsStorageService externalCartModificationsStorageService;

	@Mock
	private CartModel cartModel;
	private CommerceCartParameter parameter;

	@Before
	public void setUp() throws Exception
	{
		parameter = new CommerceCartParameter();
		parameter.setCart(cartModel);
	}

	@Test(expected = IllegalStateException.class)

	public void shouldThrowIllegalStateExceptionWhen() throws CalculationException
	{
		doThrow(CalculationException.class).when(commerceCartService).recalculateCart(parameter);

		wileyb2bCartValidationStrategy.validateCart(parameter);
	}

	public void shouldReturnModificationFromSessionAndExternalCall() throws CalculationException
	{
		final CommerceCartModification failedModification = new CommerceCartModification();
		failedModification.setStatusCode("failedStatus");
		when(wileyFailedCartModificationsStorageService.popAll()).thenReturn(
				Collections.singletonList(failedModification));
		final ExternalCartModification externalModification = ExternalCartModification.Builder.get().withStatusCode(
				"externalStatus").build();
		when(externalCartModificationsStorageService.popAll(cartModel)).thenReturn(
				Collections.singletonList(externalModification));

		List<CommerceCartModification> modifications = wileyb2bCartValidationStrategy.validateCart(parameter);

		Assert.assertEquals(modifications.size(), 2);
		Assert.assertEquals(modifications.get(0).getStatusCode(), FAILED_STATUS);
		Assert.assertEquals(modifications.get(1).getStatusCode(), EXTERNAL_STATUS);
	}
}
