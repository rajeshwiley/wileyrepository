package com.wiley.core.wel.preorder.dao.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.wel.preorder.dao.WelProductReferencesDao;


@IntegrationTest
public class WelProductReferencesDaoImplIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{

	public static final String TEST_PRE_ORDER_PRODUCT = "TEST_PRE_ORDER_PRODUCT";
	public static final String TEST_ACTIVE_PRODUCT = "TEST_ACTIVE_PRODUCT";
	public static final String WEL_PRODUCT_CATALOG = "welProductCatalog";
	public static final String ONLINE = "Online";

	@Resource
	private CatalogVersionService catalogVersionService;

	@Resource
	private WelProductReferencesDao welProductReferencesDao;

	@Resource
	private ProductService productService;

	private ProductModel targetProduct;
	private CatalogVersionModel catalogVersionModel;

	@Before
	public void setUp() throws Exception
	{
		catalogVersionModel = catalogVersionService.getCatalogVersion(WEL_PRODUCT_CATALOG, ONLINE);
		catalogVersionService.setSessionCatalogVersions(Collections.singletonList(catalogVersionModel));

		importCsv("/wileycore/test/product/dao/WelProductReferencesDaoImplIntegrationTest/testProducts.impex", DEFAULT_ENCODING);

		targetProduct = productService.getProductForCode(TEST_PRE_ORDER_PRODUCT);
	}

	@Test
	public void testFindSourceProductsForTargetProduct() throws Exception
	{
		// Given the targetProduct model with source product reference
		importCsv("/wileycore/test/product/dao/WelProductReferencesDaoImplIntegrationTest/createProductReference.impex",
				DEFAULT_ENCODING);

		// When
		final List<ProductModel> sourceProducts = welProductReferencesDao.findSourceReferenceProducts(
				targetProduct, ProductReferenceTypeEnum.NEXT_VERSION);

		// Then
		assertNotNull(sourceProducts);
		assertEquals(1,
				sourceProducts.size()); //Target product is pre-order product. Pre-order product has only one active product
		assertEquals(TEST_ACTIVE_PRODUCT, sourceProducts.get(0).getCode());
	}

	@Test
	public void testFindSourceProductsForTargetProductWithoutSourceProduct()
	{
		// Given the targetProduct model without source product references

		// When
		final List<ProductModel> targetProducts = welProductReferencesDao.findSourceReferenceProducts(
				targetProduct, ProductReferenceTypeEnum.NEXT_VERSION);

		// Then
		assertNotNull(targetProducts);
		assertTrue(targetProducts.isEmpty());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testFindSourceProductsForNull()
	{
		// When
		welProductReferencesDao.findSourceReferenceProducts(null, null);
	}
}
