package com.wiley.core.servicelayer.processdefinition.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.servicelayer.dao.processdefinition.WileyProcessDefinitionDao;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyProcessDefinitionServiceImplUnitTest
{
	@Mock
	private WileyProcessDefinitionDao daoMock;

	@InjectMocks
	private WileyProcessDefinitionServiceImpl service;

	@Test
	public void getAllProcessDefinitionsTest()
	{
		service.getAllProcessDefinitions();
		verify(daoMock, atLeastOnce()).findAllProcessDefinitions();
	}

	@Test
	public void getProcessDefinitionsByCodeTest()
	{
		final String testCode = "test_code";
		service.getProcessDefinitionsByCode(testCode);
		verify(daoMock, atLeastOnce()).findProcessDefinitionsByCode(testCode);
	}

}