package com.wiley.core.wileyb2c.availability.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.commerceservices.stock.CommerceStockService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.wileyb2c.classification.Wileyb2cClassificationService;
import com.wiley.core.wileyb2c.restriction.impl.Wileyb2cHonorProductStockLevelAvailabilityStrategy;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cHonorProductStockLevelAvailabilityStrategyUnitTest
{
	@InjectMocks
	private Wileyb2cHonorProductStockLevelAvailabilityStrategy wileyb2cHonorProductStockLevelAvailabilityStrategy;
	@Mock
	private Wileyb2cClassificationService wileyb2cClassificationService;
	@Mock
	private BaseStoreService baseStoreService;
	@Mock
	private CommerceStockService commerceStockService;
	@Mock
	private ClassificationClassModel classificationClass;
	@Mock
	private BaseStoreModel currentBaseStore;
	@Mock
	private ProductModel product;

	@Test(expected = IllegalArgumentException.class)
	public void testNullProduct()
	{

		wileyb2cHonorProductStockLevelAvailabilityStrategy.isRestricted((ProductModel) null);

	}

	@Test
	public void testOutOfStockProductWithHonorStockLevelInformation() {
		when(wileyb2cClassificationService.resolveClassificationClass(product)).thenReturn(classificationClass);
		when(baseStoreService.getCurrentBaseStore()).thenReturn(currentBaseStore);
		when(classificationClass.getHonorStockLevelInformation()).thenReturn(true);
		when(commerceStockService.getStockLevelForProductAndBaseStore(product, currentBaseStore)).thenReturn(0L);

		final boolean isRestricted = wileyb2cHonorProductStockLevelAvailabilityStrategy.isRestricted(product);

		Assert.assertTrue(isRestricted);
	}

	@Test
	public void testInStockProductWithHonorStockLevelInformation() {
		when(wileyb2cClassificationService.resolveClassificationClass(product)).thenReturn(classificationClass);
		when(baseStoreService.getCurrentBaseStore()).thenReturn(currentBaseStore);
		when(classificationClass.getHonorStockLevelInformation()).thenReturn(true);
		when(commerceStockService.getStockLevelForProductAndBaseStore(product, currentBaseStore)).thenReturn(10L);

		final boolean isRestricted = wileyb2cHonorProductStockLevelAvailabilityStrategy.isRestricted(product);

		Assert.assertFalse(isRestricted);
	}

	@Test
	public void testOutOfStockProductWithoutHonorStockLevelInformation() {
		when(wileyb2cClassificationService.resolveClassificationClass(product)).thenReturn(classificationClass);
		when(baseStoreService.getCurrentBaseStore()).thenReturn(currentBaseStore);
		when(classificationClass.getHonorStockLevelInformation()).thenReturn(false);
		when(commerceStockService.getStockLevelForProductAndBaseStore(product, currentBaseStore)).thenReturn(0L);

		final boolean isRestricted = wileyb2cHonorProductStockLevelAvailabilityStrategy.isRestricted(product);

		Assert.assertFalse(isRestricted);
	}

	@Test
	public void testInStockProductWithoutHonorStockLevelInformation() {
		when(wileyb2cClassificationService.resolveClassificationClass(product)).thenReturn(classificationClass);
		when(baseStoreService.getCurrentBaseStore()).thenReturn(currentBaseStore);
		when(classificationClass.getHonorStockLevelInformation()).thenReturn(false);
		when(commerceStockService.getStockLevelForProductAndBaseStore(product, currentBaseStore)).thenReturn(10L);

		final boolean isRestricted = wileyb2cHonorProductStockLevelAvailabilityStrategy.isRestricted(product);

		Assert.assertFalse(isRestricted);
	}
}
