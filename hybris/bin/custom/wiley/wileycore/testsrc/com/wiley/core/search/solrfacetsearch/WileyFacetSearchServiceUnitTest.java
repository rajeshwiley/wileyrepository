package com.wiley.core.search.solrfacetsearch;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.config.ValueRange;
import de.hybris.platform.solrfacetsearch.config.ValueRangeSet;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.wiley.core.search.solrfacetsearch.WileyFacetSearchService.RANGE_PATTERN;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyFacetSearchServiceUnitTest
{
	private static final String FROM = "FROM";
	private static final String TO = "TO";
	private static final String NAME = "NAME";
	@InjectMocks
	private WileyFacetSearchService wileyFacetSearchService;
	@Mock
	private IndexedType indexedType;
	@Mock
	private WileySearchQuery searchQuery;
	@Mock
	private IndexedProperty indexedProperty;
	@Mock
	private ValueRangeSet valueRangeSet;
	@Mock
	private ValueRange valueRange;

	@Test
	public void populateQueryFacetsWhenNoIndexedPropertiesShouldNotAddQuery()
	{
		when(indexedType.getIndexedProperties()).thenReturn(Collections.emptyMap());

		wileyFacetSearchService.populateQueryFacets(indexedType, searchQuery);

		verify(searchQuery, never()).addQueryFacet(any());
	}

	@Test
	public void populateQueryFacetsWhenNoQueryFacetsShouldNotAddQuery()
	{
		when(indexedType.getIndexedProperties()).thenReturn(Collections.singletonMap("test", indexedProperty));
		when(indexedProperty.isQueryFacet()).thenReturn(false);

		wileyFacetSearchService.populateQueryFacets(indexedType, searchQuery);

		verify(searchQuery, never()).addQueryFacet(any());
	}


	@Test
	public void populateQueryFacetsWhenNoQueryValueRangesShouldNotAddQuery()
	{
		when(indexedType.getIndexedProperties()).thenReturn(Collections.singletonMap("test", indexedProperty));
		when(indexedProperty.isQueryFacet()).thenReturn(true);
		when(indexedProperty.getQueryValueRangeSets()).thenReturn(Collections.emptyMap());

		wileyFacetSearchService.populateQueryFacets(indexedType, searchQuery);

		verify(searchQuery, never()).addQueryFacet(any());
	}

	@Test
	public void populateQueryFacetsWhenNoValueRangesShouldNotAddQuery()
	{
		when(indexedType.getIndexedProperties()).thenReturn(Collections.singletonMap("test", indexedProperty));
		when(indexedProperty.isQueryFacet()).thenReturn(true);
		when(indexedProperty.getQueryValueRangeSets()).thenReturn(Collections.singletonMap("default", valueRangeSet));
		when(valueRangeSet.getValueRanges()).thenReturn(Collections.emptyList());

		wileyFacetSearchService.populateQueryFacets(indexedType, searchQuery);

		verify(searchQuery, never()).addQueryFacet(any());
	}


	@Test
	public void populateQueryFacetsWhenValueRangeExistsShouldAddQuery()
	{
		when(indexedType.getIndexedProperties()).thenReturn(Collections.singletonMap("test", indexedProperty));
		when(indexedProperty.isQueryFacet()).thenReturn(true);
		when(indexedProperty.getQueryValueRangeSets()).thenReturn(Collections.singletonMap("default", valueRangeSet));
		when(valueRangeSet.getValueRanges()).thenReturn(Collections.singletonList(valueRange));
		when(valueRange.getFrom()).thenReturn(FROM);
		when(valueRange.getTo()).thenReturn(TO);
		when(indexedProperty.getName()).thenReturn(NAME);

		wileyFacetSearchService.populateQueryFacets(indexedType, searchQuery);

		verify(searchQuery).addQueryFacet(new WileyQueryFacet(NAME, String.format(RANGE_PATTERN, FROM, TO)));
	}
}
