package com.wiley.core.payment.strategies.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.acceleratorservices.payment.data.CustomerInfoData;
import de.hybris.platform.acceleratorservices.payment.data.OrderInfoData;
import de.hybris.platform.acceleratorservices.payment.data.PaymentInfoData;
import de.hybris.platform.acceleratorservices.payment.data.SubscriptionInfoData;
import de.hybris.platform.acceleratorservices.payment.data.WPGHttpValidateResultData;
import de.hybris.platform.commerceservices.order.CommerceCheckoutService;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.payment.PaymentService;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.dto.TransactionStatusDetails;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.payment.enums.WileyTransactionStatusEnum;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyPaymentTransactionStrategyImplTest
{
	private static final String PAYMENT_PROVIDER = "paymentProvider";
	private static final BigDecimal AMOUNT = new BigDecimal("10.10");
	private static final String MERCHANT_RESPONSE = "merchantResponse";
	private static final String SUCCESSFUL_CODE = "0";
	private static final String TOKEN = "token";
	private static final String AUTH_CODE = "authCode";
	private static final String TRANSACTION_ID = "transactionId";
	private static final String KNOWN_ERROR_CODE = "3";
	private static final String UNKNOWN_ERROR_CODE = "1010";

	@InjectMocks
	private WileyPaymentTransactionStrategyImpl underTest;
	@Mock
	private CommerceCheckoutService mockCommerceCheckoutService;
	@Mock
	private ModelService mockModelService;
	@Mock
	private PaymentService mockPaymentService;
	@Mock
	private Converter<CustomerInfoData, AddressModel> customerInfoToBillingAddressConverter;
	@Mock
	private WileyCreditCardPaymentInfoCreateStrategy wileyCreditCardPaymentInfoCreateStrategy;

	private CurrencyModel usdCurrency;
	private CreateSubscriptionResult createSubscriptionResult;
	private WPGHttpValidateResultData wpgResult;

	@Before
	public void setup()
	{
		final PaymentTransactionModel paymentTransaction = new PaymentTransactionModel();
		PaymentTransactionEntryModel paymentTransactionEntry = new PaymentTransactionEntryModel();
		paymentTransactionEntry.setPaymentTransaction(paymentTransaction);

		when(mockModelService.create(PaymentTransactionModel.class)).thenReturn(paymentTransaction);
		when(mockModelService.create(PaymentTransactionEntryModel.class)).thenReturn(paymentTransactionEntry);
		when(mockCommerceCheckoutService.getPaymentProvider()).thenReturn(PAYMENT_PROVIDER);
		when(mockPaymentService.getNewPaymentTransactionEntryCode(eq(paymentTransaction),
				any(PaymentTransactionType.class))).thenReturn("entryCode");

		AddressModel address = new AddressModel();
		CustomerInfoData customerInfoData = new CustomerInfoData();
		when(customerInfoToBillingAddressConverter.convert(customerInfoData)).thenReturn(address);

		SubscriptionInfoData subscriptionInfoData = new SubscriptionInfoData();
		PaymentInfoData paymentInfoData = new PaymentInfoData();
		when(wileyCreditCardPaymentInfoCreateStrategy
				.createCreditCardPaymentInfo(eq(subscriptionInfoData), eq(paymentInfoData), eq(address),
						any(CustomerModel.class), anyBoolean())).thenReturn(new CreditCardPaymentInfoModel());

		usdCurrency = new CurrencyModel();

		wpgResult = new WPGHttpValidateResultData();
		wpgResult.setValue(AMOUNT.toString());
		wpgResult.setTransID(TRANSACTION_ID);
		wpgResult.setAuthCode(AUTH_CODE);
		wpgResult.setToken(TOKEN);
		wpgResult.setReturnCode(SUCCESSFUL_CODE);
		wpgResult.setMerchantResponse(MERCHANT_RESPONSE);

		createSubscriptionResult = new CreateSubscriptionResult();
		createSubscriptionResult.setOrderInfoData(new OrderInfoData());
		createSubscriptionResult.setCustomerInfoData(customerInfoData);
		createSubscriptionResult.setWpgResultInfoData(wpgResult);
		createSubscriptionResult.setSubscriptionInfoData(subscriptionInfoData);
		createSubscriptionResult.setPaymentInfoData(paymentInfoData);
	}

	@Test
	public void shouldPopulateAuthTransactionInformation()
	{
		PaymentTransactionEntryModel paymentTransactionEntry = underTest.savePaymentTransactionEntry(new CustomerModel(),
				usdCurrency, createSubscriptionResult, PaymentTransactionType.AUTHORIZATION);

		assertEquals(PaymentTransactionType.AUTHORIZATION, paymentTransactionEntry.getType());
		assertEquals(AMOUNT, paymentTransactionEntry.getAmount());
		assertEquals(TRANSACTION_ID, paymentTransactionEntry.getRequestId());
		assertEquals(TOKEN, paymentTransactionEntry.getRequestToken());
		assertEquals(AUTH_CODE, paymentTransactionEntry.getWpgAuthCode());
		assertEquals(usdCurrency, paymentTransactionEntry.getCurrency());
	}

	@Test
	public void shouldPopulateValidateTransactionInformation()
	{
		PaymentTransactionEntryModel paymentTransactionEntry = underTest.savePaymentTransactionEntry(new CustomerModel(),
				usdCurrency, createSubscriptionResult, PaymentTransactionType.CREATE_SUBSCRIPTION);

		assertEquals(PaymentTransactionType.CREATE_SUBSCRIPTION, paymentTransactionEntry.getType());
		assertEquals(TRANSACTION_ID, paymentTransactionEntry.getRequestId());
		assertEquals(TOKEN, paymentTransactionEntry.getRequestToken());
	}

	@Test
	public void shouldPopulateSuccessfulTransactionStatus()
	{
		PaymentTransactionEntryModel paymentTransactionEntry = underTest.savePaymentTransactionEntry(new CustomerModel(),
				usdCurrency, createSubscriptionResult, PaymentTransactionType.AUTHORIZATION);

		assertEquals(TransactionStatus.ACCEPTED.name(), paymentTransactionEntry.getTransactionStatus());
		assertEquals(TransactionStatusDetails.SUCCESFULL.name(), paymentTransactionEntry.getTransactionStatusDetails());
		assertEquals(MERCHANT_RESPONSE, paymentTransactionEntry.getWpgMerchantResponse());
	}

	@Test
	public void shouldPopulateFailedKnownTransactionStatus()
	{
		wpgResult.setReturnCode(KNOWN_ERROR_CODE);
		PaymentTransactionEntryModel paymentTransactionEntry = underTest.savePaymentTransactionEntry(new CustomerModel(),
				usdCurrency, createSubscriptionResult, PaymentTransactionType.AUTHORIZATION);

		assertEquals(WileyTransactionStatusEnum.REFERRAL.name(), paymentTransactionEntry.getTransactionStatus());
		assertEquals(WileyTransactionStatusEnum.REFERRAL.getDescription(), paymentTransactionEntry.getTransactionStatusDetails());
		assertEquals(WileyTransactionStatusEnum.REFERRAL.getCode(), paymentTransactionEntry.getWpgResponseCode());

		assertEquals(MERCHANT_RESPONSE, paymentTransactionEntry.getWpgMerchantResponse());
	}

	@Test
	public void shouldPopulateFailedUnknownTransactionStatus()
	{
		wpgResult.setReturnCode(UNKNOWN_ERROR_CODE);
		PaymentTransactionEntryModel paymentTransactionEntry = underTest.savePaymentTransactionEntry(new CustomerModel(),
				usdCurrency, createSubscriptionResult, PaymentTransactionType.AUTHORIZATION);

		assertEquals(WileyTransactionStatusEnum.UNEXPECTED_CODE_FROM_WPG.name(), paymentTransactionEntry.getTransactionStatus());
		assertEquals(WileyTransactionStatusEnum.UNEXPECTED_CODE_FROM_WPG.getDescription(),
				paymentTransactionEntry.getTransactionStatusDetails());
		assertEquals(WileyTransactionStatusEnum.UNEXPECTED_CODE_FROM_WPG.getCode(), paymentTransactionEntry.getWpgResponseCode());

		assertEquals(MERCHANT_RESPONSE, paymentTransactionEntry.getWpgMerchantResponse());
	}
}