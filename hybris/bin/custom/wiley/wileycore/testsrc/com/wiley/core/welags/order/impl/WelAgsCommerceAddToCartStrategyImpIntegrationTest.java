package com.wiley.core.welags.order.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.commerceservices.order.impl.DefaultCommerceAddToCartStrategy;
import de.hybris.platform.site.BaseSiteService;

import javax.annotation.Resource;

import org.junit.Before;

import com.wiley.core.wiley.order.impl.WileyCommerceAddToCartStrategyImpIntegrationTest;


/**
 * @author Dzmitryi_Halahayeu
 */
@IntegrationTest
public class WelAgsCommerceAddToCartStrategyImpIntegrationTest extends WileyCommerceAddToCartStrategyImpIntegrationTest
{
	private static final String BASE_SITE = "wel";
	
	@Resource
	private WelAgsCommerceAddToCartStrategy welAgsCommerceAddToCartStrategyImpl;
	@Resource 
	private BaseSiteService baseSiteService;
	
	@Before
	public void setUp()
	{
		baseSiteService.setCurrentBaseSite(baseSiteService.getBaseSiteForUID(BASE_SITE), true);
	}
	@Override
	protected DefaultCommerceAddToCartStrategy getAddToCartStrategy()
	{
		return welAgsCommerceAddToCartStrategyImpl;
	}

	@Override
	protected String getRestrictedUserGroupProductCode()
	{
		return "WEL_USER_GROUP_RESTRICTED";
	}

}
