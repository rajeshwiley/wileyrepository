package com.wiley.core.wileyb2b.order.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BUnitService;
import de.hybris.platform.core.model.order.OrderModel;
import static org.mockito.Mockito.when;

import com.wiley.core.integration.order.email.Wileyb2bSendEmailWithOrderDetailsGateway;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Unit test for {@link Wileyb2bSendEmailWithOrderDetailsServiceImpl}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2bSendEmailWithOrderDetailsServiceImplUnitTest
{

	private static final String TEST_EMAIL = "testemail@example.com";
	private static final String TEST_B2B_UNIT_SAP_ACCOUNT_NUMBER = "testSapAccountNumber";
	private static final String TEST_B2B_CUSTOMER_UID = "testuid";
	private static final String TEST_ORDER_CODE = "100000";
	private static final boolean TEST_GATEWAY_EMAIL_SENT_RESULT = true;

	@InjectMocks
	private Wileyb2bSendEmailWithOrderDetailsServiceImpl wileyb2bSendEmailWithOrderDetailsService;

	@Mock
	private Wileyb2bSendEmailWithOrderDetailsGateway wileyb2bSendEmailWithOrderDetailsGatewayMock;

	@Mock
	private B2BUnitService<B2BUnitModel, B2BCustomerModel> b2bUnitServiceMock;

	@Mock
	private B2BUnitModel b2BUnitModelMock;

	@Mock
	private B2BCustomerModel b2BCustomerMock;

	@Mock
	private OrderModel orderMock;

	@Rule
	public final ExpectedException exception = ExpectedException.none();


	@Test
	public void shouldThrowIllegalArgumentExceptionOnNullCustomer()
	{
		//Given
		exception.expect(IllegalArgumentException.class);

		//When
		wileyb2bSendEmailWithOrderDetailsService.sendEmailWithOrderDetails(null, null, null);
	}

	@Test
	public void shouldThrowIllegalArgumentExceptionOnNullOrder()
	{
		//Given
		exception.expect(IllegalArgumentException.class);

		//When
		wileyb2bSendEmailWithOrderDetailsService.sendEmailWithOrderDetails(b2BCustomerMock, null, null);
	}

	@Test
	public void shouldThrowIllegalArgumentExceptionOnNullEmail()
	{
		//Given
		exception.expect(IllegalArgumentException.class);

		//When
		wileyb2bSendEmailWithOrderDetailsService.sendEmailWithOrderDetails(b2BCustomerMock, orderMock, null);
	}

	@Test
	public void shouldThrowIllegalArgumentExceptionOnEmptyEmail()
	{
		//Given
		exception.expect(IllegalArgumentException.class);

		//When
		wileyb2bSendEmailWithOrderDetailsService.sendEmailWithOrderDetails(b2BCustomerMock, orderMock, StringUtils.EMPTY);
	}

	@Test
	public void shouldReturnWileyb2bSendEmailWithOrderDetailsGatewayResult()
	{
		//Given
		when(b2bUnitServiceMock.getParent(b2BCustomerMock)).thenReturn(b2BUnitModelMock);
		when(orderMock.getCode()).thenReturn(TEST_ORDER_CODE);
		when(b2BCustomerMock.getUid()).thenReturn(TEST_B2B_CUSTOMER_UID);
		when(b2BUnitModelMock.getSapAccountNumber()).thenReturn(TEST_B2B_UNIT_SAP_ACCOUNT_NUMBER);
		when(wileyb2bSendEmailWithOrderDetailsGatewayMock
				.sendEmailWithOrderDetails(TEST_B2B_CUSTOMER_UID, TEST_EMAIL, TEST_ORDER_CODE, TEST_B2B_UNIT_SAP_ACCOUNT_NUMBER))
				.thenReturn(TEST_GATEWAY_EMAIL_SENT_RESULT);

		//When
		boolean isEmailSent = wileyb2bSendEmailWithOrderDetailsService.sendEmailWithOrderDetails(b2BCustomerMock, orderMock,
				TEST_EMAIL);

		//Then
		Assert.assertTrue("Service should return boolean result from underlying gateway",
				TEST_GATEWAY_EMAIL_SENT_RESULT == isEmailSent);
	}

}
