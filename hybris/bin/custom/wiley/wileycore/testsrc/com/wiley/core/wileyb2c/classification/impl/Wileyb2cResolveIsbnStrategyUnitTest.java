package com.wiley.core.wileyb2c.classification.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureValue;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.wileyb2c.classification.Wileyb2cClassificationAttributes;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cResolveIsbnStrategyUnitTest
{
	@InjectMocks
	private Wileyb2cResolveIsbnStrategy wileyb2cResolveIsbnStrategy;
	@Mock
	private Feature feature;
	@Mock
	private FeatureValue featureValue;

	@Test
	public void processFeature()
	{
		when(feature.getValue()).thenReturn(featureValue);
		when(featureValue.getValue()).thenReturn("9781786301062");

		final String processFeature = wileyb2cResolveIsbnStrategy.processFeature(feature);

		assertEquals(processFeature, "978-1-786-30106-2");
	}

	@Test
	public void getAttributes()
	{
		final Set<Wileyb2cClassificationAttributes> attributes = wileyb2cResolveIsbnStrategy.getAttributes();

		assertEquals(attributes, Wileyb2cResolveIsbnStrategy.CLASSIFICATION_ATTRIBUTES);
	}

	@Test(expected = NullPointerException.class)
	public void expectedExceptionWhenIsbnIsNull()
	{
		when(feature.getValue()).thenReturn(featureValue);
		when(featureValue.getValue()).thenReturn(null);

		wileyb2cResolveIsbnStrategy.processFeature(feature);
	}

	@Test(expected = IllegalArgumentException.class)
	public void expectedExceptionWhenIsbnLengthIsInvalid()
	{
		when(feature.getValue()).thenReturn(featureValue);
		when(featureValue.getValue()).thenReturn("");

		wileyb2cResolveIsbnStrategy.processFeature(feature);
	}
}
