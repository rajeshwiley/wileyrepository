package com.wiley.core.wileyb2c.basesite.theme.impl;


import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteTheme;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static junit.framework.Assert.assertEquals;


/**
 * Test for {@link Wileyb2cThemeNameResolutionStrategyImpl}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cThemeNameResolutionStrategyImplUnitTest
{
	private static final String TEST_BASE_SITE_THEME = "basesitetheme";
	private static final String TEST_DEFAULT_BASE_SITE_THEME = "defaultbasesitetheme";

	@InjectMocks
	private Wileyb2cThemeNameResolutionStrategyImpl wileyb2cThemeNameResolutionStrategy;

	@Mock
	private BaseSiteModel baseSiteMock;

	@Before
	public void setUp() throws Exception
	{
		wileyb2cThemeNameResolutionStrategy.setDefaultThemeName(TEST_DEFAULT_BASE_SITE_THEME);
	}

	@Test
	public void shouldReturnDefaultThemeWhenBaseSiteThemeIsNull()
	{
		//Given
		when(baseSiteMock.getTheme()).thenReturn(null);

		//When
		String theme = wileyb2cThemeNameResolutionStrategy.resolveThemeName(baseSiteMock);

		//Then
		assertEquals(TEST_DEFAULT_BASE_SITE_THEME, theme);
	}


	@Test
	public void shouldReturnSiteThemeCode()
	{
		//Given
		when(baseSiteMock.getTheme()).thenReturn(SiteTheme.valueOf(TEST_BASE_SITE_THEME));

		//When
		String theme = wileyb2cThemeNameResolutionStrategy.resolveThemeName(baseSiteMock);

		//Then
		assertEquals(TEST_BASE_SITE_THEME, theme);
	}

}
