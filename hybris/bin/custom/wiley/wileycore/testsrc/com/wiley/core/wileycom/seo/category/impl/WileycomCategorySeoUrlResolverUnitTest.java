package com.wiley.core.wileycom.seo.category.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileycomCategorySeoUrlResolverUnitTest
{
	private static final String FULL_PATTERN = "/{topCategoryName}/{categoryName}-c-{categoryCode}/";
	private static final String TOP_PATTERN = "/{categoryName}-c-{categoryCode}/";

	private static final String EXPECTED_FULL_SEO_URL_WITH_SEO_NAMES = "/topCatSeoName/cat+Seo+Name-c-catCode/";
	private static final String EXPECTED_FULL_SEO_URL_WITH_NAMES = "/topCatName/catName-c-catCode/";

	private static final String EXPECTED_TOP_SEO_URL_WITH_SEO_NAMES = "/cat+Seo+Name-c-catCode/";

	private static final String SEO_NAME_WITH_RESERVED_CHARACTERS = "seo name :/#?&@%+";
	private static final String EXPECTED_SEO_NAME_WITH_RESERVED_CHARACTERS = "seo+name+%3A%2F%23%3F%26%40%25%2B";

	@InjectMocks
	private WileycomCategorySeoUrlResolver underTest;
	@Mock
	private CategoryModel mockCategory;
	@Mock
	private CategoryModel mockTopCategory;
	@Mock
	private CategoryService mockCategoryService;


	@Before
	public void setup()
	{
		when(mockCategory.getCode()).thenReturn("catCode");
		when(mockCategory.getSeoName()).thenReturn("cat Seo Name");
		when(mockCategory.getName()).thenReturn("catName");

		when(mockTopCategory.getCode()).thenReturn("topCatCode");
		when(mockTopCategory.getSeoName()).thenReturn("topCatSeoName");
		when(mockTopCategory.getName()).thenReturn("topCatName");

		underTest.setFullUrlPattern(FULL_PATTERN);
		underTest.setTopUrlPattern(TOP_PATTERN);

	}

	private void whenCategoryHasPath(final CategoryModel... pathElements)
	{
		when(mockCategoryService.getPathsForCategory(mockCategory))
				.thenReturn(Collections.singletonList(Arrays.asList(pathElements)));
	}

	@Test
	public void shouldCreateFullSeoUrlForNonTopCategory()
	{
		whenCategoryHasPath(mock(CategoryModel.class), mockTopCategory, mockCategory);

		final String actualSeoUrl = underTest.resolveInternal(mockCategory);
		assertEquals(EXPECTED_FULL_SEO_URL_WITH_SEO_NAMES, actualSeoUrl);
	}

	@Test
	public void shouldCreateTopOnlySeoUrlForTopCategory()
	{
		whenCategoryHasPath(mock(CategoryModel.class), mockCategory);

		final String actualSeoUrl = underTest.resolveInternal(mockCategory);
		assertEquals(EXPECTED_TOP_SEO_URL_WITH_SEO_NAMES, actualSeoUrl);
	}

	@Test
	public void shouldCreateTopOnlySeoUrlForRootCategory()
	{
		whenCategoryHasPath(mockCategory);

		final String actualSeoUrl = underTest.resolveInternal(mockCategory);
		assertEquals(EXPECTED_TOP_SEO_URL_WITH_SEO_NAMES, actualSeoUrl);
	}

	@Test
	public void shouldUseNamesIfNoSeoNamesSet()
	{
		whenCategoryHasPath(mock(CategoryModel.class), mockTopCategory, mockCategory);

		when(mockTopCategory.getSeoName()).thenReturn(null);
		when(mockCategory.getSeoName()).thenReturn(null);

		final String actualSeoUrl = underTest.resolveInternal(mockCategory);
		assertEquals(EXPECTED_FULL_SEO_URL_WITH_NAMES, actualSeoUrl);
	}

	@Test
	public void shouldUrlSafeSeoNameWithReservedCharacters()
	{
		// Given
		when(mockCategory.getSeoName()).thenReturn(SEO_NAME_WITH_RESERVED_CHARACTERS);

		// When
		final String actualSeoName = underTest.getSeoName(mockCategory);

		// Then
		assertEquals(EXPECTED_SEO_NAME_WITH_RESERVED_CHARACTERS, actualSeoName);
	}
}
