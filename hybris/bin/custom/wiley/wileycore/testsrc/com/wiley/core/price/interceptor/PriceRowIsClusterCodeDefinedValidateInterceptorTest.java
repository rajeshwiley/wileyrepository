package com.wiley.core.price.interceptor;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.europe1.PDTRowTestDataBuilder;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.model.ModelService;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Resource;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Integration test for {@link PriceRowIsClusterCodeDefinedValidateInterceptor}
 */
@IntegrationTest
public class PriceRowIsClusterCodeDefinedValidateInterceptorTest extends AbstractWileyServicelayerTransactionalTest
{
    private static final String PRODUCT_1 = "TEST_PRODUCT_1";
    private static final String PRODUCT_2 = "TEST_PRODUCT_2";
    private static final String CLUSTER_CODE_MESSAGE = "Cluster code";
    private static final String EUR = "EUR";
    private static final String USD = "USD";
    private static final String GBP = "GBP";

    @Resource
    private ModelService modelService;
    @Resource
    private CommonI18NService commonI18NService;
    @Resource
    private ProductService productService;
    private UnitModel unit;
    
    
    @Before
    public void setUp() throws Exception
    {
        importCsv("/wileycore/test/product/clusterCodeCurrencyPriceRowDependency.impex", DEFAULT_ENCODING);

        final PDTRowTestDataBuilder testDataBuilder = new PDTRowTestDataBuilder(modelService);
        unit = testDataBuilder.createUnit("pieces-test", "pieces");
    }

    //*** positive cases ****/
    @Test
    public void testProduct2HasClusterCodeForEur()
    {
    	// Given
        Exception catchedException = null;
        
    	// Testing
        try {
            createPriceRow(PRODUCT_2, commonI18NService.getCurrency(EUR));
        } catch (Exception e) {
            catchedException = e;
        }

        // Verify
        assertNull(catchedException);
    }
    
    @Test
    public void testUsdDoesNotRequireClusterCode()
    {
    	// Given
        Exception catchedException = null;
        
    	// Testing
        try {
            createPriceRow(PRODUCT_2, commonI18NService.getCurrency(USD));
        } catch (Exception e) {
            catchedException = e;
        }

        // Verify
        assertNull(catchedException);
    }
    
    //*** negative cases ****/
    @Test
    public void testProduct1HasNoClusterCodeForEur()
    {
        // Given
        Exception catchedException = null;

        // Testing
        try {
            createPriceRow(PRODUCT_1, commonI18NService.getCurrency(EUR));
        } catch (Exception e) {
            catchedException = e;
        }

        // Verify
        final Throwable cause = catchedException.getCause();
        final String message = cause.getMessage();
        assertTrue(cause instanceof InterceptorException);
        assertTrue(message.contains(PRODUCT_1));
        assertTrue(message.contains(CLUSTER_CODE_MESSAGE));
    }
    
    @Test
    public void testProduct1HasNoClusterCodeForGbp()
    {
        // Given
        Exception catchedException = null;

        // Testing
        try {
            createPriceRow(PRODUCT_1, commonI18NService.getCurrency(GBP));
        } catch (Exception e) {
            catchedException = e;
        }
        
        // Verify
        final Throwable cause = catchedException.getCause();
        final String message = cause.getMessage();
        assertTrue(cause instanceof InterceptorException);
        assertTrue(message.contains(PRODUCT_1));
        assertTrue(message.contains(CLUSTER_CODE_MESSAGE));
    }

    @Test
    public void testProduct2HasNoClusterCodeForGbp()
    {
        // Given
        Exception catchedException = null;

        // Testing
        try {
            createPriceRow(PRODUCT_2, commonI18NService.getCurrency(GBP));
        } catch (Exception e) {
            catchedException = e;
        }

        // Verify

        final Throwable cause = catchedException.getCause();
        final String message = cause.getMessage();
        assertTrue(cause instanceof InterceptorException);
        assertTrue(message.contains(PRODUCT_2));
        assertTrue(message.contains(CLUSTER_CODE_MESSAGE));
    }

    private PriceRowModel createPriceRow(final String productCode, final CurrencyModel currency)
    {
        final PriceRowModel priceRow = modelService.create(PriceRowModel.class);
        priceRow.setProduct(productService.getProductForCode(productCode));
        priceRow.setUnit(unit);
        priceRow.setCurrency(currency);
        priceRow.setPrice(1.0);

        modelService.save(priceRow);
        return priceRow;
    }
}