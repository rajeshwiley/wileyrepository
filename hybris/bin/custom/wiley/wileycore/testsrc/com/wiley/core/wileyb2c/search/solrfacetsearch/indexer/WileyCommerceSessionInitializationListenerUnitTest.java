package com.wiley.core.wileyb2c.search.solrfacetsearch.indexer;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.storesession.WileyStoreSessionService;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyCommerceSessionInitializationListenerUnitTest
{
	private static final String LANGUAGE_CODE = "en_US";
	@Spy
	@InjectMocks
	private WileyCommerceSessionInitializationListener wileyCommerceSessionInitializationListener;
	@Mock
	private WileyStoreSessionService wileyStoreSessionService;
	@Mock
	private CommonI18NService commonI18NService;
	@Mock
	private FacetSearchConfig facetSearchConfig;
	@Mock
	private IndexedType indexedType;
	@Mock
	private IndexConfig indexConfig;
	@Mock
	private LanguageModel currentLanguage;

	@Test
	public void initializeSession()
	{
		when(facetSearchConfig.getIndexConfig()).thenReturn(indexConfig);
		when(commonI18NService.getCurrentLanguage()).thenReturn(currentLanguage);
		when(currentLanguage.getIsocode()).thenReturn(LANGUAGE_CODE);
		doNothing().when(wileyCommerceSessionInitializationListener).initializePriceFactory();
		doNothing().when(wileyCommerceSessionInitializationListener).initializeMediaUrlStrategy();

		wileyCommerceSessionInitializationListener.initializeSession(facetSearchConfig, indexedType);

	}


}
