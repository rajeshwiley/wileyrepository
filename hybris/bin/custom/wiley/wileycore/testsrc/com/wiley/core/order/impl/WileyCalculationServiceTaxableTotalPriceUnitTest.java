package com.wiley.core.order.impl;


import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.CoreAlgorithms;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.jalo.order.price.JaloPriceFactoryException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Arrays;
import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.price.impl.WileyDiscountCalculationServiceImpl;

import static org.fest.assertions.Assertions.assertThat;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyCalculationServiceTaxableTotalPriceUnitTest
{
	private static final double EPSILON = 0.0001d;

	@InjectMocks
	private WileyCalculationService calculationService;
	@Spy
	private WileyDiscountCalculationServiceImpl wileyDiscountCalculationServiceSpy;
	@Mock
	private ModelService modelServiceMock;

	private CurrencyModel currency;
	private OrderModel order;

	@Before
	public void setup()
	{
		currency = new CurrencyModel();
		currency.setDigits(2);

		order = new OrderModel();
		order.setCurrency(currency);
	}

	@Test
	public void shouldNotFailOnEmptyEntries() throws JaloPriceFactoryException
	{
		//Given

		order.setEntries(Collections.emptyList());

		//When
		calculationService.calculateTaxableTotalPrice(order);

	}

	@Test
	public void shouldCalculatedTotalTaxablePriceForOneEntry() throws JaloPriceFactoryException
	{
		//Given
		order.setTotalDiscounts(5.55d);
		order.setSubtotal(10.05d);

		OrderEntryModel entry1 = new OrderEntryModel();
		entry1.setTotalPrice(10.05d);
		entry1.setOrder(order);

		order.setEntries(Arrays.asList(entry1));

		double expectedSubtotalWithoutDisocunt = CoreAlgorithms.round(order.getSubtotal() - order.getTotalDiscounts(),
				currency.getDigits());

		//When
		calculationService.calculateTaxableTotalPrice(order);

		//Then
		assertThat(expectedSubtotalWithoutDisocunt).isEqualTo(entry1.getTaxableTotalPrice());
	}

	/**
	 * Test:
	 * 1) Checks case when two taxable prices has rounding issue(i.e. from 1.015 to 1.02)
	 * that causes one additional cent which is sent to taxable system
	 * 2) Checks calculation of two entries
	 */
	@Test
	public void shouldCalculatedTotalTaxablePriceNotAddingCentDueToRoundingIssue() throws JaloPriceFactoryException
	{
		//Given
		order.setTotalDiscounts(11.11d);
		order.setSubtotal(20.10d);

		OrderEntryModel entry1 = new OrderEntryModel();
		entry1.setTotalPrice(10.05d);
		entry1.setOrder(order);

		OrderEntryModel entry2 = new OrderEntryModel();
		entry2.setTotalPrice(10.05d);
		entry2.setOrder(order);

		order.setEntries(Arrays.asList(entry1, entry2));

		double expectedSubtotalMinusDiscount = order.getSubtotal() - order.getTotalDiscounts();

		//When
		calculationService.calculateTaxableTotalPrice(order);

		//Then
		double sumTaxableTotalPrice = entry1.getTaxableTotalPrice() + entry2.getTaxableTotalPrice();
		double roundingDifference = Math.abs(expectedSubtotalMinusDiscount - sumTaxableTotalPrice);
		assertThat(roundingDifference).isLessThan(EPSILON);
	}
}
