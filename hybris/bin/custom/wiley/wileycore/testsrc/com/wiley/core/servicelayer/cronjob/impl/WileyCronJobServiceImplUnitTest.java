package com.wiley.core.servicelayer.cronjob.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyCronJobServiceImplUnitTest
{
	@Mock
	private ModelService modelServiceMock;

	@InjectMocks
	private WileyCronJobServiceImpl cronJobService;

	@Test
	public void getCronJobByPk()
	{
		final PK pk = PK.fromLong(1L);
		final CronJobModel cronJobModel = new CronJobModel();
		when(modelServiceMock.get(pk)).thenReturn(cronJobModel);
		final CronJobModel cronJobByPk = cronJobService.getCronJobByPk(pk);
		Assert.assertEquals(cronJobModel, cronJobByPk);
	}

	@Test
	public void getCronJobByLongPk()
	{
		final long pk = 1L;
		final CronJobModel cronJobModel = new CronJobModel();
		when(modelServiceMock.get(PK.fromLong(pk))).thenReturn(cronJobModel);
		final CronJobModel cronJobByPk = cronJobService.getCronJobByPk(pk);
		Assert.assertEquals(cronJobModel, cronJobByPk);
	}

	@Test
	public void getNullByPk()
	{
		final PK pk = PK.fromLong(1L);
		when(modelServiceMock.get(pk)).thenReturn(null);
		final CronJobModel cronJobByPk = cronJobService.getCronJobByPk(pk);
		Assert.assertNull(cronJobByPk);
	}

	@Test
	public void getNullByLongPk()
	{
		final long pk = 1L;
		when(modelServiceMock.get(PK.fromLong(pk))).thenReturn(null);
		final CronJobModel cronJobByPk = cronJobService.getCronJobByPk(pk);
		Assert.assertNull(cronJobByPk);
	}

	@Test
	public void getNonCronJobByPk()
	{
		final PK pk = PK.fromLong(1L);
		final ItemModel itemModel = new ItemModel();
		when(modelServiceMock.get(pk)).thenReturn(itemModel);
		final CronJobModel cronJobByPk = cronJobService.getCronJobByPk(pk);
		Assert.assertNull(cronJobByPk);
	}

	@Test
	public void getNonCronJobByLongPk()
	{
		final long pk = 1L;
		final ItemModel itemModel = new ItemModel();
		when(modelServiceMock.get(PK.fromLong(pk))).thenReturn(itemModel);
		final CronJobModel cronJobByPk = cronJobService.getCronJobByPk(pk);
		Assert.assertNull(cronJobByPk);
	}
}