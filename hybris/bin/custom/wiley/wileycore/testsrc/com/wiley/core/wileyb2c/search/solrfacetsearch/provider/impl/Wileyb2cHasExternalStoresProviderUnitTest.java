package com.wiley.core.wileyb2c.search.solrfacetsearch.provider.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyWebLinkModel;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cHasExternalStoresProviderUnitTest
{
	@InjectMocks
	private Wileyb2cHasExternalStoresProvider wileyb2cHasExternalStoresProvider;

	@Mock
	private ProductModel productModel;
	@Mock
	private WileyWebLinkModel store;

	@Test
	public void collectValuesWhenProductAndHasExternalStoresShouldReturnTrue() throws FieldValueProviderException
	{
		when(productModel.getExternalStores()).thenReturn(Collections.singleton(store));

		final List<String> values = wileyb2cHasExternalStoresProvider.collectValues(null, null, productModel);

		assertEquals(values.size(), 1);
		assertEquals(values.get(0), "true");
	}

	@Test
	public void collectValuesWhenProductAndNoExternalStoresShouldReturnFalse() throws FieldValueProviderException
	{
		when(productModel.getExternalStores()).thenReturn(Collections.emptyList());

		final List<String> values = wileyb2cHasExternalStoresProvider.collectValues(null, null, productModel);

		assertEquals(values.size(), 1);
		assertEquals(values.get(0), "false");
	}

	@Test
	public void collectValuesWhenNonProductShouldReturnEmptyList() throws FieldValueProviderException
	{
		final List<String> values = wileyb2cHasExternalStoresProvider.collectValues(null, null, 1);

		assertEquals(values.size(), 0);
	}

}
