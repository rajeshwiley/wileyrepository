package com.wiley.core.wel.preorder.service.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.enums.WileyProductLifecycleEnum;
import com.wiley.core.model.WileyVariantProductModel;
import com.wiley.core.wel.preorder.dao.impl.WelProductReferencesDaoImpl;
import com.wiley.core.wel.preorder.strategy.impl.WelProductLifecycleStatusStrategyImpl;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WelPreOrderServiceImplUnitTest
{
	public static final String TEST_ACTIVE_PRODUCT = "TEST_ACTIVE_PRODUCT";
	public static final String TEST_PRE_ORDER_PRODUCT = "TEST_PRE_ORDER_PRODUCT";

	@Mock
	private ProductModel testPreOrderProduct;

	@Mock
	private ProductModel testActiveProduct;

	@Mock
	private CartModel cartModelMock;

	@Mock
	private WileyVariantProductModel wileyVariantProductModel;

	@Mock
	private AbstractOrderEntryModel orderEntryModelMock;

	@Mock
	private ProductReferenceModel testProductReference;

	@InjectMocks
	private WelPreOrderServiceImpl welPreOrderService;

	@Mock
	private WelProductReferencesDaoImpl welProductReferencesDao;

	@Mock
	private WelProductLifecycleStatusStrategyImpl welProductLifecycleStatusStrategy;

	List<ProductModel> activeProducts = new ArrayList<>();
	final List<ProductReferenceModel> testProductReferences = new ArrayList<>();

	@Before
	public void setUp() throws Exception
	{
		when(testActiveProduct.getCode()).thenReturn(TEST_ACTIVE_PRODUCT);
		when(testActiveProduct.getLifecycleStatus()).thenReturn(WileyProductLifecycleEnum.ACTIVE);
		when(testPreOrderProduct.getCode()).thenReturn(TEST_PRE_ORDER_PRODUCT);
		when(testProductReference.getSource()).thenReturn(testActiveProduct);
		when(testProductReference.getTarget()).thenReturn(testPreOrderProduct);
		when(testProductReference.getReferenceType()).thenReturn(ProductReferenceTypeEnum.NEXT_VERSION);

		testProductReferences.add(testProductReference);
		when(testActiveProduct.getProductReferences()).thenReturn(testProductReferences);

		activeProducts.add(testActiveProduct);
	}

	@Test
	public void testSuccessfullyGettingProductByPreOrderProduct()
	{
		when(testPreOrderProduct.getLifecycleStatus()).thenReturn(WileyProductLifecycleEnum.PRE_ORDER);
		when(welProductReferencesDao
				.findSourceReferenceProducts(testPreOrderProduct, ProductReferenceTypeEnum.NEXT_VERSION))
				.thenReturn(activeProducts);

		ProductModel activeProduct = welPreOrderService.getActiveProductForPreOrderProduct(testPreOrderProduct).get();

		assertNotNull(activeProduct);
		assertEquals(WileyProductLifecycleEnum.ACTIVE, activeProduct.getLifecycleStatus());
		assertEquals(TEST_ACTIVE_PRODUCT, activeProduct.getCode());
		final Optional<ProductReferenceModel> preOrderProduct = activeProduct.getProductReferences()
				.stream()
				.filter(productReferenceModel -> productReferenceModel.getReferenceType()
						== ProductReferenceTypeEnum.NEXT_VERSION)
				.findFirst();
		assertTrue(preOrderProduct.isPresent());
		assertEquals(TEST_PRE_ORDER_PRODUCT, preOrderProduct.get().getTarget().getCode());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGettingProductByNotPreOrderProduct()
	{
		when(testPreOrderProduct.getLifecycleStatus()).thenReturn(WileyProductLifecycleEnum.ACTIVE);

		ProductModel activeProduct = welPreOrderService.getActiveProductForPreOrderProduct(testPreOrderProduct).get();
	}

	@Test
	public void testGettingProductForPreOrderProductWithoutActiveProduct()
	{
		when(testPreOrderProduct.getLifecycleStatus()).thenReturn(WileyProductLifecycleEnum.PRE_ORDER);
		activeProducts = new ArrayList<>();
		when(welProductReferencesDao
				.findSourceReferenceProducts(testPreOrderProduct, ProductReferenceTypeEnum.NEXT_VERSION))
				.thenReturn(activeProducts);

		ProductModel activeProduct = welPreOrderService.getActiveProductForPreOrderProduct(testPreOrderProduct).
				orElse(null);

		assertNull(activeProduct);
	}



	@Test
	public void testIsPreOrderPositive()
	{
		// Given
		when(cartModelMock.getEntries()).thenReturn(Arrays.asList(orderEntryModelMock));
		when(orderEntryModelMock.getProduct()).thenReturn(wileyVariantProductModel);
		when(wileyVariantProductModel.getBaseProduct()).thenReturn(testPreOrderProduct);
		when(welProductLifecycleStatusStrategy.getLifecycleStatusForProduct(wileyVariantProductModel))
				.thenReturn(WileyProductLifecycleEnum.PRE_ORDER);
		// When
		boolean result = welPreOrderService.isPreOrder(cartModelMock);
		// Then
		assertTrue(result);
	}
	@Test
	public void testIsPreOrderNegative()
	{
		// When
		boolean result = welPreOrderService.isPreOrder(cartModelMock);
		// Then
		assertFalse(result);
	}
}
