package com.wiley.core.product.interceptors;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.enums.ProductEditionFormat;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ProductEditionFormatInterceptorUnitTest
{

	private ProductEditionFormatInterceptor testInstance = new ProductEditionFormatInterceptor();

	@Test(expected = InterceptorException.class)
	public void onPrepareShouldThrowInterceptorExceptionForWelProductCatalogIfEditionFormatIsNull() throws Exception
	{
		final String welProductCatalogId = "welProductCatalog";
		ProductModel testProduct = givenProduct(welProductCatalogId, null);
		//When
		testInstance.onValidate(testProduct, null);
	}

	@Test
	public void onPrepareShouldIgnoreAgsProductCatalogIfEditionFormatIsNull() throws Exception
	{
		final String welProductCatalogId = "agsProductCatalog";
		ProductModel testProduct = givenProduct(welProductCatalogId, null);
		//When
		testInstance.onValidate(testProduct, null);
	}


	@Test
	public void onPrepareShouldNotThrowInterceptorExceptionForWelProductCatalogIfEditionFormatIsNotNull() throws Exception
	{
		final String welProductCatalogId = "welProductCatalog";
		ProductModel testProduct = givenProduct(welProductCatalogId, ProductEditionFormat.DIGITAL);
		//When
		testInstance.onValidate(testProduct, null);
	}

	@Test
	public void onPrepareShouldNotThrowInterceptorExceptionForAgsProductCatalogIfEditionFormatIsNotNull() throws Exception
	{
		final String welProductCatalogId = "agsProductCatalog";
		ProductModel testProduct = givenProduct(welProductCatalogId, ProductEditionFormat.DIGITAL);
		//When
		testInstance.onValidate(testProduct, null);
	}

	@Test
	public void onPrepareShouldNotValidateForOtherCatalogs() throws Exception
	{
		final String welProductCatalogId = "wileyProductCatalog";
		ProductModel testProduct = givenProduct(welProductCatalogId, null);
		//When
		testInstance.onValidate(testProduct, null);
	}

	private ProductModel givenProduct(final String catalogId, final ProductEditionFormat editionFormat)
	{
		ProductModel productModel = mock(ProductModel.class);
		CatalogVersionModel catalogVersion = givenCatalogVersionModel(catalogId);
		when(productModel.getCatalogVersion()).thenReturn(catalogVersion);
		when(productModel.getEditionFormat()).thenReturn(editionFormat);
		return productModel;
	}

	private CatalogVersionModel givenCatalogVersionModel(final String catalogId)
	{
		CatalogModel catalogModel = mock(CatalogModel.class);
		when(catalogModel.getId()).thenReturn(catalogId);

		CatalogVersionModel catalogVersionModel = mock(CatalogVersionModel.class);
		when(catalogVersionModel.getCatalog()).thenReturn(catalogModel);

		return catalogVersionModel;
	}

}