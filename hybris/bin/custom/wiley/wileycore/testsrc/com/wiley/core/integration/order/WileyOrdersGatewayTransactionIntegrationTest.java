package com.wiley.core.integration.order;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.ServicelayerTest;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.springframework.transaction.IllegalTransactionStateException;


@IntegrationTest
public class WileyOrdersGatewayTransactionIntegrationTest extends ServicelayerTest
{
	@Resource
	private WileyOrdersGateway wileyOrdersGateway;

	@Test(expected = IllegalTransactionStateException.class)
	public void shouldThrowExceptionWithoutTransaction()
	{
		wileyOrdersGateway.publish(new OrderModel(), StringUtils.EMPTY);
	}

}
