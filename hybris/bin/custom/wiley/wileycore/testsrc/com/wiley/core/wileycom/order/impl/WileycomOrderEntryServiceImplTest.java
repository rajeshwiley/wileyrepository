package com.wiley.core.wileycom.order.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Author Herman_Chukhrai (EPAM)
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileycomOrderEntryServiceImplTest
{

	@InjectMocks
	private WileycomOrderEntryServiceImpl wileycomOrderEntryService;

	@Mock
	private OrderModel orderModel;
	@Mock
	private OrderEntryModel entry1Mock;
	@Mock
	private OrderEntryModel entry2Mock;
	@Mock
	private ProductModel productModel1Mock;
	@Mock
	private ProductModel productModel2Mock;

	private static final String ISBN_1 = "ISBN_1";
	private static final String ISBN_2 = "ISBN_2";
	private static final String ISBN_3 = "ISBN_3";

	@Before
	public void setUp() throws Exception
	{
		// given
		when(orderModel.getEntries()).thenReturn(Arrays.asList(entry1Mock, entry2Mock));
		when(entry1Mock.getProduct()).thenReturn(productModel1Mock);
		when(entry2Mock.getProduct()).thenReturn(productModel2Mock);
		when(productModel1Mock.getIsbn()).thenReturn(ISBN_1);
		when(productModel2Mock.getIsbn()).thenReturn(ISBN_2);
	}

	@Test
	public void testFindOrderEntry1() throws Exception
	{
		//when
		final Optional<AbstractOrderEntryModel> entry = wileycomOrderEntryService.findOrderEntryByIsbn(orderModel, ISBN_1);

		//then
		assertNotNull(entry);
		assertTrue(entry.isPresent());
		assertEquals(productModel1Mock, entry.get().getProduct());
	}

	@Test
	public void testFindOrderEntry2() throws Exception
	{
		//when
		final Optional<AbstractOrderEntryModel> entry = wileycomOrderEntryService.findOrderEntryByIsbn(orderModel, ISBN_2);

		//then
		assertNotNull(entry);
		assertTrue(entry.isPresent());
		assertEquals(productModel2Mock, entry.get().getProduct());
	}

	@Test
	public void testOrderNotFound() throws Exception
	{
		//when
		final Optional<AbstractOrderEntryModel> entry = wileycomOrderEntryService.findOrderEntryByIsbn(orderModel, ISBN_3);

		//then
		assertNotNull(entry);
		assertFalse(entry.isPresent());
	}

	@Test(expected = IllegalStateException.class)
	public void testSameProductShouldNotBeUsedInMultipleOrderEntries() throws Exception
	{
		//given
		when(productModel2Mock.getIsbn()).thenReturn(ISBN_1);
		//when
		wileycomOrderEntryService.findOrderEntryByIsbn(orderModel, ISBN_1);
	}
}