package com.wiley.core.wileycom.order.impl;

import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.i18n.WileyCountryService;
import com.wiley.core.servicelayer.i18n.WileycomI18NService;
import com.wiley.core.wiley.order.impl.WileyCommerceAddToCartStrategyImpIntegrationTest;
import com.wiley.core.wiley.restriction.exception.ProductNotVisibleException;

import reactor.util.Assert;


/**
 * @author Dzmitryi_Halahayeu
 */
@Ignore
public abstract class WileycomCommerceAddToCartStrategyImpIntegrationTest extends WileyCommerceAddToCartStrategyImpIntegrationTest
{
	private static final String US_ISO_CODE = "US";
	private static final String PRODUCT_CODE = "WCOM_PRODUCT";
	private static final String NON_PURCHASABLE_PRODUCT = "WCOM_NON_PURCHASABLE_PRODUCT";
	private static final String CANADA_ISO_CODE = "CA";
	private static final String DEFAULT_ENCODING = "UTF-8";

	@Resource
	private UserService userService;
	@Resource
	private CommonI18NService commonI18NService;
	@Resource
	private BaseSiteService baseSiteService;
	@Resource
	private ProductService productService;
	@Resource
	private WileycomI18NService wileycomI18NService;
	@Resource
	private WileyCountryService wileyCountryService;

	@Before
	public void setUp() throws Exception
	{
		importCsv("/wileycore/test/product/WileycomCommerceAddToCartStrategyImpIntegrationTest/testProducts.impex",
				DEFAULT_ENCODING);

		baseSiteService.setCurrentBaseSite(getSiteId(), true);
		wileycomI18NService.setCurrentCountry(wileyCountryService.findCountryByCode(US_ISO_CODE));
	}

	@Test
	public void testFailWhenCountryRestrictionApply() throws CommerceCartModificationException, ImpExException
	{
		importCsv("/wileycore/test/product/WileycomCommerceAddToCartStrategyImpIntegrationTest/testProductsPrices.impex",
				DEFAULT_ENCODING);
		wileycomI18NService.setCurrentCountry(wileyCountryService.findCountryByCode(CANADA_ISO_CODE));
		final ProductModel usProduct = productService.getProductForCode(PRODUCT_CODE);
		final CommerceCartParameter commerceCartParameter = createCartParameter(
				usProduct);

		boolean isFailed = false;
		try
		{
			getAddToCartStrategy().addToCart(commerceCartParameter);
		}
		catch (final CommerceCartModificationException exception)
		{
			isFailed = true;
			checkException(exception, WileyCoreConstants.PRODUCT_IS_NOT_PURCHASABLE_FOR_COUNTRY,
					"not available due to country check");
		}

		assertTrue(isFailed);
	}

	@Test
	public void testFailWhenAssortmentRestrictionApply() throws CommerceCartModificationException
	{
		final ProductModel b2cProduct = productService.getProductForCode(getRestrictedAssortmentProductCode());
		baseSiteService.setCurrentBaseSite(getSiteId(), true);
		final CommerceCartParameter commerceCartParameter = createCartParameter(b2cProduct);

		boolean isFailed = false;
		try
		{
			getAddToCartStrategy().addToCart(commerceCartParameter);
		}
		catch (final CommerceCartModificationException exception)
		{
			isFailed = true;
			checkException(exception, WileyCoreConstants.PRODUCT_IS_NOT_PURCHASABLE, "due to assortment check");
		}

		assertTrue(isFailed);
	}

	@Test
	@Ignore("[ECSC-20075] ignored test which tests obsolete attribute. Should be deleted")
	public void testFailWhenNonPurchasableRestrictionApply() throws CommerceCartModificationException
	{
		final ProductModel nonPurchasableProduct = productService.getProductForCode(NON_PURCHASABLE_PRODUCT);
		final CommerceCartParameter commerceCartParameter = createCartParameter(nonPurchasableProduct);

		boolean isFailed = false;
		try
		{
			getAddToCartStrategy().addToCart(commerceCartParameter);
		}
		catch (final CommerceCartModificationException exception)
		{
			isFailed = true;
			checkException(exception, WileyCoreConstants.PRODUCT_IS_NOT_PURCHASABLE, "is not purchasable");
		}

		assertTrue(isFailed);
	}



	@Test
	@Ignore("Should be fixed in scope of ECSC-23296")
	public void testDoNotFailWhenPurchasableHaveSameCountryAndAssortment() throws CommerceCartModificationException
	{
		final ProductModel product = productService.getProductForCode(PRODUCT_CODE);

		final CommerceCartParameter commerceCartParameter = createCartParameter(product);

		final CommerceCartModification commerceCartModification = getAddToCartStrategy().addToCart(
				commerceCartParameter);

		Assert.notNull(commerceCartModification);
	}

	protected void checkException(final CommerceCartModificationException exception, final String messageCode,
			final String message)
	{
		final Throwable cause = exception.getCause();
		assertNotNull(cause);
		assertEquals(cause.getClass(), ProductNotVisibleException.class);
		assertTrue(exception.getMessage().contains(message));
		final ProductNotVisibleException productNotVisibleException = (ProductNotVisibleException) cause;
		assertEquals(productNotVisibleException.getErrorMessageCode(), messageCode);
	}

	@Override
	protected String getRestrictedUserGroupProductCode()
	{
		return "WCOM_USER_GROUP_RESTRICTED";
	}

	protected abstract String getSiteId();

	protected abstract String getRestrictedAssortmentProductCode();
}
