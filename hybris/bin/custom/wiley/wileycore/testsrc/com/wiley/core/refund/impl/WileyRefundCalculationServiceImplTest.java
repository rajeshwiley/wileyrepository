package com.wiley.core.refund.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyGiftCardProductModel;
import com.wiley.core.model.WileyProductModel;
import com.wiley.core.payment.transaction.PaymentTransactionService;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyRefundCalculationServiceImplTest
{
	public static final long ENTRY_QUANTITY = 2L;
	public static final long GIFT_CARD_ENTRY_QUANTITY = 3L;

	@InjectMocks
	private WileyRefundCalculationServiceImpl testedInstance = new WileyRefundCalculationServiceImpl();

	@Mock
	private PaymentTransactionModel transaction;
	@Mock
	private PaymentTransactionService transactionService;
	@Mock
	private PaymentTransactionEntryModel captureTransactionEntry;
	@Mock
	private AbstractOrderEntryModel orderEntry;
	@Mock
	private AbstractOrderEntryModel giftCardOrderEntry;
	@Mock
	private WileyGiftCardProductModel giftCardProduct;
	@Mock
	private WileyProductModel product;

	@Before
	public void setUp()
	{
		when(giftCardOrderEntry.getProduct()).thenReturn(giftCardProduct);
		when(orderEntry.getProduct()).thenReturn(product);
		when(giftCardOrderEntry.getQuantity()).thenReturn(GIFT_CARD_ENTRY_QUANTITY);
		when(orderEntry.getQuantity()).thenReturn(ENTRY_QUANTITY);
	}

	/**
	 * test of calculateMaximumAvailableRefundAmountForOrder method
	 */
	@Test
	public void shouldBeCaptureTransactionTotalMinusShippingCostMinusRefundedAmount()
	{
		//Given
		Double orderTotal = 100d;
		Double refund1 = 10d;
		Double refund2 = 11d;
		BigDecimal expectedRefundableAmount = BigDecimal.valueOf(orderTotal)
				.subtract(BigDecimal.valueOf(refund1))
				.subtract(BigDecimal.valueOf(refund2));
		OrderModel orderModel = givenOrderModel(orderTotal, refund1, refund2);
		//Testing
		BigDecimal maxAvailableRefundAmount = testedInstance.calculateMaximumAvailableRefundAmountForOrder(orderModel);
		//Verify
		assertTrue(0 == expectedRefundableAmount.compareTo(maxAvailableRefundAmount));
	}

	/**
	 * test of calculateMaximumAvailableRefundAmountForOrder method
	 */
	@Test
	public void shouldWorkIfDeliveryCostIsNull()
	{
		//Given
		Double orderTotal = 100d;
		Double refund1 = 10d;
		Double refund2 = 11d;
		BigDecimal expectedRefundableAmount = BigDecimal.valueOf(orderTotal)
				.subtract(BigDecimal.valueOf(refund1))
				.subtract(BigDecimal.valueOf(refund2));
		OrderModel orderModel = givenOrderModel(orderTotal, refund1, refund2);
		//Testing
		BigDecimal maxAvailableRefundAmount = testedInstance.calculateMaximumAvailableRefundAmountForOrder(orderModel);
		//Verify
		assertTrue(0 == expectedRefundableAmount.compareTo(maxAvailableRefundAmount));
	}

	@Test
	public void shouldSubtractGiftCardAmountFromMaximumRefundAmount()
	{
		//Given
		Double orderTotal = 100d;
		Double giftCardPrice = 20d;
		BigDecimal giftCardTotalPrice = BigDecimal.valueOf(giftCardPrice * GIFT_CARD_ENTRY_QUANTITY);
		BigDecimal expectedRefundableAmount = BigDecimal.valueOf(orderTotal).subtract(giftCardTotalPrice);

		OrderModel orderModel = givenOrderModel(orderTotal);
		when(orderModel.getEntries()).thenReturn(Arrays.asList(orderEntry, giftCardOrderEntry));
		when(giftCardOrderEntry.getBasePrice()).thenReturn(giftCardPrice);

		//When
		BigDecimal maxAvailableRefundAmount = testedInstance.calculateMaximumAvailableRefundAmountForOrder(orderModel);
		//Then
		assertEquals(expectedRefundableAmount.doubleValue(), maxAvailableRefundAmount.doubleValue());
	}

	@Test
	public void shouldProcessAllTransactions()
	{
		//Given
		Double transactionTotal1 = 100d;
		Double transactionTotal2 = 100d;
		Double refund1 = 100d;
		BigDecimal expectedRefundableAmount = BigDecimal.valueOf(transactionTotal1)
				.subtract(BigDecimal.valueOf(refund1))
				.add(BigDecimal.valueOf(transactionTotal2));
		OrderModel orderModel = givenOrderModelWithTwoTransactions(transactionTotal1, transactionTotal2, refund1);
		//Testing
		BigDecimal maxAvailableRefundAmount = testedInstance.calculateMaximumAvailableRefundAmountForOrder(orderModel);
		//Verify
		assertTrue(0 == expectedRefundableAmount.compareTo(maxAvailableRefundAmount));
	}

	private OrderModel givenOrderModel(final Double orderTotal, final Double... refundEntriesCost)
	{
		OrderModel orderModel = mock(OrderModel.class);
		PaymentTransactionModel paymentTransactionModel = mock(PaymentTransactionModel.class);

		final PaymentTransactionEntryModel captureTransactionEntry =
				givenTransactionEntryForTypeAndAmount(PaymentTransactionType.CAPTURE, orderTotal);
		when(transactionService.getAcceptedTransactionEntries(orderModel, PaymentTransactionType.CAPTURE))
				.thenReturn(Collections.singletonList(captureTransactionEntry));
		when(captureTransactionEntry.getPaymentTransaction()).thenReturn(paymentTransactionModel);

		List<PaymentTransactionEntryModel> refundEntries = Stream.of(refundEntriesCost)
				.map(entryCost -> givenTransactionEntryForTypeAndAmount(PaymentTransactionType.REFUND_FOLLOW_ON, entryCost))
				.collect(Collectors.toList());
		when(transactionService.getAcceptedTransactionEntries(paymentTransactionModel, PaymentTransactionType.REFUND_FOLLOW_ON))
				.thenReturn(refundEntries);

		return orderModel;
	}

	private OrderModel givenOrderModelWithTwoTransactions(final Double transactionTotal1, final Double transactionTotal2,
			final Double... refundEntriesCost)
	{
		OrderModel orderModel = mock(OrderModel.class);
		// first payment transaction
		PaymentTransactionModel paymentTransactionModel1 = mock(PaymentTransactionModel.class);

		final PaymentTransactionEntryModel captureTransactionEntry1 =
				givenTransactionEntryForTypeAndAmount(PaymentTransactionType.CAPTURE, transactionTotal1);
		when(captureTransactionEntry1.getPaymentTransaction()).thenReturn(paymentTransactionModel1);

		List<PaymentTransactionEntryModel> refundEntries = Stream.of(refundEntriesCost)
				.map(entryCost -> givenTransactionEntryForTypeAndAmount(PaymentTransactionType.REFUND_FOLLOW_ON, entryCost))
				.collect(Collectors.toList());
		// first transaction is fully refunded
		when(transactionService.getAcceptedTransactionEntries(paymentTransactionModel1, PaymentTransactionType.REFUND_FOLLOW_ON))
				.thenReturn(refundEntries);

		// second payment transaction
		PaymentTransactionModel paymentTransactionModel2 = mock(PaymentTransactionModel.class);
		final PaymentTransactionEntryModel captureTransactionEntry2 =
				givenTransactionEntryForTypeAndAmount(PaymentTransactionType.CAPTURE, transactionTotal2);
		when(captureTransactionEntry2.getPaymentTransaction()).thenReturn(paymentTransactionModel2);

		when(transactionService.getAcceptedTransactionEntries(orderModel, PaymentTransactionType.CAPTURE))
				.thenReturn(Arrays.asList(captureTransactionEntry1, captureTransactionEntry2));

		return orderModel;
	}

	private PaymentTransactionEntryModel givenTransactionEntryForTypeAndAmount(final PaymentTransactionType type,
			final Double orderTotal)
	{
		PaymentTransactionEntryModel captureTransaction = mock(PaymentTransactionEntryModel.class);
		when(captureTransaction.getType()).thenReturn(type);
		when(captureTransaction.getTransactionStatus()).thenReturn(TransactionStatus.ACCEPTED.name());
		when(captureTransaction.getAmount()).thenReturn(new BigDecimal(orderTotal));
		return captureTransaction;
	}
}