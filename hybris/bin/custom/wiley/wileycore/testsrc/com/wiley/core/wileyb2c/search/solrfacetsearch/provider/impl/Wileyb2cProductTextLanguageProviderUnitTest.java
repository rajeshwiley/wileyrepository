package com.wiley.core.wileyb2c.search.solrfacetsearch.provider.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyPurchaseOptionProductModel;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cProductTextLanguageProviderUnitTest
{
	private static final String ISO_CODE = "isoCode";
	@InjectMocks
	private Wileyb2cProductTextLanguageProvider wileyb2cProductTextLanguageProvider;
	@Mock
	private WileyPurchaseOptionProductModel wileyPurchaseOptionProductModel;
	@Mock
	private ProductModel baseProduct;
	@Mock
	private IndexConfig indexConfig;
	@Mock
	private IndexedProperty indexedProperty;
	@Mock
	private LanguageModel languageModel;

	@Before
	public void setUp() throws Exception
	{
		when(wileyPurchaseOptionProductModel.getBaseProduct()).thenReturn(baseProduct);
	}

	@Test
	public void collectValuesWhenLanguageNotNullShouldReturnEmptyLanguage() throws FieldValueProviderException
	{
		when(baseProduct.getTextLanguage()).thenReturn(languageModel);
		when(languageModel.getIsocode()).thenReturn(ISO_CODE);

		final List<String> values = wileyb2cProductTextLanguageProvider.collectValues(indexConfig, indexedProperty,
				wileyPurchaseOptionProductModel);

		Assert.assertEquals(values.size(), 1);
		Assert.assertEquals(values.get(0), ISO_CODE);
	}

	@Test
	public void collectValuesWhenLanguageNullShouldReturnEmptyList() throws FieldValueProviderException
	{
		final List<String> values = wileyb2cProductTextLanguageProvider.collectValues(indexConfig, indexedProperty,
				wileyPurchaseOptionProductModel);

		Assert.assertTrue(values.isEmpty());
	}
}
