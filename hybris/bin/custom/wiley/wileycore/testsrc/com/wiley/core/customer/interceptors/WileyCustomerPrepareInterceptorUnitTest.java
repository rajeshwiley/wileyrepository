package com.wiley.core.customer.interceptors;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.strategies.CustomerNameStrategy;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * Unit test for {@link WileyCustomerPrepareInterceptor}
 *
 * @author Maksim_Kozich
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyCustomerPrepareInterceptorUnitTest {
    public static final String FIRST_NAME = "First_Name";
    public static final String LAST_NAME = "Last_Name";
    public static final String CONCATENATED_NAME = "First_Name Last_Name";
    public static final String FIRST_NAME_TWO_WORDS = "some name";

    @Mock
    private CustomerNameStrategy customerNameStrategy;

    @Mock
    private CustomerModel customerModel;

    @Mock
    private InterceptorContext interceptorContext;

    @InjectMocks
    private WileyCustomerPrepareInterceptor interceptor;

    /**
     * Sets up.
     */
    @Before
    public void setUp() {
        when(customerNameStrategy.splitName(CONCATENATED_NAME)).thenReturn(new String[]{FIRST_NAME, LAST_NAME});
        when(customerNameStrategy.getName(FIRST_NAME, LAST_NAME)).thenReturn(CONCATENATED_NAME);
    }

    /**
     * Tests that customer name is set properly.
     *
     * @throws InterceptorException
     */
    @Test
    public void testPrepareEmptyName() throws InterceptorException {
        when(customerModel.getFirstName()).thenReturn(FIRST_NAME);
        when(customerModel.getLastName()).thenReturn(LAST_NAME);

        interceptor.onPrepare(customerModel, interceptorContext);

        verify(customerNameStrategy).getName(eq(FIRST_NAME), eq(LAST_NAME));
        verify(customerModel).setName(eq(CONCATENATED_NAME));
        verify(customerModel).setCustomerID(anyString());
    }

    @Test
    public void testPrepareNotEmptyName() throws InterceptorException {
        when(customerModel.getName()).thenReturn(CONCATENATED_NAME);
        when(customerModel.getFirstName()).thenReturn(FIRST_NAME);
        when(customerModel.getLastName()).thenReturn(LAST_NAME);

        interceptor.onPrepare(customerModel, interceptorContext);
        verify(customerNameStrategy, never()).getName(eq(FIRST_NAME), eq(LAST_NAME));
        verify(customerModel, never()).setName(any());
        verify(customerModel).setCustomerID(anyString());
    }

    @Test
    public void testPrepareNotEmptyNameEmptyFirstName() throws InterceptorException {
        when(customerModel.getName()).thenReturn(CONCATENATED_NAME);
        when(customerModel.getFirstName()).thenReturn(null);
        when(customerModel.getLastName()).thenReturn(LAST_NAME);

        interceptor.onPrepare(customerModel, interceptorContext);
        verify(customerModel).setFirstName(FIRST_NAME);
        verify(customerModel, never()).setLastName(any());
    }

    @Test
    public void testPrepareNotEmptyNameEmptyLastName() throws InterceptorException {
        when(customerModel.getName()).thenReturn(CONCATENATED_NAME);
        when(customerModel.getFirstName()).thenReturn(FIRST_NAME);
        when(customerModel.getLastName()).thenReturn(null);

        interceptor.onPrepare(customerModel, interceptorContext);
        verify(customerModel, never()).setFirstName(any());
        verify(customerModel).setLastName(LAST_NAME);
    }

    @Test
    public void testPrepareNotEmptyNameEmptyFirstLastName() throws InterceptorException {
        when(customerModel.getName()).thenReturn(CONCATENATED_NAME);
        when(customerModel.getFirstName()).thenReturn(null);
        when(customerModel.getLastName()).thenReturn(null);

        interceptor.onPrepare(customerModel, interceptorContext);
        verify(customerModel).setFirstName(FIRST_NAME);
        verify(customerModel).setLastName(LAST_NAME);
    }

    @Test
    public void testPrepareEmptyNameEmptyFirstLastName() throws InterceptorException {
        when(customerModel.getName()).thenReturn(null);
        when(customerModel.getFirstName()).thenReturn(null);
        when(customerModel.getLastName()).thenReturn(null);

        interceptor.onPrepare(customerModel, interceptorContext);
        verify(customerModel, never()).setFirstName(any());
        verify(customerModel, never()).setLastName(any());
    }

    @Test
    public void testPrepareEmptyNameEmptyLastNameTwoWordFirstName() throws InterceptorException {
        when(customerNameStrategy.getName(FIRST_NAME_TWO_WORDS, StringUtils.EMPTY)).thenReturn(CONCATENATED_NAME);
        when(customerModel.getName()).thenReturn(null);
        when(customerModel.getFirstName()).thenReturn(FIRST_NAME_TWO_WORDS);
        when(customerModel.getLastName()).thenReturn(StringUtils.EMPTY);

        interceptor.onPrepare(customerModel, interceptorContext);
        verify(customerModel).setName(CONCATENATED_NAME);
        verify(customerModel, never()).setFirstName(any());
        verify(customerModel, never()).setLastName(any());
    }

    @Test
    public void testPrepareEmptyNameNotEmptyLastName() throws InterceptorException {
        when(customerModel.getName()).thenReturn(null);
        when(customerModel.getFirstName()).thenReturn(null);
        when(customerModel.getLastName()).thenReturn(LAST_NAME);

        interceptor.onPrepare(customerModel, interceptorContext);
        verify(customerModel, never()).setFirstName(any());
        verify(customerModel, never()).setLastName(any());
    }
}
