package com.wiley.core.payment.strategies.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.payment.data.WPGHttpValidateResultData;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.site.BaseSiteService;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.payment.strategies.SecurityHashGeneratorStrategy;

import static com.wiley.core.payment.constants.PaymentConstants.WPG.OPERATION_AUTH;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WPGValidateResponseSecurityValidationStrategyImplTest
{
	private static final String OPERATION = OPERATION_AUTH;
	private static final String RETURN_CODE = "0";
	private static final String RETURN_MESSAGE = "SUCCESS";
	private static final String VENDOR_ID = "204";
	private static final String TRANSACTION_ID = "1448275448283";
	private static final String MERCHANT_RESPONSE = "APPROVED";
	private static final String MERCHANT_ID = "2292388873";
	private static final String CSC_RESULT = "OK";
	private static final String AVS_ADDRESS_RESULT = "X";
	private static final String AVS_POST_RESULT = "X";
	private static final String TOKEN = "T344058642196266";
	private static final String ACQUIRER_ID = "A";
	private static final String ACQUIRER_NAME = "AMEX";
	private static final String BANK_ID = "U";
	private static final String BANK_NAME = "UNKNOWN";
	private static final String MASKED_CARD_NUMBER = "344058xxxxx6266";
	private static final String CARD_EXPIRY = "0517";
	private static final String TIMESTAMP = "1448275461";
	private static final String SECURITY_HASH = "8b8eee6b6ee9a5fb1417fa01b8fd6bc7";
	private static final String INVALID_SECURITY_HASH = "8b8eee6b6ee9a5fb1417fa01b8fd6bc8";

	private static final String TEST_SITE_ID = "testSiteId";
	private static final String SECURITY_BASE_STRING = OPERATION + RETURN_CODE + RETURN_MESSAGE
			+ VENDOR_ID + TRANSACTION_ID + MERCHANT_RESPONSE + MERCHANT_ID + CSC_RESULT + AVS_ADDRESS_RESULT
			+ AVS_POST_RESULT + TOKEN + ACQUIRER_ID + ACQUIRER_NAME + BANK_ID + BANK_NAME
			+ MASKED_CARD_NUMBER + CARD_EXPIRY + TIMESTAMP;


	@Mock
	private SecurityHashGeneratorStrategy mockSecurityHashGeneratorStrategy;

	@Mock
	private BaseSiteService mockBaseSiteService;
	@Mock
	private BaseSiteModel mockBaseSite;

	@InjectMocks
	private WPGValidateResponseSecurityValidationStrategyImpl testedInstance =
			new WPGValidateResponseSecurityValidationStrategyImpl()
			{
				@Override
				public SecurityHashGeneratorStrategy getSecurityHashGeneratorStrategy()
				{
					return mockSecurityHashGeneratorStrategy;
				}
			};
	private WPGHttpValidateResultData resultData = new WPGHttpValidateResultData();

	@Before
	public void setUp()
	{
		when(mockBaseSiteService.getCurrentBaseSite()).thenReturn(mockBaseSite);
		when(mockBaseSite.getUid()).thenReturn(TEST_SITE_ID);

		resultData.setOperation(OPERATION);
		resultData.setReturnCode(RETURN_CODE);
		resultData.setReturnMessage(RETURN_MESSAGE);
		resultData.setVendorID(VENDOR_ID);
		resultData.setTransID(TRANSACTION_ID);
		resultData.setMerchantResponse(MERCHANT_RESPONSE);
		resultData.setMerchantID(MERCHANT_ID);
		resultData.setCSCResult(CSC_RESULT);
		resultData.setAVSAddrResult(AVS_ADDRESS_RESULT);
		resultData.setAVSPostResult(AVS_POST_RESULT);
		resultData.setToken(TOKEN);
		resultData.setAcquirerID(ACQUIRER_ID);
		resultData.setAcquirerName(ACQUIRER_NAME);
		resultData.setBankID(BANK_ID);
		resultData.setBankName(BANK_NAME);
		resultData.setMaskedCardNumber(MASKED_CARD_NUMBER);
		resultData.setCardExpiry(CARD_EXPIRY);
		resultData.setTimestamp(TIMESTAMP);
		resultData.setSecurity(SECURITY_HASH);
	}

	@Test
	public void shouldReturnSuccessfulResult()
	{
		when(mockSecurityHashGeneratorStrategy.generateSecurityHash(TEST_SITE_ID, SECURITY_BASE_STRING)).thenReturn(
				SECURITY_HASH);

		assertTrue(testedInstance.validate(resultData));
	}

	@Test
	public void shouldReturnFalse()
	{
		when(mockSecurityHashGeneratorStrategy.generateSecurityHash(TEST_SITE_ID, SECURITY_BASE_STRING)).thenReturn(
				INVALID_SECURITY_HASH);

		assertFalse(testedInstance.validate(resultData));
	}
}