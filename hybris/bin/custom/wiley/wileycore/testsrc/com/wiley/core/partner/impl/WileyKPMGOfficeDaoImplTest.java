package com.wiley.core.partner.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.ImmutableMap;
import com.wiley.core.model.KpmgOfficeModel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyKPMGOfficeDaoImplTest
{
	private static final String PARTNER_ID = "KPMG";
	private static final String SELECT_OFFICES_BY_PARTNER_ID_QUERY = "SELECT {office.pk} "
			+ "FROM {KpmgOffice AS office "
			+ "JOIN WileyPartnerCompany AS partner "
			+ "ON {office.partner} = {partner.pk}} "
			+ "WHERE {partner.uid} = ?partnerId";

	private static final String OFFICE_CODE = "E090394";
	private static final String SELECT_OFFICE_BY_CODE = "SELECT {office.pk} "
			+ "FROM {KpmgOffice AS office} "
			+ "WHERE {office.code} = ?officeCode";

	@InjectMocks
	private WileyKPMGOfficeDaoImpl testedInstance = new WileyKPMGOfficeDaoImpl();

	@Mock
	private FlexibleSearchService flexibleSearchService;
	@Mock
	private KpmgOfficeModel firstOffice;
	@Mock
	private KpmgOfficeModel secondOffice;
	@Mock
	private SearchResult<KpmgOfficeModel> searchResult;

	@Test
	public void findOfficesShouldReturnListOfOffices()
	{
		//Given
		when(flexibleSearchService.<KpmgOfficeModel>search(
				SELECT_OFFICES_BY_PARTNER_ID_QUERY, ImmutableMap.of("partnerId", PARTNER_ID))).thenReturn(searchResult);
		when(searchResult.getResult()).thenReturn(Arrays.asList(firstOffice, secondOffice));
		//When
		final List<KpmgOfficeModel> offices = testedInstance.findOffices(PARTNER_ID);
		//Then
		assertEquals(Arrays.asList(firstOffice, secondOffice), offices);
	}

	@Test
	public void findOfficesShouldReturnEmptyListWhenResultListIsNull()
	{
		//Given
		when(flexibleSearchService.<KpmgOfficeModel>search(
				SELECT_OFFICES_BY_PARTNER_ID_QUERY, ImmutableMap.of("partnerId", PARTNER_ID))).thenReturn(searchResult);
		when(searchResult.getResult()).thenReturn(null);
		//When
		final List<KpmgOfficeModel> offices = testedInstance.findOffices(PARTNER_ID);
		//Then
		assertTrue(offices.isEmpty());
	}

	@Test
	public void findOfficeByCodeShouldReturnFirstOfficeFromSearchResult()
	{
		//Given
		when(flexibleSearchService.<KpmgOfficeModel>search(
				SELECT_OFFICE_BY_CODE, ImmutableMap.of("officeCode", OFFICE_CODE))).thenReturn(searchResult);
		when(searchResult.getResult()).thenReturn(Arrays.asList(firstOffice, secondOffice));
		//When
		final KpmgOfficeModel office = testedInstance.findOfficeByCode(OFFICE_CODE);
		//Then
		assertEquals(firstOffice, office);
	}

	@Test
	public void findOfficeByCodeShouldReturnNullIfResultListIsEmpty()
	{
		//Given
		when(flexibleSearchService.<KpmgOfficeModel>search(
				SELECT_OFFICE_BY_CODE, ImmutableMap.of("officeCode", OFFICE_CODE))).thenReturn(searchResult);
		when(searchResult.getResult()).thenReturn(Arrays.asList());
		//When
		final KpmgOfficeModel office = testedInstance.findOfficeByCode(OFFICE_CODE);
		//Then
		assertNull(office);
	}

}