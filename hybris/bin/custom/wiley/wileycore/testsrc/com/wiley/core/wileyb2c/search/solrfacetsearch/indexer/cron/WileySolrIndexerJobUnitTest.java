package com.wiley.core.wileyb2c.search.solrfacetsearch.indexer.cron;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.CronJobService;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfigService;
import de.hybris.platform.solrfacetsearch.enums.IndexerOperationValues;
import de.hybris.platform.solrfacetsearch.indexer.IndexerService;
import de.hybris.platform.solrfacetsearch.indexer.exceptions.IndexerException;
import de.hybris.platform.solrfacetsearch.model.config.SolrFacetSearchConfigModel;
import de.hybris.platform.solrfacetsearch.model.indexer.cron.SolrIndexerCronJobModel;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMapOf;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileySolrIndexerJobUnitTest
{
	private static final String CONFIG_NAME = "test";
	private static final String ANOTHER_CONFIG_NAME = "test2";
	private static final String JOB_CODE = "jobCode";
	private static final String ANOTHER_JOB_CODE = "jobCode2";
	@InjectMocks
	private WileySolrIndexerJob wileySolrIndexerJob;
	@Mock
	private CronJobModel cronJobModel;
	@Mock
	private SolrIndexerCronJobModel solrIndexerCronJobModel;
	@Mock
	private CronJobService cronJobService;
	@Mock
	private SolrFacetSearchConfigModel solrFacetSearchConfig;
	@Mock
	private SolrFacetSearchConfigModel anotherFacetSeachConfig;
	@Mock
	private FacetSearchConfigService facetSearchConfigService;
	@Mock
	private FacetSearchConfig facetSearchConfig;
	@Mock
	private IndexerService indexerService;
	@Mock
	private CronJobModel runningJob;
	@Mock
	private SolrIndexerCronJobModel runningSolJob;
	@Mock
	private ModelService modelService;

	@Before
	public void setUp() throws Exception
	{
		when(facetSearchConfigService.getConfiguration(CONFIG_NAME)).thenReturn(facetSearchConfig);
		when(solrIndexerCronJobModel.getIndexerOperation()).thenReturn(IndexerOperationValues.FULL);
		when(solrIndexerCronJobModel.getFacetSearchConfig()).thenReturn(solrFacetSearchConfig);
		when(solrFacetSearchConfig.getName()).thenReturn(CONFIG_NAME);
		when(solrIndexerCronJobModel.getCode()).thenReturn(JOB_CODE);
		when(runningSolJob.getFacetSearchConfig()).thenReturn(anotherFacetSeachConfig);
		when(runningSolJob.getCode()).thenReturn(ANOTHER_JOB_CODE);
	}

	@Test
	public void performIndexingJobWhenNonSolrJobShouldReturnFailure()
	{
		final PerformResult performResult = wileySolrIndexerJob.performIndexingJob(cronJobModel);

		assertEquals(performResult.getResult(), CronJobResult.FAILURE);
	}

	@Test
	public void performIndexingJobWhenRunningNonSolrJobShouldIndex() throws IndexerException
	{
		when(cronJobService.getRunningOrRestartedCronJobs()).thenReturn(Collections.singletonList(runningJob));

		final PerformResult performResult = wileySolrIndexerJob.performIndexingJob(solrIndexerCronJobModel);

		verify(indexerService).performFullIndex(any(), anyMapOf(String.class, String.class));
		assertEquals(performResult.getResult(), CronJobResult.SUCCESS);
	}

	@Test
	public void performIndexingJobWhenRunningSolrJobAndSelfShouldIndex() throws IndexerException
	{
		when(cronJobService.getRunningOrRestartedCronJobs()).thenReturn(Collections.singletonList(solrIndexerCronJobModel));

		final PerformResult performResult = wileySolrIndexerJob.performIndexingJob(solrIndexerCronJobModel);

		verify(indexerService).performFullIndex(any(), anyMapOf(String.class, String.class));
		assertEquals(performResult.getResult(), CronJobResult.SUCCESS);
	}

	@Test
	public void performIndexingJobWhenRunningSolrJobForOtherIndexShouldIndex() throws IndexerException
	{
		when(cronJobService.getRunningOrRestartedCronJobs()).thenReturn(Collections.singletonList(runningSolJob));
		when(anotherFacetSeachConfig.getName()).thenReturn(ANOTHER_CONFIG_NAME);

		final PerformResult performResult = wileySolrIndexerJob.performIndexingJob(solrIndexerCronJobModel);

		verify(indexerService).performFullIndex(any(), anyMapOf(String.class, String.class));
		assertEquals(performResult.getResult(), CronJobResult.SUCCESS);
	}
}
