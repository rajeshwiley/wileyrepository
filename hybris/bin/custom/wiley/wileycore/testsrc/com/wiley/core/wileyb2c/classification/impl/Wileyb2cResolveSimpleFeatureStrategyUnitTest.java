package com.wiley.core.wileyb2c.classification.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureValue;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.wileyb2c.classification.Wileyb2cClassificationAttributes;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cResolveSimpleFeatureStrategyUnitTest
{
	private static final String TEST_VALUE = "test";
	@InjectMocks
	private Wileyb2cResolveSimpleFeatureStrategy wileyb2cResolveSimpleFeatureStrategy;
	@Mock
	private Feature feature;
	@Mock
	private FeatureValue featureValue;


	@Test
	public void processFeature() throws ParseException
	{
		when(feature.getValue()).thenReturn(featureValue);
		when(featureValue.getValue()).thenReturn(TEST_VALUE);

		final String processFeature = wileyb2cResolveSimpleFeatureStrategy.processFeature(feature);

		assertEquals(processFeature, TEST_VALUE);
	}

	@Test
	public void getAttributes()
	{
		final Set<Wileyb2cClassificationAttributes> attributes = wileyb2cResolveSimpleFeatureStrategy.getAttributes();

		assertEquals(attributes, Wileyb2cResolveSimpleFeatureStrategy.CLASSIFICATION_ATTRIBUTES);
	}
}
