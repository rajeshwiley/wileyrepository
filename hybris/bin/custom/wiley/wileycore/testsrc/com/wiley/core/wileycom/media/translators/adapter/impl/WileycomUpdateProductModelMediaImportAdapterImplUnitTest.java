package com.wiley.core.wileycom.media.translators.adapter.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.impex.jalo.header.UnresolvedValueException;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileycomUpdateProductModelMediaImportAdapterImplUnitTest
{
	private static final String PRODUCT_CODE = "productCode";
	private static final String MEDIA_CODE = "mediaCode";
	@Mock
	private ModelService modelService;
	@Mock
	private ProductService productService;
	@Mock
	private CatalogVersionService catalogVersionService;
	@Mock
	private Item item;
	@InjectMocks
	private WileycomUpdateProductModelMediaImportAdapterImpl wileycomUpdateProductModelMediaImportAdapter;

	@Test
	public void performImport() throws UnresolvedValueException
	{
		MediaModel mediaModel = new MediaModel();
		mediaModel.setCode(MEDIA_CODE);
		ProductModel productModel = new ProductModel();
		when(modelService.get(item)).thenReturn(mediaModel);
		when(productService.getProductForCode(any(CatalogVersionModel.class), eq(PRODUCT_CODE))).thenReturn(productModel);

		wileycomUpdateProductModelMediaImportAdapter.performImport(PRODUCT_CODE, item);

		Assert.assertEquals(productModel.getPicture().getCode(), MEDIA_CODE);
	}


}
