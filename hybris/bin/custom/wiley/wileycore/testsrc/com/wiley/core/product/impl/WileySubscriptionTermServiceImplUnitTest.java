package com.wiley.core.product.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.subscriptionservices.model.SubscriptionTermModel;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static junit.framework.Assert.assertEquals;


/**
 * Test for {@link WileySubscriptionTermServiceImpl}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileySubscriptionTermServiceImplUnitTest
{

	private static final String TEST_ID_PARAMETER = "testid";

	@InjectMocks
	private WileySubscriptionTermServiceImpl wileySubscriptionTermService;

	@Mock(name = "subscriptionTermGenericDao")
	private DefaultGenericDao<SubscriptionTermModel> subscriptionTermGenericDaoMock;

	@Test(expected = IllegalStateException.class)
	public void shouldThrowIllegalStateExceptionDueToNullParameter()
	{
		//Given
		//no additional setup

		//When
		wileySubscriptionTermService.getSubscriptionTerm(null);
	}

	@Test(expected = IllegalStateException.class)
	public void shouldThrowIllegalStateExceptionDueToEmptyParameter()
	{
		//Given
		//no additional setup

		//When
		wileySubscriptionTermService.getSubscriptionTerm(StringUtils.EMPTY);
	}

	@Test
	public void shouldReturnItem()
	{
		//Given
		SubscriptionTermModel expectedSubscriptionTerm = mock(SubscriptionTermModel.class);
		when(subscriptionTermGenericDaoMock.find(Collections.singletonMap(SubscriptionTermModel.ID, TEST_ID_PARAMETER)))
				.thenReturn(Collections.singletonList(expectedSubscriptionTerm));

		//When
		SubscriptionTermModel actualSubscriptionTerm = wileySubscriptionTermService.getSubscriptionTerm(TEST_ID_PARAMETER);

		//Then
		assertEquals(expectedSubscriptionTerm, actualSubscriptionTerm);
	}


	@Test(expected = UnknownIdentifierException.class)
	public void shouldThrowUnknownIdentifierException()
	{
		//Given
		when(subscriptionTermGenericDaoMock.find(Collections.singletonMap(SubscriptionTermModel.ID, TEST_ID_PARAMETER)))
				.thenReturn(Collections.EMPTY_LIST);

		//When
		wileySubscriptionTermService.getSubscriptionTerm(TEST_ID_PARAMETER);
	}

	@Test(expected = AmbiguousIdentifierException.class)
	public void shouldThrowAmbiguousIdentifierException()
	{
		//Given
		List<SubscriptionTermModel> daoFoundTerms = Arrays.asList(mock(SubscriptionTermModel.class),
				mock(SubscriptionTermModel.class));
		when(subscriptionTermGenericDaoMock.find(Collections.singletonMap(SubscriptionTermModel.ID, TEST_ID_PARAMETER)))
				.thenReturn(daoFoundTerms);

		//When
		wileySubscriptionTermService.getSubscriptionTerm(TEST_ID_PARAMETER);
	}

}
