package com.wiley.core.payment.strategies.impl;

import com.wiley.core.order.WileyCartService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.payment.cybersource.enums.TransactionTypeEnum;
import de.hybris.platform.acceleratorservices.payment.data.OrderInfoData;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyCreateSubscriptionRequestStrategyTest
{

	private OrderInfoData orderInfoDataMock;

	@InjectMocks
	private WileyCreateSubscriptionRequestStrategy testedInstance;

	@Mock
	private WileyCartService wileyCartService;


	@Before
	public void setUp()
	{

	}

	@Test
	public void shouldInvokePopulateFromCurrentCart()
	{
		testedInstance.getRequestOrderInfoData(TransactionTypeEnum.authorization);
		verify(wileyCartService).populateFromCurrentCart(any());
	}

}