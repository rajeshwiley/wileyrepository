package com.wiley.core.wileyb2c.classification.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeModel;
import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.classification.features.FeatureValue;
import de.hybris.platform.core.model.product.ProductModel;

import java.text.ParseException;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.wileyb2c.classification.Wileyb2cClassificationAttributes;


/**
 * @author Tanmoy_Mondal
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cResolveMultivaluedFeatureStrategyUnitTest {

  private static final String TEST_MULTI_VALUE = "societyLink";

  @InjectMocks
  private Wileyb2cResolveMultivaluedFeatureStrategy wileyb2cResolveMultivaluedFeatureStrategy;
  @Mock
  private Feature feature;
  @Mock
  private ProductModel productModel;
  @Mock
  private AbstractWileyb2cResolveClassificationAttributeStrategy abstractClassificationService;
  @Mock
  private ClassificationService classificationService;
  @Mock
  private ClassAttributeAssignmentModel classAttributeAssignmentModel;
  @Mock
  private ClassificationAttributeModel classificationAttributeModel;

  private List<FeatureValue> multivaluedList;
  private FeatureValue featureValue;
  private List<Feature> features;

  @Before
  public void setUp() 
  {
    featureValue = new FeatureValue(TEST_MULTI_VALUE);
    multivaluedList = Arrays.asList(featureValue);
    when(feature.getValues()).thenReturn(multivaluedList);
    features = Arrays.asList(feature);
    
    when(feature.getValue()).thenReturn(featureValue);
    when(feature.getClassAttributeAssignment()).thenReturn(classAttributeAssignmentModel);
    when(classAttributeAssignmentModel.getClassificationAttribute())
        .thenReturn(classificationAttributeModel);
    when(classificationAttributeModel.getCode()).thenReturn(TEST_MULTI_VALUE);
    when(classificationService.getFeatures(productModel)).thenReturn(new FeatureList(features));
    when(abstractClassificationService.getFeatureByAttribute(productModel,
        Wileyb2cClassificationAttributes.SOCIETY_LINK)).thenReturn(feature);
  }

  @Test
  public void shouldProcessFeature() throws ParseException 
  {
    final String processFeature = wileyb2cResolveMultivaluedFeatureStrategy.processFeature(feature);
    assertEquals(processFeature, multivaluedList.toString());
  }

  @Test
  public void shouldResolveAttributeValueWhenClassificationAttributeExist() 
  {
    final List<String> actualMultivalueList = wileyb2cResolveMultivaluedFeatureStrategy
        .resolveAttributeValue(productModel, Wileyb2cClassificationAttributes.SOCIETY_LINK);

    assertEquals(multivaluedList, actualMultivalueList);
  }

}
