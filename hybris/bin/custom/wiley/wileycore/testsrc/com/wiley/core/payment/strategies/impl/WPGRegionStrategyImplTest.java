package com.wiley.core.payment.strategies.impl;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WPGRegionStrategyImplTest
{
	private static final String USD = "USD";
	private static final String OTHER_CURRENCY = "JPY";
	private static final String NA_REGION = "USD";

	private WPGRegionStrategyImpl testedInstance = new WPGRegionStrategyImpl();
	private Map<String, String> regionsByCurrency = new HashMap<>();

	@Test
	public void shouldReturnRegionByCurrency()
	{
		testedInstance.setRegionsByCurrency(regionsByCurrency);

		regionsByCurrency.put(USD, NA_REGION);

		final String region = testedInstance.getRegionByCurrency(USD);

		assertEquals(NA_REGION, region);
	}

	@Test
	public void shouldReturnEmptyString()
	{
		testedInstance.setRegionsByCurrency(regionsByCurrency);
		regionsByCurrency.put(USD, NA_REGION);

		final String region = testedInstance.getRegionByCurrency(OTHER_CURRENCY);

		assertEquals(StringUtils.EMPTY, region);
	}
}