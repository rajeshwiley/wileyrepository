package com.wiley.core.payment.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.model.WileyTnsMerchantModel;
import com.wiley.core.payment.tnsmerchant.dao.WileyTnsMerchantDao;


@IntegrationTest
public class WileyTnsMerchantServiceImplIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	private static final String IMPEX_PATH = "/wileycore/test/payment/WileyTnsMerchantServiceImplIntegrationTest/";
	private static final String TNS_MERCHANT_IMPEX = IMPEX_PATH + "tnsMerchant.impex";

	private static final String COUNTRY_ISO_CODE_DE = "DE";
	private static final String COUNTRY_ISO_CODE_US = "US";
	private static final String CURRENCY_ISO_CODE = "EUR";
	private static final String TNS_MERCHANT_ID = "TESTWLYHYB-EMEA";

	@Resource
	private WileyTnsMerchantDao wileyTnsMerchantDao;

	@Before
	public void setUp() throws Exception
	{
		importCsv(TNS_MERCHANT_IMPEX, DEFAULT_ENCODING);
	}

	@Test
	public void shouldReturnMerchantId()
	{
		final List<WileyTnsMerchantModel> list = wileyTnsMerchantDao.findTnsMerchant(COUNTRY_ISO_CODE_DE, CURRENCY_ISO_CODE);
		assertFalse(list.isEmpty());
		final WileyTnsMerchantModel wileyTnsMerchantModel = list.get(0);
		assertNotNull(wileyTnsMerchantModel);
		assertEquals(TNS_MERCHANT_ID, wileyTnsMerchantModel.getId());
	}

	@Test
	public void shouldNotReturnMerchantId()
	{
		final List<WileyTnsMerchantModel> list = wileyTnsMerchantDao.findTnsMerchant(COUNTRY_ISO_CODE_US, CURRENCY_ISO_CODE);
		assertTrue(list.isEmpty());
	}
}
