package com.wiley.core.wileyb2c.sitemap.job;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.enums.SiteMapPageEnum;
import de.hybris.platform.acceleratorservices.model.SiteMapConfigModel;
import de.hybris.platform.acceleratorservices.model.SiteMapMediaCronJobModel;
import de.hybris.platform.acceleratorservices.model.SiteMapPageModel;
import de.hybris.platform.acceleratorservices.sitemap.generator.SiteMapGenerator;
import de.hybris.platform.basecommerce.strategies.ActivateBaseSiteInSessionStrategy;
import de.hybris.platform.catalog.model.CatalogUnawareMediaModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.core.model.media.MediaFolderModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.internal.util.collections.Sets;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Created by Maksim_Kozich on 31.08.17.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cSiteMapMediaJobUnitTest
{
	private static final String TEST_FOLDER_NAME = "testfolder";
	private static final String TEST_FILE_NAME = "testdile";

	@Mock
	private MediaFolderModel mediaFolderModelMock;

	@Mock
	private Configuration configurationMock;

	@Mock
	private File fileMock;

	@Mock
	private FileInputStream fileInputStreamMock;

	@Mock
	private ModelService modelServiceMock;

	@Mock
	private MediaService mediaServiceMock;

	@Mock
	private ConfigurationService configurationServiceMock;

	@Mock
	private CatalogUnawareMediaModel catalogUnawareMediaModelMock;

	@Mock
	private SiteMapMediaCronJobModel siteMapMediaCronJobModelMock;

	@Mock
	private Logger loggerMock;

	@Mock
	private CMSSiteModel cmsSiteModelMock;

	@Mock
	private SiteMapConfigModel siteMapConfigModelMock;

	@Mock
	private SiteMapPageModel productSiteMapPageModelMock;

	@Mock
	private SiteMapPageModel homepageSiteMapPageModelMock;

	@Mock
	private SiteMapPageModel categorylandingSiteMapPageModelMock;

	@Mock
	private SiteMapPageModel contentSiteMapPageModelMock;

	@Mock
	private SiteMapPageModel customSiteMapPageModelMock;

	@Mock
	private SiteMapGenerator productSiteMapGeneratorModelMock;

	@Mock
	private SiteMapGenerator homepageSiteMapGeneratorModelMock;

	@Mock
	private SiteMapGenerator categorylandingSiteMapGeneratorModelMock;

	@Mock
	private SiteMapGenerator contentSiteMapGeneratorModelMock;

	@Mock
	private SiteMapGenerator customSiteMapGeneratorModelMock;

	@Mock
	private CMSSiteService cmsSiteServiceMock;

	@Mock
	private ActivateBaseSiteInSessionStrategy<CMSSiteModel> activateBaseSiteInSessionMock;

	private Map<SiteMapPageModel, SiteMapPageEnum> siteMapPageModelSiteMapPageEnumMap;

	private Map<SiteMapPageEnum, SiteMapGenerator> siteMapPageEnumSiteMapGeneratorMap;

	@Spy
	@InjectMocks
	private Wileyb2cSiteMapMediaJob testInstance;

	@Test
	public void testMediaFolderSetProperly() throws FileNotFoundException
	{
		// Given
		doReturn(fileInputStreamMock).when(testInstance).getFileInputStream(fileMock);
		when(configurationServiceMock.getConfiguration()).thenReturn(configurationMock);
		when(configurationMock.getString(Wileyb2cSiteMapMediaJob.SITE_MAP_MEDIA_JOB_OUTPUT_FOLDER_PROPERTY_NAME)).thenReturn(
				TEST_FOLDER_NAME);
		when(mediaServiceMock.getFolder(TEST_FOLDER_NAME)).thenReturn(mediaFolderModelMock);
		when(modelServiceMock.create(CatalogUnawareMediaModel.class)).thenReturn(catalogUnawareMediaModelMock);
		when(fileMock.getName()).thenReturn(TEST_FILE_NAME);

		// When
		CatalogUnawareMediaModel result = testInstance.createCatalogUnawareMediaModel(fileMock);

		// Then
		assertSame(catalogUnawareMediaModelMock, result);
		verify(modelServiceMock).save(catalogUnawareMediaModelMock);
		verify(mediaServiceMock).setStreamForMedia(catalogUnawareMediaModelMock, fileInputStreamMock, TEST_FILE_NAME,
				Wileyb2cSiteMapMediaJob.SITE_MAP_MIME_TYPE, mediaFolderModelMock);
		verify(mediaServiceMock, never()).setStreamForMedia(catalogUnawareMediaModelMock, fileInputStreamMock, TEST_FILE_NAME,
				Wileyb2cSiteMapMediaJob.SITE_MAP_MIME_TYPE);
	}

	@Test
	public void testExceptionIsThrownInCaseAnyGeneratorFailed()
	{
		// Given
		initMaps();
		Set<SiteMapPageEnum> badSiteMapPages = Sets.newSet(SiteMapPageEnum.CATEGORYLANDING, SiteMapPageEnum.CONTENT);
		whenPerformMocks(badSiteMapPages);

		// When
		try
		{
			testInstance.perform(siteMapMediaCronJobModelMock);
			fail();
		}
		catch (IllegalArgumentException e)
		{
			// Then
			verifyPerformMocks(badSiteMapPages);

			assertEquals(Wileyb2cSiteMapMediaJob.
					PREPARE_MODEL_LIST_FOR_ONE_OF_SITE_MAP_PAGES_FAILED_MESSAGE, e.getMessage());
		}
	}

	@Test
	public void testExceptionIsNotThrownInCaseNoGeneratorFailed()
	{
		// Given
		initMaps();
		Set<SiteMapPageEnum> noBadSiteMapPages = Sets.newSet();
		whenPerformMocks(noBadSiteMapPages);

		// When
		testInstance.perform(siteMapMediaCronJobModelMock);

		// Then
		verifyPerformMocks(noBadSiteMapPages);
	}

	protected void initMaps()
	{
		siteMapPageModelSiteMapPageEnumMap = new HashMap<>();
		siteMapPageModelSiteMapPageEnumMap.put(productSiteMapPageModelMock, SiteMapPageEnum.PRODUCT);
		siteMapPageModelSiteMapPageEnumMap.put(homepageSiteMapPageModelMock, SiteMapPageEnum.HOMEPAGE);
		siteMapPageModelSiteMapPageEnumMap.put(categorylandingSiteMapPageModelMock, SiteMapPageEnum.CATEGORYLANDING);
		siteMapPageModelSiteMapPageEnumMap.put(contentSiteMapPageModelMock, SiteMapPageEnum.CONTENT);
		siteMapPageModelSiteMapPageEnumMap.put(customSiteMapPageModelMock, SiteMapPageEnum.CUSTOM);

		siteMapPageEnumSiteMapGeneratorMap = new HashMap<>();
		siteMapPageEnumSiteMapGeneratorMap.put(SiteMapPageEnum.PRODUCT, productSiteMapGeneratorModelMock);
		siteMapPageEnumSiteMapGeneratorMap.put(SiteMapPageEnum.HOMEPAGE, homepageSiteMapGeneratorModelMock);
		siteMapPageEnumSiteMapGeneratorMap.put(SiteMapPageEnum.CATEGORYLANDING, categorylandingSiteMapGeneratorModelMock);
		siteMapPageEnumSiteMapGeneratorMap.put(SiteMapPageEnum.CONTENT, contentSiteMapGeneratorModelMock);
		siteMapPageEnumSiteMapGeneratorMap.put(SiteMapPageEnum.CUSTOM, customSiteMapGeneratorModelMock);
	}

	protected void whenPerformMocks(final Set<SiteMapPageEnum> badSiteMapPages)
	{
		when(testInstance.getLogger()).thenReturn(loggerMock);
		when(siteMapMediaCronJobModelMock.getContentSite()).thenReturn(cmsSiteModelMock);
		when(cmsSiteModelMock.getSiteMapConfig()).thenReturn(siteMapConfigModelMock);

		for (Map.Entry<SiteMapPageModel, SiteMapPageEnum> entry : siteMapPageModelSiteMapPageEnumMap.entrySet())
		{
			SiteMapPageModel entryKey = entry.getKey();
			when(entryKey.getCode()).thenReturn(entry.getValue());
			when(entryKey.getActive()).thenReturn(Boolean.TRUE);
		}

		final Collection<SiteMapPageModel> siteMapPages = siteMapPageModelSiteMapPageEnumMap.keySet();
		when(siteMapConfigModelMock.getSiteMapPages()).thenReturn(siteMapPages);


		for (Map.Entry<SiteMapPageEnum, SiteMapGenerator> entry : siteMapPageEnumSiteMapGeneratorMap.entrySet())
		{
			SiteMapPageEnum entryKey = entry.getKey();
			SiteMapGenerator entryValue = entry.getValue();
			when(testInstance.getGeneratorForSiteMapPage(entryKey)).thenReturn(entryValue);
			if (badSiteMapPages.contains(entryKey))
			{
				doThrow(IllegalArgumentException.class).when(testInstance).prepareModelsList(
						eq(siteMapMediaCronJobModelMock), eq(cmsSiteModelMock), eq(siteMapConfigModelMock), any(),
						eq(entryKey), eq(entryValue));
			}
			else
			{
				doNothing().when(testInstance).prepareModelsList(
						eq(siteMapMediaCronJobModelMock), eq(cmsSiteModelMock), eq(siteMapConfigModelMock), any(),
						eq(entryKey), eq(entryValue));
			}
		}
	}

	protected void verifyPerformMocks(final Set<SiteMapPageEnum> badSiteMapPages)
	{
		for (Map.Entry<SiteMapPageEnum, SiteMapGenerator> entry : siteMapPageEnumSiteMapGeneratorMap.entrySet())
		{
			SiteMapPageEnum entryKey = entry.getKey();
			SiteMapGenerator entryValue = entry.getValue();
			verify(testInstance).prepareModelsList(
					eq(siteMapMediaCronJobModelMock), eq(cmsSiteModelMock), eq(siteMapConfigModelMock), any(),
					eq(entryKey), eq(entryValue));
			if (badSiteMapPages.contains(entryKey))
			{
				verify(loggerMock).error(
						eq(String.format(Wileyb2cSiteMapMediaJob.PREPARE_MODEL_LIST_FOR_SITE_MAP_PAGE_FAILED_MESSAGE, entryKey)),
						isA(IllegalArgumentException.class));
			}
			else
			{
				verify(loggerMock, never()).error(
						eq(String.format(Wileyb2cSiteMapMediaJob.PREPARE_MODEL_LIST_FOR_SITE_MAP_PAGE_FAILED_MESSAGE, entryKey)),
						isA(IllegalArgumentException.class));
			}
		}
	}
}
