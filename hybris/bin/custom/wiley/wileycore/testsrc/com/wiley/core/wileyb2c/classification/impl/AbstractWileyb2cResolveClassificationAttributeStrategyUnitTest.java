package com.wiley.core.wileyb2c.classification.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeModel;
import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.classification.features.FeatureValue;
import de.hybris.platform.core.model.product.ProductModel;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.wileyb2c.classification.Wileyb2cClassificationAttributes;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AbstractWileyb2cResolveClassificationAttributeStrategyUnitTest
{
	private static final Wileyb2cClassificationAttributes CLASSIFICATION_ATTRIBUTE =
			Wileyb2cClassificationAttributes.IMPACT_FACTOR;
	private static final Wileyb2cClassificationAttributes WRONG_CLASSIFICATION_ATTRIBUTE =
			Wileyb2cClassificationAttributes.NUMBER_OF_PAGES;
	private static final String PROCESSED_FEATURE = "feature";
	@InjectMocks
	private AbstractWileyb2cResolveClassificationAttributeStrategy wileyb2cResolveClassificationAttributeStrategy =
			new AbstractWileyb2cResolveClassificationAttributeStrategy()
			{
				@Override
				protected String processFeature(final Feature feature)
				{
					return PROCESSED_FEATURE;
				}

				@Override
				protected Set<Wileyb2cClassificationAttributes> getAttributes()
				{
					return Collections.singleton(CLASSIFICATION_ATTRIBUTE);
				}
			};
	@Mock
	private ClassificationService classificationService;
	@Mock
	private ProductModel productModel;
	@Mock
	private Feature feature;
	@Mock
	private ClassAttributeAssignmentModel classificationAttributeAssignment;
	@Mock
	private ClassificationAttributeModel classificationAttribute;
	@Mock
	private FeatureValue testValue;

	@Test
	public void resolveAttributeWhenFeatureShouldProcessIt()
	{
		when(classificationService.getFeatures(productModel)).thenReturn(new FeatureList(feature));
		when(feature.getClassAttributeAssignment()).thenReturn(classificationAttributeAssignment);
		when(classificationAttributeAssignment.getClassificationAttribute()).thenReturn(classificationAttribute);
		when(classificationAttribute.getCode()).thenReturn(CLASSIFICATION_ATTRIBUTE.getCode());
		when(feature.getValue()).thenReturn(testValue);

		final String resolveAttribute = wileyb2cResolveClassificationAttributeStrategy.resolveAttribute(productModel,
				CLASSIFICATION_ATTRIBUTE);

		Assert.assertEquals(resolveAttribute, PROCESSED_FEATURE);
	}

	@Test
	public void resolveAttributeWhenFeatureShouldReturnEmptyString()
	{
		when(classificationService.getFeatures(productModel)).thenReturn(new FeatureList());

		final String resolveAttribute = wileyb2cResolveClassificationAttributeStrategy.resolveAttribute(productModel,
				CLASSIFICATION_ATTRIBUTE);

		Assert.assertEquals(StringUtils.EMPTY, resolveAttribute);
	}

	@Test
	public void applyWhenContainsShouldReturnTrue()
	{
		final boolean apply = wileyb2cResolveClassificationAttributeStrategy.apply(CLASSIFICATION_ATTRIBUTE);

		Assert.assertTrue(apply);
	}

	@Test
	public void applyWhenDoesNotContainsShouldReturnFalse()
	{
		final boolean apply = wileyb2cResolveClassificationAttributeStrategy.apply(WRONG_CLASSIFICATION_ATTRIBUTE);

		Assert.assertFalse(apply);
	}

}
