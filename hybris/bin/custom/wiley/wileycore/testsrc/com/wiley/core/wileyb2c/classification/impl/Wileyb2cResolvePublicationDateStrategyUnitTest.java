package com.wiley.core.wileyb2c.classification.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureValue;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.wileyb2c.classification.Wileyb2cClassificationAttributes;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cResolvePublicationDateStrategyUnitTest
{
	@InjectMocks
	private Wileyb2cResolvePublicationDateStrategy wileyb2cResolvePublicationDateStrategy;
	@Mock
	private Feature feature;
	@Mock
	private FeatureValue featureValue;
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");


	@Test
	public void processFeature() throws ParseException
	{
		when(feature.getValue()).thenReturn(featureValue);
		when(featureValue.getValue()).thenReturn(DATE_FORMAT.parse("2007-12-25"));

		final String processFeature = wileyb2cResolvePublicationDateStrategy.processFeature(feature);

		assertEquals(processFeature, "Dec 2007");
	}

	@Test
	public void getAttributes()
	{
		final Set<Wileyb2cClassificationAttributes> attributes = wileyb2cResolvePublicationDateStrategy.getAttributes();

		assertEquals(attributes, Wileyb2cResolvePublicationDateStrategy.CLASSIFICATION_ATTRIBUTES);
	}
}
