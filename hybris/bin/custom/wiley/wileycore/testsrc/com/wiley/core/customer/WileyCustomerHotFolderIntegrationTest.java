package com.wiley.core.customer;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.jalo.user.UserManager;
import de.hybris.platform.servicelayer.ServicelayerTest;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.Config;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.annotation.Resource;

import org.apache.commons.io.FileUtils;
import org.junit.BeforeClass;
import org.junit.Test;


/**
 * Integration test for checking importing customers via hot-folder.
 */
@IntegrationTest
public class WileyCustomerHotFolderIntegrationTest extends ServicelayerTransactionalTest
{
	private static String hotFolderPath;

	@Resource
	private UserService userService;


	@BeforeClass
	public static void cleanup() throws IOException
	{
		hotFolderPath = Config.getString("wiley.impex.basefolder", null) + "/junit/ebp-customer/";
		FileUtils.cleanDirectory(new File(hotFolderPath));
	}

	@Test
	public void checkImportingOfFileWithNewCustomer() throws Exception
	{
		copyFileToHotFolder("/wileycore/test/customer/wiley_customer-1.csv");
		ExecutorService executor = Executors.newSingleThreadExecutor();
		Future<CustomerModel> future = executor.submit(
				new GetWileyCustomerTask("test1@test.com", userService, Registry.getCurrentTenant()));
		try
		{
			CustomerModel customerModel = future.get(2, TimeUnit.SECONDS);
			assertNotNull(customerModel);
			assertNotNull(customerModel.getCustomerID());
			assertNotNull(customerModel.getEncodedPassword());
		}
		catch (TimeoutException e)
		{
			future.cancel(true);
		}
		executor.shutdownNow();
	}

	@Test
	public void checkImportingOfFileWithNewCustomerWithPassword() throws Exception
	{
		copyFileToHotFolder("/wileycore/test/customer/wiley_customer-4.csv");
		ExecutorService executor = Executors.newSingleThreadExecutor();
		Future<CustomerModel> future = executor.submit(
				new GetWileyCustomerTask("test5@test.com", userService, Registry.getCurrentTenant()));
		try
		{
			CustomerModel customerModel = future.get(2, TimeUnit.SECONDS);
			assertNotNull(customerModel);
			assertNotNull(customerModel.getCustomerID());
			assertNotNull(customerModel.getEncodedPassword());
		}
		catch (TimeoutException e)
		{
			future.cancel(true);
		}
		executor.shutdownNow();
	}

	@Test
	public void checkImportingOfFileWithExistingCustomer() throws Exception
	{
		importCsv("/wileycore/test/customer/impex_wiley_customer.csv", "utf-8");
		copyFileToHotFolder("/wileycore/test/customer/wiley_customer-2.csv");
		ExecutorService executor = Executors.newSingleThreadExecutor();
		Future<CustomerModel> future = executor.submit(
				new GetWileyCustomerTask("test1@test.com", userService, Registry.getCurrentTenant()));
		try
		{
			CustomerModel customerModel = future.get(2, TimeUnit.SECONDS);
			assertNotNull(customerModel);
			assertEquals("testFirstName1", customerModel.getFirstName());
			assertEquals("testLastName1", customerModel.getLastName());
		}
		catch (TimeoutException e)
		{
			future.cancel(true);
		}
		executor.shutdownNow();
	}

	@Test
	public void checkImportProceedCorrectlyIfMeetInvalidCsvRow() throws Exception
	{
		int size = UserManager.getInstance().getAllUsers().size();
		ExecutorService executor = Executors.newSingleThreadExecutor();
		Future<CustomerModel> future = executor.submit(
				new GetWileyCustomerTask("test3@test.com", userService, Registry.getCurrentTenant()));
		try
		{
			CustomerModel customerModel = future.get(2, TimeUnit.SECONDS);
			assertEquals(size + 2, UserManager.getInstance().getAllUsers().size());
			assertNotNull(customerModel);
		}
		catch (TimeoutException e)
		{
			future.cancel(true);
		}
		executor.shutdownNow();
	}

	private void copyFileToHotFolder(final String path) throws IOException
	{
		InputStream inputStream = ServicelayerTest.class.getResourceAsStream(path);
		File dest = new File(hotFolderPath + "wiley_customer-" + (System.currentTimeMillis() / 1000L) + ".csv");
		FileUtils.copyInputStreamToFile(inputStream, dest);
	}
}

