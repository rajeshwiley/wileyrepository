package com.wiley.core.store.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.BaseStoreModel;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Default unit test for {@link WileyBaseStoreServiceImpl}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyBaseStoreServiceImplUnitTest
{

	@Mock
	private BaseSiteService baseSiteServiceMock;

	@InjectMocks
	private WileyBaseStoreServiceImpl wileyBaseStoreService;

	@Mock
	private BaseSiteModel baseSiteModelMock;

	@Mock
	private BaseStoreModel baseStoreModelMock1;

	@Mock
	private BaseStoreModel baseStoreModelMock2;

	private String baseSiteUid = "someBaseSiteUid";

	private String baseStoreUid1 = "baseStore1";

	private String baseStoreUid2 = "baseStore2";

	@Before
	public void setUp() throws Exception
	{
		when(baseSiteServiceMock.getBaseSiteForUID(eq(baseSiteUid))).thenReturn(baseSiteModelMock);
		when(baseStoreModelMock1.getUid()).thenReturn(baseStoreUid1);
		when(baseStoreModelMock2.getUid()).thenReturn(baseStoreUid2);
	}

	@Test
	public void testGetBaseStoreUidForSiteWhenBaseSiteHasFewStores()
	{
		// Given
		when(baseSiteModelMock.getStores()).thenReturn(Arrays.asList(baseStoreModelMock1, baseStoreModelMock2));

		// When
		final String actualBaseStoreUid = wileyBaseStoreService.getBaseStoreUidForSite(baseSiteUid);

		// Then
		assertEquals(baseStoreUid1, actualBaseStoreUid);
	}

	@Test
	public void testGetBaseStoreUidForSiteWhenBaseSiteHasOneStory()
	{
		// Given
		when(baseSiteModelMock.getStores()).thenReturn(Arrays.asList(baseStoreModelMock2));

		// When
		final String actualBaseStoreUid = wileyBaseStoreService.getBaseStoreUidForSite(baseSiteUid);

		// Then
		assertEquals(baseStoreUid2, actualBaseStoreUid);
	}

	@Test
	public void testGetBaseStoreUidForSiteWhenBaseSiteHasNoStories()
	{
		// Given
		when(baseSiteModelMock.getStores()).thenReturn(Collections.emptyList());

		// When
		try
		{
			wileyBaseStoreService.getBaseStoreUidForSite(baseSiteUid);
			fail("Expected " + IllegalStateException.class);
		}
		catch (IllegalStateException e)
		{
			// Then
			// Success
		}
	}

	@Test
	public void testGetBaseStoreUidForSiteWithNullParameter()
	{
		// Given
		// no changes in test data

		// When
		try
		{
			wileyBaseStoreService.getBaseStoreUidForSite(null);
			fail("Expected " + IllegalArgumentException.class);
		}
		catch (IllegalArgumentException e)
		{
			// Then
			// Success
		}
	}

	@Test
	public void testGetBaseStoreUidForSiteWhenSiteIsNotFound()
	{
		// Given
		when(baseSiteServiceMock.getBaseSiteForUID(eq(baseSiteUid))).thenReturn(null);

		// When
		try
		{
			wileyBaseStoreService.getBaseStoreUidForSite(baseSiteUid);
			fail("Expected " + UnknownIdentifierException.class);
		}
		catch (UnknownIdentifierException e)
		{
			// Then
			// Success
		}
	}

	@Test
	public void testGetBaseStoreForBaseSiteWhenBaseSiteHasFewStores()
	{
		// Given
		when(baseSiteModelMock.getStores()).thenReturn(Arrays.asList(baseStoreModelMock1, baseStoreModelMock2));

		// When
		final Optional<BaseStoreModel> actualOptionalBaseStore = wileyBaseStoreService.getBaseStoreForBaseSite(baseSiteModelMock);

		// Then
		assertNotNull(actualOptionalBaseStore);
		assertTrue(actualOptionalBaseStore.isPresent());
		assertSame(baseStoreModelMock1, actualOptionalBaseStore.get());
	}

	@Test
	public void testGetBaseStoreForBaseSiteWhenBaseSiteHasOneStory()
	{
		// Given
		when(baseSiteModelMock.getStores()).thenReturn(Arrays.asList(baseStoreModelMock2));

		// When
		final Optional<BaseStoreModel> actualOptionalBaseStore = wileyBaseStoreService.getBaseStoreForBaseSite(baseSiteModelMock);

		// Then
		assertNotNull(actualOptionalBaseStore);
		assertTrue(actualOptionalBaseStore.isPresent());
		assertSame(baseStoreModelMock2, actualOptionalBaseStore.get());
	}

	@Test
	public void testGetBaseStoreForBaseSiteWhenBaseSiteHasNoStories()
	{
		// Given
		when(baseSiteModelMock.getStores()).thenReturn(Collections.emptyList());

		// When
		final Optional<BaseStoreModel> actualOptionalBaseStore = wileyBaseStoreService.getBaseStoreForBaseSite(baseSiteModelMock);

		// Then
		assertNotNull(actualOptionalBaseStore);
		assertFalse(actualOptionalBaseStore.isPresent());
	}

	@Test
	public void testGetBaseStoreForBaseSiteWithNullParameter()
	{
		// Given
		// no changes in test data

		// When
		try
		{
			wileyBaseStoreService.getBaseStoreForBaseSite(null);
			fail("Expected " + IllegalArgumentException.class);
		}
		catch (IllegalArgumentException e)
		{
			// Then
			// Success
		}
	}
}