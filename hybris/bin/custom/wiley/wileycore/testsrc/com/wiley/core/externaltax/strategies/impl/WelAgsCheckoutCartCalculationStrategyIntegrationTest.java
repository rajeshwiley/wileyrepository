package com.wiley.core.externaltax.strategies.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.site.BaseSiteService;

import javax.annotation.Resource;

import org.junit.Before;

import com.wiley.core.order.impl.WileyCheckoutCartCalculationStrategyIntegrationTest;
import com.wiley.core.wiley.externaltax.strategies.impl.WileyCheckoutCartCalculationStrategy;


/**
 * @author Dzmitryi_Halahayeu
 */
@IntegrationTest
public class WelAgsCheckoutCartCalculationStrategyIntegrationTest extends WileyCheckoutCartCalculationStrategyIntegrationTest
{
	private static final String BASE_SITE = "wel";
	
	@Resource
	private WileyCheckoutCartCalculationStrategy welAgsCommerceCartCalculationStrategy;
	@Resource 
	private BaseSiteService baseSiteService;
	
	@Before
	public void setUp()
	{
		baseSiteService.setCurrentBaseSite(baseSiteService.getBaseSiteForUID(BASE_SITE), true);
	}

	@Override
	protected WileyCheckoutCartCalculationStrategy getCartCalculationStrategy()
	{
		return welAgsCommerceCartCalculationStrategy;
	}

	@Override
	protected String getRestrictedUserGroupProductCode()
	{
		return "WEL_USER_GROUP_RESTRICTED";
	}
}
