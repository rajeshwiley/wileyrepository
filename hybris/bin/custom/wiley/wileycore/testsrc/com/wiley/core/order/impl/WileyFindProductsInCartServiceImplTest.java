package com.wiley.core.order.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.GenericVariantProductModel;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyProductVariantSetModel;
import com.wiley.core.product.WileyProductService;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyFindProductsInCartServiceImplTest
{
	@InjectMocks
	private WileyFindProductsInCartServiceImpl testedInstance = new WileyFindProductsInCartServiceImpl();

	@Mock
	private WileyProductService wileyProductServiceMock;
	@Mock
	private CategoryModel categoryMock;
	@Mock
	private ProductModel productInCart;
	@Mock
	private ProductModel productNotInCart;
	@Mock
	private VariantProductModel variantProductInCart;
	@Mock
	private WileyProductVariantSetModel setCategoryProduct;
	@Mock
	GenericVariantProductModel variantOfSetProduct;
	@Mock
	private VariantProductModel variantProductNotInCart;
	@Mock
	private AbstractOrderModel abstractOrderMock;
	@Mock
	private AbstractOrderEntryModel orderEntryMock;

	@Before
	public void setUp()
	{
		when(abstractOrderMock.getEntries()).thenReturn(Arrays.asList(orderEntryMock));
	}

	@Test
	public void shouldReturnTrueIfCartContainsAtLeastOneProduct()
	{
		// Given
		when(orderEntryMock.getProduct()).thenReturn(productInCart);
		// When
		boolean containsProduct = testedInstance.containsAtLeastOneProduct(abstractOrderMock,
				Arrays.asList(productInCart, productNotInCart));
		// Then
		assertTrue(containsProduct);
	}

	@Test
	public void shouldReturnFalseIfCartContainsNoProductsFromList()
	{
		// Given
		when(orderEntryMock.getProduct()).thenReturn(productInCart);
		// When
		boolean containsProduct = testedInstance.containsAtLeastOneProduct(abstractOrderMock,
				Arrays.asList(productNotInCart, productNotInCart));
		// Then
		assertFalse(containsProduct);
	}

	@Test
	public void shouldReturnFalseIfCartIsEmpty()
	{
		// Given
		when(abstractOrderMock.getEntries()).thenReturn(Arrays.asList());
		// When
		boolean containsProduct = testedInstance.containsAtLeastOneProduct(abstractOrderMock,
				Arrays.asList(productNotInCart, productNotInCart));
		// Then
		assertFalse(containsProduct);
	}

	@Test
	public void shouldReturnFalseIfProductListIsNull()
	{
		// When
		Collection<ProductModel> products = null;
		boolean containsProduct = testedInstance.containsAtLeastOneProduct(abstractOrderMock, products);
		// Then
		assertFalse(containsProduct);
	}


	@Test
	public void shouldReturnTrueIfCartContainsAtLeastOneProductFromCategory()
	{
		// Given
		when(wileyProductServiceMock.getProductsForCategory(categoryMock)).thenReturn(
				Arrays.asList(productInCart, productNotInCart));
		when(orderEntryMock.getProduct()).thenReturn(productInCart);
		// When
		boolean containsProduct = testedInstance.containsAtLeastOneProduct(abstractOrderMock, categoryMock);
		// Then
		assertTrue(containsProduct);
	}

	@Test
	public void shouldReturnTrueIfProductFromCategoryIsSet()
	{
		// Given
		when(wileyProductServiceMock.getProductsForCategory(categoryMock)).thenReturn(
				Arrays.asList(setCategoryProduct));
		when(setCategoryProduct.getProducts()).thenReturn(Arrays.asList(variantOfSetProduct));
		when(orderEntryMock.getProduct()).thenReturn(variantOfSetProduct);
		// When
		boolean containsProduct = testedInstance.containsAtLeastOneProduct(abstractOrderMock, categoryMock);
		// Then
		assertTrue(containsProduct);
	}

	@Test
	public void shouldReturnFalseIfCartContainsNoProductsFromCategory()
	{
		// Given
		when(wileyProductServiceMock.getProductsForCategory(categoryMock)).thenReturn(
				Arrays.asList(productNotInCart, productNotInCart));
		when(orderEntryMock.getProduct()).thenReturn(productInCart);
		// When
		boolean containsProduct = testedInstance.containsAtLeastOneProduct(abstractOrderMock, categoryMock);
		// Then
		assertFalse(containsProduct);
	}

	@Test
	public void shouldReturnFalseIfCategoryIsNull()
	{
		// When
		CategoryModel category = null;
		boolean containsProduct = testedInstance.containsAtLeastOneProduct(abstractOrderMock, category);
		// Then
		assertFalse(containsProduct);
	}

	@Test
	public void shouldReturnTrueIfCartContainsAtLeastOneVariantProduct()
	{
		// Given
		when(productNotInCart.getVariants()).thenReturn(Arrays.asList(variantProductInCart, variantProductNotInCart));
		when(orderEntryMock.getProduct()).thenReturn(variantProductInCart);
		// When
		boolean containsProduct = testedInstance.containsAtLeastOneProduct(abstractOrderMock,
				Arrays.asList(productNotInCart));
		// Then
		assertTrue(containsProduct);
	}


	@Test
	public void shouldReturnFalseIfCartContainsNoVariantProductsFromBaseProductList()
	{
		// Given
		when(productNotInCart.getVariants()).thenReturn(Arrays.asList(variantProductNotInCart, variantProductNotInCart));
		when(orderEntryMock.getProduct()).thenReturn(variantProductInCart);
		// When
		boolean containsProduct = testedInstance.containsAtLeastOneProduct(abstractOrderMock,
				Arrays.asList(productNotInCart));
		// Then
		assertFalse(containsProduct);
	}

	@Test
	public void shouldReturnFalseIfVariantsListIsNull()
	{
		// Given
		when(productNotInCart.getVariants()).thenReturn(null);
		when(orderEntryMock.getProduct()).thenReturn(variantProductInCart);
		// When
		boolean containsProduct = testedInstance.containsAtLeastOneProduct(abstractOrderMock,
				Arrays.asList(productNotInCart, productNotInCart));
		// Then
		assertFalse(containsProduct);
	}
}