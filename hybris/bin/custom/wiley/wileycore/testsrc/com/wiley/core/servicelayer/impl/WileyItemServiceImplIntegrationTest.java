package com.wiley.core.servicelayer.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.servicelayer.type.TypeService;

import java.util.Collection;
import java.util.Collections;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.servicelayer.WileyItemService;

import static org.fest.assertions.Assertions.assertThat;


/**
 * Test for {@link WileyItemService}
 */
@IntegrationTest
public class WileyItemServiceImplIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	private static final String RESOURCE_PREFIX = "/wileycore/test/servicelayer/WileyItemServiceImplIntegrationTest";
	private static final String TEST_NON_CATALOG_ITEM_TYPE = "Currency";
	private static final String TEST_CATALOG_ITEM_TYPE = "CMSParagraphComponent";
	private static final String TEST_CATALOG_ID = "testItemCatalog";
	private static final String TEST_CATALOD_VERSION = "Staged";

	@Resource(name = "wileyItemService")
	private WileyItemService wileyItemService;
	@Resource
	private CatalogVersionService catalogVersionService;
	@Resource
	private TypeService typeService;

	private ComposedTypeModel testType;
	private CatalogVersionModel testCatalogVersion;

	@Before
	public void setUp() throws Exception
	{
		importCsv(RESOURCE_PREFIX + "/catalog.impex", DEFAULT_ENCODING);
		testType = typeService.getComposedTypeForCode(TEST_CATALOG_ITEM_TYPE);
		testCatalogVersion = catalogVersionService.getCatalogVersion(TEST_CATALOG_ID, TEST_CATALOD_VERSION);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowExceptionDueToIllegalComposedType() throws Exception
	{
		//Given
		final ComposedTypeModel nonCatalogType = typeService.getComposedTypeForCode(TEST_NON_CATALOG_ITEM_TYPE);

		//When
		wileyItemService.getCatalogAwareItems(nonCatalogType, Collections.emptyList());
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowExceptionDueToEmptyVersion() throws Exception
	{
		//When
		wileyItemService.getCatalogAwareItems(testType, Collections.emptyList());
	}

	@Test
	public void shouldReturnEmptyList() throws Exception
	{
		//When
		final Collection<ItemModel> items = wileyItemService.getCatalogAwareItems(testType,
				Collections.singletonList(testCatalogVersion));

		//Then
		assertThat(items).isEmpty();
	}

	@Test
	public void shouldReturnCatalogItem() throws Exception
	{
		//Given
		importCsv(RESOURCE_PREFIX + "/item-cmsparagraphcomponent.impex", DEFAULT_ENCODING);

		//When
		final Collection<ItemModel> items = wileyItemService.getCatalogAwareItems(testType,
				Collections.singletonList(testCatalogVersion));

		//Then
		assertThat(items).hasSize(1);
		ItemModel item = items.iterator().next();
		assertThat(item.getItemtype()).isEqualTo(TEST_CATALOG_ITEM_TYPE);
	}

	@Test
	public void shouldReturnCatalogSubtypeItem() throws Exception
	{
		//Given
		importCsv(RESOURCE_PREFIX + "/item-cmstabparagraphcomponent.impex", DEFAULT_ENCODING);

		//When
		final Collection<ItemModel> items = wileyItemService.getCatalogAwareItems(testType,
				Collections.singletonList(testCatalogVersion));

		//Then
		assertThat(items).hasSize(1);
		ItemModel item = items.iterator().next();
		assertThat(item.getItemtype()).isNotEqualTo(TEST_CATALOG_ITEM_TYPE);
		assertThat(typeService.isAssignableFrom(TEST_CATALOG_ITEM_TYPE, item.getItemtype())).isTrue();
	}
}
