package com.wiley.core.category.attributehandler;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.variants.model.VariantCategoryModel;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.wiley.core.enums.VariantCategoryType;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;


@UnitTest
public class TypeVariantCategoryAttributeHandlerUnitTest
{
	private static final String WEL_CPA_TYPE_CODE = "WEL_CPA_TYPE";

	private static final String WEL_CPA_PART = "WEL_CPA_PART";

	private static final String WEL_CPA_LEVEL = "WEL_CPA_LEVEL";

	private static final String WEL_CPA_TYPE_EBOOK = "WEL_CPA_TYPE_EBOOK";
	private TypeVariantCategoryAttributeHandler typeVariantCategoryAttributeHandler = new TypeVariantCategoryAttributeHandler();
	@Mock
	private VariantCategoryModel variantCategoryModel;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testGetTypeVariantCategory()
	{
		// Given
		when(variantCategoryModel.getCode()).thenReturn(WEL_CPA_TYPE_CODE);

		// When
		final VariantCategoryType variantCategoryType = typeVariantCategoryAttributeHandler.get(variantCategoryModel);

		// Then
		assertEquals(VariantCategoryType.TYPE, variantCategoryType);
	}

	@Test
	public void testGetPartVariantCategory()
	{
		// Given
		when(variantCategoryModel.getCode()).thenReturn(WEL_CPA_PART);

		// When
		final VariantCategoryType variantCategoryType = typeVariantCategoryAttributeHandler.get(variantCategoryModel);

		// Then
		assertEquals(VariantCategoryType.PART, variantCategoryType);
	}



	@Test
	public void testGetLevelVariantCategory()
	{
		// Given
		when(variantCategoryModel.getCode()).thenReturn(WEL_CPA_LEVEL);

		// When
		final VariantCategoryType variantCategoryType = typeVariantCategoryAttributeHandler.get(variantCategoryModel);

		// Then
		assertEquals(VariantCategoryType.LEVEL, variantCategoryType);
	}

	@Test
	public void testGetNotDefinedVariantCategory()
	{
		// Given
		when(variantCategoryModel.getCode()).thenReturn(WEL_CPA_TYPE_EBOOK);

		// When
		final VariantCategoryType variantCategoryType = typeVariantCategoryAttributeHandler.get(variantCategoryModel);

		// Then
		assertEquals(null, variantCategoryType);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetVariantCategoryNull()
	{
		// When
		typeVariantCategoryAttributeHandler.get(null);

	}
}
