package com.wiley.core.wileyas.order.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.commerceservices.strategies.CartValidationStrategy;

import javax.annotation.Resource;

import org.junit.Before;

import com.wiley.core.wiley.order.impl.WileyCartValidationStrategy;
import com.wiley.core.wiley.order.impl.WileyCommerceCartValidationStrategyIntegrationTest;


/**
 * @author Maksim_Kozich
 */
@IntegrationTest
public class WileyasCartValidationStrategyIntegrationTest
		extends WileyCommerceCartValidationStrategyIntegrationTest
{
	private static final String DEFAULT_ENCODING = "UTF-8";

	@Resource
	private WileyCartValidationStrategy wileyasCartValidationStrategy;

	@Override
	@Before
	public void setUp() throws Exception
	{
		importCsv("/wileycore/test/product/WileyasCartValidationStrategyIntegrationTest/testProducts.impex",
				DEFAULT_ENCODING);
		super.setUp();
	}

	@Override
	protected String getSiteId()
	{
		return "asSite";
	}

	@Override
	protected CartValidationStrategy getCartValidationStrategy()
	{
		return wileyasCartValidationStrategy;
	}

	@Override
	protected String getRestrictedUserGroupProductCode()
	{
		return "AS_USER_GROUP_RESTRICTED";
	}
}
