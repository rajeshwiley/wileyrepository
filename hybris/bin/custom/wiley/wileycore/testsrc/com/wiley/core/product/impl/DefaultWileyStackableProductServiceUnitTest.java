package com.wiley.core.product.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Unit test for {@link WileyStackableProductServiceImpl}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultWileyStackableProductServiceUnitTest
{
	private ProductModel product;

	@InjectMocks
	private WileyStackableProductServiceImpl wileyStackablePriceProductService;


	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowExceptionDueToNullProduct() throws Exception
	{
		// When
		wileyStackablePriceProductService.hasStackablePrice(null);
	}

	@Test
	public void shouldHaveStackablePrice() throws Exception
	{
		// Given
		final ProductModel product = new ProductModel();
		product.setStackablePrice(Boolean.TRUE);

		// When
		final boolean hasStackablePrice = wileyStackablePriceProductService.hasStackablePrice(product);

		// Then
		assertTrue(hasStackablePrice);
	}

	@Test
	public void shouldNotHaveStackablePriceDueToFalse() throws Exception
	{
		// Given
		final ProductModel product = new ProductModel();
		product.setStackablePrice(Boolean.FALSE);

		// When
		final boolean hasStackablePrice = wileyStackablePriceProductService.hasStackablePrice(product);

		// Then
		assertFalse(hasStackablePrice);
	}

	@Test
	public void shouldNotHaveStackablePriceDueToNullAttribute() throws Exception
	{
		// Given
		final ProductModel product = new ProductModel();
		product.setStackablePrice(null);

		// When
		final boolean hasStackablePrice = wileyStackablePriceProductService.hasStackablePrice(product);

		// Then
		assertFalse(hasStackablePrice);
	}

}