package com.wiley.core.product.interceptors.freetrial;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.enums.ProductEditionFormat;
import com.wiley.core.model.WileyFreeTrialProductModel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;


/**
 * Unit test for {@link WileyFreeTrialProductInitInterceptor}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyFreeTrialProductInitInterceptorUnitTest
{
	@InjectMocks
	private WileyFreeTrialProductInitInterceptor wileyFreeTrialProductInitInterceptor;

	@Mock
	private InterceptorContext interceptorContext;

	@Test
	public void testInit() throws InterceptorException
	{
		WileyFreeTrialProductModel freeTrialProductModel = new WileyFreeTrialProductModel();

		assertNull(freeTrialProductModel.getEditionFormat());

		wileyFreeTrialProductInitInterceptor.onInitDefaults(freeTrialProductModel, interceptorContext);

		assertEquals(ProductEditionFormat.DIGITAL, freeTrialProductModel.getEditionFormat());
	}
}
