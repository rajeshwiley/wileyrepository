package com.wiley.core.wileyb2c.weblinks.providers.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Collections;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyWebLinkModel;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultWileyWebLinkUrlProviderUnitTest
{
	private static final String TEST_URL = "test_url";
	private DefaultWileyWebLinkUrlProvider underTest = new DefaultWileyWebLinkUrlProvider();
	@Mock
	private WileyWebLinkModel mockLink;

	@Test
	public void shouldReturnFirstParameterAsUrl() {
		when(mockLink.getParameters()).thenReturn(Collections.singletonList(TEST_URL));
		final String actualUrl = underTest.getWebLinkUrl(mockLink);
		assertEquals(TEST_URL, actualUrl);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowWhenNoParameters() {
		when(mockLink.getParameters()).thenReturn(Collections.emptyList());
		underTest.getWebLinkUrl(mockLink);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowWhenLinkIsNull() {
		when(mockLink.getParameters()).thenReturn(null);
		underTest.getWebLinkUrl(mockLink);
	}


}
