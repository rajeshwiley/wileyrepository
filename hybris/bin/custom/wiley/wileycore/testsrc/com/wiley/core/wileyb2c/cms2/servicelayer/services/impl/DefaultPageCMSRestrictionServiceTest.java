/**
 *
 */
package com.wiley.core.wileyb2c.cms2.servicelayer.services.impl;

import static org.junit.Assert.assertFalse;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.exceptions.RestrictionEvaluationException;
import de.hybris.platform.cms2.model.restrictions.AbstractRestrictionModel;
import de.hybris.platform.cms2.servicelayer.data.RestrictionData;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


@UnitTest
public class DefaultPageCMSRestrictionServiceTest
{

	@InjectMocks
	private final DefaultPageCMSRestrictionService defaultPageCMSRestrictionService = new DefaultPageCMSRestrictionService();

	@Mock
	private AbstractRestrictionModel restriction;

	@Mock
	private RestrictionData restrictionDataInfo;

	@Before
	public void prepare()
	{
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testdefaultFunctionalityForReturnFalseFromEvaluate() throws RestrictionEvaluationException
	{
		assertFalse(defaultPageCMSRestrictionService.evaluate(restriction, restrictionDataInfo));
	}
}
