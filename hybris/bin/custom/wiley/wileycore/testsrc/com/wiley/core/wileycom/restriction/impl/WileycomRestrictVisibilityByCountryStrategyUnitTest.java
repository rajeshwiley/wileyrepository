package com.wiley.core.wileycom.restriction.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.product.ProductModel;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.servicelayer.i18n.WileycomI18NService;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileycomRestrictVisibilityByCountryStrategyUnitTest
{

	@InjectMocks
	private WileycomRestrictVisibilityByCountryStrategy wileycomRestrictVisibilityByCountryStrategy;
	@Mock
	private WileycomI18NService wileycomI18NService;
	@Mock
	private ProductModel productModel;
	@Mock
	private CountryModel usCountry;
	@Mock
	private CountryModel canadaCountry;
	private CommerceCartParameter parameter;

	@Before
	public void before()
	{
		parameter = new CommerceCartParameter();
		parameter.setProduct(productModel);
	}

	@Test
	public void shouldNotBeRestrictedWhenProductDontHaveStoreCountry() throws CommerceCartModificationException
	{
		when(productModel.getCountryRestrictions()).thenReturn(Collections.singletonList(usCountry));
		when(wileycomI18NService.getCurrentCountry()).thenReturn(Optional.of(canadaCountry));

		final boolean restricted = wileycomRestrictVisibilityByCountryStrategy.isRestricted(parameter);

		assertFalse(restricted);
	}

	@Test
	public void shouldNotBeRestrictedWhenProductDontHaveCountries() throws CommerceCartModificationException
	{
		when(productModel.getCountryRestrictions()).thenReturn(Collections.emptyList());
		when(wileycomI18NService.getCurrentCountry()).thenReturn(Optional.of(usCountry));

		final boolean restricted = wileycomRestrictVisibilityByCountryStrategy.isRestricted(parameter);

		assertFalse(restricted);
	}

	@Test
	public void shouldBeRestrictedWhenProductHaveCountry() throws CommerceCartModificationException
	{
		when(productModel.getCountryRestrictions()).thenReturn(Collections.singletonList(usCountry));
		when(wileycomI18NService.getCurrentCountry()).thenReturn(Optional.of(usCountry));

		final boolean restricted = wileycomRestrictVisibilityByCountryStrategy.isRestricted(parameter);

		assertTrue(restricted);
	}

}
