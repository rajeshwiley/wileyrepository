package com.wiley.core.jalo.translators;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Iterator;

import javax.annotation.Resource;

import org.fest.assertions.Assertions;
import org.fest.assertions.GenericAssert;
import org.junit.Before;
import org.junit.Test;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.adapters.AbstractLinksTwoDimensionListImportAdapter;
import com.wiley.core.enums.WileyWebLinkTypeEnum;
import com.wiley.core.model.WileyWebLinkModel;

import static com.wiley.core.enums.WileyWebLinkTypeEnum.INSTRUCTORS_COMPANION_SITE;
import static com.wiley.core.enums.WileyWebLinkTypeEnum.STUDENTS_COMPANION_SITE;
import static org.fest.assertions.Assertions.assertThat;


/**
 * Test for {@link WebLinksTranslator} and {@link ExternalStoresTranslator}.
 * Due to both translators are based on common logic in {@link AbstractLinksTwoDimensionListImportAdapter}
 * so this test covers all possible cases from {@link WebLinksTranslator}
 * and a few special cases from {@link ExternalStoresTranslator}.
 */
public class WileyWebLinksIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	private static final String TEST_PRODUCT_CODE = "WCOM_PRODUCT";
	private static final WileyWebLinkTypeEnum TEST_LINK_TYPE_1 = INSTRUCTORS_COMPANION_SITE;
	private static final WileyWebLinkTypeEnum TEST_LINK_TYPE_2 = STUDENTS_COMPANION_SITE;
	private static final String TEST_LINK_URL_2 = "http://bcs.wiley.com/?bcsId=10170";
	private static final String TEST_LINK_URL_1 = "http://bcs.wiley.com/?bcsId=10169";
	private static final String LANG_EN_ISOCODE = "en";

	private static final String TEST_RESOURCES_FOLDER = "/wileycore/test/jalo/translators/WileyWebLinksIntegrationTest";

	@Resource
	private ProductService productService;

	@Resource
	private CommonI18NService commonI18NService;

	@Resource
	private ModelService modelService;

	@Before
	public void setUp() throws Exception
	{
		commonI18NService.setCurrentLanguage(commonI18NService.getLanguage(LANG_EN_ISOCODE));
	}

	@Test
	public void shouldFillEmptyFieldDueToEmptyImpexValue() throws Exception
	{
		//Given
		importCsv(TEST_RESOURCES_FOLDER + "/emptyWebLinks.impex", DEFAULT_ENCODING);

		//When
		final ProductModel product = productService.getProductForCode(TEST_PRODUCT_CODE);

		//Then
		assertThat(product.getWebLinks()).isEmpty();
	}

	@Test
	public void shouldFillEmptyFieldDueToListOfEmptyTokens() throws Exception
	{
		//Given
		importCsv(TEST_RESOURCES_FOLDER + "/emptyTokensWebLinks.impex", DEFAULT_ENCODING);

		//When
		final ProductModel product = productService.getProductForCode(TEST_PRODUCT_CODE);

		//Then
		assertThat(product.getWebLinks()).isEmpty();
	}

	@Test(expected = ImpExException.class)
	public void shouldThrowExceptionDueToAbsentPipe() throws Exception
	{
		final String impexFileName = TEST_RESOURCES_FOLDER + "/webLinkWithoutPipe.impex";
		importStream(getClass().getResourceAsStream(impexFileName), DEFAULT_ENCODING, impexFileName, false);
	}


	@Test(expected = ImpExException.class)
	public void shouldThrowExceptionDueToAbsentUrl() throws Exception
	{
		//Given
		final String impexFileName = TEST_RESOURCES_FOLDER + "/webLinkWithPipe.impex";
		importStream(getClass().getResourceAsStream(impexFileName), DEFAULT_ENCODING, impexFileName, false);
	}

	@Test(expected = ImpExException.class)
	public void shouldThrowExceptionDueToWebLinksWrongHeaderType() throws Exception
	{
		final String impexFileName = TEST_RESOURCES_FOLDER + "/wrongHeaderTypeWebLinks.impex";
		importStream(getClass().getResourceAsStream(impexFileName), DEFAULT_ENCODING, impexFileName, false);
	}

	

	@Test
	public void shouldFillOneWebLink() throws Exception
	{
		//Given
		importCsv(TEST_RESOURCES_FOLDER + "/webLinks1.impex", DEFAULT_ENCODING);

		//When
		final ProductModel product = productService.getProductForCode(TEST_PRODUCT_CODE);

		//Then
		assertThat(product.getWebLinks()).hasSize(1);
		WebLinkAssert.assertThat(product.getWebLinks().iterator().next()).hasTypeAndUrl(TEST_LINK_TYPE_1, TEST_LINK_URL_1);
	}

	@Test
	public void shouldFillTwoLinkItems() throws Exception
	{
		//Given
		importCsv(TEST_RESOURCES_FOLDER + "/twoWebLinks.impex", DEFAULT_ENCODING);

		//When
		final ProductModel product = productService.getProductForCode(TEST_PRODUCT_CODE);

		//Then
		assertThat(product.getWebLinks()).hasSize(2);
		final Iterator<WileyWebLinkModel> iterator = product.getWebLinks().iterator();
		WebLinkAssert.assertThat(iterator.next()).hasTypeAndUrl(TEST_LINK_TYPE_1, TEST_LINK_URL_1);
		WebLinkAssert.assertThat(iterator.next()).hasTypeAndUrl(TEST_LINK_TYPE_2, TEST_LINK_URL_2);
	}

	/**
	 * Method checks import for partof relation between ProductModel and WileyWebLinkModel
	 * i.e. guarantee that old instances of {@link WileyWebLinkModel} are substituted and removed.
	 * Such test ensures that import won't leave {@link WileyWebLinkModel} instances which are not attached to product.
	 */
	@Test
	public void shouldSubstituteOldWebLink() throws Exception
	{
		//Given
		importCsv(TEST_RESOURCES_FOLDER + "/webLinks1.impex", DEFAULT_ENCODING);

		final ProductModel product = productService.getProductForCode(TEST_PRODUCT_CODE);

		assertThat(product.getWebLinks()).hasSize(1);
		final WileyWebLinkModel link = product.getWebLinks().iterator().next();
		WebLinkAssert.assertThat(link).hasTypeAndUrl(TEST_LINK_TYPE_1, TEST_LINK_URL_1);

		//When
		importCsv(TEST_RESOURCES_FOLDER + "/webLinks2.impex", DEFAULT_ENCODING);
		modelService.refresh(product);

		//Then
		assertThat(modelService.isRemoved(link)).isTrue();
		assertThat(product.getWebLinks()).hasSize(1);
		WebLinkAssert.assertThat(product.getWebLinks().iterator().next()).hasTypeAndUrl(TEST_LINK_TYPE_2, TEST_LINK_URL_2);
	}


	@Test
	public void shouldIgnoreValue() throws Exception
	{
		//Given
		importCsv(TEST_RESOURCES_FOLDER + "/webLinks1.impex", DEFAULT_ENCODING);

		final ProductModel product = productService.getProductForCode(TEST_PRODUCT_CODE);

		assertThat(product.getWebLinks()).hasSize(1);
		final WileyWebLinkModel link = product.getWebLinks().iterator().next();
		WebLinkAssert.assertThat(link).hasTypeAndUrl(TEST_LINK_TYPE_1, TEST_LINK_URL_1);

		//When
		importCsv(TEST_RESOURCES_FOLDER + "/ignoreWebLinks1.impex", DEFAULT_ENCODING);
		modelService.refresh(product);

		//Then
		assertThat(product.getWebLinks()).hasSize(1);
		assertThat(modelService.isRemoved(link)).isFalse();
	}

	/**
	 * Assert to check {@link WileyWebLinkModel} import result
	 */
	public static class WebLinkAssert extends GenericAssert<WebLinkAssert, WileyWebLinkModel>
	{
		public static WebLinkAssert assertThat(final WileyWebLinkModel webLink)
		{
			return new WebLinkAssert(webLink);
		}

		WebLinkAssert(final WileyWebLinkModel webLink)
		{
			super(WebLinkAssert.class, webLink);
		}

		public WebLinkAssert hasTypeAndUrl(final WileyWebLinkTypeEnum type, final String url)
		{
			Assertions.assertThat(this.actual.getType()).isEqualTo(type);
			Assertions.assertThat(this.actual.getParameters().get(0)).isEqualTo(url);
			return this;
		}
	}

}

