/**
 *
 */
package com.wiley.core.externaltax.strategies.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyasTaxAddressStrategyImplTest
{
	@Mock
	private AbstractOrderModel abstractOrder;

	@Mock
	private AddressModel address;

	@InjectMocks
	private final WileyasTaxAddressStrategyImpl testedInstance = new WileyasTaxAddressStrategyImpl();

	@Test
	public void testShouldReturnPaymentAddress()
	{
		// given
		when(abstractOrder.getPaymentAddress()).thenReturn(address);
		// when
		final AddressModel returnedAddress = testedInstance.resolvePaymentAddress(abstractOrder);
		// then
		Assert.assertEquals(returnedAddress, address);
	}
}
