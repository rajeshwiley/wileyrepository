package com.wiley.core.payment.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.payment.PaymentService;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.methods.CardPaymentService;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.store.BaseStoreModel;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.payment.enums.WileyTransactionStatusEnum;
import com.wiley.core.payment.request.WileySubscriptionAuthorizeRequest;
import com.wiley.core.payment.response.WileyAuthorizeResult;
import com.wiley.core.payment.strategies.CreateSubscriptionAuthorizeRequestStrategy;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyWPGPaymentServiceImplAuthorizeTest
{

	private static final String TRANSACTION_ID = "transactionId";
	private static final String PAYMENT_PROVIDER = "paymentProvider";
	private static final String REQUEST_TOKEN = "requestToken";
	private static final BigDecimal VALUE = new BigDecimal("10.01");
	private static final String WPG_AUTH_CODE = "wpgAuthCode";
	private static final String SITE_ID = "siteId";
	private static final String USD = "USD";
	private static final String NEW_ENTRY_CODE = "newEntryCode";
	private static final String MERCHANT_RESPONSE = "merchantResponse";
	private static final TransactionStatus TRANSACTION_STATUS = TransactionStatus.ACCEPTED;
	private static final Date REQUEST_TIME = new Date();
	private static final WileyTransactionStatusEnum WPG_STATUS = WileyTransactionStatusEnum.SUCCESS;
	private static final String CUSTOMER_UID = "customerUid";
	@Mock
	private PaymentTransactionModel mockTransaction;
	@Mock
	private PaymentTransactionEntryModel mockAuthEntry;
	@Mock
	private OrderModel mockOrder;
	@Mock
	private CustomerModel mockCustomer;
	@Mock
	private CurrencyModel mockCurrency;
	@Mock
	private BaseSiteModel mockSite;
	@Mock
	private BaseStoreModel mockStore;
	@Mock
	private ModelService mockModelService;
	@Mock
	private CardPaymentService mockCardPaymentService;
	@Mock
	private PaymentService mockPaymentService;
	@Mock
	private CreateSubscriptionAuthorizeRequestStrategy mockSubscriptionAuthorizeRequestStrategy;
	@Mock
	private WileySubscriptionAuthorizeRequest mockRequest;

	private WileyAuthorizeResult authorizeResult = new WileyAuthorizeResult();
	@InjectMocks
	private WileyWPGPaymentServiceImpl underTest = new WileyWPGPaymentServiceImpl();

	@Before
	public void setup()
	{
		when(mockPaymentService.getNewPaymentTransactionEntryCode(mockTransaction, PaymentTransactionType.AUTHORIZATION))
				.thenReturn(NEW_ENTRY_CODE);

		when(mockModelService.create(PaymentTransactionEntryModel.class)).thenReturn(mockAuthEntry);
		when(mockModelService.create(PaymentTransactionModel.class)).thenReturn(mockTransaction);

		when(mockCardPaymentService.authorize(any(WileySubscriptionAuthorizeRequest.class))).thenReturn(authorizeResult);
		when(mockSubscriptionAuthorizeRequestStrategy.createRequest(mockOrder)).thenReturn(mockRequest);

		when(mockOrder.getSite()).thenReturn(mockSite);
		when(mockOrder.getStore()).thenReturn(mockStore);
		when(mockOrder.getUser()).thenReturn(mockCustomer);
		when(mockOrder.getCurrency()).thenReturn(mockCurrency);

		when(mockCustomer.getUid()).thenReturn(CUSTOMER_UID);
		when(mockStore.getPaymentProvider()).thenReturn(PAYMENT_PROVIDER);
		when(mockSite.getUid()).thenReturn(SITE_ID);
		when(mockCurrency.getIsocode()).thenReturn(USD);


		authorizeResult.setStatus(WPG_STATUS);
		authorizeResult.setTransactionStatus(TRANSACTION_STATUS);
		authorizeResult.setTotalAmount(VALUE);
		authorizeResult.setRequestId(TRANSACTION_ID);
		authorizeResult.setRequestToken(REQUEST_TOKEN);
		authorizeResult.setMerchantResponse(MERCHANT_RESPONSE);
		authorizeResult.setTransactionStatus(TRANSACTION_STATUS);
		authorizeResult.setAuthorizationTime(REQUEST_TIME);
		authorizeResult.setAuthorizationCode(WPG_AUTH_CODE);
	}

	@Test
	public void shouldCreateAndSaveAuthTransactionEntry()
	{
		PaymentTransactionEntryModel entryModel = underTest.authorize(mockOrder);
		assertSame(mockAuthEntry, entryModel);

		verify(mockAuthEntry).setRequestId(TRANSACTION_ID);
		verify(mockAuthEntry).setRequestToken(REQUEST_TOKEN);
		verify(mockAuthEntry).setTransactionStatus(TRANSACTION_STATUS.toString());
		verify(mockAuthEntry).setCurrency(mockCurrency);
		verify(mockAuthEntry).setCode(NEW_ENTRY_CODE);
		verify(mockAuthEntry).setAmount(VALUE);
		verify(mockAuthEntry).setTime(REQUEST_TIME);
		verify(mockAuthEntry).setTransactionStatusDetails(WPG_STATUS.getDescription());
		verify(mockAuthEntry).setWpgResponseCode(WPG_STATUS.getCode());
		verify(mockAuthEntry).setWpgMerchantResponse(MERCHANT_RESPONSE);
		verify(mockAuthEntry).setWpgAuthCode(WPG_AUTH_CODE);

		verify(mockAuthEntry).setPaymentTransaction(mockTransaction);

		verify(mockTransaction).setPaymentProvider(PAYMENT_PROVIDER);

		ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
		verify(mockTransaction).setCode(captor.capture());
		assertTrue(captor.getValue().startsWith(CUSTOMER_UID));
		verify(mockModelService).saveAll(mockOrder, mockAuthEntry);


	}

}
