package com.wiley.core.wileybundle.service.impl;

import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.product.ProductService;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyBundleModel;
import com.wiley.core.servicelayer.i18n.impl.WileycomI18NServiceImpl;
import com.wiley.core.wiley.restriction.WileyProductRestrictionService;

@RunWith(MockitoJUnitRunner.class)
public class WileyBundlesFilterServiceImplTest
{
	private static final String CURRENT_SESSION_COUNTRY_ISO = "en";
	private static final String FALLBACK_COUNTRY_ISO = "fc_iso";
	private static final String ANOTHER_COUNTRY_ISO = "another";

	private WileyBundlesFilterServiceImpl testInstance;

	@Mock
	private WileyProductRestrictionService wileyProductRestrictionService;
	@Mock
	private ProductService productService;
	@Mock
	private WileycomI18NServiceImpl wileycomI18NService;

	private CountryModel currentSessionCountry;
	private CountryModel fallbackCountry;
	private CountryModel someAnotherCountry;

	@Before
	public void setUp() throws Exception
	{
		testInstance = new WileyBundlesFilterServiceImpl(wileyProductRestrictionService, productService,
				wileycomI18NService);

		currentSessionCountry = givenCountry(CURRENT_SESSION_COUNTRY_ISO);
		someAnotherCountry = givenCountry(ANOTHER_COUNTRY_ISO);
		fallbackCountry = givenCountry(FALLBACK_COUNTRY_ISO);

		when(wileycomI18NService.getFallbackCountries(currentSessionCountry))
				.thenReturn(Arrays.asList(currentSessionCountry, fallbackCountry));

		when(wileycomI18NService.getCurrentCountry()).thenReturn(Optional.of(currentSessionCountry));

	}

	@Test
	public void shouldFilterBundleIfUpsellProductIsNotValid() {
		final String sourceProductCode = "sourceProductCode";
		final String upsellProductCodeThatDoesNotExists = "upsellProductCode";
		final CountryModel bundleCountry = currentSessionCountry;
		final WileyBundleModel bundle = givenBundle(sourceProductCode, upsellProductCodeThatDoesNotExists, bundleCountry);
		final List<WileyBundleModel> bundles = Collections.singletonList(bundle);

		final List<WileyBundleModel> result = testInstance.filterBundles(bundles);

		boolean noBundleWithNotValid = result.stream()
				.noneMatch(bundleModel -> bundleModel.getUpsellProductCode().equals(upsellProductCodeThatDoesNotExists));

		assertTrue(noBundleWithNotValid);
	}

	@Test
	public void shouldReturnOnlyCountryBundlesIfExistsForCurrentSessionCountry() {
		//Ensure that no fallback countries for current country
		when(wileycomI18NService.getFallbackCountries(currentSessionCountry))
				.thenReturn(Collections.singletonList(currentSessionCountry));

		String sourceProductCode = "sourceProductCode";
		String validUpsellProductCode1 = "validUpsellProductCode1";
		CountryModel thisBundleCountry = currentSessionCountry;

		String validUpsellProductCode2 = "validUpsellProductCode1";

		WileyBundleModel bundleForSessionCountry =
				givenBundle(sourceProductCode, validUpsellProductCode1, thisBundleCountry);

		WileyBundleModel bundleForAnotherCountry =
				givenBundle(sourceProductCode, validUpsellProductCode2, someAnotherCountry);

		List<WileyBundleModel> bundles = Arrays.asList(bundleForSessionCountry, bundleForAnotherCountry);

		//When
		final List<WileyBundleModel> result = testInstance.filterBundles(bundles);

		//Then
		boolean allBundlesBelongsToSessionCountry = result.stream()
				.allMatch(bundleModel -> bundleModel.getCountry().equals(currentSessionCountry));

		assertTrue(allBundlesBelongsToSessionCountry);
	}

	@Test
	public void shouldReturnOnlyCountryBundlesIgnoringFallbacksIfCurrentCountryBundleExists() {
		//Ensure that no fallback countries for current country
		when(wileycomI18NService.getFallbackCountries(currentSessionCountry))
				.thenReturn(Arrays.asList(currentSessionCountry, fallbackCountry));

		String sourceProductCode = "sourceProductCode";
		String validUpsellProductCode1 = "validUpsellProductCode1";
		CountryModel thisBundleCountry = currentSessionCountry;

		String validUpsellProductCode2 = "validUpsellProductCode1";

		WileyBundleModel bundleForSessionCountry =
				givenBundle(sourceProductCode, validUpsellProductCode1, thisBundleCountry);

		WileyBundleModel bundleForAnotherCountry =
				givenBundle(sourceProductCode, validUpsellProductCode2, fallbackCountry);

		List<WileyBundleModel> bundles = Arrays.asList(bundleForSessionCountry, bundleForAnotherCountry);

		//When
		final List<WileyBundleModel> result = testInstance.filterBundles(bundles);

		//Then
		boolean allBundlesBelongsToSessionCountry = result.stream()
				.allMatch(bundleModel -> bundleModel.getCountry().equals(currentSessionCountry));

		assertTrue(allBundlesBelongsToSessionCountry);
	}

	@Test
	public void shouldReturnBundlesForFallbacksIfNoBundlesForSessionCountry() {
		String sourceProductCode = "sourceProductCode";
		String validUpsellProductCode2 = "validUpsellProductCode1";

		WileyBundleModel bundleForAnotherCountry =
				givenBundle(sourceProductCode, validUpsellProductCode2, fallbackCountry);

		List<WileyBundleModel> bundles = Arrays.asList(bundleForAnotherCountry);

		//When
		final List<WileyBundleModel> result = testInstance.filterBundles(bundles);

		//Then
		boolean allBundlesBelongsToSessionCountry = result.stream()
				.allMatch(bundleModel -> bundleModel.getCountry().equals(fallbackCountry));

		assertTrue(allBundlesBelongsToSessionCountry);
	}


	@Test
	public void shouldReturnOnlyCountryBundlesForFallbacksIgnoringAllOthers() {
		String sourceProductCode = "sourceProductCode";
		String validUpsellProductCode1 = "validUpsellProductCode1";

		String validUpsellProductCode2 = "validUpsellProductCode1";

		WileyBundleModel bundleForFallbackCountry =
				givenBundle(sourceProductCode, validUpsellProductCode1, fallbackCountry);

		WileyBundleModel bundleForAnotherCountry =
				givenBundle(sourceProductCode, validUpsellProductCode2, someAnotherCountry);

		List<WileyBundleModel> bundles = Arrays.asList(bundleForFallbackCountry, bundleForAnotherCountry);

		//When
		final List<WileyBundleModel> result = testInstance.filterBundles(bundles);

		//Then
		boolean noAnotherBundlesExceptFallback = result.stream()
				.allMatch(bundleModel -> bundleModel.getCountry().equals(fallbackCountry));

		assertTrue(noAnotherBundlesExceptFallback);
	}


	private WileyBundleModel givenBundle(final String sourceProductCodel, final String upsellProductCode,
			final CountryModel country) {
		final WileyBundleModel bundle = new WileyBundleModel();
		bundle.setSourceProductCode(sourceProductCodel);
		bundle.setUpsellProductCode(upsellProductCode);
		bundle.setCountry(country);
		return bundle;
	}

	private CountryModel givenCountry(final String isoCode) {
		CountryModel country = new CountryModel();
		country.setIsocode(isoCode);
		return country;
	}



}