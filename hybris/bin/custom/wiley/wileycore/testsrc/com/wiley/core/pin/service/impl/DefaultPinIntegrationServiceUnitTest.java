package com.wiley.core.pin.service.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.integration.wileycore.WileyCoreGateway;
import com.wiley.core.integration.wileycore.dto.Message;
import com.wiley.core.integration.wileycore.dto.PinResponse;
import com.wiley.core.integration.wileycore.dto.PinResponseCode;
import com.wiley.core.integration.wileycore.dto.Pins;
import com.wiley.core.model.PinModel;
import com.wiley.core.pin.service.PinService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultPinIntegrationServiceUnitTest
{
	private static final String PIN_CODE = "pin_code";
	private static final String FIRST_NAME = "John";
	private static final String LAST_NAME = "Doe";

	@Mock
	private PinService pinService;

	@Mock
	private WileyCoreGateway wileyCoreGateway;

	@Mock
	private PinModel pinModel;

	@Mock
	private AbstractOrderModel orderModel;

	@Mock
	private CustomerModel customerModel;

	@Mock
	private BaseSiteModel siteModel;

	@InjectMocks
	private DefaultPinIntegrationService defaultPinIntegrationService;

	@Before
	public void setUp()
	{
		when(pinService.getPinForCode(PIN_CODE)).thenReturn(pinModel);
		when(pinModel.getCode()).thenReturn(PIN_CODE);
		when(pinService.getPinForOrder(orderModel)).thenReturn(pinModel);
		when(customerModel.getFirstName()).thenReturn(FIRST_NAME);
		when(customerModel.getLastName()).thenReturn(LAST_NAME);
		when(orderModel.getUser()).thenReturn(customerModel);
		when(orderModel.getSite()).thenReturn(siteModel);
	}

	@Test
	public void shouldMethodPinIsUsedForOrderPlacementBeTrueIfPinWasUsedForOrderPlacement()
	{
		when(pinService.isPinUsedForOrder(orderModel)).thenReturn(true);
		Assert.assertTrue(defaultPinIntegrationService.isPinUsedForOrderPlacement(orderModel));
	}

	@Test
	public void shouldMethodPinIsUsedForOrderPlacementBeFalseIfPinWasNotUsedForOrderPlacement()
	{
		when(pinService.isPinUsedForOrder(orderModel)).thenReturn(false);
		Assert.assertFalse(defaultPinIntegrationService.isPinUsedForOrderPlacement(orderModel));
	}

	@Test
	public void shouldMethodValidatePinForOrderBeFalseIfPinWasInvalid()
	{
		when(wileyCoreGateway.validatePin(PIN_CODE, siteModel)).thenReturn(createMessage(PIN_CODE, PinResponseCode.INVALID_PIN));
		Assert.assertFalse(defaultPinIntegrationService.validatePinForOrder(orderModel));
	}

	@Test
	public void shouldMethodValidatePinForOrderBeFalseIfPinWasAlreadyActivated()
	{
		when(wileyCoreGateway.validatePin(PIN_CODE, siteModel)).thenReturn(createMessage(PIN_CODE,
				PinResponseCode.PIN_IS_ALREADY_ACTIVATED));
		Assert.assertFalse(defaultPinIntegrationService.validatePinForOrder(orderModel));
	}

	@Test
	public void shouldMethodValidatePinForOrderBeTrueIfPinIsValid()
	{
		when(wileyCoreGateway.validatePin(PIN_CODE, siteModel)).thenReturn(createMessage(PIN_CODE, PinResponseCode.SUCCESS));
		Assert.assertTrue(defaultPinIntegrationService.validatePinForOrder(orderModel));
	}

	@Test
	public void shouldMethodValidatePinForOrderBeFalseIfUnknownErrorHappened()
	{
		Message message = createMessage(PIN_CODE, PinResponseCode.INVALID_PIN);
		message.setPins(null);
		when(wileyCoreGateway.validatePin(PIN_CODE, siteModel)).thenReturn(message);
		Assert.assertFalse(defaultPinIntegrationService.validatePinForOrder(orderModel));
	}

	@Test
	public void shouldMethodActivatePinForOrderBeFalseIfPinWasInvalid()
	{
		when(wileyCoreGateway.activatePin(PIN_CODE, FIRST_NAME, LAST_NAME, siteModel)).thenReturn(
				createMessage(PIN_CODE, PinResponseCode.INVALID_PIN));
		Assert.assertFalse(defaultPinIntegrationService.activatePinForOrder(orderModel));
	}

	@Test
	public void shouldMethodActivatePinForOrderBeFalseIfPinWasAlreadyActivated()
	{
		when(wileyCoreGateway.activatePin(PIN_CODE, FIRST_NAME, LAST_NAME, siteModel)).thenReturn(createMessage(PIN_CODE,
				PinResponseCode.PIN_IS_ALREADY_ACTIVATED));
		Assert.assertFalse(defaultPinIntegrationService.activatePinForOrder(orderModel));
	}

	@Test
	public void shouldMethodActivatePinForOrderBeTrueIfPinIsValid()
	{
		when(wileyCoreGateway.activatePin(PIN_CODE, FIRST_NAME, LAST_NAME, siteModel)).thenReturn(
				createMessage(PIN_CODE, PinResponseCode.SUCCESS));
		Assert.assertTrue(defaultPinIntegrationService.activatePinForOrder(orderModel));
	}

	@Test
	public void shouldMethodActivatePinForOrderBeFalseIfUnknownErrorHappened()
	{
		Message message = createMessage(PIN_CODE, PinResponseCode.INVALID_PIN);
		message.setPins(null);
		when(wileyCoreGateway.activatePin(PIN_CODE, FIRST_NAME, LAST_NAME, siteModel)).thenReturn(message);
		Assert.assertFalse(defaultPinIntegrationService.activatePinForOrder(orderModel));
	}

	private Message createMessage(final String pinCode, final PinResponseCode pinResponseCode)
	{
		Message message = new Message();
		Pins pins = new Pins();
		PinResponse pinResponse = new PinResponse();
		pinResponse.setPin(pinCode);
		pinResponse.setResponsecode(pinResponseCode);
		pins.getPinresponse().add(pinResponse);
		message.setPins(pins);
		return message;
	}
}
