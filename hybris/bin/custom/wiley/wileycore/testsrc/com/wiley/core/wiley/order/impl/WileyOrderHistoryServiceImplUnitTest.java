package com.wiley.core.wiley.order.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderhistory.model.OrderHistoryEntryModel;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static java.util.Arrays.asList;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyOrderHistoryServiceImplUnitTest
{
	@InjectMocks
	private WileyOrderHistoryServiceImpl testInstance = new WileyOrderHistoryServiceImpl();

	@Mock
	private ModelService mockModelService;

	@Mock
	private KeyGenerator mockKeyGenerator;

	@Mock
	private OrderModel mockOrder;

	@Mock
	private OrderModel mockSnapshotOrder;

	@Mock
	private OrderEntryModel mockOrderEntry;

	@Mock
	private AbstractOrderEntryModel mockAbstractOrderEntry;

	@Mock
	private OrderEntryModel mockSnapshotOrderEntry;

	@Mock
	private AbstractOrderEntryModel mockSnapshotAbstractOrderEntry;

	@Mock
	private OrderHistoryEntryModel mockFirstOrderHistoryEntryModel, mockSecondOrderHistoryEntryModel;

	@Mock
	private Date mockFirstDate, mockSecondDate;

	@Mock
	private OrderModel mockPreviousOrder;

	@Before
	public void setUp()
	{
		when(mockOrder.getEntries()).thenReturn(asList(mockOrderEntry, mockAbstractOrderEntry));
		when(mockSnapshotOrder.getEntries()).thenReturn(asList(mockSnapshotOrderEntry, mockSnapshotAbstractOrderEntry));
		when(mockModelService.clone(mockOrder)).thenReturn(mockSnapshotOrder);
		when(mockModelService.clone(mockOrderEntry)).thenReturn(mockSnapshotOrderEntry);
		when(mockModelService.clone(mockAbstractOrderEntry)).thenReturn(mockSnapshotAbstractOrderEntry);

	}

	@Test
	public void shouldSetNullBusinessKeyForSnapshotEntry()
	{
		testInstance.createHistorySnapshot(mockOrder);

		verify(mockSnapshotOrderEntry).setBusinessKey(null);
		verify(mockSnapshotAbstractOrderEntry).setBusinessKey(null);
	}

	@Test
	public void shouldSetOriginalOrderEntryForSnapshotOne()
	{
		testInstance.createHistorySnapshot(mockOrder);

		verify(mockSnapshotOrderEntry).setOriginalOrderEntry(mockOrderEntry);
		verify(mockSnapshotAbstractOrderEntry, never()).setOriginalOrderEntry(any());
	}

	@Test
	public void testGetPreviousOrderVersion()
	{
		when(mockFirstDate.getTime()).thenReturn(1L);
		when(mockSecondDate.getTime()).thenReturn(2L);
		when(mockFirstOrderHistoryEntryModel.getTimestamp()).thenReturn(mockFirstDate);
		when(mockSecondOrderHistoryEntryModel.getTimestamp()).thenReturn(mockSecondDate);
		when(mockOrder.getHistoryEntries()).thenReturn(Arrays.asList(mockFirstOrderHistoryEntryModel,
				mockSecondOrderHistoryEntryModel));
		when(mockSecondOrderHistoryEntryModel.getPreviousOrderVersion()).thenReturn(mockPreviousOrder);

		OrderModel previousOrderVersion = testInstance.getPreviousOrderVersion(mockOrder);

		assertEquals(mockPreviousOrder, previousOrderVersion);
	}
}
