package com.wiley.core.jalo.translators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.impex.jalo.header.AbstractDescriptor;
import de.hybris.platform.impex.jalo.header.SpecialColumnDescriptor;
import de.hybris.platform.jalo.order.Order;
import de.hybris.platform.jalo.user.User;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;


/**
 * Test for {@link NotNullPasswordTranslator}
 */
@UnitTest
public class NotNullPasswordTranslatorUnitTest
{
	private static final String ENCODED_PASSWORD = "encodedPassword";
	private static final String CELL_VALUE = "cellValue";
	private static final String ENCODING = "testencoding";

	@Spy
	private NotNullPasswordTranslator notNullPasswordTranslator = new NotNullPasswordTranslator();

	@Mock
	private SpecialColumnDescriptor specialColumnDescriptor;

	@Mock
	private AbstractDescriptor.DescriptorParams descriptorParams;

	@Mock
	private User user;

	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);
		when(specialColumnDescriptor.getDescriptorData()).thenReturn(descriptorParams);
		when(descriptorParams.getModifier(NotNullPasswordTranslator.ENCODING)).thenReturn(ENCODING);
		doNothing().when(notNullPasswordTranslator).performSuperInit(any());
		notNullPasswordTranslator.init(specialColumnDescriptor);
	}

	@Test
	public void testEncodingPrependedToCellValueIfPasswordIsNull() throws Exception
	{
		// given
		when(user.getEncodedPassword()).thenReturn(null);

		// when
		notNullPasswordTranslator.performImport(CELL_VALUE, user);

		// then
		verify(notNullPasswordTranslator).performSuperImport(Mockito.eq(ENCODING + ":" + CELL_VALUE), Mockito.eq(user));
	}

	@Test
	public void testNothingIsDoneIfPasswordIsNotNull() throws Exception
	{
		// given
		when(user.getEncodedPassword()).thenReturn(ENCODED_PASSWORD);

		// when
		notNullPasswordTranslator.performImport(CELL_VALUE, user);

		// then
		verify(notNullPasswordTranslator, never()).performSuperImport(any(), any());
	}

	@Test
	public void testNothingIsDoneForNullUser() throws Exception
	{
		// given

		// when
		notNullPasswordTranslator.performImport(CELL_VALUE, null);

		// then
		verify(notNullPasswordTranslator, never()).performSuperImport(any(), any());
	}

	@Test
	public void testNothingIsDoneForNotUser() throws Exception
	{
		// given

		// when
		notNullPasswordTranslator.performImport(CELL_VALUE, new Order());

		// then
		verify(notNullPasswordTranslator, never()).performSuperImport(any(), any());
	}
}
