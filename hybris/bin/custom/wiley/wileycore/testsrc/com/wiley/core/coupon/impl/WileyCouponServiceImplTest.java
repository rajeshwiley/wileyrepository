package com.wiley.core.coupon.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.order.CommerceCartCalculationStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.couponservices.model.AbstractCouponModel;
import de.hybris.platform.couponservices.services.CouponManagementService;
import de.hybris.platform.order.CalculationService;
import de.hybris.platform.promotions.PromotionsService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.site.BaseSiteService;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Created by Sergiy_Mishkovets on 10/23/2017.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyCouponServiceImplTest
{
	private static final String COUPON_CODE_NEW = "couponCodeNew";
	private static final String COUPON_CODE_APPLIED_1 = "couponCodeApplied1";
	private static final String COUPON_CODE_APPLIED_2 = "couponCodeApplied2";

	@InjectMocks
	private WileyCouponServiceImpl testInstance = new WileyCouponServiceImpl();

	@Mock
	private CartModel mockCartModel;

	@Mock
	private OrderModel mockOrderModel;

	@Mock
	private CommerceCartCalculationStrategy mockCommerceCartCalculationStrategy;
	
	@Mock
	private CalculationService calculationService;

	@Mock
	private PromotionsService promotionsService;

	@Mock
	private CouponManagementService mockCouponManagementService;

	@Mock
	private ModelService modelService;
	
	@Mock
	private BaseSiteService baseSiteService;

	@Mock
	private AbstractCouponModel mockCouponModel;

	@Before
	public void setUp()
	{
		when(mockCouponManagementService.redeem(COUPON_CODE_NEW, mockCartModel)).thenReturn(true);

		when(mockCartModel.getAppliedCouponCodes()).thenReturn(Arrays.asList(COUPON_CODE_APPLIED_1, COUPON_CODE_APPLIED_2));
		when(mockOrderModel.getAppliedCouponCodes()).thenReturn(Arrays.asList(COUPON_CODE_APPLIED_1, COUPON_CODE_APPLIED_2));
	}

	@Test
	public void shouldUseCommerceCartCalculationStrategyWhenRedeemCouponForCart()
	{
		makeCouponAvailable(COUPON_CODE_NEW);

		testInstance.redeemCoupon(COUPON_CODE_NEW, mockCartModel);
		verify(mockCommerceCartCalculationStrategy).recalculateCart(any(CommerceCartParameter.class));
	}

	@Test
	public void shouldUseCommerceCartCalculationStrategyWhenReleaseCouponForCart()
	{
		makeCouponAvailable(COUPON_CODE_APPLIED_1);

		testInstance.releaseCouponCode(COUPON_CODE_APPLIED_1, mockCartModel);
		verify(mockCommerceCartCalculationStrategy).recalculateCart(any(CommerceCartParameter.class));
	}

	@Test
	public void shouldNotUseCommerceCartCalculationStrategyForOrder()
	{
		makeCouponAvailable(COUPON_CODE_APPLIED_1);

		testInstance.redeemCoupon(COUPON_CODE_APPLIED_1, mockOrderModel);
		verify(mockCommerceCartCalculationStrategy, never()).recalculateCart(any(CommerceCartParameter.class));
	}

	@Test
	public void shouldReleaseAllCouponsFromCart()
	{
		makeCouponAvailable(COUPON_CODE_APPLIED_1);
		makeCouponAvailable(COUPON_CODE_APPLIED_2);

		testInstance.releaseAllCoupons(mockCartModel);
		verify(mockCouponManagementService).releaseCouponCode(COUPON_CODE_APPLIED_1);
		verify(mockCouponManagementService).releaseCouponCode(COUPON_CODE_APPLIED_2);
	}

	@Test
	public void shouldReleaseAllCouponsFromOrder()
	{
		makeCouponAvailable(COUPON_CODE_APPLIED_1);
		makeCouponAvailable(COUPON_CODE_APPLIED_2);

		testInstance.releaseAllCoupons(mockOrderModel);
		verify(mockCouponManagementService).releaseCouponCode(COUPON_CODE_APPLIED_1);
		verify(mockCouponManagementService).releaseCouponCode(COUPON_CODE_APPLIED_2);
	}

	private void makeCouponAvailable(final String couponCode)
	{
		when(mockCouponManagementService.getCouponForCode(couponCode)).thenReturn(Optional.of(mockCouponModel));
	}
}