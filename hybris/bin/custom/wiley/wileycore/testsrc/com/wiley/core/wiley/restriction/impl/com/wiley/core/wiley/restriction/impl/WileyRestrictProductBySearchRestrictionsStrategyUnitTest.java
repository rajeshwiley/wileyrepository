package com.wiley.core.wiley.restriction.impl.com.wiley.core.wiley.restriction.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doThrow;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.wiley.restriction.impl.WileyRestrictProductBySearchRestrictionsStrategy;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyRestrictProductBySearchRestrictionsStrategyUnitTest
{
	private static final String PRODUCT_CODE = "productCode";
	@Mock
	private ProductService productService;

	@InjectMocks
	private WileyRestrictProductBySearchRestrictionsStrategy wileyRestrictProductBySearchRestrictionsStrategy;

	@Test
	public void whenNoProductByCodeThenRestrictionApply()
	{
		final CommerceCartParameter commerceCartParameter = createCartParameter();
		doThrow(UnknownIdentifierException.class).when(productService).getProductForCode(PRODUCT_CODE);

		final boolean restricted = wileyRestrictProductBySearchRestrictionsStrategy.isRestricted(commerceCartParameter);

		assertTrue(restricted);
	}

	@Test
	public void whenProductExistsThenRestrictionDoNotApply()
	{
		final CommerceCartParameter commerceCartParameter = createCartParameter();

		final boolean restricted = wileyRestrictProductBySearchRestrictionsStrategy.isRestricted(commerceCartParameter);

		assertFalse(restricted);
	}

	@Test
	public void whenSeveralProductsByCodeThenRestrictionNotApplied()
	{
		final CommerceCartParameter commerceCartParameter = createCartParameter();
		doThrow(AmbiguousIdentifierException.class).when(productService).getProductForCode(PRODUCT_CODE);

		final boolean restricted = wileyRestrictProductBySearchRestrictionsStrategy.isRestricted(commerceCartParameter);

		assertFalse(restricted);
	}

	private CommerceCartParameter createCartParameter()
	{
		final CommerceCartParameter commerceCartParameter = new CommerceCartParameter();
		commerceCartParameter.setProduct(createProduct(PRODUCT_CODE));
		return commerceCartParameter;
	}

	private ProductModel createProduct(final String productCode)
	{
		final ProductModel productModel = new ProductModel();
		productModel.setCode(productCode);
		return productModel;
	}

}
