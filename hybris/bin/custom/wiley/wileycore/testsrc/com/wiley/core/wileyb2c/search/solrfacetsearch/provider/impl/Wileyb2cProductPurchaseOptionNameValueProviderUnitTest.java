package com.wiley.core.wileyb2c.search.solrfacetsearch.provider.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.wileyb2c.classification.Wileyb2cClassificationService;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cProductPurchaseOptionNameValueProviderUnitTest
{
	private static final String NAME = "name";
	@InjectMocks
	private Wileyb2cProductPurchaseOptionNameValueProvider wileyb2cProductPurchaseOptionNameValueProvider;
	@Mock
	private Wileyb2cClassificationService wileyb2cClassificationService;
	@Mock
	private ClassificationClassModel classificationClassModel;
	@Mock
	private ProductModel productModel;

	@Test
	public void collectValues() throws FieldValueProviderException
	{
		when(wileyb2cClassificationService.resolveClassificationClass(productModel)).thenReturn(classificationClassModel);
		when(classificationClassModel.getName()).thenReturn(NAME);

		final List<String> values = wileyb2cProductPurchaseOptionNameValueProvider.collectValues(null, null, productModel);

		assertEquals(values.size(), 1);
		assertEquals(values.get(0), NAME);
	}
}
