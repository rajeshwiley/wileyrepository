package com.wiley.core.payment.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.payment.data.CustomerShipToData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyCustomerShipToDataPopulatorTest
{
	private static final String COUNTRY_NAME = "United Kingdom";
	private static final String COUNTRY_NUMERIC_ISOCODE = "826";
	private static final String REGION_NAME = "South East";
	private static final String TITLE_NAME = "Mr.";
	private WileyCustomerShipToDataPopulator testedInstance = new WileyCustomerShipToDataPopulator();

	@Mock
	private CartModel cart;

	@Mock(answer = Answers.RETURNS_DEEP_STUBS)
	private AddressModel deliveryAddress;

	private CustomerShipToData customerShipToData = new CustomerShipToData();


	@Before
	public void setup()
	{
		when(cart.getDeliveryAddress()).thenReturn(deliveryAddress);
	}

	@Test
	public void shouldPopulateCountryNumericIsocode()
	{
		when(deliveryAddress.getCountry().getNumeric()).thenReturn(COUNTRY_NUMERIC_ISOCODE);
		when(deliveryAddress.getCountry().getName()).thenReturn(COUNTRY_NAME);

		testedInstance.populate(cart, customerShipToData);

		assertEquals(COUNTRY_NUMERIC_ISOCODE, customerShipToData.getCountryNumericIsocode());
		assertEquals(COUNTRY_NAME, customerShipToData.getShipToCountryName());
	}

	@Test
	public void shouldNotPopulateCountryNumericIsocode()
	{
		when(deliveryAddress.getCountry().getNumeric()).thenReturn(null);

		testedInstance.populate(cart, customerShipToData);

		assertNull(customerShipToData.getCountryNumericIsocode());
	}

	@Test
	public void shouldPopulateRegionName()
	{
		when(deliveryAddress.getRegion().getName()).thenReturn(REGION_NAME);

		testedInstance.populate(cart, customerShipToData);

		assertEquals(REGION_NAME, customerShipToData.getShipToStateName());
	}


	@Test
	public void shouldPopulateTitleName()
	{
		when(deliveryAddress.getTitle().getName()).thenReturn(TITLE_NAME);

		testedInstance.populate(cart, customerShipToData);

		assertEquals(TITLE_NAME, customerShipToData.getShipToTitleName());
	}

}
