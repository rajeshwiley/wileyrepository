package com.wiley.core.wel.resolver.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.site.BaseSiteService;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.category.WileyCategoryService;


/**
 * JUnit test suite for {@link WelProductModelUrlResolver}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WelProductModelUrlResolverUnitTest
{
	private static final String ROOT_CATEGORY_NAME = "root-category-name";
	private static final String ROOT_CATEGORY_SEO_LABEL = "root category seo label";

	private static final String SUBCATEGORY_CODE = "SUBCATEGORY_LEVEL_I";
	private static final String SUBCATEGORY_NAME = "subcategory-name";
	private static final String SUBCATEGORY_SEO_LABEL = "subcategory seo label";

	private static final String CFA_LEVEL_SUB_CATEGORY_SEO_LABEL = "cfaLevelSubCategorySeoLabel";
	private static final String CFA_LEVEL_SUB_CATEGORY_CODE = "CFA_LEVEL_SUBCAT";

	private static final String PRODUCT_CODE = "product-code";
	private static final String PRODUCT_NAME = "product-name";

	private static final String PRODUCT_SEO_LABEL = "product seo label";
	private static final String WEL_MESSAGE_FORMAT = "/{0}/products/{1}/{2}";
	private static final String WEL_PRODUCT_URL_PATTERN = "/{category-path}/products/{subcategory-path}{product-code}";

	private static final String SLASH = "/";
	private static final String PRODUCTS = "products";
	private static final String PRODUCT_LINE_ROOT_NAME = "productLineRootName";
	private static final String PRODUCT_LINE_ROOT_SEO_LABEL = "productLineRootSEOLabel";

	@Mock
	private BaseSiteService mockBaseSiteService;

	@Mock
	private BaseSiteModel mockBaseSiteModel;

	@Mock
	private CommerceCategoryService mockCommerceCategoryService;

	@Mock
	private ProductModel mockProductModel;

	@Mock
	private CategoryModel mockProductRootCategoryModel;

	@Mock
	private CategoryModel mockProductLineRootCategoryModel;

	@Mock
	private CategoryModel mockSubcategoryModel;

	@Mock
	private CategoryModel mockCFALevelCategory;
	@Mock
	private CategoryModel mockCFALevelSubCategory;

	@Mock
	private CatalogModel mockCatalogModel;

	@Mock
	private WileyCategoryService mockWileyCategoryService;

	@InjectMocks
	private WelProductModelUrlResolver testedInstance = new WelProductModelUrlResolver();

	@Before
	public void setUp()
	{
		List<CategoryModel> categories = Arrays.asList(mockProductRootCategoryModel, mockSubcategoryModel);
		Collection<List<CategoryModel>> pathsForCategory = Collections.singletonList(categories);

		when(mockProductModel.getSupercategories()).thenReturn(categories);
		when(mockSubcategoryModel.getCode()).thenReturn(SUBCATEGORY_CODE);
		when(mockCommerceCategoryService.getPathsForCategory(mockProductRootCategoryModel)).thenReturn(pathsForCategory);
		when(mockCommerceCategoryService.getPathsForCategory(mockCFALevelSubCategory))
				.thenReturn(Collections.singletonList(Arrays.asList(mockCFALevelCategory, mockCFALevelSubCategory)));
		when(mockProductRootCategoryModel.getName()).thenReturn(ROOT_CATEGORY_NAME);
		when(mockProductLineRootCategoryModel.getName()).thenReturn(PRODUCT_LINE_ROOT_NAME);
		testedInstance.setDefaultPattern(WEL_PRODUCT_URL_PATTERN);

		when(mockSubcategoryModel.getName()).thenReturn(SUBCATEGORY_NAME);
		when(mockProductModel.getName()).thenReturn(PRODUCT_NAME);
		when(mockProductModel.getCode()).thenReturn(PRODUCT_CODE);

		when(mockProductLineRootCategoryModel.getSeoLabel()).thenReturn(PRODUCT_LINE_ROOT_SEO_LABEL);
		when(mockProductRootCategoryModel.getSeoLabel()).thenReturn(ROOT_CATEGORY_SEO_LABEL);
		when(mockSubcategoryModel.getSeoLabel()).thenReturn(SUBCATEGORY_SEO_LABEL);
		when(mockProductModel.getSeoLabel()).thenReturn(PRODUCT_SEO_LABEL);
		when(mockCFALevelSubCategory.getSeoLabel()).thenReturn(CFA_LEVEL_SUB_CATEGORY_SEO_LABEL);
		when(mockCFALevelSubCategory.getCode()).thenReturn(CFA_LEVEL_SUB_CATEGORY_CODE);

		when(mockWileyCategoryService.getPrimaryWileyCategoryForProduct(mockProductModel))
				.thenReturn(mockProductLineRootCategoryModel);
		when(mockCFALevelSubCategory.getSupercategories()).thenReturn(Arrays.asList(mockCFALevelCategory));


	}

	@Test
	public void shouldUseSeoLabelIfAvailableForWelSite() throws UnsupportedEncodingException
	{
		when(mockWileyCategoryService.isCFACategory(mockProductLineRootCategoryModel)).thenReturn(true);

		String resolvedUrl = testedInstance.resolveInternal(mockProductModel);
		String expectedUrl = MessageFormat.format(WEL_MESSAGE_FORMAT,
				convertToSafeAndLowerCaseUrl(PRODUCT_LINE_ROOT_SEO_LABEL),
				convertToSafeAndLowerCaseUrl(SUBCATEGORY_SEO_LABEL),
				convertToSafeAndLowerCaseUrl(PRODUCT_SEO_LABEL));

		assertEquals("URL should contain model.seoLabel if available.", expectedUrl, resolvedUrl);
	}

	@Test
	public void shouldDefaultToHierarchyRootForProductWhenProductLineCannotBeResolved()
			throws UnsupportedEncodingException
	{
		when(mockWileyCategoryService.isCFACategory(mockProductRootCategoryModel)).thenReturn(true);
		when(mockWileyCategoryService.getPrimaryWileyCategoryForProduct(mockProductModel))
				.thenReturn(null);

		String resolvedUrl = testedInstance.resolveInternal(mockProductModel);
		String expectedUrl = MessageFormat.format(WEL_MESSAGE_FORMAT,
				convertToSafeAndLowerCaseUrl(ROOT_CATEGORY_SEO_LABEL),
				convertToSafeAndLowerCaseUrl(SUBCATEGORY_SEO_LABEL),
				convertToSafeAndLowerCaseUrl(PRODUCT_SEO_LABEL));

		assertEquals("URL should contain model.seoLabel if available.", expectedUrl, resolvedUrl);
	}

	@Test
	public void shouldUseNameIfSeoLabelIsNotAvailableForWelSite() throws UnsupportedEncodingException
	{
		when(mockWileyCategoryService.isCFACategory(mockProductLineRootCategoryModel)).thenReturn(true);
		when(mockProductLineRootCategoryModel.getSeoLabel()).thenReturn(null);
		when(mockSubcategoryModel.getSeoLabel()).thenReturn(null);
		when(mockProductModel.getSeoLabel()).thenReturn(null);

		String resolvedUrl = testedInstance.resolveInternal(mockProductModel);
		String expectedUrl = MessageFormat.format(WEL_MESSAGE_FORMAT,
				convertToSafeAndLowerCaseUrl(PRODUCT_LINE_ROOT_NAME),
				convertToSafeAndLowerCaseUrl(SUBCATEGORY_NAME),
				convertToSafeAndLowerCaseUrl(PRODUCT_NAME));

		assertEquals("URL should contain model.name if model.seoLabel is not available.", expectedUrl, resolvedUrl);
	}

	@Test
	public void shouldSetSubCategoryForCFAProducts() throws UnsupportedEncodingException
	{
		when(mockWileyCategoryService.isCFACategory(mockProductLineRootCategoryModel)).thenReturn(true);

		String resolvedUrl = testedInstance.resolveInternal(mockProductModel);
		String expectedUrl = SLASH + convertToSafeAndLowerCaseUrl(PRODUCT_LINE_ROOT_SEO_LABEL) + SLASH + PRODUCTS
				+ SLASH + convertToSafeAndLowerCaseUrl(SUBCATEGORY_SEO_LABEL)
				+ SLASH + convertToSafeAndLowerCaseUrl(PRODUCT_SEO_LABEL);

		assertEquals("Non CFA Product URL should not contain subcategory", expectedUrl, resolvedUrl);
	}

	@Test
	public void shouldSetCFALevelSubCategoryIfAvailable() throws UnsupportedEncodingException
	{
		when(mockWileyCategoryService.isCFACategory(mockProductLineRootCategoryModel)).thenReturn(true);
		when(mockWileyCategoryService.isCFALevelCategory(mockCFALevelCategory)).thenReturn(true);

		when(mockProductModel.getSupercategories()).thenReturn(
				Arrays.asList(mockProductRootCategoryModel, mockCFALevelSubCategory, mockSubcategoryModel));

		String resolvedUrl = testedInstance.resolveInternal(mockProductModel);
		String expectedUrl = SLASH + convertToSafeAndLowerCaseUrl(PRODUCT_LINE_ROOT_SEO_LABEL) + SLASH + PRODUCTS
				+ SLASH + convertToSafeAndLowerCaseUrl(CFA_LEVEL_SUB_CATEGORY_SEO_LABEL)
				+ SLASH + convertToSafeAndLowerCaseUrl(PRODUCT_SEO_LABEL);

		assertEquals("Non CFA Product URL should not contain subcategory", expectedUrl, resolvedUrl);
	}


	@Test
	public void shouldSkipSubCategoryFroNonCFAProducts() throws UnsupportedEncodingException
	{
		when(mockWileyCategoryService.isCFACategory(mockProductLineRootCategoryModel)).thenReturn(false);

		String resolvedUrl = testedInstance.resolveInternal(mockProductModel);
		String expectedUrl = SLASH + convertToSafeAndLowerCaseUrl(PRODUCT_LINE_ROOT_SEO_LABEL) + SLASH + PRODUCTS
				+ SLASH + convertToSafeAndLowerCaseUrl(PRODUCT_SEO_LABEL);

		assertEquals("Non CFA Product URL should not contain subcategory", expectedUrl, resolvedUrl);
	}

	private String convertToSafeAndLowerCaseUrl(final String text) throws UnsupportedEncodingException
	{
		return URLEncoder.encode(text, "utf-8").replaceAll("[^%A-Za-z0-9\\-]+", "-").toLowerCase();
	}
}
