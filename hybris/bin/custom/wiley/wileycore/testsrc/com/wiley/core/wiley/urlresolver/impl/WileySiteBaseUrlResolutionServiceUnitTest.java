package com.wiley.core.wiley.urlresolver.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.store.BaseStoreModel;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.apache.commons.configuration.Configuration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.store.WileyBaseStoreService;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileySiteBaseUrlResolutionServiceUnitTest
{
	private static final String STORE_UID = "testUid1";
	private static final String SITE_UID = "testUid2";
	private static final String MEDIA_URL = "mediaUrl";
	private static final String SITE_URL = "siteUrl";
	private static final String MEDIA_PREFIX = "media.";
	private static final String WEBSITE_PREFIX = "website.";
	private static final String HTTPS = ".https";
	@InjectMocks
	private WileySiteBaseUrlResolutionService wileySiteBaseUrlResolutionService;
	@Mock
	private Configuration mockConfiguration;
	@Mock
	private ConfigurationService mockConfigurationService;
	@Mock
	private WileyBaseStoreService wileyBaseStoreService;
	private BaseSiteModel baseSiteModel;
	private BaseStoreModel baseStoreModel;

	@Before
	public void setUp()
	{
		when(mockConfigurationService.getConfiguration()).thenReturn(mockConfiguration);
		baseSiteModel = new BaseSiteModel();
		baseSiteModel.setUid(SITE_UID);
		baseStoreModel = new BaseStoreModel();
		baseStoreModel.setUid(STORE_UID);
	}

	@Test(expected = IllegalStateException.class)
	public void getMediaUrlForSiteWhenStoreIsEmptyShouldThrowIllegalStateException()
	{
		when(wileyBaseStoreService.getBaseStoreForBaseSite(baseSiteModel)).thenReturn(Optional.empty());

		wileySiteBaseUrlResolutionService.getMediaUrlForSite(baseSiteModel, true);
	}

	@Test
	public void getMediaUrlForSiteWhenStoreIsPresentAndSecureShouldGetHttpsUrlFromProperty()
	{
		when(wileyBaseStoreService.getBaseStoreForBaseSite(baseSiteModel)).thenReturn(Optional.of(baseStoreModel));
		when(mockConfiguration.getString(MEDIA_PREFIX + STORE_UID + HTTPS, null)).thenReturn(MEDIA_URL);

		String url = wileySiteBaseUrlResolutionService.getMediaUrlForSite(baseSiteModel, true);

		Assert.assertEquals(url, MEDIA_URL);
	}

	@Test(expected = IllegalStateException.class)
	public void getWebsiteUrlForSiteWhenStoreIsEmptyShouldThrowIllegalStateException()
	{
		when(wileyBaseStoreService.getBaseStoreForBaseSite(baseSiteModel)).thenReturn(Optional.empty());

		wileySiteBaseUrlResolutionService.getWebsiteUrlForSite(baseSiteModel, "", true, "");
	}

	@Test
	public void getWebsiteUrlForSiteWhenStoreIsPresentAndSecureShouldGetHttpsUrlFromProperty()
	{
		when(wileyBaseStoreService.getBaseStoreForBaseSite(baseSiteModel)).thenReturn(Optional.of(baseStoreModel));
		when(mockConfiguration.getString(WEBSITE_PREFIX + STORE_UID + HTTPS, null)).thenReturn(SITE_URL);

		String url = wileySiteBaseUrlResolutionService.getWebsiteUrlForSite(baseSiteModel, "", true, "");

		Assert.assertEquals(url, SITE_URL);
	}

}
