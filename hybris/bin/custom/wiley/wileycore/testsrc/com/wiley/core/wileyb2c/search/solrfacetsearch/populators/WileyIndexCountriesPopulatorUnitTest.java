package com.wiley.core.wileyb2c.search.solrfacetsearch.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.model.config.SolrFacetSearchConfigModel;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyIndexCountriesPopulatorUnitTest
{
	@InjectMocks
	private WileyIndexCountriesPopulator wileyIndexCountriesPopulator;
	@Mock
	private SolrFacetSearchConfigModel solrFacetSearchConfig;
	@Mock
	private IndexConfig indexConfig;
	@Mock
	private CountryModel countryModel;

	@Test
	public void populate()
	{
		final List<CountryModel> countries = Collections.singletonList(countryModel);
		when(solrFacetSearchConfig.getCountries()).thenReturn(countries);

		wileyIndexCountriesPopulator.populate(solrFacetSearchConfig, indexConfig);

		verify(indexConfig).setCountries(countries);
	}
}
