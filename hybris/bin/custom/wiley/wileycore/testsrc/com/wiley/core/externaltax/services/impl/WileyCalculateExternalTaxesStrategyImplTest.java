package com.wiley.core.externaltax.services.impl;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.externaltax.WileyTaxGateway;
import com.wiley.core.externaltax.dto.TaxCalculationRequestDto;
import com.wiley.core.externaltax.dto.TaxCalculationResponseDto;
import com.wiley.core.externaltax.dto.TaxCalculationResponseEntryDto;
import com.wiley.core.externaltax.dto.TaxValueDto;
import com.wiley.core.externaltax.strategies.impl.WileyCalculateExternalTaxesStrategyImpl;

import static java.util.Arrays.asList;


/**
 * This tests checks how {@link WileyCalculateExternalTaxesStrategyImpl} approaching with external tax calculation service.
 * It is focusing on tracking calculation result
 */
@RunWith(MockitoJUnitRunner.class)
public class WileyCalculateExternalTaxesStrategyImplTest
{
	private static final double ZERO_TAX_AMOUNT = 0.0;
	private static final String SITE_ID = "asSite";
	private static final Integer ENTRY_ID_0 = 0;

	@InjectMocks
	private WileyCalculateExternalTaxesStrategyImpl testInstance;

	@Mock
	private WileyTaxGateway mockTaxGateway;

	@Mock
	private Converter<AbstractOrderModel, TaxCalculationRequestDto> mockTaxCalculationRequestDtoConverter;

	@Mock
	private AbstractOrderModel mockAbstractOrder;

	@Mock
	private CurrencyModel mockCurrency;

	private TaxCalculationRequestDto taxCalculationRequestDto;
	private TaxCalculationResponseDto taxCalculationResponseDto;

	@Before
	public void setUp()
	{
		when(mockAbstractOrder.getCurrency()).thenReturn(mockCurrency);

		taxCalculationRequestDto = new TaxCalculationRequestDto();


		taxCalculationResponseDto = new TaxCalculationResponseDto();
		final TaxValueDto taxValueDto = givenTaxValueDto("tx1", 10.0);
		final TaxCalculationResponseEntryDto taxCalculationResponseEntryDto = givenTaxCalculationResponseEntryDto(ENTRY_ID_0,
				taxValueDto);
		taxCalculationResponseDto.setEntries(asList(taxCalculationResponseEntryDto));
		taxCalculationResponseDto.setHandlingTaxes(Collections.emptyList());
		taxCalculationResponseDto.setShippingTaxes(Collections.emptyList());

		when(mockTaxCalculationRequestDtoConverter.convert(mockAbstractOrder)).thenReturn(taxCalculationRequestDto);
		when(mockTaxGateway.calculateTax(taxCalculationRequestDto)).thenReturn(taxCalculationResponseDto);
	}

	private TaxCalculationResponseEntryDto givenTaxCalculationResponseEntryDto(final Integer entryId,
			final TaxValueDto... taxValues)
	{
		final TaxCalculationResponseEntryDto taxCalculationResponseEntryDto = new TaxCalculationResponseEntryDto();
		taxCalculationResponseEntryDto.setId(entryId);
		taxCalculationResponseEntryDto.setTaxes(asList(taxValues));
		return taxCalculationResponseEntryDto;
	}

	@Test
	public void shouldCallGatewayForTaxCalculation()
	{
		testInstance.calculateExternalTaxes(mockAbstractOrder);

		verify(mockTaxGateway).calculateTax(eq(taxCalculationRequestDto));
	}

	private TaxValueDto givenTaxValueDto(final String code, final Double value)
	{
		final TaxValueDto taxValueDto = new TaxValueDto();
		taxValueDto.setCode(code);
		taxValueDto.setValue(value);
		return taxValueDto;
	}

	//TODO: Cover result population logic with tests

}
