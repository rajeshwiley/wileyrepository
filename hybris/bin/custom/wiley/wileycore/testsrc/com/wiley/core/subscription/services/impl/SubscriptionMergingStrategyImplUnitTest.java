package com.wiley.core.subscription.services.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.subscriptionservices.enums.SubscriptionStatus;
import de.hybris.platform.subscriptionservices.model.SubscriptionProductModel;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileySubscriptionModel;


/**
 * Default unit test for {@link SubscriptionMergingStrategyImpl}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SubscriptionMergingStrategyImplUnitTest
{

	@InjectMocks
	private SubscriptionMergingStrategyImpl subscriptionMergingStrategy;

	// Test data

	@Mock
	private WileySubscriptionModel prevWileySubscriptionModelMock;

	@Mock
	private WileySubscriptionModel nextWileySubscriptionModelMock;

	@Before
	public void setUp() throws Exception
	{
		when(prevWileySubscriptionModelMock.getStatus()).thenReturn(SubscriptionStatus.ACTIVE);
	}

	@Test
	public void testMergeSubscriptionsFromDateWhenWileySubscriptionHasProductInsteadOfSubscriptionProduct()
	{
		// Given
		when(nextWileySubscriptionModelMock.getProduct()).thenReturn(mock(ProductModel.class));

		try
		{
			// When
			subscriptionMergingStrategy.mergeSubscriptionsFromDate(prevWileySubscriptionModelMock, nextWileySubscriptionModelMock,
					new Date());
			fail("Expected " + IllegalStateException.class);
		}
		catch (IllegalStateException e)
		{
			// Then
			// Success
		}
	}

	@Test
	public void testMergeSubscriptionsFromDateWhenWileySubscriptionHasSubscriptionProduct()
	{
		// Given
		when(nextWileySubscriptionModelMock.getProduct()).thenReturn(mock(SubscriptionProductModel.class));

		// When
		final Date date = subscriptionMergingStrategy.mergeSubscriptionsFromDate(prevWileySubscriptionModelMock,
				nextWileySubscriptionModelMock, new Date());

		// Success is when there are no exceptions
		assertNotNull(date);
	}
}