package com.wiley.core.order.strategies.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.assistedserviceservices.AssistedServiceService;
import de.hybris.platform.assistedserviceservices.utils.AssistedServiceSession;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.order.strategies.WileyOrderCancelabilityStrategy;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyAsmAwareOrderCancelabilityStrategyUnitTest
{
	@InjectMocks
	private WileyAsmAwareOrderCancelabilityStrategyImpl wileyOrderCancelabilityStrategy;
	@Mock
	private WileyOrderCancelabilityStrategy wileyAsmOrderCancelabilityStrategy, wileyDefaultOrderCancelabilityStrategy;
	@Mock
	private OrderModel orderMock;
	@Mock
	private AssistedServiceService assistedServiceServiceMock;
	@Mock
	private AssistedServiceSession assistedServiceSessionMock;

	@Test
	public void behaviourTestWithAsmSession()
	{
		when(wileyAsmOrderCancelabilityStrategy.isCancelable(anyObject())).thenReturn(true);
		when(wileyAsmOrderCancelabilityStrategy.hasCancelableEntries(anyObject())).thenReturn(true);
		when(assistedServiceServiceMock.getAsmSession()).thenReturn(assistedServiceSessionMock);

		wileyOrderCancelabilityStrategy.hasCancelableEntries(orderMock);
		wileyOrderCancelabilityStrategy.isCancelable(orderMock);
		wileyOrderCancelabilityStrategy.isCancelableOrderEntry(orderMock, OrderStatus.CREATED);
		wileyOrderCancelabilityStrategy.isCancelableEntryState(OrderStatus.CREATED);

		verify(wileyAsmOrderCancelabilityStrategy).hasCancelableEntries(anyObject());
		verify(wileyAsmOrderCancelabilityStrategy).isCancelable(anyObject());
		verify(wileyAsmOrderCancelabilityStrategy).isCancelableOrderEntry(anyObject(), anyObject());
		verify(wileyAsmOrderCancelabilityStrategy).isCancelableEntryState(anyObject());
		verify(wileyDefaultOrderCancelabilityStrategy, never()).hasCancelableEntries(anyObject());
		verify(wileyDefaultOrderCancelabilityStrategy, never()).isCancelable(anyObject());
		verify(wileyDefaultOrderCancelabilityStrategy, never()).isCancelableOrderEntry(anyObject(), anyObject());
		verify(wileyDefaultOrderCancelabilityStrategy, never()).isCancelableEntryState(anyObject());
	}

	@Test
	public void behaviourTestWithoutAsmSession()
	{
		when(wileyDefaultOrderCancelabilityStrategy.isCancelable(anyObject())).thenReturn(true);
		when(wileyDefaultOrderCancelabilityStrategy.hasCancelableEntries(anyObject())).thenReturn(true);
		when(assistedServiceServiceMock.getAsmSession()).thenReturn(null);

		wileyOrderCancelabilityStrategy.hasCancelableEntries(orderMock);
		wileyOrderCancelabilityStrategy.isCancelable(orderMock);
		wileyOrderCancelabilityStrategy.isCancelableOrderEntry(orderMock, OrderStatus.CREATED);
		wileyOrderCancelabilityStrategy.isCancelableEntryState(OrderStatus.CREATED);

		verify(wileyDefaultOrderCancelabilityStrategy).hasCancelableEntries(anyObject());
		verify(wileyDefaultOrderCancelabilityStrategy).isCancelable(anyObject());
		verify(wileyDefaultOrderCancelabilityStrategy).isCancelableOrderEntry(anyObject(), anyObject());
		verify(wileyDefaultOrderCancelabilityStrategy).isCancelableEntryState(anyObject());
		verify(wileyAsmOrderCancelabilityStrategy, never()).hasCancelableEntries(anyObject());
		verify(wileyAsmOrderCancelabilityStrategy, never()).isCancelable(anyObject());
		verify(wileyAsmOrderCancelabilityStrategy, never()).isCancelableOrderEntry(anyObject(), anyObject());
		verify(wileyAsmOrderCancelabilityStrategy, never()).isCancelableEntryState(anyObject());
	}
}
