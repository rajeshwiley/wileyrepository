package com.wiley.core.wileyb2c.classification.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureValue;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.wileyb2c.classification.Wileyb2cClassificationAttributes;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cResolveIssnStrategyUnitTest
{
	@InjectMocks
	private Wileyb2cResolveIssnStrategy wileyb2cResolveIssnStrategy;
	@Mock
	private Feature feature;
	@Mock
	private FeatureValue featureValue;

	@Test
	public void processFeature()
	{
		when(feature.getValue()).thenReturn(featureValue);
		when(featureValue.getValue()).thenReturn("23669608");

		final String processFeature = wileyb2cResolveIssnStrategy.processFeature(feature);

		assertEquals(processFeature, "2366-9608");
	}

	@Test
	public void getAttributes()
	{
		final Set<Wileyb2cClassificationAttributes> attributes = wileyb2cResolveIssnStrategy.getAttributes();

		assertEquals(attributes, Wileyb2cResolveIssnStrategy.CLASSIFICATION_ATTRIBUTES);
	}
}
