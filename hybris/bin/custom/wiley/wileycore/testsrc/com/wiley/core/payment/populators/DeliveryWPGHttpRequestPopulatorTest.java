package com.wiley.core.payment.populators;

import com.wiley.core.order.WileyCartService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.payment.data.PaymentData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;


/**
 * Test for {@link DeliveryMethodWPGHttpRequestPopulator}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DeliveryWPGHttpRequestPopulatorTest
{

	@InjectMocks
	private DeliveryMethodWPGHttpRequestPopulator deliveryMethodWPGHttpRequestPopulator;

	@Mock
	private WileyCartService wileyCartServiceMock;

	@Mock
	private CartModel cartModelMock;

	@Mock
	private DeliveryModeModel deliveryModeModelMock;

	private PaymentData paymentDataMock;

	private static final String WPG_CUSTOM_DELIVERY_METHOD_NAME = "WPG_CUSTOM_delivery_method_name";
	private static final String WPG_CUSTOM_DELIVERY_METHOD_DESCRIPTION = "WPG_CUSTOM_delivery_method_description";

	private static final String DELIVERY_METHOD_NAME = "One day";
	private static final String DELIVERY_METHOD_DESCRIPTION = "One day description";



	@Before
	public void setUp()
	{
		when(wileyCartServiceMock.getSessionCart()).thenReturn(cartModelMock);
		when(cartModelMock.getDeliveryMode()).thenReturn(deliveryModeModelMock);

		when(deliveryModeModelMock.getDescription()).thenReturn(DELIVERY_METHOD_DESCRIPTION);
		paymentDataMock = new PaymentData();
	}

	@Test
	public void shouldPopulateDeliveryMethodName() throws Exception
	{
		when(deliveryModeModelMock.getName()).thenReturn(DELIVERY_METHOD_NAME);

		deliveryMethodWPGHttpRequestPopulator.populate(null, paymentDataMock);

		assertEquals(DELIVERY_METHOD_NAME, paymentDataMock.getParameters().get(WPG_CUSTOM_DELIVERY_METHOD_NAME));
	}


	@Test
	public void shouldPopulateDeliveryMethodDescription() throws Exception
	{
		when(deliveryModeModelMock.getDescription()).thenReturn(DELIVERY_METHOD_DESCRIPTION);

		deliveryMethodWPGHttpRequestPopulator.populate(null, paymentDataMock);

		assertEquals(DELIVERY_METHOD_DESCRIPTION, paymentDataMock.getParameters().get(WPG_CUSTOM_DELIVERY_METHOD_DESCRIPTION));
	}

}