package com.wiley.core.util;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.AmazonS3Exception;


/**
 * Created by Uladzimir_Barouski on 2/10/2016.
 */
@UnitTest
public class RetryableAmazonS3ClientFactoryUnitTest
{

	private static final String ENDPOINT_KEY = "testEndpoint";
	private static final String ACCESS_KEY_VAL = "testKey";
	private static final String SECRET_ACCESS_KEY_VAL = "testSecretKey";
	private static final String BUCKET = "testBucket";
	private static final String FILE_NAME = "testFileName";
	private static final String RETRY_METHOD = "getObjectMetadata(java.lang.String,java.lang.String)";
	private static final String RETRY_PROPERTY_FLAG_NAME = "s3MediaStorageStrategy.objectMetaData.retry";

	@Mock
	private AmazonS3 s3ServiceMock;

	@Mock(answer = Answers.RETURNS_DEEP_STUBS)
	private ConfigurationService configurationServiceMock;

	private AmazonS3 s3Service;

	private RetryableAmazonS3ClientFactory clientFactory;

	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);
		clientFactory = new RetryableAmazonS3ClientFactory()
		{
			@Override
			AmazonS3 createAmazonS3(final String accessKey, final String secretAccessKey, final String endPoint)
			{
				return RetryableAmazonS3ClientFactoryUnitTest.this.s3ServiceMock;
			}
		};
		clientFactory.setInvolvedMethods(Arrays.asList(RETRY_METHOD));
		clientFactory.setAttempts(2);
		clientFactory.setConfigurationService(configurationServiceMock);
	}

	@Test
	public void testObjectMetadataWithRetryOnAndAmazonS3Exception() throws Exception
	{
		given(configurationServiceMock.getConfiguration().getBoolean(RETRY_PROPERTY_FLAG_NAME, false)).willReturn(true);
		s3Service = clientFactory.getS3Service(ACCESS_KEY_VAL, SECRET_ACCESS_KEY_VAL, ENDPOINT_KEY);
		AmazonS3Exception mockAmazon = new AmazonS3Exception("");
		given(s3ServiceMock.getObjectMetadata(BUCKET, FILE_NAME)).willThrow(mockAmazon);
		try
		{
			s3Service.getObjectMetadata(BUCKET, FILE_NAME);
			Assert.fail("should have failed");
		}
		catch (Exception e)
		{
			//ok
		}
		verify(s3ServiceMock, times(2)).getObjectMetadata(BUCKET, FILE_NAME);
		verifyNoMoreInteractions(s3ServiceMock);
	}

	@Test
	public void testObjectMetadataWithRetryOnNotAmazonS3Exception() throws Exception
	{
		try
		{
			given(configurationServiceMock.getConfiguration().getBoolean(RETRY_PROPERTY_FLAG_NAME, false)).willReturn(true);
			s3Service = clientFactory.getS3Service(ACCESS_KEY_VAL, SECRET_ACCESS_KEY_VAL, ENDPOINT_KEY);
			IllegalArgumentException mockExc = new IllegalArgumentException("");
			given(s3ServiceMock.getObjectMetadata(BUCKET, FILE_NAME)).willThrow(mockExc);
			s3Service.getObjectMetadata(BUCKET, FILE_NAME);
			Assert.fail("should have failed");
		}
		catch (Exception e)
		{
			//ok
		}
	}

	@Test
	public void testObjectMetadataWithRetryOff() throws Exception
	{
		try
		{
			s3Service = clientFactory.getS3Service(ACCESS_KEY_VAL, SECRET_ACCESS_KEY_VAL, ENDPOINT_KEY);
			AmazonS3Exception mockAmazon = new AmazonS3Exception("");
			given(s3ServiceMock.getObjectMetadata(BUCKET, FILE_NAME)).willThrow(mockAmazon);
			s3Service.getObjectMetadata(BUCKET, FILE_NAME);
			Assert.fail("should have failed");
		}
		catch (Exception e)
		{
			//ok
		}
	}
}