package com.wiley.core.returns.strategy.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.ReturnStatus;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.returns.model.ReturnEntryModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyReturnEntryBasedReturnableCheckTest
{

	private static final long COMPLETED_EXPECTED_QTY = 5L;
	private static final long FAILED_EXPECTED_QTY = 10L;
	private static final long ORDER_ENTRY_QTY = 15L;
	private static final int MINIMAL_REFUNDABLE_QUANTITY = 1;
	private static final int NEGATIVE_QUANTITY = -1;

	@Mock
	private ReturnEntryModel mockFailedReturnEntry;
	@Mock
	private ReturnEntryModel mockCompletedReturnEntry;
	@Mock
	private SearchResult mockSearchResult;
	@Mock
	private OrderModel mockOrder;

	@Mock
	private OrderEntryModel mockOrderEntry;


	@Mock
	private FlexibleSearchService flexibleSearchService;

	@InjectMocks
	private WileyReturnEntryBasedReturnableCheck underTest = new WileyReturnEntryBasedReturnableCheck();

	@Before
	public void setup()
	{
		when(mockFailedReturnEntry.getStatus()).thenReturn(ReturnStatus.PAYMENT_FAILED);
		when(mockFailedReturnEntry.getExpectedQuantity()).thenReturn(FAILED_EXPECTED_QTY);

		when(mockCompletedReturnEntry.getStatus()).thenReturn(ReturnStatus.COMPLETED);
		when(mockCompletedReturnEntry.getExpectedQuantity()).thenReturn(COMPLETED_EXPECTED_QTY);

		when(mockOrderEntry.getQuantity()).thenReturn(ORDER_ENTRY_QTY);

		when(mockSearchResult.getResult()).thenReturn(Arrays.asList(mockFailedReturnEntry, mockCompletedReturnEntry));
		doReturn(mockSearchResult).when(flexibleSearchService).search(anyString(), anyMap());
	}

	@Test
	public void shouldIgnoreAlreadyReturnedItems()
	{
		assertTrue(underTest.perform(mockOrder, mockOrderEntry, ORDER_ENTRY_QTY));
	}

	@Test
	public void shouldReturnFalseWhenRequestedQtyMoreThanEntryQty()
	{
		assertFalse(underTest.perform(mockOrder, mockOrderEntry, ORDER_ENTRY_QTY + 1));
	}

	@Test
	public void shouldBeAbleReturnTrueIgnoringAlreadyRefundedItems()
	{
		assertTrue(underTest.perform(mockOrder, mockOrderEntry, ORDER_ENTRY_QTY - COMPLETED_EXPECTED_QTY + 1));
	}

	@Test
	public void shouldBeAbleReturnOneItem()
	{
		assertTrue(underTest.perform(mockOrder, mockOrderEntry, MINIMAL_REFUNDABLE_QUANTITY));
	}

	@Test
	public void shouldDeclineNegativeQuantity()
	{
		assertFalse(underTest.perform(mockOrder, mockOrderEntry, NEGATIVE_QUANTITY));
	}
}
