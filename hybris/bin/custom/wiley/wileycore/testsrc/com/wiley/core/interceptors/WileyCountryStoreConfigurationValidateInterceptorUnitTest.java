package com.wiley.core.interceptors;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyCountryStoreConfigurationModel;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyCountryStoreConfigurationValidateInterceptorUnitTest
{
	@Mock
	private CountryModel country;

	@Mock
	private RegionModel region1;

	@Mock
	private RegionModel region2;

	@Mock
	private RegionModel region3;

	@Mock
	private WileyCountryStoreConfigurationModel wileyCountryStoreConfigurationModel;

	private List<RegionModel> subRegions;

	private List<RegionModel> regions;

	@InjectMocks()
	private WileyCountryStoreConfigurationValidateInterceptor interceptor;

	@Before
	public void setUp()
	{
		regions = new ArrayList<>(3);
		regions.add(region1);
		regions.add(region2);
		regions.add(region3);
		subRegions = Collections.singletonList(region1);
		when(wileyCountryStoreConfigurationModel.getCountry()).thenReturn(country);
		when(wileyCountryStoreConfigurationModel.getRegions()).thenReturn(subRegions);
		when(country.getRegions()).thenReturn(regions);
	}

	@Test
	public void onValidate() throws InterceptorException
	{
		interceptor.onValidate(wileyCountryStoreConfigurationModel, null);
	}

	@Test(expected = InterceptorException.class)
	public void onValidateExc() throws InterceptorException
	{
		regions.remove(0);
		interceptor.onValidate(wileyCountryStoreConfigurationModel, null);
	}
}