package com.wiley.core.paymentinfo.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.user.AddressModel;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Default unit test for {@link WileyCardPaymentInfoCcOwnerGeneratorImpl}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyCardPaymentInfoCcOwnerGeneratorImplUnitTest
{

	@InjectMocks
	private WileyCardPaymentInfoCcOwnerGeneratorImpl wileyCardPaymentInfoCcOwnerGenerator;

	@Mock
	private AddressModel addressModelMock;

	@Test
	public void testGetCCOwnerFromBillingAddress()
	{
		// Given
		final String testFirstName = "someTestFirstName";
		final String testLastName = "someTestLastName";
		when(addressModelMock.getFirstname()).thenReturn(testFirstName);
		when(addressModelMock.getLastname()).thenReturn(testLastName);

		// When
		final Optional<String> optionalActualOwner = wileyCardPaymentInfoCcOwnerGenerator.getCCOwnerFromBillingAddress(
				addressModelMock);

		// Then
		assertNotNull(optionalActualOwner);
		assertTrue("Expected that optionalActualOwner is present.", optionalActualOwner.isPresent());

		final String actualOwner = optionalActualOwner.get();
		assertEquals(testFirstName + " " + testLastName, actualOwner);
	}

	@Test
	public void testGetCCOwnerFromBillingAddressWithNullParameter()
	{
		try
		{
			// When
			wileyCardPaymentInfoCcOwnerGenerator.getCCOwnerFromBillingAddress(null);
			fail("Expected IllegalArgumentException");
		}
		catch (IllegalArgumentException e)
		{
			// Then
			// Success
		}
	}


}