package com.wiley.core.search.solrfacetsearch.populators.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.ValueRangeSet;
import de.hybris.platform.solrfacetsearch.model.config.SolrIndexedPropertyModel;
import de.hybris.platform.solrfacetsearch.model.config.SolrValueRangeModel;
import de.hybris.platform.solrfacetsearch.model.config.SolrValueRangeSetModel;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyCommerceIndexedPropertyQueryFacetPopulatorUnitTest
{
	private static final String DEFAULT = "default";
	private static final boolean IS_QUERY_FACET = true;
	@InjectMocks
	private WileyCommerceIndexedPropertyQueryFacetPopulator wileyCommerceIndexedPropertyQueryFacetPopulator;
	@Mock
	private SolrIndexedPropertyModel solrIndexedPropertyModel;
	@Mock
	private IndexedProperty indexedProperty;
	@Mock
	private SolrValueRangeSetModel queryRangeSet;
	@Mock
	private SolrValueRangeModel valueRange;
	@Mock
	private Converter<SolrValueRangeSetModel, ValueRangeSet> valueRangeSetConverter;
	@Mock
	private ValueRangeSet convertedRangeSet;

	@Test
	public void populate()
	{
		when(solrIndexedPropertyModel.getQueryRangeSets()).thenReturn(Collections.singletonList(queryRangeSet));
		when(queryRangeSet.getSolrValueRanges()).thenReturn(Collections.singletonList(valueRange));
		when(solrIndexedPropertyModel.isQueryFacet()).thenReturn(IS_QUERY_FACET);
		when(valueRangeSetConverter.convert(queryRangeSet)).thenReturn(convertedRangeSet);
		when(convertedRangeSet.getQualifier()).thenReturn(DEFAULT);

		wileyCommerceIndexedPropertyQueryFacetPopulator.populate(solrIndexedPropertyModel, indexedProperty);

		verify(indexedProperty).setQueryValueRangeSets(argThat(new ArgumentMatcher<Map<String, ValueRangeSet>>()
		{
			@Override
			public boolean matches(final Object argument)
			{
				final Map<String, ValueRangeSet> map = (Map<String, ValueRangeSet>) argument;
				final ValueRangeSet valueRangeSet = map.get(DEFAULT);
				return convertedRangeSet.equals(valueRangeSet);
			}
		}));
		verify(indexedProperty).setQueryFacet(IS_QUERY_FACET);
	}

}
