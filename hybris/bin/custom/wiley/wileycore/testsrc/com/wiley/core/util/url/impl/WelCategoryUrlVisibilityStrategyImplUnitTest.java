package com.wiley.core.util.url.impl;

import com.wiley.core.category.WileyCategoryService;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.variants.model.VariantCategoryModel;
import de.hybris.platform.variants.model.VariantValueCategoryModel;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WelCategoryUrlVisibilityStrategyImplUnitTest
{
	private static final String ROOT_CODE = "rootCode";
	private static final String NON_CFA_SUB_CATEGORY = "nonCFASubCategory";
	@InjectMocks
	private WelCategoryUrlVisibilityStrategyImpl testInstance;

	@Mock
	private WileyCategoryService mockWileyCategoryService;

	@Mock
	private CategoryModel mockCategory;
	@Mock
	private VariantCategoryModel mockVariantCategory;
	@Mock
	private VariantValueCategoryModel mockNonCfaLevelCategory;
	@Mock
	private VariantValueCategoryModel mockCfaLevelCategory;
	@Mock
	private CategoryModel mockFreeTrialCategory;
	@Mock
	private CategoryModel mockGiftCardsCategory;
	@Mock
	private CategoryModel mockSupplementsCategory;
	@Mock
	private CategoryModel mockPartnersCategory;
	@Mock
	private CategoryModel mockNonCFASubCategory;

	@Before
	public void setUp()
	{
		when(mockWileyCategoryService.isFreeTrialCategory(mockFreeTrialCategory)).thenReturn(true);
		when(mockWileyCategoryService.isGiftCardsCategory(mockGiftCardsCategory)).thenReturn(true);
		when(mockWileyCategoryService.isSupplementsCategory(mockSupplementsCategory)).thenReturn(true);
		when(mockWileyCategoryService.isPartnersCategory(mockPartnersCategory)).thenReturn(true);
		when(mockWileyCategoryService.isCFALevelVariantCategory(mockCfaLevelCategory)).thenReturn(true);
		when(mockWileyCategoryService.isCFALevelVariantCategory(mockNonCfaLevelCategory)).thenReturn(false);

		when(mockCategory.getCode()).thenReturn(ROOT_CODE);
		when(mockNonCFASubCategory.getCode()).thenReturn(NON_CFA_SUB_CATEGORY);


		when(mockWileyCategoryService.getTopLevelCategoriesForCategory(mockNonCFASubCategory))
				.thenReturn(Arrays.asList(mockCategory));
		when(mockWileyCategoryService.getTopLevelCategoriesForCategory(mockCategory))
				.thenReturn(Arrays.asList(mockCategory));

	}

	@Test
	public void shouldReturnFalseIfCategoryIsNonCFAAndNonRoot()
	{
		assertFalse(testInstance.isUrlForItemVisible(mockNonCFASubCategory));
	}

	@Test
	public void shouldReturnFalseIfCategoryIsFreeTrial()
	{
		assertFalse(testInstance.isUrlForItemVisible(mockFreeTrialCategory));
	}

	@Test
	public void shouldReturnFalseIfCategoryIsGiftCards()
	{
		assertFalse(testInstance.isUrlForItemVisible(mockGiftCardsCategory));
	}

	@Test
	public void shouldReturnFalseIfCategoryIsSupplements()
	{
		assertFalse(testInstance.isUrlForItemVisible(mockSupplementsCategory));
	}

	@Test
	public void shouldReturnFalseIfCategoryIsPartners()
	{
		assertFalse(testInstance.isUrlForItemVisible(mockSupplementsCategory));
	}

	@Test
	public void shouldReturnFalseIfCategoryIsVariantCategory()
	{
		assertFalse(testInstance.isUrlForItemVisible(mockVariantCategory));
	}

	@Test
	public void shouldReturnFalseIfCategoryIsVariantValueAndNonCfaLevel()
	{
		assertFalse(testInstance.isUrlForItemVisible(mockNonCfaLevelCategory));
	}

	@Test
	public void shouldReturnTrueIfCategoryIsVariantValueAndCfaLevel()
	{
		assertTrue(testInstance.isUrlForItemVisible(mockCfaLevelCategory));
	}

	@Test
	public void shouldReturnTrueIfCategoryShouldBeVisible()
	{
		assertTrue(testInstance.isUrlForItemVisible(mockCategory));
	}
}
