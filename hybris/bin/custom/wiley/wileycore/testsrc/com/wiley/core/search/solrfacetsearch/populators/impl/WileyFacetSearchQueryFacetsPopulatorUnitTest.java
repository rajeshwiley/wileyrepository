package com.wiley.core.search.solrfacetsearch.populators.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.search.FacetField;
import de.hybris.platform.solrfacetsearch.search.FacetValueField;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.List;

import org.apache.solr.client.solrj.SolrQuery;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.search.solrfacetsearch.WileySearchQuery;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyFacetSearchQueryFacetsPopulatorUnitTest
{
	private static final String VALUE = "[NOW TO FROM]";
	private static final String ESCAPED_VALUE = "\\[NOW\\ TO\\ FROM\\]";

	@InjectMocks
	private WileyFacetSearchQueryFacetsPopulator wileyFacetSearchQueryFacetsPopulator;
	@Mock
	private IndexedProperty indexedProperty;
	@Mock
	private SolrQuery target;
	@Mock
	private WileySearchQuery wileySearchQueryMock;
	@Mock
	private FacetSearchConfig facetSearchConfigMock;
	@Mock
	private FacetField facetFieldMock1;
	@Mock
	private FacetField facetFieldMock2;
	@Mock
	private FacetField facetFieldMock3;
	@Mock
	private FacetValueField facetValueFieldMock1;
	@Mock
	private FacetValueField facetValueFieldMock2;
	@Mock
	private FacetValueField facetValueFieldMock3;
	public static final Integer FACET_LIMIT = 5;
	public static final Integer FACET_VIEW_MORE_LIMIT = 6;

	@Test
	public void escapeIfNeededWhenNonQueryFacetShouldEscape()
	{
		when(indexedProperty.isQueryFacet()).thenReturn(false);

		final String escaped = wileyFacetSearchQueryFacetsPopulator.escapeIfNeeded(VALUE, indexedProperty);

		assertEquals(escaped, ESCAPED_VALUE);
	}

	@Test
	public void escapeIfNeededWhenQueryFacetShouldNotEscape()
	{
		when(indexedProperty.isQueryFacet()).thenReturn(true);

		final String escaped = wileyFacetSearchQueryFacetsPopulator.escapeIfNeeded(VALUE, indexedProperty);

		assertEquals(escaped, VALUE);
	}

	@Test
	public void testGetFilteredQueryFacetsShouldNotFilterFacetValueFacetsIfFacetCodeProvided()
	{
		// given
		final String queryFacetCode = "facet field";
		final String queryFacetValueField = "facet value field";

		when(facetFieldMock1.getField()).thenReturn(queryFacetCode);
		when(facetFieldMock2.getField()).thenReturn("facet field 2");
		when(facetFieldMock3.getField()).thenReturn(queryFacetValueField);
		List<FacetField> facetFieldMocks = Lists.newArrayList(facetFieldMock1, facetFieldMock2, facetFieldMock3);

		when(facetValueFieldMock1.getField()).thenReturn("facet value field 1");
		when(facetValueFieldMock2.getField()).thenReturn("facet value field 2");
		when(facetValueFieldMock3.getField()).thenReturn(queryFacetValueField);
		List<FacetValueField> facetValueFieldMocks = Lists.newArrayList(facetValueFieldMock1, facetValueFieldMock2,
				facetValueFieldMock3);

		when(wileySearchQueryMock.getFacets()).thenReturn(facetFieldMocks);
		when(wileySearchQueryMock.getFacetValues()).thenReturn(facetValueFieldMocks);
		when(wileySearchQueryMock.getFacetCode()).thenReturn(queryFacetCode);

		// when
		List<FacetField> result = wileyFacetSearchQueryFacetsPopulator.getFilteredQueryFacets(wileySearchQueryMock);

		//then
		assertTrue(result.contains(facetFieldMock1));
		assertTrue(result.contains(facetFieldMock3));
	}

	@Test
	public void testGetFilteredQueryFacetsShouldNotFilterAnyFacetIfFacetCodeNotProvided()
	{
		// given
		List<FacetField> facetFieldMocks = Lists.newArrayList(facetFieldMock1, facetFieldMock2, facetFieldMock3);
		when(wileySearchQueryMock.getFacets()).thenReturn(facetFieldMocks);
		when(wileySearchQueryMock.getFacetCode()).thenReturn(null);

		// when
		List<FacetField> result = wileyFacetSearchQueryFacetsPopulator.getFilteredQueryFacets(wileySearchQueryMock);

		// then
		assertEquals(facetFieldMocks, result);
	}

	@Test
	public void testResolveFacetLimitShouldReturnConfiguredViewMoreFacetLimitIfQueryFlagTrue()
	{
		// given
		when(wileySearchQueryMock.getUseFacetViewMoreLimit()).thenReturn(true);
		when(wileySearchQueryMock.getFacetSearchConfig()).thenReturn(facetSearchConfigMock);
		when(facetSearchConfigMock.getFacetLimit()).thenReturn(FACET_LIMIT);
		when(facetSearchConfigMock.getFacetViewMoreLimit()).thenReturn(FACET_VIEW_MORE_LIMIT);

		// when
		Integer result = wileyFacetSearchQueryFacetsPopulator.resolveFacetLimit(wileySearchQueryMock);

		// then
		assertEquals(FACET_VIEW_MORE_LIMIT, result);
	}

	@Test
	public void testResolveFacetLimitShouldReturnConfiguredLimitIfQueryFlagNotProvided()
	{
		// given
		when(wileySearchQueryMock.getUseFacetViewMoreLimit()).thenReturn(null);
		when(wileySearchQueryMock.getFacetSearchConfig()).thenReturn(facetSearchConfigMock);
		when(facetSearchConfigMock.getFacetLimit()).thenReturn(FACET_LIMIT);
		when(facetSearchConfigMock.getFacetViewMoreLimit()).thenReturn(FACET_VIEW_MORE_LIMIT);


		// when
		Integer result = wileyFacetSearchQueryFacetsPopulator.resolveFacetLimit(wileySearchQueryMock);

		// then
		assertEquals(FACET_LIMIT, result);
	}

	@Test
	public void testResolveFacetLimitShouldReturnConfiguredLimitIfQueryFlagFalse()
	{
		// given
		when(wileySearchQueryMock.getUseFacetViewMoreLimit()).thenReturn(false);
		when(wileySearchQueryMock.getFacetSearchConfig()).thenReturn(facetSearchConfigMock);
		when(facetSearchConfigMock.getFacetLimit()).thenReturn(FACET_LIMIT);
		when(facetSearchConfigMock.getFacetViewMoreLimit()).thenReturn(FACET_VIEW_MORE_LIMIT);


		// when
		Integer result = wileyFacetSearchQueryFacetsPopulator.resolveFacetLimit(wileySearchQueryMock);

		// then
		assertEquals(FACET_LIMIT, result);
	}

	@Test
	public void testResolveFacetLimitShouldReturnDefaultLimitIfQueryLimitAndConfiguredLimitNotProvided()
	{
		// given
		when(wileySearchQueryMock.getUseFacetViewMoreLimit()).thenReturn(null);
		when(wileySearchQueryMock.getFacetSearchConfig()).thenReturn(facetSearchConfigMock);
		when(facetSearchConfigMock.getFacetLimit()).thenReturn(null);

		// when
		Integer result = wileyFacetSearchQueryFacetsPopulator.resolveFacetLimit(wileySearchQueryMock);

		// then
		assertEquals(WileyFacetSearchQueryFacetsPopulator.DEFAULT_LIMIT, result.intValue());
	}

	@Test
	public void testResolveTermFacetLimitShouldReturnDefaultLimitForDefaultFacetLimit()
	{
		// given

		// when
		Integer result = wileyFacetSearchQueryFacetsPopulator.resolveTermFacetLimit(
				wileyFacetSearchQueryFacetsPopulator.DEFAULT_LIMIT);

		// then
		assertEquals(WileyFacetSearchQueryFacetsPopulator.DEFAULT_LIMIT, result.intValue());
	}

	@Test
	public void testResolveTermFacetLimitShouldReturnHigherLimitForNonDefaultFacetLimit()
	{
		// given

		// when
		Integer result = wileyFacetSearchQueryFacetsPopulator.resolveTermFacetLimit(FACET_LIMIT);

		// then
		assertEquals(FACET_LIMIT + 1, result.intValue());
	}

}
