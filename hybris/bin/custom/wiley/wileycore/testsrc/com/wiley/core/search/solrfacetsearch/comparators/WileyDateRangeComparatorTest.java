package com.wiley.core.search.solrfacetsearch.comparators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.solrfacetsearch.search.FacetValue;
import static org.junit.Assert.assertEquals;

import org.junit.Test;


@UnitTest
public class WileyDateRangeComparatorTest
{

	private static final int ANY_COUNT = 0;
	private static final boolean ANY_SELECTED = true;

	private static final String NOW_MINUS_30_DAY = "NOW-30DAY TO NOW";
	private static final String NOW_MINUS_90_DAY = "NOW-90DAY TO NOW";
	private static final String NOW_MINUS_1_YEAR = "NOW-1YEAR TO NOW";
	private static final String NOW_MINUS_3_YEAR = "NOW-3YEAR TO NOW";
	private static final String NOW_MINUS_5_YEAR = "NOW-5YEAR TO NOW";
	private static final String NOW_MINUS_20_YEAR = "* TO NOW";
	private static final String UNKNOWN_VALUE = "-5WEEK TO NOW";
	private static final String INCORRECT_VALUE = "INCORRECT";
	public static final int IS_BIGGER = 1;
	public static final int IS_SMALLER = -1;
	public static final int IS_EQUAL = 0;



	private WileyDateRangeComparator testInstance = new WileyDateRangeComparator();

	@Test
	public void shouldSuccessfullyCompareDefaultPatterns() {
		assertEquals(IS_BIGGER, compare(NOW_MINUS_90_DAY, NOW_MINUS_30_DAY));
		assertEquals(IS_BIGGER, compare(NOW_MINUS_1_YEAR, NOW_MINUS_90_DAY));
		assertEquals(IS_BIGGER, compare(NOW_MINUS_3_YEAR, NOW_MINUS_1_YEAR));
		assertEquals(IS_BIGGER, compare(NOW_MINUS_5_YEAR, NOW_MINUS_3_YEAR));
		assertEquals(IS_BIGGER, compare(NOW_MINUS_20_YEAR, NOW_MINUS_5_YEAR));

		assertEquals(IS_SMALLER, compare(NOW_MINUS_30_DAY, NOW_MINUS_90_DAY));
		assertEquals(IS_SMALLER, compare(NOW_MINUS_90_DAY, NOW_MINUS_1_YEAR));
		assertEquals(IS_SMALLER, compare(NOW_MINUS_1_YEAR, NOW_MINUS_3_YEAR));
		assertEquals(IS_SMALLER, compare(NOW_MINUS_3_YEAR, NOW_MINUS_5_YEAR));
		assertEquals(IS_SMALLER, compare(NOW_MINUS_5_YEAR, NOW_MINUS_20_YEAR));
	}

	@Test
	public void shouldHandleIncorrectValues() {
		assertEquals(IS_BIGGER, compare(NOW_MINUS_30_DAY, UNKNOWN_VALUE));
		assertEquals(IS_SMALLER, compare(UNKNOWN_VALUE, NOW_MINUS_30_DAY));
		assertEquals(IS_EQUAL, compare(UNKNOWN_VALUE, INCORRECT_VALUE));
	}

	int compare(final String name1, final String name2) {
		return testInstance.compare(givenValue(name1), givenValue(name2));
	}

	private FacetValue givenValue(final String name) {
		return new FacetValue(name, ANY_COUNT, ANY_SELECTED);
	}

}