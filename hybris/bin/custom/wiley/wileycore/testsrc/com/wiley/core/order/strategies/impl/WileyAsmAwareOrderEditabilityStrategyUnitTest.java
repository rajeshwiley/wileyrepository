package com.wiley.core.order.strategies.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.assistedserviceservices.AssistedServiceService;
import de.hybris.platform.assistedserviceservices.utils.AssistedServiceSession;
import de.hybris.platform.core.model.order.OrderModel;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.order.strategies.WileyOrderEditabilityStrategy;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyAsmAwareOrderEditabilityStrategyUnitTest
{

	@InjectMocks
	private WileyAsmAwareOrderEditabilityStrategyImpl orderEditabilityStrategy;
	@Mock
	private WileyOrderEditabilityStrategy wileyAsmOrderEditabilityStrategy, wileyDefaultOrderEditabilityStrategy;
	@Mock
	private OrderModel orderMock;
	@Mock
	private AssistedServiceService assistedServiceServiceMock;
	@Mock
	private AssistedServiceSession assistedServiceSessionMock;

	@Test
	public void behaviourTestWithAsmSession()
	{
		when(wileyAsmOrderEditabilityStrategy.isEditable(anyObject())).thenReturn(true);
		when(wileyAsmOrderEditabilityStrategy.hasAllEditableEntries(anyObject())).thenReturn(true);
		when(assistedServiceServiceMock.getAsmSession()).thenReturn(assistedServiceSessionMock);

		orderEditabilityStrategy.hasAllEditableEntries(orderMock);
		orderEditabilityStrategy.isEditable(orderMock);

		verify(wileyAsmOrderEditabilityStrategy).hasAllEditableEntries(anyObject());
		verify(wileyAsmOrderEditabilityStrategy).isEditable(anyObject());
		verify(wileyDefaultOrderEditabilityStrategy, never()).hasAllEditableEntries(anyObject());
		verify(wileyDefaultOrderEditabilityStrategy, never()).isEditable(anyObject());
	}

	@Test
	public void behaviourTestWithoutAsmSession()
	{
		when(wileyDefaultOrderEditabilityStrategy.isEditable(anyObject())).thenReturn(true);
		when(wileyDefaultOrderEditabilityStrategy.hasAllEditableEntries(anyObject())).thenReturn(true);
		when(assistedServiceServiceMock.getAsmSession()).thenReturn(null);

		orderEditabilityStrategy.hasAllEditableEntries(orderMock);
		orderEditabilityStrategy.isEditable(orderMock);

		verify(wileyDefaultOrderEditabilityStrategy).hasAllEditableEntries(anyObject());
		verify(wileyDefaultOrderEditabilityStrategy).isEditable(anyObject());
		verify(wileyAsmOrderEditabilityStrategy, never()).hasAllEditableEntries(anyObject());
		verify(wileyAsmOrderEditabilityStrategy, never()).isEditable(anyObject());
	}

}
