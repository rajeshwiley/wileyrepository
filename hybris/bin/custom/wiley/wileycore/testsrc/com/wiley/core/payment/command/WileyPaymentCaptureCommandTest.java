package com.wiley.core.payment.command;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.payment.commands.result.CaptureResult;
import de.hybris.platform.payment.dto.TransactionStatus;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.integration.wpg.WileyPaymentGateway;
import com.wiley.core.payment.enums.WileyTransactionStatusEnum;
import com.wiley.core.payment.request.WileyCaptureRequest;
import com.wiley.core.payment.response.WileyCaptureResult;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyPaymentCaptureCommandTest
{
	private static final String AMOUNT = "10.00";
	private static final String AMOUNT_WITH_DIFFERENT_SCALE = "10.0000000";
	private static final String AMOUNT_WITH_DIFFERENT_VALUE = "10.01";
	@Mock
	private WileyPaymentGateway mockWileyPaymentGateway;

	@InjectMocks
	private WileyPaymentCaptureCommand underTest;

	@Mock
	private Map<String, String> mockTokenSettleResponse;
	@Mock
	private WileyCaptureRequest mockCaptureCommandRequest;
	@Mock
	private WileyCaptureResult mockCaptureCommandResponse;


	@Before
	public void setup()
	{
		when(mockWileyPaymentGateway.capture(mockCaptureCommandRequest)).thenReturn(mockCaptureCommandResponse);

		when(mockCaptureCommandResponse.getTotalAmount()).thenReturn(new BigDecimal(AMOUNT));
		when(mockCaptureCommandResponse.getStatus()).thenReturn(WileyTransactionStatusEnum.SUCCESS);

		when(mockCaptureCommandRequest.getTotalAmount()).thenReturn(new BigDecimal(AMOUNT));


	}

	@Test
	public void shouldCallClientAndConvertResult()
	{
		CaptureResult result = underTest.perform(mockCaptureCommandRequest);
		assertSame(mockCaptureCommandResponse, result);
	}

	@Test
	public void shouldHandleAmountMismatchForSuccessfulTransaction()
	{
		when(mockCaptureCommandRequest.getTotalAmount()).thenReturn(new BigDecimal(AMOUNT_WITH_DIFFERENT_VALUE));
		WileyCaptureResult result = (WileyCaptureResult) underTest.perform(mockCaptureCommandRequest);
		assertNotNull(result);
		assertEquals(TransactionStatus.ERROR, result.getTransactionStatus());
		assertEquals(WileyTransactionStatusEnum.AMOUNT_MISMATCH, result.getStatus());
	}

	@Test
	public void shouldNotHandleAmountMismatchForFailedTransaction()
	{
		when(mockCaptureCommandResponse.getStatus()).thenReturn(WileyTransactionStatusEnum.DECLINED);

		when(mockCaptureCommandRequest.getTotalAmount()).thenReturn(new BigDecimal(AMOUNT_WITH_DIFFERENT_VALUE));
		CaptureResult result = underTest.perform(mockCaptureCommandRequest);
		assertSame(mockCaptureCommandResponse, result);

	}

	@Test
	public void shouldTolerateAmountScaleMismatch()
	{
		when(mockCaptureCommandRequest.getTotalAmount()).thenReturn(new BigDecimal(AMOUNT_WITH_DIFFERENT_SCALE));
		CaptureResult result = underTest.perform(mockCaptureCommandRequest);
		assertSame(mockCaptureCommandResponse, result);

	}

	@Test
	public void shouldHandleOtherExceptions()
	{
		doThrow(NullPointerException.class).when(mockWileyPaymentGateway).capture(mockCaptureCommandRequest);
		WileyCaptureResult result = (WileyCaptureResult) underTest.perform(mockCaptureCommandRequest);
		assertNotNull(result);
		assertEquals(TransactionStatus.ERROR, result.getTransactionStatus());
		assertEquals(WileyTransactionStatusEnum.GENERAL_SYSTEM_ERROR, result.getStatus());
	}
}
