package com.wiley.core.wileyb2c.product.access;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.OrderService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.services.BaseStoreService;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.junit.Test;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.wileyb2c.customer.WileyB2CCustomerAccountService;


/**
 * @author Dzmitryi_Halahayeu
 */
@IntegrationTest
public class Wileyb2cDownloadDigitalProductServiceIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	private static final String CUSTOMER_ID = "customer@test.com";
	private static final String BASE_STORE = "wileyb2c";
	private static final String INCORRECT_ORDER = "wileyb2corder_1";
	private static final String CORRECT_ORDER = "wileyb2corder_2";
	@Resource
	private Wileyb2cDownloadDigitalProductService wileyb2cDownloadDigitalProductService;
	@Resource
	private WileyB2CCustomerAccountService wileyB2CCustomerAccountService;
	@Resource
	private OrderService orderService;
	@Resource
	private BaseStoreService baseStoreService;
	@Resource
	private UserService userService;


	@Test
	public void testWhenProductIsCorrectThenCorrectUrlGenerated() throws Exception
	{
		OrderModel order = wileyB2CCustomerAccountService.getOrderForCode(
				(CustomerModel) userService.getUserForUID(CUSTOMER_ID), CORRECT_ORDER,
				baseStoreService.getBaseStoreForUid(BASE_STORE));
		OrderEntryModel entryModel = orderService.getEntryForNumber(order, 0);

		String redirectUrl = wileyb2cDownloadDigitalProductService.generateRedirectUrl(entryModel);

		assertNotNull(redirectUrl);
		Pattern urlPattern = Pattern.compile(
				Pattern.quote("http://d2wjvrn2hd9hlg.cloudfront.net/") + Pattern
						.quote(entryModel.getProduct().getDigitalContentPath())
						+ Pattern.quote("?Expires=") + ".+" + Pattern.quote("&Signature=") + ".+" + Pattern
						.quote("Key-Pair-Id=APKAIJHJ537PSH2JAE3Q"));
		assertTrue(urlPattern.matcher(redirectUrl).find());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testWhenProductIsNotCorrectThenShouldThrowException() throws Exception
	{
		OrderModel order = wileyB2CCustomerAccountService.getOrderForCode(
				(CustomerModel) userService.getUserForUID(CUSTOMER_ID), INCORRECT_ORDER,
				baseStoreService.getBaseStoreForUid(BASE_STORE));
		OrderEntryModel entryModel = orderService.getEntryForNumber(order, 0);

		wileyb2cDownloadDigitalProductService.generateRedirectUrl(entryModel);
	}
}
