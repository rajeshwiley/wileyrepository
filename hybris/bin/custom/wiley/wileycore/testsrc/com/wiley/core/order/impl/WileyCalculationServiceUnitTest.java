package com.wiley.core.order.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.strategies.calculation.OrderRequiresCalculationStrategy;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.util.DiscountValue;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyCalculationServiceUnitTest
{

	@Mock
	private ModelService modelService;
	@Mock
	private OrderRequiresCalculationStrategy orderRequiresCalculationStrategy;
	@InjectMocks
	private WileyCalculationService testInstance = new WileyCalculationService();

	@Mock
	private AbstractOrderEntryModel entryModelOneMock;
	@Mock
	private AbstractOrderEntryModel entryModelTwoMock;

	private CartModel cartModel = new CartModel();

	@Before
	public void setUp()
	{
		when(entryModelOneMock.getSubtotalPrice()).thenReturn(3.0);
		when(entryModelTwoMock.getSubtotalPrice()).thenReturn(9.0);

		when(orderRequiresCalculationStrategy.requiresCalculation(any(AbstractOrderEntryModel.class))).thenReturn(true);
		cartModel.setEntries(Arrays.asList(entryModelOneMock, entryModelTwoMock));
	}

	@Test
	public void subtotalWithoutDiscountShouldBeCalculated()
	{

		testInstance.calculateSubtotalWithoutDiscount(cartModel);
		assertEquals(Double.valueOf(12.0), cartModel.getSubTotalWithoutDiscount());
		verify(modelService).save(cartModel);
	}

	@Test
	public void totalPriceShouldNotBeNegativeAfterApplyingDiscount()
	{
		DiscountValue discountValue = new DiscountValue("code", 2.5d, true, 2.5d, "US");
		BigDecimal result = testInstance.subtractDiscount(BigDecimal.valueOf(2.2d), discountValue);
		assertEquals(BigDecimal.ZERO, result);
	}

}
