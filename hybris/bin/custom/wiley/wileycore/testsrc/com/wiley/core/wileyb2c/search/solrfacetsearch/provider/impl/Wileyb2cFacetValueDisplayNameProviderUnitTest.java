package com.wiley.core.wileyb2c.search.solrfacetsearch.provider.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.ValueRange;
import de.hybris.platform.solrfacetsearch.config.ValueRangeSet;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Collections;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.wiley.core.search.solrfacetsearch.WileyFacetSearchService.RANGE_PATTERN;
import static org.springframework.util.Assert.isNull;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cFacetValueDisplayNameProviderUnitTest
{
	private static final String FROM = "*";
	private static final String TO = "NOW";
	private static final String ANOTHER_FROM = "NOW-5DAY";
	private static final String ANOTHER_TO = "NOW-2DAY";
	private static final String NAME = "name";
	@InjectMocks
	private Wileyb2cFacetValueDisplayNameProvider wileyb2cFacetValueDisplayNameProvider;
	@Mock
	private SearchQuery searchQuery;
	@Mock
	private IndexedProperty indexedProperty;
	@Mock
	private ValueRangeSet queryValueRangeSet;
	@Mock
	private ValueRange valueRange;

	@Test
	public void getDisplayNameWhenNoQueryValueRangeSetsShouldReturnNull()
	{
		when(indexedProperty.getQueryValueRangeSets()).thenReturn(Collections.emptyMap());

		final String displayName = wileyb2cFacetValueDisplayNameProvider.getDisplayName(searchQuery, indexedProperty,
				createValue(FROM, TO));

		isNull(displayName);
	}

	@Test
	public void getDisplayNameWhenNoQueryValueRangesShouldReturnNull()
	{
		when(indexedProperty.getQueryValueRangeSets()).thenReturn(Collections.singletonMap("default", queryValueRangeSet));
		when(queryValueRangeSet.getValueRanges()).thenReturn(Collections.emptyList());

		final String displayName = wileyb2cFacetValueDisplayNameProvider.getDisplayName(searchQuery, indexedProperty,
				createValue(FROM, TO));

		isNull(displayName);
	}

	private String createValue(final String from, final String to)
	{
		return String.format(RANGE_PATTERN, from, to);
	}

	@Test
	public void getDisplayNameWheNoMatchingValueRangesShouldReturnNull()
	{
		when(indexedProperty.getQueryValueRangeSets()).thenReturn(Collections.singletonMap("default", queryValueRangeSet));
		when(queryValueRangeSet.getValueRanges()).thenReturn(Collections.singletonList(valueRange));
		when(valueRange.getFrom()).thenReturn(ANOTHER_FROM);
		when(valueRange.getTo()).thenReturn(ANOTHER_TO);

		final String displayName = wileyb2cFacetValueDisplayNameProvider.getDisplayName(searchQuery, indexedProperty,
				createValue(FROM, TO));

		isNull(displayName);
	}

	@Test
	public void getDisplayNameWhenMatchingValueRangesShouldReturnValueRangeName()
	{
		when(indexedProperty.getQueryValueRangeSets()).thenReturn(Collections.singletonMap("default", queryValueRangeSet));
		when(queryValueRangeSet.getValueRanges()).thenReturn(Collections.singletonList(valueRange));
		when(valueRange.getFrom()).thenReturn(FROM);
		when(valueRange.getTo()).thenReturn(TO);
		when(valueRange.getName()).thenReturn(NAME);

		final String displayName = wileyb2cFacetValueDisplayNameProvider.getDisplayName(searchQuery, indexedProperty,
				createValue(FROM, TO));

		assertEquals(displayName, NAME);
	}

}
