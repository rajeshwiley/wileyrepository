package com.wiley.core.payment.strategies.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.payment.data.CustomerInfoData;
import de.hybris.platform.acceleratorservices.payment.data.PaymentInfoData;
import de.hybris.platform.acceleratorservices.payment.data.SubscriptionInfoData;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.impl.DefaultCMSSiteService;
import de.hybris.platform.commerceservices.customer.CustomerEmailResolutionService;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.paymentinfo.CardPaymentInfoCcOwnerGenerator;


/**
 * Default unit test for {@link WileyCreditCardPaymentInfoCreateStrategy}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyCreditCardPaymentInfoCreateStrategyUnitTest
{

	private static final String BILLING_PHONE1 = "123-23-23";
	private static final boolean SAVE_IN_ACCOUNT = true;
	private static final String TEST_OWNER_FIRST_NAME_LAST_NAME = "TestOwnerFirstName LastName";
	private static final String TEST_SITE = "testSite";
	@Mock
	private ModelService modelService;
	@Spy
	@InjectMocks
	private WileyCreditCardPaymentInfoCreateStrategy wileyCreditCardPaymentInfoCreateStrategy;

	// Test data
	@Mock
	private CommonI18NService commonI18NService;
	@Mock
	private CustomerEmailResolutionService customerEmailResolutionService;
	@Mock
	private CardPaymentInfoCcOwnerGenerator cardPaymentInfoCcOwnerGeneratorMock;
	@Mock
	private CustomerModel customerModelMock;
	@Mock
	private CustomerInfoData customerInfoDataMock;
	@Mock
	private SubscriptionInfoData subscriptionInfoDataMock;
	@Mock
	private PaymentInfoData paymentInfoDataMock;
	@Mock
	private CreditCardPaymentInfoModel creditCardPaymentInfoModelMock;
	@Mock
	private AddressModel billingAddressMock;
	@Mock
	private CMSSiteModel mockBaseSite;
	@Mock
	private DefaultCMSSiteService cmsSiteService;

	@Before
	public void setUp() throws Exception
	{
		when(mockBaseSite.getUid()).thenReturn(TEST_SITE);

		when(cmsSiteService.getCurrentSite()).thenReturn(mockBaseSite);

		when(customerInfoDataMock.getBillToPhoneNumber()).thenReturn(BILLING_PHONE1);

		when(modelService.create(AddressModel.class)).thenReturn(billingAddressMock);

		when(modelService.create(CreditCardPaymentInfoModel.class)).thenReturn(creditCardPaymentInfoModelMock);

		Mockito.doReturn(creditCardPaymentInfoModelMock)
				.when(wileyCreditCardPaymentInfoCreateStrategy)
				.createCreditCardPaymentInfo(
						subscriptionInfoDataMock,
						paymentInfoDataMock,
						billingAddressMock,
						customerModelMock,
						SAVE_IN_ACCOUNT);

		when(cardPaymentInfoCcOwnerGeneratorMock.getCCOwnerFromBillingAddress(same(billingAddressMock))).thenReturn(
				Optional.of(TEST_OWNER_FIRST_NAME_LAST_NAME));

		when(paymentInfoDataMock.getCardCardType()).thenReturn("001");

		when(wileyCreditCardPaymentInfoCreateStrategy.getCmsSiteService()).thenReturn(cmsSiteService);
	}

	@Test
	public void shouldPopulatePhoneFromCustomerInfoDataDuringSavingSubscription()
	{
		// Given

		// When
		wileyCreditCardPaymentInfoCreateStrategy.saveSubscription(
				customerModelMock,
				customerInfoDataMock,
				subscriptionInfoDataMock,
				paymentInfoDataMock,
				SAVE_IN_ACCOUNT);

		// Then
		verify(billingAddressMock).setPhone1(eq(BILLING_PHONE1));
		verify(billingAddressMock).setOwner(creditCardPaymentInfoModelMock);
		verify(modelService).save(creditCardPaymentInfoModelMock);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldNotPopulatePhoneFromCustomerInfoDataDuringSavingSubscriptionIfItIsNull()
	{
		// Given
		final CustomerInfoData customerInfoData = null;

		// When
		wileyCreditCardPaymentInfoCreateStrategy.saveSubscription(customerModelMock, customerInfoData, subscriptionInfoDataMock,
				paymentInfoDataMock, SAVE_IN_ACCOUNT);

		// Then
		// check expected exception
	}

	@Test
	public void testCcOwnerDuringCreditCardPaymentCreationWithEmptyCardAccountHolderName()
	{
		// Given
		when(paymentInfoDataMock.getCardAccountHolderName()).thenReturn("");

		// When
		wileyCreditCardPaymentInfoCreateStrategy.createCreditCardPaymentInfo(
				subscriptionInfoDataMock,
				paymentInfoDataMock,
				billingAddressMock,
				customerModelMock,
				false);

		// Then
		verify(creditCardPaymentInfoModelMock).setCcOwner(eq(TEST_OWNER_FIRST_NAME_LAST_NAME));
	}

	@Test
	public void testCcOwnerDuringCreditCardPaymentCreationWithNullCardAccountHolderName()
	{
		// Given
		when(paymentInfoDataMock.getCardAccountHolderName()).thenReturn(null);

		// When
		wileyCreditCardPaymentInfoCreateStrategy.createCreditCardPaymentInfo(
				subscriptionInfoDataMock,
				paymentInfoDataMock,
				billingAddressMock,
				customerModelMock,
				false);

		// Then
		verify(creditCardPaymentInfoModelMock).setCcOwner(eq(TEST_OWNER_FIRST_NAME_LAST_NAME));
	}

	@Test
	public void testCcOwnerDuringCreditCardPaymentCreationWithCardAccountHolderName()
	{
		// Given
		final String validCardAccountHolderName = "ValidCardAccountHolderName";
		when(paymentInfoDataMock.getCardAccountHolderName()).thenReturn(validCardAccountHolderName);

		// When
		wileyCreditCardPaymentInfoCreateStrategy.createCreditCardPaymentInfo(
				subscriptionInfoDataMock,
				paymentInfoDataMock,
				billingAddressMock,
				customerModelMock,
				false);

		// Then
		verify(creditCardPaymentInfoModelMock).setCcOwner(eq(validCardAccountHolderName));
	}
}
