package com.wiley.core.search.solrfacetsearch.populators.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.solrfacetsearch.search.FieldNameTranslator;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.solrfacetsearch.search.impl.SearchQueryConverterData;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.apache.solr.client.solrj.SolrQuery;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static de.hybris.platform.solrfacetsearch.provider.FieldNameProvider.FieldType.INDEX;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyFacetSearchQueryFieldsListPopulatorUnitTest
{
	private static final String INDEXED_FIELD_1 = "field1";
	private static final String TRANSLATED_INDEXED_FIELD_1 = "translatedField1";
	private static final String INDEXED_FIELD_2 = "field2";
	private static final String TRANSLATED_INDEXED_FIELD_2 = "translatedField2";

	@InjectMocks
	private WileyFacetSearchQueryFieldsListPopulator testInstance;

	@Mock
	private FieldNameTranslator mockFieldNameTranslator;
	@Mock
	private SearchQueryConverterData mockSearchQueryConverterData;
	@Mock
	private SearchQuery mockSearchQuery;

	private SolrQuery solrQuery;

	@Before
	public void setUp()
	{
		when(mockSearchQueryConverterData.getSearchQuery()).thenReturn(mockSearchQuery);
		when(mockSearchQuery.getFields()).thenReturn(Arrays.asList(INDEXED_FIELD_1, INDEXED_FIELD_2));
		when(mockFieldNameTranslator.translate(mockSearchQuery, INDEXED_FIELD_1, INDEX)).thenReturn(TRANSLATED_INDEXED_FIELD_1);
		when(mockFieldNameTranslator.translate(mockSearchQuery, INDEXED_FIELD_2, INDEX)).thenReturn(TRANSLATED_INDEXED_FIELD_2);
		solrQuery = new SolrQuery();
	}

	@Test
	public void shouldAddCommonFieldsToSolrQueryFieldsList()
	{
		testInstance.populate(mockSearchQueryConverterData, solrQuery);
		String fields = solrQuery.getFields();
		assertTrue(fields.contains("indexOperationId_long"));
		assertTrue(fields.contains("id"));
		assertTrue(fields.contains("pk"));
		assertTrue(fields.contains("catalogId"));
		assertTrue(fields.contains("catalogVersion"));
	}

	@Test
	public void shouldAddIndexedTypeFieldsToSolrQueryFieldsList()
	{
		testInstance.populate(mockSearchQueryConverterData, solrQuery);
		String fields = solrQuery.getFields();
		assertTrue(fields.contains(TRANSLATED_INDEXED_FIELD_1));
		assertTrue(fields.contains(TRANSLATED_INDEXED_FIELD_2));
	}

}
