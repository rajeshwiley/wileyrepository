package com.wiley.core.jobs;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.commerceservices.order.impl.DefaultCommerceCartService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.CronJobService;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.model.AbandonCartEmailCronJobModel;
import com.wiley.core.model.AbandonCartEmailHistoryEntryModel;
import com.wiley.core.subscription.services.WileySubscriptionService;


/**
 * Default integration test for {@link AbandonCartEmailJobPerformable}.
 */
@IntegrationTest
public class AbandonCartEmailJobPerformableIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	private static final Logger LOG = Logger.getLogger(AbandonCartEmailJobPerformableIntegrationTest.class);
	private static final int MILLISECONDS_IN_SECOND = 1000;
	private static final int ABANDON_CART_AGE_IN_SECONDS = 5;
	private static final Double TOTAL_PRICE = Double.valueOf(34);
	private static final Map<String, String> CARTS;
	private static final List<String> AVAILABLE_CART_CODES;
	private static final String CART_CODE_ANONYMOUS = "cart_code_anonymous";
	private static final String CART_CODE_ANOTHER_SITE = "cart_code_another_site";
	private static final String CART_CODE_RECENTLY_UPDATED1 = "cart_code_recently_updated";
	private static final String CART_NO_ENTRY_CART = "cart_no_entry_cart";
	private static final String CART_CODE_ALREADY_SENT = "cart_code_already_sent";
	private static final String TEST_ANY_TIME = "2018-04-03T04:14:00";
	private static final String CART_CODE_RECENTLY_UPDATED = "cart_code_recently_updated";

	static
	{
		CARTS = new HashMap<String, String>(6);
		CARTS.put("cart_code_recently_updated", "mockuser1@mock.com");
		CARTS.put("cart_code_2", "mockuser2@mock.com");
		CARTS.put(CART_CODE_ALREADY_SENT, "mockuser3@mock.com");
		CARTS.put(CART_CODE_ANOTHER_SITE, "mockuser4@mock.com");
		CARTS.put(CART_CODE_ANONYMOUS, "anonymous");
		CARTS.put(CART_NO_ENTRY_CART, "mockuser4@mock.com");

		AVAILABLE_CART_CODES = new ArrayList<>(4);
		AVAILABLE_CART_CODES.add(CART_CODE_ANONYMOUS);
		AVAILABLE_CART_CODES.add(CART_CODE_ANOTHER_SITE);
		AVAILABLE_CART_CODES.add(CART_CODE_RECENTLY_UPDATED1);
		AVAILABLE_CART_CODES.add(CART_NO_ENTRY_CART);
	}

	@Resource
	private CronJobService cronJobService;

	@Resource
	private WileySubscriptionService wileySubscriptionService;

	@Resource
	private ModelService modelService;

	@Resource
	private DefaultCommerceCartService defaultCommerceCartService;

	@Resource
	private UserService userService;

	@Resource
	private AbandonCartEmailJobPerformable abandonCartEmailJobPerformable;

	@Before
	public void setUp() throws Exception
	{
		importCsv("/wileycore/test/jobs/testAbandonCartJobPerformable.impex", DEFAULT_ENCODING);
		importCsv("/wileycore/test/cart/cartsForEmailAbandonCart.impex", DEFAULT_ENCODING);
	}

	@Test
	public void testPerform() throws Exception
	{
		// Given
		Thread.sleep(MILLISECONDS_IN_SECOND * ABANDON_CART_AGE_IN_SECONDS);

		// modify recentlyChangedCart - recentlyChangedCart should not be processed by cronJob
		CartModel recentlyChangedCart = defaultCommerceCartService.getCartForCodeAndUser(CART_CODE_RECENTLY_UPDATED,
				userService.getUserForUID(CARTS.get(CART_CODE_RECENTLY_UPDATED)));
		recentlyChangedCart.setTotalPrice(TOTAL_PRICE);
		modelService.save(recentlyChangedCart);
		// we expect that the event listener modifies sendAbandonEmailDate.
		CartModel cartEmailAlreadySent = defaultCommerceCartService.getCartForCodeAndUser(CART_CODE_ALREADY_SENT,
				userService.getUserForUID(CARTS.get(CART_CODE_ALREADY_SENT)));
		createHistoryEntry(cartEmailAlreadySent);

		// getting cron job for executing abandonCartEmailJobPerformable.perform(cronJob).
		final CronJobModel cronJob = cronJobService.getCronJob("testSite1-abandonCartEmailCronJob");

		// When
		final PerformResult performResult = abandonCartEmailJobPerformable.perform((AbandonCartEmailCronJobModel) cronJob);

		// Then
		assertEquals(CronJobStatus.FINISHED, performResult.getStatus());
		assertEquals(CronJobResult.SUCCESS, performResult.getResult());

		for (Map.Entry<String, String> entry : CARTS.entrySet())
		{
			String code = entry.getKey();
			String user = entry.getValue();
			CartModel cartForUserAndCode = defaultCommerceCartService.getCartForCodeAndUser(code,
					userService.getUserForUID(user));
			Collection<AbandonCartEmailHistoryEntryModel> abandonCartEmailHistory =
					cartForUserAndCode.getAbandonCartEmailHistory();
			if (AVAILABLE_CART_CODES.contains(code))
			{
				assertTrue("AbandonCartEmailHistory list should be empty",
						CollectionUtils.isEmpty(abandonCartEmailHistory));
			}
			else
			{
				assertTrue("AbandonCartEmailHistory list should not be empty",
						CollectionUtils.isNotEmpty(abandonCartEmailHistory));
				assertEquals("AbandonCartEmailHistory list size should be 1", 1, abandonCartEmailHistory.size());
				Optional<AbandonCartEmailHistoryEntryModel> first = abandonCartEmailHistory.stream().findFirst();
				if (first.isPresent())
				{
					assertNotNull(first.get().getEmailSentDate());
				}
				else
				{
					fail("AbandonCartEmailHistory list should contain 1 record" + cartForUserAndCode.getCode());
				}
			}
		}
	}

	private void createHistoryEntry(final CartModel cartModel)
	{
		final Instant instant = Instant.now();
		AbandonCartEmailHistoryEntryModel abandonCartEmailInformation = modelService.create(
				AbandonCartEmailHistoryEntryModel.class);
		abandonCartEmailInformation.setOrder(cartModel);
		abandonCartEmailInformation.setEmailSentDate(Date.from(instant));
		modelService.save(abandonCartEmailInformation);
	}
}
