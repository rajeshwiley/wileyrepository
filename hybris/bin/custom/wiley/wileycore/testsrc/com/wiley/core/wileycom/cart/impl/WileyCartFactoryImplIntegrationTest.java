package com.wiley.core.wileycom.cart.impl;

import com.wiley.core.cart.impl.WileyCartFactoryImpl;
import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.site.BaseSiteService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.enums.OrderType;
import com.wiley.core.servicelayer.i18n.WileycomI18NService;


/**
 * Test for {@link WileyCartFactoryImpl}
 */
@IntegrationTest
public class WileyCartFactoryImplIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{

	@Resource
	private ModelService modelService;

	@Resource
	private WileycomI18NService wileycomI18NService;

	@Resource
	private WileyCartFactoryImpl wileyCartFactory;

	@Resource
	private BaseSiteService baseSiteService;


	@Before
	public void setUp() throws Exception
	{
		baseSiteService.setCurrentBaseSite("wileyb2c", true);
	}

	@Test
	public void shouldSetSessionCountryDuringCartCreation()
	{

		//Given
		final CountryModel sessionCountry = wileycomI18NService.setDefaultCurrentCountry();

		//When
		final CartModel cart = wileyCartFactory.createCartInternal();

		//Than
		assertTrue("Cart should have session country", cart.getCountry().equals(sessionCountry));
	}

	@Test
	public void shouldSetCertainOrderType()
	{
		//Given
		final CountryModel sessionCountry = wileycomI18NService.setDefaultCurrentCountry();

		//When
		final CartModel cart = wileyCartFactory.createCartWithType(OrderType.REGISTRATION_CODE_ACIVATION);

		//Than
		assertEquals("Cart should have 'REGISTRATION_CODE_ACIVATION' type", OrderType.REGISTRATION_CODE_ACIVATION,
				cart.getOrderType());
	}

}
