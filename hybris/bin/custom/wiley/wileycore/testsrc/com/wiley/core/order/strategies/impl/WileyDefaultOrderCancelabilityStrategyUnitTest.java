package com.wiley.core.order.strategies.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.wiley.core.order.strategies.impl.OrderEntriesUtil.createOrderEntriesWithStatus;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyDefaultOrderCancelabilityStrategyUnitTest
{

	private static final String HYBRIS_SYSTEM = "hybris";
	@InjectMocks
	private WileyDefaultOrderCancelabilityStrategyImpl orderCancelabilityStrategy;
	@Mock
	private OrderModel orderMock;
	@Mock
	private Set<OrderStatus> orderCancelableStatuses;
	@Mock
	private Set<OrderStatus> orderEntryCancelableStatuses;

	@Before
	public void setUp()
	{
		when(orderCancelableStatuses.contains(OrderStatus.CREATED)).thenReturn(true);
		when(orderCancelableStatuses.contains(OrderStatus.SUSPENDED)).thenReturn(true);
		when(orderCancelableStatuses.contains(OrderStatus.FUNDING_DENIED)).thenReturn(true);
		when(orderCancelableStatuses.contains(OrderStatus.INVOICE_PENDING)).thenReturn(true);
		when(orderCancelableStatuses.contains(OrderStatus.INVOICE_OVERDUE)).thenReturn(true);
		when(orderCancelableStatuses.contains(OrderStatus.PAYMENT_WAIVED)).thenReturn(true);
		when(orderCancelableStatuses.contains(OrderStatus.PAYMENT_CAPTURED)).thenReturn(true);
		when(orderCancelableStatuses.contains(OrderStatus.CANCELLED)).thenReturn(true);
		when(orderEntryCancelableStatuses.contains(OrderStatus.CREATED)).thenReturn(true);
		when(orderMock.getSourceSystem()).thenReturn(HYBRIS_SYSTEM);
	}

	@Test
	public void shouldReturnCancelableForCorrectStatuses()
	{
		Set<OrderStatus> statuses = new HashSet<>(Arrays.asList(OrderStatus.CREATED,
				OrderStatus.SUSPENDED, OrderStatus.FUNDING_DENIED, OrderStatus.INVOICE_PENDING, OrderStatus.INVOICE_OVERDUE,
				OrderStatus.PAYMENT_WAIVED, OrderStatus.PAYMENT_CAPTURED, OrderStatus.CANCELLED));
		createOrderEntriesWithStatus(orderMock, statuses);
		assertTrue(orderCancelabilityStrategy.isCancelable(orderMock));
	}

	@Test
	public void shouldReturnNotCancelableForIncorrectStatuses()
	{
		Set<OrderStatus> statuses = new HashSet<>(Arrays.asList(OrderStatus.FAILED, OrderStatus.PAYMENT_FAILED));
		createOrderEntriesWithStatus(orderMock, statuses);
		assertFalse(orderCancelabilityStrategy.isCancelable(orderMock));
	}

	@Test
	public void shouldReturnCancelableForCorrectEntryStatuses()
	{
		assertTrue(orderCancelabilityStrategy.isCancelableOrderEntry(orderMock, OrderStatus.CREATED));
	}

	@Test
	public void shouldReturnNotCancelableForIncorrectEntryStatuses()
	{
		assertFalse(orderCancelabilityStrategy.isCancelableOrderEntry(orderMock, OrderStatus.FAILED));
	}
}
