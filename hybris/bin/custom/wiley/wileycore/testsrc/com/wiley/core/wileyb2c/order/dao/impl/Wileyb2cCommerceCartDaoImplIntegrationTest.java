package com.wiley.core.wileyb2c.order.dao.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;


/**
 * Default integation test for {@link Wileyb2cCommerceCartDaoImpl}.
 */
@IntegrationTest
public class Wileyb2cCommerceCartDaoImplIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{

	private static final String TEST_STORE_UID = "wileyb2c";
	private static final String TEST_USER_UID = "test@hybris.com";
	private static final String TEST_CART_GUID = "123456";
	private static final String UNKNOWN_CART_GUID = "5765765876876";

	@Resource
	private BaseStoreService baseStoreService;

	@Resource
	private UserService userService;

	@Resource
	private Wileyb2cCommerceCartDaoImpl wileyb2cCommerceCartDao;

	@Before
	public void setUp() throws Exception
	{
		importCsv("/wileycore/test/order/Wileyb2cCommerceCartDaoImplIntegrationTest/cart.impex", DEFAULT_ENCODING);
	}

	@Test
	public void testGetCartForGuidAndStoreAndUser()
	{
		// Given
		// Setup the system under test
		final BaseStoreModel baseStore = baseStoreService.getBaseStoreForUid(TEST_STORE_UID);
		final UserModel user = userService.getUserForUID(TEST_USER_UID);

		// When
		final CartModel cart = wileyb2cCommerceCartDao.getCartForGuidAndStoreAndUser(TEST_CART_GUID, baseStore, user);

		// Then
		assertNotNull(cart);
		assertEquals(TEST_CART_GUID, cart.getGuid());
		assertEquals(baseStore, cart.getStore());
		assertEquals(user, cart.getUser());
	}

	@Test
	public void testGetCartForGuidAndStoreAndUserWhenCartIsNotFound()
	{
		// Given
		// Setup the system under test
		final BaseStoreModel baseStore = baseStoreService.getBaseStoreForUid(TEST_STORE_UID);
		final UserModel user = userService.getUserForUID(TEST_USER_UID);

		// When
		final CartModel cart = wileyb2cCommerceCartDao.getCartForGuidAndStoreAndUser(UNKNOWN_CART_GUID, baseStore, user);

		// Then
		assertNull(cart);
	}

	@Test
	public void testGetCartForGuidAndStoreAndUserWhenGuidIsNull()
	{
		// Given
		// Setup the system under test
		final String guid = null;
		final BaseStoreModel baseStore = baseStoreService.getBaseStoreForUid(TEST_STORE_UID);
		final UserModel user = userService.getUserForUID(TEST_USER_UID);

		// When
		final CartModel cart = wileyb2cCommerceCartDao.getCartForGuidAndStoreAndUser(guid, baseStore, user);

		// Then
		assertNotNull(cart);
		assertEquals(baseStore, cart.getStore());
		assertEquals(user, cart.getUser());
	}

	@Test
	public void testGetCartForGuidAndStoreAndUserWhenGuidIsNullAndCartIsNotExists()
	{
		// Given
		// Setup the system under test
		final String guid = null;
		final BaseStoreModel baseStore = baseStoreService.getBaseStoreForUid(TEST_STORE_UID);
		final UserModel user = userService.getAdminUser();

		// When
		final CartModel cart = wileyb2cCommerceCartDao.getCartForGuidAndStoreAndUser(guid, baseStore, user);

		// Then
		assertNull(cart);
	}

	@Test
	public void testGetCartForGuidAndStore()
	{
		// Given
		final String guid = TEST_CART_GUID;
		final BaseStoreModel baseStore = baseStoreService.getBaseStoreForUid(TEST_STORE_UID);

		// When
		final CartModel cart = wileyb2cCommerceCartDao.getCartForGuidAndStore(guid, baseStore);

		// Then
		assertNotNull(cart);
		assertEquals(guid, cart.getGuid());
		assertEquals(baseStore, cart.getStore());
	}

	@Test
	public void testGetCartForGuidAndStoreWhenCartIsNotFound()
	{
		// Given
		final BaseStoreModel baseStore = baseStoreService.getBaseStoreForUid(TEST_STORE_UID);

		// When
		final CartModel cart = wileyb2cCommerceCartDao.getCartForGuidAndStore(UNKNOWN_CART_GUID, baseStore);

		// Then
		assertNull(cart);
	}

	@Test
	public void testGetCartsForStoreAndUser()
	{
		// Given
		final BaseStoreModel baseStore = baseStoreService.getBaseStoreForUid(TEST_STORE_UID);
		final UserModel user = userService.getUserForUID(TEST_USER_UID);

		// When
		final List<CartModel> carts = wileyb2cCommerceCartDao.getCartsForStoreAndUser(baseStore, user);

		// Then
		assertNotNull(carts);
		for (CartModel cart : carts)
		{
			assertEquals(baseStore, cart.getStore());
			assertEquals(user, cart.getUser());
		}
	}

	@Test
	public void testGetCartsForStoreAndUserWhenCartIsNotFound()
	{
		// Given
		final BaseStoreModel baseStore = baseStoreService.getBaseStoreForUid(TEST_STORE_UID);
		final UserModel user = userService.getAdminUser();

		// When
		final List<CartModel> carts = wileyb2cCommerceCartDao.getCartsForStoreAndUser(baseStore, user);

		// Then
		assertNotNull(carts);
		assertTrue(carts.isEmpty());
	}
}