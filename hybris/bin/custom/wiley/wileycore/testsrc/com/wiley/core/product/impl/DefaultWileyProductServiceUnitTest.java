package com.wiley.core.product.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.subscriptionservices.model.SubscriptionProductModel;
import de.hybris.platform.variants.model.VariantProductModel;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyCollection;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.category.WileyCategoryService;
import com.wiley.core.model.WileyProductModel;
import com.wiley.core.model.WileyVariantProductModel;
import com.wiley.core.product.dao.WileyProductDao;


/**
 * Unit test for {@link DefaultWileyProductService}
 *
 * @author Aliaksei_Zlobich
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultWileyProductServiceUnitTest
{
	private static final String BASE_PRODUCT_CATEGORY_CODE = "baseProductCategoryCode";
	private static final String VARIANT_VALUE_CATEGORY_CODE = "variantValueCategoryCode";

	private static final String CFA_PLATINUM_LEVEL_1_CODE = "cfaPlatinumLevel1";
	private static final String CFA_GOLD_LEVEL_1_CODE = "cfaGoldLevel1";
	private static final String CFA_SILVER_LEVEL_1_CODE = "cfaSilverLevel1";
	private static final String CFA_PLATINUM_LEVEL_1_EBOOK_CODE = "cfaPlatinumLevel1EBook";
	private static final String CFA_PLATINUM_LEVEL_1_PRINT_CODE = "cfaPlatinumLevel1Print";
	private static final String CFA_GOLD_LEVEL_1_EBOOK_CODE = "cfaGoldLevel1EBook";
	private static final String CFA_GOLD_LEVEL_1_PRINT_CODE = "cfaGoldLevel1Print";
	private static final String CFA_SILVER_LEVEL_1_EBOOK_CODE = "cfaSilverLevel1EBook";
	private static final String CFA_SILVER_LEVEL_1_PRINT_CODE = "cfaSilverLevel1Print";


	@Mock
	private WileyCategoryService categoryService;

	@Mock
	private WileyProductModel product;

	@Mock
	private WileyVariantProductModel variantProduct;

	@Mock
	private VariantProductModel cfaPlatinumLevel1EBook, cfaPlatinumLevel1Print, cfaGoldLevel1EBook,
			cfaGoldLevel1Print, cfaSilverLevel1EBook, cfaSilverLevel1Print;
	@Mock
	private ProductModel cfaPlatinumLevel1, cfaGoldLevel1, cfaSilverLevel1;

	@Mock
	private CategoryModel category;

	@Mock
	private WileyProductDao wileyProductDao;

	@Mock
	private CatalogVersionService catalogVersionService;

	@InjectMocks
	private DefaultWileyProductService defaultWileyProductService;

	/**
	 * Sets up.
	 *
	 * @throws Exception
	 * 		the exception
	 */
	@Before
	public void setUp() throws Exception
	{
		when(product.getSupercategories()).thenReturn(Collections.emptyList());
		initVariants();
	}

	private void initVariants()
	{
		givenVariantWithBaseProduct(cfaPlatinumLevel1EBook, cfaPlatinumLevel1);
		givenVariantWithBaseProduct(cfaPlatinumLevel1Print, cfaPlatinumLevel1);
		givenVariantWithBaseProduct(cfaGoldLevel1EBook, cfaGoldLevel1);
		givenVariantWithBaseProduct(cfaGoldLevel1Print, cfaGoldLevel1);
		givenVariantWithBaseProduct(cfaSilverLevel1EBook, cfaSilverLevel1);
		givenVariantWithBaseProduct(cfaSilverLevel1Print, cfaSilverLevel1);

		givenProductWithCode(cfaPlatinumLevel1, CFA_PLATINUM_LEVEL_1_CODE);
		givenProductWithCode(cfaPlatinumLevel1EBook, CFA_PLATINUM_LEVEL_1_EBOOK_CODE);
		givenProductWithCode(cfaPlatinumLevel1Print, CFA_PLATINUM_LEVEL_1_PRINT_CODE);

		givenProductWithCode(cfaGoldLevel1, CFA_GOLD_LEVEL_1_CODE);
		givenProductWithCode(cfaGoldLevel1EBook, CFA_GOLD_LEVEL_1_EBOOK_CODE);
		givenProductWithCode(cfaGoldLevel1Print, CFA_GOLD_LEVEL_1_PRINT_CODE);

		givenProductWithCode(cfaSilverLevel1, CFA_SILVER_LEVEL_1_CODE);
		givenProductWithCode(cfaSilverLevel1EBook, CFA_SILVER_LEVEL_1_EBOOK_CODE);
		givenProductWithCode(cfaSilverLevel1Print, CFA_SILVER_LEVEL_1_PRINT_CODE);
	}

	/**
	 * Test get system requirements success on category.
	 *
	 * @throws Exception
	 * 		the exception
	 */
	@Test
	public void testGetSystemRequirementsSuccessOnCategory() throws Exception
	{
		// Given
		final String sysReq = "SystemRequirements.Success";
		when(categoryService.getSystemRequirementsFromCategoryTree(anyCollection(), any())).thenReturn(
				sysReq);

		// When
		final String systemRequirements = defaultWileyProductService.getSystemRequirements(product);

		// Then
		assertEquals(sysReq, systemRequirements);
	}

	/**
	 * Test get system requirements success on product.
	 *
	 * @throws Exception
	 * 		the exception
	 */
	@Test
	public void testGetSystemRequirementsSuccessOnProduct() throws Exception
	{
		// Given
		final String sysReq = "SystemRequirements.Success";
		when(categoryService.getSystemRequirementsFromCategoryTree(anyCollection(), any())).thenReturn(
				sysReq);
		final String sysReqProduct = "SystemRequirements.Product";
		when(product.getSystemRequirements()).thenReturn(sysReqProduct);

		// When
		final String systemRequirements = defaultWileyProductService.getSystemRequirements(product);

		// Then
		assertEquals(sysReqProduct, systemRequirements);
	}


	/**
	 * Test get system requirements success on variant product.
	 */
	@Test
	public void testGetSystemRequirementsSuccessOnProductVariant() throws Exception
	{
		// Given
		final String sysReq = "SystemRequirements.Success";
		when(categoryService.getSystemRequirementsFromCategoryTree(anyCollection(), any())).thenReturn(
				sysReq);
		final String sysReqProduct = "SystemRequirements.VariantProduct";
		when(variantProduct.getSystemRequirements()).thenReturn(sysReqProduct);

		// When
		final String systemRequirements = defaultWileyProductService.getSystemRequirements(variantProduct);

		// Then
		assertEquals(sysReqProduct, systemRequirements);
	}

	/**
	 * getSystemRequirements test
	 * Test get system requirements from variant's base product super categories if variant's systemRequirements value is null.
	 */
	@Test
	public void shouldReturnSystemRequirementsForVariantBaseProductCategoryIfVariantValueIsNull() throws Exception
	{
		// Given
		final String sysReq = "SystemRequirements.Success";
		final ProductModel baseProduct = givenBaseProductForVariant(variantProduct);
		givenCategoryForProduct(baseProduct);

		when(categoryService.getSystemRequirementsFromCategoryTree(eq(baseProduct.getSupercategories()), any())).thenReturn(
				sysReq);

		// When
		final String systemRequirements = defaultWileyProductService.getSystemRequirements(variantProduct);

		// Then
		assertEquals(sysReq, systemRequirements);
	}

	@Test
	public void testGetSystemRequirementsShouldReturnNullForNonWileyProducts()
	{
		// When
		final String systemRequirements = defaultWileyProductService.getSystemRequirements(new ProductModel());
		//Then
		assertNull(systemRequirements);
	}

	@Test
	public void testCanBePartOfProductSetSuccess()
	{
		// Given
		WileyVariantProductModel wileyVariantProduct = new WileyVariantProductModel();

		// When
		final boolean result = defaultWileyProductService.canBePartOfProductSet(wileyVariantProduct);

		// Then
		assertTrue("Expected that product can be part of a product set.", result);
	}

	@Test
	public void testCanBePartOfProductSetSuccessIfProductIsNotPartOfSet()
	{
		// Given
		WileyProductModel wileyProductModel = new WileyProductModel();

		// When
		final boolean result = defaultWileyProductService.canBePartOfProductSet(wileyProductModel);

		// Then
		assertFalse("Expected that WileyProductModel cannot be part of a product set.", result);
	}

	@Test
	public void testCanBePartOfProductSetIfProductNull()
	{
		// Given
		// no changes in test data

		// When
		final boolean result = defaultWileyProductService.canBePartOfProductSet(null);

		// Then
		assertFalse("Expected that null cannot be part of a product set.", result);
	}

	@Test
	public void testGetProductForIsbnAndTermWhenTermIsNull()
	{

		String isbn = "test-isbn";
		String code = "test-code";
		String catalogId = "test-catalogId";
		String catalogVersion = "test-catalogVersion";

		CatalogVersionModel catalogVersionModel = mock(CatalogVersionModel.class);

		// Given
		when(product.getCode()).thenReturn(code);
		when(catalogVersionService.getCatalogVersion(catalogId, catalogVersion)).thenReturn(catalogVersionModel);
		when(wileyProductDao.findProductsByIsbn(isbn, catalogVersionModel)).thenReturn(Arrays.asList(product));

		// When
		ProductModel productModel = defaultWileyProductService.getProductForIsbnAndTerm(isbn, null, catalogId, catalogVersion);

		// Then
		assertEquals(code, productModel.getCode());
	}

	@Test
	public void testGetProductForIsbnAndTermWhenTermIsNotNull()
	{

		String isbn = "test-isbn";
		String term = "test-term";
		String code = "test-code";
		String catalogId = "test-catalogId";
		String catalogVersion = "test-catalogVersion";

		CatalogVersionModel catalogVersionModel = mock(CatalogVersionModel.class);

		SubscriptionProductModel subscriptionProductModel = mock(SubscriptionProductModel.class);

		// Given
		when(subscriptionProductModel.getCode()).thenReturn(code);
		when(catalogVersionService.getCatalogVersion(catalogId, catalogVersion)).thenReturn(catalogVersionModel);
		when(wileyProductDao.findProductsByIsbnAndTerm(isbn, term, catalogVersionModel))
				.thenReturn(Arrays.asList(subscriptionProductModel));

		// When
		ProductModel productModel = defaultWileyProductService.getProductForIsbnAndTerm(isbn, term, catalogId, catalogVersion);

		// Then
		assertEquals(code, productModel.getCode());
	}

	@Test
	public void testGetWileyProductsFromCategories()
	{
		List<ProductModel> expectedProductsList = Arrays.asList(cfaPlatinumLevel1EBook, cfaPlatinumLevel1EBook,
				cfaGoldLevel1EBook);
		when(wileyProductDao.findWileyProductsFromCategories(BASE_PRODUCT_CATEGORY_CODE, VARIANT_VALUE_CATEGORY_CODE)).thenReturn(
				new ArrayList<>(expectedProductsList));

		List<ProductModel> actualProductsList = defaultWileyProductService.getWileyProductsFromCategories(
				BASE_PRODUCT_CATEGORY_CODE, VARIANT_VALUE_CATEGORY_CODE);
		actualProductsList.equals(expectedProductsList);
	}

	@Test
	public void testFilterProductsWhenAllItemsInListAreVariants()
	{
		List<ProductModel> variantsList = Arrays.asList(cfaPlatinumLevel1EBook, cfaPlatinumLevel1Print, cfaGoldLevel1EBook,
				cfaGoldLevel1Print);
		List<ProductModel> expectedVariantsList = Arrays.asList(cfaPlatinumLevel1EBook, cfaGoldLevel1EBook);
		List<ProductModel> actualVariantsList = defaultWileyProductService.filterProducts(variantsList);
		assertEquals(expectedVariantsList, actualVariantsList);
	}

	@Test
	public void testFilterProductsWhenItemsHaveDifferentTypes()
	{
		List<ProductModel> variantsList = Arrays.asList(cfaPlatinumLevel1EBook, cfaPlatinumLevel1Print, cfaGoldLevel1EBook,
				cfaGoldLevel1Print, cfaSilverLevel1);
		List<ProductModel> expectedVariantsList = Arrays.asList(cfaPlatinumLevel1EBook, cfaGoldLevel1EBook, cfaSilverLevel1);
		List<ProductModel> actualVariantsList = defaultWileyProductService.filterProducts(variantsList);
		assertEquals(expectedVariantsList, actualVariantsList);
	}

	@Test
	public void testFilterProductsWhenVariantsGroupsSizeAreDifferent()
	{
		List<ProductModel> variantsList = Arrays.asList(cfaPlatinumLevel1Print, cfaGoldLevel1EBook,
				cfaGoldLevel1Print, cfaSilverLevel1EBook);
		List<ProductModel> expectedVariantsList = Arrays.asList(cfaPlatinumLevel1Print, cfaGoldLevel1EBook, cfaSilverLevel1EBook);
		List<ProductModel> actualVariantsList = defaultWileyProductService.filterProducts(variantsList);
		assertEquals(expectedVariantsList, actualVariantsList);
	}


	@Test
	public void testProductsToValidate()
	{
		// Given
		List<ProductModel> expectedProductsList = Arrays.asList(cfaPlatinumLevel1EBook, cfaPlatinumLevel1EBook,
				cfaGoldLevel1EBook);
		final ArticleApprovalStatus approvalStatus = ArticleApprovalStatus.CHECK;
		final CatalogVersionModel catalogVersion = new CatalogVersionModel();
		when(wileyProductDao.findProductsToValidate(approvalStatus, catalogVersion)).thenReturn(expectedProductsList);

		// When
		List<ProductModel> productsToValidate = defaultWileyProductService.getProductsToValidate(approvalStatus, catalogVersion);

		// Then
		assertEquals(expectedProductsList, productsToValidate);
	}

	@Test
	public void testIsBaseProductFromCategory()
	{
		when(category.getCode()).thenReturn(BASE_PRODUCT_CATEGORY_CODE);
		when(product.getSupercategories()).thenReturn(Collections.singleton(category));
		when(variantProduct.getBaseProduct()).thenReturn(product);

		boolean result = defaultWileyProductService.isBaseProductFromCategory(variantProduct, BASE_PRODUCT_CATEGORY_CODE);

		assertTrue(result);
	}

	@Test
	public void testIsBaseProductNotFromCategory()
	{
		when(product.getSupercategories()).thenReturn(Collections.emptyList());
		when(variantProduct.getBaseProduct()).thenReturn(product);

		boolean result = defaultWileyProductService.isBaseProductFromCategory(variantProduct, BASE_PRODUCT_CATEGORY_CODE);

		assertFalse(result);
	}

	@Test
	public void testIsProductFromCategory()
	{
		when(variantProduct.getBaseProduct()).thenReturn(null);

		boolean result = defaultWileyProductService.isBaseProductFromCategory(variantProduct, BASE_PRODUCT_CATEGORY_CODE);

		assertFalse(result);
	}

	private ProductModel givenBaseProductForVariant(final WileyVariantProductModel variant)
	{
		final WileyProductModel wileyProductModel = mock(WileyProductModel.class);
		when(variant.getBaseProduct()).thenReturn(wileyProductModel);
		return wileyProductModel;
	}

	private void givenVariantWithBaseProduct(final VariantProductModel variantProductModel, final ProductModel baseProductModel)
	{
		when(variantProductModel.getBaseProduct()).thenReturn(baseProductModel);
	}

	private void givenProductWithCode(final ProductModel productModel, final String productCode)
	{
		when(productModel.getCode()).thenReturn(productCode);
	}

	private CategoryModel givenCategoryForProduct(final ProductModel product)
	{
		final CategoryModel categoryModel = mock(CategoryModel.class);
		when(product.getSupercategories()).thenReturn(Collections.singletonList(categoryModel));
		return categoryModel;
	}

}