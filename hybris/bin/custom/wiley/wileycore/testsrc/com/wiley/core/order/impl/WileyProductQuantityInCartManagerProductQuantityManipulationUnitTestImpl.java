package com.wiley.core.order.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.product.WileyCountableProductService;

import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


/**
 * Unit test for {@link WileyProductQuantityInCartManagerImpl}.<br/>
 *
 * This class contains tests to check logic which responsible for quantity manipulation, different quantity checks.
 *
 * @author Aliaksei_Zlobich
 */
@UnitTest
public class WileyProductQuantityInCartManagerProductQuantityManipulationUnitTestImpl
{
	@InjectMocks
	private WileyProductQuantityInCartManagerImpl wileyProductQuantityInCartManagerImpl;

	@Mock
	private ModelService modelServiceMock;


	// Test data
	@Mock
	private CartModel cartMock;
	@Mock
	private CartEntryModel cartEntryMock1;
	@Mock
	private ProductModel productMock1;
	@Mock
	private CartEntryModel cartEntryMock2;
	@Mock
	private ProductModel productMock2;
	@Mock
	private CommerceCartModification commerceCartModification;
	
	@Mock
  private CommerceCartModification commerceCartModification2;
	@Mock
	private WileyCountableProductService wileyCountableProductServiceMock;

	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);

		// set up test data
		when(cartMock.getEntries()).thenReturn(Arrays.asList(cartEntryMock1, cartEntryMock2));

		when(cartEntryMock1.getProduct()).thenReturn(productMock1);
		when(cartEntryMock2.getProduct()).thenReturn(productMock2);

		when(cartEntryMock1.getQuantity()).thenReturn((long) 123);
		when(cartEntryMock2.getQuantity()).thenReturn((long) 123);

		// set up services
		when(wileyCountableProductServiceMock.canProductHaveQuantity(productMock1)).thenReturn(false);
		when(wileyCountableProductServiceMock.canProductHaveQuantity(productMock2)).thenReturn(true);

	}

	@Test
	public void testCheckAndUpdateQuantityIfRequired() throws Exception
	{
		// Given
		// test data without changes

		// When
		List<CommerceCartModification> commerceCartModifications = new ArrayList<CommerceCartModification>();
		commerceCartModifications.add(commerceCartModification);
		commerceCartModifications.add(commerceCartModification2);
		when(commerceCartModification.getEntry()).thenReturn(cartEntryMock1);
		when(commerceCartModification2.getEntry()).thenReturn(cartEntryMock2);
		wileyProductQuantityInCartManagerImpl.checkAndUpdateQuantityIfRequired(cartMock, commerceCartModifications);
		
		// Then
		verify(wileyCountableProductServiceMock).canProductHaveQuantity(eq(productMock1));
		verify(wileyCountableProductServiceMock).canProductHaveQuantity(eq(productMock2));
		verify(cartEntryMock1).setQuantity(eq((long) WileyCoreConstants.DEFAULT_QUANTITY_FOR_PRODUCT_WHICH_CANNOT_HAVE_QUANTITY));
		verify(modelServiceMock).saveAll(Arrays.asList(cartEntryMock1));

		verify(cartEntryMock2, never()).setQuantity(anyLong());
		assertTrue(commerceCartModifications.size() == 1);
		assertSame(cartEntryMock2, commerceCartModifications.get(0).getEntry());
	}
}
