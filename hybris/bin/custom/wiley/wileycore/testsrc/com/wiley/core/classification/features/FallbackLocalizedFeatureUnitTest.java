package com.wiley.core.classification.features;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.classification.features.FeatureValue;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.internal.model.impl.LocaleProvider;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.Assertions.assertThat;


/**
 * Test for {@link FallbackLocalizedFeature}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class FallbackLocalizedFeatureUnitTest
{
	private static final Locale EN_US_LOCALE = new Locale("en", "US");
	private static final Locale EN_DE_LOCALE = new Locale("en", "DE");
	private static final Locale EN_GB_LOCALE = new Locale("en", "GB");
	private static final FeatureValue US_FEATURE_VALUE = new FeatureValue("test US feature value");
	private static final FeatureValue DE_FEATURE_VALUE = new FeatureValue("test DE feature value");
	private static final FeatureValue GB_FEATURE_VALUE = new FeatureValue("test GB feature value");
	private static final Locale CURRENT_LOCALE = new Locale("en");

	private FallbackLocalizedFeature feature;

	@Mock
	private I18NService i18nService;

	@Mock
	private LocaleProvider localeProvider;

	@Before
	public void setUp()
	{
		final ClassAttributeAssignmentModel classAttributeAssignmentModel = new ClassAttributeAssignmentModel();

		feature = spy(
				new FallbackLocalizedFeature(classAttributeAssignmentModel, Collections.emptyMap(), CURRENT_LOCALE, i18nService));
		when(feature.getLocaleProvider()).thenReturn(localeProvider);
		when(feature.isFallbackEnabled()).thenReturn(true);

	}

	@Test
	public void shouldReturnNullValueDueToNoFeatureValues()
	{
		//Given
		//no additional condition

		//When
		final FeatureValue value = feature.getValue(EN_US_LOCALE);

		//Then
		assertThat(value).isNull();
	}

	@Test
	public void shouldReturnNullValueDueToFallbackDisabled()
	{
		//Given
		when(feature.isFallbackEnabled()).thenReturn(false);
		when(localeProvider.getFallbacks(EN_US_LOCALE)).thenReturn(Collections.singletonList(EN_DE_LOCALE));
		feature.setValues(Collections.singletonList(DE_FEATURE_VALUE), EN_DE_LOCALE);

		//When
		final FeatureValue value = feature.getValue(EN_US_LOCALE);

		//Then
		assertThat(value).isNull();
		verify(localeProvider, times(0)).getFallbacks(any());
	}


	@Test
	public void shouldReturnUsValue()
	{
		//Given
		feature.setValues(Collections.singletonList(US_FEATURE_VALUE), EN_US_LOCALE);

		//When
		final FeatureValue value = feature.getValue(EN_US_LOCALE);

		//Then
		assertThat(value).isEqualTo(US_FEATURE_VALUE);
	}

	@Test
	public void shouldReturnNullValueDueToNullFallbacks()
	{
		//Given
		when(localeProvider.getFallbacks(EN_US_LOCALE)).thenReturn(null);

		//When
		final FeatureValue value = feature.getValue(EN_US_LOCALE);

		//Then
		assertThat(value).isNull();
	}

	@Test
	public void shouldReturnNullValueDueToEmptyFallbacks()
	{
		//Given
		when(localeProvider.getFallbacks(EN_US_LOCALE)).thenReturn(Collections.emptyList());

		//When
		final FeatureValue value = feature.getValue(EN_US_LOCALE);

		//Then
		assertThat(value).isNull();
	}

	@Test
	public void shouldReturnNullValueDueToEmptyENAndDEFeatureValues()
	{
		//Given
		when(localeProvider.getFallbacks(EN_US_LOCALE)).thenReturn(Collections.singletonList(EN_DE_LOCALE));
		feature.setValues(Collections.emptyList(), EN_DE_LOCALE);

		//When
		final FeatureValue value = feature.getValue(EN_US_LOCALE);

		//Then
		assertThat(value).isNull();
	}

	@Test
	public void shouldReturnDEFeatureValues()
	{
		//Given
		when(localeProvider.getFallbacks(EN_US_LOCALE)).thenReturn(Collections.singletonList(EN_DE_LOCALE));
		feature.setValues(Collections.singletonList(DE_FEATURE_VALUE), EN_DE_LOCALE);

		//When
		final FeatureValue value = feature.getValue(EN_US_LOCALE);

		//Then
		assertThat(value).isEqualTo(DE_FEATURE_VALUE);
	}


	@Test
	public void shouldReturnGBFeatureValue()
	{
		//Given
		when(localeProvider.getFallbacks(EN_US_LOCALE)).thenReturn(Arrays.asList(EN_DE_LOCALE, EN_GB_LOCALE));
		feature.setValues(Collections.singletonList(GB_FEATURE_VALUE), EN_GB_LOCALE);

		//When
		final FeatureValue value = feature.getValue(EN_US_LOCALE);

		//Then
		assertThat(value).isEqualTo(GB_FEATURE_VALUE);
	}

	@Test
	public void shouldReturnNullValuesDueToNoValue()
	{
		//Given
		//no additional condition

		//When
		final List<FeatureValue> values = feature.getValues(EN_US_LOCALE);

		//Then
		assertThat(values).isEmpty();
	}


	@Test
	public void shouldReturnUsListValues()
	{
		//Given
		feature.setValues(Collections.singletonList(US_FEATURE_VALUE), EN_US_LOCALE);

		//When
		final List<FeatureValue> values = feature.getValues(EN_US_LOCALE);

		//Then
		assertThat(values).isEqualTo(Collections.singletonList(US_FEATURE_VALUE));
	}

	@Test
	public void shouldReturnGBFeatureValues()
	{
		//Given
		when(localeProvider.getFallbacks(EN_US_LOCALE)).thenReturn(Arrays.asList(EN_DE_LOCALE, EN_GB_LOCALE));
		feature.setValues(Collections.singletonList(GB_FEATURE_VALUE), EN_GB_LOCALE);

		//When
		final List<FeatureValue> values = feature.getValues(EN_US_LOCALE);

		//Then
		assertThat(values).isEqualTo(Collections.singletonList(GB_FEATURE_VALUE));
	}

}
