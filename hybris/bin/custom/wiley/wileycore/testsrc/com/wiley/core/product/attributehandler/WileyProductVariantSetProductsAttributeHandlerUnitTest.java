package com.wiley.core.product.attributehandler;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.hamcrest.core.IsSame;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.wiley.core.model.WileyProductVariantSetModel;
import com.wiley.core.product.WileyProductVariantSetService;

import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;


/**
 * Unit test for {@link WileyProductVariantSetProductsAttributeHandler}
 *
 * @author Aliaksei_Zlobich
 */
@UnitTest
public class WileyProductVariantSetProductsAttributeHandlerUnitTest
{

	private WileyProductVariantSetProductsAttributeHandler attributeHandler;

	private List<GenericVariantProductModel> productCollection;

	@Mock
	private WileyProductVariantSetService wileyProductVariantSetServiceMock;

	@Mock
	private WileyProductVariantSetModel productVariantSetMock;

	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);

		productCollection = new ArrayList<>();
		productCollection.add(new GenericVariantProductModel());

		// Setup service WileyProductVariantSetService
		when(wileyProductVariantSetServiceMock.getSetEntries(eq(productVariantSetMock))).thenReturn(productCollection);

		// Setup attribute handler
		attributeHandler = new WileyProductVariantSetProductsAttributeHandler();
		attributeHandler.setWileyProductVariantSetService(wileyProductVariantSetServiceMock);
	}

	@Test
	public void testGet() throws Exception
	{
		// Given
		// no changes in test data

		// Then
		final Collection<GenericVariantProductModel> genericVariantProductModels = attributeHandler.get(productVariantSetMock);

		// When
		// attribute handler should delegate to service
		assertThat(genericVariantProductModels, IsSame.sameInstance(productCollection));
	}
}