package com.wiley.core.subscription.services.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.time.TimeService;
import de.hybris.platform.subscriptionservices.enums.SubscriptionStatus;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileySubscriptionModel;
import com.wiley.core.subscription.dao.WileySubscriptionDao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


/**
 * Unit test for {@link WileySubscriptionServiceImpl}.<br/>
 * Contains test to check renewal logic.
 *
 * @author Aliaksei_Zlobich
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileySubscriptionServiceImplRenewalUnitTest
{

	private WileySubscriptionServiceImpl wileySubscriptionService;

	private LocalDateTime currentDateTime;

	@Mock
	private TimeService timeServiceMock;

	@Mock
	private WileySubscriptionDao wileySubscriptionDaoMock;

	// Test data
	@Mock
	private WileySubscriptionModel subscriptionModelMock;

	@Mock
	private WileySubscriptionModel getSubscriptionModelMock2;

	@Before
	public void setUp() throws Exception
	{
		currentDateTime = LocalDateTime.parse("2017-04-03T04:14:00");

		// Setup test data
		final ZoneId zone = ZoneId.systemDefault();
		when(timeServiceMock.getCurrentTime()).thenReturn(Date.from(currentDateTime.atZone(zone).toInstant()));

		when(subscriptionModelMock.getRenewalEnabled()).thenReturn(true);
		when(subscriptionModelMock.getRequireRenewal()).thenReturn(true);
		when(subscriptionModelMock.getHybrisRenewal()).thenReturn(true);

		when(getSubscriptionModelMock2.getRenewalEnabled()).thenReturn(false);
		when(getSubscriptionModelMock2.getRequireRenewal()).thenReturn(false);
		when(getSubscriptionModelMock2.getHybrisRenewal()).thenReturn(true);

		final LocalDate day = currentDateTime.toLocalDate();
		final ZonedDateTime startOfCurrentDay = day.atStartOfDay().atZone(zone);
		final ZonedDateTime startOfNextDay = day.plusDays(1).atStartOfDay().atZone(zone);
		when(wileySubscriptionDaoMock.findSubscriptionsWithExpirationDateBetween(eq(SubscriptionStatus.ACTIVE), eq(
				startOfCurrentDay), eq(startOfNextDay))).thenReturn(Arrays.asList(subscriptionModelMock,
				getSubscriptionModelMock2));

		// Setup WileySubscriptionServiceImpl
		wileySubscriptionService = new WileySubscriptionServiceImpl();
		wileySubscriptionService.setTimeService(timeServiceMock);
		wileySubscriptionService.setSubscriptionDao(wileySubscriptionDaoMock);
	}

	@Test
	public void shouldReturnAllSubscriptionWhichShouldBeRenewed()
	{
		// Given
		// No changes in test data

		// When
		final List<WileySubscriptionModel> subscriptions =
				wileySubscriptionService.getAllSubscriptionsWhichShouldBeRenewed();

		// Then
		assertNotNull(subscriptions);
		assertEquals(1, subscriptions.size());
		assertEquals(subscriptionModelMock, subscriptions.get(0));
	}

	@Test
	public void testIsRequiredRenewalCase1RequiredAndEnabledRenewal()
	{
		// Given
		WileySubscriptionModel wileySubscriptionModelMock = mock(WileySubscriptionModel.class);
		when(wileySubscriptionModelMock.getRenewalEnabled()).thenReturn(true);
		when(wileySubscriptionModelMock.getRequireRenewal()).thenReturn(true);

		// When
		final boolean result = wileySubscriptionService.isRequiredRenewal(wileySubscriptionModelMock);

		// Then
		assertTrue(result);
	}

	@Test
	public void testIsRequiredRenewalCase2RequiredButNotEnabledRenewal()
	{
		// Given
		WileySubscriptionModel wileySubscriptionModelMock = mock(WileySubscriptionModel.class);
		when(wileySubscriptionModelMock.getRenewalEnabled()).thenReturn(false);
		when(wileySubscriptionModelMock.getRequireRenewal()).thenReturn(true);

		// When
		final boolean result = wileySubscriptionService.isRequiredRenewal(wileySubscriptionModelMock);

		// Then
		assertFalse(result);
	}

	@Test
	public void testIsRequiredRenewalCase3EnabledButNotRequired()
	{
		// Given
		WileySubscriptionModel wileySubscriptionModelMock = mock(WileySubscriptionModel.class);
		when(wileySubscriptionModelMock.getRenewalEnabled()).thenReturn(true);
		when(wileySubscriptionModelMock.getRequireRenewal()).thenReturn(false);

		// When
		final boolean result = wileySubscriptionService.isRequiredRenewal(wileySubscriptionModelMock);

		// Then
		assertFalse(result);
	}

	@Test
	public void testIsRequiredRenewalCase4NotEnabledAndNotRequired()
	{
		// Given
		WileySubscriptionModel wileySubscriptionModelMock = mock(WileySubscriptionModel.class);
		when(wileySubscriptionModelMock.getRenewalEnabled()).thenReturn(false);
		when(wileySubscriptionModelMock.getRequireRenewal()).thenReturn(false);

		// When
		final boolean result = wileySubscriptionService.isRequiredRenewal(wileySubscriptionModelMock);

		// Then
		assertFalse(result);
	}

}