package com.wiley.core.integration.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.time.TimeService;

import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.wiley.core.security.WileyMessageEncoder;

import static org.mockito.Mockito.when;


/**
 * Test for WileyIntegrationServiceImpl
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileyIntegrationServiceImplUnitTest
{
	@Mock
	private TimeService timeServiceMock;

	@Mock
	private WileyMessageEncoder wileyMessageEncoderMock;

	@InjectMocks
	private WileyIntegrationServiceImpl wileyIntegrationService;

	@Before
	public void setUp() throws Exception
	{
		ReflectionTestUtils.setField(wileyIntegrationService, "authUrl", "https://test.graphicstandards.com/auth");
	}

	@Test
	public void testGetMagicLink() throws Exception
	{
		// Given
		CustomerModel customerData = new CustomerModel();
		String uid = "customer@gmail.com";
		customerData.setUid(uid);
		customerData.setFirstName("Alice");

		Date timestamp = Date.from(
				ZonedDateTime.of(2011, 4, 17, 15, 10, 0, 0, ZoneId.ofOffset("", ZoneOffset.ofHours(4))).toInstant());
		when(timeServiceMock.getCurrentTime()).thenReturn(timestamp);
		String formattedTimestamp = "2011-04-17T11:10:00+0000";
		String token = "f00e65fbd8609a96c492f1aa83a98ced523e23e6d306f540bddb9b8156d31f320e51eb4ab71951f554d58c27606d3bb8fc222b09"
				+ "673dcb0bbe06cbe8b98e66e844d561f8e100e77f6a25a178ea8170e5";

		when(wileyMessageEncoderMock.encode(uid + formattedTimestamp)).thenReturn(token);

		// When
		String magicLink = wileyIntegrationService.getMagicLink(customerData);

		// Then
		Assert.assertEquals(String.format("https://test.graphicstandards.com/auth"
				+ "?uid=%s"
				+ "&fname=Alice"
				+ "&timestamp=2011-04-17T11:10:00%%2B0000"
				+ "&token=%s", uid, token), magicLink);
	}
}
