package com.wiley.core.wileyb2c.i18n.impl;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.model.LegacyCartConfigurationModel;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.servicelayer.session.SessionService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Optional;

import static com.wiley.core.constants.WileyCoreConstants.CURRENT_COUNTRY_SESSION_ATTR;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cI18NServiceImplTest
{
	public static final String TAX_EU_SHORT_MESSAGE = "eu short message";
	public static final String TAX_AU_SHORT_MESSAGE = "au short message";
	public static final String TAX_EU_TOOLTIP = "eu tooltip";
	public static final String TAX_AU_TOOLTIP = "au tooltip";
	public static final String OTHER_REGION_CODE = "OTHER REGION CODE";
	public static final String PRODUCT_TAX_AU_SHORT_MESSAGE = "product.tax.au.short.message";
	public static final String PRODUCT_TAX_AU_TOOLTIP = "product.tax.au.tooltip";
	public static final String PRODUCT_TAX_EU_SHORT_MESSAGE = "product.tax.eu.short.message";
	public static final String PRODUCT_TAX_EU_TOOLTIP = "product.tax.eu.tooltip";
	public static final String PUBLICATION_DATE_FORMAT_DEFAULT = "dd-MM-yyyy";
	public static final String PUBLICATION_DATE_FORMAT_US = "MM-dd-yyyy";
	public static final String ISOCODE_US = "us";
	public static final String ISOCODE_DEFAULT = "default";
	public static final String DATE_FORMAT_CONSTANT = "product.publication.date.format.";

	@Mock
	private SessionService sessionService;

	@Mock
	private CountryModel countryMock;

	@Mock
	private LegacyCartConfigurationModel legacyCartConfigurationModel;


	@Spy
	@InjectMocks
	private Wileyb2cI18NServiceImpl testedInstance = new Wileyb2cI18NServiceImpl();

	@Before
	public void setUp()
	{
		when(sessionService.getAttribute(CURRENT_COUNTRY_SESSION_ATTR)).thenReturn(countryMock);
		when(countryMock.getLegacyCart()).thenReturn(legacyCartConfigurationModel);

		doReturn(TAX_AU_SHORT_MESSAGE).when(testedInstance).getLocalizedString(PRODUCT_TAX_AU_SHORT_MESSAGE);
		doReturn(TAX_AU_TOOLTIP).when(testedInstance).getLocalizedString(PRODUCT_TAX_AU_TOOLTIP);
		doReturn(TAX_EU_SHORT_MESSAGE).when(testedInstance).getLocalizedString(PRODUCT_TAX_EU_SHORT_MESSAGE);

		doReturn(TAX_EU_TOOLTIP).when(testedInstance).getLocalizedString(PRODUCT_TAX_EU_TOOLTIP);
		doReturn(PUBLICATION_DATE_FORMAT_US)
				.when(testedInstance).getLocalizedString(DATE_FORMAT_CONSTANT + ISOCODE_US);
		doReturn(PUBLICATION_DATE_FORMAT_DEFAULT)
				.when(testedInstance).getLocalizedString(DATE_FORMAT_CONSTANT + ISOCODE_DEFAULT);

	}


	@Test
	public void testTaxDisclaimerIsRequiredForAustraliaCartRegion()
	{
		//given
		when(legacyCartConfigurationModel.getCode()).thenReturn(WileyCoreConstants.LEGACY_CART_REGION_AU);

		//when
		final Optional<String> shortMessage = testedInstance.getCurrentCountryTaxShortMsg();
		final Optional<String> tooltip = testedInstance.getCurrentCountryTaxTooltip();

		//then
		assertEquals(TAX_AU_SHORT_MESSAGE, shortMessage.get());
		assertEquals(TAX_AU_TOOLTIP, tooltip.get());
	}

	@Test
	public void testTaxDisclaimerIsRequiredForEuropeCartRegion()
	{
		//given
		when(legacyCartConfigurationModel.getCode()).thenReturn(WileyCoreConstants.LEGACY_CART_REGION_EU);

		//when
		final Optional<String> shortMessage = testedInstance.getCurrentCountryTaxShortMsg();
		final Optional<String> tooltip = testedInstance.getCurrentCountryTaxTooltip();

		//then
		assertEquals(TAX_EU_SHORT_MESSAGE, shortMessage.get());
		assertEquals(TAX_EU_TOOLTIP, tooltip.get());
	}

	@Test
	public void testTaxDisclaimerIsNotRequiredForEmptySessionCountry()
	{
		//when
		final Optional<String> shortMessage = testedInstance.getCurrentCountryTaxShortMsg();
		final Optional<String> tooltip = testedInstance.getCurrentCountryTaxTooltip();

		//then
		assertEquals(Optional.empty(), shortMessage);
		assertEquals(Optional.empty(), tooltip);
	}

	@Test
	public void testTaxDisclaimerIsNotRequiredForOtherCountries()
	{
		//given
		when(legacyCartConfigurationModel.getCode()).thenReturn(OTHER_REGION_CODE);

		//when
		final Optional<String> shortMessage = testedInstance.getCurrentCountryTaxShortMsg();
		final Optional<String> tooltip = testedInstance.getCurrentCountryTaxTooltip();

		//then
		assertEquals(Optional.empty(), shortMessage);
		assertEquals(Optional.empty(), tooltip);
	}

	@Test
	public void testCurrentDateFormatForUsCountry()
	{
		//given
		when(countryMock.getIsocode()).thenReturn(ISOCODE_US);

		//when
		final String dateFormat = testedInstance.getCurrentDateFormat();

		//then
		assertEquals(PUBLICATION_DATE_FORMAT_US, dateFormat);
	}

	@Test
	public void testCurrentDateFormatForNonUsCountry()
	{
		//given
		when(countryMock.getIsocode()).thenReturn(ISOCODE_DEFAULT);

		//when
		final String dateFormat = testedInstance.getCurrentDateFormat();

		//then
		assertEquals(PUBLICATION_DATE_FORMAT_DEFAULT, dateFormat);
	}
}