package com.wiley.core.wileyb2c.visibility.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.subscriptionservices.model.SubscriptionTermModel;
import de.hybris.platform.variants.model.VariantProductModel;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileyProductModel;
import com.wiley.core.wileyb2c.product.Wileyb2cCommercePriceService;
import com.wiley.core.wileyb2c.restriction.impl.Wileyb2cRestrictAvailabilityByPricesStrategy;
import com.wiley.core.wileyb2c.subscription.services.Wileyb2cFreeTrialSubscriptionTermCheckingStrategy;
import com.wiley.core.wileyb2c.subscription.services.Wileyb2cSubscriptionService;


/**
 * @author Dzmitryi_Halahayeu
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cRestrictAvailabilityByPricesStrategyUnitTest
{
	@InjectMocks
	private Wileyb2cRestrictAvailabilityByPricesStrategy wileyb2cRestrictAvailabilityByPricesStrategy;
	@Mock
	private Wileyb2cCommercePriceService commercePriceService;
	@Mock
	private Wileyb2cSubscriptionService wileyb2cSubscriptionService;
	@Mock
	private Wileyb2cFreeTrialSubscriptionTermCheckingStrategy wileyb2cFreeTrialSubscriptionCheckingStrategy;
	@Mock
	private WileyProductModel wileyProductModel;
	@Mock
	private ProductModel productModel;
	@Mock
	private SubscriptionTermModel subscriptionTerm;
	@Mock
	private PriceInformation priceInfo;
	@Mock
	private VariantProductModel productVariant;
	private CommerceCartParameter parameter;



	@Before
	public void before()
	{
		Set<SubscriptionTermModel> set = new HashSet<>();
		set.add(subscriptionTerm);
		parameter = new CommerceCartParameter();
		when(wileyProductModel.getSubscriptionTerms()).thenReturn(set);
	}

	@Test
	public void shouldBeNotRestrictedWhenSubscriptionAndTrial()
	{
		parameter.setProduct(wileyProductModel);
		parameter.setSubscriptionTerm(subscriptionTerm);
		when(wileyb2cSubscriptionService.isProductSubscription(wileyProductModel)).thenReturn(true);
		when(wileyb2cFreeTrialSubscriptionCheckingStrategy.isFreeTrial(subscriptionTerm)).thenReturn(true);

		final boolean restricted = wileyb2cRestrictAvailabilityByPricesStrategy.isRestricted(parameter);

		assertFalse(restricted);
	}

	@Test
	public void shouldBeNotRestrictedWhenSubscriptionAndHasPrice()
	{
		parameter.setProduct(wileyProductModel);
		parameter.setSubscriptionTerm(subscriptionTerm);
		when(wileyb2cSubscriptionService.isProductSubscription(wileyProductModel)).thenReturn(true);
		when(commercePriceService.validateProductSubscriptionTermPrice(wileyProductModel, subscriptionTerm)).thenReturn(true);

		final boolean restricted = wileyb2cRestrictAvailabilityByPricesStrategy.isRestricted(parameter);

		assertFalse(restricted);
	}

	@Test
	public void shouldBeRestrictedWhenSubscriptionAndHasNotPrice()
	{
		parameter.setProduct(wileyProductModel);
		parameter.setSubscriptionTerm(subscriptionTerm);
		when(wileyb2cSubscriptionService.isProductSubscription(wileyProductModel)).thenReturn(true);
		when(commercePriceService.validateProductSubscriptionTermPrice(wileyProductModel, subscriptionTerm)).thenReturn(false);

		final boolean restricted = wileyb2cRestrictAvailabilityByPricesStrategy.isRestricted(parameter);

		assertTrue(restricted);
	}

	@Test
	public void shouldBeNotRestrictedWhenNotSubscriptionAndHasNotVariantsAndHasWebPrice()
	{
		parameter.setProduct(productModel);
		when(productModel.getVariants()).thenReturn(Collections.emptyList());
		when(commercePriceService.getWebPriceForProduct(productModel)).thenReturn(priceInfo);

		final boolean restricted = wileyb2cRestrictAvailabilityByPricesStrategy.isRestricted(parameter);

		assertFalse(restricted);
	}

	@Test
	public void shouldBeRestrictedWhenNotSubscriptionAndHasNotVariantsAndHasNotWebPrice()
	{
		parameter.setProduct(productModel);
		when(productModel.getVariants()).thenReturn(Collections.emptyList());
		when(commercePriceService.getWebPriceForProduct(productModel)).thenReturn(null);

		final boolean restricted = wileyb2cRestrictAvailabilityByPricesStrategy.isRestricted(parameter);

		assertTrue(restricted);
	}

	@Test
	public void shouldBeNotRestrictedWhenNotSubscriptionAndHasVariantsAndHasPrice()
	{
		parameter.setProduct(productModel);
		when(productModel.getVariants()).thenReturn(Collections.singletonList(productVariant));
		when(commercePriceService.getFromPriceForProduct(productModel)).thenReturn(priceInfo);

		final boolean restricted = wileyb2cRestrictAvailabilityByPricesStrategy.isRestricted(parameter);

		assertFalse(restricted);
	}


	@Test
	public void shouldBeRestrictedWhenNotSubscriptionAndHasVariantsAndHasNotPrice()
	{
		parameter.setProduct(productModel);
		when(productModel.getVariants()).thenReturn(Collections.singletonList(productVariant));
		when(commercePriceService.getFromPriceForProduct(productModel)).thenReturn(null);

		final boolean restricted = wileyb2cRestrictAvailabilityByPricesStrategy.isRestricted(parameter);

		assertTrue(restricted);
	}
}
