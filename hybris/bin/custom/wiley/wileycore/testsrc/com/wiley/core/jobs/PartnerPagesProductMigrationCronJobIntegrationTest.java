package com.wiley.core.jobs;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.cronjob.CronJobService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.wiley.core.category.WileyCategoryService;
import com.wiley.core.model.jobs.PartnerPagesProductMigrationCronJobModel;

import junit.framework.Assert;


@IntegrationTest
public class PartnerPagesProductMigrationCronJobIntegrationTest extends ServicelayerTransactionalTest
{
	@Resource
	private CronJobService cronJobService;
	@Resource
	private ProductService productService;
	@Resource
	private WileyCategoryService categoryService;
	@Resource
	private ModelService modelService;
	@Resource
	private CatalogVersionService catalogVersionService;

	protected static final String DEFAULT_ENCODING = "UTF-8";

	@Before
	public void setUp() throws Exception
	{
		importCsv("/wileycore/test/jobs/testPartnerPagesProductMigrationCronJob.impex", DEFAULT_ENCODING);
		catalogVersionService.setSessionCatalogVersion("welProductCatalog", "Online");
	}

	@Test
	public void testPerformPartnerPagesProductMigrationCronJob() throws Exception
	{
		final PartnerPagesProductMigrationCronJobModel cronJob =
				(PartnerPagesProductMigrationCronJobModel) cronJobService.getCronJob("testPartnerPagesProductMigrationCronJob");
		ProductModel newProduct = productService.getProductForCode("WEL_PARTNER_PRODUCT_NEW");
		final ProductModel oldProduct = productService.getProductForCode("WEL_PARTNER_PRODUCT_OLD");

		Set<CategoryModel> oldPartnerCategories = getParnerCategories(oldProduct);

		cronJob.setNewProduct(newProduct);
		cronJob.setOldProduct(oldProduct);

		modelService.save(cronJob);

		cronJobService.performCronJob(cronJob, true);

		modelService.refresh(cronJob);
		modelService.refresh(newProduct);

		Assert.assertEquals(CronJobResult.SUCCESS, cronJob.getResult());
		Assert.assertEquals(CronJobStatus.FINISHED, cronJob.getStatus());

		Set<CategoryModel> newPartnerCategories = getParnerCategories(newProduct);
		assert (oldPartnerCategories.containsAll(newPartnerCategories));
		assert (newPartnerCategories.containsAll(oldPartnerCategories));
	}

	private Set<CategoryModel> getParnerCategories(final ProductModel product)
	{
		Set<CategoryModel> partnerCategories = new HashSet<CategoryModel>();
		for (CategoryModel category : product.getSupercategories())
		{
			boolean isPartenerCategory = category.getSupercategories().stream().anyMatch(
					c -> categoryService.isPartnersCategory(c));
			if (isPartenerCategory)
			{
				partnerCategories.add(category);
			}
		}
		return partnerCategories;
	}

}
