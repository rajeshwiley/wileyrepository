package com.wiley.core.subscription.services.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.time.TimeService;
import de.hybris.platform.subscriptionservices.enums.SubscriptionStatus;
import de.hybris.platform.subscriptionservices.model.SubscriptionProductModel;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.integration.esb.EsbB2CSubscriptionGateway;
import com.wiley.core.model.WileySubscriptionModel;
import com.wiley.core.subscription.dao.WileySubscriptionDao;


/**
 * Unit test for {@link WileySubscriptionServiceImpl}.<br/>
 *
 * @author Aliaksei_Zlobich
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileySubscriptionServiceImplUnitTest
{

	@Mock
	private WileySubscriptionDao wileySubscriptionDaoMock;

	@Mock
	private TimeService timeServiceMock;

	@InjectMocks
	private WileySubscriptionServiceImpl wileySubscriptionService = new WileySubscriptionServiceImpl();

	@Mock
	private CartModel cartModelMock;

	@Mock
	private AbstractOrderEntryModel orderEntryModelMock;

	@Mock
	private EsbB2CSubscriptionGateway esbB2CSubscriptionGatewayMock;

	@Mock
	private ModelService modelService;

	private Date currentDateStub;

	@Before
	public void setUp() throws Exception
	{
		currentDateStub = new Date();

		when(timeServiceMock.getCurrentTime()).thenReturn(currentDateStub);
	}

	@Test
	public void methodDoesAnySubscriptionProductExistInCartShouldReturnTrueWhenOnlySubscriptionProduct()
	{
		// Given
		// creating mock entry with SubscriptionProduct.
		initCartModelMock(Arrays.asList(new SubscriptionProductModel()));

		// When
		final boolean result = wileySubscriptionService.doesAnySubscriptionProductExistInCart(cartModelMock);

		// Then
		assertTrue("Expected that cart has subscription product.", result);
	}

	@Test
	public void methodDoesAnySubscriptionProductExistInCartShouldReturnFalseWhenOnlyRegularProduct()
	{
		// Given
		// creating mock entry with Product.
		initCartModelMock(Arrays.asList(new ProductModel()));

		// When
		final boolean result = wileySubscriptionService.doesAnySubscriptionProductExistInCart(cartModelMock);

		// Then
		assertFalse("Expected that cart doesn't have subscription product.", result);
	}

	@Test
	public void methodDoesAnySubscriptionProductExistInCartShouldReturnTrueWhenSubscriptionProductAtTheBeginning()
	{
		// Given
		initCartModelMock(Arrays.asList(new SubscriptionProductModel(), new ProductModel(), new ProductModel()));

		// When
		final boolean result = wileySubscriptionService.doesAnySubscriptionProductExistInCart(cartModelMock);

		// Then
		assertTrue("Expected that cart has subscription product.", result);
	}

	@Test
	public void methodDoesAnySubscriptionProductExistInCartShouldReturnTrueWhenSubscriptionProductInTheMiddle()
	{
		// Given
		initCartModelMock(Arrays.asList(new ProductModel(), new SubscriptionProductModel(), new ProductModel()));

		// When
		final boolean result = wileySubscriptionService.doesAnySubscriptionProductExistInCart(cartModelMock);

		// Then
		assertTrue("Expected that cart has subscription product.", result);
	}

	@Test
	public void methodDoesAnySubscriptionProductExistInCartShouldReturnTrueWhenSubscriptionProductAtTheEnd()
	{
		// Given
		initCartModelMock(Arrays.asList(new ProductModel(), new ProductModel(), new SubscriptionProductModel()));

		// When
		final boolean result = wileySubscriptionService.doesAnySubscriptionProductExistInCart(cartModelMock);

		// Then
		assertTrue("Expected that cart has subscription product.", result);
	}

	@Test
	public void methodDoesAnySubscriptionProductExistInCartShouldReturnFalseWhenNoSubscriptionProduct()
	{
		// Given
		initCartModelMock(Arrays.asList(new ProductModel(), new ProductModel(), new ProductModel()));

		// When
		final boolean result = wileySubscriptionService.doesAnySubscriptionProductExistInCart(cartModelMock);

		// Then
		assertFalse("Expected that cart doesn't have subscription product.", result);
	}

	@Test
	public void methodClonePaymentInfoAndAddressAndDeleteOld()
	{

		doNothing().when(esbB2CSubscriptionGatewayMock).changePaymentMethod(any(PaymentInfoModel.class), anyString());

		final CreditCardPaymentInfoModel cloned = simulateCloning();
		when(modelService.clone(any())).thenReturn(cloned);

		PaymentInfoModel pinfoAccountMock = Mockito.mock(CreditCardPaymentInfoModel.class);
		when(pinfoAccountMock.getDuplicate()).thenReturn(false);

		PaymentInfoModel pinfoPreviousMock = Mockito.mock(CreditCardPaymentInfoModel.class);
		when(pinfoPreviousMock.getDuplicate()).thenReturn(true);
		WileySubscriptionModel subscriptionMock = Mockito.mock(WileySubscriptionModel.class);
		when(subscriptionMock.getPaymentInfo()).thenReturn(pinfoPreviousMock);

		wileySubscriptionService.updatePaymentInfo(subscriptionMock, pinfoAccountMock);

		assertEquals(Boolean.TRUE, cloned.getDuplicate());
		assertEquals(pinfoAccountMock, cloned.getOriginal());
		assertEquals(subscriptionMock, cloned.getOwner());
		verify(modelService).saveAll(eq(subscriptionMock), eq(cloned));
		verify(modelService).remove(eq(pinfoPreviousMock));

	}

	@Test
	public void methodClonePaymentInfoAndAddressAndLeaveOld()
	{

		doNothing().when(esbB2CSubscriptionGatewayMock).changePaymentMethod(any(PaymentInfoModel.class), anyString());

		final CreditCardPaymentInfoModel cloned = simulateCloning();
		when(modelService.clone(any())).thenReturn(cloned);

		PaymentInfoModel pinfoAccountMock = Mockito.mock(CreditCardPaymentInfoModel.class);
		when(pinfoAccountMock.getDuplicate()).thenReturn(false);

		PaymentInfoModel pinfoPreviousMock = Mockito.mock(CreditCardPaymentInfoModel.class);
		when(pinfoPreviousMock.getDuplicate()).thenReturn(false);
		WileySubscriptionModel subscriptionMock = Mockito.mock(WileySubscriptionModel.class);
		when(subscriptionMock.getPaymentInfo()).thenReturn(pinfoPreviousMock);

		wileySubscriptionService.updatePaymentInfo(subscriptionMock, pinfoAccountMock);

		assertEquals(Boolean.TRUE, cloned.getDuplicate());
		assertEquals(pinfoAccountMock, cloned.getOriginal());
		assertEquals(subscriptionMock, cloned.getOwner());
		verify(modelService).saveAll(eq(subscriptionMock), eq(cloned));
		verify(modelService, times(0)).remove(any(CreditCardPaymentInfoModel.class));

	}

	@Test
	public void testGetAllSubscriptionsWhichShouldBeRenewedTodayShouldReturnOnlyHybrisRenewalSubscriptions()
	{
		// Given
		WileySubscriptionModel wileySubscriptionModelMock1 = mock(WileySubscriptionModel.class);
		WileySubscriptionModel wileySubscriptionModelMock2 = mock(WileySubscriptionModel.class);

		// prepare hybris renewal subscription
		when(wileySubscriptionModelMock1.getHybrisRenewal()).thenReturn(false);
		when(wileySubscriptionModelMock1.getRequireRenewal()).thenReturn(true);
		when(wileySubscriptionModelMock1.getRenewalEnabled()).thenReturn(true);

		// prepare non hybris renewal subscription
		when(wileySubscriptionModelMock2.getHybrisRenewal()).thenReturn(true);
		when(wileySubscriptionModelMock2.getRequireRenewal()).thenReturn(true);
		when(wileySubscriptionModelMock2.getRenewalEnabled()).thenReturn(true);

		when(wileySubscriptionDaoMock
				.findSubscriptionsWithExpirationDateBetween(eq(SubscriptionStatus.ACTIVE), any(ZonedDateTime.class),
						any(ZonedDateTime.class))).thenReturn(
				Arrays.asList(wileySubscriptionModelMock1, wileySubscriptionModelMock2));

		// When
		final List<WileySubscriptionModel> resultList =
				wileySubscriptionService.getAllSubscriptionsWhichShouldBeRenewed();

		// Then
		assertEquals(Arrays.asList(wileySubscriptionModelMock2), resultList);
	}

	private CreditCardPaymentInfoModel simulateCloning()
	{
		CreditCardPaymentInfoModel pinfoClone = new CreditCardPaymentInfoModel();
		AddressModel billingAddress = new AddressModel();
		pinfoClone.setBillingAddress(billingAddress);
		return pinfoClone;
	}


	private void initCartModelMock(final List<ProductModel> products)
	{
		final List<AbstractOrderEntryModel> orderEntries = products.stream()
				.map(product -> {
					AbstractOrderEntryModel orderEntryModelMock = mock(AbstractOrderEntryModel.class);
					when(orderEntryModelMock.getProduct()).thenReturn(product);
					return orderEntryModelMock;
				})
				.collect(Collectors.toList());
		when(cartModelMock.getEntries()).thenReturn(orderEntries);
	}

}