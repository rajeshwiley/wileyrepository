package com.wiley.core.jobs;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;

import java.util.Arrays;
import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.model.WileySubscriptionModel;
import com.wiley.core.subscription.events.SubscriptionRenewalEvent;
import com.wiley.core.subscription.services.WileySubscriptionService;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


/**
 * Default unit test for {@link SubscriptionRenewalJobPerformable}.<br/>
 *
 * Test checks event publishing in different cases.
 *
 * @author Aliaksei_Zlobich
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SubscriptionRenewalJobPerformableUnitTest
{

	private SubscriptionRenewalJobPerformable subscriptionRenewalJobPerformable;

	@Mock
	private EventService eventServiceMock;

	@Mock
	private WileySubscriptionService wileySubscriptionServiceMock;

	@Mock
	private WileySubscriptionModel wileySubscriptionModelMock;

	@Mock
	private CronJobModel cronJobModelMock;

	@Before
	public void setUp() throws Exception
	{
		subscriptionRenewalJobPerformable = new SubscriptionRenewalJobPerformable();
		subscriptionRenewalJobPerformable.setEventService(eventServiceMock);
		subscriptionRenewalJobPerformable.setWileySubscriptionService(wileySubscriptionServiceMock);
	}

	@Test
	public void shouldSendEventProductsNeedRenewal()
	{
		// Given
		when(wileySubscriptionServiceMock.getAllSubscriptionsWhichShouldBeRenewed()).thenReturn(Arrays.asList(
				wileySubscriptionModelMock));

		// When
		subscriptionRenewalJobPerformable.perform(cronJobModelMock);

		// Then
		verify(eventServiceMock).publishEvent(argThat(new ArgumentMatcher<AbstractEvent>()
		{
			@Override
			public boolean matches(final Object argument)
			{
				SubscriptionRenewalEvent event = (SubscriptionRenewalEvent) argument;
				return wileySubscriptionModelMock.equals(event.getSubscription());
			}
		}));
	}

	@Test
	public void shouldNotSendEventIfProductsDoNotNeedRenewal()
	{
		// Given
		when(wileySubscriptionServiceMock.getAllSubscriptionsWhichShouldBeRenewed()).thenReturn(
				Collections.<WileySubscriptionModel> emptyList());

		// When
		subscriptionRenewalJobPerformable.perform(cronJobModelMock);

		// Then
		verify(eventServiceMock, never()).publishEvent(any(SubscriptionRenewalEvent.class));
	}
}