package com.wiley.core.category.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.catalog.model.classification.ClassificationSystemVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantCategoryModel;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import org.apache.commons.collections.CollectionUtils;
import org.hamcrest.collection.IsIterableContainingInOrder;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.google.common.collect.FluentIterable;
import com.wiley.core.constants.WileyCoreConstants;

import static junit.framework.TestCase.assertFalse;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


/**
 * Unit test for {@link DefaultWileyCategoryService}
 *
 * @author Aliaksei_Zlobich
 */
@UnitTest
public class DefaultWileyCategoryServiceUnitTest
{
	private static final String PARTNERS_CATEGORY_CODE = "WEL_PARTNERS_CATEGORY";
	private static final String GIFT_CARDS_CATEGORY_CODE = "WEL_GIFT_CARDS_CATEGORY";
	private static final String FREE_TRIAL_CATEGORY_CODE = "WEL_CFA_FREE_TRIAL_CATEGORY";
	private static final String SUPPLEMENTS_CATEGORY_CODE = "WEL_CPA_SUPPLEMENTS_CATEGORY";
	private static final String WEL_CFA_LEVEL_VARIANT_CATEGORY_CODE = "WEL_CFA_LEVEL_1";

	@Mock
	private ProductModel product;
	// Root level categories
	@Mock
	private CategoryModel rootCategory1;
	@Mock
	private CategoryModel rootCategory2;
	// First level categories
	@Mock
	private CategoryModel firstLevel1;
	@Mock
	private CategoryModel firstLevel2;
	@Mock
	private CategoryModel firstLevel3;
	@Mock
	private CategoryModel firstLevel4;
	@Mock
	private CategoryModel firstLevel5;
	@Mock
	private CategoryModel firstLevel6;
	// Second level categorise
	@Mock
	private CategoryModel secondLevel1;
	@Mock
	private CategoryModel secondLevel2;
	@Mock
	private CategoryModel secondLevel3;
	@Mock
	private CategoryModel secondLevel4;
	@Mock
	private CategoryModel secondLevel5;
	@Mock
	private CategoryModel secondLevel6;
	@Mock
	private CategoryModel secondLevel7;
	@Mock
	private CategoryModel secondLevel8;
	@Mock
	private CategoryModel secondLevel9;
	@Mock
	private CategoryModel secondLevel10;
	@Mock
	private ClassificationSystemVersionModel catalogVersion;

	private DefaultWileyCategoryService defaultWileyCategoryService;


	/**
	 * SetUp method.
	 */
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		this.defaultWileyCategoryService = new DefaultWileyCategoryService();

		// Set up category structure
		// root 1 to first level 1,2
		when(rootCategory1.getCategories()).thenReturn(Arrays.asList(firstLevel1, firstLevel2));
		when(firstLevel1.getSupercategories()).thenReturn(Arrays.asList(rootCategory1));
		when(firstLevel2.getSupercategories()).thenReturn(Arrays.asList(rootCategory1));
		// root 2 to first level 3,4
		when(rootCategory2.getCategories()).thenReturn(Arrays.asList(firstLevel3, firstLevel4));
		when(firstLevel3.getSupercategories()).thenReturn(Arrays.asList(rootCategory2));
		when(firstLevel4.getSupercategories()).thenReturn(Arrays.asList(rootCategory2));
		// first level 1,2 to second level 1,2,3,4
		when(firstLevel1.getCategories()).thenReturn(Arrays.asList(secondLevel1, secondLevel2));
		when(secondLevel1.getSupercategories()).thenReturn(Arrays.asList(firstLevel1));
		when(secondLevel2.getSupercategories()).thenReturn(Arrays.asList(firstLevel1));
		when(firstLevel2.getCategories()).thenReturn(Arrays.asList(secondLevel3, secondLevel4));
		when(secondLevel3.getSupercategories()).thenReturn(Arrays.asList(firstLevel2));
		when(secondLevel4.getSupercategories()).thenReturn(Arrays.asList(firstLevel2));
		// first level 3,4 to second level 5,6,7,8
		when(firstLevel3.getCategories()).thenReturn(Arrays.asList(secondLevel5, secondLevel6));
		when(secondLevel5.getSupercategories()).thenReturn(Arrays.asList(firstLevel3));
		when(secondLevel6.getSupercategories()).thenReturn(Arrays.asList(firstLevel3));
		when(firstLevel4.getCategories()).thenReturn(Arrays.asList(secondLevel7, secondLevel8));
		when(secondLevel7.getSupercategories()).thenReturn(Arrays.asList(firstLevel4));
		when(secondLevel8.getSupercategories()).thenReturn(Arrays.asList(firstLevel4));

		when(rootCategory1.getCatalogVersion()).thenReturn(catalogVersion);
		when(rootCategory2.getCatalogVersion()).thenReturn(catalogVersion);
	}

	/**
	 * Test get system requirements from category tree empty list.
	 */
	@Test
	public void testGetSystemRequirementsFromCategoryTreeEmptyList()
	{
		// Given
		final List<CategoryModel> categories = Collections.emptyList();

		// When
		final String systemRequirements = defaultWileyCategoryService.getSystemRequirementsFromCategoryTree(
				categories, categoryModel -> true);

		// Then
		assertNull("Expected null because source list is empty.", systemRequirements);
	}

	/**
	 * Test get system requirements from category tree null list.
	 */
	@Test
	public void testGetSystemRequirementsFromCategoryTreeNullList()
	{
		// Given
		final List<CategoryModel> categories = null;

		// When
		final String systemRequirements = defaultWileyCategoryService.getSystemRequirementsFromCategoryTree(
				categories, categoryModel -> true);

		// Then
		assertNull("Expected null because source list is null.", systemRequirements);
	}

	/**
	 * Test get system requirements from category tree success on root level.
	 */
	@Test
	public void testGetSystemRequirementsFromCategoryTreeSuccessOnRootLevel()
	{
		// Given
		final String srRoot1 = "SystemRequirements.RootCategory1";
		when(rootCategory1.getSystemRequirements()).thenReturn(srRoot1);


		// When
		String systemRequirements = defaultWileyCategoryService.getSystemRequirementsFromCategoryTree(
				Arrays.asList(secondLevel3, secondLevel4), categoryModel -> true);

		// Then
		assertEquals(srRoot1, systemRequirements);
	}

	/**
	 * Test get system requirements from category tree success on first level.
	 */
	@Test
	public void testGetSystemRequirementsFromCategoryTreeSuccessOnFirstLevel()
	{
		// Given
		final String stFirst1 = "SystemRequirements.FirstLevel2";
		when(firstLevel1.getSystemRequirements()).thenReturn(stFirst1);

		// When
		String systemRequirements = defaultWileyCategoryService.getSystemRequirementsFromCategoryTree(
				Arrays.asList(secondLevel1, secondLevel2), categoryModel -> true);

		// Then
		assertEquals(stFirst1, systemRequirements);
	}

	/**
	 * Test get system requirements from category tree not found.
	 */
	@Test
	public void testGetSystemRequirementsFromCategoryTreeNotFound()
	{
		// Given
		final String srRoot1 = "SystemRequirements.RootCategory1";
		when(rootCategory1.getSystemRequirements()).thenReturn(srRoot1);

		// When
		String systemRequirements = defaultWileyCategoryService.getSystemRequirementsFromCategoryTree(
				Arrays.asList(secondLevel7, secondLevel8), categoryModel -> true);

		// Then
		assertNull("Expected null because there are no system requirements on category tree.", systemRequirements);
	}

	/**
	 * Test get system requirements from category tree found the closest.
	 */
	@Test
	public void testGetSystemRequirementsFromCategoryTreeFoundTheClosest()
	{
		// Given
		final String stFirst1 = "SystemRequirements.FirstLevel2";
		when(firstLevel1.getSystemRequirements()).thenReturn(stFirst1);

		final String stSecond6 = "SystemRequirements.SecondLevel6";
		when(secondLevel6.getSystemRequirements()).thenReturn(stSecond6);

		// When
		String systemRequirements = defaultWileyCategoryService.getSystemRequirementsFromCategoryTree(
				Arrays.asList(secondLevel1, secondLevel7, secondLevel8, secondLevel6), categoryModel -> true);

		// Then
		assertEquals(stSecond6, systemRequirements);
	}

	/**
	 * Test filter categories filter category.
	 */
	@Test
	public void testFilterCategoriesFilterCategory()
	{
		// Given
		List<CategoryModel> sourceList = Arrays.asList(new CategoryModel(), new VariantCategoryModel(), new CategoryModel(), new
				ClassificationClassModel(), new CategoryModel());

		// When
		final Collection<CategoryModel> filteredList = defaultWileyCategoryService.filterCategories(sourceList,
				categoryModel -> CategoryModel.class.equals(categoryModel
						.getClass()));

		// Then
		assertTrue("Expected only simple categories.", FluentIterable.from(filteredList).allMatch(categoryModel -> CategoryModel
				.class.equals(categoryModel.getClass())));
	}

	/**
	 * Test filter categories null filter.
	 */
	@Test
	public void testFilterCategoriesNullFilter()
	{
		// Given
		List<CategoryModel> sourceList = Arrays.asList(new CategoryModel(), new VariantCategoryModel(), new CategoryModel(), new
				ClassificationClassModel(), new CategoryModel());
		int sourceListSize = sourceList.size();

		// When
		final Collection<CategoryModel> filteredList = defaultWileyCategoryService.filterCategories(sourceList,
				null);

		// Then
		assertEquals("Expected the same size.", sourceListSize, filteredList.size());
		assertTrue(FluentIterable.from(filteredList).anyMatch(categoryModel -> CategoryModel.class.equals(
				categoryModel.getClass())));
		assertTrue(FluentIterable.from(filteredList).anyMatch(categoryModel -> VariantCategoryModel.class.equals(
				categoryModel.getClass())));
		assertTrue(FluentIterable.from(filteredList).anyMatch(categoryModel -> ClassificationClassModel.class.equals(
				categoryModel.getClass())));
	}

	/**
	 * Test get parent categories collection null source collection.
	 */
	@Test
	public void testGetParentCategoriesCollectionNullSourceCollection()
	{
		// Given
		List<CategoryModel> sourceCollection = null;

		// When
		final Collection<CategoryModel> resultList = this.defaultWileyCategoryService.getParentCategoriesCollection(
				sourceCollection);

		// Then
		assertNotNull(resultList);
		assertTrue("Expected empty collection.", CollectionUtils.isEmpty(resultList));
	}

	/**
	 * Test get parent categories collection success 1.
	 */
	@Test
	public void testGetParentCategoriesCollectionSuccess1()
	{
		// Given
		List<CategoryModel> sourceCollection = Arrays.asList(secondLevel1);

		// When
		final Collection<CategoryModel> resultList = this.defaultWileyCategoryService.getParentCategoriesCollection(
				sourceCollection);

		// Then
		assertNotNull(resultList);
		assertEquals(1, resultList.size());
		assertEquals(firstLevel1, resultList.iterator().next());
	}

	/**
	 * Test get parent categories collection success 2.
	 */
	@Test
	public void testGetParentCategoriesCollectionSuccess2()
	{
		// Given
		List<CategoryModel> sourceCollection = Arrays.asList(secondLevel1, secondLevel8);

		// When
		final Collection<CategoryModel> resultList = this.defaultWileyCategoryService.getParentCategoriesCollection(
				sourceCollection);

		// Then
		assertNotNull(resultList);
		assertEquals(2, resultList.size());
		assertTrue(IsIterableContainingInOrder.contains(is(firstLevel1), is(firstLevel4)).matches(resultList));
	}

	@Test
	public void shouldReturnPrimaryCategoryForProduct()
	{
		// Given
		final CategoryModel rootCategory = new CategoryModel();
		setUpRootCategory(rootCategory);
		// When
		final CategoryModel primaryCategory = defaultWileyCategoryService.getPrimaryWileyCategoryForProduct(product);
		// Then
		assertEquals(rootCategory, primaryCategory);
	}

	@Test
	public void shouldReturnPrimaryCategoryFromSuperCategory()
	{
		// Given
		final CategoryModel rootCategory = new CategoryModel();
		rootCategory.setCatalogVersion(catalogVersion);
		when(firstLevel2.getSupercategories()).thenReturn(Arrays.asList(rootCategory));
		when(secondLevel2.getSupercategories()).thenReturn(Arrays.asList(firstLevel2));
		when(product.getSupercategories()).thenReturn(Arrays.asList(secondLevel2));
		when(catalogVersion.getRootCategories()).thenReturn(Arrays.asList(rootCategory));

		// When
		final CategoryModel primaryCategory = defaultWileyCategoryService.getPrimaryWileyCategoryForProduct(product);
		// Then
		assertEquals(rootCategory, primaryCategory);
	}

	@Test
	public void shouldReturnNullIfPrimaryCategoryIsClassificationCategory()
	{
		// Given
		setUpRootCategory(new ClassificationClassModel());
		// When
		final CategoryModel primaryCategory = defaultWileyCategoryService.getPrimaryWileyCategoryForProduct(product);
		// Then
		assertNull(primaryCategory);
	}

	@Test
	public void shouldReturnNullIfPrimaryCategoryIsVariantCategory()
	{
		// Given
		setUpRootCategory(new VariantCategoryModel());
		// When
		final CategoryModel primaryCategory = defaultWileyCategoryService.getPrimaryWileyCategoryForProduct(product);
		// Then
		assertNull(primaryCategory);
	}

	@Test
	public void shouldReturnPrimaryCategoryDifferentFromPartner()
	{
		// Given
		final CategoryModel rootCategory = new CategoryModel();
		final CategoryModel partnerCategory = new CategoryModel();
		partnerCategory.setCode(PARTNERS_CATEGORY_CODE);
		rootCategory.setCatalogVersion(catalogVersion);
		partnerCategory.setCatalogVersion(catalogVersion);

		when(product.getSupercategories()).thenReturn(Arrays.asList(partnerCategory, rootCategory));
		when(catalogVersion.getRootCategories()).thenReturn(Arrays.asList(partnerCategory, rootCategory));

		// When
		final CategoryModel primaryCategory = defaultWileyCategoryService.getPrimaryWileyCategoryForProduct(product);
		// Then
		assertEquals(rootCategory, primaryCategory);
	}

	@Test
	public void shouldReturnPrimaryCategoryDifferentFromGiftCard()
	{
		// Given
		final CategoryModel rootCategory = new CategoryModel();
		final CategoryModel giftCardCategory = new CategoryModel();
		giftCardCategory.setCode(GIFT_CARDS_CATEGORY_CODE);
		rootCategory.setCatalogVersion(catalogVersion);
		giftCardCategory.setCatalogVersion(catalogVersion);

		when(product.getSupercategories()).thenReturn(Arrays.asList(giftCardCategory, rootCategory));
		when(catalogVersion.getRootCategories()).thenReturn(Arrays.asList(giftCardCategory, rootCategory));

		// When
		final CategoryModel primaryCategory = defaultWileyCategoryService.getPrimaryWileyCategoryForProduct(product);
		// Then
		assertEquals(rootCategory, primaryCategory);
	}

	@Test
	public void shouldReturnGiftCardCategoryAsPrimaryCategory()
	{
		final CategoryModel giftCardCategory = new CategoryModel();
		giftCardCategory.setCode(GIFT_CARDS_CATEGORY_CODE);
		setUpRootCategory(giftCardCategory);
		// When
		final CategoryModel primaryCategory = defaultWileyCategoryService.getPrimaryWileyCategoryForProduct(product);
		// Then
		assertEquals(giftCardCategory, primaryCategory);
	}

	@Test
	public void shouldReturnPartnerCategoryAsPrimaryCategory()
	{
		final CategoryModel partnerCategory = new CategoryModel();
		partnerCategory.setCode(PARTNERS_CATEGORY_CODE);
		setUpRootCategory(partnerCategory);
		// When
		final CategoryModel primaryCategory = defaultWileyCategoryService.getPrimaryWileyCategoryForProduct(product);
		// Then
		assertEquals(partnerCategory, primaryCategory);
	}

	@Test
	public void shouldReturnTrueIfCategoryIsCFA()
	{
		when(rootCategory1.getCode()).thenReturn(WileyCoreConstants.WEL_CFA_CATEGORY_CODE);
		assertTrue(defaultWileyCategoryService.isCFACategory(rootCategory1));
	}

	@Test
	public void shouldReturnFalseIfCategoryIsNotCFA()
	{
		when(rootCategory1.getCode()).thenReturn(GIFT_CARDS_CATEGORY_CODE);
		assertFalse(defaultWileyCategoryService.isCFACategory(rootCategory1));
	}

	@Test
	public void shouldReturnTrueIfCategoryIsCFALevelVariantCategory()
	{
		when(rootCategory1.getCode()).thenReturn(WEL_CFA_LEVEL_VARIANT_CATEGORY_CODE);
		assertTrue(defaultWileyCategoryService.isCFALevelVariantCategory(rootCategory1));
	}

	@Test
	public void shouldReturnFalseIfCategoryIsNotCFALevelVariantCategory()
	{
		when(rootCategory1.getCode()).thenReturn(GIFT_CARDS_CATEGORY_CODE);
		assertFalse(defaultWileyCategoryService.isCFALevelVariantCategory(rootCategory1));
	}

	@Test
	public void shouldReturnTrueIfCategoryIsFreeTrial()
	{
		when(rootCategory1.getCode()).thenReturn(FREE_TRIAL_CATEGORY_CODE);
		assertTrue(defaultWileyCategoryService.isFreeTrialCategory(rootCategory1));
	}

	@Test
	public void shouldReturnFalseIfCategoryIsNotFreeTrial()
	{
		when(rootCategory1.getCode()).thenReturn(GIFT_CARDS_CATEGORY_CODE);
		assertFalse(defaultWileyCategoryService.isFreeTrialCategory(rootCategory1));
	}

	@Test
	public void shouldReturnTrueIfCategoryIsGiftCards()
	{
		when(rootCategory1.getCode()).thenReturn(GIFT_CARDS_CATEGORY_CODE);
		assertTrue(defaultWileyCategoryService.isGiftCardsCategory(rootCategory1));
	}

	@Test
	public void shouldReturnFalseIfCategoryIsNotGiftCards()
	{
		when(rootCategory1.getCode()).thenReturn(FREE_TRIAL_CATEGORY_CODE);
		assertFalse(defaultWileyCategoryService.isGiftCardsCategory(rootCategory1));
	}

	@Test
	public void shouldReturnTrueIfCategoryCodeIsPartners()
	{
		when(rootCategory1.getCode()).thenReturn(PARTNERS_CATEGORY_CODE);
		assertTrue(defaultWileyCategoryService.isPartnersCategory(rootCategory1));
	}

	@Test
	public void shouldReturnTrueIfCategoryCodeIsNotPartnersButSuperOneIs()
	{
		when(rootCategory1.getCode()).thenReturn(PARTNERS_CATEGORY_CODE);
		when(firstLevel1.getCode()).thenReturn(GIFT_CARDS_CATEGORY_CODE);
		when(firstLevel1.getSupercategories()).thenReturn(Arrays.asList(rootCategory1));
		assertTrue(defaultWileyCategoryService.isPartnersCategory(firstLevel1));
	}

	@Test
	public void shouldReturnFalseIfCategoryCodeIsNotPartnersAndNoSuperCategories()
	{
		when(rootCategory1.getSupercategories()).thenReturn(null);
		assertFalse(defaultWileyCategoryService.isPartnersCategory(rootCategory1));
	}

	@Test
	public void shouldReturnFalseIfNorCategoryCodeNorSuperOnesArePartners()
	{
		when(rootCategory1.getCode()).thenReturn(GIFT_CARDS_CATEGORY_CODE);
		when(firstLevel1.getSupercategories()).thenReturn(Arrays.asList(rootCategory1));
		assertFalse(defaultWileyCategoryService.isPartnersCategory(rootCategory1));
	}

	@Test
	public void shouldReturnTrueIfCategoryIsSupplements()
	{
		when(rootCategory1.getCode()).thenReturn(SUPPLEMENTS_CATEGORY_CODE);
		assertTrue(defaultWileyCategoryService.isSupplementsCategory(rootCategory1));
	}

	@Test
	public void shouldReturnFalseIfCategoryIsNotSupplements()
	{
		when(rootCategory1.getCode()).thenReturn(FREE_TRIAL_CATEGORY_CODE);
		assertFalse(defaultWileyCategoryService.isSupplementsCategory(rootCategory1));
	}

	private void setUpRootCategory(final CategoryModel rootCategory)
	{
		rootCategory.setCatalogVersion(catalogVersion);
		when(product.getSupercategories()).thenReturn(Arrays.asList(rootCategory));
		when(catalogVersion.getRootCategories()).thenReturn(Arrays.asList(rootCategory1, rootCategory2, rootCategory));
	}

	@Test
	public void getTopLevelCategoriesForCategoryShouldReturnOnlyTopLevelCategories() {
		//Given
		final CategoryModel topLevelCategory1 = givenCategory();
		final CategoryModel topLevelCategory2 = givenCategory();
		final CategoryModel childLevelCategory = givenCategory(topLevelCategory1);
		final CategoryModel lowestChildCategory = givenCategory(topLevelCategory2, childLevelCategory);
		//When
		Collection<CategoryModel> result = defaultWileyCategoryService.getTopLevelCategoriesForCategory(lowestChildCategory);
		//Then
		assertTrue(result.contains(topLevelCategory1));
		assertTrue(result.contains(topLevelCategory2));
		assertTrue(result.size() == 2);
	}

	@Test
	public void getTopLevelCategoriesForCategoryShouldNotReturnClassificationCategories() {
		final CategoryModel topLevelCategory1 = givenCategory(givenClassifyingCategory());
		final CategoryModel topLevelCategory2 = givenCategory(givenClassifyingCategory());
		final CategoryModel childLevelCategory = givenCategory(topLevelCategory1);
		final CategoryModel lowestChildCategory = givenCategory(topLevelCategory2, childLevelCategory);

		//When
		Collection<CategoryModel> result = defaultWileyCategoryService.getTopLevelCategoriesForCategory(lowestChildCategory);
		//Then
		assertTrue(result.contains(topLevelCategory1));
		assertTrue(result.contains(topLevelCategory2));
		assertTrue(result.size() == 2);
	}

	@Test
	public void getTopLevelCategoriesForCategoryShouldNotReturnClassificationCategory() {
		final CategoryModel topLevelCategory1 = givenCategory(givenClassifyingCategory());
		//When
		Collection<CategoryModel> result = defaultWileyCategoryService.getTopLevelCategoriesForCategory(topLevelCategory1);
		//Then
		assertTrue(result.contains(topLevelCategory1));
		assertTrue(result.size() == 1);
	}

	@Test
	public void testGetSubcategoriesWithDescendantProductsShouldReturnFirstLevelSubCategoriesWithAtLeastOneDescendantProduct() {
		//given
		List<ProductModel> products = Arrays.asList(product);
		when(rootCategory2.getCategories()).thenReturn(Arrays.asList(firstLevel3, firstLevel4, firstLevel5));
		when(firstLevel4.getProducts()).thenReturn(products);
		when(firstLevel5.getCategories()).thenReturn(Arrays.asList(secondLevel9, secondLevel10));
		when(secondLevel10.getProducts()).thenReturn(products);
		//When
		Collection<CategoryModel> result = defaultWileyCategoryService.getSubcategoriesWithDescendantProducts(rootCategory2);
		//Then
		assertTrue(result.size() == 2);
		assertTrue(result.contains(firstLevel4));
		assertTrue(result.contains(firstLevel5));
	}

	private CategoryModel givenCategory(final CategoryModel... supercategories) {
		final CategoryModel categoryModel = mock(CategoryModel.class);
		when(categoryModel.getSupercategories()).thenReturn(Arrays.asList(supercategories));
		when(categoryModel.getCode()).thenReturn(UUID.randomUUID().toString());
		return categoryModel;
	}

	private ClassificationClassModel givenClassifyingCategory() {
		final ClassificationClassModel categoryModel = mock(ClassificationClassModel.class);
		when(categoryModel.getCode()).thenReturn(UUID.randomUUID().toString());
		return categoryModel;
	}
}