package com.wiley.core.billingfrequency.dao.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.subscriptionservices.model.BillingFrequencyModel;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;


/**
 * Default integration test for {@link WileyBillingFrequencyDaoImpl}.
 */
@IntegrationTest
public class WileyBillingFrequencyDaoImplIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{

	@Resource
	private WileyBillingFrequencyDaoImpl wileyBillingFrequencyDao;

	@Test
	public void testFindBillingFrequenciesByCodeWhenItemIsFound() throws Exception
	{
		// Given
		importCsv("/wileycore/test/subscription/billingFrequency.impex", DEFAULT_ENCODING);
		String billingFrequencyCode = "DAILY";

		// When
		List<BillingFrequencyModel> billingFrequencyModels = wileyBillingFrequencyDao.findBillingFrequenciesByCode(
				billingFrequencyCode);

		// Then
		assertNotNull(billingFrequencyModels);
		assertEquals(1, billingFrequencyModels.size());
		assertEquals(billingFrequencyCode, billingFrequencyModels.get(0).getCode());
	}

	@Test
	public void testFindBillingFrequenciesByCodeWhenItemIsNotFound() throws Exception
	{
		// Given
		// no changes in test data

		// When
		List<BillingFrequencyModel> billingFrequencyModels = wileyBillingFrequencyDao.findBillingFrequenciesByCode(
				"SomeBillingFrequencyCode");

		// Then
		assertNotNull(billingFrequencyModels);
		assertTrue(billingFrequencyModels.isEmpty());
	}
}