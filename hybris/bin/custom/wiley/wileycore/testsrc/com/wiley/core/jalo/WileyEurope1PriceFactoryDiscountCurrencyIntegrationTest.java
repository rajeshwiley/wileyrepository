package com.wiley.core.jalo;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import com.wiley.core.order.data.ProductDiscountParameter;
import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.europe1.enums.UserDiscountGroup;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.order.AbstractOrderEntry;
import de.hybris.platform.jalo.order.price.DiscountInformation;
import de.hybris.platform.jalo.order.price.JaloPriceFactoryException;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.util.DiscountValue;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;


/**
 * Contains test to check behavior when we need find only discounts related to currency.
 */
@IntegrationTest
public class WileyEurope1PriceFactoryDiscountCurrencyIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{

	public static final String USER_UID = "test@hybris.com";
	public static final String CART_CODE = "test-cart1";
	public static final String BASE_SITE_WEL = "wel";
	public static final String USD_CURRENCY = "USD";

	@Resource(name = "wileyPriceFactory")
	private WileyEurope1PriceFactory wileyEurope1PriceFactory;

	@Resource
	private CommerceCartService commerceCartService;

	@Resource
	private UserService userService;

	@Resource
	private ModelService modelService;

	@Resource
	private ProductService productService;

	@Resource
	private CommonI18NService commonI18NService;

	@Resource
	private BaseSiteService baseSiteService;


	@Before
	public void setUp() throws Exception
	{
		baseSiteService.setCurrentBaseSite(BASE_SITE_WEL, true);
	}

	@Test
	public void shouldReturnOnlyDiscountsRelatedToCurrencyAssignedToDiscount() throws ImpExException, JaloPriceFactoryException
	{
		// Given
		importCsv("/wileycore/test/product/WileyEurope1PriceFactoryDiscountCurrencyIntegrationTest/eurDiscount.impex",
				DEFAULT_ENCODING);
		importCsv("/wileycore/test/product/WileyEurope1PriceFactoryDiscountCurrencyIntegrationTest/usdDiscount.impex",
				DEFAULT_ENCODING);
		importCsv("/wileycore/test/product/WileyEurope1PriceFactoryDiscountCurrencyIntegrationTest/testUser.impex",
				DEFAULT_ENCODING);
		importCsv("/wileycore/test/product/WileyEurope1PriceFactoryDiscountCurrencyIntegrationTest/cartWithUSD.impex",
				DEFAULT_ENCODING);

		final UserModel user = userService.getUserForUID(USER_UID);
		final CartModel cart = commerceCartService.getCartForCodeAndUser(CART_CODE, user);

		// When
		final List<DiscountValue> discountValues = wileyEurope1PriceFactory.getDiscountValues(
				(AbstractOrderEntry) modelService.getSource(cart.getEntries().get(0)));

		// Then
		assertNotNull("Should return empty collection.", discountValues);
		final int expectedListSize = 1;
		final boolean isAbsolute = true;
		checkExpectedDiscount(discountValues, expectedListSize, 20.0, isAbsolute);
	}

	@Test
	public void shouldReturnOnlyDiscountsRelatedToCurrencyAssignedToDiscountRow() throws ImpExException, JaloPriceFactoryException
	{
		// Given
		importCsv("/wileycore/test/product/WileyEurope1PriceFactoryDiscountCurrencyIntegrationTest"
				+ "/eurDiscountWithCurrencyOndiscountRow.impex", DEFAULT_ENCODING);
		importCsv("/wileycore/test/product/WileyEurope1PriceFactoryDiscountCurrencyIntegrationTest"
				+ "/usdDiscountWithCurrencyOnDiscountRow.impex", DEFAULT_ENCODING);
		importCsv("/wileycore/test/product/WileyEurope1PriceFactoryDiscountCurrencyIntegrationTest/testUser.impex",
				DEFAULT_ENCODING);
		importCsv("/wileycore/test/product/WileyEurope1PriceFactoryDiscountCurrencyIntegrationTest/cartWithUSD.impex",
				DEFAULT_ENCODING);

		final UserModel user = userService.getUserForUID(USER_UID);
		final CartModel cart = commerceCartService.getCartForCodeAndUser(CART_CODE, user);

		// When
		final List<DiscountValue> discountValues = wileyEurope1PriceFactory.getDiscountValues(
				(AbstractOrderEntry) modelService.getSource(cart.getEntries().get(0)));

		// Then
		assertNotNull("Should return empty collection.", discountValues);
		final int expectedListSize = 1;
		final boolean isAbsolute = true;
		checkExpectedDiscount(discountValues, expectedListSize, 60.0, isAbsolute);
	}

	@Test
	public void testGetProductDiscountValues() throws ImpExException, JaloPriceFactoryException
	{
		// Given
		importCsv("/wileycore/test/product/WileyEurope1PriceFactoryDiscountCurrencyIntegrationTest/eurDiscount.impex",
				DEFAULT_ENCODING);
		importCsv("/wileycore/test/product/WileyEurope1PriceFactoryDiscountCurrencyIntegrationTest/usdDiscount.impex",
				DEFAULT_ENCODING);
		importCsv("/wileycore/test/product/WileyEurope1PriceFactoryDiscountCurrencyIntegrationTest/testUser.impex",
				DEFAULT_ENCODING);
		importCsv("/wileycore/test/product/WileyEurope1PriceFactoryDiscountCurrencyIntegrationTest/cartWithUSD.impex",
				DEFAULT_ENCODING);

		ProductModel product = productService.getProductForCode("WEL_CPA_PLATINUM_AUD_EBOOK");
		UserModel user = userService.getUserForUID(USER_UID);
		CurrencyModel currency = commonI18NService.getCurrency(USD_CURRENCY);
		final Date now = new Date();
		Boolean isNet = false;

		ProductDiscountParameter parameter = new ProductDiscountParameter();
		parameter.setProduct(product);
		parameter.setUserDiscountGroup(UserDiscountGroup.STUDENT);
		parameter.setUser(user);
		parameter.setCurrency(currency);
		parameter.setDate(now);
		parameter.setNet(isNet);
		parameter.setAdditionalFilter(Optional.empty());

		// When
		final List<DiscountValue> discountValues = wileyEurope1PriceFactory.getProductDiscountValues(parameter);

		// Then
		assertNotNull("Should return empty collection.", discountValues);
		final int expectedListSize = 1;
		final boolean isAbsolute = true;
		checkExpectedDiscount(discountValues, expectedListSize, 20.0, isAbsolute);
	}

	@Test
	public void testGetProductDiscountInformations() throws JaloPriceFactoryException, ImpExException
	{
		// Given
		importCsv("/wileycore/test/product/WileyEurope1PriceFactoryDiscountCurrencyIntegrationTest/eurDiscount.impex",
				DEFAULT_ENCODING);
		importCsv("/wileycore/test/product/WileyEurope1PriceFactoryDiscountCurrencyIntegrationTest/usdDiscount.impex",
				DEFAULT_ENCODING);

		ProductModel product = productService.getProductForCode("WEL_CPA_PLATINUM_AUD_EBOOK");
		final SessionContext sessionContext = JaloSession.getCurrentSession().getSessionContext();
		CurrencyModel currency = commonI18NService.getCurrency(USD_CURRENCY);
		sessionContext.setCurrency(modelService.getSource(currency));
		Date nowDate = new Date();
		boolean isNet = false;

		// When
		final List<DiscountInformation> productDiscountInformations = wileyEurope1PriceFactory.getProductDiscountInformations(
				sessionContext, modelService.getSource(product), nowDate, isNet);

		// Then
		final List<DiscountValue> discountValues = productDiscountInformations.stream()
				.map(DiscountInformation::getDiscountValue)
				.collect(Collectors.toList());

		final int expectedListSize = 1;
		final boolean isAbsolute = true;
		checkExpectedDiscount(discountValues, expectedListSize, 20.0, isAbsolute);
	}

	@Test
	public void shouldNotFilterRelativeDiscounts() throws ImpExException, JaloPriceFactoryException
	{
		// Given
		importCsv(
				"/wileycore/test/product/WileyEurope1PriceFactoryDiscountCurrencyIntegrationTest/relative20PercentDiscount.impex",
				DEFAULT_ENCODING);
		importCsv(
				"/wileycore/test/product/WileyEurope1PriceFactoryDiscountCurrencyIntegrationTest/relative50PercentDiscount.impex",
				DEFAULT_ENCODING);

		ProductModel product = productService.getProductForCode("WEL_CPA_PLATINUM_AUD_EBOOK");
		final SessionContext sessionContext = JaloSession.getCurrentSession().getSessionContext();
		CurrencyModel currency = commonI18NService.getCurrency(USD_CURRENCY);
		sessionContext.setCurrency(modelService.getSource(currency));
		Date nowDate = new Date();
		boolean isNet = false;

		// When
		final List<DiscountInformation> productDiscountInformations = wileyEurope1PriceFactory.getProductDiscountInformations(
				sessionContext, modelService.getSource(product), nowDate, isNet);

		// Then
		final List<DiscountValue> discountValues = productDiscountInformations.stream()
				.map(DiscountInformation::getDiscountValue)
				.collect(Collectors.toList());

		final int expectedListSize = 2;
		final boolean isAbsolute = false;
		checkExpectedDiscount(discountValues, expectedListSize, 20.0, isAbsolute);
		checkExpectedDiscount(discountValues, expectedListSize, 50.0, isAbsolute);
	}

	private void checkExpectedDiscount(final List<DiscountValue> discountValues, int listSize, final Double discountValue,
			boolean isAbsolute)
	{
		assertEquals("List should be expected size.", listSize, discountValues.size());
		assertTrue("List of discount values should contain expected discount.",
				containsDiscount(discountValues, discountValue, isAbsolute));
	}

	private boolean containsDiscount(final List<DiscountValue> discountValues, final Double value, boolean isAbsolute)
	{
		return discountValues.stream().anyMatch(discountValue ->
				value.equals(discountValue.getValue()) && isAbsolute == discountValue.isAbsolute()
		);
	}
}