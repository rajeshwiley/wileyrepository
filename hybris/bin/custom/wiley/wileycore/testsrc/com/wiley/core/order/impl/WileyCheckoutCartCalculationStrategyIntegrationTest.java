package com.wiley.core.order.impl;

import de.hybris.platform.commerceservices.order.CommerceCartCalculationStrategy;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import static org.junit.Assert.assertEquals;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Ignore;
import org.junit.Test;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.model.WileyProductModel;
import com.wiley.core.wiley.session.storage.WileyFailedCartModificationsStorageService;


/**
 * @author Dzmitryi_Halahayeu
 */
@Ignore
public abstract class WileyCheckoutCartCalculationStrategyIntegrationTest extends ServicelayerTransactionalTest
{
	private static final String USD_CURRENCY = "USD";
	private static final String TEST_GROUP = "testgroup";
	@Resource
	private ModelService modelService;
	@Resource
	private UserService userService;
	@Resource
	private CommonI18NService commonI18NService;
	@Resource
	private ProductService productService;
	@Resource
	private WileyFailedCartModificationsStorageService wileyFailedCartModificationsStorageService;

	@Test
	public void testWhenProductHasIncorrectUserGroupShouldRemoveCartEntryAndAddFailures()
	{
		final WileyProductModel product = (WileyProductModel) productService.getProductForCode(
				getRestrictedUserGroupProductCode());
		final CommerceCartParameter commerceCartParameter = createCartParameter(product);

		getCartCalculationStrategy().calculateCart(commerceCartParameter);

		assertEquals(commerceCartParameter.getCart().getEntries().size(), 0);
		final List<CommerceCartModification> modifications = wileyFailedCartModificationsStorageService.popAll();
		assertEquals(modifications.size(), 1);
		final CommerceCartModification commerceCartModification = modifications.get(0);
		assertEquals(commerceCartModification.getMessage(), WileyCoreConstants.BASKET_VALIDATION_UNVAILABLE);
		assertEquals(commerceCartModification.getMessageParameters().length, 1);
		assertEquals(commerceCartModification.getMessageParameters()[0], product.getName());
	}

	protected abstract CommerceCartCalculationStrategy getCartCalculationStrategy();

	protected abstract String getRestrictedUserGroupProductCode();

	@Test
	public void testWhenProductHasCorrectUserGroupShouldNotRemoveCartEntryAndDoNotHaveFailures()
	{
		final WileyProductModel product = (WileyProductModel) productService.getProductForCode(
				getRestrictedUserGroupProductCode());
		final UserModel currentUser = userService.getCurrentUser();
		currentUser.setGroups(Collections.singleton(userService.getUserGroupForUID(TEST_GROUP)));
		final CommerceCartParameter commerceCartParameter = createCartParameter(product);

		getCartCalculationStrategy().calculateCart(commerceCartParameter);

		assertEquals(commerceCartParameter.getCart().getEntries().size(), 1);
		final List<CommerceCartModification> modifications = wileyFailedCartModificationsStorageService.popAll();
		assertEquals(modifications.size(), 0);
	}

	protected CommerceCartParameter createCartParameter(final ProductModel product)
	{
		final UserModel user = userService.getCurrentUser();
		final CommerceCartParameter commerceCartParameter = new CommerceCartParameter();
		commerceCartParameter.setEnableHooks(true);
		commerceCartParameter.setProduct(product);
		final CartModel cart = new CartModel();
		cart.setUser(user);
		cart.setCurrency(commonI18NService.getCurrency(USD_CURRENCY));
		cart.setDate(new Date());
		cart.setNet(Boolean.TRUE);
		final CartEntryModel cartEntryModel = new CartEntryModel();
		cartEntryModel.setBasePrice(44.0);
		cartEntryModel.setQuantity(1L);
		cartEntryModel.setProduct(product);
		cartEntryModel.setOrder(cart);
		cartEntryModel.setUnit(product.getUnit());
		modelService.save(cartEntryModel);
		cart.setEntries(Collections.singletonList(cartEntryModel));
		modelService.save(cart);
		commerceCartParameter.setCart(cart);
		commerceCartParameter.setQuantity(1);
		commerceCartParameter.setUser(user);
		return commerceCartParameter;
	}


}
