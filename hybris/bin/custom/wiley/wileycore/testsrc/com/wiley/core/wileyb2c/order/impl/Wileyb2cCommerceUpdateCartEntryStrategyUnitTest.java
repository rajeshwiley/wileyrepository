package com.wiley.core.wileyb2c.order.impl;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;


/**
 * Created by Georgii_Gavrysh on 7/26/2016.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class Wileyb2cCommerceUpdateCartEntryStrategyUnitTest
{
	@InjectMocks
	Wileyb2cCommerceUpdateCartEntryStrategy wileyb2cCommerceUpdateCartEntryStrategy;

	@Test
	public void getAllowedCartAdjustmentForProductTest()
	{
		final long allowedCartAdjustmentForProduct = wileyb2cCommerceUpdateCartEntryStrategy.getAllowedCartAdjustmentForProduct(
				null,
				null, 2, null);
		assertThat(allowedCartAdjustmentForProduct, is(2L));
	}
}
