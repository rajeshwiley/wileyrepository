package com.wiley.core.impex;

import com.wiley.core.AbstractWileyServicelayerTransactionalTest;
import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.jalo.type.TypeManager;
import de.hybris.platform.jalo.user.Customer;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.servicelayer.user.impl.DefaulPasswordEncoderService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * Created by Maksim_Kozich on 17.02.2016.
 *
 * The purpose of this test is to cover all edge cases when importing customers via hmc after go-live.
 */
@IntegrationTest
public class ImportCustomersIntegrationTest extends AbstractWileyServicelayerTransactionalTest
{
	public static final String SUPPORTED_IMPEX_ENCODING = "utf-8";
	public static final String PASSWORD_ENCODING = "pbkdf2";
	public static final String CUSTOMER_1_UID = "CusTomeR1@gmail.com";
	public static final String CUSTOMER_2_UID = "CusTomeR2@gmail.com";
	public static final String CUSTOMER_3_UID = "CusTomeR3@gmail.com";
	public static final String PASSWORD_1_OLD = "AGS password1";
	public static final String PASSWORD_2_OLD = "AGS password2";
	public static final String PASSWORD_3_NEW = "WEL password3";
	public static final String CUSTOMER_1_FIRST_NAME = "AGS Customer 1 First Name";
	public static final String CUSTOMER_2_FIRST_NAME_NEW = "WEL Customer 2 First Name";
	public static final String CUSTOMER_3_FIRST_NAME = "WEL Customer 3 First Name";
	public static final String CUSTOMER_1_LAST_NAME = "AGS Customer 1 Last Name";
	public static final String CUSTOMER_2_LAST_NAME_NEW = "WEL Customer 2 Last Name";
	public static final String CUSTOMER_3_LAST_NAME = "WEL Customer 3 Last Name";
	public static final String CUSTOMER_1_SHIPPING_STREET_NAME = "AGS Shipping 1 Street Name";
	public static final String CUSTOMER_2_SHIPPING_STREET_NAME_OLD = "AGS Shipping 2 Street Name";
	public static final String CUSTOMER_2_SHIPPING_STREET_NAME_NEW = "WEL Shipping 2 Street Name";
	public static final String CUSTOMER_3_SHIPPING_STREET_NAME = "WEL Shipping 3 Street Name";
	public static final String CUSTOMER_1_BILLING_STREET_NAME = "AGS Billing 1 Street Name";
	public static final String CUSTOMER_2_BILLING_STREET_NAME_OLD = "AGS Billing 2 Street Name";
	public static final String CUSTOMER_2_BILLING_STREET_NAME_NEW = "WEL Billing 2 Street Name";
	public static final String CUSTOMER_3_BILLING_STREET_NAME = "WEL Billing 3 Street Name";
	public static final String GROUP_OLD = "testgroup";
	public static final String GROUP_NEW = "customergroup";

	@Resource
	private UserService userService;

	@Resource
	private DefaulPasswordEncoderService passwordEncoderService;

	@Before
	public void setUp() throws Exception
	{
		//Prevent default setting of customer usergroup
		ComposedType customerT = TypeManager.getInstance().getComposedType(Customer.class);
		customerT.getAttributeDescriptor("groups").setDefaultValue(new LinkedHashSet());

		importCsv("/wileycore/test/customer/ags_existing_customers.impex", SUPPORTED_IMPEX_ENCODING);
		importCsv("/wileycore/test/customer/wel_new_customers.impex", SUPPORTED_IMPEX_ENCODING);
	}

	/**
	 * If there is no existing customers in the system yet - all customers should be imported.
	 * For existing customers:
	 * 1. All provided fields should be updated. Passwords MUST NOT be changed.
	 * 2. New addresses should be added. Default billing and shipping addresses MUST be changed.
	 * 3. Previous customer groups should be valid. New 'customergroup' should be added.
	 * 4. Addresses and groups should be added even for the customers with Capital letters in email despite hybis OOTB bug
	 * (hyris saves Customer.uid in lowercase so fails to load references to just loaded customers).
	 */
	@Test
	public void testAllCustomersSuccessfullyImported() throws Exception
	{
		assertCustomer(CUSTOMER_1_UID, PASSWORD_1_OLD, CUSTOMER_1_FIRST_NAME, CUSTOMER_1_LAST_NAME,
				CUSTOMER_1_SHIPPING_STREET_NAME, CUSTOMER_1_BILLING_STREET_NAME,
				Arrays.asList(CUSTOMER_1_SHIPPING_STREET_NAME, CUSTOMER_1_BILLING_STREET_NAME),
				Arrays.asList(GROUP_OLD));
		assertCustomer(CUSTOMER_2_UID, PASSWORD_2_OLD, CUSTOMER_2_FIRST_NAME_NEW, CUSTOMER_2_LAST_NAME_NEW,
				CUSTOMER_2_SHIPPING_STREET_NAME_NEW, CUSTOMER_2_BILLING_STREET_NAME_NEW,
				Arrays.asList(CUSTOMER_2_SHIPPING_STREET_NAME_OLD, CUSTOMER_2_BILLING_STREET_NAME_OLD,
						CUSTOMER_2_SHIPPING_STREET_NAME_NEW, CUSTOMER_2_BILLING_STREET_NAME_NEW),
				Arrays.asList(GROUP_OLD, GROUP_NEW));
		assertCustomer(CUSTOMER_3_UID, PASSWORD_3_NEW, CUSTOMER_3_FIRST_NAME, CUSTOMER_3_LAST_NAME,
				CUSTOMER_3_SHIPPING_STREET_NAME, CUSTOMER_3_BILLING_STREET_NAME,
				Arrays.asList(CUSTOMER_3_SHIPPING_STREET_NAME, CUSTOMER_3_BILLING_STREET_NAME),
				Arrays.asList(GROUP_NEW));
	}

	private void assertCustomer(final String originalUid, final String plainTextPassword, final String firstName,
			final String lastName, final String shippingStreetName, final String billingStreetName,
			final List<String> streetNames, final List<String> groups)
	{
		UserModel user = userService.getUserForUID(originalUid.toLowerCase());
		Assert.assertNotNull("Imported customer not found", user);
		Assert.assertTrue("Imported customer not of type CustomerModel", user instanceof CustomerModel);
		CustomerModel customer = (CustomerModel) user;
		Assert.assertEquals("Wrong customer originalUid", originalUid.toLowerCase(), customer.getOriginalUid());
		Assert.assertEquals("Wrong customer password encoding", PASSWORD_ENCODING, customer.getPasswordEncoding());
		Assert.assertTrue("Wrong customer password", passwordEncoderService.isValid(user, plainTextPassword));
		Assert.assertEquals("Wrong customer first name", firstName, customer.getFirstName());
		Assert.assertEquals("Wrong customer last name", lastName, customer.getLastName());
		Set<String> customerGroupsUids = customer.getGroups().stream().map(p -> p.getUid()).collect(Collectors.toSet());
		Set<String> expectedGroupsUids = new HashSet<>(groups);
		Assert.assertEquals("Wrong customer groups", expectedGroupsUids, customerGroupsUids);
	}

}
