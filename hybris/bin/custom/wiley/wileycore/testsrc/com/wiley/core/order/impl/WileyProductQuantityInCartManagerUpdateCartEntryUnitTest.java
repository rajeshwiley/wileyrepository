package com.wiley.core.order.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceUpdateCartEntryStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.wiley.core.enums.ProductEditionFormat;
import com.wiley.core.product.WileyCountableProductService;
import com.wiley.core.product.WileyProductEditionFormatService;

import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;


/**
 * Unit test for {@link WileyProductQuantityInCartManagerImpl}.
 *
 * @author Aliaksei_Zlobich
 */
@UnitTest
public class WileyProductQuantityInCartManagerUpdateCartEntryUnitTest
{

	@Mock
	private CommerceUpdateCartEntryStrategy commerceUpdateCartEntryStrategyMock;

	@Mock
	private WileyProductEditionFormatService wileyProductEditionFormatServiceMock;

	@Mock
	private WileyCountableProductService wileyCountableProductServiceMock;
	@InjectMocks
	private WileyProductQuantityInCartManagerImpl updateCartEntryStrategyDecorator;

	// Test data
	@Mock
	private CartModel cartMock;

	@Mock
	private AbstractOrderEntryModel orderEntryMock;
	private final long orderEntryNumber = 1234;

	@Mock
	private ProductModel productMock;



	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);

		// Setup test data
		when(orderEntryMock.getEntryNumber()).thenReturn((int) orderEntryNumber);
		when(orderEntryMock.getProduct()).thenReturn(productMock);
		when(cartMock.getEntries()).thenReturn(Arrays.asList(orderEntryMock));

		// Setup mock services
		when(wileyCountableProductServiceMock.canProductHaveQuantity(any(ProductModel.class))).thenAnswer(
				invocation -> {
					final ProductModel product = (ProductModel) invocation.getArguments()[0];
					return product.getEditionFormat() == ProductEditionFormat.PHYSICAL;
				});
	}

	@Test
	public void testUpdateQuantityForCartEntryCase1() throws CommerceCartModificationException
	{
		testAddingWithDifferentQuantityAndEditionFormat(1234, ProductEditionFormat.PHYSICAL);
	}

	@Test
	public void testUpdateQuantityForCartEntryCase2() throws CommerceCartModificationException
	{
		testAddingWithDifferentQuantityAndEditionFormat(1, ProductEditionFormat.DIGITAL);
	}

	@Test
	public void testUpdateQuantityForCartEntryCase3() throws CommerceCartModificationException
	{
		testAddingWithDifferentQuantityAndEditionFormat(1, ProductEditionFormat.DIGITAL_AND_PHYSICAL);
	}

	@Test
	public void testUpdateQuantityForCartEntryCase4() throws CommerceCartModificationException
	{
		testThrowingExceptionWithDifferentQuantityAndEditionFormat(1234, ProductEditionFormat.DIGITAL);
	}

	@Test
	public void testUpdateQuantityForCartEntryCase5() throws CommerceCartModificationException
	{
		testThrowingExceptionWithDifferentQuantityAndEditionFormat(1234, ProductEditionFormat.DIGITAL_AND_PHYSICAL);

	}

	@Test
	public void testUpdateToShippingModeForCartEntry() throws CommerceCartModificationException
	{
		// Given
		CommerceCartParameter parameterMock = mock(CommerceCartParameter.class);

		// When
		this.updateCartEntryStrategyDecorator.updateToShippingModeForCartEntry(parameterMock);

		// Then
		// Method should just delegate to commerceUpdateCartEntryStrategy
		verify(commerceUpdateCartEntryStrategyMock).updateToShippingModeForCartEntry(eq(parameterMock));
		verifyNoMoreInteractions(commerceUpdateCartEntryStrategyMock);
	}

	@Test
	public void testUpdatePointOfServiceForCartEntry() throws CommerceCartModificationException
	{
		// Given
		CommerceCartParameter parameterMock = mock(CommerceCartParameter.class);

		// When
		this.updateCartEntryStrategyDecorator.updatePointOfServiceForCartEntry(parameterMock);

		// Then
		// Method should just delegate to commerceUpdateCartEntryStrategy
		verify(commerceUpdateCartEntryStrategyMock).updatePointOfServiceForCartEntry(eq(parameterMock));
		verifyNoMoreInteractions(commerceUpdateCartEntryStrategyMock);
	}

	private void testAddingWithDifferentQuantityAndEditionFormat(final long quantity, final ProductEditionFormat editionFormat)
			throws CommerceCartModificationException
	{
		// Given
		CommerceCartParameter parameter = new CommerceCartParameter();
		parameter.setEntryNumber(orderEntryNumber);
		parameter.setCart(cartMock);
		parameter.setQuantity(quantity);

		when(productMock.getEditionFormat()).thenReturn(editionFormat);

		// When
		this.updateCartEntryStrategyDecorator.updateQuantityForCartEntry(parameter);

		// Then
		// Method should just delegate to commerceUpdateCartEntryStrategy
		verify(commerceUpdateCartEntryStrategyMock).updateQuantityForCartEntry(eq(parameter));
		verify(wileyCountableProductServiceMock).canProductHaveQuantity(eq(productMock));
		verifyNoMoreInteractions(commerceUpdateCartEntryStrategyMock, wileyProductEditionFormatServiceMock);
	}

	private void testThrowingExceptionWithDifferentQuantityAndEditionFormat(final long quantity,
			final ProductEditionFormat editionFormat)
	{
		// Given
		CommerceCartParameter parameter = new CommerceCartParameter();
		parameter.setEntryNumber(orderEntryNumber);
		parameter.setCart(cartMock);
		parameter.setQuantity(quantity);

		when(productMock.getEditionFormat()).thenReturn(editionFormat);

		// When
		try
		{
			this.updateCartEntryStrategyDecorator.updateQuantityForCartEntry(parameter);
			fail("Expected CartModificationException.");
		}
		catch (CommerceCartModificationException e)
		{
			// Then
			// Success
			verify(wileyCountableProductServiceMock).canProductHaveQuantity(eq(productMock));
			verifyNoMoreInteractions(wileyProductEditionFormatServiceMock);
			verifyZeroInteractions(commerceUpdateCartEntryStrategyMock);
		}
	}
}