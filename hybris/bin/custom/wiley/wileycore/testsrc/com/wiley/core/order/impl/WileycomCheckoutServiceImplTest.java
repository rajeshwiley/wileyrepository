package com.wiley.core.order.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.user.UserService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wiley.core.wileycom.order.dao.WileycomOrderDao;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WileycomCheckoutServiceImplTest
{
	private static final String USER_UID = "user-uid";

	@InjectMocks
	private WileycomCheckoutServiceImpl wileycomCheckoutService;

	@Mock
	private UserService userServiceMock;

	@Mock
	private WileycomOrderDao wileycomOrderDaoMock;

	@Mock
	private CustomerModel customerModelMock;

	@Mock
	private OrderModel orderModelMock;

	@Test
	public void shouldGetLastOrderForCustomer()
	{
		// Given
		when(userServiceMock.getUserForUID(USER_UID)).thenReturn(customerModelMock);
		when(wileycomOrderDaoMock.getLastOrderForCustomer(customerModelMock)).thenReturn(Optional.of(orderModelMock));

		// When
		final Optional<OrderModel> result = wileycomCheckoutService.getLastOrderForCustomer(USER_UID);

		// Then
		assertEquals("The given order model should have been returned!", result, Optional.of(orderModelMock));
		verify(wileycomOrderDaoMock).getLastOrderForCustomer(customerModelMock);
	}

	@Test (expected = UnknownIdentifierException.class)
	public void shouldNotGetLastOrderForCustomerForUnknownCustomer()
	{
		// Given
		when(userServiceMock.getUserForUID(USER_UID)).thenThrow(UnknownIdentifierException.class);

		// When
		wileycomCheckoutService.getLastOrderForCustomer(USER_UID);

		// Then
		fail("Execution should have interrupted with exception!");
	}
}
