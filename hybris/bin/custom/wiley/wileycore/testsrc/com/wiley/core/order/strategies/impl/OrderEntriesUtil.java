package com.wiley.core.order.strategies.impl;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Set;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;

public final class OrderEntriesUtil
{
	private OrderEntriesUtil()
	{

	}

	public static void createOrderEntriesWithStatus(final OrderModel order, final Set<OrderStatus> statuses)
	{
		when(order.getEntries()).thenReturn(new ArrayList<>());
		statuses.forEach(status -> {
			OrderEntryModel entry = mock(OrderEntryModel.class);
			when(entry.getStatus()).thenReturn(status);
			order.getEntries().add(entry);
		});
	}
}
