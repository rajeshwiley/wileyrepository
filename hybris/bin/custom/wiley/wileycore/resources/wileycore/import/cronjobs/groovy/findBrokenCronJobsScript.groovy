package com.wiley.core.wileyb2c.search.solrfacetsearch.indexer.cron

import de.hybris.platform.cronjob.model.CronJobModel
import de.hybris.platform.servicelayer.cronjob.CronJobService
import org.apache.commons.collections4.CollectionUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory

Logger LOG = LoggerFactory.getLogger(CronJobService.class)
List<CronJobModel> cronJobs = cronJobService.getRunningOrRestartedCronJobs()
Set<Thread> threads = Thread.getAllStackTraces().keySet()

Set<String> threadNames = new HashSet<>()
for (Thread t : threads) {
    threadNames.add(t.getName())
}

List<String> brokenJobCodes = new ArrayList<>()

for (CronJobModel cj : cronJobs) {
    boolean hasThread = false
    for (String threadName : threadNames) {
        if (threadName.contains(cj.getCode())) {
            hasThread = true
            break
        }
    }
    if (!hasThread) {
        LOG.error("Potentially Broken CronJob with CODE: {}", cj.getCode())
        brokenJobCodes.add(cj.getCode())
    }
}

if (CollectionUtils.isNotEmpty(brokenJobCodes)) {
    String jobs = String.join(", ", brokenJobCodes)
    throw new IllegalStateException("There are not finished jobs found: [" + jobs + "]")
}
else {
    LOG.info("No broken jobs found, Good Job!")
}
