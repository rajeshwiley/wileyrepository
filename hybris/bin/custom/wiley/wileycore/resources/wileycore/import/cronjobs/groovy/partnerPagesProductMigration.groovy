import com.wiley.core.category.WileyCategoryService
import com.wiley.core.model.jobs.PartnerPagesProductMigrationCronJobModel
import de.hybris.platform.category.model.CategoryModel
import de.hybris.platform.core.model.product.ProductModel
import de.hybris.platform.servicelayer.model.ModelService

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage

PartnerPagesProductMigrationCronJobModel productMigrationCronJobModel = (PartnerPagesProductMigrationCronJobModel) cronjob;
ProductModel oldProduct = (ProductModel) productMigrationCronJobModel.oldProduct;
ProductModel newProduct = (ProductModel) productMigrationCronJobModel.newProduct;

validateParameterNotNullStandardMessage('oldProduct', oldProduct);
validateParameterNotNullStandardMessage('newProduct', newProduct);

log.info('Old product code = ' + oldProduct.getCode());
log.info('New product code = ' + newProduct.getCode());

WileyCategoryService categoryService = (WileyCategoryService) spring.getBean('categoryService');
ModelService modelService = (ModelService) spring.getBean('modelService');

for (CategoryModel category : oldProduct.getSupercategories()) {
    boolean isPartnerCategory = category.getSupercategories().stream().anyMatch { c -> categoryService.isPartnersCategory(c) };
    if (isPartnerCategory) {
        List<ProductModel> products = new ArrayList(category.getProducts());
        products.remove(oldProduct);
        log.info('Category with code=' + category.code + ' was removed from a product with code=' + oldProduct.code);
        products.add(newProduct);
        log.info('Category with code=' + category.code + ' was added to a product with code=' + newProduct.code);
        category.setProducts(products);
        modelService.save(category);
    }
}
productMigrationCronJobModel.oldProduct = null;
productMigrationCronJobModel.newProduct = null;

modelService.save(productMigrationCronJobModel);
log.info('Partner Pages Product Migration Cron Job has finished.');