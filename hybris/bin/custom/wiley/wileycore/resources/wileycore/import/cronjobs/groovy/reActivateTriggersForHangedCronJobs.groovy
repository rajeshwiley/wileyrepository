import de.hybris.platform.core.GenericCondition
import de.hybris.platform.core.GenericQuery
import de.hybris.platform.cronjob.enums.CronJobStatus
import de.hybris.platform.cronjob.model.TriggerModel
import de.hybris.platform.servicelayer.cronjob.CronJobService
import org.slf4j.LoggerFactory

ACTIVE_ATTRIBUTE = "active"
LOG = LoggerFactory.getLogger(CronJobService.class)

GenericQuery query = new GenericQuery("Trigger")
query.addConditions(GenericCondition.equals("active", Boolean.TRUE))
List<TriggerModel> activeTriggers = genericSearchService.search(query).getResult()
final Date now = new Date()
filteredTriggers = activeTriggers
        .findAll({ validateTriggerCronJob(it) })
        .findAll({it.getActivationTime() != null && it.getActivationTime().before(now)}
)

toggleTriggers(filteredTriggers)

private boolean validateTriggerCronJob(TriggerModel it) {
    it.getCronJob() != null &&
    it.getCronJob().getActive() &&
    it.getCronJob().getStatus() != null &&
    it.getCronJob().getStatus() != CronJobStatus.RUNNING &&
    it.getCronJob().getStatus() != CronJobStatus.RUNNINGRESTART
}

private void toggleTriggers(Collection<TriggerModel> triggers) {
    triggers
            .each { setActiveItem(it, false) }
            .each { setActiveItem(it, true) }
}


private boolean setActiveItem(final TriggerModel item, final boolean isActive)
{
    modelService.setAttributeValue(item, ACTIVE_ATTRIBUTE, isActive)
    modelService.save(item)

    cronJobCode = item.getCronJob() != null ? "[" + item.getCronJob().getCode() + "]" : "[N/A]"
    jobCode = item.getJob() != null ? "[" + item.getJob().getCode() + "]" : "[N/A]"
    LOG.info("Trigger {} for CronJob {} or Job {} was set to active={}", item, cronJobCode, jobCode, isActive)
    return true
}