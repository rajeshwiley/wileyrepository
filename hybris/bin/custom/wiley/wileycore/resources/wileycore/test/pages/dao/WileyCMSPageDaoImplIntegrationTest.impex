#
# Import the CMS content for the Wileycom site
#
$contentCatalog = wileyb2cContentCatalog
$contentCatalogName = wileyb2c Content Catalog
$contentCV = catalogVersion(CatalogVersion.catalog(Catalog.id[default = $contentCatalog]), CatalogVersion.version[default = Online])[default = $contentCatalog:Online]
$productCatalog = wileyProductCatalog
$productCatalogName = WILEYCOM Product Catalog
$productCV = catalogVersion(catalog(id[default = $productCatalog]), version[default = 'Online'])[unique = true, default = $productCatalog:Online]
$picture = media(code, $contentCV);
$image = image(code, $contentCV);
$media = media(code, $contentCV);
$page = page(uid, $contentCV);
$contentPage = contentPage(uid, $contentCV);
$product = product(code, $productCV)
$category = category(code, $productCV)
$siteResource = jar:com.wiley.wileycom.initialdata.constants.WileycomInitialDataConstants&/wileycominitialdata/import/coredata/contentCatalogs/$contentCatalog
$lang = en

# Import modulegen config properties into impex macros
UPDATE GenericItem[processor = de.hybris.platform.commerceservices.impex.impl.ConfigPropertyImportProcessor]; pk[unique = true]
$wileycoreJarResourceCms = $config-jarResourceCmsValue


# Create PageTemplates
# These define the layout for pages
# "FrontendTemplateName" is used to define the JSP that should be used to render the page for pages with multiple possible layouts.
# "RestrictedPageTypes" is used to restrict templates to page types
INSERT_UPDATE PageTemplate; $contentCV[unique = true]; uid[unique = true]; name; frontendTemplateName; restrictedPageTypes(code); active[default = true]
; ; SearchResultsEmptyPageTemplate       ; Search Results Empty Page Template        ; search/searchEmptyPage                                                           ; ContentPage

# Add Velocity templates that are in the CMS Cockpit. These give a better layout for editing pages
# The FileLoaderValueTranslator loads a File into a String property. The templates could also be inserted in-line in this file.

UPDATE PageTemplate; $contentCV[unique = true]; uid[unique = true]; velocityTemplate[translator = de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]
; ; SearchResultsEmptyPageTemplate       ; $wileycoreJarResourceCms/structure-view/wileycom/structure_searchResultsEmptyPageTemplate.vm

# Create ContentSlotNames
# Each PageTemplate has a number of ContentSlotNames, with a list of valid components for the slot.
# There are a standard set of slots and a number of specific slots for each template.
# Standard slots are SiteLogo, HeaderLinks, MiniCart and NavigationBar (that all appear in the Header), and the Footer.

# Search Results Empty Page Template
INSERT_UPDATE ContentSlotName; name[unique = true]; template(uid, $contentCV)[unique = true][default = 'SearchResultsEmptyPageTemplate']; validComponentTypes(code); compTypeGroup(code)
; TopContent             ; ; ;
; BottomContent          ; ; ;

# Create Content Slots
INSERT_UPDATE ContentSlot; $contentCV[unique = true]; uid[unique = true]; name; active
; ; TopContentSlot                    ; Top Content                                              ; true
; ; BottomContentSlot                 ; Bottom Content                                           ; true

INSERT_UPDATE ContentSlotForTemplate; $contentCV[unique = true]; uid[unique = true]; position[unique = true]; pageTemplate(uid, $contentCV)[unique = true][default = 'SearchResultsEmptyPageTemplate']; contentSlot(uid, $contentCV)[unique = true]; allowOverwrite
; ; TopContent-SearchResultsEmpty             ; TopContent             ; ; TopContentSlot                      ; true
; ; BottomContent-SearchResultsEmpty          ; BottomContent          ; ; BottomContentSlot                   ; true

# Functional Content Pages
INSERT_UPDATE ContentPage; $contentCV[unique = true]; uid[unique = true]; name; masterTemplate(uid, $contentCV); label; defaultPage[default = 'true']; approvalStatus(code)[default = 'approved']; homepage[default = 'false']
; ; searchEmpty ; Search Results Empty Page ; SearchResultsEmptyPageTemplate ; searchEmpty

# Preview Image for use in the CMS Cockpit for special ContentPages
INSERT_UPDATE Media; $contentCV[unique = true]; code[unique = true]; mime; realfilename; folder(qualifier)[default = 'images']
; ; searchResultsEmptyPagePreview ; text/png ; SearchResultsEmptyPage.png ;

# Functional Content Pages
UPDATE ContentPage; $contentCV[unique = true]; uid[unique = true]; previewImage(code, $contentCV)
; ; searchEmpty ; searchResultsEmptyPagePreview

# Functional Content Pagesrobotsmetatag(code)
INSERT_UPDATE ContentPage; $contentCV[unique = true]; uid[unique = true]; name; masterTemplate(uid, $contentCV); label; robotsmetatag(code)[default = 'UNDEFINED']; defaultPage[default = 'true']; approvalStatus(code)[default = 'approved']; homepage[default = 'false']
; ; searchEmpty                         ; Search Results Empty Page                ; SearchResultsEmptyPageTemplate       ; searchEmpty                       ; NOINDEX_NOFOLLOW


###### Search Results Empty Page ######

INSERT_UPDATE ContentSlot; $contentCV[unique = true]; uid[unique = true]; name; active; cmsComponents(&componentRef)
; ; TopContent-searchEmpty    ; Top Content for Search Results Empty page    ; true ;
; ; BottomContent-searchEmpty ; Bottom Content for Search Results Empty page ; true ;

INSERT_UPDATE ContentSlotForPage; $contentCV[unique = true]; uid[unique = true]; position[unique = true]; page(uid, $contentCV)[unique = true][default = 'searchEmpty']; contentSlot(uid, $contentCV)[unique = true]
; ; Top-searchEmpty    ; TopContent    ; ; TopContent-searchEmpty
; ; Bottom-searchEmpty ; BottomContent ; ; BottomContent-searchEmpty

INSERT_UPDATE CMSParagraphComponent; $contentCV[unique = true]; uid[unique = true]; name; &componentRef
; ; testTopParagraphComponent    ; No Search Results Top Paragraph Component    ; testTopParagraphComponent

UPDATE ContentSlot; $contentCV[unique = true]; uid[unique = true]; cmsComponents(&componentRef)
; ; TopContent-searchEmpty    ; testTopParagraphComponent

INSERT_UPDATE CMSLinkComponent; $contentCV[unique = true]; uid[unique = true]; name; url
; ; testlink ; Test Nav Link  ; /test