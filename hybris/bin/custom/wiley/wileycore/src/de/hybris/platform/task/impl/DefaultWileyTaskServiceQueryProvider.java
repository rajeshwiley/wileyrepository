package de.hybris.platform.task.impl;

import de.hybris.platform.core.PK;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.processengine.model.ProcessTaskModel;
import de.hybris.platform.task.constants.TaskConstants;
import de.hybris.platform.util.Config;

import java.time.Duration;
import java.time.Instant;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class DefaultWileyTaskServiceQueryProvider extends DefaultTaskServiceQueryProvider
{
    private static final String TYPE_SYSTEM = "db.type.system.name";

    public String getValidTasksToExecuteQuery(final Collection<String> myGroups, final boolean processTriggerTasks,
                                              final boolean isNodeExclusiveModeEnabled, final boolean shouldRunFullQuery)
    {
        return buildQuery(processTriggerTasks, myGroups);
    }

    public Map<String, Object> setQueryParameters(final Collection<String> myGroups, final boolean processTriggerTasks,
                              final Integer nodeId, final Integer noNode,  final Set<PK> pksOfTriggerTaskTypeAndSubtypes,
                              final Duration fullQueryTaskExecutionTimeThreshold)
    {
        Map<String, Object> parameters = new HashMap(5);
        long now = System.currentTimeMillis();
        parameters.put("now", now);
        if (!fullQueryTaskExecutionTimeThreshold.isNegative() && !fullQueryTaskExecutionTimeThreshold.isZero())
        {
            parameters.put("executionTimeThreshold", Instant.ofEpochMilli(now)
                    .minus(fullQueryTaskExecutionTimeThreshold).toEpochMilli());
        }
        else
        {
            parameters.put("executionTimeThreshold", 0);
        }

        parameters.put("false", Boolean.FALSE);
        parameters.put("nodeId", nodeId);
        parameters.put("noNode", noNode);
        if (!myGroups.isEmpty())
        {
            parameters.put("myGroups", myGroups);
        }
        if (!processTriggerTasks)
        {
            parameters.put("excludedTypes", pksOfTriggerTaskTypeAndSubtypes);
        }
        parameters.put("typeSystem", Config.getParameter(TYPE_SYSTEM));

        return parameters;
    }

    private String buildQuery(final boolean processTriggerTasks, final Collection<String> myGroups)
    {
        return "SELECT {" + Item.PK + "}, hjmpTS FROM {" + TaskConstants.TC.TASK + " AS t} WHERE {"
                + "failed} = ?false " + (!processTriggerTasks ? " AND {itemtype} NOT IN (?excludedTypes) " : "")
                + "AND (({expirationTimeMillis} < ?now) OR ({executionTimeMillis} <= ?now "
                + "AND ({nodeId} = ?nodeId OR {nodeId} IS NULL) AND " + (myGroups.isEmpty() ?
                "{nodeGroup} IS NULL " : "({nodeGroup} IN (?myGroups) OR {nodeGroup} IS NULL) ")
                + "AND {runningOnClusterNode} = ?noNode AND NOT EXISTS ({{SELECT {" + Item.PK + "} FROM {"
                + TaskConstants.TC.TASKCONDITION + "} WHERE {task}={t." + Item.PK + "}"
                + " AND ({fulfilled} = ?false OR {fulfilled} IS NULL)}})))"
                + " AND {t." + Item.PK + "} not in ({{SELECT {pt:"
                + Item.PK + "} from {ProcessTask as pt left join BusinessProcess as bp on {pt:" + ProcessTaskModel.PROCESS
                + "} = {bp:" + Item.PK + "}} where {bp:" + BusinessProcessModel.TYPESYSTEM + "} <> ?typeSystem and {bp:"
                + BusinessProcessModel.TYPESYSTEM + "} is not null AND {"
                + "failed" + "} = ?false " + (!processTriggerTasks ? " AND {itemtype} NOT IN (?excludedTypes) " : "")
                + "AND (({expirationTimeMillis} < ?now) OR ({executionTimeMillis} <= ?now "
                + "AND ({nodeId} = ?nodeId OR {nodeId} IS NULL) AND " + (myGroups.isEmpty() ?
                "{nodeGroup} IS NULL " : "({nodeGroup} IN (?myGroups) OR {nodeGroup} IS NULL) ")
                + "AND {runningOnClusterNode} = ?noNode AND NOT EXISTS ({{SELECT {" + Item.PK + "} FROM {"
                + TaskConstants.TC.TASKCONDITION + "} WHERE {task}={pt." + Item.PK + "} "
                + "AND ({fulfilled} = ?false OR {fulfilled} IS NULL)}})))}})";
    }
}