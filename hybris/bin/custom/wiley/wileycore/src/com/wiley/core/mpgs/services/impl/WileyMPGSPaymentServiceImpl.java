package com.wiley.core.mpgs.services.impl;

import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Objects;
import java.util.Optional;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.model.WileyTnsMerchantModel;
import com.wiley.core.mpgs.constants.WileyMPGSConstants;
import com.wiley.core.mpgs.request.WileyAuthorizationRequest;
import com.wiley.core.mpgs.request.WileyCaptureRequest;
import com.wiley.core.mpgs.request.WileyRetrieveSessionRequest;
import com.wiley.core.mpgs.request.WileyTokenizationRequest;
import com.wiley.core.mpgs.response.WileyAuthorizationResponse;
import com.wiley.core.mpgs.response.WileyCaptureResponse;
import com.wiley.core.mpgs.response.WileyFollowOnRefundResponse;
import com.wiley.core.mpgs.response.WileyRetrieveSessionResponse;
import com.wiley.core.mpgs.response.WileyTokenizationResponse;
import com.wiley.core.mpgs.services.WileyCardPaymentService;
import com.wiley.core.mpgs.services.WileyMPGSMerchantService;
import com.wiley.core.mpgs.services.WileyMPGSPaymentEntryService;
import com.wiley.core.mpgs.services.WileyMPGSPaymentService;
import com.wiley.core.mpgs.services.WileyUrlGenerationService;
import com.wiley.core.mpgs.services.strategies.CardVerificationStrategy;
import com.wiley.core.mpgs.strategies.WileyMPGSTransactionIdGeneratorStrategy;
import com.wiley.core.payment.request.WileyFollowOnRefundRequest;
import com.wiley.core.payment.strategies.AmountToPayCalculationStrategy;
import com.wiley.core.payment.transaction.PaymentTransactionService;




public class WileyMPGSPaymentServiceImpl implements WileyMPGSPaymentService, ApplicationContextAware
{

	private static final Logger LOG = LogManager.getLogger(WileyMPGSPaymentServiceImpl.class);

	private ApplicationContext applicationContext;

	@Resource
	private WileyCardPaymentService wileyCardPaymentService;

	private AmountToPayCalculationStrategy amountToPayCalculationStrategy;

	@Resource
	private PaymentTransactionService paymentTransactionService;

	@Resource
	private WileyMPGSPaymentEntryService wileyMPGSPaymentEntryService;

	@Resource
	private WileyUrlGenerationService wileyUrlGenerationService;

	@Resource
	private ConfigurationService configurationService;

	@Resource
	private WileyMPGSTransactionIdGeneratorStrategy wileyMPGSTransactionIdGenerateStrategy;

	@Resource
	protected WileyMPGSMerchantService wileyMPGSMerchantService;

	@Override
	public PaymentTransactionEntryModel retrieveSession(@Nonnull final AbstractOrderModel cart, @Nonnull final String sessionId)
	{
		ServicesUtil.validateParameterNotNull(cart, "[cart] can't be null.");
		ServicesUtil.validateParameterNotNull(sessionId, "[sessionId] can't be null.");

		final String tnsMerchantId = wileyMPGSMerchantService.getMerchantID(cart);

		String retrieveSessionUrl = wileyUrlGenerationService.getRetrieveSessionUrl(tnsMerchantId, sessionId);
		WileyRetrieveSessionRequest retrieveSessionRequest = new WileyRetrieveSessionRequest(retrieveSessionUrl,
				WileyMPGSConstants.MPGS_PAYMENT_PROVIDER);

		WileyRetrieveSessionResponse retrieveSessionResponse = wileyCardPaymentService.retrieveSession(
				retrieveSessionRequest);

		PaymentTransactionModel paymentTransaction = wileyMPGSPaymentEntryService.createPaymentTransaction(cart);
		return wileyMPGSPaymentEntryService.createRetrieveSessionEntry(retrieveSessionResponse, paymentTransaction);
	}

	/**
	 * @param cart
	 * @param sessionId
	 * 		Session id from tns gateway
	 * @return Verify entry model
	 */
	@Override
	public PaymentTransactionEntryModel verify(@Nonnull final AbstractOrderModel cart, @Nonnull final String sessionId)
	{
		ServicesUtil.validateParameterNotNull(cart, "[cart] can't be null.");
		ServicesUtil.validateParameterNotNull(sessionId, "[sessionId] can't be null.");

		return getCardVerificationStrategy(cart).verify(cart, sessionId);
	}

	private CardVerificationStrategy getCardVerificationStrategy(final AbstractOrderModel cart)
	{
		final WileyTnsMerchantModel merchant = wileyMPGSMerchantService.getMerchant(cart);
		String cardVerificationStrategyBeanName = WileyCoreConstants.DEFAULT_MPGS_CARD_VERIFY_STRATEGY;
		if (!merchant.isEnableVerification())
		{
			cardVerificationStrategyBeanName = WileyCoreConstants.DEFAULT_MPGS_CARD_NO_VERIFY_STRATEGY;
			LOG.info(String.format("\"Verify\" operation for merchant %s will be skipped", merchant.getId()));
		}
		return applicationContext.getBean(cardVerificationStrategyBeanName, CardVerificationStrategy.class);
	}

	@Override
	public PaymentTransactionEntryModel tokenizeCart(@Nonnull final CartModel cart,
			@Nonnull final PaymentTransactionEntryModel retrieveSessionTransactionEntry, final Boolean saveInAccount)
	{
		ServicesUtil.validateParameterNotNull(retrieveSessionTransactionEntry,
				"[retrieveSessionTransactionEntry] can't be null.");

		final String tnsMerchantId = wileyMPGSMerchantService.getMerchantID(cart);

		String tokenizationUrl = wileyUrlGenerationService.getTokenizationUrl(tnsMerchantId);
		WileyTokenizationRequest tokenizationRequest = new WileyTokenizationRequest(tokenizationUrl,
				retrieveSessionTransactionEntry.getRequestId(), WileyMPGSConstants.MPGS_PAYMENT_PROVIDER);
		tokenizationRequest.setSourceOfFundsType(WileyMPGSConstants.SOURCE_OF_FUNDS_TYPE);
		WileyTokenizationResponse tokenizationResponse = wileyCardPaymentService.tokenize(tokenizationRequest);

		return wileyMPGSPaymentEntryService.createTokenizationEntry(
				tokenizationResponse, retrieveSessionTransactionEntry.getPaymentTransaction(), saveInAccount);
	}


	@Override
	public PaymentTransactionEntryModel tokenizeOrder(@Nonnull final OrderModel order,
			@Nonnull final PaymentTransactionEntryModel retrieveSessionTransactionEntry, final Boolean saveInAccount)
	{
		ServicesUtil.validateParameterNotNull(retrieveSessionTransactionEntry,
				"[order] can't be null.");
		ServicesUtil.validateParameterNotNull(retrieveSessionTransactionEntry,
				"[retrieveSessionTransactionEntry] can't be null.");

		final String tnsMerchantId = wileyMPGSMerchantService.getMerchantID(order);

		String tokenizationUrl = wileyUrlGenerationService.getTokenizationUrl(tnsMerchantId);
		WileyTokenizationRequest tokenizationRequest = new WileyTokenizationRequest(tokenizationUrl,
				retrieveSessionTransactionEntry.getRequestId(), WileyMPGSConstants.MPGS_PAYMENT_PROVIDER);
		tokenizationRequest.setSourceOfFundsType(WileyMPGSConstants.SOURCE_OF_FUNDS_TYPE);

		WileyTokenizationResponse tokenizationResponse = wileyCardPaymentService.tokenize(tokenizationRequest);

		return wileyMPGSPaymentEntryService.createTokenizationEntry(
				tokenizationResponse, retrieveSessionTransactionEntry.getPaymentTransaction(), saveInAccount, order);
	}


	@Override
	public PaymentTransactionEntryModel authorize(@Nonnull final AbstractOrderModel abstractOrder)
	{
		ServicesUtil.validateParameterNotNull(abstractOrder, "[abstractOrder] can't be null.");
		final PaymentTransactionModel paymentTransaction = wileyMPGSPaymentEntryService.createPaymentTransaction(abstractOrder);
		WileyAuthorizationRequest request = createWileyAuthorizationRequest(abstractOrder, paymentTransaction);
		WileyAuthorizationResponse authorizationResponse = wileyCardPaymentService.authorize(request);

		return wileyMPGSPaymentEntryService.createAuthorizationPaymentTransactionEntry(authorizationResponse,
				paymentTransaction);
	}

	private WileyAuthorizationRequest createWileyAuthorizationRequest(final AbstractOrderModel abstractOrder,
			final PaymentTransactionModel transaction)
	{
		WileyAuthorizationRequest request = new WileyAuthorizationRequest();

		ServicesUtil.validateParameterNotNull(abstractOrder.getCurrency(), "[currency] can't be null");
		ServicesUtil.validateParameterNotNull(abstractOrder.getPaymentInfo(), "[paymentInfo] can't be null");
		AddressModel billingAddress = abstractOrder.getPaymentInfo().getBillingAddress();
		if (billingAddress == null)
		{
			billingAddress = abstractOrder.getPaymentAddress();
		}

		ServicesUtil.validateParameterNotNull(billingAddress, "[billingAddress] can't be null");
		ServicesUtil.validateParameterNotNull(billingAddress.getCountry(), "[country] can't be null");

		request.setPaymentProvider(WileyMPGSConstants.MPGS_PAYMENT_PROVIDER);
		request.setApiOperation(WileyMPGSConstants.API_OPERATION_AUTHORIZE);
		request.setToken(extractTokenFromCart(abstractOrder));
		final String transactionId = wileyMPGSTransactionIdGenerateStrategy.generateTransactionId();
		final String orderId = getMPGSOrderId(transaction);
		final String tnsMerchantId = wileyMPGSMerchantService.getMerchantID(abstractOrder);
		request.setUrlPath(wileyUrlGenerationService.getAuthorizationURL(tnsMerchantId, orderId, transactionId));
		final String currency = abstractOrder.getCurrency().getIsocode();
		request.setCurrency(currency);
		request.setTotalAmount(amountToPayCalculationStrategy.getOrderAmountToPay(abstractOrder));
		request.setBillingStreet1(billingAddress.getLine1());
		request.setBillingStreet2(billingAddress.getLine2());
		request.setBillingCity(billingAddress.getTown());
		final RegionModel region = billingAddress.getRegion();
		request.setStateProvince(region != null ? region.getIsocode() : null);
		request.setBillingPostalCode(billingAddress.getPostalcode());
		request.setBillingCountry(billingAddress.getCountry().getIsocode3());
		request.setTransactionReference(getTransactionReference(transactionId));

		return request;
	}


	private String extractTokenFromCart(final AbstractOrderModel cart)
	{
		return ((CreditCardPaymentInfoModel) cart.getPaymentInfo()).getSubscriptionId();
	}


	@Override
	public Optional<PaymentTransactionEntryModel> capture(@Nonnull final PaymentTransactionModel transaction)
	{
		ServicesUtil.validateParameterNotNull(transaction, "[transaction] can't be null.");
		ServicesUtil.validateParameterNotNull(transaction.getOrder(), "[transaction.order] can't be null.");

		Optional<PaymentTransactionEntryModel> authorizationEntry = paymentTransactionService.findAuthorizationEntry(transaction);
		PaymentTransactionEntryModel captureEntry = null;

		if (authorizationEntry.isPresent())
		{
			final AbstractOrderModel order = transaction.getOrder();
			final String orderId = getMPGSOrderId(transaction);
			final String transactionId = wileyMPGSTransactionIdGenerateStrategy.generateTransactionId();
			final String tnsMerchantId = wileyMPGSMerchantService.getMerchantID(order);
			String captureUrl = wileyUrlGenerationService.getCaptureUrl(tnsMerchantId, orderId, transactionId);
			WileyCaptureRequest captureRequest = new WileyCaptureRequest(captureUrl, authorizationEntry.get().getAmount(),
					WileyMPGSConstants.MPGS_PAYMENT_PROVIDER, authorizationEntry.get().getCurrency().getIsocode());
			captureRequest.setTransactionReference(getTransactionReference(transactionId));

			WileyCaptureResponse captureResponse = wileyCardPaymentService.capture(captureRequest);

			captureEntry = wileyMPGSPaymentEntryService.createCaptureEntry(captureResponse, transaction);
		}

		return Optional.ofNullable(captureEntry);
	}


	@Override
	public PaymentTransactionEntryModel refundFollowOn(@Nonnull final PaymentTransactionModel paymentTransaction,
			@Nonnull final BigDecimal refundAmount)
	{
		ServicesUtil.validateParameterNotNull(paymentTransaction, "[paymentTransaction] can't be null.");
		OrderModel order = (OrderModel) paymentTransaction.getOrder();
		ServicesUtil.validateParameterNotNull(order, "[paymentTransaction.order] can't be null.");
		ServicesUtil.validateParameterNotNull(refundAmount, "[refundAmount] can't be null.");

		PaymentTransactionEntryModel capturePaymentTransactionEntryModel = getCapturePaymentTransactionEntryModel(
				paymentTransaction);

		Currency currency = Currency.getInstance(capturePaymentTransactionEntryModel.getCurrency().getIsocode());

		final String orderId = getMPGSOrderId(paymentTransaction);
		final String transactionId = wileyMPGSTransactionIdGenerateStrategy.generateTransactionId();
		final String tnsMerchantId = capturePaymentTransactionEntryModel.getTnsMerchantId();

		String refundFollowOnUrl = wileyUrlGenerationService.getRefundFollowOnUrl(tnsMerchantId, orderId, transactionId);
		WileyFollowOnRefundRequest refundRequest = new WileyFollowOnRefundRequest(refundFollowOnUrl,
				WileyMPGSConstants.MPGS_PAYMENT_PROVIDER, currency, refundAmount);
		refundRequest.setTransactionReference(getTransactionReference(transactionId));

		WileyFollowOnRefundResponse refundResponse = wileyCardPaymentService.refundFollowOn(refundRequest);

		return wileyMPGSPaymentEntryService.createFollowOnRefundEntry(
				refundResponse, paymentTransaction);
	}

	protected String getTransactionReference(final String transactionId)
	{
		ServicesUtil.validateParameterNotNull(transactionId, "[transactionId] can't be null");
		// TODO: use siteConfigService for getting vendor.id for asSite.
		final String vendorId = configurationService.getConfiguration().getString(WileyMPGSConstants.PAYMENT_MPGS_VENDOR_ID);
		StringBuilder transactionReference = new StringBuilder("0");
		transactionReference.append(vendorId).append(transactionId);
		return transactionReference.toString();
	}

	private String getMPGSOrderId(@Nonnull final PaymentTransactionModel transaction)
	{
		ServicesUtil.validateParameterNotNull(transaction, "[transaction] can't be null.");
		final AbstractOrderModel order = transaction.getOrder();
		ServicesUtil.validateParameterNotNull(order, "[transaction.order] can't be null.");

		//based on PaymentProvider version
		if (Objects.equals(transaction.getPaymentProvider(), WileyMPGSConstants.MPGS_PAYMENT_PROVIDER))
		{
			return order.getGuid();
		}
		else
		{
			return transaction.getCode();
		}
	}

	@Required
	public void setAmountToPayCalculationStrategy(
			final AmountToPayCalculationStrategy amountToPayCalculationStrategy)
	{
		this.amountToPayCalculationStrategy = amountToPayCalculationStrategy;
	}

	protected PaymentTransactionEntryModel getCapturePaymentTransactionEntryModel(
			final PaymentTransactionModel paymentTransaction)
	{
		return paymentTransaction.getEntries()
				.stream()
				.filter(entry -> entry.getType().equals(PaymentTransactionType.CAPTURE))
				.findFirst()
				.orElseThrow(() -> new IllegalArgumentException(
						"'CAPTURE' PaymentTransactionEntry doesn't exist for transaction code: " + paymentTransaction.getCode()));
	}

	@Override
	public void setApplicationContext(final ApplicationContext applicationContext) throws BeansException
	{
		this.applicationContext = applicationContext;
	}
}
