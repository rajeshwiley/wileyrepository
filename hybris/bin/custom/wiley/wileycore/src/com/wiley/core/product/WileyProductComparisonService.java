/**
 *
 */
package com.wiley.core.product;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;

/**
 * Service class for wiley product comparison table.
 *
 * @spring.bean wileyProductComparisonService
 */
public interface WileyProductComparisonService
{
	/**
	 *
	 * Retrieve the products to be displayed in the comparison table for the
	 * given category. The products are sorted in descending order of the
	 * prices. The price for each product is calculated as the minimum price of
	 * all its variants.
	 *
	 * @param categoryModel
	 * @param maxNumberOfProducts
	 * @return list of products for comparison table
	 */
	List<ProductModel> getProductsForComparison(CategoryModel categoryModel, Integer maxNumberOfProducts);


	/**
	 * Retrieve the variant products to be displayed in the comparison table for the
	 * given baseProductCategory and variantValueCategory. The products are sorted in descending order of the
	 * prices.
	 *
	 * @param baseProductCategoryModel
	 * @param variantValueCategoryCode
	 * @param maxNumberOfProducts
	 * @return list of variant products for comparison table
	 */
	List<ProductModel> getProductsForComparison(String baseProductCategoryModel,
			String variantValueCategoryCode, Integer maxNumberOfProducts);

}
