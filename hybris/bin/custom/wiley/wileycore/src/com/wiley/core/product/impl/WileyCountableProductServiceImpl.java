package com.wiley.core.product.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantProductModel;

import javax.annotation.Nonnull;


public class WileyCountableProductServiceImpl extends AbstractWileyCountableProductService
{
	@Override
	protected boolean doCanProductHaveQuantity(@Nonnull final ProductModel product)
	{
		Boolean countable = getCountableProperty(product);
		if (countable == null)
		{
			return true;
		}
		return countable;
	}

	private Boolean getCountableProperty(final ProductModel product)
	{
		Boolean countable = product.getCountable();
		if (countable == null && product instanceof VariantProductModel)
		{
			countable = ((VariantProductModel) product).getBaseProduct().getCountable();
		}
		return countable;
	}

}
