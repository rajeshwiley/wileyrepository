package com.wiley.core.payment.command;


import de.hybris.platform.payment.commands.FollowOnRefundCommand;
import de.hybris.platform.payment.dto.TransactionStatus;

import javax.xml.ws.WebServiceException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.integration.wpg.WileyPaymentGateway;
import com.wiley.core.payment.enums.WileyTransactionStatusEnum;
import com.wiley.core.payment.request.WileyFollowOnRefundRequest;
import com.wiley.core.payment.response.WileyRefundResult;


public class WileyFollowOnRefundCommand implements FollowOnRefundCommand<WileyFollowOnRefundRequest>
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyFollowOnRefundCommand.class);

	@Autowired
	private WileyPaymentGateway wileyPaymentGateway;

	@Override
	public WileyRefundResult perform(final WileyFollowOnRefundRequest commandRequest)
	{
		WileyRefundResult refundResult;
		try
		{
			refundResult = wileyPaymentGateway.tokenRefund(commandRequest);
			refundResult.setCurrency(commandRequest.getCurrency());
		}
		catch (WebServiceException ex)
		{
			LOG.error(String.format("WPG unable to process refund due to communication problems. Merchant "
							+ "Transaction code:%s, request id:%s, order code:%s",
					commandRequest.getMerchantTransactionCode(), commandRequest.getRequestId(), commandRequest.getOrderCode()),
					ex);
			refundResult = createFailureResult(WileyTransactionStatusEnum.WPG_COMMUNICATION_FAILURE, commandRequest);
		}
		catch (Exception ex)
		{
			LOG.error(String.format("WPG unable to process refund. Merchant Transaction code:%s, request id:%s, order code:%s",
					commandRequest.getMerchantTransactionCode(), commandRequest.getRequestId(), commandRequest.getOrderCode()),
					ex);
			refundResult = createFailureResult(WileyTransactionStatusEnum.GENERAL_SYSTEM_ERROR, commandRequest);
		}
		return refundResult;
	}

	private WileyRefundResult createFailureResult(final WileyTransactionStatusEnum status,
			final WileyFollowOnRefundRequest commandRequest)
	{
		WileyRefundResult failureResult = new WileyRefundResult();
		failureResult.setTransactionStatus(TransactionStatus.ERROR);
		failureResult.setWpgTransactionStatus(status);
		failureResult.setTotalAmount(commandRequest.getTotalAmount());
		failureResult.setCurrency(commandRequest.getCurrency());

		return failureResult;
	}
}
