package com.wiley.core.customer;

/**
 * Defines possible values for redirecting user after resetting password
 */
public enum ResetPasswordRedirectType
{
	/**
	 * Specifies redirect to checkout login page
	 */
	ch,
	/**
	 * Specifies redirect to PIN Activation login page
	 */
	pl,
	/**
	 * Specifies redirect to login page
	 */
	lp;

	public String toString() {
		return this.name();
	}

	public static ResetPasswordRedirectType getDefault() {
		return lp;
	}

	/**
	 * Returns retrieved value if it is not null, or default
	 * @param redirectType - current redirect type
	 * @return - recieved redirectType or default
	 */
	public static ResetPasswordRedirectType getOrDefaultRedirectType(final ResetPasswordRedirectType redirectType) {
		return redirectType == null ? ResetPasswordRedirectType.getDefault() : redirectType;
	}
}
