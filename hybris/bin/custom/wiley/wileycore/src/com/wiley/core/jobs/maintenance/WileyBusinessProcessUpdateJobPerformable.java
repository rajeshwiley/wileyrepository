package com.wiley.core.jobs.maintenance;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.jobs.AbstractMaintenanceJobPerformable;
import de.hybris.platform.processengine.enums.ProcessState;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import com.wiley.core.model.BusinessProcessUpdateMaintenanceCronJobModel;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public class WileyBusinessProcessUpdateJobPerformable extends AbstractMaintenanceJobPerformable
{
	private static final Logger LOGGER = LoggerFactory.getLogger(WileyBusinessProcessUpdateJobPerformable.class);

	@Value("${db.type.system.name}")
	private String typeSystemProp;


	@Override
	public FlexibleSearchQuery getFetchQuery(final CronJobModel cronJob)
	{
		if (cronJob instanceof BusinessProcessUpdateMaintenanceCronJobModel)
		{
			if (LOGGER.isInfoEnabled())
			{
				LOGGER.info("Cron Job " + cronJob.getCode() + " is started");
			}
			final BusinessProcessUpdateMaintenanceCronJobModel cjm = (BusinessProcessUpdateMaintenanceCronJobModel) cronJob;
			final List<ProcessState> processStates = cjm.getProcessStates();
			final String typeSystem = cjm.getTypeSystem();
			String query = "select {pk} from {BusinessProcess} where {state} in (?states)";

			final Map<String, Object> parameters = new HashMap<>();

			if (CollectionUtils.isEmpty(processStates))
			{
				parameters.put("states", Collections.singletonList(ProcessState.RUNNING));
			}
			else
			{
				parameters.put("states", processStates);
			}

			if (StringUtils.isNoneEmpty(typeSystem))
			{
				query += " and {typeSystem} = ?typeSystem";
				parameters.put("typeSystem", typeSystem);
			}

			return new FlexibleSearchQuery(query, parameters);
		}
		else
		{
			throw new UnsupportedOperationException("Cron Job must be instance of BusinessProcessUpdateMaintenanceCronJobModel");
		}
	}

	@Override
	public void process(final List<ItemModel> elements, final CronJobModel cronJob)
	{
		if (cronJob instanceof BusinessProcessUpdateMaintenanceCronJobModel)
		{
			final BusinessProcessUpdateMaintenanceCronJobModel cjm = (BusinessProcessUpdateMaintenanceCronJobModel) cronJob;
			final String newTypeSystem = cjm.getNewTypeSystem();
			elements.stream()
					.filter(e -> e instanceof BusinessProcessModel)
					.forEach(e -> ((BusinessProcessModel) e)
							.setTypeSystem(StringUtils.isEmpty(newTypeSystem) ? typeSystemProp : newTypeSystem));
			modelService.saveAll(elements);
		}
		else
		{
			throw new UnsupportedOperationException("Cron Job must be instance of BusinessProcessUpdateMaintenanceCronJobModel");
		}
	}
}
