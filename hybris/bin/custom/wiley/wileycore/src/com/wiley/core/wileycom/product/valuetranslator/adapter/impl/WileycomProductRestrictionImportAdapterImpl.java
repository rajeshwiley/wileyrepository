package com.wiley.core.wileycom.product.valuetranslator.adapter.impl;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.impex.jalo.header.UnresolvedValueException;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.voucher.model.ProductRestrictionModel;
import de.hybris.platform.voucher.model.PromotionVoucherModel;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.wiley.core.product.WileyProductService;
import com.wiley.core.wileycom.valuetranslator.adapter.AbstractWileycomImportAdapter;


/**
 * @author Dzmitryi_Halahayeu
 */
public class WileycomProductRestrictionImportAdapterImpl extends AbstractWileycomImportAdapter
{
	private static final String DELIMITER = "\\|";
	@Resource
	private WileyProductService productService;
	@Resource
	private ModelService modelService;
	@Resource
	private CatalogVersionService catalogVersionService;

	@Override
	public void performImport(final String cellValue, final Item promotionVoucher)
			throws UnresolvedValueException
	{
		Assert.hasText(cellValue);
		Assert.notNull(promotionVoucher);
		String[] isbns = cellValue.split(DELIMITER);
		PromotionVoucherModel promotionVoucherModel = modelService.get(promotionVoucher);
		List<ProductModel> products = new ArrayList<>();
		for (String isbn : isbns)
		{
			products.add(productService
					.getProductForIsbn(isbn, catalogVersionService.getCatalogVersion(getCatalogId(), getCatalogVersion())));
		}

		promotionVoucherModel.getRestrictions().stream().filter(restriction -> restriction instanceof ProductRestrictionModel)
				.forEach(restriction -> modelService.remove(restriction));

		ProductRestrictionModel productRestrictionModel = new ProductRestrictionModel();
		productRestrictionModel.setProducts(products);
		productRestrictionModel.setVoucher(promotionVoucherModel);
		modelService.save(productRestrictionModel);
	}
}
