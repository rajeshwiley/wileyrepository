package com.wiley.core.payment.transaction;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.paypal.hybris.data.ResultErrorData;


public interface PaymentTransactionService
{
	PaymentTransactionEntryModel getAcceptedTransactionEntry(OrderModel orderModel,
			PaymentTransactionType paymentTransactionType);

	List<PaymentTransactionEntryModel> getAcceptedTransactionEntries(OrderModel orderModel,
			PaymentTransactionType paymentTransactionType);

	PaymentTransactionEntryModel getAcceptedTransactionEntry(PaymentTransactionModel transaction,
			PaymentTransactionType transactionType);

	List<PaymentTransactionEntryModel> getAcceptedTransactionEntries(PaymentTransactionModel transaction,
			PaymentTransactionType transactionEntryType);

	double calculateTotalAuthorized(OrderModel order);

	PaymentTransactionEntryModel createTransactionEntry(PaymentTransactionType type, String status,
			String statusDetails, String requestId, AbstractOrderModel abstractOrder, CurrencyModel currency,
			double amount, Date timeStamp, PaymentTransactionModel paymentTransaction);

	String createPayPalErrorDetails(List<ResultErrorData> errors);

	Optional<PaymentTransactionEntryModel> findAuthorizationEntry(PaymentTransactionModel transaction);

	PaymentTransactionModel getLastPaymentTransaction(AbstractOrderModel orderModel);
}
