package com.wiley.core.jalo;

import de.hybris.platform.cms2.jalo.restrictions.UserRestriction;
import de.hybris.platform.cms2.jalo.site.CMSSite;
import de.hybris.platform.cms2.model.UserRestrictionDescription;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.util.localization.Localization;

import java.util.Collection;
import java.util.Iterator;

import org.apache.log4j.Logger;

import com.wiley.core.wileyb2c.cms2.model.Wileyb2cCMSSiteRestrictionDescriptionHandler;


public class Wileyb2cCMSSiteRestriction extends GeneratedWileyb2cCMSSiteRestriction
{
	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(Wileyb2cCMSSiteRestriction.class.getName());

	@Override
	protected Item createItem(final SessionContext ctx, final ComposedType type, final ItemAttributeMap allAttributes)
			throws JaloBusinessException
	{
		// business code placed here will be executed before the item is created
		// then create the item
		final Item item = super.createItem(ctx, type, allAttributes);
		// business code placed here will be executed after the item was created
		// and return the item
		return item;
	}

	/**
	 * OOTB_CODE
	 * It's based on {@link UserRestriction#getDescription(SessionContext)}. Also similar logic is called from
	 * {@link Wileyb2cCMSSiteRestrictionDescriptionHandler} like in {@link UserRestrictionDescription}
	 * It looks like OOTB this logic is implemented several times for support as Jalo, as Service model.
	 * I prefer to follow the same OOTB approach to prevent unexpected Hybris behavior
	 */
	@Override
	public String getDescription(final SessionContext sessionContext)
	{
		final Collection<CMSSite> sites = this.getSites();

		StringBuilder result = new StringBuilder();
		if (sites != null && !sites.isEmpty())
		{
			String localizedString = Localization.getLocalizedString("type.Wileyb2cCMSSiteRestriction.description.text");
			result.append(localizedString == null ? "Page only applies on sites:" : localizedString);
			Iterator site = sites.iterator();

			while (site.hasNext())
			{
				UserModel user = (UserModel) site.next();
				result.append(" ").append(user.getName()).append(" (").append(user.getUid()).append(");");
			}
		}

		return result.toString();
	}

}
