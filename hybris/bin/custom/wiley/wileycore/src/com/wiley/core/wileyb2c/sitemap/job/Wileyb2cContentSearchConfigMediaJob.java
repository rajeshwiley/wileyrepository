package com.wiley.core.wileyb2c.sitemap.job;

import de.hybris.platform.acceleratorservices.cronjob.SiteMapMediaJob;
import de.hybris.platform.acceleratorservices.enums.SiteMapPageEnum;
import de.hybris.platform.acceleratorservices.model.SiteMapConfigModel;
import de.hybris.platform.acceleratorservices.model.SiteMapMediaCronJobModel;
import de.hybris.platform.acceleratorservices.model.SiteMapPageModel;
import de.hybris.platform.acceleratorservices.sitemap.generator.SiteMapGenerator;
import de.hybris.platform.catalog.model.CatalogUnawareMediaModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.store.WileyBaseStoreService;


/**
 * @author Dzmitryi_Halahayeu
 */
public class Wileyb2cContentSearchConfigMediaJob extends Wileyb2cSiteMapMediaJob
{
	private static final Logger LOG = Logger.getLogger(Wileyb2cContentSearchConfigMediaJob.class);
	private WileyBaseStoreService wileyBaseStoreService;

	/**
	 * OOTB_CODE based on {@link SiteMapMediaJob#perform(SiteMapMediaCronJobModel)}. Custom logic is placed
	 * in methods {@link #mergeFiles(File, List)} and {@link #updateConfig(File, CMSSiteModel)}
	 *
	 * @param cronJob
	 * @return
	 */
	@Override
	public PerformResult perform(final SiteMapMediaCronJobModel cronJob)
	{
		File contentSearchConfigFile = null;
		final CMSSiteModel contentSite = cronJob.getContentSite();

		getCmsSiteService().setCurrentSite(contentSite);
		// set the catalog version for the current session
		getActivateBaseSiteInSession().activate(contentSite);

		final SiteMapConfigModel siteMapConfig = contentSite.getContentSearchConfig();
		final Collection<SiteMapPageModel> siteMapPages = siteMapConfig.getSiteMapPages();
		final Collection<SiteMapPageModel> failedSiteMapPages = new ArrayList<>();
		for (final SiteMapPageModel siteMapPage : siteMapPages)
		{
			final List<File> configFiles = new ArrayList<>();
			final SiteMapPageEnum pageType = siteMapPage.getCode();
			final SiteMapGenerator generator = this.getGeneratorForSiteMapPage(pageType);

			if (BooleanUtils.isTrue(siteMapPage.getActive()) && generator != null)
			{
				try
				{
					prepareModelsList(cronJob, contentSite, siteMapConfig, configFiles, pageType, generator);
				}
				catch (IllegalArgumentException e)
				{
					getLogger().error(String.format(PREPARE_MODEL_LIST_FOR_SITE_MAP_PAGE_FAILED_MESSAGE, siteMapPage.getCode()),
							e);
					failedSiteMapPages.add(siteMapPage);
				}
			}
			else
			{
				LOG.warn(String.format("Skipping SiteMap page %s active %s", siteMapPage.getCode(), siteMapPage.getActive()));
			}
			if (!configFiles.isEmpty())
			{
				try
				{
					if (contentSearchConfigFile == null)
					{
						contentSearchConfigFile = File.createTempFile("seeds", ".txt");
					}
					mergeFiles(contentSearchConfigFile, configFiles);
				}
				catch (IOException e)
				{
					throw new RuntimeException(e);
				}
			}
		}
		if (!failedSiteMapPages.isEmpty())
		{
			throw new IllegalArgumentException(PREPARE_MODEL_LIST_FOR_ONE_OF_SITE_MAP_PAGES_FAILED_MESSAGE);
		}

		if (contentSearchConfigFile != null)
		{
			updateConfig(contentSearchConfigFile, contentSite);
		}


		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);


	}

	private void updateConfig(final File contentSearchConfigFile, final CMSSiteModel contentSite)
	{
		final MediaModel existingConfig = contentSite.getContentSearchUrls();
		final CatalogUnawareMediaModel contentSearchConfig = createCatalogUnawareMediaModel(contentSearchConfigFile);

		contentSite.setContentSearchUrls(contentSearchConfig);
		modelService.save(contentSite);

		// clean up old config media
		if (existingConfig != null)
		{
			modelService.remove(existingConfig);
		}
	}

	private void mergeFiles(final File contentSearchConfigFile, final List<File> configFiles) throws IOException
	{
		for (final File configFile : configFiles)
		{
			String fileToString = FileUtils.readFileToString(configFile);
			FileUtils.writeStringToFile(contentSearchConfigFile, fileToString, true);
		}
	}

	@Required
	public void setWileyBaseStoreService(final WileyBaseStoreService wileyBaseStoreService)
	{
		this.wileyBaseStoreService = wileyBaseStoreService;
	}

	@Override
	protected Logger getLogger()
	{
		return LOG;
	}
}
