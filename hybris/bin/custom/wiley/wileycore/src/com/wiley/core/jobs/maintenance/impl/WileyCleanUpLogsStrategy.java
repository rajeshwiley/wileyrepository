package com.wiley.core.jobs.maintenance.impl;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.cronjob.model.JobLogModel;
import de.hybris.platform.cronjob.model.LogFileModel;
import de.hybris.platform.jobs.maintenance.MaintenanceCleanupStrategy;
import de.hybris.platform.jobs.maintenance.impl.CleanUpLogsStrategy;
import de.hybris.platform.processengine.enums.BooleanOperator;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import static de.hybris.platform.jobs.maintenance.impl.CleanUpLogsStrategy.CRON_JOBS_QUERY;
import static de.hybris.platform.jobs.maintenance.impl.CleanUpLogsStrategy.JOB_LOGS_QUERY;
import static de.hybris.platform.jobs.maintenance.impl.CleanUpLogsStrategy.LOG_FILES_QUERY;


public class WileyCleanUpLogsStrategy implements MaintenanceCleanupStrategy<CronJobModel, CronJobModel>
{
	private static final Logger LOG = Logger.getLogger(WileyCleanUpLogsStrategy.class.getName());

	private FlexibleSearchService flexibleSearchService;
	private ModelService modelService;

	@Override
	public FlexibleSearchQuery createFetchQuery(final CronJobModel cjm)
	{
		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(CRON_JOBS_QUERY);
		fsq.setResultClassList(Collections.singletonList(CronJobModel.class));
		return fsq;
	}


	/**
	 * Adjusted OOTB method {@link CleanUpLogsStrategy#process(List)}. .
	 * Modification: filtered out null objects from given {@param elements}.
	 *
	 * @param elements
	 * 		List of cronjobs selected for cleaning logs.
	 */
	@Override
	public void process(final List<CronJobModel> elements)
	{
		LOG.info("Searching " + elements.size() + " cron jobs for log entries eligible for deletion.");
		final ArrayList<ItemModel> toBeRemoved = new ArrayList<>();

		elements.stream()
				.filter(Objects::nonNull)
				.forEach(cronJobModel -> processSingleCronJob(cronJobModel, toBeRemoved));
	}

	private void processSingleCronJob(final CronJobModel cronJobModel, final ArrayList<ItemModel> toBeRemoved)
	{
		toBeRemoved.clear();
		final List<JobLogModel> jobLogModels = this.getLogModels(cronJobModel, JOB_LOGS_QUERY);
		toBeRemoved.addAll(findLogModels(jobLogModels, cronJobModel.getLogsOperator(), cronJobModel.getLogsDaysOld(),
				cronJobModel.getLogsCount()));

		final List<LogFileModel> logFileModels = this.getLogModels(cronJobModel, LOG_FILES_QUERY);
		toBeRemoved.addAll(findLogModels(logFileModels, cronJobModel.getFilesOperator(), cronJobModel.getFilesDaysOld(),
				cronJobModel.getFilesCount()));

		if (!toBeRemoved.isEmpty())
		{
			LOG.info("Removing " + toBeRemoved.size() + " log entries.");
			modelService.removeAll(toBeRemoved);
		}
	}

	private List<ItemModel> findLogModels(final List<? extends ItemModel> logModels, final BooleanOperator operator,
			final Integer daysOld, final Integer maxCount)
	{
		int logModelsCount = logModels.size();
		final List<ItemModel> toBeRemoved = new ArrayList<>();
		for (final ItemModel logModel : logModels)
		{
			switch (operator)
			{
				case AND:
					if (isOlderThan(logModel.getCreationtime(), daysOld) && logModelsCount > maxCount)
					{
						logModelsCount--;
						toBeRemoved.add(logModel);
					}
					break;
				case OR:
					if (isOlderThan(logModel.getCreationtime(), daysOld) || logModelsCount > maxCount)
					{
						logModelsCount--;
						toBeRemoved.add(logModel);
					}
					break;
				default:
					throw new IllegalStateException("Unsupported operator: " + operator.getCode() + ".");
			}
		}
		return toBeRemoved;
	}

	private <T> List<T> getLogModels(final CronJobModel cronJobModel, final String queryString)
	{
		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(queryString);
		fsq.addQueryParameter("cronjob", cronJobModel.getPk().getLong());
		return flexibleSearchService.<T> search(fsq).getResult();
	}

	private boolean isOlderThan(final Date date, final int daysOld)
	{
		final Date before = DateTime.now().minusDays(daysOld).toDate();
		return date.before(before);
	}

	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}

	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}
}
