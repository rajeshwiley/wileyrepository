package com.wiley.core.order.dao.impl;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.search.flexiblesearch.PagedFlexibleSearchService;
import de.hybris.platform.commerceservices.search.flexiblesearch.data.SortQueryData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import com.wiley.core.order.dao.WileyOrderEntryDao;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


/**
 * Default implementation of {@link WileyOrderEntryDao}
 */
public class DefaultWileyOrderEntryDao implements WileyOrderEntryDao
{
	private static final String ORDER_NAME = "order";
	private static final String ORDER_ENTRY_NAME = "entry";
	private static final String ORDER_ENTRIES_SORT_BY_DATE = "byDate";

	private static final String QUERY_FIND_ORDER_ENTRIES_BY_PRODUCT_AND_OWNER = "SELECT {e." + OrderEntryModel.PK + "} FROM"
			+ " {" + OrderEntryModel._TYPECODE + " AS e"
			+ " JOIN " + OrderModel._TYPECODE + " AS o ON {e." + OrderEntryModel.ORDER + "} = {o." + OrderModel.PK + "}"
			+ " JOIN " + UserModel._TYPECODE + " AS u ON {o." + OrderModel.USER + "} = {u." + UserModel.PK + "}"
			+ " JOIN " + ProductModel._TYPECODE + " AS p ON {e." + OrderEntryModel.PRODUCT + "} = {p." + ProductModel.PK + "}}"
			+ " WHERE {u." + UserModel.UID + "} = ?ownerId AND {p." + ProductModel.CODE + "} = ?productCode";

	private static final String QUERY_FIND_ORDER_ENTRIES_BY_PRODUCT_AND_OWNER_EXCLUDING_STATUSES =
			QUERY_FIND_ORDER_ENTRIES_BY_PRODUCT_AND_OWNER + " AND {o." + OrderModel.STATUS + "} not in (?orderStatuses)";

	private static final String QUERY_FIND_ORDER_ENTRIES_BY_BUSINESS_VALUES = "SELECT {" + ORDER_ENTRY_NAME
			+ ":" + AbstractOrderEntryModel.PK
			+ "} FROM {" + AbstractOrderEntryModel._TYPECODE + " AS " + ORDER_ENTRY_NAME
			+ " JOIN " + AbstractOrderModel._TYPECODE + " AS " + ORDER_NAME + " ON {" + ORDER_NAME + ":"
			+ AbstractOrderModel.PK + "} = {" + ORDER_ENTRY_NAME + ":" + AbstractOrderEntryModel.ORDER + "}}"
			+ " WHERE {" + ORDER_NAME + ":" + AbstractOrderModel.SITE + "} = ?" + AbstractOrderModel.SITE
			+ " AND {" + ORDER_ENTRY_NAME + ":originalOrderEntry} IS NULL";

	private static final String QUERY_FIND_ORDER_ENTRY_BY_GUID = "SELECT {entry.PK} FROM "
			+ "{OrderEntry AS entry "
			+ "JOIN AbstractOrder AS order ON {entry.order}={order.PK}} "
			+ "WHERE {entry.guid}=?guid "
			+ "AND {entry.originalOrderEntry} IS NULL";

	private static final String WHERE_AND_USER_UID = " AND {" + ORDER_NAME + ":" + AbstractOrderModel.USER + "} = ?"
			+ AbstractOrderModel.USER;

	private static final String WHERE_AND_ORDER_STATUSES = " AND {" + ORDER_ENTRY_NAME + ":"
			+ AbstractOrderEntryModel.STATUS + "} IN (?" + AbstractOrderEntryModel.STATUS + ")";

	private static final String WHERE_AND_BUSINESS_ITEM_ID = " AND {" + ORDER_ENTRY_NAME + ":"
			+ AbstractOrderEntryModel.BUSINESSITEMID + "} = ?" + AbstractOrderEntryModel.BUSINESSITEMID;

	private static final String WHERE_AND_BUSINESS_KEY = " AND {" + ORDER_ENTRY_NAME + ":"
			+ AbstractOrderEntryModel.ORIGINALBUSINESSKEY + "} = ?" + AbstractOrderEntryModel.ORIGINALBUSINESSKEY;

	private static final String ORDER_QUERY_BY_DATE = " ORDER BY {" + ORDER_ENTRY_NAME + ":"
			+ AbstractOrderEntryModel.CREATIONTIME + "} DESC";

	@Resource
	private PagedFlexibleSearchService pagedFlexibleSearchService;


	@Resource
	private FlexibleSearchService flexibleSearchService;


	@Override
	public List<OrderEntryModel> findOrderEntriesByProductAndOwnerExcludingStatuses(final String productCode,
											final String ownerId, final OrderStatus... orderStatuses)
	{
		validateParameterNotNull(productCode, "Product code must not be null!");
		validateParameterNotNull(ownerId, "Owner ID must not be null!");

		final Map<String, Object> params = new HashMap<>();
		params.put("ownerId", ownerId);
		params.put("productCode", productCode);
		params.put("orderStatuses", Arrays.asList(orderStatuses));
		SearchResult<OrderEntryModel> searchResult = getFlexibleSearchService().search(
				QUERY_FIND_ORDER_ENTRIES_BY_PRODUCT_AND_OWNER_EXCLUDING_STATUSES, params);
		return searchResult.getResult();
	}

	@Override
	public List<OrderEntryModel> findOrderEntryByGuid(final String guid)
	{
		validateParameterNotNull(guid, "guid");

		final FlexibleSearchQuery query = new FlexibleSearchQuery(QUERY_FIND_ORDER_ENTRY_BY_GUID);
		query.addQueryParameter("guid", guid);
		return flexibleSearchService.<OrderEntryModel> search(query).getResult();
	}

	public SearchPageData<AbstractOrderEntryModel> findOrderEntriesByBusinessValues(final BaseSiteModel baseSite,
			final UserModel user, final String businessItemId, final String businessKey,
			final PageableData pagination, final List<OrderStatus> statuses)
	{
		final StringBuilder query = new StringBuilder(QUERY_FIND_ORDER_ENTRIES_BY_BUSINESS_VALUES);
		final Map<String, Object> params = new HashMap<>();
		params.put(AbstractOrderModel.SITE, baseSite);

		if (user != null)
		{
			params.put(AbstractOrderModel.USER, user);
			query.append(WHERE_AND_USER_UID);
		}

		if (StringUtils.isNotEmpty(businessItemId))
		{
			params.put(AbstractOrderEntryModel.BUSINESSITEMID, businessItemId);
			query.append(WHERE_AND_BUSINESS_ITEM_ID);
		}

		if (StringUtils.isNotEmpty(businessKey))
		{
			params.put(AbstractOrderEntryModel.ORIGINALBUSINESSKEY, businessKey);
			query.append(WHERE_AND_BUSINESS_KEY);
		}

		if (CollectionUtils.isNotEmpty(statuses))
		{
			params.put(AbstractOrderEntryModel.STATUS, statuses);
			query.append(WHERE_AND_ORDER_STATUSES);
		}

		if (pagination.getSort().equals(ORDER_ENTRIES_SORT_BY_DATE))
		{
			query.append(ORDER_QUERY_BY_DATE);
		}

		final SortQueryData result = new SortQueryData();
		result.setSortCode(pagination.getSort());
		result.setQuery(query.toString());

		return pagedFlexibleSearchService.search(Arrays.asList(result), ORDER_ENTRIES_SORT_BY_DATE, params, pagination);
	}

	public FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}
}
