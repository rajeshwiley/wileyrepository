package com.wiley.core.address;

import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.user.daos.AddressDao;

import java.util.Optional;


public interface WileyAddressDao extends AddressDao
{
	/**
	 * Is used for searching address by id
	 *
	 * @param addressId
	 * @return
	 */
	Optional<AddressModel> findAddressById(String addressId);

	/**
	 * Is used for searching address by external id
	 * @param addressExternalId
	 * @param ownerId
	 * @return
	 */
	Optional<AddressModel> findAddressByExternalIdAndOwner(String addressExternalId, String ownerId);
}
