package com.wiley.core.payment.strategies;


public interface SecurityHashGeneratorStrategy
{
	String generateSecurityHash(String siteId, String counterMeasureBase);
}
