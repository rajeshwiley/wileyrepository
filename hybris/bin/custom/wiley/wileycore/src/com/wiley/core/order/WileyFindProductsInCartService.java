package com.wiley.core.order;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.Collection;
import java.util.List;


public interface WileyFindProductsInCartService
{
	/**
	 *
	 * @returns cart or order entries which contain products from given collection
	 */
	List<AbstractOrderEntryModel> getEntriesForProducts(AbstractOrderModel cart,
			Collection<ProductModel> products);

	/**
	 * Checks if abstract order contains at least one product from category
	 *
	 * @return true if abstract order contains at least one product from the category
	 *
	 */
	boolean containsAtLeastOneProduct(AbstractOrderModel abstractOrder, CategoryModel category);

	/**
	 * Checks if abstract order contains at least one product
	 *
	 * @return true if abstract order contains at least one product from the collection of products
	 *
	 */
	boolean containsAtLeastOneProduct(AbstractOrderModel abstractOrder, Collection<ProductModel> products);
}
