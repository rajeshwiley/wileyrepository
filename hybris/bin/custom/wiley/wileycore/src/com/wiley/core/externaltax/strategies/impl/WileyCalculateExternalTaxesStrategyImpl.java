/**
 *
 */
package com.wiley.core.externaltax.strategies.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.util.TaxValue;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.externaltax.WileyExternalTaxDocument;
import com.wiley.core.externaltax.WileyTaxGateway;
import com.wiley.core.externaltax.dto.TaxCalculationRequestDto;
import com.wiley.core.externaltax.dto.TaxCalculationResponseDto;
import com.wiley.core.externaltax.dto.TaxCalculationResponseEntryDto;
import com.wiley.core.externaltax.dto.TaxValueDto;
import com.wiley.core.externaltax.strategies.WileyCalculateExternalTaxesStrategy;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


/**
 * Service responsible for call to external tax calculation service
 */
public class WileyCalculateExternalTaxesStrategyImpl implements WileyCalculateExternalTaxesStrategy
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyCalculateExternalTaxesStrategyImpl.class);

	private static final boolean IS_TAX_ABSOLUTE = true;

	private WileyTaxGateway taxGateway;

	private Converter<AbstractOrderModel, TaxCalculationRequestDto> taxCalculationRequestDtoConverter;


	@Override
	public WileyExternalTaxDocument calculateExternalTaxes(final AbstractOrderModel orderModel)
	{
		validateParameterNotNull(orderModel, "Order is null. Cannot apply external tax to it.");
		validateParameterNotNull(orderModel.getCurrency(), "Order currency can't be null.");
		validateParameterNotNull(orderModel.getEntries(), "Order entries can't be null.");

		final TaxCalculationRequestDto taxCalculationRequestDto = taxCalculationRequestDtoConverter.convert(orderModel);
		final TaxCalculationResponseDto taxCalculationResponseDto = taxGateway.calculateTax(taxCalculationRequestDto);

		return convertTaxCalculationResponse(taxCalculationResponseDto, orderModel);
	}

	private WileyExternalTaxDocument convertTaxCalculationResponse(final TaxCalculationResponseDto taxCalculationResponseDto,
			final AbstractOrderModel orderModel)
	{
		final WileyExternalTaxDocument externalTaxDocument = new WileyExternalTaxDocument();
		for (TaxCalculationResponseEntryDto entryDto : taxCalculationResponseDto.getEntries())
		{
			externalTaxDocument.setTaxesForOrderEntry(entryDto.getId(), convertTaxValues(entryDto.getTaxes(), orderModel));
		}
		externalTaxDocument.setHandlingCostTaxes(convertTaxValues(taxCalculationResponseDto.getHandlingTaxes(), orderModel));
		externalTaxDocument.setShippingCostTaxes(convertTaxValues(taxCalculationResponseDto.getShippingTaxes(), orderModel));

		return externalTaxDocument;
	}

	private List<TaxValue> convertTaxValues(final List<TaxValueDto> externalTaxValues, final AbstractOrderModel abstractOrder)
	{
		return externalTaxValues
				.stream()
				.map(externalTaxValue -> createTaxValue(externalTaxValue, abstractOrder.getCurrency().getIsocode()))
				.collect(Collectors.toList());
	}

	private TaxValue createTaxValue(final TaxValueDto externalTaxValue, final String currencyIsoCode)
	{
		final Double taxValue = externalTaxValue.getValue();
		return new TaxValue(externalTaxValue.getCode(), taxValue, IS_TAX_ABSOLUTE, taxValue, currencyIsoCode);
	}

	@Required
	public void setTaxGateway(final WileyTaxGateway taxGateway)
	{
		this.taxGateway = taxGateway;
	}

	@Required
	public void setTaxCalculationRequestDtoConverter(
			final Converter<AbstractOrderModel, TaxCalculationRequestDto> taxCalculationRequestDtoConverter)
	{
		this.taxCalculationRequestDtoConverter = taxCalculationRequestDtoConverter;
	}
}
