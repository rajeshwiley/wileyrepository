package com.wiley.core.sync.impl;

import com.wiley.core.sync.WileySynchronizationDao;
import com.wiley.core.sync.WileySynchronizationService;
import de.hybris.platform.catalog.jalo.synchronization.CatalogVersionSyncCronJob;
import de.hybris.platform.catalog.jalo.synchronization.CatalogVersionSyncJob;
import de.hybris.platform.catalog.model.synchronization.CatalogVersionSyncCronJobModel;
import de.hybris.platform.catalog.model.synchronization.CatalogVersionSyncJobModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.servicelayer.cronjob.CronJobService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;


public class WileySynchronizationServiceImpl implements WileySynchronizationService
{
	private static final Logger LOG = LoggerFactory.getLogger(WileySynchronizationServiceImpl.class);

	@Resource
	private ModelService modelService;
	@Resource
	private CronJobService cronJobService;
	@Resource
	private WileySynchronizationDao wileySynchronizationDao;

	@Override
	public CatalogVersionSyncJobModel getCatalogVersionSyncJob(final String code)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("code", code);

		final List<CatalogVersionSyncJobModel> syncJobs = wileySynchronizationDao.findCatalogVersionSyncJobs(code);
		ServicesUtil.validateIfSingleResult(syncJobs, CatalogVersionSyncJobModel.class, "code", code);

		return syncJobs.get(0);
	}

	@Override
	public CronJobResult performSynchronization(final Collection<? extends ItemModel> items,
			final CatalogVersionSyncJobModel syncJobModel)
	{
		if (items.isEmpty())
		{
			return CronJobResult.SUCCESS;
		}

		final CatalogVersionSyncJob syncJob = modelService.toPersistenceLayer(syncJobModel);
		final CatalogVersionSyncCronJob cronJob = (CatalogVersionSyncCronJob) syncJob.newExecution();

		items.stream().forEach(item -> cronJob.addPendingItem(item.getPk(), null));

		return performSynchronization(cronJob);
	}

	@Override
	public <T> List<T> getSynchronizationTargets(final T item)
	{
		return wileySynchronizationDao.findSynchronizationTargetsBySource(item);
	}


	private CronJobResult performSynchronization(final CatalogVersionSyncCronJob cronJob)
	{
		final CatalogVersionSyncCronJobModel cronJobModel = modelService.toModelLayer(cronJob);
		modelService.save(cronJobModel);
		cronJobService.performCronJob(cronJobModel, true);
		return cronJobModel.getResult();
	}

}
