package com.wiley.core.mpgs.dto.json;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true)
public class Order
{
	private String amount;
	private String currency;
	private String status;

	public String getAmount()
	{
		return amount;
	}

	public void setAmount(final String amount)
	{
		this.amount = amount;
	}

	public String getCurrency()
	{
		return currency;
	}

	public void setCurrency(final String currency)
	{
		this.currency = currency;
	}

	public String getStatus() { return status; }

	public void setStatus(final String status) { this.status = status; }
}
