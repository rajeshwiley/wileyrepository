package com.wiley.core.jalo;

import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.order.AbstractOrder;
import de.hybris.platform.jalo.order.delivery.JaloDeliveryModeException;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.util.PriceValue;


public class ExternalDeliveryMode extends GeneratedExternalDeliveryMode
{

	@Override
	protected Item createItem(final SessionContext ctx, final ComposedType type, final ItemAttributeMap allAttributes)
			throws JaloBusinessException
	{
		// business code placed here will be executed before the item is created
		// then create the item
		final Item item = super.createItem(ctx, type, allAttributes);

		// business code placed here will be executed after the item was created
		// and return the item
		return item;
	}

	public PriceValue getCost(final SessionContext ctx, final AbstractOrder order) throws JaloDeliveryModeException
	{
		return new PriceValue(order.getCurrency().getIsoCode(), this.getCostValue(), order.isNet().booleanValue());
	}

	public PriceValue getCost(final AbstractOrder order)
	{
		try
		{
			return this.getCost(this.getSession().getSessionContext(), order);
		}
		catch (final JaloDeliveryModeException e)
		{
			throw new RuntimeException(String.format("Couldn't load costs for order [%s]", order.getCode()));
		}
	}
}
