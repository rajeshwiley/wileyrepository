package com.wiley.core.pages.interceptor;

import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;
import de.hybris.platform.servicelayer.interceptor.impl.LocalizedMessageAwareValidator;

import java.util.Collection;
import java.util.Collections;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.wiley.core.model.WileyPartnerPageModel;
import com.wiley.core.pages.WileyCMSPageService;

import static com.wiley.core.constants.WileyCoreConstants.DEFAULT_CATEGORY_CODE;


public class WileyPartnerPageValidateInterceptor extends LocalizedMessageAwareValidator
		implements ValidateInterceptor<WileyPartnerPageModel>
{
	@Resource(name = "wileyCMSPageService")
	private WileyCMSPageService wileyCMSPageService;

	@Override
	public void onValidate(final WileyPartnerPageModel wileyPartnerPageModel, final InterceptorContext interceptorContext)
			throws InterceptorException
	{
		final String partnerCategoryCode = wileyPartnerPageModel.getPartnerCategoryCode();
		if (StringUtils.isEmpty(partnerCategoryCode))
		{
			throw new InterceptorException(this.getLocalizedMessage("mandatoryattributesvalidator.missing",
					WileyPartnerPageModel.PARTNERCATEGORYCODE, WileyPartnerPageModel._TYPECODE, wileyPartnerPageModel));
		}

		if (DEFAULT_CATEGORY_CODE.equals(partnerCategoryCode))
		{
			return;
		}

		final Collection<AbstractPageModel> pages =
				wileyCMSPageService.findAllPagesByCategoryCode(WileyPartnerPageModel._TYPECODE,
						Collections.singleton(wileyPartnerPageModel.getCatalogVersion()), partnerCategoryCode);
		if (CollectionUtils.isNotEmpty(pages) && pages.stream().noneMatch(r -> wileyPartnerPageModel.equals(r)))
		{
			throw new InterceptorException(this.getLocalizedMessage("ambiguous.unique.key",
					WileyPartnerPageModel.PARTNERCATEGORYCODE, WileyPartnerPageModel._TYPECODE, pages.size()));
		}
	}
}
