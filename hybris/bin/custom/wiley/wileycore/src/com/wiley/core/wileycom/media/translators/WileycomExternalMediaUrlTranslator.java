package com.wiley.core.wileycom.media.translators;

import com.wiley.core.wileycom.valuetranslator.AbstractWileycomAllowEmptySpecialValueTranslator;
import com.wiley.core.wileycom.valuetranslator.AbstractWileycomSpecialValueTranslator;


/**
 * Implementation of {@link AbstractWileycomSpecialValueTranslator} to resolve external media URL
 * using ISBN. Import logic is delegated to
 * {@link com.wiley.core.wileycom.media.translators.adapter.impl.WileycomExternalMediaUrlImportAdapterImpl}
 *
 * @author Dzmitryi_Halahayeu
 */
public class WileycomExternalMediaUrlTranslator extends AbstractWileycomAllowEmptySpecialValueTranslator
{
	private static final String DEFAULT_IMPORT_ADAPTER_NAME = "wileycomExternalMediaUrlImportAdapter";

	@Override
	protected String getDefaultImportAdapterName()
	{
		return DEFAULT_IMPORT_ADAPTER_NAME;
	}

}
