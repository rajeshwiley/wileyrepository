package com.wiley.core.util;

import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.validation.exceptions.ConfigurableHybrisConstraintViolation;
import de.hybris.platform.validation.exceptions.HybrisConstraintViolation;
import de.hybris.platform.validation.services.impl.DefaultConstraintViolationFactory;
import de.hybris.platform.validation.services.impl.DefaultHybrisConstraintViolation;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Locale;

import javax.validation.ConstraintViolation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wiley.core.validation.service.impl.WileyLocalizedHybrisConstraintViolation;
import com.wiley.core.validation.violation.WileyHybrisConstraintViolation;


/**
 * This factory overridden to fix hybris-bug:
 * when implemented multi-language validation
 * (https://wiki.hybris.com/display/release5/How+to+Validate+Data+For+Multiple+Languages)
 * hybris doen't support custom error message showing. Only default message taken from .properties
 * It happens because multi-language valudation is executed without itemModel in context,
 * however it requires to read localized message from database.
 * See: LocalizedHybrisConstraintViolation.getLocalizedMessage().buildLocalizedMessage().buildConstraintModel()
 * At least hybris 5.7 has that bug.
 */
public class WileyConstraintViolationFactory extends DefaultConstraintViolationFactory
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyConstraintViolationFactory.class);

	private CommonI18NService commonI18NService;

	private void injectDependencies(final DefaultHybrisConstraintViolation violation)
	{
		violation.setModelService(modelService);
		violation.setTypeService(typeService);
		violation.setBundleProvider(bundleProvider);
		violation.setI18nService(i18nService);
	}

	@Override
	public ConfigurableHybrisConstraintViolation lookupViolation()
	{
		WileyHybrisConstraintViolation hybrisViolation = new WileyHybrisConstraintViolation();
		injectDependencies(hybrisViolation);
		return hybrisViolation;
	}

	@Override
	public HybrisConstraintViolation createLocalizedConstraintViolation(final ConstraintViolation violation, final Locale locale)
	{
		ConstraintViolation proxy = violation;
		if (needSubstitution(violation))
		{
			proxy = substituteItemInstance(violation);
		}
		WileyLocalizedHybrisConstraintViolation hybrisViolation = new WileyLocalizedHybrisConstraintViolation();
		this.injectDependencies(hybrisViolation);
		hybrisViolation.setConstraintViolation(proxy);
		hybrisViolation.setViolationLanguage(locale);
		hybrisViolation.setCommonI18NService(commonI18NService);
		return hybrisViolation;
	}

	private ConstraintViolation substituteItemInstance(final ConstraintViolation violation)
	{
		return (ConstraintViolation) Proxy.newProxyInstance(
				ConstraintViolation.class.getClassLoader(),
				new Class[] { ConstraintViolation.class },
				new ViolationWithInstanceProxy(violation));
	}

	private boolean needSubstitution(final ConstraintViolation violation)
	{
		return (violation.getLeafBean() == null) && (violation.getRootBeanClass() != null);
	}

	private static class ViolationWithInstanceProxy implements InvocationHandler
	{

		private static final String METHOD_NAME = "getLeafBean";
		private ConstraintViolation original;

		ViolationWithInstanceProxy(final ConstraintViolation original)
		{
			this.original = original;
		}

		@Override
		public Object invoke(final Object proxy, final Method method, final Object[] args) throws Throwable
		{
			if (METHOD_NAME.equals(method.getName()))
			{
				return createInstance();
			}
			return method.invoke(original, args);
		}

		private Object createInstance()
		{
			Object instance = null;
			try
			{
				instance = original.getRootBeanClass().newInstance();
			}
			catch (InstantiationException | IllegalAccessException ex)
			{
				LOG.error(String.format("Instantialtion of %s failed", original.getRootBeanClass().getName()), ex);
			}
			return instance;
		}
	}

	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}
}
