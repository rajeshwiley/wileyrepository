package com.wiley.core.validation.service;

import de.hybris.platform.core.model.c2l.CountryModel;


/**
 * This service contains a tax validation logic
 */
public interface WileyTaxValidationService
{
	boolean validateTaxInformation(CountryModel country, String taxNumber);
}
