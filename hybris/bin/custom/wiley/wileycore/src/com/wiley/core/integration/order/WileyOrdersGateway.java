package com.wiley.core.integration.order;


import de.hybris.platform.core.model.order.OrderModel;

import javax.annotation.Nonnull;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


/**
 * Gateway interface to publish order
 */
public interface WileyOrdersGateway
{
	/**
	 * Publishes created/modified/canceled order.<br/>
	 * Method requires existing transaction by <code>@Transactional(propagation = Propagation.MANDATORY)</code> annotation.
	 *
	 * @param newOrder
	 * 		new state of order
	 * @param previousOrderState
	 * 		previous state of order. can be null for new order
	 */
	@Transactional(propagation = Propagation.MANDATORY)
	void publish(@Nonnull OrderModel newOrder, String previousOrderState);
}
