package com.wiley.core.wileycom.customer.impl;

import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.UUID;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.exceptions.ExternalSystemException;
import com.wiley.core.integration.esb.EsbExternalAddressGateway;
import com.wiley.core.wileycom.customer.WileycomExternalAddressService;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


/**
 * Created by Uladzimir_Barouski on 7/11/2016.
 */
public class WileycomExternalAddressServiceImpl implements WileycomExternalAddressService
{

	protected static final Logger LOG = Logger.getLogger(WileycomExternalAddressServiceImpl.class);

	@Resource
	private EsbExternalAddressGateway esbExternalAddressGateway;

	@Resource
	private ModelService modelService;

	private CustomerAccountService customerAccountService;

	@Override
	public void addShippingAddress(@Nonnull final CustomerModel customer, @Nonnull final AddressModel address,
			@Nonnull final boolean makeThisAddressTheDefault)
	{
		validateParameterNotNullStandardMessage("customer", customer);
		validateParameterNotNullStandardMessage("address", address);
		addAddressToExternalSystem(customer, address);

		saveAddress(customer, address, makeThisAddressTheDefault);
	}

	@Override
	public void addBillingAddress(@Nonnull final CustomerModel customer, @Nonnull final AddressModel address,
			@Nonnull final boolean makeThisAddressTheDefault) throws ExternalSystemException, IllegalArgumentException
	{
		validateParameterNotNullStandardMessage("customer", customer);
		validateParameterNotNullStandardMessage("address", address);
		addAddressToExternalSystem(customer, address);
		modelService.save(address); // saving address due to external id will be generated.
	}

	@Override
	public void updateBillingAddress(@Nonnull final CustomerModel customer, @Nonnull final AddressModel address,
			final boolean makeThisAddressTheDefault)
	{
		validateParameterNotNullStandardMessage("customer", customer);
		validateParameterNotNullStandardMessage("address", address);

		updateAddressToExternalSystem(customer, address);
		// we don't expect any changes under models during sending date to external system, so saving is not required.
	}

	/**
	 * Sends address to ESB and always save address to db
	 *
	 * @param customer
	 * @param address
	 * @param makeThisAddressTheDefault
	 */
	@Override
	public void addAddressFromCheckout(@Nonnull final CustomerModel customer, @Nonnull final AddressModel address,
			final boolean makeThisAddressTheDefault)
	{
		validateParameterNotNullStandardMessage("customer", customer);
		validateParameterNotNullStandardMessage("address", address);
		try
		{
			addShippingAddress(customer, address, makeThisAddressTheDefault);
		}
		catch (ExternalSystemException | IllegalArgumentException e)
		{
			LOG.warn(String.format("External system could not process address for user %s", customer.getUid()), e);
			address.setVisibleInAddressBook(false);
			saveAddress(customer, address, false);
		}
	}

	protected void saveAddress(final CustomerModel customer, final AddressModel address, final boolean makeThisAddressTheDefault)
	{
		// Store the address against the user
		getCustomerAccountService().saveAddressEntry(customer, address);

		if (makeThisAddressTheDefault)
		{
			getCustomerAccountService().setDefaultAddressEntry(customer, address);
		}
	}


	protected CustomerAccountService getCustomerAccountService()
	{
		return customerAccountService;
	}

	@Override
	public void updateShippingAddress(@Nonnull final CustomerModel customer, @Nonnull final AddressModel address,
			@Nonnull final boolean makeThisAddressTheDefault)
	{
		validateParameterNotNullStandardMessage("customer", customer);
		validateParameterNotNullStandardMessage("address", address);

		updateAddressToExternalSystem(customer, address);

		saveAddress(customer, address, makeThisAddressTheDefault);
		// remove default address from customer model
		if (!makeThisAddressTheDefault && address.equals(customer.getDefaultShipmentAddress()))
		{
			getCustomerAccountService().clearDefaultAddressEntry(customer);
		}
	}

	@Override
	public void updateAddressFromCheckout(final CustomerModel customer, final AddressModel address,
			final boolean makeThisAddressTheDefault)
	{
		validateParameterNotNullStandardMessage("customer", customer);
		validateParameterNotNullStandardMessage("address", address);
		try
		{
			updateShippingAddress(customer, address, makeThisAddressTheDefault);
		}
		catch (ExternalSystemException | IllegalArgumentException e)
		{
			LOG.warn(String.format("External system could not process address for user %s", customer.getUid()), e);
			saveAddress(customer, address, makeThisAddressTheDefault);
		}
	}

	@Required
	public void setCustomerAccountService(final CustomerAccountService customerAccountService)
	{
		this.customerAccountService = customerAccountService;
	}


	@Override
	public void deleteAddress(final CustomerModel customer, final AddressModel address)
	{
		validateParams(customer, address);

		if (address.getShippingAddress())
		{
			esbExternalAddressGateway.deleteShippingAddress(address, customer);
		}
		if (address.getBillingAddress())
		{
			esbExternalAddressGateway.deleteBillingAddress(address, customer);
		}
	}


	protected void addAddressToExternalSystem(@Nonnull final CustomerModel customer, @Nonnull final AddressModel address)
	{
		if (StringUtils.isEmpty(customer.getCustomerID()))
		{
			throw new IllegalArgumentException("Customer ID cannot be empty!");
		}
		if (address.getVisibleInAddressBook())
		{
			address.setExternalId(UUID.randomUUID().toString());
			if (address.getShippingAddress())
			{
				esbExternalAddressGateway.addShippingAddress(address, customer);
			}
			if (address.getBillingAddress())
			{
				esbExternalAddressGateway.addBillingAddress(address, customer);
			}
		}
	}

	private void updateAddressToExternalSystem(final CustomerModel customer, final AddressModel address)
	{
		if (StringUtils.isEmpty(customer.getCustomerID()))
		{
			throw new IllegalArgumentException("Customer ID cannot be empty!");
		}
		if (address.getShippingAddress())
		{
			esbExternalAddressGateway.updateShippingAddress(address, customer);
		}
		if (address.getBillingAddress())
		{
			esbExternalAddressGateway.updateBillingAddress(address, customer);
		}
	}

	private void validateParams(final CustomerModel customer, final AddressModel address)
	{
		validateParameterNotNullStandardMessage("customer", customer);
		validateParameterNotNullStandardMessage("address", address);
		if (StringUtils.isEmpty(customer.getCustomerID()))
		{
			throw new IllegalArgumentException("Customer ID cannot be empty!");
		}
		if (StringUtils.isEmpty(address.getExternalId()))
		{
			throw new IllegalArgumentException("Address external ID cannot be empty!");
		}
	}
}
