package com.wiley.core.interceptors;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Objects;

import org.apache.log4j.Logger;
import org.quartz.CronExpression;

import com.wiley.core.model.EtTriggerModel;


public class WileyEtTriggerValidateInterceptor implements ValidateInterceptor<EtTriggerModel>
{

	private static final Logger LOG = Logger.getLogger(WileyEtTriggerValidateInterceptor.class);
	private static final String DATE_PATTERN = "MM/dd/yyyy hh:mm a";

	@Override
	public void onValidate(final EtTriggerModel triggerModel, final InterceptorContext interceptorContext)
			throws InterceptorException
	{
		if (isActivationTimeModified(triggerModel, interceptorContext) || isCronExpressionModified(triggerModel,
				interceptorContext))
		{
			try
			{
				Date etActivationTime = getEtActivationTime(triggerModel, interceptorContext);
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_PATTERN);
				String formattedDate = simpleDateFormat.format(etActivationTime);
				DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(DATE_PATTERN);
				LocalDateTime localDateTime = LocalDateTime.parse(formattedDate, dateTimeFormatter);
				ZonedDateTime zonedDateTime = localDateTime.atZone(ZoneId.of("America/New_York"));
				triggerModel.setActivationTime(Date.from(zonedDateTime.toInstant()));
			}
			catch (ParseException e)
			{
				LOG.error(e.getMessage(), e);
			}
		}
	}

	private boolean isCronExpressionModified(final EtTriggerModel triggerModel, final InterceptorContext interceptorContext)
	{
		return interceptorContext.isModified(
				triggerModel, EtTriggerModel.CRONEXPRESSION) && Objects.nonNull(triggerModel.getCronExpression());
	}

	private boolean isActivationTimeModified(final EtTriggerModel triggerModel, final InterceptorContext interceptorContext)
	{
		return interceptorContext.isModified(triggerModel, EtTriggerModel.ACTIVATIONTIME) && Objects.nonNull(
				triggerModel.getActivationTime());
	}

	private Date getEtActivationTime(final EtTriggerModel triggerModel, final InterceptorContext interceptorContext)
			throws ParseException
	{
		Date etActivationTime;
		if (isActivationTimeModified(triggerModel, interceptorContext))
		{
			etActivationTime = triggerModel.getActivationTime();
		}
		else
		{
			CronExpression cronExpression = new CronExpression(triggerModel.getCronExpression());
			etActivationTime = cronExpression.getNextValidTimeAfter(new Date());
		}
		return etActivationTime;
	}
}
