package com.wiley.core.wileyb2b.customer.impl;

import com.google.common.base.Preconditions;
import com.wiley.core.wileyb2b.customer.dao.WileyB2BCustomerAccountDao;
import com.wiley.core.wileycom.customer.service.impl.WileycomCustomerAccountServiceImpl;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.store.BaseStoreModel;


public class Wileyb2bCustomerAccountServiceImpl extends WileycomCustomerAccountServiceImpl
{
	@Override
	public SearchPageData<OrderModel> getOrderList(final CustomerModel customerModel, final BaseStoreModel store,
												   final OrderStatus[] status, final PageableData pageableData) {
		Preconditions.checkArgument(customerModel instanceof B2BCustomerModel, "Customer should be B2B customer");
		Preconditions.checkNotNull(store, "Store cannot be null");
		return ((WileyB2BCustomerAccountDao) getCustomerAccountDao())
				.findOrdersByUnitAndStore(((B2BCustomerModel) customerModel).getDefaultB2BUnit(), store, status, pageableData);
	}
}
