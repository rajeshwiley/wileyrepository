package com.wiley.core.event;

import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.servicelayer.event.ClusterAwareEvent;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.IndexedType;

import java.util.Collection;
import java.util.List;
import java.util.Set;


/**
 * Created by Raman_Hancharou on 4/11/2017.
 */
public class UpdatePartialTypeIndexEvent extends AbstractEvent implements ClusterAwareEvent
{
	private FacetSearchConfig facetSearchConfig;
	private IndexedType indexedType;
	private Collection<IndexedProperty> indexedProperties;
	private List<PK> pks;
	private Set<CountryModel> countries;

	public UpdatePartialTypeIndexEvent(final FacetSearchConfig facetSearchConfig, final IndexedType indexedType,
			final Collection<IndexedProperty> indexedProperties, final List<PK> pks, final Set<CountryModel> countries)
	{
		this.facetSearchConfig = facetSearchConfig;
		this.indexedType = indexedType;
		this.indexedProperties = indexedProperties;
		this.pks = pks;
		this.countries = countries;
	}

	@Override
	public boolean publish(final int i, final int i1)
	{
		return true;
	}

	public FacetSearchConfig getFacetSearchConfig()
	{
		return facetSearchConfig;
	}

	public void setFacetSearchConfig(final FacetSearchConfig facetSearchConfig)
	{
		this.facetSearchConfig = facetSearchConfig;
	}

	public IndexedType getIndexedType()
	{
		return indexedType;
	}

	public void setIndexedType(final IndexedType indexedType)
	{
		this.indexedType = indexedType;
	}

	public Collection<IndexedProperty> getIndexedProperties()
	{
		return indexedProperties;
	}

	public void setIndexedProperties(final Collection<IndexedProperty> indexedProperties)
	{
		this.indexedProperties = indexedProperties;
	}

	public List<PK> getPks()
	{
		return pks;
	}

	public void setPks(final List<PK> pks)
	{
		this.pks = pks;
	}

	public Set<CountryModel> getCountries()
	{
		return countries;
	}

	public void setCountries(final Set<CountryModel> countries)
	{
		this.countries = countries;
	}

}
