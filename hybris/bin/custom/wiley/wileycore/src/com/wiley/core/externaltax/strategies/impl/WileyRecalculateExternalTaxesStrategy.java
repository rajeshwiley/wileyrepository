/**
 *
 */
package com.wiley.core.externaltax.strategies.impl;

import de.hybris.platform.commerceservices.externaltax.RecalculateExternalTaxesStrategy;
import de.hybris.platform.commerceservices.order.CommerceCartHashCalculationStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceOrderParameter;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.session.SessionService;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;


/**
 *
 */
public class WileyRecalculateExternalTaxesStrategy implements RecalculateExternalTaxesStrategy
{
	private CommerceCartHashCalculationStrategy commerceCartHashCalculationStrategy;
	@Autowired
	private SessionService sessionService;

	@Override
	public boolean recalculate(final AbstractOrderModel abstractOrderModel)
	{

		final CommerceOrderParameter parameter = new CommerceOrderParameter();
		parameter.setOrder(abstractOrderModel);
		final String orderCalculationHash = commerceCartHashCalculationStrategy.buildHashForAbstractOrder(parameter);
		final String sessionHash = sessionService.getAttribute(SESSION_ATTIR_ORDER_RECALCULATION_HASH);
		boolean needsRecalculation = false;

		if (!StringUtils.isBlank(sessionHash) && !StringUtils.equals(sessionHash, orderCalculationHash))
		{
			needsRecalculation = true;
		}

		return needsRecalculation;
	}

	@Required
	public void setCommerceCartHashCalculationStrategy(
			final CommerceCartHashCalculationStrategy commerceCartHashCalculationStrategy)
	{
		this.commerceCartHashCalculationStrategy = commerceCartHashCalculationStrategy;
	}
}
