package com.wiley.core.wileycom.valuetranslator;

import de.hybris.platform.impex.jalo.header.SpecialColumnDescriptor;
import de.hybris.platform.jalo.Item;

import org.apache.commons.lang3.StringUtils;


/**
 * Implementation of {@link AbstractDefaultWileycomSpecialValueTranslator} that allows empty value for processing
 *
 * @author Dzmitryi_Halahayeu
 */
public abstract class AbstractWileycomAllowEmptySpecialValueTranslator extends AbstractDefaultWileycomSpecialValueTranslator
{
	/**
	 * Check should we process cell value or use default/skip
	 * This method override logic from {@link AbstractDefaultWileycomSpecialValueTranslator#isEmpty(String)}
	 * and return always false in case value if empty due to empty cell value is valid for this
	 * translator and we want to process it in such case
	 *
	 * @param cellValue
	 * 		the cell value
	 * @return If false {@link SpecialColumnDescriptor} will call  {@link #performImport(String, Item)}.
	 * If true will use {@link SpecialColumnDescriptor#getDefaultValueDefinition()} or skip value if not defined
	 */
	@Override
	public boolean isEmpty(final String cellValue)
	{
		if (StringUtils.isEmpty(cellValue))
		{
			return true;
		}
		return super.isEmpty(cellValue);
	}
}
