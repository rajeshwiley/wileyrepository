package com.wiley.core.mpgs.dto.json;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;



@JsonIgnoreProperties(ignoreUnknown = true)
public class Response
{
	private String gatewayCode;

	public String getGatewayCode()
	{
		return gatewayCode;
	}

	public void setGatewayCode(final String gatewayCode)
	{
		this.gatewayCode = gatewayCode;
	}
}
