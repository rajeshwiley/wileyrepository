package com.wiley.core.university.services.impl;


import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.List;
import java.util.Optional;

import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import com.wiley.core.model.UniversityModel;
import com.wiley.core.university.dao.UniversityDao;
import com.wiley.core.university.services.UniversityService;


/**
 * Default implementation of {@link UniversityService}.
 */
public class UniversityServiceImpl implements UniversityService
{

	private static final Logger LOG = LoggerFactory.getLogger(UniversityServiceImpl.class);


	private FlexibleSearchService flexibleSearchService;

	private UniversityDao universityDao;

	public Optional<UniversityModel> getUniversityByCode(@Nonnull final String universityCode)
	{
		Assert.notNull(universityCode, "Parameter universityCode cannot be null.");

		try
		{
			UniversityModel example = new UniversityModel();
			example.setCode(universityCode);
			UniversityModel universityModel = flexibleSearchService.getModelByExample(example);
			return Optional.of(universityModel);
		}
		catch (ModelNotFoundException e)
		{
			LOG.error(String.format("Error happened during search of university with code %s due to error of [%s]",
					universityCode, e.getMessage()));
			return Optional.empty();
		}
	}

	@Override
	public List<UniversityModel> getUniversitiesByRegion(@Nonnull final RegionModel region)
	{
		return universityDao.getActiveUniversitiesByRegion(region);
	}

	public void setUniversityDao(final UniversityDao universityDao)
	{
		this.universityDao = universityDao;
	}

	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}
}
