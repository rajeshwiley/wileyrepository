package com.wiley.core.wileyb2c.subscription;

import de.hybris.platform.jalo.order.price.PriceFactory;
import de.hybris.platform.subscriptionservices.subscription.impl.FindSubscriptionPricingWithCurrentPriceFactoryStrategy;


/**
 *
 */
public class Wileyb2cFindSubscriptionPricingWithCurrentPriceFactoryStrategy
		extends FindSubscriptionPricingWithCurrentPriceFactoryStrategy
{

	private PriceFactory priceFactory;

	@Override
	public PriceFactory getCurrentPriceFactory()
	{
		return priceFactory;
	}

	public PriceFactory getPriceFactory()
	{
		return priceFactory;
	}

	public void setPriceFactory(final PriceFactory priceFactory)
	{
		this.priceFactory = priceFactory;
	}
}
