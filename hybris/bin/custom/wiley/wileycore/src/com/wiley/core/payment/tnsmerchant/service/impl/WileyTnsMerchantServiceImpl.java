package com.wiley.core.payment.tnsmerchant.service.impl;

import java.util.List;

import javax.annotation.Resource;

import com.wiley.core.model.WileyTnsMerchantModel;
import com.wiley.core.payment.tnsmerchant.dao.WileyTnsMerchantDao;
import com.wiley.core.payment.tnsmerchant.service.WileyTnsMerchantService;

import static org.apache.commons.collections4.CollectionUtils.isEmpty;


public class WileyTnsMerchantServiceImpl implements WileyTnsMerchantService
{
	@Resource
	private WileyTnsMerchantDao wileyTnsMerchantDao;

	@Override
	public WileyTnsMerchantModel getTnsMerchant(final String countryIsoCode, final String currencyIsoCode)
	{
		final List<WileyTnsMerchantModel> tnsMerchants = wileyTnsMerchantDao.findTnsMerchant(countryIsoCode, currencyIsoCode);

		if (isEmpty(tnsMerchants))
		{
			final String errorMessage = String.format(
					"TnsMerchant not found for country with isocode: %s and currency with isocode: %s", countryIsoCode,
					currencyIsoCode);
			throw new IllegalStateException(errorMessage);
		}

		return tnsMerchants.get(0);
	}
}
