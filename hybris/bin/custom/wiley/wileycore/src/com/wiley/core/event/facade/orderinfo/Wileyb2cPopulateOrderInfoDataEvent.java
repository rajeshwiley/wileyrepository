package com.wiley.core.event.facade.orderinfo;

import com.wiley.core.event.facade.WileyOneObjectFacadeEvent;
import de.hybris.platform.acceleratorservices.payment.data.OrderInfoData;

/**
 * Created by Georgii_Gavrysh on 12/9/2016.
 */
public class Wileyb2cPopulateOrderInfoDataEvent extends WileyOneObjectFacadeEvent<OrderInfoData> {
    /**
     * Create a new ApplicationEvent.
     *
     * @param source the object on which the event initially occurred (never {@code null})
     */
    public Wileyb2cPopulateOrderInfoDataEvent(final OrderInfoData source) {
        super(source);
    }
}
