package com.wiley.core.mpgs.services.strategies.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;

import org.springframework.beans.factory.annotation.Value;

import com.wiley.core.model.WileyTnsMerchantModel;
import com.wiley.core.mpgs.services.strategies.WileyMPGSMerchantIdStrategy;

import static com.wiley.core.constants.WileyCoreConstants.DEFAULT_MERCHANT_ID_PROP;


public class WileyMPGSMerchantIdStrategyImpl extends AbstractMPGSMerchantIdStrategy implements WileyMPGSMerchantIdStrategy
{
	private String defaultMerchantId;

	@Override
	public String getMerchantID(final AbstractOrderModel abstractOrderModel)
	{
		return defaultMerchantId;
	}

	@Override
	public WileyTnsMerchantModel getMerchant(final AbstractOrderModel abstractOrderModel)
	{
		WileyTnsMerchantModel merchant = new WileyTnsMerchantModel();
		merchant.setId(defaultMerchantId);
		merchant.setEnableVerification(true);
		return merchant;
	}

	@Value("${" + DEFAULT_MERCHANT_ID_PROP + "}")
	public void setDefaultMerchantId(final String defaultMerchantId)
	{
		this.defaultMerchantId = defaultMerchantId;
	}
}
