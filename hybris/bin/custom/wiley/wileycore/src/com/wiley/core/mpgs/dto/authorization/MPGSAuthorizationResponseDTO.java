package com.wiley.core.mpgs.dto.authorization;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.wiley.core.mpgs.dto.json.Error;
import com.wiley.core.mpgs.dto.json.Order;
import com.wiley.core.mpgs.dto.json.Response;
import com.wiley.core.mpgs.dto.json.authorization.SourceOfFunds;
import com.wiley.core.mpgs.dto.json.Transaction;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MPGSAuthorizationResponseDTO
{
	private String result;
	private Order order;
	private SourceOfFunds sourceOfFunds;
	private Transaction transaction;
	private Error error;
	private Response response;
	private String timeOfRecord;

	public String getResult()
	{
		return result;
	}

	public void setResult(final String result)
	{
		this.result = result;
	}

	public Order getOrder()
	{
		return order;
	}

	public void setOrder(final Order order)
	{
		this.order = order;
	}

	public SourceOfFunds getSourceOfFunds()
	{
		return sourceOfFunds;
	}

	public void setSourceOfFunds(final SourceOfFunds sourceOfFunds)
	{
		this.sourceOfFunds = sourceOfFunds;
	}

	public Transaction getTransaction()
	{
		return transaction;
	}

	public void setTransaction(final Transaction transaction)
	{
		this.transaction = transaction;
	}

	public Error getError()
	{
		return error;
	}

	public void setError(final Error error)
	{
		this.error = error;
	}

	public Response getResponse()
	{
		return response;
	}

	public void setResponse(final Response response)
	{
		this.response = response;
	}

	public String getTimeOfRecord()
	{
		return timeOfRecord;
	}

	public void setTimeOfRecord(final String timeOfRecord)
	{
		this.timeOfRecord = timeOfRecord;
	}
}
