package com.wiley.core.email.service;

import de.hybris.platform.acceleratorservices.email.impl.DefaultEmailService;
import de.hybris.platform.acceleratorservices.model.email.EmailAddressModel;
import de.hybris.platform.acceleratorservices.model.email.EmailAttachmentModel;
import de.hybris.platform.acceleratorservices.model.email.EmailMessageModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.core.GenericSearchConstants.LOG;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;
import java.util.Date;
import java.util.List;


/**
 * Service to create and send emails.
 */
public class WileyDefaultEmailService extends DefaultEmailService {

    private static final Logger LOG = Logger.getLogger(WileyDefaultEmailService.class);

    @Autowired
    private ConfigurationService configurationService;

    @Override
    public boolean send(final EmailMessageModel message) {
        if (message == null) {
            throw new IllegalArgumentException("message must not be null");
        }
        final boolean sendEnabled = getConfigurationService().getConfiguration()
                .getBoolean(EMAILSERVICE_SEND_ENABLED_CONFIG_KEY, true);
        if (sendEnabled) {
            try {
                final HtmlEmail email = getPerConfiguredEmail();
                email.setCharset("UTF-8");

                final List<EmailAddressModel> toAddresses = message.getToAddresses();
                setAddresses(message, email, toAddresses);

                final String recipient = configurationService.getConfiguration().getString("wiley.cc.email");
                email.setCc(Collections.singletonList(new InternetAddress(recipient)));

                final EmailAddressModel fromAddress = message.getFromAddress();
                email.setFrom(fromAddress.getEmailAddress(), nullifyEmpty(fromAddress.getDisplayName()));

                addReplyTo(message, email);

                email.setSubject(message.getSubject());
                email.setHtmlMsg(getBody(message));

                // To support plain text parts use email.setTextMsg()

                final List<EmailAttachmentModel> attachments = message.getAttachments();
                if (!processAttachmentsSuccessful(email, attachments)) {
                    return false;
                }

                // Important to log all emails sent out
                LOG.info("Wiley Sending Email [" + message.getPk() + "] To ["
                        + convertToStrings(toAddresses) + "] From [" + fromAddress.getEmailAddress()
                        + "] Subject [" + email.getSubject() + "]");

                // Send the email and capture the message ID
                final String messageID = email.send();

                message.setSent(true);
                message.setSentMessageID(messageID);
                message.setSentDate(new Date());
                getModelService().save(message);

                return true;
            } catch (final EmailException | AddressException e) {
                logInfo(message, (EmailException) e);
            }
        } else {
            LOG.warn("Could not send e-mail pk [" + message.getPk() + "] subject [" + message.getSubject()
                    + "]");
            LOG.info(
                    "Email sending has been disabled. Check the config property 'emailservice.send.enabled'");
            return true;
        }

        return false;
    }
}
