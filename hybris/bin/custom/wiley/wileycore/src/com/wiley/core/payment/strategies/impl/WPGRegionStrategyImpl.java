package com.wiley.core.payment.strategies.impl;


import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.wiley.core.payment.strategies.WPGRegionStrategy;


public class WPGRegionStrategyImpl implements WPGRegionStrategy
{
	private Map<String, String> regionsByCurrency;

	@Override
	public String getRegionByCurrency(final String currency)
	{
		String region = regionsByCurrency.get(currency);
		if (region == null)
		{
			region = StringUtils.EMPTY;
		}
		return region;
	}

	public Map<String, String> getRegionsByCurrency()
	{
		return regionsByCurrency;
	}

	public void setRegionsByCurrency(final Map<String, String> regionsByCurrency)
	{
		this.regionsByCurrency = regionsByCurrency;
	}
}
