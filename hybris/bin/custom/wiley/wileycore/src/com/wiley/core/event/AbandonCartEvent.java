package com.wiley.core.event;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;

import javax.annotation.Nonnull;



/**
 * Abandon cart event.
 */
public class AbandonCartEvent extends AbstractEvent
{
	private CartModel cart;

	public AbandonCartEvent(@Nonnull final CartModel cartModel)
	{
		this.cart = cartModel;
	}

	public CartModel getCart()
	{
		return cart;
	}

	public void setCart(final CartModel cart)
	{
		this.cart = cart;
	}
}
