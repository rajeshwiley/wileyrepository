package com.wiley.core.event;

import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;

import javax.annotation.Resource;

import com.wiley.core.wileyb2b.unit.WileyB2BUnitService;


public class B2BUnitActiveUpdatedListener extends AbstractEventListener<B2BUnitActiveUpdatedEvent>
{
	@Resource
	private WileyB2BUnitService wileyB2BUnitService;

	@Override
	protected void onEvent(final B2BUnitActiveUpdatedEvent b2BUnitActiveUpdatedEvent)
	{
		wileyB2BUnitService.handleB2BUnitActiveStatusUpdated(b2BUnitActiveUpdatedEvent.getSapAccountNumber());
	}
}
