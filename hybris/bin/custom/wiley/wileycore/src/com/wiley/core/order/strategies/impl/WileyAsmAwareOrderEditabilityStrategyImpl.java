package com.wiley.core.order.strategies.impl;

import de.hybris.platform.assistedserviceservices.AssistedServiceService;
import de.hybris.platform.core.model.order.OrderModel;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.order.strategies.AbstractOrderModificationStrategy;
import com.wiley.core.order.strategies.WileyOrderEditabilityStrategy;


public class WileyAsmAwareOrderEditabilityStrategyImpl extends AbstractOrderModificationStrategy
		implements WileyOrderEditabilityStrategy
{

	@Resource(name = "wileyAsmOrderEditabilityStrategy")
	private WileyOrderEditabilityStrategy wileyAsmOrderEditabilityStrategy;
	@Resource(name = "wileyDefaultOrderEditabilityStrategy")
	private WileyOrderEditabilityStrategy wileyDefaultOrderEditabilityStrategy;

	@Autowired
	private AssistedServiceService assistedServiceService;

	@Override
	public boolean isEditable(final OrderModel order)
	{
		if (isAsmSession())
		{
			return wileyAsmOrderEditabilityStrategy.isEditable(order);
		}
		else
		{
			return wileyDefaultOrderEditabilityStrategy.isEditable(order);
		}
	}

	@Override
	public boolean hasAllEditableEntries(final OrderModel order)
	{
		if (isAsmSession())
		{
			return wileyAsmOrderEditabilityStrategy.hasAllEditableEntries(order);
		}
		else
		{
			return wileyDefaultOrderEditabilityStrategy.hasAllEditableEntries(order);
		}
	}

	/**
	 * [OOTB code duplication] Source: {de.hybris.platform.assistedservicefacades.impl.DefaultAssistedServiceFacade}
	 *
	 * Changes made: no changes, we don't have an access to facades from core layer
	 */
	private boolean isAsmSession()
	{
		return assistedServiceService.getAsmSession() != null;
	}

}
