package com.wiley.core.order.impl;

import com.wiley.core.enums.ProductEditionFormat;
import com.wiley.core.model.WileyProductVariantSetModel;
import com.wiley.core.order.WileyFindProductsInCartService;
import com.wiley.core.order.WileyMultiDimensionalCommerceCartService;
import com.wiley.core.order.WileyProductQuantityInCartManager;
import com.wiley.core.product.WileyProductService;
import com.wiley.core.product.WileyProductVariantSetInformation;
import com.wiley.core.product.WileyProductVariantSetService;
import com.wiley.core.wiley.order.impl.WileyCommerceCartServiceImpl;
import de.hybris.platform.commerceservices.order.CommerceCartMergingException;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartModificationStatus;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.variants.model.GenericVariantProductModel;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


/**
 * Default implementation of {@link WileyMultiDimensionalCommerceCartService}
 */
public class DefaultWileyMultiDimensionalCommerceCartService extends WileyCommerceCartServiceImpl implements
		WileyMultiDimensionalCommerceCartService
{

	private static final Logger LOG = Logger.getLogger(DefaultWileyMultiDimensionalCommerceCartService.class);

	public static final int DEFAULT_QUANTITY = 1;

	@Resource
	private WileyProductVariantSetService wileyProductVariantSetService;

	@Resource
	private WileyFindProductsInCartService wileyFindProductsInCartService;

	@Resource
	private WileyProductService productService;

	@Resource
	private CartService cartService;

	@Resource
	private WileyProductQuantityInCartManager welProductQuantityInCartManager;

	@Override
	@Transactional(rollbackFor = CommerceCartModificationException.class)
	@Nonnull
	public List<CommerceCartModification> addToCart(@Nonnull final CartModel cart,
			@Nullable final List<CommerceCartParameter> parameters) throws CommerceCartModificationException
	{
		ServicesUtil.validateParameterNotNullStandardMessage("cart", cart);

		if (CollectionUtils.isEmpty(parameters))
		{
			return Collections.emptyList();
		}

		if (LOG.isDebugEnabled())
		{
			StringBuilder sb = new StringBuilder();
			sb.append(String.format("Cart : [%s]\n", cart));
			sb.append("CommerceCart parameters:\n");
			parameters.forEach(param -> sb.append(String.format("Param [%s]: Product [%s], Quantity [%s]\n", param,
					param.getProduct(), param.getQuantity())));
			LOG.debug(sb.toString());
		}

		// Make sure that CommerceCartParameters will affect the same cart
		parameters.forEach(param -> param.setCart(cart));

		List<CommerceCartModification> productModificationList = new ArrayList<>(parameters.size());

		// Adding products to cart
		for (CommerceCartParameter param : parameters)
		{
			// keep quantity for allowing sets to be combined
			productModificationList.add(welProductQuantityInCartManager.addToCart(param,
					false)); // throws CommerceCartModificationException
		}

		// Collecting products to product set
		if (isAddedAtLeastOneProductWileyVariantProduct(productModificationList))
		{
			this.collectCartEntriesToSet(cart, productModificationList); // throws CommerceCartModificationException
		}
		else if (LOG.isDebugEnabled())
		{
			LOG.debug("Skipped collecting cart entries to set because there were no new added products");
		}

		// make sure that from PDP customer can add only one product.
		// if product already exists in cart, we don't change the quantity.
		// if product didn't exist in cart before, we allow to add product to cart with requested quantity.
		keepQuantityForEntriesInCartOrAllowToAddIfProductDidNotExist(cart, productModificationList);

		// if at least one product was added to cart.
		if (CollectionUtils.isNotEmpty(productModificationList))
		{
			welProductQuantityInCartManager.checkAndUpdateQuantityIfRequired(cart, productModificationList);
		}

		return productModificationList;
	}

	private void keepQuantityForEntriesInCartOrAllowToAddIfProductDidNotExist(final CartModel cart,
			final List<CommerceCartModification> productModificationList) throws CommerceCartModificationException
	{
		// added test case which cover gap in check against adding more then one product which was not in cart originally
		for (CommerceCartModification modification : productModificationList)
		{
			final AbstractOrderEntryModel entry = modification.getEntry();

			// works only for non-physical products
			if (!ProductEditionFormat.PHYSICAL.equals(entry.getProduct().getEditionFormat()))
			{
				final long quantity = ObjectUtils.defaultIfNull(entry.getQuantity(), Long.valueOf(0L));
				final long quantityAdded = modification.getQuantityAdded();

				if (quantity > 0 && quantityAdded > 0 && quantity - quantityAdded > 0)
				{
					final CommerceCartParameter parameter = new CommerceCartParameter();
					parameter.setEnableHooks(true);
					parameter.setCart(cart);
					parameter.setEntryNumber(entry.getEntryNumber());
					parameter.setQuantity(quantity - quantityAdded);
					getCommerceUpdateCartEntryStrategy().updateQuantityForCartEntry(
							parameter); // throws CommerceCartModificationException
				}
			}
		}
	}

	@Override
	@Transactional(rollbackFor = CommerceCartMergingException.class)
	public void mergeCarts(final CartModel fromCart, final CartModel toCart, final List<CommerceCartModification> modifications)
			throws CommerceCartMergingException
	{
		super.mergeCarts(fromCart, toCart, modifications);

		try
		{
			collectCartEntriesToSet(toCart, modifications);
		}
		catch (CommerceCartModificationException e)
		{
			LOG.error(e.getMessage(), e);
			throw new CommerceCartMergingException(e.getMessage());
		}

		welProductQuantityInCartManager.checkAndUpdateQuantityIfRequired(toCart, modifications);


	}

	@Nonnull
	List<CommerceCartModification> collectCartEntriesToSet(@Nonnull final CartModel cart,
			@Nonnull final List<CommerceCartModification> cartModificationsToUpdate) throws CommerceCartModificationException
	{
		ServicesUtil.validateParameterNotNullStandardMessage("cart", cart);
		ServicesUtil.validateParameterNotNullStandardMessage("cartModificationsToUpdate", cartModificationsToUpdate);

		List<CommerceCartModification> commerceCartModificationList = new ArrayList<>();

		Set<ProductModel> productsInCart = getProductsWhichCanBePartOfSet(cart);

		// find potential sets.
		final List<WileyProductVariantSetInformation> sets = getWileyProductVariantSetService()
				.getWileyProductSetsByProducts(productsInCart);

		if (LOG.isDebugEnabled())
		{
			StringBuilder sb = new StringBuilder();
			sb.append("Potential Product Sets:\n");
			sets.forEach(setInfo -> sb.append(String.format("Product set: [%s], included products [%s]\n",
					setInfo.getProductSet(), setInfo.getIncludedProducts())));
			LOG.debug(sb.toString());
		}

		boolean first = true;
		for (WileyProductVariantSetInformation setInfo : sets)
		{
			if (first)
			{
				// if this is the first product set we apply it without and additional checks
				commerceCartModificationList.add(addProductSetInsteadOfSeparateProducts(cart, setInfo,
						cartModificationsToUpdate));
				first = false;
			}
			else
			{
				// here we check if there is another set which can be applied to remaining products

				// getting products of set
				final List<GenericVariantProductModel> includedProducts = setInfo.getIncludedProducts();

				// getting remaining products which can be part of set from cart
				final Set<ProductModel> remainingProductsWhichCanBePartOfSet = getProductsWhichCanBePartOfSet(cart);

				// if there are no remaining products we can stop trying to apply product sets
				if (CollectionUtils.isEmpty(remainingProductsWhichCanBePartOfSet))
				{
					if (LOG.isDebugEnabled())
					{
						LOG.debug("There are no remaining products. Stopping adding product sets.");
					}
					break;
				}

				// if there are products which can be collected to product set
				if (remainingProductsWhichCanBePartOfSet.containsAll(includedProducts))
				{
					commerceCartModificationList.add(addProductSetInsteadOfSeparateProducts(cart, setInfo,
							cartModificationsToUpdate));
				}
			}
		}

		return commerceCartModificationList;
	}

	private CommerceCartModification addIfNotExistsInCart(final CommerceCartParameter parameter)
			throws CommerceCartModificationException
	{
		ServicesUtil.validateParameterNotNullStandardMessage("parameter", parameter);

		final ProductModel product = parameter.getProduct();
		final CartModel cart = parameter.getCart();

		ServicesUtil.validateParameterNotNullStandardMessage("product", product);
		ServicesUtil.validateParameterNotNullStandardMessage("cart", cart);

		// Check if product already exists in cart.
		final AbstractOrderEntryModel orderEntry = getOrderEntry(cart, product);
		if (orderEntry != null)
		{
			if (LOG.isDebugEnabled())
			{
				LOG.debug(String.format("Product [%s] already exists in cart [%s]. Skipping adding.", product, cart));
			}

			// Create the card modification result
			CommerceCartModification modification = new CommerceCartModification();
			modification.setQuantityAdded(0);
			modification.setQuantity(orderEntry.getQuantity());
			modification.setStatusCode(CommerceCartModificationStatus.SUCCESS);
			modification.setEntry(orderEntry);
			return modification;
		}

		return this.addToCart(parameter);
	}

	private CommerceCartModification addIfNotExistsInCart(final CartModel cartModel, final WileyProductVariantSetModel set)
			throws CommerceCartModificationException
	{
		CommerceCartParameter parameter = createCommerceCartParameter(cartModel, set);
		return this.addIfNotExistsInCart(parameter);
	}

	private AbstractOrderEntryModel getOrderEntry(final CartModel cart, final ProductModel product)
	{
		final List<AbstractOrderEntryModel> entries = cart.getEntries();
		return entries.stream()
				.filter(entry -> product.equals(entry.getProduct()))
				.findFirst()
				.orElse(null);
	}

	private boolean isAddedAtLeastOneProductWileyVariantProduct(final List<CommerceCartModification> cartModifications)
	{
		return cartModifications.stream()
				.filter(modification -> modification.getQuantityAdded() > 0)
				.filter(modification ->
						Boolean.TRUE.equals(getProductService().canBePartOfProductSet(modification.getEntry().getProduct())))
				.findFirst()
				.isPresent();
	}

	private CommerceCartModification addProductSetInsteadOfSeparateProducts(final CartModel cartModel,
			final WileyProductVariantSetInformation setInfo, final List<CommerceCartModification> cartModificationsToUpdate)
			throws CommerceCartModificationException
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug(String.format("Trying to add product set [%s] to cart [%s].", setInfo.getProductSet(), cartModel));
		}

		CommerceCartModification setCartModification = addIfNotExistsInCart(cartModel, setInfo.getProductSet());

		// if Product Set was added successfully we can remove appropriate cart entries.
		if (CommerceCartModificationStatus.SUCCESS.equals(setCartModification.getStatusCode()))
		{
			// finding entry numbers which will be removed.
			final List<AbstractOrderEntryModel> entries = wileyFindProductsInCartService.getEntriesForProducts(cartModel,
					new HashSet<>(setInfo.getIncludedProducts()));
			updateQuantityOfCartEntries(cartModel, entries, cartModificationsToUpdate);
		}
		else
		{
			throw new CommerceCartModificationException(String.format("Product Set [%s] was not added to cart [%s]. Status code"
							+ " [%s].", setInfo.getProductSet(),
					cartModel, setCartModification.getStatusCode()));
		}
		return setCartModification;
	}

	private Set<ProductModel> getProductsWhichCanBePartOfSet(final CartModel cart)
	{
		final List<AbstractOrderEntryModel> entries = cart.getEntries();
		if (CollectionUtils.isNotEmpty(entries))
		{
			return entries.stream()
					.map(AbstractOrderEntryModel::getProduct)
					.filter(productModel -> getProductService().canBePartOfProductSet(productModel))
					.collect(Collectors.toSet());
		}
		return Collections.emptySet();
	}

	private void updateQuantityOfCartEntries(final CartModel cart, final List<AbstractOrderEntryModel> entries,
			final List<CommerceCartModification> cartModificationsToUpdate)
			throws CommerceCartModificationException
	{
		if (CollectionUtils.isNotEmpty(entries))
		{
			for (AbstractOrderEntryModel entry : entries)
			{
				CommerceCartParameter parameter = new CommerceCartParameter();
				parameter.setCart(cart);
				parameter.setEntryNumber(entry.getEntryNumber());
				long quantity = ObjectUtils.defaultIfNull(entry.getQuantity(), Long.valueOf(0L));
				parameter.setQuantity(--quantity);

				// if quantity equals or less then 0 entry will be deleted.
				// It's a default implementation of CommerceUpdateCartEntryStrategy.
				final CommerceCartModification updateModification =
						getCommerceUpdateCartEntryStrategy().updateQuantityForCartEntry(
								parameter);

				if (getModelService().isRemoved(entry))
				{
					// if entry was removed, replace the entry in cartModificationsToUpdate for stub which will be returned by
					// CommerceUpdateCartEntryStrategy.
					cartModificationsToUpdate.stream()
							.filter(modification -> modification.getEntry().equals(entry))
							.peek(modification -> modification.setEntry(updateModification.getEntry()))
							.peek(modification -> modification.getEntry().setQuantity(null))
							.findFirst();
				}
			}
		}
	}

	private CommerceCartParameter createCommerceCartParameter(final CartModel cartModel, final WileyProductVariantSetModel set)
	{
		CommerceCartParameter parameter = new CommerceCartParameter();
		parameter.setEnableHooks(true);
		parameter.setCart(cartModel);
		parameter.setProduct(set);
		parameter.setQuantity(DEFAULT_QUANTITY);
		parameter.setUnit(set.getUnit());
		parameter.setCreateNewEntry(false);
		return parameter;
	}

	public WileyProductVariantSetService getWileyProductVariantSetService()
	{
		return wileyProductVariantSetService;
	}

	public WileyProductService getProductService()
	{
		return productService;
	}

	public CartService getCartService()
	{
		return cartService;
	}

	public void setWileyProductVariantSetService(final WileyProductVariantSetService wileyProductVariantSetService)
	{
		this.wileyProductVariantSetService = wileyProductVariantSetService;
	}

	public void setProductService(final WileyProductService productService)
	{
		this.productService = productService;
	}

	public void setWelProductQuantityInCartManager(final WileyProductQuantityInCartManager welProductQuantityInCartManager)
	{
		this.welProductQuantityInCartManager = welProductQuantityInCartManager;
	}

	public void searchAndRemoveBrokenCarts(final UserModel userModel)
	{
		validateParameterNotNull(userModel, "user cannot be null");
		final List<CartModel> cartModelList = getCartsForSiteAndUser(getBaseSiteService().getCurrentBaseSite(), userModel);
		cartModelList.forEach(cartModel ->
		{
			if (cartModel.getEntries().stream().anyMatch(e -> e.getProduct() == null))
			{
				getModelService().remove(cartModel);
			}
		});
	}
}
