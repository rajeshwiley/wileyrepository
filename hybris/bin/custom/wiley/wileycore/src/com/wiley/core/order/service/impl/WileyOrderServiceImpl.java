package com.wiley.core.order.service.impl;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;

import java.util.List;
import java.util.Optional;

import javax.annotation.Nonnull;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.enums.PaymentModeEnum;
import com.wiley.core.order.WileyCheckoutService;
import com.wiley.core.order.dao.WileyOrderDao;
import com.wiley.core.order.service.WileyOrderService;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateIfSingleResult;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;
import static org.apache.commons.collections.CollectionUtils.isNotEmpty;


public class WileyOrderServiceImpl implements WileyOrderService
{
	private final WileyOrderDao wileyOrderDao;

	private final WileyCheckoutService wileyCheckoutService;

	@Autowired
	public WileyOrderServiceImpl(final WileyOrderDao wileyOrderDao,
			final WileyCheckoutService wileyCheckoutService)
	{
		this.wileyOrderDao = wileyOrderDao;
		this.wileyCheckoutService = wileyCheckoutService;
	}


	public OrderModel getOrderForGUID(final String guid, final BaseSiteModel baseSite)
	{
		final List<OrderModel> orders = wileyOrderDao.findOrdersByGuid(guid, baseSite);
		validateIfSingleResult(orders, OrderModel.class, "guid", guid);
		return orders.get(0);
	}

	@Override
	public boolean isOrderEligibleForPayment(final OrderModel orderModel)
	{
		return isZeroOrder(orderModel) ?
				false :
				Optional.ofNullable(orderModel.getPaymentMode())
					.map(PaymentModeModel::getCode)
					.filter(code -> PaymentModeEnum.CARD.getCode().equalsIgnoreCase(code)
							|| PaymentModeEnum.PAYPAL.getCode().equalsIgnoreCase(code))
					.isPresent();
	}

	@Override
	public boolean isNewOrder(final OrderModel orderModel)
	{
		return CollectionUtils.isEmpty(orderModel.getHistoryEntries());
	}

	@Override
	public boolean isExternalOrder(final OrderModel order)
	{
		return !WileyCoreConstants.SOURCE_SYSTEM_HYBRIS.equals(order.getSourceSystem());
	}

	@Override
	public boolean isOrderExistsForGuid(@Nonnull final String guid, final BaseSiteModel baseSite)
	{
		validateParameterNotNullStandardMessage("guid", guid);
		final List<OrderModel> orders = wileyOrderDao.findOrdersByGuid(guid, baseSite);
		return isNotEmpty(orders);
	}


	protected boolean isZeroOrder(final OrderModel order)
	{
		return !wileyCheckoutService.isNonZeroOrder(order);
	}
}
