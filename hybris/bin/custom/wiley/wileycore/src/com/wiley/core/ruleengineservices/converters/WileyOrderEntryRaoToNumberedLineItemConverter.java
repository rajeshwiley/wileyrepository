package com.wiley.core.ruleengineservices.converters;

import de.hybris.order.calculation.money.Currency;
import de.hybris.order.calculation.money.Money;
import de.hybris.platform.ruleengineservices.converters.OrderEntryRaoToNumberedLineItemConverter;
import de.hybris.platform.ruleengineservices.rao.AbstractOrderRAO;
import de.hybris.platform.ruleengineservices.rao.OrderEntryRAO;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import com.wiley.core.ruleengineservices.calculation.WileyNumberedLineItem;


/**
 * OOTB copy of {@link OrderEntryRaoToNumberedLineItemConverter} class.
 * Modification: class instantiates {@link WileyNumberedLineItem} that consume subtotal price from RAO object.
 */
public class WileyOrderEntryRaoToNumberedLineItemConverter extends OrderEntryRaoToNumberedLineItemConverter
{
	public WileyNumberedLineItem convert(final OrderEntryRAO entryRao) throws ConversionException
	{
		ServicesUtil.validateParameterNotNull(entryRao, "order entry rao must not be null");
		ServicesUtil.validateParameterNotNull(entryRao.getOrder(), "corresponding entry cart rao must not be null");

		AbstractOrderRAO rao = entryRao.getOrder();
		Currency currency = this.getAbstractOrderRaoToCurrencyConverter().convert(rao);
		Money basePrice = new Money(entryRao.getBasePrice(), currency);
		Money subtotalPrice = new Money(entryRao.getSubtotalPrice(), currency);
		WileyNumberedLineItem lineItem = new WileyNumberedLineItem(basePrice, subtotalPrice, entryRao.getQuantity());
		lineItem.setEntryNumber(entryRao.getEntryNumber());
		return lineItem;
	}
}
