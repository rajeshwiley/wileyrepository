package com.wiley.core.cms.service;

import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;

import java.util.List;


/**
 * Created by Uladzimir_Barouski on 3/22/2017.
 */
public interface WileyCMSService
{
	List<AbstractCMSComponentModel> findNotSyncComponentsForPage(AbstractPageModel pageModel);

	List<AbstractPageModel> findPagesByComponent(AbstractCMSComponentModel componentModel);
}
