package com.wiley.core.mpgs.services.strategies.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.Comparator;
import java.util.List;

import com.wiley.core.mpgs.services.WileyMPGSPaymentEntryService;


public abstract class AbstractCardVerificationStrategy
{
	protected WileyMPGSPaymentEntryService wileyMPGSPaymentEntryService;

	PaymentTransactionModel getLastPaymentTransaction(final AbstractOrderModel abstractOrder)
	{
		ServicesUtil.validateParameterNotNull(abstractOrder, "[abstractOrder] can't be null.");
		final List<PaymentTransactionModel> paymentTransactions = abstractOrder.getPaymentTransactions();
		ServicesUtil.validateParameterNotNull(paymentTransactions, "[abstractOrder.paymentTransactions] can't be null.");

		final Comparator<PaymentTransactionModel> creationTimeComparing = Comparator.comparing(
				PaymentTransactionModel::getCreationtime);
		return paymentTransactions.stream().max(creationTimeComparing).orElseGet(
				() -> wileyMPGSPaymentEntryService.createPaymentTransaction(abstractOrder));
	}

	public void setWileyMPGSPaymentEntryService(final WileyMPGSPaymentEntryService wileyMPGSPaymentEntryService)
	{
		this.wileyMPGSPaymentEntryService = wileyMPGSPaymentEntryService;
	}
}
