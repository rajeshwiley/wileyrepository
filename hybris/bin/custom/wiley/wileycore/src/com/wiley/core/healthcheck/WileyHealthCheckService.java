package com.wiley.core.healthcheck;

import java.util.Map;


public interface WileyHealthCheckService
{
	/**
	 * Returns current application build details obtained from git
	 */
	Map<String, Object> getBuildDetails();

	/**
	 * Perform all the registered healthchecks
	 */
	Map<String, Object> performInstanceHealthCheck();
}