package com.wiley.core.abstractorderentry.dao.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.List;

import javax.annotation.Resource;

import com.wiley.core.abstractorderentry.dao.WileyAbstractOrderEntryDao;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


public class WileyAbstractOrderEntryDaoImpl implements WileyAbstractOrderEntryDao
{
	private static final String FIND_ENTRY_BY_GUID_AND_SITE = "SELECT {entry.PK} FROM "
			+ "{AbstractOrderEntry AS entry "
			+ "JOIN AbstractOrder AS order ON {entry.order}={order.PK} "
			+ "JOIN BaseSite AS site ON {order.site}={site.PK}} "
			+ "WHERE {site.uid}=?siteId AND {entry.guid}=?guid " 
			+ "AND {entry.originalOrderEntry} IS NULL";

	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Override
	public List<AbstractOrderEntryModel> findAbstractOrderEntriesByBusinessKey(final String businessKey)
	{
		validateParameterNotNull(businessKey, "businessKey");
		final AbstractOrderEntryModel abstractOrderEntryModel = new AbstractOrderEntryModel();
		abstractOrderEntryModel.setBusinessKey(businessKey);
		return flexibleSearchService.getModelsByExample(abstractOrderEntryModel);
	}

	@Override
	public List<AbstractOrderEntryModel> findAbstractOrderEntryByGuid(final String siteId, final String guid)
	{
		validateParameterNotNull(siteId, "siteId");
		validateParameterNotNull(guid, "guid");

		final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_ENTRY_BY_GUID_AND_SITE);
		query.addQueryParameter("siteId", siteId);
		query.addQueryParameter("guid", guid);
		return flexibleSearchService.<AbstractOrderEntryModel> search(query).getResult();
	}
}
