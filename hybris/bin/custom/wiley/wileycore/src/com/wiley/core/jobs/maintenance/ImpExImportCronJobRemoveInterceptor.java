package com.wiley.core.jobs.maintenance;

import de.hybris.platform.impex.model.ImpExMediaModel;
import de.hybris.platform.impex.model.cronjob.ImpExImportCronJobModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.RemoveInterceptor;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Collection;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Value;

import com.wiley.core.jobs.maintenance.impl.WileyCronJobRemoveUtil;


/**
 * Created by Raman_Hancharou on 4/14/2017.
 */
public class ImpExImportCronJobRemoveInterceptor implements RemoveInterceptor<ImpExImportCronJobModel>
{
	@Resource
	private ModelService modelService;

	@Value("${impex.doNotRemove.media}")
	private String doNotRemoveMedias;

	@Override
	public void onRemove(final ImpExImportCronJobModel job, final InterceptorContext interceptorContext)
			throws InterceptorException
	{
		Collection<Object> toRemove = new ArrayList<>();

		final ImpExMediaModel jobMedia = job.getJobMedia();
		if (jobMedia != null && WileyCronJobRemoveUtil.isRemovableMedia(jobMedia, doNotRemoveMedias))
		{
			toRemove.add(jobMedia);
		}
		Collection<ImpExMediaModel> externalDataCollection = job.getExternalDataCollection();
		if (CollectionUtils.isNotEmpty(externalDataCollection))
		{
			externalDataCollection.stream().forEach(media -> toRemove.add(media));
		}
		if (!toRemove.isEmpty())
		{
			modelService.removeAll(toRemove);
		}
	}

}
