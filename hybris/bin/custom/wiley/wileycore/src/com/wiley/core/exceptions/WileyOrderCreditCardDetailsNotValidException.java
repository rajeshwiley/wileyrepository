package com.wiley.core.exceptions;

public class WileyOrderCreditCardDetailsNotValidException extends RuntimeException
{
	private String errorCode;

	public WileyOrderCreditCardDetailsNotValidException()
	{
	}

	public WileyOrderCreditCardDetailsNotValidException(final String errorCode)
	{
		this.errorCode = errorCode;
	}

	public String getErrorCode()
	{
		return errorCode;
	}
}
