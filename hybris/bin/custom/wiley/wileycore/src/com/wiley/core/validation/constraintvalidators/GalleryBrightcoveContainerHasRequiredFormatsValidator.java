package com.wiley.core.validation.constraintvalidators;

import de.hybris.platform.core.model.product.ProductModel;

import javax.validation.ConstraintValidatorContext;

import com.wiley.core.validation.constraints.GalleryBrightcoveContainerHasRequiredFormats;


public class GalleryBrightcoveContainerHasRequiredFormatsValidator
		extends AbstractWileyProductConstraintValidator<GalleryBrightcoveContainerHasRequiredFormats>
{
	@Override
	public boolean isValid(final ProductModel productModel, final ConstraintValidatorContext constraintValidatorContext)
	{
		return getValidationService().validateGalleryBrightcoveContainerContainsRequiredFormats(productModel.getGalleryImages());
	}
}
