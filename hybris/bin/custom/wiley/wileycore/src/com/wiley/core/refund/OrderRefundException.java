package com.wiley.core.refund;

public class OrderRefundException extends Exception {
    private final String orderCode;

    public OrderRefundException(final String orderCode) {
        this.orderCode = orderCode;
    }

    public OrderRefundException(final String orderCode, final String message, final Throwable nested) {
        super("Order Refund(" + orderCode + ") :" + message, nested);
        this.orderCode = orderCode;
    }

    public OrderRefundException(final String orderCode, final String message) {
        super("Order Refund(" + orderCode + ") :" + message);
        this.orderCode = orderCode;
    }

    public OrderRefundException(final String orderCode, final Throwable nested) {
        super("orderCode: " + orderCode, nested);
        this.orderCode = orderCode;
    }

    public String getOrderCode() {
        return this.orderCode;
    }
}
