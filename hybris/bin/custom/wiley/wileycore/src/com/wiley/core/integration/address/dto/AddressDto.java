package com.wiley.core.integration.address.dto;

import javax.validation.constraints.Pattern;


public class AddressDto
{
	//pattern for latin characters
	@Pattern(regexp = "^[\\u0020-\\u007F\\u00A0-\\u017F]*$",
			message = "{com.wiley.core.validation.constraints.LatinCharacters.message}")
	private String line1;

	//pattern for latin characters
	@Pattern(regexp = "^[\\u0020-\\u007F\\u00A0-\\u017F]*$",
			message = "{com.wiley.core.validation.constraints.LatinCharacters.message}")
	private String line2;

	//pattern for latin characters
	@Pattern(regexp = "^[\\u0020-\\u007F\\u00A0-\\u017F]*$",
			message = "{com.wiley.core.validation.constraints.LatinCharacters.message}")
	private String town;

	private String regionIso2;

	private String postalCode;

	private String countryIso2;

	private double relevance;

	private String decision;

	public String getLine1()
	{
		return line1;
	}

	public void setLine1(final String line1)
	{
		this.line1 = line1;
	}

	public String getLine2()
	{
		return line2;
	}

	public void setLine2(final String line2)
	{
		this.line2 = line2;
	}

	public String getTown()
	{
		return town;
	}

	public void setTown(final String town)
	{
		this.town = town;
	}

	public String getRegionIso2()
	{
		return regionIso2;
	}

	public void setRegionIso2(final String regionIso2)
	{
		this.regionIso2 = regionIso2;
	}

	public String getPostalCode()
	{
		return postalCode;
	}

	public void setPostalCode(final String postalCode)
	{
		this.postalCode = postalCode;
	}

	public String getCountryIso2()
	{
		return countryIso2;
	}

	public void setCountryIso2(final String countryIso2)
	{
		this.countryIso2 = countryIso2;
	}

	public double getRelevance()
	{
		return relevance;
	}

	public void setRelevance(final double relevance)
	{
		this.relevance = relevance;
	}

	public String getDecision()
	{
		return decision;
	}

	public void setDecision(final String decision)
	{
		this.decision = decision;
	}

	@Override
	public String toString()
	{
		return "AddressDto{"
				+ "line1='" + line1 + '\''
				+ ", line2='" + line2 + '\''
				+ ", town='" + town + '\''
				+ ", regionIso2='" + regionIso2 + '\''
				+ ", postalCode='" + postalCode + '\''
				+ ", countryIso2='" + countryIso2 + '\''
				+ ", relevance=" + relevance
				+ ", decision=" + decision + '}';
	}
}