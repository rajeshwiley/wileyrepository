package com.wiley.core.wileyb2b.order;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.core.model.order.OrderModel;


/**
 * Wiley B2B Service to trigger email sending with order details in PDF form
 */
public interface Wileyb2bSendEmailWithOrderDetailsService
{
	boolean sendEmailWithOrderDetails(B2BCustomerModel b2BCustomer, OrderModel order, String email);
}
