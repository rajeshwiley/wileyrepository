package com.wiley.core.regCode.exception;

/**
 * Abstract class for Validation and Burn registration code exceptions
 */
public class AbstractRegCodeOperationException extends RuntimeException
{
	public AbstractRegCodeOperationException(final String message)
	{
		super(message);
	}

	public AbstractRegCodeOperationException(final String message, final Throwable cause)
	{
		super(message, cause);
	}
}
