package com.wiley.core.event.facade;

import de.hybris.platform.servicelayer.event.events.AbstractEvent;

import java.io.Serializable;

/**
 * Created by Georgii_Gavrysh on 12/9/2016.
 */
public class WileyOneObjectFacadeEvent<PARAM extends Serializable> extends AbstractEvent {

    private PARAM parameter;

    /**
     * Create a new ApplicationEvent.
     *
     * @param source the object on which the event initially occurred (never {@code null})
     */
    public WileyOneObjectFacadeEvent(final PARAM source) {
        super(source);
        this.parameter = source;
    }

    public PARAM getParameter() {
        return parameter;
    }

    public void setParameter(final PARAM parameter) {
        this.parameter = parameter;
    }
}
