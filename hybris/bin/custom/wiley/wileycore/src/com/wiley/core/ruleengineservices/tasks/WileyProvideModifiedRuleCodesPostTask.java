package com.wiley.core.ruleengineservices.tasks;

import de.hybris.platform.ruleengine.ExecutionContext;
import de.hybris.platform.ruleengine.RuleEngineActionResult;
import de.hybris.platform.ruleengineservices.maintenance.tasks.impl.ProvideModifiedRuleCodesPostTask;

import java.util.Collection;
import java.util.Objects;


/**
 * Override OOTB {@link ProvideModifiedRuleCodesPostTask} to avoid exception during publish promotion rule.
 * When we have more than 1 node and publish promotion rule, other nodes starts publishing promotion rules in
 * de.hybris.platform.ruleengine.event.OnKieModuleSwappingEventListener. But not set executionContext to RuleEngineActionResult.
 * OnKieModuleSwappingEventListener deprecated in Hybris 1811
 */
public class WileyProvideModifiedRuleCodesPostTask extends ProvideModifiedRuleCodesPostTask
{
	@Override
	public boolean execute(final RuleEngineActionResult result)
	{
		if (!result.isActionFailed())
		{
			Collection<String> modifiedRuleCodes = getModifiedRuleCodes(result);
			addExecutionContextIfNotExist(result);
			result.getExecutionContext().setModifiedRuleCodes(modifiedRuleCodes);
			return true;
		}
		else
		{
			return false;
		}
	}

	private void addExecutionContextIfNotExist(final RuleEngineActionResult result)
	{
		if (Objects.isNull(result.getExecutionContext()))
		{
			result.setExecutionContext(new ExecutionContext());
		}
	}
}
