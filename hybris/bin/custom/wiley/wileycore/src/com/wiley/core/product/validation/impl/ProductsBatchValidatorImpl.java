package com.wiley.core.product.validation.impl;

import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.validation.model.constraints.ConstraintGroupModel;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.wiley.core.model.ProductValidationErrorModel;
import com.wiley.core.product.validation.ProductsBatchValidator;
import com.wiley.core.product.validation.ProductsValidationService;

import static de.hybris.platform.servicelayer.interceptor.impl.InterceptorExecutionPolicy.DISABLED_INTERCEPTOR_BEANS;


public class ProductsBatchValidatorImpl implements ProductsBatchValidator
{
	private static final Logger LOG = LoggerFactory.getLogger(ProductsBatchValidatorImpl.class);

	@Resource
	private ProductsValidationService productsValidationService;

	@Resource
	private ModelService modelService;

	@Resource
	private SessionService sessionService;

	private Set<String> disabledInterceptorBeans;

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public CronJobResult validateProducts(final List<ProductModel> idsBatch, final CronJobModel cronJob,
			final List<ConstraintGroupModel> constraintGroups)
	{
		CronJobResult result = CronJobResult.SUCCESS;

		LOG.debug("Processing [{}] batch...", idsBatch);

		final List<ProductModel> productsInBatch = idsBatch; //productsValidationService.getProductsByCodes(idsBatch);

		final int idBatchSize = idsBatch.size();
		if (productsInBatch.size() != idBatchSize)
		{
			LOG.warn(
					"Batch [{}] couldn't retrieve all products by ids. [{}] products has been missed from [{}]",
					idsBatch, idBatchSize - productsInBatch.size(), idBatchSize);
			result = CronJobResult.FAILURE;
		}

		final List<ProductValidationErrorModel> errorsInBatch = validateProducts(cronJob, productsInBatch, constraintGroups);

		final ValidProductFilter validProductFilter = new ValidProductFilter(errorsInBatch);

		for (final ProductModel product : productsInBatch)
		{
			product.setApprovalStatus(validProductFilter.test(product) ?
					ArticleApprovalStatus.APPROVED :
					ArticleApprovalStatus.UNAPPROVED);
		}

		LOG.debug("Persisting [{}] product validation errors of [{}] batch...", errorsInBatch.size(), idsBatch);

		sessionService.executeInLocalViewWithParams(
				Collections.singletonMap(DISABLED_INTERCEPTOR_BEANS, disabledInterceptorBeans), new SessionExecutionBody()
				{
					@Override
					public void executeWithoutResult()
					{
						modelService.saveAll(errorsInBatch);
						modelService.saveAll(productsInBatch);
					}
				});

		LOG.debug("The [{}] batch has been processed with result [{}]", idsBatch, result);

		return result;
	}

	private List<ProductValidationErrorModel> validateProducts(final CronJobModel cronJob,
			final List<ProductModel> productsInBatch, final List<ConstraintGroupModel> constraintGroups)
	{
		return productsInBatch.stream()
				.flatMap(productModel -> productsValidationService.validate(productModel, cronJob, constraintGroups).stream())
				.collect(Collectors.toList());
	}

	/**
	 * Filters out products which has associated validation error
	 */
	private static class ValidProductFilter implements Predicate<ProductModel>
	{
		private final List<ProductValidationErrorModel> errorsInBatch;

		ValidProductFilter(final List<ProductValidationErrorModel> errorsInBatch)
		{
			this.errorsInBatch = errorsInBatch;
		}

		@Override
		public boolean test(final ProductModel productModel)
		{
			return errorsInBatch.stream().noneMatch(errorModel -> productModel.equals(errorModel.getProduct()));
		}
	}

	public Set<String> getDisabledInterceptorBeans()
	{
		return disabledInterceptorBeans;
	}

	public void setDisabledInterceptorBeans(final Set<String> disabledInterceptorBeans)
	{
		this.disabledInterceptorBeans = disabledInterceptorBeans;
	}

}