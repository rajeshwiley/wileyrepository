package com.wiley.core.exceptions;

/**
 * This exception can be thrown if some validation rules are not passed during external system invocation.
 */
public class ExternalSystemValidationException extends ExternalSystemException
{
	public ExternalSystemValidationException()
	{
		super();
	}

	public ExternalSystemValidationException(final String message)
	{
		super(message);
	}

	public ExternalSystemValidationException(final String message, final Throwable cause)
	{
		super(message, cause);
	}
}
