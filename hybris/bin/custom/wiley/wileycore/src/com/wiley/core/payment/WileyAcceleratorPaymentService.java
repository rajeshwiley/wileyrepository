package com.wiley.core.payment;

import de.hybris.platform.acceleratorservices.payment.PaymentService;
import de.hybris.platform.acceleratorservices.payment.data.PaymentSubscriptionResultItem;
import de.hybris.platform.core.model.user.CustomerModel;

import java.util.Map;

public interface WileyAcceleratorPaymentService extends PaymentService
{
	PaymentSubscriptionResultItem completeHopCreatePaymentSubscriptionWithoutCheckout(CustomerModel customerModel,
																					  Map<String, String> parameters);
}
