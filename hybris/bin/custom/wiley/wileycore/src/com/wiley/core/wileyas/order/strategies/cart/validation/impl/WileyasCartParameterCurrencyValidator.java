package com.wiley.core.wileyas.order.strategies.cart.validation.impl;

import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.util.PriceValue;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.model.WileyExternalDiscountModel;
import com.wiley.core.wiley.strategies.cart.validation.WileyCartParameterValidator;

import static org.apache.commons.lang.BooleanUtils.isTrue;


public class WileyasCartParameterCurrencyValidator implements WileyCartParameterValidator
{
	public static final String CURRENCY_MESSAGE = "Currency isn't specified for absolute external discount [%s]";
	private CommonI18NService commonI18NService;

	@Override
	public void validateAddToCart(final CommerceCartParameter parameter) throws CommerceCartModificationException
	{
		validateExternalPriceValues(parameter);
		validateExternalDiscounts(parameter);
	}

	protected void validateExternalDiscounts(final CommerceCartParameter parameter) throws CommerceCartModificationException
	{
		final List<WileyExternalDiscountModel> externalDiscounts = parameter.getExternalDiscounts();
		if (CollectionUtils.isNotEmpty(externalDiscounts))
		{
			for (WileyExternalDiscountModel externalDiscount : externalDiscounts)
			{
				if (isTrue(externalDiscount.getAbsolute()) && externalDiscount.getCurrency() == null)
				{
					throw new CommerceCartModificationException(String.format(CURRENCY_MESSAGE, externalDiscount.getGuid()));
				}
			}
		}
	}

	protected void validateExternalPriceValues(final CommerceCartParameter parameter) throws CommerceCartModificationException
	{
		final List<PriceValue> externalPriceValues = parameter.getExternalPriceValues();
		if (CollectionUtils.isNotEmpty(externalPriceValues))
		{
			for (PriceValue externalPriceValue : externalPriceValues)
			{
				try
				{
					getCommonI18NService().getCurrency(externalPriceValue.getCurrencyIso());
				}
				catch (UnknownIdentifierException e)
				{
					throw new CommerceCartModificationException(
							String.format(CURRENCY_MESSAGE, externalPriceValue.getCurrencyIso()), e);
				}
			}
		}
	}

	public CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	@Required
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}
}
