package com.wiley.core.payment;

public final class WileyHttpRequestParams
{
	public static final String WPG_OPERATION = "WPG_operation";
	public static final String WPG_TIMESTAMP = "WPG_timestamp";
	public static final String WPG_VENDOR_ID = "WPG_vendorID";
	public static final String WPG_TRANSACTION_ID = "WPG_transID";
	public static final String WPG_METHOD = "WPG_method";
	public static final String WPG_DESCRIPTION = "WPG_description";
	public static final String WPG_REGION = "WPG_region";
	public static final String WPG_ADDRESS = "WPG_address";
	public static final String WPG_POSTCODE = "WPG_postcode";
	public static final String WPG_COUNTRY_CODE = "WPG_countryCode";
	public static final String WPG_ALLOW_AVS_FAIL = "WPG_allowAVSfail";
	public static final String WPG_SECURITY = "WPG_security";
	public static final String WPG_VALUE = "WPG_value";
	public static final String WPG_CURRENCY = "WPG_currency";
	public static final String WPG_CUSTOM_SITE_URL = "WPG_CUSTOM_site_url";
	public static final String WPG_CUSTOM_OPERATION = "WPG_CUSTOM_operation";
	public static final String WPG_CUSTOM_BACK_URL = "WPG_CUSTOM_back_url";
	public static final String WPG_CUSTOM_ADOBE_ANALYTICS_SCRIPT_URL = "WPG_CUSTOM_adobe_analytics_script_url";
	public static final String WPG_TOKEN = "WPG_token";
	public static final String WPG_AUTH_CODE = "WPG_authCode";
	public static final String WPG_CUSTOM_INITIATOR = "WPG_CUSTOM_initiator";
	public static final String WPG_CUSTOM_CUSTOMER_FIRST_NAME = "WPG_CUSTOM_customer_first_name";
	public static final String WPG_CUSTOM_THEME = "WPG_CUSTOM_theme";
	public static final String WPG_CUSTOM_CURRENT_YEAR = "WPG_CUSTOM_CURRENT_YEAR";
	public static final String WPG_CUSTOM_GRAPHICSTANDARDS_LINK = "WPG_CUSTOM_GRAPHICSTANDARDS_LINK";
	public static final String WPG_CUSTOM_WILEY_LINK = "WPG_CUSTOM_WILEY_LINK";
	public static final String WPG_CUSTOM_LOGO_URL = "WPG_CUSTOM_logo_url";
	public static final String WPG_CUSTOM_LOGO_IMAGE = "WPG_CUSTOM_logo_image";
	public static final String WPG_CUSTOM_RESPONSIVE_LOGO_DATA = "WPG_CUSTOM_responsive_logo_data";

	private WileyHttpRequestParams()
	{
	}

}
