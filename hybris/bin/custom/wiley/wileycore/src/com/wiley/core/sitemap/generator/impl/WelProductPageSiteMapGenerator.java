package com.wiley.core.sitemap.generator.impl;

import de.hybris.platform.acceleratorservices.sitemap.generator.impl.ProductPageSiteMapGenerator;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import com.wiley.core.util.url.UrlVisibilityStrategy;


public class WelProductPageSiteMapGenerator extends ProductPageSiteMapGenerator
{
	@Resource
	private UrlVisibilityStrategy welProductUrlVisibilityStrategy;

	@Override
	protected List<ProductModel> getDataInternal(final CMSSiteModel siteModel)
	{
		List<ProductModel> products = super.getDataInternal(siteModel);
		return products
				.stream()
				.filter(product -> welProductUrlVisibilityStrategy.isUrlForItemVisible(product))
				.collect(Collectors.toList());
	}
}
