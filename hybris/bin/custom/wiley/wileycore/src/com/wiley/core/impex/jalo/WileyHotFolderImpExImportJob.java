package com.wiley.core.impex.jalo;

import de.hybris.platform.impex.jalo.Importer;
import de.hybris.platform.impex.jalo.cronjob.ImpExImportCronJob;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.type.ComposedType;

import org.apache.log4j.Logger;

import com.wiley.core.integration.handlers.WileyHotFolderCronJobErrorHandler;


public class WileyHotFolderImpExImportJob extends GeneratedWileyHotFolderImpExImportJob
{
	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(WileyHotFolderImpExImportJob.class.getName());

	@Override
	protected Item createItem(final SessionContext ctx, final ComposedType type, final ItemAttributeMap allAttributes)
			throws JaloBusinessException
	{
		// business code placed here will be executed before the item is created
		// then create the item
		final Item item = super.createItem(ctx, type, allAttributes);
		// business code placed here will be executed after the item was created
		// and return the item
		return item;
	}

	@Override
	protected Importer adjustImporter(final Importer importer, final ImpExImportCronJob cronJob)
	{
		super.adjustImporter(importer, cronJob);
		importer.setErrorHandler(new WileyHotFolderCronJobErrorHandler(cronJob));
		return importer;
	}

}
