package com.wiley.core.mpgs.services.strategies.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.dto.TransactionStatusDetails;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;

import java.util.Date;

import com.wiley.core.mpgs.response.WileyVerifyResponse;
import com.wiley.core.mpgs.services.strategies.CardVerificationStrategy;


public class MpgsNoCardVerificationStrategy extends AbstractCardVerificationStrategy implements CardVerificationStrategy
{

	@Override
	public PaymentTransactionEntryModel verify(final AbstractOrderModel cart, final String sessionId)
	{
		final PaymentTransactionModel lastPaymentTransaction = getLastPaymentTransaction(cart);
		WileyVerifyResponse verifyResponse = new WileyVerifyResponse();
		verifyResponse.setTimeOfRecord(new Date());
		verifyResponse.setStatus(TransactionStatus.ACCEPTED.toString());
		verifyResponse.setStatusDetails(TransactionStatusDetails.SUCCESFULL.toString());

		return wileyMPGSPaymentEntryService.createVerifyEntry(verifyResponse,
				lastPaymentTransaction);
	}
}
