package com.wiley.core.externaltax.services.impl;


/**
 * Error is thrown in case if any error occurs during call to tax calculation server
 */
public class TaxSystemException extends RuntimeException
{
	public TaxSystemException()
	{
	}

	public TaxSystemException(final Throwable cause)
	{
		super(cause);
	}

	public TaxSystemException(final String paramString)
	{
		super(paramString);
	}

	public TaxSystemException(final String paramString, final Throwable paramThrowable)
	{
		super(paramString, paramThrowable);
	}
}
