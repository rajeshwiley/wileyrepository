package com.wiley.core.wileycom.order.impl;

import de.hybris.platform.commerceservices.delivery.impl.DefaultDeliveryService;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import com.wiley.core.model.ExternalDeliveryModeModel;
import com.wiley.core.wileycom.order.WileycomDeliveryService;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


public class WileycomDeliveryServiceImpl extends DefaultDeliveryService implements WileycomDeliveryService
{

	@Resource
	private WileycomDeliveryModeLookupStrategy wileycomDeliveryModeLookupStrategy;

	/**
	 * Get the supported external delivery modes for the abstract order.
	 *
	 * @param abstractOrder
	 * @return the list of supported externaldelivery modes, by default sorted by cost
	 */
	@Nonnull
	public List<ExternalDeliveryModeModel> getSupportedExternalDeliveryModeListForOrder(final AbstractOrderModel abstractOrder)
	{
		validateParameterNotNull(abstractOrder, "abstractOrder model cannot be null");
		final List<ExternalDeliveryModeModel> deliveryModes =
				wileycomDeliveryModeLookupStrategy.getSelectableDeliveryModesForOrder(
						abstractOrder);
		sortExternalDeliveryModes(deliveryModes);

		validateParameterNotNullStandardMessage("externalDeliveryModes", deliveryModes);
		return deliveryModes;
	}

	/**
	 * finds external delivery mode by passed code from passed deliveryModes
	 *
	 * @param currentDeliveryModeCode
	 * @param externalDeliveryModes
	 * @return found external delivery mode
	 */
	@Override
	public Optional<ExternalDeliveryModeModel> findDeliveryModeByExternalCode(@Nonnull final String currentDeliveryModeCode,
			@Nonnull final List<ExternalDeliveryModeModel> externalDeliveryModes)
	{
		validateParameterNotNull(currentDeliveryModeCode, "Delivery mode external code cannot be null");
		validateParameterNotNull(externalDeliveryModes, "External delivery modes cannot be null");

		return externalDeliveryModes.stream().filter(
				externalDeliveryModeModel -> currentDeliveryModeCode.equals(externalDeliveryModeModel.getExternalCode()))
				.findFirst();
	}

	/**
	 * Gets first delivery mode from passed deliveryMode list
	 *
	 * @param externalDeliveryModes
	 * 		list of available modes
	 * @return first dilivery mode from passed list
	 */
	@Nonnull
	@Override
	public ExternalDeliveryModeModel getDefaultDeliveryMode(@Nonnull final List<ExternalDeliveryModeModel> externalDeliveryModes)
	{
		ServicesUtil.validateIfAnyResult(externalDeliveryModes, "External delivery mode list is null or empty");
		return externalDeliveryModes.get(0);
	}

	protected void sortExternalDeliveryModes(final List<ExternalDeliveryModeModel> externalDeliveryModeModels)
	{
		Collections.sort(externalDeliveryModeModels, new ExternalDeliveryModeCostComparator());
	}

	public static class ExternalDeliveryModeCostComparator implements Comparator<ExternalDeliveryModeModel>
	{

		@Override
		public int compare(final ExternalDeliveryModeModel deliveryMode1, final ExternalDeliveryModeModel deliveryMode2)
		{
			final Double value1 = deliveryMode1.getCostValue();
			final Double value2 = deliveryMode2.getCostValue();

			if (value1 == null && value2 == null)
			{
				return deliveryMode1.getCode().compareTo(deliveryMode2.getCode());
			}
			else if (value1 == null)
			{
				return -1;
			}
			else if (value2 == null)
			{
				return 1;
			}

			final int result = value1.compareTo(value2);
			return result == 0 ? deliveryMode1.getCode().compareTo(deliveryMode2.getCode()) : result;
		}
	}

}
