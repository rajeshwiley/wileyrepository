package com.wiley.core.interceptors;

import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;

import org.springframework.beans.factory.annotation.Value;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public class WileyBusinessProcessPrepareInterceptor implements PrepareInterceptor<BusinessProcessModel>
{
	@Value("${db.type.system.name}")
	private String typeSystem;

	@Override
	public void onPrepare(final BusinessProcessModel model, final InterceptorContext ctx)
	{
		if (ctx.isNew(model))
		{
			model.setTypeSystem(typeSystem);
		}
	}
}
