package com.wiley.core.wiley.caching.impl;

import de.hybris.platform.regioncache.CacheValueLoadException;
import de.hybris.platform.regioncache.CacheValueLoader;
import de.hybris.platform.regioncache.key.CacheKey;


public class WileyDefaultCacheValueLoader implements CacheValueLoader
{
	private final Object cachedValue;

	public WileyDefaultCacheValueLoader(final Object cachedValue)
	{
		this.cachedValue = cachedValue;
	}

	@Override
	public Object load(final CacheKey cacheKey) throws CacheValueLoadException
	{
		if (cacheKey == null) {
			return null;
		}

		return cachedValue;
	}
}
