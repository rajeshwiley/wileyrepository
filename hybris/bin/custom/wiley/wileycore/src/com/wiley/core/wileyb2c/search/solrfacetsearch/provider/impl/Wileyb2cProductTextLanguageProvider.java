package com.wiley.core.wileyb2c.search.solrfacetsearch.provider.impl;

import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;

import java.util.Collections;
import java.util.List;

import com.wiley.core.model.WileyPurchaseOptionProductModel;
import com.wiley.core.wileycom.search.solrfacetsearch.provider.impl.AbstractWileycomValueProvider;


/**
 */
public class Wileyb2cProductTextLanguageProvider extends AbstractWileycomValueProvider<String>
{

	@Override
	protected List<String> collectValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty, final Object model)
			throws FieldValueProviderException
	{
		final LanguageModel textLanguage = ((WileyPurchaseOptionProductModel) model).getBaseProduct().getTextLanguage();

		return textLanguage == null ? Collections.emptyList() : Collections.singletonList(textLanguage.getIsocode());
	}


}
