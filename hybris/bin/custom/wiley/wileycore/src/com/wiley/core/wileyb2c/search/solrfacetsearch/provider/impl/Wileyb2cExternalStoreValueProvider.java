package com.wiley.core.wileyb2c.search.solrfacetsearch.provider.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.enums.WileyWebLinkTypeEnum;
import com.wiley.core.model.WileyWebLinkModel;
import com.wiley.core.wileyb2c.weblinks.WileyWebLinkService;
import com.wiley.core.wileycom.search.solrfacetsearch.provider.impl.AbstractWileycomValueProvider;


/**
 * Created by Raman_Hancharou on 6/14/2017.
 * Provides values for variant product with the (!)only one link(!) to WILEY_PLUS_STORE or WOL_STORE
 */
public class Wileyb2cExternalStoreValueProvider extends AbstractWileycomValueProvider<String>
{
	private String externalStoreUrlProperty;
	private String externalStoreTypeProperty;

	private WileyWebLinkService wileyWebLinkService;

	@Override
	protected List<String> collectValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty, final Object model)
			throws FieldValueProviderException
	{
		final List<String> values = new ArrayList<>();
		if (model instanceof ProductModel)
		{
			final Collection<WileyWebLinkModel> externalStores = ((ProductModel) model).getExternalStores();
			if (CollectionUtils.isNotEmpty(externalStores) && CollectionUtils.size(externalStores) == 1)
			{
				final WileyWebLinkModel wileyWebLink = externalStores.iterator().next();
				final WileyWebLinkTypeEnum webLinkType = wileyWebLink.getType();
				if (WileyWebLinkTypeEnum.WILEY_PLUS_STORE.equals(webLinkType) || WileyWebLinkTypeEnum.WOL_STORE.equals(
						webLinkType) || WileyWebLinkTypeEnum.REQUEST_QUOTE.equals(webLinkType))
				{
					if (externalStoreUrlProperty.equals(indexedProperty.getName()))
					{
						values.add(wileyWebLinkService.getWebLinkUrl(wileyWebLink));
					}
					else if (externalStoreTypeProperty.equals(indexedProperty.getName()))
					{
						values.add(webLinkType.getCode());
					}
				}
			}
		}
		return values;
	}

	@Required
	public void setExternalStoreUrlProperty(final String externalStoreUrlProperty)
	{
		this.externalStoreUrlProperty = externalStoreUrlProperty;
	}

	@Required
	public void setExternalStoreTypeProperty(final String externalStoreTypeProperty)
	{
		this.externalStoreTypeProperty = externalStoreTypeProperty;
	}

	@Required
	public void setWileyWebLinkService(final WileyWebLinkService wileyWebLinkService)
	{
		this.wileyWebLinkService = wileyWebLinkService;
	}
}
