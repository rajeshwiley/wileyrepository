package com.wiley.core.externaltax.strategies.impl;

import de.hybris.platform.commerceservices.order.CommerceCartHashCalculationStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceOrderParameter;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;

import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.externaltax.strategies.WileyTaxAddressStrategy;

public class WileyCartHashCalculationStrategy implements CommerceCartHashCalculationStrategy
{

	private WileyTaxAddressStrategy taxAddressStrategy;

	@Override
	@Deprecated
	public String buildHashForAbstractOrder(final AbstractOrderModel abstractOrderModel,
			final List<String> additionalValues)
	{
		final CommerceOrderParameter parameter = new CommerceOrderParameter();
		parameter.setOrder(abstractOrderModel);
		parameter.setAdditionalValues(additionalValues);
		return this.buildHashForAbstractOrder(parameter);
	}

	@Override
	public String buildHashForAbstractOrder(final CommerceOrderParameter parameter)
	{
		final AbstractOrderModel abstractOrderModel = parameter.getOrder();
		final List<String> additionalValues = parameter.getAdditionalValues();

		final StringBuilder orderValues = new StringBuilder();

		final AddressModel paymentAddress = taxAddressStrategy.resolvePaymentAddress(abstractOrderModel);
		final AddressModel deliveryAddress = taxAddressStrategy.resolveDeliveryAddress(abstractOrderModel);
		
		orderValues.append(convertAddressToString(paymentAddress));
		orderValues.append(convertAddressToString(deliveryAddress));
		if (abstractOrderModel.getDeliveryCost() != null)
		{
			orderValues.append(abstractOrderModel.getDeliveryCost().toString());
		}

		for (final AbstractOrderEntryModel abstractOrderEntryModel : abstractOrderModel.getEntries())
		{
			orderValues.append(convertAbstractOrderEntryToString(abstractOrderEntryModel));
		}
		if (abstractOrderModel.getCurrency() != null)
		{
			orderValues.append(abstractOrderModel.getCurrency().getIsocode());
		}
		if (abstractOrderModel.getTotalDiscounts() != null)
		{
			orderValues.append(abstractOrderModel.getTotalDiscounts().toString());
		}

		if (additionalValues != null)
		{
			for (final String additionalValue : additionalValues)
			{
				orderValues.append(additionalValue);
			}
		}

		final String orderValue = orderValues.toString();
		return DigestUtils.md5Hex(orderValue);

	}

	private String convertAddressToString(final AddressModel address)
	{
		final StringBuilder addressStringBuilder = new StringBuilder();
		if (address != null)
		{
			addressStringBuilder.append(address.getTown());
			addressStringBuilder.append(address.getPostalcode());
			if (address.getRegion() != null)
			{
				addressStringBuilder.append(address.getRegion().getIsocodeShort());
			}
			if (address.getCountry() != null)
			{
				addressStringBuilder.append(address.getCountry().getIsocode());
			}
		}
		return addressStringBuilder.toString();
	}

	private String convertAbstractOrderEntryToString(final AbstractOrderEntryModel abstractOrderEntryModel)
	{
		final StringBuilder entryValues = new StringBuilder();

		entryValues.append(abstractOrderEntryModel.getTotalPrice().toString());
		entryValues.append(abstractOrderEntryModel.getSubtotalPrice().toString());
		entryValues.append(abstractOrderEntryModel.getProduct().getCode());
		entryValues.append(abstractOrderEntryModel.getQuantity().toString());

		return entryValues.toString();
	}

	@Required
	public void setTaxAddressStrategy(final WileyTaxAddressStrategy taxAddressStrategy)
	{
		this.taxAddressStrategy = taxAddressStrategy;
	}
}
