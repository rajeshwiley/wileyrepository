package com.wiley.core.price;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;

import java.math.BigDecimal;


public interface WileyDiscountCalculationService
{
	/**
	 * Calculates total discount including order level and order entry level discounts
	 *
	 * @param abstractOrder
	 * 		order to calculate discount for
	 * @return total discount for order
	 */
	double getTotalDiscount(AbstractOrderModel abstractOrder);

	/**
	 * Calculates proportional order entry discount value. This value includes any entry level discount as it plus order
	 * level discount distributed proportionally to item "weight" in order total price.
	 * "Weight" is calculated as item base price (price per unit) multiplied by item quantity.
	 *
	 * @param entry
	 * 		entry to calculate discount for
	 * @return discount value
	 */
	BigDecimal getEntryProportionalDiscount(AbstractOrderEntryModel entry);

	/**
	 * Calculates proportional order entry discount value. This value includes just order
	 * level discount distributed proportionally to item "weight" in order total price.
	 * "Weight" is calculated as item base price (price per unit) multiplied by item quantity.
	 *
	 * @param entry
	 * 		entry to calculate discount for
	 * @return discount value
	 */

	BigDecimal getEntryOrderLevelProportionalDiscount(AbstractOrderEntryModel entry);
}
