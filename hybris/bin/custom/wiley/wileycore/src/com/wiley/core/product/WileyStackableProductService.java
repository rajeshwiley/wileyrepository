package com.wiley.core.product;

import de.hybris.platform.core.model.product.ProductModel;

import javax.annotation.Nonnull;


/**
 * Service for recognizing if product has stackable price
 */
public interface WileyStackableProductService
{
	/**
	 * Checks if product has stackable price
	 *
	 * @param product
	 *          the product
	 * @return true if product has true {@link ProductModel#STACKABLEPRICE} else false
	 */
	boolean hasStackablePrice(@Nonnull ProductModel product);
}
