package com.wiley.core.search.solrfacetsearch.populators.impl;

import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.solrfacetsearch.search.impl.populators.FacetSearchQuerySortsPopulator;

import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;


/**
 * Changes the logic of OOTB de.hybris.platform.solrfacetsearch.search.impl.populators.FacetSearchQuerySortsPopulator
 * populator to use base product code for promoted items
 */
public class WileyFacetSearchQuerySortsPopulator extends FacetSearchQuerySortsPopulator
{
	@Resource
	private ModelService modelService;

	@Override
	protected String buildPromotedItemsSort(final SearchQuery searchQuery)
	{
		List<PK> promotedItems = searchQuery.getPromotedItems();
		if (CollectionUtils.isEmpty(promotedItems))
		{
			return "";
		}
		else
		{
			StringBuilder query = new StringBuilder(256);
			query.append("query({!v='");
			float score = 10000.0F;
			int index = 0;

			for (Iterator var7 = promotedItems.iterator(); var7.hasNext(); ++index)
			{
				PK promotedItem = (PK) var7.next();
				if (index != 0)
				{
					query.append(' ');
				}
				Object o = modelService.get(promotedItem);
				ProductModel productModel = null;
				if (o instanceof ProductModel)
				{
					productModel = (ProductModel) o;
				}

				if (productModel != null)
				{
					query.append("baseProductCode_string");
					query.append(':');
					query.append(productModel.getCode());
					query.append("^=");
					query.append(score);
					score = Math.nextDown(score);
				}
			}

			query.append("'},1)");
			return query.toString();
		}
	}
}
