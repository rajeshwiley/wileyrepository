package com.wiley.core.wileyb2c.jalo;

import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.europe1.channel.strategies.RetrieveChannelStrategy;
import de.hybris.platform.europe1.constants.Europe1Tools;
import de.hybris.platform.europe1.constants.GeneratedEurope1Constants;
import de.hybris.platform.europe1.enums.PriceRowChannel;
import de.hybris.platform.europe1.jalo.AbstractDiscountRow;
import de.hybris.platform.europe1.jalo.DiscountRow;
import de.hybris.platform.europe1.jalo.Europe1PriceFactory;
import de.hybris.platform.europe1.jalo.PDTRowsQueryBuilder;
import de.hybris.platform.europe1.jalo.PriceRow;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.c2l.Country;
import de.hybris.platform.jalo.c2l.Currency;
import de.hybris.platform.jalo.enumeration.EnumerationValue;
import de.hybris.platform.jalo.flexiblesearch.FlexibleSearch;
import de.hybris.platform.jalo.order.AbstractOrder;
import de.hybris.platform.jalo.order.AbstractOrderEntry;
import de.hybris.platform.jalo.order.price.JaloPriceFactoryException;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.jalo.product.Unit;
import de.hybris.platform.jalo.user.User;
import de.hybris.platform.subscriptionservices.jalo.SubscriptionTerm;
import de.hybris.platform.util.PriceValue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.base.Preconditions;
import com.wiley.core.jalo.WileyEurope1PriceFactory;
import com.wiley.core.servicelayer.i18n.impl.WileycomI18NServiceImpl;
import com.wiley.core.wileyb2c.order.Wileyb2cPriceFactory;

import static com.wiley.core.constants.WileyCoreConstants.CURRENT_COUNTRY_SESSION_ATTR;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


/**
 * Price factory with specific logic for Wiley B2C.
 */
public class Wileyb2cEurope1PriceFactory extends WileyEurope1PriceFactory implements Wileyb2cPriceFactory
{
	@Autowired
	private RetrieveChannelStrategy retrieveChannelStrategy;

	@Autowired
	private WileyB2CPriceFactorySubscriptionHelper b2cPriceFactorySubscriptionHelper;

	@Autowired
	private WileycomI18NServiceImpl wileycomI18NService;

	@Override
	protected List<PriceRow> filterPriceRows(final List<PriceRow> priceRows)
	{
		final Currency sessionCurrency = getSession().getSessionContext().getCurrency();
		final Country sessionCountry = getSession().getSessionContext().getAttribute("sessionCountry");

		List<PriceRow> filteredPriceRows = filterPriceRowsByCountryAndCurrency(priceRows, sessionCurrency, sessionCountry);

		filteredPriceRows = super.filterPriceRows(filteredPriceRows);
		return filteredPriceRows;
	}

	protected List<PriceRow> filterPriceRowsForCountry(final List<PriceRow> priceRows, final Country country)
	{
		CountryModel countryModel = getModelService().toModelLayer(country);

		//TODO-1808 review the fix 
		if (countryModel == null)
		{
			countryModel = wileycomI18NService.getDefaultCountry();
		}

		List<CountryModel> fallbackCountries = wileycomI18NService.getFallbackCountries(countryModel);

		List<PriceRow> filteredPriceRows = new ArrayList<>();

		for (CountryModel fallbackCountryModel : fallbackCountries)
		{
			List<PriceRow> fallbackCountryPriceRows = priceRows.stream()
					.filter(priceRow -> priceRowHasSameCountry(priceRow, fallbackCountryModel))
					.collect(Collectors.toList());
			if (!fallbackCountryPriceRows.isEmpty())
			{
				filteredPriceRows.addAll(fallbackCountryPriceRows);
				break;
			}
		}

		if (filteredPriceRows.isEmpty())
		{
			List<PriceRow> globalPriceRows = priceRows.stream()
					.filter(priceRow -> priceRowHasNoCountry(priceRow))
					.collect(Collectors.toList());
			filteredPriceRows.addAll(globalPriceRows);
		}


		return filteredPriceRows;
	}

	private boolean priceRowHasSameCountry(final PriceRow priceRow, final CountryModel country)
	{
		PriceRowModel priceRowModel = getModelService().toModelLayer(priceRow);
		return country != null && country.getIsocode() != null
				&& priceRowModel != null && priceRowModel.getCountry() != null
				&& country.getIsocode().equals(priceRowModel.getCountry().getIsocode());
	}

	private boolean priceRowHasNoCountry(final PriceRow priceRow)
	{
		PriceRowModel priceRowModel = getModelService().toModelLayer(priceRow);
		return priceRowModel != null && priceRowModel.getCountry() == null;
	}


	@Override
	protected PriceRow matchPriceRowForPrice(final SessionContext ctx, final Date date, final AbstractOrderEntry orderEntry)
			throws JaloPriceFactoryException
	{
		AbstractOrder order = orderEntry.getOrder(ctx);
		if (b2cPriceFactorySubscriptionHelper.isFreeTrialSubscriptionEntry(orderEntry))
		{
			getZeroPriceValue(order.getCurrency(ctx), order.isNet());
		}

		User user = order.getUser();
		Product product = orderEntry.getProduct(ctx);
		boolean giveAwayMode = orderEntry.isGiveAway(ctx);
		long quantity = orderEntry.getQuantity(ctx);
		boolean net = order.isNet();
		EnumerationValue productGroup = getPPG(ctx, product);
		EnumerationValue userGroup = getUPG(ctx, user);
		Unit unit = orderEntry.getUnit(ctx);
		Currency currency = order.getCurrency(ctx);

		validateMatchPriceRowForPrice(product, productGroup, user, userGroup, unit, currency, date);

		Collection<PriceRow> rows = queryPriceRows4Price(ctx, product, productGroup, user,
				userGroup);
		if (CollectionUtils.isEmpty(rows))
		{
			return null;
		}

		final Country sessionCountry = extractCountry(ctx);
		final PriceRowChannel channel = retrieveChannelStrategy.getChannel(ctx);

		rows = filterPriceRows4Price(rows, quantity, unit, currency, date,
				giveAwayMode, channel, sessionCountry);

		if (b2cPriceFactorySubscriptionHelper.isSubscriptionProduct(product))
		{
			SubscriptionTerm subscriptionTerm = b2cPriceFactorySubscriptionHelper.getSubscriptionTerm(orderEntry, ctx);
			Preconditions.checkNotNull(subscriptionTerm, "SubscriptionTerm should be attached to order entry "
					+ "for subscription product");
			rows = filterPriceRowsForSubscription(rows, subscriptionTerm, product);
		}

		if (CollectionUtils.isEmpty(rows))
		{
			return null;
		}

		final List<PriceRow> rowsAsList = new ArrayList<>(rows);
		Collections.sort(rowsAsList, new WileyB2CPriceRowMatchComparator(currency, net, unit));
		return rowsAsList.get(0);
	}

	@Override
	public PriceValue getBasePrice(final AbstractOrderEntry entry) throws JaloPriceFactoryException
	{
		final SessionContext ctx = getSession().getSessionContext();
		final boolean giveAwayMode = entry.isGiveAway(ctx);
		final boolean entryIsRejected = entry.isRejected(ctx);
		if (!giveAwayMode && !entryIsRejected)
		{
			if (b2cPriceFactorySubscriptionHelper.isFreeTrialSubscriptionEntry(entry))
			{
				AbstractOrder order = entry.getOrder(ctx);
				return getZeroPriceValue(order.getCurrency(ctx), order.isNet());
			}
		}
		return super.getBasePrice(entry);
	}


	private Collection<PriceRow> filterPriceRowsForSubscription(
			final Collection<PriceRow> priceRows,
			final SubscriptionTerm subscriptionTerm,
			final Product product
	)
	{

		if (CollectionUtils.isEmpty(priceRows))
		{
			return Collections.emptyList();
		}

		if (subscriptionTerm == null)
		{
			return priceRows;
		}

		return priceRows
				.stream()
				.filter(row -> b2cPriceFactorySubscriptionHelper.isPriceRowContainsSubscriptionTerm(subscriptionTerm, row))
				.filter(row -> b2cPriceFactorySubscriptionHelper.isPriceRowAllowedForSubscriptionProduct(product, row))
				.collect(Collectors.toList());
	}

	private void validateMatchPriceRowForPrice(final Product product, final EnumerationValue productGroup, final User user,
			final EnumerationValue userGroup, final Unit unit, final Currency currency, final Date date)
			throws JaloPriceFactoryException
	{
		if ((product == null) && (productGroup == null))
		{
			throw new JaloPriceFactoryException(
					"cannot match price without product and product group - at least one must be present", 0);
		}

		if ((user == null) && (userGroup == null))
		{
			throw new JaloPriceFactoryException(
					"cannot match price without user and user group - at least one must be present", 0);
		}

		if (currency == null)
		{
			throw new JaloPriceFactoryException("cannot match price without currency", 0);
		}

		if (date == null)
		{
			throw new JaloPriceFactoryException("cannot match price without date", 0);
		}

		if (unit == null)
		{
			throw new JaloPriceFactoryException("cannot match price without unit", 0);
		}
	}

	/**
	 * Except OOTB cases, in addition, method filters price rows by country.
	 *
	 * @param rows
	 * 		priceRows for filtering
	 * @param qty
	 * 		minimum quantity that should be allowed by price
	 * @param unit
	 * 		price {@link Unit}
	 * @param curr
	 * 		price {@link Currency}
	 * @param date
	 * 		matching of date to fit into price date range
	 * @param giveAwayMode
	 * 		give away price
	 * @param channel
	 * 		required {@link PriceRowChannel}
	 * @param country
	 * 		match price from collection by specified {@link Country}.
	 * @return Method returns list of prices with one of the following conditions:<ul>
	 * <li>PriceRow.country is null</li>
	 * <li>PriceRow.country is equal to <code>country</code> parameter</li>
	 * </ul>
	 */
	protected List<PriceRow> filterPriceRows4Price(final Collection<PriceRow> rows,
			final long qty, final Unit unit, final Currency curr, final Date date,
			final boolean giveAwayMode, final PriceRowChannel channel,
			final Country country)
	{

		List<PriceRow> filteredRows = super.filterPriceRows4Price(rows, qty, unit, curr, date, giveAwayMode, channel);

		if (filteredRows.isEmpty())
		{
			return Collections.EMPTY_LIST;
		}

		return filterPriceRowsByCountryAndCurrency(filteredRows, curr, country);

	}

	protected List<PriceRow> filterPriceRowsByCountryAndCurrency(final List<PriceRow> rows, final Currency currency,
			final Country country)
	{
		List<PriceRow> result = filterPriceRowsForCurrency(rows, currency);
		result = filterPriceRowsForCountry(result, country);
		return result;
	}

	public List<PriceInformation> filterPriceInfoBySubscriptionTerm(final Collection<PriceInformation> priceInformations,
			@NotNull final SubscriptionTerm subscriptionTerm)
	{
		validateParameterNotNullStandardMessage("subscriptionTerm", subscriptionTerm);

		if (CollectionUtils.isEmpty(priceInformations))
		{
			return Collections.emptyList();
		}
		return priceInformations.stream().filter(priceInfo ->
				{
					final PriceRow priceRow = (PriceRow) priceInfo.getQualifierValue("pricerow");
					return b2cPriceFactorySubscriptionHelper.isPriceRowContainsSubscriptionTerm(subscriptionTerm, priceRow);
				}
		).collect(Collectors.toList());
	}

	@Override
	protected List getPriceInformations(final SessionContext ctx, final Product product, final EnumerationValue productGroup,
			final User user, final EnumerationValue userGroup, final Currency curr, final boolean net, final Date date,
			final Collection taxValues) throws JaloPriceFactoryException
	{
		if (b2cPriceFactorySubscriptionHelper.isSubscriptionProduct(product))
		{
			final Currency sessionCurrency = getSession().getSessionContext().getCurrency();
			List<PriceRow> priceRows = this.filterPriceRowsForCurrency(
					this.matchPriceRowsForInfo(ctx, product, productGroup, user, userGroup, curr, date, net),
					sessionCurrency
			);
			return convertPriceRowsToPriceInfos(ctx, product, user, curr, net, date, taxValues, priceRows);

		}
		return super.getPriceInformations(ctx, product, productGroup, user, userGroup, curr, net, date, taxValues);
	}

	@Override
	public List<PriceRow> matchPriceRowsForInfo(final SessionContext ctx, final Product product,
			final EnumerationValue productGroup, final User user, final EnumerationValue userGroup, final Currency currency,
			final Date date, final boolean net) throws JaloPriceFactoryException
	{
		if (product == null && productGroup == null)
		{
			throw new JaloPriceFactoryException(
					"cannot match price info without product and product group - at least one must be present", 0);
		}
		else if (user == null && userGroup == null)
		{
			throw new JaloPriceFactoryException(
					"cannot match price info without user and user group - at least one must be present", 0);
		}
		else if (currency == null)
		{
			throw new JaloPriceFactoryException("cannot match price info without currency", 0);
		}
		else if (date == null)
		{
			throw new JaloPriceFactoryException("cannot match price info without date", 0);
		}
		else
		{
			Collection rows = this.queryPriceRows4Price(ctx, product, productGroup, user, userGroup);
			if (!rows.isEmpty())
			{
				final Country sessionCountry = extractCountry(ctx);
				PriceRowChannel channel = this.retrieveChannelStrategy.getChannel(ctx);
				ArrayList ret = new ArrayList(rows);
				if (ret.size() > 1)
				{
					Collections.sort(ret, new WileyB2CPriceRowInfoComparator(currency, net));
				}

				return this.filterPriceRows4Info(ret, currency, date, channel, sessionCountry);
			}
			else
			{
				return Collections.EMPTY_LIST;
			}
		}
	}



	protected List<PriceRow> filterPriceRows4Info(final Collection<PriceRow> rows, final Currency curr, final Date date,
			final PriceRowChannel channel, final Country country)
	{

		List<PriceRow> filteredRows = super.filterPriceRows4Info(rows, curr, date, channel);

		if (rows.isEmpty())
		{
			return Collections.EMPTY_LIST;
		}
		else
		{
			return filterPriceRowsByCountryAndCurrency(filteredRows, curr, country);
		}
	}

	private Country extractCountry(final SessionContext ctx)
	{
		if (ctx.getAttribute(CURRENT_COUNTRY_SESSION_ATTR) != null)
		{
			return (Country) ctx.getAttribute(CURRENT_COUNTRY_SESSION_ATTR);
		}

		return null;
	}

	@Override
	public List<AbstractDiscountRow> matchDiscountRows(@Nonnull final Product product,
			@Nonnull final EnumerationValue productGroup, @Nullable final EnumerationValue userGroup, final int maxCount)
			throws JaloPriceFactoryException
	{
		if (userGroup == null)
		{
			throw new JaloPriceFactoryException(
					"cannot match discounts without user group", 0);
		}

		final List<AbstractDiscountRow> discountRows = (List<AbstractDiscountRow>) this
				.queryDiscounts4Price(this.getSession().getSessionContext(), product, productGroup, userGroup);

		return discountRows;
	}


	/**
	 * OOTB_CODE
	 * It's based on {@link Europe1PriceFactory#queryDiscounts4Price(...)} but without user.
	 */
	protected Collection<? extends AbstractDiscountRow> queryDiscounts4Price(final SessionContext ctx, final Product product,
			final EnumerationValue productGroup, final EnumerationValue userGroup)
	{
		boolean global = product == null && productGroup == null;
		String discountRowTypeCode =
				global ? GeneratedEurope1Constants.TC.GLOBALDISCOUNTROW : GeneratedEurope1Constants.TC.DISCOUNTROW;
		PK productPk = product == null ? null : product.getPK();
		PK productGroupPk = productGroup == null ? null : productGroup.getPK();
		PK userGroupPk = userGroup == null ? null : userGroup.getPK();
		String productId = this.extractProductId(ctx, product);
		PDTRowsQueryBuilder builder = this.getPDTRowsQueryBuilderFor(discountRowTypeCode);
		PDTRowsQueryBuilder.QueryWithParams
				queryAndParams = builder.withAnyProduct().withAnyUser().withProduct(productPk).withProductId(productId)
				.withProductGroup(productGroupPk).withUserGroup(userGroupPk).build();
		return FlexibleSearch.getInstance().search(ctx, queryAndParams.getQuery(), queryAndParams.getParams(), DiscountRow.class)
				.getResult();
	}


	/**
	 * This comparator treats Country in PriceRow. Prices with
	 * country will be first in result list.
	 * <br>
	 * Class extends OOTB {@link de.hybris.platform.europe1.jalo.Europe1PriceFactory.PriceRowInfoComparator
	 * PriceRowInfoComparator}.
	 */

	private class WileyB2CPriceRowInfoComparator
			extends Europe1PriceFactory.PriceRowInfoComparator implements PriceFactoryComparator
	{

		WileyB2CPriceRowInfoComparator(final Currency curr, final boolean net)
		{
			super(curr, net);
		}

		public int compare(final PriceRow row1, final PriceRow row2)
		{
			final Integer preChecks = doComparatorChecks(row1, row2);
			if (preChecks != null)
			{
				return preChecks;
			}

			return super.compare(row1, row2);
		}
	}

	/**
	 * This comparator treats Country in PriceRow. Prices with
	 * country will be first in result list.
	 * <br>
	 * Class extends OOTB {@link de.hybris.platform.europe1.jalo.Europe1PriceFactory.PriceRowMatchComparator
	 * PriceRowMatchComparator}.
	 */
	private class WileyB2CPriceRowMatchComparator
			extends Europe1PriceFactory.PriceRowMatchComparator implements PriceFactoryComparator
	{

		WileyB2CPriceRowMatchComparator(final Currency curr, final boolean net,
				final Unit unit)
		{

			super(curr, net, unit);
		}

		@Override
		public int compare(final PriceRow row1, final PriceRow row2)
		{

			final Integer preChecks = doComparatorChecks(row1, row2);
			if (preChecks != null)
			{
				return preChecks;
			}

			return super.compare(row1, row2);
		}

	}

	private interface PriceFactoryComparator extends Comparator<PriceRow>
	{
		default Integer doComparatorChecks(final PriceRow row1, final PriceRow row2)
		{
			final Object country1 = row1.getProperty(PriceRowModel.COUNTRY);
			final Object country2 = row2.getProperty(PriceRowModel.COUNTRY);

			if (country1 != null && country2 == null)
			{
				return -1;
			}

			if (country1 == null && country2 != null)
			{
				return 1;
			}
			return null;
		}
	}

	private List<PriceInformation> convertPriceRowsToPriceInfos(final SessionContext ctx, final Product product, final User user,
			final Currency curr, final boolean net, final Date date, final Collection taxValues, final List<PriceRow> priceRows)
			throws JaloPriceFactoryException
	{
		ArrayList<PriceInformation> priceInfos = new ArrayList<>(priceRows.size());
		Collection theTaxValues = taxValues;
		ArrayList<PriceInformation> defaultPriceInfos = new ArrayList<>(priceRows.size());
		PriceRowChannel channel = this.retrieveChannelStrategy.getChannel(ctx);

		for (PriceRow row : priceRows)
		{
			PriceInformation pInfo = Europe1Tools.createPriceInformation(row, curr);
			if (pInfo.getPriceValue().isNet() != net)
			{
				if (theTaxValues == null)
				{
					theTaxValues = Europe1Tools.getTaxValues(
							this.getTaxInformations(product, this.getPTG(ctx, product), user, this.getUTG(ctx, user), date));
				}

				pInfo = new PriceInformation(pInfo.getQualifiers(), pInfo.getPriceValue().getOtherPrice(theTaxValues));
			}

			if (row.getChannel() == null)
			{
				defaultPriceInfos.add(pInfo);
			}

			if (channel == null && row.getChannel() == null)
			{
				priceInfos.add(pInfo);
			}
			else if (channel != null && row.getChannel() != null
					&& row.getChannel().getCode().equalsIgnoreCase(channel.getCode()))
			{
				priceInfos.add(pInfo);
			}
		}

		if (priceInfos.size() == 0)
		{
			return defaultPriceInfos;
		}

		return priceInfos;
	}
}
