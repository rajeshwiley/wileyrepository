package com.wiley.core.wiley.ruleengine.impl;

import de.hybris.platform.ruleengine.RuleEvaluationContext;
import de.hybris.platform.ruleengine.RuleEvaluationResult;
import de.hybris.platform.ruleengine.enums.DroolsSessionType;
import de.hybris.platform.ruleengine.exception.DroolsInitializationException;
import de.hybris.platform.ruleengine.impl.DefaultPlatformRuleEngineService;
import de.hybris.platform.ruleengine.model.DroolsRuleEngineContextModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

import org.drools.core.command.runtime.AdvanceSessionTimeCommand;
import org.drools.core.command.runtime.rule.FireAllRulesCommand;
import org.kie.api.builder.ReleaseId;
import org.kie.api.command.BatchExecutionCommand;
import org.kie.api.command.Command;
import org.kie.api.runtime.ExecutionResults;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.rule.AgendaFilter;
import org.kie.internal.command.CommandFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wiley.ruleengine.model.WileyDroolsRuleEngineContextModel;


public class WileyPseudoClockRuleEngineService extends DefaultPlatformRuleEngineService
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyPseudoClockRuleEngineService.class);
	private final Map<ReleaseId, KieContainer> kieContainerMap;

	public WileyPseudoClockRuleEngineService()
	{
		super();
		this.kieContainerMap = new HashMap<>();
	}

	/**
	 * TODO-REFACT-1808 Review changes related to deletion of customization about ClockType.PSEUDO_CLOCK and
	 * the corresponding tests in WileyPseudoClockRuleEngineServiceUnitTest.
	 */
	@Override
	public RuleEvaluationResult evaluate(final RuleEvaluationContext context)
	{
		if (LOG.isDebugEnabled() && Objects.nonNull(context.getFacts()))
		{
			LOG.debug("Rule evaluation triggered with the facts: {}", context.getFacts());
		}

		RuleEvaluationResult ruleEvaluationResult;

		try
		{
			this.getRuleEngineContainerRegistry().lockReadingRegistry();
			RuleEvaluationResult result = new RuleEvaluationResult();
			ReleaseId deployedReleaseId = getKieSessionHelper().getDeployedKieModuleReleaseId(context);
			KieContainer kContainer = getRuleEngineContainerRegistry().getActiveContainer(deployedReleaseId);

			if (Objects.isNull(kContainer))
			{
				LOG.warn("KieContainer with releaseId [{}] was not found. Trying to look up for closest matching version...",
						deployedReleaseId);
				ReleaseId tryDeployedReleaseId = getRuleEngineContainerRegistry()
						.lookupForDeployedRelease(
								new String[] { deployedReleaseId.getGroupId(), deployedReleaseId.getArtifactId() })
						.orElseThrow(() -> new DroolsInitializationException(
								"Cannot complete the evaluation: rule engine was not initialized for releaseId ["
										+ deployedReleaseId + "]"));
				LOG.info("Found KieContainer with releaseId [{}]", tryDeployedReleaseId);
				kContainer = getRuleEngineContainerRegistry().getActiveContainer(tryDeployedReleaseId);
			}

			AgendaFilter agendaFilter = context.getFilter() instanceof AgendaFilter ? (AgendaFilter) context.getFilter() : null;
			this.getRuleEngineCacheService().provideCachedEntities(context);
			DroolsRuleEngineContextModel ruleEngineContext = (DroolsRuleEngineContextModel) context.getRuleEngineContext();

			// A customization
			BatchExecutionCommand command = getCommandObject(context, ruleEngineContext, agendaFilter);

			DroolsSessionType sessionType = ruleEngineContext.getKieSession().getSessionType();
			Supplier<ExecutionResults> executionResultsSupplier = DroolsSessionType.STATEFUL.equals(sessionType) ?
					executionResultsSupplierWithStatefulSession(kContainer, command, context) :
					executionResultsSupplierWithStatelessSession(kContainer, command, context);
			result.setExecutionResult(executionResultsSupplier.get());
			ruleEvaluationResult = result;
		}
		finally
		{
			this.getRuleEngineContainerRegistry().unlockReadingRegistry();
		}

		return ruleEvaluationResult;
	}

	protected BatchExecutionCommand getCommandObject(final RuleEvaluationContext context,
			final DroolsRuleEngineContextModel ruleEngineContext,
			final AgendaFilter agendaFilter)
	{
		List<Command> commands = new ArrayList<>();

		if (ruleEngineContext instanceof WileyDroolsRuleEngineContextModel)
		{
			// OOTB customization to use date passed via rule engine context parameter
			Date date = ((WileyDroolsRuleEngineContextModel) ruleEngineContext).getDate();
			final AdvanceSessionTimeCommand timeCommand = new AdvanceSessionTimeCommand(date.getTime(), TimeUnit.MILLISECONDS);
			commands.add(timeCommand);
		}

		return makeBatchExecutionCommand(context, agendaFilter, commands);
	}

	protected BatchExecutionCommand makeBatchExecutionCommand(final RuleEvaluationContext context,
			final AgendaFilter agendaFilter, final List<Command> commands)
	{
		commands.add(CommandFactory.newInsertElements(context.getFacts()));
		FireAllRulesCommand fireAllRulesCommand = Objects.nonNull(agendaFilter) ?
				new FireAllRulesCommand(agendaFilter) :
				new FireAllRulesCommand();
		LOG.debug("Adding command [{}]", fireAllRulesCommand);
		commands.add(fireAllRulesCommand);

		return CommandFactory.newBatchExecution(commands);
	}
}