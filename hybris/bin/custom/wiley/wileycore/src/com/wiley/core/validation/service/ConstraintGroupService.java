package com.wiley.core.validation.service;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.validation.model.constraints.ConstraintGroupModel;

import java.util.Collection;


public interface ConstraintGroupService 
{
	ConstraintGroupModel getConstraintGroupForId(String constraintGroupId);

	/**
	 * Returns ConstraintGroups attached to item's catalog. For now only supported catalog types are Product and Category.
	 * @param item
	 * @return collection of constraint groups or empty list if item is not supported or catalog has no constraint groups
	 */
	Collection<ConstraintGroupModel> getCatalogSpecificConstraintGroupForItem(ItemModel item);

}
