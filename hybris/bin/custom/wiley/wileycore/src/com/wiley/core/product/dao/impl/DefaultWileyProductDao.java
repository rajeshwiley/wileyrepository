/**
 *
 */
package com.wiley.core.product.dao.impl;

import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.constants.CategoryConstants;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.link.LinkModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.subscriptionservices.model.SubscriptionProductModel;
import de.hybris.platform.subscriptionservices.model.SubscriptionTermModel;
import de.hybris.platform.util.FlexibleSearchUtils;
import de.hybris.platform.variants.model.VariantValueCategoryModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Required;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import com.wiley.core.model.WileyProductModel;
import com.wiley.core.model.WileyVariantProductModel;
import com.wiley.core.product.dao.WileyProductDao;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


/**
 *
 */
public class DefaultWileyProductDao implements WileyProductDao
{

	/**
	 * The constant QUERY_PARAM_CODE.
	 */
	public static final String QUERY_PARAM_CODE = "code";
	public static final String QUERY_PARAM_CATEGORY = "category";
	public static final String QUERY_PARAM_BASE_PRODUCT_CATEGORY = "baseProductCategoryCode";
	public static final String QUERY_PARAM_VARIANT_VALUE_CATEGORY = "variantValueCategoryCode";

	private FlexibleSearchService flexibleSearchService;

	@Override
	public List<WileyProductModel> findWileyProductsByCode(final String code)
	{
		validateParameterNotNull(code, "WileyProduct code must not be null!");

		StringBuilder queryString = new StringBuilder();
		queryString.append("SELECT {" + WileyProductModel.PK + "} FROM {" + WileyProductModel._TYPECODE + "!} WHERE {"
				+ WileyProductModel.CODE + "} = ?code");
		final FlexibleSearchQuery query = new FlexibleSearchQuery(queryString);
		query.addQueryParameter(QUERY_PARAM_CODE, code);
		final SearchResult<WileyProductModel> result = flexibleSearchService.search(query);

		return result.getResult();

	}

	public List<WileyProductModel> findWileyProductsFromCategoryRoot(final String categoryCode)
	{
		validateParameterNotNull(categoryCode, "category must not be null!");

		final StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("SELECT {p:").append(WileyProductModel.PK).append("} ");
		stringBuilder.append("FROM {").append(WileyProductModel._TYPECODE).append(" AS p ");
		stringBuilder.append("JOIN ").append(CategoryConstants.Relations.CATEGORYPRODUCTRELATION).append(" AS l ");
		stringBuilder.append("ON {l:").append(LinkModel.TARGET).append("}={p:").append(WileyProductModel.PK).append("} } ");
		stringBuilder.append("WHERE ").append("{l:" + LinkModel.SOURCE + "} IN ");
		stringBuilder.append("({{SELECT {cat." + CategoryModel.PK + "} ");
		stringBuilder.append("FROM {" + CategoryModel._TYPECODE + " AS cat} ");
		stringBuilder.append("WHERE {cat." + CategoryModel.CODE + "} = ?category}})");
		stringBuilder.append(" AND  ( {p:" + WileyProductModel.ONLINEDATE + "} IS NULL OR {p:");
		stringBuilder.append(WileyProductModel.ONLINEDATE + "} <= ?session.user." + UserModel.CURRENTDATE);
		stringBuilder.append(" ) AND ( {p:" + WileyProductModel.OFFLINEDATE + "} IS NULL OR {p:");
		stringBuilder.append(WileyProductModel.OFFLINEDATE + "} > ?session.user." + UserModel.CURRENTDATE + " ) ");

		// We need to provide OOTB ordering according to ECSC-23078.
		// The same ORDER BY is used when CategoryModel.getProducts() is executed provided that products were lazy loaded
		// in a category
		stringBuilder.append(" ORDER BY {l:" + LinkModel.SEQUENCENUMBER + "} ASC ,{l:" + LinkModel.PK + "} ASC");

		final FlexibleSearchQuery query = new FlexibleSearchQuery(stringBuilder.toString());
		query.addQueryParameter(QUERY_PARAM_CATEGORY, categoryCode);

		final SearchResult<WileyProductModel> result = flexibleSearchService.search(query);
		return result.getResult();
	}

	@Override
	public List<ProductModel> findWileyProductsFromCategories(final String baseProductCategoryCode,
			final String variantValueCategoryCode)
	{
		validateParameterNotNull(baseProductCategoryCode, "baseProductCategoryCode must not be null!");
		validateParameterNotNull(variantValueCategoryCode, "variantValueCategoryCode must not be null!");

		/*
			 query is based on the assumption that product should be in variantValueCategories, so
			 	- usually it means product is variant
			  	- if it's variant we have to check that baseProduct is in baseProductCategory, not product directly
		 */
		final String queryString = "SELECT DISTINCT {p:" + WileyVariantProductModel.PK + " } FROM {VariantProduct as p "
				+ "JOIN " + CategoryConstants.Relations.CATEGORYPRODUCTRELATION + " AS l "
				+ "ON {l:" + LinkModel.TARGET + "}={p:" + WileyVariantProductModel.PK + "}} "
				+ "WHERE "

				+ "{p:" + WileyVariantProductModel.BASEPRODUCT + "} IN "
				+ "({{SELECT {l:" + LinkModel.TARGET + "} FROM {" + CategoryConstants.Relations.CATEGORYPRODUCTRELATION
				+ " AS l} "
				+ "WHERE {l:" + LinkModel.SOURCE + "} IN "
				+ "({{SELECT {cat:" + CategoryModel.PK + "} FROM {Category AS cat} "
				+ "WHERE {cat:" + CategoryModel.CODE + "}=?baseProductCategoryCode}}) }}) "

				+ "AND "

				+ "{p:" + WileyVariantProductModel.PK + "} IN "
				+ "({{SELECT {l:" + LinkModel.TARGET + "} FROM {" + CategoryConstants.Relations.CATEGORYPRODUCTRELATION
				+ " AS l} "
				+ "WHERE {l:" + LinkModel.SOURCE + "} IN "
				+ "({{SELECT {cat:" + VariantValueCategoryModel.PK + "} FROM {VariantValueCategory AS cat} "
				+ "WHERE {cat:" + VariantValueCategoryModel.CODE + "} = ?variantValueCategoryCode}}) }})"

				+ " ORDER BY {p:" + WileyVariantProductModel.NAME + "}";

		final FlexibleSearchQuery query = new FlexibleSearchQuery(queryString);
		query.addQueryParameter(QUERY_PARAM_BASE_PRODUCT_CATEGORY, baseProductCategoryCode);
		query.addQueryParameter(QUERY_PARAM_VARIANT_VALUE_CATEGORY, variantValueCategoryCode);

		final SearchResult<ProductModel> result = flexibleSearchService.search(query);
		return result.getResult();
	}

	@Override
	public SearchResult<ProductModel> findProductsByCategoryExcludingSubCategories(final CategoryModel category)
	{
		validateParameterNotNull(category, "No category specified");

		final Map<String, Object> params = new HashMap<String, Object>();

		final StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("SELECT {p:").append(ProductModel.PK).append("} ");
		stringBuilder.append("FROM {").append(ProductModel._TYPECODE).append(" AS p ");
		stringBuilder.append("JOIN ").append(CategoryConstants.Relations.CATEGORYPRODUCTRELATION).append(" AS l ");
		stringBuilder.append("ON {l:").append(LinkModel.TARGET).append("}={p:").append(ProductModel.PK).append("} } ");

		final Collection<CategoryModel> categories = new ArrayList<CategoryModel>();
		categories.add(category);

		final String inPart = FlexibleSearchUtils.buildOracleCompatibleCollectionStatement(//
				"{l:" + LinkModel.SOURCE + "} IN (?cat)", "cat", "AND", categories, params);

		stringBuilder.append("WHERE ").append(inPart);
		stringBuilder.append(" ORDER BY {p:name}");

		final FlexibleSearchQuery query = new FlexibleSearchQuery(stringBuilder.toString());
		query.addQueryParameters(params);

		return flexibleSearchService.search(query);
	}

	@Override
	public List<SubscriptionProductModel> findProductsByIsbnAndTerm(final String isbn, final String term,
			final CatalogVersionModel catalogVersionModel)
	{
		validateParameterNotNull(isbn, "No isbn specified");
		validateParameterNotNull(term, "No term specified");
		validateParameterNotNull(catalogVersionModel, "No catalogVersionModel specified");

		return flexibleSearchService.<SubscriptionProductModel> search(
				"SELECT {p:" + SubscriptionProductModel.PK + "} FROM {" + SubscriptionProductModel._TYPECODE + " AS p}"
						+ " WHERE {p:" + SubscriptionProductModel.ISBN + "} = ?isbn"
						+ " AND {p:" + SubscriptionProductModel.CATALOGVERSION + "} = ?catalogVersion"
						+ " AND {p:" + SubscriptionProductModel.SUBSCRIPTIONTERM + "} IN"
						+ " ({{ SELECT {st:" + SubscriptionTermModel.PK + "} FROM {" + SubscriptionTermModel._TYPECODE + " AS st}"
						+ " WHERE {st:" + SubscriptionTermModel.ID + "} = ?term }})",
				ImmutableMap.of("isbn", isbn, "term", term, "catalogVersion", catalogVersionModel)).getResult();
	}

	@Override
	public List<ProductModel> findProductsByIsbn(final String isbn, final CatalogVersionModel catalogVersionModel)
	{
		validateParameterNotNull(isbn, "No isbn specified");
		validateParameterNotNull(catalogVersionModel, "No catalogVersionModel specified");

		return flexibleSearchService.<ProductModel> search(
				"SELECT {" + ProductModel.PK + "} FROM {" + ProductModel._TYPECODE + "}"
						+ " WHERE {" + ProductModel.ISBN + "} = ?isbn"
						+ " AND {" + ProductModel.CATALOGVERSION + "} = ?catalogVersion",
				ImmutableMap.of("isbn", isbn, "catalogVersion", catalogVersionModel)).getResult();
	}

	@Override
	public List<ProductModel> findProductsByIsbn(final String isbn)
	{
		validateParameterNotNull(isbn, "No isbn specified");

		return flexibleSearchService.<ProductModel> search(
				"SELECT {" + ProductModel.PK + "} FROM {" + ProductModel._TYPECODE + "}"
						+ " WHERE {" + ProductModel.ISBN + "} = ?isbn",
				ImmutableMap.of("isbn", isbn)).getResult();
	}

	@Nonnull
	@Override
	public List<ProductModel> findProductsToValidate(@Nonnull final ArticleApprovalStatus approvalStatus,
			@Nonnull final CatalogVersionModel catalogVersion)
	{
		Preconditions.checkNotNull(approvalStatus);
		Preconditions.checkNotNull(catalogVersion);

		return flexibleSearchService.<ProductModel> search(
				"SELECT {" + ProductModel.PK + "} FROM {" + ProductModel._TYPECODE + "}"
						+ " WHERE {" + ProductModel.APPROVALSTATUS + "} = ?approvalStatus"
						+ " AND {" + ProductModel.CATALOGVERSION + "} = ?catalogVersion",
				ImmutableMap.of("approvalStatus", approvalStatus, "catalogVersion", catalogVersion)).getResult();
	}

	@Override
	public List<ProductModel> findProductsByMediaContainerInGalleryImages(final MediaContainerModel mediaContainer)
	{
		Preconditions.checkNotNull(mediaContainer);

		return flexibleSearchService.<ProductModel> search(
				"SELECT {" + ProductModel.PK + "} FROM {" + ProductModel._TYPECODE + " AS p}"
						+ " WHERE {p:" + ProductModel.GALLERYIMAGES + "} LIKE '%" + mediaContainer.getPk() + "%'").getResult();
	}

	@Override
	public List<ProductModel> findProductsByMediaContainerInBackgroudImage(final MediaContainerModel mediaContainer)
	{
		Preconditions.checkNotNull(mediaContainer);

		return flexibleSearchService.<ProductModel> search(
				"SELECT {" + ProductModel.PK + "} FROM {" + ProductModel._TYPECODE + " AS p}"
						+ " WHERE {p:" + ProductModel.BACKGROUNDIMAGE + "}=?mediaContainer",
				ImmutableMap.of("mediaContainer", mediaContainer)).getResult();
	}

	/**
	 * Sets flexible search service.
	 *
	 * @param flexibleSearchService
	 * 		the flexible search service
	 */
	@Required
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}
}
