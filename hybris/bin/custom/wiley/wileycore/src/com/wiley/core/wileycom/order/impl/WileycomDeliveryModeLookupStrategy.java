package com.wiley.core.wileycom.order.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;

import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.wiley.core.exceptions.ExternalSystemException;
import com.wiley.core.integration.esb.EsbCartCalculationGateway;
import com.wiley.core.model.ExternalDeliveryModeModel;


/**
 * Strategy calls external service for getting supported delivery modes
 */
public class WileycomDeliveryModeLookupStrategy
{
	private static final Logger LOG = Logger.getLogger(WileycomDeliveryModeLookupStrategy.class);


	@Resource
	private EsbCartCalculationGateway esbCartCalculationGateway;

	/**
	 * Gets the list of external delivery modes for given order/cart
	 *
	 * @param abstractOrderModel
	 * @return list of delivery modes
	 */
	public List<ExternalDeliveryModeModel> getSelectableDeliveryModesForOrder(final AbstractOrderModel abstractOrderModel)
	{
		List<ExternalDeliveryModeModel> deliveryOptions = Collections.emptyList();
		if (abstractOrderModel.getDeliveryAddress() != null)
		{
			try
			{
				deliveryOptions = esbCartCalculationGateway.getExternalDeliveryModes(abstractOrderModel);
			}
			catch (final ExternalSystemException e)
			{
				//failed to get delivery modes - empty list will be returned
				LOG.warn("Failed to receive external delivery modes - list of available delivery modes is empty", e);
			}
		}
		return deliveryOptions;
	}
}
