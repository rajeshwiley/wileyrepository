package com.wiley.core.search.solrfacetsearch;

import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.solrfacetsearch.model.redirect.SolrFacetSearchKeywordRedirectModel;
import de.hybris.platform.solrfacetsearch.search.KeywordRedirectSorter;
import de.hybris.platform.solrfacetsearch.search.KeywordRedirectValue;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.solrfacetsearch.search.SolrFacetSearchKeywordDao;
import de.hybris.platform.solrfacetsearch.search.impl.DefaultSolrKeywordRedirectService;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

public class WileySolrKeywordRedirectService extends DefaultSolrKeywordRedirectService
{
	@Autowired
	private CommonI18NService commonI18NService;
	@Autowired
	private SolrFacetSearchKeywordDao solrFacetSearchKeywordDao;
	@Autowired
	private KeywordRedirectSorter keywordRedirectSorter;

	@Override
	public List<KeywordRedirectValue> getKeywordRedirect(final SearchQuery query)
	{
		final String theQuery = query.getUserQuery();
		String langIso = query.getLanguage();
		List<KeywordRedirectValue> result = new ArrayList();
		if (StringUtils.isBlank(langIso))
		{
			langIso = this.commonI18NService.getCurrentLanguage().getIsocode();
		}
		if (StringUtils.isNotBlank(theQuery))
		{
			result = returnMatchingRedirects(theQuery, query, langIso);
			if (result.isEmpty())
			{
				final LanguageModel language = commonI18NService.getLanguage(langIso);
				final List<LanguageModel> fallbackLanguages = language.getFallbackLanguages();
				for (final LanguageModel lang : fallbackLanguages)
				{
					result = returnMatchingRedirects(theQuery, query, lang.getIsocode());
					if (!result.isEmpty())
					{
						return result;
					}
				}
			}
		}

		return result;
	}

	private List<KeywordRedirectValue> returnMatchingRedirects(final String theQuery, final SearchQuery query,
			final String langIso)
	{
		final List<KeywordRedirectValue> result = new ArrayList();
		final List<SolrFacetSearchKeywordRedirectModel> redirects = findKeywordRedirects(query, langIso);

		for (final SolrFacetSearchKeywordRedirectModel redirect : redirects)
		{
			handleKeywordMatch(result, theQuery, redirect);
		}
		return result;
	}

	private List<SolrFacetSearchKeywordRedirectModel> findKeywordRedirects(final SearchQuery searchQuery,
			final String langIso)
	{
		final List result = solrFacetSearchKeywordDao.findKeywords(searchQuery.getFacetSearchConfig().getName(), langIso);
		return keywordRedirectSorter.sort(result);
	}

}
