package com.wiley.core.price.service;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.model.PriceRowModel;

import java.util.List;


public interface WileyPriceService
{

	/**
	 * Fetching the Price row for priceId
	 *
	 * @param priceId
	 * @return PriceRowModel
	 */
	PriceRowModel getPriceRowForPriceId(String priceId);

	/**
	 * Checking if price row already exist in database
	 *
	 * @param priceId
	 * @return boolean
	 */
	boolean isPriceRowAlreadyExist(String priceId);

	/**
	 * Return product prices list
	 *
	 * @param productModel
	 * 		product
	 * @param currencyModel
	 * 		currency model
	 * @return product price information
	 */
	List<PriceRowModel> getPricesForProduct(ProductModel productModel, CurrencyModel currencyModel);

	/**
	 * Save new price item. Checks existing prices effective dates and updates them accordingly
	 *
	 * @param price
	 */
	void savePriceUpdate(PriceRowModel price);
}
