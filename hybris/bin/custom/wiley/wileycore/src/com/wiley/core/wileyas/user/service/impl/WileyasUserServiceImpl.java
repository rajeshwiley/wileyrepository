package com.wiley.core.wileyas.user.service.impl;

import com.wiley.core.wileyas.user.service.WileyasUserService;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.user.impl.DefaultUserService;
import de.hybris.platform.servicelayer.util.ServicesUtil;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public class WileyasUserServiceImpl extends DefaultUserService implements WileyasUserService
{

	@Override
	public UserModel getUserForUID(final String userId) {
		ServicesUtil.validateParameterNotNull(userId, "The given userID is null!");
		try {
			return super.getUserForUID(userId.toLowerCase());
		}
		catch (UnknownIdentifierException e) {
			return super.getUserForUID(userId);
		}
	}


	@Override
	public boolean isUserExisting(final String uid) {
		try
		{
			return getUserForUID(uid) != null;
		}
		catch (UnknownIdentifierException ex)
		{
			return false;
		}
	}

	@Override
	public boolean isUserExisting(final String uid, final Class ofType) {
		try
		{
			return ofType.isInstance(getUserForUID(uid));
		}
		catch (UnknownIdentifierException ex)
		{
			return false;
		}
	}
}
