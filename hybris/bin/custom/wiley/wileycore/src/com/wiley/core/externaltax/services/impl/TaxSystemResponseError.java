package com.wiley.core.externaltax.services.impl;

/**
 * Error is thrown in case if external tax system answer us with 'ERROR' response.
 */
public class TaxSystemResponseError extends TaxSystemException
{

	public TaxSystemResponseError()
	{
	}

	public TaxSystemResponseError(final String paramString)
	{
		super(paramString);
	}

	public TaxSystemResponseError(final String paramString, final Throwable paramThrowable)
	{
		super(paramString, paramThrowable);
	}
}
