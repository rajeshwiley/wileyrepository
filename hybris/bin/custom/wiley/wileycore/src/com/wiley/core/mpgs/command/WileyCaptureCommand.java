package com.wiley.core.mpgs.command;


import com.wiley.core.mpgs.request.WileyCaptureRequest;
import com.wiley.core.mpgs.response.WileyCaptureResponse;


public interface WileyCaptureCommand extends WileyCommand<WileyCaptureRequest, WileyCaptureResponse>
{
}
