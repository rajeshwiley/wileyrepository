package com.wiley.core.price.strategy.impl;

import de.hybris.platform.europe1.model.PDTRowModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;
import java.util.TreeSet;

import com.wiley.core.price.strategy.EffectiveDatesPriceReplacementStrategy;


public class EffectiveDatesPriceReplacementStrategyImpl implements EffectiveDatesPriceReplacementStrategy
{
	private static final Date DEFAULT_END_DATE = Date.from(
			ZonedDateTime.of(2100, 1, 1, 0, 0, 0, 0, ZoneId.systemDefault()).toInstant());

	private ModelService modelService;

	public void replacePrices(final Collection<PriceRowModel> prices, final PriceRowModel price)
	{

		Date startDate = price.getStartTime();
		if (startDate == null)
		{
			modelService.removeAll(prices);
		}
		else if (prices.stream().map(PDTRowModel::getStartTime).anyMatch(Objects::isNull))
		{
			modelService.removeAll(prices);
			price.setEndTime(DEFAULT_END_DATE);
		}
		else
		{
			Optional<PriceRowModel> foundPrice = prices.stream().filter(p -> startDate.equals(p.getStartTime())).findAny();
			if (foundPrice.isPresent())
			{
				PriceRowModel replacedPrice = foundPrice.get();
				price.setEndTime(replacedPrice.getEndTime());
				modelService.remove(replacedPrice);
			}
			else
			{
				TreeSet<PriceRowModel> orderedPrices = new TreeSet<>(
						Comparator.comparing(PDTRowModel::getStartTime, Comparator.naturalOrder()));
				orderedPrices.addAll(prices);

				PriceRowModel prevPrice = orderedPrices.lower(price);
				if (prevPrice != null)
				{
					prevPrice.setEndTime(startDate);
					modelService.save(prevPrice);
				}

				PriceRowModel nextPrice = orderedPrices.higher(price);
				if (nextPrice != null)
				{
					price.setEndTime(nextPrice.getStartTime());
				}
				else
				{
					price.setEndTime(DEFAULT_END_DATE);
				}
			}
		}
		modelService.save(price);
	}

	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}
}
