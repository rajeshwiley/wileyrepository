package com.wiley.core.promotions;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.promotionengineservices.promotionengine.impl.DefaultPromotionEngineService;
import de.hybris.platform.promotions.model.PromotionGroupModel;
import de.hybris.platform.promotions.result.PromotionOrderResults;
import de.hybris.platform.ruleengine.RuleEvaluationResult;
import de.hybris.platform.ruleengine.model.AbstractRuleEngineContextModel;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.concurrent.locks.ReentrantLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.promotions.strategies.WileyRuleEngineContextDateStrategy;
import com.wiley.ruleengine.model.WileyDroolsRuleEngineContextModel;


/**
 * This enhanced promotion engine service is required because the out-the-box service
 * {@link DefaultPromotionEngineService} uses {@link Object} for session mutex and it adds this
 * plain object to session. Since Object is not {@link java.io.Serializable} this causes an exception in Redis
 * serialization process.
 * <p>
 * My best idea was to use {@link ReentrantLock} object since it fulfills both requirement can be used as a mutex and
 * implements {@link java.io.Serializable} interface.
 * <p>
 * TODO check if mutex in session works with session replication. I have suspicious that it doesn't
 */
public class WileyPromotionEngineService extends DefaultPromotionEngineService
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyPromotionEngineService.class);
	private WileyRuleEngineContextDateStrategy wileyRuleEngineContextDateStrategy;

	@Override
	public PromotionOrderResults updatePromotions(final Collection<PromotionGroupModel> promotionGroups,
			final AbstractOrderModel order)
	{
		final Object perSessionLock = getSessionService().getOrLoadAttribute("promotionsUpdateLock",
				Mutex::new);
		synchronized (perSessionLock)
		{
			return updatePromotionsNotThreadSafe(promotionGroups, order);
		}
	}

	/**
	 * OOTB-1808
	 */
	@Override
	public RuleEvaluationResult evaluate(final AbstractOrderModel order, final Collection<PromotionGroupModel> promotionGroups,
			final Date date)
	{
		Date contextDate = getWileyRuleEngineContextDateStrategy().getRuleEngineContextDate(order);
		if (contextDate == null)
		{
			contextDate = date;
		}

		return super.evaluate(order, promotionGroups, contextDate);
	}

	/**
	 * OOTB-1808
	 */
	@Override
	protected AbstractRuleEngineContextModel determineRuleEngineContext(final AbstractOrderModel order)
	{
		final WileyDroolsRuleEngineContextModel ruleEngineContext =
				(WileyDroolsRuleEngineContextModel) super.determineRuleEngineContext(order);
		ruleEngineContext.setDate(getWileyRuleEngineContextDateStrategy().getRuleEngineContextDate(order));
		return ruleEngineContext;
	}

	/**
	 * The mutex to be registered.
	 * Doesn't need to be anything but a plain Object to synchronize on.
	 * Should be serializable to allow for session persistence.
	 */
	@SuppressWarnings("serial")
	private static class Mutex implements Serializable
	{
	}

	public WileyRuleEngineContextDateStrategy getWileyRuleEngineContextDateStrategy()
	{
		return wileyRuleEngineContextDateStrategy;
	}

	@Required
	public void setWileyRuleEngineContextDateStrategy(
			final WileyRuleEngineContextDateStrategy wileyRuleEngineContextDateStrategy)
	{
		this.wileyRuleEngineContextDateStrategy = wileyRuleEngineContextDateStrategy;
	}
}