package com.wiley.core.wiley.ruleengine.drools.impl;

import de.hybris.platform.ruleengine.RuleEvaluationContext;
import de.hybris.platform.ruleengine.drools.impl.DefaultKieSessionHelper;
import de.hybris.platform.ruleengine.model.DroolsKIESessionModel;
import de.hybris.platform.ruleengine.model.DroolsRuleEngineContextModel;

import java.util.Objects;

import org.drools.core.ClockType;
import org.drools.core.SessionConfiguration;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.StatelessKieSession;

import com.wiley.ruleengine.model.WileyDroolsRuleEngineContextModel;


public class WileyKieSessionHelper<T> extends DefaultKieSessionHelper<T>
{
	/* TODO-REFACT-1808
	 * Based on OOTB DefaultKieSessionHelper.initializeStatelessKieSessionInternal(RuleEvaluationContext,
	 * DroolsRuleEngineContextModel, KieContainer)
	 */
	@Override
	protected StatelessKieSession initializeStatelessKieSessionInternal(final RuleEvaluationContext context,
			final DroolsRuleEngineContextModel ruleEngineContext, final KieContainer kieContainer)
	{
		SessionConfiguration sessionConfiguration = null;

		// The customization: set clock type to a session configuration
		if (ruleEngineContext instanceof WileyDroolsRuleEngineContextModel)
		{
			sessionConfiguration = configurePseudoClock(ruleEngineContext, kieContainer);
		}

		DroolsKIESessionModel kieSession = ruleEngineContext.getKieSession();
		assertSessionIsStateless(kieSession);

		// The customization: calling the 2-parameter 'newStatelessKieSession' method instead of the 1-parameter overload
		StatelessKieSession session = kieContainer.newStatelessKieSession(kieSession.getName(), sessionConfiguration);

		if (Objects.nonNull(context.getGlobals()))
		{
			context.getGlobals().forEach(session::setGlobal);
		}

		registerStatelessKieSessionListeners(context, session, ruleEngineContext.getRuleFiringLimit());
		return session;
	}

	protected KieSession initializeKieSessionInternal(final RuleEvaluationContext context,
			final DroolsRuleEngineContextModel ruleEngineContext, final KieContainer kieContainer)
	{
		DroolsKIESessionModel kieSession = ruleEngineContext.getKieSession();
		this.assertSessionIsStateful(kieSession);
		KieSession session;

		// The customization: set clock type to a session configuration
		if (ruleEngineContext instanceof WileyDroolsRuleEngineContextModel)
		{
			SessionConfiguration sessionConfiguration = configurePseudoClock(ruleEngineContext, kieContainer);
			session = kieContainer.newKieSession(kieSession.getName(), sessionConfiguration);
		}
		else
		{
			session = kieContainer.newKieSession(kieSession.getName());
		}

		if (Objects.nonNull(context.getGlobals()))
		{
			context.getGlobals().forEach(session::setGlobal);
		}

		this.registerKieSessionListeners(context, session, ruleEngineContext.getRuleFiringLimit());
		return session;
	}

	private SessionConfiguration configurePseudoClock(final DroolsRuleEngineContextModel ruleEngineContext,
			final KieContainer kieContainer)
	{
		String kieSessionName = ruleEngineContext.getKieSession().getName();
		SessionConfiguration sessionConfiguration =
				(SessionConfiguration) kieContainer.getKieSessionConfiguration(kieSessionName);
		sessionConfiguration.setClockType(ClockType.PSEUDO_CLOCK);

		return sessionConfiguration;
	}
}