package com.wiley.core.order.hook;

import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.commerceservices.service.data.CommerceOrderResult;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.subscriptionservices.subscription.impl.DefaultSubscriptionCommercePlaceOrderMethodHook;
import org.apache.commons.collections.CollectionUtils;

public class WileySubscriptionCommercePlaceOrderMethodHook extends DefaultSubscriptionCommercePlaceOrderMethodHook
{
    @Override
    public void beforeSubmitOrder(final CommerceCheckoutParameter parameter, final CommerceOrderResult result)
            throws InvalidCartException
    {
        ServicesUtil.validateParameterNotNull(parameter, "parameters cannot be null");
        ServicesUtil.validateParameterNotNull(result, "result cannot be null");
        if (CollectionUtils.isNotEmpty(parameter.getCart().getChildren()))
        {
            super.beforeSubmitOrder(parameter, result);
        }
    }
}
