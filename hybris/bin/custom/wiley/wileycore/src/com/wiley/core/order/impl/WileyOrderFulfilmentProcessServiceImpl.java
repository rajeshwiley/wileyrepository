package com.wiley.core.order.impl;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.exception.FullfilmentProcessStaringException;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.processengine.enums.ProcessState;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.store.BaseStoreModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;
import com.wiley.core.model.WileyOrderProcessModel;
import com.wiley.core.order.WileyOrderFulfilmentProcessService;
import com.wiley.core.order.dao.WileyOrderProcessDao;


public class WileyOrderFulfilmentProcessServiceImpl implements WileyOrderFulfilmentProcessService
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyOrderFulfilmentProcessServiceImpl.class);
	private static final List<OrderStatus> ALLOWED_ORDER_STATUSES = new ArrayList<>(
			Arrays.asList(OrderStatus.CREATED, OrderStatus.PAYMENT_AUTHORIZED));

	@Resource
	private BusinessProcessService businessProcessService;

	@Resource
	private ModelService modelService;

	@Resource
	private WileyOrderProcessDao wileyOrderProcessDao;

	@Override
	public OrderProcessModel startFulfilmentProcessForOrder(final OrderModel order, final boolean shouldStartProcess)
			throws FullfilmentProcessStaringException
	{
		Preconditions.checkArgument(order != null, "order parameter can not be null");
		Preconditions.checkArgument(ALLOWED_ORDER_STATUSES.contains(order.getStatus()), "order is in incorrect status: [%s]",
				order.getStatus());

		return this.startFulfilmentProcessForOrderInternal(order, shouldStartProcess, null, true);
	}

	@Override
	public OrderProcessModel startFulfilmentProcessForOrder(final OrderModel order, final boolean shouldStartProcess,
			final String previousOrderState, final boolean processPayment) throws FullfilmentProcessStaringException
	{
		Preconditions.checkArgument(order != null, "order parameter can not be null");
		Preconditions.checkArgument(StringUtils.isNotEmpty(previousOrderState),
				"previousOrderState parameter can not be null or empty");

		return startFulfilmentProcessForOrderInternal(order, shouldStartProcess, previousOrderState, processPayment);
	}

	private OrderProcessModel startFulfilmentProcessForOrderInternal(final OrderModel order, final boolean shouldStartProcess,
			final String previousOrderState, final boolean processPayment) throws FullfilmentProcessStaringException
	{
		BaseStoreModel store = order.getStore();
		if (store == null)
		{
			LOG.error("Unable to start fulfilment process for order [" + order.getCode()
					+ "]. Store was not set on Order.");
			throw new FullfilmentProcessStaringException(order.getCode(), "", "Order.store is null");
		}

		final String fulfilmentProcessDefinitionName = store.getSubmitOrderProcessCode();
		if (fulfilmentProcessDefinitionName == null || fulfilmentProcessDefinitionName.isEmpty())
		{
			LOG.error("Unable to start fulfilment process for order [" + order.getCode() + "]. Store [" + store.getUid()
					+ "] has missing SubmitOrderProcessCode");
			throw new FullfilmentProcessStaringException(order.getCode(), "", "SubmitOrderProcessCode for [" + store.getUid()
					+ "] store is null or empty");
		}

		final String processCode = fulfilmentProcessDefinitionName + "-" + order.getCode() + "-" + System.currentTimeMillis();
		final OrderProcessModel businessProcessModel = businessProcessService.createProcess(processCode,
				fulfilmentProcessDefinitionName);
		businessProcessModel.setOrder(order);
		if (businessProcessModel instanceof WileyOrderProcessModel)
		{
			WileyOrderProcessModel wileyOrderProcessModel = ((WileyOrderProcessModel) businessProcessModel);
			wileyOrderProcessModel.setActiveFor(order);
			LOG.debug("Updating WileyOrderProcess {}: set ActiveFor to order.code={}", businessProcessModel.getCode(),
					order.getCode());
			wileyOrderProcessModel.setPreviousOrderState(previousOrderState);
			wileyOrderProcessModel.setProcessPayment(processPayment);
		}
		modelService.save(businessProcessModel);
		if (shouldStartProcess)
		{
			businessProcessService.startProcess(businessProcessModel);
			LOG.info(String.format("Created and started the process %s", processCode));
		}
		else
		{
			LOG.info(String.format("Created the process %s, but not started", processCode));
		}
		return businessProcessModel;
	}

	@Override
	public boolean existsProcessActiveFor(final OrderModel order)
	{
		return wileyOrderProcessDao.findProcessActiveFor(order).isPresent();
	}

	@Override
	public Optional<WileyOrderProcessModel> getOrderProcessActiveFor(final OrderModel order)
	{
		return wileyOrderProcessDao.findProcessActiveFor(order);
	}

	@Override
	public Optional<OrderProcessModel> getOrderProcessInStateForOrder(final OrderModel orderModel,
			final ProcessState processState)
	{
		return wileyOrderProcessDao.findProcessInSateForOrder(orderModel, processState);
	}

}
