package com.wiley.core.wiley.circuitbreaker.helper;

import javax.annotation.Nonnull;

import com.netflix.hystrix.HystrixCommandProperties;


/**
 * Created by Aliaksei_Zlobich on 10/31/2016.
 */
public interface PropertiesSetterHelper
{

	/**
	 * Adds configuration to <code>HystrixCommandProperties.Setter</code> for command.
	 *
	 * @param commandKey
	 * @param propertiesSetter
	 */
	void configureCircuitBreakerCommand(@Nonnull String commandKey,
			@Nonnull HystrixCommandProperties.Setter propertiesSetter);

}
