package com.wiley.core.wileycom.i18n.dao.impl;

import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.List;

import javax.annotation.Resource;

import com.wiley.core.model.WorldRegionModel;
import com.wiley.core.wileycom.i18n.dao.WileycomWorldRegionDao;


public class WileycomWorldRegionDaoImpl implements WileycomWorldRegionDao
{
	private static final String QUERY_SEARCH_ALL_COUNTRIES = "select {wr:pk} from {WorldRegion as wr}";

	@Resource
	FlexibleSearchService flexibleSearchService;

	@Override
	public List<WorldRegionModel> findWorldRegions()
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(QUERY_SEARCH_ALL_COUNTRIES);
		return flexibleSearchService.<WorldRegionModel> search(query).getResult();
	}
}
