package com.wiley.core.search.solrfacetsearch.populators.impl;

import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.solrfacetsearch.search.impl.SearchQueryConverterData;
import de.hybris.platform.solrfacetsearch.search.impl.populators.AbstractFacetSearchQueryPopulator;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.solr.client.solrj.SolrQuery;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;
import static java.util.Arrays.asList;


public class WileyFacetSearchQueryFieldsListPopulator extends AbstractFacetSearchQueryPopulator
{
	private final List<String> commonFieldsList = asList("indexOperationId_long", "id", "pk", "catalogId", "catalogVersion");

	@Override
	public void populate(final SearchQueryConverterData searchQueryConverterData, final SolrQuery solrQuery)
			throws ConversionException
	{
		validateParameterNotNull(searchQueryConverterData, "searchQueryConverterData mustn't be null");
		validateParameterNotNull(solrQuery, "solrQuery mustn't be null");
		
		final SearchQuery searchQuery = searchQueryConverterData.getSearchQuery();
		final List<String> fieldsList = new ArrayList<>();
		fieldsList.addAll(commonFieldsList);
		searchQuery.getFields().stream()
				.map(field -> getFieldNameTranslator().translate(searchQuery, field, FieldNameProvider.FieldType.INDEX))
				.collect(Collectors.toCollection(() -> fieldsList));

		solrQuery.setFields(fieldsList.toArray(new String[fieldsList.size()]));
	}
}
