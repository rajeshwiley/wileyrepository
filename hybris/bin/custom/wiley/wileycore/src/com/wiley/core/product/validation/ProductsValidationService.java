package com.wiley.core.product.validation;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.validation.model.constraints.ConstraintGroupModel;

import java.util.List;

import javax.annotation.Nonnull;

import com.wiley.core.model.ProductValidationErrorModel;


/**
 * Serving the needs of  the products validation on demand. Leverages existing
 * <a href="https://wiki.hybris.com/display/release5/Data+Validation+Framework">Data Validation Framework</a> by adding custom
 * validation constraints
 */
public interface ProductsValidationService
{

	/**
	 * Validates provided {@code productModel} against <code>constraintGroups</code>
	 *
	 * @param productModel
	 * 		product being validated
	 * @param cronJobModel
	 * 		cron job initiated validation
	 * @param constraintGroups
	 * 		list of {@link ConstraintGroupModel constraints group}
	 * @return list of {@link ProductValidationErrorModel persistence error models}
	 * @see <a href="https://help.hybris.com/6.3.0/hcd/8ba7f5a9866910148b749e7217fa45fa.html">Data Validation Framework</a>
	 */
	@Nonnull
	List<ProductValidationErrorModel> validate(@Nonnull ProductModel productModel, @Nonnull CronJobModel cronJobModel,
			@Nonnull List<ConstraintGroupModel> constraintGroups);
}
