package com.wiley.core.paymentinfo.impl;

import java.util.Arrays;


/**
 * Contains helper-methods to mask and normalize credit card number
 *
 * Created by Raman_Hancharou on 1/15/2016.
 */
public class WileyCreditCardNumberHelper
{
	/**
	 * Mask credit card number string.
	 *
	 * @param creditCardNumber
	 * 		the credit card number
	 * @return the string
	 */
	public String maskCreditCardNumber(final String creditCardNumber) {
		String result = normalizeCreditCardNumber(creditCardNumber);
		if (result != null && result.length() > 4)
		{
			int len = result.length();
			char[] chars = new char[len];
			result.getChars(0, len, chars, 0);
			Arrays.fill(chars, 0, len - 4, '*');
			result = String.valueOf(chars);
		}
		return result;
	}

	/**
	 * Normalize credit card number string.
	 *
	 * @param creditCardNumber
	 * 		the credit card number
	 * @return the string
	 */
	public String normalizeCreditCardNumber(final String creditCardNumber)
	{
		if (creditCardNumber == null)
		{
			return creditCardNumber;
		}
		return creditCardNumber.replaceAll(" ", "");
	}
}
