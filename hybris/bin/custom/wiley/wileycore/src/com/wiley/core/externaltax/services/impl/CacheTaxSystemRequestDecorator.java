package com.wiley.core.externaltax.services.impl;

import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import com.wiley.core.externaltax.services.TaxSystemRequestCommand;
import com.wiley.core.externaltax.xml.caching.TaxServiceCacheAccess;
import com.wiley.core.externaltax.xml.parsing.dto.TaxServiceRequest;

import java.util.Optional;


/**
 * Implementation of {@link TaxSystemRequestCommand} which caches tax amount for each unique {@link TaxServiceRequest}
 */
public class CacheTaxSystemRequestDecorator implements TaxSystemRequestCommand
{
	private static final Logger LOG = LoggerFactory.getLogger(CacheTaxSystemRequestDecorator.class);

	private TaxServiceCacheAccess taxServiceCacheAccess;

	private TaxSystemRequestCommand taxSystemRequestCommand;

	@Override
	@Nonnull
	public Double execute(final TaxServiceRequest taxServiceRequest, final String orderCode, final int entryNumber)
	{
		Double result;

		Optional<Double> taxRequest = taxServiceCacheAccess.get(taxServiceRequest);
		if (taxRequest.isPresent())
		{
			result = taxRequest.get();
			LOG.debug("Getting tax from cache for order [{}] entry [{}] and it is [{}]", orderCode, entryNumber, result);
		}
		else
		{
			result = taxSystemRequestCommand.execute(taxServiceRequest, orderCode, entryNumber);
			taxServiceCacheAccess.put(taxServiceRequest, result);
		}

		Assert.notNull(result);

		return result;
	}

	public void setTaxServiceCacheAccess(final TaxServiceCacheAccess taxServiceCacheAccess)
	{
		this.taxServiceCacheAccess = taxServiceCacheAccess;
	}

	public void setTaxSystemRequestCommand(final TaxSystemRequestCommand taxSystemRequestCommand)
	{
		this.taxSystemRequestCommand = taxSystemRequestCommand;
	}
}
