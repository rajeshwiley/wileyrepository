package com.wiley.core.integration.edi.dao;

import de.hybris.platform.store.BaseStoreModel;

import java.util.List;

import com.wiley.fulfilmentprocess.model.WileyExportProcessModel;


public interface EdiOrderDao
{
	/**
	 * Retrieves Export proccesses for current site based on payment type we want to export
	 *
	 * @param store
	 * 		- current store
	 * @param paymentType
	 * 		- string representation of payment type
	 * @return - list of Processes to export
	 */
	List<WileyExportProcessModel> getExportProcessesReadyForExport(BaseStoreModel store, String paymentType);

	/**
	 * Retrieves Export proccesses for current site based on payment type we want to export
	 *
	 * @param store
	 * 		- current store
	 * @param paymentType
	 * 		- string representation of payment type
	 * @return - list of Processes to export
	 */
	List<WileyExportProcessModel> getExportProcessesReadyForDailyReportExport(BaseStoreModel store, String paymentType);

	/**
	 * Retrieves Export zero dollar physical order processes for current site we want to export
	 *
	 * @param store
	 * 		- current store
	 * @return - list of Processes to export
	 */
	List<WileyExportProcessModel> getZeroDollarPhysicalOrderProcessesReadyForExport(BaseStoreModel store);

}
