package com.wiley.core.event;

import com.google.common.base.Preconditions;
import com.wiley.core.event.models.WileyFailedOrderProcessesEvent;
import com.wiley.core.model.WileyOrderProcessModel;
import com.wiley.fulfilmentprocess.model.WileyFailedOrderProcessesReportingEmailProcessModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class WileyFailedOrderProcessesEventListener extends AbstractEventListener<WileyFailedOrderProcessesEvent> {

    private static final Logger LOG = LoggerFactory.getLogger(WileyFailedOrderProcessesEventListener.class);
    public static final String SEND_ORDER_CANCELLED_EMAIL_PROCESS_DEFINITIN_NAME = "sendOrderCancelledEmailProcess";

    private final ModelService modelService;
    private final BusinessProcessService businessProcessService;

    @Autowired
    public WileyFailedOrderProcessesEventListener(final ModelService modelService,
                                                  final BusinessProcessService businessProcessService) {
        this.modelService = modelService;
        this.businessProcessService = businessProcessService;
    }


    @Override
    protected void onEvent(final WileyFailedOrderProcessesEvent wileyFailedOrderProcessesEvent) {
        LOG.info("Starting business process to prepare and send email about failed fulfillment processes");
        Preconditions.checkState(CollectionUtils.isNotEmpty(wileyFailedOrderProcessesEvent.getProcesses()));
        final Set<WileyOrderProcessModel> processes = new HashSet<>(wileyFailedOrderProcessesEvent.getProcesses());
        BaseSiteModel baseSite = processes.iterator().next().getOrder().getSite();
        WileyFailedOrderProcessesReportingEmailProcessModel process =
                businessProcessService.createProcess(buildProcessCode(),
                        SEND_ORDER_CANCELLED_EMAIL_PROCESS_DEFINITIN_NAME);
        process.setFailedProcesses(new ArrayList<>(wileyFailedOrderProcessesEvent.getProcesses()));
        process.setSite(baseSite);
        process.setDateFrom(wileyFailedOrderProcessesEvent.getDateFrom());
        process.setDateTo(wileyFailedOrderProcessesEvent.getDateTo());
        modelService.save(process);
        businessProcessService.startProcess(process);
    }

    private String buildProcessCode() {
        return "wileyFailedOrderProcesses-" + UUID.randomUUID().toString();
    }
}
