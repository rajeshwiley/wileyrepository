package com.wiley.core.wileyb2c.order.dao.impl;

import de.hybris.platform.commerceservices.order.dao.impl.DefaultCommerceCartDao;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.wiley.core.wileyb2c.order.dao.Wileyb2cCommerceCartDao;


/**
 * Default implementation of {@link Wileyb2cCommerceCartDao}.
 */
public class Wileyb2cCommerceCartDaoImpl extends DefaultCommerceCartDao implements Wileyb2cCommerceCartDao
{
	private static final String FIND_CART_FOR_USER_AND_STORE = SELECTCLAUSE + "WHERE {" + CartModel.USER + "} = ?user AND {"
			+ CartModel.STORE + "} = ?store " + ORDERBYCLAUSE;

	private static final String FIND_CART_FOR_GUID_AND_USER_AND_STORE = SELECTCLAUSE + "WHERE {" + CartModel.GUID + "} = ?guid"
			+ " AND {" + CartModel.USER + "} = ?user AND {" + CartModel.STORE + "} = ?store " + ORDERBYCLAUSE;

	private static final String FIND_CART_FOR_GUID_AND_STORE = SELECTCLAUSE + "WHERE {" + CartModel.GUID + "} = ?guid AND {"
			+ CartModel.STORE + "} = ?store " + ORDERBYCLAUSE;

	private static final String FIND_CARTS_FOR_STORE_AND_USER = SELECTCLAUSE + "WHERE {" + CartModel.STORE + "} = ?store AND {"
			+ CartModel.USER + "} = ?user " + ORDERBYCLAUSE;

	@Nullable
	@Override
	public CartModel getCartForGuidAndStoreAndUser(@Nullable final String guid, @Nonnull final BaseStoreModel store,
			@Nonnull final UserModel user)
	{
		if (guid != null)
		{
			final Map<String, Object> params = new HashMap<String, Object>();
			params.put("guid", guid);
			params.put("store", store);
			params.put("user", user);
			final List<CartModel> carts = doSearch(FIND_CART_FOR_GUID_AND_USER_AND_STORE, params, CartModel.class);
			if (carts != null && !carts.isEmpty())
			{
				return carts.get(0);
			}
			return null;
		}
		else
		{
			return getCartForStoreAndUser(store, user);
		}
	}

	@Nullable
	@Override
	public CartModel getCartForGuidAndStore(@Nonnull final String guid, @Nonnull final BaseStoreModel store)
	{
		final Map<String, Object> params = new HashMap<>();
		params.put("guid", guid);
		params.put("store", store);
		final List<CartModel> carts = doSearch(FIND_CART_FOR_GUID_AND_STORE, params, CartModel.class);
		if (carts != null && !carts.isEmpty())
		{
			return carts.get(0);
		}
		return null;
	}

	@Nonnull
	@Override
	public List<CartModel> getCartsForStoreAndUser(@Nonnull final BaseStoreModel store, @Nonnull final UserModel user)
	{
		final Map<String, Object> params = new HashMap<>();
		params.put("store", store);
		params.put("user", user);
		return doSearch(FIND_CARTS_FOR_STORE_AND_USER, params, CartModel.class);
	}

	private CartModel getCartForStoreAndUser(final BaseStoreModel store, final UserModel user)
	{
		final Map<String, Object> params = new HashMap<String, Object>();
		params.put("store", store);
		params.put("user", user);
		final List<CartModel> carts = doSearch(FIND_CART_FOR_USER_AND_STORE, params, CartModel.class);
		if (carts != null && !carts.isEmpty())
		{
			return carts.get(0);
		}
		return null;
	}
}
