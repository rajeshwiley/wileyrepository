package com.wiley.core.product.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;




import javax.annotation.Resource;

import java.util.Optional;
import com.wiley.core.model.WileyVariantProductModel;
import com.wiley.core.product.WileyVariantProductService;
import com.wiley.core.product.dao.WileyVariantProductDao;


/**
 * Default implementation of {@link WileyVariantProductService}
 *
 * @author Aliaksei_Zlobich
 */
public class DefaultWileyVariantProductService implements WileyVariantProductService
{

	@Resource
	private WileyVariantProductDao wileyVariantProductDao;

	@Override
	public Optional<WileyVariantProductModel> findProductByISBNIfExists(final String isbn,
			final CatalogVersionModel catalogVersion)
	{
		ServicesUtil.validateParameterNotNull(isbn, "ISBN cannot be null.");
		ServicesUtil.validateParameterNotNull(catalogVersion, "CatalogVersion cannot be null.");
		return getWileyVariantProductDao().findProductByISBNIfExists(isbn, catalogVersion);
	}

	/**
	 * Gets wiley variant product dao.
	 *
	 * @return the wiley variant product dao
	 */
	public WileyVariantProductDao getWileyVariantProductDao()
	{
		return wileyVariantProductDao;
	}

	/**
	 * Sets wiley variant product dao.
	 *
	 * @param wileyVariantProductDao
	 * 		the wiley variant product dao
	 */
	public void setWileyVariantProductDao(final WileyVariantProductDao wileyVariantProductDao)
	{
		this.wileyVariantProductDao = wileyVariantProductDao;
	}
}
