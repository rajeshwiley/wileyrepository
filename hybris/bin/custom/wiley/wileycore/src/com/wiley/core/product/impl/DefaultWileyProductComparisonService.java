/**
 *
 */
package com.wiley.core.product.impl;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.price.CommercePriceService;
import de.hybris.platform.commerceservices.util.AbstractComparator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import com.google.common.base.Preconditions;
import com.wiley.core.product.WileyProductComparisonService;
import com.wiley.core.product.WileyProductService;
import com.wiley.core.wiley.restriction.WileyProductRestrictionService;


public class DefaultWileyProductComparisonService implements WileyProductComparisonService
{
	private WileyProductService productService;
	private CommercePriceService commercePriceService;
	private WileyProductRestrictionService wileyProductRestrictionService;

	@Override
	public List<ProductModel> getProductsForComparison(final CategoryModel categoryModel,
			final Integer maxNumberOfProducts)
	{
		Preconditions.checkArgument(categoryModel != null, "category can't be null");
		Preconditions.checkArgument(maxNumberOfProducts != null, "number of products can't be null");

		final List<ProductModel> products = getProductService()
				.getProductsForCategoryExcludingSubcategories(categoryModel);
		return createProductsForComparison(products, maxNumberOfProducts);
	}

	@Override
	public List<ProductModel> getProductsForComparison(final String baseProductCategoryCode,
			final String variantValueCategoryCode, final Integer maxNumberOfProducts)
	{
		Preconditions.checkArgument(baseProductCategoryCode != null, "baseProductCategoryCode can't be null");
		Preconditions.checkArgument(variantValueCategoryCode != null, "variantValueCategoryCode can't be null");
		Preconditions.checkArgument(maxNumberOfProducts != null, "number of products can't be null");

		final List<ProductModel> products = getProductService()
				.getWileyProductsFromCategories(baseProductCategoryCode, variantValueCategoryCode);
		return createProductsForComparison(getProductService().filterProducts(products), maxNumberOfProducts);
	}

	private List<ProductModel> createProductsForComparison(final List<ProductModel> products,
			final Integer maxNumberOfProducts)
	{
		final Map<PriceInformation, ProductModel> productPriceMap = new TreeMap<>(Collections.reverseOrder(
				PriceInformationComparator.INSTANCE));
		PriceInformation priceInfo;
		for (final ProductModel productModel : products)
		{
			if (wileyProductRestrictionService.isVisible(productModel))
			{
				ProductModel productWithFilteredVariants = wileyProductRestrictionService.filterRestrictedProductVariants(
						productModel);
				priceInfo = getCommercePriceService().getFromPriceForProduct(productWithFilteredVariants);
				productPriceMap.put(priceInfo, productModel);
			}
		}
		return getProductsFromMap(productPriceMap, maxNumberOfProducts);
	}

	/**
	 * @param productPriceMap
	 * @param maxNumberOfProducts
	 */
	private List<ProductModel> getProductsFromMap(final Map<PriceInformation, ProductModel> productPriceMap,
			final Integer maxNumberOfProducts)
	{
		final List<ProductModel> productsForComparison = new ArrayList<>();
		int count = 1;

		for (final Map.Entry<PriceInformation, ProductModel> entry : productPriceMap.entrySet())
		{
			if (count++ <= maxNumberOfProducts)
			{
				productsForComparison.add(entry.getValue());
			}
			else
			{
				break;
			}
		}
		return productsForComparison;
	}

	/**
	 * @return the productService
	 */
	public WileyProductService getProductService()
	{
		return productService;
	}

	/**
	 * @param productService
	 * 		the productService to set
	 */
	public void setProductService(final WileyProductService productService)
	{
		this.productService = productService;
	}

	/**
	 * @return the commercePriceService
	 */
	public CommercePriceService getCommercePriceService()
	{
		return commercePriceService;
	}

	/**
	 * @param commercePriceService
	 * 		the commercePriceService to set
	 */
	public void setCommercePriceService(final CommercePriceService commercePriceService)
	{
		this.commercePriceService = commercePriceService;
	}

	public static class PriceInformationComparator extends AbstractComparator<PriceInformation>
	{
		public static final PriceInformationComparator INSTANCE = new PriceInformationComparator();

		@Override
		protected int compareInstances(final PriceInformation price1, final PriceInformation price2)
		{
			Assert.isTrue(price1.getPriceValue().getCurrencyIso().equals(price2.getPriceValue().getCurrencyIso()),
					"differing currency of web prices");

			int compareResult = compareValues(price1.getPriceValue().getValue(), price2.getPriceValue().getValue());
			if (compareResult == 0)
			{
				//if prices are the same, put new value after
				compareResult = AbstractComparator.AFTER;
			}
			return compareResult;
		}
	}

	@Required
	public void setWileyProductRestrictionService(
			final WileyProductRestrictionService wileyProductRestrictionService)
	{
		this.wileyProductRestrictionService = wileyProductRestrictionService;
	}
}
