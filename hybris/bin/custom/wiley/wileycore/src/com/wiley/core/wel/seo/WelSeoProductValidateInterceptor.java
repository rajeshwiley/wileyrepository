package com.wiley.core.wel.seo;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class WelSeoProductValidateInterceptor
		extends AbstractWelSeoCatalogAwareValidateInterceptor
		implements ValidateInterceptor<ProductModel>
{

	private static final Logger LOG = LoggerFactory.getLogger(WelSeoProductValidateInterceptor.class);

	public WelSeoProductValidateInterceptor(final EventService eventService)
	{
		super(eventService);
	}

	@Override
	public void onValidate(final ProductModel productModel, final InterceptorContext interceptorContext)
			throws InterceptorException
	{
		CatalogVersionModel catalogVersion = productModel.getCatalogVersion();
		if (catalogVersion != null && isCatalogAllowed(catalogVersion.getCatalog())
				&& StringUtils.equals(CATALOG_VERSION, catalogVersion.getVersion()))
		{
			LOG.debug("WelSeoProductValidateInterceptor executing on save Product");
			if (interceptorContext.isNew(productModel)
					|| interceptorContext.isModified(productModel, ProductModel.NAME)
					|| interceptorContext.isModified(productModel, ProductModel.SEOLABEL)
					|| interceptorContext.isModified(productModel, ProductModel.SUPERCATEGORIES))
			{
				LOG.debug("Sending ClusterAwareEvent to invalidate WEL Seo Map on all server nodes.");
				publishClusterAwareEvent();
			}
		}
	}
}
