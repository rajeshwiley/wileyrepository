package com.wiley.core.order;

import java.util.Date;


public interface WileyB2BCheckoutService extends WileycomCheckoutService
{
	void setDesiredShippingDate(Date desiredShippingDate);
}
