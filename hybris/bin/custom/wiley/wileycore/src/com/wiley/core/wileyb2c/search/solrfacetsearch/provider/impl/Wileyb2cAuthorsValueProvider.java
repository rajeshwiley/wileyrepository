package com.wiley.core.wileyb2c.search.solrfacetsearch.provider.impl;

import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.impl.ValueProviderParameterUtils;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import com.wiley.core.model.AuthorInfoModel;
import com.wiley.core.wileyb2c.classification.Wileyb2cClassificationService;
import com.wiley.core.wileycom.search.solrfacetsearch.provider.impl.AbstractWileycomValueProvider;


/**
 * Created by Uladzimir_Barouski on 3/7/2017.
 */
public class Wileyb2cAuthorsValueProvider extends AbstractWileycomValueProvider<String>
{
	public static final String SKIP_CATEGORY_PARAM = "skipCategory";

	@Resource
	private Wileyb2cClassificationService wileyb2cClassificationService;

	@Override
	public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
			final Object model) throws FieldValueProviderException
	{
		final VariantProductModel variantProductModel = (VariantProductModel) model;
		final List<AuthorInfoModel> authorInformation = getAuthorInfos(variantProductModel);
		if (CollectionUtils.isNotEmpty(authorInformation) && indexValues(indexedProperty, variantProductModel))
		{
			return super.getFieldValues(indexConfig, indexedProperty, model);
		}
		return Collections.emptyList();
	}

	@Override
	protected List<String> collectValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty, final Object model)
			throws FieldValueProviderException
	{
		final List<AuthorInfoModel> authorInformation = getAuthorInfos((VariantProductModel) model);
		if (CollectionUtils.isNotEmpty(authorInformation))
		{
			return authorInformation
					.stream()
					.map(AuthorInfoModel::getName)
					.filter(StringUtils::isNotEmpty)
					.collect(Collectors.toList());
		}
		return Collections.emptyList();
	}

	private boolean indexValues(final IndexedProperty indexedProperty, final VariantProductModel variantProductModel)
	{
		final String skipCategory = ValueProviderParameterUtils.getString(indexedProperty, SKIP_CATEGORY_PARAM,
				StringUtils.EMPTY);
		final ClassificationClassModel classificationClass = wileyb2cClassificationService.resolveClassificationClass(
				variantProductModel);
		return !skipCategory.equals(classificationClass.getCode());
	}

	private List<AuthorInfoModel> getAuthorInfos(final VariantProductModel model)
	{
		return model.getBaseProduct().getAuthorInfos();
	}
}
