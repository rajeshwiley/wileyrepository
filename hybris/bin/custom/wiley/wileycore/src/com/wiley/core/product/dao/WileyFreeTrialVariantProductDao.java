package com.wiley.core.product.dao;

import java.util.List;

import com.wiley.core.model.WileyFreeTrialVariantProductModel;


/**
 * Created by Raman_Hancharou on 3/30/2016.
 */
public interface WileyFreeTrialVariantProductDao
{
	List<WileyFreeTrialVariantProductModel> findFreeTrialVariantProductsByCode(String code);
}
