package com.wiley.core.customer.populator;

import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.integration.alm.user.dto.UserDto;

import de.hybris.platform.converters.impl.AbstractPopulatingConverter;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.model.ModelService;

public class WileyCustomerModelReverseConverter extends AbstractPopulatingConverter<UserDto, CustomerModel>
{
	@Autowired
	private ModelService modelService;
	
	@Override
	public CustomerModel convert(final UserDto source) throws ConversionException
	{
		CustomerModel customerModel = modelService.create(CustomerModel.class);
		superPopulate(source, customerModel);
		return customerModel;
	}
	
	protected void superPopulate(final UserDto source, final CustomerModel target)
	{
		super.populate(source, target);
	}

}
