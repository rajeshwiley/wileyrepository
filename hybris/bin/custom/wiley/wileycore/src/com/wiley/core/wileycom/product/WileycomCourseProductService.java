package com.wiley.core.wileycom.product;

import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;


/**
 * Created by Uladzimir_Barouski on 8/23/2016.
 */
public interface WileycomCourseProductService
{
	List<ProductModel> getAvailableSetsForCourse(String isbn);
}
