package com.wiley.core.product.dao.impl;

import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.List;

import javax.annotation.Resource;

import com.wiley.core.model.WileyProductVariantSetModel;
import com.wiley.core.product.dao.WileyProductVariantSetDao;


/**
 * Default implementation of {@link WileyProductVariantSetDao}.
 *
 * @author Aliaksei_Zlobich
 */
public class DefaultWileyProductVariantSetDao implements WileyProductVariantSetDao
{

	private static final String QUERY_FIND_ACTIVE_PRODUCT_SETS = "SELECT {s." + WileyProductVariantSetModel.PK + "} FROM "
			+ "{" + WileyProductVariantSetModel._TYPECODE + " AS s} ORDER BY {s." + WileyProductVariantSetModel.MODIFIEDTIME
			+ "} DESC";

	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Override
	public List<WileyProductVariantSetModel> findProductSets()
	{
		FlexibleSearchQuery query = new FlexibleSearchQuery(QUERY_FIND_ACTIVE_PRODUCT_SETS);
		return getFlexibleSearchService().<WileyProductVariantSetModel> search(query).getResult();
	}

	public FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}
}
