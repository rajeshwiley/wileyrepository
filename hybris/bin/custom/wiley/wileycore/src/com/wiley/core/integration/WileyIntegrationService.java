package com.wiley.core.integration;

import de.hybris.platform.core.model.user.CustomerModel;


/**
 * Holds logic required for Wiley integration
 */
public interface WileyIntegrationService
{
	/**
	 * Builds "magic" link for Wiley integration.
	 * <p>
	 * Please take a look at corresponding <a href="https://confluence.wiley.ru/display/ECSC/IDD%3A+Link-with-
	 * token+authentication+mechanism">IDD: Link-with-token authentication mechanism</a> for more details
	 *
	 * @param customerData customer data
	 * @return String URL with query parameters required for user authentication at AGS
	 */
	String getMagicLink(CustomerModel customerData);
}
