package com.wiley.core.integration.invoice;

import javax.annotation.Nonnull;

import com.wiley.core.integration.invoice.dto.InvoiceDto;


public interface InvoiceGateway
{
	InvoiceDto getInvoice(@Nonnull String referenceId);
}
