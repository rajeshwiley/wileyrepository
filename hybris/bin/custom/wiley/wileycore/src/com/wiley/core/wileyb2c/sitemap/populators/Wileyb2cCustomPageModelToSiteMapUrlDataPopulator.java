package com.wiley.core.wileyb2c.sitemap.populators;

import de.hybris.platform.acceleratorservices.sitemap.data.SiteMapUrlData;
import de.hybris.platform.acceleratorservices.sitemap.populators.CustomPageToSiteMapUrlDataPopulator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.apache.log4j.Logger;


/**
 * Created by Maksim_Kozich on 21.09.17.
 */
public class Wileyb2cCustomPageModelToSiteMapUrlDataPopulator extends CustomPageToSiteMapUrlDataPopulator
{
	private static final Logger LOG = Logger.getLogger(Wileyb2cCustomPageModelToSiteMapUrlDataPopulator.class);

	@Override
	public void populate(final String source, final SiteMapUrlData siteMapUrlData) throws ConversionException
	{
		superPopulate(source, siteMapUrlData);
		if (siteMapUrlData.getLoc() == null)
		{
			getLogger().error("Resolved relative URL for custom page '" + source + "' is null");
		}
	}

	protected void superPopulate(final String source, final SiteMapUrlData siteMapUrlData)
	{
		super.populate(source, siteMapUrlData);
	}

	protected Logger getLogger()
	{
		return LOG;
	}
}
