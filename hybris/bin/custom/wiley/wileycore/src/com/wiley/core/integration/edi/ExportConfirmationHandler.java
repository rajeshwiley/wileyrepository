package com.wiley.core.integration.edi;

import org.springframework.messaging.Message;


public interface ExportConfirmationHandler
{
	Message confirm(Message message);
}
