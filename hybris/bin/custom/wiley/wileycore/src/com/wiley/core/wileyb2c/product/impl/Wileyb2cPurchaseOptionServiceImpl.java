package com.wiley.core.wileyb2c.product.impl;

import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.google.common.base.Preconditions;
import com.wiley.core.wiley.restriction.WileyProductRestrictionService;
import com.wiley.core.wileyb2c.classification.Wileyb2cClassificationService;
import com.wiley.core.wileyb2c.product.Wileyb2cPurchaseOptionService;


public class Wileyb2cPurchaseOptionServiceImpl implements Wileyb2cPurchaseOptionService
{
	@Autowired
	private Wileyb2cClassificationService wileyb2cClassificationService;
	@Autowired
	@Qualifier("wileyb2cProductRestrictionService")
	private WileyProductRestrictionService wileyProductRestrictionService;

	@Override
	public ProductModel getFirstPurchaseOptionVariant(final ProductModel productModel)
	{
		Preconditions.checkArgument(productModel != null);
		VariantProductModel topVariant = null;
		final Collection<VariantProductModel> variants = productModel.getVariants();
		if (CollectionUtils.isNotEmpty(variants))
		{
			int minSeqNumber = Integer.MAX_VALUE;
			for (final VariantProductModel variant : variants)
			{
				final boolean isVisible = wileyProductRestrictionService.isVisible(variant);
				if (isVisible)
				{
					final ClassificationClassModel classificationClass =
							wileyb2cClassificationService.resolveClassificationClass(variant);
					final int purchaseOptionSeqNum =
							classificationClass.getSequenceNumber()
									!= null ? classificationClass.getSequenceNumber() : Integer.MAX_VALUE;
					if (purchaseOptionSeqNum <= minSeqNumber)
					{
						topVariant = variant;
						minSeqNumber = purchaseOptionSeqNum;
					}
				}
			}
		}
		return topVariant;
	}
}
