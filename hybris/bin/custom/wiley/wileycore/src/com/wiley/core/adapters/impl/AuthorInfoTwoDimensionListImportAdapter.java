package com.wiley.core.adapters.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.List;
import java.util.Locale;

import javax.annotation.Nonnull;

import org.apache.commons.lang3.text.StrTokenizer;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.base.Preconditions;
import com.wiley.core.adapters.AbstractTwoDimensionListImportAdapter;
import com.wiley.core.jalo.AuthorInfo;
import com.wiley.core.model.AuthorInfoModel;


/**
 * Created by Aliaksei_Zhvaleuski on 29.03.2017.
 */
public class AuthorInfoTwoDimensionListImportAdapter extends AbstractTwoDimensionListImportAdapter<AuthorInfoModel>
{
	private static final boolean IS_MANDATORY = true;
	private static final boolean IS_NOT_MANDATORY = false;

	@Autowired
	private ModelService modelService;

	@Override
	public AuthorInfoModel convertToModel(@Nonnull final String authorInfoToken, @Nonnull final ProductModel productModel)
			throws ImpExException
	{

		Preconditions.checkNotNull(authorInfoToken, "authorInfoToken parameter should not be null");
		Preconditions.checkNotNull(productModel, "productModel parameter should not be null");

		final StrTokenizer authorTokenizer = getSecondDimensionTokenizer(authorInfoToken);

		final AuthorInfoModel authorInfoModel = modelService.create(AuthorInfoModel.class);
		authorInfoModel.setProduct(productModel);
		authorInfoModel.setName(getNextValue(authorTokenizer, IS_MANDATORY, AuthorInfo.NAME), Locale.ENGLISH);
		authorInfoModel.setRole(getNextValue(authorTokenizer, IS_NOT_MANDATORY, AuthorInfo.ROLE), Locale.ENGLISH);
		return authorInfoModel;
	}

	@Override
	public void saveModels(final ProductModel product, final List<AuthorInfoModel> authorInfos)
	{
		product.setAuthorInfos(authorInfos);
		modelService.save(product);
	}
}
