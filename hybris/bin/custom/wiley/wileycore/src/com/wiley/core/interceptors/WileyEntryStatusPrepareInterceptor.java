package com.wiley.core.interceptors;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;

import java.util.Date;


public class WileyEntryStatusPrepareInterceptor implements PrepareInterceptor<AbstractOrderEntryModel>
{
	@Override
	public void onPrepare(final AbstractOrderEntryModel abstractOrderEntryModel, final InterceptorContext interceptorContext)
			throws InterceptorException
	{
		if (interceptorContext.isModified(abstractOrderEntryModel, AbstractOrderEntryModel.STATUS))
		{
			abstractOrderEntryModel.setStatusModifiedTime(new Date());
		}
	}
}
