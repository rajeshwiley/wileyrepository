package com.wiley.core.product.dao;

import de.hybris.platform.europe1.model.DiscountRowModel;

public interface WileyProductStudentDiscountDao
{

    void getStudentDiscount(DiscountRowModel discountRowModel);
}
