package com.wiley.core.mpgs.dto.retrievesession;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.wiley.core.mpgs.dto.json.Error;
import com.wiley.core.mpgs.dto.json.Session;


@JsonIgnoreProperties(ignoreUnknown = true)
public class MPGSRetrieveSessionResponseDTO
{
	private Session session;
	private Error error;
	private String result;

	public String getResult()
	{
		return result;
	}

	public void setResult(final String result)
	{
		this.result = result;
	}

	public Session getSession()
	{
		return session;
	}

	public void setSession(final Session session)
	{
		this.session = session;
	}

	public Error getError()
	{
		return error;
	}

	public void setError(final Error error)
	{
		this.error = error;
	}
}
