package com.wiley.core.wileyb2c.search.solrfacetsearch.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.model.config.SolrFacetSearchConfigModel;


/**
 * @author Dzmitryi_Halahayeu
 */
public class WileyIndexCountriesPopulator implements Populator<SolrFacetSearchConfigModel, IndexConfig>
{
	@Override
	public void populate(final SolrFacetSearchConfigModel solrFacetSearchConfigModel, final IndexConfig indexConfig)
			throws ConversionException
	{
		indexConfig.setCountries(solrFacetSearchConfigModel.getCountries());
	}
}
