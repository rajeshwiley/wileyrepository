package com.wiley.core.wileycom.users.service;

import de.hybris.platform.core.model.user.UserModel;


/**
 * Created by Mikhail_Asadchy on 16.06.2016.
 */
public interface WileycomUsersService
{

	/**
	 * Makes a call to users service to obtain information about user authentication (valid credentials or not)
	 *
	 * @param user
	 * 		- user model
	 * @param password
	 * 		- user password as a plain text
	 * @return - valid or not
	 */
	boolean authenticate(UserModel user, String password);

	/**
	 * Makes a call to users service to update user's password
	 *
	 * @param user
	 * 		- user model
	 * @param oldPassword
	 *@param password
	 * 		- user password as a plain text  @return - updated or not
	 */
	boolean updatePassword(UserModel user, String oldPassword, String password);

}
