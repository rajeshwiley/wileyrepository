package com.wiley.core.wileyb2c.weblinks.impl;

import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.enums.WileyWebLinkTypeEnum;
import com.wiley.core.model.WileyWebLinkModel;
import com.wiley.core.wileyb2c.weblinks.WileyWebLinkService;
import com.wiley.core.wileyb2c.weblinks.providers.WileyWebLinkUrlProvider;


public class WileyWebLinkServiceImpl implements WileyWebLinkService
{
	private static final Logger LOG  = LoggerFactory.getLogger(WileyWebLinkServiceImpl.class);

	private WileyWebLinkUrlProvider defaultWebLinkProvider;
	private Map<WileyWebLinkTypeEnum, WileyWebLinkUrlProvider> urlProviders;

	@Override
	public String getWebLinkUrl(final WileyWebLinkModel webLink)
	{
		String url = null;
		try {
			url = getWebLinkProvider(webLink.getType()).getWebLinkUrl(webLink);
		} catch (IllegalArgumentException exc) {
			LOG.error("Link cannot be resolved", exc);
		}
		return url;
	}

	private WileyWebLinkUrlProvider getWebLinkProvider(final WileyWebLinkTypeEnum type)
	{
		final WileyWebLinkUrlProvider provider;
		if (MapUtils.isNotEmpty(urlProviders) && urlProviders.containsKey(type))
		{
			provider = urlProviders.get(type);
		}
		else
		{
			provider = defaultWebLinkProvider;
		}
		return provider;
	}

	@Required
	public void setDefaultWebLinkProvider(final WileyWebLinkUrlProvider defaultWebLinkProvider)
	{
		this.defaultWebLinkProvider = defaultWebLinkProvider;
	}

	public void setUrlProviders(
			final Map<WileyWebLinkTypeEnum, WileyWebLinkUrlProvider> urlProviders)
	{
		this.urlProviders = urlProviders;
	}
}
