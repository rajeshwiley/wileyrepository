package com.wiley.core.cart;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartFactory;

import com.wiley.core.enums.OrderType;

public interface WileyCartFactory extends CartFactory
{
	CartModel createCartWithType(OrderType orderType);
}
