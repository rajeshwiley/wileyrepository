package com.wiley.core.externaltax.strategies.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;

import java.util.Optional;

import com.wiley.core.externaltax.strategies.WileyTaxCodeStrategy;
import com.wiley.core.model.WileyPurchaseOptionProductModel;


public class WileyIsbn13TaxCodeStrategy implements WileyTaxCodeStrategy
{
	@Override
	public Optional<String> getTaxCode(final AbstractOrderEntryModel entry)
	{
		if (entry.getProduct() instanceof WileyPurchaseOptionProductModel)
		{
			return Optional.ofNullable(entry.getProduct().getCode());
		}
		return Optional.empty();
	}
}
