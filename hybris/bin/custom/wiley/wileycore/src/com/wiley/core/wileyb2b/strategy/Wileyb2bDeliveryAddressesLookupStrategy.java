package com.wiley.core.wileyb2b.strategy;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BUnitService;
import de.hybris.platform.b2bacceleratorservices.company.CompanyB2BCommerceService;
import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.b2bacceleratorservices.strategies.impl.DefaultB2BDeliveryAddressesLookupStrategy;
import de.hybris.platform.commerceservices.strategies.DeliveryAddressesLookupStrategy;
import de.hybris.platform.commerceservices.util.ItemComparator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;


/**
 * B2B delivery addresses lookup strategy, that don't use cost center
 * (substitution for {@link DefaultB2BDeliveryAddressesLookupStrategy}).
 */
class Wileyb2bDeliveryAddressesLookupStrategy implements DeliveryAddressesLookupStrategy
{

	private DeliveryAddressesLookupStrategy fallbackDeliveryAddressesLookupStrategy;

	@Resource
	private B2BUnitService<B2BUnitModel, B2BCustomerModel> b2bUnitService;

	@Resource
	private CompanyB2BCommerceService companyB2BCommerceService;

	/**
	 * Return address from B2BUnit
	 */
	@Override
	public List<AddressModel> getDeliveryAddressesForOrder(final AbstractOrderModel abstractOrder,
			final boolean visibleAddressesOnly)
	{
		if (CheckoutPaymentType.ACCOUNT.equals(abstractOrder.getPaymentType()))
		{
			// Lookup the
			final B2BUnitModel b2BUnit = b2bUnitService.getParent(companyB2BCommerceService.<B2BCustomerModel> getCurrentUser());
			final Set<AddressModel> addresses = collectShippingAddressesForUnit(b2BUnit);
			if (addresses != null && !addresses.isEmpty())
			{
				return sortAddresses(addresses);
			}

			// Can't find any pay on account addresses yet - maybe the cost centre is not set yet?
			return Collections.emptyList();
		}
		else
		{
			// Use fallback
			return fallbackDeliveryAddressesLookupStrategy.getDeliveryAddressesForOrder(abstractOrder, visibleAddressesOnly);
		}
	}

	protected Set<AddressModel> collectShippingAddressesForUnit(final B2BUnitModel b2bUnit)
	{
		final Set<B2BUnitModel> branch = b2bUnitService.getBranch(b2bUnit);
		final Set<AddressModel> addresses = new HashSet<AddressModel>();
		for (final B2BUnitModel unit : branch)
		{
			if (CollectionUtils.isNotEmpty(unit.getAddresses()))
			{
				unit.getAddresses().forEach(addressModel -> {
					if (Boolean.TRUE.equals(addressModel.getShippingAddress()))
					{
						addresses.add(addressModel);
					}
				});
			}
		}
		return addresses;
	}

	protected List<AddressModel> sortAddresses(final Collection<AddressModel> addresses)
	{
		final List<AddressModel> result = new ArrayList<AddressModel>(addresses);
		Collections.sort(result, ItemComparator.INSTANCE);
		return result;
	}

	@Required
	public void setFallbackDeliveryAddressesLookupStrategy(
			final DeliveryAddressesLookupStrategy fallbackDeliveryAddressesLookupStrategy)
	{
		this.fallbackDeliveryAddressesLookupStrategy = fallbackDeliveryAddressesLookupStrategy;
	}

}
