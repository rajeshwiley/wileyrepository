package com.wiley.core.ruleengineservices.converters.populator;

import de.hybris.platform.converters.Converters;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.ruleengineservices.converters.populator.OrderEntryRaoPopulator;
import de.hybris.platform.ruleengineservices.rao.AbstractRuleActionRAO;
import de.hybris.platform.ruleengineservices.rao.DiscountRAO;
import de.hybris.platform.ruleengineservices.rao.OrderEntryRAO;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.util.DiscountValue;

import java.math.BigDecimal;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;

import javax.annotation.Resource;


/**
 * Entry populator that incorporates default RAO population
 * {@bean "defaultOrderEntryRaoPopulator"} and populates subtotal price
 */
public class WileyOrderEntryRaoPopulator implements Populator<AbstractOrderEntryModel, OrderEntryRAO>
{
	@Resource(name = "defaultOrderEntryRaoPopulator")
	private OrderEntryRaoPopulator orderEntryRaoPopulator;

	@Resource(name = "wileyDiscountRaoConverter")
	private Converter<DiscountValue, DiscountRAO> wileyDiscountRaoConverter;


	public void populate(final AbstractOrderEntryModel source, final OrderEntryRAO target) throws ConversionException
	{
		orderEntryRaoPopulator.populate(source, target);
		Double subtotalPrice = source.getSubtotalPrice();
		if (Objects.nonNull(subtotalPrice))
		{
			target.setSubtotalPrice(BigDecimal.valueOf(subtotalPrice));
		}

		populateActionsFromDiscountValues(source.getDiscountValues(), target);
	}

	/**
	 * ExternalDiscount and DiscountRow are presented as DiscountValues in the orderEntry
	 * This method populates actions from DiscountValues, which allows to use them during applying promotion rules
	 *
	 * @param discountValues
	 * @param target
	 */
	private void populateActionsFromDiscountValues(final List<DiscountValue> discountValues, final OrderEntryRAO target)
	{
		final List<DiscountRAO> discountActions = Converters.convertAll(discountValues, wileyDiscountRaoConverter);
		final LinkedHashSet<AbstractRuleActionRAO> actions = new LinkedHashSet<>();
		actions.addAll(discountActions);
		if (target.getActions() != null)
		{
			actions.addAll(target.getActions());
		}

		target.setActions(actions);
	}

}
