package com.wiley.core.wileyb2c.search.solrfacetsearch.provider.impl;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.search.solrfacetsearch.provider.impl.ProductAttributesValueResolver;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;

import java.util.List;
import java.util.stream.Collectors;


/**
 * Created by Uladzimir_Barouski on 3/7/2017.
 */
public class Wileyb2cCategoryCodeResolver extends ProductAttributesValueResolver
{
	@Override
	protected Object getAttributeValue(final IndexedProperty indexedProperty, final ProductModel product,
			final String attributeName) throws FieldValueProviderException
	{
		final List<CategoryModel> categories = (List<CategoryModel>) super.getAttributeValue(indexedProperty, product,
				attributeName);
		return categories.stream().map(CategoryModel::getCode).collect(Collectors.toList());
	}
}
