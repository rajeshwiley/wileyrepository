package com.wiley.core.wileycom.order.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;

import org.apache.commons.collections.CollectionUtils;

import com.wiley.core.wileycom.order.WileycomOrderEntryService;

import static java.lang.String.format;


/**
 * Author Herman_Chukhrai (EPAM)
 */
public class WileycomOrderEntryServiceImpl implements WileycomOrderEntryService
{
	@Override
	public Optional<AbstractOrderEntryModel> findOrderEntryByIsbn(@Nonnull final AbstractOrderModel order,
			@Nonnull final String isbn)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("order", order);
		ServicesUtil.validateParameterNotNullStandardMessage("isbn", isbn);

		final List<AbstractOrderEntryModel> entries = order.getEntries();

		Optional<AbstractOrderEntryModel> result = Optional.empty();

		if (CollectionUtils.isNotEmpty(entries))
		{
			final List<AbstractOrderEntryModel> foundEntries = order.getEntries().stream()
					.filter(entry -> isbn.equals(entry.getProduct().getIsbn())).collect(Collectors.toList());

			final int size = foundEntries.size();
			if (size == 1)
			{
				return Optional.of(foundEntries.get(0));
			}
			else if (size > 1)
			{
				final String errorMessage = format("There are %d order entry with product ISBN [%s] but should be only one", size,
						isbn);
				throw new IllegalStateException(errorMessage);
			}
		}

		return result;
	}
}
