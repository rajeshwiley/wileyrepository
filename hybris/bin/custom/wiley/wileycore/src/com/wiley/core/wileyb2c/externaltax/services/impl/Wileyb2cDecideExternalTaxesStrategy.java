package com.wiley.core.wileyb2c.externaltax.services.impl;

import de.hybris.platform.commerceservices.externaltax.DecideExternalTaxesStrategy;
import de.hybris.platform.core.model.order.AbstractOrderModel;


/**
 * Implementation of {@link DecideExternalTaxesStrategy} that check is external tax calculation possible. We are
 * checking existence of payment address, because in case of payment address is provided we are already have all information
 * needed for external tax calculation
 *
 * @author Dzmitryi_Halahayeu
 */
public class Wileyb2cDecideExternalTaxesStrategy implements DecideExternalTaxesStrategy
{
	@Override
	public boolean shouldCalculateExternalTaxes(final AbstractOrderModel abstractOrder)
	{
		return abstractOrder.getTotalPrice() > 0 && abstractOrder.getPaymentAddress() != null;
	}
}
