package com.wiley.core.wileyb2c.search.solrfacetsearch.indexer;

import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.indexer.IndexerService;
import de.hybris.platform.solrfacetsearch.indexer.exceptions.IndexerException;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * Created by Raman_Hancharou on 4/11/2017.
 */
public interface WileyIndexerService extends IndexerService
{

	void updatePartialTypeIndex(FacetSearchConfig facetSearchConfig, IndexedType indexedType,
			Collection<IndexedProperty> indexedProperties, List<PK> pks, Set<CountryModel> countries) throws
			IndexerException;

	void restoreIndex(FacetSearchConfig facetSearchConfig, Map<String, String> indexerHints)
			throws IndexerException;
}
