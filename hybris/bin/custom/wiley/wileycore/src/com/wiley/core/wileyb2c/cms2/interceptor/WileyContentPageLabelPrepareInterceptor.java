package com.wiley.core.wileyb2c.cms2.interceptor;

import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PersistenceOperation;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;

import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class WileyContentPageLabelPrepareInterceptor implements PrepareInterceptor<ContentPageModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyContentPageLabelPrepareInterceptor.class);
	private static final String PAGE_PATH_SEPARATOR = "/";

	@Override
	public void onPrepare(final ContentPageModel contentPage, final InterceptorContext interceptorContext)
			throws InterceptorException
	{
		if (isPagePathAffected(contentPage, interceptorContext))
		{
			updateAllLabels(contentPage, interceptorContext);
		}
	}

	private void updateAllLabels(final ContentPageModel contentPage, final InterceptorContext interceptorContext)
			throws InterceptorException
	{
		if (!interceptorContext.contains(contentPage, PersistenceOperation.SAVE))
		{
			updatePageLabel(contentPage);
			interceptorContext.registerElementFor(contentPage, PersistenceOperation.SAVE);
			final Collection<ContentPageModel> childPages = contentPage.getChildPages();
			if (CollectionUtils.isNotEmpty(childPages))
			{
				for (ContentPageModel childPage : childPages)
				{
					updateAllLabels(childPage, interceptorContext);
				}
			}
		}
		else
		{
			LOG.error("Unable to update Hierarchy label. Page '{}' forms cyclic dependency", contentPage.getUid());
		}
	}

	private void updatePageLabel(final ContentPageModel contentPage)
	{
		String pageLabel = contentPage.getPageLabel();
		if (StringUtils.isBlank(pageLabel))
		{
			LOG.warn("Missing 'pageLabel' attribute for page '{}'. Hierarchy Label creation for this page will be skipped",
					contentPage.getUid());
		}
		else
		{
			if (contentPage.isHomepage())
			{
				// home pages should not be accessible by label link
				contentPage.setLabel(contentPage.getPageLabel());
			}
			else
			{
				String parentLabel = getParentLabel(contentPage);
				contentPage.setLabel(parentLabel + PAGE_PATH_SEPARATOR + contentPage.getPageLabel());
			}
		}
	}

	private String getParentLabel(final ContentPageModel contentPage)
	{
		return contentPage.getParentPage() != null ? contentPage.getParentPage().getLabel() : "";
	}

	private boolean isPagePathAffected(final ContentPageModel contentPage, final InterceptorContext interceptorContext)
	{
		return interceptorContext.isModified(contentPage, ContentPageModel.PAGELABEL)
				|| interceptorContext.isModified(contentPage, ContentPageModel.PARENTPAGE);
	}
}
