package com.wiley.core.product;

import com.wiley.core.model.WileyFreeTrialVariantProductModel;


/**
 * Created by Raman_Hancharou on 3/30/2016.
 */
public interface WileyFreeTrialVariantProductService
{
	WileyFreeTrialVariantProductModel getFreeTrialVariantProductByCode(String code);
}
