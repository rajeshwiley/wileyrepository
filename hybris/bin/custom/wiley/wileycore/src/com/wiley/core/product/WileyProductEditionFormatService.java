package com.wiley.core.product;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;

import javax.annotation.Nonnull;


/**
 * Service contains methods for working with product's edition format.
 *
 * @author Aliaksei_Zlobich
 */
public interface WileyProductEditionFormatService {

	/**
	 * Check if order has only digital products
	 *
	 * @param abstractOrder
	 * @return true if all products in given cart are digital.
	 */
	boolean isDigitalCart(AbstractOrderModel abstractOrder);

	/**
	 * Check if order has only physical products
	 *
	 * @param abstractOrder
	 * @return true if all products in given cart are digital.
	 */
	boolean isPhysicalCart(AbstractOrderModel abstractOrder);
  
	/**
	 * Finds all products which requires shipping.
	 *
	 * @param abstractOrder
	 * 		the abstract order
	 * @return the all shippable products
	 */
	@Nonnull
	List<ProductModel> getAllShippableProducts(@Nonnull AbstractOrderModel abstractOrder);

	/**
	 * Returns true if Cart has a digital product, otherwise false.
	 * @param cartModel the Cart of products to check
	 * @return
	 */
	boolean hasCartDigitalProduct(@Nonnull CartModel cartModel);

	/**
	 * Returns true if Product is a digital product, otherwise false.
	 * @param product the product to check
	 * @return true if product is digital
	 */
	boolean isDigitalProduct(ProductModel product);

	/**
	 * Returns true if Product is a physical(shippable) product, otherwise false.
	 * @param product the product to check
	 * @return true if product is physical(shippable)
	 */
	boolean isProductShippable(ProductModel product);
}
