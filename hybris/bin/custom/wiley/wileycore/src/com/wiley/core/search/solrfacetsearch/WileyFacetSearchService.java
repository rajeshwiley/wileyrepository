package com.wiley.core.search.solrfacetsearch;

import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.config.ValueRange;
import de.hybris.platform.solrfacetsearch.config.ValueRangeSet;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.solrfacetsearch.search.impl.DefaultFacetSearchService;

import org.apache.commons.collections4.MapUtils;


/**
 * @author Dzmitryi_Halahayeu
 */
public class WileyFacetSearchService extends DefaultFacetSearchService
{
	public static final String RANGE_PATTERN = "[%s TO %s]";
	private boolean skipFacets;

	@Override
	public SearchQuery createSearchQuery(final FacetSearchConfig facetSearchConfig, final IndexedType indexedType)
	{
		return new WileySearchQuery(facetSearchConfig, indexedType);
	}

	@Override
	protected void populateFacetFields(final FacetSearchConfig facetSearchConfig, final IndexedType indexedType,
			final SearchQuery searchQuery)
	{
		if (!skipFacets)
		{
			super.populateFacetFields(facetSearchConfig, indexedType, searchQuery);
			populateQueryFacets(indexedType, (WileySearchQuery) searchQuery);
		}
	}

	void populateQueryFacets(final IndexedType indexedType, final WileySearchQuery searchQuery)
	{
		for (final IndexedProperty indexedProperty : indexedType.getIndexedProperties().values())
		{
			if (indexedProperty.isQueryFacet() && MapUtils.isNotEmpty(indexedProperty.getQueryValueRangeSets()))
			{

				for (final ValueRangeSet valueRangeSet : indexedProperty.getQueryValueRangeSets().values())
				{
					for (final ValueRange valueRange : valueRangeSet.getValueRanges())
					{
						final WileyQueryFacet wileyQueryFacet = new WileyQueryFacet(indexedProperty.getName(),
								String.format(RANGE_PATTERN, valueRange.getFrom(), valueRange.getTo()));
						searchQuery.addQueryFacet(wileyQueryFacet);
					}
				}
			}
		}
	}

	public boolean isSkipFacets()
	{
		return skipFacets;
	}

	public void setSkipFacets(final boolean skipFacets)
	{
		this.skipFacets = skipFacets;
	}
}
