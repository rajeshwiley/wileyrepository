package com.wiley.core.metainformation;

import de.hybris.platform.acceleratorservices.metainformation.HtmlMetaTag;
import de.hybris.platform.acceleratorservices.storefront.data.MetaElementData;

import java.util.Map;

import org.apache.commons.lang.StringUtils;


/**
 * Created by Raman_Hancharou on 7/5/2017.
 */
public class WileyHtmlMetaTag extends HtmlMetaTag
{

	public static final String CONTENT = "content";
	public static final String META = "meta";

	/**
	 * Override OOTB behavior to write elements with not-empty content attribute
	 */
	@Override
	protected void writeMetaElement(final MetaElementData metaElementData)
	{
		// Write the open element
		final Map<String, String> elementAttributes = getAttributesMap(metaElementData);
		if (elementAttributes != null && elementAttributes.size() > 0 && StringUtils.isNotEmpty(elementAttributes.get(CONTENT)))
		{
			htmlElementHelper.writeOpenElement(pageContext, META, elementAttributes);
		}
	}

	@Override
	protected Map<String, String> getAttributesMap(final MetaElementData metaElementData)
	{
		return super.getAttributesMap(metaElementData);
	}
}
