package com.wiley.core.payment.populators;

import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.CustomerBillToDataPopulator;
import de.hybris.platform.acceleratorservices.payment.data.CustomerBillToData;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.springframework.util.Assert;


public class WileyCustomerBillToDataPopulator extends CustomerBillToDataPopulator
{
	@Override
	public void populate(final AddressModel paymentAddress, final CustomerBillToData target) throws ConversionException
	{
		final CountryModel countryModel = paymentAddress.getCountry();

		Assert.notNull(countryModel);

		super.populate(paymentAddress, target);

		target.setCountryNumericIsocode(countryModel.getNumeric());
		target.setBillToCountryName(countryModel.getName());

		final RegionModel region = paymentAddress.getRegion();
		if (region != null)
		{
			target.setBillToStateName(region.getName());
		}
	}
}
