package com.wiley.core.abstractorderentry.service;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;

import java.util.Optional;


public interface WileyAbstractOrderEntryService
{
	Optional<AbstractOrderEntryModel> getAbstractOrderEntryForBusinessKey(String businessKey);

	AbstractOrderEntryModel getAbstractOrderEntryForGuid(String siteId, String guid);
}
