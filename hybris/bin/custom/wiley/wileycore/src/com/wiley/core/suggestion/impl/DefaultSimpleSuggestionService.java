/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.core.suggestion.impl;

import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;

import java.util.List;

import com.wiley.core.suggestion.SimpleSuggestionService;
import com.wiley.core.suggestion.dao.SimpleSuggestionDao;


/**
 * Default implementation of {@link SimpleSuggestionService}.
 */
public class DefaultSimpleSuggestionService implements SimpleSuggestionService
{
	private SimpleSuggestionDao simpleSuggestionDao;

	/**
	 * Gets references for purchased in category.
	 *
	 * @param category
	 * 		the category
	 * @param user
	 * 		the user
	 * @param referenceType
	 * 		the reference type
	 * @param excludePurchased
	 * 		the exclude purchased
	 * @param limit
	 * 		the limit
	 * @return the references for purchased in category
	 */
	@SuppressWarnings("deprecation")
	@Override
	@Deprecated
	public List<ProductModel> getReferencesForPurchasedInCategory(final CategoryModel category, final UserModel user,
			final ProductReferenceTypeEnum referenceType, final boolean excludePurchased, final Integer limit)
	{
		return getSimpleSuggestionDao().findProductsRelatedToPurchasedProductsByCategory(category, user, referenceType,
				excludePurchased, limit);
	}

	/**
	 * Gets references for purchased in category.
	 *
	 * @param category
	 * 		the category
	 * @param referenceTypes
	 * 		the reference types
	 * @param user
	 * 		the user
	 * @param excludePurchased
	 * 		the exclude purchased
	 * @param limit
	 * 		the limit
	 * @return the references for purchased in category
	 */
	@Override
	public List<ProductModel> getReferencesForPurchasedInCategory(final CategoryModel category,
			final List<ProductReferenceTypeEnum> referenceTypes, final UserModel user, final boolean excludePurchased,
			final Integer limit)
	{
		return getSimpleSuggestionDao().findProductsRelatedToPurchasedProductsByCategory(category, referenceTypes, user,
				excludePurchased, limit);
	}

	/**
	 * Gets references for products.
	 *
	 * @param products
	 * 		the products
	 * @param referenceTypes
	 * 		the reference types
	 * @param user
	 * 		the user
	 * @param excludePurchased
	 * 		the exclude purchased
	 * @param limit
	 * 		the limit
	 * @return the references for products
	 */
	@Override
	public List<ProductModel> getReferencesForProducts(final List<ProductModel> products,
			final List<ProductReferenceTypeEnum> referenceTypes, final UserModel user, final boolean excludePurchased,
			final Integer limit)
	{
		return getSimpleSuggestionDao().findProductsRelatedToProducts(products, referenceTypes, user, excludePurchased, limit);
	}

	/**
	 * Gets simple suggestion dao.
	 *
	 * @return the simple suggestion dao
	 */
	protected SimpleSuggestionDao getSimpleSuggestionDao()
	{
		return simpleSuggestionDao;
	}

	/**
	 * Sets simple suggestion dao.
	 *
	 * @param simpleSuggestionDao
	 * 		the simple suggestion dao
	 */
	public void setSimpleSuggestionDao(final SimpleSuggestionDao simpleSuggestionDao)
	{
		this.simpleSuggestionDao = simpleSuggestionDao;
	}
}
