package com.wiley.core.wiley.service;

import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.cronjob.model.JobModel;

public interface WileyJobsDataImportService
{
    /**
     * Set {@link CronJobModel#ACTIVE} flag
     *
     * @param cronJobCode
     * 		{@link CronJobModel#CODE}
     * @return True if CronJob was found and updated, otherwise false
     */
    boolean deactivateCronJob(String cronJobCode);

    /**
     * Set {@link CronJobModel#ACTIVE} flag
     *
     * @param cronJobCode
     * 		{@link CronJobModel#CODE}
     * @param activateTriggers
     * @return True if CronJob was found and updated, otherwise false
     */
    boolean activateCronJob(String cronJobCode, boolean activateTriggers);

    /**
     * Set {@link JobModel#ACTIVE} flag
     *
     * @param jobCode
     * 		{@link JobModel#CODE}
     * @return True if Job was found and updated, otherwise false
     */
    boolean activateJob(String jobCode);

    /**
     * Set {@link JobModel#ACTIVE} flag
     *
     * @param jobCode
     * 		{@link JobModel#CODE}
     * @return True if Job was found and updated, otherwise false
     */
    boolean deactivateJob(String jobCode);

    /**
     * Activation and execution of cron job synchronously
     *
     * @param cronJobCode
     * 		Cron job code
     * @return True if cron job was executed
     */
    boolean executeCronJob(String cronJobCode, boolean activateTriggers);
}
