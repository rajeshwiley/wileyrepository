package com.wiley.core.integration.eloqua.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.google.common.base.MoreObjects;


/**
 * Please see http://secure.eloqua.com/api/docs/Static/Rest/1.0/t_fieldvalue_f280e94e267dd511545d45fb61379018.htm
 *
 * @author Aliaksei_Zlobich
 */
@XmlRootElement(name = "FieldValue")
@XmlAccessorType(XmlAccessType.FIELD)
public class FieldValueDto
{

	private String type = "FieldValue";

	private Integer id;

	private String value;

	public String getType()
	{
		return type;
	}

	public void setType(final String type)
	{
		this.type = type;
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(final Integer id)
	{
		this.id = id;
	}

	public String getValue()
	{
		return value;
	}

	public void setValue(final String value)
	{
		this.value = value;
	}

	@Override
	public String toString()
	{
		return MoreObjects.toStringHelper(this)
				.add("type", type)
				.add("id", id)
				.add("value", value)
				.toString();
	}
}
