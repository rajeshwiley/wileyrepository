package com.wiley.core.wileyb2c.customer.impl;

import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.ArrayList;

import com.wiley.core.wileycom.customer.service.impl.WileycomCustomerAccountServiceImpl;
import com.wiley.core.wileyb2c.customer.WileyB2CCustomerAccountService;


/**
 * Customer Account Service, contains logic specific for Wiley b2c site ONLY!
 */
public class WileyB2CCustomerAccountServiceImpl extends WileycomCustomerAccountServiceImpl
		implements WileyB2CCustomerAccountService
{
	@Override
	public void convertGuestToCustomer(final String pwd, final String orderGUID, final boolean acceptPromotions)
			throws DuplicateUidException
	{
		final OrderModel orderModel = getOrderDetailsForGUID(orderGUID, getBaseStoreService().getCurrentBaseStore());
		if (orderModel == null)
		{
			throw new UnknownIdentifierException("Order with guid " + orderGUID + " not found in current BaseStore");
		}

		final CustomerModel customer = (CustomerModel) orderModel.getUser();
		if (!CustomerType.GUEST.equals(customer.getType()))
		{
			throw new IllegalArgumentException("Order with guid " + orderGUID + " does not belong to guest user");
		}

		fillValuesForCustomerInfo(customer, orderModel);
		resetShippingAndPaymentInfo(customer);
		customer.setAcceptPromotions(acceptPromotions);
		register(customer, pwd);
		getUserService().setCurrentUser(customer);
	}

	private void resetShippingAndPaymentInfo(final CustomerModel customer)
	{
		customer.setDefaultShipmentAddress(null);
		customer.setDefaultPaymentInfo(null);
		customer.setDefaultPaymentAddress(null);
		customer.setPaymentInfos(new ArrayList<>());
		customer.setAddresses(new ArrayList<>());
	}
}
