package com.wiley.core.externaltax.strategies;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;


/**
 * The strategy for resolving the address for Tax calculation.
 */
public interface WileyTaxAddressStrategy
{
	/**
	 * Resolve proper payment address for tax calculation.
	 * @param abstractOrder - the Order
	 * @return - Address for taxes calculation
	 */
	AddressModel resolvePaymentAddress(AbstractOrderModel abstractOrder);

	/**
	 * Resolve proper delivery address for tax calculation.
	 * @param abstractOrder - the Order
	 * @return - Address for taxes calculation
	 */
	AddressModel resolveDeliveryAddress(AbstractOrderModel abstractOrder);
}
