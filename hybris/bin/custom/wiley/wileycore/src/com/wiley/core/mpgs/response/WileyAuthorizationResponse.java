package com.wiley.core.mpgs.response;


import java.math.BigDecimal;
import java.util.Date;


public class WileyAuthorizationResponse extends WileyResponse
{
	private String token;
	private String transactionId;
	private String currency;
	private BigDecimal totalAmount;
	private Date transactionCreatedTime;


	public String getToken()
	{
		return token;
	}

	public void setToken(final String token)
	{
		this.token = token;
	}

	public String getTransactionId()
	{
		return transactionId;
	}

	public void setTransactionId(final String transactionId)
	{
		this.transactionId = transactionId;
	}

	public String getCurrency()
	{
		return currency;
	}

	public void setCurrency(final String currency)
	{
		this.currency = currency;
	}

	public Date getTransactionCreatedTime()
	{
		return transactionCreatedTime;
	}

	public void setTransactionCreatedTime(final Date transactionCreatedTime)
	{
		this.transactionCreatedTime = transactionCreatedTime;
	}

	public BigDecimal getTotalAmount()
	{
		return totalAmount;
	}

	public void setTotalAmount(final BigDecimal totalAmount)
	{
		this.totalAmount = totalAmount;
	}
}
