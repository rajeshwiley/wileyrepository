package com.wiley.core.mpgs.response;




import java.math.BigDecimal;
import java.util.Date;


public class WileyCaptureResponse extends WileyResponse
{
	private String transactionId;
	private String transactionCurrency;
	private BigDecimal transactionAmount;
	private Date transactionCreatedTime;

	public String getTransactionId()
	{
		return transactionId;
	}

	public void setTransactionId(final String transactionId)
	{
		this.transactionId = transactionId;
	}

	public String getTransactionCurrency()
	{
		return transactionCurrency;
	}

	public void setTransactionCurrency(final String transactionCurrency)
	{
		this.transactionCurrency = transactionCurrency;
	}

	public BigDecimal getTransactionAmount()
	{
		return transactionAmount;
	}

	public void setTransactionAmount(final BigDecimal transactionAmount)
	{
		this.transactionAmount = transactionAmount;
	}

	public Date getTransactionCreatedTime()
	{
		return transactionCreatedTime;
	}

	public void setTransactionCreatedTime(final Date transactionCreatedTime)
	{
		this.transactionCreatedTime = transactionCreatedTime;
	}
}
