package com.wiley.core.validation.constraintvalidators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.wiley.core.validation.constraints.DoubleNonnegative;


/**
 * Validator for {@link DoubleNonnegative} annotation
 */
public class DoubleNonnegativeValidator implements ConstraintValidator<DoubleNonnegative, Double>
{
	@Override
	public void initialize(final DoubleNonnegative doubleNonnegative)
	{
		//ignore
	}

	@Override
	public boolean isValid(final Double value, final ConstraintValidatorContext constraintValidatorContext)
	{
		if (value == null)
		{
			return true; //delegate null case to @NotNull
		}

		return 0D <= value;
	}
}
