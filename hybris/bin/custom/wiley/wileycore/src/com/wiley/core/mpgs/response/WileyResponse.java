package com.wiley.core.mpgs.response;

public abstract class WileyResponse
{
	private String status;
	private String statusDetails;

	public String getStatus()
	{
		return status;
	}

	public void setStatus(final String status)
	{
		this.status = status;
	}

	public String getStatusDetails()
	{
		return statusDetails;
	}

	public void setStatusDetails(final String statusDetails)
	{
		this.statusDetails = statusDetails;
	}
}
