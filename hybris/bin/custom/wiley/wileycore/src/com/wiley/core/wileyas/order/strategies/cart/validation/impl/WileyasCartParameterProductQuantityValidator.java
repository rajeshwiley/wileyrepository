package com.wiley.core.wileyas.order.strategies.cart.validation.impl;

import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.product.ProductModel;

import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.product.WileyCountableProductService;
import com.wiley.core.wiley.strategies.cart.validation.WileyCartParameterValidator;


public class WileyasCartParameterProductQuantityValidator implements WileyCartParameterValidator
{
	public static final String PRODUCT_QUANTITY_MESSAGE = "Product '%s' cannot have quantity greater than %d.";
	private WileyCountableProductService wileyCountableProductService;

	@Override
	public void validateAddToCart(final CommerceCartParameter parameter) throws CommerceCartModificationException
	{
		final ProductModel product = parameter.getProduct();
		final long quantity = parameter.getQuantity();
		if (!getWileyCountableProductService().canProductHaveQuantity(product))
		{
			// if client is trying to add more than maximum allowed quantity, throw exception
			if (quantity > WileyCoreConstants.DEFAULT_QUANTITY_FOR_PRODUCT_WHICH_CANNOT_HAVE_QUANTITY)
			{
				throw new CommerceCartModificationException(String.format(PRODUCT_QUANTITY_MESSAGE,
						product.getCode(), WileyCoreConstants.DEFAULT_QUANTITY_FOR_PRODUCT_WHICH_CANNOT_HAVE_QUANTITY));
			}
		}
	}

	public WileyCountableProductService getWileyCountableProductService()
	{
		return wileyCountableProductService;
	}

	@Required
	public void setWileyCountableProductService(final WileyCountableProductService wileyCountableProductService)
	{
		this.wileyCountableProductService = wileyCountableProductService;
	}
}
