package com.wiley.core.wileyb2b.order.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BUnitService;
import de.hybris.platform.b2bacceleratorservices.enums.CheckoutPaymentType;
import de.hybris.platform.b2bacceleratorservices.order.impl.DefaultB2BCommerceCartService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.InvoicePaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;

import java.util.Optional;

import javax.annotation.Resource;


public class Wileyb2bCommerceCartServiceImpl extends DefaultB2BCommerceCartService
{
	@Resource
	private B2BUnitService<B2BUnitModel, B2BCustomerModel> b2bUnitService;

	@Override
	public InvoicePaymentInfoModel createInvoicePaymentInfo(final CartModel cartModel)
	{
		InvoicePaymentInfoModel invoicePaymentInfoModel = super.createInvoicePaymentInfo(cartModel);
		Optional<AddressModel> paymentAddressFromOrganizationLevel = getPaymentAddressFromOrganizationLevel(cartModel.getUser());
		paymentAddressFromOrganizationLevel.ifPresent(address -> invoicePaymentInfoModel.setBillingAddress(address));
		return invoicePaymentInfoModel;
	}

	/**
	 * Get payment address from B2B organization level for the given customer.
	 *
	 * @param customerModel the customer model
	 * @return the B2B organization level payment address
	 */
	private Optional<AddressModel> getPaymentAddressFromOrganizationLevel(final UserModel customerModel)
	{
		Optional<AddressModel> paymentAddress = Optional.empty();
		if (customerModel instanceof B2BCustomerModel)
		{
			B2BCustomerModel b2bCustomerModel = (B2BCustomerModel) customerModel;
			Optional<B2BUnitModel> unit = b2bUnitService.getAllUnitsOfOrganization(b2bCustomerModel).stream().findAny();
			if (unit.isPresent()) {
				Optional<B2BUnitModel> rootUnit = Optional.ofNullable(b2bUnitService.getRootUnit(unit.get()));
				if (rootUnit.isPresent()) {
					paymentAddress = Optional.ofNullable(rootUnit.get().getBillingAddress());
				}
			}
		}
		return paymentAddress;
	}

	/**
	 * Clear payment by invoice specific attributes from cartModel on change payment type event
	 *
	 * @param cartModel cart model, which contains cartEntries with payment specific attributes
	 */
	@Override
	public void calculateCartForPaymentTypeChange(final CartModel cartModel)
	{
		if (!CheckoutPaymentType.ACCOUNT.equals(cartModel.getPaymentType()) && !cartModel.getEntries().isEmpty())
		{
			cartModel.getEntries().forEach(entry -> entry.setPoNumber(null));
		}
		super.calculateCartForPaymentTypeChange(cartModel);
	}
}