package com.wiley.core.wileyb2c.subscription.dao.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;

import java.util.List;

import com.wiley.core.model.WileySubscriptionModel;
import com.wiley.core.subscription.dao.impl.WileySubscriptionDaoImpl;
import com.wiley.core.wileyb2c.subscription.dao.Wileyb2cSubscriptionDao;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


public class Wileyb2cSubscriptionDaoImpl extends WileySubscriptionDaoImpl implements Wileyb2cSubscriptionDao
{
	private static final String QUERY_FIND_SUBSCRIPTIONS_BY_PRODUCT_AND_CUSTOMER = "SELECT {s:pk} FROM {WileySubscription AS s "
			+ "JOIN " + ProductModel._TYPECODE + " AS p ON {s." + WileySubscriptionModel.PRODUCT + "} = {p." + ProductModel.PK
			+ "}"
			+ "JOIN " + CustomerModel._TYPECODE + " AS c ON {s." + WileySubscriptionModel.CUSTOMER + "} = {c." + CustomerModel.PK
			+ "}}"
			+ "WHERE {p." + ProductModel.CODE + "}=?productCode AND {c." + CustomerModel.UID + "}=?customerUid";

	public Wileyb2cSubscriptionDaoImpl(final String typecode)
	{
		super(typecode);
	}

	@Override
	public List<WileySubscriptionModel> findSubscriptionsByProductAndCustomer(final String productCode, final String customerUid)
	{
		validateParameterNotNull(productCode, "productCode must not be null!");
		validateParameterNotNull(customerUid, "customerUid must not be null!");

		FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(QUERY_FIND_SUBSCRIPTIONS_BY_PRODUCT_AND_CUSTOMER);
		flexibleSearchQuery.addQueryParameter("productCode", productCode);
		flexibleSearchQuery.addQueryParameter("customerUid", customerUid);
		return getFlexibleSearchService().<WileySubscriptionModel> search(flexibleSearchQuery).getResult();
	}


}
