package com.wiley.core.subscription.events;

import de.hybris.platform.servicelayer.event.events.AbstractEvent;

import com.wiley.core.model.WileySubscriptionModel;


/**
 * Simple POJO class.<br/>
 * Event starts process of renewing subscriptions.
 *
 * @author Aliaksei_Zlobich
 */
public class SubscriptionRenewalEvent extends AbstractEvent
{
	/**
	 * Subscription which should be renewed.
	 */
	private WileySubscriptionModel subscription;

	public void setSubscription(final WileySubscriptionModel subscription)
	{
		this.subscription = subscription;
	}


	public WileySubscriptionModel getSubscription()
	{
		return subscription;
	}
}
