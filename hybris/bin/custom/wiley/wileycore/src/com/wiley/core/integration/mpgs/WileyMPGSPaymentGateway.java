package com.wiley.core.integration.mpgs;

import javax.annotation.Nonnull;
import org.springframework.messaging.handler.annotation.Payload;
import com.wiley.core.mpgs.request.WileyAuthorizationRequest;
import com.wiley.core.mpgs.request.WileyCaptureRequest;
import com.wiley.core.mpgs.request.WileyTokenizationRequest;
import com.wiley.core.mpgs.request.WileyVerifyRequest;
import com.wiley.core.mpgs.response.WileyAuthorizationResponse;
import com.wiley.core.mpgs.response.WileyFollowOnRefundResponse;
import com.wiley.core.mpgs.response.WileyCaptureResponse;
import com.wiley.core.mpgs.response.WileyTokenizationResponse;
import com.wiley.core.mpgs.request.WileyRetrieveSessionRequest;
import com.wiley.core.mpgs.response.WileyRetrieveSessionResponse;
import com.wiley.core.mpgs.response.WileyVerifyResponse;
import com.wiley.core.payment.request.WileyFollowOnRefundRequest;



public interface WileyMPGSPaymentGateway
{
	@Nonnull
	WileyRetrieveSessionResponse retrieveSession(@Nonnull @Payload WileyRetrieveSessionRequest request);

	@Nonnull
	WileyVerifyResponse verify(@Nonnull @Payload WileyVerifyRequest request);

	@Nonnull
	WileyTokenizationResponse tokenize(@Nonnull @Payload WileyTokenizationRequest request);

	@Nonnull
	WileyAuthorizationResponse authorize(@Nonnull @Payload WileyAuthorizationRequest request);

	@Nonnull
	WileyCaptureResponse capture(@Nonnull @Payload WileyCaptureRequest request);

	@Nonnull
	WileyFollowOnRefundResponse refundFollowOn(@Nonnull @Payload WileyFollowOnRefundRequest request);
}
