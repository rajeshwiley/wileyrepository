package com.wiley.core.integration.alm.user.service;

import javax.annotation.Nonnull;

import com.wiley.core.integration.alm.user.dto.UserDto;


public interface AlmUserService
{
	@Nonnull
	UserDto getUserData(@Nonnull String userId);
}
