package com.wiley.core.search.solrfacetsearch;

import de.hybris.platform.solrfacetsearch.search.FacetSearchException;
import de.hybris.platform.solrfacetsearch.search.SearchResult;
import de.hybris.platform.solrfacetsearch.search.context.FacetSearchContext;
import de.hybris.platform.solrfacetsearch.solr.Index;

import java.io.IOException;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrServerException;


/**
 * @author Dzmitryi_Halahayeu
 */
public class WileyFacetContentSearchStrategy extends WileyFacetSearchStrategy
{

	@Override
	protected SearchResult postProcessSearchResult(final SolrClient solrClient, final FacetSearchContext facetSearchContext,
			final Index index,
			final SearchResult searchResult)
			throws FacetSearchException, SolrServerException, IOException
	{
		return searchResult;
	}
}
