package com.wiley.core.integration.address.service;

import com.wiley.core.integration.address.dto.AddressDto;
import com.wiley.core.integration.address.dto.AddressVerificationResultDto;


/**
 * Interface for verifying an Address with an external verification service. Each class that implements this interface
 * will work with a different service.
 */
public interface WileyasAddressVerificationService
{
	/**
	 * @param addressData
	 * 		the address to be verified.
	 * @return address suggestions and address verification decision.
	 */
	AddressVerificationResultDto verifyAddress(AddressDto addressData);
}
