package com.wiley.core.wileyb2c.product.access.impl;

import de.hybris.platform.core.model.order.OrderEntryModel;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.enums.WileyDigitalContentTypeEnum;
import com.wiley.core.wileyb2c.product.access.Wileyb2cDownloadDigitalProductStrategy;


/**
 * Generating signed url for secured access to digital product from VitalSource
 *
 * @author Dzmitryi_Halahayeu
 */
public class Wileyb2cVitalSourceDownloadDigitalStrategy implements Wileyb2cDownloadDigitalProductStrategy
{
	private String vitalSourceUrl;

	@Override
	public boolean apply(@Nonnull final OrderEntryModel orderEntry)
	{
		return WileyDigitalContentTypeEnum.VITAL_SOURCE.equals(orderEntry.getProduct().getDigitalContentType());
	}

	@Override
	public String generateRedirectUrl(@Nonnull final OrderEntryModel orderEntry) throws Exception
	{
		return vitalSourceUrl;
	}

	@Required
	public void setVitalSourceUrl(final String vitalSourceUrl)
	{
		this.vitalSourceUrl = vitalSourceUrl;
	}

}
