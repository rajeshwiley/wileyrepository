package com.wiley.core.wileyb2c.search.solrfacetsearch.indexer.strategies;

import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexOperation;
import de.hybris.platform.solrfacetsearch.indexer.IndexerContext;
import de.hybris.platform.solrfacetsearch.indexer.exceptions.IndexerException;
import de.hybris.platform.solrfacetsearch.indexer.strategies.impl.DefaultIndexerStrategy;
import de.hybris.platform.solrfacetsearch.indexer.workers.IndexerWorker;
import de.hybris.platform.solrfacetsearch.solr.SolrIndexOperationService;
import de.hybris.platform.solrfacetsearch.solr.exceptions.SolrServiceException;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.util.Config;
import de.hybris.platform.util.ThreadUtilities;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * @author Dzmitryi_Halahayeu
 */
public class WileyIndexerStrategy extends DefaultIndexerStrategy
{
	private static final Logger LOG = LogManager.getLogger(WileyIndexerStrategy.class);
	private final List<Long> operationIds = new ArrayList<>();
	@Resource
	private SolrIndexOperationService solrIndexOperationService;
	@Resource
	private BaseStoreService baseStoreService;
	@Resource
	private BaseSiteService baseSiteService;
	
	private static final int DEFAULT_NUMBER_OF_THREADS = 2;

	@Override
	public void execute() throws IndexerException
	{
		final String baseSiteUid = getFacetSearchConfig().getIndexConfig().getBaseSite().getUid();
		baseSiteService.setCurrentBaseSite(baseSiteUid, true);
		getSessionService().setAttribute("currentStore", baseStoreService.getCurrentBaseStore());
		super.execute();
		endIndexing();
	}

	private void endIndexing()
			throws IndexerException
	{
		//We run delete operation together with update. Delete operation is the first one
		if (IndexOperation.DELETE.equals(this.getIndexOperation()))
		{
			return;
		}
		LOG.debug("Finish indexing. Operations to finish {}",
				() -> operationIds.stream().map(Object::toString).collect(Collectors.joining(",")));
		for (final Long operationId : operationIds)
		{
			try
			{
				LOG.debug("Finish index for operation {}", operationId);
				this.solrIndexOperationService.endOperation(operationId, false);
			}
			catch (SolrServiceException exception)
			{
				throw new IndexerException(exception);
			}
		}
	}

	/*
	 * OOTB code with additional change in logic for passing number threads based
	 * on configuration and adding IndexOperationId to list of operationid's 
	 *
	 * @see de.hybris.platform.solrfacetsearch.indexer.strategies.impl.
	 * DefaultIndexerStrategy#doExecute(de.hybris.platform.solrfacetsearch.indexer
	 * .IndexerContext)
	 */
	@Override
	protected void doExecute(final IndexerContext indexerContext) throws IndexerException
	{
		final IndexConfig indexConfig = indexerContext.getFacetSearchConfig().getIndexConfig();
		final List pks = indexerContext.getPks();
		final ArrayList workers = new ArrayList();
		final int batchSize = indexConfig.getBatchSize();
		int numberOfThreads = indexConfig.getNumberOfThreads();
		if (numberOfThreads <= 0)
		{
			numberOfThreads = getThreadCount();
		}
		final int numberOfWorkers = (int) Math.ceil((double) pks.size() / (double) batchSize);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Batch size: " + batchSize);
			LOG.debug("Number of threads: " + numberOfThreads);
			LOG.debug("Number of workers: " + numberOfWorkers);
		}

		final ExecutorService executorService = this.createIndexerWorkersPool(numberOfThreads);
		final ExecutorCompletionService completionService = new ExecutorCompletionService(executorService);
		final int maxRetries = Math.max(0, indexConfig.getMaxRetries());
		final int maxBatchRetries = Math.max(0, indexConfig.getMaxBatchRetries());

		try
		{
			int index = 0;

			for (int start = 0; index < numberOfWorkers; start += batchSize, ++index)
			{
				final int end = Math.min(start + batchSize, pks.size());
				final List workerPks = pks.subList(start, end);
				final IndexerWorker indexerWorker = this.createIndexerWorker(indexerContext, index, workerPks);
				final IndexerWorkerWrapper indexerWorkerWrapper = new IndexerWorkerWrapper(indexerWorker,
						Integer.valueOf(index), maxBatchRetries, workerPks);
				workers.add(indexerWorkerWrapper);
			}

			this.runWorkers(indexerContext, completionService, workers, maxRetries);
		}
		finally
		{
			executorService.shutdownNow();
		}
		operationIds.add(indexerContext.getIndexOperationId());
	}

	private int getThreadCount()
	{
		final String expression = Config.getParameter("solrindexconfig.number.cores");
		return ThreadUtilities.getNumberOfThreadsFromExpression(expression, DEFAULT_NUMBER_OF_THREADS);
	}
}
