package com.wiley.core.wileyb2c.i18n.dao.impl;

import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.store.BaseStoreModel;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import com.wiley.core.wileyb2c.i18n.dao.Wileyb2cLanguageDao;


public class Wileyb2cLanguageDaoImpl implements Wileyb2cLanguageDao
{
	/*
			SELECT {l:isocde} FROM 
			{
				BaseStore AS bs 
				JOIN BaseStore2LanguageRel AS bsl 
				ON {bsl:source} = {bs:PK}
				JOIN Language AS l
				ON {bsl:target} = {l:PK}
			} 
			WHERE {bs:uid}=?baseStoreUid
			AND LENGTH({l:isocode}) = 5
			AND RIGHT({l:isocode},2) IN 
			({{
			  SELECT {c:isocode} FROM 
			  {
				  BaseStore AS bs 
				  JOIN BaseStore2CountryRel AS bsc
				  ON {bsc:target} = {bs:PK}
				  JOIN Country AS c
				  ON {bsc:source} = {c:PK}
			  } 
			  WHERE {bs:uid}=?baseStoreUid
			}})
		 */
	private static final StringBuilder QUERY_AVAILABLE_LANGUAGE_ISO_CODES = new StringBuilder("")
			.append("SELECT {l:").append(LanguageModel.ISOCODE).append("} FROM {")
			.append(BaseStoreModel._TYPECODE).append(" AS bs ")
			.append("JOIN BaseStore2LanguageRel AS bsl ")
			.append("ON {bsl:source} = {bs:").append(BaseStoreModel.PK).append("} ")
			.append("JOIN ").append(LanguageModel._TYPECODE).append(" AS l ")
			.append("ON {bsl:target} = {l:").append(LanguageModel.PK).append("}} ")

			.append("WHERE {bs:").append(BaseStoreModel.UID).append("}=?baseStoreUid ")
			.append("AND LENGTH({l:").append(LanguageModel.ISOCODE).append("}) = 5 ")
			.append("AND RIGHT({l:").append(LanguageModel.ISOCODE).append("},2) IN ")
			.append("({{")
			.append("  SELECT {c:").append(CountryModel.ISOCODE).append("} FROM ")
			.append("  {")
			.append("  ").append(BaseStoreModel._TYPECODE).append(" AS bs ")
			.append("  JOIN BaseStore2CountryRel AS bsc")
			.append("  ON {bsc:target} = {bs:").append(BaseStoreModel.PK).append("} ")
			.append("  JOIN ").append(CountryModel._TYPECODE).append(" AS c ")
			.append("  ON {bsc:source} = {c:").append(CountryModel.PK).append("}} ")
			.append("  WHERE {bs:").append(BaseStoreModel.UID).append("}=?baseStoreUid")
			.append("}})");

	private static final String BASE_STORE_UID_PARAMETER = "baseStoreUid";

	@Resource
	private FlexibleSearchService flexibleSearchService;

	public List<String> findLanguageIsoCodesForBaseStore(final String baseStoreUid)
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(QUERY_AVAILABLE_LANGUAGE_ISO_CODES.toString());
		final ArrayList resultClassList = new ArrayList();
		resultClassList.add(String.class);
		query.addQueryParameter(BASE_STORE_UID_PARAMETER, baseStoreUid);
		query.setResultClassList(resultClassList);
		return flexibleSearchService.<String> search(query).getResult();
	}
}
