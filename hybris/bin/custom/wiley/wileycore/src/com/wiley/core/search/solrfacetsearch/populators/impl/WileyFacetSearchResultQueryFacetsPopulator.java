package com.wiley.core.search.solrfacetsearch.populators.impl;

import de.hybris.platform.solrfacetsearch.config.FacetSortProvider;
import de.hybris.platform.solrfacetsearch.config.FacetType;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.config.ValueRange;
import de.hybris.platform.solrfacetsearch.config.ValueRangeSet;
import de.hybris.platform.solrfacetsearch.provider.FacetDisplayNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FacetValueDisplayNameProvider;
import de.hybris.platform.solrfacetsearch.search.Facet;
import de.hybris.platform.solrfacetsearch.search.FacetField;
import de.hybris.platform.solrfacetsearch.search.FacetValue;
import de.hybris.platform.solrfacetsearch.search.FacetValueField;
import de.hybris.platform.solrfacetsearch.search.FieldNameTranslator;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.solrfacetsearch.search.context.FacetSearchContext;
import de.hybris.platform.solrfacetsearch.search.impl.SearchResultConverterData;
import de.hybris.platform.solrfacetsearch.search.impl.SolrSearchResult;
import de.hybris.platform.solrfacetsearch.search.impl.populators.FacetSearchResultFacetsPopulator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.util.NamedList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.wiley.core.search.solrfacetsearch.populators.impl.WileyAbstractFacetSearchQueryPopulator.GROUP_FACET;


/**
 * @author Dzmitryi_Halahayeu
 */
public class WileyFacetSearchResultQueryFacetsPopulator extends FacetSearchResultFacetsPopulator
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyFacetSearchResultQueryFacetsPopulator.class);
	private static final String VAL = "val";
	private static final String COUNT = "count";
	private static final String BUCKETS = "buckets";
	private static final String FACETS = "facets";
	private static final String DEFAULT_RANGE_SET = "default";
	private static final String QUERY_FACET_NAME_DELIMETER = ":";
	private Set<String> skippedKeys;

	@Override
	public void populate(final SearchResultConverterData source, final SolrSearchResult target)
	{
		QueryResponse queryResponse = source.getQueryResponse();
		NamedList<Object> responseFacets = (NamedList<Object>) queryResponse.getResponse().get(FACETS);
		if (responseFacets != null && responseFacets.size() > 0)
		{
			FacetSearchContext searchContext = source.getFacetSearchContext();
			IndexedType indexedType = searchContext.getIndexedType();
			final SearchQuery searchQuery = searchContext.getSearchQuery();
			boolean isGroupFacets = searchQuery.isGroupFacets();
			FieldNameTranslator.FieldInfosMapping fieldInfosMapping = this.getFieldNameTranslator().getFieldInfos(
					source.getFacetSearchContext());
			Map fieldInfos = fieldInfosMapping.getInvertedFieldInfos();

			Map<String, Facet> facetsMap = new HashMap<>();
			for (final Map.Entry<String, Object> responseFacet : responseFacets)
			{
				Pair<String, List<Pair<Object, Integer>>> solrFacetValues = extractValueCounts(responseFacet, isGroupFacets);

				if (solrFacetValues != null)
				{
					String translatedFieldName = solrFacetValues.getLeft();
					FieldNameTranslator.FieldInfo fieldInfo = (FieldNameTranslator.FieldInfo) fieldInfos.get(
							translatedFieldName);

					if (fieldInfo != null)
					{
						String fieldName = fieldInfo.getFieldName();
						IndexedProperty indexedProperty = fieldInfo.getIndexedProperty();
						final Set<FacetValue> facetValues = new TreeSet<>();
						final List<FacetValue> selectedFacetValues = new ArrayList();

						for (Pair<Object, Integer> solrFacetValue : solrFacetValues.getRight())
						{
							Object value = solrFacetValue.getKey();
							Integer count = solrFacetValue.getValue();
							boolean isMultiSelect = indexedProperty != null && isMultiSelect(indexedProperty);
							boolean selected = isFacetValueSelected(searchQuery, fieldName, value.toString());
							boolean showFacet = isMultiSelect || !selected;
							final Facet facet = facetsMap.get(fieldName);

							if (facet != null)
							{
								facetValues.addAll(facet.getFacetValues());
							}
							if (count > 0)
							{
								String displayName = resolveFacetValueDisplayName(searchQuery, indexedProperty,
										value.toString());
								boolean isQueryFacet = indexedProperty != null && indexedProperty.isQueryFacet();
								FacetValue facetValue = null;
								if (displayName == null && !isQueryFacet)
								{
									facetValue = new FacetValue(value.toString(), value.toString(), count, selected);
								}
								else if (displayName != null)
								{
									facetValue = new FacetValue(value.toString(), displayName, count, selected);
								}

								if (Objects.nonNull(facetValue))
								{
									if (selected)
									{
										selectedFacetValues.add(facetValue);
									}
									if (showFacet)
									{
										facetValues.add(facetValue);
									}
								}
							}
						}

						List<FacetValue> facetValuesList = new ArrayList<>(facetValues);

						if (indexedProperty != null)
						{
							facetValuesList = sortFacetValues(searchQuery, indexedType, indexedProperty, facetValuesList);
						}

						Facet facet = new Facet(fieldName, facetValuesList);
						facet.setPriority(getFacetPriority(searchQuery.getFacets(), fieldName));
						facet.setFacetType(indexedProperty.getFacetType());
						facet.setMultiselect(indexedProperty.isMultiSelect());
						facet.setSelectedFacetValues(selectedFacetValues);
						facetsMap.put(facet.getName(), facet);
					}
				}
			}
			List<Facet> facetList = new ArrayList<>(facetsMap.values());
			Collections.sort(facetList, this::compareFacets);
			facetList.forEach(target::addFacet);
		}
	}

	private int getFacetPriority(final List<FacetField> facets, final String fieldName)
	{
		return facets.stream().filter(facetField -> fieldName.equals(facetField.getField())).mapToInt(FacetField::getPriority)
				.findFirst()
				.orElse(0);
	}

	/* TODO-REFACT-1808
	 * OOTB 6.2
	 */
	protected List<FacetValue> sortFacetValues(final SearchQuery searchQuery, final IndexedType indexedType,
			final IndexedProperty indexedProperty, final List<FacetValue> facetValues)
	{
		if (isRanged(indexedProperty))
		{
			List<ValueRange> valueRanges = this.getValueRanges(indexedProperty,
					indexedProperty.isCurrency() ? searchQuery.getCurrency() : null);
			List<FacetValue> sortedFacetValues = new ArrayList<>(facetValues.size());

			for (ValueRange valueRange : valueRanges)
			{
				FacetValue facetValue = this.findFacetValue(facetValues, valueRange.getName());

				if (facetValue != null)
				{
					sortedFacetValues.add(facetValue);
				}
			}

			return sortedFacetValues;
		}
		else
		{
			FacetSortProvider sortProvider = this.resolveFacetValuesSortProvider(indexedProperty);

			if (sortProvider != null)
			{
				Comparator<FacetValue> comparator = sortProvider.getComparatorForTypeAndProperty(indexedType, indexedProperty);
				facetValues.sort(comparator);
			}

			return facetValues;
		}
	}

	/* TODO-REFACT-1808
	 * OOTB 6.2
	 */
	protected FacetValue findFacetValue(final List<FacetValue> facetValues, final String name)
	{
		for (FacetValue facetValue : facetValues)
		{
			if (name.equals(facetValue.getName()))
			{
				return facetValue;
			}
		}

		return null;
	}

	/* TODO-REFACT-1808
	 * OOTB 6.2
	 */
	protected List<ValueRange> getValueRanges(final IndexedProperty property, final String qualifier)
	{
		ValueRangeSet valueRangeSet;

		if (qualifier == null)
		{
			valueRangeSet = property.getValueRangeSets().get(DEFAULT_RANGE_SET);
		}
		else
		{
			valueRangeSet = property.getValueRangeSets().get(qualifier);

			if (valueRangeSet == null)
			{
				valueRangeSet = property.getValueRangeSets().get(DEFAULT_RANGE_SET);
			}
		}

		return valueRangeSet != null ? valueRangeSet.getValueRanges() : Collections.emptyList();
	}

	/* TODO-REFACT-1808
	 * OOTB 6.2
	 */
	protected boolean isRanged(final IndexedProperty indexedProperty)
	{
		return !MapUtils.isEmpty(indexedProperty.getValueRangeSets());
	}

	/* TODO-REFACT-1808
	 * OOTB 1808 modified (method signature)
	 */
	protected FacetSortProvider resolveFacetValuesSortProvider(final IndexedProperty indexedProperty)
	{
		String beanName = indexedProperty.getFacetSortProvider();
		Object bean = beanName != null ? getBeanFactory().getBean(beanName) : null;
		return bean instanceof FacetSortProvider ? (FacetSortProvider) bean : null;
	}

	protected Object getFacetValueDisplayNameProvider(final IndexedProperty indexedProperty)
	{
		String name = indexedProperty != null ? indexedProperty.getFacetDisplayNameProvider() : null;
		return name == null ? null : getBeanFactory().getBean(name);
	}

	/* TODO-REFACT-1808
	 * OOTB 6.2
	 * Needs to be refactored to use de.hybris.platform.solrfacetsearch.search.impl.populators.FacetSearchResultFacetsPopulator
	 * .isFacetValueSelected(FacetInfo, Count)
	 */
	protected boolean isFacetValueSelected(final SearchQuery searchQuery, final String field, final String value)
	{
		Iterator iterator = searchQuery.getFacetValues().iterator();
		FacetValueField facetValue;

		do
		{
			if (!iterator.hasNext())
			{
				return false;
			}

			facetValue = (FacetValueField) iterator.next();
		}
		while (!facetValue.getField().equals(field) || !facetValue.getValues().contains(value));

		return true;
	}

	/* TODO-REFACT-1808
	 * OOTB 1808 modified (method signature)
	 * Needs to be refactored to use de.hybris.platform.solrfacetsearch.search.impl.populators.FacetSearchResultFacetsPopulator
	 * .resolveFacetValueDisplayName(SearchQuery, FacetSearchResultFacetsPopulator, Object, String)
	 */
	protected String resolveFacetValueDisplayName(final SearchQuery searchQuery, final IndexedProperty indexedProperty,
			final String facetValue)
	{
		Object facetDisplayNameProvider = this.getFacetValueDisplayNameProvider(indexedProperty);
		if (facetDisplayNameProvider != null)
		{
			if (facetDisplayNameProvider instanceof FacetValueDisplayNameProvider)
			{
				return ((FacetValueDisplayNameProvider) facetDisplayNameProvider).getDisplayName(searchQuery, indexedProperty,
						facetValue);
			}

			if (facetDisplayNameProvider instanceof FacetDisplayNameProvider)
			{
				return ((FacetDisplayNameProvider) facetDisplayNameProvider).getDisplayName(searchQuery, facetValue);
			}
		}

		return facetValue;
	}

	/* TODO-REFACT-1808
	 * OOTB 6.2
	 */
	protected boolean isMultiSelect(final IndexedProperty indexedProperty)
	{
		FacetType facetType = indexedProperty.getFacetType();
		return facetType == FacetType.MULTISELECTAND || facetType == FacetType.MULTISELECTOR;
	}

	private Pair<String, List<Pair<Object, Integer>>> extractValueCounts(final Map.Entry<String, Object> pair,
			final boolean isGroupFacets)
	{
		if (skippedKeys.contains(pair.getKey()))
		{
			LOG.debug("Response pair with key {} was skipped according to bean configuration", pair.getKey());
			return null;
		}

		if (pair.getValue() instanceof NamedList) //facet items are only NamedList
		{
			NamedList<Object> namedList = (NamedList) pair.getValue();

			//query facets
			if (pair.getKey().contains(QUERY_FACET_NAME_DELIMETER) && namedList.get(COUNT) instanceof Integer)
			{
				String translatedFieldName = pair.getKey().split(QUERY_FACET_NAME_DELIMETER)[0];
				String value = pair.getKey().split(QUERY_FACET_NAME_DELIMETER)[1];
				Integer count = getCount(namedList, isGroupFacets);
				List<Pair<Object, Integer>> facetValueCounts = Collections.singletonList(Pair.of(value, count));
				return Pair.of(translatedFieldName, facetValueCounts);
			}

			//non-query facets
			if (namedList.get(BUCKETS) instanceof List)
			{
				String translatedFieldName = pair.getKey();
				List<NamedList> list = (List) namedList.get(BUCKETS);
				List<Pair<Object, Integer>> facetValueCounts = new ArrayList<>(list.size());
				for (NamedList<Integer> namedValue : list)
				{
					Object value = namedValue.get(VAL);
					Integer count = getCount(namedValue, isGroupFacets);
					facetValueCounts.add(Pair.of(value, count));
				}
				return Pair.of(translatedFieldName, facetValueCounts);
			}
		}

		throw new IllegalArgumentException(String.format("Solr response facet pair has unexpected [%s] key", pair.getKey()));
	}

	private Integer getCount(final NamedList<?> namedList, final boolean isGroupFacets)
	{
		Integer count = (Integer) namedList.get(isGroupFacets ? GROUP_FACET : COUNT);
		return count == null ? 0 : count;
	}

	public Set<String> getSkippedKeys()
	{
		return skippedKeys;
	}

	public void setSkippedKeys(final Set<String> skippedKeys)
	{
		this.skippedKeys = skippedKeys;
	}
}