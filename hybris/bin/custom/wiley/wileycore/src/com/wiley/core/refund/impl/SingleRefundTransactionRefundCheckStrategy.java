package com.wiley.core.refund.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.base.Preconditions;
import com.wiley.core.integration.edi.service.impl.WileyPurchaseOrderService;
import com.wiley.core.payment.transaction.PaymentTransactionService;
import com.wiley.core.refund.WileyRefundCheckStrategy;
import com.wiley.fulfilmentprocess.model.WileyExportProcessModel;


public class SingleRefundTransactionRefundCheckStrategy implements WileyRefundCheckStrategy
{
	@Autowired
	private PaymentTransactionService transactionService;
	@Autowired
	private WileyPurchaseOrderService wileyPurchaseOrderService;

	@Override
	public boolean isFullyRefundedOrder(final OrderModel order)
	{
		Preconditions.checkNotNull(order);
		final PaymentTransactionEntryModel captureTransactionEntry =
				transactionService.getAcceptedTransactionEntry(order, PaymentTransactionType.CAPTURE);
		final List<PaymentTransactionEntryModel> refundTransactionEntries =
				transactionService.getAcceptedTransactionEntries(order, PaymentTransactionType.REFUND_FOLLOW_ON);
		if (captureTransactionEntry == null)
		{
			throw new IllegalStateException("Payment was not captured yet for order: " + order.getCode());
		}
		if (!refundTransactionEntries.isEmpty())
		{
			final BigDecimal captureAmount = captureTransactionEntry.getAmount();
			BigDecimal refundAmount = refundTransactionEntries.stream().map(PaymentTransactionEntryModel::getAmount).reduce(
					BigDecimal.ZERO, BigDecimal::add);

			if (captureAmount.compareTo(refundAmount) == 0)
			{

				return true;
			}
			else if (captureAmount.compareTo(refundAmount) < 0)
			{

				throw new IllegalStateException("Refund amount for order is bigger then capture amount");
			}
		}
		return false;
	}


	@Override
	public boolean isOrderRefundedByOneTransaction(final WileyExportProcessModel exportProcess)
	{
		Preconditions.checkNotNull(exportProcess);
		Preconditions.checkNotNull(exportProcess.getOrder());
		Preconditions.checkNotNull(exportProcess.getTransactionEntry());

		OrderModel order = exportProcess.getOrder();

		final PaymentTransactionEntryModel captureTransactionEntry =
				transactionService.getAcceptedTransactionEntry(order, PaymentTransactionType.CAPTURE);

		final boolean isRefundedProcess = wileyPurchaseOrderService.isOrderRefunded(exportProcess);

		if (isRefundedProcess)
		{
			BigDecimal refundAmount = exportProcess.getTransactionEntry().getAmount();
			return refundAmount.compareTo(captureTransactionEntry.getAmount()) == 0;

		}
		return false;
	}

}
