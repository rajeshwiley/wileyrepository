package com.wiley.core.cart.impl;


import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.order.dao.impl.DefaultCommerceCartDao;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import com.wiley.core.cart.WileyCommerceCartDao;
import com.wiley.core.enums.OrderType;
import com.wiley.core.model.AbandonCartEmailHistoryEntryModel;


/**
 * Default implementation of {@link WileyCommerceCartDao}
 */
public class WileyCommerceCartDaoImpl extends DefaultCommerceCartDao implements WileyCommerceCartDao
{

	protected static final String NOT_ANONYMOUS_CLAUSE = "AND {u." + UserModel.UID + "} != 'anonymous' ";

	protected static final String NOT_SAVED_CART_ABANDON_CART_CLAUSE = "AND {c." + CartModel.SAVETIME + "} IS NULL ";

	protected static final String ABANDON_CART_EMAIL_NOT_SENT = "AND NOT EXISTS ("
			+ " {{ SELECT * FROM {" + AbandonCartEmailHistoryEntryModel._TYPECODE + " as h} WHERE {h."
			+ AbandonCartEmailHistoryEntryModel.ORDER + "} = {c." + CartModel.PK + "}}}) ";

	protected static final String SELECTCLAUSE_ABANDON_CART = "SELECT {" + CartModel.PK + "} "
			+ "FROM {" + CartModel._TYPECODE + " AS c JOIN " + UserModel._TYPECODE
			+ " AS u ON {c." + CartModel.USER + "} = {u." + UserModel.PK + "}" + "} ";

	protected static final String FIND_ABANDON_CARTS_FOR_SITE = SELECTCLAUSE_ABANDON_CART
			+ "WHERE {c." + CartModel.MODIFIEDTIME + "} <= ?modifiedBefore AND {c." + CartModel.SITE + "} = ?site "
			+ ABANDON_CART_EMAIL_NOT_SENT + NOT_SAVED_CART_ABANDON_CART_CLAUSE
			+ NOT_ANONYMOUS_CLAUSE + ORDERBYCLAUSE;

	private static final String WHERE_CLAUSE_BY_USER_AND_SITE = "WHERE {" + CartModel.SITE
			+ "} = ?site AND {" + CartModel.USER + "} = ?user ";

	private static final String FIND_ALL_CARTS_FOR_SITE_AND_USER =
			SELECTCLAUSE + WHERE_CLAUSE_BY_USER_AND_SITE + ORDERBYCLAUSE;

	private static final String FIND_ALL_CARTS_FOR_SITE_AND_USER_AND_ORDERTYPE =
			SELECTCLAUSE + WHERE_CLAUSE_BY_USER_AND_SITE + "AND {"
					+ CartModel.ORDERTYPE + "} = ?type " + ORDERBYCLAUSE;

	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Override
	public List<CartModel> getAbandonCarts(final Date modifiedBefore, final BaseSiteModel site)
	{
		final Map<String, Object> params = new HashMap<String, Object>();
		params.put("site", site);
		params.put("modifiedBefore", modifiedBefore);
		return doSearch(FIND_ABANDON_CARTS_FOR_SITE, params, CartModel.class);
	}

	@Nonnull
	@Override
	public List<CartModel> findAllCartsForStoreAndUserAndType(@Nonnull final BaseSiteModel site,
			@Nonnull final UserModel user,
			@Nonnull final OrderType type)
	{
		final Map<String, Object> params = new HashMap<>();
		params.put("site", site);
		params.put("user", user);
		params.put("type", type);

		final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_ALL_CARTS_FOR_SITE_AND_USER_AND_ORDERTYPE);
		query.addQueryParameters(params);
		query.setDisableCaching(true);
		final SearchResult<CartModel> searchResult = flexibleSearchService.search(query);
		return searchResult.getResult();
	}

	@Override
	public List<CartModel> findAllCartsForUserAndSite(final BaseSiteModel site, final UserModel user)
	{
		final Map<String, Object> params = new HashMap<>();
		params.put("site", site);
		params.put("user", user);

		final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_ALL_CARTS_FOR_SITE_AND_USER);
		query.addQueryParameters(params);
		query.setDisableCaching(true);
		final SearchResult<CartModel> searchResult = flexibleSearchService.search(query);
		return searchResult.getResult();
	}
}
