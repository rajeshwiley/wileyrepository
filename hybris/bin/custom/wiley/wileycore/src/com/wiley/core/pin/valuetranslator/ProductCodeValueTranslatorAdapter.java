package com.wiley.core.pin.valuetranslator;

import de.hybris.platform.impex.jalo.header.UnresolvedValueException;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloInvalidParameterException;


/**
 * Spring context aware Translator Adapter for {@code PinModel} specific import export during hot folder process,
 * which uses isbn and optional term to lookup actual product and persist it's code as String into {@code PinModel}.
 */
public interface ProductCodeValueTranslatorAdapter
{
	/**
	 * Import interface of {@code AbstractValueProvider} and given catalog to look up for products
	 * @param valueExpr
	 * 		the isbn or isbn:term expression
	 * @param toItem
	 * 		the item the actual row represents in impex
	 * @param catalogId
	 * 		the catalogId for product lookup
	 * @param catalogVersion
	 * 		the catalogVersion for product lookup
	 * @return
	 * 		the code of product if found
	 * @throws UnresolvedValueException
	 * 		when valueExpr is in invalid format
	 */
	Object importValue(String valueExpr, Item toItem, String catalogId, String catalogVersion)
			throws UnresolvedValueException;

	/**
	 * Export interface of {@code AbstractValueProvider}
	 * @param objectToExport
	 * 		the object to be exported
	 * @return
	 * 		its string representation
	 * @throws JaloInvalidParameterException
	 */
	String exportValue(Object objectToExport) throws JaloInvalidParameterException;
}
