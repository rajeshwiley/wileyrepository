package com.wiley.core.event;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;
import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;
import de.hybris.platform.site.BaseSiteService;

import java.util.Set;

import org.springframework.beans.factory.annotation.Required;


/**
 * Created by Uladzimir_Barouski on 6/30/2016.
 */
public abstract class WileyAbstractSiteAwareApplicationListener extends AbstractEventListener
{

	private Set<String> siteUids;

	private BaseSiteService siteService;

	@Override
	protected void onEvent(final AbstractEvent abstractEvent)
	{
		final BaseSiteModel currentBaseSite = siteService.getCurrentBaseSite();
		if (currentBaseSite != null && siteUids.contains(currentBaseSite.getUid()))
		{
			siteSpecificOnEvent(abstractEvent);
		}
	}

	@Required
	public void setSiteUids(final Set<String> siteUids)
	{
		this.siteUids = siteUids;
	}

	@Required
	public void setSiteService(final BaseSiteService siteService)
	{
		this.siteService = siteService;
	}

	protected abstract void siteSpecificOnEvent(AbstractEvent abstractEvent);
}
