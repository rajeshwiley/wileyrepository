package com.wiley.core.b2b.unit.service;

import com.wiley.core.exceptions.ExternalSystemException;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.user.AddressModel;

public interface WileyB2BUnitExternalAddressService
{
	/**
	 * Saves provided address in external system
	 * @param unit unit to save address for
	 * @param addressModel address to save
	 * @return externally generated address id
	 * @throws ExternalSystemException if communication with external system was unsuccessful
	 */
	String addShippingAddress(B2BUnitModel unit, AddressModel addressModel) throws ExternalSystemException;
	
	/**
	 * Deletes provided address in external system
	 * @param unit unit to delete address for
	 * @param addressId address id to delete
	 * @return True if deleted successfully
	 * @throws ExternalSystemException if communication with external system was unsuccessful
	 */
	Boolean deleteShippingAddress(B2BUnitModel unit, String addressId) throws ExternalSystemException;

	/**
	 * Updates provided address in external system
	 * @param unit unit to save address for
	 * @param addressModel address to update. Address should have set 'externalId'
	 * @return 'true' if address was updated
	 * @throws ExternalSystemException if communication with external system was unsuccessful
	 */
	boolean updateShippingAddress(B2BUnitModel unit, AddressModel addressModel) throws ExternalSystemException;
}
