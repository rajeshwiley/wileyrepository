package com.wiley.core.mpgs.request;


import java.math.BigDecimal;


public class WileyAuthorizationRequest
{
	private String urlPath;
	private String paymentProvider;
	private String token;
	private String apiOperation;
	private BigDecimal totalAmount;
	private String currency;
	private String billingStreet1;
	private String billingStreet2;
	private String billingCity;
	private String stateProvince;
	private String billingPostalCode;
	private String billingCountry;
	private String transactionReference;

	public String getUrlPath()
	{
		return urlPath;
	}

	public void setUrlPath(final String urlPath)
	{
		this.urlPath = urlPath;
	}

	public String getApiOperation()
	{
		return apiOperation;
	}

	public void setApiOperation(final String apiOperation)
	{
		this.apiOperation = apiOperation;
	}

	public String getToken()
	{
		return token;
	}

	public void setToken(final String token)
	{
		this.token = token;
	}

	public String getPaymentProvider()
	{
		return paymentProvider;
	}

	public void setPaymentProvider(final String paymentProvider)
	{
		this.paymentProvider = paymentProvider;
	}

	public String getCurrency()
	{
		return currency;
	}

	public void setCurrency(final String currency)
	{
		this.currency = currency;
	}

	public String getBillingCity()
	{
		return billingCity;
	}

	public void setBillingCity(final String billingCity)
	{
		this.billingCity = billingCity;
	}

	public String getBillingPostalCode()
	{
		return billingPostalCode;
	}

	public void setBillingPostalCode(final String billingPostalCode)
	{
		this.billingPostalCode = billingPostalCode;
	}

	public String getBillingCountry()
	{
		return billingCountry;
	}

	public void setBillingCountry(final String billingCountry)
	{
		this.billingCountry = billingCountry;
	}

	public String getBillingStreet1()
	{
		return billingStreet1;
	}

	public void setBillingStreet1(final String billingStreet1)
	{
		this.billingStreet1 = billingStreet1;
	}

	public String getBillingStreet2()
	{
		return billingStreet2;
	}

	public void setBillingStreet2(final String billingStreet2)
	{
		this.billingStreet2 = billingStreet2;
	}

	public String getStateProvince()
	{
		return stateProvince;
	}

	public void setStateProvince(final String stateProvince)
	{
		this.stateProvince = stateProvince;
	}

	public BigDecimal getTotalAmount()
	{
		return totalAmount;
	}

	public void setTotalAmount(final BigDecimal totalAmount)
	{
		this.totalAmount = totalAmount;
	}

	public String getTransactionReference()
	{
		return transactionReference;
	}

	public void setTransactionReference(final String transactionReference)
	{
		this.transactionReference = transactionReference;
	}
}
