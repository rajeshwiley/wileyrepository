package com.wiley.core.strategies.robotsmetatag.impl;

import de.hybris.platform.cms2.model.pages.AbstractPageModel;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.google.common.base.Preconditions;
import com.wiley.core.enums.RobotsMetaTag;
import com.wiley.core.strategies.robotsmetatag.RobotsMetatagResolverService;


public class RobotsMetatagResolverServiceImpl implements RobotsMetatagResolverService
{
	private static final Logger LOG = LoggerFactory.getLogger(RobotsMetatagResolverServiceImpl.class);

	private Map<String, String> robotsValuesByPageTypeMapping;
	private Map<String, String> robotsValuesByPageUidMapping;

	@Override
	public String resolveByPageType(final AbstractPageModel page)
	{
		Preconditions.checkNotNull(page, "Page can't be null");
		LOG.debug("Resolving meta tag for page by type with uid: [{}], type: [{}]", page.getUid(), page.getItemtype());
		return robotsValuesByPageTypeMapping.get(page.getItemtype());
	}

	@Override
	public boolean needResolveByPageType(final AbstractPageModel page)
	{
		return RobotsMetaTag.UNDEFINED == page.getRobotsMetaTag()
				&& robotsValuesByPageTypeMapping.containsKey(page.getItemtype());
	}

	@Override
	public boolean needResolveByPageUid(final AbstractPageModel page)
	{
		return RobotsMetaTag.UNDEFINED == page.getRobotsMetaTag()
				&& robotsValuesByPageUidMapping.containsKey(page.getUid());
	}

	@Override
	public String resolveByPageUid(final String code)
	{
		Preconditions.checkNotNull(code, "Page code can't be null");
		LOG.debug("Resolving meta tag for page with uid: [{}]", code);
		return robotsValuesByPageUidMapping.get(code);
	}

	@Required
	public void setRobotsValuesByPageTypeMapping(final Map<String, String> robotsValuesByPageTypeMapping)
	{
		this.robotsValuesByPageTypeMapping = robotsValuesByPageTypeMapping;
	}

	@Required
	public void setRobotsValuesByPageUidMapping(
			final Map<String, String> robotsValuesByCodeTypeMapping)
	{
		this.robotsValuesByPageUidMapping = robotsValuesByCodeTypeMapping;
	}
}
