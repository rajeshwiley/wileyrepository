package com.wiley.core.order;

import de.hybris.platform.acceleratorservices.payment.data.OrderInfoData;
import de.hybris.platform.order.CartService;

/**
 * Created by Georgii_Gavrysh on 12/9/2016.
 */
public interface WileyCartService extends CartService {
    /**
     * populates orderInfoData from the current cart
     * @param orderInfoData
     */
    void populateFromCurrentCart(OrderInfoData orderInfoData);

    boolean isCartWithProductsFromCategory(String categoryCode);
}
