package com.wiley.core.wiley.process.strategies;

import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.store.BaseStoreModel;


/**
 * @author Dzmitryi_Halahayeu
 */
public interface WileyProcessContextStoreResolutionStrategy
{

	/**
	 * Resolves store to be used for the given business process.
	 *
	 * @param businessProcess
	 * 		the business process
	 * @return the base store
	 */
	BaseStoreModel getBaseStore(BusinessProcessModel businessProcess);

}
