package com.wiley.core.integration.customer.gateway;

import de.hybris.platform.core.model.user.CustomerModel;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;

import javax.annotation.Nonnull;
import javax.validation.constraints.NotNull;


/**
 * Gateway interface for Wiley B2C and B2B customer related requests.
 */
public interface WileyCustomerGateway
{
	String PASSWORD = "password";
	String USERID = "userId";

	@Nonnull
	Boolean registerCustomer(@Nonnull @Payload CustomerModel customerModel, @Nonnull @Header(PASSWORD) String password);

	@Nonnull
	void updateCustomer(@Nonnull @Payload CustomerModel customer);

	void forceResetPassword(@NotNull @Header(USERID) String uid, @NotNull @Payload String newPassword);
}
