package com.wiley.core.wileycom.stock.strategies;

import com.google.common.base.Preconditions;
import de.hybris.platform.commerceservices.stock.strategies.WarehouseSelectionStrategy;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.List;
import java.util.Optional;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import com.wiley.core.servicelayer.i18n.WileycomI18NService;
import com.wiley.core.wileycom.stock.dao.WileycomWarehouseDao;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


/**
 * Strategy that implements {@link WarehouseSelectionStrategy} for selecting warehouse according country and base store
 *
 * @author Dzmitryi_Halahayeu
 */
public class WileycomWarehouseSelectionStrategy implements WarehouseSelectionStrategy
{
	@Resource
	private WileycomWarehouseDao wileycomWarehouseDao;
	@Resource
	private WileycomI18NService wileycomI18NService;


	/**
	 * Retrieves only default warehouses that correspond current session country
	 * selected by a customer (and current base store as well)
	 *
	 * @param baseStore
	 * 		the base store
	 * @return the list of warehouse
	 */
	@Override
	public List<WarehouseModel> getWarehousesForBaseStore(@Nonnull final BaseStoreModel baseStore)
	{
		validateParameterNotNull(baseStore, "baseStore cannot be null");

		Optional<CountryModel> currentCountry = wileycomI18NService.getCurrentCountry();
		Preconditions.checkArgument(currentCountry.isPresent(), "currentCountry cannot be null");
		return wileycomWarehouseDao.getDefaultWarehousesForBaseStore(baseStore, currentCountry.get());
	}
}
