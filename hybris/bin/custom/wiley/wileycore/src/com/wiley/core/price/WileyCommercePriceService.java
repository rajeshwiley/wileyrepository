package com.wiley.core.price;

import de.hybris.platform.commerceservices.price.CommercePriceService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;


public interface WileyCommercePriceService extends CommercePriceService
{
	/**
	 * Find the lowest price among product variant prices and checks if variants has different prices. If there
	 * are no variants OR variants has not prices OR all variants has the same price "Starting At" price is considered
	 * to be absent and 'null' is returned.
	 *
	 * @param productModel
	 * 		product
	 * @param ignoreSetVariant
	 * 		if true Complete Set variant price will be ignored and only type/part variants price
	 * 		will be taken into account.
	 * @return 'starting at' price if present (see rules above)
	 */
	PriceInformation getStartingAtPrice(ProductModel productModel, boolean ignoreSetVariant);
}
