package com.wiley.core.partner.impl;


import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import com.wiley.core.model.KpmgOfficeModel;
import com.wiley.core.partner.WileyKPMGEnrollmentService;
import com.wiley.core.partner.WileyKPMGOfficeDao;


public class WileyKPMGEnrollmentServiceImpl implements WileyKPMGEnrollmentService
{
	private static final Logger LOG = Logger.getLogger(WileyKPMGEnrollmentServiceImpl.class);

	@Resource
	private WileyKPMGOfficeDao wileyKpmgOfficeDao;

	@Override
	public List<KpmgOfficeModel> getOffices(final String partnerId)
	{
		final List<KpmgOfficeModel> offices = wileyKpmgOfficeDao.findOffices(partnerId);
		if (CollectionUtils.isEmpty(offices))
		{
			LOG.error("Cannot find any offices for partner company with id: " + partnerId);
		}
		return offices;
	}

	@Override
	public KpmgOfficeModel getOfficeByCode(final String code)
	{
		return wileyKpmgOfficeDao.findOfficeByCode(code);
	}
}
