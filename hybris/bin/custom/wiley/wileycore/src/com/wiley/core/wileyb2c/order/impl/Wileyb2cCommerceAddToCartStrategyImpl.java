package com.wiley.core.wileyb2c.order.impl;

import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import de.hybris.platform.subscriptionservices.model.SubscriptionTermModel;

import java.util.List;
import java.util.Optional;

import com.wiley.core.enums.WileyProductSubtypeEnum;
import com.wiley.core.wileycom.order.impl.WileycomCommerceAddToCartStrategyImpl;

import static org.apache.commons.collections.CollectionUtils.isNotEmpty;


/**
 * Created by Uladzimir_Barouski on 7/4/2016.
 */
public class Wileyb2cCommerceAddToCartStrategyImpl extends WileycomCommerceAddToCartStrategyImpl
{

	@Override
	protected CartEntryModel createCartEntry(final CommerceCartParameter parameter,
			final UnitModel orderableUnit, final long actualAllowedQuantityChange, final int number, final boolean addToPresent)
	{
		final CartModel cart = parameter.getCart();
		final ProductModel product = parameter.getProduct();

		Optional<SubscriptionTermModel> originalTerm = Optional.empty();
		if (WileyProductSubtypeEnum.SUBSCRIPTION.equals(product.getSubtype()))
		{
			final AbstractOrderEntryModel existedEntry = getFirstCartEntryForProducts(cart, product);
			if (existedEntry != null)
			{
				final SubscriptionTermModel originalSubscription = existedEntry.getSubscriptionTerm();

					/*If existed entry has not the same subscription term than remove it from cart*/
				if (originalSubscription != null && !originalSubscription.equals(parameter.getSubscriptionTerm()))
				{
					getModelService().remove(existedEntry);
					getModelService().refresh(cart);
					originalTerm = Optional.of(originalSubscription);
				}
			}
		}

		final CartEntryModel entry = super.createCartEntry(parameter, orderableUnit,
				actualAllowedQuantityChange, number, addToPresent);

		if (entry != null)
		{
			entry.setAdditionalInfo(parameter.getAdditionalInfo());
			entry.setSubscriptionTerm(parameter.getSubscriptionTerm());
			entry.setOriginalSubscriptionId(originalTerm.isPresent() ? originalTerm.get().getId() : null);
		}

		return entry;
	}

	private CartEntryModel getFirstCartEntryForProducts(final CartModel cart, final ProductModel product)
	{
		final List<CartEntryModel> entriesForProduct = getCartService().getEntriesForProduct(cart, product);
		return isNotEmpty(entriesForProduct) ? entriesForProduct.get(0) : null;
	}

	/**
	 * overridden to allow to buy products which are out of stock
	 */
	@Override
	protected long getAllowedCartAdjustmentForProduct(final CartModel cartModel, final ProductModel productModel,
			final long quantityToAdd, final PointOfServiceModel pointOfServiceModel)
	{
		return quantityToAdd;
	}
}
