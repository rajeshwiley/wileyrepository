package com.wiley.core.order;

import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;

import javax.annotation.Nonnull;


/**
 * Strategy for setting payment address before WPG call
 *
 * @author Dzmitryi_Halahayeu
 */
public interface WileySetPaymentAddressStrategy
{
	void setPaymentAddress(@Nonnull CommerceCheckoutParameter commerceCheckoutParameter);

}
