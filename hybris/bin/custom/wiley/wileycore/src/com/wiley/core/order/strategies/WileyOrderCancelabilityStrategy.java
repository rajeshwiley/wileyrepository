package com.wiley.core.order.strategies;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;

public interface WileyOrderCancelabilityStrategy
{

	boolean isCancelable(OrderModel order);
	
	boolean hasCancelableEntries(OrderModel order);
	
	boolean isCancelableEntryState(OrderStatus entryState);
	
	boolean isCancelableOrderEntry(OrderModel order, OrderStatus entryState);

}
