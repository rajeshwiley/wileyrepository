package com.wiley.core.validation.service;

import de.hybris.platform.catalog.model.ProductFeatureModel;
import de.hybris.platform.core.model.media.MediaContainerModel;

import java.util.Collection;
import java.util.List;


/**
 * This service contains a validation logic that is invoked from DynamicConstraints
 */
public interface WileyDynamicConstraintValidationService
{
	/**
	 * Validation check: either null OR the first item is a media container with images and NOT brightcove media container
	 */
	boolean validateFirstGalleryContainerIsNotVideo(Collection<MediaContainerModel> gallery);

	/**
	 * Validation check: either null OR each media container with images contains both the normal and thumbnail formats
	 * (zoom format is optional)
	 */
	boolean validateGalleryImagesContainerContainsRequiredFormats(Collection<MediaContainerModel> gallery);

	/**
	 * Validation check: either null OR a media container with both the normal and thumbnail formats (zoom format is optional)
	 */
	boolean validateExternalImageContainerContainsRequiredFormats(MediaContainerModel externalImage);

	/**
	 * Validation check: either null OR each brightcove media container (if any) contains the thumbnail format
	 */
	boolean validateGalleryBrightcoveContainerContainsRequiredFormats(Collection<MediaContainerModel> gallery);

	/**
	 * Validation check: either null OR a media container with both the desktop and mobile formats
	 */
	boolean validateBackgroundImageContainerContainsRequiredFormats(MediaContainerModel backgroundImage);

	/**
	 * Validation check for classification attribute "isbn13" rule: either null OR (size = 13 AND no dashes or hyphens)
	 */
	boolean validateIsbn13HasRequiredFormat(List<ProductFeatureModel> features);

	/**
	 * Validation check for classification attribute "onlineIssn" rule: either null OR (size = 8 AND no dashes or hyphens)
	 */
	boolean validateOnlineIssnHasRequiredFormat(List<ProductFeatureModel> features);

	/**
	 * Validation check for classification attribute "printIssn" rule: either null OR (size = 8 AND no dashes or hyphens)
	 */
	boolean validatePrintIssnHasRequiredFormat(List<ProductFeatureModel> features);
}
