package com.wiley.core.wileyb2c.cms2.servicelayer.services.evaluator.impl;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.data.RestrictionData;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.cms2.servicelayer.services.evaluator.CMSRestrictionEvaluator;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.wiley.core.model.Wileyb2cCMSSiteRestrictionModel;


/**
 * @author Dzmitryi_Halahayeu
 */
public class Wileyb2cCMSSiteRestrictionEvaluator implements CMSRestrictionEvaluator<Wileyb2cCMSSiteRestrictionModel>
{
	private static final Logger LOG = Logger.getLogger(Wileyb2cCMSSiteRestrictionEvaluator.class);
	@Resource
	private CMSSiteService cmsSiteService;

	@Override
	public boolean evaluate(final Wileyb2cCMSSiteRestrictionModel restriction, final RestrictionData restrictionData)
	{
		CMSSiteModel currentSite = cmsSiteService.getCurrentSite();
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Current site: " + restriction.getUid());
			LOG.debug("Restricted Users: " + StringUtils.join(restriction.getSites(), "; "));
		}

		return restriction.getSites().contains(currentSite);
	}
}

