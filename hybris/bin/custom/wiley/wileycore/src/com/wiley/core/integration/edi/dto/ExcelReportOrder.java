package com.wiley.core.integration.edi.dto;

import java.util.Date;


public class ExcelReportOrder
{


	private String productId;
	private String purchaseOrderNumber;
	private String orderType;
	private String creditCardType;
	private String authCode;
	private String creditCardToken;
	private String creditCardOwner;
	private String orderCurrency;
	private Double orderAmount;
	private Double tax;
	private Double shipping;
	private Double grandTotal;
	private Double totalDiscounts;
	private Date creationTime;
	private Date transactionDate;

	public Date getCreationTime()
	{
		return creationTime;
	}

	public void setCreationTime(final Date creationTime)
	{
		this.creationTime = creationTime;
	}

	public String getOrderType()
	{
		return orderType;
	}

	public void setOrderType(final String orderType)
	{
		this.orderType = orderType;
	}

	public String getOrderCurrency()
	{
		return orderCurrency;
	}

	public void setOrderCurrency(final String orderCurrency)
	{
		this.orderCurrency = orderCurrency;
	}

	public String getCreditCardOwner()
	{
		return creditCardOwner;
	}

	public void setCreditCardOwner(final String creditCardOwner)
	{
		this.creditCardOwner = creditCardOwner;
	}

	public Double getTotalDiscounts()
	{
		return totalDiscounts;
	}

	public void setTotalDiscounts(final Double totalDiscounts)
	{
		this.totalDiscounts = totalDiscounts;
	}

	public String getCreditCardType()
	{
		return creditCardType;
	}

	public void setCreditCardType(final String creditCardType)
	{
		this.creditCardType = creditCardType;
	}

	public Double getOrderAmount()
	{
		return orderAmount;
	}

	public void setOrderAmount(final Double orderAmount)
	{
		this.orderAmount = orderAmount;
	}

	public Double getTax()
	{
		return tax;
	}

	public void setTax(final Double tax)
	{
		this.tax = tax;
	}

	public Double getShipping()
	{
		return shipping;
	}

	public void setShipping(final Double shipping)
	{
		this.shipping = shipping;
	}

	public Double getGrandTotal()
	{
		return grandTotal;
	}

	public void setGrandTotal(final Double grandTotal)
	{
		this.grandTotal = grandTotal;
	}

	public String getPurchaseOrderNumber()
	{
		return purchaseOrderNumber;
	}

	public void setPurchaseOrderNumber(final String purchaseOrderNumber)
	{
		this.purchaseOrderNumber = purchaseOrderNumber;
	}

	public String getAuthCode()
	{
		return authCode;
	}

	public void setAuthCode(final String authCode)
	{
		this.authCode = authCode;
	}

	public String getCreditCardToken()
	{
		return creditCardToken;
	}

	public void setCreditCardToken(final String creditCardToken)
	{
		this.creditCardToken = creditCardToken;
	}

	public String getProductId()
	{
		return productId;
	}

	public void setProductId(final String productId)
	{
		this.productId = productId;
	}

	public Date getTransactionDate()
	{
		return transactionDate;
	}

	public void setTransactionDate(final Date transactionDate)
	{
		this.transactionDate = transactionDate;
	}
}
