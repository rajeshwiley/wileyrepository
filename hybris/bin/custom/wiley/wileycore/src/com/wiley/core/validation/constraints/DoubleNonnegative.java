package com.wiley.core.validation.constraints;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.wiley.core.validation.constraintvalidators.DoubleNonnegativeValidator;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;


/**
 * Validation annotation for check of non-negative Double value
 */
@Target({ FIELD, METHOD, PARAMETER, ANNOTATION_TYPE })
@Retention(RUNTIME)
@Constraint(validatedBy = DoubleNonnegativeValidator.class)
@Documented
public @interface DoubleNonnegative
{
	String message() default "{com.wiley.core.validation.constraints.DoubleNonnegative.message}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}
