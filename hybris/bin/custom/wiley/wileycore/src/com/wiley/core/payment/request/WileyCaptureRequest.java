package com.wiley.core.payment.request;

import de.hybris.platform.payment.commands.request.CaptureRequest;

import java.math.BigDecimal;
import java.util.Currency;

public class WileyCaptureRequest extends CaptureRequest
{

	private final String authCode;
	private final String site;

	public WileyCaptureRequest(final String merchantTransactionCode, final String requestId, final String requestToken,
							   final Currency currency, final BigDecimal totalAmount, final String paymentProvider,
							   final String authCode, final String site)
	{
		super(merchantTransactionCode, requestId, requestToken, currency, totalAmount, paymentProvider);
		this.authCode = authCode;
		this.site = site;
	}

	public String getAuthCode()
	{
		return authCode;
	}

	public String getSite()
	{
		return site;
	}
}
