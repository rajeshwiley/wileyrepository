package com.wiley.core.payment.strategies.impl;

import de.hybris.platform.acceleratorservices.payment.data.WPGHttpValidateResultData;
import de.hybris.platform.site.BaseSiteService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.payment.strategies.SecurityHashGeneratorStrategy;
import com.wiley.core.payment.strategies.WPGResponseSecurityValidationStrategy;


public abstract class WPGAuthResponseSecurityValidationStrategyImpl implements WPGResponseSecurityValidationStrategy
{
	private static final Logger LOG = Logger.getLogger(WPGAuthResponseSecurityValidationStrategyImpl.class);
	@Autowired
	private BaseSiteService baseSiteService;

	@Override
	public boolean validate(final WPGHttpValidateResultData resultData)
	{
		StringBuilder md5Base = new StringBuilder();
		md5Base.append(resultData.getOperation());
		md5Base.append(resultData.getReturnCode());
		md5Base.append(resultData.getReturnMessage());
		md5Base.append(resultData.getVendorID());
		md5Base.append(resultData.getTransID());
		md5Base.append(resultData.getValue());
		md5Base.append(resultData.getCurrency());
		md5Base.append(resultData.getMerchantResponse());
		md5Base.append(resultData.getMerchantID());
		md5Base.append(resultData.getCSCResult());
		md5Base.append(resultData.getAVSAddrResult());
		md5Base.append(resultData.getAVSPostResult());
		md5Base.append(resultData.getToken());
		md5Base.append(resultData.getAuthCode());
		md5Base.append(resultData.getAcquirerID());
		md5Base.append(resultData.getAcquirerName());
		md5Base.append(resultData.getBankID());
		md5Base.append(resultData.getBankName());
		md5Base.append(resultData.getMaskedCardNumber());
		md5Base.append(resultData.getCardExpiry());
		md5Base.append(resultData.getTimestamp());

		String calculatedSecurity = getSecurityHashGeneratorStrategy()
				.generateSecurityHash(baseSiteService.getCurrentBaseSite().getUid(), md5Base.toString());

		if (resultData.getSecurity() == null || !resultData.getSecurity().equals(calculatedSecurity))
		{
			String errorMessage = String.format("Calculated Security: %s; Security received from WPG Http Validate Result: %s",
					calculatedSecurity, resultData.getSecurity());
			LOG.error(errorMessage);
			return false;
		}
		return true;
	}

	public abstract SecurityHashGeneratorStrategy getSecurityHashGeneratorStrategy();
}
