package com.wiley.core.wileyb2c.product;

import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;

import javax.annotation.Nonnull;

import com.wiley.core.enums.WileyProductSubtypeEnum;


/**
 * B2C service for course product
 */
public interface Wileyb2cCourseProductService
{
	/**
	 * Get EPROF product, which has {@link ProductReferenceTypeEnum#WILEY_PLUS_COURSE} product reference
	 * to <code>courseProduct</code>.
	 * <br/><br/>
	 * For details see <a href="https://confluence.wiley.ru/display/ECSC/P2+-+SAD.+05+-+Information+Architecture">
	 * P2 - SAD. 05 - Information Architecture</a> article on Wiley confluence.
	 *
	 * @param courseProduct
	 * 		course product
	 * @return EPROF product
	 * @throws NullPointerException
	 * 		if <code>courseProduct</code> is null
	 * @throws IllegalArgumentException
	 * 		if course product subtype is not {@link WileyProductSubtypeEnum#COURSE}
	 * @throws IllegalStateException
	 * 		if found 0 or more then 1 references to course product with type <code>WILEY_PLUS_COURSE</code>
	 */
	@Nonnull
	ProductModel getReferencedEprofProduct(@Nonnull ProductModel courseProduct);


	/**
	 * Get bundle products for <code>eprofProduct</code> product.
	 * Method search bundle product that:
	 * <ul>
	 * <li>
	 * Has {@link ProductReferenceTypeEnum#WILEY_PLUS_PRODUCT_SET WILEY_PLUS_PRODUCT_SET}
	 * reference from EPROF product (EPROF is the source)
	 * </li>
	 * <li>
	 * Has {@link ProductReferenceTypeEnum#PRODUCT_SET_COMPONENT PRODUCT_SET_COMPONENT}
	 * reference to course product (course is the target)
	 * </li>
	 * </ul>
	 * <br/><br/>
	 * For details see <a href="https://confluence.wiley.ru/display/ECSC/P2+-+SAD.+05+-+Information+Architecture">
	 * P2 - SAD. 05 - Information Architecture</a> article on Wiley confluence.
	 *
	 * @param eprofProduct
	 * 		EPROF product
	 * @param courseProduct
	 * 		course product
	 * @return list of bundle products
	 * @throws NullPointerException
	 * 		if either <code>courseProduct</code> or <code>eprofProduct</code> is null
	 * @throws IllegalArgumentException
	 * 		if one of conditions is met:
	 * 		<ul><li><code>eprofProduct</code> subtype is not {@link WileyProductSubtypeEnum#COURSE}</li>
	 * 		<li><code>eprofProduct</code> is not purchasable ({@link ProductModel#getPurchasable()} is not <code>true</code>)</li>
	 * 		<li><code>courseProduct</code> subtype is not {@link WileyProductSubtypeEnum#COURSE}</li></ul>
	 */
	@Nonnull
	List<ProductModel> getReferencedBundleProducts(@Nonnull ProductModel eprofProduct,
			@Nonnull ProductModel courseProduct);


}
