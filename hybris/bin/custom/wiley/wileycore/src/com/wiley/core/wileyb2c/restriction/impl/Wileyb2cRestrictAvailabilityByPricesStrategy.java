package com.wiley.core.wileyb2c.restriction.impl;

import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.subscriptionservices.model.SubscriptionTermModel;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.model.WileyProductModel;
import com.wiley.core.wiley.restriction.WileyRestrictProductStrategy;
import com.wiley.core.wiley.restriction.dto.WileyRestrictionCheckResultDto;
import com.wiley.core.wiley.restriction.impl.WileyProductRestrictionServiceImpl;
import com.wiley.core.wileyb2c.product.Wileyb2cCommercePriceService;
import com.wiley.core.wileyb2c.subscription.services.Wileyb2cFreeTrialSubscriptionTermCheckingStrategy;
import com.wiley.core.wileyb2c.subscription.services.Wileyb2cSubscriptionService;


/**
 * @author Dzmitryi_Halahayeu
 */
public class Wileyb2cRestrictAvailabilityByPricesStrategy implements WileyRestrictProductStrategy
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyProductRestrictionServiceImpl.class);
	private Wileyb2cCommercePriceService commercePriceService;
	@Autowired
	private Wileyb2cSubscriptionService wileyb2cSubscriptionService;
	@Resource
	private Wileyb2cFreeTrialSubscriptionTermCheckingStrategy wileyb2cFreeTrialSubscriptionCheckingStrategy;

	@Override
	public boolean isRestricted(final ProductModel product)
	{
		final PriceInformation info;
		// logic for retrieving product prices is similar to ProductPricePopulator
		if (CollectionUtils.isEmpty(product.getVariants()))
		{
			LOG.debug("Getting Web price for product [{}].", product.getCode());
			info = commercePriceService.getWebPriceForProduct(product);
		}
		else
		{
			LOG.debug("Getting From price for product [{}].", product.getCode());
			info = commercePriceService.getFromPriceForProduct(product);
		}

		return info == null;
	}

	@Override
	public boolean isRestricted(final CommerceCartParameter parameter)
	{
		final ProductModel product = parameter.getProduct();
		final SubscriptionTermModel subscriptionTerm = parameter.getSubscriptionTerm();

		if (isSubscriptionProduct(product, subscriptionTerm))
		{
			final WileyProductModel wileyProductModel = (WileyProductModel) product;
			final boolean isValid = wileyb2cFreeTrialSubscriptionCheckingStrategy.isFreeTrial(subscriptionTerm)
					|| commercePriceService.validateProductSubscriptionTermPrice(wileyProductModel, subscriptionTerm);
			return !isValid;
		}
		else
		{
			return isRestricted(product);
		}
	}

	@Override
	public WileyRestrictionCheckResultDto createErrorResult(final CommerceCartParameter parameter)
	{
		final String result;
		final ProductModel product = parameter.getProduct();
		final SubscriptionTermModel subscriptionTerm = parameter.getSubscriptionTerm();

		if (isSubscriptionProduct(product, subscriptionTerm))
		{
			result = "checkout.multi.subscription.not.available";
		}
		else
		{
			result = WileyCoreConstants.PRODUCT_IS_NOT_PURCHASABLE_FOR_COUNTRY;
		}
		return WileyRestrictionCheckResultDto.failureResult(result,
				String.format("Product [%s] has no configured price.", parameter.getProduct().getCode()),
				this.getErrorMessageParameters(parameter));
	}

	private boolean isSubscriptionProduct(final ProductModel product, final SubscriptionTermModel subscriptionTerm)
	{
		final boolean isSubscriptionProduct = product instanceof WileyProductModel
				&& wileyb2cSubscriptionService.isProductSubscription((WileyProductModel) product);
		return isSubscriptionProduct && subscriptionTerm != null;
	}

	@Required
	public void setCommercePriceService(final Wileyb2cCommercePriceService commercePriceService)
	{
		this.commercePriceService = commercePriceService;
	}

	private Object[] getErrorMessageParameters(final CommerceCartParameter parameter)
	{
		final ProductModel product = parameter.getProduct();
		final SubscriptionTermModel subscriptionTerm = parameter.getSubscriptionTerm();

		if (isSubscriptionProduct(product, subscriptionTerm))
		{
			return new Object[] { subscriptionTerm.getName(), product.getCode() };
		}
		return null;
	}
}
