package com.wiley.core.integration.alm.user.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

import org.hibernate.validator.constraints.NotBlank;


@JsonInclude(NON_NULL)
public class UserDto
{
	@NotBlank
	@JsonProperty(value = "userId")
	private String userId;

	@JsonProperty(value = "lastName")
	private String lastName;

	@JsonProperty(value = "firstName")
	private String firstName;

	@JsonProperty(value = "email")
	private String email;

	public String getUserId()
	{
		return userId;
	}

	public void setUserId(final String userId)
	{
		this.userId = userId;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(final String lastName)
	{
		this.lastName = lastName;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(final String firstName)
	{
		this.firstName = firstName;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(final String email)
	{
		this.email = email;
	}
}
