package com.wiley.core.billingfrequency.impl;

import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.subscriptionservices.model.BillingFrequencyModel;

import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import com.wiley.core.billingfrequency.WileyBillingFrequencyService;
import com.wiley.core.billingfrequency.dao.WileyBillingFrequencyDao;


/**
 * Default implementation of {@link WileyBillingFrequencyService}.
 */
public class WileyBillingFrequencyServiceImpl implements WileyBillingFrequencyService
{

	@Resource
	private WileyBillingFrequencyDao wileyBillingFrequencyDao;

	@Override
	@Nonnull
	public BillingFrequencyModel getBillingFrequencyByCode(@Nonnull final String code)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("code", code);

		final List<BillingFrequencyModel> billingFrequencies = wileyBillingFrequencyDao.findBillingFrequenciesByCode(code);

		ServicesUtil.validateIfSingleResult(billingFrequencies,
				String.format("Could not found billing frequency for code [%s]", code),
				String.format("There are more then one billing frequency for code [%s]", code));

		return billingFrequencies.get(0);
	}
}
