package com.wiley.core.payment.response.validators;

import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.RegionData;

import java.util.ArrayList;
import java.util.regex.Matcher;

import org.apache.commons.lang.StringUtils;

import com.google.common.base.Preconditions;
import com.paypal.hybris.data.GetExpressCheckoutDetailsResultData;
import com.paypal.hybris.data.ResultErrorData;
import com.paypal.hybris.facade.validators.PayPalExpressCheckoutResponseValidator;
import com.wiley.core.constants.WileyCoreConstants;


public class PayPalResponseBillingUnicodeCharactersValidator implements PayPalExpressCheckoutResponseValidator
{
	private static final String PAYPAL_CHECKOUT_UNICODE_ERROR_SHORT_MESSAGE = "Unsupported address characters from PayPal";
	private static final String PAYPAL_CHECKOUT_UNICODE_ERROR_LONG_MESSAGE =
			"paypal.checkout.billingInformation.unicode.error.longMessage";

	@Override
	public boolean validateResponse(final GetExpressCheckoutDetailsResultData responseData)
	{
		AddressData billingAddress = responseData.getBillingAddress();
		Preconditions.checkNotNull(billingAddress, "Billing address from PayPal can't be null");

		boolean fieldsAreValid = validateStandardFields(billingAddress) && validateCountryAndRegion(billingAddress);

		if (!fieldsAreValid)
		{
			if (responseData.getErrors() == null)
			{
				responseData.setErrors(new ArrayList<>());
			}

			final ResultErrorData error = new ResultErrorData();

			error.setShortMessage(PAYPAL_CHECKOUT_UNICODE_ERROR_SHORT_MESSAGE);
			error.setLongMessage(PAYPAL_CHECKOUT_UNICODE_ERROR_LONG_MESSAGE);
			error.setUseLocalizedLongMessage(true);
			error.setErrorCode(PayPalResponseBillingUnicodeCharactersValidator.class.getName());

			responseData.getErrors().add(error);
		}

		return fieldsAreValid;
	}

	private boolean validateStandardFields(final AddressData billingAddress)
	{
		return isValidUnicodeCharacters(billingAddress.getId())
				&& isValidUnicodeCharacters(billingAddress.getAddressSummary())
				&& isValidUnicodeCharacters(billingAddress.getFirstName())
				&& isValidUnicodeCharacters(billingAddress.getLastName())
				&& isValidUnicodeCharacters(billingAddress.getLine1())
				&& isValidUnicodeCharacters(billingAddress.getLine2())
				&& isValidUnicodeCharacters(billingAddress.getTown())
				&& isValidUnicodeCharacters(billingAddress.getPostalCode())
				&& isValidUnicodeCharacters(billingAddress.getPhone())
				&& isValidUnicodeCharacters(billingAddress.getEmail());
	}

	protected boolean validateCountryAndRegion(final AddressData billingAddress)
	{
		final RegionData region = billingAddress.getRegion();
		final CountryData country = billingAddress.getCountry();

		boolean fieldsAreValid = true;

		if (country != null)
		{
			fieldsAreValid = isValidUnicodeCharacters(country.getIsocode()) && isValidUnicodeCharacters(country.getName());
		}

		if (region != null && fieldsAreValid)
		{
			return isValidUnicodeCharacters(region.getIsocode()) && isValidUnicodeCharacters(region.getName());
		}

		return fieldsAreValid;
	}

	private boolean isValidUnicodeCharacters(final String addressField)
	{
		if (StringUtils.isNotBlank(addressField))
		{
			final Matcher matcher = WileyCoreConstants.CHARACTERS_REGEX.matcher(addressField);
			return matcher.matches();
		}

		return true;
	}
}
