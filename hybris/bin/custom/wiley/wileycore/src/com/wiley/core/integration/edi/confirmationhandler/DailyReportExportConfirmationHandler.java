package com.wiley.core.integration.edi.confirmationhandler;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.messaging.Message;

import com.wiley.core.integration.edi.ExportConfirmationHandler;
import com.wiley.core.integration.edi.Headers;
import com.wiley.core.integration.edi.strategy.EdiOrderProvisionStrategy;
import com.wiley.fulfilmentprocess.model.WileyExportProcessModel;


public class DailyReportExportConfirmationHandler implements ExportConfirmationHandler
{
	private static final Logger LOG = Logger.getLogger(DailyReportExportConfirmationHandler.class);

	@Resource(name = "ediOrderProvisionStrategy")
	private EdiOrderProvisionStrategy orderProvisionStrategy;

	@Override
	public Message confirm(final Message message)
	{
		LOG.info("Marking orders as daily report sent");
		List<WileyExportProcessModel> exportedProcesses =
				(List<WileyExportProcessModel>) message.getHeaders().get(Headers.CONVERTED_PROCESSES);
		orderProvisionStrategy.markDailyReportSent(exportedProcesses);
		return message;
	}
}
