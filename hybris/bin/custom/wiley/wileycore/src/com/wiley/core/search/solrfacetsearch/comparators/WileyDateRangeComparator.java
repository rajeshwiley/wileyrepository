package com.wiley.core.search.solrfacetsearch.comparators;

import de.hybris.platform.solrfacetsearch.search.FacetValue;

import java.util.Comparator;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.MutableDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class WileyDateRangeComparator implements Comparator<FacetValue>
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyDateRangeComparator.class);
	public static final String PERIODS_SEPARATOR = " TO ";
	public static final int NOT_RANGED_DEFAULT_VALUE = -1;
	public static final String DAY_PATTERN = "DAY";
	public static final String MONTH_PATTERN = "MONTH";
	public static final int DAYS_IN_MONTH = 30;
	public static final String YEAR_PATTERN = "YEAR";
	public static final int DAYS_IN_YEAR = 365;

	@Override
	public int compare(final FacetValue o1, final FacetValue o2)
	{
		LOG.debug("Comparing {} and {}", o1.getName(), o2.getName());
		final int result = getValue(o1).compareTo(getValue(o2));
		LOG.debug("The result of comparation between [{}] and [{}] is: [{}]", o1.getName(), o2.getName(), result);
		return result;
	}

	private Integer getValue(final FacetValue facetValue)
	{
		final String value = facetValue.getName();
		final String[] periods = value.split(PERIODS_SEPARATOR);

		if (periods.length != 2 || StringUtils.isBlank(periods[0]))
		{
			LOG.error("Incorrect format of the facet query for date range facet. "
					+ "No range specified. The facet value is: {}", value);
			return NOT_RANGED_DEFAULT_VALUE;
		}

		String facetVal = cleanupValue(periods[0]);

		if ("*".equals(facetVal))
		{
			MutableDateTime epoch = new MutableDateTime();
			epoch.setDate(0); //Set to Epoch time
			DateTime now = DateTime.now();
			return Days.daysBetween(epoch, now).getDays();
		}

		if (facetVal.contains(DAY_PATTERN))
		{
			return extractUnitsFromDateRange(facetVal, DAY_PATTERN);
		}
		else if (facetVal.contains(MONTH_PATTERN))
		{
			return extractUnitsFromDateRange(facetVal, MONTH_PATTERN) * DAYS_IN_MONTH;
		}
		else if (facetVal.contains(YEAR_PATTERN))
		{
			return extractUnitsFromDateRange(facetVal, YEAR_PATTERN) * DAYS_IN_YEAR;
		}

		return NOT_RANGED_DEFAULT_VALUE;
	}

	private static String cleanupValue(final String period)
	{
		return period
				.trim()
				.replace("[", "")
				.replaceAll(" ", "")
				.replaceAll("NOW-", "");
	}

	private static Integer extractUnitsFromDateRange(final String facetVal, final String pattern)
	{
		try
		{
			return Integer.valueOf(facetVal.replace(pattern, ""));
		}
		catch (IllegalArgumentException e)
		{
			LOG.error("Incorrect format of the facet query for date range facet: {}", facetVal);
			return NOT_RANGED_DEFAULT_VALUE;
		}
	}
}
