package com.wiley.core.mpgs.services.impl;


import de.hybris.platform.servicelayer.util.ServicesUtil;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Value;

import com.wiley.core.mpgs.services.WileyUrlGenerationService;


public class WileyMPGSUrlGenerationServiceImpl implements WileyUrlGenerationService
{
	private static final String TOKENIZATION_URL_PART = "/token/";
	private static final String ORDER_URL_PART = "/order/";
	private static final String TRANSACTION_URL_PART = "/transaction/";
	private static final String RETRIEVE_SESSION_URL = "/session/";
	private static final String MERCHANT_ID_PART = "/merchant/";

	@Value("${payment.mpgs.endpoint}")
	private String paymentMpgsEndpoint;

	@Override
	public String getRetrieveSessionUrl(@Nonnull final String tnsMerchantId, @Nonnull final String sessionId)
	{
		ServicesUtil.validateParameterNotNull(sessionId, "[sessionId] can't be null.");
		ServicesUtil.validateParameterNotNull(tnsMerchantId, "[tnsMerchantId] can't be null.");

		final StringBuilder urlBuilder = new StringBuilder(getPaymentMpgsEndpoint());
		urlBuilder.append(MERCHANT_ID_PART).append(tnsMerchantId).append(RETRIEVE_SESSION_URL).append(sessionId);
		return urlBuilder.toString();
	}

	@Override
	public String getVerifyUrl(@Nonnull final String orderGuid, @Nonnull final String transactionId, final String tnsMerchantId)
	{
		return getTransactionUrl(tnsMerchantId, orderGuid, transactionId);
	}

	@Override
	public String getTokenizationUrl(@Nonnull final String tnsMerchantId)
	{
		ServicesUtil.validateParameterNotNull(tnsMerchantId, "[tnsMerchantId] can't be null.");

		final StringBuilder urlBuilder = new StringBuilder(getPaymentMpgsEndpoint());
		urlBuilder.append(MERCHANT_ID_PART).append(tnsMerchantId).append(TOKENIZATION_URL_PART);
		return urlBuilder.toString();
	}

	@Override
	public String getAuthorizationURL(@Nonnull final String tnsMerchantId,
			@Nonnull final String orderGuid, @Nonnull final String transactionId)
	{
		ServicesUtil.validateParameterNotNull(tnsMerchantId, "[tnsMerchantId] can't be null.");
		return getTransactionUrl(tnsMerchantId, orderGuid, transactionId);
	}

	@Override
	public String getCaptureUrl(@Nonnull final String tnsMerchantId,
			@Nonnull final String orderGuid, @Nonnull final String transactionId)
	{
		ServicesUtil.validateParameterNotNull(tnsMerchantId, "[tnsMerchantId] can't be null.");
		return getTransactionUrl(tnsMerchantId, orderGuid, transactionId);
	}

	@Override
	public String getRefundFollowOnUrl(@Nonnull final String tnsMerchantId,
			@Nonnull final String orderGuid, @Nonnull final String transactionId)
	{
		ServicesUtil.validateParameterNotNull(tnsMerchantId, "[tnsMerchantId] can't be null.");
		return getTransactionUrl(tnsMerchantId, orderGuid, transactionId);
	}

	private String getTransactionUrl(@Nonnull final String tnsMerchantId,
			@Nonnull final String orderGuid, @Nonnull final String transactionId)
	{
		ServicesUtil.validateParameterNotNull(orderGuid, "[orderGuid] can't be null.");
		ServicesUtil.validateParameterNotNull(transactionId, "[transactionId] can't be null.");
		ServicesUtil.validateParameterNotNull(tnsMerchantId, "[tnsMerchantId] can't be null.");


		final StringBuilder urlBuilder = new StringBuilder(getPaymentMpgsEndpoint());
		urlBuilder.append(MERCHANT_ID_PART).append(tnsMerchantId).append(ORDER_URL_PART)
				.append(orderGuid).append(TRANSACTION_URL_PART).append(transactionId);
		return urlBuilder.toString();
	}

	protected String getPaymentMpgsEndpoint()
	{
		return paymentMpgsEndpoint;
	}
}

