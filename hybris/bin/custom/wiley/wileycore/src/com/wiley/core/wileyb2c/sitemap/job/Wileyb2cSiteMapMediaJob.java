package com.wiley.core.wileyb2c.sitemap.job;

import de.hybris.platform.acceleratorservices.cronjob.SiteMapMediaJob;
import de.hybris.platform.acceleratorservices.enums.SiteMapPageEnum;
import de.hybris.platform.acceleratorservices.model.SiteMapConfigModel;
import de.hybris.platform.acceleratorservices.model.SiteMapMediaCronJobModel;
import de.hybris.platform.acceleratorservices.model.SiteMapPageModel;
import de.hybris.platform.acceleratorservices.sitemap.generator.SiteMapGenerator;
import de.hybris.platform.catalog.model.CatalogUnawareMediaModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.core.model.media.MediaFolderModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.log4j.Logger;


/**
 * Created by Maksim_Kozich on 31.08.17.
 */
public class Wileyb2cSiteMapMediaJob extends SiteMapMediaJob
{
	protected static final String SITE_MAP_MIME_TYPE = "text/plain";
	private static final Logger LOG = Logger.getLogger(Wileyb2cSiteMapMediaJob.class);

	protected static final String SITE_MAP_MEDIA_JOB_OUTPUT_FOLDER_PROPERTY_NAME = "site.map.media.job.output.folder";
	protected static final String PREPARE_MODEL_LIST_FOR_SITE_MAP_PAGE_FAILED_MESSAGE =
			"prepare model list for SiteMap page %s failed";
	protected static final String PREPARE_MODEL_LIST_FOR_ONE_OF_SITE_MAP_PAGES_FAILED_MESSAGE =
			"prepare model list for one of SiteMap pages failed";

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	/**
	 * Adjusted OOTB {@link SiteMapMediaJob#perform(SiteMapMediaCronJobModel)} method.
	 * Modification: custom fail logic in case any generator failed.
	 *
	 * {@inheritDoc}
	 */
	@Override
	public PerformResult perform(final SiteMapMediaCronJobModel cronJob)
	{

		final List<MediaModel> siteMapMedias = new ArrayList<>();
		final CMSSiteModel contentSite = cronJob.getContentSite();

		getCmsSiteService().setCurrentSite(contentSite);
		// set the catalog version for the current session
		getActivateBaseSiteInSession().activate(contentSite);

		final SiteMapConfigModel siteMapConfig = contentSite.getSiteMapConfig();
		final Collection<SiteMapPageModel> siteMapPages = siteMapConfig.getSiteMapPages();
		// OOTB modification
		final Collection<SiteMapPageModel> failedSiteMapPages = new ArrayList<>();
		for (final SiteMapPageModel siteMapPage : siteMapPages)
		{
			final List<File> siteMapFiles = new ArrayList<>();
			final SiteMapPageEnum pageType = siteMapPage.getCode();
			final SiteMapGenerator generator = this.getGeneratorForSiteMapPage(pageType);

			if (BooleanUtils.isTrue(siteMapPage.getActive()) && generator != null)
			{
				// OOTB modification
				try
				{
					prepareModelsList(cronJob, contentSite, siteMapConfig, siteMapFiles, pageType, generator);
				}
				catch (IllegalArgumentException e)
				{
					getLogger().error(String.format(PREPARE_MODEL_LIST_FOR_SITE_MAP_PAGE_FAILED_MESSAGE, siteMapPage.getCode()),
							e);
					failedSiteMapPages.add(siteMapPage);
				}
			}
			else
			{
				LOG.warn(String.format("Skipping SiteMap page %s active %s", siteMapPage.getCode(), siteMapPage.getActive()));
			}
			if (!siteMapFiles.isEmpty())
			{
				for (final File siteMapFile : siteMapFiles)
				{
					siteMapMedias.add(createCatalogUnawareMediaModel(siteMapFile));
				}
			}
		}
		// OOTB modification
		if (!failedSiteMapPages.isEmpty())
		{
			throw new IllegalArgumentException(PREPARE_MODEL_LIST_FOR_ONE_OF_SITE_MAP_PAGES_FAILED_MESSAGE);
		}

		if (!siteMapMedias.isEmpty())
		{
			final Collection<MediaModel> existingSiteMaps = contentSite.getSiteMaps();

			contentSite.setSiteMaps(siteMapMedias);
			modelService.save(contentSite);

			// clean up old sitemap medias
			if (CollectionUtils.isNotEmpty(existingSiteMaps))
			{
				modelService.removeAll(existingSiteMaps);
			}
		}


		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}


	/**
	 * Adjusted OOTB {@link SiteMapMediaJob#createCatalogUnawareMediaModel(File)} method.
	 * Modification: custom output folder.
	 *
	 * {@inheritDoc}
	 */
	@Override
	protected CatalogUnawareMediaModel createCatalogUnawareMediaModel(final File siteMapFile)
	{
		MediaFolderModel outputFolder = getMediaService().getFolder(configurationService.getConfiguration().getString(
				SITE_MAP_MEDIA_JOB_OUTPUT_FOLDER_PROPERTY_NAME));
		final CatalogUnawareMediaModel media = modelService.create(CatalogUnawareMediaModel.class);
		media.setCode(siteMapFile.getName());
		modelService.save(media);
		try
		{
			getMediaService().setStreamForMedia(media, getFileInputStream(siteMapFile), siteMapFile.getName(), SITE_MAP_MIME_TYPE,
					outputFolder);
		}
		catch (final FileNotFoundException e)
		{
			LOG.error(e);
		}
		return media;
	}

	protected FileInputStream getFileInputStream(final File siteMapFile) throws FileNotFoundException
	{
		return new FileInputStream(siteMapFile);
	}

	protected Logger getLogger()
	{
		return LOG;
	}

	@Override
	protected SiteMapGenerator getGeneratorForSiteMapPage(final SiteMapPageEnum siteMapPageEnum)
	{
		return super.getGeneratorForSiteMapPage(siteMapPageEnum);
	}

	@Override
	protected void prepareModelsList(final SiteMapMediaCronJobModel cronJob, final CMSSiteModel contentSite,
			final SiteMapConfigModel siteMapConfig, final List<File> siteMapFiles, final SiteMapPageEnum pageType,
			final SiteMapGenerator generator)
	{
		super.prepareModelsList(cronJob, contentSite, siteMapConfig, siteMapFiles, pageType, generator);
	}
}
