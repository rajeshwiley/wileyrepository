package com.wiley.core.price.dao.impl;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.Date;
import java.util.List;

import com.wiley.core.price.dao.WileyPriceDao;


public class WileyPriceDaoImpl extends DefaultGenericDao<PriceRowModel> implements WileyPriceDao
{
	private static final String TODAY = "today";
	private static final String SELECT_PRICES = "select {" + PriceRowModel.PK + "} from {" + PriceRowModel._TYPECODE + "}";
	private static final String PRODUCT_MATCH_CLAUSE =
			"({" + PriceRowModel.PRODUCTMATCHQUALIFIER + "}=?" + PriceRowModel.PRODUCTMATCHQUALIFIER
					+ " or {"
					+ PriceRowModel.PRODUCTID + "}=?" + PriceRowModel.PRODUCTID + ")";
	private static final String PRODUCT_ID_MATCH_CLAUSE = "{" + PriceRowModel.PRODUCTID + "}=?" + PriceRowModel.PRODUCTID;
	private static final String CURRENCY_MATCH_CLAUSE = "{" + PriceRowModel.CURRENCY + "}=?" + PriceRowModel.CURRENCY;
	private static final String COUNTRY_MATCH_CLAUSE = "{" + PriceRowModel.COUNTRY + "}=?" + PriceRowModel.COUNTRY;
	private static final String COUNTRY_IS_NULL_CLAUSE = "{" + PriceRowModel.COUNTRY + "} is null";
	private static final String MIN_QUANTITY_MATCH_CLAUSE = "{" + PriceRowModel.MINQTD + "}=?" + PriceRowModel.MINQTD;

	private static final String FIND_PRICES_BY_PRODUCT =
			SELECT_PRICES + " where " + PRODUCT_MATCH_CLAUSE + " and {" + PriceRowModel.USERMATCHQUALIFIER + "} = 0 and "
					+ CURRENCY_MATCH_CLAUSE + " and {" + PriceRowModel.NET
					+ "}=?" + PriceRowModel.NET + " and ({" + PriceRowModel.STARTTIME + "} is null or {" + PriceRowModel.STARTTIME
					+ "}<=?" + TODAY + ") and ({" + PriceRowModel.ENDTIME + "} is null or {" + PriceRowModel.ENDTIME + "}>?"
					+ TODAY + ")";
	private static final String FIND_PRICE =
			SELECT_PRICES + " where " + PRODUCT_ID_MATCH_CLAUSE + " and " + CURRENCY_MATCH_CLAUSE + " and "
					+ MIN_QUANTITY_MATCH_CLAUSE;

	public WileyPriceDaoImpl()
	{
		super(PriceRowModel._TYPECODE);
	}

	@Override
	public List<PriceRowModel> findPricesByProduct(final ProductModel productModel, final CurrencyModel currencyModel)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("productModel", productModel);
		ServicesUtil.validateParameterNotNullStandardMessage("currencyModel", currencyModel);

		FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_PRICES_BY_PRODUCT);
		query.addQueryParameter(PriceRowModel.PRODUCTMATCHQUALIFIER, productModel.getPk());
		query.addQueryParameter(PriceRowModel.PRODUCTID, productModel.getCode());
		query.addQueryParameter(PriceRowModel.CURRENCY, currencyModel);
		query.addQueryParameter(PriceRowModel.NET, true);
		query.addQueryParameter(TODAY, new Date());

		SearchResult<PriceRowModel> result = getFlexibleSearchService().search(query);
		return result.getResult();
	}

	@Override
	public List<PriceRowModel> findPrices(final PriceRowModel price)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("productId", price.getProductId());
		ServicesUtil.validateParameterNotNullStandardMessage("currency", price.getCurrency());

		String query = FIND_PRICE;
		if (price.getCountry() != null)
		{
			query += " and " + COUNTRY_MATCH_CLAUSE;
		}
		else
		{
			query += " and " + COUNTRY_IS_NULL_CLAUSE;
		}

		FlexibleSearchQuery fsQuery = new FlexibleSearchQuery(query);
		fsQuery.addQueryParameter(PriceRowModel.PRODUCTID, price.getProductId());
		fsQuery.addQueryParameter(PriceRowModel.CURRENCY, price.getCurrency());
		fsQuery.addQueryParameter(PriceRowModel.MINQTD, price.getMinqtd() != null ? price.getMinqtd() : 1L);
		if (price.getCountry() != null)
		{
			fsQuery.addQueryParameter(PriceRowModel.COUNTRY, price.getCountry());
		}

		final SearchResult<PriceRowModel> searchResult = getFlexibleSearchService().search(fsQuery);
		return searchResult.getResult();
	}
}
