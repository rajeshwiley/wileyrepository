/**
 *
 */
package com.wiley.core.wiley.externaltax.strategies.impl;

import de.hybris.platform.commerceservices.order.impl.DefaultCommerceCartCalculationStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartModel;


/**
 * Extending {@link DefaultCheckoutCartCalculationStrategy} for calling
 * {@link de.hybris.platform.commerceservices.externaltax.ExternalTaxesService} during recalculation of cart
 *
 * @author Dzmitryi_Halahayeu
 */
public class WileyCheckoutCartCalculationStrategy extends DefaultCommerceCartCalculationStrategy
{

	@Override
	public boolean recalculateCart(final CommerceCartParameter parameter)
	{
		final CartModel cartModel = parameter.getCart();
		final boolean calculated = super.recalculateCart(parameter);
		getExternalTaxesService().calculateExternalTaxes(cartModel);
		return calculated;
	}

}
