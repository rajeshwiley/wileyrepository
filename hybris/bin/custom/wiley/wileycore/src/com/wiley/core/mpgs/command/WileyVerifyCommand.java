package com.wiley.core.mpgs.command;

import com.wiley.core.mpgs.request.WileyVerifyRequest;
import com.wiley.core.mpgs.response.WileyVerifyResponse;


public interface WileyVerifyCommand extends WileyCommand<WileyVerifyRequest, WileyVerifyResponse>
{
}
