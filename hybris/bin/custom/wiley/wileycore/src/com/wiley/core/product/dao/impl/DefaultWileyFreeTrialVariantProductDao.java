package com.wiley.core.product.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;

import java.util.Collections;
import java.util.List;

import javax.annotation.Nonnull;

import com.wiley.core.model.WileyFreeTrialProductModel;
import com.wiley.core.model.WileyFreeTrialVariantProductModel;
import com.wiley.core.product.dao.WileyFreeTrialVariantProductDao;


/**
 * Created by Raman_Hancharou on 3/30/2016.
 */
public class DefaultWileyFreeTrialVariantProductDao extends DefaultGenericDao<WileyFreeTrialVariantProductModel>
		implements WileyFreeTrialVariantProductDao
{

	public DefaultWileyFreeTrialVariantProductDao()
	{
		super(WileyFreeTrialVariantProductModel._TYPECODE);
	}

	public DefaultWileyFreeTrialVariantProductDao(final String typecode)
	{
		super(typecode);
	}

	@Override
	@Nonnull
	public List<WileyFreeTrialVariantProductModel> findFreeTrialVariantProductsByCode(@Nonnull final String code)
	{
		return find(Collections.singletonMap(WileyFreeTrialProductModel.CODE, code));
	}
}
