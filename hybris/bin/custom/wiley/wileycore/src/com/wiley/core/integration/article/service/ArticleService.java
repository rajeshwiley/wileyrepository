package com.wiley.core.integration.article.service;

import java.util.Optional;

import javax.annotation.Nonnull;

import com.wiley.core.integration.article.dto.ArticleDto;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public interface ArticleService
{
	@Nonnull
	Optional<ArticleDto> getArticleData(@Nonnull String articleId);
}
