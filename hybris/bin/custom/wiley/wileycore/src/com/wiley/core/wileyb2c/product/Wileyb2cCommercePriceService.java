package com.wiley.core.wileyb2c.product;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.subscriptionservices.model.SubscriptionTermModel;

import java.util.List;

import com.wiley.core.model.WileyProductModel;
import com.wiley.core.price.WileyCommercePriceService;


/**
 * Author Herman_Chukhrai (EPAM)
 */
public interface Wileyb2cCommercePriceService extends WileyCommercePriceService
{
	List<PriceInformation> findPricesByProductAndSubscriptionTerm(ProductModel productModel,
			SubscriptionTermModel subscriptionTerm);

	boolean validateProductSubscriptionTermPrice(WileyProductModel productModel,
			SubscriptionTermModel subscriptionTerm);

	/**
	 * Calculates a sum of the set components prices;
	 *
	 * @param productModel
	 * @return
	 */
	PriceInformation getComponentsOriginalPrice(ProductModel productModel);
}
