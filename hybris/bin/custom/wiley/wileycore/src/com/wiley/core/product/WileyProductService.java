package com.wiley.core.product;

import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.Date;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.wiley.core.model.WileyProductModel;


/**
 * Interface extends functionality of {@link ProductService}.
 *
 * @author Aliaksei_Zlobich
 */
public interface WileyProductService extends ProductService
{


	/**
	 * Tries to get system requirements from product and, if the requirements are not found, searches it on category tree.<br/>
	 * Uses {@link com.wiley.core.category.WileyCategoryService}.
	 *
	 * @param product
	 * 		- any subclass of ProductModel.
	 * @return system requirements for product or empty string.
	 */
	@Nullable
	String getSystemRequirements(ProductModel product);

	/**
	 * Returns the Product with the specified code.
	 *
	 * @param code
	 * 		the code of the Product
	 * @return the WileyProduct matching the specified code.
	 * @throws de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException
	 * 		if no Product with the specified code and Catalog is found.
	 * @throws de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException
	 * 		if more than one Product with the specified code is found
	 */
	WileyProductModel getWileyProductForCode(String code);

	/**
	 * !!! This method should NOT be used for storefront !!!
	 * Returns the Product with the specified code and catalog version.
	 * It disables search restrictions and therefore can return any product from any catalog version,
	 * including products in Staged catalog version and not approved products
	 *
	 * @param code
	 * 		the code of the Product
	 * @param catalogVersionModel
	 * 		catalog version to which the product belongs
	 * @return the WileyProduct matching the specified code if it exists. Otherwise returns null.
	 * @throws de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException
	 * 		if more than one Product with the specified code is found
	 */
	WileyProductModel getWileyProductForCodeIfExists(String code, CatalogVersionModel catalogVersionModel);

	/**
	 * Saves product
	 * !!! This method should NOT be used for storefront !!!
	 * It disables search restrictions and therefore can save any product from any catalog version,
	 * including products in Staged catalog version and not approved products
	 *
	 * @param product
	 */
	void saveProduct(ProductModel product);

	/**
	 * Checks if product can be part of Product Set.
	 *
	 * @param product
	 * 		the product
	 * @return true if given product is Product Set else false.
	 */
	boolean canBePartOfProductSet(@Nullable ProductModel product);


	/**
	 * Returns all Products belonging to the specified Category as a list. For
	 * the search the current session active catalog versions of the current
	 * user are used. This method will not fetch the products from the
	 * subcategories of the specified category.
	 *
	 * @param categoryModel
	 * 		the category the returned Products belong to
	 * @return a list of Products which belong to the specified Category
	 * @throws IllegalArgumentException
	 * 		if parameter category is null
	 */
	List<ProductModel> getProductsForCategoryExcludingSubcategories(CategoryModel categoryModel);

	/**
	 * Returns the latest wileyProducts list from the root of category.
	 *
	 * @param categoryCode
	 * 		the Category code
	 * @return the list of {@link WileyProductModel}
	 */
	List<WileyProductModel> getLatestWileyProductsFromCategoryRoot(String categoryCode);

	/**
	 * Returns wileyProduct list from the root of category according to date.
	 * Method only compares the year and month of exam dates, and ignores the day.
	 *
	 * @param categoryCode
	 * 		the Category code
	 * @param date
	 * 		the Date
	 * @return the list of {@link WileyProductModel}
	 */
	List<WileyProductModel> getWileyProductsFromCategoryRoot(String categoryCode, Date date);

	/**
	 * Returns the products list from the root baseProductCategoryCode and variantValueCategoryCode
	 *
	 * @param baseProductCategoryCode
	 * 		category, base product should belong to
	 * @param variantValueCategoryCode
	 * 		variantValueCategory, productVariant should belong to
	 */
	List<ProductModel> getWileyProductsFromCategories(String baseProductCategoryCode,
			String variantValueCategoryCode);


	/**
	 * Returns true is variant product is addon. False in other case
	 *
	 * @param variantProductModel
	 * 		the variant product
	 * @return the boolean
	 */
	Boolean isAddon(VariantProductModel variantProductModel);

	/**
	 * Get latest exam date date.
	 *
	 * @param products
	 * 		the products
	 * @return the date
	 */
	Date getLatestExamDate(List<WileyProductModel> products);

	/**
	 * Get Product by isbn and optional term if it is a SubscriptionProduct.
	 *
	 * @param isbn
	 * 		the isbn of given product
	 * @param term
	 * 		the term of SubscriptionProduct
	 * @param catalogId
	 * 		the catalogId to lookup in
	 * @param catalogVersion
	 * 		the catalogVersion to lookup in
	 * @return the product
	 */
	ProductModel getProductForIsbnAndTerm(String isbn, String term, String catalogId, String catalogVersion);

	/**
	 * Get Product by isbn.
	 *
	 * @param isbn
	 * 		the isbn of given product
	 * @return the product
	 */
	ProductModel getProductForIsbn(String isbn);

	/**
	 * Get Product by isbn
	 *
	 * @param isbn
	 * 		the isbn of given product
	 * @param catalogVersion
	 * 		the catalogVersion to lookup in
	 * @return the product
	 */
	@Nullable
	ProductModel getProductForIsbn(@Nonnull String isbn, @Nonnull CatalogVersionModel catalogVersion);

	/**
	 * method is used for filtering products list with the following logic:
	 * - variant products are grouped by common base product codes
	 * - non variant products are grouped by product codes
	 * - elements in each group are sorted by code
	 * - collect first elements from each group to resulted list
	 *
	 * @param products
	 * @return filtered products list
	 */
	List<ProductModel> filterProducts(List<ProductModel> products);

	/**
	 * Method returns true if there is at least one product with appropriate code and catalogVersion
	 *
	 * @param catalogVersionModel
	 * @param productCode
	 * @return
	 */
	boolean isProductForCodeExist(CatalogVersionModel catalogVersionModel, String productCode);


	/**
	 * Provides data access methods for products validation needs
	 *
	 * @param approvalStatus
	 * 		The {@link ArticleApprovalStatus} of products
	 * @param catalogVersion
	 */
	@Nonnull
	List<ProductModel> getProductsToValidate(ArticleApprovalStatus approvalStatus, CatalogVersionModel catalogVersion);

	/**
	 * Get Product by MediaContainer in galleryImages.
	 *
	 * @param mediaContainer
	 * 		the mediaContainer of given product
	 * @return the product
	 */
	List<ProductModel> getProductsForMediaContainerInGalleryImages(MediaContainerModel mediaContainer);

	/**
	 * Get Product by MediaContainer in backgroundImage.
	 *
	 * @param mediaContainer
	 * 		the mediaContainer of given product
	 * @return the product
	 */
	List<ProductModel> getProductsForMediaContainerInBackgroundImage(MediaContainerModel mediaContainer);

	/**
	 * Update modification time for products of catalogs versions.
	 * Prefer catalog versions have to be included in current session or selected in a different way.
	 *
	 * @param productCode
	 * 		product to be updated
	 */
	void updateProductsModifiedTime(String productCode);

	boolean isBaseProductFromCategory(ProductModel product, String categoryCode);
}
