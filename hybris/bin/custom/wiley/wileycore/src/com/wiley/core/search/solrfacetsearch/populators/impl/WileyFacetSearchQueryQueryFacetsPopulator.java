package com.wiley.core.search.solrfacetsearch.populators.impl;

import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.search.impl.SearchQueryConverterData;

import com.wiley.core.search.client.solrj.WileySolrQuery;
import com.wiley.core.search.client.solrj.facet.QueryFacetSwg;
import com.wiley.core.search.solrfacetsearch.WileyQueryFacet;
import com.wiley.core.search.solrfacetsearch.WileySearchQuery;


/**
 * Search query populator for Query Facets
 */
public class WileyFacetSearchQueryQueryFacetsPopulator extends WileyAbstractFacetSearchQueryPopulator
{
	public static final String QUERY_FACET_NAME_DELIMETER = ":";

	@Override
	public void populate(final SearchQueryConverterData source, final WileySolrQuery target) throws ConversionException
	{
		final WileySearchQuery wileySearchQuery = (WileySearchQuery) source.getSearchQuery();
		for (final WileyQueryFacet wileyQueryFacet : wileySearchQuery.getQueryFacets())
		{
			if (facetNotExcluded(wileySearchQuery, wileyQueryFacet))
			{
				QueryFacetSwg queryFacet = buildQueryFacetSwg(wileySearchQuery, wileyQueryFacet);
				target.getJsonParam().getFacet().addQueryFacet(queryFacet);
			}
		}
	}

	private boolean facetNotExcluded(final WileySearchQuery wileySearchQuery, final WileyQueryFacet wileyQueryFacet)
	{
		return wileySearchQuery.getFacets().stream()
				.filter(facetField -> facetField.getField().equals(wileyQueryFacet.getField()))
				.findAny()
				.isPresent();
	}

	private QueryFacetSwg buildQueryFacetSwg(final WileySearchQuery wileySearchQuery, final WileyQueryFacet wileyQueryFacet)
	{
		final String translatedField = this.getFieldNameTranslator().translate(wileySearchQuery, wileyQueryFacet.getField(),
				FieldNameProvider.FieldType.INDEX);

		String fullQuery = translatedField + QUERY_FACET_NAME_DELIMETER + wileyQueryFacet.getValue();
		QueryFacetSwg queryFacet = new QueryFacetSwg(fullQuery);
		String groupFacet = getGroupFacet(wileySearchQuery);
		if (groupFacet != null)
		{
			queryFacet.getFacet().put(GROUP_FACET, groupFacet);
		}
		return queryFacet;
	}
}
