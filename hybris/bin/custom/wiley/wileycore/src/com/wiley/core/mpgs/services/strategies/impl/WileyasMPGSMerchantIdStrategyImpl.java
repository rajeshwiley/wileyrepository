package com.wiley.core.mpgs.services.strategies.impl;

import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wiley.core.model.WileyTnsMerchantModel;
import com.wiley.core.mpgs.services.strategies.WileyMPGSMerchantIdStrategy;
import com.wiley.core.payment.tnsmerchant.service.WileyTnsMerchantService;


public class WileyasMPGSMerchantIdStrategyImpl extends AbstractMPGSMerchantIdStrategy implements WileyMPGSMerchantIdStrategy
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyasMPGSMerchantIdStrategyImpl.class);

	@Resource
	private WileyTnsMerchantService wileyTnsMerchantService;

	@Override
	public String getMerchantID(final AbstractOrderModel abstractOrderModel)
	{
		final WileyTnsMerchantModel tnsMerchant = getMerchant(abstractOrderModel);
		final String tnsMerchantId = tnsMerchant.getId();

		if (StringUtils.isEmpty(tnsMerchantId))
		{
			final CountryModel country = abstractOrderModel.getPaymentAddress().getCountry();
			final CurrencyModel currency = abstractOrderModel.getCurrency();
			final String errorMessage = String.format("Merchant ID is not set for the country [%s] and currency [%s]",
					country.getIsocode(), currency.getIsocode());
			LOG.error(errorMessage);
			throw new IllegalStateException(errorMessage);
		}
		return tnsMerchantId;
	}

	@Override
	public WileyTnsMerchantModel getMerchant(final AbstractOrderModel abstractOrderModel)
	{
		final CountryModel country = abstractOrderModel.getPaymentAddress().getCountry();
		final CurrencyModel currency = abstractOrderModel.getCurrency();
		return wileyTnsMerchantService.getTnsMerchant(country.getIsocode(), currency.getIsocode());
	}
}
