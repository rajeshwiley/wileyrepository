package com.wiley.core.validation.service.impl;

import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.validation.exceptions.ConfigurableHybrisConstraintViolation;
import de.hybris.platform.validation.exceptions.HybrisConstraintViolation;
import de.hybris.platform.validation.extractor.ConstraintsExtractor;
import de.hybris.platform.validation.localized.LocalizedAttributeConstraint;
import de.hybris.platform.validation.localized.LocalizedConstraintsRegistry;
import de.hybris.platform.validation.localized.TypeLocalizedConstraints;
import de.hybris.platform.validation.model.constraints.AbstractConstraintModel;
import de.hybris.platform.validation.services.ConstraintViolationFactory;
import de.hybris.platform.validation.services.impl.DefaultValidationService;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;

import javax.validation.Configuration;
import javax.validation.ConstraintViolation;
import javax.validation.ValidationException;
import javax.validation.Validator;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class WileyValidationService extends DefaultValidationService
{
	private static final Logger LOG = Logger.getLogger(WileyValidationService.class);
	@Autowired
	private LocalizedConstraintsRegistry localizedConstraintsRegistry;
	@Autowired
	private ConstraintsExtractor constraintsExtractor;
	@Autowired
	private ConstraintViolationFactory violationFactory;
	private long lastReloadOfValidationEngine;
	private volatile Validator validator;

	//OOTB code
	@Override
	public <T> Set<HybrisConstraintViolation> validate(final T object, final Class<?>... groups)
	{
		final Set resultRaw = getValidator().validate(object, groups); //NOSONAR
		final HashSet result = new HashSet(resultRaw.size());
		final Iterator localizedConstraint = resultRaw.iterator();

		while (localizedConstraint.hasNext())
		{
			final ConstraintViolation typeLocalizedConstraints = (ConstraintViolation) localizedConstraint.next();
			final HybrisConstraintViolation viol = wrapConstraint(typeLocalizedConstraints);
			if (viol != null)
			{
				result.add(viol);
			}
		}

		if (isMultiLanguageValidationDisabled())
		{
			return result;
		}
		else
		{
			final TypeLocalizedConstraints typeLocalizedConstraints1 = localizedConstraintsRegistry.get(object.getClass());
			final Iterator viol1 = typeLocalizedConstraints1.getConstraints().iterator();

			while (viol1.hasNext())
			{
				final LocalizedAttributeConstraint localizedConstraint1 = (LocalizedAttributeConstraint) viol1.next();
				if (localizedConstraint1.definedForGroups(groups))
				{
					deleteViolationsTriggeredByConstraint(result, localizedConstraint1);
					result.addAll(validateLocalizedConstraint(object, localizedConstraint1, groups));
				}
			}

			return result;
		}
	}

	//OOTB
	private Validator getValidator()
	{
		Validator ret = validator;
		if (ret == null)
		{
			synchronized (this)
			{
				ret = validator;
				if (ret == null)
				{
					ret = createCustomizedValidator();
					setValidator(ret);
				}
			}
		}

		return ret;
	}

	//OOTB
	private void setValidator(final Validator newOne)
	{
		lastReloadOfValidationEngine = System.currentTimeMillis() - 1L;
		validator = newOne;
	}

	//OOTB
	public boolean needReloadOfValidationEngine(final AbstractConstraintModel model)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("model", model);

		return !this.getModelService().isNew(model) && (validator != null && lastReloadOfValidationEngine < model
				.getModifiedtime().getTime());
	}

	//OOTB
	private Validator createCustomizedValidator()
	{
		Validator ret = null;
		final Configuration cfg = getOrCreateConfiguration(true);
		appendXMLMapping(cfg);

		try
		{
			ret = getOrCreateConfiguration(false).buildValidatorFactory().getValidator();
		}
		catch (final ValidationException e)
		{
			LOG.error("Problem occurred with loading mappings stream: " + e.getMessage(), e);
			if (LOG.isDebugEnabled())
			{
				LOG.debug(String.format("Not parseable XML stream [\n%s\n]", new Object[]
				{ readXMLSource() }));
			}
			else
			{
				LOG.error("Please set log level to DEBUG for more details!");
			}
		}

		return ret;
	}

	//OOTB
	void appendXMLMapping(final Configuration cfg)
	{
		InputStream input = null;

		try
		{
			input = constraintsExtractor.extractConstraints();
			cfg.addMapping(input);
		}
		finally
		{
			IOUtils.closeQuietly(input);
		}

	}

	//OOTB
	private String readXMLSource()
	{
		String src = null;
		InputStream stream = null;

		try
		{
			stream = constraintsExtractor.extractConstraints();
			src = IOUtils.toString(stream);
		}
		catch (final IOException e)
		{
			LOG.error(e);
			src = "error getting xml source: " + e.getMessage();
		}
		finally
		{
			IOUtils.closeQuietly(stream);
		}

		return src;
	}

	//OOTB
	private HybrisConstraintViolation wrapConstraint(final ConstraintViolation violation)
	{
		final ConfigurableHybrisConstraintViolation result = lookupViolation();
		result.setConstraintViolation(violation);
		return result;
	}

	//OOTB
	private void deleteViolationsTriggeredByConstraint(final Set<HybrisConstraintViolation> violations,
			final LocalizedAttributeConstraint localizedConstraint)
	{
		violations.removeIf(localizedConstraint::matchesNonLocalizedViolation);
	}

	private Set<HybrisConstraintViolation> validateLocalizedConstraint(final Object object,
			final LocalizedAttributeConstraint localizedConstraint, final Class... groups)
	{
		final HashSet result = new HashSet();
		final Iterator constraints = localizedConstraint.getLanguages().iterator();

		while (constraints.hasNext())
		{
			final Locale locale = (Locale) constraints.next();
			final String attribute = localizedConstraint.getAttribute();
			//modified OOTB code to with toString instead of casting
			final Object attributeValue = getModelService().getAttributeValue(object, attribute, locale);
			final String attributeStringValue = attributeValue == null ? null : attributeValue.toString();
			final Set violations = validateValue(object.getClass(), attribute, attributeStringValue, groups);
			final Iterator voilationsIter = violations.iterator();

			while (voilationsIter.hasNext())
			{
				final HybrisConstraintViolation v = (HybrisConstraintViolation) voilationsIter.next();
				if (localizedConstraint.matchesNonLocalizedViolation(v))
				{
					final HybrisConstraintViolation newViolation = violationFactory
							.createLocalizedConstraintViolation(v.getConstraintViolation(), locale);
					result.add(newViolation);
				}
			}
		}

		return result;
	}

}
