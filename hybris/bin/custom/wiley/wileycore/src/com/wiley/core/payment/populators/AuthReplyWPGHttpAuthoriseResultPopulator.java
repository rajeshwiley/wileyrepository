package com.wiley.core.payment.populators;

import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.response.AbstractResultPopulator;
import de.hybris.platform.acceleratorservices.payment.data.AuthReplyData;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.acceleratorservices.payment.data.OrderInfoData;
import de.hybris.platform.acceleratorservices.payment.data.SignatureData;
import de.hybris.platform.acceleratorservices.payment.data.SubscriptionSignatureData;
import de.hybris.platform.acceleratorservices.payment.data.WPGHttpValidateResultData;
import de.hybris.platform.acceleratorservices.payment.enums.DecisionsEnum;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wiley.core.payment.enums.WileyTransactionStatusEnum;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


public class AuthReplyWPGHttpAuthoriseResultPopulator
		extends AbstractResultPopulator<Map<String, String>, CreateSubscriptionResult>
{
	private static final Logger LOG = LoggerFactory.getLogger(AuthReplyWPGHttpAuthoriseResultPopulator.class);

	@Override
	public void populate(final Map<String, String> wpgResponseParams, final CreateSubscriptionResult target)
			throws ConversionException
	{
		validateParameterNotNull(target, "Parameter [CreateSubscriptionResult] target cannot be null");
		final WPGHttpValidateResultData wpgResultInfoData = target.getWpgResultInfoData();
		validateParameterNotNull(wpgResultInfoData, "Parameter [wpgResultInfoData] wpgResultInfoData cannot be null");

		String wpgReturnCode = wpgResultInfoData.getReturnCode();
		if (WileyTransactionStatusEnum.SUCCESS.getCode().equals(wpgReturnCode))
		{
			target.setDecision(DecisionsEnum.ACCEPT.name());
		}
		else
		{
			target.setDecision(DecisionsEnum.REJECT.name());
		}

		final AuthReplyData authReplyData = new AuthReplyData();
		authReplyData.setCvnDecision(Boolean.TRUE);
		target.setAuthReplyData(authReplyData);

		final OrderInfoData orderInfoData = new OrderInfoData();
		target.setOrderInfoData(orderInfoData);

		final SignatureData signatureData = new SignatureData();
		target.setSignatureData(signatureData);

		final SubscriptionSignatureData subscriptionSignatureData = new SubscriptionSignatureData();
		target.setSubscriptionSignatureData(subscriptionSignatureData);
		try
		{
			target.setReasonCode(Integer.parseInt(wpgReturnCode));
		}
		catch (NumberFormatException ex)
		{
			LOG.warn("Not numeric return code return from WPG", ex);
		}
	}
}
