/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.core.event;

import de.hybris.platform.orderprocessing.events.OrderProcessingEvent;
import de.hybris.platform.returns.model.ReturnRequestModel;


/**
 * The type Order refund event.
 */
public class WileyOrderRefundEvent extends OrderProcessingEvent
{
	private static final long serialVersionUID = 1L;
	private ReturnRequestModel returnRequest;

	public WileyOrderRefundEvent(final ReturnRequestModel returnRequest)
	{
		super(null);
		this.returnRequest = returnRequest;
	}

	public ReturnRequestModel getReturnRequest()
	{
		return returnRequest;
	}

	public void setReturnRequest(final ReturnRequestModel returnRequest)
	{
		this.returnRequest = returnRequest;
	}
}
