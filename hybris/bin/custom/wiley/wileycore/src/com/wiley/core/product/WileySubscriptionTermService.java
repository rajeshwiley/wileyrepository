package com.wiley.core.product;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.subscriptionservices.model.SubscriptionTermModel;

import java.util.Optional;


/**
 * Wliey service to work with {@link SubscriptionTermModel}
 */
public interface WileySubscriptionTermService
{
	/**
	 * Fetch subscription term by id
	 *
	 * @param id
	 * 		Value to be equal to {@link SubscriptionTermModel#ID} attribute
	 * @return Found SubscriptionTermModel
	 */
	SubscriptionTermModel getSubscriptionTerm(String id);

	Optional<SubscriptionTermModel> findSubscriptionTerm(ProductModel product, String subscriptionTermId);

	boolean isSubscriptionProduct(ProductModel product);
}
