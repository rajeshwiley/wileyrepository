package com.wiley.core.wileyas.order.dao;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;

import java.util.List;

import com.wiley.core.order.dao.WileyOrderEntryDao;


/**
 * Created by Maksim_kozich on 27.02.2018.
 */
public interface WileyasOrderEntryDao extends WileyOrderEntryDao
{
	List<AbstractOrderEntryModel> findOrderEntriesByBusinessKey(String businessKey, List<OrderStatus> excludedOrderEntryStatuses);
}
