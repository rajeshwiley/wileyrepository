package com.wiley.core.externaltax.dto;

import java.util.Date;
import java.util.List;


public class TaxCalculationRequestDto
{
	private String siteId;
	private Date date;
	private String currency;
	private TaxAddressDto paymentAddress;
	private TaxAddressDto deliveryAddress;
	private List<TaxCalculationRequestEntryDto> entries;
	private Double handlingCost;
	private Double shippingCost;

	public String getSiteId()
	{
		return siteId;
	}

	public void setSiteId(final String siteId)
	{
		this.siteId = siteId;
	}

	public Date getDate()
	{
		return date;
	}

	public void setDate(final Date date)
	{
		this.date = date;
	}

	public String getCurrency()
	{
		return currency;
	}

	public void setCurrency(final String currency)
	{
		this.currency = currency;
	}

	public TaxAddressDto getPaymentAddress()
	{
		return paymentAddress;
	}

	public void setPaymentAddress(final TaxAddressDto paymentAddress)
	{
		this.paymentAddress = paymentAddress;
	}

	public TaxAddressDto getDeliveryAddress()
	{
		return deliveryAddress;
	}

	public void setDeliveryAddress(final TaxAddressDto deliveryAddress)
	{
		this.deliveryAddress = deliveryAddress;
	}

	public List<TaxCalculationRequestEntryDto> getEntries()
	{
		return entries;
	}

	public void setEntries(final List<TaxCalculationRequestEntryDto> entries)
	{
		this.entries = entries;
	}

	public Double getHandlingCost()
	{
		return handlingCost;
	}

	public void setHandlingCost(final Double handlingCost)
	{
		this.handlingCost = handlingCost;
	}

	public Double getShippingCost()
	{
		return shippingCost;
	}

	public void setShippingCost(final Double shippingCost)
	{
		this.shippingCost = shippingCost;
	}
}
