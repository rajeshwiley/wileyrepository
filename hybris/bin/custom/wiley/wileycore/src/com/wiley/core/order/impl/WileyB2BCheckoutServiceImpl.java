package com.wiley.core.order.impl;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.order.WileyB2BCheckoutService;

public class WileyB2BCheckoutServiceImpl extends WileycomCheckoutServiceImpl implements WileyB2BCheckoutService
{

	@Autowired
	private CartService cartService;
	@Autowired
	private ModelService modelService;

	@Override
	public void setDesiredShippingDate(final Date desiredShippingDate)
	{
		if (cartService.hasSessionCart()) {
			final CartModel cartModel = cartService.getSessionCart();
			cartModel.setDesiredShippingDate(desiredShippingDate);
			modelService.save(cartModel);
		}
	}
}
