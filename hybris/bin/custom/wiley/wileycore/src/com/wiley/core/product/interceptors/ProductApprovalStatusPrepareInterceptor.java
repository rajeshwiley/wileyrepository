package com.wiley.core.product.interceptors;

import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;
import de.hybris.platform.servicelayer.user.UserService;

import javax.annotation.Resource;

import com.wiley.core.constants.WileyCoreConstants;


/**
 * Created by Raman_Hancharou on 5/15/2017.
 */
public class ProductApprovalStatusPrepareInterceptor implements PrepareInterceptor<ProductModel>
{
	@Resource
	private UserService userService;

	@Override
	public void onPrepare(final ProductModel productModel, final InterceptorContext interceptorContext)
			throws InterceptorException
	{
		try
		{
			final UserGroupModel productEditorGroup = userService.getUserGroupForUID(WileyCoreConstants.PRODUCTEDITORGROUP_UID);
			final UserModel currentUser = userService.getCurrentUser();
			if (userService.isMemberOfGroup(currentUser, productEditorGroup) && (interceptorContext == null || interceptorContext
					.isModified(productModel)))
			{
				productModel.setApprovalStatus(ArticleApprovalStatus.CHECK);
			}
		}
		catch (UnknownIdentifierException e)
		{
			//don't prepare when producteditorgroup not found
		}
	}
}

