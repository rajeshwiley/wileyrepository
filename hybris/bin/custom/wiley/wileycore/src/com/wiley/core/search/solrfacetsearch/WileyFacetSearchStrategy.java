package com.wiley.core.search.solrfacetsearch;

import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.model.SolrIndexModel;
import de.hybris.platform.solrfacetsearch.search.FacetSearchException;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.solrfacetsearch.search.SearchResult;
import de.hybris.platform.solrfacetsearch.search.context.FacetSearchContext;
import de.hybris.platform.solrfacetsearch.search.context.FacetSearchContextFactory;
import de.hybris.platform.solrfacetsearch.search.impl.AbstractFacetSearchStrategy;
import de.hybris.platform.solrfacetsearch.search.impl.SearchQueryConverterData;
import de.hybris.platform.solrfacetsearch.search.impl.SearchResultConverterData;
import de.hybris.platform.solrfacetsearch.solr.Index;
import de.hybris.platform.solrfacetsearch.solr.SolrIndexService;
import de.hybris.platform.solrfacetsearch.solr.SolrSearchProvider;
import de.hybris.platform.solrfacetsearch.solr.SolrSearchProviderFactory;
import de.hybris.platform.solrfacetsearch.solr.exceptions.SolrServiceException;

import java.io.IOException;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrRequest.METHOD;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.util.IOUtils;
import org.springframework.beans.factory.annotation.Required;


/**
 * OOTB_CODE copypaste of DefaultFacetSearchStrategy to POST solr query instead of GET. Moreover customized logic is placed in
 * {@link #postProcessSearchResult(SolrClient, FacetSearchContext, Index, SearchResult)}
 *
 * Created by Uladzimir_Barouski on 3/17/2017.
 */
public abstract class WileyFacetSearchStrategy extends AbstractFacetSearchStrategy
{
	private static final Logger LOG = Logger.getLogger(WileyFacetSearchStrategy.class);
	private SolrSearchProviderFactory solrSearchProviderFactory;
	private FacetSearchContextFactory<FacetSearchContext> facetSearchContextFactory;
	private Converter<SearchQueryConverterData, SolrQuery> facetSearchQueryConverter;
	private Converter<SearchResultConverterData, SearchResult> facetSearchResultConverter;
	private SolrIndexService solrIndexService;

	public WileyFacetSearchStrategy()
	{
	}

	public FacetSearchContextFactory<FacetSearchContext> getFacetSearchContextFactory()
	{
		return facetSearchContextFactory;
	}

	public SolrSearchProviderFactory getSolrSearchProviderFactory()
	{
		return solrSearchProviderFactory;
	}

	public void setSolrSearchProviderFactory(final SolrSearchProviderFactory solrSearchProviderFactory)
	{
		this.solrSearchProviderFactory = solrSearchProviderFactory;
	}

	@Required
	public void setFacetSearchContextFactory(final FacetSearchContextFactory<FacetSearchContext> facetSearchContextFactory)
	{
		this.facetSearchContextFactory = facetSearchContextFactory;
	}

	public Converter<SearchQueryConverterData, SolrQuery> getFacetSearchQueryConverter()
	{
		return facetSearchQueryConverter;
	}

	@Required
	public void setFacetSearchQueryConverter(final Converter<SearchQueryConverterData, SolrQuery> facetSearchQueryConverter)
	{
		this.facetSearchQueryConverter = facetSearchQueryConverter;
	}

	public Converter<SearchResultConverterData, SearchResult> getFacetSearchResultConverter()
	{
		return facetSearchResultConverter;
	}

	@Required
	public void setFacetSearchResultConverter(final Converter<SearchResultConverterData, SearchResult> facetSearchResultConverter)
	{
		this.facetSearchResultConverter = facetSearchResultConverter;
	}

	public SolrIndexService getSolrIndexService()
	{
		return solrIndexService;
	}

	@Required
	public void setSolrIndexService(final SolrIndexService solrIndexService)
	{
		this.solrIndexService = solrIndexService;
	}

	public SearchResult search(final SearchQuery searchQuery, final Map<String, String> searchHints) throws FacetSearchException
	{
		checkQuery(searchQuery);
		SolrClient solrClient = null;

		try
		{
			FacetSearchConfig facetSearchConfig = searchQuery.getFacetSearchConfig();
			IndexedType indexedType = searchQuery.getIndexedType();
			FacetSearchContext facetSearchContext = this.facetSearchContextFactory.createContext(facetSearchConfig, indexedType,
					searchQuery);
			facetSearchContext.getSearchHints().putAll(searchHints);
			this.facetSearchContextFactory.initializeContext();
			this.checkContext(facetSearchContext);

			if (MapUtils.isNotEmpty(searchHints))
			{
				if (!Boolean.parseBoolean(searchHints.get("execute")))
				{
					return null;
				}
			}

			SolrSearchProvider solrSearchProvider = this.solrSearchProviderFactory.getSearchProvider(facetSearchConfig,
					indexedType);
			SolrIndexModel activeIndex = this.solrIndexService.getActiveIndex(facetSearchConfig.getName(),
					indexedType.getIdentifier());
			Index index = solrSearchProvider.resolveIndex(facetSearchConfig, indexedType, activeIndex.getQualifier());
			solrClient = solrSearchProvider.getClient(index);
			SearchQueryConverterData searchQueryConverterData = new SearchQueryConverterData();
			searchQueryConverterData.setFacetSearchContext(facetSearchContext);
			searchQueryConverterData.setSearchQuery(searchQuery);
			SolrQuery solrQuery = facetSearchQueryConverter.convert(searchQueryConverterData);

			if (LOG.isDebugEnabled())
			{
				LOG.debug(solrQuery);
			}

			METHOD method = this.resolveQueryMethod(facetSearchConfig);
			QueryResponse queryResponse = solrClient.query(index.getName(), solrQuery, method);
			SearchResultConverterData searchResultConverterData = new SearchResultConverterData();
			searchResultConverterData.setFacetSearchContext(facetSearchContext);
			searchResultConverterData.setQueryResponse(queryResponse);
			SearchResult searchResult = (SearchResult) this.facetSearchResultConverter.convert(searchResultConverterData);
			this.facetSearchContextFactory.getContext().setSearchResult(searchResult);
			this.facetSearchContextFactory.destroyContext();

			return postProcessSearchResult(solrClient, facetSearchContext, index, searchResult);
		}
		catch (SolrServerException | IOException | RuntimeException | SolrServiceException e)
		{
			this.facetSearchContextFactory.destroyContext(e);
			throw new FacetSearchException(e.getMessage(), e);
		}
		finally
		{
			IOUtils.closeQuietly(solrClient);
		}
	}

	protected abstract SearchResult postProcessSearchResult(SolrClient solrClient,
			FacetSearchContext facetSearchContext,
			Index index,
			SearchResult searchResult)
			throws FacetSearchException, SolrServerException, IOException;
}