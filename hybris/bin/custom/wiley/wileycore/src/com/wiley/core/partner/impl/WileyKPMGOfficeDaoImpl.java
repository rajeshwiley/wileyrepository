package com.wiley.core.partner.impl;


import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import com.wiley.core.model.KpmgOfficeModel;
import com.wiley.core.model.WileyPartnerCompanyModel;
import com.wiley.core.partner.WileyKPMGOfficeDao;


public class WileyKPMGOfficeDaoImpl extends DefaultGenericDao<KpmgOfficeModel> implements WileyKPMGOfficeDao
{
	private static final Logger LOG = Logger.getLogger(WileyKPMGOfficeDaoImpl.class);

	private static final String SELECT_OFFICES_BY_PARTNER_ID_QUERY = "SELECT {office.pk} "
			+ "FROM {" + KpmgOfficeModel._TYPECODE + " AS office "
			+ "JOIN " + WileyPartnerCompanyModel._TYPECODE + " AS partner "
			+ "ON {office." + KpmgOfficeModel.PARTNER + "} = {partner." + WileyPartnerCompanyModel.PK + "}} "
			+ "WHERE {partner." + WileyPartnerCompanyModel.UID + "} = ?partnerId";

	private static final String SELECT_OFFICE_BY_CODE = "SELECT {office.pk} "
			+ "FROM {" + KpmgOfficeModel._TYPECODE + " AS office} "
			+ "WHERE {office." + KpmgOfficeModel.CODE + "} = ?officeCode";

	public WileyKPMGOfficeDaoImpl()
	{
		super(KpmgOfficeModel._TYPECODE);
	}

	@Override
	public List<KpmgOfficeModel> findOffices(final String partnerId)
	{
		Map<String, Object> queryParams = new HashMap<>();
		queryParams.put("partnerId", partnerId);

		final SearchResult<KpmgOfficeModel> searchResult = getFlexibleSearchService().search(SELECT_OFFICES_BY_PARTNER_ID_QUERY,
				queryParams);
		return getResultOrEmptyList(searchResult);
	}

	@Override
	public KpmgOfficeModel findOfficeByCode(final String code)
	{
		Map<String, Object> queryParams = new HashMap<>();
		queryParams.put("officeCode", code);

		final SearchResult<KpmgOfficeModel> searchResult = getFlexibleSearchService().search(SELECT_OFFICE_BY_CODE,
				queryParams);
		return getSingleResult(searchResult, code);
	}

	private KpmgOfficeModel getSingleResult(final SearchResult<KpmgOfficeModel> searchResult, final String code)
	{
		KpmgOfficeModel office = null;
		List<KpmgOfficeModel> offices = searchResult.getResult();

		if (CollectionUtils.isNotEmpty(offices))
		{
			office = offices.get(0);
		}
		else
		{
			LOG.error("Cannot find " + KpmgOfficeModel._TYPECODE + " with code " + code);
		}

		if (offices != null && offices.size() > 1)
		{
			LOG.error("Found multiple objects of type " + KpmgOfficeModel._TYPECODE + " with code " + code);
		}
		return office;
	}

	private List<KpmgOfficeModel> getResultOrEmptyList(final SearchResult<KpmgOfficeModel> searchResult)
	{
		List<KpmgOfficeModel> offices = searchResult.getResult();
		if (offices == null)
		{
			offices = Collections.emptyList();
		}
		return offices;
	}
}
