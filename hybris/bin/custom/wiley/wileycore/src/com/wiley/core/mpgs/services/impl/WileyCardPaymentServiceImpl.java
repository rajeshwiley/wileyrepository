package com.wiley.core.mpgs.services.impl;

import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.payment.commands.factory.CommandNotSupportedException;
import de.hybris.platform.payment.methods.impl.DefaultCardPaymentServiceImpl;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import javax.annotation.Nonnull;

import com.wiley.core.mpgs.command.WileyFollowOnRefundCommand;
import com.wiley.core.mpgs.command.WileyRetrieveSessionCommand;
import com.wiley.core.mpgs.command.WileyAuthorizationCommand;
import com.wiley.core.mpgs.command.WileyCaptureCommand;
import com.wiley.core.mpgs.command.WileyTokenizationCommand;
import com.wiley.core.mpgs.command.WileyVerifyCommand;
import com.wiley.core.mpgs.request.WileyAuthorizationRequest;
import com.wiley.core.mpgs.request.WileyCaptureRequest;
import com.wiley.core.mpgs.request.WileyRetrieveSessionRequest;
import com.wiley.core.mpgs.request.WileyTokenizationRequest;
import com.wiley.core.mpgs.request.WileyVerifyRequest;
import com.wiley.core.mpgs.response.WileyAuthorizationResponse;
import com.wiley.core.mpgs.response.WileyFollowOnRefundResponse;
import com.wiley.core.mpgs.response.WileyCaptureResponse;
import com.wiley.core.mpgs.response.WileyRetrieveSessionResponse;
import com.wiley.core.mpgs.response.WileyTokenizationResponse;
import com.wiley.core.mpgs.response.WileyVerifyResponse;
import com.wiley.core.mpgs.services.WileyCardPaymentService;
import com.wiley.core.payment.request.WileyFollowOnRefundRequest;



public class WileyCardPaymentServiceImpl extends DefaultCardPaymentServiceImpl implements WileyCardPaymentService
{
	@Override
	public WileyRetrieveSessionResponse retrieveSession(@Nonnull final WileyRetrieveSessionRequest request)
	{
		ServicesUtil.validateParameterNotNull(request, "[request] cannot be null.)");
		try
		{
			WileyRetrieveSessionCommand command = getCommandFactoryRegistry().getFactory(
					request.getPaymentProvider()).createCommand(WileyRetrieveSessionCommand.class);
			return command.perform(request);
		}
		catch (CommandNotSupportedException ex)
		{
			throw new AdapterException(ex.getMessage(), ex);
		}
	}

	@Override
	public WileyVerifyResponse verify(final WileyVerifyRequest request)
	{
		ServicesUtil.validateParameterNotNull(request, "[request] cannot be null.)");
		try
		{
			WileyVerifyCommand command = getCommandFactoryRegistry().getFactory(request.getPaymentProvider()).createCommand(
					WileyVerifyCommand.class);
			return command.perform(request);
		}
		catch (CommandNotSupportedException ex)
		{
			throw new AdapterException(ex.getMessage(), ex);
		}
	}

	@Override
	public WileyTokenizationResponse tokenize(@Nonnull final WileyTokenizationRequest request)
	{
		ServicesUtil.validateParameterNotNull(request, "[request] cannot be null.)");
		try
		{
			WileyTokenizationCommand command = getCommandFactoryRegistry().getFactory(
					request.getPaymentProvider()).createCommand(
					WileyTokenizationCommand.class);
			return command.perform(request);
		}
		catch (CommandNotSupportedException ex)
		{
			throw new AdapterException(ex.getMessage(), ex);
		}
	}

	@Override
	public WileyAuthorizationResponse authorize(final WileyAuthorizationRequest request)
	{
		ServicesUtil.validateParameterNotNull(request, "[request] cannot be null.)");
		try
		{
			WileyAuthorizationCommand command = getCommandFactoryRegistry().getFactory(request.getPaymentProvider())
					.createCommand(WileyAuthorizationCommand.class);
			return command.perform(request);
		}
		catch (CommandNotSupportedException ex)
		{
			throw new AdapterException(ex.getMessage(), ex);
		}
	}

	@Override
	public WileyCaptureResponse capture(@Nonnull final WileyCaptureRequest request)
	{
		ServicesUtil.validateParameterNotNull(request, "[request] cannot be null.)");
		try
		{
			WileyCaptureCommand command = getCommandFactoryRegistry().getFactory(request.getPaymentProvider()).createCommand(
					WileyCaptureCommand.class);
			return command.perform(request);
		}
		catch (CommandNotSupportedException ex)
		{
			throw new AdapterException(ex.getMessage(), ex);
		}
	}

	@Override
	public WileyFollowOnRefundResponse refundFollowOn(final WileyFollowOnRefundRequest request)
	{
		ServicesUtil.validateParameterNotNull(request, "[request] cannot be null.)");
		try
		{
			WileyFollowOnRefundCommand command = getCommandFactoryRegistry().getFactory(
					request.getPaymentProvider()).createCommand(WileyFollowOnRefundCommand.class);
			return command.perform(request);
		}
		catch (CommandNotSupportedException ex)
		{
			throw new AdapterException(ex.getMessage(), ex);
		}
	}
}
