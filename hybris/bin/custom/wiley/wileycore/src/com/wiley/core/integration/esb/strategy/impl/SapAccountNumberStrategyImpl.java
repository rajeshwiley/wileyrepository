package com.wiley.core.integration.esb.strategy.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BUnitService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import com.wiley.core.integration.esb.strategy.SapAccountNumberStrategy;

import org.springframework.util.Assert;


/**
 * Default implementation of {@link SapAccountNumberStrategy}
 */
public class SapAccountNumberStrategyImpl implements SapAccountNumberStrategy
{
	@Resource
	private B2BUnitService<B2BUnitModel, B2BCustomerModel> b2bUnitService;

	@Override
	@Nonnull
	public String getSapAccountNumber(@Nonnull final CartModel cartModel)
	{
		Assert.notNull(cartModel);

		final UserModel user = cartModel.getUser();
		Assert.notNull(user);

		String sapAccountNumber = null;
		if (user instanceof B2BCustomerModel)
		{
			final B2BUnitModel defaultB2BUnit = b2bUnitService.getParent((B2BCustomerModel) user);
			if (defaultB2BUnit != null)
			{
				sapAccountNumber = defaultB2BUnit.getSapAccountNumber();
			}
		}

		Assert.notNull(sapAccountNumber);
		return sapAccountNumber;
	}
}
