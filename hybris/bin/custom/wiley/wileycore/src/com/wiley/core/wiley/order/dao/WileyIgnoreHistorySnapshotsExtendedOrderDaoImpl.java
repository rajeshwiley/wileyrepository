package com.wiley.core.wiley.order.dao;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ruleengineservices.order.dao.impl.DefaultExtendedOrderDao;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;

import java.util.HashMap;
import java.util.Map;


/**
 * Custom DAO to avoid ambiguous identifier exception for orders with history snapshots with the same order code.
 */
public class WileyIgnoreHistorySnapshotsExtendedOrderDaoImpl extends DefaultExtendedOrderDao
{
	@Override
	public AbstractOrderModel findOrderByCode(final String code)
	{
		AbstractOrderModel result;
		try
		{
			result = super.findOrderByCode(code);
		}
		catch (AmbiguousIdentifierException e)
		{
			Map<String, Object> queryParams = new HashMap();
			queryParams.put("code", code);
			result = (OrderModel) this.getFlexibleSearchService().searchUnique(new FlexibleSearchQuery(
					"SELECT {pk}, {creationtime}, {code} FROM {Order} WHERE {code} = ?code "
							+ "AND {" + OrderModel.VERSIONID + "} IS NULL ", queryParams));
		}
		return result;
	}
}
