package com.wiley.core.order.strategies;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.order.WileyOrderFulfilmentProcessService;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.processengine.enums.ProcessState;


public abstract class AbstractOrderModificationStrategy
{
	@Autowired
	private WileyOrderFulfilmentProcessService wileyOrderFulfilmentProcessService;


	protected boolean hasAllEntriesInState(final OrderModel order, final Set<OrderStatus> states)
	{
		return order.getEntries().stream().allMatch(entry -> states.contains(entry.getStatus()));
	}

	protected boolean hasActiveFulfillmentProcess(final OrderModel orderModel)
	{
		return wileyOrderFulfilmentProcessService.existsProcessActiveFor(orderModel);
	}

	protected boolean hasFailedFulfillmentProcess(final OrderModel orderModel)
	{
		return wileyOrderFulfilmentProcessService.getOrderProcessInStateForOrder(orderModel, ProcessState.FAILED).isPresent();
	}

	protected boolean isCorrectSourceSystem(final OrderModel orderModel)
	{
		return WileyCoreConstants.SOURCE_SYSTEM_HYBRIS.equals(orderModel.getSourceSystem());
	}
}
