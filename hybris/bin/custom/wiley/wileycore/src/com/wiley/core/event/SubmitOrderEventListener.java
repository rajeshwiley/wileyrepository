/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.core.event;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.event.AbstractSiteEventListener;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.events.SubmitOrderEvent;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.wiley.core.order.WileyOrderFulfilmentProcessService;


/**
 * Listener for order submits.
 */
public class SubmitOrderEventListener extends AbstractSiteEventListener<SubmitOrderEvent>
{
	private static final Logger LOG = Logger.getLogger(SubmitOrderEventListener.class);
	private static final boolean SHOULD_START_PROCESS = true;

	@Resource
	private WileyOrderFulfilmentProcessService wileyOrderFulfilmentProcessService;


	@Override
	protected void onSiteEvent(final SubmitOrderEvent event)
	{
		final OrderModel order = event.getOrder();
		ServicesUtil.validateParameterNotNullStandardMessage("event.order", order);
		wileyOrderFulfilmentProcessService.startFulfilmentProcessForOrder(order, SHOULD_START_PROCESS);
	}

	@Override
	protected boolean shouldHandleEvent(final SubmitOrderEvent event)
	{
		final OrderModel order = event.getOrder();
		ServicesUtil.validateParameterNotNullStandardMessage("event.order", order);
		final BaseSiteModel site = order.getSite();
		ServicesUtil.validateParameterNotNullStandardMessage("event.order.site", site);
		return true;
	}

}
