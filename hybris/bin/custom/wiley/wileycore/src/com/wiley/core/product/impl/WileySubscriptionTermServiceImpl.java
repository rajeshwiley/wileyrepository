package com.wiley.core.product.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.subscriptionservices.model.SubscriptionTermModel;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;

import com.google.common.base.Preconditions;
import com.wiley.core.enums.WileyProductSubtypeEnum;
import com.wiley.core.product.WileySubscriptionTermService;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateIfSingleResult;
import static java.lang.String.format;


public class WileySubscriptionTermServiceImpl implements WileySubscriptionTermService
{
	@Resource
	private DefaultGenericDao<SubscriptionTermModel> subscriptionTermGenericDao;

	/**
	 * @throws IllegalStateException
	 * 		if {@code id} is null or empty
	 * @throws UnknownIdentifierException
	 * 		if service doesn't find subscription term
	 * @throws AmbiguousIdentifierException
	 * 		if serivce finds more than one subscription term
	 */
	@Override
	public SubscriptionTermModel getSubscriptionTerm(final String id)
	{
		Preconditions.checkState(StringUtils.isNotEmpty(id), "Parameter id should not be null or empty");

		List<SubscriptionTermModel> results = subscriptionTermGenericDao.find(
				Collections.singletonMap(SubscriptionTermModel.ID, id));


		validateIfSingleResult(results, format("Subscription term with id ['%s'] was not found!", id),
				format("Subscription term with id ['%s'] is not unique, [%d] subscription terms found!", id,
						Integer.valueOf(results.size())));

		return results.get(0);
	}

	@Override
	public Optional<SubscriptionTermModel> findSubscriptionTerm(final ProductModel product, final String subscriptionTermId)
	{
		final String isbn = product.getIsbn();
		final Optional<SubscriptionTermModel> subscriptionTerm;
		if (isSubscriptionProduct(product))
		{
			if (StringUtils.isEmpty(subscriptionTermId))
			{
				throw new IllegalArgumentException(
						format("Subscription term should be provided for subscription product with ISBN: [%s]", isbn));
			}

			final SubscriptionTermModel subscriptionTermModel = getSubscriptionTerm(subscriptionTermId);
			subscriptionTerm = Optional.of(subscriptionTermModel);
		}
		else
		{
			subscriptionTerm = Optional.empty();
		}
		return subscriptionTerm;
	}


	public boolean isSubscriptionProduct(@Nonnull final ProductModel product)
	{
		return WileyProductSubtypeEnum.SUBSCRIPTION.equals(product.getSubtype());
	}

}
