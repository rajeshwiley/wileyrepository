package com.wiley.core.integration.cdm;

import com.wiley.core.integration.cdm.dto.CDMCreateUserResponse;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.user.CustomerModel;

import javax.annotation.Nonnull;


/**
 * The interface for connection with CDM.
 */
public interface CDMGateway 
{
	/**
     *
     * @param customer
     * @param site
     *  site from order, because there is no site in session for business process
     * @return
     */
    @Nonnull
    CDMCreateUserResponse createCDMCustomer(@Nonnull CustomerModel customer, @Nonnull BaseSiteModel site);
}
