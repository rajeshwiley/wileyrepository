package com.wiley.core.partner;

import java.util.List;

import com.wiley.core.model.KpmgOfficeModel;


/**
 * KPMG Enrollment Service
 */
public interface WileyKPMGEnrollmentService
{
	/**
	 *
	 * @param partnerId identifier of the Wiley Partner Company
	 * @return the list of KPMG Offices associate with given partner company
	 * @throws IllegalArgumentException if partnerId is null
	 */
	List<KpmgOfficeModel> getOffices(String partnerId);

	/**
	 *
	 * @param code
	 * @return KPMG Office or null
	 */
	KpmgOfficeModel getOfficeByCode(String code);
}
