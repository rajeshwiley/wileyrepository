package com.wiley.core.payment.strategies;


public interface WPGVendorIdStrategy
{
	Integer getVendorId(String site);
}
