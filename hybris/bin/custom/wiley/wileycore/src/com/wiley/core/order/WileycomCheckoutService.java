package com.wiley.core.order;

import de.hybris.platform.core.model.order.OrderModel;

import java.util.Optional;


public interface WileycomCheckoutService extends WileyCheckoutService
{
	/**
	 * Returns the last order (first order from order list sorted by date in descending order) for the given customer.
	 *
	 * @param customerUid Uid of the given customer
	 */
	Optional<OrderModel> getLastOrderForCustomer(String customerUid);
}
