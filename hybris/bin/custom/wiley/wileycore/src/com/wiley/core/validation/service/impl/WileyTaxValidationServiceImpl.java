package com.wiley.core.validation.service.impl;

import de.hybris.platform.core.model.c2l.CountryModel;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wiley.core.exceptions.WileyVatTaxNumberNotValidException;
import com.wiley.core.integration.vies.ViesCheckVatGateway;
import com.wiley.core.integration.vies.dto.CheckVatResponseDto;
import com.wiley.core.validation.service.WileyTaxValidationService;


/**
 * This service contains a tax validation logic
 */
public class WileyTaxValidationServiceImpl implements WileyTaxValidationService
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyTaxValidationServiceImpl.class);

	@Resource
	private ViesCheckVatGateway viesCheckVatGateway;

	@Override
	public boolean validateTaxInformation(final CountryModel country, final String taxNumber)
	{
		boolean result = false;
		if (country.getApplicableTaxExemption() != null && StringUtils.isNotEmpty(taxNumber))
		{
			switch (country.getApplicableTaxExemption())
			{
				case VAT:
					result = validateTaxNumberViaVies(country, taxNumber);
					break;
				default:
			}
		}
		return result;
	}

	private boolean validateTaxNumberViaVies(final CountryModel country, final String taxNumber)
	{
		try
		{
			final CheckVatResponseDto checkVatResponseDto = viesCheckVatGateway.checkVat(country.getIsocode(), taxNumber);

			if (!checkVatResponseDto.getValid())
			{
				throw new WileyVatTaxNumberNotValidException();
			}
			else
			{
				// Invalid VAT number is not saved in cart. So mark tax number validated only if tax number is correct.
				return true;
			}

		}
		catch (WileyVatTaxNumberNotValidException e)
		{
			// throw exception to controller to reject field value
			throw e;
		}
		catch (Exception e)
		{
			LOG.warn("Exception while validating VAT in VIES", e);
			return false;
		}
	}
}
