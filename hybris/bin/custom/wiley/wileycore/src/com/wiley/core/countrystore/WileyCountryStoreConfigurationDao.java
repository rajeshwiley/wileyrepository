package com.wiley.core.countrystore;

import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.List;

import com.wiley.core.model.WileyCountryStoreConfigurationModel;


public interface WileyCountryStoreConfigurationDao
{
	List<WileyCountryStoreConfigurationModel> findConfigurationByCountryAndBasestore(CountryModel countryModel,
			BaseStoreModel baseStoreModel);
}
