package com.wiley.core.wileycom.media;

import javax.annotation.Nonnull;

import org.springframework.messaging.handler.annotation.Payload;


/**
 * @author Dzmitryi_Halahayeu
 */
public interface WileycomGetMediaGateway
{

	/**
	 * Get URL to media
	 *
	 * @param isbn
	 * 		isbn13-digit
	 * @return URL to media
	 */
	@Nonnull
	String getURL(@Nonnull @Payload String isbn);

}
