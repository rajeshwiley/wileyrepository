package com.wiley.core.wileyb2c.weblinks.providers.impl;

import org.apache.commons.lang.Validate;

import com.wiley.core.model.WileyWebLinkModel;
import com.wiley.core.wileyb2c.weblinks.providers.WileyWebLinkUrlProvider;


/**
 * Simple URL provider that expects url to be the first parameter of given link
 */
public class DefaultWileyWebLinkUrlProvider implements WileyWebLinkUrlProvider
{
	@Override
	public String getWebLinkUrl(final WileyWebLinkModel webLink)
	{
		Validate.notNull(webLink);
		Validate.notEmpty(webLink.getParameters());
		return webLink.getParameters().get(0);
	}
}
