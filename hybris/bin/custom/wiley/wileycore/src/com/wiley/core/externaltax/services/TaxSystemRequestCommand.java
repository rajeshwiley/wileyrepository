package com.wiley.core.externaltax.services;

import javax.annotation.Nonnull;

import com.wiley.core.externaltax.services.impl.TaxSystemException;
import com.wiley.core.externaltax.services.impl.TaxSystemResponseError;
import com.wiley.core.externaltax.xml.parsing.dto.TaxServiceRequest;


/**
 * Encapsulates a request to external tax system as an object
 */
public interface TaxSystemRequestCommand
{
	/**
	 * Performs request to external tax system using provided {@link TaxServiceRequest}
	 *
	 * @param taxServiceRequest
	 * 		data transfer object for request taxes
	 * @param orderCode
	 * 		String order code
	 * @param entryNumber
	 * 		int entry number
	 * @return Double tax amount as a response from external tax system
	 * @throws TaxSystemException
	 * 		if some error occurs during tax calculation
	 * @throws TaxSystemResponseError
	 * 		if external tax calculation system responses with error due to invalid user input
	 */
	@Nonnull
	Double execute(TaxServiceRequest taxServiceRequest, String orderCode, int entryNumber);
}
