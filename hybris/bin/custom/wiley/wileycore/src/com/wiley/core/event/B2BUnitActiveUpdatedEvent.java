package com.wiley.core.event;

import de.hybris.platform.servicelayer.event.ClusterAwareEvent;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;


/**
 * Type of event when B2BUnit active status is changed
 */
public class B2BUnitActiveUpdatedEvent extends AbstractEvent implements ClusterAwareEvent
{
	private String sapAccountNumber;

	public B2BUnitActiveUpdatedEvent(final String sapAccountNumber)
	{
		this.sapAccountNumber = sapAccountNumber;
	}

	public String getSapAccountNumber()
	{
		return sapAccountNumber;
	}

	public void setSapAccountNumber(final String sapAccountNumber)
	{
		this.sapAccountNumber = sapAccountNumber;
	}

	@Override
	public boolean publish(final int sourceNodeId, final int targetNodeId)
	{
		return true;
	}
}
