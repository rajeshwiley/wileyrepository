package com.wiley.core.validation.constraintvalidators;

import de.hybris.platform.core.Registry;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.wiley.core.i18n.WileyCommonI18NService;
import com.wiley.core.validation.constraints.ValidLocale;

public class ValidLocaleValidator implements ConstraintValidator<ValidLocale, String>
{
	private WileyCommonI18NService wileyCommonI18NService;

	@Override
	public void initialize(final ValidLocale validLocale)
	{
		wileyCommonI18NService = Registry.getApplicationContext().getBean(WileyCommonI18NService.class);
	}

	@Override
	public boolean isValid(final String value, final ConstraintValidatorContext constraintValidatorContext)
	{
		return wileyCommonI18NService.isValidLanguage(value);
	}

}
