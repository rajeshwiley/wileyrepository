package com.wiley.core.price.dao;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.servicelayer.internal.dao.GenericDao;

import java.util.List;


public interface WileyPriceDao extends GenericDao<PriceRowModel>
{
	/**
	 * Finds product prices list
	 *
	 * @param productModel
	 * 		product
	 * @param currencyModel
	 * 		currency model
	 * @return product price information
	 */
	List<PriceRowModel> findPricesByProduct(ProductModel productModel, CurrencyModel currencyModel);

	/**
	 * @param price
	 * @return
	 */
	List<PriceRowModel> findPrices(PriceRowModel price);
}
