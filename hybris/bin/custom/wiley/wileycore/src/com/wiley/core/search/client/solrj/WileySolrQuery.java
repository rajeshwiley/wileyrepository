package com.wiley.core.search.client.solrj;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.solr.client.solrj.SolrQuery;
import org.codehaus.jackson.map.ObjectMapper;


/**
 * SOLR query that substitute old facet API to new JSON Facet API
 */
public class WileySolrQuery extends SolrQuery
{
	public static final String JSON_PARAM_KEY = "json";

	private JsonParamSwg jsonParam = new JsonParamSwg();

	public WileySolrQuery()
	{
	}

	public WileySolrQuery(final String q)
	{
		super(q);
	}

	public WileySolrQuery(final String k, final String v, final String... params)
	{
		super(k, v, params);

	}

	public JsonParamSwg getJsonParam()
	{
		return jsonParam;
	}

	public void setJsonParam(final JsonParamSwg jsonParam)
	{
		this.jsonParam = jsonParam;
	}

	public SolrQuery setFacetSort(final String sort)
	{
		jsonParam.getFacet().setSort(sort);
		return this;
	}

	public Iterator<String> getParameterNamesIterator()
	{
		return this.getParameterNames().iterator();
	}

	@Override
	public Set<String> getParameterNames()
	{
		Set<String> paramNames = new HashSet<>(super.getParameterNames());
		paramNames.add(JSON_PARAM_KEY);
		return paramNames;
	}

	@Override
	public String[] getParams(final String param)
	{
		if (JSON_PARAM_KEY.equals(param))
		{
			return new String[] { createJSONFromObject(jsonParam) };
		}
		return super.getParams(param);
	}

	public String createJSONFromObject(final JsonParamSwg jsonParam)
	{
		try
		{
			final ObjectMapper mapper = new ObjectMapper();
			return mapper.writeValueAsString(jsonParam);

		}
		catch (IOException e)
		{
			throw new IllegalArgumentException(e.getMessage(), e);
		}
	}

	@Override
	public Iterator<Map.Entry<String, String[]>> iterator()
	{
		final Iterator<String> it = this.getParameterNamesIterator();
		return new Iterator<Map.Entry<String, String[]>>()
		{
			public boolean hasNext()
			{
				return it.hasNext();
			}

			public Map.Entry<String, String[]> next()
			{
				final String key = it.next();
				return new Map.Entry<String, String[]>()
				{
					public String getKey()
					{
						return key;
					}

					public String[] getValue()
					{
						return getParams(key);
					}

					public String[] setValue(final String[] newValue)
					{
						throw new UnsupportedOperationException("read-only");
					}

					public String toString()
					{
						return this.getKey() + "=" + Arrays.toString(this.getValue());
					}
				};
			}
		};
	}
}
