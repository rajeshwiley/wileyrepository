package com.wiley.core.integration.sabrix.service.impl;

import de.hybris.platform.commerceservices.price.CommercePriceService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;

import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;

import com.wiley.core.integration.sabrix.service.SetPriceSeparatorService;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


/**
 * Author Herman_Chukhrai (EPAM)
 */
public class SetPriceSeparatorServiceImpl implements SetPriceSeparatorService
{
	@Resource
	private CommercePriceService commercePriceService;

	@Override
	public Map<ProductModel, Double> splitSetPriceOverProducts(@NotNull final Collection<ProductModel> products,
			@NotNull final Double setPrice)
	{
		validateParameterNotNullStandardMessage("products", products);
		validateParameterNotNullStandardMessage("setPrice", setPrice);

		final Map<ProductModel, Double> productPriceMap = products.stream().collect(
				Collectors.toMap(productModel -> productModel, productModel1 -> {
					final PriceInformation priceForProduct = commercePriceService.getFromPriceForProduct(productModel1);
					return priceForProduct != null ? priceForProduct.getPriceValue().getValue() : 0d;
				}));


		return updateProductPrice(productPriceMap, setPrice);
	}

	private static Map<ProductModel, Double> updateProductPrice(final Map<ProductModel, Double> productPriceMap,
			final Double setPrice)
	{
		final double pricesSum = productPriceMap.entrySet().stream().mapToDouble(Map.Entry::getValue).sum();
		productPriceMap.replaceAll((product, price) -> pricesSum > 0 ? price * setPrice / pricesSum : 0);
		return productPriceMap;
	}
}
