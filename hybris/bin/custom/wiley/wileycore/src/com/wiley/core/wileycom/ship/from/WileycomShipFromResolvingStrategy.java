package com.wiley.core.wileycom.ship.from;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;

import javax.annotation.Nonnull;


/**
 * @author Dzmitryi_Halahayeu
 */
public interface WileycomShipFromResolvingStrategy
{
	AddressModel resolveShipFromAddress(@Nonnull ProductModel product);
}
