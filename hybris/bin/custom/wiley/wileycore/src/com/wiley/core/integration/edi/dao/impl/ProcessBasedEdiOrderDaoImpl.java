package com.wiley.core.integration.edi.dao.impl;

import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.store.BaseStoreModel;

import java.time.Instant;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.google.common.collect.Maps;
import com.paypal.hybris.model.PaypalPaymentInfoModel;
import com.wiley.core.integration.edi.EDIConstants;
import com.wiley.core.integration.edi.dao.EdiOrderDao;
import com.wiley.core.util.WileyDateUtils;
import com.wiley.fulfilmentprocess.model.WileyExportProcessModel;


public class ProcessBasedEdiOrderDaoImpl implements EdiOrderDao
{
	@Resource
	private FlexibleSearchService flexibleSearchService;
	@Resource
	private ConfigurationService configurationService;

	@Override
	public List<WileyExportProcessModel> getExportProcessesReadyForExport(final BaseStoreModel store, final String paymentType)
	{
		final String exportedFlagField = WileyExportProcessModel.DONE;
		return getExportProcesses(store, paymentType, exportedFlagField);
	}

	@Override
	public List<WileyExportProcessModel> getExportProcessesReadyForDailyReportExport(final BaseStoreModel store,
			final String paymentType)
	{
		final String exportedFlagField = WileyExportProcessModel.DAILYREPORTSENT;
		return getExportProcesses(store, paymentType, exportedFlagField);
	}

	@Override
	public List<WileyExportProcessModel> getZeroDollarPhysicalOrderProcessesReadyForExport(final BaseStoreModel store)
	{
		final String exportedFlagField = WileyExportProcessModel.DONE;
		return getExportZeroDollarPhysicalOrderProcesses(store, exportedFlagField);
	}

	/**
	 * Define actual type of payment info for orders from OrderProcess
	 *
	 * @param paymentType
	 * 		- payment type as search criteria
	 * @return - String representation of model class of payment type we are filtering
	 */
	private String definePaymentInfoTypeByPaymentType(final String paymentType)
	{
		switch (paymentType)
		{
			case EDIConstants.PAYMENT_TYPE_PAYPAL:
				return PaypalPaymentInfoModel._TYPECODE;
			case EDIConstants.PAYMENT_TYPE_CREDIT_CARD:
				return CreditCardPaymentInfoModel._TYPECODE;
			default:
				throw new IllegalArgumentException("IncorrectPaymentType provided for EDI export job");
		}
	}

	private Date getGrabOrdersBeforeDate(final String paymentType)
	{
		final String grabOrdersTimePropertyName = String.format(EDIConstants.EDI_GRAB_ORDERS_TIME_PROPERTY_PATTERN,
				paymentType.toLowerCase());
		String grabOrdersTime = configurationService.getConfiguration().getString(grabOrdersTimePropertyName,
				EDIConstants.EDI_DEFAULT_GRAB_ORDERS_TIME);

		LocalTime configuredTime = LocalTime.parse(grabOrdersTime, DateTimeFormatter.ofPattern("HH:mm:ss"));
		Instant instant = WileyDateUtils.getClosestPastLocalDateTime(configuredTime).atZone(ZoneId.systemDefault()).toInstant();
		return Date.from(instant);
	}

	private List<WileyExportProcessModel> getExportProcesses(final BaseStoreModel store, final String paymentType,
			final String exportedFlagField)
	{
		boolean grabAllPendingOrders = configurationService.getConfiguration().getBoolean(
				EDIConstants.EDI_GRAB_ALL_PENDING_ORDERS_PROPERTY, false);
		Map<String, Object> params = Maps.newHashMap();
		params.put("store", store.getUid());

		String paymentInfoType = definePaymentInfoTypeByPaymentType(paymentType);
		String selectOrdersReadyForExportQuery =
				"SELECT {wep:PK} " + "FROM {WileyExportProcess as wep "
						+ "LEFT JOIN Order AS o ON {wep:order} = {o:PK} "
						+ "LEFT JOIN BaseStore AS bs ON {o:store} = {bs:PK} "
						+ String.format("LEFT JOIN %s! AS pi ON {o:paymentInfo} = {pi:PK}} ", paymentInfoType)
						+ "WHERE {wep:systemCode}  = 'edi' "
						+ "AND {wep:" + exportedFlagField + "} != true "
						+ "AND {bs:uid} = ?store AND  {pi:code} is NOT NULL";
		if (!grabAllPendingOrders)
		{
			selectOrdersReadyForExportQuery += " AND {wep.transactionDate} < ?grabOrdersBeforeDate";
			params.put("grabOrdersBeforeDate", getGrabOrdersBeforeDate(paymentType));
		}

		final FlexibleSearchQuery query = new FlexibleSearchQuery(selectOrdersReadyForExportQuery, params);
		return flexibleSearchService.<WileyExportProcessModel> search(query).getResult();
	}

	private List<WileyExportProcessModel> getExportZeroDollarPhysicalOrderProcesses(final BaseStoreModel store,
			final String exportedFlagField)
	{
		Map<String, Object> params = Maps.newHashMap();
		params.put("store", store.getUid());

		String selectOrdersReadyForExportQuery =
				"SELECT {wep:PK} " + "FROM {WileyExportProcess as wep "
						+ "JOIN Order AS o ON {wep:order} = {o:PK} "
						+ "JOIN BaseStore AS bs ON {o:store} = {bs:PK}} "
						+ "WHERE {wep:systemCode}  = 'edi' "
						+ "AND {wep:" + exportedFlagField + "} != true "
						+ "AND {bs:uid} = ?store AND  {o:paymentInfo} is NULL";

		final FlexibleSearchQuery query = new FlexibleSearchQuery(selectOrdersReadyForExportQuery, params);
		return flexibleSearchService.<WileyExportProcessModel> search(query).getResult();
	}
}
