package com.wiley.core.servicelayer.cronjob.impl;

import de.hybris.platform.core.PK;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.impl.DefaultCronJobService;

import com.wiley.core.servicelayer.cronjob.WileyCronJobService;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public class WileyCronJobServiceImpl extends DefaultCronJobService implements WileyCronJobService
{
	@Override
	public CronJobModel getCronJobByPk(final PK pk)
	{
		Object model;
		try
		{
			model = getModelService().get(pk);
		}
		catch (Exception ex)
		{
			return null;
		}
		if (model instanceof CronJobModel)
		{
			return (CronJobModel) model;
		}
		return null;
	}

	@Override
	public CronJobModel getCronJobByPk(final Long pk)
	{
		return getCronJobByPk(PK.fromLong(pk));
	}
}
