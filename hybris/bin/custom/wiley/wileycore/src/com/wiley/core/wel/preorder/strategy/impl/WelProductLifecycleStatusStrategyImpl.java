package com.wiley.core.wel.preorder.strategy.impl;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.variants.model.VariantProductModel;

import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.category.WileyCategoryService;
import com.wiley.core.enums.WileyProductLifecycleEnum;
import com.wiley.core.wel.preorder.strategy.ProductLifecycleStatusStrategy;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

public class WelProductLifecycleStatusStrategyImpl implements ProductLifecycleStatusStrategy
{
	private ProductService productService;

	private WileyCategoryService wileyCategoryService;

	@Override
	public WileyProductLifecycleEnum getLifecycleStatusForProduct(final ProductModel product)
	{
		validateParameterNotNull(product, "product must not be null!");

		final CategoryModel primaryCategoryForProduct = wileyCategoryService.getPrimaryWileyCategoryForProduct(product);

		WileyProductLifecycleEnum lifecycleStatus = null;

		if (!wileyCategoryService.isCFACategory(primaryCategoryForProduct) && product instanceof VariantProductModel
				&& ((VariantProductModel) product).getBaseProduct() != null)
		{
			lifecycleStatus = ((VariantProductModel) product).getBaseProduct().getLifecycleStatus();
		}
		else
		{
			lifecycleStatus = product.getLifecycleStatus();
		}
		return lifecycleStatus;
	}


	@Override
	public WileyProductLifecycleEnum getLifecycleStatusForProduct(final String productCode)
	{
		validateParameterNotNull(productCode, "productCode must not be null!");
		final ProductModel product = productService.getProductForCode(productCode);
		return getLifecycleStatusForProduct(product);
	}


	@Required
	public void setProductService(final ProductService productService)
	{
		this.productService = productService;
	}

	@Required
	public void setWileyCategoryService(final WileyCategoryService wileyCategoryService)
	{
		this.wileyCategoryService = wileyCategoryService;
	}
}
