package com.wiley.core.payment.populators;

import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionRequest;
import de.hybris.platform.acceleratorservices.payment.data.PaymentData;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.Map;

import static com.wiley.core.payment.WileyHttpRequestParams.WPG_ADDRESS;
import static com.wiley.core.payment.WileyHttpRequestParams.WPG_COUNTRY_CODE;
import static com.wiley.core.payment.WileyHttpRequestParams.WPG_DESCRIPTION;
import static com.wiley.core.payment.WileyHttpRequestParams.WPG_METHOD;
import static com.wiley.core.payment.WileyHttpRequestParams.WPG_POSTCODE;
import static com.wiley.core.payment.WileyHttpRequestParams.WPG_REGION;
import static com.wiley.core.payment.WileyHttpRequestParams.WPG_SECURITY;
import static com.wiley.core.payment.WileyHttpRequestParams.WPG_TIMESTAMP;
import static com.wiley.core.payment.WileyHttpRequestParams.WPG_TRANSACTION_ID;
import static com.wiley.core.payment.WileyHttpRequestParams.WPG_VENDOR_ID;

public abstract class WPGHttpValidateRequestPopulator extends WPGHttpRequestPopulator
{
	@Override
	public void populate(final CreateSubscriptionRequest source, final PaymentData target) throws ConversionException
	{
		super.populate(source, target);
		addRequestQueryParam(target, WPG_SECURITY, createSecurityHashCode(target.getParameters()));
	}

	@Override
	protected String createSecurityHashCode(final Map<String, String> parameters)
	{
		StringBuilder wpgSecuritySource = new StringBuilder();
		wpgSecuritySource.append(parameters.get(WPG_TIMESTAMP));
		wpgSecuritySource.append(parameters.get(WPG_VENDOR_ID));
		wpgSecuritySource.append(parameters.get(WPG_TRANSACTION_ID));
		wpgSecuritySource.append(parameters.get(WPG_METHOD));
		wpgSecuritySource.append(parameters.get(WPG_DESCRIPTION));
		wpgSecuritySource.append(parameters.get(WPG_REGION));
		wpgSecuritySource.append(parameters.get(WPG_ADDRESS));
		wpgSecuritySource.append(parameters.get(WPG_POSTCODE));
		wpgSecuritySource.append(parameters.get(WPG_COUNTRY_CODE));

		return getSecurityHashGeneratorStrategy().generateSecurityHash(getSiteId(), wpgSecuritySource.toString());
	}
}
