package com.wiley.core.interceptors;

import de.hybris.platform.servicelayer.cronjob.impl.TriggerValidateInterceptor;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import com.wiley.core.model.EtTriggerModel;


public class WileyTriggerValidateInterceptor extends TriggerValidateInterceptor
{
	@Override
	public void onValidate(final Object triggerModel, final InterceptorContext ctx) throws InterceptorException
	{
		if (!(triggerModel instanceof EtTriggerModel))
		{
			super.onValidate(triggerModel, ctx);
		}
	}
}
