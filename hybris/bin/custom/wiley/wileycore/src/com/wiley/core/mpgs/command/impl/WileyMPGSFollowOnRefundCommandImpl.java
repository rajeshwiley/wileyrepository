package com.wiley.core.mpgs.command.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import com.wiley.core.integration.mpgs.WileyMPGSPaymentGateway;
import com.wiley.core.mpgs.command.WileyFollowOnRefundCommand;
import com.wiley.core.mpgs.response.WileyFollowOnRefundResponse;
import com.wiley.core.payment.request.WileyFollowOnRefundRequest;


public class WileyMPGSFollowOnRefundCommandImpl implements WileyFollowOnRefundCommand
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyMPGSFollowOnRefundCommandImpl.class);

	@Autowired
	private WileyMPGSPaymentGateway wileyMpgsPaymentGateway;

	@Override
	public WileyFollowOnRefundResponse perform(final WileyFollowOnRefundRequest request)
	{
		WileyFollowOnRefundResponse response = null;
		try
		{
			response = wileyMpgsPaymentGateway.refundFollowOn(request);
		}
		catch (HttpClientErrorException | HttpServerErrorException ex)
		{
			LOG.error("Unable to refund due to MPGS error " + ex.getResponseBodyAsString(), ex);
			response = createErrorResponse(WileyFollowOnRefundResponse::new);
		}
		catch (final Exception ex)
		{
			LOG.error("Unable to refund due to unexpected error", ex);
			response = createHybrisExceptionResponse(WileyFollowOnRefundResponse::new);
		}
		return response;
	}

}