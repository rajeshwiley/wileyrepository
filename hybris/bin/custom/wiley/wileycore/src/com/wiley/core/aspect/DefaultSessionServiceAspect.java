package com.wiley.core.aspect;

import com.google.common.base.Preconditions;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

import java.io.Serializable;

@Aspect
public class DefaultSessionServiceAspect
{

	/**
	 * Prevents to store non-serializable attribute in session. Session needs to contain only serializable objects in
	 * order to replicate session across nodes
	 * @param joinPoint
	 */
	@Before("execution(* de.hybris.platform.servicelayer.session.impl.DefaultSessionService.setAttribute(..))")
	public void setAttribute(final JoinPoint joinPoint)
	{
		String name = (String) joinPoint.getArgs()[0];
		Object value = joinPoint.getArgs()[1];
		if (value != null)
		{
			Preconditions.checkArgument(value instanceof Serializable,
					"Attempt to store not serializable session attribute for key:"
							+ name + " value class: " + value.getClass());
		}
	}
}
