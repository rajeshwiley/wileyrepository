package com.wiley.core.wiley.restriction;

import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.product.ProductModel;

import javax.annotation.Nonnull;

import com.wiley.core.wiley.restriction.dto.WileyRestrictionCheckResultDto;


/**
 * Contains logic according to verification of product visibility and availability.
 * If product isn't available it's impossible to proceed to checkout with it. Visibility extends availability.
 * If product isn't visible it's impossible to proceed to checkout with it, find it in PLP and see it in PDP.
 * This service is called on before cart calculation hook and should contain only quick and simple
 * validation. We shouldn't use this service for expensive validation with external calls.
 * Use {@link com.wiley.core.wiley.order.impl.WileyCartValidationStrategy} for expensive validation instead.
 *
 * @author Dzmitryi_Halahayeu
 */
public interface WileyProductRestrictionService
{

	@Nonnull
	WileyRestrictionCheckResultDto isAvailable(@Nonnull CommerceCartParameter parameter);

	@Nonnull
	WileyRestrictionCheckResultDto isVisible(@Nonnull CommerceCartParameter parameter);

	@Nonnull
	boolean isSearchable(@Nonnull ProductModel productModel);

	@Nonnull
	boolean isAvailable(@Nonnull ProductModel product);

	@Nonnull
	boolean isVisible(@Nonnull ProductModel product);

	ProductModel filterRestrictedProductVariants(ProductModel product);
}
