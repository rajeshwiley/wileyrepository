package com.wiley.core.mpgs.dto.token;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.wiley.core.mpgs.dto.json.Error;
import com.wiley.core.mpgs.dto.json.Response;
import com.wiley.core.mpgs.dto.json.SourceOfFunds;



@JsonIgnoreProperties(ignoreUnknown = true)
public class MPGSTokenResponseDTO
{

	private String token;
	private SourceOfFunds sourceOfFunds;
	private Response response;
	private String result;
	private Error error;

	public Error getError()
	{
		return error;
	}

	public void setError(final Error error)
	{
		this.error = error;
	}

	public String getToken()
	{
		return token;
	}

	public void setToken(final String token)
	{
		this.token = token;
	}

	public SourceOfFunds getSourceOfFunds()
	{
		return sourceOfFunds;
	}

	public void setSourceOfFunds(final SourceOfFunds sourceOfFunds)
	{
		this.sourceOfFunds = sourceOfFunds;
	}

	public Response getResponse()
	{
		return response;
	}

	public void setResponse(final Response response)
	{
		this.response = response;
	}

	public String getResult()
	{
		return result;
	}

	public void setResult(final String result)
	{
		this.result = result;
	}
}
