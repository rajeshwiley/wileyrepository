package com.wiley.core.integration.sabrix.dto;

import com.google.common.base.MoreObjects;


public class Wileyb2cTaxResponse
{
	private Double orderTotalTax;

	public Double getOrderTotalTax()
	{
		return orderTotalTax;
	}

	public void setOrderTotalTax(final Double orderTotalTax)
	{
		this.orderTotalTax = orderTotalTax;
	}


	@Override
	public String toString()
	{
		return MoreObjects.toStringHelper(this)
				.add("orderTotalTax", orderTotalTax)
				.toString();
	}
}


