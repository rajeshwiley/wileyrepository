package com.wiley.core.product;

import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.List;

import com.wiley.core.model.WileyProductVariantSetModel;


/**
 * @author Aliaksei_Zlobich
 */
public class WileyProductVariantSetInformation
{

	private List<GenericVariantProductModel> includedProducts;

	private WileyProductVariantSetModel productSet;


	public void setIncludedProducts(final List<GenericVariantProductModel> includedProducts)
	{
		this.includedProducts = includedProducts;
	}


	public List<GenericVariantProductModel> getIncludedProducts()
	{
		return includedProducts;
	}


	public void setProductSet(final WileyProductVariantSetModel productSet)
	{
		this.productSet = productSet;
	}


	public WileyProductVariantSetModel getProductSet()
	{
		return productSet;
	}
}
