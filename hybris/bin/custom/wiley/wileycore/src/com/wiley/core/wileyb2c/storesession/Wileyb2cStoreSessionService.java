package com.wiley.core.wileyb2c.storesession;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;


public interface Wileyb2cStoreSessionService
{
	boolean isValidSpecialLocale(@Nullable String specialLocale);

	@Nonnull
	void setCurrentSpecialLocale(@Nonnull String isoCode);

	String getCurrentSpecialLocale();

	String getDefaultSpecialLocale();

	/**
	 * Set current country from locale code
	 *
	 * @param isoCode
	 */
	void setCurrentCountry(String isoCode);


}
