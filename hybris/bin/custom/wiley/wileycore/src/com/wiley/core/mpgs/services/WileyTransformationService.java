package com.wiley.core.mpgs.services;

import java.util.Date;

import javax.annotation.Nonnull;


public interface WileyTransformationService
{
	String transformStatusIfSuccessful(String status);

	Date transformStringToDate(@Nonnull String date);
}
