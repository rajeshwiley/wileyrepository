package com.wiley.core.search.solrfacetsearch;

import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;

import java.util.ArrayList;
import java.util.List;


/**
 * @author Dzmitryi_Halahayeu
 */
public class WileySearchQuery extends SearchQuery
{
	private final List<WileyQueryFacet> queryFacets = new ArrayList<>();
	private String facetCode;
	private Boolean useFacetViewMoreLimit;
	private Integer facetLimit;

	public WileySearchQuery(final FacetSearchConfig facetSearchConfig,
			final IndexedType indexedType)
	{
		super(facetSearchConfig, indexedType);
	}

	public void addQueryFacet(final WileyQueryFacet wileyQueryFacet)
	{
		queryFacets.add(wileyQueryFacet);
	}

	public List<WileyQueryFacet> getQueryFacets()
	{
		return queryFacets;
	}

	public String getFacetCode()
	{
		return facetCode;
	}

	public void setFacetCode(final String facetCode)
	{
		this.facetCode = facetCode;
	}

	public Boolean getUseFacetViewMoreLimit()
	{
		return useFacetViewMoreLimit;
	}

	public void setUseFacetViewMoreLimit(final Boolean useFacetViewMoreLimit)
	{
		this.useFacetViewMoreLimit = useFacetViewMoreLimit;
	}

	public Integer getFacetLimit()
	{
		return facetLimit;
	}

	public void setFacetLimit(final Integer facetLimit)
	{
		this.facetLimit = facetLimit;
	}
}
