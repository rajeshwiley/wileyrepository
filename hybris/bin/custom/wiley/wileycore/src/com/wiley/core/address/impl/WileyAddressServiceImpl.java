package com.wiley.core.address.impl;

import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.impl.DefaultAddressService;

import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;

import com.wiley.core.address.WileyAddressDao;
import com.wiley.core.address.WileyAddressService;


public class WileyAddressServiceImpl extends DefaultAddressService implements WileyAddressService
{
	@Resource
	private WileyAddressDao wileyAddressDao;

	@Resource
	private ModelService modelService;

	@Override
	public Optional<AddressModel> getAddressById(final String addressId)
	{
		return wileyAddressDao.findAddressById(addressId);
	}

	@Override
	public Optional<AddressModel> getAddressByExternalIdAndOwner(final String addressExternalId, final String ownerId)
	{
		return wileyAddressDao.findAddressByExternalIdAndOwner(addressExternalId, ownerId);
	}

	public void updateAddressWithTransactionId(final AddressModel address, final String transactionId)
	{
		if (StringUtils.isNotEmpty(transactionId))
		{
			modelService.refresh(address);
			address.setTransactionId(transactionId);
			modelService.save(address);
		}
	}

}
