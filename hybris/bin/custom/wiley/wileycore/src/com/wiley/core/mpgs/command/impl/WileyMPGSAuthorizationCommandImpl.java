package com.wiley.core.mpgs.command.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import com.wiley.core.integration.mpgs.WileyMPGSPaymentGateway;
import com.wiley.core.mpgs.command.WileyAuthorizationCommand;
import com.wiley.core.mpgs.request.WileyAuthorizationRequest;
import com.wiley.core.mpgs.response.WileyAuthorizationResponse;


public class WileyMPGSAuthorizationCommandImpl implements WileyAuthorizationCommand
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyMPGSAuthorizationCommandImpl.class);

	@Autowired
	private WileyMPGSPaymentGateway wileyMpgsPaymentGateway;

	@Override
	public WileyAuthorizationResponse perform(final WileyAuthorizationRequest request)
	{
		WileyAuthorizationResponse response;
		try
		{
			response = wileyMpgsPaymentGateway.authorize(request);
		}
		catch (HttpClientErrorException | HttpServerErrorException ex)
		{
			LOG.error("Unable to authorize card due to MPGS error " + ex.getResponseBodyAsString(), ex);
			response = createErrorResponse(WileyAuthorizationResponse::new);
		}
		catch (final Exception ex)
		{
			LOG.error("Unable to authorize card due to unexpected error", ex);
			response = createHybrisExceptionResponse(WileyAuthorizationResponse::new);
		}
		return response;
	}

}
