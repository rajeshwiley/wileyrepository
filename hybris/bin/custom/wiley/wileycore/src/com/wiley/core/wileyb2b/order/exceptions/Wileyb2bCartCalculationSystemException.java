package com.wiley.core.wileyb2b.order.exceptions;

import de.hybris.platform.servicelayer.exceptions.SystemException;


/**
 * This exception is thrown when something went wrong during cart calculation on wileyb2b
 */
public class Wileyb2bCartCalculationSystemException extends SystemException
{
	public Wileyb2bCartCalculationSystemException(final String message)
	{
		super(message);
	}

	public Wileyb2bCartCalculationSystemException(final Throwable cause)
	{
		super(cause);
	}

	public Wileyb2bCartCalculationSystemException(final String message, final Throwable cause)
	{
		super(message, cause);
	}
}
