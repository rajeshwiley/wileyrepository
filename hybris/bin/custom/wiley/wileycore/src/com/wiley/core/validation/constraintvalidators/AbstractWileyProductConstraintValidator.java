package com.wiley.core.validation.constraintvalidators;

import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.product.ProductModel;

import java.lang.annotation.Annotation;

import javax.validation.ConstraintValidator;

import com.wiley.core.validation.service.WileyDynamicConstraintValidationService;


abstract class AbstractWileyProductConstraintValidator<T extends Annotation> implements ConstraintValidator<T, ProductModel>
{

	private WileyDynamicConstraintValidationService validationService;

	@Override
	public void initialize(final T t)
	{
		 validationService = Registry.getApplicationContext().getBean(WileyDynamicConstraintValidationService.class);
	}

	protected WileyDynamicConstraintValidationService getValidationService()
	{
		return validationService;
	}
}
