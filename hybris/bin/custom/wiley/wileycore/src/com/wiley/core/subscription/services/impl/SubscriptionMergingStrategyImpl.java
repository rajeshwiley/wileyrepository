package com.wiley.core.subscription.services.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.subscriptionservices.enums.SubscriptionStatus;
import de.hybris.platform.subscriptionservices.model.SubscriptionProductModel;
import de.hybris.platform.subscriptionservices.model.SubscriptionTermModel;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wiley.core.model.WileySubscriptionModel;
import com.wiley.core.subscription.services.SubscriptionMergingStrategy;
import com.wiley.core.util.Assert;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


public class SubscriptionMergingStrategyImpl implements SubscriptionMergingStrategy
{
	private static final Logger LOG = LoggerFactory.getLogger(SubscriptionMergingStrategyImpl.class);

	private static final String NEXT_NOT_NULL_MESSAGE = "The next subscription should not be Null";
	private static final String PRODUCT_NOT_NULL_MESSAGE = "The next's subscriptionProduct should not be Null";

	@Override
	public Date mergeSubscriptions(final WileySubscriptionModel previousSubscr, final WileySubscriptionModel nextSubscr)
	{
		validateParameterNotNull(nextSubscr, NEXT_NOT_NULL_MESSAGE);
		Date fromDate = nextSubscr.getStartDate() != null ? nextSubscr.getStartDate() : new Date();
		return mergeSubscriptionsFromDate(previousSubscr, nextSubscr, fromDate);
	}

	@Override
	public Date mergeSubscriptionsFromDate(final WileySubscriptionModel previousSubscr, final WileySubscriptionModel nextSubscr,
			final Date date)
	{
		validateParameterNotNull(nextSubscr, NEXT_NOT_NULL_MESSAGE);
		validateParameterNotNull(nextSubscr.getProduct(), PRODUCT_NOT_NULL_MESSAGE);

		LocalDate fromDate = convertToLocalDate(date);
		//LocalDate fromDate = LocalDate.from();
		long surplus = 0;
		if (isActive(previousSubscr))
		{
			surplus += calculateRemaining(previousSubscr, fromDate);
			if (surplus < 0)
			{
				LOG.warn("WileySubscription with code {} has expirationDate {} in comparizon with {}", previousSubscr.getCode(),
						previousSubscr.getExpirationDate(), date);
			}

		}
		LocalDate expirationWithoutExtend = calculateExpirationFromDate(nextSubscr, fromDate);
		return Date.from(extendSubscription(expirationWithoutExtend, surplus).atZone(ZoneId.systemDefault()).toInstant());
	}

	private boolean isActive(final WileySubscriptionModel subscription)
	{
		return subscription != null && SubscriptionStatus.ACTIVE == subscription.getStatus();
	}

	private long calculateRemaining(final WileySubscriptionModel current, final LocalDate now)
	{
		long result = 0;
		if (current.getExpirationDate() != null)
		{
			LocalDate expir = convertToLocalDate(current.getExpirationDate());
			result = now.until(expir, ChronoUnit.DAYS);
		}
		return result;
	}

	private LocalDate calculateExpirationFromDate(final WileySubscriptionModel subscription, final LocalDate now)
	{
		final ProductModel product = subscription.getProduct();

		Assert.state(product instanceof SubscriptionProductModel,
				() -> String.format("Product [%s] should be instance of %s.", product.getCode(), SubscriptionProductModel.class));

		return applySubscriptionTerm(((SubscriptionProductModel) product).getSubscriptionTerm(), now);
	}

	private LocalDate applySubscriptionTerm(final SubscriptionTermModel subscriptionTerm, final LocalDate now)
	{
		LocalDate result = now;
		if (subscriptionTerm != null)
		{
			int termNumber = subscriptionTerm.getTermOfServiceNumber();
			switch (subscriptionTerm.getTermOfServiceFrequency())
			{
				case MONTHLY:
					result = now.plusMonths(termNumber);
					break;
				case ANNUALLY:
					result = now.plusYears(termNumber);
					break;
				default:
					LOG.warn("Unsupported Subscription term frequency: {}", subscriptionTerm.getTermOfServiceFrequency());
			}
		}
		return result;
	}

	private LocalDateTime extendSubscription(final LocalDate dateTime, long surplusDays)
	{
		LocalDate extended = dateTime;
		if (surplusDays > 0)
		{
			extended = dateTime.plusDays(surplusDays);
		}
		return rollForwardToMidnight(extended);
	}

	private LocalDateTime rollForwardToMidnight(final LocalDate dateTime)
	{
		return LocalDateTime.of(dateTime.getYear(), dateTime.getMonth(), dateTime.getDayOfMonth(), 23, 59, 59);
	}

	private LocalDate convertToLocalDate(final Date date)
	{
		return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault()).toLocalDate();
	}

}
