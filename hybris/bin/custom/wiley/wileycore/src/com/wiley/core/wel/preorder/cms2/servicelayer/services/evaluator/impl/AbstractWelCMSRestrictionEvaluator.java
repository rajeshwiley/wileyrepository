package com.wiley.core.wel.preorder.cms2.servicelayer.services.evaluator.impl;

import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;
import de.hybris.platform.cms2.model.restrictions.AbstractRestrictionModel;
import de.hybris.platform.cms2.servicelayer.data.RestrictionData;
import de.hybris.platform.cms2.servicelayer.services.evaluator.CMSRestrictionEvaluator;

import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;


public abstract class AbstractWelCMSRestrictionEvaluator<T extends AbstractRestrictionModel>
		implements CMSRestrictionEvaluator<T>
{
	public static final String COMPONENT = "component";
	public static final String IS_PRE_ORDER = "isPreOrder";

	protected String getComponentUid(final RestrictionData restrictionData)
	{
		final Object component = restrictionData.getValue(COMPONENT);
		String componentUid = null;
		if (component instanceof AbstractCMSComponentModel)
		{
			componentUid = ((AbstractCMSComponentModel) component).getUid();
		}
		return componentUid;
	}

	protected Boolean isPreOrder()
	{
		final ServletRequestAttributes servletRequestAttributes =
				(ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		final Object isPreOrder = servletRequestAttributes.getAttribute(IS_PRE_ORDER, RequestAttributes.SCOPE_REQUEST);
		if (isPreOrder instanceof Boolean)
		{
			return (Boolean) isPreOrder;
		}
		else
		{
			return Boolean.FALSE;
		}
	}
}
