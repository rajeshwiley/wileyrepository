package com.wiley.core.mpgs.command.impl;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import com.wiley.core.integration.mpgs.WileyMPGSPaymentGateway;
import com.wiley.core.mpgs.command.WileyTokenizationCommand;
import com.wiley.core.mpgs.request.WileyTokenizationRequest;
import com.wiley.core.mpgs.response.WileyTokenizationResponse;



public class WileyMPGSTokenizationCommandImpl implements WileyTokenizationCommand
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyMPGSTokenizationCommandImpl.class);

	@Autowired
	private WileyMPGSPaymentGateway wileyMpgsPaymentGateway;

	@Override
	public WileyTokenizationResponse perform(final WileyTokenizationRequest tokenizationRequest)
	{

		WileyTokenizationResponse response;
		try
		{
			WileyTokenizationRequest request = tokenizationRequest;
			response = wileyMpgsPaymentGateway.tokenize(request);
		}
		catch (HttpClientErrorException | HttpServerErrorException ex)
		{
			LOG.error("Unable to tokenize card due to MPGS error " + ex.getResponseBodyAsString(), ex);
			response = createErrorResponse(WileyTokenizationResponse::new);
		}
		catch (final Exception ex)
		{
			LOG.error("Unable to tokenize card due to unexpected error", ex);
			response = createHybrisExceptionResponse(WileyTokenizationResponse::new);
		}
		return response;
	}


}