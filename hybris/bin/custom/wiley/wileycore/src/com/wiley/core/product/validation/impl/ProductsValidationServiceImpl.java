package com.wiley.core.product.validation.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.validation.exceptions.HybrisConstraintViolation;
import de.hybris.platform.validation.model.constraints.ConstraintGroupModel;
import de.hybris.platform.validation.services.ValidationService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import com.google.common.base.Preconditions;
import com.wiley.core.model.ProductValidationErrorModel;
import com.wiley.core.product.validation.ProductsValidationService;


/**
 * The only implementation of {@link ProductsValidationService}
 */
public class ProductsValidationServiceImpl implements ProductsValidationService
{
	@Resource
	ValidationService validationService;

	@Resource
	ModelService modelService;


	@Nonnull
	@Override
	public List<ProductValidationErrorModel> validate(@Nonnull final ProductModel productModel,
			@Nonnull final CronJobModel cronJobModel, @Nonnull final List<ConstraintGroupModel> constraintGroups)
	{
		Preconditions.checkNotNull(productModel);
		Preconditions.checkNotNull(cronJobModel);
		Preconditions.checkNotNull(constraintGroups);

		final Set<HybrisConstraintViolation> constraintViolations = validationService.validate(productModel,
				constraintGroups);

		return constraintViolations.stream()
				.map(constraintViolation -> populateProductValidationErrorModel(constraintViolation, productModel, cronJobModel))
				.collect(Collectors.toList());
	}



	private ProductValidationErrorModel populateProductValidationErrorModel(final HybrisConstraintViolation constraintViolation,
			final ProductModel productModel,
			final CronJobModel cronJobModel)
	{
		final ProductValidationErrorModel validationError = modelService.create(ProductValidationErrorModel.class);
		validationError.setCode(constraintViolation.getQualifier());
		validationError.setMessage(constraintViolation.getLocalizedMessage());
		validationError.setCronJob(cronJobModel);
		validationError.setProduct(productModel);
		return validationError;
	}
}