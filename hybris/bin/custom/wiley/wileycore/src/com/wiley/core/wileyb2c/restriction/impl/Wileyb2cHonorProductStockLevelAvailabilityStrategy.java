package com.wiley.core.wileyb2c.restriction.impl;

import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.commerceservices.stock.CommerceStockService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.wiley.restriction.WileyRestrictProductStrategy;
import com.wiley.core.wiley.restriction.dto.WileyRestrictionCheckResultDto;
import com.wiley.core.wileyb2c.classification.Wileyb2cClassificationService;


public class Wileyb2cHonorProductStockLevelAvailabilityStrategy implements WileyRestrictProductStrategy
{
	private Wileyb2cClassificationService wileyb2cClassificationService;
	private BaseStoreService baseStoreService;
	private CommerceStockService wileycomCommerceStockService;


	@Override
	public boolean isRestricted(final ProductModel product)
	{
		Assert.notNull(product);

		final ClassificationClassModel classificationClass = wileyb2cClassificationService.resolveClassificationClass(product);
		final BaseStoreModel currentBaseStore = baseStoreService.getCurrentBaseStore();

		final boolean isProductRestricted =
				classificationClass.getHonorStockLevelInformation()
						&& wileycomCommerceStockService.getStockLevelForProductAndBaseStore(product, currentBaseStore) == 0;

		return isProductRestricted;
	}

	@Override
	public boolean isRestricted(final CommerceCartParameter parameter)
	{
		return isRestricted(parameter.getProduct());
	}

	@Override
	public WileyRestrictionCheckResultDto createErrorResult(final CommerceCartParameter parameter)
	{
		return WileyRestrictionCheckResultDto.failureResult(WileyCoreConstants.PRODUCT_IS_NOT_PURCHASABLE,
				String.format("Product [%s] is not available because of Stock level amount is zero",
						parameter.getProduct().getCode()), null);
	}

	@Required
	public void setWileyb2cClassificationService(final Wileyb2cClassificationService wileyb2cClassificationService)
	{
		this.wileyb2cClassificationService = wileyb2cClassificationService;
	}

	@Required
	public void setBaseStoreService(final BaseStoreService baseStoreService)
	{
		this.baseStoreService = baseStoreService;
	}

	@Required
	public void setWileycomCommerceStockService(final CommerceStockService wileycomCommerceStockService)
	{
		this.wileycomCommerceStockService = wileycomCommerceStockService;
	}

}
