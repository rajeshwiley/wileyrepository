package com.wiley.core.locale.impl;

import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.wiley.core.locale.WileyLocaleService;
import com.wiley.core.wileycom.storesession.WileycomStoreSessionService;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


public class WileyLocaleServiceImpl implements WileyLocaleService
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyLocaleServiceImpl.class);

	private static final String ENCODED_LOCALE_ATTRIBUTE_NAME = "encodedLocale";
	private static final String ENCODED_LOCALE_SEPARATOR = "-";
	private static final String LOCALE_SEPARATOR = "_";
	private static final String ENCODED_LOCALE_TEMPLATE = "%s-%s";
	private static final String ENCODED_LOCALE_PATTERN = "^[a-z]{2}-[a-z]{2}$";
	private static final boolean RECALCULATE_CART = false;

	private static final String ERROR_ENCODED_LOCALE_NOT_VALID = "Encoded Locale [{}] isn't valid";

	private final WileycomStoreSessionService wileycomStoreSessionService;
	private final SessionService sessionService;
	private final CommonI18NService commonI18NService;
	private final CommerceCommonI18NService commerceCommonI18NService;


	@Value("${default.encoded.locale}")
	private String defaultEncodedLocale;

	@Autowired
	public WileyLocaleServiceImpl(final WileycomStoreSessionService wileycomStoreSessionService,
			final SessionService sessionService, final CommonI18NService commonI18NService,
			final CommerceCommonI18NService commerceCommonI18NService)
	{
		this.wileycomStoreSessionService = wileycomStoreSessionService;
		this.sessionService = sessionService;
		this.commonI18NService = commonI18NService;
		this.commerceCommonI18NService = commerceCommonI18NService;
	}

	@Override
	public String getDefaultEncodedLocale()
	{
		return defaultEncodedLocale;
	}

	@Override
	public String getCurrentEncodedLocale()
	{
		return sessionService.getAttribute(ENCODED_LOCALE_ATTRIBUTE_NAME);
	}

	@Override
	public String getEncodedLocale(final CountryModel country, final LanguageModel language)
	{
		validateParameterNotNull(country, "Country mustn't be null");
		validateParameterNotNull(language, "Language mustn't be null");
		String languageIsoCode = commonI18NService.getLocaleForLanguage(language).getLanguage();
		return String.format(ENCODED_LOCALE_TEMPLATE, languageIsoCode, country.getIsocode()).toLowerCase();
	}

	@Override
	public CountryModel getCountryForEncodedLocale(final String encodedLocale)
	{
		validateParameterNotNull(encodedLocale, "Encoded locale mustn't be null");
		String countryIsoCode = getCountryIsoCodeFromEncodedLocale(encodedLocale).toUpperCase();
		return commonI18NService.getCountry(countryIsoCode);
	}

	@Override
	public void setEncodedLocale(final String encodedLocale)
	{
		if (isValidEncodedLocale(encodedLocale))
		{
			final CountryModel countryModel = getCountryForEncodedLocale(encodedLocale);
			wileycomStoreSessionService.setCurrentCountry(countryModel.getIsocode(), RECALCULATE_CART);
			wileycomStoreSessionService.setCurrentLanguage(countryModel.getDefaultLanguage().getIsocode());
			sessionService.setAttribute(ENCODED_LOCALE_ATTRIBUTE_NAME, encodedLocale);
		}
	}

	@Override
	public boolean isValidEncodedLocale(final String encodedLocale)
	{
		boolean isValidEncodedLocale = false;
		try
		{
			final CountryModel countryModel = getCountryForEncodedLocale(encodedLocale);
			String languageIsoCodeFromEncodedLocale = getLanguageIsoCodeFromEncodedLocale(encodedLocale);
			isValidEncodedLocale = isCountrySupported(countryModel) && isLanguageSupportedForCountry(
					languageIsoCodeFromEncodedLocale, countryModel);
		}
		catch (UnknownIdentifierException | IllegalArgumentException e)
		{
			LOG.warn(ERROR_ENCODED_LOCALE_NOT_VALID, encodedLocale);
		}
		return isValidEncodedLocale;
	}

	private boolean isEncodedLocaleParsed(final String encodedLocale)
	{
		return encodedLocale.matches(ENCODED_LOCALE_PATTERN);
	}

	private boolean isLanguageSupportedForCountry(final String languageIsoCode, final CountryModel countryModel)
	{
		String countryLanguageIsoCode = getLanguageIsoCodeFromLocale(countryModel.getDefaultLanguage().getIsocode());
		return countryLanguageIsoCode.equals(languageIsoCode);
	}

	private boolean isCountrySupported(final CountryModel countryModel)
	{
		return commerceCommonI18NService.getAllCountries().contains(countryModel);

	}

	private String getLanguageIsoCodeFromEncodedLocale(final String encodedLocale)
	{
		if (isEncodedLocaleParsed(encodedLocale))
		{
			return encodedLocale.split(ENCODED_LOCALE_SEPARATOR)[0];
		}
		else
		{
			throw new IllegalArgumentException(ERROR_ENCODED_LOCALE_NOT_VALID);
		}
	}

	private String getLanguageIsoCodeFromLocale(final String locale)
	{
		return locale.split(LOCALE_SEPARATOR)[0];
	}

	private String getCountryIsoCodeFromEncodedLocale(final String encodedLocale)
	{
		if (isEncodedLocaleParsed(encodedLocale))
		{
			return encodedLocale.split(ENCODED_LOCALE_SEPARATOR)[1];
		}
		else
		{
			throw new IllegalArgumentException(ERROR_ENCODED_LOCALE_NOT_VALID);
		}
	}

	@Override
	public List<String> getAllEncodedLocales()
	{
		Collection<CountryModel> countryModels = commerceCommonI18NService.getAllCountries();
		List<String> locales = countryModels
				.stream()
				.map(country -> getEncodedLocaleForCountry(country))
				.collect(Collectors.toList());
		return locales;
	}

	private String getEncodedLocaleForCountry(final CountryModel country)
	{
		return String.format("%s-%s", commonI18NService.getLocaleForLanguage(country.getDefaultLanguage()).getLanguage(),
				country.getIsocode().toLowerCase());
	}
}
