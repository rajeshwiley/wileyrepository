/**
 *
 */
package com.wiley.core.integration.cdm.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;


@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CDMCreateUserErrorResponse {
  @JsonProperty("ErrorCode")
  private String errorCode;

  @JsonProperty("ErrorMessage")
  private String errorMessage;

  @JsonProperty("StackTrace")
  private String stackTrace;

  /**
   * @return the errorCode
   */
  public String getErrorCode() {
    return errorCode;
  }

  /**
   * @param errorCode
   *          the errorCode to set
   */
  public void setErrorCode(final String errorCode) {
    this.errorCode = errorCode;
  }

  /**
   * @return the errorMessage
   */
  public String getErrorMessage() {
    return errorMessage;
  }

  /**
   * @param errorMessage
   *          the errorMessage to set
   */
  public void setErrorMessage(final String errorMessage) {
    this.errorMessage = errorMessage;
  }

  /**
   * @return the stackTrace
   */
  public String getStackTrace() {
    return stackTrace;
  }

  /**
   * @param stackTrace
   *          the stackTrace to set
   */
  public void setStackTrace(final String stackTrace) {
    this.stackTrace = stackTrace;
  }

  @Override
  public String toString() {
    return "CDMCreateUserErrorResponse [errorCode=" + errorCode + ", errorMessage=" + errorMessage
        + ", stackTrace= " + stackTrace + "]";
  }
}
