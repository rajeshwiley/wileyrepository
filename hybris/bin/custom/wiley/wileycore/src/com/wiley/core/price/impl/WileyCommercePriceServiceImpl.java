package com.wiley.core.price.impl;

import de.hybris.platform.commerceservices.price.impl.DefaultCommercePriceService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;

import com.wiley.core.model.WileyProductVariantSetModel;
import com.wiley.core.price.WileyCommercePriceService;


public class WileyCommercePriceServiceImpl extends DefaultCommercePriceService implements WileyCommercePriceService
{
	@Override
	public PriceInformation getStartingAtPrice(final ProductModel product, final boolean ignoreSetVariant)
	{
		final Collection<VariantProductModel> variants = product.getVariants();
		PriceInformation startingAtPrice = null;
		if (CollectionUtils.isNotEmpty(variants))
		{
			List<PriceInformation> sortedVariantPrices = variants
					.stream()
					//do not count price for Sets
					.filter(variant -> !ignoreSetVariant || !(variant instanceof WileyProductVariantSetModel))
					.map(this::getWebPriceForProduct)
					.filter(Objects::nonNull)
					.sorted(PriceInformationComparator.INSTANCE)
					.collect(Collectors.toList());
			if (CollectionUtils.isNotEmpty(sortedVariantPrices))
			{
				startingAtPrice = findStartingAtPrice(sortedVariantPrices);
			}

		}
		return startingAtPrice;
	}

	private PriceInformation findStartingAtPrice(final List<PriceInformation> sortedVariantPrices)
	{
		final PriceInformation minPrice = sortedVariantPrices.get(0);
		boolean hasDifferentPrices = sortedVariantPrices
				.stream().anyMatch(price -> Double.compare(
						price.getPriceValue().getValue(),
						minPrice.getPriceValue().getValue()) != 0);
		return hasDifferentPrices ? minPrice : null;
	}
}
