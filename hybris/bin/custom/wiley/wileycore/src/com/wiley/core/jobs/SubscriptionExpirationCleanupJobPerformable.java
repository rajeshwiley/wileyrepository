package com.wiley.core.jobs;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.subscriptionservices.enums.SubscriptionStatus;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.model.WileySubscriptionModel;
import com.wiley.core.subscription.services.WileySubscriptionService;


public class SubscriptionExpirationCleanupJobPerformable extends AbstractJobPerformable<CronJobModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(SubscriptionExpirationCleanupJobPerformable.class);

	@Autowired
	private WileySubscriptionService wileySubscriptionService;

	@Override
	public PerformResult perform(final CronJobModel cronJob)
	{
		LOG.info("Starting job for cron job [{}].", cronJob.getCode());

		final List<WileySubscriptionModel> expiringSubscriptions = wileySubscriptionService.getAllExpiredSubscriptions();

		if (CollectionUtils.isEmpty(expiringSubscriptions))
		{
			LOG.debug("There are no subscriptions to set EXPIRED.");
		}
		else
		{
			wileySubscriptionService.updateSubscriptionStatus(expiringSubscriptions, SubscriptionStatus.EXPIRED);
			LOG.debug("Performed batch update for {} subscriptions to set EXPIRED", expiringSubscriptions.size());
		}

		LOG.info("Completed job for cron job [{}].", cronJob.getCode());
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}

}
