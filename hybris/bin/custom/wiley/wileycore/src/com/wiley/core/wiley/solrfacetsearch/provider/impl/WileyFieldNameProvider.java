package com.wiley.core.wiley.solrfacetsearch.provider.impl;

import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.provider.impl.DefaultFieldNameProvider;
import de.hybris.platform.util.Config;

import java.util.Locale;


/**
 * @author Dzmitryi_Halahayeu
 */
public class WileyFieldNameProvider extends DefaultFieldNameProvider
{
	static final String USED_SEPARATOR = Config.getString("solr.indexedproperty.forbidden.char", "_");
	/**
	 * Multi value customized
	 **/
	public static final String MULTIVALUE_POSTFIX = "mv_c";
	private static final String RANGE_TYPE_TEXT = "text";
	private static final String RANGE_TYPE_STRING = "string";

	/**
	 * OOTB_CODE based on {{@link DefaultFieldNameProvider#getFieldName(IndexedProperty, String, String, String)}}
	 * We changed only postfix for multi value field. See {@link #MULTIVALUE_POSTFIX}. This change due to by default Hybris
	 * index text value to SOLR dynamic field like "text_{$locale_name}" and "text_${locale_name}_mv". For example:
	 * <p>
	 * For "en" locale: we should have "text_en" and "text_en_mv" dynamic fields.
	 * For "en_us" locale: we should have "text_en_us" and "text_en_us_mv" dynamic fields.
	 * <p>
	 * In our configuration we use locale "en_mv"(English Maldives) and it cause name conflict with "en" locale.
	 */
	@Override
	protected String getFieldName(final IndexedProperty indexedProperty, final String name, final String type,
			final String specifier)
	{
		String rangeType = type;
		if (isRanged(indexedProperty))
		{
			rangeType = RANGE_TYPE_STRING;
		}
		rangeType = rangeType.toLowerCase(Locale.ROOT);
		StringBuilder fieldName = new StringBuilder();
		if (specifier == null)
		{
			fieldName.append(name).append(USED_SEPARATOR).append(rangeType);
		}
		else if (RANGE_TYPE_TEXT.equals(rangeType))
		{
			fieldName.append(name).append(USED_SEPARATOR).append(RANGE_TYPE_TEXT).append(USED_SEPARATOR).append(
					specifier.toLowerCase(Locale.ROOT));
		}
		else
		{
			fieldName.append(name).append(USED_SEPARATOR).append(specifier.toLowerCase(Locale.ROOT)).append(USED_SEPARATOR)
					.append(rangeType);
		}

		if (indexedProperty.isMultiValue())
		{
			fieldName.append(USED_SEPARATOR).append(MULTIVALUE_POSTFIX);
		}

		return fieldName.toString();
	}
}