package com.wiley.core.media.url.impl;

import de.hybris.platform.media.MediaSource;
import de.hybris.platform.media.storage.MediaStorageConfigService;
import de.hybris.platform.media.url.MediaURLStrategy;
import de.hybris.platform.util.MediaUtil;

import org.springframework.beans.factory.annotation.Required;

import com.google.common.base.Preconditions;
 
public class CloudFrontMediaWebURLStrategy implements MediaURLStrategy {
    private String basePath;
 
    protected String getBasePath() {
        return basePath;
    }
 
    @Required
    public void setBasePath(final String basePath) {
        this.basePath = basePath;
    }
 
    @Override
    public String getUrlForMedia(final MediaStorageConfigService.MediaFolderConfig config, final MediaSource media) {
        Preconditions.checkArgument(config != null, "Folder config is required to perform this operation");
        Preconditions.checkArgument(media != null, "MediaSource is required to perform this operation");
 
        return MediaUtil.addTrailingFileSepIfNeeded(getBasePath()) + media.getLocation();
    }
}