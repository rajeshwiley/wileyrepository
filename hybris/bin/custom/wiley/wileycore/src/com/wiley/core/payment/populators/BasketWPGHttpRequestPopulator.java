package com.wiley.core.payment.populators;

import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.request.AbstractRequestPopulator;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionRequest;
import de.hybris.platform.acceleratorservices.payment.data.OrderEntryInfoData;
import de.hybris.platform.acceleratorservices.payment.data.PaymentData;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;

import com.wiley.core.servicelayer.i18n.WileycomI18NService;

import static org.apache.commons.lang.StringEscapeUtils.escapeXml;


public class BasketWPGHttpRequestPopulator extends AbstractRequestPopulator<CreateSubscriptionRequest, PaymentData>
{
	private static final String WPG_CUSTOM_BASKET_TAX = "WPG_CUSTOM_BASKET_tax";
	private static final String WPG_CUSTOM_BASKET_TAX_CALCULATED = "WPG_CUSTOM_BASKET_tax_calculated";
	private static final String WPG_CUSTOM_BASKET_CURRENCY_SYMBOL = "WPG_CUSTOM_BASKET_currency_symbol";
	private static final String WPG_CUSTOM_BASKET_TOTAL = "WPG_CUSTOM_BASKET_total";
	private static final String WPG_CUSTOM_BASKET_SUBTOTAL = "WPG_CUSTOM_BASKET_subtotal";
	private static final String WPG_CUSTOM_BASKET_SHIPPING = "WPG_CUSTOM_BASKET_shipping";
	private static final String WPG_CUSTOM_BASKET_DESIRED_SHIPPING_DATE = "WPG_CUSTOM_BASKET_desired_shipping_date";
	private static final String WPG_CUSTOM_BASKET_ORDER_WITHOUT_SHIPPING = "WPG_CUSTOM_BASKET_order_without_shipping";

	private static final String WPG_CUSTOM_BASKET_DISCOUNT = "WPG_CUSTOM_BASKET_discount";
	private static final String WPG_CUSTOM_BASKET_DISCOUNTCODE = "WPG_CUSTOM_BASKET_discount_code";
	private static final String WPG_CUSTOM_BASKET_STUDENT_FLOW = "WPG_CUSTOM_BASKET_student_flow";

	private static final String WPG_CUSTOM_BASKET_COUNT = "WPG_CUSTOM_BASKET_count";
	private static final String WPG_CUSTOM_BASKET_PREFIX = "WPG_CUSTOM_BASKET_";
	private static final String WPG_CUSTOM_BASKET_ENTRY_NAME = "_name";
	private static final String WPG_CUSTOM_BASKET_ENTRY_DESCRIPTION = "_desc";
	private static final String WPG_CUSTOM_BASKET_ENTRY_IMAGE_LINK = "_imgUrl";
	private static final String WPG_CUSTOM_BASKET_ENTRY_FEATURE = "_feature";
	private static final String WPG_CUSTOM_BASKET_ENTRY_FEATURE_COUNT = "_feature_count";
	private static final String WPG_CUSTOM_BASKET_ENTRY_QUANTITY = "_qty";
	private static final String WPG_CUSTOM_BASKET_ENTRY_PRICE = "_price";
	private static final String WPG_CUSTOM_BASKET_ENTRY_TOTAL = "_total";
	private static final String WPG_CUSTOM_BASKET_ENTRY_TAX = "_tax";
	private static final String WPG_CUSTOM_TAX_LABEL = "WPG_CUSTOM_tax_label";
	private final SimpleDateFormat desiredShippingDateFormat = new SimpleDateFormat("MM/dd/yyyy");

	@Resource
	private WileycomI18NService wileycomI18NService;

	@Override
	public void populate(final CreateSubscriptionRequest source, final PaymentData target)
			throws ConversionException
	{
		final List<OrderEntryInfoData> orderEntries = source.getOrderInfoData().getOrderEntries();
		if (CollectionUtils.isNotEmpty(orderEntries))
		{
			addRequestQueryParam(target, WPG_CUSTOM_BASKET_COUNT, String.valueOf(orderEntries.size()));
			addRequestQueryParam(target, WPG_CUSTOM_BASKET_TOTAL, source.getOrderInfoData().getOrderTotal());
			addRequestQueryParam(target, WPG_CUSTOM_BASKET_SUBTOTAL, source.getOrderInfoData().getSubtotalWithoutDiscount());

			addRequestQueryParam(target, WPG_CUSTOM_BASKET_TAX, source.getOrderInfoData().getTaxAmount());
			addRequestQueryParam(target, WPG_CUSTOM_BASKET_TAX_CALCULATED,
					String.valueOf(source.getOrderInfoData().isTaxCalculated()));
			addRequestQueryParam(target, WPG_CUSTOM_BASKET_SHIPPING, source.getOrderInfoData().getShippingCost());
			addRequestQueryParam(target, WPG_CUSTOM_BASKET_DISCOUNT, source.getOrderInfoData().getDiscount());
			addRequestQueryParam(target, WPG_CUSTOM_BASKET_DISCOUNTCODE, source.getOrderInfoData().getDiscountCode());
			addRequestQueryParam(target, WPG_CUSTOM_BASKET_CURRENCY_SYMBOL, source.getOrderInfoData().getCurrencySymbol());
			final Optional<CountryModel> currentCountry = getWileycomI18NService().getCurrentCountry();
			if (currentCountry.isPresent())
			{
				addRequestQueryParam(target, WPG_CUSTOM_TAX_LABEL,
						currentCountry.get().getTaxName());
			}
			addRequestQueryParam(target, WPG_CUSTOM_BASKET_STUDENT_FLOW,
					convertBoolean(source.getOrderInfoData().isStudentFlow()));
			addRequestQueryParam(target, WPG_CUSTOM_BASKET_ORDER_WITHOUT_SHIPPING,
					convertBoolean(source.getOrderInfoData().isOrderWithoutShipping()));

			addBasketEntries(orderEntries, target);
			if (source.getOrderInfoData().getDesiredShippingDate() != null)
			{
				addRequestQueryParam(target, WPG_CUSTOM_BASKET_DESIRED_SHIPPING_DATE,
						desiredShippingDateFormat.format(source.getOrderInfoData().getDesiredShippingDate()));
			}
		}
	}

	//assumes that WPG could have specific contract for type formatting
	private static String convertBoolean(boolean param)
	{
		return Boolean.toString(param);
	}

	private void addBasketEntries(final List<OrderEntryInfoData> orderEntries, final PaymentData target)
	{
		for (int orderEntryIndex = 1; orderEntryIndex <= orderEntries.size(); orderEntryIndex++)
		{
			final OrderEntryInfoData orderEntryInfo = orderEntries.get(orderEntryIndex - 1);

			addRequestQueryParam(target, WPG_CUSTOM_BASKET_PREFIX + orderEntryIndex
					+ WPG_CUSTOM_BASKET_ENTRY_NAME, escapeXml(orderEntryInfo.getName()));
			addRequestQueryParam(target, WPG_CUSTOM_BASKET_PREFIX + orderEntryIndex
					+ WPG_CUSTOM_BASKET_ENTRY_DESCRIPTION, escapeXml(orderEntryInfo.getDescription()));
			addRequestQueryParam(target, WPG_CUSTOM_BASKET_PREFIX + orderEntryIndex
					+ WPG_CUSTOM_BASKET_ENTRY_IMAGE_LINK, orderEntryInfo.getImageUrl());
			addRequestQueryParam(target, WPG_CUSTOM_BASKET_PREFIX + orderEntryIndex
					+ WPG_CUSTOM_BASKET_ENTRY_QUANTITY, orderEntryInfo.getQuantity());
			addRequestQueryParam(target, WPG_CUSTOM_BASKET_PREFIX + orderEntryIndex
					+ WPG_CUSTOM_BASKET_ENTRY_PRICE, orderEntryInfo.getPrice());
			addRequestQueryParam(target, WPG_CUSTOM_BASKET_PREFIX + orderEntryIndex
					+ WPG_CUSTOM_BASKET_ENTRY_TOTAL, orderEntryInfo.getTotal());
			addRequestQueryParam(target, WPG_CUSTOM_BASKET_PREFIX + orderEntryIndex
					+ WPG_CUSTOM_BASKET_ENTRY_TAX, orderEntryInfo.getTax());

			addEntryFeatures(target, orderEntryIndex, orderEntryInfo);
		}
	}

	private void addEntryFeatures(final PaymentData target, final int orderEntryIndex, final OrderEntryInfoData orderEntryInfo)
	{
		if (CollectionUtils.isNotEmpty(orderEntryInfo.getFeatures()))
		{
			addRequestQueryParam(target, WPG_CUSTOM_BASKET_PREFIX + orderEntryIndex
					+ WPG_CUSTOM_BASKET_ENTRY_FEATURE_COUNT, String.valueOf(orderEntryInfo.getFeatures().size()));

			for (int featureIndex = 1; featureIndex <= orderEntryInfo.getFeatures().size(); featureIndex++)
			{
				addRequestQueryParam(target, WPG_CUSTOM_BASKET_PREFIX + orderEntryIndex + "_" + featureIndex
						+ WPG_CUSTOM_BASKET_ENTRY_FEATURE, orderEntryInfo.getFeatures().get(featureIndex - 1));
			}
		}
	}

	public WileycomI18NService getWileycomI18NService()
	{
		return wileycomI18NService;
	}

	public void setWileycomI18NService(final WileycomI18NService wileycomI18NService)
	{
		this.wileycomI18NService = wileycomI18NService;
	}
}
