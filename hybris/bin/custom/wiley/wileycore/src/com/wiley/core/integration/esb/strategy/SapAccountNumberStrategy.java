package com.wiley.core.integration.esb.strategy;

import de.hybris.platform.core.model.order.CartModel;

import javax.annotation.Nonnull;


/**
 * This enricher is used to get additional data for spring integration headers.
 */
public interface SapAccountNumberStrategy
{

	@Nonnull
	String getSapAccountNumber(@Nonnull CartModel cartModel);

}
