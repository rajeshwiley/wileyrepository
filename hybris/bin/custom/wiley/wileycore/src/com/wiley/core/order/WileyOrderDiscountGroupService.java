package com.wiley.core.order;

import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.europe1.enums.UserDiscountGroup;

import javax.annotation.Nonnull;


public interface WileyOrderDiscountGroupService
{

	UserDiscountGroup resolveDiscountGroupByAbstractOrder(@Nonnull AbstractOrderModel abstractOrder);

	/**
	 * This method decides if student discount group should be applied to the cart or not.
	 *
	 * @param commerceCartParameter
	 */
	void decideIfDiscountGroupOrVoucherShouldBeApplied(@Nonnull CommerceCartParameter commerceCartParameter);

	/**
	 * This will return TRUE any of products in order has student discount specified. Will return false otherwise.
	 *
	 * @param abstractOrder
	 */
	boolean hasStudentDiscountProducts(AbstractOrderModel abstractOrder);
}
