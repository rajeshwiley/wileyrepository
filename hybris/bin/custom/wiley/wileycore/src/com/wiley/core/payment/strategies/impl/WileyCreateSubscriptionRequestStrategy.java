package com.wiley.core.payment.strategies.impl;

import com.wiley.core.order.WileyCartService;
import de.hybris.platform.acceleratorservices.payment.cybersource.enums.TransactionTypeEnum;
import de.hybris.platform.acceleratorservices.payment.cybersource.strategies.impl.DefaultCreateSubscriptionRequestStrategy;
import de.hybris.platform.acceleratorservices.payment.data.OrderInfoData;

public class WileyCreateSubscriptionRequestStrategy extends DefaultCreateSubscriptionRequestStrategy {

    private WileyCartService wileyCartService;

    @Override
    protected OrderInfoData getRequestOrderInfoData(final TransactionTypeEnum transactionType) {
        final OrderInfoData orderInfoData = super.getRequestOrderInfoData(transactionType);
        getCartService().populateFromCurrentCart(orderInfoData);
        return orderInfoData;
    }

    @Override
    public WileyCartService getCartService() {
        return wileyCartService;
    }

    public void setWileyCartService(final WileyCartService wileyCartService) {
        this.wileyCartService = wileyCartService;
    }


}
