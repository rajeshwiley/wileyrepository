package com.wiley.core.wileyb2c.cms.service.impl;

import de.hybris.platform.acceleratorcms.component.slot.impl.DefaultCMSPageSlotComponentService;
import de.hybris.platform.acceleratorcms.data.CmsPageRequestContextData;
import de.hybris.platform.acceleratorcms.model.components.CMSTabParagraphContainerModel;
import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;
import de.hybris.platform.cms2.model.contents.components.SimpleCMSComponentModel;
import de.hybris.platform.cms2.model.contents.containers.AbstractCMSComponentContainerModel;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.constants.WileyCoreConstants;

import reactor.util.CollectionUtils;


public class WileyCMSPageSlotComponentServiceImpl extends DefaultCMSPageSlotComponentService
{
	@Autowired
	private SessionService sessionService;

	/**
	 * The method was overridden to add a check for CMSTabParagraphContainerModel
	 * and return the container itself instead of list of CMSTabParagraph
	 * components inside it
	 */
	@Override
	protected List<AbstractCMSComponentModel> flattenComponentHierarchy(
			final CmsPageRequestContextData cmsPageRequestContextData, final AbstractCMSComponentModel component,
			final boolean evaluateRestrictions, final int limit)
	{
		if (limit == 0)
		{
			return Collections.emptyList();
		}
		else if (component instanceof SimpleCMSComponentModel)
		{
			return Collections.singletonList(component);
		}
		else if (component instanceof CMSTabParagraphContainerModel)
		{
			final CMSTabParagraphContainerModel container = (CMSTabParagraphContainerModel) component;

			final List<AbstractCMSComponentModel> abstractCMSComponentModels = flattenComponentContainerHierarchy(
					cmsPageRequestContextData, container, true, limit);

			final List<SimpleCMSComponentModel> simpleCMSComponents = new ArrayList<>();
			if (!CollectionUtils.isEmpty(abstractCMSComponentModels))
			{
				abstractCMSComponentModels.stream().filter(r -> r instanceof SimpleCMSComponentModel)
						.forEach(r -> simpleCMSComponents.add((SimpleCMSComponentModel) r));
			}
			sessionService.getCurrentSession()
					.setAttribute(WileyCoreConstants.TAB_CONTAINER_TABS + container.getUid(), simpleCMSComponents);
			return Collections.singletonList(container);
		}
		else if (component instanceof AbstractCMSComponentContainerModel)
		{
			final AbstractCMSComponentContainerModel container = (AbstractCMSComponentContainerModel) component;
			return flattenComponentContainerHierarchy(cmsPageRequestContextData, container, evaluateRestrictions, limit);
		}
		else
		{
			return Collections.singletonList(component);
		}
	}
}
