package com.wiley.core.delivery;

import de.hybris.platform.commerceservices.delivery.dao.CountryZoneDeliveryModeDao;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.store.BaseStoreModel;

import java.util.List;



public interface WileyCountryZoneDeliveryModeDao extends CountryZoneDeliveryModeDao
{
	List<DeliveryModeModel> getSupportedDeliveryModes(BaseStoreModel baseStore, CurrencyModel currencyModel,
			CountryModel countryModel);
}
