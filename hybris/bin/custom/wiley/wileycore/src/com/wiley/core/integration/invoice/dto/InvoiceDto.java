package com.wiley.core.integration.invoice.dto;

public class InvoiceDto
{
	private String mimeType;
	private byte[] binaryFile;

	public String getMimeType()
	{
		return mimeType;
	}

	public void setMimeType(final String mimeType)
	{
		this.mimeType = mimeType;
	}

	public byte[] getBinaryFile()
	{
		return binaryFile;
	}

	public void setBinaryFile(final byte[] binaryFile)
	{
		this.binaryFile = binaryFile;
	}
}
