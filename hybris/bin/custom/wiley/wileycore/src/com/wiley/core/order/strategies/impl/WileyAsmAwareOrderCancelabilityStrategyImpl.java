package com.wiley.core.order.strategies.impl;

import de.hybris.platform.assistedserviceservices.AssistedServiceService;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.order.strategies.AbstractOrderModificationStrategy;
import com.wiley.core.order.strategies.WileyOrderCancelabilityStrategy;


public class WileyAsmAwareOrderCancelabilityStrategyImpl extends AbstractOrderModificationStrategy
		implements WileyOrderCancelabilityStrategy
{

	@Resource(name = "wileyAsmOrderCancelabilityStrategy")
	private WileyOrderCancelabilityStrategy wileyAsmOrderCancelabilityStrategy;
	@Resource(name = "wileyDefaultOrderCancelabilityStrategy")
	private WileyOrderCancelabilityStrategy wileyDefaultOrderCancelabilityStrategy;

	@Autowired
	private AssistedServiceService assistedServiceService;

	@Override
	public boolean isCancelable(final OrderModel order)
	{
		if (isAsmSession())
		{
			return wileyAsmOrderCancelabilityStrategy.isCancelable(order);
		}
		else
		{
			return wileyDefaultOrderCancelabilityStrategy.isCancelable(order);
		}
	}

	@Override
	public boolean hasCancelableEntries(final OrderModel order)
	{
		if (isAsmSession())
		{
			return wileyAsmOrderCancelabilityStrategy.hasCancelableEntries(order);
		}
		else
		{
			return wileyDefaultOrderCancelabilityStrategy.hasCancelableEntries(order);
		}
	}

	@Override
	public boolean isCancelableEntryState(final OrderStatus entryState)
	{
		if (isAsmSession())
		{
			return wileyAsmOrderCancelabilityStrategy.isCancelableEntryState(entryState);
		}
		else
		{
			return wileyDefaultOrderCancelabilityStrategy.isCancelableEntryState(entryState);
		}
	}

	@Override
	public boolean isCancelableOrderEntry(final OrderModel order, final OrderStatus entryState)
	{
		if (isAsmSession())
		{
			return wileyAsmOrderCancelabilityStrategy.isCancelableOrderEntry(order, entryState);
		}
		else
		{
			return wileyDefaultOrderCancelabilityStrategy.isCancelableOrderEntry(order, entryState);
		}
	}

	/**
	 * [OOTB code duplication] Source: {de.hybris.platform.assistedservicefacades.impl.DefaultAssistedServiceFacade}
	 *
	 * Changes made: no changes, we don't have an access to facades from core layer
	 */
	private boolean isAsmSession()
	{
		return assistedServiceService.getAsmSession() != null;
	}

}
