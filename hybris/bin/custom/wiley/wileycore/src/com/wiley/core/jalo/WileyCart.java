package com.wiley.core.jalo;

import de.hybris.platform.jalo.c2l.Currency;
import de.hybris.platform.jalo.order.Cart;
import de.hybris.platform.util.TaxValue;


/**
 * Created by Uladzimir_Barouski on 3/10/2016.
 */
public class WileyCart extends Cart
{
	/**
	 * Overrides calculateAbsoluteTotalTaxValue method to calculate taxValue for 1 unit of entry
	 *  ignore entry quantity. External tax service returns tax considering quantity.
	 *
	 * @param curr
	 * @param currencyIso
	 * @param digits
	 * @param net
	 * @param tax
	 * @param cumulatedEntryQuantities
	 * @return TaxValue
	 */
	@Override
	protected TaxValue calculateAbsoluteTotalTaxValue(final Currency curr, final String currencyIso, int digits, boolean net,
			final TaxValue tax, double cumulatedEntryQuantities) {
		TaxValue taxValue = tax;
		String taxValueIsoCode = taxValue.getCurrencyIsoCode();
		if (taxValueIsoCode != null && !currencyIso.equalsIgnoreCase(taxValueIsoCode))
		{
			Currency taxCurrency = this.getSession().getC2LManager().getCurrencyByIsoCode(taxValueIsoCode);
			taxValue = new TaxValue(taxValue.getCode(), taxCurrency.convertAndRound(curr, taxValue.getValue()), true, 0.0D,
					currencyIso);
		}
		//As we use external tax service to calculate tax for specified quantity we should apply taxValue for 1 unit of entry
		return taxValue.apply(1.0, 0.0D, digits, net, currencyIso);
	}
}
