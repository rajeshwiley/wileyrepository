package com.wiley.core.integration.impl;

import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.session.Session;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;

import com.google.common.collect.Queues;
import com.wiley.core.integration.ExternalCartModification;
import com.wiley.core.integration.ExternalCartModificationsStorageService;


/**
 * Thread-safe implementation. The storage has a hybris session scope. It means that cart modifications are not share
 * between hybris sessions and, if a hybris session is invalidated and removed,
 * all stored modifications will be removed as wel.<br/>
 */
public class WileyExternalCartModificationsStorageService implements ExternalCartModificationsStorageService
{

	private static final String EXTERNAL_CART_MODIFICATIONS_MAP_SESSION_ATTRIBUTE = "externalCartModificationsMap";

	private static final String ENABLE_EXTERNAL_CART_MODIFICATIONS_STORAGE_ATTRIBUTE = "enableExternalCartModificationsStorage";
	private static final String CART_MODEL = "cartModel";

	@Resource
	private SessionService sessionService;

	@Override
	public void pushAll(@Nonnull final CartModel cartModel,
			@Nonnull final List<ExternalCartModification> externalCartModifications)
	{
		ServicesUtil.validateParameterNotNullStandardMessage(CART_MODEL, cartModel);
		ServicesUtil.validateParameterNotNullStandardMessage("externalCartModifications", externalCartModifications);

		if (isStorageEnabledForCurrentSession() && !externalCartModifications.isEmpty())
		{
			final ConcurrentMap<String, Deque<ExternalCartModification>> externalModificationsMap =
					getOrInitExternalCartModificationMap();

			final String mapKey = getMapKey(cartModel);
			Deque<ExternalCartModification> modifications = externalModificationsMap
					.computeIfAbsent(mapKey, key -> createSynchronizedDeque());

			// needed to reduce size of modifications in the deque, refresh order of elements and remove possible duplicates
			synchronized (modifications)
			{
				modifications.removeAll(externalCartModifications);
				modifications.addAll(externalCartModifications);
			}
		}
	}

	@Override
	@Nonnull
	public List<ExternalCartModification> popAll(@Nonnull final CartModel cartModel)
	{
		ServicesUtil.validateParameterNotNullStandardMessage(CART_MODEL, cartModel);

		List<ExternalCartModification> resultList = Collections.emptyList();

		if (isStorageEnabledForCurrentSession())
		{
			final Map<String, Deque<ExternalCartModification>> modificationsMap = getExternalCartModificationsMap(
					sessionService.getCurrentSession());

			if (MapUtils.isNotEmpty(modificationsMap))
			{
				final String mapKey = getMapKey(cartModel);
				final Deque<ExternalCartModification> modifications = modificationsMap.get(mapKey);
				if (CollectionUtils.isNotEmpty(modifications))
				{
					synchronized (modifications)
					{
						resultList = new ArrayList<>(modifications);
						modifications.clear();
					}
				}
			}
		}
		return resultList;
	}

	@Override
	@Nonnull
	public Optional<ExternalCartModification> popLastModificationForCartAndEntry(@Nonnull final CartModel cartModel,
			@Nonnull final CartEntryModel cartEntryModel)
	{
		ServicesUtil.validateParameterNotNullStandardMessage(CART_MODEL, cartModel);
		ServicesUtil.validateParameterNotNullStandardMessage("cartEntryModel", cartEntryModel);
		ServicesUtil.validateParameterNotNullStandardMessage("cartEntryModel.product", cartEntryModel.getProduct());
		ServicesUtil.validateParameterNotNullStandardMessage("cartEntryModel.entryNumber", cartEntryModel.getEntryNumber());

		Optional<ExternalCartModification> resultObject = Optional.empty();

		if (isStorageEnabledForCurrentSession())
		{
			final ConcurrentMap<String, Deque<ExternalCartModification>> modificationsMap =
					getExternalCartModificationsMap(sessionService.getCurrentSession());

			if (MapUtils.isNotEmpty(modificationsMap))
			{
				final String mapKey = getMapKey(cartModel);
				final Deque<ExternalCartModification> modifications = modificationsMap.get(mapKey);
				if (CollectionUtils.isNotEmpty(modifications))
				{
					synchronized (modifications) {
						resultObject = popLastModificationForCartEntry(cartEntryModel, modifications);
					}
				}
			}
		}
		return resultObject;
	}

	@Override
	public void enableStorageForCurrentSession()
	{
		sessionService.setAttribute(ENABLE_EXTERNAL_CART_MODIFICATIONS_STORAGE_ATTRIBUTE, Boolean.TRUE);
	}

	@Override
	public void disableStorageForCurrentSession()
	{
		sessionService.removeAttribute(ENABLE_EXTERNAL_CART_MODIFICATIONS_STORAGE_ATTRIBUTE);
	}

	private Optional<ExternalCartModification> popLastModificationForCartEntry(final CartEntryModel cartEntryModel,
			final Deque<ExternalCartModification> modifications)
	{
		Optional<ExternalCartModification> resultObject = Optional.empty();
		Iterator<ExternalCartModification> descendingIterator = modifications.descendingIterator();
		while (descendingIterator.hasNext())
		{
			final ExternalCartModification modification = descendingIterator.next();
			if (isModificationForCartEntry(modification, cartEntryModel))
			{
				descendingIterator.remove();
				resultObject = Optional.of(modification);
				break;
			}
		}
		return resultObject;
	}

	private boolean isModificationForCartEntry(final ExternalCartModification modification, final CartEntryModel cartEntryModel)
	{
		return modification.getEntryNumber().equals(cartEntryModel.getEntryNumber()) && modification.getProductCode()
				.equals(cartEntryModel.getProduct().getCode());
	}

	private boolean isStorageEnabledForCurrentSession()
	{
		return Boolean.TRUE.equals(sessionService.getAttribute(ENABLE_EXTERNAL_CART_MODIFICATIONS_STORAGE_ATTRIBUTE));
	}

	private ConcurrentMap<String, Deque<ExternalCartModification>> getOrInitExternalCartModificationMap()
	{
		MapSessionAttribute<String, Deque<ExternalCartModification>> attribute = sessionService.getOrLoadAttribute(
				EXTERNAL_CART_MODIFICATIONS_MAP_SESSION_ATTRIBUTE,
				() -> new MapSessionAttribute<>(createConcurrentMap()));
		return attribute.getMap();
	}

	private ConcurrentMap<String, Deque<ExternalCartModification>> getExternalCartModificationsMap(final Session currentSession)
	{
		MapSessionAttribute<String, Deque<ExternalCartModification>> attribute = currentSession.getAttribute(
				EXTERNAL_CART_MODIFICATIONS_MAP_SESSION_ATTRIBUTE);
		return attribute != null ? attribute.getMap() : null;
	}

	private String getMapKey(final CartModel cartModel)
	{
		return cartModel.getPk().toString();
	}

	private Deque<ExternalCartModification> createSynchronizedDeque()
	{
		// Assumption: only one thread per cart modifies the deque.
		// It is a rare case when more then one thread in the same session modify the same cart.
		return Queues.synchronizedDeque(new ArrayDeque<>());
	}

	private ConcurrentMap<String, Deque<ExternalCartModification>> createConcurrentMap()
	{
		return new ConcurrentHashMap<>();
	}

	// this class is necessary to resolve OOTB bug when map is not stored correctly in session.
	private static class MapSessionAttribute<K, V>
	{
		private ConcurrentMap<K, V> map;

		MapSessionAttribute(final ConcurrentMap<K, V> map)
		{
			this.map = map;
		}

		ConcurrentMap<K, V> getMap()
		{
			return map;
		}
	}
}
