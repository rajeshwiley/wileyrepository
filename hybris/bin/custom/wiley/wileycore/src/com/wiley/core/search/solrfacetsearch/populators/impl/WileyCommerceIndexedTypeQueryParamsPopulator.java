package com.wiley.core.search.solrfacetsearch.populators.impl;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.model.config.SolrIndexedTypeModel;

public class WileyCommerceIndexedTypeQueryParamsPopulator implements Populator<SolrIndexedTypeModel, IndexedType>
{
	@Override
	public void populate(final SolrIndexedTypeModel solrIndexedTypeModel, final IndexedType indexedType)
			throws ConversionException
	{
		indexedType.setMinimumShouldMatch(solrIndexedTypeModel.getMinimumShouldMatch());
		indexedType.setBoostFunction(solrIndexedTypeModel.getBoostFunction());
		indexedType.setSpellcheckAccuracy(solrIndexedTypeModel.getSpellcheckAccuracy());
		indexedType.setHighlightEnabled(solrIndexedTypeModel.isHighlightEnabled());
	}
}
