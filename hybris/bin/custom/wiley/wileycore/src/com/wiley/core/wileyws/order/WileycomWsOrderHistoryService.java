package com.wiley.core.wileyws.order;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderhistory.OrderHistoryService;


/**
 * The interface Wileycom ws order history service
 * for managing history entries.
 */
public interface WileycomWsOrderHistoryService extends OrderHistoryService
{

	/**
	 * Create history entry with link to order.
	 *
	 * @param orderModel
	 * 		the order model
	 * @param description
	 * 		the description
	 */
	void createHistoryEntry(OrderModel orderModel, String description);

	/**
	 * Create history entry wilth link to original order and snapshot.
	 *
	 * @param orderModel
	 * 		the order model
	 * @param snapshot
	 * 		the snapshot
	 * @param description
	 * 		the description
	 */
	void createHistoryEntry(OrderModel orderModel, OrderModel snapshot, String description);
}
