package com.wiley.core.wiley.customer;

import de.hybris.platform.commerceservices.customer.CustomerEmailResolutionService;
import de.hybris.platform.core.model.user.CustomerModel;

import org.apache.commons.lang.StringUtils;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;


public class WileyCustomerEmailResolutionService implements CustomerEmailResolutionService
{

	@Override
	public String getEmailForCustomer(final CustomerModel customerModel)
	{
		validateParameterNotNullStandardMessage("customerModel", customerModel);

		if (StringUtils.isNotEmpty(customerModel.getEmailAddress()))
		{
			return customerModel.getEmailAddress();
		}
		return customerModel.getUid();
	}

}
