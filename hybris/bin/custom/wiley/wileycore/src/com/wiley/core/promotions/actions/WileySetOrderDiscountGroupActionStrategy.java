package com.wiley.core.promotions.actions;

import com.wiley.core.model.actions.WileySetOrderDiscountGroupActionModel;
import com.wiley.core.promotions.actions.rao.WileySetOrderDiscountGroupRAO;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.europe1.enums.UserDiscountGroup;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.promotionengineservices.action.impl.AbstractRuleActionStrategy;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.ruleengineservices.rao.AbstractRuleActionRAO;
import de.hybris.platform.ruleengineservices.rao.CartRAO;
import org.apache.commons.lang.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;

public abstract class WileySetOrderDiscountGroupActionStrategy
		extends AbstractRuleActionStrategy<WileySetOrderDiscountGroupActionModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(WileySetOrderDiscountGroupActionStrategy.class);

	@Override
	public List<? extends ItemModel> apply(final AbstractRuleActionRAO action)
	{
		if (!(action instanceof WileySetOrderDiscountGroupRAO))
		{
			LOG.error("cannot apply {}, action is not of type WileyApplyPartnerDiscountRAO", this.getClass().getSimpleName());
			return Collections.emptyList();
		}

		final WileySetOrderDiscountGroupRAO applyPartnerDiscountRAO = (WileySetOrderDiscountGroupRAO) action;
		if (!(applyPartnerDiscountRAO.getAppliedToObject() instanceof CartRAO))
		{
			LOG.error("cannot apply {}, appliedToObject is not of type CartRAO, but {}", getClass().getSimpleName(),
					action.getAppliedToObject());
			return Collections.emptyList();
		}

		final PromotionResultModel promoResult = getPromotionActionService().createPromotionResult(action);
		if (promoResult == null)
		{
			LOG.error("cannot apply {}, promotionResult could not be created.", this.getClass().getSimpleName());
			return Collections.emptyList();
		}

		final AbstractOrderModel order = promoResult.getOrder();
		if (order == null)
		{
			LOG.error("cannot apply {}, order not found", this.getClass().getSimpleName());
			detachResult(promoResult);
			return Collections.emptyList();
		}


		UserDiscountGroup discountGroup = getDiscountGroup(order);
		if (discountGroup == null)
		{
			LOG.error("cannot apply {}, no discount group is found", this.getClass().getSimpleName());
			detachResult(promoResult);
			return Collections.emptyList();
		}

		order.setDiscountGroup(discountGroup);

		final WileySetOrderDiscountGroupActionModel actionModel = createPromotionAction(promoResult, action);
		actionModel.setPromotionResult(promoResult);
		actionModel.setOrder(order);

		getModelService().saveAll(promoResult, actionModel, order);
		getModelService().refresh(order);

		this.recalculateIfNeeded(order);


		return Collections.singletonList(promoResult);
	}

	private void detachResult(final PromotionResultModel promoResult)
	{
		// detach the promotion result if its not saved yet.
		if (getModelService().isNew(promoResult))
		{
			getModelService().detach(promoResult);
		}
	}


	@Override
	public void undo(final ItemModel item)
	{
		if (item instanceof WileySetOrderDiscountGroupActionModel)
		{
			final WileySetOrderDiscountGroupActionModel action = (WileySetOrderDiscountGroupActionModel) item;
			final AbstractOrderModel order = action.getOrder();
			order.setDiscountGroup(null);
			undoInternal(action);
			getModelService().save(order);
			getModelService().refresh(order);
			recalculateIfNeeded(order);
		}
	}

	protected abstract UserDiscountGroup getDiscountGroup(AbstractOrderModel order);

	@Override
	protected boolean recalculateIfNeeded(final AbstractOrderModel order)
	{
		if (BooleanUtils.isTrue(this.getForceImmediateRecalculation()))
		{
			try
			{
				getCalculationService().recalculate(order);
			} catch (CalculationException ex)
			{
				LOG.error(String.format("Recalculation of order with code '%s' failed.", order.getCode()), ex);
				order.setCalculated(Boolean.FALSE);
				this.getModelService().save(order);
				return false;
			}
		}

		return true;
	}
}
