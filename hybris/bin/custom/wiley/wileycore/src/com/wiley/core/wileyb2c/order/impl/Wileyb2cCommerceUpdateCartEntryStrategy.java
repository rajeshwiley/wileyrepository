package com.wiley.core.wileyb2c.order.impl;

import de.hybris.platform.commerceservices.order.impl.DefaultCommerceUpdateCartEntryStrategy;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;


/**
 *
 */
public class Wileyb2cCommerceUpdateCartEntryStrategy extends DefaultCommerceUpdateCartEntryStrategy
{
	/**
	 * overridden to allow to buy products which are out of stock
	 */
	@Override
	protected long getAllowedCartAdjustmentForProduct(final CartModel cartModel, final ProductModel productModel,
			final long quantityToAdd, final PointOfServiceModel pointOfServiceModel)
	{
		return quantityToAdd;
	}
}
