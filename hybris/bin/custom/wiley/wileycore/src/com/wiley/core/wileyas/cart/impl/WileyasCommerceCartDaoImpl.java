package com.wiley.core.wileyas.cart.impl;

import com.wiley.core.cart.impl.WileyCommerceCartDaoImpl;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.order.CartModel;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WileyasCommerceCartDaoImpl extends WileyCommerceCartDaoImpl
{
    protected static final String FIND_ABANDON_CARTS_FOR_SITE = SELECTCLAUSE_ABANDON_CART
            + "WHERE {c." + CartModel.MODIFIEDTIME + "} <= ?modifiedBefore AND {c." + CartModel.SITE + "} = ?site "
            + NOT_ANONYMOUS_CLAUSE + ORDERBYCLAUSE;

    @Override
    public List<CartModel> getAbandonCarts(final Date modifiedBefore, final BaseSiteModel site)
    {
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("site", site);
        params.put("modifiedBefore", modifiedBefore);
        return doSearch(FIND_ABANDON_CARTS_FOR_SITE, params, CartModel.class);
    }
}
