package com.wiley.core.validation.constraintvalidators;

import de.hybris.platform.core.model.product.ProductModel;

import javax.validation.ConstraintValidatorContext;

import com.wiley.core.validation.constraints.OnlineIssnHasRequiredFormat;


public class OnlineIssnHasRequiredFormatValidator
		extends AbstractWileyProductConstraintValidator<OnlineIssnHasRequiredFormat>
{
	@Override
	public boolean isValid(final ProductModel productModel, final ConstraintValidatorContext constraintValidatorContext)
	{
		return getValidationService().validateOnlineIssnHasRequiredFormat(productModel.getFeatures());
	}
}
