package com.wiley.core.wileyb2c.sitemap.generator.impl;


import de.hybris.platform.acceleratorservices.sitemap.data.SiteMapUrlData;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.util.FlexibleSearchUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.enums.RobotsMetaTag;
import com.wiley.core.strategies.robotsmetatag.RobotsMetatagResolverService;


public class Wileyb2cContentPageSiteMapGenerator extends AbstractWileyb2cSiteMapGenerator<ContentPageModel>
{
	private static final String NOINDEX = "NOINDEX";
	private RobotsMetatagResolverService robotsMetatagResolverService;
	private CMSPageService cmsPageService;

	@Override
	public List<SiteMapUrlData> getSiteMapUrlData(final List<ContentPageModel> models)
	{
		return Converters.convertAll(models, getSiteMapUrlDataConverter());
	}

	@Override
	protected List<ContentPageModel> getDataInternal(final CMSSiteModel siteModel)
	{
		final Map<String, Object> params = new HashMap<String, Object>();

		final StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append("SELECT {cp." + ContentPageModel.PK + "} FROM {" + ContentPageModel._TYPECODE
				+ " AS cp} WHERE ");
		queryBuilder
				.append(FlexibleSearchUtils.buildOracleCompatibleCollectionStatement("{cp." + ContentPageModel.CATALOGVERSION
								+ "} in (?catalogVersions)", "catalogVersions", "OR",
						getCatalogVersionService().getSessionCatalogVersions(),
						params));

		List<ContentPageModel> contentPages = doSearch(queryBuilder.toString(), params, ContentPageModel.class);

		List<ContentPageModel> availableContentPages = filterOutPagesWithNoIndexRobotsMetagTag(contentPages);

		return availableContentPages;
	}

	private List<ContentPageModel> filterOutPagesWithNoIndexRobotsMetagTag(final List<ContentPageModel> contentPages)
	{
		List<ContentPageModel> availableContentPages = new ArrayList<>(contentPages);
		List<ContentPageModel> noIndexPages = new ArrayList<>();

		availableContentPages.stream()
				.filter(page -> page.equals(cmsPageService.getHomepage()))
				.forEach(noIndexPages::add);

		availableContentPages.stream()
				.filter(page -> robotsMetatagResolverService.needResolveByPageType(page))
				.filter(page -> robotsMetatagResolverService.resolveByPageType(page).contains(NOINDEX))
				.forEach(noIndexPages::add);

		availableContentPages.stream()
				.filter(page -> robotsMetatagResolverService.needResolveByPageUid(page))
				.filter(page -> robotsMetatagResolverService.resolveByPageUid(page.getUid()).contains(NOINDEX))
				.forEach(noIndexPages::add);

		availableContentPages.stream()
				.filter(page -> page.getRobotsMetaTag().equals(RobotsMetaTag.NOINDEX_NOFOLLOW) || page.getRobotsMetaTag()
						.equals(RobotsMetaTag.NOINDEX_FOLLOW))
				.forEach(noIndexPages::add);

		availableContentPages.removeAll(noIndexPages);

		return availableContentPages;
	}

	@Required
	public void setRobotsMetatagResolverService(final RobotsMetatagResolverService robotsMetatagResolverService)
	{
		this.robotsMetatagResolverService = robotsMetatagResolverService;
	}

	@Required
	public void setCmsPageService(final CMSPageService cmsPageService)
	{
		this.cmsPageService = cmsPageService;
	}
}
