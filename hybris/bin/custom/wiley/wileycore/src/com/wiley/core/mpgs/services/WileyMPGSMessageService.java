package com.wiley.core.mpgs.services;

import de.hybris.platform.payment.model.PaymentTransactionEntryModel;


public interface WileyMPGSMessageService
{

	String getStorefrontMessageKey(PaymentTransactionEntryModel paymentEntry);

	String getAuthorizationStorefrontMessageKey(PaymentTransactionEntryModel paymentEntry);
}
