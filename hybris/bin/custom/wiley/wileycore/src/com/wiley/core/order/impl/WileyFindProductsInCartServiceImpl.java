package com.wiley.core.order.impl;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;

import com.wiley.core.model.WileyProductVariantSetModel;
import com.wiley.core.order.WileyFindProductsInCartService;
import com.wiley.core.product.WileyProductService;


public class WileyFindProductsInCartServiceImpl implements WileyFindProductsInCartService
{
	@Resource
	private WileyProductService productService;

	@Override
	public List<AbstractOrderEntryModel> getEntriesForProducts(final AbstractOrderModel cart,
			final Collection<ProductModel> products)
	{
		final List<AbstractOrderEntryModel> entries = cart.getEntries();
		return entries.stream()
				.filter(entry -> products.contains(entry.getProduct()))
				.collect(Collectors.toList());
	}

	private List<ProductModel> getProductsForCategory(final CategoryModel category)
	{
		List<ProductModel> allProducts = new ArrayList<ProductModel>();
		final List<ProductModel> productsForCategory = productService.getProductsForCategory(category);
		for (ProductModel product : productsForCategory)
		{
			if (product instanceof WileyProductVariantSetModel)
			{
				allProducts.addAll(((WileyProductVariantSetModel) product).getProducts());
			}
			allProducts.add(product);
		}
		return allProducts;
	}


	@Override
	public boolean containsAtLeastOneProduct(final AbstractOrderModel abstractOrder, final CategoryModel category)
	{
		boolean hasProducts = false;
		final List<ProductModel> productsForCategory = getProductsForCategory(category);
		if (CollectionUtils.isNotEmpty(productsForCategory))
		{
			hasProducts = containsAtLeastOneProduct(abstractOrder, productsForCategory);
		}
		return hasProducts;
	}

	@Override
	public boolean containsAtLeastOneProduct(final AbstractOrderModel abstractOrder, final Collection<ProductModel> products)
	{
		boolean hasProducts = false;
		if (CollectionUtils.isNotEmpty(products))
		{
			if (orderHasAProduct(abstractOrder, products))
			{
				hasProducts = true;
			}
			else if (orderHasAVariantProduct(abstractOrder, products))
			{
				hasProducts = true;
			}
		}
		return hasProducts;
	}

	private boolean orderHasAVariantProduct(final AbstractOrderModel abstractOrder,
			final Collection<ProductModel> baseProducts)
	{
		return baseProducts.stream()
				.filter(baseProduct -> CollectionUtils.isNotEmpty(baseProduct.getVariants()))
				.filter(baseProduct -> orderHasAProduct(abstractOrder, baseProduct.getVariants()))
				.findAny().isPresent();
	}

	private boolean orderHasAProduct(final AbstractOrderModel abstractOrder,
			final Collection<? extends ProductModel> products)
	{
		return CollectionUtils.isNotEmpty(getEntriesForProducts(abstractOrder, new ArrayList(products)));
	}
}
