package com.wiley.core.integration.esb;

import de.hybris.platform.core.model.order.CartModel;

import java.util.Map;

import com.wiley.core.product.data.ExternalInventoryStatus;


/**
 * Created by Mikhail_Asadchy on 7/27/2016.
 */
public interface EsbRealTimeInventoryCheckGateway
{

	/**
	 * Performs a call to external system to check inventory of products into the cart
	 *
	 * @param cartModel
	 * 		- cart to check inventory
	 * @return - map of sapProductCoeds and list of their inventory statuses
	 */
	Map<String, ExternalInventoryStatus> performRealTimeInventoryCheck(CartModel cartModel);
}
