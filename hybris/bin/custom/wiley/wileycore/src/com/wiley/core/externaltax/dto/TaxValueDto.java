package com.wiley.core.externaltax.dto;

public class TaxValueDto
{
	private String code;
	private Double value;

	public String getCode()
	{
		return code;
	}

	public void setCode(final String code)
	{
		this.code = code;
	}

	public Double getValue()
	{
		return value;
	}

	public void setValue(final Double value)
	{
		this.value = value;
	}
}
