package com.wiley.core.integration.article.service.impl;

import java.util.Optional;

import javax.annotation.Nonnull;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.exceptions.ExternalSystemException;
import com.wiley.core.integration.article.ArticleGateway;
import com.wiley.core.integration.article.dto.ArticleDto;
import com.wiley.core.integration.article.service.ArticleService;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public class ArticleServiceImpl implements ArticleService
{
	private static final Logger LOG = Logger.getLogger(ArticleServiceImpl.class);
	@Autowired
	private ArticleGateway articleGateway;

	@Nonnull
	@Override
	public Optional<ArticleDto> getArticleData(@Nonnull final String articleId)
	{
		try
		{
			return Optional.of(articleGateway.getArticleData(articleId));
		}
		catch (ExternalSystemException ex)
		{
			LOG.info(ex.getMessage(), ex);
			return Optional.empty();
		}
	}
}
