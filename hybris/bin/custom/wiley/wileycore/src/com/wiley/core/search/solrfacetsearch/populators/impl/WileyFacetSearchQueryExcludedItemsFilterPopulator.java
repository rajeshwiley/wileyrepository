package com.wiley.core.search.solrfacetsearch.populators.impl;

import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.solrfacetsearch.search.impl.SearchQueryConverterData;
import de.hybris.platform.solrfacetsearch.search.impl.populators.AbstractFacetSearchQueryPopulator;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.solr.client.solrj.SolrQuery;


/**
 * Changes the logic of OOTB
 * de.hybris.platform.solrfacetsearch.search.impl.populators.FacetSearchQueryExcludedItemsFilterPopulator
 * populator to filter excluded products based on base product code
 */
public class WileyFacetSearchQueryExcludedItemsFilterPopulator extends AbstractFacetSearchQueryPopulator
{
	@Resource
	private ModelService modelService;

	public void populate(final SearchQueryConverterData source, final SolrQuery target)
	{
		SearchQuery searchQuery = source.getSearchQuery();
		List<PK> excludedItems = searchQuery.getExcludedItems();
		if (CollectionUtils.isNotEmpty(excludedItems))
		{
			String filterQuery = "-baseProductCode_string" + (String) excludedItems.stream().map(pk -> {
				Object o = modelService.get(pk);
				ProductModel productModel = null;
				if (o instanceof ProductModel)
				{
					productModel = (ProductModel) o;
				}
				return productModel != null ? productModel.getCode() : "";
			}).collect(
					Collectors.joining(" OR ", ":(", ")"));
			target.addFilterQuery(new String[] { filterQuery });
		}

	}
}

