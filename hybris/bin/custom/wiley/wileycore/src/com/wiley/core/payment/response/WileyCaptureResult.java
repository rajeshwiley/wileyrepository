package com.wiley.core.payment.response;

import com.wiley.core.payment.enums.WileyTransactionStatusEnum;
import de.hybris.platform.payment.commands.result.CaptureResult;

public class WileyCaptureResult extends CaptureResult
{
	private WileyTransactionStatusEnum status;
	private String merchantResponse;

	public WileyTransactionStatusEnum getStatus()
	{
		return status;
	}

	public void setStatus(final WileyTransactionStatusEnum status)
	{
		this.status = status;
	}

	public String getMerchantResponse()
	{
		return merchantResponse;
	}

	public void setMerchantResponse(final String merchantResponse)
	{
		this.merchantResponse = merchantResponse;
	}
}
