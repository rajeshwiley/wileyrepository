package com.wiley.core.util.url.impl;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.variants.model.VariantProductModel;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;

import com.wiley.core.category.WileyCategoryService;
import com.wiley.core.model.WileyFreeTrialProductModel;
import com.wiley.core.model.WileyGiftCardProductModel;
import com.wiley.core.util.url.UrlVisibilityStrategy;


public class WelProductUrlVisibilityStrategyImpl implements UrlVisibilityStrategy<ProductModel>
{
	@Autowired
	private WileyCategoryService wileyCategoryService;

	@Override
	public boolean isUrlForItemVisible(@NotNull final ProductModel product)
	{
		ServicesUtil.validateParameterNotNull(product, "Product couldn't be null");

		final CategoryModel category = wileyCategoryService.getPrimaryWileyCategoryForProduct(product);
		// hide explicit gift cards
		if (product instanceof WileyGiftCardProductModel)
		{
			return false;
		}
		// explicit show free trials
		if (product instanceof WileyFreeTrialProductModel)
		{
			return true;
		}
		// hide partners products
		if (wileyCategoryService.isPartnersCategory(category))
		{
			return false;
		}
		//Hide base products for cfa and variants for non-cfa
		if (wileyCategoryService.isCFACategory(category) ^ product instanceof VariantProductModel)
		{
			return false;
		}
		// default behavior
		return true;
	}
}
