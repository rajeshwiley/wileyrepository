/**
 *
 */
package com.wiley.core.wileyb2c.resolver.impl;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.url.impl.DefaultCategoryModelUrlResolver;


/**
 * Creates a custom URL for PLP.
 */
public class CustomPlpLinkUrlResolver extends DefaultCategoryModelUrlResolver
{
	private static final String CATEGORY_SEO_NAME = "{category-seo-name}";
	private static final String CATEGORY_CODE = "{category-code}";

	@Override
	protected String resolveInternal(final CategoryModel category)
	{
		String resolvedUrl;
		String patternToUrl = getPattern();

		patternToUrl = resolveCategorySeoName(category, patternToUrl);
		patternToUrl = resolveCategoryCode(category, patternToUrl);

		resolvedUrl = patternToUrl;

		return resolvedUrl;
	}

	private String resolveCategorySeoName(final CategoryModel category, final String url)
	{
		final String categorySeoName = category.getSeoName() != null ? category.getSeoName() : category.getName();
		return url.replace(CATEGORY_SEO_NAME, urlSafe(categorySeoName));
	}

	private String resolveCategoryCode(final CategoryModel category, final String url)
	{
		final String categoryCode = urlEncode(category.getCode()).replaceAll("\\+", "%20");
		return url.replace(CATEGORY_CODE, categoryCode);
	}
}
