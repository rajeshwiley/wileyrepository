package com.wiley.core.wiley.restriction.impl;

import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import javax.annotation.Resource;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.wiley.restriction.WileyRestrictProductStrategy;
import com.wiley.core.wiley.restriction.dto.WileyRestrictionCheckResultDto;


/**
 * @author Dzmitryi_Halahayeu
 */
public class WileyRestrictProductBySearchRestrictionsStrategy implements WileyRestrictProductStrategy
{
	@Resource
	private ProductService productService;

	@Override
	public boolean isRestricted(final ProductModel product)
	{
		try
		{
			productService.getProductForCode(product.getCode());
		}
		catch (final UnknownIdentifierException e)
		{
			return true;
		}
		
		/*
			if method is called for group different from customergroup (hmc, productcockpit), 
			catalogVersion restriction isn't applied and we would get this exception.     
		 */
		catch (final AmbiguousIdentifierException e)
		{
			return false;
		}
		return false;
	}

	@Override
	public boolean isRestricted(final CommerceCartParameter parameter)
	{
		return isRestricted(parameter.getProduct());
	}


	@Override
	public WileyRestrictionCheckResultDto createErrorResult(final CommerceCartParameter parameter)
	{
		return WileyRestrictionCheckResultDto.failureResult(WileyCoreConstants.PRODUCT_IS_NOT_PURCHASABLE,
				String.format("Product [%s] is not available due to flexible search restrictions",
						parameter.getProduct().getCode()), null);
	}

}
