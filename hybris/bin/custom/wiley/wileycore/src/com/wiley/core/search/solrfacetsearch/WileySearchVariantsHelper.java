package com.wiley.core.search.solrfacetsearch;

import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.search.Document;
import de.hybris.platform.solrfacetsearch.search.FacetSearchException;
import de.hybris.platform.solrfacetsearch.search.FieldNameTranslator;
import de.hybris.platform.solrfacetsearch.search.QueryField;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.solrfacetsearch.search.SearchResult;
import de.hybris.platform.solrfacetsearch.search.SearchResultGroup;
import de.hybris.platform.solrfacetsearch.search.SearchResultGroupCommand;
import de.hybris.platform.solrfacetsearch.search.context.FacetSearchContext;
import de.hybris.platform.solrfacetsearch.search.context.FacetSearchContextFactory;
import de.hybris.platform.solrfacetsearch.search.impl.SearchQueryConverterData;
import de.hybris.platform.solrfacetsearch.search.impl.SearchResultConverterData;
import de.hybris.platform.solrfacetsearch.solr.Index;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrRequest;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Required;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * @author Dzmitryi_Halahayeu
 */
public class WileySearchVariantsHelper
{
	private static final Logger LOG = Logger.getLogger(WileySearchVariantsHelper.class);
	private static final String PARAM_HIGHLIGHT_FIELDS = "hl.fl";
	private static final String PARAM_HIGHLIGHT_QUERY = "hl.q";
	private static final String PARAM_ENABLE_HIGHLIGHT = "hl";
	private static final String PARAM_FRAGMENT_SIZE = "hl.fragsize";
	private static final String PARAM_HIGHLIGHT_PRE_TAG = "hl.simple.pre";
	private static final String PARAM_HIGHLIGHT_POST_TAG = "hl.simple.post";
	private static final String PARAM_HIGHLIGHT_PERSERVE_MULTI = "hl.preserveMulti";

	private FieldNameTranslator fieldNameTranslator;
	private Converter<SearchResultConverterData, SearchResult> facetSearchResultConverter;
	private Converter<SearchQueryConverterData, SolrQuery> facetSearchQueryConverter;
	private FacetSearchContextFactory<FacetSearchContext> facetSearchContextFactory;

	private String highlightPreTag;
	private String highlightPostTag;

	private Integer fragSizeLimit;

	public void queryForVariants(final SolrClient solrClient, final Index index,
			final SearchResult baseProductSearchResult, final SearchQuery searchQuery)
			throws SolrServerException, IOException, FacetSearchException
	{
		final SearchResultGroupCommand searchResultGroupCommand = createSearchResultGroupCommand(baseProductSearchResult);
		if (searchResultGroupCommand.getGroups().size() > 0)
		{
			final SearchQuery variantSearchQuery = prepareSearchQuery((WileySearchQuery) searchQuery, searchResultGroupCommand);
			final Map<String, List<Document>> documentsByGroupValues = sendRequest(solrClient, index, variantSearchQuery);
			populateVariants(documentsByGroupValues, searchResultGroupCommand);
		}
	}

	private void populateVariants(final Map<String, List<Document>> documentsByGroupValues,
			final SearchResultGroupCommand searchResultGroupCommand)
	{

		searchResultGroupCommand.getGroups().forEach(searchResultGroup ->
		{
			searchResultGroup.getDocuments().clear();
			final List<Document> variants = documentsByGroupValues.get(searchResultGroup.getGroupValue());
			variants.forEach(variant -> searchResultGroup.getDocuments().add(variant));
		});
	}

	private Map<String, List<Document>> sendRequest(final SolrClient solrClient, final Index index, final SearchQuery searchQuery)
			throws SolrServerException, IOException, FacetSearchException
	{
		final FacetSearchContext facetSearchContext = this.facetSearchContextFactory.createContext(
				searchQuery.getFacetSearchConfig(), searchQuery.getIndexedType(), searchQuery);
		this.facetSearchContextFactory.initializeContext();
		final SearchQueryConverterData variantSearchQueryConverterData = new SearchQueryConverterData();
		variantSearchQueryConverterData.setFacetSearchContext(facetSearchContext);
		variantSearchQueryConverterData.setSearchQuery(searchQuery);
		final SolrQuery variantsSolrQuery = this.facetSearchQueryConverter.convert(variantSearchQueryConverterData);
		if (LOG.isDebugEnabled())
		{
			LOG.debug(variantsSolrQuery);
		}
		final QueryResponse variantQueryResponse = solrClient.query(index.getName(), variantsSolrQuery, SolrRequest.METHOD.POST);
		final SearchResultConverterData variantsResultConverterData = new SearchResultConverterData();
		variantsResultConverterData.setFacetSearchContext(facetSearchContext);
		variantsResultConverterData.setQueryResponse(variantQueryResponse);
		final SearchResult variantsSearchResult = this.facetSearchResultConverter.convert(variantsResultConverterData);

		this.facetSearchContextFactory.getContext().setSearchResult(variantsSearchResult);
		this.facetSearchContextFactory.destroyContext();

		Assert.assertNotNull(variantsSearchResult.getGroupCommands());
		Assert.assertTrue(variantsSearchResult.getGroupCommands().size() == 1);

		return variantsSearchResult.getGroupCommands().get(0).getGroups()
				.stream().collect(Collectors.toMap(SearchResultGroup::getGroupValue, SearchResultGroup::getDocuments));


	}

	private SearchQuery prepareSearchQuery(final WileySearchQuery searchQuery,
			final SearchResultGroupCommand searchResultGroupCommand)
	{
		final WileySearchQuery variantsSearchQuery = new WileySearchQuery(searchQuery.getFacetSearchConfig(),
				searchQuery.getIndexedType());
		searchQuery.getQueryFacets().forEach(variantsSearchQuery::addQueryFacet);
		variantsSearchQuery.setCatalogVersions(searchQuery.getCatalogVersions());
		variantsSearchQuery.setLanguage(searchQuery.getLanguage());
		variantsSearchQuery.setCurrency(searchQuery.getCurrency());
		variantsSearchQuery.setOffset(0);
		variantsSearchQuery.setPageSize(searchQuery.getPageSize());
		variantsSearchQuery.setDefaultOperator(searchQuery.getDefaultOperator());
		searchQuery.getFilterQueries().forEach(variantsSearchQuery::addFilterQuery);
		searchQuery.getFilterRawQueries().forEach(variantsSearchQuery::addFilterRawQuery);
		searchQuery.getGroupCommands().forEach(variantsSearchQuery::addGroupCommand);
		variantsSearchQuery.setGroupFacets(searchQuery.isGroupFacets());
		searchQuery.getFields().forEach(variantsSearchQuery::addField);
		searchQuery.getRawParams().forEach(variantsSearchQuery::addRawParam);
		enableSearchTermHighlight(searchQuery, variantsSearchQuery);
		variantsSearchQuery.addFilterQuery(new QueryField(searchResultGroupCommand.getName(), SearchQuery.Operator.OR,
				searchResultGroupCommand.getGroups().stream().map(SearchResultGroup::getGroupValue)
						.toArray(String[]::new)));
		return variantsSearchQuery;
	}

	private void enableSearchTermHighlight(final WileySearchQuery originalQuery, final WileySearchQuery variantsSearchQuery)
	{
		if (originalQuery.getIndexedType().isHighlightEnabled() && StringUtils.isNotBlank(originalQuery.getUserQuery()))
		{
			//enable highlight
			variantsSearchQuery.addRawParam(PARAM_ENABLE_HIGHLIGHT, Boolean.TRUE.toString());
			variantsSearchQuery.addRawParam(PARAM_FRAGMENT_SIZE, fragSizeLimit.toString());
			//add field list - same as for text query
			List<String> fields = originalQuery.getFreeTextPhraseQueries().stream()
					.map(field -> fieldNameTranslator.translate(originalQuery, field.getField(),
							FieldNameProvider.FieldType.INDEX))
					.collect(Collectors.toList());
			variantsSearchQuery.addRawParam(PARAM_HIGHLIGHT_FIELDS, StringUtils.join(fields, " "));
			//specify highlight query - use original user query
			variantsSearchQuery.addRawParam(PARAM_HIGHLIGHT_QUERY, originalQuery.getUserQuery());
			//pass tags to mark highlighted word
			variantsSearchQuery.addRawParam(PARAM_HIGHLIGHT_PRE_TAG, highlightPreTag);
			variantsSearchQuery.addRawParam(PARAM_HIGHLIGHT_POST_TAG, highlightPostTag);
			//Important parameter. If true, multi-valued fields will return all values in the order
			//they were saved in the index. Authors are multivalued field so we need this behavior to be enabled
			variantsSearchQuery.addRawParam(PARAM_HIGHLIGHT_PERSERVE_MULTI, Boolean.TRUE.toString());
		}
	}

	private SearchResultGroupCommand createSearchResultGroupCommand(final SearchResult baseProductSearchResult)
	{
		Assert.assertNotNull(baseProductSearchResult.getGroupCommands());
		Assert.assertTrue(baseProductSearchResult.getGroupCommands().size() == 1);
		return baseProductSearchResult.getGroupCommands().get(0);
	}

	@Required
	public void setFacetSearchResultConverter(
			final Converter<SearchResultConverterData, SearchResult> facetSearchResultConverter)
	{
		this.facetSearchResultConverter = facetSearchResultConverter;
	}

	@Required
	public void setFacetSearchQueryConverter(
			final Converter<SearchQueryConverterData, SolrQuery> facetSearchQueryConverter)
	{
		this.facetSearchQueryConverter = facetSearchQueryConverter;
	}

	@Required
	public void setFacetSearchContextFactory(
			final FacetSearchContextFactory<FacetSearchContext> facetSearchContextFactory)
	{
		this.facetSearchContextFactory = facetSearchContextFactory;
	}

	@Required
	public void setFieldNameTranslator(final FieldNameTranslator fieldNameTranslator)
	{
		this.fieldNameTranslator = fieldNameTranslator;
	}

	@Required
	public void setHighlightPostTag(final String highlightPostTag)
	{
		this.highlightPostTag = highlightPostTag;
	}

	@Required
	public void setHighlightPreTag(final String highlightPreTag)
	{
		this.highlightPreTag = highlightPreTag;
	}

	@Required
	public void setFragSizeLimit(final Integer fragSizeLimit)
	{
		this.fragSizeLimit = fragSizeLimit;
	}
}
