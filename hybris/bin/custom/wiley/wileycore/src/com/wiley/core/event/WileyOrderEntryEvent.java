package com.wiley.core.event;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;


public class WileyOrderEntryEvent extends AbstractEvent
{
	private String code;

	private AbstractOrderEntryModel entry;

	public WileyOrderEntryEvent(final String code, final AbstractOrderEntryModel entryModel)
	{
		this.code = code;
		this.entry = entryModel;
	}

	public String getCode()
	{
		return code;
	}

	public void setCode(final String code)
	{
		this.code = code;
	}

	public AbstractOrderEntryModel getEntry()
	{
		return entry;
	}

	public void setEntry(final AbstractOrderEntryModel entry)
	{
		this.entry = entry;
	}
}
