package com.wiley.core.wileyb2c.classification.impl;

import de.hybris.platform.classification.features.Feature;

import java.util.Collections;
import java.util.Set;

import com.google.common.base.Preconditions;
import com.wiley.core.wileyb2c.classification.Wileyb2cClassificationAttributes;


/**
 * @author Dzmitryi_Halahayeu
 */
public class Wileyb2cResolveIsbnStrategy extends AbstractWileyb2cResolveClassificationAttributeStrategy
{
	public static final Set<Wileyb2cClassificationAttributes> CLASSIFICATION_ATTRIBUTES =
			Collections.singleton(Wileyb2cClassificationAttributes.ISBN13);
	private static final String DASH = "-";
	private static final int FIRST_POSITION_INDEX = 3;
	private static final int SECOND_POSITION_INDEX = 5;
	private static final int THIRD_POSITION_INDEX = 9;
	private static final int FOURTH_POSITION_INDEX = 15;
	private static final int VALID_ISBN_LENGTH = 13;

	@Override
	protected String processFeature(final Feature feature)
	{
		String isbn = getIsbn(feature);
		StringBuilder sb = new StringBuilder(isbn);
		sb.insert(FIRST_POSITION_INDEX, DASH);
		sb.insert(SECOND_POSITION_INDEX, DASH);
		sb.insert(THIRD_POSITION_INDEX, DASH);
		sb.insert(FOURTH_POSITION_INDEX, DASH);
		return sb.toString();
	}

	@Override
	protected Set<Wileyb2cClassificationAttributes> getAttributes()
	{
		return CLASSIFICATION_ATTRIBUTES;
	}

	private String getIsbn(final Feature feature)
	{
		Preconditions.checkNotNull(feature.getValue().getValue(), "ISBN must not be null");
		String isbn = feature.getValue().getValue().toString();
		final String invalidLengthMessage = String.format("ISBN must contain %s symbols but has %s",
				VALID_ISBN_LENGTH, isbn.length());
		Preconditions.checkArgument(isbn.length() == VALID_ISBN_LENGTH, invalidLengthMessage);
		return isbn;
	}
}
