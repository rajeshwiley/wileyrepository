package com.wiley.core.product.interceptors;

import com.wiley.core.model.WileyDiscountRowModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

public class WileyDiscountRowValidationInterceptor implements ValidateInterceptor<WileyDiscountRowModel>
{

	@Override
	public void onValidate(final WileyDiscountRowModel wileyDiscountRowModel,
						   final InterceptorContext interceptorContext) throws InterceptorException
	{
		if ((wileyDiscountRowModel.getPg() == null) == (wileyDiscountRowModel.getProduct() == null))
		{
			throw new InterceptorException("Either product or product discount class is required");
		}
	}
}
