package com.wiley.core.refund;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;

import java.math.BigDecimal;
import java.util.List;

import com.wiley.fulfilmentprocess.model.WileyExportProcessModel;


public interface WileyRefundService
{
	/**
	 * Performs refund transaction. The amount of money to be refunded is specified in {@param refundAmount} - it should
	 * be positive and not greater then allowed maximum refundable amount by
	 * {@link WileyRefundCalculationService#calculateMaximumAvailableRefundAmountForOrder}.
	 * Multiple refunds are allowed up to maximum refundable amount consumed by all refunds transaction in order.
	 *
	 * @param order
	 * 		order to make refund for
	 * @param refundAmount
	 * 		amount to be refunded
	 * @return transaction entry for successful or not successful refund operation.
	 */
	PaymentTransactionEntryModel processRefundPaymentTransaction(OrderModel order, BigDecimal refundAmount);

	boolean isFullyRefundedOrder(OrderModel order);

	boolean isOrderRefundedByOneTransaction(WileyExportProcessModel process);

	boolean processRefundForModifiedOrder(OrderModel order);

	void processRefund(OrderRefundRequest refundRequest) throws OrderRefundException;

	boolean isEligibleForRefund(OrderModel orderModel);

	List<AbstractOrderEntryModel> getRefundableEntries(List<AbstractOrderEntryModel> entries);

	BigDecimal getMaxRefundAmount(OrderModel orderModel);
}
