package com.wiley.core.order.hook;

import com.wiley.core.model.WileyPartnerCompanyModel;
import de.hybris.platform.commerceservices.order.hook.CommercePlaceOrderMethodHook;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.commerceservices.service.data.CommerceOrderResult;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.europe1.enums.UserDiscountGroup;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.commons.lang.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;

public class WileyPlaceOrderHook implements CommercePlaceOrderMethodHook
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyPlaceOrderHook.class);

	@Resource
	private ModelService modelService;

	@Override
	public void afterPlaceOrder(final CommerceCheckoutParameter parameter, final CommerceOrderResult orderModel)
			throws InvalidCartException
	{
		//do nothing
	}

	@Override
	public void beforePlaceOrder(final CommerceCheckoutParameter parameter) throws InvalidCartException
	{
		//do nothing
	}

	@Override
	public void beforeSubmitOrder(final CommerceCheckoutParameter parameter, final CommerceOrderResult result)
			throws InvalidCartException
	{
		cleanUpPartnerInfo(result.getOrder());
	}

	private void cleanUpPartnerInfo(final OrderModel order)
	{
		WileyPartnerCompanyModel wileyPartner = order.getWileyPartner();
		if (wileyPartner != null)
		{
			UserDiscountGroup orderDiscountGroup = order.getDiscountGroup();
			UserDiscountGroup partnerDiscountGroup = wileyPartner.getUserDiscountGroup();
			if (ObjectUtils.notEqual(orderDiscountGroup, partnerDiscountGroup))
			{
				LOG.info("Order {} was not placed in partner checkout flow -> cleaning up partner in order",
						order.getCode());
				order.setWileyPartner(null);
				modelService.save(order);
			}
		}

	}
}
