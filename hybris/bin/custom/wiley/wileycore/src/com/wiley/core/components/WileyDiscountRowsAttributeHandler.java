package com.wiley.core.components;

import de.hybris.platform.europe1.enums.UserDiscountGroup;
import de.hybris.platform.europe1.jalo.AbstractDiscountRow;
import de.hybris.platform.europe1.model.DiscountRowModel;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.enumeration.EnumerationManager;
import de.hybris.platform.jalo.enumeration.EnumerationValue;
import de.hybris.platform.jalo.order.price.JaloPriceFactoryException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.model.attribute.AbstractDynamicAttributeHandler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.exceptions.WileyDiscountNotFoundException;
import com.wiley.core.jalo.WileyPurchaseOptionProduct;
import com.wiley.core.model.WileyPurchaseOptionProductModel;
import com.wiley.core.model.components.WileyPurchaseOptionProductListComponentModel;
import com.wiley.core.wileyb2c.order.Wileyb2cPriceFactory;


/**
 * @author Dzmitryi_Halahayeu
 */
public class WileyDiscountRowsAttributeHandler
		extends AbstractDynamicAttributeHandler<List<DiscountRowModel>, WileyPurchaseOptionProductListComponentModel>
{
	private Wileyb2cPriceFactory priceFactory;
	private ModelService modelService;

	@Override
	public List<DiscountRowModel> get(final WileyPurchaseOptionProductListComponentModel model)
	{
		if (CollectionUtils.isEmpty(model.getItems()))
		{
			return Collections.emptyList();
		}
		final UserDiscountGroup userDiscountGroup = model.getUg();
		List<DiscountRowModel> discountRows = new ArrayList<>();
		for (final WileyPurchaseOptionProductModel purchaseOptionProductModel : model.getItems())
		{
			final SessionContext sessionContext = JaloSession.getCurrentSession().getSessionContext();
			final EnumerationValue udg = EnumerationManager.getInstance().getEnumerationValue(userDiscountGroup.getType(),
					userDiscountGroup.getCode());
			final WileyPurchaseOptionProduct product = modelService.getSource(purchaseOptionProductModel);

			try
			{
				final List<AbstractDiscountRow> discountJaloRows = priceFactory.matchDiscountRows(product,
						priceFactory.getPDG(sessionContext, product), udg, -1);
				discountRows.addAll(
						discountJaloRows.stream().<DiscountRowModel> map(modelService::get).collect(Collectors.toList()));
			}
			catch (JaloPriceFactoryException e)
			{
				throw new WileyDiscountNotFoundException(
						"Could not resovle discount rows for product " + purchaseOptionProductModel.getCode(), e);
			}
		}
		return discountRows;
	}

	@Required
	public void setPriceFactory(final Wileyb2cPriceFactory priceFactory)
	{
		this.priceFactory = priceFactory;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}
}
