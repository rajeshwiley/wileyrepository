package com.wiley.core.externaltax;

import com.wiley.core.externaltax.dto.TaxCalculationRequestDto;
import com.wiley.core.externaltax.dto.TaxCalculationResponseDto;


public interface WileyTaxGateway
{
	TaxCalculationResponseDto calculateTax(TaxCalculationRequestDto taxCalculationRequestDto);
}
