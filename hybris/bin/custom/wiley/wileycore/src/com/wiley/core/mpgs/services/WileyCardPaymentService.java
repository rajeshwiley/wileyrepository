package com.wiley.core.mpgs.services;

import com.wiley.core.mpgs.request.WileyAuthorizationRequest;



import com.wiley.core.mpgs.request.WileyCaptureRequest;
import com.wiley.core.mpgs.request.WileyRetrieveSessionRequest;
import com.wiley.core.mpgs.request.WileyTokenizationRequest;
import com.wiley.core.mpgs.request.WileyVerifyRequest;
import com.wiley.core.mpgs.response.WileyAuthorizationResponse;
import com.wiley.core.mpgs.response.WileyFollowOnRefundResponse;
import com.wiley.core.mpgs.response.WileyCaptureResponse;
import com.wiley.core.mpgs.response.WileyRetrieveSessionResponse;
import com.wiley.core.mpgs.response.WileyTokenizationResponse;
import com.wiley.core.mpgs.response.WileyVerifyResponse;
import com.wiley.core.payment.request.WileyFollowOnRefundRequest;



public interface WileyCardPaymentService
{
	WileyRetrieveSessionResponse retrieveSession(WileyRetrieveSessionRequest request);

	WileyVerifyResponse verify(WileyVerifyRequest request);

	WileyTokenizationResponse tokenize(WileyTokenizationRequest request);

	WileyAuthorizationResponse authorize(WileyAuthorizationRequest request);

	WileyCaptureResponse capture(WileyCaptureRequest request);

	WileyFollowOnRefundResponse refundFollowOn(WileyFollowOnRefundRequest request);

}
