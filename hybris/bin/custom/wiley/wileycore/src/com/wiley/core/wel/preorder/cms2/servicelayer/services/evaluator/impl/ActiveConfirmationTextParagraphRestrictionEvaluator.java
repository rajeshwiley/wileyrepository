package com.wiley.core.wel.preorder.cms2.servicelayer.services.evaluator.impl;

import de.hybris.platform.cms2.servicelayer.data.RestrictionData;

import java.util.Objects;

import com.wiley.core.model.WelActiveConfirmationCMSRestrictionModel;


/**
 * It is used by the ConfirmationTestParagraph component only,
 * that is used on the WEL order confirmation page for Active products.
 * A different component will be always shown(Restriction doesn't work for other components).
 * The general contract is: if there is a pre-order, the ConfirmationTestParagraph is hidden.
 */
public class ActiveConfirmationTextParagraphRestrictionEvaluator
		extends AbstractWelCMSRestrictionEvaluator<WelActiveConfirmationCMSRestrictionModel>
{
	public static final String CONFIRMATION_TEXT_PARAGRAPH = "ConfirmationTextParagraph";

	@Override
	public boolean evaluate(final WelActiveConfirmationCMSRestrictionModel welActiveConfirmationCMSRestrictionModel,
			final RestrictionData restrictionData)
	{
		String componentUid = getComponentUid(restrictionData);
		if (!Objects.equals(componentUid, CONFIRMATION_TEXT_PARAGRAPH))
		{
			return Boolean.TRUE;
		}

		final Boolean isActiveConfirmationTextShown = !isPreOrder();

		return isActiveConfirmationTextShown;
	}
}
