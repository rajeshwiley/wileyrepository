package com.wiley.core.welags.order.impl;

import de.hybris.platform.core.model.order.CartModel;

import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.order.WileyCheckoutService;
import com.wiley.core.wiley.order.impl.WileyCartValidationStrategy;


/**
 * @author Dzmitryi_Halahayeu
 */
public class WelAgsCommerceCartValidationStrategy extends WileyCartValidationStrategy
{
	private WileyCheckoutService wileyCheckoutService;

	/**
	 * Modifies parent logic:
	 * Suppresses clearing of payment info for PayPal flow - this info is required to authorize payment
	 * @param cartModel
	 */
	@Override
	protected void validatePayment(final CartModel cartModel)
	{
		if (!wileyCheckoutService.isPaypalCheckout())
		{
			super.validatePayment(cartModel);
		}
	}

	@Required
	public void setWileyCheckoutService(final WileyCheckoutService wileyCheckoutService)
	{
		this.wileyCheckoutService = wileyCheckoutService;
	}
}
