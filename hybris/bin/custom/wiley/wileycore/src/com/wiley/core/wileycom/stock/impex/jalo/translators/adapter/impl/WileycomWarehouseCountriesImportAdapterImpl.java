package com.wiley.core.wileycom.stock.impex.jalo.translators.adapter.impl;

import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.impex.jalo.header.UnresolvedValueException;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import com.wiley.core.i18n.WileyCountryService;
import com.wiley.core.wileycom.valuetranslator.adapter.AbstractWileycomImportAdapter;


/**
 * @author Dzmitryi_Halahayeu
 */
public class WileycomWarehouseCountriesImportAdapterImpl extends AbstractWileycomImportAdapter
{
	private static final String DELIMITER = ",";
	@Resource
	private ModelService modelService;
	@Resource
	private WileyCountryService wileyCountryService;

	/**
	 * Import collection of {de.hybris.platform.core.model.c2l.CountryModel} for which
	 * {@link de.hybris.platform.ordersplitting.model.WarehouseModel} is applicable
	 *
	 * @param cellValue
	 * 		String representation of list of ISO-2 country codes.
	 * 		For example: UA, GB, US
	 * @param warehouse
	 * 		warehouse to add applicable countries
	 * @throws UnresolvedValueException
	 * 		when valueExpr is in invalid format
	 */
	@Override
	public void performImport(final String cellValue, final Item warehouse) throws UnresolvedValueException
	{
		Assert.notNull(warehouse);
		Assert.hasText(cellValue);
		String[] countryIso2Codes = StringUtils.isEmpty(cellValue) ? new String[] {} : cellValue.split(DELIMITER);
		WarehouseModel warehouseModel = modelService.get(warehouse);

		Set<CountryModel> countries = new HashSet<>();
		for (String countryIso2Code : countryIso2Codes)
		{
			CountryModel country = wileyCountryService.findCountryByCode(countryIso2Code);
			if (country == null)
			{
				throw new UnresolvedValueException("Can not resolve country for ISO code " + countryIso2Code);
			}
			countries.add(country);
		}
		warehouseModel.setCountries(countries);
		modelService.save(warehouseModel);
	}

}
