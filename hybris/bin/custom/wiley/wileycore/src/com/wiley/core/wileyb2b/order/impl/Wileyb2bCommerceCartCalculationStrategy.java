package com.wiley.core.wileyb2b.order.impl;

import de.hybris.platform.commerceservices.order.impl.DefaultCommerceCartCalculationStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.exceptions.CalculationException;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import com.wiley.core.wileyb2b.order.exceptions.Wileyb2bCartCalculationSystemException;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


/**
 * Wileyb2b specific cart calculation strategy.
 */
public class Wileyb2bCommerceCartCalculationStrategy extends DefaultCommerceCartCalculationStrategy
{

	@Resource
	private Wileyb2bCartCalculationService wileyb2bCartCalculationService;

	@Override
	public boolean calculateCart(@Nonnull final CommerceCartParameter parameter)
	{
		validateParameterNotNull(parameter, "parameter cannot be null");

		final CartModel cartModel = parameter.getCart();

		validateParameterNotNull(cartModel, "Cart model cannot be null");

		if (getCalculationService().requiresCalculation(cartModel))
		{
			try
			{
				parameter.setRecalculate(false);
				beforeCalculate(parameter);
				wileyb2bCartCalculationService.calculate(cartModel);
			}
			catch (final CalculationException e)
			{
				throw new Wileyb2bCartCalculationSystemException(e.getMessage(), e);
			}
			finally
			{
				afterCalculate(parameter);
			}
			return true;
		}
		return false;
	}

	@Override
	public boolean recalculateCart(@Nonnull final CommerceCartParameter parameter)
	{
		validateParameterNotNull(parameter, "parameter cannot be null");

		final CartModel cartModel = parameter.getCart();

		validateParameterNotNull(cartModel, "Cart model cannot be null");

		try
		{
			parameter.setRecalculate(true);
			beforeCalculate(parameter);
			wileyb2bCartCalculationService.recalculate(cartModel);
		}
		catch (final CalculationException e)
		{
			throw new Wileyb2bCartCalculationSystemException(e.getMessage(), e);
		}
		finally
		{
			afterCalculate(parameter);

		}
		return true;
	}
}
