package com.wiley.core.event.facade.payment;

import com.wiley.core.event.facade.WileyTwoParametersFacadeEventWithoutResult;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionRequest;
import de.hybris.platform.acceleratorservices.payment.data.PaymentData;

/**
 * Created by Georgii_Gavrysh on 12/20/2016.
 */
public class BasketProductAvailabilityb2bWPGHttpRequestPopulatorEvent
        extends WileyTwoParametersFacadeEventWithoutResult<CreateSubscriptionRequest, PaymentData> {
    public BasketProductAvailabilityb2bWPGHttpRequestPopulatorEvent(final CreateSubscriptionRequest firstParameter,
                                                                    final PaymentData secondParameter) {
        super(firstParameter, secondParameter);
    }
}
