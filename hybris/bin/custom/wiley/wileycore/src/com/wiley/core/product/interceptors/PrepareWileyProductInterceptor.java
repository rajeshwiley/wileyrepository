package com.wiley.core.product.interceptors;


import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.enums.UserDiscountGroup;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.europe1.model.DiscountRowModel;

import com.wiley.core.product.dao.WileyProductStudentDiscountDao;
import com.wiley.core.util.interceptor.AbstractCatalogAwareInterceptor;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;

public class PrepareWileyProductInterceptor extends AbstractCatalogAwareInterceptor implements PrepareInterceptor<ProductModel>
{
    private static final Logger LOG = LoggerFactory.getLogger(PrepareWileyProductInterceptor.class);

    private static final String STAGE_CATALOG_VERSION = "Stage";

    @Resource
    private ModelService modelService;

    @Resource
    WileyProductStudentDiscountDao wileyProductStudentDiscountDao;


    @Override
    public void onPrepare(final ProductModel productModel, final InterceptorContext interceptorContext)
            throws InterceptorException
    {
        try
        {
            //Adding Student Discount for WEL Catalog new product only
            if (productModel.getItemModelContext().isNew()) {
                final CatalogVersionModel prodCatalogVersion = (CatalogVersionModel) productModel.getCatalogVersion();
                if (prodCatalogVersion != null) {
                    final CatalogModel prodCatalog = (CatalogModel) productModel.getCatalogVersion().getCatalog();
                    if (prodCatalog != null) {
                        if (isCatalogAllowed(prodCatalog) && isVersionAllowed(prodCatalogVersion.getVersion())) {
                            LOG.info("Adding Student discount to Product Model");
                            final DiscountRowModel discountRowModel = modelService.create(DiscountRowModel.class);
                            discountRowModel.setProduct(productModel);
                            discountRowModel.setCatalogVersion(productModel.getCatalogVersion());
                            wileyProductStudentDiscountDao.getStudentDiscount(discountRowModel);
                            discountRowModel.setUg(UserDiscountGroup.STUDENT);
                            Collection<DiscountRowModel> discountRows = productModel.getEurope1Discounts();
                            discountRows.add(discountRowModel);
                            productModel.setEurope1Discounts(discountRows);
                        }
                    }
                }
            }
        }
        catch (UnknownIdentifierException e)
        {
            LOG.error("Error Executing PrepareWileyProductInterceptor");
        }
    }

}