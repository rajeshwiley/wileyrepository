package com.wiley.core.wileycom.stock.strategies;

import de.hybris.platform.ordersplitting.model.StockLevelModel;

import java.util.Collection;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;

import org.apache.log4j.Logger;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateIfAnyResult;


/**
 * The strategy fetches the primary stock level.
 * If more than one stock level marked as primary found first primary stock level will be returned
 * If primary stock levels not found the strategy return first not primary stock level
 */
public class WileycomPrimaryStockLevelStrategy
{
	private static final Logger LOG = Logger.getLogger(WileycomPrimaryStockLevelStrategy.class);

	@Nonnull
	public StockLevelModel getPrimaryStockLevel(@Nonnull final Collection<StockLevelModel> stockLevels)
	{
		validateIfAnyResult(stockLevels, "stockLevels cannot be null or empty");

		if (stockLevels.size() == 1)
		{
			return stockLevels.iterator().next();
		}
		Collection<StockLevelModel> primaryStockLevels = stockLevels.stream().filter(stockLevel ->
				stockLevel.getPrimary() != null ? stockLevel.getPrimary()  : false)
				.collect(Collectors.toList());
		StockLevelModel result;
		if (!primaryStockLevels.isEmpty())
		{
			result = primaryStockLevels.iterator().next();
			if (primaryStockLevels.size() > 1)
			{
				LOG.warn(String.format(
						"More than one primary stock level for product %s was found. Primary stock level %s was selected.",
						result.getProductCode(), result.getInStockStatus()));
			}
		}
		else
		{
			result = stockLevels.iterator().next();
			LOG.warn(String.format(
					"Primary stock level for product %s wasn't found. Non-primary stock level %s was selected.",
					result.getProductCode(), result.getInStockStatus()));
		}
		return result;
	}
}
