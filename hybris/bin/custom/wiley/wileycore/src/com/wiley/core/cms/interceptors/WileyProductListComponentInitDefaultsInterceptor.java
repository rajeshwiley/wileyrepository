package com.wiley.core.cms.interceptors;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.contents.components.SimpleCMSComponentModel;
import de.hybris.platform.servicelayer.interceptor.InitDefaultsInterceptor;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import java.util.Arrays;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import com.wiley.core.cms.service.WileyCMSComponentService;
import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.model.actions.AddToLegacyCartActionModel;

import static com.wiley.core.constants.WileyCoreConstants.WILEY_ADD_TO_LEGACY_CART_ACTION_UID;


/**
 * Created by Uladzimir_Barouski on 5/17/2017.
 */
public class WileyProductListComponentInitDefaultsInterceptor implements InitDefaultsInterceptor<SimpleCMSComponentModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(SimpleCMSComponentModel.class);

	@Value(value = WileyCoreConstants.STAGED_CATALOG_VERSION)
	private String defaultCatalogVersion;

	@Resource
	private WileyCMSComponentService cmsComponentService;

	@Override
	public void onInitDefaults(final SimpleCMSComponentModel simpleCmsComponentModel,
			final InterceptorContext interceptorContext) throws InterceptorException
	{
		try
		{
			AddToLegacyCartActionModel addToLegacyCartAction = cmsComponentService.getAbstractCMSComponentForCatalogVersion(
					WILEY_ADD_TO_LEGACY_CART_ACTION_UID, defaultCatalogVersion);
			simpleCmsComponentModel.setActions(Arrays.asList(addToLegacyCartAction));
		}
		catch (CMSItemNotFoundException e)
		{
			LOG.error(WILEY_ADD_TO_LEGACY_CART_ACTION_UID + " not found");
		}
	}
}
