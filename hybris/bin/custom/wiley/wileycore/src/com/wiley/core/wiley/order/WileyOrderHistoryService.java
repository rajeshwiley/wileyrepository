package com.wiley.core.wiley.order;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderhistory.OrderHistoryService;


public interface WileyOrderHistoryService extends OrderHistoryService
{
	/**
	 * Finds previous order version (snapshot) in history.
	 *
	 * @param order
	 * @return founded previous order version
	 */
	OrderModel getPreviousOrderVersion(OrderModel order);
}
