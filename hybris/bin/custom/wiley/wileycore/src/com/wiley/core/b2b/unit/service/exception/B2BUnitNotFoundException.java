package com.wiley.core.b2b.unit.service.exception;

public class B2BUnitNotFoundException extends RuntimeException
{
	public B2BUnitNotFoundException(final String message, final Throwable cause)
	{
		super(message, cause);
	}

	public B2BUnitNotFoundException(final String message)
	{
		super(message);
	}
}
