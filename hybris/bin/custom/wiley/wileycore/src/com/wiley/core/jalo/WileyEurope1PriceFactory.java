/**
 *
 */
package com.wiley.core.jalo;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.europe1.constants.Europe1Tools;
import de.hybris.platform.europe1.enums.UserDiscountGroup;
import de.hybris.platform.europe1.jalo.AbstractDiscountRow;
import de.hybris.platform.europe1.jalo.PriceRow;
import de.hybris.platform.europe1.model.AbstractDiscountRowModel;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.c2l.Currency;
import de.hybris.platform.jalo.enumeration.EnumerationManager;
import de.hybris.platform.jalo.enumeration.EnumerationValue;
import de.hybris.platform.jalo.order.AbstractOrder;
import de.hybris.platform.jalo.order.AbstractOrderEntry;
import de.hybris.platform.jalo.order.price.Discount;
import de.hybris.platform.jalo.order.price.JaloPriceFactoryException;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.jalo.product.Unit;
import de.hybris.platform.jalo.user.User;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.subscriptionservices.jalo.ExtendedCatalogAwareEurope1PriceFactory;
import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.util.PriceValue;
import de.hybris.platform.util.localization.Localization;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.util.ObjectUtils;

import com.wiley.core.order.WileyOrderDiscountGroupService;
import com.wiley.core.order.WileyPriceFactory;
import com.wiley.core.order.data.ProductDiscountParameter;

import static java.util.Objects.nonNull;


/**
 * Price factory with specific logic for Wiley.
 */
public class WileyEurope1PriceFactory extends ExtendedCatalogAwareEurope1PriceFactory
		implements WileyPriceFactory
{

	private static final Logger LOG = Logger.getLogger(WileyEurope1PriceFactory.class);

	@Resource
	private WileyOrderDiscountGroupService wileyOrderDiscountGroupService;

	@Resource
	private ModelService modelService;

	protected ModelService getModelService()
	{
		return modelService;
	}

	// It is necessary to instantiate DiscountRowMatchComparator which has protected constructor
	protected class WileyDiscountRowMatchComparator extends DiscountRowMatchComparator
	{
	}

	protected class WileyPriceRowMatchComparator extends PriceRowMatchComparator
	{
		protected WileyPriceRowMatchComparator(final Currency curr, final boolean net, final Unit unit)
		{
			super(curr, net, unit);
		}

		@Override
		public int compare(final PriceRow priceRow1, final PriceRow priceRow2)
		{
			final long compareMinQty = priceRow1.getMinQuantity() - priceRow2.getMinQuantity();
			return (int) compareMinQty;
		}
	}
	
	@Override
	protected List<PriceRow> filterPriceRows(final List<PriceRow> priceRows)
	{
		final Currency sessionCurrency = getSession().getSessionContext().getCurrency();
		return filterPriceRowsForCurrency(super.filterPriceRows(priceRows), sessionCurrency);
	}

	protected List<PriceRow> filterPriceRowsForCurrency(final List<PriceRow> priceRows, final Currency currency) {
		return priceRows.stream()
				.filter(priceRow -> priceRowHasSameCurrencyAsSession(currency, priceRow))
				.collect(Collectors.toList());
	}

	private boolean priceRowHasSameCurrencyAsSession(final Currency sessionCurrency, final PriceRow priceRow)
	{
		return sessionCurrency != null
				&& sessionCurrency.getIsocode() != null
				&& priceRow != null && priceRow.getCurrency() != null
				&& sessionCurrency.getIsocode().equals(priceRow.getCurrency().getIsocode());
	}

	private static Predicate<AbstractDiscountRow> discountRowCurrencyFilter(final Currency currency)
	{
		return (discountRow) -> {
			Boolean isAbsolute;
			Currency discountCurrency;
			// DiscountRow overrides Discount if DiscountRow has value.
			// Please, see implementation of Europe1Tools.createDiscountValueList.
			if (nonNull(discountRow.getValue()))
			{
				isAbsolute = discountRow.isAbsolute();
				discountCurrency = discountRow.getCurrency();
			}
			else
			{
				final Discount discount = discountRow.getDiscount();
				isAbsolute = discount.isAbsolute();
				discountCurrency = discount.getCurrency();
			}
			return Boolean.FALSE.equals(isAbsolute) || currency.equals(discountCurrency);
		};
	}

	@Override
	protected EnumerationValue getUDG(final SessionContext ctx, final AbstractOrder order)
			throws JaloPriceFactoryException
	{
		EnumerationValue udg;

		final UserDiscountGroup userDiscountGroup = wileyOrderDiscountGroupService
				.resolveDiscountGroupByAbstractOrder(modelService.get(order));

		if (nonNull(userDiscountGroup))
		{
			udg = EnumerationManager.getInstance().getEnumerationValue(userDiscountGroup.getType(),
					userDiscountGroup.getCode());
		}
		else
		{
			udg = super.getUDG(ctx, order);
		}
		return udg;
	}

	@Override
	public Collection getTaxValues(final AbstractOrderEntry entry) throws JaloPriceFactoryException
	{
		return Collections.emptyList();
	}

	@Override
	@Nonnull
	public List<DiscountValue> getProductDiscountValues(
			@Nonnull final ProductDiscountParameter parameter) throws JaloPriceFactoryException
	{

		ServicesUtil.validateParameterNotNullStandardMessage("parameter", parameter);

		final ProductModel product = parameter.getProduct();
		final UserDiscountGroup userDiscountGroup = parameter.getUserDiscountGroup();
		final UserModel user = parameter.getUser();
		final CurrencyModel currency = parameter.getCurrency();
		final Date date = parameter.getDate();
		final Optional<Predicate<AbstractDiscountRowModel>> additionalFilter = parameter
				.getAdditionalFilter();

		ServicesUtil.validateParameterNotNullStandardMessage("product", product);
		ServicesUtil.validateParameterNotNullStandardMessage("userDiscountGroup", userDiscountGroup);
		ServicesUtil.validateParameterNotNullStandardMessage("user", user);
		ServicesUtil.validateParameterNotNullStandardMessage("currency", currency);
		ServicesUtil.validateParameterNotNullStandardMessage("date", date);
		ServicesUtil.validateParameterNotNullStandardMessage("additionalFilter", additionalFilter);

		//region Converting models to jalo
		final SessionContext sessionContext = JaloSession.getCurrentSession().getSessionContext();
		final Product productJalo = modelService.getSource(product);
		final EnumerationValue udg = EnumerationManager.getInstance()
				.getEnumerationValue(userDiscountGroup.getType(), userDiscountGroup.getCode());
		final User userJalo = modelService.getSource(user);
		final Currency currencyJalo = modelService.getSource(currency);
		//endregion

		List<AbstractDiscountRow> discountRows = this.matchDiscountRows(productJalo,
				this.getPDG(sessionContext, productJalo), userJalo, udg, currencyJalo, date, -1);

		// it provides availability to filter discount rows based on criteria.
		// It is necessary for filtering only student discounts,
		// because OOTB returns also discounts which is not assigned to any specific discount group (discounts which are common
		// for all discount groups)
		if (additionalFilter.isPresent())
		{
			final Predicate<AbstractDiscountRowModel> predicate = additionalFilter.get();
			discountRows = discountRows.stream()
					.filter(discountRow -> predicate.test(modelService.get(discountRow)))
					.collect(Collectors.toList());
		}

		final List<DiscountValue> discountValueList = Europe1Tools
				.createDiscountValueList(discountRows);

		return CollectionUtils.isNotEmpty(discountValueList) ? discountValueList : Collections.emptyList();
	}

	@Override
	@Nonnull
	public List<AbstractDiscountRow> matchDiscountRows(@Nonnull final Product product,
			@Nonnull final EnumerationValue productGroup, @Nonnull final User user,
			@Nullable final EnumerationValue userGroup, @Nonnull final Currency currency,
			@Nonnull final Date date, final int maxCount) throws JaloPriceFactoryException
	{
		if (user == null && userGroup == null) //NOSONAR
		{
			throw new JaloPriceFactoryException(
					"cannot match discounts without user and user group - at least one must be present", 0);
		}
		if (currency == null) //NOSONAR
		{
			throw new JaloPriceFactoryException("cannot match price without currency", 0);
		}
		if (date == null) //NOSONAR
		{
			throw new JaloPriceFactoryException("cannot match price without date", 0);
		}

		final Collection<AbstractDiscountRow> discountRows = (Collection<AbstractDiscountRow>) this
				.queryDiscounts4Price(this.getSession().getSessionContext(), product, productGroup, user,
						userGroup);

		if (CollectionUtils.isNotEmpty(discountRows))
		{
			List<AbstractDiscountRow> filteredDiscountRows = (List<AbstractDiscountRow>) this
					.filterDiscountRows4Price(discountRows, date);

			// filtering discounts by currency
			if (CollectionUtils.isNotEmpty(filteredDiscountRows))
			{
				filteredDiscountRows = filterDiscountsByCurrencyAndSortIt(filteredDiscountRows, currency);
			}

			return filteredDiscountRows;
		}

		return Collections.emptyList();
	}

	@Override
	public PriceValue getBasePrice(final AbstractOrderEntry entry) throws JaloPriceFactoryException
	{
		/* Start of fix for OOTB issue proposed by Hybris/SAP in ECSC-2000 */
		final SessionContext ctx = getSession().getSessionContext();
		final AbstractOrder order = entry.getOrder(ctx);
		Currency currency = null;
		EnumerationValue productGroup = null;
		User user = null;
		EnumerationValue userGroup = null;
		Unit unit = null;
		long quantity = 0;
		boolean net = false;
		// Bussiness logic changed: instead of comparing the cart creation date we are using current date
		final Date date = getBasePriceDate(entry);
		final Product product = entry.getProduct();
		final boolean giveAwayMode = entry.isGiveAway(ctx);
		final boolean entryIsRejected = entry.isRejected(ctx);
		PriceRow row;
		if (giveAwayMode && entryIsRejected)
		{
			row = null;
		}
		else
		{
			productGroup = getPPG(ctx, product);
			user = order.getUser();
			userGroup = getUPG(ctx, user);
			quantity = entry.getQuantity(ctx);
			unit = entry.getUnit(ctx);
			currency = order.getCurrency(ctx);
			net = order.isNet();

			row = matchPriceRowForPrice(ctx, date, entry);
		}

		if (row != null)
		{
			final Currency rowCurr = row.getCurrency();
			if (currency.equals(rowCurr))
			{
				final double price = row.getPriceAsPrimitive() / row.getUnitFactorAsPrimitive();
				final Unit priceUnit = row.getUnit();
				final Unit entryUnit = entry.getUnit();
				final double convertedPrice = priceUnit.convertExact(entryUnit, price);
				return new PriceValue(
						currency.getIsoCode(),
						convertedPrice,
						row.isNetAsPrimitive());
			}
			else
			{
				final String errorMessage = getPriceRowNotFoundErrorMessage(currency,
						productGroup, user, userGroup, unit, quantity, net, date, product);
				LOG.error(errorMessage);
				return null;
			}

		}
		else if (giveAwayMode)
		{
			return getZeroPriceValue(order.getCurrency(ctx), order.isNet());
		}
		else
		{
			final String errorMessage = getPriceRowNotFoundErrorMessage(currency,
					productGroup, user, userGroup, unit, quantity, net, date, product);
			LOG.error(errorMessage);
			return null;
		}
		/* End of fix for OOTB issue proposed by Hybris/SAP in ECSC-2000 */
	}

	protected Date getBasePriceDate(final AbstractOrderEntry entry)
	{
		return new Date();
	}

	protected PriceValue getZeroPriceValue(final Currency currency, final boolean isNet)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("currency", currency);
		return new PriceValue(currency.getIsoCode(), 0.0, isNet);
	}

	protected PriceRow matchPriceRowForPrice(final SessionContext ctx, final Date date, final AbstractOrderEntry orderEntry)
			throws JaloPriceFactoryException
	{
		final AbstractOrder order = orderEntry.getOrder(ctx);
		final User user = order.getUser();
		final Product product = orderEntry.getProduct(ctx);
		final boolean giveAwayMode = orderEntry.isGiveAway(ctx);

		return matchPriceRowForPrice(ctx, product, getPPG(ctx, product), order.getUser(), getUPG(ctx, user),
				orderEntry.getQuantity(ctx), orderEntry.getUnit(ctx), order.getCurrency(ctx),
				date, // Bussiness logic changed: deleting the date attribution to the cart creation date
				order.isNet(), giveAwayMode);

	}

		private String getPriceRowNotFoundErrorMessage(final Currency currency, final EnumerationValue productGroup,
				final User user, final EnumerationValue userGroup, final Unit unit, final long quantity, final boolean net,
				final Date date, final Product product) throws JaloPriceFactoryException
	{
		return Localization
				.getLocalizedString("exception.europe1pricefactory.getbaseprice.jalopricefactoryexception1",
						new Object[]
								{ product, productGroup, user, userGroup, Long.toString(quantity), unit,
										currency, date, Boolean.toString(net) });
	}

	private List<AbstractDiscountRow> filterDiscountsByCurrencyAndSortIt(final List<AbstractDiscountRow> filteredDiscountRows,
			final Currency currency)
	{
		return filteredDiscountRows.stream()
				.filter(discountRowCurrencyFilter(currency))
				.sorted(new WileyDiscountRowMatchComparator())
				.collect(Collectors.toList());
	}
	
	public List<PriceRow> getPriceRowsForOrderEntry(final AbstractOrderEntryModel entry) throws JaloPriceFactoryException
	{
		final AbstractOrderEntry entryItem = modelService.getSource(entry);
		final AbstractOrder order = entryItem.getOrder();
		final SessionContext ctx = getSession().getSessionContext();
		final Product product = entryItem.getProduct(ctx);
		final EnumerationValue productGroup = getPPG(ctx, product);
		final User user = order.getUser();
		EnumerationValue userGroup = getUPG(ctx, user);
		final Boolean net = order.isNet();
		final Currency currency = order.getCurrency(ctx);
		final Date date = order.getDate(ctx);
		Collection<PriceRow> priceRowCollection = super.matchPriceRowsForInfo(ctx, product, productGroup, user, 
		userGroup, currency, date, net);

		List<PriceRow> priceRowList = null;
		if (!ObjectUtils.isEmpty(priceRowCollection))
		{
			priceRowList = priceRowCollection.stream().filter(row -> {
				final Currency rowCurr = row.getCurrency();
				return currency.equals(rowCurr);
			}).collect(Collectors.toList());

			final Unit unit = entryItem.getUnit(ctx);
			Collections.sort(priceRowList, new WileyPriceRowMatchComparator(currency, net, unit));
		}
		return priceRowList;
	}
}
