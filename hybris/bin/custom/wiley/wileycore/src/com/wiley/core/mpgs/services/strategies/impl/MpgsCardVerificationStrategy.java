package com.wiley.core.mpgs.services.strategies.impl;

import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.Objects;

import javax.annotation.Nonnull;

import com.wiley.core.mpgs.constants.WileyMPGSConstants;
import com.wiley.core.mpgs.request.WileyVerifyRequest;
import com.wiley.core.mpgs.response.WileyVerifyResponse;
import com.wiley.core.mpgs.services.WileyCardPaymentService;
import com.wiley.core.mpgs.services.WileyMPGSMerchantService;
import com.wiley.core.mpgs.services.WileyUrlGenerationService;
import com.wiley.core.mpgs.services.strategies.CardVerificationStrategy;
import com.wiley.core.mpgs.strategies.WileyMPGSTransactionIdGeneratorStrategy;


public class MpgsCardVerificationStrategy extends AbstractCardVerificationStrategy implements CardVerificationStrategy
{
	private WileyCardPaymentService wileyCardPaymentService;

	private WileyUrlGenerationService wileyUrlGenerationService;

	private WileyMPGSTransactionIdGeneratorStrategy wileyMPGSTransactionIdGenerateStrategy;

	private WileyMPGSMerchantService wileyMPGSMerchantService;

	@Override
	public PaymentTransactionEntryModel verify(final AbstractOrderModel cart, final String sessionId)
	{
		final PaymentTransactionModel lastPaymentTransaction = getLastPaymentTransaction(cart);
		WileyVerifyRequest verifyRequest = createVerifyRequest(cart, sessionId, lastPaymentTransaction);

		WileyVerifyResponse verifyResponse = wileyCardPaymentService.verify(verifyRequest);

		return wileyMPGSPaymentEntryService.createVerifyEntry(verifyResponse,
				lastPaymentTransaction);
	}


	private WileyVerifyRequest createVerifyRequest(@Nonnull final AbstractOrderModel cart, @Nonnull final String sessionId,
			final PaymentTransactionModel paymentTransaction)
	{
		WileyVerifyRequest verifyRequest = new WileyVerifyRequest();

		verifyRequest.setApiOperation(WileyMPGSConstants.API_OPERATAION_VERIFY);
		verifyRequest.setCurrency(cart.getCurrency().getIsocode());
		verifyRequest.setSessionId(sessionId);
		verifyRequest.setSourceOfFundsType(WileyMPGSConstants.SOURCE_OF_FUNDS_TYPE);
		AddressModel billingAddress = cart.getUser().getDefaultPaymentAddress();
		if (billingAddress != null)
		{
			verifyRequest.setBillingStreet1(billingAddress.getLine1());
			verifyRequest.setBillingStreet2(billingAddress.getLine2());
			verifyRequest.setBillingCity(billingAddress.getTown());
			verifyRequest.setBillingCountry(billingAddress.getCountry().getIsocode3());
			verifyRequest.setBillingPostalCode(billingAddress.getPostalcode());
			final RegionModel region = billingAddress.getRegion();
			verifyRequest.setStateProvince(region != null ? region.getIsocode() : null);

		}
		verifyRequest.setPaymentProvider(WileyMPGSConstants.MPGS_PAYMENT_PROVIDER);
		final String transactionId = wileyMPGSTransactionIdGenerateStrategy.generateTransactionId();
		final String orderId = getMPGSOrderId(paymentTransaction);
		final String tnsMerchantId = wileyMPGSMerchantService.getMerchantID(cart);
		verifyRequest.setUrlPath(wileyUrlGenerationService.getVerifyUrl(orderId, transactionId, tnsMerchantId));
		return verifyRequest;
	}

	private String getMPGSOrderId(@Nonnull final PaymentTransactionModel transaction)
	{
		ServicesUtil.validateParameterNotNull(transaction, "[transaction] can't be null.");
		final AbstractOrderModel order = transaction.getOrder();
		ServicesUtil.validateParameterNotNull(order, "[transaction.order] can't be null.");

		//based on PaymentProvider version
		if (Objects.equals(transaction.getPaymentProvider(), WileyMPGSConstants.MPGS_PAYMENT_PROVIDER))
		{
			return order.getGuid();
		}
		else
		{
			return transaction.getCode();
		}
	}

	public void setWileyCardPaymentService(final WileyCardPaymentService wileyCardPaymentService)
	{
		this.wileyCardPaymentService = wileyCardPaymentService;
	}

	public void setWileyUrlGenerationService(final WileyUrlGenerationService wileyUrlGenerationService)
	{
		this.wileyUrlGenerationService = wileyUrlGenerationService;
	}

	public void setWileyMPGSTransactionIdGenerateStrategy(
			final WileyMPGSTransactionIdGeneratorStrategy wileyMPGSTransactionIdGenerateStrategy)
	{
		this.wileyMPGSTransactionIdGenerateStrategy = wileyMPGSTransactionIdGenerateStrategy;
	}

	public void setWileyMPGSMerchantService(final WileyMPGSMerchantService wileyMPGSMerchantService)
	{
		this.wileyMPGSMerchantService = wileyMPGSMerchantService;
	}
}
