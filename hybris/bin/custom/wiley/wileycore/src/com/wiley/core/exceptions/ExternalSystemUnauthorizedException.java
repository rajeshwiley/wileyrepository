package com.wiley.core.exceptions;

/**
 * Created by Sergiy_Mishkovets on 7/14/2016.
 */
public class ExternalSystemUnauthorizedException extends ExternalSystemClientErrorException
{
	public ExternalSystemUnauthorizedException(final String message)
	{
		super(message);
	}

	public ExternalSystemUnauthorizedException(final String message, final Throwable cause)
	{
		super(message, cause);
	}

	public ExternalSystemUnauthorizedException(final Throwable cause)
	{
		super(cause);
	}
}
