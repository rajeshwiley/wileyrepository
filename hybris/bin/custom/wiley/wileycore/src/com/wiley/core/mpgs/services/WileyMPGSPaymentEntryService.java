package com.wiley.core.mpgs.services;


import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;

import javax.annotation.Nonnull;

import com.wiley.core.mpgs.response.WileyAuthorizationResponse;
import com.wiley.core.mpgs.response.WileyCaptureResponse;
import com.wiley.core.mpgs.response.WileyFollowOnRefundResponse;
import com.wiley.core.mpgs.response.WileyRetrieveSessionResponse;
import com.wiley.core.mpgs.response.WileyTokenizationResponse;
import com.wiley.core.mpgs.response.WileyVerifyResponse;


public interface WileyMPGSPaymentEntryService
{
	PaymentTransactionEntryModel createRetrieveSessionEntry(WileyRetrieveSessionResponse retrieveSessionResponse,
			PaymentTransactionModel paymentTransaction);

	PaymentTransactionEntryModel createVerifyEntry(@Nonnull WileyVerifyResponse verifyResponse,
			@Nonnull PaymentTransactionModel paymentTransaction);

	PaymentTransactionEntryModel createTokenizationEntry(WileyTokenizationResponse tokenizationResponse,
			PaymentTransactionModel transaction, Boolean saveInAccount);

	PaymentTransactionEntryModel createTokenizationEntry(WileyTokenizationResponse tokenizationResponse,
			PaymentTransactionModel transaction, Boolean saveInAccount, OrderModel orderModel);

	PaymentTransactionEntryModel createAuthorizationPaymentTransactionEntry(
			WileyAuthorizationResponse authorizationResponse, PaymentTransactionModel authorizePaymentTransaction);

	PaymentTransactionEntryModel createCaptureEntry(WileyCaptureResponse captureResponse, PaymentTransactionModel transaction);

	PaymentTransactionEntryModel createFollowOnRefundEntry(WileyFollowOnRefundResponse refundResponse,
			PaymentTransactionModel paymentTransaction);

	PaymentTransactionModel createPaymentTransaction(AbstractOrderModel cart);
}
