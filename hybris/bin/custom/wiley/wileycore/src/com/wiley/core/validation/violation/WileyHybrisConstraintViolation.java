package com.wiley.core.validation.violation;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.type.AttributeDescriptorModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.core.model.type.TypeModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.validation.annotations.Dynamic;
import de.hybris.platform.validation.enums.Severity;
import de.hybris.platform.validation.messages.ResourceBundleProvider;
import de.hybris.platform.validation.model.constraints.AbstractConstraintModel;
import de.hybris.platform.validation.model.constraints.AttributeConstraintModel;
import de.hybris.platform.validation.model.constraints.DynamicConstraintModel;
import de.hybris.platform.validation.payloads.Info;
import de.hybris.platform.validation.payloads.Warn;
import de.hybris.platform.validation.services.impl.DefaultHybrisConstraintViolation;

import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Path;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrSubstitutor;
import org.springframework.beans.factory.annotation.Required;

import com.google.common.collect.Maps;


/**
 * This class adds ability to distinguish what exactly dynamic constraint was violated.
 * OOTB cannot do that properly (return first Dynamic constraint for type)
 */
public class WileyHybrisConstraintViolation extends DefaultHybrisConstraintViolation
{
	//OOTB start
	//CHECKSTYLE:OFF

	private static final String PREFIX = "{";
	private static final String SUFFIX = "}";
	private ConstraintViolation constraintViolation;
	private AbstractConstraintModel constraintModel;
	private String localizeMessage;
	protected ModelService modelService;
	protected TypeService typeService;
	protected ResourceBundleProvider bundleProvider;
	protected I18NService i18nService;

	public ConstraintViolation getConstraintViolation() {
		return this.constraintViolation;
	}

	public String getProperty() {
		return this.constraintViolation.getPropertyPath().toString();
	}

	public String getModelName() {
		return this.constraintViolation.getRootBeanClass().getSimpleName();
	}

	public String getTypeName() {
		String name = this.getModelName();
		return name.length() > 5 && name.endsWith("Model")?name.substring(0, name.length() - 5):this.getModelName();
	}

	public String getQualifier() {
		String property = this.getProperty();
		return property.isEmpty()?this.getTypeName():this.getTypeName() + "." + property;
	}

	public String getMessageTemplate() {
		return this.constraintViolation.getMessageTemplate();
	}

	public AbstractConstraintModel getConstraintModel() {
		if(this.constraintModel == null) {
			this.constraintModel = this.buildConstraintModel(this.constraintViolation);
		}

		return this.constraintModel;
	}

	public String getLocalizedMessage() {
		if(this.localizeMessage == null) {
			this.localizeMessage = this.buildLocalizedMessage(this.constraintViolation);
		}

		return this.localizeMessage;
	}

	public Severity getViolationSeverity() {
		Set payloadSeverities = this.constraintViolation.getConstraintDescriptor().getPayload();
		if(payloadSeverities != null && !payloadSeverities.isEmpty()) {
			Class severity = (Class)payloadSeverities.iterator().next();
			return severity.equals(Info.class)?Severity.INFO:(severity.equals(Warn.class)?Severity.WARN:Severity.ERROR);
		} else {
			return Severity.ERROR;
		}
	}

	private String getLocalizedString(String key, Locale locale, Map<String, Object> placeholders) {
		ResourceBundle bundle = this.bundleProvider.getResourceBundle(locale);
		String result = bundle.getString(key);
		if(placeholders != null) {
			result = this.replacePlaceholders(result, placeholders);
		}

		return result;
	}

	String replacePlaceholders(String value, Map<String, Object> placeholders) {
		StrSubstitutor sub = new StrSubstitutor(placeholders, "{", "}");
		return sub.replace(value);
	}

	private String buildLocalizedMessage(ConstraintViolation constraintViolation) {
		AbstractConstraintModel constraintmodel = this.buildConstraintModel(constraintViolation);
		Map constraintDescriptors = constraintViolation.getConstraintDescriptor().getAttributes();
		HashMap attributes = Maps.newHashMapWithExpectedSize(constraintDescriptors.size() + 3);
		attributes.putAll(constraintDescriptors);
		if(constraintmodel instanceof AttributeConstraintModel && ((AttributeConstraintModel)constraintmodel).getDescriptor() != null && !StringUtils.isEmpty(((AttributeConstraintModel)constraintmodel).getDescriptor().getName())) {
			attributes.put("field", ((AttributeConstraintModel)constraintmodel).getDescriptor().getName());
		} else {
			attributes.put("field", constraintViolation.getPropertyPath());
		}

		attributes.put("type", constraintViolation.getRootBeanClass().getSimpleName());
		String result = constraintmodel != null?constraintmodel.getMessage():null;
		if(result != null && !StringUtils.isBlank(result)) {
			result = this.replacePlaceholders(result, attributes);
		} else {
			result = constraintViolation.getMessageTemplate();
			if(result.charAt(0) == 123 && result.charAt(result.length() - 1) == 125) {
				result = result.substring(1, result.length() - 1);
				result = this.getLocalizedString(result, this.i18nService.getCurrentLocale(), attributes);
			}
		}

		return result;
	}

	private AbstractConstraintModel buildConstraintModel(final ConstraintViolation constraintViolation)
	{
		AbstractConstraintModel result = null;
		Object model = constraintViolation.getLeafBean();
		if (model instanceof ItemModel)
		{
			String type = modelService.getModelType(model);
			TypeModel typeModel = typeService.getType(type);
			if (typeModel instanceof ComposedTypeModel)
			{
				ComposedTypeModel compTypeModel = (ComposedTypeModel) typeModel;
				Iterator nodeIter = constraintViolation.getPropertyPath().iterator();

				Path.Node node;
				for (node = (Path.Node) nodeIter.next(); nodeIter.hasNext(); node = (Path.Node) nodeIter.next())
				{
					;
				}

				String qualifier = node.getName();
				Annotation anno = constraintViolation.getConstraintDescriptor().getAnnotation();
				Class annotationClass = anno.annotationType();
				HashSet constraints = new HashSet();
				if (qualifier != null)
				{
					if (qualifier.charAt(0) == 95)
					{
						qualifier = qualifier.substring(1);
					}

					while (compTypeModel != null)
					{
						try
						{
							AttributeDescriptorModel attribConstraintModel = typeService.getAttributeDescriptor(
									compTypeModel, qualifier);
							constraints.addAll(attribConstraintModel.getConstraints());
							compTypeModel = compTypeModel.getSuperType();
						}
						catch (UnknownIdentifierException ex)
						{
							compTypeModel = null;
						}
					}
				}
				else
				{
					while (compTypeModel != null)
					{
						constraints.addAll(compTypeModel.getConstraints());
						compTypeModel = compTypeModel.getSuperType();
					}
				}

				Iterator allConstraints = constraints.iterator();

				while (allConstraints.hasNext())
				{
					AbstractConstraintModel constraintModel = (AbstractConstraintModel) allConstraints.next();
					if (annotationClass.equals(Dynamic.class))
					{
						if (isViolatedDynamicConstraint(constraintModel, constraintViolation))
						{
							result = constraintModel;
							break;
						}
					}
					else if (constraintModel.getAnnotation().equals(annotationClass))
					{
						result = constraintModel;
						break;
					}
				}
			}
		}

		return result;
	}



	public void setConstraintViolation(ConstraintViolation constraintViolation) {
		this.constraintViolation = constraintViolation;
	}

	@Required
	public void setModelService(ModelService modelService) {
		this.modelService = modelService;
	}

	@Required
	public void setTypeService(TypeService typeService) {
		this.typeService = typeService;
	}

	@Required
	public void setBundleProvider(ResourceBundleProvider bundleProvider) {
		this.bundleProvider = bundleProvider;
	}

	@Required
	public void setI18nService(I18NService i18nService) {
		this.i18nService = i18nService;
	}

	public String toString() {
		return "[" + this.getViolationSeverity() + "] " + this.getQualifier() + ": " + this.getLocalizedMessage();
	}
	//OOTB end
	//CHECKSTYLE:ON

	//Added functionality: distinguish dynamic constraints
	// by expression (was only by annotation type which is Dynamic all the time)
	private boolean isViolatedDynamicConstraint(final AbstractConstraintModel constraintModel,
			final ConstraintViolation constraintViolation)
	{
		boolean violated = false;
		if (constraintModel instanceof DynamicConstraintModel) {
			final String constraintExpression = ((DynamicConstraintModel) constraintModel).getExpression();
			final String violationExpression =
					(String) constraintViolation.getConstraintDescriptor().getAttributes().get("expression");
			violated = StringUtils.equals(constraintExpression, violationExpression);
		}
		return violated;
	}
}
