package com.wiley.core.promotions.strategies.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;

import java.util.Date;

import com.wiley.core.promotions.strategies.WileyRuleEngineContextDateStrategy;


public class WileyCurrentTimeRuleEngineContextDateStrategy implements WileyRuleEngineContextDateStrategy
{
	@Override
	public Date getRuleEngineContextDate(final AbstractOrderModel order)
	{
		return new Date();
	}
}
