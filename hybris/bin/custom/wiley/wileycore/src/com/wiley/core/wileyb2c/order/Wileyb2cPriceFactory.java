package com.wiley.core.wileyb2c.order;

import de.hybris.platform.europe1.jalo.AbstractDiscountRow;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.enumeration.EnumerationValue;
import de.hybris.platform.jalo.order.price.JaloPriceFactoryException;
import de.hybris.platform.jalo.product.Product;

import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.wiley.core.order.WileyPriceFactory;


/**
 * Contains methods which are specific for Wiley B2C.
 */
public interface Wileyb2cPriceFactory extends WileyPriceFactory
{

	/**
	 * Finds product discount ignoring currency.
	 *
	 * @param product
	 * @return list of discounts.
	 * @throws JaloPriceFactoryException
	 * 		if some exception occurred while searching product discounts.
	 */
	List<AbstractDiscountRow> matchDiscountRows(@Nonnull Product product,
			@Nonnull EnumerationValue productGroup, @Nullable EnumerationValue userGroup, int maxCount)
			throws JaloPriceFactoryException;

	EnumerationValue getPDG(SessionContext ctx, Product product);

}
