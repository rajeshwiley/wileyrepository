package com.wiley.core.wileyb2c.subscription.services.impl;

import de.hybris.platform.subscriptionservices.model.SubscriptionTermModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wiley.core.wileyb2c.subscription.services.Wileyb2cFreeTrialSubscriptionTermCheckingStrategy;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


public class Wileyb2cFreeTrialSubscriptionTermCheckingStrategyImpl
		implements Wileyb2cFreeTrialSubscriptionTermCheckingStrategy
{
	private static final Logger LOG = LoggerFactory.getLogger(Wileyb2cFreeTrialSubscriptionTermCheckingStrategyImpl.class);
	private static final String FREE_TRIAL_BILLING_FREQUENCY_CODE = "FREE";

	@Override
	public boolean isFreeTrial(final SubscriptionTermModel subscriptionTerm)
	{
		validateParameterNotNull(subscriptionTerm, "subscriptionTerm must not be null");
		if (subscriptionTerm.getBillingPlan() == null || subscriptionTerm.getBillingPlan().getBillingFrequency() == null)
		{
			LOG.warn("Not possible to get billing frequency for subscription term [{}]. Return false",
					subscriptionTerm.getName());
			return false;
		}
		return FREE_TRIAL_BILLING_FREQUENCY_CODE.equals(subscriptionTerm.getBillingPlan().getBillingFrequency().getCode());
	}
}
