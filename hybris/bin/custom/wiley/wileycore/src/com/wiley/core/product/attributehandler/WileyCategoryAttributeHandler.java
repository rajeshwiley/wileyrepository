package com.wiley.core.product.attributehandler;

import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.attribute.AbstractDynamicAttributeHandler;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;


public class WileyCategoryAttributeHandler
		extends AbstractDynamicAttributeHandler<Collection<CategoryModel>, ProductModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyCategoryAttributeHandler.class);

	private String categoryCode;
	private CategoryService categoryService;
	private List<String> supportedProductCatalogsList;

	@Override
	public Collection<CategoryModel> get(final ProductModel productModel)
	{
		if (productModel.getCatalogVersion() == null
				|| !supportedProductCatalogsList.contains(productModel.getCatalogVersion().getCatalog().getId()))
		{
			return Collections.emptyList();
		}
		final CategoryModel category = getCategoryService().getCategoryForCode(productModel.getCatalogVersion(),
				categoryCode);
		Collection<CategoryModel> resultCategories = new ArrayList<>();
		for (final CategoryModel cat : productModel.getSupercategories())
		{
			if (getCategoryService().getAllSupercategoriesForCategory(cat).contains(category))
			{
				resultCategories.add(cat);
			}
		}
		return resultCategories;
	}

	@Override
	public void set(final ProductModel productModel, final Collection<CategoryModel> inputCategories)
	{

		if (productModel.getCatalogVersion() == null
				||		!supportedProductCatalogsList.contains(productModel.getCatalogVersion().getCatalog().getId()))
		{
			LOG.debug("Can't set category attribute for catalog : [{}]", getCatalogId(productModel));
		}
		else
		{
			final CategoryModel category = getCategoryService().getCategoryForCode(productModel.getCatalogVersion(),
					categoryCode);
			checkCategoriesBelongToProperType(inputCategories, category);

			Collection<CategoryModel> finalCategories = new ArrayList<>();
			finalCategories.addAll(getNotProcessableCategories(productModel, category));
			finalCategories.addAll(inputCategories);
			productModel.setSupercategories(finalCategories);
		}
	}

	/**
	 * Returns categories that not needed to be processed.
	 */
	private Collection<CategoryModel> getNotProcessableCategories(final ProductModel productModel,
			final CategoryModel category)
	{
		Collection<CategoryModel> finalCategories = new ArrayList<>();
		final Collection<CategoryModel> superCategories = productModel.getSupercategories();

		if (CollectionUtils.isEmpty(superCategories))
		{
			return finalCategories;
		}

		for (final CategoryModel cat : superCategories)
		{
			if (!getCategoryService().getAllSupercategoriesForCategory(cat).contains(category))
			{
				finalCategories.add(cat);
			}
		}
		return finalCategories;
	}

	private void checkCategoriesBelongToProperType(final Collection<CategoryModel> inputCategories, final CategoryModel category)
	{
		for (final CategoryModel cat : inputCategories)
		{
			if (!getCategoryService().getAllSupercategoriesForCategory(cat).contains(category))
			{
				final String errorMessage = "Category " + cat.getCode() + " is not a subcategory of " + category.getCode();
				throw new IllegalArgumentException(errorMessage);
			}
		}

	}

	private String getCatalogId(final ProductModel productModel)
	{
		return productModel.getCatalogVersion() == null	? null : productModel.getCatalogVersion().getCatalog().getId();
	}

	public CategoryService getCategoryService()
	{
		return categoryService;
	}

	public void setCategoryService(final CategoryService categoryService)
	{
		this.categoryService = categoryService;
	}

	public void setCategoryCode(final String categoryCode)
	{
		this.categoryCode = categoryCode;
	}

	@Required
	public void setSupportedProductCatalogsList(final List<String> supportedProductCatalogsList)
	{
		this.supportedProductCatalogsList = supportedProductCatalogsList;
	}
}
