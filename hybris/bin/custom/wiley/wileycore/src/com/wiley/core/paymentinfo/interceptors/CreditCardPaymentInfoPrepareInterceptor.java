package com.wiley.core.paymentinfo.interceptors;

import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;




/**
 * Interceptor mask credit card number to improve security
 *
 * Created by Raman_Hancharou on 1/14/2016.
 *
 */
public class CreditCardPaymentInfoPrepareInterceptor implements PrepareInterceptor<CreditCardPaymentInfoModel>
{
	private static final int DIGITS_TO_STORE = 4;

	@Override
	public void onPrepare(final CreditCardPaymentInfoModel creditCardPaymentInfoModel,
			final InterceptorContext interceptorContext)
			throws InterceptorException
	{
		final String cardNumber = creditCardPaymentInfoModel.getNumber();
		creditCardPaymentInfoModel.setNumber(cardNumber.substring(cardNumber.length() - DIGITS_TO_STORE));
	}
}
