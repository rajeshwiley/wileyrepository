package com.wiley.core.externaltax.xml.caching.impl;

import de.hybris.platform.regioncache.key.CacheKey;
import de.hybris.platform.regioncache.key.CacheUnitValueType;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.wiley.core.externaltax.xml.parsing.dto.TaxServiceRequest;


public class TaxServiceCacheKey implements CacheKey
{
	private final TaxServiceRequest taxServiceRequest;
	private final String tenantId;

	public TaxServiceCacheKey(final TaxServiceRequest taxServiceRequest, final String tenantId)
	{
		this.taxServiceRequest = taxServiceRequest;
		this.tenantId = tenantId;
	}

	@Override
	public CacheUnitValueType getCacheValueType()
	{
		return CacheUnitValueType.SERIALIZABLE;
	}

	@Override
	public Object getTypeCode()
	{
		return "__Response_INFO__";
	}

	@Override
	public String getTenantId()
	{
		return tenantId;
	}


	@Override
	public int hashCode()
	{
		return Objects.hashCode(taxServiceRequest, tenantId);
	}

	@Override
	public boolean equals(final Object obj)
	{
		if (this == obj)
		{
			return true;
		}
		if (obj == null || getClass() != obj.getClass())
		{
			return false;
		}
		final TaxServiceCacheKey other = (TaxServiceCacheKey) obj;
		return Objects.equal(this.taxServiceRequest, other.taxServiceRequest)
				&& Objects.equal(this.tenantId, other.tenantId);
	}

	@Override
	public String toString()
	{
		return MoreObjects.toStringHelper(this)
				.add("taxServiceRequest", taxServiceRequest)
				.add("tenantId", tenantId)
				.toString();
	}
}
