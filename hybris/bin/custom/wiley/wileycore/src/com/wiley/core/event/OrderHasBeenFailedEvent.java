package com.wiley.core.event;

import de.hybris.platform.orderprocessing.events.OrderProcessingEvent;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;


public class OrderHasBeenFailedEvent extends OrderProcessingEvent
{
	private static final long serialVersionUID = 1L;

	public OrderHasBeenFailedEvent(final OrderProcessModel process)
	{
		super(process);
	}
}
