package com.wiley.core.pages.interceptor;

import com.wiley.core.model.WileyDiscountRowModel;
import com.wiley.core.model.WileyPartnerCompanyModel;
import com.wiley.core.model.WileyPartnerConfigurationModel;
import com.wiley.core.model.WileyPartnerPageModel;
import com.wiley.core.sync.WileySynchronizationService;
import de.hybris.platform.catalog.jalo.CatalogManager;
import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.synchronization.CatalogVersionSyncJobModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.europe1.enums.UserDiscountGroup;
import de.hybris.platform.europe1.model.DiscountRowModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.wiley.core.constants.WileyCoreConstants.DEFAULT_CATEGORY_CODE;
import static de.hybris.platform.cronjob.enums.CronJobResult.SUCCESS;


public class WileyPartnerPagePrepareInterceptor implements PrepareInterceptor<WileyPartnerPageModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyPartnerPagePrepareInterceptor.class);

	@Resource
	private ModelService modelService;
	@Resource
	private WileySynchronizationService wileySynchronizationService;
	@Resource
	private TransactionTemplate transactionTemplate;
	@Resource
	private EnumerationService enumerationService;

	@Override
	public void onPrepare(final WileyPartnerPageModel wileyPartnerPageModel, final InterceptorContext interceptorContext)
			throws InterceptorException
	{
		WileyPartnerConfigurationModel partnerConfiguration = wileyPartnerPageModel.getPartnerConfiguration();
		if (wileyPartnerPageModel.getItemModelContext().isNew())
		{
			if (partnerConfiguration != null)
			{
				final WileyPartnerCompanyModel partnerCompany = getPartnerCompany(partnerConfiguration);

				CategoryModel partnerCategory = partnerConfiguration.getCategory();
				wileyPartnerPageModel.setPartnerCategoryCode(partnerCategory.getCode());

				if (partnerCompany.getUserDiscountGroup() == null)
				{
					UserDiscountGroup partnerDiscountGroup = UserDiscountGroup.valueOf(partnerCompany.getUid());
					modelService.save(partnerDiscountGroup);
					modelService.refresh(partnerDiscountGroup);
					enumerationService.setEnumerationName(partnerDiscountGroup, partnerCompany.getLocName());
					partnerCompany.setUserDiscountGroup(partnerDiscountGroup);
				}

				final List<DiscountRowModel> discountRows = partnerConfiguration.getWileyDiscountRows()
						.stream()
						.map(this::transformDiscountRow)
						.map(discountRow -> updateUserGroupForDiscountRow(discountRow, partnerCompany.getUserDiscountGroup()))
						.collect(Collectors.toList());

				final List<ItemModel> itemsToSave = new ArrayList<>();
				itemsToSave.add(partnerCompany);
				itemsToSave.addAll(discountRows);

				final List<ItemModel> itemsToSync = new ArrayList<>();
				itemsToSync.add(partnerCategory);
				itemsToSync.addAll(partnerCategory.getProducts());
				itemsToSync.addAll(findCategoriesWithoutSynchronizationTarget(partnerCategory.getProducts()));
				itemsToSync.addAll(discountRows);

				modelService.saveAll(itemsToSave);

				synchronizeItems(itemsToSync, partnerCategory.getCatalogVersion().getCatalog());

				transactionTemplate.execute(new TransactionCallbackWithoutResult()
				{
					@Override
					protected void doInTransactionWithoutResult(final TransactionStatus transactionStatus)
					{
						modelService.removeAll(partnerConfiguration.getWileyDiscountRows());
						modelService.remove(partnerConfiguration);
					}
				});

			} else if (StringUtils.isBlank(wileyPartnerPageModel.getPartnerCategoryCode()))
			{
				wileyPartnerPageModel.setPartnerCategoryCode(DEFAULT_CATEGORY_CODE);
			}
		}
	}

	private DiscountRowModel updateUserGroupForDiscountRow(final DiscountRowModel discountRow,
														   final UserDiscountGroup userDiscountGroup)
	{
		discountRow.setUg(userDiscountGroup);
		return discountRow;
	}

	private DiscountRowModel transformDiscountRow(final WileyDiscountRowModel wileyDiscountRow)
	{
		final DiscountRowModel discountRow = modelService.create(DiscountRowModel.class);
		discountRow.setProduct(wileyDiscountRow.getProduct());
		discountRow.setValue(wileyDiscountRow.getValue());
		discountRow.setCatalogVersion(wileyDiscountRow.getCatalogVersion());
		discountRow.setCurrency(wileyDiscountRow.getCurrency());
		discountRow.setDiscount(wileyDiscountRow.getDiscount());
		discountRow.setStartTime(wileyDiscountRow.getStartTime());
		discountRow.setEndTime(wileyDiscountRow.getEndTime());
		discountRow.setPg(wileyDiscountRow.getPg());
		return discountRow;
	}

	private WileyPartnerCompanyModel getPartnerCompany(final WileyPartnerConfigurationModel partnerConfiguration)
	{
		Set<WileyPartnerCompanyModel> wileyPartners = partnerConfiguration.getCategory().getWileyPartners();
		if (wileyPartners.iterator().hasNext())
		{
			return wileyPartners.iterator().next();
		} else
		{
			throw new IllegalStateException(
					"There is no partner defined for category: " + partnerConfiguration.getCategory().getCode());
		}

	}

	private List<CategoryModel> findCategoriesWithoutSynchronizationTarget(final List<ProductModel> products)
	{
		List<CategoryModel> categoriesWithoutTarget = new ArrayList<>();
		for (ProductModel product : products)
		{
			Collection<CategoryModel> supercategories = product.getSupercategories();
			for (CategoryModel category : supercategories)
			{
				if (CollectionUtils.isEmpty(wileySynchronizationService.getSynchronizationTargets(category)))
				{
					LOG.warn("Found missing synchronization target for category {}. "
							+ "This category will be synchronized to Online catalog version", category.getCode());
					categoriesWithoutTarget.add(category);
				}
			}
		}
		return categoriesWithoutTarget;
	}

	private void synchronizeItems(final List itemsToSync, final CatalogModel catalog)
	{
		CatalogVersionSyncJobModel catalogVersionSyncJob = wileySynchronizationService.getCatalogVersionSyncJob(
				createJobIdentifier(catalog.getId()));
		CronJobResult syncResult =
				wileySynchronizationService.performSynchronization(itemsToSync, catalogVersionSyncJob);
		if (!SUCCESS.equals(syncResult))
		{
			throw new RuntimeException("There was an issue during synchronizing product page related information");
		}
	}

	protected String createJobIdentifier(final String catalogId)
	{
		return "sync " + catalogId + ":" + CatalogManager.OFFLINE_VERSION + "->" + CatalogManager.ONLINE_VERSION;
	}
}
