package com.wiley.core.media;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.media.MediaService;


/**
 * Interface extends functionality of {@link MediaService}
 *
 * Created by Raman_Hancharou on 11/9/2015.
 */
public interface WileyMediaService extends MediaService
{


	/**
	 * Gets file name without extension.
	 *
	 * @param mediaModel
	 * 		the media model
	 * @return the file name without extension
	 */
	String getFileNameWithoutExtension(MediaModel mediaModel);

	/**
	 * convert media container to media data
	 * @param currentSite
	 * @param mediaContainer
	 * @return
	 */
	String convertMediaContainerToMediaData(CMSSiteModel currentSite, MediaContainerModel mediaContainer);
}
