package com.wiley.core.mpgs.command;


import com.wiley.core.mpgs.response.WileyFollowOnRefundResponse;
import com.wiley.core.payment.request.WileyFollowOnRefundRequest;


public interface WileyFollowOnRefundCommand extends WileyCommand<WileyFollowOnRefundRequest, WileyFollowOnRefundResponse>
{
}