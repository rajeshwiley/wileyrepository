package com.wiley.core.customer.service;

import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;

import com.wiley.core.customer.ResetPasswordRedirectType;


public interface WileyCustomerAccountService extends CustomerAccountService
{
	void forgottenPassword(CustomerModel customerModel, ResetPasswordRedirectType type);

	void updateCustomerProfile(CustomerModel customerModel) throws DuplicateUidException;

	AddressModel getPaymentAddress(UserModel customerModel);
	
	/**
	 * Checks whether user is registered in the system
	 * @param uid
	 * @return boolean value
	 */
	boolean isRegisteredUser(String uid);

}
