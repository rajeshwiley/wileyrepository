package com.wiley.core.wileycom.restriction.impl;

import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.product.ProductModel;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.wiley.restriction.WileyRestrictProductStrategy;
import com.wiley.core.wiley.restriction.dto.WileyRestrictionCheckResultDto;

public class WileycomRestrictVisibilityByCustomFlagStrategy implements WileyRestrictProductStrategy
{
	@Override
	public boolean isRestricted(final ProductModel product)
	{
		return Boolean.TRUE.equals(product.getCustom());
	}

	@Override
	public boolean isRestricted(final CommerceCartParameter parameter)
	{
		return isRestricted(parameter.getProduct());
	}

	@Override
	public WileyRestrictionCheckResultDto createErrorResult(final CommerceCartParameter parameter)
	{
		return WileyRestrictionCheckResultDto.failureResult(WileyCoreConstants.PRODUCT_IS_NOT_PURCHASABLE,
				String.format("Product [%s] is not available, custom product check.", parameter.getProduct().getCode()), null);
	}

}
