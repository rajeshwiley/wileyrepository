package com.wiley.core.sync.impl;

import com.wiley.core.sync.WileySynchronizationDao;
import de.hybris.platform.catalog.model.synchronization.CatalogVersionSyncJobModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;
import java.util.Map;


public class WileySynchronizationDaoImpl implements WileySynchronizationDao
{
	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Override
	public List<CatalogVersionSyncJobModel> findCatalogVersionSyncJobs(final String code)
	{
		ServicesUtil.validateParameterNotNull(code, "CatalogVersionSyncJob shouldn't be null");
		final CatalogVersionSyncJobModel jobModelExample = new CatalogVersionSyncJobModel();
		jobModelExample.setCode(code);
		return flexibleSearchService.getModelsByExample(jobModelExample);
	}

	@Override
	public <T> List<T> findSynchronizationTargetsBySource(final T item)
	{
		Map<String, Object> params = Collections.singletonMap("source", item);
		SearchResult<T> searchResult = flexibleSearchService.search("SELECT {ist:targetItem}, "
				+ "{ist:targetVersion} FROM {ItemSyncTimestamp AS ist} "
				+ "WHERE {ist:sourceItem} = ?source AND {ist:targetVersion} IS NOT NULL", params);
		return searchResult.getResult();
	}
}
