package com.wiley.core.product;

import de.hybris.platform.core.model.product.ProductModel;

import javax.annotation.Nullable;

import com.wiley.core.model.WileyFreeTrialProductModel;


/**
 * Created by Aliaksei_Zlobich on 3/16/2016.
 */
public interface WileyFreeTrialProductService
{

	WileyFreeTrialProductModel getFreeTrialProductByCode(String code);

	@Nullable
	WileyFreeTrialProductModel getRelatedFreeTrialForProduct(ProductModel productModel);
}
