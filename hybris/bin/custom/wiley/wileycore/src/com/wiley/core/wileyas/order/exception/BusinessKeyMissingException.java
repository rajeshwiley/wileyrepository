package com.wiley.core.wileyas.order.exception;

/**
 * Created by Maksim_Kozich on 21.02.2018
 */
public class BusinessKeyMissingException extends RuntimeException
{
	public BusinessKeyMissingException(final String message)
	{
		super(message);
	}
}
