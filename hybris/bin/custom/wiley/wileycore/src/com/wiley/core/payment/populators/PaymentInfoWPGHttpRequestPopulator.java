package com.wiley.core.payment.populators;

import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.request.AbstractRequestPopulator;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionRequest;
import de.hybris.platform.acceleratorservices.payment.data.PaymentData;
import de.hybris.platform.acceleratorservices.payment.data.PaymentInfoData;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import static org.apache.commons.lang.StringUtils.leftPad;


/**
 * Populator to fill payment info
 */
public class PaymentInfoWPGHttpRequestPopulator extends AbstractRequestPopulator<CreateSubscriptionRequest, PaymentData>
{
	private static final String WPG_CUSTOM_PAYINFO_CARD_NUMBER = "WPG_CUSTOM_payinfo_card_number";
	private static final String WPG_CUSTOM_PAYINFO_EXPIRY_MONTH = "WPG_CUSTOM_payinfo_expiry_month";
	private static final String WPG_CUSTOM_PAYINFO_EXPIRY_YEAR = "WPG_CUSTOM_payinfo_expiry_year";
	private static final char MASKING_CHARACTER = '*';
	private static final int CARD_NUMBER_LENGTH = 16;

	@Override
	public void populate(final CreateSubscriptionRequest createSubscriptionRequest, final PaymentData paymentData)
			throws ConversionException
	{
		PaymentInfoData paymentInfo = createSubscriptionRequest.getPaymentInfoData();
		addRequestQueryParam(paymentData, WPG_CUSTOM_PAYINFO_CARD_NUMBER,
				leftPad(paymentInfo.getCardAccountNumber(), CARD_NUMBER_LENGTH, MASKING_CHARACTER));
		addRequestQueryParam(paymentData, WPG_CUSTOM_PAYINFO_EXPIRY_MONTH, String.valueOf(paymentInfo.getCardExpirationMonth()));
		addRequestQueryParam(paymentData, WPG_CUSTOM_PAYINFO_EXPIRY_YEAR, String.valueOf(paymentInfo.getCardExpirationYear()));
	}
}
