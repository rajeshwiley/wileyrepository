package com.wiley.core.jalo.translators;

import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.impex.jalo.header.HeaderValidationException;
import de.hybris.platform.impex.jalo.header.SpecialColumnDescriptor;
import de.hybris.platform.servicelayer.type.TypeService;

import java.util.List;

import com.wiley.core.adapters.impl.AuthorInfoTwoDimensionListImportAdapter;
import com.wiley.core.model.AuthorInfoModel;


/**
 * Transator for import two dimension data (comma separated list where each item is pipe '|' separated list)
 * into {@link ProductModel#setAuthorInfos(List)}. Impex cell value is imported as english value only (no specific localization).
 *
 * For example, "name1|role1, name2|role2" translates to two {@link AuthorInfoModel},
 * each with English name and English role appropriately.
 */
public class AuthorInfosTranslator extends AbstractWileySpecialValueTranslator
{

	@Override
	public void init(final SpecialColumnDescriptor columnDescriptor) throws HeaderValidationException
	{
		this.columnDescriptor = columnDescriptor;
		this.twoDimensionListImportAdapter = Registry.getApplicationContext().getBean(
				AuthorInfoTwoDimensionListImportAdapter.class);
		this.typeService = Registry.getApplicationContext().getBean(TypeService.class);
	}

}
