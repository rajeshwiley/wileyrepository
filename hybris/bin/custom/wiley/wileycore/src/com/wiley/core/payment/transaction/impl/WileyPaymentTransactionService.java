package com.wiley.core.payment.transaction.impl;

import com.google.common.base.Preconditions;
import com.paypal.hybris.constants.PaypalConstants;
import com.paypal.hybris.data.ResultErrorData;
import com.wiley.core.payment.transaction.PaymentTransactionService;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


public class WileyPaymentTransactionService implements PaymentTransactionService
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyPaymentTransactionService.class);
	private static final int MAX_ALLOWED_MESSAGE_LENGTH = 255;

	@Autowired
	private CommonI18NService commonI18NService;
	@Autowired
	private ModelService modelService;

	@Override
	public PaymentTransactionEntryModel getAcceptedTransactionEntry(final OrderModel orderModel,
			final PaymentTransactionType paymentTransactionType)
	{
		List<PaymentTransactionEntryModel> filteredTransactionEntries =
				getAcceptedTransactionEntries(orderModel.getPaymentTransactions(), paymentTransactionType);
		return getSingleTransactionEntry(orderModel, null, paymentTransactionType, filteredTransactionEntries);
	}

	@Override
	public List<PaymentTransactionEntryModel> getAcceptedTransactionEntries(final OrderModel orderModel,
			final PaymentTransactionType paymentTransactionType)
	{
		return getAcceptedTransactionEntries(orderModel.getPaymentTransactions(), paymentTransactionType);
	}

	@Override
	public PaymentTransactionEntryModel getAcceptedTransactionEntry(final PaymentTransactionModel transaction,
			final PaymentTransactionType transactionEntryType)
	{
		List<PaymentTransactionEntryModel> filteredTransactionEntries = getAcceptedTransactionEntries(Arrays.asList(transaction),
				transactionEntryType);
		return getSingleTransactionEntry(transaction.getOrder(), transaction, transactionEntryType, filteredTransactionEntries);
	}

	private List<PaymentTransactionEntryModel> getAcceptedTransactionEntries(final List<PaymentTransactionModel> transactions,
			final PaymentTransactionType transactionEntryType)
	{
		List<PaymentTransactionEntryModel> filteredTransactionEntries = new ArrayList<>();
		for (PaymentTransactionModel transaction : transactions)
		{
			filteredTransactionEntries.addAll(getAcceptedTransactionEntries(transaction, transactionEntryType));
		}
		return filteredTransactionEntries;
	}

	@Override
	public List<PaymentTransactionEntryModel> getAcceptedTransactionEntries(final PaymentTransactionModel transaction,
			final PaymentTransactionType transactionEntryType)
	{
		Preconditions.checkNotNull(transaction);
		Preconditions.checkNotNull(transactionEntryType);

		if (CollectionUtils.isEmpty(transaction.getEntries()))
		{
			return Collections.emptyList();
		}
		return transaction.getEntries().stream()
				.filter(entry -> transactionEntryType.equals(entry.getType())
						&& TransactionStatus.ACCEPTED.name().equals(entry.getTransactionStatus()))
				.collect(Collectors.toList());
	}

	private PaymentTransactionEntryModel getSingleTransactionEntry(final AbstractOrderModel order,
			final PaymentTransactionModel transaction, final PaymentTransactionType transactionEntryType,
			final List<PaymentTransactionEntryModel> filteredTransactionEntries)
	{
		String transactionCodeLog = "";
		if (transaction != null)
		{
			transactionCodeLog = ", transaction " + transaction.getCode();
		}

		PaymentTransactionEntryModel transactionEntry = null;
		if (filteredTransactionEntries.size() > 1)
		{
			LOG.warn("Order " + order.getCode() + transactionCodeLog
					+ " contains multiple entries with status ACCEPTED and type " + transactionEntryType);
		}

		if (CollectionUtils.isEmpty(filteredTransactionEntries))
		{
			LOG.debug("In order " + order.getCode() + transactionCodeLog
					+ " does not contain entry with status ACCEPTED and type " + transactionEntryType);
		}
		else
		{
			transactionEntry = filteredTransactionEntries.get(0);
		}
		return transactionEntry;
	}

	@Override
	public double calculateTotalAuthorized(final OrderModel order)
	{
		double result = 0D;
		if (order.getPaymentInfo() != null && order.getPaymentTransactions() != null)
		{
			result = order.getPaymentTransactions().stream()
					.filter(trans -> trans.getEntries() != null)
					.flatMap(trans -> trans.getEntries().stream())
					.filter(trEntry -> isAuthAccepted(trEntry))
					.mapToDouble(trEntry -> trEntry.getAmount().doubleValue())
					.sum();
		}
		// round the sum instead of rounding each transaction separately
		return commonI18NService.roundCurrency(result, order.getCurrency().getDigits());
	}

	@Override
	public PaymentTransactionEntryModel createTransactionEntry(final PaymentTransactionType type, final String status,
			final String statusDetails, final String requestId, final AbstractOrderModel abstractOrder,
			final CurrencyModel currency, final double amount, final Date timeStamp,
			final PaymentTransactionModel paymentTransaction)
	{
		final PaymentTransactionEntryModel paymentTransactionEntry = modelService.create(PaymentTransactionEntryModel.class);

		paymentTransactionEntry.setRequestId(requestId);
		paymentTransactionEntry.setType(type);
		paymentTransactionEntry.setTransactionStatus(status);
		paymentTransactionEntry.setTransactionStatusDetails(statusDetails);

		final String code = PaypalConstants.PAYMENT_PROVIDER_NAME + "_order_" + abstractOrder.getCode() + "_stamp_"
				+ System.currentTimeMillis();
		paymentTransactionEntry.setCode(code);

		paymentTransactionEntry.setCurrency(currency);

		final BigDecimal transactionAmount = BigDecimal.valueOf(amount);
		paymentTransactionEntry.setAmount(transactionAmount);

		paymentTransactionEntry.setTime(timeStamp);
		paymentTransactionEntry.setPaymentTransaction(paymentTransaction);
		modelService.save(paymentTransactionEntry);

		return paymentTransactionEntry;
	}

	@Override
	public String createPayPalErrorDetails(final List<ResultErrorData> errors)
	{
		StringBuilder errorString = new StringBuilder();
		if (CollectionUtils.isNotEmpty(errors)) {
			errors.stream().forEach(
					error -> errorString.append(String.format("%s-%s. ", error.getErrorCode(), error.getLongMessage())));
		}
		return StringUtils.substring(errorString.toString(), 0, MAX_ALLOWED_MESSAGE_LENGTH);
	}

	private boolean isAuthAccepted(final PaymentTransactionEntryModel transactionEntry)
	{
		return transactionEntry.getType().equals(PaymentTransactionType.AUTHORIZATION)
				&& TransactionStatus.ACCEPTED.name().equals(transactionEntry.getTransactionStatus())
				&& transactionEntry.getAmount() != null;
	}

	public Optional<PaymentTransactionEntryModel> findAuthorizationEntry(final PaymentTransactionModel transaction)
	{
		return transaction.getEntries().stream()
				.filter(entry ->
						PaymentTransactionType.AUTHORIZATION.equals(entry.getType())
								&& TransactionStatus.ACCEPTED.toString().equals(entry.getTransactionStatus()))
				.findFirst();
	}

	public PaymentTransactionModel getLastPaymentTransaction(final AbstractOrderModel orderModel)
	{
		ServicesUtil.validateParameterNotNull(orderModel, "[orderModel] can't be null.");

		final Comparator<PaymentTransactionModel> creationTimeComparing = Comparator.comparing(
				PaymentTransactionModel::getCreationtime);
		return orderModel.getPaymentTransactions().stream().max(creationTimeComparing)
				.orElseThrow(() -> new IndexOutOfBoundsException("no paymentTransactions"));
	}

}
