package com.wiley.core.wiley.restriction.dto;

/**
 * @author Dzmitryi_Halahayeu
 */
public final class WileyRestrictionCheckResultDto
{
	private final boolean success;
	private String errorMessage;
	private Object[] errorMessageParameters;
	private String errorMessageCode;

	private WileyRestrictionCheckResultDto(final boolean success)
	{
		this.success = success;
	}

	public static WileyRestrictionCheckResultDto successfulResult()
	{
		return new WileyRestrictionCheckResultDto(true);
	}

	public static WileyRestrictionCheckResultDto failureResult(final String errorMessageCode, final String errorMessage,
			final Object[] errorMessageParameters)
	{
		final WileyRestrictionCheckResultDto checkResult = new WileyRestrictionCheckResultDto(false);
		checkResult.setErrorMessageCode(errorMessageCode);
		checkResult.setErrorMessage(errorMessage);
		checkResult.setErrorMessageParameters(errorMessageParameters);
		return checkResult;
	}

	public boolean isSuccess()
	{
		return success;
	}

	public String getErrorMessage()
	{
		return errorMessage;
	}

	public String getErrorMessageCode()
	{
		return errorMessageCode;
	}

	public Object[] getErrorMessageParameters()
	{
		return errorMessageParameters;
	}

	private void setErrorMessage(final String errorMessage)
	{
		this.errorMessage = errorMessage;
	}

	private void setErrorMessageCode(final String errorMessageCode)
	{
		this.errorMessageCode = errorMessageCode;
	}

	private void setErrorMessageParameters(final Object[] errorMessageParameters)
	{
		this.errorMessageParameters = errorMessageParameters;
	}
}
