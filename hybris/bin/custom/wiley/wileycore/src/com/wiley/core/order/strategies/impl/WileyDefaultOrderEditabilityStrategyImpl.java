package com.wiley.core.order.strategies.impl;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.order.WileyCheckoutService;
import com.wiley.core.order.strategies.AbstractOrderModificationStrategy;
import com.wiley.core.order.strategies.WileyOrderEditabilityStrategy;


public class WileyDefaultOrderEditabilityStrategyImpl extends AbstractOrderModificationStrategy
		implements WileyOrderEditabilityStrategy
{
	private Set<OrderStatus> orderEntryEditableStatuses;

	@Autowired
	private WileyCheckoutService wileyCheckoutService;

	@Override
	public boolean isEditable(final OrderModel order)
	{
		return isCorrectSourceSystem(order)
				&& wileyCheckoutService.isNonZeroOrder(order)
				&& hasAllEditableEntries(order)
				&& !hasActiveFulfillmentProcess(order)
				&& !hasFailedFulfillmentProcess(order);
	}

	@Override
	public boolean hasAllEditableEntries(final OrderModel order)
	{
		return hasAllEntriesInState(order, getOrderEntryEditableStatuses());
	}

	private Set<OrderStatus> getOrderEntryEditableStatuses()
	{
		return orderEntryEditableStatuses;
	}

	@Required
	public void setOrderEntryEditableStatuses(final Set<OrderStatus> orderEntryEditableStatuses)
	{
		this.orderEntryEditableStatuses = orderEntryEditableStatuses;
	}
}
