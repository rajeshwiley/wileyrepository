package com.wiley.core.partner.impl;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.user.daos.UserGroupDao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.wiley.core.model.WileyPartnerCompanyModel;
import com.wiley.core.partner.WileyPartnerCompanyDao;


/**
 * DAO for {@link WileyPartnerCompanyModel}.
 */

public class DefaultWileyPartnerCompanyDao extends DefaultGenericDao<WileyPartnerCompanyModel> implements WileyPartnerCompanyDao
{
	@Resource
	private UserGroupDao userGroupDao;

	public DefaultWileyPartnerCompanyDao()
	{
		super(WileyPartnerCompanyModel._TYPECODE);
	}

	private static final String PARTNERS_BY_CATEGORY = "SELECT DISTINCT {partner." + WileyPartnerCompanyModel.PK + "} "
			+ "FROM {" + WileyPartnerCompanyModel._CATEGORY2PARTNERCOMPANYRELATION + " AS rel "
			+ "JOIN " + WileyPartnerCompanyModel._TYPECODE + " AS partner "
			+ "ON {rel.target} = {partner." + WileyPartnerCompanyModel.PK + "} "
			+ "JOIN " + CategoryModel._TYPECODE + " AS category "
			+ "ON {rel.source} = {category." + CategoryModel.PK + "}} "
			+ "WHERE {category." + CategoryModel.CODE + "} = ?categoryCode";

	@Override
	public List<WileyPartnerCompanyModel> getPartnersByParentUserGroup(final String userGroupUid)
	{
		UserGroupModel preferredPartnerUserGroup = findUserGroupByUid(userGroupUid);
		final String query =
				"SELECT {partner.pk} "
						+ "FROM {" + PrincipalGroupModel._PRINCIPALGROUPRELATION + " AS rel "
						+ "JOIN " + WileyPartnerCompanyModel._TYPECODE + " AS partner "
						+ "ON {rel.source} = {partner.pk}} "
						+ "WHERE {rel.target} = " + preferredPartnerUserGroup.getPk();
		final SearchResult<WileyPartnerCompanyModel> searchResult = getFlexibleSearchService().search(query);
		return searchResult.getResult();
	}

	@Override
	public List<WileyPartnerCompanyModel> getPartnersByCategory(final String categoryCode)
	{
		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put("categoryCode", categoryCode);
		final SearchResult<WileyPartnerCompanyModel> searchResult = getFlexibleSearchService()
				.search(new FlexibleSearchQuery(PARTNERS_BY_CATEGORY, queryParams));
		return searchResult.getResult();
	}


	@Override
	public UserGroupModel findUserGroupByUid(final String uid)
	{
		return userGroupDao.findUserGroupByUid(uid);
	}
}
