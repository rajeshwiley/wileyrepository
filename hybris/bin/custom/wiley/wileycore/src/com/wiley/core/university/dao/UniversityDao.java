package com.wiley.core.university.dao;

import de.hybris.platform.core.model.c2l.RegionModel;

import java.util.List;

import javax.annotation.Nonnull;

import com.wiley.core.model.UniversityModel;


/**
 * Dao to retrieve University related data
 */
public interface UniversityDao
{
	/**
	 * Retrieves list of active universities for region
	 *
	 * @param region
	 * @return
	 */
	@Nonnull
	List<UniversityModel> getActiveUniversitiesByRegion(@Nonnull RegionModel region);
}
