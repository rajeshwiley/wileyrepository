package com.wiley.core.mpgs.dto.verify;



import com.wiley.core.mpgs.dto.json.Response;
import com.wiley.core.mpgs.dto.json.Transaction;


public class MPGSVerifyResponseDTO
{
	private String result;
	private Response response;
	private Transaction transaction;
	private String timeOfRecord;

	public String getResult()
	{
		return result;
	}

	public void setResult(final String result)
	{
		this.result = result;
	}

	public Response getResponse()
	{
		return response;
	}

	public void setResponse(final Response response)
	{
		this.response = response;
	}

	public Transaction getTransaction()
	{
		return transaction;
	}

	public void setTransaction(final Transaction transaction)
	{
		this.transaction = transaction;
	}

	public String getTimeOfRecord()
	{
		return timeOfRecord;
	}

	public void setTimeOfRecord(final String timeOfRecord)
	{
		this.timeOfRecord = timeOfRecord;
	}
}
