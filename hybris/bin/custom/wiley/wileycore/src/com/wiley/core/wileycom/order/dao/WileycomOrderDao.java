package com.wiley.core.wileycom.order.dao;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.daos.OrderDao;

import java.util.Optional;


/**
 * The interface Wileycom order dao.
 */
public interface WileycomOrderDao extends OrderDao
{
	/**
	 * Find orders by code order model.
	 *
	 * @param orderCode
	 * 		the order code
	 * @return the order model
	 */
	OrderModel findOrdersByCode(String orderCode);

	/**
	 * Returns the last order (first order from order list sorted by date in descending order) for the given customer.
	 */
	Optional<OrderModel> getLastOrderForCustomer(CustomerModel customerModel);
}
