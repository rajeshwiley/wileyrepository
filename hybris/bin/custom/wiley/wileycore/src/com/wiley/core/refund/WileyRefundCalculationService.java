package com.wiley.core.refund;

import de.hybris.platform.core.model.order.OrderModel;

import java.math.BigDecimal;


public interface WileyRefundCalculationService
{
	BigDecimal calculateMaximumAvailableRefundAmountForOrder(OrderModel orderModels);
}
