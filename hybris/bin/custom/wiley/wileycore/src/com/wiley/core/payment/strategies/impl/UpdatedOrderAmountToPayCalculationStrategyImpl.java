package com.wiley.core.payment.strategies.impl;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.base.Preconditions;
import com.wiley.core.order.PaymentActonType;
import com.wiley.core.order.PendingPaymentActon;
import com.wiley.core.payment.strategies.AmountToPayCalculationStrategy;
import com.wiley.core.wiley.order.impl.WileyOrderPaymentServiceImpl;


public class UpdatedOrderAmountToPayCalculationStrategyImpl implements AmountToPayCalculationStrategy
{
	@Autowired
	private CommonI18NService commonI18NService;
	@Autowired
	private WileyOrderPaymentServiceImpl wileyOrderPaymentService;

	@Override
	public BigDecimal getOrderAmountToPay(final AbstractOrderModel abstractOrderModel)
	{
		Preconditions.checkArgument(abstractOrderModel instanceof OrderModel, "Not OrderModel");
		Preconditions.checkState(abstractOrderModel.getCalculated(), "Order is not calculated");

		OrderModel orderModel = (OrderModel) abstractOrderModel;
		CurrencyModel currencyModel = orderModel.getCurrency();

		Collection<PendingPaymentActon> pendingAuthorizeActions = wileyOrderPaymentService.getPendingPaymentActions(orderModel)
				.stream()
				.filter(this::isAuthorizeAction)
				.collect(Collectors.toList());

		Double amountToAuthorize = pendingAuthorizeActions.stream().mapToDouble(PendingPaymentActon::getAmount).sum();



		final int digits = currencyModel.getDigits();
		final double totalPriceWithTax = commonI18NService.roundCurrency(amountToAuthorize, digits);
		return BigDecimal.valueOf(totalPriceWithTax);
	}

	private boolean isAuthorizeAction(final PendingPaymentActon paymentAction)
	{
		return PaymentActonType.AUTHORIZE.equals(paymentAction.getAction());
	}
}
