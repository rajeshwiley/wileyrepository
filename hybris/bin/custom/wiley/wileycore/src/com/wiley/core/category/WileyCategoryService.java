package com.wiley.core.category;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantValueCategoryModel;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;


/**
 * Interface extends functionality of {@link CategoryService}
 *
 * @author Aliaksei_Zlobich
 */
public interface WileyCategoryService extends CategoryService
{

	/**
	 * Tries to find the closest system requirements on category tree.
	 *
	 * @param categories
	 * 		source collection
	 * @param categoryFilter
	 * 		category filter
	 * @return system requirements from category or empty string.
	 */
	@Nullable
	String getSystemRequirementsFromCategoryTree(@Nullable Collection<CategoryModel> categories,
			@Nullable Predicate<CategoryModel> categoryFilter);


	/**
	 * `
	 * Gets primary wiley category (ex.: CPA, CMA, PMP etc.) for product.
	 *
	 * @param product
	 * 		the product
	 * @return the primary wiley category for product
	 */
	CategoryModel getPrimaryWileyCategoryForProduct(@Nullable ProductModel product);

	/**
	 * Gets primary wiley category (ex.: CPA, CMA, PMP etc.) for given variant value category (e.g WEL_CFA_LEVEL_III).
	 *
	 * @param variantValueCategoryModel
	 * 		the variant value category
	 * @return the primary wiley category for variant value category or null if corresponding primary cannot be found
	 */
	CategoryModel getPrimaryWileyCategoryForVariantCategory(VariantValueCategoryModel variantValueCategoryModel);

	/**
	 * Gets primary supercategory category for all partner categories in given catalog version
	 *
	 * @return primary supercategory category for all partner categories
	 */
	CategoryModel getPartnersCategory(CatalogVersionModel catalogVersion);

	/**
	 * Find all super categories that do not have super categories
	 * @param categoryModel - category to find
	 * @return - Collection of categories, can contain duplicates.
	 */
	List<CategoryModel> getTopLevelCategoriesForCategory(CategoryModel categoryModel);

	/**
	 * Checks if current category is CFA Category
	 *
	 * @param category
	 * @return true if category is CFA category
	 */
	boolean isCFACategory(CategoryModel category);

	/**
	 * Checks if current category is CFA Level Category
	 *
	 * @param category
	 * @return true if category is CFA category
	 */
	boolean isCFALevelCategory(CategoryModel category);

	/**
	 * Checks if current category is CFA Level Variant Category
	 *
	 * @param category
	 * @return true if category is CFA category
	 */
	boolean isCFALevelVariantCategory(CategoryModel category);

	/**
	 * Checks if current category is Free Trial Category
	 *
	 * @param category
	 * @return true if category is Free Trial category
	 */
	boolean isFreeTrialCategory(CategoryModel category);

	/**
	 * Checks if current category is Gift Cards Category
	 *
	 * @param category
	 * @return true if category is Gift Cards category
	 */
	boolean isGiftCardsCategory(CategoryModel category);

	/**
	 * Checks if current category is Partners Category
	 *
	 * @param category
	 * @return true if category is Partners category
	 */
	boolean isPartnersCategory(CategoryModel category);

	/**
	 * Checks if current category is Supplements Category
	 *
	 * @param category
	 * @return true if category is Supplements category
	 */
	boolean isSupplementsCategory(CategoryModel category);

	/**
	 * Checks if category with code exists for catalogVersion
	 * @param catalogVersion
	 * @param categoryCode
	 * @return true if category exists
	 */
	boolean isCategoryForCodeExists(CatalogVersionModel catalogVersion, String categoryCode);

	/**
	 * Find all sub categories with descendant products
	 * @param categoryModel - category to find
	 * @return - Collection of categories.
	 */
	List<CategoryModel> getSubcategoriesWithDescendantProducts(CategoryModel categoryModel);
}
