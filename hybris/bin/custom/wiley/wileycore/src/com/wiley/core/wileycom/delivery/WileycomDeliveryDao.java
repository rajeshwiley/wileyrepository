package com.wiley.core.wileycom.delivery;

import java.util.List;

import com.wiley.core.model.ExternalDeliveryModeModel;


/**
 * Created by Mikhail_Asadchy on 8/29/2016.
 */
public interface WileycomDeliveryDao
{

	List<ExternalDeliveryModeModel> getSupportedExternalDeliveryModes(List<String> externalCodes);

}
