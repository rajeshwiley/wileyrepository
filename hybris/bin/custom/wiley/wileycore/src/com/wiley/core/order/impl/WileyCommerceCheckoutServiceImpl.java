package com.wiley.core.order.impl;

import de.hybris.platform.commerceservices.order.impl.DefaultCommerceCheckoutService;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;

import javax.annotation.Nonnull;

import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.order.WileyCommerceCheckoutService;
import com.wiley.core.order.WileySetPaymentAddressStrategy;


/**
 * @author Dzmitryi_Halahayeu
 */
public class WileyCommerceCheckoutServiceImpl extends DefaultCommerceCheckoutService implements WileyCommerceCheckoutService
{
	private WileySetPaymentAddressStrategy wileySetPaymentAddressStrategy;

	@Override
	public void setPaymentAddress(@Nonnull final CommerceCheckoutParameter commerceCartParameter)
	{
		wileySetPaymentAddressStrategy.setPaymentAddress(commerceCartParameter);
	}

	@Required
	public void setWileySetPaymentAddressStrategy(
			final WileySetPaymentAddressStrategy wileySetPaymentAddressStrategy)
	{
		this.wileySetPaymentAddressStrategy = wileySetPaymentAddressStrategy;
	}
}
