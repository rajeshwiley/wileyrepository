package com.wiley.core.order.dao;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.enums.ProcessState;

import java.util.Optional;

import javax.annotation.Nonnull;

import com.wiley.core.model.WileyOrderProcessModel;


public interface WileyOrderProcessDao
{
	/**
	 * Search for business process which {@link WileyOrderProcessModel#ACTIVEFOR} is assinged to <code>order</code>
	 *
	 * @param order
	 * 		Assigned order
	 * @return Optional with business process item
	 */
	Optional<WileyOrderProcessModel> findProcessActiveFor(@Nonnull OrderModel order);

	/**
	 * Search for business process which {@link OrderProcessModel#ORDER} is equal to <code>orderModel<code/>
	 * and {@link OrderProcessModel#PROCESSSTATE} is equal to to <code>processState</code>
	 *
	 * @param orderModel
	 * 		order model
	 * @param processState
	 * 		process state
	 * @return Optional with business process item
	 */
	Optional<OrderProcessModel> findProcessInSateForOrder(@Nonnull OrderModel orderModel, ProcessState processState);
}
