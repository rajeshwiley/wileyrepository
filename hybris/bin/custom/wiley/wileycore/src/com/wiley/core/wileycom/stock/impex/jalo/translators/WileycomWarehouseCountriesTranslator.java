package com.wiley.core.wileycom.stock.impex.jalo.translators;

import com.wiley.core.wileycom.valuetranslator.AbstractWileycomAllowEmptySpecialValueTranslator;


/**
 * Implementation of {@link AbstractWileycomAllowEmptySpecialValueTranslator} to add
 * {@link de.hybris.platform.core.model.c2l.CountryModel} to
 * {@link de.hybris.platform.ordersplitting.model.WarehouseModel}. Import logic is delegated to
 * {@link com.wiley.core.wileycom.stock.impex.jalo.translators.adapter.impl.WileycomWarehouseCountriesImportAdapterImpl}
 *
 * @author Dzmitryi_Halahayeu
 */
public class WileycomWarehouseCountriesTranslator
		extends AbstractWileycomAllowEmptySpecialValueTranslator
{
	private static final String DEFAULT_IMPORT_ADAPTER_NAME = "wileycomWarehouseCountriesImportAdapter";

	@Override
	protected String getDefaultImportAdapterName()
	{
		return DEFAULT_IMPORT_ADAPTER_NAME;
	}

}
