package com.wiley.core.integration.esb;

import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;

import javax.annotation.Nonnull;

import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;


public interface EsbExternalAddressGateway
{
	String EXTERNAL_ADDRESS_CUSTOMER = "header_customer";

	/**
	 * Executes call to add customer's shipping address into external systems.
	 * Performs call to ESB service and is not aware of further integrations.
	 * In case of inconsistent address or ESB failure ExternalSystemException should be caught
	 *
	 * @param addressModel
	 * 		The address model with filled data including externalId value.
	 * 		This model may not be persisted in DB
	 * @param customerModel
	 * 		The customer existing in both external system and hybris DB.
	 * 		This model must have not null customerId value
	 */
	void addShippingAddress(@Nonnull @Payload AddressModel addressModel,
			@Nonnull @Header(EXTERNAL_ADDRESS_CUSTOMER) CustomerModel customerModel);

	/**
	 * Executes call to update customer's shipping address in external systems.
	 * Performs call to ESB service and is not aware of further integrations.
	 * This address must exist in external system with id equals to given addressModel.externalId
	 * In case of inconsistent address or ESB failure ExternalSystemException should be caught
	 *
	 * @param addressModel
	 * 		The address model with filled data including externalId value.
	 * @param customerModel
	 * 		The customer existing in both external system and hybris DB.
	 * 		This model must have not null customerId value
	 */
	void updateShippingAddress(@Nonnull @Payload AddressModel addressModel,
			@Nonnull @Header(EXTERNAL_ADDRESS_CUSTOMER) CustomerModel customerModel);

	/**
	 * Executes call to delete customer's shipping address in external systems.
	 * Performs call to ESB service and is not aware of further integrations.
	 * This address must exist in external system with id equals to given addressModel.externalId
	 *
	 * @param addressModel
	 * 		The address model with filled data including externalId value.
	 * @param customerModel
	 * 		The customer existing in both external system and hybris DB.
	 * 		This model must have not null customerId value
	 */
	void deleteShippingAddress(@Nonnull @Payload AddressModel addressModel,
			@Nonnull @Header(EXTERNAL_ADDRESS_CUSTOMER) CustomerModel customerModel);


	/**
	 * Executes call to add customer's billing address into external systems.
	 * Performs call to ESB service and is not aware of further integrations.
	 * In case of inconsistent address or ESB failure ExternalSystemException should be caught
	 *
	 * @param addressModel
	 * 		The address model with filled data including externalId value.
	 * 		This model may not be persisted in DB
	 * @param customerModel
	 * 		The customer existing in both external system and hybris DB.
	 * 		This model must have not null customerId value
	 */
	void addBillingAddress(@Nonnull @Payload AddressModel addressModel,
			@Nonnull @Header(EXTERNAL_ADDRESS_CUSTOMER) CustomerModel customerModel);

	/**
	 * Executes call to update customer's billing address in external systems.
	 * Performs call to ESB service and is not aware of further integrations.
	 * This address must exist in external system with id equals to given addressModel.externalId
	 * In case of inconsistent address or ESB failure ExternalSystemException should be caught
	 *
	 * @param addressModel
	 * 		The address model with filled data including externalId value.
	 * @param customerModel
	 * 		The customer existing in both external system and hybris DB.
	 * 		This model must have not null customerId value
	 */
	void updateBillingAddress(@Nonnull @Payload AddressModel addressModel,
			@Nonnull @Header(EXTERNAL_ADDRESS_CUSTOMER) CustomerModel customerModel);

	/**
	 * Executes call to delete customer's billing address in external systems.
	 * Performs call to ESB service and is not aware of further integrations.
	 * This address must exist in external system with id equals to given addressModel.externalId
	 *
	 * @param addressModel
	 * 		The address model with filled data including externalId value.
	 * @param customerModel
	 * 		The customer existing in both external system and hybris DB.
	 * 		This model must have not null customerId value
	 */
	void deleteBillingAddress(@Nonnull @Payload AddressModel addressModel,
			@Nonnull @Header(EXTERNAL_ADDRESS_CUSTOMER) CustomerModel customerModel);


}
