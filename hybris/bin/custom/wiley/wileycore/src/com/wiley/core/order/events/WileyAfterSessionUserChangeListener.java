package com.wiley.core.order.events;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;
import de.hybris.platform.servicelayer.event.events.AfterSessionUserChangeEvent;
import de.hybris.platform.servicelayer.user.UserService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.event.WileyAbstractSiteAwareApplicationListener;


/**
 * Copypaste AfterSessionUserChangeListener to add ability use site specific cartService
 *
 * Created by Uladzimir_Barouski on 6/30/2016.
 */
public class WileyAfterSessionUserChangeListener extends WileyAbstractSiteAwareApplicationListener
{
	private static final Logger LOG = Logger.getLogger(WileyAfterSessionUserChangeListener.class);

	private CartService cartService;

	private UserService userService;

	@Override
	protected void siteSpecificOnEvent(final AbstractEvent abstractEvent)
	{
		if (abstractEvent instanceof AfterSessionUserChangeEvent)
		{
			if (LOG.isDebugEnabled())
			{
				LOG.debug("AfterSessionUserChangeEvent received.");
			}
			final UserModel user = userService.getCurrentUser();
			cartService.changeCurrentCartUser(user);
		}
	}

	@Required
	public void setCartService(final CartService cartService)
	{
		this.cartService = cartService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}
}
