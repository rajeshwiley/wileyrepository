package com.wiley.core.cms.dao.impl;

import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.ItemSyncTimestampModel;
import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;
import de.hybris.platform.cms2.model.contents.contentslot.ContentSlotModel;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.relations.ContentSlotForPageModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.wiley.core.cms.dao.WileyCMSComponentDAO;



/**
 * Created by Anton_Lukyanau on 1/4/2017.
 */
public class WileyCMSComponentDAOImpl implements WileyCMSComponentDAO
{
	private static final String FIND_SYNC_COMPONENTS_QUERY =
			"SELECT {" + ItemSyncTimestampModel.SOURCEITEM + "}"
					+ " FROM {" + ItemSyncTimestampModel._TYPECODE + " as ists} WHERE"
					+ " {ists." + ItemSyncTimestampModel.LASTSYNCTIME + "} >= {c." + AbstractCMSComponentModel.MODIFIEDTIME + "}"
					+ " AND {ists." + ItemSyncTimestampModel.LASTSYNCSOURCEMODIFIEDTIME + "} = {c."
					+ AbstractCMSComponentModel.MODIFIEDTIME;

	private static final String FIND_NOT_SYNC_COMPONENTS_FOR_PAGE_QUERY =
			"SELECT {c." + AbstractCMSComponentModel.PK + "} FROM "
					+ "{" + AbstractPageModel._TYPECODE + " as apm "
					+ "JOIN " + ContentSlotForPageModel._TYPECODE + " as csfpm on {csfpm.page}={apm.PK} "
					+ " JOIN " + ContentSlotModel._TYPECODE + " as csm on {csfpm.contentSlot}={csm.pk} "
					+ " JOIN " + AbstractCMSComponentModel._ELEMENTSFORSLOT + " as elForSlot on {csm.pk}={elForSlot.source} "
					+ " JOIN " + AbstractCMSComponentModel._TYPECODE + " as c on {c.pk}={elForSlot.target} "
					+ " JOIN " + CatalogVersionModel._TYPECODE + " as cv on {apm.catalogVersion}={cv.pk} "
					+ " JOIN " + CatalogModel._TYPECODE + " as catalog on {cv.catalog}={catalog.pk}"
					+ "} WHERE "
					+ " {apm." + AbstractPageModel.UID + "} = ?uid AND "
					+ " {catalog." + CatalogModel.ID + "} = ?catalogId AND "
					+ " {cv." + CatalogVersionModel.VERSION + "} LIKE ?catalogVersion AND"
					+ " {c.pk} NOT IN "
					+ "({{" + FIND_SYNC_COMPONENTS_QUERY + "}}})";

	@Resource
	private FlexibleSearchService flexibleSearchService;



	@Override
	public List<AbstractCMSComponentModel> findNotSyncComponentsForPage(final AbstractPageModel componentModel)
	{
		final Map<String, Object> queryParams = new HashMap<String, Object>();
		queryParams.put("uid", componentModel.getUid());
		queryParams.put("catalogId", componentModel.getCatalogVersion().getCatalog().getId());
		queryParams.put("catalogVersion", componentModel.getCatalogVersion().getVersion());

		final List<AbstractCMSComponentModel> result = flexibleSearchService.<AbstractCMSComponentModel> search(
				new FlexibleSearchQuery(FIND_NOT_SYNC_COMPONENTS_FOR_PAGE_QUERY, queryParams)).getResult();

		return result;
	}
}