package com.wiley.core.integration.sabrix.service;

import de.hybris.platform.core.model.product.ProductModel;

import java.util.Collection;
import java.util.Map;


/**
 * Service designed to split set price over set's products in proportion of they initial price
 *
 * For example product1 has price $80 and product2 has price $60,
 * Product set includes this two products and has discounted price 70$,
 * than service should spread set's price 70$ over product components and it will create map
 * product1 -> price: $40
 * product2 -> price: $30
 *
 * Author Herman_Chukhrai (EPAM)
 */
public interface SetPriceSeparatorService
{
	/**
	 * Method would split set's price over product set components in proportion to their initial prices
	 *
	 * @param products
	 * 		components of product set
	 * @param setPrice
	 * 		price of price set
	 * @return product - price map
	 */
	Map<ProductModel, Double> splitSetPriceOverProducts(Collection<ProductModel> products, Double setPrice);
}
