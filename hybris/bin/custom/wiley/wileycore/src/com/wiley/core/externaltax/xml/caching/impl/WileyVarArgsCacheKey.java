package com.wiley.core.externaltax.xml.caching.impl;


import de.hybris.platform.core.Registry;
import de.hybris.platform.regioncache.key.CacheKey;
import de.hybris.platform.regioncache.key.CacheUnitValueType;

import java.util.Arrays;


public class WileyVarArgsCacheKey implements CacheKey
{
	private final Object[] params;

	public WileyVarArgsCacheKey(final Object... params)
	{
		this.params = params;
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (!(o instanceof WileyVarArgsCacheKey))
		{
			return false;
		}

		final WileyVarArgsCacheKey that = (WileyVarArgsCacheKey) o;

		return Arrays.equals(params, that.params);
	}

	@Override
	public int hashCode()
	{
		return Arrays.hashCode(params);
	}

	@Override
	public CacheUnitValueType getCacheValueType()
	{
		return CacheUnitValueType.SERIALIZABLE;
	}

	@Override
	public Object getTypeCode()
	{
		return "__VarArgsKey__";
	}

	@Override
	public String getTenantId()
	{
		return Registry.getCurrentTenant().getTenantID();
	}
}
