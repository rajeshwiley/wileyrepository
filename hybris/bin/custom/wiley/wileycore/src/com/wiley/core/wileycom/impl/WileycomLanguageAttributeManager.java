package com.wiley.core.wileycom.impl;

import de.hybris.platform.acceleratorservices.urlencoder.attributes.impl.AbstractUrlEncodingAttributeManager;
import de.hybris.platform.acceleratorservices.urlencoder.attributes.impl.DefaultLanguageAttributeManager;
import de.hybris.platform.processengine.model.BusinessProcessModel;

import java.util.Collection;

import javax.annotation.Resource;

import com.wiley.core.locale.WileyLocaleService;


public class WileycomLanguageAttributeManager extends AbstractUrlEncodingAttributeManager
{
	@Resource
	private DefaultLanguageAttributeManager languageAttributeManager;

	@Resource
	private WileyLocaleService wileyLocaleService;

	@Override
	public Collection<String> getAllAvailableValues()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean isValid(final String value)
	{
		return wileyLocaleService.isValidEncodedLocale(value);
	}

	@Override
	public void updateAndSyncForAttrChange(final String value)
	{
		wileyLocaleService.setEncodedLocale(value);
	}

	@Override
	public String getDefaultValue()
	{
		return wileyLocaleService.getDefaultEncodedLocale();
	}

	@Override
	public String getCurrentValue()
	{
		return wileyLocaleService.getCurrentEncodedLocale();
	}

	@Override
	public String getAttributeValueForEmail(final BusinessProcessModel businessProcessModel)
	{
		return languageAttributeManager.getAttributeValueForEmail(businessProcessModel);
	}
}
