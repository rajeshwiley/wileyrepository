package com.wiley.core.event;

import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.wiley.core.integration.order.WileyOrderEntryGateway;

public class WileyOrderEntryEventListener extends AbstractEventListener<WileyOrderEntryEvent>
{
	private static final Logger LOG = Logger.getLogger(WileyOrderEntryEventListener.class);

	@Resource
	private WileyOrderEntryGateway wileyOrderEntryGateway;

	@Override
	protected void onEvent(final WileyOrderEntryEvent wileyOrderEntryEvent)
	{
		wileyOrderEntryGateway.publish(wileyOrderEntryEvent.getEntry(), wileyOrderEntryEvent.getCode());
	}
}
