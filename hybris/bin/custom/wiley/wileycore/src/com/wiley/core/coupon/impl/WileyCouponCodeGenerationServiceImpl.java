package com.wiley.core.coupon.impl;

import de.hybris.platform.couponservices.model.MultiCodeCouponModel;
import de.hybris.platform.couponservices.services.impl.DefaultCouponCodeGenerationService;
import de.hybris.platform.jalo.JaloSystemException;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.voucher.jalo.SerialVoucher;
import de.hybris.platform.voucher.model.SerialVoucherModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;


/**
 * Created by Sergiy_Mishkovets on 11/7/2017.
 */
public class WileyCouponCodeGenerationServiceImpl extends DefaultCouponCodeGenerationService
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyCouponCodeGenerationServiceImpl.class);

	@Autowired
	private FlexibleSearchService flexibleSearchService;

	@Value("${legacy.serial.voucher.validation}")
	private boolean validateThroughSerialVoucher;

	@Override
	protected boolean verifyCipherText(final MultiCodeCouponModel coupon, final String couponCode)
	{
		boolean result = super.verifyCipherText(coupon, couponCode);
		if (!result && validateThroughSerialVoucher)
		{
			try
			{
				SerialVoucherModel voucherTemplate = new SerialVoucherModel();
				voucherTemplate.setCode(coupon.getCouponId());
				SerialVoucherModel voucherModel = flexibleSearchService.getModelByExample(voucherTemplate);
				SerialVoucher voucher = getModelService().getSource(voucherModel);
				return voucher.checkVoucherCode(couponCode);
			}
			catch (ModelNotFoundException | IllegalStateException | JaloSystemException exc)
			{
				LOG.warn("Old voucher code cannot be validated: {}", exc.getMessage());
			}
		}
		return result;
	}

}
