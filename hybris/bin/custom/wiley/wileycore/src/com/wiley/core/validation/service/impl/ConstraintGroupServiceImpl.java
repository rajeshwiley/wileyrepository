package com.wiley.core.validation.service.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.contents.CMSItemModel;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.internal.dao.GenericDao;
import de.hybris.platform.validation.model.constraints.ConstraintGroupModel;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import com.wiley.core.validation.service.ConstraintGroupService;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateIfSingleResult;
import static java.util.Collections.singletonMap;


public class ConstraintGroupServiceImpl implements ConstraintGroupService
{

	@Resource
	private GenericDao<ConstraintGroupModel> constraintGroupGenericDao;

	@Override
	public ConstraintGroupModel getConstraintGroupForId(final String constraintGroupId)
	{
		final List<ConstraintGroupModel> constraintGroupModels = constraintGroupGenericDao.find(
				singletonMap(ConstraintGroupModel.ID, constraintGroupId));
		validateIfSingleResult(constraintGroupModels, ConstraintGroupModel.class, ConstraintGroupModel.ID, constraintGroupId);
		return constraintGroupModels.get(0);
	}

	@Override
	public Collection<ConstraintGroupModel> getCatalogSpecificConstraintGroupForItem(final ItemModel item)
	{
		final CatalogVersionModel catalogVersionModel = getItemCatalogVersion(item);
		return catalogVersionModel != null ? catalogVersionModel.getCatalog().getConstraintGroups() : Collections.emptyList();

	}

	private CatalogVersionModel getItemCatalogVersion(final ItemModel item)
	{
		CatalogVersionModel catalogVersionModel = null;
		if (item instanceof ProductModel)
		{
			catalogVersionModel = ((ProductModel) item).getCatalogVersion();
		}
		else if (item instanceof CategoryModel)
		{
			catalogVersionModel = ((CategoryModel) item).getCatalogVersion();
		}
		else if (item instanceof CMSItemModel)
		{
			catalogVersionModel = ((CMSItemModel) item).getCatalogVersion();
		}
		return catalogVersionModel;
	}
}
