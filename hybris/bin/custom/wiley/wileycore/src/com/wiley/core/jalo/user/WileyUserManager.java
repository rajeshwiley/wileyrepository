package com.wiley.core.jalo.user;

import de.hybris.platform.jalo.user.PasswordCheckingStrategy;
import de.hybris.platform.jalo.user.User;
import de.hybris.platform.jalo.user.UserManager;
import de.hybris.platform.persistence.security.EJBPasswordEncoderNotFoundException;


/**
 * Created by Uladzimir_Barouski on 5/5/2016.
 */
public class WileyUserManager extends UserManager
{
	private PasswordCheckingStrategy passwordCheckingStrategy;

	@Override
	public boolean checkPassword(final User user, final String plainPassword) throws EJBPasswordEncoderNotFoundException
	{
		return passwordCheckingStrategy.checkPassword(user, plainPassword);
	}

	@Override
	public void setPasswordCheckingStrategy(final PasswordCheckingStrategy paramPasswordCheckingStrategy)
	{
		passwordCheckingStrategy = paramPasswordCheckingStrategy;
	}
}
