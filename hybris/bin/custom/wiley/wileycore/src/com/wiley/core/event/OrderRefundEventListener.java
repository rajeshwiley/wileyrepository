/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.wiley.core.event;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.commerceservices.event.AbstractSiteEventListener;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.returns.model.ReturnProcessModel;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import org.springframework.beans.factory.annotation.Required;


/**
 * The type Order refund event listener.
 */
public class OrderRefundEventListener extends AbstractSiteEventListener<WileyOrderRefundEvent>
{

	private ModelService modelService;
	private BusinessProcessService businessProcessService;

	/**
	 * On site event.
	 *
	 * @param event
	 * 		the event
	 */
	@Override
	protected void onSiteEvent(final WileyOrderRefundEvent event)
	{
		ReturnRequestModel returnRequestModel = event.getReturnRequest();
		final OrderModel orderModel = returnRequestModel.getOrder();
		ReturnProcessModel returnProcessModel = getBusinessProcessService().createProcess(
				"sendOrderRefundEmailProcess-" + orderModel.getCode() + "-" + System.currentTimeMillis(),
				"sendOrderRefundEmailProcess");
		returnProcessModel.setReturnRequest(returnRequestModel);
		getModelService().save(returnProcessModel);
		getBusinessProcessService().startProcess(returnProcessModel);
	}

	/**
	 * Should handle event boolean.
	 *
	 * @param event
	 * 		the event
	 * @return the boolean
	 */
	@Override
	protected boolean shouldHandleEvent(final WileyOrderRefundEvent event)
	{
		ReturnRequestModel returnRequest = event.getReturnRequest();
		ServicesUtil.validateParameterNotNullStandardMessage("event.returnRequest", returnRequest);
		final OrderModel order = returnRequest.getOrder();
		ServicesUtil.validateParameterNotNullStandardMessage("event.order", order);
		final BaseSiteModel site = order.getSite();
		ServicesUtil.validateParameterNotNullStandardMessage("event.order.site", site);
		return SiteChannel.B2C.equals(site.getChannel());
	}

	/**
	 * Gets model service.
	 *
	 * @return the model service
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * Sets model service.
	 *
	 * @param modelService
	 * 		the model service
	 */
	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	/**
	 * Gets business process service.
	 *
	 * @return the business process service
	 */
	public BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	/**
	 * Sets business process service.
	 *
	 * @param businessProcessService
	 * 		the business process service
	 */
	@Required
	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}

}
