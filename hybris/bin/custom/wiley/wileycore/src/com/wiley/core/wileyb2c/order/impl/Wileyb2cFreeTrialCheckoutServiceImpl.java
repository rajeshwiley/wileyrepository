package com.wiley.core.wileyb2c.order.impl;

import de.hybris.platform.commerceservices.order.CommerceAddToCartStrategy;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCheckoutService;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.commerceservices.service.data.CommerceOrderResult;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.order.CartFactory;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.testframework.Transactional;

import javax.annotation.Resource;

import com.wiley.core.enums.OrderType;
import com.wiley.core.model.WileyProductModel;
import com.wiley.core.product.WileyProductService;
import com.wiley.core.wileyb2c.order.Wileyb2cFreeTrialCheckoutService;
import com.wiley.core.wileyb2c.subscription.services.Wileyb2cFreeTrialService;


/**
 * Created by Uladzimir_Barouski on 9/20/2016.
 */
public class Wileyb2cFreeTrialCheckoutServiceImpl implements Wileyb2cFreeTrialCheckoutService
{
	@Resource
	private WileyProductService productService;

	@Resource
	private CartFactory wileyCartFactory;

	@Resource
	private CommerceAddToCartStrategy wileyb2cCommerceAddToCartStrategy;

	@Resource
	private CommerceCheckoutService commerceCheckoutService;

	@Resource
	private ModelService modelService;
	@Resource
	private Wileyb2cFreeTrialService wileyb2cFreeTrialService;

	@Resource
	private Populator<ProductModel, AbstractOrderEntryModel> wileyb2cSubscriptionProductEntryPopulator;

	@Override
	@Transactional
	public String placeFreeTrialOrder(final String code) throws CommerceCartModificationException, InvalidCartException
	{
		WileyProductModel productModel = productService.getWileyProductForCode(code);

		final CartModel cartModel = createCart();
		addFreeTrial(productModel, cartModel);
		return placeFreeTrialOrder(cartModel);
	}

	private CartModel createCart()
	{
		final CartModel cartModel = wileyCartFactory.createCart();
		cartModel.setOrderType(OrderType.FREE_TRIAL);
		return cartModel;
	}

	private void addFreeTrial(final WileyProductModel productModel, final CartModel cartModel)
			throws CommerceCartModificationException
	{
		CommerceCartParameter parameter = new CommerceCartParameter();
		parameter.setCart(cartModel);
		parameter.setProduct(productModel);
		parameter.setUnit(productModel.getUnit());
		parameter.setQuantity(1L);
		parameter.setSubscriptionTerm(wileyb2cFreeTrialService.getFreeTrialForProduct(productModel));
		CommerceCartModification commerceCartModification = wileyb2cCommerceAddToCartStrategy.addToCart(parameter);

		AbstractOrderEntryModel entry = commerceCartModification.getEntry();
		wileyb2cSubscriptionProductEntryPopulator.populate(productModel, entry);
	}

	private String placeFreeTrialOrder(final CartModel cartModel) throws InvalidCartException
	{
		CommerceCheckoutParameter placeOrderParameter = new CommerceCheckoutParameter();
		placeOrderParameter.setCart(cartModel);
		placeOrderParameter.setEnableHooks(true);
		CommerceOrderResult commerceOrderResult = commerceCheckoutService.placeOrder(placeOrderParameter);
		String orderCode = commerceOrderResult.getOrder().getCode();
		modelService.remove(cartModel);
		return orderCode;
	}
}
