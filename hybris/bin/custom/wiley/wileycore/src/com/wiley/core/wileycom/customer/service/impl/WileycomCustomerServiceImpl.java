package com.wiley.core.wileycom.customer.service.impl;

import de.hybris.platform.core.model.user.CustomerModel;

import javax.annotation.Resource;

import com.wiley.core.integration.customer.gateway.WileyCustomerGateway;
import com.wiley.core.wileycom.customer.service.WileycomCustomerService;

public class WileycomCustomerServiceImpl implements WileycomCustomerService
{
	@Resource
	private WileyCustomerGateway wileyCustomerGateway;

	@Override
	public boolean register(final CustomerModel customerModel, final String password)
	{
		return wileyCustomerGateway.registerCustomer(customerModel, password);
	}

}
