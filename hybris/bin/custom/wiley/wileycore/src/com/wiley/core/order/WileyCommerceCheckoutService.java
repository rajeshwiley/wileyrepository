package com.wiley.core.order;

import de.hybris.platform.commerceservices.order.CommerceCheckoutService;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;

import javax.annotation.Nonnull;


public interface WileyCommerceCheckoutService extends CommerceCheckoutService
{
	void setPaymentAddress(@Nonnull CommerceCheckoutParameter commerceCartParameter);
}
