/**
 *
 */
package com.wiley.core.externaltax.services.impl;

import de.hybris.platform.commerceservices.externaltax.CalculateExternalTaxesStrategy;
import de.hybris.platform.commerceservices.externaltax.RecalculateExternalTaxesStrategy;
import de.hybris.platform.commerceservices.externaltax.impl.DefaultExternalTaxesService;
import de.hybris.platform.commerceservices.order.CommerceCartHashCalculationStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceOrderParameter;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.externaltax.WileyExternalTaxDocument;
import com.wiley.core.externaltax.strategies.WileyCalculateExternalTaxesStrategy;


/**
 *
 */
public class WileyExternalTaxServiceImpl extends DefaultExternalTaxesService
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyExternalTaxServiceImpl.class);
	private static final Double ZERO_TAX = 0d;
	private static final String MSG_CLEARING_TAXES_FOR_ORDER = "Clearing taxes for order [%1$s] %2$s";
	private static final String MSG_CART_NOT_ELIGIBLE_FOR_TAX = "as cart is not eligible for tax calculation.";
	private static final String MSG_CART_MODIFIED_TAX_DISABLED = "as cart is modified and tax system is disable.";
	private static final String MSG_ERROR_IN_TAX_CLCULATION = "due to error in tax clculation.";

	private CommerceCartHashCalculationStrategy commerceCartHashCalculationStrategy;


	@Override
	public boolean calculateExternalTaxes(final AbstractOrderModel abstractOrder)
	{

		if (getDecideExternalTaxesStrategy().shouldCalculateExternalTaxes(abstractOrder))
		{
			if (Boolean.TRUE.equals(abstractOrder.getStore().getExternalTaxEnabled()))
			{
				try
				{
					final WileyExternalTaxDocument exTaxDocument = getCalculateExternalTaxesStrategy().calculateExternalTaxes(
							abstractOrder);
					getApplyExternalTaxesStrategy().applyExternalTaxes(abstractOrder, exTaxDocument);
					abstractOrder.setTaxCalculated(true);
					saveOrder(abstractOrder);
					saveHashForOrderInSession(abstractOrder);
				}
				catch (final Exception e)
				{
					// Clear taxes if there is any error in tax calculation
					LOG.error(String.format("Unexpected error occurs while getting tax for order [%1$s] with reason [%2$s]",
							abstractOrder.getCode(), e.getMessage()), e);
					clearTaxes(abstractOrder, MSG_ERROR_IN_TAX_CLCULATION);
				}
			}
			else
			{
				// Hash is not same means user has changed something in cart and
				// tax system is disabled so set tax to 0.
				if (getRecalculateExternalTaxesStrategy().recalculate(abstractOrder))
				{
					clearTaxes(abstractOrder, MSG_CART_MODIFIED_TAX_DISABLED);
				}
			}
		}
		else
		{
			// clear Taxes if cart is not eligible for external tax calculation
			// according to WelAgsDetermineExternalTaxStrategy
			clearTaxes(abstractOrder, MSG_CART_NOT_ELIGIBLE_FOR_TAX);
		}
		return false;
	}

	/**
	 * Generate and save hash in session. This has will be used to check if user
	 * has modified the cart in his current session.
	 */
	private void saveHashForOrderInSession(final AbstractOrderModel abstractOrder)
	{
		final CommerceOrderParameter parameter = new CommerceOrderParameter();
		parameter.setOrder(abstractOrder);
		final String orderCalculationHash = commerceCartHashCalculationStrategy.buildHashForAbstractOrder(parameter);
		getSessionService().setAttribute(RecalculateExternalTaxesStrategy.SESSION_ATTIR_ORDER_RECALCULATION_HASH,
				orderCalculationHash);
	}

	private void clearTaxes(final AbstractOrderModel abstractOrder, final String clearMsg)
	{
		abstractOrder.setTaxCalculated(false);

		if (isOrderHasTaxes(abstractOrder))
		{
			LOG.info(String.format(MSG_CLEARING_TAXES_FOR_ORDER, abstractOrder.getCode(), clearMsg));
			clearTaxValues(abstractOrder);
			abstractOrder.setTotalTax(ZERO_TAX);
		}
		saveOrder(abstractOrder);
	}

	private boolean isOrderHasTaxes(final AbstractOrderModel abstractOrder)
	{
		Boolean isTaxPresent = Boolean.FALSE;
		for (final AbstractOrderEntryModel entryModel : abstractOrder.getEntries())
		{
			if (!entryModel.getTaxValues().isEmpty())
			{
				isTaxPresent = Boolean.TRUE;
			}
		}
		return isTaxPresent;
	}

	@Override
	protected WileyCalculateExternalTaxesStrategy getCalculateExternalTaxesStrategy()
	{
		return (WileyCalculateExternalTaxesStrategy) super.getCalculateExternalTaxesStrategy();
	}

	@Override
	@Required
	public void setCalculateExternalTaxesStrategy(final CalculateExternalTaxesStrategy calculateExternalTaxesStrategy)
	{

		if (!(calculateExternalTaxesStrategy instanceof WileyCalculateExternalTaxesStrategy))
		{
			throw new IllegalStateException("This class requires " + WileyCalculateExternalTaxesStrategy.class.getName());
		}

		super.setCalculateExternalTaxesStrategy(calculateExternalTaxesStrategy);
	}

	@Required
	public void setCommerceCartHashCalculationStrategy(
			final CommerceCartHashCalculationStrategy commerceCartHashCalculationStrategy)
	{
		this.commerceCartHashCalculationStrategy = commerceCartHashCalculationStrategy;
	}
}
