package com.wiley.core.servicelayer.cronjob;

import de.hybris.platform.core.PK;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.CronJobService;


/**
 * @author Vadzim Yemelyanchyk <Vadzim_Yemelyanchyk@epam.com>
 */
public interface WileyCronJobService extends CronJobService
{
	CronJobModel getCronJobByPk(PK pk);

	CronJobModel getCronJobByPk(Long pk);
}
