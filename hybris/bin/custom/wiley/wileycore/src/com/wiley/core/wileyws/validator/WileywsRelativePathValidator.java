package com.wiley.core.wileyws.validator;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


public abstract class WileywsRelativePathValidator implements Validator
{
	private String path;

	@Override
	public void validate(final Object target, final Errors errors)
	{
		Assert.notNull(errors, "Errors object must not be null");
		if (pathObjectExists(errors))
		{
			validatePathObject(target, errors);
		}
	}

	protected abstract void validatePathObject(Object target, Errors errors);

	protected boolean pathObjectExists(final Errors errors)
	{
		return StringUtils.isNotEmpty(getPath()) && errors.getFieldValue(getPath()) != null;
	}

	public String getPath()
	{
		return path;
	}

	@Required
	public void setPath(final String path)
	{
		this.path = path;
	}
}
