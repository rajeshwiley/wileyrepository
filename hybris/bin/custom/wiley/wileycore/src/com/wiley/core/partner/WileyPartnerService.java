package com.wiley.core.partner;


import de.hybris.platform.core.model.order.AbstractOrderModel;

import java.util.List;
import java.util.Optional;

import javax.annotation.Nonnull;

import com.wiley.core.model.WileyPartnerCompanyModel;


public interface WileyPartnerService
{
	WileyPartnerCompanyModel getPartnerById(String partnerId);

	List<WileyPartnerCompanyModel> getPartnersByCategory(@Nonnull String categoryCode);

	boolean orderContainsPartnerProduct(WileyPartnerCompanyModel wileyPartner,
			AbstractOrderModel abstractOrder);

	/**
	 * Indicates if order has been submitted by "partner" flow
	 *
	 * @param orderModel order
	 * @return boolean
	 */
	boolean isPartnerOrder(@Nonnull AbstractOrderModel orderModel);

	/**
	 * Returns partner code for order submitted by "partner" flow
 	 *
	 * @param orderModel order
	 * @return Optional<String> partner code
	 */
	Optional<String> getPartnerCode(@Nonnull AbstractOrderModel orderModel);

	/**
	 *
	 * @param partnerCompanyUid
	 * @return true if partnerCompany belongs to kpmgPartner User Group, otherwise returns false
	 */
	boolean partnerBelongsToUserGroup(@Nonnull String partnerCompanyUid, @Nonnull String userGroupUid);
}
