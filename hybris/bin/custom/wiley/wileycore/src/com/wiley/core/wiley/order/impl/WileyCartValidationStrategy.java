package com.wiley.core.wiley.order.impl;

import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationStatus;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.commerceservices.strategies.CartValidationStrategy;
import de.hybris.platform.commerceservices.strategies.impl.DefaultCartValidationStrategy;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Required;

import com.wiley.core.order.hook.WileyCartValidateProductRestrictionsMethodHook;
import com.wiley.core.wiley.session.storage.WileyFailedCartModificationsStorageService;


/**
 * Validate cart on start checkout and place order
 *
 * @author Dzmitryi_Halahayeu
 */
public class WileyCartValidationStrategy extends DefaultCartValidationStrategy implements CartValidationStrategy
{
	@Resource
	protected WileyFailedCartModificationsStorageService wileyFailedCartModificationsStorageService;
	private WileyCartValidateProductRestrictionsMethodHook wileyCartValidateProductRestrictionsMethodHook;
	private boolean validateOOTB;

	@Override
	public List<CommerceCartModification> validateCart(final CommerceCartParameter parameter)
	{
		List<CommerceCartModification> modifications = new ArrayList<>();
		wileyCartValidateProductRestrictionsMethodHook.beforeCalculate(parameter);
		modifications.addAll(wileyFailedCartModificationsStorageService.popAll());
		if (isValidateOOTB())
		{
			modifications.addAll(super.validateCart(parameter));
		}
		return modifications;
	}

	/**
	 * OOTB code without stock levels validation
	 *
	 * @param cartModel
	 * @param cartEntryModel
	 * @return
	 */
	@Override
	protected CommerceCartModification validateCartEntry(final CartModel cartModel, final CartEntryModel cartEntryModel)
	{
		try
		{
			getProductService().getProductForCode(cartEntryModel.getProduct().getCode());
		}
		catch (final UnknownIdentifierException e)
		{
			return createEmptyProductModification(cartModel, cartEntryModel);
		}

		final CommerceCartModification modification = new CommerceCartModification();
		modification.setStatusCode(CommerceCartModificationStatus.SUCCESS);
		modification.setQuantityAdded(cartEntryModel.getQuantity());
		modification.setQuantity(cartEntryModel.getQuantity());
		modification.setEntry(cartEntryModel);
		return modification;
	}


	/**
	 * OOTB code
	 *
	 * @param cartModel
	 * @param cartEntryModel
	 * @return
	 */
	private CommerceCartModification createEmptyProductModification(final CartModel cartModel,
			final CartEntryModel cartEntryModel)
	{
		final CommerceCartModification modification = new CommerceCartModification();
		modification.setStatusCode(CommerceCartModificationStatus.UNAVAILABLE);
		modification.setQuantityAdded(0);
		modification.setQuantity(0);

		final CartEntryModel entry = new CartEntryModel()
		{
			@Override
			public Double getBasePrice()
			{
				return null;
			}

			@Override
			public Double getTotalPrice()
			{
				return null;
			}
		};
		entry.setProduct(cartEntryModel.getProduct());

		modification.setEntry(entry);

		getModelService().remove(cartEntryModel);
		getModelService().refresh(cartModel);

		return modification;
	}

	@Required
	public void setWileyCartValidateProductRestrictionsMethodHook(
			final WileyCartValidateProductRestrictionsMethodHook wileyCartValidateProductRestrictionsMethodHook)
	{
		this.wileyCartValidateProductRestrictionsMethodHook = wileyCartValidateProductRestrictionsMethodHook;
	}

	public boolean isValidateOOTB()
	{
		return validateOOTB;
	}

	@Required
	public void setValidateOOTB(final boolean validateOOTB)
	{
		this.validateOOTB = validateOOTB;
	}
}
