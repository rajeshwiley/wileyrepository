package com.wiley.core.wileyb2c.product;

import de.hybris.platform.core.model.product.ProductModel;


public interface Wileyb2cPurchaseOptionService
{
	/**
	 * Finds and returns product variant with lowest sequence number of variant classification class
	 */
	ProductModel getFirstPurchaseOptionVariant(ProductModel productModel);
}
