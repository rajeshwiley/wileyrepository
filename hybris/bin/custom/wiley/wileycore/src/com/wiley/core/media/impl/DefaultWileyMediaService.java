package com.wiley.core.media.impl;

import com.wiley.core.event.facade.mediacontainer.ConvertMediaContainerToMediaDataEvent;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.media.impl.DefaultMediaService;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.util.Assert;

import com.wiley.core.media.WileyMediaService;


/**
 * Created by Raman_Hancharou on 11/9/2015.
 */
public class DefaultWileyMediaService extends DefaultMediaService implements WileyMediaService,
		ApplicationEventPublisherAware
{

	private ApplicationEventPublisher applicationEventPublisher;

	@Override
	public String getFileNameWithoutExtension(final MediaModel mediaModel)
	{
		Assert.notNull(mediaModel, "MediaModel should not be null.");
		String fileName = mediaModel.getRealFileName();
		if (fileName != null && fileName.contains(".")) {
			return fileName.substring(0, fileName.lastIndexOf('.'));
		}
		return fileName;
	}

	@Override
	public String convertMediaContainerToMediaData(final CMSSiteModel currentSite, final MediaContainerModel mediaContainer) {
		final ConvertMediaContainerToMediaDataEvent event =
				new ConvertMediaContainerToMediaDataEvent(currentSite, mediaContainer);
		applicationEventPublisher.publishEvent(event);
		return event.getResult();
	}

	@Override
	public void setApplicationEventPublisher(final ApplicationEventPublisher applicationEventPublisher) {
		this.applicationEventPublisher = applicationEventPublisher;
	}
}
