package com.wiley.core.wileyb2c.restriction.impl;

import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.product.ProductModel;

import org.springframework.util.Assert;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.enums.WileyProductLifecycleEnum;
import com.wiley.core.wiley.restriction.WileyRestrictProductStrategy;
import com.wiley.core.wiley.restriction.dto.WileyRestrictionCheckResultDto;


public class Wileyb2cAvailabilityByStatusStrategy implements WileyRestrictProductStrategy
{
	@Override
	public boolean isRestricted(final ProductModel product)
	{
		Assert.notNull(product);
		return WileyProductLifecycleEnum.DISCONTINUED == product.getLifecycleStatus()
				|| WileyProductLifecycleEnum.PREVIEW == product.getLifecycleStatus();
	}

	@Override
	public boolean isRestricted(final CommerceCartParameter parameter)
	{
		return isRestricted(parameter.getProduct());
	}

	@Override
	public WileyRestrictionCheckResultDto createErrorResult(final CommerceCartParameter parameter)
	{
		return WileyRestrictionCheckResultDto.failureResult(WileyCoreConstants.PRODUCT_IS_NOT_PURCHASABLE,
				String.format("Product [%s] is not available due to the status [%s]",
						parameter.getProduct().getCode(), parameter.getProduct().getLifecycleStatus()),
				null);
	}
}
