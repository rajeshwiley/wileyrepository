package com.wiley.core.wileyb2c.product.access;

import de.hybris.platform.core.model.order.OrderEntryModel;

import javax.annotation.Nonnull;


/**
 * Strategy that generates redirect link to access digital product according to product digitalContentType
 *
 * @author Dzmitryi_Halahayeu
 */
public interface Wileyb2cDownloadDigitalProductStrategy
{
	boolean apply(@Nonnull OrderEntryModel orderEntry);

	String generateRedirectUrl(@Nonnull OrderEntryModel orderEntry) throws Exception;
}
