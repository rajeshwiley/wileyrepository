package com.wiley.core.product.interceptors;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import org.springframework.util.Assert;

import com.wiley.core.model.WileyProductModel;


/**
 * Class validates {@link WileyProductModel} before save it.
 */
public class WileyProductNameValidateInterceptor implements ValidateInterceptor<WileyProductModel>
{

	private static final int MAX_PRODUCT_NAME_SIZE = 100;

	@Override
	public void onValidate(final WileyProductModel productModel,
			final InterceptorContext interceptorContext) throws InterceptorException
	{
		checkName(productModel);
	}

	/**
	 * Checks WileyProductModel name length. It cannot exceed MAX_PRODUCT_NAME_SIZE
	 * All subtypes of WileyProductModel will have this validation
	 * VariantProduct and WileyVariantProduct won't have this validation as VariantProduct is subtype of ProductModel
	 *
	 * @param productModel
	 * @throws InterceptorException
	 */
	private void checkName(final WileyProductModel productModel) throws InterceptorException
	{
		Assert.notNull(productModel);


		final String name = productModel.getName();
		if (name != null && name.length() > MAX_PRODUCT_NAME_SIZE)
		{
			throw new InterceptorException(String.format(
					"Product Identifier cannot exceed [%s] characters  ", MAX_PRODUCT_NAME_SIZE));
		}


		return;

	}
}
