package com.wiley.core.wileycom.users.service.exception;

import org.springframework.security.core.AuthenticationException;


/**
 * Created by Mikhail_Asadchy on 17.06.2016.
 */
public class WileyUserUnreachableException extends AuthenticationException
{
	public WileyUserUnreachableException(final String msg, final Throwable t)
	{
		super(msg, t);
	}
}
