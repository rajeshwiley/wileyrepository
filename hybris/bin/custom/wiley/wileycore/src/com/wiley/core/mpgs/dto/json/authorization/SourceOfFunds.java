package com.wiley.core.mpgs.dto.json.authorization;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;



@JsonIgnoreProperties(ignoreUnknown = true)
public class SourceOfFunds
{
	private String token;

	public String getToken() { return token; }

	public void setToken(final String token) { this.token = token; }
}
