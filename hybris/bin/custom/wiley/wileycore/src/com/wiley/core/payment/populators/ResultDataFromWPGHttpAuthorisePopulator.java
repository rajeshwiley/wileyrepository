package com.wiley.core.payment.populators;

import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.response.AbstractResultPopulator;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.acceleratorservices.payment.data.WPGHttpValidateResultData;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.Map;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ResultDataFromWPGHttpAuthorisePopulator
		extends AbstractResultPopulator<Map<String, String>, CreateSubscriptionResult>
{
	private static final Logger LOG = LoggerFactory.getLogger("ResultDataFromWPGHttpAuthorisePopulator");
	private static final String OPERATION_PARAM = "operation";
	private static final String RETURN_CODE_PARAM = "returnCode";
	private static final String RETURN_MESSAGE_PARAM = "returnMessage";
	private static final String VENDOR_ID_PARAM = "vendorID";
	private static final String TRANSACTION_ID_PARAM = "transID";
	private static final String VALUE_PARAM = "value";
	private static final String CURRENCY_PARAM = "currency";
	private static final String MERCHANT_RESPONSE_PARAM = "merchantResponse";
	private static final String MERCHANT_ID_PARAM = "merchantID";
	private static final String AVS_ADDRESS_RESULT_PARAM = "AVSAddrResult";
	private static final String AVS_POST_RESULT_PARAM = "AVSPostResult";
	private static final String TOKEN_PARAM = "token";
	private static final String AUTH_CODE_PARAM = "authCode";
	private static final String ACQUIRER_ID_PARAM = "acquirerID";
	private static final String ACQUIRER_NAME_PARAM = "acquirerName";
	private static final String BANK_ID_PARAM = "bankID";
	private static final String BANK_NAME_PARAM = "bankName";
	private static final String MASKED_CARD_NUMBER_PARAM = "maskedCardNumber";
	private static final String CARD_EXPIRY_PARAM = "cardExpiry";
	private static final String TIMESTAMP_PARAM = "timestamp";
	private static final String CSC_RESULT_PARAM = "CSCResult";
	private static final String SECURITY_PARAM = "security";

	@Override
	public void populate(final Map<String, String> parameters, final CreateSubscriptionResult createSubscriptionResult)
			throws ConversionException	{
		final WPGHttpValidateResultData wpgResultInfoData = new WPGHttpValidateResultData();
		wpgResultInfoData.setOperation(parameters.get(OPERATION_PARAM));
		wpgResultInfoData.setReturnCode(parameters.get(RETURN_CODE_PARAM));
		wpgResultInfoData.setReturnMessage(parameters.get(RETURN_MESSAGE_PARAM));
		wpgResultInfoData.setVendorID(parameters.get(VENDOR_ID_PARAM));
		wpgResultInfoData.setTransID(parameters.get(TRANSACTION_ID_PARAM));
		wpgResultInfoData.setValue(parameters.get(VALUE_PARAM));
		wpgResultInfoData.setCurrency(parameters.get(CURRENCY_PARAM));
		wpgResultInfoData.setMerchantResponse(parameters.get(MERCHANT_RESPONSE_PARAM));
		wpgResultInfoData.setMerchantID(parameters.get(MERCHANT_ID_PARAM));
		wpgResultInfoData.setAVSAddrResult(parameters.get(AVS_ADDRESS_RESULT_PARAM));
		wpgResultInfoData.setAVSPostResult(parameters.get(AVS_POST_RESULT_PARAM));
		wpgResultInfoData.setAuthCode(parameters.get(AUTH_CODE_PARAM));
		wpgResultInfoData.setToken(parameters.get(TOKEN_PARAM));
		wpgResultInfoData.setAcquirerID(parameters.get(ACQUIRER_ID_PARAM));
		wpgResultInfoData.setAcquirerName(parameters.get(ACQUIRER_NAME_PARAM));
		wpgResultInfoData.setBankID(parameters.get(BANK_ID_PARAM));
		wpgResultInfoData.setBankName(parameters.get(BANK_NAME_PARAM));
		wpgResultInfoData.setMaskedCardNumber(parameters.get(MASKED_CARD_NUMBER_PARAM));
		wpgResultInfoData.setCardExpiry(parameters.get(CARD_EXPIRY_PARAM));
		wpgResultInfoData.setTimestamp(parameters.get(TIMESTAMP_PARAM));
		wpgResultInfoData.setCSCResult(parameters.get(CSC_RESULT_PARAM));
		wpgResultInfoData.setSecurity(parameters.get(SECURITY_PARAM));
		LOG.debug("Parsed Authorize operation on WPG HTTP Service. Response='{}'",
				ToStringBuilder.reflectionToString(wpgResultInfoData, ToStringStyle.MULTI_LINE_STYLE));

		createSubscriptionResult.setWpgResultInfoData(wpgResultInfoData);
		createSubscriptionResult.setRequestId(wpgResultInfoData.getTransID());
	}
}
