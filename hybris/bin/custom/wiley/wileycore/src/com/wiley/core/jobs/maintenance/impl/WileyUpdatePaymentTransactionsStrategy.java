package com.wiley.core.jobs.maintenance.impl;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.jobs.maintenance.MaintenanceCleanupStrategy;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;


public class WileyUpdatePaymentTransactionsStrategy implements MaintenanceCleanupStrategy<PaymentTransactionModel, CronJobModel>
{

	private static final String ORDERS_QUERY =
			"SELECT {PK} FROM {PaymentTransaction} WHERE {PaymentTransaction.plannedamount} IS NULL "
					+ "AND {PaymentTransaction.order} IS NOT NULL";

	@Resource
	private FlexibleSearchService flexibleSearchService;
	@Resource
	private ModelService modelService;
	@Resource
	private CommonI18NService commonI18NService;

	@Override
	public FlexibleSearchQuery createFetchQuery(final CronJobModel cjm)
	{
		final FlexibleSearchQuery fsq = new FlexibleSearchQuery(ORDERS_QUERY);
		fsq.setResultClassList(Collections.singletonList(CronJobModel.class));
		return fsq;
	}

	@Override
	public void process(final List<PaymentTransactionModel> elements)
	{
		elements.stream()
				.filter(paymentTransactionModel -> null != paymentTransactionModel.getOrder())
				.forEach(this::processTransactionInfoAmount);

		elements.stream()
				.filter(paymentTransactionModel -> null != paymentTransactionModel.getOrder())
				.flatMap(transaction -> transaction.getEntries().stream())
				.filter(transactionEntry -> transactionEntry.getType() == PaymentTransactionType.RETRIEVE_SESSION)
				.forEach(transactionEntry -> transactionEntry
						.setCurrency(transactionEntry.getPaymentTransaction().getOrder().getCurrency()));

		modelService.saveAll();
	}

	private void processTransactionInfoAmount(final PaymentTransactionModel transactionModel)
	{
		AbstractOrderModel order = transactionModel.getOrder();
		PaymentInfoModel paymentInfo = order.getPaymentInfo();
		CurrencyModel currency = order.getCurrency();
		if (currency != null)
		{
			Integer digits = currency.getDigits();
			if (digits == null)
			{
				digits = 0;
			}
			Double totalPrice = order.getTotalPrice();
			if (totalPrice == null)
			{
				totalPrice = 0d;
			}
			Double totalTax = order.getTotalTax();
			if (totalTax == null)
			{
				totalTax = 0d;
			}
			double totalPriceWithTax = commonI18NService.roundCurrency(totalPrice + totalTax, digits);

			transactionModel.setPlannedAmount(BigDecimal.valueOf(totalPriceWithTax));

		}
		transactionModel.setInfo(paymentInfo);
	}
}
