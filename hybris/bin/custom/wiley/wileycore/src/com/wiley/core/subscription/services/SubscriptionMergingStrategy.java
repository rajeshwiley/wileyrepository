package com.wiley.core.subscription.services;

import java.util.Date;

import javax.annotation.Nonnull;

import com.wiley.core.model.WileySubscriptionModel;


public interface SubscriptionMergingStrategy
{
	/**
	 * This method should calculate an expirationDate for new created subscription
	 * as a result of merged extension. Unutilized period of previous subscription
	 * must be summarized with whole period of new one.
	 * The current timestamp is considered as start date
	 *
	 * @param previousSubscr
	 * 		The subscription getting expired or extended, should have status Active to be extended
	 * @param nextSubscr
	 * 		The just created subscription whose expiration time we is calculating, assigned product should not be null
	 * @return The expiration time of merged extended period
	 */
	Date mergeSubscriptions(WileySubscriptionModel previousSubscr, @Nonnull WileySubscriptionModel nextSubscr);

	/**
	 * This method should calculate an expirationDate for new created subscription
	 * as a result of merged extension. Unutilized period of previous subscription
	 * must be summarized with whole period of new one.
	 *
	 * @param previousSubscr
	 * 		The subscription getting expired or extended, should have status Active to be extended
	 * @param nextSubscr
	 * 		The just created subscription whose expiration time we is calculating, assigned product should not be null
	 * @param date
	 * 		The moment we considered as a startTime of subscription
	 * @return The expiration time of merged extended period
	 */
	Date mergeSubscriptionsFromDate(WileySubscriptionModel previousSubscr, @Nonnull WileySubscriptionModel nextSubscr,
			Date date);
}
