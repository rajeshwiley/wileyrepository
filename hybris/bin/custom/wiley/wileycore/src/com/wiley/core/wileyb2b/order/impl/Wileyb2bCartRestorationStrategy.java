package com.wiley.core.wileyb2b.order.impl;

import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartRestoration;
import de.hybris.platform.commerceservices.order.CommerceCartRestorationException;
import de.hybris.platform.commerceservices.order.impl.DefaultCommerceCartRestorationStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartModel;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wiley.core.wileyb2b.order.exceptions.Wileyb2bCartCalculationSystemException;

import javax.annotation.Resource;

import com.wiley.core.wileyb2b.order.Wileyb2bCommerceAddToCartStrategy;


/**
 * Wileyb2b specific CartRestorationStrategy
 */
public class Wileyb2bCartRestorationStrategy extends DefaultCommerceCartRestorationStrategy
{

	private static final Logger LOG = LoggerFactory.getLogger(Wileyb2bCartRestorationStrategy.class);

	@Resource(name = "wileyb2bCommerceAddToCartStrategyImpl")
	private Wileyb2bCommerceAddToCartStrategy wileyb2bCommerceAddToCartStrategy;

	@Override
	public CommerceCartRestoration restoreCart(final CommerceCartParameter parameter) throws CommerceCartRestorationException
	{
		final CartModel cartModel = parameter.getCart();
		final CommerceCartRestoration restoration = new CommerceCartRestoration();
		final List<CommerceCartModification> modifications = new ArrayList<>();
		if (cartModel != null)
		{
			if (getBaseSiteService().getCurrentBaseSite().equals(cartModel.getSite()))
			{
				if (LOG.isDebugEnabled())
				{
					LOG.debug("Restoring from cart " + cartModel.getCode() + ".");
				}
				if (isCartInValidityPeriod(cartModel))
				{
					cartModel.setCalculated(Boolean.FALSE);
					if (!cartModel.getPaymentTransactions().isEmpty())
					{
						// clear payment transactions
						clearPaymentTransactionsOnCart(cartModel);
						// reset guid since its used as a merchantId for payment subscriptions
						// and is a base id for generating PaymentTransaction.code
						// see
						// de.hybris.platform.payment.impl.DefaultPaymentServiceImpl.authorize(DefaultPaymentServiceImpl.java:177)
						cartModel.setGuid(getGuidKeyGenerator().generate().toString());
					}

					getModelService().save(cartModel);
					try
					{
						getCommerceCartCalculationStrategy().recalculateCart(parameter);
					}
					catch (final IllegalStateException | Wileyb2bCartCalculationSystemException ex)
					{
						LOG.error("Failed to recalculate order [" + cartModel.getCode() + "]", ex);
					}

					getCartService().setSessionCart(cartModel);

					if (LOG.isDebugEnabled())
					{
						LOG.debug("Cart " + cartModel.getCode() + " was found to be valid and was restored to the session.");
					}
				}
				else
				{
					try
					{
						modifications.addAll(rebuildSessionCart(parameter));
					}
					catch (final CommerceCartModificationException | Wileyb2bCartCalculationSystemException e)
					{
						throw new CommerceCartRestorationException(e.getMessage(), e);
					}
				}
			}
			else
			{
				LOG.warn(String.format("Current Site %s does not equal to cart %s Site %s",
						getBaseSiteService().getCurrentBaseSite(), cartModel, cartModel.getSite()));
			}
		}
		restoration.setModifications(modifications);
		return restoration;
	}

	/**
	 * Custom B2B implementation replaces for-each adding of products from one cart to another with call to special
	 * batch method in AddToCartStrategy, that let us avoid cart recalculation many times.
	 * More details https://confluence.wiley.ru/display/ECSC/R2_POC%3A+External+cart+calculation
	 * @param parameter
	 * @param fromCartModel
	 * @param toCartModel
	 * @param modifications
	 * @throws CommerceCartModificationException
	 */
	@Override
	protected void rewriteEntriesFromCartToCart(final CommerceCartParameter parameter, final CartModel fromCartModel,
			final CartModel toCartModel, final List<CommerceCartModification> modifications)
			throws CommerceCartModificationException
	{
		wileyb2bCommerceAddToCartStrategy.addAllEntriesToCart(fromCartModel, toCartModel, modifications);
	}
}
