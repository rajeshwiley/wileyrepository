/**
 *
 */
package com.wiley.core.wileyb2c.cms2.servicelayer.services.impl;

import de.hybris.platform.cms2.exceptions.RestrictionEvaluationException;
import de.hybris.platform.cms2.model.restrictions.AbstractRestrictionModel;
import de.hybris.platform.cms2.servicelayer.data.RestrictionData;
import de.hybris.platform.cms2.servicelayer.services.impl.DefaultCMSRestrictionService;


/**
 *
 */
public class DefaultPageCMSRestrictionService extends DefaultCMSRestrictionService
{
	@Override
	public boolean evaluate(final AbstractRestrictionModel restriction, final RestrictionData restrictionDataInfo)
			throws RestrictionEvaluationException
	{
		return false;
	}
}
