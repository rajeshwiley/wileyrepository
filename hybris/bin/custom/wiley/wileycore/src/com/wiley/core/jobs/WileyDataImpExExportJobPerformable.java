package com.wiley.core.jobs;

import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.impex.model.ImpExMediaModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.impex.ExportConfig;
import de.hybris.platform.servicelayer.impex.ExportResult;
import de.hybris.platform.servicelayer.impex.ExportService;
import de.hybris.platform.servicelayer.impex.impl.FileBasedImpExResource;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.util.MediaUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.text.StrBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import com.google.common.base.Preconditions;
import com.wiley.core.model.WileyDataImpExExportCronJobModel;

import static com.wiley.core.constants.WileyCoreConstants.HYBRIS_DATA_DIR_KEY;
import static de.hybris.platform.util.CSVConstants.HYBRIS_ENCODING;


/**
 * Job produces export data and export media ZIP files on the filesystem.
 */
public class WileyDataImpExExportJobPerformable extends AbstractJobPerformable<WileyDataImpExExportCronJobModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(WileyDataImpExExportJobPerformable.class);
	private static final boolean NOT_APPEND = false;
	private static final String LANGUAGE_IMPEX_MACRO = "$lang";
	private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm-ss");

	@Resource
	private ConfigurationService configurationService;
	@Resource
	private ExportService exportService;
	@Resource
	private MediaService mediaService;
	@Value("${HYBRIS_TEMP_DIR}")
	private String tempDirectory;

	@Override
	public PerformResult perform(final WileyDataImpExExportCronJobModel cronJob)
	{
		LOG.info("Starting job with code={}", cronJob.getCode());


		final File script;
		try
		{
			script = generateExportScript(cronJob);
		}
		catch (IOException e)
		{
			LOG.error("Script generation failed due to IO issue", e);
			return new PerformResult(CronJobResult.FAILURE, CronJobStatus.ABORTED);
		}

		final ExportResult exportResult = performExport(script);

		if (exportResult.isSuccessful())
		{
			try
			{
				copyMedia(exportResult.getExportedData(), cronJob.getDataFilePath());
				if (exportResult.getExportedMedia() != null)
				{
					copyMedia(exportResult.getExportedMedia(), cronJob.getMediaFilePath());
				}
			}
			catch (IOException e)
			{
				LOG.error("ImpEx Export was executed successfully, but ImpExMediaModel copying failed due to IO issue", e);
				return new PerformResult(CronJobResult.FAILURE, CronJobStatus.ABORTED);
			}

			LOG.info("Successfully finished export and data copy");
			return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
		}
		else
		{

			LOG.error("Finished export with error. Files were not copied, see cronjob with code={}", cronJob.getCode());
			return new PerformResult(CronJobResult.FAILURE, CronJobStatus.FINISHED);
		}
	}

	private ExportResult performExport(final File script)
	{
		ExportConfig exportConfig = new ExportConfig();
		exportConfig.setScript(new FileBasedImpExResource(script, HYBRIS_ENCODING));
		return exportService.exportData(exportConfig);
	}

	@Nonnull
	private File generateExportScript(final WileyDataImpExExportCronJobModel cronJob)
			throws IOException
	{
		final String exportScriptPath = String.format("%s/data-export-%s-%s.impex", tempDirectory, cronJob.getCode(),
				DATE_TIME_FORMATTER.format(LocalDateTime.now()));
		File exportScript = new File(exportScriptPath);

		final String exportImpex = getResourceAsString(cronJob.getScript());
		final String localizedExportImpex = getResourceAsString(cronJob.getLocalizedScript());
		try (OutputStream exportFileStream = new FileOutputStream(exportScript))
		{
			IOUtils.write(exportImpex, exportFileStream);

			for (final LanguageModel language : cronJob.getBaseStore().getLanguages())
			{
				StrBuilder languageScript = new StrBuilder(localizedExportImpex);
				languageScript.replaceAll(LANGUAGE_IMPEX_MACRO, language.getIsocode());
				IOUtils.write(languageScript.toString(), exportFileStream);
			}
		}

		LOG.info("Created {} export file", exportScript.getAbsolutePath());
		return exportScript;
	}

	private String getResourceAsString(final String fileResource) throws IOException
	{
		try
		{
			return IOUtils.toString(getClass().getResourceAsStream(fileResource), HYBRIS_ENCODING);
		}
		catch (IOException e)
		{
			throw new RuntimeException("Cron job is not configured properly or files are missing", e);
		}
	}

	private void copyMedia(@Nonnull final ImpExMediaModel media, @Nonnull final String filePath) throws IOException
	{
		Preconditions.checkNotNull(media, "media parameter should not be null");
		Preconditions.checkNotNull(filePath, "filePath parameter should not be null");

		File targetFile = new File(configurationService.getConfiguration().getString(HYBRIS_DATA_DIR_KEY) + filePath);
		targetFile.getParentFile().mkdirs();
		LOG.info("Copying media with code={} to file with path {}", media.getCode(), targetFile.getAbsolutePath());
		try (InputStream input = mediaService.getStreamFromMedia(media);
				OutputStream output = new FileOutputStream(targetFile, NOT_APPEND))
		{
			MediaUtil.copy(input, output);
		}
	}



}
