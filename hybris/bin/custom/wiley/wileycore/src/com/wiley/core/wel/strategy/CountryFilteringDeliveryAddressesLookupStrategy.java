package com.wiley.core.wel.strategy;

import de.hybris.platform.commerceservices.delivery.DeliveryService;
import de.hybris.platform.commerceservices.strategies.impl.DefaultDeliveryAddressesLookupStrategy;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;


public class CountryFilteringDeliveryAddressesLookupStrategy extends DefaultDeliveryAddressesLookupStrategy
{
	@Resource
	private DeliveryService deliveryService;

	@Override
	public List<AddressModel> getDeliveryAddressesForOrder(final AbstractOrderModel abstractOrder,
			final boolean visibleAddressesOnly)
	{
		List<AddressModel> addresses = super.getDeliveryAddressesForOrder(abstractOrder, visibleAddressesOnly);
		return filterByDeliveryCountries(addresses, abstractOrder);
	}

	/**
	 * ECSC-29772: Restored from Hybris 6.2 DefaultDeliveryService.getSupportedDeliveryModesForOrder()
	 *   to prevent shipping address population while PayPal checkout:
	 *   the address has been populated while processing a response from PayPal by
	 *   PayPalHopPaymentResponseController:doHandleHopResponse().
	 */
	private List<AddressModel> filterByDeliveryCountries(final List<AddressModel> addresses,
			final AbstractOrderModel abstractOrder)
	{
		if (addresses != null && !addresses.isEmpty())
		{
			final List<CountryModel> deliveryCountries = deliveryService.getDeliveryCountriesForOrder(abstractOrder);
			final List<AddressModel> result = new ArrayList<>();

			// Filter for delivery addresses
			for (final AddressModel address : addresses)
			{
				if (address.getCountry() != null)
				{
					// Filter out invalid addresses for the site
					final boolean validForSite = deliveryCountries != null && deliveryCountries.contains(address.getCountry());

					if (validForSite)
					{
						result.add(address);
					}
				}
			}

			return result;
		}

		return Collections.emptyList();
	}
}