package com.wiley.core.wileyb2c.i18n.impl;

import com.wiley.core.constants.WileyCoreConstants;
import com.wiley.core.servicelayer.i18n.impl.WileycomI18NServiceImpl;
import com.wiley.core.wileyb2c.i18n.Wileyb2cI18NService;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.util.localization.Localization;

import java.util.Optional;


public class Wileyb2cI18NServiceImpl extends WileycomI18NServiceImpl implements Wileyb2cI18NService
{

	@Override
	public Optional<String> getCurrentCountryTaxShortMsg()
	{
		Optional<String> result = Optional.empty();
		final Optional<CountryModel> userSessionCountry = getCurrentCountry();

		if (userSessionCountry.isPresent())
		{
			String regionCode = userSessionCountry.get().getLegacyCart().getCode();

			if (isMessageRegionSpecific(regionCode))
			{
				result = Optional.of(getMsgForRegion(WileyCoreConstants.TAX_SHORT_MESSAGE_CODE, regionCode));
			}
		}

		return result;
	}

	@Override
	public Optional<String> getCurrentCountryTaxTooltip()
	{
		Optional<String> result = Optional.empty();
		final Optional<CountryModel> userSessionCountry = getCurrentCountry();

		if (userSessionCountry.isPresent())
		{
			String regionCode = userSessionCountry.get().getLegacyCart().getCode();

			if (isMessageRegionSpecific(regionCode))
			{
				result = Optional.of(getMsgForRegion(WileyCoreConstants.TAX_TOOLTIP_CODE, regionCode));
			}
		}

		return result;
	}

	@Override
	public String getCurrentDateFormat()
	{
		String result = getMsgForRegion(WileyCoreConstants.PUBLICATION_DATE_FORMAT, WileyCoreConstants.COUNTRY_US);
		final Optional<CountryModel> userSessionCountry = getCurrentCountry();

		if (userSessionCountry.isPresent())
		{
			String isocode = userSessionCountry.get().getIsocode();

			if (isNotCountryUS(isocode))
			{
				result = getMsgForRegion(WileyCoreConstants.PUBLICATION_DATE_FORMAT, WileyCoreConstants.COUNTRY_DEFAULT);
			}
		}

		return result;
	}
	protected String getLocalizedString(final String key) {
		return Localization.getLocalizedString(key);
	}

	private boolean isMessageRegionSpecific(final String regionCode)
	{
		return WileyCoreConstants.LEGACY_CART_REGION_AU.equals(regionCode) || WileyCoreConstants.LEGACY_CART_REGION_EU.equals(
				regionCode);
	}

	private boolean isNotCountryUS(final String isocode)
	{
		return !WileyCoreConstants.COUNTRY_US.equalsIgnoreCase(isocode);
	}

	private String getMsgForRegion(final String taxMsg, final String legacyCartCode)
	{

		return getLocalizedString(taxMsg.replace("<REGION_CODE>", legacyCartCode));
	}

}
