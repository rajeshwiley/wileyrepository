package com.wiley.core.order.dao.impl;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.order.daos.impl.DefaultOrderDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.wiley.core.enums.WileyProductLifecycleEnum;
import com.wiley.core.order.dao.WileyOrderDao;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


public class WileyOrderDaoImpl extends DefaultOrderDao implements WileyOrderDao
{
	private static final String FIND_ORDER_BY_GUID =
			"SELECT {order.PK} FROM {Order AS order} "
					+ "WHERE {order.guid}=?guid "
					+ "AND {order.site}=?site "
					+ "AND {order.originalVersion} IS NULL";

	private static final String QUERY_FIND_PRE_ORDERS_WITH_ACTIVE_PRODUCTS = "SELECT {o." + OrderModel.PK + "} FROM"
			+ " {" + OrderModel._TYPECODE + " AS o"
			+ " JOIN " + OrderEntryModel._TYPECODE + " AS oe ON {o." + OrderModel.PK + "} = {oe." + OrderEntryModel.ORDER + "}"
			+ " JOIN " + VariantProductModel._TYPECODE + " AS vp ON {vp." + VariantProductModel.PK + "} = {oe."
			+ OrderEntryModel.PRODUCT + "}"
			+ " JOIN " + ProductModel._TYPECODE + " AS bp ON {vp." + VariantProductModel.BASEPRODUCT + "} = {bp."
			+ ProductModel.PK + "}}"
			+ " WHERE {o." + OrderModel.STATUS + "} = ?orderStatus AND "
			+ "("
			+ "({bp." + ProductModel.LIFECYCLESTATUS + "} = ?lifeCycleStatus "
			+ "AND {vp." + ProductModel.LIFECYCLESTATUS + "} IS NULL )"
			+ " OR "
			+ "({bp." + ProductModel.LIFECYCLESTATUS + "} = ?lifeCycleStatus "
			+ "AND {vp." + ProductModel.LIFECYCLESTATUS + "} = ?lifeCycleStatus )"
			+ ")"
			+ " AND {o." + OrderModel.ORIGINALVERSION + "} IS NULL";

	@Resource
	private FlexibleSearchService flexibleSearchService;

	public List<OrderModel> findOrdersByGuid(final String guid, final BaseSiteModel baseSite)
	{
		validateParameterNotNull(guid, "guid must not be null!");
		validateParameterNotNull(baseSite, "baseSite must not be null!");
		final FlexibleSearchQuery query = new FlexibleSearchQuery(FIND_ORDER_BY_GUID);
		query.addQueryParameter("guid", guid);
		query.addQueryParameter("site", baseSite);
		return flexibleSearchService.<OrderModel> search(query).getResult();
	}


	@Override
	public List<OrderModel> findPreOrdersWithActiveProducts()
	{
		final Map<String, Object> params = new HashMap<>();
		params.put("orderStatus", OrderStatus.PREORDER);
		params.put("lifeCycleStatus", WileyProductLifecycleEnum.ACTIVE);
		SearchResult<OrderModel> searchResult = getFlexibleSearchService().search(
				QUERY_FIND_PRE_ORDERS_WITH_ACTIVE_PRODUCTS, params);
		return searchResult.getResult();
	}

	public FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}
}