package com.wiley.core.promotions.actions.rao.populators;

import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.ruleengineservices.converters.populator.CartRaoPopulator;
import de.hybris.platform.ruleengineservices.rao.AbstractRuleActionRAO;
import de.hybris.platform.ruleengineservices.rao.CartRAO;
import de.hybris.platform.ruleengineservices.rao.DiscountRAO;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.util.DiscountValue;

import java.util.LinkedHashSet;
import java.util.List;

import javax.annotation.Resource;

import com.wiley.core.model.WileyPartnerCompanyModel;


public class WileyCartRAOPopulator extends CartRaoPopulator
{
	@Resource(name = "wileyDiscountRaoConverter")
	private Converter<DiscountValue, DiscountRAO> wileyDiscountRaoConverter;

	@Override
	public void populate(final AbstractOrderModel source, final CartRAO target) throws ConversionException
	{
		super.populate(source, target);
		WileyPartnerCompanyModel wileyPartner = source.getWileyPartner();
		if (wileyPartner != null)
		{
			target.setPartnerId(wileyPartner.getUid());
		}

		populateActionsFromDiscountValues(source.getGlobalDiscountValues(), target);
	}

	/**
	 * ExternalDiscount and DiscountRow are presented as DiscountValues in the order
	 * This method populates actions from DiscountValues, which allows to use them during applying promotion rules
	 *
	 * @param discountValues
	 * @param target
	 */
	private void populateActionsFromDiscountValues(final List<DiscountValue> discountValues, final CartRAO target)
	{
		final List<DiscountRAO> discountActions = Converters.convertAll(discountValues, wileyDiscountRaoConverter);
		final LinkedHashSet<AbstractRuleActionRAO> actions = new LinkedHashSet<>();
		actions.addAll(discountActions);
		if (target.getActions() != null)
		{
			actions.addAll(target.getActions());
		}

		target.setActions(actions);
	}
}
