/**
 *
 */
package com.wiley.core.servicelayer.i18n.impl;

import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.i18n.L10NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.annotation.Nonnull;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import com.wiley.core.i18n.WileyCommonI18NService;
import com.wiley.core.i18n.WileyCountryService;
import com.wiley.core.servicelayer.i18n.WileycomI18NService;

import static com.wiley.core.constants.WileyCoreConstants.CURRENT_COUNTRY_SESSION_ATTR;


public class WileycomI18NServiceImpl implements WileycomI18NService
{

	private static final Logger LOG = LoggerFactory.getLogger(WileycomI18NServiceImpl.class);

	private String defaultCountryIsocode;

	@Autowired
	private WileyCountryService wileyCountryService;

	@Autowired
	private SessionService sessionService;

	@Autowired
	private CartService cartService;

	@Autowired
	private CommonI18NService commonI18NService;

	@Autowired
	private L10NService l10nService;

	@Autowired
	private ModelService modelService;

	@Autowired
	private WileyCommonI18NService wileyCommonI18NService;

	@Override
	public void setCurrentCountry(@Nonnull final CountryModel countryModel)
	{
		Assert.notNull(countryModel);

		sessionService.setAttribute(CURRENT_COUNTRY_SESSION_ATTR, countryModel);

		//OOTB SessionCurrencyChangeListener will change currency for cart
		commonI18NService.setCurrentCurrency(wileyCommonI18NService.getDefaultCurrency(countryModel));

		commonI18NService.setCurrentLanguage(countryModel.getDefaultLanguage());

		if (cartService.hasSessionCart())
		{
			final CartModel cart = cartService.getSessionCart();
			cart.setCountry(countryModel);
			modelService.save(cart);
		}

		LOG.debug("Setting current country [{}]", countryModel);
	}

	@Nonnull
	@Override
	public Optional<CountryModel> getCurrentCountry()
	{
		return Optional.ofNullable(sessionService.getAttribute(CURRENT_COUNTRY_SESSION_ATTR));
	}

	@Nonnull
	@Override
	public CountryModel setDefaultCurrentCountry()
	{
		final CountryModel defaultCountry = getDefaultCountry();

		setCurrentCountry(defaultCountry);

		LOG.debug("Setting default current country [{}]", defaultCountry);

		return defaultCountry;
	}


	@Nonnull
	@Override
	public CountryModel getDefaultCountry()
	{
		final CountryModel defaultCountry = wileyCountryService.findCountryByCode(defaultCountryIsocode);

		ServicesUtil.validateParameterNotNull(defaultCountry,
				String.format("Wrong config, not valid default country isocode [%s]", defaultCountryIsocode));

		return defaultCountry;
	}

	public void setDefaultCountryIsocode(final String defaultCountryIsocode)
	{
		this.defaultCountryIsocode = defaultCountryIsocode;
	}

	protected SessionService getSessionService()
	{
		return sessionService;
	}

	public List<CountryModel> getFallbackCountries(final CountryModel countryModel)
	{
		final List<CountryModel> result = new ArrayList<>();
		result.add(countryModel);
		final List<CountryModel> fallbackCountries = countryModel.getFallbackCountries();
		if (CollectionUtils.isNotEmpty(fallbackCountries))
		{
			result.addAll(fallbackCountries);
		}
		return result;
	}
}
