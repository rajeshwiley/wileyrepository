package com.wiley.core.payment.transaction.returns.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.returns.strategy.ReturnableCheck;


import com.wiley.core.refund.WileyRefundCalculationService;

import java.math.BigDecimal;



/**
 * Strategy for checking that total refund amount does not exceed initially charged Order total.
 */
public class WileyTotalAmountExcessReturnCheck implements ReturnableCheck
{
	private WileyRefundCalculationService calculationService;


	@Override
	public boolean perform(final OrderModel orderModel, final AbstractOrderEntryModel abstractOrderEntryModel, final long l)
	{
		return doesAnyAmountStillCanBeRefunded(orderModel);
	}

	private boolean doesAnyAmountStillCanBeRefunded(final OrderModel orderModel)
	{
		return calculationService.calculateMaximumAvailableRefundAmountForOrder(orderModel).compareTo(BigDecimal.ZERO) > 0;
	}


	public void setCalculationService(final WileyRefundCalculationService calculationService)
	{
		this.calculationService = calculationService;
	}
}
