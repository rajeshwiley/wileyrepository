package com.wiley.core.externaltax.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import com.wiley.core.externaltax.dto.TaxAddressDto;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;


public class TaxAddressDtoPopulator implements Populator<AddressModel, TaxAddressDto>
{
	@Override
	public void populate(final AddressModel source, final TaxAddressDto target) throws ConversionException
	{
		validateParameterNotNull(source, "source mustn't be null");
		validateParameterNotNull(target, "target mustn't be null");

		target.setPostcode(source.getPostalcode());
		target.setCity(source.getTown());
		populateCountryIsoCode(source, target);
		populateRegionIsoCode(source, target);
	}

	private void populateCountryIsoCode(final AddressModel source, final TaxAddressDto target)
	{
		final CountryModel country = source.getCountry();
		if (country != null)
		{
			target.setCountry(country.getIsocode());
		}
	}

	private void populateRegionIsoCode(final AddressModel source, final TaxAddressDto target)
	{
		final RegionModel region = source.getRegion();
		if (region != null)
		{
			target.setState(region.getIsocodeShort());
		}
	}
}
