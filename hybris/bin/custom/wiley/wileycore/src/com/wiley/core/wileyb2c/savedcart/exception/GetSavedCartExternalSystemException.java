package com.wiley.core.wileyb2c.savedcart.exception;


import com.wiley.core.exceptions.ExternalSystemException;


public class GetSavedCartExternalSystemException extends RuntimeException
{
	public GetSavedCartExternalSystemException(final String errorMessage, final ExternalSystemException exception)
	{
		super(errorMessage, exception);
	}
}
