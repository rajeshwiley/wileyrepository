package com.wiley.core.returns.strategy.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.returns.strategy.impl.DefaultConsignmentBasedReturnableCheck;

import java.util.ArrayList;
import java.util.List;


public class WileyConsignmentBasedReturnableCheck extends DefaultConsignmentBasedReturnableCheck
{
	private List<String> skipForSites = new ArrayList<>();

	@Override
	public boolean perform(final OrderModel order, final AbstractOrderEntryModel orderEntry, final long returnQuantity)
	{
		if (order.getSite() != null && skipForSites.contains(order.getSite().getUid()))
		{
			return true;
		}
		return super.perform(order, orderEntry, returnQuantity);
	}

	public List<String> getSkipForSites()
	{
		return skipForSites;
	}

	public void setSkipForSites(final List<String> skipForSites)
	{
		this.skipForSites = skipForSites;
	}
}
