package com.wiley.core.wileycom.media.translators.adapter.impl;

import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.impex.jalo.header.UnresolvedValueException;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import com.wiley.core.exceptions.ExternalSystemNotFoundException;
import com.wiley.core.wileycom.media.WileycomGetMediaGateway;
import com.wiley.core.wileycom.media.WileycomVerifyMediaGateway;
import com.wiley.core.wileycom.product.valuetranslator.adapter.impl.WileycomTemporaryRestrictionImportAdapterImpl;
import com.wiley.core.wileycom.valuetranslator.adapter.AbstractWileycomImportAdapter;


/**
 * @author Dzmitryi_Halahayeu
 */
public class WileycomExternalMediaUrlImportAdapterImpl extends AbstractWileycomImportAdapter
{
	private static final Logger LOG = LoggerFactory.getLogger(WileycomTemporaryRestrictionImportAdapterImpl.class);
	@Resource
	private ModelService modelService;
	@Resource
	private WileycomVerifyMediaGateway wileycomVerifyMediaGateway;
	@Resource
	private WileycomGetMediaGateway wileycomGetMediaGateway;

	/**
	 * Resolved external URL for import {@link MediaModel} using {@link WileycomVerifyMediaGateway}
	 *
	 * @param cellValue
	 * 		ISBN
	 * @param item
	 * 		media model
	 */
	@Override
	public void performImport(final String cellValue, final Item item) throws UnresolvedValueException
	{
		Assert.notNull(item);
		Assert.hasText(cellValue);
		String url = wileycomGetMediaGateway.getURL(cellValue);
		try
		{
			wileycomVerifyMediaGateway.verifyURL(url);
		}
		catch (ExternalSystemNotFoundException exception)
		{
			LOG.warn("Broken url " + url, exception);
			throw new UnresolvedValueException("Broken url " + url);
		}
		MediaModel mediaModel = modelService.get(item);
		mediaModel.setInternalURL(url);
	}

}
