package com.wiley.core.wileyas.order.strategies.calculation.impl;

import de.hybris.platform.jalo.order.price.PriceFactory;
import de.hybris.platform.order.strategies.calculation.impl.FindPricingWithCurrentPriceFactoryStrategy;


public class WileyasOrderEditFindPricingWithCurrentPriceFactoryStrategy extends FindPricingWithCurrentPriceFactoryStrategy
{
	private PriceFactory priceFactory;

	@Override
	public PriceFactory getCurrentPriceFactory()
	{
		return priceFactory;
	}

	public PriceFactory getPriceFactory()
	{
		return priceFactory;
	}

	public void setPriceFactory(final PriceFactory priceFactory)
	{
		this.priceFactory = priceFactory;
	}
}