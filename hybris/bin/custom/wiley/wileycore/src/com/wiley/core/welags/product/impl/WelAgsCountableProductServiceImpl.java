package com.wiley.core.welags.product.impl;

import de.hybris.platform.core.model.product.ProductModel;

import javax.annotation.Nonnull;

import com.wiley.core.enums.ProductEditionFormat;
import com.wiley.core.product.impl.AbstractWileyCountableProductService;


/**
 * @author Dzmitryi_Halahayeu
 */
public class WelAgsCountableProductServiceImpl extends AbstractWileyCountableProductService
{

	@Override
	protected boolean doCanProductHaveQuantity(@Nonnull final ProductModel product)
	{
		final ProductEditionFormat editionFormat = product.getEditionFormat();

		// if edition format is null we use OOTB behavior (product can have quantity).
		return editionFormat == null || ProductEditionFormat.PHYSICAL == editionFormat;
	}


}
